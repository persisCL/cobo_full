object frmActivacionesSerbanc: TfrmActivacionesSerbanc
  Left = 236
  Top = 291
  Caption = 'Generaci'#243'n de Archivo de Activaciones Serbanc'
  ClientHeight = 289
  ClientWidth = 557
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poMainFormCenter
  Visible = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  DesignSize = (
    557
    289)
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 8
    Top = 8
    Width = 542
    Height = 236
    Anchors = [akLeft, akTop, akRight, akBottom]
  end
  object lblNombreArchivo: TLabel
    Left = 18
    Top = 24
    Width = 125
    Height = 13
    Caption = 'Generar  Archivos en:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label1: TLabel
    Left = 18
    Top = 65
    Width = 120
    Height = 13
    Caption = 'Seleccione Empresa:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object btnProcesar: TButton
    Left = 307
    Top = 256
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Procesar'
    Enabled = False
    TabOrder = 0
    OnClick = btnProcesarClick
  end
  object btnCancelar: TButton
    Left = 388
    Top = 256
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Cancelar'
    Enabled = False
    TabOrder = 1
    OnClick = btnCancelarClick
  end
  object btnSalir: TButton
    Left = 470
    Top = 256
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Salir'
    TabOrder = 2
    OnClick = btnSalirClick
  end
  object pnlAyuda: TPanel
    Left = 499
    Top = 14
    Width = 33
    Height = 27
    Anchors = [akTop, akRight]
    TabOrder = 3
    object ImgAyuda: TImage
      Left = 1
      Top = 1
      Width = 31
      Height = 25
      Cursor = crHandPoint
      Align = alClient
      Anchors = [akTop, akRight]
      Picture.Data = {
        07544269746D617036100000424D361000000000000036000000280000002000
        0000200000000100200000000000001000000000000000000000000000000000
        0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00C6C6
        C600B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5
        B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5
        B500B5B5B500B5B5B500B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636B63006363
        6300636363006363630063636300636363006363630063636300636363006363
        6300636363006363630063636300636363006363630063636300636363006363
        63006363630063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF006363630094E7
        FF008CE7FF009CE7FF009CE7FF009CE7FF009CE7FF00A5EFFF00ADEFFF00ADEF
        FF00ADEFFF00ADEFFF00B5EFFF00BDEFFF00BDEFFF00BDEFFF00BDEFFF00BDEF
        FF00C6F7FF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00CEF7FF00D6F7FF00DEF7FF00DEF7
        FF00E7FFFF00E7FFFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00CEF7FF00D6F7FF00DEF7FF00DEF7
        FF00E7FFFF00E7FFFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00CEF7FF00D6F7FF00DEF7FF00DEF7
        FF00E7FFFF00E7FFFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF00E7FF
        FF00E7FFFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF006363
        630063636300EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF006363
        630063636300EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF009CD6
        FF00B5DEFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF009CD6
        FF00B5DEFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF006363
        63004AADFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF006363
        63000084FF00DEEFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF00B5DE
        FF0063636300108CFF00DEEFFF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF00E7FF
        FF009CD6FF0063636300108CFF00DEF7FF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF00E7FF
        FF00E7FFFF00DEEFFF0063636300108CFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00C6EFFF00ADDEFF00DEF7FF00E7FF
        FF00E7FFFF00EFFFFF0084C6FF0063636300A5D6FF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF006363630063636300DEF7FF00E7FF
        FF00E7FFFF00EFFFFF00A5D6FF0063636300A5D6FF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF0042A5FF006363630094D6FF00E7FF
        FF00E7FFFF00DEEFFF00108CFF0063636300DEF7FF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00C6EFFF00108CFF0063636300319C
        FF004AADFF000084FF00636363006BBDFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00CEEFFF0042A5FF006363
        6300636363006363630084C6FF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF00E7FF
        FF00E7FFFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00CEF7FF00D6F7FF00DEF7FF00DEF7
        FF00E7FFFF00E7FFFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00CEF7FF00D6F7FF00DEF7FF00DEF7
        FF00E7FFFF00E7FFFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0063636300BDEF
        FF00BDEFFF00BDEFFF00BDEFFF00BDEFFF00BDEFFF00BDEFFF00BDEFFF00BDEF
        FF00BDEFFF00BDEFFF00BDEFFF00BDEFFF00BDEFFF00BDEFFF00BDEFFF00BDEF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636B63006363
        6300636363006363630063636300636363006363630063636300636363006363
        6300636363006363630063636300636363006363630063636300636363006363
        63006363630063636300FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00}
      Stretch = True
      Transparent = True
      OnClick = ImgAyudaClick
      OnMouseMove = ImgAyudaMouseMove
    end
  end
  object pnlAvance: TPanel
    Left = 25
    Top = 92
    Width = 499
    Height = 129
    Anchors = [akLeft, akRight, akBottom]
    BevelOuter = bvNone
    TabOrder = 4
    Visible = False
    DesignSize = (
      499
      129)
    object lblProcesoGeneral: TLabel
      Left = 8
      Top = 78
      Width = 43
      Height = 13
      Anchors = [akLeft, akBottom]
      Caption = 'Progreso'
    end
    object lblReferencia: TLabel
      Left = 80
      Top = 56
      Width = 62
      Height = 13
      Caption = 'lblReferencia'
    end
    object lblEmpresa: TLabel
      Left = 80
      Top = 37
      Width = 62
      Height = 13
      Caption = 'lblEmpresa'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 196
      Top = 111
      Width = 161
      Height = 13
      Caption = 'Presione [Esc] para Cancelar'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object pbProgreso: TProgressBar
      Left = 64
      Top = 77
      Width = 432
      Height = 17
      Anchors = [akLeft, akRight, akBottom]
      Max = 180000
      Smooth = True
      Step = 1
      TabOrder = 0
    end
    object dbgActivaciones: TDBGrid
      Left = 390
      Top = 61
      Width = 108
      Height = 52
      DataSource = dsActivaciones
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      Visible = False
    end
    object dbgPatentes: TDBGrid
      Left = 390
      Top = 13
      Width = 107
      Height = 42
      DataSource = dsPatentes
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      Visible = False
    end
  end
  object txtDestinoArchivo: TPickEdit
    Left = 16
    Top = 40
    Width = 393
    Height = 21
    Enabled = True
    ReadOnly = True
    TabOrder = 5
    EditorStyle = bteTextEdit
    OnButtonClick = txtDestinoArchivoButtonClick
  end
  object cbEmpresasRecaudadoras: TVariantComboBox
    Left = 16
    Top = 81
    Width = 393
    Height = 21
    Style = vcsDropDownList
    ItemHeight = 13
    TabOrder = 6
    Items = <>
  end
  object spObtenerComprobantesPendientesActivacionSerbanc: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorLocation = clUseServer
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'ObtenerComprobantesPendientesActivacionSerbanc;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoEmpresasRecaudadoras'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 76
    Top = 12
  end
  object spObtenerConsumosPorPatenteSerbanc: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    CommandTimeout = 5000
    ProcedureName = 'ObtenerConsumosPorPatenteSerbanc'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Direction = pdInputOutput
        Precision = 19
        Value = Null
      end
      item
        Name = '@TotalImporte'
        Attributes = [paNullable]
        DataType = ftLargeint
        Direction = pdInputOutput
        Precision = 19
        Value = Null
      end
      item
        Name = '@TotalCantidad'
        Attributes = [paNullable]
        DataType = ftLargeint
        Direction = pdInputOutput
        Precision = 19
        Value = Null
      end
      item
        Name = '@CodigoEmpresasRecaudadoras'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 108
    Top = 12
  end
  object SQLQuery: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <>
    Left = 140
    Top = 12
  end
  object spActualizarComprobantesEnviadosSerbanc: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CommandTimeout = 3000
    ProcedureName = 'ActualizarComprobantesEnviadosSerbanc'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoOperacionInterface'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoEmpresasRecaudadoras'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 172
    Top = 12
  end
  object spPrepararComprobantesPendientesActivacionSerbanc: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'PrepararComprobantesPendientesActivacionSerbanc'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@MaximoComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Direction = pdInputOutput
        Precision = 19
        Value = Null
      end
      item
        Name = '@Cantidad'
        Attributes = [paNullable]
        DataType = ftLargeint
        Direction = pdInputOutput
        Precision = 19
        Value = Null
      end
      item
        Name = '@CodigoEmpresasRecaudadoras'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 308
    Top = 12
  end
  object spObtenerComprobantesPendientesActivacionSerbancDomPpal: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerComprobantesPendientesActivacionSerbancDomPpal'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Cantidad'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoEmpresasRecaudadoras'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 348
    Top = 12
  end
  object spObtenerComprobantesPendientesActivacionSerbancDomFact: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerComprobantesPendientesActivacionSerbancDomFact'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Cantidad'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoEmpresasRecaudadoras'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 236
    Top = 12
  end
  object spLimpiarTemporales: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'LimpiarTemporalesActivacionesSerbanc'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 424
    Top = 12
  end
  object spInsertarErroresSerbanc: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CommandTimeout = 5000
    ProcedureName = 'InsertarErroresActivacionesSerbanc'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoOperacionInterface'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 204
    Top = 12
  end
  object spPrepararTablasTemporalesActivacionesSerbanc: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'PrepararTablasTemporalesActivacionesSerbanc'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 272
    Top = 12
  end
  object ObtenerTotalEnviado: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerTotalComprobantesActivacionSerbanc;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalComprobantes'
        Attributes = [paNullable]
        DataType = ftLargeint
        Direction = pdInputOutput
        Precision = 19
        Value = Null
      end
      item
        Name = '@CantidadComprobantes'
        Attributes = [paNullable]
        DataType = ftLargeint
        Direction = pdInputOutput
        Precision = 19
        Value = Null
      end
      item
        Name = '@CodigoEmpresasRecaudadoras'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 388
    Top = 12
  end
  object dsActivaciones: TDataSource
    DataSet = spObtenerComprobantesPendientesActivacionSerbanc
    Left = 384
    Top = 152
  end
  object dsPatentes: TDataSource
    DataSet = spObtenerConsumosPorPatenteSerbanc
    Left = 384
    Top = 104
  end
end
