{----------------------------------------------------------------------------
 File Name: ImprentaJORDAN.pas
 Author:    gcasais
 Date Created: 04/12/2004
 Language: ES-AR
 Description:  M�dulo de interface de impresi�n masiva para imprenta

 Revision 1:
 Author:       sguastoni
 Date Created: 05/01/2006
 Description:  Agregado campo @Usuario a spActualizarFechaImpresionComprobantesEnviadosAImprenta

 Revision 2:
    Date: 03/03/2009
    Author: mpiazza
    Description:    Ref (SRX01226) se cambia" now " por nowbase

 Revision 3:
    Date: 04/03/2009
    Author: pdominguez
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura


 Revision 4:
    Date : 11-03-2009
    Author : Nelson Droguett Sierra
    Description : Separar la generacion de los archivos de interface agrupando
                  TipoComprobanteFiscal
 Revision 5:
    Date : 19-03-2009
    Author : Nelson Droguett Sierra
    Description : Se integra la DLL para el timbre electronico.
                  Se agrega el XML de timbre electronico a los archivos planos


  Revision 6:
    Date : 02-04-2009
    Author : Nelson Droguett Sierra
    Description : En el caso de existir un archivo con el mismo nombre en el
    directorio seleccionado, debe preguntar si la reescribe o no.

  Revision 7:
    Date : 11-06-2009
    Author : Nelson Droguett Sierra
    Description : Se cambia la obtencion del timbre electronico llamando primero
                  a una funcion que obtiene los datos del comprobante electronico
                  para el timbre.

Revision 8:
Author: mbecerra
Date: 27-Junio-2009
Description:    1.-		Se agrega un TFileStream de modo de no cargar todo en memoria
                    	en TStringList y que colapse por memoria insuficiente. Para
                        eso se crea una constante que define la cantidad de bytes
                        que una vez que el tama�o de la TStringList alcanza ese valor,
                        se graba en disco usando el FileStream.

Revision    :9
Author      : Nelson Droguett Sierra
Fecha       : 09-07-2009
Descripcion : Importa desde el servidor la estructura de directorios de la DLL
                para el timbrado electronico a una carpeta local especificada
                como parametrogeneral DIR_JORDAN_TIMBRE_ELECTRONICO. Esto porque
                el timbrado electronico falla en trabajos masivos si pierde la
                conexion.
              El vaciado desde los stringList a disco, se hace cada 1 MB de informacion
              se escriben los fileStream, y ademas se descarga y carga nuevamente la DLL

Revision    : 10
Author      : Nelson Droguett Sierra
Date        : 06-Noviembre-2009
Descripcion : Se llama al LlenarDetalleConsumoComprobantes, para asegurarse que todos los
            comprobantes tengan DetalleConsumoComprobantes. Si no tuviera el archivo de
            JORDAN es rechazado porque le faltan lineas. La razon de que no todos tengan
            esta en el proceso de facturacion. Esta medida de control de seguridad se hace
            solo para garantizar la existencia de todos los detalles ANTES de emitor JORDAN.

            Ademas, no existe ahora la posibilidad de seleccionar la carpeta de destino de los
            archivos, ya que son producto de un comando BCP , en una consola de SQL, desde
            donde no se podria acceder a una carpeta local seleccionada por el usuario en su
            m�quina. Asi que solo se informa en que carpeta quedaran los archivos sin posibilidad
            de que el usuario lo modifique.

Revision    : 11
Author      : Nelson Droguett Sierra
Date        : 03-Junio-2010
Descripcion : Se copia de la carpeta DBNet solo los archivos necesarios para el funcionamiento
            	dlls y directorio de los CAF

Firma       : SS-954-NDR-20110907
Description : Se muestra el contenido de la tabla Jordan_Advertencias en el caso de tener registros al final del proceso

Firma       : SS-330-NDR-20120117
Description : Se fuerza el parametro Password en el ConnectionString de la conexion BaseJordan

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)


Firma       : SS_1385_NDR_20150922
Descripcion : Cambiar la transaccionalidad de delphi por Execute('TRANSACCIONALIDAD SQL') para evitar tener rollback oculto


-----------------------------------------------------------------------------}
unit ImprentaJORDAN;

interface

uses
    //Imprenta
    // Rev.5 / 22-03-2009 / Nelson Droguett Sierra
    DTEControlDLL ,               // DLL Para Facturacion Electronica
    DMConnection,                 //Coneccion a base de datos OP_CAC
    Util,                         //stringtofile,padl..
    UtilProc,                     //Mensajes
    UtilDB,                       //Rutinas para base de datos
    //ComunesInterfaces,            //Procedimientos y Funciones Comunes a todos los formularios
    ComunInterfaces,            //Procedimientos y Funciones Comunes a todos los formularios
    PeaTypes,                     //Constantes
    PeaProcs,                     //NowBase
    ConstParametrosGenerales,     //Constantes
    SeleccionProcFacturacion,     //Selecciona el proceso de Facturaci�n
    //Otros
    Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
    Dialogs,  StdCtrls, ExtCtrls, DB, ADODB, ListBoxEx, DBListEx, Buttons,
    DmiCtrls, DPSControls, ComCtrls, DateUtils, DBTables, BuscaTab, Grids,
    DBGrids, Mask, DBCtrls, EventLog, ActnList, StrUtils, inifiles, Math, PsAPI,
    ShellApi;

type
    TfrmImprentaJORDAN = class(TForm)
        nb: TNotebook;
        Label1: TLabel;
        Bevel1: TBevel;
        Bevel2: TBevel;
        DBListEx1: TDBListEx;
        Label2: TLabel;
        DS: TDataSource;
        lblCantidad: TLabel;
        Label6: TLabel;
        Label7: TLabel;
        btnSiguiente: TButton;
        btnObtenerComprobantes: TButton;
        btnAnterior: TButton;
        btnFinalizar: TButton;
        btnSalir: TButton;
        pnlprogreso: TPanel;
        Label8: TLabel;
        pbProgreso: TProgressBar;
        bteImagen: TBuscaTabEdit;
        Label9: TLabel;
        btCatalogo: TBuscaTabla;
        tblCatalogoImagenesImprenta: TADOTable;
        lblDescri: TLabel;
        spObtenerComprobantesImprentaPreview: TADOStoredProc;
        spActualizarComprobantesEnviadosAImprenta: TADOStoredProc;
        Update: TADOQuery;
        spPrepararProcesoInterfazImprenta: TADOStoredProc;
        spObtenerLineasInterfazImprenta: TADOStoredProc;
        qryEliminarTemporales: TADOQuery;
        labelProgreso: TLabel;
        neComprobanteInicial: TNumericEdit;
        neComprobanteFinal: TNumericEdit;
        Label3: TLabel;
        Label5: TLabel;
        qryLineas: TADOQuery;
        btnBuscarProceso: TButton;
        spObtenerOperacionesImpresionMasiva: TADOStoredProc;
        spObtenerOperacionesImpresionMasivaCodigoOperacionInterfase: TIntegerField;
        spObtenerOperacionesImpresionMasivaFecha: TDateTimeField;
        spObtenerOperacionesImpresionMasivaUsuario: TStringField;
        spObtenerOperacionesImpresionMasivaCantidad: TIntegerField;
        buscaOperaciones: TBuscaTabla;
        btnProcesosFacturacion: TButton;
        lblForzandoReimpresion: TLabel;
        lblRangoSeleccionado: TLabel;
        lblImprimiendoElectronicos: TLabel;
        Label77: TLabel;
        txtUbicacion: TPickEdit;
        spActualizarFechaImpresionComprobantesEnviadosAImprenta: TADOStoredProc;
    ActionList1: TActionList;
    AConfig_EGATE_HOME_Manual: TAction;
    AConfig_EGATE_HOME_Parametrico: TAction;
    ACargarDLL: TAction;
    ATimbrarDinamico: TAction;
    ATimbraryObtenerImagen: TAction;
    ALiberarDLL: TAction;
    imgTimbre: TImage;
    lblProcesoInterrumpido: TLabel;
    spObtenerDatosParaTimbreElectronico: TADOStoredProc;
    qryLineasTipoComprobante: TStringField;
    qryLineasNumeroComprobante: TLargeintField;
    qryLineasLinea: TStringField;
    qryLineasTipoComprobanteFiscal: TStringField;
    qryLineasNumeroComprobanteFiscal: TLargeintField;
    qryLineasGrupoImprenta: TIntegerField;
    qryLineasFechaEmision: TDateTimeField;
    qryLineasImporteTotal: TLargeintField;
    qryLineasPrimeraLineaDetalle: TStringField;
    qryLineasRutReceptor: TStringField;
    qryLineasRazonSocialReceptor: TStringField;
    qryLineasFechaHoraTimbre: TDateTimeField;
    qryLineasCodigoConcepto: TIntegerField;
    dbgDatosJORDAN: TDBGrid;
    dsObtenerDatosJORDAN: TDataSource;
    ObtenerDatosJORDAN: TADOStoredProc;
    JORDAN_ActualizarTED: TADOStoredProc;
    JORDAN_ObtenerComprobantesATimbrar: TADOStoredProc;
    dsJORDANObtenerComprobantesATimbrar: TDataSource;
    DBGJordanObtenerComprobantesATimbrar: TDBGrid;
    Panel1: TPanel;
    Panel5: TPanel;
    pbPaso1Simple: TProgressBar;
    pbPaso4Multiple: TProgressBar;
    pbPaso3Multiple: TProgressBar;
    pbPaso2Multiple: TProgressBar;
    pbPaso2Simple: TProgressBar;
    pbPaso3Simple: TProgressBar;
    pbPaso4Simple: TProgressBar;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    pbPaso1Multiple: TProgressBar;
    BaseJORDAN: TADOConnection;
    JORDAN_ActualizarFechaHoraImpresion: TADOStoredProc;
    JORDAN_LineasAImprenta: TADOStoredProc;
    spLlenarDetalleConsumoComprobantes: TADOStoredProc;
    pnlTop: TPanel;                                                             //SS-954-NDR-20110907
    pnlClient: TPanel;                                                          //SS-954-NDR-20110907
    DBLEJordanAdvertencias: TDBListEx;                                          //SS-954-NDR-20110907
    lblTitulo: TLabel;                                                          //SS-954-NDR-20110907
    lblAdvertencia: TLabel;                                                     //SS-954-NDR-20110907
    qryJordanAdvertencia: TADOQuery;                                            //SS-954-NDR-20110907
    dsJordanAdvertencias: TDataSource;                                          //SS-954-NDR-20110907
    DBMemoMensaje: TDBMemo;                                                     //SS-954-NDR-20110907
    DBMemoLinea: TDBMemo;                                                       //SS-954-NDR-20110907
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSalirClick(Sender: TObject);
    procedure btnSiguienteClick(Sender: TObject);
    procedure btnAnteriorClick(Sender: TObject);
    procedure btnObtenerComprobantesClick(Sender: TObject);
    procedure btnFinalizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    function  btCatalogoProcess(Tabla: TDataSet; var Texto: String): Boolean;
    procedure btCatalogoSelect(Sender: TObject; Tabla: TDataSet);
    procedure btnBuscarProcesoClick(Sender: TObject);
    function  buscaOperacionesProcess(Tabla: TDataSet; var Texto: String): Boolean;
    procedure buscaOperacionesSelect(Sender: TObject; Tabla: TDataSet);
    procedure btnProcesosFacturacionClick(Sender: TObject);
    procedure txtUbicacionButtonClick(Sender: TObject);
    // Rev.5 / 22-03-2009 / Nelson Droguett Sierra
    procedure AConfig_EGATE_HOME_ManualExecute(Sender: TObject);
    procedure AConfig_EGATE_HOME_ParametricoExecute(Sender: TObject);
    procedure ACargarDLLExecute(Sender: TObject);
    procedure ALiberarDLLExecute(Sender: TObject);
    private
        FCodigoOperacion : Integer;
        FDestino : AnsiString;
        FOutFile : TextFile;
        FNombreArchivoSimple : AnsiString;
        FNombreArchivoControlSimple : AnsiString;
        FNombreArchivoMultiple : AnsiString;
        FNombreArchivoControlMultiple : AnsiString;

        // Rev.4 / 11-03-2009 / Nelson Droguett Sierra ------------------------
        FOutFileFAC : TextFile;
        FNombreArchivoSimpleFAC : AnsiString;
        FNombreArchivoControlSimpleFAC : AnsiString;
        FNombreArchivoMultipleFAC : AnsiString;
        FNombreArchivoControlMultipleFAC : AnsiString;

        FOutFileBOL : TextFile;
        FNombreArchivoSimpleBOL : AnsiString;
        FNombreArchivoControlSimpleBOL : AnsiString;
        FNombreArchivoMultipleBOL : AnsiString;
        FNombreArchivoControlMultipleBOL : AnsiString;
        //---------------------------------------------------------------------
        //---------------------------------------------------------------------


        FCancel : Boolean;
        FOnProcess : Boolean;
        FComprobantesAfectados : Integer;
        //FComprobantesExitosos : Integer;
        //FCantidadRegistros : Integer;
        FNombreImagenCatalogo : AnsiString;
        //FTotalComprobantes : Int64;
        FFechaProceso : TDateTime;
        FDetalleTransitosaparte : Boolean;
        FReproceso : Boolean;
        FPorFacturacion : boolean;
        FFechaProcesoAnterior : TDateTime;
        FFechaProcFacturacion : AnsiString;
        FNumeroProcesoAnterior : Longint;
        FNumeroProceso : Integer;
        FCantidadFacturados : Integer;
        FCantSinImprimir : Integer;
        FComprobanteInicial : Int64;
        FComprobanteFinal : Int64;
        FNoPrnFinal : Int64;
        FNoPrnInicial : Int64;
        FForzarReimpresion : Boolean;
        FImprimirElectronicos : Boolean;
        FHayDatosArchivoSimple : Boolean;
        FHayDatosArchivoMultiple : Boolean;

        // Rev.4 / 11-03-2009 / Nelson Droguett Sierra ------------------------
        FHayDatosArchivoSimpleFAC : Boolean;
        FHayDatosArchivoMultipleFAC : Boolean;
        FHayDatosArchivoSimpleBOL : Boolean;
        FHayDatosArchivoMultipleBOL : Boolean;
        FComprobantesExitososFAC : Integer;
        FCantidadRegistrosFAC : Integer;
        FTotalComprobantesFAC : Int64;
        FComprobantesExitososBOL : Integer;
        FCantidadRegistrosBOL : Integer;
        FTotalComprobantesBOL : Int64;
        FCantidadTimbresFAC : Integer;
        FCantidadTimbresBOL : Integer;
        //---------------------------------------------------------------------
        MSG_SQL_SQL:String;
        //---------------------------------------------------------------------
        FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
        procedure CrearNombresArchivos(Path: String);
        function  CrearArchivo(Archivo : String; var DescriError : AnsiString; var FExist : Boolean) : Boolean;
        function  RegistrarOperacion: boolean;
        function  CerrarOperacion : Boolean;
        function  ActualizarComprobantesEnviados : Boolean;
        function  ActualizarComprobantes : Boolean;
        procedure HabilitarControles(Habilitado : Boolean);
        procedure CargarComprobantesPorFacturacion;
        Function  ObtenerCantidadComprobantesAImprimir : int64;
        Function  ObtenerCantidadComprobantesAImprimirFACSimple : int64;
        Function  ObtenerCantidadComprobantesAImprimirBOLSimple : int64;
        Function  ObtenerCantidadComprobantesAImprimirFACMultiple : int64;
        Function  ObtenerCantidadComprobantesAImprimirBOLMultiple : int64;
        Function  ObtenerComprobantesPendientesSimples : int64;
        Function  ObtenerComprobantesPendientesMultiples : int64;

        procedure EscribirArchivoLog(ArchLog, Texto: AnsiString);
        function  ObtieneTipoDocumento(sTipoComprobanteFiscal:String) : integer;
        function  EscribeLog(sMensaje:String):Boolean;

        function  PrepararProcesoInterfazImprenta:Boolean;
        function  ObtenerLineasInterfazImprenta:Boolean;
        function  TimbrarComprobantesElectronicos:Boolean;
        function  GenerarArchivosDatos:Boolean;

        // Rev. 10 / 06-Noviembre-2009 / Nelson Droguett Sierra ----------------------------
        function LlenarDetalleConsumoComprobantes:Boolean;
        //----------------------------------------------------------------------------------


      public
        iComprobantesSimples : int64;
        iComprobantesMultiples : int64;

        CantidadFAC : int64;
        CantidadBOL : int64;
        CantidadFACSimple : int64;
        CantidadBOLSimple : int64;
        CantidadFACMultiple : int64;
        CantidadBOLMultiple : int64;
        FLogBuffer : TStringList;
        function Inicializar : Boolean;
        function CurrentMemoryUsage : Cardinal;
        Function DelTree(DirName : string): Boolean;
    end;

var
  frmImprentaJORDAN: TfrmImprentaJORDAN;

const
    //REV.8 tama�o en bytes a procesar en la TStringList
    CONST_TAMANO_BYTES_STRINGLIST = 5 * 1024 * 1024;			{1 MB}

implementation

{$R *.dfm}

const
    TOP_SP_OBTENER_LINEAS = 1000;

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    gcasais
  Date Created: 03/12/2004
  Description:  Inicializaci�n de este form
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TfrmImprentaJORDAN.Inicializar : Boolean;
Resourcestring
    MSG_INIT_ERROR = 'Error al Inicializar';
    MSG_ERROR_LOADING_PARAMETERS    = 'Error cargando los par�metros generales.';
    MSG_ERROR_THE_GENERAL_PARAMETER_DOES_NOT_EXISTS = 'El par�metro DIR_SALIDA_IMPRENTA no existe en la base de daros.';
    MSG_ERROR_USER_AND_PASSWORD_DOES_NOT_EXISTS = 'Los parametros USUARIO_BD_JORDAN y PASSWORD_BD_JORDAN no existen en la base de datos.';
    MSG_ERROR = 'Error';
Const
    HINT_BTN_TODOS = 'Todo los Comprobantes pendientes de impresi�n';
    HINT_BTN_PROC_IMPRESION = 'Seleccionar un Proceso de Impresi�n masiva anterior';
    HINT_BTN_PROC_FACTURACION = 'Seleccionar los Comprobantes de un proceso de Facturaci�n espec�fico';
var
    sUsuarioBDJordan    : String;
    sPasswordBDJordan   : String;
    CarpetaDBNet		: String;
    bUserOK,bPasswordOK  : Boolean;
begin
	Result := False;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

	bUserOK := ObtenerParametroGeneral(DMConnections.BaseCAC, 'USUARIO_BD_JORDAN', sUsuarioBDJordan);
	bPasswordOK := ObtenerParametroGeneral(DMConnections.BaseCAC, 'PASSWORD_BD_JORDAN', sPasswordBDJordan);
	if bUserOK AND bPasswordOK then  begin
    	// Nos conectamos a la misma base de datos del CAC
		  BaseJordan.Connected:=False;
    	BaseJordan.ConnectionString := DMConnections.BaseCAC.ConnectionString;
    	BaseJordan.ConnectionString := AnsiReplaceStr(BaseJordan.ConnectionString,'User ID=usr_op','User ID='+Trim(sUsuarioBDJordan));
    	BaseJordan.ConnectionString := AnsiReplaceStr(BaseJordan.ConnectionString,'Password=usr_op','Password='+Trim(sPasswordBDJordan));
      // se agrega por que en Windows 7 el ConnectionString se obtiene sin el parametro Password                    //SS-330-NDR-20120117
      if Pos('Password',BaseJordan.ConnectionString)=0 then                                                         //SS-330-NDR-20120117
      begin                                                                                                         //SS-330-NDR-20120117
        BaseJordan.ConnectionString := BaseJordan.ConnectionString + ';' + 'Password='+Trim(sPasswordBDJordan);     //SS-330-NDR-20120117
      end;                                                                                                          //SS-330-NDR-20120117
    	BaseJORDAN.Connected:=True;

    	lblCantidad.Caption := '';
    	btnProcesosFacturacion.Hint     := HINT_BTN_PROC_FACTURACION;
    	btnProcesosFacturacion.ShowHint := True;
    	btnBuscarProceso.Hint           := HINT_BTN_PROC_IMPRESION;
    	btnBuscarProceso.ShowHint       := True;
    	btnObtenerComprobantes.Hint     := HINT_BTN_TODOS;
    	btnObtenerComprobantes.ShowHint := True;
    	MSG_SQL_SQL		:= 'SELECT dbo.CONST_IH_INTERFACES_ERROR_SQL()';

		// Rev.9 / 09-07-2009 / Nelson Droguett Sierra -------------------------
    	ObtenerParametroGeneral(DMConnections.BaseCAC, 'DIR_JORDAN_TIMBRE_ELECTRONICO', CarpetaDBNet);
        //Rev.11 / 03-Junio-2010 / Nelson Droguett Sierra -------------------------------
    	//Deltree(ExpandFileName(GoodDir(CarpetaDBNet)+'..'));

      try
          Result := (DMConnections.BaseCAC.Connected) and (OpenTables([tblCatalogoImagenesImprenta]));
          if Result then begin
              nb.PageIndex := 0;
              FNombreImagenCatalogo := '';
              result := ObtenerParametroGeneral(DMConnections.BaseCAC, 'DIR_SALIDA_IMPRENTA', FDestino);
              if not result then begin
                  msgBoxErr(MSG_ERROR_LOADING_PARAMETERS , MSG_ERROR_THE_GENERAL_PARAMETER_DOES_NOT_EXISTS, caption, MB_ICONERROR);
                  exit;
              end;
              if spObtenerComprobantesImprentaPreview.Active then begin
                  spObtenerComprobantesImprentaPreview.Close;
              end;
              // Rev.10 / 06-Noviembre-2009 / Nelson Droguett Sierra ------------------------------------
              txtUbicacion.Text := FDestino;
              // ----------------------------------------------------------------------------------------
              FOnProcess := False;
              FReProceso := False;
              FFechaProcesoAnterior := nulldate;
              FFechaProcFacturacion := '';
              FNumeroProcesoAnterior := 0;
              FNumeroProceso := 0;
              FCantidadFacturados := 0;
              FCantSinImprimir := 0;
              FComprobanteInicial := 0;
              FComprobanteFinal := 0;
              FNoPrnFinal := 0;
              FNoPrnInicial := 0;
              FForzarReimpresion := False;
              FImprimirElectronicos := False;
              neComprobanteInicial.Clear;
              neComprobanteFinal.Clear;
              neComprobanteInicial.Enabled := False;
              neComprobanteFinal.Enabled := False;
              FFechaProceso := NowBase(DMConnections.BaseCAC );
              CrearNombresArchivos(FDestino);
              FDetalleTransitosAparte := False;
              FLogBuffer:= TStringList.Create;
              FLogBuffer.Clear;
          end;
      except
          on E: Exception do begin
              MsgBoxErr(MSG_INIT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
          end;
      end;
    end
    else
    begin
      msgBoxErr(MSG_ERROR_LOADING_PARAMETERS , MSG_ERROR_USER_AND_PASSWORD_DOES_NOT_EXISTS, caption, MB_ICONERROR);
      exit;
    end;
end;

function TfrmImprentaJORDAN.LlenarDetalleConsumoComprobantes:Boolean;
begin
	try
		spLlenarDetalleConsumoComprobantes.Parameters.ParamByName('@NumeroProcesoFacturacion').Value := FNumeroProceso;
		spLlenarDetalleConsumoComprobantes.ExecProc;
        Result := True;
	except
		on e: Exception do begin
            Result := False;
		end;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnSiguienteClick
  Author:    ndonadio
  Date Created: 14/03/2005
  Description:  Paso a la pagina Siguiente
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmImprentaJORDAN.btnSiguienteClick(Sender: TObject);
Resourcestring
   MSG_ERROR_THE_ENTERED_DATA_ARE_INCORRECT = 'Los datos ingresados son incorrectos';
   MSG_ERROR_INITIAL_NUMBER_INCORRECT = 'El N� Inicial debe ser mayor que cero y menor que el N� Final.';
   MSG_ERROR_FINAL_NUMBER_INCORRECT = 'El N� Final debe ser mayor que cero y mayor que el N� Inicial';
Const
   STR_SELECTED_PRINTING_RANGE  = 'Comprobantes seleccionados entre el Nro. %d y el Nro. %d';
   STR_FORCING_REPRINTING = 'Se incluyen comprobantes ya impresos anteriormente.';
   STR_PRINTIG_ELECTRONICS = 'Se incluyen comprobantes marcados para envio electr�nico';
begin
    //Valido los campos antes de pasar de pagina
    if neComprobanteInicial.Enabled then
        if not ValidateControls(  [neComprobanteInicial,neComprobanteFinal],
           [((neComprobanteInicial.ValueInt > 0) AND (neComprobanteInicial.ValueInt <= neComprobanteFinal.ValueInt ) )
           ,((neComprobanteFinal.ValueInt > 0) AND (neComprobanteInicial.ValueInt <= neComprobanteFinal.ValueInt ))],
           MSG_ERROR_THE_ENTERED_DATA_ARE_INCORRECT,[MSG_ERROR_INITIAL_NUMBER_INCORRECT,
           MSG_ERROR_FINAL_NUMBER_INCORRECT]) then Exit;
    nb.PageIndex := nb.PageIndex + 1;
    btnFinalizar.Enabled := True;
    //Seteo las labels que "recuerdan" que se ha seleccionado en la sig. ventana...
    lblRangoSeleccionado.Caption := Format(STR_SELECTED_PRINTING_RANGE, [neComprobanteInicial.ValueInt, neComprobanteFinal.ValueInt]);
    //indica si fuerza o no reimpresion
    if FForzarReimpresion then begin
        lblForzandoReimpresion.Caption := STR_FORCING_REPRINTING;
    end else begin
        lblForzandoReimpresion.Caption := '';
    end;
    //indica si se imprimen electronicos
    if FImprimirElectronicos then begin
        lblImprimiendoElectronicos.Caption := STR_PRINTIG_ELECTRONICS;
    end else begin
        lblImprimiendoElectronicos.Caption := '';
    end;
    //Si el label de Forzar Reimpresion esta vacio, reemplazo por el de Imprimiendo electronicos
    //para que no quede el rengl�n en blanco...
    if ( TRIM(lblForzandoReimpresion.Caption) = '' ) then begin
        lblForzandoReimpresion.Caption := lblImprimiendoElectronicos.Caption;
        lblImprimiendoElectronicos.Caption := '';
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnAnteriorClick
  Author:    lgisuk
  Date Created: 06/12/2005
  Description:  Vuelvo a la pagina Anterior
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmImprentaJORDAN.btnAnteriorClick(Sender: TObject);
begin
    nb.PageIndex := nb.PageIndex - 1;
end;
{-----------------------------------------------------------------------------
  Function Name: btnObtenerComprobantesClick
  Author:    gcasais
  Date Created: 12/01/2005
  Description: Genera la "vista previa" de comprobantes a imprimir
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmImprentaJORDAN.btnObtenerComprobantesClick(Sender: TObject);
resourcestring
    MSG_INVOICE_ERROR = 'Error al obtener los comprobantes';
    MSG_ERROR = 'Error';
const
    STR_CAPTION = 'Proceso masivo de impresi�n';
const
    STR_THEY_HAVE_BEEN_RECORDS_TO_SEND = 'Se han encontrado %d comprobantes para enviar a imprimir (se visualizan hasta los 100 primeros). Presione "Siguiente" para continuar.';
begin
    neComprobanteFinal.Enabled := True;
    neComprobanteInicial.Enabled := True;
    Caption := STR_CAPTION;
    FReProceso := True;
    FNumeroProceso := 0;
    FNumeroProcesoAnterior := 0;
    FForzarReimpresion := False;
    FImprimirElectronicos := False;
    try
        with spObtenerComprobantesImprentaPreview do begin
            Close;
            Parameters.ParamByName('@CodigoOperacion').Value := NULL;
            Parameters.ParamByName('@NumeroProcesoFacturacion').Value := NULL;
            CommandTimeOut := 5000;
            Open;
        end;
        FComprobantesAfectados := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT COUNT (*) FROM COMPROBANTES WITH (NOLOCK) '+ ' WHERE ( FECHAHORAIMPRESO IS NULL)  AND ( TIPOCOMPROBANTE = ' + QuotedStr('NK') + ' ) '+ ' AND ( ENVIOELECTRONICO = 0 ) AND ( ESTADOPAGO <> ''A'') '  );
        FComprobanteInicial := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT MIN (NumeroComprobante) FROM COMPROBANTES WITH (NOLOCK) '+ ' WHERE ( FECHAHORAIMPRESO IS NULL)  AND ( TIPOCOMPROBANTE = ' + QuotedStr('NK') + ' ) '+ ' AND ( ENVIOELECTRONICO = 0 ) AND ( ESTADOPAGO <> ''A'') '  );
        neComprobanteInicial.ValueInt := FComprobanteInicial;
        FComprobanteFinal := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT MAX (NumeroComprobante) FROM COMPROBANTES WITH (NOLOCK) '+ ' WHERE ( FECHAHORAIMPRESO IS NULL)  AND ( TIPOCOMPROBANTE = ' + QuotedStr('NK') + ' ) '+ ' AND ( ENVIOELECTRONICO = 0 ) AND ( ESTADOPAGO <> ''A'') '  );
        neComprobanteFinal.ValueInt := FComprobanteFinal;
        btnSiguiente.Enabled := (FComprobantesAfectados > 0);
        lblCantidad.Caption := Format(STR_THEY_HAVE_BEEN_RECORDS_TO_SEND ,[FComprobantesAfectados]);
    except
        on E: Exception do begin
            MsgBoxErr(MSG_INVOICE_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
        end;
    end
end;

{-----------------------------------------------------------------------------
  Function Name: btCatalogoProcess
  Author:    gcasais
  Date Created: 24/12/2004
  Description: Dibujamos la tabla de b�squeda
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TfrmImprentaJORDAN.btCatalogoProcess(Tabla: TDataSet;var Texto: String): Boolean;
begin
    Texto  := Tabla.FieldByName('Nombre').AsString + Space(10) + '(' + Tabla.FieldByName('Descripcion').AsString + ')';
    Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: btCatalogoSelect
  Author:    gcasais
  Date Created: 24/12/2004
  Description: Cargamos el nombre de la imagen que se seleccion�
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmImprentaJORDAN.btCatalogoSelect(Sender: TObject; Tabla: TDataSet);
begin
    FNombreImagenCatalogo := Trim(Tabla.FieldByName('Nombre').AsString);
    bteImagen.Text := FNombreImagenCatalogo;
end;

{-----------------------------------------------------------------------------
  Function Name: btnBuscarProcesoClick
  Author:    ndonadio
  Date Created: 02/05/2005
  Description: Setea todo para reprocesar un preceso de facturacion anterior
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmImprentaJORDAN.btnBuscarProcesoClick(Sender: TObject);
resourcestring
    MSG_INVOICE_ERROR = 'Error al obtener los comprobantes';
    MSG_ERROR = 'Error';
const
    STR_CAPTION = 'Proceso masivo de impresi�n';
const
    STR_THEY_HAVE_BEEN_RECORDS_TO_SEND_TO_PRINT = 'Se han encontrado %d comprobantes para enviar a imprimir (se visualizan hasta los 100 primeros). Presione "Siguiente" para continuar.';
    STR_REPROCESS_OPERATION = ' - Reprocesando operaci�n N� %d del %s';
begin
    //Obtengo las operaciones de impresion masiva
    with spObtenerOperacionesImpresionMasiva do begin
        CommandTimeOut := 5000;
       	Open;
    end;
    FForzarReimpresion := False;
    FImprimirElectronicos := False;
    FNumeroProcesoAnterior := 0;
    FNumeroProceso := 0;
    buscaOperaciones.Activate ;
    if FNumeroProcesoAnterior > 0 then  begin
        try
            //Obtengo el preview
            with spObtenerComprobantesImprentaPreview, Parameters do begin
                Close;
                ParamByName('@CodigoOperacion').Value := FNumeroProcesoAnterior;
                ParamByName('@NumeroProcesoFacturacion').Value := NULL;
                CommandTimeOut := 5000;
                Open;
            end;
            FComprobantesAfectados := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT COUNT (*) FROM DetalleComprobantesEnviados WITH (NOLOCK) WHERE  DetalleComprobantesEnviados.CodigoOperacionInterfase = ' + IntToStr(FNumeroProcesoAnterior));
            FComprobanteInicial := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT MIN (NumeroComprobante) FROM DetalleComprobantesEnviados WITH (NOLOCK) WHERE  DetalleComprobantesEnviados.CodigoOperacionInterfase = ' + IntToStr(FNumeroProcesoAnterior));
            neComprobanteInicial.ValueInt := FComprobanteInicial;
            FComprobanteFinal := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT MAX (NumeroComprobante) FROM DetalleComprobantesEnviados WITH (NOLOCK) WHERE  DetalleComprobantesEnviados.CodigoOperacionInterfase = ' + IntToStr(FNumeroProcesoAnterior));
            neComprobanteFinal.ValueInt := FComprobanteFinal;
            btnSiguiente.Enabled := (FComprobantesAfectados > 0);
            lblCantidad.Caption := Format(STR_THEY_HAVE_BEEN_RECORDS_TO_SEND_TO_PRINT,[FComprobantesAfectados]);
            neComprobanteFinal.Enabled := False;
            neComprobanteInicial.Enabled := False;
            // cargo la fecha del proceso anterior
            FFechaProcesoAnterior := QueryGetValueDateTime( DMConnections.BaseCAC, Format('SELECT Fecha FROM LogOperacionesInterfases (NOLOCK) WHERE CodigoOperacionInterfase = %d',[FNumeroProcesoAnterior]));
            FReProceso := True;
            Caption := STR_CAPTION + Format(STR_REPROCESS_OPERATION,[FNUmeroProcesoAnterior, FormatDateTime('dd/mm/yyyy', FFechaProcesoAnterior)]);
        except
            on E: Exception do begin
                MsgBoxErr(MSG_INVOICE_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
                neComprobanteFinal.Enabled := True;
                neComprobanteInicial.Enabled := True;
                FReProceso := False;
            end;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: buscaOperacionesProcess
  Author:    lgisuk
  Date Created: 06/12/2005
  Description:  veo los procesos realizados
  Parameters:  None
  Return Value: None
-----------------------------------------------------------------------------}
function TfrmImprentaJORDAN.buscaOperacionesProcess(Tabla: TDataSet;var Texto: String): Boolean;
const
    STR_PROCESS = 'Fecha Realizaci�n: %s  -  Usuario: %s - %d Comprobantes Procesados';
begin
	  with Tabla do begin
      	Texto := Format(STR_PROCESS, [FieldByName('Fecha').AsString, FieldByName('Usuario').AsString, FieldByName('Cantidad').asInteger]);
    end;
    Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: buscaOperacionesSelect
  Author:    lgisuk
  Date Created: 06/12/2005
  Description:  selecciono un proceso
  Parameters:  None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmImprentaJORDAN.buscaOperacionesSelect(Sender: TObject; Tabla: TDataSet);
begin
    with Tabla do begin
        FNumeroProcesoAnterior := FieldByName('CodigoOperacionInterfase').AsInteger;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnProcesosFacturacionClick
  Author:    lgisuk
  Date Created: 06/12/2005
  Description:  
  Parameters:  None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmImprentaJORDAN.btnProcesosFacturacionClick(Sender: TObject);
resourcestring
    MSG_INVOICE_ERROR = 'Error al obtener los comprobantes';
const
    STR_DOING_PROC_OF_INVOINCIG = ' - Procesando Proc. de Facturaci�n N� %d del %s';
    STR_CAPTION = 'Proceso masivo de impresi�n';
var
    f: TdlgSeleccionProcFacturacion;
begin
    Application.CreateForm(TdlgSeleccionProcFacturacion, f);
    if not f.Inicializar then exit;
    if ( f.ShowModal <> mrOK ) then begin
        f.Release;
        Exit;
    end;
    FNumeroProcesoAnterior := 0;
    FNumeroProceso:=  f.FNumeroProceso;
    FCantSinImprimir := f.FCantNoPrn;
    FComprobantesAfectados := f.FSelTotal;
    FCantidadFacturados := f.FCantTotal;
    FComprobanteInicial := f.FSelFirst;
    FComprobanteFinal := f.FSelLast;
    FNoPrnFinal     := f.FMaxNoPrn;
    FNoPrnInicial   := f.FMinNoPrn ;
    FFechaProcFacturacion := f.FFechaProceso;
    neComprobanteFinal.Enabled := True;
    neComprobanteInicial.Enabled := True;
    FForzarReimpresion := not f.chkSoloPendientes.Checked;
    FImprimirElectronicos := f.chkImprimirElectronicas.Checked;
    FPorFacturacion := True;
    f.Release;
    Caption := STR_CAPTION + Format(STR_DOING_PROC_OF_INVOINCIG,[FNumeroProceso, FFechaProcFacturacion]);
    CargarComprobantesPorFacturacion;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarComprobantesPorFacturacion
  Author:    lgisuk
  Date Created: 06/12/2005
  Description:
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmImprentaJORDAN.CargarComprobantesPorFacturacion;
resourcestring
    MSG_INVOICE_ERROR = 'Error al obtener los comprobantes';
    MSG_ERROR = 'Error';
const
    STR_THEY_HAVE_FOUND_VOUCHERS_FOR_THE_PROCESS_OF_INVOICING = 'Se han encontrado %d comprobantes pertenecientes al Proceso de Facturacion N� %d del d�a %s (se visualizan hasta los 100 primeros). Presione "Siguiente" para continuar.';
begin
    try
        with spObtenerComprobantesImprentaPreview, Parameters do begin
            Close;
            ParamByName('@CodigoOperacion').Value := NULL;
            ParamByName('@NumeroProcesoFacturacion').Value := FNumeroProceso;
            ParamByName('@ForzarReimpresion').Value := IIF(FForzarReimpresion = True, 1, 0); //Forzar Reimpresion
            ParamByName('@ImprimirElectronicos').Value := IIF(FImprimirElectronicos = True, 1, 0); //Imprimir Electronicos
            CommandTimeOut := 5000;
            Open;
        end;
    except
        on E: Exception do begin
            MsgBoxErr(MSG_INVOICE_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
            neComprobanteFinal.Enabled := True;
            neComprobanteInicial.Enabled := True;
            FPorFacturacion := False;
            Exit;
        end;
    end;
    lblCantidad.Caption := Format(STR_THEY_HAVE_FOUND_VOUCHERS_FOR_THE_PROCESS_OF_INVOICING, [FComprobantesAfectados, FNumeroProceso, FFechaProcFacturacion]);
    neComprobanteInicial.ValueInt := FComprobanteInicial;
    neComprobanteFinal.ValueInt := FComprobanteFinal;
    btnSiguiente.Enabled := (FComprobantesAfectados > 0);
end;

{******************************** Function Header ******************************
  Function Name: btnBrowseForFolderClick
  Author:    gcasais
  Date Created: 06/12/2004
  Description: Seleccionar la ubicaci�n para depositar el archivo creado
  Parameters: None
  Return Value: None
********************************************************************************}
procedure TfrmImprentaJORDAN.txtUbicacionButtonClick(Sender: TObject);
const
    STR_SELECT_LOCATION = 'Seleccione una ubicaci�n para el archivo';
var
    Location: String;
begin
    Location := Trim(BrowseForFolder(STR_SELECT_LOCATION));
    if Location = '' then exit;
    FNombreArchivoSimple := '';
    txtUbicacion.Text := Location;
    FDestino := GoodDir(txtUbicacion.Text);
    CrearNombresArchivos(FDestino);
    btnFinalizar.Enabled := True;
end;

{-----------------------------------------------------------------------------
  Function Name: CrearNombresArchivos
  Author:    lgisuk
  Date Created: 06/12/2005
  Description: Crea el nombre de los archivos
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmImprentaJORDAN.CrearNombresArchivos(Path: String);
const
    CONTROL_FILE_PREFIX = 'Control-';
    TYPE_VOUCHER  = 'NC';
    FILE_PREFIX = TYPE_VOUCHER;
    DATE_FORMAT = 'ddmmyyyy';
    FILE_EXTENSION = '.txt';
    TYPE_SIMPLE = 'S';
    TYPE_MULTIPLE = 'M';
    // Rev.4 / 11-03-2009 / Nelson Droguett Sierra.
    FILE_PREFIXFAC = 'FA';
    FILE_PREFIXBOL = 'BO';
    //---------------------------------------------------------------------
begin
    //FNombreArchivoControlSimple := GoodDir(Path) + CONTROL_FILE_PREFIX + FormatDateTime(DATE_FORMAT, FFechaProceso) + TYPE_SIMPLE + FILE_EXTENSION;
    FNombreArchivoSimple := GoodDir(Path) + FILE_PREFIX + FormatDateTime(DATE_FORMAT, FFechaProceso) + TYPE_SIMPLE + FILE_EXTENSION;
    //FNombreArchivoControlMultiple := GoodDir(Path) + CONTROL_FILE_PREFIX + FormatDateTime(DATE_FORMAT, FFechaProceso) + TYPE_MULTIPLE + FILE_EXTENSION;
    //FNombreArchivoMultiple := GoodDir(Path) + FILE_PREFIX + FormatDateTime(DATE_FORMAT, FFechaProceso) + TYPE_MULTIPLE + FILE_EXTENSION;
    //txtUbicacion.Text := FNombreArchivoSimple;
    // Rev.4 / 11-03-2009 / Nelson Droguett Sierra ---------------------------

    FNombreArchivoControlSimpleFAC := GoodDir(Path) + CONTROL_FILE_PREFIX + FILE_PREFIXFAC + FormatDateTime(DATE_FORMAT, FFechaProceso) + TYPE_SIMPLE + FILE_EXTENSION;
    FNombreArchivoSimpleFAC := GoodDir(Path) + FILE_PREFIXFAC  + FormatDateTime(DATE_FORMAT, FFechaProceso) + TYPE_SIMPLE + FILE_EXTENSION;

    FNombreArchivoControlMultipleFAC := GoodDir(Path) + CONTROL_FILE_PREFIX + FILE_PREFIXFAC + FormatDateTime(DATE_FORMAT, FFechaProceso) + TYPE_MULTIPLE + FILE_EXTENSION;
    FNombreArchivoMultipleFAC := GoodDir(Path) + FILE_PREFIXFAC  + FormatDateTime(DATE_FORMAT, FFechaProceso) + TYPE_MULTIPLE + FILE_EXTENSION;

    FNombreArchivoControlSimpleBOL := GoodDir(Path) + CONTROL_FILE_PREFIX + FILE_PREFIXBOL  + FormatDateTime(DATE_FORMAT, FFechaProceso) + TYPE_SIMPLE + FILE_EXTENSION;
    FNombreArchivoSimpleBOL := GoodDir(Path) + FILE_PREFIXBOL + FormatDateTime(DATE_FORMAT, FFechaProceso) + TYPE_SIMPLE + FILE_EXTENSION;

    FNombreArchivoControlMultipleBOL := GoodDir(Path) + CONTROL_FILE_PREFIX  + FILE_PREFIXBOL + FormatDateTime(DATE_FORMAT, FFechaProceso) + TYPE_MULTIPLE + FILE_EXTENSION;
    FNombreArchivoMultipleBOL := GoodDir(Path) + FILE_PREFIXBOL + FormatDateTime(DATE_FORMAT, FFechaProceso) + TYPE_MULTIPLE + FILE_EXTENSION;

    //txtUbicacion.Text := FNombreArchivoSimple;
    // Rev.10 / 10-Noviembre-2009 / Nelson Droguett Sierra -------------------
    txtUbicacion.Text := GoodDir(Path); 
    // -----------------------------------------------------------------------
end;


{-----------------------------------------------------------------------------
  Function Name: CrearArchivo
  Author:    gcasais
  Date Created: 21/12/2004
  Description: Creaci�n del Archivo de interface
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TfrmImprentaJORDAN.CrearArchivo(Archivo: String; var DescriError: AnsiString; var FExist: boolean):Boolean;
Resourcestring
    MSG_ERROR_PROCESS_CANCELLED = 'Se ha cancelado el proceso debido a la existencia de un archivo id�ntico en el directorio';
    MSG_ERROR_OF_DESTINY = 'de destino';
const
    STR_THE_FILE = 'El archivo ';
    STR_ALREADY_EXISTS = ' ya existe en el directorio de destino. �Desea reemplazarlo?';
    STR_ATTENTION = 'Atenci�n';
begin
    Result := False;
    //Verificamos que no exista uno con el mismo nombre
    if  FileExists(Archivo) and (MsgBox(STR_THE_FILE + Archivo + STR_ALREADY_EXISTS, STR_ATTENTION, MB_YESNO + MB_ICONQUESTION) = IDNO) then begin
        DescriError := MSG_ERROR_PROCESS_CANCELLED + CRLF + MSG_ERROR_OF_DESTINY;
        //devuelvo FExists en True para que no informe de Error...
        FExist := True;
        Exit;
    end;
    // Listo, intentamos crearlo
    try
        AssignFile(FOutFileFAC, Trim(Archivo));
        Rewrite(FOutFileFAC);
        CloseFile(FOutFileFAC);
        // Rev.4 / 11-03-2009 / Nelson Droguett Sierra
        Erase(FOutFileFAC);
        Result := True;
    except
        on e : Exception do begin
            DescriError := e.Message;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: HabilitarControles
  Author:    lgisuk
  Date Created: 06/12/2005
  Description: habilita los controles
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmImprentaJORDAN.HabilitarControles(Habilitado: Boolean);
begin
    // Rev.10 / 10-Noviembre-2009 / Nelson Droguett Sierra--------------------
    //txtUbicacion.Enabled := Habilitado;
    bteImagen.Enabled := Habilitado;
end;

{-----------------------------------------------------------------------------
  Function Name: RegistrarOperacion
  Author:    flamas
  Date Created: 18/01/2005
  Description: Actualiza el LogOperacionesInterface
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TfrmImprentaJORDAN.RegistrarOperacion: boolean;
resourcestring
  	MSG_COULD_NOT_REGISTER_OPERATION = 'No se pudo registrar la operaci�n';
    MSG_ERROR = 'Error';
var
    DescError : string;
begin
    Result := RegistrarOperacionEnLogInterface( DMConnections.BaseCAC, RO_MOD_INTERFAZ_SALIENTE_IMPRESION, ExtractFileName(FNombreArchivoSimple), UsuarioSistema, '', True, False, FFechaProceso, 0, FCodigoOperacion, DescError);
    if not Result then MsgBoxErr(MSG_COULD_NOT_REGISTER_OPERATION, DescError, MSG_ERROR, MB_ICONERROR);
end;


{-----------------------------------------------------------------------------
  Function Name: ActualizarComprobante
  Author:    gcasais
  Date Created: 13/01/2005
  Description: Actualiza un comprobante como impreso  (fechaHoraImpreso)
  Parameters: None
  Return Value: Boolean

  Revision : 1
      Author : ggomez
      Date : 06/12/2006
      Description : Quit� el uso de la transacci�n pues esta hace que se bloque�
        la tabla Comprobantes. Optamos por quitarla pues es poco probable de que
        falle esta actualizaci�n.
        Asi mismo agregu� que en caso de error, deje un registro en la tabla
        ErroresInterfaces y si este registro no se realiza con exito, entonces
        deja el registro en un archivo que est� en el mismo
        lugar donde se encuentra el ejecutable de la app, en la que se indica
        que fall� un determinado proceso. Esto lo hice porque si ocurre un error
        que adem�s de las pantallas de error que el operador capture, queden m�s
        datos a�n.
-----------------------------------------------------------------------------}
function TfrmImprentaJORDAN.ActualizarComprobantes: Boolean;
resourcestring
    MSG_ERROR_UPDATING_NK = 'Error al actualizar un comprobante';
    MSG_ERROR = 'Impresi�n Comprobantes. Error actualizando Fecha Impresi�n de Comprobante. C�d. Op. Interface: %d. Fecha para actualizar: %s. �ltimo N� Comprobante actualizado: %d.';
const
    FILE_NAME_ERRORS = '.\Errors.txt';
var
    NumeroComp: int64;
    CodigoErrorSQL	:integer;
begin
    NumeroComp := 0;
    try
        //DMConnections.BaseCAC.BeginTrans;                                     //SS_1385_NDR_20150922
        DMConnections.BaseCAC.Execute('BEGIN TRAN ImprentaJORDAN');               //SS_1385_NDR_20150922

        with JORDAN_ActualizarFechaHoraImpresion, Parameters do begin
            Close;
            ParamByName('@FechaHoraImpreso').Value  := FFechaProceso;
            ParamByName('@NumeroComprobante').Value := 0;
            ParamByName('@NumeroProcesoFacturacion').Value := FNumeroProceso;
            //revision 1
            ParamByName('@Usuario').Value := UsuarioSistema;
            CommandTimeOut := 5000;
            while  NumeroComp >= 0 do begin;
                ExecProc;
                NumeroComp := ParamByName('@NumeroComprobante').Value;
            end;
        end;
        //DMConnections.BaseCAC.CommitTrans;                                                                //SS_1385_NDR_20150922
        DMConnections.BaseCAC.Execute('COMMIT TRAN ImprentaJORDAN');					              //SS_1385_NDR_20150922
        Result := True;
    except
        on E: Exception do begin
            //DMConnections.BaseCAC.RollbackTrans;                              //SS_1385_NDR_20150922
            DMConnections.BaseCAC.Execute('IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION ImprentaJORDAN END');	    //SS_1385_NDR_20150922
            //if not AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacion,
            //        Format(MSG_ERROR,
            //            [FCodigoOperacion,
            //            FormatDateTime('dd/mm/yyyy hh:mm:ss', FFechaProceso),
            //            NumeroComp])) then begin
            CodigoErrorSQL				:= QueryGetValueInt(DMConnections.BaseCAC, MSG_SQL_SQL);
            if AgregarEnErroresInterfaces( DMConnections.BaseCAC,FCodigoOperacion,CodigoErrorSQL,E.Message) then
                EscribirArchivoLog(FILE_NAME_ERRORS,
                    Format(MSG_ERROR,
                        [FCodigoOperacion,
                        FormatDateTime('dd/mm/yyyy hh:mm:ss', FFechaProceso),
                        NumeroComp]));


            raise Exception.Create( MSG_ERROR_UPDATING_NK + CRLF + E.Message);
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ActualizarComprobantesEnviados
  Author:    flamas
  Date Created: 18/01/2005
  Description: Actualiza el Detalle de Comprobantes enviados por la interfaz
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TfrmImprentaJORDAN.ActualizarComprobantesEnviados : Boolean;
resourcestring
    MSG_ERROR_UPDATING_SENT_INVOICES = 'Error actualizando facturas enviadas';
    MSG_ERROR = 'Error';
begin
  	try
    	with spActualizarComprobantesEnviadosAImprenta, Parameters do begin
          	ParamByName( '@CodigoOperacionInterfase' ).Value := FCodigoOperacion;
      			ParamByName( '@FechaHoraImpreso' ).Value := FFechaProceso;
            ParamByName( '@Cantidad' ).Value := 1;
            CommandTimeOut := 5000;
            while ParamByName( '@Cantidad' ).Value > 0 do begin
                ExecProc;
            end;
            Result := True;
        end;
    except
        on E : Exception do begin
              Result := False;
              MsgBoxErr(MSG_ERROR_UPDATING_SENT_INVOICES, e.Message, MSG_ERROR, MB_ICONERROR);
        end;
    end;
end;


function TfrmImprentaJORDAN.EscribeLog(sMensaje: String): Boolean;
begin
//  if chbLogSucesos.Checked then
//  begin
//     FLogBuffer.Add(FormatDateTime('yyyy/mm/dd HH:nn:ss:zzz',Now)+'   '+sMensaje);
//  end;
//  Result := True;
end;


{******************************** Function Header ******************************
Function Name: EscribirArchivoLog
Author : ggomez
Date Created : 06/12/2006
Description : Dado un nombre de archivo escribe en �l el texto pasado como par�metro.
    Si ocurre un error al escribir en el archivo, genera un evento de windows
    (Visor de sucesos de windows).
Parameters : ArchLog, Texto: AnsiString
Return Value : None
*******************************************************************************}
procedure TfrmImprentaJORDAN.EscribirArchivoLog(ArchLog, Texto: AnsiString);
resourcestring
    MSG_FECHA = 'Fecha Log: %s. %s.';
    MSG_ERROR_REGISTRAR_LOG = 'Error registrar log: %s. Texto: %s. Error: %s.';
var
    f: TextFile;
begin
    try
        MoveToMyOwnDir;
        AssignFile(F, ArchLog);
        try
            // Si el archivo no existe, entonces crearlo.
            if not FileExists(ArchLog) then begin
                ReWrite(F);
            end else begin
                // El archivo existe, entonces abrirlo.
                Append(F);
            end;
            // Escribir en el archivo la informaci�n
            Writeln(F, Format(MSG_FECHA,
                [FormatDateTime('yyyymmdd hh:mm:ss', Now), Texto]));
        finally
            CloseFile(F);
        end;
    except
        on E: Exception do begin
            EventLogReportEvent(elError, Format(MSG_ERROR_REGISTRAR_LOG,
                [Archlog, Texto, E.Message]), '');
        end;
    end; // except
end;


{-----------------------------------------------------------------------------
  Function Name: CerrarOperacion
  Author:    lgisuk
  Date Created: 06/12/2005
  Description: Actualizo el log de operaciones al final
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
Function TfrmImprentaJORDAN.CerrarOperacion: boolean;
Resourcestring
    MSG_ERROR = 'No se pudo Actualizar el log de operaciones';
Const
    STR_OBSERVATION = 'Finalizo OK!';
var
    DescError : String;
begin
    Result := ActualizarLogOperacionesInterfaseAlFinal(DMConnections.BaseCAC, FCodigoOperacion, STR_OBSERVATION , DescError);
    if not Result then begin
        MsgBoxErr(MSG_ERROR, DescError, Self.caption, MB_ICONERROR);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ObtenerCantidadComprobantesAImprimir
  Author:    lgisuk
  Date Created: 22/12/2005
  Description: Obtengo la cantidad de comprobantes a imprimir
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
Function TfrmImprentaJORDAN.ObtenerCantidadComprobantesAImprimir : int64;
begin
    Result := QueryGetValueInt(DMConnections.BaseCAC, ' SELECT COUNT(NumeroComprobante) FROM JORDANComprobantesAImprimir WITH (NOLOCK) ' + ' WHERE FechaHoraImpreso IS NULL ');
end;


// Rev.4 / 18-03-2009 / Nelson Droguett Sierra
function TfrmImprentaJORDAN.ObtenerCantidadComprobantesAImprimirBOLSimple: int64;
begin
    Result := QueryGetValueInt(DMConnections.BaseCAC, ' SELECT COUNT(NumeroComprobante) FROM JORDANComprobantesAImprimir WITH (NOLOCK) ' + ' WHERE (FechaHoraImpreso IS NULL) AND TipoComprobanteFiscal IN ('+''''+TC_BOLETA+''''+','+''''+TC_BOLETA_AFECTA+''''+','+''''+TC_BOLETA_EXENTA+''''+','+''''+'BM'+''''+ ') AND SoloNK=1');
end;



// Rev.4 / 18-03-2009 / Nelson Droguett Sierra
function TfrmImprentaJORDAN.ObtenerCantidadComprobantesAImprimirFACSimple: int64;
begin
    Result := QueryGetValueInt(DMConnections.BaseCAC, ' SELECT COUNT(NumeroComprobante) FROM JORDANComprobantesAImprimir WITH (NOLOCK) ' + ' WHERE (FechaHoraImpreso IS NULL) AND  TipoComprobanteFiscal IN ('+''''+TC_FACTURA+''''+','+''''+TC_FACTURA_AFECTA+''''+','+''''+TC_FACTURA_EXENTA+''''+','+''''+'FM'+''''+ ') AND SoloNK=1');
end;

                                                                   // Rev.4 / 18-03-2009 / Nelson Droguett Sierra
function TfrmImprentaJORDAN.ObtenerCantidadComprobantesAImprimirBOLMultiple: int64;
begin
    Result := QueryGetValueInt(DMConnections.BaseCAC, ' SELECT COUNT(NumeroComprobante) FROM JORDANComprobantesAImprimir WITH (NOLOCK) ' + ' WHERE (FechaHoraImpreso IS NULL) AND TipoComprobanteFiscal IN ('+''''+TC_BOLETA+''''+','+''''+TC_BOLETA_AFECTA+''''+','+''''+TC_BOLETA_EXENTA+''''+','+''''+'BM'+''''+ ') AND SoloNK=0');
end;



// Rev.4 / 18-03-2009 / Nelson Droguett Sierra
function TfrmImprentaJORDAN.ObtenerCantidadComprobantesAImprimirFACMultiple: int64;
begin
    Result := QueryGetValueInt(DMConnections.BaseCAC, ' SELECT COUNT(NumeroComprobante) FROM JORDANComprobantesAImprimir WITH (NOLOCK) ' + ' WHERE (FechaHoraImpreso IS NULL) AND  TipoComprobanteFiscal IN ('+''''+TC_FACTURA+''''+','+''''+TC_FACTURA_AFECTA+''''+','+''''+TC_FACTURA_EXENTA+''''+','+''''+'FM'+''''+ ') AND SoloNK=0');
end;


function TfrmImprentaJORDAN.ObtenerComprobantesPendientesSimples: int64;
begin
    Result := QueryGetValueInt(DMConnections.BaseCAC, ' SELECT COUNT(NumeroComprobante) FROM JORDANComprobantesAImprimir WITH (NOLOCK) ' + ' WHERE (FechaHoraImpreso IS NULL) AND SoloNK=1');
end;



// Rev.4 / 18-03-2009 / Nelson Droguett Sierra
function TfrmImprentaJORDAN.ObtenerComprobantesPendientesMultiples: int64;
begin
    Result := QueryGetValueInt(DMConnections.BaseCAC, ' SELECT COUNT(NumeroComprobante) FROM JORDANComprobantesAImprimir WITH (NOLOCK) ' + ' WHERE (FechaHoraImpreso IS NULL) AND SoloNK=0');
end;



// Rev.4 / 18-03-2009 / Nelson Droguett Sierra
// Obtiene el tipo de documento electronico a partir del TipoComprobante
function TfrmImprentaJORDAN.ObtieneTipoDocumento(sTipoComprobanteFiscal:String): integer;
begin
  if ((sTipoComprobanteFiscal = TC_FACTURA_AFECTA) OR  (sTipoComprobanteFiscal = TC_FACTURA)) then
     Result:=33
  else if sTipoComprobanteFiscal = TC_FACTURA_EXENTA  then
     Result:=34
  else if ( (sTipoComprobanteFiscal = TC_BOLETA) OR
  (sTipoComprobanteFiscal = TC_BOLETA_AFECTA) ) then
     Result:=39
  else if sTipoComprobanteFiscal = TC_BOLETA_EXENTA  then
     Result:=41

end;

//----------------------------------------------------------------------------





{-----------------------------------------------------------------------------
  Function Name: btnFinalizarClick
  Author:    gcasais
  Date Created: 21/12/2004
  Description: Ejecuci�n del proceso
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmImprentaJORDAN.btnFinalizarClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: ObtenerMensajeFinalizacionDeProceso
      Author:    lgisuk
      Date Created: 23/01/2006
      Description: Obtengo el mensaje para mostrar al finalizar el proceso
      Parameters: None
      Return Value: None
    -----------------------------------------------------------------------------}
    Function ObtenerMensajeFinalizacionDeProceso : String;
    Resourcestring
        MSG_PROCESS_FINISHED  = 'El proceso finaliz� correctamente!';
    Const
        STR_SIMPLE_FILE = 'Archivo Simple : ';
        STR_MULTIPLE_FILE = 'Archivo M�ltiple : ';
        STR_GENERATED =  'Generado.';
        STR_NOT_GENERATED = 'No Generado.';
        STR_THERE_ARE_NO_SIMPLE_COLLECTION_NOTES_TO_SEND_IN_THIS_PROCESES = 'No hay notas de cobro simples' + CRLF + 'para enviar en este proceso.';
        STR_THERE_ARE_NO_COLLECTION_NOTES_OF_MULTIPLE_PAGES_TO_SEND_IN_THIS_PROCESES = 'No hay notas de cobro de m�ltiples p�ginas' + CRLF + 'para enviar en este proceso.';
    begin
        Result := MSG_PROCESS_FINISHED + CRLF +
                  CRLF +
                  STR_SIMPLE_FILE + IIF(FHayDatosArchivoSimple = True, STR_GENERATED, STR_NOT_GENERATED + CRLF + STR_THERE_ARE_NO_SIMPLE_COLLECTION_NOTES_TO_SEND_IN_THIS_PROCESES + CRLF) + CRLF +
                  STR_MULTIPLE_FILE + IIF(FHayDatosArchivoMultiple = True, STR_GENERATED, STR_NOT_GENERATED + CRLF + STR_THERE_ARE_NO_COLLECTION_NOTES_OF_MULTIPLE_PAGES_TO_SEND_IN_THIS_PROCESES) + CRLF;
    end;

resourcestring
    MSG_ERROR_CANNOT_CREATEFILE = 'Error al crear el archivo';
    MSG_ERROR = 'Error';
    //
    MSG_PROCESS_CANCELLED = 'Proceso cancelado';
var
    Error: String;
    FileAlreadyExist, bExito: boolean;
begin


    EscribeLog('Inicio del Proceso');
    FileAlreadyExist := False;



    //Deshabilito los botones
    btnAnterior.Enabled := False;
    btnFinalizar.Enabled := False;
    btnSalir.Enabled    := False;
    HabilitarControles(False);

    //Obtener la fecha del proceso
    FFechaProceso := NowBase(DMConnections.BaseCAC);

    // Facturacion Electronica
    // Rev.5 / 19-03-2009 / Nelson Droguett Sierra
    AConfig_EGATE_HOME_Parametrico.Execute;
    //ACargarDLL.Execute;
    //--------------------------------------------------------

    nb.PageIndex:=nb.PageIndex+1;
    Application.ProcessMessages;
    bExito:=True;
    if RegistrarOperacion then
       if LlenarDetalleConsumoComprobantes AND Not(FCancel) then
       begin
         if PrepararProcesoInterfazImprenta AND Not(FCancel) then
         begin
            if ObtenerLineasInterfazImprenta AND Not(FCancel) then
            begin
              if TimbrarComprobantesElectronicos AND Not(FCancel) then
              begin
                  if GenerarArchivosDatos AND Not(FCancel) then
                  begin
                      if ActualizarComprobantes then
                      begin
                        if CerrarOperacion AND Not(FCancel) then
                        begin
                            MsgBox(ObtenerMensajeFinalizacionDeProceso);
                        end
                        else
                        begin
                          ShowMessage('Error al cerrar la operacion');
                          bExito:=False;
                        end;
                      end
                      else
                      begin
                        ShowMessage('Error al actualizar la fecha de impresion de los comprobantes');
                        bExito:=False;
                      end;
                  end
                  else
                  begin
                    ShowMessage('Error al generar los archivos de datos');
                    bExito:=False;
                  end
              end
              else
              begin
                ShowMessage('Error en la generaci�n de timbre electronico'+CR+
                              'Comprobante : ' +
                              DBGJordanObtenerComprobantesATimbrar.DataSource.DataSet.FieldByName('TipoComprobante').AsString + ' ' +
                              DBGJordanObtenerComprobantesATimbrar.DataSource.DataSet.FieldByName('NumeroComprobante').AsString);
                bExito:=False;
              end
            end
            else
            begin
              ShowMessage('Error en la generacion de lineas de comprobantes');
              bExito:=False;
            end;
         end
         else
         begin
           ShowMessage('Error en la seleccion de los comprobantes a imprimir');
           bExito:=False
         end
       end
       else
       begin
         ShowMessage('Error Llenando los Detalles de Consumo de Comprobantes');
         bExito:=False
       end
    else
    begin
        ShowMessage('Error al registrar la operacion');
        bExito:=False;
    end;

    if Not bExito then
    begin
        if FCancel then begin
            //Registro que el proceso fue cancelado
            RegistrarCancelacionInterface(DMConnections.BaseCAC, FCodigoOperacion,Error);
            //Muestro cartel informando que el proceso fue cancelado
            MsgBox(MSG_PROCESS_CANCELLED);
        end;
    end;
    //Borro los mensajes
    lblRangoSeleccionado.Caption := '';
    lblForzandoReimpresion.Caption := '';
    lblImprimiendoElectronicos.Caption := '';
    //Habilito botones
    btnAnterior.Enabled := True;
    btnSalir.Enabled := True;
    HabilitarControles(True);
    //Vuelvo a la primea pagina para iniciar un nuevo proceso
    qryJordanAdvertencia.Close;                                                                             //SS-954-NDR-20110907
    qryJordanAdvertencia.Parameters.ParamByName('NumeroProcesoFacturacion').Value:=FNumeroProceso;          //SS-954-NDR-20110907
    qryJordanAdvertencia.Prepared:=True;                                                                    //SS-954-NDR-20110907
    qryJordanAdvertencia.Open;                                                                              //SS-954-NDR-20110907
    if (Not qryJordanAdvertencia.IsEmpty) then                                                              //SS-954-NDR-20110907
      nb.PageIndex := 3                                                                                     //SS-954-NDR-20110907
    else                                                                                                    //SS-954-NDR-20110907
      nb.PageIndex := 0;                                                                                    //SS-954-NDR-20110907
    //if chbLogSucesos.Checked then
    //   FLogBuffer.SaveToFile(GoodDir(InstallIni.ReadString('LOGJORDAN','CARPETALOG','C:'))+'Log Jordan '+DateToStr(Now)+'.log');
end;

{-----------------------------------------------------------------------------
  Function Name: FormKeyDown
  Author:    gcasais
  Date Created: 23/12/2004
  Description: Permito cancelar la operacion
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmImprentaJORDAN.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
    case Key of
        VK_ESCAPE : FCancel := True;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnSalirClick
  Author:    lgisuk
  Date Created: 06/12/2005
  Description: Permito salir del formulario
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmImprentaJORDAN.btnSalirClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 06/12/2005
  Description: Permito salir si no esta procesando
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmImprentaJORDAN.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    //Permite salir solo si no esta procesando.
  	CanClose := not FOnProcess;
end;


{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    gcasais
  Date Created: 27/12/2004
  Description: Libero el form de memoria
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmImprentaJORDAN.FormClose(Sender: TObject; var Action: TCloseAction);
var
	CarpetaDBNet:String;
begin
	// Rev.9 / 09-07-2009 / Nelson Droguett Sierra -------------------------
    ObtenerParametroGeneral(DMConnections.BaseCAC, 'DIR_JORDAN_TIMBRE_ELECTRONICO', CarpetaDBNet);
    //Rev.11 / 03-Junio-2010 / Nelson Droguett Sierra--------------------------------
    //Deltree(ExpandFileName(GoodDir(CarpetaDBNet)+'..'));

    Action := caFree;
end;

// Rev.5 / 22-03-2009 / Nelson Droguett Sierra
// Carga la DLL
procedure TfrmImprentaJORDAN.ACargarDLLExecute(Sender: TObject);
var
	Cod : integer;
begin
    EscribeLog( 'Carga de la DLL En Memoria');
    Cod := CargarDLL();
    //EscribeLog( 'END Carga de la DLL En Memoria');
    //if Cod = 0 then mmoLog.Lines.Add('DLL OK')
    //else LogueaError();
end;


// Rev.5 / 22-03-2009 / Nelson Droguett Sierra
// Configurar la variable de entorno en forma manual (directorio local)
procedure TfrmImprentaJORDAN.AConfig_EGATE_HOME_ManualExecute(Sender: TObject);
const
	ValorPrueba = 'D:\SUITE\';
    //ValorPrueba = '\\pino\op_test\Aplicaciones\DBNET\suite\';
var
    Valor : string;
begin
    Valor := GetEnvironmentVariable('EGATE_HOME');
    SetEnvironmentVariable(PChar('EGATE_HOME'), PChar(ValorPrueba));
    Valor := GetEnvironmentVariable('EGATE_HOME');
    if ValorPrueba = Valor then PARAM_DIR_EGATE_HOME := Valor;
end;



// Rev.5 / 22-03-2009 / Nelson Droguett Sierra
// Configurar la variable de entorno en forma parametrica.
procedure TfrmImprentaJORDAN.AConfig_EGATE_HOME_ParametricoExecute(Sender: TObject);
var
    Mensaje : string;
begin
	//if not DMConnections.BaseCAC.Connected then ConectarBase();

    //if ConfigurarVariable_EGATE_HOME(Mensaje) then
    //  mmoLog.Lines.Add('EGATE_HOME Exitosa: ' + PARAM_DIR_EGATE_HOME)
    //else mmoLog.Lines.Add('EGATE_HOME Error: ' + Mensaje);
    ConfigurarVariable_EGATE_HOME(DMConnections.BaseCAC, False, Mensaje, True);
end;


// Rev.5 / 22-03-2009 / Nelson Droguett Sierra
// Liberar la DLL de memoria.
procedure TfrmImprentaJORDAN.ALiberarDLLExecute(Sender: TObject);
begin
  EscribeLog( 'Liberar la DLL de Memoria');
  LiberarDLL();
  //EscribeLog( 'END Liberar la DLL de Memoria');
    //if LiberarDLL() then mmoLog.Lines.Add('DLL Liberada OK')
    //else mmoLog.Lines.Add('Error al Liberar DLL');
end;





{
PageFaultCount - the number of page faults.
PeakWorkingSetSize - the peak working set size, in bytes.
WorkingSetSize - the current working set size, in bytes.
QuotaPeakPagedPoolUsage - The peak paged pool usage, in bytes.
QuotaPagedPoolUsage - The current paged pool usage, in bytes.
QuotaPeakNonPagedPoolUsage - The peak nonpaged pool usage, in bytes.
QuotaNonPagedPoolUsage - The current nonpaged pool usage, in bytes.
PagefileUsage - The current space allocated for the pagefile, in bytes. Those pages may or may not be in memory.
PeakPagefileUsage - The peak space allocated for the pagefile, in bytes.
}

// Rev.9 / 09-07-2009 / Nelson Droguett Sierra -------------------------
function TfrmImprentaJORDAN.CurrentMemoryUsage: Cardinal;
var
  pmc: TProcessMemoryCounters;
begin
  pmc.cb := SizeOf(pmc) ;
  if GetProcessMemoryInfo(GetCurrentProcess, @pmc, SizeOf(pmc)) then
    Result := pmc.WorkingSetSize
  else
    RaiseLastOSError;
end;

// Rev.9 / 09-07-2009 / Nelson Droguett Sierra -------------------------
function TfrmImprentaJORDAN.DelTree(DirName: string): Boolean;
var
  SHFileOpStruct : TSHFileOpStruct;
  DirBuf : array [0..255] of char;
begin
  try
   Fillchar(SHFileOpStruct,Sizeof(SHFileOpStruct),0) ;
   FillChar(DirBuf, Sizeof(DirBuf), 0 ) ;
   StrPCopy(DirBuf, DirName) ;
   with SHFileOpStruct do begin
    Wnd := 0;
    pFrom := @DirBuf;
    wFunc := FO_DELETE;
    fFlags := FOF_ALLOWUNDO;
    fFlags := fFlags or FOF_NOCONFIRMATION;
    fFlags := fFlags or FOF_SILENT;
   end;
    Result := (SHFileOperation(SHFileOpStruct) = 0) ;
   except
    Result := False;
  end;
end;



function TfrmImprentaJORDAN.PrepararProcesoInterfazImprenta: Boolean;
Resourcestring
    MSG_ERROR = 'Error al determinar comprobantes a imprimir';
var
  bReturnValue:Boolean;
  iComprobantes:integer;
begin
    bReturnValue:=True;
    try
        iComprobantesSimples := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT COUNT (*) FROM JORDANComprobantesAImprimir WITH (NOLOCK) '+
                                                                 'WHERE NumeroProcesoFacturacion='+IntToStr(FNumeroProceso)+' AND SoloNK=1'  );
        if iComprobantesSimples=0 then
        begin
          with spPrepararProcesoInterfazImprenta, Parameters do begin
               Refresh;
               ParamByName('@ComprobanteInicial').Value := neCOmprobanteInicial.ValueInt;
               ParamByName('@ComprobanteFinal').Value := neCOmprobanteFinal.ValueInt;
               ParamByName('@ForzarReimpresion').Value := IIF(FForzarReimpresion = True, 1 , 0); //Forzar Reimpresion
               ParamByName('@ImprimirElectronicos').Value := IIF(FImprimirElectronicos = True, 1 , 0); //Imprimir Electronicos
               ParamByName('@CodigoOperacion').Value := IIF(FReproceso = True, FNumeroProcesoAnterior, NULL); //Reproceso
               ParamByName('@NumeroProcesoFacturacion').Value := IIF(FPorFacturacion = True, FNumeroProceso, NULL); //Proceso Facturacion
               ParamByName('@SoloNK').Value := 1; //Simple
               CommandTimeOut := 50000;
               ExecProc;
          end;
          iComprobantesSimples := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT COUNT (*) FROM JORDANComprobantesAImprimir WITH (NOLOCK) '+
                                                                   'WHERE NumeroProcesoFacturacion='+IntToStr(FNumeroProceso)+' AND SoloNK=1'  );
        end;
//        CantidadBolSimple := ObtenerCantidadComprobantesAImprimirBOLSimple;
//        CantidadFacSimple := ObtenerCantidadComprobantesAImprimirFACSimple;

        pbPaso1Simple.Max := iComprobantesSimples;
        pbPaso1Simple.Position:=pbPaso1Simple.Max;
        pbPaso2Simple.Max := iComprobantesSimples;
        pbPaso3Simple.Max := iComprobantesSimples;
        pbPaso4Simple.Max := iComprobantesSimples;
        self.Repaint;
        Application.ProcessMessages;

        iComprobantesMultiples := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT COUNT (*) FROM JORDANComprobantesAImprimir WITH (NOLOCK) '+
                                                                 'WHERE NumeroProcesoFacturacion='+IntToStr(FNumeroProceso)+' AND SoloNK=0'  );
        if iComprobantesMultiples=0 then
        begin
          with spPrepararProcesoInterfazImprenta, Parameters do begin
               Refresh;
               ParamByName('@ComprobanteInicial').Value := neCOmprobanteInicial.ValueInt;
               ParamByName('@ComprobanteFinal').Value := neCOmprobanteFinal.ValueInt;
               ParamByName('@ForzarReimpresion').Value := IIF(FForzarReimpresion = True, 1 , 0); //Forzar Reimpresion
               ParamByName('@ImprimirElectronicos').Value := IIF(FImprimirElectronicos = True, 1 , 0); //Imprimir Electronicos
               ParamByName('@CodigoOperacion').Value := IIF(FReproceso = True, FNumeroProcesoAnterior, NULL); //Reproceso
               ParamByName('@NumeroProcesoFacturacion').Value := IIF(FPorFacturacion = True, FNumeroProceso, NULL); //Proceso Facturacion
               ParamByName('@SoloNK').Value := 0; //Multiple
               CommandTimeOut := 50000;
               ExecProc;
          end;
          iComprobantesMultiples := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT COUNT (*) FROM JORDANComprobantesAImprimir WITH (NOLOCK) '+
                                                                   'WHERE NumeroProcesoFacturacion='+IntToStr(FNumeroProceso)+' AND SoloNK=0'  );

        end;
//        CantidadBolMultiple := ObtenerCantidadComprobantesAImprimirBOLMultiple;
//        CantidadFacMultiple := ObtenerCantidadComprobantesAImprimirFACMultiple;
        pbPaso1Multiple.Max := iComprobantesMultiples;
        pbPaso1Multiple.Position := pbPaso1Multiple.Max;
        pbPaso2Multiple.Max := iComprobantesMultiples;
        pbPaso3Multiple.Max := iComprobantesMultiples;
        pbPaso4Multiple.Max := iComprobantesMultiples;
        self.Repaint;
        Application.ProcessMessages;
    except
        on E:Exception do begin
            MsgBoxErr(MSG_ERROR, e.Message, 'ERROR', MB_ICONERROR);
            bReturnValue:=False;
        end;
    end;
    Result := bReturnValue;
end;


function TfrmImprentaJORDAN.ObtenerLineasInterfazImprenta: Boolean;
var
  bReturnValue:Boolean;
  iComprobantesPendientesSimples,iComprobantesPendientesMultiples:int64;
begin
    bReturnValue := True;
    iComprobantesPendientesSimples:=ObtenerComprobantesPendientesSimples;
    while (iComprobantesPendientesSimples > 0) and (not FCancel) do begin
        with spObtenerLineasInterfazImprenta, Parameters do begin
            if (State <> dsInactive) then  Close;
            Refresh;
            ParamByName('@Img').Value := FNombreImagenCatalogo;
            ParamByName('@FechaProceso').Value := FFechaProceso;
            ParamByName('@SoloNK').Value := 1; //Simple
            ParamByName('@TotalComprobantesFAC').Value := 0;
            ParamByName('@TotalComprobantesBOL').Value := 0;
            CommandTimeOut := 50000;
            ExecProc;
        end;
        iComprobantesPendientesSimples := ObtenerComprobantesPendientesSimples;
        pbPaso2Simple.Position := (iComprobantesSimples - iComprobantesPendientesSimples);
        Application.ProcessMessages;
        if FCancel then begin
            bReturnValue:=False;
            Exit;
        end;
    end;
    pbPaso2Simple.Position := (iComprobantesSimples - iComprobantesPendientesSimples);
    Application.ProcessMessages;


    if bReturnValue then
    begin
      iComprobantesPendientesMultiples:=ObtenerComprobantesPendientesMultiples;
      while (iComprobantesPendientesMultiples > 0) and (not FCancel) do begin
          with spObtenerLineasInterfazImprenta, Parameters do begin
              if (State <> dsInactive) then  Close;
              Refresh;
              ParamByName('@Img').Value := FNombreImagenCatalogo;
              ParamByName('@FechaProceso').Value := FFechaProceso;
              ParamByName('@SoloNK').Value := 0; //Multiple
              ParamByName('@TotalComprobantesFAC').Value := 0;
              ParamByName('@TotalComprobantesBOL').Value := 0;
              CommandTimeOut := 50000;
              ExecProc;
          end;
          iComprobantesPendientesMultiples := ObtenerComprobantesPendientesMultiples;
          pbPaso2Multiple.Position := (iComprobantesMultiples - iComprobantesPendientesMultiples);
          Application.ProcessMessages;
          if FCancel then begin
              bReturnValue:=False;
              Exit;
          end;
      end;
      pbPaso2Multiple.Position := (iComprobantesMultiples - iComprobantesPendientesMultiples);
      Application.ProcessMessages;
    end;
    Result:=bReturnValue;
end;

function TfrmImprentaJORDAN.TimbrarComprobantesElectronicos: Boolean;
resourcestring
    MSG_ERROR_GENERATING_TEMP_FILE  = 'Error generando el archivo temporal.';
    MSG_ERROR_OBTENER_RUT           = 'Ha ocurrido un error al obtener el RUT de la concesionaria.';
    MSG_ERROR_TIMBRE	              = 'No se pudo timbrar el documento electr�nico';
Const
    STR_WRITING_FILE_IN_DISC = 'Escribiendo los datos al disco.';
var
  Timbre : TTimbreArchivo;
  RutConcesionaria : String;
  CodigoErrorSQL,iRegistro,iSoloNK:integer;
  iComprobantesPendientesTimbre,iPendientesTimbreSimples,iPendientesTimbreMultiples:int64;
  bTimbreVacio:Boolean;
begin
  try
    ObtenerParametroGeneral(ObtenerDatosJordan.Connection, 'RUT_EMPRESA', RutConcesionaria);
  except
    on E: Exception do begin
        MsgBoxErr(MSG_ERROR_OBTENER_RUT, E.Message, Self.Caption, MB_ICONSTOP);
        Result := False;
        Exit;
    end;
  end;
  bTimbreVacio:=False;
  labelProgreso.Caption := STR_WRITING_FILE_IN_DISC;
  try
      iComprobantesPendientesTimbre := QueryGetValueInt(DMConnections.BaseCAC,
                                      'SELECT COUNT (*) FROM JORDANComprobantesAImprimir WITH (NOLOCK) '+
                                      'WHERE NumeroProcesoFacturacion='+IntToStr(FNumeroProceso)+
                                      ' AND TED IS NULL'  );
      iPendientesTimbreSimples:=QueryGetValueint(ObtenerDatosJORDAN.Connection,
                                     'SELECT COUNT(*) FROM JORDANComprobantesAImprimir  WITH (NOLOCK) WHERE (TED IS NULL) AND SoloNK=1',5000);
      pbPaso3Simple.Position    := iComprobantesSimples - iPendientesTimbreSimples;
      iPendientesTimbreMultiples:= QueryGetValueInt(ObtenerDatosJORDAN.Connection,
                                     'SELECT COUNT(*) FROM JORDANComprobantesAImprimir  WITH (NOLOCK) WHERE (TED IS NULL) AND SoloNK=0',5000);
      pbPaso3Multiple.Position  := iComprobantesMultiples - iPendientesTimbreMultiples;


      if iComprobantesPendientesTimbre>0 then
      begin
        ACargarDLL.Execute;
        Timbre.Timbre:='';
        Timbre.Archivo:='';
        DBGJordanObtenerComprobantesATimbrar.DataSource.DataSet.DisableControls;
        JORDAN_ObtenerComprobantesATimbrar.CommandTimeout:=10000;
        for iSoloNK := 1 downto  0  do
        begin
          while ifthen(iSoloNK=1,iPendientesTimbreSimples,iPendientesTimbreMultiples)>0 do
          begin
            JORDAN_ObtenerComprobantesATimbrar.Close;
            JORDAN_ObtenerComprobantesATimbrar.Parameters.Refresh;
            JORDAN_ObtenerComprobantesATimbrar.Parameters.ParamByName('@NumeroProcesoFacturacion').Value := FNumeroProceso;
            JORDAN_ObtenerComprobantesATimbrar.Parameters.ParamByName('@SoloNk').Value := iSoloNK;  // Pasa con 1 para simple y despues con 0 para multiple
            JORDAN_ObtenerComprobantesATimbrar.Open;
            if NOT (JORDAN_ObtenerComprobantesATimbrar.IsEmpty) then
            begin
              With DBGJordanObtenerComprobantesATimbrar.DataSource.DataSet do
              begin
                  First;
                  while Not(Eof) do
                  begin
                    spObtenerDatosParaTimbreElectronico.Parameters.Refresh;
                    spObtenerDatosParaTimbreElectronico.Parameters.ParamByName('@TipoComprobante').Value := FieldByName('TipoComprobante').AsString;
                    spObtenerDatosParaTimbreElectronico.Parameters.ParamByName('@NumeroComprobante').Value := FieldByName('NumeroComprobante').AsInteger;
                    spObtenerDatosParaTimbreElectronico.ExecProc;
                    if ( ObtenerTimbreDBNet(  spObtenerDatosParaTimbreElectronico.Parameters.ParamByName('@RutEmisor').Value,
                                                  spObtenerDatosParaTimbreElectronico.Parameters.ParamByName('@DVRutEmisor').Value,
                                                  spObtenerDatosParaTimbreElectronico.Parameters.ParamByName('@CodigoTipoDocumentoElectronico').Value,
                                                  spObtenerDatosParaTimbreElectronico.Parameters.ParamByName('@NumeroComprobanteFiscal').Value,
                                                  spObtenerDatosParaTimbreElectronico.Parameters.ParamByName('@FechaEmision').Value,
                                                  spObtenerDatosParaTimbreElectronico.Parameters.ParamByName('@TotalComprobante').Value,
                                                  spObtenerDatosParaTimbreElectronico.Parameters.ParamByName('@DescripcionPrimerItem').Value,
                                                  spObtenerDatosParaTimbreElectronico.Parameters.ParamByName('@RutReceptor').Value,
                                                  spObtenerDatosParaTimbreElectronico.Parameters.ParamByName('@DVRutReceptor').Value,
                                                  spObtenerDatosParaTimbreElectronico.Parameters.ParamByName('@RazonSocialONombrePersona').Value,
                                                  spObtenerDatosParaTimbreElectronico.Parameters.ParamByName('@FechaTimbre').Value,
                                                  False, Timbre
                                              )
                       ) then
                      begin
                        if Trim(Timbre.Timbre)<>'' then
                        begin
                          JORDAN_ActualizarTED.Close;
                          JORDAN_ActualizarTED.Parameters.ParamByName('@TipoComprobante').Value:=FieldByName('TipoComprobante').AsString;
                          JORDAN_ActualizarTED.Parameters.ParamByName('@NumeroComprobante').Value:=FieldByName('NumeroComprobante').AsInteger;
                          JORDAN_ActualizarTED.Parameters.ParamByName('@TED').Value:=Timbre.Timbre;
                          JORDAN_ActualizarTED.ExecProc;
                          if iSoloNK=1 then FHayDatosArchivoSimple:=True else FHayDatosArchivoMultiple:=True;
                        end
                        else
                        begin
                          bTimbreVacio:=True;
                          Break;
                        end;
                      end
                    else
                      begin
                        MsgBox(MSG_ERROR_TIMBRE + CR +
                                DBGJordanObtenerComprobantesATimbrar.DataSource.DataSet.FieldByName('TipoComprobanteFiscal').AsString + ' ' +
                                DBGJordanObtenerComprobantesATimbrar.DataSource.DataSet.FieldByName('NumeroComprobanteFiscal').AsString ,
                                Caption,
                                MB_ICONERROR);
                        Break;
                      end;
                    Next;
                  end;
                  if bTimbreVacio then
                     Break;

              end;
            end;
            iPendientesTimbreSimples:=QueryGetValueint(ObtenerDatosJORDAN.Connection,
                                           'SELECT COUNT(*) FROM JORDANComprobantesAImprimir  WITH (NOLOCK) WHERE (TED IS NULL) AND SoloNK=1',5000);
            pbPaso3Simple.Position    := iComprobantesSimples - iPendientesTimbreSimples;
            iPendientesTimbreMultiples:= QueryGetValueInt(ObtenerDatosJORDAN.Connection,
                                           'SELECT COUNT(*) FROM JORDANComprobantesAImprimir  WITH (NOLOCK) WHERE (TED IS NULL) AND SoloNK=0',5000);
            pbPaso3Multiple.Position  := iComprobantesMultiples - iPendientesTimbreMultiples;
            Application.ProcessMessages;
          end;
          if bTimbreVacio then
             Break;
        end;
        DBGJordanObtenerComprobantesATimbrar.DataSource.DataSet.EnableControls;
        ALiberarDLL.Execute;
      end
      else
      begin
        pbPaso3Simple.Position := iComprobantesSimples;
        pbPaso3Multiple.Position := iComprobantesMultiples;
      end;
      if bTimbreVacio then
      begin
        lblProcesoInterrumpido.Visible:=True;
        Result:= False
      end
      else
        Result := True;
  except
      on E : Exception do begin
          Result := false;
          ShowMessage(E.Message + CR + 'Comprobante : ' +
                      DBGJordanObtenerComprobantesATimbrar.DataSource.DataSet.FieldByName('TipoComprobante').AsString + ' ' +
                      DBGJordanObtenerComprobantesATimbrar.DataSource.DataSet.FieldByName('NumeroComprobante').AsString);
      end;
  end;
end;

function TfrmImprentaJORDAN.GenerarArchivosDatos: Boolean;
resourcestring
    MSG_ERROR_GENERATING_TEMP_FILE  = 'Error generando el archivo temporal.';
    MSG_ERROR_OBTENER_RUT           = 'Ha ocurrido un error al obtener el RUT de la concesionaria.';
    MSG_ERROR_TIMBRE	              = 'No se pudo timbrar el documento electr�nico';
Const
    STR_WRITING_FILE_IN_DISC = 'Escribiendo los datos al disco.';
var
  CodigoErrorSQL,iRegistro,iSoloNK:integer;
begin
  try
    for iSoloNK := 1 downto  0  do
    begin
        JORDAN_LineasAImprenta.Close;
        JORDAN_LineasAImprenta.Parameters.ParamByName('@NumeroProcesoFacturacion').Value:=FNumeroProceso;
        JORDAN_LineasAImprenta.Parameters.ParamByName('@SoloNK').Value:=iSoloNK;
        JORDAN_LineasAImprenta.ExecProc;
        if JORDAN_LineasAImprenta.Parameters.ParamByName('@DescripcionError').Value<>'' then
        begin
          ShowMessage(JORDAN_LineasAImprenta.Parameters.ParamByName('@DescripcionError').Value);
        end;
        
        if iSoloNK=1 then
           pbPaso4Simple.Position := pbPaso4Simple.Max
        else
           pbPaso4Multiple.Position := pbPaso4Multiple.Max;
        Application.ProcessMessages;
    end;
    FHayDatosArchivoSimple := QueryGetValueint(ObtenerDatosJORDAN.Connection,
                                           'SELECT COUNT(*) FROM JORDANComprobantesAImprimir  WITH (NOLOCK) WHERE (TED IS NOT NULL) AND SoloNK=1 AND FaseProceso=3',5000) > 0 ;
    FHayDatosArchivoMultiple  := QueryGetValueInt(ObtenerDatosJORDAN.Connection,
                                           'SELECT COUNT(*) FROM JORDANComprobantesAImprimir  WITH (NOLOCK) WHERE (TED IS NOT NULL) AND SoloNK=0 AND FaseProceso=3',5000) > 0;
    Result:=True;
  except
      on E : Exception do begin
          Result := false;
          ShowMessage(E.Message);
      end;
  end;
end;



end.
