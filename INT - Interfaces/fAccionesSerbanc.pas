
{********************************** Unit Header ********************************
File Name : fAccionesSerbanc.pas
Author : ndonadio
Date Created: 18/08/2005
Language : ES-AR
Description :

Revision: 1
Author: jconcheyro
Date:   07/09/2006
Description: le subo el timeout a los stores e ActualizarComprobantesEnviados a 180

Revision: 2
Author: pdominguez
Date:   29/05/2009
Description: SS 803
        - Se modificaron los siguientes procedimientos / funciones:
            GuardarDetalle
            btnProcesarClick
            ValidarAccion
            ValidarLinea
            ValidarTiposDeudorSerbanc
            LeerRegistroControl

Revision: 3
Author: ebaeza
Date: 10-03-2014
Firma: SS_1174_EBA_20140310
Description: SS 1174
        - Se modifican las siguientes estructuras / funciones:
          TRegDetalle : se agrega campo Observacion
          ParsearLinea : se obtiene la observacion desde la linea obtenida de archivo
          GuardarDetalle : se agrega parametro Observaciones
          btnBuscarArchivoClick: se corrige ortograf�a
      11/03/2014
          -Se agrega la validacion del Tipo de cobranza en la existencia del registro
      13/03/2014
          -Se corrigen errores NO asociados a la SS_1174 detectado por testing
           -Error al mover archivo, este problema se genera porque al llamar la funcion
            MoverArchivoProcesado se le esta pasando solo el path sin el nombre del archivo
            a mover.
            Y se implementa que al momento de cambiar la ubicacion del origen del archivo
            se actualice la variable global FOrigen, esto tambi�n producia un error
           -Error al cancelar en la ventana de impresion/visualizacion de informe
            Realmente no es un error, cuando el usuario cancela esta operacion ya sea
            presionando la tecla ESC o haciendo click en el boton CANCELAR, no se controla
            que esta es una accion realizada por el usuario, enviando un mensaje utilizando
            la ventana de error lo que produce un mal entendido al usuario.
            Se implementa el control de cancelar por parte del usuario y se mostrara una mensaje
            utilizando un mensaje utilizando ventana de advertencia.
          - Se agrega la variable privada FNameFile de tipo AnsiString, para obtener el nombre
            del archivo seleccionado
          - Se modificaron los siguientes procedure / funciones:
            btnProcesarClick
            btnBuscarArchivoClick
      17/03/2014
          - Se limpia el objeto listbox que contiene los errores, y el objeto cdsRechazos
             al inicio del procedure btnProcesarClick
             se fuerza la deshabilitacion el boton procesar en finally
          -Se corrigue comparacion en el IF donde informa que el archivo ya ha sido
             procesado y si quiero procesarlo o no, cuando la respuesta es NO, igual
             habilitaba el boton procesar.
             Se agrega al principio la deshabilitacion del boton procesar
       21/03/2014
          -Se eliminan las validaciones de duplicidad, por solicitud de MVI v�a correo, es todo lo contrario de la SS_1174.
          -Se crea nueva firma SS_1174_EBA_20140321, para diferenciar este cambio, no menor
          -La funcion ValidarAccion se comenta completa
          -La funcion ValidarLinea se comenta la validacion de duplicidad que hacia con el objeto cdsArchivo

          1-Se elimina objeto de tipo TADOStoredProc llamado spValidarAccionCobranza. (El codigo relacionado a este objeto en el archivo FAccionesSerbanc.pas esta comentado)
          2-Modificacion del indice del objeto cdsArchivo se cambia de true a false las propiedades ixPrimary e ixUnique.



Firma       : SS_1147Q_NDR_20141202[??]
Descripcion : Si es VS el convenio es de largo 12 no 17

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

Firma       : SS_1284_CQU_20150527
Descripcion : Se cambia la obtencion del codigo de convenio, para CN usa la funci�n
              y para VS la consulta que rellena con 0 el NumeroConvenio
*******************************************************************************}
unit fAccionesSerbanc;

interface

uses
    // Utiles
    PeaProcs, PeaTypes, ComunesInterfaces, Util, UtilProc, StrUtils,
    // Parametros Generales
    ConstParametrosGenerales,
    // DB
    UtilDB, DMConnection,
    // Reporte finalizacion
    fReporteAccionesSerbanc,
    // Otros
    Windows, Messages, SysUtils, Variants, Classes,  Graphics,  Controls, Forms,
    Dialogs, StdCtrls, DmiCtrls, ComCtrls, ExtCtrls, DB, ADODB, ppDB,     ppDBPipe, ppPrnabl, ppClass,
    ppCtrls, ppBands,  ppCache,  ppComm,   ppRelatv, ppProd,    ppReport, UtilRB,
  DBClient, Buttons, CollPnl, ListBoxEx, DBListEx;


type
  TRegDetalle = record
        TipoCobranza: Integer;
        TipoDeudor  : Integer;
        RUT         : String;
        CodigoConv  : Integer;
        TipoAccion  : Integer;
        FEchaAccion : TDateTime;
        NumeroConvenio: String;
        CodigoEmpresa: integer;
        Observacion: string;         //SS_1174_EBA_20140310
  end;


  TfrmAccionesSerbanc = class(TForm)
    pnlAbajo: TPanel;
    OpenDialog: TOpenDialog;
    spInsertarActividadCobranzaSerbanc: TADOStoredProc;
    spRechazarAccion: TADOStoredProc;
    PageControl1: TPageControl;
    tsValidas: TTabSheet;
    dblValidas: TDBListEx;
    cpPanelTop: TCollapsablePanel;
    btnBuscarArchivo: TSpeedButton;
    lblOrigen: TLabel;
    txtOrigen: TEdit;
    cdsArchivo: TClientDataSet;
    cdsRechazos: TClientDataSet;
    dsRechazos: TDataSource;
    btnProcesar: TButton;
    btnCancelar: TButton;
    btnSalir: TButton;
    PnlAvance: TPanel;
    pbProgreso: TProgressBar;
    cdsNombreAcciones: TClientDataSet;
    tsErrores: TTabSheet;
    cdsErrores: TClientDataSet;
    ObtenerErroresAcciones: TADOStoredProc;
    lblProcesoGeneral: TLabel;
    lblErrores: TListBox;
    cdsTiposDeudores: TClientDataSet;
    cdsTiposCobranza: TClientDataSet;
    procedure btnCancelarClick(Sender: TObject);
    procedure btnProcesarClick(Sender: TObject);
    procedure ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure ImgAyudaClick(Sender: TObject);
//    procedure txtOrigenButtonClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSalirClick(Sender: TObject);
    procedure btnBuscarArchivoClick(Sender: TObject);
  private
    { Private declarations }
    FProcesando: Boolean;
    FCancelar: Boolean;
    FDestinoProcesados,
    FOrigen: AnsiString;
    FNameFile: AnsiString;              //SS_1174_EBA_20140310
    FCodigoOperacionInterfase: Integer;
    FErrorSinDescripcion: Ansistring;
    FFechaProcesamientoActual: TDateTime;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    // funciones y procedures
    function LeerRegistroControl( SList: TStringList; IndexToRead: Integer;
                                    var FechaArchivo: TDateTime; var CantRegistros: Integer;
                                    var Error: AnsiString):boolean;
    function ParsearLinea(Origen: AnsiString; FechaArchivo: TDateTime ;
                        var Destino: TRegDetalle; var Error: AnsiString): boolean;
    Function ObtenerCodigoConvenio(NumeroConvenio: string; var Error: AnsiString): Integer;
    Function ValidarTipoActividad( Actividad: String; var Error: AnsiString): Boolean;
//    function ValidarAccion(RegDetalle: TRegDetalle; var Motivo: string; var ErrorProceso: string; CodigoEmpresaCobranza: Integer):Boolean;              // SS_1174_EBA_20140321
    function ValidarLinea(RegDetalle: TRegDetalle; CodigoOperacion: Integer;var ErrorProceso: AnsiString; CodigoEmpresaCobranza: Integer): Boolean;
//    Function GuardarDetalle(RegDetalle: TRegDetalle; CodigoOperacion: Integer; var Error: AnsiString): Boolean;
    Function GuardarDetalle(CodigoOperacion: Integer; var Error: AnsiString; CodigoEmpresaCobranza: Integer): Boolean;
    function AgregarAccionRechazada(RegDetalle: TRegDetalle; CodigoOperacion: integer; Motivo: AnsiString): Boolean;
//    Function ActualizarComprobantesEnviados(CodigoOperacion: integer; var Error: AnsiString): Boolean;
    Function GuardarErrores(FCodigoOperacionInterfase: Integer; strErrores: TStringList; var Error: AnsiString ): Boolean;
    procedure HabilitarBotones;
    procedure MostrarReporteFinalizacion;
    procedure ActualizarComprobantesEnviadosActivos(CodigoOperacion: Integer);
    procedure ActualizarComprobantesEnviadosNoActivos(CodigoOperacion: Integer);
    procedure ActualizarComprobantesEnviadosDetalle(CodigoOperacion: Integer);
    function CargarNombreAcciones: Boolean;
    function CargarTiposDeudores: Boolean;
    function CargarTiposCobranzas: Boolean;
    function CargarErroresAcciones(var MensajeError: AnsiString): Boolean;
    function DescripcionError(unCodigoError: string): String;
    function ValidarTiposDeudorSerbanc(CodigoTipoDeudor: integer; var Motivo, ErrorProceso: AnsiString): boolean;
    function ValidarCodigoEmpresaRecaudadora(CodigoEmpresa: integer): boolean;
    function ObtenerCodigoEmpresa(NombreArchivo: string):integer;
    function ValidarTipoDeudor(deudorStr: String;  var Error: AnsiString): Boolean;
    function ValidarTipoCobranza(TipoCobranzaStr: String;
  var Error: AnsiString): Boolean;
  public
    { Public declarations }
    function Inicializar(Titulo: AnsiString): boolean;
  end;


var
  frmAccionesSerbanc: TfrmAccionesSerbanc;

implementation

{$R *.dfm}

procedure TfrmAccionesSerbanc.btnSalirClick(Sender: TObject);
begin
    if not FProcesando then  Close;
end;

procedure TfrmAccionesSerbanc.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TfrmAccionesSerbanc.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := not FProcesando;
end;

function TfrmAccionesSerbanc.CargarNombreAcciones: Boolean;
var
    Sql: string;
    unSp: TAdoStoredProc;
begin
    Result := False;
    unsp := TADOStoredProc.Create(nil);
    with unSp do begin
        Connection := DmConnections.BaseCAC;
        ProcedureName := 'ObtenerTiposActividadesCobranzasSerbanc';
        try
            Open;
            while not Eof do begin
                cdsNombreAcciones.AppendRecord([ FieldByName('CodigoActividadCobranza').asinteger,
                    FieldByName('ActividadCobranza').asString]);
                Next;
            end;
            Result := True;
        finally
            Close;
            Free;
        end;
    end;

end;

function TfrmAccionesSerbanc.CargarTiposDeudores: Boolean;
var
    Sql: string;
    unSp: TAdoStoredProc;
begin
    Result := False;
    unsp := TADOStoredProc.Create(nil);
    with unSp do begin
        Connection := DmConnections.BaseCAC;
        ProcedureName := 'ObtenerTiposDeudores';
        try
            Open;
            while not Eof do begin
                cdsTiposDeudores.AppendRecord([ FieldByName('CodigoTipoDeudor').asinteger]);
                Next;
            end;
            Result := True;
        finally
            Close;
            Free;
        end;
    end;
end;

function TfrmAccionesSerbanc.CargarTiposCobranzas: Boolean;
var
    Sql: string;
    unSp: TAdoStoredProc;
begin
    Result := False;
    unsp := TADOStoredProc.Create(nil);
    with unSp do begin
        Connection := DmConnections.BaseCAC;
        ProcedureName := 'ObtenerTiposCobranzas';
        try
            Open;
            while not Eof do begin
                cdsTiposCobranza.AppendRecord([ FieldByName('CodigoTipoCobranza').asinteger]);
                Next;
            end;
            Result := True;
        finally
            Close;
            Free;
        end;
    end;
end;

function TfrmAccionesSerbanc.CargarErroresAcciones(var MensajeError: AnsiString): Boolean;
const
    COD_MODULO = 58;
begin
    Result := False;
    with ObtenerErroresAcciones do begin
        try
            Parameters.ParamByName('@CodigoModulo').Value := COD_MODULO;
            Open;
            while not Eof do begin
                cdsErrores.AppendRecord([FieldByName('CodigoTipoError').asString,
                    FieldByName('DescripcionError').asString]);
                Next;
            end;
            Result := True;
        except
            on e: exception do begin
                MensajeError := 'No se pudieron cargar los errores de acciones de cobranza, Error: ' +  E.Message;
            end;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: Inicializar
Author : ndonadio
Date Created : 10/08/2005
Description :
Parameters : None
Return Value : boolean
*******************************************************************************}
function TfrmAccionesSerbanc.Inicializar(Titulo: AnsiString): boolean;
const
    DIR_ORIGEN_ACT_SERBANC  = 'DIR_ORIGEN_ACT_SERBANC';
    DIR_DEST_PROC_ACT_SERBANC   = 'DIR_DEST_PROC_ACT_SERBANC';
resourcestring
    ERROR_CANNOT_INITIALIZE     = 'No se puede inicializar la interfaz.';
    MSG_NOTHING_TO_PROCESS      = 'No hay Activaciones Pendientes.';
    ERROR_CANNOT_GET_PARAMETER  = 'Error obteniendo el parametro general %s';
    MSG_INIT_ERROR = 'Error al Inicializar';
    MSG_ERROR = 'Error';
var
    DescripcionError: AnsiString;
begin
    Result := False;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

    // Le pongo de caption lo que trae de parametro...
    Caption :=  Titulo;
    //Centro el form
  	CenterForm(Self);
    try
        FErrorSinDescripcion := QueryGetValue(DmConnections.BaseCAC, 'select dbo.CONST_CODIGO_ERROR_SIN_DESCRIPCION()' , 50) + ' Error en par�metro: %s.';
        cdsArchivo.CreateDataSet;
        cdsArchivo.LogChanges := False;
        cdsRechazos.CreateDataSet;
        cdsRechazos.LogChanges := False;
        cdsNombreAcciones.CreateDataSet;
        cdsNombreAcciones.LogChanges := False;
        cdsErrores.CreateDataSet;
        cdsErrores.LogChanges := False;
        cdsTiposDeudores.CreateDataSet;
        cdsTiposDeudores.LogChanges := False;
        cdsTiposCobranza.CreateDataSet;
        cdsTiposCobranza.LogChanges := False;
        CargarNombreAcciones;
        CargarTiposDeudores;
        CargarTiposCobranzas;
        if not CargarErroresAcciones(DescripcionError) then
            MsgBoxErr(MSG_INIT_ERROR, DescripcionError, MSG_ERROR, MB_ICONERROR);
    except
        on e: Exception do begin
            MsgBoxErr(MSG_INIT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
        end;
    end;

    // Cargar el parametro general de directorio de origen standard
    if not ObtenerParametroGeneral(DMConnections.BaseCAC, DIR_ORIGEN_ACT_SERBANC, FOrigen) then begin
        MsgBoxErr(ERROR_CANNOT_INITIALIZE, Format(ERROR_CANNOT_GET_PARAMETER, [DIR_ORIGEN_ACT_SERBANC]), Caption, MB_ICONSTOP);
        Exit;
    end;
    // Cargo el parametro que me indica a donde mandar los procesados...
    if not ObtenerParametroGeneral(DMConnections.BaseCAC, DIR_DEST_PROC_ACT_SERBANC, FDestinoProcesados) then begin
        MsgBoxErr(ERROR_CANNOT_INITIALIZE, Format(ERROR_CANNOT_GET_PARAMETER, [DIR_DEST_PROC_ACT_SERBANC]), Caption, MB_ICONSTOP);
        Exit;
    end;
    FProcesando := False;
    FCancelar := False;
    // muestra el directorio de destino
    txtOrigen.Text := GoodDir(FOrigen);
    Result := True;
end;

//Comento todo este bloque porque ahora se hace por otra componente.
{******************************** Function Header ******************************
Function Name: txtOrigenButtonClick
Author : ndonadio
Date Created : 10/08/2005
Description :  Muestra el dialogo para seleccionar un archivo a procesar...
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
//procedure TfrmAccionesSerbanc.txtOrigenButtonClick(Sender: TObject);
    {******************************** Function Header ******************************
    Function Name: ObtenerFiltro
    Author : ndonadio
    Date Created : 10/08/2005
    Description :
    Parameters : None
    Return Value : AnsiString
    *******************************************************************************}
{    Function ObtenerFiltro: AnsiString;
    Const
        FILE_TITLE	   = 'Rendicion ed Acciones Serbanc|';
        FILE_NAME 	   = 'ACN_COB_';
        FILE_EXTENSION = '.txt';
    begin
            Result:= FILE_TITLE + FILE_NAME + '*' + FILE_EXTENSION;
    end;

resourcestring
    MSG_ERROR = 'Error';
  	MSG_ERROR_FILE_DOES_NOT_EXIST   = 'El archivo %s no existe';
  	MSG_ERROR_INVALID_FILENAME 		= 'El nombre del archivo %s es inv�lido';
  	MSG_FILE_ALREADY_PROCESSED 		= 'El archivo %s ya fue procesado';
var
    Filtro: string;
begin
    //obtengo el filtro a aplicar a este tipo de archivos
    Filtro := ObtenerFiltro;

    Opendialog.InitialDir := txtOrigen.Text;;
  	OpenDialog.FileName := '';
    OpenDialog.Filter := Filtro; //solo veo este tipo de archivos

    if opendialog.execute then begin
        txtOrigen.text:=UpperCase( opendialog.filename );

        if not FileExists( txtOrigen.text ) then begin
        	MsgBox( Format ( MSG_ERROR_FILE_DOES_NOT_EXIST, [txtOrigen.text]), MSG_ERROR, MB_ICONERROR );
            Exit;
        end;

    	if  VerificarArchivoProcesado( DMConnections.BaseCAC, txtOrigen.text ) then begin
    			MsgBox (Format(MSG_FILE_ALREADY_PROCESSED, [ExtractFileName(txtOrigen.text)]),Caption, MB_ICONSTOP);
                Exit;
    	end;

        btnProcesar.Enabled := True;
	end;
end;}

{******************************** Function Header ******************************
Function Name: ImgAyudaClick
Author : ndonadio
Date Created : 10/08/2005
Description : muestra el globito de ayuda...
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmAccionesSerbanc.ImgAyudaClick(Sender: TObject);
Resourcestring
    MSG_ACCIONES_SERBANC  = ' ' + CRLF +
                          'El Archivo de Rendicion de Acciones es enviado' + CRLF +
                          'por SERBANC  para informar al ESTABLECIMIENTO ' + CRLF +
                          'de las diferentes Gestiones de Cobranza que est� llevando a cabo' + CRLF +
                          ' ';
begin
    //si esta procesando sale
    if FProcesando then exit;
    //Muestro el mensaje
    //MsgBoxBalloon(MSG_ACCIONES_SERBANC, Caption, MB_ICONQUESTION, IMGAYUDA);
end;

procedure TfrmAccionesSerbanc.ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
    //if FProcesando then ImgAyuda.Cursor := crdefault else ImgAyuda.Cursor := crHandPoint;
end;


{******************************** Function Header ******************************
Function Name: btnProcesarClick
Author : ndonadio
Date Created : 10/08/2005
Description : Procesa el archivo seleccionado.
Parameters : Sender: TObject
Return Value : None
*******************************************************************************
  Revision 1
  lgisuk
  16/10/07
  Ahora se registra en el log que el usuario cancelo el proceso
******************************************************************************
  Revision 2
  lgisuk
  06/11/2007
  Ahora se registra en el log las excepciones, se quita la transaccion y
  muestra un mensaje de error mas descriptivo.
******************************************************************************
  Revision 3
  pdominguez
  29/05/2009
  Se a�ade el par�metro CodigoEmpresa a la llamada a la funci�n GuardarDetalle
******************************************************************************
  Revision 4
  ebaeza
  13/03/2014
  Se agrega el nombre del archivo en los parametros que se pasan a la funcion
  MoverArchivoProcesado
  ---------------------------
  17-03-2014
  Firma: SS_1174_EBA_20140310
  Description: SS 1174
          - Se limpia el objeto listbox que contiene los errores, y el objeto cdsRechazos
          al inicio del procedure btnProcesarClick
          se fuerza la deshabilitacion el boton procesar en finally
******************************************************************************}
procedure TfrmAccionesSerbanc.btnProcesarClick(Sender: TObject);
resourcestring
    // Errores...
    ERROR_CONTROL_NO_VERIFICA       = 'Los valores del Registro de Control no concuerdan con el archivo.'+CRLF+'El Archivo no es v�lido.';
    ERROR_EMPRESACODIGO             = 'C�digo de empresa no es V�lido';
    ERROR_CANT_LOAD_FILE            = 'No se puede abrir el archivo especificado';
    ERROR_CANT_REGISTER_LOG         = 'No se puede registrar la Operaci�n en el Log.';
    ERROR_CANT_READ_CONTROL_ROW     = 'No se puede leer el registro de control del archivo.';
    ERROR_CANNOT_SAVE_DATA          = 'No se puede guardar el registro de Actividad';
    ERROR_CANT_FINISH_OPERATION     = 'No se puede finalizar la operaci�n';
    ERROR_EN_LINEA                  = 'Linea Nro. %d - ';
    ERROR_DEFAULT                   = 'No se puede procesar';
    MSG_PROCESS_CANCELLED_BY_USER   = 'Proceso Cancelado por el usuario';
  	MSG_FILE_ALREADY_PROCESSED 		  = 'El archivo %s ya fue procesado' + CRLF + '�Desea reprocesar el archivo?';
    MSG_END_OK                      = 'El proceso termin� con �xito';
const
    REGISTRO_A_LEER = 0; // el primero de todos...
    STR_LINE =  'Linea: ';
    MSG_VALIDANDO = 'Validando datos..';
var
    strOrigen,
    strErrores: TStringList;
    AuxString,
    DescError: AnsiString;
    FechaGeneracion: TDatetime;
    CantidadRegistros: Integer;
    CodigoEmpresa : integer;
    i, RegistrosRefresco: integer;
    RegDetalle: TRegDetalle;
begin
    // verifico que el archivo no haya sido ya procesado...
    {if VerificarArchivoProcesado(DMConnections.BaseCAC, ExtractFileName(txtOrigen.Text)) then begin
        if not MsgBox(Format(MSG_FILE_ALREADY_PROCESSED, [ExtractFileName(txtOrigen.text)]), Caption,
            MB_ICONQUESTION or MB_YESNO) = IDYES then
            Exit;
    end;}

    //limpiando los objetos                                     // SS_1174_EBA_20140310
    lblErrores.items.clear;                                     // SS_1174_EBA_20140310
    if not cdsRechazos.IsEmpty then cdsRechazos.EmptyDataSet;   // SS_1174_EBA_20140310

    // creo la string list que contendr� al archivo
    strOrigen := TStringList.Create;
    strErrores := TStringList.Create;
    // Indico que se ha comenzado a procesar
    FProcesando := True;
    HabilitarBotones;
    FFechaProcesamientoActual := NowBase(DMConnections.BaseCAC);
    if not cdsArchivo.Active then cdsArchivo.Active := True;
    if not cdsArchivo.IsEmpty then cdsArchivo.EmptyDataSet;
    if not cdsRechazos.Active then cdsRechazos.Active := True;
    if not cdsRechazos.IsEmpty then cdsRechazos.EmptyDataSet;
    lblProcesoGeneral.Visible := True;
    lblProcesoGeneral.Caption := MSG_VALIDANDO;

    try
        cdsRechazos.DisableControls;
        //valido que la empresa informada en el archivo sea valida
        CodigoEmpresa := ObtenerCodigoEmpresa( txtOrigen.Text);
        if CodigoEmpresa = -1 then begin
            MsgBoxErr(ERROR_CANT_LOAD_FILE,ERROR_EMPRESACODIGO, Caption, MB_ICONERROR);
            Exit;
        end;
        if Not ValidarCodigoEmpresaRecaudadora( CodigoEmpresa ) then begin
            MsgBoxErr(ERROR_CANT_LOAD_FILE,ERROR_EMPRESACODIGO, Caption, MB_ICONERROR);
            Exit;
        end;
        // Trato de Abrir el Archivo
        try
            strOrigen.LoadFromFile(txtOrigen.Text);
        except
            on e:exception do begin
                MsgBoxErr(ERROR_CANT_LOAD_FILE, e.Message, Caption, MB_ICONERROR);
                Exit;
            end;
        end;
        // Leo el registro de control
         if not LeerRegistroControl( strOrigen, REGISTRO_A_LEER, FechaGeneracion, CantidadRegistros, DescError) then begin
            MsgBoxErr(ERROR_CANT_READ_CONTROL_ROW, DescError, Caption, MB_ICONERROR);
            Exit;
         end;

        // Valido el reg. de Control Vs. Suma de Datos
        if CantidadRegistros <> (strOrigen.Count - 1 ) then begin
            // no concuerda... no proceso
            MsgBoxErr(ERROR_DEFAULT, ERROR_CONTROL_NO_VERIFICA, Caption, MB_ICONERROR);
            exit;
        end;

        //preparo la barra de avance
        pbProgreso.Max :=  strOrigen.Count + 5;
//        pbProgreso.StepIt;
        RegistrosRefresco := strOrigen.Count div 100;

        if RegistrosRefresco = 0 then RegistrosRefresco := 1;

        // Registro Inicio de Proceso en el Log
        if not RegistrarOperacionEnLogInterface(DMConnections.BaseCAC, RO_MOD_INTERFAZ_ENTRADA_ACCIONES_SERBANC,
          ExtractFileNAme(txtOrigen.Text),UsuarioSistema,'',True,False,FechaGeneracion,
          0, FCodigoOperacionInterfase, DescError) then begin
            MsgBoxErr(ERROR_CANT_REGISTER_LOG,DescError, Caption, MB_ICONERROR);
            Exit;
        end;
        pbProgreso.StepIt;

        // Mientras el string list tenga datos
        i := 1;
        pbProgreso.Tag := pbProgreso.Position;
        While (i < strOrigen.Count) and not FCancelar do begin
            // Parseo un registro y lo guardo en ... donde?? lo proceso directamente? o no?
            AuxString := strOrigen.Strings[i];
            if not ParsearLinea( AuxString, FechaGeneracion, RegDetalle, DescError) then begin
                strErrores.Add(Format(ERROR_EN_LINEA, [i])+ DescError + ',( '+ auxString +')');
//                ReErrores.Lines.Text := strErrores.Text;
            end
            else begin
                  {if not GuardarDetalle(RegDetalle, FCodigoOperacionInterfase, DescError) then begin
                        if Trim(descError) = '' then begin
                             strErrores.Add(Format(ERROR_EN_LINEA, [i]) + 'el convenio no tiene comprobantes.,( '+ auxString +')');
                        end else begin
                            //Registro la excepcion en el log de operaciones
                            ActualizarObservacionLogOperaciones(DMConnections.BaseCAC, FCodigoOperacionInterfase, ERROR_CANNOT_SAVE_DATA + ' - ' + STR_LINE + IntToStr(i) + ' - ' + strOrigen[i] + ' - ' + DescError);
                            MsgBoxErr(ERROR_CANNOT_SAVE_DATA + CRLF + STR_LINE + IntToStr(i) + CRLF + strOrigen[i], DescError, Caption, MB_ICONERROR);
                            Exit;
                        end;
                  end;}

                  if not ValidarLinea(RegDetalle, FCodigoOperacionInterfase, DescError, CodigoEmpresa) then begin
                            //Registro la excepcion en el log de operaciones
                            //ActualizarObservacionLogOperaciones(DMConnections.BaseCAC, FCodigoOperacionInterfase, ERROR_CANNOT_SAVE_DATA + ' - ' + STR_LINE + IntToStr(i) + ' - ' + strOrigen[i] + ' - ' + DescError);
                            MsgBoxErr(ERROR_CANNOT_SAVE_DATA + CRLF + STR_LINE + IntToStr(i) + CRLF + strOrigen[i], DescError, Caption, MB_ICONERROR);
                            Exit;
                  end;
            end;
            // Paso al siguiente string
            inc(i);
            if i mod RegistrosRefresco = 0 then begin
                pbProgreso.Position := pbProgreso.tag + i;
                Application.ProcessMessages;
            end;
        end;
        pbProgreso.Position := pbProgreso.tag + i;
        Application.ProcessMessages;
        if FCancelar then begin
            //registro que el proceso fue cancelado
            //RegistrarCancelacionInterfaz(DMConnections.BaseCAC, FCodigoOperacionInterfase);
            MsgBox(MSG_PROCESS_CANCELLED_BY_USER, Caption, MB_ICONWARNING);
            Exit;
        end;
        pbProgreso.StepIt;
        try
            if not GuardarDetalle(FCodigoOperacionInterfase, DescError, CodigoEmpresa) then // SS 803
                Raise Exception.Create(DescError);
            {ActualizarComprobantesEnviadosActivos(FCodigoOperacionInterfase);
            ActualizarComprobantesEnviadosNoActivos(FCodigoOperacionInterfase);
            ActualizarComprobantesEnviadosDetalle(FCodigoOperacionInterfase);}
        except
            on E: Exception do begin
                //Registro la excepcion en el log de operaciones
                //ActualizarObservacionLogOperaciones(DMConnections.BaseCAC, FCodigoOperacionInterfase, ERROR_CANT_FINISH_OPERATION + ' ' + e.Message);
                MSgBoxErr(ERROR_CANT_FINISH_OPERATION, E.Message, Caption, MB_ICONERROR);
                Exit;
            end;
        end; // except


        // Registro los cambios en los Coprobantes Enviados.
//         if not ActualizarComprobantesEnviados(FCodigoOperacionInterfase, descError) then begin
//            DMConnections.BaseCAC.RollbackTrans;
//            MSgBoxErr(ERROR_CANT_FINISH_OPERATION, DescError, Caption, MB_ICONERROR);
//            Exit;
//         end;

        pbProgreso.StepIt;
        // Guardo los errores
        if not GuardarErrores(FCodigoOperacionInterfase, strErrores, DescError) then begin
            //Registro la excepcion en el log de operaciones
            //ActualizarObservacionLogOperaciones(DMConnections.BaseCAC, FCodigoOperacionInterfase, ERROR_CANT_FINISH_OPERATION + ' ' + DescError);
            MSgBoxErr(ERROR_CANT_FINISH_OPERATION, DescError, Caption, MB_ICONERROR);
            Exit;
        end;
        if strErrores.Count > 0 then begin
            lblErrores.items.Assign(strErrores);
        end;
        pbProgreso.StepIt;
        // Cierro el Log de Operaciones
        if not ActualizarLogOperacionesInterfaseAlFinal(DMConnections.BaseCAC, FCodigoOperacionInterfase, 'Proceso Finaliz� OK!', DescError) then begin
            //Registro la excepcion en el log de operaciones
            //ActualizarObservacionLogOperaciones(DMConnections.BaseCAC, FCodigoOperacionInterfase, ERROR_CANT_FINISH_OPERATION + ' ' + DescError);
            MSgBoxErr(ERROR_CANT_FINISH_OPERATION, DescError, Caption, MB_ICONERROR);
            Exit;
        end;
        pbProgreso.StepIt;

        // Muevo Archivos Procesados
		//MoverArchivoProcesado(Caption,FOrigen,FDestinoProcesados,True);						// SS_1174_EBA_20140310 
        MoverArchivoProcesado(Caption,FOrigen+FNameFile,FDestinoProcesados+'\'+FNameFile,True);	// SS_1174_EBA_20140310
        //Informo que el proceso termino Ok!
        MsgBox(MSG_END_OK, caption, MB_ICONINFORMATION);
        // Muestro Reporte
        MostrarReporteFinalizacion;

    finally
        // habilitar botones y etceteras...
        cdsRechazos.EnableControls;
        FProcesando := False;
        HabilitarBotones;
        btnProcesar.Enabled := False;                                                  // SS_1174_EBA_20140310 
    end;
end;

{******************************** Function Header ******************************
Function Name: LeerRegistroControl
Author : ndonadio
Date Created : 11/08/2005
Description :   Lee y valida el registro de control.
Parameters : SList: TStringList; IndexToRead: Integer; var FechaArchivo: TDateTime; var CantRegistros: Integer; var Error: AnsiString
Return Value : boolean

Revision : 1
    Author : pdominguez
    Date   : 03/06/2009
    Description : SS 803
            - Se cambi� la longitud de lectura del campo N�mero Registros
*******************************************************************************}
function TfrmAccionesSerbanc.LeerRegistroControl(SList: TStringList; IndexToRead: Integer;
  var FechaArchivo: TDateTime; var CantRegistros: Integer; var Error: AnsiString): boolean;
resourcestring
    ERROR_STR_IS_NOT_A_VALID_DATE = '%s no es una fecha v�lida.';
var
    aux: AnsiString;
begin
    Result := False;
    aux := SList.Strings[IndexToRead];
    if  not FechaHoraAAAAMMDDToDateTime(DMCOnnections.BaseCAC, Copy(aux,1,14), FechaArchivo, Error) then begin
        Error := Format(ERROR_STR_IS_NOT_A_VALID_DATE, [ Copy(aux,1,14)]) + CRLF + Error;
        Exit;
    end;
    try
        CantRegistros := StrToInt(Copy(aux,15,7)); // Rev. 1 (SS 803)
        Result := True;
    except
        on e:exception do begin
            Error := e.Message;
        end;
    end;
end;

function TfrmAccionesSerbanc.DescripcionError(unCodigoError: string): String;
begin
    Result := FErrorSinDescripcion;
    if cdsErrores.FindKey([unCodigoError]) then
        Result := cdsErrores.FieldByName('DescripcionError').asString;
end;

{******************************** Function Header ******************************
Function Name: ParsearLinea
Author : ndonadio
Date Created : 10/08/2005
Description : Parsea un string y almacena los datos en un record...
Parameters : Origen: AnsiString; Destino: TRegDetalle; Error: AnsiString
Return Revision : 1
    Author : vpaszkowicz
    Date : 10/06/2009
    Description :Agrego una validaci�n por tipo deudor.

Revision : 2
  Author      : ebaeza
  Date        : 10-03-2014
  Firma       : SS_1174_EBA_20140310
  Description : SS 1174
              -Agrega que obtenga la observaci�n  NO OBLIGATORIA
*******************************************************************************}
function TfrmAccionesSerbanc.ParsearLinea(Origen: AnsiString; FechaArchivo: TDateTime ;
                        var Destino: TRegDetalle; var Error: AnsiString): boolean;
resourcestring
    //ERROR_E004_NUMEROCONV_INVALIDO      =   'Error E004: El valor "%s" no es un n�mero de convenio v�lido';
    //ERROR_E005_FECHA_INVALIDA           =   'Error E005: El valor "%s" no es una fecha v�lida.';
    //ERROR_E014_NUMERORUT_INVALIDO       =   'Error E014: El valor "%s" no es un RUT v�lido.';
    //ERROR_E017_TIPODEUDOR_INVALIDO      =   'Error E017: "%s" No es un Tipo de Deudor V�lido';
    //ERROR_E019_TIPOCOBRANZA_INVALIDO    =   'Error E019: "%s" No es un Tipo de Cobranza V�lido';
    ERROR_E004_NUMEROCONV_INVALIDO      =   'Error E004: ';
    ERROR_E005_FECHA_INVALIDA           =   'Error E005: ';
    ERROR_E014_NUMERORUT_INVALIDO       =   'Error E014: ';
    ERROR_E019_TIPOCOBRANZA_INVALIDO    =   'Error E019: ';
const
    // ubicacion (inicio) y longitud de los campos
    U_TC    = 1;  // tipo cobranza
    L_TC    = 2;
    U_TD    = 3;  // tipo deudor
    L_TD    = 1;
    U_RUT   = 4;  // rut
    L_RUT   = 10;
    U_COD   = 14; // codigo cliente (num convenio)
    L_COD   = 17;
    U_ACT   = 31; //actividad o accion de cobranza
    L_ACT   = 2;
    U_FECHA = 33; // fecha de accion...
    L_FECHA = 8;
    U_OBS   = 41; // Observaci�n no obligatoria     //SS_1174_EBA_20140310
    L_OBS   = 200;                                  //SS_1174_EBA_20140310
var
    aux: AnsiString;
    Convenio: integer;
begin
    Result := False;
    // parseo los datos de a uno para poder aislar el tipo de error si lo hubiese.
    // Tipo de Cobranza
        Aux := Copy(Origen, U_TC, L_TC);
        if not ValidarTipoCobranza(Aux, Error) then
            Exit;
        try
            Destino.TipoCobranza := StrToInt(Aux);
        except
            Error :=  Format(ERROR_E019_TIPOCOBRANZA_INVALIDO + DescripcionError('E019'), [Aux]);
            Exit;
        end;
        // Tipo de Deudor
        Aux := Copy(Origen, U_TD, L_TD);
        //Ah� dentro tambi�n verifico que se pueda convertir a integer
        if not ValidarTipoDeudor(Aux, Error) then
            Exit;
        Destino.TipoDeudor := StrToInt(Aux);
    // RUT
        Aux := Copy(Origen, U_RUT, L_RUT);
        if not ValidarRUT(DMConnections.BaseCAC, Aux) then begin
            Error := FORMAT(ERROR_E014_NUMERORUT_INVALIDO + DescripcionError('E014'),[aux]);
            Exit;
        end;
        Destino.RUT := Aux;
    // Codigo de Cliente (nuestro N�mero de Convenio)
        Aux := Copy(Origen, U_COD, L_COD);
        if ObtenerCodigoConcesionariaNativa=CODIGO_VS then                      //SS_1147Q_NDR_20141202[??]
        begin                                                                   //SS_1147Q_NDR_20141202[??]
          Aux:=Copy(Aux, 6, 12);                                                //SS_1147Q_NDR_20141202[??]
        end;                                                                    //SS_1147Q_NDR_20141202[??]
        Destino.NumeroConvenio := Aux;
        Convenio := ObtenerCodigoConvenio(Aux, Error);
        if Convenio = 0 then  Exit; //si fallo salgo...
        Destino.CodigoConv := Convenio;
    // Codigo de Actividad de Cobranza
        Aux := Copy(Origen, U_ACT, L_ACT);
        if not ValidarTipoActividad(Aux, Error) then Exit;
        Destino.TipoAccion := StrToINt(Aux);
    // Fecha de Acci�n
        Aux := Copy(Origen, U_FECHA, L_FECHA);
        if not ValidarFechaAAAAMMDD(Aux) then begin
            Error :=Format( ERROR_E005_FECHA_INVALIDA +  DescripcionError('E005'), [aux]);
            Exit;
        end;
        Destino.FechaAccion := EncodeDate( StrToInt(Copy(Aux, 1, 4)), StrToInt(Copy(Aux, 5, 2)),
                                            StrToInt(Copy(Aux, 7, 2)));

    // Observacion NO obligatoria           //SS_1174_EBA_20140310
        Aux := Copy(Origen, U_OBS, L_OBS);  //SS_1174_EBA_20140310
        Destino.Observacion := Aux;         //SS_1174_EBA_20140310

        Result := True;
end;

{******************************** Function Header ******************************
Function Name: ObtenerCodigoConvenio
Author : ndonadio
Date Created : 10/08/2005
Description : Obtiene el codigo de convenio a partir del numero.
                Si no puede obtener retorna 0 y el error en Error
Parameters : NumeroConvenio: string    ; Error: AnsiString
Return Value : Integer
*******************************************************************************}
function TfrmAccionesSerbanc.ObtenerCodigoConvenio(NumeroConvenio: string; var Error: AnsiString): Integer;
resourcestring
    //ERROR_E004_EL_CONVENIO_NO_EXISTE    =   'Error E004: El valor "%s" no es un n�mero de convenio v�lido.';
    ERROR_E004_EL_CONVENIO_NO_EXISTE    =   'Error E004: ';
    SQL_REVISAR_CONVENIO_VS             = 'SELECT TOP 1 CodigoConvenio FROM Convenio WITH(NOLOCK) WHERE '   // SS_1284_CQU_20150527
                                        + 'RIGHT(REPLICATE(''0'', 12) + LTRIM(RTRIM(NumeroConvenio)), 12)'  // SS_1284_CQU_20150527
                                        + ' = RIGHT(REPLICATE(''0'', 12) + LTRIM(RTRIM(''%s'')), 12)';      // SS_1284_CQU_20150527
    SQL_REVISAR_CONVENIO_CN             = 'SELECT ISNULL(dbo.ObtenerCodigoConvenio(''%s''),0)';             // SS_1284_CQU_20150527
var
    Conv: Integer;
begin
    try
        //Conv := QueryGetValueInt(DMConnections.BaseCAC, FORMAT('SELECT ISNULL(dbo.ObtenerCodigoConvenio(''%s''),0)',[NumeroConvenio]));   // SS_1284_CQU_20150527
        if ObtenerCodigoConcesionariaNativa = CODIGO_VS then                                                                                // SS_1284_CQU_20150527
            Conv := QueryGetValueInt(DMConnections.BaseCAC, FORMAT(SQL_REVISAR_CONVENIO_VS, [NumeroConvenio]))                              // SS_1284_CQU_20150527
        else                                                                                                                                // SS_1284_CQU_20150527
            Conv := QueryGetValueInt(DMConnections.BaseCAC, FORMAT(SQL_REVISAR_CONVENIO_CN, [NumeroConvenio]));                             // SS_1284_CQU_20150527

        if Conv = 0 then begin
            Error := Format(ERROR_E004_EL_CONVENIO_NO_EXISTE + DescripcionError('E004'), [NumeroConvenio]);
        end;
        Result := Conv;
    except
        on e:exception do begin
            Result := 0;
            Error := e.Message;
        end;
    end;
end;
{******************************** Function Header ******************************
Function Name: ObtenerCodigoEmpresa
Author : mpiazza
Date Created : 28/05/2009
Description : obtengo el codigo de empresa del archivo, y si existe error retorna -1
Parameters : NombreArchivo: string
Return Value : integer

*******************************************************************************}
function TfrmAccionesSerbanc.ObtenerCodigoEmpresa(
  NombreArchivo: string): integer;
var
  sCadena: string;
begin

  try
      sCadena := Copy(ExtractFileName(NombreArchivo), 9, 2);
      result := strtoint(sCadena);
    except
      on e:exception do begin
          Result := -1;
      end;
  end;

end;

function TfrmAccionesSerbanc.ValidarTipoCobranza(TipoCobranzaStr: String;
  var Error: AnsiString): Boolean;
resourcestring
    ERROR_E019_TIPOCOBRANZA_INVALIDO    =   'Error E019: ';
var
    Cobranza: Integer;
begin
    Result := False;
    try
        //Busco en mi tablita temporal, para no acceder a la base.
        Cobranza := strtoint(TipoCobranzaStr);
        If not cdsTiposCobranza.FindKey([Cobranza]) then begin
            Error := Format(ERROR_E019_TIPOCOBRANZA_INVALIDO + DescripcionError('E019'), [TipoCobranzaStr]);
            Exit;
        end;
        Result := True;
    except
        on e:exception do begin
            Error := ERROR_E019_TIPOCOBRANZA_INVALIDO + e.Message;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: ValidarTipoActividad
Author : ndonadio
Date Created : 10/08/2005
Description : Valida el n�mero de actividad.
Parameters : Actividad: String; var Error: AnsiString
Return Value : Boolean
Revision : 1
    Author : vpaszkowicz
    Date : 17/03/2008
    Description : Si algo fall� retorno la descripci�n en texto.
*******************************************************************************}
function TfrmAccionesSerbanc.ValidarTipoActividad(Actividad: String;
  var Error: AnsiString): Boolean;
resourcestring
    ERROR_E002_ACTIVIDAD_NO_VALIDA = 'El valor "%s" no es un C�digo de Actividad V�lido';
var
    Act: Integer;
begin
    Result := False;
    try
        //Busco en mi tablita temporal, para no acceder a la base.
        act := strtoint(Actividad);
        If not cdsNombreAcciones.FindKey([Act]) then begin
            Error := Format(DescripcionError('E002'), [Act]);
            Exit;
        end;
        Result := True;
    except
        on e:exception do begin
            Error := Format(ERROR_E002_ACTIVIDAD_NO_VALIDA, [Actividad]) + CRLF + e.Message
        end;
    end;
end;

function TfrmAccionesSerbanc.ValidarTipoDeudor(deudorStr: String;
  var Error: AnsiString): Boolean;
resourceString
    ERROR_E017_TIPODEUDOR_INVALIDO      =   'Error E017: ';
var
    deudor: Integer;
begin
    Result := False;
    try
        deudor := strtoint(deudorStr);
        If not cdsTiposDeudores.FindKey([deudor]) then begin
            Error := Format(ERROR_E017_TIPODEUDOR_INVALIDO + DescripcionError('E017'), [deudorStr]);
            Exit;
        end;
        Result := True;
    except
        on e:exception do begin
            Error := Format(ERROR_E017_TIPODEUDOR_INVALIDO + DescripcionError('E017'), [DeudorStr]) + CRLF + e.Message
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: ValidarTiposDeudorSerbanc
Author : mpiazza
Date Created : 28/05/2009
Description : valida el tipo de deudor serbanc
Parameters : CodigoTipoDeudor: integer
Return Value : boolean

Revision : 1
    Author : pdominguez
    Date   : 29/05/2009
    Description : SS 803
            - Se a�adieron los par�metros Motivo y ErrorProceso
*******************************************************************************}
function TfrmAccionesSerbanc.ValidarTiposDeudorSerbanc(CodigoTipoDeudor: integer; var Motivo, ErrorProceso: AnsiString): boolean;
ResourceString
    ERROR_E017_TIPO_DEUDOR_INVALIDO = 'Error E017: ';
var
    Conv: Integer;

begin
    try
        Conv := QueryGetValueInt(DMConnections.BaseCAC, FORMAT('SELECT ISNULL(dbo.ValidarTiposDeudorSerbanc(%d),0)',[CodigoTipoDeudor]));
        Result := Conv > 0;
        if not Result then
            Motivo := Format(ERROR_E017_TIPO_DEUDOR_INVALIDO + DescripcionError('E019'), [IntToStr(Conv)]);
    except
        on e:exception do begin
            ErrorProceso := e.Message;
            Result := False;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: ValidarAccion
Author :
Date Created :
Parameters : RegDetalle: TRegDetalle; var Motivo: string; var ErrorProceso: string
Return Value : Boolean
Description :

Revision : 1
    Author : pdominguez
    Date : 29/05/2009
    Description : SS 803
            - Se a�ade el par�metro CodigoEmpresaCobranza a la funci�n.
            - Se a�ade la asignaci�n al par�metro CodigoEmpresaCobranza al SP
            spValidarAccionCobranza.

Revision : 2
  Author : ebaeza
  Date : 11-03-2014
  firma : SS_1174_EBA_20140310
  Description: SS 1174
          - Se agrega la asignacion el parametro CodigoTipoCobranza al SP
            spValidarAccionCobranza.
       21/03/2014
          -Se eliminan las validaciones de duplicidad, por solicitud de MVI v�a correo, es todo lo contrario de la SS_1174.
          -Se crea nueva firma SS_1174_EBA_20140321, para diferenciar este cambio, no menor
*******************************************************************************}
{function TfrmAccionesSerbanc.ValidarAccion(RegDetalle: TRegDetalle; var Motivo: string; var ErrorProceso: string; CodigoEmpresaCobranza: Integer):Boolean;           // SS_1174_EBA_20140321
begin                                                                                                                                                                 // SS_1174_EBA_20140321
    Result := False;                                                                                                                                                  // SS_1174_EBA_20140321
    try                                                                                                                                                               // SS_1174_EBA_20140321
        spValidarAccionCobranza.Parameters.Refresh;                                                                                                                   // SS_1174_EBA_20140321
        spValidarAccionCobranza.Parameters.ParamByName('@CodigoConvenio').Value := RegDetalle.CodigoConv;                                                             // SS_1174_EBA_20140321
        spValidarAccionCobranza.Parameters.ParamByName('@FechaAccion').Value := TDateTime(RegDetalle.FechaAccion);                                                    // SS_1174_EBA_20140321
        spValidarAccionCobranza.Parameters.ParamByName('@CodigoActividadCobranza').Value := RegDetalle.TipoAccion;                                                    // SS_1174_EBA_20140321
        spValidarAccionCobranza.Parameters.ParamByName('@DescripcionError').Value := '';                                                                              // SS_1174_EBA_20140321
        spValidarAccionCobranza.Parameters.ParamByName('@CodigoEmpresaCobranza').Value := CodigoEmpresaCobranza;                                                      // SS_1174_EBA_20140321
        spValidarAccionCobranza.Parameters.PAramByName('@CodigoTipoCobranza').Value := RegDetalle.TipoCobranza;      //SS_1174_EBA_20140310                           // SS_1174_EBA_20140321
        spValidarAccionCobranza.ExecProc;                                                                                                                             // SS_1174_EBA_20140321
        Motivo := spValidarAccionCobranza.Parameters.ParamByName('@DescripcionError').Value;                                                                          // SS_1174_EBA_20140321
        Result := (Trim(Motivo) = '');                                                                                                                                // SS_1174_EBA_20140321
    except                                                                                                                                                            // SS_1174_EBA_20140321
        on e: exception do begin                                                                                                                                      // SS_1174_EBA_20140321
            ErrorProceso := 'Error grave: ' + e.Message;                                                                                                              // SS_1174_EBA_20140321
        end;                                                                                                                                                          // SS_1174_EBA_20140321
    end;                                                                                                                                                              // SS_1174_EBA_20140321
end;}                                                                                                                                                                 // SS_1174_EBA_20140321

{******************************** Function Header ******************************
Function Name: ValidarCodigoEmpresaRecaudadora
Author : mpiazza
Date Created : 28/05/2009
Description : valida el CodigoEmpresaRecaudadora
Parameters : CodigoTipoDeudor: integer
Return Value : boolean
*******************************************************************************}
function TfrmAccionesSerbanc.ValidarCodigoEmpresaRecaudadora(
  CodigoEmpresa: integer): boolean;
var
    Conv: Integer;
begin
    try
        Conv := QueryGetValueInt(DMConnections.BaseCAC, FORMAT('SELECT ISNULL(dbo.ValidarEmpresasRecaudadoras(%d),0)',[CodigoEmpresa]));
        Result := Conv > 0;
    except
        on e:exception do begin
            Result := false;
        end;
    end;

end;

function TfrmAccionesSerbanc.AgregarAccionRechazada(RegDetalle: TRegDetalle; CodigoOperacion: integer; Motivo: AnsiString):Boolean;
var
    NombreAccion: string;
begin
    Result := False;
    try
        if cdsNombreAcciones.FindKey([RegDetalle.TipoAccion]) then
            NombreAccion := cdsNombreAcciones.FieldByName('NombreAccion').asString;
        cdsRechazos.AppendRecord([RegDetalle.NumeroConvenio, DateTimeToStr(RegDetalle.FechaAccion), RegDetalle.TipoAccion, Motivo, NombreAccion]);
        spRechazarAccion.Parameters.Refresh;
        spRechazarAccion.Parameters.ParamByName('@CodigoOperacionInterfase').value := CodigoOperacion;
        spRechazarAccion.Parameters.ParamByName('@CodigoConvenio').value := RegDetalle.CodigoConv;
        spRechazarAccion.Parameters.ParamByName('@CodigoActividadCobranza').value := RegDetalle.TipoAccion;
        spRechazarAccion.Parameters.ParamByName('@FechaAccion').value := TDateTime(RegDetalle.FechaAccion);
        spRechazarAccion.Parameters.ParamByName('@Motivo').value := Motivo;
        spRechazarAccion.ExecProc;
        Result := True;
    except
        on e: exception do begin
            Result := False;
        end;
    end;
end;
{******************************** Function Header ******************************
Function Name: ValidarLinea
Author : ?????
Date Created :
Description : Valida linea por linea, todos los datos que ella contenga
Parameters : RegDetalle: TRegDetalle; CodigoOperacion: Integer;
    var ErrorProceso: AnsiString
Return Value : boolean

Revision: 1
Author: mpiazza
Date:   28/05/2009
Description: ref ss-803 se agregan validaciones de rut, convenio, TiposActividadesCobranzasSerbanc

Revision: 2
Author: pdominguez
Date:   28/05/2009
Description: SS 803
        - Se a�ade el par�metro CodigoEmpresaCobranza a la funci�n.

Revision: 3
Author: ebaeza
Date: 11/03/20104
firma: SS_1174_EBA_20140310
Description: SS 1174
          - Se a�ade el campo TipoCobranza a la validacion del ClienDataset cdsArchivo
       21/03/2014
          -Se eliminan las validaciones de duplicidad, por solicitud de MVI v�a correo, es todo lo contrario de la SS_1174.
          -Se crea nueva firma SS_1174_EBA_20140321, para diferenciar este cambio, no menor
          -La funcion ValidarAccion se comenta completa
          -La funcion ValidarLinea se comenta la validacion de duplicidad que hacia con el objeto cdsArchivo

          1-Se elimina objeto de tipo TADOStoredProc llamado spValidarAccionCobranza. (El codigo relacionado a este objeto en el archivo FAccionesSerbanc.pas esta comentado)
          2-Modificacion del indice del objeto cdsArchivo se cambia de true a false las propiedades ixPrimary e ixUnique.
*******************************************************************************}
function TfrmAccionesSerbanc.ValidarLinea(RegDetalle: TRegDetalle; CodigoOperacion: Integer;
    var ErrorProceso: AnsiString; CodigoEmpresaCobranza: Integer): Boolean;
resourceString
    MSG_MOTIVO_DUPLICADO = 'L�nea del archivo duplicada';
    MSG_MOTIVO_ERROR_GRABAR_RECHAZO = 'Error grave al crear un registro de rechazo por duplicado en el archivo � error de parseo';
    MSG_MOTIVO_RUT_INVALIDO = 'no contiene un n�mero de RUT v�lido';
    MSG_MOTIVO_CONVENIO_INVALIDO = 'no contiene un n�mero de convenio v�lido';
    MSG_MOTIVO_TIPODEUDOR_INVALIDO = 'C�digo de TipoDeudor no es v�lido';
    MSG_MOTIVO_FECHAACCION_INVALIDO = 'Fecha Acci�n no es v�lido';
var
    Motivo: AnsiString;
begin
    Result := False;
    Motivo := '';
    ErrorProceso := '';

    {//if cdsArchivo.FindKey([RegDetalle.CodigoConv,RegDetalle.FechaAccion,RegDetalle.TipoAccion]) then							            // SS_1174_EBA_20140310       // SS_1174_EBA_20140321
      if cdsArchivo.FindKey([RegDetalle.CodigoConv,RegDetalle.FechaAccion,RegDetalle.TipoAccion,RegDetalle.TipoCobranza]) then    // SS_1174_EBA_20140310     // SS_1174_EBA_20140321
          //Si encontramos una repetici�n no lo guardamos                                                                                                     // SS_1174_EBA_20140321
          //Inc(CantidadRechazos);                                                                                                                            // SS_1174_EBA_20140321
          Motivo := DescripcionError('R003')                                                                                                                  // SS_1174_EBA_20140321
      else begin}                                                                                                                                             // SS_1174_EBA_20140321

        //if ValidarAccion(RegDetalle, Motivo, ErrorProceso, CodigoEmpresaCobranza) then                                                                      // SS_1174_EBA_20140321

           //en la siguiente validacion se cambia la forma de validar para reutilizar codigo ya existente
           if FFechaProcesamientoActual > RegDetalle.FEchaAccion then begin
                        //Lo agrego a la lista.
                cdsArchivo.AppendRecord([RegDetalle.CodigoConv, RegDetalle.FechaAccion,
                  RegDetalle.TipoAccion, RegDetalle.TipoCobranza, RegDetalle.TipoDeudor,
                  RegDetalle.Observacion,	                                                                                                                    //SS_1174_EBA_20140310
                  False])
           end else Motivo :=  MSG_MOTIVO_FECHAACCION_INVALIDO;

        // if ErrorProceso <> '' then Exit;                                                                                                                   // SS_1174_EBA_20140321

//    end;//cdsArchivo.FindKey                                                                                                                                // SS_1174_EBA_20140321


    if (Motivo <> '') then begin                                                                                                                              
        if not AgregarAccionRechazada(RegDetalle, CodigoOperacion, Motivo) then begin                                                                         
            ErrorProceso := MSG_MOTIVO_ERROR_GRABAR_RECHAZO;                                                                                                  
            Exit;                                                                                                                                             
        end;
    end;

    Result := True;
end;

{******************************** Function Header ******************************
Function Name: GuardarDetalle
Author : ndonadio
Date Created : 10/08/2005
Description : Guarda en el Detalle de Acciones vinculado al Log.
Parameters : RegDetalle: TRegDetalle; CodigoOperacion: Integer; var Error: AnsiString
Return Value : Boolean

Revision : 1
    Author : pdominguez
    Date   : 29/05/2009
    Description : SS 803
            - Se a�adi� el par�metro CodigoEmpresaCobranza.

Revision : 2
  Author  : ebaezao
  Date    : 10/03/2014
  Firma   : SS_1174_EBA_20140310
  Description: SS 1174
        - se agrega parametro Observaciones
*******************************************************************************}
//function TfrmAccionesSerbanc.GuardarDetalle(RegDetalle: TRegDetalle; CodigoOperacion: Integer;
function TfrmAccionesSerbanc.GuardarDetalle(CodigoOperacion: Integer; var Error: AnsiString; CodigoEmpresaCobranza: Integer): Boolean;
resourceString
    MSG_GRABANDO = 'Grabando acciones..';
    MSG_COMPLETO = 'Proceso finalizado.';
var
    RegistrosRefresco: Integer;
begin
    Result := False;
    try
        lblProcesoGeneral.Visible := True;
        lblProcesoGeneral.Caption := MSG_GRABANDO;
        pbProgreso.Min := 0;
        pbProgreso.Max :=  cdsArchivo.RecordCount;
        RegistrosRefresco := cdsArchivo.RecordCount div 100;
        if RegistrosRefresco = 0 then RegistrosRefresco := 1;

        Application.ProcessMessages;
        cdsArchivo.First;
        while not cdsArchivo.Eof do begin
            with spInsertarActividadCobranzaSerbanc, cdsArchivo do begin
                Parameters.ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacion;// int,
                Parameters.ParamByName('@CodigoConvenio').Value := FieldByName('CodigoConvenio').AsInteger;// int,
                Parameters.ParamByName('@CodigoTipoCobranza').Value := FieldByName('CodigoTipoCobranza').AsInteger;// int,
                Parameters.ParamByName('@CodigoActividadCobranza').Value := FieldByName('CodigoActividadCobranza').AsInteger;// int,
                Parameters.ParamByName('@FechaAccion').Value := FieldByName('FechaAccion').AsDateTime;// DateTime
                Parameters.ParamByName('@CodigoEmpresaCobranza').Value := CodigoEmpresaCobranza; // Rev. 1 (SS 803)
                Parameters.ParamByName('@Observaciones').Value := FieldByName('Observaciones').AsString; // SS_1174_EBA_20140310
                ExecProc;
                if Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
                    // el prc ejecuto, pero no hizo nada...
                    // la linea no era valida
                    Error := '';
                    Exit;
                end;
            end;
            cdsArchivo.Edit;
            cdsArchivo.FieldByName('Procesado').AsBoolean := True;
            cdsArchivo.Post;
            if cdsArchivo.RecNo mod RegistrosRefresco = 0 then begin
                pbProgreso.Position := cdsArchivo.RecNo;
                Application.ProcessMessages;
            end;
            cdsArchivo.Next;
        end;
        pbProgreso.Position := cdsArchivo.RecordCount;
        lblProcesoGeneral.Visible := True;
        lblProcesoGeneral.Caption := MSG_COMPLETO;
        Application.ProcessMessages;
        Result := True;
    except
        // si falla devolvemos error
         on e:exception do begin
            Error := e.Message;
            Exit;
         end;
    end;
end;


{******************************** Function Header ******************************
Function Name: GuardarErrores
Author : ndonadio
Date Created : 10/08/2005
Description : Mete los stringLists de errores en la tabla de errores interfases.
Parameters : FCodigoOperacionInterfase: Integer; strErrores: TStringList; var Error: AnsiString
Return Value : Boolean
*******************************************************************************}
function TfrmAccionesSerbanc.GuardarErrores(FCodigoOperacionInterfase: Integer;
  strErrores: TStringList; var Error: AnsiString): Boolean;
resourcestring
    ERROR_LOG_ERRORES   =   'No se puede insertar en el Log de Errores';
    MSG_GRABANDO = 'Grabando Errores..';

var
    i, RegistroErrores: integer;
begin
    Result := False;
    lblProcesoGeneral.Caption := MSG_GRABANDO;
    pbProgreso.Position := 0;
    pbProgreso.Max := strErrores.Count;
    RegistroErrores := strErrores.Count div 100;
    if RegistroErrores = 0 then RegistroErrores := 1;
    // ciclo po el stringlist de errores
    for i := 0 to strErrores.Count - 1  do begin
        if not AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacionInterfase, strErrores.Strings[i]) then begin
            Error := ERROR_LOG_ERRORES;
            Exit;
        end;
        if i mod RegistroErrores = 0 then begin
            pbProgreso.Position := i;
            Application.ProcessMessages;
        end;

    end;
    pbProgreso.Position := strErrores.Count;
    Application.ProcessMessages;
    Result := True;
end;


{******************************** Function Header ******************************
Function Name: HabilitarBotones
Author : ndonadio
Date Created : 05/08/2005
Description : Habilita o deshabilita botones basandose en si est� procesando o no.
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmAccionesSerbanc.HabilitarBotones;
begin
    btnSalir.Enabled := NOT FProcesando;
    btnProcesar.Enabled := NOT FProcesando;
    pnlAvance.Visible := FProcesando;
    btnCancelar.Enabled := FProcesando;
    //lblReferencia.Caption := '';
    pbProgreso.Position := 0;
    FCancelar := False;
end;

procedure TfrmAccionesSerbanc.btnBuscarArchivoClick(Sender: TObject);
    {******************************** Function Header ******************************
    Function Name: ObtenerFiltro
    Author : ndonadio
    Date Created : 10/08/2005
    Description :
    Parameters : None
    Return Value : AnsiString

    Revision : 1
      Author  : ebaeza
      Date : 10/03/2014
      firma: SS_1174_EBA_20140310
      description : se corrige ortografia
      -----------------------------------
      Date: 13/03/2014
      description: Se obtiene el nombre del archivo asignandolo a la variable privada
                    FNameFile y se obtiene el path el que se asigna a la variable
                    privada FOrigen
              17/03/2014
                -Se corrigue comparacion en el IF donde informa que el archivo ya ha sido
                procesado y si quiero procesarlo o no, cuando la respuesta es NO, igual
                habilitaba el boton procesar.
                Se agrega al principio la deshabilitacion del boton procesar
    *******************************************************************************}
    Function ObtenerFiltro: AnsiString;
    Const
		//FILE_TITLE	   = 'Rendicion ed Acciones Serbanc|';	//SS_1174_EBA_20140310
        FILE_TITLE	   = 'Rendicion de Acciones Serbanc|';  	//SS_1174_EBA_20140310
        FILE_NAME 	   = 'ACN_COB_';
        FILE_EXTENSION = '.txt';
        FILE_NAME_VS   = 'AVS_COB_';                                        // SS_1284_CQU_20150527
    begin
            //Result:= FILE_TITLE + FILE_NAME + '*' + FILE_EXTENSION;       // SS_1284_CQU_20150527
            if ObtenerCodigoConcesionariaNativa = CODIGO_VS then            // SS_1284_CQU_20150527
                Result:= FILE_TITLE + FILE_NAME_VS + '*' + FILE_EXTENSION   // SS_1284_CQU_20150527
            else                                                            // SS_1284_CQU_20150527
                Result:= FILE_TITLE + FILE_NAME + '*' + FILE_EXTENSION;     // SS_1284_CQU_20150527
    end;

resourcestring
    MSG_ERROR = 'Error';
  	MSG_ERROR_FILE_DOES_NOT_EXIST   = 'El archivo %s no existe';
  	MSG_ERROR_INVALID_FILENAME 		= 'El nombre del archivo %s es inv�lido';
  	MSG_FILE_ALREADY_PROCESSED 		= 'El archivo %s ya fue procesado' + CRLF + '�Desea reprocesar el archivo?';
var
    Filtro: string;
    resp: Integer;         // SS_1174_EBA_20140310
begin

    btnProcesar.Enabled := False;                               // SS_1174_EBA_20140310
    if not cdsRechazos.IsEmpty then cdsRechazos.EmptyDataSet;   // SS_1174_EBA_20140310
    lblErrores.items.clear;                                     // SS_1174_EBA_20140310

    //obtengo el filtro a aplicar a este tipo de archivos
    Filtro := ObtenerFiltro;

    Opendialog.InitialDir := txtOrigen.Text;;
  	OpenDialog.FileName := '';
    OpenDialog.Filter := Filtro; //solo veo este tipo de archivos

    if opendialog.execute then begin
      txtOrigen.text:=UpperCase(Opendialog.Filename);

      if not FileExists(txtOrigen.text) then begin
        MsgBox( Format ( MSG_ERROR_FILE_DOES_NOT_EXIST, [txtOrigen.text]), MSG_ERROR, MB_ICONERROR );
        Exit;
      end;

      if  VerificarArchivoProcesado(DMConnections.BaseCAC, txtOrigen.text) then begin
        //MsgBox (Format(MSG_FILE_ALREADY_PROCESSED, [ExtractFileName(txtOrigen.text)]),Caption, MB_ICONSTOP);
        resp := MsgBox(Format(MSG_FILE_ALREADY_PROCESSED, [ExtractFileName(txtOrigen.text)]), Caption, MB_ICONQUESTION or MB_YESNO); // SS_1174_EBA_20140310
        if not (resp = IDYES) then                                                                                                   // SS_1174_EBA_20140310
            Exit;
      end;

      FNameFile := ExtractFileName(Opendialog.Filename);    //SS_1174_EBA_20140310
      FOrigen := ExtractFilePath(Opendialog.Filename);      //SS_1174_EBA_20140310

      btnProcesar.Enabled := True;
	  end;
end;

procedure TfrmAccionesSerbanc.btnCancelarClick(Sender: TObject);
begin
    FCancelar := True;
end;

{******************************** Function Header ******************************
Function Name:
Author : ndonadio
Date Created : 18/08/2005
Description :   Lanza el reporte de finalizacion.
Parameters : Not available
Return Value : Not available

date: 13/03/2014
author: ebaeza
firma: SS_1174_EBA_20140310
description: SS_1174
    Se agrega el resourcestring MSG_CANCELED
*******************************************************************************}
procedure TfrmAccionesSerbanc.MostrarReporteFinalizacion;
resourcestring
    ERROR_REPORT    = 'No se puede mostrar el reporte';
    MSG_CANCELED = 'Ejecuci�n del reporte cancelada por el usuario';          // SS_1174_EBA_20140310
var
    fr: TfrmReporteAccionesSerbanc;
    descError: AnsiString;
begin
    try
        Application.Createform(TfrmReporteAccionesSerbanc, fr);
        if not fr.MostrarReporte(FCodigoOperacionInterfase, Caption, descError) then begin
          if descError = '' then begin                                                         // SS_1174_EBA_20140310
            //Informo que el proceso fue cancelado por el usuario                              // SS_1174_EBA_20140310
            MsgBox(MSG_CANCELED, Caption, MB_ICONINFORMATION);                                 // SS_1174_EBA_20140310
          end else begin                                                                       // SS_1174_EBA_20140310
            MsgBoxErr(ERROR_REPORT, descError, Caption, MB_ICONERROR);                         // SS_1174_EBA_20140310
            Exit;                                                                              // SS_1174_EBA_20140310
          end;                                                                                 // SS_1174_EBA_20140310
        end else begin
            fr.Release;
        end;
    except
        on e:exception do begin
            MsgBoxErr(ERROR_REPORT, e.Message, Caption, MB_ICONERROR);
            Exit;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: ActualizarComprobantesEnviadosActivos
Author :
Date Created :
Description :
Parameters : CodigoOperacion: Integer
Return Value : None
Revision : 1
    Author : vpaszkowicz
    Date : 28/01/2008
    Description : Adapto el procedimiento para que se ejecute por bloques
*******************************************************************************}
procedure TfrmAccionesSerbanc.ActualizarComprobantesEnviadosDetalle(CodigoOperacion: Integer);
var
    sp: TADOStoredProc;
    Finalizo: Boolean;
begin
    sp := TADOStoredProc.Create(nil);
    Finalizo := False;
    try
        sp.Connection       := spInsertarActividadCobranzaSerbanc.Connection;
        sp.CommandTimeout   := 180;
        sp.ProcedureName    := 'ActualizarComprobantesEnviadosAccionesSerbancDetalle';
        sp.Parameters.Refresh;
        sp.Parameters.ParamByName('@Finalizo').Direction := pdInputOutput;
        sp.Parameters.ParamByName('@Finalizo').Value := False;
        while not Finalizo do begin
            sp.Parameters.ParamByName('@CodigoOperacion').Value := CodigoOperacion;
            sp.ExecProc;
            Finalizo := sp.Parameters.ParamByName('@Finalizo').Value;
        end;
    finally
        sp.Free;
    end; // finally
end;

{******************************** Function Header ******************************
Function Name: ActualizarComprobantesEnviadosActivos
Author :
Date Created :
Description :
Parameters : CodigoOperacion: Integer
Return Value : None
Revision : 1
    Author : vpaszkowicz
    Date : 28/01/2008
    Description : Adapto el procedimiento para que se ejecute por bloques
*******************************************************************************}
procedure TfrmAccionesSerbanc.ActualizarComprobantesEnviadosActivos(CodigoOperacion: Integer);
var
    sp: TADOStoredProc;
    Finalizo: Boolean;
begin
    sp := TADOStoredProc.Create(nil);
    Finalizo := False;
    try
        sp.Connection       := spInsertarActividadCobranzaSerbanc.Connection;
        sp.CommandTimeout   := 180;
        sp.ProcedureName    := 'ActualizarComprobantesEnviadosAccionesSerbancActivos';
        sp.Parameters.Refresh;
        sp.Parameters.ParamByName('@Finalizo').Direction := pdInputOutput;
        sp.Parameters.ParamByName('@Finalizo').Value := False;
        while not Finalizo do begin
            sp.Parameters.ParamByName('@CodigoOperacion').Value := CodigoOperacion;
            sp.ExecProc;
            Finalizo := sp.Parameters.ParamByName('@Finalizo').Value;
        end;
    finally
        sp.Free;
    end; // finally
end;

{******************************** Function Header ******************************
Function Name: ActualizarComprobantesEnviadosNoActivos
Author :
Date Created :
Description :
Parameters : CodigoOperacion: Integer
Return Value : None
Revision : 1
    Author : vpaszkowicz
    Date : 28/01/2008
    Description : Adapto el procedimiento para que se ejecute por bloques
*******************************************************************************}
procedure TfrmAccionesSerbanc.ActualizarComprobantesEnviadosNoActivos(CodigoOperacion: Integer);
var
    sp: TADOStoredProc;
    Finalizo: Boolean;
begin
    sp := TADOStoredProc.Create(nil);
    Finalizo := False;
    try
        sp.Connection       := spInsertarActividadCobranzaSerbanc.Connection;
        sp.CommandTimeout   := 180;
        sp.ProcedureName    := 'ActualizarComprobantesEnviadosAccionesSerbancNoActivos';
        sp.Parameters.Refresh;
        sp.Parameters.ParamByName('@Finalizo').Direction := pdInputOutput;
        sp.Parameters.ParamByName('@Finalizo').Value := False;
        while not Finalizo do begin
            sp.Parameters.ParamByName('@CodigoOperacion').Value := CodigoOperacion;
            sp.ExecProc;
            Finalizo := sp.Parameters.ParamByName('@Finalizo').Value;
        end;
    finally
        sp.Free;
    end; // finally
end;

end.
