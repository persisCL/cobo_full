{-----------------------------------------------------------------------------
 File Name: SeleccionProcFacturacion.pas
 Author:    ndonadio
 Date Created: 01/07/2005
 Language: ES-AR
 Description: Permite seleccionar un proceso de facturaci�n especifico y envia
                a imprimir las NK de dicho Proceso.

 Firma        : SS_660_NDR_20131112
 Descripcion  : Se debe informar al lado de cantidad de comprobantes que el proceso es (Por Morosidad)
 -----------------------------------------------------------------------------}

unit SeleccionProcFacturacion;

interface

uses
    // Base de Datos
    DMConnection, UtilDB,
    // Comunes
    PeaTypes, PeaProcs, ComunesInterfaces, Util, UtilProc,
    // GEneral
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, ExtCtrls, StdCtrls, ListBoxEx, DBListEx, Validate,
  DateEdit, DateUtils, strUtils, ImgList;                                          //SS_660_NDR_20131112

type
  TdlgSeleccionProcFacturacion = class(TForm)
    gbRangoFechas: TGroupBox;
    deFDesde: TDateEdit;
    deFHasta: TDateEdit;
    Label2: TLabel;
    Label3: TLabel;
    dblProcs: TDBListEx;
    btnCancelar: TButton;
    btnAceptar: TButton;
    Bevel1: TBevel;
    Label1: TLabel;
    lblFecha: TLabel;
    Label4: TLabel;
    lblNumProc: TLabel;
    lblSeleccion: TLabel;
    lblNKImpresas: TLabel;
    lblNKNOIMpresas: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Bevel2: TBevel;
    spObtenerProcesos: TADOStoredProc;
    dsObtenerProcesos: TDataSource;
    Label8: TLabel;
    chkSoloPendientes: TCheckBox;
    lbl2: TLabel;
    chkImprimirElectronicas: TCheckBox;
    lblCantidadTotal: TLabel;
    lblNoTerminadasCaption: TLabel;
    lblNoTerminadas: TLabel;
    lblNoSePuede: TLabel;
    ilCheck: TImageList;															  //SS_660_NDR_20131112
    procedure dblProcsClick(Sender: TObject);
    procedure deFDesdeChange(Sender: TObject);
    procedure chkSoloPendientesClick(Sender: TObject);
    procedure chkImprimirElectronicasClick(Sender: TObject);
    procedure dblProcsDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn;      //SS_660_NDR_20131112
      Rect: TRect; State: TOwnerDrawState; var Text: string; var TxtRect: TRect;      //SS_660_NDR_20131112
      var ItemWidth: Integer; var DefaultDraw: Boolean);                              //SS_660_NDR_20131112
  private
    { Private declarations }
    FchkElectronicasHaveAccess,
    FchkPendientesHaveAccess    : boolean;
    procedure ClearDataLabels;
    procedure CalcularSeleccion;
  public
    { Public declarations }
    FNumeroProceso: Integer;
    FFechaProceso: AnsiString;
    FMinTotal, FMaxTotal, FCantTotal, FNoTerminadas,
    FMinPrn, FMaxPrn, FCantPrn,
    FMinNoPrn, FMaxNoPrn, FCantNoPrn: Integer ;
    FMinEMail_I, FMaxEMail_I, FCantEmail_I,
    FMinEMail_NI, FMaxEMail_NI, FCantEmail_NI: Integer;
    FSelTotal, FSelFirst, FSelLast: Integer;
    function Inicializar: Boolean;
  end;

resourcestring
    LBL_NKS_NUMBERS   =   'entre el N� %d al N� %d, Total: %d Notas.';
    LBL_TOTAL_NKS     =   'Total: %d; (Enviadas por e-mail: %d)';
var
  dlgSeleccionProcFacturacion: TdlgSeleccionProcFacturacion;

implementation

{$R *.dfm}
{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    ndonadio
  Date Created: 01/07/2005
  Description: Inicializa el formulario, las fechas, etc.
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TdlgSeleccionProcFacturacion.Inicializar: Boolean;
resourcestring
    ERROR_NOT_INITIALIZE    =   'Error al inicializar el form.';
begin
    Result := False;

    //centro el formlario
	CenterForm(Self);

    ClearDataLabels;
    FNumeroProceso := 0;

    FchkElectronicasHaveAccess := ExisteAcceso('imprimir_nk_envio_electronico');
    FchkPendientesHaveAccess := ExisteAcceso('forzar_reimpresion_nk');

    chkSoloPendientes.Enabled := FchkPendientesHaveAccess;
    chkImprimirElectronicas.Visible := FchkElectronicasHaveAccess;

    try
        // seteo fechas por defecto...
        // Hasta HOY
        deFHasta.Date := NowBase(DMConnections.BaseCAC);
        //si estamos a mitad de mes o mas... muestro el mes en curso...DESDE = 01 - mes en curso
        //sino, muestro el mes anterior y lo que va de este..01-mes anterior
        if DaysBetween(deFHasta.Date,StartOfTheMonth(deFHAsta.date)) >= 15 then
            deFDesde.Date := StartOfTheMonth(deFHasta.Date)
        else deFDesde.Date := StartOfTheMonth(deFHasta.Date-15); //como estamos a menos del dia 15, (hoy-15) es el mes anterior...

        // abro el dataset...
        With spObtenerProcesos.Parameters do begin
            ParamByName('@FDesde').Value := deFDesde.Date;
            ParamByName('@FHasta').Value := deFHasta.Date;
        end;

        spObtenerProcesos.Open;
        dblProcsClick(self);

        Result := True;
    except
        on e:exception do MsgBoxErr(ERROR_NOT_INITIALIZE,e.Message,caption,MB_ICONERROR);
    end;

end;

{-----------------------------------------------------------------------------
  Function Name: dblProcsClick
  Author:    ndonadio
  Date Created: 01/07/2005
  Description: Muestra los datos del proceso de facturacion seleccionado
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TdlgSeleccionProcFacturacion.dblProcsClick(Sender: TObject);
resourcestring
MSG_THERE_IS_NOT_ANY_NK             = 'No hay Comprobantes';
MSG_THERE_IS_NOT_ANY_PRINTED_NK     = 'No hay Comprobantes Impresos';
MSG_THERE_IS_NOT_ANY_NOT_PRINTED_NK = 'No hay Comprobantes Pendientes de Imprimir';
MSG_THERE_IS_NOT_ANY_NOT_ELECTRONIC_NK = 'No hay Comprobantes marcados para Envio Electr�nico';
begin
// actualizo los labels...
    if NOT(spObtenerProcesos.Active) OR (spObtenerProcesos.IsEmpty) then begin
        FNumeroProceso  := 0;
        FFechaProceso   := '';
        FMinPrn         := 0;
        FMaxPrn         := 0;
        FCantPrn        := 0;
        FMinNoPrn       := 0;
        FMaxNoPrn       := 0;
        FCantNoPrn      := 0;
        FMinTotal       := 0;
        FMaxTotal       := 0;
        FCantTotal      := 0;
        FNoTerminadas   := 0;
        lblFecha.Caption := '';
        lblNumProc.Caption := '';
        lblNKImpresas.Caption := '';
        lblNKNOImpresas.Caption := '';
        lblSeleccion.Caption := '';
        lblCantidadTotal.Caption := '';
        FNumeroProceso := 0;
        btnAceptar.Enabled := False;
        chkSoloPendientes.Enabled := False;
        chkImprimirElectronicas.Enabled := False;
    end
    else begin
        with spObtenerProcesos do begin
            FNumeroProceso  := FieldByName('NumeroProcesoFacturacion').asInteger;
            FFechaProceso   := FormatDateTime('dd/mm/yyyy HH:nn:ss', FieldByName('FechaHoraInicio').AsDateTime);
            FMinPrn         := FieldByName('Primer_NK_Impresa').asInteger;
            FMaxPrn         := FieldByName('Ultima_NK_Impresa').asInteger;
            FCantPrn        := FieldByName('Cantidad_Impresos').asInteger;
            FMinNoPrn       := FieldByName('Primer_NK_NoImpresa').asInteger;
            FMaxNoPrn       := FieldByName('Ultima_NK_NoImpresa').asInteger;
            FCantNoPrn      := FieldByName('CantidadNoImpresos').asInteger;
            FMinEmail_NI    := FieldByName('Primer_NK_ElectronicaNI').asInteger;
            FMaxEmail_NI    := FieldByName('Ultima_NK_ElectronicaNI').asInteger;
            FCantEmail_NI   := FieldByName('Cantidad_ElectronicasNI').asInteger;
            FMinEmail_I     := FieldByName('Primer_NK_ElectronicaI').asInteger;
            FMaxEmail_I     := FieldByName('Ultima_NK_ElectronicaI').asInteger;
            FCantEmail_I    := FieldByName('Cantidad_ElectronicasI').asInteger;
            FMinTotal       := FieldByName('Primer_NK').asInteger;
            FMaxTotal       := FieldByName('Ultima_NK').asInteger;
            FCantTotal      := FieldByName('Cantidad_Total').asInteger;
            FNoTerminadas   := FieldByName('NoTerminadas').asInteger;
        end;

        lblFecha.Caption := FFechaProceso;
        lblNumProc.Caption := IntToStr(FNumeroProceso);

        // actualizo los labels de cantidades...
        if  ( FCantTotal > 0) then lblCantidadTotal.Caption := Format(LBL_TOTAL_NKS,[FCantTotal, (FCantEmail_I+FCantEmail_NI)])
        else  lblCantidadTotal.Caption := MSG_THERE_IS_NOT_ANY_NK;

        if  ( FCantPrn > 0) then lblNKImpresas.Caption := Format(LBL_NKS_NUMBERS,[FMinPrn, FMaxPrn, FCantPrn])
        else  lblNKImpresas.Caption := MSG_THERE_IS_NOT_ANY_PRINTED_NK;

        if  ( FCantNoPrn > 0) then lblNKNOImpresas.Caption := Format(LBL_NKS_NUMBERS,[FMinNoPrn, FMaxNoPrn, FCantNoPrn])
        else  lblNKNOImpresas.Caption := MSG_THERE_IS_NOT_ANY_NOT_PRINTED_NK;

        if FNoTerminadas>0 then
        begin
          lblNoTerminadasCaption.Visible:=True;
          lblNoTerminadas.Visible:=True;
          lblNoSePuede.Visible:=True;
          lblNoTerminadasCaption.Caption:='Comprobantes Fiscales sin Terminar :';
          lblNoSePuede.Caption:='NO SE PUEDE ENVIAR ARCHIVO A JORDAN';
          lblNoTerminadas.Caption:=IntToStr(FNoTerminadas);
          btnAceptar.Visible:=False;
        end
        else
        begin
          lblNoTerminadasCaption.Visible:=False;
          lblNoTerminadas.Visible:=False;
          lblNoSePuede.Visible:=False;
          btnAceptar.Visible:=True;
        end;

        // calculo que es lo seleccionado
        CalcularSeleccion;

        // Habilito o no los check box si hay permisos...
        chkSoloPendientes.Enabled := FchkPendientesHaveAccess;
        chkImprimirElectronicas.Enabled := chkImprimirElectronicas.Visible and FchkElectronicasHaveAccess;
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: deFDesdeChange
  Author:    ndonadio
  Date Created: 01/07/2005
  Description: Ante el cambio de fechas en los combos, cierra y vulve a
                abrir el SP con los nuevos parametros.
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TdlgSeleccionProcFacturacion.deFDesdeChange(Sender: TObject);
resourceString
    MSG_INVALID_START_DATE  =   'La fecha ingresada es incorrecta. La fecha "Desde" debe ser anterior o igual a la fecha "Hasta"';
    MSG_INVALID_END_DATE    =   'La fecha ingresada es incorrecta. La fecha "Hasta" debe ser posterior o igual a la fecha "Desde"';
    MSG_INVALID_DATES       =   'El rango de fechas es invalido';
    MSG_ERROR_DATA_RETRIEVING =   'Error al obtener los datos';
begin

    if ActiveControl <> Sender then Exit;

    // valido los combos de fecha
    if TDateEdit(Sender).Name = 'deFHasta' then  begin
        if NOT ValidateControls([deFHasta],[(deFDesde.Date <= deFHasta.Date)],MSG_INVALID_DATES,[MSG_INVALID_END_DATE]) then Exit;
    end else
        if TDateEdit(Sender).Name = 'deFDesde' then begin
          if NOT ValidateControls([deFDesde],[(deFDesde.Date <= deFHasta.Date)],MSG_INVALID_DATES,[MSG_INVALID_START_DATE]) then Exit;
        end
        else Exit;

    try
        // abro el sp
        if spObtenerProcesos.Active then spObtenerProcesos.Close;
        With spObtenerProcesos.Parameters do begin
            ParamByName('@FDesde').Value := iif(deFDesde.IsEmpty, NULL, deFDesde.Date);
            ParamByName('@FHasta').Value := iif(deFHasta.IsEmpty, NULL, deFHasta.Date);
        end;
        spObtenerProcesos.Open;
        // Actualizo la lista...
        dblProcsClick(self);
    except
        on e:Exception do begin
            // si fallo muestro el error
            MsgBoxErr(MSG_ERROR_DATA_RETRIEVING, e.Message, Caption,MB_ICONERROR);
        end;
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: ClearDataLabels
  Author:    ndonadio
  Date Created: 01/07/2005
  Description: Limpia las caption de los labels cuyo nombre empieza con "lbl"
    (NOTA: Esto no se si esta bien, pero yo suelo ponerle nombre a los labels
    son modificados en ejecucion. Aquellos que solo son etiquetas (ej. "Nombre:")
    los dejo con el nombre default que de LabelXX... Se aceptan suferencias...
    si se estipula una convenci{on de nombres para diferenciar un label de otro
    este proc podria ponerse en una unit de las comunes...)
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TdlgSeleccionProcFacturacion.ClearDataLabels;
const
    DATA_LABEL_PREFIX = 'lbl';
var
    i: integer;
begin
    for i := 0 to ControlCount-1 do
    begin
        if LeftStr(Controls[i].Name,3) = DATA_LABEL_PREFIX then TLabel(Controls[i]).Caption := '';
    end;
end;

{******************************** Function Header ******************************
Function Name: CalcularSeleccion
Author : ndonadio
Date Created : 14/07/2005
Description : calcula los valores seleccionados y activa o no el boton aceptar.
Parameters : None
Return Value : None
*******************************************************************************}
procedure TdlgSeleccionProcFacturacion.CalcularSeleccion;
begin

    // inicializo valores con las NO Impresas
    FSelTotal := FCantNoPrn; // cantidad seleccionada
    FSelFirst := FMinNoPrn; // Minimo Seleccionado
    FSelLast  := FMaxNoPrn; // Maximo Seleccionado

    // sumo las ya impresas si corresponde
    if (not chkSoloPendientes.Checked) and (FCantPrn > 0 ) then begin
        FSelTotal := FSelTotal + FCantPrn;
        FSelFirst := iif((FMinPrn < FSelFirst) or (FSelFirst = 0), FMinPrn, FSelFirst);
        FSelLast  := iif(FMaxPrn > FSelLast , FMaxPrn, FSelLast);
    end;

    // sumo las electronicas si corresponde
    if chkImprimirElectronicas.Checked  and (FCantEmail_NI > 0 ) then begin
        FSelTotal := FSelTotal + FCantEmail_NI;
        FSelFirst := iif((FMinEmail_NI < FSelFirst) or (FSelFirst = 0), FMinEmail_NI, FSelFirst);
        FSelLast  := iif(FMaxEmail_NI > FSelLast, FMaxEmail_NI, FSelLast);
    end;

    if (not chkSoloPendientes.Checked) and (chkImprimirElectronicas.Checked) and (FCantEmail_I > 0 ) then begin
            FSelTotal := FSelTotal + FCantEmail_I;
            FSelFirst := iif((FMinEmail_I < FSelFirst) or (FSelFirst = 0), FMinEmail_I, FSelFirst);
            FSelLast  := iif(FMaxEmail_I > FSelLast, FMaxEmail_I, FSelLast);
    end;

    // Habilito el boton aceptar si tengo algo seleccionado...
    btnAceptar.Enabled := (FSelTotal > 0);

    // actualizo el label que muestra las cantidades...
    lblSeleccion.Caption := Format(LBL_NKS_NUMBERS,[FSelFirst, FSelLast, FSelTotal])

end;

{******************************** Function Header ******************************
Function Name: chkSoloPendientesClick
Author : ndonadio
Date Created : 14/07/2005
Description :   Selecciono o no imprimir las ya impresas
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TdlgSeleccionProcFacturacion.chkSoloPendientesClick(
  Sender: TObject);
begin
    CalcularSeleccion;
end;

{******************************** Function Header ******************************
Function Name: chkImprimirElectronicasClick
Author : ndonadio
Date Created : 14/07/2005
Description : Selecciono o no imprimir las electronicas
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TdlgSeleccionProcFacturacion.chkImprimirElectronicasClick(
  Sender: TObject);
begin
    CalcularSeleccion;
end;


//BEGIN : SS_660_NDR_20131112-------------------------------------------------------------
procedure TdlgSeleccionProcFacturacion.dblProcsDrawText(Sender: TCustomDBListEx;
  Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
  var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
  var DefaultDraw: Boolean);
  var bmp : TBitmap;
begin
  with Sender do  begin
      if (Column.FieldName = 'PorMorosidad') then begin
          Text := '';
          Canvas.FillRect(Rect);
          bmp := TBitMap.Create;
	      	DefaultDraw := False;
          try
              if (spObtenerProcesos.FieldByName('PorMorosidad').AsBoolean) then begin
                  ilCheck.GetBitmap(1, Bmp);
              end else begin
                  ilCheck.GetBitmap(0, Bmp);
              end;

              Canvas.Draw((Rect.Left + Rect.Right - Bmp.Width) div 2, Rect.Top, bmp);
          finally
              bmp.Free;
          end;
      end;
  end;
end;
//END : SS_660_NDR_20131112-------------------------------------------------------------

end.
