{-------------------------------------------------------------------------------
 File Name: FrmGenerarArchivoNomina.pas
 Author:    lgisuk
 Date Created: 25/11/2004
 Language: ES-AR
 Description: M�dulo de la interface TransBank -
				Generar Archivo de Nomina
--------------------------------------------------------------------------------
 Revision History
--------------------------------------------------------------------------------
 Author: rcastro
 Date Created: 17/12/2004
 Description: revisi�n general

 Revision 1
 Author: sguastoni
 Date Created: 05-01-2007
 Description: Agregado parametro en spActualizarComprobantesEstadoDebitoAEnviado
              con el nombre @Usuario

 Revision : 2
    Author : vpaszkowicz
    Date : 11/07/2007
    Description : Agrego el total del saldo de los convenios al store spObtenerNominaTbk,
    por lo tanto saco la funci�n y el store que obten�an esta suma.

    Revision : 3
        Author : jconcheyro
        Date : 08/08/2007
        Description : Agrego la lista de Grupos de facturacion para pasarle como
        parametro al store un lista con los grupos a tener en cuenta

  Revision : 4
    Date: 03/03/2009
    Author: mpiazza
    Description:    Ref (SRX01226) se cambia" now " por nowbase

  Revision : 5
    Date: 13/04/2009
    Author: mpiazza
    Description: ss750 se actualiza el log de interfase con el monto
                 y cantidad de registros totales para el reporte


  Revision 6
  Author: mbecerra
  Date: 02-Junio-2006
  Description:		Se modifica para que al crear el listado de grupos
                	seleccionados por el operador, no REPITA el grupo, sino que
                    aparezca una sola vez.

    Autor           :   Claudio Quezada
    Fecha           :   06-01-2015
    Firma           :   SS_1147_CQU_20150106
    Descripci�n     :   Se agrega la l�gica para que determine si usa formato VS o CN

Autor		:	CQuezadaI
Fecha		:	05-03-2015
Firma		:	SS_1147_CQU_20150305
Descripcion	:	Se realizan correcciones de la primera versi�n, seg�n solicitud de PVergara de VS

Autor		:	CQuezadaI
Fecha		:	31-03-2015
Firma		:	SS_1147_CQU_20150324
Descripcion	:	Se realiza correccion a la fecha de vencimiento de la tarjeta.

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

Firma       :   SS_1147_CQU_20150519
Descripcion :   Se agrega el TipoComprobante a la funci�n ActualizarDetalleComprobanteEnviado

  Firma       : SS_1385_NDR_20150922
  Descripcion : Cambiar la transaccionalidad de delphi por Execute('TRANSACCIONALIDAD SQL') para evitar tener rollback oculto
 -------------------------------------------------------------------------------}
unit FrmGenerarArchivoNomina;

interface

uses
     //Gestion de Interfaces
     DMConnection,                  //Coneccion a base de datos OP_CAC
     Util,                          //Stringtofile,padl..
     Utildb,                        //Rutinas para base de datos
     UtilProc,                      //Mensajes
     PeaProcs,                      //NowBase
     ComunesInterfaces,             //Procedimientos y Funciones Comunes a todos los formularios
     ConstParametrosGenerales,      //Obtengo Valores de la Tabla Parametros Generales
     FrmRptEnvioComprobantes,       //Reporte de Comprobantes Enviados
     PeaTypes,                      // SS_1147_CQU_20150106
     //General
     Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
     Dialogs, ExtCtrls, DB, ADODB, StdCtrls, Grids, DBGrids, Validate,
     DateEdit, DPSControls, ListBoxEx, DBListEx, strutils, ComCtrls,
     ppDB, ppTxPipe, ppCtrls, ppBands, ppPrnabl, ppClass, ppCache, ppComm,
     ppRelatv, ppProd, ppReport, UtilRB, DmiCtrls, BuscaTab, DBClient, ImgList;

type
  TFGenerarArchivoNomina = class(TForm)
    Bevel: TBevel;
    lblCTitulo: TLabel;
    pnlAvance: TPanel;
    lblProcesoGeneral: TLabel;
    pbProgreso: TProgressBar;
    lblMensaje: TLabel;
    btnProcesar: TButton;
    btnSalir: TButton;
    spObtenerNominaTBK: TADOStoredProc;
    lblTitulo: TMemo;
    pnlAyuda: TPanel;
    ImgAyuda: TImage;
    pnlReprocesar: TPanel;
    buscaInterfaces: TBuscaTabla;
    edFecha: TBuscaTabEdit;
    lblFechaInterface: TLabel;
    lblNombreArchivo: TLabel;
    lblArchivoInterface: TLabel;
    spObtenerInterfasesEjecutadas: TADOStoredProc;
    spObtenerInterfasesEjecutadasFecha: TDateTimeField;
    spObtenerInterfasesEjecutadasDescripcion: TStringField;
    spObtenerInterfasesEjecutadasCodigoOperacionInterfase: TIntegerField;
    Panel1: TPanel;
    lblUltimosGruposFacturados: TLabel;
    dblGrupos: TDBListEx;
    dsGrupos: TDataSource;
    ilImages: TImageList;
    cdGrupos: TClientDataSet;
    spObtenerUltimosGruposFacturados: TADOStoredProc;
    lblCambiarNombre: TLabel;	// SS_1147_CQU_20150106
    procedure ImgAyudaClick(Sender: TObject);
    procedure ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
    function  buscaInterfacesProcess(Tabla: TDataSet;var Texto: String): Boolean;
    procedure buscaInterfacesSelect(Sender: TObject; Tabla: TDataSet);
    procedure edFechaChange(Sender: TObject);
    procedure btnProcesarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure dblGruposDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure dblGruposDblClick(Sender: TObject);
    procedure lblCambiarNombreClick(Sender: TObject);	// SS_1147_CQU_20150106

  private
    FCodigoOperacion : Integer;
    FCodigoOperacionAnterior : Integer;
    FCancelar: Boolean;
    FErrorGrave : Boolean;
    FNombreArchivoNomina : AnsiString;
    FNombreArchivoControl : AnsiString;
    FDatosArchivoControl : AnsiString;
    FNumeroSecuencia : Integer;
    FTBK_CodigodeComercio  : Ansistring;
    FTBK_DirectorioDestino : Ansistring;
    FMontoTotal     : Int64;
    FCantidadTotal  : Int64;
    FCodigoNativa   : Integer;        // SS_1147_CQU_20150106
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    Function  CrearNombreArchivos(var NombreArchivoNomina, NombreArchivoControl: AnsiString; Fecha : TDateTime) : Boolean;
    function  RegistrarOperacion : boolean;
    Function  ObtenerDebitos : Boolean;
    Function  GuardarArchivo(var CodigoOperacion:integer) : Boolean;
    Function  GenerarReporteDeFinalizacion(CodigoOperacionInterfase:integer) : Boolean;
    function CargarGrupos: Boolean;
    function GenerarListaGruposAIncluir(var Lista: string): boolean;
    function ExistenGruposSeleccionados: boolean;

    { Private declarations }
  public
    Function  Inicializar(txtCaption: ANSIString; MDIChild: Boolean; Reprocesar : Boolean = False ): Boolean;
    { Public declarations }
  end;

var
  FGenerarArchivoNomina: TFGenerarArchivoNomina;
  FLista: TStringList;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    gcasais
  Date Created: 25/11/2004
  Description: M�dulo de inicializaci�n de este formulario
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFGenerarArchivoNomina.Inicializar(txtCaption : ANSIString; MDIChild : Boolean; Reprocesar : Boolean = False ): Boolean;

    {******************************** Function Header ******************************
    Function Name: VerificarParametrosGenerales
    Author : lgisuk
    Date Created : 18/07/2005
    Description : Obtengo los Parametros Generales que se utilizaran en el formulario
                  al inicializar y verifico que los valores obtenidos sean validos.
    Parameters : None
    Return Value : boolean
    *******************************************************************************}
    Function VerificarParametrosGenerales : Boolean;
    resourcestring
        MSG_ERROR_CHECK_GENERAL_PARAMETER = 'Error al verificar parametro general';
        MSG_ERROR                         = 'Error';
        STR_NOT_EXISTS_GENERAL_PARAMETER  = 'No existe parametro general: ';
        STR_EMPTY_GENERAL_PARAMETER       = 'Parametro general vacio: ';                                         //Se verifica que los codigos no esten vacios, si no tienen razon de ser
        STR_DIRECTORY_NOT_EXISTS          = 'No existe el directorio indicado en parametros generales: ' + CRLF; //Los directorios de destino y errores en procesos de envio son criticos
    const
        TBK_CODIGODECOMERCIO  = 'TBK_CodigodeComercio';
        TBK_DIRECTORIODESTINO = 'TBK_DirectorioDestino';
    var
        DescError : AnsiString;
    begin
        Result    := True;
        DescError := '';
        try
            try

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, TBK_CODIGODECOMERCIO , FTBK_CodigodeComercio) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + TBK_CODIGODECOMERCIO;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                if not (FTBK_CodigodeComercio <> '') then begin
                   DescError := STR_EMPTY_GENERAL_PARAMETER + TBK_CODIGODECOMERCIO;
                   Result := False;
                   Exit;
                end;

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, TBK_DIRECTORIODESTINO , FTBK_DirectorioDestino) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + TBK_DIRECTORIODESTINO;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                FTBK_DirectorioDestino := GoodDir(FTBK_DirectorioDestino);
                if  not DirectoryExists(FTBK_DirectorioDestino) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FTBK_DirectorioDestino;
                    Result := False;
                    Exit;
                end;

                // Obtengo la Concesionaria                         // SS_1147_CQU_20150106
                FCodigoNativa := ObtenerCodigoConcesionariaNativa;  // SS_1147_CQU_20150106
            except
                on e: Exception do begin
                    Result := False;
                    MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, e.Message, MSG_ERROR, MB_ICONWARNING);
                end;
            end;

        finally
            //si no paso la verificacion de parametros generales
            if (Result = False) then begin
                //informo la situaci�n
                MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, DescError, MSG_ERROR, MB_ICONWARNING);
            end;
        end;
    end;

resourcestring
	MSG_INIT_ERROR = 'Error al Inicializar';
	MSG_ERROR = 'Error';
begin
	Result := False;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;

    //Centro el formulario
	CenterForm(Self);
	try
        DMConnections.BaseCAC.Connected := True;
        Result := DMConnections.BaseCAC.Connected and
                        VerificarParametrosGenerales and
                                CrearNombreArchivos(FNombreArchivoNomina , FNombreArchivoControl , Now);
        if not Result then Exit;
    except
        on e: Exception do begin
            MsgBoxErr(MSG_INIT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
            Exit;
        end;
    end;
    // Todo Ok, terminamos de inicializar
    lblTitulo.text := FNombreArchivoNomina;   //Indico que archivo voy a generar
    lblMensaje.Caption := '';                 //No muestro ningun mensaje
    PnlAvance.visible := False;               //Oculto el Panel de Avance
    FCancelar := False;                       //inicio cancelar en false
    FCodigoOperacion := 0;                    //Inicializo Codigo de Operacion
    FCodigoOperacionAnterior := 0;            //inicializo codigo operacion anterior
    FLista.Clear;                             //Vacio la lista donde voy a generar el archivo

    lblCambiarNombre.Visible := (FCodigoNativa = CODIGO_VS);    // SS_1147_CQU_20150106
    if not CargarGrupos then begin
        Result := False;
        Exit;
    end;



    //Si es re-proceso
    if Reprocesar = true then begin
        //hago visible el panel de busqueda de interfaz anterior
        pnlreprocesar.Visible := True;
        //Obtengo las interfaces ejecutadas
        spObtenerInterfasesEjecutadas.Parameters.ParamByName( '@CodigoModulo' ).Value := 35;
        spObtenerInterfasesEjecutadas.CommandTimeout := 500;
        spObtenerInterfasesEjecutadas.Open;
        //deshabilito boton procesar hasta que seleccionen una interfaz
        btnProcesar.Enabled := False;
        lblCambiarNombre.Enabled := btnProcesar.Enabled;    // SS_1147_CQU_20150106        
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ImgAyudaClick
  Author:    lgisuk
  Date Created: 03/06/2005
  Description: Mensaje de Ayuda
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoNomina.ImgAyudaClick(Sender: TObject);
Resourcestring
    MSG_NOMINA          = ' ' + CRLF +
                          'El Archivo de Nomina es' + CRLF +
                          'utilizado por el ESTABLECIMIENTO para informar a TRANSBANK' + CRLF +
                          'el detalle de los pagos a recaudar' + CRLF +
                          ' ' + CRLF +
                          'Nombre del Archivo: NOMI99999999mmddN.sss' + CRLF +
                          ' ';
begin
    //si esta procesando sale
    if PnlAvance.visible = True then exit;
    //Muestro el mensaje
    MsgBoxBalloon(MSG_NOMINA, Caption, MB_ICONQUESTION, IMGAYUDA);
end;

{-----------------------------------------------------------------------------
  Function Name: ImgAyudaMouseMove
  Author:    lgisuk
  Date Created: 03/06/2005
  Description:
  Parameters: Sender: TObject;Shift: TShiftState; X, Y: Integer
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoNomina.ImgAyudaMouseMove(Sender: TObject;Shift: TShiftState; X, Y: Integer);
begin
    if PnlAvance.visible = true then ImgAyuda.Cursor := crdefault else ImgAyuda.Cursor := crHandPoint;
end;

{-----------------------------------------------------------------------------
  Function Name: CrearNombreArchivos
  Author:    lgisuk
  Date Created: 05/07/2005
  Description: creo los nombres para el archivo de nomina y para el archivo de control
  Parameters: var NombreArchivoNomina,NombreArchivoControl: AnsiString; dFecha : TDateTime
  Return Value: Boolean
-----------------------------------------------------------------------------
  Revision :1
  Date: 23/09/2009
  Author: mpiazza
  Description: ss750 se incrementa a 200 segundos  el comandTimeOut
                    de ObtenerUltimosGruposFacturados
-----------------------------------------------------------------------------}
function TFGenerarArchivoNomina.CargarGrupos: Boolean;
begin
    Result := False;
    spObtenerUltimosGruposFacturados.Close;

    try
        cdGrupos.CreateDataSet;
        spObtenerUltimosGruposFacturados.CommandTimeout := 200;
        spObtenerUltimosGruposFacturados.Open;
        Result := not spObtenerUltimosGruposFacturados.IsEmpty;
        while not spObtenerUltimosGruposFacturados.Eof do begin
            // Insertamos nuestro registro
            cdGrupos.AppendRecord([False, spObtenerUltimosGruposFacturados.FieldByName('CodigoGrupoFacturacion').AsInteger,
              spObtenerUltimosGruposFacturados.FieldByName('FechaProgramadaEjecucion').AsDateTime,
              spObtenerUltimosGruposFacturados.FieldByName('FechaCorte').AsDateTime,
              spObtenerUltimosGruposFacturados.FieldByName('FechaEmision').AsDateTime,
              spObtenerUltimosGruposFacturados.FieldByName('FechaVencimiento').AsDateTime,
              spObtenerUltimosGruposFacturados.FieldByName('NoTerminados').AsInteger]);
            spObtenerUltimosGruposFacturados.Next;
        end;
        spObtenerUltimosGruposFacturados.Close;

    except
        on e: Exception do begin
            MsgBoxErr('Error obteniendo los �ltimos grupos facturados', e.Message, Caption, MB_ICONERROR);
            Result:= False;
        end;
    end;

end;

Function TFGenerarArchivoNomina.CrearNombreArchivos(var NombreArchivoNomina, NombreArchivoControl: AnsiString; Fecha : TDateTime): Boolean;

    {-----------------------------------------------------------------------------
      Function Name: ObtenerNumeroSecuencia
      Author:    lgisuk
      Date Created: 05/07/2005
      Description: obtengo el numero de secuencia sirve para numerar el archivo en caso que se genere mas de una vez
      Parameters: var NumeroSecuencia:AnsiString
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function ObtenerNumeroSecuencia(var NumeroSecuencia: AnsiString) : Boolean;
    resourcestring
        MSG_ERROR = 'Error al crear el numero de secuencia';
    Var
        Valor : Integer;
    begin
        try
            //Obtengo el numero de secuencia
            Valor := QueryGetValueInt(DMConnections.BaseCAC, 'Select dbo.ObtenerNumeroSecuenciaTBK (35)' );
            //el numero de secuencia debe ocupar 3 posiciones
            NumeroSecuencia := IStr0(Valor , 3);
            Result := True;
        except
            raise Exception.Create(MSG_ERROR);
        end;
    end;

resourcestring
    MSG_FILE_NAME_ERROR = 'Error al crear los nombres de los archivos';
    MSG_DIRECTORY_ERROR = 'El directorio de salida %s no existe.' + crlf + 'Verifique los par�metros generales.';
const
    FILE_EXTENSION = '.txt';
var
    Year, Month, Day : Word;
    Mes, Dia : AnsiString;
    NumeroSecuencia : AnsiString;
begin
    try
        //Obtengo a�o, mes y dia en base a la fecha recibida
        DecodeDate(Fecha, Year, Month, Day);
        if Length(IntToStr(Month)) = 1 then Mes := '0' + IntToStr(Month) else Mes := IntToStr(Month);
        if Length(IntToStr(Day)) = 1 then Dia := '0' + IntToStr(Day) else Dia := IntToStr(Day);
        //Obtengo Numero de Secuencia
        ObtenerNumeroSecuencia(NumeroSecuencia);
        //Genero el Nombre del Archivo de Nomina
        if FCodigoNativa = CODIGO_VS then begin                                                             // SS_1147_CQU_20150106
            NombreArchivoNomina := 'NOMI' + FTBK_CodigodeComercio;                                          // SS_1147_CQU_20150106
            NombreArchivoNomina := NombreArchivoNomina + Mes + Dia;                                         // SS_1147_CQU_20150106
            NombreArchivoNomina := NombreArchivoNomina + 'N CICLO ' + NumeroSecuencia;                      // SS_1147_CQU_20150106
            NombreArchivoNomina := GoodDir(FTBK_DirectorioDestino) + NombreArchivoNomina + FILE_EXTENSION;  // SS_1147_CQU_20150106
        end else begin                                                                                      // SS_1147_CQU_20150106
        NombreArchivoNomina := 'NOMI' + FTBK_CodigodeComercio;
        NombreArchivoNomina := NombreArchivoNomina + Mes + Dia;
        NombreArchivoNomina := NombreArchivoNomina + '.' + 'N' + '.' + NumeroSecuencia;
        NombreArchivoNomina := GoodDir(FTBK_DirectorioDestino) + NombreArchivoNomina + FILE_EXTENSION;
        end;                                                                                                // SS_1147_CQU_20150106
        //Genero el Nombre del Archivo de Control
        NombreArchivoControl := 'CONTROL' + FTBK_CodigodeComercio;
        NombreArchivoControl := NombreArchivoControl + Mes + Dia;
        NombreArchivoControl := NombreArchivoControl + '.' + 'N' + '.' + NumeroSecuencia;
        NombreArchivoControl := GoodDir(FTBK_DirectorioDestino) + NombreArchivoControl + FILE_EXTENSION;
        //guardo el numero de secuencia
        FNumeroSecuencia := StrToInt(NumeroSecuencia);
        Result := True;
    except
        raise Exception.Create(MSG_FILE_NAME_ERROR);
    end;
end;


procedure TFGenerarArchivoNomina.dblGruposDblClick(Sender: TObject);
begin
    cdGrupos.Edit;
    if cdGrupos.FieldByName('Seleccionado').AsBoolean then begin
        cdGrupos.FieldByName('Seleccionado').AsBoolean := False;
    end else begin
        if ( cdGrupos.FieldByName('NoTerminados').AsInteger > 0 ) then
        begin
            ShowMessage('Este Proceso de Facturacion tiene comprobantes no terminados');
            cdGrupos.FieldByName('Seleccionado').AsBoolean := False;
        end
        else
            cdGrupos.FieldByName('Seleccionado').AsBoolean := True;
    end;
    cdGrupos.Post;

end;

procedure TFGenerarArchivoNomina.dblGruposDrawText(Sender: TCustomDBListEx;
  Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
  var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
  var DefaultDraw: Boolean);
begin
    if Column.FieldName = 'Seleccionado' then begin
        if cdGrupos.FieldByName('Seleccionado').AsBoolean = True then begin
              ilImages.Draw(Sender.Canvas, Rect.Left, Rect.Top, 1);
          end else if cdGrupos.FieldByName('Seleccionado').AsBoolean = False then begin
              ilImages.Draw(Sender.Canvas, Rect.Left, Rect.Top, 0);
          end;
          DefaultDraw := False;
    end;

end;

{-----------------------------------------------------------------------------
  Function Name: buscaInterfacesProcess
  Author:    lgisuk
  Date Created: 05/07/2005
  Description: Muestra la lista de procesos ejecutados
  Parameters: Tabla: TDataSet; var Texto: String
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFGenerarArchivoNomina.buscaInterfacesProcess(Tabla: TDataSet; var Texto: String): Boolean;
begin
	Result := True;
	with Tabla do
    	Texto := FieldByName('Descripcion').AsString + ' - ' + FieldByName('Fecha').AsString;
end;

{-----------------------------------------------------------------------------
  Function Name: buscaInterfacesSelect
  Author:    lgisuk
  Date Created: 05/07/2005
  Description: Al Seleccionar un proceso, muestra la fecha y crea
               el nombre del archivo
  Parameters: Sender: TObject;Tabla: TDataSet
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoNomina.buscaInterfacesSelect(Sender: TObject;Tabla: TDataSet);
begin
    //Obtengo la fecha de la interfaz y el codigo de operaci�n anterior
  	edFecha.Text := FormatDateTime( 'dd-mm-yyyy', Tabla.FieldByName('Fecha').AsDateTime);
    FCodigoOperacionAnterior := Tabla.FieldByName('CodigoOperacionInterfase').Value;
    //Creo el nombre de los archivos a enviar
    CrearNombreArchivos(FNombreArchivoNomina, FNombreArchivoControl, Tabla.FieldByName('Fecha').AsDateTime);
    lblNombreArchivo.Caption := ExtractFileName(FNombreArchivoNomina);
    //No permito cambiar la interfaz una vez elegida
    edfecha.Enabled := False;
end;

{-----------------------------------------------------------------------------
  Function Name: edFechaChange
  Author:    lgisuk
  Date Created: 05/07/2005
  Description: Verifica que halla un fecha de interfaz para procesar
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoNomina.edFechaChange(Sender: TObject);
begin
    btnProcesar.Enabled := ( edFecha.Text <> '' );
    lblCambiarNombre.Enabled := btnProcesar.Enabled;    // SS_1147_CQU_20150106    
end;

function TFGenerarArchivoNomina.ExistenGruposSeleccionados: boolean;
begin
    Result := False;
    try
        cdGrupos.First;
        while not cdGrupos.Eof do begin
            if cdGrupos.FieldByName('Seleccionado').AsBoolean then begin
                Result := True;
                Break;
            end;
            cdGrupos.Next;
        end;
        cdGrupos.First;
    except
        on E: Exception do begin
            MsgBoxErr('Error contando grupos para la generaci�n ', e.Message, Caption, MB_ICONERROR);
        end;
    end;

end;

{-----------------------------------------------------------------------------
  Function Name: RegistrarOperacion
  Author:    lgisuk
  Date Created: 07/07/2005
  Description: Registra la Operaci�n en el Log de Operaciones
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
function TFGenerarArchivoNomina.RegistrarOperacion : Boolean;
resourcestring
  	MSG_COULD_NOT_REGISTER_OPERATION = 'No se pudo registrar la operaci�n';
    MSG_ERROR = 'Error';
Const
    STR_OBSERVACION = 'Generar Archivo Nomina';
    COD_MODULO = 35;
var
    DescError : String;
begin
    Result := RegistrarOperacionEnLogInterface(DMConnections.BaseCAC, COD_MODULO, ExtractFileName(FNombreArchivoNomina), UsuarioSistema, STR_OBSERVACION, False, False, NowBase(DMConnections.BaseCAC), 0, FCodigoOperacion, DescError);
    if (not Result) then MsgBoxErr(MSG_COULD_NOT_REGISTER_OPERATION, DescError, MSG_ERROR, MB_ICONERROR);
end;

{-----------------------------------------------------------------------------------------
  Function Name: AbrirBTNClick
  Author:    lgisuk
  Date Created: 09/12/2004
  Description: Obtengo Los Comprobantes de convenios con PAT Para Informar a Transbank
  Parameters: Sender: TObject
  Return Value: None
------------------------------------------------------------------------------------------}
Function TFGenerarArchivoNomina.ObtenerDebitos:boolean;
Resourcestring
    MSG_QUERY_ERROR = 'No se pudo abrir la consulta';
    MSG_EMPTY_QUERY_ERROR = 'No hay comprobantes a cobrar pendientes de ser enviados!';
    MSG_ERROR = 'Error';
var
    ListaGrupos: string;
begin
    Result := False;
    try
        SPObtenerNominaTbk.Close;
        SPObtenerNominaTbk.Parameters.Refresh;
        if not GenerarListaGruposAIncluir( ListaGrupos ) then Exit ;
        SPObtenerNominaTbk.Parameters.ParamByName( '@ListaGruposAIncluir' ).Value := ListaGrupos;

        SPObtenerNominaTbk.Parameters.ParamByName( '@CodigoOperacionInterfase' ).Value := iif (FCodigoOperacionAnterior = 0, NULL, FCodigoOperacionAnterior );
        SPObtenerNominaTbk.Parameters.ParamByName( '@Total' ).Value := NULL;
        SPObtenerNominaTbk.CommandTimeout := 5000;
        SPObtenerNominaTbk.Open;
        Result := (SPObtenerNominaTbk.RecordCount > 0);
        if not Result then begin
            FErrorGrave := True;
            MsgBox(MSG_EMPTY_QUERY_ERROR);
        end;
    except
       on e: Exception do begin
            FErrorGrave := True;
            MsgBoxErr(MSG_QUERY_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
            Exit;
       end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: Guardar
  Author:    lgisuk
  Date Created: 11/03/2005
  Description: Guardo el Archivo
  Parameters: var CodigoOperacion:integer
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFGenerarArchivoNomina.GuardarArchivo(var CodigoOperacion:integer):boolean;

    {-----------------------------------------------------------------------------
      Function Name: GuardarArchivos
      Author:    lgisuk
      Date Created: 07/07/2005
      Description: Guarda los archivos de nomina y control
      Parameters: var DescError:AnsiString
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function GuardarArchivos(var DescError : AnsiString) : Boolean;
    Resourcestring
        MSG_CANCEL = 'Se ha cancelado el proceso debido a la existencia de un ' + CRLF + 'archivo id�ntico en el directorio de destino';
        MSG_ERROR  = 'No se pudo guardar archivo de nomina';
    Const
        FILE_TITLE   = ' El archivo:  ';
        FILE_EXISTS  = ' ya existe en el directorio de destino. �Desea reemplazarlo?';
        FILE_WARNING = ' Atenci�n';
    begin
        //Verificamos que no exista uno con el mismo nombre
        if FileExists(FNombreArchivoNomina) or FileExists(FNombreArchivoControl) then begin
            if (MsgBox( FILE_TITLE + FNombreArchivoNomina + FILE_EXISTS,FILE_WARNING , MB_YESNO + MB_ICONQUESTION) = IDNO) then begin
                DescError := MSG_CANCEL; //informo que fue cancelado
                Result := False; //fallo
                Exit; //y salgo
            end;
        end;
        //creo el archivo
        try
            if FCodigoNativa <> CODIGO_VS then begin                        // SS_1147_CQU_20150106
            Result := StringToFile(FLista.text, FNombreArchivoNomina) and
                            StringToFile(FDatosArchivoControl, FNombreArchivoControl);
            end else                                                        // SS_1147_CQU_20150106
                Result := StringToFile(FLista.text, FNombreArchivoNomina);  // SS_1147_CQU_20150106
            if not Result then DescError := MSG_ERROR;
        except
            raise Exception.Create(MSG_ERROR);
        end;
    end;

    {-----------------------------------------------------------------------------
      Function Name: ActualizarDetalleComprobantesEnviados
      Author:    lgisuk
      Date Created: 07/07/2005
      Description: Registra todos los comprobantes que fueron informados en esa operacion
      Parameters: CodigoOperacion:integer
      Return Value: boolean
    -----------------------------------------------------------------------------}
    {Function ActualizarDetalleComprobantesEnviados(CodigoOperacion : Integer) : Boolean;
    Resourcestring
        MSG_ERROR = 'No se pudo Registrar los comprobantes enviados';
    begin
        with SPActualizarDetalleComprobantesEnviados.Parameters do begin
            Refresh;
            ParamByName('@CodigoOperacionInterfaseAnterior').Value := iif (FCodigoOperacionAnterior = 0, NULL, FCodigoOperacionAnterior );
            ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacion;
        end;
        try
            SPActualizarDetalleComprobantesEnviados.CommandTimeout := 5000;
            SPActualizarDetalleComprobantesEnviados.ExecProc;
            Result := True;
        except
            raise Exception.Create(MSG_ERROR);
        end;
    end;}

    {-----------------------------------------------------------------------------
      Function Name: ActualizarComprobantesEstadoDebitoAEnviado
      Author:    lgisuk
      Date Created: 07/07/2005
      Description: Marco los comprobantes que recien fueron informados como enviados
      Parameters: None
      Return Value: boolean
      Revision : 1
          Author : vpaszkowicz
          Date : 12/07/2007
          Description : La comento dado que ahora aunque ya haya enviado un comprobante
          lo puedo volver a enviar dado que siempre uso la �ltima NK del convenio.
    -----------------------------------------------------------------------------}
    {Function ActualizarComprobantesEstadoDebitoAEnviado : Boolean;
    Resourcestring
        MSG_ERROR = 'No se pudo actualizar el estado debito de los comprobantes';
    begin
        //Si esta re-procesando no marco los comprobantes salgo.
        if (FCodigoOperacionAnterior <> 0) then begin
             Result := True;
             exit;
        end;
        try
            //revision 1
            SPActualizarComprobantesEstadoDebitoAEnviado.Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
            SPActualizarComprobantesEstadoDebitoAEnviado.CommandTimeout := 5000;
            SPActualizarComprobantesEstadoDebitoAEnviado.ExecProc;
            Result := True;
        except
            raise Exception.Create(MSG_ERROR);
        end;
    end;}

    {-----------------------------------------------------------------------------
      Function Name: GuardarNumeroSecuencia
      Author:    lgisuk
      Date Created: 07/07/2005
      Description: Guardo el numero de secuencia del archivo
      Parameters: CodigoOperacionInterfase:integer;NumeroSecuencia:integer
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function GuardarNumeroSecuencia(CodigoOperacionInterfase : Integer ; NumeroSecuencia : Integer ) : Boolean;
    resourcestring
        MSG_ERROR = 'Error al guardar el numero de secuencia: ';
    var
        DescError : Ansistring;
    begin
        try
            ActualizarNumeroSecuencia(DMConnections.BaseCAC, CodigoOperacionInterfase, NumeroSecuencia, DescError);    //InstallIni.WriteInteger('TRANSBANK', 'NOMINA' ,NumeroSecuencia);
            Result := True;
        except
            raise Exception.Create(MSG_ERROR + DescError);
        end;
    end;

Resourcestring
    MSG_SAVE_FILE_ERROR = 'No se pudo Guardar el Archivo: ';
    MSG_SAVE_EMPTY_DATA_ERROR = 'No hay datos para guardar en el archivo';
    MSG_ERROR = 'Error';
var
    DescError : Ansistring;
begin
    Result := False;
    if not Assigned(FLista) or (FLista.Count = 0) then begin
        FErrorGrave := True;
        DescError := MSG_SAVE_EMPTY_DATA_ERROR;
        MsgBoxErr(MSG_SAVE_FILE_ERROR, DescError, MSG_ERROR, MB_ICONERROR);
        Exit;
    end;
    try
        //Creo el Archivo de Nomina y el Archivo de Control
        //GuardarArchivos(DescError);

        //Inicio la transaccion
        //DMConnections.BaseCAC.BeginTrans;

        Result := (//Registro Todos los comprobantes que fueron informados en esa operacion
                   //ActualizarDetalleComprobantesEnviados(Codigooperacion) and
                   //Marco Todos los comprobantes que recien fueron informados como enviados
                   //ActualizarComprobantesEstadoDebitoAEnviado and

                   //Modifico esta parte en donde guardo en el archivo y registro en la base.
                   GuardarArchivos(DescError) and

                   //Y Guardo el numero de secuencia
                   GuardarNumeroSecuencia(CodigoOperacion,FnumeroSecuencia)
                  );

        {if Result = True then begin
            //Acepto la Transaccion
            DMConnections.BaseCAC.CommitTrans;
        end else begin
            //Cancelo la transccion
            DMConnections.BaseCAC.RollbackTrans;
            //Elimino los Archivos
            DeleteFile(FNombreArchivoNomina);
            DeleteFile(FNombreArchivoControl);
            FErrorGrave := True;
            MsgBoxErr(MSG_SAVE_FILE_ERROR, DescError, MSG_ERROR, MB_ICONERROR);
            Exit;
        end;}

    except
       on e: Exception do begin
            FErrorGrave := True;
            MsgBoxErr(MSG_SAVE_FILE_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
            Exit;
       end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: GenerarReporteDeFinalizacion
  Author:    lgisuk
  Date Created: 11/03/2005
  Description: Genero el Reporte de Finalizacion de Proceso sirve para
               para verificar si el proceso se realizo segun parametros
               normales.
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
function TFGenerarArchivoNomina.GenerarListaGruposAIncluir(
  var Lista: string): boolean;
var
    ListaTemporal :TStringList;
    I: integer;
    sGrupo : string;
begin
    Result := False;
    ListaTemporal := TStringList.Create;
    try
        try
            ListaTemporal.Duplicates := dupIgnore; // la lista ignora los duplicados
            cdGrupos.First;
            while not cdGrupos.Eof do begin
                {REV.6
                if cdGrupos.FieldByName('Seleccionado').AsBoolean then
                	ListaTemporal.Add( IntToStr(cdGrupos.FieldByName('GrupoFacturacion').AsInteger));}

                sGrupo := IntToStr(cdGrupos.FieldByName('GrupoFacturacion').AsInteger);
                if cdGrupos.FieldByName('Seleccionado').AsBoolean and (ListaTemporal.IndexOf(sGrupo) < 0) then
                    ListaTemporal.Add(sGrupo);

                {FIN REV.6 }

                cdGrupos.Next;
            end;
            cdGrupos.First;
            for I := 0 to ListaTemporal.Count - 1 do begin
                Lista := Lista + ListaTemporal[I];
                if (I < (ListaTemporal.Count -1) ) then   // asi no me agrega una coma al final
                    Lista := Lista + ','
            end;
            Result := True;
        except
            on E: Exception do begin
                MsgBoxErr('Error generando par�metro con lista de grupos para el store', e.Message, Caption, MB_ICONERROR);
            end;
        end;
    finally
            ListaTemporal.Free;
    end;
end;

Function TFGenerarArchivoNomina.GenerarReporteDeFinalizacion(CodigoOperacionInterfase : Integer) : Boolean;
Resourcestring
    STR_COMPROBANTES_ENVIADOS = 'Comprobantes Enviados';
var
    FEnvio : TFRptEnvioComprobantes;
begin
    Result := False;
    try
        Application.CreateForm(TFRptEnvioComprobantes , FEnvio);
        if not FEnvio.Inicializar(STR_COMPROBANTES_ENVIADOS, CodigoOperacionInterfase) then FEnvio.Release;
        Result := True;
    except
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ProcesarBTNClick
  Author:    lgisuk
  Date Created: 04/01/2005
  Description: Genera el Archivo de Nomina
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------
  Revision : 4
    Date: 13/04/2009
    Author: mpiazza
    Description: ss750 se actualiza el log de interfase con el monto
                 y cantidad de registros totales para el reporte
-----------------------------------------------------------------------------
  Revision 5
  mpiazza
  15/04/2009
  Ref: SS-750 Se agrega Monto a ActualizarDetalleComprobanteEnviado
-----------------------------------------------------------------------------}

procedure TFGenerarArchivoNomina.btnProcesarClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: Generar
      Author:    lgisuk
      Date Created: 07/07/2005
      Description: creo los datos de los archivos de nomina y del archivo de control
      Parameters: None
      Return Value: boolean
      Revision :
          Author : vpaszkowicz
          Date : 13/07/2007
          Description : Coment� algunas funciones por cuestiones de performance
    -----------------------------------------------------------------------------}
    Function GenerarArchivo : Boolean;

        {-----------------------------------------------------------------------------
          Function Name: ObtenerSumaMontosNomina
          Author:    lgisuk
          Date Created: 07/07/2005
          Description: Obtengo la Suma de los Montos del Archivo de Nomina
          Parameters: var Valor:Ansistring
          Return Value: boolean
          Revision : 2
              Author : vpaszkowicz
              Date : 11/07/2007
              Description : Comento esta funci�n porque queda en desuso.
              Ahora retorno esto en un par�metro del store spObtenerNominaTbk
        -----------------------------------------------------------------------------}
        {Function ObtenerSumaMontosNomina(var Valor : Ansistring) : Boolean;
        Resourcestring
            MSG_ERROR = 'No se pudo Obtener La Suma de los Montos de la Nomina';
        begin
            Valor := '0';
            try
                SPObtenerSumaMontosNominaTbk.Close;
                SPObtenerSumaMontosNominaTbk.Parameters.Refresh;
                SPObtenerSumaMontosNominaTbk.Parameters.ParamByName( '@CodigoOperacionInterfase' ).Value := iif (FCodigoOperacionAnterior = 0, NULL, FCodigoOperacionAnterior );
                SPObtenerSumaMontosNominaTbk.Parameters.ParamByName( '@Valor' ).Value := 0;
                SPObtenerSumaMontosNominaTbk.CommandTimeout := 5000;
                SPObtenerSumaMontosNominaTbk.ExecProc;
                Valor := SPObtenerSumaMontosNominaTbk.Parameters.ParamByName('@Valor').Value;
                Result := True;
            except
                //cambiar por msgboxerr!! asi muesta si el store tuvo alg�n error
                raise Exception.Create(MSG_ERROR);
            end;
        end;}
        function RellenarCaracter(Texto, Caracter : string; EsIzquierda : boolean; LargoTotal : Integer) : string;	// SS_1147_CQU_20150106
        begin																										// SS_1147_CQU_20150106
            Result := Texto;																						// SS_1147_CQU_20150106
            while Length(Result) < LargoTotal do begin																// SS_1147_CQU_20150106
                if EsIzquierda then Result := Caracter + Result														// SS_1147_CQU_20150106
                else Result := Result + Caracter;																	// SS_1147_CQU_20150106
            end;																									// SS_1147_CQU_20150106
            Result := Copy(Result, 1, LargoTotal);																	// SS_1147_CQU_20150106
        end;																										// SS_1147_CQU_20150106
        {-----------------------------------------------------------------------------
          Function Name: QueryToTxt
          Author:    lgisuk
          Date Created: 07/07/2005
          Description: Convierto la consulta a TXT
          Parameters: Query:TCustomAdoDataSet
          Return Value: Boolean
          Revision : 2
              Author : vpaszkowicz
              Date : 13/07/2007
              Description : Agrego la parte de registrar los comprobantes que envie al
              archivo de texto despues de enviar cada linea.
          Revision : 3
            Date: 13/04/2009
            Author: mpiazza
            Description: ss750 se actualiza el log de interfase con el monto
                         y cantidad de registros totales para el reporte

        -----------------------------------------------------------------------------}
        Function QueryToTxt(Query : TCustomAdoDataSet) : Boolean;
        resourcestring
            MSG_PROCESS_ERROR = 'Error procesando los datos de las nominas';
        var
            i: Integer;
            Linea, DescriError: AnsiString;
        begin
            Result := False;
            if not Assigned(Query) or (not Assigned(FLista))  then Exit;
            try
                with Query do begin
                    First;
                    while not Eof and (not FCancelar) do begin
                        i := 1;
                        Linea := '';
                        //mientras halla registros y no sea cancelado
                        //while (i <= Query.FieldCount) and (not FCancelar) do begin                                                           // SS_1147_CQU_20150106
                        //    //genero las lineas del archivo                                                                                  // SS_1147_CQU_20150106
                        //    Linea := Linea + Fields.FieldByNumber(i).AsString + ';';                                                         // SS_1147_CQU_20150106
                        //    Inc(i);                                                                                                          // SS_1147_CQU_20150106
                        //end;                                                                                                                 // SS_1147_CQU_20150106
                        if FCodigoNativa <> CODIGO_VS then begin                                                                               // SS_1147_CQU_20150106
                            Linea :=    Query.FieldByName('TipodeTransaccion').AsString + ';'               //  Filler                         // SS_1147_CQU_20150106
                                    +	Query.FieldByName('MontodelaTransaccion').AsString + ';'            //	Monto Facturado                // SS_1147_CQU_20150106
                                    +	Query.FieldByName('NumerodelaTarjeta').AsString + ';'               //  N�mero Tarjeta                 // SS_1147_CQU_20150106
                                    +	Query.FieldByName('FechadeExpiracion').AsString + ';'               //  Fecha Expiraci�n               // SS_1147_CQU_20150106
                                    +   Query.FieldByName('Filler').AsString + ';'                          //  Filler                         // SS_1147_CQU_20150106
                                    +	Query.FieldByName('NombredelTarjetaHabiente').AsString + ';'        //  Nombre del Due�o de la Tarjeta // SS_1147_CQU_20150106
                                    +	Query.FieldByName('DescripcionDelServicio').AsString + ';'          //  Descripcion del Servicio       // SS_1147_CQU_20150106
                                    +	Query.FieldByName('TelefonoDelTarjetaHabiente').AsString + ';'      //  Tel�fono                       // SS_1147_CQU_20150106
                                    +	Query.FieldByName('NumeroIdentificadordelServicio').AsString + ';'  //  Cta. Facturacion               // SS_1147_CQU_20150106
                                    +	Query.FieldByName('RutdelTarjetaHAbiente').AsString + ';'           //  Rut                            // SS_1147_CQU_20150106
                                    +	Query.FieldByName('ReferenciaDelCobroEfectuado').AsString + ';'     //  N�mero del Documento (�ltimo)  // SS_1147_CQU_20150106
                                    +	Query.FieldByName('CodigodeAutorizacion').AsString + ';'            //  C�digo de Autorizaci�n         // SS_1147_CQU_20150106
                                    +	Query.FieldByName('CodigodeRespuesta').AsString + ';'               //  C�digo de Respuesta            // SS_1147_CQU_20150106
                                    +	Query.FieldByName('GlosaRespuesta').AsString + ';'                  //  Glosa Respuesta                // SS_1147_CQU_20150106
                                    +   Query.FieldByName('Filler1').AsString + ';'                         //  Filler1                        // SS_1147_CQU_20150106
                                    +   Query.FieldByName('Filler2').AsString + ';'                         //  Filler2                        // SS_1147_CQU_20150106
                                    +   Query.FieldByName('FechaProceso').AsString + ';'                    //  FechaProceso                   // SS_1147_CQU_20150106
                                    +   Query.FieldByName('Observaciones').AsString + ';';                  //  Observaciones                  // SS_1147_CQU_20150106
                        end else begin                                                                                                         // SS_1147_CQU_20150106
                            Linea :=    Query.FieldByName('TipodeTransaccion').AsString + ';'               //  Filler                         // SS_1147_CQU_20150106
                                    +	Query.FieldByName('MontodelaTransaccion').AsString + ';'            //	Monto Facturado                // SS_1147_CQU_20150106
                                    +	Query.FieldByName('NumeroTarjetaCredito').AsString + ';'            //  N�mero Tarjeta                 // SS_1147_CQU_20150106
                                    +	Query.FieldByName('FechaVencimiento').AsString + ';'		        //	Vencimiento Tarjeta            // SS_1147_CQU_20150324 // SS_1147_CQU_20150305 // SS_1147_CQU_20150106
                                    //+   FormatDateTime( 'MM/YY', Query.FieldByName('FechaVencimiento').AsDateTime) + ';'                     // SS_1147_CQU_20150324 // SS_1147_CQU_20150324 // SS_1147_CQU_20150305
                                    +	';'															        //	Nada                           // SS_1147_CQU_20150106
                                    +	';'															        //	Nada                           // SS_1147_CQU_20150106
                                    +	'Autopista Vespucio Sur;'									        //	Filler                         // SS_1147_CQU_20150106
                                    +	';'															        //	Nada                           // SS_1147_CQU_20150106
                                    +	Query.FieldByName('NumeroIdentificadordelServicio').AsString + ';'  //  Cta. Facturacion               // SS_1147_CQU_20150106
                                    +	Query.FieldByName('Rut').AsString + ';'								//  Rut Cliente                    // SS_1147_CQU_20150106
                                    +	Query.FieldByName('TipoDocumento').AsString                         //	TipoDocumento                  // SS_1147_CQU_20150106
                                    +   RellenarCaracter(                                                   //                                 // SS_1147_CQU_20150106
                                            //Query.FieldByName('ReferenciaDelCobroEfectuado').AsString,    //  Y                              // SS_1147_CQU_20150305  // SS_1147_CQU_20150106
                                            Query.FieldByName('NumeroComprobanteFiscal').AsString,          //  Y                              // SS_1147_CQU_20150305
                                            '0', True, 10) + ';'                                            //  NumeroDocumento                // SS_1147_CQU_20150106
                                    +	RellenarCaracter('0', '0', False, 8) + ';'							//	Filler                         // SS_1147_CQU_20150106
                                    +	RellenarCaracter('0', '0', False, 3) + ';'							//  Filler                         // SS_1147_CQU_20150106
                                    +	';'															        //	Nada                           // SS_1147_CQU_20150106
                                    +	';'															        //	Nada                           // SS_1147_CQU_20150106
                                    +	';'															        //	Nada                           // SS_1147_CQU_20150106
                                    +	';';    													        //	Nada                           // SS_1147_CQU_20150106
                        end;                                                                                                                   // SS_1147_CQU_20150106
                        FLista.Add(Linea);
                        if not ActualizarDetalleComprobanteEnviado(DMConnections.BaseCAC,
                            FCodigoOperacion, Query.FieldByName('ReferenciaDelCobroEfectuado').Value,
                            Query.FieldByName('TipoComprobanteReferencia').AsString,                                                           // SS_1147_CQU_20150519
                            DescriError, Query.FieldByName('MontodelaTransaccion').AsInteger) then
                            raise Exception.Create(DescriError);
                        Next;
                    end;
                end;
                Result := (not FCancelar) and (FLista.Count > 0);
            except
                raise Exception.Create(MSG_PROCESS_ERROR);
            end;
        end;

    Resourcestring
        MSG_ERROR_PROCESS = 'No se pudo Generar el TXT';
        MSG_ERROR = 'Error';
    var
        Year, Month, Day: Word;
        Mes, Dia: AnsiString;
        CantidadRegistros : Integer;
        SumaMontosNomina : AnsiString;
    begin
        Result := False;
        try
            //Genero Datos Archivo Control
            DecodeDate(now, Year, Month, Day);                                                                                                              //Separo la fecha actual para darle el formato requerido DDMMAAAA
            if Length(IntToStr(Month)) = 1 then Mes := '0'+ IntToStr(Month) else Mes := IntToStr(Month);
            if Length(IntToStr(Day)) = 1 then Dia := '0'+ IntToStr(Day) else Dia := IntToStr(Day);
            CantidadRegistros:=SPObtenerNominaTBK.RecordCount;                                                                                              //Obtengo la Cantidad de Registros
            //Obtengo Suma Monto Transacciones
            if CantidadRegistros <> 0 then
                //ObtenerSumaMontosNomina(SumaMontosNomina)
                SumaMontosNomina := SPObtenerNominaTBK.Parameters.ParamByName('@Total').value
                                      else SumaMontosNomina:='0';
            //Encabezado
            FDatosArchivocontrol:= Dia +                               //Dia
                                   Mes +                               //Mes
                                   inttostr(Year) + ';' +              //A�o
                                   SumaMontosNomina + ';' +            //Total
                                   inttostr(CantidadRegistros) + ';' + //Cantidad
                                   '' + ';' ;                          //Filler
            FMontoTotal     := strtoint(SumaMontosNomina);
            FCantidadTotal  := CantidadRegistros;
            //Genero Datos Archivo Nomina
            if QueryToTxt(SPObtenerNominaTbk) then
            Result := True;
        except
            on e: Exception do begin
                FErrorGrave := True;
                MsgBoxErr(MSG_ERROR_PROCESS, e.Message, MSG_ERROR, MB_ICONERROR);
                Exit;
            end;
        end;
    end;

    {-----------------------------------------------------------------------------
      Function Name: ActualizarLog
      Author:    lgisuk
      Date Created: 07/07/2005
      Description: Actualizo el log al finalizar
      Parameters:
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function ActualizarLog : Boolean;
    Resourcestring
         MSG_ERROR = 'No se pudo Actualizar el log de operaciones';
    Const
         STR_OBSERVACION = 'Finalizo OK!';
    var
         DescError : String;
    begin
        try
            Result := ActualizarLogOperacionesInterfaseAlFinal
                            (DMConnections.BaseCAC, FCodigoOperacion, STR_OBSERVACION , DescError);
        except
            on e: Exception do begin
               Result := False;
               MsgBoxErr(MSG_ERROR,e.Message, Self.caption, MB_ICONERROR);
            end;
        end;
    end;

resourcestring
    MSG_PROCESS_CANCEL = 'Proceso cancelado';
    MSG_PROCESS_FINALLY_WITH_ERROR = 'El proceso finaliz� con Errores';
  	MSG_PROCESS_FINALLY_OK = 'El proceso finaliz� con �xito';
    MSG_SAVE_FILE_ERROR = 'No se pudo Guardar el Archivo: ';
    MSG_ERROR = 'Error';
    MSG_SIN_GRUPOS_SELECCIONADOS = 'No se seleccionaron grupos de facturacion a incluir';
Const
    STR_REGISTER_OPERATION = 'Registrando Operacion en el Log de Operaciones...';
    STR_OBTAINING_DATA = 'Obteniendo los Datos...';
    STR_CREATE_FILE = 'Creando el Archivo...';
    STR_SAVE_FILE = 'Guardando el Archivo...';
var
    DescError: AnsiString;
begin

    if not ExistenGruposSeleccionados  then begin
        MsgBoxBalloon(MSG_SIN_GRUPOS_SELECCIONADOS, MSG_ERROR, MB_ICONQUESTION , dblGrupos );
        Exit;
    end;

  	btnProcesar.Enabled := False;       //Deshabilito el Boton
  	pbProgreso.Position := 0;           //Inicio la Barra de progreso
  	pbProgreso.Max := 4;                //Establezco como maximo la cantidad de tres operaciones
    PnlAvance.visible := True;          //El Panel de Avance es visible durante el procesamiento
    FLista.Clear;                       //Vacio la lista donde voy a generar el archivo
  	Screen.Cursor := crHourGlass;
    KeyPreview := True;
    FMontoTotal     := 0 ;
    FCantidadTotal  := 0 ;
    lblCambiarNombre.Enabled := btnProcesar.Enabled;    // SS_1147_CQU_20150106

    try


        //Registro la operaci�n en el Log
        Lblmensaje.Caption := STR_REGISTER_OPERATION;             //Informo Tarea
        pbProgreso.Position := pbProgreso.Position + 1;           //Muestro el progreso
        Application.ProcessMessages;                              //Refresco la pantalla
        if not RegistrarOperacion then begin
            FErrorGrave := True;
            Exit;
        end;

    	//Obtenemos los datos
    	Lblmensaje.Caption := STR_OBTAINING_DATA;                 //Informo Tarea
    	pbProgreso.Position := pbProgreso.Position + 1;           //Muestro el progreso
    	Application.ProcessMessages;                              //Refresco la pantalla
    	if not ObtenerDebitos then begin
            FErrorGrave := True;
            Exit;
        end;

        //Inicio la transaccion
        //DMConnections.BaseCAC.BeginTrans;                                     //SS_1385_NDR_20150922
        DMConnections.BaseCAC.Execute('BEGIN TRAN frmGenerarArchivoNomina');    //SS_1385_NDR_20150922


      //Creamos el archivo
    	Lblmensaje.Caption := STR_CREATE_FILE;                    //Informo Tarea
    	pbProgreso.Position := pbProgreso.Position + 1;           //Muestro el progreso
    	Application.ProcessMessages;                              //Refresco la pantalla
    	if not GenerarArchivo then begin
            FErrorGrave := True;
            //Si hay alg�n error debo hacer rollback dado que por cada l�nea estoy
            //grabando el comprobante en la base de datos.
            //DMConnections.BaseCAC.RollbackTrans;                              //SS_1385_NDR_20150922
            DMConnections.BaseCAC.Execute('IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION frmGenerarArchivoNomina END');	//SS_1385_NDR_20150922
            Exit;
        end;

    	//Guarda el Archivo
      Lblmensaje.Caption := STR_SAVE_FILE;                       //Informo Tarea
    	pbProgreso.Position := pbProgreso.Position + 1;            //Muestro el progreso
    	Application.ProcessMessages;                               //Refresco la pantalla
    	{if not GuardarArchivo(FCodigoOperacion) then begin
            FErrorGrave := True;
            Exit;
        end;}

        if GuardarArchivo(FCodigoOperacion) and not FErrorGrave then begin
            //Acepto la Transaccion
            //DMConnections.BaseCAC.CommitTrans;                                          //SS_1385_NDR_20150922
            DMConnections.BaseCAC.Execute('COMMIT TRAN frmGenerarArchivoNomina');					//SS_1385_NDR_20150922

        end else begin
            //Cancelo la transccion
            //DMConnections.BaseCAC.RollbackTrans;                                        //SS_1385_NDR_20150922
            DMConnections.BaseCAC.Execute('IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION frmGenerarArchivoNomina END');	//SS_1385_NDR_20150922

            //Elimino los Archivos
            DeleteFile(FNombreArchivoNomina);
            DeleteFile(FNombreArchivoControl);
            FErrorGrave := True;
            MsgBoxErr(MSG_SAVE_FILE_ERROR, DescError, MSG_ERROR, MB_ICONERROR);
            Exit;
        end;
        //ss750
        ActualizarLogOperacionesInterfaseAlFinal(DMConnections.BaseCAC, FCodigoOperacion, FCantidadTotal, FMontoTotal, '', DescError);
      //Actualizo el log al Final
      ActualizarLog;

    finally
      	Screen.Cursor := crDefault;                                 //Cursor en Default
        pbProgreso.Position := 0;                                   //Inicio la Barra de progreso
        PnlAvance.visible := False;                                 //Oculto el Panel de Avance
    		lblMensaje.Caption := '';                                   //Borro el mensaje
        if FCancelar then begin
            //Muestro mensaje que el proceso fue cancelado
            MsgBox(MSG_PROCESS_CANCEL, Self.Caption, MB_OK + MB_ICONINFORMATION);
        end else if FErrorGrave then begin
            //Muestro mensaje que finalizo por un error
            MsgBox(MSG_PROCESS_FINALLY_WITH_ERROR, Self.Caption, MB_OK + MB_ICONINFORMATION);
        end else begin
            //muestro mensaje que el proceso finalizo exitosamente
            MsgBox(MSG_PROCESS_FINALLY_OK, Self.Caption, MB_OK + MB_ICONINFORMATION);
            //Muestro el Reporte de Finalizacion del Proceso
            GenerarReportedeFinalizacion(FCodigoOperacion);
        end;
        KeyPreview := False;
  	end;
end;

{-----------------------------------------------------------------------------
  Function Name: FormKeyDown
  Author:    lgisuk
  Date Created: 11/03/2005
  Description: Permito cancelar el proceso
  Parameters: Sender: TObject;var Key: Word; Shift: TShiftState
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoNomina.FormKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
begin
    case Key of
        VK_ESCAPE: FCancelar := True;
    else
        FCancelar := False;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: SalirBTNClick
  Author:    lgisuk
  Date Created: 11/03/2005
  Description: Salgo del formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoNomina.btnSalirClick(Sender: TObject);
begin
	Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormCloseQuery
  Author:    lgisuk
  Date Created: 11/03/2005
  Description: Permite salir solo si no esta procesando
  Parameters: Sender: TObject;var CanClose: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoNomina.FormCloseQuery(Sender: TObject;var CanClose: Boolean);
begin
    //Permite salir solo si no esta procesando
	CanClose := not PnlAvance.visible;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 11/03/2005
  Description: Lo libero de memoria
  Parameters: Sender: TObject;var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoNomina.FormClose(Sender: TObject;var Action: TCloseAction);
begin
	Action := caFree;
end;

{-----------------------------------------------------------------------------
  Function Name : lblCambiarNombreClick
  Author        : CQuezada
  Firma         : SS_1147_CQU_20150106
  Date Created  : 22/01/2015
  Description   : Consulta el nuevo nombre para Vespucio Sur ya que ellos no usan secuencia
  Parameters    : Sender: TObject;
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoNomina.lblCambiarNombreClick(Sender: TObject);                            // SS_1147_CQU_20150106
resourcestring                                                                                      // SS_1147_CQU_20150106
    Titulo  =   'Cambiar Nombre de Archivo';                                                        // SS_1147_CQU_20150106
    Mensaje =   'Ingrese el nombre del archivo a generar';                                          // SS_1147_CQU_20150106
var                                                                                                 // SS_1147_CQU_20150106
    NuevoNombre,                                                                                    // SS_1147_CQU_20150106
    RespaldoDirectorio : AnsiString;                                                                // SS_1147_CQU_20150106
begin                                                                                               // SS_1147_CQU_20150106
    NuevoNombre := ExtractFileName(FNombreArchivoNomina);                                           // SS_1147_CQU_20150106
    RespaldoDirectorio := ExtractFilePath(FNombreArchivoNomina);                                    // SS_1147_CQU_20150106
    NuevoNombre := InputBox(Titulo, Mensaje, NuevoNombre);                                          // SS_1147_CQU_20150106
    if NuevoNombre <> '' then FNombreArchivoNomina := GoodDir(RespaldoDirectorio) + NuevoNombre;    // SS_1147_CQU_20150106
    lblTitulo.text := FNombreArchivoNomina;                                                         // SS_1147_CQU_20150106
end;                                                                                                // SS_1147_CQU_20150106

initialization
    FLista   := TStringList.Create;
finalization
    FreeAndNil(Flista);
end.
