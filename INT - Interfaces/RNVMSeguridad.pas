{

Etiqueta    : 20160315 MGO
Descripci�n : Se utiliza InstallIni en vez de ApplicationIni

}
unit RNVMSeguridad;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Util, UtilProc, StdCtrls, ExtCtrls, PeaProcs;

type
  TfrmSeguridadRNVM = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    txtClave: TEdit;
    txtValidacion: TEdit;
    Bevel1: TBevel;
    btnAceptar: TButton;
    btnCancelar: TButton;
    procedure btnAceptarClick(Sender: TObject);
  private
    { Private declarations }
  public
    function Inicializar: Boolean;
  end;

var
  frmSeguridadRNVM: TfrmSeguridadRNVM;

implementation

{$R *.dfm}

{ TfrmSeguridadRNVM }

function TfrmSeguridadRNVM.Inicializar: Boolean;
begin
    { INICIO : 20160315 MGO
    txtClave.Text := ApplicationIni.ReadString('RNVMSeguridad', 'Password', '');
    }
    txtClave.Text := InstallIni.ReadString('RNVMSeguridad', 'Password', '');
    // FIN : 20160315 MGO
    Result := True;
end;

procedure TfrmSeguridadRNVM.btnAceptarClick(Sender: TObject);
resourcestring
    MSG_PASSWORD_MISMATCH = 'Las Claves Ingresadas No Coinciden';
    MSG_BLANK_PASSWORD = 'La Clave No Puede Ser Vac�a';
    MSG_ERROR = 'Validaci�n de Clave de Acceso';
begin
    if not ValidateControls([txtClave, txtValidacion],
      [(Trim(txtClave.Text) <>  ''), (Trim(txtClave.Text) = Trim(txtValidacion.Text))],
      MSG_ERROR, [MSG_BLANK_PASSWORD, MSG_PASSWORD_MISMATCH]) then Exit;
    { INICIO : 20160315 MGO
    ApplicationIni.WriteString('RNVMSeguridad', 'Password', txtClave.Text);
    }
    InstallIni.WriteString('RNVMSeguridad', 'Password', txtClave.Text);
    // FIN : 20160315 MGO
    Close;
end;

end.

