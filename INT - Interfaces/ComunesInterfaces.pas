{-----------------------------------------------------------------------------
 File Name: ComunesInterfaces.pas
 Author:    lgisuk
 Date Created: 25/11/2004
 Language: ES-AR
 Description:  Procedimientos y Funciones Comunes a todos los formularios
  Revisi�n   : 2
  Autor      : mpiazza
  Fecha      : 17/04/2009
  Descripci�n: ss 750 se agrego esta funcion sobrecargada
              ActualizarLogOperacionesInterfaseAlFinal con el montoarchivo como
              nuevo campo

 Revision	:	3
 Author		: 	ebaeza
 Date		:	14/06/2011
 firma      :   SS-377-EBA-20110613
 Descripcion:	Cambio estructura directorios de acceso a imagenes de transitos

 Author		: 	CQuezada
 Date		:	19/05/2015
 firma      :   SS_1147_CQU_20150519
 Descripcion:	Se agrega el TipoComprobante a la funci�n ActualizarDetalleComprobanteEnviado
-----------------------------------------------------------------------------}
unit ComunesInterfaces;

interface

uses //Comunes Interfaces
     DMConnection,
     UtilProc,
     UtilDB,
     Util,
     ConstParametrosGenerales,
     PeaTypes,
     MOPInfBind, 
     ImgProcs,
     ImgTypes,
     JPEGPlus,
     Filtros,
     //General
     Classes, ADODB, VariantComboBox, Windows, Variants, Controls, SysUtils, Forms, frmRptErroresSintaxis;
type
    // Estados de un Proceso de Contabilizaci�n.
    TEstadoContabilizacion = (ecDesconocida, ecIniciada, ecFinalizada, ecDeshaciendo, ecDeshecha);

resourcestring
    MSG_PROCESO_CONTABILIZACION_INICIADO    = 'Iniciado';
    MSG_PROCESO_CONTABILIZACION_FINALIZADO  = 'Finalizado';
    MSG_PROCESO_CONTABILIZACION_DESHACIENDO = 'Deshaciendo';
    MSG_PROCESO_CONTABILIZACION_DESHECHO    = 'Deshecho';

const
    XML_DISK_ID         = 'DISC_';
    XML_SEP_REPROCESO   = '_';
    XML_FOLDER_EXISTS   = 'Carpeta_Existe';
    //parametros generales
    DIR_DESTINO_XML_INFRACCIONES    = 'DIR_Destino_XML_Infracciones';
    INFRACCIONES_XML_TAMANIO_CD     = 'Infracciones_XML_Tama�o_CD';
    DIR_RESPUESTA_XML_INFR_PROC     = 'DIR_Resp_XML_Inf_Procesadas';
    DIR_ORIGEN_RESPUESTA_XML_INFR   = 'DIR_Respuestas_XML_Infractores';

    // Identifde Estados de un Proceso de Contabilizaci�n.
    CONST_ESTADO_CONTABILIZACION_INICIADO       = 1;
    CONST_ESTADO_CONTABILIZACION_FINALIZADO     = 2;
    CONST_ESTADO_CONTABILIZACION_DESHACIENDO    = 3;
    CONST_ESTADO_CONTABILIZACION_DESHECHO       = 4;

    //Author lgisuk
    Function RegistrarOperacionEnLogInterface(Conn: TAdoConnection;CodigoModulo:Integer;NombreArchivo,Usuario,Observaciones:string;recibido, Reprocesamiento:boolean;FechaHoraRecepcion:tdatetime;UltimoRegistroEnviado: integer;var CodigoOperacionInterfase:integer;Var DescError:Ansistring):boolean; overload;
    Function ActualizarObservacionesEnLogInterface(Base:TADOConnection;SP:tadostoredproc;CodigoOperacionInterfase:Integer;Observaciones:string;Agrega:boolean):boolean; overload;
    Function ActualizarLogOperacionesInterfaseAlFinal(Conn: TAdoConnection;CodigoOperacionInterfase:integer;Observaciones:string;Var DescError:Ansistring):boolean; overload;
    Function  RegistrarCancelacionInterfaz(Conn : TAdoConnection; CodigoOperacionInterfase : Integer) : Boolean;
    Function ActualizarNumeroSecuencia(Conn: TAdoConnection;CodigoOperacionInterfase:integer;NumeroSecuencia:integer;Var DescError:Ansistring):boolean;
    function AgregarDetalleMandatoRecibido(Conn: TAdoConnection; CodigoOperacionInterfase: Integer;CodigoConvenio: Integer; TipoOperacion: string; CodigoRespuesta: string; GlosaRespuesta: string; var DescError: AnsiString): Boolean;
    function AgregarDetalleMandatoEnviado(Conn: TAdoConnection; CodigoOperacionInterfase: Integer;CodigoConvenio: Integer; TipoOperacion: string; var DescError: AnsiString): Boolean;
    //function ActualizarDetalleComprobanteEnviado(Conn: TAdoConnection; CodigoOperacionInterfase: Integer; NumeroComprobante: Int64; var DescError: AnsiString ; ComprobanteMonto : Int64 = 0): Boolean;                           // SS_1147_CQU_20150519
    function ActualizarDetalleComprobanteEnviado(Conn: TAdoConnection; CodigoOperacionInterfase: Integer; NumeroComprobante: Int64; TipoComprobante : string ; var DescError: AnsiString ; ComprobanteMonto : Int64 = 0): Boolean;  // SS_1147_CQU_20150519
    Function ExistePalabra(frase,buscar:string):boolean;
  	function AgregarErrorInterfase(Conn: TAdoConnection; CodigoOperacionInterfase : Integer; DescripcionError : String; CodigoTipoError: Integer = -1) : Boolean; // SS 909
    function AgregarErrorInterfaz(Conn: TAdoConnection; CodigoOperacionInterfase : Integer; CodigoModulo : Integer; CodigoError : Integer; Fecha : TDateTime; DescripcionError : String) : Boolean;
    //Author flamas
    Function CargarFechasDeInterfasesEjecutadas( sp : TADOStoredProc; CodigoModulo:Integer; Combo : TVariantComboBox ) : boolean;
    function ValidarFechaAAAAMMDD( sFecha : string ) : boolean;
    function VerificarArchivoProcesado( Base:TADOConnection; sFileName : string ) : boolean;
    function ValidarHeaderFooterPRESTO(var sArchivo : TStringList; sNombreArchivo, sEPS, sGlosa : string) : boolean;
    //Author mtraversa
    function RegistrarComprobanteEnviadoEnLog(Conn: TAdoConnection; TipoComprobante: ShortString;NumeroComprobante: LongInt; CodigoOperacionInterfase: Integer; var DescError: AnsiString): Boolean;
    function MsgProcesoFinalizado(sMensajeOK, sMensajeError, sTitulo: AnsiString; RProcesados, RExito, RError: integer):boolean;
    //Author ndonadio
    function MoverArchivoProcesado(MsgTitulo, PathNameOrigen, PathNameDestino: AnsiString; NoInfo: Boolean = False): boolean;
    Function RegistrarOperacionEnLogInterface(Conn: TAdoConnection; CodigoModulo:Integer;Fecha: TDateTime; NombreArchivo,Usuario,Observaciones:string; CorrelativoDisco, CorrelativoArchivo: Integer; IndiceReprocesamiento: Byte; var CodigoOperacionInterfase:integer; Var DescError:Ansistring):boolean; overload;
    Function ActualizarLogOperacionesInterfaseAlFinal(Conn: TAdoConnection; CodigoOperacionInterfase: integer; Observaciones:string; CorrDisco, CorrArchivo, IndiceReprocesamiento: Integer; FileName: AnsiString; Var DescError:Ansistring):boolean; overload;
    function TamanioArchivo(FullPathFileName: AnsiString): integer;
    function NombreArchivoXML(PathDestino: AnsiString; NumCorrDisco, NumCorrArchivo: Integer; Fecha: TDateTime; pConcesionaria: Integer): String;
    function NombreCarpetaXML(PathDestino: AnsiString; NumCorrDisco: Integer; IndiceReproceso: Byte): AnsiString;
    function CrearCarpetaXML(PathDestino: AnsiString; NumCorrDisco: Integer; IndiceReproceso: Byte; var Carpeta: AnsiString; var Error: AnsiString ): Boolean;
    function VerificarTamanioUnidad(Path: AnsiString): Int64;
    function InfraccionToXML(SourceSP: TADOStoredProc; ImagePath: AnsiString; FNombreCortoConcesionaria: String; var XMLContainer: IXMLDenunciosType; var CantInfracciones: Integer; var DescriError: AnsiString): Boolean;
    function TransaccionesToXML( SourceSP: TADOStoredProc; var XMLContainer: IXMLDenunciosType;var CantInfracciones: Integer;  var DescriError: AnsiString): boolean;
    function ImageURLEncode(const Image: AnsiString): utf8String;
    function FechaHoraAAAAMMDDToDateTime(Conn: TAdoConnection;  strFecha: Ansistring; var outFecha: TDateTime; var Error: AnsiString): boolean;
    Function ActualizarObservacionesEnLogInterface(Base: TADOConnection; CodigoOperacionInterfase: Integer; Observaciones: AnsiString; Agrega: Boolean; var Error: AnsiString): Boolean; overload;
    //Author ggomes
    procedure ObtenerDatosProcesoContabilizacion(Conn: TADOConnection; NumeroProceso: Integer; var FechaJornada: TDateTime; var FechaHoraInicio: TDateTime; var FechaHoraCierre: TDateTime; var Usuario: AnsiString; var Estado: TEstadoContabilizacion);
    //Author vpaszkowicz
    function FechaValidaEnNombreArchivo(unaFechaStr: string): Boolean;
    Function GenerarReporteErroresSintaxis(TituloReporte: string; ListaErrores: TStringList; DirectorioErrores: AnsiString; CodigoModulo: integer): Boolean;
    Function RegistrarOperacionEnLogInterface(Conn: TAdoConnection;CodigoModulo:Integer;NombreArchivo,Usuario,Observaciones:string;recibido, Reprocesamiento:boolean;FechaHoraRecepcion:tdatetime;UltimoRegistroEnviado: integer;var CodigoOperacionInterfase:integer; LineasArchivo: integer; MontoArchivo: int64; var DescError:Ansistring):boolean; overload;

    //Author Mpiazza
    function ActualizarLogOperacionesInterfaseAlFinal(Conn: TAdoConnection; CodigoOperacionInterfase, LineasArchivo : integer; MontoArchivo : Int64; Observaciones : string; Var DescError: string): boolean; overload;


implementation

resourcestring
    MSG_ERROR = 'Error';

{******************************** Function Header ******************************
Function Name: RegistrarOperacionEnLogInterface
Author : vpaszkowicz
Date Created : 20/03/2008
Description : Registra la operaci�n en el log y adem�s graba las lineas del ar-
chivo y el monto recibido. Usado para la recepci�n de rendiciones.
Parameters : Conn: TAdoConnection;CodigoModulo:Integer;NombreArchivo,Usuario,Observaciones:string;recibido, Reprocesamiento:boolean;FechaHoraRecepcion:tdatetime;UltimoRegistroEnviado: integer;var CodigoOperacionInterfase:integer; LineasArchivo: integer; MontoArchivo: int64; var DescError:Ansistring
Return Value : boolean
*******************************************************************************}
function RegistrarOperacionEnLogInterface(Conn: TAdoConnection;CodigoModulo:Integer;NombreArchivo,Usuario,Observaciones:string;recibido, Reprocesamiento:boolean;FechaHoraRecepcion:tdatetime;UltimoRegistroEnviado: integer;var CodigoOperacionInterfase:integer; LineasArchivo: integer; MontoArchivo: int64; var DescError:Ansistring):boolean; overload;
var sp : TADOStoredProc;
begin
  	CodigoOperacionInterfase := 0;
    Result := False;
    sp := TADOStoredProc.Create(Nil);
    try
        try
            sp.Connection := Conn;
            sp.ProcedureName := 'AgregarLogOperacionesInterfases';
            with SP.Parameters do begin
                Refresh;
                ParamByName('@CodigoModulo').Value:= CodigoModulo;
                ParamByName('@NombreArchivo').Value:= NombreArchivo;
                ParamByName('@Usuario').Value:= Usuario;
                ParamByName('@Observaciones').Value:= Observaciones;
                ParamByName('@Reprocesamiento').Value:= Reprocesamiento;
                ParamByName('@FechaHoraRecepcion').Value:= FechaHoraRecepcion;
                ParamByName('@UltimoRegistroEnviado').Value:= Iif( UltimoRegistroEnviado = 0, null, UltimoRegistroEnviado );
                ParamByName('@CodigoOperacionInterfase').Value := 0;
                ParamByName('@LineasArchivo').Value := LineasArchivo;
                ParamByName('@MontoArchivo').Value := MontoArchivo;
            end;
            SP.ExecProc;                                                                             //Ejecuto el procedimiento
            CodigoOperacionInterfase:= SP.Parameters.ParamByName('@CodigoOperacionInterfase').Value; //Obtengo el Id
            result:=true;
		    except
            on E : Exception do begin
                DescError := E.Message;
            end;
        end;
    finally
        SP.Free;
	  end;
end;

{-----------------------------------------------------------------------------
  Function Name: RegistrarCancelacionInterfaz
  Author:    lgisuk
  Date Created: 11/10/2007
  Description: Registra que el proceso ha sido cancelado por el usuario
               y la fecha/hora en la que se produjo la cancelacion
  Parameters: Conn: TAdoConnection;CodigoOperacionInterfase:integer;
  Return Value: boolean
-----------------------------------------------------------------------------
  Revision 1
  lgisuk
  05/11/2007
  Ahora utilizamos el mismo stored procedure para registrar la finalizacion
  correcta y la cancelacion.
-----------------------------------------------------------------------------}
Function  RegistrarCancelacionInterfaz(Conn : TAdoConnection; CodigoOperacionInterfase : Integer) : Boolean;
Const
  STR_PROCESS_CANCEL = 'Proceso Cancelado por el Usuario';
var
  sp : TADOStoredProc;
begin
    Result := False;
    sp := TADOStoredProc.Create(Nil);
    try
        try
            sp.Connection := Conn;
            sp.ProcedureName := 'ActualizarLogOperacionesInterfaseAlFinal';
            with SP.Parameters do begin
                Refresh;
                ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacionInterfase;
                ParamByName('@Observaciones').Value := STR_PROCESS_CANCEL;
                //mbecerra, 20-Mayo-2009
                //Este par�metro no existe en el stored de Producci�n
                //ParamByName('@FinalizoOK').Value := 0;
            end;
            SP.ExecProc;                                                                             //Ejecuto el procedimiento
            Result := True;
		    except
            on E : Exception do begin
                MsgBoxErr(MSG_ERROR, E.message, 'Error Al', 0);                                        //el stored procedure podria fallar
            end;
        end;
    finally
        SP.Free;
  	end;
end;

{-----------------------------------------------------------------------------
  Function Name: RegistrarOperacionEnLogInterface
  Author:    lgisuk
  Date Created: 12/01/2005
  Description: Registra la Operacion en el Log 
  Parameters: Conn: TAdoConnection;CodigoModulo:Integer;Fecha:tdatetime;NombreArchivo,Usuario,Observaciones:string;recibido, Reprocesamiento:boolean;FechaHoraRecepcion:tdatetime;UltimoRegistroEnviado: integer;var CodigoOperacionInterfase:integer;Var DescError:Ansistring
  Return Value: boolean
-----------------------------------------------------------------------------}
Function  RegistrarOperacionEnLogInterface(Conn : TAdoConnection; CodigoModulo : Integer; NombreArchivo, Usuario, Observaciones : String; Recibido, Reprocesamiento : Boolean; FechaHoraRecepcion : TDateTime; UltimoRegistroEnviado : Integer; var CodigoOperacionInterfase : Integer; Var DescError : Ansistring) : Boolean; overload;
var sp : TADOStoredProc;
begin
  	CodigoOperacionInterfase := 0;
    Result := False;
    sp := TADOStoredProc.Create(Nil);
    try
        try
            sp.Connection := Conn;
            sp.ProcedureName := 'AgregarLogOperacionesInterfases';
            with SP.Parameters do begin
                Refresh;
                ParamByName('@CodigoModulo').Value:= CodigoModulo;
                ParamByName('@NombreArchivo').Value:= NombreArchivo;
                ParamByName('@Usuario').Value:= Usuario;
                ParamByName('@Observaciones').Value:= Observaciones;
                ParamByName('@Reprocesamiento').Value:= Reprocesamiento;
                ParamByName('@FechaHoraRecepcion').Value:= FechaHoraRecepcion;
                ParamByName('@UltimoRegistroEnviado').Value:= Iif( UltimoRegistroEnviado = 0, null, UltimoRegistroEnviado );
                ParamByName('@CodigoOperacionInterfase').Value := 0;
            end;
            SP.ExecProc;                                                                             //Ejecuto el procedimiento
            CodigoOperacionInterfase:= SP.Parameters.ParamByName('@CodigoOperacionInterfase').Value; //Obtengo el Id
            result:=true;
		    except
            on E : Exception do begin
                DescError := E.Message;
            end;
        end;
    finally
        SP.Free;
	  end;
end;

{-----------------------------------------------------------------------------
  Function Name: RegistrarOperacionEnLogInterface
  Author:    ndonadio
  Date Created: 09/06/2005
  Description:   Registra operacion en Log de Interface SOLO para interfaz XML Infractores...
  Parameters: Conn: TAdoConnection; CodigoModulo:Integer; Fecha: TDateTime; NombreArchivo,Usuario,Observaciones:string; CorrelativoDisco, CorrelativoArchivo: Integer; IndiceReprocesamiento: Byte; var CodigoOperacionInterfase:integer; Var DescError:Ansistring
  Return Value: boolean
-----------------------------------------------------------------------------}
Function  RegistrarOperacionEnLogInterface(Conn: TAdoConnection; CodigoModulo: Integer; Fecha: TDateTime; NombreArchivo, Usuario, Observaciones: string; CorrelativoDisco, CorrelativoArchivo: Integer; IndiceReprocesamiento: Byte; var CodigoOperacionInterfase: Integer; Var DescError: Ansistring): boolean; overload;
resourcestring
    MSG_ERROR_INSERTING_DISK_REGISTRATION = 'No se pudo registrar la informaci�n del Disco en el Log de Operaciones';
var
    qr : TADOQuery;
begin
    qr := TADOQuery.Create(nil);
    DescError := '';
    try
        try
            // Registro la operacion
            result := RegistrarOperacionEnLogInterface(Conn, CodigoModulo,NombreArchivo, Usuario, Observaciones, False, (IndiceReprocesamiento > 0), Fecha, 0, CodigoOperacionInterfase, DescError);
            if not result then raise Exception.Create(DescError);
            // Agrego en la tabla de extension... DetalleDiscoInfracciones
            qr.Connection := Conn;
            qr.SQL.Text :=  Format( 'INSERT INTO DetalleDiscoInfracciones (CodigoOperacionInterfase, CorrelativoDisco, CorrelativoArchivo, IndiceReproceso) VALUES ( %d, %d, %d, %d )' , [CodigoOperacionInterfase,CorrelativoDisco, CorrelativoArchivo,IndiceReprocesamiento]);
            qr.Prepared := True;
            if qr.ExecSQL <> 1 then raise Exception.Create(MSG_ERROR_INSERTING_DISK_REGISTRATION);
            Result := True;
        except
            on E : Exception do begin
                Result := False;
                DescError := e.Message;
            end;
        end;
    finally
        if Assigned(qr) then  qr.Free;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ActualizarNumeroSecuenciaTBK
  Author:    lgisuk
  Date Created: 06/04/2005
  Description: Actualizo el numero de Secuencia
  Parameters: Conn: TAdoConnection;CodigoOperacionInterfase:string;NumeroSecuencia:string;Var DescError:Ansistring
  Return Value: boolean
-----------------------------------------------------------------------------}
Function  ActualizarNumeroSecuencia(Conn : TAdoConnection; CodigoOperacionInterfase : Integer; NumeroSecuencia : Integer; Var DescError : AnsiString) : Boolean;
var sp : TADOStoredProc;
begin
    Result := False;
    sp := TADOStoredProc.Create(Nil);
    try
        try
            sp.Connection := Conn;
            sp.ProcedureName := 'ActualizarNumeroSecuenciaTBK';
            with SP.Parameters do begin
                Refresh;
                ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacionInterfase;
                ParamByName('@NumeroSecuencia').Value := NumeroSecuencia;
            end;
            SP.ExecProc;                                                                             //Ejecuto el procedimiento
            Result := True;
		    except
            on e : Exception do begin
                DescError := e.Message;
            end;
        end;
    finally
        SP.Free;
	  end;
end;


{-----------------------------------------------------------------------------
  Function Name: ActualizarLogOperacionesInterfaseAlFinal
  Author:    lgisuk
  Date Created: 04/07/2005
  Description: Asigna la fecha de finalizaci�n al final y las observaciones
  Parameters: Conn: TAdoConnection;CodigoOperacionInterfase:integer;Observaciones:string;Var DescError:Ansistring
  Return Value: boolean
-----------------------------------------------------------------------------}
Function  ActualizarLogOperacionesInterfaseAlFinal(Conn : TAdoConnection; CodigoOperacionInterfase : Integer; Observaciones : String; Var DescError : Ansistring) : Boolean;
var sp : TADOStoredProc;
begin
    Result := False;
    sp := TADOStoredProc.Create(Nil);
    try
        try
            sp.Connection := Conn;
            sp.ProcedureName := 'ActualizarLogOperacionesInterfaseAlFinal';
            with SP.Parameters do begin
                Refresh;
                ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacionInterfase;
                ParamByName('@Observaciones').Value := Observaciones;
            end;
            SP.ExecProc;                                                                             //Ejecuto el procedimiento
            Result := True;
		    except
            on E : Exception do begin
                DescError := E.Message;
            end;
        end;
    finally
        SP.Free;
  	end;
end;


{-----------------------------------------------------------------------------
  Function Name: ActualizarObservacionesEnLogInterface
  Author:    lgisuk
  Date Created: 13/12/2004
  Description: Permite Actualizar la Observacion de una operacion del log
  Parameters: SP:tadostoredproc;CodigoOperacionInterfase:Integer;Observaciones:string
  Return Value: Integer
-----------------------------------------------------------------------------
  Revision 1
  lgisuk
  26/10/2007
  Agrego actualizaci�n parametros SP

  Revision : 2
    Date: 26/02/2009
    Author: pdominguez
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura
-----------------------------------------------------------------------------}
Function ActualizarObservacionesEnLogInterface(Base:TADOConnection; SP:tadostoredproc; CodigoOperacionInterfase:Integer; Observaciones:string; Agrega:boolean):boolean; overload;
var SqlQuery, Value : String;
begin
    // Nombre del SP en OP_CAC -> ActualizarObservacionesLogOperacionesInterfase
    Result := False;
    SqlQuery:='SELECT Observaciones FROM LogOperacionesInterfases WITH (NOLOCK) WHERE CodigoOperacionInterfase = '+IntToStr(CodigoOperacionInterfase);
   	Value := QueryGetValue(Base, SqlQuery);
    if Agrega = True then begin
        Value := Value + Observaciones;
    end else begin
        Value := Observaciones;
    end;
    with SP.Parameters do begin
        ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacionInterfase;
        ParamByName('@Observaciones').Value := Value;
    end;
    try
        SP.ExecProc;
        Result := True;                                                                             //Ejecuto el procedimiento
    except
        on E : Exception do begin
            MsgBoxErr(MSG_ERROR, E.message, 'Error Al', 0);                                        //el stored procedure podria fallar
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: ActualizarObservacionesEnLogInterface
Author : ndonadio
Date Created : 29/09/2005
Description :  version de funcion con el mismo nombre pero con dos grandes diferencias
                    1) No requiere se le pase un SP.
                    2) Si falla, no manda mensaje, sino que devuelve False y el error en Error: AnsiString;
Parameters : Base: TADOConnection; CodigoOperacionInterfase: Integer; Observaciones: AnsiString; Agrega: Boolean; var Error: AnsiString
Return Value : Boolean

Revision : 1
    Date: 26/02/2009
    Author: pdominguez
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura
*******************************************************************************}
Function ActualizarObservacionesEnLogInterface(Base: TADOConnection; CodigoOperacionInterfase: Integer;
    Observaciones: AnsiString; Agrega: Boolean; var Error: AnsiString): Boolean; overload;
var
    SqlQuery,
    Value       : AnsiString;
    SP          : TADOStoredProc;
begin
    // Nombre del SP en OP_CAC -> ActualizarObservacionesLogOperacionesInterfase
    Result := False;

    SP := TADOStoredProc.Create(nil);
    SP.Connection := Base;
    SP.ProcedureName := 'ActualizarObservacionesLogOperacionesInterfase';

    SqlQuery := 'SELECT Observaciones FROM LogOperacionesInterfases WHERE WITH (NOLOCK) CodigoOperacionInterfase = '+inttostr(CodigoOperacionInterfase);
    try
        Value := QueryGetValue(Base, SqlQuery);
    except
        on e:exception do begin
            Error := e.Message;
            Exit;
        end;
    end;

    if Agrega then Value := Value + Observaciones else Value := Observaciones;

    try
        with SP.Parameters do begin
            Refresh;
            ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacionInterfase;
            ParamByName('@Observaciones').Value := Value;
        end;
        SP.ExecProc;    //Ejecuto el procedimiento
        Result := True;
    except
        on e:exception do begin
            Error := e.Message;
            Exit;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: AgregarDetalleMandatoRecibido
  Author:    lgisuk
  Date Created: 22/06/2005
  Description: permite registrar el mandato recibido
  Parameters: Conn: TAdoConnection; CodigoOperacionInterfase: Integer; CodigoConvenio: Integer; TipoOperacion: string; CodigoRespuesta: string; GlosaRespuesta: string; var DescError: AnsiString
  Return Value: Boolean
-----------------------------------------------------------------------------}
function AgregarDetalleMandatoRecibido(Conn: TAdoConnection; CodigoOperacionInterfase: Integer; CodigoConvenio: Integer; TipoOperacion: string; CodigoRespuesta: string; GlosaRespuesta: string; var DescError: AnsiString): Boolean;
begin
    Result := False;
    try
        with TADOStoredProc.Create(Nil) do
            try
                Connection := Conn;
                ProcedureName := 'AgregarDetalleMandatoRecibido';
                with Parameters do begin
                    Refresh;
                    ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacionInterfase;
                    ParamByName('@CodigoConvenio').Value           := CodigoConvenio;
                    ParamByName('@TipoOperacion').Value            := TipoOperacion;
                    ParamByName('@CodigoRespuesta').Value          := CodigoRespuesta;
                    ParamByName('@GlosaRespuesta').Value           := GlosaRespuesta;
                end;
                ExecProc;
                Result := True;
            finally
                Free;
            end;
    except
        on E: Exception do
			DescError := E.Message;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: AgregarDetalleMandatoEnviado
  Author:    lgisuk
  Date Created: 22/06/2005
  Description: Permite registrar el mandato enviado
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
function AgregarDetalleMandatoEnviado(Conn: TAdoConnection; CodigoOperacionInterfase: Integer; CodigoConvenio: Integer; TipoOperacion: string; var DescError: AnsiString): Boolean;
begin
    Result := False;
    try
        with TADOStoredProc.Create(Nil) do
            try
                Connection := Conn;
                ProcedureName := 'AgregarDetalleMandatoEnviado';
                with Parameters do begin
                    Refresh;
                    ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacionInterfase;
                    ParamByName('@CodigoConvenio').Value := CodigoConvenio;
                    ParamByName('@TipoOperacion').Value := TipoOperacion;
                end;
                ExecProc;
                Result := True;
            finally
                Free;
            end;
    except
        on E : Exception do begin
    			  DescError := E.Message;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: ActualizarDetalleComprobanteEnviado
Author : vpaszkowicz
Date Created : 12/07/2007
Description : Graba los datos de un comprobante utilizado para la generaci�n de un d�bito
Parameters : Conn: TAdoConnection; CodigoOperacionInterfase: Integer; NumeroComprobante: Int64; var DescError: AnsiString
Return Value : Boolean
*******************************************************************************}
//function ActualizarDetalleComprobanteEnviado(Conn: TAdoConnection; CodigoOperacionInterfase: Integer; NumeroComprobante: Int64; var DescError: AnsiString ; ComprobanteMonto : Int64 = 0): Boolean;                           // SS_1147_CQU_20150519
function ActualizarDetalleComprobanteEnviado(Conn: TAdoConnection; CodigoOperacionInterfase: Integer; NumeroComprobante: Int64; TipoComprobante : string; var DescError: AnsiString ; ComprobanteMonto : Int64 = 0): Boolean;   // SS_1147_CQU_20150519
begin
    Result := False;
    try
        with TADOStoredProc.Create(Nil) do
            try
                Connection := Conn;
                ProcedureName := 'ActualizarDetalleComprobanteEnviado';
                with Parameters do begin
                    Refresh;
                    ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacionInterfase;
                    ParamByName('@NumeroComprobante').Value := NumeroComprobante;
                    ParamByName('@TipoComprobante').Value := TipoComprobante;   // SS_1147_CQU_20150519                    
                    ParamByName('@ComprobanteMonto').Value := ComprobanteMonto;
                end;
                ExecProc;
                Result := True;
            finally
                Free;
            end;
    except
        on E : Exception do begin
    			  DescError := E.Message;
        end;
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: ExistePalabra
  Author:    lgisuk
  Date Created: 07/12/2004
  Description: verifica si existe una palabra dentro de una frase
  Parameters: frase,buscar:string
  Return Value: boolean
-----------------------------------------------------------------------------}
Function ExistePalabra(Frase, Buscar : String) : Boolean;
var Posi : Integer;
  	Bandera : Boolean;
begin
     Posi := Pos(Buscar, Frase); //Esta funcion delphi hace lo mismo que la substring que cree
     if Posi = 0 then begin
        Bandera := False;
     end
     else begin
        Bandera := True;
     end;
     Result := Bandera;
end;

{-----------------------------------------------------------------------------
  Function Name: AgregarErrorInterfase
  Author:    lgisuk
  Date Created: 17/06/2005
  Description: Agrega un mensaje de Error en la interfase
  Parameters: Conn: TAdoConnection; nCodigoOperacionInterfase, sDescripcionError : string
  Return Value: boolean

  Revision: 1
  Author: pdominguez
  Date: 05/08/2010
  Description: SS 909 - BHTU Copec Rechazos
    - Se a�ade el par�metro CodigoTipoError.
-----------------------------------------------------------------------------}
function AgregarErrorInterfase(Conn: TAdoConnection; CodigoOperacionInterfase : Integer; DescripcionError : String; CodigoTipoError: Integer = -1) : Boolean;
resourcestring
    MSG_ERROR_GENERATING_ERROR_LOG = 'Error generando log de errores';
  	MSG_ERROR = 'Error';
var
    spAgregarErrorInterfase : TADOStoredProc;
begin
  	Result := False;
    spAgregarErrorInterfase := TADOStoredProc.Create(Nil);
   	try
		    try
            with spAgregarErrorInterfase, Parameters do begin
                Connection := Conn;
                ProcedureName := 'AgregarErrorInterfase';
            	  Refresh;
                ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacionInterfase;
                ParamByName('@DescripcionError').Value := DescripcionError;
                if CodigoTipoError <> -1 then // Rev. 1 (SS 909)
                    ParamByName('@CodigoTipoError').Value := CodigoTipoError; // Rev. 1 (SS 909)
                ExecProc;
                Result := True;
            end;
        except
			      on e: Exception do begin
                MsgBoxErr(MSG_ERROR_GENERATING_ERROR_LOG, e.Message, MSG_ERROR, MB_ICONERROR);
            end;
        end;
    finally
        spAgregarErrorInterfase.Free;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: AgregarErrorInterfaz
  Author:    lgisuk
  Date Created: 19/01/2006
  Description: Agrega un mensaje de Error en la interfase
  Parameters:
  Return Value: boolean
-----------------------------------------------------------------------------}
function AgregarErrorInterfaz(Conn: TAdoConnection; CodigoOperacionInterfase : Integer; CodigoModulo : Integer; CodigoError : Integer; Fecha : TDateTime; DescripcionError : String) : Boolean;
resourcestring
    MSG_ERROR_GENERATING_ERROR_LOG = 'Error generando log de errores';
  	MSG_ERROR = 'Error';
var
    spAgregarErrorInterfaz : TADOStoredProc;
begin
  	Result := False;
    spAgregarErrorInterfaz := TADOStoredProc.Create(Nil);
   	try
		    try
            with spAgregarErrorInterfaz, Parameters do begin
                Connection := Conn;
                ProcedureName := 'AgregarErrorInterfaz';
            	  Refresh;
                ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacionInterfase;
                ParamByName('@CodigoModulo').Value := CodigoModulo;
                ParamByName('@CodigoError').Value := CodigoError;
                ParamByName('@Fecha').Value := Fecha;
				        ParamByName('@DescripcionError').Value := DescripcionError;
                ExecProc;
                Result := True;
            end;
        except
			      on e: Exception do begin
                MsgBoxErr(MSG_ERROR_GENERATING_ERROR_LOG, e.Message, MSG_ERROR, MB_ICONERROR);
            end;
        end;
    finally
        spAgregarErrorInterfaz.Free;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarFechasDeInterfasesEjecutadas
  Author:    flamas
  Date Created: 09/12/2004
  Description: Carga un VariantComoBox con las Fechas en que se ejecutaron las interfaces
  				de un m�dulo determinado y sus correspondientes c�digos de operaci�n
                sirve para reprocesar la interfase de una fecha determinada
  Parameters: sp : TADOStoreProcedure; CodigoModulo:Integer; Combo : TVariantComboBox
  Return Value: boolean
-----------------------------------------------------------------------------}
Function  CargarFechasDeInterfasesEjecutadas( sp : TADOStoredProc; CodigoModulo:Integer; Combo : TVariantComboBox ) : boolean;
resourcestring
	MSG_COULD_NOT_LOAD_INTERFACES = 'No se pudieron cargar las interfaces anteriores';
begin
  	result := False;
    try
    	with SP do begin
    		Parameters.ParamByName( '@CodigoModulo' ).Value := CodigoModulo;
    		try
    			  Open;
        		Combo.Clear;
        		while not Eof do begin
                Combo.Items.Add( FormatDateTime( 'dd-mm-yyyy', FieldByName( 'FECHA' ).AsDateTime), FieldByName( 'CODIGOOPERACIONINTERFASE' ).AsInteger );
                Next;
        		end;
        		result := True;
    		except
    			on e: Exception do
        			MsgBoxErr(MSG_COULD_NOT_LOAD_INTERFACES, e.Message, MSG_ERROR, MB_ICONERROR);
    		end;
    	end;
    finally
    	  SP.Close;
  	end;
end;

{-----------------------------------------------------------------------------
  Function Name: ValidarFecha
  Author:    flamas
  Date Created: 23/12/2004
  Description: Valida una fecha dada en formato AAAMMDD
  Parameters: sFecha : string
  Return Value: boolean
-----------------------------------------------------------------------------}
function ValidarFechaAAAAMMDD( sFecha : string ) : boolean;
begin
	  try
        EncodeDate( StrToInt(Copy(sFecha, 1, 4)), StrToInt(Copy(sFecha, 5, 2)), StrToInt(Copy(sFecha, 7, 2)));
        Result := True;
    except
    	  on exception do begin
            Result := False;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: FechaHoraAAAAMMDDToDateTime
Author : ndonadio
Date Created : 11/08/2005
Description : Valida y convierte una fecha hora en formato
                AAAAMMDDHHMMSS a un DateTime.
                Se basa en la fn YYYYMMDDHHMMSS_TO_DATETIME
                en la DB CAC del SQL.
Parameters : strFecha: Ansistring; var outFecha: TDateTime
Return Value : boolean
*******************************************************************************}
function FechaHoraAAAAMMDDToDateTime(Conn: TAdoConnection;  strFecha: Ansistring; var outFecha: TDateTime; var Error: AnsiString): boolean;
begin
    Result := False;
    try
        outFecha := QueryGetValueDateTime(Conn, Format('SELECT dbo.YYYYMMDDHHMMSS_TO_DATETIME(''%s'','':'')',[TRIM(strFecha)]));
        Result := True;
    except
        on e:exception do begin
            Error := e.Message;
        end;
    end;
end;
{-----------------------------------------------------------------------------
  Function Name: VerificarArchivoProcesado
  Author:    flamas
  Date Created: 23/12/2004
  Description: Verifica si el Archivo ha sido procesado en una interface
  Parameters: sFileName : string
  Return Value: boolean
-----------------------------------------------------------------------------
  Revision 1
  lgisuk
  26/09/2006
  Ahora se le env�a la fecha actual por par�metro para obtener el a�o actual.
-----------------------------------------------------------------------------}
function VerificarArchivoProcesado ( Base:TADOConnection; sFileName : string ) : boolean;
resourcestring
    MSG_FILE_ALREADY_PROCESSED = 'El archivo %s ya fue procesado';
begin
    Result := ( QueryGetValue(Base, 'SELECT dbo.VerificarArchivoInterfaseProcesado(''' + ExtractFileName( sFileName ) + ''',getdate())') = 'True' );
end;

{-----------------------------------------------------------------------------
  Function Name: RegistrarComprobanteEnviadoEnLog
  Author:    mtraversa
  Date Created: 28/12/2004
  Description: Crea un nuevo registro en la tabla DetalleComprobantesEnviados
  Parsmeters: Conn: TAdoConnection; TipoComprobante: ShortString; NumeroComprobante: LongInt; CodigoOperacionInterfase: Integer
  Return Value:None
-----------------------------------------------------------------------------}
function RegistrarComprobanteEnviadoEnLog(Conn: TAdoConnection; TipoComprobante: ShortString; NumeroComprobante: LongInt; CodigoOperacionInterfase: Integer; var DescError: AnsiString): Boolean;
begin
    Result := False;
    try
        with TADOStoredProc.Create(Nil) do
            try
                Connection := Conn;
                ProcedureName := 'AgregarDetalleComprobantesEnviados';
                with Parameters do begin
                    Refresh;
                    ParamByName('@TipoComprobante').Value          := TipoComprobante;
                    ParamByName('@NumeroComprobante').Value        := NumeroComprobante;
                    ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacionInterfase;
                end;
                ExecProc;
                Result := True;
        finally
            Free;
        end;
    except
        on E: Exception do begin
            DescError := E.Message;
        end;
    end;
end;


{-----------------------------------------------------------------------------
  Procedure: MsgProcesoFinalizado
  Author:    ndonadio
  Date:      24-Abr-2005
  Arguments: sMensaje (AnsiString):   Mensjae Original.
             sTitulo: (AnsiString):  Caption para el dialogo.
             RProcesados, RExito, RError (integer) : Cantidad de Registros
                                        procesados total, exitosos y erroneos.
  Result:    boolean
  Decription:
-----------------------------------------------------------------------------}
function MsgProcesoFinalizado(sMensajeOK, sMensajeError, sTitulo: AnsiString; RProcesados, RExito, RError: integer):boolean;
resourcestring
    MSG_PROC_INFO                       =   CrLf+'Cantidad de Registros Procesados: %d '+
                                            CrLf+'Cantidad de Registros Procesados con �xito: %d ' +
                                            CrLf+'Cantidad de Registros Procesados con Error: %d ';
    MSG_ASK_SHOW_ERROR_REPORT           =   CrLf + 'Desea ver el Reporte de Errores?.';
var
    Mensaje     : AnsiString;
    Respuesta   : Boolean;
begin
    Mensaje :=  Format(MSG_PROC_INFO,[RProcesados, RExito, RError]);

    if RError > 0 then begin
        Mensaje := sMensajeError + Mensaje +  MSG_ASK_SHOW_ERROR_REPORT;
        Respuesta := (MsgBox(Mensaje, sTitulo, MB_ICONWARNING + MB_YESNO) = mrYes);
    end
    else begin
        Mensaje := sMensajeOK + Mensaje;
        Respuesta := False;
        MsgBox(Mensaje, sTitulo, MB_ICONINFORMATION);
    end;
    Result :=  Respuesta;
end;

{-----------------------------------------------------------------------------
  Function Name: MoverArchivoProcesado
  Author:    ndonadio
  Date Created: 02/05/2005
  Description: Mueve el archivo de origen al de destino.
                Se utiliza principalmente para mover los archivos procesados
                exitosamente a un directorio de archivos procesados.

  Parameters: PathNameOrigen, PathNameDestino: AnsiString
  Return Value: boolean
-----------------------------------------------------------------------------}
function MoverArchivoProcesado(MsgTitulo, PathNameOrigen, PathNameDestino: AnsiString; NoInfo: Boolean = False): boolean;
resourcestring
    MSG_ERROR_MOVING_FILE   =   'El archivo %s '+CrLf+
                                'no se ha podido mover a: %s'+CrLf +
                                'Deber� completar la operaci�n manualmente.';
    MSG_MOVING_COMPLETE     =   'El archivo %s '+CrLf+
                                'ha sido movido a: %s';
begin
 try
    if TRIM(PathNameOrigen) = TRIM(PathNameDestino) then
    begin
        Result := True;
        Exit;
    end;
    if MoveFile(PChar(PathNameOrigen), PChar(PathNameDestino)) then begin
        Result := True;
        if NOT NoInfo then MsgBox(Format(MSG_MOVING_COMPLETE,[PathNameOrigen, PathNameDestino]),'INFORMACI�N: '+MsgTitulo,MB_ICONINFORMATION);
    end
    else begin
        Result := False;
        MsgBox(Format(MSG_ERROR_MOVING_FILE, [PathNameOrigen, PathNameDestino]),'ADVERTENCIA: '+MsgTitulo,MB_ICONWARNING);
    end;
 except
    Result := False;
    MsgBox(Format(MSG_ERROR_MOVING_FILE,[PathNameOrigen, PathNameDestino]),'ADVERTENCIA: '+MsgTitulo,MB_ICONWARNING);
 end;
end;

{-----------------------------------------------------------------------------
  Function Name: TamanioArchivo
  Author:    ndonadio
  Date Created: 07/06/2005
  Description: Devuelve el tama�o del archivo especificado, en bytes...
  Parameters: FullPathFileName: AnsiString
  Return Value: bigint
-----------------------------------------------------------------------------}
function  TamanioArchivo(FullPathFileName: AnsiString):integer;
begin
    Result := Util.GetFileSize(FullPathFileName);
end;

{-----------------------------------------------------------------------------
  Function Name: ValidarHeaderFooterPRESTO
  Author:    flamas
  Date Created: 27/05/2005
  Description: Valida un Header de PRESTO
  Parameters: var sArchivo : TStringList; sNombreArchivo, sEPS, sGlosa : string
  Return Value: boolean
-----------------------------------------------------------------------------}
function  ValidarHeaderFooterPRESTO(var sArchivo : TStringList; sNombreArchivo, sEPS, sGlosa : string) : boolean;
resourcestring
	MSG_INVALID_HEADER = 'El encabezado del archivo es inv�lido';
	MSG_INVALID_DESCRIPTION = 'La glosa es incorrecta';
	MSG_INVALID_DATE = 'La fecha de glosa es incorrecta';
	MSG_INVALID_COMPANY = 'El c�digo de empresa prestadora de servicios es inv�lido';
	MSG_INVALID_FOOTER = 'El pie de p�gina del archivo es inv�lido';
	MSG_INVALID_REGISTER_COUNT 	= 'La cantidad de registros del archivo'#10#13 + 'no coincide con la cantidad reportada';
	MSG_ERROR = 'Error';
var
  	nLineas : integer;
    nRegs	: integer;
begin
  	nLineas := sArchivo.Count - 1;

    result := False;

    // Valida el Header
  	if ( Pos( 'HEADER', sArchivo[0] ) <> 1 ) then begin
    	MsgBox( MSG_INVALID_HEADER, MSG_ERROR, MB_ICONERROR );
    	Exit;
    end;

    // Valida la EPS
    if ( Pos( sEPS , sArchivo[0] ) <> 8 ) then begin
    	MsgBox( MSG_INVALID_COMPANY, MSG_ERROR, MB_ICONERROR );
    	Exit;
    end;

    // Valida la Glosa
	  if  (Pos( UpperCase(Trim( sGlosa )), UpperCase(sArchivo[0])) <> 15)  then begin
    	MsgBox( MSG_INVALID_DESCRIPTION, MSG_ERROR, MB_ICONERROR );
     	Exit;
    end;

    // Valida el Footer
    if ( Pos( 'FOOTER', sArchivo[nLineas] ) <> 1 ) then begin
    	MsgBox( MSG_INVALID_FOOTER, MSG_ERROR, MB_ICONERROR );
    	Exit;
    end;

  	try
      	nRegs := StrToInt( trim( Copy( sArchivo[nLineas], 8, 6 )));
    except
      	on exception do begin
    	    	MsgBox( MSG_INVALID_FOOTER, MSG_ERROR, MB_ICONERROR );
    		    Exit;
		    end;
    end;

    if ( nRegs <> nLineas - 1 ) then begin
    	  MsgBox( MSG_INVALID_REGISTER_COUNT, MSG_ERROR, MB_ICONERROR );
    	  Exit;
    end;

    Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: NombreArchivoXML
  Author:    ndonadio
  Date Created: 05/07/2005
  Description: Obtiene el nombre de un archivo XML de infracciones
  Parameters: PathDestino: AnsiString; NumCorrDisco, NumCorrArchivo: Integer; Fecha: TDateTime
  Return Value: String
-----------------------------------------------------------------------------}
function NombreArchivoXML(PathDestino: AnsiString; NumCorrDisco, NumCorrArchivo: Integer; Fecha: TDateTime; pConcesionaria: Integer): String;
const
    FILE_PREFIX             = 'ingreso_';
    FORMAT_FECHA            = 'yyyyymmdd';
    FILE_EXT                = '.XML';
    FORMAT_CORRDISCO        = '0000';
    FORMAT_CORRARCHIVO      = '0000';
var
   FileName : AnsiString;
begin
    FileName := FILE_PREFIX +  FormatFloat('00',pConcesionaria)  + FormatFloat(FORMAT_CORRDISCO,NumCorrDisco)+ FormatFloat(FORMAT_CORRARCHIVO,NumCorrArchivo)+'_'+FormatDateTime(FORMAT_FECHA,Fecha);
    FileName := FileName + FILE_EXT;
    Result := GoodDir(PathDestino)+FileName;
end;

{-----------------------------------------------------------------------------
  Function Name: NombreCarpetaXML
  Author:    ndonadio
  Date Created: 05/07/2005
  Description: Obtiene el nombre de una carpeta destino de los XML de infracciones
                a partir del Num de disco, etc.
  Parameters: PathDestino: AnsiString; NumCorrDisco: Integer; IndiceReproceso: Byte
  Return Value: AnsiString
-----------------------------------------------------------------------------}
function NombreCarpetaXML(PathDestino: AnsiString; NumCorrDisco: Integer; IndiceReproceso: Byte): AnsiString;
begin
    if IndiceReproceso > 0 then begin
        Result := GoodDir(PathDestino)+FormatFloat(XML_DISK_ID+'0000',NumCorrDisco)+XML_SEP_REPROCESO+FormatFloat('000',IndiceReproceso)
    end else begin
        Result := GoodDir(PathDestino)+FormatFloat(XML_DISK_ID+'0000',NumCorrDisco);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: CrearCarpetaXML
  Author:    ndonadio
  Date Created: 30/06/2005
  Description: Crea una carpeta en el destino indicado.
               Si debe crear carpetas intermedias, pregunta.
               Al finalizar, devuelve verdadero si la carpeta existe. Si ya existia
               devuelve un mensaje en el var Error.
               Si no se pudo crear (y no existe) devuelve False
  Parameters: PathDestino: AnsiString; NumCorrDisco: Integer; IndiceReproceso: Byte; var Carpeta: AnsiString; var Error: AnsiString
  Return Value: Boolean
-----------------------------------------------------------------------------}
function CrearCarpetaXML(PathDestino: AnsiString; NumCorrDisco: Integer; IndiceReproceso: Byte; var Carpeta: AnsiString; var Error: AnsiString): Boolean;
resourcestring
    MSG_CANCEL = 'El Proceso ha sido Cancelado';
    MSG_DIRECTORY_ALREADY_EXISTS = 'El directorio "%s" ya existe.' ;
    MSG_CREATE_ALL_DIRECTORIES   = 'No existen todas las carpetas especificadas en el path. '+CRLF + ' %s ' +CRLF + 'Desea crearlas?';
    MSG_ERROR = 'Ha ocurrido un error al generar la carpeta %s. '+ CRLf + 'El Proceso ha sido cancelado';
begin
    Carpeta := NombreCarpetaXML(PathDestino, NumCorrDisco, IndiceReproceso);
    Result := false;
    // Si el directorio existe retornamos ese directorio
    if DirectoryExists(Carpeta) then  begin
        Error := XML_FOLDER_EXISTS;
        Result      := true;
        Exit;
    end;
    // Si el directorio no existe verificamos si el Path Raiz existe
    if (not DirectoryExists(PathDestino)) and
      (MsgBox(Format(MSG_CREATE_ALL_DIRECTORIES, [Carpeta]), 'XML Infractores', MB_ICONQUESTION+MB_YESNO)<>mrYES) then begin
        Error := MSG_CANCEL;
        exit;
    end;
    // Creamos la carpeta destino
    ForceDirectories(Carpeta);
    Result := true;
end;

{-----------------------------------------------------------------------------
  Function Name: VerificarTamanioUnidad
  Author:    ndonadio
  Date Created: 05/07/2005
  Description: Verifica el tama�o de una carpeta como al suma de los tama�os de
                los archivos contenidos en ella...
  Parameters: Path: AnsiString
  Return Value: Int64
-----------------------------------------------------------------------------}
function VerificarTamanioUnidad(Path: AnsiString): Int64;
var
    StrPath : AnsiString;
    sr: TSearchRec;
    TotalSize: Int64;
begin
    TotalSize:= 0;
    StrPath := GoodDir(Path)+'*.*';
    if FindFirst(StrPath, faAnyFile, sr) = 0 then begin
        repeat
            TotalSize := TotalSize+sr.Size;
        until FindNext(sr) <> 0;
        FindClose(sr);
    end;
    Result := TotalSize;
end;

{******************************** Function Header ******************************
Function Name: InfraccionToXML
Author       : fmalisia
Date Created : 01-Jul-2005
Description  : Agrega un registro de infracci�n en el XML, si tiene exito retorna 0, si no encuetra la imagen retorna 1
Parameters   : SourceSP: TADOStoredProc; ImagePath: AnsiString; var XMLContainer: IXMLDenunciosType; var CantInfracciones: byte; var DescriError: AnsiString
Return Value : integer

 Revision	:	3
 Author		: 	ebaeza
 Date		:	14/06/2011
 firma      :   SS-377-EBA-20110613
 Descripcion:	Cambio estructura directorios de acceso a imagenes de transitos
                Se agrega parametro FNombreCortoConcesionaria con el nombre corto de la concesionaria
*******************************************************************************}
//function InfraccionToXML(SourceSP: TADOStoredProc; ImagePath: AnsiString; var XMLContainer: IXMLDenunciosType; var CantInfracciones: Integer; var DescriError: AnsiString): Boolean;
function InfraccionToXML(SourceSP: TADOStoredProc; ImagePath: AnsiString; FNombreCortoConcesionaria: String; var XMLContainer: IXMLDenunciosType; var CantInfracciones: Integer; var DescriError: AnsiString): Boolean;
resourcestring
    STR_INFRACTION_WITHOUT_IMAGE    = 'Infraccion N� %d: No se puede obtener la imagen. La Infraccion no se procesa.';
var
    ImagenTransito: TJPEGPlusImage;
    FechaHoraTrx: TDateTime;
  	Error: TTipoErrorImg;
    DataImage: TDataImage;
    NumCorrCA : Int64;
    TipoImagen: TTipoImagen;
    strStream: TStringStream;
    strImagenCodificada: AnsiString;
begin
    DescriError := '';
    Result := false;

    // Creamos el JPEG y la consulta
    if Assigned(ImagenTransito) then FreeAndNil(ImagenTransito);
    ImagenTransito := TJPEGPlusImage.Create;
    strStream := TStringStream.Create('');

    try
        try
            // Separo los parametros para obtener la imagen...
            NumCorrCA := SourceSP.FieldByName('RefNumCorrCA').asInteger; //291132;//
            FechaHoraTrx    := SourceSP.FieldByName('RefFechaHoraTrx').asDateTime; //StrtoDate('15/04/2005');//

            // Limpio las variables de imagen...
            if (SourceSP.FieldByName('RefRegistrationAccessibility').asInteger = 1) then TipoImagen := tiFrontal
            else TipoImagen := tiFrontal2;

            //se agrega el nombre corto de la concesionaria // SS-377-EBA-20110613
//            if ObtenerImagenTransito(ImagePath, NumCorrCA, FechaHoraTrx, TipoImagen, ImagenTransito, DataImage, DescriError, Error) then begin                                  // SS-377-EBA-20110613
            if ObtenerImagenTransito(ImagePath, FNombreCortoConcesionaria, NumCorrCA, FechaHoraTrx, TipoImagen, ImagenTransito, DataImage, DescriError, Error) then begin         // SS-377-EBA-20110613
                // Convierto la imagen a un cadena (char)
                ImagenTransito.SaveToStream(strStream);
                strImagenCodificada := ImageURLEncode(strStream.DataString);


                // Creo la infraccion
                XMLContainer.add;
                // Cargo los datos
                XMLContainer.infraccion[CantInfracciones].Concesionaria    := SourceSP.FieldByName('Concesionaria').asInteger;
                XMLContainer[CantInfracciones].NumInfraccion    := SourceSP.FieldByName('NumInfraccion').asInteger;
                XMLContainer[CantInfracciones].Infractor.Nombre := trim(SourceSP.FieldByName('Nombre').asString);
                XMLContainer[CantInfracciones].Infractor.ApellidoPaterno  := trim(SourceSP.FieldByName('ApellidoPaterno').asString);
                XMLContainer[CantInfracciones].Infractor.ApellidoMaterno  := trim(SourceSP.FieldByName('ApellidoMaterno').asString);
                XMLContainer[CantInfracciones].Infractor.Rut    := trim(SourceSP.FieldByName('Rut').asString);
                XMLContainer[CantInfracciones].Infractor.DV     := trim(SourceSP.FieldByName('Rut_DV').asString);
                XMLContainer[CantInfracciones].Infractor.Domicilio  := trim(SourceSP.FieldByName('Domicilio').asString);
                XMLContainer[CantInfracciones].Infractor.Comuna := trim(SourceSP.FieldByName('Comuna').asString);
                XMLContainer[CantInfracciones].Vehiculo.Patente := trim(SourceSP.FieldByName('Patente').asString);
                XMLContainer[CantInfracciones].Vehiculo.DV      := trim(SourceSP.FieldByName('Patente_DV').asString);
                XMLContainer[CantInfracciones].Vehiculo.TipoVehiculo  := trim(SourceSP.FieldByName('TipoVehiculo').asString);
                XMLContainer[CantInfracciones].Vehiculo.Marca   := trim(SourceSP.FieldByName('Marca').asString);
                XMLContainer[CantInfracciones].Vehiculo.Modelo  := trim(SourceSP.FieldByName('Modelo').asString);
                XMLContainer[CantInfracciones].Vehiculo.Agno    := SourceSP.FieldByName('Agno').asInteger;
                XMLContainer[CantInfracciones].Vehiculo.Color   := trim(SourceSP.FieldByName('Color').asString);
                XMLContainer[CantInfracciones].Fotografia.Imagen := strImagenCodificada;
                Result := true;
            end else begin
                // Fall� la obtencion de la image
                DescriError := Format(STR_INFRACTION_WITHOUT_IMAGE, [SourceSP.FieldByName('NumInfraccion').asInteger]);
            end;
        except
            on e: Exception do begin
                DescriError := e.Message;
            end;
        end;
    finally
        if Assigned(ImagenTransito) then FreeAndNil(ImagenTransito);
        if Assigned(strStream) then strStream.Free;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: TransaccionesToXML
  Author:    ndonadio
  Date Created: 05/07/2005
  Description: Guarda los transitos de una infraccion en el Container XML
                que se recibe como paraemtro
  Parameters: SourceSP: TADOStoredProc; var XMLContainer: IXMLDenunciosType;
                var CantInfracciones: Integer
  Return Value: boolean
-----------------------------------------------------------------------------}
function TransaccionesToXML(SourceSP: TADOStoredProc; var XMLContainer: IXMLDenunciosType; var CantInfracciones: Integer; var DescriError: AnsiString): boolean;
var
    iTrx: Integer;
begin
    iTrx := 0;
    Result := False;

    try
        While not SourceSP.Eof do begin
            XMLContainer[CantInfracciones].Transaccion.Add;
            XMLContainer[CantInfracciones].Transaccion[iTrx].Ident_PC       := trim(SourceSP.FieldByName('Ident_PC').asString);
            XMLContainer[CantInfracciones].Transaccion[iTrx].Num_Corr_Punto := SourceSP.FieldByName('Num_Corr_Punto').asInteger;
            XMLContainer[CantInfracciones].Transaccion[iTrx].Time_PC        := SourceSP.FieldByName('Time_PC').asInteger;
            XMLContainer[CantInfracciones].Transaccion[iTrx].Fecha_transaccion := trim(SourceSP.FieldByName('FechaTransaccion').asString);
            XMLContainer[CantInfracciones].Transaccion[iTrx].Hora_local     := trim(SourceSP.FieldByName('HoraLocal').asString);
            XMLContainer[CantInfracciones].Transaccion[iTrx].Num_Corr_CO    := SourceSP.FieldByName('NumCorrCO').asInteger;
            XMLContainer[CantInfracciones].Transaccion[iTrx].Num_Corr_CA    := SourceSP.FieldByName('NumCorrCA').asInteger;
            XMLContainer[CantInfracciones].Transaccion[iTrx].ConsolidacionManual := trim(SourceSP.FieldByName('ConsolidacionManual').asString);
            inc(iTrx);
            SourceSP.Next;
        end;
        Result := True;
    except
        on e: exception do begin
            DescriError := e.Message;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: ImageURLEncode
Author : ndonadio
Date Created : 11/08/2005
Description : convierte una imagen a formato utf8
Parameters : none
Return Value : Not available
*******************************************************************************}
function ImageURLEncode(const Image: AnsiString): utf8String;

    function HTTPEncodeX(const AStr: String): String;
    const
        NoConversion = ['A'..'Z','a'..'z','0'..'9','-', '.', '*', '_'];
    var
        Len: Integer;
        Sp, Rp: PChar;
    begin
        SetLength(Result, Length(AStr) * 3);
        Sp := PChar(AStr);
        Rp := PChar(Result);
        Len := Length(AStr);
        while Len > 0 do begin
            if Sp^ in NoConversion then begin
                Rp^ := Sp^
                end else if Sp^ = ' ' then begin
                    Rp^ := '+';
                end else begin
                    FormatBuf(Rp^, 3, '%%%.2x', 6, [Ord(Sp^)]);
                    Inc(Rp, 2);
                end;
                Inc(Rp);
                Inc(Sp);
                Dec(Len);
        end;
        SetLength(Result, Rp - PChar(Result));
    end;

var
    FRawImage: WideString;
    i: Integer;
begin
    SetLength(FRawImage, Length(Image));
    for i := 1 to Length(Image) do begin
        if Image[i] > #127 then begin
            FRawImage[i] := WideChar($FF00 + Ord(Image[i]));
        end else begin
            FRawImage[i] := WideChar(Ord(Image[i]));
        end;
    end;
    Result := HttpEncodeX(utf8Encode(FRawImage));
end;

{******************************** Function Header ******************************
Function Name: ActualizarLogOperacionesInterfaseAlFinal
Author : ndonadio
Date Created : 11/08/2005
Description : Actualiza el log de operaciones al finalizar de generar
              el archivo de xml de infracciones
Parameters : none
Return Value : Not available
*******************************************************************************}
Function  ActualizarLogOperacionesInterfaseAlFinal(Conn: TAdoConnection; CodigoOperacionInterfase: integer; Observaciones:string; CorrDisco, CorrArchivo, IndiceReprocesamiento: Integer; FileName: AnsiString; Var DescError:Ansistring):boolean; overload;
resourcestring
    MSG_ERROR_UPDATING_DISK_REGISTRATION   =   'No se pudo actualizar la informaci�n del Disco en el Log de Operaciones';
var
    qr: TADOQuery;
begin
    // Actualizamos el log de operaciones
    result := False;

    if not ActualizarLogOperacionesInterfaseAlFinal(Conn, CodigoOperacionInterfase, Observaciones, DescError) then exit;
    // Agrego en la tabla de extension... DetalleDiscoInfracciones
    qr := TAdoQuery.Create(nil);
    try
        qr.SQL.Text := '';
        qr.Connection := Conn;
        try
            qr.SQL.Text :=  Format( 'UPDATE DetalleDiscoInfracciones ' +
                                    '   SET CorrelativoDisco = %d, ' +
                                    '       CorrelativoArchivo = %d, ' +
                                    '       IndiceReproceso = %d ' +
                                    ' WHERE CodigoOperacionInterfase =  %d',
                                    [CorrDisco, CorrArchivo, IndiceReprocesamiento, CodigoOperacionInterfase]);
            qr.Prepared := True;
            if qr.ExecSQL <> 1 then begin
                DescError := MSG_ERROR_UPDATING_DISK_REGISTRATION;
                exit;
            end;
            qr.SQL.Text := '';
            qr.SQL.Text :=  Format( 'UPDATE LogOperacionesInterfases SET NombreArchivo = ''%s'' ' +
                                    ' WHERE CodigoOperacionInterfase =  %d',
                                    [trim(FileName), CodigoOperacionInterfase]);
            qr.Prepared := True;
            if qr.ExecSQL <> 1 then begin
                DescError := MSG_ERROR_UPDATING_DISK_REGISTRATION;
                exit;
            end;
            result := True;
        except
            on e: Exception do begin
                DescError := e.message;
            end;
        end;
    finally
        qr.Free;
    end;
end;

{******************************** Function Header ******************************
Function Name: ObtenerDatosProcesoContabilizacion
Author : ggomez
Date Created : 11/08/2005
Description : Retorna en los par�metros de salida los datos de un Proceso de
    Contabilizaci�n.
Parameters : none
Return Value : Not available
*******************************************************************************}
procedure ObtenerDatosProcesoContabilizacion(Conn: TADOConnection; NumeroProceso: Integer; var FechaJornada: TDateTime; var FechaHoraInicio: TDateTime; var FechaHoraCierre: TDateTime; var Usuario: AnsiString; var Estado: TEstadoContabilizacion);
var
    sp: TADOStoredProc;
begin
    sp := TADOStoredProc.Create(Nil);
    try
        sp.Connection       := Conn;
        sp.ProcedureName    := 'ObtenerDatosProcesoContabilizacion';
        sp.Parameters.Refresh;
        sp.Parameters.ParamByName('@NumeroProcesoContabilizacion').Value    := NumeroProceso;
        sp.Parameters.ParamByName('@FechaJornada').Value                    := Null;
        sp.Parameters.ParamByName('@FechaHoraInicio').Value                 := Null;
        sp.Parameters.ParamByName('@FechaHoraCierre').Value                 := Null;
        sp.Parameters.ParamByName('@Usuario').Value                         := Null;
        sp.Parameters.ParamByName('@Estado').Value                          := Null;
        sp.ExecProc;

        FechaJornada    := sp.Parameters.ParamByName('@FechaJornada').Value;
        FechaHoraInicio := sp.Parameters.ParamByName('@FechaHoraInicio').Value;
        FechaHoraCierre := iif(sp.Parameters.ParamByName('@FechaHoraCierre').Value = Null, NullDate, sp.Parameters.ParamByName('@FechaHoraCierre').Value);
        Usuario         := sp.Parameters.ParamByName('@Usuario').Value;
        case sp.Parameters.ParamByName('@Estado').Value of
            CONST_ESTADO_CONTABILIZACION_INICIADO       : Estado := ecIniciada;
            CONST_ESTADO_CONTABILIZACION_FINALIZADO     : Estado := ecFinalizada;
            CONST_ESTADO_CONTABILIZACION_DESHACIENDO    : Estado := ecDeshaciendo;
            CONST_ESTADO_CONTABILIZACION_DESHECHO       : Estado := ecDeshecha;
            else Estado := ecDesconocida;
        end;
    finally
        sp.Free;
    end;
end;
{-----------------------------------------------------------------------------
  Procedure: FechaValidaEnNombreArchivo
  Author:    vpaszkowicz
  Date:      17-Ene-2007
  Arguments: unaFechaStr: string
  Result:    Boolean
  Description: Valida si la fecha en el string con formato aaaammdd es v�lida
            se usa para los nombres de archivos en interfaces.
-----------------------------------------------------------------------------}
function FechaValidaEnNombreArchivo(unaFechaStr: string): Boolean;
var
    i: integer;
    dd, mm, aaaa: word;
begin
    Result := False;
    if Length(unaFechaStr) <> 8 then Exit;
    for i := 1 to length(unaFechaStr) do if not (unaFechaStr[i] in ['0'..'9', ' ']) then exit;
    try
        dd      := IVal(Copy(unaFechaStr, 7, 2));
        if dd > 31 then Exit;
        mm      := Ival(Copy(unaFechaStr, 5, 2));
        if mm > 12 then Exit;
        aaaa    := Ival(Copy(unaFechaStr, 1, 4));
        if aaaa < 1950 then Exit;
        Result := True;
    except
        Result := False;
    end;
end;

{******************************** Function Header ******************************
Function Name: GenerarReporteErroresSintaxis
Author : vpaszkowicz
Date Created : 19/11/2007
Description : Muevo esta funci�n para poder generar un reporte de errores de sin-
taxis del archivo de rendiciones.
Parameters : TituloReporte: string
Return Value : Boolean
*******************************************************************************}
Function GenerarReporteErroresSintaxis(TituloReporte: string; ListaErrores: TStringList; DirectorioErrores: AnsiString; CodigoModulo: integer): Boolean;
resourcestring
    MSG_REPORT_ERROR = 'No se pudo generar el reporte de errores de Sintaxis';
    MSG_ERROR        = 'Error';
var
    F : TFRptErroresSintaxis;
begin
    Result := False;
    try
        try
            //muestro el reporte
            Application.CreateForm(TFRptErroresSintaxis, F);
            if not F.Inicializar(IntToStr(CodigoModulo) , TituloReporte , DirectorioErrores ,ListaErrores) then f.Release;
            Result := True;
        except
           on E : Exception do begin
                MsgBoxErr(MSG_REPORT_ERROR, E.Message, MSG_ERROR, MB_ICONERROR);
                Exit;
            end;
        end;
    finally
        if Assigned(f) then f.Release;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ActualizarLogOperacionesInterfaseAlFinal
  Author:    mbecerra
  Date: 17-Julio-2008
  Description:
                Actualiza la tabla LogOperacionesInterfases con la
                Observaci�n, LineasArchivo y MontoArchivo.

  Revisi�n   : 1
  Autor      : mpiazza
  Fecha      : 17/04/2009
  Descripci�n: ss 750 se agrego esta funcion sobrecargada
              ActualizarLogOperacionesInterfaseAlFinal con el montoarchivo como
              nuevo campo

-----------------------------------------------------------------------------}
function ActualizarLogOperacionesInterfaseAlFinal(Conn: TAdoConnection; CodigoOperacionInterfase, LineasArchivo : integer; MontoArchivo : Int64; Observaciones : string; Var DescError: string): boolean; overload;
var sp : TADOStoredProc;
begin
    Result := False;
    sp := TADOStoredProc.Create(Nil);
    DescError := '';
    try
        try
            sp.Connection := Conn;
            sp.ProcedureName := 'ActualizarLogOperacionesInterfaseAlFinal';
            with SP.Parameters do begin
                Refresh;
                ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacionInterfase;
                ParamByName('@Observaciones').Value := Observaciones;
                ParamByName('@LineasArchivo').Value := LineasArchivo;
                ParamByName('@MontoArchivo').Value  := MontoArchivo;
            end;
            SP.ExecProc;
            Result := True;
		    except
            on E : Exception do begin
                DescError := E.Message;
            end;
        end;
    finally
        SP.Free;
  	end;
end;

end.
