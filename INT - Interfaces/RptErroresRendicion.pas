{-------------------------------------------------------------------------------


Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.


-------------------------------------------------------------------------------}
unit RptErroresRendicion;


interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ppBands, ppCache, ppClass, ppComm, ppRelatv, ppProd, ppReport,
  UtilRB, ppDB, ppTxPipe, ppCtrls, ppPrnabl, ppStrtch, ppSubRpt, ppModule,
  raCodMod, ppMemo,RBSetup, ppParameter, ppVar, StdCtrls, daDataModule;

type
  TfrmReporteErroresRendicion = class(TForm)
    ppReport1: TppReport;
    ppErrores: TppTextPipeline;
    rbiListado: TRBInterface;
    ppParameterList1: TppParameterList;
    Label1: TLabel;
    ppHeaderBand1: TppHeaderBand;
    ppLine2: TppLine;
    lbl_usuario: TppLabel;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppImage2: TppImage;
    ppDetailBand1: TppDetailBand;
    ppDBText1: TppDBText;
    ppErroresppField1: TppField;
    procedure rbiListadoExecute(Sender: TObject; var Cancelled: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    function Inicializar( sArchivo1 : ANSIString ): Boolean;
    function Ejecutar:Boolean;
  end;

var
  frmReporteErroresRendicion: TfrmReporteErroresRendicion;

implementation

uses
        util, utilProc, RStrings, ConstParametrosGenerales;                  //SS_1147_NDR_20140710
resourcestring
    TITULO_REPORTE_VENTANA_DE_DIALOGO = 'Rendiciones Débitos PAC : Errores';

{$R *.dfm}

function TfrmReporteErroresRendicion.Inicializar( sArchivo1 : ANSIString ): Boolean;
var
	Config: TRBConfig;
  RutaLogo: AnsiString;                                                         //SS_1147_NDR_20140710
begin
  ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
  try                                                                                                 //SS_1147_NDR_20140710
    ppImage2.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
  except                                                                                              //SS_1147_NDR_20140710
    On E: Exception do begin                                                                          //SS_1147_NDR_20140710
      Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
      MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
      Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
    end;                                                                                              //SS_1147_NDR_20140710
  end;                                                                                                //SS_1147_NDR_20140710

	ppErrores.FileName := sArchivo1;
  ppErrores.Open;

  rbiListado.Caption := TITULO_REPORTE_VENTANA_DE_DIALOGO;

	// Carga la Configuración de Usuario

	Config := rbiListado.GetConfig;
    if Config.ShowUser then begin
    	if UsuarioSistema <> '' then lbl_usuario.caption := 'Usuario: ' +
        	UsuarioSistema + ' ' else lbl_usuario.Caption := '';
    end else begin
    	lbl_usuario.Caption := '';
    end;

    if Config.ShowDateTime then begin
    	lbl_usuario.caption := lbl_usuario.caption +
        FormatShortDate(Date) + ' ' + FormatTime(Time);
    end;
    result := True;
end;

function TfrmReporteErroresRendicion.Ejecutar:Boolean;
begin
	 rbiListado.Execute(True);
	 result := true;
end;

procedure TfrmReporteErroresRendicion.rbiListadoExecute(Sender: TObject;
  var Cancelled: Boolean);
begin
	try
		try
			Screen.Cursor := crHourGlass;
		finally
			Screen.Cursor := crDefault;
		end;
	except
		on E: Exception do begin
			Cancelled := True;
			MsgBoxErr(MSG_ERROR_DATOS_LISTADO, E.Message, Self.Caption, MB_ICONSTOP);
		end;
	end;
end;


end.
