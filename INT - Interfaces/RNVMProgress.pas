unit RNVMProgress;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls, Util;

type
  TfrmRNVMProgress = class(TForm)
    btnCancelar: TButton;
    pbConsultas: TProgressBar;
    Bevel1: TBevel;
    lblInfo: TLabel;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    FCancelar : Boolean;
    FCantidadConsultas, FConsultaActual: Integer;
    procedure SetCantidadConsultas(const Value: Integer);
    procedure SetConsultaActual(const Value: Integer);
    function GetMustCancel: Boolean;
    Procedure Actualizar(Posicion: Integer);
    { Private declarations }
  public
    function Inicializar: Boolean;
  published
    property CantidadConsultas:Integer  write SetCantidadConsultas;
    property ConsultaActual: Integer write SetConsultaActual;
    property Cancelar: Boolean read GetMustCancel;
  end;
var
  frmRNVMProgress: TfrmRNVMProgress;

implementation

{$R *.dfm}

procedure TfrmRNVMProgress.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action := caNone;
end;

function TfrmRNVMProgress.GetMustCancel: Boolean;
begin
    Result := FCancelar;
end;

function TfrmRNVMProgress.Inicializar: Boolean;
resourcestring
    MSG_INIT_ERROR = 'Debe fijar el valor de la propiedad cantidad de consultas antes' + CRLF +
      'de invocar al metodo Inicializar';
begin
    if FCantidadConsultas = 0 then begin
        raise Exception.Create(MSG_INIT_ERROR);
        Exit;
    end;
    pbConsultas.Max := FCantidadConsultas;
    pbConsultas.Min := 1;
    pbConsultas.Smooth := True;
    pbConsultas.Step := 1;
    FCancelar := False;
    lblInfo.Caption := Format(lblInfo.Caption, [FCantidadConsultas]);
    Application.ProcessMessages;
    Result := True;
end;

procedure TfrmRNVMProgress.SetCantidadConsultas(const Value: Integer);
begin
    FCantidadConsultas := Value;
end;

procedure TfrmRNVMProgress.SetConsultaActual(const Value: Integer);
begin
    FConsultaActual := Value;
    Actualizar(FConsultaActual);
end;

procedure TfrmRNVMProgress.btnCancelarClick(Sender: TObject);
begin
    FCancelar := True;
    Close;
end;

procedure TfrmRNVMProgress.Actualizar(Posicion: Integer);
begin
    pbConsultas.Position := Posicion;
    Application.ProcessMessages;
end;

procedure TfrmRNVMProgress.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
    CanClose := FCancelar;
end;

end.

