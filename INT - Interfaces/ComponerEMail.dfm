object frmComponerMail: TfrmComponerMail
  Left = 197
  Top = 182
  BorderStyle = bsDialog
  Caption = 'Composici'#243'n de e-Mail'
  ClientHeight = 446
  ClientWidth = 688
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  DesignSize = (
    688
    446)
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 8
    Top = 8
    Width = 673
    Height = 393
  end
  object Label1: TLabel
    Left = 21
    Top = 20
    Width = 31
    Height = 13
    Caption = 'Para:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 22
    Top = 39
    Width = 44
    Height = 13
    Caption = 'Asunto:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblPara: TLabel
    Left = 80
    Top = 21
    Width = 32
    Height = 13
    Caption = 'lblPara'
  end
  object lblAsunto: TLabel
    Left = 81
    Top = 38
    Width = 32
    Height = 13
    Caption = 'lblPara'
  end
  object btnCerrar: TButton
    Left = 600
    Top = 408
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Cerrar'
    Default = True
    TabOrder = 0
    OnClick = btnCerrarClick
  end
  object Memo: TMemo
    Left = 16
    Top = 72
    Width = 657
    Height = 322
    TabOrder = 1
  end
end
