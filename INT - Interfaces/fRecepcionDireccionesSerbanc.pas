
{********************************** Unit Header ********************************
File Name : fRecepcionDireccionesSerbanc.pas
Author : ndonadio
Date Created: 30/08/2005
Language : ES-AR
Description :   Procesa la recepcion de un archivo de nuvas direcciones de serbanc
                segun esp. v.6.



Firma       : SS_1147Q_NDR_20141202[??]
Descripcion : Si es VS el convenio es de largo 12 no 17

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)
 
Firma       : SS_1385_NDR_20150922
Descripcion : Cambiar la transaccionalidad de delphi por Execute('TRANSACCIONALIDAD SQL') para evitar tener rollback oculto

*******************************************************************************}
unit fRecepcionDireccionesSerbanc;

interface
uses
    // Utiles
    PeaProcs, PeaTypes, ComunesInterfaces, Util, UtilProc, StrUtils,
    // Parametros Generales
    ConstParametrosGenerales,
    // DB
    UtilDB, DMConnection,
    // Reporte Finalizacion
    fReporteNvsDirSerbanc,
    // Otros
    Windows, Messages, SysUtils, Variants, Classes,  Graphics,  Controls, Forms,
    Dialogs, StdCtrls, DmiCtrls, ComCtrls, ExtCtrls, DB, ADODB, ppDB,     ppDBPipe, ppPrnabl, ppClass,
    ppCtrls, ppBands,  ppCache,  ppComm,   ppRelatv, ppProd,    ppReport, UtilRB;


type
  TfrmRecpcionNuevasDireccionesSerbanc = class(TForm)
    Bevel1: TBevel;
    lblNombreArchivo: TLabel;
    lblReferencia: TLabel;
    pnlAyuda: TPanel;
    ImgAyuda: TImage;
    pnlAvance: TPanel;
    lblProcesoGeneral: TLabel;
    pbProgreso: TProgressBar;
    txtOrigen: TPickEdit;
    btnProcesar: TButton;
    btnCancelar: TButton;
    btnSalir: TButton;
    OpenDialog: TOpenDialog;
    spActualizarDomicilioSerbanc: TADOStoredProc;
    procedure txtOrigenChange(Sender: TObject);
    procedure txtOrigenButtonClick(Sender: TObject);
    procedure ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure ImgAyudaClick(Sender: TObject);
    procedure btnProcesarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    FCancelar,
    FProcesando:        boolean;
    FOrigen:            AnsiString;
    FFechaArchivo:      TDateTime;
    FCodigoOperacion:   Integer;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    {functions}
    function VerificarHeader(strFile: TStringList; var Error: AnsiString): boolean;
    function VerificarSintaxis(strFile: TStringList; var Error: AnsiString): boolean;
    function ProcesarLinea(Linea: AnsiString; var Error: AnsiString): Boolean;
    function ValidarLinea(Linea: AnsiString; var csvDireccion: AnsiString; var Error: AnsiString): Boolean;
    {procedures}
    procedure MostrarReporte;
    procedure HabilitarBotones;
    procedure ActualizarProgreso(Mensaje: AnsiString);
  public
    { Public declarations }
    function Inicializar(Titulo: AnsiString): boolean;
  end;

var
  frmRecpcionNuevasDireccionesSerbanc: TfrmRecpcionNuevasDireccionesSerbanc;

implementation

{$R *.dfm}

procedure TfrmRecpcionNuevasDireccionesSerbanc.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action := caFree;
end;

{******************************** Function Header ******************************
Function Name: FormCloseQuery
Author : ndonadio
Date Created : 30/08/2005
Description :   Indica que si se esta procesando NO se puede cerrar el form.
Parameters : Sender: TObject; var CanClose: Boolean
Return Value : None
*******************************************************************************}
procedure TfrmRecpcionNuevasDireccionesSerbanc.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
    CanClose := not FCancelar;
end;

{******************************** Function Header ******************************
Function Name: btnSalirClick
Author : ndonadio
Date Created : 30/08/2005
Description :  Sale del modulo.
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmRecpcionNuevasDireccionesSerbanc.btnSalirClick(Sender: TObject);
begin
    Close;
end;

{******************************** Function Header ******************************
Function Name: btnCancelarClick
Author : ndonadio
Date Created : 30/08/2005
Description :   Indica la cancelacion del proceso
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmRecpcionNuevasDireccionesSerbanc.btnCancelarClick(Sender: TObject);
begin
    FCancelar := True;
end;

{******************************** Function Header ******************************
Function Name: btnProcesarClick
Author : ndonadio
Date Created : 30/08/2005
Description :   Procesa el archivo de nuevas direcciones
                Modifica y/p agrega las nuevas direcciones cuyos datos sean
                validos.
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmRecpcionNuevasDireccionesSerbanc.btnProcesarClick(Sender: TObject);
resourcestring
    ERROR_HEADER    = 'Los datos del Header no concuerdan con el archivo. El proceso en es cancelado.';
    ERROR_SINTAXIS  = 'El archivo es inv�lido sint�cticamente. No se puede procesar.';
    ERROR_CANT_REGISTER_OP      = 'No se puede registrar la operaci�n';
    ERROR_CANT_END_OP           = 'El proceso ha concluido, pero no se puede registrar el cierre de la operacion.';
    PROCESS_CANCELED            = 'Proceso cancelado por el usuario';
    PROCESS_ENDS_OK             = 'El proceso finaliz� con �xito';
    // for Progress Bar
    CAP_OPENING_FILE        = 'Abriendo el archivo...';
    CAP_REGISTERING_OP      = 'Registrando la operaci�n';
    CAP_PROCESSING_LINE     = 'Procesando la l�nea %d de %d';
    CAP_ENDING_OP           = 'Registrando Fin de la Operacion';
const
    OBS_LOG         = 'Finalizado OK';
    OBS_LOG_FAIL    = 'Proceso cancelado o terminado anormalmente';

var
    strDirecciones:     TStringList;
    descError:          AnsiString;
    i, Cantidad:        Integer;
begin
    FProcesando := True;
    HabilitarBotones;
    // Abre el archivo
    strDirecciones := TStringList.Create;
    try
        ActualizarProgreso(CAP_OPENING_FILE);
        strDirecciones.LoadFromFile(txtOrigen.Text);
        pbProgreso.Max := (strDirecciones.Count - 1) * 2 + 3;
        // Verifica Header
        if not VerificarHeader(strDirecciones, descError) then begin
            MsgBoxErr(ERROR_HEADER, descError, caption, MB_ICONSTOP);
            Exit;
        end;
        // Analiza sintacticamente
        if not VerificarSintaxis(strDirecciones, descError) then begin
            MsgBoxErr(ERROR_SINTAXIS, descError, caption, MB_ICONSTOP);
            Exit;
        end;
        // Registra inicio de operacion en log de interfases
        ActualizarProgreso(CAP_REGISTERING_OP);
        if not RegistrarOperacionEnLogInterface(DMCOnnections.BaseCAC,RO_MOD_INTERFAZ_ENTRADA_DIRECCIONES_SERBANC, ExtractFileName(txtOrigen.Text),
          UsuarioSistema,'',True,False,FFechaArchivo, 0, FCodigoOperacion,descError) then begin
            MsgBoxErr(ERROR_CANT_REGISTER_OP, descError, Caption, MB_ICONERROR);
            Exit;
        end;
        // Procesa Archivo
        Cantidad := strDirecciones.Count - 1 ;
        i := 1;
        ActualizarProgreso(Format(CAP_PROCESSING_LINE,[i,Cantidad]));
        While (i <= Cantidad ) and (not FCancelar) do begin
            if not ProcesarLinea(strDirecciones.Strings[i], descError) then
                // Registro en el log de errores...
                if not AgregarErrorInterfase(DMConnections.BaseCAC,FCodigoOperacion, descError) then
                    Exit; // Fallo la registracion del error -> SALGO!
            inc(i);
            ActualizarProgreso(Format(CAP_PROCESSING_LINE,[i,Cantidad]));
        end;
        // Cierra Log
        ActualizarProgreso(CAP_ENDING_OP);
        if not ActualizarLogOperacionesInterfaseAlFinal(DMConnections.BaseCAC,FCodigoOPeracion,OBS_LOG,descError) then begin
              MsgBoxErr(ERROR_CANT_END_OP, descError, Caption, MB_ICONERROR);
              Exit;
        end;
        ActualizarProgreso('');
        pbProgreso.Position := pbProgreso.Max;
        MsgBox(PROCESS_ENDS_OK, Caption, MB_ICONINFORMATION);
        // Muestra Reporte
        MostrarReporte;
    finally
        if FCancelar then MsgBox(PROCESS_CANCELED, Caption, MB_ICONWARNING);
        FProcesando := False;
        strDirecciones.Free;
        HabilitarBotones;
        // Saco el nombre del archivo del txt de origen..
        txtOrigen.Text := ExtractFilePath(txtOrigen.Text);
    end;
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaClick
Author : ndonadio
Date Created : 30/08/2005
Description :  Muestra el globo de informacion del modulo
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmRecpcionNuevasDireccionesSerbanc.ImgAyudaClick(Sender: TObject);
Resourcestring
    MSG_ACCIONES_SERBANC  = ' ' + CRLF +
                          'El Archivo de Nuevas Direcciones es enviado' +
                          'por SERBANC  para informar al ESTABLECIMIENTO ' + CRLF +
                          'los cambios en las direcciones de los clientes' +
                          'detectados por Serbanc.' + CRLF +
                          ' ';
begin
    //si esta procesando sale
    if FProcesando then exit;
    //Muestro el mensaje
    MsgBoxBalloon(MSG_ACCIONES_SERBANC, Caption, MB_ICONQUESTION, IMGAYUDA);
end;
procedure TfrmRecpcionNuevasDireccionesSerbanc.ImgAyudaMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
    if FProcesando then ImgAyuda.Cursor := crDefault else ImgAyuda.Cursor := crHandPoint;
end;

{******************************** Function Header ******************************
Function Name: HabilitarBotones
Author : ndonadio
Date Created : 30/08/2005
Description :   Habilita / Dehabilita botones, barra de progreso y setea variables
                de acuerdo al estado (valor) de FProcesando.
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmRecpcionNuevasDireccionesSerbanc.HabilitarBotones;
begin
    btnProcesar.Enabled := not FProcesando; // boton Procesar
    btnCancelar.Enabled := FProcesando;     // boton Cancelar
    btnSalir.Enabled := not FProcesando;    // boton Salir
    txtOrigen.Enabled := not FProcesando;   // Selector de Archivo
    Cursor := iif( FProcesando, crHourglass, crDefault); // Cursor
    FCancelar := False;
    pnlAvance.Visible := FProcesando;
    lblReferencia.Caption := '';
    lblReferencia.Visible := FProcesando;
end;

{******************************** Function Header ******************************
Function Name: Inicializar
Author : ndonadio
Date Created : 30/08/2005
Description :   Realia tareas de inicializacion del form...
Parameters : None
Return Value : boolean
*******************************************************************************}
function TfrmRecpcionNuevasDireccionesSerbanc.Inicializar(Titulo: AnsiString): boolean;
resourcestring
    ERROR_CANNOT_INITIALIZE     = 'No se puede inicializar la interfaz.';
    MSG_NOTHING_TO_PROCESS      = 'No hay Activaciones Pendientes.';
    ERROR_CANNOT_GET_PARAMETER  = 'Error obteniendo el parametro general %s';
const
    DIR_ORIGEN_NVSDIR_SERBANC = 'DIR_ORIGEN_NVSDIR_SERBANC';
begin
    // Obtiene el directorio "default" donde deber�a estar el archivo
    Result := False;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

    // Le pongo de caption lo que trae de parametro...
    Caption :=  Titulo;
    //Centro el form
 	CenterForm(Self);
    // Cargar el parametro general de directorio de origen standard
    if not ObtenerParametroGeneral(DMConnections.BaseCAC, DIR_ORIGEN_NVSDIR_SERBANC, FOrigen) then begin
        MsgBoxErr(ERROR_CANNOT_INITIALIZE, Format(ERROR_CANNOT_GET_PARAMETER, [DIR_ORIGEN_NVSDIR_SERBANC]), Caption, MB_ICONSTOP);
        Exit;
    end;
    txtOrigen.Text := GoodDir(FOrigen);
    FProcesando := False; // indico que no se esta procesando
    HabilitarBotones; // Habilita/Dehabilita botones y demas segun FProcesando.
    Result := True;
end;

{******************************** Function Header ******************************
Function Name: ActualizarProgreso
Author : ndonadio
Date Created : 30/08/2005
Description :   Actualiza la barra de progreso y el label referencia.
                Llama al process mesaages de la aplicacion
Parameters : Mensaje: AnsiString
Return Value : None
*******************************************************************************}
procedure TfrmRecpcionNuevasDireccionesSerbanc.ActualizarProgreso(Mensaje: AnsiString);
begin
    pbProgreso.StepIt;
    lblReferencia.Caption := Mensaje;
    Application.ProcessMessages;
end;

procedure TfrmRecpcionNuevasDireccionesSerbanc.txtOrigenButtonClick(Sender: TObject);
    {******************************** Function Header ******************************
    Function Name: ObtenerFiltro
    Author : ndonadio
    Date Created : 10/08/2005
    Description :
    Parameters : None
    Return Value : AnsiString
    *******************************************************************************}
    Function ObtenerFiltro: AnsiString;
    Const
        FILE_TITLE	   = 'Archivo de Nuevas Direcciones|';
        FILE_NAME 	   = 'NVS_DIR_';
        FILE_EXTENSION = '.txt';
    begin
            Result:= FILE_TITLE + FILE_NAME + '*' + FILE_EXTENSION;
    end;

resourcestring
    MSG_ERROR = 'Error';
  	MSG_ERROR_FILE_DOES_NOT_EXIST   = 'El archivo %s no existe';
  	MSG_ERROR_INVALID_FILENAME 		= 'El nombre del archivo %s es inv�lido';
  	MSG_FILE_ALREADY_PROCESSED 		= 'El archivo %s ya fue procesado';
var
    Filtro: string;
begin
    //obtengo el filtro a aplicar a este tipo de archivos
    Filtro := ObtenerFiltro;

    Opendialog.InitialDir := txtOrigen.Text;;
  	OpenDialog.FileName := '';
    OpenDialog.Filter := Filtro; //solo veo este tipo de archivos

    if opendialog.execute then begin
        txtOrigen.text:=UpperCase( opendialog.filename );
        // si el archivo no existe...
        if not FileExists( txtOrigen.text ) then begin
        	MsgBox( Format ( MSG_ERROR_FILE_DOES_NOT_EXIST, [txtOrigen.text]), MSG_ERROR, MB_ICONERROR );
            Exit;
        end;
        // si el archivo ya se proceso...
    	if  VerificarArchivoProcesado( DMConnections.BaseCAC, txtOrigen.text ) then begin
    			MsgBox (Format(MSG_FILE_ALREADY_PROCESSED, [ExtractFileName(txtOrigen.text)]),Caption, MB_ICONSTOP);
                Exit;
    	end;
        btnProcesar.Enabled := True;
	end;

end;

{******************************** Function Header ******************************
Function Name: VerificarHeader
Author : ndonadio
Date Created : 30/08/2005
Description :   Valida el header
Parameters : strFile: TStringList; var Error: AnsiString
Return Value : boolean
*******************************************************************************}
function TfrmRecpcionNuevasDireccionesSerbanc.VerificarHeader(strFile: TStringList;
  var Error: AnsiString): boolean;
resourcestring
    ERROR_CANTIDAD_INVALIDA         = '"%s" no es un valor numerico v�lido.' ;
    ERROR_CANTIDAD_NO_CONCORDANTE   = 'La cantidad informada en el Header ' + CRLF +
                                      'no concuerda con la cantidad de lineas en el archivo.';
    HEADER_NOT_FOUND                = 'No se puede leer el Header. El archivo puede estar vacio.';
    CAP_VALIDATING_HEADER           = 'Validando Header del Archivo';
var
    auxHeader: AnsiString;
    sFecha, sCant: AnsiString;
    Fecha: TDateTime;
    Cantidad: Integer;
begin
    Result := False;
    // parseo el header
    try
        auxHeader := strFile.Strings[0];
    except
        Error := HEADER_NOT_FOUND;
        Exit;
    end;
    sFecha  := copy(auxHEader, 1,14);
    sCant   := copy(auxHeader, 15, 7);
    // valido los datos del header
    if not FechaHoraAAAAMMDDToDateTime(DMConnections.BaseCAC, sFecha, Fecha, Error) then Exit;
    try
        Cantidad := StrToInt(sCant);
    except
        Error := Format(ERROR_CANTIDAD_INVALIDA, [sCant]);
        Exit;
    end;
    // Ahora verifico que los valores concuerden con el archivo.
    if Cantidad <> (strFile.Count-1) then begin
        Error := ERROR_CANTIDAD_NO_CONCORDANTE;
        Exit;
    end;
    FFechaArchivo := Fecha;
    Error := '';
    ActualizarProgreso(CAP_VALIDATING_HEADER);
    Result := True;
end;

{******************************** Function Header ******************************
Function Name: VerificarSintaxis
Author : ndonadio
Date Created : 30/08/2005
Description : Verifica la sintaxis del archivo.
Parameters : strFile: TStringList; var Error: AnsiString
Return Value : boolean
*******************************************************************************}
function TfrmRecpcionNuevasDireccionesSerbanc.VerificarSintaxis(strFile: TStringList;
  var Error: AnsiString): boolean;
resourcestring
    ERR_LINE_LENGTH = 'La l�nea %d tiene una longitud inv�lida. El archivo no puede ser procesado.';
    CAP_VERIFYING_SYNTAX = 'Validando Sintaxis Linea %d de %d';
const
    LINE_LENGTH = 303;
var
    i: integer;
begin
    Result := False;
    for i := 1 to strFile.Count-1 do begin
        if  Length(strFile.Strings[i]) <> 303 then begin
            Error := FORMAT( ERR_LINE_LENGTH, [i]);
            Exit;
        end;
        //ValidarLinea(strFile.Strings[i])
        ActualizarProgreso(Format(CAP_VERIFYING_SYNTAX, [i,strFile.Count-1 ]));
    end;
    Result := True;
end;

{******************************** Function Header ******************************
Function Name: ProcesarLinea
Author : ndonadio
Date Created : 30/08/2005
Description :   Procesa una linea y actualiza la direccion a trav�z de un sp.
Parameters : Linea: AnsiString; var Error: AnsiString
Return Value : Boolean
*******************************************************************************}
function TfrmRecpcionNuevasDireccionesSerbanc.ProcesarLinea(Linea: AnsiString;
  var Error: AnsiString): Boolean;
resourcestring
    ERROR_ACTUALIZANDO_DOMICILIO = 'Error al intentar actualizar el domicilio para el convenio %s';
var
    csvDireccion: AnsiString;
begin
    Result := False;
    if not ValidarLinea(Linea, csvDireccion, Error) then Exit;
    try
        //DMConnections.BaseCAC.BeginTrans;                                               //SS_1385_NDR_20150922
        DMConnections.BaseCAC.Execute('BEGIN TRAN fRecepcionDireccionesSerbanc');         //SS_1385_NDR_20150922

        with spActualizarDomicilioSerbanc do begin

            Parameters.ParamByName('@TipoDeudor').Value := ParseParamByNumber(csvDireccion, 1,'>'); // tipo deudor
            Parameters.ParamByName('@NumeroRUT').Value := ParseParamByNumber(csvDireccion, 2,'>'); // num RUT
            Parameters.ParamByName('@NumeroConvenio').Value := ParseParamByNumber(csvDireccion, 3,'>'); // num convenio
            Parameters.ParamByName('@SCalleDesnormalizada').Value := ParseParamByNumber(csvDireccion, 7,'>'); // dir calle
            Parameters.ParamByName('@SNumero').Value := ParseParamByNumber(csvDireccion, 8,'>'); // dir puerta
            Parameters.ParamByName('@SDetalle').Value := ParseParamByNumber(csvDireccion, 9,'>'); // dir detalle
            Parameters.ParamByName('@SCodigoPostal').Value := ParseParamByNumber(csvDireccion,10,'>'); // cp
            Parameters.ParamByName('@SDescripcionComuna').Value := ParseParamByNumber(csvDireccion,11,'>'); // comuna
            Parameters.ParamByName('@SCodigoRegion').Value := ParseParamByNumber(csvDireccion,12,'>'); // region
            Parameters.ParamByName('@Telefono1').Value := ParseParamByNumber(csvDireccion,13,'>'); // tel 1
            Parameters.ParamByName('@Telefono2').Value := ParseParamByNumber(csvDireccion,14,'>'); // tel 2
            Parameters.ParamByName('@Celular').Value := ParseParamByNumber(csvDireccion,15,'>'); // celular
            Parameters.ParamByName('@EMail').Value := ParseParamByNumber(csvDireccion,16,'>'); // email
            Parameters.ParamByName('@CodigoOperacion').Value := FCodigoOperacion;

            ExecProc;

            if Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
                if Parameters.ParamByName('@DescripcionError').Value = '' then
                        Error := Format(ERROR_ACTUALIZANDO_DOMICILIO , [ParseParamByNumber(csvDireccion, 3,',')])
                else    Error := Parameters.ParamByName('@DescripcionError').Value;
                //DMConnections.BaseCAC.RollbackTrans;                                                                              //SS_1385_NDR_20150922
                DMConnections.BaseCAC.Execute('IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION fRecepcionDireccionesSerbanc END');	    //SS_1385_NDR_20150922
                Exit;
            end;
        end; //with
        //DMConnections.BaseCAC.CommitTrans;                                                                //SS_1385_NDR_20150922
        DMConnections.BaseCAC.Execute('COMMIT TRAN fRecepcionDireccionesSerbanc');					                                //SS_1385_NDR_20150922
        Result := True;
    except
        on e:exception do begin
            //if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.RollbackTrans;                                  //SS_1385_NDR_20150922
            DMConnections.BaseCAC.Execute('IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION fRecepcionDireccionesSerbanc END');	    //SS_1385_NDR_20150922
            Error := e.Message;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: MostrarReporte
Author : ndonadio
Date Created : 30/08/2005
Description :   Lanza el reporte de finalizacion del proceso.
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmRecpcionNuevasDireccionesSerbanc.MostrarReporte;
resourcestring
    ERROR_REPORT    = 'No se puede mostrar el reporte';
var
    fr: TfrmReporteNvsDirSerbanc;
    descError: AnsiString;
begin
    try
        Application.Createform(TfrmReporteNvsDirSerbanc, fr);
        if not fr.MostrarReporte(FCodigoOperacion, Caption, descError) then begin
            MsgBoxErr(ERROR_REPORT, descError, Caption, MB_ICONERROR);
            Exit;
        end else begin
            fr.Release;
        end;
    except
        on e:exception do begin
            MsgBoxErr(ERROR_REPORT, e.Message, Caption, MB_ICONERROR);
            Exit;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: ValidarLinea
Author : ndonadio
Date Created : 30/08/2005
Description :  Valida los datos de una linea y genera una nueva linea
                con los valores separados por comas.
Parameters : Linea: AnsiString; var csvDireccion, Error: AnsiString
Return Value : Boolean
*******************************************************************************}
function TfrmRecpcionNuevasDireccionesSerbanc.ValidarLinea(Linea: AnsiString; var csvDireccion,
  Error: AnsiString): Boolean;
    function ValidarFormatoRUTConv(RUT: AnsiString): Boolean;
    var
        aux: AnsiString;
        i: Integer;
    begin
        Result := False;
        aux:= trim(RUT);
        //if Length(aux) > 10 then Exit;
        for i := 1 to Length(aux) do begin
            if not (aux[i] in ['0'..'9','k','K']) then Exit;
        end;
        Result := true;
    end;
    function ValidarEmail(Email, Convenio: AnsiString; var Error: AnsiString): Boolean;
    resourcestring
         ERROR_MAIL_INVALIDO = 'El valor %s no es v�lido como direccion de email. Convenio %s';
    var
        i: Integer;
        aux: AnsiString;
    begin
        if trim(Email) <> '' then begin
            Error := Format(ERROR_MAIL_INVALIDO,[TRIM(Email), TRIM(Convenio)]);
            Result := False;
            i := AnsiPos('@', EMail);
            if i = 0 then Exit;
            aux := Copy(Email, i+1, Length(email));
            if (AnsiPos('@', Aux) <> 0) then Exit;
            if (AnsiPos('.', Aux) = 0) then Exit;
        end;
        Error := '';
        Result := True;
    end;

ResourceString
    ERROR_RUT_INVALIDO              = 'El valor "%s" no es correcto como Numero de RUT';
    ERROR_CONVENIO_INVALIDO         = 'El valor "%s" no es correcto como Numero de Convenio';
    ERROR_TIPO_DEUDOR_INVALIDO    = 'El valor "%s" no es correcto como Tipo de Deudor';
var
    aux: AnsiString;
    Convenio: AnsiString;
begin
    Result := False;
    // 1.tipo de deudor
    aux := Copy(Linea, 1,1);
    try
        StrToInt(aux);
    except
        Error := Format(ERROR_TIPO_DEUDOR_INVALIDO, [trim(aux)]);
        Exit;
    end;
    csvDireccion := Aux;
    // 2.RUT
    aux := Copy(Linea, 2, 10);
    if not ValidarFormatoRUTConv(aux) then begin
        Error := Format(ERROR_RUT_INVALIDO, [trim(aux)]);
        Exit;
    end;
    csvDireccion := csvDireccion + '>' +Aux;
    // 3.NumeroConvenio
    aux := Copy(Linea, 12, 17);
    if ObtenerCodigoConcesionariaNativa=CODIGO_VS then                          //SS_1147Q_NDR_20141202[??]
    begin                                                                       //SS_1147Q_NDR_20141202[??]
      aux := Copy(aux, 6, 12);                                                  //SS_1147Q_NDR_20141202[??]
    end;                                                                        //SS_1147Q_NDR_20141202[??]

    if not ValidarFormatoRUTConv(aux) then begin
        Error := Format(ERROR_CONVENIO_INVALIDO, [trim(aux)]);
        Exit;
    end;
    Convenio := aux;
    csvDireccion := csvDireccion + '>' +Aux;
    // 4.nombre
    aux := Copy(Linea, 29, 30);
    csvDireccion := csvDireccion + '>' +Aux;
    // 5.Apeliido P
    aux := Copy(Linea, 59, 20);
    csvDireccion := csvDireccion + '>' +Aux;
    // 6.Apellido M
    aux := Copy(Linea, 79, 20);
    csvDireccion := csvDireccion + '>' +Aux;
    // 7.Direccion - Calle
    aux := Copy(Linea, 99, 50);
    csvDireccion := csvDireccion + '>' +Aux;
    // 8.Direccion - Puerta
    aux := Copy(Linea, 149, 10);
    csvDireccion := csvDireccion + '>' +Aux;
    // 9.Direccion - Detalle
    aux := Copy(Linea, 159, 30);
    csvDireccion := csvDireccion + '>' +Aux;
    // 10.Codigo Postal
    aux := Copy(Linea, 189, 7);
    csvDireccion := csvDireccion + '>' +Aux;
    // 11.Comuna
    aux := Copy(Linea, 196, 20);
    csvDireccion := csvDireccion + '>' +Aux;
    // 12.Region
    aux := Copy(Linea, 216, 3);
    csvDireccion := csvDireccion + '>' +Aux;
    // 13.Telefono 1
    aux := Copy(Linea, 219, 15);
    csvDireccion := csvDireccion + '>' +Aux;
    // 14.Telefono 2
    aux := Copy(Linea, 234, 15);
    csvDireccion := csvDireccion + '>' +Aux;
    // 15.Celular
    aux := Copy(Linea, 249, 15);
    csvDireccion := csvDireccion + '>' +Aux;
    // 16.Email
    aux := Copy(Linea, 264, 40);
    if not ValidarEmail(aux, Convenio, Error) then exit;
    csvDireccion := csvDireccion + '>' +Aux;
    Result := True;
end;

procedure TfrmRecpcionNuevasDireccionesSerbanc.txtOrigenChange(Sender: TObject);
begin
    btnProcesar.Enabled := (TRIM(ExtractFileName(txtOrigen.Text)) <> '') ;
end;


end.
