object FGenerarNominaCMRRNUT: TFGenerarNominaCMRRNUT
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = 'N'#243'mina Contratos CMR RNUT'
  ClientHeight = 210
  ClientWidth = 415
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  DesignSize = (
    415
    210)
  PixelsPerInch = 96
  TextHeight = 13
  object bvl1: TBevel
    Left = 8
    Top = 8
    Width = 399
    Height = 145
    Anchors = [akLeft, akTop, akRight, akBottom]
    ExplicitWidth = 318
  end
  object lbl1: TLabel
    Left = 29
    Top = 28
    Width = 283
    Height = 13
    Caption = 'Destino Archivo de N'#243'mina Contratos CMR RNUT'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblReferencia: TLabel
    Left = 43
    Top = 92
    Width = 338
    Height = 26
    Anchors = [akLeft, akBottom]
    Caption = 
      'Este proceso puede tardar varios minutos, cuando se haya '#13#10'compl' +
      'etado, aparecer'#225' un mensaje de confirmaci'#243'n.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = False
  end
  object btnSalir: TButton
    Left = 332
    Top = 176
    Width = 75
    Height = 26
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Salir'
    TabOrder = 0
    OnClick = btnSalirClick
  end
  object btnGenerar: TButton
    Left = 251
    Top = 176
    Width = 75
    Height = 26
    Anchors = [akRight, akBottom]
    Caption = '&Generar'
    TabOrder = 1
    OnClick = btnGenerarClick
  end
  object txtDestino: TEdit
    Left = 29
    Top = 51
    Width = 364
    Height = 21
    Hint = 'Ruta donde se almacenara el archivo de contrato'
    Anchors = [akLeft, akTop, akRight]
    Enabled = False
    ReadOnly = True
    TabOrder = 2
  end
  object spGenerarContratosCMR_RNUT: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'GenerarContratosCMR_RNUT;1'
    Parameters = <>
    Left = 24
    Top = 176
  end
end
