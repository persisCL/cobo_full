object frmReprocesarDiscoXMLInfractores: TfrmReprocesarDiscoXMLInfractores
  Left = 306
  Top = 129
  BorderStyle = bsDialog
  Caption = 'Reprocesar Generaci'#243'n de Disco XML de Infracciones'
  ClientHeight = 276
  ClientWidth = 582
  Color = clBtnFace
  Constraints.MinHeight = 308
  Constraints.MinWidth = 588
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  DesignSize = (
    582
    276)
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 6
    Top = 7
    Width = 569
    Height = 226
    Anchors = [akLeft, akTop, akRight, akBottom]
  end
  object Bevel2: TBevel
    Left = 349
    Top = 24
    Width = 219
    Height = 161
    Anchors = [akTop, akRight, akBottom]
    Style = bsRaised
  end
  object Label1: TLabel
    Left = 14
    Top = 10
    Width = 109
    Height = 13
    Caption = 'Discos Procesados'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 357
    Top = 46
    Width = 74
    Height = 16
    Anchors = [akTop, akRight]
    Caption = 'Disco Nro:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblNumeroDisco: TLabel
    Left = 437
    Top = 46
    Width = 121
    Height = 16
    Alignment = taRightJustify
    Anchors = [akTop, akRight]
    AutoSize = False
    Caption = 'lblNumeroDisco'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblIndiceReproceso: TLabel
    Left = 437
    Top = 70
    Width = 121
    Height = 13
    Alignment = taRightJustify
    Anchors = [akTop, akRight]
    AutoSize = False
    Caption = 'lblIndiceReproceso'
  end
  object Label5: TLabel
    Left = 357
    Top = 114
    Width = 92
    Height = 13
    Anchors = [akTop, akRight]
    Caption = 'Archivos Incluidos: '
  end
  object Label6: TLabel
    Left = 357
    Top = 135
    Width = 106
    Height = 13
    Anchors = [akTop, akRight]
    Caption = 'Infracciones Incluidas:'
  end
  object lblArchivos: TLabel
    Left = 485
    Top = 114
    Width = 73
    Height = 13
    Alignment = taRightJustify
    Anchors = [akTop, akRight]
    AutoSize = False
    Caption = 'lblArchivos'
  end
  object lblInfracciones: TLabel
    Left = 493
    Top = 135
    Width = 65
    Height = 13
    Alignment = taRightJustify
    Anchors = [akTop, akRight]
    AutoSize = False
    Caption = 'lblInfracciones'
  end
  object Label3: TLabel
    Left = 357
    Top = 95
    Width = 33
    Height = 13
    Anchors = [akTop, akRight]
    Caption = 'Fecha:'
  end
  object lblFecha: TLabel
    Left = 485
    Top = 95
    Width = 73
    Height = 13
    Alignment = taRightJustify
    Anchors = [akTop, akRight]
    AutoSize = False
    Caption = 'lblFecha'
  end
  object Label4: TLabel
    Left = 357
    Top = 157
    Width = 101
    Height = 13
    Anchors = [akTop, akRight]
    Caption = 'Infracciones a Incluir:'
  end
  object lblInfARepro: TLabel
    Left = 493
    Top = 157
    Width = 65
    Height = 13
    Alignment = taRightJustify
    Anchors = [akTop, akRight]
    AutoSize = False
    Caption = 'lblInfARepro'
  end
  object btnProcesar: TButton
    Left = 420
    Top = 242
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Procesar'
    TabOrder = 0
    OnClick = btnProcesarClick
  end
  object btnSalir: TButton
    Left = 500
    Top = 242
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Salir'
    TabOrder = 1
    OnClick = btnSalirClick
  end
  object dblDiscos: TDBListEx
    Left = 11
    Top = 26
    Width = 333
    Height = 155
    Anchors = [akLeft, akTop, akRight, akBottom]
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 90
        Header.Caption = 'Nro. Correlativo'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        Color = clWindow
        IsLink = False
        FieldName = 'CorrelativoDisco'
      end
      item
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 120
        Header.Caption = 'Cant. Reprocesos'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        Color = clWindow
        IsLink = False
        FieldName = 'Reprocesos'
      end
      item
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 100
        Header.Caption = 'Fecha Ult. Proc.'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        Color = clWindow
        IsLink = False
        FieldName = 'Fecha'
      end>
    Color = clWhite
    DataSource = dsObtenerEnviosXMLInfracciones
    DragReorder = True
    ParentColor = False
    TabOrder = 2
    TabStop = True
    OnClick = dblDiscosClick
  end
  object pnlAvance: TPanel
    Left = 11
    Top = 192
    Width = 557
    Height = 34
    Anchors = [akLeft, akRight, akBottom]
    TabOrder = 3
    DesignSize = (
      557
      34)
    object labelAvance: TLabel
      Left = 10
      Top = 8
      Width = 56
      Height = 13
      Caption = 'Avance...'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object pbProgreso: TProgressBar
      Left = 127
      Top = 7
      Width = 422
      Height = 17
      Anchors = [akLeft, akTop, akRight]
      Smooth = True
      Step = 1
      TabOrder = 0
    end
  end
  object spObtenerEnviosXMLInfracciones: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerEnviosXMLInfracciones'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 8
    Top = 240
  end
  object dsObtenerEnviosXMLInfracciones: TDataSource
    DataSet = spObtenerEnviosXMLInfracciones
    Left = 48
    Top = 240
  end
  object spObtenerInfraccionesAEnviarMOP: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerInfraccionesAEnviarMOP'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaDesde'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaHasta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoInfraccion'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@Disco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 93
    Top = 240
  end
  object spObtenerInfraccionesTransitosAEnviarMOP: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerInfraccionesTransitosAEnviarMOP'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoInfraccionINicial'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoInfraccionFinal'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Disco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 126
    Top = 241
  end
  object spActualizarDetalleInfraccionesEnviadas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDetalleInfraccionesEnviadas'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoOperacionInterface'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoInfraccion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoEstado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDetalleEstado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoOperacionRecepcion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 168
    Top = 240
  end
  object spBorrarReprocesoParcialXMLInfracciones: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'BorrarReprocesoParcialXMLInfracciones'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CorrelativoDisco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IndiceReproceso'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 208
    Top = 240
  end
  object spValidarImagenDeReferenciaInfraccion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ValidarImagenDeReferenciaInfraccion'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Infraccion'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end>
    Left = 248
    Top = 240
  end
  object spActualizarReprocesoXMLInfracciones: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarReprocesoXMLInfracciones'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@CorrelativoDisco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@IndiceReproceso'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
      end>
    Left = 336
    Top = 216
  end
end
