{-------------------------------------------------------------------------------
 File Name: FrmRptRecepcionComprobantesServipag.pas
 Author:    ggomez
 Date Created: 10/07/2006
 Language: ES-AR
 Description: Gestion de Interfases - Reporte de finalizacion del proceso
              Informa la cantidad de comprobantes recibidos , el importe total y
              otra informacion util para verificar si el proceso se realizo
              segun parametros normales.



Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.


Firma       :   SS_1147_MBE_20150414
Description :   Se corrigen errores en la generaci�n del reporte

-------------------------------------------------------------------------------}
unit FrmRptRecepcionComprobantesServipag;

interface

uses
  //Reporte
  DMConnection,              //Coneccion a base de datos OP_CAC
  Util,                      //Stringtofile,padl..
  UtilProc,                  //Mensajes
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UtilRB, ppCtrls, ppBands, ppPrnabl, ppClass, ppCache, ppProd,
  ppReport, ppDB, ppComm, ppRelatv, ppTxPipe, DB, ppDBPipe, ADODB,
  ppParameter, ppModule, raCodMod, ppStrtch, ppSubRpt,RBSetup, daDataModule, ConstParametrosGenerales; //SS_1147_NDR_20140710

type
  TFrmRptRecepcionComprobantesServipag = class(TForm)
    ppReporte: TppReport;
    ppParameterList1: TppParameterList;
    RBIListado: TRBInterface;
    spObtenerReporteRecepcionComprobantesServipag: TADOStoredProc;
    dbplspObtenerReporteRecepcionComprobantesServipag: TppDBPipeline;
    dsspObtenerReporteRecepcionComprobantesServipag: TDataSource;
    spObtenerReportePagosRechazadosServipag: TADOStoredProc;
    dsspObtenerReportePagosRechazadosServipag: TDataSource;
    dbplspObtenerReportePagosRechazadosServipag: TppDBPipeline;
    spObtenerReporteRecepcionComprobantesServipagCodigoServicio: TStringField;
    spObtenerReporteRecepcionComprobantesServipagCantidadAceptados: TIntegerField;
    spObtenerReporteRecepcionComprobantesServipagMontoAceptados: TLargeIntField;
    spObtenerReporteRecepcionComprobantesServipagMontoAceptadosAMostrar: TStringField;
    spObtenerReporteRecepcionComprobantesServipagCantidadRechazados: TIntegerField;
    spObtenerReporteRecepcionComprobantesServipagMontoRechazados: TLargeIntField;
    spObtenerReporteRecepcionComprobantesServipagMontoRechazadosAMostrar: TStringField;
    spObtenerReporteRecepcionComprobantesServipagCantidadTotal: TIntegerField;
    spObtenerReporteRecepcionComprobantesServipagMontoTotal: TLargeIntField;
    spObtenerReporteRecepcionComprobantesServipagMontoTotalAMostrar: TStringField;
    spObtenerReportePagosRechazadosServipagNumeroComprobante: TLargeintField;
    spObtenerReportePagosRechazadosServipagNumeroConvenio: TStringField;
    spObtenerReportePagosRechazadosServipagCodigoServicio: TStringField;
    spObtenerReportePagosRechazadosServipagMonto: TLargeIntField;
    spObtenerReportePagosRechazadosServipagMontoAMostrar: TStringField;
    spObtenerReportePagosRechazadosServipagMotivoRechazo: TStringField;
    ppHeaderBand1: TppHeaderBand;
    ppImage2: TppImage;
    ppLtitulo: TppLabel;
    ppModulo: TppLabel;
    pptFechaProcesamiento: TppLabel;
    pptNombreArchivo: TppLabel;
    pptUsuario: TppLabel;
    ppFechaProcesamiento: TppLabel;
    ppNombreArchivo: TppLabel;
    ppUsuario: TppLabel;
    ppLine3: TppLine;
    ppLabel5: TppLabel;
    ppNumeroProceso: TppLabel;
    ppLine8: TppLine;
    ppDetailBand1: TppDetailBand;
    ppSubReportDetalleComprobantesRechazados: TppSubReport;
    ChildReporteDetalleRechazos: TppChildReport;
    ppTitleBand3: TppTitleBand;
    ppLine9: TppLine;
    lblDetalleDeRechazos: TppLabel;
    ppLine10: TppLine;
    lblNumeroComprobante: TppLabel;
    lblMotivoRechazo: TppLabel;
    lblMonto: TppLabel;
    ppLine11: TppLine;
    lblConvenio: TppLabel;
    lblCodigoServicio: TppLabel;
    ppDetailBand4: TppDetailBand;
    dtxtNumeroComprobante: TppDBText;
    dtxtMotivoRechazo: TppDBText;
    dtxtMontoAMostrar: TppDBText;
    dtxtNumeroConvenio: TppDBText;
    dtxtCodigoServicio: TppDBText;
    ppSummaryBand1: TppSummaryBand;
    raCodeModule4: TraCodeModule;
    ppSubReportResumenRechazos: TppSubReport;
    ChilReportResumen: TppChildReport;
    ppTitleBand2: TppTitleBand;
    ppLine5: TppLine;
    lblResumen: TppLabel;
    ppLine6: TppLine;
    lblResumenCodigoServicio: TppLabel;
    lblResumenCantidadTotal: TppLabel;
    lblMontoTotal: TppLabel;
    ppLine7: TppLine;
    lblResumenCantidadAceptados: TppLabel;
    lblResumenMontoAceptados: TppLabel;
    lblCantidadRecahazados: TppLabel;
    lblResumenMontoRechazados: TppLabel;
    ppDetailBand3: TppDetailBand;
    dtxtResumenCodigoServicio: TppDBText;
    dtxtMontoRechazadosAMostrar: TppDBText;
    dtxtMontoTotalAMostrar: TppDBText;
    dtxtResumenCantidadAceptados: TppDBText;
    dtxtMontoAceptadosAMostrar: TppDBText;
    dtxtResumenCantidadRechazados: TppDBText;
    dtxtResumenCantidadTotal: TppDBText;
    ppSummaryBand3: TppSummaryBand;
    ppLabel9: TppLabel;
    ppLine13: TppLine;
    ppLine14: TppLine;
    dbcTotalCantidadAceptados: TppDBCalc;
    dbcTotalCantidadRechazados: TppDBCalc;
    dbcTotalCantidadTotal: TppDBCalc;
    raCodeModule1: TraCodeModule;
    lblTotalMontoAceptados: TppLabel;
    lblTotalMontoRechazados: TppLabel;
    lblTotalMontoTotal: TppLabel;
    ppLabel1: TppLabel;
    ppLineas: TppLabel;
    ppLabel3: TppLabel;
    ppMontoArchivo: TppLabel;
    ppSubReport1: TppSubReport;
    ppChildReport1: TppChildReport;
    ppTitleBand1: TppTitleBand;
    ppDetailBand2: TppDetailBand;
    ppSummaryBand2: TppSummaryBand;
    ppLabel2: TppLabel;
    ppDBText1: TppDBText;
    dsErrores: TDataSource;
    ppDBPipelineErrores: TppDBPipeline;
    qryErroresInterfases: TADOQuery;
    procedure RBIListadoExecute(Sender: TObject; var Cancelled: Boolean);
  private
    FNombreReporte: String;
    // Para mantener el c�digo de operaci�n interfase para el cual generar el reporte.
    FCodigoOperacionInterfase: Integer;
    { Private declarations }
  public
    function Inicializar(NombreReporte: AnsiString; CodigoOperacionInterfase: Integer): Boolean;
    { Public declarations }
  end;

var
  FRptRecepcionComprobantesServipag: TFrmRptRecepcionComprobantesServipag;

implementation

{$R *.dfm}

const
 CONST_SIN_DATOS = 'Sin datos.';

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    ggomez
  Date Created: 10/07/2006
  Description: Inicializo este formulario
  Parameters: NombreReporte: AnsiString
    CodigoOperacionInterfase: Integer
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFrmRptRecepcionComprobantesServipag.Inicializar(NombreReporte: AnsiString; CodigoOperacionInterfase: Integer): Boolean;
var                                                                             //SS_1147_NDR_20140710
  RutaLogo: AnsiString;                                                         //SS_1147_NDR_20140710
begin
      ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
      try                                                                                                 //SS_1147_NDR_20140710
        ppImage2.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
      except                                                                                              //SS_1147_NDR_20140710
        On E: Exception do begin                                                                          //SS_1147_NDR_20140710
          Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
          MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
          Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
        end;                                                                                              //SS_1147_NDR_20140710
      end;                                                                                                //SS_1147_NDR_20140710

    FNombreReporte              := NombreReporte;
    FCodigoOperacionInterfase   := CodigoOperacionInterfase;
    // Ejecutar el reporte.
    RBIListado.Execute;
    Result := True;
end;


{-----------------------------------------------------------------------------
  Function Name: RBIListadoExecute
  Author:    ggomez
  Date Created: 10/07/2006
  Description:  Ejecuto el informe
  Parameters: Sender: TObject;var Cancelled: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrmRptRecepcionComprobantesServipag.RBIListadoExecute(Sender: TObject;var Cancelled: Boolean);
resourcestring
    MSG_ERROR_TO_GENERATE_REPORT = 'Error al generar el reporte.';
    MSG_ERROR = 'Error';
var
    Config: TRBConfig;
    Fecha : TDateTime;
begin
    // Configuraci�n del reporte
    Config := rbiListado.GetConfig;
    if Config.BinName <> '' then begin
        rbiListado.Report.PrinterSetup.BinName := Config.BinName;
    end;

    try
        // Abrir la consulta de Comprobantes Pagos
        spObtenerReporteRecepcionComprobantesServipag.Close;
        spObtenerReporteRecepcionComprobantesServipag.Parameters.Refresh;
        spObtenerReporteRecepcionComprobantesServipag.Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacionInterfase;
        spObtenerReporteRecepcionComprobantesServipag.Parameters.ParamByName('@FechaProcesamiento').Value := NULL;
        spObtenerReporteRecepcionComprobantesServipag.Parameters.ParamByName('@NombreArchivo').Value := NULL;
        spObtenerReporteRecepcionComprobantesServipag.Parameters.ParamByName('@Modulo').Value := NULL;
        spObtenerReporteRecepcionComprobantesServipag.Parameters.ParamByName('@Usuario').Value := NULL;
        spObtenerReporteRecepcionComprobantesServipag.Parameters.ParamByName('@MontoTotalAceptadosAMostrar').Value := Null;
        spObtenerReporteRecepcionComprobantesServipag.Parameters.ParamByName('@MontoTotalRechazadosAMostrar').Value := Null;
        spObtenerReporteRecepcionComprobantesServipag.Parameters.ParamByName('@MontoTotalAMostrar').Value := Null;
        spObtenerReporteRecepcionComprobantesServipag.Parameters.ParamByName('@LineasArchivo').Value := Null;
        spObtenerReporteRecepcionComprobantesServipag.Parameters.ParamByName('@MontoArchivo').Value := Null;

        spObtenerReporteRecepcionComprobantesServipag.CommandTimeOut := 500;
        spObtenerReporteRecepcionComprobantesServipag.Open;

        // Asigno los valores a los campos del reporte
        ppmodulo.Caption                := spObtenerReporteRecepcionComprobantesServipag.Parameters.ParamByName('@Modulo').Value;
//        ppFechaProcesamiento.Caption    := DateToStr(spObtenerReporteRecepcionComprobantesServipag.Parameters.ParamByName('@FechaProcesamiento').Value);              // SS_1147_MBE_20150414
        Fecha := VarToDateTime(spObtenerReporteRecepcionComprobantesServipag.Parameters.ParamByName('@FechaProcesamiento').Value);                                                     // SS_1147_MBE_20150414
        ppFechaProcesamiento.Caption    := FormatDateTime('dd/mm/yyyy', Fecha);                                                                                         // SS_1147_MBE_20150414
        ppNombreArchivo.Caption         := Copy(spObtenerReporteRecepcionComprobantesServipag.Parameters.ParamByName('@NombreArchivo').Value, 1, 40);
        ppUsuario.Caption               := spObtenerReporteRecepcionComprobantesServipag.Parameters.ParamByName('@Usuario').Value;
        ppNumeroProceso.Caption         := IntToStr(FCodigoOperacionInterfase);
        ppLineas.Caption                := iif(spObtenerReporteRecepcionComprobantesServipag.Parameters.ParamByName('@LineasArchivo').Value = 0,CONST_SIN_DATOS,IntToStr(spObtenerReporteRecepcionComprobantesServipag.Parameters.ParamByName('@LineasArchivo').Value));
        ppMontoArchivo.Caption          := iif(spObtenerReporteRecepcionComprobantesServipag.Parameters.ParamByName('@MontoArchivo').Value = '0', CONST_SIN_DATOS, spObtenerReporteRecepcionComprobantesServipag.Parameters.ParamByName('@MontoArchivo').Value);

        lblTotalMontoAceptados.Caption  := '';
        lblTotalMontoRechazados.Caption := '';
        lblTotalMontoTotal.Caption      := '';
        if spObtenerReporteRecepcionComprobantesServipag.Parameters.ParamByName('@MontoTotalAceptadosAMostrar').Value <> NULL then
        	lblTotalMontoAceptados.Caption := spObtenerReporteRecepcionComprobantesServipag.Parameters.ParamByName('@MontoTotalAceptadosAMostrar').Value;

        if spObtenerReporteRecepcionComprobantesServipag.Parameters.ParamByName('@MontoTotalRechazadosAMostrar').Value <> NULL then
        	lblTotalMontoRechazados.Caption := spObtenerReporteRecepcionComprobantesServipag.Parameters.ParamByName('@MontoTotalRechazadosAMostrar').Value;

        if spObtenerReporteRecepcionComprobantesServipag.Parameters.ParamByName('@MontoTotalAMostrar').Value <> NULL then
        	lblTotalMontoTotal.Caption := spObtenerReporteRecepcionComprobantesServipag.Parameters.ParamByName('@MontoTotalAMostrar').Value;

        // Obtener los comprobantes rechazados.
        spObtenerReportePagosRechazadosServipag.Close;
        spObtenerReportePagosRechazadosServipag.Parameters.Refresh;
        spObtenerReportePagosRechazadosServipag.Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacionInterfase;
        spObtenerReportePagosRechazadosServipag.CommandTimeOut := 500;
        spObtenerReportePagosRechazadosServipag.Open;


        //abrir la consulta de errores Interfases
        qryErroresInterfases.Close;
        qryErroresInterfases.SQL.Text := 'SELECT * FROM ErroresInterfases WHERE CodigoOperacionInterfase = ' + IntToStr(FCodigoOperacionInterfase);
        qryErroresInterfases.Open;

        
        //Configuro el Report Builder y Ejecuto el Reporte
        rbiListado.Caption := FNombreReporte;

    except
        on E : Exception do begin
            MsgBoxErr(MSG_ERROR_TO_GENERATE_REPORT, E.Message, MSG_ERROR, MB_ICONERROR);
        end;
    end;
end;

end.


