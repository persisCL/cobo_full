{------------------------------------------------------------------------------------
 File Name: FrmRptEnvioInfraccionesXML.pas
 Author:    lgisuk
 Date Created: 20/01/2006
 Language: ES-AR
 Description: M�dulo de la interface Infracciones - Reporte de Envio de Infracciones

 Revision : 1
 Author   : pdominguez
 Date     : 23/06/2010
 Description : Infractores Fase 2
    - Se modificaron los siguientes procedimientos:
        Inicializar.

 Revision : 2
 Author   : pdominguez
 Date     : 08/07/2010
 Description : Infractores Fase 2
    - Se modificaron los siguientes objetos:
        spObtenerDatosReporteInfraccionesTransitosAEnviarMOP: TADOStoredProc.
        
    - Se modificaron los siguientes procedimientos:
        Inicializar.



Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.


-------------------------------------------------------------------------------------}
unit FrmRptEnvioInfraccionesXML;

interface

uses
    //Reporte
    DMConnection,  //Coneccion a base de datos OP_CAC
    Util,          //Stringtofile,padl..
    UtilProc,      //Mensajes
    //General
    Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
    Dialogs, DB, ADODB, ppParameter, ppBands, ppCtrls, ppPrnabl, ppClass,
    ppCache, ppComm, ppRelatv, ppProd, ppReport, UtilRB, ppDB, ppDBPipe,
    DBClient, ppPageBreak, ppStrtch, ppSubRpt, ppModule, raCodMod, ConstParametrosGenerales;       //SS_1147_NDR_20140710

type
  TfrmRptEnvioXMLInfracciones = class(TForm)
    ppReporte: TppReport;
    ppParameterList1: TppParameterList;
    dsDatosReporte: TDataSource;
    ppDBDatosReporte: TppDBPipeline;
    RBIListado: TRBInterface;
    dsErroresReporte: TDataSource;
    ppDBErroresReporte: TppDBPipeline;
    spObtenerDatosReporteInfraccionesTransitosAEnviarMOP: TADOStoredProc;
    spObtenerErroresReporteInfraccionesTransitosAEnviarMOP: TADOStoredProc;
    ppHeaderBand1: TppHeaderBand;
    ppImage2: TppImage;
    ppLtitulo: TppLabel;
    ppLine3: TppLine;
    ppLabel2: TppLabel;
    ppLabel4: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppSubReportResultado: TppSubReport;
    ppChildReport1: TppChildReport;
    ppTitleBand1: TppTitleBand;
    ppTipoComprobante: TppLabel;
    ppMinComprobante: TppLabel;
    ppMaxComprobante: TppLabel;
    ppMinFechaEmision: TppLabel;
    ppLine2: TppLine;
    ppLabel1: TppLabel;
    ppLabel3: TppLabel;
    ppDetailBand2: TppDetailBand;
    ppDBText1: TppDBText;
    ppDBText6: TppDBText;
    ppDBText5: TppDBText;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    ppSummaryBand2: TppSummaryBand;
    pplblTotales: TppLabel;
    ppDBCalc1: TppDBCalc;
    ppDBCalc2: TppDBCalc;
    ppDBCalcDiscos: TppDBCalc;
    pplblDiscos: TppLabel;
    ppLine4: TppLine;
    ppSubReportErrores: TppSubReport;
    ppChildReport3: TppChildReport;
    ppTitleBand3: TppTitleBand;
    ppLabel5: TppLabel;
    ppLabel7: TppLabel;
    ppLine5: TppLine;
    ppDetailBand4: TppDetailBand;
    ppDBText8: TppDBText;
    ppSummaryBand4: TppSummaryBand;
    ppSummaryBand1: TppSummaryBand;
    raCodeModule2: TraCodeModule;
    procedure RBIListadoExecute(Sender: TObject; var Cancelled: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    function Inicializar(CodigoOperacion, CodigoOperacionReemision: Integer; NombreConcesionaria: AnsiString; var DescError: AnsiString): Boolean;
  end;

var
  frmRptEnvioXMLInfracciones: TfrmRptEnvioXMLInfracciones;


implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    lgisuk
  Date Created: 20/01/2006
  Description: Inicializo este formulario
  Parameters: NombreReporte: AnsiString
  Return Value: Boolean

 Revision : 1
 Author   : pdominguez
 Date     : 23/06/2010
 Description : Infractores Fase 2
    -  Se cambian los par�metros de inicializaci�n. Se a�aden los dos nuevos
    objetos contenedores de los datos a mostrar, spObtenerDatosReporteInfraccionesTransitosAEnviarMOP y
    spObtenerErroresReporteInfraccionesTransitosAEnviarMOP

 Revision : 2
 Author   : pdominguez
 Date     : 08/07/2010
 Description : Infractores Fase 2
    - Se a�ade el nuevo par�metro CodigoOperacionReemision.
    - Se a�ade la asignaci�n del nuevo par�metro @CodigoOperacionInterfaseReEmision
    del objeto spObtenerDatosReporteInfraccionesTransitosAEnviarMOP: TADOStoredProc.
-----------------------------------------------------------------------------}
function TfrmRptEnvioXMLInfracciones.Inicializar(CodigoOperacion, CodigoOperacionReemision: Integer; NombreConcesionaria: AnsiString; var DescError: AnsiString): Boolean;
resourcestring
    MSG_INIT_ERROR = 'No se puede inicializar el reporte de finalizaci�n';
    MSG_ERROR = 'Error';
    rsTituloReporte = 'Reporte de %s de Archivos XML de Infracciones';
var
    i : integer;
    RutaLogo: AnsiString;                                                         //SS_1147_NDR_20140710
begin
    ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
    try                                                                                                 //SS_1147_NDR_20140710
      ppImage2.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
    except                                                                                              //SS_1147_NDR_20140710
      On E: Exception do begin                                                                          //SS_1147_NDR_20140710
        Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
        MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
        Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
      end;                                                                                              //SS_1147_NDR_20140710
    end;                                                                                                //SS_1147_NDR_20140710

    try
        Result := False;

        try
            with spObtenerDatosReporteInfraccionesTransitosAEnviarMOP do begin
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoOperacionInterfase').Value := iif(CodigoOperacion = 0, NULL, CodigoOperacion);
                Parameters.ParamByName('@CodigoOperacionInterfaseReEmision').Value := iif(CodigoOperacionReemision = 0, NULL, CodigoOperacionReemision);
                Open;
            end;

            with spObtenerErroresReporteInfraccionesTransitosAEnviarMOP do begin
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoOperacionInterfase').Value := iif(CodigoOperacion = 0, CodigoOperacionReemision, CodigoOperacion);
                Open;
            end;

            ppLabel4.Caption := 'Concesionaria: ' + NombreConcesionaria;
            ppLtitulo.Caption := Format(rsTituloReporte,[iif(CodigoOperacion <> 0, 'Generaci�n', 'ReEmisi�n')]);

        except
            on E : Exception do begin
                DescError := e.Message;
                raise;
            End;
        end;

        Result := True;
    finally
        if Result then Result := RBIListado.Execute;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: RBIListadoExecute
  Author:    lgisuk
  Date Created: 20/01/2006
  Description: Ejecuto el informe
  Parameters: Sender: TObject;var Cancelled: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmRptEnvioXMLInfracciones.RBIListadoExecute(Sender: TObject; var Cancelled: Boolean);
resourcestring
    TITULO_INFORME_INFRACCIONES_NO_FACTURADAS   = 'Reporte de Generaci�n de Archivos XML de Infracciones No Facturadas';
    TITULO_INFORME_INFRACCIONES_FACTURADAS      = 'Reporte de Generaci�n de Archivos XML de Infracciones Facturadas';
Const
    STR_JUST_ONE_DISK = 'Totales en el disco:';
begin
    //Asigno nombre al reporte
    rbiListado.Caption := Caption;

    //Si es un solo disco
    if spObtenerDatosReporteInfraccionesTransitosAEnviarMOP.RecordCount = 1 then begin
        //Oculto los toteles
        pplblTotales.Visible := False;
        ppdbcalcDiscos.Visible := False;
        //Informo que se creo un solo disco
        pplblDiscos.Caption := STR_JUST_ONE_DISK;
        {
        if Facturados = 0 then
            ppLtitulo.Caption := TITULO_INFORME_INFRACCIONES_NO_FACTURADAS
        else
            ppLtitulo.Caption := TITULO_INFORME_INFRACCIONES_FACTURADAS;
        }
    end;
end;

end.
