unit FrmParametros;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Db, DBTables, UtilProc, UtilDB, ExtCtrls,
  CheckLst, ComCtrls, ADODB, Buttons, DPSControls, PeaProcs;

type
  TFormParametros = class(TForm)
    Parametros: TADOTable;
    PageControl: TPageControl;
    Interfaces: TTabSheet;
    txtDirArchivosInterfaces: TEdit;
    Label1: TLabel;
    btnDirArchivosImagenes: TSpeedButton;
    btnCancelar: TDPSButton;
    btnAceptar: TDPSButton;
    btnAplicar: TDPSButton;
    procedure btnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnAceptarClick(Sender: TObject);
	procedure btnAplicarClick(Sender: TObject);
	procedure btnDirArchivosImagenesClick(Sender: TObject);
	procedure ActualizarBotones(Sender: TObject);
  private
	{ Private declarations }
  public
	{ Public declarations }
	Function Inicializar: boolean;
  end;

var
  FormParametros: TFormParametros;

implementation
{$R *.DFM}

function TFormParametros.Inicializar: boolean;
begin
	CenterForm(Self);
	PageControl.TabIndex := 0;
	if not OpenTables([Parametros]) then
		Result := False
	else begin
		txtDirArchivosInterfaces.Text := Trim(Parametros.FieldByName('DirArchivosInterfaces').AsString);
        btnAplicar.Enabled := False;
		Result := True;
	end;
end;

procedure TFormParametros.btnCancelarClick(Sender: TObject);
begin
	Close;
end;

procedure TFormParametros.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Action := caFree;
end;

procedure TFormParametros.btnAceptarClick(Sender: TObject);
begin
	btnAplicarClick(btnAplicar);
	Close;
end;

procedure TFormParametros.btnAplicarClick(Sender: TObject);
begin
	with Parametros do begin
		if Eof then Append else Edit;
		FieldByName('Id').AsInteger := 1;
		FieldByName('DirArchivosInterfaces').AsString := Trim(txtDirArchivosInterfaces.text);
		Post;
	end;
    btnAplicar.Enabled := False;
end;

procedure TFormParametros.btnDirArchivosImagenesClick(Sender: TObject);
var
    DirArchivosInterfaces: AnsiString;
begin
    DirArchivosInterfaces := Trim(BrowseForFolder(Self));
    if not (DirArchivosInterfaces = '') then txtDirArchivosInterfaces.Text := DirArchivosInterfaces;
end;

procedure TFormParametros.ActualizarBotones(Sender: TObject);
begin
	BtnAplicar.Enabled := True;
end;

end.
