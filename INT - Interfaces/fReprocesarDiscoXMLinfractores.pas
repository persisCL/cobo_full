{
    Revision : 4
    Author: pdominguez
    Date: 21/10/2010
    Description: SS 930 - Reproceso disco XML Infracciones
        - Se a�adi� el objeto:
            spActualizarReprocesoXMLInfracciones: TADOStoredProc
        - Se modificaron las funciones:
            btnProcesarClick,
            GrabarArchivo,
            EliminarResiduos
}
unit fReprocesarDiscoXMLinfractores;

interface

uses
    // Base de Datos
  DMConnection, UtilDB,
    // Interfases
  ComunesInterfaces,
  // XML
  MOPInfBind,
  // Otros
  ConstParametrosGenerales,UtilProc, PeaTypes, PeaProcs, strUtils, Util,
  // Generales
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, ExtCtrls, StdCtrls, ListBoxEx, DBListEx, ComCtrls,
  FrmRptEnvioInfraccionesXML;

type
  TfrmReprocesarDiscoXMLInfractores = class(TForm)
    btnProcesar: TButton;
    btnSalir: TButton;
    Bevel1: TBevel;
    dblDiscos: TDBListEx;
    Label1: TLabel;
    Label2: TLabel;
    lblNumeroDisco: TLabel;
    lblIndiceReproceso: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    lblArchivos: TLabel;
    lblInfracciones: TLabel;
    Label3: TLabel;
    lblFecha: TLabel;
    spObtenerEnviosXMLInfracciones: TADOStoredProc;
    dsObtenerEnviosXMLInfracciones: TDataSource;
    spObtenerInfraccionesAEnviarMOP: TADOStoredProc;
    spObtenerInfraccionesTransitosAEnviarMOP: TADOStoredProc;
    spActualizarDetalleInfraccionesEnviadas: TADOStoredProc;
    spBorrarReprocesoParcialXMLInfracciones: TADOStoredProc;
    spValidarImagenDeReferenciaInfraccion: TADOStoredProc;
    pnlAvance: TPanel;
    labelAvance: TLabel;
    pbProgreso: TProgressBar;
    Bevel2: TBevel;
    Label4: TLabel;
    lblInfARepro: TLabel;
    spActualizarReprocesoXMLInfracciones: TADOStoredProc;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dblDiscosClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure btnProcesarClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    { Private declarations }
    FDirectorioDestino: AnsiString;
    FImagePath: AnsiString;
    FSameFolder: Boolean;
    FProcesando: boolean;
    FCancelar: boolean;
    FCodigoOperacion: Integer;
    FprimeraOperacion: Integer;
    FCorrDisco, FCorrArchivo, FIndiceReproceso: Integer;
    FstrErrores: TStringList;
    function GrabarArchivo(const XMLContainer:IXMLDenunciosType;
      var Error: AnsiString; var CancelUsuario: Boolean): Boolean;
    function ActualizarLogOperacion( const XMLContainer: IXMLDenunciosType; FileName: AnsiString; var DescError : Ansistring) : boolean;
    function EliminarResiduos( CorrDisco: Integer; IndiceReproceso: Integer): Boolean;
    procedure MostrarReporteFinalizacion(DescriError: AnsiString);
    procedure ClearDataLabels;

  public
    { Public declarations }
    function Inicializar : Boolean;
  end;

var
  frmReprocesarDiscoXMLInfractores: TfrmReprocesarDiscoXMLInfractores;
  ProcesarFacturados : Integer;

implementation

{$R *.dfm}
function  TfrmReprocesarDiscoXMLInfractores.Inicializar: Boolean;
resourcestring
	MSG_INIT_ERROR                      = 'Error al Inicializar';
    MSG_NO_DISK                         = 'No hay discos a reprocesar';
    MSG_ERROR_PARAM_GENERAL             = 'Error cargando par�metro general.';
    MSG_CARGANDO_PARAM_GENERAL          = 'No se pudo obtener el par�metro general %s.';
	MSG_INVALID_DESTINATION_DIRECTORY   = 'El directorio de env�os no existe';
	MSG_INVALID_IMAGES_PATH             = 'El directorio de im�genes de tr�nsitos es inv�lido';
    
begin
    Result := False;
    ClearDataLabels;

    //Obtiene los parametros generales que necesita...
    if not ObtenerParametroGeneral(DMConnections.BaseCAC, DIR_IMAGENES_TRANSITOS, FImagePath) then begin
        MsgBoxErr(MSG_ERROR_PARAM_GENERAL, format(MSG_CARGANDO_PARAM_GENERAL, [DIR_IMAGENES_TRANSITOS]), Caption, MB_ICONERROR);
        Exit;
    end;
    if not ObtenerParametroGeneral(DMConnections.BaseCAC, DIR_DESTINO_XML_INFRACCIONES, FDirectorioDestino) then begin
        MsgBoxErr(MSG_ERROR_PARAM_GENERAL, format(MSG_CARGANDO_PARAM_GENERAL, [DIR_DESTINO_XML_INFRACCIONES]), Caption, MB_ICONERROR);
        Exit;
    end;

    //valida que sean directorios validos...
	if (TRIM(FImagePath) = '') OR (not DirectoryExists( FImagePath )) then begin
		MsgBox(MSG_INVALID_IMAGES_PATH, Caption, MB_ICONERROR);
		Exit;
	end;
	if not DirectoryExists( FDirectorioDestino ) then begin
		MsgBox(MSG_INVALID_DESTINATION_DIRECTORY, Caption, MB_ICONERROR);
		Exit;
	end;

    // intenta abrir el sp de discos...
    try
        spObtenerEnviosXMLInfracciones.Open;
    except
        on e:exception do begin
            MsgBoxErr(MSG_INIT_ERROR, e.Message, Caption, mb_ICONSTOP);
            exit;
        end;
    end;

    if spObtenerEnviosXMLInfracciones.IsEmpty then begin
        spObtenerEnviosXMLInfracciones.Close;
        MsgBox(MSG_NO_DISK, Caption, MB_ICONINFORMATION);
        Exit;
    end;

    FProcesando := False;
    dblDiscos.SelectedIndex := 0;
    dblDiscosClick(self);
    FSameFolder := False;
    FStrErrores := TStringList.Create;

    FCorrDisco := 0;
    FCorrArchivo := 0;
    FIndiceReproceso := 0;
    Result := True;
end;

procedure TfrmReprocesarDiscoXMLInfractores.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TfrmReprocesarDiscoXMLInfractores.dblDiscosClick(Sender: TObject);
begin

    if (not spObtenerEnviosXMLInfracciones.Active) or (spObtenerEnviosXMLInfracciones.IsEmpty) then begin
        ClearDataLabels;
        btnProcesar.Enabled := False;
        Exit;
    end;

    lblNumeroDisco.caption := FormatFloat('0000',spObtenerEnviosXMLInfracciones.FieldByName('CorrelativoDisco').asInteger);

    lblIndiceReproceso.caption := iif( spObtenerEnviosXMLInfracciones.FieldByName('MaxReproceso').asInteger > 0, Format('Ultimo Reproceso Nro. %d',[spObtenerEnviosXMLInfracciones.FieldByName('MaxReproceso').asInteger]), 'Sin Reprocesos');

    lblFecha.Caption        := spObtenerEnviosXMLInfracciones.FieldByName('Fecha').asString;
    lblArchivos.caption     := spObtenerEnviosXMLInfracciones.FieldByName('Cant_Archivos').asString;
    lblInfracciones.caption := spObtenerEnviosXMLInfracciones.FieldByName('Cant_Infracciones').asString;
    lblInfARepro.Caption    := spObtenerEnviosXMLInfracciones.FieldByName('Cant_A_Reenviar').asString;

    if spObtenerEnviosXMLInfracciones.FieldByName('Cant_A_Reenviar').asInteger = 0 then btnProcesar.Enabled := False
    else btnProcesar.Enabled := True;


end;

procedure TfrmReprocesarDiscoXMLInfractores.btnSalirClick(Sender: TObject);
begin
    if FProcesando then FCancelar := True else  Close;
end;

{******************************** Function Header ******************************
Function Name: btnProcesarClick
Author :
Date Created :
Description :
Parameters : Sender: TObject
Return Value : None

Revision 1:
    Author : ggomez
    Date : 09/06/2006
    Description : Agregu� que aunque la infracci�n no tenga imagen v�lida,
        tambi�n vaya a la pr�xima infracci�n.

Revision 2:
    Author : ggomez
    Date : 15/06/2006
    Description : Agregu� un try para cada SP (spObtenerInfraccionesAEnviarMOP,
        spObtenerTransacciones) para determinar mejor que es lo que produce un
        error.

Revision 3:
    Author : ggomez
    Date : 16/06/2006
    Description :
        - Cambi� el nombre del TADOStoredProc que obtiene los transitos de las
        infracciones.
        - Agregu� el pasaje de los par�metros @CodigoInfraccionInicial y
        @CodigoInfraccionFinal, al SP spObtenerInfraccionesTransitosAEnviarMOP.

Revision : 4
    Author: pdominguez
    Date: 21/10/2010
    Description: SS 930 - Reproceso disco XML Infracciones
        - Se comenta todo el c�digo que registraba informaci�n en el Log de
        Operaciones.
        - Se a�ade la gesti�n de totales para informar al final del proceso.
        - Se a�ade la actualizaci�n del Indice de Reproceso mediante el objeto
        spActualizarReprocesoXMLInfracciones: TADOStoredProc.
*******************************************************************************}
procedure TfrmReprocesarDiscoXMLInfractores.btnProcesarClick(Sender: TObject);
resourcestring
    MSG_ERROR_OPEN_DATASOURCE_INFRACCIONES  = 'No se pueden obtener los datos de las infracciones.';
    MSG_ERROR_OPEN_DATASOURCE_TRANSITOS     = 'No se pueden obtener los datos de los tr�nsitos asociados a las infracciones';
    MSG_SUCCEDED                    = 'El proceso ha finalizado con �xito.' + CRLF + CRLF + 'Se reproces� el Disco Nro. %d con %d Archivos y %d Infracciones.'; // SS_930_20101021
    MSG_CANCELED                    = 'El proceso ha sido Cancelado.';
    MSG_CANCELED_WITH_GARBAGE       = 'No se pudo eliminar el reproceso parcial. ' + CRLF +
                                      'Esto puede ocasionar problemas al recibir las respuestas.';
    MSG_NOTHING_TO_PROCESS          = 'Las infracciones del disco ya han tenido respuesta o fueron anuladas.'+CrLf+'No hay infracciones para procesar';
    MSG_ERROR_SAVING                = 'No se puede grabar el archivo';
    STR_INFRACTION_WITHOUT_IMAGE    = 'Infraccion N� %d: No se puede obtener la imagen. La Infraccion no se procesa.';
    STR_ERROR_IN_TRANSACTION        = 'Infraccion N� %d: Error al procesar los tr�nsitos. La Infracci�n no se procesa.';
    MSG_ERROR_INFRACCIONES_XML      = 'No se pudo insertar la infracci�n en el archivo XML.';
    MSG_ERROR_CANT_VALIDATE_REF_TRX = 'No se puede validar la Imagen (Tr�nsito) de Referencia de la Infraccion.';
    STR_ERROR_IMAGEN_REF_ANULADA    = 'Infraccion N� %d : El tr�nsito al que corresponde la imagen de referencia est� anulado.';
    MSG_ERROR_EN_PROCESO            = 'Se produjo un Error inesperado en el proceso. Error: %s'; // SS_930_20101021
const
   BTN_SALIR = '&Salir';
   BTN_CANCEL = '&Cancelar';
var
    XMLContainer: IXMLDenunciosType;
    CodInf, CantInfracciones, TotalInfracciones, TotalArchivos: integer; // SS_930_20101021
    DescriError: AnsiString;
    UltimoLogConError: Integer;
begin
    FCorrDisco := spObtenerEnviosXMLInfracciones.FieldByName('CorrelativoDisco').asInteger;
    FIndiceReproceso := spObtenerEnviosXMLInfracciones.FieldByName('MaxReproceso').asInteger + 1;
    CantInfracciones := 0;
    Totalinfracciones := 0; // SS_930_20101021
    TotalArchivos := 0; // SS_930_20101021
    pbProgreso.Max := spObtenerEnviosXMLInfracciones.FieldByName('cant_Infracciones').asInteger;
    FStrErrores.Clear;
    UltimoLogConError   := 0;

    XMLContainer := NewDenuncios;
    XMLContainer.Clear;
    try
        // Abrir el SP que me trae las infracciones...
        spObtenerInfraccionesAEnviarMOP.Close;
        spObtenerInfraccionesAEnviarMOP.Parameters.Refresh;
        spObtenerInfraccionesAEnviarMOP.Parameters.ParamByName('@FechaDesde').value := NULL;
        spObtenerInfraccionesAEnviarMOP.Parameters.ParamByName('@FechaHasta').value := NULL;
        spObtenerInfraccionesAEnviarMOP.Parameters.ParamByName('@CodigoInfraccion').value := NULL;
        spObtenerInfraccionesAEnviarMOP.Parameters.ParamByName('@Disco').value := FCorrDisco;
        spObtenerInfraccionesAEnviarMOP.CommandTimeout := 500;
        spObtenerInfraccionesAEnviarMOP.Open;
    except
        on e: Exception do begin
            MsgBoxErr(MSG_ERROR_OPEN_DATASOURCE_INFRACCIONES, e.Message, Caption, MB_ICONSTOP);
            Exit;
        end;
    end;

    try
        // Abrir el SP que me trae las transacciones...
        spObtenerInfraccionesTransitosAEnviarMOP.Close;
        spObtenerInfraccionesTransitosAEnviarMOP.Parameters.Refresh;
        spObtenerInfraccionesTransitosAEnviarMOP.Parameters.ParamByName('@CodigoInfraccionInicial').Value   := Null;
        spObtenerInfraccionesTransitosAEnviarMOP.Parameters.ParamByName('@CodigoInfraccionFinal').Value     := Null;
        spObtenerInfraccionesTransitosAEnviarMOP.Parameters.ParamByName('@Disco').Value                     := FCorrDisco;
        spObtenerInfraccionesTransitosAEnviarMOP.CommandTimeout := 500;
        spObtenerInfraccionesTransitosAEnviarMOP.Open;

    except
        on e: Exception do begin
            MsgBoxErr(MSG_ERROR_OPEN_DATASOURCE_TRANSITOS, e.Message, Caption, MB_ICONSTOP);
            Exit;
        end;
    end;

    // Inicializo variables
    dblDiscos.Enabled   := False;
    btnProcesar.Enabled := False;
    FCancelar           := False;
    btnSalir.Caption    := BTN_CANCEL;
    FProcesando         := True;

    //registro la operacion en el log...
    FCorrArchivo := spObtenerInfraccionesAEnviarMOP.FieldByName('CorrelativoArchivo').asInteger;

    { // SS_930_20101020
    if (not spObtenerInfraccionesAEnviarMOP.Eof) and (not FCancelar) then begin
        if not RegistrarOperacionEnLogInterface(DMConnections.BaseCAC, RO_MOD_INTERFAZ_SALIENTE_XML_INFRACCIONES,
          NowBase(DMCOnnections.BaseCAC), '', UsuarioSistema, '', FCorrDisco, FCorrArchivo, FIndiceReproceso,
          FCodigoOperacion, DescriError) then begin
            exit;
        end;
    end;
    FprimeraOperacion := FCodigoOperacion;
    } // Fin Rev. SS_930_20101020

    try  // con un finally me aseguro que pase lo que pase, los Sp se cierran...
        while (not spObtenerInfraccionesAEnviarMOP.Eof) and (not FCancelar) do begin
            CodInf := spObtenerInfraccionesAEnviarMOP.FieldByName('CodigoInfraccion').asInteger;
            Application.ProcessMessages;

            // Verifico que el transito de referencia este activo, si no es asi, continuo con
            //  la siguiente infraccion... por ahora no salgo con error, ni registro nada...
            //  el mismo SP de validacion, se encargar de pasar la infraccion
            //  a estado IMAGEN DE REF ANULADA si corresponde.

            { // SS_930_20101020
            try
                with spValidarImagenDeReferenciaInfraccion do begin
                    Close;
                    Parameters.ParamByName('@Infraccion').Value := CodInf;
                    ExecProc;
                end;
            except
                on e: Exception do begin
                    // Si Fall� salgo con error...
                    MsgBoxErr(MSG_ERROR_CANT_VALIDATE_REF_TRX, e.Message, Caption, MB_ICONERROR);
                    Exit;
                end;
            end; // except


            if spValidarImagenDeReferenciaInfraccion.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin

                // Agrego la infraccion al log de errores para mostrarla al final...
            	AgregarErrorInterfase(DMConnections.BaseCAC, FCodigoOperacion,
                                       Format(STR_ERROR_IMAGEN_REF_ANULADA,[CodInf]));

                if (UltimoLogConError <> FCodigoOperacion) then begin
                    // si todavia no agregue este FCodigoOperacion
                    FstrErrores.Add(IntToStr(FCodigoOperacion));
                    UltimoLogConError := FCodigoOperacion;
                end;

// Agregado en Revision 1:
                pbProgreso.StepIt;
                // Ir a la siguiente infracci�n
                spObtenerInfraccionesAEnviarMOP.Next;

            end else begin // if spValidarImagenDeReferenciaInfraccion.
            } // Fin Rev. SS_930_20101020
            begin
                // guardo la infraccion en el XML
                if InfraccionToXML( spObtenerInfraccionesAEnviarMOP, FImagePath,
                        XMLContainer, CantInfracciones, DescriError) then begin

                    // Agregar las transacciones
                    spObtenerInfraccionesTransitosAEnviarMOP.Filtered := False;
                    spObtenerInfraccionesTransitosAEnviarMOP.Filter := '';
                    spObtenerInfraccionesTransitosAEnviarMOP.Filter := Format('CodigoInfraccion = %d', [CodInf]);
                    spObtenerInfraccionesTransitosAEnviarMOP.Filtered := True;

                    // Guardo las transacciones en un archivo temporal
                    if not TransaccionesToXML(spObtenerInfraccionesTransitosAEnviarMOP, XMLContainer, CantInfracciones, DescriError) then begin
                        // Si fallo al carga de transacciones elimino la infraccion...
                        MsgBoxErr(Format(STR_ERROR_IN_TRANSACTION, [XMLContainer.Infraccion[CantInfracciones].NumInfraccion]),
                            DescriError, Caption, MB_ICONERROR);
//                        MostrarReporteFinalizacion(DescriError); // SS_930_20101021
                        Exit;
                    end;
                    inc(CantInfracciones);
                end else begin
                    // Ocurrio una excepci�n, mostramos el error y nos vamos
                    MsgBoxErr(MSG_ERROR_INFRACCIONES_XML, DescriError, Caption, MB_ICONERROR);
//                    MostrarReporteFinalizacion(DescriError); // SS_930_20101021
                    Exit;
                end; // else if InfraccionToXML(

                pbProgreso.StepIt;
                // Ir a la siguiente infracci�n
                spObtenerInfraccionesAEnviarMOP.Next;
                //  si es la infraccion nro. 50 o debo cambiar de archivo...
                if ((spObtenerInfraccionesAEnviarMOP.EOF)
                            OR (spObtenerInfraccionesAEnviarMOP.FieldByName('CorrelativoArchivo').AsInteger <> FCorrArchivo))
                        and ( XMLContainer.Count > 0 ) then begin

                    if GrabarArchivo(XMLContainer, DescriError, FCancelar) then begin
                        // Grab� el archivo, entonces reinicio para un nuevo archivo
                        XMLContainer.Clear;
                        Inc(TotalArchivos); // SS_930_20101021
                        TotalInfracciones := TotalInfracciones + CantInfracciones; // SS_930_20101021
                        CantInfracciones := 0;
                        if (not spObtenerInfraccionesAEnviarMOP.Eof) and (not FCancelar) then begin
                            FCorrArchivo := spObtenerInfraccionesAEnviarMOP.FieldByName('CorrelativoArchivo').asInteger;
                            { // SS_930_20101021
                            if not RegistrarOperacionEnLogInterface(DMConnections.BaseCAC,
                                    RO_MOD_INTERFAZ_SALIENTE_XML_INFRACCIONES,
                                    NowBase(DMCOnnections.BaseCAC), '', UsuarioSistema, '',
                                    FCorrDisco, FCorrArchivo, FIndiceReproceso,
                                    FCodigoOperacion, DescriError) then begin

                                MostrarReporteFinalizacion(DescriError);
                                Exit;
                            end; // if not RegistrarOperacionEnLogInterface(
                            } // Fin Rev. SS_930_20101021
                        end; // if (not spObtenerInfraccionesAEnviarMOP.Eof) and (not FCancelar)
                    end else begin // if GrabarArchivo(
                        // No pude grabar, muestro error y salgo
                        MsgBoxErr(MSG_ERROR_SAVING, DescriError, caption, MB_ICONERROR);
//                        MostrarReporteFinalizacion(DescriError); // SS_930_20101021
                        Exit;
                    end; // else if GrabarArchivo(
                end; // else si es la infraccion nro. 50 o debo cambiar de archivo...
            end; // else if....validar Imagen Ref.
         end; //while

        if not FCancelar then
            try // SS_930_20101021
                with spActualizarReprocesoXMLInfracciones, Parameters do begin
                    if Active then Close;
                    Parameters.Refresh;
                    ParamByName('@CorrelativoDisco').Value := FCorrDisco;
                    ParamByName('@IndiceReproceso').Value  := FIndiceReproceso;
                    ExecProc;
                end;
                // si no cancel� el usuario, entonces el proceso fue exitoso
                MsgBox(
                    Format(MSG_SUCCEDED,[FCorrDisco, TotalArchivos, TotalInfracciones]),
                    Caption,
                    MB_ICONINFORMATION);
                spObtenerEnviosXMLInfracciones.Requery([]);
                dblDiscosClick(Self);
            except
                on e:Exception do begin
                    MsgBox(Format(MSG_ERROR_EN_PROCESO, [e.Message]), Caption, MB_ICONERROR);
                end;
            end // Fin Rev. SS 930_20101021
        else begin
            // el usuario cancel�...
            if EliminarResiduos(FCorrDisco, FIndiceReproceso) then  begin
                MsgBox(MSG_CANCELED, Caption, MB_ICONSTOP);
            end else begin
                MsgBoxErr(MSG_CANCELED, MSG_CANCELED_WITH_GARBAGE, Caption, MB_ICONSTOP);
            end;
        end;
//        MostrarReporteFinalizacion(DescriError); // SS_930_20101021
    finally
        // me aseguro de cerrar los recordsets...
        spObtenerInfraccionesAEnviarMOP.Close;
        spObtenerInfraccionesTransitosAEnviarMOP.Close;
        // y de dejar los botones como deben estar...
        Cursor := crDefault;
        btnProcesar.Enabled := True;
        btnSalir.Caption := BTN_SALIR;
        pbProgreso.Position := pbProgreso.Min;
        FProcesando := False;
        dblDiscos.Enabled := True;
        FCancelar := False;
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: RegistrarOperacion
  Author:    ndonadio
  Date Created: 05/07/2005
  Description: Registra la operacion en el log de operaciones y en el detalle de
                infracciones enviadas.
  Parameters: const XMLContainer: IXMLDenunciosType; FileName:AnsiString; CorrDisco, CorrArchivo: Integer; IndiceReproceso: byte; DescError : Ansistring
  Return Value: boolean
-----------------------------------------------------------------------------}
function TfrmReprocesarDiscoXMLInfractores.ActualizarLogOperacion(const XMLContainer: IXMLDenunciosType;
  FileName: AnsiString; var DescError: Ansistring) : boolean;
resourcestring
	MSG_COULD_NOT_REGISTER_OPERATION     = 'No se pudo registrar la operaci�n';
    MSG_COULD_NOT_REGISTER_INFRACTION_LOG = 'No se pudo registrar la lista de Infracciones Enviadas';
    MSG_ERROR = 'Error';
var
    i: integer;
begin
    Result := false;
    // Registro las Infracciones en el Log...
    DMConnections.BaseCAC.BeginTrans;
    try
        i := 0;
        While i < XMLContainer.Count do begin
            spActualizarDetalleInfraccionesEnviadas.Parameters.ParamByName('@CodigoInfraccion').Value := XMLContainer[i].NumInfraccion;
            spActualizarDetalleInfraccionesEnviadas.Parameters.ParamByName('@CodigoOPeracionInterface').Value := FCodigoOperacion;
            spActualizarDetalleInfraccionesEnviadas.ExecProc;
            // Si el SP retorna que no pudo terminar el proceso,
            // levanto una excepcion para hacer el rollbak y salir
            if spActualizarDetalleInfraccionesEnviadas.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then
                raise Exception.Create(MSG_COULD_NOT_REGISTER_INFRACTION_LOG);
            inc(i);
        end;

        // Debo actualizar que la operaci�n termino bien.
        if not ActualizarLogOperacionesInterfaseAlFinal(DMConnections.BaseCAC,
          FCodigoOperacion, '', FCorrDisco, FCorrArchivo, FIndiceReproceso, FileName, DescError) then begin
            exit;
        end;
        DMConnections.BaseCAC.CommitTrans;
        Result := true;
    except
        on e:exception do begin
            //si fall�, hago el rollback y le paso a Error el mensaje de la excepcion
            DMConnections.BaseCAC.RollbackTrans;
            DescError := e.Message;
        end;
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: GrabarArchivo
  Author:    ndonadio
  Date Created: 05/07/2005
  Description: Verifica y crea la carptea, Guarda el archivo
                y registra la operacion en el log
  Parameters: const XMLContainer:IXMLDenunciosType; pCorrDisco, pCorrFile: Integer;
              pIndiceReproceso: byte; var Error: AnsiString; CancelUsuario: Boolean
  Return Value: Boolean

  Revision : 4
    Author: pdominguez
    Date: 21/10/2010
    Description: SS 930 - Reproceso disco XML Infracciones
        - Se comenta todo el c�digo que registraba informaci�n en el Log de
        Operaciones.
-----------------------------------------------------------------------------}
function TfrmReprocesarDiscoXMLInfractores.GrabarArchivo(const XMLContainer:IXMLDenunciosType;
  var Error: AnsiString; var CancelUsuario: Boolean): Boolean;
resourcestring
    ERROR_COULD_NOT_CREATE_FILE         = 'No se puede crear el archivo de salida: %s';
    ERROR_FOLDER_EXISTS                 = 'La Carpeta ya existe. El proceso ha sido cancelado por el usuario.';
    ERROR_FILE_ALREADY_EXISTS           = 'Archivo "%s" ya existe. El proceso ha sido Cancelado.';
    FOLDER_ALREADY_EXISTS               = 'La Carpeta %s ya existe.'+CRLF+'Desea continuar?';
    MSG_FOLDER_NOT_CREATED              = 'No se ha creado la carpeta';
    MSG_COULD_NOT_REGISTER_OPERATION    = 'No se pudo registrar la operaci�n';
    MSG_ERROR_CREAR_DIRECTORIO          = 'Error creando el directorio %s';
    MSG_ERROR_RENOMBRANDO_ARCHIVO       = 'El archivo temporal %s no se ha podido mover a %s';
var
    FullPath, FileName, TempFileName: AnsiString;
begin
    TempFileName := '';
    TempFileName := FDirectorioDestino + TempFile;
    Result := False;
    CancelUsuario := false;
    //Guardo el archivo temporal...
    if not Guardar(XMLContainer, TempFileName) then begin
        Error := Format(ERROR_COULD_NOT_CREATE_FILE, [TempFileName]);
        Exit;
    end;
    try // Si no existe la creamos
        FullPath := GoodDir(NombreCarpetaXML(FDirectorioDestino, FCorrDisco, FIndiceReproceso));
        FileName := NombreArchivoXML(FullPath, FCorrDisco, FCorrArchivo, NowBase(DMConnections.BaseCAC));
        if not DirectoryExists(FullPath) then begin
            if not forceDirectories(FullPath) then begin
                Error := format(MSG_ERROR_CREAR_DIRECTORIO, [FullPath]);
                exit;
            end;
        end else begin
            // Si existe y es posible usar el mismo directorio, preguntamos si se desea continuar,
            // caso contrario nos vamos
            if (not FSameFolder) and (MsgBox(Format(FOLDER_ALREADY_EXISTS, [FullPath]), Caption, MB_ICONQUESTION+MB_YESNO) <> mrYES ) then begin
                Error := ERROR_FOLDER_EXISTS;
                CancelUsuario := true;
                exit;
            end;
        end;

        FSameFolder := True;

        // Obtenemos el nombre del archivo final...
        if FileExists(FileName) then begin
            Error := ERROR_FILE_ALREADY_EXISTS;
            exit;
        end;

        // Renombro al archivo temporal
        if not renameFile(TempFileName, FileName) then begin
            Error := format(MSG_ERROR_RENOMBRANDO_ARCHIVO, [TempFileName, FileName]);
            exit;
        end;

        // Genero el DTD
        GenerarDTD(FullPath);

        // Registro la operacion en el log de interfaces
        Error := '';
        { // SS_930_20101021
        if not ActualizarLogOperacion(XMLContainer, ExtractFileName(FileName), Error) then begin
            // Como no se pudo crear la operaci�n borramos el archivo, si era el �nico borramos el directorio tambi�n
            deleteFile(FileName);
            if (DirectoryExists(FullPath)) and (VerificarTamanioUnidad(FullPath) = 0) then RemoveDir(FullPath);
            exit;
        end;
        } // Fin Rev. SS_930_20101021
        Result := True;
    finally
        //pase lo qoe pase, me deshago del ptemporal...
        DeleteFile(TempFileName);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ClearDataLabels
  Author:    ndonadio
  Date Created: 05/07/2005
  Description:  Borro el contenido de todos los labels que empiezan con
                determinado prefijo...
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmReprocesarDiscoXMLInfractores.ClearDataLabels;
const
    LBL_PREFIX = 'lbl';
var
    i: integer;
begin
    for i := 0 to ControlCount-1 do
    begin
        if LeftStr(Controls[i].Name,3) = LBL_PREFIX then TLabel(Controls[i]).Caption := '';
    end;
end;

procedure TfrmReprocesarDiscoXMLInfractores.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
    if FProcesando then begin
        CanClose := False;
        FCancelar := True ;
    end;
end;

{
Revision : 4
    Author: pdominguez
    Date: 21/10/2010
    Description: SS 930 - Reproceso disco XML Infracciones
        - Se comenta todo el c�digo que eliminaba la informaci�n registrada en el Log de
        Operaciones.
}
function TfrmReprocesarDiscoXMLInfractores.EliminarResiduos(CorrDisco: integer; IndiceReproceso: integer): boolean;
var
    Carpeta: AnsiString;
    StrPath : AnsiString;
begin
    Result := False;
    { // SS_930_20101021
    try
        spBorrarReprocesoParcialXMLInfracciones.Parameters.ParamByName('@CorrelativoDisco').Value := CorrDisco;
        spBorrarReprocesoParcialXMLInfracciones.Parameters.ParamByName('@IndiceReproceso').Value := IndiceReproceso;
        spBorrarReprocesoParcialXMLInfracciones.Prepared := True;
        spBorrarReprocesoParcialXMLInfracciones.ExecProc;
        Result := (spBorrarReprocesoParcialXMLInfracciones.Parameters.ParamByName('@RETURN_VALUE').Value = 0 );
    except
        Exit;
    end;

    if not Result then Exit;
    } // Fin Rev. SS_930_20101021
    
    Carpeta := NombreCarpetaXML(FDirectorioDestino, CorrDisco, IndiceReproceso);
    StrPath := GoodDir(Carpeta)+'*.*'; // SS_930_20101021
    Del(StrPath); // borro todos los archivos en la carpeta...
    Result := RemoveDir(Carpeta);
end;


{******************************** Function Header ******************************
Function Name: MostrarReporteFinalizacion
Author : ndonadio
Date Created : 11/07/2005
Description : Invoca al Reporte de Finalizacion del Proceso.
Parameters : None
Return Value : None
*******************************************************************************}
procedure TfrmReprocesarDiscoXMLInfractores.MostrarReporteFinalizacion(DescriError: AnsiString);
var
    f: TfrmRptEnvioXMLInfracciones;
begin
    // determino si el disco es de facturados o no facturados
    ProcesarFacturados := QueryGetValueInt (DMConnections.BaseCAC, Format (
                                            'SELECT TOP 1 ' +
	                                        '       CASE 	WHEN SUBSTRING(NombreArchivo, 29,1) = ''1'' THEN 0 ' +
		                                    '               WHEN SUBSTRING(NombreArchivo, 29,1) = ''2'' THEN 1 ' +
		                                    '               ELSE NULL ' +
	                                        '       END ' +
                                            'FROM ' +
	                                        '   DetalleDiscoInfracciones D (NOLOCK) ' +
                                            '   INNER JOIN LogOperacionesInterfases L (NOLOCK) ' +
		                                    '       ON L.CodigoOperacionInterfase =  D.CodigoOperacionInterfase ' +
		                                    '       AND L.FechaHoraFinalizacion IS NOT NULL ' +
                                            'WHERE ' +
	                                        '   CorrelativoDisco = %d',
                                            [FCorrDisco]));

    Application.CreateForm(TfrmRptEnvioXMLInfracciones, f);
    f.Inicializar(FprimeraOperacion, DescriError, FstrErrores, ProcesarFacturados);

end;


end.
