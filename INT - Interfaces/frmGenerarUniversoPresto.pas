{********************************** File Header ********************************
File Name   : frmRecepcionUniversoPresto.pas
Author      : rcastro
Date Created: 23/12/2004
Language    : ES-AR
Description : Modulo de la interfaz Presto - Recepci�n del Universo
*******************************************************************************}
{Revision History
*******************************************************************************}
{Author: flamas
 23/12/2004: Valida el nombre del archivo
*******************************************************************************}
{Author: ndonadio
 05/04/2005: Consulta al usuario si quiere continuar o no, presentandole un resumen de las cantidades a procesar y de las cantidades existentes.
*******************************************************************************}
{Author: lgisuk
 16/03/2005: Cree filtro para buscar el archivo.
 16/03/2005: Agregue cabeceras y comentarios.
 21/06/2005: Obtengo la cantidad de errores contemplados que se produjeron al procesar el archivo.
 21/06/2005: Ordene las rutinas por su orden de ejecuci�n.
 01/07/2005: Actualizo el log al finalizar.
 06/07/2005: Cree rutina para obtener el ultimo convenio, y modifique comportamiento de la barra de progreso.
 14/07/2005: Cree mensaje de ayuda contextual, Defini tama�o de ventana minimo, Permito maximizar y ajustar los forms. Separe units del form de las generales.
 26/07/2005: Quite la llamada al parametro general 'CodigodeComercio' en rutina obtener filtro porque no se utilizaba.
 26/07/2005: Corregi Tabulaci�n. se perdio al migrar a la nueva version de Delphi.
 26/07/2005: Obtengo los Parametros Generales que se utilizaran en el formulario al inicializar y verifico que los valores obtenidos sean validos.



Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

  
Firma       : SS_1385_NDR_20150922
Descripcion : Cambiar la transaccionalidad de delphi por Execute('TRANSACCIONALIDAD SQL') para evitar tener rollback oculto

*******************************************************************************}
unit frmGenerarUniversoPresto;

interface

uses
  //Recepcion del universo de Presto
  DMConnection,
  Util,
  UtilProc,
  ConstParametrosGenerales,
  PeaProcs,
  UtilDB,
  PeaTypes,
  ComunesInterfaces,
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, DB, ADODB, Grids, DBGrids, StdCtrls, Buttons, ComCtrls,
  ListBoxEx, DBListEx,  DPSControls, ppDB, ppTxPipe, StrUtils,
  ppParameter, ppModule, raCodMod, ppCtrls, ppBands, ppReport, ppPrnabl,
  ppClass, ppStrtch, ppSubRpt, ppCache, ppComm, ppRelatv, ppProd, UtilRB,
  RStrings, RBSetup, HashingMd5;

const
	TAMANIO_BLOQUE_A_IMPORTAR = 250;

type
    TRegistroUniverso = record
       	FechaInicio: TDateTime;
        RUTSubscriptor: string;
        NumeroTarjeta: string;
        NumeroConvenio: string;
        DiaVencimiento: integer;
        DiaFacturacion: integer;
        Error: String;
  	end;

  TFUniversoPresto = class (TForm)
	Bevel1: TBevel;
	btnAbrirArchivo: TSpeedButton;
	Label2: TLabel;
	edOrigen: TEdit;
	rbiListado: TRBInterface;
  ppReporteGenUniversoPresto: TppReport;
	ppDetailBand1: TppDetailBand;
	ppParameterList1: TppParameterList;
	ppErrores: TppTextPipeline;
	ppTextPipeline1ppField1: TppField;
	btnProcesar: TButton;
	btnCancelar: TButton;
	btnSalir: TButton;
	OpenDialog: TOpenDialog;
	ppDBText2: TppDBText;
	ppHeaderBand1: TppHeaderBand;
	ppImage1: TppImage;
	ppLabel4: TppLabel;
	ppLabel7: TppLabel;
	ppLine1: TppLine;
	spRegistrarOperacion: TADOStoredProc;
	spBorrarUniversoAnterior: TADOStoredProc;
	spObtenerUltimoCodigoConvenio: TADOStoredProc;
	spProcesarExistentesUniversoPresto: TADOStoredProc;
	spProcesarNoExistentesEnUniverso: TADOStoredProc;
  pplblUsuario: TppLabel;
  pnlAvance: TPanel;
  lblprocesoGeneral: TLabel;
  pbProgreso: TProgressBar;
  lblReferencia: TLabel;
  pnlAyuda: TPanel;
  ImgAyuda: TImage;
	procedure FormClose (Sender: TObject; var Action: TCloseAction);
	procedure btnProcesarClick (Sender: TObject);
	procedure btnCancelarClick (Sender: TObject);
	procedure btnSalirClick (Sender: TObject);
	procedure btnAbrirArchivoClick (Sender: TObject);
	procedure FormCloseQuery (Sender: TObject; var CanClose: Boolean);
	procedure rbiListadoExecute (Sender: TObject; var Cancelled: Boolean);
  procedure ImgAyudaClick(Sender: TObject);
  procedure ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
  private
	{ Private declarations }
	FCodigoOperacion : integer;
	FCancelar : boolean;
	FArchivoErrores : String;
    slUniverso, slErrores : TStringList;
    FPresto_CodigodeComercio : AnsiString;
 	FPresto_Directorio_Origen_Universo : AnsiString;
    FPresto_DirectorioUniversoProcesado : AnsiString;
    FMandatosMayoraArchivo : String;
    FArchivoMayoraMandato  : String;
    FDiferenciaPermitidaArchVSBase   : integer;
    FDiferenciaPermitidaBaseVSArch   : integer;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
	function  ParsearLinea (Linea: string; var regUniverso : TRegistroUniverso): boolean;
	function  AnalizarUniverso : boolean;
  function  ConfirmaCantidades(Lineas: integer): Boolean;
	function  RegistrarOperacion : Boolean;
	function  BorrarUniversoAnterior : boolean;
	function  PasarUniversoATabla : boolean;
	function  ObtenerUltimoConvenio : Longint;
	function  ProcesarExistentesEnUniverso : boolean;
	function  ProcesarNoExistentesEnUniverso : boolean;
	procedure ProcesarErrores (sLineasSQL : string; nLineaAEjecutar : integer);
  public
	function  Inicializar (txtCaption: ANSIString; MDIChild:Boolean): Boolean;
	{ Public declarations }
  end;

var
  FUniversoPresto: TFUniversoPresto;

Const
	RO_MOD_INTERFAZ_ENTRANTE_UNIVERSO_PRESTO		= 31;

implementation

{$R *.dfm}

{******************************** Function Header ******************************
Function Name: Inicializar
Author       : rcastro
Date Created : 23/12/2004
Description  : Inicializacion de este Formulario
Parameters   : txtCaption: ANSIString; MDIChild:Boolean
Return Value : Boolean
*******************************************************************************}
function TFUniversoPresto.Inicializar (txtCaption: ANSIString; MDIChild:Boolean): Boolean;

    {******************************** Function Header ******************************
    Function Name: VerificarParametrosGenerales
    Author : lgisuk
    Date Created : 26/07/2005
    Description : Obtengo los Parametros Generales que se utilizaran en el formulario
                  al inicializar y verifico que los valores obtenidos sean validos.
    Parameters : None
    Return Value : boolean
    *******************************************************************************}
    Function VerificarParametrosGenerales : boolean;
    resourcestring
        MSG_ERROR_CHECK_GENERAL_PARAMETER = 'Error al verificar parametro general';
        MSG_ERROR                         = 'Error';
        STR_NOT_EXISTS_GENERAL_PARAMETER  = 'No existe parametro general: ';
        STR_EMPTY_GENERAL_PARAMETER       = 'Parametro general vacio: ';                                         //Se verifica que los codigos no esten vacios, si no tienen razon de ser
        STR_DIRECTORY_NOT_EXISTS          = 'No existe el directorio indicado en parametros generales: ' + CRLF; //Los directorios de destino y errores en procesos de envio son criticos
    const
        PRESTO_CODIGODECOMERCIO            = 'Presto_CodigodeComercio';
        PRESTO_DIRECTORIO_ORIGEN_UNIVERSO  = 'Presto_Directorio_Origen_Universo';
        PRESTO_DIRECTORIOUNIVERSOPROCESADO = 'Presto_DirectorioUniversoProcesado';
    var
        DescError : AnsiString;
    begin
        Result    := True;
        DescError := '';
        try
            try

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, PRESTO_CODIGODECOMERCIO , FPresto_CodigodeComercio) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + PRESTO_CODIGODECOMERCIO;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                if not (FPresto_CodigodeComercio <> '') then begin
                   DescError := STR_EMPTY_GENERAL_PARAMETER + PRESTO_CODIGODECOMERCIO;
                   Result := False;
                   Exit;
                end;

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, PRESTO_DIRECTORIO_ORIGEN_UNIVERSO , FPresto_Directorio_Origen_Universo) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + PRESTO_DIRECTORIO_ORIGEN_UNIVERSO;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                FPresto_Directorio_Origen_Universo := GoodDir(FPresto_Directorio_Origen_Universo);
                if  not DirectoryExists(FPresto_Directorio_Origen_Universo) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FPresto_Directorio_Origen_Universo;
                    Result := False;
                    Exit;
                end;

                //Obtengo el parametro general
                ObtenerParametroGeneral(DMConnections.BaseCAC,PRESTO_DIRECTORIOUNIVERSOPROCESADO,FPresto_DirectorioUniversoProcesado);

                if not ObtenerParametroGeneral(DMConnections.BaseCAC, 'CANT_MAX_DIF_PAT_PRESTO_BASEVSARCH',FDiferenciaPermitidaBaseVSArch) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + 'CANT_MAX_DIF_PAT_PRESTO_BASEVSARCH';
                    Result := False;
                    Exit;
                end;

                if not ObtenerParametroGeneral(DMConnections.BaseCAC, 'CANT_MAX_DIF_PAT_PRESTO_ARCHVSBASE',FDiferenciaPermitidaArchVSBase) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + 'CANT_MAX_DIF_PAT_PRESTO_ARCHVSBASE';
                    Result := False;
                    Exit;
                end;

                if not ObtenerParametroGeneral(DMConnections.BaseCAC, 'MSG_DIF_ARCHIVO_MAYOR_A_MANDATOS',FArchivoMayoraMandato) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + 'MSG_DIF_ARCHIVO_MAYOR_A_MANDATOS';
                    Result := False;
                    Exit;
                end;

                if not ObtenerParametroGeneral(DMConnections.BaseCAC, 'MSG_DIF_MANDATOS_MAYOR_A_ARCHIVO',FMandatosMayoraArchivo) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + 'MSG_DIF_MANDATOS_MAYOR_A_ARCHIVO';
                    Result := False;
                    Exit;
                end;

            except
                on e: Exception do begin
                    Result := False;
                    MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, e.Message, MSG_ERROR, MB_ICONWARNING);
                end;
            end;

        finally
            //si no paso la verificacion de parametros generales
            if (Result = False) then begin
                //informo la situaci�n
                MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, DescError, MSG_ERROR, MB_ICONWARNING);
            end;
        end;
    end;

resourcestring
	MSG_INIT_ERROR = 'Error al Inicializar';
	MSG_ERROR = 'Error';
begin
	Result := False;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;


  //Elijo el modo en que si visualizara la ventana
	if not MDIChild then begin
		FormStyle := fsNormal;
		Visible := False;
	end;

  //Centro el form
	CenterForm (Self);
	try
		DMConnections.BaseCAC.Connected := True;
		Result := DMConnections.BaseCAC.Connected and
                            VerificarParametrosGenerales;
	except
		on e: Exception do begin
			MsgBoxErr (MSG_INIT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
		end;
	end;

  //Resolver ac� lo que necesita este form para inicializar correctamente
  Caption := AnsiReplaceStr (txtCaption, '&', '');
	btnCancelar.Enabled := False;
	btnProcesar.Enabled := False;
	pnlAvance.Visible := False;
	lblReferencia.Caption := '';

end;

{******************************** Function Header ******************************
Function Name: ImgAyudaClick
Author : lgisuk
Date Created : 14/07/2005
Description :  Mensaje de Ayuda
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFUniversoPresto.ImgAyudaClick(Sender: TObject);
Resourcestring
    MSG_UNIVERSO        = ' ' + CRLF +
                          'El archivo de Universo' + CRLF +
                          'es enviado por PRESTO al ESTABLECIMIENTO' + CRLF +
                          'segun la periodicidad convenida y contiene' + CRLF +
                          'el detalle de todos los mandantes que tienen' + CRLF +
                          'medio de pago PAT de PRESTO' + CRLF +
                          ' ' + CRLF +
                          'Nombre del Archivo: UNI1DDMM1D' + CRLF +
                          ' ' + CRLF +
                          'Se utiliza para actualizar los datos de' + CRLF +
                          'los mandantes en el sistema del ESTABLECIMIENTO.' + CRLF +
                          ' ';
begin
    //si esta procesando sale
    if PnlAvance.visible = true then exit;
    //Muestro el mensaje
    MsgBoxBalloon(MSG_UNIVERSO , Caption, MB_ICONQUESTION, IMGAYUDA);
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaMouseMove
Author : lgisuk
Date Created : 14/07/2005
Description :
Parameters : Sender: TObject;Shift: TShiftState; X, Y: Integer
Return Value : None
*******************************************************************************}
procedure TFUniversoPresto.ImgAyudaMouseMove(Sender: TObject;Shift: TShiftState; X, Y: Integer);
begin
    if PnlAvance.visible = true then ImgAyuda.Cursor := crdefault else ImgAyuda.Cursor := crHandPoint;
end;

{******************************** Function Header ******************************
Function Name: btnAbrirArchivoClick
Author       : rcastro
Date Created : 23/12/2004
Description  : Selecciona y analiza el nombre del archivo asociado al Universo
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFUniversoPresto.btnAbrirArchivoClick (Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: ObtenerFiltro
      Author:    lgisuk
      Date Created: 16/03/2005
      Description: Obtengo el Filtro
      Parameters: var Filtro: AnsiString
      Return Value: Boolean
    -----------------------------------------------------------------------------}
    Function ObtenerFiltro(var Filtro: AnsiString): Boolean;
    Const
        FILE_TITLE = 'Universo PRESTO|';
        FILE_NAME = 'UNI1';
    begin
        //Creo el Filtro
        Filtro := FILE_TITLE + FILE_NAME + '*';
        Result:= True;
    end;

    {-----------------------------------------------------------------------------
      Function Name: ValidarNombreArchivo
      Author:    flamas
      Date Created: 23/12/2004
      Description: Valida el nombre del archivo
      Parameters: sFileName : string
      Return Value: boolean
    -----------------------------------------------------------------------------}
    function ValidarNombreArchivoPresto(sFileName, sTipoArchivo : string) : boolean;
    resourcestring
      	MSG_INVALIDA_FILE_NAME = 'El nombre del archivo %s es inv�lido';
        MSG_ERROR  = 'Error';
    begin
        result :=	(Copy(ExtractFileName(sFileName), 1, 4) = sTipoArchivo) and (Copy(ExtractFileName(sFileName), 9, 2) = '1D');
      	if not result then MsgBox (Format (MSG_INVALIDA_FILE_NAME, [ExtractFileName (sFileName)]), MSG_ERROR, MB_ICONERROR);
    end;

resourcestring
	MSG_ERROR = 'Error';
	MSG_FILE_NOT_EXISTS = 'El archivo %s no existe';
	MSG_FILE_ALREADY_PROCESED = 'El archivo %s ya fue procesado' + CRLF + 'Desea procesarlo nuevamente ?';
	FILE_NAME = 'UNI1';
var
  Filtro: string;
begin

    //obtengo el filtro a aplicar a este tipo de archivos
    ObtenerFiltro(Filtro);

    Opendialog.InitialDir := FPresto_Directorio_Origen_Universo;
  	OpenDialog.FileName := '';
    OpenDialog.Filter := Filtro; //solo veo este tipo de archivos

  	if opendialog.execute then begin

    		edOrigen.text:=UpperCase (opendialog.filename) ;

        if not ValidarNombreArchivoPresto(edOrigen.text, FILE_NAME) then Exit;

        if not FileExists (edOrigen.text) then begin
            MsgBox (Format (MSG_FILE_NOT_EXISTS, [edOrigen.text]), MSG_ERROR, MB_ICONERROR);
            Exit;
        end;

        if VerificarArchivoProcesado (DMConnections.BaseCAC, edOrigen.text) then begin
        if MsgBox(Format(MSG_FILE_ALREADY_PROCESED, [edOrigen.text]),self.Caption, MB_YESNO + MB_ICONQUESTION) <> IDYES then
                Exit;
        end;

        btnProcesar.Enabled := True;
  	end;
end;

{-----------------------------------------------------------------------------
  Function Name: ConfirmaCantidades
  Author:    ndonadio
  Date Created: 05/04/2005
  Description: Consulta al usuario si quiere continuar o no,
                presentandole un resumen de las cantidades a procesar
                y de las cantidades existentes.
  Parameters: Lineas: integer
  Return Value: Boolean

 Revision 1
 Author : Fsandi
 Date : 24-05-2007
 Description : Se modifica las consultas SQL en delphi, se las cambia para que llamen funciones de la Base de Datos

 Revision 2
 Author : Fsandi
 Date : 12-06-2007
 Description : Ahora se ponen mensajes de advertencia o error si la diferencia es mucha entre el total mandatos y el archivo

 Revision 3
 Author : Fsandi
 Date : 26-06-2007
 Description : Por solicitud del cliente, se permite continuar aunque aparezca el mensaje de error.
-----------------------------------------------------------------------------}
function TFUniversoPresto.ConfirmaCantidades(Lineas: integer): Boolean;
resourcestring
    MSG_CONFIRMATION = 'Actualmente existen %d Mandatos Confirmados '+crlf+'y %d Mandatos Pendientes, que hacen'+crlf+
                       'un total de %d Mandatos PAT-Presto Existentes.'+crlf+crlf+
                       'El Archivo seleccionado contiene %d Mandatos a procesar.';
    MSG_KEEP_PROCESSING = 'Desea continuar con el procesamiento del archivo?';
    MSG_TITLE = 'Confirme Recepci�n de Universo PAT - PRESTO';
var
    MandatosPendientes: integer;
    MandatosConfirmados: integer;
    MandatosExistentes: integer;
    CodigoEmisorTarjetaPresto : Integer;
    MensajeCompleto : String;
begin
     CodigoEmisorTarjetaPresto := QueryGetValueInt(DMConnections.BaseCAC,'select DBO.CONST_CODIGO_EMISOR_TARJETA_PRESTO()');
    //Obtengo la cantida de mandatos Existentes/Pendientes y Confirmados
    MandatosExistentes  :=  QueryGetValueInt(DMConnections.BaseCAC, format('select dbo.ObtenerMandatos(%d,%d)',[4,CodigoEmisorTarjetaPresto]));
    MandatosPendientes  :=  QueryGetValueInt(DMConnections.BaseCAC, format('select dbo.ObtenerMandatos(%d,%d)',[5,CodigoEmisorTarjetaPresto]));
    MandatosConfirmados :=  QueryGetValueInt(DMConnections.BaseCAC, format('select dbo.ObtenerMandatos(%d,%d)',[6,CodigoEmisorTarjetaPresto]));
    //Armo el mensaje
    MensajeCompleto := Format(MSG_CONFIRMATION,[MandatosConfirmados,MandatosPendientes,MandatosExistentes, Lineas]);

    // se comparan las cantidades en el archivo y la tabla que no se pasen de ciertas diferencias determinadas por parametro
    // si se superan los par�metros, se solicita confirmaci�n o se cancela el proceso.

    if MandatosExistentes > Lineas then begin
        if MandatosExistentes - Lineas > FDiferenciaPermitidaBaseVSArch then begin
            MensajeCompleto := MensajeCompleto + CRLF + CRLF + FMandatosMayoraArchivo;
        end else begin
            MensajeCompleto := MensajeCompleto + CRLF + CRLF + MSG_KEEP_PROCESSING;
        end;
        Result := ( MsgBox(MensajeCompleto,MSG_TITLE, MB_ICONQUESTION + MB_YESNO) = mrYes );
    end;

    if Lineas > MandatosExistentes then begin
        if Lineas - MandatosExistentes > FDiferenciaPermitidaArchVSBase then begin
            MensajeCompleto := MensajeCompleto +crlf+crlf+ FArchivoMayoraMandato;
        end else begin
            MensajeCompleto := MensajeCompleto + CRLF + CRLF + MSG_KEEP_PROCESSING ;
        end;
        Result := ( MsgBox(MensajeCompleto,MSG_TITLE, MB_ICONQUESTION + MB_YESNO) = mrYes );
    end;
    
    if Lineas = MandatosExistentes then begin
        MensajeCompleto := MensajeCompleto + CRLF + CRLF + MSG_KEEP_PROCESSING;
        Result := ( MsgBox(MensajeCompleto,MSG_TITLE, MB_ICONQUESTION + MB_YESNO) = mrYes );
    end;

end;



{******************************** Function Header ******************************
Function Name: ParsearLinea
Author       : rcastro
Date Created : 28/12/2004
Description  : Parsea una l�nea del archivo de interfase,
				indicando tipo de error, si lo hay
Parameters   : Linea: string; var regUniverso : TRegistroUniverso
Return Value : boolean
*******************************************************************************}
function TFUniversoPresto.ParsearLinea (Linea: string; var regUniverso : TRegistroUniverso): boolean;
begin
	Result := False;
	try

		with regUniverso do begin

 			Error := '';

			if (Trim (Linea) = '') then begin
				Error := '* No hay datos';
				Exit
			end;

      FPresto_CodigodeComercio := PADL(FPresto_CodigodeComercio, 6, '0');

			if trim (Copy (Linea, 1, 6)) <> FPresto_CodigodeComercio then Error := '* El n�mero de EPS es inv�lido ';

			RUTSubscriptor := trim (Copy (Linea, 7, 8));

      try
          StrToInt64(RUTSubscriptor);
      except
				  on exception do Error := '* El RUT es inv�lido';
      end;

      try
          FechaInicio	:= EncodeDate(	StrToInt('20' + Copy(Linea,35,2)), StrToInt(Copy(Linea,37,2)), StrToInt(Copy(Linea,39,2)));
      except
          on exception do Error := '* La fecha de inicio es inv�lida';
      end;

			RUTSubscriptor := RUTSubscriptor + Linea[15];
			if not (Linea[15] in ['0'..'9','K']) then Error := '* El d�gito verificador del RUT es inv�lido ';
			NumeroTarjeta  := trim (Copy (Linea, 16, 19));
			NumeroConvenio := Trim (Copy (Linea, 41, 17));

			// Verifica si es un valor correcto para un d�a
			try
                DiaVencimiento:= StrToInt(Trim(Copy (Linea, 58, 2)));
				if not (DiaVencimiento in [1..31]) then Error := '* El d�a de vencimiento es inv�lido ';
			except
				on exception do Error := '* El contenido del d�a de vencimiento es inv�lido ';
			end;

			// Verifica si es un valor correcto para un d�a
			try
                DiaFacturacion:= StrToInt(Trim(Copy (Linea, 60, 2)));
				        if not (DiaFacturacion in [1..31]) then Error := '* El d�a de facturaci�n es inv�lido ';
			except
				  on exception do Error := '* El contenido del d�a de facturaci�n es inv�lido ';
			end;

		end;

	finally

		Result := regUniverso.Error = ''

	end;
end;

{******************************** Function Header ******************************
Function Name: AnalizarUniverso
Author       : rcastro
Date Created : 28/12/2004
Description  : Analiza el contenido del archivo, l�nea a l�nea
Parameters   : None
Return Value : boolean
*******************************************************************************}
function TFUniversoPresto.AnalizarUniverso : boolean;
resourcestring
	MSG_UNIVERSE_HAS_ERRORS = 'El archivo de Universo contiene %d l�neas inv�lidas y %d l�neas v�lidas';
	MSG_ANALYSIS_TEXT       = 'Analizando contenido de linea %d';
	MSG_INVALID_RECORDS 	= 'Al archivo de Universo no tienen ning�n registro v�lido';
var
	l: LongInt;
	regUniverso : TRegistroUniverso;
begin
	// saco el header y footer del Universo
	slUniverso.Delete (0);
	slUniverso.Delete (slUniverso.Count-1);

	Screen.Cursor := crHourGlass;
	pbProgreso.Max := slUniverso.Count;
	pnlAvance.Visible := True;
	pbProgreso.Position := 0;

	l := 0;
	while (l <= slUniverso.Count-1) and (not FCancelar) do begin
		pbProgreso.StepIt;
		lblReferencia.Caption := Format (MSG_ANALYSIS_TEXT,[l]);

		Application.ProcessMessages;

		if (not FCancelar) and (not ParsearLinea (slUniverso.Strings[l], regUniverso)) then begin
			slErrores.Add ('Registro ' + IntToStr (l) + ': ' + regUniverso.Error);
		end;

		inc (l)
	end;

	lblReferencia.Caption := '';
	pbProgreso.Position := 0;
	pnlAvance.Visible := False;
	Screen.Cursor := crDefault;

	result := False;
	if not FCancelar then begin
		if slErrores.Count = slUniverso.Count then begin
			MsgBox( MSG_INVALID_RECORDS, self.Caption, MB_ICONSTOP );
		end
		else begin
			if slErrores.Count <> 0 then slErrores.Insert(0, Format(MSG_UNIVERSE_HAS_ERRORS, [slErrores.Count-1, slUniverso.Count-slErrores.Count+1]));

			result := (slErrores.Count = 0) or
					  ((slErrores.Count <> 0) and
					   (MsgBox(Format(MSG_UNIVERSE_HAS_ERRORS + #10#13#10#13 + 'Desea continuar con el procesamiento ?',
										[slErrores.Count-1, slUniverso.Count-slErrores.Count+1]),self.Caption, MB_YESNO + MB_ICONQUESTION) = IDYES))
		end
	end;
end;

{******************************** Function Header ******************************
Function Name: RegistrarOperacion
Author       : rcastro
Date Created : 28/12/2004
Description  : Registra la operaci�n asociada al proceso realizado
Parameters   : None
Return Value : integer
*******************************************************************************}
function TFUniversoPresto.RegistrarOperacion : Boolean;
resourcestring
	MSG_PROCESS_FINISHED_SUCCESFULY  = 'Proceso finalizado exitosamente';
	MSG_PROCESS_FINISHED_WITH_ERROR  = 'Proceso terminado con Errores';
	MSG_OPERATION_CANT_BE_REGISTERED = 'No se pudo registrar la operaci�n';
var
	sDescrip,
	DescError : string;
begin
	sDescrip := iif (slErrores.Count = 0, MSG_PROCESS_FINISHED_SUCCESFULY, MSG_PROCESS_FINISHED_WITH_ERROR);
	result := RegistrarOperacionEnLogInterface(DMConnections.BaseCAC, RO_MOD_INTERFAZ_ENTRANTE_UNIVERSO_PRESTO, ExtractFileName(edOrigen.text), UsuarioSistema, sDescrip, True, False, NowBase(DMConnections.BaseCAC), 0, FCodigoOperacion, DescError);

	if not result then MsgBoxErr(MSG_OPERATION_CANT_BE_REGISTERED, DescError, 'Error', MB_ICONERROR);
end;

{******************************** Function Header ******************************
Function Name: BorrarUniversoAnterior
Author       : rcastro
Date Created : 30/12/2004
Description  : Elimina el ultimo universo que estaba cargado en la tabla
Parameters   : None
Return Value : boolean
*******************************************************************************}
function TFUniversoPresto.BorrarUniversoAnterior : boolean;
resourcestring
	MSG_DELETING_UNIVERSE_DATA = 'Eliminando datos del Universo';
	MSG_UNIVERSE_CANT_BE_DELETED 	= 'No se pudo eliminar el Universo anterior';
begin
	Screen.Cursor := crHourGlass;
	lblReferencia.Caption := MSG_DELETING_UNIVERSE_DATA;
	lblReferencia.Repaint;

	try
		spBorrarUniversoAnterior.ExecProc;
		result := True;
	except
		on e: Exception do result := False
	end;

	lblReferencia.Caption := '';
	lblReferencia.Repaint;
	Screen.Cursor := crDefault;

	if not result then MsgBox (MSG_UNIVERSE_CANT_BE_DELETED, 'Error', MB_ICONERROR)
end;

{******************************** Function Header ******************************
Function Name: PasarUniversoATabla
Author       : rcastro
Date Created : 28/12/2004
Description  : Transfiere el contenido del archivo de interfase a la BD
Parameters   : None
Return Value : boolean
*******************************************************************************}
function TFUniversoPresto.PasarUniversoATabla : boolean;
resourcestring
	MSG_PROCESSING_UNIVERSE_DATA = 'Generando Archivo de Universo - Cantidad de registros: %d';
	MSG_PARSING_ERROR			 = 'Error en l�nea: %s ' + CRLF + '%s';
var
	nNroLineaScript,
	nLineasScript,
	nNroLinea,
	nPrimeraLineaAEjecutar :integer;
	FRegistroUniverso : TRegistroUniverso;
	sLineasSQL : string;
begin
	Screen.Cursor := crHourglass;

	nLineasScript := slUniverso.Count;
	lblReferencia.Caption := Format (MSG_PROCESSING_UNIVERSE_DATA, [nLineasScript]);
	pbProgreso.Position := 0;
	pbProgreso.Max := slUniverso.Count;
	pnlAvance.Visible := True;

	nNroLineaScript := 0;
	sLineasSQL := '';

	while (nNroLineaScript < nLineasScript) and (not FCancelar) do begin
		// Realiza la importaci�n por Bloque
		nNroLinea := 0;
		sLineasSQL := '';
		nPrimeraLineaAEjecutar := nNroLineaScript;

		while (nNroLineaScript < nLineasScript) and (nNroLinea < TAMANIO_BLOQUE_A_IMPORTAR) do begin
			if ParsearLinea (slUniverso[nNroLineaScript], FRegistroUniverso) then begin
				with FRegistroUniverso do begin
					sLineasSQL := sLineasSQL + 'EXEC AGREGARUNIVERSOPRESTO ' +
												QuotedStr (FormatDateTime('dd-mm-yyyy', FechaInicio)) + ', ' +
												QuotedStr (RUTSubscriptor) + ', ' +
												QuotedStr (NumeroTarjeta) + ', ' +
												QuotedStr (NumeroConvenio) + ', ' +
												IntToStr (DiaVencimiento) + ', ' +
                                                IntToStr (DiaFacturacion) + #10;
					Inc (nNroLinea);
				end
			end else begin
				slErrores.Add (Trim (Format(MSG_PARSING_ERROR, [slUniverso[nNroLineaScript], FRegistroUniverso.Error])))
			end;

			Inc (nNroLineaScript);
		end;

		// Ejecuta los Inserts
    //DMConnections.BaseCAC.BeginTrans;                                       //SS_1385_NDR_20150922
    DMConnections.BaseCAC.Execute('BEGIN TRAN frmGenerarUniversoPresto');     //SS_1385_NDR_20150922
		try
			DMConnections.BaseCAC.Execute ('SET DATEFORMAT DMY'#10 + sLineasSQL);
      //DMConnections.BaseCAC.CommitTrans;                                              //SS_1385_NDR_20150922
      DMConnections.BaseCAC.Execute('COMMIT TRAN frmGenerarUniversoPresto');					//SS_1385_NDR_20150922
		except
			on e: exception do begin
        //DMConnections.BaseCAC.RollbackTrans;                                            //SS_1385_NDR_20150922
        DMConnections.BaseCAC.Execute('IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION frmGenerarUniversoPresto END');	//SS_1385_NDR_20150922
				ProcesarErrores (sLineasSQL, nPrimeraLineaAEjecutar);
			end;
		end;

		pbProgreso.Position := nNroLineaScript;
		Application.ProcessMessages;
	end;

	pbProgreso.Position := 0;
	pnlAvance.Visible := false;
	lblReferencia.Caption := '';
	Screen.Cursor := crDefault;

	result := not FCancelar
end;

{-----------------------------------------------------------------------------
  Function Name: ObtenerUltimoConvenio
  Author:    lgisuk
  Date Created: 06/07/2005
  Description: Obtiene el ultimo convenio
  Parameters: None
  Return Value: Longint
-----------------------------------------------------------------------------}
function TFUniversoPresto.ObtenerUltimoConvenio : Longint;
begin
	try
		spObtenerUltimoCodigoConvenio.Open;
		result := spObtenerUltimoCodigoConvenio.FieldByNAme( 'UltimoConvenio' ).AsInteger;
		spObtenerUltimoCodigoConvenio.Close;
	except
		on exception do result := 1000000
	end;
end;



{******************************** Function Header ******************************
Function Name: ProcesarExistentesUniversoPAC
Author       : rcastro
Date Created : 30/12/2004
Description  : Actualiza la informacion de los mandatos con la que viene en
               en el archivo y los confirma
Parameters   : None
Return Value : boolean
*******************************************************************************}
function TFUniversoPresto.ProcesarExistentesEnUniverso : boolean;
resourcestring
	MSG_UNIVERSE_PROCESS_ERROR = 'Error al procesar el Universo Presto';
	MSG_UNIVERSE_PROCESS_TEXT	= 'Procesando convenios existentes en Universo Presto';
var
	FSiguienteConvenio : integer;
	FDescError : string;
	PosError : Integer;
begin
	result := True;

	Screen.Cursor := crHourglass;

	PosError := slErrores.Count;
	lblReferencia.Caption := MSG_UNIVERSE_PROCESS_TEXT;
	pnlAvance.Visible := True;
	pbProgreso.Position := 0;
	pbProgreso.Max := ObtenerUltimoConvenio;

	FSiguienteConvenio := 0;
	while (FSiguienteConvenio <> - 1) and (not FCancelar) do begin
		Application.ProcessMessages;

		with spProcesarExistentesUniversoPresto, Parameters do begin
			ParamByName ('@CodigoConvenioInicial').Value := FSiguienteConvenio;
			try
				ExecProc;

				FSiguienteConvenio := ParamByName('@UltimoCodigoConvenio').Value;
				FDescError := Trim(VarToStr(ParamByName('@DescripcionError').Value));
				if FDescError <> '' then
                	slErrores.Add (FDescError);

				if FSiguienteConvenio <> -1 then Inc (FSiguienteConvenio);
				pbProgreso.Position := FSiguienteConvenio;
			except
				on e: Exception do begin
					result := false;
					slErrores.Insert (PosError, MSG_UNIVERSE_PROCESS_ERROR);
         	lblReferencia.Caption := '';
					Break;
				end;
			end;
		end;
	end;

	pbProgreso.Position := 0;
	pnlAvance.Visible := False;
	lblReferencia.Caption := '';
	Screen.Cursor := crDefault;
end;

{******************************** Function Header ******************************
Function Name: ProcesarNoExistentesUniversoPAC
Author       : rcastro
Date Created : 30/12/2004
Description  : Si estaban confirmados los mandatos los elimina
Parameters   : None
Return Value : boolean
*******************************************************************************}
function TFUniversoPresto.ProcesarNoExistentesEnUniverso : boolean;
resourcestring
  	MSG_UNIVERSE_PROCESS_ERROR 	= 'Error al procesar el Universo Presto';
  	MSG_UNIVERSE_PROCESS_TEXT 	= 'Procesando convenios no existentes en Universo Presto';
    MSG_ERROR                   = 'Error';
var
	FSiguienteConvenio : integer;
	FDescError : string;
	PosError : Integer;
begin
	result := True;

	Screen.Cursor := crHourglass;

	PosError := slErrores.Count;
	lblReferencia.Caption := MSG_UNIVERSE_PROCESS_TEXT;
	pnlAvance.Visible := True;
	pbProgreso.Position := 0;
	pbProgreso.Max := ObtenerUltimoConvenio;

	FSiguienteConvenio := 0;

	while (FSiguienteConvenio <> - 1) and (not FCancelar) do begin
		Application.ProcessMessages;

		with spProcesarNoExistentesEnUniverso, Parameters do begin
			ParamByName( '@CodigoConvenioInicial' ).Value := FSiguienteConvenio;
			try
				ExecProc;

				FSiguienteConvenio := ParamByName ('@UltimoCodigoConvenio').Value;
				FDescError := Trim(VarToStr (ParamByName ('@DescripcionError').Value));
				if FDescError <> '' then slErrores.Add (FDescError);

				if FSiguienteConvenio <> -1 then Inc (FSiguienteConvenio);
				pbProgreso.Position := FSiguienteConvenio;
			except
				on e: Exception do begin
					result := false;
					MsgBoxErr (MSG_UNIVERSE_PROCESS_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
					slErrores.Insert (PosError, MSG_UNIVERSE_PROCESS_ERROR);
         	lblReferencia.Caption := '';
					Break;
				end;
			end;
		end;
	end;

	lblReferencia.Caption := '';
	pbProgreso.Position := 0;
	pnlAvance.Visible := False;

	Screen.Cursor := crDefault;
end;

{******************************** Function Header ******************************
Function Name: ProcesarErrores
Author       : rcastro
Date Created : 03/01/2005
Description  : Identifica los registros con errores
Parameters   : sLineasSQL : string; nLineaAEjecutar : integer
Return Value : None
*******************************************************************************}
procedure TFUniversoPresto.ProcesarErrores (sLineasSQL : string; nLineaAEjecutar : integer);

    {******************************** Function Header ******************************
    Function Name: ClasificarError
    Author       : rcastro
    Date Created : 03/01/2005
    Description  : Clasifica el tipo de error en el registro
    Parameters   : regUniverso : TRegistroUniverso
    Return Value : None
    *******************************************************************************}
    procedure ClasificarError (regUniverso : TRegistroUniverso);
    var
        value : variant;
    resourcestring
        MSG_CONTRACT_NUMBER_INVALID = 'El convenio %s no existe en la Base de Datos';
        MSG_RUT_NUMBER_INVALID = 'El RUT %s no se encuentra en la Base de Datos';
    begin
        // Verifica si hay error en el numero de Convenio
        value := QueryGetValue (DMConnections.BaseCAC, 'SELECT dbo.ObtenerCodigoConvenio(''' + regUniverso.NumeroConvenio + ''')');
        if (value = '') then slErrores.Add (Format(MSG_CONTRACT_NUMBER_INVALID, [regUniverso.NumeroConvenio]));

        // Verifica si hay error en el RUT
        value := QueryGetValue (DMConnections.BaseCAC, 'SELECT dbo.ObtenerCodigoPersonaRUT(''' + regUniverso.RUTSubscriptor + ''')');
        if (value = '') then slErrores.Add (Format (MSG_RUT_NUMBER_INVALID, [regUniverso.RUTSubscriptor]));
    end;


var
	sExecSQL : string;
	nPos : integer;
	FRegistroUniverso : TRegistroUniverso;
begin
	nPos := Pos( #10, sLineasSQL );
	while ( nPos <> 0 ) do begin
		sExecSQL := Copy( sLineasSQL, 1, nPos );
		sLineasSQL := Copy( sLineasSQL, nPos + 1, length( sLineasSQL ) ); // Saltea el #10#13
		nPos := Pos( #10, sLineasSQL );

		// Ejecuta los Inserts
		with DMConnections, BaseCAC do begin
			try
				Execute(sExecSQL);
			except
				on exception do begin
					ParsearLinea (slUniverso[nLineaAEjecutar], FRegistroUniverso);
					ClasificarError (FRegistroUniverso);
				end;
			end;
		end;

		Inc( nLineaAEjecutar );
	end;
end;


{******************************** Function Header ******************************
Function Name: rbiListadoExecute
Author       : rcastro
Date Created : 28/12/2004
Description  : Genera el listado de errores
Parameters   : Sender: TObject; var Cancelled: Boolean
Return Value : None
*******************************************************************************}
procedure TFUniversoPresto.rbiListadoExecute (Sender: TObject;var Cancelled: Boolean);
begin
	try
		try
			Screen.Cursor := crHourGlass;
		finally
			Screen.Cursor := crDefault;
		end;
	except
		on E: Exception do begin
			Cancelled := True;
			MsgBoxErr (MSG_ERROR_DATOS_LISTADO, E.Message, Self.Caption, MB_ICONSTOP);
		end;
	end;
end;

{******************************** Function Header ******************************
Function Name: btnProcesarClick
Author       : rcastro
Date Created : 23/12/2004
Description  : Procesa el contenido del Universo
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFUniversoPresto.btnProcesarClick (Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: ObtenerCantidadErroresInterfaz
      Author:    lgisuk
      Date Created: 21/06/2005
      Description: Obtengo la cantidad de errores contemplados que se produjeron
                   al procesar el archivo
      Parameters: CodigoOperacion:string
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function ObtenerCantidadErroresInterfaz(CodigoOperacion:integer;var Cantidad:integer):boolean;
    resourcestring
        MSG_ERROR = 'No se pudo obtener la cantidad de errores de la interfaz';
    begin
        Cantidad := 0;
        try
            Cantidad := QueryGetValueInt(DMconnections.BaseCAC, 'SELECT dbo.ObtenerCantidadErroresInterfaz ('+inttostr(CodigoOperacion)+')');
            Result := true;
        except
           on e: Exception do begin
               MsgBoxErr(MSG_ERROR,e.Message, Self.caption, MB_ICONERROR);
               Result := False;
           end;
        end;
    end;

   {-----------------------------------------------------------------------------
      Function Name: ActualizarLog
      Author:    lgisuk
      Date Created: 01/07/2005
      Description: Actualizo el log al finalizar
      Parameters: CantidadErrores:integer
      Return Value: boolean
   -----------------------------------------------------------------------------}
   Function ActualizarLog(CantidadErrores:integer):boolean;
   Resourcestring
        MSG_ERROR = 'No se pudo Actualizar el log de operaciones';
   Const
        STR_ERRORES = 'Finalizo OK!, Errores Validaci�n: ';
   var
        DescError : string;
   begin
        try
            Result := ActualizarLogOperacionesInterfaseAlFinal
                            (DMConnections.BaseCAC,FCodigoOperacion,STR_ERRORES+ IntToStr(CantidadErrores),DescError);
        except
            on e: Exception do begin
               MsgBoxErr(MSG_ERROR,e.Message, Self.caption, MB_ICONERROR);
               Result := False;
            end;
        end;
   end;

resourcestring
	MSG_PROCESS_FINISHED_SUCCESFULY 	= 'El proceso finaliz� con �xito.';
	MSG_PROCESS_FINISHED_WITH_ERRORS 	= 'El proceso finaliz� con errores.';
	MSG_PROCESS_INCOMPLETE			 	= 'El proceso no se pudo completar';
	MSG_CANT_OPEN_FILE 					= 'No se puede abrir el archivo %s';
	MSG_FILE_IS_EMPTY		 			= 'El archivo seleccionado est� vac�o';
	MSG_HEADER_FOOTER_VALIDATION_FAILS	= 'La cabecera/pi� del archivo son inv�lidos';
	PRESTO_UNIVERSE_REFERENCE_TEXT		= 'Universo enviado por Presto - '; 
Const
    STR_OPEN_FILE               = 'Abrir Archivo...';
    STR_CONFIRM                 = 'Confirmar Cantidades...';
    STR_CHECK_FILE              = 'Control del Archivo...';
    STR_ANALYZING               = 'Analizando Mandatos...';
    STR_REGISTER_OPERATION      = 'Registrando Operacion en el Log de Operaciones...';
    STR_IMPORT                  = 'Importar Registros...';
    STR_PROCESS                 = 'Actualizando Mandatos...';
var
    TotalRegistros: integer;
    FVerReporteErrores: boolean;
    CantidadErrores : integer;
    RutaLogo: AnsiString;                                                                               //SS_1147_NDR_20140710
begin


  ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
  try                                                                                                 //SS_1147_NDR_20140710
    ppImage1.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
  except                                                                                              //SS_1147_NDR_20140710
    On E: Exception do begin                                                                          //SS_1147_NDR_20140710
      Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
      MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
      Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
    end;                                                                                              //SS_1147_NDR_20140710
  end;                                                                                                //SS_1147_NDR_20140710

    //Creo los stringlist
	slUniverso := TStringList.Create;
	slErrores := TStringList.Create;

	//Deshabilita los botones
	btnCancelar.Enabled := True;
	btnProcesar.Enabled := False;
	btnAbrirArchivo.Enabled := False;

	FCancelar := False;
    FVerReporteErrores := False;

	try
		try

            //Obtiene el archivo de universo
            lblReferencia.Caption := STR_OPEN_FILE;
      			slUniverso.text:= FileToString (edOrigen.text);

            //Verifica si el Archivo est� vac�o
            if slUniverso.Count = 0 then

            //Informo que el archivo esta vacio
    				slErrores.Add (Format (MSG_FILE_IS_EMPTY, [ExtractFileName (edOrigen.text)]))

			else begin

                //Obtengo la cantidad de registros
                TotalRegistros := slUniverso.Count;

                //Confirmo cantidades
                lblReferencia.Caption := STR_CONFIRM;
                if not ConfirmaCantidades(slUniverso.Count) then begin
                    Exit;
                end;

				        //Valida Header/Footer
                lblReferencia.Caption := STR_CHECK_FILE;
                FPresto_CodigodeComercio := PADL(FPresto_CodigodeComercio, 6, '0');
        				if not ValidarHeaderFooterPresto (slUniverso, edOrigen.Text, FPresto_CodigodeComercio, PRESTO_UNIVERSE_REFERENCE_TEXT) then begin
          					slErrores.Add (MSG_HEADER_FOOTER_VALIDATION_FAILS);
          					Exit;
        				end;

        				//Analiza el contenido del Universo
                lblReferencia.Caption := STR_ANALYZING;
         				if not AnalizarUniverso then begin
                    Exit;
                end;

                //Registra la operacion en el log de operaciones
                lblReferencia.Caption := STR_REGISTER_OPERATION;
                if not RegistrarOperacion then begin
                    Exit;
                end;

        				//Borra el Universo anterior
                lblReferencia.Caption := STR_IMPORT;
        				if not BorrarUniversoAnterior then begin
                    Exit;
                end;

        				//Genera el Universo en la BD
         				if not PasarUniversoATabla then begin
                    Exit;
                end;

        				//Actualiza Convenio en base a los convenios Existentes en el Universo Presto
                lblReferencia.Caption := STR_PROCESS;
        				if not ProcesarExistentesEnUniverso then begin
                    Exit;
                end;

        				//Actualiza Convenio en base a los convenios NO Existentes en el Universo Presto
        				if not ProcesarNoExistentesEnUniverso then begin
                    Exit;
                end;

                //Obtengo la cantidad de errores
                ObtenerCantidadErroresInterfaz(FCodigoOperacion, CantidadErrores);

                //Actualizo el log al Final
                ActualizarLog(CantidadErrores);

                //Muestro Cartel de Finalizaci�n
                FVerreporteErrores := MsgProcesoFinalizado(MSG_PROCESS_FINISHED_SUCCESFULY, MSG_PROCESS_FINISHED_WITH_ERRORS, Self.Caption, TotalRegistros,TotalRegistros-slErrores.Count,slErrores.Count);
			end
		except

			on e: Exception do begin
			  	MsgBoxErr (MSG_CANT_OPEN_FILE, e.Message, 'Error', MB_ICONERROR);
			end;

		end;
	finally

        //Si Finalizo con errores
		if (not FCancelar) and (slErrores.Count <> 0) then begin

            //Si hubo errores muestro un reporte con los errores
      			FArchivoErrores := ChangeFileExt (edOrigen.text, '.ERR');
      			slErrores.SaveToFile (FArchivoErrores);
         		ppErrores.FileName := FArchivoErrores;
            if FVerReporteErrores then begin
                rbiListado.Execute (True);
            end;

		end;

    //Si no cancelaron la operacion
    if not FCancelar then begin
         //Muevo el archivo Procesado a otro Directorio
         MoverArchivoProcesado(Caption,edOrigen.Text, FPresto_DirectorioUniversoProcesado);
    end;

    //Restablezco los botones
		btnCancelar.Enabled := False;
		btnProcesar.Enabled := False;
		btnAbrirArchivo.Enabled := True;

    //borro la referencia
    lblreferencia.Caption := '';

    //Elimino los stringlist
		FreeAndNil (slUniverso);
		FreeAndNil (slErrores);
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnCancelarClick
  Author:    lgisuk
  Date Created: 16/03/2005
  Description: Permite cancelar la operacion
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFUniversoPresto.btnCancelarClick (Sender: TObject);
resourcestring
	MSG_PROCESS_CANCELLED_BY_USER = 'Proceso cancelado por el usuario';
begin
	FCancelar := True;
  lblreferencia.Caption := '';
	MsgBox (MSG_PROCESS_CANCELLED_BY_USER, self.Caption, MB_ICONINFORMATION);
end;

{-----------------------------------------------------------------------------
  Function Name: FormCloseQuery
  Author:    lgisuk
  Date Created: 16/03/2005
  Description: Permite salir si no esta procesando
  Parameters: Sender: TObject;var CanClose: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFUniversoPresto.FormCloseQuery (Sender: TObject;var CanClose: Boolean);
begin
	CanClose := not btnCancelar.Enabled;
end;

{-----------------------------------------------------------------------------
  Function Name: btnSalirClick
  Author:    lgisuk
  Date Created: 16/03/2005
  Description: cierro el formualario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFUniversoPresto.btnSalirClick (Sender: TObject);
begin
	close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 16/03/2005
  Description: Lo libero de memoria
  Parameters: Sender: TObject;var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFUniversoPresto.FormClose (Sender: TObject;var Action: TCloseAction);
begin
   Action := caFree;
end;

end.




