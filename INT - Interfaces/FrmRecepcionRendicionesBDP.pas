{------------------------------------------------------------------------------
 File Name: FrmRecepcionRendicionesBDP.pas
 Author:    lgisuk
 Date Created: 12/07/2006
 Language: ES-AR
 Description: M�dulo de la interface Santander - Recepci�n de Rendiciones BDP

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

--------------------------------------------------------------------------------}
unit FrmRecepcionRendicionesBDP;

interface

uses
    //Recepcion de Rendiciones
  	DMConnection,                         //Coneccion a base de datos OP_CAC
    Util,                                 //Stringtofile,padl..
    UtilProc,                             //Mensajes
    ConstParametrosGenerales,             //Obtengo Valores de la Tabla Parametros Generales
    ComunesInterfaces,                    //Procedimientos y Funciones Comunes a todos los formularios
    Peatypes,                             //Constantes
    PeaProcs,                             //NowBase
    FrmRptErroresSintaxis,                //Reporte de Errores de sintaxis
    FrmRptRecepcionComprobantesBDP, //Reporte del proceso
    //General
  	Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  	Dialogs, StdCtrls, Buttons, DB, ADODB, ExtCtrls, Grids, DBGrids,
  	DPSControls, ListBoxEx, DBListEx, ComCtrls, StrUtils, UtilRB,
    ppCtrls, ppBands, ppPrnabl, ppClass, ppCache, ppProd, ppReport, ppDB,
    ppComm, ppRelatv, ppTxPipe, UtilDB, ppDBPipe, ppModule, raCodMod;

type

  //Estructura donde voy a almacenar cada Rendicion
  TRendicion = record
    NotaCobro : integer;
    Monto : INT64;
  	Fecha : TDateTime;
    CodigoServicio : Integer;
    MedioPago : Integer;
  end;

  TFRecepcionRendicionesBDP = class(TForm)
    OpenDialog: TOpenDialog;
    pnlOrigen: TPanel;
    btnAbrirArchivo: TSpeedButton;
    txtOrigen: TEdit;
    lblOrigen: TLabel;
    btnProcesar: TButton;
    btnSalir: TButton;
    Bevel: TBevel;
    lblMensaje: TLabel;
    pnlAvance: TPanel;
    lblProcesoGeneral: TLabel;
    pbProgreso: TProgressBar;
    pnlAyuda: TPanel;
    ImgAyuda: TImage;
    SPProcesarRendicionesBDP: TADOStoredProc;
    procedure btnAbrirArchivoClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure txtOrigenChange(Sender: TObject);
    procedure btnProcesarClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure ImgAyudaClick(Sender: TObject);
    procedure ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
  private
    { Private declarations }
    FCodigoOperacion : Integer;
    FErrorGrave : Boolean;      //Indica que se produjo un error
    FCancelar : Boolean;        //Indica si ha sido cancelado por el usuario
    FTotalRegistros : Integer;
    FBDP_Directorio_Rendiciones : AnsiString;
    FBDP_Directorio_Procesados : AnsiString;
    FBDP_Directorio_Errores : AnsiString;
    FMontoArchivo: int64;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
  	Function   Abrir : Boolean;
    Function   Control : Boolean;
    function   ConfirmaCantidades : Boolean;
    function   ParseLineToRecord(Linea : String; var Rendicion : TRendicion; var DescError : String) : Boolean;
    Function   AnalizarRendicionesTXT : Boolean;
    function   RegistrarOperacion : Boolean;
    Function   ActualizarRendiciones : Boolean;
    Function   GenerarReporteErroresSintaxis : Boolean;
    Function   GenerarReporteDeFinalizacion(CodigoOperacionInterfase : Integer) : Boolean;
  public
    { Public declarations }
  	Function  Inicializar(txtCaption : ANSIString) : Boolean;
  end;

var
  FRecepcionRendicionesBDP : TFRecepcionRendicionesBDP;
  FLista : TStringList;
  FErrores : TStringList;

Const
  RO_MOD_INTERFAZ_ENTRANTE_RENDICIONES_BDP = 74;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    lgisuk
  Date Created: 12/07/2006
  Description: M�dulo de inicializaci�n de este formulario
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFRecepcionRendicionesBDP.Inicializar(txtCaption : ANSIString) : Boolean;

    {******************************** Function Header ******************************
    Function Name: VerificarParametrosGenerales
    Author : lgisuk
    Date Created : 12/07/2006
    Description : Obtengo los Parametros Generales que se utilizaran en el formulario
                  al inicializar y verifico que los valores obtenidos sean validos.
    Parameters : None
    Return Value : boolean
    *******************************************************************************}
    Function VerificarParametrosGenerales : Boolean;
    resourcestring
        MSG_ERROR_CHECK_GENERAL_PARAMETER = 'Error al verificar parametro general';
        MSG_ERROR                         = 'Error';
        STR_NOT_EXISTS_GENERAL_PARAMETER  = 'No existe parametro general: ';
        STR_EMPTY_GENERAL_PARAMETER       = 'Parametro general vacio: ';                                         //Se verifica que los codigos no esten vacios, si no tienen razon de ser
        STR_DIRECTORY_NOT_EXISTS          = 'No existe el directorio indicado en parametros generales: ' + CRLF; //Los directorios de destino y errores en procesos de envio son criticos
    const
        BDP_DIRECTORIO_RENDICIONES        = 'Santander_Rendiciones_BDP';
        BDP_DIRECTORIO_ERRORES            = 'Santander_Directorio_Errores';
        BDP_DIRECTORIO_PROCESADOS         = 'STD_DirectorioRendicionesProcesadas';
    var
        DescError : AnsiString;
    begin
        Result    := True;
        DescError := '';
        try
            try

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, BDP_DIRECTORIO_RENDICIONES , FBDP_Directorio_Rendiciones) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + BDP_DIRECTORIO_RENDICIONES;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                FBDP_Directorio_Rendiciones := GoodDir(FBDP_Directorio_Rendiciones);
                if  not DirectoryExists(FBDP_Directorio_Rendiciones) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FBDP_Directorio_Rendiciones;
                    Result := False;
                    Exit;
                end;

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, BDP_DIRECTORIO_ERRORES , FBDP_Directorio_Errores) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + BDP_DIRECTORIO_ERRORES;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                FBDP_Directorio_Errores := GoodDir(FBDP_Directorio_Errores);
                if  not DirectoryExists(FBDP_Directorio_Errores) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FBDP_Directorio_Errores;
                    Result := False;
                    Exit;
                end;

                //Obtengo el parametro general
                ObtenerParametroGeneral(DMConnections.BaseCAC,BDP_DIRECTORIO_PROCESADOS, FBDP_Directorio_Procesados);

            except
                on E : Exception do begin
                    Result := False;
                    MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, E.Message, MSG_ERROR, MB_ICONWARNING);
                end;
            end;

        finally
            //si no paso la verificacion de parametros generales
            if (Result = False) then begin
                //informo la situaci�n
                MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, DescError, MSG_ERROR, MB_ICONWARNING);
            end;
        end;
    end;

resourcestring
    MSG_INIT_ERROR = 'Error al Inicializar';
    MSG_ERROR = 'Error';
begin
  	Result := False;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;
    
    //Centro el formulario
  	CenterForm(Self);
  	try
  		DMConnections.BaseCAC.Connected := True;
  		Result := DMConnections.BaseCAC.Connected and
                           VerificarParametrosGenerales;
  	except
        on E : Exception do begin
            MsgBoxErr(MSG_INIT_ERROR, E.Message, MSG_ERROR, MB_ICONERROR);
        end;
  	end;
    //Resolver ac� lo que necesita este form para inicializar correctamente
    Caption := AnsiReplaceStr(txtCaption, '&', '');  //titulo
  	btnProcesar.enabled := False; //Procesar
  	lblmensaje.Caption := '';     //limpio el indicador
    PnlAvance.visible := False;    //Oculto el Panel de Avance
    FCodigoOperacion := 0;         //inicializo codigo de operacion
end;

{-----------------------------------------------------------------------------
  Function Name: ImgAyudaClick
  Author:    lgisuk
  Date Created: 12/07/2006
  Description: Mensaje de Ayuda
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRecepcionRendicionesBDP.ImgAyudaClick(Sender: TObject);
ResourceString
     MSG_RENDICION      = ' ' + CRLF +
                          'El Archivo de Rendiciones' + CRLF +
                          'es enviado por SANTANDER al ESTABLECIMIENTO' + CRLF +
                          'y tiene por objeto dar a conocer al Establecimiento, ' + CRLF +
                          'los comprobantes que fueron pagados a traves de la Web.' + CRLF +
                          ' ' + CRLF +
                          'Nombre del Archivo: RENCNEBDPYYYYMMDD.TXT' + CRLF +
                          ' ' + CRLF +
                          'Se utiliza para ratificar los pagos recibidos ' + CRLF +
                          'y para registrar los pagos que no se recibieron ' + CRLF +
                          'a traves de la Web en el sistema del ESTABLECIMIENTO.' + CRLF +
                          ' ';
begin
    //si esta procesando sale
    if PnlAvance.visible = True then Exit;

    //Muestro el mensaje
    MsgBoxBalloon(MSG_RENDICION, Caption, MB_ICONQUESTION, IMGAYUDA);
end;

{-----------------------------------------------------------------------------
  Function Name: ImgAyudaMouseMove
  Author:    lgisuk
  Date Created: 12/07/2006
  Description: permito hacer click en la ayuda solo si no esta procesando
  Parameters: Sender: TObject;Shift: TShiftState; X, Y: Integer
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRecepcionRendicionesBDP.ImgAyudaMouseMove(Sender: TObject;Shift: TShiftState; X, Y: Integer);
begin
    if PnlAvance.Visible = True then ImgAyuda.Cursor := crDefault else ImgAyuda.Cursor := crHandPoint;
end;

{-----------------------------------------------------------------------------
  Function Name: BuscarArchivoBTNClick
  Author:    lgisuk
  Date Created: 12/07/2006
  Description: Busco el archivo a procesar
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRecepcionRendicionesBDP.btnAbrirArchivoClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: EsArchivoValido
      Author:    lgisuk
      Date Created: 12/07/2006
      Description: Verifica si es un Archivo Valido para Esta Pantalla
      Parameters:
      Return Value: None
    -----------------------------------------------------------------------------}
    Function EsArchivoValido(Nombre : AnsiString) : Boolean;
    begin
        Result:=   ExistePalabra(Nombre, 'RENCNEBDP') and
                        ExistePalabra(uppercase(Nombre), '.TXT');
    end;

resourcestring
    MSG_ERROR = 'No es un archivo de Rendiciones Valido! ';
begin
    OpenDialog.InitialDir := FBDP_Directorio_Rendiciones;
    if OpenDialog.execute then begin
        if not (EsArchivoValido (OpenDialog.Filename)) then begin
            //inform que no es un archivo valido
            MsgBox(MSG_ERROR , self.Caption, MB_OK + MB_ICONINFORMATION);
            txtOrigen.text := '';
        end
        else begin
            BTNAbrirArchivo.Enabled := False;
            txtOrigen.text := OpenDialog.FileName;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: EOrigenChange
  Author:    lgisuk
  Date Created: 12/07/2006
  Description: Controlo que abra un archivo valido
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRecepcionRendicionesBDP.txtOrigenChange(Sender: TObject);
begin
    //Controlo que abra un archivo valido
  	btnProcesar.Enabled := FileExists( txtOrigen.Text);
end;

{-----------------------------------------------------------------------------
  Function Name: Abrir
  Author:    lgisuk
  Date Created: 12/07/2006
  Description: Abro el Archivo TXT
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
Function TFRecepcionRendicionesBDP.Abrir : Boolean;
resourcestring
  	MSG_OPEN_FILE_ERROR = 'Error al abrir el archivo';
begin
    Result := False;
    FLista.text := FileToString(txtOrigen.text);
    FTotalRegistros := FLista.Count - 1; 
    if FLista.text <> '' then  begin
        Result := True;
        if FBDP_Directorio_Procesados <> '' then begin
            if RightStr(FBDP_Directorio_Procesados,1) = '\' then  FBDP_Directorio_Procesados := FBDP_Directorio_Procesados + ExtractFileName(txtOrigen.text)
            else FBDP_Directorio_Procesados := FBDP_Directorio_Procesados + '\' + ExtractFileName(txtOrigen.text);
        end;
    end else begin
        MsgBox(MSG_OPEN_FILE_ERROR, Self.Caption, MB_OK + MB_ICONINFORMATION);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: Control
  Author:    lgisuk
  Date Created: 19/07/2006
  Description: verifico la consistencia entre los valores reflejados
               en el Registro del encabezado y los mismos conceptos calculados
               a partir de los registros de detalle.
-----------------------------------------------------------------------------}
Function TFRecepcionRendicionesBDP.Control : Boolean;
resourcestring
    MSG_NOT_EQUAL_COUNT = 'La cantidad reflejada en el registro de encabezado no coincide'+ CRLF + 'con la cantidad calculada!';
    MSG_NOT_EQUAL_SUM   = 'La suma reflejada en el registro de encabezado no coincide con la suma calculada!';
    MSG_CANT_CONTROL    = 'Cantidad Control: ';
    MSG_CANT_CALC       = 'Cantidad Calculada: ';
    MSG_SUM_CONTROL     = 'Suma Control: ';
    MSG_SUM_CALC        = 'Suma Calculada: ';
    MSG_DIF             = 'Diferencia: ';
    MSG_ERROR           = 'No se pudo realizar el control del archivo!';
var
    i : Integer;
    FCantidadControl, FSumaControl, FSumaReal: Real;
begin
    Result := False;
    try
        //Obtengo la cantidad y la suma del archivo de control
        FCantidadControl := StrToFloat(Trim(Copy(Flista.Strings[0], 9, 7)));
        FSumaControl := StrToFloat(Trim(Copy(Flista.Strings[0], 17, 17)));
        //comparo la cantidad contra la calculada
        If FCantidadControl <> FTotalRegistros then begin
            MsgBoxErr(MSG_NOT_EQUAL_COUNT + CRLF + MSG_CANT_CONTROL  + FloatToSTR(FCantidadControl) + CRLF + MSG_CANT_CALC  + FloatToSTR(FTotalRegistros) + CRLF + MSG_DIF + FloatToSTR(FCantidadControl - FTotalRegistros) ,'', self.text, 0);
            exit;
        end;
        //sumo los registros del archivo
        i:=1; 
        FSumaReal:=0;
      	while i < FLista.count do begin
            FSumaReal := FSumaReal + StrToFloat(Trim(Copy(Flista.Strings[i], 10, 10)));
            inc(i);
        end;
        //comparo la suma contra la calculada
        if FSumacontrol <> FSumaReal then begin
            MsgBoxErr(MSG_NOT_EQUAL_SUM  + CRLF + MSG_SUM_CONTROL  + FloatToSTR(FSumaControl) + CRLF + MSG_SUM_CALC  + FloatToSTR(FSumaReal) + CRLF + MSG_DIF + FloatToSTR(FSumaControl - FSumaReal), '', self.text, 0);
            exit;
        end;
        Result := True;
    except
        on E : Exception do begin
            MsgBoxErr(MSG_ERROR, E.Message, MSG_ERROR, MB_ICONERROR);
    		end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ConfirmaCantidades
  Author:    lgisuk
  Date Created: 12/07/2006
  Description:  Consulta al usuario si quiere continuar o no,
                presentandole un resumen de las cantidades a procesar
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFRecepcionRendicionesBDP.ConfirmaCantidades: Boolean;
ResourceString
    MSG_TITLE        = 'Recepci�n de Rendiciones';
    MSG_CONFIRMATION = 'El Archivo seleccionado contiene %d rendiciones a procesar.'+crlf+crlf+
                       'Desea continuar con el procesamiento del archivo?';
var
    Mensaje : String;
begin
    //Armo el mensaje
    Mensaje := Format(MSG_CONFIRMATION,[FTotalRegistros]);
    //lo muestro
    Result := ( MsgBox(Mensaje,MSG_TITLE, MB_ICONQUESTION + MB_YESNO) = mrYes );
end;

{-----------------------------------------------------------------------------
  Function Name: ParseLineToRecord
  Author:    lgisuk
  Date Created: 12/07/2006
  Description: Arma un registro de Rendicion en base a la linea leida del TXT
  Parameters:
  Return Value: boolean
-----------------------------------------------------------------------------
  Revision 1
  lgisuk
  02/08/2007
  Pase monte de StrToInt to StrtoINT64
-----------------------------------------------------------------------------}
function TFRecepcionRendicionesBDP.ParseLineToRecord(Linea : String; var Rendicion : TRendicion; var DescError : String) : Boolean;
Resourcestring
    MSG_ERROR                   = 'Error al leer el registro';
Const
    STR_VOUCHER                 = 'Comprobante: ';
    STR_DIVIDER                 = ' - ';
    STR_INVOICE_NUMBER_ERROR    = 'El n�mero de la nota de cobro es inv�lido.';
    STR_AMMOUNT_ERROR           = 'El monto es inv�lido.';
  	STR_DATE_ERROR              = 'La fecha es inv�lida.';
    STR_PAYMENT_METHOD_ERROR    = 'El tipo de pago es inv�lido.';
    STR_INVALID_SERVICE         = 'El canal de pago es incorrecto';
Var
    Ano, Mes, Dia : String;
    sMonto : String;
    sMedioPago : String;
    sCodigoServicio : String;
    NumeroComprobante : String;
begin
    Result := False;
    DescError := '';

    try

        //Obtengo los Datos de la Rendici�n
        with Rendicion do begin

            //Parseo la linea
            NumeroComprobante  := Trim( Copy(Linea, 1, 9 ));
            sMonto 				     := Trim(Copy(Linea, 10, 10));
            Ano                := Copy(Linea, 20, 4);
            Mes                := Copy(Linea, 24, 2);
            Dia                := Copy(Linea, 26, 2);
            sMedioPago 		   	 := Copy(Linea, 29, 1);
      			sCodigoServicio		 := Copy(Linea, 30, 1);

            //Verifico que el numero de comprobante sea numerico
            try
                NotaCobro := StrToInt(NumeroComprobante);
            except
                DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER + STR_INVOICE_NUMBER_ERROR;
                Exit;
            end;

            //Verifico que el Monto sea numerico
            try
                Monto := StrToInt64(sMonto);
            except
                DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER + STR_AMMOUNT_ERROR;
                Exit;
            end;

            //Verifico que sea una fecha valida
            try
                Fecha := EncodeDate(StrToInt(Ano), StrToInt(Mes), StrToInt(Dia));
            except
                DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER +  STR_DATE_ERROR;
                Exit;
            end;

            //verifico que el valor de medio de pago sea numerico
            try
                MedioPago := StrToInt(sMedioPago );
            except
                DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER + STR_PAYMENT_METHOD_ERROR;
                Exit;
            end;

            //Verifico que el medio de pago sea efectivo
            if (MedioPago <> 0)  then begin
                DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER + STR_PAYMENT_METHOD_ERROR;
                Exit;
            end;

            //canal de pago
            try
                CodigoServicio := StrToInt(sCodigoServicio );
            except
                DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER + STR_INVALID_SERVICE;
                Exit;
            end;
            
        end;

        Result := True;

    except
        on E: Exception do begin
            MsgBoxErr(MSG_ERROR, E.Message, Self.Caption, MB_ICONERROR);
            FErrorGrave:=true;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: AnalizarRencionesTXT
  Author:    lgisuk
  Date Created: 12/07/2006
  Description: Antes de hacer nada Parsea todo el Archivo para verificar
  				que el parseo sea v�lido
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
Function TFRecepcionRendicionesBDP.AnalizarRendicionesTXT : Boolean;
Resourcestring
    MSG_ERROR_SYNTAXIS = 'El Archivo contiene errores de Sintaxis';
var
    I : Integer;
    Rendicion : TRendicion;
    DescError : String;
begin
    FCancelar := False;                //Permito que la importacion sea cancelada
    FMontoArchivo := 0;
    PbProgreso.Position := 0;          //Inicio la Barra de progreso
    PbProgreso.Max := FLista.Count;  //Establezco como maximo la cantidad de registros del memo
    FErrores.Clear;
    I := 1;
    //recorro las lineas del archivo
    while I < FLista.Count  do begin

        //si cancelan la operacion
        if FCancelar then begin
            Result := False;             //El analisis fue cancelado por el usuario
            pbProgreso.Position := 0;  //Si la importacion fue detenida, Vuelvo a Comenzar
            Exit;                      //salgo de la rutina
        end;

        //Arma un registro de Rendicion en base a la linea leida del TXT
        //verifica que los valores recibidos sean validos sino devuelve una
        //Descripci�n del error.
        if not ParseLineToRecord(Flista.Strings[i], Rendicion, DescError) then begin
            //lo inserto en el string list de errores
            FErrores.Add(DescError);
        end
        else FMontoArchivo := FMontoArchivo + Rendicion.Monto;

        pbProgreso.Position:= pbProgreso.Position + 1;  //Muestro el progreso
        Application.ProcessMessages;                    //Refresco la pantalla

        I := I + 1;
    end;

    Result:= (FErrores.count = 0);

    if (Result = False) then MsgBox(MSG_ERROR_SYNTAXIS, Self.Caption, MB_OK + MB_ICONINFORMATION);

end;

{-----------------------------------------------------------------------------
  Function Name: RegistrarOperacion
  Author:    lgisuk
  Date Created: 12/07/2006
  Description: Registra la Operaci�n en el Log de Operaciones
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
function TFRecepcionRendicionesBDP.RegistrarOperacion : Boolean;
resourcestring
	  MSG_COULD_NOT_REGISTER_OPERATION = 'No se pudo registrar la operaci�n';
    MSG_ERROR = 'Error';
Const
    STR_OBSERVACION = 'Procesar Archivo de Rendiciones';
var
    NombreArchivo: String;
  	DescError    : String;
begin
   NombreArchivo := Trim(ExtractFileName(TxtOrigen.text));
   Result := RegistrarOperacionEnLogInterface (DMConnections.BaseCAC , RO_MOD_INTERFAZ_ENTRANTE_RENDICIONES_BDP , NombreArchivo, UsuarioSistema , STR_OBSERVACION , False , False, NowBase(DMConnections.BaseCAC) , 0 , FCodigoOperacion, FTotalRegistros, FMontoArchivo, DescError);
   if (not Result) then MsgBoxErr(MSG_COULD_NOT_REGISTER_OPERATION, DescError, MSG_ERROR, MB_ICONERROR);
end;

{-----------------------------------------------------------------------------
  Function Name: ActualizarRenciones
  Author:    lgisuk
  Date Created: 12/07/2006
  Description: Proceso el archivo de rendiciones
  Parameters: 
  Return Value: Boolean
-----------------------------------------------------------------------------}
Function TFRecepcionRendicionesBDP.ActualizarRendiciones : Boolean;

      {-----------------------------------------------------------------------------
        Function Name: ActualizarRendicion
        Author:    lgisuk
        Date Created: 12/07/2006
        Description: Registro el pago con la informacion recibida en el archivo
        Parameters:
        Return Value: boolean
      -----------------------------------------------------------------------------
        Revision 1
         Author: lgisuk
         Date Created: 14/12/2006
         Description: Escribo sentencias sin utilizar comando with,
                      no utilizo refresh.
                      
        Revision 2
         Author: lgisuk
         Date Created: 29/03/2006
         Description: al store se le pasa como parametro si es reintento o no para 
         que diferencie un reintento de un registro duplicado
                      
                      
                      
      -----------------------------------------------------------------------------}
      Function ActualizarRendicion (Rendicion : TRendicion; var DescError : String) : Boolean;
      resourcestring
          MSG_ERROR_SAVING_INVOICE = 'Error al Asentar la Operaci�n Comprobante: %d  ';
          MSG_ERROR     = 'Error al actualizar la Rendicion';
      const
          STR_COMPROBANTE  = 'Comprobante: ';
          STR_SEPARADOR = ' - ';
      var
          Error : String;
          //
          Reintentos : Integer;
          mensajeDeadlock : AnsiString;
      begin
          Result := False;
          DescError := '';
          Error := '';

          //Intentamos registrar el pago de un comprobante.
          Reintentos  := 0;

          while Reintentos < 3 do begin

                  try
                        SPProcesarRendicionesBDP.Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacion;
                        SPProcesarRendicionesBDP.Parameters.ParamByName('@NotaCobro').Value := Rendicion.NotaCobro;
                        SPProcesarRendicionesBDP.Parameters.ParamByName('@Monto').Value := Rendicion.Monto;
                        SPProcesarRendicionesBDP.Parameters.ParamByName('@Fecha').Value := Rendicion.Fecha;
                        SPProcesarRendicionesBDP.Parameters.ParamByName('@MedioPago').Value := Rendicion.MedioPago;
                        SPProcesarRendicionesBDP.Parameters.ParamByName('@CodigoServicio').Value := Rendicion.CodigoServicio;
                        SPProcesarRendicionesBDP.Parameters.ParamByName('@Usuario').Value	:= UsuarioSistema;
                        SPProcesarRendicionesBDP.Parameters.ParamByName('@Reintento').Value	:= Reintentos;
                        SPProcesarRendicionesBDP.Parameters.ParamByName('@DescripcionError').Value := '';
                        SPProcesarRendicionesBDP.CommandTimeout := 500;
                        SPProcesarRendicionesBDP.ExecProc;
                        Error := Trim(Copy(VarToStr(SPProcesarRendicionesBDP.Parameters.ParamByName('@DescripcionError').Value),1,100));
                        if Error <> '' then begin
                            DescError := STR_COMPROBANTE + IntToStr(Rendicion.NotaCobro) + STR_SEPARADOR + Error;
                            exit;
                        end;
                        Result := True;
                        //1/4 de segundo entre registracion de pago y otro
                        Sleep(25);
                        //Salgo del ciclo por Ok
                        Break;
                        
                  except
                      on  E : Exception do begin
                          if (pos('deadlock', e.Message) > 0) then begin
                              mensajeDeadlock := e.message;
                              inc(Reintentos);
                              sleep(2000);
                          end else begin
                              FErrorGrave := True;
                              MsgBoxErr(Format(MSG_ERROR_SAVING_INVOICE, [Rendicion.NotaCobro]), E.Message, Self.caption, MB_ICONERROR);
                              Break;
                          end;
                      end;
                  end;
          end;

          //una vez que se reintento 3 veces x deadlock lo informo
          if Reintentos = 3 then begin
              FErrorGrave := True;
              MsgBoxErr(Format(MSG_ERROR_SAVING_INVOICE, [Rendicion.NotaCobro]), MensajeDeadLock, MSG_ERROR, MB_ICONERROR);
          end;

      end;

var
    i: Integer;
    Rendicion : TRendicion;
    DescError: String;
begin
    Result := False;
    FCancelar := False;                //Permito que la importacion sea cancelada
    pbProgreso.Position := 0;          //Inicio la Barra de progreso
    pbProgreso.Max := FLista.Count;  //Establezco como maximo la cantidad de registros del memo
    I := 1; 
    while I < FLista.Count  do begin

        //si cancelan la operacion
        if FCancelar then begin
             pbProgreso.Position := 0;
             Exit;
        end;

        //Obtengo el registro con los datos de la rendicion del archivo
        ParseLineToRecord(Flista.Strings[i], Rendicion, DescError);
        //Registro el pago con la informaci�n de la rendicion
        ActualizarRendicion (Rendicion, DescError);

        pbProgreso.Position:= pbProgreso.Position + 1;  //Muestro el progreso
        Application.ProcessMessages;                    //Refresco la pantalla
        i:= i + 1;
    end;

    Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: GenerarReporteErroresSintaxis
  Author:    lgisuk
  Date Created: 12/07/2006
  Description: Genero el reporte de errores de sintaxis
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFRecepcionRendicionesBDP.GenerarReporteErroresSintaxis : Boolean;
resourcestring
    MSG_REPORT_ERROR = 'No se pudo generar el reporte de errores de Sintaxis';
    MSG_ERROR        = 'Error';
const
    REPORT_TITLE     = 'Santander - Procesar Archivo de Rendiciones';
var
    F : TFRptErroresSintaxis;
begin
    Result := False;
    try
        //muestro el reporte
        Application.CreateForm(TFRptErroresSintaxis, F);
        if not F.Inicializar( IntToStr(RO_MOD_INTERFAZ_ENTRANTE_RENDICIONES_BDP) , REPORT_TITLE , FBDP_Directorio_Errores ,FErrores) then f.Release;
        Result := True;
    except
       on E : Exception do begin
            MsgBoxErr(MSG_REPORT_ERROR, E.Message, MSG_ERROR, MB_ICONERROR);
            Exit;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: GenerarReporteDeFinalizacion
  Author:    lgisuk
  Date Created: 12/07/2006
  Description: Genero el Reporte de Finalizacion de Proceso sirve para
               para verificar si el proceso se realizo segun parametros
               normales.
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFRecepcionRendicionesBDP.GenerarReporteDeFinalizacion(CodigoOperacionInterfase : Integer) : Boolean;
Const
    STR_TITLE = 'Comprobantes Recibidos';
var
    FRecepcion : TFrmRptRecepcionComprobantesBDP;
begin
    Result := False;
    try
        //muestro el reporte
        Application.CreateForm(TFrmRptRecepcionComprobantesBDP,FRecepcion);
        if not fRecepcion.Inicializar( STR_TITLE, CodigoOperacionInterfase) then fRecepcion.Release;
        Result := True;
    except
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnProcesarClick
  Author:    lgisuk
  Date Created: 12/07/2006
  Description: Procesa el archivo de Rendiciones
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRecepcionRendicionesBDP.btnProcesarClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: ObtenerCantidadErroresInterfaz
      Author:    lgisuk
      Date Created: 12/07/2006
      Description: Obtengo la cantidad de errores contemplados que se produjeron
                   al procesar el archivo
      Parameters: CodigoOperacion:string
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function ObtenerCantidadErroresInterfaz(CodigoOperacion : Integer ; var Cantidad : Integer) : Boolean;
    resourcestring
        MSG_ERROR = 'No se pudo obtener la cantidad de errores de la interfaz';
    begin
        Cantidad := 0;
        try
            Cantidad := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.ObtenerCantidadErroresInterfaz ('+IntToStr(CodigoOperacion)+')');
            Result := True;
        except
           on E : Exception do begin
               MsgBoxErr(MSG_ERROR, E.Message, Self.caption, MB_ICONERROR);
               Result := False;
           end;
        end;
    end;

   {-----------------------------------------------------------------------------
      Function Name: ActualizarLog
      Author:    lgisuk
      Date Created: 12/07/2006
      Description: Actualizo el log al finalizar
      Parameters: CantidadErrores:integer
      Return Value: boolean
   -----------------------------------------------------------------------------}
   Function ActualizarLog (CantidadErrores : Integer) : Boolean;
   resourcestring
        MSG_ERROR = 'No se pudo Actualizar el log de operaciones';
   const
        STR_ERRORES = 'Finalizo OK!, Errores Validaci�n: ';
   var
        DescError : String;
   begin
        try
            Result := ActualizarLogOperacionesInterfaseAlFinal
                            (DMConnections.BaseCAC,FCodigoOperacion,STR_ERRORES+ IntToStr(CantidadErrores),DescError);
        except
            on E : Exception do begin
               MsgBoxErr(MSG_ERROR, E.Message, Self.caption, MB_ICONERROR);
               Result := False;
            end;
        end;
   end;

resourcestring
    MSG_PROCESS_CANCEL = 'Proceso cancelado por el usuario';
    MSG_PROCESS_NOT_COMPLETE = 'El proceso no se pudo completar';
    MSG_PROCESS_FINALLY_WITH_ERROR = 'El proceso finaliz� con Errores';
  	MSG_PROCESS_FINALLY_OK = 'Proceso finalizado con �xito';
Const
    STR_OPEN_FILE          = 'Abrir Archivo...';
    STR_CHECK_FILE         = 'Control del Archivo...';
    STR_CONFIRM            = 'Confirmar Cantidades...';
    STR_ANALYZING          = 'Analizando Rendiciones...';
    STR_REGISTER_OPERATION = 'Registrando Operacion en el Log de Operaciones...';
    STR_PROCESS            = 'Registrando los Pagos...';
var
    CantidadErrores : Integer;
begin
    BtnProcesar.Enabled := False;
    PnlAvance.Visible := True;       //El Panel de Avance es visible durante el procesamiento
    KeyPreview := True;
  	try
        //Inicio la operacion

        //Abro el archivo
    		Lblmensaje.Caption := STR_OPEN_FILE;
    		if not Abrir then begin
            FErrorGrave := True;
            Exit;
        end;

        //Controlo el archivo
        Lblmensaje.Caption := STR_CHECK_FILE;
        if not Control then begin
            FErrorGrave := True;
            Exit;
        end;

        //El usuario acepta o no continuar con el proceso
        Lblmensaje.Caption := STR_CONFIRM;
        if not ConfirmaCantidades then begin
            FErrorGrave := True;
            Exit;
        end;

        //Analizo si hay errores de parseo o sintaxis en el archivo
        Lblmensaje.Caption := STR_ANALYZING;
        if not AnalizarRendicionesTxt then begin
            FErrorGrave := True;
            Exit;
        end;

        //Registro la operaci�n en el Log
        Lblmensaje.Caption := STR_REGISTER_OPERATION;
        if not RegistrarOperacion then begin
            FErrorGrave := True;
            Exit;
        end;

        //Proceso el archivo de Rendiciones
    		Lblmensaje.Caption := STR_PROCESS;
    		if not ActualizarRendiciones then begin
            FErrorGrave := True;
            Exit;
        end;

        //Obtengo la cantidad de errores
        ObtenerCantidadErroresInterfaz(FCodigoOperacion, CantidadErrores);

        //Actualizo el log al Final
        ActualizarLog(CantidadErrores);

  	finally
        pbProgreso.Position := 0;  //Inicio la Barra de progreso
        PnlAvance.visible := False;  //Oculto el Panel de Avance
    		Lblmensaje.Caption := '';
        if FCancelar then begin
            //Si fue cancelado por el usuario muestro mensaje que el proceso fue cancelado
            MsgBox(MSG_PROCESS_CANCEL, self.Caption, MB_OK + MB_ICONINFORMATION);
        end else if FErrorGrave then begin
            //Si se produjo un error grave muestro un cartel indicando que hubo errores
            MsgBox(MSG_PROCESS_NOT_COMPLETE, self.Caption, MB_OK + MB_ICONINFORMATION);
            //Genero el Reporte de Error con el Report Builder
            if (Ferrores.Count > 0) then GenerarReporteErroresSintaxis;
        end else begin

            //Muestro Mensaje de Finalizaci�n
            MsgBox(MSG_PROCESS_FINALLY_OK, self.Caption, MB_OK + MB_ICONINFORMATION);

            //Genero el reporte de finalizacion de proceso
            GenerarReportedeFinalizacion(FCodigoOperacion);

        end;
        KeyPreview := False;
  	end;
end;

{-----------------------------------------------------------------------------
  Function Name: FormKeyDown
  Author:    lgisuk
  Date Created: 12/07/2006
  Description: permito cancelar la operacion
  Parameters: Sender: TObject;var Key: Word; Shift: TShiftState
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRecepcionRendicionesBDP.FormKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
begin
    case Key of
        VK_ESCAPE: FCancelar := True;
    else
        FCancelar := False;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: SalirBTNClick
  Author:    lgisuk
  Date Created: 12/07/2006
  Description: cierro el formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRecepcionRendicionesBDP.btnSalirClick(Sender: TObject);
begin
  	Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormCloseQuery
  Author:    lgisuk
  Date Created: 12/07/2006
  Description: impido salir si esta procesando
  Parameters: Sender: TObject;var CanClose: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRecepcionRendicionesBDP.FormCloseQuery(Sender: TObject;var CanClose: Boolean);
begin
    //Permite salir solo si no esta procesando
    CanClose := not PnlAvance.visible;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 12/07/2006
  Description: lo libero de memoria
  Parameters: Sender: TObject;var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRecepcionRendicionesBDP.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    Action := caFree;
end;

initialization
    FLista   := TStringList.Create;
    FErrores := TStringList.Create;
finalization
    FreeAndNil(Flista);
    FreeAndNil(FErrores);
end.


