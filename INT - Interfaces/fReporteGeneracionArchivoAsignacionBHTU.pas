unit fReporteGeneracionArchivoAsignacionBHTU;

interface

uses
 // Utiles
    PeaProcs, PeaTypes, ComunesInterfaces, Util, UtilProc, StrUtils,
    // Parametros GEnerales
    ConstParametrosGenerales,
   // DB
    UtilDB, DMConnection,
    // Otros
    Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
    Dialogs, StdCtrls, DmiCtrls, ComCtrls, ExtCtrls, DB, ADODB, ppDB, ppDBPipe, ppPrnabl, ppClass,
  ppCtrls, ppBands, ppCache, ppComm, ppRelatv, ppProd, ppReport, UtilRB, ppParameter;


type
  TfrmReporteGeneracionArchivoAsignacionBHTU = class(TForm)
    spObtenerLog: TADOStoredProc;
    dsReporte: TDataSource;
    ppReport2: TppReport;
    ppHeaderBand2: TppHeaderBand;
    lbl_usuario: TppLabel;
    ppLabel13: TppLabel;
    ppLabel14: TppLabel;
    ppLabel18: TppLabel;
    ppLabel19: TppLabel;
    ppDetailBand2: TppDetailBand;
    ppFooterBand2: TppFooterBand;
    RBI: TRBInterface;
    ppDBPipeline: TppDBPipeline;
    ppDBText: TppDBText;
    ppLabel1: TppLabel;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppLabel2: TppLabel;
    ppDBText5: TppDBText;
  private
    { Private declarations }
  public
    { Public declarations }
    function Inicializar(Titulo: AnsiString; CodigoOperacion: Integer): boolean;
  end;

var
  frmReporteGeneracionArchivoAsignacionBHTU: TfrmReporteGeneracionArchivoAsignacionBHTU;

implementation

{$R *.dfm}

{ TfrmReporteGeneracionArchivoAsignacionBHTU }

function TfrmReporteGeneracionArchivoAsignacionBHTU.Inicializar(Titulo: AnsiString; CodigoOperacion: Integer): boolean;
resourcestring
    ERROR_CANT_SHOW_REPORT = 'No se puede mostrar el reporte';
begin
    Result := False;
    try
        RBI.Caption := Titulo;
        spObtenerLog.Parameters.ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacion;
        spObtenerLog.Open;
        Result := RBI.Execute(True);
    except
        MsgBox(ERROR_CANT_SHOW_REPORT, Caption, MB_ICONERROR);
    end;

end;

end.
