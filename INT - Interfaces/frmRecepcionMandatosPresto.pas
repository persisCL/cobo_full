{-----------------------------------------------------------------------------
 File Name: FrmRecepcionMandatosPresto.pas
 Author:    flamas
 Date Created: 03/01/2005
 Language: ES-AR
 Description: Modulo de la interfaz Presto - Recepci�n de Mandatos
-----------------------------------------------------------------------------}
{Revision History
-----------------------------------------------------------------------------}
{Author: lgisuk
16/03/2005: Obtengo el Filtro
21/06/2005: Obtengo la cantidad de errores contemplados que se produjeron al procesar el archivo
21/06/2005: Ordene las rutinas por su orden de ejecuci�n.
01/07/2005: Actualizo el log al finalizar
14/07/2005: Mensaje de Ayuda, Defini tama�o de ventana minimo, Permito maximizar y ajustar los forms. Separe units del form de las generales.
26/07/2005: Quite la llamada al parametro general 'CodigodeComercio' en rutina obtener filtro porque no se utilizaba.
26/07/2005: Corregi Tabulaci�n. se perdio al migrar a la nueva version de Delphi.
26/07/2005: Obtengo los Parametros Generales que se utilizaran en el formulario al inicializar y verifico que los valores obtenidos sean validos.



Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.



Firma       : SS_1147Q_NDR_20141202[??]
Descripcion : Si es VS el convenio es de largo 12 no 17

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

------------------------------------------------------------------------------}
unit frmRecepcionMandatosPresto;

interface

uses
  //Recepcion Mandatos Presto
  DMConnection,
  Util,
  UtilProc,
  ComunesInterfaces,
  ConstParametrosGenerales,
  PeaProcs,
  PeaTypes,
  UtilDB,
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, DB, ADODB, Grids, DBGrids, StdCtrls, Buttons, ComCtrls,
  ListBoxEx, DBListEx, DPSControls, ppDB, ppTxPipe, StrUtils,
  ppParameter, ppModule, raCodMod, ppCtrls, ppBands, ppReport, ppPrnabl,
  ppClass, ppStrtch, ppSubRpt, ppCache, ppComm, ppRelatv, ppProd, UtilRB,
  RStrings, RBSetup,ppDBPipe;

type
  TMandatoPrestoRecord = record
    TipoOperacion : string;
    NumeroDocumento : string;
    NumeroTarjeta : string;
    NumeroConvenio : string;
    FechaInicio	: TDateTime;
    CodigoRespuesta : string;
    GlosaRespuesta : string;
  end;

  TfRecepcionMandatosPresto = class(TForm)
    Bevel1: TBevel;
    btnAbrirArchivo: TSpeedButton;
    Label2: TLabel;
    edOrigen: TEdit;
    OpenDialog: TOpenDialog;
    rbiListado: TRBInterface;
    ppReporteRecepMandatosPresto: TppReport;
  	ppDetailBand1: TppDetailBand;
    ppSubReport1: TppSubReport;
    ppChildReport1: TppChildReport;
    ppHeaderBand2: TppHeaderBand;
    ppLine2: TppLine;
    ppLabel1: TppLabel;
    lbl_usuario: TppLabel;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppImage2: TppImage;
    ppDetailBand2: TppDetailBand;
    ppDBText1: TppDBText;
    ppParameterList1: TppParameterList;
    btnProcesar: TButton;
    btnCancelar: TButton;
    btnSalir: TButton;
    spActualizarRecepcionMandatosPresto: TADOStoredProc;
    dsObtenerMandatosRechazadosPresto: TDataSource;
    ppErroresPipeline: TppTextPipeline;
    ppField1: TppField;
    pnlAvance: TPanel;
    lblprocesoGeneral: TLabel;
    pbProgreso: TProgressBar;
    lblReferencia: TLabel;
    pnlAyuda: TPanel;
    ImgAyuda: TImage;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnProcesarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure btnAbrirArchivoClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure ImgAyudaClick(Sender: TObject);
    procedure ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
  private
    { Private declarations }
    FCodigoOperacion : integer;
  	FDetenerImportacion: boolean;
  	FMandatosTXT : TStringList;
    FErrores : TStringList;
  	FErrorMsg : string;
    FVerReporteErrores: Boolean;
    FTotalRegistros: integer;
    FPresto_CodigodeComercio : AnsiString;
   	FPresto_Directorio_Origen_Mandatos : AnsiString;
    FPresto_DirectorioMandatosProcesados: AnsiString;
    FPresto_Directorio_Errores	: AnsiString;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    function  ParseMandatoPrestoLine(sline : string; var MandatoRecord : TMandatoPrestoRecord; var sParseError : string) : boolean;
  	function  AnalizarMandatosTXT : boolean;
  	function  RegistrarOperacion : boolean;
    function  ActualizarMandatos : boolean;
	  procedure GenerarReportesErrores;
  public
	  function  Inicializar(txtCaption: ANSIString; MDIChild:Boolean): Boolean;
	{ Public declarations }
  end;

var
  fRecepcionMandatosPresto: TfRecepcionMandatosPresto;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    flamas
  Date Created: 29/12/2004
  Description: Inicializa el Formulario
  Parameters: txtCaption: ANSIString; MDIChild:Boolean
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TfRecepcionMandatosPresto.Inicializar(txtCaption: ANSIString; MDIChild:Boolean): Boolean;

{******************************** Function Header ******************************
    Function Name: VerificarParametrosGenerales
    Author : lgisuk
    Date Created : 26/07/2005
    Description : Obtengo los Parametros Generales que se utilizaran en el formulario
                  al inicializar y verifico que los valores obtenidos sean validos.
    Parameters : None
    Return Value : boolean
    *******************************************************************************}
    Function VerificarParametrosGenerales : boolean;
    resourcestring
        MSG_ERROR_CHECK_GENERAL_PARAMETER = 'Error al verificar parametro general';
        MSG_ERROR                         = 'Error';
        STR_NOT_EXISTS_GENERAL_PARAMETER  = 'No existe parametro general: ';
        STR_EMPTY_GENERAL_PARAMETER       = 'Parametro general vacio: ';                                         //Se verifica que los codigos no esten vacios, si no tienen razon de ser
        STR_DIRECTORY_NOT_EXISTS          = 'No existe el directorio indicado en parametros generales: ' + CRLF; //Los directorios de destino y errores en procesos de envio son criticos
    const
        PRESTO_CODIGODECOMERCIO             = 'Presto_CodigodeComercio';
        PRESTO_DIRECTORIO_ORIGEN_MANDATOS   = 'Presto_Directorio_Origen_Mandatos';
        PRESTO_DIRECTORIO_ERRORES           = 'Presto_Directorio_Errores';
        PRESTO_DIRECTORIOMANDATOSPROCESADOS = 'Presto_DirectorioMandatosProcesados';
    var
        DescError : AnsiString;
    begin
        Result    := True;
        DescError := '';
        try
            try

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, PRESTO_CODIGODECOMERCIO , FPresto_CodigodeComercio) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + PRESTO_CODIGODECOMERCIO;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                if not (FPresto_CodigodeComercio <> '') then begin
                   DescError := STR_EMPTY_GENERAL_PARAMETER + PRESTO_CODIGODECOMERCIO;
                   Result := False;
                   Exit;
                end;

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, PRESTO_DIRECTORIO_ORIGEN_MANDATOS  , FPresto_Directorio_Origen_Mandatos) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + PRESTO_DIRECTORIO_ORIGEN_MANDATOS;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                FPresto_Directorio_Origen_Mandatos := GoodDir(FPresto_Directorio_Origen_Mandatos);
                if  not DirectoryExists(FPresto_Directorio_Origen_Mandatos) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FPresto_Directorio_Origen_Mandatos;
                    Result := False;
                    Exit;
                end;

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, PRESTO_DIRECTORIO_ERRORES , FPresto_Directorio_Errores) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + PRESTO_DIRECTORIO_ERRORES;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                FPresto_Directorio_Errores := GoodDir(FPresto_Directorio_Errores);
                if  not DirectoryExists(FPresto_Directorio_Errores) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FPresto_Directorio_Errores;
                    Result := False;
                    Exit;
                end;

                //Obtengo el parametro general
                ObtenerParametroGeneral(DMConnections.BaseCAC,PRESTO_DIRECTORIOMANDATOSPROCESADOS,FPresto_DirectorioMandatosProcesados);

            except
                on e: Exception do begin
                    Result := False;
                    MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, e.Message, MSG_ERROR, MB_ICONWARNING);
                end;
            end;

        finally
            //si no paso la verificacion de parametros generales
            if (Result = False) then begin
                //informo la situaci�n
                MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, DescError, MSG_ERROR, MB_ICONWARNING);
            end;
        end;
    end;


resourcestring
    MSG_INIT_ERROR = 'Error al Inicializar';
    MSG_ERROR = 'Error';
begin
  	Result := False;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;

    //Elijo el modo en que si visualizara la ventana
  	if not MDIChild then begin
  		FormStyle := fsNormal;
  		Visible := False;
  	end;

    //Centro el form
  	CenterForm(Self);
  	try
        DMConnections.BaseCAC.Connected := True;
  		  Result := DMConnections.BaseCAC.Connected and
                                VerificarParametrosGenerales;
  	except
        on e: Exception do begin
            MsgBoxErr(MSG_INIT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
        end;
    end;

    //Resolver ac� lo que necesita este form para inicializar correctamente
   	Caption := AnsiReplaceStr(txtCaption, '&', '');
    btnCancelar.Enabled := False;
    btnProcesar.Enabled := False;
    pnlAvance.Visible := False;
    lblReferencia.Caption := '';
end;


{******************************** Function Header ******************************
Function Name: ImgAyudaClick
Author : lgisuk
Date Created : 14/07/2005
Description : Mensaje de Ayuda
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfRecepcionMandatosPresto.ImgAyudaClick(Sender: TObject);
Resourcestring
    MSG_RESPUESTA_MANDATOS = ' ' + CRLF +
                             'El Archivo de Respuesta a Mandatos' + CRLF +
                             'es la respuesta que env�a PRESTO al ESTABLECIMIENTO' + CRLF +
                             'para cada Archivo de Mandatos recibido' + CRLF +
                             'y tiene por objeto dar a conocer al ESTABLECIMIENTO,' + CRLF +
                             'el resultado del procesamiento de la informaci�n enviada.' + CRLF +
                             ' ' + CRLF +
                             'Nombre del Archivo: RSP1DDMM1D' + CRLF +
                             ' ' + CRLF +
                             'Se utiliza para actualizar los datos de' + CRLF +
                             'los mandantes en el sistema del ESTABLECIMIENTO.' + CRLF +
                             ' ';
begin
    //si esta procesando sale
    if PnlAvance.visible = true then exit;
    //Muestro el mensaje
    MsgBoxBalloon(MSG_RESPUESTA_MANDATOS , Caption, MB_ICONQUESTION, IMGAYUDA);
end;

{******************************** Function Header ******************************
Function Name: ImgAyudaMouseMove
Author : lgisuk
Date Created : 14/07/2005
Description :
Parameters : Sender: TObject;Shift: TShiftState; X, Y: Integer
Return Value : None
*******************************************************************************}
procedure TfRecepcionMandatosPresto.ImgAyudaMouseMove(Sender: TObject;Shift: TShiftState; X, Y: Integer);
begin
    if PnlAvance.visible = true then ImgAyuda.Cursor := crdefault else ImgAyuda.Cursor := crHandPoint;
end;


{-----------------------------------------------------------------------------
  Function Name: btnAbrirArchivoClick
  Author:    flamas
  Date Created: 28/12/2004
  Description: Abre un archivo de Mandatos
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfRecepcionMandatosPresto.btnAbrirArchivoClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: ObtenerFiltro
      Author:    lgisuk
      Date Created: 16/03/2005
      Description: Obtengo el Filtro
      Parameters: var Filtro: AnsiString
      Return Value: Boolean
    -----------------------------------------------------------------------------}
    Function ObtenerFiltro(var Filtro: AnsiString): Boolean;
    Const
        FILE_TITLE     = 'Recepci�n Mandatos PRESTO|';
        FILE_NAME      = 'RSP1';
    begin
        //Creo el Filtro
    		Filtro := FILE_TITLE + FILE_NAME + '*';
        Result:= True;
    end;

    {-----------------------------------------------------------------------------
      Function Name: ValidarNombreArchivo
      Author:    flamas
      Date Created: 23/12/2004
      Description: Valida el nombre del archivo
      Parameters: sFileName : string
      Return Value: boolean
    -----------------------------------------------------------------------------}
    function ValidarNombreArchivoPresto(sFileName, sTipoArchivo : string) : boolean;
    resourcestring
        MSG_INVALIDA_FILE_NAME = 'El nombre del archivo %s es inv�lido';
        MSG_ERROR  = 'Error';
    begin
        result :=	(Copy(ExtractFileName(sFileName), 1, 4) = sTipoArchivo) and (Copy(ExtractFileName(sFileName), 9, 2) = '1D');
        if not result then MsgBox (Format (MSG_INVALIDA_FILE_NAME, [ExtractFileName (sFileName)]), MSG_ERROR, MB_ICONERROR);
    end;


resourcestring
    MSG_ERROR_FILE_DOES_NOT_EXIST    = 'El archivo %s no existe';
    MSG_ERROR_INVALID_FILENAME       = 'El nombre del archivo %s es inv�lido';
    MSG_ERROR_FILE_ALREADY_PROCESSED = 'El archivo %s ya fue procesado'+ #10#13 + 'Desea re-procesarlo ?';
    MSG_ERROR = 'Error';
const
  	FILE_NAME      = 'RSP1';
var
    Filtro: string;
begin
    //obtengo el filtro a aplicar a este tipo de archivos
    ObtenerFiltro(Filtro);

    Opendialog.InitialDir := FPresto_Directorio_Origen_Mandatos;
  	OpenDialog.FileName := '';
    OpenDialog.Filter := Filtro; //solo veo este tipo de archivos

    if opendialog.execute then begin

       edOrigen.text:=UpperCase( opendialog.filename );

        if  not ValidarNombreArchivoPresto(edOrigen.text, FILE_NAME) then begin
            MsgBox(Format(MSG_ERROR_INVALID_FILENAME, [ExtractFileName(edOrigen.text)]), MSG_ERROR, MB_ICONERROR );
            Exit;
        end;

        if not FileExists( edOrigen.text ) then begin
        	MsgBox( Format ( MSG_ERROR_FILE_DOES_NOT_EXIST, [edOrigen.text]), MSG_ERROR, MB_ICONERROR );
            Exit;
        end;

        if  VerificarArchivoProcesado( DMConnections.BaseCAC, edOrigen.text ) and (MsgBox(Format(MSG_ERROR_FILE_ALREADY_PROCESSED, [ExtractFileName(edOrigen.text)]), Caption, MB_YESNO + MB_ICONQUESTION) = IDNO) then Exit;

        if FPresto_DirectorioMandatosProcesados <> '' then begin
            if RightStr(FPresto_DirectorioMandatosProcesados,1) = '\' then  FPresto_DirectorioMandatosProcesados := FPresto_DirectorioMandatosProcesados + ExtractFileName(edOrigen.text)
            else FPresto_DirectorioMandatosProcesados := FPresto_DirectorioMandatosProcesados + '\' + ExtractFileName(edOrigen.text);
        end;

        btnProcesar.Enabled := True;

  	end;

end;

{-----------------------------------------------------------------------------
  Function Name: ParsePrestoLine
  Author:    flamas
  Date Created: 23/12/2004
  Description: Parsea la linea 
  Parameters: sline : string; var LiderRecord : TRendicionLiderRecord
  Return Value: boolean
-----------------------------------------------------------------------------}
function TfRecepcionMandatosPresto.ParseMandatoPrestoLine(sline : string; var MandatoRecord : TMandatoPrestoRecord; var sParseError : string) : boolean;
resourcestring
    MSG_COMPANY_CODE_ERROR 	= 'C�digo de Empresa Prestadora de Servicios inv�lido';
    MSG_STARTING_DATE_ERROR = 'Fecha de inicio del Mandato inv�lida';
begin
  	result := False;
    with MandatoRecord do begin
  		try
            sParseError := MSG_COMPANY_CODE_ERROR;

            FPresto_CodigodeComercio := PADL(FPresto_CodigodeComercio, 6, '0');

            if ( Copy( sline, 11, 6 ) <> FPresto_CodigodeComercio ) then Exit; // Verifica si es de mi concesionaria
            TipoOperacion 	:= Trim(Copy(sline, 17, 1));
            NumeroDocumento := Trim(Copy(sline, 18, 9));
            NumeroTarjeta 	:= Trim(Copy(sline, 97, 19));
            NumeroConvenio 	:= Trim(Copy(sline, 130, 17));                      
            if ObtenerCodigoConcesionariaNativa=CODIGO_VS then                  //SS_1147Q_NDR_20141202[??]
            begin                                                               //SS_1147Q_NDR_20141202[??]
              NumeroConvenio := Copy(NumeroConvenio,6,12);                      //SS_1147Q_NDR_20141202[??]
            end;                                                                //SS_1147Q_NDR_20141202[??]
            sParseError 	  := MSG_STARTING_DATE_ERROR;
            FechaInicio		  := EncodeDate(	StrToInt('20' + Copy(sline,116,2)), StrToInt(Copy(sline,118,2)), StrToInt(Copy(sline,120,2)));
            CodigoRespuesta := Trim(Copy(sline, 152, 6)); // Toma el �ltimo digito del C�digo de Respuesta 00000'0'
            GlosaRespuesta 	:= Trim(Copy(sline, 153, 94));
            sParseError 	:= ''; //lo borro porque no hubo error
            result := True;

        except
        	  on exception do Exit;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: AnalizarMandatosTXT
  Author:    flamas
  Date Created: 06/01/2005
  Description: Antes de hacer nada Parsea todo el Archivo para verificar
  				que el parseo sea v�lido
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
function  TfRecepcionMandatosPresto.AnalizarMandatosTXT : boolean;
resourcestring
  	MSG_ANALIZING_WARRANTS_FILE	 = 'Analizando Archivo de Mandatos - Cantidad de lineas : %d';
    MSG_WARRANTS_FILE_HAS_ERRORS = 'El archivo de Mandatos contiene errores'#10#13'L�nea : %d';
    MSG_ERROR					 = 'Error';
var
  	nNroLineaScript, nLineasScript :integer;
    FMandatoRecord : TMandatoPrestoRecord;
    sParseError : string;
begin
    Screen.Cursor := crHourglass;

    nLineasScript := FMandatosTXT.Count - 1;
    lblReferencia.Caption := Format( MSG_ANALIZING_WARRANTS_FILE, [nLineasScript] );
    pbProgreso.Position := 1;
    pbProgreso.Max := FMandatosTXT.Count - 1;
    pnlAvance.Visible := True;

    nNroLineaScript := 1;

    while ( nNroLineaScript < nLineasScript ) and ( not FDetenerImportacion ) and (	FErrorMsg = '' ) do begin

    		if not ParseMandatoPrestoLine(FMandatosTXT[nNroLineaScript], FMandatoRecord, sParseError) then begin
          			// Si encuentra un error termina
                FErrorMsg := Format(MSG_WARRANTS_FILE_HAS_ERRORS, [nNroLineaScript]) + #10#13 + sParseError;
                MsgBox(FErrorMsg, Caption, MB_ICONERROR);
        				Screen.Cursor := crDefault;
               	result := False;
                Exit;
        end;

        Inc( nNroLineaScript );
        pbProgreso.Position := nNroLineaScript;
        Application.ProcessMessages;
    end;

    result := (not FDetenerImportacion);
    Screen.Cursor := crDefault;
end;

{-----------------------------------------------------------------------------
  Function Name: RegistrarOperacion
  Author:    flamas
  Date Created: 23/12/2004
  Description: Registra la Operaci�n en el Log de Operaciones
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
function TfRecepcionMandatosPresto.RegistrarOperacion : boolean;
resourcestring
	  MSG_COULD_NOT_REGISTER_OPERATION = 'No se pudo registrar la operaci�n';
    MSG_ERROR = 'Error';
var
	  DescError : string;
begin
	  result := RegistrarOperacionEnLogInterface( DMConnections.BaseCAC, RO_MOD_INTERFAZ_ENTRANTE_MANDATOS_PRESTO, ExtractFileName(edOrigen.text), UsuarioSistema, '', True, False, NowBase(DMConnections.BaseCAC), 0, FCodigoOperacion, DescError  );
    if not result then MsgBoxErr(MSG_COULD_NOT_REGISTER_OPERATION, DescError, MSG_ERROR, MB_ICONERROR);
end;

{-----------------------------------------------------------------------------
  Function Name: ActualizarMandatos
  Author:    flamas
  Date Created: 23/12/2004
  Description: Actualiza los Mandatos en la base de datos
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
function TfRecepcionMandatosPresto.ActualizarMandatos : boolean;
resourcestring
    MSG_PROCESSING_WARRANTS_FILE		= 'Procesando archivo de Mandatos - Cantidad de lineas : %d';
    MSG_ERROR_PROCESSING_WARRANTS_FILE	= 'Error procesando archivo de Mandatos';
    MSG_PROCESSING			   			= 'Procesando...';
  	MSG_ERROR							= 'Error';
var
  	nNroLineaScript, nLineasScript : integer;
    FMandatoRecord : TMandatoPrestoRecord;
    sDescripcionError : string;
    sParseError : string;
begin
  	Screen.Cursor := crHourglass;

  	nLineasScript := FMandatosTXT.Count - 2;
    FTotalRegistros := nLineasScript - 2;
    lblReferencia.Caption := Format( MSG_PROCESSING_WARRANTS_FILE, [nLineasScript] );
    lblReferencia.Caption := MSG_PROCESSING;
    pbProgreso.Position := 0;
    pbProgreso.Max := FMandatosTXT.Count - 2;
    pnlAvance.Visible := True;

    nNroLineaScript := 1;

    while ( nNroLineaScript < nLineasScript ) and ( not FDetenerImportacion ) and ( FErrorMsg = '' ) do begin

        if ParseMandatoPrestoLine(FMandatosTXT[nNroLineaScript], FMandatoRecord, sParseError) then begin

        	with spActualizarRecepcionMandatosPresto, Parameters, FMandatoRecord do begin
  				try
                	ParamByName( '@CodigoOperacionInterfase' ).Value	:= FCodigoOperacion;
                	ParamByName( '@FechaRecepcion' ).Value 				:= NowBase(DMConnections.BaseCAC);
                	ParamByName( '@FechaAlta' ).Value 					:= FechaInicio;
                	ParamByName( '@TipoOperacion' ).Value 				:= TipoOperacion;
                	ParamByName( '@NumeroDocumento' ).Value 			:= NumeroDocumento;
                	ParamByName( '@NumeroTarjeta' ).Value 				:= NumeroTarjeta;
                	ParamByName( '@NumeroConvenio' ).Value 				:= NumeroConvenio;
                	ParamByName( '@CodigoRespuesta' ).Value 			:= CodigoRespuesta;
                	ParamByName( '@GlosaRespuesta' ).Value 				:= GlosaRespuesta;
                  ExecProc;

                	sDescripcionError := Trim( VarToStr( ParamByName( '@DescripcionError' ).Value ));
                	if ( sDescripcionError <> '' ) then FErrores.Add( StringReplace( sDescripcionError, #10#13, ' - ', [rfReplaceAll]));
  				except
                	on e: exception do begin
                				MsgBoxErr(MSG_ERROR_PROCESSING_WARRANTS_FILE, e.Message, MSG_ERROR, MB_ICONERROR);
                        FErrorMsg := MSG_ERROR_PROCESSING_WARRANTS_FILE;
        					end;
          end
        end;
    end;

        Inc( nNroLineaScript );
        pbProgreso.Position := nNroLineaScript;
        Application.ProcessMessages;
    end;

    pbProgreso.Position := pbProgreso.Max;
    Screen.Cursor := crDefault;
    result := ( not FDetenerImportacion ) and ( FErrorMsg = '' );
end;

{-----------------------------------------------------------------------------
  Function Name: GenerarReportesErrores
  Author:    flamas
  Date Created: 23/12/2004
  Description: Genera el Reporte de Errores 
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfRecepcionMandatosPresto.GenerarReportesErrores;
resourcestring
    WARRANTS_ERRORS_FILE			= 'Mandatos Presto - Errores de Procesamiento ';
    REPORT_DIALOG_CAPTION 			= 'Mandatos Presto - Errores';
    MSG_ERROR_SAVING_ERRORS_FILE 	= 'Error generando archivo de errores';
    MSG_ERR_REPORT                  = 'El Reporte de Errores se guardo en: %s';
    TITLE_ERR_REPORT                = 'Reporte de Errores';
var
    sArchivoErrores		: string;
  	Config: TRBConfig;
    RutaLogo: AnsiString;                                                                               //SS_1147_NDR_20140710
begin

	if ( FErrores.Count = 0 ) then Exit;


    ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
    try                                                                                                 //SS_1147_NDR_20140710
      ppImage2.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
    except                                                                                              //SS_1147_NDR_20140710
      On E: Exception do begin                                                                          //SS_1147_NDR_20140710
        Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
        MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
        Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
      end;                                                                                              //SS_1147_NDR_20140710
    end;                                                                                                //SS_1147_NDR_20140710


    // Generaci�n de Reportes de Errores
    sArchivoErrores := FPresto_Directorio_Errores + WARRANTS_ERRORS_FILE + FormatDateTime ( 'yyyy-mm-dd', Now ) + '.txt';
    try
    	FErrores.SaveToFile( sArchivoErrores );
    except
    	on e:exception do begin
        	MsgBoxErr(MSG_ERROR_SAVING_ERRORS_FILE, e.Message, 'Error', MB_ICONERROR);
            Exit;
        end;
    end;

    if not FVerReporteErrores then begin
        MsgBox(Format(MSG_ERR_REPORT,[sArchivoErrores]),TITLE_ERR_REPORT,MB_ICONINFORMATION);
        Exit;
    end;
    // Asigna los Pipelines del reporte
    ppErroresPipeline.FileName := sArchivoErrores;

    rbiListado.Caption := REPORT_DIALOG_CAPTION;

  	// Carga la Configuraci�n de Usuario
    // la funci�n del componente RBInterface no funciona en este caso
    // porque el reporte tiene subreportes y el label est� dentro de un subreporte
    // Adem�s existen 3 labes uno en cada subreporte y no se pueden llamar igual
  	Config := rbiListado.GetConfig;
    if Config.ShowUser then begin
    	if UsuarioSistema <> '' then lbl_usuario.caption := 'Usuario: ' +
        	UsuarioSistema + ' ' else lbl_usuario.Caption := '';
    end else begin
    	lbl_usuario.Caption := '';
    end;
    if Config.ShowDateTime then begin
    	lbl_usuario.caption := lbl_usuario.caption +
        FormatShortDate(Date) + ' ' + FormatTime(Time);
    end;
    rbiListado.Execute(True);
end;

{-----------------------------------------------------------------------------
  Function Name: btnProcesarClick
  Author:    flamas
  Date Created: 23/12/2004
  Description: Procesa el archivo de d�bitos
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfRecepcionMandatosPresto.btnProcesarClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: ObtenerCantidadErroresInterfaz
      Author:    lgisuk
      Date Created: 21/06/2005
      Description: Obtengo la cantidad de errores contemplados que se produjeron
                   al procesar el archivo
      Parameters: CodigoOperacion:string
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function ObtenerCantidadErroresInterfaz(CodigoOperacion:integer;var Cantidad:integer):boolean;
    resourcestring
        MSG_ERROR = 'No se pudo obtener la cantidad de errores de la interfaz';
    begin
        Cantidad := 0;
        try
            Cantidad := QueryGetValueInt(DMconnections.BaseCAC, 'SELECT dbo.ObtenerCantidadErroresInterfaz ('+inttostr(CodigoOperacion)+')');
            Result := true;
        except
           on e: Exception do begin
               MsgBoxErr(MSG_ERROR,e.Message, Self.caption, MB_ICONERROR);
               Result := False;
           end;
        end;
    end;

   {-----------------------------------------------------------------------------
      Function Name: ActualizarLog
      Author:    lgisuk
      Date Created: 01/07/2005
      Description: Actualizo el log al finalizar
      Parameters: CantidadErrores:integer
      Return Value: boolean
   -----------------------------------------------------------------------------}
   Function ActualizarLog(CantidadErrores:integer):boolean;
   Resourcestring
        MSG_ERROR = 'No se pudo Actualizar el log de operaciones';
   Const
        STR_ERRORES = 'Finalizo OK!, Errores Validaci�n: ';
   var
        DescError : string;
   begin
        try
            Result := ActualizarLogOperacionesInterfaseAlFinal
                            (DMConnections.BaseCAC,FCodigoOperacion,STR_ERRORES+ IntToStr(CantidadErrores),DescError);
        except
            on e: Exception do begin
               MsgBoxErr(MSG_ERROR,e.Message, Self.caption, MB_ICONERROR);
               Result := False;
            end;
        end;
   end;

resourcestring
	MSG_PROCESS_SUCCEDED 				= 'El proceso finaliz� con �xito';
	MSG_PROCESS_FINLIZED_WITH_ERRORS	= 'El proceso finaliz� con errores.';
	MSG_PROCESS_COULD_NOT_BE_COMPLETED 	= 'El proceso no se pudo completar';
	MSG_ERROR_CANNOT_OPEN_WARRANTS_FILE = 'No se puede abrir el archivo de mandatos %s';
  MSG_ERRROR_WARRANTS_FILE_IS_EMPTY	= 'El archivo seleccionado est� vac�o';
	PRESTO_RECEIVED_WARRANTS_DESCRIPTION= 'Mandatos validados por Presto -';
Var
    CantidadErrores: Integer;
begin
    //Creo los TStringList
	FMandatosTXT := TStringList.Create;
  FErrores := TStringList.Create;

	FErrorMsg := '';

	// Deshabilita los botones
	btnCancelar.Enabled := True;
	btnProcesar.Enabled := False;
	btnSalir.Enabled := False;
	btnAbrirArchivo.Enabled := False;
	edOrigen.Enabled := False;

	FDetenerImportacion := False;

	try
		try
        	//Lee el archivo del Mandatos
			FMandatosTXT.text:= FileToString(edOrigen.text);

			// Verifica si el Archivo Contiene alguna linea
			if ( FMandatosTXT.Count = 0 ) then
				MsgBox(MSG_ERRROR_WARRANTS_FILE_IS_EMPTY, Caption, MB_ICONERROR)
			else begin

                //Obtengo el codigo de comercio
                FPresto_CodigodeComercio := PADL(FPresto_CodigodeComercio, 6, '0');

            	if  ValidarHeaderFooterPresto( FMandatosTXT, edOrigen.Text, FPresto_CodigodeComercio, PRESTO_RECEIVED_WARRANTS_DESCRIPTION ) and
                	AnalizarMandatosTXT then begin   //verifica si hay error parseo

                    	if RegistrarOperacion  and // Primero registra la Operaci�n para obtener el C�digo de la misma
                        	ActualizarMandatos then begin // Guarda los Mandatos en la Base de Datos

                                //Obtengo la cantidad de errores
                                ObtenerCantidadErroresInterfaz(FCodigoOperacion, CantidadErrores);
                                //Actualizo el log al Final
                                ActualizarLog(CantidadErrores);
                                //Muestra un carte de Finalizacion
                                FVerReporteErrores :=  MsgProcesoFinalizado(MSG_PROCESS_SUCCEDED,MSG_PROCESS_FINLIZED_WITH_ERRORS, Caption, FTotalRegistros,FTotalRegistros-FErrores.Count,FErrores.Count)  ;
                                //Si hubo errores genera el reporte de errores
                								if (FErrores.Count > 0) then GenerarReportesErrores;
                                //Mueve el archivo procesado a otro directorio
                                MoverArchivoProcesado(Caption,edOrigen.Text, FPresto_DirectorioMandatosProcesados);

                        end else begin

                        		//Muestra un cartel que no pudo completar el proceso
                    			MsgBox(MSG_PROCESS_COULD_NOT_BE_COMPLETED, Caption, MB_ICONERROR);

                        end;
                end;


			end;
		except
			on e: Exception do begin
				MsgBoxErr(Format( MSG_ERROR_CANNOT_OPEN_WARRANTS_FILE, [ExtractFilePath(edOrigen.Text)]), e.Message, 'Error', MB_ICONERROR);
				FErrorMsg := MSG_ERROR_CANNOT_OPEN_WARRANTS_FILE;
			end;
		end;
	finally
        //Libero los TStringList
    		FreeAndNil(FMandatosTXT);
    		FreeAndNil(FErrores);
     		//Lo desactiva para poder Cerrar el Form
    		btnCancelar.Enabled := False;
        Close;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnCancelarClick
  Author:    flamas
  Date Created: 07/12/2004
  Description: Detiene el proceso
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfRecepcionMandatosPresto.btnCancelarClick(Sender: TObject);
resourcestring
	MSG_PROCESS_CANCELED_BY_USER = 'Proceso cancelado por el usuario';
begin
    FDetenerImportacion := True;
    FErrorMsg := MSG_PROCESS_CANCELED_BY_USER;
    MsgBox(MSG_PROCESS_CANCELED_BY_USER, Caption, MB_ICONWARNING);
end;

{-----------------------------------------------------------------------------
  Function Name: FormCloseQuery
  Author:    lgisuk
  Date Created: 15/03/2005
  Description: Permito salir si no esta procesando
  Parameters: Sender: TObject;var CanClose: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfRecepcionMandatosPresto.FormCloseQuery(Sender: TObject;var CanClose: Boolean);
begin
  	CanClose := not btnCancelar.Enabled;
end;

{-----------------------------------------------------------------------------
  Function Name: btnSalirClick
  Author:    lgisuk
  Date Created: 15/03/2005
  Description:  Cierro el formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfRecepcionMandatosPresto.btnSalirClick(Sender: TObject);
begin
    close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 15/03/2005
  Description: Lo libero de memoria
  Parameters: Sender: TObject;var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfRecepcionMandatosPresto.FormClose(Sender: TObject;var Action: TCloseAction);
begin
   Action := caFree;
end;


end.
