{
    Unit Name: frmReEmisionAnulacionDiscoXMLInfracciones
    Author: pdominguez
    Date Created: 25/06/2010

    Description: Infractores Fase 2
        - ReEmite la estructura y archivos de un Disco XML Emitido.
        - Permite la anulaci�n de un Disco XML Emitido.

 Revision	:	1
 Author		: 	ebaeza
 Date		:	14/06/2011
 firma      :   SS-377-EBA-20110613
 Descripcion:	Cambio estructura directorios de acceso a imagenes de transitos

 Author         :	CQuezadaI
 Firma          :	SS_660_CQU_20121010
 Descripcion    :	Agerga la l�gica para considerar las infracciones por morosidad

Author          :   Claudio Quezada Ib��ez
Date            :   16-Mayo-2013
Firma           :   SS_1091_CQU_20130516
Description     :   Se modifica la visualizaci�n de im�genes para poder desplegar im�genes de p�rticos Italianos.

 Author		    : 	Claudio Quezada Ib��ez
 Date		    :	16/08/2012
 firma          :   SS_660_CQU_20130816
 Descripcion    :   Se agrega el Tipo de Infraccion al XML

 Author         :   Claudio Quezada Ib��ez
Date            :   16-Agosto-2013
Firma           :   SS_1091_CQU_20130816
Description     :   Se ELIMINAN las l�neas que evitaban enviar la imagen italiana al XML.
                    Cuando una imagen es italiana se carga en un Jpeg luego de haberlo cargado como BMP.

Fecha           :   11/10/2013
Firma           :   SS_1138_CQU_20131009
Descripcion     :   Se agrega llamada a la funci�n ObtenerTipoImagenPorRegistratioAccesibility
                    que devuelve el TTipoImagen seg�n un RegistrationAccessibility indicado,
                    de esta manera se podr� cargar la foto que seleccion� el operador
                    al validar la Infracci�n.

Fecha           :   30/10/2013
Firma           :   SS_1135_CQU_20131030
Descripcion     :   Se deben obtener las imagenes de infracciones desde el directorio en el que se graban
                    y no desde el directorio de las imagenes de tr�nsitos ya que este �ltimo podr�a ser borrado,
                    a diferencia del de infracciones que debe ser mantenido en el tiempo.

Fecha           :   12/12/2013
Firma           :   SS_1135_CQU_20131212
Descripcion     :   Se corrige un error eterno, no se deshabilitan los botones para anular y reEmitir cuando se cambia alg�n combo
                    o no existen datos en la grilla.
                    Se implementa que deshabilite los botones cuando se carguen los datos del disco y adem�s cuando se pinche el bot�n.

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

Fecha           :   17/09/2015
Firma           :   SS_1386_CQU_20150917
Descripcion     :   Cuando es un XML de VS se deben enviar el nombre separado
                    por Nombre, Apellido y ApellidoMaterno para ello se agregan los valores.
                    Se agerga detecci�n de la ConcesionariaNativa

}

unit frmReEmisionAnulacionDiscoXMLInfracciones;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ListBoxEx, DBListEx, StdCtrls, DmiCtrls, VariantComboBox, ExtCtrls,
  jpeg, // SS_1091_CQU_20130816
  ComCtrls, Validate, DateEdit, DB, ADODB;

type
  TReEmisionAnulacionDiscoXMLInfraccionesForm = class(TForm)
    GroupBox2: TGroupBox;
    cbConcesionaria: TVariantComboBox;
    GroupBox1: TGroupBox;
    cbTiposDisco: TVariantComboBox;
    GroupBox5: TGroupBox;
    Label1: TLabel;
    Label13: TLabel;
    peDirectorioImagenes: TPickEdit;
    peDirectorioXML: TPickEdit;
    Label2: TLabel;
    dblDiscos: TDBListEx;
    gbEstado: TGroupBox;
    lblArchivosGenerados: TLabel;
    lblInfraccionesRestantes: TLabel;
    Label3: TLabel;
    pbProgreso: TProgressBar;
    lsbxAvisos: TListBox;
    Bevel1: TBevel;
    btnReEmitir: TButton;
    btnSalir: TButton;
    btnAnular: TButton;
    Label5: TLabel;
    deFechaDesde: TDateEdit;
    Label4: TLabel;
    deFechaHasta: TDateEdit;
    btnActualizarDatosDiscos: TButton;
    Label6: TLabel;
    lblDiscosMostrados: TLabel;
    spObtenerDatosDiscoXMLInfractoresEmitidos: TADOStoredProc;
    dsObtenerDatosDiscoXMLInfractoresEmitidos: TDataSource;
    spCargarInfraccionesTransitosDiscoXMLEmitido: TADOStoredProc;
    spObtenerInfraccionesTransitosAEnviarMOP: TADOStoredProc;
    spObtenerInfraccionesAEnviarMOP: TADOStoredProc;
    fnObtenerNombreCortoConcesionaria: TADOStoredProc;
    spActualizarInfraccionesTransitosAEnviarMOP: TADOStoredProc;
    spActualizarDiscoXMLInfractoresReEmisiones: TADOStoredProc;
    spActualizarDiscoXMLInfractoresReEmisionesErrores: TADOStoredProc;
    fnSePuedeAnularDiscoXML: TADOStoredProc;
    spAnularDiscoXMLInfracciones: TADOStoredProc;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cbConcesionariaChange(Sender: TObject);
    procedure deFechaDesdeChange(Sender: TObject);
    procedure deFechaHastaChange(Sender: TObject);
    procedure cbTiposDiscoChange(Sender: TObject);
    procedure btnActualizarDatosDiscosClick(Sender: TObject);
    procedure dblDiscosDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure spObtenerDatosDiscoXMLInfractoresEmitidosAfterScroll(
      DataSet: TDataSet);
    procedure peDirectorioImagenesChange(Sender: TObject);
    procedure peDirectorioXMLChange(Sender: TObject);
    procedure peDirectorioImagenesButtonClick(Sender: TObject);
    procedure peDirectorioXMLButtonClick(Sender: TObject);
    procedure dblDiscosDrawBackground(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var DefaultDraw: Boolean);
    procedure btnReEmitirClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure btnSalirClick(Sender: TObject);
    procedure btnAnularClick(Sender: TObject);
  private
    { Private declarations }
    FCodigoConcesionaria,
    FTipoDisco,
    FCodigoOperacion : Integer;

    FFechaDesde,
    FFechaHasta: TDateTime;

    FNombreCortoConcesionaria: String;

    //FDirectorioImagenes: String   // SS-377-EBA-20110613
    FImagePathNFI : AnsiString;     // SS-377-EBA-20110613

    FDirectorioSalidaXML: String;

    FProcesando: Boolean;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    FCodigoNativa : Integer;                                                           // SS_1386_CQU_20150917
    procedure MuestraAviso(Aviso: String);
    function InicializarInterfase: Boolean;
    procedure ActualizarControles(Habilitar: Boolean);
    procedure RefrescaDatos;
    function CargarDatosDiscos(Concesionaria, TipoDisco: Integer; FechaDesde, FechaHasta: TDateTime): boolean;
    procedure InicializarDatosControles;
    function MostrarReporteFinalizacion(var DescriError: AnsiString): boolean;
    procedure ReEmitirDiscoXML;
    procedure AnulaDiscoXML(Concesinaria, TipoDisco, Disco: Integer);
  public
    { Public declarations }
    function Inicializar: boolean;
  end;

var
  ReEmisionAnulacionDiscoXMLInfraccionesForm: TReEmisionAnulacionDiscoXMLInfraccionesForm;

implementation

{$R *.dfm}

Uses
    DMConnection,
    PeaProcs,
    PeaProcsCN,
    SysUtilsCN,
    Util,
    UtilProc,
    ConstParametrosGenerales,
    MOPInfBind,
    ComunesInterfaces,
    PeaTypes,
    JPEGPlus,
    ImgTypes,
    ImgProcs,
    StrUtils,
    FrmRptEnvioInfraccionesXML;

procedure TReEmisionAnulacionDiscoXMLInfraccionesForm.MuestraAviso(Aviso: String);
begin
    lsbxAvisos.Items.Insert(0, FormatDateTime('dd/mm/yyyy hh:nn:ss - ', NowBase(DMConnections.BaseCAC)) + Aviso);
    lsbxAvisos.ItemIndex := 0;
    Application.ProcessMessages;
end;

function TReEmisionAnulacionDiscoXMLInfraccionesForm.InicializarInterfase: Boolean;
    resourcestring
        rsRegistrarOperacionTitulo = 'Registrar Operaci�n Interfase';
        rsInterfazRegistrada = 'Se registr� la Interfase con el c�digo %d';
    Var
    vError: String;
begin
    try
        Result := RegistrarOperacionEnLogInterface(
                    DMConnections.BaseCAC,
                    RO_MOD_INTERFAZ_SALIENTE_XML_INFRACCIONES,
                    '',
                    UsuarioSistema,
                    '',
                    False,
                    False,
                    NowBase(DMConnections.BaseCAC),
                    0,
                    FCodigoOperacion,
                    vError);
    Finally
        if Not Result then
            ShowMsgBoxCN(rsRegistrarOperacionTitulo, vError, MB_ICONERROR, Self)
        else MuestraAviso(Format(rsInterfazRegistrada,[FCodigoOperacion]));
    end;
end;


procedure TReEmisionAnulacionDiscoXMLInfraccionesForm.ActualizarControles(Habilitar: Boolean);
begin
    btnReEmitir.Enabled := Habilitar and (not spObtenerDatosDiscoXMLInfractoresEmitidos.FieldByName('Anulado').AsBoolean);
    btnAnular.Enabled := Habilitar and (not spObtenerDatosDiscoXMLInfractoresEmitidos.FieldByName('Anulado').AsBoolean);
    cbConcesionaria.Enabled := Habilitar;
    deFechaDesde.Enabled := Habilitar;
    deFechaHasta.Enabled := Habilitar;
    cbTiposDisco.Enabled := Habilitar;
    dblDiscos.Enabled := Habilitar;
    peDirectorioImagenes.Enabled := Habilitar;
    peDirectorioXML.Enabled := Habilitar;
end;

procedure TReEmisionAnulacionDiscoXMLInfraccionesForm.RefrescaDatos;
    var
        lPuntero: TBookmark;
begin
    with spObtenerDatosDiscoXMLInfractoresEmitidos do begin
        lPuntero := GetBookmark;
        Requery();
        GotoBookmark(lPuntero);
        FreeBookmark(lPuntero);
    end;
end;

procedure TReEmisionAnulacionDiscoXMLInfraccionesForm.cbConcesionariaChange(
  Sender: TObject);
begin
    FCodigoConcesionaria := cbConcesionaria.Value;

    with fnObtenerNombreCortoConcesionaria do begin
        Parameters.ParamByName('@CodigoConcesionaria').Value := FCodigoConcesionaria;
        ExecProc;
        FNombreCortoConcesionaria := Parameters.ParamByName('@RETURN_VALUE').Value;
    end;

    CargarDatosDiscos(FCodigoConcesionaria, FTipoDisco, FFechaDesde, FFechaHasta);
end;

procedure TReEmisionAnulacionDiscoXMLInfraccionesForm.cbTiposDiscoChange(
  Sender: TObject);
begin
    FTipoDisco := cbTiposDisco.Value;
    CargarDatosDiscos(FCodigoConcesionaria, FTipoDisco, FFechaDesde, FFechaHasta);
end;

procedure TReEmisionAnulacionDiscoXMLInfraccionesForm.dblDiscosDrawBackground(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var DefaultDraw: Boolean);
begin
    if spObtenerDatosDiscoXMLInfractoresEmitidos.Active then

    if spObtenerDatosDiscoXMLInfractoresEmitidos.FieldByName('Anulado').AsBoolean then begin
        Sender.Canvas.Font.Style := Sender.Canvas.Font.Style + [fsBold];
        if (odSelected in State) then
            if (odFocused in State) then
                Sender.Canvas.Brush.Color := clRed
            else Sender.Canvas.Brush.Color := clGray;
    end;
end;

procedure TReEmisionAnulacionDiscoXMLInfraccionesForm.dblDiscosDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
    const
        cColumnasNumericas = 'CodigoDisco*TotalArchivos*TotalInfracciones*TotalReEmisiones*CodigoOperacionInterfase';
        cColumnasFecha = 'FechaDesde*FechaHasta';
        cComlumasFechaHora = 'FechaHora';
begin
    if Pos(Column.FieldName, cComlumasFechaHora) <> 0 then
        Text := FormatDateTime('dd/mm/yyyy - hh:nn:ss', spObtenerDatosDiscoXMLInfractoresEmitidos.FieldByName(Column.FieldName).AsDateTime);

    if Pos(Column.FieldName, cColumnasFecha) <> 0 then
        Text := FormatDateTime('dd/mm/yyyy', spObtenerDatosDiscoXMLInfractoresEmitidos.FieldByName(Column.FieldName).AsDateTime);

    if Column.FieldName = 'Anulado' Then
        if spObtenerDatosDiscoXMLInfractoresEmitidos.FieldByName('Anulado').AsBoolean then
            Text := 'S�'
        else Text := 'No';

    if Pos(Column.FieldName, cColumnasNumericas) <> 0 then
        Text := FloatToStrF(spObtenerDatosDiscoXMLInfractoresEmitidos.FieldByName(Column.FieldName).AsFloat, ffNumber, 15, 0) + ' ';

    if spObtenerDatosDiscoXMLInfractoresEmitidos.FieldByName('Anulado').AsBoolean then begin
        Sender.Canvas.Font.Style := Sender.Canvas.Font.Style + [fsBold];
        if not (odSelected in State) then
            Sender.Canvas.Font.Color := clRed;
    end
    else if not (odFocused in State) then
        Sender.Canvas.Font.Color := clBlack;
end;

procedure TReEmisionAnulacionDiscoXMLInfraccionesForm.deFechaDesdeChange(
  Sender: TObject);
begin
    FFechaDesde := deFechaDesde.Date;
    InicializarDatosControles;
end;

procedure TReEmisionAnulacionDiscoXMLInfraccionesForm.deFechaHastaChange(
  Sender: TObject);
begin
    FFechaHasta := deFechaHasta.Date;
    InicializarDatosControles;
end;

procedure TReEmisionAnulacionDiscoXMLInfraccionesForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TReEmisionAnulacionDiscoXMLInfraccionesForm.FormCloseQuery(
  Sender: TObject; var CanClose: Boolean);
begin
    CanClose := not FProcesando;
end;

procedure TReEmisionAnulacionDiscoXMLInfraccionesForm.InicializarDatosControles;
begin
    if spObtenerDatosDiscoXMLInfractoresEmitidos.Active then spObtenerDatosDiscoXMLInfractoresEmitidos.Close;
    lblDiscosMostrados.Caption := '0';
    btnActualizarDatosDiscos.Enabled := True;
    btnAnular.Enabled := False;
    btnreEmitir.Enabled := False;
end;

{
 Revision	:	1
 Author		: 	ebaeza
 Date		:	14/06/2011
 firma      :   SS-377-EBA-20110613
 Descripcion:	Cambio estructura directorios de acceso a imagenes de transitos
}
procedure TReEmisionAnulacionDiscoXMLInfraccionesForm.peDirectorioImagenesButtonClick(Sender: TObject);
    resourcestring
        rsSeleccioneUbicacion = 'Seleccione el Directorio que las Im�genes';
    var
        lDirectorio : String;
begin
    lDirectorio := Trim(BrowseForFolder(rsSeleccioneUbicacion));
    if lDirectorio <> '' then begin
        peDirectorioImagenes.Text := lDirectorio;
        //FDirectorioImagenes := lDirectorio;       // SS-377-EBA-20110613
        FImagePathNFI := lDirectorio;               // SS-377-EBA-20110613
    end;
end;

{
 Revision	:	1
 Author		: 	ebaeza
 Date		:	14/06/2011
 firma      :   SS-377-EBA-20110613
 Descripcion:	Cambio estructura directorios de acceso a imagenes de transitos
}
procedure TReEmisionAnulacionDiscoXMLInfraccionesForm.peDirectorioImagenesChange(
  Sender: TObject);
begin
    //FDirectorioImagenes := peDirectorioImagenes.Text;     // SS-377-EBA-20110613
    FImagePathNFI := peDirectorioImagenes.Text;             // SS-377-EBA-20110613
end;

procedure TReEmisionAnulacionDiscoXMLInfraccionesForm.peDirectorioXMLButtonClick(Sender: TObject);
    resourcestring
        rsSeleccioneUbicacion = 'Seleccione el Directorio de Salida para los Discos XML';
    var
        lDirectorio : String;
begin
    lDirectorio := Trim(BrowseForFolder(rsSeleccioneUbicacion));
    if lDirectorio <> '' then begin
        peDirectorioXML.Text := lDirectorio;
        FDirectorioSalidaXML := GoodDir(lDirectorio);
    end;
end;

procedure TReEmisionAnulacionDiscoXMLInfraccionesForm.peDirectorioXMLChange(
  Sender: TObject);
begin
    FDirectorioSalidaXML := peDirectorioXML.Text;
end;

procedure TReEmisionAnulacionDiscoXMLInfraccionesForm.spObtenerDatosDiscoXMLInfractoresEmitidosAfterScroll(
  DataSet: TDataSet);
begin
    if DataSet.Active then begin
        btnAnular.Enabled := not DataSet.FieldByName('Anulado').AsBoolean;
        btnReEmitir.Enabled := not DataSet.FieldByName('Anulado').AsBoolean;
    end
    else begin
        btnAnular.Enabled := False;
        btnReEmitir.Enabled := False;
    end;

end;

procedure TReEmisionAnulacionDiscoXMLInfraccionesForm.btnActualizarDatosDiscosClick(
  Sender: TObject);
begin
    CargarDatosDiscos(FCodigoConcesionaria, FTipoDisco, FFechaDesde, FFechaHasta);
end;

procedure TReEmisionAnulacionDiscoXMLInfraccionesForm.btnAnularClick(Sender: TObject);
    resourcestring
        rsTituloAnulacion = 'Anulaci�n de Disco XML Infractores';
        rsPreguntaAnulacion =
            'Disco Seleccionado:' + CRLF + CRLF +
            'Concesionaria: %s' + CRLF +
            'Tipo Disco: %s' + CRLF +
            'Disco N�mero: %d' + CRLF + CRLF +
            '� Esta seguro de Anular el Disco Seleccionado ?';
        rsNoexistenRegistros = 'No existen Discos para anular';                     // SS_1135_CQU_20131212
begin
    if dblDiscos.DataSource.DataSet.RecordCount = 0 then begin                      // SS_1135_CQU_20131212
        ShowMsgBoxCN(rsTituloAnulacion, rsNoexistenRegistros, MB_ICONERROR, Self);  // SS_1135_CQU_20131212
        btnAnular.Enabled := False;                                                 // SS_1135_CQU_20131212
        Exit;                                                                       // SS_1135_CQU_20131212
    end;                                                                            // SS_1135_CQU_20131212
    
    try
        with fnSePuedeAnularDiscoXML do begin
            Parameters.ParamByName('@CodigoConcesionaria').Value := spObtenerDatosDiscoXMLInfractoresEmitidos.FieldByName('CodigoConcesionaria').AsInteger;
            Parameters.ParamByName('@CodigoTipo').Value := spObtenerDatosDiscoXMLInfractoresEmitidos.FieldByName('CodigoTipo').AsInteger;
            Parameters.ParamByName('@CodigoDisco').Value := spObtenerDatosDiscoXMLInfractoresEmitidos.FieldByName('CodigoDisco').AsInteger;
            ExecProc;
        end;

        if fnSePuedeAnularDiscoXML.Parameters.ParamByName('@RETURN_VALUE').Value then
            if ShowMsgBoxCN(
                rsTituloAnulacion,
                Format(
                    rsPreguntaAnulacion,
                    [cbConcesionaria.Text,
                     cbTiposDisco.Text,
                     spObtenerDatosDiscoXMLInfractoresEmitidos.FieldByName('CodigoDisco').AsInteger]),
                MB_ICONQUESTION,
                Self) = mrOk then
                    AnulaDiscoXML(
                        spObtenerDatosDiscoXMLInfractoresEmitidos.FieldByName('CodigoConcesionaria').AsInteger,
                        spObtenerDatosDiscoXMLInfractoresEmitidos.FieldByName('CodigoTipo').AsInteger,
                        spObtenerDatosDiscoXMLInfractoresEmitidos.FieldByName('CodigoDisco').AsInteger);
    except
        on e:Exception do ShowMsgBoxCN(e, Self);
    end;
end;

procedure TReEmisionAnulacionDiscoXMLInfraccionesForm.btnReEmitirClick(Sender: TObject);
    resourcestring
        rsParametrosSeleccionados =
            'Par�metros Seleccionados:' + CRLF + CRLF +
            'Directorio de Imagenes: %s' + CRLF +
            'Directorio Destino Archivos XML: %s' + CRLF + CRLF;

        rsMensajeConfirmacion = '� Est� seguro de lanzar el proceso con los par�metros especificados ?';
        rsTitulo = 'ReEmisi�n Disco XML Infractores';
        rsNoexistenRegistros = 'No existen Discos para ReEmitir';          // SS_1135_CQU_20131212
begin
    if dblDiscos.DataSource.DataSet.RecordCount = 0 then begin             // SS_1135_CQU_20131212
        ShowMsgBoxCN(rsTitulo, rsNoexistenRegistros, MB_ICONERROR, Self);  // SS_1135_CQU_20131212
        btnReEmitir.Enabled := False;                                      // SS_1135_CQU_20131212
        Exit;                                                              // SS_1135_CQU_20131212
    end;                                                                   // SS_1135_CQU_20131212

    if ShowMsgBoxCN(
                        rsTitulo,
                        Format(rsParametrosSeleccionados,
                                [peDirectorioImagenes.Text,
                                 peDirectorioXML.Text])
                            + rsMensajeConfirmacion ,
                        MB_ICONQUESTION,
                        Self) = mrOk then
        ReEmitirDiscoXML;
end;

function TReEmisionAnulacionDiscoXMLInfraccionesForm.CargarDatosDiscos(Concesionaria: Integer; TipoDisco: Integer; FechaDesde: TDateTime; FechaHasta: TDateTime): boolean;
    resourcestring
        rsErrorTitulo = 'Carga Datos Discos XML';
        rsErrorDescripcion = 'Se produjo un Error inesperado al cargar los Datos de los Discos XML Emitidos.' + CRLF + CRLF + 'Error: %s';
begin
    try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;
        try
            Result := False;

            with spObtenerDatosDiscoXMLInfractoresEmitidos do begin
                //if Active then Close;                                     // SS_1135_CQU_20131212
                if Active then begin                                        // SS_1135_CQU_20131212
                    Close;                                                  // SS_1135_CQU_20131212
                    btnReEmitir.Enabled := False;                           // SS_1135_CQU_20131212
                    btnAnular.Enabled := False;                             // SS_1135_CQU_20131212
                end;                                                        // SS_1135_CQU_20131212

                with Parameters do begin
                    ParamByName('@Concesionaria').Value := Concesionaria;
                    ParamByName('@CodigoTipo').Value    := TipoDisco;
                    ParamByName('@FechaDesde').Value    := FechaDesde;
                    ParamByName('@FechaHasta').Value    := FechaHasta;
                end;
                Open;

                lblDiscosMostrados.Caption := IntToStr(RecordCount);
            end;

            btnActualizarDatosDiscos.Enabled := False;
            Result := True;
        except
            on e: Exception do ShowMsgBoxCN(rsErrorTitulo, Format(rsErrorDescripcion,[e.Message]), MB_ICONERROR, Self);
        end;
    finally
        Screen.Cursor := crDefault;
        Application.ProcessMessages;
    end;
end;

{
 Revision	:	1
 Author		: 	ebaeza
 Date		:	14/06/2011
 firma      :   SS-377-EBA-20110613
 Descripcion:	Cambio estructura directorios de acceso a imagenes de transitos
}
function TReEmisionAnulacionDiscoXMLInfraccionesForm.Inicializar: boolean;
    resourcestring
        rsErrorTituloGeneral = 'Inicializar ReEmisi�n/Anulaci�n Discos XML';
        rsErrorDescripcion   = 'Se produjo un Error inesperado al Inicializar el formulario.' + CRLF + CRLF + 'Error: %s';
        rsErrorParametosGenerales = 'Carga de Par�metros Generales.' + CRLF + CRLF + 'Se produjo un error al intentar cargar el Par�metro General: %s.';
begin
    try
        FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
        FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
        FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
        FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
        Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        try
            Result := False;

{            if not ObtenerParametroGeneral(DMConnections.BaseCAC, DIR_IMAGENES_TRANSITOS, FDirectorioImagenes) then                                    // SS-377-EBA-20110613
                raise EErrorExceptionCN.Create(rsErrorTituloGeneral, Format(rsErrorParametosGenerales, [DIR_IMAGENES_TRANSITOS]))                       // SS-377-EBA-20110613
            else peDirectorioImagenes.Text := FDirectorioImagenes; }                                                                                    // SS-377-EBA-20110613

            //if not ObtenerParametroGeneral(DMConnections.BaseCAC, DIR_IMAGENES_TRANSITOS_NUEVO_FORMATO_IMAGEN, FImagePathNFI) then                       // SS_1135_CQU_20131030  // SS-377-EBA-20110613
            //    raise EErrorExceptionCN.Create(rsErrorTituloGeneral, Format(rsErrorParametosGenerales, [DIR_IMAGENES_TRANSITOS_NUEVO_FORMATO_IMAGEN]))   // SS_1135_CQU_20131030  // SS-377-EBA-20110613
            //else peDirectorioImagenes.Text := FImagePathNFI;                                                                                             // SS_1135_CQU_20131030  // SS-377-EBA-20110613

            if not ObtenerParametroGeneral(DMConnections.BaseCAC, DIR_IMAGENES_TRANSITOS_INFRACTORES, FImagePathNFI) then                                  // SS_1135_CQU_20131030
                raise EErrorExceptionCN.Create(rsErrorTituloGeneral, Format(rsErrorParametosGenerales, [DIR_IMAGENES_TRANSITOS_INFRACTORES]))              // SS_1135_CQU_20131030
            else peDirectorioImagenes.Text := FImagePathNFI;                                                                                               // SS_1135_CQU_20131030

            if not ObtenerParametroGeneral(DMConnections.BaseCAC, DIR_DESTINO_XML_INFRACCIONES, FDirectorioSalidaXML) then
                raise EErrorExceptionCN.Create(rsErrorTituloGeneral, Format(rsErrorParametosGenerales, [DIR_DESTINO_XML_INFRACCIONES]))
            else peDirectorioXML.Text := FDirectorioSalidaXML;

            CargarComboConcesionarias(DMConnections.BaseCAC, cbConcesionaria, False);
            FCodigoConcesionaria := cbConcesionaria.Value;
            FCodigoNativa := ObtenerCodigoConcesionariaNativa;                      // SS_1386_CQU_20150917

            with fnObtenerNombreCortoConcesionaria do begin
                Parameters.ParamByName('@CodigoConcesionaria').Value := FCodigoConcesionaria;
                ExecProc;
                FNombreCortoConcesionaria := Trim(Parameters.ParamByName('@RETURN_VALUE').Value);
            end;

            deFechaDesde.Date := IncMonth(Date, -3) - (Day(Date) - 1);
            deFechaHasta.Date := Date;

            with cbTiposDisco do begin
                Items.Add('Infracciones Sin Regularizar', 1);
                Items.Add('Infracciones Regularizadas', 2);
                Items.Add('Infracciones Por Morosidad', 3); // SS_660_CQU_20121010
                ItemIndex := 0;
                FTipoDisco := Value;
            end;

            FProcesando := False;

            Result := CargarDatosDiscos(FCodigoConcesionaria, FTipoDisco, FFechaDesde, FFechaHasta);
        except
            on e: Exception do ShowMsgBoxCN(rsErrorTituloGeneral, Format(rsErrorDescripcion,[e.Message]), MB_ICONERROR, Self);
        end;
    finally
        Screen.Cursor := crDefault;
        Application.ProcessMessages;
    end;
end;

function TReEmisionAnulacionDiscoXMLInfraccionesForm.MostrarReporteFinalizacion(var DescriError: AnsiString): boolean;
    var
        f: TfrmRptEnvioXMLInfracciones;
begin
    Result := False;
    DescriError := '';

    try
        Application.CreateForm(TfrmRptEnvioXMLInfracciones, f);
        Result := f.Inicializar(0, FCodigoOperacion, Trim(cbConcesionaria.Text), DescriError);
    except
        on e: Exception do DescriError := e.Message;
    end;
end;

procedure TReEmisionAnulacionDiscoXMLInfraccionesForm.ReEmitirDiscoXML;
    var
        XMLDenuncios : IXMLDenunciosType;

        lArchivoEnCurso,
        lTipoDisco,
        lDisco,
        lReEmisiones,
        lRegistrosRefresco,
        lArchivosGenerados: Integer;

        lError: String;

    resourcestring
        rsTituloValidaciones = 'Validaci�n Directorios';
        rsTituloGeneracionXML = 'ReEmisi�n Disco XML';
        rsErrorControl_peDirectorioImagenes = 'No existe o No se tiene acceso al directorio de las Im�genes.';
        rsErrorControl_peDirectorioXML = 'No existe o No se tiene acceso al directorio de las Im�genes.';
        rsErrorGeneracionDiscoXML = 'Se produjo un error durante la ReEmisi�n del Disco XML.' + CRLF + CRLF + 'Error: /s';



    function ValidarCondiciones: boolean;
        var
            vControles  : Array [1..1] of TControl;
            vCondiciones: Array [1..1] of Boolean;
            vMensajes   : Array [1..1] of String;
    begin
//        vControles[1]   := peDirectorioImagenes;
        vControles[1]   := peDirectorioXML;

//        vCondiciones[1] := DirectoryExists(peDirectorioImagenes.Text);
        vCondiciones[1] := DirectoryExists(peDirectorioXML.Text);

//        vMensajes[1]    := rsErrorControl_peDirectorioImagenes;
        vMensajes[1]    := rsErrorControl_peDirectorioXML;

        Result := ValidateControlsCN(vControles, vCondiciones, rsTituloValidaciones, vMensajes, Self);
    end;

    function CargarDatosDiscoXML: Boolean;
        resourcestring
            rsDatosCargados = 'Se Cargaron los Datos a procesar';
    begin
        try
            Result := False;
            with spCargarInfraccionesTransitosDiscoXMLEmitido do begin
                Parameters.ParamByName('@CodigoConcesionaria').Value :=  spObtenerDatosDiscoXMLInfractoresEmitidos.FieldbyName('CodigoConcesionaria').AsInteger;
                Parameters.ParamByName('@CodigoTipo').Value := spObtenerDatosDiscoXMLInfractoresEmitidos.FieldbyName('CodigoTipo').AsInteger;
                Parameters.ParamByName('@CodigoDisco').Value := spObtenerDatosDiscoXMLInfractoresEmitidos.FieldbyName('CodigoDisco').AsInteger;
                ExecProc;

            end;
            MuestraAviso(rsDatosCargados);
            Result := True;
        except
            on e: Exception do ShowMsgBoxCN(e, Self);
        end;
    end;

    function GuardarArchivoXML: boolean;
        resourcestring
            rsErrorGuardarArchivoXML = 'Se produjo un Error';
        var
            vCarpetaDestino,
            vNombreArchivoTemp,
            vNombreArchivo,
            vMensajeTipoDisco: String;  // SS_660_CQU_20121010

        function NombreCarpetaXMLConcesionaria(DirectorioDestino, NombreCortoConcesionaria: String; Regularizadas: Boolean): String;
        begin
            case Regularizadas of
                False:
                    Result := GoodDir(GoodDir(GoodDir(DirectorioDestino) + NombreCortoConcesionaria) + 'SinRegularizar');
                True:
                    Result := GoodDir(GoodDir(GoodDir(DirectorioDestino) + NombreCortoConcesionaria) + 'Regularizadas');
            end;
        end;

        function NombreCarpetaXMLConcesionariaMorosidad(DirectorioDestino, NombreCortoConcesionaria: String): String;   // SS_660_CQU_20121010
        begin                                                                                                           // SS_660_CQU_20121010
            Result := GoodDir(GoodDir(GoodDir(DirectorioDestino) + NombreCortoConcesionaria) + 'PorMorosidad');         // SS_660_CQU_20121010
        end;                                                                                                            // SS_660_CQU_20121010

    begin
        try
            Result := False;

            try
                case lTipoDisco of
                    //1: vCarpetaDestino := NombreCarpetaXMLConcesionaria(FDirectorioSalidaXML, FNombreCortoConcesionaria, False);      // SS_660_CQU_20121010
                    //2: vCarpetaDestino := NombreCarpetaXMLConcesionaria(FDirectorioSalidaXML, FNombreCortoConcesionaria, True);       // SS_660_CQU_20121010
                    1: begin                                                                                                            // SS_660_CQU_20121010
                        vCarpetaDestino     := NombreCarpetaXMLConcesionaria(FDirectorioSalidaXML, FNombreCortoConcesionaria, False);   // SS_660_CQU_20121010
                        vMensajeTipoDisco   := '"Infracciones Sin Regularizar"';                                                        // SS_660_CQU_20121010
                    end;                                                                                                                // SS_660_CQU_20121010
                    2: begin                                                                                                            // SS_660_CQU_20121010
                        vCarpetaDestino     := NombreCarpetaXMLConcesionaria(FDirectorioSalidaXML, FNombreCortoConcesionaria, True);    // SS_660_CQU_20121010
                        vMensajeTipoDisco   := '"Infracciones Regularizadas"';                                                          // SS_660_CQU_20121010
                    end;                                                                                                                // SS_660_CQU_20121010
                    3: begin                                                                                                            // SS_660_CQU_20121010
                        vCarpetaDestino     := NombreCarpetaXMLConcesionariaMorosidad(FDirectorioSalidaXML, FNombreCortoConcesionaria); // SS_660_CQU_20121010
                        vMensajeTipoDisco   := '"Infracciones Por Morosidad"';                                                          // SS_660_CQU_20121010
                    end;                                                                                                                // SS_660_CQU_20121010
                end;

                if not DirectoryExists(vCarpetaDestino) then
                    if not ForceDirectories(vCarpetaDestino) then raise Exception.Create('No se pudo crear la Carpeta Temporal para el Disco XML.');

                vNombreArchivoTemp := GoodDir(vCarpetaDestino) + TempFile;

                if Guardar(XMLDenuncios, vNombreArchivoTemp) then begin

                    vCarpetaDestino := NombreCarpetaXML(GoodDir(vCarpetaDestino), lDisco, lReEmisiones);

                    if not DirectoryExists(vCarpetaDestino) then
                        if Not ForceDirectories(vCarpetaDestino) then
                            raise Exception.Create('No se pudo crear la Carpeta Destino para el Disco XML.')
                        //else MuestraAviso(Format('Se cre� un Disco Nuevo de %s. Disco Nro. %d',[iif(lTipoDisco = 1, '"Infracciones Sin Regularizar"', '"Infracciones Regularizadas"'), lDisco]));    // SS_660_CQU_20121010
                        else MuestraAviso(Format('Se cre� un Disco Nuevo de %s. Disco Nro. %d',[vMensajeTipoDisco, lDisco]));                                                                          // SS_660_CQU_20121010


                    if not FileExists(GoodDir(vCarpetaDestino) + 'Denuncios.DTD') then
                        GenerarDTD(vCarpetaDestino);

                    vNombreArchivo  := NombreArchivoXML(vCarpetaDestino, lDisco, lArchivoEnCurso, NowBase(DMConnections.BaseCAC), FCodigoConcesionaria);

                    if FileExists(vNombreArchivo) then DeleteFile(vNombreArchivo);

                    if RenameFile(vNombreArchivoTemp, vNombreArchivo) then begin

                        XMLDenuncios.Clear;

                        Inc(lArchivosGenerados);
                        lblArchivosGenerados.Caption := IntToStr(lArchivosGenerados);
                        Application.ProcessMessages;

                        Result := True;
                    end;

                    if FileExists(vNombreArchivoTemp) then DeleteFile(vNombreArchivoTemp);
                end;
            except
                raise;
            end;
        finally

        end;
    end;

    Function ActualizarDatosXMLTemp(TipoDisco, CodigoInfraccion, Disco, Archivo: Integer; Error: String): Boolean;
    begin
        try
            Result := False;
            with spActualizarInfraccionesTransitosAEnviarMOP do begin
                Parameters.ParamByName('@CodigoTipo').Value            := TipoDisco;
                Parameters.ParamByName('@CodigoDisco').Value           := Disco;
                Parameters.ParamByName('@CodigoArchivo').Value         := Archivo;
                Parameters.ParamByName('@CodigoInfraccion').Value      := CodigoInfraccion;
                Parameters.ParamByName('@CodigoDiscoAsignado').Value   := NULL;
                Parameters.ParamByName('@CodigoArchivoAsignado').Value := NULL;
                Parameters.ParamByName('@Error').Value                 := iif(Trim(Error) = '', NULL, Trim(Error));
                ExecProc;
            end;
            Result := True;
        except
            raise;
        end;
    end;

    function ProcesarInfraccion: Boolean;

        function DatosInfraccionCompletos: Boolean;
            resourcestring
                rsDatosIncompletos = 'C�digo Infracci�n: %d, Patente: %s. Error: Datos Incompletos.';
        begin
            with spObtenerInfraccionesTransitosAEnviarMOP do begin
                Result :=
                    (trim(FieldByName('Nombre').AsString) <> '') and
                    (trim(FieldByName('NumeroDocumento').AsString) <> '') and
                    (trim(FieldByName('Domicilio').AsString) <> '') and
                    (trim(FieldByName('Comuna').AsString) <> '') and
                    (trim(FieldByName('Patente').AsString) <> '') and
                    (trim(FieldByName('PatenteDV').AsString) <> '') and
                    (trim(FieldByName('TipoVehiculo').AsString) <> '') and
                    (trim(FieldByName('Marca').AsString) <> '');

                if not Result then
                    ActualizarDatosXMLTemp(
                                            lTipoDisco,
                                            FieldByName('CodigoInfraccion').AsInteger,
                                            lDisco,
                                            lArchivoEnCurso,
                                            Format(rsDatosIncompletos,
                                                [FieldByName('CodigoInfraccion').AsInteger,
                                                 iif(FieldByName('Patente').IsNull, 'Sin Patente', Trim(FieldByName('Patente').AsString))]));
            end;
        end;

        function EsImagenValida: boolean;
            resourcestring
                rsImagenNoValida = 'C�digo Infracci�n: %d, Patente: %s. Error: Imagen de Referencia Anulada o NO V�lida.';
        begin
            with spObtenerInfraccionesTransitosAEnviarMOP do begin
                Result :=
                    (not FieldByName('TransitoAnulado').IsNull) and (not (FieldByName('TransitoAnulado').AsInteger = 1));

                if not Result then
                    ActualizarDatosXMLTemp(
                                            lTipoDisco,
                                            FieldByName('CodigoInfraccion').AsInteger,
                                            lDisco,
                                            lArchivoEnCurso,
                                            Format(rsImagenNoValida,
                                                [FieldByName('CodigoInfraccion').AsInteger,
                                                 iif(FieldByName('Patente').IsNull, 'Sin Patente', Trim(FieldByName('Patente').AsString))]));
            end;
        end;

        function AlmacenarInfraccionXML: boolean;
            resourcestring
                rsImagenNoValida = 'C�digo Infracci�n: %d, Patente: %s. No se pudo recuperar la Imagen de Referencia. Error: %s';
            var
                vImagenTransito: TJPEGPlusImage;
                vRefNumCorrCA: Int64;
                vTipoImagen: TTipoImagen;
                vDataImage: TDataImage;
                vTipoErrorImagen: TTipoErrorImg;
                vFechaHoraTrx: TDateTime;
                vStrStream: TStringStream;
                vStrImagenCodificada: AnsiString;
                vError: AnsiString;
                vImagenTransitoItaliano : TBitmap;              // SS_1091_CQU_20130516
                vEsItaliano : Boolean;                          // SS_1091_CQU_20130516
                vContinuar  : Boolean;                          // SS_1091_CQU_20130516
                vImagenJpeg : TJPEGImage;                       // SS_1091_CQU_20130816
        begin
            Result := False;

            try
                try
                    vImagenTransito := TJPEGPlusImage.Create;
                    vRefNumCorrCA   := spObtenerInfraccionesTransitosAEnviarMOP.FieldByName('RefNumCorrCA').AsInteger;
                    vFechaHoraTrx   := spObtenerInfraccionesTransitosAEnviarMOP.FieldByName('RefFechaHoraTransito').AsDateTime;

                    //if (spObtenerInfraccionesTransitosAEnviarMOP.FieldByName('RefRegistrationAccessibility').asInteger = 1) then                                              // SS_1138_CQU_20131009
                    //    vTipoImagen := tiFrontal                                                                                                                              // SS_1138_CQU_20131009
                    //else vTipoImagen := tiFrontal2;                                                                                                                           // SS_1138_CQU_20131009
                    vTipoImagen := ObtenerTipoImagenPorRegistratioAccesibility(spObtenerInfraccionesTransitosAEnviarMOP.FieldByName('RefRegistrationAccessibility').AsInteger); // SS_1138_CQU_20131009

                    vStrStream              := TStringStream.Create('');                                                            // SS_1091_CQU_20130516
                    vImagenTransitoItaliano := TBitmap.Create;                                                                      // SS_1091_CQU_20130516
                    vEsItaliano             := spObtenerInfraccionesTransitosAEnviarMOP.FieldByName('EsItaliano').AsBoolean;        // SS_1091_CQU_20130516
                    // Valido previamente si es italiano y cargo la imagen que corresponda, coloco una variable para saber          // SS_1091_CQU_20130516
                    // si carg� o no la imagen.                                                                                     // SS_1091_CQU_20130516
                    if ObtenerImagenTransitoInfractor(FImagePathNFI, FNombreCortoConcesionaria, vRefNumCorrCA,                      // SS_1035_CQU_20131030
                    //if vEsItaliano and ObtenerImagenTransito(FImagePathNFI, FNombreCortoConcesionaria, vRefNumCorrCA,             // SS_1135_CQU_20131030 // SS_1091_CQU_20130516
                            vFechaHoraTrx, vTipoImagen, vImagenTransitoItaliano, vDataImage, vError, vTipoErrorImagen) then begin   // SS_1091_CQU_20130516
                                                                                                                                    // SS_1091_CQU_20130516
                        //vImagenTransitoItaliano.SaveToStream(vStrStream);                                                         // SS_1091_CQU_20130816 // SS_1091_CQU_20130516
                        vImagenJpeg := TJPEGImage.Create;                                                                           // SS_1091_CQU_20130816
                        vImagenJpeg.Assign(vImagenTransitoItaliano);                                                                // SS_1091_CQU_20130816
                        vImagenJpeg.SaveToStream(vStrStream);                                                                       // SS_1091_CQU_20130816
                        vContinuar := True;                                                                                         // SS_1091_CQU_20130516
                                                                                                                                    // SS_1091_CQU_20130516
                    end else                                                                                                        // SS_1091_CQU_20130516
                    if ObtenerImagenTransitoInfractor(FImagePathNFI, FNombreCortoConcesionaria, vRefNumCorrCA, vFechaHoraTrx,       // SS_1135_CQU_20131030
                    //if ObtenerImagenTransito(FImagePathNFI, FNombreCortoConcesionaria, vRefNumCorrCA, vFechaHoraTrx,              // SS_1135_CQU_20131030 // SS_1091_CQU_20130516
                            vTipoImagen, vImagenTransito, vDataImage, vError, vTipoErrorImagen) then begin                          // SS_1091_CQU_20130516
                                                                                                                                    // SS_1091_CQU_20130516
                        vImagenTransito.SaveToStream(vStrStream);                                                                   // SS_1091_CQU_20130516
                        vContinuar := True;                                                                                         // SS_1091_CQU_20130516
                                                                                                                                    // SS_1091_CQU_20130516
                    end else                                                                                                        // SS_1091_CQU_20130516
                        vContinuar := False;                                                                                        // SS_1091_CQU_20130516

                    if vContinuar then begin                                                                                        // SS_1091_CQU_20130516
                    //if ObtenerImagenTransito(FImagePathNFI, FNombreCortoConcesionaria, vRefNumCorrCA, vFechaHoraTrx, vTipoImagen, vImagenTransito, vDataImage, vError, vTipoErrorImagen) then begin         // SS-377-EBA-20110613    // SS_1091_CQU_20130516
//                    if ObtenerImagenTransito(FDirectorioImagenes, vRefNumCorrCA, vFechaHoraTrx, vTipoImagen, vImagenTransito, vDataImage, vError, vTipoErrorImagen) then begin                           // SS-377-EBA-20110613
//                    if ObtenerImagenTransito(FDirectorioImagenes, 9840341, vFechaHoraTrx, vTipoImagen, vImagenTransito, vDataImage, vError, vTipoErrorImagen) then begin
                        //vStrStream := TStringStream.Create('');                                                                   // SS_1091_CQU_20130516
                        //vImagenTransito.SaveToStream(vStrStream);                                                                 // SS_1091_CQU_20130516
                        vStrImagenCodificada := ImageURLEncode(vStrStream.DataString);                                              // SS_1091_CQU_20130516

                        XMLDenuncios.Add;

                        with XMLDenuncios.Infraccion[XMLDenuncios.Count - 1] do begin
                            Concesionaria := FCodigoConcesionaria;

                            with spObtenerInfraccionesTransitosAEnviarMOP do begin
                                NumInfraccion             := FieldByName('CodigoInfraccion').AsInteger;
                                TipoInfraccion            := FieldByName('CodigoTipoInfraccion').AsInteger; // SS_660_CQU_20130816

                                Infractor.Nombre          := Trim(FieldByName('Nombre').AsString);
                                //Infractor.ApellidoPaterno := '';                                              // SS_1386_CQU_20150917
                                //Infractor.ApellidoMaterno := '';                                              // SS_1386_CQU_20150917
                                if FCodigoNativa <> CODIGO_VS then begin                                        // SS_1386_CQU_20150917
                                    Infractor.ApellidoPaterno := '';                                            // SS_1386_CQU_20150917
                                    Infractor.ApellidoMaterno := '';                                            // SS_1386_CQU_20150917
                                end else begin                                                                  // SS_1386_CQU_20150917
                                    Infractor.ApellidoPaterno := Trim(FieldByName('Apellido').AsString);        // SS_1386_CQU_20150917
                                    Infractor.ApellidoMaterno := Trim(FieldByName('ApellidoMaterno').AsString); // SS_1386_CQU_20150917
                                end;                                                                            // SS_1386_CQU_20150917
                                Infractor.Rut             := Copy(Trim(FieldByName('NumeroDocumento').AsString), 1, Length(Trim(FieldByName('NumeroDocumento').AsString)) - 1);
                                Infractor.DV              := RightStr(Trim(FieldByName('NumeroDocumento').AsString), 1);
                                Infractor.Domicilio       := Trim(FieldByName('Domicilio').AsString);
                                Infractor.Comuna          := Trim(FieldByName('Comuna').AsString);

                                Vehiculo.Patente      := Trim(FieldByName('Patente').AsString);
                                Vehiculo.DV           := Trim(FieldByName('PatenteDV').AsString);
                                Vehiculo.TipoVehiculo := Trim(FieldByName('TipoVehiculo').AsString);
                                Vehiculo.Marca        := Trim(FieldByName('Marca').AsString);
                                Vehiculo.Modelo       := Trim(FieldByName('Modelo').AsString);
                                Vehiculo.Agno         := FieldByName('Anio').AsInteger;
                                Vehiculo.Color        := Trim(FieldByName('Color').AsString);

                                Fotografia.Imagen := vStrImagenCodificada;                    // SS_1091_CQU_20130516

                                while not Eof do begin

                                    Transaccion.Add;
                                    with Transaccion[Transaccion.Count - 1] do begin
                                        Ident_PC            := FieldByName('IdentPC').AsString;
                                        Num_Corr_Punto      := FieldByName('NumCorrPunto').AsInteger;
                                        Time_PC             := FieldByName('TimePC').AsInteger;
                                        Fecha_transaccion   := FormatDateTime('dd"/"mm"/"yyyy', FieldByName('FechaHoraTransito').AsDateTime);
                                        Hora_local          := FormatDateTime('hh:nn:ss', FieldByName('FechaHoraTransito').AsDateTime);
                                        //Num_Corr_CO         := FieldByName('NumCorrCO_CA').AsInteger; // SS_1138_CQU_20131009
                                        Num_Corr_CO         := FieldByName('NumCorrCO_CA').Value;       // SS_1138_CQU_20131009
                                        //Num_Corr_CA         := FieldByName('NumCorrCA').AsInteger;    // SS_1138_CQU_20131009
                                        Num_Corr_CA         := FieldByName('NumCorrCA').Value;          // SS_1138_CQU_20131009
                                        ConsolidacionManual := FieldByName('ConsolidacionManual').AsString;
                                    end;

                                    Next;
                                end;
                            end;
                        end;

                        Result := True;
                    end;
                except
                    on e: Exception do vError := e.Message;
                end;
            finally
                if Assigned(vStrStream) then FreeAndNil(vStrStream);
                if Assigned(vImagenTransito) then FreeAndNil(vImagenTransito);
                if Assigned(vImagenTransitoItaliano) then FreeAndNil(vImagenTransitoItaliano);  // SS_1091_CQU_20130516
                if Assigned(vImagenJpeg) then FreeAndNil(vImagenJpeg);                          // SS_1091_CQU_20130816
                
                if not Result then
                    ActualizarDatosXMLTemp(
                                            lTipoDisco,
                                            spObtenerInfraccionesTransitosAEnviarMOP.FieldByName('CodigoInfraccion').AsInteger,
                                            lDisco,
                                            lArchivoEnCurso,
                                            Format(rsImagenNoValida,
                                                [spObtenerInfraccionesTransitosAEnviarMOP.FieldByName('CodigoInfraccion').AsInteger,
                                                 iif(spObtenerInfraccionesTransitosAEnviarMOP.FieldByName('Patente').IsNull, 'Sin Patente', Trim(spObtenerInfraccionesTransitosAEnviarMOP.FieldByName('Patente').AsString)),
                                                 vError]));
            end;
        end;

    begin
        try
            try
                Result := False;

                with spObtenerInfraccionesTransitosAEnviarMOP, spObtenerInfraccionesTransitosAEnviarMOP.Parameters do begin
                    if Active then Close;

                    ParamByName('@CodigoTipo').Value       := spObtenerInfraccionesAEnviarMOP.FieldByName('CodigoTipo').AsInteger;
                    ParamByName('@CodigoDisco').Value      := spObtenerInfraccionesAEnviarMOP.FieldByName('CodigoDisco').AsInteger;
                    ParamByName('@CodigoArchivo').Value    := spObtenerInfraccionesAEnviarMOP.FieldByName('CodigoArchivo').AsInteger;
                    ParamByName('@CodigoInfraccion').Value := spObtenerInfraccionesAEnviarMOP.FieldByName('CodigoInfraccion').AsInteger;
                    Open;
                end;

                if DatosInfraccionCompletos and EsImagenValida then Result := AlmacenarInfraccionXML;
            except
                raise;
            end;
        finally
        end;
    end;



begin
    try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        FProcesando := False;
        ActualizarControles(False);
        lsbxAvisos.Clear;

        if ValidarCondiciones and CargarDatosDiscoXML and InicializarInterfase then begin
            XMLDenuncios := NewDenuncios;

            try
                FProcesando := True;

                with spObtenerInfraccionesAEnviarMOP do begin
                    if Active then Close;
                    Open;

                    lArchivoEnCurso := FieldByName('CodigoArchivo').AsInteger;
                    lTipoDisco      := FieldByName('CodigoTipo').AsInteger;
                    lDisco          := FieldByName('CodigoDisco').AsInteger;
                    lReEmisiones    := spObtenerDatosDiscoXMLInfractoresEmitidos.FieldByName('TotalReEmisiones').AsInteger + 1;

                    pbProgreso.Max := RecordCount;
                    pbProgreso.Min := 1;

                    lRegistrosRefresco  := RecordCount div 100;
                    if lRegistrosRefresco = 0 then lRegistrosRefresco := 1;

                    gbEstado.Caption := ' Estado del Proceso: En Curso ... ';
                    lblInfraccionesRestantes.Visible := True;
                    lblInfraccionesRestantes.Caption := Format('Infracciones Restantes por Procesar: %d', [RecordCount]);
                    MuestraAviso('Procesando Infracciones ...');

                    lArchivosGenerados := 0;

                    while not Eof do begin

                        if RecNo >= lRegistrosRefresco then
                            if RecNo mod lRegistrosRefresco = 0 then begin
                                pbProgreso.Position := RecNo;
                                lblInfraccionesRestantes.Caption := Format('Infracciones Restantes por Procesar: %d', [RecordCount - RecNo]);
                                Application.ProcessMessages;
                            end;

                        if lArchivoEnCurso <> FieldByName('CodigoArchivo').AsInteger then begin
                            GuardarArchivoXML;
                            lArchivoEnCurso := FieldByName('CodigoArchivo').AsInteger;
                        end;

                        ProcesarInfraccion;
                        spObtenerInfraccionesAEnviarMOP.Next;
                    end;

                    if XMLDenuncios.Count > 0 then GuardarArchivoXML;

                    pbProgreso.Position := RecordCount;
                    lblInfraccionesRestantes.Caption := 'Infracciones Restantes por Procesar: 0';
                end;

                MuestraAviso('Actualizando Datos ReEmisi�n ...');

                with spActualizarDiscoXMLInfractoresReEmisiones do begin
                    Parameters.ParamByName('@CodigoConcesionaria').Value :=  FCodigoConcesionaria;
                    Parameters.ParamByName('@CodigoTipo').Value := lTipoDisco;
                    Parameters.ParamByName('@CodigoDisco').Value := lDisco;
                    Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacion;
                    ExecProc;
                end;

                with spActualizarDiscoXMLInfractoresReEmisionesErrores do begin
                    Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacion;
                    ExecProc;
                end;

                MuestraAviso('Cerrando Interfase ...');

                lError := '';
                if not ActualizarLogOperacionesInterfaseAlFinal(DMConnections.BaseCAC, FCodigoOperacion, 'El proceso finaliz� Correctamente.',lError) then
                    raise Exception.Create(lError);

                MuestraAviso('Imprimiendo Reporte de Interfase ...');

                if not MostrarReporteFinalizacion(lError) then
                    raise Exception.Create(lError);

            except
                on e: Exception do ShowMsgBoxCN(rsTituloGeneracionXML, e.Message, MB_ICONERROR, Self);
            end;
        end;
    finally
        if FProcesando then begin
            FProcesando := False;
            gbEstado.Caption := ' Estado del Proceso: Finalizado ';
            MuestraAviso('Proceso Finalizado.');

            RefrescaDatos;
        end;

        ActualizarControles(True);

        Screen.Cursor := crDefault;
        Application.ProcessMessages;
    end;
end;

procedure TReEmisionAnulacionDiscoXMLInfraccionesForm.btnSalirClick(
  Sender: TObject);
begin
    Close;
end;

procedure TReEmisionAnulacionDiscoXMLInfraccionesForm.AnulaDiscoXML(Concesinaria: Integer; TipoDisco: Integer; Disco: Integer);
    resourcestring
        rsTituloAnulacionXML = 'Anulaci�n Disco XML';
    Var
        lError: String;
begin
    try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        FProcesando := False;
        ActualizarControles(False);
        lsbxAvisos.Clear;

        if InicializarInterfase then try
            FProcesando := True;

            gbEstado.Caption := ' Estado del Proceso: En Curso ... ';
            MuestraAviso('Anulando Disco XML ...');

            with spAnularDiscoXMLInfracciones do begin
                Parameters.ParamByName('@CodigoConcesionaria').Value := spObtenerDatosDiscoXMLInfractoresEmitidos.FieldByName('CodigoConcesionaria').AsInteger;
                Parameters.ParamByName('@CodigoTipo').Value := spObtenerDatosDiscoXMLInfractoresEmitidos.FieldByName('CodigoTipo').AsInteger;
                Parameters.ParamByName('@CodigoDisco').Value := spObtenerDatosDiscoXMLInfractoresEmitidos.FieldByName('CodigoDisco').AsInteger;
                Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacion;
                ExecProc;
            end;

            MuestraAviso('Disco XML Anulado.');

            MuestraAviso('Cerrando Interfase ...');

            lError := '';
            if not ActualizarLogOperacionesInterfaseAlFinal(DMConnections.BaseCAC, FCodigoOperacion, 'El proceso finaliz� Correctamente.',lError) then
                raise Exception.Create(lError);

        except
            on e: Exception do ShowMsgBoxCN(rsTituloAnulacionXML, e.Message, MB_ICONERROR, Self);
        end;        
    finally
        if FProcesando then begin
            FProcesando := False;
            gbEstado.Caption := ' Estado del Proceso: Finalizado ';
            MuestraAviso('Proceso Finalizado.');

            RefrescaDatos;
        end;

        ActualizarControles(True);

        Screen.Cursor := crDefault;
        Application.ProcessMessages;
    end;
end;

end.
