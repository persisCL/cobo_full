{------------------------------------------------------------------------------
 File Name: FrmRecepcionRendicionesMisCuentas.pas
 Author:    lgisuk
 Date Created: 21/07/2006
 Language: ES-AR
 Description: M�dulo de la interface MisCuentas - Recepci�n de Rendiciones
 Revision : 1
     Author : vpaszkowicz
     Date : 20/03/2008
     Description : Agrego al log la suma de los montos y la cantidad de re-
     gistros contenidos en el archivo.

Firma       : SS-367-NDR-20110923
Description : Validar que todos los caracteres de la cadena NumeroComprobante sean
            numeros. Si no se cumple, rechazar el archivo completo.

Firma       :   SS_1228_MCA_20141106
Descripcion :   se agregan dos nuevo medios de pago TDEBIT como pago tarjeta de debito
                y TCREDI como pago tarjeta de credito

Autor       :   Claudio Quezada
Fecha       :   06-02-2015
Firma       :   SS_1147_CQU_20150316
Descripcion :   Seg�n la Concesionaria Nativa, se leen los archivos con uno u otro formato


Firma       :   SS_1147_MBE_20150414
Description :   Se agrega el tipo de comprobante, ya que la deuda puede originarse por NK, SD o TD impago.
                Y se necesita registrar este comprobante.
                Adem�s, se corrigen errores de programaci�n y se agregan los mensajes de error

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

Firma       : SS_1147_MCA_20150505
Descripcion : se agrega los errores a la lista de erorres para despues grabarlos en erroresinterfases

Firma       : SS_1279_CQU_20150619
Descripcion : Se agrega lo nuevo de UNIRED.

Firma       : SS_1279_CQU_20150701
Descripcion : Se corrigen algunos detalles indicados por FBE

Firma       : SS_1378_MCA_20150915
Descripcion : se agrega codigomodulo para identificar la rendicion
--------------------------------------------------------------------------------}
unit FrmRecepcionRendicionesMisCuentas;

interface

uses
    //Recepcion de Rendiciones
  	DMConnection,                          //Coneccion a base de datos OP_CAC
    Util,                                  //Stringtofile,padl..
    UtilProc,                              //Mensajes
    ConstParametrosGenerales,              //Obtengo Valores de la Tabla Parametros Generales
    ComunesInterfaces,                     //Procedimientos y Funciones Comunes a todos los formularios
    Peatypes,                              //Constantes
    PeaProcs,                              //NowBase
    FrmRptErroresSintaxis,                 //Reporte de Errores de sintaxis
    FrmRptRecepcionComprobantesMisCuentas, //Reporte del proceso
    //General
  	Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  	Dialogs, StdCtrls, Buttons, DB, ADODB, ExtCtrls, Grids, DBGrids,
  	DPSControls, ListBoxEx, DBListEx, ComCtrls, StrUtils, UtilRB,
    ppCtrls, ppBands, ppPrnabl, ppClass, ppCache, ppProd, ppReport, ppDB,
    ppComm, ppRelatv, ppTxPipe, UtilDB, ppDBPipe, ppModule, raCodMod;

type

  //Estructura donde voy a almacenar cada Rendicion
  TRendicion = record
  	Fecha : TDateTime;
    EPS : string;
    Sucursal : integer;
    Terminal : string;
    Correlativo : integer;
    FechaContable : TDateTime;
    CodigoServicio : string;
    TipoOperacion : string;
    IndicadorContable : string;
    Monto : INT64;
    MedioPago : string;
    CodigoBancoCheque : integer;
    CuentaCheque : string;
    SerieCheque : integer;
    PlazaCheque : integer;
    TipoTarjeta : string;
    MarcaTarjeta : string;
    NumeroTarjeta : string;
    ExpiracionTarjeta : string;
    TipoCuotas : string;
    NumeroCuotas : integer;
    Autorizacion : string;
    TipoComprobante : string;                   // SS_1147_MBE_20150414
//    NotaCobro : integer;                      // SS_1147_MBE_20150414
    NumeroComprobante : Int64;                  // SS_1147_MBE_20150414
    Identificador : string;
    NumeroConvenio : string;                    // SS_1279_CQU_20150619
  end;

  TFRecepcionRendicionesMisCuentas = class(TForm)
    OpenDialog: TOpenDialog;
    pnlOrigen: TPanel;
    btnAbrirArchivo: TSpeedButton;
    txtOrigen: TEdit;
    lblOrigen: TLabel;
    btnProcesar: TButton;
    btnSalir: TButton;
    Bevel: TBevel;
    lblMensaje: TLabel;
    pnlAvance: TPanel;
    lblProcesoGeneral: TLabel;
    pbProgreso: TProgressBar;
    pnlAyuda: TPanel;
    ImgAyuda: TImage;
    SpObtenerResumenComprobantesRecibidos: TADOStoredProc;
    SpObtenerResumenComprobantesRecibidosMotivo: TStringField;
    SpObtenerResumenComprobantesRecibidosCantidad: TIntegerField;
    SpObtenerResumenComprobantesRecibidosMonto: TStringField;
    SPProcesarRendicionesMisCuentas: TADOStoredProc;
    procedure btnAbrirArchivoClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure txtOrigenChange(Sender: TObject);
    procedure btnProcesarClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure ImgAyudaClick(Sender: TObject);
    procedure ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
  private
    { Private declarations }
    FCodigoOperacion : Integer;
    FErrorGrave : Boolean;      //Indica que se produjo un error
    FCancelar : Boolean;        //Indica si ha sido cancelado por el usuario
    FTotalRegistros : Integer;
    FCantidadControl : Real;
    FSumaControl : Real;
    FCodigoModuloInterfase: integer;                                            //SS_1378_MCA_20150915
    //FSumaReal : Real;
    FSumaReal: Int64;
    FMisCuentas_Directorio_Rendiciones : AnsiString;
    FMisCuentas_Directorio_Procesados : AnsiString;
    FMisCuentas_Directorio_Errores : AnsiString;
    FCodigoNativa : Integer;        // SS_1147_CQU_20150316
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
  	Function   Abrir : Boolean;
    Function   Control : Boolean;
    function   ConfirmaCantidades: Boolean;
    function   ParseLineToRecord(Linea : String; var Rendicion : TRendicion; var DescError : String) : Boolean;
    Function   AnalizarRendicionesTXT : Boolean;
    function   RegistrarOperacion : Boolean;
    Function   ActualizarRendiciones : Boolean;
    Function   GenerarReporteErroresSintaxis : Boolean;
    Function   GenerarReporteDeFinalizacion(CodigoOperacionInterfase : Integer) : Boolean;
    procedure GrabarErrores;                                                        // SS_1147_MBE_20150414
  public
    { Public declarations }
  	//Function  Inicializar(txtCaption : ANSIString) : Boolean;                         //SS_1378_MCA_20150915
  	Function  Inicializar(txtCaption : ANSIString; CodigoModulo: integer) : Boolean;    //SS_1378_MCA_20150915
  end;

var
  FRecepcionRendicionesMisCuentas : TFRecepcionRendicionesMisCuentas;
  FLista : TStringList;
  FErrores : TStringList;

Const
  RO_MOD_INTERFAZ_ENTRANTE_RENDICIONES_MISCUENTAS = 73;

implementation

{$R *.dfm}

{ ********************************************************
 Function: EsNumero
 Author: mbecerra
 Date: 14-Mayo-2008
 Description: Verifica que una cadena contega s�lo n�meros
***********************************************************************}
function EsNumero(texto : AnsiString) : boolean;
var
   i : integer;
begin
    i := 1;
    Result := True;
    while Result and (i <= Length(texto)) do begin
    	Result := (texto[i] in ['0'..'9']);
        Inc(i);
    end;

end;

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    lgisuk
  Date Created: 21/07/2006
  Description: M�dulo de inicializaci�n de este formulario
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
//function TFRecepcionRendicionesMisCuentas.Inicializar(txtCaption : ANSIString) : Boolean;                         //SS_1378_MCA_20150915
function TFRecepcionRendicionesMisCuentas.Inicializar(txtCaption : ANSIString; CodigoModulo: integer) : Boolean;    //SS_1378_MCA_20150915

    {******************************** Function Header ******************************
    Function Name: VerificarParametrosGenerales
    Author : lgisuk
    Date Created : 21/07/2006
    Description : Obtengo los Parametros Generales que se utilizaran en el formulario
                  al inicializar y verifico que los valores obtenidos sean validos.
    Parameters : None
    Return Value : boolean
    *******************************************************************************}
    Function VerificarParametrosGenerales : Boolean;
    resourcestring
        MSG_ERROR_CHECK_GENERAL_PARAMETER = 'Error al verificar parametro general';
        MSG_ERROR                         = 'Error';
        STR_NOT_EXISTS_GENERAL_PARAMETER  = 'No existe parametro general: ';
        STR_EMPTY_GENERAL_PARAMETER       = 'Parametro general vacio: ';                                         //Se verifica que los codigos no esten vacios, si no tienen razon de ser
        STR_DIRECTORY_NOT_EXISTS          = 'No existe el directorio indicado en parametros generales: ' + CRLF; //Los directorios de destino y errores en procesos de envio son criticos
    const
        MisCuentas_DIRECTORIO_RENDICIONES        = 'MisCuentas_Rendiciones';
        MisCuentas_DIRECTORIO_ERRORES            = 'MisCuentas_Directorio_Errores';
        MisCuentas_DIRECTORIO_PROCESADOS         = 'MisCuentas_Directorio_Procesados';
    var
        DescError : AnsiString;
    begin
        Result    := True;
        DescError := '';
        try
            try
                // Obtengo la Concesionaria Nativa                  // SS_1147_CQU_20150316
                FCodigoNativa := ObtenerCodigoConcesionariaNativa;  // SS_1147_CQU_20150316

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, MisCuentas_DIRECTORIO_RENDICIONES , FMisCuentas_Directorio_Rendiciones) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + MisCuentas_DIRECTORIO_RENDICIONES;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                FMisCuentas_Directorio_Rendiciones := GoodDir(FMisCuentas_Directorio_Rendiciones);
                if  not DirectoryExists(FMisCuentas_Directorio_Rendiciones) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FMisCuentas_Directorio_Rendiciones;
                    Result := False;
                    Exit;
                end;

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, MisCuentas_DIRECTORIO_ERRORES , FMisCuentas_Directorio_Errores) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + MisCuentas_DIRECTORIO_ERRORES;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                FMisCuentas_Directorio_Errores := GoodDir(FMisCuentas_Directorio_Errores);
                if  not DirectoryExists(FMisCuentas_Directorio_Errores) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FMisCuentas_Directorio_Errores;
                    Result := False;
                    Exit;
                end;

                //Obtengo el parametro general
                ObtenerParametroGeneral(DMConnections.BaseCAC,MisCuentas_DIRECTORIO_PROCESADOS, FMisCuentas_Directorio_Procesados);

            except
                on E : Exception do begin
                    Result := False;
                    MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, E.Message, MSG_ERROR, MB_ICONWARNING);
                end;
            end;

        finally
            //si no paso la verificacion de parametros generales
            if (Result = False) then begin
                //informo la situaci�n
                MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, DescError, MSG_ERROR, MB_ICONWARNING);
            end;
        end;
    end;

resourcestring
    MSG_INIT_ERROR = 'Error al Inicializar';
    MSG_ERROR = 'Error';
begin
  	Result := False;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

    //Centro el formulario
  	CenterForm(Self);
  	try
  		DMConnections.BaseCAC.Connected := True;
  		Result := DMConnections.BaseCAC.Connected and
                           VerificarParametrosGenerales;


  	except
        on E : Exception do begin
            MsgBoxErr(MSG_INIT_ERROR, E.Message, MSG_ERROR, MB_ICONERROR);
        end;
  	end;
    //Resolver ac� lo que necesita este form para inicializar correctamente
    Caption := AnsiReplaceStr(txtCaption, '&', '');  //titulo
  	btnProcesar.enabled := False; //Procesar
  	lblmensaje.Caption := '';     //limpio el indicador
    PnlAvance.visible := False;    //Oculto el Panel de Avance
    FCodigoOperacion := 0;         //inicializo codigo de operacion
    FCodigoModuloInterfase := CodigoModulo;                                     //SS_1378_MCA_20150915
end;

{----------------------------------------------------------------------------- Inicio    SS_1147_MBE_20150414
            GrabarErrores

Author      :   mbecerra
Date        :   15-Abril-2015
Description :   Grabar los errores encontrados en el procesmiento de la interfaz

-----------------------------------------------------------------------------}
procedure TFRecepcionRendicionesMisCuentas.GrabarErrores;
var
    i : Integer;
begin
    for i := 0 to FErrores.Count - 1 do begin
        AgregarErrorInterfase( DMConnections.BaseCAC, FCodigoOperacion, FErrores[i], -1);
    end;

end;                                                                            // Fin      SS_1147_MBE_20150414


{-----------------------------------------------------------------------------
  Function Name: ImgAyudaClick
  Author:    lgisuk
  Date Created: 21/07/2006
  Description: Mensaje de Ayuda
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------
  Revision 1
  24/01/07
  Lgisuk
  Ahora informo en la ayuda que la interfaz permite procesar dos archivos
-----------------------------------------------------------------------------
  Revision 2
  02/02/07
  Lgisuk
  Ahora informo en la ayuda que la interfaz permite procesar mas de un archivo
-----------------------------------------------------------------------------}
procedure TFRecepcionRendicionesMisCuentas.ImgAyudaClick(Sender: TObject);
resourcestring
    MSG_RENDICION      = ' ' + CRLF +
                          'Los Archivo de Rendiciones' + CRLF +
                          'son enviados por las EMPRESAS DE COBRANZAS al ESTABLECIMIENTO' + CRLF +
                          'y tienen por objeto dar a conocer al Establecimiento, ' + CRLF +
                          'los comprobantes que fueron cobrados en las EMPRESAS DE COBRANZAS.' + CRLF +
                          ' ' + CRLF +
                          'Nombre de los Archivos:' + CRLF +
                          ' ' + CRLF +
                          'mcAAAAMMDD.txt [MISCUENTAS]' + CRLF +
                          'cmAAAAMMDD.txt [CMR]' + CRLF +
                          'xxAAAAMMDD.txt [OTROS]' + CRLF +
                          ' ' + CRLF +
                          'Se utilizan para registrar los pagos de los' + CRLF +
                          'comprobantes aceptados en el sistema del ESTABLECIMIENTO.' + CRLF +
                          ' ';
begin
    //si esta procesando sale
    if PnlAvance.visible = True then Exit;
    //Muestro el mensaje
    MsgBoxBalloon(MSG_RENDICION, Caption, MB_ICONQUESTION, IMGAYUDA);
end;

{-----------------------------------------------------------------------------
  Function Name: ImgAyudaMouseMove
  Author:    lgisuk
  Date Created: 21/07/2006
  Description: permito hacer click en la ayuda solo si no esta procesando
  Parameters: Sender: TObject;Shift: TShiftState; X, Y: Integer
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRecepcionRendicionesMisCuentas.ImgAyudaMouseMove(Sender: TObject;Shift: TShiftState; X, Y: Integer);
begin
    if PnlAvance.Visible = True then ImgAyuda.Cursor := crDefault else ImgAyuda.Cursor := crHandPoint;
end;

{-----------------------------------------------------------------------------
  Function Name: BuscarArchivoBTNClick
  Author:    lgisuk
  Date Created: 21/07/2006
  Description: Busco el archivo a procesar
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRecepcionRendicionesMisCuentas.btnAbrirArchivoClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: EsArchivoValidoCajasyPortal
      Author:    lgisuk
      Date Created: 21/07/2006
      Description: Verifica si es un Archivo Valido para Esta Pantalla
      Parameters:
      Return Value: None
    -----------------------------------------------------------------------------
      Revision 1 :
          Author : vpaszkowicz
          Date : 17/01/2007
          Description :Como el archivo puede ser tambi�n CM valido este caso y
          seteo en la variable del Formulario con que archivo estoy trabajando
    -----------------------------------------------------------------------------
      Revision 2 :
          Author : lgisuk
          Date : 01/02/2007
          Description : Ahora verifico solo que sea una fecha valida y tenga
          extension TXT
    -----------------------------------------------------------------------------}
    Function EsArchivoValido(Nombre : AnsiString) : Boolean;
    var
        NombreArchivo: AnsiString;
        ResultadoVS : Boolean;                                                  				// SS_1147_CQU_20150316
    begin

        NombreArchivo := UpperCase(extractfilename(nombre));
        if FCodigoNativa = CODIGO_VS then begin                                 				// SS_1147_CQU_20150316
            ResultadoVS := False;																// SS_1147_CQU_20150316
            if ExistePalabra(uppercase(Nombre), '.TXT') then begin								// SS_1147_CQU_20150316
                if Caption = 'Unimarc' then begin												// SS_1147_CQU_20150316
                    //ResultadoVS := (ExistePalabra(NombreArchivo, 'UNI_CAJA')					// SS_1147_CQU_20150609 // SS_1147_CQU_20150316
                    ResultadoVS := (ExistePalabra(NombreArchivo, 'UNI_CAJA_')					// SS_1147_CQU_20150609
                                    and FechaValidaEnNombreArchivo(Copy(NombreArchivo,10,8))	// SS_1147_CQU_20150316
                            )																	// SS_1147_CQU_20150316
                            or  (                                                               // SS_1279_CQU_20150619
                                    ExistePalabra(NombreArchivo, 'UNI_WEB_')				    // SS_1279_CQU_20150619
                                    and FechaValidaEnNombreArchivo(Copy(NombreArchivo,9,8))	    // SS_1279_CQU_20150619
                            )																	// SS_1279_CQU_20150619
                            or  (                                                               // SS_1279_CQU_20150619
                                    ExistePalabra(NombreArchivo, 'UNI_AUTO_')					// SS_1279_CQU_20150619
                                    and FechaValidaEnNombreArchivo(Copy(NombreArchivo,10,8))	// SS_1279_CQU_20150619
                            )																	// SS_1279_CQU_20150619
                            or  (                                                               // SS_1279_CQU_20150619
                                    ExistePalabra(NombreArchivo, 'UNI_INFW_')					// SS_1279_CQU_20150619
                                    and FechaValidaEnNombreArchivo(Copy(NombreArchivo,10,8))    // SS_1279_CQU_20150619
                            )																	// SS_1279_CQU_20150619
                            or  (                                                               // SS_1279_CQU_20150619
                                    ExistePalabra(NombreArchivo, 'UNI_INFA_')					// SS_1279_CQU_20150619
                                    and FechaValidaEnNombreArchivo(Copy(NombreArchivo,10,8))    // SS_1279_CQU_20150619
                            )																	// SS_1279_CQU_20150619
                            or  (																// SS_1147_CQU_20150316
                                        //ExistePalabra(NombreArchivo, 'UNI_INF') 				// SS_1147_CQU_20150609 // SS_1147_CQU_20150316
                                        ExistePalabra(NombreArchivo, 'UNI_INF_') 				// SS_1147_CQU_20150609 // SS_1147_CQU_20150316
                                    and FechaValidaEnNombreArchivo(Copy(NombreArchivo,9,8))		// SS_1147_CQU_20150316
                            );																	// SS_1147_CQU_20150316
                end;																			// SS_1147_CQU_20150316
                if Caption = 'Sencillito' then begin											// SS_1147_CQU_20150316
                    ResultadoVS := (															// SS_1147_CQU_20150316
                                        ExistePalabra(NombreArchivo, 'STO_CAJA_PORTAL')			// SS_1147_CQU_20150316
                                    and FechaValidaEnNombreArchivo(Copy(NombreArchivo,17,8))	// SS_1147_CQU_20150316
                                    and (Length(NombreArchivo) = 28)							// SS_1147_CQU_20150316
                            )																	// SS_1147_CQU_20150316
                            or  (																// SS_1147_CQU_20150316
                                        ExistePalabra(NombreArchivo, 'STO_CAJA')				// SS_1147_CQU_20150316
                                    and FechaValidaEnNombreArchivo(Copy(NombreArchivo,10,8))	// SS_1147_CQU_20150316
                                    and (Length(NombreArchivo) = 21)                            // SS_1147_CQU_20150316
                            )																	// SS_1147_CQU_20150316
                            or (																// SS_1147_CQU_20150316
                                        ExistePalabra(NombreArchivo, 'STO_INF_PORTAL')			// SS_1147_CQU_20150316
                                    and FechaValidaEnNombreArchivo(Copy(NombreArchivo,16,8))	// SS_1147_CQU_20150316
                                    and (Length(NombreArchivo) = 27)							// SS_1147_CQU_20150316
                            )																	// SS_1147_CQU_20150316
                            or  (																// SS_1147_CQU_20150316
                                        ExistePalabra(NombreArchivo, 'STO_INF')					// SS_1147_CQU_20150316
                                    and FechaValidaEnNombreArchivo(Copy(NombreArchivo,9,8))		// SS_1147_CQU_20150316
                                    and (Length(NombreArchivo) = 20)							// SS_1147_CQU_20150316
                            );																	// SS_1147_CQU_20150316
                end;																			// SS_1147_CQU_20150316
            end;																				// SS_1147_CQU_20150316
            Result := ResultadoVS;																// SS_1147_CQU_20150316
        end else begin																			// SS_1147_CQU_20150316
        Result:= (Length(NombreArchivo) = 14)
                and FechaValidaEnNombreArchivo(Copy(NombreArchivo,3,8))
                and ExistePalabra(uppercase(Nombre), '.TXT');
        end;																					// SS_1147_CQU_20150316
    end;


resourcestring
    MSG_ERROR = 'No es un archivo de Rendiciones ';
const
    STR_VALID = 'Valido!';
begin
    FErrorGrave := False;   // SS_1279_CQU_20150701
    OpenDialog.InitialDir := FMisCuentas_Directorio_Rendiciones;
    if OpenDialog.execute then begin
        if not EsArchivoValido (opendialog.filename)  then begin
            //inform que no es un archivo valido
            MsgBox(MSG_ERROR + STR_VALID , self.Caption, MB_OK + MB_ICONINFORMATION);
            txtOrigen.text := '';
        end
        else begin
            BTNAbrirArchivo.Enabled := False;
            btnProcesar.enabled := True;
            txtOrigen.text := OpenDialog.FileName;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: EOrigenChange
  Author:    lgisuk
  Date Created: 21/07/2006
  Description: Controlo que abra un archivo valido
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRecepcionRendicionesMisCuentas.txtOrigenChange(Sender: TObject);
begin
    //Controlo que abra un archivo valido
  	btnProcesar.Enabled := FileExists( txtOrigen.Text);
end;

{-----------------------------------------------------------------------------
  Function Name: Abrir
  Author:    lgisuk
  Date Created: 21/07/2006
  Description: Abro el Archivo TXT
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
Function TFRecepcionRendicionesMisCuentas.Abrir : Boolean;
resourcestring
  	MSG_OPEN_FILE_ERROR = 'Error al abrir el archivo';
begin
    Result := False;
    FLista.text := FileToString(txtOrigen.text);
    //FTotalRegistros := FLista.Count - 2;      // SS_1147_CQU_20150316
    //if FCodigoNativa = CODIGO_VS                                  // SS_1279_CQU_20150701 // SS_1147_CQU_20150316
    //then FTotalRegistros := FLista.Count - 1                      // SS_1279_CQU_20150701 // SS_1147_CQU_20150316
    //else FTotalRegistros := FLista.Count - 2;                     // SS_1279_CQU_20150701 // SS_1147_CQU_20150316
    // Los archivos de Unimarc vienen con una linea vacia al final  // SS_1279_CQU_20150701
    //FTotalRegistros := FLista.Count - 2;                          //SS_1378_MCA_20150915 // SS_1279_CQU_20150701
    FTotalRegistros := FLista.Count - 1;                            //SS_1378_MCA_20150915  // SS_1279_CQU_20150701

    if FLista.text <> '' then  begin
        Result := True;
        if FMisCuentas_Directorio_Procesados <> '' then begin
            if RightStr(FMisCuentas_Directorio_Procesados,1) = '\' then  FMisCuentas_Directorio_Procesados := FMisCuentas_Directorio_Procesados + ExtractFileName(txtOrigen.text)
            else FMisCuentas_Directorio_Procesados := FMisCuentas_Directorio_Procesados + '\' + ExtractFileName(txtOrigen.text);
        end;
    end else begin
        MsgBox(MSG_OPEN_FILE_ERROR, Self.Caption, MB_OK + MB_ICONINFORMATION);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: Control
  Author:    lgisuk
  Date Created: 21/07/2006
  Description: verifico la consistencia entre los valores reflejados
               en el Registro de Footer y los mismos conceptos calculados
               a partir de los registros de detalle.
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFRecepcionRendicionesMisCuentas.Control : Boolean;
resourcestring
    MSG_INVALID_FOOTER 	= 'El pie de p�gina del archivo es inv�lido!';
    MSG_NOT_EQUAL_COUNT = 'La cantidad reflejada en el registro de Footer no coincide'+ CRLF + 'con la cantidad calculada!';
    MSG_NOT_EQUAL_SUM   = 'La suma reflejada en el registro de Footer no coincide con la suma calculada!';
    MSG_CANT_CONTROL    = 'Cantidad Control: ';
    MSG_CANT_CALC       = 'Cantidad Calculada: ';
    MSG_SUM_CONTROL     = 'Suma Control: ';
    MSG_SUM_CALC        = 'Suma Calculada: ';
    MSG_DIF             = 'Diferencia: ';
    MSG_ERROR           = 'No se pudo realizar el control del archivo!';
var
    i : Integer;
begin
    Result := False;
    try
        if FCodigoNativa = CODIGO_VS then begin                                             //SS_1147_CQU_20150316
            //FCantidadControl := StrToFloat(Trim(Copy(Flista.Strings[0], 9, 7)));          // SS_1279_CQU_20150619 //SS_1147_CQU_20150316
            //FCantidadControl := StrToFloat(Trim(Copy(Flista.Strings[0], 9, 7))) - 1;      //SS_1378_MCA_20150915   // SS_1279_CQU_20150619
            FCantidadControl := StrToFloat(Trim(Copy(Flista.Strings[0], 9, 7)));            //SS_1378_MCA_20150915   // SS_1279_CQU_20150619
            FSumaControl := StrToFloat(Trim(Copy(Flista.Strings[0], 16, 17)));              //SS_1147_CQU_20150316
        end
        else begin                                                                          //SS_1147_CQU_20150316
            //Verifico si existe el footer
            if ( Pos( 'FOOTER', Flista.Strings[FLista.Count - 1] ) <> 1 ) then begin
                  MsgBox( MSG_INVALID_FOOTER, MSG_ERROR, MB_ICONERROR );

                  Exit;
            end;

            //Obtengo la cantidad y la suma del archivo de control
            FCantidadControl := StrToFloat(Trim(Copy(Flista.Strings[FLista.Count - 1], 8, 6)));
            FSumaControl := StrToFloat(Trim(Copy(Flista.Strings[FLista.Count - 1], 30, 43)));
        end;                                                                                //SS_1147_CQU_20150316

        //comparo la cantidad contra la calculada
        If FCantidadControl <> FTotalRegistros then begin
            //MsgBoxErr(MSG_NOT_EQUAL_COUNT + CRLF + MSG_CANT_CONTROL  + FloatToSTR(FCantidadControl) + CRLF + MSG_CANT_CALC  + FloatToSTR(FTotalRegistros) + CRLF + MSG_DIF + FloatToSTR(FCantidadControl - FTotalRegistros) ,'', self.text, 0);                               // SS_1279_CQU_20150701
            MsgBoxErr(ReplaceStr(MSG_NOT_EQUAL_COUNT, 'Footer', 'Header') + CRLF + MSG_CANT_CONTROL  + FloatToSTR(FCantidadControl) + CRLF + MSG_CANT_CALC  + FloatToSTR(FTotalRegistros) + CRLF + MSG_DIF + FloatToSTR(FCantidadControl - FTotalRegistros) ,'', self.text, 0); // SS_1279_CQU_20150701
            exit;
        end;

        //sumo los registros del archivo
        i:=1;
        FSumaReal:=0;
        if FCodigoNativa = CODIGO_VS then begin                                                 //SS_1147_CQU_20150316
            while i <= FLista.count - 1 do begin                                                //SS_1147_CQU_20150316
                if Caption = 'Unimarc' then                                                     //SS_1147_CQU_20150316
                    FSumaReal := FSumaReal + StrToInt64(Trim(Copy(Flista.Strings[i], 25, 8)))   //SS_1147_CQU_20150316
                else if Caption = 'Sencillito' then                                             //SS_1147_CQU_20150316
                    FSumaReal := FSumaReal + StrToInt64(Trim(Copy(Flista.Strings[i], 13, 8)));  //SS_1147_CQU_20150316
                inc(i);                                                                         //SS_1147_CQU_20150316
            end;                                                                                //SS_1147_CQU_20150316
        end
        else begin                                                                              //SS_1147_CQU_20150316
            while i < FLista.count - 1 do begin
                //FSumaReal := FSumaReal + StrToFloat(Trim(Copy(Flista.Strings[i], 73, 10)));
                FSumaReal := FSumaReal + StrToInt64(Trim(Copy(Flista.Strings[i], 73, 10)));
                inc(i);
            end;
        end;                                                                                    //SS_1147_CQU_20150316

        //comparo la suma contra la calculada
        if FSumacontrol <> FSumaReal then begin
            //MsgBoxErr('',MSG_NOT_EQUAL_SUM  + CRLF + MSG_SUM_CONTROL  + FloatToSTR(FSumaControl) + CRLF + MSG_SUM_CALC  + FloatToSTR(FSumaReal) + CRLF + MSG_DIF + FloatToSTR(FSumaControl - FSumaReal), self.text, 0);                               // SS_1279_CQU_20150701
            MsgBoxErr('',ReplaceStr(MSG_NOT_EQUAL_SUM, 'Footer', 'Header')  + CRLF + MSG_SUM_CONTROL  + FloatToSTR(FSumaControl) + CRLF + MSG_SUM_CALC  + FloatToSTR(FSumaReal) + CRLF + MSG_DIF + FloatToSTR(FSumaControl - FSumaReal), self.text, 0); // SS_1279_CQU_20150701
            exit;
        end;
        Result := True;
        
    except
        on E : Exception do begin
            MsgBoxErr(MSG_ERROR, E.Message, MSG_ERROR, MB_ICONERROR);
    		end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ConfirmaCantidades
  Author:    lgisuk
  Date Created: 21/07/2006
  Description:  Consulta al usuario si quiere continuar o no,
                presentandole un resumen de las cantidades a procesar
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFRecepcionRendicionesMisCuentas.ConfirmaCantidades: Boolean;
ResourceString
    MSG_TITLE        = 'Recepci�n de Rendiciones';
    MSG_CONFIRMATION = 'El Archivo seleccionado contiene %d rendiciones a procesar.'+crlf+crlf+
                       'Desea continuar con el procesamiento del archivo?';
var
    Mensaje : String;
begin
    //Armo el mensaje
    Mensaje := Format(MSG_CONFIRMATION,[FTotalRegistros]);
    //lo muestro
    Result := ( MsgBox(Mensaje,MSG_TITLE, MB_ICONQUESTION + MB_YESNO) = mrYes );
end;

{-----------------------------------------------------------------------------
  Function Name: ParseLineToRecord
  Author:    lgisuk
  Date Created: 21/07/2006
  Description: Arma un registro de Rendicion en base a la linea leida del TXT
  Parameters:
  Return Value: boolean
------------------------------------------------------------------------------
  Revision 1:
      Author : ggomez
      Date : 27/07/2006
      Description : Para el campo Correlativo, agregu� que verifique que sea
        num�rico si el campo tiene un valor distinto de la cadena vac�a.
        En el caso de que en el archivo el valor para el campo Correlativo sea
        la cadena vac�a, se coloca como Correlativo el valor 0 (cero). Acord�
        con CN (dferruz) colocar un cero.
------------------------------------------------------------------------------
  Revision 2:
      Author : lgisuk
      Date : 02/08/2006
      Description : Pase monto de StrToInt a StrToInt64
-----------------------------------------------------------------------------}
function TFRecepcionRendicionesMisCuentas.ParseLineToRecord(Linea : String; var Rendicion : TRendicion; var DescError : String) : Boolean;

    {-----------------------------------------------------------------------
      Function Name: InicializarRegistroRendicion
      Author:    lgisuk
      Date Created: 21/07/2006
      Description: inicializo el registro de rendicion
      Parameters:
      Return Value: boolean
    -----------------------------------------------------------------------------}
    procedure InicializarRegistroRendicion( var Rendicion : TRendicion );
    begin
        with Rendicion do begin
            CodigoBancoCheque := 0;
            CuentaCheque := '';
            SerieCheque := 0;
            PlazaCheque := 0;
            TipoTarjeta := '';
            MarcaTarjeta := '';
            NumeroTarjeta := '';
            ExpiracionTarjeta := '';
            TipoCuotas := '';
            NumeroCuotas := 0;
            Autorizacion := '';
        end;
    end;

Resourcestring
    MSG_ERROR                   = 'Error al leer el registro';
Const
    STR_VOUCHER                 = 'Comprobante: ';
    STR_DIVIDER                 = ' - ';
  	STR_DATE_ERROR              = 'La fecha es inv�lida.';
    STR_COMPANY_ERROR           = 'La empresa prestadora de servicios es inv�lida.';
    STR_BRANCH_ERROR            = 'La sucursal es inv�lida.';
    STR_CONTROL_NUMBER_ERROR    = 'El correlativo es inv�lido.';
    STR_INVOICE_DATE_ERROR      = 'La fecha contable es inv�lida.';
    STR_OPERATION_TYPE_ERROR    = 'El tipo de operaci�n es inv�lido.';
    STR_ACCOUNT_INDICATOR_ERROR = 'El indicador contable es inv�lido.';
    STR_AMMOUNT_ERROR           = 'El monto es inv�lido.';
    STR_PAYMENT_METHOD_ERROR    = 'El tipo de pago es inv�lido.';
    STR_BANK_CODE_ERROR         = 'El c�digo de banco es inv�lido.';
    STR_CHECK_SERIAL_ERROR      = 'La serie del cheque es inv�lida.';
    STR_ORIGIN_CHECK_ERROR      = 'La Plaza del cheque es inv�lida.';
    STR_NUMBER_OF_QUOTES_ERROR  = 'El Numero de Coutas en inv�lido.';
    STR_INVOICE_NUMBER_ERROR    = 'El n�mero de la nota de cobro es inv�lido.';
Var
    Ano, Mes, Dia : String;
    sSucursal : String;
    sCorrelativo : String;
    AnoC, MesC, DiaC : String;
    sMonto : String;
    sCodigoBancoCheque : String;
    sSerieCheque : String;
    sPlazaCheque : String;
    sNumeroCuotas : String;
    sNumeroComprobante : String;                            // SS_1147_MBE_20150414
    sTipoComprobanteFiscal, sTipoComprobante : string;      // SS_1147_CQU_20150316
//    NumeroComprobanteFiscal, NroComprobante : Int64;      // SS_1147_MBE_20150414   // SS_1147_CQU_20150316
    sNumeroCheque, sLote, sTipoCliente : string;            // SS_1279_CQU_20150619
begin
    Result := False;
    DescError := '';

    try

        //Inicializo el Registro de Rendici�n
        InicializarRegistroRendicion (Rendicion);

        //Obtengo los Datos de la Rendici�n
//        with Rendicion do begin                                                   // SS_1147_MBE_20150414
            if FCodigoNativa = CODIGO_VS then begin                                 // SS_1147_CQU_20150316
                //Parseo la linea                                                   // SS_1147_CQU_20150316
                if Caption = 'Unimarc' then begin                                   // SS_1147_CQU_20150316
                    sNumeroComprobante      := Copy(Linea, 1, 10);                  // SS_1147_MBE_20150414
//                    NumeroComprobanteFiscal := StrToInt64(Copy(Linea, 1, 10));    // SS_1147_MBE_20150414
                    sTipoComprobanteFiscal  := Trim(Copy(Linea, 11, 2));            // SS_1147_CQU_20150316
                    Rendicion.NumeroConvenio:= Trim(Copy(Linea, 13, 12));           // SS_1279_CQU_20150619
//                    ObtenerDocumentoInterno(sTipoComprobanteFiscal,               // SS_1147_MBE_20150414       // SS_1147_CQU_20150316
//                                            NumeroComprobanteFiscal,              // SS_1147_MBE_20150414       // SS_1147_CQU_20150316
//                                            TipoComprobante, NroComprobante);     // SS_1147_MBE_20150414       // SS_1147_CQU_20150316
                    case StrToInt(sTipoComprobanteFiscal) of                        // SS_1147_MBE_20150414
                        33 : sTipoComprobante := TC_FACTURA_AFECTA;                 // SS_1147_MBE_20150414
                        34 : sTipoComprobante := TC_FACTURA_EXENTA;                 // SS_1147_MBE_20150414
                        39 : sTipoComprobante := TC_BOLETA_AFECTA;                  // SS_1147_MBE_20150414
                        41 : sTipoComprobante := TC_BOLETA_EXENTA;                  // SS_1147_MBE_20150414
                    end;                                                            // SS_1147_MBE_20150414

                    Rendicion.TipoComprobante := sTipoComprobante;                  // SS_1147_MBE_20150414

//                    NumeroComprobante   := IntToStr(NroComprobante);              // SS_1147_MBE_20150414                     // SS_1147_CQU_20150316
                    sMonto 				:= Trim(Copy(Linea, 25, 8));                // SS_1147_CQU_20150316
                    Ano                 := Copy(Linea, 33, 4);                      // SS_1147_CQU_20150316
                    Mes                 := Copy(Linea, 37, 2);                      // SS_1147_CQU_20150316
                    Dia                 := Copy(Linea, 39, 2);                      // SS_1147_CQU_20150316
                    sNumeroCheque       := Copy(Linea, 41, 7);                      // SS_1279_CQU_20150619
                    sCodigoBancoCheque  := Trim(Copy(Linea, 48, 3));                // SS_1147_CQU_20150316
                    sPlazaCheque 		:= Trim(Copy(Linea, 51, 4));                // SS_1147_CQU_20150316
                    Rendicion.MedioPago := Trim(Copy(Linea, 55, 1));                // SS_1147_MBE_20150414                     // SS_1147_CQU_20150316
                    if      Rendicion.MedioPago = '0' then Rendicion.MedioPago := 'CASH'         // SS_1147_MBE_20150414        // SS_1147_CQU_20150316
                    else if Rendicion.MedioPago = '1' then Rendicion.MedioPago := 'CHEQUE'       // SS_1147_MBE_20150414        // SS_1147_CQU_20150316
                    //else if Rendicion.MedioPago = '5' then Rendicion.MedioPago := 'TCREDI'     // SS_1279_CQU_20150701  // SS_1147_MBE_20150414        // SS_1147_CQU_20150316
                    //else if Rendicion.MedioPago = '6' then Rendicion.MedioPago := 'TDEBIT'     // SS_1279_CQU_20150701  // SS_1279_CQU_20150619
                    else if Rendicion.MedioPago = '5' then Rendicion.MedioPago := 'TDEBIT'       // SS_1279_CQU_20150701
                    else if Rendicion.MedioPago = '6' then Rendicion.MedioPago := 'TCREDI'       // SS_1279_CQU_20150701
                    else    Rendicion.MedioPago   := '';                                         // SS_1147_MBE_20150414        // OJO, falta 'TDEBIT'               // SS_1147_CQU_20150316
                    sSucursal           := Trim(Copy(Linea, 56, 3));                // SS_1147_CQU_20150316
                    Rendicion.Terminal  := Trim(Copy(Linea, 59, 4));                // SS_1147_MBE_20150414                     // SS_1147_CQU_20150316
                    sLote               := Trim(Copy(Linea, 63, 2));                // SS_1279_CQU_20150619
                    sCorrelativo        := Trim(Copy( Linea, 65, 3 ));              // SS_1147_CQU_20150316
                    Rendicion.CodigoServicio      := Copy(Linea, 68, 2);            // SS_1147_MBE_20150414                     // CanalPago en VS          // SS_1147_CQU_20150316
                    if      Rendicion.CodigoServicio = '01' then Rendicion.CodigoServicio := 'UNIRED'                           // SS_1147_MBE_20150414     // SS_1147_CQU_20150316
                    else if Rendicion.CodigoServicio = '02' then Rendicion.CodigoServicio := 'UNIWEB'                           // SS_1147_MBE_20150414     // SS_1147_CQU_20150316
                    //else if Rendicion.CodigoServicio = '03' then Rendicion.CodigoServicio := 'UNIAUTO';                       // SS_1279_CQU_20150701  // SS_1279_CQU_20150619
                    else if Rendicion.CodigoServicio = '03' then Rendicion.CodigoServicio := 'UNITBK';                          // SS_1279_CQU_20150701
                    AnoC                := Copy(Linea, 33, 4);                      // SS_1147_CQU_20150316
                    MesC                := Copy(Linea, 37, 2);                      // SS_1147_CQU_20150316
                    DiaC                := Copy(Linea, 39, 2);                      // SS_1147_CQU_20150316
                    sTipoCliente        := Copy(Linea, 70, 1);                      // SS_1279_CQU_20150619
                    Rendicion.TipoOperacion     := 'INGRES';                        // SS_1147_MBE_20150414                     // En Duro porque no viene  // SS_1147_CQU_20150316
                    Rendicion.IndicadorContable := 'H';                             // SS_1147_MBE_20150414                     // En Duro porque no viene  // SS_1147_CQU_20150316
                end                                                                 // SS_1147_CQU_20150316
                else if Caption = 'Sencillito' then begin                           // SS_1147_CQU_20150316
                    sNumeroComprobante      := Copy(Linea, 1, 10);                  // SS_1147_MBE_20150414
//                    NumeroComprobanteFiscal := StrToInt64(Copy(Linea, 1, 10));      // SS_1147_CQU_20150316
                    sTipoComprobanteFiscal  := Trim(Copy(Linea, 11, 2));            // SS_1147_CQU_20150316

//                    ObtenerDocumentoInterno(sTipoComprobanteFiscal,               // SS_1147_MBE_20150414                     // SS_1147_CQU_20150316
//                                            NumeroComprobanteFiscal,              // SS_1147_MBE_20150414                     // SS_1147_CQU_20150316
//                                            TipoComprobante, NroComprobante);     // SS_1147_MBE_20150414                     // SS_1147_CQU_20150316
                    case StrToInt(sTipoComprobanteFiscal) of                        // SS_1147_MBE_20150414
                        33 : sTipoComprobante := TC_FACTURA_AFECTA;                 // SS_1147_MBE_20150414
                        34 : sTipoComprobante := TC_FACTURA_EXENTA;                 // SS_1147_MBE_20150414
                        39 : sTipoComprobante := TC_BOLETA_AFECTA;                  // SS_1147_MBE_20150414
                        41 : sTipoComprobante := TC_BOLETA_EXENTA;                  // SS_1147_MBE_20150414
                    end;                                                            // SS_1147_MBE_20150414

                    Rendicion.TipoComprobante := sTipoComprobante;                  // SS_1147_MBE_20150414

//                    NumeroComprobante   := IntToStr(NroComprobante);              // SS_1147_MBE_20150414                     // SS_1147_CQU_20150316

                    sMonto 				:= Trim(Copy(Linea, 13, 8));                // SS_1147_CQU_20150316
                    Ano                 := Copy(Linea, 21, 4);                      // SS_1147_CQU_20150316
                    Mes                 := Copy(Linea, 25, 2);                      // SS_1147_CQU_20150316
                    Dia                 := Copy(Linea, 27, 2);                      // SS_1147_CQU_20150316
                    sCodigoBancoCheque  := Trim(Copy(Linea, 36, 3));                // SS_1147_CQU_20150316
                    Rendicion.MedioPago := Trim(Copy(Linea, 43, 1));                // SS_1147_CQU_20150316
                    if      Rendicion.MedioPago = '0' then Rendicion.MedioPago := 'CASH'      // SS_1147_MBE_20150414           // SS_1147_CQU_20150316
                    else if Rendicion.MedioPago = '1' then Rendicion.MedioPago := 'CHEQUE'    // SS_1147_MBE_20150414           // SS_1147_CQU_20150316
                    else    Rendicion.MedioPago   := '';                                      // SS_1147_MBE_20150414           // SS_1147_CQU_20150316
                    AnoC                := Copy(Linea, 21, 4);                      // SS_1147_CQU_20150316
                    MesC                := Copy(Linea, 25, 2);                      // SS_1147_CQU_20150316
                    DiaC                := Copy(Linea, 27, 2);                      // SS_1147_CQU_20150316
                    Rendicion.TipoOperacion 	  := 'INGRES';                      // SS_1147_MBE_20150414                     // En Duro porque no viene  // SS_1147_CQU_20150316
                    Rendicion.IndicadorContable   := 'H';                           // SS_1147_MBE_20150414                     // En Duro porque no viene  // SS_1147_CQU_20150316
                    Rendicion.CodigoServicio      := 'SENCI';                       // SS_1147_MBE_20150414                     // SS_1147_CQU_20150316
                    sSucursal           := '0';                                     // SS_1147_CQU_20150316
                end;                                                                // SS_1147_CQU_20150316
                //TipoDocumento     := Copy(Linea, 11, 2 );     // No lo usamos     // SS_1147_CQU_20150316
                //CtaFacturacion    := Copy(Linea, 13, 12 );    // No lo usamos     // SS_1147_CQU_20150316
                //NroCheque         := Copy(Linea, 41, 7);      // No lo usamos     // SS_1147_CQU_20150316
                //Lote              := Copy(Linea, 63, 2);  // No lo usamos         // SS_1147_CQU_20150316
                //TipoCliente       := Copy(Linea, 70, 1);  // No lo usamos         // SS_1147_CQU_20150316
                //EPS 	            := Trim(Copy( Linea, 15, 6));                   // SS_1147_CQU_20150316
                //CuentaCheque 	   	:= Copy(Linea, 92, 11 );                        // SS_1147_CQU_20150316
                //sSerieCheque 		:= Trim(Copy(Linea, 103, 7));                   // SS_1147_CQU_20150316
                //TipoTarjeta         := Copy(Linea, 113, 6);                       // SS_1147_CQU_20150316
                //MarcaTarjeta        := Copy(Linea, 119, 6);                       // SS_1147_CQU_20150316
                //NumeroTarjeta       := Copy(Linea, 125, 19);                      // SS_1147_CQU_20150316
                //ExpiracionTarjeta   := Copy(Linea, 144, 4);                       // SS_1147_CQU_20150316
                //TipoCuotas          := Copy(Linea, 148, 6);                       // SS_1147_CQU_20150316
                //sNumeroCuotas       := Trim( Copy(Linea, 154, 2 ));               // SS_1147_CQU_20150316
                //Autorizacion        := Copy(Linea, 156, 8 );                      // SS_1147_CQU_20150316
                //Identificador       := Copy(Linea, 173, 91 );                     // SS_1147_CQU_20150316
            end
            else begin                                                              // SS_1147_CQU_20150316
                //Parseo la linea
                Ano                := Copy(Linea, 1, 4);
                Mes                := Copy(Linea, 5, 2);
                Dia                := Copy(Linea, 7, 2);
                Rendicion.EPS      := Trim(Copy( Linea, 15, 6));                    // SS_1147_MBE_20150414
                sSucursal          := Trim(Copy(Linea, 21, 6));
                Rendicion.Terminal := Copy(Linea, 27, 15);                          // SS_1147_MBE_20150414
                sCorrelativo 	   := Trim(Copy( Linea, 42, 10 ));
                AnoC               := Copy(Linea, 52, 4);
                MesC               := Copy(Linea, 56, 2);
                DiaC               := Copy(Linea, 58, 2);
                Rendicion.CodigoServicio	 := Copy(Linea, 60, 6);                 // SS_1147_MBE_20150414
                Rendicion.TipoOperacion 	 := Copy(Linea, 66, 6);                 // SS_1147_MBE_20150414
                Rendicion.IndicadorContable  := Copy(Linea, 72, 1);                 // SS_1147_MBE_20150414
                sMonto 				         := Trim(Copy(Linea, 73, 10));
                Rendicion.MedioPago 		 := Trim(Copy(Linea, 83, 6));           // SS_1147_MBE_20150414
                sCodigoBancoCheque           := Trim(Copy(Linea, 89, 3));
                Rendicion.CuentaCheque 	   	 := Copy(Linea, 92, 11 );               // SS_1147_MBE_20150414
                sSerieCheque 		         := Trim(Copy(Linea, 103, 7));
                sPlazaCheque 		         := Trim(Copy(Linea, 110, 3));
                Rendicion.TipoTarjeta        := Copy(Linea, 113, 6);                // SS_1147_MBE_20150414
                Rendicion.MarcaTarjeta       := Copy(Linea, 119, 6);                // SS_1147_MBE_20150414
                Rendicion.NumeroTarjeta      := Copy(Linea, 125, 19);               // SS_1147_MBE_20150414
                Rendicion.ExpiracionTarjeta  := Copy(Linea, 144, 4);                // SS_1147_MBE_20150414
                Rendicion.TipoCuotas         := Copy(Linea, 148, 6);                // SS_1147_MBE_20150414
                sNumeroCuotas                := Trim( Copy(Linea, 154, 2 ));
                Rendicion.Autorizacion       := Copy(Linea, 156, 8 );               // SS_1147_MBE_20150414
                sNumeroComprobante           := Copy(Linea, 164, 9 );               // SS-367-NDR-20110923
                Rendicion.TipoComprobante    := 'NK';                               // SS_1147_MBE_20150414
                Rendicion.Identificador      := Copy(Linea, 173, 91 );              // SS_1147_MBE_20150414
            end;                                                                    // SS_1147_CQU_20150316

            //Verifico que sea una fecha valida
            try
                Rendicion.Fecha := EncodeDate(StrToInt(Ano), StrToInt(Mes), StrToInt(Dia));
            except
                DescError := STR_VOUCHER + sNumeroComprobante + STR_DIVIDER +  STR_DATE_ERROR;
                Exit;
            end;

            if FCodigoNativa <> CODIGO_VS then begin                            //SS_1147_CQU_20150316
            //Verifico que la empresa prestador de servicios sea costanera norte
                if (Rendicion.EPS <> 'COSNOR') then begin
                    DescError := STR_VOUCHER + sNumeroComprobante + STR_DIVIDER +  STR_COMPANY_ERROR;
                    Exit;
                end;
            end;                                                                //SS_1147_CQU_20150316

            //Verifico que la sucursal sea numerica
            try
                Rendicion.Sucursal := StrToInt(sSucursal);
            except
                DescError := STR_VOUCHER + sNumeroComprobante + STR_DIVIDER +  STR_BRANCH_ERROR;
                Exit;
            end;

            // Agregado en Revision 1
            // Incializar el Correlativo con 0 (cero).
            Rendicion.Correlativo := 0;
            // Si el correlativo es distinto de vac�o, entonces verificar si es un n�mero.
            if Trim(sCorrelativo) <> EmptyStr then begin
                //Verifico que el numero correlativo sea numerico
                try
                    Rendicion.Correlativo := StrToInt(sCorrelativo);
                except
                    DescError := STR_VOUCHER + sNumeroComprobante + STR_DIVIDER + STR_CONTROL_NUMBER_ERROR;
                    Exit;
                end;
            end;

            //Verifico que la fecha contable sea valida
            try
                Rendicion.FechaContable := EncodeDate(StrToInt(AnoC), StrToInt(MesC), StrToInt(DiaC));
            except
                DescError := STR_VOUCHER + sNumeroComprobante + STR_DIVIDER + STR_INVOICE_DATE_ERROR;
                Exit;
            end;

            //Verico que la operacion sea de ingreso
            if (Rendicion.TipoOperacion <> 'INGRES') then begin
                DescError := STR_VOUCHER + sNumeroComprobante + STR_DIVIDER + STR_OPERATION_TYPE_ERROR;
                Exit;
            end;

            //Verifico que el indicador contable sea haber
            if (Rendicion.IndicadorContable <> 'H') then begin
                DescError := STR_VOUCHER + sNumeroComprobante + STR_DIVIDER + STR_ACCOUNT_INDICATOR_ERROR;
                Exit;
            end;

            //Verifico que el Monto sea numerico
            try
                Rendicion.Monto := StrToInt64(sMonto);
            except
                DescError := STR_VOUCHER + sNumeroComprobante + STR_DIVIDER + STR_AMMOUNT_ERROR;
                Exit;
            end;

            //Verifico que el medio de pago sea efectivo o cheque
            //if (MedioPago <> 'CASH') and (MedioPago <> 'CHEQUE') then begin   //SS_1228_MCA_20141106
            if (Rendicion.MedioPago <> 'CASH') and (Rendicion.MedioPago <> 'CHEQUE') and (Rendicion.MedioPago <> 'TCREDI') and (Rendicion.MedioPago <> 'TDEBIT') then begin //SS_1228_MCA_20141106
                DescError := STR_VOUCHER + sNumeroComprobante + STR_DIVIDER + STR_PAYMENT_METHOD_ERROR;
                Exit;
            end;

            //Verifico que el Codigo de Banco del cheque sea numerico
            try
                Rendicion.CodigoBancoCheque := StrToInt(sCodigoBancoCheque);
            except
                DescError := STR_VOUCHER + sNumeroComprobante + STR_DIVIDER + STR_BANK_CODE_ERROR;
                Exit;
            end;

            if FCodigoNativa <> CODIGO_VS then begin                            //SS_1147_CQU_20150316
                //Verifico que el Numero de Serie del cheque sea numerico
                try
                    Rendicion.SerieCheque := StrToInt(sSerieCheque);
                except
                    DescError := STR_VOUCHER + sNumeroComprobante + STR_DIVIDER + STR_CHECK_SERIAL_ERROR;
                    Exit;
                end;

                //Verifico que la plaza del cheque sea numerica
                try
                    Rendicion.PlazaCheque := StrToInt(sPlazaCheque);
                except
                    DescError := STR_VOUCHER + sNumeroComprobante + STR_DIVIDER + STR_ORIGIN_CHECK_ERROR;
                    Exit;
                end;

                //Verifico que el numero de cuotas sea numerico
                try
                    Rendicion.NumeroCuotas := StrToIntDef(sNumeroCuotas, 0);
                except
                    DescError := STR_VOUCHER + sNumeroComprobante + STR_DIVIDER + STR_NUMBER_OF_QUOTES_ERROR;
                    Exit;
                end;
            end;                                                                //SS_1147_CQU_20150316

            
            //Verifico que el numero de comprobante sea numerico                                                // SS-367-NDR-20110923
            try                                                                                                 // SS-367-NDR-20110923
                if EsNumero(sNumeroComprobante) then                                                            // SS_1147_MBE_20150414  //SS-367-NDR-20110923
 //                  NotaCobro := StrToInt(NumeroComprobante)                                                   // SS-367-NDR-20110923
                    Rendicion.NumeroComprobante := StrToInt64(sNumeroComprobante)                               // SS_1147_MBE_20150414
                else                                                                                            // SS-367-NDR-20110923
                begin                                                                                           // SS-367-NDR-20110923
                  DescError := STR_VOUCHER + sNumeroComprobante + STR_DIVIDER + STR_INVOICE_NUMBER_ERROR;       // SS-367-NDR-20110923
                  Exit;                                                                                         // SS-367-NDR-20110923
                end;                                                                                            // SS-367-NDR-20110923
            except                                                                                              // SS-367-NDR-20110923
                DescError := STR_VOUCHER + sNumeroComprobante + STR_DIVIDER + STR_INVOICE_NUMBER_ERROR;         // SS-367-NDR-20110923
                Exit;                                                                                           // SS-367-NDR-20110923
            end;                                                                                                // SS-367-NDR-20110923

 //       end;                                                                                                  // SS_1147_MBE_20150414

        Result := True;

    except
        on E: Exception do begin
            MsgBoxErr(MSG_ERROR, E.Message, Self.Caption, MB_ICONERROR);
            FErrorGrave:=true;
        end;
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: AnalizarRencionesTXT
  Author:    lgisuk
  Date Created: 21/07/2006
  Description: Antes de hacer nada Parsea todo el Archivo para verificar
  				que el parseo sea v�lido
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
Function TFRecepcionRendicionesMisCuentas.AnalizarRendicionesTXT : Boolean;
Resourcestring
    MSG_ERROR_SYNTAXIS = 'El Archivo contiene errores de Sintaxis';
var
    I : Integer;
    Rendicion : TRendicion;
    DescError : String;
begin
    FCancelar := False;                //Permito que la importacion sea cancelada
    PbProgreso.Position := 0;          //Inicio la Barra de progreso
    PbProgreso.Max := FLista.Count-1;  //Establezco como maximo la cantidad de registros del memo
    FErrores.Clear;
    I := 1;
    if FCodigoNativa = CODIGO_VS then begin	                                                    // SS_1147_CQU_20150316
        while I <= FLista.Count - 1 do begin													// SS_1147_CQU_20150316
            //si cancelan la operacion															// SS_1147_CQU_20150316
            if FCancelar then begin																// SS_1147_CQU_20150316
                Result := False;             //El analisis fue cancelado por el usuario			// SS_1147_CQU_20150316
                pbProgreso.Position := 0;  //Si la importacion fue detenida, Vuelvo a Comenzar	// SS_1147_CQU_20150316
                Exit;                      //salgo de la rutina									// SS_1147_CQU_20150316
            end;																				// SS_1147_CQU_20150316

            //Arma un registro de Rendicion en base a la linea leida del TXT					// SS_1147_CQU_20150316
            //verifica que los valores recibidos sean validos sino devuelve una					// SS_1147_CQU_20150316
            //Descripci�n del error.															 // SS_1147_CQU_20150316
//            if not ParseLineToRecord(Flista.Strings[i], Rendicion, DescError) then begin		// SS_1147_MBE_20150414         // SS_1147_CQU_20150316
                //lo inserto en el string list de errores										// SS_1147_CQU_20150316
//                FErrores.Add(DescError);														// SS_1147_MBE_20150414         // SS_1147_CQU_20150316
//            end;																				// SS_1147_MBE_20150414         // SS_1147_CQU_20150316

            DescError := '';
            ParseLineToRecord(Flista.Strings[i], Rendicion, DescError);	                        // SS_1147_MBE_20150414
            if DescError <> '' then begin                                                       // SS_1147_MBE_20150414
                FErrores.Add(DescError);                                                        // SS_1147_MBE_20150414
            end;                                                                                // SS_1147_MBE_20150414


            pbProgreso.Position:= pbProgreso.Position + 1;  //Muestro el progreso				// SS_1147_CQU_20150316
            Application.ProcessMessages;                    //Refresco la pantalla				// SS_1147_CQU_20150316

            Inc(I);																				// SS_1147_CQU_20150316
        end;																					// SS_1147_CQU_20150316
    end
    else begin																				    // SS_1147_CQU_20150316
        //recorro las lineas del archivo
        while I < FLista.Count - 1 do begin

            //si cancelan la operacion
            if FCancelar then begin
                Result := False;             //El analisis fue cancelado por el usuario
                pbProgreso.Position := 0;  //Si la importacion fue detenida, Vuelvo a Comenzar
                Exit;                      //salgo de la rutina
            end;

            //Arma un registro de Rendicion en base a la linea leida del TXT
            //verifica que los valores recibidos sean validos sino devuelve una
            //Descripci�n del error.
            if not ParseLineToRecord(Flista.Strings[i], Rendicion, DescError) then begin
                //lo inserto en el string list de errores
                FErrores.Add(DescError);
            end;

            pbProgreso.Position:= pbProgreso.Position + 1;  //Muestro el progreso
            Application.ProcessMessages;                    //Refresco la pantalla

            I := I + 1;
        end;
    end;																						// SS_1147_CQU_20150316
    Result:= (FErrores.count = 0);

    if (Result = False) then MsgBox(MSG_ERROR_SYNTAXIS, Self.Caption, MB_OK + MB_ICONINFORMATION);

end;

{-----------------------------------------------------------------------------
  Function Name: RegistrarOperacion
  Author:    lgisuk
  Date Created: 21/07/2006
  Description: Registra la Operaci�n en el Log de Operaciones
  Parameters: None
  Return Value: boolean
------------------------------------------------------------------------------
  Revision 1  :
      Author : vpaszkowicz
      Date : 17/01/2007
      Description : Registro la operacion para CMR FALABELLA
------------------------------------------------------------------------------
  Revision 2  :
      Author : lgisuk
      Date : 02/02/2007
      Description : Todos los archivos se registran como modulo de mis cuentas
  Revision : 3
      Author : vpaszkowicz
      Date : 20/03/2008
      Description : Agrego al log la suma de los montos y la cantidad de re-
      gistros contenidos en el archivo.
-----------------------------------------------------------------------------}
function TFRecepcionRendicionesMisCuentas.RegistrarOperacion : Boolean;
resourcestring
	  MSG_COULD_NOT_REGISTER_OPERATION = 'No se pudo registrar la operaci�n';
    MSG_ERROR = 'Error';
Const
    STR_OBSERVACION = 'Procesar Archivo de Rendiciones';
var
    NombreArchivo: String;
  	DescError    : String;
begin
   NombreArchivo := Trim(ExtractFileName(TxtOrigen.text));
   //Result := RegistrarOperacionEnLogInterface (DMConnections.BaseCAC , RO_MOD_INTERFAZ_ENTRANTE_RENDICIONES_MISCUENTAS , NombreArchivo, UsuarioSistema , STR_OBSERVACION , False , False, NowBase(DMConnections.BaseCAC) , 0 , FCodigoOperacion, FTotalRegistros, FSumaReal, DescError);      //SS_1378_MCA_20150915
   Result := RegistrarOperacionEnLogInterface (DMConnections.BaseCAC , FCodigoModuloInterfase, NombreArchivo, UsuarioSistema , STR_OBSERVACION , False , False, NowBase(DMConnections.BaseCAC) , 0 , FCodigoOperacion, FTotalRegistros, FSumaReal, DescError);                                  //SS_1378_MCA_20150915
   if (not Result) then MsgBoxErr(MSG_COULD_NOT_REGISTER_OPERATION, DescError, MSG_ERROR, MB_ICONERROR);
end;


{-----------------------------------------------------------------------------
  Function Name: ActualizarRenciones
  Author:    lgisuk
  Date Created: 21/07/2006
  Description: Proceso el archivo de rendiciones
  Parameters: 
  Return Value: Boolean
-----------------------------------------------------------------------------}
Function TFRecepcionRendicionesMisCuentas.ActualizarRendiciones : Boolean;

      {-----------------------------------------------------------------------------
        Function Name: ActualizarRendicion
        Author:    lgisuk
        Date Created: 21/07/2006
        Description: Registro el pago con la informacion recibida en el archivo
        Parameters:
        Return Value: boolean
       -----------------------------------------------------------------------------
        Revision 1
         Author: lgisuk
         Date Created: 14/12/2006
         Description: Escribo sentencias sin utilizar comando with,
                      no utilizo refresh.
      -----------------------------------------------------------------------------
       Revision 2
         Author: lgisuk
         Date Created: 14/03/2007
         Description: ahora al llamar a registrar al pago informo si se trata de un
         reintento
      -----------------------------------------------------------------------------}
      Function ActualizarRendicion (Rendicion : TRendicion; var DescError : String) : Boolean;
      resourcestring
          MSG_ERROR_SAVING_INVOICE = 'Error al Asentar la Operaci�n Comprobante: %d  ';
          MSG_ERROR     = 'Error al actualizar la Rendicion';
      const
          STR_COMPROBANTE  = 'Comprobante: ';
          STR_SEPARADOR = ' - ';
      var
          Error : String;
          //
          Reintentos : Integer;
          mensajeDeadlock : AnsiString;
      begin
          Result := False;
          DescError := '';
          Error := '';

          //Intentamos registrar el pago de un comprobante.
          Reintentos  := 0;

          while Reintentos < 3 do begin

                try
                      SPProcesarRendicionesMisCuentas.Parameters.Refresh;                                                                               // SS_1147_MBE_20150414
                      SPProcesarRendicionesMisCuentas.Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacion;
                      SPProcesarRendicionesMisCuentas.Parameters.ParamByName('@Fecha').Value := Rendicion.Fecha;
                      SPProcesarRendicionesMisCuentas.Parameters.ParamByName('@EPS').Value := Rendicion.EPS;
                      SPProcesarRendicionesMisCuentas.Parameters.ParamByName('@Sucursal').Value := Rendicion.Sucursal;
                      SPProcesarRendicionesMisCuentas.Parameters.ParamByName('@Terminal').Value := Rendicion.Terminal;
                      SPProcesarRendicionesMisCuentas.Parameters.ParamByName('@Correlativo').Value := Rendicion.Correlativo;
                      SPProcesarRendicionesMisCuentas.Parameters.ParamByName('@FechaContable').Value := Rendicion.FechaContable;
                      SPProcesarRendicionesMisCuentas.Parameters.ParamByName('@CodigoServicio').Value := Rendicion.CodigoServicio;
                      SPProcesarRendicionesMisCuentas.Parameters.ParamByName('@TipoOperacion').Value := Rendicion.TipoOperacion;
                      SPProcesarRendicionesMisCuentas.Parameters.ParamByName('@IndicadorContable').Value := Rendicion.IndicadorContable;
                      SPProcesarRendicionesMisCuentas.Parameters.ParamByName('@Monto').Value := Rendicion.Monto;
                      SPProcesarRendicionesMisCuentas.Parameters.ParamByName('@MedioPago').Value := Rendicion.MedioPago;
                      SPProcesarRendicionesMisCuentas.Parameters.ParamByName('@CodigoBancoCheque').Value := IIF( Rendicion.CodigoBancoCheque <> 0, Rendicion.CodigoBancoCheque, NULL );
                      SPProcesarRendicionesMisCuentas.Parameters.ParamByName('@CuentaCheque').Value := IIF( Rendicion.CuentaCheque <> '', Rendicion.CuentaCheque, NULL );
                      SPProcesarRendicionesMisCuentas.Parameters.ParamByName('@SerieCheque').Value := IIF( Rendicion.SerieCheque <> 0, Rendicion.SerieCheque, NULL );
                      SPProcesarRendicionesMisCuentas.Parameters.ParamByName('@PlazaCheque').Value := IIF( Rendicion.PlazaCheque <> 0, Rendicion.PlazaCheque, NULL );
                      SPProcesarRendicionesMisCuentas.Parameters.ParamByName('@TipoTarjeta').Value := IIF( Rendicion.TipoTarjeta <> '', Rendicion.TipoTarjeta, NULL );
                      SPProcesarRendicionesMisCuentas.Parameters.ParamByName('@MarcaTarjeta').Value := IIF( Rendicion.MarcaTarjeta <> '', Rendicion.MarcaTarjeta, NULL );
                      SPProcesarRendicionesMisCuentas.Parameters.ParamByName('@NumeroTarjeta').Value := IIF( Rendicion.NumeroTarjeta <> '', Rendicion.NumeroTarjeta, NULL );
                      SPProcesarRendicionesMisCuentas.Parameters.ParamByName('@ExpiracionTarjeta').Value := IIF( Rendicion.ExpiracionTarjeta <> '', Rendicion.ExpiracionTarjeta, NULL );
                      SPProcesarRendicionesMisCuentas.Parameters.ParamByName('@TipoCuotas').Value := IIF( Rendicion.TipoCuotas <> '', Rendicion.TipoCuotas, NULL );
                      SPProcesarRendicionesMisCuentas.Parameters.ParamByName('@NumeroCuotas').Value := IIF( Rendicion.NumeroCuotas <> 0, Rendicion.NumeroCuotas, NULL );
                      SPProcesarRendicionesMisCuentas.Parameters.ParamByName('@Autorizacion').Value := IIF( Rendicion.Autorizacion <> '', Rendicion.Autorizacion, NULL );
                      SPProcesarRendicionesMisCuentas.Parameters.ParamByName('@TipoComprobante').Value := Rendicion.TipoComprobante;              // SS_1147_MBE_20150414
//                      SPProcesarRendicionesMisCuentas.Parameters.ParamByName('@NotaCobro').Value := Rendicion.NotaCobro;                        // SS_1147_MBE_20150414
                      SPProcesarRendicionesMisCuentas.Parameters.ParamByName('@NumeroComprobante').Value := Rendicion.NumeroComprobante;          // SS_1147_MBE_20150414
                      SPProcesarRendicionesMisCuentas.Parameters.ParamByName('@Identificador').Value := IIF( Rendicion.Identificador <> '', Rendicion.Identificador, NULL );
                      SPProcesarRendicionesMisCuentas.Parameters.ParamByName('@Usuario').Value	:= UsuarioSistema;
                      SPProcesarRendicionesMisCuentas.Parameters.ParamByName('@Reintento').Value	:= Reintentos;
                      SPProcesarRendicionesMisCuentas.Parameters.ParamByName('@NumeroConvenio').Value := Rendicion.NumeroConvenio;                // SS_1279_CQU_20150619
                      SPProcesarRendicionesMisCuentas.Parameters.ParamByName('@DescripcionError').Value := '';
                      SPProcesarRendicionesMisCuentas.CommandTimeout := 5000;
                      SPProcesarRendicionesMisCuentas.ExecProc;
                      Error := Trim(Copy(VarToStr(SPProcesarRendicionesMisCuentas.Parameters.ParamByName('@DescripcionError').Value),1,100));
                      if Error <> '' then begin
//                          DescError := STR_COMPROBANTE + IntToStr(Rendicion.NotaCobro) + STR_SEPARADOR + Error;                                   // SS_1147_MBE_20150414
                            //DescError := STR_COMPROBANTE + IntToStr(Rendicion.NumeroComprobante) + STR_SEPARADOR + Error;                         // SS_1147_MCA_20150505 // SS_1147_MBE_20150414
                            FErrores.Add(Error);                                                                                                    // SS_1147_MCA_20150505
//                          Exit;                                                                                                                   // SS_1147_MBE_20150414
                      end;
                      Result := True;
                      //1/4 de segundo entre registracion de pago y otro
                      Sleep(25);
                      //Salgo del ciclo por Ok
                      Break;
                except
                    on  E : Exception do begin
                        if (pos('deadlock', e.Message) > 0) then begin
                            mensajeDeadlock := e.message;
                            inc(Reintentos);
                            sleep(2000);
                        end else begin
                            FErrorGrave := True;
//                            MsgBoxErr(Format(MSG_ERROR_SAVING_INVOICE, [Rendicion.NotaCobro]), E.Message, Self.caption, MB_ICONERROR);            // SS_1147_MBE_20150414
                            MsgBoxErr(Format(MSG_ERROR_SAVING_INVOICE, [Rendicion.NumeroComprobante]), E.Message, Self.caption, MB_ICONERROR);      // SS_1147_MBE_20150414
                            Break;
                        end;
                    end;
                end;

          end;

          //una vez que se reintento 3 veces x deadlock lo informo
          if Reintentos = 3 then begin
              FErrorGrave := True;
//              MsgBoxErr(Format(MSG_ERROR_SAVING_INVOICE, [Rendicion.NotaCobro]), MensajeDeadLock, MSG_ERROR, MB_ICONERROR);                       // SS_1147_MBE_20150414
                MsgBoxErr(Format(MSG_ERROR_SAVING_INVOICE, [Rendicion.NumeroComprobante]), MensajeDeadLock, MSG_ERROR, MB_ICONERROR);               // SS_1147_MBE_20150414
          end;

      end;

var
    i: Integer;
    Rendicion : TRendicion;
    DescError: String;
begin
    Result := False;
    FCancelar := False;                //Permito que la importacion sea cancelada
    pbProgreso.Position := 0;          //Inicio la Barra de progreso
    pbProgreso.Max := FLista.Count-1;  //Establezco como maximo la cantidad de registros del memo
    I := 1;
    if FCodigoNativa = CODIGO_VS then begin                                     // SS_1147_CQU_20150316
        while I <= FLista.Count - 1  do begin                                   // SS_1147_CQU_20150316
            //si cancelan la operacion                                          // SS_1147_CQU_20150316
            if FCancelar then begin                                             // SS_1147_CQU_20150316
                 pbProgreso.Position := 0;                                      // SS_1147_CQU_20150316
                 Exit;                                                          // SS_1147_CQU_20150316
            end;                                                                // SS_1147_CQU_20150316

            DescError := '';                                                    // SS_1147_MBE_20150414
            ParseLineToRecord(Flista.Strings[i], Rendicion, DescError);         // SS_1147_CQU_20150316
            if DescError <> '' then begin                                       // SS_1147_MBE_20150414
                FErrores.Add(DescError);                                        // SS_1147_MBE_20150414
            end;                                                                // SS_1147_MBE_20150414

            ActualizarRendicion (Rendicion, DescError);                         // SS_1147_CQU_20150316
            pbProgreso.Position:= pbProgreso.Position + 1;                      // SS_1147_CQU_20150316
            Application.ProcessMessages;                                        // SS_1147_CQU_20150316
            Inc(I);                                                             // SS_1147_CQU_20150316
        end;                                                                    // SS_1147_CQU_20150316
    end
    else begin                                                                  // SS_1147_CQU_20150316
        while I < FLista.Count - 1  do begin

            //si cancelan la operacion
            if FCancelar then begin
                 pbProgreso.Position := 0;
                 Exit;
            end;

            //Obtengo el registro con los datos de la rendicion del archivo
            ParseLineToRecord(Flista.Strings[i], Rendicion, DescError);
            //Registro el pago con la informaci�n de la rendicion
            ActualizarRendicion (Rendicion, DescError);

            pbProgreso.Position:= pbProgreso.Position + 1;  //Muestro el progreso
            Application.ProcessMessages;                    //Refresco la pantalla
            i:= i + 1;
        end;
    end;                                                                        // SS_1147_CQU_20150316
    Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: GenerarReporteErroresSintaxis
  Author:    lgisuk
  Date Created: 21/07/2006
  Description: Genero el reporte de errores de sintaxis
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFRecepcionRendicionesMisCuentas.GenerarReporteErroresSintaxis : Boolean;
resourcestring
    MSG_REPORT_ERROR = 'No se pudo generar el reporte de errores de Sintaxis';
    MSG_ERROR        = 'Error';
const
    REPORT_TITLE     = 'MisCuentas - Procesar Archivo de Rendiciones';
var
    F : TFRptErroresSintaxis;
begin
    Result := False;
    try
        //muestro el reporte
        Application.CreateForm(TFRptErroresSintaxis, F);
        //if not F.Inicializar( IntToStr(RO_MOD_INTERFAZ_ENTRANTE_RENDICIONES_MISCUENTAS) , REPORT_TITLE , FMisCuentas_Directorio_Errores ,FErrores) then f.Release;        //SS_1378_MCA_20150915
        if not F.Inicializar( IntToStr(FCodigoModuloInterfase) , REPORT_TITLE , FMisCuentas_Directorio_Errores ,FErrores) then f.Release;                                   //SS_1378_MCA_20150915
        Result := True;
    except
       on E : Exception do begin
            MsgBoxErr(MSG_REPORT_ERROR, E.Message, MSG_ERROR, MB_ICONERROR);
            Exit;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: GenerarReporteDeFinalizacion
  Author:    lgisuk
  Date Created: 21/07/2006
  Description: Genero el Reporte de Finalizacion de Proceso sirve para
               para verificar si el proceso se realizo segun parametros
               normales.
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFRecepcionRendicionesMisCuentas.GenerarReporteDeFinalizacion(CodigoOperacionInterfase : Integer) : Boolean;
Const
    STR_TITLE = 'Comprobantes Recibidos';
var
    FRecepcion : TFrmRptRecepcionComprobantesMisCuentas;
begin
    Result := False;
    try
        //muestro el reporte
        Application.CreateForm(TFrmRptRecepcionComprobantesMisCuentas,FRecepcion);
        if not fRecepcion.Inicializar( STR_TITLE, CodigoOperacionInterfase) then fRecepcion.Release;
        Result := True;
    except
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: btnProcesarClick
  Author:    lgisuk
  Date Created: 21/07/2006
  Description: Procesa el archivo de Rendiciones
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------
  Revision 1
  lgisuk
  02/02/07
  Se quita el control de cabecera del archivo
-----------------------------------------------------------------------------}
procedure TFRecepcionRendicionesMisCuentas.btnProcesarClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: ObtenerCantidadErroresInterfaz
      Author:    lgisuk
      Date Created: 21/07/2006
      Description: Obtengo la cantidad de errores contemplados que se produjeron
                   al procesar el archivo
      Parameters: CodigoOperacion:string
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function ObtenerCantidadErroresInterfaz(CodigoOperacion : Integer ; var Cantidad : Integer) : Boolean;
    resourcestring
        MSG_ERROR = 'No se pudo obtener la cantidad de errores de la interfaz';
    begin
        Cantidad := 0;
        try
            Cantidad := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.ObtenerCantidadErroresInterfaz ('+IntToStr(CodigoOperacion)+')');
            Result := True;
        except
           on E : Exception do begin
               MsgBoxErr(MSG_ERROR, E.Message, Self.caption, MB_ICONERROR);
               Result := False;
           end;
        end;
    end;

     {-----------------------------------------------------------------------------
      Function Name:  ObtenerResumen
      Author:    lgisuk
      Date Created: 21/07/2006
      Description:  Obtengo el resumen del proceso realizado
      Parameters:
      Return Value: boolean
    -----------------------------------------------------------------------------}
    procedure ObtenerResumen (CodigoOperacionInterfase : Integer;Var CantRegs, Aprobados, Rechazados : Integer);
    resourcestring
      MSG_ERROR = 'Error';
      MSG_ERROR_QUERY = 'Error Obteniendo Resumen';
    begin
      try
          With spObtenerResumenComprobantesRecibidos, Parameters do begin
              Close;
              ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacionInterfase;
              CommandTimeout := 500;
              Open;
              CantRegs := Parameters.ParamByName('@CantidadComprobantesRecibidos').Value;
              Aprobados := Parameters.ParamByName('@CantidadComprobantesAceptados').Value;
              Rechazados := Parameters.ParamByName('@CantidadComprobantesRechazados').Value;
              Close;
          end;
      except
          on E : Exception do begin
              MsgBoxErr(MSG_ERROR_QUERY, E.Message, MSG_ERROR, MB_ICONERROR);
          end;
      end;
    end;

   {-----------------------------------------------------------------------------
      Function Name: ActualizarLog
      Author:    lgisuk
      Date Created: 21/07/2006
      Description: Actualizo el log al finalizar
      Parameters: CantidadErrores:integer
      Return Value: boolean
   -----------------------------------------------------------------------------}
   Function ActualizarLog (CantidadErrores : Integer) : Boolean;
   resourcestring
        MSG_ERROR = 'No se pudo Actualizar el log de operaciones';
   const
        STR_ERRORES = 'Finalizo OK!, Errores Validaci�n: ';
   var
        DescError : String;
   begin
        try
            Result := ActualizarLogOperacionesInterfaseAlFinal
                            (DMConnections.BaseCAC,FCodigoOperacion,STR_ERRORES+ IntToStr(CantidadErrores),DescError);
        except
            on E : Exception do begin
               MsgBoxErr(MSG_ERROR, E.Message, Self.caption, MB_ICONERROR);
               Result := False;
            end;
        end;
   end;

resourcestring
    MSG_PROCESS_CANCEL = 'Proceso cancelado por el usuario';
    MSG_PROCESS_NOT_COMPLETE = 'El proceso no se pudo completar';
    MSG_PROCESS_FINALLY_WITH_ERROR = 'El proceso finaliz� con Errores';
  	MSG_PROCESS_FINALLY_OK = 'Proceso finalizado con �xito';
Const
    STR_OPEN_FILE          = 'Abrir Archivo...';
    STR_CHECK_FILE         = 'Control del Archivo...';
    STR_CONFIRM            = 'Confirmar Cantidades...';
    STR_ANALYZING          = 'Analizando Rendiciones...';
    STR_REGISTER_OPERATION = 'Registrando Operacion en el Log de Operaciones...';
    STR_PROCESS            = 'Registrando los Pagos...';
var
    CantRegs, Aprobados, Rechazados : integer;
    CantidadErrores : Integer;
begin
    BtnProcesar.Enabled := False;
    PnlAvance.Visible := True;       //El Panel de Avance es visible durante el procesamiento
    KeyPreview := True;
  	try
        //Inicio la operacion

        //Abro el archivo
    		Lblmensaje.Caption := STR_OPEN_FILE;
    		if not Abrir then begin
            FErrorGrave := True;
            Exit;
        end;

        //Controlo el archivo
        Lblmensaje.Caption := STR_CHECK_FILE;
        if not Control then begin
            FErrorGrave := True;
            Exit;
        end;

        //El usuario acepta o no continuar con el proceso
        Lblmensaje.Caption := STR_CONFIRM;
        if not ConfirmaCantidades then begin
            FErrorGrave := True;
            Exit;
        end;

        //Analizo si hay errores de parseo o sintaxis en el archivo
        Lblmensaje.Caption := STR_ANALYZING;
        if not AnalizarRendicionesTxt then begin
            FErrorGrave := True;
            Exit;
        end;

        //Registro la operaci�n en el Log
        Lblmensaje.Caption := STR_REGISTER_OPERATION;
        if not RegistrarOperacion then begin
            FErrorGrave := True;
            Exit;
        end;

        //Proceso el archivo de Rendiciones
    		Lblmensaje.Caption := STR_PROCESS;
    		if not ActualizarRendiciones then begin
            FErrorGrave := True;
            Exit;
        end;

        // grabar los errores encontrados                                           // SS_1147_MBE_20150414
        GrabarErrores();                                                            // SS_1147_MBE_20150414

        //Obtengo la cantidad de errores
        ObtenerCantidadErroresInterfaz(FCodigoOperacion, CantidadErrores);

        //Actualizo el log al Final
        ActualizarLog(CantidadErrores);

  	finally
        pbProgreso.Position := 0;  //Inicio la Barra de progreso
        PnlAvance.visible := False;  //Oculto el Panel de Avance
    		Lblmensaje.Caption := '';
        if FCancelar then begin
            //Si fue cancelado por el usuario muestro mensaje que el proceso fue cancelado
            MsgBox(MSG_PROCESS_CANCEL, self.Caption, MB_OK + MB_ICONINFORMATION);
        end else if FErrorGrave then begin
            //Si se produjo un error grave muestro un cartel indicando que hubo errores
            MsgBox(MSG_PROCESS_NOT_COMPLETE, self.Caption, MB_OK + MB_ICONINFORMATION);
            //Genero el Reporte de Error con el Report Builder
            if (Ferrores.Count > 0) then GenerarReporteErroresSintaxis;
        end else begin

            //Obtengo Resumen del Proceso
            ObtenerResumen (FCodigoOperacion, CantRegs, Aprobados, Rechazados);

            //Muestro Mensaje de Finalizaci�n
            if MsgProcesoFinalizado(MSG_PROCESS_FINALLY_OK , MSG_PROCESS_FINALLY_WITH_ERROR , Caption , CantRegs , Aprobados , Rechazados) then begin
                //Genero el reporte de finalizacion de proceso
                GenerarReportedeFinalizacion(FCodigoOperacion);
            end;

        end;
        btnAbrirArchivo.Enabled := True;
        KeyPreview := False;
        Screen.Cursor := crDefault; // SS_1279_CQU_20150701
  	end;
end;

{-----------------------------------------------------------------------------
  Function Name: FormKeyDown
  Author:    lgisuk
  Date Created: 21/07/2006
  Description: permito cancelar la operacion
  Parameters: Sender: TObject;var Key: Word; Shift: TShiftState
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRecepcionRendicionesMisCuentas.FormKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
begin
    case Key of
        VK_ESCAPE: FCancelar := True;
    else
        FCancelar := False;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: SalirBTNClick
  Author:    lgisuk
  Date Created: 21/07/2006
  Description: cierro el formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRecepcionRendicionesMisCuentas.btnSalirClick(Sender: TObject);
begin
  	Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormCloseQuery
  Author:    lgisuk
  Date Created: 21/07/2006
  Description: impido salir si esta procesando
  Parameters: Sender: TObject;var CanClose: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRecepcionRendicionesMisCuentas.FormCloseQuery(Sender: TObject;var CanClose: Boolean);
begin
    //Permite salir solo si no esta procesando
    CanClose := not PnlAvance.visible;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 21/07/2006
  Description: lo libero de memoria
  Parameters: Sender: TObject;var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRecepcionRendicionesMisCuentas.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    Action := caFree;
end;

initialization
    FLista   := TStringList.Create;
    FErrores := TStringList.Create;
finalization
    FreeAndNil(Flista);
    FreeAndNil(FErrores);
end.


