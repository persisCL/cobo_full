object fGeneracionXMLInfracciones: TfGeneracionXMLInfracciones
  Left = 178
  Top = 184
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Generaci'#243'n de Disco XML de Infracciones'
  ClientHeight = 670
  ClientWidth = 562
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  DesignSize = (
    562
    670)
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = -1
    Top = 2
    Width = 555
    Height = 616
    Anchors = [akLeft, akTop, akRight, akBottom]
    ExplicitHeight = 671
  end
  object lblConcesionaria: TLabel
    Left = 23
    Top = 16
    Width = 85
    Height = 13
    Caption = 'Concesionaria:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object btnProcesar: TButton
    Left = 386
    Top = 636
    Width = 79
    Height = 25
    Caption = '&Procesar'
    Enabled = False
    TabOrder = 0
    OnClick = btnProcesarClick
  end
  object btnSalir: TButton
    Left = 471
    Top = 636
    Width = 79
    Height = 25
    Cancel = True
    Caption = '&Salir'
    TabOrder = 1
    OnClick = btnSalirClick
  end
  object GroupBox3: TGroupBox
    Left = 12
    Top = 39
    Width = 538
    Height = 89
    Caption = ' Datos '#218'ltimo Proceso '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    object Label4: TLabel
      Left = 30
      Top = 39
      Width = 221
      Height = 13
      Caption = 'Disco Infracciones NO Regularizadas :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 52
      Top = 58
      Width = 199
      Height = 13
      Caption = 'Disco Infracciones Regularizadas :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 94
      Top = 20
      Width = 157
      Height = 13
      Caption = 'Procesado hasta la Fecha :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblProcesadoHastaLaFecha: TLabel
      Left = 256
      Top = 20
      Width = 100
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Fecha no disponible'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lblUltimoDiscoNoRegularizadas: TLabel
      Left = 256
      Top = 39
      Width = 100
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lblUltimoDiscoRegularizadas: TLabel
      Left = 256
      Top = 58
      Width = 100
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
  end
  object GroupBox4: TGroupBox
    Left = 12
    Top = 130
    Width = 538
    Height = 320
    Caption = ' Datos Proceso '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
    object Label5: TLabel
      Left = 16
      Top = 20
      Width = 148
      Height = 13
      Caption = 'Procesar infracciones desde el '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 278
      Top = 20
      Width = 37
      Height = 13
      Caption = 'hasta el'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label12: TLabel
      Left = 16
      Top = 50
      Width = 236
      Height = 13
      Caption = 'Fecha Infracci'#243'n m'#225's antigua sin enviar :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblFechaMinimaInfraccion: TLabel
      Left = 259
      Top = 50
      Width = 100
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Fecha no disponible'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lblTotalInfraccionesAEnviar: TLabel
      Left = 259
      Top = 69
      Width = 100
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label9: TLabel
      Left = 75
      Top = 69
      Width = 177
      Height = 13
      Caption = 'Total Infracciones a Procesar :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label10: TLabel
      Left = 84
      Top = 88
      Width = 169
      Height = 13
      Caption = 'Infracciones Sin Regularizar :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblInfraccionesSinRegularizar: TLabel
      Left = 259
      Top = 88
      Width = 100
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label11: TLabel
      Left = 90
      Top = 107
      Width = 163
      Height = 13
      Caption = 'Infracciones Regularizadas :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblInfraccionesRegularizadas: TLabel
      Left = 259
      Top = 107
      Width = 100
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lblInfraccionesMorosas: TLabel
      Left = 89
      Top = 124
      Width = 163
      Height = 13
      Caption = 'Total Infracciones Morosas :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblInfraccionesMorosasTotal: TLabel
      Left = 259
      Top = 124
      Width = 100
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object deFechaDesde: TDateEdit
      Left = 170
      Top = 17
      Width = 97
      Height = 21
      AutoSelect = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnChange = deFechaHastaChange
      Date = -693594.000000000000000000
    end
    object deFechaHasta: TDateEdit
      Left = 330
      Top = 17
      Width = 97
      Height = 21
      AutoSelect = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnChange = deFechaHastaChange
      Date = -693594.000000000000000000
    end
    object GroupBox5: TGroupBox
      Left = 11
      Top = 147
      Width = 514
      Height = 110
      Caption = ' Opciones Directorios '
      TabOrder = 2
      DesignSize = (
        514
        110)
      object Label1: TLabel
        Left = 17
        Top = 63
        Width = 190
        Height = 13
        Caption = 'Directorio destino archivos XML :'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label13: TLabel
        Left = 17
        Top = 20
        Width = 121
        Height = 13
        Caption = 'Directorio im'#225'genes :'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object peDirectorioImagenes: TPickEdit
        Left = 17
        Top = 36
        Width = 486
        Height = 21
        Anchors = [akLeft, akTop, akRight]
        Enabled = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 0
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        OnChange = peDirectorioImagenesChange
        EditorStyle = bteTextEdit
        OnButtonClick = peDirectorioImagenesButtonClick
      end
      object peDirectorioXML: TPickEdit
        Left = 17
        Top = 79
        Width = 486
        Height = 21
        Anchors = [akLeft, akTop, akRight]
        Enabled = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 1
        OnChange = peDirectorioXMLChange
        EditorStyle = bteTextEdit
        OnButtonClick = peDirectorioXMLButtonClick
      end
    end
    object GroupBox6: TGroupBox
      Left = 11
      Top = 262
      Width = 514
      Height = 47
      Caption = ' Opciones de Disco '
      TabOrder = 3
      DesignSize = (
        514
        47)
      object chAnexarADisco: TCheckBox
        Left = 21
        Top = 15
        Width = 176
        Height = 23
        Anchors = [akTop, akRight]
        Caption = 'Continuar Disco Anterior'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnClick = chAnexarADiscoClick
      end
      object cbProcesarFacturados: TCheckBox
        Left = 199
        Top = 18
        Width = 273
        Height = 17
        Caption = 'Generar Disco Infracciones Regularizadas '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        OnClick = cbProcesarFacturadosClick
      end
    end
    object btnActualizarDatosProceso: TButton
      Left = 452
      Top = 15
      Width = 75
      Height = 25
      Caption = 'Actualizar'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      OnClick = btnActualizarDatosProcesoClick
    end
  end
  object gbEstado: TGroupBox
    Left = 12
    Top = 452
    Width = 538
    Height = 175
    Caption = ' Estado del Proceso: Sin Iniciar. '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
    DesignSize = (
      538
      175)
    object lblArchivosGenerados: TLabel
      Left = 153
      Top = 40
      Width = 6
      Height = 13
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lblInfraccionesRestantes: TLabel
      Left = 278
      Top = 42
      Width = 247
      Height = 13
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      AutoSize = False
      Caption = 'Procesando 9999 de 9999 infracciones'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label2: TLabel
      Left = 24
      Top = 40
      Width = 123
      Height = 13
      Caption = 'Archivos Generados :'
    end
    object pbProgreso: TProgressBar
      Left = 18
      Top = 19
      Width = 507
      Height = 17
      Anchors = [akLeft, akTop, akRight]
      Smooth = True
      Step = 1
      TabOrder = 0
    end
    object lsbxAvisos: TListBox
      Left = 16
      Top = 61
      Width = 508
      Height = 101
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ItemHeight = 13
      ParentFont = False
      TabOrder = 1
    end
  end
  object pnlAyuda: TPanel
    Left = 514
    Top = 11
    Width = 25
    Height = 23
    Anchors = [akTop, akRight]
    BevelInner = bvRaised
    TabOrder = 5
    object ImgAyuda: TImage
      Left = 2
      Top = 2
      Width = 21
      Height = 19
      Cursor = crHandPoint
      Align = alClient
      Picture.Data = {
        07544269746D617036100000424D361000000000000036000000280000002000
        0000200000000100200000000000001000000000000000000000000000000000
        0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00C6C6
        C600B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5
        B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5
        B500B5B5B500B5B5B500B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636B63006363
        6300636363006363630063636300636363006363630063636300636363006363
        6300636363006363630063636300636363006363630063636300636363006363
        63006363630063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF006363630094E7
        FF008CE7FF009CE7FF009CE7FF009CE7FF009CE7FF00A5EFFF00ADEFFF00ADEF
        FF00ADEFFF00ADEFFF00B5EFFF00BDEFFF00BDEFFF00BDEFFF00BDEFFF00BDEF
        FF00C6F7FF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00CEF7FF00D6F7FF00DEF7FF00DEF7
        FF00E7FFFF00E7FFFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00CEF7FF00D6F7FF00DEF7FF00DEF7
        FF00E7FFFF00E7FFFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00CEF7FF00D6F7FF00DEF7FF00DEF7
        FF00E7FFFF00E7FFFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF00E7FF
        FF00E7FFFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF006363
        630063636300EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF006363
        630063636300EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF009CD6
        FF00B5DEFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF009CD6
        FF00B5DEFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF006363
        63004AADFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF006363
        63000084FF00DEEFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF00B5DE
        FF0063636300108CFF00DEEFFF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF00E7FF
        FF009CD6FF0063636300108CFF00DEF7FF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF00E7FF
        FF00E7FFFF00DEEFFF0063636300108CFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00C6EFFF00ADDEFF00DEF7FF00E7FF
        FF00E7FFFF00EFFFFF0084C6FF0063636300A5D6FF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF006363630063636300DEF7FF00E7FF
        FF00E7FFFF00EFFFFF00A5D6FF0063636300A5D6FF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF0042A5FF006363630094D6FF00E7FF
        FF00E7FFFF00DEEFFF00108CFF0063636300DEF7FF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00C6EFFF00108CFF0063636300319C
        FF004AADFF000084FF00636363006BBDFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00CEEFFF0042A5FF006363
        6300636363006363630084C6FF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00D6F7FF00DEF7FF00DEF7FF00E7FF
        FF00E7FFFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00CEF7FF00D6F7FF00DEF7FF00DEF7
        FF00E7FFFF00E7FFFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636363008CE7
        FF00B5EFFF00C6EFFF00C6F7FF00CEF7FF00CEF7FF00D6F7FF00DEF7FF00DEF7
        FF00E7FFFF00E7FFFF00EFFFFF00F7FFFF00F7FFFF00F7FFFF00FFFFFF00FFFF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0063636300BDEF
        FF00BDEFFF00BDEFFF00BDEFFF00BDEFFF00BDEFFF00BDEFFF00BDEFFF00BDEF
        FF00BDEFFF00BDEFFF00BDEFFF00BDEFFF00BDEFFF00BDEFFF00BDEFFF00BDEF
        FF00BDEFFF0063636300B5B5B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00636B63006363
        6300636363006363630063636300636363006363630063636300636363006363
        6300636363006363630063636300636363006363630063636300636363006363
        63006363630063636300FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00}
      Stretch = True
      Transparent = True
      OnClick = ImgAyudaClick
      OnMouseMove = ImgAyudaMouseMove
      ExplicitLeft = 10
      ExplicitTop = -15
    end
  end
  object cbConcesionaria: TVariantComboBox
    Left = 114
    Top = 13
    Width = 381
    Height = 21
    Style = vcsDropDownList
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ItemHeight = 13
    ParentFont = False
    TabOrder = 6
    OnChange = cbConcesionariaChange
    Items = <>
  end
  object spObtenerInfraccionesAEnviarMOP: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerInfraccionesAEnviarMOP'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 295
    Top = 544
  end
  object spObtenerInfraccionesTransitosAEnviarMOP: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerInfraccionesTransitosAEnviarMOP'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipo'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoDisco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoArchivo'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoInfraccion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 329
    Top = 544
  end
  object spObtenerDatosInfraccionesAEnviarMOP: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDatosInfraccionesAEnviarMOP'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaDesde'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaHasta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoConcesionaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@SepararRegularizadas'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 176
    Top = 544
  end
  object fnObtenerNombreCortoConcesionaria: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerNombreCortoConcesionaria'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdReturnValue
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoConcesionaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 67
    Top = 541
  end
  object spCargarInfraccionesTransitosAEnviarMOP: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CommandTimeout = 120
    ProcedureName = 'CargarInfraccionesTransitosAEnviarMOP'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConcesionaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@FechaDesde'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaHasta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@SepararRegularizadas'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 139
    Top = 541
  end
  object fnObtenerUltimoArchivoDiscoXML: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerUltimoArchivoDiscoXML'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        Attributes = [paNullable]
        DataType = ftWord
        Direction = pdReturnValue
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoConcesionaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoTipo'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoDisco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 256
    Top = 544
  end
  object spActualizarInfraccionesTransitosAEnviarMOP: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarInfraccionesTransitosAEnviarMOP'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipo'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoDisco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoArchivo'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoInfraccion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDiscoAsignado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoArchivoAsignado'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Error'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end>
    Left = 107
    Top = 541
  end
  object spConsolidarInfraccionesTransitosAEnviarMOP: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ConsolidarInfraccionesTransitosAEnviarMOP;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConcesionaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@FechaDesde'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaHasta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoOperacionInterfase'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@DescError'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 255
        Value = Null
      end>
    Left = 216
    Top = 544
  end
end
