object FormInterfazImprentaInfractores: TFormInterfazImprentaInfractores
  Left = 167
  Top = 186
  Caption = 'Proceso masivo de impresi'#243'n de cartas a infractores'
  ClientHeight = 527
  ClientWidth = 712
  Color = clBtnFace
  Constraints.MinHeight = 428
  Constraints.MinWidth = 720
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  Visible = True
  OnClose = FormClose
  OnKeyDown = FormKeyDown
  DesignSize = (
    712
    527)
  PixelsPerInch = 96
  TextHeight = 13
  object nb: TNotebook
    Left = 0
    Top = 0
    Width = 712
    Height = 484
    Align = alTop
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 1
    object TPage
      Left = 0
      Top = 0
      Caption = 'Paso1'
      DesignSize = (
        712
        484)
      object Bevel1: TBevel
        Left = 8
        Top = 0
        Width = 696
        Height = 468
        Anchors = [akLeft, akTop, akRight, akBottom]
      end
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 521
        Height = 13
        Caption = 
          'Paso 1 - Seleccionar los infractores existentes para generar las' +
          ' Notificaciones a Infractores'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 20
        Top = 39
        Width = 70
        Height = 13
        Hint = 'Fecha inicial de las infracciones a procesar'
        Caption = 'Fecha  &Desde:'
        FocusControl = txt_FechaDesde
        ParentShowHint = False
        ShowHint = True
      end
      object Label5: TLabel
        Left = 20
        Top = 65
        Width = 64
        Height = 13
        Hint = 'Fecha final de las infracciones a procesar'
        Caption = 'Fecha &Hasta:'
        FocusControl = txt_FechaHasta
        ParentShowHint = False
        ShowHint = True
      end
      object Label2: TLabel
        Left = 21
        Top = 91
        Width = 65
        Height = 13
        Hint = 'Fecha de env'#237'o que figura en la carta'
        Caption = 'Fecha &Env'#237'o:'
        FocusControl = txt_FechaEnvio
        ParentShowHint = False
        ShowHint = True
      end
      object Label4: TLabel
        Left = 526
        Top = 427
        Width = 169
        Height = 13
        Anchors = [akRight, akBottom]
        Caption = 'Presione "Siguiente" para continuar'
        ExplicitTop = 294
      end
      object Label9: TLabel
        Left = 20
        Top = 119
        Width = 104
        Height = 13
        Caption = '&Reimprimir cancelada:'
        FocusControl = edProceso
        ParentShowHint = False
        ShowHint = False
      end
      object Label10: TLabel
        Left = 283
        Top = 35
        Width = 73
        Height = 13
        Hint = 'Fecha inicial de las infracciones a procesar'
        Caption = '&Concesionaria :'
        FocusControl = txt_FechaDesde
        ParentShowHint = False
        ShowHint = True
      end
      object dblInfractores: TDBListEx
        Left = 16
        Top = 148
        Width = 676
        Height = 273
        Anchors = [akLeft, akTop, akRight, akBottom]
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'Concesionaria'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'NombreCorto'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 80
            Header.Caption = 'RUT'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            Sorting = csAscending
            IsLink = False
            OnHeaderClick = dblInfractoresColumns0HeaderClick
            FieldName = 'NumeroDocumento'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 150
            Header.Caption = 'Nombre'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            OnHeaderClick = dblInfractoresColumns0HeaderClick
            FieldName = 'Nombre'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 90
            Header.Caption = 'Patente'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = dblInfractoresColumns0HeaderClick
            FieldName = 'Patente'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 100
            Header.Caption = 'Infracciones'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = dblInfractoresColumns0HeaderClick
            FieldName = 'CantidadInfracciones'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 220
            Header.Caption = 'Domicilio'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = dblInfractoresColumns0HeaderClick
            FieldName = 'Domicilio'
          end>
        DataSource = dsListaInfractores
        DragReorder = True
        ParentColor = False
        TabOrder = 5
        TabStop = True
      end
      object btnSiguiente: TButton
        Left = 616
        Top = 445
        Width = 78
        Height = 25
        Anchors = [akRight, akBottom]
        Caption = 'Siguiente'
        Enabled = False
        TabOrder = 4
        OnClick = btnSiguienteClick
      end
      object btnObtenerInfractores: TButton
        Left = 583
        Top = 117
        Width = 112
        Height = 25
        Anchors = [akTop, akRight]
        Caption = '&Obtener Infractores'
        TabOrder = 3
        OnClick = btnObtenerInfractoresClick
      end
      object txt_FechaDesde: TDateEdit
        Left = 128
        Top = 35
        Width = 124
        Height = 21
        Hint = 'Fecha inicial de las infracciones a procesar'
        AutoSelect = False
        TabOrder = 0
        Date = -693594.000000000000000000
      end
      object txt_FechaHasta: TDateEdit
        Left = 128
        Top = 61
        Width = 124
        Height = 21
        Hint = 'Fecha final de las infracciones a procesar'
        AutoSelect = False
        TabOrder = 1
        Date = -693594.000000000000000000
      end
      object txt_FechaEnvio: TDateEdit
        Left = 128
        Top = 87
        Width = 124
        Height = 21
        Hint = 'Fecha de env'#237'o que figura en la carta'
        AutoSelect = False
        TabOrder = 2
        Date = -693594.000000000000000000
      end
      object edProceso: TBuscaTabEdit
        Left = 128
        Top = 114
        Width = 197
        Height = 21
        Hint = 'B'#250'squeda de procesos de generaci'#243'n de interfaz cancelados'
        Enabled = True
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 6
        EditorStyle = bteTextEdit
        BuscaTabla = btOperacionesInterfaz
      end
      object CLB_Concesionarias: TVariantCheckListBox
        Left = 359
        Top = 35
        Width = 201
        Height = 97
        ItemHeight = 13
        Items = <>
        TabOrder = 7
      end
    end
    object TPage
      Left = 0
      Top = 0
      Caption = 'Paso2'
      ExplicitWidth = 0
      ExplicitHeight = 0
      DesignSize = (
        712
        484)
      object Bevel2: TBevel
        Left = 8
        Top = 8
        Width = 697
        Height = 468
        Anchors = [akLeft, akTop, akRight, akBottom]
        ExplicitHeight = 335
      end
      object Label6: TLabel
        Left = 16
        Top = 16
        Width = 404
        Height = 13
        Caption = 
          'Paso 2 - Creaci'#243'n del archivo de interface de infractores para i' +
          'mprenta'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object btnBrowseForFolder: TSpeedButton
        Left = 431
        Top = 71
        Width = 23
        Height = 22
        Caption = '...'
        OnClick = btnBrowseForFolderClick
      end
      object Label7: TLabel
        Left = 23
        Top = 54
        Width = 397
        Height = 13
        Caption = 
          'Seleccione la ubicaci'#243'n en d'#243'nde desea crear el archivo de inter' +
          'face'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object btnAnterior: TButton
        Left = 463
        Top = 443
        Width = 75
        Height = 25
        Anchors = [akRight, akBottom]
        Caption = 'Anterior'
        TabOrder = 0
        OnClick = btnAnteriorClick
      end
      object btnFinalizar: TButton
        Left = 625
        Top = 446
        Width = 75
        Height = 25
        Anchors = [akRight, akBottom]
        Caption = 'Finalizar'
        Enabled = False
        TabOrder = 1
        OnClick = btnFinalizarClick
      end
      object txtUbicacion: TEdit
        Left = 24
        Top = 72
        Width = 401
        Height = 21
        ReadOnly = True
        TabOrder = 2
      end
      object pnlprogreso: TPanel
        Left = 136
        Top = 279
        Width = 379
        Height = 137
        Anchors = [akLeft, akRight, akBottom]
        BevelOuter = bvNone
        Caption = 'Presione Escape o Cancelar para cancelar el proceso'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
        Visible = False
        DesignSize = (
          379
          137)
        object Label8: TLabel
          Left = 16
          Top = 20
          Width = 80
          Height = 13
          Anchors = [akLeft, akBottom]
          Caption = 'Progreso general'
        end
        object lblDescri: TLabel
          Left = 160
          Top = 0
          Width = 3
          Height = 13
        end
        object labelProgreso: TLabel
          Left = 16
          Top = 80
          Width = 341
          Height = 13
          Alignment = taCenter
          Anchors = [akLeft, akTop, akRight]
          AutoSize = False
          Caption = 'labelProgreso'
        end
        object pbProgreso: TProgressBar
          Left = 16
          Top = 40
          Width = 339
          Height = 17
          Anchors = [akLeft, akRight, akBottom]
          TabOrder = 0
        end
      end
      object btnCancelar: TButton
        Left = 544
        Top = 443
        Width = 75
        Height = 25
        Anchors = [akRight, akBottom]
        Caption = 'Cancelar'
        TabOrder = 4
        OnClick = btnCancelarClick
      end
    end
  end
  object btnSalir: TButton
    Left = 629
    Top = 496
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Salir'
    TabOrder = 0
    OnClick = btnSalirClick
  end
  object btnLimpiar: TButton
    Left = 548
    Top = 496
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Limpiar'
    TabOrder = 2
    OnClick = btnLimpiarClick
  end
  object dsListaInfractores: TDataSource
    DataSet = spResumirListaInterfazInfractores
    Left = 50
    Top = 496
  end
  object spGenerarListaInterfazInfractores: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'GenerarListaInterfazInfractores'
    Parameters = <>
    Left = 80
    Top = 496
  end
  object qryEliminarTemporales: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <>
    SQL.Strings = (
      'DELETE FROM ListaInterfazInfracciones')
    Left = 140
    Top = 496
  end
  object spActualizarOperacionEnCartasAInfractores: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarOperacionEnCartasAInfractores'
    Parameters = <>
    Left = 173
    Top = 496
  end
  object spAgruparListaInterfazInfractores: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'AgruparListaInterfazInfractores'
    Parameters = <>
    Left = 110
    Top = 496
  end
  object btOperacionesInterfaz: TBuscaTabla
    Caption = 'Notificaci'#243'n Masiva a Infractores'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HelpContext = 0
    Dataset = spObtenerOperacionesInterfazInfractores
    OnProcess = btOperacionesInterfazProcess
    OnSelect = btOperacionesInterfazSelect
    Left = 209
    Top = 495
  end
  object spObtenerOperacionesInterfazInfractores: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerOperacionesInterfazInfractores'
    Parameters = <>
    Left = 238
    Top = 495
  end
  object spResumirListaInterfazInfractores: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ResumirListaInterfazInfractores'
    Parameters = <>
    Left = 270
    Top = 495
  end
end
