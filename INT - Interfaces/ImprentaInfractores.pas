{********************************** File Header ********************************
File Name   : ImprentaInfractores.pas
Author      : rcastro
Date Created: 13/04/2005
Language    : ES-AR
Description : Notificaciones a infractores: interfaz para imprenta
*******************************************************************************}

unit ImprentaInfractores;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DMConnection, Util, UtilProc, UtilDB, StdCtrls, ExtCtrls, DB,
  ADODB, ListBoxEx, DBListEx, Buttons, DmiCtrls, DPSControls, ComCtrls,
  ConstParametrosGenerales, DateUtils, DBTables, BuscaTab, ComunesInterfaces,
  PeaTypes, Grids, DBGrids, PeaProcs, Mask, DBCtrls, Validate,
  DateEdit;

type
  TTiposPagos = (tpNinguno, tpPAT, tpPAC);
  TfrmImprentaInfractores = class(TForm)
	nb: TNotebook;
	Label1: TLabel;
	Bevel1: TBevel;
	Bevel2: TBevel;
    dblInfractores: TDBListEx;
	dsObtenerListaInfractores: TDataSource;
	Label6: TLabel;
	btnBrowseForFolder: TSpeedButton;
	Label7: TLabel;
	btnSiguiente: TButton;
	btnObtenerInfractores: TButton;
	btnAnterior: TButton;
	btnFinalizar: TButton;
	btnSalir: TButton;
	txtUbicacion: TEdit;
	pnlprogreso: TPanel;
	Label8: TLabel;
	pbProgreso: TProgressBar;
	lblDescri: TLabel;
	spObtenerListaInfractores: TADOStoredProc;
	qryEliminarTemporales: TADOQuery;
	labelProgreso: TLabel;
	Label3: TLabel;
	txt_FechaDesde: TDateEdit;
	Label5: TLabel;
	txt_FechaHasta: TDateEdit;
	spActualizarDetalleInfractoresEnviados: TADOStoredProc;
    Label2: TLabel;
    txt_FechaImpresion: TDateEdit;
    Label4: TLabel;
    spAgruparInfraccionesCarta: TADOStoredProc;
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure btnSalirClick(Sender: TObject);
	procedure btnSiguienteClick(Sender: TObject);
	procedure btnAnteriorClick(Sender: TObject);
	procedure btnObtenerInfractoresClick(Sender: TObject);
	procedure btnBrowseForFolderClick(Sender: TObject);
	procedure btnFinalizarClick(Sender: TObject);
	procedure FormKeyDown(Sender: TObject; var Key: Word;
	  Shift: TShiftState);
  private
	FDestino, FNombreArchivo, FNombreArchivoControl: AnsiString;
	FOutFile: TextFile;
	FCancel, FOnProcess: Boolean;
	FCantidadCartas,
	FCodOperacionesInterfase: Integer;
	FFileBuffer: TStringList;
	function GenerarArchivo:Boolean;
	function ActualizarInfractoresEnviados : boolean;
	function CrearArchivo(Archivo: String; var DescriError: AnsiString):Boolean;
	function  RegistrarOperacion : boolean;
	procedure ResetearPreview;
	procedure CrearNombresArchivos(Path: String);
	procedure HabilitarControles(Habilitado: Boolean);
	function EscribirArchivoControl: Boolean;
  public
	function Inicializar: Boolean;
  end;

resourcestring
	MSG_SELECT_LOCATION     = 'Seleccione una ubicaci�n para el archivo';
	MSG_CANNOT_CREATEFILE   = 'Error al crear el archivo';
	MSG_ERROR               = 'Error';

Const
	SEPARADOR               = '|';
	FILE_EXTENSION          = '.txt';
	FILE_PREFIX				= 'INFR-CN-';
	CONTROL_FILE_PREFIX     = 'INFR-CN-CONTROL-';

var
  frmImprentaInfractores: TfrmImprentaInfractores;


implementation

uses FrmMain;

{$R *.dfm}

{$Message Hint 'Definir que va en el archivo, si el detalle de c/infracci�n o la cantidad de ellas'}

{******************************** Function Header ******************************
Function Name: Inicializar
Author       : rcastro
Date Created : 13/04/2005
Description  : Inicializa el formulario
Parameters   : None
Return Value : Boolean
*******************************************************************************}
function TfrmImprentaInfractores.Inicializar: Boolean;
resourcestring
	MSG_INIT_ERROR = 'Error al Inicializar';
	MSG_ERROR_CARGANDO_PARAMETROS = 'Error cargando los par�metros generales.';
	MSG_PARAMETER_NOT_EXISTS = 'El par�metro DIR_SALIDA_IMPRENTA_INFRACTORES no existe en la base de datos.';
begin
	Result := False;
	try
		Result := DMConnections.BaseCAC.Connected;
		if Result then begin
			nb.PageIndex := 0;
			result := ObtenerParametroGeneral(DMConnections.BaseCAC, 'DIR_SALIDA_IMPRENTA_INFRACTORES', FDestino);
			if not result then begin
				msgBoxErr(MSG_ERROR_CARGANDO_PARAMETROS, MSG_PARAMETER_NOT_EXISTS, caption, MB_ICONERROR);
				exit;
			end;
			FOnProcess := False;

			CrearNombresArchivos(FDestino);
			FFileBuffer:= TStringList.Create;
			txt_FechaImpresion.Date := NowBase(DMConnections.BaseCAC);

			spObtenerListaInfractores.Close;
		end;
	except
		on e: Exception do begin
			MsgBoxErr(MSG_INIT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
		end;
	end;

//    cbIncluirYaNotificadas.Checked := False;
	btnSiguiente.Enabled := false
end;

procedure TfrmImprentaInfractores.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	if FOnProcess then begin
		Action := caNone;
		Exit;
	end;

	Action := caFree;
end;

procedure TfrmImprentaInfractores.btnSalirClick(Sender: TObject);
begin
	Close;
end;

procedure TfrmImprentaInfractores.btnSiguienteClick(Sender: TObject);
begin
	nb.PageIndex := nb.PageIndex + 1;
	btnFinalizar.Enabled := True;
end;

procedure TfrmImprentaInfractores.btnAnteriorClick(Sender: TObject);
begin
	nb.PageIndex := nb.PageIndex - 1;
end;

procedure TfrmImprentaInfractores.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	case Key of
		VK_ESCAPE:
			begin
				//Todo mal, nos canecelaron en el medio
				FCancel := True;
			end;
	end;
end;

{******************************** Function Header ******************************
Function Name: CrearNombresArchivos
Author       : rcastro
Date Created : 13/04/2005
Description  : Define el nombre del archivo de salida y de control
Parameters   : Path: String
Return Value : None
*******************************************************************************}
procedure TfrmImprentaInfractores.CrearNombresArchivos(Path: String);
begin
	FNombreArchivoControl   := GoodDir(Path) + CONTROL_FILE_PREFIX + FormatDateTime('ddmmyyyy', NowBase(DMConnections.BaseCAC)) + FILE_EXTENSION;
	FNombreArchivo          := FILE_PREFIX  + FormatDateTime('ddmmyyyy', NowBase(DMConnections.BaseCAC)) + FILE_EXTENSION;
	txtUbicacion.Text       := GoodDir(Path) + FNombreArchivo;
end;

procedure TfrmImprentaInfractores.ResetearPreview;
begin
	btnObtenerInfractores.Click;
end;

procedure TfrmImprentaInfractores.HabilitarControles(Habilitado: Boolean);
begin
	txtUbicacion.Enabled        := Habilitado;
	btnBrowseForFolder.Enabled  := Habilitado;
end;

{******************************** Function Header ******************************
Function Name: RegistrarOperacion
Author       : rcastro
Date Created : 13/04/2005
Description  : Registra la operaci�n de interfaz
Parameters   : None
Return Value : boolean
*******************************************************************************}
function TfrmImprentaInfractores.RegistrarOperacion : boolean;
resourcestring
	MSG_COULD_NOT_REGISTER_OPERATION = 'No se pudo registrar la operaci�n';
	MSG_ERROR = 'Error';
var
	DescError : string;
begin
	result := RegistrarOperacionEnLogInterface( DMConnections.BaseCAC, RO_MOD_INTERFAZ_SALIENTE_CARTAS_INFRACTORES, NowBase(DMConnections.BaseCAC),
      ExtractFileName(FNombreArchivo), UsuarioSistema, '', True, NowBase(DMConnections.BaseCAC), FCodOperacionesInterfase, DescError);

	if not result then
		MsgBoxErr(MSG_COULD_NOT_REGISTER_OPERATION, DescError, MSG_ERROR, MB_ICONERROR);
end;

{******************************** Function Header ******************************
Function Name: btnObtenerInfractoresClick
Author       : rcastro
Date Created : 13/04/2005
Description  : Obtiene la lista de infractores seg�n filtros ingresados
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmImprentaInfractores.btnObtenerInfractoresClick(Sender: TObject);
resourcestring
	MSG_ERROR_GETTING_INFRACTORS = 'Error al obtener los infractores';
	MSG_ERROR_DATE_ORDER= 'La fecha desde debe ser menor que la fecha hasta.';
begin
	// Valida que la fecha Desde no sea mayor que la fecha Hasta
	if (txt_FechaDesde.Date<>nulldate) and (txt_FechaHasta.Date<>nulldate) and (txt_FechaDesde.Date>txt_FechaHasta.Date) then begin
		MsgBoxBalloon(MSG_ERROR_DATE_ORDER,Caption,MB_ICONSTOP,txt_FechaDesde);
		exit;
	end;

	try
		qryEliminarTemporales.ExecSQL
	except
		on e: Exception do //
	end;

    cursor := crHourGlass;
	try
		try
			with spObtenerListaInfractores, Parameters do begin
				Close;
				Parameters.Refresh;
				Parameters.ParamByName('@FechaDesde').Value		:= iif(txt_FechaDesde.Date=nulldate,null,txt_FechaDesde.Date);
				Parameters.ParamByName('@FechaHasta').Value		:= iif(txt_FechaHasta.Date=nulldate,null,txt_FechaHasta.Date);
//				Parameters.ParamByName('@IncluirYaNotificadas').Value:= cbIncluirYaNotificadas.Checked;
				Open;
			end
		except
			on e: Exception do begin
                cursor := crDefault;
				MsgBoxErr(MSG_ERROR_GETTING_INFRACTORS, e.Message, MSG_ERROR, MB_ICONERROR);
			end;
		end
	finally
    cursor := crDefault;
		btnSiguiente.Enabled := not spObtenerListaInfractores.IsEmpty
	end
end;

{******************************** Function Header ******************************
Function Name: btnBrowseForFolderClick
Author       : rcastro
Date Created : 13/04/2005
Description  : Define ubicaci�n y nombre del archivo a generar
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmImprentaInfractores.btnBrowseForFolderClick(Sender: TObject);
var
	Location: String;
begin
	Location := Trim(BrowseForFolder(MSG_SELECT_LOCATION));
	if Location = '' then exit;
	FNombreArchivo := '';
	txtUbicacion.Text := Location;
	FDestino := GoodDir(txtUbicacion.Text);

	CrearNombresArchivos(GoodDir(txtUbicacion.Text));

	btnFinalizar.Enabled := True;
end;

{******************************** Function Header ******************************
Function Name: CrearArchivo
Author       : rcastro
Date Created : 13/04/2005
Description  : Crea el archivo para imprenta
Parameters   : Archivo: String; var DescriError: AnsiString
Return Value : Boolean
*******************************************************************************}
function TfrmImprentaInfractores.CrearArchivo(Archivo: String; var DescriError: AnsiString):Boolean;
resourcestring
	MSG_FILE_EXISTS = 'El archivo %s ya existe en el directorio de destino. �Desea reemplazarlo?';
	MSG_ERROR_DESCRIPTION = 'Se ha cancelado el proceso debido a la existencia de un archivo id�ntico en el directorio de destino';
	MSG_WARNING = 'Atenci�n';
begin
	Result := False;
	//Verificamos que no exista uno con el mismo nombre
	if FileExists(Archivo) then begin
		if (MsgBox(Format (MSG_FILE_EXISTS, [Archivo]), MSG_WARNING, MB_YESNO + MB_ICONQUESTION) = IDNO) then begin
			DescriError := MSG_ERROR_DESCRIPTION;
			Exit;
		end;
	end;
	// Listo, intentamos crearlo
	try
		AssignFile(FOutFile, Trim(Archivo));
		Rewrite(FOutFile);
		Result := True;
		CloseFile(FOutFile);
	except
		on e:Exception do begin
			CloseFile(FOutFile);
			DescriError := e.Message;
		end;
	end;
end;

{******************************** Function Header ******************************
Function Name: btnFinalizarClick
Author       : rcastro
Date Created : 14/04/2005
Description  : Registro de notificaciones y generacion de archivo de interfaz
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmImprentaInfractores.btnFinalizarClick(Sender: TObject);
resourcestring
	MSG_PROCESS_FAIL            = 'El proceso finaliz� con errores';
	MSG_DELETE_ERROR            = 'Error al eliminar el archivo de interface';
	MSG_PROCESS_FINISHED        = 'El proceso finaliz� correctamente';
	MSG_PROCESS_CANCELLED       = 'Proceso cancelado';
var
	Error: String;
	TodoOk: Boolean;
begin
	//Comenzamos a generar el archivo de interface
	CrearNombresArchivos(FDestino);

	// Intentamos crear el archivo
	if not CrearArchivo(txtUbicacion.Text, Error) then begin
		MsgBoxErr(MSG_ERROR, Error, MSG_CANNOT_CREATEFILE, MB_ICONERROR);
		Exit;
	end;

	txt_FechaImpresion.Date := iif(txt_FechaDesde.Date= nulldate, NowBase(DMConnections.BaseCAC), txt_FechaImpresion.Date);

	spAgruparInfraccionesCarta.Open;
	HabilitarControles(False);
	btnFinalizar.Enabled := False;
	btnSalir.Enabled    := False;
	FCancel             := False;
	pbProgreso.Step     := 1;
	pbProgreso.Min      := 0;
	pbprogreso.Max 		:= spAgruparInfraccionesCarta.RecordCount;
	pbProgreso.Smooth   := True;
	pbProgreso.Position := 0;
	KeyPreview          := True;
	pnlProgreso.Visible := True;
	btnAnterior.Enabled := False;
	FOnProcess          := True;
	TodoOk				:= False;

    // Comienzo transacci�n
	Screen.Cursor := crHourGlass;
	DMConnections.BaseCAC.BeginTrans;
	try
		try
			TodoOk := RegistrarOperacion and ActualizarInfractoresEnviados and
					    GenerarArchivo and EscribirArchivoControl
		except
			on e:Exception do begin
        		Screen.Cursor := crDefault;
				MsgBoxErr(MSG_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
			end;
		end;
	finally
        if TodoOk then begin
            DMConnections.BaseCAC.CommitTrans;
    		Screen.Cursor := crDefault;

			MsgBox(MSG_PROCESS_FINISHED);
        end
        else begin
			// Errores � Cancelaron
    		if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.RollbackTrans;
    		Screen.Cursor := crDefault;

			if FCancel then begin
				labelProgreso.Caption := MSG_PROCESS_CANCELLED;
				MsgBox(MSG_PROCESS_CANCELLED);
			end
            else MsgBox(MSG_PROCESS_FAIL);

            if FileExists(txtUbicacion.Text) then begin
                try
                    DeleteFile(txtUbicacion.Text);
                except
                    on e: Exception do begin
                        MsgBoxErr(MSG_DELETE_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
                    end;
                end
            end;

            if FileExists(FNombreArchivoControl) then begin
                try
                    DeleteFile(FNombreArchivoControl);
                except
                    on e: Exception do begin
                        MsgBoxErr(MSG_DELETE_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
                    end;
                end
            end;
		end;

		spAgruparInfraccionesCarta.Close;
		qryEliminarTemporales.ExecSQL;
		FOnProcess := False;
		KeyPreview := False;
		pbProgreso.Position := 0;
		btnAnterior.Enabled := True;
		btnSalir.Enabled := True;
		txtUbicacion.Clear;
		lblDescri.Caption := '';
		ResetearPreview;
		HabilitarControles(True);
	end;
end;

{******************************** Function Header ******************************
Function Name: ActualizarInfractoresEnviados
Author       : rcastro
Date Created : 14/04/2005
Description  : Registra las infracciones notificadas
Parameters   : None
Return Value : boolean
*******************************************************************************}
function TfrmImprentaInfractores.ActualizarInfractoresEnviados : boolean;
resourcestring
	MSG_ERROR_UPDATING_INFRACTORS_DETAIL = 'Error actualizando detalle de infractores enviados';
	MSG_PROCESSING_INFRACTORS = 'Procesando %d infracciones... ';
begin
	labelProgreso.Caption := FORMAT(MSG_PROCESSING_INFRACTORS, [pbprogreso.Max]);
	labelProgreso.Update;
	pbProgreso.Position := pbprogreso.Max;
	self.Repaint;

    try
        spActualizarDetalleInfractoresEnviados.Parameters.ParamByname ('@CodigoOperacionInterfase').Value := FCodOperacionesInterfase;
        spActualizarDetalleInfractoresEnviados.ExecProc;
        result := True;
    except
        on e:Exception do begin
            result := false;
            raise Exception.Create(MSG_ERROR_UPDATING_INFRACTORS_DETAIL + Space(1) + e.message );
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: GenerarArchivo
Author       : rcastro
Date Created : 14/04/2005
Description  : Genera el archivo de interfaz
Parameters   : None
Return Value : Boolean
*******************************************************************************}
function TfrmImprentaInfractores.GenerarArchivo: Boolean;
resourcestring
	MSG_ERROR_WRITING_FILE = 'Error generando el archivo para imprenta.';
	MSG_WRITING_FILE = 'Escribiendo los datos al disco.';
	LETTER_TYPE	  = 'INFR18';
	CLIENT_PREFIX = '5000';
	LETTER_PREFIX = '6000';
	DETAIL_PREFIX = '7000';
	RESUME_PREFIX = '9000';
	DETAIL_FIELDS = '3';
	RESUME_FIELDS = '2';
	LETTER_SIGNATURE_1 = 'F001';
	LETTER_SIGNATURE_2 = 'F002';
var
	FDocumento : String;
begin
	labelProgreso.Caption := MSG_WRITING_FILE;
	pbProgreso.Position := 0;
	self.Repaint;

    try
        AssignFile(FOutFile, Trim(txtUbicacion.Text));
        Rewrite(FOutFile);

        with spAgruparInfraccionesCarta do begin
            DisableControls;
            FDocumento := '';
            FCantidadCartas := 0;

            First;
            while not EoF do begin
                if FDocumento <> Trim(FieldByName('NumeroDocumento').AsString) then begin
                    FCantidadCartas := FCantidadCartas + 1;
                    FDocumento := Trim(FieldByName('NumeroDocumento').AsString);

                    // Datos Cliente
                    WriteLn (FOutFile,
                        CLIENT_PREFIX, SEPARADOR,
                        Trim(FieldByName('Nombre').AsString), SEPARADOR,
                        SEPARADOR, SEPARADOR, SEPARADOR, SEPARADOR,
                        Trim(FieldByName('Region').AsString), SEPARADOR,
                        Trim(FieldByName('Comuna').AsString), SEPARADOR,
                        Trim(FieldByName('Calle').AsString), SEPARADOR,
                        Trim(FieldByName('Numero').AsString), SEPARADOR,
                        Trim(FieldByName('Detalle').AsString), SEPARADOR,
                        Trim(FieldByName('CodigoPostal').AsString), SEPARADOR);

                    // Datos Carta
                    WriteLn (FOutFile,
                        LETTER_PREFIX, SEPARADOR,
                        LETTER_TYPE, SEPARADOR,
                        FormatDateTime ('dd/mm/yyyy', txt_FechaImpresion.Date), SEPARADOR,
                        LETTER_SIGNATURE_1, SEPARADOR);
                end;

                // Item Carta
                WriteLn (FOutFile,
                    DETAIL_PREFIX, SEPARADOR,
                    LETTER_TYPE, SEPARADOR,
                    DETAIL_FIELDS, SEPARADOR,
                    Trim(FieldByName('Patente').AsString), SEPARADOR,
                    Trim(FieldByName('Categoria').AsString), SEPARADOR,
                    FormatDateTime ('dd/mm/yyyy', FieldByName('FechaHora').AsDateTime), SEPARADOR);
                pbProgreso.Position := pbProgreso.Position + 1;

                Next;
            end;

            EnableControls;
        end;

        CloseFile(FOutFile);
        Result := True;
    except
        on e: Exception do begin
            result := false;
            raise Exception.Create(MSG_ERROR_WRITING_FILE + Space(1) + e.message );
        end;
    end;

	labelProgreso.Caption := '';
end;

{******************************** Function Header ******************************
Function Name: EscribirArchivoControl
Author       : rcastro
Date Created : 14/04/2005
Description  : Genera el archivo de control de interfaz
Parameters   : None
Return Value : Boolean
*******************************************************************************}
function TfrmImprentaInfractores.EscribirArchivoControl: Boolean;
resourcestring
	MSG_CONTROL_WRITE_FAIL = 'Error generando el archivo de control.';
const
	CONTROL_PREFIX = '8000';
var
	Linea: AnsiString;
begin
    try
        Linea := CONTROL_PREFIX + SEPARADOR +
                    IntToStr(spAgruparInfraccionesCarta.RecordCount + 2*FCantidadCartas) + SEPARADOR +
                    IntToStr(FCantidadCartas) + SEPARADOR;
        StringToFile(Linea, FNombreArchivoControl);

        Result := True;
    except
        on e: Exception do begin
            result := false;
            raise Exception.Create(MSG_CONTROL_WRITE_FAIL + Space(1) + e.message );
        end;
    end;
end;

end.

