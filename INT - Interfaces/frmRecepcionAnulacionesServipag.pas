{------------------------------------------------------------------------------
 File Name: frmRecepcionAnulacionesServipag.pas
 Author: nefernandez
 Date Created: 13/07/2007
 Language: ES-AR
 Description: M�dulo de la interface Servipag - Recepci�n de Anulaciones
 Revision : 1
     Author : vpaszkowicz
     Date : 19/11/2007
     Description :Cambio el lugar desde donde tomo la suma en el footer, ya
    que se estaba tomando como las rendiciones, que corresponden al campo in-
    gresos. Ahora las tomo desde el campo egresos de la especificaci�n de
    anulaciones.

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

Firma		:	SS_1259_MBE_20150506
Description	:	Se agrega el formato de anulaciones de VS

Firma		:	SS_1259_MCA_20150512
Description	:	Continuacion del desarrollo.

Firma       :   SS_1285_CQU_20150529
Descripcion :   Se modifica el proceso para optimizarlo y, en el caso de VS, quitar la validacion del CodigoServicio

Firma       : SS_1340_NDR_20150708
Desripcion  : No procesa las anulaciones (en VS envian TipoComprobanteFiscal y NumeroComprobanteFiscal)
              Se graban los errores acumulados en la lista de errores interfaces (ya no graba errores en el SP)

Firma       :   SS_1340_CQU_20150806
Desripcion  :   Se incorporan los siguientes cambios solicitados por Miguel Apablaza
                    �	Fecha y Hora contable deber�an quedar con la fecha y hora contable de la rendici�n.
                        Ahora est� quedando con 30-12-1899
                    �	Forma de pago deber�a ser CHEQUE en vez de CASH
                    �	Agregar los campos TipoComprobanteOriginal y NotaCobroOriginal a la tabla AnulacionesServipag.
                        En los campos �original� deber�an quedar registrados el tipo y n�mero de comprobante fiscal
                        enviado en el archivo. En los campos TipoComprobante y NotaCobro deber�an quedar
                        los datos del comprobante interno (NK o SD)

Firma       :   SS_1373_CQU_20150901
Descripcion :   Se detecta un ERROR el cual no indica el numero comprobante cuando hay error de parseo.
                Se agrega limpieza de variable DescError para evaluar si ActualizarAnulacion o no.
                Tanto en CN como en VS se coloca el valor de NumeroComprobante y NumeroComprobanteFiscal
                en el campo Anulacion.NotaCobro por lo tanto no es valido enviar NumeroComprobanteFiscal
                ya que para CN no funcionaria.
--------------------------------------------------------------------------------}
unit frmRecepcionAnulacionesServipag;

interface

uses
  DMConnection,
  Util,
  UtilProc,
  ConstParametrosGenerales,
  ComunesInterfaces,
  PeaTypes,
  PeaProcs,
  UtilDB,
  FrmRptErroresSintaxis,
  frmRptRecepcionAnulacionesServipag,
  // General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StrUtils, ExtCtrls, Buttons, StdCtrls, DB, ADODB, ComCtrls;

type

  //Estructura para almacenar cada registro de anulaci�n
  TAnulacion = record
  	Fecha : TDateTime;
    EPS : string;
    Sucursal : integer;
    Terminal : string;
    Correlativo : integer;
    FechaContable : TDateTime;
    CodigoServicio : string;
    TipoOperacion : string;
    IndicadorContable : string;
    Monto : integer;
    MedioPago : string;
    CodigoBancoCheque : integer;
    CuentaCheque : string;
    SerieCheque : integer;
    PlazaCheque : integer;
    TipoTarjeta : string;
    MarcaTarjeta : string;
    NumeroTarjeta : string;
    ExpiracionTarjeta : string;
    TipoCuotas : string;
    NumeroCuotas : integer;
    Autorizacion : string;
    NotaCobro : integer;
    TipoComprobanteFiscal : string;						// SS_1259_MBE_20150506
    NumeroComprobanteFiscal : string;					// SS_1259_MBE_20150506
    Identificador : string;
  end;

  TRecepcionAnulacionesServipagForm = class(TForm)
    Bevel1: TBevel;
    btnProcesar: TButton;
    btnSalir: TButton;
    pnlOrigen: TPanel;
    txtOrigen: TEdit;
    Label1: TLabel;
    btnAbrirArchivo: TSpeedButton;
    pnlAyuda: TPanel;
    imgAyuda: TImage;
    OpenDialog: TOpenDialog;
    spProcesarAnulacionesServipag: TADOStoredProc;
    pnlAvance: TPanel;
    Label2: TLabel;
    pbProgreso: TProgressBar;
    lblMensaje: TLabel;
    spObtenerResumenAnulacionesServipag: TADOStoredProc;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSalirClick(Sender: TObject);
    procedure btnAbrirArchivoClick(Sender: TObject);
    procedure btnProcesarClick(Sender: TObject);
    procedure txtOrigenChange(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure imgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure imgAyudaClick(Sender: TObject);
  private
    { Private declarations }
    FCodigoConcesionariaNativa : Integer;											// SS_1259_MBE_20150506
    FCodigoOperacion: Integer;
    FErrorGrave: Boolean;
    FCancelar: Boolean;
    FCantidadControl: Real;
    FSumaControl: Real;
    FSumaReal: Real;
    FTotalRegistros: Integer;
    FServipag_Directorio_Anulaciones : AnsiString;
    //FServipag_Directorio_Procesados : AnsiString;									// SS_1259_MCA_20150512
    FServipag_Directorio_Errores : AnsiString;
    FCodigoServicio: String;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    procedure InicializarRegistroAnulacion(var Anulacion: TAnulacion);
    function VerificarParametrosGenerales : Boolean;
  	function AbrirArchivoTxt: Boolean;
  	function AnalizarAnulacionesTxt: Boolean;
    function ControlarArchivoTxt: Boolean;
  	function CargarRegistroAnulacion(Linea: String; var Anulacion: TAnulacion; var DescError: String): Boolean;
  	function RegistrarOperacion: Boolean;
    function ProcesarAnulaciones: Boolean;
    function ActualizarAnulacion(Anulacion: TAnulacion; var DescError: String): Boolean;
    function ObtenerCantidadErroresInterfaz(CodigoOperacion: Integer; var Cantidad: Integer): Boolean;
    function ConfirmaCantidades: Boolean;
    function ActualizarLog(CantidadErrores: Integer): Boolean;
    function GenerarReporteErroresSintaxis: Boolean;
    procedure ObtenerResumen(CodigoOperacionInterfase: Integer; var CantRegs, Aprobados, Rechazados: Integer);
    function GenerarReporteDeFinalizacion(CodigoOperacionInterfase: Integer): Boolean;
    function EsArchivoValido(Nombre: AnsiString): Boolean;
    procedure GrabarErrores;                                                                        //SS_1340_NDR_20150708
  public
    { Public declarations }
  	function Inicializar(txtCaption: AnsiString): Boolean;
  end;

var
  RecepcionAnulacionesServipagForm: TRecepcionAnulacionesServipagForm;
  FLista: TStringList;
  FErrores: TStringList;

implementation

{$R *.dfm}

function TRecepcionAnulacionesServipagForm.VerificarParametrosGenerales : Boolean;
resourcestring
    MSG_ERROR_CHECK_GENERAL_PARAMETER = 'Error al verificar parametro general';
    MSG_ERROR                         = 'Error';
    STR_NOT_EXISTS_GENERAL_PARAMETER  = 'No existe parametro general: ';
    STR_EMPTY_GENERAL_PARAMETER       = 'Parametro general vacio: ';                                         //Se verifica que los codigos no esten vacios, si no tienen razon de ser
    STR_DIRECTORY_NOT_EXISTS          = 'No existe el directorio indicado en parametros generales: ' + CRLF; //Los directorios de destino y errores en procesos de envio son criticos
const
    SERVIPAG_DIRECTORIO_ANULACIONES        = 'Servipag_Directorio_Anulaciones';
    SERVIPAG_DIRECTORIO_ERRORES            = 'Servipag_Directorio_Errores';
    //SERVIPAG_DIRECTORIO_PROCESADOS         = 'Servipag_Directorio_Procesados';
var
    DescError : AnsiString;
begin
    Result    := True;
    DescError := '';
    try
        try

            //Obtengo el Parametro General
            if not ObtenerParametroGeneral(DMConnections.BaseCAC, SERVIPAG_DIRECTORIO_ANULACIONES , FServipag_Directorio_Anulaciones) then begin
                DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + SERVIPAG_DIRECTORIO_ANULACIONES;
                Result := False;
                Exit;
            end;
            //Verifico que sea v�lido
            FServipag_Directorio_Anulaciones := GoodDir(FServipag_Directorio_Anulaciones);
            if  not DirectoryExists(FServipag_Directorio_Anulaciones) then begin
                DescError := STR_DIRECTORY_NOT_EXISTS + FServipag_Directorio_Anulaciones;
                Result := False;
                Exit;
            end;

            //Obtengo el Parametro General
            if not ObtenerParametroGeneral(DMConnections.BaseCAC, SERVIPAG_DIRECTORIO_ERRORES , FServipag_Directorio_Errores) then begin
                DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + SERVIPAG_DIRECTORIO_ERRORES;
                Result := False;
                Exit;
            end;
            //Verifico que sea v�lido
            FServipag_Directorio_Errores := GoodDir(FServipag_Directorio_Errores);
            if  not DirectoryExists(FServipag_Directorio_Errores) then begin
                DescError := STR_DIRECTORY_NOT_EXISTS + FServipag_Directorio_Errores;
                Result := False;
                Exit;
            end;

            //Obtengo el parametro general
            //ObtenerParametroGeneral(DMConnections.BaseCAC,SERVIPAG_DIRECTORIO_PROCESADOS, FServipag_Directorio_Procesados);

        except
            on E : Exception do begin
                Result := False;
                MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, E.Message, MSG_ERROR, MB_ICONWARNING);
            end;
        end;

    finally
        if (Result = False) then begin
            MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, DescError, MSG_ERROR, MB_ICONWARNING);
        end;
    end;
end;

function TRecepcionAnulacionesServipagForm.AbrirArchivoTxt: Boolean;
resourcestring
  	MSG_OPEN_FILE_ERROR = 'Error al abrir el archivo de anulaciones';
begin
    Result := False;
    FLista.text := FileToString(txtOrigen.text);
    FTotalRegistros := FLista.Count - 1;					// SS_1259_MCA_20150512
    if FLista.text <> '' then  begin
        Result := True;
    end else begin
        MsgBox(MSG_OPEN_FILE_ERROR, Self.Caption, MB_OK + MB_ICONINFORMATION);
    end;
end;

{-----------------------------------------------------------------------------
Revisi�n: 1
Author: nefernandez
Date: 22/08/2007
Description: Se controla que el registro de HEADER de anulaciones servipag
tenga la descripci�n adecuada
Revision : 2
    Author : vpaszkowicz
    Date : 19/11/2007
    Description : Cambio el lugar desde donde tomo la suma en el footer, ya
    que se estaba tomando como las rendiciones, que corresponden al campo in-
    gresos. Ahora las tomo desde el campo egresos de la especificaci�n de
    anulaciones.
-----------------------------------------------------------------------------}
function TRecepcionAnulacionesServipagForm.ControlarArchivoTxt: Boolean;
resourcestring
    MSG_INVALID_FOOTER 	= 'El pie de p�gina del archivo es inv�lido!';
    MSG_NOT_EQUAL_COUNT = 'La cantidad reflejada en el registro de Footer no coincide'+ CRLF + 'con la cantidad calculada!';
    MSG_NOT_EQUAL_SUM   = 'La suma reflejada en el registro de Footer no coincide con la suma calculada!';
    MSG_CANT_CONTROL    = 'Cantidad Control: ';
    MSG_CANT_CALC       = 'Cantidad Calculada: ';
    MSG_SUM_CONTROL     = 'Suma Control: ';
    MSG_SUM_CALC        = 'Suma Calculada: ';
    MSG_FECHA_PROCESO	= 'Fecha de Proceso';									  															// SS_1259_MBE_20150506
    MSG_CANT_NO_COINCIDE = 'No coincide la cantidad de registros informada en el archivo con la cantidad real del l�neas del archivo';		 // SS_1259_MBE_20150506
    MSG_SUMA_NO_COINCIDE = 'No coincide la suma de pagos informada en el archivo con la suma real del archivo';								// SS_1259_MBE_20150506
    MSG_DIF             = 'Diferencia: ';
    MSG_ERROR           = 'No se pudo realizar el control del archivo!';
    MSG_HEADER          = 'Anulaciones en Caja y Portal de Servipag';
    MSG_INVALID_HEADER 	= 'En encabezado no tiene la descripci�n: ';
var
    i : Integer;
    Anno, Mes, Dia : Word;													  	  // SS_1259_MBE_20150506
    ItemAProcesar,                                                                // SS_1259_MBE_20150506
    Glosa: String;
    FechaControl : TDateTime;                                                     // SS_1259_MBE_20150506
begin
    Result := False;
    try
    			// INICIO	SS_1259_MBE_20150506
        if FCodigoConcesionariaNativa = CODIGO_VS then begin
        	ItemAProcesar := MSG_FECHA_PROCESO;
        	Anno := StrToInt(Copy(FLista[0],1,4));
            Mes	 := StrToInt(Copy(FLista[0],5,2));
            Dia  := StrToInt(Copy(FLista[0],7,2));

        	// obtener la fecha
            FechaControl :=	EncodeDate(Anno, Mes, Dia);
            // obtener la cantidad de registros
            ItemAProcesar := MSG_CANT_CONTROL;
            FCantidadControl := StrToFloat(Copy(FLista[0],9,7));

            // obtener la suma de saldos
            ItemAProcesar := MSG_SUM_CONTROL;
            FSumaControl := StrToFloat(Copy(FLista[0], 16, 17));

            if FCantidadControl <> FTotalRegistros then begin
                MsgBox(MSG_CANT_NO_COINCIDE + FloatToStr(FCantidadControl) + ' --> ' + IntToStr(FTotalRegistros), Caption, MB_ICONEXCLAMATION);
                Exit;
            end;

            //sumar los registros del archivo
            FSumaReal := 0;
            for i:= 1 to FLista.count - 1 do begin
                FSumaReal := FSumaReal + StrToFloat(Trim(Copy(Flista.Strings[i], 13, 8)));
            end;

            //comparar la suma del archivo contra la suma calculada
            if FSumacontrol <> FSumaReal then begin
                MsgBox(MSG_SUMA_NO_COINCIDE + FloatToStr(FSumaControl) + ' --> ' + FloatToStr(FSumaReal), Caption, MB_ICONEXCLAMATION);
                Exit;
            end;
            
        end   // FIN    SS_1259_MBE_20150506
        else begin                                                               // SS_1259_MBE_20150506

            //Verifico que el HEADER tenga la descripcion apropiada.
            Glosa := Copy(Flista.Strings[0], 15, 69);
            if ( Pos( uppercase(MSG_HEADER), uppercase(Glosa) ) <> 1 ) then begin
                  MsgBox( MSG_INVALID_HEADER + MSG_HEADER, MSG_ERROR, MB_ICONERROR );
                  Exit;
            end;

            //Verifico si existe el footer
            if ( Pos( 'FOOTER', Flista.Strings[FLista.Count - 1] ) <> 1 ) then begin
                  MsgBox( MSG_INVALID_FOOTER, MSG_ERROR, MB_ICONERROR );

                  Exit;
            end;

            //Obtengo la cantidad y la suma del archivo de control
            FCantidadControl := StrToFloat(Trim(Copy(Flista.Strings[FLista.Count - 1], 8, 6)));
        //Rev.1 - Cambi� la especificaci�n, la suma se toma desde el lugar de los egresos.
            //FSumaControl := StrToFloat(Trim(Copy(Flista.Strings[FLista.Count - 1], 30, 43)));
            FSumaControl := StrToFloat(Trim(Copy(Flista.Strings[FLista.Count - 1], 15, 14)));
            //comparo la cantidad contra la calculada
            If FCantidadControl <> FTotalRegistros then begin
                MsgBoxErr(MSG_NOT_EQUAL_COUNT + CRLF + MSG_CANT_CONTROL  + FloatToSTR(FCantidadControl) + CRLF + MSG_CANT_CALC  + FloatToSTR(FTotalRegistros) + CRLF + MSG_DIF + FloatToSTR(FCantidadControl - FTotalRegistros) ,'', self.text, 0);
                exit;
            end;
            //sumo los registros del archivo
            i:=1; //tmp original 2
            FSumaReal:=0;
            while i < FLista.count - 1 do begin
                FSumaReal := FSumaReal + StrToFloat(Trim(Copy(Flista.Strings[i], 73, 10)));
                inc(i);
            end;
            //comparo la suma contra la calculada
            if FSumacontrol <> FSumaReal then begin
                MsgBoxErr('',MSG_NOT_EQUAL_SUM  + CRLF + MSG_SUM_CONTROL  + FloatToSTR(FSumaControl) + CRLF + MSG_SUM_CALC  + FloatToSTR(FSumaReal) + CRLF + MSG_DIF + FloatToSTR(FSumaControl - FSumaReal), self.text, 0);
                exit;
            end;
            
        end;                                                                        // SS_1259_MBE_20150506
        
        Result := True;
    except
        on E : Exception do begin
            MsgBoxErr(MSG_ERROR, E.Message, MSG_ERROR, MB_ICONERROR);
    		end;
    end;
end;

{-----------------------------------------------------------------------
Function Name: InicializarRegistroAnulacion
Author: nefernandez
Date Created: 13/07/2007
Description: inicializo el registro de anulacion
Parameters: Anulacion
Return Value: boolean
-----------------------------------------------------------------------------}
procedure TRecepcionAnulacionesServipagForm.InicializarRegistroAnulacion(var Anulacion: TAnulacion );
begin
    with Anulacion do begin
        CodigoBancoCheque := 0;
        CuentaCheque := '';
        SerieCheque := 0;
        PlazaCheque := 0;
        TipoTarjeta := '';
        MarcaTarjeta := '';
        NumeroTarjeta := '';
        ExpiracionTarjeta := '';
        TipoCuotas := '';
        NumeroCuotas := 0;
        Autorizacion := '';
    end;
end;

{-----------------------------------------------------------------------------
Function Name: AnalizarAnulacionesTxt
Author: nefernandez
Date Created: 27/07/2007
Description: Hace el "parse" de todo el Archivo para verificar
que los campos sean v�lidos
Parameters: None
Return Value: Boolean
-----------------------------------------------------------------------------}
function TRecepcionAnulacionesServipagForm.AnalizarAnulacionesTxt: Boolean;
Resourcestring
    MSG_ERROR_SYNTAXIS = 'El Archivo de Anulaciones contiene errores de Sintaxis';
var
    i: Integer;
    Anulacion : TAnulacion;
    DescError: String;
begin
    FCancelar := False;
    pbProgreso.Position := 0;
    PbProgreso.Max := FLista.Count-1;
    i := 1;
    while i < FLista.Count do begin						// SS_1259_MCA_20150512

        //si cancelan la operacion
        if FCancelar then begin
            Result := False;             //El analisis fue cancelado por el usuario
            pbProgreso.Position := 0;  //Si la importacion fue detenida, Vuelvo a Comenzar
            Exit;                      //salgo de la rutina
        end;

        if not CargarRegistroAnulacion(Flista.Strings[i], Anulacion, DescError) then begin
            FErrores.Add(DescError);
        end;

        pbProgreso.Position:= pbProgreso.Position + 1;  //Muestro el progreso
        Application.ProcessMessages;                    //Refresco la pantalla

        i:= i + 1;
    end;

    Result:= (FErrores.count = 0);

    if (Result = False) then MsgBox(MSG_ERROR_SYNTAXIS, Self.Caption, MB_OK + MB_ICONINFORMATION);

end;

{-----------------------------------------------------------------------------
Function Name: CargarRegistroAnulacion
Author: nefernandez
Date Created: 13/07/2007
Description: Carla el registro de Anulaci�n en base a la linea leida del TXT
Parameters: Linea, Anulacion, DescError
Return Value: boolean

Revisi�n: 1
Author: nefernandez
Date: 26/07/2007
Description: Se verifica que el campo "correlativo" sea mayor a cero
-----------------------------------------------------------------------------}
function TRecepcionAnulacionesServipagForm.CargarRegistroAnulacion(Linea: String; var Anulacion: TAnulacion; var DescError: String): Boolean;
Resourcestring
    MSG_ERROR = 'Error al leer el registro de anulaciones';
const
    STR_VOUCHER                 = 'Comprobante: ';
    STR_DIVIDER                 = ' - ';
  	STR_DATE_ERROR              = 'La fecha es inv�lida.';
    STR_COMPANY_ERROR           = 'La empresa prestadora de servicios es inv�lida.';
    STR_BRANCH_ERROR            = 'La sucursal es inv�lida.';
    STR_CONTROL_NUMBER_ERROR    = 'El correlativo es inv�lido.';
    STR_CONTROL_CORRELATIVO     = 'El c�digo externo de identificaci�n de pagos debe ser mayor a 0 (cero).';
    STR_INVOICE_DATE_ERROR      = 'La fecha contable es inv�lida.';
    STR_OPERATION_TYPE_ERROR    = 'El tipo de operaci�n es inv�lido.';
    STR_ACCOUNT_INDICATOR_ERROR = 'El indicador contable es inv�lido.';
    STR_AMMOUNT_ERROR           = 'El monto es inv�lido.';
    STR_PAYMENT_METHOD_ERROR    = 'El tipo de pago es inv�lido.';
    STR_BANK_CODE_ERROR         = 'El c�digo de banco es inv�lido.';
    STR_CHECK_SERIAL_ERROR      = 'La serie del cheque es inv�lida.';
    STR_ORIGIN_CHECK_ERROR      = 'La Plaza del cheque es inv�lida.';
    STR_NUMBER_OF_QUOTES_ERROR  = 'El Numero de Coutas en inv�lido.';
    STR_INVOICE_NUMBER_ERROR    = 'El n�mero de la nota de cobro es inv�lido.';
var
    Ano, Mes, Dia : String;
    sSucursal : String;
    sCorrelativo : String;
    AnoC, MesC, DiaC : String;
    sMonto : String;
    sCodigoBancoCheque : String;
    sSerieCheque : String;
    sPlazaCheque : String;
    sNumeroCuotas : String;
    sCanal, sCajero, sLote,                                             // SS_1259_MBE_20150506
    sFormaPago, sTipoCliente, TipoComprobante : string;					// SS_1259_MBE_20150506
    NumeroComprobante : String;
begin
    Result := False;
    DescError := '';

    try

        InicializarRegistroAnulacion (Anulacion);

        //	INICIO			SS_1259_MBE_20150506
        if FCodigoConcesionariaNativa = CODIGO_VS then begin
            with Anulacion do begin
                NumeroComprobanteFiscal	:= Copy(Linea, 1, 10);
                NumeroComprobante := NumeroComprobanteFiscal;   // SS_1373_CQU_20150901
                TipoComprobante   := Copy(Linea, 11, 2);
                case StrToInt(TipoComprobante) of
                    33 : TipoComprobanteFiscal := TC_FACTURA_AFECTA;
                    34 : TipoComprobanteFiscal := TC_FACTURA_EXENTA;
                    39 : TipoComprobanteFiscal := TC_BOLETA_AFECTA;
                    41 : TipoComprobanteFiscal := TC_BOLETA_EXENTA;
                end;
                sMonto				:= Copy(Linea, 13, 8);
                Ano					:= Copy(Linea, 21, 4);
                Mes					:= Copy(Linea, 25, 2);
                Dia					:= Copy(Linea, 27, 2);
                sSerieCheque		:= Copy(Linea, 29, 7);
                sCodigoBancoCheque	:= Copy(Linea, 36, 3);
                sPlazaCheque		:= Copy(Linea, 39, 4);
                sFormaPago			:= Copy(Linea, 43, 1);
                sSucursal			:= Copy(Linea, 44, 3);
                sCajero				:= Copy(Linea, 47, 4);
                sLote				:= Copy(Linea, 51, 2);
                sCorrelativo		:= Copy(Linea, 53, 3);
                sCanal				:= Copy(Linea, 56, 2);
                sTipoCliente		:= Copy(Linea, 58, 1);
                //CodigoServicio      := iif(Trim(Copy(Linea, 59, 6)) = '', 'UNCAJA', Trim(Copy(Linea, 59, 6)));    // SS_1285_CQU_20150529
                CodigoServicio      := '';                                                                          // SS_1285_CQU_20150529
                //MedioPago 		    := iif(Trim(Copy(Linea, 63, 6)) = '', 'CASH', Trim(Copy(Linea, 63, 6)));    // SS_1340_CQU_20150806
                MedioPago 		    := iif(Trim(Copy(Linea, 63, 6)) = '', 'CHEQUE', Trim(Copy(Linea, 63, 6)));      // SS_1340_CQU_20150806
                
                try
                    Fecha := EncodeDate(StrToInt(Ano), StrToInt(Mes), StrToInt(Dia));
                except
                    DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER +  STR_DATE_ERROR;
                    Exit;
                end;

                //Verifico que la sucursal sea numerica
                try
                    Sucursal := StrToInt(sSucursal);
                except
                    DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER +  STR_BRANCH_ERROR;
                    Exit;
                end;

                //Si el correlativo viene vacio asigno cero
                if Trim(Scorrelativo) = '' then SCorrelativo := '0';

                //Verifico que el numero correlativo sea numerico
                try
                    Correlativo := StrToInt(sCorrelativo);
                except
                    DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER + STR_CONTROL_NUMBER_ERROR;
                    Exit;
                end;

                //Verifico que el numero "correlativo" (identificaci�n del pago en el sistema externo) sea
                //mayor a cero
                if (Correlativo <= 0) then begin
                    DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER + STR_CONTROL_CORRELATIVO;
                    Exit;
                end;

                //Verifico que el Monto sea numerico
                try
                    Monto := StrToInt(sMonto);
                except
                    DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER + STR_AMMOUNT_ERROR;
                    Exit;
                end;

                //Verifico que el Codigo de Banco del cheque sea numerico
                try
                    CodigoBancoCheque := StrToInt(sCodigoBancoCheque);
                except
                    DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER + STR_BANK_CODE_ERROR;
                    Exit;
                end;

                //Verifico que el Numero de Serie del cheque sea numerico
                try
                    SerieCheque := StrToInt(sSerieCheque);
                except
                    DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER + STR_CHECK_SERIAL_ERROR;
                    Exit;
                end;

                //Verifico que la plaza del cheque sea numerica
                try
                    PlazaCheque := StrToInt(sPlazaCheque);
                except
                    DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER + STR_ORIGIN_CHECK_ERROR;
                    Exit;
                end;

                //Verifico que el numero de cuotas sea numerico
                try
                    NumeroCuotas := StrToIntDef(sNumeroCuotas, 0);
                except
                    DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER + STR_NUMBER_OF_QUOTES_ERROR;
                    Exit;
                end;

                //Verifico que el numero de comprobante sea numerico
                try
                    NotaCobro := StrToInt(NumeroComprobanteFiscal);
                except
                    DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER + STR_INVOICE_NUMBER_ERROR;
                    Exit;
                end;
            end;
            //validar la data


        end   // FIN 		SS_1259_MBE_20150506
        else begin

            with Anulacion do begin
                Ano                := Copy(Linea, 1, 4);
                Mes                := Copy(Linea, 5, 2);
                Dia                := Copy(Linea, 7, 2);
                EPS 	           := Trim(Copy( Linea, 15, 6));
                sSucursal          := Trim(Copy(Linea, 21, 6));
                Terminal           := Copy(Linea, 27, 15);
                sCorrelativo 	   := Trim(Copy( Linea, 42, 10 ));
                AnoC               := Copy(Linea, 52, 4);
                MesC               := Copy(Linea, 56, 2);
                DiaC               := Copy(Linea, 58, 2);
                CodigoServicio	   := Copy(Linea, 60, 6);
                TipoOperacion 	   := Copy(Linea, 66, 6);
                IndicadorContable  := Copy(Linea, 72, 1);
                sMonto 			   := Trim(Copy(Linea, 73, 10));
                MedioPago 		   := Trim(Copy(Linea, 83, 6));
                sCodigoBancoCheque := Trim(Copy(Linea, 89, 3));
                CuentaCheque 	   := Copy(Linea, 92, 11 );
                sSerieCheque 	   := Trim(Copy(Linea, 103, 7));
                sPlazaCheque 	   := Trim(Copy(Linea, 110, 3));
                TipoTarjeta        := Copy(Linea, 113, 6);
                MarcaTarjeta       := Copy(Linea, 119, 6);
                NumeroTarjeta      := Copy(Linea, 125, 19);
                ExpiracionTarjeta  := Copy(Linea, 144, 4);
                TipoCuotas         := Copy(Linea, 148, 6);
                sNumeroCuotas      := Trim( Copy(Linea, 154, 2 ));
                Autorizacion       := Copy(Linea, 156, 8 );
                NumeroComprobante  := Trim( Copy(Linea, 164, 9 ));
                Identificador      := Copy(Linea, 173, 91 );

                //Verifico que sea una fecha valida
                try
                    Fecha := EncodeDate(StrToInt(Ano), StrToInt(Mes), StrToInt(Dia));
                except
                    DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER +  STR_DATE_ERROR;
                    Exit;
                end;

                //Verifico que la empresa prestador de servicios sea costanera norte
                if (EPS <> 'COSNOR') then begin
                    DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER +  STR_COMPANY_ERROR;
                    Exit;
                end;

                //Verifico que la sucursal sea numerica
                try
                    Sucursal := StrToInt(sSucursal);
                except
                    DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER +  STR_BRANCH_ERROR;
                    Exit;
                end;

                //Si el correlativo viene vacio asigno cero
                if Trim(Scorrelativo) = '' then SCorrelativo := '0';

                //Verifico que el numero correlativo sea numerico
                try
                    Correlativo := StrToInt(sCorrelativo);
                except
                    DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER + STR_CONTROL_NUMBER_ERROR;
                    Exit;
                end;

                //Verifico que el numero "correlativo" (identificaci�n del pago en el sistema externo) sea
                //mayor a cero
                if (Correlativo <= 0) then begin
                    DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER + STR_CONTROL_CORRELATIVO;
                    Exit;
                end;

                //Verifico que la fecha contable sea valida
                try
                    FechaContable := EncodeDate(StrToInt(AnoC), StrToInt(MesC), StrToInt(DiaC));
                except
                    DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER + STR_INVOICE_DATE_ERROR;
                    Exit;
                end;

                //Verico que la operacion sea de ingreso
                if (TipoOperacion <> 'INGRES') then begin
                    DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER + STR_OPERATION_TYPE_ERROR;
                    Exit;
                end;

                //Verifico que el indicador contable sea haber
                if (IndicadorContable <> 'H') then begin
                    DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER + STR_ACCOUNT_INDICATOR_ERROR;
                    Exit;
                end;

                //Verifico que el Monto sea numerico
                try
                    Monto := StrToInt(sMonto);
                except
                    DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER + STR_AMMOUNT_ERROR;
                    Exit;
                end;

                //Verifico que el Codigo de Banco del cheque sea numerico
                try
                    CodigoBancoCheque := StrToInt(sCodigoBancoCheque);
                except
                    DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER + STR_BANK_CODE_ERROR;
                    Exit;
                end;

                //Verifico que el Numero de Serie del cheque sea numerico
                try
                    SerieCheque := StrToInt(sSerieCheque);
                except
                    DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER + STR_CHECK_SERIAL_ERROR;
                    Exit;
                end;

                //Verifico que la plaza del cheque sea numerica
                try
                    PlazaCheque := StrToInt(sPlazaCheque);
                except
                    DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER + STR_ORIGIN_CHECK_ERROR;
                    Exit;
                end;

                //Verifico que el numero de cuotas sea numerico
                try
                    NumeroCuotas := StrToIntDef(sNumeroCuotas, 0);
                except
                    DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER + STR_NUMBER_OF_QUOTES_ERROR;
                    Exit;
                end;

                //Verifico que el numero de comprobante sea numerico
                try
                    NotaCobro := StrToInt(NumeroComprobante);
                except
                    DescError := STR_VOUCHER + NumeroComprobante + STR_DIVIDER + STR_INVOICE_NUMBER_ERROR;
                    Exit;
                end;
            end;
        end;

        Result := True;

    except
        on E: Exception do begin
            MsgBoxErr(MSG_ERROR, E.Message, Self.Caption, MB_ICONERROR);
            FErrorGrave:=true;
        end;
    end;
end;

{-----------------------------------------------------------------------------
Function Name: RegistrarOperacion
Author: nefernandez
Date Created: 13/07/2007
Description: Registra la Operaci�n en el Log de Operaciones
Parameters: None
Return Value: boolean
-----------------------------------------------------------------------------}
function TRecepcionAnulacionesServipagForm.RegistrarOperacion: Boolean;
resourcestring
    MSG_COULD_NOT_REGISTER_OPERATION = 'No se pudo registrar la operaci�n';
    MSG_ERROR = 'Error';
Const
    STR_OBSERVACION = 'Procesar Archivo de Anulaciones';
var
    NombreArchivo: String;
  	DescError    : String;
begin
   NombreArchivo := Trim(ExtractFileName(TxtOrigen.text));
   Result := RegistrarOperacionEnLogInterface (DMConnections.BaseCAC , RO_MOD_INTERFAZ_ENTRANTE_ANULACIONES_SERVIPAG , NombreArchivo, UsuarioSistema , STR_OBSERVACION , False , False, NowBase(DMConnections.BaseCAC) , 0 , FCodigoOperacion , DescError);
   if (not Result) then MsgBoxErr(MSG_COULD_NOT_REGISTER_OPERATION, DescError, MSG_ERROR, MB_ICONERROR);
end;

{-----------------------------------------------------------------------------
Function Name: ActualizarAnulacion
Author: nefernandez
Date Created: 13/07/2007
Description: Envia el registro de anulaci�n a la base de datos, para su
procesamiento
Parameters: Anulacion, DescError
Return Value: Boolean
-----------------------------------------------------------------------------}
function TRecepcionAnulacionesServipagForm.ActualizarAnulacion(Anulacion: TAnulacion; var DescError: String): Boolean;
resourcestring
    MSG_ERROR_SAVING_INVOICE = 'Error al Asentar la Anulaci�n Comprobante: %d  ';
    MSG_ERROR = 'Error al actualizar la Rendicion';
const
    STR_COMPROBANTE  = 'Comprobante: ';
    STR_SEPARADOR = ' - ';
var
    Error : String;
begin
    Result := False;
    try
        spProcesarAnulacionesServipag.Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacion;
        spProcesarAnulacionesServipag.Parameters.ParamByName('@Fecha').Value := Anulacion.Fecha;
        spProcesarAnulacionesServipag.Parameters.ParamByName('@EPS').Value := Anulacion.EPS;
        spProcesarAnulacionesServipag.Parameters.ParamByName('@Sucursal').Value := Anulacion.Sucursal;
        spProcesarAnulacionesServipag.Parameters.ParamByName('@Terminal').Value := Anulacion.Terminal;
        spProcesarAnulacionesServipag.Parameters.ParamByName('@Correlativo').Value := Anulacion.Correlativo;
        spProcesarAnulacionesServipag.Parameters.ParamByName('@FechaContable').Value := Anulacion.FechaContable;
        spProcesarAnulacionesServipag.Parameters.ParamByName('@CodigoServicio').Value := Anulacion.CodigoServicio;
        spProcesarAnulacionesServipag.Parameters.ParamByName('@TipoOperacion').Value := Anulacion.TipoOperacion;
        spProcesarAnulacionesServipag.Parameters.ParamByName('@IndicadorContable').Value := Anulacion.IndicadorContable;
        spProcesarAnulacionesServipag.Parameters.ParamByName('@IndicadorContable').Value := Anulacion.IndicadorContable;
        spProcesarAnulacionesServipag.Parameters.ParamByName('@Monto').Value := Anulacion.Monto;
        spProcesarAnulacionesServipag.Parameters.ParamByName('@MedioPago').Value := Anulacion.MedioPago;
        spProcesarAnulacionesServipag.Parameters.ParamByName('@CodigoBancoCheque').Value := Anulacion.CodigoBancoCheque;
        spProcesarAnulacionesServipag.Parameters.ParamByName('@CuentaCheque').Value := Anulacion.CuentaCheque;
        spProcesarAnulacionesServipag.Parameters.ParamByName('@SerieCheque').Value := Anulacion.SerieCheque;
        spProcesarAnulacionesServipag.Parameters.ParamByName('@PlazaCheque').Value := Anulacion.PlazaCheque;
        spProcesarAnulacionesServipag.Parameters.ParamByName('@TipoTarjeta').Value := Anulacion.TipoTarjeta;
        spProcesarAnulacionesServipag.Parameters.ParamByName('@MarcaTarjeta').Value := Anulacion.MarcaTarjeta;
        spProcesarAnulacionesServipag.Parameters.ParamByName('@NumeroTarjeta').Value := Anulacion.NumeroTarjeta;
        spProcesarAnulacionesServipag.Parameters.ParamByName('@ExpiracionTarjeta').Value := Anulacion.ExpiracionTarjeta;
        spProcesarAnulacionesServipag.Parameters.ParamByName('@TipoCuotas').Value := Anulacion.TipoCuotas;
        spProcesarAnulacionesServipag.Parameters.ParamByName('@NumeroCuotas').Value := Anulacion.NumeroCuotas;
        spProcesarAnulacionesServipag.Parameters.ParamByName('@Autorizacion').Value := Anulacion.Autorizacion;
        spProcesarAnulacionesServipag.Parameters.ParamByName('@NotaCobro').Value := Anulacion.NotaCobro;									// SS_1373_CQU_20150901 // SS_1259_MCA_20150512
        //spProcesarAnulacionesServipag.Parameters.ParamByName('@NotaCobro').Value := Anulacion.NumeroComprobanteFiscal;					// SS_1259_MCA_20150512
        //spProcesarAnulacionesServipag.Parameters.ParamByName('@TipoComprobante').Value := Anulacion.TipoComprobanteFiscal;				    //SS_1340_NDR_20150708 SS_1259_MCA_20150512
        spProcesarAnulacionesServipag.Parameters.ParamByName('@TipoComprobanteFiscal').Value := Anulacion.TipoComprobanteFiscal;				//SS_1340_NDR_20150708 SS_1259_MCA_20150512
        spProcesarAnulacionesServipag.Parameters.ParamByName('@Identificador').Value := Anulacion.Identificador;
        spProcesarAnulacionesServipag.Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
        spProcesarAnulacionesServipag.Parameters.ParamByName('@DescripcionError').Value := '';

        spProcesarAnulacionesServipag.CommandTimeout := 500;
        spProcesarAnulacionesServipag.ExecProc;

        Error := Trim(Copy(VarToStr(spProcesarAnulacionesServipag.Parameters.ParamByName('@DescripcionError').Value),1,200));			// SS_1259_MCA_20150512
        if Error <> '' then begin
            DescError := STR_COMPROBANTE + IntToStr(Anulacion.NotaCobro) + STR_SEPARADOR + Error;
            exit;
        end;

        Result := True;
    except
        on  E : Exception do begin
            FErrorGrave := True;
            MsgBoxErr(Format(MSG_ERROR_SAVING_INVOICE, [Anulacion.NotaCobro]), E.Message, Self.caption, MB_ICONERROR);
        end;
    end;
end;

{-----------------------------------------------------------------------------
Function Name: ObtenerCantidadErroresInterfaz
Author: nefernandez
Date Created: 13/07/2007
Description: Obtengo la cantidad de errores que se produjeron
al procesar el archivo
Parameters: CodigoOperacion, Cantidad
Return Value: boolean
-----------------------------------------------------------------------------}
function TRecepcionAnulacionesServipagForm.ObtenerCantidadErroresInterfaz(CodigoOperacion : Integer ; var Cantidad : Integer) : Boolean;
resourcestring
    MSG_ERROR = 'No se pudo obtener la cantidad de errores de la interfaz';
begin
    Cantidad := 0;
    try
        Cantidad := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.ObtenerCantidadErroresInterfaz ('+IntToStr(CodigoOperacion)+')');
        Result := True;
    except
       on E : Exception do begin
           MsgBoxErr(MSG_ERROR, E.Message, Self.caption, MB_ICONERROR);
           Result := False;
       end;
    end;
end;

{-----------------------------------------------------------------------------
Function Name: ConfirmaCantidades
Author: nefernandez
Date Created: 13/07/2207
Description: Pregunta al usuario si quiere continuar o no,
presentandole un resumen de las cantidades a procesar
Parameters:
Return Value: Boolean
-----------------------------------------------------------------------------}
function TRecepcionAnulacionesServipagForm.ConfirmaCantidades: Boolean;
ResourceString
    MSG_TITLE        = 'Recepci�n de Anulaciones';
    MSG_CONFIRMATION = 'El Archivo seleccionado contiene %d anulaciones a procesar.'+crlf+crlf+					// SS_1259_MCA_20150512
                       'Desea continuar con el procesamiento del archivo?';
var
    Mensaje : String;
begin
    Mensaje := Format(MSG_CONFIRMATION,[FTotalRegistros]);
    Result := ( MsgBox(Mensaje,MSG_TITLE, MB_ICONQUESTION + MB_YESNO) = mrYes );
end;

{-----------------------------------------------------------------------------
Function Name: GenerarReporteErroresSintaxis
Author: nefernandez
Date Created: 13/07/2007
Description: Genero el reporte de errores de sintaxis
Parameters: None
Return Value: boolean
-----------------------------------------------------------------------------}
function TRecepcionAnulacionesServipagForm.GenerarReporteErroresSintaxis : Boolean;
resourcestring
    MSG_REPORT_ERROR = 'No se pudo generar el reporte de errores de Sintaxis';
    MSG_ERROR        = 'Error';
const
    REPORT_TITLE     = 'Servipag - Procesar Archivo de Anulaciones';
var
    F : TFRptErroresSintaxis;
begin
    Result := False;
    try
        Application.CreateForm(TFRptErroresSintaxis, F);
        if not F.Inicializar( IntToStr(RO_MOD_INTERFAZ_ENTRANTE_ANULACIONES_SERVIPAG) , REPORT_TITLE , FServipag_Directorio_Errores ,FErrores) then f.Release;
        Result := True;
    except
       on E : Exception do begin
            MsgBoxErr(MSG_REPORT_ERROR, E.Message, MSG_ERROR, MB_ICONERROR);
            Exit;
        end;
    end;
end;

procedure TRecepcionAnulacionesServipagForm.GrabarErrores;                                                  //SS_1340_NDR_20150708
var                                                                                                         //SS_1340_NDR_20150708
    i : Integer;                                                                                            //SS_1340_NDR_20150708
begin                                                                                                       //SS_1340_NDR_20150708
    for i := 0 to FErrores.Count - 1 do begin                                                               //SS_1340_NDR_20150708
        AgregarErrorInterfase( DMConnections.BaseCAC, FCodigoOperacion, FErrores[i], -1);                   //SS_1340_NDR_20150708
    end;                                                                                                    //SS_1340_NDR_20150708
end;                                                                                                        //SS_1340_NDR_20150708

{-----------------------------------------------------------------------------
Function Name: ProcesarAnulaciones
Author: nefernandez
Date Created: 13/07/2007
Description: Proceso el archivo de anulaciones
Parameters:
Return Value: Boolean
-----------------------------------------------------------------------------}
function TRecepcionAnulacionesServipagForm.ProcesarAnulaciones: Boolean;
var
    i: Integer;
    Anulacion: TAnulacion;
    DescError: String;
begin
    Result := False;
    FCancelar := False;
    pbProgreso.Position := 0;
    pbProgreso.Max := FLista.Count-1;
    i := 1;
    while i < FLista.Count do begin								// SS_1259_MCA_20150512

        if FCancelar then begin
             pbProgreso.Position := 0;
             Exit;
        end;

        if not CargarRegistroAnulacion(Flista.Strings[i], Anulacion, DescError) then begin			// SS_1259_MCA_20150512
            FErrores.Add(DescError);																// SS_1259_MCA_20150512
        end;																						// SS_1259_MCA_20150512
        if not ActualizarAnulacion (Anulacion, DescError) then begin								// SS_1259_MCA_20150512
            FErrores.Add(DescError);																// SS_1259_MCA_20150512
        end;																						// SS_1259_MCA_20150512

        pbProgreso.Position:= pbProgreso.Position + 1;
        Application.ProcessMessages;

        i:= i + 1;
    end;

    Result := True;
end;

{-----------------------------------------------------------------------------
Function Name: ActualizarLog
Author: nefernandez
Date Created: 13/07/2007
Description: Actualizo el log al finalizar
Parameters: CantidadErrores
Return Value: boolean
-----------------------------------------------------------------------------}
function TRecepcionAnulacionesServipagForm.ActualizarLog(CantidadErrores: Integer): Boolean;
resourcestring
    MSG_ERROR = 'No se pudo Actualizar el log de operaciones';
const
    STR_ERRORES = 'Finalizo OK!, Errores Validaci�n: ';
var
    DescError : String;
begin
    try
        Result := ActualizarLogOperacionesInterfaseAlFinal
                        (DMConnections.BaseCAC,FCodigoOperacion,STR_ERRORES+ IntToStr(CantidadErrores),DescError);
    except
        on E : Exception do begin
           MsgBoxErr(MSG_ERROR, E.Message, Self.caption, MB_ICONERROR);
           Result := False;
        end;
    end;
end;

{-----------------------------------------------------------------------------
Function Name: ObtenerResumen
Author: nefernandez
Date Created: 13/07/2007
Description:  Obtengo el resumen del proceso realizado
Parameters: CodigoOperacionInterfase, CantRegs, Aprobados, Rechazados
Return Value: boolean
-----------------------------------------------------------------------------}
procedure TRecepcionAnulacionesServipagForm.ObtenerResumen(CodigoOperacionInterfase: Integer; var CantRegs, Aprobados, Rechazados: Integer);
resourcestring
    MSG_ERROR = 'Error';
    MSG_ERROR_QUERY = 'Error Obteniendo Resumen';
begin
    try
        spObtenerResumenAnulacionesServipag.close;
        spObtenerResumenAnulacionesServipag.Parameters.ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacionInterfase;
        spObtenerResumenAnulacionesServipag.CommandTimeout := 500;
        spObtenerResumenAnulacionesServipag.Open;

        CantRegs := spObtenerResumenAnulacionesServipag.Parameters.ParamByName('@CantidadAnulacionesRecibidas').Value;
        Aprobados := spObtenerResumenAnulacionesServipag.Parameters.ParamByName('@CantidadAnulacionesAceptadas').Value;
        Rechazados := spObtenerResumenAnulacionesServipag.Parameters.ParamByName('@CantidadAnulacionesRechazadas').Value;

        spObtenerResumenAnulacionesServipag.Close;
    except
        on E : Exception do begin
            MsgBoxErr(MSG_ERROR_QUERY, E.Message, MSG_ERROR, MB_ICONERROR);
        end;
    end;
end;

{-----------------------------------------------------------------------------
Function Name: GenerarReporteDeFinalizacion
Author: nefernandez
Date Created: 13/07/2007
Description: Genero el Reporte de Finalizacion de Proceso sirve para
para verificar si el proceso se realizo segun parametros normales.
Parameters: CodigoOperacionInterfase
Return Value: boolean
-----------------------------------------------------------------------------}
function TRecepcionAnulacionesServipagForm.GenerarReporteDeFinalizacion(CodigoOperacionInterfase: Integer): Boolean;
resourcestring
    MSG_REPORT_ERROR = 'No se pudo generar el reporte de resumen de anulaciones';
    MSG_ERROR        = 'Error';
const
    STR_TITLE = 'Anulaciones Recibidas';
var
    FRecepcion : TRptRecepcionAnulacionesServipagForm;
begin
    Result := False;
    try
        Application.CreateForm(TRptRecepcionAnulacionesServipagForm, FRecepcion);
        if not fRecepcion.Inicializar( STR_TITLE, CodigoOperacionInterfase) then fRecepcion.Release;
        Result := True;
    except
       on E : Exception do begin
            MsgBoxErr(MSG_REPORT_ERROR, E.Message, MSG_ERROR, MB_ICONERROR);
            Exit;
        end;
    end;
end;

{-----------------------------------------------------------------------------
Function Name: btnProcesarClick
Author: nefernandez
Date Created: 13/07/2007
Description: Procesa el archivo de Anulaciones
Parameters: Sender
Return Value: None
-----------------------------------------------------------------------------}
procedure TRecepcionAnulacionesServipagForm.btnProcesarClick(Sender: TObject);
resourcestring
    MSG_PROCESS_CANCEL = 'Proceso cancelado por el usuario';
    MSG_PROCESS_NOT_COMPLETE = 'El proceso no se pudo completar';
    MSG_PROCESS_FINALLY_WITH_ERROR = 'El proceso finaliz� con Errores';
  	MSG_PROCESS_FINALLY_OK = 'Proceso finalizado con �xito';
const
    STR_OPEN_FILE          = 'Abrir Archivo...';
    STR_CHECK_FILE         = 'Control del Archivo...';
    STR_CONFIRM            = 'Confirmar Cantidades...';
    STR_ANALYZING          = 'Analizando Anulaciones...';
    STR_REGISTER_OPERATION = 'Registrando Operacion en el Log de Operaciones...';
    STR_PROCESS            = 'Registrando las Anulaciones...';
var
    CantidadErrores : Integer;
    CantRegs, Aprobados, Rechazados : integer;
begin
    btnProcesar.Enabled := False;
    pnlAvance.Visible := True;
  	try
   		Lblmensaje.Caption := STR_OPEN_FILE;
   		if not AbrirArchivoTxt then begin
            FErrorGrave := True;
            Exit;
        end;

        Lblmensaje.Caption := STR_CHECK_FILE;
        if not ControlarArchivoTxt then begin
            FErrorGrave := True;
            Exit;
        end;

        Lblmensaje.Caption := STR_CONFIRM;
        if not ConfirmaCantidades then begin
            FErrorGrave := True;
            Exit;
        end;

        //Lblmensaje.Caption := STR_ANALYZING;      // SS_1285_CQU_20150529
        //if not AnalizarAnulacionesTxt then begin  // SS_1285_CQU_20150529
        //    FErrorGrave := True;                  // SS_1285_CQU_20150529
        //    Exit;                                 // SS_1285_CQU_20150529
        //end;                                      // SS_1285_CQU_20150529

        Lblmensaje.Caption := STR_REGISTER_OPERATION;
        if not RegistrarOperacion then begin
            FErrorGrave := True;
            Exit;
        end;

   		Lblmensaje.Caption := STR_PROCESS;
   		if not ProcesarAnulaciones then begin
            FErrorGrave := True;
            Exit;
        end;

        // grabar los errores encontrados                                           //SS_1340_NDR_20150708
        GrabarErrores();                                                            //SS_1340_NDR_20150708

        ObtenerCantidadErroresInterfaz(FCodigoOperacion, CantidadErrores);

        ActualizarLog(CantidadErrores);

    finally

        pbProgreso.Position := 0;
        pnlAvance.visible := False;
   		Lblmensaje.Caption := '';

        if FCancelar then begin
            //Si fue cancelado por el usuario muestro mensaje que el proceso fue cancelado
            MsgBox(MSG_PROCESS_CANCEL, self.Caption, MB_OK + MB_ICONINFORMATION);
        end else if FErrorGrave then begin
            //Si se produjo un error grave muestro un cartel indicando que hubo errores
            MsgBox(MSG_PROCESS_NOT_COMPLETE, self.Caption, MB_OK + MB_ICONINFORMATION);
            if (FErrores.Count > 0) then GenerarReporteErroresSintaxis;
        end
        else begin
            //Obtengo Resumen del Proceso
            ObtenerResumen (FCodigoOperacion, CantRegs, Aprobados, Rechazados);

            if MsgProcesoFinalizado(MSG_PROCESS_FINALLY_OK , MSG_PROCESS_FINALLY_WITH_ERROR , Caption , CantRegs , Aprobados , Rechazados) then begin
                GenerarReportedeFinalizacion(FCodigoOperacion);
            end;
        end;
        KeyPreview := False;
    end;
end;

{-----------------------------------------------------------------------------
Function Name: btnSalirClick
Author: nefernandez
Date Created: 13/07/2007
Description: cierro el formulario
Parameters: Sender
Return Value: None
-----------------------------------------------------------------------------}
procedure TRecepcionAnulacionesServipagForm.btnSalirClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
Function Name: FormClose
Author: nefernandez
Date Created: 13/07/2007
Description: lo libero de memoria
Parameters: Sender, Action
Return Value: None
-----------------------------------------------------------------------------}
procedure TRecepcionAnulacionesServipagForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

{-----------------------------------------------------------------------------
Function Name: FormCloseQuery
Author: nefernandez
Date Created: 13/07/2007
Description: impido salir si esta procesando
Parameters: Sender, CanClose
Return Value: None
-----------------------------------------------------------------------------}
procedure TRecepcionAnulacionesServipagForm.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
    CanClose := not PnlAvance.visible;
end;

{-----------------------------------------------------------------------------
Function Name: FormKeyDown
Author: nefernandez
Date Created: 13/07/2007
Description: permito cancelar la operacion
Parameters: Sender, Key, Shift
Return Value: None
-----------------------------------------------------------------------------}
procedure TRecepcionAnulacionesServipagForm.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    case Key of
        VK_ESCAPE: FCancelar := True;
    else
        FCancelar := False;
    end;
end;

{-----------------------------------------------------------------------------
Function Name: Inicializar
Author: nefernandez
Date Created: 13/07/2007
Description: M�dulo de inicializaci�n de este formulario
Parameters: None
Return Value: Boolean
-----------------------------------------------------------------------------}
function TRecepcionAnulacionesServipagForm.Inicializar(txtCaption: AnsiString): Boolean;
resourcestring
    MSG_INIT_ERROR = 'Error al Inicializar';
    MSG_ERROR = 'Error';
begin
  	Result := False;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

  	CenterForm(Self);
  	try
  		DMConnections.BaseCAC.Connected := True;
  		Result := DMConnections.BaseCAC.Connected and
                           VerificarParametrosGenerales;

  	except
        on E : Exception do begin
            MsgBoxErr(MSG_INIT_ERROR, E.Message, MSG_ERROR, MB_ICONERROR);
        end;
  	end;

    //Inicializar
    Caption := AnsiReplaceStr(txtCaption, '&', '');  //titulo
  	btnProcesar.enabled := False;
  	lblmensaje.Caption := '';
    pnlAvance.visible := False;
    FCodigoOperacion := 0;
    FCodigoServicio := '05';
    FErrores.Clear;																			// SS_1259_MCA_20150512
    FCodigoConcesionariaNativa := ObtenerCodigoConcesionariaNativa;							// SS_1259_MBE_20150506
end;

{-----------------------------------------------------------------------------
Function Name: EsArchivoValido
Author: nefernandez
Date Created: 04/07/2007
Description: Verifica si es un archivo v�lido para la pantalla de Recepci�n de Anulaciones
Parameters:
Return Value: None
-----------------------------------------------------------------------------}
function TRecepcionAnulacionesServipagForm.EsArchivoValido(Nombre: AnsiString): Boolean;
begin
    if FCodigoConcesionariaNativa = CODIGO_VS then begin                                   // SS_1259_MBE_20150506
        Result :=	ExistePalabra(Trim(ExtractFileName(Nombre)), 'REC96410') and		   // SS_1259_MBE_20150506
                	ExistePalabra(UpperCase(Nombre), '.TXT');                              // SS_1259_MBE_20150506
    end                                                                                    // SS_1259_MBE_20150506
    else begin                                                                             // SS_1259_MBE_20150506
        Result:=    iif(FCodigoServicio = '05', True, False) and
                    ExistePalabra(copy(Trim(ExtractFileName(Nombre)), 1, 2), '05') and
                    ExistePalabra(uppercase(Nombre), '.TXT');
    end;
end;                                                                                       // SS_1259_MBE_20150506

{-----------------------------------------------------------------------------
Function Name: btnAbrirArchivoClick
Author: nefernandez
Date Created: 13/07/2007
Description: Busco el archivo a procesar
Parameters: Sender
Return Value: None
-----------------------------------------------------------------------------}
procedure TRecepcionAnulacionesServipagForm.btnAbrirArchivoClick(Sender: TObject);
resourcestring
    MSG_ERROR	= 'No es un archivo de Anulaciones Servipag de ';						    // SS_1259_MBE_20150506
    MSG_CN		= 'Costanera Norte';														// SS_1259_MBE_20150506
    MSG_VS		= 'Vespucio Sur';                                                           // SS_1259_MBE_20150506
    
const
//    STR_UN_CAJA_PORTAL = '[UNCAJA/UNPORT]';												// SS_1259_MBE_20150506
    STR_VALID = ' Valido!';
//var										// SS_1259_MCA_20150512
    //str_servicio: String;					// SS_1259_MCA_20150512
begin
    OpenDialog.InitialDir := FServipag_Directorio_Anulaciones;
    if OpenDialog.execute then begin

        if not (EsArchivoValido(opendialog.filename)) then begin
            if FCodigoConcesionariaNativa = CODIGO_VS then begin                                      // SS_1259_MBE_20150506
            	MsgBox(MSG_ERROR + MSG_VS + STR_VALID , Self.Caption, MB_OK + MB_ICONINFORMATION);    // SS_1259_MBE_20150506
            end                                                                                       // SS_1259_MBE_20150506
            else begin                                                                                // SS_1259_MBE_20150506
            	MsgBox(MSG_ERROR + MSG_CN + STR_VALID , Self.Caption, MB_OK + MB_ICONINFORMATION);    // SS_1259_MBE_20150506
            end;                                                                                      // SS_1259_MBE_20150506

            txtOrigen.text := '';
        end
        else begin
            btnAbrirArchivo.Enabled := False;
            txtOrigen.text := OpenDialog.FileName;
        end;

    end;
end;

{-----------------------------------------------------------------------------
Function Name: txtOrigenChange
Author: nefernandez
Date Created: 13/07/2007
Description: Controlo que abra un archivo valido
Parameters: Sender
Return Value: None
-----------------------------------------------------------------------------}
procedure TRecepcionAnulacionesServipagForm.txtOrigenChange(Sender: TObject);
begin
  	btnProcesar.Enabled := FileExists( txtOrigen.Text);
end;

{-----------------------------------------------------------------------------
Function Name: ImgAyudaClick
Author: nefernandez
Date Created: 13/07/2007
Description: Mensaje de Ayuda
Parameters: Sender
Return Value: None
-----------------------------------------------------------------------------}
procedure TRecepcionAnulacionesServipagForm.imgAyudaClick(Sender: TObject);
var
    MSG_RENDICION : String;
begin
    if PnlAvance.visible = True then Exit;

    MSG_RENDICION      := ' ' + CRLF +
                          'El Archivo de Anulaciones' + CRLF +
                          'es enviado por SERVIPAG al ESTABLECIMIENTO' + CRLF +
                          'y tiene por objeto dar a conocer al Establecimiento, ' + CRLF +
                          'los pagos que fueron anulados en SERVIPAG.' + CRLF +
                          ' ' + CRLF +
                          'Nombre del Archivo: '+ FCodigoServicio +'AAAAMMDD.txt' + CRLF +
                          ' ' + CRLF +
                          'Se utiliza para registrar las anulaciones de los pagos ' + CRLF +
                          'en el sistema del ESTABLECIMIENTO.' + CRLF +
                          ' ';

    MsgBoxBalloon(MSG_RENDICION, Caption, MB_ICONQUESTION, IMGAYUDA);
end;

{-----------------------------------------------------------------------------
Function Name: ImgAyudaMouseMove
Author: nefernandez
Date Created: 13/07/2007
Description: permito hacer click en la ayuda solo si no esta procesando
Parameters: Sender, Shift, X, Y
Return Value: None
-----------------------------------------------------------------------------}
procedure TRecepcionAnulacionesServipagForm.imgAyudaMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
    if PnlAvance.Visible = True then ImgAyuda.Cursor := crDefault else ImgAyuda.Cursor := crHandPoint;
end;

initialization
    FLista   := TStringList.Create;
    FErrores := TStringList.Create;
finalization
    FreeAndNil(Flista);
    FreeAndNil(FErrores);

end.
