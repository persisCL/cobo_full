object FormEjecutarInterfaz: TFormEjecutarInterfaz
  Left = 0
  Top = 0
  Caption = 'Ejecutar Interfaz'
  ClientHeight = 266
  ClientWidth = 620
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  DesignSize = (
    620
    266)
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel: TBevel
    Left = 7
    Top = 7
    Width = 605
    Height = 210
    Anchors = [akLeft, akTop, akRight, akBottom]
    ExplicitWidth = 620
  end
  object lbl1: TLabel
    Left = 24
    Top = 24
    Width = 89
    Height = 13
    Caption = #218'ltimo proceso:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbl2: TLabel
    Left = 48
    Top = 56
    Width = 126
    Height = 13
    Caption = 'Fecha hora comienzo:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbl3: TLabel
    Left = 48
    Top = 75
    Width = 137
    Height = 13
    Caption = 'Fecha hora finalizaci'#243'n:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbl4: TLabel
    Left = 48
    Top = 94
    Width = 115
    Height = 13
    Caption = 'Nombre del archivo:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbl5: TLabel
    Left = 48
    Top = 132
    Width = 107
    Height = 13
    Caption = 'Cantidad registros:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblFechaHoraProceso: TLabel
    Left = 190
    Top = 56
    Width = 111
    Height = 13
    Caption = 'lblFechaHoraRecepcion'
  end
  object lblFechaHoraFinalizacion: TLabel
    Left = 190
    Top = 75
    Width = 116
    Height = 13
    Caption = 'lblFechaHoraFinalizacion'
  end
  object lblNombreArchivo: TLabel
    Left = 190
    Top = 94
    Width = 83
    Height = 13
    Caption = 'lblNombreArchivo'
  end
  object lblLineasArchivo: TLabel
    Left = 190
    Top = 132
    Width = 76
    Height = 13
    Caption = 'lblLineasArchivo'
  end
  object lbl6: TLabel
    Left = 48
    Top = 170
    Width = 69
    Height = 13
    Caption = 'Finaliz'#243' OK:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbl7: TLabel
    Left = 48
    Top = 189
    Width = 89
    Height = 13
    Caption = 'Observaciones:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblFinalizoOK: TLabel
    Left = 190
    Top = 170
    Width = 59
    Height = 13
    Caption = 'lblFinalizoOK'
  end
  object lblObservaciones: TLabel
    Left = 190
    Top = 189
    Width = 81
    Height = 13
    Caption = 'lblObservaciones'
  end
  object lbl8: TLabel
    Left = 48
    Top = 113
    Width = 48
    Height = 13
    Caption = 'Usuario:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblUsuario: TLabel
    Left = 190
    Top = 113
    Width = 46
    Height = 13
    Caption = 'lblUsuario'
  end
  object Label1: TLabel
    Left = 48
    Top = 151
    Width = 69
    Height = 13
    Caption = 'Monto total:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblMontoTotal: TLabel
    Left = 190
    Top = 151
    Width = 64
    Height = 13
    Caption = 'lblMontoTotal'
  end
  object btnProcesar: TButton
    Left = 436
    Top = 234
    Width = 93
    Height = 24
    Anchors = [akRight, akBottom]
    Caption = 'Procesar'
    TabOrder = 0
    OnClick = btnProcesarClick
  end
  object btnSalir: TButton
    Left = 535
    Top = 233
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Salir'
    TabOrder = 1
    OnClick = btnSalirClick
  end
end
