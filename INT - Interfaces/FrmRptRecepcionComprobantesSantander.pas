{-------------------------------------------------------------------------------
 File Name: FrmRptRecepcionComprobantesSantander.pas
 Author:    ggomez
 Date Created: 13/07/2006
 Language: ES-AR
 Description: Gestion de Interfases - Reporte de finalizacion del proceso
              Informa la cantidad de comprobantes recibidos , el importe total y
              otra informacion util para verificar si el proceso se realizo
              seg�n par�metros normales.

 Revision 1
 Author: mbecerra
 Date: 23-Junio-2009
 Description:	Se agregan los errores de interfases.



Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.

Fecha       : 27-04-2015
Firma       : SS_1147_CQU_20150423
Descripcion : Se cambia el titulo de un SubReporte de "Errores" a "Errores de Validacion"
-------------------------------------------------------------------------------}
unit FrmRptRecepcionComprobantesSantander;

interface

uses
  //Reporte
  DMConnection,              //Coneccion a base de datos OP_CAC
  Util,                      //Stringtofile,padl..
  UtilProc,                  //Mensajes
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UtilRB, ppCtrls, ppBands, ppPrnabl, ppClass, ppCache, ppProd,
  ppReport, ppDB, ppComm, ppRelatv, ppTxPipe, DB, ppDBPipe, ADODB,
  ppParameter, ppModule, raCodMod, ppStrtch, ppSubRpt,RBSetup, daDataModule, ConstParametrosGenerales; //SS_1147_NDR_20140710

type
  TFrmRptRecepcionComprobantesSantander = class(TForm)
    ppReporte: TppReport;
    ppParameterList1: TppParameterList;
    RBIListado: TRBInterface;
    spObtenerReporteRecepcionComprobantesSantander: TADOStoredProc;
    dbplspObtenerReporteRecepcionComprobantesSantander: TppDBPipeline;
    dsspObtenerReporteRecepcionComprobantesSantander: TDataSource;
    spObtenerReportePagosRechazadosSantander: TADOStoredProc;
    dsspObtenerReportePagosRechazadosSantander: TDataSource;
    dbplspObtenerReportePagosRechazadosSantander: TppDBPipeline;
    ppHeaderBand1: TppHeaderBand;
    ppImage2: TppImage;
    ppLtitulo: TppLabel;
    ppModulo: TppLabel;
    pptFechaProcesamiento: TppLabel;
    pptNombreArchivo: TppLabel;
    pptUsuario: TppLabel;
    ppFechaProcesamiento: TppLabel;
    ppNombreArchivo: TppLabel;
    ppUsuario: TppLabel;
    ppLine3: TppLine;
    ppLabel5: TppLabel;
    ppNumeroProceso: TppLabel;
    ppLine8: TppLine;
    ppDetailBand1: TppDetailBand;
    ppSubReportDetalleComprobantesRechazados: TppSubReport;
    ChildReporteDetalleRechazos: TppChildReport;
    ppTitleBand3: TppTitleBand;
    ppLine9: TppLine;
    lblDetalleDeRechazos: TppLabel;
    ppLine10: TppLine;
    lblNumeroComprobante: TppLabel;
    lblMotivoRechazo: TppLabel;
    lblMonto: TppLabel;
    ppLine11: TppLine;
    lblConvenio: TppLabel;
    lblCodigoServicio: TppLabel;
    ppDetailBand4: TppDetailBand;
    dtxtNumeroComprobante: TppDBText;
    dtxtMotivoRechazo: TppDBText;
    dtxtMontoAMostrar: TppDBText;
    dtxtNumeroConvenio: TppDBText;
    dtxtCodigoServicio: TppDBText;
    ppSummaryBand1: TppSummaryBand;
    raCodeModule4: TraCodeModule;
    ppSubReportResumenRechazos: TppSubReport;
    ChilReportResumen: TppChildReport;
    ppTitleBand2: TppTitleBand;
    ppLine5: TppLine;
    lblResumen: TppLabel;
    ppLine6: TppLine;
    lblResumenCodigoServicio: TppLabel;
    lblResumenCantidadTotal: TppLabel;
    lblMontoTotal: TppLabel;
    lblResumenCantidadAceptados: TppLabel;
    lblResumenMontoAceptados: TppLabel;
    lblCantidadRecahazados: TppLabel;
    lblResumenMontoRechazados: TppLabel;
    ppDetailBand3: TppDetailBand;
    dtxtResumenCodigoServicio: TppDBText;
    dtxtMontoRechazadosAMostrar: TppDBText;
    dtxtMontoTotalAMostrar: TppDBText;
    dtxtResumenCantidadAceptados: TppDBText;
    dtxtMontoAceptadosAMostrar: TppDBText;
    dtxtResumenCantidadRechazados: TppDBText;
    dtxtResumenCantidadTotal: TppDBText;
    ppSummaryBand3: TppSummaryBand;
    ppLabel9: TppLabel;
    ppLine13: TppLine;
    ppLine14: TppLine;
    dbcTotalCantidadAceptados: TppDBCalc;
    dbcTotalCantidadRechazados: TppDBCalc;
    dbcTotalCantidadTotal: TppDBCalc;
    raCodeModule1: TraCodeModule;
    lblTotalMontoAceptados: TppLabel;
    lblTotalMontoRechazados: TppLabel;
    lblTotalMontoTotal: TppLabel;
    ppCantidadControl: TppLabel;
    ppMontoControl: TppLabel;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppLine1: TppLine;
    ppLabel1: TppLabel;
    ppLineas: TppLabel;
    ppLabel6: TppLabel;
    ppMontoArchivo: TppLabel;
    spObtenerErroresInterfase: TADOStoredProc;
    dsErroresInterfaces: TDataSource;
    ppDBPipeline1: TppDBPipeline;
    ppSubReport1: TppSubReport;
    ppChildReport1: TppChildReport;
    ppTitleBand1: TppTitleBand;
    ppDetailBand2: TppDetailBand;
    ppSummaryBand2: TppSummaryBand;
    ppLabel2: TppLabel;
    ppDBText1: TppDBText;
    procedure RBIListadoExecute(Sender: TObject; var Cancelled: Boolean);
  private
    FNombreReporte: String;
    // Para mantener el c�digo de operaci�n interfase para el cual generar el reporte.
    FCodigoOperacionInterfase: Integer;
    { Private declarations }
  public
    function Inicializar(NombreReporte: AnsiString; CodigoOperacionInterfase: Integer): Boolean;
    { Public declarations }
  end;

var
  FRptRecepcionComprobantesSantander: TFrmRptRecepcionComprobantesSantander;

implementation

{$R *.dfm}

const
 CONST_SIN_DATOS = 'Sin datos.';

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    ggomez
  Date Created: 10/07/2006
  Description: Inicializo este formulario
  Parameters: NombreReporte: AnsiString
    CodigoOperacionInterfase: Integer
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFrmRptRecepcionComprobantesSantander.Inicializar(NombreReporte: AnsiString; CodigoOperacionInterfase: Integer): Boolean;
var                                                                             //SS_1147_NDR_20140710
  RutaLogo: AnsiString;                                                         //SS_1147_NDR_20140710
begin
      ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
      try                                                                                                 //SS_1147_NDR_20140710
        ppImage2.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
      except                                                                                              //SS_1147_NDR_20140710
        On E: Exception do begin                                                                          //SS_1147_NDR_20140710
          Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
          MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
          Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
        end;                                                                                              //SS_1147_NDR_20140710
      end;                                                                                                //SS_1147_NDR_20140710

    FNombreReporte              := NombreReporte;
    FCodigoOperacionInterfase   := CodigoOperacionInterfase;
    // Ejecutar el reporte.
    RBIListado.Execute;
    Result := True;
end;


{-----------------------------------------------------------------------------
  Function Name: RBIListadoExecute
  Author:    ggomez
  Date Created: 10/07/2006
  Description:  Ejecuto el informe
  Parameters: Sender: TObject;var Cancelled: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrmRptRecepcionComprobantesSantander.RBIListadoExecute(Sender: TObject;var Cancelled: Boolean);
resourcestring
    MSG_ERROR_TO_GENERATE_REPORT = 'Error al generar el reporte.';
    MSG_ERROR = 'Error';
var
    Config: TRBConfig;
begin
    // Configuraci�n del reporte
    Config := rbiListado.GetConfig;
    if Config.BinName <> '' then begin
        rbiListado.Report.PrinterSetup.BinName := Config.BinName;
    end;

    try
        // Abrir la consulta de Comprobantes Pagos
        spObtenerReporteRecepcionComprobantesSantander.Close;
        spObtenerReporteRecepcionComprobantesSantander.Parameters.Refresh;
        spObtenerReporteRecepcionComprobantesSantander.Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacionInterfase;
        spObtenerReporteRecepcionComprobantesSantander.Parameters.ParamByName('@FechaProcesamiento').Value := NULL;
        spObtenerReporteRecepcionComprobantesSantander.Parameters.ParamByName('@NombreArchivo').Value := NULL;
        spObtenerReporteRecepcionComprobantesSantander.Parameters.ParamByName('@Modulo').Value := NULL;
        spObtenerReporteRecepcionComprobantesSantander.Parameters.ParamByName('@Usuario').Value := NULL;
        spObtenerReporteRecepcionComprobantesSantander.Parameters.ParamByName('@MontoControl').Value := NULL;
        spObtenerReporteRecepcionComprobantesSantander.Parameters.ParamByName('@CantidadControl').Value := NULL;
        spObtenerReporteRecepcionComprobantesSantander.Parameters.ParamByName('@MontoTotalAceptadosAMostrar').Value := Null;
        spObtenerReporteRecepcionComprobantesSantander.Parameters.ParamByName('@MontoTotalRechazadosAMostrar').Value := Null;
        spObtenerReporteRecepcionComprobantesSantander.Parameters.ParamByName('@MontoTotalAMostrar').Value := Null;
        spObtenerReporteRecepcionComprobantesSantander.Parameters.ParamByName('@LineasArchivo').Value := Null;
        spObtenerReporteRecepcionComprobantesSantander.Parameters.ParamByName('@MontoArchivo').Value := Null;

        spObtenerReporteRecepcionComprobantesSantander.CommandTimeOut := 500;
        spObtenerReporteRecepcionComprobantesSantander.Open;

        // Asigno los valores a los campos del reporte
        ppmodulo.Caption                := spObtenerReporteRecepcionComprobantesSantander.Parameters.ParamByName('@Modulo').Value;
        ppFechaProcesamiento.Caption    := DateToStr(spObtenerReporteRecepcionComprobantesSantander.Parameters.ParamByName('@FechaProcesamiento').Value);
        ppNombreArchivo.Caption         := Copy(spObtenerReporteRecepcionComprobantesSantander.Parameters.ParamByName('@NombreArchivo').Value, 1, 40);
        ppUsuario.Caption               := spObtenerReporteRecepcionComprobantesSantander.Parameters.ParamByName('@Usuario').Value;
        ppNumeroProceso.Caption         := IntToStr(FCodigoOperacionInterfase);
        ppLineas.Caption                := iif(spObtenerReporteRecepcionComprobantesSantander.Parameters.ParamByName('@LineasArchivo').Value = 0, CONST_SIN_DATOS, IntToStr(spObtenerReporteRecepcionComprobantesSantander.Parameters.ParamByName('@LineasArchivo').Value));
        ppMontoArchivo.Caption          := iif(spObtenerReporteRecepcionComprobantesSantander.Parameters.ParamByName('@MontoArchivo').Value = '0', CONST_SIN_DATOS, spObtenerReporteRecepcionComprobantesSantander.Parameters.ParamByName('@MontoArchivo').Value);

        ppCantidadControl.Caption :=       spObtenerReporteRecepcionComprobantesSantander.Parameters.ParamByName('@CantidadControl').Value;
        ppMontoControl.Caption :=       spObtenerReporteRecepcionComprobantesSantander.Parameters.ParamByName('@MontoControl').Value;


        lblTotalMontoAceptados.Caption  := spObtenerReporteRecepcionComprobantesSantander.Parameters.ParamByName('@MontoTotalAceptadosAMostrar').Value;
        lblTotalMontoRechazados.Caption := spObtenerReporteRecepcionComprobantesSantander.Parameters.ParamByName('@MontoTotalRechazadosAMostrar').Value;
        lblTotalMontoTotal.Caption      := spObtenerReporteRecepcionComprobantesSantander.Parameters.ParamByName('@MontoTotalAMostrar').Value;

        // Obtener los comprobantes rechazados.
        spObtenerReportePagosRechazadosSantander.Close;
        spObtenerReportePagosRechazadosSantander.Parameters.Refresh;
        spObtenerReportePagosRechazadosSantander.Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacionInterfase;
        spObtenerReportePagosRechazadosSantander.CommandTimeOut := 500;
        spObtenerReportePagosRechazadosSantander.Open;

        //REV.1
        spObtenerErroresInterfase.Close;
        spObtenerErroresInterfase.Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacionInterfase;
        spObtenerErroresInterfase.Open;

        //Configuro el Report Builder y Ejecuto el Reporte
        rbiListado.Caption := FNombreReporte;

    except
        on E : Exception do begin
            MsgBoxErr(MSG_ERROR_TO_GENERATE_REPORT, E.Message, MSG_ERROR, MB_ICONERROR);
        end;
    end;
end;

end.


