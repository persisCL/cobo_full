{
Revision	: 1
Author		: Nelson Droguett Sierra
Date		: 17-Agosto-2010
Description	: Se debe definir si la plantilla incluye adjuntos o no
            	Se debe definir el content type del mensaje 0=Texto 1=HTML
                Se debe definir la firma que acompa�a al mensaje

Revision	: 2
Author		: Nelson Droguett Sierra
Date		: 16-Septiembre-2010
Description	: Se modifica la interpretacion del radiogroup IncluyeAdjunto y del
            	radioGroup ContentType. Ya que los valores de los controles no
                coincidian con lo almacenado en los campos
Firma		: SS-601-NDR-20100916


Author      : Manuel Cabello Ocaranza
Date        : 31-Enero-2013
Description : Se agrega nuevo campo Email y Clave a la plantilla.
Firma       : SS_1075_MCO_20130131

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

}

unit ABMPlantillasEmail;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb,
  UtilProc, DmiCtrls, PeaProcs, ADODB, DMConnection, Util,
  DPSControls, VariantComboBox, Math,variants,
  SysUtilsCN, EncriptaRijandel;                                                 //SS_1075_MCO_20130131

type
  TFormPlantillasEmail = class(TForm)
    AbmToolbar1: TAbmToolbar;
    DBList1: TAbmList;
    GroupB: TPanel;
    Label15: TLabel;
    Panel2: TPanel;
    Plantillas: TADOTable;
    txt_CodigoFirma: TNumericEdit;
    Notebook: TNotebook;
    txtPlantilla: TMemo;
    Label2: TLabel;
    txtAsunto: TEdit;
    BtnSalir: TButton;
    BtnCancelar: TButton;
    rgIncluyeAdjunto: TRadioGroup;
    rgContentType: TRadioGroup;
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    lbl_Vendedor: TLabel;
    cbFirma: TVariantComboBox;
    txtFirma: TMemo;
    BtnAceptar: TButton;
    lblEmail: TLabel;                                                           //SS_1075_MCO_20130131
    Txt_Email: TEdit;                                                           //SS_1075_MCO_20130131
    txtClaveSender: TEdit;                                                      //SS_1075_MCO_20130131
    lblClaveSender: TLabel;                                                     //SS_1075_MCO_20130131
    lblRepetirClave: TLabel;                                                    //SS_1075_MCO_20130131
    txtConfirmarClave: TEdit;                                                   //SS_1075_MCO_20130131
    procedure BtnCancelarClick(Sender: TObject);
    procedure DBList1Click(Sender: TObject);
    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure DBList1Edit(Sender: TObject);
    procedure DBList1Refresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
    procedure DBList1Delete(Sender: TObject);
    procedure DBList1Insert(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    procedure cbFirmaChange(Sender: TObject);
  private
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    { Private declarations }
    procedure Limpiar_Campos;
    procedure VolverCampos;
    procedure HabilitarCampos;
  public
    { Public declarations }
    function Inicializa: boolean;

  end;

var
  FormFirmasEmail: TFormPlantillasEmail;

implementation

resourcestring
	MSG_DELETE_QUESTION		= '�Est� seguro de querer eliminar esta Plantilla?';
    MSG_DELETE_ERROR		= 'No se puede eliminar esta Plantilla porque hay datos que dependen de ella.';
    MSG_DELETE_CAPTION		= 'Eliminar Plantilla';
    MSG_ACTUALIZAR_ERROR  	= 'No se pudieron actualizar los datos de la Plantilla.';
    MSG_ACTUALIZAR_CAPTION	= 'Actualizar Plantilla';


{$R *.DFM}

procedure TFormPlantillasEmail.VolverCampos;
begin
	DbList1.Estado     			:= Normal;
	DbList1.Enabled    			:= True;
	DbList1.SetFocus;
	Notebook.PageIndex 			:= 0;
    groupb.Enabled     			:= False;
    txt_CodigoFirma.Enabled	    := True;
end;

function TFormPlantillasEmail.Inicializa: boolean;
Var
	S: TSize;
begin
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

	//Rev.1 / 17-Agosto-2010 / Nelson Droguett Sierra --------------------------------
    CargarFirmasEmail(DMConnections.BaseCAC,cbFirma,0,True);

	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
    Notebook.PageIndex := 0;
	if not OpenTables([Plantillas]) then
		Result := False
	else begin
		Result := True;
		DbList1.Reload;
	end;
end;

procedure TFormPlantillasEmail.BtnCancelarClick(Sender: TObject);
begin
   	VolverCampos;
end;


{
	ProcedureName	: DBList1Click
    Author          : Nelson Droguett Sierra
    Date			: 16-Septiembre-2010
    Description		: al hacer click en la grilla, carga los controles con los valores
                	de los campos.
}
procedure TFormPlantillasEmail.DBList1Click(Sender: TObject);
begin
	with Plantillas do begin
		txt_CodigoFirma.Value	:= FieldByName('CodigoPlantilla').AsInteger;
		txtAsunto.text	:= FieldByName('Asunto').AsString;
		txtPlantilla.lines.text	:= FieldByName('Plantilla').AsString;
        // Rev.1 / 17-Agosto-2010 / Nelson Droguett Sierra ----------------------------
        // Rev.2 / 16-Septiembre-2010 / Nelson Droguett Sierra ----------------------------
        rgIncluyeAdjunto.ItemIndex := IfThen(FieldByName('ArchivoAdjunto').AsBoolean,0,1);  //SS-601-NDR-20100916
        rgContentType.ItemIndex := FieldByName('ContentType').AsInteger-1;                  //SS-601-NDR-20100916
        if (Not FieldByName('CodigoFirma').IsNull) then begin
          cbFirma.Value := FieldByName('CodigoFirma').AsInteger;
          txtFirma.lines.text	:= cbFirma.Text;
        end
        else begin
          cbFirma.ItemIndex:=0;
          txtFirma.lines.Clear;
        end;
        if (not FieldByName('Email_Sender').IsNull) then                         // SS_1075_MCO_20130131
            Txt_Email.Text := FieldByName('Email_Sender').AsString               // SS_1075_MCO_20130131
        else                                                                     // SS_1075_MCO_20130131
            Txt_Email.Text := EmptyStr;                                          // SS_1075_MCO_20130131
         if (not FieldByName('Clave_Sender').IsNull) then begin                  // SS_1075_MCO_20130131
            txtClaveSender.Text := FieldByName('Clave_Sender').AsString;         // SS_1075_MCO_20130131
            txtConfirmarClave.Text := txtClaveSender.Text                        //SS_1075_MCO_20130131
         end                                                                     //SS_1075_MCO_20130131
         else begin                                                              //SS_1075_MCO_20130131
            txtClaveSender.Text := EmptyStr;                                     //SS_1075_MCO_20130131
            txtConfirmarClave.Text := EmptyStr;                                  //SS_1075_MCO_20130131
         end;                                                                    //SS_1075_MCO_20130131
        //FinRev.1--------------------------------------------------------------------
	end;
end;

procedure TFormPlantillasEmail.DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
var i: integer;
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
        for i:=0 to sender.SubTitulos.Count - 1 do
    		TextOut(Cols[i], Rect.Top, tabla.Fields[i].AsString);
	end;
end;

procedure TFormPlantillasEmail.DBList1Edit(Sender: TObject);
begin
	HabilitarCampos;
    dblist1.Estado     := modi;
end;

procedure TFormPlantillasEmail.DBList1Insert(Sender: TObject);
begin
	Limpiar_Campos;
    HabilitarCampos;
    dblist1.Estado     := Alta;
end;

procedure TFormPlantillasEmail.DBList1Refresh(Sender: TObject);
begin
	if DBList1.Empty then Limpiar_Campos;
end;

procedure TFormPlantillasEmail.Limpiar_Campos;
begin
	txt_codigoFirma.Clear;
    txtAsunto.Clear;
	txtPlantilla.Lines.Clear;
    Txt_Email.Clear;                                                            //SS_1075_MCO_20130131
    txtClaveSender.Clear;                                                       //SS_1075_MCO_20130131
    txtConfirmarClave.Clear;                                                    //SS_1075_MCO_20130131
end;

procedure TFormPlantillasEmail.AbmToolbar1Close(Sender: TObject);
begin
    close;
end;

procedure TFormPlantillasEmail.DBList1Delete(Sender: TObject);
begin
	Screen.Cursor := crHourGlass;
	If MsgBox(MSG_DELETE_QUESTION, MSG_DELETE_CAPTION, MB_YESNO + MB_ICONQUESTION) =
		IDYES then begin
		try
			Plantillas.Delete;
		Except
			On E: Exception do begin
				Plantillas.Cancel;
				MsgBoxErr(MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
			end;
		end;
		DbList1.Reload;
	end;
	VolverCampos;
	Screen.Cursor      := crDefault;
end;

{
	ProcedureName	: BtnAceptarClick
    Author          : Nelson Droguett Sierra
    Date			: 16-Septiembre-2010
    Description		: Acepta los cambios hechos en los controles y los graba en la tabla.
}
procedure TFormPlantillasEmail.BtnAceptarClick(Sender: TObject);
resourcestring
    MSG_VERIFICAR_ASUNTO   = 'Debe llenar la informaci�n del asunto';
    MSG_VERIFICAR_PLANTILLA   = 'Debe llenar la informaci�n de la plantilla';
    MSG_VERIFICAR_EMAIL     = 'Debe verificar el Email';                        //SS_1075_MCO_20130131
    MSG_VERIFICAR_CLAVE     = 'Debe verificar la Clave';                        //SS_1075_MCO_20130131
    MSG_VERIFICAR_EMAIL_CLAVE     = 'Debe verificar el Email y Clave';          //SS_1075_MCO_20130131
begin
    If (ValidateControls([TxtAsunto], [TRIM(TxtAsunto.Text) <> ''],'Faltan Datos', [MSG_VERIFICAR_ASUNTO]) and
       (ValidateControls([TxtPlantilla], [TRIM(TxtPlantilla.Text) <> ''],'Faltan Datos', [MSG_VERIFICAR_PLANTILLA])) and
       (ValidateControls([Txt_Email],[(IsValidEMailCN(Txt_Email.Text) or (TRIM(txt_EMail.Text) = ''))],'Email Incorrecto', [MSG_VERIFICAR_EMAIL])) and             //SS_1075_MCO_20130131
       (ValidateControls([txtClaveSender],[(TRIM(txtClaveSender.Text) = TRIM(txtConfirmarClave.Text))],'Las claves no coinciden', [MSG_VERIFICAR_CLAVE]))) then    //SS_1075_MCO_20130131
    begin

        if ((Trim(Txt_Email.Text) <> Emptystr) and (Trim(txtClaveSender.Text) = Emptystr)) then begin                                                              //SS_1075_MCO_20130131
            MsgBoxBalloon(MSG_VERIFICAR_CLAVE, 'Faltan Datos', MB_ICONSTOP, txtClaveSender);                                                                       //SS_1075_MCO_20130131
            txtClaveSender.SetFocus;                                                                                                                               //SS_1075_MCO_20130131
            Exit;                                                                                                                                                  //SS_1075_MCO_20130131
        end;                                                                                                                                                       //SS_1075_MCO_20130131
        if ((Trim(Txt_Email.Text) = Emptystr) and (Trim(txtClaveSender.Text) <> Emptystr)) then begin                                                              //SS_1075_MCO_20130131
            MsgBoxBalloon(MSG_VERIFICAR_EMAIL, 'Faltan Datos', MB_ICONSTOP, Txt_Email);                                                                            //SS_1075_MCO_20130131
            Txt_Email.SetFocus;                                                                                                                                    //SS_1075_MCO_20130131
            Exit;                                                                                                                                                  //SS_1075_MCO_20130131
        end;                                                                                                                                                       //SS_1075_MCO_20130131

        Screen.Cursor := crHourGlass;
        With Plantillas do begin
            Try
                if DbList1.Estado = Alta then begin
                    Append;
                end else Edit;
                FieldByName('Asunto').AsString			:= txtAsunto.text;
                FieldByName('Plantilla').AsString		:= txtPlantilla.text;
                //Rev.1 / 17-Agosto-2010 / Nelson Droguett Sierra --------------------
                //Rev.2 / 16-Septiembre-2010 / Nelson Droguett Sierra --------------------
                FieldByName('ArchivoAdjunto').AsBoolean := (rgIncluyeAdjunto.ItemIndex = 0);    //SS-601-NDR-20100916
                FieldByName('ContentType').AsInteger	:= rgContentType.ItemIndex+1;           //SS-601-NDR-20100916
                if cbFirma.Value > 0 then
                   FieldByName('CodigoFirma').AsInteger	:= cbFirma.Value
                else
                   FieldByName('CodigoFirma').Value := Null;
                //FinRev.1------------------------------------------------------------

                if (Txt_Email.Text <> '') then                                    //SS_1075_MCO_20130131
                    FieldByName('Email_Sender').Value :=  txt_Email.Text          //SS_1075_MCO_20130131
                else                                                              //SS_1075_MCO_20130131
                    FieldByName('Email_Sender').Value :=  Null;                   //SS_1075_MCO_20130131

                if (txtClaveSender.Text <> '') then                               //SS_1075_MCO_20130131
                    FieldByName('Clave_Sender').AsString :=  txtClaveSender.Text  //SS_1075_MCO_20130131
                else                                                              //SS_1075_MCO_20130131
                    FieldByName('Clave_Sender').Value :=  Null;                   //SS_1075_MCO_20130131
                Post;
            except
                On E: EDataBaseError do begin
                    Cancel;
                    MsgBoxErr(MSG_ACTUALIZAR_ERROR, E.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
                    Exit;
                end;
            end;
        end;
        VolverCampos;
        Screen.Cursor	:= crDefault;
    end;
end;

procedure TFormPlantillasEmail.FormShow(Sender: TObject);
begin
   	DBList1.Reload;
end;

procedure TFormPlantillasEmail.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFormPlantillasEmail.BtnSalirClick(Sender: TObject);
begin
     close;
end;

procedure TFormPlantillasEmail.cbFirmaChange(Sender: TObject);
begin
   if cbFirma.ItemIndex > 0 then
     	txtFirma.Lines.Text:=cbFirma.Text
   else
   		txtfirma.Lines.Clear;
end;

procedure TFormPlantillasEmail.HabilitarCampos;
begin
	DbList1.Enabled    			:= False;
	Notebook.PageIndex 			:= 1;
    groupb.Enabled     			:= True;
    txt_CodigoFirma.Enabled	    := False;
    txtPlantilla.SetFocus;
end;

end.
