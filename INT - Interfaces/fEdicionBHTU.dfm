object frmEdicionBHTU: TfrmEdicionBHTU
  Left = 345
  Top = 321
  BorderStyle = bsDialog
  Caption = 'Edici'#243'n BHTU'
  ClientHeight = 213
  ClientWidth = 341
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  PixelsPerInch = 96
  TextHeight = 13
  object lblFechaUso: TLabel
    Left = 16
    Top = 26
    Width = 57
    Height = 13
    Caption = 'lblFechaUso'
  end
  object Label1: TLabel
    Left = 16
    Top = 60
    Width = 38
    Height = 13
    Caption = 'Patente'
  end
  object Label2: TLabel
    Left = 16
    Top = 92
    Width = 75
    Height = 13
    Caption = 'Fecha de Venta'
  end
  object Label3: TLabel
    Left = 16
    Top = 124
    Width = 47
    Height = 13
    Caption = 'Categor'#237'a'
  end
  object lblNumeroDayPass: TLabel
    Left = 16
    Top = 8
    Width = 314
    Height = 13
    Caption = 'BHTU N'#186' 99999 (9999999999999999999999999999999)'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Bevel: TBevel
    Left = 8
    Top = 48
    Width = 329
    Height = 105
  end
  object lblDetalle: TLabel
    Left = 8
    Top = 180
    Width = 169
    Height = 29
    AutoSize = False
    Caption = 'lblDetalle'
    Visible = False
    WordWrap = True
  end
  object btnAceptar: TButton
    Left = 184
    Top = 183
    Width = 75
    Height = 25
    Caption = 'Aceptar'
    ModalResult = 1
    TabOrder = 2
    OnClick = btnAceptarClick
  end
  object btnCancelar: TButton
    Left = 259
    Top = 183
    Width = 75
    Height = 25
    Caption = 'Cancelar'
    ModalResult = 2
    TabOrder = 3
  end
  object txtPatente: TEdit
    Left = 201
    Top = 56
    Width = 121
    Height = 21
    MaxLength = 6
    TabOrder = 0
    Text = 'txtPatente'
  end
  object deFechaVenta: TDateEdit
    Left = 201
    Top = 88
    Width = 121
    Height = 21
    AutoSelect = False
    TabOrder = 1
    Date = -693594.000000000000000000
  end
  object pbProgreso: TProgressBar
    Left = 8
    Top = 159
    Width = 329
    Height = 17
    TabOrder = 5
    Visible = False
  end
  object cbCategoria: TVariantComboBox
    Left = 200
    Top = 120
    Width = 121
    Height = 21
    Style = vcsDropDownList
    Enabled = False
    ItemHeight = 13
    TabOrder = 4
    Items = <>
  end
  object spLiberarBHTU: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'LiberarAsignacionBHTU'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroDayPass'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CodigoHistoricoBHTU'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 64
    Top = 176
  end
  object spModificarBHTU: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ModificarBHTU'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroDayPass'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaVenta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@CodigoHistoricoBHTU'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 96
    Top = 176
  end
end
