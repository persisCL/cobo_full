{******************************************************************************
File Name       :   fReporteRecepcionVentasBHTU.pas
Author          :   ndonadio
Date Created    :   05/10/2005
Language        :   ES-AR
Description     :   Muestra el reporte de recepcion de archivo de ventas de BHTU

Revision 1
Author          :   dcepeda
Date            :   25-03-2010
Description     : 	SS-871, Se agregan los campos a desplegar en el reporte:
                    MontoArchivo y DiferenciaMontos.


Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.


*******************************************************************************}
unit fReporteRecepcionVentasBHTU;

interface

uses
    //Reporte de Recepcion de Ventas
    DMConnection,              //Coneccion a base de datos OP_CAC
    Util,                      //Stringtofile,padl..
    UtilProc,                  //Mensajes
    //general
    Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
    Dialogs, DB, ppDB, ppDBPipe, ADODB, UtilRB, ppParameter, ppBands,
    ppCtrls, ppPrnabl, ppClass, ppCache, ppComm, ppRelatv, ppProd, ppReport,
    RBSetup, StdCtrls, ConstParametrosGenerales;                                //SS_1147_NDR_20140710

type
  TfrmReporteRecepcionVentasBHTU = class(TForm)
    RBIListado: TRBInterface;
    DataSource: TDataSource;
    SpObtenerReporteRecepcionDayPass: TADOStoredProc;
    ppDBReporte: TppDBPipeline;
    ppReporte: TppReport;
    ppHeaderBand2: TppHeaderBand;
    ppImage1: TppImage;
    ppLabel6: TppLabel;
    ppModulo: TppLabel;
    ppLabel8: TppLabel;
    ppLabel9: TppLabel;
    ppLabel10: TppLabel;
    ppMaxFechaEmision: TppLabel;
    ppLabel11: TppLabel;
    ppLabel12: TppLabel;
    ppFechaProcesamiento: TppLabel;
    ppNombreArchivo: TppLabel;
    ppUsuario: TppLabel;
    ppCantidadComprobantes: TppLabel;
    ppLine2: TppLine;
    ppLabel16: TppLabel;
    ppLabel17: TppLabel;
    ppLabel18: TppLabel;
    ppLabel19: TppLabel;
    ppCantidad1: TppLabel;
    ppCantidad2: TppLabel;
    ppCantidad3: TppLabel;
    ppCantidad4: TppLabel;
    ppMonto1: TppLabel;
    ppMonto2: TppLabel;
    ppMonto3: TppLabel;
    ppMonto4: TppLabel;
    ppLine4: TppLine;
    ppLine5: TppLine;
    ppTipoComprobante: TppLabel;
    ppLine6: TppLine;
    ppLine7: TppLine;
    ppLabel28: TppLabel;
    ppImporteTotal: TppLabel;
    pptImporteTotal: TppLabel;
    ppLine8: TppLine;
    ppLabel29: TppLabel;
    ppCantidadAceptados: TppLabel;
    ppLabel30: TppLabel;
    ppCantidadRechazados: TppLabel;
    ppLabel31: TppLabel;
    ppCantidadTotal: TppLabel;
    ppDetailBand2: TppDetailBand;
    ppDBText2: TppDBText;
    ppSummaryBand2: TppSummaryBand;
    ppParameterList2: TppParameterList;
    ppLabel1: TppLabel;
    importeunitarioCategoria1: TppLabel;
    importeunitarioCategoria2: TppLabel;
    importeunitarioCategoria3: TppLabel;
    importeunitarioCategoria4: TppLabel;
	//REV.1:
    ppLabel2                            : TppLabel;
    ppMontoArchivo                      : TppLabel;
    ppLabel4                            : TppLabel;
    ppDiferenciaMontos                  : TppLabel;
    //Fin REV.1

    procedure RBIListadoExecute(Sender: TObject; var Cancelled: Boolean);
  private
    { Private declarations }
    FNombreReporte  : AnsiString;
    FCodigoOperacionInterfase: Integer;
  public
    { Public declarations }
    function Inicializar(NombreReporte : AnsiString; CodigoOperacionInterfase : Integer) : Boolean;
  end;

var
  frmReporteRecepcionVentasBHTU : TfrmReporteRecepcionVentasBHTU;

implementation

{$R *.dfm}

{******************************** Function Header ******************************
Function Name   :   Inicializar
Author          :   ndonadio
Date Created    :   05/10/2005
Description     :   Inicializacion de este formulario
*******************************************************************************}
function TfrmReporteRecepcionVentasBHTU.Inicializar(NombreReporte : AnsiString; CodigoOperacionInterfase : Integer) : Boolean;
var                                                                                                     //SS_1147_NDR_20140710
  RutaLogo: AnsiString;                                        //SS_1147_NDR_20140710
begin
  ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
  try                                                                                                 //SS_1147_NDR_20140710
    ppImage1.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
  except                                                                                              //SS_1147_NDR_20140710
    On E: Exception do begin                                                                          //SS_1147_NDR_20140710
      Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
      MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
      Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
    end;                                                                                              //SS_1147_NDR_20140710
  end;                                                                                                //SS_1147_NDR_20140710

  FNombreReporte              := NombreReporte;
  FCodigoOperacionInterfase   := CodigoOperacionInterfase;
  RBIListado.Execute;
  Result                      := True;
end;

{v*****************************************************************************
Function Name   :   RBIListadoExecute
Author          :   dcepeda         (ndonadio)
Date Created    :   08-Abril-2010   (05/10/2005)
Description     :   Ejecuta el reporte, devuelve los campos a desplegar en
                    pantalla.
*******************************************************************************}
procedure TfrmReporteRecepcionVentasBHTU.RBIListadoExecute(Sender : TObject; var Cancelled : Boolean);
resourcestring
    //REV.1: Cambio de mensajes
	MSG_ERROR_AL_GENERAR_REPORTE    = 'Advertencia y/o Error al generar reporte';
    MSG_ERROR                       = 'Advertencia y/o Error';
    //Fin REV.1
var
    Config  : TRBConfig;
begin
    //configuración del reporte
    Config  := rbiListado.GetConfig;
    if Config.BinName <> '' then rbiListado.Report.PrinterSetup.BinName := Config.BinName;
    try
        //Abre la consulta
        with SpObtenerReporteRecepcionDayPass do begin

            Close;
            Parameters.Refresh;
            Parameters.ParamByName('@CodigoOperacionInterfase').Value   := Fcodigooperacioninterfase;
            Parameters.ParamByName('@Modulo').Value                     := NULL;
            Parameters.ParamByName('@FechaProcesamiento').Value         := NULL;
            Parameters.ParamByName('@NombreArchivo').Value              := NULL;
            Parameters.ParamByName('@Usuario').Value                    := NULL;
            Parameters.ParamByName('@CantidadComprobantes').Value       := NULL;
            Parameters.ParamByName('@ImporteTotal').Value               := NULL;
            Parameters.ParamByName('@CantidadCategoria1').Value         := NULL;
	        Parameters.ParamByName('@ImporteCategoria1').Value          := NULL;
	        Parameters.ParamByName('@CantidadCategoria2').Value         := NULL;
	        Parameters.ParamByName('@ImporteCategoria2').Value          := NULL;
	        Parameters.ParamByName('@CantidadCategoria3').VALUE         := NULL;
	        Parameters.ParamByName('@ImporteCategoria3').Value          := NULL;
	        Parameters.ParamByName('@CantidadCategoria4').Value         := NULL;
	        Parameters.ParamByName('@ImporteCategoria4').Value          := NULL;
            Parameters.ParamByName('@TotalRegistros').Value             := NULL;
            Parameters.ParamByName('@TotalProcesados').Value            := NULL;
            Parameters.ParamByName('@importeunitarioCategoria1').Value  := NULL;
            Parameters.ParamByName('@importeunitarioCategoria2').Value  := NULL;
            Parameters.ParamByName('@importeunitarioCategoria3').Value  := NULL;
            Parameters.ParamByName('@importeunitarioCategoria4').Value  := NULL;
        	//REV.1:
            Parameters.ParamByName('@MontoArchivo').Value               := NULL;
            Parameters.ParamByName('@DiferenciaMontos').Value           := NULL;
            //Fin REV.1
            CommandTimeOut                                              := 500;
            Open;
            if SpObtenerReporteRecepcionDayPass.IsEmpty then begin
              	Close;
                Exit;
            end;

            ppmodulo.Caption                    := Parameters.ParamByName('@Modulo').Value;
            ppFechaProcesamiento.Caption        := DateToStr(Parameters.ParamByName('@FechaProcesamiento').Value);
            ppNombreArchivo.Caption             := copy(Parameters.ParamByName('@NombreArchivo').Value,1,40);
            ppUsuario.Caption                   := Parameters.ParamByName('@Usuario').Value;
            //ppCantidadComprobantes.Caption      := Parameters.ParamByName('@CantidadComprobantes').Value; //REV.1
            ppCantidadComprobantes.Caption      := Parameters.ParamByName('@TotalProcesados').Value;    //REV.1
            ppCantidadTotal.Caption             := Parameters.ParamByName('@TotalRegistros').Value;
            //ppCantidadAceptados.Caption         :=Parameters.ParamByName('@TotalProcesados').Value; //REV.1
            ppCantidadAceptados.Caption         := Parameters.ParamByName('@CantidadComprobantes').Value; //REV.1
            //ppCantidadRechazados.Caption        := Parameters.ParamByName('@TotalRegistros').Value - Parameters.ParamByName('@CantidadComprobantes').Value; //REV.1
            ppCantidadRechazados.Caption        := Parameters.ParamByName('@TotalProcesados').Value - Parameters.ParamByName('@CantidadComprobantes').Value; //REV.1
            ppImporteTotal.Caption              := Parameters.ParamByName('@ImporteTotal').Value;
            ppCantidad1.Caption                 := inttostr(Parameters.ParamByName('@CantidadCategoria1').Value);
            ppCantidad2.Caption                 := inttostr(Parameters.ParamByName('@CantidadCategoria2').Value);
            ppCantidad3.Caption                 := inttostr(Parameters.ParamByName('@CantidadCategoria3').Value);
            ppCantidad4.Caption                 := inttostr(Parameters.ParamByName('@CantidadCategoria4').Value);
            ppMonto1.Caption                    := Parameters.ParamByName('@ImporteCategoria1').Value;
            ppMonto2.Caption                    := Parameters.ParamByName('@ImporteCategoria2').Value;
            ppMonto3.Caption                    := Parameters.ParamByName('@ImporteCategoria3').Value;
            ppMonto4.Caption                    := Parameters.ParamByName('@ImporteCategoria4').Value;
            importeunitarioCategoria1.Caption   := Parameters.ParamByName('@importeunitarioCategoria1').Value;
            importeunitarioCategoria2.Caption   := Parameters.ParamByName('@importeunitarioCategoria2').Value;
            importeunitarioCategoria3.Caption   := Parameters.ParamByName('@importeunitarioCategoria3').Value;
            importeunitarioCategoria4.Caption   := Parameters.ParamByName('@importeunitarioCategoria4').Value;
            //REV.1: nuevos conceptos agregados para publicar en reporte.
            ppMontoArchivo.Caption              := Parameters.ParamByName('@MontoArchivo').Value;
            ppDiferenciaMontos.Caption          := Parameters.ParamByName('@DiferenciaMontos').Value;
            //Fin REV.1
        end;

        //Configura el Report Builder y Ejecuto el Reporte
        rbiListado.Caption                      :=  FNombreReporte;

    except
        on E: exception do begin
            MsgBoxErr(MSG_ERROR_AL_GENERAR_REPORTE, E.Message, MSG_ERROR, MB_ICONERROR );
        end;
    end;
end;

end.
