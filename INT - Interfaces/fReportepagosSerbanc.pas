unit fReportePagosSerbanc;

interface

uses
 // Utiles
    PeaProcs, PeaTypes, ComunesInterfaces, Util, UtilProc, StrUtils,
    // Parametros GEnerales
    ConstParametrosGenerales,
   // DB
    UtilDB, DMConnection,
    // Otros
    Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
    Dialogs, StdCtrls, DmiCtrls, ComCtrls, ExtCtrls, DB, ADODB, ppDB, ppDBPipe, ppPrnabl, ppClass,
  ppCtrls, ppBands, ppCache, ppComm, ppRelatv, ppProd, ppReport, UtilRB;


type
  TfrmReportePagosSerbanc = class(TForm)
    spObtenerDatosReportePagosSerbanc: TADOStoredProc;
    dsReportePagos: TDataSource;
    ppDBPDatosReporte: TppDBPipeline;
    ppReportePagos: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppLabel1: TppLabel;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    pplblFechaProceso: TppLabel;
    pplblNroProceso: TppLabel;
    pplblArchivo: TppLabel;
    pplblUsuarioProceso: TppLabel;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLabel8: TppLabel;
    ppLabel9: TppLabel;
    ppLabel10: TppLabel;
    ppLabel11: TppLabel;
    ppLblRegistrosRecibidos: TppLabel;
    pplblRegistrosRechazados: TppLabel;
    ppLblRegistrosValidos: TppLabel;
    pplblRegistrosProcesados: TppLabel;
    pplblConveniosProcesados: TppLabel;
    pplblNotasPagadas: TppLabel;
    pplblMontoTotal: TppLabel;
    ppLabel12: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppDBText1: TppDBText;
    ppFooterBand1: TppFooterBand;
    RBI: TRBInterface;
    procedure RBIExecute(Sender: TObject; var Cancelled: Boolean);
  private
    { Private declarations }
    function PrepararReportePagosSerbanc(var Error: AnsiString): boolean;
  public
    { Public declarations }
    FCodigoOperacionInterfase: Integer;
    FError: AnsiString;
    function MostrarReporte(CodigoOperacionInterfase: Integer; Titulo: AnsiString; var Error: AnsiString): boolean;
  end;

var
  frmReportePagosSerbanc: TfrmReportePagosSerbanc;

implementation

{$R *.dfm}
function TfrmReportePagosSerbanc.MostrarReporte(CodigoOperacionInterfase: Integer; Titulo: AnsiString; var Error: AnsiString): boolean;
resourcestring
    MSG_CANCELED = 'Ejecuci�n del reporte cancelada por el usuario';
begin
    Result := False;
    FCodigoOperacionInterfase := CodigoOperacionInterfase;
    try
        RBI.Caption := 'Reporte de ' + Titulo;
        if not RBI.Execute(True) then begin
            Error := FError;
            Exit;
        end;
        Result := True;
    except
        on e:exception do begin
            Error := e.Message;
            Exit;
        end;
    end;
end;

procedure TfrmReportePagosSerbanc.RBIExecute(Sender: TObject; var Cancelled: Boolean);
resourcestring
    MSG_CANT_GATHER_DATA_FOR_REPORT = 'No se pueden obtener los datos para el reporte';
    MSG_THERE_IS_NOTHING_TO_REPORT  = 'No hay datos para generar el reporte';
var
    DescError: AnsiString;
    FechaProceso: TDateTime;
begin
    if not PrepararReportePagosSerbanc(DescError) then begin
        if trim(DescError) = '' then  Ferror := MSG_THERE_IS_NOTHING_TO_REPORT
        else FError := DescError;
        Cancelled := True;
        Exit;
    end;
    CurrencyString := '$';
    CurrencyFormat := 2;
    ThousandSeparator := '.';

    with spObtenerDatosReportePagosSerbanc do begin
        ppLblRegistrosRecibidos.caption :=  Parameters.ParamByName('@RegistrosRecibidos').Value;
        pplblRegistrosRechazados.caption := Parameters.ParamByName('@RegistrosRechazados').Value;
        ppLblRegistrosValidos.caption := Parameters.ParamByName('@RegistrosValidos').Value;
        pplblRegistrosProcesados.caption := Parameters.ParamByName('@RegistrosProcesados').Value;
        pplblConveniosProcesados.caption := Parameters.ParamByName('@ConveniosProcesados').Value;
        pplblNotasPagadas.caption := Parameters.ParamByName('@NotasdeCobroPagadas').Value;
        pplblMontoTotal.caption := Parameters.ParamByName('@MontoTotal').Value;
        FechaProceso := Parameters.ParamByName('@FechaHoraProceso').Value;
        pplblUsuarioProceso.Caption := Parameters.ParamByName('@Usuario').Value;
        pplblArchivo.Caption := Parameters.ParamByName('@ArchivoRecibido').Value;
    end;
    pplblNroProceso.Caption := Format( 'Proceso N� %d', [FCodigoOperacionInterfase]);
    pplblFechaProceso.Caption := Format( 'Fecha de Proceso: %s; Hora: %s',[FormatDateTime('dd-mm-yyyy', FechaProceso),FormatDateTime('HH:nn', FechaProceso)]);
end;


{******************************** Function Header ******************************
Function Name: PrepararReportePagosSerbanc
Author : ndonadio
Date Created : 24/08/2005
Description :   prepara los datos del reporte
Parameters : var Error: AnsiString
Return Value : boolean
*******************************************************************************}
function TfrmReportePagosSerbanc.PrepararReportePagosSerbanc(
  var Error: AnsiString): boolean;
begin
    Result := False;
    try
        spObtenerDatosReportePagosSerbanc.Parameters.ParamByName('@CodigoOperacion').Value := FCodigoOperacionInterfase;
        spObtenerDatosReportePagosSerbanc.Open;
        Result := True;
    except
        on e:exception do begin
            error := e.Message;
        end;
    end;
end;

end.

