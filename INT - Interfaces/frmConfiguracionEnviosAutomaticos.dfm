object formConfiguracionEnviosAutomaticos: TformConfiguracionEnviosAutomaticos
  Left = 374
  Top = 132
  BorderStyle = bsDialog
  Caption = 'Configuraci'#243'n de env'#237'os autom'#225'ticos'
  ClientHeight = 375
  ClientWidth = 390
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 6
    Top = 24
    Width = 371
    Height = 81
  end
  object Bevel2: TBevel
    Left = 6
    Top = 114
    Width = 371
    Height = 87
  end
  object Label2: TLabel
    Left = 16
    Top = 30
    Width = 197
    Height = 13
    Caption = 'Emails autom'#225'ticos de facturaci'#243'n:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 24
    Top = 52
    Width = 91
    Height = 13
    Caption = 'C'#243'digo de Plantilla:'
  end
  object Label4: TLabel
    Left = 24
    Top = 75
    Width = 81
    Height = 13
    Caption = 'C'#243'digo de Firma:'
  end
  object Label5: TLabel
    Left = 16
    Top = 122
    Width = 243
    Height = 13
    Caption = 'Emails autom'#225'ticos de detalle de tr'#225'nsitos:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label6: TLabel
    Left = 24
    Top = 150
    Width = 91
    Height = 13
    Caption = 'C'#243'digo de Plantilla:'
  end
  object Label7: TLabel
    Left = 24
    Top = 177
    Width = 81
    Height = 13
    Caption = 'C'#243'digo de Firma:'
  end
  object Label1: TLabel
    Left = 8
    Top = 6
    Width = 251
    Height = 13
    Caption = 'Configuraci'#243'n de env'#237'o de e-Mail autom'#225'tico'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Bevel3: TBevel
    Left = 6
    Top = 210
    Width = 371
    Height = 87
  end
  object Label8: TLabel
    Left = 16
    Top = 218
    Width = 189
    Height = 13
    Caption = 'Env'#237'o de comprobantes a clientes'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label9: TLabel
    Left = 24
    Top = 246
    Width = 91
    Height = 13
    Caption = 'C'#243'digo de Plantilla:'
  end
  object Label10: TLabel
    Left = 24
    Top = 273
    Width = 81
    Height = 13
    Caption = 'C'#243'digo de Firma:'
  end
  object txtPlantillaNK: TNumericEdit
    Left = 128
    Top = 48
    Width = 57
    Height = 21
    TabOrder = 0
  end
  object txtFirmaNK: TNumericEdit
    Left = 128
    Top = 72
    Width = 57
    Height = 21
    TabOrder = 1
  end
  object txtPlantillaDetalle: TNumericEdit
    Left = 128
    Top = 147
    Width = 57
    Height = 21
    TabOrder = 2
  end
  object txtFirmaDetalle: TNumericEdit
    Left = 128
    Top = 171
    Width = 57
    Height = 21
    TabOrder = 3
  end
  object btnGrabar: TButton
    Left = 224
    Top = 344
    Width = 75
    Height = 25
    Caption = '&Aceptar'
    Default = True
    TabOrder = 4
    OnClick = btnGrabarClick
  end
  object btnCancelar: TButton
    Left = 304
    Top = 344
    Width = 75
    Height = 25
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 5
    OnClick = btnCancelarClick
  end
  object txtPlantillaCliente: TNumericEdit
    Left = 128
    Top = 243
    Width = 57
    Height = 21
    TabOrder = 6
  end
  object txtFirmaCliente: TNumericEdit
    Left = 128
    Top = 267
    Width = 57
    Height = 21
    TabOrder = 7
  end
  object spObtenerPlantillasActivas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EMAIL_ObtenerPlantillasActivas;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 280
    Top = 136
  end
  object spGrabarPlantillasActivas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EMAIL_GrabarPlantillasActivas;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPlantillaFactura'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPlantillaDetalleFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPlantillaResumenTrimestral'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoFirmaFactura'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoFirmaDetalleFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoFirmaResumenTrimestral'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 240
    Top = 136
  end
end
