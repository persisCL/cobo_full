{-------------------------------------------------------------------------------
 File Name: FrmGenerarArchivoNovedades.pas
 Author:    lgisuk
 Date Created: 14/06/2005
 Language: ES-AR
 Description: M�dulo de la interface Falabella - Generar Archivo de Novedades

 Revision : 1
    Date: 03/03/2009
    Author: mpiazza
    Description:    Ref (SRX01226) se cambia" now " por nowbase

 Revision : 2
    Date: 06/07/2009
    Author: pdominguez
    Description: SS 812
        - Se reescribe por completo el proceso de generaci�n del Archivo de
        Novedades.
        - Se implementa el reenvio de mandatos no confirmados.
        - Se crea el objeto spActualizarConvenioMedioPagoAutomaticoPAT: TStoredProc
        - Se Crea el procedimiento GenerarArchivoNovedades.

 Revision : 3
    Author : pdominguez
    Date   : 24/07/2009
    Description : SS 812
        - Se modifican los siguientes procedimientos / Funciones:
            GenerarReporteFinalizacionProceso
            GenerarArchivoNovedades

 Revision 4
 Author: mbecerra
 Date: 09-Octubre-2009
 Description:	Se cambia el tipo de dato SumatoriaCuentas para corregir un bug
                "Aritmethic overflow" en la funci�n GenerarArchivoNovedades

 Autor       :   Claudio Quezada
 Fecha       :   31-03-2015
 Firma       :   SS_1147_CQU_20150324
 Descripcion :   Seg�n la Concesionaria Nativa, se leen los archivos con uno u otro formato

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

Firma       : SS_1147_MCA_20150417
Descripcion :  se cambia formato fecha para VS

Firma       :   SS_1147_CQU_20150423
Descripcion :   Se corrige el filler en el Header de Vespucio, no lleva puntos sino espacios

Firma       :   SS_1147_CQU_20150428
Descripcion :   Se corrige el RUT, ahora se alinea a la izquierda y se rellena con espacios al final (largo 11)

Firma       :   SS_1314_CQU_20150908
Descripcion :   FBE pide cambiar la fecha del archivo por la del dia en que se genera y no por la fecha elegida
-------------------------------------------------------------------------------}
unit FrmGenerarArchivoNovedades;

interface

uses
  //Envio de Novedades
  DMConnection,              //Coneccion a base de datos OP_CAC
  UtilProc,                  //Mensajes
  Util,                      //Stringtofile,padl..
  UtilDB,                    //Rutinas para base de datos
  ComunesInterfaces,         //Procedimientos y Funciones Comunes a todos los formularios
  ConstParametrosGenerales,  //Obtengo Valores de la Tabla Parametros Generales
  Peatypes,                  //Constantes
  PeaProcs,                  //NowBase
  FrmRptEnvioNovedades,      //Reporte del Proceso
  //General
  DB, ADODB, ComCtrls, StdCtrls, Controls,ExtCtrls, Classes, Windows, Messages, SysUtils,
  Variants,  Graphics,  Forms, Dialogs, Grids, DBGrids,  DPSControls, ListBoxEx,
  DBListEx, StrUtils, UtilRB, ppProd, ppClass, ppReport, ppComm, ppRelatv,
  ppDB, ppTxPipe, ppPrnabl, ppStrtch, ppSubRpt, ppCache, ppBands, ppCtrls,
  Validate, DateEdit;

type
  TFGenerarArchivoNovedades = class(TForm)
    pnlAvance: TPanel;
    pbProgreso: TProgressBar;
    Bevel1: TBevel;
    btnProcesar: TButton;
    SalirBTN: TButton;
    Lprocesogeneral: TLabel;
    lblMensaje: TLabel;
    Ltitulo: TMemo;
    pnlAyuda: TPanel;
    ImgAyuda: TImage;
    pnlReprocesar: TPanel;
    lblNombreArchivo: TLabel;
    lblArchivoInterface: TLabel;
    lblFechaInterfase: TLabel;
    edFecha: TDateEdit;
    lbl_observacion: TLabel;
    SPObtenerMandatosNovedadesCMR: TADOStoredProc;
    spActualizarConvenioMedioPagoAutomaticoPAT: TADOStoredProc;
    spActualizarNovedadesEnviadasCMR: TADOStoredProc;
    procedure ImgAyudaClick(Sender: TObject);
    procedure ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
    procedure btnProcesarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SalirBTNClick(Sender: TObject);
    procedure edFechaChange(Sender: TObject);
  private
    { Private declarations }
    FCodigoOperacion : Integer;
    FCancelar : Boolean;
    FNombreArchivoNovedades : AnsiString;
    FNumeroSecuencia : Integer;
    FFechaDesde : String;
    FFechaSugerida : TDateTime;
    FCMR_CodigodeAutopista : AnsiString;
    FCMR_Directorio_Destino_Novedades : AnsiString;
    FCodigoNativa : Integer;    // SS_1147_CQU_20150324
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    Function CrearNombreArchivoNovedades(var NombreArchivo: AnsiString; Fecha: TDateTime): Boolean;
    Function GenerarReporteFinalizacionProceso(CodigoOperacionInterfase:Integer): Boolean;
    procedure GenerarArchivoNovedades;
  public
    Function Inicializar : Boolean;
    { Public declarations }
  end;

var
  FGenerarArchivoNovedades: TFGenerarArchivoNovedades;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    Lgisuk
  Date Created: 17/06/2005
  Description: M�dulo de inicializaci�n de este formulario
  Parameters: None
  Return Value: Boolean

  Revision : 1
    Date: 03/03/2009
    Author: mpiazza
    Description:    Ref (SRX01226) se cambia" now " por nowbase

  Revision : 2
    Date: 18/07/2009
    Author: pdominguez
    Description: SS 812
            - Se pone el foco en el input de la fecha al mostrar el formulario.
-----------------------------------------------------------------------------}
function TFGenerarArchivoNovedades.Inicializar : Boolean;

    {******************************** Function Header ******************************
    Function Name: VerificarParametrosGenerales
    Author : lgisuk
    Date Created : 19/07/2005
    Description : Obtengo los Parametros Generales que se utilizaran en el formulario
                  al inicializar y verifico que los valores obtenidos sean validos.
    Parameters : None
    Return Value : boolean
    *******************************************************************************}
    Function VerificarParametrosGenerales : Boolean;
    resourcestring
        MSG_ERROR_CHECK_GENERAL_PARAMETER = 'Error al verificar parametro general';
        MSG_ERROR                         = 'Error';
        STR_NOT_EXISTS_GENERAL_PARAMETER  = 'No existe parametro general: ';
        STR_EMPTY_GENERAL_PARAMETER       = 'Parametro general vacio: ';                                         //Se verifica que los codigos no esten vacios, si no tienen razon de ser
        STR_DIRECTORY_NOT_EXISTS          = 'No existe el directorio indicado en parametros generales: ' + CRLF; //Los directorios de destino y errores en procesos de envio son criticos
    const
        CMR_CODIGODEAUTOPISTA            = 'CMR_CodigodeAutopista';
        CMR_DIRECTORIO_DESTINO_NOVEDADES = 'CMR_Directorio_Destino_Novedades';
    var
        DescError : AnsiString;
    begin
        Result    := True;
        DescError := '';
        try
            try
                // Obtengo la concesionaria nativa                  // SS_1147_CQU_20150324
                FCodigoNativa := ObtenerCodigoConcesionariaNativa;  // SS_1147_CQU_20150324

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, CMR_CODIGODEAUTOPISTA , FCMR_CodigodeAutopista) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + CMR_CODIGODEAUTOPISTA;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                if not (FCMR_CodigodeAutopista <> '') then begin
                   DescError := STR_EMPTY_GENERAL_PARAMETER + CMR_CODIGODEAUTOPISTA;
                   Result := False;
                   Exit;
                end;

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, CMR_DIRECTORIO_DESTINO_NOVEDADES , FCMR_Directorio_Destino_Novedades) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + CMR_DIRECTORIO_DESTINO_NOVEDADES;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                FCMR_Directorio_Destino_Novedades := GoodDir(FCMR_Directorio_Destino_Novedades);
                if  not DirectoryExists(FCMR_Directorio_Destino_Novedades) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FCMR_Directorio_Destino_Novedades;
                    Result := False;
                    Exit;
                end;

            except
                on E : Exception do begin
                    Result := False;
                    MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, E.Message, MSG_ERROR, MB_ICONWARNING);
                end;
            end;

        finally
            //si no paso la verificacion de parametros generales
            if (Result = False) then begin
                //informo la situaci�n
                MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, DescError, MSG_ERROR, MB_ICONWARNING);
            end;
        end;
    end;

    {-----------------------------------------------------------------------------
      Function Name: ObtenerUltimoEnvioInterfaz
      Author:    lgisuk
      Date Created: 19/07/2005
      Description: Obtengo la fecha en la que se proceso la ultima interfaz
                   para proponersela al operador
      Parameters: None
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function ObtenerUltimoEnvioInterfaz : Boolean;
    Resourcestring
         MSG_ERROR_FECHA = 'No se pudo obtener la fecha del ultimo envio interfaz!';
    var
        SQLFechaDesde : String;
    begin
        //Obtengo la fecha en la que se ejecuto la ultima interfaz
        try
            SQLFechaDesde := Format('SELECT dbo.DarFecha(dbo.UltimoEnvioInterfaz (%d))', [RO_MOD_INTERFAZ_SALIENTE_NOVEDADES_CMR]);
            EdFecha.Date := QueryGetValueDateTime(DMConnections.BaseCAC, SQLFechaDesde);
            FFechaDesde := QueryGetValue(DMConnections.BaseCAC, SQLFechaDesde);
            //Si no encuentra ninguna fecha que busque en todo el historico
//            if FFechaDesde = '' then FFechaDesde:='1900-01-01';
            Result := True;
        except
            on E : Exception do begin
               Result := False;
               MsgBoxErr(MSG_ERROR_FECHA, E.Message, Self.caption, MB_ICONERROR);
            end;
        end;
    end;

resourcestring
	  MSG_INIT_ERROR = 'Error al Inicializar';
	  MSG_ERROR = 'Error';
Const
    STR_DATE='Existen novedades a procesar desde: ';
begin
	Result := False;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;

  //Centro el formulario
	CenterForm(Self);
	try
        DMConnections.BaseCAC.Connected := True;
        Result := DMConnections.BaseCAC.Connected and
                        VerificarParametrosGenerales and
                                CrearNombreArchivoNovedades(FNombreArchivoNovedades, NowBase(DMConnections.BaseCAC));
    except
        on E : Exception do begin
            MsgBoxErr(MSG_INIT_ERROR, E.Message, MSG_ERROR, MB_ICONERROR);
            Exit;
        end;
    end;
    // Todo Ok, terminamos de inicializar
    lTitulo.text := FNombreArchivoNovedades;      //indico que archivo voy a generar
    lblMensaje.Caption := '';                     //no muestro ningun mensaje
    PnlAvance.visible := False;                   //Oculto el Panel de Avance
    FCancelar := False;                           //inicio cancelar en false
    FCodigoOperacion := 0;                        //Inicializo el codigo de operacion
    //hago visible el panel de busqueda de fecha
    pnlreprocesar.Visible := True;
    //inicializo fecha en null
    edFecha.Date := NULLDATE;
    //deshabilito boton procesar hasta que seleccionen una fecha
    btnProcesar.Enabled := False;
    //obtengo la fecha de la ultima vez que se envio un archivo
    ObtenerUltimoEnvioInterfaz;
    //Obtengo la observaci�n
    FFechaSugerida := QueryGetValueDateTime(DMConnections.BaseCAC ,'select dbo.ObtenerFechaInterfazFalabella()');
    lbl_observacion.Caption:=STR_DATE + DateToStr(FFechaSugerida);

    edFecha.SetFocus; // Rev. 2 (SS 812)
end;

{-----------------------------------------------------------------------------
  Function Name: ImgAyudaClick
  Author:    lgisuk
  Date Created: 03/06/2005
  Description: Mensaje de Ayuda
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoNovedades.ImgAyudaClick(Sender: TObject);
Resourcestring
    MSG_NOVEDADES       = ' ' + CRLF +
                          'El Archivo de Novedades es' + CRLF +
                          'utilizado por el ESTABLECIMIENTO para informar a FALABELLA' + CRLF +
                          'las altas, bajas y modificaciones de mandantes' + CRLF +
                          'efectuadas por el ESTABLECIMIENTO' + CRLF +
                          ' ' + CRLF +
                          'Nombre del Archivo: 04_CCNOVEDADES_DDMMAA.xx' + CRLF +
                          ' ';
begin
    //si esta procesando sale
    if PnlAvance.Visible = True then Exit;
    //Muestro el mensaje
    MsgBoxBalloon(MSG_NOVEDADES, Caption, MB_ICONQUESTION, IMGAYUDA);
end;

{-----------------------------------------------------------------------------
  Function Name: ImgAyudaMouseMove
  Author:    lgisuk
  Date Created: 03/06/2005
  Description: permito hacer click en la ayuda solo si no esta procesando
  Parameters: Sender: TObject;Shift: TShiftState; X, Y: Integer
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoNovedades.ImgAyudaMouseMove(Sender: TObject;Shift: TShiftState; X, Y: Integer);
begin
    if PnlAvance.Visible = True then ImgAyuda.Cursor := crDefault else ImgAyuda.Cursor := crHandPoint;
end;

{-----------------------------------------------------------------------------
  Function Name: CrearNombreArchivoNovedades
  Author:    lgisuk
  Date Created: 22/06/2005
  Description: Crea el nombre del archivo de novedades
  Parameters: var NombreArchivo: AnsiString
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFGenerarArchivoNovedades.CrearNombreArchivoNovedades(var NombreArchivo: AnsiString; Fecha: TDateTime): Boolean;
resourcestring
    MSG_FILE_NAME_ERROR = 'Error al crear el nombre del archivo';
    MSG_DIRECTORY_ERROR = 'El directorio de salida %s no existe.' + crlf +
                          'Verifique los par�metros generales.';
const
    FILE_NAME = 'CCNOVEDADES';
var
    Year, Month, Day : Word;
    Mes,Dia : AnsiString;
    NumeroSecuencia : AnsiString;
begin
    try
        Fecha := NowBase(DMConnections.BaseCAC); // SS_1314_CQU_20150908
        //Obtengo a�o, mes y dia actual
        DecodeDate(Fecha, Year, Month, Day);
        if Length(IntToStr(Month)) = 1 then Mes := '0'+ IntToStr(Month) else Mes := IntToStr(Month);
        if Length(IntToStr(Day)) = 1 then Dia := '0'+ IntToStr(Day) else Dia := IntToStr(Day);
        //Asigo Numero de Secuencia es fijo
        NumeroSecuencia := '01';
        //Genero el Nombre del Archivo de Novedades
        FCMR_CodigodeAutopista := IStr0(StrtoInt(FCMR_CodigodeAutopista),2);
        NombreArchivo := FCMR_CodigodeAutopista + '_' + FILE_NAME + '_';
        NombreArchivo := NombreArchivo + Dia + Mes + Copy(IntToStr(Year),3,2);
        NombreArchivo := FCMR_Directorio_Destino_Novedades + Trim(NombreArchivo) + '.' + NumeroSecuencia;
        //guardo el numero de secuencia
        FNumeroSecuencia := StrToInt(NumeroSecuencia);
        Result := True;
    except
        raise Exception.Create(MSG_FILE_NAME_ERROR);
    end;
end;

{******************************** Function Header ******************************
Function Name: edFechaChange
Author : lgisuk
Date Created : 11/07/2005
Description : Permito Re-Procesar el envio de mandatos
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFGenerarArchivoNovedades.edFechaChange(Sender: TObject);
Resourcestring
    STR_FILE_NAME = 'nn_CCNOVEDADES_DDMMAA.xx';
begin
	if ( edFecha.Date <> nulldate ) then begin
   		btnProcesar.Enabled := True;
   		FFechaDesde :=  DateTimeToStr(edFecha.Date);

        //Creo el nombre del archivo a enviar
        CrearNombreArchivoNovedades(FNombreArchivoNovedades, edFecha.Date);
        lblNombreArchivo.Caption := ExtractFileName(FNombreArchivoNovedades);

    end else begin
   		btnProcesar.Enabled := False;
   		lblNombreArchivo.Caption := STR_FILE_NAME;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: GenerarInformedelProceso
  Author:    lgisuk
  Date Created: 23/06/2005
  Description: Muestro un informe de finalizaci�n de proceso
  Parameters: CodigoOperacionInterfase:Integer; NombreReporte, CantidadRegistros, SumatoriaCuentas: AnsiString
  Return Value: None

  Revision : 1
    Author : pdominguez
    Date   : 24/07/2009
    Description : SS 812
        - Se eliminan los par�metros CantidadRegistros y SumatoriaCuentas.
-----------------------------------------------------------------------------}
Function TFGenerarArchivoNovedades.GenerarReporteFinalizacionProceso(CodigoOperacionInterfase:Integer):boolean;
resourcestring
    MSG_REPORT_ERROR = 'No se pudo generar el reporte de finalizaci�n de proceso';
    MSG_ERROR        = 'Error';
const
    REPORT_TITLE     = 'Envio Novedades';
var
    F : TFRptEnvioNovedades;
begin
    Result := False;
    try
        //Muestro el reporte
        Application.CreateForm(TFRptEnvioNovedades, F);
        if not F.Inicializar(CodigoOperacionInterfase, REPORT_TITLE) then f.Release;
        Result := True;
    except
        on E : Exception do begin
            MsgBoxErr(MSG_REPORT_ERROR, E.Message, MSG_ERROR, MB_ICONERROR);
            Exit;
        end;
    end;
end;

procedure TFGenerarArchivoNovedades.btnProcesarClick(Sender: TObject);
begin
    GenerarArchivoNovedades;
end;

{-----------------------------------------------------------------------------
  Function Name: FormKeyDown
  Author:    lgisuk
  Date Created: 25/04/2005
  Description: Permito Cancelar
  Parameters: Sender: TObject;var Key: Word; Shift: TShiftState
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoNovedades.FormKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
begin
    if key = VK_ESCAPE then begin
        FCancelar := True;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: FormCloseQuery
  Author:    lgisuk
  Date Created: 25/04/2005
  Description: Permite salir solo si no esta procesando.
  Parameters: Sender: TObject;var CanClose: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoNovedades.FormCloseQuery(Sender: TObject;var CanClose: Boolean);
begin
    //Permite salir solo si no esta procesando.
	CanClose := not PnlAvance.visible;
end;

{-----------------------------------------------------------------------------
  Function Name: SalirBTNClick
  Author:    lgisuk
  Date Created: 25/04/2005
  Description: cierro el formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoNovedades.SalirBTNClick(Sender: TObject);
begin
	Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 25/04/2005
  Description: Lo libero de memoria
  Parameters: Sender: TObject;var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFGenerarArchivoNovedades.FormClose(Sender: TObject;var Action: TCloseAction);
begin
	Action := caFree;
end;

{******************************** Procedure Header *****************************
Procedure Name: GenerarArchivoNovedades
Author : pdominguez
Date Created : 03/07/2009
Parameters : None
Description : SS 812
        - Se reescribe el proceso de generaci�n del Archivo de Env�o de Novedades.
            Se crean los siguientes procedimientos / funciones:
                ActualizarNovedadesCMR,
                RegistrarInicioEnLogOperaciones,
                GrabarArchivo,
                RegistrarNumeroSecuencia,
                ExisteError,
                RegistrarFinalizacionEnLogOperaciones.
        - Se implementa el reenvio de mandatos no confirmados.

Revision : 1
    Author : pdominguez
    Date   : 24/07/2009
    Description : SS 812
        - Se corrige el error en la conversi�n de la sumatoria de cuentas a String
        para evitar la salida de la sumatoria en anotaci�n cient�fica. Se cambian
        las funciones FloatToStr por CurrToStr y StrToFloat por StrToCurr.
*******************************************************************************}
procedure TFGenerarArchivoNovedades.GenerarArchivoNovedades;
    resourcestring
        MSG_EXISTEN_NOVEDADES     = 'Existen novedades anteriores para la fecha que se ha especificado. � Desea generar igual el archivo de "Novedades" ?';
        MSG_OBSERVACION_INTERFASE = 'Generar Archivo Novedades';

        MSG_ARCHIVO_YA_EXISTE = 'El archivo: %s ya existe en el directorio de destino. � Desea reemplazarlo ?';
        MSG_ATENCION_CAPTION  = ' Atenci�n';

        MSG_ERROR_GUARDANDO_ARCHIVO   = 'Se produjo un error al guardar el Archivo.';
        MSG_ERROR_DETALLE_MANDATO     = 'Se produjo un error al almacenar el detalle del Archivo de Mandatos. Error: %s';
        MSG_ERROR_INICIO_INTERFASE    = 'Se produjo un error al Iniciar el Registro de la Interfase. Error: %s';
        MSG_ERROR_NUMERO_SECUENCIA    = 'Se produjo un error al almacenar el N�mero de Secuencia. Error: %s';
        MSG_ERROR_REGISTRAR_INTERFASE = 'Se produjo un error al registrar la interfase: Error: %s';
        MSG_ERROR_FINALIZAR_INTERFASE = 'Se produjo un error al finalizar la interfase: Error: %s';
        MSG_ERROR_PROCESO             = 'El proceso finaliz� con errores.';

        MSG_ERROR_INESPERADO                    = 'Se produjo un Error Inesperado en el proceso.';
        MSG_ERROR_INESPERADO_ERRORES_ANTERIORES = 'Errores en Proceso: %s' + CRLF + CRLF + 'Error Inesperado: %s';
        MSG_ERROR_INESPERADO_ERROR              = 'Error Inesperado: %s';
        MSG_ERROR_ACTUALIZANDO_NOVEDADES        = 'Se produjo un Error actualizando las Novedades Enviadas.';

        MSG_FINALIZO_OK                    = 'Finalizo OK!';
        MSG_FINALIZO_CON_ERRORES           = 'El proceso finaliz� con errores. %s';
        MSG_CANCELADO_USUARIO              = 'El proceso fue cancelado por el Usuario.';
        MSG_NO_EXISTEN_MANDATOS_A_PROCESAR = 'No hay Mandatos PAT con novedades para informar!';

        MSG_INICIANDO_PROCESO      = 'Iniciando Proceso. Espere Por Favor ...';
        MSG_SELECCIONANDO_DATOS    = 'Seleccionando Datos a Procesar. Espere Por Favor ...';
        MSG_PROCESANDO_DATOS       = 'Procesando Datos Seleccionados. Espere Por Favor ...';
        MSG_GRABANDO_FICHERO       = 'Grabando Fichero de Novedades. Espere Por Favor ...';
        MSG_ACTUALIZANDO_NOVEDADES = 'Actualizando Datos de las Novedades Enviadas. Espere Por Favor ...';
        MSG_FINALIZANDO_PROCESO    = 'Finalizando Proceso. Espere Por Favor ...';
        MSG_PROCESO_FINALIZADO     = 'Proceso Finalizado.';

    var
        DescError,
        ObservacionesProceso: AnsiString;
        RegistrosArchivoNovedades: TStringList;
        //SumatoriaCuentas: Real;           		//REV.4
        SumatoriaCuentas : int64;                	//REV.4
        FechaInterfase: TDateTime;
        SQLObtenerFecha: String;
        RegistrosRefresco: Integer;

    {-----------------------------------------------------------------------------
        Function Name: ReemplazarKporNueve
        Author:    lgisuk
        Date Created: 22/06/2005
        Description: Reemplaza la K que viene en algunos numeros de convenio
                por un Nueve, para conseguir que cada numero de convenio
                sea un valor numerico y se pueda sumar
        Parameters: valor:string
        Return Value: string
    -----------------------------------------------------------------------------}
    Function ReemplazarKporNueve(valor : String) : String;
    begin
        Result := AnsiReplaceStr(Uppercase(valor), 'K', '9');
    end;

   {******************************** Procedure Header *****************************
        Procedure Name: ActualizarNovedadesCMR
        Author : pdominguez
        Date Created : 16/07/2009
        Parameters : CodigoInterfase: Integer; FechaInterfase: TDateTime; var DescError: AnsiString
        Description : SS 812
            - Actualizamos el campo FechaUltimoEnvioNovedad de la tabla
            ConvenioMedioPagoAutomaticoPAT e insertamos los mandatos enviados en la
            Interfase en la tabla DetalleMandatosenviados.
    *******************************************************************************}
    Procedure ActualizarNovedadesCMR(CodigoInterfase: Integer; FechaInterfase: TDateTime; var DescError: AnsiString);
    begin
        try
            lblMensaje.Caption := MSG_ACTUALIZANDO_NOVEDADES;
            Application.ProcessMessages;

            with spActualizarNovedadesEnviadasCMR do begin
                Parameters.ParamByName('@CodigoOperacionInterfase').Value := CodigoInterfase;
                Parameters.ParamByName('@FechaInterfase').Value := FechaInterfase;
                ExecProc;
            end;
        except
            on E:Exception do
                DescError := E.Message;
        end;

    end;

   {******************************** Function Header ******************************
        Function Name: RegistrarInicioEnLogOperaciones
        Author : pdominguez
        Date Created : 06/07/2009
        Parameters : var DescError: AnsiString
        Return Value : Boolean
        Description : SS 812
            - Registramos el inicio del proceso en el Log de Operaciones.
    *******************************************************************************}
    Function RegistrarInicioEnLogOperaciones(var DescError: AnsiString): Boolean;
    begin
        if not RegistrarOperacionEnLogInterface(
                    DMConnections.BaseCAC,
                    RO_MOD_INTERFAZ_SALIENTE_NOVEDADES_CMR,
                    Trim(ExtractFileName(FNombreArchivoNovedades)),
                    UsuarioSistema,
                    MSG_OBSERVACION_INTERFASE,
                    False,
                    False,
                    StrToDateTime(FFechaDesde),
                    0,
                    FCodigoOperacion,
                    DescError) then DescError := Format(MSG_ERROR_INICIO_INTERFASE,[DescError]);
        Result := DescError = '';
    end;

   {******************************** Function Header ******************************
        Function Name: GrabarArchivo
        Author : pdominguez
        Date Created : 06/07/2009
        Parameters : var DescError: AnsiString
        Return Value : Boolean
        Description : SS 812
            - Grabamos el archivo generado a disco.
    *******************************************************************************}
    function GrabarArchivo(var DescError: AnsiString): Boolean;
    begin
        lblMensaje.Caption := MSG_GRABANDO_FICHERO;
        Application.ProcessMessages;

        if not StringToFile(RegistrosArchivoNovedades.Text,FNombreArchivoNovedades) Then
            DescError := MSG_ERROR_GUARDANDO_ARCHIVO;
        Result := DescError = '';
    end;

   {******************************** Procedure Header *****************************
        Procedure Name: RegistrarNumeroSecuencia
        Author : pdominguez
        Date Created : 06/07/2009
        Parameters : var DescError: AnsiString
        Description : SS 812
            - Almacenamos el N�mero de Secuencia en Log de Operaciones.
    *******************************************************************************}
    procedure RegistrarNumeroSecuencia(var DescError: AnsiString);
    begin
        If not ActualizarNumeroSecuencia(
                    DMConnections.BaseCAC,
                    FCodigoOperacion,
                    FNumeroSecuencia,
                    DescError) then DescError := Format(MSG_ERROR_NUMERO_SECUENCIA,[DescError]);
    end;

   {******************************** Function Header ******************************
        Function Name: ExisteError
        Author : pdominguez
        Date Created : 06/07/2009
        Parameters : None
        Return Value : Boolean
        Description : SS 812
            - Devuelve verdadero si se ha producido alg�n error durante el proceso
            mirando si la variable DescError contiene informaci�n.
    *******************************************************************************}
    function ExisteError: Boolean;
    begin
        Result := DescError <> '';
    end;

   {******************************** Procedure Header *****************************
        Procedure Name: RegistrarFinalizacionEnLogOperaciones
        Author : pdominguez
        Date Created : 06/07/2009
        Parameters : var DescError: AnsiString
        Description : ss 812
            - Actualizamos los datos de finalizaci�n del proceso en el Log de
            Operaciones.
   *******************************************************************************}
    procedure RegistrarFinalizacionEnLogOperaciones(var DescError: AnsiString);
        var
            FinalizoOK: Integer;
        const
            SQLActualizarLogOperaciones =
                'UPDATE LogOperacionesInterfases ' +
                'SET Observaciones = ''%s'', FinalizoOK = %d, FechaHoraFinalizacion = GetDate() ' +
                'WHERE CodigoOperacionInterfase = %d';
    begin
        try
            FinalizoOK := IIf(((not FCancelar) and (not ExisteError)),1,0);

            QueryExecute(
                DMConnections.BaseCAC,
                Format(SQLActualizarLogOperaciones,[ObservacionesProceso,FinalizoOK,FCodigoOperacion]));
        except
            on E:Exception do begin
                DescError := Format(MSG_ERROR_FINALIZAR_INTERFASE,[e.Message]);
            end;
        end;
    end;
begin
    //si la fecha sugerida al operador es menor a la fecha elegida
    if FFechaSugerida < EdFecha.Date then
        //pido confirmaci�n al operador para continuar
        if MsgBox(MSG_EXISTEN_NOVEDADES,MSG_ATENCION_CAPTION,MB_YESNO  + MB_ICONINFORMATION) = mrNo then exit;
    // Si el archivo ya existe, pedimos confirmaci�n para continuar y sobreescribir el antiguo.
    if FileExists(FNombreArchivoNovedades) then
        if (MsgBox(Format(MSG_ARCHIVO_YA_EXISTE,[ExtractFileName(FNombreArchivoNovedades)]),MSG_ATENCION_CAPTION , MB_YESNO + MB_ICONQUESTION) = IDYES) then
            DeleteFile(FNombreArchivoNovedades)
        else Exit;

    try
        DescError := '';
        FCancelar := False;

      	btnprocesar.Enabled := False;
        SalirBTN.Enabled    := False;
        KeyPreview          := True;
        lblMensaje.Caption  := MSG_INICIANDO_PROCESO;
        PnlAvance.visible   := True;

      	Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        try
            // Si registramos la Interfase iniciamos el proceso
            if RegistrarInicioEnLogOperaciones(DescError) then
                With SPObtenerMandatosNovedadesCMR do begin

                    lblMensaje.Caption := MSG_SELECCIONANDO_DATOS;
                    Application.ProcessMessages;
                    Parameters.ParamByName('@FechaInterfase').Value := FFechaDesde;
                    Open;

                    if RecordCount > 0 then try

                        pbProgreso.Max     := RecordCount;
                        lblMensaje.Caption := MSG_PROCESANDO_DATOS;
                        RegistrosRefresco  := RecordCount div 100;
                        if RegistrosRefresco = 0 then RegistrosRefresco := 1;
                        Application.ProcessMessages;

                        // Obtenemos la Fecha asignada a la Interfase para actualizar el campo FechaUltimoEnvioNovedad
                        // de la tabla ConvenioMedioPagoAutomaticoPAT.
                        SQLObtenerFecha := Format('SELECT dbo.DarFecha(Fecha) FROM LogOperacionesInterfases WHERE CodigoOperacionInterfase = %d', [FCodigoOperacion]);
                        FechaInterfase  := QueryGetValueDateTime(DMConnections.BaseCAC, SQLObtenerFecha);

                        RegistrosArchivoNovedades := TStringList.Create;
                        SumatoriaCuentas := 0;
                        DescError := '';

                        while (not Eof) and (not ExisteError) and (not FCancelar) do begin
                            if FCodigoNativa = CODIGO_VS then begin                                 // SS_1147_CQU_20150324
                                RegistrosArchivoNovedades.Add(                                      // SS_1147_CQU_20150324
                                    FieldByName('TipoDeRegistro').AsString +                        // SS_1147_CQU_20150324
                                    FieldByName('NumeroDeComercio').AsString +                      // SS_1147_CQU_20150324
                                    PadR(Trim(FieldByName('NumerodeContrato').AsString), 20, ' ') + // SS_1147_CQU_20150324
                                    //PadL(trim(FieldByName('RutTarjetaHabiente').AsString), 11, '0') + // SS_1147_CQU_20150428       // SS_1147_CQU_20150324
                                    PadR(Trim(FieldByName('RutTarjetaHabiente').AsString), 11, ' ') +   // SS_1147_CQU_20150428
                                    PadR(FieldByName('DEIC_TARJETA').AsString,16,' ') +             // SS_1147_CQU_20150324
                                    PadL('', 3, ' ') + // Filler 3                                  // SS_1147_CQU_20150324
                                    FieldByName('DEIC_FEC_EXPIRA').AsString +                       // SS_1147_CQU_20150324
                                    FieldByName('OrigenMandato').AsString +                         // SS_1147_CQU_20150324
                                    'INF' +                                                         // SS_1147_CQU_20150324
                                    PadL('', 2, ' ') + // Filler                                    // SS_1147_CQU_20150324
                                    FieldByName('DiasCorridosExcepcion').AsString +                 // SS_1147_CQU_20150324
                                    PadL('', 23, ' ') + // Filler                                   // SS_1147_CQU_20150324
                                    FieldByName('DiaDeVencimiento').AsString +                      // SS_1147_CQU_20150324
                                    PadL('', 48, ' ')); // Filler                                   // SS_1147_CQU_20150324
                            end else begin                                                          // SS_1147_CQU_20150324
                                RegistrosArchivoNovedades.Add(
                                    FieldByName('TipoDeRegistro').AsString +
                                    FieldByName('NumeroDeComercio').AsString +
                                    FieldByName('NumerodeContrato').AsString +
                                    FieldByName('Filler').AsString +
                                    PadR(FieldByName('RutTarjetaHabiente').AsString,11,' ') +
                                    PadR(FieldByName('DEIC_TARJETA').AsString,19,' ') +
                                    FieldByName('DEIC_FEC_EXPIRA').AsString +
                                    FieldByName('OrigenMandato').AsString +
                                    FieldByName('EstadoInstruccion').AsString +
                                    FieldByName('DiasCorridosExcepcion').AsString +
                                    FieldByName('CodigoDeRespuesta').AsString +
                                    FieldByName('GlosaDeRespuesta').AsString +
                                    FieldByName('DiaDeVencimiento').AsString +
                                    FieldByName('Observaciones').AsString);
                            end;                                                                    // SS_1147_CQU_20150324
                            SumatoriaCuentas :=
                                SumatoriaCuentas +
                                //REV.4 StrToCurr(ReemplazarKporNueve(FieldByName('NumerodeContrato').asstring)); // Rev. 1 (SS 812)
                                //StrToint64(ReemplazarKporNueve(FieldByName('NumerodeContrato').asstring));        // SS_1147_CQU_20150324 // REV.4
                                StrToint64(ReemplazarKporNueve(Trim(FieldByName('NumerodeContrato').asstring)));    // SS_1147_CQU_20150324

                            if RecNo >= RegistrosRefresco then
                                if RecNo mod RegistrosRefresco = 0 then begin
                                    pbProgreso.Position := RecNo;
                                    Application.ProcessMessages;
                                end;

                            if not ExisteError then Next;
                        end;

                        pbProgreso.Position := RecordCount;
                        Application.ProcessMessages;

                        if (not ExisteError) and (not FCancelar) then begin
                            // Insertamos el Registro de Cabecera
                            if FCodigoNativa = CODIGO_VS then begin                                                 // SS_1147_CQU_20150324
                                RegistrosArchivoNovedades.Insert(                                                   // SS_1147_CQU_20150324
                                    0,                                                                              // SS_1147_CQU_20150324
                                    FormatDateTime('ddmmyyyy',NowBase(DMConnections.BaseCAC)) + // Fecha Envio      // SS_1147_MCA_20150417 // SS_1147_CQU_20150324
                                    PADL(IntToStr(RegistrosArchivoNovedades.count),12,'0') +    // Cant.Registros   // SS_1147_CQU_20150324
                                    PADL(IntToStr(SumatoriaCuentas),20,'0') +                   // Sum.Nro.Cuentas  // SS_1147_CQU_20150324
                                    //PADL('',109,'.'));                                          // Relleno        // SS_1147_CQU_20150423  // SS_1147_CQU_20150324
                                    PADL('',109,' '));                                          // Relleno          // SS_1147_CQU_20150423
                            end else begin                                                                          // SS_1147_CQU_20150324
                            RegistrosArchivoNovedades.Insert(
                                0,
                                FormatDateTime('ddmmyyyy',NowBase(DMConnections.BaseCAC)) +
                                PADL(IntToStr(RegistrosArchivoNovedades.count),12,'0') +
                                Space(2) +
                                //REV.4 PADL(CurrToStr(SumatoriaCuentas),18,'0') +  // Rev. 1 (SS 812)
                                PADL(IntToStr(SumatoriaCuentas),18,'0') +				//REV.4
                                PADL('',109,' '));
                            end;                                                                                    // SS_1147_CQU_20150324
                            // Grabamos el Archivo y registramos el n�mero de secuencia.
                            if GrabarArchivo(DescError) then RegistrarNumeroSecuencia(DescError);
                        end;

                        if not ExisteError then ActualizarNovedadesCMR(FCodigoOperacion,FechaInterfase,DescError);

                        if (not ExisteError) and (not FCancelar) then begin
                            ObservacionesProceso := MSG_FINALIZO_OK;
                        end else begin
                            if FileExists(FNombreArchivoNovedades) then DeleteFile(FNombreArchivoNovedades);
                            if FCancelar then
                                ObservacionesProceso := MSG_CANCELADO_USUARIO
                            else ObservacionesProceso := Format(MSG_FINALIZO_CON_ERRORES,[DescError]);
                        end;

                        lblMensaje.Caption := MSG_FINALIZANDO_PROCESO;
                        Application.ProcessMessages;

                    finally
                        FreeAndnil(RegistrosArchivoNovedades);
                    end else ObservacionesProceso := MSG_NO_EXISTEN_MANDATOS_A_PROCESAR;
                    // Actualizamos el registro en log de operaciones.
                    RegistrarFinalizacionEnLogOperaciones(DescError);

                    lblMensaje.Caption := MSG_PROCESO_FINALIZADO;
                    Application.ProcessMessages;
                end;

            Screen.Cursor := crDefault;
            Application.ProcessMessages;

            if FCancelar then
                MsgBox(ObservacionesProceso, Self.Caption, MB_OK + MB_ICONINFORMATION)
            else if ExisteError then
                MsgBoxErr(MSG_ERROR_PROCESO, DescError, Self.Caption, MB_ICONERROR)
            else begin
                MsgBox(ObservacionesProceso, Self.Caption, MB_OK + MB_ICONINFORMATION);
                // Lanzamos el Reporte final sino ha habido ning�n error en el proceso
                // y hemos procesado registros.
                if SPObtenerMandatosNovedadesCMR.RecordCount > 0 then
                    GenerarReporteFinalizacionProceso(FCodigoOperacion);
            end;

        except
            on E:Exception do begin
                if FileExists(FNombreArchivoNovedades) then DeleteFile(FNombreArchivoNovedades);

                Screen.Cursor := crDefault;
                Application.ProcessMessages;

                try
                    RegistrarFinalizacionEnLogOperaciones(DescError);

                    if ExisteError then
                        MsgBoxErr(MSG_ERROR_INESPERADO, Format(MSG_ERROR_INESPERADO_ERRORES_ANTERIORES,[DescError,e.Message]), Self.Caption, MB_ICONERROR)
                    else
                        MsgBoxErr(MSG_ERROR_INESPERADO, Format(MSG_ERROR_INESPERADO_ERROR,[e.Message]), Self.Caption, MB_ICONERROR)
                except
                    on E:Exception do begin
                        if ExisteError then
                            MsgBoxErr(MSG_ERROR_INESPERADO, Format(MSG_ERROR_INESPERADO_ERRORES_ANTERIORES,[DescError,e.Message]), Self.Caption, MB_ICONERROR)
                        else
                            MsgBoxErr(MSG_ERROR_INESPERADO, Format(MSG_ERROR_INESPERADO_ERROR,[e.Message]), Self.Caption, MB_ICONERROR)
                    end;
                end;
            end;
        end;

    finally
        if SPObtenerMandatosNovedadesCMR.Active then SPObtenerMandatosNovedadesCMR.Close;
        PnlAvance.visible   := False;
        KeyPreview          := False;
      	btnprocesar.Enabled := True;
        SalirBTN.Enabled    := True;
        lblMensaje.Caption  := '';
    end;
end;

end.

