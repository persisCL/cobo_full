{-------------------------------------------------------------------------------
 File Name: FrmRvm.pas
 Author:    lgisuk
 Date Created: 23/02/2005
 Language: ES-AR
 Description:  Modulo de la Interface RNVM - Consulta al RNVM

 Revision 1:
     Author : ggomez
     Date : 21/02/2006
     Description :
        - Agregu� el CheckBox para forzar la consulta al RNVM. Si se
        selecciona, se har� la consulta al RNVM sin verificar las condiciones de
        existencia de datos recientes, ni de cantidad de consultas realizadas.
        - Si no se desea forzar la consulta, se busca el c�digo de infracci�n
        m�s reciente para la patente. Esto se hace para poder pasar el par�metro
        CodigoInfractor al m�todo TDMActualizarDatosInfractor.Actualizar.

 Autor		: Mcabello
 Fecha		: 11/11/2013
 Firma		: SS_1143_MCA_20131111
 Description: Se agrega bot�n Abrir XML que permite abrir el �ltimo documento xml almacenado de la consulta al RNVM,
 			adem�s se agregan nuevos campos en la consulta al RNVM y Consulta �ltima Modificaci�n. Fecha Adquisi�n del Vehiculo

 Firma      : SS_1227_MCA_20141105
 Descripcion: se agrega validaci�n para que la patente consultada contenga 6 digitos

 Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)


--------------------------------------------------------------------------------}
unit FrmRvm;

interface

uses
  //Consulta al RNVM  
  DMConnection,
  UtilProc,
  Util,
  PeaProcs,
  ConstParametrosGenerales,
  RVMClient,
  RVMTypes,
  RVMLogin,
  ActualizarDatosInfractor,
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, RNVMConfig, Menus,
  xmldom, XMLIntf, msxmldom, XMLDoc, DB, ADODB,
  OleCtrls, SHDocVw;						//SS_1143_MCA_2013111

type
  TFRvm = class(TForm)
    Memo: TMemo;
    pnlAbajo: TPanel;
    pnlConsulta: TGroupBox;
    lblpatente: TLabel;
    txtPatente: TEdit;
    btnConsultar: TButton;
    btnCerrar: TButton;
    cbUpdate: TCheckBox;
    BTNUltimaModificacion: TButton;
    SPObtenerUltimaConsultaRNVM: TADOStoredProc;
    chbForzarConsultaRNVM: TCheckBox;
    ImgAyuda: TImage;
    btnAbrirXML: TButton;														//SS_1143_MCA_20131111
    wbXML: TWebBrowser;                                                         //SS_1143_MCA_20131111
    lblFechaConsulta: TLabel;                                                   //SS_1143_MCA_20131111
    procedure ImgAyudaClick(Sender: TObject);
    procedure BTNUltimaModificacionClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCerrarClick(Sender: TObject);
    procedure btnConsultarClick(Sender: TObject);
    procedure btnAbrirXMLClick(Sender: TObject);                                //SS_1143_MCA_2013111
    procedure txtPatenteChange(Sender: TObject);                                //SS_1143_MCA_2013111
  private
    FUsername: String;
    FPassWord: String;
    FPathXML: string;															//SS_1143_MCA_2013111
    Act: TDMActualizarDatosInfractor;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216

    function ObtenerMaximoCodigoInfraccionDePatente(Patente: AnsiString): Integer;
  public
    Function Inicializar: Boolean;
    { Public declarations }
  end;

const
    FILE_EXTENSION = '.xml';
var
  FRvm: TFRvm;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    lgisuk
  Date Created: 23/02/2005
  Description: M�dulo de inicializaci�n de este formulario
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFRvm.Inicializar: Boolean;
resourcestring
     MSG_ERROR_OBTENER_PARAMETRO_GENERAL = 'Error al obtener informaci�n de par�metros generales';
     MSG_ERROR_PARAMETRO_INEXISTENTE = 'No se encontro informaci�n del parametro general RNVM_Directorio_Respuestas';		//SS_1143_MCA_20131111
     MSG_ERROR = 'Error';
begin
	Result := False;                                                                               //SS_1143_MCA_20131111
    
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

    ActiveControl := txtPatente;
    //cbUpdate.Checked := True;                                                                    //SS_1143_MCA_20131111
    BTNUltimaModificacion.Enabled 	:= ExisteAcceso('Boton_Consulta_Ultima_modificacion');         //SS_1143_MCA_20131111
    btnConsultar.Enabled            := ExisteAcceso('Boton_Consulta_RNVM');                        //SS_1143_MCA_20131111
	btnAbrirXML.Enabled				:= ExisteAcceso('Boton_AbrirXML');                         	   //SS_1143_MCA_20131111
    cbUpdate.Enabled				:= ExisteAcceso('Check_Actualizar_Datos_Infractores');         //SS_1143_MCA_20131111
    chbForzarConsultaRNVM.Enabled	:= ExisteAcceso('Check_Forzar_Consulta_RNVM');                 //SS_1143_MCA_20131111
    cbUpdate.Checked := cbUpdate.Enabled;                                                          //SS_1143_MCA_20131111

	if not ObtenerParametroGeneral(DMConnections.BaseCAC,'RNVM_Directorio_Respuestas', FPathXML) then begin    //SS_1143_MCA_20131111
    	MsgBoxErr(MSG_ERROR_OBTENER_PARAMETRO_GENERAL + Space (1) + 'RNVM_Directorio_Respuestas', MSG_ERROR_PARAMETRO_INEXISTENTE, Caption, MB_ICONSTOP); //SS_1143_MCA_20131111
        Exit;                                                                   //SS_1143_MCA_20131111
    end;                                                                        //SS_1143_MCA_20131111

	FPathXML := GoodDir(FPathXML);												//SS_1143_MCA_20131111
    Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: btnConsultarClick
  Author:    lgisuk
  Date Created: 23/02/2005
  Description: consulto los datos al RVM
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRvm.btnConsultarClick(Sender: TObject);
resourcestring
    MSG_ERROR_IN_QUERY = 'Error en la consulta';
    MSG_ERROR = 'Error';
    MSG_ERROR_OBTENER_CODIGO_INFRACCION = 'Ha ocurrido un error al obtener el C�digo de la Infracci�n m�s reciente.';
    MSG_ERROR_PATENTE = 'No se puede realizar la consulta, Formato de Patente inv�lido';    //SS_1227_MCA_20141105
Const
    VEHICLE_INFORMATION = 'Informaci�n referida al autom�vil';
    LICENSEPLATE = 'Patente: ';
    CATEGORY = 'Categoria: ';
    MANUFACTURER = 'Fabricante: ';
    MODEL = 'Modelo: ';
    COLOR = 'Color: ';
    ENGINEID = 'Id. Motor: ';
    CHASSISID = 'Id. Chassis: ';
    SERIAL = 'Nro. Serie: ';
    VINNUMBER = 'Nro. VIN: ';
    YEAR = 'A�o: ';
    INSURANCE_INFORMATION = 'Informaci�n referida al seguro del autom�vil';
    ENTITY = 'Entidad: ';
    CONTRACTID = 'Id. Contrato: ';
    EXPIRATIONDATE = 'Fecha de Vencimiento: ';
    OWNER_INFORMATION = 'Informaci�n referida al propietario del autom�vil';
    OWNERID1 = 'Id. Propietario 1: ';
    OWNERNAME1 = 'Nombre Propietario 1: ';
    OWNERID2 = 'Id. Propietario 2: ';
    OWNERNAME2 = 'Nombre Propietario 2: ';
    ACQUISITION = 'Fecha de Adquisici�n del autom�vil';                         //SS_1143_MCA_20131111
    ACQUISITIONDATE = 'Fecha de Adquisici�n: ';
    REGISTRATION = 'Inscripci�n: ';                                             //SS_1143_MCA_20131111
    REGISTRATIONNUMBER = 'N� Inscripci�n: ';                                    //SS_1143_MCA_20131111
    REGISTRATIONDATE = 'Fecha Inscripci�n: ';                                   //SS_1143_MCA_20131111
    ANNOTATIONS = 'Anotaciones:';
    WITHOUT_ANNOTATIONS = '< Sin Anotaciones >';
    OTHERS_OWNERS_INFORMATION = 'Informaci�n referida al propietario anterior del autom�vil';
    NOTE1 = 'Nota 1: ';
    NOTE2 = 'Nota 2: ';
    REPERTOIRE = 'Repertorio: ';
    MSG_NO_EXISTE_VEV = 'La patente solicitada NO existe en el RNVM';
    MSG_TITULO = 'Patente Inexistente';
    REGISTERED_ADDRESS = 'DOMICILIO REGISTRADO';
    STREET = 'Calle: ';
    NUMBER = 'Numero: ';
    LETTER = 'Letra: ';
    REST_DIRECTION = 'Resto Direcci�n: ';
    COMMUNE = 'Comuna: ';
    RESERVED = 'Reservado';
var
    f: TfrmLogin;
    Respuesta: TRVMInformation;
    i: Integer;
    TextoError: String;
    CodigoInfraccion: Integer;
begin
    if txtpatente.Text = '' then Exit;
    if Length(txtPatente.Text) <> 6 then begin                                  //SS_1227_MCA_20141105
        MsgBoxBalloon(MSG_ERROR_PATENTE, Caption, MB_ICONERROR, txtPatente);    //SS_1227_MCA_20141105
        Exit;                                                                   //SS_1227_MCA_20141105
    end;                                                                        //SS_1227_MCA_20141105
    Memo.Lines.Clear;
    wbXML.SendToBack;         											        //SS_1143_MCA_20131111
    wbXML.Navigate('about:blank');                                              //SS_1143_MCA_20131111
    memo.BringToFront;                                                          //SS_1143_MCA_20131111
    lblFechaConsulta.Caption := EmptyStr;                                       //SS_1143_MCA_20131111

    Act := TDMActualizarDatosInfractor.Create(Self);
    if not Act.ConfigurarParametrosGenerales(TextoError) then begin
        MsgBox(MSG_ERROR, TextoError, MB_ICONERROR);
        FreeAndNil(Act);
        Exit;
    end;
    try
        Application.CreateForm(TfrmLogin, f);
        f.UserName := 'cnorte';
        if f.ShowModal = mrOK then begin
            FUsername := f.UserName;
            FPassword := f.Password;
        end else Exit;
    finally
        F.Free;
    end;
    try
        Screen.Cursor := crHourGlass;

        CodigoInfraccion := 0;
        // Si NO se est� forzando la consulta al RNVM, buscar el c�digo de
        // Infracci�n M�ximo para la patente.
        if not chbForzarConsultaRNVM.Checked then begin
            try
                CodigoInfraccion := ObtenerMaximoCodigoInfraccionDePatente(Trim(txtPatente.Text));
            except
                on E: Exception do begin
                    MsgBoxErr(MSG_ERROR_OBTENER_CODIGO_INFRACCION, E.Message, Caption, MB_ICONSTOP);
                    Exit;
                end;
            end;
        end;

        if Act.Actualizar(not(cbUpdate.Checked), Trim(txtPatente.Text),
                CodigoInfraccion, FUserName, FPassWord, 0, Respuesta,
                TextoError, not chbForzarConsultaRNVM.Checked) then begin
            // Procesar la respuesta
            Memo.Lines.Add(VEHICLE_INFORMATION);
            Memo.Lines.Add('');
            Memo.Lines.Add(LICENSEPLATE + Respuesta.RequestedLicensePlate);
            Memo.Lines.Add(CATEGORY + Respuesta.VehicleInformation.Category);
            Memo.Lines.Add(MANUFACTURER + Respuesta.VehicleInformation.Manufacturer);
            Memo.Lines.Add(MODEL + Respuesta.VehicleInformation.Model);
            Memo.Lines.Add(COLOR + Respuesta.VehicleInformation.Color);
            Memo.Lines.Add(ENGINEID + Respuesta.VehicleInformation.EngineID);
            Memo.Lines.Add(CHASSISID + Respuesta.VehicleInformation.ChassisID);
            Memo.Lines.Add(SERIAL + Respuesta.VehicleInformation.Serial);
            Memo.Lines.Add(VINNUMBER + Respuesta.VehicleInformation.VINNumber);
            Memo.Lines.Add(YEAR + Respuesta.VehicleInformation.Year);
            Memo.Lines.Add('');
            Memo.Lines.Add(INSURANCE_INFORMATION);
            Memo.Lines.Add('');
            Memo.Lines.Add(ENTITY + Respuesta.InsuranceInformation.Entity);
            Memo.Lines.Add(CONTRACTID + Respuesta.InsuranceInformation.ContractID);
            Memo.Lines.Add(EXPIRATIONDATE + Respuesta.InsuranceInformation.ExpirationDate);
            Memo.Lines.Add('');
            Memo.Lines.Add(OWNER_INFORMATION);
            Memo.Lines.Add('');
            Memo.Lines.Add(OWNERID1 + Respuesta.OwnerInformation.OwnerID1);
            Memo.Lines.Add(OWNERNAME1 + Respuesta.OwnerInformation.OwnerName1);
            Memo.Lines.Add('');
            Memo.Lines.Add('Amo-COM');
            Memo.Lines.Add('RUT 1: ' + Respuesta.OwnerInformationCOM.RUT1);
            Memo.Lines.Add('Nombre1: ' + Respuesta.OwnerInformationCOM.Nombre1);
            Memo.Lines.Add('RUT 2: ' + Respuesta.OwnerInformationCOM.RUT2);
            Memo.Lines.Add('Nombre 2: ' + Respuesta.OwnerInformationCOM.Nombre2);
            Memo.Lines.Add('');
			Memo.Lines.Add(ACQUISITION);                                                        	//SS_1143_MCA_20131111
            Memo.Lines.Add(ACQUISITIONDATE + Respuesta.OwnerRegistration.AcquisitionDate);    		//SS_1143_MCA_20131111
            Memo.Lines.Add(REGISTRATION + Respuesta.OwnerRegistration.Registration);              	//SS_1143_MCA_20131111
            Memo.Lines.Add(REGISTRATIONNUMBER + Respuesta.OwnerRegistration.RegistrationNumber);    //SS_1143_MCA_20131111
            Memo.Lines.Add(REGISTRATIONDATE + Respuesta.OwnerRegistration.RegistrationDate);    	//SS_1143_MCA_20131111
        	Memo.Lines.Add('');                                                                     //SS_1143_MCA_20131111
            Memo.Lines.Add(REGISTERED_ADDRESS);
            Memo.Lines.Add(STREET + Respuesta.AddressInformation.Street);
            Memo.Lines.Add(NUMBER + Respuesta.AddressInformation.DoorNumber);
            Memo.Lines.Add(LETTER + Respuesta.AddressInformation.Letter);
            Memo.Lines.Add(REST_DIRECTION + Respuesta.AddressInformation.OtherInfo);
            Memo.Lines.Add(COMMUNE + Respuesta.AddressInformation.Commune);
            Memo.Lines.Add(RESERVED + Respuesta.AddressInformation.Field6);
            Memo.Lines.Add('');
            Memo.Lines.Add(ANNOTATIONS);
            Memo.Lines.Add('');
            Memo.Lines.Add(Respuesta.VehicleNotes);
            for i:= Low(Respuesta.DomainConstraints.Notas) to High(Respuesta.DomainConstraints.Notas) do
                if Trim(Respuesta.Domainconstraints.Notas[i]) <> '' then
                    Memo.Lines.Add(Respuesta.Domainconstraints.Notas[i]);

            Memo.Lines.Add('');
            Memo.Lines.Add(OTHERS_OWNERS_INFORMATION);
            Memo.Lines.Add('');
            Memo.Lines.Add(NOTE1 + Respuesta.DomainRequests.Nota1);
            Memo.Lines.Add(NOTE2 + Respuesta.DomainRequests.Nota2);
            Memo.Lines.Add(OWNERID1 + Respuesta.OthersOwnersInformation.OwnerID1);
            Memo.Lines.Add(OWNERNAME1 + Respuesta.OthersOwnersInformation.OwnerName1);
            Memo.SelStart := 0;
            Memo.SetFocus;
            if TextoError <> '' then MsgBox(TextoError);
        end else begin
            MsgBoxErr(MSG_ERROR, TextoError, MSG_ERROR_IN_QUERY, MB_ICONERROR);
        end;
    finally
        Screen.Cursor := crDefault;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: BTNUltimaModificacionClick
  Author:    lgisuk
  Date Created: 21/02/2006
  Description: Consulto los datos obtenidos en la ultima consulta al RNVM
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRvm.BTNUltimaModificacionClick(Sender: TObject);
resourcestring
    MSG_ERROR_IN_QUERY = 'Error en la consulta';
    MSG_ERROR_EMPTY = 'No hay Informaci�n para la patente ingresada!';
    MSG_ERROR = 'Error';
    MSG_ERROR_PATENTE = 'No se puede realizar la consulta, Formato de Patente inv�lido';    //SS_1227_MCA_20141105
Const
    DATE_INFORMATION = 'Fecha ultima modificaci�n';
    DATE = 'Fecha: ';
    VEHICLE_INFORMATION = 'Informaci�n referida al autom�vil';
    LICENSEPLATE = 'Patente: ';
    CATEGORY = 'Categoria: ';
    MANUFACTURER = 'Fabricante: ';
    MODEL = 'Modelo: ';
    COLOR = 'Color: ';
    ENGINEID = 'Id. Motor: ';
    CHASSISID = 'Id. Chassis: ';
    YEAR = 'A�o: ';
    OWNER_INFORMATION = 'Informaci�n referida al propietario del autom�vil';
    OWNERID1 = 'Id. Propietario 1: ';
    OWNERNAME1 = 'Nombre Propietario 1: ';
    REGISTERED_ADDRESS = 'Domicilio registrado';
    STREET = 'Domicilio: ';
    ACQUISITION = 'Fecha de Adquisici�n del autom�vil';                         //SS_1143_MCA_20131111
    ACQUISITIONDATE = 'Fecha Adquisici�n: ';                                    //SS_1143_MCA_20131111
begin
    Memo.Lines.Clear;
    lblFechaConsulta.Caption := EmptyStr;                                       //SS_1143_MCA_20131111
    if txtpatente.Text = '' then Exit;
    if Length(txtPatente.Text) <> 6 then begin                                  //SS_1227_MCA_20141105
        MsgBoxBalloon(MSG_ERROR_PATENTE, Caption, MB_ICONERROR, txtPatente);    //SS_1227_MCA_20141105
        Exit;                                                                   //SS_1227_MCA_20141105
    end;                                                                        //SS_1227_MCA_20141105
    try
        try
            with spObtenerUltimaConsultaRNVM, Parameters do begin

                Close;
                Parambyname('@Patente').Value := txtpatente.text;
                CommandTimeOut := 500;
                Open;
                //Si encontro datos para la patente
                if RecordCount > 0 then begin
                    Screen.Cursor := crHourGlass;
                    wbXML.SendToBack;                                           //SS_1143_MCA_20131111
                    memo.BringToFront;                                          //SS_1143_MCA_20131111
					wbXML.Navigate('about:blank');                              //SS_1143_MCA_20131111
                    // Procesar la respuesta
                    Memo.Lines.Add(DATE_INFORMATION);
                    Memo.Lines.Add('');
                    Memo.Lines.Add(DATE + Fieldbyname('UltimaFechaModificacion').asstring);
                    Memo.Lines.Add('');
                    Memo.Lines.Add(VEHICLE_INFORMATION);
                    Memo.Lines.Add('');
                    Memo.Lines.Add(LICENSEPLATE + Fieldbyname('LicensePlate').asstring);
                    Memo.Lines.Add(CATEGORY + Fieldbyname('Category').asstring);
                    Memo.Lines.Add(MANUFACTURER + Fieldbyname('Manufacturer').asstring);
                    Memo.Lines.Add(MODEL + Fieldbyname('Model').asstring);
                    Memo.Lines.Add(COLOR + Fieldbyname('Color').asstring);
                    Memo.Lines.Add(ENGINEID + Fieldbyname('EngineID').asstring);
                    Memo.Lines.Add(CHASSISID + Fieldbyname('ChasisID').asstring);
                    Memo.Lines.Add(YEAR + Fieldbyname('Dyear').asstring);
                    Memo.Lines.Add('');
                    Memo.Lines.Add(OWNER_INFORMATION);
                    Memo.Lines.Add('');
                    Memo.Lines.Add(OWNERID1 + Fieldbyname('OwnerID1').asstring);
                    Memo.Lines.Add(OWNERNAME1 + Fieldbyname('OwnerName1').asstring);
                    Memo.Lines.Add('');
                    Memo.Lines.Add(ACQUISITION);                                                 //SS_1143_MCA_20131111
                    Memo.Lines.Add(ACQUISITIONDATE + Fieldbyname('FechaAdquisicion').asstring);  //SS_1143_MCA_20131111
                    Memo.Lines.Add('');                                                          //SS_1143_MCA_20131111
                    Memo.Lines.Add(REGISTERED_ADDRESS);
                    Memo.Lines.Add('');
                    Memo.Lines.Add(STREET + Fieldbyname('Domicilio').asstring);
                    Memo.SelStart := 0;
                    Memo.SetFocus;
                end else begin
                    Msgbox(MSG_ERROR_EMPTY);
                end;

                Close;

            end;
        except
            on E : Exception do begin
                MsgBoxErr(MSG_ERROR, E.Message ,MSG_ERROR_IN_QUERY, MB_ICONERROR);
            end;
        end;
    finally
        Screen.Cursor := crDefault;
    end;
end;

{******************************** Function Header ******************************
Function Name: btnAbrirXMLClick
Author : mcabello
Date Created : 11/11/2013
Description : Permite abrir el xml almacenado en el disco
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFRvm.btnAbrirXMLClick(Sender: TObject);                                          //SS_1143_MCA_20131111
resourcestring                                                                              //SS_1143_MCA_20131111
    MSG_ERROR_IN_QUERY = 'Error en la consulta';                                            //SS_1143_MCA_20131111
    MSG_ERROR_EMPTY = 'No hay Informaci�n para la patente ingresada!';                      //SS_1143_MCA_20131111
    MSG_ERROR = 'Error';                                                                    //SS_1143_MCA_20131111
    MSG_ERROR_NO_EXISTE_XML = 'No existe archivo XML para la patente ingresada';            //SS_1143_MCA_20131111
    MSG_ERROR_PATENTE = 'No se puede abrir el XML, Formato de Patente inv�lido';            //SS_1227_MCA_20141105
var                                                                                         //SS_1143_MCA_20131111
    Archivo:String;                                                                         //SS_1143_MCA_20131111
    fechaConsulta: TDateTime;                                                               //SS_1143_MCA_20131111
    sr: TSearchRec;                                                                         //SS_1143_MCA_20131111
begin                                                                                       //SS_1143_MCA_20131111
	fechaConsulta := NullDate;                                                              //SS_1143_MCA_20131111
	Memo.Lines.Clear;                                                                       //SS_1143_MCA_20131111
    lblFechaConsulta.Caption := EmptyStr;                                                   //SS_1143_MCA_20131111

    if txtpatente.Text = '' then Exit;                                                      //SS_1143_MCA_20131111
    if Length(txtPatente.Text) <> 6 then begin                                              //SS_1227_MCA_20141105
        MsgBoxBalloon(MSG_ERROR_PATENTE, Caption, MB_ICONERROR, txtPatente);                //SS_1227_MCA_20141105
        Exit;                                                                               //SS_1227_MCA_20141105
    end;                                                                                    //SS_1227_MCA_20141105

    try                                                                                     //SS_1143_MCA_20131111
        try                                                                                 //SS_1143_MCA_20131111
            with spObtenerUltimaConsultaRNVM, Parameters do begin                           //SS_1143_MCA_20131111

                Close;                                                                      //SS_1143_MCA_20131111
                Parambyname('@Patente').Value := txtpatente.text;                           //SS_1143_MCA_20131111
                CommandTimeOut := 500;                                                      //SS_1143_MCA_20131111
                Open;                                                                       //SS_1143_MCA_20131111
                //Si encontro datos para la patente
                if RecordCount > 0 then begin                                               //SS_1143_MCA_20131111
                    Screen.Cursor := crHourGlass;                                           //SS_1143_MCA_20131111
                    // Procesar la respuesta                                                //SS_1143_MCA_20131111
                    fechaConsulta := FieldByName('UltimaFechaModificacion').AsDateTime;     //SS_1143_MCA_20131111
                end else begin                                                              //SS_1143_MCA_20131111
                    Msgbox(MSG_ERROR_EMPTY);                                                //SS_1143_MCA_20131111
                    wbXML.Align := alClient;                                                //SS_1143_MCA_20131111
                    wbXML.BringToFront;                                                     //SS_1143_MCA_20131111
	                memo.SendToBack;                                                        //SS_1143_MCA_20131111
                    wbXML.Navigate('about:blank');                                          //SS_1143_MCA_20131111
                    Exit;                                                                   //SS_1143_MCA_20131111
                end;                                                                        //SS_1143_MCA_20131111
                                                                                            //SS_1143_MCA_20131111
                Close;                                                                      //SS_1143_MCA_20131111

            end;                                                                            //SS_1143_MCA_20131111
        except                                                                              //SS_1143_MCA_20131111
            on E : Exception do begin                                                       //SS_1143_MCA_20131111
                MsgBoxErr(MSG_ERROR, E.Message ,MSG_ERROR_IN_QUERY, MB_ICONERROR);          //SS_1143_MCA_20131111
            end;                                                                            //SS_1143_MCA_20131111
        end;                                                                                //SS_1143_MCA_20131111

        if fechaConsulta <> NullDate then begin                                             //SS_1143_MCA_20131111

            Archivo:= FPathXML + FormatDateTime('yyyymm', fechaConsulta) + '\' + TRIM(txtPatente.Text) + '*' + FILE_EXTENSION;    //SS_1143_MCA_20131111
            if FindFirst(Archivo, integer(faReadOnly), sr) = 0 then begin                   //SS_1143_MCA_20131111
            	wbXML.BringToFront;                                                         //SS_1143_MCA_20131111
                memo.SendToBack;                                                            //SS_1143_MCA_20131111
	            wbXML.Align := alClient;                                                    //SS_1143_MCA_20131111
            	wbXML.Navigate(FPathXML + FormatDateTime('yyyymm', fechaConsulta) + '\' + sr.Name);		//SS_1143_MCA_20131111
                lblFechaConsulta.Caption := 'Fecha Consulta : ' + DateToStr(fechaConsulta); //SS_1143_MCA_20131111
            end                                                                             //SS_1143_MCA_20131111
            else begin                                                                      //SS_1143_MCA_20131111
               	MsgBox(MSG_ERROR_NO_EXISTE_XML);                                            //SS_1143_MCA_20131111
                wbXML.Align := alClient;                                                    //SS_1143_MCA_20131111
                wbXML.BringToFront;                                                         //SS_1143_MCA_20131111
                memo.SendToBack;                                                            //SS_1143_MCA_20131111
                wbXML.Navigate('about:blank');                                              //SS_1143_MCA_20131111
            end;                                                                            //SS_1143_MCA_20131111
        end                                                                                 //SS_1143_MCA_20131111
        else begin                                                                          //SS_1143_MCA_20131111
        	MsgBox(MSG_ERROR_EMPTY);                                                        //SS_1143_MCA_20131111
            wbXML.Align := alClient;                                                        //SS_1143_MCA_20131111
            wbXML.BringToFront;                                                             //SS_1143_MCA_20131111
            memo.SendToBack;                                                                //SS_1143_MCA_20131111
            wbXML.Navigate('about:blank');                                                  //SS_1143_MCA_20131111
        end;                                                                                //SS_1143_MCA_20131111
    finally                                                                                	//SS_1143_MCA_20131111
        Screen.Cursor := crDefault;                                                         //SS_1143_MCA_20131111
    end;                                                                                    //SS_1143_MCA_20131111
end;                                                                                        //SS_1143_MCA_20131111

{******************************** Function Header ******************************
Function Name: btnCerrarClick
Author : lgisuk
Date Created : 15/07/2005
Description : Permito cerrar el form
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFRvm.btnCerrarClick(Sender: TObject);
begin
    Close;
end;

{******************************** Function Header ******************************
Function Name: FormClose
Author : lgisuk
Date Created : 15/07/2005
Description :  lo libero de memoria
Parameters : Sender: TObject; var Action: TCloseAction
Return Value : None
*******************************************************************************}
procedure TFRvm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action := caFree;
end;



{******************************** Function Header ******************************
Function Name:
Author : ggomez
Date Created : 21/02/2006
Description : Dada una patente, retorna el c�digo de la m�xima infracci�n para
    ella. Si no encuentra una infracci�n retorna, 0 (cero).
Parameters : Not available
Return Value : Not available
*******************************************************************************}
function TFRvm.ObtenerMaximoCodigoInfraccionDePatente(
  Patente: AnsiString): Integer;
var
    sp: TADOStoredProc;
begin
    sp := TADOStoredProc.Create(Self);
    try
        sp.Connection       := DMConnections.BaseCAC;
        sp.ProcedureName    := 'ObtenerMaximoCodigoInfraccionDePatente';
        sp.CommandTimeout   := 30;
        sp.Parameters.Refresh;
        sp.Parameters.ParamByName('@Patente').Value             := Patente;
        sp.Parameters.ParamByName('@CodigoInfraccion').Value    := Null;
        sp.ExecProc;
        Result := iif(sp.Parameters.ParamByName('@CodigoInfraccion').Value = Null,
                        0, sp.Parameters.ParamByName('@CodigoInfraccion').Value);

    finally
        sp.Free;
    end; // finally
end;

procedure TFRvm.txtPatenteChange(Sender: TObject);                              //SS_1143_MCA_2013111
begin                                                                           //SS_1143_MCA_2013111
	Memo.Lines.Clear;                                                           //SS_1143_MCA_2013111
end;                                                                            //SS_1143_MCA_2013111

procedure TFRvm.ImgAyudaClick(Sender: TObject);
resourcestring
    MSG_DESCR_FUNCION   = ' ' + CRLF +
          '- Si no selecciona "Actualizar Datos de Infractores al Consultar",' + CRLF +
          'se har� la consulta al RNVM y los datos de la respuesta a la consulta no' + CRLF +
          'se almacenar�n los datos en la Base de Datos.' + CRLF +
          '- Si selecciona "Actualizar Datos de Infractores al Consultar",' + CRLF +
          'los datos de la respuesta a la consulta ser�n almacenados en la Base de Datos.' + CRLF +
          '- Si adem�s de seleccionar "Actualizar Datos de Infractores al Consultar",' + CRLF +
          'selecciona "Forzar Consulta al RNVM", se har� la consulta al RNVM y los datos' + CRLF +
          'de la respuesta a la consulta se almacenar�n en la Base de Datos.' + CRLF +
          '- Si selecciona "Actualizar Datos de Infractores al Consultar" y no,' + CRLF +
          'selecciona "Forzar Consulta al RNVM", se har� la consulta al RNVM (si los datos' + CRLF +
          'en la Base de Datos no son recientes ni la cantidad de consultas realizadas' + CRLF +
          'supere la cantidad m�xima), y los datos de la respuesta a la consulta no se' + CRLF +
          'almacenar�n en la Base de Datos.' + CRLF +
          ' ';
begin
    //Muestro el mensaje
    MsgBoxBalloon(MSG_DESCR_FUNCION, Caption, MB_ICONQUESTION, IMGAYUDA);
end;

end.
