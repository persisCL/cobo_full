object frmAccionesSerbanc: TfrmAccionesSerbanc
  Left = 353
  Top = 152
  Caption = 'Recepci'#243'n de Rendici'#243'n de Acciones de Cobranzas(Serbanc)'
  ClientHeight = 461
  ClientWidth = 892
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poOwnerFormCenter
  Visible = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  PixelsPerInch = 96
  TextHeight = 13
  object pnlAbajo: TPanel
    Left = 0
    Top = 378
    Width = 892
    Height = 83
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      892
      83)
    object PnlAvance: TPanel
      Left = 0
      Top = 0
      Width = 473
      Height = 83
      Align = alLeft
      BevelOuter = bvNone
      Caption = 'Presione [Esc] para cancelar'
      TabOrder = 3
      Visible = False
      DesignSize = (
        473
        83)
      object lblProcesoGeneral: TLabel
        Left = 17
        Top = 54
        Width = 82
        Height = 13
        Anchors = [akLeft, akBottom]
        Caption = 'Progreso general'
        Visible = False
      end
      object pbProgreso: TProgressBar
        Left = 17
        Top = 12
        Width = 428
        Height = 17
        Anchors = [akLeft, akRight, akBottom]
        Smooth = True
        Step = 1
        TabOrder = 0
      end
    end
    object btnProcesar: TButton
      Left = 651
      Top = 45
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = '&Procesar'
      Enabled = False
      TabOrder = 0
      OnClick = btnProcesarClick
    end
    object btnCancelar: TButton
      Left = 732
      Top = 45
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = '&Cancelar'
      Enabled = False
      TabOrder = 1
      OnClick = btnCancelarClick
    end
    object btnSalir: TButton
      Left = 810
      Top = 45
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = '&Salir'
      TabOrder = 2
      OnClick = btnSalirClick
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 81
    Width = 892
    Height = 297
    ActivePage = tsValidas
    Align = alClient
    TabOrder = 1
    object tsValidas: TTabSheet
      Caption = 'Rendiciones Rechazadas'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object dblValidas: TDBListEx
        Left = 2
        Top = 0
        Width = 880
        Height = 290
        Align = alCustom
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'Convenio'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'NumeroConvenio'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'Acci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'ActividadCobranza'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'Fecha'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'FechaAccion'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 500
            Header.Caption = 'Motivo de rechazo'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Motivo'
          end>
        DataSource = dsRechazos
        DragReorder = True
        ParentColor = False
        TabOrder = 0
        TabStop = True
      end
    end
    object tsErrores: TTabSheet
      Caption = 'Errores de formato'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object lblErrores: TListBox
        Left = 0
        Top = 0
        Width = 884
        Height = 269
        Align = alClient
        ItemHeight = 13
        TabOrder = 0
      end
    end
  end
  object cpPanelTop: TCollapsablePanel
    Left = 0
    Top = 0
    Width = 892
    Height = 81
    Align = alTop
    Caption = 'Archivos'
    BarColorStart = 14071199
    BarColorEnd = 10646097
    RightMargin = 0
    LeftMargin = 0
    ArrowLocation = cpaRightTop
    InternalSize = 58
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWhite
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    object btnBuscarArchivo: TSpeedButton
      Left = 736
      Top = 44
      Width = 23
      Height = 22
      Caption = '...'
      OnClick = btnBuscarArchivoClick
    end
    object lblOrigen: TLabel
      Left = 6
      Top = 47
      Width = 336
      Height = 13
      Caption = 'Seleccione el archivo de acciones de cobranza a registrar:'
      FocusControl = txtOrigen
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object txtOrigen: TEdit
      Left = 348
      Top = 44
      Width = 382
      Height = 21
      ReadOnly = True
      TabOrder = 1
    end
  end
  object OpenDialog: TOpenDialog
    Filter = 'Rendiciones|001REND*.GEN'
    Left = 279
    Top = 364
  end
  object spInsertarActividadCobranzaSerbanc: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'InsertarActividadCobranzaSerbanc'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoOperacionInterfase'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoCobranza'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoActividadCobranza'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaAccion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoEmpresaCobranza'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Observaciones'
        Attributes = [paNullable]
        DataType = ftString
        Size = 200
        Value = Null
      end>
    Left = 24
    Top = 224
  end
  object spRechazarAccion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'InsertarRechazoAccionCobranzaSerbanc;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoOperacionInterfase'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoActividadCobranza'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@FechaAccion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Motivo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end>
    Left = 56
    Top = 224
  end
  object cdsArchivo: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoConvenio'
        DataType = ftInteger
      end
      item
        Name = 'FechaAccion'
        DataType = ftDateTime
      end
      item
        Name = 'CodigoActividadCobranza'
        DataType = ftSmallint
      end
      item
        Name = 'CodigoTipoCobranza'
        DataType = ftSmallint
      end
      item
        Name = 'codigoTipoDeudor'
        DataType = ftSmallint
      end
      item
        Name = 'Observaciones'
        DataType = ftString
        Size = 200
      end
      item
        Name = 'Procesado'
        DataType = ftBoolean
      end>
    IndexDefs = <
      item
        Name = 'ixConvenioFechaAccion'
        Fields = 
          'codigoConvenio;FechaAccion;CodigoActividadCobranza;CodigoTipoCob' +
          'ranza'
      end>
    IndexName = 'ixConvenioFechaAccion'
    Params = <>
    StoreDefs = True
    Left = 32
    Top = 272
  end
  object cdsRechazos: TClientDataSet
    Aggregates = <
      item
        Visible = False
      end>
    FieldDefs = <
      item
        Name = 'NumeroConvenio'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'FechaAccion'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CodigoActividadCobranza'
        DataType = ftSmallint
      end
      item
        Name = 'Motivo'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'ActividadCobranza'
        DataType = ftString
        Size = 50
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 32
    Top = 304
  end
  object dsRechazos: TDataSource
    DataSet = cdsRechazos
    Left = 64
    Top = 304
  end
  object cdsNombreAcciones: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoAccion'
        DataType = ftSmallint
      end
      item
        Name = 'NombreAccion'
        DataType = ftString
        Size = 50
      end>
    IndexDefs = <
      item
        Name = 'IX_CodigoAccion'
        Fields = 'CodigoAccion'
      end>
    IndexName = 'IX_CodigoAccion'
    Params = <>
    StoreDefs = True
    Left = 24
    Top = 168
  end
  object cdsErrores: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoError'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'DescripcionError'
        DataType = ftString
        Size = 50
      end>
    IndexDefs = <
      item
        Name = 'IX_CodigoError'
        Fields = 'CodigoError'
      end>
    IndexName = 'IX_CodigoError'
    Params = <>
    StoreDefs = True
    Left = 24
    Top = 120
  end
  object ObtenerErroresAcciones: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'obtenererroresrechazosInterfazModulo'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoModulo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 64
    Top = 120
  end
  object cdsTiposDeudores: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoTipoDeudor'
        DataType = ftSmallint
      end>
    IndexDefs = <>
    IndexFieldNames = 'CodigoTipoDeudor'
    Params = <>
    StoreDefs = True
    Left = 64
    Top = 168
  end
  object cdsTiposCobranza: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoTipoCobranza'
        DataType = ftSmallint
      end>
    IndexDefs = <>
    IndexFieldNames = 'CodigoTipoCobranza'
    Params = <>
    StoreDefs = True
    Left = 112
    Top = 168
  end
end
