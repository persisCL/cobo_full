object frmConsultaBHTU: TfrmConsultaBHTU
  Left = 191
  Top = 153
  Width = 783
  Height = 354
  Caption = 'frmConsultaBHTU'
  Color = clBtnFace
  Constraints.MinHeight = 220
  Constraints.MinWidth = 700
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poOwnerFormCenter
  Visible = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  PixelsPerInch = 96
  TextHeight = 13
  object pnlFiltros: TPanel
    Left = 0
    Top = 0
    Width = 775
    Height = 49
    Align = alTop
    TabOrder = 0
    DesignSize = (
      775
      49)
    object Label1: TLabel
      Left = 8
      Top = 19
      Width = 79
      Height = 13
      Caption = 'Fecha de Venta:'
    end
    object Label2: TLabel
      Left = 200
      Top = 19
      Width = 42
      Height = 13
      Caption = 'Patente:'
    end
    object Label3: TLabel
      Left = 456
      Top = 19
      Width = 99
      Height = 13
      Anchors = [akTop, akRight]
      Caption = 'Cantidad  a mostrar:'
    end
    object btnConsultar: TButton
      Left = 690
      Top = 13
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Aplicar Filtro'
      TabOrder = 4
      OnClick = btnConsultarClick
    end
    object neCantidad: TNumericEdit
      Left = 559
      Top = 15
      Width = 46
      Height = 21
      Anchors = [akTop, akRight]
      TabOrder = 3
      Decimals = 0
    end
    object deFechaVenta: TDateEdit
      Left = 96
      Top = 15
      Width = 90
      Height = 21
      AutoSelect = False
      TabOrder = 0
      Date = -693594.000000000000000000
    end
    object txtPatente: TEdit
      Left = 248
      Top = 15
      Width = 65
      Height = 21
      MaxLength = 6
      TabOrder = 1
      Text = 'txtPatente'
    end
    object chkAsignados: TCheckBox
      Left = 328
      Top = 17
      Width = 97
      Height = 17
      Caption = 'Solo Asignados'
      TabOrder = 2
    end
    object btnLimpiar: TButton
      Left = 616
      Top = 13
      Width = 75
      Height = 25
      Hint = 'Limpiar Filtros'
      Anchors = [akTop, akRight]
      Caption = 'Limpiar Filtros'
      ParentShowHint = False
      ShowHint = False
      TabOrder = 5
      OnClick = btnLimpiarClick
    end
  end
  object pnlBotones: TPanel
    Left = 0
    Top = 271
    Width = 775
    Height = 49
    Align = alBottom
    TabOrder = 2
    DesignSize = (
      775
      49)
    object lblCantidad: TLabel
      Left = 8
      Top = 8
      Width = 18
      Height = 13
      Caption = 'XXX'
    end
    object btnSalir: TButton
      Left = 694
      Top = 14
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = '&Salir'
      TabOrder = 1
      OnClick = btnSalirClick
    end
    object btnEditar: TButton
      Left = 617
      Top = 14
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '&Editar'
      Default = True
      Enabled = False
      TabOrder = 0
      OnClick = btnEditarClick
    end
  end
  object dblBHTU: TDBListEx
    Left = 0
    Top = 49
    Width = 775
    Height = 222
    Align = alClient
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 100
        Header.Caption = 'Patente'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        OnHeaderClick = dblBHTUColumns0HeaderClick
        FieldName = 'Patente'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 35
        Header.Caption = 'Cat.'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'Categoria'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 120
        Header.Caption = 'Fecha de Venta'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = dblBHTUColumns2HeaderClick
        FieldName = 'FechaVenta'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 120
        Header.Caption = 'Fecha de Uso'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'FechaUso'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 250
        Header.Caption = 'Identificador'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Identificador'
      end>
    DataSource = dsListado
    DragReorder = True
    ParentColor = False
    TabOrder = 1
    TabStop = True
    OnDblClick = dblBHTUDblClick
  end
  object spObtenerListadoConsultaBHTU: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerListadoConsultaBHTU'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Cantidad'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaVenta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@Usados'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@OrdenFEcha'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = False
      end
      item
        Name = '@Total'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = 0
      end>
    Left = 24
    Top = 88
  end
  object dsListado: TDataSource
    DataSet = spObtenerListadoConsultaBHTU
    Left = 56
    Top = 88
  end
end
