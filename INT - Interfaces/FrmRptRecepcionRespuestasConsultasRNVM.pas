{-----------------------------------------------------------------------------
 File Name: FrmRptRecepcionRespuestasConsultasRNVM.pas
 Author   : Nelson Droguett
 Date Created: 07/10/2015
 Description: Reporte para el modulo de la interfaz Respuestas a Consultas RNVM

Firma       : SS_1400_NDR_20151007
Descripcion : Se crea la interfaz entrante de respuestas a consultas RNVM
------------------------------------------------------------------------------}
unit FrmRptRecepcionRespuestasConsultasRNVM;

interface

uses
  //Reporte
  DMConnection,              //Coneccion a base de datos OP_CAC
  Util,                      //Stringtofile,padl..
  UtilProc,                  //Mensajes
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UtilRB, ppCtrls, ppBands, ppPrnabl, ppClass, ppCache, ppProd,
  ppReport, ppDB, ppComm, ppRelatv, ppTxPipe, DB, ppDBPipe, ADODB,
  ppParameter, ppModule, raCodMod, ppStrtch, ppSubRpt,RBSetup, ppVar,
  strUtils;

type
  TFRptRecepcionRespuestasConsultasRNVM = class(TForm)
    ppReporte: TppReport;
    ppParameterList1: TppParameterList;
    RBIListado: TRBInterface;
    SpObtenerResumenRespuestasRNVMRecibidas: TADOStoredProc;
    ppDBPipelineResumenComprobantesRecibidos: TppDBPipeline;
    DataSource: TDataSource;
    spObtenerErroresInterfase: TADOStoredProc;
    dsObtenerErroresInterfase: TDataSource;
    ppDBPipelineErroresInterfase: TppDBPipeline;
    ppHeaderBand1: TppHeaderBand;
    ppImage2: TppImage;
    ppLtitulo: TppLabel;
    ppModulo: TppLabel;
    ppLine19: TppLine;
    ppDetailBand1: TppDetailBand;
    ppSummaryBand5: TppSummaryBand;
    raCodeModule3: TraCodeModule;
    ppLabel3: TppLabel;
    ppLine5: TppLine;
    pptNombreArchivo: TppLabel;
    pptUsuario: TppLabel;
    pptLineas: TppLabel;
    ppNombreArchivo: TppLabel;
    ppUsuario: TppLabel;
    ppLineas: TppLabel;
    pptFechaProcesamiento: TppLabel;
    ppLabel5: TppLabel;
    ppNumeroProceso: TppLabel;
    ppFechaProcesamiento: TppLabel;
    ppLabel15: TppLabel;
    ppCantidadRespuestasRechazadas: TppLabel;
    ppCantidadRespuestasAceptadas: TppLabel;
    ppCantidadRespuestasRecibidas: TppLabel;
    ppLabel12: TppLabel;
    ppLabel13: TppLabel;
    ppLabel14: TppLabel;
    ppLine12: TppLine;
    ppLine8: TppLine;
    ppSubReport1: TppSubReport;
    ppChildReport4: TppChildReport;
    ppHeaderBand2: TppHeaderBand;
    ppImage1: TppImage;
    ppLabel21: TppLabel;
    ppModulo5: TppLabel;
    ppLine24: TppLine;
    ppLine21: TppLine;
    ppLabel19: TppLabel;
    ppLabel17: TppLabel;
    ppLine22: TppLine;
    ppDetailBand5: TppDetailBand;
    ppDBMotivo: TppDBText;
    ppSummaryBand4: TppSummaryBand;
    ppLabel18: TppLabel;
    ppDBCalc1: TppDBCalc;
    ppLabel38: TppLabel;
    ppSystemVariable1: TppSystemVariable;
    ppLabel39: TppLabel;
    ppSystemVariable10: TppSystemVariable;
    ppLabel36: TppLabel;
    ppSystemVariable8: TppSystemVariable;
    ppLabel37: TppLabel;
    ppSystemVariable9: TppSystemVariable;
    SpObtenerResumenRespuestasRNVMRecibidasDescripcionError: TStringField;
    ppLabel1: TppLabel;
    procedure RBIListadoExecute(Sender: TObject; var Cancelled: Boolean);
  private
    FNombreReporte:string;
    FCodigoOperacionInterfase:integer;
    FLineas: integer;
    FMontoArchivo: int64;
    { Private declarations }
  public
    Function Inicializar(NombreReporte: AnsiString; CodigoOperacionInterfase:integer): Boolean;
    { Public declarations }
  end;

var
  FRptRecepcionRespuestasConsultasRNVM: TFRptRecepcionRespuestasConsultasRNVM;

implementation

{$R *.dfm}

const
    CONST_SIN_DATOS = 'Sin datos.';







function TFRptRecepcionRespuestasConsultasRNVM.Inicializar(NombreReporte: AnsiString; CodigoOperacionInterfase:integer): Boolean;
begin
    FNombreReporte:= NombreReporte;
    FCodigoOperacionInterfase:= CodigoOperacionInterfase;
    RBIListado.Execute;
    Result := True;
end;







procedure TFRptRecepcionRespuestasConsultasRNVM.RBIListadoExecute(Sender: TObject;var Cancelled: Boolean);
resourcestring
	  MSG_ERROR_TO_GENERATE_REPORT = 'Error Al Generar Reporte';
    MSG_ERROR = 'Error';
var
    Config: TRBConfig;
begin
    //configuración del reporte
    Config := rbiListado.GetConfig;
    if Config.BinName <> '' then rbiListado.Report.PrinterSetup.BinName := Config.BinName;
    try

        //Abro la consulta de Comprobantes Pagos
        with SpObtenerResumenRespuestasRNVMRecibidas do begin

            Close;
            Parameters.Refresh;
            Parameters.ParamByName('@CodigoOperacionInterfase').Value     := fcodigooperacioninterfase;
            Parameters.ParamByName('@FechaProcesamiento').Value           := NULL;
            Parameters.ParamByName('@NombreArchivo').Value                := NULL;
            Parameters.ParamByName('@Modulo').Value                       := NULL;
            Parameters.ParamByName('@Usuario').Value                      := NULL;
            Parameters.ParamByName('@CantidadRespuestasRecibidas').Value  := NULL;
            Parameters.ParamByName('@CantidadRespuestasAceptadas').Value  := NULL;
            Parameters.ParamByName('@CantidadRespuestasRechazadas').Value := NULL;
            Parameters.ParamByName('@LineasArchivo').Value := NULL;
            CommandTimeOut := 500;
            Open;

            //Asigno los valores a los campos del reporte
            ppmodulo.Caption:= Parameters.ParamByName('@Modulo').Value;
            ppmodulo5.Caption:= ppmodulo.Caption; // SS_904_20100824

            ppFechaProcesamiento.Caption:= DateToStr(Parameters.ParamByName('@FechaProcesamiento').Value);
            ppNombreArchivo.Caption:= Copy(Parameters.ParamByName('@NombreArchivo').Value,1,40);
            ppUsuario.Caption:= Parameters.ParamByName('@Usuario').Value;
            ppNumeroProceso.Caption := IntToStr(fcodigooperacioninterfase);
            ppLineas.Caption := iif(Parameters.ParamByName('@LineasArchivo').Value > 0, IntToStr(Parameters.ParamByName('@LineasArchivo').Value), CONST_SIN_DATOS);

            ppCantidadRespuestasRecibidas.caption:= IntToStr(Parameters.ParamByName('@CantidadRespuestasRecibidas').Value);
            ppCantidadRespuestasAceptadas.caption:= IntToStr(Parameters.ParamByName('@CantidadRespuestasAceptadas').Value);
            ppCantidadRespuestasRechazadas.caption:= IntToStr(Parameters.ParamByName('@CantidadRespuestasRechazadas').Value);
            //Asigno los valores a los campos del reporte
        end;

        //Rev 1
        spObtenerErroresInterfase.Close;
        with spObtenerErroresInterfase do begin
        	Parameters.Refresh;
            Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacionInterfase;
            Open;
        end;
        

        //Configuro el Report Builder y Ejecuto el Reporte
        rbiListado.Caption := FNombreReporte;

    except
        on E : exception do begin
            MsgBoxErr(MSG_ERROR_TO_GENERATE_REPORT, E.Message, MSG_ERROR, MB_ICONERROR );
        end;
    end;
end;

end.



