{-------------------------------------------------------------------------------
 File Name: FrmRptEnvioComprobantes.pas
 Author:    Flamas, lgisuk
 Date Created: 09/03/2005
 Language: ES-AR
 Description: Gestion de Interfases - Reporte de finalizacion del proceso
              Informa la cantidad de comprobantes enviados , el importe total y
              otra informacion util para verificar si el proceso se realizo
              segun parametros normales.


Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.


-------------------------------------------------------------------------------}
unit FrmRptEnvioComprobantes;

interface

uses
  //Reporte
  DMConnection,              //Coneccion a base de datos OP_CAC
  Util,                      //Stringtofile,padl..
  UtilProc,                  //Mensajes
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UtilRB, ppCtrls, ppBands, ppPrnabl, ppClass, ppCache, ppProd,
  ppReport, ppDB, ppComm, ppRelatv, ppTxPipe, DB, ADODB, ppModule, raCodMod,
  ppParameter, ppDBPipe,RBSetup, ConstParametrosGenerales, jpeg;          //SS_1147_NDR_20140710

type
  TFRptEnvioComprobantes = class(TForm)
    ppReporte: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppImage2: TppImage;
    ppLtitulo: TppLabel;
    ppModulo: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppDBText1: TppDBText;
    RBIListado: TRBInterface;
    SpObtenerReporteEnvioComprobantes: TADOStoredProc;
    pptFechaProcesamiento: TppLabel;
    pptNombreArchivo: TppLabel;
    pptUsuario: TppLabel;
    pptCantidadComprobantes: TppLabel;
    pptImporteTotal: TppLabel;
    ppTipoComprobante: TppLabel;
    ppMinComprobante: TppLabel;
    ppMaxComprobante: TppLabel;
    ppMinFechaEmision: TppLabel;
    ppMaxFechaEmision: TppLabel;
    ppLabel1: TppLabel;
    ppLine1: TppLine;
    ppSummaryBand1: TppSummaryBand;
    ppLine2: TppLine;
    ppFechaProcesamiento: TppLabel;
    ppNombreArchivo: TppLabel;
    ppUsuario: TppLabel;
    ppCantidadComprobantes: TppLabel;
    ppImporteTotal: TppLabel;
    ppDBPipeline: TppDBPipeline;
    DataSource: TDataSource;
    ppParameterList1: TppParameterList;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppLine3: TppLine;
    procedure RBIListadoExecute(Sender: TObject; var Cancelled: Boolean);
  private
    FNombreReporte:string;
    FCodigoOperacionInterfase:integer;
    { Private declarations }
  public
    function Inicializar(NombreReporte: AnsiString; CodigoOperacionInterfase:integer): Boolean;
    { Public declarations }
  end;

var
  FRptEnvioComprobantes: TFRptEnvioComprobantes;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    lgisuk
  Date Created: 13/04/2005
  Description: Inicializo este formulario
  Parameters: NombreReporte: AnsiString
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFRptEnvioComprobantes.Inicializar(NombreReporte : AnsiString; CodigoOperacionInterfase : Integer) : Boolean;
var                                                                             //SS_1147_NDR_20140710
  RutaLogo: AnsiString;                                                         //SS_1147_NDR_20140710
begin
    ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
    try                                                                                                 //SS_1147_NDR_20140710
      ppImage2.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
    except                                                                                              //SS_1147_NDR_20140710
      On E: Exception do begin                                                                          //SS_1147_NDR_20140710
        Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
        MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
        Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
      end;                                                                                              //SS_1147_NDR_20140710
    end;                                                                                                //SS_1147_NDR_20140710

    FNombreReporte:= NombreReporte;
    FCodigoOperacionInterfase:= CodigoOperacionInterfase;
    RBIListado.Execute;
    Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: RBIListadoExecute
  Author:    lgisuk
  Date Created: 13/04/2005
  Description: Ejecuto el informe
  Parameters: Sender: TObject;var Cancelled: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRptEnvioComprobantes.RBIListadoExecute(Sender : TObject;var Cancelled : Boolean);
resourcestring
  	MSG_ERROR_TO_GENERATE_REPORT = 'Error al generar reporte';
    MSG_ERROR = 'Error';
var
    Config : TRBConfig;
begin
    //Configuración del reporte
    Config := rbiListado.GetConfig;
    if Config.BinName <> '' then rbiListado.Report.PrinterSetup.BinName := Config.BinName;
    try
        //Abro la consulta
        with SpObtenerReporteEnvioComprobantes do begin
            Close;
            Parameters.Refresh;
            Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacionInterfase;
            Parameters.ParamByName('@Modulo').Value := NULL;
            Parameters.ParamByName('@FechaProcesamiento').Value := NULL;
            Parameters.ParamByName('@NombreArchivo').Value := NULL;
            Parameters.ParamByName('@Usuario').Value := NULL;
            Parameters.ParamByName('@CantidadComprobantes').Value := NULL;
            Parameters.ParamByName('@ImporteTotal').Value := NULL;
            CommandTimeOut := 10000;
    		    Open;

            //Asigno los valores a los campos del reporte
            ppmodulo.Caption:= Parameters.ParamByName('@Modulo').Value;
            ppFechaProcesamiento.Caption:= DateToStr(Parameters.ParamByName('@FechaProcesamiento').Value);
            ppNombreArchivo.Caption:= copy(Parameters.ParamByName('@NombreArchivo').Value,1,40);
            ppUsuario.Caption:= Parameters.ParamByName('@Usuario').Value;
            ppCantidadComprobantes.caption:= inttostr(Parameters.ParamByName('@CantidadComprobantes').Value);
            ppImporteTotal.Caption:= Parameters.ParamByName('@ImporteTotal').Value;
        end;

        //Configuro el Report Builder y Ejecuto el Reporte
        rbiListado.Caption := FNombreReporte;

    except
        on E : Exception do begin
            //Informo que no se pudo generar el reporte
            MsgBoxErr(MSG_ERROR_TO_GENERATE_REPORT, E.Message, MSG_ERROR, MB_ICONERROR );
        end;
    end;
end;

end.
