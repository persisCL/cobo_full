object FrmCancelarImpresionMasiva: TFrmCancelarImpresionMasiva
  Left = 333
  Top = 122
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Anular Proceso Masivo de Impresi'#243'n'
  ClientHeight = 113
  ClientWidth = 352
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poMainFormCenter
  Visible = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 8
    Top = 8
    Width = 337
    Height = 65
  end
  object lblFechaInterfase: TLabel
    Left = 18
    Top = 18
    Width = 243
    Height = 13
    Caption = 'Fecha del proceso comprobantes a anular:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object btnProcesar: TButton
    Left = 185
    Top = 82
    Width = 75
    Height = 25
    Caption = '&Procesar'
    Enabled = False
    TabOrder = 1
    OnClick = btnProcesarClick
  end
  object btnSalir: TButton
    Left = 269
    Top = 82
    Width = 75
    Height = 25
    Cancel = True
    Caption = '&Salir'
    TabOrder = 2
    OnClick = btnSalirClick
  end
  object edProceso: TBuscaTabEdit
    Left = 16
    Top = 37
    Width = 321
    Height = 21
    Enabled = True
    ReadOnly = True
    TabOrder = 0
    EditorStyle = bteTextEdit
    BuscaTabla = buscaOperaciones
  end
  object spObtenerOperacionesImpresionMasiva: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerOperacionesImpresionMasiva;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 18
    Top = 75
    object spObtenerOperacionesImpresionMasivaCodigoOperacionInterfase: TIntegerField
      FieldName = 'CodigoOperacionInterfase'
    end
    object spObtenerOperacionesImpresionMasivaFecha: TDateTimeField
      FieldName = 'Fecha'
    end
    object spObtenerOperacionesImpresionMasivaUsuario: TStringField
      FieldName = 'Usuario'
      Size = 50
    end
    object spObtenerOperacionesImpresionMasivaCantidad: TIntegerField
      FieldName = 'Cantidad'
      ReadOnly = True
    end
  end
  object spAnularProcesoImpresionMasivo: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'AnularProcesoImpresionMasivo'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoOperacionInterfase'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 50
    Top = 75
  end
  object buscaOperaciones: TBuscaTabla
    Caption = 'Procesos Masivos de Facturaci'#243'n'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HelpContext = 0
    Dataset = spObtenerOperacionesImpresionMasiva
    OnProcess = buscaOperacionesProcess
    OnSelect = buscaOperacionesSelect
    Left = 82
    Top = 75
  end
end
