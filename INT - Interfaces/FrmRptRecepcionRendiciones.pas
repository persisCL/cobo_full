{-------------------------------------------------------------------------------
 File Name: FrmRptRecepcionRendiciones.pas
 Author:    lgisuk
 Date Created: 31/03/2006
 Language: ES-AR
 Description: Gestion de Interfases - Reporte de finalizacion del proceso
              Informa la cantidad de comprobantes recibidos , el importe total y
              otra informacion util para verificar si el proceso se realizo
              segun parametros normales.

 Revision 1
 Author: mbecerra
 Date: 12-Enero-2009
 Description:	Se agrega el listado de errores desde ErroresInterfases.

 Revision 2
 Author: pdominguez
 Date: 24/08/2010
 Description: SS 904 - Informes Interfaces de Pago
    - Se modificaron los siguientes procedimientos / funciones:
        RBIListadoExecute



Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.

Firma       : SS_1147_MCA_20150420
descripcion : se reemplaza OPEN por EXECPROC para ejecutar el SP

Firma        : SS_1147_CQU_20150420
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.  (En todos los reportes que aplique)
-------------------------------------------------------------------------------}
unit FrmRptRecepcionRendiciones;

interface

uses
  //Reporte
  DMConnection,              //Coneccion a base de datos OP_CAC
  Util,                      //Stringtofile,padl..
  UtilProc,                  //Mensajes
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UtilRB, ppCtrls, ppBands, ppPrnabl, ppClass, ppCache, ppProd,
  ppReport, ppDB, ppComm, ppRelatv, ppTxPipe, DB, ppDBPipe, ADODB,
  ppParameter, ppModule, raCodMod, ppStrtch, ppSubRpt,RBSetup, ppVar,
  strUtils, ConstParametrosGenerales, jpeg;                                           //SS_1147_NDR_20140710

type
  TFRptRecepcionRendiciones = class(TForm)
    ppReporte: TppReport;
    ppParameterList1: TppParameterList;
    RBIListado: TRBInterface;
    SpObtenerResumenComprobantesRecibidos: TADOStoredProc;
    ppDBPipelineResumenComprobantesRecibidos: TppDBPipeline;
    DataSource: TDataSource;
    SpObtenerDetalleComprobantesAceptadosSinRecibo: TADOStoredProc;
    DataSource1: TDataSource;
    ppDBPipelineDetalleComprobantesAceptadosSinRecibo: TppDBPipeline;
    SpObtenerResumenComprobantesRecibidosMotivo: TStringField;
    SpObtenerResumenComprobantesRecibidosCantidad: TIntegerField;
    SpObtenerResumenComprobantesRecibidosMonto: TStringField;
    SPObtenerComprobantesRechazados: TADOStoredProc;
    DataSource2: TDataSource;
    ppDBPipelineObtenerComprobantesRechazados: TppDBPipeline;
    spObtenerErroresInterfase: TADOStoredProc;
    dsObtenerErroresInterfase: TDataSource;
    ppDBPipelineErroresInterfase: TppDBPipeline;
    ppHeaderBand1: TppHeaderBand;
    ppImage2: TppImage;
    ppLtitulo: TppLabel;
    ppModulo: TppLabel;
    ppLine19: TppLine;
    ppLabel38: TppLabel;
    ppSystemVariable1: TppSystemVariable;
    ppLabel39: TppLabel;
    ppSystemVariable10: TppSystemVariable;
    ppDetailBand1: TppDetailBand;
    ppLabel3: TppLabel;
    ppLine5: TppLine;
    pptNombreArchivo: TppLabel;
    pptUsuario: TppLabel;
    pptLineas: TppLabel;
    pptMontoArchivo: TppLabel;
    ppNombreArchivo: TppLabel;
    ppUsuario: TppLabel;
    ppLineas: TppLabel;
    ppMontoArchivo: TppLabel;
    pptFechaProcesamiento: TppLabel;
    ppLabel5: TppLabel;
    ppNumeroProceso: TppLabel;
    ppFechaProcesamiento: TppLabel;
    ppLabel15: TppLabel;
    ppLabel16: TppLabel;
    ppImporteTotalRecibido: TppLabel;
    ppImporteTotalAceptado: TppLabel;
    ppImporteTotalRechazados: TppLabel;
    ppCantidadComprobantesRechazados: TppLabel;
    ppCantidadComprobantesAceptados: TppLabel;
    ppCantidadComprobantesRecibidos: TppLabel;
    ppLabel12: TppLabel;
    ppLabel13: TppLabel;
    ppLabel14: TppLabel;
    ppLine12: TppLine;
    ppLine8: TppLine;
    ppSummaryBand5: TppSummaryBand;
    ppSubReport2: TppSubReport;
    ppChildReport5: TppChildReport;
    ppHeaderBand6: TppHeaderBand;
    ppImage5: TppImage;
    ppLine1: TppLine;
    ppLabel2: TppLabel;
    ppModulo2: TppLabel;
    ppLabel33: TppLabel;
    ppLine2: TppLine;
    ppLabel7: TppLabel;
    ppLabel8: TppLabel;
    ppLabel11: TppLabel;
    ppLine4: TppLine;
    ppLabel10: TppLabel;
    ppSystemVariable2: TppSystemVariable;
    ppLabel22: TppLabel;
    ppSystemVariable3: TppSystemVariable;
    ppDetailBand6: TppDetailBand;
    ppDBText8: TppDBText;
    ppDBText9: TppDBText;
    ppDBText10: TppDBText;
    ppSummaryBand6: TppSummaryBand;
    ppLine13: TppLine;
    ppLabel9: TppLabel;
    ppLine14: TppLine;
    ppCantidadRechazada: TppLabel;
    ppImporteTotalRechazado: TppLabel;
    raCodeModule1: TraCodeModule;
    ppSubReportDetalleComprobantesAceptadosSinRecibo: TppSubReport;
    ppChildReport1: TppChildReport;
    ppHeaderBand5: TppHeaderBand;
    ppImage4: TppImage;
    ppLabel23: TppLabel;
    ppModulo3: TppLabel;
    ppLine9: TppLine;
    ppLabel30: TppLabel;
    ppLine10: TppLine;
    ppLine11: TppLine;
    ppTipoComprobante: TppLabel;
    ppMinComprobante: TppLabel;
    ppLabel4: TppLabel;
    ppMaxComprobante: TppLabel;
    ppLabel1: TppLabel;
    ppLabel25: TppLabel;
    ppSystemVariable4: TppSystemVariable;
    ppLabel32: TppLabel;
    ppSystemVariable5: TppSystemVariable;
    ppDetailBand2: TppDetailBand;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppDBText1: TppDBText;
    ppDBText7: TppDBText;
    ppSummaryBand2: TppSummaryBand;
    ppImporteTotal: TppLabel;
    pptImporteTotal: TppLabel;
    ppCantidadComprobantes: TppLabel;
    ppLine15: TppLine;
    ppLine16: TppLine;
    raCodeModule2: TraCodeModule;
    ppSubReportDetalleComprobantesRechazados: TppSubReport;
    ppChildReport3: TppChildReport;
    ppHeaderBand4: TppHeaderBand;
    ppImage3: TppImage;
    ppLabel20: TppLabel;
    ppModulo4: TppLabel;
    ppLine20: TppLine;
    ppLabel24: TppLabel;
    ppLine25: TppLine;
    ppLine26: TppLine;
    ppLabel26: TppLabel;
    ppLabel27: TppLabel;
    ppLabel6: TppLabel;
    ppLabel28: TppLabel;
    ppLabel29: TppLabel;
    ppLabel34: TppLabel;
    ppSystemVariable6: TppSystemVariable;
    ppLabel35: TppLabel;
    ppSystemVariable7: TppSystemVariable;
    ppDetailBand4: TppDetailBand;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppDBText11: TppDBText;
    ppDBText12: TppDBText;
    ppDBText13: TppDBText;
    ppSummaryBand1: TppSummaryBand;
    ppImporteRechazados: TppLabel;
    ppLabel31: TppLabel;
    ppCantidadRechazados: TppLabel;
    ppLine17: TppLine;
    ppLine18: TppLine;
    raCodeModule4: TraCodeModule;
    ppSubReport1: TppSubReport;
    ppChildReport4: TppChildReport;
    ppHeaderBand2: TppHeaderBand;
    ppImage1: TppImage;
    ppLabel21: TppLabel;
    ppModulo5: TppLabel;
    ppLine24: TppLine;
    ppLine21: TppLine;
    ppLabel19: TppLabel;
    ppLabel17: TppLabel;
    ppLine22: TppLine;
    ppLabel36: TppLabel;
    ppSystemVariable8: TppSystemVariable;
    ppLabel37: TppLabel;
    ppSystemVariable9: TppSystemVariable;
    ppDetailBand5: TppDetailBand;
    ppDBMotivo: TppDBText;
    ppSummaryBand4: TppSummaryBand;
    ppLabel18: TppLabel;
    ppDBCalc1: TppDBCalc;
    raCodeModule3: TraCodeModule;
    procedure RBIListadoExecute(Sender: TObject; var Cancelled: Boolean);
  private
    FNombreReporte:string;
    FCodigoOperacionInterfase:integer;
    FLineas: integer;
    FMontoArchivo: int64;
    { Private declarations }
  public
    Function Inicializar(NombreReporte: AnsiString; CodigoOperacionInterfase:integer): Boolean;
    { Public declarations }
  end;

var
  FRptRecepcionRendiciones: TFRptRecepcionRendiciones;

implementation

{$R *.dfm}

const
    CONST_SIN_DATOS = 'Sin datos.';

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    lgisuk
  Date Created: 31/03/2006
  Description: Inicializo este formulario
  Parameters: NombreReporte: AnsiString
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFRptRecepcionRendiciones.Inicializar(NombreReporte: AnsiString; CodigoOperacionInterfase:integer): Boolean;
var                                                                             //SS_1147_NDR_20140710
  RutaLogo: AnsiString;                                                         //SS_1147_NDR_20140710
begin
    ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
    try                                                                                                 //SS_1147_NDR_20140710
      ppImage2.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
      ppImage1.Picture.LoadFromFile(Trim(RutaLogo));                                                    // SS_1147_CQU_20150420
      ppImage3.Picture.LoadFromFile(Trim(RutaLogo));                                                    // SS_1147_CQU_20150420
      ppImage4.Picture.LoadFromFile(Trim(RutaLogo));                                                    // SS_1147_CQU_20150420
      ppImage5.Picture.LoadFromFile(Trim(RutaLogo));                                                    // SS_1147_CQU_20150420
    except                                                                                              //SS_1147_NDR_20140710
      On E: Exception do begin                                                                          //SS_1147_NDR_20140710
        Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
        MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
        Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
      end;                                                                                              //SS_1147_NDR_20140710
    end;                                                                                                //SS_1147_NDR_20140710


    FNombreReporte:= NombreReporte;
    FCodigoOperacionInterfase:= CodigoOperacionInterfase;
    RBIListado.Execute;
    Result := True;
end;


{-----------------------------------------------------------------------------
  Function Name: RBIListadoExecute
  Author:    lgisuk
  Date Created: 31/03/2006
  Description:  Ejecuto el informe
  Parameters: Sender: TObject;var Cancelled: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRptRecepcionRendiciones.RBIListadoExecute(Sender: TObject;var Cancelled: Boolean);
resourcestring
	  MSG_ERROR_TO_GENERATE_REPORT = 'Error Al Generar Reporte';
    MSG_ERROR = 'Error';
var
    Config: TRBConfig;
begin
    //configuración del reporte
    Config := rbiListado.GetConfig;
    if Config.BinName <> '' then rbiListado.Report.PrinterSetup.BinName := Config.BinName;
    try

        //Abro la consulta de Comprobantes Pagos
        with SpObtenerResumenComprobantesRecibidos do begin

            Close;
            Parameters.Refresh;
            Parameters.ParamByName('@CodigoOperacionInterfase').Value := fcodigooperacioninterfase;
            Parameters.ParamByName('@Modulo').Value := NULL;
            Parameters.ParamByName('@FechaProcesamiento').Value := NULL;
            Parameters.ParamByName('@NombreArchivo').Value := NULL;
            Parameters.ParamByName('@Usuario').Value := NULL;
            Parameters.ParamByName('@CantidadComprobantesRecibidos').Value := NULL;
            Parameters.ParamByName('@ImporteTotalRecibido').Value := NULL;
            Parameters.ParamByName('@CantidadComprobantesAceptados').Value := NULL;
            Parameters.ParamByName('@ImporteTotalAceptado').Value := NULL;
            Parameters.ParamByName('@CantidadComprobantesRechazados').Value := NULL;
            Parameters.ParamByName('@ImporteTotalRechazado').Value := NULL;
            Parameters.ParamByName('@LineasArchivo').Value := NULL;
            Parameters.ParamByName('@MontoArchivo').Value := NULL;
            CommandTimeOut := 500;
            //Open;                                                             //SS_1147_MCA_20150420
            ExecProc;                                                           //SS_1147_MCA_20150420

            //Asigno los valores a los campos del reporte
            ppmodulo.Caption:= Parameters.ParamByName('@Modulo').Value;
            ppmodulo2.Caption:= ppmodulo.Caption; // SS_904_20100824
            ppmodulo3.Caption:= ppmodulo.Caption; // SS_904_20100824
            ppmodulo4.Caption:= ppmodulo.Caption; // SS_904_20100824
            ppmodulo5.Caption:= ppmodulo.Caption; // SS_904_20100824

            ppFechaProcesamiento.Caption:= DateToStr(Parameters.ParamByName('@FechaProcesamiento').Value);
            ppNombreArchivo.Caption:= Copy(Parameters.ParamByName('@NombreArchivo').Value,1,40);
            ppUsuario.Caption:= Parameters.ParamByName('@Usuario').Value;
            ppNumeroProceso.Caption := IntToStr(fcodigooperacioninterfase);
            ppLineas.Caption := iif(Parameters.ParamByName('@LineasArchivo').Value > 0, IntToStr(Parameters.ParamByName('@LineasArchivo').Value), CONST_SIN_DATOS);
            ppMontoArchivo.Caption := Parameters.ParamByName('@MontoArchivo').Value;

            ppCantidadComprobantesRecibidos.caption:= IntToStr(Parameters.ParamByName('@CantidadComprobantesRecibidos').Value);
            ppImporteTotalRecibido.Caption:= Parameters.ParamByName('@ImporteTotalRecibido').Value;
            ppCantidadComprobantesAceptados.caption:= IntToStr(Parameters.ParamByName('@CantidadComprobantesAceptados').Value);
            ppImporteTotalAceptado.Caption:= Parameters.ParamByName('@ImporteTotalAceptado').Value;
            ppCantidadComprobantesRechazados.caption:= IntToStr(Parameters.ParamByName('@CantidadComprobantesRechazados').Value);
            ppImporteTotalRechazados.Caption:= Parameters.ParamByName('@ImporteTotalRechazado').Value;
            //Asigno los valores a los campos del reporte
            ppCantidadRechazada.caption:= IntToStr(Parameters.ParamByName('@CantidadComprobantesRechazados').Value);
            ppImporteTotalRechazado.Caption:= Parameters.ParamByName('@ImporteTotalRechazado').Value;
        end;

        //Obtiene los comprobantes aprobados por la empresa de comprobanza a los cuales no
        //se les genero recibo
        with SpObtenerDetalleComprobantesAceptadosSinRecibo do begin
            Close;
            Parameters.Refresh;
            Parameters.ParamByName('@CodigoOperacionInterfase').Value := fcodigooperacioninterfase;
            Parameters.ParamByName('@Aceptado').Value := 1;
            Parameters.ParamByName('@CantidadComprobantesRechazados').Value := NULL;
            Parameters.ParamByName('@ImporteTotalRechazado').Value := NULL;
            CommandTimeOut := 500;
            Open;
            ppCantidadComprobantes.Caption := IntToStr(Parameters.ParamByName('@CantidadComprobantesRechazados').Value);
            ppImporteTotal.Caption := Parameters.ParamByName('@ImporteTotalRechazado').Value;
        end;

         //Obtengo los comprobantes rechazados por las empresas de cobranza
         with SpObtenerComprobantesRechazados do begin
            Close;
            Parameters.Refresh;
            Parameters.ParamByName('@CodigoOperacionInterfase').Value := fcodigooperacioninterfase;
            Parameters.ParamByName('@Aceptado').Value := 0;
            Parameters.ParamByName('@CantidadComprobantesRechazados').Value := NULL;
            Parameters.ParamByName('@ImporteTotalRechazado').Value := NULL;
            CommandTimeOut := 500;
            Open;
            ppCantidadRechazados.Caption := IntToStr(Parameters.ParamByName('@CantidadComprobantesRechazados').Value);
            ppImporteRechazados.Caption := Parameters.ParamByName('@ImporteTotalRechazado').Value;
        end;

        //Rev 1
        spObtenerErroresInterfase.Close;
        with spObtenerErroresInterfase do begin
        	Parameters.Refresh;
            Parameters.ParamByName('@CodigoOperacionInterfase').Value := FCodigoOperacionInterfase;
            Open;
        end;
        

        //Configuro el Report Builder y Ejecuto el Reporte
        rbiListado.Caption := FNombreReporte;

    except
        on E : exception do begin
            MsgBoxErr(MSG_ERROR_TO_GENERATE_REPORT, E.Message, MSG_ERROR, MB_ICONERROR );
        end;
    end;
end;

end.


