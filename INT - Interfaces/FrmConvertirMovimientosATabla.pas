{------------------------------------------------------------------------------
 File Name: FrmConvertirMovimientosATabla.pas
 Author:    lgisuk
 Date Created: 25/11/2004
 Language: ES-AR
 Description: M�dulo de la interface TransBank - Procesar Archivos de Movimientos
--------------------------------------------------------------------------------
 Revision History
--------------------------------------------------------------------------------
 Author: rcastro
 17/12/2004: Revisi�n General, Mensajes, ResourceString
-------------------------------------------------------------------------------
 Author: ndonadio
 05/04/2005: Cuadro de Dialogo confirmando procesamiento.
 24/04/2005: Mensaje de finalizacion con informacion del proceso y confirmacion para ver el Reporte de Errores.

 Autor         :    CQuezadaI
 Fecha         :    05/09/2013
 Firma         :    SS_1125_CQU_20130905
 Descripci�n   :    Se corrige el mensaje cuando se hace la confirmaci�n para procesar el archivo de mandatos.



Firma       : SS_1147Q_NDR_20141202[??]
Descripcion : Si es VS el convenio es de largo 12 no 17

 Autor       :   Claudio Quezada
 Fecha       :   30-03-2015
 Firma       :   SS_1147_CQU_20150324
 Descripcion :   Seg�n la Concesionaria Nativa, se leen los archivos con uno u otro formato
                 En el caso de VS el NumeroConvenio no se validar� el largo.

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

Firma       : SS_1147_MCA_20150508
Descripcion : se si produce un error al guardar el movimiento transbank se graba en la lista de erroes para luego informarla en ErroresInterfases.

Firma       : SS_1147_CQU_20150526
Descripcion : Se elimina la verificacion de si existe el convenio o no para el UNIVERSO y se hace en el SP, con ello ganamos algunos segundos de rapidez.
-------------------------------------------------------------------------------}
unit FrmConvertirMovimientosATabla;

interface

uses
    //Gestion de Interfaces
  	DMConnection,
    Util,
    UtilProc,
    PeaProcs,
    PeaTypes,                                                                   //SS_1147Q_NDR_20141202[??]
    ConstParametrosGenerales,
    ComunesInterfaces,
    FrmRptErroresSintaxis,           //Reporte de Errores de sintaxis
    //General
  	Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  	Dialogs, StdCtrls, Buttons, DB, ADODB, ExtCtrls, Grids, DBGrids,
  	DPSControls, ListBoxEx, DBListEx, ComCtrls, StrUtils, UtilRB,
    ppCtrls, ppBands, ppPrnabl, ppClass, ppCache, ppProd, ppReport, ppDB,
    ppComm, ppRelatv, ppTxPipe, UtilDB,Math;

type
  TFConvertirMovimientosATabla = class(TForm)
    OpenDialog: TOpenDialog;
    SPAgregarMovimientoTBK: TADOStoredProc;
  	SPEliminarMovimientosTBK: TADOStoredProc;
    pnlOrigen: TPanel;
    btnBuscarArchivo: TSpeedButton;
    txtOrigen: TEdit;
    lblOrigen: TLabel;
    SPProcesarExistentesUniversoPAT: TADOStoredProc;
    SPProcesarNoExistentesUniversoPAT: TADOStoredProc;
    SpProcesarNovedadesTBK: TADOStoredProc;
    SPProcesarRespuestaAMovimientosTBK: TADOStoredProc;
    btnProcesar: TButton;
    btnSalir: TButton;
    Bevel: TBevel;
    lblMensaje: TLabel;
    pnlAvance: TPanel;
    lblprocesoGeneral: TLabel;
    pbProgreso: TProgressBar;
    pnlAyuda: TPanel;
    ImgAyuda: TImage;
    procedure btnBuscarArchivoClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure txtOrigenChange(Sender: TObject);
    procedure btnProcesarClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure ImgAyudaClick(Sender: TObject);
    procedure ImgAyudaMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
  private
    { Private declarations }
    FCodigoOperacion : Integer;
    FCancelar: Boolean;         //Indica si ha sido cancelado
    FErrorGrave: Boolean;
    FProceso: AnsiString;      //Indica que proceso debo ejecutar
    FTotalRegistros: Integer;
    FTBK_CodigodeComercio : AnsiString;
    FTBK_DirectorioDestino : AnsiString;
    FTBK_DirectorioErrores : AnsiString;
    FTBK_DirectorioMandatosProcesados : AnsiString;
    FTBK_CantidadTopeMedioPagoBaja : AnsiString;
    FMandatosMayoraArchivo : String;
    FArchivoMayoraMandato  : String;
    FDiferenciaPermitidaArchVSBase   : integer;
    FDiferenciaPermitidaBaseVSArch   : integer;
    FCodigoNativa : Integer;    // SS_1147_CQU_20150324
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
  	Function   AbrirArchivo : Boolean;
    Function   ConfirmaCantidades(Lineas: Integer): Boolean;
    Function   GenerarRegistros(var Respuesta:integer):Boolean;
    Function   OpProcesarRespuestaAMovimientos : Boolean;
    function   OpProcesarBaseEntera : Boolean;
    Function   OpProcesarNovedades : Boolean;
    Function   GenerarReporteErroresSintaxis : Boolean;
    procedure GrabarErrores;                                                    //SS_1147_MCA_20150508
  public
    { Public declarations }
  	Function  Inicializar(txtCaption : ANSIString; Proceso :AnsiString; MDIChild:Boolean) : Boolean;
  end;

var
  FConvertirMovimientosATabla: TFConvertirMovimientosATabla;
  FLista, FErrores: TStringList;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    gcasais
  Date Created: 25/11/2004
  Description: M�dulo de inicializaci�n de este formulario
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFConvertirMovimientosATabla.Inicializar(txtCaption : ANSIString; Proceso : AnsiString; MDIChild : Boolean) : Boolean;

    {******************************** Function Header ******************************
    Function Name: VerificarParametrosGenerales
    Author : lgisuk
    Date Created : 20/07/2005
    Description : Obtengo los Parametros Generales que se utilizaran en el formulario
                  al inicializar y verifico que los valores obtenidos sean validos.
    Parameters : None
    Return Value : boolean
    *******************************************************************************
    Revision 1
    lgisuk
    05/02/07
    Ahora obtengo tambien la Cantidad de medios de pago que pueden
    ser eliminados sin mensaje advertencia.
    *******************************************************************************}
    Function VerificarParametrosGenerales : Boolean;
    resourcestring
        MSG_ERROR_CHECK_GENERAL_PARAMETER = 'Error al verificar parametro general';
        MSG_ERROR                         = 'Error';
        STR_NOT_EXISTS_GENERAL_PARAMETER  = 'No existe parametro general: ';
        STR_EMPTY_GENERAL_PARAMETER       = 'Parametro general vacio: ';                                         //Se verifica que los codigos no esten vacios, si no tienen razon de ser
        STR_DIRECTORY_NOT_EXISTS          = 'No existe el directorio indicado en parametros generales: ' + CRLF; //Los directorios de destino y errores en procesos de envio son criticos
    const
        TBK_CODIGODECOMERCIO             = 'TBK_CodigodeComercio';
        TBK_DIRECTORIODESTINO            = 'TBK_DirectorioDestino';
        TBK_DIRECTORIOERRORES            = 'TBK_DirectorioErrores';
        TBK_DIRECTORIOMANDATOSPROCESADOS = 'TBK_DirectorioMandatosProcesados';
        TBK_CANTIDADTOPEMEDIOPAGOBAJA    = 'TBK_CantidadTopeMedioPagoBaja';
    var
        DescError : AnsiString;
    begin
        Result    := True;
        DescError := '';
        try
            try
                // Obtengo la concesionaria nativa                  // SS_1147_CQU_20150324
                FCodigoNativa := ObtenerCodigoConcesionariaNativa;  // SS_1147_CQU_20150324

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, TBK_CODIGODECOMERCIO , FTBK_CodigodeComercio) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + TBK_CODIGODECOMERCIO;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                if not (FTBK_CodigodeComercio <> '') then begin
                   DescError := STR_EMPTY_GENERAL_PARAMETER + TBK_CODIGODECOMERCIO;
                   Result := False;
                   Exit;
                end;

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, TBK_DIRECTORIODESTINO , FTBK_DirectorioDestino) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + TBK_DIRECTORIODESTINO;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                FTBK_DirectorioDestino := GoodDir(FTBK_DirectorioDestino);
                if  not DirectoryExists(FTBK_DirectorioDestino) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FTBK_DirectorioDestino;
                    Result := False;
                    Exit;
                end;

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, TBK_DIRECTORIOERRORES , FTBK_DirectorioErrores) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + TBK_DIRECTORIOERRORES;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                FTBK_DirectorioErrores := GoodDir(FTBK_DirectorioErrores);
                if  not DirectoryExists(FTBK_DirectorioErrores) then begin
                    DescError := STR_DIRECTORY_NOT_EXISTS + FTBK_DirectorioErrores;
                    Result := False;
                    Exit;
                end;

                //Obtengo el Parametro General
                if not ObtenerParametroGeneral(DMConnections.BaseCAC, TBK_CANTIDADTOPEMEDIOPAGOBAJA , FTBK_CantidadTopeMedioPagoBaja) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + TBK_CANTIDADTOPEMEDIOPAGOBAJA;
                    Result := False;
                    Exit;
                end;
                //Verifico que sea v�lido
                if not (FTBK_CantidadTopeMedioPagoBaja <> '') then begin
                   DescError := STR_EMPTY_GENERAL_PARAMETER + TBK_CANTIDADTOPEMEDIOPAGOBAJA;
                   Result := False;
                   Exit;
                end;

                //Obtengo el parametro general
                ObtenerParametroGeneral(DMConnections.BaseCAC,TBK_DIRECTORIOMANDATOSPROCESADOS,FTBK_DirectorioMandatosProcesados);

                if not ObtenerParametroGeneral(DMConnections.BaseCAC, 'CANT_MAX_DIF_PAT_TBK_BASEVSARCH',FDiferenciaPermitidaBaseVSArch) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + 'CANT_MAX_DIF_PAT_TBK_BASEVSARCH';
                    Result := False;
                    Exit;
                end;

                if not ObtenerParametroGeneral(DMConnections.BaseCAC, 'CANT_MAX_DIF_PAT_TBK_ARCHVSBASE',FDiferenciaPermitidaArchVSBase) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + 'CANT_MAX_DIF_PAT_TBK_ARCHVSBASE';
                    Result := False;
                    Exit;
                end;

                if not ObtenerParametroGeneral(DMConnections.BaseCAC, 'MSG_DIF_ARCHIVO_MAYOR_A_MANDATOS',FArchivoMayoraMandato) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + 'MSG_DIF_ARCHIVO_MAYOR_A_MANDATOS';
                    Result := False;
                    Exit;
                end;

                if not ObtenerParametroGeneral(DMConnections.BaseCAC, 'MSG_DIF_MANDATOS_MAYOR_A_ARCHIVO',FMandatosMayoraArchivo) then begin
                    DescError := STR_NOT_EXISTS_GENERAL_PARAMETER + 'MSG_DIF_MANDATOS_MAYOR_A_ARCHIVO';
                    Result := False;
                    Exit;
                end;



            except
                on e: Exception do begin
                    Result := False;
                    MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, e.Message, MSG_ERROR, MB_ICONWARNING);
                end;
            end;

        finally
            //si no paso la verificacion de parametros generales
            if (Result = False) then begin
                //informo la situaci�n
                MsgBoxErr(MSG_ERROR_CHECK_GENERAL_PARAMETER, DescError, MSG_ERROR, MB_ICONWARNING);
            end;
        end;
    end;

resourcestring
    MSG_INIT_ERROR = 'Error al Inicializar';
  	MSG_ERROR = 'Error';
begin
  	Result := False;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;

    //Centro el formulario
  	CenterForm(Self);
    try
        DMConnections.BaseCAC.Connected := True;
        Result := DMConnections.BaseCAC.Connected and
                                VerificarParametrosGenerales;
    except
        on e: Exception do begin
            MsgBoxErr(MSG_INIT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
        end;
    end;
    //Resolver ac� lo que necesita este form para inicializar correctamente
    Caption := AnsiReplaceStr(txtCaption, '&', '');  //titulo
    Fproceso := Proceso;           //Asigno proceso a ejecutar
  	btnProcesar.enabled := False;  //Procesar
  	lblmensaje.Caption := '';      //limpio el indicador
    PnlAvance.visible := False;    //Oculto el Panel de Avance
    FErrores.Clear;                //Inicio la lista de errores
    FCodigoOperacion := 0;         //inicializo codigo de operacion
end;

{-----------------------------------------------------------------------------
  Function Name: ImgAyudaClick
  Author:    lgisuk
  Date Created: 03/06/2005
  Description: Mensaje de Ayuda
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFConvertirMovimientosATabla.ImgAyudaClick(Sender: TObject);
Resourcestring
     MSG_RESPUESTAMOVIC = ' ' + CRLF +
                          'El Archivo de Respuesta a Movimientos' + CRLF +
                          'es la respuesta que env�a TRANSBANK al ESTABLECIMIENTO' + CRLF +
                          'para cada Archivo de Movimientos recibido' + CRLF +
                          'y tiene por objeto dar a conocer al ESTABLECIMIENTO,' + CRLF +
                          'el resultado del procesamiento de la informaci�n enviada.' + CRLF +
                          ' ' + CRLF +
                          'Nombre del Archivo: 99999999_RESPUESTAMOVIC_nombre archivo' + CRLF +
                          ' ' + CRLF +
                          'Se utiliza para actualizar los datos de' + CRLF +
                          'los mandantes en el sistema del ESTABLECIMIENTO.' + CRLF +
                          ' ';

    MSG_BASEINSTDECARGO = ' ' + CRLF +
                          'El archivo Base de Instrucciones de Cargo' + CRLF +
                          'es enviado por TRANSBANK al ESTABLECIMIENTO' + CRLF +
                          'segun la periodicidad convenida y contiene' + CRLF +
                          'el detalle de todos los mandantes que tienen' + CRLF +
                          'medio de pago PAT de TRANSBANK' + CRLF +
                          ' ' + CRLF +
                          'Nombre del Archivo: 99999999_ BASEINSTDECARGO _mmddaa.S' + CRLF +
                          ' ' + CRLF +
                          'Se utiliza para actualizar los datos de' + CRLF +
                          'los mandantes en el sistema del ESTABLECIMIENTO.' + CRLF +
                          ' ';

    MSG_NOVEDADESIC     = ' ' + CRLF +
                          'El Archivo de Novedades' + CRLF +
                          'es enviado por TRANSBANK al ESTABLECIMIENTO' + CRLF +
                          'para informar las altas, bajas y modificaciones ' + CRLF +
                          'de mandantes efectuadas' + CRLF +
                          'por los Emisores y/o por TRANSBANK' + CRLF +
                          ' ' + CRLF +
                          'Nombre del Archivo: 99999999_ NOVEDADESIC _mmddaa.S' + CRLF +
                          ' ' + CRLF +
                          'Se utiliza para actualizar los datos de' + CRLF +
                          'los mandantes en el sistema del ESTABLECIMIENTO.' + CRLF +
                          ' ';

var
    Mensaje : string;
begin
    //si esta procesando sale
    if PnlAvance.visible = true then exit;
    //Asigno mensaje segun proceso a ejecutar
    if (Fproceso = 'RESPUESTAMOVIC')  then Mensaje := MSG_RESPUESTAMOVIC;
    if (Fproceso = 'BASEINSTDECARGO') then Mensaje := MSG_BASEINSTDECARGO;
    if (Fproceso = 'NOVEDADESIC')     then Mensaje := MSG_NOVEDADESIC;
    //Muestro el mensaje
    MsgBoxBalloon(Mensaje, Caption, MB_ICONQUESTION, IMGAYUDA);
end;

{-----------------------------------------------------------------------------
  Function Name: ImgAyudaMouseMove
  Author:    lgisuk
  Date Created: 03/06/2005
  Description:
  Parameters: Sender: TObject;Shift: TShiftState; X, Y: Integer
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFConvertirMovimientosATabla.ImgAyudaMouseMove(Sender: TObject;Shift: TShiftState; X, Y: Integer);
begin
    if PnlAvance.visible = true then ImgAyuda.Cursor := crdefault else ImgAyuda.Cursor := crHandPoint;
end;

{-----------------------------------------------------------------------------
  Function Name: BuscarArchivoBTNClick
  Author:    lgisuk
  Date Created: 03/01/2005
  Description: busco el archivo a procesar
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFConvertirMovimientosATabla.btnBuscarArchivoClick(Sender: TObject);

    //Verifica si es un Archivo Valido para Esta Pantalla
    Function EsArchivoValido ( Nombre : Ansistring) : Boolean;
    begin
        Result:= ExistePalabra(Nombre, FTBK_CodigodeComercio) and //posee el codigo de comercio
            	         ((ExistePalabra(Nombre, 'RESPUESTAMOVIC') and (Fproceso = 'RESPUESTAMOVIC')) or //es archivo Respuesta a Movimientos
                              (ExistePalabra(Nombre, 'BASEINSTDECARGO') and (Fproceso = 'BASEINSTDECARGO')) or //o es archivo Base Entera
                              		(ExistePalabra(Nombre, 'NOVEDADESIC') and (Fproceso = 'NOVEDADESIC')))
    end;

resourcestring
	MSG_ERROR = 'No es un archivo de Movimientos Valido!';
begin
	OpenDialog.InitialDir := FTBK_DirectorioDestino;
	if OpenDialog.execute then begin
		if not EsArchivoValido(OpenDialog.filename) then begin

            MsgBox(MSG_ERROR, Self.Caption, MB_OK + MB_ICONINFORMATION);
      			txtOrigen.text := '';

		end
		else begin

        btnBuscarArchivo.Enabled := False;
        txtOrigen.text := OpenDialog.FileName;

		end;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: EOrigenChange
  Author:    lgisuk
  Date Created: 04/07/2005
  Description: Controlo que abra un archivo valido
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFConvertirMovimientosATabla.txtOrigenChange(Sender: TObject);
begin
    //Controlo que abra un archivo valido
  	btnProcesar.Enabled := FileExists( txtOrigen.Text);
end;

{-----------------------------------------------------------------------------
  Function Name: AbrirArchivo
  Author:    lgisuk
  Date Created: 16/12/2004
  Description: Abro el Archivo TXT
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
Function TFConvertirMovimientosATabla.AbrirArchivo : Boolean;
resourcestring
	MSG_OPEN_FILE_ERROR = 'Error al abrir el archivo';
begin
    Result := False;
    FLista.text := FileToString(txtOrigen.text);
    FTotalRegistros := FLista.count-1;
    if FLista.text <> '' then  begin
        Result := True;
        if FTBK_DirectorioMandatosProcesados <> '' then begin
            if RightStr(FTBK_DirectorioMandatosProcesados,1) = '\' then  FTBK_DirectorioMandatosProcesados := FTBK_DirectorioMandatosProcesados + ExtractFileName(txtOrigen.text)
            else FTBK_DirectorioMandatosProcesados := FTBK_DirectorioMandatosProcesados + '\' + ExtractFileName(txtOrigen.text);
        end;
    end else begin
        FErrorGrave := True;
        MsgBox(MSG_OPEN_FILE_ERROR);
        Exit;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ConfirmaCantidades
  Author:    ndonadio
  Date Created: 05/04/2005
  Description:  Consulta al usuario si quiere continuar o no,
                presentandole un resumen de las cantidades a procesar
                y de las cantidades existentes.
  Parameters: Lineas: integer
  Return Value: Boolean
-----------------------------------------------------------------------------
 Revision 1
 lgisuk
 06/02/2007
 Antes para traer los convenios adheridos a transbank traia todos los convenios PAT
 que el emisor de la tarjeta no era presto. ahora se obtiene trayendo todas los
 convenios con tarjetas que pertenecen a transbank.

 Revision 2
 Author : Fsandi
 Date : 24-05-2007
 Description : Se modifica las consultas SQL en delphi, se las cambia para que llamen funciones de la Base de Datos

  Revision 3
 Author : Fsandi
 Date : 08-06-2007
 Description : Ahora se ponen mensajes de advertencia o error si la diferencia es mucha entre el total mandatos y el archivo

   Revision 4
 Author : Fsandi
 Date : 26-06-2007
 Description : Por solicitud del cliente, se permite continuar aunque aparezca el mensaje de error.
-----------------------------------------------------------------------------}
function TFConvertirMovimientosATabla.ConfirmaCantidades(Lineas: Integer): Boolean;
resourcestring
    //MSG_TITLE        = 'Confirmar Recepci�n de Universo PAT - Transbank';         // SS_1125_CQU_20130905
    MSG_TITLE        = 'Confirmar %s - Transbank';                                  // SS_1125_CQU_20130905
    MSG_CONFIRMATION = 'Actualmente existen %d Mandatos Confirmados '+crlf+
                       'y %d Mandatos Pendientes, que hacen'+crlf+
                       'un total de %d Mandatos PAT-Transbank Existentes.'+crlf+crlf+
                       'El Archivo seleccionado contiene %d Mandatos a procesar.';
    MSG_KEEP_PROCESSING = 'Desea continuar con el procesamiento del archivo?';

var
    MandatosPendientes, MandatosConfirmados, MandatosExistentes : Integer;
    MensajeCompleto : String;
    CodigoFamiliaTransbank : Integer;
    TituloMensaje : string;                                                         // SS_1125_CQU_20130905
begin
    CodigoFamiliaTransbank := QueryGetValueInt(DMConnections.BaseCAC,'select DBO.CONST_CODIGO_FAMILIA_TARJETA_TRANSBANK()');
    //Obtengo la cantida de mandatos Existentes/Pendientes y Confirmados
    MandatosExistentes  :=  QueryGetValueInt(DMConnections.BaseCAC, format('select dbo.ObtenerMandatos(%d,%d)',[1,CodigoFamiliaTransbank]));
    MandatosPendientes  :=  QueryGetValueInt(DMConnections.BaseCAC, format('select dbo.ObtenerMandatos(%d,%d)',[2,CodigoFamiliaTransbank]));
    MandatosConfirmados :=  QueryGetValueInt(DMConnections.BaseCAC, format('select dbo.ObtenerMandatos(%d,%d)',[3,CodigoFamiliaTransbank]));
    //Armo el mensaje
    MensajeCompleto := Format(MSG_CONFIRMATION,[MandatosConfirmados,MandatosPendientes,MandatosExistentes, Lineas]);
    TituloMensaje   := Format(MSG_TITLE, [ReplaceStr(Caption, ' TransBank', '')]);  // SS_1125_CQU_20130905
    // se comparan las cantidades en el archivo y la tabla que no se pasen de ciertas diferencias determinadas por parametro
    // si se superan los par�metros, se solicita confirmaci�n o se cancela el proceso.

    if MandatosExistentes > Lineas then begin
        //if MandatosExistentes - Lineas > FDiferenciaPermitidaBaseVSArch then begin                                        // SS_1125_CQU_20130905
        if (MandatosExistentes - Lineas > FDiferenciaPermitidaBaseVSArch) and not (FProceso = 'RESPUESTAMOVIC') then begin  // SS_1125_CQU_20130905
            MensajeCompleto := MensajeCompleto + CRLF + CRLF + FMandatosMayoraArchivo;
        end else begin
            MensajeCompleto := MensajeCompleto + CRLF + CRLF + MSG_KEEP_PROCESSING;
        end;
        //Result := ( MsgBox(MensajeCompleto,MSG_TITLE, MB_ICONQUESTION + MB_YESNO) = mrYes );                              // SS_1125_CQU_20130905
        Result := ( MsgBox(MensajeCompleto,TituloMensaje, MB_ICONQUESTION + MB_YESNO) = mrYes );                            // SS_1125_CQU_20130905
    end;

    if Lineas > MandatosExistentes then begin
        //if Lineas - MandatosExistentes > FDiferenciaPermitidaArchVSBase then begin                                        // SS_1125_CQU_20130905
        if (Lineas - MandatosExistentes > FDiferenciaPermitidaArchVSBase) and not (FProceso = 'RESPUESTAMOVIC') then begin  // SS_1125_CQU_20130905
            MensajeCompleto := MensajeCompleto +crlf+crlf+ FArchivoMayoraMandato;
        end else begin
            MensajeCompleto := MensajeCompleto + CRLF + CRLF + MSG_KEEP_PROCESSING ;
        end;
        //Result := ( MsgBox(MensajeCompleto,MSG_TITLE, MB_ICONQUESTION + MB_YESNO) = mrYes );      // SS_1125_CQU_20130905
        Result := ( MsgBox(MensajeCompleto,TituloMensaje, MB_ICONQUESTION + MB_YESNO) = mrYes );    // SS_1125_CQU_20130905
    end;
    
    if Lineas = MandatosExistentes then begin
        MensajeCompleto := MensajeCompleto + CRLF + CRLF + MSG_KEEP_PROCESSING;
        //Result := ( MsgBox(MensajeCompleto,MSG_TITLE, MB_ICONQUESTION + MB_YESNO) = mrYes );      // SS_1125_CQU_20130905
        Result := ( MsgBox(MensajeCompleto,TituloMensaje, MB_ICONQUESTION + MB_YESNO) = mrYes );    // SS_1125_CQU_20130905
    end;

end;

{-----------------------------------------------------------------------------
  Function Name: GenerarRegistros
  Author:    lgisuk
  Date Created: 16/12/2004
  Description:  Inserto los Registros en la Tabla TBK_Movimientos
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
Function TFConvertirMovimientosATabla.GenerarRegistros(var Respuesta : Integer) : Boolean;

    {-----------------------------------------------------------------------------
      Function Name: EliminarMovimientosTBK
      Author:    lgisuk
      Date Created: 04/07/2005
      Description: Elimino Todos los Registros de la Tabla TBK_Movimientos
      Parameters: None
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function EliminarMovimientosTBK : Boolean;
    resourcestring
        MSG_DELETE_RECORDS_ERROR = 'Error al Eliminar Movimientos';
        MSG_ERROR = 'Error';
    begin
        Result := False;
        try
            with SPEliminarMovimientosTBK do begin
                CommandTimeout := 500;
                ExecProc;                           //Ejecuto el procedimiento
                Result := True;
    		end;
        except
            on e: Exception do begin
                FErrorGrave := True;
                MsgBoxErr(MSG_DELETE_RECORDS_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
                Exit;
            end;
        end;
    end;

    {-----------------------------------------------------------------------------
      Function Name: MovimientosTxtToTable
      Author:    lgisuk
      Date Created: 04/07/2005
      Description: Convierto las lineas del TXT en Registros en la Tabla
                   TBK_Movimientos
      Parameters: var Respuesta:integer
      Return Value: Boolean
    -----------------------------------------------------------------------------}
    Function MovimientosTxtToTable(var Respuesta : Integer) : Boolean;
    resourcestring
        MSG_RECORD_ERROR          = 'Error en importaci�n Registro';
        MSG_RECORD_NOT_EXISTS     = 'No existe el convenio';
        MSG_RECORD_INVALID_LENGTH = 'Longitud menor a 17 Caracteres';
        MSG_ERROR = 'Error';
    Const
        STR_CONVENIO = 'Convenio: ';
    var
        I : Integer;
        NumeroConvenio : String;
        ExisteConvenio : Boolean;
    begin
        Result := False;
        Respuesta := 0;                    //Comienza importacion correctamente
        FCancelar := False;                //Permito que la importacion sea cancelada
        pbProgreso.Position := 0;          //Inicio la Barra de progreso
        pbProgreso.Max := FLista.Count-1;  //Establezco como maximo la cantidad de registros del memo
        i:=1; //aca iba 2
      	while i < FLista.count do begin
        		//Application.ProcessMessages; // SS_1147_CQU_20150526

        		if FCancelar then begin
                Respuesta := 2;
                pbProgreso.Position := 0;  //Si la importacion fue detenida, Vuelvo a Comenzar
                Exit;
            end;

            //obtengo el numero de convenio
            NumeroConvenio := ParseParamByNumber(FLista.strings[i],3,';');

            {verifico que la longitud del numero de convenio sea de 17 caracteres}
            //if  Length(Numeroconvenio) >= 17 then begin                       // SS_1147_CQU_20150324
            if ((FCodigoNativa <> CODIGO_VS) and (Length(Numeroconvenio) >= 17) // SS_1147_CQU_20150324
            or (FCodigoNativa = CODIGO_VS)) then begin                          // SS_1147_CQU_20150324
                    //if ObtenerCodigoConcesionariaNativa=CODIGO_VS then        // SS_1147_CQU_20150324  //SS_1147Q_NDR_20141202[??]
                    //begin                                                     // SS_1147_CQU_20150324  //SS_1147Q_NDR_20141202[??]
                    //  NumeroConvenio := Copy(NumeroConvenio,6,12);            // SS_1147_CQU_20150324 //SS_1147Q_NDR_20141202[??]
                    //end;                                                      // SS_1147_CQU_20150324  //SS_1147Q_NDR_20141202[??]
                    {verifico que le convenio exista}
                    //Existeconvenio := trim(QueryGetValue(DMConnections.BaseCAC, Format('select dbo.obtenercodigoconvenio (''%s'')',[numeroconvenio]))) <> '';     // SS_1147_CQU_20150526
                    //if Existeconvenio = true then begin                                                                                                           // SS_1147_CQU_20150526

                        with SPAgregarMovimientoTBK.Parameters do begin
                            ParamByName('@DEIC_TIPO_REG').Value := ParseParamByNumber(Flista.Strings[i],1,';');
                            ParamByName('@DEIC_COD_CRED').Value := ParseParamByNumber(Flista.Strings[i],2,';');
                            ParamByName('@DEIC_ID_SERVICIO').Value := ParseParamByNumber(Flista.Strings[i],3,';');
                            ParamByName('@DEIC_MONTO_APORTE').Value := ParseParamByNumber(Flista.Strings[i],4,';');
                            ParamByName('@DEIC_RUT').Value := ParseParamByNumber(Flista.Strings[i],5,';');
                            ParamByName('@DEIC_TARJETA').Value := ParseParamByNumber(Flista.Strings[i],6,';');
                            ParamByName('@DEIC_FEC_EXPIRA').Value := ParseParamByNumber(Flista.Strings[i],7,';');
                            ParamByName('@DEIC_TINS_CODIGO').Value := ParseParamByNumber(Flista.Strings[i],8,';');
                            ParamByName('@DEIC_ORIGEN').Value := ParseParamByNumber(Flista.Strings[i],9,';');
                            ParamByName('@DEIC_ESTADO').Value := ParseParamByNumber(Flista.Strings[i],10,';');
                            ParamByName('@DEIC_FEC_ALTA').Value := ParseParamByNumber(Flista.Strings[i],11,';');
                            ParamByName('@DEIC_HORA_ALTA').Value := ParseParamByNumber(Flista.Strings[i],12,';');
                            ParamByName('@DEIC_FEC_BAJA').Value := ParseParamByNumber(Flista.Strings[i],13,';');
                            ParamByName('@DEIC_HORA_BAJA').Value := ParseParamByNumber(Flista.Strings[i],14,';');
                            ParamByName('@DEIC_FEC_MODIF').Value := ParseParamByNumber(Flista.Strings[i],15,';');
                            ParamByName('@DEIC_HORA_MODIF').Value := ParseParamByNumber(Flista.Strings[i],16,';');
                            ParamByName('@DEIC_NOMBRE_CAMPANA').Value := ParseParamByNumber(Flista.Strings[i],17,';');
                            ParamByName('@DEIC_DIAS_EXCEPCION').Value := ParseParamByNumber(Flista.Strings[i],18,';');
                            ParamByName('@DEIC_COD_RTA').Value := ParseParamByNumber(Flista.Strings[i],19,';');
                            ParamByName('@DEIC_GLOSA_RPTA').Value := ParseParamByNumber(Flista.Strings[i],20,';');
                            ParamByName('@DEIC_OBSERVACIONES').Value := ParseParamByNumber(Flista.Strings[i],21,';');
                            ParamByName('@FILLER').Value := ParseParamByNumber(Flista.Strings[i],22,';');
                        end;
                        try
                            SPAgregarMovimientoTBK.CommandTimeout := 500;
                            SPAgregarMovimientoTBK.ExecProc;            //Ejecuto el procedimiento
                        except
                            on e: exception do begin
                                if e.Message = 'No existe el convenio' then begin                           // SS_1147_CQU_20150526
                                    FErrores.Add(STR_CONVENIO                                               // SS_1147_CQU_20150526
                                                + ParseParamByNumber(FLista.strings[i],3,';')               // SS_1147_CQU_20150526
                                                + ' - ' + MSG_RECORD_NOT_EXISTS);                           // SS_1147_CQU_20150526
                                end else begin                                                              // SS_1147_CQU_20150526
                                //FErrorGrave := True;                                                     //SS_1147_MCA_20150508
                                FErrores.Add(STR_CONVENIO + ParseParamByNumber(FLista.strings[i],3,';') +  //SS_1147_MCA_20150508
                                         ' - ' + e.Message);                                               //SS_1147_MCA_20150508
                                //MsgBoxErr(STR_CONVENIO + ParseParamByNumber(FLista.strings[i],3,';') + ' - ' + MSG_RECORD_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);      //SS_1147_MCA_20150508
                              //  Exit;
                                end;                                                                        // SS_1147_CQU_20150526
                            end;
                        end;

                    //end else begin                                                                // SS_1147_CQU_20150526
                    //    FErrores.Add(STR_CONVENIO + ParseParamByNumber(FLista.strings[i],3,';') + // SS_1147_CQU_20150526
                    //                     ' - ' + MSG_RECORD_NOT_EXISTS);                          // SS_1147_CQU_20150526
                    //end;                                                                          // SS_1147_CQU_20150526

            end else begin
            	FErrores.Add(STR_CONVENIO + ParseParamByNumber(FLista.strings[i],3,';') +
                                         ' - ' + MSG_RECORD_INVALID_LENGTH);
            end;

            pbProgreso.Position := pbProgreso.Position + 1;  //Muestro el progreso
            Application.ProcessMessages;                    //Refresco la pantalla
            i := i + 1;
        end;

        Result := True;
    end;

begin
    {Elimino los Registros de la Tabla e Importo los registros del archivo
    a la tabla TBK_MOVIMIENTOS}
    Result := (EliminarMovimientosTBK and
                    MovimientosTxtToTable(Respuesta));
end;

{-----------------------------------------------------------------------------
  Function Name: OpProcesarRespuestaAMovimientos
  Author:    lgisuk
  Date Created: 16/12/2004
  Description:
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
Function TFConvertirMovimientosATabla.OpProcesarRespuestaAMovimientos : Boolean;

    {-----------------------------------------------------------------------------
      Function Name: ProcesarRespuestaAMovimientos
      Author:    lgisuk
      Date Created: 04/07/2005
      Description: Proceso El Archivo de Respuesta a Movimientos
      Parameters: None
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function ProcesarRespuestaAMovimientos : Boolean;
    var
      	FSiguienteConvenio : Integer;
        FDescError		   : Ansistring;
    begin

        FSiguienteConvenio := 0;

        //Barra de Progreso
        pbProgreso.Position := 0;          //Inicio la Barra de progreso
        pbProgreso.Max := 10;              //Establezco cantidad maxima
        Application.ProcessMessages;


      	while ( FSiguienteConvenio <> - 1 )  do begin

        		with SPProcesarRespuestaaMovimientosTBK.Parameters do begin
            			ParamByName( '@CodigoConvenioInicial' ).Value := FSiguienteConvenio;
        		end;

            try
                with SPProcesarRespuestaaMovimientosTBK do begin
                    CommandTimeout := 500;
                    ExecProc;
                    FSiguienteConvenio := Parameters.ParamByName( '@UltimoCodigoConvenio' ).Value;
                    FDescError := VarToStr( Parameters.ParamByName( '@DescripcionError' ).Value );
                    if ( FSiguienteConvenio <> -1 ) then Inc( FSiguienteConvenio );

                    //Muestro el progreso
                    pbProgreso.Position := pbProgreso.Position + 1;
                    Application.ProcessMessages;

                end;

        		except
                 Raise; //manejo el error en el procedimiento que lo llamo
            end;
          end;

        //Llevo la barra de progreso al final
        pbProgreso.Position := 10;
        Application.ProcessMessages;


        Result := True;
    end;


resourcestring
  	MSG_PROCESS_ERROR = 'Error al procesar respuesta a Movimientos';
    MSG_ERROR = 'Error';
Const
    STR_OBSERVACION = 'Procesar Respuesta A Movimientos';
    COD_MODULO = 24;
var
    NombreArchivo : AnsiString;
    DescError : AnsiString;
begin
    Result := False;
    try
        NombreArchivo := Trim(ExtractFileName(txtOrigen.text));
        Result := (RegistrarOperacionEnLogInterface (DMConnections.BaseCAC, COD_MODULO, NombreArchivo,UsuarioSistema, STR_OBSERVACION ,false, false, now,0,FCodigoOperacion,DescError) and
                            ProcesarRespuestaAMovimientos);
    except
        on e: Exception do begin
            FErrorGrave := True;
            MsgBoxErr(MSG_PROCESS_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
            Exit;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: OpProcesarNovedades
  Author:    lgisuk
  Date Created: 13/05/2005
  Description:  Procesar las novedades que envia transbank
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
Function TFConvertirMovimientosATabla.OpProcesarNovedades : Boolean;

    {-----------------------------------------------------------------------------
      Function Name: ProcesarNovedades
      Author:    lgisuk
      Date Created: 04/07/2005
      Description: Proceso El Archivo de Respuesta a Movimientos
      Parameters: None
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function ProcesarNovedades : Boolean;
    var
      	FSiguienteConvenio : Integer;
        FDescError		   : AnsiString;
    begin
        FSiguienteConvenio := 0;

        //Barra de Progreso
        pbProgreso.Position := 0;          //Inicio la Barra de progreso
        pbProgreso.Max := 10;              //Establezco cantidad maxima
        Application.ProcessMessages;

      	while ( FSiguienteConvenio <> - 1 )  do begin

        		with SPProcesarNovedadesTBK.Parameters do begin
          			ParamByName( '@CodigoConvenioInicial' ).Value := FSiguienteConvenio;
        		end;

            try
                with SPProcesarNovedadesTBK do begin
                    CommandTimeout := 500;
                    ExecProc;
                    FSiguienteConvenio := Parameters.ParamByName( '@UltimoCodigoConvenio' ).Value;
                    FDescError := VarToStr( Parameters.ParamByName( '@DescripcionError' ).Value );
                    if ( FSiguienteConvenio <> -1 ) then Inc( FSiguienteConvenio );

                    //Muestro el progreso
                    pbProgreso.Position := pbProgreso.Position + 1;
                    Application.ProcessMessages;

                end;
        		except
                 Raise; //manejo el error en el procedimiento que lo llamo
            end;
        end;

        //Llevo la barra de progreso al final
        pbProgreso.Position := 10;
        Application.ProcessMessages;

        Result := True;
    end;


resourcestring
  	MSG_PROCESS_ERROR = 'Error al procesar Novedades';
    MSG_ERROR = 'Error';
Const
    STR_OBSERVACION = 'Procesar Novedades';
    COD_MODULO = 47;
var
    NombreArchivo : AnsiString;
    DescError : AnsiString;
begin
    Result := False;
    try
        NombreArchivo := Trim(ExtractFileName(txtOrigen.text));
        Result := (RegistrarOperacionEnLogInterface(DMConnections.BaseCAC, COD_MODULO , NombreArchivo, UsuarioSistema, STR_OBSERVACION, False, False, Now, 0, FCodigoOperacion, DescError) and
                            ProcesarNovedades);
    except
        on e: Exception do begin
            FErrorGrave := True;
            MsgBoxErr(MSG_PROCESS_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
            Exit;
        end;
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: OpProcesarBaseEntera
  Author:    lgisuk
  Date Created: 16/12/2004
  Description:
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
function TFConvertirMovimientosATabla.OpProcesarBaseEntera:boolean;

    {-----------------------------------------------------------------------------
      Function Name: ProcesarUniversoPat
      Author:    lgisuk
      Date Created: 04/07/2005
      Description: Proceso el Archivo con la Base Entera
      Parameters: None
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function ProcesarUniversoPat : Boolean;

        //Proceso el Archivo con la Base Entera (Altas/Modificaciones)
        Function ProcesarExistentesUniversoPat : Boolean;
        var
          	FSiguienteConvenio : Integer;
            FDescError		   : AnsiString;
        begin
            Result := False;
           	FSiguienteConvenio := 0;
      			Screen.Cursor := crHourglass;
            //Barra de Progreso
            pbProgreso.Position := 0;          //Inicio la Barra de progreso
            pbProgreso.Max := 10;              //Establezco cantidad maxima
            Application.ProcessMessages;

            try
                while ( FSiguienteConvenio <> - 1 )  do begin
                    with spProcesarExistentesUniversoPat.Parameters do begin
                        ParamByName( '@CodigoConvenioInicial' ).Value := FSiguienteConvenio;
                    end;
                    try
                        with spProcesarExistentesUniversoPat do begin
                            CommandTimeout := 500;
                            ExecProc;
                            FSiguienteConvenio := Parameters.ParamByName( '@UltimoCodigoConvenio' ).Value;
                            FDescError := VarToStr( Parameters.ParamByName( '@DescripcionError' ).Value );
                            if ( FSiguienteConvenio <> -1 ) then Inc( FSiguienteConvenio );

                            //Muestro el progreso
                            pbProgreso.Position:= pbProgreso.Position + 1;
                            Application.ProcessMessages;
                        end;
                    except
                         Raise; //manejo el error en el procedimiento que lo llamo
                    end;
                end;

                Result := True;

                //Llevo la barra de progreso al final
                pbProgreso.Position := 10;
                Application.ProcessMessages;


            finally
        				Screen.Cursor := crDefault;
            end;
        end;

        {-----------------------------------------------------------------------------
          Function Name: ProcesarNoExistentesUniversoPat
          Author:    lgisuk
          Date Created: 04/07/2005
          Description: Proceso el Archivo con la Base Entera (Bajas)
          Parameters: None
          Return Value: Boolean
        -----------------------------------------------------------------------------}
        Function ProcesarNoExistentesUniversoPat : Boolean;
        var
        	FSiguienteConvenio : Integer;
        	FDescError : Ansistring;
        begin
            Result := False;
            FSiguienteConvenio := 0;
      			Screen.Cursor := crHourglass;

            //Barra de Progreso
            pbProgreso.Position := 0;          //Inicio la Barra de progreso
            pbProgreso.Max := 10;              //Establezco cantidad maxima
            Application.ProcessMessages;

            try
                while ( FSiguienteConvenio <> - 1 )  do begin
                    with spProcesarNoExistentesUniversoPat.Parameters do begin
                        ParamByName( '@CodigoConvenioInicial' ).Value := FSiguienteConvenio;
                    end;
                    try
                        with spProcesarNoExistentesUniversoPat do begin
                            CommandTimeout := 500;
                            ExecProc;
                            FSiguienteConvenio := Parameters.ParamByName( '@UltimoCodigoConvenio' ).Value;
                            FDescError := VarToStr( Parameters.ParamByName( '@DescripcionError' ).Value );
                            if ( FSiguienteConvenio <> -1 ) then Inc( FSiguienteConvenio );

                            //Muestro el progreso
                            pbProgreso.Position:= pbProgreso.Position + 1;
                            Application.ProcessMessages;

                        end;
                    except
                         Raise; //manejo el error en el procedimiento que lo llamo
                    end;
                end;
                Result := True;

                //Llevo la barra de progreso al final
                pbProgreso.Position := 10;
                Application.ProcessMessages;

            finally
			        	Screen.Cursor := crDefault;
            end;
        end;


    begin
        Result := (ProcesarExistentesUniversoPat and ProcesarNoExistentesUniversoPat);
    end;

resourcestring
  	MSG_PROCESS_ERROR = 'Error al procesar base entera';
    MSG_ERROR = 'Error';
Const
    STR_OBSERVACION = 'Procesar Base Entera';
    COD_MODULO = 34;
var
    NombreArchivo : Ansistring;
    DescError : Ansistring;
begin
    result:=false;
    try
        NombreArchivo := Trim(ExtractFileName(txtOrigen.text));
        Result := (RegistrarOperacionEnLogInterface(DMConnections.BaseCAC, COD_MODULO, NombreArchivo, UsuarioSistema, STR_OBSERVACION, False, False, NowBase(DMConnections.BaseCAC), 0, FCodigoOperacion, DescError) and
                          ProcesarUniversoPat);
    except
        on e: Exception do begin
            FErrorGrave := True;
            MsgBoxErr(MSG_PROCESS_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
            Exit;
        end;
	  end;
end;

{-----------------------------------------------------------------------------
  Function Name: GenerarReporteErroresSintaxis
  Author:    lgisuk
  Date Created: 01/07/2005
  Description: Genero el reporte de errores de sintaxis
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
Function  TFConvertirMovimientosATabla.GenerarReporteErroresSintaxis : Boolean;
resourcestring
    MSG_REPORT_ERROR = 'No se pudo generar el reporte de errores de Sintaxis';
    MSG_ERROR        = 'Error';
const
    REPORT_TITLE     = 'TransBank - Procesar Archivos de Movimientos Recibidos';
var
    f : TFRptErroresSintaxis;
begin
    Result := False;
    try
        try
            //muestro el reporte
            f := TFRptErroresSintaxis.Create(Nil);
//            Application.CreateForm(TFRptErroresSintaxis, f);
            Result := f.Inicializar('34', REPORT_TITLE, FTBK_DirectorioErrores, FErrores);
        except
           on e: Exception do MsgBoxErr(MSG_REPORT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
        end;
    finally
        if Assigned(f) then FreeAndNil(f);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnProcesarClick
  Author:    lgisuk
  Date Created: 04/01/2005
  Description: Procesa la respuesta de los Archivos de Movimientos
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------
  Revision 1
  lgisuk
  05/02/07
  Ahora informo cuantos medios de pago seran dados de baja y si el operador
  autoriza proceso el universo
-----------------------------------------------------------------------------
  Revision 2
  lgisuk
  05/02/07
  Cuando muestra el primer cartel informando la cantidad de convenios pat
  adheridos, si el operador decide no continuar se muestra un cartel
  que fue cancelado por el usuario antes de daba un cartel de error
-----------------------------------------------------------------------------
  Revision 3 - SS 907
  Author: Javier De Barbieri
  Date Modification: 21/07/2010
  Actualmente si se encuentra un convenio duplicado se produce una excepci�n,
  pero el proceso continua. A solicitud del cliente se modific� este proceso
  para que se detenga en el caso que se produzca una excepci�n en la funci�n
  GenerarRegistros.}

procedure TFConvertirMovimientosATabla.btnProcesarClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: ObtenerCantidadMediosPagoAEliminar
      Author:    lgisuk
      Date Created: 05/02/2007
      Description: obtengo cuantos medios de pago seran dados de baja porque
                    no vinieron en el archivo de universo.
      Parameters:
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function ObtenerCantidadMediosPagoAEliminar(var Cantidad : Integer) : Boolean;
    resourcestring
        MSG_ERROR = 'No se pudo obtener la cantidad de medios de pago a eliminar';
    begin
        Cantidad := 0;
        try
            Cantidad := QueryGetValueInt(DMconnections.BaseCAC, 'SELECT dbo.UniversoTransbank_ObtenerCantidadMediosPagoAEliminar()');
            Result := true;
        except
           on e: Exception do begin
               MsgBoxErr(MSG_ERROR, e.Message, Self.caption, MB_ICONERROR);
               Result := False;
           end;
        end;
    end;

    {-----------------------------------------------------------------------------
      Function Name: ObtenerCantidadErroresInterfaz
      Author:    lgisuk
      Date Created: 21/06/2005
      Description: Obtengo la cantidad de errores contemplados que se produjeron
                   al procesar el archivo
      Parameters: CodigoOperacion:string
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function ObtenerCantidadErroresInterfaz(CodigoOperacion : Integer;var Cantidad : Integer) : Boolean;
    resourcestring
        MSG_ERROR = 'No se pudo obtener la cantidad de errores de la interfaz';
    begin
        Cantidad := 0;
        try
            Cantidad := QueryGetValueInt(DMconnections.BaseCAC, 'SELECT dbo.ObtenerCantidadErroresInterfaz ('+inttostr(CodigoOperacion)+')');
            Result := true;
        except
           on e: Exception do begin
               MsgBoxErr(MSG_ERROR, e.Message, Self.caption, MB_ICONERROR);
               Result := False;
           end;
        end;
    end;

   {-----------------------------------------------------------------------------
      Function Name: ActualizarLog
      Author:    lgisuk
      Date Created: 01/07/2005
      Description: Actualizo el log al finalizar
      Parameters: CantidadErrores:integer
      Return Value: boolean
   -----------------------------------------------------------------------------}
   Function ActualizarLog(CantidadErrores : Integer) : Boolean;
   Resourcestring
        MSG_ERROR = 'No se pudo Actualizar el log de operaciones';
   Const
        STR_ERRORES = 'Finalizo OK!, Errores Validaci�n: ';
   var
        DescError : String;
   begin
        try
            Result := ActualizarLogOperacionesInterfaseAlFinal
                            (DMConnections.BaseCAC,FCodigoOperacion,STR_ERRORES+ IntToStr(CantidadErrores),DescError);
        except
            on e: Exception do begin
               MsgBoxErr(MSG_ERROR, e.Message, Self.caption, MB_ICONERROR);
               Result := False;
            end;
        end;
   end;

resourcestring
    MSG_PROCESS_CANCEL = 'Proceso cancelado';
    MSG_PROCESS_FINALLY_WITH_ERROR = 'El proceso finaliz� con Errores';
  	MSG_PROCESS_FINALLY_OK = 'Proceso finalizado con �xito';
Const
    STR_OPEN_FILE               = 'Abrir Archivo...';
    STR_CONFIRM                 = 'Confirmar Cantidades...';
    STR_IMPORT                  = 'Importar Registros...';
    STR_PROCESS_RESPUESTAMOVIC  = 'Procesar Respuesta a Movimientos..';
    STR_PROCESS_BASEINSTDECARGO = 'Procesar Base Entera..';
    STR_YOU_WOULD_BE_DARAN_OF_LOSS = 'Se daran de baja ';
    STR_PAYMENT_WISHES_TO_CONTINUE_EQUAL = ' medios de pago, desea continuar igual?';
    STR_PROCESS_NOVEDADESIC     = 'Procesar Novedades..';
var
    Respuesta : Integer;
    CantidadErrores : Integer;
    CantidadAEliminar : Integer;
begin
  	btnProcesar.Enabled := False;
    PnlAvance.visible := True;       //El Panel de Avance es visible durante el procesamiento
    Respuesta := 0;
    KeyPreview := True;
  	try

        //Abro el archivo
        Lblmensaje.Caption := STR_OPEN_FILE;
    	if not AbrirArchivo then begin
            FErrorGrave := True;
            Exit;
        end;

        //El usuario acepta o no continuar con el proceso
        Lblmensaje.Caption := STR_CONFIRM;
        if not ConfirmaCantidades(FLista.Count) then begin
            FCancelar := True; //Modificado en revision 2
            Exit;
        end;

        //Inserto los Registros en la Tabla TBK_Movimientos
        //Modificaci�n SS 907: Termina el proceso en caso de encontrar
        //                      en GenerarRegistro uno ya existente.

        Lblmensaje.Caption := STR_IMPORT;
        //GenerarRegistros(Respuesta);
    		//if Respuesta <> 0 then begin
    		IF ((NOT GenerarRegistros(Respuesta)) OR (Respuesta <> 0)) then begin
            FErrorGrave := True;
            Exit;
        end;

        //Si el archivo es de Repuesta a Movimientos
    	if ExistePalabra(txtOrigen.text,'RESPUESTAMOVIC') then begin
            Lblmensaje.Caption := STR_PROCESS_RESPUESTAMOVIC;
			Application.ProcessMessages;
            //Proceso la respuesta a los Movimientos enviados
      		if not OpProcesarRespuestaAMovimientos then begin
                FErrorGrave := True;
                Exit;
            end;
        end;

        //Si el archivo es de universo
    	if ExistePalabra(txtOrigen.text,'BASEINSTDECARGO') then begin
      		Lblmensaje.Caption := STR_PROCESS_BASEINSTDECARGO;
      		Application.ProcessMessages;

            //Obtengo la cantidad de medios de pago a eliminar
            if not ObtenerCantidadMediosPagoAEliminar(CantidadAEliminar) then begin
                FErrorGrave := True;
                Exit;
            end;

            //informo la cantidad de medios de pago que seran dados de baja, solicito confirmacion.
            if ((CantidadAEliminar <= StrToInt(FTBK_CantidadTopeMedioPagoBaja)) or (MsgBox(STR_YOU_WOULD_BE_DARAN_OF_LOSS + IntToStr(CantidadAEliminar) + STR_PAYMENT_WISHES_TO_CONTINUE_EQUAL, Caption, MB_YESNO + MB_ICONQUESTION) = IDYES)) then begin

                    //si aceptan, Proceso el universo recibido
                	if not OpProcesarBaseEntera then begin
                        FErrorGrave := True;
                        Exit;
                    end;

            end else begin
                    //sino, la operacion fue cancelada por el usuario
                    FCancelar := True;
                    Exit;
            end;


        end;

        //Si el archivo es de novedades
        if ExistePalabra(txtOrigen.text,'NOVEDADESIC') then begin
            Lblmensaje.Caption := STR_PROCESS_NOVEDADESIC;
	      	Application.ProcessMessages;
            //Proceso las Novedades Recibidas
    		if not OpProcesarNovedades then begin
                FErrorGrave := True;
                Exit;
            end;
        end;

        // grabar los errores encontrados                                           //SS_1147_MCA_20150508
        GrabarErrores();                                                            //SS_1147_MCA_20150508

        //Obtengo la cantidad de errores
        ObtenerCantidadErroresInterfaz(FCodigoOperacion, CantidadErrores);

        //Actualizo el log al Final
        ActualizarLog(CantidadErrores);


	finally
        pbProgreso.Position := 0;  //Inicio la Barra de progreso
        PnlAvance.visible := False;  //Oculto el Panel de Avance
		Lblmensaje.Caption := '';

        if FCancelar then begin
            //muestro mensaje que el proceso fue cancelado
            MsgBox(MSG_PROCESS_CANCEL, self.Caption, MB_OK + MB_ICONINFORMATION);
        end else if FErrorGrave then begin
            //muestro mensaje que el proceso finalizo con error
            MsgBox(MSG_PROCESS_FINALLY_WITH_ERROR, self.Caption, MB_OK + MB_ICONINFORMATION);
            //verifico si hubo errores de sintaxis
            if FErrores.Count <> 0 then begin
                //Genero el reporte de error con el report builder
                GenerarReporteErroresSintaxis;
            end;
        end else begin
            //informo que el proceso finalizo con exito
            MsgBox(MSG_PROCESS_FINALLY_OK, self.Caption, MB_OK + MB_ICONINFORMATION);
            //si no hubo errores Genero el Reporte de Error con el Report Builder
            if FErrores.Count > 0 then GenerarReporteErroresSintaxis;
            //Permite mover el archivo que se proceso, a un carpeta definida en parametros generales
            MoverArchivoProcesado(Caption,txtOrigen.Text, FTBK_DirectorioMandatosProcesados);
        end;
        KeyPreview := False;
	end
end;

{-----------------------------------------------------------------------------
  Function Name: FormKeyDown
  Author:    lgisuk
  Date Created: 04/07/2005
  Description: Permito cancelar la operaci�n
  Parameters: Sender: TObject;var Key: Word; Shift: TShiftState
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFConvertirMovimientosATabla.FormKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
begin
    case Key of
        VK_ESCAPE: FCancelar := True;
    else
        FCancelar := False;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: SalirBTNClick
  Author:    lgisuk
  Date Created: 04/07/2005
  Description: Cierro el formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFConvertirMovimientosATabla.btnSalirClick(Sender: TObject);
begin
    close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormCloseQuery
  Author:    lgisuk
  Date Created: 04/07/2005
  Description: impido salir si esta procesando
  Parameters: Sender: TObject;var CanClose: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFConvertirMovimientosATabla.FormCloseQuery(Sender: TObject;var CanClose: Boolean);
begin
    //Permite salir solo si no esta procesando
    CanClose := not PnlAvance.visible;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 04/07/2005
  Description:  lo libero de memoria
  Parameters: Sender: TObject;var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFConvertirMovimientosATabla.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    Action := caFree;

end;

procedure TFConvertirMovimientosATabla.GrabarErrores;                           //SS_1147_MCA_20150508
var                                                                             //SS_1147_MCA_20150508
    i : Integer;                                                                //SS_1147_MCA_20150508
begin                                                                           //SS_1147_MCA_20150508
    for i := 0 to FErrores.Count - 1 do begin                                    //SS_1147_MCA_20150508
        AgregarErrorInterfase( DMConnections.BaseCAC, FCodigoOperacion, FErrores[i], -1); //SS_1147_MCA_20150508
    end;                                                                        //SS_1147_MCA_20150508

end;                                                                            //SS_1147_MCA_20150508


initialization
    FLista   := TStringList.Create;
    FErrores := TStringList.Create;
finalization
    FreeAndNil(Flista);
    FreeAndNil(FErrores);
end.


