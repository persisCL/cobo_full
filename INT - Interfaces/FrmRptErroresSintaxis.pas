{-------------------------------------------------------------------------------
 File Name: FrmRptErroresSintaxis.pas
 Author:    Lgisuk
 Date Created: 01/07/2005
 Language: ES-AR
 Description: Reporte de errores de sintaxis



Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.


-------------------------------------------------------------------------------}
Unit FrmRptErroresSintaxis;

interface

uses
  //reporte
  DMConnection,              //Coneccion a base de datos OP_CAC                 //SS_1147_NDR_20140710
  Util,                      //GoodDir..
  UtilProc,                  //Mensajes
  //general
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ppDB, ppDBPipe, ADODB, UtilRB, ppParameter, ppBands,
  ppCtrls, ppPrnabl, ppClass, ppCache, ppComm, ppRelatv, ppProd, ppReport,
  RBSetup, ppTxPipe, ConstParametrosGenerales;                                            //SS_1147_NDR_20140710

type
  TFRptErroresSintaxis = class(TForm)
    RBIListado: TRBInterface;
    ppReporte: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppImage2: TppImage;
    pptitulo: TppLabel;
    ppLabel6: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppDBText1: TppDBText;
    ppErroresPipeline: TppTextPipeline;
    ppErroresPipelineppField1: TppField;
  private
    { Private declarations }
  public
    Function Inicializar(CodigoModulo, NombreReporte, DirectorioErrores: AnsiString; Errores:TStringList): Boolean;
    { Public declarations }
  end;

var
  FRptErroresSintaxis: TFRptErroresSintaxis;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    lgisuk
  Date Created: 23/06/2005
  Description: Inicializo este formulario
  Parameters: 
  Return Value: Boolean
-----------------------------------------------------------------------------}
Function TFRptErroresSintaxis.Inicializar(CodigoModulo, NombreReporte, DirectorioErrores: AnsiString; Errores:TStringList): Boolean;
Resourcestring
    MSG_ERROR_AL_GENERAR_REPORTE = 'Error al generar el reporte';
    MSG_ERROR = 'Error';
Const
    SINTAXIS_ERROR = 'Errores de Sintaxis';
    FILE_NAME = 'Errores';
    FILE_DATE_FORMAT = 'yyyy-mm-dd';
    FILE_EXTENSION = '.txt';
Var
    ArchivoErrores:string;
    RutaLogo: AnsiString;                                                         //SS_1147_NDR_20140710
begin
    result:=false;
    ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
    try                                                                                                 //SS_1147_NDR_20140710
      ppImage2.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
    except                                                                                              //SS_1147_NDR_20140710
      On E: Exception do begin                                                                          //SS_1147_NDR_20140710
        Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
        MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
        Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
      end;                                                                                              //SS_1147_NDR_20140710
    end;                                                                                                //SS_1147_NDR_20140710
    try
        ArchivoErrores:= GoodDir(DirectorioErrores) + FILE_NAME + CodigoModulo + FormatDateTime ( FILE_DATE_FORMAT , Now ) + FILE_EXTENSION;
        Errores.SaveToFile(ArchivoErrores);
        ppErroresPipeline.FileName := ArchivoErrores;
        pptitulo.Caption:= NombreReporte;
        rbiListado.Caption := SINTAXIS_ERROR;
        RBIListado.Execute;
        Result:=true;
    except
        on E: exception do begin
            MsgBoxErr(MSG_ERROR_AL_GENERAR_REPORTE, E.Message, MSG_ERROR, MB_ICONERROR );
        end;
    end;
end;

end.
