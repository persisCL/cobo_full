object AbmEmpresasDeCobranzaForm: TAbmEmpresasDeCobranzaForm
  Left = 191
  Top = 177
  Caption = 'Empresas de Cobranza'
  ClientHeight = 547
  ClientWidth = 1198
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object AbmToolbar: TAbmToolbar
    Left = 0
    Top = 0
    Width = 1198
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar]
    OnClose = AbmToolbarClose
  end
  object GrupoDatos: TPanel
    Left = 0
    Top = 157
    Width = 1198
    Height = 351
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    TabOrder = 2
    object pnlGeneral: TPanel
      Left = 1
      Top = 1
      Width = 1196
      Height = 138
      Align = alTop
      BevelOuter = bvNone
      ParentBackground = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      object lblNombre: TLabel
        Left = 108
        Top = 13
        Width = 48
        Height = 13
        Caption = 'Nombre:'
        FocusControl = txtNombre
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblRUT: TLabel
        Left = 887
        Top = 13
        Width = 31
        Height = 13
        Caption = 'RUT:'
        FocusControl = txtRUT
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblTipoDeOperacionEmpresasCobranza: TLabel
        Left = 504
        Top = 47
        Width = 108
        Height = 13
        Caption = 'Tipo de operaci'#243'n:'
        FocusControl = txtTipoDeOperacionEmpresasCobranza
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblDescripcion: TLabel
        Left = 590
        Top = 13
        Width = 72
        Height = 13
        Caption = 'Descripci'#243'n:'
        FocusControl = txtDescripcion
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label5: TLabel
        Left = 7
        Top = 79
        Width = 111
        Height = 13
        Caption = 'Comisi'#243'n por aviso:'
        FocusControl = txtComisionPorAviso
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblComisionPorCobranza: TLabel
        Left = 204
        Top = 79
        Width = 133
        Height = 13
        Caption = 'Comisi'#243'n por cobranza:'
        FocusControl = txtComisionPorCobranza
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblAlias: TLabel
        Left = 363
        Top = 13
        Width = 32
        Height = 13
        Caption = 'Alias:'
        FocusControl = txtAlias
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblEdadDeLaDeuda: TLabel
        Left = 420
        Top = 79
        Width = 142
        Height = 13
        Caption = 'Edad de la deuda (d'#237'as):'
        FocusControl = txtEdadDeLaDeuda
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblConvenioClienteTipo: TLabel
        Left = 7
        Top = 46
        Width = 67
        Height = 13
        Caption = 'Personer'#237'a:'
        FocusControl = txtConvenioClienteTipo
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblMontoMinimo: TLabel
        Left = 7
        Top = 113
        Width = 84
        Height = 13
        Caption = 'Monto m'#237'nimo:'
        FocusControl = txtMontoMinimo
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblMontoMaximo: TLabel
        Left = 228
        Top = 113
        Width = 85
        Height = 13
        Caption = 'Monto m'#225'ximo:'
        FocusControl = txtMontoMaximo
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblCapacidadDeProceso: TLabel
        Left = 428
        Top = 113
        Width = 133
        Height = 13
        Caption = 'Capacidad de Proceso:'
        FocusControl = txtCapacidadDeProceso
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label8: TLabel
        Left = 7
        Top = 13
        Width = 37
        Height = 13
        Alignment = taRightJustify
        Caption = 'C'#243'digo:'
        FocusControl = txtCodigo
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblTipoJudicialEmpresasCobranza: TLabel
        Left = 293
        Top = 47
        Width = 38
        Height = 13
        Caption = 'Judicial:'
        FocusControl = txtTipoJudicialEmpresasCobranza
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label1: TLabel
        Left = 175
        Top = 79
        Width = 10
        Height = 13
        Caption = '%'
        FocusControl = txtComisionPorAviso
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 395
        Top = 79
        Width = 10
        Height = 13
        Caption = '%'
        FocusControl = txtComisionPorAviso
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object txtTipoDeOperacionEmpresasCobranza: TVariantComboBox
        Left = 617
        Top = 44
        Width = 145
        Height = 21
        Style = vcsDropDownList
        Color = 16444382
        ItemHeight = 13
        TabOrder = 7
        Items = <>
      end
      object txtAlias: TEdit
        Left = 400
        Top = 10
        Width = 175
        Height = 21
        Color = 16444382
        MaxLength = 200
        TabOrder = 2
      end
      object txtCodigo: TNumericEdit
        Left = 49
        Top = 10
        Width = 50
        Height = 21
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 0
        BlankWhenZero = False
      end
      object txtNombre: TEdit
        Left = 161
        Top = 10
        Width = 190
        Height = 21
        Color = 16444382
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 200
        ParentFont = False
        TabOrder = 1
      end
      object txtDescripcion: TEdit
        Left = 667
        Top = 10
        Width = 200
        Height = 21
        Color = 16444382
        MaxLength = 200
        TabOrder = 3
      end
      object txtRUT: TEdit
        Left = 923
        Top = 10
        Width = 80
        Height = 21
        Color = 16444382
        MaxLength = 10
        TabOrder = 4
        OnKeyPress = txtRUTKeyPress
      end
      object txtEdadDeLaDeuda: TNumericEdit
        Left = 568
        Top = 76
        Width = 50
        Height = 21
        Hint = 
          'Tiempo m'#237'nimo que debe tener una deuda para ser procesada por la' +
          ' Empresa de Cobranza.'
        Color = 16444382
        MaxLength = 40
        TabOrder = 10
        BlankWhenZero = False
      end
      object txtConvenioClienteTipo: TVariantComboBox
        Left = 84
        Top = 43
        Width = 182
        Height = 21
        Style = vcsDropDownList
        Color = 16444382
        ItemHeight = 13
        TabOrder = 5
        Items = <>
      end
      object txtMontoMinimo: TNumericEdit
        Left = 122
        Top = 111
        Width = 90
        Height = 21
        Color = 16444382
        MaxLength = 30
        TabOrder = 11
        Decimals = 2
        BlankWhenZero = False
      end
      object txtMontoMaximo: TNumericEdit
        Left = 321
        Top = 110
        Width = 90
        Height = 21
        Color = 16444382
        MaxLength = 30
        TabOrder = 12
        Decimals = 2
        Value = 1.000000000000000000
        BlankWhenZero = False
      end
      object txtCapacidadDeProceso: TNumericEdit
        Left = 569
        Top = 110
        Width = 50
        Height = 21
        Hint = 
          'Permite definir cuantas deudas puede manejar la empresa a un mis' +
          'mo tiempo.'
        Color = 16444382
        TabOrder = 13
        Value = 1.000000000000000000
        BlankWhenZero = False
      end
      object txtTipoJudicialEmpresasCobranza: TVariantComboBox
        Left = 337
        Top = 44
        Width = 145
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 13
        TabOrder = 6
        Items = <>
      end
      object txtComisionPorAviso: TNumericEdit
        Left = 122
        Top = 76
        Width = 50
        Height = 21
        Color = 16444382
        TabOrder = 8
        Decimals = 2
        BlankWhenZero = False
      end
      object txtComisionPorCobranza: TNumericEdit
        Left = 342
        Top = 76
        Width = 50
        Height = 21
        Color = 16444382
        TabOrder = 9
        Decimals = 2
        BlankWhenZero = False
      end
    end
    object gbMediosDeComunicacion: TGroupBox
      Left = 1
      Top = 139
      Width = 1196
      Height = 76
      Align = alTop
      Caption = ' Medios de contacto '
      TabOrder = 1
      object lblEmail: TLabel
        Left = 84
        Top = 51
        Width = 28
        Height = 13
        Alignment = taRightJustify
        Caption = 'Email:'
        FocusControl = txtEmail
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      inline FrameTelefono1: TFrameTelefono
        Left = 2
        Top = 15
        Width = 1192
        Height = 40
        Align = alTop
        TabOrder = 0
        TabStop = True
        ExplicitLeft = 2
        ExplicitTop = 15
        ExplicitWidth = 1192
        ExplicitHeight = 40
        inherited cbHasta: TComboBox
          Font.Charset = ANSI_CHARSET
          Font.Name = 'Tahoma'
        end
      end
      object txtEmail: TEdit
        Left = 118
        Top = 48
        Width = 288
        Height = 21
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        MaxLength = 100
        ParentFont = False
        TabOrder = 1
      end
    end
    object GBDomicilio: TGroupBox
      Left = 1
      Top = 215
      Width = 1196
      Height = 132
      Align = alTop
      Caption = ' Domicilio '
      TabOrder = 2
      inline FrameDomicilio1: TFrameDomicilio
        Left = 2
        Top = 15
        Width = 1192
        Height = 104
        HorzScrollBar.Visible = False
        VertScrollBar.Visible = False
        Align = alTop
        TabOrder = 0
        TabStop = True
        ExplicitLeft = 2
        ExplicitTop = 15
        ExplicitWidth = 1192
        inherited Pnl_Arriba: TPanel
          Width = 1192
          ExplicitWidth = 1192
        end
        inherited Pnl_Abajo: TPanel
          Top = 79
          Width = 1192
          Align = alBottom
          ExplicitTop = 79
          ExplicitWidth = 1192
        end
        inherited Pnl_Medio: TPanel
          Top = 50
          Width = 1192
          ExplicitTop = 50
          ExplicitWidth = 1192
        end
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 508
    Width = 1198
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object Notebook: TNotebook
      Left = 1001
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TButton
          Left = 116
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 26
          Top = 6
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object DBListDatos: TAbmList
    Left = 0
    Top = 33
    Width = 1198
    Height = 124
    TabStop = True
    TabOrder = 0
    Align = alClient
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'60'#0'C'#243'digo   '
      #0'200'#0'Nombre'
      #0'218'#0'Alias'
      #0'64'#0'Descripci'#243'n')
    Table = spListar
    Style = lbOwnerDrawFixed
    OnClick = DBListDatosClick
    OnProcess = DBListDatosProcess
    OnDrawItem = DBListDatosDrawItem
    OnRefresh = DBListDatosRefresh
    OnInsert = DBListDatosInsert
    OnDelete = DBListDatosDelete
    OnEdit = DBListDatosEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar
  end
  object spListar: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'EmpresasDeCobranza_Listar'
    Parameters = <>
    Left = 384
    Top = 72
  end
  object spEliminar: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'EmpresasDeCobranza_Eliminar'
    Parameters = <>
    Left = 420
    Top = 72
  end
  object spAlmacenar: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'EmpresasDeCobranza_Almacenar'
    Parameters = <>
    Left = 384
    Top = 104
  end
  object spTelefonos_Almacenar: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'Telefonos_Almacenar'
    Parameters = <>
    Left = 421
    Top = 104
  end
  object spEmailsContacto_Almacenar: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'EmailsContacto_Almacenar'
    Parameters = <>
    Left = 456
    Top = 104
  end
  object spActualizarDomicilio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ActualizarDomicilio'
    Parameters = <>
    Left = 488
    Top = 104
  end
end
