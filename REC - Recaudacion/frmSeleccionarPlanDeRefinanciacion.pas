{
    Autor: Ariel Unanue
    Firma: TASK_006_AUN_20170406-CU.COBO.COB.302
    Descripci�n: Lista los planes de refinanciacion que cumplen con el criterio
           pasado en Inicializar.
}
unit frmSeleccionarPlanDeRefinanciacion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DB, ADODB, DbList, Abm_obj, DmiCtrls, ExtCtrls, ListBoxEx,
  DBListEx, DMConnection;

type
  TSeleccionarPlanDeRefinanciacionForm = class(TForm)
    GrupoDatos: TPanel;
    lblNombre: TLabel;
    lblCantidadDeCuotas: TLabel;
    lblPorcentajeCuotaPie: TLabel;
    Label1: TLabel;
    txtCantidadDeCuotas: TNumericEdit;
    txtPorcentajeCuotaPie: TNumericEdit;
    PAbajo: TPanel;
    spListar: TADOStoredProc;
    Panel1: TPanel;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    DBListDatos: TDBListEx;
    DataSource1: TDataSource;
    Label2: TLabel;
    txtNombre: TLabel;
    spRefinanciacionesObtenerFechaVenCuotasParaPlanDeConvenio: TADOStoredProc;
    procedure BtnCancelarClick(Sender: TObject);
    procedure spListarAfterScroll(DataSet: TDataSet);
    procedure BtnAceptarClick(Sender: TObject);
  private
    FCodigoConvenio: Integer;
    FMontoDeuda: Integer;
    FTieneCuotas: boolean;
    FNombrePlan: string;
    { Private declarations }
  public
    { Public declarations }
    CantidadDeCuotas: Integer;
    Cuotas: array of Integer;
    VencimientoCuotas: array of TDate;
    property NombrePlan: string read FNombrePlan;
    function Inicializar(CodigoConvenio: Integer; CodigoTipoCliente: Integer; MontoDeuda: Integer; TieneCuotas: boolean): boolean;
  end;

implementation

uses DB_CRUDCommonProcs, UtilProc;

{$R *.dfm}

{ TSeleccionarPlanDeRefinanciacionForm }

procedure TSeleccionarPlanDeRefinanciacionForm.BtnAceptarClick(Sender: TObject);
resourcestring
    MSG_CONFIRMA_NUEVO_PLAN = '�Est� seguro de seleccionar un nuevo plan? ' + #13 +
                              'Todas las cuotas existentes se borrar�n y se crear�n nuevas en base al plan seleccionado.';
    MSG_CANTIDAD_DE_CUOTAS = 'La cantidad de cuotas debe ser mayor o igual a uno.';
    MSG_PORCENTAJE_CUOTA_PIE = 'El porcentaje cuota pie debe ser entre cero y cien.';
var
    Total, ValorAcumulado, FValorCuota: Integer;
    AplicarCuotas: boolean;
    i: Integer;
begin
    if txtCantidadDeCuotas.ValueInt <= 0 then begin
        MsgBoxBalloon(MSG_CANTIDAD_DE_CUOTAS, Self.Caption, MB_ICONSTOP, txtCantidadDeCuotas);
        Exit;
    end;
    if (txtPorcentajeCuotaPie.Value < 0) or (txtPorcentajeCuotaPie.Value > 100) then begin
        MsgBoxBalloon(MSG_PORCENTAJE_CUOTA_PIE, Self.Caption, MB_ICONSTOP, txtPorcentajeCuotaPie);
        Exit;
    end;

    if FTieneCuotas then begin
        AplicarCuotas := UtilProc.MsgBox(MSG_CONFIRMA_NUEVO_PLAN, Self.Caption, MB_ICONQUESTION or MB_YESNO) = mrYes;
    end else begin
        AplicarCuotas := True;
    end;

    if AplicarCuotas then begin
        FNombrePlan := txtNombre.Caption;
        if (txtCantidadDeCuotas.ValueInt <> spListar.FieldByName('CantidadDeCuotas').AsInteger) or
            (txtPorcentajeCuotaPie.Value <> spListar.FieldByName('PorcentajeCuotaPie').AsFloat) then
            FNombrePlan := FNombrePlan + ' (Modificado)';
        //
        CantidadDeCuotas := txtCantidadDeCuotas.ValueInt;
        SetLength(Cuotas, CantidadDeCuotas);
        if txtPorcentajeCuotaPie.Value > 0.0 then begin
            Cuotas[0] := Round((FMontoDeuda * (txtPorcentajeCuotaPie.Value * 10)) / 1000);
            Total := FMontoDeuda - Cuotas[0];
            FValorCuota := Round(Total / (CantidadDeCuotas - 1));
            ValorAcumulado := Cuotas[0];
            for i := 1 to CantidadDeCuotas - 1 do begin
                ValorAcumulado := ValorAcumulado + FValorCuota;
                Cuotas[i] := FValorCuota;
            end;
        end else begin
            Total := FMontoDeuda;
            FValorCuota := Round(Total / CantidadDeCuotas);
            ValorAcumulado := 0;
            for i := 0 to CantidadDeCuotas - 1 do begin
                ValorAcumulado := ValorAcumulado + FValorCuota;
                Cuotas[i] := FValorCuota;
            end;
        end;
        //por redondeo la �ltima cuota tiene menos valor o m�s valor
        if (ValorAcumulado - FMontoDeuda) > 0 then
            Cuotas[CantidadDeCuotas - 1] := Cuotas[CantidadDeCuotas - 1] - (ValorAcumulado - FMontoDeuda)
        else if (ValorAcumulado - FMontoDeuda < 0) then
            Cuotas[CantidadDeCuotas - 1] := Cuotas[CantidadDeCuotas - 1] + (FMontoDeuda - ValorAcumulado);

        //Obtengo las fechas de Vencimiento para el convenio en base al Grupo de Facturaci�n
        DB_CRUDCommonProcs.AddParameter_Codigo('CodigoConvenio', spRefinanciacionesObtenerFechaVenCuotasParaPlanDeConvenio.Parameters, FCodigoConvenio);
        DB_CRUDCommonProcs.AddParameter(spRefinanciacionesObtenerFechaVenCuotasParaPlanDeConvenio.Parameters, '@CantidadCuotas', ftInteger, pdInput, CantidadDeCuotas);
        spRefinanciacionesObtenerFechaVenCuotasParaPlanDeConvenio.Open;
        spRefinanciacionesObtenerFechaVenCuotasParaPlanDeConvenio.First;
        SetLength(VencimientoCuotas, Length(Cuotas));
        for i:= Low(VencimientoCuotas) to High(VencimientoCuotas) do begin
            VencimientoCuotas[i] := spRefinanciacionesObtenerFechaVenCuotasParaPlanDeConvenio.FieldByName('FechaVencimiento').AsDateTime;
            spRefinanciacionesObtenerFechaVenCuotasParaPlanDeConvenio.Next;
        end;
        spRefinanciacionesObtenerFechaVenCuotasParaPlanDeConvenio.Close;
        //
        ModalResult := mrOk;
    end;
end;

procedure TSeleccionarPlanDeRefinanciacionForm.BtnCancelarClick(
  Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:  aunanue
  Date: 04-04-2017
  Firma: TASK_006_AUN_20170406-CU.COBO.COB.302
  Description: Inicializa la b�squeda de planes de refinanciaci�n que cumpla con el criterio
-----------------------------------------------------------------------------}
function TSeleccionarPlanDeRefinanciacionForm.Inicializar(CodigoConvenio: Integer;
  CodigoTipoCliente: Integer; MontoDeuda: Integer; TieneCuotas: boolean): boolean;
begin
    FCodigoConvenio := CodigoConvenio;
    FMontoDeuda := MontoDeuda;
    FTieneCuotas := TieneCuotas;
    CantidadDeCuotas := 0;
    AddParameter(spListar.Parameters, '@CodigoTipoCliente', ftInteger, pdInput, CodigoTipoCliente);
    AddParameter(spListar.Parameters, '@MontoDeuda', ftFloat, pdInput, MontoDeuda);
    spListar.Open;
    Result := spListar.RecordCount > 0;
end;

procedure TSeleccionarPlanDeRefinanciacionForm.spListarAfterScroll(
  DataSet: TDataSet);
begin
    txtNombre.Caption := spListar.FieldByName('Nombre').AsString;
    txtCantidadDeCuotas.ValueInt := spListar.FieldByName('CantidadDeCuotas').AsInteger;
    txtPorcentajeCuotaPie.Value := spListar.FieldByName('PorcentajeCuotaPie').AsFloat;
end;

end.
