{-----------------------------------------------------------------------------
 File Name: frmPagoComprobantes.pas
 Author:    Modificado por Flamas
 Date Created: 21/12/2004
 Language: ES-AR
 Description:
-----------------------------------------------------------------------------}

unit frmPagoComprobantes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ListBoxEx, DBListEx, StdCtrls, DPSControls, VariantComboBox,
  DmiCtrls, DB, ADODB, DMCOnnection, utilDB, UtilFacturacion, utilProc, buscaClientes,
  PeaTypes, RStrings, DBCtrls, peaProcs, Util, PagoVentanilla;

const
	KEY_F9 	= VK_F9;
type
    TformPagoComprobantes = class(TForm)
    gbPorCliente: TGroupBox;
    lb_CodigoCliente: TLabel;
    Label5: TLabel;
    peNumeroDocumento: TPickEdit;
    cbConvenios: TVariantComboBox;
    btnPagoEfectivo: TDPSButton;
    btnSalir: TDPSButton;
    GroupBox1: TGroupBox;
    Label6: TLabel;
    lblNombre: TLabel;
    lblDomicilio: TLabel;
    Label10: TLabel;
    gbDatosComprobante: TGroupBox;
    Label2: TLabel;
    lblFecha: TLabel;
    lblFechaVencimiento: TLabel;
    Label7: TLabel;
    Label1: TLabel;
    lblTotalAPagar: TLabel;
    Label12: TLabel;
    ObtenerComprobantesImpagos: TADOStoredProc;
    cbComprobantes: TComboBox;
    Label3: TLabel;
    Label4: TLabel;
    Label9: TLabel;
    edNumeroComprobante: TNumericEdit;
    edCodigoBarras: TEdit;
    lblPagado: TLabel;
    Label11: TLabel;
    cbTiposComprobantes: TVariantComboBox;
    procedure peNumeroDocumentoChange(Sender: TObject);
    procedure cbConveniosChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure peNumeroDocumentoButtonClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnPagoEfectivoClick(Sender: TObject);
    procedure cbComprobantesChange(Sender: TObject);
    procedure edCodigoBarrasChange(Sender: TObject);
    procedure edCodigoBarrasKeyPress(Sender: TObject; var Key: Char);
    procedure cbTiposComprobantesChange(Sender: TObject);
    procedure edNumeroComprobanteChange(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    FTipoComprobante: AnsiString;
    FNumeroComprobante: Int64;
    FUltimaBusqueda: TBusquedaCliente;
    FTotalApagar: Double;
    procedure MostrarDatosCliente(CodigoCLiente: Integer);
    function CargarComprobantes : boolean;
    procedure MostrarDatosComprobante;
    procedure CargarTiposComprobantes;
    procedure LimpiarCampos;
    procedure LimpiarDetalles;
  public
    { Public declarations }
    function Inicializar: Boolean; overload;
    function Inicializar(TipoComprobante: AnsiString; NumeroComprobante: Int64): Boolean; Overload;
  end;

var
  formPagoComprobantes: TformPagoComprobantes;

implementation

{$R *.dfm}
{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    flamas
  Date Created: 21/12/2004
  Description: Inicializa el Form - Carga los tipos de Comprobantes
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TformPagoComprobantes.Inicializar: Boolean;
begin
	// Por ahora lo cargamos a mano
    lblNombre.Caption := '';
    lblDomicilio.Caption := '';
    lblPagado.Caption := '';
    CargarTiposComprobantes;
    MostrarDatosComprobante;
    Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    flamas
  Date Created: 21/12/2004
  Description: Inicializa el Form - Carga los tipos de Comprobantes y busca
  				el comprobante seleccionado
  Parameters: TipoComprobante: AnsiString; NumeroComprobante: Int64
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TformPagoComprobantes.Inicializar(TipoComprobante: AnsiString; NumeroComprobante: Int64): Boolean;
begin
	// Por ahora lo cargamos a mano
    lblNombre.Caption := '';
    lblDomicilio.Caption := '';
    lblPagado.Caption := '';
	CargarTiposComprobantes;
    cbTiposComprobantes.Value := TipoComprobante;
    edNumeroComprobante.Value := NumeroComprobante;
    Result := CargarComprobantes;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarTiposComprobantes;
  Author:    flamas
  Date Created: 22/01/2005
  Description: Carga los tipos de Comprobantes
  Parameters: TipoComprobante: AnsiString; 
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure TformPagoComprobantes.CargarTiposComprobantes;
resourcestring
	MSG_ALL_INVOICES = 'Todos';
begin
    cbTiposComprobantes.Items.Clear;
    cbTiposComprobantes.Items.Add(MSG_ALL_INVOICES,  null);
    cbTiposComprobantes.Items.Add(DescriTipoComprobante(TC_NOTA_COBRO), TC_NOTA_COBRO);
    cbTiposComprobantes.Items.Add(DescriTipoComprobante(TC_FACTURA), TC_FACTURA);
    cbTiposComprobantes.Items.Add(DescriTipoComprobante(TC_NOTA_DEBITO), TC_NOTA_DEBITO);
    cbTiposComprobantes.Value := TC_NOTA_COBRO;
end;

procedure TformPagoComprobantes.FormCreate(Sender: TObject);
begin
    CenterForm(Self);
end;

{-----------------------------------------------------------------------------
  Function Name: peNumeroDocumentoChange
  Author:    flamas
  Date Created: 20/12/2004
  Description: Actualiza los Convenios y Comprobantes Impagos de un cliente determinado
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformPagoComprobantes.peNumeroDocumentoChange(Sender: TObject);
Var
    CodigoCliente: Integer;
begin
    if ActiveControl <> Sender then Exit;

    edNumeroComprobante.Clear;
    CodigoCliente := QueryGetValueInt(DMConnections.BaseCAC, Format(
      'SELECT CodigoPersona FROM Personas WHERE CodigoDocumento = ''RUT'' ' +
      'AND NumeroDocumento = ''%s''', [peNumeroDocumento.Text]));
    if CodigoCliente > 0 then
        CargarConveniosRUT(DMConnections.BaseCAC, cbConvenios, 'RUT', peNumeroDocumento.Text, False, 0)
    else begin
    	cbConvenios.Clear;
    end;
    if cbConvenios.Items.Count > 0 then begin
        cbConvenios.ItemIndex := 0;
    end;

    MostrarDatosCliente(CodigoCliente);

    CargarComprobantes;
end;

{-----------------------------------------------------------------------------
  Function Name: cbConveniosChange
  Author:    flamas
  Date Created: 21/12/2004
  Description: Procesa un Cambio de Convenio - Trae los comprobantes impagos
  				del dicho Convenio
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformPagoComprobantes.cbConveniosChange(Sender: TObject);
begin
    if ActiveControl <> Sender then Exit;
    Cargarcomprobantes;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarComprobantes
  Author:    flamas
  Date Created: 20/12/2004
  Description: Carga los comprobantes impagos de un Convenio determinado
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
function TformPagoComprobantes.CargarComprobantes : boolean;
resourcestring
    MSG_NOT_PAID = 'No';
Var
   Comprobante: TComprobante;
begin
    ObtenerComprobantesImpagos.Close;
    ObtenerComprobantesImpagos.Parameters.ParamByName('@CodigoConvenio').Value := cbConvenios.Value;
    ObtenerComprobantesImpagos.Parameters.ParamByName('@TipoComprobante').Value := cbTiposComprobantes.Value;
    ObtenerComprobantesImpagos.Parameters.ParamByName('@NumeroComprobante').Value :=
        											iif( edNumeroComprobante.Text <> '', edNumeroComprobante.Value, null );

    FTipoComprobante := iif ( cbTiposComprobantes.Value = null, '', cbTiposComprobantes.Value );

    ObtenerComprobantesImpagos.Open;

    // Cargamos la lista de comprobantes.
    ObtenerComprobantesImpagos.disableControls;
    cbComprobantes.Items.Clear;
    try
        while not ObtenerComprobantesImpagos.eof do begin
            Comprobante := TComprobante.Create;
            Comprobante.CodigoCliente		:= ObtenerComprobantesImpagos.FieldByName('CodigoPersona').AsInteger;
            Comprobante.TipoComprobante     := ObtenerComprobantesImpagos.FieldByName('TipoComprobante').AsString;
            Comprobante.NumeroComprobante   := ObtenerComprobantesImpagos.FieldByName('NumeroComprobante').AsInteger;
            Comprobante.FechaEmision        := ObtenerComprobantesImpagos.FieldByName('FechaEmision').AsDateTime;
            Comprobante.FechaVencimiento    := ObtenerComprobantesImpagos.FieldByName('FechaVencimiento').asDateTime;
            Comprobante.TotalComprobante    := ObtenerComprobantesImpagos.FieldByName('TotalComprobante').AsFloat;
            Comprobante.TotalAPagar         := ObtenerComprobantesImpagos.FieldByName('TotalAPagar').AsFloat;
            Comprobante.Pagado				:= ( ObtenerComprobantesImpagos.FieldByName( 'EstadoPago' ).AsString = 'P' );
            cbComprobantes.AddItem(ObtenerComprobantesImpagos.FieldByName('DescriComprobante').AsString, Comprobante);

            if (TComprobante(cbComprobantes.Items.Objects[cbComprobantes.Items.Count - 1]).TipoComprobante = FTipoComprobante)
          	  and (TComprobante(cbComprobantes.Items.Objects[cbComprobantes.Items.Count - 1]).NumeroComprobante = FNumeroComprobante) then
				cbComprobantes.ItemIndex := cbComprobantes.Items.Count - 1;

            ObtenerComprobantesImpagos.next;
        end;

    	LimpiarDetalles;
        if cbComprobantes.Items.Count = 0 then begin
            Result := false;
            Exit;
        end else begin
            cbComprobantes.ItemIndex := 0;
            lblPagado.Caption := MSG_NOT_PAID;
        end;
        MostrarDatosCliente( TComprobante(cbComprobantes.Items.Objects[cbComprobantes.itemIndex]).CodigoCliente);
        MostrarDatosComprobante;
        Result := true;
    finally
        ObtenerComprobantesImpagos.EnableControls;
        cbComprobantesChange(cbComprobantes);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: MostrarDatosCliente
  Author:    flamas
  Date Created: 20/12/2004
  Description:  Muestra los datos del Cliente
  Parameters: CodigoCliente: Integer
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformPagoComprobantes.MostrarDatosCliente(CodigoCliente: Integer);
begin
    // Cargamos la informaci�n del cliente.
    lblNombre.Caption := QueryGetValue(DMConnections.BaseCAC, Format(
      'SELECT dbo.ObtenerNombrePersona(%d)', [CodigoCliente]));
    lblDomicilio.Caption := QueryGetValue(DMConnections.BaseCAC, Format(
      'SELECT dbo.ObtenerDescripcionDomicilioPrincipalPersona(%d)', [CodigoCliente]));
end;

{-----------------------------------------------------------------------------
  Function Name: MostrarDatosComprobante
  Author:    flamas
  Date Created: 20/12/2004
  Description: Muestra los datos del Comprobante
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformPagoComprobantes.MostrarDatosComprobante;
resourcestring
	MSG_YES = 'Si';
    MSG_NO = 'No';
begin
    if cbComprobantes.Items.Count > 0 then begin
        lblFecha.Caption            	:= FormatDateTime('dd/mm/yyyy', (TComprobante(cbComprobantes.Items.Objects[cbComprobantes.itemIndex]).FechaEmision));
        lblFechaVencimiento.Caption 	:= FormatDateTime('dd/mm/yyyy', (TComprobante(cbComprobantes.Items.Objects[cbComprobantes.itemIndex]).FechaVencimiento));
        lblTotalAPagar.Caption      	:= FormatFloat(FORMATO_IMPORTE , (TComprobante(cbComprobantes.Items.Objects[cbComprobantes.itemIndex]).TotalAPagar));
        FTotalApagar := Round(TComprobante(cbComprobantes.Items.Objects[cbComprobantes.itemIndex]).TotalAPagar);
        lblPagado.Caption := iif( TComprobante(cbComprobantes.Items.Objects[cbComprobantes.itemIndex]).Pagado, MSG_YES, MSG_NO );
        btnPagoEfectivo.Enabled := not TComprobante(cbComprobantes.Items.Objects[cbComprobantes.itemIndex]).Pagado;
        FNumeroComprobante :=  TComprobante(cbComprobantes.Items.Objects[cbComprobantes.itemIndex]).NumeroComprobante;
        edNumeroComprobante.ValueInt := FNumeroComprobante;
    end else LimpiarDetalles;
end;

{-----------------------------------------------------------------------------
  Function Name: LimpiarDetalles;
  Author:    flamas
  Date Created: 26/01/2005
  Description: Borra los detalles del Comprobante
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformPagoComprobantes.LimpiarDetalles;
begin
	lblFecha.Caption            := '';
    lblFechaVencimiento.Caption	:= '';
    lblTotalAPagar.Caption      := '';
    lblPagado.Caption           := '';
    lblPagado.Caption 			:= '';
    btnPagoEfectivo.Enabled     := False;
end;

{-----------------------------------------------------------------------------
  Function Name: peNumeroDocumentoButtonClick
  Author:    flamas
  Date Created: 21/12/2004
  Description: Procesa un cambio en el RUT - Muestra los comprobantes pendientes
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformPagoComprobantes.peNumeroDocumentoButtonClick(Sender: TObject);
var
    f: TFormBuscaClientes;
begin
    Application.CreateForm(TFormBuscaClientes,F);
	if F.Inicializa(FUltimaBusqueda) then begin
        F.ShowModal;
        if F.ModalResult = idOk then begin
            FUltimaBusqueda := F.UltimaBusqueda;
            peNumeroDocumento.Text := f.Persona.NumeroDocumento;
        end;
	end;
    F.free;
end;

{-----------------------------------------------------------------------------
  Function Name: cbComprobantesChange
  Author:    flamas
  Date Created: 21/12/2004
  Description: Procesa un cambio de comprobante y muestra los datos
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformPagoComprobantes.cbComprobantesChange(Sender: TObject);
begin
    if ActiveControl <> Sender then Exit;
    MostrarDatosComprobante;
end;

{-----------------------------------------------------------------------------
  Function Name: cbTiposComprobantesChange
  Author:    flamas
  Date Created: 21/12/2004
  Description: Procesa un cambio de tipo de comprobante y muestra los datos
  				del comprobante
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformPagoComprobantes.cbTiposComprobantesChange(Sender: TObject);
begin
	edCodigoBarras.Enabled := ( cbTiposComprobantes.Value = TC_NOTA_COBRO );
   if ActiveControl <> Sender then Exit;

   if ( edNumeroComprobante.Text <> '' ) then edNumeroComprobanteChange( Sender )
   else if ( edCodigoBarras.Text <> '' ) then edCodigoBarrasChange( Sender )
   else CargarComprobantes;
end;

{-----------------------------------------------------------------------------
  Function Name: edNumeroComprobanteChange
  Author:    flamas
  Date Created: 21/12/2004
  Description: Procesa un cambio de comprobante y muestra los datos del comprobante
  Parameters: Sender: TObject
  Return Value: None
  -----------------------------------------------------------------------------}
procedure TformPagoComprobantes.edNumeroComprobanteChange(Sender: TObject);
begin
    if ActiveControl <> Sender then Exit;
    CargarComprobantes;
    edCodigoBarras.Text := '';
end;

{-----------------------------------------------------------------------------
  Function Name: edCodigoBarrasChange
  Author:    flamas
  Date Created: 21/12/2004
  Description: Procesa un cambio del c�digo de barras y muestra los datos
  				del comprobante
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformPagoComprobantes.edCodigoBarrasChange(Sender: TObject);
resourcestring
	MSG_ALREADY_PAID = 'Este comprobante est� pago';
begin
    if ActiveControl <> Sender then Exit;
	if Length(edCodigoBarras.Text) = 29 then begin
    	cbTiposComprobantes.Value := TC_NOTA_COBRO;
    	edNumeroComprobante.Value := StrToInt( Copy( edCodigoBarras.Text, 21, Length(edCodigoBarras.Text)));
        CargarComprobantes;
    end
    else begin
        edNumeroComprobante.Clear;
        cbConvenios.ItemIndex           := -1;
        cbConvenios.Value               := '';
        peNumeroDocumento.Clear;
        lblFecha.Caption            	:= '';
        lblFechaVencimiento.Caption 	:= '';
        lblTotalAPagar.Caption      	:= '';
        lblPagado.Caption               := '';
        lblNombre.Caption               := '';
        lblDomicilio.Caption            := '';
        btnPagoEfectivo.Enabled     	:= False;
    end;
end;

procedure TformPagoComprobantes.edCodigoBarrasKeyPress(Sender: TObject; var Key: Char);
begin
	if  not (Key in [#8, #9, #13, '0'..'9']) then Key := #0;
    if Key = #13 then begin
        if edCodigobarras.Text <> '' then begin
            edCodigoBarras.Text :=  stringreplace( format('%29.0d',[ival(edCodigoBarras.Text)]), ' ' , '0',[rfReplaceAll]);
            btnPagoEfectivo.SetFocus ;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnPagoEfectivoClick
  Author:    flamas
  Date Created: 21/12/2004
  Description: Paga un comprobante en Efectivo
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformPagoComprobantes.btnPagoEfectivoClick(Sender: TObject);
var
    f: TfrmPagoVentanilla;
begin
    Application.CreateForm(TfrmPagoVentanilla, f);
    if f.Inicializar(FTipoComprobante, FNumeroComprobante, FTotalApagar) then f.ShowModal;
        if f.ModalResult = mrOk then LimpiarCampos;
    f.Free;
end;

procedure TformPagoComprobantes.btnSalirClick(Sender: TObject);
begin
    close;
end;

procedure TformPagoComprobantes.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    action := caFree;
end;

{-----------------------------------------------------------------------------
  Procedure: TformPagoComprobantes.LimpiarCampos
  Author:    gcasais
  Date:      19-Ene-2005
  Arguments: None
  Result:    None
-----------------------------------------------------------------------------}

procedure TformPagoComprobantes.LimpiarCampos;
begin
    edNumeroComprobante.Clear;
    peNumeroDocumento.Clear;
    edCodigoBarras.Clear;
    cbConvenios.Clear;
    cbComprobantes.Clear;
    LimpiarDetalles;
    if edCodigoBarras.Enabled then Activecontrol := edCodigoBarras
    else ActiveControl := edNumeroComprobante;
end;

procedure TformPagoComprobantes.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if Key = KEY_F9 then btnPagoEfectivo.Click;
end;

end.
