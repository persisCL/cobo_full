object RefinanciacionSeleccionarRepLegalCNForm: TRefinanciacionSeleccionarRepLegalCNForm
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = ' Seleccionar / Entrar Representante Legal CN - %s'
  ClientHeight = 465
  ClientWidth = 773
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 757
    Height = 412
    Align = alCustom
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = '  Seleccione un Representante Legal  '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    DesignSize = (
      757
      412)
    object DBListEx1: TDBListEx
      Left = 7
      Top = 20
      Width = 742
      Height = 141
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 100
          Header.Caption = 'Rut'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          IsLink = False
          FieldName = 'NumeroDocumento'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'Nombre'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          IsLink = False
          FieldName = 'Nombre'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 175
          Header.Caption = 'Apellido'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          IsLink = False
          FieldName = 'Apellido'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'Apellido Materno'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          IsLink = False
          FieldName = 'ApellidoMaterno'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'Tipo Refinanciaci'#243'n'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          IsLink = False
          FieldName = 'TipoMedioPagoDesc'
        end>
      DataSource = dsRepresentantesCN
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
      OnDblClick = DBListEx1DblClick
    end
    object GroupBox2: TGroupBox
      Left = 7
      Top = 192
      Width = 742
      Height = 211
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = '  Entrar Representante Legal  '
      TabOrder = 2
      DesignSize = (
        742
        211)
      object Label3: TLabel
        Left = 18
        Top = 17
        Width = 21
        Height = 13
        Caption = 'Rut:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label1: TLabel
        Left = 168
        Top = 17
        Width = 41
        Height = 13
        Caption = 'Nombre:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label2: TLabel
        Left = 299
        Top = 17
        Width = 41
        Height = 13
        Caption = 'Apellido:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label4: TLabel
        Left = 477
        Top = 17
        Width = 84
        Height = 13
        Caption = 'Apellido Materno:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object edtNombre: TEdit
        Left = 166
        Top = 36
        Width = 130
        Height = 21
        CharCase = ecUpperCase
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        MaxLength = 60
        ParentFont = False
        TabOrder = 0
      end
      object edtApellido: TEdit
        Left = 298
        Top = 36
        Width = 173
        Height = 21
        CharCase = ecUpperCase
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        MaxLength = 60
        ParentFont = False
        TabOrder = 1
      end
      object edtApellidoMaterno: TEdit
        Left = 473
        Top = 36
        Width = 173
        Height = 21
        CharCase = ecUpperCase
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        MaxLength = 30
        ParentFont = False
        TabOrder = 2
      end
      object peRut: TPickEdit
        Left = 17
        Top = 36
        Width = 143
        Height = 21
        Hint = ' Seleccionar el Cliente a Refinanciar '
        Enabled = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        OnChange = peRutChange
        OnKeyPress = peRutKeyPress
        EditorStyle = bteTextEdit
        OnButtonClick = peRutButtonClick
      end
      object GroupBox3: TGroupBox
        Left = 7
        Top = 70
        Width = 727
        Height = 131
        Anchors = [akLeft, akTop, akRight, akBottom]
        Caption = '  Datos Empresa Recaudadora  '
        TabOrder = 3
        object Label6: TLabel
          Left = 35
          Top = 24
          Width = 41
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nombre:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label7: TLabel
          Left = 29
          Top = 47
          Width = 47
          Height = 13
          Alignment = taRightJustify
          Caption = 'Direcci'#243'n:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label8: TLabel
          Left = 39
          Top = 70
          Width = 37
          Height = 13
          Alignment = taRightJustify
          Caption = 'Regi'#243'n:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label9: TLabel
          Left = 33
          Top = 93
          Width = 43
          Height = 13
          Alignment = taRightJustify
          Caption = 'Comuna:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object edtEmpresaNombre: TEdit
          Left = 84
          Top = 21
          Width = 177
          Height = 21
          CharCase = ecUpperCase
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object edtEmpresaDireccion: TEdit
          Left = 84
          Top = 44
          Width = 321
          Height = 21
          CharCase = ecUpperCase
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
        object vcbEmpresaRegion: TVariantComboBox
          Left = 84
          Top = 67
          Width = 221
          Height = 21
          Style = vcsDropDownList
          DroppedWidth = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ItemHeight = 13
          ParentFont = False
          TabOrder = 2
          OnChange = vcbEmpresaRegionChange
          Items = <>
        end
        object vcbEmpresaComuna: TVariantComboBox
          Left = 84
          Top = 90
          Width = 221
          Height = 21
          Style = vcsDropDownList
          DroppedWidth = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ItemHeight = 13
          ParentFont = False
          TabOrder = 3
          Items = <>
        end
      end
    end
    object chbTodos: TCheckBox
      Left = 7
      Top = 167
      Width = 194
      Height = 17
      Caption = 'Ver todos los Representantes'
      TabOrder = 1
      OnClick = chbTodosClick
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 425
    Width = 773
    Height = 40
    Align = alBottom
    Color = 14408667
    ParentBackground = False
    TabOrder = 1
    DesignSize = (
      773
      40)
    object btnCancelar: TButton
      Left = 690
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Cancelar'
      TabOrder = 1
      OnClick = btnCancelarClick
    end
    object btnAceptar: TButton
      Left = 611
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Aceptar'
      TabOrder = 0
      OnClick = btnAceptarClick
    end
  end
  object spObtenerRefinanciacionRepresentantesCN: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerRefinanciacionRepresentantesCN'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@CodigoTipoMedioPago'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 464
    Top = 80
  end
  object dsRepresentantesCN: TDataSource
    DataSet = cdsRepresentantesCN
    Left = 304
    Top = 120
  end
  object cdsRepresentantesCN: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspObtenerRefinanciacionRepresentantesCN'
    Left = 288
    Top = 104
    object cdsRepresentantesCNCodigoRepresentante: TAutoIncField
      FieldName = 'CodigoRepresentante'
      ReadOnly = True
    end
    object cdsRepresentantesCNApellido: TStringField
      FieldName = 'Apellido'
      FixedChar = True
      Size = 60
    end
    object cdsRepresentantesCNApellidoMaterno: TStringField
      FieldName = 'ApellidoMaterno'
      FixedChar = True
      Size = 30
    end
    object cdsRepresentantesCNNombre: TStringField
      FieldName = 'Nombre'
      FixedChar = True
      Size = 60
    end
    object cdsRepresentantesCNCodigoDocumento: TStringField
      FieldName = 'CodigoDocumento'
      FixedChar = True
      Size = 4
    end
    object cdsRepresentantesCNNumeroDocumento: TStringField
      FieldName = 'NumeroDocumento'
      FixedChar = True
      Size = 11
    end
    object cdsRepresentantesCNCodigoTipoMedioPago: TSmallintField
      FieldName = 'CodigoTipoMedioPago'
    end
    object cdsRepresentantesCNTipoMedioPagoDesc: TStringField
      FieldName = 'TipoMedioPagoDesc'
      FixedChar = True
      Size = 30
    end
    object cdsRepresentantesCNEmpresa: TStringField
      FieldName = 'Empresa'
      Size = 50
    end
    object cdsRepresentantesCNDireccion: TStringField
      FieldName = 'Direccion'
      Size = 75
    end
    object cdsRepresentantesCNCodigoPais: TStringField
      FieldName = 'CodigoPais'
      FixedChar = True
      Size = 3
    end
    object cdsRepresentantesCNCodigoRegion: TStringField
      FieldName = 'CodigoRegion'
      FixedChar = True
      Size = 3
    end
    object cdsRepresentantesCNCodigoComuna: TStringField
      FieldName = 'CodigoComuna'
      FixedChar = True
      Size = 3
    end
  end
  object dspObtenerRefinanciacionRepresentantesCN: TDataSetProvider
    DataSet = spObtenerRefinanciacionRepresentantesCN
    Left = 424
    Top = 96
  end
  object spObtenerRefinanciacionRepresentanteCN: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerRefinanciacionRepresentantesCN'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@CodigoTipoMedioPago'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 128
    Top = 72
  end
  object spActualizarRefinanciacionRepresentantesCN: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarRefinanciacionRepresentantesCN'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRepresentante'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Apellido'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@ApellidoMaterno'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@Nombre'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@CodigoTipoMedioPago'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Empresa'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@Direccion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 75
        Value = Null
      end
      item
        Name = '@CodigoPais'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoRegion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoComuna'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 112
    Top = 120
  end
end
