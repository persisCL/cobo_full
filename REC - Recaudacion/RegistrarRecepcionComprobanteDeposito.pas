unit RegistrarRecepcionComprobanteDeposito;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ListBoxEx, DBListEx, ImgList, ComCtrls, ToolWin, ExtCtrls,
  RegistroDineroEntregadoEmpresaTransporte, DB, UtilProc;

type
  TfrmRegistrarRecepcionComprobanteDeposito = class(TForm)
    pnlOpciones: TPanel;
    tlbBotonera: TToolBar;
    btnSalir: TToolButton;
    btn1: TToolButton;
    btnAgregar: TToolButton;
    btnEliminar: TToolButton;
    btnEditar: TToolButton;
    btn2: TToolButton;
    btnImprimir: TToolButton;
    btnBuscar: TToolButton;
    ilActivos: TImageList;
    pnlGuardar: TPanel;
    btnAceptar: TButton;
    btnCancelar: TButton;
    dblRecibos: TDBListEx;
    ilCheck: TImageList;
    procedure dblRecibosDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dblRecibosLinkClick(Sender: TCustomDBListEx;
      Column: TDBListExColumn);
    procedure btnAceptarClick(Sender: TObject);
  private
    { Private declarations }
    oRegistros : TRegistroDineroTransporte;
    chequeados: Integer;
    FUsuario: string;
    FCodigoPuntoVenta: Integer;
    procedure CargarRegistros();
  public
    { Public declarations }
    function Inicializar(CodigoPuntoVenta:Integer; Usuario: string): boolean;
  end;

var
  frmRegistrarRecepcionComprobanteDeposito: TfrmRegistrarRecepcionComprobanteDeposito;

implementation

{$R *.dfm}

function TfrmRegistrarRecepcionComprobanteDeposito.Inicializar(CodigoPuntoVenta:Integer; Usuario: string): Boolean;
begin
    Result := false;
                  
    if CodigoPuntoVenta = -1 then
    begin
        MsgBox('Configure el Punto de Venta', 'Error de configuración', MB_OK);
        Exit;
    end;

    FUsuario := Usuario;
    FCodigoPuntoVenta := CodigoPuntoVenta;
    CargarRegistros();
    chequeados := 0;
    Result := True;
end;

procedure TfrmRegistrarRecepcionComprobanteDeposito.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    FreeAndNil(oRegistros);
    dblRecibos.DataSource.Free;
    Action := caFree;
end;

procedure TfrmRegistrarRecepcionComprobanteDeposito.btnSalirClick(Sender: TObject);
begin
    Close;
end;


procedure TfrmRegistrarRecepcionComprobanteDeposito.CargarRegistros();
begin
    try

        if not Assigned(oRegistros) then
            oRegistros := TRegistroDineroTransporte.Create;

        if not Assigned(dblRecibos.DataSource) then
            dblRecibos.DataSource := TDataSource.Create(nil);

        dblRecibos.DataSource.DataSet := oRegistros.Obtener(False);

    except
        on e: Exception do
            MsgBoxErr('Error cargando registros', e.Message, 'Error de carga de datos', MB_ICONERROR);
    end;
end;


procedure TfrmRegistrarRecepcionComprobanteDeposito.dblRecibosDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
var
    bmp: TBitMap;
begin
    if Column.FieldName='RecibidoPorBanco' then
    begin
        if Text = 'False' then
            Text := 'NO'
        else
            Text := 'SI';
    end;
end;



procedure TfrmRegistrarRecepcionComprobanteDeposito.dblRecibosLinkClick(Sender: TCustomDBListEx; Column: TDBListExColumn);
begin
    if Column.Header.Caption = 'Recibido' then
    begin
        if Assigned(oRegistros) and Assigned(oRegistros.ClientDataSet) and (oRegistros.ClientDataSet.RecordCount > 0) then
        begin
            if not oRegistros.RecibidoPorBanco then
            begin
                oRegistros.RecibidoPorBanco := True;
                chequeados := chequeados + 1;
            end
            else
            begin
                oRegistros.RecibidoPorBanco := False;
                chequeados := chequeados - 1;
            end;
            dblRecibos.Refresh;
            
            if chequeados > 0 then
            begin
                btnAceptar.Enabled := true;
                btnCancelar.Enabled := true;
            end
            else
            begin
                btnAceptar.Enabled := false;
                btnCancelar.Enabled := false;
            end;
        end;                                                 
    end;
end;


procedure TfrmRegistrarRecepcionComprobanteDeposito.btnAceptarClick(Sender: TObject);
begin
    try
        try
        oRegistros.ClientDataSet.DisableControls;
        oRegistros.ClientDataSet.First;
        while not oRegistros.ClientDataSet.Eof do
        begin
            if oRegistros.RecibidoPorBanco then
            begin
                oRegistros.Modificar(oRegistros.Fecha, oRegistros.CodigoBanco, oRegistros.Folio,
                                    oRegistros.NumCuentaCorriente, oRegistros.Monto, oRegistros.EmpresaTransporteValores, FCodigoPuntoVenta, True, FUsuario, oRegistros.ID);
            end;
            oRegistros.ClientDataSet.Next;
        end;

        CargarRegistros();
        MsgBox('Registros actualizados con exito', 'Actualización', MB_OK);
        except
            on e: Exception do
                MsgBoxErr('Error guardando cambios', e.Message, 'Error', MB_ICONERROR);
        end;   
    finally
        oRegistros.ClientDataSet.EnableControls;
    end;
end;

procedure TfrmRegistrarRecepcionComprobanteDeposito.btnCancelarClick(Sender: TObject);
begin
    if Assigned(oRegistros) and Assigned(oRegistros.ClientDataSet) and (oRegistros.ClientDataSet.RecordCount > 0) then
    begin
        oRegistros.ClientDataSet.First;
        while not oRegistros.ClientDataSet.eof do
        begin
            oRegistros.RecibidoPorBanco := False;
            oRegistros.ClientDataSet.Next;
        end;
        oRegistros.ClientDataSet.First;
        dblRecibos.Refresh;
    end;
end;

end.
