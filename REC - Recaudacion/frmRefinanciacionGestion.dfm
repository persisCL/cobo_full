object RefinanciacionGestionForm: TRefinanciacionGestionForm
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = ' Gesti'#243'n Refinanciaci'#243'n'
  ClientHeight = 742
  ClientWidth = 948
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnlDatosPrincipales: TPanel
    Left = 0
    Top = 0
    Width = 948
    Height = 428
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      948
      428)
    object GroupBox1: TGroupBox
      Left = 5
      Top = 6
      Width = 938
      Height = 417
      Anchors = [akLeft, akTop, akRight]
      Caption = '  Datos Principales Refinanciaci'#243'n  '
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentBackground = False
      ParentColor = False
      ParentFont = False
      TabOrder = 0
      DesignSize = (
        938
        417)
      object Label1: TLabel
        Left = 24
        Top = 23
        Width = 24
        Height = 13
        Caption = 'Tipo:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label2: TLabel
        Left = 214
        Top = 23
        Width = 41
        Height = 13
        Caption = 'N'#250'mero:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label4: TLabel
        Left = 343
        Top = 23
        Width = 33
        Height = 13
        Caption = 'Fecha:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblNumRefinanciacion: TLabel
        Left = 259
        Top = 23
        Width = 64
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NumRefi'
      end
      object Label5: TLabel
        Left = 554
        Top = 23
        Width = 40
        Height = 13
        Alignment = taRightJustify
        Anchors = [akRight, akBottom]
        Caption = 'Estado :'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblEstado: TLabel
        Left = 596
        Top = 23
        Width = 93
        Height = 13
        Alignment = taRightJustify
        Anchors = [akRight, akBottom]
        AutoSize = False
        Caption = '<Estado>'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 711
        Top = 23
        Width = 117
        Height = 13
        Alignment = taRightJustify
        Anchors = [akRight, akBottom]
        Caption = 'Importe Refinanciaci'#243'n :'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblTotalRefinanciado2: TLabel
        Left = 845
        Top = 23
        Width = 80
        Height = 13
        Alignment = taRightJustify
        Anchors = [akRight, akBottom]
        AutoSize = False
        Caption = '$ 0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label8: TLabel
        Left = 744
        Top = 37
        Width = 84
        Height = 13
        Alignment = taRightJustify
        Anchors = [akRight, akBottom]
        Caption = 'Importe Pagado :'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label10: TLabel
        Left = 743
        Top = 67
        Width = 85
        Height = 13
        Alignment = taRightJustify
        Anchors = [akRight, akBottom]
        Caption = 'Importe Vencido :'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ExplicitTop = 69
      end
      object lblDeudaPagada: TLabel
        Left = 845
        Top = 37
        Width = 80
        Height = 13
        Alignment = taRightJustify
        Anchors = [akRight, akBottom]
        AutoSize = False
        Caption = '$ 0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblDeudaVencida: TLabel
        Left = 845
        Top = 67
        Width = 80
        Height = 13
        Alignment = taRightJustify
        Anchors = [akRight, akBottom]
        AutoSize = False
        Caption = '$ 0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitTop = 69
      end
      object Label12: TLabel
        Left = 732
        Top = 52
        Width = 96
        Height = 13
        Alignment = taRightJustify
        Anchors = [akRight, akBottom]
        Caption = 'Importe Pendiente :'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblDeudaPendiente: TLabel
        Left = 845
        Top = 52
        Width = 80
        Height = 13
        Alignment = taRightJustify
        Anchors = [akRight, akBottom]
        AutoSize = False
        Caption = '$ 0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblProtestada: TLabel
        Left = 626
        Top = 38
        Width = 63
        Height = 13
        Caption = 'Protestada'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = False
      end
      object vcbTipoRefinanciacion: TVariantComboBox
        Left = 54
        Top = 20
        Width = 145
        Height = 21
        Hint = ' Seleccionar Tipo de Refinanciaci'#243'n '
        Style = vcsDropDownList
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ItemHeight = 13
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnChange = vcbTipoRefinanciacionChange
        Items = <
          item
            Caption = 'Avenimiento'
            Value = 38
          end
          item
            Caption = 'Repactaci'#243'n'
            Value = 36
          end>
      end
      object GroupBox2: TGroupBox
        Left = 8
        Top = 82
        Width = 923
        Height = 197
        Anchors = [akLeft, akRight, akBottom]
        Caption = '  Datos Cliente  '
        TabOrder = 3
        DesignSize = (
          923
          197)
        object Label3: TLabel
          Left = 21
          Top = 23
          Width = 21
          Height = 13
          Caption = 'Rut:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object lblNombreCli: TLabel
          Left = 48
          Top = 45
          Width = 70
          Height = 13
          Caption = 'lblNombreCli'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label7: TLabel
          Left = 283
          Top = 23
          Width = 49
          Height = 13
          Caption = 'Convenio:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object lblPersoneria: TLabel
          Left = 127
          Top = 45
          Width = 61
          Height = 13
          Caption = 'lblPersoneria'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object lblConcesionaria: TLabel
          Left = 552
          Top = 21
          Width = 92
          Height = 13
          Caption = 'lblConcesionaria'
        end
        object lblEnListaAmarilla: TLabel
          Left = 701
          Top = 21
          Width = 208
          Height = 14
          Alignment = taCenter
          AutoSize = False
          Color = clYellow
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          Transparent = False
          Visible = False
        end
        object peRut: TPickEdit
          Left = 48
          Top = 18
          Width = 143
          Height = 21
          Hint = ' Seleccionar el Cliente a Refinanciar '
          Enabled = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnChange = peRutChange
          OnKeyPress = peRutKeyPress
          EditorStyle = bteTextEdit
          OnButtonClick = peRutButtonClick
        end
        object vcbConvenios: TVariantComboBox
          Left = 338
          Top = 18
          Width = 208
          Height = 19
          Style = vcsOwnerDrawFixed
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ItemHeight = 13
          ParentFont = False
          TabOrder = 1
          OnChange = vcbConveniosChange
          OnDrawItem = vcbConveniosDrawItem
          Items = <>
        end
        object GroupBox4: TGroupBox
          Left = 8
          Top = 151
          Width = 908
          Height = 39
          Anchors = [akLeft, akRight]
          Caption = '  Representante Legal  '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          DesignSize = (
            908
            39)
          object lblNombreRepLegCli: TLabel
            Left = 228
            Top = 18
            Width = 112
            Height = 13
            Caption = 'lblNombreRepLegCli'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object lblRutRepLegCliDesc: TLabel
            Left = 20
            Top = 18
            Width = 21
            Height = 13
            Caption = 'Rut:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object lblRutRepLegCli: TLabel
            Left = 47
            Top = 18
            Width = 88
            Height = 13
            Caption = 'lblRutRepLegCli'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object lblNombreRepLegCliDesc: TLabel
            Left = 179
            Top = 18
            Width = 41
            Height = 13
            Caption = 'Nombre:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object btnCambiarRepLegCli: TButton
            Left = 822
            Top = 12
            Width = 80
            Height = 21
            Anchors = [akRight, akBottom]
            Caption = 'Cambiar'
            TabOrder = 0
            OnClick = btnCambiarRepLegCliClick
          end
        end
        object btnBuscarRut: TButton
          Left = 196
          Top = 18
          Width = 61
          Height = 21
          Caption = 'Buscar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
          OnClick = btnBuscarRutClick
        end
        object GroupBox7: TGroupBox
          Left = 8
          Top = 60
          Width = 908
          Height = 52
          Anchors = [akLeft, akRight, akBottom]
          Caption = '  Domicilios  '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 4
          DesignSize = (
            908
            52)
          object lblDomicilioCliDesc: TLabel
            Left = 23
            Top = 17
            Width = 71
            Height = 13
            Caption = 'Domicilio Part.:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object lbldomicilioFactDesc: TLabel
            Left = 22
            Top = 32
            Width = 72
            Height = 13
            Caption = 'Domicilio Fact.:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object lblDomicilioFact: TLabel
            Left = 98
            Top = 32
            Width = 71
            Height = 13
            Caption = 'lblDomicilioFact'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object lblDomicilioCli: TLabel
            Left = 98
            Top = 17
            Width = 77
            Height = 13
            Caption = 'lblDomicilioCli'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object btnRestaurarDomicilio: TButton
            Left = 739
            Top = 12
            Width = 80
            Height = 21
            Anchors = [akRight, akBottom]
            Caption = 'Restaurar'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            OnClick = btnRestaurarDomicilioClick
          end
          object btnCambiarDomicilio: TButton
            Left = 821
            Top = 12
            Width = 80
            Height = 21
            Anchors = [akRight, akBottom]
            Caption = 'Cambiar'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            OnClick = btnCambiarDomicilioClick
          end
        end
        object GroupBox8: TGroupBox
          Left = 8
          Top = 112
          Width = 908
          Height = 39
          Anchors = [akLeft, akRight, akBottom]
          Caption = '  Tel'#233'fonos  '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 5
          DesignSize = (
            908
            39)
          object Label13: TLabel
            Left = 20
            Top = 18
            Width = 51
            Height = 13
            Caption = 'Tel'#233'fonos:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object lblTelefonos: TLabel
            Left = 78
            Top = 18
            Width = 68
            Height = 13
            Caption = 'lblTelefonos'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object btnRestaurarTelefono: TButton
            Left = 739
            Top = 12
            Width = 80
            Height = 21
            Anchors = [akRight, akBottom]
            Caption = 'Restaurar'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            OnClick = btnRestaurarTelefonoClick
          end
          object btnCambiarTelefono: TButton
            Left = 822
            Top = 12
            Width = 80
            Height = 21
            Anchors = [akRight, akBottom]
            Caption = 'Cambiar'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            OnClick = btnCambiarTelefonoClick
          end
        end
      end
      object dedtFechaRefi: TDateEdit
        Left = 385
        Top = 20
        Width = 104
        Height = 21
        AutoSelect = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        OnChange = dedtFechaRefiChange
        Date = -693594.000000000000000000
      end
      object GroupBox3: TGroupBox
        Left = 8
        Top = 279
        Width = 923
        Height = 39
        Anchors = [akLeft, akRight]
        Caption = '  Representante Legal CN  '
        TabOrder = 4
        DesignSize = (
          923
          39)
        object Label14: TLabel
          Left = 20
          Top = 18
          Width = 21
          Height = 13
          Caption = 'Rut:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object lblRutRepLegCN: TLabel
          Left = 47
          Top = 18
          Width = 89
          Height = 13
          Caption = 'lblRutRepLegCN'
        end
        object lblNombreRepLegCNDesc: TLabel
          Left = 179
          Top = 18
          Width = 41
          Height = 13
          Caption = 'Nombre:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object lblNombreRepLegCN: TLabel
          Left = 228
          Top = 18
          Width = 113
          Height = 13
          Caption = 'lblNombreRepLegCN'
        end
        object btnCambiarRepLegCN: TButton
          Left = 831
          Top = 12
          Width = 80
          Height = 21
          Anchors = [akRight, akBottom]
          Caption = 'Cambiar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          OnClick = btnCambiarRepLegCNClick
        end
      end
      object GroupBox5: TGroupBox
        Left = 8
        Top = 318
        Width = 923
        Height = 92
        Anchors = [akLeft, akRight]
        Caption = '  Otros Datos  '
        TabOrder = 5
        object lblCausaRol: TLabel
          Left = 20
          Top = 67
          Width = 67
          Height = 13
          Caption = 'Causa Rol N'#186':'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object lblGastoNotarial: TLabel
          Left = 498
          Top = 67
          Width = 72
          Height = 13
          Caption = 'Gasto Notarial:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object lblDemanda: TLabel
          Left = 181
          Top = 67
          Width = 90
          Height = 13
          Caption = 'Importe Demanda:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object lblDatosGastoNotarial: TLabel
          Left = 655
          Top = 67
          Width = 123
          Height = 13
          Caption = 'lblDatosGastoNotarial'
        end
        object nedtGastoNotarial: TNumericEdit
          Left = 576
          Top = 64
          Width = 68
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
          OnChange = nedtGastoNotarialChange
        end
        object gbDatosJPL: TGroupBox
          Left = 8
          Top = 14
          Width = 908
          Height = 46
          Caption = '  Datos Juzgado Polic'#237'a Local  '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          object lblJPL_Nro: TLabel
            Left = 20
            Top = 20
            Width = 59
            Height = 13
            Caption = 'Juzgado N'#176':'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object lblJPL_Region: TLabel
            Left = 154
            Top = 20
            Width = 37
            Height = 13
            Caption = 'Regi'#243'n:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object lblJPL_Comuna: TLabel
            Left = 400
            Top = 20
            Width = 43
            Height = 13
            Caption = 'Comuna:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object nedtJPLNro: TNumericEdit
            Left = 84
            Top = 17
            Width = 51
            Height = 21
            TabOrder = 0
          end
          object vcbJPLRegion: TVariantComboBox
            Left = 197
            Top = 17
            Width = 185
            Height = 21
            Style = vcsDropDownList
            ItemHeight = 13
            TabOrder = 1
            OnChange = vcbJPLRegionChange
            Items = <>
          end
          object vcbJPLComuna: TVariantComboBox
            Left = 449
            Top = 17
            Width = 185
            Height = 21
            Style = vcsDropDownList
            ItemHeight = 13
            TabOrder = 2
            Items = <>
          end
        end
        object edtCausaRol: TEdit
          Left = 92
          Top = 64
          Width = 72
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          MaxLength = 20
          ParentFont = False
          TabOrder = 1
        end
        object nedtDemanda: TNumericEdit
          Left = 277
          Top = 64
          Width = 104
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = False
          TabOrder = 2
        end
        object btnSugerirImporteDemanda: TButton
          Left = 385
          Top = 64
          Width = 94
          Height = 21
          Caption = ' Sugerir Importe'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 4
          OnClick = btnSugerirImporteDemandaClick
        end
      end
      object cbNotificacion: TCheckBox
        Left = 54
        Top = 44
        Width = 158
        Height = 17
        Hint = ' Marcar si es Avenimiento con Notificaci'#243'n '
        Caption = 'Avenimiento con Notificaci'#243'n'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
      end
    end
  end
  object pnlAcciones: TPanel
    Left = 0
    Top = 702
    Width = 948
    Height = 40
    Align = alBottom
    Anchors = []
    Color = 14408667
    ParentBackground = False
    TabOrder = 2
    DesignSize = (
      948
      40)
    object btnGrabar: TButton
      Left = 791
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Grabar'
      TabOrder = 0
      OnClick = btnGrabarClick
    end
    object btnCancelar: TButton
      Left = 868
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Cancelar'
      TabOrder = 1
      OnClick = btnCancelarClick
    end
    object btnImprimir: TButton
      Left = 8
      Top = 6
      Width = 75
      Height = 25
      Caption = 'Imprimir'
      TabOrder = 2
      OnClick = btnImprimirClick
    end
    object btnValidar: TButton
      Left = 331
      Top = 6
      Width = 103
      Height = 25
      Caption = 'Validar Datos'
      TabOrder = 3
      OnClick = btnValidarClick
    end
    object btnImprimirOriginal: TButton
      Left = 205
      Top = 6
      Width = 116
      Height = 25
      Caption = 'Imprimir Doc. Original'
      TabOrder = 4
      OnClick = btnImprimirOriginalClick
    end
    object btnImprimirDefinitiva: TButton
      Left = 85
      Top = 6
      Width = 116
      Height = 25
      Caption = 'Impresi'#243'n Definitiva'
      TabOrder = 5
      OnClick = btnImprimirDefinitivaClick
    end
  end
  object pnlDatosDetalle: TPanel
    Left = 0
    Top = 428
    Width = 948
    Height = 274
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      948
      274)
    object GroupBox6: TGroupBox
      Left = 5
      Top = 1
      Width = 938
      Height = 272
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = '  Detalle Refinanciaci'#243'n  '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      DesignSize = (
        938
        272)
      object PageControl1: TPageControl
        Left = 8
        Top = 18
        Width = 923
        Height = 247
        ActivePage = tbsCuotas
        Anchors = [akLeft, akTop, akRight, akBottom]
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        HotTrack = True
        ParentFont = False
        TabOrder = 0
        object tbsComprobantes: TTabSheet
          Caption = ' Comprobantes Refinanciados '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel3: TPanel
            Left = 0
            Top = 0
            Width = 915
            Height = 34
            Align = alTop
            BevelInner = bvRaised
            BevelOuter = bvLowered
            Color = 14408667
            ParentBackground = False
            TabOrder = 0
            object btnEliminarComprobante: TButton
              Left = 82
              Top = 5
              Width = 75
              Height = 25
              Caption = 'Eliminar'
              TabOrder = 1
              OnClick = btnEliminarComprobanteClick
            end
            object btnAgregarComprobantes: TButton
              Left = 5
              Top = 5
              Width = 75
              Height = 25
              Caption = 'A'#241'adir'
              TabOrder = 0
              OnClick = btnAgregarComprobantesClick
            end
          end
          object Panel8: TPanel
            Left = 0
            Top = 189
            Width = 915
            Height = 30
            Align = alBottom
            BevelInner = bvRaised
            BevelOuter = bvLowered
            Color = 14408667
            ParentBackground = False
            TabOrder = 1
            DesignSize = (
              915
              30)
            object Label9: TLabel
              Left = 495
              Top = 9
              Width = 135
              Height = 13
              Anchors = [akRight, akBottom]
              Caption = 'Comprobantes Cancelados: '
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              ExplicitLeft = 287
            end
            object Label11: TLabel
              Left = 705
              Top = 9
              Width = 114
              Height = 13
              Anchors = [akRight, akBottom]
              Caption = 'Importe Refinanciaci'#243'n:'
              ExplicitLeft = 497
            end
            object lblTotalRefinanciado: TLabel
              Left = 825
              Top = 9
              Width = 80
              Height = 13
              Alignment = taRightJustify
              Anchors = [akRight, akBottom]
              AutoSize = False
              Caption = '$ 0'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              ExplicitLeft = 617
            end
            object lblCantidadComprobantes: TLabel
              Left = 635
              Top = 9
              Width = 42
              Height = 13
              Alignment = taRightJustify
              Anchors = [akRight, akBottom]
              AutoSize = False
              Caption = '0'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              ExplicitLeft = 427
            end
          end
          object Panel1: TPanel
            Left = 0
            Top = 34
            Width = 915
            Height = 155
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 2
            object PageControl2: TPageControl
              Left = 538
              Top = 0
              Width = 377
              Height = 155
              ActivePage = tbsDetalleComprobante
              Align = alRight
              MultiLine = True
              TabOrder = 0
              TabPosition = tpBottom
              object tbsDetalleComprobante: TTabSheet
                Caption = '  Detalle Comprobante  '
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object DBListEx2: TDBListEx
                  Left = 0
                  Top = 0
                  Width = 369
                  Height = 129
                  Align = alClient
                  BorderStyle = bsSingle
                  Columns = <
                    item
                      Alignment = taLeftJustify
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      Width = 250
                      Header.Caption = 'Descripci'#243'n'
                      Header.Font.Charset = DEFAULT_CHARSET
                      Header.Font.Color = clWindowText
                      Header.Font.Height = -11
                      Header.Font.Name = 'Tahoma'
                      Header.Font.Style = [fsBold]
                      IsLink = False
                      FieldName = 'Descripcion'
                    end
                    item
                      Alignment = taRightJustify
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      Width = 95
                      Header.Caption = 'Importe'
                      Header.Font.Charset = DEFAULT_CHARSET
                      Header.Font.Color = clWindowText
                      Header.Font.Height = -11
                      Header.Font.Name = 'Tahoma'
                      Header.Font.Style = [fsBold]
                      Header.Alignment = taRightJustify
                      IsLink = False
                      FieldName = 'DescImporte'
                    end>
                  DataSource = dsDetalleComprobanteRefinanciado
                  DragReorder = True
                  ParentColor = False
                  TabOrder = 0
                  TabStop = True
                  OnDrawText = DBListEx2DrawText
                end
              end
            end
            object DBListEx1: TDBListEx
              Left = 0
              Top = 0
              Width = 538
              Height = 155
              Align = alClient
              BorderStyle = bsSingle
              Columns = <
                item
                  Alignment = taCenter
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Width = 95
                  Header.Caption = 'Fecha Emisi'#243'n'
                  Header.Font.Charset = DEFAULT_CHARSET
                  Header.Font.Color = clWindowText
                  Header.Font.Height = -11
                  Header.Font.Name = 'Tahoma'
                  Header.Font.Style = [fsBold]
                  Header.Alignment = taCenter
                  IsLink = False
                  FieldName = 'FechaEmision'
                end
                item
                  Alignment = taCenter
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Width = 85
                  Header.Caption = 'Vencimiento'
                  Header.Font.Charset = DEFAULT_CHARSET
                  Header.Font.Color = clWindowText
                  Header.Font.Height = -11
                  Header.Font.Name = 'Tahoma'
                  Header.Font.Style = [fsBold]
                  Header.Alignment = taCenter
                  IsLink = False
                  FieldName = 'FechaVencimiento'
                end
                item
                  Alignment = taLeftJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Width = 175
                  Header.Caption = 'Comprobante'
                  Header.Font.Charset = DEFAULT_CHARSET
                  Header.Font.Color = clWindowText
                  Header.Font.Height = -11
                  Header.Font.Name = 'Tahoma'
                  Header.Font.Style = [fsBold]
                  IsLink = False
                  FieldName = 'DescriComprobante'
                end
                item
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Width = 80
                  Header.Caption = 'Saldo Pendiente'
                  Header.Font.Charset = DEFAULT_CHARSET
                  Header.Font.Color = clWindowText
                  Header.Font.Height = -11
                  Header.Font.Name = 'Tahoma'
                  Header.Font.Style = [fsBold]
                  Header.Alignment = taRightJustify
                  IsLink = False
                  FieldName = 'SaldoPendienteDescri'
                end
                item
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Width = 80
                  Header.Caption = 'Saldo Refinanciado'
                  Header.Font.Charset = DEFAULT_CHARSET
                  Header.Font.Color = clWindowText
                  Header.Font.Height = -11
                  Header.Font.Name = 'Tahoma'
                  Header.Font.Style = [fsBold]
                  Header.Alignment = taRightJustify
                  IsLink = False
                  FieldName = 'SaldoRefinanciadoDescri'
                end>
              DataSource = dsComprobantesRefinanciados
              DragReorder = True
              ParentColor = False
              TabOrder = 1
              TabStop = True
              OnDrawBackground = DBListEx1DrawBackground
              OnDrawText = DBListEx1DrawText
            end
          end
        end
        object tbsCuotas: TTabSheet
          Caption = ' Cuotas '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ImageIndex = 1
          ParentFont = False
          object Panel4: TPanel
            Left = 0
            Top = 0
            Width = 915
            Height = 34
            Align = alTop
            BevelInner = bvRaised
            BevelOuter = bvLowered
            Color = 14408667
            ParentBackground = False
            TabOrder = 0
            DesignSize = (
              915
              34)
            object lblNombrePlan: TLabel
              Left = 633
              Top = 10
              Width = 67
              Height = 13
              Anchors = [akLeft, akTop, akRight]
              Caption = 'lblNombrePlan'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object btnEditarCuota: TButton
              Left = 82
              Top = 5
              Width = 75
              Height = 25
              Caption = 'Editar'
              TabOrder = 1
              OnClick = btnEditarCuotaClick
            end
            object btnAgregarCuota: TButton
              Left = 4
              Top = 5
              Width = 75
              Height = 25
              Caption = 'A'#241'adir'
              TabOrder = 0
              OnClick = btnAgregarCuotaClick
            end
            object btnEliminarCuota: TButton
              Left = 159
              Top = 5
              Width = 75
              Height = 25
              Caption = 'Eliminar'
              TabOrder = 2
              OnClick = btnEliminarCuotaClick
            end
            object btnSugerirImportes: TButton
              Left = 808
              Top = 5
              Width = 97
              Height = 25
              Anchors = [akRight, akBottom]
              Caption = 'Sugerir Importes'
              TabOrder = 3
              OnClick = btnSugerirImportesClick
            end
            object btnHistorial: TButton
              Left = 322
              Top = 5
              Width = 96
              Height = 25
              Caption = 'Historial Estados'
              TabOrder = 4
              OnClick = btnHistorialClick
            end
            object btnAnularCuota: TButton
              Left = 236
              Top = 5
              Width = 75
              Height = 25
              Caption = 'Anular'
              TabOrder = 5
              OnClick = btnAnularCuotaClick
            end
            object btnHistorialPagos: TButton
              Left = 420
              Top = 5
              Width = 96
              Height = 25
              Caption = 'Historial Pagos'
              TabOrder = 6
              OnClick = btnHistorialPagosClick
            end
            object btnDefinirPlan: TButton
              Left = 528
              Top = 5
              Width = 96
              Height = 25
              Caption = 'Definir Plan'
              TabOrder = 7
              OnClick = btnDefinirPlanClick
            end
          end
          object DBListEx3: TDBListEx
            Left = 0
            Top = 34
            Width = 915
            Height = 155
            Align = alClient
            BorderStyle = bsSingle
            Columns = <
              item
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 30
                Header.Caption = 'ID'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -11
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = [fsBold]
                Header.Alignment = taRightJustify
                IsLink = False
                FieldName = 'CodigoCuota'
              end
              item
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                Width = 35
                Header.Caption = 'Ant.'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -11
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = [fsBold]
                Header.Alignment = taRightJustify
                IsLink = False
                FieldName = 'CodigoCuotaAntecesora'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 120
                Header.Caption = 'Forma de Pago'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -11
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = [fsBold]
                IsLink = False
                FieldName = 'DescripcionFormaPago'
              end
              item
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 35
                Header.Caption = 'Nro.'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -11
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = [fsBold]
                Header.Alignment = taRightJustify
                IsLink = False
                FieldName = 'NumeroCuota'
              end
              item
                Alignment = taCenter
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 75
                Header.Caption = 'Fecha'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -11
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = [fsBold]
                Header.Alignment = taCenter
                Sorting = csAscending
                IsLink = False
                OnHeaderClick = DBListEx3Columns3HeaderClick
                FieldName = 'FechaCuota'
              end
              item
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 100
                Header.Caption = 'Importe'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -11
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = [fsBold]
                Header.Alignment = taRightJustify
                IsLink = False
                FieldName = 'Importe'
              end
              item
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 100
                Header.Caption = 'Saldo'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -11
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = [fsBold]
                Header.Alignment = taRightJustify
                IsLink = False
                FieldName = 'Saldo'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 115
                Header.Caption = 'Estado'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -11
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = [fsBold]
                IsLink = False
                FieldName = 'DescripcionEstadoCuota'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 90
                Header.Caption = 'Rut'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -11
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = [fsBold]
                IsLink = False
                FieldName = 'TitularNumeroDocumento'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 150
                Header.Caption = 'Titular'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -11
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = [fsBold]
                IsLink = False
                FieldName = 'TitularNombre'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 120
                Header.Caption = 'Nro. Doc. / Confirm.'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -11
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = [fsBold]
                IsLink = False
                FieldName = 'DocumentoNumero'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 175
                Header.Caption = 'Banco'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -11
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = [fsBold]
                IsLink = False
                FieldName = 'DescripcionBanco'
              end
              item
                Alignment = taLeftJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 175
                Header.Caption = 'Tipo Tarjeta'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -11
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = [fsBold]
                IsLink = False
                FieldName = 'DescripcionTarjeta'
              end
              item
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Width = 100
                Header.Caption = 'Cuotas Tarjeta'
                Header.Font.Charset = DEFAULT_CHARSET
                Header.Font.Color = clWindowText
                Header.Font.Height = -11
                Header.Font.Name = 'Tahoma'
                Header.Font.Style = [fsBold]
                Header.Alignment = taRightJustify
                IsLink = False
                FieldName = 'TarjetaCuotas'
              end>
            DataSource = dsCuotas
            DragReorder = True
            ParentColor = False
            TabOrder = 1
            TabStop = True
            OnDblClick = DBListEx3DblClick
            OnDrawBackground = DBListEx3DrawBackground
            OnDrawText = DBListEx3DrawText
          end
          object Panel7: TPanel
            Left = 0
            Top = 189
            Width = 915
            Height = 30
            Align = alBottom
            BevelInner = bvRaised
            BevelOuter = bvLowered
            Color = 14408667
            ParentBackground = False
            TabOrder = 2
            DesignSize = (
              915
              30)
            object Label18: TLabel
              Left = 453
              Top = 9
              Width = 38
              Height = 13
              Anchors = [akRight, akBottom]
              Caption = 'Cuotas:'
              ExplicitLeft = 406
            end
            object lblCuotasCantidad: TLabel
              Left = 486
              Top = 9
              Width = 36
              Height = 13
              Alignment = taRightJustify
              Anchors = [akRight, akBottom]
              AutoSize = False
              Caption = '0'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              ExplicitLeft = 439
            end
            object Label20: TLabel
              Left = 757
              Top = 9
              Width = 69
              Height = 13
              Anchors = [akRight, akBottom]
              Caption = 'Importe Total:'
              ExplicitLeft = 710
            end
            object Label21: TLabel
              Left = 559
              Top = 9
              Width = 81
              Height = 13
              Anchors = [akRight, akBottom]
              Caption = 'Importe Pagado:'
              ExplicitLeft = 512
            end
            object lblDeudaPagada2: TLabel
              Left = 652
              Top = 9
              Width = 80
              Height = 13
              Alignment = taRightJustify
              Anchors = [akRight, akBottom]
              AutoSize = False
              Caption = '$ 0'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              ExplicitLeft = 605
            end
            object lblCuotasTotal: TLabel
              Left = 827
              Top = 9
              Width = 80
              Height = 13
              Alignment = taRightJustify
              Anchors = [akRight, akBottom]
              AutoSize = False
              Caption = '$ 0'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              ExplicitLeft = 780
            end
          end
        end
      end
    end
  end
  object spDetalleComprobante: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerListadoCargosFacturaAImprimir'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end>
    Left = 608
    Top = 496
  end
  object cdsComprobantesRefinanciados: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'FechaEmision'
        DataType = ftDateTime
      end
      item
        Name = 'FechaVencimiento'
        DataType = ftDateTime
      end
      item
        Name = 'TipoComprobante'
        Attributes = [faFixed]
        DataType = ftString
        Size = 2
      end
      item
        Name = 'NumeroComprobante'
        DataType = ftLargeint
      end
      item
        Name = 'DescriComprobante'
        DataType = ftString
        Size = 78
      end
      item
        Name = 'SaldoPendienteDescri'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'TotalComprobanteDescri'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'DescriAPagar'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'SaldoPendienteComprobante'
        DataType = ftLargeint
      end
      item
        Name = 'SaldoRefinanciado'
        DataType = ftLargeint
      end
      item
        Name = 'SaldoRefinanciadoDescri'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'TotalComprobante'
        DataType = ftLargeint
      end
      item
        Name = 'DescriComprobanteFiscal'
        DataType = ftString
        Size = 63
      end>
    IndexDefs = <>
    Params = <>
    ProviderName = 'dspObtenerRefinanciacionComprobantes'
    StoreDefs = True
    AfterScroll = cdsComprobantesRefinanciadosAfterScroll
    Left = 608
    Top = 280
    object cdsComprobantesRefinanciadosFechaEmision: TDateTimeField
      FieldName = 'FechaEmision'
    end
    object cdsComprobantesRefinanciadosFechaVencimiento: TDateTimeField
      FieldName = 'FechaVencimiento'
    end
    object cdsComprobantesRefinanciadosTipoComprobante: TStringField
      FieldName = 'TipoComprobante'
      FixedChar = True
      Size = 2
    end
    object cdsComprobantesRefinanciadosNumeroComprobante: TLargeintField
      FieldName = 'NumeroComprobante'
    end
    object cdsComprobantesRefinanciadosDescriComprobante: TStringField
      FieldName = 'DescriComprobante'
      Size = 78
    end
    object cdsComprobantesRefinanciadosSaldoPendienteDescri: TStringField
      FieldName = 'SaldoPendienteDescri'
    end
    object cdsComprobantesRefinanciadosTotalComprobanteDescri: TStringField
      FieldName = 'TotalComprobanteDescri'
    end
    object cdsComprobantesRefinanciadosDescriAPagar: TStringField
      FieldName = 'DescriAPagar'
    end
    object cdsComprobantesRefinanciadosSaldoPendienteComprobante: TLargeintField
      FieldName = 'SaldoPendienteComprobante'
    end
    object cdsComprobantesRefinanciadosSaldoRefinanciado: TLargeintField
      FieldName = 'SaldoRefinanciado'
    end
    object cdsComprobantesRefinanciadosSaldoRefinanciadoDescri: TStringField
      FieldName = 'SaldoRefinanciadoDescri'
    end
  end
  object dsComprobantesRefinanciados: TDataSource
    DataSet = cdsComprobantesRefinanciados
    Left = 592
    Top = 296
  end
  object cdsDetalleComprobanteRefinanciado: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspDetalleComprobante'
    Left = 552
    Top = 528
  end
  object dspDetalleComprobante: TDataSetProvider
    DataSet = spDetalleComprobante
    Left = 584
    Top = 512
  end
  object dsDetalleComprobanteRefinanciado: TDataSource
    DataSet = cdsDetalleComprobanteRefinanciado
    Left = 520
    Top = 536
  end
  object cdsPatentesPrecautoria: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoPatente'
        DataType = ftSmallint
      end
      item
        Name = 'Patente'
        Attributes = [faFixed]
        DataType = ftString
        Size = 10
      end
      item
        Name = 'MarcaModeloColor'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'Convenio'
        DataType = ftString
        Size = 80
      end
      item
        Name = 'CodigoEstadoCuenta'
        DataType = ftSmallint
      end
      item
        Name = 'UsuarioCreacion'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'FechaCreacion'
        DataType = ftDateTime
      end
      item
        Name = 'UsuarioModificacion'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'FechaModificacion'
        DataType = ftDateTime
      end>
    IndexDefs = <>
    Params = <>
    ProviderName = 'dspObtenerRefinanciacionPatentesPrecautoria'
    StoreDefs = True
    Left = 320
    Top = 552
    object cdsPatentesPrecautoriaPatente: TStringField
      FieldName = 'Patente'
      FixedChar = True
      Size = 10
    end
    object cdsPatentesPrecautoriaMarcaModeloColor: TStringField
      FieldName = 'MarcaModeloColor'
      Size = 100
    end
    object cdsPatentesPrecautoriaConvenio: TStringField
      FieldName = 'Convenio'
      Size = 80
    end
    object cdsPatentesPrecautoriaCodigoEstadoCuenta: TSmallintField
      FieldName = 'CodigoEstadoCuenta'
    end
  end
  object dsPatentesPrecautoria: TDataSource
    DataSet = cdsPatentesPrecautoria
    Left = 288
    Top = 568
  end
  object cdsCuotas: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoCuota'
        DataType = ftSmallint
      end
      item
        Name = 'CodigoEstadoCuota'
        DataType = ftSmallint
      end
      item
        Name = 'DescripcionEstadoCuota'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'CodigoFormaPago'
        DataType = ftSmallint
      end
      item
        Name = 'DescripcionFormaPago'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'TitularNombre'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'TitularTipoDocumento'
        Attributes = [faFixed]
        DataType = ftString
        Size = 4
      end
      item
        Name = 'TitularNumeroDocumento'
        DataType = ftString
        Size = 11
      end
      item
        Name = 'CodigoBanco'
        DataType = ftInteger
      end
      item
        Name = 'DescripcionBanco'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'DocumentoNumero'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'FechaCuota'
        DataType = ftDateTime
      end
      item
        Name = 'CodigoTarjeta'
        DataType = ftInteger
      end
      item
        Name = 'DescripcionTarjeta'
        Attributes = [faFixed]
        DataType = ftString
        Size = 50
      end
      item
        Name = 'TarjetaCuotas'
        DataType = ftSmallint
      end
      item
        Name = 'Importe'
        DataType = ftLargeint
      end
      item
        Name = 'Saldo'
        DataType = ftLargeint
      end
      item
        Name = 'CodigoEntradaUsuario'
        Attributes = [faFixed]
        DataType = ftString
        Size = 2
      end
      item
        Name = 'EstadoImpago'
        DataType = ftBoolean
      end
      item
        Name = 'CodigoCuotaAntecesora'
        DataType = ftSmallint
      end
      item
        Name = 'NumeroCuota'
        DataType = ftSmallint
      end>
    IndexDefs = <
      item
        Name = 'FechaCuota'
        Fields = 'FechaCuota'
      end
      item
        Name = 'FechaCuotaDESC'
        Fields = 'FechaCuota'
        Options = [ixDescending]
      end>
    IndexName = 'FechaCuota'
    Params = <>
    ProviderName = 'dspObtenerRefinanciacionCuotas'
    StoreDefs = True
    AfterScroll = cdsCuotasAfterScroll
    Left = 728
    Top = 288
    object cdsCuotasCodigoCuota: TSmallintField
      FieldName = 'CodigoCuota'
    end
    object cdsCuotasCodigoEstadoCuota: TSmallintField
      FieldName = 'CodigoEstadoCuota'
    end
    object cdsCuotasDescripcionEstadoCuota: TStringField
      FieldName = 'DescripcionEstadoCuota'
      Size = 30
    end
    object cdsCuotasCodigoFormaPago: TSmallintField
      FieldName = 'CodigoFormaPago'
    end
    object cdsCuotasDescripcionFormaPago: TStringField
      FieldName = 'DescripcionFormaPago'
      Size = 30
    end
    object cdsCuotasTitularNombre: TStringField
      FieldName = 'TitularNombre'
      Size = 100
    end
    object cdsCuotasTitularTipoDocumento: TStringField
      FieldName = 'TitularTipoDocumento'
      FixedChar = True
      Size = 4
    end
    object cdsCuotasTitularNumeroDocumento: TStringField
      FieldName = 'TitularNumeroDocumento'
      Size = 11
    end
    object cdsCuotasCodigoBanco: TIntegerField
      FieldName = 'CodigoBanco'
    end
    object cdsCuotasDescripcionBanco: TStringField
      FieldName = 'DescripcionBanco'
      Size = 50
    end
    object cdsCuotasDocumentoNumero: TStringField
      FieldName = 'DocumentoNumero'
    end
    object cdsCuotasFechaCuota: TDateTimeField
      FieldName = 'FechaCuota'
    end
    object cdsCuotasCodigoTarjeta: TIntegerField
      FieldName = 'CodigoTarjeta'
    end
    object cdsCuotasDescripcionTarjeta: TStringField
      FieldName = 'DescripcionTarjeta'
      FixedChar = True
      Size = 50
    end
    object cdsCuotasTarjetaCuotas: TSmallintField
      FieldName = 'TarjetaCuotas'
    end
    object cdsCuotasImporte: TLargeintField
      FieldName = 'Importe'
    end
    object cdsCuotasSaldo: TLargeintField
      FieldName = 'Saldo'
    end
    object cdsCuotasCodigoEntradaUsuario: TStringField
      FieldName = 'CodigoEntradaUsuario'
      FixedChar = True
      Size = 2
    end
    object cdsCuotasEstadoImpago: TBooleanField
      FieldName = 'EstadoImpago'
    end
    object cdsCuotasCodigoCuotaAntecesora: TSmallintField
      FieldName = 'CodigoCuotaAntecesora'
    end
    object cdsCuotasCuotaNueva: TBooleanField
      FieldKind = fkInternalCalc
      FieldName = 'CuotaNueva'
    end
    object cdsCuotasCuotaModificada: TBooleanField
      FieldKind = fkInternalCalc
      FieldName = 'CuotaModificada'
    end
    object cdsCuotasNumeroCuota: TSmallintField
      FieldName = 'NumeroCuota'
    end
  end
  object dsCuotas: TDataSource
    DataSet = cdsCuotas
    Left = 712
    Top = 304
  end
  object spActualizarRefinanciacionDatosCabecera: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarRefinanciacionDatosCabecera'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoMedioPago'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Fecha'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoEstado'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@TotalDeuda'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@TotalPagado'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@TotalDemanda'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@Notificacion'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@RepLegalCliNumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@RepLegalCliNombre'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@RepLegalCliApellido'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@RepLegalCliApellidoMaterno'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@RepLegalCNNumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@RepLegalCNNombre'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@RepLegalCNApellido'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@RepLegalCNApellidoMaterno'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@NumeroCausaRol'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@GastoNotarial'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@Validada'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@JPLNumero'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@JPLCodigoPais'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@JPLCodigoRegion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@JPLCodigoComuna'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@NombrePlanRefinanciacion'
        DataType = ftString
        Size = 255
        Value = Null
      end>
    Left = 656
    Top = 576
  end
  object spObtenerRefinanciacionDatosCabecera: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerRefinanciacionDatosCabecera'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 664
    Top = 616
  end
  object spActualizarRefinanciacionComprobantes: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarRefinanciacionComprobantes'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@SaldoPendiente'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@BorrarExistentes'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 664
    Top = 664
  end
  object spObtenerRefinanciacionComprobantes: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerRefinanciacionComprobantes'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 696
    Top = 240
  end
  object dspObtenerRefinanciacionComprobantes: TDataSetProvider
    DataSet = spObtenerRefinanciacionComprobantes
    Left = 664
    Top = 264
  end
  object spObtenerRefinanciacionCuotas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerRefinanciacionCuotas'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCuota'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@SinCuotasAnuladas'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 800
    Top = 248
  end
  object dspObtenerRefinanciacionCuotas: TDataSetProvider
    DataSet = spObtenerRefinanciacionCuotas
    Left = 776
    Top = 272
  end
  object spActualizarRefinanciacionCuotas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarRefinanciacionCuotas'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaCuota'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Importe'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@Saldo'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CodigoEstadoCuota'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@CodigoFormaPago'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@TitularNombre'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@TitularTipoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@TitularNumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@DocumentoNumero'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@CodigoBanco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTarjeta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TarjetaCuotas'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@BorrarExistentes'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@CodigoCuota'
        Attributes = [paNullable]
        DataType = ftSmallint
        Direction = pdInputOutput
        Precision = 5
        Value = Null
      end
      item
        Name = '@CodigoCuotaAntecesora'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end>
    Left = 648
    Top = 152
  end
  object spObtenerRefinanciacionPatentesPrecautoria: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerRefinanciacionPatentesPrecautoria'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 376
    Top = 536
  end
  object dspObtenerRefinanciacionPatentesPrecautoria: TDataSetProvider
    DataSet = spObtenerRefinanciacionPatentesPrecautoria
    Left = 352
    Top = 544
  end
  object spActualizarRefinanciacionPatentesPrecautoria: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarRefinanciacionPatentesPrecautoria'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@MarcaModeloColor'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@Convenio'
        Attributes = [paNullable]
        DataType = ftString
        Size = 80
        Value = Null
      end
      item
        Name = '@CodigoEstadoCuenta'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@BorrarExistentes'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 576
    Top = 168
  end
  object spObtenerRefinanciacionDomicilio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerRefinanciacionDomicilio'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 352
    Top = 640
  end
  object spActualizarRefinanciacionDomicilio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarRefinanciacionDomicilio'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Calle'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@Numero'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Piso'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Dpto'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@Detalle'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@CodigoPostal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@CodigoPais'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoRegion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoComuna'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@BorrarExistentes'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 392
    Top = 664
  end
  object spActualizarRefinanciacionTelefonos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarRefinanciacionTelefonos'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoArea'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@NumeroTelefono'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@BorrarExistentes'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 112
    Top = 584
  end
  object spObtenerRefinanciacionTelefonos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerRefinanciacionTelefonos'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 112
    Top = 536
  end
  object spObtenerTelefonoPersonaSinFormato: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerTelefonoPersonaSinFormato'
    Parameters = <>
    Left = 112
    Top = 624
  end
  object spObtenerRefinanciacionCuotasDatosCuota: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerRefinanciacionCuotas'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCuota'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end>
    Left = 472
    Top = 240
  end
  object spActualizarRefinanciacionCuotasEstados: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarRefinanciacionCuotasEstados'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCuota'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@CodigoEstadoCuota'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 488
    Top = 144
  end
  object spActualizarRefinanciacionAceptada: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarRefinanciacionAceptada'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRefinanciacionUnificada'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 808
    Top = 544
  end
  object spActualizarRefinanciacionDocumentoOriginal: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarRefinanciacionDocumentoOriginal'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRefinanciacionUnificada'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@DocumentoOriginal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@AnexoCuotas'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@AnexoComprobantes'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@AnexoComprobantes2'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end>
    Left = 840
    Top = 600
  end
  object spActualizarEstadoConvenio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarEstadoConvenio'
    Parameters = <>
    Left = 520
    Top = 687
  end
  object spIngresarConvenioACobranzaInterna: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'IngresarConvenioACobranzaInterna'
    Parameters = <>
    Left = 272
    Top = 431
  end
end
