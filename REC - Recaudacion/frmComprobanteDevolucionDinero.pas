{----------------------------------------------------------------------------
                	frmComprobanteDevolucionDinero

Author		: mbecerra
Date		: 16-Junio-2011
Description	:		(Ref: SS 959)
                Ventana que permite la creaci�n e impresi�n del comprobante
                de devoluci�n de dinero


Author		: mvillarroel
Date		: 07-Julio-2013
Firma   : SS_660_MVI_20130711
Description	:		(Ref: SS 660)
                Se agrega validaci�n para indicar si un Convenio est� en Lista Amarilla

Firma       : SS_660_CQU_20130711
Descripcion : Se agrega el par�metro CodigoConcesionaria en la funci�n EstaConvenioEnListaAmarilla

Firma       : SS_660_MVI_20130729
Descripcion : Se cambia texto y se agrega estado del convenio en lista amarilla

Firma       :   SS_1120_MVI_20130820
Descripcion :   Se cambia el llamado a llenar el combo de convenios para que utilice el procedimiento  CargarConveniosRUT.
                Se crea procedimiento OnDrawItem para llamar al procedimiento que pinta los convenios.
                Se llama funciones para obtener el numero de convenio sin formato.

Firma       :   SS_660_MVI_20130909
Descripcion :   Se realiza la limpieza del label lista amarilla, dentro de la limpieza de los convenios
                y se modifica validaci�n para saber si se obtiene resultado del SP ObtenerEstadoConvenioEnListaAmarilla.
                Se acorta tama�o del lblEnListaAmarilla y se elimina texto inicial, ya que siempre es reemplazado.                

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

 Firma          : SS_1408_MCA_20151027
 Descripcion    : muestra un mensaje si el cliente se encuentre en registrado en Tabla ClientesAContactar

-----------------------------------------------------------------------------}
unit frmComprobanteDevolucionDinero;

interface

uses
  DMConnection,
  PeaProcs,
  UtilProc,
  UtilDB,
  Util,
  BuscaClientes,
  frmReporteCompDevDinero,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, VariantComboBox, DmiCtrls, Validate, DateEdit, DB, ADODB,
  PeaTypes;                                                                           //SS_1120_MVI_20130820

type
  TComprobanteDevolucionDineroForm = class(TForm)
    Label1: TLabel;
    peRutCliente: TPickEdit;
    Label2: TLabel;
    vcbConvenios: TVariantComboBox;
    Label3: TLabel;
    Label4: TLabel;
    btnAceptar: TButton;
    Label6: TLabel;
    spObtenerConvenios: TADOStoredProc;
    Label7: TLabel;
    spCrearComprobanteDevolucionDinero: TADOStoredProc;
    vcbReservaComprobantesDV: TVariantComboBox;
    lblImporte: TLabel;
    lblFecha: TLabel;
    btnSalir: TButton;
    spObtenerReservaComprobantesDV: TADOStoredProc;
    lblEnListaAmarilla: TLabel;                                                       //SS_660_MVI_20130711
    procedure peRutClienteButtonClick(Sender: TObject);
    procedure ValidaBusca(Sender: TObject; var Key: Char);
    procedure btnAceptarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure BuscarReservaComprobantesDV;
    procedure vcbConveniosChange(Sender: TObject);
    procedure vcbReservaComprobantesDVChange(Sender: TObject);
    procedure vcbConveniosDrawItem(Control: TWinControl; Index: Integer;               //SS_1120_MVI_20130820
      Rect: TRect; State: TOwnerDrawState);                                            //SS_1120_MVI_20130820
    
  private
    { Private declarations }
    FUltimaBusqueda: TBusquedaCliente;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
  public
    { Public declarations }
    function Inicializar : Boolean;
    procedure BuscarConvenios;
    procedure MuestraError(CodigoError : Integer);
    procedure LimpiarDatos;
  end;

var
  ComprobanteDevolucionDineroForm: TComprobanteDevolucionDineroForm;

implementation

{$R *.dfm}

{----------------------------------------------------------------------------
                	QueryGetValueBool

Author		: mbecerra
Date		: 16-Junio-2011
Description	:		(Ref: SS 959)
                Devuelve un valor booleano de la consulta
-----------------------------------------------------------------------------}
function QueryGetValueBool(Conn : TADOConnection; cSQL : string) : Boolean;
var
    aQry : TADOQuery;
begin
	Result := False;
    aQry := TADOQuery.Create(nil);
    aQry.SQL.Text := cSQL;
    aQry.Connection := Conn;
    aQry.Open;
    if not aQry.Eof then begin
       	Result := aQry.Fields[0].AsBoolean;
    end;
    aQry.Close;
    aQry.Free;
end;

{----------------------------------------------------------------------------
                	Inicializar

Author		: mbecerra
Date		: 16-Junio-2011
Description	:		(Ref: SS 959)
                Inicializa la ventana que permite la creaci�n e
                impresi�n del comprobante de devoluci�n de dinero
-----------------------------------------------------------------------------}
function TComprobanteDevolucionDineroForm.Inicializar;
resourcestring
    MSG_CAPTION		= 'Devoluci�n de Dinero';
begin

	Result := True;
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216
    CenterForm(Self);
    Caption := MSG_CAPTION;
    LimpiarDatos;
    lblEnListaAmarilla.Visible:=False;                                                    //SS_660_MVI_20130711
end;

{----------------------------------------------------------------------------
                	MuestraError

Author		: mbecerra
Date		: 16-Junio-2011
Description	:		(Ref: SS 959)
              Despliega un mensaje acorde al c�digo de error retornado por
              el stored CrearComprobante
-----------------------------------------------------------------------------}
procedure TComprobanteDevolucionDineroForm.MuestraError;
resourcestring
	MSG_DESCONOCIDO			= 'Error desconocido %d';
	MSG_MENOS_UNO			= 'El convenio no existe o no se ha especificado';
    MSG_MENOS_DOS			= 'Este convenio se encuentra dado de baja: No tiene saldo a favor del cliente';
    MSG_MENOS_TRES			= 'Ocurri� un error al actualizar pagos comprobantes';
    MSG_MENOS_CUATRO		= 'No se pudo crear el proceso de facturaci�n';
    MSG_MENOS_CINCO			= 'No se pudo crear el comprobante en la tabla Comprobantes';
    MSG_MENOS_SEIS			= 'No se pudo actualizar el saldo del convenio';
    MSG_MENOS_SIETE			= 'No se pudo cambiar el estado del comprobante de Impago a Emitido';
    MSG_MENOS_OCHO			= 'No se pudo crear el Lote de facturaci�n';
    MSG_MENOS_NUEVE			= 'No se pudo crear el movimiento cuenta';
    MSG_MENOS_DIEZ			= 'No se pudo Insertar la relaci�n en tabla ComprobantesNKCK';
    MSG_MENOS_ONCE			= 'Error al Reaplicar Convenio';
    MSG_MENOS_DOCE      	= 'No se encuentra la Reserva DV';
    MSG_MENOS_TRECE     	= 'Ocurrio un problema al intentar deshacer la Aplicaci�n';
    MSG_MENOS_CATORCE   	= 'Convenio no tiene saldo o no tiene una NK pagada y no vencida.';
    MSG_MENOS_QUINCE    	= 'Convenio no tiene saldo a favor o el monto de la devoluci�n lo excede';
    MSG_MENOS_DIECISEIS		= 'No se pudo levantar intereses de la NK o no existe registro en HistoricoInteresesComprobantes';
    MSG_MENOS_DIECISIETE	= 'No se pudo actualizar el saldo de la NK';
    
begin
	case CodigoError of
    	-1 : MsgBoxBalloon(MSG_MENOS_UNO, Caption, MB_ICONERROR, vcbConvenios);
        -2 : MsgBoxBalloon(MSG_MENOS_DOS, Caption, MB_ICONERROR, vcbConvenios);
        -3 : MsgBox(MSG_MENOS_TRES, Caption, MB_ICONERROR);
        -4 : MsgBox(MSG_MENOS_CUATRO, Caption, MB_ICONERROR);
        -5 : MsgBox(MSG_MENOS_CINCO, Caption, MB_ICONERROR);
        -6 : MsgBox(MSG_MENOS_SEIS, Caption, MB_ICONERROR);
        -7 : MsgBox(MSG_MENOS_SIETE, Caption, MB_ICONERROR);
        -8 : MsgBox(MSG_MENOS_OCHO, Caption, MB_ICONERROR);
        -9 : MsgBox(MSG_MENOS_NUEVE, Caption, MB_ICONERROR);
       -10 : MsgBox(MSG_MENOS_DIEZ, Caption, MB_ICONERROR);
       -11 : MsgBox(MSG_MENOS_ONCE, Caption, MB_ICONERROR);
       -12 : MsgBoxBalloon(MSG_MENOS_DOCE, Caption, MB_ICONERROR, vcbReservaComprobantesDV);
       -13 : MsgBox(MSG_MENOS_TRECE, Caption, MB_ICONERROR);
       -14 : MsgBox(MSG_MENOS_CATORCE, Caption, MB_ICONERROR);
       -15 : MsgBox(MSG_MENOS_QUINCE, Caption, MB_ICONERROR);
       -16 : MsgBox(MSG_MENOS_DIECISEIS, Caption, MB_ICONERROR);
       -17 : MsgBox(MSG_MENOS_DIECISIETE, Caption, MB_ICONERROR);
       else  MsgBox(Format(MSG_DESCONOCIDO, [CodigoError]), Caption, MB_ICONERROR);

    end;
end;

{----------------------------------------------------------------------------
                	LimpiarDatos

Author		: mbecerra
Date		: 16-Junio-2011
Description	:		(Ref: SS 959)
              Limpia los campos de la pantalla
-----------------------------------------------------------------------------}
procedure TComprobanteDevolucionDineroForm.LimpiarDatos;
begin
    peRutCliente.Text := '';
    vcbConvenios.Clear;
    vcbReservaComprobantesDV.Clear;
    lblImporte.Caption  := '';
    lblFecha.Caption    := '';
    lblEnListaAmarilla.Visible  := False;                                         //SS_660_MVI_20130909
    lblEnListaAmarilla.Caption  := EmptyStr;                                      //SS_660_MVI_20130909
end;

{----------------------------------------------------------------------------
                	BuscarConvenio

Author		: mbecerra
Date		: 16-Junio-2011
Description	:		(Ref: SS 959)
              Busca los convenios asociados al Rut ingresado en el campo
-----------------------------------------------------------------------------}
procedure TComprobanteDevolucionDineroForm.BuscarConvenios;
resourcestring
	MSG_NO_HAY_DATOS = 'No se encontraron convenios para el %s Indicado';
    MSG_ERROR = 'Error al obtener los convenios asociados al %s';

begin
	try
    	vcbConvenios.Clear;
   // 	spObtenerConvenios.Close;                                                                         //SS_1120_MVI_20130820
   //     spObtenerConvenios.Parameters.Refresh;                                                          //SS_1120_MVI_20130820
   //     spObtenerConvenios.Parameters.ParamByName('@CodigoDocumento').Value := 'RUT';                   //SS_1120_MVI_20130820
   // 	spObtenerConvenios.Parameters.ParamByName('@NumeroDocumento').Value := peRutCliente.Text;         //SS_1120_MVI_20130820
   //     spObtenerConvenios.Parameters.ParamByName('@SoloActivos').Value := False;                       //SS_1120_MVI_20130820
   // 	spObtenerConvenios.Open;                                                                          //SS_1120_MVI_20130820
   // 	if not spObtenerConvenios.IsEmpty then begin                                                      //SS_1120_MVI_20130820
   //     	while not spObtenerConvenios.Eof do begin                                                     //SS_1120_MVI_20130820
	 //           vcbConvenios.Items.Add(	spObtenerConvenios.FieldByName('NumeroConvenio').AsString,        //SS_1120_MVI_20130820
   //                         			spObtenerConvenios.FieldByName('CodigoConvenio').AsString );          //SS_1120_MVI_20130820
   //     		spObtenerConvenios.Next;                                                                    //SS_1120_MVI_20130820
   //     	end;                                                                                          //SS_1120_MVI_20130820

   //     	spObtenerConvenios.Close;                                                                     //SS_1120_MVI_20130820
   //         if vcbConvenios.Items.Count > 0 then begin                                                  //SS_1120_MVI_20130820
   //         	vcbConvenios.SetFocus;                                                                    //SS_1120_MVI_20130820
   //             vcbConvenios.ItemIndex := 0;                                                            //SS_1120_MVI_20130820
   //             if Assigned(vcbConvenios.OnChange) then vcbConvenios.OnChange(nil);                     //SS_1120_MVI_20130820
   //         end;                                                                                        //SS_1120_MVI_20130820

   // 	end                                                                                               //SS_1120_MVI_20130820
   // 	else MsgBox(Format(MSG_NO_HAY_DATOS,[peRutCliente.Text]), Caption, MB_ICONEXCLAMATION);           //SS_1120_MVI_20130820
                                                                                                          //SS_1120_MVI_20130820
        EstaClienteConMensajeEspecial(DMConnections.BaseCAC, PadL(peRUTCLiente.Text, 9, '0' ));           //SS_1408_MCA_20151027
        CargarConveniosRUT(DMCOnnections.BaseCAC,vcbConvenios, 'RUT',                                     //SS_1120_MVI_20130820
        PadL(peRUTCLiente.Text, 9, '0' ));                                                                //SS_1120_MVI_20130820

        if vcbConvenios.Items.Count > 0 then begin                                                        //SS_1120_MVI_20130820
            vcbConvenios.SetFocus;                                                                        //SS_1120_MVI_20130820
            vcbConvenios.ItemIndex := 0;                                                                  //SS_1120_MVI_20130820
            if Assigned(vcbConvenios.OnChange) then vcbConvenios.OnChange(nil);                           //SS_1120_MVI_20130820
        end;                                                                                              //SS_1120_MVI_20130820

    except on e:exception do begin
        	MsgBoxErr(Format(MSG_ERROR,[peRutCliente.Text]), e.Message, Caption, MB_ICONERROR);
    	end;
    end;

end;
{----------------------------------------------------------------------------
                	BuscarConvenio

Author		: alabra
Date		: 04-Octubre-2011
Description	:  (Ref: SS 959)
              Busca todos las reservas de comprobantes asociadas a un Convenio
              para el cliente x, para la fecha de hoy.
-----------------------------------------------------------------------------}
procedure TComprobanteDevolucionDineroForm.BuscarReservaComprobantesDV;
resourcestring
	MSG_NO_HAY_DATOS = 'No se encontraron Reservas para el convenio: %s Indicado';
    MSG_ERROR        = 'Error al obtener las reservas asociados al convenio %s';                                                                                //SS_660_MVI_20130711
   //SQL_ESTACONVENIOENLISTAAMARILLA = 'SELECT dbo.EstaConvenioEnListaAmarilla(CodigoConvenio,GETDATE() ) from Convenio where NumeroConvenio = ''%s'' ';        //SS_660_CQU_20130711 //SS_660_MVI_20130711
   //SQL_ESTACONVENIOENLISTAAMARILLA = 'SELECT dbo.EstaConvenioEnListaAmarilla(CodigoConvenio,GETDATE(), NULL ) from Convenio where NumeroConvenio = ''%s'' ';  //SS_660_MVI_20130729  //SS_660_CQU_20130711
   SQL_ESTACONVENIOENLISTAAMARILLA = 'SELECT dbo.ObtenerEstadoConvenioEnListaAmarilla(%s) ';                                                                    //SS_660_MVI_20130729   //SS_660_CQU_20130711
   SQL_OBTENER_CONVENIO = 'SELECT dbo.ObtenerNumeroConvenio(%s)';                                         //SS_1120_MVI_20130820
var
    FechaHoy : TDate;
    EstadoListaAmarilla : String;                                                                                                           //SS_660_MVI_20130729
begin
    try

        lblEnListaAmarilla.Color:=clYellow;                                                                                                  //SS_660_MVI_20130711
       // lblEnListaAmarilla.Visible  := QueryGetValue(   DMConnections.BaseCAC,                                                             //SS_660_MVI_20130729      //SS_660_MVI_20130711
       //                                                 Format(SQL_ESTACONVENIOENLISTAAMARILLA,                                            //SS_660_MVI_20130729      //SS_660_MVI_20130711
       //                                                         [   vcbConvenios.Text                                                      //SS_660_MVI_20130729      //SS_660_MVI_20130711
       //                                                         ]                                                                          //SS_660_MVI_20130729      //SS_660_MVI_20130711
       //                                                       )                                                                            //SS_660_MVI_20130729      //SS_660_MVI_20130711
       //                                              )='True';                                                                             //SS_660_MVI_20130729      //SS_660_MVI_20130711

       EstadoListaAmarilla:= QueryGetValue(   DMConnections.BaseCAC,                                                                         //SS_660_MVI_20130729
                                                 Format(SQL_ESTACONVENIOENLISTAAMARILLA,                                                     //SS_660_MVI_20130729
                                                         [   IntToStr(vcbConvenios.Value)                                                    //SS_660_MVI_20130729
                                                         ]                                                                                   //SS_660_MVI_20130729
                                                       )                                                                                     //SS_660_MVI_20130729
                                                       );                                                                                    //SS_660_MVI_20130729
                                                                                                                                             //SS_660_MVI_20130729
//        if EstadoListaAmarilla <> 'NULL' then begin                                                                                        //SS_660_MVI_20130909  //SS_660_MVI_20130729
        if EstadoListaAmarilla <> '' then begin                                                                                              //SS_660_MVI_20130909  //SS_660_MVI_20130729
        lblEnListaAmarilla.Visible  := True;                                                                                                 //SS_660_MVI_20130729
        lblEnListaAmarilla.Caption  := EstadoListaAmarilla;                                                                                  //SS_660_MVI_20130729
                                                                                                                                             //SS_660_MVI_20130729
        end                                                                                                                                  //SS_660_MVI_20130729
        else begin                                                                                                                           //SS_660_MVI_20130729
                                                                                                                                             //SS_660_MVI_20130729
        lblEnListaAmarilla.Visible  := False;                                                                                                //SS_660_MVI_20130729
                                                                                                                                             //SS_660_MVI_20130729
        end;                                                                                                                                 //SS_660_MVI_20130729

        FechaHoy := Trunc(NowBase(DMConnections.BaseCAC));
        vcbReservaComprobantesDV.Clear;
        if vcbConvenios.ItemIndex >= 0 then begin

            spObtenerReservaComprobantesDV.Close;
            with spObtenerReservaComprobantesDV.Parameters do begin
                Refresh;
                ParamByName('@IdReservaComprobanteDV').Value        := Null;
             //   ParamByName('@NumeroConvenio').Value                := vcbConvenios.Text;                   //SS_1120_MVI_20130820
                ParamByName('@NumeroConvenio').Value                := QueryGetValue(DMConnections.BaseCAC, Format(SQL_OBTENER_CONVENIO, [vcbConvenios.Value]));                          //SS_1120_MVI_20130820
                ParamByName('@RutCliente').Value                    := peRutCliente.Text;
                ParamByName('@NumeroRecibo').Value                  := Null;
                ParamByName('@FechaSolicitud').Value                := FechaHoy;
                ParamByName('@FechaHoraImpresionDV').Value          := Null;
                ParamByName('@IncluyeComprobanteInactivo').Value    := False;
                ParamByName('@IncluyeReservasEliminadas').Value		:= 0;
            end;
            spObtenerReservaComprobantesDV.Open;

            if not spObtenerReservaComprobantesDV.IsEmpty then begin
                while not spObtenerReservaComprobantesDV.Eof do begin
                    //solamente tomamos las Reservas que no han sido impresas
                    if spObtenerReservaComprobantesDV.FieldByName('UsuarioImpresionDV').AsString = '' then begin
                        vcbReservaComprobantesDV.Items.Add(	spObtenerReservaComprobantesDV.FieldByName('IdReservaComprobanteDV').AsString,
                                                        	spObtenerReservaComprobantesDV.FieldByName('IdReservaComprobanteDV').AsString );
                    end;
                    spObtenerReservaComprobantesDV.Next;
                end;

                spObtenerReservaComprobantesDV.Close;
                if vcbReservaComprobantesDV.Items.Count > 0 then begin
                    vcbReservaComprobantesDV.SetFocus;
                    vcbReservaComprobantesDV.ItemIndex := 0;
                    if Assigned(vcbReservaComprobantesDV.OnChange) then vcbReservaComprobantesDV.OnChange(nil);
                end
                else begin
                	lblImporte.Caption  := '';
                	lblFecha.Caption    := '';
                	MsgBox(Format(MSG_NO_HAY_DATOS,[vcbConvenios.Text]), Caption, MB_ICONEXCLAMATION);
                end;

            end
            else begin
                lblImporte.Caption  := '';
                lblFecha.Caption    := '';
                MsgBox(Format(MSG_NO_HAY_DATOS,[vcbConvenios.Text]), Caption, MB_ICONEXCLAMATION);
            end;
        end;
    except on e:exception do begin
        	MsgBoxErr(Format(MSG_ERROR,[vcbConvenios.Text]), e.Message, Caption, MB_ICONERROR);
    	end;
    end;
end;

{----------------------------------------------------------------------------
                	btnAceptarClick

Author		: mbecerra
Date		: 16-Junio-2011
Description	:		(Ref: SS 959)
              Genera e imprime el comprobanted e devoluci�n de dinero
-----------------------------------------------------------------------------}
procedure TComprobanteDevolucionDineroForm.btnAceptarClick(Sender: TObject);
resourcestring
	MSG_CAPTION						= 'Validaciones';
    MSG_CONTINUAR					= 'Se va a crear un nuevo comprobante de Devoluci�n de dinero para esta reserva, �Desea Continuar?';
    MSG_ERROR						= 'Ocurri� un error al Crear el comprobante de Devoluci�n de Dinero'; 
    MSG_EXITO						= 'Se ha creado exitosamente el comprobante de devoluci�n de dinero n�mero %d';
    
    //mensajes de validaciones
    MSG_NO_ESTA_RUT			= 'Falta el Rut del cliente';
    MSG_NO_ESTA_CONVENIO	= 'Falta el convenio del cliente';
    MSG_NO_ESTA_RESERVA     = 'Falta la Reserva de Devoluci�n de dinero';
var
    Resultado : Integer;
    NumeroComprobante : Cardinal;
begin
     //Validar que se hayan ingresado todos los datos
    if not ValidateControls(
    						[	peRutCliente, vcbConvenios, vcbReservaComprobantesDV
    						],
                            [	peRutCliente.Text <> '', vcbConvenios.ItemIndex >= 0,
                                vcbReservaComprobantesDV.ItemIndex >= 0
                            ],
                            MSG_CAPTION,
                            [	MSG_NO_ESTA_RUT, MSG_NO_ESTA_CONVENIO,
                                MSG_NO_ESTA_RESERVA
                            ]
    					) then Exit;

    if MsgBox(MSG_CONTINUAR, Caption, MB_ICONQUESTION + MB_YESNO) = IDNO then Exit;

    try
      //Crear el comprobante
      //el stored tiene transaccionalidad
    	spCrearComprobanteDevolucionDinero.Close;
        with spCrearComprobanteDevolucionDinero do begin
        	Parameters.Refresh;
        	Parameters.ParamByName('@IdReservaComprobanteDV').Value			:= vcbReservaComprobantesDV.Value;
			Parameters.ParamByName('@Usuario').Value						:= UsuarioSistema;
			Parameters.ParamByName('@NumeroComprobanteDevDinero').Value		:= NULL;
            ExecProc;
            Resultado := Parameters.ParamByName('@RETURN_VALUE').Value;
            if Resultado < 0 then MuestraError(Resultado)
            else begin
            	NumeroComprobante := Parameters.ParamByName('@NumeroComprobanteDevDinero').Value;
            	//Lanzar la impresi�n
                Application.CreateForm(TReporteCompDevDineroForm, ReporteCompDevDineroForm);
                ReporteCompDevDineroForm.Inicializar(NumeroComprobante);
                MsgBox(Format(MSG_EXITO, [NumeroComprobante]), Caption, MB_ICONINFORMATION);
                ReporteCompDevDineroForm.Release;
                LimpiarDatos();
            end;
        end;
    except on e:exception do begin
        	MsgBoxErr(MSG_ERROR, e.Message, Caption, MB_ICONERROR);
    	end;
    end;
end;

procedure TComprobanteDevolucionDineroForm.btnSalirClick(Sender: TObject);
begin
    Close;
end;

{----------------------------------------------------------------------------
                	peRutClienteButtonClick

Author		: mbecerra
Date		: 16-Junio-2011
Description	:		(Ref: SS 959)
              Cierra la ventana actual
-----------------------------------------------------------------------------}
procedure TComprobanteDevolucionDineroForm.peRutClienteButtonClick(
  Sender: TObject);
var
	f : TFormBuscaClientes;
    TeclaEnter : Char;
begin
	Application.CreateForm(TFormBuscaClientes, f);
    if f.Inicializa(FUltimaBusqueda) then begin
        f.ShowModal;
        if f.ModalResult = IDOK then begin
            FUltimaBusqueda := f.UltimaBusqueda;
            TPickEdit(Sender).Text := f.Persona.NumeroDocumento;
            TeclaEnter := #13;
            ValidaBusca(Sender, TeclaEnter);
        end;
    end;

    f.Free;

end;

{----------------------------------------------------------------------------
                	ValidaBusca

Author		: mbecerra
Date		: 16-Junio-2011
Description	:		(Ref: SS 959)
              Cierra la ventana actual 
-----------------------------------------------------------------------------}
procedure TComprobanteDevolucionDineroForm.ValidaBusca(Sender: TObject;
  var Key: Char);
resourcestring
	MSG_RUT = 'El Rut %s ingresado es inv�lido';

begin
    if Key = #13 then begin
    	Key := #0;
        if ValidarRUT(DMConnections.BaseCAC, TPickEdit(Sender).Text) then begin
        	BuscarConvenios();
        end
        else begin
            MsgBox(Format(MSG_RUT,[TPickEdit(Sender).Text]), Caption, MB_ICONEXCLAMATION);
        end;
    end
    else begin
    	if not (Key in ['0'..'9', 'K', 'k', Char(VK_BACK)]) then Key := #0
        else begin
        	vcbConvenios.Clear;
            vcbReservaComprobantesDV.Clear;
            lblImporte.Caption  := '';
            lblFecha.Caption    := '';
            lblEnListaAmarilla.Visible  := False;                                         //SS_660_MVI_20130909
            lblEnListaAmarilla.Caption  := EmptyStr;                                      //SS_660_MVI_20130909

        end;

    end;
end;

{----------------------------------------------------------------------------
                	vcbConveniosChange

Author		: mbecerra
Date		: 16-Junio-2011
Description	:		(Ref: SS 959)
              Muestra el Saldo del convenio.

-----------------------------------------------------------------------------}
procedure TComprobanteDevolucionDineroForm.vcbConveniosChange(Sender: TObject);
begin
    BuscarReservaComprobantesDV();
end;

//BEGIN:SS_1120_MVI_20130820 -------------------------------------------------
{-----------------------------------------------------------------------------
  Procedure: TComprobanteDevolucionDineroForm.vcbConveniosDrawItem
  Date:      20-August-2013
  Description: Verifica si existe alg�n convenio de baja para llamar a la funci�n
                que colorea de rojo el item.
-----------------------------------------------------------------------------}
procedure TComprobanteDevolucionDineroForm.vcbConveniosDrawItem(
  Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  if Pos( TXT_CONVENIO_DE_BAJA, vcbConvenios.Items[Index].Caption ) > 0 then  begin

     ItemComboBoxColor(Control, Index, Rect, State, clred, true);

  end
  else begin

     ItemComboBoxColor(Control, Index, Rect, State);

  end;                                                                                  

end;
//END:SS_1120_MVI_20130820 ---------------------------------------------------
{----------------------------------------------------------------------------
                	vcbReservaComprobantesDVChange

Author		: alabra
Date		: 04-Octubre-2011
Description	:		(Ref: SS 959) SS_959_ALA_20111004
              Obtiene la fecha e importe de la reserva seleccionada
-----------------------------------------------------------------------------}
procedure TComprobanteDevolucionDineroForm.vcbReservaComprobantesDVChange(
  Sender: TObject);
resourcestring
	MSG_NO_HAY_DATOS = 'No se encontraron los datos de la reserva %s';
    MSG_ERROR        = 'Error al obtener la reserva %s';
   SQL_OBTENER_CONVENIO = 'SELECT dbo.ObtenerNumeroConvenio(%s)';                              //SS_1120_MVI_20130820
var
    FechaHoy : TDate;
begin
    try
        FechaHoy := Trunc(NowBase(DMConnections.BaseCAC));
        if vcbReservaComprobantesDV.ItemIndex >= 0 then begin

            spObtenerReservaComprobantesDV.Close;
            with spObtenerReservaComprobantesDV.Parameters do begin
                Refresh;
                ParamByName('@IdReservaComprobanteDV').Value        := vcbReservaComprobantesDV.Value;
            //    ParamByName('@NumeroConvenio').Value                := vcbConvenios.Text;                         //SS_1120_MVI_20130820
                ParamByName('@NumeroConvenio').Value                := QueryGetValue(DMConnections.BaseCAC, Format(SQL_OBTENER_CONVENIO, [vcbConvenios.Value]));;                          //SS_1120_MVI_20130820
                ParamByName('@RutCliente').Value                    := peRutCliente.Text;
                ParamByName('@NumeroRecibo').Value                  := Null;
                ParamByName('@FechaSolicitud').Value                := FechaHoy;
                ParamByName('@FechaHoraImpresionDV').Value          := Null;
                ParamByName('@IncluyeComprobanteInactivo').Value    := True;
            	ParamByName('@IncluyeReservasEliminadas').Value		:= 0;
            end;
            spObtenerReservaComprobantesDV.Open;
            if not spObtenerReservaComprobantesDV.IsEmpty then begin
                lblImporte.Caption  := spObtenerReservaComprobantesDV.FieldByName('Importe').AsString;
                lblFecha.Caption    := FormatDateTime('dd-mm-yyyy', spObtenerReservaComprobantesDV.FieldByName('FechaSolicitud').AsDateTime);
            end
            else MsgBox(Format(MSG_NO_HAY_DATOS,[vcbReservaComprobantesDV.Text]), Caption, MB_ICONEXCLAMATION);
        end;
    except on e:exception do begin
        	MsgBoxErr(Format(MSG_ERROR,[vcbReservaComprobantesDV.Text]), e.Message, Caption, MB_ICONERROR);
    	end;
    end;
end;

end.
