object RefinanciacionABMRepLegalCNForm: TRefinanciacionABMRepLegalCNForm
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = ' ABM Representantes Legales CN'
  ClientHeight = 568
  ClientWidth = 994
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object pnlAcciones: TPanel
    Left = 0
    Top = 0
    Width = 994
    Height = 40
    Align = alTop
    Color = 14408667
    ParentBackground = False
    TabOrder = 0
    ExplicitWidth = 1095
    object btnEditar: TButton
      Left = 89
      Top = 9
      Width = 75
      Height = 25
      Caption = 'Editar'
      TabOrder = 1
      OnClick = btnEditarClick
    end
    object btnAgregar: TButton
      Left = 12
      Top = 9
      Width = 75
      Height = 25
      Caption = 'A'#241'adir'
      TabOrder = 0
      OnClick = btnAgregarClick
    end
    object btnEliminar: TButton
      Left = 166
      Top = 9
      Width = 75
      Height = 25
      Caption = 'Eliminar'
      TabOrder = 2
      OnClick = btnEliminarClick
    end
  end
  object pnlSalir: TPanel
    Left = 0
    Top = 528
    Width = 994
    Height = 40
    Align = alBottom
    Color = 14408667
    ParentBackground = False
    TabOrder = 1
    ExplicitTop = 534
    ExplicitWidth = 1095
    DesignSize = (
      994
      40)
    object btnSalir: TButton
      Left = 911
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Salir'
      TabOrder = 0
      OnClick = btnSalirClick
      ExplicitLeft = 1012
    end
  end
  object pnlEntradaDatos: TPanel
    Left = 0
    Top = 202
    Width = 994
    Height = 326
    Align = alBottom
    TabOrder = 2
    Visible = False
    ExplicitTop = 208
    ExplicitWidth = 1095
    DesignSize = (
      994
      326)
    object GroupBox2: TGroupBox
      Left = 10
      Top = 6
      Width = 974
      Height = 312
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = '  Entrar Representante Legal  '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      ExplicitWidth = 1075
      DesignSize = (
        974
        312)
      object Label3: TLabel
        Left = 100
        Top = 31
        Width = 21
        Height = 13
        Caption = 'Rut:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label1: TLabel
        Left = 80
        Top = 54
        Width = 41
        Height = 13
        Caption = 'Nombre:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label2: TLabel
        Left = 80
        Top = 78
        Width = 41
        Height = 13
        Caption = 'Apellido:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label4: TLabel
        Left = 37
        Top = 102
        Width = 84
        Height = 13
        Caption = 'Apellido Materno:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label5: TLabel
        Left = 25
        Top = 125
        Width = 96
        Height = 13
        Caption = 'Tipo Refinanciaci'#243'n:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object edtNombre: TEdit
        Left = 129
        Top = 51
        Width = 143
        Height = 21
        CharCase = ecUpperCase
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        MaxLength = 60
        ParentFont = False
        TabOrder = 0
      end
      object edtApellido: TEdit
        Left = 129
        Top = 75
        Width = 221
        Height = 21
        CharCase = ecUpperCase
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        MaxLength = 60
        ParentFont = False
        TabOrder = 1
      end
      object edtApellidoMaterno: TEdit
        Left = 129
        Top = 98
        Width = 221
        Height = 21
        CharCase = ecUpperCase
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        MaxLength = 30
        ParentFont = False
        TabOrder = 2
      end
      object peRut: TPickEdit
        Left = 129
        Top = 28
        Width = 143
        Height = 21
        Hint = ' Seleccionar el Cliente a Refinanciar '
        Enabled = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnChange = peRutChange
        OnKeyPress = peRutKeyPress
        EditorStyle = bteTextEdit
        OnButtonClick = peRutButtonClick
      end
      object vcbTiposRefinanciacion: TVariantComboBox
        Left = 129
        Top = 122
        Width = 145
        Height = 21
        Style = vcsDropDownList
        DroppedWidth = 0
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ItemHeight = 13
        ParentFont = False
        TabOrder = 4
        Items = <>
      end
      object btnAceptar: TButton
        Left = 811
        Top = 278
        Width = 75
        Height = 25
        Anchors = [akRight, akBottom]
        Caption = 'Aceptar'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 6
        OnClick = btnAceptarClick
        ExplicitLeft = 912
      end
      object btnCancelar: TButton
        Left = 888
        Top = 278
        Width = 75
        Height = 25
        Anchors = [akRight, akBottom]
        Caption = 'Cancelar'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 7
        OnClick = btnCancelarClick
        ExplicitLeft = 989
      end
      object GroupBox1: TGroupBox
        Left = 9
        Top = 149
        Width = 956
        Height = 123
        Anchors = [akLeft, akTop, akRight, akBottom]
        Caption = '  Datos Empresa Recaudadora  '
        TabOrder = 5
        ExplicitWidth = 1057
        object Label6: TLabel
          Left = 71
          Top = 28
          Width = 41
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nombre:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label7: TLabel
          Left = 65
          Top = 47
          Width = 47
          Height = 13
          Alignment = taRightJustify
          Caption = 'Direcci'#243'n:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label8: TLabel
          Left = 75
          Top = 70
          Width = 37
          Height = 13
          Alignment = taRightJustify
          Caption = 'Regi'#243'n:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label9: TLabel
          Left = 69
          Top = 93
          Width = 43
          Height = 13
          Alignment = taRightJustify
          Caption = 'Comuna:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object edtEmpresaNombre: TEdit
          Left = 120
          Top = 21
          Width = 177
          Height = 21
          CharCase = ecUpperCase
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object edtEmpresaDireccion: TEdit
          Left = 120
          Top = 44
          Width = 321
          Height = 21
          CharCase = ecUpperCase
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
        object vcbEmpresaRegion: TVariantComboBox
          Left = 120
          Top = 67
          Width = 221
          Height = 21
          Style = vcsDropDownList
          DroppedWidth = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ItemHeight = 13
          ParentFont = False
          TabOrder = 2
          OnChange = vcbEmpresaRegionChange
          Items = <>
        end
        object vcbEmpresaComuna: TVariantComboBox
          Left = 120
          Top = 90
          Width = 221
          Height = 21
          Style = vcsDropDownList
          DroppedWidth = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ItemHeight = 13
          ParentFont = False
          TabOrder = 3
          Items = <>
        end
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 40
    Width = 994
    Height = 162
    Align = alClient
    BevelOuter = bvNone
    Caption = 'Panel3'
    TabOrder = 3
    ExplicitWidth = 1095
    ExplicitHeight = 168
    object DBListEx1: TDBListEx
      Left = 0
      Top = 0
      Width = 994
      Height = 162
      Align = alClient
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 100
          Header.Caption = 'Rut'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          IsLink = False
          FieldName = 'NumeroDocumento'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'Nombre'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          IsLink = False
          FieldName = 'Nombre'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 175
          Header.Caption = 'Apellido'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          IsLink = False
          FieldName = 'Apellido'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'Apellido Materno'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          IsLink = False
          FieldName = 'ApellidoMaterno'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'Tipo Refinanciaci'#243'n'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          IsLink = False
          FieldName = 'TipoMedioPagoDesc'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'Empresa Recaudadora'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          IsLink = False
          FieldName = 'Empresa'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 200
          Header.Caption = 'Empresa Direcci'#243'n'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          IsLink = False
          FieldName = 'Direccion'
        end>
      DataSource = dsRepresentantesCN
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
      ExplicitWidth = 1095
      ExplicitHeight = 168
    end
  end
  object spObtenerRefinanciacionRepresentantesCN: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerRefinanciacionRepresentantesCN'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@CodigoTipoMedioPago'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 464
    Top = 80
  end
  object dspObtenerRefinanciacionRepresentantesCN: TDataSetProvider
    DataSet = spObtenerRefinanciacionRepresentantesCN
    Left = 424
    Top = 96
  end
  object cdsRepresentantesCN: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspObtenerRefinanciacionRepresentantesCN'
    Left = 288
    Top = 104
    object cdsRepresentantesCNCodigoRepresentante: TAutoIncField
      FieldName = 'CodigoRepresentante'
      ReadOnly = True
    end
    object cdsRepresentantesCNApellido: TStringField
      FieldName = 'Apellido'
      FixedChar = True
      Size = 60
    end
    object cdsRepresentantesCNApellidoMaterno: TStringField
      FieldName = 'ApellidoMaterno'
      FixedChar = True
      Size = 30
    end
    object cdsRepresentantesCNNombre: TStringField
      FieldName = 'Nombre'
      FixedChar = True
      Size = 60
    end
    object cdsRepresentantesCNCodigoDocumento: TStringField
      FieldName = 'CodigoDocumento'
      FixedChar = True
      Size = 4
    end
    object cdsRepresentantesCNNumeroDocumento: TStringField
      FieldName = 'NumeroDocumento'
      FixedChar = True
      Size = 11
    end
    object cdsRepresentantesCNCodigoTipoMedioPago: TSmallintField
      FieldName = 'CodigoTipoMedioPago'
    end
    object cdsRepresentantesCNTipoMedioPagoDesc: TStringField
      FieldName = 'TipoMedioPagoDesc'
      FixedChar = True
      Size = 30
    end
    object cdsRepresentantesCNEmpresa: TStringField
      FieldName = 'Empresa'
      Size = 50
    end
    object cdsRepresentantesCNDireccion: TStringField
      FieldName = 'Direccion'
      Size = 75
    end
    object cdsRepresentantesCNCodigoPais: TStringField
      FieldName = 'CodigoPais'
      FixedChar = True
      Size = 3
    end
    object cdsRepresentantesCNCodigoRegion: TStringField
      FieldName = 'CodigoRegion'
      FixedChar = True
      Size = 3
    end
    object cdsRepresentantesCNCodigoComuna: TStringField
      FieldName = 'CodigoComuna'
      FixedChar = True
      Size = 3
    end
  end
  object dsRepresentantesCN: TDataSource
    DataSet = cdsRepresentantesCN
    Left = 304
    Top = 120
  end
  object spObtenerRefinanciacionRepresentanteCN: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerRefinanciacionRepresentantesCN'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@CodigoTipoMedioPago'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 128
    Top = 72
  end
  object spActualizarRefinanciacionRepresentantesCN: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarRefinanciacionRepresentantesCN'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRepresentante'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Apellido'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@ApellidoMaterno'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@Nombre'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@CodigoTipoMedioPago'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Empresa'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@Direccion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 75
        Value = Null
      end
      item
        Name = '@CodigoPais'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoRegion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoComuna'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 112
    Top = 120
  end
  object spEliminarRefinanciacionRepresentantesCN: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarRefinanciacionRepresentantesCN'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end>
    Left = 456
    Top = 160
  end
end
