{-----------------------------------------------------------------------------
 Unit Name: FrmReporteChequesCierreDeTurno
 Author:    mlopez
 Date:      02-Ago-2005
 Description: Reporte resumen de los cheques recibidos por operario
              al Cierre de Turno

Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.


-----------------------------------------------------------------------------}
unit FrmReporteChequesCierreDeTurno;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ppDB, ppDBPipe, ADODB, UtilRB, ppParameter, ppBands,
  ppCtrls, ppPrnabl, ppClass, ppCache, ppComm, ppRelatv, ppProd, ppReport,
  RBSetup, ppStrtch, ppSubRpt, ppModule, raCodMod, DMConnection, Util,
  UtilDB, UtilProc, ppVar, ConstParametrosGenerales;                            //SS_1147_NDR_20140710

type
  TFormReporteChequesCierreDeTurno = class(TForm)
    rpt_ChequesCierreDeTurno: TppReport;
    ppDetailBand1: TppDetailBand;
    ppParameterList1: TppParameterList;
	RBIListado: TRBInterface;
    sp_ObtenerChequesBancoSantantanderCierreTurno: TADOStoredProc;
    tpp_ObtenerChequesBancoSantantanderCierreTurno: TppDBPipeline;
    ds_ObtenerChequesBancoSantantanderCierreTurno: TDataSource;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppGroupFooterBand1: TppGroupFooterBand;
    sr_ChequesSantander: TppSubReport;
    pp_ChequesSantander: TppChildReport;
    ppTitleBand3: TppTitleBand;
    ppDetailBand4: TppDetailBand;
    ppSummaryBand4: TppSummaryBand;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    raCodeModule1: TraCodeModule;
    sr_OtrosBancos: TppSubReport;
    pp_ChequesOtrosBancos: TppChildReport;
    ppTitleBand1: TppTitleBand;
    pp_ChequesOtrosBancosImage1: TppImage;
    pp_ChequesOtrosBancosLabel5: TppLabel;
    pp_ChequesOtrosBancosLabel6: TppLabel;
    pp_ChequesOtrosBancosLabel7: TppLabel;
    pp_ChequesOtrosBancosLabel8: TppLabel;
    ppDetailBand2: TppDetailBand;
    pp_ChequesOtrosBancosDBText4: TppDBText;
    pp_ChequesOtrosBancosDBText5: TppDBText;
    pp_ChequesOtrosBancosDBText6: TppDBText;
    ppSummaryBand2: TppSummaryBand;
    raCodeModule3: TraCodeModule;
    sp_ObtenerChequesOtrosBancosCierreTurno: TADOStoredProc;
    ds_ObtenerChequesOtrosBancosCierreTurno: TDataSource;
    tppDB_ChequesOtrosBancos: TppDBPipeline;
    sp_ObtenerValesVistaCierreTurno: TADOStoredProc;
    ds_ObtenerValesVistaCierreTurno: TDataSource;
    tppDB_ValesVista: TppDBPipeline;
    sr_ValesVista: TppSubReport;
    ppChildReport1: TppChildReport;
    ppTitleBand2: TppTitleBand;
    ppImage1: TppImage;
    ppLabel1: TppLabel;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppDetailBand3: TppDetailBand;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppSummaryBand3: TppSummaryBand;
    raCodeModule4: TraCodeModule;
    ppLabel8: TppLabel;
    ppLine5: TppLine;
    ppLine6: TppLine;
    ppLine2: TppLine;
    ppLabel10: TppLabel;
    lbl_NumeroTurno: TppLabel;
    ppLine7: TppLine;
    ppLabel12: TppLabel;
    lbl_UsuarioTurno: TppLabel;
    ppLabel14: TppLabel;
    lbl_PuntoVenta: TppLabel;
    ppImage2: TppImage;
    ppLabel16: TppLabel;
    ppTitleBand4: TppTitleBand;
    lbl_usuario: TppLabel;
    lin_Top: TppLine;
    ppLine8: TppLine;
    ppLabel11: TppLabel;
    ppDBCalc2: TppDBCalc;
    ppDBText7: TppDBText;
    ppLabel15: TppLabel;
    ppDBText8: TppDBText;
    ppDBText9: TppDBText;
    ppDBText10: TppDBText;
    ppGroup2: TppGroup;
    ppGroupHeaderBand2: TppGroupHeaderBand;
    ppGroupFooterBand2: TppGroupFooterBand;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppLabel2: TppLabel;
    ppLine4: TppLine;
    ppLabel13: TppLabel;
    ppLabel19: TppLabel;
    ppDBCalc1: TppDBCalc;
    ppLine9: TppLine;
    ppLine3: TppLine;
    ppLine10: TppLine;
    ppGroup3: TppGroup;
    ppGroupHeaderBand3: TppGroupHeaderBand;
    ppGroupFooterBand3: TppGroupFooterBand;
    ppLine1: TppLine;
    ppLine11: TppLine;
    ppLine12: TppLine;
    ppLabel9: TppLabel;
    ppDBCalc3: TppDBCalc;
    procedure RBIListadoExecute(Sender: TObject; var Cancelled: Boolean);
    function GenerarReporte: Boolean;
  private
    { Private declarations }
    FNumeroTurno: Integer;
    FUsuarioTurno: AnsiString;
    FPuntoVenta: AnsiString;
  public
    function Inicializar(NumeroTurno: Integer; UsuarioTurno, PuntoVenta: AnsiString): Boolean;
    { Public declarations }
  end;

var
  FormReporteChequesCierreDeTurno: TFormReporteChequesCierreDeTurno;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    mlopez
  Date Created: 29/07/2005
  Description: Inicializo este formulario
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFormReporteChequesCierreDeTurno.Inicializar(NumeroTurno: Integer; UsuarioTurno, PuntoVenta: AnsiString): Boolean;
var                                                                             //SS_1147_NDR_20140710
  RutaLogo:AnsiString;                                                          //SS_1147_NDR_20140710
begin
    ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
    try                                                                                                 //SS_1147_NDR_20140710
      ppImage2.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
      pp_ChequesOtrosBancosImage1.Picture.LoadFromFile(Trim(RutaLogo));                                 //SS_1147_NDR_20140710
      ppImage1.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
    except                                                                                              //SS_1147_NDR_20140710
      On E: Exception do begin                                                                          //SS_1147_NDR_20140710
        Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
        MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
        Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
      end;                                                                                              //SS_1147_NDR_20140710
    end;                                                                                                //SS_1147_NDR_20140710

    Result := False;
  	FNumeroTurno := NumeroTurno;
    FUsuarioTurno := UsuarioTurno;
    FPuntoVenta := PuntoVenta;
    //Genera el reporte
    if GenerarReporte then begin
        //Ejecuta el reporte
        Result := RBIListado.Execute;
    end;
end;

{-----------------------------------------------------------------------------
  Procedure: GenerarReporte
  Author:    mlopez
  Date:      03-Ago-2005
  Description: Obtiene los datos para generar el reporte
  Arguments:
  Result:    Boolean
-----------------------------------------------------------------------------}
function TFormReporteChequesCierreDeTurno.GenerarReporte: Boolean;
resourcestring
    MSG_REPORTE_VACIO = 'No se encontraron cheques para imprimir el analítico';
    MSG_ERROR_AL_GENERAR_REPORTE = 'Error al generar el reporte';
    MSG_ERROR = 'Error';
Const
    REPORT = 'Reporte';
begin
    Result := True;
    try
        with sp_ObtenerChequesBancoSantantanderCierreTurno do begin
            Close;
            Parameters.Refresh;
            Parameters.ParamByName('@NumeroTurno').value:= FNumeroTurno;
            CommandTimeOut := 500;
            Open;
        end;
    except
        on e: Exception do begin
            Result := False;
            MsgBoxErr(MSG_ERROR, E.Message, REPORT , MB_ICONERROR);
        end;
    end;

    // Cheques de otros bancos
    try
        with sp_ObtenerChequesOtrosBancosCierreTurno do begin
            Close;
            Parameters.Refresh;
            Parameters.ParamByName('@NumeroTurno').value:= FNumeroTurno;
            CommandTimeOut := 500;
            Open;
        end;
    except
        on e: Exception do begin
            Result := False;
            MsgBoxErr(MSG_ERROR, E.Message, REPORT , MB_ICONERROR);
        end;
    end;

    // Vales Vista
    try
        with sp_ObtenerValesVistaCierreTurno do begin
            Close;
            Parameters.Refresh;
            Parameters.ParamByName('@NumeroTurno').value:= FNumeroTurno;
            CommandTimeOut := 500;
            Open;
        end;
    except
        on e: Exception do begin
            Result := False;
            MsgBoxErr(MSG_ERROR, E.Message, REPORT , MB_ICONERROR);
        end;
    end;

    //Si las tres secciones estan vacias informa reporte vacio
    if sp_ObtenerChequesBancoSantantanderCierreTurno.IsEmpty and sp_ObtenerChequesOtrosBancosCierreTurno.IsEmpty and sp_ObtenerValesVistaCierreTurno.IsEmpty then begin
        MsgBox(MSG_REPORTE_VACIO);
        Result := False;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: RBIListadoExecute
  Author:    mlopez
  Date Created: 29/07/2005
  Description: Ejecuto el informe
  Parameters: Sender: TObject;var Cancelled: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReporteChequesCierreDeTurno.RBIListadoExecute(Sender: TObject;var Cancelled: Boolean);
begin
    lbl_NumeroTurno.Caption := IntToStr(FNumeroTurno);
    lbl_UsuarioTurno.Caption := FUsuarioTurno;
    lbl_PuntoVenta.Caption := FPuntoVenta;
end;

end.
