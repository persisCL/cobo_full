object BuscarTraspasosForm: TBuscarTraspasosForm
  Left = 0
  Top = 0
  Caption = 'BuscarTraspasosForm'
  ClientHeight = 510
  ClientWidth = 706
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 706
    Height = 169
    Align = alTop
    TabOrder = 0
    DesignSize = (
      706
      169)
    object Label1: TLabel
      Left = 73
      Top = 40
      Width = 33
      Height = 13
      Caption = 'Fecha:'
    end
    object Label2: TLabel
      Left = 16
      Top = 59
      Width = 34
      Height = 13
      Caption = 'Desde:'
    end
    object Label3: TLabel
      Left = 333
      Top = 59
      Width = 32
      Height = 13
      Caption = 'Hasta:'
    end
    object Label4: TLabel
      Left = 283
      Top = 40
      Width = 103
      Height = 13
      Caption = 'N'#250'mero de Traspaso:'
    end
    object Label5: TLabel
      Left = 243
      Top = 59
      Width = 34
      Height = 13
      Caption = 'Desde:'
    end
    object Label6: TLabel
      Left = 112
      Top = 59
      Width = 32
      Height = 13
      Caption = 'Hasta:'
    end
    object Label7: TLabel
      Left = 16
      Top = 120
      Width = 57
      Height = 13
      Caption = 'Rut Cliente:'
    end
    object Label8: TLabel
      Left = 223
      Top = 117
      Width = 104
      Height = 13
      Caption = 'Numero de Convenio:'
    end
    object Label9: TLabel
      Left = 509
      Top = 40
      Width = 34
      Height = 13
      Caption = 'Monto:'
    end
    object Label10: TLabel
      Left = 459
      Top = 59
      Width = 34
      Height = 13
      Caption = 'Desde:'
    end
    object Label11: TLabel
      Left = 549
      Top = 59
      Width = 32
      Height = 13
      Caption = 'Hasta:'
    end
    object deFechaDesde: TDateEdit
      Left = 16
      Top = 72
      Width = 90
      Height = 21
      AutoSelect = False
      TabOrder = 0
      Date = -693594.000000000000000000
    end
    object deFechaHasta: TDateEdit
      Left = 112
      Top = 72
      Width = 90
      Height = 21
      AutoSelect = False
      TabOrder = 1
      Date = -693594.000000000000000000
    end
    object neNumDesde: TNumericEdit
      Left = 243
      Top = 72
      Width = 84
      Height = 21
      TabOrder = 2
    end
    object neNumHasta: TNumericEdit
      Left = 333
      Top = 72
      Width = 84
      Height = 21
      TabOrder = 3
    end
    object peRut: TPickEdit
      Left = 16
      Top = 136
      Width = 186
      Height = 21
      Enabled = True
      TabOrder = 6
      OnKeyPress = ValidaTeclaBusca
      EditorStyle = bteTextEdit
      OnButtonClick = peRutButtonClick
    end
    object neMontoDesde: TNumericEdit
      Left = 459
      Top = 72
      Width = 84
      Height = 21
      TabOrder = 4
    end
    object neMontoHasta: TNumericEdit
      Left = 549
      Top = 72
      Width = 84
      Height = 21
      TabOrder = 5
    end
    object btnBuscar: TButton
      Left = 425
      Top = 134
      Width = 75
      Height = 25
      Caption = '&Buscar'
      TabOrder = 7
      OnClick = btnBuscarClick
    end
    object vcbConvenios: TVariantComboBox
      Left = 223
      Top = 136
      Width = 196
      Height = 19
      Style = vcsOwnerDrawFixed
      ItemHeight = 13
      TabOrder = 8
      OnChange = vcbConveniosChange
      OnDrawItem = vcbConveniosDrawItem
      Items = <>
    end
    object btnLimpiar: TButton
      Left = 506
      Top = 134
      Width = 75
      Height = 25
      Caption = 'Limpiar'
      TabOrder = 9
      OnClick = btnLimpiarClick
    end
    object btnSalir: TButton
      Left = 618
      Top = 16
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Salir'
      TabOrder = 10
      OnClick = btnSalirClick
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 169
    Width = 706
    Height = 341
    Align = alClient
    TabOrder = 1
    object dbListadoTraspasos: TDBListEx
      Left = 2
      Top = 15
      Width = 702
      Height = 324
      Align = alClient
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Width = 80
          Header.Caption = 'Fecha'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Arial'
          Header.Font.Style = [fsBold]
          IsLink = False
          OnHeaderClick = OrdenarPorColumna
          FieldName = 'FechaCreacion'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Width = 150
          Header.Caption = 'N'#250'mero Traspaso'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Arial'
          Header.Font.Style = [fsBold]
          Header.Alignment = taRightJustify
          IsLink = False
          OnHeaderClick = OrdenarPorColumna
          FieldName = 'NumeroComprobante'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Width = 150
          Header.Caption = 'Tipo Documento'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Arial'
          Header.Font.Style = [fsBold]
          IsLink = False
          OnHeaderClick = OrdenarPorColumna
          FieldName = 'TipoComprobante'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Width = 150
          Header.Caption = 'Monto'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Arial'
          Header.Font.Style = [fsBold]
          Header.Alignment = taRightJustify
          IsLink = False
          OnHeaderClick = OrdenarPorColumna
          FieldName = 'Monto'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Width = 150
          Header.Caption = 'Numero Convenio'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Arial'
          Header.Font.Style = [fsBold]
          IsLink = False
          OnHeaderClick = OrdenarPorColumna
          FieldName = 'NumeroConvenio'
        end>
      DataSource = dsListado
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
    end
  end
  object dsListado: TDataSource
    DataSet = spObtenerTraspasos
    Left = 584
    Top = 128
  end
  object spObtenerTraspasos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerTraspasos'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@MensajeError'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@RutCliente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@FechaDesde'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaHasta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@NumeroTraspasoDesde'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@NumeroTraspasoHasta'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@MontoDesde'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@MontoHasta'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end>
    Left = 584
    Top = 96
  end
  object spObtenerConvenios: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'ObtenerConvenios;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@SoloActivos'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@SoloContanera'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 616
    Top = 96
  end
end
