{-----------------------------------------------------------------------------
 Unit Name: FrmReporteAnaliticoCierreDeTurno
 Author:    ggomez
 Purpose: Reporte con el Anal�tico de Cierre de Turno.
 History:

 Revision: 1
 Author: nfernandez
 Date: 10/06/2008
 Description: Se agrega la agrupaci�n (nivel 0) por el campo
 txt_TipoMedioPago (Canal de Pago).

 Revision : 2
 Author : pdominguez
 Date   : 24/06/2009
 Description : SS 809
    - Se modificaron / a�adieron los siguientes procedimientos / funciones:
        lbl_BancoPrint
        MostrarCheque
        MostrarAutorizacion
        txt_BancoPrint

 Revision : 3
 Author : plaza
 Date   : 16/02/2010
 Description : SS 528
    - Se modifico la funcion MostrarCheque

 Rev.: SS_637_PDO_20110414
 Description: Se a�ade el N�mero de Refinanciaci�n al Reporte.


Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.

Firma       :   SS_1147_MCA_20150216
Descripcion :   se reemplaza execProc x Open dado que daba error al abrir el sp

-----------------------------------------------------------------------------}
unit FrmReporteAnaliticoCierreDeTurno;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ppComm, ppRelatv, ppProd, ppClass, ppReport, UtilRB, DB, ppDB,
  ppDBPipe, ADODB, PeaTypes,
  DMConnection, UtilProc, ppBands, ppCache, ppVar, ppCtrls, ppPrnabl,
  ppParameter, CobranzasResources, PeaProcsCN, frmMuestraMensaje, ppStrtch, ppSubRpt,
  ConstParametrosGenerales, PeaProcs;                                                     //SS_1147_NDR_20140710

type
  TFormReporteAnaliticoCierreDeTurno = class(TForm)
    rbi_AnaliticoCierreDeTurno: TRBInterface;
    pln_DatosReporte: TppDBPipeline;
    dsDatosReporte: TDataSource;
    sp_ObtenerAnaliticoCierreDeTurno: TADOStoredProc;
    rpt_analiticoCierreDeTurnoRefinanciacion: TppReport;
    ppDetailBand1: TppDetailBand;
    ppSubReport1: TppSubReport;
    ppChildReport1: TppChildReport;
    ppDetailBand2: TppDetailBand;
    ppSummaryBand2: TppSummaryBand;
    ppGroup4: TppGroup;
    ppGroupHeaderBand3: TppGroupHeaderBand;
    ppGroupFooterBand3: TppGroupFooterBand;
    ppGroup5: TppGroup;
    ppGroupHeaderBand4: TppGroupHeaderBand;
    ppGroupFooterBand4: TppGroupFooterBand;
    ppGroup6: TppGroup;
    ppGroupHeaderBand5: TppGroupHeaderBand;
    ppGroupFooterBand5: TppGroupFooterBand;
    ppFooterBand1: TppFooterBand;
    ppSubReport2: TppSubReport;
    ppChildReport2: TppChildReport;
    ppDetailBand3: TppDetailBand;
    ppFooterBand2: TppFooterBand;
    ppSummaryBand3: TppSummaryBand;
    ppGroup7: TppGroup;
    ppGroupHeaderBand6: TppGroupHeaderBand;
    ppGroupFooterBand6: TppGroupFooterBand;
    ppGroup8: TppGroup;
    ppGroupHeaderBand7: TppGroupHeaderBand;
    ppGroupFooterBand7: TppGroupFooterBand;
    ppGroup9: TppGroup;
    ppGroupHeaderBand8: TppGroupHeaderBand;
    ppGroupFooterBand8: TppGroupFooterBand;
    ppHeaderBand1: TppHeaderBand;
    ppHeaderBand2: TppHeaderBand;
    ppLabel3: TppLabel;
    ppImage1: TppImage;
    ppLabel4: TppLabel;
    lbl_usuario1: TppLabel;
    ppLine8: TppLine;
    ppLine9: TppLine;
    ppLabel6: TppLabel;
    lbl_NumeroTurno1: TppLabel;
    ppLabel8: TppLabel;
    lbl_UsuarioTurno1: TppLabel;
    lbl_PuntoVenta1: TppLabel;
    ppLabel11: TppLabel;
    ppLabel12: TppLabel;
    ppImage2: TppImage;
    ppLabel13: TppLabel;
    lbl_usuario2: TppLabel;
    ppLine10: TppLine;
    ppLine11: TppLine;
    ppLabel15: TppLabel;
    lbl_NumeroTurno2: TppLabel;
    ppLabel17: TppLabel;
    lbl_UsuarioTurno2: TppLabel;
    lbl_PuntoVenta2: TppLabel;
    ppLabel20: TppLabel;
    ppShape1: TppShape;
    ppShape2: TppShape;
    ppLabel21: TppLabel;
    ppDBText1: TppDBText;
    ppLabel22: TppLabel;
    ppDBText2: TppDBText;
    ppLabel23: TppLabel;
    ppDBText3: TppDBText;
    ppLabel24: TppLabel;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppdbtxt_Comprobante: TppDBText;
    ppdbtxt_FechaEmision: TppDBText;
    txt_Importe1: TppDBText;
    ppdbtxt_FechaHora: TppDBText;
    txt_banco1: TppDBText;
    txt_NumeroCheque1: TppDBText;
    txt_FechaCheque1: TppDBText;
    ppDBText15: TppDBText;
    ppDBText16: TppDBText;
    ppDBText17: TppDBText;
    ppDBText18: TppDBText;
    txt_banco2: TppDBText;
    txt_NumeroCheque2: TppDBText;
    txt_FechaCheque2: TppDBText;
    ppDBText22: TppDBText;
    ppLine12: TppLine;
    ppDBCalc3: TppDBCalc;
    ppLabel25: TppLabel;
    ppLine13: TppLine;
    ppDBCalc4: TppDBCalc;
    ppLabel26: TppLabel;
    ppLabel27: TppLabel;
    ppLine14: TppLine;
    ppDBCalc5: TppDBCalc;
    ppLine15: TppLine;
    ppDBCalc6: TppDBCalc;
    ppLabel28: TppLabel;
    ppLabel29: TppLabel;
    ppSystemVariable1: TppSystemVariable;
    ppLabel30: TppLabel;
    ppSystemVariable2: TppSystemVariable;
    ppLabel31: TppLabel;
    ppLine16: TppLine;
    ppLine17: TppLine;
    ppDBCalc7: TppDBCalc;
    ppLabel32: TppLabel;
    ppLine18: TppLine;
    ppLine19: TppLine;
    ppDBCalc8: TppDBCalc;
    lbl_ColComprobante1: TppLabel;
    ppLine20: TppLine;
    lbl_Fecha1: TppLabel;
    lbl_Importe1: TppLabel;
    lbl_FechaPago1: TppLabel;
    lbl_Banco1: TppLabel;
    lbl_NumeroCheque1: TppLabel;
    lbl_FechaCheque1: TppLabel;
    lbl_ColComprobante2: TppLabel;
    lbl_Fecha2: TppLabel;
    lbl_Importe2: TppLabel;
    lbl_FechaPago2: TppLabel;
    lbl_Banco2: TppLabel;
    lbl_NumeroCheque2: TppLabel;
    lbl_FechaCheque2: TppLabel;
    ppLabel48: TppLabel;
    ppLine21: TppLine;
    ppLabel49: TppLabel;
    sp_ObtenerAnaliticoCierreDeTurnoRefinanciacion: TADOStoredProc;
    pln_DatosReporteRefinanciacion: TppDBPipeline;
    dsDatosReporteRefinanciacion: TDataSource;
    lbl_NroRefinanciacion: TppLabel;
    lbl_NroConvenio: TppLabel;
    lbl_FechaCuota: TppLabel;
    lbl_MontoCuota: TppLabel;
    lbl_FormaPagoCuota: TppLabel;
    lbl_NroDocumentoCuota: TppLabel;
    lbl_NroDocumentoPago: TppLabel;
    ppdbtxt_Cuotas: TppDBText;
    ppdbtxt_FechaCuota: TppDBText;
    ppdbtxt_NroConvenio: TppDBText;
    ppdbtxt_MontoCuota: TppDBText;
    ppdbtxt_FormaPagoCuota: TppDBText;
    ppdbtxt_NroDocCuota: TppDBText;
    ppdbtxt_NroDocPagoCuota: TppDBText;
    spObtenerMaestroConcesionaria: TADOStoredProc;
    procedure rbi_AnaliticoCierreDeTurnoExecute(Sender: TObject;
      var Cancelled: Boolean);
    procedure txt_RepactacionNumeroPrint(Sender: TObject);  // SS_637_PDO_20110414
    procedure lbl_RepactacionNumeroPrint(Sender: TObject);
    procedure txt_banco1Print(Sender: TObject);                 // SS_637_PDO_20110503
    procedure txt_banco2Print(Sender: TObject);                 // SS_637_PDO_20110503
    procedure txt_NumeroCheque1Print(Sender: TObject);          // SS_637_PDO_20110503
    procedure txt_NumeroCheque2Print(Sender: TObject);          // SS_637_PDO_20110503
    procedure txt_FechaCheque1Print(Sender: TObject);           // SS_637_PDO_20110503
    procedure txt_FechaCheque2Print(Sender: TObject);           // SS_637_PDO_20110503
    procedure ppGroupHeaderBand4BeforePrint(Sender: TObject);   // SS_637_PDO_20110503
    procedure ppSubReport1Print(Sender: TObject);               // SS_637_PDO_20110503
    procedure ppDetailBand2BeforePrint(Sender: TObject);        // SS_637_PDO_20110503
    procedure lbl_NumeroCheque2Print(Sender: TObject);          // SS_637_PDO_20110503
    procedure lbl_FechaCheque2Print(Sender: TObject);           // SS_637_PDO_20110503
    procedure lbl_Banco2Print(Sender: TObject);
    procedure txt_Importe1Print(Sender: TObject);                 // SS_637_PDO_20110503
  private
    { Private declarations }
    FNumeroTurno: Integer;
    FUsuarioTurno,
    FPuntoVenta: AnsiString;
    function MostrarCheque(Refinanciacion: Boolean): Boolean;       // SS_637_PDO_20110503
    function MostrarAutorizacion(Refinanciacion: Boolean): Boolean; // SS_637_PDO_20110503
    function MostrarNroRepactacion: Boolean;    // SS_637_PDO_20110414
  public
    { Public declarations }
	function Inicializar (NumeroTurno: Integer; UsuarioTurno, PuntoVenta: AnsiString): Boolean;

  end;

var
  FormReporteAnaliticoCierreDeTurno: TFormReporteAnaliticoCierreDeTurno;

implementation


{$R *.dfm}

{ TFormReporteAnaliticoCierreDeTurno }

function TFormReporteAnaliticoCierreDeTurno.Inicializar(NumeroTurno: Integer; UsuarioTurno, PuntoVenta: AnsiString): Boolean;
var
    RutaLogo,sDireccion,sLocalidad: AnsiString;                                        //SS_1147_NDR_20140710
begin
    ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
    try                                                                                                 //SS_1147_NDR_20140710
      ppImage1.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
      ppImage2.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
    except                                                                                              //SS_1147_NDR_20140710
      On E: Exception do begin                                                                          //SS_1147_NDR_20140710
        Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
        MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
        Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
      end;                                                                                              //SS_1147_NDR_20140710
    end;                                                                                                //SS_1147_NDR_20140710

    with spObtenerMaestroConcesionaria do                                                               //SS_1147_NDR_20140710
    begin                                                                                               //SS_1147_NDR_20140710
      Close;                                                                                            //SS_1147_NDR_20140710
      Parameters.Refresh;                                                                               //SS_1147_NDR_20140710
      Parameters.ParamByName('@CodigoConcesionaria').Value := ObtenerCodigoConcesionariaNativa;         //SS_1147_NDR_20140710
      //ExecProc;   //SS_1147_MCA_20150216
      Open;         //SS_1147_MCA_20150216                                                                                //SS_1147_NDR_20140710
      sDireccion    := FieldByName('DireccionConcesionaria').AsString;                                  //SS_1147_NDR_20140710
      sLocalidad    := FieldByName('Comuna').AsString + '  ' +                                          //SS_1147_NDR_20140710
                       FieldByName('Region').AsString+ '  ' +                                           //SS_1147_NDR_20140710
                       FieldByName('Pais').AsString;                                                    //SS_1147_NDR_20140710
                                                                                                        //SS_1147_NDR_20140710
      ppLabel3.Caption := sDireccion + ' ' + sLocalidad;                                                //SS_1147_NDR_20140710
      ppLabel12.Caption := sDireccion + ' ' + sLocalidad;                                                //SS_1147_NDR_20140710
    end;                                                                                                //SS_1147_NDR_20140710


    try                                                     // SS_637_PDO_20110414
        Result := False;                                    // SS_637_PDO_20110414

        try                                                 // SS_637_PDO_20110414
            CargarConstantesCobranzasCanalesFormasPago;     // SS_637_PDO_20110414
            FNumeroTurno := NumeroTurno;
            FUsuarioTurno := UsuarioTurno;
            FPuntoVenta := PuntoVenta;
            TPanelMensajesForm.OcultaPanel;                 // SS_637_PDO_20110414
            rbi_AnaliticoCierreDeTurno.Execute;

        	Result := True;
        except                                               // SS_637_PDO_20110414
            on e: Exception do begin                         // SS_637_PDO_20110414
                ShowMsgBoxCN(e, Self);                       // SS_637_PDO_20110414
            end;                                             // SS_637_PDO_20110414
        end;                                                 // SS_637_PDO_20110414
    finally                                                  // SS_637_PDO_20110414
        TPanelMensajesForm.OcultaPanel;                      // SS_637_PDO_20110414
    end;                                                     // SS_637_PDO_20110414
end;

procedure TFormReporteAnaliticoCierreDeTurno.rbi_AnaliticoCierreDeTurnoExecute(Sender: TObject; var Cancelled: Boolean);
resourcestring
	MSG_ERROR_AL_IMPRIMIR_INFORME = 'Error al imprimir el informe.';
	MSG_ERROR = 'Error';
begin
	try
		try
			Screen.Cursor := crHourGlass;
            Application.ProcessMessages;
            
            with sp_ObtenerAnaliticoCierreDeTurno   , Parameters do begin
            	Close;
                ParamByName('@NumeroTurno').Value             := FNumeroTurno;
                ParamByName('@AnaliticoRefinanciacion').Value := 0;  // SS_637_PDO_20110503
                Open;
            end; // with

            with sp_ObtenerAnaliticoCierreDeTurnoRefinanciacion   , Parameters do begin     // SS_637_PDO_20110503
            	Close;                                                                      // SS_637_PDO_20110503
                ParamByName('@NumeroTurno').Value             := FNumeroTurno;              // SS_637_PDO_20110503
                ParamByName('@AnaliticoRefinanciacion').Value := 1;                         // SS_637_PDO_20110503
                Open;                                                                       // SS_637_PDO_20110503
            end;

            lbl_NumeroTurno1.Caption  := IntToStr(FNumeroTurno);  // SS_637_PDO_20110503
            lbl_UsuarioTurno1.Caption := FUsuarioTurno;           // SS_637_PDO_20110503
            lbl_usuario1.Caption      := FUsuarioTurno;           // SS_637_PDO_20110503
            lbl_PuntoVenta1.Caption   := FPuntoVenta;             // SS_637_PDO_20110503

            lbl_NumeroTurno2.Caption  := IntToStr(FNumeroTurno);  // SS_637_PDO_20110503
            lbl_UsuarioTurno2.Caption := FUsuarioTurno;           // SS_637_PDO_20110503
            lbl_usuario2.Caption      := FUsuarioTurno;           // SS_637_PDO_20110503
            lbl_PuntoVenta2.Caption   := FPuntoVenta;             // SS_637_PDO_20110503
		finally
			Screen.Cursor := crDefault;
		end
	except
		on E: Exception do begin
			MsgBoxErr(MSG_ERROR_AL_IMPRIMIR_INFORME, E.Message, MSG_ERROR,
            	MB_ICONERROR );
		end;
	end;
end;

{-----------------------------------------------------------------------------
  Procedure: TFormReporteAnaliticoCierreDeTurno.MostrarCheque
  Author:    ggomez
  Date:      29-Jun-2005
  Arguments: None
  Result:    Boolean
  Descripcion: Retorna True, si el la Forma de Pago del comprobante que se est�
    procesando, es Cheque o es Vale Vista.
    Adem�s, cambia el caption de los t�tulos de columnas seg�n si es Cheque o si
    es Vale Vista.

  Revision : 1
    Author : pdominguez
    Date   : 24/06/2009
    Description : SS 809
        - Se a�adi� la asignaci�n del DataField del objeto txt_Banco, debido a
        la incorporaci�n de la columna C�digo Autorizacion de las formas de
        pago Tarjeta Cr�dito y Tarjeta D�bito.

  Revision : 2
    Author : plaza
    Date   : 17/02/2010
    Description : SS 528
        - Ahora tambi�n retorna True si la forma de pago es PAGARE.
        Tambi�n cambia el caption de los titulos para esta nueva forma de pago.

-----------------------------------------------------------------------------}
function TFormReporteAnaliticoCierreDeTurno.MostrarCheque(Refinanciacion: Boolean): Boolean;
resourcestring
    MSG_CAPTION_BANCO_CHEQUE     = 'Banco Cheque';
    MSG_CAPTION_NRO_CHEQUE       = 'N� Cheque';
    MSG_CAPTION_BANCO_VALE_VISTA = 'Banco Vale Vista';
    MSG_CAPTION_NRO_VALE_VISTA   = 'N� Vale Vista';
//Revision 2
    MSG_CAPTION_FECHA_CHEQUE     = 'Fecha Vto.';
    MSG_CAPTION_BLANCO           = '';
//Fin de revision 2
var                                  // SS_637_PDO_20110503
    lCodigoFormaPago: Integer;       // SS_637_PDO_20110503
begin

//Revision 2
    Result := True;

    try
        if not Refinanciacion then begin                                                                    // SS_637_PDO_20110503
            lCodigoFormaPago := sp_ObtenerAnaliticoCierreDeTurno.FieldByName('CodigoFormaPago').AsInteger;  // SS_637_PDO_20110503

            if lCodigoFormaPago = FORMA_PAGO_CHEQUE then begin                                          // SS_637_PDO_20110503
                lbl_Banco1.Caption        := MSG_CAPTION_BANCO_CHEQUE;                                  // SS_637_PDO_20110503
                lbl_NumeroCheque1.Caption := MSG_CAPTION_NRO_CHEQUE;                                    // SS_637_PDO_20110503
                txt_Banco1.DataField      := 'Banco'; // SS (809)                                       // SS_637_PDO_20110503
            end                                                                                         // SS_637_PDO_20110503
            else if lCodigoFormaPago = FORMA_PAGO_VALE_VISTA then begin                                 // SS_637_PDO_20110503
                lbl_Banco1.Caption        := MSG_CAPTION_BANCO_VALE_VISTA;                              // SS_637_PDO_20110503
                lbl_NumeroCheque1.Caption := MSG_CAPTION_NRO_VALE_VISTA;                                // SS_637_PDO_20110503
                txt_Banco1.DataField      := 'Banco'; // SS (809)                                       // SS_637_PDO_20110503
            end                                                                                         // SS_637_PDO_20110503
            else if lCodigoFormaPago in [FORMA_PAGO_PAGARE, FORMA_PAGO_DEPOSITO_ANTICIPADO] then begin  // SS_637_PDO_20110503
                lbl_Banco1.Caption   := MSG_CAPTION_FECHA_CHEQUE;                                       // SS_637_PDO_20110503
                txt_Banco1.DataField := 'ChequeFecha';                                                  // SS_637_PDO_20110503

                lbl_NumeroCheque1.Caption   := MSG_CAPTION_BLANCO;                                      // SS_637_PDO_20110503
                txt_NumeroCheque1.DataField := MSG_CAPTION_BLANCO;                                      // SS_637_PDO_20110503
                lbl_FechaCheque1.Caption    := MSG_CAPTION_BLANCO;                                      // SS_637_PDO_20110503
                txt_FechaCheque1.DataField  := MSG_CAPTION_BLANCO;                                      // SS_637_PDO_20110503
            end                                                                                         // SS_637_PDO_20110503
            else begin                                                                                  // SS_637_PDO_20110503
                Result := False;                                                                        // SS_637_PDO_20110503
            end;                                                                                        // SS_637_PDO_20110503
        end
        else begin
            lCodigoFormaPago := sp_ObtenerAnaliticoCierreDeTurnoRefinanciacion.FieldByName('CodigoFormaPago').AsInteger;   // SS_637_PDO_20110503

            if lCodigoFormaPago = FORMA_PAGO_CHEQUE then begin                                          // SS_637_PDO_20110503
                lbl_Banco2.Caption        := MSG_CAPTION_BANCO_CHEQUE;                                  // SS_637_PDO_20110503
                lbl_NumeroCheque2.Caption := MSG_CAPTION_NRO_CHEQUE;                                    // SS_637_PDO_20110503
                txt_Banco2.DataField      := 'Banco'; // SS (809)                                       // SS_637_PDO_20110503
            end                                                                                         // SS_637_PDO_20110503
            else if lCodigoFormaPago = FORMA_PAGO_VALE_VISTA then begin                                 // SS_637_PDO_20110503
                lbl_Banco2.Caption        := MSG_CAPTION_BANCO_VALE_VISTA;                              // SS_637_PDO_20110503
                lbl_NumeroCheque2.Caption := MSG_CAPTION_NRO_VALE_VISTA;                                // SS_637_PDO_20110503
                txt_Banco2.DataField      := 'Banco'; // SS (809)                                       // SS_637_PDO_20110503
            end                                                                                         // SS_637_PDO_20110503
            else if lCodigoFormaPago in [FORMA_PAGO_PAGARE, FORMA_PAGO_DEPOSITO_ANTICIPADO] then begin  // SS_637_PDO_20110503
                lbl_Banco2.Caption   := MSG_CAPTION_FECHA_CHEQUE;                                       // SS_637_PDO_20110503
                txt_Banco2.DataField := 'ChequeFecha';                                                  // SS_637_PDO_20110503

                lbl_NumeroCheque2.Caption   := MSG_CAPTION_BLANCO;                                      // SS_637_PDO_20110503
                txt_NumeroCheque2.DataField := MSG_CAPTION_BLANCO;                                      // SS_637_PDO_20110503
                lbl_FechaCheque2.Caption    := MSG_CAPTION_BLANCO;                                      // SS_637_PDO_20110503
                txt_FechaCheque2.DataField  := MSG_CAPTION_BLANCO;                                      // SS_637_PDO_20110503
            end                                                                                         // SS_637_PDO_20110503
            else begin
                Result := False;
            end;
        end;
    except
        Result := False;
    end;
//Fin de Revision 2
end;

{******************************** Function Header ******************************
Function Name: MostrarAutorizacion
Author : pdominguez
Date Created : 24/06/2009
Parameters : None
Return Value : Boolean
Description : SS 809
    - Valida si se debe mostrar la columna C�digo de Autorizaci�n.
*******************************************************************************}
function TFormReporteAnaliticoCierreDeTurno.MostrarAutorizacion(Refinanciacion: Boolean): Boolean;
resourcestring
    MSG_CAPTION_AUTORIZACION = 'C�d. Autorizaci�n';
var
    lCodigoFormaPago: Integer;
begin
    if not Refinanciacion then begin                                                                                      // SS_637_PDO_20110503
        lCodigoFormaPago := sp_ObtenerAnaliticoCierreDeTurno.FieldByName('CodigoFormaPago').AsInteger                     // SS_637_PDO_20110503
    end                                                                                                                   // SS_637_PDO_20110503
    else begin                                                                                                            // SS_637_PDO_20110503
        lCodigoFormaPago := sp_ObtenerAnaliticoCierreDeTurnoRefinanciacion.FieldByName('CodigoFormaPago').AsInteger       // SS_637_PDO_20110503
    end;                                                                                                                  // SS_637_PDO_20110503

	Result := lCodigoFormaPago in [CONST_FORMA_PAGO_TARJETA_DEBITO, CONST_FORMA_PAGO_TARJETA_CREDITO];                    // SS_637_PDO_20110503

    if Result then begin                                                                                                  // SS_637_PDO_20110503
        if not Refinanciacion then begin                                                                                  // SS_637_PDO_20110503
            lbl_Banco1.Caption   := MSG_CAPTION_AUTORIZACION;                                                             // SS_637_PDO_20110503
            txt_banco1.DataField := 'Autorizaci�n';                                                                       // SS_637_PDO_20110503
        end                                                                                                               // SS_637_PDO_20110503
        else begin                                                                                                        // SS_637_PDO_20110503
            lbl_Banco2.Caption   := MSG_CAPTION_AUTORIZACION;                                                             // SS_637_PDO_20110503
            txt_banco2.DataField := 'Autorizaci�n';                                                                       // SS_637_PDO_20110503
        end;                                                                                                              // SS_637_PDO_20110503
    end;                                                                                                                  // SS_637_PDO_20110503
end;

procedure TFormReporteAnaliticoCierreDeTurno.lbl_RepactacionNumeroPrint(Sender: TObject);   // SS_637_PDO_20110414
begin
    TppLabel(Sender).Visible := MostrarNroRepactacion;
end;                                                                                  

function TFormReporteAnaliticoCierreDeTurno.MostrarNroRepactacion: Boolean; // SS_637_PDO_20110414
begin
    Result :=
        sp_ObtenerAnaliticoCierreDeTurnoRefinanciacion.FieldByName('CodigoTipoMedioPago').AsInteger
            in [CANAL_PAGO_AVENIMIENTO, CANAL_PAGO_REPACTACION];
end;

procedure TFormReporteAnaliticoCierreDeTurno.txt_banco1Print(Sender: TObject);  // SS_637_PDO_20110503
begin
	TppDBText(Sender).Visible :=
        TppDBText(Sender).Visible and
        MostrarCheque(False) or MostrarAutorizacion(False);
end;

procedure TFormReporteAnaliticoCierreDeTurno.txt_banco2Print(Sender: TObject);  // SS_637_PDO_20110503
begin
	TppDBText(Sender).Visible := MostrarCheque(True) Or MostrarAutorizacion(True);
end;

procedure TFormReporteAnaliticoCierreDeTurno.txt_NumeroCheque1Print(Sender: TObject);   // SS_637_PDO_20110503
begin
	TppDBText(Sender).Visible := TppDBText(Sender).Visible and MostrarCheque(False);
end;

procedure TFormReporteAnaliticoCierreDeTurno.txt_NumeroCheque2Print(Sender: TObject);   // SS_637_PDO_20110503
begin
	TppDBText(Sender).Visible := MostrarCheque(True);
end;

procedure TFormReporteAnaliticoCierreDeTurno.txt_FechaCheque1Print(Sender: TObject);    // SS_637_PDO_20110503
begin
	TppDBText(Sender).Visible := TppDBText(Sender).Visible and MostrarCheque(False);
end;

procedure TFormReporteAnaliticoCierreDeTurno.txt_FechaCheque2Print(Sender: TObject);    // SS_637_PDO_20110503
begin
	TppDBText(Sender).Visible := MostrarCheque(True);
end;

procedure TFormReporteAnaliticoCierreDeTurno.ppGroupHeaderBand4BeforePrint(Sender: TObject);    // SS_637_PDO_20110503
    var
        EsPagoCuotasRefinanciacion: Boolean;
begin
    EsPagoCuotasRefinanciacion :=
        sp_ObtenerAnaliticoCierreDeTurno.FieldByName('CodigoTipoMedioPago').AsInteger
            in [CANAL_PAGO_AVENIMIENTO, CANAL_PAGO_REPACTACION];
                                                                                              
    lbl_ColComprobante1.Visible := not EsPagoCuotasRefinanciacion;
    lbl_Fecha1.Visible          := not EsPagoCuotasRefinanciacion;                            
    lbl_FechaPago1.Visible      := not EsPagoCuotasRefinanciacion;                            
    lbl_Banco1.Visible          := not EsPagoCuotasRefinanciacion;
    lbl_NumeroCheque1.Visible   := not EsPagoCuotasRefinanciacion;                            
    lbl_FechaCheque1.Visible    := not EsPagoCuotasRefinanciacion;                            

    lbl_NroRefinanciacion.Visible := EsPagoCuotasRefinanciacion;                              
    lbl_FechaCuota.Visible        := EsPagoCuotasRefinanciacion;                              
    lbl_NroConvenio.Visible       := EsPagoCuotasRefinanciacion;                              
    lbl_MontoCuota.Visible        := EsPagoCuotasRefinanciacion;                              
    lbl_FormaPagoCuota.Visible    := EsPagoCuotasRefinanciacion;
    lbl_NroDocumentoCuota.Visible := EsPagoCuotasRefinanciacion;                              
    lbl_NroDocumentoPago.Visible  := EsPagoCuotasRefinanciacion;                              

    if not EsPagoCuotasRefinanciacion then begin                                              
        lbl_Banco1.Visible        := MostrarCheque(False) or MostrarAutorizacion(False);
        lbl_NumeroCheque1.Visible := MostrarCheque(False);                                    
        lbl_FechaCheque1.Visible  := MostrarCheque(False);
    end;                                                                                      
end;

procedure TFormReporteAnaliticoCierreDeTurno.ppSubReport1Print(Sender: TObject);    // SS_637_PDO_20110503
begin
    lbl_NroRefinanciacion.Top := 10.054;
    lbl_FechaCuota.Top        := 10.054;
    lbl_NroConvenio.Top       := 10.054;
    lbl_MontoCuota.Top        := 10.054;
    lbl_FormaPagoCuota.Top    := 10.054;                                              
    lbl_NroDocumentoCuota.Top := 10.054;
    lbl_NroDocumentoPago.Top  := 10.054;
    ppGroupHeaderBand4.Height := 18.256001;                                           

    ppdbtxt_Cuotas.Top          := 0.26499999;                                        
    ppdbtxt_NroConvenio.Top     := 0.26499999;                                        
    ppdbtxt_FechaCuota.Top      := 0.26499999;                                        
    ppdbtxt_MontoCuota.Top      := 0.26499999;
    ppdbtxt_FormaPagoCuota.Top  := 0.26499999;                                        
    ppdbtxt_NroDocCuota.Top     := 0.26499999;
    ppdbtxt_NroDocPagoCuota.Top := 0.26499999;
    ppDetailBand2.Height        := 5.027;
end;

procedure TFormReporteAnaliticoCierreDeTurno.ppDetailBand2BeforePrint(Sender: TObject); // SS_637_PDO_20110503
    var
        EsPagoCuotasRefinanciacion: Boolean;
begin
    EsPagoCuotasRefinanciacion :=
        sp_ObtenerAnaliticoCierreDeTurno.FieldByName('CodigoTipoMedioPago').AsInteger
            in [CANAL_PAGO_AVENIMIENTO, CANAL_PAGO_REPACTACION];

    ppdbtxt_Comprobante.Visible  := not EsPagoCuotasRefinanciacion;
    ppdbtxt_FechaEmision.Visible := not EsPagoCuotasRefinanciacion;
    ppdbtxt_FechaHora.Visible    := not EsPagoCuotasRefinanciacion;
    txt_banco1.Visible           := not EsPagoCuotasRefinanciacion;
    txt_NumeroCheque1.Visible    := not EsPagoCuotasRefinanciacion;
    txt_FechaCheque1.Visible     := not EsPagoCuotasRefinanciacion;

    ppdbtxt_Cuotas.Visible            := EsPagoCuotasRefinanciacion;
    ppdbtxt_NroConvenio.Visible       := EsPagoCuotasRefinanciacion;
    ppdbtxt_FechaCuota.Visible        := EsPagoCuotasRefinanciacion;
    ppdbtxt_MontoCuota.Visible        := EsPagoCuotasRefinanciacion;
    ppdbtxt_FormaPagoCuota.Visible    := EsPagoCuotasRefinanciacion;
    ppdbtxt_NroDocCuota.Visible       := EsPagoCuotasRefinanciacion;
    ppdbtxt_NroDocPagoCuota.Visible   := EsPagoCuotasRefinanciacion;
end;

procedure TFormReporteAnaliticoCierreDeTurno.lbl_NumeroCheque2Print(Sender: TObject);   // SS_637_PDO_20110503
begin
	TppDBText(Sender).Visible := MostrarCheque(True);
end;

procedure TFormReporteAnaliticoCierreDeTurno.lbl_FechaCheque2Print(Sender: TObject);    // SS_637_PDO_20110503
begin
	TppLabel(Sender).Visible := MostrarCheque(True);
end;

procedure TFormReporteAnaliticoCierreDeTurno.lbl_Banco2Print(Sender: TObject);  // SS_637_PDO_20110503
begin
    TppLabel(Sender).Visible := MostrarCheque(True) or MostrarAutorizacion(True);
end;

procedure TFormReporteAnaliticoCierreDeTurno.txt_RepactacionNumeroPrint(Sender: TObject);   // SS_637_PDO_20110503
begin
    TppDBText(Sender).Visible := MostrarNroRepactacion;
end;

procedure TFormReporteAnaliticoCierreDeTurno.txt_Importe1Print(Sender: TObject);   // SS_637_PDO_20110503
begin
    if TppDBText(Sender).Visible then begin
        if TppDBPipeline(TppDBText(Sender).DataPipeline).DataSource.DataSet.FieldByName(TppDBText(Sender).DataField).AsInteger < 0 then begin
            TppDBText(Sender).Font.Style := TppDBText(Sender).Font.Style + [fsBold];
        end
        else begin
            TppDBText(Sender).Font.Style := TppDBText(Sender).Font.Style - [fsBold];
        end;
    end;
end;







end.
