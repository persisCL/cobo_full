�
 TFRMPAGOVENTANILLACASTIGO 0/  TPF0TfrmPagoVentanillaCastigofrmPagoVentanillaCastigoLeftTop� BorderStylebsDialogCaptionCobro de Comprobante - CastigoClientHeight� ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnShowFormShow
DesignSize��  PixelsPerInch`
TextHeight TBevelBevel1Left Top Width�Height�  TLabellbl_FormaPagoLeft� TopWidthGHeightCaptionForma de pago  TLabel
lbl_MonedaLeft�TopWidthLHeightCaptionMonto a Pagar:   TLabellblFechaDePagoLeftTop<WidthIHeightCaptionFecha de Pago  TLabellbl_CanalDePagoLeftTopWidthEHeightCaptionCanal de pago  TBevelBevel2Left Top Width�HeightAlignalTopExplicitLeft�ExplicitTop�  TLabellbl1LeftyTop� Width�HeightCaption[   Se procederá a Castigar el Convenio seleccionado. Por favor verificar los datos mostrados.  TButton	btnCerrarLeftTop� WidthKHeightAnchorsakRightakBottom Cancel	Caption&CerrarTabOrderOnClickbtnCerrarClick  TNumericEdit	neImporteLeft�Top Width� HeightEnabled	MaxLength
TabOrder  	TDateEditdeFechaPagoLeftTopOWidth� Height
AutoSelectEnabledTabOrderDate     �U��  TButton
btnAceptarLeft�Top� WidthKHeightAnchorsakRightakBottom Caption&AceptarDefault	TabOrderOnClickbtnAceptarClick  TVariantComboBoxcb_CanalesDePagoLeftTop Width� HeightStylevcsDropDownListEnabled
ItemHeightTabOrder OnChangecb_CanalesDePagoChangeItems   TEditedtCodigoFormaPagoLeft� Top Width"HeightCharCaseecUpperCaseEnabled	MaxLengthTabOrderOnChangeedtCodigoFormaPagoChange  TEditedtDescFormaPagoLeft� Top Width� HeightTabStopReadOnly	TabOrder  TADOStoredProcCrearRecibo
ConnectionDMConnections.BaseCACProcedureNameCrearRecibo
ParametersName@RETURN_VALUEDataType	ftInteger	DirectionpdReturnValue	Precision
Value  Name
@FechaHora
Attributes
paNullable DataType
ftDateTimeValue  Name@Importe
Attributes
paNullable DataType
ftLargeint	Precision
Value  Name@NumeroRecibo
Attributes
paNullable DataType	ftInteger	DirectionpdInputOutput	Precision
Value  Name@CodigoTipoMedioPago
Attributes
paNullable DataTypeftWord	PrecisionValue  Name@CodigoUsuario
Attributes
paNullable DataTypeftStringSizeValue   LeftUTop6  TADOStoredProcCerrarPagoComprobante
ConnectionDMConnections.BaseCACProcedureNameCerrarPagoComprobante
ParametersName@RETURN_VALUEDataType	ftInteger	DirectionpdReturnValue	Precision
Value  Name@TipoComprobante
Attributes
paNullable DataTypeftStringSizeValue  Name@NumeroComprobante
Attributes
paNullable DataType
ftLargeint	PrecisionValue  Name
@FechaHora
Attributes
paNullable DataType
ftDateTimeValue   LeftUTop�   TADOStoredProc"sp_RegistrarDetallePagoComprobante
ConnectionDMConnections.BaseCACCursorLocationclUseServerLockTypeltBatchOptimisticCommandTimeout<ProcedureName!RegistrarDetallePagoComprobante;1
ParametersName@RETURN_VALUEDataType	ftInteger	DirectionpdReturnValue	Precision
Value  Name@IDDetallePagoComprobante
Attributes
paNullable DataType	ftInteger	DirectionpdInputOutput	Precision
Value  Name@CodigoTipoMedioPago
Attributes
paNullable DataTypeftWord	PrecisionValue  Name@CodigoFormaPago
Attributes
paNullable DataTypeftWord	PrecisionValue  Name@PAC_CodigoBanco
Attributes
paNullable DataType	ftInteger	Precision
Value  Name@PAC_CodigoTipoCuentaBancaria
Attributes
paNullable DataTypeftWord	PrecisionValue  Name@PAC_NroCuentaBancaria
Attributes
paNullable DataTypeftStringSize2Value  Name@PAC_Sucursal
Attributes
paNullable DataTypeftStringSize2Value  Name@PAC_Titular
Attributes
paNullable DataTypeftStringSizedValue  Name@PAT_CodigoTipoTarjetaCredito
Attributes
paNullable DataTypeftWord	PrecisionValue  Name@PAT_NumeroTarjetaCredito
Attributes
paNullable DataTypeftStringSizeValue  Name@PAT_FechaVencimiento
Attributes
paNullable DataTypeftStringSizeValue  Name@PAT_CodigoEmisorTarjetaCredito
Attributes
paNullable DataTypeftWord	PrecisionValue  Name@PAT_Titular
Attributes
paNullable DataTypeftStringSizedValue  Name@ChequeCodigoBanco
Attributes
paNullable DataType	ftInteger	Precision
Value  Name@ChequeNumero
Attributes
paNullable DataTypeftStringSizeValue  Name@ChequeTitular
Attributes
paNullable DataTypeftStringSizedValue  Name@ChequeFecha
Attributes
paNullable DataType
ftDateTimeValue  Name@ChequeCodigoDocumento
Attributes
paNullable DataTypeftStringSizeValue  Name@ChequeNumeroDocumento
Attributes
paNullable DataTypeftStringSizeValue  Name@ChequeMonto
Attributes
paNullable DataType
ftLargeint	PrecisionValue  Name@Cupon
Attributes
paNullable DataType	ftInteger	Precision
Value  Name@Autorizacion
Attributes
paNullable DataTypeftStringSizeValue  Name@Cuotas
Attributes
paNullable DataTypeftWord	PrecisionValue  Name
@NumeroPOS
Attributes
paNullable DataTypeftWord	PrecisionValue  Name@Web_NumeroFinalTarjetaCredito
Attributes
paNullable DataTypeftStringSizeValue  Name@Web_TipoVenta
Attributes
paNullable DataTypeftStringSizeValue  Name@Titular
Attributes
paNullable DataTypeftStringSizedValue  Name@CodigoTipoTarjeta
Attributes
paNullable DataTypeftWord	PrecisionValue  Name@CodigoBancoEmisor
Attributes
paNullable DataType	ftInteger	Precision
Value  Name@RepactacionNumero
Attributes
paNullable DataTypeftStringSizeValue   Left�Top>  TADOStoredProcsp_RegistrarPagoComprobante
ConnectionDMConnections.BaseCACCursorLocationclUseServerLockTypeltBatchOptimisticCommandTimeout<ProcedureNameRegistrarPagoComprobante;1
ParametersName@RETURN_VALUEDataType	ftInteger	DirectionpdReturnValue	Precision
Value  Name@NumeroPago
Attributes
paNullable DataType	ftInteger	DirectionpdInputOutput	Precision
Value  Name
@FechaHora
Attributes
paNullable DataType
ftDateTimeValue  Name@TipoComprobante
Attributes
paNullable DataTypeftStringSizeValue  Name@NumeroComprobante
Attributes
paNullable DataType
ftLargeint	PrecisionValue  Name@Importe
Attributes
paNullable DataType
ftLargeint	PrecisionValue  Name@EstadoPago
Attributes
paNullable DataTypeftStringSizeValue  Name@NumeroRecibo
Attributes
paNullable DataType	ftInteger	Precision
Value  Name@NumeroTurno
Attributes
paNullable DataType	ftInteger	Precision
Value  Name@Usuario
Attributes
paNullable DataTypeftStringSizeValue  Name@IDDetallePagoComprobante
Attributes
paNullable DataType	ftInteger	Precision
Value  Name@CodigoConceptoPago
Attributes
paNullable DataType	ftInteger	Precision
Value  Name@DescripcionPago
Attributes
paNullable DataTypeftStringSize<Value  Name@PagarSoloComprobanteSolicitado
Attributes
paNullable DataType	ftInteger	Precision
Value  Name@NoGenerarIntereses
Attributes
paNullable DataType	ftBooleanValue   LeftTop�   TADOStoredProc RegistrarPagoNotaCobroInfraccion
ConnectionDMConnections.BaseCACProcedureName"RegistrarPagoNotaCobroInfraccion;1
Parameters LeftUTop~  TClientDataSet
cdsChequesActive	
Aggregates AutoCalcFields	FieldDefsNameTitularDataTypeftStringSize NameRUTDataTypeftStringSize NameBancoDataTypeftStringSize NameNumeroChequeDataType	ftInteger NameFechaChequeDataTypeftDate NameMontoDataType	ftInteger NameCodigoBancoDataType	ftInteger  	IndexDefs Params 	StoreDefs	LeftTopeData
�   �   ��              � Titular I    WIDTH   RUT I    WIDTH   Banco I    WIDTH   NumeroCheque      FechaCheque      Monto      CodigoBanco          TDataSource	DsChequesDataSet
cdsChequesLeftTop�   TADOStoredProcspActualizarInfraccionesPagadas
ConnectionDMConnections.BaseCACProcedureNameActualizarInfraccionesPagadas
ParametersName@RETURN_VALUEDataType	ftInteger	DirectionpdReturnValue	Precision
Value  Name@NumeroComprobante
Attributes
paNullable DataType
ftLargeint	PrecisionValue  Name@TipoComprobante
Attributes
paNullable DataTypeftStringSizeValue   Left� Top�   TADOStoredProcspCastigarConvenio
ConnectionDMConnections.BaseCACProcedureNameCastigarConvenio
Parameters LefthTop�   TADOStoredProcsp_ActualizarClienteMoroso
ConnectionDMConnections.BaseCACProcedureNameActualizarClienteMoroso
ParametersName@NumeroDocumentoDataTypeftStringSizeValue  Name@CodigoRefinanciacionDataType	ftIntegerValue  Name@UsuarioDataTypeftStringSizeValue  Name@ErrorDescriptionDataTypeftString	DirectionpdOutputSize�Value   LeftHTopp  TADOStoredProcspValidarFormaDePagoEnCanal
ConnectionDMConnections.BaseCACProcedureNameValidarFormaDePagoEnCanal
ParametersName@RETURN_VALUEDataType	ftInteger	DirectionpdReturnValue	Precision
Value  Name@CodigoTipoMedioPago
Attributes
paNullable DataType	ftInteger	Precision
Value  Name@CodigoEntradaUsuario
Attributes
paNullable DataTypeftStringSizeValue  Name@ValidarPagoAnticipado
Attributes
paNullable DataType	ftInteger	Precision
Value  Name@CodigoUsuario
Attributes
paNullable DataTypeftStringSizeValue  Name@CodigoSistema
Attributes
paNullable DataTypeftWord	PrecisionValue  Name@CodigoFormaDePago
Attributes
paNullable DataType	ftInteger	DirectionpdInputOutput	Precision
Value  Name@DescripcionFormaDePago
Attributes
paNullable DataTypeftString	DirectionpdInputOutputSizeValue  Name@EsValidaEnCanal
Attributes
paNullable DataType	ftInteger	DirectionpdInputOutput	Precision
Value  Name@AceptaPagoParcial
Attributes
paNullable DataType	ftInteger	DirectionpdInputOutput	Precision
Value  Name@CodigoPantalla
Attributes
paNullable DataType
ftSmallint	DirectionpdInputOutput	PrecisionValue  Name@CodigoBanco
Attributes
paNullable DataType	ftInteger	DirectionpdInputOutput	Precision
Value  Name@CodigoTarjeta
Attributes
paNullable DataType	ftInteger	DirectionpdInputOutput	Precision
Value   Left� TopD  TADOStoredProcsp_ValidarFormaDePagoEnCanal
ConnectionDMConnections.BaseCACProcedureNameValidarFormaDePagoEnCanal
ParametersName@RETURN_VALUEDataType	ftInteger	DirectionpdReturnValue	Precision
Value  Name@CodigoTipoMedioPago
Attributes
paNullable DataType	ftInteger	Precision
Value  Name@CodigoEntradaUsuario
Attributes
paNullable DataTypeftStringSizeValue  Name@ValidarPagoAnticipado
Attributes
paNullable DataType	ftInteger	Precision
Value  Name@CodigoUsuario
Attributes
paNullable DataTypeftStringSizeValue  Name@CodigoSistema
Attributes
paNullable DataTypeftWord	PrecisionValue  Name@CodigoFormaDePago
Attributes
paNullable DataType	ftInteger	DirectionpdInputOutput	Precision
Value  Name@DescripcionFormaDePago
Attributes
paNullable DataTypeftString	DirectionpdInputOutputSizeValue  Name@EsValidaEnCanal
Attributes
paNullable DataType	ftInteger	DirectionpdInputOutput	Precision
Value  Name@AceptaPagoParcial
Attributes
paNullable DataType	ftInteger	DirectionpdInputOutput	Precision
Value  Name@CodigoPantalla
Attributes
paNullable DataType
ftSmallint	DirectionpdInputOutput	PrecisionValue  Name@CodigoBanco
Attributes
paNullable DataType	ftInteger	DirectionpdInputOutput	Precision
Value  Name@CodigoTarjeta
Attributes
paNullable DataType	ftInteger	DirectionpdInputOutput	Precision
Value   Left� TopD   