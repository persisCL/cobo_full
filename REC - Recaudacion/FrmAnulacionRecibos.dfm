object frm_AnulacionRecibos: Tfrm_AnulacionRecibos
  Left = 315
  Top = 149
  BorderStyle = bsDialog
  Caption = 'Anulaci'#243'n de Recibos'
  ClientHeight = 411
  ClientWidth = 660
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  DesignSize = (
    660
    411)
  PixelsPerInch = 96
  TextHeight = 13
  object gbPorCliente: TGroupBox
    Left = 5
    Top = 2
    Width = 649
    Height = 64
    Anchors = [akLeft, akTop, akRight]
    Caption = ' B'#250'squeda de Recibos'
    Color = clBtnFace
    ParentColor = False
    TabOrder = 0
    object lb_CodigoCliente: TLabel
      Left = 169
      Top = 14
      Width = 61
      Height = 13
      Caption = 'RUT Cliente:'
    end
    object Label5: TLabel
      Left = 317
      Top = 14
      Width = 103
      Height = 13
      Caption = 'N'#250'mero de Convenio:'
    end
    object Label4: TLabel
      Left = 12
      Top = 13
      Width = 74
      Height = 13
      Caption = 'N'#250'mero Recibo'
    end
    object peNumeroDocumento: TPickEdit
      Left = 169
      Top = 28
      Width = 145
      Height = 21
      CharCase = ecUpperCase
      Enabled = True
      TabOrder = 1
      OnChange = peNumeroDocumentoChange
      Decimals = 0
      EditorStyle = bteTextEdit
      OnButtonClick = peNumeroDocumentoButtonClick
    end
    object cbConvenios: TVariantComboBox
      Left = 317
      Top = 28
      Width = 209
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 2
      OnChange = cbConveniosChange
      Items = <>
    end
    object edNumeroRecibo: TNumericEdit
      Left = 12
      Top = 28
      Width = 143
      Height = 21
      TabOrder = 0
      OnChange = edNumeroReciboChange
      Decimals = 0
    end
  end
  object btnAnular: TDPSButton
    Left = 5
    Top = 378
    Width = 196
    Anchors = [akLeft, akBottom]
    Caption = 'Anular (F9)'
    Default = True
    TabOrder = 1
    OnClick = btnAnularClick
  end
  object btnSalir: TDPSButton
    Left = 579
    Top = 378
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Salir'
    TabOrder = 2
    OnClick = btnSalirClick
  end
  object GroupBox1: TGroupBox
    Left = 4
    Top = 70
    Width = 650
    Height = 60
    Anchors = [akLeft, akTop, akRight]
    Caption = ' Datos Recibo '
    TabOrder = 3
    DesignSize = (
      650
      60)
    object Label6: TLabel
      Left = 18
      Top = 18
      Width = 63
      Height = 13
      Anchors = [akLeft, akBottom]
      Caption = 'N'#186' Recibo:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblNumeroRecibo: TLabel
      Left = 92
      Top = 18
      Width = 51
      Height = 13
      Anchors = [akLeft, akTop, akRight]
      Caption = 'NroRecibo'
    end
    object lblEstado: TLabel
      Left = 238
      Top = 18
      Width = 33
      Height = 13
      Anchors = [akLeft, akTop, akRight]
      Caption = 'Estado'
    end
    object Label10: TLabel
      Left = 186
      Top = 18
      Width = 44
      Height = 13
      Caption = 'Estado:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 17
      Top = 37
      Width = 40
      Height = 13
      Anchors = [akLeft, akBottom]
      Caption = 'Fecha:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblFecha: TLabel
      Left = 92
      Top = 37
      Width = 30
      Height = 13
      Anchors = [akLeft, akTop, akRight]
      Caption = 'Fecha'
    end
    object lblMensaje: TLabel
      Left = 303
      Top = 19
      Width = 61
      Height = 13
      Anchors = [akLeft, akTop, akRight]
      Caption = 'lblMensaje'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 186
      Top = 38
      Width = 40
      Height = 13
      Caption = 'Monto:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblImporte: TLabel
      Left = 238
      Top = 38
      Width = 35
      Height = 13
      Anchors = [akLeft, akTop, akRight]
      Caption = 'Importe'
    end
  end
  object gbDatosRecibo: TGroupBox
    Left = 4
    Top = 132
    Width = 650
    Height = 181
    Anchors = [akLeft, akTop, akRight]
    Caption = ' Detalle del Recibo '
    TabOrder = 4
    object DBListEx1: TDBListEx
      Left = 6
      Top = 15
      Width = 631
      Height = 156
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 80
          Header.Caption = 'Tipo Compr.'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'DescTipoComprobante'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 70
          Header.Caption = 'N'#186' Compr.'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'NumeroDocumento'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 80
          Header.Caption = 'Fecha Pago'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'FechaHora'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 70
          Header.Caption = 'Monto'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'DescImporte'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 120
          Header.Caption = 'Convenio'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Convenio'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 70
          Header.Caption = 'RUT'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'NumeroDocumento'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 200
          Header.Caption = 'Cliente'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'NombreCliente'
        end>
      DataSource = dsComprobantesRecibo
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
    end
  end
  object gbAnulacion: TGroupBox
    Left = 6
    Top = 317
    Width = 650
    Height = 54
    Anchors = [akLeft, akTop, akRight]
    Caption = ' Anulaci'#243'n '
    Enabled = False
    TabOrder = 5
    DesignSize = (
      650
      54)
    object Label3: TLabel
      Left = 10
      Top = 22
      Width = 100
      Height = 13
      Anchors = [akLeft, akBottom]
      Caption = 'Fecha Anulaci'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label9: TLabel
      Left = 240
      Top = 22
      Width = 103
      Height = 13
      Caption = 'Motivo Anulaci'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object deFechaAnulacion: TDateEdit
      Left = 115
      Top = 18
      Width = 94
      Height = 21
      AutoSelect = False
      TabOrder = 0
      Date = -693594
    end
    object cbMotivosAnulacion: TVariantComboBox
      Left = 348
      Top = 18
      Width = 209
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 1
      Items = <>
    end
  end
  object spObtenerRecibo: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerRecibo'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@NumeroRecibo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 58
    Top = 224
  end
  object spObtenerComprobantesRecibo: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerComprobantesRecibo'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@NumeroRecibo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 40
      end>
    Left = 220
    Top = 223
  end
  object dsComprobantesRecibo: TDataSource
    DataSet = spObtenerComprobantesRecibo
    Left = 179
    Top = 224
  end
  object spObtenerMotivosAnulacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerMotivosAnulacion'
    Parameters = <>
    Left = 323
    Top = 224
  end
  object spAnularRecibo: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'AnularRecibo'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroRecibo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaAnulacion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoMotivoAnulacion'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 415
    Top = 224
  end
end
