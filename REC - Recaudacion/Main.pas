{********************************** File Header ********************************
File Name : Main.pas
Author :
Date Created:
Language : ES-AR
Description :

Revision 1:
Author : jconcheyro
Date Created: 31/10/2005
Language : ES-AR
Description : Se crea el metodo HabilitarMenuInformes para manejar el estado enlabled del
menu informes.

Revision 2:
Author : jconcheyro
Date Created: 07/08/2006
Language : ES-AR
Description : Si se desea cerrar la aplicaci�n teniendo un turno abierto, ahora no somos tan estrictos
y permitimos cerrar el turno o salir sin cerrarlo

Revision 3:
Author : jconcheyro
Date Created: 18/08/2006
Language : ES-AR
Description : Se agrega el form ReaplicarConvenio

Revision 4:
Author : jconcheyro
Date : 17/11/2006
Description : En inicializar se agrega VerificarExisteCotizacionUFDelDia
Que muestra un mensaje de aviso si no existe la Cotizacion de UF del d�a
que se usa en Arriendos de Telev�a.

Revision 4:
Author : jconcheyro
Date : 18/12/2006
Description : VerificarExisteCotizacionUFDelDia ahora se llama VerificarExistenCotizacionesDelDia
y se paso a peaprocs

Revision : 5
    Author : pdominguez
    Date   : 10/06/2009
    Description : SS 809
            - Se modific� el procedimiento RefrescarBarraDeEstado.

Revision 7:
    Date: 24/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura
Revision : 7
Date: 29/01/2010
Author: mpiazza
Description:  Ref SS-510 Se modifica mnu_sistemaClick(... se agrega una validacion por estacion de trabajo

Revision: 8
    Author : pdominguez
    Date   : 03/02/2010
    Description : SS 510
        - Se modificaron/a�adieron los siguientes objetos/constantes/variables:
            actConfiguracionPuesto: TAction

        - Se modificaron/a�adieron los siguientes procedimientos/funciones:
            actConfiguracionPuestoExecute
            HabilitarItemsTurno

            
Firma:		: SS-959-MBE-201106010
Description : Se agrega la opci�n de men� "devoluci�n de Dinero"

Firma		: SS_740_MBE_20110720
Description	:	Se agregan las opciones de Traspaso de saldos entre convenios y de b�squeda
            	de traspasos realizados


Firma       : SS-971-NDR-20110823, SS-971-PDO-20111102
Description : Se modifica posicion de opciones en el arreglo de funciones adicionales

Firma       : SS_1004_ALA_20111021
Description : Deshabilitar men� de m�dulo Cobranzas : Ventas : Autorizaci�n de pago nota cr�dito

Firma       : SS-966-NDR-20111207
Description : Agregar permisos para el BDP BCH manual

Firma       : SS_1004_NDR_20111220
Description : Deshabilitar men� de m�dulo Cobranzas : Ventas : Pago nota cr�dito


Firma       :   SS_925_NDR_20131122
Description :   Genera nuevos menu segun campo GenerarMenues de tabla SistemasVersiones (no del .ini)

Firma       :   SS_925_MBE_20131223
Description :   Se cambia la ventana de Acerca de.

Firma       :   SS_1147_CQU_20140731
Descripci�n :   Se agrega el manejo de color en el men� y algunas pantallas

Firma       :   SS_1176_NDR_20140326
Description :   Permiso para reversar refinanciacion


Firma       : SS_1147_NDR_20141216
Descripcion : Color de menues segun concesionaria nativa


Firma       : SS_1147AAA_NDR_20150414
Descripcion : Correccion a observaciones de Refinanciaci�n (Caption y StatusBar)

Autor           :   CQuezadaI
Firma           :   SS_1245_CQU_20150224
Fecha           :   24-feb-2015
Descripci�n     :   Se agrega el permiso para el bot�n de importe sujerido

Firma			:	SS_1410_MCA_20151027
Descripcion		:	se agrega nueva funcion del sistema para pagos no recuperables

Firma			:	SS_1413_20151113
Description		:	Se agregan los permisos del WebService para Ventanilla

Firma			:	SS_1414_20151119
Description		:	Se agregan los permisos de Sencillito para pago por Ventanilla

Firma       : SS_1436_NDR_20151230
Descripcion : Dejar un registro en RegistroOperaciones cuando el usuario sale del sistema  o cuando trata de ingresar con password erronea

Firma       : SS_1437_NDR_20151230
Descripcion : Que los menus de los informes se carguen segun los permisos del usuario, y que dependan del GenerarMenues del Sistema

Etiqueta    : 20160315 MGO
Descripci�n : Se utiliza InstallIni en vez de ApplicationIni

*******************************************************************************}
unit Main;

interface

uses
	frmTraspasarDeuda,					//SS_740_MBE_20110720
    frmBuscarTraspasos,					//SS_740_MBE_20110720
	frmComprobanteDevolucionDinero,		//SS_959_MBE_20130220
    frmAcercaDe,                                          //SS_925_MBE_20131223

  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Dialogs, Navigator, Menus,
  ExtCtrls, IconBars, ImgList, Buttons, StdCtrls, UtilProc, ComCtrls, ToolWin, ActnList, UtilDB,
  DmConnection, util, peaprocs, login, CacProcs, DB, ADODB, jpeg, DPSAbout, CambioPassword,
  FrmAperturaCierrePuntoVenta, PeaTypes, FrmActivarTarjeta, frmConfirmaContrasena, RStrings,
  ActnMan, frmCobroComprobantes, FrmPagoNotasCredito, ReimpresionRecibo, FrmAnaliticoCierreDeTurno,
  FrmAnulacionRecibos, frm_AutorizacionPagoNotasCredito, FrmListadoTurnos,fProtestoCheque,
  XPStyleActnCtrls, XPMan, frmReemisionNotaCobro, frmCobroAnticipado, CustomAssistance, frmCobroComprobantesBI,
  frmConveniosMorosos, frmReaplicarConvenio, FrmReimpresionNotaCobroInfractores,
  frmRefinanciacionProcesos, frmRefinanciacionABMRepLegalCN,
  frmCobroComprobantesCastigo,     //TASK_020_GLE_20161121
  frmNovedadesConvenios,
  frmAjusteCaja,                                   //TASK_031_GLE_20170111
  frmCarpetaDeudores,				//TASK_124_CFU_20170320
  ClaseBase, Categoria, FrameDatosVehiculos, TurnosCategoriasTags     //TASK_108_JMA_20170214        //TASK_125_JMA_20170404
  ,RegistrarEntregaDineroEmpresasTransporte         //TASK_125_JMA_20170404                 //TASK_127_JMA_20170405
  ,RegistrarRecepcionComprobanteDeposito;        //TASK_127_JMA_20170405
type
  TMyMenuItem = class(TMenuItem);

type
  TMainForm = class(TForm)
	PageImagenes: TImageList;
    MainMenu: TMainMenu;
	mnu_sistema: TMenuItem;
    mnu_salir: TMenuItem;
	StatusBar: TStatusBar;
	HotLinkImagenes: TImageList;
	MenuImagenes: TImageList;
	ImagenFondo: TImage;
    mnu_Ventana: TMenuItem;
    Mnu_Cascada: TMenuItem;
    mnu_Mosaico: TMenuItem;
    N2: TMenuItem;
    mnu_Siguiente: TMenuItem;
    mnu_Anterior: TMenuItem;
    mnu_Ayuda: TMenuItem;
    mnu_AcercaDe: TMenuItem;
    mnu_Mantenimiento: TMenuItem;
    mnu_CambiodeContrasena: TMenuItem;
    mnu_Informes: TMenuItem;
    mnu_AbrirTurno: TMenuItem;
	mnu_CerrarTurno: TMenuItem;
	N1: TMenuItem;
    mnuCobroComprobante: TMenuItem;
	mnu_Cobranzas: TMenuItem;
    ActualizarTurno: TADOStoredProc;
    mnu_IconBar: TMenuItem;
    mnu_Ver: TMenuItem;
    mnu_ConfigurarPuesto: TMenuItem;
    ActionManager1: TActionManager;
    actCobroComprobante: TAction;
    actAbrirTurno: TAction;
    actCerrarTurno: TAction;
    actAdicionarMontoTurno: TAction;
    mnu_AdicionarMontoenlaCaja: TMenuItem;
    mnu_AdicionarTelevias: TMenuItem;
    actAdicionarTelevias: TAction;
    N3: TMenuItem;
    N4: TMenuItem;
    mnu_ReimpresindeRecibo: TMenuItem;
    mnu_AnaliticoDeCierreDeTurno: TMenuItem;
    N5: TMenuItem;
    mnu_AnulaciondeRecibos: TMenuItem;
    mnu_CerrarTurnosTerceros: TMenuItem;
    actPagoNotaCredito: TAction;
    actReimpresionRecibo: TAction;
    actAnulacionRecibos: TAction;
    actAutorizacionPagoNotasCredito: TAction;
    actAnaliticoCierreTurnos: TAction;
    actCerrarTurnosTerceros: TAction;
    mnuRegistrarChequeProtestado: TMenuItem;
    actAnulacionChequesProtestados: TAction;
    mnu_BarraDeNavegacion: TMenuItem;
    mnu_BarraDeHotLinks: TMenuItem;
    XPManifest1: TXPManifest;
    mnu_ReemisionNotaCobro: TMenuItem;
    mnu_ChequesAlCierredeTurno: TMenuItem;
    mnu_CobroAnticipado: TMenuItem;
    actCobroAnticipado: TAction;
    mnu_CobroNotaCobroInfractores: TMenuItem;
    actConvenioMoroso: TAction;
    actReaplicarConvenio: TAction;
    mnu_ConveniosMorosos: TMenuItem;
    mnu_ReaplicarConvenio: TMenuItem;
    actReimpresionNI: TAction;
    ReimpresionNI1: TMenuItem;
    mnuTraspasos: TMenuItem;
    mnuTraspasarEntreConvenios: TMenuItem;
    mnuBuscarTraspasos: TMenuItem;
    actConfiguracionPuesto: TAction;
    Refinanciacin1: TMenuItem;
    ProcesosRefinanciaciones1: TMenuItem;
    ABMRepresentantesLegalesCN1: TMenuItem;
    mnuDevolucionDinero: TMenuItem;
    N6: TMenuItem;
    mnuCobrodeComprobanteCastigo: TMenuItem;                            //TASK_020_GLE 20161125
    mnu_EtiquetarConvenios: TMenuItem;
    actCobroComprobanteCastigo: TAction;                                //TASK_020_GLE 20161125
    mnuAjustedeCaja: TMenuItem;
    mnu_accionesdecobranza: TMenuItem;
    mnu_morosos: TMenuItem;
    mnu_planesderefinanciacion: TMenuItem;
    mnu_EmpresasDeCobranza: TMenuItem;
    mnu_CarpetaDeudores: TMenuItem;                                         //TASK_031_GLE_20170111
    mnu_RegistrarEntregadeEfectivoyCheques: TMenuItem;					//TASK_125_JMA_20170404
    mnu_N7: TMenuItem;
    mnu_RegistrarRecepcionDeposito: TMenuItem;                                         			//TASK_125_JMA_20170404
    procedure mnu_CobroNotaCobroInfractoresClick(Sender: TObject);
    procedure actConvenioMorosoExecute(Sender: TObject);
    procedure actReaplicarConvenioExecute(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure actCobroAnticipadoExecute(Sender: TObject);
    procedure mnu_ChequesAlCierredeTurnoClick(Sender: TObject);
    procedure mnu_ReemisionNotaCobroClick(Sender: TObject);
	procedure mnu_salirClick(Sender: TObject);
	procedure Cerrar1Click(Sender: TObject);
	procedure FormPaint(Sender: TObject);
	procedure FormResize(Sender: TObject);
    procedure Mnu_CascadaClick(Sender: TObject);
	procedure mnu_AcercaDeClick(Sender: TObject);
	procedure mnu_CambiodeContrasenaClick(Sender: TObject);
    procedure mnu_VentanaClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure mnu_CobranzasClick(Sender: TObject);
    procedure mnu_IconBarClick(Sender: TObject);
    procedure actCobroComprobanteExecute(Sender: TObject);
    procedure actAbrirTurnoExecute(Sender: TObject);
    procedure actCerrarTurnoExecute(Sender: TObject);
    procedure actAdicionarMontoTurnoExecute(Sender: TObject);
    procedure actAdicionarTeleviasExecute(Sender: TObject);
    procedure mnu_sistemaClick(Sender: TObject);
    procedure actReimpresionReciboExecute(Sender: TObject);
    procedure actAnulacionRecibosExecute(Sender: TObject);
    procedure actAutorizacionPagoNotasCreditoExecute(Sender: TObject);
    procedure actAnaliticoCierreTurnosExecute(Sender: TObject);
    procedure actCerrarTurnosTercerosExecute(Sender: TObject);
    procedure actPagoNotaCreditoExecute(Sender: TObject);
    procedure actAnulacionChequesProtestadosExecute(Sender: TObject);
    procedure mnu_BarraDeNavegacionClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure mnu_BarraDeHotLinksClick(Sender: TObject);
    procedure actReimpresionNIExecute(Sender: TObject);
    procedure mnuTraspasarEntreConveniosClick(Sender: TObject);
    procedure mnuBuscarTraspasosClick(Sender: TObject);
    procedure actConfiguracionPuestoExecute(Sender: TObject);
    procedure ConsultarRefinanciaciones1Click(Sender: TObject);
    procedure mnu_MantenimientoRefinanciacionesClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ProcesosRefinanciaciones1Click(Sender: TObject);
    procedure ABMRepresentantesLegalesCN1Click(Sender: TObject);
    procedure mnuDevolucionDineroClick(Sender: TObject);

    function CambiarEventoMenu(Menu: TMenu): Boolean;
    procedure actCobroComprobanteCastigoExecute(Sender: TObject);             //TASK_020_GLE_20161121
    procedure mnu_EtiquetarConveniosClick(Sender: TObject);
    procedure mnu_CargarNovedadesConveniosClick(Sender: TObject);
    procedure mnuAjustedeCajaClick(Sender: TObject);
    procedure mnu_accionesdecobranzaClick(Sender: TObject);
    procedure mnu_planesderefinanciacionClick(Sender: TObject);
    procedure mnu_EmpresasDeCobranzaClick(Sender: TObject);
    procedure mnu_CarpetaDeudoresClick(Sender: TObject);					//TASK_031_GLE_20170111
    procedure mnu_RegistrarEntregadeEfectivoyChequesClick(Sender: TObject);//TASK_125_JMA_20170404
    procedure mnu_RegistrarRecepcionDepositoClick(Sender: TObject);   //TASK_127_JMA_20170405
    //procedure mnu_InformesClick(Sender: TObject);                           //SS_1147_NDR_20141216

  private
	{ Private declarations }
	FEsCliente: Boolean;
	FNombre: Ansistring;
	FApellido: AnsiString;
	FCodigoCliente: integer;
    FNumeroPOS, FPuntoEntrega, FPuntoVenta: Integer;
    FUsaPOS: Boolean;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;              //SS_1147_NDR_20141216

	procedure InicializarVariablesCAC;
    procedure HabilitarItemsTurno(Habilitar: Boolean);
    procedure DeshabilitarTodosItemsTurnos;
    procedure ConfigurarSistemaTurno;
    procedure RefrescarBarraDeEstado;
    procedure RefrescarEstadosIconBar;
    procedure AjustarBarraDeEstado;
    procedure InicializarIconBar;
    procedure GuardarUserSettings;
    procedure HabilitarMenuInformes;
  public
	{ Public declarations }
    property NumeroPOS: integer read FNumeroPos; //TASK_005_AUN_20170403-CU.COBO.COB.301
    property PuntoEntrega: integer read FPuntoEntrega; //TASK_005_AUN_20170403-CU.COBO.COB.301
    property PuntoVenta: integer read FPuntoVenta; //TASK_005_AUN_20170403-CU.COBO.COB.301
	function Inicializa: Boolean;
    procedure DrawMenuItem(Sender: TObject; ACanvas: TCanvas;ARect: TRect; Selected: Boolean);            //SS_1147_NDR_20141216
  end;

var
  MainForm: TMainForm;

implementation

uses
    GeneradorDeIni, StartupSAU, CobranzasResources, PeaProcsCN, SysUtilsCN,
    frmRefinanciacionConsultas, frmAbmAccionesDeCobranza, frmAbmPlanesDeRefinanciacion,
    frmAbmEmpresasDeCobranza;

resourcestring
	MSG_TURNO		 	            = 'TURNO ABIERTO: %d';

    CAPTION_FIN_COMUNICACION        = 'Finalizar Comunicaci�n';
    MSG_TERMINAR_COMUNICACION       = 'Finalizar Comunicaci�n';
    MSG_NUEVA_ORDEN_SERVICIO        = 'Nueva Orden de Servicio';

    // Navegador
    NAME_FILES						= 'pagArchivos';
    NAME_SALES						= 'pagVentas';
    CAPTION_FILES					= 'Archivos';
    CAPTION_SALES					= 'Ventas';

    CAPTION_OPEN_SHIFT				= 'Abrir Turno';
    CAPTION_CLOSE_SHIFT				= 'Cerrar Turno';
    CAPTION_ADD_MONEY				= 'Adicionar Monto en la Caja';
    CAPTION_SHIFT_REPORT			= 'Anal�tico de Cierre de Caja';
    CAPTION_INVOICE_PAYMENT			= 'Cobro de Comprobantes';
    CAPTION_PUNISHMENT_PAYMENT      = 'Cobro de Comprobantes - Castigo';    //TASK_020_GLE_20161121
    CAPTION_CREDIT_PAYMENT			= 'Pago de Notas de Cr�dito';
    CAPTION_RECEIPT_REPRINT			= 'Reimpresi�n de Recibo';
    CAPTION_PAYMENT_CANCEL			= 'Anulaci�n de Recibos';
    CAPTION_PAYMENT_AUTHORIZATION	= 'Autorizaci�n de Pago de Notas de Cr�dito';
    CAPTION_CHECK_CANCEL			= 'Anulaci�n de Cheques Protestados';

    HINT_OPEN_SHIFT					= 'Permite abrir un turno';
    HINT_CLOSE_SHIFT				= 'Permite cerrar un turno';
    HINT_ADD_MONEY					= 'Permite adicionar un Monto en la Caja';
    HINT_SHIFT_REPORT				= 'Permite generar el reporte Anal�tico de Cierre de Caja';
    HINT_INVOICE_PAYMENT			= 'Permite Cobrar Comprobantes';
    HINT_CREDIT_PAYMENT				= 'Permite Pagar Notas de Cr�dito';
    HINT_RECEIPT_REPRINT			= 'Permite reimprimir un Recibo';
    HINT_PAYMENT_CANCEL				= 'Permite anular Recibos';
    HINT_PAYMENT_AUTHORIZATION		= 'Permite autorizar el de Pago de Notas de Cr�dito';
    HINT_CHECK_CANCEL				= 'Permite anular Cheques protestados';

{$R *.dfm}
{$I ..\comunes\Ambiente.inc}

{******************************** Function Header ******************************
Function Name: Inicializa
Author :
Date Created :
Description :
Parameters : None
Return Value : Boolean

Revision 1:
    Author : ggomez
    Date : 04/08/2006
    Description : Agregu� las funciones adicionales para los canales de pago:
        RegistrarPago_Santander_PEC, RegistrarPago_Santander_POC,
        RegistrarPago_MisCuentas.
Revision 2:
    Author : ggomez
    Date : 08/08/2006
    Description : Agregu� la funci�n adicional para el canal de pago
        RegistrarPago_Cobranzas.

Revision 3:
    Author : lgisuk
    Date : 02/10/2006
    Description : Agregu� la funci�n adicional para el canal de pago
        RegistrarPago_Santander_BDP.

Revision 4:
    Author : lgisuk
    Date : 24/01/2007
    Description : Agregu� la funci�n adicional para el canal de pago
        RegistrarPago_MISCUENTAS_CMR.

Revision 5:
    Author : lgisuk
    Date : 02/02/2007
    Description : Agregu� funci�nes adicionales para diez nuevos canales de pago
    genericos.

Revision 6:
    Author: nefernandez
    Date: 13/07/2007
    Description : Agregu� funciones adicionales para 2 nuevos canales de pago
    (Servipag Caja Unificado y Portal Unificado).

Revision 7:
    Author: nefernandez
    Date: 10/09/2007
    Description : Agregu� funciones adicionales para Pagos Servipag Express Unificado.

Revision 8:
Author: mbecerra
Date: 22-Agosto-2008
Description: 	Se agregaron las funciones de Servipag Por Rut (Portal, caja y Express)
*******************************************************************************}
function TMainForm.Inicializa: Boolean;
resourcestring
    // Msgs para la seguridad
    MSG_SEGURIDAD_COBRO_COMPROBANTES      	= 'Seguridad - Cobro Comprobantes';
    MSG_CHECK_SELECCION_MANUAL              = 'Pest. Comp. Impagos: Check Selecc. Manual';


    MSG_SEGURIDAD_VENTANILLA            	= 'Seguridad - Cobro por ventanilla';
    MSG_SEGURIDAD_FECHA_COBRO           	= 'Fecha de cobro';
    MSG_SEGURIDAD_MONTO_PAGADO				= 'Monto Pagado';
    MSG_VER_INFORMES_DE_TERCEROS        	= 'Ver informes de terceros';
    MSG_MODIF_FECHA_ANULACION_RECIBO    	= 'Anulaci�n Recibos - Fecha Anulaci�n Recibo';
    MSG_MODIF_FECHA_VENCIMIENTO_REEMISION	= 'Reemisi�n Nota Cobro - Fecha Vto. Reemisi�n';
    MSG_REGISTRARPAGO_NINGUNO           	= 'Registrar Pagos Canal: Ninguno';
    MSG_REGISTRARPAGO_PAT               	= 'Registrar Pagos Canal: PAT';
    MSG_REGISTRARPAGO_PAC               	= 'Registrar Pagos Canal: PAC';
    MSG_REGISTRARPAGO_WEBPAY            	= 'Registrar Pagos Canal: WebPay';
    MSG_REGISTRARPAGO_LIDER             	= 'Registrar Pagos Canal: L�der';
    MSG_REGISTRARPAGO_SERVIPAG            = 'Registrar Pagos Canal: Servipag';
    MSG_REGISTRARPAGO_VENTANILLA        	= 'Registrar Pagos Canal: Ventanilla';
    MSG_REGISTRARPAGO_SERBANC               = 'Registrar Pagos Canal: Serbanc';

    MSG_REGISTRARPAGO_SANTANDER_PEC         = 'Registrar Pagos Canal: Santander PEC';
    MSG_REGISTRARPAGO_SANTANDER_POC         = 'Registrar Pagos Canal: Santander POC';
    MSG_REGISTRARPAGO_MISCUENTAS            = 'Registrar Pagos Canal: Mis Cuentas';
    MSG_REGISTRARPAGO_MISCUENTAS_CMR        = 'Registrar Pagos Canal: Mis Cuentas - CMR';
    MSG_REGISTRARPAGO_MISCUENTAS_MEDIOPAGO_01 = 'Registrar Pagos Canal: Mis Cuentas - Medio Pago 01';
    MSG_REGISTRARPAGO_MISCUENTAS_MEDIOPAGO_02 = 'Registrar Pagos Canal: Mis Cuentas - Medio Pago 02';
    MSG_REGISTRARPAGO_MISCUENTAS_MEDIOPAGO_03 = 'Registrar Pagos Canal: Mis Cuentas - Medio Pago 03';
    MSG_REGISTRARPAGO_MISCUENTAS_MEDIOPAGO_04 = 'Registrar Pagos Canal: Mis Cuentas - Medio Pago 04';
    MSG_REGISTRARPAGO_MISCUENTAS_MEDIOPAGO_05 = 'Registrar Pagos Canal: Mis Cuentas - Medio Pago 05';
    MSG_REGISTRARPAGO_MISCUENTAS_MEDIOPAGO_06 = 'Registrar Pagos Canal: Mis Cuentas - Medio Pago 06';
    MSG_REGISTRARPAGO_MISCUENTAS_MEDIOPAGO_07 = 'Registrar Pagos Canal: Mis Cuentas - Medio Pago 07';
    MSG_REGISTRARPAGO_MISCUENTAS_MEDIOPAGO_08 = 'Registrar Pagos Canal: Mis Cuentas - Medio Pago 08';
    MSG_REGISTRARPAGO_MISCUENTAS_MEDIOPAGO_09 = 'Registrar Pagos Canal: Mis Cuentas - Medio Pago 09';
    MSG_REGISTRARPAGO_MISCUENTAS_MEDIOPAGO_10 = 'Registrar Pagos Canal: Mis Cuentas - Medio Pago 10';

    MSG_REGISTRARPAGO_SERVIPAG_PORTAL       = 'Registrar Pagos Canal: Servipag Portal';
    MSG_REGISTRARPAGO_SERVIPAG_EXPRESS      = 'Registrar Pagos Canal: Servipag Express';
    MSG_REGISTRARPAGO_SANTANDER_BPD         = 'Registrar Pagos Canal: Santander BDP';
    MSG_REGISTRARPAGO_BANCOCHILE_BDP        = 'Registrar Pagos Canal: BancoChile BDP';                   //SS-966-NDR-20111207
    MSG_REGISTRARPAGO_COBRANZAS             = 'Registrar Pagos Canal: Cobranzas';
    // Revision 6
    MSG_REGISTRARPAGO_SERVIPAG_UN_CAJA      = 'Registrar Pagos Canal: Servipag Caja Unificado';
    MSG_REGISTRARPAGO_SERVIPAG_UN_PORTAL    = 'Registrar Pagos Canal: Servipag Portal Unificado';
    // Revision 7
    MSG_REGISTRARPAGO_SERVIPAG_UN_EXPRESS   = 'Registrar Pagos Canal: Servipag Express Unificado';
    //Revision 8
    MSG_REGISTRARPAGO_SERVIPAG_RUT_PORTAL	= 'Registrar Pagos Canal: Servipag Por Rut Portal';
    MSG_REGISTRARPAGO_SERVIPAG_RUT_CAJA		= 'Registrar Pagos Canal: Servipag Por Rut Caja';
    MSG_REGISTRARPAGO_SERVIPAG_RUT_EXPRESS	= 'Registrar Pagos Canal: Servipag Por Rut Express';
    MSG_ABM_REPRESENTANTES_LEGALES_CN       = 'ABM Representantes Legales CN';

    MSG_SEGURIDAD_REFINANCIACION          = 'Seguridad - Refinanciaci�n';

    MSG_SEGURIDAD_DEPOSITO_REF            = 'Procesos Ref. - Generar Dep�sito';
    MSG_SEGURIDAD_PAGAR_PROTEST_CHEQ_REF  = 'Procesos Ref. - Pagar / Protestar Cheques';

    MSG_SEGURIDAD_NUEVA_REF               = 'Mantenimiento Ref. - Nueva';
    MSG_SEGURIDAD_EDITAR_REF              = 'Mantenimiento Ref. - Editar / Consultar';
    MSG_SEGURIDAD_A_TRAMITE_REF           = 'Mantenimiento Ref. - A Tr�mite';
    MSG_SEGURIDAD_ANULAR_REF              = 'Mantenimiento Ref. - Anular';
    MSG_SEGURIDAD_PROTESTAR_REF           = 'Mantenimiento Ref. - Protestar';
    MSG_SEGURIDAD_CUOTAS_REF              = 'Mantenimiento Ref. - Gesti�n Cuotas';
    MSG_SEGURIDAD_RECIBOS_REF             = 'Mantenimiento Ref. - Gesti�n Recibos';          // SS_989_PDO_20120119
    MSG_SEGURIDAD_RECIBOS_IMPRIMIR_REF    = 'Mantenimiento Ref. - Gest. Recibos - Imprimir'; // SS_989_PDO_20120119
    MSG_SEGURIDAD_RECIBOS_ANULAR_REF      = 'Mantenimiento Ref. - Gest. Recibos - Anular';   // SS_989_PDO_20120119
    MSG_SEGURIDAD_REVERSAR_REFINANCIACION = 'Mantenimiento Ref. - Reversar';                 //SS_1176_NDR_20140326;
    MSG_SEGURIDAD_PAGAR_REFINANCIACION    = 'Mantenimiento Ref. - Pagar';                    //TASK_005_AUN_20170403-CU.COBO.COB.301

    MSG_SEGURIDAD_FECHA_GESTION_REF       = 'Gesti�n Ref. - Fecha Refinanciaci�n';
    MSG_SEGURIDAD_REP_LEGAL_CN_REF        = 'Gesti�n Ref. - Alta / Modificaci�n Rep. Legal CN';
    MSG_SEGURIDAD_ADD_CUOTA_REF           = 'Gesti�n Ref. - A�adir Cuota';
    MSG_SEGURIDAD_EDITAR_CUOTA_REF        = 'Gesti�n Ref. - Editar Cuota';
    MSG_SEGURIDAD_ELIMINAR_CUOTA_REF      = 'Gesti�n Ref. - Eliminar Cuota';
    MSG_SEGURIDAD_ANULAR_CUOTA_REF        = 'Gesti�n Ref. - Anular Cuota';
    MSG_SEGURIDAD_IMPRIMIR_DEFINITIVO     = 'Gesti�n Ref. - Impresi�n Definitiva';
    MSG_SEGURIDAD_IMPRIMIR_HIST_PAGOS_REF = 'Gesti�n Ref. - Hist. Pagos - Imprimir Recibo';
    MSG_SEGURIDAD_BOTON_REF               = 'Gesti�n Ref. - Sugerir Importe';  // SS_1245_CQU_20150224

    MSG_SEGURIDAD_FECHA_COBRO_REF         = 'Pago Cuotas - Fecha de cobro';
    MSG_SEGURIDAD_MONTO_PAGADO_REF        = 'Pago Cuotas - Monto Pagado';

    MSG_SEGURIDAD_COBRAR_CUOTA_REF        = 'Cobro Comprobantes - Cobrar Cuota';
    MSG_SEGURIDAD_ACTIVAR_REF             = 'Cobro Comprobantes - Activar Refinanciaci�n';
    MSG_SEGURIDAD_COMBO_REF               = 'Cobro Comprobantes - Selector Refinanciaciones';

    MSG_SEGURIDAD_VENTAS                  = 'Seguridad - Ventas';    //SS-971-PDO-20111102
    MSG_REGISTRARPAGO_NO_RECUPERABLE     = 'Forma de Pago - No Recuperable';					//SS_1410_MCA_20151027

    // Pagos a tarv�s del Web Service															 // SS_1413_20151113
    MSG_WEBS_ServiEstadoCN					= 'WebService Servi Estado CN';						// SS_1413_20151113
    MSG_WEBS_CajaVecinaCN					= 'WebService Caja Vecina CN';						// SS_1413_20151113
    MSG_WEBS_OnLineBcoEstadoCN			   	= 'WebService On Line Bco.Estado CN'; 				// SS_1413_20151113
    MSG_WEBS_CajaUnired			   			= 'WebService Caja Unired';							// SS_1413_20151113
    MSG_WEBS_WebUNired			   			= 'WebService Web Unired';							// SS_1413_20151113
    MSG_WEBS_TBKUnired			   			= 'WebService Transbank Unired';					// SS_1413_20151113
    MSG_WEBS_ServiEstadoVS					= 'WebService Servi Estado VS';						// SS_1413_20151113
    MSG_WEBS_CajaVecinaVS					= 'WebService Caja Vecina VS';						// SS_1413_20151113
    MSG_WEBS_OnLineBcoEstadoVS			   	= 'WebService OnLine Bco.Estado VS'; 				// SS_1413_20151113

    MSG_SENCILLITO_X_VENTANILLA				= 'Sencillito Pago por Ventanilla';					// SS_1414_20151119

const
    FuncionesAdicionales: Array[1..80] of TPermisoAdicional =                          			//TASK_005_AUN_20170403-CU.COBO.COB.301
        (
                (Funcion: 'seguridad_cobro_comprobantes';           Descripcion: MSG_SEGURIDAD_COBRO_COMPROBANTES;           Nivel: 1),
                (Funcion: 'check_seleccion_manual';            	    Descripcion: MSG_CHECK_SELECCION_MANUAL;              	 Nivel: 2),
                (Funcion: 'seguridad_cobro_ventanilla';             Descripcion: MSG_SEGURIDAD_VENTANILLA;                 	 Nivel: 1),
                (Funcion: 'modif_fecha_cobro';              	    Descripcion: MSG_SEGURIDAD_FECHA_COBRO;                	 Nivel: 2),
                (Funcion: 'modif_monto_pagado';             	    Descripcion: MSG_SEGURIDAD_MONTO_PAGADO;               	 Nivel: 2),
                (Funcion: 'RegistrarPago_PAT';              	    Descripcion: MSG_REGISTRARPAGO_PAT;                    	 Nivel: 2),
                (Funcion: 'RegistrarPago_PAC';              	    Descripcion: MSG_REGISTRARPAGO_PAC;                    	 Nivel: 2),
                (Funcion: 'RegistrarPago_WebPay';           	    Descripcion: MSG_REGISTRARPAGO_WEBPAY;                   Nivel: 2),
                (Funcion: 'RegistrarPago_Lider';                    Descripcion: MSG_REGISTRARPAGO_LIDER;                    Nivel: 2),
                (Funcion: 'RegistrarPago_Servipag';                 Descripcion: MSG_REGISTRARPAGO_SERVIPAG;           	     Nivel: 2),
                (Funcion: 'RegistrarPago_Serbanc';                  Descripcion: MSG_REGISTRARPAGO_SERBANC;           	     Nivel: 2),
                (Funcion: 'RegistrarPago_Ventanilla';       	    Descripcion: MSG_REGISTRARPAGO_VENTANILLA;               Nivel: 2),
                (Funcion: 'RegistrarPago_Santander_PEC';            Descripcion: MSG_REGISTRARPAGO_SANTANDER_PEC;      	     Nivel: 2),
                (Funcion: 'RegistrarPago_Santander_POC';            Descripcion: MSG_REGISTRARPAGO_SANTANDER_POC;            Nivel: 2),
                (Funcion: 'RegistrarPago_MisCuentas';               Descripcion: MSG_REGISTRARPAGO_MISCUENTAS;               Nivel: 2),
                (Funcion: 'RegistrarPago_MisCuentas_CMR';           Descripcion: MSG_REGISTRARPAGO_MISCUENTAS_CMR;           Nivel: 2),

                (Funcion: 'RegistrarPago_MisCuentas_MedioPago_01';  Descripcion: MSG_REGISTRARPAGO_MISCUENTAS_MEDIOPAGO_01;  Nivel: 2),
                (Funcion: 'RegistrarPago_MisCuentas_MedioPago_02';  Descripcion: MSG_REGISTRARPAGO_MISCUENTAS_MEDIOPAGO_02;  Nivel: 2),
                (Funcion: 'RegistrarPago_MisCuentas_MedioPago_03';  Descripcion: MSG_REGISTRARPAGO_MISCUENTAS_MEDIOPAGO_03;  Nivel: 2),
                (Funcion: 'RegistrarPago_MisCuentas_MedioPago_04';  Descripcion: MSG_REGISTRARPAGO_MISCUENTAS_MEDIOPAGO_04;  Nivel: 2),
                (Funcion: 'RegistrarPago_MisCuentas_MedioPago_05';  Descripcion: MSG_REGISTRARPAGO_MISCUENTAS_MEDIOPAGO_05;  Nivel: 2),
                (Funcion: 'RegistrarPago_MisCuentas_MedioPago_06';  Descripcion: MSG_REGISTRARPAGO_MISCUENTAS_MEDIOPAGO_06;  Nivel: 2),
                (Funcion: 'RegistrarPago_MisCuentas_MedioPago_07';  Descripcion: MSG_REGISTRARPAGO_MISCUENTAS_MEDIOPAGO_07;  Nivel: 2),
                (Funcion: 'RegistrarPago_MisCuentas_MedioPago_08';  Descripcion: MSG_REGISTRARPAGO_MISCUENTAS_MEDIOPAGO_08;  Nivel: 2),
                (Funcion: 'RegistrarPago_MisCuentas_MedioPago_09';  Descripcion: MSG_REGISTRARPAGO_MISCUENTAS_MEDIOPAGO_09;  Nivel: 2),
                (Funcion: 'RegistrarPago_MisCuentas_MedioPago_10';  Descripcion: MSG_REGISTRARPAGO_MISCUENTAS_MEDIOPAGO_10;  Nivel: 2),

                (Funcion: 'RegistrarPago_Servipag_Portal';     	    Descripcion: MSG_REGISTRARPAGO_SERVIPAG_PORTAL;    	     Nivel: 2),
                (Funcion: 'RegistrarPago_Servipag_Express';    	    Descripcion: MSG_REGISTRARPAGO_SERVIPAG_EXPRESS;         Nivel: 2),
                (Funcion: 'RegistrarPago_Santander_BDP';            Descripcion: MSG_REGISTRARPAGO_SANTANDER_BPD;      	     Nivel: 2),
                (Funcion: 'RegistrarPago_BancoDeChile_BDP';         Descripcion: MSG_REGISTRARPAGO_BANCOCHILE_BDP;      	   Nivel: 2), //SS-966-NDR-20111207
                (Funcion: 'RegistrarPago_Cobranzas';       	        Descripcion: MSG_REGISTRARPAGO_COBRANZAS;         	     Nivel: 2),
                // Revision 6
                (Funcion: 'RegistrarPago_Servipag_Un_Caja';      Descripcion: MSG_REGISTRARPAGO_SERVIPAG_UN_CAJA;      Nivel: 2),       //SS-971-NDR-20110823
                (Funcion: 'RegistrarPago_Servipag_Un_Portal';    Descripcion: MSG_REGISTRARPAGO_SERVIPAG_UN_PORTAL;    Nivel: 2),       //SS-971-NDR-20110823
                // Revision 7                                                                                                           //SS-971-NDR-20110823
                (Funcion: 'RegistrarPago_Servipag_Un_Expr';      Descripcion: MSG_REGISTRARPAGO_SERVIPAG_UN_EXPRESS;   Nivel: 2),       //SS-971-NDR-20110823
                //Revision 8                                                                                                            //SS-971-NDR-20110823
                (Funcion: 'RegistrarPago_ServiPag_Rut_Portal';	 Descripcion: MSG_REGISTRARPAGO_SERVIPAG_RUT_PORTAL;   Nivel: 2),       //SS-971-NDR-20110823
                (Funcion: 'RegistrarPago_ServiPag_Rut_Caja';	 Descripcion: MSG_REGISTRARPAGO_SERVIPAG_RUT_CAJA;	   Nivel: 2),       //SS-971-NDR-20110823
                (Funcion: 'RegistrarPago_ServiPag_Rut_Express';  Descripcion: MSG_REGISTRARPAGO_SERVIPAG_RUT_EXPRESS;  Nivel: 2),       //SS-971-NDR-20110823

                (Funcion: 'RegistrarPago_Sencillito';  				Descripcion: MSG_SENCILLITO_X_VENTANILLA;  Nivel: 2),			// SS_1414_20151119
                (Funcion: 'RegPago_WebService_ServiEstadoCN';		Descripcion: MSG_WEBS_ServiEstadoCN;		Nivel: 2),         	// SS_1413_20151113
                (Funcion: 'RegPago_WebService_CajaVecinaCN';		Descripcion: MSG_WEBS_CajaVecinaCN;			Nivel: 2),         	// SS_1413_20151113
                (Funcion: 'RegPago_WebService_OnlineBcoEstadoCN';	Descripcion: MSG_WEBS_OnLineBcoEstadoCN;	Nivel: 2),       	// SS_1413_20151113
                (Funcion: 'RegPago_WebService_CajaUnired';  		Descripcion: MSG_WEBS_CajaUnired; 			Nivel: 2),         	// SS_1413_20151113
                (Funcion: 'RegPago_WebService_WebUnired';			Descripcion: MSG_WEBS_WebUNired;			Nivel: 2),         	// SS_1413_20151113
                (Funcion: 'RegPago_WebService_TBKUnired';			Descripcion: MSG_WEBS_TBKUnired;			Nivel: 2),         	// SS_1413_20151113
                (Funcion: 'RegPago_WebService_ServiEstadoVS';		Descripcion: MSG_WEBS_ServiEstadoVS;		Nivel: 2),          // SS_1413_20151113
                (Funcion: 'RegPago_WebService_CajaVecinaVS';		Descripcion: MSG_WEBS_CajaVecinaVS;			Nivel: 2),       	// SS_1413_20151113
                (Funcion: 'RegPago_WebService_OnlineBcoEstadoVS'; 	Descripcion: MSG_WEBS_OnLineBcoEstadoVS;	Nivel: 2),       	// SS_1413_20151113
                  
                (Funcion: 'seguridad_ventas';              	   	 Descripcion: MSG_SEGURIDAD_VENTAS;                  Nivel: 1),           //SS-971-PDO-20111102
                (Funcion: 'Modif_Fecha_Anulacion_Recibo';        Descripcion: MSG_MODIF_FECHA_ANULACION_RECIBO;  	   Nivel: 2),           //SS-971-PDO-20111102
                (Funcion: 'Modif_Fecha_Vencimiento_Reemision';   Descripcion: MSG_MODIF_FECHA_VENCIMIENTO_REEMISION; Nivel: 2),           //SS-971-PDO-20111102
                (Funcion: 'seguridad_cobro_Refinanciacion';      Descripcion: MSG_SEGURIDAD_REFINANCIACION;           Nivel: 1),
                (Funcion: 'deposito_proc_ref';               Descripcion: MSG_SEGURIDAD_DEPOSITO_REF;             Nivel: 2),
                (Funcion: 'pagar_protest_cheq_proc_ref';     Descripcion: MSG_SEGURIDAD_PAGAR_PROTEST_CHEQ_REF;   Nivel: 2),

                (Funcion: 'nueva_mant_ref';                  Descripcion: MSG_SEGURIDAD_NUEVA_REF;                Nivel: 2),
                (Funcion: 'editar_mant_ref';                 Descripcion: MSG_SEGURIDAD_EDITAR_REF;               Nivel: 2),
                (Funcion: 'a_tramite_mant_ref';              Descripcion: MSG_SEGURIDAD_A_TRAMITE_REF;            Nivel: 2),
                (Funcion: 'anular_mant_ref';                 Descripcion: MSG_SEGURIDAD_ANULAR_REF;               Nivel: 2),
				(Funcion: 'Reversar_Refinanciacion';         Descripcion: MSG_SEGURIDAD_REVERSAR_REFINANCIACION;  Nivel: 2),         //SS_1176_NDR_20140326
                (Funcion: 'protestar_mant_ref';              Descripcion: MSG_SEGURIDAD_PROTESTAR_REF;            Nivel: 2),
                (Funcion: 'gest_cuotas_mant_ref';            Descripcion: MSG_SEGURIDAD_CUOTAS_REF;               Nivel: 2),
                (Funcion: 'gest_recibos_mant_ref';           Descripcion: MSG_SEGURIDAD_RECIBOS_REF;              Nivel: 2),        // SS_989_PDO_20120119
                (Funcion: 'gest_recibos_impr_ref';           Descripcion: MSG_SEGURIDAD_RECIBOS_IMPRIMIR_REF;     Nivel: 2),        // SS_989_PDO_20120119
                (Funcion: 'gest_recibos_anul_ref';           Descripcion: MSG_SEGURIDAD_RECIBOS_ANULAR_REF;       Nivel: 2),        // SS_989_PDO_20120119
                (Funcion: 'pagar_mant_ref';                  Descripcion: MSG_SEGURIDAD_PAGAR_REFINANCIACION;     Nivel: 2),        //TASK_005_AUN_20170403-CU.COBO.COB.301

                (Funcion: 'modif_fecha_gestion_ref';         Descripcion: MSG_SEGURIDAD_FECHA_GESTION_REF;        Nivel: 2),
                (Funcion: 'modif_rep_legal_cn_gestion_ref';  Descripcion: MSG_SEGURIDAD_REP_LEGAL_CN_REF;         Nivel: 2),
                (Funcion: 'imprimir_hist_pagos_ref';         Descripcion: MSG_SEGURIDAD_IMPRIMIR_HIST_PAGOS_REF;  Nivel: 2),
                (Funcion: 'add_cuota_gestion_ref';           Descripcion: MSG_SEGURIDAD_ADD_CUOTA_REF;            Nivel: 2),
                (Funcion: 'editar_cuota_gestion_ref';        Descripcion: MSG_SEGURIDAD_EDITAR_CUOTA_REF;         Nivel: 2),
                (Funcion: 'eliminar_cuota_gestion_ref';      Descripcion: MSG_SEGURIDAD_ELIMINAR_CUOTA_REF;       Nivel: 2),
                (Funcion: 'anular_cuota_gestion_ref';        Descripcion: MSG_SEGURIDAD_ANULAR_CUOTA_REF;         Nivel: 2),
                (Funcion: 'imprimir_definitivo_gestion_ref'; Descripcion: MSG_SEGURIDAD_IMPRIMIR_DEFINITIVO;      Nivel: 2),
				(Funcion: 'boton_sugerir_importe_ref';       Descripcion: MSG_SEGURIDAD_BOTON_REF;                Nivel: 2),    // SS_1245_CQU_20150224
                
                (Funcion: 'modif_fecha_cobro_ref';           Descripcion: MSG_SEGURIDAD_FECHA_COBRO_REF;          Nivel: 2),
                (Funcion: 'modif_monto_pagado_ref';          Descripcion: MSG_SEGURIDAD_MONTO_PAGADO_REF;         Nivel: 2),

                (Funcion: 'cobrar_cuota_cob_comp_ref';       Descripcion: MSG_SEGURIDAD_COBRAR_CUOTA_REF;         Nivel: 2),
                (Funcion: 'activar_ref_Cob_comp_ref';        Descripcion: MSG_SEGURIDAD_ACTIVAR_REF;              Nivel: 2),
                (Funcion: 'Combo_ref_cob_comp_ref';          Descripcion: MSG_SEGURIDAD_COMBO_REF;                Nivel: 2),
                (Funcion: 'Forma_Pago_No_Recuperable';		 Descripcion: MSG_REGISTRARPAGO_NO_RECUPERABLE;       Nivel: 2),					//SS_1410_MCA_20151027
			    (Funcion: 'ver_informes_de_terceros';		 Descripcion: MSG_VER_INFORMES_DE_TERCEROS;       	  Nivel: 1)

         );

var
	f: TLoginForm;
begin
	Result := False;
    Application.Title := MSG_COBRANZAS_TITLE;
    SistemaActual := SYS_COBRANZAS;
    Width   := Screen.WorkAreaWidth;
    Height  := Screen.WorkAreaHeight;
    Left    := Screen.WorkAreaLeft;
    Top     := Screen.WorkAreaTop;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clBtnFace'));                    //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clBlack'));                      //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));                    //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));                   //SS_1147_NDR_20141216
    CambiarEventoMenu(MainMenu);                                                                                      //SS_1147_NDR_20141216

    try
        try
            // Login
            Application.CreateForm(TLoginForm, f);
            if f.Inicializar(DMConnections.BaseCAC, SYS_COBRANZAS) and (f.ShowModal = mrOk) then begin
                UsuarioSistema := f.CodigoUsuario;

                //BEGIN : SS_925_NDR_20131122-----------------------------------------------------------------------
                //if not ValidarVersion(DMConnections.BaseCAC, SistemaActual) then begin
                //    MsgBoxErr(MSG_ERROR_VERSION, MSG_ERROR_VERSION_APLICACION, self.Caption, MB_ICONSTOP);
                //    Exit;
                //end;

                InicializarVariablesCac;
                // Men�es
                //if (InstallIni.ReadInteger('General', 'GenerarMenues', 1) = 1) and
                //    not GenerateMenuFile(MainMenu, SYS_COBRANZAS, DMConnections.BaseCAC, FuncionesAdicionales)
                //    then Exit;

                //INICIO:	20160801 CFU
                //CargarInformes(DMConnections.BaseCAC, SYS_COBRANZAS, MainMenu);            //SS_1437_NDR_20151230
                //TERMINO:	20160801 CFU
                if not ValidarVersion(DMConnections.BaseCAC, SistemaActual, MainMenu, FuncionesAdicionales) then begin
                    MsgBoxErr(MSG_ERROR_VERSION, MSG_ERROR_VERSION_APLICACION, self.Caption, MB_ICONSTOP);
                    Exit;
                end;
                //END : SS_925_NDR_20131122-----------------------------------------------------------------------

                Screen.Cursor := crHourGlass;
                { INICIO : 20160315 MGO
                FPuntoEntrega := ApplicationIni.ReadInteger('General', 'PuntoEntrega', -1);
                FPuntoVenta   := ApplicationIni.ReadInteger('General', 'PuntoVenta', -1);
                FUsaPOS       := ApplicationIni.ReadInteger('General', 'UsaPOSNET', -1) = 1;
                }
                FPuntoEntrega := InstallIni.ReadInteger('General', 'PuntoEntrega', -1);
                FPuntoVenta   := InstallIni.ReadInteger('General', 'PuntoVenta', -1);
                FUsaPOS       := InstallIni.ReadInteger('General', 'UsaPOSNET', -1) = 1;
                // FIN : 20160315 MGO

                if not VerificarPuntoEntregaVenta(DMConnections.BaseCAC, FPuntoEntrega, FPuntoVenta) then begin
                    MsgBox(MSG_ERROR_PUNTO_ENTREGA_VENTA, self.Caption, MB_ICONWARNING);
                    FPuntoEntrega := -1;
                    FPuntoVenta := -1;
                    actAbrirTurno.Enabled := False;
                    actCerrarTurno.Enabled := False;
                    actAdicionarMontoTurno.Enabled := False;
                    actAdicionarTelevias.Enabled := False;
                end;

                CargarAccesos(DMConnections.BaseCAC, UsuarioSistema, SYS_COBRANZAS);
                HabilitarPermisosMenu(Menu);
                //CargarInformes(DMConnections.BaseCAC, SYS_COBRANZAS, MainMenu, DMConnections.cnInformes);            //SS_1437_NDR_20151230

                //INICIO:	20160801 CFU
                //HabilitarMenuInformes;

                //CambiarEventoMenu(MainMenu);                                    //SS_1147_NDR_20141216
                //TERMINO: 20160801 CFU

                //INICIO:	20160801 CFU
                CargarInformes(DMConnections.BaseCAC, SYS_COBRANZAS, MainMenu);            //SS_1437_NDR_20151230
                CambiarEventoMenu(MainMenu);
                MainForm.Update;
                MainForm.Repaint;
                //TERMINO:	20160801 CFU

                if (FPuntoEntrega > 0) and  (FPuntoVenta > 0)  then begin
                // Si el P.Entrega y el P.Venta no son validos no actualizo
                // ni reconfiguro el sistema hasta que el usuario lo haga manualmente.
                    ConfigurarSistemaTurno;
                    RefrescarBarraDeEstado;
                end;


                //INICIO: 	20160801 CFU
                //MainForm.Update;
                //MainForm.Refresh;
                //MainForm.Repaint;
                //TERMINO:	20160801 CFU

                Result := True;
            end
            else                                                                    //SS_1436_NDR_20151230
            begin                                                                   //SS_1436_NDR_20151230
                UsuarioSistema := f.CodigoUsuario;                                  //SS_1436_NDR_20151230
            end;                                                                    //SS_1436_NDR_20151230
        except
            on e: Exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
        f.Release;
        if Assigned(FormStartup) then FreeAndNil(FormStartup);
    end;

    if Result then InicializarIconBar;
end;

procedure TMainForm.mnu_salirClick(Sender: TObject);
begin
	Close;
end;

procedure TMainForm.Cerrar1Click(Sender: TObject);
begin
	Close;
end;

procedure TMainForm.InicializarVariablesCAC;
begin
    FEsCliente  					:= False;
	FCodigoCLiente 					:= 0;
    FApellido						:= '';
    FNombre							:= '';
end;

//TASK_003_AUN_20170313-CU.COBO.ADM.COB.104
procedure TMainForm.mnu_accionesdecobranzaClick(Sender: TObject);
var
 f: TAbmAccionesDeCobranzaForm;
begin
    if FindFormOrCreate(TAbmAccionesDeCobranzaForm, f) then
        f.Show
    else begin
        if f.Inicializa then f.Show
        else f.Release;
    end;
end;

//TASK_004_AUN_20170314-CU.COBO.ADM.COB.101
procedure TMainForm.mnu_planesderefinanciacionClick(Sender: TObject);
var
 f: TAbmPlanesDeRefinanciacionForm;
begin
    if FindFormOrCreate(TAbmPlanesDeRefinanciacionForm, f) then
        f.Show
    else begin
        if f.Inicializa then f.Show
        else f.Release;
    end;
end;

//TASK_001_AUN_20170308-CU.COBO.ADM.COB.102
procedure TMainForm.mnu_EmpresasDeCobranzaClick(Sender: TObject);
var
    f: TAbmEmpresasDeCobranzaForm;
begin
    if FindFormOrCreate(TAbmEmpresasDeCobranzaForm, f) then
        f.Show
    else begin
        if f.Inicializa then f.Show
        else f.Release;
    end;
end;

procedure TMainForm.FormPaint(Sender: TObject);
begin
	DrawMDIBackground(ImagenFondo.Picture.Graphic);
end;

procedure TMainForm.FormResize(Sender: TObject);
begin
	DrawMDIBackground(ImagenFondo.Picture.Graphic);
    AjustarBarraDeEstado;
end;

procedure TMainForm.Mnu_CascadaClick(Sender: TObject);
begin
	if sender = mnu_cascada then cascade
	  else if sender = mnu_mosaico then tile
	  else if sender = mnu_siguiente then next
	  else if sender = mnu_anterior then previous;
end;

{-----------------------------------------------------------------------------
  Function Name : TMainForm.mnuAjustedeCajaClick
  Author        : gleon
  Date Created  : 11/01/2017
  Description   : abre formulario para ajuste de caja
  Parameters    : Sender: TObject
  Return Value  : None
  firma         : TASK_031_GLE_20170111
-----------------------------------------------------------------------------}
procedure TMainForm.mnuAjustedeCajaClick(Sender: TObject);
resourcestring
    MSG_ERROR_TURNO_CERRADO     = 'Debe abrir un turno para Ajustar el Importe en Caja de un turno.';
    MSG_ERRORPERMISOS           = 'No posee permisos para registrar Ajustes en Importes Efectivos de Turnos';
    CAPTION_AJUSTE_CAJA     	= 'Ajuste de Caja - Cambio Efectivo';
var
    f: TFormAjusteCaja;
	DatosTurno: TDatosTurno;
begin
    if ExisteAcceso('mnuAjustedeCaja') then begin
        (* Verificar que el turno que se est� por utilizar est� abierto y sea del
        usuario logueado. *)
        if VerTurnoAbierto(DMConnections.BaseCAC, FPuntoEntrega, FPuntoVenta, UsuarioSistema, DatosTurno) then begin
            GNumeroTurno := DatosTurno.NumeroTurno;
        end else begin
            if (DatosTurno.NumeroTurno <> -1)
                    and ((DatosTurno.Estado = TURNO_ESTADO_ABIERTO)
                    and (Trim(DatosTurno.CodigoUsuario) <> (UpperCase(UsuarioSistema)))) then begin
                MsgBox(Format(MSG_ERROR_TURNO_ABIERTO_OTRO_USUARIO, [DatosTurno.CodigoUsuario]),
                    CAPTION_AJUSTE_CAJA, MB_ICONSTOP);
            end else begin
                MsgBox(MSG_ERROR_TURNO_CERRADO, CAPTION_AJUSTE_CAJA, MB_ICONSTOP);

            end;
            Exit;
        end;
    end else begin
        MsgBox(MSG_ERRORPERMISOS, CAPTION_AJUSTE_CAJA, MB_ICONSTOP);
        Exit;
    end;

    try
        f := TFormAjusteCaja.Create(Self);
        if f.Inicializar(GNumeroTurno, UsuarioSistema, FPuntoVenta, ExisteAcceso('mnuAjustedeCaja')) then begin
            f.ShowModal;
        end;

    finally
        if Assigned(f) then FreeAndNil(f);
    end;
end;

{----------------------------------------------------------
                  mnuBuscarTraspasosClick

Author			: mbecerra
Date			: 20-Julio-2011
Description 	:	Se invoca al formulario que permite la b�squeda
                	de traspasos
-------------------------------------------------------------------}
procedure TMainForm.mnuBuscarTraspasosClick(Sender: TObject);
resourcestring
	MSG_CAPTION = 'B�squeda de Traspasos entre Convenios';
    
var
	f : TBuscarTraspasosForm;
begin
    if FindFormOrCreate(TBuscarTraspasosForm, f) then f.Show
    else if not f.Inicializar(MSG_CAPTION) then f.Release;

end;

{----------------------------------------------------------
                  mnuTraspasarEntreConveniosClick

Author			: mbecerra
Date			: 20-Julio-2011
Description 	:	Se invoca al formulario que permite el traspaso
					de saldos entre convenios
-------------------------------------------------------------------}
procedure TMainForm.mnuTraspasarEntreConveniosClick(Sender: TObject);
resourcestring
	MSG_TRASPASO = 'Traspaso Entre Convenios';
    
var
	f : TTraspasarDeudaForm;
begin
	if FindFormOrCreate(TTraspasarDeudaForm, f) then f.Show
    else if not f.Inicializar(MSG_TRASPASO) then f.Release;


end;

procedure TMainForm.mnu_acercadeClick(Sender: TObject);
begin
    Application.CreateForm(TAcercaDeForm, AcercaDeForm);
    AcercaDeForm.ShowModal;
    AcercaDeForm.Release;
end;

procedure TMainForm.mnu_CambiodeContrasenaClick(Sender: TObject);
Var
	f: TFormCambioPassword;
begin
	if not mnu_CambiodeContrasena.Enabled then Exit;
	Application.CreateForm(TFormCambioPassword, f);
	if f.Inicializa(DmConnections.BaseCAC, UsuarioSistema) then f.ShowModal;
	f.Release;
end;

procedure TMainForm.mnu_CargarNovedadesConveniosClick(Sender: TObject);
var
	f : TfrmNovedadesConvenios;
begin
    if FindFormOrCreate(TfrmNovedadesConvenios, f) then
        f.Show;
end;

{-----------------------------------------------------------------------------
  Procedure Name :   mnu_CarpetaDeudoresClick
  Author         :   cfuentesc
  Date Created   :   20/03/2017
  Description    :   Creaci�n ventana Carpeta Deudores
  Parameters     :   Sender: TObject
  Firma			 :   TASK_124_CFU_20170320
-----------------------------------------------------------------------------}
procedure TMainForm.mnu_CarpetaDeudoresClick(Sender: TObject);
var
    f: TFormCarpetaDeudores;
	DatosTurno: TDatosTurno;
resourcestring
    MSG_ERROR_TURNO_CERRADO     = 'Debe abrir un turno para realizar acciones de cobranza.';
    MSG_ERRORPERMISOS           = 'No posee permisos para realizar acciones de cobranza.';
    CAPTION_CARPETA_DEUDORES	= 'Carpeta Deudores';
begin
    if ExisteAcceso('mnu_CarpetaDeudores') then begin
        (* Verificar que el turno que se est� por utilizar est� abierto y sea del usuario logueado. *)
        if VerTurnoAbierto(DMConnections.BaseCAC, FPuntoEntrega, FPuntoVenta, UsuarioSistema, DatosTurno) then begin
            GNumeroTurno := DatosTurno.NumeroTurno;
        end else begin
            if (DatosTurno.NumeroTurno <> -1)
                    and ((DatosTurno.Estado = TURNO_ESTADO_ABIERTO)
                    and (Trim(DatosTurno.CodigoUsuario) <> (UpperCase(UsuarioSistema)))) then begin
                MsgBox(Format(MSG_ERROR_TURNO_ABIERTO_OTRO_USUARIO, [DatosTurno.CodigoUsuario]),
                    CAPTION_CARPETA_DEUDORES, MB_ICONSTOP);
            end else begin
                MsgBox(MSG_ERROR_TURNO_CERRADO, CAPTION_CARPETA_DEUDORES, MB_ICONSTOP);
            end;
            Exit;
        end;
    end else begin
        MsgBox(MSG_ERRORPERMISOS, CAPTION_CARPETA_DEUDORES, MB_ICONSTOP);
        Exit;
    end;

    if FindFormOrCreate(TFormCarpetaDeudores, f) then
        f.Show
    else begin
        if f.Inicializar() then 
            f.Show
        else 
            f.Release;
    end;
end;

procedure TMainForm.mnu_VentanaClick(Sender: TObject);
begin
	mnu_cascada.Enabled 	:= GetMenuItemCount(mnu_ventana.handle) > 5;
	mnu_mosaico.Enabled		:= mnu_cascada.Enabled;
	mnu_anterior.Enabled	:= GetMenuItemCount(mnu_ventana.handle) > 7;
	mnu_siguiente.Enabled 	:= mnu_anterior.Enabled;
end;

//TASK_005_AUN_20170403-CU.COBO.COB.301
procedure TMainForm.mnu_MantenimientoRefinanciacionesClick(Sender: TObject);
begin
    try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        if Assigned(RefinanciacionConsultasForm) then begin
            if RefinanciacionConsultasForm.WindowState = wsMinimized then RefinanciacionConsultasForm.WindowState := wsNormal;
            RefinanciacionConsultasForm.BringToFront;
        end
        else begin
            RefinanciacionConsultasForm := TRefinanciacionConsultasForm.Create(Application);
            if RefinanciacionConsultasForm.Inicializar then
                RefinanciacionConsultasForm.Show
            else RefinanciacionConsultasForm.Free;
        end;
    finally
        Screen.Cursor := crDefault;
    end;
end;

procedure TMainForm.ProcesosRefinanciaciones1Click(Sender: TObject);
begin
    try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        if Assigned(RefinanciacionProcesosForm) then begin
            if RefinanciacionProcesosForm.WindowState = wsMinimized then RefinanciacionProcesosForm.WindowState := wsNormal;
            RefinanciacionProcesosForm.BringToFront;
        end
        else begin
            Application.CreateForm(TRefinanciacionProcesosForm, RefinanciacionProcesosForm);
            if RefinanciacionProcesosForm.Inicializar then
                RefinanciacionProcesosForm.Show
            else RefinanciacionProcesosForm.Free;
        end;
    finally
        Screen.Cursor := crDefault;
    end;
end;

procedure TMainForm.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	if (Shift = []) and (Key = VK_ESCAPE) then close;
end;

procedure TMainForm.mnu_CobranzasClick(Sender: TObject);
begin
    actCobroComprobante.Enabled				:= ExisteAcceso('mnuCobroComprobante') and (GNumeroTurno > 0);                      //TASK_020_GLE_20161121
    //actCobroComprobante.Enabled				:= ExisteAcceso('mnu_CobrodeComprobantes') and (GNumeroTurno > 0);
    actCobroAnticipado.Enabled				:= ExisteAcceso('mnu_CobroAnticipado') and (GNumeroTurno > 0);
    //actPagoNotaCredito.Enabled				:= ExisteAcceso('mnu_PagodeNotadeCredito') and (GNumeroTurno > 0);              //SS_1004_NDR_20111220
    //actAutorizacionPagoNotasCredito.Enabled	:= ExisteAcceso('mnu_AutorizacionPagoNotasCredito') and (GNumeroTurno > 0);     //SS_1004_ALA_20111021
    actAnulacionRecibos.Enabled				:= ExisteAcceso('mnu_AnulaciondeRecibos') and (GNumeroTurno > 0);
    actAdicionarMontoTurno.Enabled			:= ExisteAcceso('mnu_AdicionarMontoenlaCaja') and (GNumeroTurno > 0);
    actAdicionarTelevias.Enabled			:= ExisteAcceso('mnu_AdicionarTelevias') and (GNumeroTurno > 0);
    actConvenioMoroso.Enabled               := ExisteAcceso('mnu_ConveniosMorosos') and (GNumeroTurno > 0);
    actReaplicarConvenio.Enabled            := ExisteAcceso('mnu_ReaplicarConvenio') and (GNumeroTurno > 0);
    actCobroComprobanteCastigo.Enabled      := ExisteAcceso('mnuCobrodeComprobanteCastigo') and (GNumeroTurno  > 0);

    RefrescarEstadosIconBar;
end;

procedure TMainForm.mnu_IconBarClick(Sender: TObject);
begin
	GNavigator.IconBarVisible	:= not GNavigator.IconBarVisible;
    mnu_IconBar.Checked			:= GNavigator.IconBarVisible;
end;

procedure TMainForm.actCobroComprobanteExecute(Sender: TObject);
resourcestring
    MSG_ERROR_TURNO_CERRADO     = 'Debe abrir un turno para registrar un Pago.';
    MSG_ERRORPERMISOS           = 'No posee permisos para registrar Pagos.';
    CAPTION_COBRO_COMPROBANTES	= 'Cobro de Comprobantes';
var
    f: TformCobroComprobantes;
	DatosTurno: TDatosTurno;
begin
    if ExisteAcceso('mnuCobroComprobante') then begin             // TASK_020_GLE_20161121
        (* Verificar que el turno que se est� por utilizar est� abierto y sea del
        usuario logueado. *)
        if VerTurnoAbierto(DMConnections.BaseCAC, FPuntoEntrega, FPuntoVenta, UsuarioSistema, DatosTurno) then begin
            GNumeroTurno := DatosTurno.NumeroTurno;
        end else begin
            if (DatosTurno.NumeroTurno <> -1)
                    and ((DatosTurno.Estado = TURNO_ESTADO_ABIERTO)
                    and (Trim(DatosTurno.CodigoUsuario) <> (UpperCase(UsuarioSistema)))) then begin
                MsgBox(Format(MSG_ERROR_TURNO_ABIERTO_OTRO_USUARIO, [DatosTurno.CodigoUsuario]),
                    CAPTION_COBRO_COMPROBANTES, MB_ICONSTOP);
            end else begin
                MsgBox(MSG_ERROR_TURNO_CERRADO, CAPTION_COBRO_COMPROBANTES, MB_ICONSTOP);
            end;
            Exit;
        end;
    end else begin
        MsgBox(MSG_ERRORPERMISOS, CAPTION_COBRO_COMPROBANTES, MB_ICONSTOP);
        Exit;
    end;

    try
        f := TformCobroComprobantes.Create(Self);
        if f.Inicializar(FNumeroPOS, FPuntoEntrega, FPuntoVenta) then begin
            f.ShowModal;
        end;
    finally
        if Assigned(f) then FreeAndNil(f);
    end;
    {
    if FindFormOrCreate(TformCobroComprobantes, f) then f.Show
    else begin
  	    if not f.Inicializar(FNumeroPOS, FPuntoEntrega, FPuntoVenta) then f.release
        else  f.show; // modal;
    end;
    }
end;

 {-----------------------------------------------------------------------------
  Function Name :   actCobroComprobanteCastigoExecute
  Author        :   GLEON
  Date Created  :   30/11/2016
  Description   :   Ejecuta submen� Cobro Comprobante Castigo
  Parameters    :   Sender
  Return Value  :   None
  firma         :   TASK_020_GLE 20161125
-----------------------------------------------------------------------------}
procedure TMainForm.actCobroComprobanteCastigoExecute(Sender: TObject);
resourcestring
    MSG_ERROR_TURNO_CERRADO     = 'Debe abrir un turno para registrar un Pago.';
    MSG_ERRORPERMISOS           = 'No posee permisos para registrar Pagos.';
    CAPTION_COBRO_COMPROBANTES	= 'Cobro de Comprobantes - Castigo';
var
    f: TformCobroComprobantesCastigo;
	DatosTurno: TDatosTurno;

begin
    //INICIO: TASK_020_GLE_20161121
    if ExisteAcceso('mnuCobrodeComprobanteCastigo') then begin
        //Verifica que el turno que se est� por utilizar est� abierto y sea del usuario logueado
        if VerTurnoAbierto(DMConnections.BaseCAC, FPuntoEntrega, FPuntoVenta, UsuarioSistema, DatosTurno) then begin
            GNumeroTurno := DatosTurno.NumeroTurno;
        end else begin
            if (DatosTurno.NumeroTurno <> -1)
                    and ((DatosTurno.Estado = TURNO_ESTADO_ABIERTO)
                    and (Trim(DatosTurno.CodigoUsuario) <> (UpperCase(UsuarioSistema)))) then begin
                MsgBox(Format(MSG_ERROR_TURNO_ABIERTO_OTRO_USUARIO, [DatosTurno.CodigoUsuario]),
                    CAPTION_COBRO_COMPROBANTES, MB_ICONSTOP);
            end else begin
                MsgBox(MSG_ERROR_TURNO_CERRADO, CAPTION_COBRO_COMPROBANTES, MB_ICONSTOP);
            end;
            Exit;
        end;
    end else begin
        MsgBox(MSG_ERRORPERMISOS, CAPTION_COBRO_COMPROBANTES, MB_ICONSTOP);
        Exit;
    end;

    try
        f := TformCobroComprobantesCastigo.Create(Self);
        if f.Inicializar(FNumeroPOS, FPuntoEntrega, FPuntoVenta) then begin
            f.Show;//f.ShowModal;
        end
        else begin
            f.Release;
        end;
    finally
        //if Assigned(f) then FreeAndNil(f);
    end;
     //FIN: TASK_020_GLE_20161121
end;

procedure TMainForm.ABMRepresentantesLegalesCN1Click(Sender: TObject);
begin
    try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        RefinanciacionABMRepLegalCNForm := TRefinanciacionABMRepLegalCNForm.Create(Self);
        if RefinanciacionABMRepLegalCNForm.Inicializar then begin
            RefinanciacionABMRepLegalCNForm.ShowModal;
        end;
    finally
        if Assigned(RefinanciacionABMRepLegalCNForm) then FreeAndNil(RefinanciacionABMRepLegalCNForm);
        Screen.Cursor := crDefault;
    end;
end;

procedure TMainForm.actAbrirTurnoExecute(Sender: TObject);
resourcestring
    CAPTION_OPEN_SHIFT = 'Abrir Turno';
var
    fTurno: TFormAperturaCierrePuntoVenta;
begin
    if ExisteTurnoAbiertoPorUsuario(DMConnections.BaseCAC, UsuarioSistema) then
    begin
        MSgBox(MSG_ERROR_OPEN_SHIFT, SELF.Caption, MB_ICONSTOP);
        Exit;
    end;

    VerificarExistenCotizacionesDelDia;
    Application.CreateForm(TFormAperturaCierrePuntoVenta, fTurno);
    try
        if (fTurno.Inicializar(CAPTION_OPEN_SHIFT, UsuarioSistema, FPuntoVenta,Abrir,
        		FPuntoEntrega, GNumeroTurno, mnu_AdicionarTelevias.Visible, mnu_AdicionarMontoenlaCaja.Visible)) then begin
            if ( fTurno.ShowModal = mrOK ) then begin
            	GNumeroTurno := fTurno.FNumeroTurno;
    			HabilitarItemsTurno(True);
            end;
        end;
	finally;
        fTurno.Release;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: HabilitarItemsTurno
  Author:    ndonadio
  Date Created: 01/02/2005
  Description:
  Parameters: Habilitar: Boolean ( .FALSE. Turno Cerrado - .TRUE. turno Abierto )
  Return Value: None
-----------------------------------------------------------------------------
  Revision 1
  Lgisuk
  14/09/06
  si el turno esta cerrado impido que se habilite el menu Nota de cobro infractores

  Revision: 2
    Author : pdominguez
    Date   : 03/02/2010
    Description : SS 510
            - Se incluye la opci�n "Configuraci�n del Puesto" de la opci�n de
            menu "Mantenimiento" dentro de la gesti�n del procedimiento, para
            que no est� disponible cuando el turno est� ya abierto.
-----------------------------------------------------------------------------}
procedure TMainForm.HabilitarItemsTurno(Habilitar: Boolean);
begin
	actAbrirTurno.Enabled := not Habilitar;
    actConfiguracionPuesto.Enabled := not habilitar; // Rev. 1 SS 510
	actCerrarTurno.Enabled := Habilitar;
    actAdicionarMontoTurno.Enabled := Habilitar and ExisteAcceso('mnu_AdicionarMontoenlaCaja');
    actAdicionarTelevias.Enabled := Habilitar and ExisteAcceso('mnu_AdicionarTelevias');
    mnu_CobroNotaCobroInfractores.Enabled := Habilitar; //Revision 1
    RefrescarEstadosIconBar;
	if Habilitar then StatusBar.Panels.Items[2].Text := MSG_CAPTION_ESTADO_TURNO_ABIERTO
	else begin
        GNumeroTurno := 0;
        StatusBar.Panels.Items[2].Text := MSG_CAPTION_ESTADO_TURNO_CERRADO;
    end;
end;

procedure TMainForm.actCerrarTurnoExecute(Sender: TObject);
resourcestring
	CAPTION_CLOSE_SHIFT = 'Cerrar Turno';
var
    fCierre: TFormAperturaCierrePuntoVenta;
    bTurnoCerrado: boolean;
begin
	bTurnoCerrado := False;
	try
        // Antes de cerrar hago que ingrese las cantidades finales
        Application.CreateForm(TFormAperturaCierrePuntoVenta, fCierre);
        if (fCierre.Inicializar(CAPTION_CLOSE_SHIFT, UsuarioSistema, FPuntoVenta, Cerrar,
        	FPuntoEntrega, GNumeroTurno, mnu_AdicionarTelevias.Visible, mnu_AdicionarMontoenlaCaja.Visible)) then
            bTurnoCerrado := (fCierre.ShowModal = mrOk);
        fCierre.Release;
    finally
    	fCierre.Release;
    end;

    if bTurnoCerrado then begin
    	StatusBar.Panels.Items[2].Text := MSG_CAPTION_ESTADO_TURNO_CERRADO;
        GNumeroTurno := 0;
        HabilitarItemsTurno(False);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: actAdicionarMontoTurnoExecute
  Author:    ndonadio
  Date Created: 01/02/2005
  Description:  Adiciona un Monto en la Caja
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.actAdicionarMontoTurnoExecute(Sender: TObject);
resourcestring
    CAPTION_ADD_CASH = 'Adicionar Monto en Efectivo';
var
    fTurno: TFormAperturaCierrePuntoVenta;
begin
    try
    	Application.CreateForm(TFormAperturaCierrePuntoVenta, fTurno);
        if (fTurno.Inicializar(CAPTION_ADD_CASH, UsuarioSistema, FPuntoVenta, AdicionarDinero,
        	FPuntoEntrega, GNumeroTurno, mnu_AdicionarTelevias.Visible, mnu_AdicionarMontoenlaCaja.Visible)) then begin
            fTurno.ShowModal;
        end;
	finally;
        fTurno.Release;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: actAdicionarTeleviasExecute
  Author:    ndonadio
  Date Created: 01/02/2005
  Description:  Adiciona Telev�as a la Caja
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.actAdicionarTeleviasExecute(Sender: TObject);
resourcestring
    CAPTION_ADD_TAGS = 'Adicionar Telev�as';
var
    fTurno: TFormAperturaCierrePuntoVenta;
begin
    try
    	Application.CreateForm(TFormAperturaCierrePuntoVenta, fTurno);
        if (fTurno.Inicializar(CAPTION_ADD_TAGS, UsuarioSistema, FPuntoVenta, AdicionarTelevia,
        	FPuntoEntrega, GNumeroTurno, mnu_AdicionarTelevias.Visible, mnu_AdicionarMontoenlaCaja.Visible)) then begin
            fTurno.ShowModal;
        end;
	finally;
        fTurno.Release;
    end;
end;

procedure TMainForm.ConfigurarSistemaTurno;
var
    DatosTurno: TDatosTurno;
begin
     if NOT actAbrirTurno.Visible then begin
        HabilitarItemsTurno(False)
     end else begin
        if VerTurnoAbierto(DMConnections.BaseCAC, FPuntoEntrega, FPuntoVenta, UsuarioSistema, DatosTurno) then begin
            GNumeroTurno := DatosTurno.NumeroTurno;
            HabilitarItemsTurno(True)
        end else begin
            if (DatosTurno.NumeroTurno <> -1)
                    and ((DatosTurno.Estado = TURNO_ESTADO_ABIERTO)
                    and (Trim(DatosTurno.CodigoUsuario) <> (uppercase(UsuarioSistema)))) then begin
                // Si es distinto es que en el mismo POS y PV hay un turno abierto por otro usuario
                DeshabilitarTodosItemsTurnos;
                MsgBox(Format(MSG_CAPTION_TURNO_ABIERTO_OTRO_USUARIO,[DatosTurno.CodigoUsuario]), self.Caption, MB_ICONWARNING);
            end else begin
                HabilitarItemsTurno(False);
            end;
        end;
     end;
end;

procedure TMainForm.ConsultarRefinanciaciones1Click(Sender: TObject);
begin
end;

{-----------------------------------------------------------------------------
  Function Name: RefrescarBarraDeEstado
  Author:    ndonadio
  Date Created: 02/02/2005
  Description: Refresca la inof en la barra de estado.
  Parameters: None
  Return Value: None

  Revision 1:
    Date: 24/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

  Revision : 2
    Author : pdominguez
    Date   : 10/06/2009
    Description : SS 809
            - Se elimina el warning por no tener POSNet asociado.
-----------------------------------------------------------------------------}
procedure TMainForm.RefrescarBarraDeEstado;
resourcestring
    MSG_POS_ATENCION = 'No existe un POSNET TransBank configurado para este punto de venta' + CRLF + CRLF +
      'Los Pagos con tarjeta de credito no registrar�n el codigo de comercio' + CRLF + CRLF +
      'Solicite asistencia al administrador del sistema inform�ndole esta advertencia';
    MSG_ATENCION = 'Atenci�n';
    MSG_POS = 'POSNET';
begin
  Caption := Caption +  ' >> ' + ObtenerNombreConcesionariaNativa();                                             //SS_1147AAA_NDR_20150414

	StatusBar.Panels.Items[0].Text  := Format(MSG_USER, [UsuarioSistema]);
    StatusBar.Panels[0].Width:= StatusBar.Font.Size * (Length(StatusBar.Panels[0].text));

	StatusBar.Panels[1].Text := Trim(Format(MSG_CONECTADO_A, [InstallIni.ReadString('Database CAC', 'Server', ''), InstallIni.ReadString('Database CAC', 'DatabaseName', '')])) + ' >> ' + ObtenerNombreConcesionariaNativa(); //SS_1147AAA_NDR_20150414
	StatusBar.Panels[1].Width:= StatusBar.Font.Size * (Length(StatusBar.Panels[1].text));

    if VerificarPuntoEntregaVenta(DMConnections.BaseCAC,fPuntoEntrega, fPuntoVenta) then begin
        StatusBar.Panels[3].Text := format(' %s (%s)',
                [ObtenerDescripcionPuntoEntrega(DMConnections.BaseCAC, fPuntoEntrega)
                ,IntToStr(FPuntoEntrega)]);
    end else begin
        StatusBar.Panels[3].Text := MSG_PUNTO_ENTREGA_VENTA;
        StatusBar.Panels[3].Width:= StatusBar.Font.Size * (Length(StatusBar.Panels[3].text));
    end;

     //Si tenemos que trabajar con POS intentamos obtener la configuracion
     if FUsaPos then FNumeroPOS := QueryGetValueInt(DMConnections.BaseCAC,
       'SELECT NUMEROPOS FROM PUNTOSVENTA  WITH (NOLOCK) WHERE CodigoPuntoEntrega = ' + IntToStr(FPuntoEntrega) +
       'AND CodigoPuntoVenta = ' + IntToStr(FPuntoVenta));

      if FNumeroPOS <= 0 then begin
//          MsgBox(MSG_POS_ATENCION, MSG_ATENCION, MB_ICONWARNING); // SS 809
          FNumeroPOS := 0;
      end;

      StatusBar.Panels[4].Text := ' ' + MSG_POS + ' ' ;
      StatusBar.Panels[5].Text := ' '+ IntToStr(FNumeroPos) + '   ';
      AjustarBarraDeEstado;
end;

procedure TMainForm.AjustarBarraDeEstado;
var
    i: integer;
    currentwidth: integer;
begin
    CurrentWidth := 0;
    For i := 0 to StatusBar.Panels.count -1 do begin
        StatusBar.Panels[i].Width := StatusBar.Font.Size * (Length(StatusBar.Panels[i].text));
        CurrentWidth := CurrentWidth + StatusBar.Panels[i].Width ;
    end;

    if StatusBar.Width < CurrentWidth then begin
        While StatusBar.Width < CurrentWidth do begin
             StatusBar.Panels[1].Width := StatusBar.Panels[1].Width - 1 ;
             StatusBar.Panels[3].Width := StatusBar.Panels[3].Width - 1 ;
             CurrentWidth := 0;
             For i := 0 to StatusBar.Panels.count -1 do begin
                 CurrentWidth := CurrentWidth + StatusBar.Panels[i].Width ;
             end;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: mnuAutorizacionPagoNotasCreditoClick
  Author:    flamas
  Date Created: 01/06/2005
  Description: Autoriza el PAgo de una Nota de Cr�dito
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.DeshabilitarTodosItemsTurnos;
begin
    GNumeroTurno := 0;
    actAbrirTurno.Enabled          := False;
    actCerrarTurno.Enabled         := False;
    actAdicionarMontoTurno.Enabled := False;
    StatusBar.Panels.Items[2].Text := EmptyStr;
    RefrescarEstadosIconBar;
end;

{-----------------------------------------------------------------------------
  Function Name: mnu_sistemaClick
  Author:
  Date Created:  /  /
  Description:
  Parameters: Sender: TObject
  Return Value: N/A

  Revision : 1
    Date: 29/01/2010
    Author: mpiazza
    Description:  Ref SS-510 se le agrega una validacion por estacion de trabajo

-----------------------------------------------------------------------------}
procedure TMainForm.mnu_sistemaClick(Sender: TObject);
var
    DatosTurno: TDatosTurno;
begin
    if VerTurnoAbierto(DMConnections.BaseCAC, FPuntoEntrega, FPuntoVenta, UsuarioSistema, DatosTurno) then begin
        GNumeroTurno := DatosTurno.NumeroTurno;
        HabilitarItemsTurno(True)
    end else begin
        if (DatosTurno.NumeroTurno <> -1)
                and ((DatosTurno.Estado = TURNO_ESTADO_ABIERTO)
                and (Trim(DatosTurno.CodigoUsuario) <> (UpperCase(UsuarioSistema))))
                and ( ResolveHostNameStr(GetMachineName) <> DatosTurno.IdEstacion )then begin//Si es distinto es que en el mismo POS y PV hay un turno abierto por otro usuario
            DeshabilitarTodosItemsTurnos;
        end else begin
            HabilitarItemsTurno(False);
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: actReimpresionNIExecute
  Author:    jconcheyro
  Date Created: 04/12/2006
  Description: Solamente reimprime las NI
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.actReimpresionNIExecute(Sender: TObject);
var
    f:TReimpresionNotaCobroInfractoresForm;
begin
    if FindFormOrCreate(TReimpresionNotaCobroInfractoresForm, f)then begin
        f.Show;
    end else begin
        if f.Inicializar(StringReplace(ReimpresionNI1.Caption, '&', EmptyStr, [rfReplaceAll])) then f.Show
        else f.Release;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: actReimpresionReciboExecute
  Author:    flamas
  Date Created: 02/06/2005
  Description:
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.actReimpresionReciboExecute(Sender: TObject);
var
    f: TfrmReimprimeRecibo;
begin
    if FindFormOrCreate(TfrmReimprimeRecibo, f) then begin
        f.Show;
    end else begin
        if not f.Inicializa(True) then f.Release
        else  f.Show;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: actAnulacionRecibosExecute
  Author:    flamas
  Date Created: 02/06/2005
  Description:
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.actAnulacionRecibosExecute(Sender: TObject);
resourcestring
    MSG_ERROR_TURNO_CERRADO		= 'Debe abrir un turno para anular recibos.';
    MSG_ERRORPERMISOS           = 'No posee permisos para anular recibos.';
    CAPTION_ANULACION_RECIBOS 	= 'Anulaci�n de Recibos';
var
    f: Tfrm_AnulacionRecibos;
begin
    if NOT ExisteAcceso('mnu_AnulaciondeRecibos') then Exit;

    Application.CreateForm(Tfrm_AnulacionRecibos, f);
	if not f.Inicializar then f.release else f.show;
end;

{-----------------------------------------------------------------------------
  Function Name: actAutorizacionPagoNotasCreditoExecute
  Author:    flamas
  Date Created: 02/06/2005
  Description:
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.actAutorizacionPagoNotasCreditoExecute(Sender: TObject);
resourcestring
    MSG_ERROR_TURNO_CERRADO		= 'Debe abrir un turno para autorizar un pago.';
    MSG_ERRORPERMISOS           = 'No posee permisos para autorizar pagos.';
    CAPTION_PAGO_NOTAS_CREDITO 	= 'Autorizaci�n de Pago de Notas de Cr�dito';
var
    f: TfrmAutorizacioPagoNotasCredito;
	DatosTurno: TDatosTurno;
begin
    //Si por x motivo el menu se habilita, deshabilitamos el menu y salimos de la funcionalidad.
    actAutorizacionPagoNotasCredito.Enabled := false;                           //SS_1004_ALA_20111021
    Exit;                                                                       //SS_1004_ALA_20111021
    if ExisteAcceso('mnu_AutorizacionPagoNotasCredito') then begin
        (* Verificar que el turno que se est� por utilizar est� abierto y sea del
        usuario logueado. *)
        if VerTurnoAbierto( DMConnections.BaseCAC, FPuntoEntrega, FPuntoVenta, UsuarioSistema, DatosTurno) then begin
            GNumeroTurno := DatosTurno.NumeroTurno;
        end else begin
            if (DatosTurno.NumeroTurno <> -1)
                    and ((DatosTurno.Estado = TURNO_ESTADO_ABIERTO)
                    and (Trim(DatosTurno.CodigoUsuario) <> (UpperCase(UsuarioSistema)))) then begin
                MsgBox(Format(MSG_ERROR_TURNO_ABIERTO_OTRO_USUARIO, [DatosTurno.CodigoUsuario]),
                    CAPTION_PAGO_NOTAS_CREDITO, MB_ICONSTOP);
            end else begin
                MsgBox(MSG_ERROR_TURNO_CERRADO, CAPTION_PAGO_NOTAS_CREDITO, MB_ICONSTOP);
            end;
            Exit;
        end;
    end else begin
        MsgBox(MSG_ERRORPERMISOS, CAPTION_PAGO_NOTAS_CREDITO, MB_ICONSTOP);
        Exit;
    end;

    Application.CreateForm(TfrmAutorizacioPagoNotasCredito, f);
	if not f.Inicializar(FPuntoEntrega, FPuntoVenta) then f.release else f.show;
end;

{-----------------------------------------------------------------------------
  Function Name: actAnaliticoCierreTurnosExecute
  Author:    flamas
  Date Created: 02/06/2005
  Description:
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.actAnaliticoCierreTurnosExecute(Sender: TObject);
var
    f: TFormAnaliticoCierreDeTurno;
begin
   	Application.CreateForm(TFormAnaliticoCierreDeTurno, f);
    try
        if f.Inicializar(f.Caption, ExisteAcceso('ver_informes_de_terceros'), trAnaliticoCierreTurno) then begin
            f.ShowModal;
        end;
	finally;
        f.Release;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: actCerrarTurnosTercerosExecute
  Author:    flamas
  Date Created: 02/06/2005
  Description:
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.actCerrarTurnosTercerosExecute(Sender: TObject);
var
    f: TFormListadoTurnos;
begin
	if FindFormOrCreate(TFormListadoTurnos, f) then begin
        f.Show;
	end else begin
        if f.Inicializar(True, FPuntoEntrega, FPuntoVenta) then
            f.Show
        else f.Release;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: actPagoNotaCreditoExecute
  Author:    flamas
  Date Created: 02/06/2005
  Description:
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.actPagoNotaCreditoExecute(Sender: TObject);
resourcestring
    MSG_ERROR_TURNO_CERRADO     = 'Debe abrir un turno para efectuar un Pago.';
    MSG_ERRORPERMISOS           = 'No posee permisos para registrar Pagos.';
    CAPTION_PAGO_NOTAS_CREDITO 	= 'Pago de Notas de Cr�dito';
var
    f: Tfrm_PagoNotasCredito;
	DatosTurno: TDatosTurno;
begin
    //Si por x motivo el menu se habilita, deshabilitamos el menu y salimos de la funcionalidad.
    actPagoNotaCredito.Enabled := false;                           //SS_1004_NDR_20111220
    Exit;                                                          //SS_1004_NDR_20111220
    if ExisteAcceso('mnu_PagodeNotadeCredito') then begin
        (* Verificar que el turno que se est� por utilizar est� abierto y sea del
        usuario logueado. *)
        if VerTurnoAbierto(DMConnections.BaseCAC, FPuntoEntrega, FPuntoVenta, UsuarioSistema, DatosTurno) then begin
            GNumeroTurno := DatosTurno.NumeroTurno;
        end else begin
            if (DatosTurno.NumeroTurno <> -1)
                    and ((DatosTurno.Estado = TURNO_ESTADO_ABIERTO)
                    and (Trim(DatosTurno.CodigoUsuario) <> (UpperCase(UsuarioSistema)))) then begin
                MsgBox(Format(MSG_ERROR_TURNO_ABIERTO_OTRO_USUARIO, [DatosTurno.CodigoUsuario]),
                    CAPTION_PAGO_NOTAS_CREDITO, MB_ICONSTOP);
            end else begin
                MsgBox(MSG_ERROR_TURNO_CERRADO, CAPTION_PAGO_NOTAS_CREDITO, MB_ICONSTOP);
            end;
            Exit;
        end;
    end else begin
        MsgBox(MSG_ERRORPERMISOS, CAPTION_PAGO_NOTAS_CREDITO, MB_ICONSTOP);
        Exit;
    end;

    Application.CreateForm(Tfrm_PagoNotasCredito, f);
	if not f.Inicializar(FPuntoEntrega, FPuntoVenta) then f.release else f.showmodal;
end;

{-----------------------------------------------------------------------------
  Function Name: RefrescarEstadosIconBar
  Author:    flamas
  Date Created: 03/06/2005
  Description: Refresca los Estados de la IconBar
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.RefrescarEstadosIconBar;
begin
    if ( GNavigator = nil ) then Exit;
    
	GNavigator.SetIconEnabled(NAME_FILES, CAPTION_OPEN_SHIFT, actAbrirTurno.Enabled);
	GNavigator.SetIconEnabled(NAME_FILES, CAPTION_CLOSE_SHIFT, actCerrarTurno.Enabled);
	GNavigator.SetIconEnabled(NAME_FILES, CAPTION_ADD_MONEY, actAdicionarMontoTurno.Enabled);
	GNavigator.SetIconEnabled(NAME_FILES, CAPTION_OPEN_SHIFT, actAbrirTurno.Enabled);

	GNavigator.SetIconEnabled(NAME_SALES, CAPTION_INVOICE_PAYMENT, actCobroComprobante.Enabled);
    GNavigator.SetIconEnabled(NAME_SALES, CAPTION_PUNISHMENT_PAYMENT, actCobroComprobanteCastigo.Enabled);   //TASK_020_GLE 20161125
    GNavigator.SetIconEnabled(NAME_SALES, 'Ajuste de Importe Efectivo en Caja', actCobroComprobanteCastigo.Enabled);   //TASK_020_GLE 20161125
	//GNavigator.SetIconEnabled(NAME_SALES, CAPTION_CREDIT_PAYMENT, actPagoNotaCredito.Enabled);                                    //SS_1004_NDR_20111220
	//GNavigator.SetIconEnabled(NAME_SALES, CAPTION_PAYMENT_AUTHORIZATION, actAutorizacionPagoNotasCredito.Enabled);                //SS_1004_ALA_20111021
	GNavigator.SetIconEnabled(NAME_SALES, CAPTION_PAYMENT_CANCEL, actAnulacionRecibos.Enabled);
end;


procedure TMainForm.actAnulacionChequesProtestadosExecute(Sender: TObject);
resourcestring
    MSG_ERROR_TURNO_CERRADO		= 'Debe abrir un turno para anular recibos.';
    MSG_ERRORPERMISOS           = 'No posee permisos para anular recibos.';
    CAPTION_ANULACION_RECIBOS 	= 'Anulaci�n de Recibos';
var
    f: TfrmProtestoCheque;
begin
    if not ExisteAcceso('mnuRegistrarChequeProtestado') then Exit;

    if FindFormOrCreate(TfrmProtestoCheque,f) then begin
            f.Show;
    end
    else if not f.Inicializar then f.release else f.show;

end;

{-----------------------------------------------------------------------------
  Procedure: TMainForm.InicializarIconBar
  Author:    ggomez
  Date:      28-Jun-2005
  Arguments: None
  Result:    None
  Description: Inicializa la barra de �conos.
-----------------------------------------------------------------------------}
procedure TMainForm.InicializarIconBar;
var
    Bmp: TBitmap;
    NavigatorHasPages : Boolean;
begin
    //------------------------------------------------------------------------
	// Navegador
    //------------------------------------------------------------------------
	Bmp                         := TBitmap.Create;
	GNavigator                  := TNavigator.Create(Self);

    GNavigator.ShowBrowserBar       := GetUserSettingShowBrowserBar;
    mnu_BarraDeNavegacion.Checked	:= GNavigator.ShowBrowserBar;

    GNavigator.ShowHotLinkBar   := GetUserSettingShowHotLinksBar;
    mnu_BarraDeHotLinks.Checked := GNavigator.ShowHotLinkBar;

    GNavigator.IconBarVisible   := False;
    mnu_IconBar.Checked         := GNavigator.IconBarVisible;
    NavigatorHasPages           := False;

	//----------------------------------------------------------
	// Agregamos las p�ginas e �conos HABILITADAS al toolbar
	//----------------------------------------------------------
	if 	ExisteAcceso('mnu_AbrirTurno') or  ExisteAcceso('mnu_CerrarTurno') or
        ExisteAcceso('mnu_AdicionarMontoenlaCaja') or ExisteAcceso('mnu_AnaliticoDeCierreDeTurno')
        or ExisteAcceso ('mnuAjustedeCaja')                     //TASK_031_GLE_20170111
        then begin
		GNavigator.AddPage(NAME_FILES, CAPTION_FILES, '');
    	NavigatorHasPages := True;
    end;

	if 	ExisteAcceso('mnuCobroComprobante') or ExisteAcceso('mnu_CobrodeComprobantesCastigo') or            //TASK_020_GLE_20161121
    	ExisteAcceso('mnu_ReimpresindeRecibo') or ExisteAcceso('mnu_AnulaciondeRecibos')                    //TASK_020_GLE_20161121

        then begin
  		GNavigator.AddPage(NAME_SALES, CAPTION_SALES, '');
    	NavigatorHasPages := True;
    end;

   	//----------------------------------------------------------
	// Agregamos items en la p�gina de Procesos
	if ExisteAcceso('mnu_AbrirTurno') then begin
        PageImagenes.GetBitmap(1, Bmp);
        GNavigator.AddIcon(NAME_FILES, CAPTION_OPEN_SHIFT, HINT_OPEN_SHIFT, Bmp, actAbrirTurnoExecute);
        Bmp.Assign(nil);
    end;

	if ExisteAcceso('mnu_CerrarTurno') then begin
        PageImagenes.GetBitmap(5, Bmp);
        GNavigator.AddIcon(NAME_FILES, CAPTION_CLOSE_SHIFT, HINT_CLOSE_SHIFT, Bmp, actCerrarTurnoExecute);
        Bmp.Assign(nil);
    end;

	if ExisteAcceso('mnu_AdicionarMontoenlaCaja') then begin
        PageImagenes.GetBitmap(3, Bmp);
        GNavigator.AddIcon(NAME_FILES, CAPTION_ADD_MONEY, HINT_ADD_MONEY, Bmp, actAdicionarMontoTurnoExecute);
        Bmp.Assign(nil);
    end;

	if ExisteAcceso('mnu_AnaliticoDeCierreDeTurno') then begin
        PageImagenes.GetBitmap(8, Bmp);
        GNavigator.AddIcon(NAME_FILES, CAPTION_SHIFT_REPORT, HINT_SHIFT_REPORT, Bmp, actAnaliticoCierreTurnosExecute);
        Bmp.Assign(nil);
    end;

   	//----------------------------------------------------------
	// Agregamos items en la p�gina de Ventas
    if ExisteAcceso('mnuCobroComprobante') then begin
        PageImagenes.GetBitmap(6, Bmp);
        GNavigator.AddIcon(NAME_SALES, CAPTION_INVOICE_PAYMENT, HINT_INVOICE_PAYMENT, Bmp, actCobroComprobanteExecute);
        Bmp.Assign(nil);
    end;

    if ExisteAcceso('mnu_CobrodeComprobantesCastigo') then begin                                                                  //TASK_020_GLE_20161121
        PageImagenes.GetBitmap(6, Bmp);                                                                                           //TASK_020_GLE_20161121
        GNavigator.AddIcon(NAME_SALES, CAPTION_PUNISHMENT_PAYMENT, HINT_INVOICE_PAYMENT, Bmp, actCobroComprobanteCastigoExecute); //TASK_020_GLE_20161121
        Bmp.Assign(nil);                                                                                                          //TASK_020_GLE_20161121
    end;                                                                                                                          //TASK_020_GLE_20161121

    if ExisteAcceso('mnu_PagodeNotadeCredito') then begin
        PageImagenes.GetBitmap(7, Bmp);
        GNavigator.AddIcon(NAME_SALES, CAPTION_CREDIT_PAYMENT, HINT_CREDIT_PAYMENT, Bmp, actPagoNotaCreditoExecute);
        Bmp.Assign(nil);
    end;

    if ExisteAcceso('mnu_ReimpresindeRecibo') then begin
        PageImagenes.GetBitmap(0, Bmp);
        GNavigator.AddIcon(NAME_SALES, CAPTION_RECEIPT_REPRINT, HINT_RECEIPT_REPRINT, Bmp, actReimpresionReciboExecute);
        Bmp.Assign(nil);
    end;

    if ExisteAcceso('mnu_AnulaciondeRecibos') then begin
        PageImagenes.GetBitmap(4, Bmp);
        GNavigator.AddIcon(NAME_SALES, CAPTION_PAYMENT_CANCEL, HINT_PAYMENT_CANCEL, Bmp, actAnulacionRecibosExecute);
        Bmp.Assign(nil);
    end;

    if ExisteAcceso('mnuRegistrarChequeProtestado') then begin
        PageImagenes.GetBitmap(10, Bmp);
        GNavigator.AddIcon(NAME_SALES, CAPTION_CHECK_CANCEL, HINT_CHECK_CANCEL, Bmp, actAnulacionChequesProtestadosExecute);
        Bmp.Assign(nil);
    end;

    {	if ExisteAcceso('mnu_AutorizacionPagoNotasCredito') then begin
        PageImagenes.GetBitmap(3, Bmp);
        GNavigator.AddIcon(NAME_SALES, CAPTION_PAYMENT_AUTHORIZATION, HINT_PAYMENT_AUTHORIZATION, Bmp, actAnulacionRecibosExecute);
        Bmp.Assign(nil);
    end;}


	if NavigatorHasPages then GNavigator.IconBarActivePage := 0;

	Bmp.Free;
	Update;

    RefrescarEstadosIconBar;

    GNavigator.IconBarVisible   := GetUserSettingShowIconBar;
    mnu_IconBar.Checked		    := GNavigator.IconBarVisible;
    GNavigator.ConnectionCOP    := Nil;
    GNavigator.ConnectionCAC    := DMConnections.BaseCAC;
end;

procedure TMainForm.mnu_BarraDeNavegacionClick(Sender: TObject);
begin
	GNavigator.ShowBrowserBar       := not GNavigator.ShowBrowserBar;
    mnu_BarraDeNavegacion.Checked	:= GNavigator.ShowBrowserBar;
end;

procedure TMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
resourcestring                                                                                    //SS_1436_NDR_20151230
	MSG_REGISTRO_CAPTION	= 'Error al Grabar Registro de Operaciones';                          //SS_1436_NDR_20151230
var                                                                                               //SS_1436_NDR_20151230
  DescriError: array[0..255] of char;                                                             //SS_1436_NDR_20151230
begin
    (* Guardar las opciones de configuraci�n del usuario. *)
    GuardarUserSettings;
    if ( CrearRegistroOperaciones(                                                                //SS_1436_NDR_20151230
            DMCOnnections.BaseCAC,                                                                //SS_1436_NDR_20151230
            SYS_COBRANZAS,                                                                        //SS_1436_NDR_20151230
            RO_MOD_LOGOUT,                                                                        //SS_1436_NDR_20151230
            RO_AC_LOGOUT,                                                                         //SS_1436_NDR_20151230
            0,                                                                                    //SS_1436_NDR_20151230
            GetMachineName,                                                                       //SS_1436_NDR_20151230
            UsuarioSistema,                                                                       //SS_1436_NDR_20151230
            'LOGOUT DEL SISTEMA',                                                                 //SS_1436_NDR_20151230
            0,0,0,0,                                                                              //SS_1436_NDR_20151230
            DescriError) = -1 ) then                                                              //SS_1436_NDR_20151230
    begin                                                                                         //SS_1436_NDR_20151230
      MsgBoxErr(MSG_ERROR_ACCESO, StrPas(DescriError), MSG_REGISTRO_CAPTION, MB_ICONSTOP);        //SS_1436_NDR_20151230
    end;                                                                                          //SS_1436_NDR_20151230
end;

procedure TMainForm.mnu_BarraDeHotLinksClick(Sender: TObject);
begin
	GNavigator.ShowHotLinkBar   := not GNavigator.ShowHotLinkBar;
    mnu_BarraDeHotLinks.Checked := GNavigator.ShowHotLinkBar;
end;

{-----------------------------------------------------------------------------
  Procedure: TMainForm.GuardarUserSettings
  Author:    ggomez
  Date:      30-Jun-2005
  Arguments: None
  Result:    None
  Description: Guarda las opciones de configuraci�n para el usuario logueado
    en el Sistema Operativo.
-----------------------------------------------------------------------------}
procedure TMainForm.GuardarUserSettings;
begin
    SetUserSettingShowIconBar(mnu_IconBar.Checked);
    SetUserSettingShowBrowserBar(mnu_BarraDeNavegacion.Checked);
    SetUserSettingShowHotLinksBar(mnu_BarraDeHotLinks.Checked);
end;

procedure TMainForm.mnu_ReemisionNotaCobroClick(Sender: TObject);
resourcestring
    MSG_ERROR_TURNO_CERRADO     = 'Debe abrir un turno para Reemitir una Nota de Cobro.';
    MSG_ERRORPERMISOS           = 'No posee permisos para Reemitir Notas de Cobro.';
    CAPTION_COBRO_COMPROBANTES	= 'Reemisi�n de Nota de Cobro';
var
    f: TformReemisionNotaCobro;
  	DatosTurno: TDatosTurno;
begin
    if ExisteAcceso('mnu_ReemisionNotaCobro') then begin
        (* Verificar que el turno que se est� por utilizar est� abierto y sea del
        usuario logueado. *)
        if VerTurnoAbierto(DMConnections.BaseCAC, FPuntoEntrega, FPuntoVenta, UsuarioSistema, DatosTurno) then begin
            GNumeroTurno := DatosTurno.NumeroTurno;
        end else begin
            if (DatosTurno.NumeroTurno <> -1)
                    and ((DatosTurno.Estado = TURNO_ESTADO_ABIERTO)
                    and (Trim(DatosTurno.CodigoUsuario) <> (UpperCase(UsuarioSistema)))) then begin
                MsgBox(Format(MSG_ERROR_TURNO_ABIERTO_OTRO_USUARIO, [DatosTurno.CodigoUsuario]),
                       CAPTION_COBRO_COMPROBANTES, MB_ICONSTOP);
            end else begin
                MsgBox(MSG_ERROR_TURNO_CERRADO, CAPTION_COBRO_COMPROBANTES, MB_ICONSTOP);
            end;
            Exit;
        end;
    end else begin
        MsgBox(MSG_ERRORPERMISOS, CAPTION_COBRO_COMPROBANTES, MB_ICONSTOP);
        Exit;
    end;

    if FindFormOrCreate(TformReemisionNotaCobro, f) then f.Show
    else begin
  	    if not f.Inicializar(FNumeroPOS, FPuntoEntrega, FPuntoVenta) then f.release
        else  f.ShowModal; // modal;
    end;
end;

procedure TMainForm.mnu_ChequesAlCierredeTurnoClick(Sender: TObject);
var
    f: TFormAnaliticoCierreDeTurno;
begin
   	Application.CreateForm(TFormAnaliticoCierreDeTurno, f);
    try
        if f.Inicializar(f.Caption, ExisteAcceso('ver_informes_de_terceros'), trChequesCierreTurno) then begin
            f.ShowModal;
        end;
	finally;
        f.Release;
    end;
end;

procedure TMainForm.actCobroAnticipadoExecute(Sender: TObject);
resourcestring
    MSG_ERROR_TURNO_CERRADO     = 'Debe abrir un turno para registrar un Pago.';
    MSG_ERRORPERMISOS           = 'No posee permisos para registrar Pagos.';
    CAPTION_COBRO_COMPROBANTES	= 'Cobro de Comprobantes';
var
    f: TformCobroAnticipado;
	DatosTurno: TDatosTurno;
begin
    if ExisteAcceso('mnuCobroComprobante') then begin //TASK_020_GLE_20161121
        (* Verificar que el turno que se est� por utilizar est� abierto y sea del
        usuario logueado. *)
        if VerTurnoAbierto(DMConnections.BaseCAC, FPuntoEntrega, FPuntoVenta, UsuarioSistema, DatosTurno) then begin
            GNumeroTurno := DatosTurno.NumeroTurno;
        end else begin
            if (DatosTurno.NumeroTurno <> -1)
                    and ((DatosTurno.Estado = TURNO_ESTADO_ABIERTO)
                    and (Trim(DatosTurno.CodigoUsuario) <> (UpperCase(UsuarioSistema)))) then begin
                MsgBox(Format(MSG_ERROR_TURNO_ABIERTO_OTRO_USUARIO, [DatosTurno.CodigoUsuario]),
                    CAPTION_COBRO_COMPROBANTES, MB_ICONSTOP);
            end else begin
                MsgBox(MSG_ERROR_TURNO_CERRADO, CAPTION_COBRO_COMPROBANTES, MB_ICONSTOP);
            end;
            Exit;
        end;
    end else begin
        MsgBox(MSG_ERRORPERMISOS, CAPTION_COBRO_COMPROBANTES, MB_ICONSTOP);
        Exit;
    end;

    if FindFormOrCreate(TformCobroAnticipado, f) then f.Show
    else begin
  	    if not f.Inicializar(FNumeroPOS, FPuntoEntrega, FPuntoVenta) then f.release
        else  f.show; // modal;
    end;
end;

{******************************** Function Header ******************************
Function Name: HabilitarMenuInformes
Author : jconcheyro
Date Created : 31/10/2005
Description : Cambia el estado enable del menu informes dependiendo de la existencia de items en �l. Kanav 1428
Parameters : None
Return Value : None
*******************************************************************************}
procedure TMainForm.HabilitarMenuInformes;
begin
    mnu_informes.Enabled := mnu_informes.Count > 0;
end;

procedure TMainForm.mnu_CobroNotaCobroInfractoresClick(Sender: TObject);
resourcestring
    MSG_ERROR_TURNO_CERRADO     = 'Debe abrir un turno para registrar un Pago.';
    MSG_ERRORPERMISOS           = 'No posee permisos para registrar Pagos.';
    CAPTION_COBRO_COMPROBANTES	= 'Cobro de Comprobantes';
var
    f: TformCobroComprobantesBI;
	DatosTurno: TDatosTurno;
begin
    if ExisteAcceso('mnuCobroComprobante') then begin //TASK_020_GLE_20161121
        (* Verificar que el turno que se est� por utilizar est� abierto y sea del
        usuario logueado. *)
        if VerTurnoAbierto(DMConnections.BaseCAC, FPuntoEntrega, FPuntoVenta, UsuarioSistema, DatosTurno) then begin
            GNumeroTurno := DatosTurno.NumeroTurno;
        end else begin
            if (DatosTurno.NumeroTurno <> -1)
                    and ((DatosTurno.Estado = TURNO_ESTADO_ABIERTO)
                    and (Trim(DatosTurno.CodigoUsuario) <> (UpperCase(UsuarioSistema)))) then begin
                MsgBox(Format(MSG_ERROR_TURNO_ABIERTO_OTRO_USUARIO, [DatosTurno.CodigoUsuario]),
                    CAPTION_COBRO_COMPROBANTES, MB_ICONSTOP);
            end else begin
                MsgBox(MSG_ERROR_TURNO_CERRADO, CAPTION_COBRO_COMPROBANTES, MB_ICONSTOP);
            end;
            Exit;
        end;
    end else begin
        MsgBox(MSG_ERRORPERMISOS, CAPTION_COBRO_COMPROBANTES, MB_ICONSTOP);
        Exit;
    end;

    if FindFormOrCreate(TformCobroComprobantesBI, f) then f.Show
    else begin
  	    if not f.Inicializar(FNumeroPOS, FPuntoEntrega, FPuntoVenta) then f.release
        else  f.show; // modal;
    end;
end;

procedure TMainForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
ResourceString
    MSG_TURNO_ABIERTO = 'Desea cerrar el Turno antes de salir del sistema?';
    MSG_AVISO         = 'Aviso';
var
    Respuesta: integer;

begin
    // si tiene un turno abierto no permito que salga
    // Revision 2 : damos 3 botones: cerrar el turno, salir sin cerrar o cancelar el cerrado y seguir trabajando
    if GNumeroTurno <> 0 then begin
        Respuesta := MsgBox(MSG_TURNO_ABIERTO, MSG_AVISO, MB_ICONINFORMATION or MB_YESNOCANCEL);
        if Respuesta = IDYES then begin
            actCerrarTurnoExecute(self);
        CanClose := False;
        end;
        if Respuesta = IDNO then CanClose := True;
        if Respuesta = IDCANCEL then CanClose := False;
    end;
end;

{******************************* Procedure Header ******************************
Procedure Name: actConfiguracionPuestoExecute
Author : pdominguez
Date Created : 03/02/2010
Parameters : Sender: TObject
Description : SS 510
        - Se mueve el evento OnClick del item "Configuraci�n del Puesto" a una
        acci�n dentro del ActionManager1.
*******************************************************************************}
procedure TMainForm.actConfiguracionPuestoExecute(Sender: TObject);
var
	f : TFormGeneradorIni;
begin
	Application.CreateForm(TFormGeneradorIni, f);
	if f.Inicializa then begin
        if f.ShowModal = mrOK then begin
            //{ INICIO : 20160315 MGO
            FPuntoEntrega   := ApplicationIni.ReadInteger('General', 'PuntoEntrega', -1);
            FPuntoVenta     := ApplicationIni.ReadInteger('General', 'PuntoVenta', -1);
            {
            FPuntoEntrega   := InstallIni.ReadInteger('General', 'PuntoEntrega', -1);
            FPuntoVenta     := InstallIni.ReadInteger('General', 'PuntoVenta', -1);
            }// FIN : 20160315 MGO

            RefrescarBarraDeEstado;

            ConfigurarSistemaTurno;
        end;
    end;
    f.Release;
end;

procedure TMainForm.actConvenioMorosoExecute(Sender: TObject);

var
	f: TConveniosMorosos;
begin
    if FindFormOrCreate(TConveniosMorosos, f) then
        f.Show
    else begin
  	    if not f.Inicializar(StringReplace(mnu_ConveniosMorosos.Caption, '&','',[rfReplaceAll]))
            then f.release
        else
            f.show; // modal;
    end;
end;

procedure TMainForm.actReaplicarConvenioExecute(Sender: TObject);
var
	f: TformReaplicarConvenio;
begin
    if FindFormOrCreate(TformReaplicarConvenio, f) then
        f.Show
    else begin
  	    if not f.Inicializar then f.release
        else f.show; // modal;
    end;
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
    {$IFNDEF PRODUCCION}
        Caption := Caption + Format(' - Versi�n %s: %s', [cAmbiente, GetFileVersionString(GetExeName, True)]);
    {$ELSE}
        Caption := Caption + Format(' - Versi�n %s: %s', [cAmbiente, GetFileVersionString(GetExeName, True)]);
//        Caption := Caption + Format(' - Versi�n: %s', [GetFileVersionString(GetExeName, True)]);
    {$ENDIF}
end;

{----------------------------------------------------------
        	mnuDevolucionDineroClick

Author		: mbecerra
Date		: 10-Junio-2011
Description	:		(Ref SS 959)
            	Invoca al nuevo formulario frmDevolucion

------------------------------------------------------------}
procedure TMainForm.mnuDevolucionDineroClick(Sender: TObject);
resourcestring
	MSG_SIN_PERMISOS		= 'Usuario no tiene permisos para esta acci�n';
var
	DatosTurno: TDatosTurno;
    
begin

    if ExisteAcceso('mnuDevolucionDinero') then begin
        //Verificar que el turno que se est� por utilizar est� abierto y sea del usuario logueado
        if VerTurnoAbierto(DMConnections.BaseCAC, FPuntoEntrega, FPuntoVenta, UsuarioSistema, DatosTurno) then begin
            GNumeroTurno := DatosTurno.NumeroTurno;
        end
        else begin
            if	(DatosTurno.NumeroTurno <> -1) and
            	(
                	(DatosTurno.Estado = TURNO_ESTADO_ABIERTO) and
                    (Trim(DatosTurno.CodigoUsuario) <> UpperCase(UsuarioSistema))
            	) then
            begin
                MsgBox(Format(MSG_ERROR_TURNO_ABIERTO_OTRO_USUARIO, [DatosTurno.CodigoUsuario]), Caption, MB_ICONSTOP);
            end
            else MsgBox(MSG_ERROR_TURNO_CERRADO, Caption, MB_ICONSTOP);

            Exit;
        end;
    end
    else begin
        MsgBox(MSG_SIN_PERMISOS, Caption, MB_ICONSTOP);
        Exit;
    end;
 
	Application.CreateForm(TComprobanteDevolucionDineroForm, ComprobanteDevolucionDineroForm);
    if ComprobanteDevolucionDineroForm.Inicializar then ComprobanteDevolucionDineroForm.ShowModal;
    ComprobanteDevolucionDineroForm.Release;

end;

//BEGIN : SS_1147_NDR_20141216 -------------------------------------------------
function TMainForm.CambiarEventoMenu(Menu: TMenu): Boolean;

	Procedure CambiarEventoItem(Nivel: Integer; Item: TmenuItem);
	Var
		i: Integer;
	begin
    if (Nivel <> 0) then
       Item.OnDrawItem := DrawMenuItem;

		for i := 0 to Item.Count - 1 do begin
			CambiarEventoItem(Nivel + 1, Item.Items[i]);
		end;
	end;

begin
	CambiarEventoItem(0, Menu.Items);
	Result := True;
end;
//END : SS_1147_NDR_20141216 -------------------------------------------------


//BEGIN : SS_1147_NDR_20141216 -------------------------------------------------
procedure TMainForm.DrawMenuItem(Sender: TObject; ACanvas: TCanvas;
  ARect: TRect; Selected: Boolean);
var
  S: String;
begin
    with ACanvas do
    begin
        S := TMenuItem(Sender).Caption;
        if Selected then
        begin
            Brush.Color := FColorMenuSel;
            Font.Color := FColorFontSel;
        end
        else
        begin
            if TMenuItem(Sender).Enabled then
            begin
                Brush.Color := FColorMenu;
                Font.Color := FColorFont;
                Font.Style:=[fsBold];
            end
            else
            begin
                Brush.Color := FColorMenu;
                Font.Color := clGrayText;
                Font.Style:=[];
            end;
        end;


        if (Parent = nil) and (TMenuItem(Sender).MenuIndex = 8 ) and not Selected then
            ARect.Right := Width;

        FillRect(ARect);
        DrawText(ACanvas.Handle, PChar(S), Length(S), ARect, DT_SINGLELINE or DT_VCENTER);

        if S='-' then
        begin
            with ACanvas do
            begin
                ACanvas.MoveTo(ARect.Left,ARect.top + 4);
                ACanvas.Pen.Color := FColorFont;
                ACanvas.LineTo(ARect.Right, ARect.top + 4 );
            end;
        end;

    end;
end;

//END : SS_1147_NDR_20141216 -------------------------------------------------


//INICIO: TASK_078_JMA_20161128
//Este form es utilizado para Etiquetar los Convenios        TASK_078_JMA_20170110
procedure TMainForm.mnu_EtiquetarConveniosClick(Sender: TObject);
var
    frmCarga: TfrmNovedadesConvenios;
begin
    if FindFormOrCreate(TfrmNovedadesConvenios, frmCarga) then
        frmCarga.Show
    else begin
  	    frmCarga.show;
    end;

end;
//TERMINO: TASK_078_JMA_20161128

{INICIO: TASK_125_JMA_20170404}
procedure TMainForm.mnu_RegistrarEntregadeEfectivoyChequesClick(Sender: TObject);
var
    frm: TfrmRegistrarEntregaDineroEmpTransporte;
begin
    if FindFormOrCreate(TfrmRegistrarEntregaDineroEmpTransporte, frm) then
        frm.show
    else
    begin
  	    if not frm.Inicializar(FPuntoVenta, UsuarioSistema) then
            frm.release
        else
            frm.show;
    end;
end;
{TERMINO: TASK_125_JMA_20170404}

{INICIO: TASK_127_JMA_20170405}
procedure TMainForm.mnu_RegistrarRecepcionDepositoClick(Sender: TObject);
var
    frm: TfrmRegistrarRecepcionComprobanteDeposito;
begin
    if FindFormOrCreate(TfrmRegistrarRecepcionComprobanteDeposito, frm) then
        frm.show
    else
    begin
  	    if not frm.Inicializar(PuntoVenta,UsuarioSistema) then
            frm.release
        else
            frm.show;
    end;
end;
{TERMINO: TASK_127_JMA_20170405}


end.
