{-----------------------------------------------------------------------------
 File Name: FrmAnulacionRecibos.pas
 Author:    flamas
 Date Created: xx/06/2005
 Language: ES-AR
 Description:

 Revision 1:

 Author:    ndonadio
 Date Created: 22/06/2005
 Language: ES-AR
 Description: Se elimino el control de turno abierto. 

-----------------------------------------------------------------------------}

unit FrmAnulacionRecibos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ListBoxEx, DBListEx, StdCtrls, DPSControls, VariantComboBox,
  DmiCtrls, DB, ADODB, DMCOnnection, utilDB, UtilFacturacion, utilProc, buscaClientes,
  PeaTypes, RStrings, DBCtrls, peaProcs, Util, PagoVentanilla, Validate,
  DateEdit;

const
	KEY_F9 	= VK_F9;
type
  Tfrm_AnulacionRecibos = class(TForm)
    gbPorCliente: TGroupBox;
    lb_CodigoCliente: TLabel;
    Label5: TLabel;
    peNumeroDocumento: TPickEdit;
    cbConvenios: TVariantComboBox;
    btnAnular: TDPSButton;
    btnSalir: TDPSButton;
    GroupBox1: TGroupBox;
    Label6: TLabel;
    lblNumeroRecibo: TLabel;
    lblEstado: TLabel;
    Label10: TLabel;
    gbDatosRecibo: TGroupBox;
    spObtenerRecibo: TADOStoredProc;
    Label4: TLabel;
    edNumeroRecibo: TNumericEdit;
    Label1: TLabel;
    lblFecha: TLabel;
    lblMensaje: TLabel;
    Label2: TLabel;
    lblImporte: TLabel;
    spObtenerComprobantesRecibo: TADOStoredProc;
    DBListEx1: TDBListEx;
    dsComprobantesRecibo: TDataSource;
    gbAnulacion: TGroupBox;
    Label3: TLabel;
    Label9: TLabel;
    deFechaAnulacion: TDateEdit;
    cbMotivosAnulacion: TVariantComboBox;
    spObtenerMotivosAnulacion: TADOStoredProc;
    spAnularRecibo: TADOStoredProc;
    procedure peNumeroDocumentoChange(Sender: TObject);
    procedure cbConveniosChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure peNumeroDocumentoButtonClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edNumeroReciboChange(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btnAnularClick(Sender: TObject);
  private
    { Private declarations }
    FUltimaBusqueda: TBusquedaCliente;
	FNumeroRecibo: integer;
//    FPuntoEntrega, FPuntoVenta: Integer;
    dFechaRecibo: TDateTime;
	function  CargarRecibos : boolean;
    function  ObtenerMotivosAnulacion : boolean;
    procedure LimpiarDetalles;
//    function ValidarDatosTurno: Boolean;
  public
    { Public declarations }
    function Inicializar: Boolean; overload;
  end;

var
  frm_AnulacionRecibos: Tfrm_AnulacionRecibos;

implementation

{$R *.dfm}
function Tfrm_AnulacionRecibos.Inicializar: Boolean;
begin
    LimpiarDetalles;
    Result := ObtenerMotivosAnulacion;
end;

procedure Tfrm_AnulacionRecibos.FormCreate(Sender: TObject);
begin
    CenterForm(Self);
end;

{-----------------------------------------------------------------------------
  Function Name: peNumeroDocumentoChange
  Author:    flamas
  Date Created: 20/12/2004
  Description: Actualiza los Convenios y Comprobantes Impagos de un cliente determinado
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure Tfrm_AnulacionRecibos.peNumeroDocumentoChange(Sender: TObject);
Var
    CodigoCliente: Integer;
begin
    if ActiveControl <> Sender then Exit;

    edNumeroRecibo.Clear;
    CodigoCliente := QueryGetValueInt(DMConnections.BaseCAC, Format(
      'SELECT CodigoPersona FROM Personas WHERE CodigoDocumento = ''RUT'' ' +
      'AND NumeroDocumento = ''%s''', [iif(peNumeroDocumento.Text <> '',
      									trim(PadL(peNumeroDocumento.Text, 9, '0')), '')]));
    if CodigoCliente > 0 then
        CargarConveniosRUT(DMConnections.BaseCAC, cbConvenios, 'RUT', peNumeroDocumento.Text, False, 0)
    else begin
    	cbConvenios.Clear;
    end;

    if cbConvenios.Items.Count > 0 then cbConvenios.ItemIndex := 0;

    CargarRecibos;
end;

{-----------------------------------------------------------------------------
  Function Name: cbConveniosChange
  Author:    flamas
  Date Created: 21/12/2004
  Description: Procesa un Cambio de Convenio - Trae los comprobantes impagos
  				del dicho Convenio
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure Tfrm_AnulacionRecibos.cbConveniosChange(Sender: TObject);
begin
    if ActiveControl <> Sender then Exit;
    CargarRecibos;
end;

{-----------------------------------------------------------------------------
  Function Name: ObtenerMotivosAnulacion
  Author:    flamas
  Date Created: 17/05/2005
  Description:
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
function  Tfrm_AnulacionRecibos.ObtenerMotivosAnulacion : boolean;
resourcestring
	MSG_ERROR_LOADING_VOID_CAUSES 	= 'Error obteniendo motivos de anulaci�n';
    MSG_ERROR						= 'Error';
begin
	result := False;
	try
    	spObtenerMotivosAnulacion.Open;
        with spObtenerMotivosAnulacion do begin
        	while not EOF do begin
            	cbMotivosAnulacion.Items.Add( 	FieldByName('Descripcion').AsString,
                                                FieldByName('CodigoMotivoAnulacion').AsInteger);
            	Next;
            end;
            Close;
            result := cbMotivosAnulacion.Items.Count > 0;
        end;
    except
        on e: Exception do
            MsgBoxErr(MSG_ERROR_LOADING_VOID_CAUSES, e.Message, MSG_ERROR, MB_ICONERROR);
    end;

end;


{-----------------------------------------------------------------------------
  Function Name: CargarRecibos
  Author:    flamas
  Date Created: 20/12/2004
  Description: Carga los comprobantes impagos de un Convenio determinado
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
function Tfrm_AnulacionRecibos.CargarRecibos : boolean;
resourcestring
    MSG_ERROR_LOADING_TICKET 	= 'Error obteniendo el recibo';
    MSG_HAS_NEWER_INVOICES		= 'Existen notas de cobro emitidas posteriores al recibo.';
    MSG_ERROR					= 'Error';
begin
	Result := False;
    spObtenerRecibo.Close;
    spObtenerComprobantesRecibo.Close;
	spObtenerRecibo.Parameters.ParamByName('@CodigoConvenio').Value := cbConvenios.Value;
	spObtenerRecibo.Parameters.ParamByName('@NumeroRecibo').Value := iif( edNumeroRecibo.Text <> '', edNumeroRecibo.Value, null );

    try
		spObtenerRecibo.Open;
        LimpiarDetalles;

        // Cargamos el Detalle del Recibo.
        with spObtenerRecibo do begin
        	if not IsEmpty then begin
                DisableControls;
				FNumeroRecibo := FieldByName('NumeroRecibo').AsInteger;
                lblNumeroRecibo.Caption := FieldByName('NumeroRecibo').AsString;
                lblFecha.Caption := FormatDateTime( 'dd/mm/yyyy', FieldByName('FechaHora').AsDateTime);
                lblEstado.Caption := FieldByName('DescEstado').AsString;
                lblImporte.Caption := FieldByName('DescImporte').AsString;
                dFechaRecibo := FieldByName('FechaHora').AsDateTime;
                if FieldByName('TieneNotasCobroPosteriores').AsBoolean then lblMensaje.Caption := MSG_HAS_NEWER_INVOICES;
                EnableControls;
                spObtenerComprobantesRecibo.Parameters.ParamByName('@NumeroRecibo').Value := FieldByName('NumeroRecibo').AsInteger;
                spObtenerComprobantesRecibo.Open;
                btnAnular.Enabled := (not FieldByName('TieneNotasCobroPosteriores').AsBoolean) and (FieldByName('Estado').AsString <> 'A');                gbAnulacion.Enabled := btnAnular.Enabled;
                if gbAnulacion.Enabled then begin
                    deFechaAnulacion.Date := NowBase(DMCOnnections.BaseCAC);
                    deFechaAnulacion.ReadOnly := not ExisteAcceso('Modif_Fecha_Anulacion_Recibo');
                    cbMotivosAnulacion.ItemIndex := 0;
                end;
            end;

        end;
        Result := true;
    except
        on e: Exception do
            MsgBoxErr(MSG_ERROR_LOADING_TICKET, e.Message, MSG_ERROR, MB_ICONERROR);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: LimpiarDetalles;
  Author:    flamas
  Date Created: 26/01/2005
  Description: Borra los detalles del Comprobante
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure Tfrm_AnulacionRecibos.LimpiarDetalles;
begin
    lblNumeroRecibo.Caption := '';
    lblFecha.Caption := '';
    lblImporte.Caption := '';
    lblEstado.Caption := '';
    lblMensaje.Caption := '';
    deFechaAnulacion.Date := NullDate;
    cbMotivosAnulacion.Value := Null;
    btnAnular.Enabled := False;
end;

{-----------------------------------------------------------------------------
  Function Name: peNumeroDocumentoButtonClick
  Author:    flamas
  Date Created: 21/12/2004
  Description: Procesa un cambio en el RUT - Muestra los comprobantes pendientes
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure Tfrm_AnulacionRecibos.peNumeroDocumentoButtonClick(Sender: TObject);
var
    f: TFormBuscaClientes;
begin
    Application.CreateForm(TFormBuscaClientes,F);
	if F.Inicializa(FUltimaBusqueda) then begin
        F.ShowModal;
        if F.ModalResult = idOk then begin
            FUltimaBusqueda := F.UltimaBusqueda;
            peNumeroDocumento.Text := f.Persona.NumeroDocumento;
        end;
	end;
    F.free;
end;

{-----------------------------------------------------------------------------
  Function Name: edNumeroReciboChange
  Author:    flamas
  Date Created: 17/05/2005
  Description:
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure Tfrm_AnulacionRecibos.edNumeroReciboChange(Sender: TObject);
begin
    if ActiveControl <> Sender then Exit;
    CargarRecibos;
end;

procedure Tfrm_AnulacionRecibos.btnSalirClick(Sender: TObject);
begin
    close;
end;

procedure Tfrm_AnulacionRecibos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    action := caFree;
end;

procedure Tfrm_AnulacionRecibos.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if (Key = KEY_F9) and btnAnular.Enabled then btnAnular.Click;
end;

{-----------------------------------------------------------------------------
  Function Name: btnPagoClick
  Author:    flamas
  Date Created: 21/12/2004
  Description: Paga una Nota de Cobro
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure Tfrm_AnulacionRecibos.btnAnularClick(Sender: TObject);
resourcestring
    MSG_ERROR 								= 'Error';
    MSG_ERROR_ANULANDO_RECIBO				= 'Error al anular el Recibo';
    MSG_RECIBO_ANULADO_CORRECTAMENTE		= 'El recibo N� %d se anul� correctamente';
    MSG_CONFIRMAR_ANULACION 				= 'Desea anular el recibo ?';
    MSG_CAPTION 							= 'Anulaci�n de Recibo';
    MSG_FECHA_ANULACION_MAYOR_RECIBO 		= 'La fecha de anulaci�n debe ser posterior a la del recibo.';
    MSG_DEBE_SELECCIONAR_MOTIVO_ANULACION 	= 'Debe seleccionar un motivo de anulaci�n.';
    MSG_ANULA_OTRO_RECIBO 					= 'Desea anular otro recibo ?';
begin
	// Valida los datos para anular
    if not ValidateControls(
      [deFechaAnulacion,
       cbMotivosAnulacion],
      [deFechaAnulacion.Date >= dFechaRecibo,
       cbMotivosAnulacion.ItemIndex <> -1],
      caption,
      [MSG_FECHA_ANULACION_MAYOR_RECIBO,
       MSG_DEBE_SELECCIONAR_MOTIVO_ANULACION]) then begin
        exit;
	end;

    if MsgBox(MSG_CONFIRMAR_ANULACION, MSG_CAPTION, MB_ICONQUESTION or MB_DEFBUTTON2 or MB_YESNO) <> IDYES then Exit;

//    if not ValidarDatosTurno then Exit;

    try
        try
            spAnularRecibo.Parameters.ParamByName('@NumeroRecibo').Value 			:= FNumeroRecibo;
            spAnularRecibo.Parameters.ParamByName('@FechaAnulacion').Value 			:= StrToDate(DateToStr(deFechaAnulacion.Date));
            spAnularRecibo.Parameters.ParamByName('@CodigoMotivoAnulacion').Value 	:= cbMotivosAnulacion.Value;
            spAnularRecibo.Parameters.ParamByName('@CodigoUsuario').Value 			:= UsuarioSistema;
            spAnularRecibo.ExecProc;
            MsgBox(Format(MSG_RECIBO_ANULADO_CORRECTAMENTE, [FNumeroRecibo]),MSG_CAPTION, MB_ICONINFORMATION);
        except
            on e: Exception do begin
                MsgBoxErr(MSG_ERROR_ANULANDO_RECIBO, e.Message, MSG_ERROR, MB_ICONERROR);
            end;
        end;
    finally
    	if MsgBox(MSG_ANULA_OTRO_RECIBO, MSG_CAPTION, MB_ICONQUESTION or MB_DEFBUTTON2 or MB_YESNO) <> IDYES then Close
        else begin
    		edNumeroRecibo.Clear;
        	peNumeroDocumento.Clear;
        	LimpiarDetalles;
        end;
    end;
end;
(*
function Tfrm_AnulacionRecibos.ValidarDatosTurno: Boolean;
var
    DatosTurno: TDatosTurno;
begin
    if VerTurnoAbierto(DMConnections.BaseCAC, FPuntoEntrega, FPuntoVenta, UsuarioSistema, DatosTurno) then begin
        GNumeroTurno := DatosTurno.NumeroTurno;
        Result := True;
    end else begin
        if (DatosTurno.NumeroTurno <> -1)
                and ((DatosTurno.Estado = TURNO_ESTADO_ABIERTO)
                and (Trim(DatosTurno.CodigoUsuario) <> (UpperCase(UsuarioSistema)))) then begin
            MsgBox(Format(MSG_ERROR_TURNO_ABIERTO_OTRO_USUARIO, [DatosTurno.CodigoUsuario]),
                Self.Caption, MB_ICONSTOP);
        end else begin
            MsgBox(MSG_ERROR_TURNO_CERRADO, Self.Caption, MB_ICONSTOP);
        end;
        Result := False;
    end;
end;
 *)
end.
