object FormConfigReporteEstadoCaja: TFormConfigReporteEstadoCaja
  Left = 180
  Top = 242
  BorderStyle = bsDialog
  Caption = 'Reporte de Estado de Caja'
  ClientHeight = 114
  ClientWidth = 446
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 14
    Top = 46
    Width = 31
    Height = 13
    Caption = 'Turno:'
  end
  object Label1: TLabel
    Left = 14
    Top = 22
    Width = 39
    Height = 13
    Caption = 'Usuario:'
  end
  object lbl_Usuario: TLabel
    Left = 71
    Top = 20
    Width = 39
    Height = 13
    Caption = 'Label1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object cb_Turnos: TComboBox
    Left = 70
    Top = 38
    Width = 361
    Height = 21
    Style = csDropDownList
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ItemHeight = 13
    ParentFont = False
    TabOrder = 0
  end
  object btn_Aceptar: TDPSButton
    Left = 269
    Top = 74
    Width = 78
    Caption = '&Aceptar'
    Default = True
    TabOrder = 1
    OnClick = btn_AceptarClick
  end
  object btn_Salir: TDPSButton
    Left = 353
    Top = 74
    Caption = '&Salir'
    TabOrder = 2
    OnClick = btn_SalirClick
  end
  object qry_TurnosUsuario: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'Usuario'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    SQL.Strings = (
      'SELECT '
      '* FROM TURNOS'
      'WHERE'
      'CodigoUsuario = :Usuario'
      'ORDER BY FechaHoraApertura DESC')
    Left = 114
    Top = 51
  end
end
