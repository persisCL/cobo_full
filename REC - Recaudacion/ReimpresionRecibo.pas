{********************************** Unit Header ********************************
File Name : ReimpresionRecibo.pas
Author :
Date Created:
Language : ES-AR
Description :

Revision 1
Author : ggomez
Date : 22/09/2005
Description : Agregu� que adem�s de seleccionar un recibo desde la lista, se
    pueda ingresar un n� de recibo.
    Adem�s, modifiqu� el SP ObtenerListaRecibos para que retorne los �ltimos
    100 recibos emitidos. esto se hizo para que no tire TimeOut la ejecuci�n del
    SP.

Revision 2
Author : ggomez
Date : 07/10/2005
Description :
    - Modifiqu� el m�todo ExisteRecibo, ahora se llama
    ExisteReciboNoAnulado y verifica que exista un recibo NO anulado.

    - Modifiqu� el m�todo Inicilizar para que tome un par�metro para indicar
        si el form es MDIChild o No.

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

*******************************************************************************}
unit ReimpresionRecibo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DMConnection, StdCtrls, DmiCtrls, BuscaTab, DB, ADODB, UtilDB, Util,
  UtilProc,FrmImprimir, ReporteRecibo, PeaProcs, ExtCtrls, PeaTypes, jpeg, CobranzasRoutines, SysUtilsCN, PeaProcsCN;

type
  TfrmReimprimeRecibo = class(TForm)
	btRecibos: TBuscaTabla;
    spRecibos: TADOStoredProc;
    dsRecibos: TDataSource;
    bteRecibos: TBuscaTabEdit;
    Label1: TLabel;
    btnImprimir: TButton;
    btnCancelar: TButton;
    Bevel1: TBevel;
    ImgAyuda: TImage;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ImgAyudaClick(Sender: TObject);
    procedure bteRecibosKeyPress(Sender: TObject; var Key: Char);
    procedure btnCancelarClick(Sender: TObject);
    procedure btRecibosSelect(Sender: TObject; Tabla: TDataSet);
    procedure btnImprimirClick(Sender: TObject);
    function btRecibosProcess(Tabla: TDataSet; var Texto: String): Boolean;
  private
	{ Private declarations }
	FNumeroRecibo: Int64;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    Function ExisteReciboNoAnulado(Numero: integer):boolean;
  public
	{ Public declarations }
	function Inicializa(MDIChild: Boolean): boolean;
  end;

var
  frmReimprimeRecibo: TfrmReimprimeRecibo;
  FLogo: TBitmap;

implementation


{$R *.dfm}

procedure TfrmReimprimeRecibo.btnCancelarClick(Sender: TObject);
begin
    close;
end;

function TfrmReimprimeRecibo.Inicializa(MDIChild: Boolean): Boolean;
resourcestring
	MSG_ERROR = 'No se pudo abrir la tabla de datos';
	MSG_LOGO_NO_ENCONTRADO = 'No se ha encontrado el archivo con la imagen del logo para el recibo.';
	MSG_CAPTION			   = 'Cobro de Comprobantes';
begin
    FLogo := TBitMap.Create;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

	try
        if MDIChild then Self.FormStyle := fsMDIChild
        else             Self.FormStyle := fsNormal;

		if not LevantarLogo(FLogo) then MsgBox(MSG_LOGO_NO_ENCONTRADO, MSG_CAPTION, MB_ICONINFORMATION + MB_OK);
        FNumeroRecibo := -1;
        spRecibos.Open;
        Result := True;
	Except
		on e:exception do begin
			Result := False;
			MsgBoxErr(MSG_ERROR,e.Message,'ERROR', mb_ICONSTOP);
		end;
	end;
end;


procedure TfrmReimprimeRecibo.btRecibosSelect(Sender: TObject;
  Tabla: TDataSet);
begin
    bteRecibos.Text := spRecibos.FieldByName('NumeroRecibo').AsString;
end;

procedure TfrmReimprimeRecibo.btnImprimirClick(Sender: TObject);
    resourcestring
        MSG_CONFIRMAR_RECIBO = 'Desea Re Imprimir el Comprobante de Cancelaci�n Nro. %d';
        MSG_IMPRESION_EXITOSA = '�La impresi�n se ha realizado con �xito?';      //SS-1006-1015-MCO-20120904
        MSG_LOGO_NO_ENCONTRADO = 'No se ha encontrado el archivo con la imagen del logo para el recibo.';
        MSG_CAPTION = 'Reimpresi�n';
        MSG_ERROR_NRO_RECIBO = 'Debe indicarse un n�mero de Comprobante de Cancelaci�n.';
        MSG_ERROR_RECIBO_NO_EXISTE = 'No existe Comprobante de Cancelaci�n para el n�mero seleccionado.';
        MSG_ERROR_RECIBO_REFINAN   = 'El Recibo pertenece a una Cancelaci�n por Avenimiento o Repactaci�n. NO se puede reimprimir.';
    var
        r: TformRecibo;
        RespuestaImpresion: TRespuestaImprimir;
begin
    try
        try
            FNumeroRecibo := bteRecibos.ValueInt;


            if ValidateControls([bteRecibos, bteRecibos, bteRecibos], [(FNumeroRecibo > 0) , ExisteReciboNoAnulado(FNumeroRecibo), not EsReciboCancelacionRefinanciacion(DMConnections.BaseCAC, FNumeroRecibo)],
                Caption, [MSG_ERROR_NRO_RECIBO, MSG_ERROR_RECIBO_NO_EXISTE, MSG_ERROR_RECIBO_REFINAN]) then begin

                // Mostrar el di�logo previo a realizar la impresi�n.
                RespuestaImpresion := frmImprimir.Ejecutar(MSG_CAPTION, Format(MSG_CONFIRMAR_RECIBO, [FNumeroRecibo]));

                case RespuestaImpresion of
                    // Si la respuesta fue Aceptar, ejecutar el reporte.
                    riAceptarConfigurarImpresora, riAceptar: begin
                        Application.CreateForm(TformRecibo, r);
                        try
                            if r.Inicializar(FLogo, FNumeroRecibo, RespuestaImpresion = riAceptarConfigurarImpresora) then begin
                                // Preguntar si la impresi�n fue exitosa.
                                while MsgBox(MSG_IMPRESION_EXITOSA, MSG_CAPTION, MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = mrNo do begin
                                    // Mostrar el di�logo de Configurar Impresora para permitir al operador
                                    // reintentar la impresi�n.
                                    if not r.Inicializar(FLogo, fNumeroRecibo, True) then Break;
                                end;
                            end;
                        finally
                            if assigned(r) then FreeAndNil(r);
                        end;
                    end;
                end;
            end;
        except
            on e: Exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally

    end;
end;

function TfrmReimprimeRecibo.ExisteReciboNoAnulado(Numero: integer): boolean;
begin
    Result := UpperCase(QueryGetValue(DMConnections.BaseCAC,
                Format('SELECT dbo.ExisteComprobanteNoAnulado(%s, %d)', [QuotedStr(TC_RECIBO), Numero]))) = 'TRUE';
end;

function TfrmReimprimeRecibo.btRecibosProcess(Tabla: TDataSet;
  var Texto: String): Boolean;
begin
    Texto := Tabla.FieldByName('NumeroRecibo').AsString + Space(1) +
      Tabla.FieldByName('Descripcion').AsString;
    Result := True;
end;

procedure TfrmReimprimeRecibo.bteRecibosKeyPress(Sender: TObject;
  var Key: Char);
begin
    // Si ingresa el signo - (menos), obviarlo.
    if Key = '-' then Key := #0;
end;

procedure TfrmReimprimeRecibo.ImgAyudaClick(Sender: TObject);
resourcestring
    MSG_AYUDA   = ' ' + 'Reimpresi�n de Recibos No Anulados.'
                    + CRLF
                    + CRLF
                    + 'Ingrese el N� del Recibo o seleccione uno de entre'
                    + CRLF +'los �ltimos Recibos emitidos.' + CRLF +
                    ' ';
begin
    // Mostrar el mensaje
    MsgBoxBalloon(MSG_AYUDA, Caption, MB_ICONQUESTION, IMGAYUDA);
end;

procedure TfrmReimprimeRecibo.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

initialization
    FLogo := nil;
finalization
    FreeAndNil(FLogo);
end.
