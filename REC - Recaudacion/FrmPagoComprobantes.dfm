object formPagoComprobantes: TformPagoComprobantes
  Left = 196
  Top = 211
  BorderStyle = bsDialog
  Caption = 'Pago de Comprobantes'
  ClientHeight = 269
  ClientWidth = 660
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  DesignSize = (
    660
    269)
  PixelsPerInch = 96
  TextHeight = 13
  object gbPorCliente: TGroupBox
    Left = 3
    Top = 2
    Width = 649
    Height = 100
    Anchors = [akLeft, akTop, akRight]
    Caption = ' B'#250'squeda Comprobante '
    Color = clBtnFace
    ParentColor = False
    TabOrder = 0
    object lb_CodigoCliente: TLabel
      Left = 12
      Top = 55
      Width = 61
      Height = 13
      Caption = 'RUT Cliente:'
    end
    object Label5: TLabel
      Left = 184
      Top = 55
      Width = 103
      Height = 13
      Caption = 'N'#250'mero de Convenio:'
    end
    object Label1: TLabel
      Left = 424
      Top = 55
      Width = 154
      Height = 13
      Caption = 'Tal'#243'n Asociado al Comprobante:'
    end
    object Label3: TLabel
      Left = 12
      Top = 17
      Width = 97
      Height = 13
      Caption = 'Tipos Comprobantes'
    end
    object Label4: TLabel
      Left = 184
      Top = 17
      Width = 103
      Height = 13
      Caption = 'N'#250'mero Comprobante'
    end
    object Label9: TLabel
      Left = 424
      Top = 17
      Width = 81
      Height = 13
      Caption = 'C'#243'digo de Barras'
    end
    object peNumeroDocumento: TPickEdit
      Left = 12
      Top = 69
      Width = 145
      Height = 21
      CharCase = ecUpperCase
      Enabled = True
      TabOrder = 3
      OnChange = peNumeroDocumentoChange
      Decimals = 0
      EditorStyle = bteTextEdit
      OnButtonClick = peNumeroDocumentoButtonClick
    end
    object cbConvenios: TVariantComboBox
      Left = 184
      Top = 69
      Width = 209
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 4
      OnChange = cbConveniosChange
      Items = <>
    end
    object cbComprobantes: TComboBox
      Left = 424
      Top = 69
      Width = 217
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 5
      OnChange = cbComprobantesChange
    end
    object edNumeroComprobante: TNumericEdit
      Left = 184
      Top = 32
      Width = 209
      Height = 21
      TabOrder = 2
      OnChange = edNumeroComprobanteChange
      Decimals = 0
    end
    object edCodigoBarras: TEdit
      Left = 424
      Top = 32
      Width = 217
      Height = 21
      TabOrder = 0
      OnChange = edCodigoBarrasChange
      OnKeyPress = edCodigoBarrasKeyPress
    end
    object cbTiposComprobantes: TVariantComboBox
      Left = 12
      Top = 32
      Width = 145
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 1
      OnChange = cbTiposComprobantesChange
      Items = <>
    end
  end
  object btnPagoEfectivo: TDPSButton
    Left = 5
    Top = 236
    Width = 196
    Anchors = [akLeft, akBottom]
    Caption = 'Pagar (F9)'
    Default = True
    TabOrder = 1
    OnClick = btnPagoEfectivoClick
  end
  object btnSalir: TDPSButton
    Left = 579
    Top = 236
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Salir'
    TabOrder = 2
    OnClick = btnSalirClick
  end
  object GroupBox1: TGroupBox
    Left = 4
    Top = 103
    Width = 650
    Height = 60
    Anchors = [akLeft, akTop, akRight]
    Caption = ' Datos del Cliente '
    TabOrder = 3
    DesignSize = (
      650
      60)
    object Label6: TLabel
      Left = 17
      Top = 18
      Width = 48
      Height = 13
      Anchors = [akLeft, akBottom]
      Caption = 'Nombre:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblNombre: TLabel
      Left = 91
      Top = 18
      Width = 72
      Height = 13
      Anchors = [akLeft, akTop, akRight]
      Caption = 'Apellido Cliente'
    end
    object lblDomicilio: TLabel
      Left = 90
      Top = 39
      Width = 74
      Height = 13
      Anchors = [akLeft, akTop, akRight]
      Caption = 'DomicilioCliente'
    end
    object Label10: TLabel
      Left = 16
      Top = 39
      Width = 56
      Height = 13
      Caption = 'Domicilio:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object gbDatosComprobante: TGroupBox
    Left = 4
    Top = 168
    Width = 650
    Height = 62
    Anchors = [akLeft, akTop, akRight]
    Caption = ' Datos del Tal'#243'n Asociado al Comprobante'
    TabOrder = 4
    object Label2: TLabel
      Left = 12
      Top = 18
      Width = 105
      Height = 13
      Caption = 'Fecha de Emisi'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblFecha: TLabel
      Left = 149
      Top = 18
      Width = 91
      Height = 13
      AutoSize = False
      Caption = 'Fecha'
    end
    object lblFechaVencimiento: TLabel
      Left = 149
      Top = 40
      Width = 91
      Height = 13
      AutoSize = False
      Caption = 'Fecha Vencimiento'
    end
    object Label7: TLabel
      Left = 12
      Top = 40
      Width = 131
      Height = 13
      Caption = 'Fecha de Vencimiento:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblTotalAPagar: TLabel
      Left = 502
      Top = 18
      Width = 64
      Height = 13
      AutoSize = False
      Caption = 'Total a Pagar'
    end
    object Label12: TLabel
      Left = 413
      Top = 18
      Width = 82
      Height = 13
      Caption = 'Total a Pagar:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblPagado: TLabel
      Left = 502
      Top = 40
      Width = 47
      Height = 13
      Caption = 'lblPagado'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label11: TLabel
      Left = 413
      Top = 40
      Width = 48
      Height = 13
      Caption = 'Pagado:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object ObtenerComprobantesImpagos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerComprobantesImpagos'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 524
    Top = 115
  end
end
