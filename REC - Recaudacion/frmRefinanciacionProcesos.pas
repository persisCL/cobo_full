{
  Author  : Mcabello
  Date    : 09/08/2013
  Firma   : SS_1121_MCA_20130809
  Description: valida que no bloquee la refinanciacion si tiene mas de un cheque
  se agrega codigorefinanciacion + Numerodocumento como clave de bloqueo


 }
unit frmRefinanciacionProcesos;

interface

uses
	PeaTypes,
	PeaProcs,
    UtilProc,
    UtilDB,
    Util,
    UtilFacturacion,
    DMConnection,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Contnrs,

  CobranzasResources, frmMuestraMensaje,
  Dialogs, ComCtrls, ExtCtrls, StdCtrls, DmiCtrls, DB, ADODB, Validate, DateEdit, ListBoxEx, DBListEx, ImgList, DBClient, Provider,
  PeaProcsCN, SysUtilsCN, frmRefinanciacionChequesDepositadosRpt, BuscaClientes;

type
  TRefinanciacionProcesosForm = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    btnBuscar: TButton;
    cbOpciones: TComboBox;
    spObtenerRefinanciacionChequesADepositar: TADOStoredProc;
    pcOpciones: TPageControl;
    tbsVacio: TTabSheet;
    tbsDeposito: TTabSheet;
    Panel3: TPanel;
    btnSalir: TButton;
    spActualizarRefinanciacionCuotasEstados: TADOStoredProc;
    spCrearRefinanciacionComprobanteDeposito: TADOStoredProc;
    spCrearRefinanciacionComprobanteDepositoDetalle: TADOStoredProc;
    tbsDepositado: TTabSheet;
    spObtenerRefinanciacionChequesDepositados: TADOStoredProc;
    DBlExDepositados: TDBListEx;
    Panel2: TPanel;
    btnDepositar: TButton;
    dsChequesDepositados: TDataSource;
    Panel4: TPanel;
    btnPagarProtestar: TButton;
    cdsChequesDepositados: TClientDataSet;
    dspRefinanciacionObtenerChequesDepositados: TDataSetProvider;
    cdsChequesDepositadosPagado: TBooleanField;
    cdsChequesDepositadosProtestado: TBooleanField;
    lnCheck: TImageList;
    Panel5: TPanel;
    cdsChequesADepositar: TClientDataSet;
    dspRefinanciacionObtenerChequesADepositar: TDataSetProvider;
    cdsChequesADepositarDepositar: TBooleanField;
    DBListEx1: TDBListEx;
    dsChequesADepositar: TDataSource;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    deFechaDocumento: TDateEdit;
    Label2: TLabel;
    chkPendiente: TCheckBox;
    chkProtestado: TCheckBox;
    chkEnCobranza: TCheckBox;
    chkMoraPactada: TCheckBox;
    chkDepositar: TCheckBox;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    nedtNroDeposito: TNumericEdit;
    Label5: TLabel;
    nedtNroRefinanciacion: TNumericEdit;
    chkPagar: TCheckBox;
    chkProtestar: TCheckBox;
    tbsCobrarVV: TTabSheet;
    cdsValeVistasACobrar: TClientDataSet;
    dsValeVistasACobrar: TDataSource;
    spRefinanciacionObtenerValeVistasACobrar: TADOStoredProc;
    dspRefinanciacionObtenerValeVistasACobrar: TDataSetProvider;
    cdsValeVistasACobrarIDRefinanciacion: TIntegerField;
    cdsValeVistasACobrarIDDetallePagoComprobante: TIntegerField;
    cdsValeVistasACobrarEstado: TSmallintField;
    cdsValeVistasACobrarRut: TStringField;
    cdsValeVistasACobrarCodigoConvenio: TIntegerField;
    cdsValeVistasACobrarCodigoRefinanciacion: TIntegerField;
    cdsValeVistasACobrarNumeroConvenio: TStringField;
    cdsValeVistasACobrarNombre: TStringField;
    cdsValeVistasACobrarChequeNumero: TStringField;
    cdsValeVistasACobrarChequeMonto: TLargeintField;
    cdsValeVistasACobrarMontoDesc: TStringField;
    cdsValeVistasACobrarChequeFecha: TDateTimeField;
    cdsValeVistasACobrarChequeTitular: TStringField;
    cdsValeVistasACobrarChequeCodigoBanco: TIntegerField;
    cdsValeVistasACobrarFormaPagoDescr: TStringField;
    cdsValeVistasACobrarBancoDescr: TStringField;
    cdsValeVistasACobrarCobrar: TBooleanField;
    Panel6: TPanel;
    GroupBox3: TGroupBox;
    Label6: TLabel;
    deFechaValeVistas: TDateEdit;
    chkCobrar: TCheckBox;
    Panel7: TPanel;
    btnCobrarVV: TButton;
    DBListEx2: TDBListEx;
    tbsPagoCuotas: TTabSheet;
    DBListEx3: TDBListEx;
    Panel8: TPanel;
    btnPagarCuota: TButton;
    Panel9: TPanel;
    GroupBox4: TGroupBox;
    Label7: TLabel;
    Label8: TLabel;
    nedtRefinanciacionPagoCuotas: TNumericEdit;
    chkPagarCuotas: TCheckBox;
    peRut: TPickEdit;
    spObtenerRefinanciacionCuotasAPagar: TADOStoredProc;
    dspRefinanciacionObtenerCuotasAPagar: TDataSetProvider;
    cdsCuotasAPagar: TClientDataSet;
    dsCuotasAPagar: TDataSource;
    cdsCuotasAPagarPagar: TBooleanField;
    cdsChequesADepositarCodigoRefinanciacion: TIntegerField;
    cdsChequesADepositarCodigoCuota: TSmallintField;
    cdsChequesADepositarCodigoEstadoCuota: TSmallintField;
    cdsChequesADepositarDescripcionEstadoCuota: TStringField;
    cdsChequesADepositarCodigoFormaPago: TSmallintField;
    cdsChequesADepositarDescripcionFormaPago: TStringField;
    cdsChequesADepositarTitularNombre: TStringField;
    cdsChequesADepositarTitularTipoDocumento: TStringField;
    cdsChequesADepositarTitularNumeroDocumento: TStringField;
    cdsChequesADepositarCodigoBanco: TIntegerField;
    cdsChequesADepositarDescripcionBanco: TStringField;
    cdsChequesADepositarDocumentoNumero: TStringField;
    cdsChequesADepositarFechaCuota: TDateTimeField;
    cdsChequesADepositarImporte: TLargeintField;
    cdsChequesADepositarEstadoImpago: TBooleanField;
    cdsChequesADepositarNumeroConvenio: TStringField;
    cdsChequesADepositarImporteDesc: TStringField;
    cdsChequesDepositadosIDComprobanteDeposito: TIntegerField;
    cdsChequesDepositadosUsuarioCreacion: TStringField;
    cdsChequesDepositadosCodigoRefinanciacion: TIntegerField;
    cdsChequesDepositadosCodigoCuota: TSmallintField;
    cdsChequesDepositadosTitularNumeroDocumento: TStringField;
    cdsChequesDepositadosTitularNombre: TStringField;
    cdsChequesDepositadosCodigoConvenio: TIntegerField;
    cdsChequesDepositadosNumeroConvenio: TStringField;
    cdsChequesDepositadosDocumentoNumero: TStringField;
    cdsChequesDepositadosImporte: TLargeintField;
    cdsChequesDepositadosImporteDesc: TStringField;
    cdsChequesDepositadosBancoDesc: TStringField;
    cdsChequesDepositadosFormaPagoDesc: TStringField;
    spActualizarRefinanciacionComprobanteDepositoDetalle: TADOStoredProc;
    cdsChequesDepositadosDescripcionFormaPago: TStringField;
    lblNombreCli: TLabel;
    lblPersoneria: TLabel;
    cdsCuotasAPagarRut: TStringField;
    cdsCuotasAPagarNombrePersona: TStringField;
    cdsCuotasAPagarCodigoRefinanciacion: TIntegerField;
    cdsCuotasAPagarCodigoCuota: TSmallintField;
    cdsCuotasAPagarCodigoEstadoCuota: TSmallintField;
    cdsCuotasAPagarDescripcionEstadoCuota: TStringField;
    cdsCuotasAPagarCodigoFormaPago: TSmallintField;
    cdsCuotasAPagarDescripcionFormaPago: TStringField;
    cdsCuotasAPagarTitularNombre: TStringField;
    cdsCuotasAPagarTitularTipoDocumento: TStringField;
    cdsCuotasAPagarTitularNumeroDocumento: TStringField;
    cdsCuotasAPagarCodigoBanco: TIntegerField;
    cdsCuotasAPagarDescripcionBanco: TStringField;
    cdsCuotasAPagarDocumentoNumero: TStringField;
    cdsCuotasAPagarFechaCuota: TDateTimeField;
    cdsCuotasAPagarImporte: TLargeintField;
    cdsCuotasAPagarImporteDesc: TStringField;
    cdsCuotasAPagarEstadoImpago: TBooleanField;
    cdsCuotasAPagarNumeroConvenio: TStringField;
    cdsCuotasAPagarSaldo: TLargeintField;
    cdsCuotasAPagarSaldoDesc: TStringField;
    cdsChequesDepositadosFechaDeposito: TDateTimeField;
    cdsChequesDepositadosFechaCuota: TDateTimeField;
    cdsChequesDepositadosTitularTipoDocumento: TStringField;
    cdsChequesDepositadosCodigoBanco: TIntegerField;
    cdsChequesDepositadosCodigoTarjeta: TIntegerField;
    cdsChequesDepositadosTarjetaCuotas: TSmallintField;
    cdsChequesDepositadosCodigoFormaPago: TSmallintField;
    spActualizarRefinanciacionCuotaPago: TADOStoredProc;
    cdsChequesADepositarSaldo: TLargeintField;
    cdsChequesADepositarSaldoDesc: TStringField;
    deFechaPagoDeposito: TDateEdit;     // SS_637_PDO_20110503
    Label9: TLabel;
    procedure cbOpcionesChange(Sender: TObject);
    procedure btnBuscarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnDepositarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure DBlExDepositadosDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
    procedure DBlExDepositadosLinkClick(Sender: TCustomDBListEx; Column: TDBListExColumn);
    procedure chkPagarClick(Sender: TObject);
    procedure chkProtestarClick(Sender: TObject);
    Procedure OrdenarColumna(Sender: TObject);
    procedure btnPagarProtestarClick(Sender: TObject);
    procedure DBListEx1DrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
    procedure DBListEx1LinkClick(Sender: TCustomDBListEx; Column: TDBListExColumn);
    procedure DBListEx2LinkClick(Sender: TCustomDBListEx; Column: TDBListExColumn);
    procedure DBListEx2DrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
    procedure btnCobrarVVClick(Sender: TObject);
    procedure peRutButtonClick(Sender: TObject);
    procedure DBListEx3DrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
    procedure DBListEx3LinkClick(Sender: TCustomDBListEx; Column: TDBListExColumn);
    procedure btnPagarCuotaClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure peRutKeyPress(Sender: TObject; var Key: Char);
    procedure peRutChange(Sender: TObject);
    procedure peRutKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure chkDepositarClick(Sender: TObject);
    procedure chkPagarCuotasClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);   // SS_637_PDO_20110503
  private
    { Private declarations }
    FUltimaBusqueda: TBusquedaCliente;
    FCodigoCliente: Integer;
    FbtnDepositarPermisos,
    FbtnPagarProtestarPermisos: Boolean;
    procedure MostrarDatosCliente(CodigoCliente: Integer; Personeria: string);
    function ValidarFechaPagoDeposito: Boolean; // SS_637_PDO_20110503
  public
    { Public declarations }
    FComponentes: TObjectList;

    function Inicializar : boolean;
    function Validaciones : boolean;

    procedure MostrarDatos;
    Procedure MostrarChequesDepositados;
    Procedure MostrarChequesADepositar;
    Procedure GenerarDeposito;
    procedure MostrarValeVistasACobrar;
    procedure MostrarCuotasAPagar;
    procedure ActualizaChequesDepositadosAccion;
  end;

var
  RefinanciacionProcesosForm: TRefinanciacionProcesosForm;

const
    SQLObtenerNombrePersona = 'SELECT dbo.ObtenerNombrePersona(%d)';

implementation

{$R *.dfm}

const
	//Opciones de la ComboBox
    CBX_STR_CHEQUES_DEPOSITO	    = 'Listado de Cheques para Dep�sito';
    CBX_STR_CHEQUES_PAGAR_PROTESTAR = 'Pagar/Protestar Cheques';
    CBX_STR_VALE_VISTA_COBRAR       = 'Cobrar Vale Vistas';
    CBX_STR_PAGO_CUOTAS             = 'Pago Cuotas';

resourcestring
    MSG_1		= 'No se pudo actualizar fecha vencimiento';
    MSG_2		= 'No se pudo cambiar el estado a pagado';
    MSG_3		= 'No se pudo cambiar el estado';
    MSG_4		= 'No se pudo dejar registro en hist�rico';
    MSG_5		= 'No se pudo actualizar el monto pagado en la refinanciaci�n';
    MSG_6		= 'No se pudo cambiar el estado de la refinanciaci�n a Terminada';

    RS_TITULO_AVISO_TURNO  = 'Apertura de Turno';
    RS_MENSAJE_AVISO_TURNO = 'Para realizar la Operaci�n es necesario tener un Turno Abierto.';

{------------------------------------------------------------------------
        	Inicializar

Author: mbecerra
Date: 09-Enero-2009
Description:	Inicializa el formulario cargando las opciones b�sicas.
----------------------------------------------------------------------------}
function TRefinanciacionProcesosForm.Inicializar;
resourcestring
	STR_CAPTION             = 'Procesos de Refinanciaci�n';
    MSG_ERROR	            = 'Error al Inicilizar Formulario de Procesos de Refinanciaci�n. Error: %s';
begin
    try
        Result := False;
        CambiarEstadoCursor(CURSOR_RELOJ);

        try
            Caption := STR_CAPTION;

            CargarConstantesCobranzasRefinanciacion;

            FbtnDepositarPermisos      := ExisteAcceso('deposito_proc_ref');
            FbtnPagarProtestarPermisos := ExisteAcceso('pagar_protest_cheq_proc_ref');

            //Cargar las opciones de la Combobox
            cbOpciones.Clear;
            cbOpciones.Items.Add(CBX_STR_CHEQUES_DEPOSITO);
            cbOpciones.Items.Add(CBX_STR_CHEQUES_PAGAR_PROTESTAR);
    //    	cbOpciones.Items.Add(CBX_STR_VALE_VISTA_COBRAR);
    //        cbOpciones.Items.Add(CBX_STR_PAGO_CUOTAS);

            tbsVacio.TabVisible := False;
            tbsDeposito.TabVisible := False;
            tbsDepositado.TabVisible := False; // SS_637_20100928
            tbsCobrarVV.TabVisible := False; // SS_637_20101007
            tbsPagoCuotas.TabVisible := False; // SS_637_20101009
            pcOpciones.ActivePage := tbsVacio;


            btnDepositar.Enabled      := False; // SS_637_20100928
            btnPagarProtestar.Enabled := False;

            deFechaDocumento.Date    := NowBase(DMConnections.BaseCAC); // SS_637_20100927
            deFechaValeVistas.Date   := NowBase(DMConnections.BaseCAC); // SS_637_20101007
            deFechaPagoDeposito.Date := Trunc(NowBase(DMConnections.BaseCAC)); // SS_637_PDO_20110503

            lblNombreCli.Caption  := EmptyStr;
            lblPersoneria.Caption := EmptyStr;

            Result := True;
        except on e:exception do begin
            ShowMsgBoxCN(STR_CAPTION, Format(MSG_ERROR, [e.Message]), MB_ICONERROR, Self);
            end;
        end;
    finally
        TPanelMensajesForm.OcultaPanel;
        CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

{------------------------------------------------------------------------
        	btnBuscarClick

Author: mbecerra
Date: 09-Enero-2009
Description:	Recupera los registros de acuerdo a la opci�n indicada
----------------------------------------------------------------------------}
procedure TRefinanciacionProcesosForm.btnBuscarClick(Sender: TObject);
begin
	if Validaciones() then MostrarDatos();
end;

{------------------------------------------------------------------------
        	cbOpcionesChange

Author: mbecerra
Date: 09-Enero-2009
Description:	Muestra las opciones disponibles para el item seleccionado.
----------------------------------------------------------------------------}
procedure TRefinanciacionProcesosForm.cbOpcionesChange(Sender: TObject);
begin
	if cbOpciones.Text = CBX_STR_CHEQUES_DEPOSITO then
        pcOpciones.ActivePage := tbsDeposito
    else if cbOpciones.Text = CBX_STR_CHEQUES_PAGAR_PROTESTAR then
        pcOpciones.ActivePage := tbsDepositado
    else if cbOpciones.Text = CBX_STR_VALE_VISTA_COBRAR then
        pcOpciones.ActivePage := tbsCobrarVV
    else pcOpciones.ActivePage := tbsPagoCuotas;
end;

procedure TRefinanciacionProcesosForm.chkDepositarClick(Sender: TObject);
    var
        Puntero: TBookmark;
begin
    if cdsChequesADepositar.Active and (cdsChequesADepositar.RecordCount > 0) then
        with cdsChequesADepositar do try
            Screen.Cursor := crDefault;
            Application.ProcessMessages;

            dsChequesADepositar.DataSet := Nil;
            Puntero := cdsChequesADepositar.GetBookmark;
            First;

            while not eof do begin
                Edit;
                cdsChequesADepositarDepositar.AsBoolean := chkDepositar.Checked;
                Post;
                Next;
            end;
        finally
            if Assigned(Puntero) then begin
                if cdsChequesADepositar.BookmarkValid(Puntero) then begin
                    cdsChequesADepositar.GotoBookmark(Puntero);
                end;
                cdsChequesADepositar.FreeBookmark(Puntero);
            end;
            dsChequesADepositar.DataSet := cdsChequesADepositar;
            Screen.Cursor := crDefault;
        end;
end;

procedure TRefinanciacionProcesosForm.chkPagarClick(Sender: TObject);
begin
    if chkPagar.Checked then
        if chkProtestar.Checked then chkProtestar.Checked := False;
    ActualizaChequesDepositadosAccion;
end;

procedure TRefinanciacionProcesosForm.chkPagarCuotasClick(Sender: TObject);
    var
        Puntero: TBookmark;
begin
    if cdsCuotasAPagar.Active and (cdsCuotasAPagar.RecordCount > 0) then
        with cdsCuotasAPagar do try
            Screen.Cursor := crHourGlass;
            Application.ProcessMessages;

            Puntero := cdsCuotasAPagar.GetBookmark;
            dsCuotasAPagar.DataSet := Nil;
            First;

            while not Eof do begin
                Edit;
                cdsCuotasAPagarPagar.AsBoolean := chkPagarCuotas.Checked;
                Post;
                Next;
            end;
        finally
            if Assigned(Puntero) then begin
                if cdsCuotasAPagar.BookmarkValid(Puntero) then begin
                    cdsCuotasAPagar.GotoBookmark(Puntero);
                end;
                cdsCuotasAPagar.FreeBookmark(Puntero);
            end;

            dsCuotasAPagar.DataSet := cdsCuotasAPagar;
            Screen.Cursor := crDefault;
        end;
end;

procedure TRefinanciacionProcesosForm.chkProtestarClick(Sender: TObject);
begin
    if chkProtestar.Checked then
        if chkPagar.Checked then chkPagar.Checked := False;
    ActualizaChequesDepositadosAccion;
end;

procedure TRefinanciacionProcesosForm.DBlExDepositadosDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
var
    bmp: TBitMap;
begin
    with Sender do begin
        if cdsChequesDepositados.FieldByName(Column.FieldName).DataType = ftDateTime then begin
            Text := FormatDateTime('dd/mm/yyyy', cdsChequesDepositados.FieldByName(Column.FieldName).AsDateTime);
        end;

        if (Column.FieldName = 'Pagado') or (Column.FieldName = 'Protestado') then try
            Text := '';
            Canvas.FillRect(Rect);
            bmp := TBitMap.Create;
			DefaultDraw := False;

            if (cdsChequesDepositados.FieldByName(Column.FieldName).AsBoolean ) then
                lnCheck.GetBitmap(0, Bmp)
            else
                lnCheck.GetBitmap(1, Bmp);

            Canvas.Draw((Rect.Left + Rect.Right - Bmp.Width) div 2, Rect.Top, bmp);
        finally
            bmp.Free;
        end; // finally


    end; // with
end;

procedure TRefinanciacionProcesosForm.DBlExDepositadosLinkClick(Sender: TCustomDBListEx; Column: TDBListExColumn);
begin
    try
        cdsChequesDepositados.Edit;
        if Column.FieldName = 'Pagado' then begin
            if cdsChequesDepositados.FieldByName('Pagado').AsBoolean then
                cdsChequesDepositados.FieldByName('Pagado').AsBoolean := False
            else begin
                cdsChequesDepositados.FieldByName('Pagado').AsBoolean := True;
                cdsChequesDepositados.FieldByName('Protestado').AsBoolean := False;
            end;
        end;

        if Column.FieldName = 'Protestado' then begin
            if cdsChequesDepositados.FieldByName('Protestado').AsBoolean then
                cdsChequesDepositados.FieldByName('Protestado').AsBoolean := False
            else begin
                cdsChequesDepositados.FieldByName('Protestado').AsBoolean := True;
                cdsChequesDepositados.FieldByName('Pagado').AsBoolean := False;
            end;
        end;
    finally
        cdsChequesDepositados.Post;
    end;
end;

procedure TRefinanciacionProcesosForm.DBListEx1DrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
var
    bmp: TBitMap;
begin
    with Sender do begin
        if cdsChequesADepositarImporte.AsLargeInt <> cdsChequesADepositarSaldo.AsLargeInt then begin
            if (Column.FieldName = 'SaldoDesc') or (Column.FieldName = 'ImporteDesc') then begin
                Canvas.Font.Style := Column.Font.Style + [fsBold];

                if Column.FieldName = 'SaldoDesc' then begin
                    if not (odSelected in State) then Canvas.Font.Color := clMaroon;
                end;
            end;
        end;

        if (Column.FieldName = 'Depositar') then try
            Text := '';
            Canvas.FillRect(Rect);
            bmp := TBitMap.Create;
			DefaultDraw := False;

            if cdsChequesADepositarImporte.AsLargeInt <> cdsChequesADepositarSaldo.AsLargeInt then begin
                lnCheck.GetBitmap(2, Bmp)
            end
            else begin
                if (cdsChequesADepositar.FieldByName(Column.FieldName).AsBoolean ) then
                    lnCheck.GetBitmap(0, Bmp)
                else
                    lnCheck.GetBitmap(1, Bmp);
            end;

            Canvas.Draw((Rect.Left + Rect.Right - Bmp.Width) div 2, Rect.Top, bmp);
        finally
            bmp.Free;
        end; // finally
    end; // with
end;

procedure TRefinanciacionProcesosForm.DBListEx1LinkClick(Sender: TCustomDBListEx; Column: TDBListExColumn);
    resourcestring
        MSG_ERROR_TITULO = 'Validaci�n Dep�sito';
        MSG_ERROR_TEXTO  = 'No se puede depositar el cheque, pues tiene pagos A Cuenta.';
begin
    if cdsChequesADepositarImporte.AsLargeInt = cdsChequesADepositarSaldo.AsLargeInt then try
        cdsChequesADepositar.Edit;
        cdsChequesADepositarDepositar.AsBoolean := not cdsChequesADepositarDepositar.AsBoolean;
    finally
        cdsChequesADepositar.Post;
    end
    else ShowMsgBoxCN(MSG_ERROR_TITULO, MSG_ERROR_TEXTO, MB_ICONWARNING, Self);
end;

procedure TRefinanciacionProcesosForm.DBListEx2DrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
var
    bmp: TBitMap;
begin
    with Sender do begin
        if (Column.FieldName = 'Cobrar') then try
            Text := '';
            Canvas.FillRect(Rect);
            bmp := TBitMap.Create;
			DefaultDraw := False;

            if (cdsValeVistasACobrar.FieldByName(Column.FieldName).AsBoolean ) then
                lnCheck.GetBitmap(0, Bmp)
            else
                lnCheck.GetBitmap(1, Bmp);

            Canvas.Draw((Rect.Left + Rect.Right - Bmp.Width) div 2, Rect.Top, bmp);
        finally
            bmp.Free;
        end; // finally
    end; // with
end;

procedure TRefinanciacionProcesosForm.DBListEx2LinkClick(Sender: TCustomDBListEx; Column: TDBListExColumn);
begin
    try
        cdsValeVistasACobrar.Edit;
        if Column.FieldName = 'Cobrar' then begin
            cdsValeVistasACobrarCobrar.AsBoolean :=  not cdsValeVistasACobrarCobrar.AsBoolean;
        end;
    finally
        cdsValeVistasACobrar.Post;
    end;
end;

procedure TRefinanciacionProcesosForm.DBListEx3DrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
var
    bmp: TBitMap;
begin
    with Sender do begin
        if (Column.FieldName = 'Pagar') then try
            Text := '';
            Canvas.FillRect(Rect);
            bmp := TBitMap.Create;
			DefaultDraw := False;

            if (cdsCuotasAPagar.FieldByName(Column.FieldName).AsBoolean ) then
                lnCheck.GetBitmap(0, Bmp)
            else
                lnCheck.GetBitmap(1, Bmp);

            Canvas.Draw((Rect.Left + Rect.Right - Bmp.Width) div 2, Rect.Top, bmp);
        finally
            bmp.Free;
        end; // finally
    end; // with
end;

procedure TRefinanciacionProcesosForm.DBListEx3LinkClick(Sender: TCustomDBListEx; Column: TDBListExColumn);
begin
    try
        cdsCuotasAPagar.Edit;
        cdsCuotasAPagarPagar.AsBoolean := not cdsCuotasAPagarPagar.AsBoolean;
    finally
        cdsCuotasAPagar.Post;
    end;
end;

{------------------------------------------------------------------------
        	FormClose

Author: mbecerra
Date: 09-Enero-2009
Description:	Cierra el formulario y limpia las variables privadas.
----------------------------------------------------------------------------}
procedure TRefinanciacionProcesosForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TRefinanciacionProcesosForm.FormCreate(Sender: TObject);
begin
    FFormState := FFormState - [fsVisible];
end;

procedure TRefinanciacionProcesosForm.FormDestroy(Sender: TObject);
begin
    RefinanciacionProcesosForm := Nil;
end;

{------------------------------------------------------------------------
        	Validaciones

Author: mbecerra
Date: 09-Enero-2009
Description:	Valida que existan los datos m�nimos para la consulta de datos.
----------------------------------------------------------------------------}
function TRefinanciacionProcesosForm.Validaciones;
resourcestring
	MSG_OPCIONES	= 'No ha seleccionado ninguna opci�n';
    MSG_FECHA       = 'No ha ingresado la fecha';
begin
	Result := ValidateControls([cbOpciones], [cbOpciones.Text <> ''], Caption, [MSG_OPCIONES]);

	if Result and (cbOpciones.Text = CBX_STR_CHEQUES_DEPOSITO) then begin
    	Result := ValidateControls([deFechaDocumento], [deFechaDocumento.Date > 0], Caption, [MSG_FECHA]);
    end
    else if Result and (cbOpciones.Text = CBX_STR_CHEQUES_PAGAR_PROTESTAR) then begin

    end;
end;

{------------------------------------------------------------------------
        	MostrarDatos

Author: mbecerra
Date: 09-Enero-2009
Description:	Visualiza los datos de acuerdo a la opci�n seleccionada.
----------------------------------------------------------------------------}
procedure TRefinanciacionProcesosForm.MostrarDatos;
begin
	if cbOpciones.Text = CBX_STR_CHEQUES_DEPOSITO then begin
        MostrarChequesADepositar;
    end
    else if cbOpciones.Text = CBX_STR_CHEQUES_PAGAR_PROTESTAR then begin
        MostrarChequesDepositados;
    end
    else if cbOpciones.Text = CBX_STR_VALE_VISTA_COBRAR then begin
        MostrarValeVistasACobrar;
    end
    else if cbOpciones.Text = CBX_STR_PAGO_CUOTAS then begin
        MostrarCuotasAPagar;
    end;

end;

{------------------------------------------------------------------------
        	btnDepositarClick

Author: mbecerra
Date: 09-Enero-2009
Description:	Genera el comprobante de Dep�sito y cambia los estado de las cuotas de
				Refinanciaci�n a Depositado.
----------------------------------------------------------------------------}
procedure TRefinanciacionProcesosForm.btnCobrarVVClick(Sender: TObject);
    resourcestring
        TITULO_PROCESO = 'Proceso Cobrar Vales Vista';
        MSG_PROCESO = 'Va a proceder a Cobrar %d Vale(s) Vista. � Desea ejecutar el Proceso ?';
        MSG_PROCESO_EXITO = 'El proceso se complet� sin Errores.';
        MSG_SIN_REGISTROS = 'No ha seleccionado ning�n Vale Vista para Cobrar.';
        MSG_ERROR_COBROVV = 'Error Procesando Vale Vista Nro. %s, Refinanciaci�n %d, Detalle Comprobante %d';
        MSG_ERROR_INESPERADO = 'Se produjo un Error inesperado en el proceso. Error: %s';
    var
        ValeVistaACobrar, Retorno: Integer;
        Puntero: TBookmark;
        Ahora: TDateTime;
        Mensaje: String;

    function AceptarProceso: Boolean;
    begin
        Result := (ShowMsgBoxCN(TITULO_PROCESO, Format(MSG_PROCESO, [ValeVistaACobrar]), MB_ICONQUESTION, Self) = mrOk);
    end;
begin
{
    try
        with cdsValeVistasACobrar do begin
            try
                Puntero := GetBookmark;
                dsValeVistasACobrar.DataSet := Nil;

                First;
                ValeVistaACobrar := 0;

                while not Eof do begin
                    if cdsValeVistasACobrarCobrar.AsBoolean then Inc(ValeVistaACobrar);
                    Next;
                end;
            finally
                if BookmarkValid(Puntero) then GotoBookmark(Puntero);
                dsValeVistasACobrar.DataSet := cdsValeVistasACobrar;
            end;

            if (ValeVistaACobrar <> 0) then begin
                btnCobrarVV.Enabled := False;
                EmptyKeyQueue;
                Application.ProcessMessages;

                if AceptarProceso then try
                    Screen.Cursor := crHourGlass;
                    Application.ProcessMessages;

                    dsValeVistasACobrar.DataSet := Nil;
                    First;
                    Ahora := NowBase(DMConnections.BaseCAC);

                    DMConnections.BaseCAC.BeginTrans;

                    while not eof do begin
                        if cdsValeVistasACobrarCobrar.AsBoolean then
                            with spRefinanciacionCambiarEstadoFecha do begin
                                Parameters.ParamByName('@EsCambioFechaVencimiento').Value	:= 0;
                                Parameters.ParamByName('@IDRefinanciacion').Value			:= cdsValeVistasACobrarIDRefinanciacion.AsInteger;
                                Parameters.ParamByName('@IDDetallePagoComprobante').Value	:= cdsValeVistasACobrarIDDetallePagoComprobante.AsInteger;
                                Parameters.ParamByName('@EstadoActual').Value				:= REFINANCIACION_ESTADO_CUOTA_PENDIENTE;
                                Parameters.ParamByName('@EstadoNuevo').Value				:= REFINANCIACION_ESTADO_CUOTA_PAGADA;
                                Parameters.ParamByName('@FechaActual').Value				:= cdsValeVistasACobrarChequeFecha.AsDateTime;
                                Parameters.ParamByName('@FechaNueva').Value					:= Ahora;
                                Parameters.ParamByName('@Usuario').Value					:= UsuarioSistema;
                                Parameters.ParamByName('@MontoPago').Value					:= cdsValeVistasACobrarChequeMonto.AsInteger;
                                ExecProc;
                                Retorno := Parameters.ParamByName('@RETURN_VALUE').Value;
                                if Retorno < 0 then begin
                                    Mensaje := 'Error Desconocido: ';
                                    case Retorno of
                                        -1 : Mensaje := MSG_1;
                                        -2 : Mensaje := MSG_2;
                                        -3 : Mensaje := MSG_3;
                                        -4 : Mensaje := MSG_4;
                                        -5 : Mensaje := MSG_5;
                                        -6 : Mensaje := MSG_6;
                                    end;

                                    raise EGeneralExceptionCN.Create
                                                (TITULO_PROCESO,
                                                 Mensaje + CRLF +
                                                 Format
                                                    (MSG_ERROR_COBROVV,
                                                     [cdsValeVistasACobrarChequeNumero.AsString,
                                                      cdsValeVistasACobrarCodigoRefinanciacion.AsInteger,
                                                      cdsValeVistasACobrarIDDetallePagoComprobante.AsInteger]));
                                end;
                            end;
                        Next;
                    end;

                    if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.CommitTrans;
                    ShowMsgBoxCN(TITULO_PROCESO, MSG_PROCESO_EXITO, MB_ICONINFORMATION, Self);
                    
                finally
                    Close;
                    btnCobrarVV.Enabled := False;
                    dsValeVistasACobrar.DataSet := cdsValeVistasACobrar;

                    Screen.Cursor := crDefault;
                    Application.ProcessMessages;
                end
                else btnCobrarVV.Enabled := True;
            end
            else ShowMsgBoxCN(TITULO_PROCESO, MSG_SIN_REGISTROS, MB_ICONERROR, Self);
        end;
    except
        on E:Exception do begin
            if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.RollbackTrans;

            Screen.Cursor := crDefault;
            Application.ProcessMessages;

            if E is EGeneralExceptionCN then
                ShowMsgBoxCN(E, Self)
            else ShowMsgBoxCN(TITULO_PROCESO, Format(MSG_ERROR_INESPERADO, [e.Message]), MB_ICONERROR, Self);
        end;
    end;
    }
end;

procedure TRefinanciacionProcesosForm.btnDepositarClick(Sender: TObject);
begin
    GenerarDeposito;
end;

procedure TRefinanciacionProcesosForm.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TRefinanciacionProcesosForm.MostrarChequesDepositados;
begin
    try

        Screen.Cursor := crDefault;
        Application.ProcessMessages;

        with spObtenerRefinanciacionChequesDepositados do begin
            Parameters.Refresh;
            Parameters.ParamByName('@CodigoRefinanciacion').Value := iif(nedtNroRefinanciacion.Value > 0, nedtNroRefinanciacion.Value, NULL);
            Parameters.ParamByName('@IDComprobanteDeposito').Value := iif(nedtNroDeposito.Value > 0, nedtNroDeposito.Value, NULL);
        end;

        with cdsChequesDepositados do try
            if Active then Close;

            dsChequesDepositados.DataSet := Nil;
            Open;

            if chkPagar.Checked or chkProtestar.Checked then
                while not eof do begin
                    Edit;
                    cdsChequesDepositadosPagado.AsBoolean := chkPagar.Checked;
                    cdsChequesDepositadosProtestado.AsBoolean := chkProtestar.Checked;
                    Post;
                    Next;
                end;
        finally
            First;
            dsChequesDepositados.DataSet := cdsChequesDepositados;
            btnPagarProtestar.Enabled    := FbtnPagarProtestarPermisos and (cdsChequesDepositados.RecordCount > 0)
        end;
    finally
        Screen.Cursor := crDefault;
    end;
end;

procedure TRefinanciacionProcesosForm.MostrarChequesADepositar;
begin
    try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        with spObtenerRefinanciacionChequesADepositar do begin
            Parameters.Refresh;
            Parameters.ParamByName('@FechaHasta').Value := deFechaDocumento.Date;
            Parameters.ParamByName('@Pendiente').Value := iif(chkPendiente.Checked, 1, 0);
            Parameters.ParamByName('@Protestado').Value := iif(chkProtestado.Checked, 1, 0);
            Parameters.ParamByName('@EnCobranza').Value := iif(chkEnCobranza.Checked, 1, 0);
            Parameters.ParamByName('@MoraPactada').Value := iif(chkMoraPactada.Checked, 1, 0);
        end;

        with cdsChequesADepositar do try
            if Active then Close;

            dsChequesADepositar.DataSet := Nil;
            Open;

            if chkDepositar.Checked then
                while not eof do begin
                    if cdsChequesADepositarImporte.AsLargeInt = cdsChequesADepositarSaldo.AsLargeInt then begin
                        Edit;
                        cdsChequesADepositarDepositar.AsBoolean := True;
                        Post;
                    end;

                    Next;
                end;
        finally
            First;
            dsChequesADepositar.DataSet := cdsChequesADepositar;
            btnDepositar.Enabled := FbtnDepositarPermisos and (cdsChequesADepositar.RecordCount > 0)
        end;
    finally
        Screen.Cursor := crDefault;
    end;
end;

// SS_637_20100923
procedure TRefinanciacionProcesosForm.OrdenarColumna(sender: TObject);
begin
    if not TCustomDBListEx(TDBListExColumn(sender).Columns.List).DataSource.DataSet.Active then
        exit
    else
        OrdenarGridPorColumna(TDBListExColumn(sender).Columns.List,
                                TDBListExColumn(sender),
                                TDBListExColumn(sender).FieldName,
                                True);
end;
procedure TRefinanciacionProcesosForm.peRutButtonClick(Sender: TObject);
    var
        BuscaClientes: TFormBuscaClientes;
begin
    try
        BuscaClientes := TFormBuscaClientes.Create(Self);
        if BuscaClientes.Inicializa(FUltimaBusqueda)then begin
            if BuscaClientes.ShowModal = mrOk then begin
                FUltimaBusqueda := BuscaClientes.UltimaBusqueda;
                peRut.Text      := BuscaClientes.Persona.NumeroDocumento;
                FCodigoCliente  := BuscaClientes.Persona.CodigoPersona;
                MostrarDatosCliente(BuscaClientes.Persona.CodigoPersona, BuscaClientes.Persona.Personeria);
            end;
        end;
    finally
        if Assigned(BuscaClientes) then FreeAndNil(BuscaClientes);
    end;
end;

procedure TRefinanciacionProcesosForm.peRutChange(Sender: TObject);
begin
    FCodigoCliente        := 0;
    lblNombreCli.Caption  := '';
    lblPersoneria.Caption := '';
end;

procedure TRefinanciacionProcesosForm.peRutKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    resourcestring
        rs_BUSQUEDA_TITULO = 'B�squeda Persona';
        rs_BUSQUEDA_NO_ENCONTRADA = 'El Rut introducido NO Existe o NO es V�lido.';
    var
        FPersona: TDatosPersonales;
begin
    case Key of
        VK_RETURN: begin
            FPersona := ObtenerDatosPersonales(DMConnections.BaseCAC, 'RUT', peRut.Text);

            if FPersona.CodigoPersona <> -1 then begin
                MostrarDatosCliente(FPersona.CodigoPersona, FPersona.Personeria);
                FCodigoCliente := FPersona.CodigoPersona;
            end
            else begin
                FCodigoCliente := 0;
                ShowMsgBoxCN(rs_BUSQUEDA_TITULO, rs_BUSQUEDA_NO_ENCONTRADA, MB_ICONWARNING, Self);
            end
        end;
    end;
end;

procedure TRefinanciacionProcesosForm.peRutKeyPress(Sender: TObject; var Key: Char);
begin
end;

// Fin Rev. SS_637_20100923


procedure TRefinanciacionProcesosForm.btnPagarCuotaClick(Sender: TObject);
    resourcestring
        TITULO_PROCESO         = 'Proceso Pago Cuotas';
        MSG_PROCESO            = 'Va a proceder a efectuar el pago de %d Cuotas. � Desea ejecutar el Proceso ?';
        MSG_PROCESO_EXITO      = 'El proceso se complet� sin Errores.';
        MSG_SIN_REGISTROS      = 'No ha seleccionado ninguna Cuota para Cobrar.';
        MSG_ERROR_COBRO_CUOTA  = 'Error Procesando la Cuota de Forma de Pago %s, Refinanciaci�n %d, Cuota %d';
        MSG_ERROR_INESPERADO   = 'Se produjo un Error inesperado en el proceso. Error: %s';

    var
        CuotasAPagar, Retorno: Integer;
        Puntero: TBookmark;
        Ahora: TDateTime;
        Mensaje: String;

    function AceptarProceso: Boolean;
    begin
        Result := (ShowMsgBoxCN(TITULO_PROCESO, Format(MSG_PROCESO, [CuotasAPagar]), MB_ICONQUESTION, Self) = mrOk);
    end;
begin
    {
    if GNumeroTurno > 0 then try
        with cdsCuotasAPagar do begin
            try
                Puntero := GetBookmark;
                dsCuotasAPagar.DataSet := Nil;

                First;
                CuotasAPagar := 0;

                while not Eof do begin
                    if cdsCuotasAPagarPagar.AsBoolean then Inc(CuotasAPagar);
                    Next;
                end;
            finally
                if BookmarkValid(Puntero) then GotoBookmark(Puntero);
                dsCuotasAPagar.DataSet := cdsCuotasAPagar;
            end;

            if (CuotasAPagar <> 0) then begin
                btnPagarCuota.Enabled := False;
                EmptyKeyQueue;
                Application.ProcessMessages;

                if AceptarProceso then try
                    Screen.Cursor := crHourGlass;
                    Application.ProcessMessages;

                    dsCuotasAPagar.DataSet := Nil;
                    First;
                    Ahora := NowBase(DMConnections.BaseCAC);

                    QueryExecute(DMConnections.BaseCAC, 'BEGIN TRANSACTION PagarCuota');

                    while not eof do begin
                        if cdsCuotasAPagarPagar.AsBoolean then
                            with spActualizarRefinanciacionCuotasEstados, spActualizarRefinanciacionCuotasEstados.Parameters do begin
                                Parameters.Refresh;
                                ParamByName('@CodigoRefinanciacion').Value	:= cdsCuotasAPagarCodigoRefinanciacion.AsInteger;
                                ParamByName('@CodigoCuota').Value			:= cdsCuotasAPagarCodigoCuota.AsInteger;
                                ParamByName('@CodigoEstadoCuota').Value	    := REFINANCIACION_ESTADO_CUOTA_PAGADA;
                                ParamByName('@Usuario').Value				:= UsuarioSistema;

                                ExecProc;

                                Retorno := Parameters.ParamByName('@RETURN_VALUE').Value;
                                if Retorno < 0 then begin
                                    raise EErrorExceptionCN.Create
                                                (TITULO_PROCESO,
                                                 Format
                                                    (MSG_ERROR_COBRO_CUOTA,
                                                     [Trim(cdsCuotasAPagarDescripcionFormaPago.AsString),
                                                      cdsCuotasAPagarCodigoRefinanciacion.AsInteger,
                                                      cdsCuotasAPagarCodigoCuota.AsInteger]));
                                end;
                            end;
                        Next;
                    end;

                    QueryExecute(DMConnections.BaseCAC, 'IF @@TRANCOUNT > 0 BEGIN COMMIT TRANSACTION PagarCuota END');
                    ShowMsgBoxCN(TITULO_PROCESO, MSG_PROCESO_EXITO, MB_ICONINFORMATION, Self);
                    
                finally
                    Close;
                    btnPagarCuota.Enabled := False;
                    dsCuotasAPagar.DataSet := cdsCuotasAPagar;

                    Screen.Cursor := crDefault;
                    Application.ProcessMessages;
                end
                else btnPagarCuota.Enabled := True;
            end
            else ShowMsgBoxCN(TITULO_PROCESO, MSG_SIN_REGISTROS, MB_ICONERROR, Self);
        end;
    except
        on E:Exception do begin
            QueryExecute(DMConnections.BaseCAC, 'IF @@TRANCOUNT > 0 BEGIN ROLLBACK TRANSACTION PagarCuota END');

            Screen.Cursor := crDefault;
            Application.ProcessMessages;

            if E is EGeneralExceptionCN then
                ShowMsgBoxCN(E, Self)
            else ShowMsgBoxCN(TITULO_PROCESO, Format(MSG_ERROR_INESPERADO, [e.Message]), MB_ICONERROR, Self);
        end;
    end
    else ShowMsgBoxCN(RS_TITULO_AVISO_TURNO, RS_MENSAJE_AVISO_TURNO, MB_ICONWARNING, Self);
    }
end;

procedure TRefinanciacionProcesosForm.btnPagarProtestarClick(Sender: TObject);
    resourcestring
        TITULO_PROCESO = 'Proceso Pagar / Protestar Cheques Depositados';
        MSG_PROCESO = 'Va a proceder a Pagar %d Cheques y a Protestar %d. � Desea ejecutar el Proceso ?';
        MSG_PROCESO_EXITO = 'El proceso se complet� sin Errores.';
        MSG_SIN_REGISTROS = 'No ha seleccionado ning�n Cheque o Pagar� para Pagar o Protestar.';
        MSG_ERROR_DEPOSITO = 'Error Procesando Cheque del Dep�sito %d, Refinanciaci�n %d, Cuota %d';
        MSG_ERROR_INESPERADO = 'Se produjo un Error inesperado en el proceso. Error: %s';

        RS_ERROR_EJECUCION_TITULO  = 'Error de Ejecuci�n';
        RS_ERROR_EJECUCION_MENSAJE = 'Error en la ejecuci�n del procedimiento %s. C�digo de Error: %d.';
    var
        ChequesAPagar, ChequesAProtestar, Retorno: Integer;
        Puntero: TBookmark;
        Mensaje: String;

    function AceptarProceso: Boolean;
    begin
        Result := (ShowMsgBoxCN(TITULO_PROCESO, Format(MSG_PROCESO, [ChequesAPagar, ChequesAProtestar]), MB_ICONQUESTION, Self) = mrOk);
    end;

begin
    if GNumeroTurno > 0 then begin
        if ValidarFechaPagoDeposito then try
            with cdsChequesDepositados do begin
                try
                    Puntero := GetBookmark;
                    dsChequesDepositados.DataSet := Nil;

                    First;
                    ChequesAPagar := 0; ChequesAProtestar := 0;

                    while not Eof do begin
                        if cdsChequesDepositadosPagado.AsBoolean then Inc(ChequesAPagar);
                        if cdsChequesDepositadosProtestado.AsBoolean then Inc(ChequesAProtestar);
                        Next;
                    end;
                finally
                    if Assigned(Puntero) then begin
                        if BookmarkValid(Puntero) then GotoBookmark(Puntero);
                        FreeBookmark(Puntero);
                    end;
                    dsChequesDepositados.DataSet := cdsChequesDepositados;
                end;

                if (ChequesAPagar <> 0) or (ChequesAProtestar <> 0) then begin
                    btnPagarProtestar.Enabled := False;
                    EmptyKeyQueue;
                    Application.ProcessMessages;

                    if AceptarProceso then try
                        Screen.Cursor := crHourGlass;
                        Application.ProcessMessages;

                        dsChequesDepositados.DataSet := Nil;
                        First;

                        QueryExecute(DMConnections.BaseCAC, 'BEGIN TRANSACTION PagarProtestarCheques');

                        while not eof do begin
                            if cdsChequesDepositadosPagado.AsBoolean or cdsChequesDepositadosProtestado.AsBoolean then begin
                                with spActualizarRefinanciacionComprobanteDepositoDetalle do begin
                                    Parameters.Refresh;
                                    Parameters.ParamByName('@IDComprobanteDeposito').Value := cdsChequesDepositadosIDComprobanteDeposito.AsInteger;
                                    Parameters.ParamByName('@CodigoRefinanciacion').Value  := cdsChequesDepositadosCodigoRefinanciacion.AsInteger;
                                    Parameters.ParamByName('@CodigoCuota').Value           := cdsChequesDepositadosCodigoCuota.AsInteger;

                                    ExecProc;

                                    Retorno := Parameters.ParamByName('@RETURN_VALUE').Value;
                                    if Retorno < 0 then begin
                                        raise EGeneralExceptionCN.Create
                                                    (TITULO_PROCESO,
                                                     Format
                                                        (MSG_ERROR_DEPOSITO,
                                                         [cdsChequesDepositadosIDComprobanteDeposito.AsInteger,
                                                          cdsChequesDepositadosCodigoRefinanciacion.AsInteger,
                                                          cdsChequesDepositadosCodigoCuota.AsInteger]));
                                    end;
                                end;

                                if cdsChequesDepositadosPagado.AsBoolean then begin
                                    with spActualizarRefinanciacionCuotaPago do begin
                                        if Active then Close;
                                        Parameters.Refresh;

                                        with Parameters do begin
                                            ParamByName('@CodigoRefinanciacion').Value   := cdsChequesDepositadosCodigoRefinanciacion.AsInteger;
                                            ParamByName('@CodigoCuota').Value            := cdsChequesDepositadosCodigoCuota.AsInteger;
                                            ParamByName('@Importe').Value                := cdsChequesDepositadosImporte.AsLargeInt;
                                            ParamByName('@Anticipado').Value             := 0;
                                            ParamByName('@CodigoFormaPago').Value        := cdsChequesDepositadosCodigoFormaPago.AsInteger;
                                            ParamByName('@TitularNombre').Value          := iif(cdsChequesDepositadosTitularNombre.AsString = EmptyStr, Null, cdsChequesDepositadosTitularNombre.AsString);
                                            ParamByName('@TitularTipoDocumento').Value   := iif(cdsChequesDepositadosTitularTipoDocumento.AsString = EmptyStr, Null, cdsChequesDepositadosTitularTipoDocumento.AsString);
                                            ParamByName('@TitularNumeroDocumento').Value := iif(cdsChequesDepositadosTitularNumeroDocumento.AsString = EmptyStr, Null, cdsChequesDepositadosTitularNumeroDocumento.AsString);
                                            ParamByName('@DocumentoNumero').Value        := iif(cdsChequesDepositadosDocumentoNumero.AsString = EmptyStr, Null, cdsChequesDepositadosDocumentoNumero.AsString);
                                            ParamByName('@CodigoBanco').Value            := iif(cdsChequesDepositadosCodigoBanco.AsInteger = 0, Null, cdsChequesDepositadosCodigoBanco.AsInteger);
                                            ParamByName('@CodigoTarjeta').Value          := iif(cdsChequesDepositadosCodigoTarjeta.AsInteger = 0, Null, cdsChequesDepositadosCodigoTarjeta.AsInteger);
                                            ParamByName('@TarjetaCuotas').Value          := iif(cdsChequesDepositadosTarjetaCuotas.AsInteger = 0, Null, cdsChequesDepositadosTarjetaCuotas.AsInteger);
                                            ParamByName('@Usuario').Value                := UsuarioSistema;
                                            ParamByName('@NumeroTurno').Value            := GNumeroTurno;
                                            ParamByName('@FechaPago').Value              := Trunc(deFechaPagoDeposito.Date);    // SS_637_PDO_20110503
                                            ParamByName('@NumeroRecibo').Value           := Null;
                                        end;

                                        ExecProc;

                                        if Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
                                            raise EErrorExceptionCN.Create(
                                                RS_ERROR_EJECUCION_TITULO,
                                                Format(RS_ERROR_EJECUCION_MENSAJE,
                                                       [ProcedureName,
                                                        Parameters.ParamByName('@RETURN_VALUE').Value]));
                                        end;
                                    end;
                                end;

                                with spActualizarRefinanciacionCuotasEstados do begin
                                    Parameters.Refresh;
                                    Parameters.ParamByName('@CodigoRefinanciacion').Value	:= cdsChequesDepositadosCodigoRefinanciacion.AsInteger;
                                    Parameters.ParamByName('@CodigoCuota').Value			:= cdsChequesDepositadosCodigoCuota.AsInteger;
                                    Parameters.ParamByName('@CodigoEstadoCuota').Value	    := iif(cdsChequesDepositadosPagado.AsBoolean, REFINANCIACION_ESTADO_CUOTA_PAGADA, REFINANCIACION_ESTADO_CUOTA_PROTESTADO);
                                    Parameters.ParamByName('@Usuario').Value				:= UsuarioSistema;

                                    ExecProc;

                                    Retorno := Parameters.ParamByName('@RETURN_VALUE').Value;
                                    if Retorno < 0 then begin
                                        raise EGeneralExceptionCN.Create
                                                    (TITULO_PROCESO,
                                                     Format
                                                        (MSG_ERROR_DEPOSITO,
                                                         [cdsChequesDepositadosIDComprobanteDeposito.AsInteger,
                                                          cdsChequesDepositadosCodigoRefinanciacion.AsInteger,
                                                          cdsChequesDepositadosCodigoCuota.AsInteger]));
                                    end;
                                end;
                            end;

                            Next;
                        end;

                        QueryExecute(DMConnections.BaseCAC, 'IF @@TRANCOUNT > 0 BEGIN COMMIT TRANSACTION PagarProtestarCheques END');

                        ShowMsgBoxCN(TITULO_PROCESO, MSG_PROCESO_EXITO, MB_ICONINFORMATION, Self);

                    finally
                        Close;
                        btnPagarProtestar.Enabled := False;
                        dsChequesDepositados.DataSet := cdsChequesDepositados;

                        Screen.Cursor := crDefault;
                        Application.ProcessMessages;
                    end
                    else btnPagarProtestar.Enabled := True;
                end
                else ShowMsgBoxCN(TITULO_PROCESO, MSG_SIN_REGISTROS, MB_ICONERROR, Self);
            end;
        except
            on E:Exception do begin
                QueryExecute(DMConnections.BaseCAC, 'IF @@TRANCOUNT > 0 BEGIN ROLLBACK TRANSACTION PagarProtestarCheques END');

                Screen.Cursor := crDefault;
                Application.ProcessMessages;

                if E is EGeneralExceptionCN then
                    ShowMsgBoxCN(E, Self)
                else ShowMsgBoxCN(TITULO_PROCESO, Format(MSG_ERROR_INESPERADO, [e.Message]), MB_ICONERROR, Self);
            end;
        end;
    end
    else ShowMsgBoxCN(RS_TITULO_AVISO_TURNO, RS_MENSAJE_AVISO_TURNO, MB_ICONWARNING, Self);
end;

procedure TRefinanciacionProcesosForm.GenerarDeposito;
    resourcestring
        TITULO_PROCESO = 'Proceso Generar Deposito';
        MSG_PROCESO = 'Va a proceder a Generara el Dep�sito de %d Cheques. � Desea ejecutar el Proceso ?';
        MSG_PROCESO_EXITO = 'El proceso se complet� sin Errores.';
        MSG_SIN_REGISTROS = 'No ha seleccionado ning�n Cheque para Depositar.';
        MSG_ERROR_DEPOSITO = 'Error Procesando el Dep�sito para el Cheque/Pagar� Nro. %s, Banco %s, Refinanciaci�n %d, Cuota %d';
        MSG_ERROR_CAMBIO_ESTADO = ' Se Produjo un Error al Cambiar el Estado para el Cheque/Pagar� Nro. %s, Banco %s, Refinanciaci�n %d, Cuota %d';
        MSG_ERROR_INESPERADO = 'Se produjo un Error inesperado en el proceso. Error: %s';

        MSG_NOPUDO  = 'No se pudo generar el comprobante de Dep�sito';
    var
        ChequesADepositar, Retorno, Comprobante: Integer;
        Puntero: TBookmark;
        Mensaje: String;
        ReporteDepositos: TRefinanciacionChequesDepositadosRptForm;
        IDBloqueo: Integer;
        ListaIDBloqueos: TStringList;
        ResultadoBloqueo,
        Veces: Integer;

    function AceptarProceso: Boolean;
    begin
        Result := (ShowMsgBoxCN(TITULO_PROCESO, Format(MSG_PROCESO, [ChequesADepositar]), MB_ICONQUESTION, Self) = mrOk);
    end;
begin
    try
        ListaIDBloqueos := TStringList.Create;
        try
            with cdsChequesADepositar do begin
                try
                    CambiarEstadoCursor(CURSOR_RELOJ, Self, FComponentes);
                    Puntero := GetBookmark;
                    dsChequesADepositar.DataSet := Nil;

                    First;
                    ChequesADepositar := 0;

                    while not Eof do begin
                        if cdsChequesADepositarDepositar.AsBoolean then begin

                            IDBloqueo := 0;
                            ResultadoBloqueo :=
                                ObtenerBloqueo
                                    (
                                        DMConnections.BaseCAC,
                                        'RefDepositarCheques',
                                        //cdsChequesADepositarCodigoRefinanciacion.AsString,  //SS_1121_MCA_20130809
                                        cdsChequesADepositarCodigoRefinanciacion.AsString + cdsChequesADepositarDocumentoNumero.AsString, //SS_1121_MCA_20130809
                                        UsuarioSistema,
                                        IDBloqueo
                                    );

                            if ResultadoBloqueo = 0 then begin
                                ListaIDBloqueos.Add(IntToStr(IDBloqueo));
                                Inc(ChequesADepositar);
                            end
                            else begin
                                cdsChequesADepositar.Edit;
                                cdsChequesADepositarDepositar.AsBoolean := False;
                                cdsChequesADepositar.Post;
                            end;
                        end;
                        Next;
                    end;
                finally
                    if Assigned(Puntero) then begin
                        if BookmarkValid(Puntero) then GotoBookmark(Puntero);
                        FreeBookmark(Puntero);
                    end;
                    dsChequesADepositar.DataSet := cdsChequesADepositar;
                    CambiarEstadoCursor(CURSOR_DEFECTO, Self, FComponentes);
                end;

                if (ChequesADepositar <> 0) then begin
                    if AceptarProceso then try
                        try
                            CambiarEstadoCursor(CURSOR_RELOJ, Self, FComponentes);

                            dsChequesADepositar.DataSet := Nil;
                            First;

                            QueryExecute(DMConnections.BaseCAC, 'BEGIN TRANSACTION GenerarDeposito');

                            with spCrearRefinanciacionComprobanteDeposito do begin
                                if Active then Close;
                                Parameters.Refresh;
                                Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
                                ExecProc;
                                Comprobante := Parameters.ParamByName('@RETURN_VALUE').Value;
                                if Comprobante < 0 then
                                    raise EGeneralExceptionCN.Create(TITULO_PROCESO, MSG_NOPUDO);
                            end;

                            while not eof do begin
                                if cdsChequesADepositarDepositar.AsBoolean then begin

                                    //Insertar el detalle del comprobante de dep�sito
                                    with spCrearRefinanciacionComprobanteDepositoDetalle, spCrearRefinanciacionComprobanteDepositoDetalle.Parameters do begin
                                        if Active then Close;
                                        Refresh;
                                        ParamByName('@IDComprobanteDeposito').Value	:= Comprobante;
                                        ParamByName('@CodigoRefinanciacion').Value  := cdsChequesADepositarCodigoRefinanciacion.AsInteger;
                                        ParamByName('@CodigoCuota').Value	        := cdsChequesADepositarCodigoCuota.AsInteger;
                                        ExecProc;
                                        Retorno := ParamByName('@RETURN_VALUE').Value;
                                        if Retorno < 0 then
                                            raise EGeneralExceptionCN.Create
                                                        (
                                                            TITULO_PROCESO,
                                                            Format
                                                                (
                                                                    MSG_ERROR_DEPOSITO,
                                                                    [cdsChequesADepositarDocumentoNumero.AsString,
                                                                     cdsChequesADepositarDescripcionBanco.AsString,
                                                                     cdsChequesADepositarCodigoRefinanciacion.AsInteger,
                                                                     cdsChequesADepositarCodigoCuota.AsInteger]));
                                    end;

                                    //Cambiar el estado de cada Cuota de refinanciaci�n seleccionada
                                    with spActualizarRefinanciacionCuotasEstados, spActualizarRefinanciacionCuotasEstados.Parameters do begin
                                        if Active then Close;
                                        Refresh;
                                        ParamByName('@CodigoRefinanciacion').Value	:= cdsChequesADepositarCodigoRefinanciacion.AsInteger;
                                        ParamByName('@CodigoCuota').Value			:= cdsChequesADepositarCodigoCuota.AsInteger;
                                        ParamByName('@CodigoEstadoCuota').Value	    := REFINANCIACION_ESTADO_CUOTA_DEPOSITADO; //EstadoDepositado;
                                        ParamByName('@Usuario').Value				:= UsuarioSistema;
                                        ExecProc;

                                        Retorno := ParamByName('@RETURN_VALUE').Value;
                                        if Retorno < 0 then
                                            raise EGeneralExceptionCN.Create
                                                        (
                                                            TITULO_PROCESO,
                                                            Format
                                                                (
                                                                    MSG_ERROR_DEPOSITO,
                                                                    [cdsChequesADepositarDocumentoNumero.AsString,
                                                                     cdsChequesADepositarDescripcionBanco.AsString,
                                                                     cdsChequesADepositarCodigoRefinanciacion.AsInteger,
                                                                     cdsChequesADepositarCodigoCuota.AsInteger]));
                                    end;
                                end;

                                Next;
                            end;

                            QueryExecute(DMConnections.BaseCAC, 'IF @@TRANCOUNT > 0 BEGIN COMMIT TRANSACTION GenerarDeposito END');
                            ShowMsgBoxCN(TITULO_PROCESO, MSG_PROCESO_EXITO, MB_ICONINFORMATION, Self);

                            try
                                ReporteDepositos := TRefinanciacionChequesDepositadosRptForm.Create(Self);
                                ReporteDepositos.Inicializar(Comprobante);
                                ReporteDepositos.RBInterface1.Execute;
                            finally
                                if Assigned(ReporteDepositos) then FreeAndNil(ReporteDepositos);
                            end;
                        except
                            on E:Exception do begin
                                QueryExecute(DMConnections.BaseCAC, 'IF @@TRANCOUNT > 0 BEGIN ROLLBACK TRANSACTION GenerarDeposito END');

                                Screen.Cursor := crDefault;
                                Application.ProcessMessages;

                                if E is EGeneralExceptionCN then
                                    ShowMsgBoxCN(E, Self)
                                else ShowMsgBoxCN(TITULO_PROCESO, Format(MSG_ERROR_INESPERADO, [e.Message]), MB_ICONERROR, Self);
                            end;
                        end;
                    finally
                        Close;
                        dsChequesADepositar.DataSet := cdsChequesADepositar;

                        CambiarEstadoCursor(CURSOR_DEFECTO, Self, FComponentes);
                    end;
                end
                else ShowMsgBoxCN(TITULO_PROCESO, MSG_SIN_REGISTROS, MB_ICONERROR, Self);
            end;
        except
            on E:Exception do begin
                if E is EGeneralExceptionCN then
                    ShowMsgBoxCN(E, Self)
                else ShowMsgBoxCN(TITULO_PROCESO, Format(MSG_ERROR_INESPERADO, [e.Message]), MB_ICONERROR, Self);
            end;
        end;
    finally
        if Assigned(ListaIDBloqueos) then begin
            if ListaIDBloqueos.Count > 0 then try
                CambiarEstadoCursor(CURSOR_RELOJ);
                for Veces := 0 to ListaIDBloqueos.Count - 1 do begin
                    try
                        if IDBloqueo <> 0 then begin
                            EliminarBloqueo(DMConnections.BaseCAC, StrToInt(ListaIDBloqueos.Strings[Veces]));
                        end;
                    except
                        on e: Exception do begin
                            ShowMsgBoxCN(e, Self);
                        end;
                    end;
                end;
            finally
                CambiarEstadoCursor(CURSOR_DEFECTO);
            end;
            
            FreeAndNil(ListaIDBloqueos);
        end;
    end;
end;

procedure TRefinanciacionProcesosForm.MostrarValeVistasACobrar;
begin
    {
    try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        with spRefinanciacionObtenerValeVistasACobrar do begin
            Parameters.Refresh;
            Parameters.ParamByName('@FechaHasta').Value := deFechaValeVistas.Date;
        end;

        with cdsValeVistasACobrar do try
            if Active then Close;

            dsValeVistasACobrar.DataSet := Nil;
            Open;

            if chkCobrar.Checked then
                while not eof do begin
                    Edit;
                    cdsValeVistasACobrarCobrar.AsBoolean := True;
                    Post;
                    Next;
                end;
        finally
            First;
            dsValeVistasACobrar.DataSet := cdsValeVistasACobrar;
            btnCobrarVV.Enabled := (cdsValeVistasACobrar.RecordCount > 0)
        end;
    finally
        Screen.Cursor := crDefault;
    end;
    }
end;

procedure TRefinanciacionProcesosForm.MostrarCuotasAPagar;
begin
    {
    try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        if cdsCuotasAPagar.Active then cdsCuotasAPagar.Close;

        with spObtenerRefinanciacionCuotasAPagar do begin
            Parameters.Refresh;
            Parameters.ParamByName('@CodigoPersona').Value        := iif(FCodigoCliente = 0, NUll, FCodigoCliente);
            Parameters.ParamByName('@RUTPersona').Value           := iif(Trim(peRut.Text) <> '', peRut.Text, NULL);
            Parameters.ParamByName('@CodigoRefinanciacion').Value := iif(nedtRefinanciacionPagoCuotas.Value <> 0, nedtRefinanciacionPagoCuotas.Value, NULL);
        end;

        with cdsCuotasAPagar do begin
            Open;
            dsCuotasAPagar.DataSet := Nil;
            First;

            while not Eof do begin
                Edit;
                cdsCuotasAPagarPagar.AsBoolean := chkPagarCuotas.Checked;
                Post;
                Next;
            end;
        end;

        btnPagarCuota.Enabled := cdsCuotasAPagar.RecordCount > 0;
    finally
        cdsCuotasAPagar.First;
        dsCuotasAPagar.DataSet := cdsCuotasAPagar;

        Screen.Cursor := crDefault;
    end;
    }
end;

procedure TRefinanciacionProcesosForm.MostrarDatosCliente(CodigoCliente: Integer; Personeria: string);
begin
    lblNombreCli.Caption := QueryGetValue(DMConnections.BaseCAC, Format(SQLObtenerNombrePersona, [CodigoCliente]));

    if Personeria = PERSONERIA_FISICA then begin
        lblPersoneria.Caption := ' - (Persona Natural)';
    end
    else begin
        if Personeria = PERSONERIA_JURIDICA then lblPersoneria.Caption := ' - (Persona Jur�dica)';
        if Personeria = PERSONERIA_AMBAS then lblPersoneria.Caption := ' - (Persona Natural y Jur�dica)';
    end;

    lblPersoneria.Left := lblNombreCli.Left + lblNombreCli.Width;
end;

procedure TRefinanciacionProcesosForm.ActualizaChequesDepositadosAccion;
    var
        Puntero: TBookmark;
begin
    if cdsChequesDepositados.Active and (cdsChequesDepositados.RecordCount > 0) then
        with cdsChequesDepositados do try
            Screen.Cursor := crDefault;
            Application.ProcessMessages;

            dsChequesDepositados.DataSet := Nil;
            Puntero := cdsChequesDepositados.GetBookmark;
            First;

            while not eof do begin
                Edit;
                cdsChequesDepositadosPagado.AsBoolean     := chkPagar.Checked;
                cdsChequesDepositadosProtestado.AsBoolean := chkProtestar.Checked;
                Post;
                Next;
            end;
        finally
            if Assigned(Puntero) then begin
                if cdsChequesDepositados.BookmarkValid(Puntero) then begin
                    cdsChequesDepositados.GotoBookmark(Puntero);
                end;
                cdsChequesDepositados.FreeBookmark(Puntero);
            end;
            
            dsChequesDepositados.DataSet := cdsChequesDepositados;
            Screen.Cursor := crDefault;
        end;
end;

function TRefinanciacionProcesosForm.ValidarFechaPagoDeposito: Boolean; // SS_637_PDO_20110503
    var
        lAhora: TDate;
begin
    try
        Result := False;
        lAhora := Trunc(NowBaseCN(DMConnections.BaseCAC));

        if (deFechaPagoDeposito.Date > lAhora) or (deFechaPagoDeposito.Date = NullDate) then begin
            ShowMsgBoxCN('Validaci�n Fecha Pago', 'La Fecha de Pago NO es Correcta.', MB_ICONERROR, Self);
        end
        else Result := True;
    except
        on e: Exception do begin
            ShowMsgBoxCN(e, Self);
        end;
    end;
end;

end.


