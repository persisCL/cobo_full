object RefinanciacionProcesosForm: TRefinanciacionProcesosForm
  Left = 0
  Top = 0
  Caption = 'RefinanciacionProcesosForm'
  ClientHeight = 531
  ClientWidth = 992
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 992
    Height = 49
    Align = alTop
    ParentBackground = False
    TabOrder = 0
    DesignSize = (
      992
      49)
    object Label1: TLabel
      Left = 16
      Top = 16
      Width = 48
      Height = 13
      Caption = 'Opciones:'
    end
    object btnBuscar: TButton
      Left = 400
      Top = 11
      Width = 75
      Height = 25
      Caption = 'Buscar'
      TabOrder = 0
      OnClick = btnBuscarClick
    end
    object cbOpciones: TComboBox
      Left = 70
      Top = 13
      Width = 315
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 1
      OnChange = cbOpcionesChange
    end
    object btnSalir: TButton
      Left = 906
      Top = 11
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Salir'
      TabOrder = 2
      OnClick = btnSalirClick
    end
  end
  object pcOpciones: TPageControl
    Left = 0
    Top = 49
    Width = 992
    Height = 482
    ActivePage = tbsDepositado
    Align = alClient
    TabOrder = 1
    object tbsVacio: TTabSheet
      Caption = 'tbsVacio'
    end
    object tbsDeposito: TTabSheet
      Caption = 'tbsDeposito'
      ImageIndex = 1
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 984
        Height = 89
        Align = alTop
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        DesignSize = (
          984
          89)
        object GroupBox1: TGroupBox
          Left = 2
          Top = 2
          Width = 975
          Height = 81
          Anchors = [akLeft, akTop, akRight, akBottom]
          Caption = '  Par'#225'metros B'#250'squeda  '
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentBackground = False
          ParentColor = False
          ParentFont = False
          TabOrder = 0
          object Label3: TLabel
            Left = 24
            Top = 19
            Width = 140
            Height = 13
            Caption = 'Fecha Documento Hasta:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label2: TLabel
            Left = 78
            Top = 41
            Width = 86
            Height = 13
            Caption = 'Incluir Estados:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object deFechaDocumento: TDateEdit
            Left = 171
            Top = 16
            Width = 100
            Height = 21
            AutoSelect = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            Date = -693594.000000000000000000
          end
          object chkPendiente: TCheckBox
            Left = 171
            Top = 40
            Width = 77
            Height = 17
            Caption = 'Pendiente'
            Checked = True
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            State = cbChecked
            TabOrder = 1
          end
          object chkProtestado: TCheckBox
            Left = 254
            Top = 40
            Width = 79
            Height = 17
            Caption = 'Protestado'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
          end
          object chkEnCobranza: TCheckBox
            Left = 347
            Top = 41
            Width = 97
            Height = 17
            Caption = 'En Cobranza'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
          end
          object chkMoraPactada: TCheckBox
            Left = 436
            Top = 41
            Width = 97
            Height = 17
            Caption = 'Mora Pactada'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 4
          end
          object chkDepositar: TCheckBox
            Left = 171
            Top = 59
            Width = 162
            Height = 17
            Caption = 'Marcar Todos a Depositar'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 5
            OnClick = chkDepositarClick
          end
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 406
        Width = 984
        Height = 48
        Align = alBottom
        ParentBackground = False
        TabOrder = 1
        DesignSize = (
          984
          48)
        object btnDepositar: TButton
          Left = 864
          Top = 13
          Width = 113
          Height = 25
          Anchors = [akRight, akBottom]
          Caption = 'Generar Dep'#243'sito'
          TabOrder = 0
          OnClick = btnDepositarClick
        end
      end
      object DBListEx1: TDBListEx
        Left = 0
        Top = 89
        Width = 984
        Height = 317
        Align = alClient
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 70
            Header.Caption = 'Depositar'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = True
            FieldName = 'Depositar'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 100
            Header.Caption = 'Estado'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            OnHeaderClick = OrdenarColumna
            FieldName = 'DescripcionEstadoCuota'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 80
            Header.Caption = 'Rut'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            OnHeaderClick = OrdenarColumna
            FieldName = 'TitularNumeroDocumento'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'Nombre'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'TitularNombre'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 95
            Header.Caption = 'Refinanciaci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taRightJustify
            IsLink = False
            OnHeaderClick = OrdenarColumna
            FieldName = 'CodigoRefinanciacion'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 120
            Header.Caption = 'Convenio'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            OnHeaderClick = OrdenarColumna
            FieldName = 'NumeroConvenio'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 95
            Header.Caption = 'Tipo'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'DescripcionFormaPago'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 100
            Header.Caption = 'Numero'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'DocumentoNumero'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 85
            Header.Caption = 'Fecha'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = OrdenarColumna
            FieldName = 'FechaCuota'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 90
            Header.Caption = 'Monto'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taRightJustify
            IsLink = False
            FieldName = 'ImporteDesc'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 90
            Header.Caption = 'Saldo'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taRightJustify
            IsLink = False
            FieldName = 'SaldoDesc'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 200
            Header.Caption = 'Banco'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            OnHeaderClick = OrdenarColumna
            FieldName = 'DescripcionBanco'
          end>
        DataSource = dsChequesADepositar
        DragReorder = True
        ParentColor = False
        TabOrder = 2
        TabStop = True
        OnDrawText = DBListEx1DrawText
        OnLinkClick = DBListEx1LinkClick
      end
    end
    object tbsDepositado: TTabSheet
      Caption = 'tbsDepositado'
      ImageIndex = 2
      object DBlExDepositados: TDBListEx
        Left = 0
        Top = 89
        Width = 984
        Height = 317
        Align = alClient
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 55
            Header.Caption = 'Pagar'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = True
            FieldName = 'Pagado'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 70
            Header.Caption = 'Protestar'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = True
            FieldName = 'Protestado'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 65
            Header.Caption = 'Deposito'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = OrdenarColumna
            FieldName = 'IDComprobanteDeposito'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 75
            Header.Caption = 'Fecha'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'FechaDeposito'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 90
            Header.Caption = 'Usuario'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'UsuarioCreacion'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 75
            Header.Caption = 'Rut'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            OnHeaderClick = OrdenarColumna
            FieldName = 'TitularNumeroDocumento'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 120
            Header.Caption = 'Nombre'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'TitularNombre'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 60
            Header.Caption = 'Refinanciaci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taRightJustify
            IsLink = False
            OnHeaderClick = OrdenarColumna
            FieldName = 'CodigoRefinanciacion'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 120
            Header.Caption = 'Convenio'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            OnHeaderClick = OrdenarColumna
            FieldName = 'NumeroConvenio'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 75
            Header.Caption = 'Tipo'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'DescripcionFormaPago'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 100
            Header.Caption = 'Numero'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'DocumentoNumero'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 75
            Header.Caption = 'Fecha'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = OrdenarColumna
            FieldName = 'FechaCuota'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 70
            Header.Caption = 'Monto'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taRightJustify
            IsLink = False
            FieldName = 'ImporteDesc'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 175
            Header.Caption = 'Banco'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            OnHeaderClick = OrdenarColumna
            FieldName = 'BancoDesc'
          end>
        DataSource = dsChequesDepositados
        DragReorder = True
        ParentColor = False
        TabOrder = 0
        TabStop = True
        OnDrawText = DBlExDepositadosDrawText
        OnLinkClick = DBlExDepositadosLinkClick
      end
      object Panel4: TPanel
        Left = 0
        Top = 406
        Width = 984
        Height = 48
        Align = alBottom
        TabOrder = 1
        DesignSize = (
          984
          48)
        object btnPagarProtestar: TButton
          Left = 864
          Top = 13
          Width = 113
          Height = 25
          Anchors = [akRight, akBottom]
          Caption = 'Pagar / Protestar'
          Enabled = False
          TabOrder = 0
          OnClick = btnPagarProtestarClick
        end
      end
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 984
        Height = 89
        Align = alTop
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 2
        DesignSize = (
          984
          89)
        object GroupBox2: TGroupBox
          Left = 2
          Top = 2
          Width = 975
          Height = 81
          Anchors = [akLeft, akTop, akRight, akBottom]
          Caption = '  Par'#225'metros B'#250'squeda  '
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentBackground = False
          ParentColor = False
          ParentFont = False
          TabOrder = 0
          object Label4: TLabel
            Left = 63
            Top = 21
            Width = 53
            Height = 13
            Caption = 'Dep'#243'sito:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label5: TLabel
            Left = 199
            Top = 21
            Width = 85
            Height = 13
            Caption = 'Refinanciaci'#243'n:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label9: TLabel
            Left = 373
            Top = 21
            Width = 67
            Height = 13
            Caption = 'Fecha Pago:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object nedtNroDeposito: TNumericEdit
            Left = 120
            Top = 18
            Width = 65
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
          end
          object nedtNroRefinanciacion: TNumericEdit
            Left = 288
            Top = 18
            Width = 65
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
          end
          object chkPagar: TCheckBox
            Left = 120
            Top = 59
            Width = 129
            Height = 17
            Caption = 'Marcar Todos a Pagar'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            OnClick = chkPagarClick
          end
          object chkProtestar: TCheckBox
            Left = 258
            Top = 59
            Width = 152
            Height = 17
            Caption = 'Marcar Todos a Protestar'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
            OnClick = chkProtestarClick
          end
          object deFechaPagoDeposito: TDateEdit
            Left = 448
            Top = 18
            Width = 100
            Height = 21
            AutoSelect = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 4
            Date = -693594.000000000000000000
          end
        end
      end
    end
    object tbsCobrarVV: TTabSheet
      Caption = 'tbsCobrarVV'
      ImageIndex = 3
      object Panel6: TPanel
        Left = 0
        Top = 0
        Width = 984
        Height = 89
        Align = alTop
        TabOrder = 0
        object GroupBox3: TGroupBox
          Left = 2
          Top = 2
          Width = 1153
          Height = 81
          Caption = '  Par'#225'metros B'#250'squeda  '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          object Label6: TLabel
            Left = 24
            Top = 19
            Width = 140
            Height = 13
            Caption = 'Fecha Documento Hasta:'
          end
          object deFechaValeVistas: TDateEdit
            Left = 171
            Top = 16
            Width = 100
            Height = 21
            AutoSelect = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            Date = -693594.000000000000000000
          end
          object chkCobrar: TCheckBox
            Left = 171
            Top = 59
            Width = 162
            Height = 17
            Caption = 'Marcar Todos a Cobrar'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            OnClick = chkPagarClick
          end
        end
      end
      object Panel7: TPanel
        Left = 0
        Top = 406
        Width = 984
        Height = 48
        Align = alBottom
        TabOrder = 1
        DesignSize = (
          984
          48)
        object btnCobrarVV: TButton
          Left = 598
          Top = 13
          Width = 113
          Height = 25
          Anchors = [akRight, akBottom]
          Caption = 'Cobrar'
          TabOrder = 0
          OnClick = btnCobrarVVClick
        end
      end
      object DBListEx2: TDBListEx
        Left = 0
        Top = 89
        Width = 984
        Height = 317
        Align = alClient
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 65
            Header.Caption = 'Cobrar'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = True
            FieldName = 'Cobrar'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 120
            Header.Caption = 'Estado'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            OnHeaderClick = OrdenarColumna
            FieldName = 'FormaPagoDescr'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 80
            Header.Caption = 'Rut'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            OnHeaderClick = OrdenarColumna
            FieldName = 'Rut'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'Nombre'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Nombre'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 90
            Header.Caption = 'Refinanciaci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taRightJustify
            IsLink = False
            OnHeaderClick = OrdenarColumna
            FieldName = 'CodigoRefinanciacion'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 120
            Header.Caption = 'Convenio'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            OnHeaderClick = OrdenarColumna
            FieldName = 'NumeroConvenio'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 105
            Header.Caption = 'Fecha Vale Vista'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = OrdenarColumna
            FieldName = 'ChequeFecha'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 100
            Header.Caption = 'Monto'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taRightJustify
            IsLink = False
            FieldName = 'MontoDesc'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 200
            Header.Caption = 'Banco'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            OnHeaderClick = OrdenarColumna
            FieldName = 'BancoDescr'
          end>
        DataSource = dsValeVistasACobrar
        DragReorder = True
        ParentColor = False
        TabOrder = 2
        TabStop = True
        OnDrawText = DBListEx2DrawText
        OnLinkClick = DBListEx2LinkClick
      end
    end
    object tbsPagoCuotas: TTabSheet
      Caption = 'tbsPagoCuotas'
      ImageIndex = 4
      object DBListEx3: TDBListEx
        Left = 0
        Top = 89
        Width = 984
        Height = 317
        Align = alClient
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 65
            Header.Caption = 'Pagar'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = True
            FieldName = 'Pagar'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 100
            Header.Caption = 'Estado'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            OnHeaderClick = OrdenarColumna
            FieldName = 'DescripcionEstadoCuota'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 80
            Header.Caption = 'Rut'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            OnHeaderClick = OrdenarColumna
            FieldName = 'Rut'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'Nombre'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'NombrePersona'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 55
            Header.Caption = 'Refinanciaci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taRightJustify
            IsLink = False
            OnHeaderClick = OrdenarColumna
            FieldName = 'CodigoRefinanciacion'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 120
            Header.Caption = 'Convenio'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            OnHeaderClick = OrdenarColumna
            FieldName = 'NumeroConvenio'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 95
            Header.Caption = 'Tipo'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'DescripcionFormaPago'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 125
            Header.Caption = 'Numero'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'DocumentoNumero'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 85
            Header.Caption = 'Fecha'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = False
            OnHeaderClick = OrdenarColumna
            FieldName = 'FechaCuota'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 80
            Header.Caption = 'Monto'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taRightJustify
            IsLink = False
            FieldName = 'ImporteDesc'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 80
            Header.Caption = 'Saldo'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taRightJustify
            IsLink = False
            FieldName = 'SaldoDesc'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 200
            Header.Caption = 'Banco'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            OnHeaderClick = OrdenarColumna
            FieldName = 'DescripcionBanco'
          end>
        DataSource = dsCuotasAPagar
        DragReorder = True
        ParentColor = False
        TabOrder = 0
        TabStop = True
        OnDrawText = DBListEx3DrawText
        OnLinkClick = DBListEx3LinkClick
      end
      object Panel8: TPanel
        Left = 0
        Top = 406
        Width = 984
        Height = 48
        Align = alBottom
        TabOrder = 1
        DesignSize = (
          984
          48)
        object btnPagarCuota: TButton
          Left = 598
          Top = 13
          Width = 113
          Height = 25
          Anchors = [akRight, akBottom]
          Caption = 'Pagar'
          TabOrder = 0
          OnClick = btnPagarCuotaClick
        end
      end
      object Panel9: TPanel
        Left = 0
        Top = 0
        Width = 984
        Height = 89
        Align = alTop
        TabOrder = 2
        object GroupBox4: TGroupBox
          Left = 2
          Top = 2
          Width = 1153
          Height = 81
          Caption = '  Par'#225'metros B'#250'squeda  '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          object Label7: TLabel
            Left = 63
            Top = 21
            Width = 23
            Height = 13
            Caption = 'Rut:'
          end
          object Label8: TLabel
            Left = 254
            Top = 21
            Width = 85
            Height = 13
            Caption = 'Refinanciaci'#243'n:'
          end
          object lblNombreCli: TLabel
            Left = 105
            Top = 43
            Width = 70
            Height = 13
            Caption = 'lblNombreCli'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object lblPersoneria: TLabel
            Left = 184
            Top = 43
            Width = 61
            Height = 13
            Caption = 'lblPersoneria'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object nedtRefinanciacionPagoCuotas: TNumericEdit
            Left = 343
            Top = 18
            Width = 65
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
          end
          object chkPagarCuotas: TCheckBox
            Left = 105
            Top = 59
            Width = 129
            Height = 17
            Caption = 'Marcar Todos a Pagar'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            OnClick = chkPagarCuotasClick
          end
          object peRut: TPickEdit
            Left = 91
            Top = 18
            Width = 145
            Height = 21
            CharCase = ecUpperCase
            Enabled = True
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            OnChange = peRutChange
            OnKeyDown = peRutKeyDown
            OnKeyPress = peRutKeyPress
            EditorStyle = bteTextEdit
            OnButtonClick = peRutButtonClick
          end
        end
      end
    end
  end
  object spObtenerRefinanciacionChequesADepositar: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerRefinanciacionChequesADepositar'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@FechaHasta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Pendiente'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@Protestado'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@EnCobranza'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@MoraPactada'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 104
    Top = 272
  end
  object spActualizarRefinanciacionCuotasEstados: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarRefinanciacionCuotasEstados'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCuota'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@CodigoEstadoCuota'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 296
    Top = 448
  end
  object spCrearRefinanciacionComprobanteDeposito: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CrearRefinanciacionComprobanteDeposito'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 368
    Top = 392
  end
  object spCrearRefinanciacionComprobanteDepositoDetalle: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CrearRefinanciacionComprobanteDepositoDetalle'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@IDComprobanteDeposito'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCuota'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end>
    Left = 504
    Top = 424
  end
  object spObtenerRefinanciacionChequesDepositados: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerRefinanciacionChequesDepositados'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IDComprobanteDeposito'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NoComprobarEstadoCheques'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 368
    Top = 272
  end
  object dsChequesDepositados: TDataSource
    DataSet = cdsChequesDepositados
    Left = 464
    Top = 320
  end
  object cdsChequesDepositados: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'IDComprobanteDeposito'
        DataType = ftInteger
      end
      item
        Name = 'FechaDeposito'
        DataType = ftDateTime
      end
      item
        Name = 'UsuarioCreacion'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CodigoRefinanciacion'
        DataType = ftInteger
      end
      item
        Name = 'CodigoCuota'
        DataType = ftSmallint
      end
      item
        Name = 'TitularNumeroDocumento'
        DataType = ftString
        Size = 11
      end
      item
        Name = 'TitularTipoDocumento'
        Attributes = [faFixed]
        DataType = ftString
        Size = 4
      end
      item
        Name = 'TitularNombre'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'CodigoConvenio'
        DataType = ftInteger
      end
      item
        Name = 'NumeroConvenio'
        Attributes = [faReadonly, faFixed]
        DataType = ftString
        Size = 30
      end
      item
        Name = 'DocumentoNumero'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Importe'
        Attributes = [faReadonly]
        DataType = ftLargeint
      end
      item
        Name = 'ImporteDesc'
        Attributes = [faReadonly]
        DataType = ftString
        Size = 20
      end
      item
        Name = 'FechaCuota'
        DataType = ftDateTime
      end
      item
        Name = 'CodigoBanco'
        DataType = ftInteger
      end
      item
        Name = 'BancoDesc'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'CodigoTarjeta'
        DataType = ftInteger
      end
      item
        Name = 'TarjetaCuotas'
        DataType = ftSmallint
      end
      item
        Name = 'CodigoFormaPago'
        DataType = ftSmallint
      end
      item
        Name = 'FormaPagoDesc'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'DescripcionFormaPago'
        DataType = ftString
        Size = 30
      end>
    IndexDefs = <>
    Params = <>
    ProviderName = 'dspRefinanciacionObtenerChequesDepositados'
    StoreDefs = True
    Left = 432
    Top = 304
    object cdsChequesDepositadosPagado: TBooleanField
      FieldKind = fkInternalCalc
      FieldName = 'Pagado'
    end
    object cdsChequesDepositadosProtestado: TBooleanField
      FieldKind = fkInternalCalc
      FieldName = 'Protestado'
    end
    object cdsChequesDepositadosIDComprobanteDeposito: TIntegerField
      FieldName = 'IDComprobanteDeposito'
    end
    object cdsChequesDepositadosUsuarioCreacion: TStringField
      FieldName = 'UsuarioCreacion'
      FixedChar = True
    end
    object cdsChequesDepositadosCodigoRefinanciacion: TIntegerField
      FieldName = 'CodigoRefinanciacion'
    end
    object cdsChequesDepositadosCodigoCuota: TSmallintField
      FieldName = 'CodigoCuota'
    end
    object cdsChequesDepositadosTitularNumeroDocumento: TStringField
      FieldName = 'TitularNumeroDocumento'
      Size = 11
    end
    object cdsChequesDepositadosTitularNombre: TStringField
      FieldName = 'TitularNombre'
      Size = 100
    end
    object cdsChequesDepositadosCodigoConvenio: TIntegerField
      FieldName = 'CodigoConvenio'
    end
    object cdsChequesDepositadosNumeroConvenio: TStringField
      FieldName = 'NumeroConvenio'
      FixedChar = True
      Size = 30
    end
    object cdsChequesDepositadosDocumentoNumero: TStringField
      FieldName = 'DocumentoNumero'
    end
    object cdsChequesDepositadosImporte: TLargeintField
      FieldName = 'Importe'
    end
    object cdsChequesDepositadosImporteDesc: TStringField
      FieldName = 'ImporteDesc'
    end
    object cdsChequesDepositadosBancoDesc: TStringField
      FieldName = 'BancoDesc'
      Size = 50
    end
    object cdsChequesDepositadosFormaPagoDesc: TStringField
      FieldName = 'FormaPagoDesc'
      Size = 30
    end
    object cdsChequesDepositadosDescripcionFormaPago: TStringField
      FieldName = 'DescripcionFormaPago'
      Size = 30
    end
    object cdsChequesDepositadosFechaDeposito: TDateTimeField
      FieldName = 'FechaDeposito'
    end
    object cdsChequesDepositadosFechaCuota: TDateTimeField
      FieldName = 'FechaCuota'
    end
    object cdsChequesDepositadosTitularTipoDocumento: TStringField
      FieldName = 'TitularTipoDocumento'
      FixedChar = True
      Size = 4
    end
    object cdsChequesDepositadosCodigoBanco: TIntegerField
      FieldName = 'CodigoBanco'
    end
    object cdsChequesDepositadosCodigoTarjeta: TIntegerField
      FieldName = 'CodigoTarjeta'
    end
    object cdsChequesDepositadosTarjetaCuotas: TSmallintField
      FieldName = 'TarjetaCuotas'
    end
    object cdsChequesDepositadosCodigoFormaPago: TSmallintField
      FieldName = 'CodigoFormaPago'
    end
  end
  object dspRefinanciacionObtenerChequesDepositados: TDataSetProvider
    DataSet = spObtenerRefinanciacionChequesDepositados
    Left = 400
    Top = 288
  end
  object lnCheck: TImageList
    BkColor = clWhite
    Height = 15
    Width = 15
    Left = 63
    Top = 427
    Bitmap = {
      494C01010300040008000F000F00FFFFFF00FF10FFFFFFFFFFFFFFFF424D3600
      00000000000036000000280000003C0000000F0000000100200000000000100E
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF000000FF000000FF000000FF000000FF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      FF000000FF000000FF000000FF000000FF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000FF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF000000FF00FFFFFF000000FF000000FF000000
      FF000000FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      FF00FFFFFF00FFFFFF000000FF000000FF000000FF000000FF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000FF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF000000
      000000000000FFFFFF00000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF000000FF00FFFFFF00FFFFFF00FFFFFF000000
      FF000000FF000000FF000000FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF000000FF000000FF000000
      FF00FFFFFF00FFFFFF00FFFFFF000000FF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF000000FF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000FF000000FF000000FF000000FF00FFFFFF00FFFFFF000000
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF000000
      FF000000FF000000FF00FFFFFF000000FF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF000000FF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000FF000000FF000000FF000000FF000000
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000FF000000FF000000FF000000FF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF000000FF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF000000FF000000
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000424D3E000000000000003E000000280000003C0000000F00000001000100
      00000000780000000000000000000000000000000000000000000000FFFFFF00
      FFFFFFFFFFF80000800300060008000080030006000800008003000600080000
      8003000600080000800300060008000080030006000800008003000600080000
      8003000600080000800300060008000080030006000800008003000600080000
      80030006000800008003000600080000FFFFFFFFFFF800000000000000000000
      0000000000000000000000000000}
  end
  object cdsChequesADepositar: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspRefinanciacionObtenerChequesADepositar'
    Left = 168
    Top = 304
    object cdsChequesADepositarCodigoRefinanciacion: TIntegerField
      FieldName = 'CodigoRefinanciacion'
    end
    object cdsChequesADepositarCodigoCuota: TSmallintField
      FieldName = 'CodigoCuota'
    end
    object cdsChequesADepositarCodigoEstadoCuota: TSmallintField
      FieldName = 'CodigoEstadoCuota'
    end
    object cdsChequesADepositarDescripcionEstadoCuota: TStringField
      FieldName = 'DescripcionEstadoCuota'
      Size = 30
    end
    object cdsChequesADepositarCodigoFormaPago: TSmallintField
      FieldName = 'CodigoFormaPago'
    end
    object cdsChequesADepositarDescripcionFormaPago: TStringField
      FieldName = 'DescripcionFormaPago'
      Size = 30
    end
    object cdsChequesADepositarTitularNombre: TStringField
      FieldName = 'TitularNombre'
      Size = 100
    end
    object cdsChequesADepositarTitularTipoDocumento: TStringField
      FieldName = 'TitularTipoDocumento'
      FixedChar = True
      Size = 4
    end
    object cdsChequesADepositarTitularNumeroDocumento: TStringField
      FieldName = 'TitularNumeroDocumento'
      Size = 11
    end
    object cdsChequesADepositarCodigoBanco: TIntegerField
      FieldName = 'CodigoBanco'
    end
    object cdsChequesADepositarDescripcionBanco: TStringField
      FieldName = 'DescripcionBanco'
      Size = 50
    end
    object cdsChequesADepositarDocumentoNumero: TStringField
      FieldName = 'DocumentoNumero'
    end
    object cdsChequesADepositarFechaCuota: TDateTimeField
      FieldName = 'FechaCuota'
    end
    object cdsChequesADepositarImporte: TLargeintField
      FieldName = 'Importe'
    end
    object cdsChequesADepositarImporteDesc: TStringField
      FieldName = 'ImporteDesc'
    end
    object cdsChequesADepositarEstadoImpago: TBooleanField
      FieldName = 'EstadoImpago'
    end
    object cdsChequesADepositarNumeroConvenio: TStringField
      FieldName = 'NumeroConvenio'
      FixedChar = True
      Size = 30
    end
    object cdsChequesADepositarDepositar: TBooleanField
      FieldKind = fkInternalCalc
      FieldName = 'Depositar'
    end
    object cdsChequesADepositarSaldo: TLargeintField
      FieldName = 'Saldo'
    end
    object cdsChequesADepositarSaldoDesc: TStringField
      FieldName = 'SaldoDesc'
    end
  end
  object dspRefinanciacionObtenerChequesADepositar: TDataSetProvider
    DataSet = spObtenerRefinanciacionChequesADepositar
    Left = 136
    Top = 288
  end
  object dsChequesADepositar: TDataSource
    DataSet = cdsChequesADepositar
    Left = 200
    Top = 328
  end
  object cdsValeVistasACobrar: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspRefinanciacionObtenerValeVistasACobrar'
    Left = 704
    Top = 304
    object cdsValeVistasACobrarIDRefinanciacion: TIntegerField
      FieldName = 'IDRefinanciacion'
    end
    object cdsValeVistasACobrarIDDetallePagoComprobante: TIntegerField
      FieldName = 'IDDetallePagoComprobante'
    end
    object cdsValeVistasACobrarEstado: TSmallintField
      FieldName = 'Estado'
    end
    object cdsValeVistasACobrarRut: TStringField
      FieldName = 'Rut'
      FixedChar = True
      Size = 11
    end
    object cdsValeVistasACobrarCodigoConvenio: TIntegerField
      FieldName = 'CodigoConvenio'
    end
    object cdsValeVistasACobrarCodigoRefinanciacion: TIntegerField
      FieldName = 'CodigoRefinanciacion'
    end
    object cdsValeVistasACobrarNumeroConvenio: TStringField
      FieldName = 'NumeroConvenio'
      FixedChar = True
      Size = 30
    end
    object cdsValeVistasACobrarNombre: TStringField
      FieldName = 'Nombre'
      ReadOnly = True
      Size = 100
    end
    object cdsValeVistasACobrarChequeNumero: TStringField
      FieldName = 'ChequeNumero'
    end
    object cdsValeVistasACobrarChequeMonto: TLargeintField
      FieldName = 'ChequeMonto'
    end
    object cdsValeVistasACobrarMontoDesc: TStringField
      FieldName = 'MontoDesc'
      ReadOnly = True
    end
    object cdsValeVistasACobrarChequeFecha: TDateTimeField
      FieldName = 'ChequeFecha'
    end
    object cdsValeVistasACobrarChequeTitular: TStringField
      FieldName = 'ChequeTitular'
      Size = 100
    end
    object cdsValeVistasACobrarChequeCodigoBanco: TIntegerField
      FieldName = 'ChequeCodigoBanco'
    end
    object cdsValeVistasACobrarFormaPagoDescr: TStringField
      FieldName = 'FormaPagoDescr'
      FixedChar = True
      Size = 30
    end
    object cdsValeVistasACobrarBancoDescr: TStringField
      FieldName = 'BancoDescr'
      Size = 50
    end
    object cdsValeVistasACobrarCobrar: TBooleanField
      FieldKind = fkInternalCalc
      FieldName = 'Cobrar'
    end
  end
  object dsValeVistasACobrar: TDataSource
    DataSet = cdsValeVistasACobrar
    Left = 752
    Top = 328
  end
  object spRefinanciacionObtenerValeVistasACobrar: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'RefinanciacionObtenerValeVistasACobrar'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@FechaHasta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 632
    Top = 272
  end
  object dspRefinanciacionObtenerValeVistasACobrar: TDataSetProvider
    DataSet = spRefinanciacionObtenerValeVistasACobrar
    Left = 664
    Top = 288
  end
  object spObtenerRefinanciacionCuotasAPagar: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerRefinanciacionCuotasAPagar'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@RUTPersona'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 880
    Top = 240
  end
  object dspRefinanciacionObtenerCuotasAPagar: TDataSetProvider
    DataSet = spObtenerRefinanciacionCuotasAPagar
    Left = 896
    Top = 264
  end
  object cdsCuotasAPagar: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspRefinanciacionObtenerCuotasAPagar'
    Left = 912
    Top = 288
    object cdsCuotasAPagarPagar: TBooleanField
      FieldKind = fkInternalCalc
      FieldName = 'Pagar'
    end
    object cdsCuotasAPagarRut: TStringField
      FieldName = 'Rut'
      FixedChar = True
      Size = 11
    end
    object cdsCuotasAPagarNombrePersona: TStringField
      FieldName = 'NombrePersona'
      ReadOnly = True
      Size = 100
    end
    object cdsCuotasAPagarCodigoRefinanciacion: TIntegerField
      FieldName = 'CodigoRefinanciacion'
    end
    object cdsCuotasAPagarCodigoCuota: TSmallintField
      FieldName = 'CodigoCuota'
    end
    object cdsCuotasAPagarCodigoEstadoCuota: TSmallintField
      FieldName = 'CodigoEstadoCuota'
    end
    object cdsCuotasAPagarDescripcionEstadoCuota: TStringField
      FieldName = 'DescripcionEstadoCuota'
      Size = 30
    end
    object cdsCuotasAPagarCodigoFormaPago: TSmallintField
      FieldName = 'CodigoFormaPago'
    end
    object cdsCuotasAPagarDescripcionFormaPago: TStringField
      FieldName = 'DescripcionFormaPago'
      Size = 30
    end
    object cdsCuotasAPagarTitularNombre: TStringField
      FieldName = 'TitularNombre'
      Size = 100
    end
    object cdsCuotasAPagarTitularTipoDocumento: TStringField
      FieldName = 'TitularTipoDocumento'
      FixedChar = True
      Size = 4
    end
    object cdsCuotasAPagarTitularNumeroDocumento: TStringField
      FieldName = 'TitularNumeroDocumento'
      Size = 11
    end
    object cdsCuotasAPagarCodigoBanco: TIntegerField
      FieldName = 'CodigoBanco'
    end
    object cdsCuotasAPagarDescripcionBanco: TStringField
      FieldName = 'DescripcionBanco'
      Size = 50
    end
    object cdsCuotasAPagarDocumentoNumero: TStringField
      FieldName = 'DocumentoNumero'
    end
    object cdsCuotasAPagarFechaCuota: TDateTimeField
      FieldName = 'FechaCuota'
    end
    object cdsCuotasAPagarImporte: TLargeintField
      FieldName = 'Importe'
      ReadOnly = True
    end
    object cdsCuotasAPagarImporteDesc: TStringField
      FieldName = 'ImporteDesc'
      ReadOnly = True
    end
    object cdsCuotasAPagarEstadoImpago: TBooleanField
      FieldName = 'EstadoImpago'
    end
    object cdsCuotasAPagarNumeroConvenio: TStringField
      FieldName = 'NumeroConvenio'
      ReadOnly = True
      FixedChar = True
      Size = 30
    end
    object cdsCuotasAPagarSaldo: TLargeintField
      FieldName = 'Saldo'
      ReadOnly = True
    end
    object cdsCuotasAPagarSaldoDesc: TStringField
      FieldName = 'SaldoDesc'
      ReadOnly = True
    end
  end
  object dsCuotasAPagar: TDataSource
    DataSet = cdsCuotasAPagar
    Left = 928
    Top = 312
  end
  object spActualizarRefinanciacionComprobanteDepositoDetalle: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarRefinanciacionComprobanteDepositoDetalle'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@IDComprobanteDeposito'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCuota'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end>
    Left = 688
    Top = 392
  end
  object spActualizarRefinanciacionCuotaPago: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarRefinanciacionCuotaPago'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCuota'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@Importe'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@Anticipado'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@CodigoFormaPago'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@TitularNombre'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@TitularTipoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@TitularNumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@DocumentoNumero'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@CodigoBanco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTarjeta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TarjetaCuotas'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaPago'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@NumeroRecibo'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 960
    Top = 384
  end
end
