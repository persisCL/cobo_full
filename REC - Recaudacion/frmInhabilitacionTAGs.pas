{-----------------------------------------------------------------------------
 Unit Name: frmInhabilitacionTAGs
 Autor:    	cfuentesc
 Fecha:     09/03/2017
 Prop�sito: Inhabilitar TAGs de convenios morosos
 			CU.COBO.COB.251
 Firma: 	TASK_118_CFU_20170309-Inhabilitar_TAGs_Por_Morosidad
 Historia:
-----------------------------------------------------------------------------}
unit frmInhabilitacionTAGs;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, VariantComboBox, DmiCtrls, ExtCtrls, ListBoxEx, DBListEx,
  Buttons, DMConnection, UtilDB, Util, PeaProcs, PeaProcsCN, DB,
  ADODB, UtilProc, ImgList, SysUtilsCN, DBClient, Provider;

type
  TFormInhabilitacionTAGs = class(TForm)
    dlConvenios: TDBListEx;
    Panel2: TPanel;
    btnSalir: TButton;
    btnInhabilitar: TBitBtn;
    btnCartaCertificada: TBitBtn;
    dsObtenerConveniosMorosos: TDataSource;
    lnCheck: TImageList;
    Panel1: TPanel;
    chkMarcarTodos: TCheckBox;
    lblCantidadRegistros: TLabel;
    spInhabilitarTAGsPorMorosidad: TADOStoredProc;
    spGenerarNotificacionesCobranza: TADOStoredProc;
    spObtenerDatosConvenio: TADOStoredProc;
    spGuardarAccionConvenio: TADOStoredProc;
    spIngresarConvenioACobranzaInterna: TADOStoredProc;
    cdsObtenerConveniosMorosos: TClientDataSet;
    cdsObtenerConveniosMorososMarcar: TBooleanField;
    cdsObtenerConveniosMorososCodigoConvenio: TIntegerField;
    cdsObtenerConveniosMorososFechaEnvioCartaCertif: TDateField;
    cdsObtenerConveniosMorososNumeroDocumento: TStringField;
    cdsObtenerConveniosMorososNombrePersona: TStringField;
    cdsObtenerConveniosMorososNumeroConvenio: TStringField;
    cdsObtenerConveniosMorososTipoDeCliente: TStringField;
    cdsObtenerConveniosMorososSaldo: TFloatField;
    cdsObtenerConveniosMorososDiasDeuda: TIntegerField;
    cdsObtenerConveniosMorososCantidadTAGs: TIntegerField;
    cdsObtenerConveniosMorososConcesionaria: TStringField;
    cdsObtenerConveniosMorososAcuseReciboCartaCertif: TStringField;
    spInsertarEnvioCartaAvisoInhabilitacion: TADOStoredProc;
    procedure btnSalirClick(Sender: TObject);
    procedure btnInhabilitarClick(Sender: TObject);
    procedure dlConveniosDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure chkMarcarTodosClick(Sender: TObject);
    procedure dlConveniosLinkClick(Sender: TCustomDBListEx;
      Column: TDBListExColumn);
    function TraeRegistros : Boolean;
    procedure btnCartaCertificadaClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure dlConveniosDblClick(Sender: TObject);
  private
    { Private declarations }
    CantConveniosMarcados, FCodigoPlantilla: Integer;
    FAccionCobranza: string;
    BIngresaACobInterna: Boolean;
    FListaConvenios: TStringList;
    function CargarConvenios(ListaConvenios: TStringList): Boolean;
    procedure MarcarDesmarcarConvenios(Marcar: Boolean);
  public
    { Public declarations }
	function Inicializar(ListaConvenios: TStringList; CodigoPlantilla: Integer; AccionCobranza: String; IngresaCobInterna: Boolean): Boolean;
  end;

var
  FormInhabilitacionTAGs: TFormInhabilitacionTAGs;

const
	CONST_TIPO_PLANTILLA_CARTA_CERTIFICADA				= 3;
	COD_NOTIFICACION_COBRANZA_AVISO_CARTA_CERTIFICADA	= 3;

resourcestring
    MSG_NO_HAY_REGISTROS 				= 'No hay registros para inhabilitar TAGs';
    MSG_NO_HAY_SELECCION				= 'No ha seleccionado registros para inhabilitar TAGs';

implementation

{$R *.dfm}

function TFormInhabilitacionTAGs.Inicializar(ListaConvenios: TStringList; CodigoPlantilla: Integer; AccionCobranza: String; IngresaCobInterna: Boolean): Boolean;
var
	S: TSize;
begin
    Result 		:= False;
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
    FCodigoPlantilla 	:= CodigoPlantilla;
    FAccionCobranza		:= AccionCobranza;
    FListaConvenios		:= ListaConvenios;
    BIngresaACobInterna	:= IngresaCobInterna;
    if not CargarConvenios(FListaConvenios) then
    	Exit;

    CantConveniosMarcados	:= 0;
    Result	:= True;
end;

function TFormInhabilitacionTAGs.CargarConvenios(ListaConvenios: TStringList): Boolean;
var
	i: Integer;
    strConvenios: string;
begin
	Result := False;
	try
//        Screen.Cursor := crHourGlass;
//       Application.ProcessMessages;

        strConvenios	:= '';
        for i := 0 to ListaConvenios.Count - 1 do
			strConvenios	:= strConvenios + ListaConvenios[i] + ',';
    	strConvenios := Copy(strConvenios, 1, Length(strConvenios)-1);

        with spObtenerDatosConvenio, Parameters do begin
            if Active then
                Close;
            Parameters.Refresh;
            ParamByName('@CodigosConvenio').Value := strConvenios;
        end;
        
        if not TraeRegistros then
        	Exit;

        lblCantidadRegistros.Visible	:= True;
		lblCantidadRegistros.Caption	:= FloatToStrf(cdsObtenerConveniosMorosos.RecordCount, ffNumber, 18, 0) + ' Registros';        
    finally
//        Screen.Cursor := crDefault;
    end;
    Result := True;
end;

function TFormInhabilitacionTAGs.TraeRegistros: Boolean;
resourcestring
   	MSG_ERROR_SELECT = 'Error Consultando Registros';
var
	Resultado     : Boolean;
	Retorno, i    : Integer;
	MensajeError  : string;
begin
  	try
    	spObtenerDatosConvenio.Open;
    	Retorno := spObtenerDatosConvenio.Parameters.ParamByName('@RETURN_VALUE').Value;

    	if Retorno <> 0 then begin
      		MensajeError := spObtenerDatosConvenio.Parameters.ParamByName('@ErrorDescription').Value;
      		Application.MessageBox(PChar(MensajeError),'Problema', MB_ICONERROR);
        end;

    	cdsObtenerConveniosMorosos.DisableControls;

    	cdsObtenerConveniosMorosos.Active   := False;

    	for i := 0 to cdsObtenerConveniosMorosos.Fields.Count - 1 do
      		cdsObtenerConveniosMorosos.Fields[i].ReadOnly := False;

    	try
      		cdsObtenerConveniosMorosos.EmptyDataSet;
    	except
    	end;

        cdsObtenerConveniosMorosos.CreateDataSet;
    	cdsObtenerConveniosMorosos.Active   := True;
    	cdsObtenerConveniosMorosos.ReadOnly := False;

    	spObtenerDatosConvenio.First;
    	while not spObtenerDatosConvenio.EOF do begin
      		cdsObtenerConveniosMorosos.Append;

			cdsObtenerConveniosMorosos.FieldByName('CodigoConvenio').Value	        := spObtenerDatosConvenio.FieldByName('CodigoConvenio').Value;
			cdsObtenerConveniosMorosos.FieldByName('NumeroDocumento').Value	        := spObtenerDatosConvenio.FieldByName('NumeroDocumento').Value;
			cdsObtenerConveniosMorosos.FieldByName('NombrePersona').Value	        := spObtenerDatosConvenio.FieldByName('NombrePersona').Value;
			cdsObtenerConveniosMorosos.FieldByName('NumeroConvenio').Value	        := spObtenerDatosConvenio.FieldByName('NumeroConvenio').Value;
			cdsObtenerConveniosMorosos.FieldByName('TipoDeCliente').Value    	    := spObtenerDatosConvenio.FieldByName('TipoDeCliente').Value;
			cdsObtenerConveniosMorosos.FieldByName('Saldo').Value				    := spObtenerDatosConvenio.FieldByName('Saldo').Value;
			cdsObtenerConveniosMorosos.FieldByName('DiasDeuda').Value			    := spObtenerDatosConvenio.FieldByName('DiasDeuda').Value;
            cdsObtenerConveniosMorosos.FieldByName('CantidadTAGs').Value			:= spObtenerDatosConvenio.FieldByName('CantidadTAGs').Value;
            cdsObtenerConveniosMorosos.FieldByName('Concesionaria').Value			:= spObtenerDatosConvenio.FieldByName('Concesionaria').Value;
            cdsObtenerConveniosMorosos.FieldByName('FechaEnvioCartaCertif').Value	:= spObtenerDatosConvenio.FieldByName('FechaEnvioCartaCertif').Value;
            cdsObtenerConveniosMorosos.FieldByName('AcuseReciboCartaCertif').Value	:= spObtenerDatosConvenio.FieldByName('AcuseReciboCartaCertif').Value;

      		cdsObtenerConveniosMorosos.Post;
      		spObtenerDatosConvenio.Next;
    	end;

    	for i := 1 to cdsObtenerConveniosMorosos.Fields.Count - 1 do
      		cdsObtenerConveniosMorosos.Fields[i].ReadOnly := True;

    	spObtenerDatosConvenio.Close;
    	cdsObtenerConveniosMorosos.First;
    	cdsObtenerConveniosMorosos.EnableControls;

    	Resultado := True;

  	except
    	on E : Exception do begin
      		MsgBoxErr(MSG_ERROR_SELECT, E.Message, Caption, MB_ICONERROR);
      		Resultado := False;
    	end;
  	end;

  Result := Resultado;
end;

procedure TFormInhabilitacionTAGs.chkMarcarTodosClick(Sender: TObject);
begin
	if chkMarcarTodos.Checked then begin
    	chkMarcarTodos.Caption	:= 'Desmarcar todos';
        MarcarDesmarcarConvenios(True);
        CantConveniosMarcados	:= cdsObtenerConveniosMorosos.RecordCount;
    end
    else begin
    	chkMarcarTodos.Caption	:= 'Marcar todos';
        MarcarDesmarcarConvenios(False);
        CantConveniosMarcados	:= 0;
    end;
end;

procedure TFormInhabilitacionTAGs.MarcarDesmarcarConvenios(Marcar: Boolean);
var
    Puntero: TBookmark;
begin
    try
        CambiarEstadoCursor(CURSOR_RELOJ, Self);
        Puntero := cdsObtenerConveniosMorosos.GetBookmark;
        dsObtenerConveniosMorosos.DataSet := Nil;

        with cdsObtenerConveniosMorosos do begin
        	First;
        	while not Eof do begin
                Edit;
                FieldByName('Marcar').AsBoolean := Marcar;
            	Next;
            end;
        end;

    finally
        if cdsObtenerConveniosMorosos.BookmarkValid(Puntero) then begin
            cdsObtenerConveniosMorosos.GotoBookmark(Puntero);
        end;
        cdsObtenerConveniosMorosos.FreeBookmark(Puntero);
        dsObtenerConveniosMorosos.DataSet := cdsObtenerConveniosMorosos;

        CambiarEstadoCursor(CURSOR_DEFECTO, Self);
    end;
end;

procedure TFormInhabilitacionTAGs.dlConveniosDrawText(Sender: TCustomDBListEx;
  Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
  var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
  var DefaultDraw: Boolean);
var
    bmp: TBitMap;
begin
    with Sender do begin
        if (Column.FieldName = 'Marcar') then try
            Text := '';
            Canvas.FillRect(Rect);
            bmp := TBitMap.Create;
			DefaultDraw := False;

            if cdsObtenerConveniosMorososMarcar.AsBoolean then
	            lnCheck.GetBitmap(1, Bmp)
            else
            	lnCheck.GetBitmap(0, Bmp);

            Canvas.Draw((Rect.Left + Rect.Right - Bmp.Width) div 2, Rect.Top, bmp);
        finally
            bmp.Free;
        end;
    end;
end;

procedure TFormInhabilitacionTAGs.dlConveniosLinkClick(Sender: TCustomDBListEx;
  Column: TDBListExColumn);
begin
    cdsObtenerConveniosMorosos.Edit;
    cdsObtenerConveniosMorososMarcar.AsBoolean := not cdsObtenerConveniosMorososMarcar.AsBoolean;
    if cdsObtenerConveniosMorososMarcar.AsBoolean then
    	CantConveniosMarcados := CantConveniosMarcados+1
    else
         CantConveniosMarcados := CantConveniosMarcados-1;
    cdsObtenerConveniosMorosos.Post;
end;

procedure TFormInhabilitacionTAGs.dlConveniosDblClick(Sender: TObject);
begin
    cdsObtenerConveniosMorosos.Edit;
    cdsObtenerConveniosMorososMarcar.AsBoolean := not cdsObtenerConveniosMorososMarcar.AsBoolean;
    if cdsObtenerConveniosMorososMarcar.AsBoolean then
    	CantConveniosMarcados := CantConveniosMarcados+1
    else
         CantConveniosMarcados := CantConveniosMarcados-1;
    cdsObtenerConveniosMorosos.Post;
end;

procedure TFormInhabilitacionTAGs.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	FListaConvenios.Free;
	Action	:= caFree;
end;

procedure TFormInhabilitacionTAGs.FormCreate(Sender: TObject);
begin
	BIngresaACobInterna	:= False;
    FListaConvenios		:= TStringList.Create;
end;

procedure TFormInhabilitacionTAGs.btnCartaCertificadaClick(Sender: TObject);
resourcestring
    MSG_CONFIRMA_ENVIO_CARTA_TITULO  	= 'Confirmaci�n env�o carta certificada.';
    MSG_CONFIRMA_ENVIO_CARTA_MENSAJE 	= '�Est� seguro de enviar carta certificada a los clientes seleccionados?';
    MSG_ERROR_ENVIO_CARTA_CONVENIO      = 'Error al enviar carta certificada';
    MSG_YA_SE_ENVIO_CARTA_CERTIFICADA	= 'Ya se envi� carta certificada de Inhabilitaci�n de TAGs a este convenio.'+#13+
    									  '�Seguro desea continuar?';
begin
	if (not cdsObtenerConveniosMorosos.Active) or (cdsObtenerConveniosMorosos.RecordCount=0) then begin
    	MsgBox(MSG_NO_HAY_REGISTROS, Caption, MB_ICONINFORMATION);
        Exit;
    end;

    if CantConveniosMarcados = 0 then begin
    	MsgBox(MSG_NO_HAY_SELECCION, Caption, MB_ICONINFORMATION);
        Exit;
    end;

	if ShowMsgBoxCN(MSG_CONFIRMA_ENVIO_CARTA_TITULO, MSG_CONFIRMA_ENVIO_CARTA_MENSAJE, MB_ICONQUESTION, Self) = mrOk then begin
    	try
            Screen.Cursor := crHourGlass;
            Application.ProcessMessages;
            cdsObtenerConveniosMorosos.First;
			while (not cdsObtenerConveniosMorosos.Eof) do begin
            	if (cdsObtenerConveniosMorososMarcar.AsBoolean)  then

                	if (cdsObtenerConveniosMorososFechaEnvioCartaCertif.AsDateTime > 0) then begin
                    	if MsgBox(MSG_YA_SE_ENVIO_CARTA_CERTIFICADA, Caption, MB_ICONQUESTION or MB_YESNO) = mrNo then
                        	Exit;
                    end;

                    with spGenerarNotificacionesCobranza, Parameters do begin
                        Parameters.Refresh;
                        ParamByName('@CodigoConvenio').Value		:= cdsObtenerConveniosMorososCodigoConvenio.Value;
                        ParamByName('@CodigoTipoPlantilla').Value	:= CONST_TIPO_PLANTILLA_CARTA_CERTIFICADA;
                        ParamByName('@CodigoPlantilla').Value		:= FCodigoPlantilla;
                        ParamByName('@CodigoAccionAviso').Value		:= COD_NOTIFICACION_COBRANZA_AVISO_CARTA_CERTIFICADA;
                        try
                            ExecProc;
                        except
                            on E:Exception do begin
                                MsgBoxErr(MSG_ERROR_ENVIO_CARTA_CONVENIO, E.Message, Caption, MB_ICONERROR); 
                                Exit;
                            end;
                        end;
                    end;

                    with spInsertarEnvioCartaAvisoInhabilitacion, Parameters do begin
                        Parameters.Refresh;
                        ParamByName('@CodigoConvenio').Value	:= cdsObtenerConveniosMorososCodigoConvenio.Value;
                        ParamByName('@Usuario').Value			:= UsuarioSistema;
                        try
                            ExecProc;
                        except
                            on E: Exception do begin
                                MsgBox(E.Message, Caption, MB_ICONERROR);
                                Exit;
                            end;
                        end;
                    end;

                    if BIngresaACobInterna then begin
                        with spIngresarConvenioACobranzaInterna, Parameters do begin
                            Parameters.Refresh;
                            ParamByName('@CodigoConvenio').Value	:= cdsObtenerConveniosMorososCodigoConvenio.Value;
                            ParamByName('@Usuario').Value			:= UsuarioSistema;
                            try
                                ExecProc;
                            except
                                on E: Exception do begin
                                    MsgBox(E.Message, Caption, MB_ICONERROR);
                                    Exit;
                                end;
                            end;
                        end;
                    end;

                //INICIO	: TASK_124_CFU_20170320
                with spGuardarAccionConvenio, Parameters do begin
                    Parameters.Refresh;
                    ParamByName('@CodigoConvenio').Value				:= cdsObtenerConveniosMorososCodigoConvenio.Value;
                    ParamByName('@Accion').Value						:= FAccionCobranza;
                    ParamByName('@TipoAccion').Value					:= 'Aviso por carta certificada';
                    ParamByName('@EstadoAccion').Value					:= 'Enviada';
                    ParamByName('@CodigoPlantilla').Value				:= FCodigoPlantilla;
                    ParamByName('@CodigoMensaje').Value					:= null;
                    ParamByName('@CodigoEmpresaCobranza').Value			:= null;
                    ParamByName('@CodigoRefinanciacion').Value			:= null;
                    ParamByName('@CodigoAvenimiento').Value				:= null;
                    ParamByName('@FechaInicioAccion').Value				:= Date;
                    ParamByName('@FechaFinAccion').Value				:= null;
                    ParamByName('@Usuario').Value						:= UsuarioSistema;
                    try
                        ExecProc;
                    except
                        on E:Exception do begin
                            MsgBox(E.Message, Caption, MB_ICONERROR);
                            Exit;
                        end;
                    end;
                end;
                //TERMINO	: TASK_124_CFU_20170320
            	cdsObtenerConveniosMorosos.Next;
            end;
        finally
        	Screen.Cursor := crDefault;
        end;
    end;

    CargarConvenios(FListaConvenios);
end;

procedure TFormInhabilitacionTAGs.btnInhabilitarClick(Sender: TObject);
resourcestring
    MSG_CONFIRMA_INHABILITACION_TITULO  = 'Confirmaci�n inhabilitaci�n.';
    MSG_CONFIRMA_INHABILITACION_MENSAJE = '�Est� seguro de Inhabilitar los TAGs del(los) Convenio(s) Seleccionado(s)?';
    MSG_ERROR_INHABILITAR_CONVENIO      = 'Error al inhabilitar Convenio';
    MSG_EXISTE_CONVENIO_SIN_CARTA		= 'Para al menos un convenio seleccionado no se ha enviado Carta de Aviso. S�lo se inhabilitar�n los TAGs de los convenios con Carta de Aviso enviada.';
	MSG_NO_EXISTE_CONVENIO_CON_CARTA	= 'No existen convenios seleccionados a los que se les haya enviado Carta de Aviso.';
var
	i: Integer;
    bExisteConvenioSinCarta, bExisteConvenioConCarta: Boolean;
    TListaConvenios: TStringList;
begin
	if (not cdsObtenerConveniosMorosos.Active) or (cdsObtenerConveniosMorosos.RecordCount=0) then begin
    	MsgBox(MSG_NO_HAY_REGISTROS, Caption, MB_ICONINFORMATION);
        Exit;
    end;

    if CantConveniosMarcados = 0 then begin
    	MsgBox(MSG_NO_HAY_SELECCION, Caption, MB_ICONINFORMATION);
        Exit;
    end;

	if ShowMsgBoxCN(MSG_CONFIRMA_INHABILITACION_TITULO, MSG_CONFIRMA_INHABILITACION_MENSAJE, MB_ICONQUESTION, Self) = mrOk then begin
    	try
        	bExisteConvenioSinCarta := False;
            bExisteConvenioConCarta	:= False;
            Screen.Cursor 			:= crHourGlass;
            TListaConvenios			:= TStringList.Create;
            Application.ProcessMessages;
            cdsObtenerConveniosMorosos.First;
			while (not cdsObtenerConveniosMorosos.Eof) do begin
            	if (cdsObtenerConveniosMorososMarcar.AsBoolean) then begin
                	if not (cdsObtenerConveniosMorososFechaEnvioCartaCertif.AsDateTime > 0) then
                    	bExisteConvenioSinCarta	:= True
                    else begin
                    	bExisteConvenioConCarta	:= True;
                        TListaConvenios.Add(IntToStr(cdsObtenerConveniosMorososCodigoConvenio.AsInteger));
                    end;
                end;
                cdsObtenerConveniosMorosos.Next;
            end;

            if not bExisteConvenioConCarta then begin
            	TListaConvenios.Free;
                MsgBox(MSG_NO_EXISTE_CONVENIO_CON_CARTA, Caption, MB_ICONQUESTION);
                Exit;
            end;

            if bExisteConvenioSinCarta then
                MsgBox(MSG_EXISTE_CONVENIO_SIN_CARTA, Caption, MB_ICONQUESTION);

            for i := 0 to TListaConvenios.Count - 1 do begin
                with spGuardarAccionConvenio, Parameters do begin
                    Parameters.Refresh;
                    ParamByName('@CodigoConvenio').Value				:= StrToInt(TListaConvenios[i]);
                    ParamByName('@Accion').Value						:= FAccionCobranza;
                    ParamByName('@TipoAccion').Value					:= 'Inhabilitaci�n de TAGs';
                    ParamByName('@EstadoAccion').Value					:= 'Inhabilitado';
                    ParamByName('@CodigoPlantilla').Value				:= FCodigoPlantilla;
                    ParamByName('@CodigoMensaje').Value					:= null;
                    ParamByName('@CodigoEmpresaCobranza').Value			:= null;
                    ParamByName('@CodigoRefinanciacion').Value			:= null;
                    ParamByName('@CodigoAvenimiento').Value				:= null;
                    ParamByName('@FechaInicioAccion').Value				:= Date;
                    ParamByName('@FechaFinAccion').Value				:= null;
                    ParamByName('@Usuario').Value						:= UsuarioSistema;
                    try
                        spGuardarAccionConvenio.ExecProc;
                    except
                        on E:Exception do begin
                            MsgBox(E.Message, Caption, MB_ICONERROR);
                            Exit;
                        end;
                    end;
                end;

                 with spInhabilitarTAGsPorMorosidad, Parameters do begin
                    Parameters.Refresh;
                    ParamByName('@CodigoConvenio').Value	:= StrToInt(TListaConvenios[i]);
                    ParamByName('@Usuario').Value			:= UsuarioSistema;
                    try
                        ExecProc;
                    except
                        on E:Exception do begin
                            MsgBoxErr(MSG_ERROR_INHABILITAR_CONVENIO, E.Message, Caption, MB_ICONERROR); 
                            Exit;
                        end;
                    end;
                end;

                if BIngresaACobInterna then begin
                    with spIngresarConvenioACobranzaInterna, Parameters do begin
                        Parameters.Refresh;
                        ParamByName('@CodigoConvenio').Value	:= StrToInt(TListaConvenios[i]);
                        ParamByName('@Usuario').Value			:= UsuarioSistema;
                        try
                            ExecProc;
                        except
                            on E: Exception do begin
                                MsgBox(E.Message, Caption, MB_ICONERROR);
                                Exit;
                            end;
                        end;
                    end;
                end;
            end;
        finally
        	Screen.Cursor := crDefault;
        end;
    end;

    TListaConvenios.Free;
end;

procedure TFormInhabilitacionTAGs.btnSalirClick(Sender: TObject);
begin
	Close;
end;

end.


