{-----------------------------------------------------------------------------
 Unit Name: frmAbmTiposClientes
 Author:    aunanue
 Date:      08-03-2017
 Purpose:   Administra las empresas de cobranzas: CU.COBO.ADM.COB.102
 Firma: TASK_001_AUN_20170308-CU.COBO.ADM.COB.102
 History:
-----------------------------------------------------------------------------}
unit frmAbmEmpresasDeCobranza;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, Util, utildb,
  UtilProc, OleCtrls,  DmiCtrls, Mask,  ComCtrls, PeaProcs, ADODB, BuscaTab,
  Variants, DMConnection, DPSControls, Peatypes, VariantComboBox, ListBoxEx,
  DBListEx, FreTelefono, FreDomicilio;

type
  TAbmEmpresasDeCobranzaForm = class(TForm)
    AbmToolbar: TAbmToolbar;
    GrupoDatos: TPanel;
    Panel2: TPanel;
    Notebook: TNotebook;
    lblAlias: TLabel;
    spListar: TADOStoredProc;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    DBListDatos: TAbmList;
    txtAlias: TEdit;
    lblNombre: TLabel;
    txtNombre: TEdit;
    spEliminar: TADOStoredProc;
    spAlmacenar: TADOStoredProc;
    txtCodigo: TNumericEdit;
    lblRUT: TLabel;
    txtRUT: TEdit;
    txtTipoDeOperacionEmpresasCobranza: TVariantComboBox;
    lblTipoDeOperacionEmpresasCobranza: TLabel;
    lblDescripcion: TLabel;
    txtDescripcion: TEdit;
    Label5: TLabel;
    lblComisionPorCobranza: TLabel;
    pnlGeneral: TPanel;
    lblEdadDeLaDeuda: TLabel;
    txtEdadDeLaDeuda: TNumericEdit;
    lblConvenioClienteTipo: TLabel;
    txtConvenioClienteTipo: TVariantComboBox;
    lblMontoMinimo: TLabel;
    txtMontoMinimo: TNumericEdit;
    lblMontoMaximo: TLabel;
    txtMontoMaximo: TNumericEdit;
    lblCapacidadDeProceso: TLabel;
    txtCapacidadDeProceso: TNumericEdit;
    Label8: TLabel;
    lblTipoJudicialEmpresasCobranza: TLabel;
    txtTipoJudicialEmpresasCobranza: TVariantComboBox;
    gbMediosDeComunicacion: TGroupBox;
    FrameTelefono1: TFrameTelefono;
    lblEmail: TLabel;
    txtEmail: TEdit;
    GBDomicilio: TGroupBox;
    FrameDomicilio1: TFrameDomicilio;
    txtComisionPorAviso: TNumericEdit;
    txtComisionPorCobranza: TNumericEdit;
    Label1: TLabel;
    Label2: TLabel;
    spTelefonos_Almacenar: TADOStoredProc;
    spEmailsContacto_Almacenar: TADOStoredProc;
    spActualizarDomicilio: TADOStoredProc;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure AbmToolbarClose(Sender: TObject);
    procedure BtnSalirClick(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure DBListDatosDelete(Sender: TObject);
    procedure DBListDatosEdit(Sender: TObject);
    procedure DBListDatosInsert(Sender: TObject);
    function DBListDatosProcess(Tabla: TDataSet; var Texto: string): Boolean;
    procedure DBListDatosClick(Sender: TObject);
    procedure DBListDatosDrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect;
      State: TOwnerDrawState; Cols: TColPositions);
    procedure DBListDatosRefresh(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure txtRUTKeyPress(Sender: TObject; var Key: Char);
  private
    FDomicilioEmpresa: TDatosDomicilio;
    procedure VolverCampos;
    procedure LimpiarCampos(usarCeros: boolean);
    function DatosADomicilioEmpresa(DesdeRecord: boolean; CodigoDomicilio: integer): TDatosDomicilio;
	{ Private declarations }
  public
    { Public declarations }
    function Inicializa: Boolean;
  end;

resourcestring
    STR_CAPTION = 'Empresas de cobranza';

implementation

uses DB_CRUDCommonProcs, RStrings;

var
    CodigoActual: integer;

const NOMBRE_CAMPO_CODIGO = 'CodigoEmpresaCobranza';

{$R *.DFM}

{-----------------------------------------------------------------------------
  Procedure: Inicializar
  Author:    aunanue
  Date:      08-03-2017
  Arguments: MDIChild: Boolean
  Result:    Boolean
  Purpose:   Inicializa el formulario
-----------------------------------------------------------------------------}
function TAbmEmpresasDeCobranzaForm.Inicializa: Boolean;
var
	S: TSize;
begin
	FormStyle := fsMDIChild;
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
    Notebook.PageIndex := 0;
    Self.Caption := STR_CAPTION;
    DB_CRUDCommonProcs.FillVariantItemsFromStoredProc(DMConnections.BaseCAC, 'PersoneriasCobranzasRefinanciaciones_Listar',
                                                      txtConvenioClienteTipo.Items,
                                                      'CodigoTipoCliente', 'Descripcion', [], [], Self.Caption, True);
    txtConvenioClienteTipo.Value := -1;
    DB_CRUDCommonProcs.FillVariantItemsFromStoredProc(DMConnections.BaseCAC, 'TiposJudicialesEmpresasCobranza_Listar', txtTipoJudicialEmpresasCobranza.Items,
                                           'CodigoTipoJudicialEmpresasCobranza', 'Descripcion', [], [], Self.Caption, True);
    txtConvenioClienteTipo.Value := -1;
    DB_CRUDCommonProcs.FillVariantItemsFromStoredProc(DMConnections.BaseCAC, 'TiposDeOperacionEmpresasCobranza_Listar', txtTipoDeOperacionEmpresasCobranza.Items,
                                           'CodigoTipoDeOperacionEmpresasCobranza', 'Descripcion', [], [], Self.Caption, True);
    txtTipoDeOperacionEmpresasCobranza.Value := -1;
    //
    GrupoDatos.Enabled := False;
    //OK
	Result := FrameDomicilio1.Inicializa();
    if Result then begin
        FrameTelefono1.Inicializa('Tel�fono principal:', true, true, false);
        //
        CodigoActual := 0;
        if not OpenTables([spListar]) then exit;
	    DBListDatos.Reload;
        ActiveControl := DBListDatos;
    end;
    FillChar(FDomicilioEmpresa, SizeOf(TDatosDomicilio), #0);
end;

{-----------------------------------------------------------------------------
  Function Name: LimpiarCampos
  Author:    aunanue
  Date:      09-03-2017
  Description:
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TAbmEmpresasDeCobranzaForm.LimpiarCampos(usarCeros: boolean);
begin
	txtCodigo.Clear;
    txtNombre.Clear;
    txtAlias.Clear;
    txtDescripcion.Clear;
    txtRUT.Clear;
    txtConvenioClienteTipo.Value := -1;
    txtTipoJudicialEmpresasCobranza.Value := -1;
    txtTipoDeOperacionEmpresasCobranza.Value := -1;
    if usarCeros then begin
        txtComisionPorAviso.Value    := 0;
        txtComisionPorCobranza.Value := 0;
        txtEdadDeLaDeuda.ValueInt    := 0;
        txtMontoMinimo.Value  := 0;
        txtMontoMaximo.Value  := 1;
        txtCapacidadDeProceso.ValueInt := 1;
    end else begin
        txtComisionPorAviso.Clear;
        txtComisionPorCobranza.Clear;
        txtEdadDeLaDeuda.Clear;
        txtMontoMinimo.Clear;
        txtMontoMaximo.Clear;
        txtCapacidadDeProceso.Clear;
    end;
    FrameTelefono1.Limpiar;
    txtEmail.Clear;
    FrameDomicilio1.Limpiar;
end;

procedure TAbmEmpresasDeCobranzaForm.txtRUTKeyPress(Sender: TObject;
  var Key: Char);
begin
    Key := PeaProcs.CaracteresRut(Key);
end;

{-----------------------------------------------------------------------------
  Function Name: FormShow
  Author:    aunanue
  Date:      09-03-2017
  Description: Actualizo la grilla
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TAbmEmpresasDeCobranzaForm.FormShow(Sender: TObject);
begin
	DBListDatos.Reload;
end;

{-----------------------------------------------------------------------------
  Function Name: DBListDatosRefresh
  Author:    aunanue
  Date:      09-03-2017
  Description: lipio los campos si la tabla esta vacia
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TAbmEmpresasDeCobranzaForm.DBListDatosRefresh(Sender: TObject);
begin
	 if DBListDatos.Empty then LimpiarCampos(false);
end;

{-----------------------------------------------------------------------------
  Function Name: VolverCampos
  Author:    aunanue
  Date:      09-03-2017
  Description:
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TAbmEmpresasDeCobranzaForm.VolverCampos;
begin
	DBListDatos.Estado  := Normal;
	DBListDatos.Enabled := True;
	ActiveControl       := DBListDatos;
	Notebook.PageIndex  := 0;
	GrupoDatos.Enabled  := False;
	DBListDatos.Reload;
    Screen.Cursor := crDefault;
end;

{-----------------------------------------------------------------------------
  Function Name: DBListDatosDrawItem
  Author:    aunanue
  Date:      09-03-2017
  Description: Muestro los Registros
  Parameters: Sender: TDBList; Tabla: TDataSet;Rect: TRect; State: TOwnerDrawState; Cols: TColPositions
  Return Value: None
-----------------------------------------------------------------------------}
procedure TAbmEmpresasDeCobranzaForm.DBListDatosDrawItem(Sender: TDBList; Tabla: TDataSet;Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, Istr(Tabla.FieldbyName(NOMBRE_CAMPO_CODIGO).AsInteger, 10));
        TextOut(Cols[1], Rect.Top, Trim(Tabla.FieldbyName('Nombre').AsString));
        TextOut(Cols[2], Rect.Top, Trim(Tabla.FieldbyName('Alias').AsString));
        TextOut(Cols[3], Rect.Top, Trim(Tabla.FieldbyName('Descripcion').AsString));
	end;
end;

function TAbmEmpresasDeCobranzaForm.DatosADomicilioEmpresa(DesdeRecord: boolean; CodigoDomicilio: integer): TDatosDomicilio;
begin
    //Para las empresas est� hardcoded el tipo de domicilio: 2	Comercial / Documentacion
    FDomicilioEmpresa.CodigoTipoDomicilio := 2;
    FDomicilioEmpresa.CodigoDomicilio := CodigoDomicilio;
    FDomicilioEmpresa.CodigoPais := PAIS_CHILE;
    FDomicilioEmpresa.CodigoRegion := iif(DesdeRecord, Trim(DBListDatos.Table.FieldByName('CodigoRegion').AsString), FrameDomicilio1.Region);
    FDomicilioEmpresa.CalleDesnormalizada := iif(DesdeRecord, Trim(DBListDatos.Table.FieldByName('CalleDesnormalizada').AsString), FrameDomicilio1.DescripcionCalle);
    FDomicilioEmpresa.NumeroCalleSTR := iif(DesdeRecord, Trim(DBListDatos.Table.FieldByName('NumeroCalle').AsString), FrameDomicilio1.Numero);
    FDomicilioEmpresa.Dpto := iif(DesdeRecord, Trim(DBListDatos.Table.FieldByName('Dpto').AsString), FrameDomicilio1.Depto);
    FDomicilioEmpresa.CodigoComuna := iif(DesdeRecord, Trim(DBListDatos.Table.FieldByName('CodigoComuna').AsString), FrameDomicilio1.Comuna);
    FDomicilioEmpresa.CodigoPostal := iif(DesdeRecord, Trim(DBListDatos.Table.FieldByName('CodigoPostal').AsString), FrameDomicilio1.CodigoPostal);
end;
{-----------------------------------------------------------------------------
  Function Name: DBListDatosClick
  Author:    aunanue
  Date:      09-03-2017
  Description: Selecciono un campo
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TAbmEmpresasDeCobranzaForm.DBListDatosClick(Sender: TObject);
begin
    if CodigoActual = DBListDatos.Table.FieldByName(NOMBRE_CAMPO_CODIGO).AsInteger then begin
        //hay recargas: telefono y calle que van a la DB.
        //Por lo tanto si es el mismo ID no refresco la pantalla.
        Exit;
    end;
    with (Sender AS TDbList).Table do begin
        CodigoActual := FieldByName(NOMBRE_CAMPO_CODIGO).AsInteger;
	    txtCodigo.Value	    := FieldByName(NOMBRE_CAMPO_CODIGO).AsInteger;
        txtNombre.Text      := FieldByName('Nombre').AsString;
        txtAlias.Text       := FieldByName('Alias').AsString;
        txtDescripcion.Text := FieldByName('Descripcion').AsString;
        txtRUT.Text := FieldByName('RUT').AsString;
        txtConvenioClienteTipo.Value := FieldByName('CodigoTipoCliente').AsInteger;
        txtTipoJudicialEmpresasCobranza.Value := FieldByName('CodigoTipoJudicialEmpresasCobranza').AsInteger;
        txtTipoDeOperacionEmpresasCobranza.Value := FieldByName('CodigoTipoDeOperacionEmpresasCobranza').AsInteger;
        txtComisionPorAviso.Value    := FieldByName('ComisionPorAviso').AsFloat;
        txtComisionPorCobranza.Value := FieldByName('ComisionPorCobranza').AsFloat;
        txtEdadDeLaDeuda.ValueInt    := FieldByName('EdadDeuda').AsInteger;
        txtMontoMinimo.Value  := FieldByName('MontoMinimo').AsFloat;
        txtMontoMaximo.Value  := FieldByName('MontoMaximo').AsFloat;
        txtCapacidadDeProceso.ValueInt := FieldByName('CapacidadDeProceso').AsInteger;
        //Telefono
        FrameTelefono1.CargarTelefono(FieldByName('CodigoArea').AsInteger, FieldByName('NumeroTelefono').AsString, -1,
                                      FieldByName('HorarioDesde').AsDateTime,
                                      FieldByName('HorarioHasta').AsDateTime);
        //Email
        txtEmail.Text := FieldByName('Email').AsString;
        //Domicilio
        DatosADomicilioEmpresa(true, FieldByName('CodigoDomicilio').AsInteger);
        FrameDomicilio1.CargarDatosDomicilio(FDomicilioEmpresa);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: DBListDatosProcess
  Author:    aunanue
  Date:      09-03-2017
  Description:
  Parameters: Tabla: TDataSet;var Texto: String
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TAbmEmpresasDeCobranzaForm.DBListDatosProcess(Tabla: TDataSet;
  var Texto: string): Boolean;
begin
    Texto := Tabla.FieldByName('Nombre').AsString;
	Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: DBListDatosInsert
  Author:    aunanue
  Date:      09-03-2017
  Description: limpio los campos para que ingrese un nuevo registro
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TAbmEmpresasDeCobranzaForm.DBListDatosInsert(Sender: TObject);
begin
    LimpiarCampos(true);
    CodigoActual := 0;
    GrupoDatos.Enabled  := True;
	DBListDatos.Enabled := False;
	Notebook.PageIndex  := 1;
	ActiveControl       := txtNombre;
end;

{-----------------------------------------------------------------------------
  Function Name: DBListDatosEdit
  Author:    aunanue
  Date:      09-03-2017
  Description:
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TAbmEmpresasDeCobranzaForm.DBListDatosEdit(Sender: TObject);
begin
    DBListDatos.Enabled := False;
	DBListDatos.Estado  := modi;
	Notebook.PageIndex  := 1;
	GrupoDatos.Enabled  := True;
	ActiveControl := txtNombre;
end;

{-----------------------------------------------------------------------------
  Function Name: DBListDatosDelete
  Author:    aunanue
  Date:      09-03-2017
  Description: elimino un item
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TAbmEmpresasDeCobranzaForm.DBListDatosDelete(Sender: TObject);
begin
    if MsgBox(Format(MSG_QUESTION_ELIMINAR,[STR_CAPTION]), STR_CONFIRMACION, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
        Screen.Cursor := crHourGlass;
        try
            spEliminar.Connection.BeginTrans;
            try
                AddParameter_Codigo(NOMBRE_CAMPO_CODIGO, spEliminar.Parameters, txtCodigo.ValueInt);
                AddParameter_CodigoUsuario(spEliminar.Parameters);
                AddParameter_Resultado(spEliminar.Parameters);
                spEliminar.ExecProc;
                if spEliminar.Parameters.ParamByName('@Resultado').Value > 0 then begin
                    spEliminar.Connection.CommitTrans;
                    CodigoActual := 0;
                end else begin
                    spEliminar.Connection.RollbackTrans;
                    MsgBox(MSG_EXISTEN_DATOS_RELACIONADOS_BORRANDO, STR_CAPTION, MB_ICONSTOP);
                end;
            except
                On E: Exception do begin
                    if spEliminar.Connection.InTransaction then
                        spEliminar.Connection.RollbackTrans;
                    MsgBoxErr(Format(MSG_ERROR_ELIMINAR,[STR_CAPTION]), e.message, Format(MSG_CAPTION_ELIMINAR,[STR_CAPTION]), MB_ICONSTOP);
                end
            end
        finally
            VolverCampos();
	        Screen.Cursor := crDefault;
    	end;
    end else begin
        DBListDatos.Estado  := Normal;
	    DBListDatos.Enabled := True;
	    ActiveControl       := DBListDatos;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: BtnAceptarClick
  Author:    aunanue
  Date:      09-03-2017
  Description: Acepto los cambios
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TAbmEmpresasDeCobranzaForm.BtnAceptarClick(Sender: TObject);
resourcestring
    TXT_MAYOR_QUE_EL = 'El %s debe ser mayor que %s.';
    TXT_MAYOR_IGUAL_QUE_EL = 'El %s debe ser mayor o igual que %s.';
    TXT_MAYOR_IGUAL_QUE_CERO_LA = 'La %s debe ser mayor o igual que cero.';
var
    BookM: TBookmark;
    Result, Duplicado: Boolean;
    i, CodigoTelefono, CodigoEmail, CodigoDomicilio: Integer;
    TextoMontoMinimo: string;
begin

{$REGION 'ValidateControls'}
    TextoMontoMinimo := FindFocusLabelText(Self, txtMontoMinimo, '');
  	if not ValidateControls(
        [txtNombre,
         txtAlias,
         txtDescripcion,
         txtRUT,
         txtConvenioClienteTipo,
         txtTipoDeOperacionEmpresasCobranza,
         txtComisionPorAviso,
         txtComisionPorCobranza,
         txtEdadDeLaDeuda,
         txtMontoMinimo,
         txtMontoMaximo,
         txtCapacidadDeProceso],

	    [Trim(txtNombre.Text) <> '',
         Trim(txtAlias.Text) <> '',
         Trim(txtDescripcion.Text) <> '',
         Peaprocs.ValidarRut(Trim(txtRUT.Text)),
         txtConvenioClienteTipo.ItemIndex > 0,
         txtTipoDeOperacionEmpresasCobranza.ItemIndex > 0,
         txtComisionPorAviso.Value >= 0,
         txtComisionPorCobranza.Value >= 0,
         txtEdadDeLaDeuda.Value >= 0,
         txtMontoMinimo.ValueInt >= 0,
         txtMontoMaximo.ValueInt > txtMontoMinimo.ValueInt,
         txtCapacidadDeProceso.ValueInt >= 1],

	     Format(MSG_CAPTION_ACTUALIZAR, [STR_CAPTION]),

        [Format(MSG_VALIDAR_DEBE_EL, [FindFocusLabelText(Self, txtNombre)]),
         Format(MSG_VALIDAR_DEBE_EL, [FindFocusLabelText(Self, txtAlias)]),
         Format(MSG_VALIDAR_DEBE_LA, [FindFocusLabelText(Self, txtDescripcion)]),
         rsRutInvalido,
         Format(MSG_VALIDAR_SELECCIONAR_UNO, [FindFocusLabelText(Self, txtConvenioClienteTipo)]),
         Format(MSG_VALIDAR_SELECCIONAR_UNO, [FindFocusLabelText(Self, txtTipoDeOperacionEmpresasCobranza)]),
         Format(TXT_MAYOR_IGUAL_QUE_CERO_LA, [FindFocusLabelText(Self, txtComisionPorAviso, '')]),
         Format(TXT_MAYOR_IGUAL_QUE_CERO_LA, [FindFocusLabelText(Self, txtComisionPorCobranza, '')]),
         Format(TXT_MAYOR_IGUAL_QUE_CERO_LA, [FindFocusLabelText(Self, txtEdadDeLaDeuda, '')]),
         Format(TXT_MAYOR_IGUAL_QUE_EL, [TextoMontoMinimo, '0']),
         Format(TXT_MAYOR_QUE_EL, [FindFocusLabelText(Self, txtMontoMaximo, ''), TextoMontoMinimo]),
         Format(TXT_MAYOR_IGUAL_QUE_EL, [FindFocusLabelText(Self, txtCapacidadDeProceso, ''), '1'])
         ]) then begin
		Exit;
	end;
    //Medios de comunicaci�n
    if not FrameTelefono1.ValidarTelefono then exit;
    if Trim(txtEmail.Text) <> '' then begin
        if not IsValidEMailList(Trim(txtEmail.Text)) then begin
            MsgBoxBalloon(MSG_VALIDAR_DIRECCION_EMAIL, Format(MSG_CAPTION_ACTUALIZAR, [STR_CAPTION]), MB_ICONSTOP, txtEmail);
		    txtEmail.SetFocus;
            Exit;
        end;
    end;
    //Direcci�n
    if not FrameDomicilio1.ValidarDomicilio then exit;
    
{$ENDREGION}

    Screen.Cursor := crHourGlass;
    Result := False;
    Duplicado := False;
    try
        //Validar Duplicados
        BookM := DBListDatos.Table.GetBookmark;
        try
            DBListDatos.Table.First;
            for i := 0 to DBListDatos.Table.RecordCount - 1 do begin
                if DBListDatos.Table.FieldByName(NOMBRE_CAMPO_CODIGO).AsString <> txtCodigo.Text then begin
                    if SysUtils.CompareText(Trim(DBListDatos.Table.FieldByName('Nombre').AsString), Trim(txtNombre.Text)) = 0 then begin
                        MsgBox(Format(MSG_ERROR_REGISTRO_DUPLICADO, [Trim(txtNombre.Text)]), STR_CAPTION, MB_ICONSTOP);
                        Duplicado := True;
                        break;
                    end;
                end;
                DBListDatos.Table.Next;
            end;
            DBListDatos.Table.GotoBookmark(BookM);
        finally
            DBListDatos.Table.FreeBookmark(BookM);
        end;

        if Duplicado then
            Exit;
        //
        spAlmacenar.Connection.BeginTrans;
        try
            //Primero almacenamos el Telefono
            CodigoTelefono := iif(txtCodigo.ValueInt = 0, 0, DBListDatos.Table.FieldByName('CodigoTelefono').AsInteger);
            AddParameter_Telefono(spTelefonos_Almacenar.Parameters, CodigoTelefono,
                                  FrameTelefono1.CodigoArea, FrameTelefono1.NumeroTelefono,
                                  FrameTelefono1.HorarioDesde, FrameTelefono1.HorarioHasta, false);
            spTelefonos_Almacenar.ExecProc;
            CodigoTelefono := spTelefonos_Almacenar.Parameters.ParamByName('@Resultado').Value;
            //Ahora el Email
            if Trim(txtEmail.Text) <> '' then begin
                CodigoEmail := iif(txtCodigo.ValueInt = 0, 0, DBListDatos.Table.FieldByName('CodigoEmail').AsInteger);
                AddParameter_EmailContacto(spEmailsContacto_Almacenar.Parameters, CodigoEmail, Trim(txtEmail.Text), false);
                spEmailsContacto_Almacenar.ExecProc;
                CodigoEmail := spEmailsContacto_Almacenar.Parameters.ParamByName('@Resultado').Value;
            end else begin
                //No puso un email
                CodigoEmail := 0;
            end;
            //Ahora el Domicilio
            CodigoDomicilio := iif(txtCodigo.ValueInt = 0, 0, DBListDatos.Table.FieldByName('CodigoDomicilio').AsInteger);
            DatosADomicilioEmpresa(false, CodigoDomicilio);
            AddParameter_Domicilio(spActualizarDomicilio.Parameters, FDomicilioEmpresa);
            spActualizarDomicilio.ExecProc;
            CodigoDomicilio := spActualizarDomicilio.Parameters.ParamByName('@CodigoDomicilio').Value;
            //Finalmente el resto de los datos
            AddParameter_Codigo(NOMBRE_CAMPO_CODIGO, spAlmacenar.Parameters, txtCodigo.ValueInt);
            AddParameter_Nombre(spAlmacenar.Parameters, Trim(txtNombre.Text));
            AddParameter(spAlmacenar.Parameters, '@Alias', ftString, pdInput, Trim(txtAlias.Text), 255);
            AddParameter_Descripcion(spAlmacenar.Parameters, Trim(txtDescripcion.Text));
            AddParameter(spAlmacenar.Parameters, '@RUT', ftString, pdInput, Trim(txtRUT.Text), 255);
            AddParameter(spAlmacenar.Parameters, '@CodigoTipoCliente', ftInteger, pdInput, txtConvenioClienteTipo.Value);
            AddParameter(spAlmacenar.Parameters, '@CodigoTipoJudicialEmpresasCobranza', ftInteger, pdInput, txtTipoJudicialEmpresasCobranza.Value);
            AddParameter(spAlmacenar.Parameters, '@CodigoTipoDeOperacionEmpresasCobranza', ftInteger, pdInput, txtTipoDeOperacionEmpresasCobranza.Value);
            AddParameter(spAlmacenar.Parameters, '@ComisionPorAviso', ftFloat, pdInput, txtComisionPorAviso.Value);
            AddParameter(spAlmacenar.Parameters, '@ComisionPorCobranza', ftFloat, pdInput, txtComisionPorCobranza.Value);
            AddParameter(spAlmacenar.Parameters, '@EdadDeuda', ftInteger, pdInput, txtEdadDeLaDeuda.ValueInt);
            AddParameter(spAlmacenar.Parameters, '@MontoMinimo', ftFloat, pdInput, txtMontoMinimo.Value);
            AddParameter(spAlmacenar.Parameters, '@MontoMaximo', ftFloat, pdInput, txtMontoMaximo.Value);
            AddParameter(spAlmacenar.Parameters, '@CapacidadDeProceso', ftInteger, pdInput, txtCapacidadDeProceso.ValueInt);
            AddParameter_CodigoTelefono(spAlmacenar.Parameters, CodigoTelefono);
            AddParameter_CodigoEmail(spAlmacenar.Parameters, CodigoEmail);
            AddParameter_CodigoDomicilio(spAlmacenar.Parameters, CodigoDomicilio);
            AddParameter_CodigoUsuario(spAlmacenar.Parameters);
            AddParameter_Resultado(spAlmacenar.Parameters);
            spAlmacenar.ExecProc;
            spAlmacenar.Connection.CommitTrans;
            CodigoActual := 0;
            Result := True;
        except
            On E: Exception do begin
                if spAlmacenar.Connection.InTransaction then
                    spAlmacenar.Connection.RollbackTrans;
                //
                MsgBoxErr(Format(MSG_ERROR_ACTUALIZAR,[STR_CAPTION]), e.message, Format(MSG_CAPTION_ACTUALIZAR,[STR_CAPTION]), MB_ICONSTOP);
            end;
        end;
    finally
        if (Result) then begin
            VolverCampos;
        end;
        Screen.Cursor := crDefault;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: BtnCancelarClick
  Author:    aunanue
  Date:      09-03-2017
  Description: permito cancelar
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TAbmEmpresasDeCobranzaForm.BtnCancelarClick(Sender: TObject);
begin
    VolverCampos;
end;

{-----------------------------------------------------------------------------
  Function Name: AbmToolbar1Close
  Author:    aunanue
  Date:      09-03-2017
  Description: salgo del formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TAbmEmpresasDeCobranzaForm.AbmToolbarClose(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name: BtnSalirClick
  Author:    aunanue
  Date:      09-03-2017
  Description:  salgo del formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TAbmEmpresasDeCobranzaForm.BtnSalirClick(Sender: TObject);
begin
    AbmToolbarClose(BtnSalir);
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    aunanue
  Date:      09-03-2017
  Description: lo libero de memoria
  Parameters: Sender: TObject; var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TAbmEmpresasDeCobranzaForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

end.
