{-----------------------------------------------------------------------------
 File Name: Frm_CancelacionNotasCredito.pas
 Author:    Modificado por Flamas
 Date Created: 25/01/2005
 Language: ES-AR
 Description:
 Revision : 2
    Date: 24/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura
-----------------------------------------------------------------------------}

unit FrmPagoNotasCredito;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ListBoxEx, DBListEx, StdCtrls, DPSControls, VariantComboBox,
  DmiCtrls, DB, ADODB, DMCOnnection, utilDB, UtilFacturacion, utilProc, buscaClientes,
  PeaTypes, RStrings, DBCtrls, peaProcs, Util, PagoVentanilla, Validate,
  DateEdit;

const
	KEY_F9 	= VK_F9;
type
    Tfrm_PagoNotasCredito = class(TForm)
    gbPorCliente: TGroupBox;
    lb_CodigoCliente: TLabel;
    Label5: TLabel;
    peNumeroDocumento: TPickEdit;
    cbConvenios: TVariantComboBox;
    GroupBox1: TGroupBox;
    Label6: TLabel;
    lblNombre: TLabel;
    lblDomicilio: TLabel;
    Label10: TLabel;
    gbDatosComprobante: TGroupBox;
    Label2: TLabel;
    lblFecha: TLabel;
    lblFechaVencimiento: TLabel;
    Label7: TLabel;
    Label1: TLabel;
    lblTotalAPagar: TLabel;
    Label12: TLabel;
    ObtenerNotasCreditoImpagas: TADOStoredProc;
    cbComprobantes: TComboBox;
    Label4: TLabel;
    edNumeroComprobante: TNumericEdit;
    lblPagado: TLabel;
    Label11: TLabel;
    CerrarPagoComprobante: TADOStoredProc;
    Label3: TLabel;
    deFechaPago: TDateEdit;
    Label8: TLabel;
    lblPagoAutorizado: TLabel;
    cbTiposComprobantes: TVariantComboBox;
    Label9: TLabel;
    sp_RegistrarUnDetallePagoComprobante: TADOStoredProc;
    sp_RegistrarUnPagoComprobante: TADOStoredProc;
    btnPago: TButton;
    btnSalir: TButton;
    procedure peNumeroDocumentoChange(Sender: TObject);
    procedure cbConveniosChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure peNumeroDocumentoButtonClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cbComprobantesChange(Sender: TObject);
    procedure cbTiposComprobantesChange(Sender: TObject);
    procedure edNumeroComprobanteChange(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btnPagoClick(Sender: TObject);
  private
    { Private declarations }
    FNumeroComprobante: Int64;
    FUltimaBusqueda: TBusquedaCliente;
    FTotalApagar: Double;
    FPuntoEntrega, FPuntoVenta: Integer;
    procedure MostrarDatosCliente(CodigoCLiente: Integer);
    procedure MostrarDatosComprobante;
	function  CargarComprobantes : boolean;
    procedure LimpiarDetalles;
    function  RegistrarPago : Boolean;
    function ValidarDatosTurno: Boolean;
	procedure CargarTiposComprobantes;
  public
    { Public declarations }
    function Inicializar(PuntoEntrega, PuntoVenta: Integer): Boolean; overload;
    //function Inicializar( NotaCredito : Int64): Boolean; Overload;
  end;

var
  frm_PagoNotasCredito: Tfrm_PagoNotasCredito;

implementation

{$R *.dfm}
{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    flamas
  Date Created: 21/12/2004
  Description: Inicializa el Form - Carga los tipos de Comprobantes
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
function Tfrm_PagoNotasCredito.Inicializar(PuntoEntrega, PuntoVenta: Integer): Boolean;
begin
    FPuntoEntrega := PuntoEntrega;
    FPuntoVenta := PuntoVenta;
    
	// Por ahora lo cargamos a mano
    lblNombre.Caption := '';
    lblDomicilio.Caption := '';
    lblPagado.Caption := '';
    lblPagoAutorizado.Caption := '';
    deFechaPago.Date := StrToDate(DateToStr(NowBase(DMConnections.BaseCAC)));
	CargarTiposComprobantes;
    MostrarDatosComprobante;
    Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarTiposComprobantes
  Author:    flamas
  Date Created: 09/06/2005
  Description:
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure Tfrm_PagoNotasCredito.CargarTiposComprobantes;
begin
	cbTiposComprobantes.Items.Clear;
    cbTiposComprobantes.Items.Add(DescriTipoComprobante(TC_NOTA_CREDITO), TC_NOTA_CREDITO);
    cbTiposComprobantes.Items.Add(DescriTipoComprobante(TC_NOTA_CREDITO_A_COBRO), TC_NOTA_CREDITO_A_COBRO);
	cbTiposComprobantes.Value := TC_NOTA_CREDITO;
end;

procedure Tfrm_PagoNotasCredito.FormCreate(Sender: TObject);
begin
    CenterForm(Self);
end;

{-----------------------------------------------------------------------------
  Function Name: peNumeroDocumentoChange
  Author:    flamas
  Date Created: 20/12/2004
  Description: Actualiza los Convenios y Comprobantes Impagos de un cliente determinado
  Parameters: Sender: TObject
  Return Value: None
Revision 2:
      Date: 24/02/2009
      Author: mpiazza
      Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
        los bloqueos de tablas en la lectura
-----------------------------------------------------------------------------}
procedure Tfrm_PagoNotasCredito.peNumeroDocumentoChange(Sender: TObject);
Var
    CodigoCliente: Integer;
begin
    if ActiveControl <> Sender then Exit;

    edNumeroComprobante.Clear;
    CodigoCliente := QueryGetValueInt(DMConnections.BaseCAC, Format(
      'SELECT CodigoPersona FROM Personas  WITH (NOLOCK) WHERE CodigoDocumento = ''RUT'' ' +
      'AND NumeroDocumento = ''%s''', [iif(peNumeroDocumento.Text <> '',
      									trim(PadL(peNumeroDocumento.Text, 9, '0')), '')]));
    if CodigoCliente > 0 then
        CargarConveniosRUT(DMConnections.BaseCAC, cbConvenios, 'RUT', peNumeroDocumento.Text, False, 0)
    else begin
    	cbConvenios.Clear;
    end;
    if cbConvenios.Items.Count > 0 then begin
        cbConvenios.ItemIndex := 0;
    end;

    MostrarDatosCliente(CodigoCliente);

    CargarComprobantes;
end;

{-----------------------------------------------------------------------------
  Function Name: cbConveniosChange
  Author:    flamas
  Date Created: 21/12/2004
  Description: Procesa un Cambio de Convenio - Trae los comprobantes impagos
  				del dicho Convenio
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure Tfrm_PagoNotasCredito.cbConveniosChange(Sender: TObject);
begin
    if ActiveControl <> Sender then Exit;
    Cargarcomprobantes;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarComprobantes
  Author:    flamas
  Date Created: 20/12/2004
  Description: Carga los comprobantes impagos de un Convenio determinado
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
function Tfrm_PagoNotasCredito.CargarComprobantes : boolean;
resourcestring
    MSG_NOT_PAID = 'No';
Var
   Comprobante: TNotaCredito;
begin
    ObtenerNotasCreditoImpagas.Close;
	ObtenerNotasCreditoImpagas.Parameters.ParamByName('@CodigoConvenio').Value := cbConvenios.Value;
	ObtenerNotasCreditoImpagas.Parameters.ParamByName('@TipoComprobante').Value := cbTiposComprobantes.Value;
	ObtenerNotasCreditoImpagas.Parameters.ParamByName('@NumeroComprobante').Value :=
		iif( edNumeroComprobante.Text <> '', edNumeroComprobante.Value, null );

    ObtenerNotasCreditoImpagas.Open;

    // Cargamos la lista de comprobantes.
    ObtenerNotasCreditoImpagas.disableControls;
    cbComprobantes.Items.Clear;
    try
        while not ObtenerNotasCreditoImpagas.eof do begin
        	Comprobante := TNotaCredito.Create;
            Comprobante.CodigoCliente		:= ObtenerNotasCreditoImpagas.FieldByName('CodigoPersona').AsInteger;
           	Comprobante.TipoComprobante     := ObtenerNotasCreditoImpagas.FieldByName('TipoComprobante').AsString;
            Comprobante.NumeroComprobante   := ObtenerNotasCreditoImpagas.FieldByName('NumeroComprobante').AsInteger;
            Comprobante.FechaEmision        := ObtenerNotasCreditoImpagas.FieldByName('FechaEmision').AsDateTime;
            Comprobante.FechaVencimiento    := ObtenerNotasCreditoImpagas.FieldByName('FechaVencimiento').AsDateTime;
            Comprobante.TotalComprobante    := ObtenerNotasCreditoImpagas.FieldByName('TotalComprobante').AsFloat;
            Comprobante.TotalAPagar         := ObtenerNotasCreditoImpagas.FieldByName('TotalAPagar').AsFloat;
            Comprobante.Pagado				:= (ObtenerNotasCreditoImpagas.FieldByName( 'EstadoPago' ).AsString = 'P');
            Comprobante.PagoAutorizado		:= ObtenerNotasCreditoImpagas.FieldByName('PagoAutorizado').AsBoolean;
            Comprobante.Autorizo			:= ObtenerNotasCreditoImpagas.FieldByName('Autorizo').AsString;
            Comprobante.FechaAutorizacion	:= ObtenerNotasCreditoImpagas.FieldByName('FechaAutorizacion').AsDateTime;
            
            cbComprobantes.AddItem(ObtenerNotasCreditoImpagas.FieldByName('DescriComprobante').AsString, Comprobante);

            if (TNotaCredito(cbComprobantes.Items.Objects[cbComprobantes.Items.Count - 1]).TipoComprobante = 'NC')
          	  and (TNotaCredito(cbComprobantes.Items.Objects[cbComprobantes.Items.Count - 1]).NumeroComprobante = FNumeroComprobante) then
				cbComprobantes.ItemIndex := cbComprobantes.Items.Count - 1;

            ObtenerNotasCreditoImpagas.next;
        end;

		lblNombre.Caption := '';
        lblDomicilio.Caption := '';
        LimpiarDetalles;

    	LimpiarDetalles;
        if cbComprobantes.Items.Count = 0 then begin
             Result := false;
             Exit;
        end else begin
            cbComprobantes.ItemIndex := 0;
            lblPagado.Caption := MSG_NOT_PAID;
        end;
        MostrarDatosCliente( TNotaCredito(cbComprobantes.Items.Objects[cbComprobantes.itemIndex]).CodigoCliente);
        MostrarDatosComprobante;
        Result := true;
    finally
        ObtenerNotasCreditoImpagas.EnableControls;
        cbComprobantesChange(cbComprobantes);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: MostrarDatosCliente
  Author:    flamas
  Date Created: 20/12/2004
  Description:  Muestra los datos del Cliente
  Parameters: CodigoCliente: Integer
  Return Value: None
-----------------------------------------------------------------------------}
procedure Tfrm_PagoNotasCredito.MostrarDatosCliente(CodigoCliente: Integer);
begin
    // Cargamos la informaci�n del cliente.
    lblNombre.Caption := QueryGetValue(DMConnections.BaseCAC, Format(
      'SELECT dbo.ObtenerNombrePersona(%d)', [CodigoCliente]));
    lblDomicilio.Caption := QueryGetValue(DMConnections.BaseCAC, Format(
      'SELECT dbo.ObtenerDescripcionDomicilioPrincipalPersona(%d)', [CodigoCliente]));
end;

{-----------------------------------------------------------------------------
  Function Name: MostrarDatosComprobante
  Author:    flamas
  Date Created: 20/12/2004
  Description: Muestra los datos del Comprobante
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure Tfrm_PagoNotasCredito.MostrarDatosComprobante;
resourcestring
	MSG_YES 			= 'Si';
    MSG_NO 				= 'No';
    MSG_AUTHORIZED 		= 'Autorizado';
    MSG_NOT_AUTHORIZED 	= 'No Autorizado';
begin
    if ( cbComprobantes.Items.Count > 0 ) and
       ( cbComprobantes.ItemIndex >= 0 )then begin
        lblFecha.Caption            := FormatDateTime('dd/mm/yyyy', (TNotaCredito(cbComprobantes.Items.Objects[cbComprobantes.itemIndex]).FechaEmision));
        lblFechaVencimiento.Caption := FormatDateTime('dd/mm/yyyy', (TNotaCredito(cbComprobantes.Items.Objects[cbComprobantes.itemIndex]).FechaVencimiento));
        lblTotalAPagar.Caption      := FormatFloat(FORMATO_IMPORTE , (TNotaCredito(cbComprobantes.Items.Objects[cbComprobantes.itemIndex]).TotalAPagar));
        FTotalApagar 				:= Round(TNotaCredito(cbComprobantes.Items.Objects[cbComprobantes.itemIndex]).TotalAPagar);
        lblPagado.Caption 			:= iif( TNotaCredito(cbComprobantes.Items.Objects[cbComprobantes.itemIndex]).Pagado, MSG_YES, MSG_NO );
        lblPagoAutorizado.Caption	:= iif( TNotaCredito(cbComprobantes.Items.Objects[cbComprobantes.itemIndex]).PagoAutorizado,
        								MSG_AUTHORIZED, MSG_NOT_AUTHORIZED) +
                                        iif(TNotaCredito(cbComprobantes.Items.Objects[cbComprobantes.itemIndex]).Autorizo <> '',
                                        	' - ' + TNotaCredito(cbComprobantes.Items.Objects[cbComprobantes.itemIndex]).Autorizo, '');
        btnPago.Enabled 			:= not TNotaCredito(cbComprobantes.Items.Objects[cbComprobantes.itemIndex]).Pagado and
        									TNotaCredito(cbComprobantes.Items.Objects[cbComprobantes.itemIndex]).PagoAutorizado;
        FNumeroComprobante 			:= TNotaCredito(cbComprobantes.Items.Objects[cbComprobantes.itemIndex]).NumeroComprobante;
    end else LimpiarDetalles;
end;

{-----------------------------------------------------------------------------
  Function Name: LimpiarDetalles;
  Author:    flamas
  Date Created: 26/01/2005
  Description: Borra los detalles del Comprobante
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure Tfrm_PagoNotasCredito.LimpiarDetalles;
begin
	lblFecha.Caption            := '';
    lblFechaVencimiento.Caption	:= '';
    lblTotalAPagar.Caption      := '';
    lblPagado.Caption           := '';
    lblPagado.Caption 			:= '';
    btnPago.Enabled     		:= False;
end;

{-----------------------------------------------------------------------------
  Function Name: peNumeroDocumentoButtonClick
  Author:    flamas
  Date Created: 21/12/2004
  Description: Procesa un cambio en el RUT - Muestra los comprobantes pendientes
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure Tfrm_PagoNotasCredito.peNumeroDocumentoButtonClick(Sender: TObject);
var
    f: TFormBuscaClientes;
begin
    Application.CreateForm(TFormBuscaClientes,F);
	if F.Inicializa(FUltimaBusqueda) then begin
        F.ShowModal;
        if F.ModalResult = idOk then begin
            FUltimaBusqueda := F.UltimaBusqueda;
            peNumeroDocumento.Text := f.Persona.NumeroDocumento;
        end;
	end;
    F.free;
end;

{-----------------------------------------------------------------------------
  Function Name: cbComprobantesChange
  Author:    flamas
  Date Created: 21/12/2004
  Description: Procesa un cambio de comprobante y muestra los datos
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure Tfrm_PagoNotasCredito.cbComprobantesChange(Sender: TObject);
begin
    if ActiveControl <> Sender then Exit;
    MostrarDatosComprobante;
end;

{-----------------------------------------------------------------------------
  Function Name: cbTiposComprobantesChange
  Author:    flamas
  Date Created: 21/12/2004
  Description: Procesa un cambio de tipo de comprobante y muestra los datos
  				del comprobante
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure Tfrm_PagoNotasCredito.cbTiposComprobantesChange(Sender: TObject);
begin
   if ActiveControl <> Sender then Exit;

   if ( edNumeroComprobante.Text <> '' ) then edNumeroComprobanteChange( Sender )
   else CargarComprobantes;
end;

{-----------------------------------------------------------------------------
  Function Name: edNumeroComprobanteChange
  Author:    flamas
  Date Created: 21/12/2004
  Description: Procesa un cambio de comprobante y muestra los datos del comprobante
  Parameters: Sender: TObject
  Return Value: None
  -----------------------------------------------------------------------------}
procedure Tfrm_PagoNotasCredito.edNumeroComprobanteChange(Sender: TObject);
begin
    if ActiveControl <> Sender then Exit;
    CargarComprobantes;
end;

procedure Tfrm_PagoNotasCredito.btnSalirClick(Sender: TObject);
begin
    close;
end;

procedure Tfrm_PagoNotasCredito.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    action := caFree;
end;

procedure Tfrm_PagoNotasCredito.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if (Key = KEY_F9) and btnPago.Enabled then btnPago.Click;
end;

{-----------------------------------------------------------------------------
  Function Name: btnPagoClick
  Author:    flamas
  Date Created: 21/12/2004
  Description: Paga una Nota de Cobro
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure Tfrm_PagoNotasCredito.btnPagoClick(Sender: TObject);
resourcestring
    MSG_ERROR = 'Error';
    MSG_REGISTRO_CORRECTO = 'El pago se ha registrado correctamente';
    MSG_CONFIRMAR_PAGO = 'Confirma registrar este pago ?';
    MSG_CAPTION = 'Registrar Pago';
begin
    if MsgBox(MSG_CONFIRMAR_PAGO, MSG_CAPTION,
      MB_ICONQUESTION or MB_DEFBUTTON2 or MB_YESNO) <> IDYES then exit;

    if not ValidarDatosTurno then Exit;

    DMConnections.BaseCAC.BeginTrans;
    try
    	if RegistrarPago then begin
        	DMConnections.BaseCAC.CommitTrans;
            MsgBox(MSG_REGISTRO_CORRECTO, MSG_CAPTION, MB_ICONINFORMATION );
            Close;
        end;
    except
        on e: Exception do begin
            DMConnections.BaseCAC.RollbackTrans;
            MsgBoxErr(MSG_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Procedure: RegistrarPago
  Author:    flamas
  Date:      26-01-2005
  Arguments:
  Result:    Boolean
-----------------------------------------------------------------------------}
function Tfrm_PagoNotasCredito.RegistrarPago : Boolean;
resourcestring
    MSG_ERROR_PAGO = 'Error registrando el pago de la nota de cr�dito';
    MSG_ERROR_CIERRE = 'Error completando el registro del pago';
    MSG_ERROR = 'Error';
    MSG_DESCRIPCION_PAGO = 'Pago de Nota de Cr�dito';
CONST
    CONST_CANCELACION_NOTA_CREDITO = 34;
begin
    with sp_RegistrarUnDetallePagoComprobante.Parameters do begin
        ParamByName('@CodigoTipoMedioPago').Value := CONST_CANAL_PAGO_VENTANILLA;
        ParamByName('@CodigoFormaPago').Value := CONST_FORMA_PAGO_EFECTIVO;
    end;
    sp_RegistrarUnDetallePagoComprobante.ExecProc;

    try
        with sp_RegistrarUnPagoComprobante.Parameters do begin
            ParamByName('@IDDetallePagoComprobante').Value := sp_RegistrarUnDetallePagoComprobante.Parameters.ParamByName('@IDDetallePagoComprobante').Value;
            ParamByName('@TipoComprobante').Value := TNotaCredito(cbComprobantes.Items.Objects[cbComprobantes.itemIndex]).TipoComprobante;
            ParamByName('@NumeroComprobante').Value := TNotaCredito(cbComprobantes.Items.Objects[cbComprobantes.itemIndex]).NumeroComprobante;
            ParamByName('@FechaHora').Value := deFechaPago.Date;
            ParamByName('@NumeroRecibo').Value := Null;
            ParamByName('@CodigoConceptoPago').Value := CONST_CANCELACION_NOTA_CREDITO;
            ParamByName('@DescripcionPago').Value := MSG_DESCRIPCION_PAGO;
            ParamByName('@Importe').Value := TNotaCredito(cbComprobantes.Items.Objects[cbComprobantes.itemIndex]).TotalAPagar * 100;
            ParamByName('@NumeroTurno').Value := GNumeroTurno;
            ParamByName('@Usuario').Value := UsuarioSistema;
        end;
        sp_RegistrarUnPagoComprobante.ExecProc;
    except
    	on e: Exception do begin
        	raise Exception.Create(MSG_ERROR_PAGO + CRLF + e.Message);
        end;
    end;

   // Por �ltimo, cerramos el comprobante
   try
        with CerrarPagoComprobante.Parameters do begin
            ParamByName('@TipoComprobante').Value 	:= TNotaCredito(cbComprobantes.Items.Objects[cbComprobantes.itemIndex]).TipoComprobante;
            ParamByName('@NumeroComprobante').Value := TNotaCredito(cbComprobantes.Items.Objects[cbComprobantes.itemIndex]).NumeroComprobante;
            ParamByName('@FechaHora').Value		    := deFechaPago.Date;
         end;
         CerrarPagoComprobante.ExecProc;
         Result := True;
    except
        on e: Exception do begin
            raise Exception.Create(MSG_ERROR_CIERRE + CRLF + e.Message);
        end;
    end;
end;

function Tfrm_PagoNotasCredito.ValidarDatosTurno: Boolean;
var
    DatosTurno: TDatosTurno;
begin
    (* Verificar que el turno que se est� por utilizar est� abierto y sea del
    usuario logueado. *)
    if VerTurnoAbierto(DMConnections.BaseCAC, FPuntoEntrega, FPuntoVenta, UsuarioSistema, DatosTurno) then begin
        GNumeroTurno := DatosTurno.NumeroTurno;
        Result := True;
    end else begin
        if (DatosTurno.NumeroTurno <> -1)
                and ((DatosTurno.Estado = TURNO_ESTADO_ABIERTO)
                and (Trim(DatosTurno.CodigoUsuario) <> (UpperCase(UsuarioSistema)))) then begin
            MsgBox(Format(MSG_ERROR_TURNO_ABIERTO_OTRO_USUARIO, [DatosTurno.CodigoUsuario]),
                Self.Caption, MB_ICONSTOP);
        end else begin
            MsgBox(MSG_ERROR_TURNO_CERRADO, Self.Caption, MB_ICONSTOP);
        end;
        Result := False;
    end;

end;

end.
