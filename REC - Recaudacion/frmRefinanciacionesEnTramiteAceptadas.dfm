object RefinanciacionesEnTramiteAceptadasForm: TRefinanciacionesEnTramiteAceptadasForm
  Left = 0
  Top = 0
  ActiveControl = btnCancelar
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = ' Refinanciaciones - [Rut: %s - %s]'
  ClientHeight = 218
  ClientWidth = 782
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnKeyDown = FormKeyDown
  DesignSize = (
    782
    218)
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 768
    Height = 204
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = '  Refinanciaciones en Tr'#225'mite y/o Aceptadas  '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    ExplicitWidth = 669
    DesignSize = (
      768
      204)
    object Label1: TLabel
      Left = 74
      Top = 23
      Width = 572
      Height = 13
      Caption = 
        'El Cliente Seleccionado tiene las siguientes Refinanciaciones en' +
        ' Tr'#225'mite y/o Apceptadas. Indique si desea proseguir con'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Image1: TImage
      Left = 12
      Top = 20
      Width = 32
      Height = 32
      Picture.Data = {
        07544269746D6170360C0000424D360C00000000000036000000280000002000
        0000200000000100180000000000000C0000C40E0000C40E0000000000000000
        0000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFBFBFBF2F2F2EAEAEAE9E9
        E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9EAEAEAF2F2F2FB
        FBFBFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFF2F2F2D4D4D4C0C0C0BCBC
        BCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCC0C0C0D4D4D4F2
        F2F2FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFEAEAEABB964CB67E0EB47B
        09B47A07B47A07B47A07B47A07B47A07B47A07B47A07B47B09B67E0EBB964CEA
        EAEAFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFE9E9E9B67E0EFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB67E0EE9
        E9E9FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFE9E9E9B57B07FFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB47B09E9
        E9E9FF00FFFEFEFEFCFCFCFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFA
        FAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFBFBFBE6E6E6B87A01FFFFFFFDFC
        F7FCFAF4FCFAF4FCFAF4FCFAF4FCFAF4FCFAF4FCFAF4FDFCF7FFFFFFB47A07E9
        E9E9FAFAFAEDEDEDE2E2E2E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0
        E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E0E1E1E1D3D3D3BD7900FFFFFFFBF7
        EFFAF6EDFAF6EDFAF6EDFAF6EDFAF6EDFAF6EDFAF6EDFBF7EFFFFFFFB47A08E9
        E9E9EFEFEFCDCDCDA1B3C0A1B1BDA1B1BDA1B1BDA1B1BDA1B1BDA1B1BDA1B1BD
        A1B1BDA1B1BDA1B1BDA1B1BDA1B1BDA1B1BDA1B2BDA3B1BAC07800FFFFFFF8F4
        E8F8F3E7F8F3E7F8F3E7F8F3E7F8F3E7F8F3E7F8F3E7F8F4E8FFFFFFB47A08E9
        E9E9E9E9E94E9CD14498D24094D03E92CE3E92CE3E92CE3E92CE3E92CE3E92CE
        3E92CE3E92CE3E92CE3E92CE3E92CE3E92CE3D92D13394DFBE7600FFFFFFF6EF
        E0F6EFDFF6EFE0F6EFE0F6EFDFF6EFDFF6EFDFF6EEDFF6EFE0FFFFFFB47A08E9
        E9E9E9E9E94498D23F93CFA8FBFF9AF4FF95F3FF95F3FF95F3FF95F3FF95F3FF
        95F3FF95F3FF95F3FF95F3FF95F3FF95F3FF94F4FF8DF9FFBB7200FFFFFFF4EC
        DAF4ECDAF4ECDAF4ECD9F3EBD7F3EBD8F4EBD8F4EBD8F4ECD9FFFFFFB47B08E9
        E9E9E9E9E94397D15DB3DF82D3F29AEFFF8AEAFF8BEAFF8BEAFF8BEAFF8BEAFF
        8BEAFF8BEAFF8BEAFF8BEAFF8BEAFF8BEAFF8AEBFF85F1FFB97100FFFFFFF1E8
        D2F1E8D2F1E8D3F0E6D0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB47B09E9
        E9E9E9E9E94195D07CD3F25FB2E0A5EFFF86E6FD87E5FD87E5FD87E5FD87E5FD
        87E5FD87E5FD87E5FD87E5FD87E5FD87E5FD86E7FF81EDFFB97100FFFFFFEEE4
        CAEFE5CBEFE5CBEEE3C9FFFFFFC5994AAE7100AE6F00AC6C00FFFFFFB57C0AEA
        EAEAE9E9E94094D096ECFE4096D1A6F0FF8FE6FE83E3FD84E3FD84E3FD84E3FD
        84E3FD84E3FD84E3FD84E3FD84E3FD84E3FD83E5FF7DEBFFBA7100FFFFFFECE1
        C2EDE2C4EDE2C4ECE1C2FFFFFFAF7100F2EAD6F6EFE0FAF6EFFFFFFFB57D0CF2
        F2F2E9E9E93F93CFA6F6FF459DD58EDCF79CE9FF7FE1FC81E1FC81E1FC81E1FC
        81E1FC81E1FC81E1FC81E1FC81E1FC81E1FC80E3FF7AE9FFBA7100FFFFFFEADC
        BBEADDBDEADDBDE9DCBBFFFFFFAE7000F6EFE0F8F3E8FFFFFFECDAB7C9A256FB
        FBFBE9E9E93E92CFA6F5FF5FBBE669BBE5AFEFFF7DE0FC7CDFFC7DDFFC7DDFFC
        7DDFFC7DDFFC7DDFFC7DDFFC7DDFFC7DDFFC7CE1FF75E6FFBB7200FFFFFFE7D8
        B2E7D8B3E7D8B3E7D8B1FFFFFFAC6C00FAF6EEFFFFFFEFD6ACC99E51F8F8F8FF
        FFFFE9E9E93E92CFA9F3FF76D7F6429BD5B0EEFF8BE2FB78DDFB7ADDFB7ADDFB
        7ADDFB7ADDFB7ADDFB7ADDFB7ADDFB7ADDFB79DFFE73E4FFBE7400FFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFD6AC8C824CC7CFD6F5F5F5FF
        FFFFE9E9E93D92CEAAF2FF81E6FF3F96D097DFF8A1E7FE72DAFA75DBFA76DBFA
        76DBFA76DBFA76DBFA77DBFA77DBFA77DBFA76DCFC72E1FFB48A28BE7600BB74
        00BA7400BA7400BA7400BA7400BB7500BB7400BC8C2782C5E88CB2CCEDEDEDFF
        FFFFE9E9E93D92CEAEF0FF7EE4FF53ADDE73C4EBBCEFFE6CD8FA6ED8FA6FD8FA
        6FD8FA6FD9FA70D9FA73D9FA74D9FA74D9FA74D9FB72DCFF73D8F973D8F973D8
        F973D8F973D8F973D8F973D8F973D8F973D8F987DFFBB7EDFF519BCFE4E4E4FD
        FDFDE9E9E93D92CFB1F1FF7AE1FD68C7EF47A4DAC6F4FFC3F2FFC4F2FFC4F2FF
        C4F2FFC5F2FFC7F2FF70D8FB71D7FA71D6F971D6F971D6FA70D8FD6FD9FF6ED9
        FF6ED9FF6ED9FF6ED9FF6ED9FF6ED9FF6ED8FF6BD7FEBDF1FF66ADDBD3D7D9F7
        F7F7E9E9E93D92CFB5F1FF77DDFC7CE0FE52ACDE378BCA368BCB368BCB368BCB
        368BCB368BCB388BCB4FA9DD70D8FC6FD6FA6DD4F96DD4F96CD4F96CD4FA6CD4
        FA6CD4FA6CD4FA6CD4FA6CD4FA6CD4FA6CD4FA68D2F9A1E7FE93CAE999B7CBEF
        EFEFE9E9E93D92CFB8F1FF74DBFA77DCFB7ADFFC7AE0FD7AE1FD7BE1FD7BE1FD
        7BE1FD7BE1FD7CE2FE54ACDE4FA8DD6AD5FB65D2F963D0F862D0F862D0F862D0
        F862D0F862D0F862D0F862D0F862D0F862D0F860D0F87FDBFBC8EDFB61A0CDEC
        ECECE9E9E93C92CFBDF2FF71DAFA74DAFA75DBFA76DBFB76DBFB76DBFB76DBFB
        76DBFB76DCFB77DCFC79DFFD52ACDE4AA6DDDAF9FFD6F7FFD5F6FFD5F6FFD5F6
        FFD5F6FFD5F6FFD5F6FFD5F6FFD5F7FFD5F7FFD5F7FFD4F7FFD9FCFF3D94D0F2
        F2F2E9E9E93C92CFC0F3FF6ED8FA72D8FA73D8FA73D8FA73D8FA73D8FA73D8FA
        73D8FA73D8FA73D8FA74DAFB76DDFD51ABDD388CCB368CCB368CCB368CCB368C
        CB368CCB368CCB368BCB358BCB358CCC3890CE3A91CE3B92CF3D94D079B3DCFB
        FBFBE9E9E93C92CFC4F3FF6CD6F970D7F971D7F971D7F971D7F971D7F971D7F9
        71D7F971D7F971D7F971D7F972D9FA73DAFB74DCFC74DCFC74DCFC74DCFC74DC
        FC74DCFC74DCFC73DCFC70DCFDBDF3FF3D93CFE9E9E9FF00FFFF00FFFF00FFFF
        00FFE9E9E93B92CFC9F5FF69D4F96ED5F96FD5F96FD5F96FD5F96FD5F96FD5F9
        6ED5F96ED5F96DD5F96DD5F96ED5F96ED5F96ED5FA6ED5FA6ED5FA6ED5FA6ED5
        FA6ED5FA6ED5FA6DD5FA69D5FAC7F5FF3C92CFE9E9E9FF00FFFF00FFFF00FFFF
        00FFE9E9E93B92CFCFF5FF67D3F86CD4F86DD4F86DD4F86DD4F86DD4F86BD4F8
        68D3F866D2F866D2F866D2F866D2F866D2F866D2F866D2F866D2F866D2F866D2
        F866D2F866D2F865D2F862D2F9D5F7FF3B92CFEAEAEAFF00FFFF00FFFF00FFFF
        00FFE9E9E93B92CFD3F6FF63D1F868D2F869D3F869D3F869D3F869D2F866D1F8
        A8E7FDDAF7FFDAF8FFDAF8FFDAF8FFDAF8FFDAF8FFDAF8FFDAF8FFDAF8FFDAF8
        FFDAF8FFDAF8FFDAF8FFD9F9FFDEFDFF3D94D0F2F2F2FF00FFFF00FFFF00FFFF
        00FFEAEAEA3B92CFD7F8FF5DCFF960CFF861CFF861CFF861CFF861CFF85ECFF8
        BCEEFF348CCC3991CE3A92CF3A92CF3A92CF3A92CF3A92CF3A92CF3A92CF3A92
        CF3A92CF3A92CF3A92CF3A92CF3D94D079B3DCFBFBFBFF00FFFF00FFFF00FFFF
        00FFF2F2F23D94D0DBFCFFD7F8FFD8F7FFD8F7FFD8F7FFD8F7FFD8F7FFD8F8FF
        DBFCFF3C93D0F2F2F2FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFBFBFB79B3DC3D94D03B92CF3A92CF3A92CF3A92CF3A92CF3A92CF3B92CF
        3D94D07AB4DCFBFBFBFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FF}
      Transparent = True
    end
    object Label2: TLabel
      Left = 57
      Top = 37
      Width = 223
      Height = 13
      Caption = 'la nueva Refinanciaci'#243'n o Cancelar el proceso.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object DBListEx1: TDBListEx
      Left = 12
      Top = 61
      Width = 743
      Height = 102
      Anchors = [akLeft, akTop, akRight, akBottom]
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 80
          Header.Caption = 'Nro.'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          Header.Alignment = taRightJustify
          IsLink = False
          FieldName = 'CodigoRefinanciacion'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 125
          Header.Caption = 'Tipo'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          IsLink = False
          FieldName = 'Tipo'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 80
          Header.Caption = 'Fecha'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'Fecha'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 250
          Header.Caption = 'Estado'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          IsLink = False
          FieldName = 'Estado'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 60
          Header.Caption = 'Validada'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'Validada'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 125
          Header.Caption = 'Convenio'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          IsLink = False
          FieldName = 'Convenio'
        end>
      DataSource = dsEnTramiteAceptadas
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
      OnDrawText = DBListEx1DrawText
      ExplicitWidth = 644
    end
    object btnCancelar: TButton
      Left = 681
      Top = 171
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Cancelar'
      Default = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ModalResult = 2
      ParentFont = False
      TabOrder = 1
      ExplicitLeft = 582
    end
    object btnNueva: TButton
      Left = 604
      Top = 171
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Nueva'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ModalResult = 1
      ParentFont = False
      TabOrder = 2
      ExplicitLeft = 505
    end
    object btnNuevaUnificada: TButton
      Left = 380
      Top = 171
      Width = 107
      Height = 25
      Caption = 'Nueva Unificada'
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ModalResult = 1
      ParentFont = False
      TabOrder = 3
      OnClick = btnNuevaUnificadaClick
    end
  end
  object dsEnTramiteAceptadas: TDataSource
    DataSet = spObtenerRefinanciacionesConsultas
    OnDataChange = dsEnTramiteAceptadasDataChange
    Left = 504
    Top = 104
  end
  object spObtenerRefinanciacionesConsultas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    AfterOpen = spObtenerRefinanciacionesConsultasAfterOpen
    ProcedureName = 'ObtenerRefinanciacionesConsultas'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@RUTPersona'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoMedioPago'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoEstado'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@SoloNoActivadas'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 552
    Top = 104
  end
  object lnCheck: TImageList
    BkColor = clWhite
    Height = 15
    Width = 15
    Left = 95
    Top = 131
    Bitmap = {
      494C01010200040014000F000F00FFFFFF00FF10FFFFFFFFFFFFFFFF424D3600
      00000000000036000000280000003C0000000F0000000100200000000000100E
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF000000
      000000000000FFFFFF00000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000424D3E000000000000003E000000280000003C0000000F00000001000100
      00000000780000000000000000000000000000000000000000000000FFFFFF00
      FFFFFFFC00000000800300040000000080030004000000008003000400000000
      8003000400000000800300040000000080030004000000008003000400000000
      8003000400000000800300040000000080030004000000008003000400000000
      80030004000000008003000400000000FFFFFFFC000000000000000000000000
      0000000000000000000000000000}
  end
end
