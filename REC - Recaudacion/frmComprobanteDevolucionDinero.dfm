object ComprobanteDevolucionDineroForm: TComprobanteDevolucionDineroForm
  Left = 0
  Top = 0
  Caption = 'ComprobanteDevolucionDineroForm'
  ClientHeight = 265
  ClientWidth = 640
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 41
    Top = 52
    Width = 67
    Height = 16
    Caption = 'Rut Cliente:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 51
    Top = 87
    Width = 57
    Height = 16
    Caption = 'Convenio:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 354
    Top = 137
    Width = 39
    Height = 16
    Caption = 'Fecha:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 54
    Top = 174
    Width = 54
    Height = 16
    Caption = 'Importe :'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel
    Left = 32
    Top = 8
    Width = 287
    Height = 16
    Caption = 'Crear Comprobante de Devoluci'#243'n de Dinero'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label7: TLabel
    Left = 18
    Top = 123
    Width = 90
    Height = 32
    Caption = 'Comprobantes '#13#10'Pre-Generados:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lblImporte: TLabel
    Left = 114
    Top = 174
    Width = 58
    Height = 16
    Caption = 'lblImporte'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lblFecha: TLabel
    Left = 397
    Top = 137
    Width = 47
    Height = 16
    Caption = 'lblFecha'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lblEnListaAmarilla: TLabel
    Left = 340
    Top = 112
    Width = 200
    Height = 14
    Alignment = taCenter
    AutoSize = False
    Color = clYellow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    Transparent = False
    Visible = False
  end
  object peRutCliente: TPickEdit
    Left = 114
    Top = 49
    Width = 220
    Height = 24
    Enabled = True
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    OnKeyPress = ValidaBusca
    EditorStyle = bteTextEdit
    OnButtonClick = peRutClienteButtonClick
  end
  object vcbConvenios: TVariantComboBox
    Tag = 10
    Left = 114
    Top = 84
    Width = 231
    Height = 22
    Style = vcsOwnerDrawFixed
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ItemHeight = 16
    ParentFont = False
    TabOrder = 1
    OnChange = vcbConveniosChange
    OnDrawItem = vcbConveniosDrawItem
    Items = <>
  end
  object btnAceptar: TButton
    Left = 232
    Top = 192
    Width = 102
    Height = 25
    Caption = 'Generar'
    TabOrder = 3
    OnClick = btnAceptarClick
  end
  object vcbReservaComprobantesDV: TVariantComboBox
    Tag = 10
    Left = 114
    Top = 134
    Width = 220
    Height = 22
    Style = vcsOwnerDrawFixed
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ItemHeight = 16
    ParentFont = False
    TabOrder = 2
    OnChange = vcbReservaComprobantesDVChange
    Items = <>
  end
  object btnSalir: TButton
    Left = 522
    Top = 16
    Width = 102
    Height = 25
    Caption = 'Salir'
    TabOrder = 4
    OnClick = btnSalirClick
  end
  object spObtenerConvenios: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerConvenios;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@SoloActivos'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@SoloContanera'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 440
    Top = 189
  end
  object spCrearComprobanteDevolucionDinero: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CrearComprobanteDevolucionDinero'
    Parameters = <>
    Left = 504
    Top = 189
  end
  object spObtenerReservaComprobantesDV: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerReservaComprobantesDV'
    Parameters = <>
    Left = 472
    Top = 189
  end
end
