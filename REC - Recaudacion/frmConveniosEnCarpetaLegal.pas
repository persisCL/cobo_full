unit frmConveniosEnCarpetaLegal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,

  DMConnection, SysUtilsCN,

  Dialogs, ExtCtrls, StdCtrls, ListBoxEx, DBListEx, DB, ADODB;

type
  TConveniosEnCarpetaLegalForm = class(TForm)
    GroupBox1: TGroupBox;
    spObtenerCarpetasLegalesPersona: TADOStoredProc;
    dsCarpetaLegal: TDataSource;
    DBListEx1: TDBListEx;
    btnAceptar: TButton;
    Label1: TLabel;
    Image1: TImage;
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    function inicializar(pRut, pNombre: string): boolean;
  end;

var
  ConveniosEnCarpetaLegalForm: TConveniosEnCarpetaLegalForm;

implementation

{$R *.dfm}

procedure TConveniosEnCarpetaLegalForm.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
    case key of
        VK_ESCAPE: ModalResult := mrCancel;
    end;
end;

function TConveniosEnCarpetaLegalForm.inicializar(pRut, pNombre: string): boolean;
begin
    try
        Result := False;
        CambiarEstadoCursor(CURSOR_RELOJ);

        Caption := Format(Caption, [pRut, pNombre]);
        try
            with spObtenerCarpetasLegalesPersona do begin
                Parameters.ParamByName('@NumeroDocumento').Value               := pRut;
                Open;
            end;

            Result := True;

        except
            on e: Exception do begin
                raise EErrorExceptionCN.Create('Error al inicializar TConveniosEnCarpetaLegalForm', 'Error: ' + e.Message);
            end;
        end;
    finally
        CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

end.
