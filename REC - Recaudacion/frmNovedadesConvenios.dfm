object frmNovedadesConvenios: TfrmNovedadesConvenios
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = 'Cargar Novedades Convenios'
  ClientHeight = 300
  ClientWidth = 519
  Color = clBtnFace
  Constraints.MaxHeight = 338
  Constraints.MaxWidth = 535
  Constraints.MinHeight = 338
  Constraints.MinWidth = 535
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object lblRuta: TLabel
    Left = 32
    Top = 24
    Width = 40
    Height = 13
    Caption = 'Archivo:'
  end
  object btnAplicar: TButton
    Left = 288
    Top = 267
    Width = 115
    Height = 25
    Caption = 'Aplicar Notificaciones'
    Enabled = False
    TabOrder = 0
    OnClick = btnAplicarClick
  end
  object btnSalir: TButton
    Left = 424
    Top = 267
    Width = 75
    Height = 25
    Caption = 'Salir'
    TabOrder = 1
    OnClick = btnSalirClick
  end
  object txt_Ruta: TEdit
    Left = 78
    Top = 21
    Width = 345
    Height = 21
    Enabled = False
    TabOrder = 2
  end
  object btnBuscar: TButton
    Left = 429
    Top = 19
    Width = 75
    Height = 25
    Caption = 'Buscar'
    TabOrder = 3
    OnClick = btnBuscarClick
  end
  object dbgrdNovedades: TDBGrid
    Left = 32
    Top = 64
    Width = 467
    Height = 197
    DataSource = dsNovedades
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'fldNumeroConvenio'
        ReadOnly = True
        Title.Caption = 'N'#250'mero Convenio'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'fldTextoNovedad'
        ReadOnly = True
        Title.Caption = 'Novedad'
        Visible = True
      end>
  end
  object btnFormato: TButton
    Left = 32
    Top = 267
    Width = 75
    Height = 25
    Caption = 'Formato'
    TabOrder = 5
    OnClick = btnFormatoClick
  end
  object cdsNovedades: TClientDataSet
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'fldNumeroConvenio'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'fldTextoNovedad'
        DataType = ftString
        Size = 50
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 72
    Top = 176
    Data = {
      640000009619E0BD010000001800000002000000000003000000640011666C64
      4E756D65726F436F6E76656E696F010049000000010005574944544802000200
      1E000F666C64546578746F4E6F76656461640100490000000100055749445448
      0200020032000000}
  end
  object dsNovedades: TDataSource
    DataSet = cdsNovedades
    Left = 144
    Top = 176
  end
end
