object ReimpresionNotaCobroInfractoresForm: TReimpresionNotaCobroInfractoresForm
  Left = 0
  Top = 0
  Caption = 'Reimpresion de Nota de Cobro Infractor'
  ClientHeight = 492
  ClientWidth = 740
  Color = clBtnFace
  Constraints.MinHeight = 526
  Constraints.MinWidth = 742
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poMainFormCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 740
    Height = 492
    Align = alClient
    TabOrder = 0
    object pnlFiltros: TPanel
      Left = 1
      Top = 1
      Width = 738
      Height = 96
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 738
        Height = 96
        Align = alClient
        Caption = 'Filtros'
        TabOrder = 0
        DesignSize = (
          738
          96)
        object lblNIFiltro: TLabel
          Left = 10
          Top = 20
          Width = 144
          Height = 13
          Caption = 'Nro. Nota de Cobro Infractor:'
        end
        object lblNroRut: TLabel
          Left = 414
          Top = 20
          Width = 48
          Height = 13
          Caption = 'Nro. RUT:'
        end
        object neNI: TNumericEdit
          Left = 160
          Top = 12
          Width = 178
          Height = 21
          TabOrder = 0
          OnEnter = neNIEnter
        end
        object btnBuscar: TButton
          Left = 634
          Top = 22
          Width = 96
          Height = 25
          Anchors = [akTop, akRight]
          Caption = '&Buscar'
          Default = True
          TabOrder = 4
          OnClick = btnBuscarClick
        end
        object btnLimpiar: TButton
          Left = 634
          Top = 58
          Width = 96
          Height = 25
          Anchors = [akTop, akRight]
          Caption = '&Limpiar'
          TabOrder = 5
          OnClick = btnLimpiarClick
        end
        object gbFechaEmision: TGroupBox
          Left = 3
          Top = 39
          Width = 321
          Height = 53
          Caption = 'Fecha Emisi'#243'n'
          TabOrder = 2
          object lblEmisionDesde: TLabel
            Left = 3
            Top = 22
            Width = 34
            Height = 13
            Caption = 'Desde:'
          end
          object lblEmisionHasta: TLabel
            Left = 159
            Top = 22
            Width = 32
            Height = 13
            Caption = 'Hasta:'
          end
          object deEmiDesde: TDateEdit
            Left = 40
            Top = 19
            Width = 90
            Height = 21
            AutoSelect = False
            TabOrder = 0
            OnEnter = neNIEnter
            Date = -693594.000000000000000000
          end
          object deEmiHasta: TDateEdit
            Left = 194
            Top = 19
            Width = 90
            Height = 21
            AutoSelect = False
            TabOrder = 1
            OnEnter = neNIEnter
            Date = -693594.000000000000000000
          end
        end
        object gbFechaVencimiento: TGroupBox
          Left = 325
          Top = 39
          Width = 298
          Height = 53
          Caption = 'Fecha Vencimiento'
          TabOrder = 3
          object lblVencimientoDesde: TLabel
            Left = 5
            Top = 22
            Width = 34
            Height = 13
            Caption = 'Desde:'
          end
          object lblVencimientoHasta: TLabel
            Left = 161
            Top = 22
            Width = 32
            Height = 13
            Caption = 'Hasta:'
          end
          object deVenDesde: TDateEdit
            Left = 50
            Top = 19
            Width = 90
            Height = 21
            AutoSelect = False
            TabOrder = 0
            OnEnter = neNIEnter
            Date = -693594.000000000000000000
          end
          object deVenHasta: TDateEdit
            Left = 199
            Top = 19
            Width = 90
            Height = 21
            AutoSelect = False
            TabOrder = 1
            OnEnter = neNIEnter
            Date = -693594.000000000000000000
          end
        end
        object neRUT: TPickEdit
          Left = 478
          Top = 12
          Width = 145
          Height = 21
          CharCase = ecUpperCase
          Enabled = True
          TabOrder = 1
          OnEnter = neNIEnter
          EditorStyle = bteTextEdit
          OnButtonClick = neRUTButtonClick
        end
      end
    end
    object pnlDetalle: TPanel
      Left = 1
      Top = 246
      Width = 738
      Height = 245
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object pnlDetalleImporte: TPanel
        Left = 0
        Top = 62
        Width = 738
        Height = 183
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object dbListDetalleNI: TDBListEx
          Left = 0
          Top = 0
          Width = 738
          Height = 142
          Align = alClient
          BorderStyle = bsSingle
          Columns = <
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 300
              Header.Caption = 'Descripci'#243'n'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'Descripcion'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 125
              Header.Caption = 'Importe'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'Importe'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 300
              Header.Caption = 'Observaci'#243'n'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'Observacion'
            end>
          DataSource = dsListarNIDetalle
          DragReorder = True
          ParentColor = False
          TabOrder = 0
          TabStop = True
          OnDrawText = dbListDetalleNIDrawText
        end
        object pnlBotones: TPanel
          Left = 0
          Top = 142
          Width = 738
          Height = 41
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
          DesignSize = (
            738
            41)
          object btnImprimir: TButton
            Left = 439
            Top = 9
            Width = 126
            Height = 25
            Anchors = [akRight, akBottom]
            Caption = '&Reimprimir'
            Default = True
            Enabled = False
            TabOrder = 0
            OnClick = btnImprimirClick
          end
          object btnSalir: TButton
            Left = 603
            Top = 9
            Width = 126
            Height = 25
            Anchors = [akRight, akBottom]
            Caption = '&Salir'
            TabOrder = 1
            OnClick = btnSalirClick
          end
        end
      end
      object pnlDetalleNombres: TPanel
        Left = 0
        Top = 0
        Width = 738
        Height = 62
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object lblDomicilioNI: TLabel
          Left = 32
          Top = 34
          Width = 54
          Height = 13
          Caption = 'Domicilio:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lblNombreNI: TLabel
          Left = 32
          Top = 6
          Width = 47
          Height = 13
          Caption = 'Nombre:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object edNombre: TLabel
          Left = 126
          Top = 6
          Width = 49
          Height = 13
          Caption = 'edNombre'
        end
        object edDomicilio: TLabel
          Left = 126
          Top = 34
          Width = 52
          Height = 13
          Caption = 'edDomicilio'
        end
      end
    end
    object pnlPrincipal: TPanel
      Left = 1
      Top = 97
      Width = 738
      Height = 149
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      object dbListNI: TDBListEx
        Left = 0
        Top = 0
        Width = 738
        Height = 149
        Align = alClient
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 120
            Header.Caption = 'Nro. NI'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'NumeroComprobante'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 120
            Header.Caption = 'Fecha Emisi'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'FechaEmision'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 120
            Header.Caption = 'Fecha Vencimiento'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'FechaVencimiento'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 120
            Header.Caption = 'Nro. RUT'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'NumeroDocumento'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 120
            Header.Caption = 'Total NI'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'TotalComprobante'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 120
            Header.Caption = 'Estado'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'EstadoPago'
          end>
        DataSource = dsListarNI
        DragReorder = True
        ParentColor = False
        TabOrder = 0
        TabStop = True
        OnDrawText = dbListNIDrawText
      end
    end
  end
  object spObtenerListadoNotaInfractores: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    AfterScroll = spObtenerListadoNotaInfractoresAfterScroll
    ProcedureName = 'ObtenerListadoNotaInfractores;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroNI'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroRUT'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = Null
      end
      item
        Name = '@fechaEmiDesde'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@fechaEmiHasta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@fechaVenDesde'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@fechaVenHasta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 16
    Top = 440
  end
  object dsListarNI: TDataSource
    DataSet = spObtenerListadoNotaInfractores
    Left = 48
    Top = 440
  end
  object spObtenerListadoNotaInfractoresDetalle: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerListadoNotaInfractoresDetalle;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@NumeroNI'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 50
      end>
    Left = 136
    Top = 440
  end
  object dsListarNIDetalle: TDataSource
    DataSet = spObtenerListadoNotaInfractoresDetalle
    Left = 168
    Top = 440
  end
end
