{-----------------------------------------------------------------------------
 File Name: frmCobroComprobantes.pas
 Author:    Modificado por Flamas
 Date Created: 21/12/2004
 Language: ES-AR
 Description:

 Revision 1:
  Author:    ggomez
  Date:      01-Abr-2005
  Description:
    - M�todo Cobrar: Cambi� el llamado al MsgBox previo a realizar la impresi�n
	por el llamado a un form. �ste form retorna resultado distintos seg�n lo que
	responda el operador.
    - M�todo Cobrar: Implement� que seg�n la respuesta del operador, previa a la
	impresi�n, se muestre o no el form de Configuraci�n de Impresora.
    - M�todo Cobrar: Agregu� c�digo para preguntar al operador si la impresi�n
    se realiz� con exito. Si la respuesta es NO, se muestra el form de
	Configurar Impresi�n para que pueda reintentar la impresi�n.
	- M�todo Cobrar: Agregu� c�digo para mostrar un mensaje en el caso de que
    no se encuentre el archivo con el logo para el recibo.
	- M�todo Cobrar: Cuando se muestra el di�logo de Configurar Impresora, s�lo
    se muestra el di�logo de Impresi�n exitosa cuando el operador presion� Ok.

 Revision 2:
  Author:    ggomez
  Date:      07-Jun-2005
  Description: En el llamado al Form frmPagoVentanilla agregu� el par�metro
    CodigoFormaPago, que se utiliza para seleccionar por defecto esa Forma de
    Pago.

 Revision 3:
  Author:    ggomez
  Date:      16-Jun-2005
  Description: Al filtrar por un solo comprobante, cuando se muestra un s�lo
    comprobante en la grilla, si tiene pago autom�tico, ya no se pregunta si lo
    quiere marcar para cobrar, directamente no se marca.

 Revision 4:
  Author:    ggomez
  Date:      21-Jun-2005
  Description: Agregu� un label para mostrar el monto total de los comprobantes
    seleccionados para cobrar.

 Revision 5:
  Author:    jconcheyro
  Date:      22-Nov-2005
  Description: al Onchange del cbConvenios se le pone una busqueda de domicilio de facturacion kanav 1509

 Revision 6:
    Author: jconcheyro
    Date: 18/07/2006
    Description: Carpeta Legal: se agrega mensaje de aviso si el convenio se encuentra en carpeta Legal .

 Revisi�n 7:
  Author:    ddiaz
  Date:      20/07/2006
  Description: se cambia el chequeo de Carpetas Morosos a Peaprocs

 Revisi�n 7:
  Author:    jconcheyro
  Date:      21/09/2006
  Description: Al ingresar un comprobante por c�digo de barras, ya no se seleccionan todos los anteriores
  ya que el nuevo modelo de aplicaci�n los ir� pagando despu�s del seleccionado, si queda dinero remanente

 Revision 9:
    Author: nefernandez
    Date: 23/03/2007
    Description: Se agrega mensaje de aviso si el comprobante a cobrar corresponde a un convenio que se
    encuentra en carpeta Legal y se presenta al usuario la opci�n para realizar o no dicho cobro. Esto
    se verifica tanto al indicar un comprobante espec�fico o al ingresar un convenio.

  Revision 10:
    Author: Fsandi - jconcheyro
    Date: 06-06-2007
    Description: - Se modifica el label total comprobante, para que ahora sea total exigible (saldo del convenio - Total seleccionado)
                 - Se sacaron un serie de consultas que se hac�a repetidamente a la base para obtener datos de cada comprobante
                 - y se agregaron en el store de Obtener y se subieron al clientDataSet para mejorar la performance.
  Revision: 12
  Author: mpiazza
  Date: 23/01/2009
  Description: SS 777: Mensaje segun tipo de cliente y sistema se agrego el
  procedimiento MostrarMensajeTipoClienteSistema y modifico MostrarDatosCliente

  Revision : 13
      Author : vpaszkowicz
      Date : 27/02/2009
      Description : Agrego la empresa a la que fue asignada un cliente en pro-
        ceso judicial

  Revision : 14
    Date: 11/03/2009
    Author: mpiazza
    Description:  Ref (ss795) Error en la impresion de comprobante,
                  se le agrego un except para que continue con el
                  proceso una vez informado el error

  Revision : 15
    Date: 14/04/2009
    Author:Nelson Droguett Sierra
    Description:  Se incluye logica para los comprobantes electronicos

   Revision : 16
    Date: 20-Agosto-2010
    Author:Nelson Droguett Sierra
    Description: (Ref. SS-914)
                Al dar click en el boton Cobrar F9, se desactiva este, para
                evitar que se pueda volver a presionar

  Revision : 16
    Date: 03/06/2010
    Author: pdominguez
    Description: Infractores Fase 2
        - Se a�adieron los siguientes objetos / variable / constantes:
            spCobrar_o_DescontarReimpresion: TADOStoredProc

        - Se modificaron/a�adieron los siguientes procedimientos/funciones:
            ObtenerComprobantesCobrar,
            ImprimirDetalleInfraccionesAnuladas,
            Cobrar

  Revision : 16
    Date: 17/11/2010
    Author: pdominguez
    Description: SS 527 - salir todas NK seleccionadas y advertencia anteriores impagas
        - Se modificaron los siguientes Procedimientos / Funciones:
            CargarComprobantes,
            btnCobrarClick

  Firma       : SS-1014-NDR-20120123
  Description : Sacar la b�squeda incremental, y que se ejecute cuando presione el bot�n filtrar.


	Firma: SS_740_MBE_20110720
	Description:	Se agrega el comprobante TD a la lista de comprobantes
					que se pueden cobrar

    Firma: SS_959_MBE_20110721
    Description:	Se agrega el comprobante DV a la lista de comprobantes que se
                	pueden cobrar

	Firma: SS_959_MBE_20110905
    Description:	Se modifica para que si la fecha de vencimiento es NULL, despliegue NULL
                	y no 31-12-1899

  Firma       : SS-1015-NDR-20120516
  Description : Agregar el tipo comprobante cuotas otras concesionarias y saldo inicial otras concesionarias
                al combo de tipos comprobantes.
 
  Firma       : SS_660_NDR_20121002
  Description : Verifica si el convenio seleccionado se encuentra en lista amarilla

  	Firma 		: SS_1051_PDO
	Description : Avenimiento cuota unica (Reflotada NDR 2013-04-15)

    Author          :   CQuezadaI
    Date            :   30 - Abril - 2013
    Firma           :   SS_660_CQU_20121010
    Description     :   Se verifica si el convenio est� en Lista Amarilla,
                        si se encuentra en LA se deshabilita el objeto Panel2
                        ya que de esa forma no puede presionar ningun bot�n.

Author		: mvillarroel
Date		: 07-Julio-2013
Firma   : SS_660_MVI_20130711
Description	:		(Ref: SS 660)
                Se agrega validaci�n para indicar si un Convenio est� en Lista Amarilla

Firma       : SS_660_CQU_20130711
Descripcion : Se alinea el Label de Lista Amarilla.
              Se agrega el par�metro CodigoConcesionaria en la funci�n EstaConvenioEnListaAmarilla
              Se agrega funci�n EstaPersonaListaAmarilla para buscar por el CodgoCliente

Firma       : SS_660_MVI_20130729
Descripcion : Se cambia texto y se agrega estado del convenio en lista amarilla

Firma       :   SS_1120_MVI_20130820
Descripcion :   Se cambia estilo del comboBox a: vcsOwnerDrawFixed,
                se crear el evento DrawItem y se llama al procedimeinto ItemComboBoxColor.

Firma       :   SS_660_MVI_20130909
Descripcion :   Se modifica el label de lista amarilla, para que aparezca solo cuando es seleccionado
                un convenio en lista amarilla. Mientras aparecer� un mensaje, para identificar si la persona est� en lista amarilla.
                Se incluye la limpieza del label lista amarilla, dentro de la limpieza de datos de cliente.
                Se modifica validaci�n para saber si se obtiene resultado del SP ObtenerEstadoConvenioEnListaAmarilla.
                Se acorta tama�o del lblEnListaAmarilla y se elimina texto inicial, ya que siempre es reemplazado.

 Firma          :   SS_660_MBE_20130925
 Description    :   Se debe poder pagar con refinanciaci�n aunque el convenio est� en Lista Amarilla.
                    (esto desdice lo solicitado el 26-03-2013 No se puede pagar con refinanciaci�n si el convenio est� en LA)


 Firma			:	SS_1200_MCA_20140704
 Fecha			:	04/07/2014
 Descripcion	:	Se corrige problema de operacion aritmetica ya que estaba provocando overflow

  Firma			:	SS_1236_MCA_20141226
 Descripcion	:	Se agrega leyenda de lista amarilla o en proceso por comprobante y por rut


Firma           : SS_1147_NDR_20141216
Descripcion     : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

Firma           : SS_1147_MCA_20150422
Descripcion     : se agrega label con el nombre corto de la concesionaria nativa

Firma		    :	SS_1254_MBE_20150504
Description	    :	el tipo de documento predefinido al cargar la ventana es "todos"

 Fecha          :   06/10/2015
 Firma          :   SS_1246_CQU_20151006
 Descripcion    :   Se pidi� quitar el cobro por reimpresi�n de comprobantes en TODAS partes.

 Firma          : SS_1408_MCA_20151027
 Descripcion    : muestra un mensaje si el cliente se encuentre en registrado en Tabla ClientesAContactar

 Firma          : TASK_009_GLE_20161025
 Fecha          : 25/10/2016
 Descripci�n    : Se corrige el valor que devuelve el label lblTotalAPagar

 -----------------------------------------------------------------------------}
unit frmCobroComprobantes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ListBoxEx, DBListEx, StdCtrls, DPSControls, VariantComboBox,
  DmiCtrls, DB, ADODB, DMCOnnection, utilDB, UtilFacturacion, utilProc, buscaClientes,
  PeaTypes, RStrings, DBCtrls, peaProcs, Util, PagoVentanilla, ExtCtrls, ReporteRecibo,
  FrmImprimir, DBClient, ImgList, SysUtilsCN, PeaProcsCN, frmMuestraMensaje, frmRefinanciacionGestion, //frmRefinanciacionImpresion,
  Provider, frmRefinanciacionReporteRecibo, 
  frmRefinanciacionEntrarEditarCuotas, frmRptDetInfraccionesAnuladas,

  CobranzasResources, frmActivarRefinanciacion, frmRefinanciacionConsultas, Contnrs;

const
	KEY_F9 	= VK_F9;
type
  TformCobroComprobantes = class(TForm)
  	gbPorCliente: TGroupBox;
    lb_CodigoCliente: TLabel;
    lbl_NumeroConvenio: TLabel;
    peNumeroDocumento: TPickEdit;
    cbConvenios: TVariantComboBox;
    gb_DatosCliente: TGroupBox;
    Label6: TLabel;
    lblNombre: TLabel;
    lblDomicilio: TLabel;
	  Label10: TLabel;
    Label3: TLabel;
    lbl_NumeroComprobante: TLabel;
    lbl_CodigoBarra: TLabel;
    edNumeroComprobante: TNumericEdit;
	  edCodigoBarras: TEdit;
  	cbTiposComprobantes: TVariantComboBox;
    Label8: TLabel;
    lblRUT: TLabel;
    ds_Comprobantes: TDataSource;
    Img_Tilde: TImageList;
    spObtenerDomicilioFacturacion: TADOStoredProc;
    lblImpresoraFiscal: TLabel;
    cbImpresorasFiscales: TVariantComboBox;
    spObtenerSaldoConveniosComprobantesImpagos: TADOStoredProc;
    cds_Comprobantes: TClientDataSet;
    pcOpcionesComprobantes: TPageControl;
    tbsComprobantes: TTabSheet;
    tbsRefinanciaciones: TTabSheet;
    GroupBox1: TGroupBox;
    Label15: TLabel;
    lblTotalSelec: TLabel;
    dl_Comprobantes: TDBListEx;
    gbDatosComprobante: TGroupBox;
    Label2: TLabel;
    lblFecha: TLabel;
    lblFechaVencimiento: TLabel;
    Label7: TLabel;
    lblTotalAPagar: TLabel;
    Label12: TLabel;
    lbl_Estado: TLabel;
    Label11: TLabel;
    Label13: TLabel;
    lblDescPATPAC: TLabel;
    lblEstadoDebito: TLabel;
    Label14: TLabel;
    lblMedioPagoAutomatico: TLabel;
    lblCapRespuesta: TLabel;
    lblRespuestaDebito: TLabel;
    Panel1: TPanel;
    btnCobrar: TButton;
    btnSalir: TButton;
    spObtenerRefinanciacionesConsultas: TADOStoredProc;
    dspObtenerRefinanciacionesConsultas: TDataSetProvider;
    cdsRefinanciaciones: TClientDataSet;
    dsRefinanciaciones: TDataSource;
    cdsRefinanciacionesCodigoRefinanciacion: TIntegerField;
    cdsRefinanciacionesTipo: TStringField;
    cdsRefinanciacionesFecha: TDateTimeField;
    cdsRefinanciacionesEstado: TStringField;
    cdsRefinanciacionesNumeroDocumento: TStringField;
    cdsRefinanciacionesNombrePersona: TStringField;
    cdsRefinanciacionesConvenio: TStringField;
    cdsRefinanciacionesTotalDeuda: TStringField;
    cdsRefinanciacionesTotalPagado: TStringField;
    cdsRefinanciacionesValidada: TBooleanField;
    Panel2: TPanel;
    btnActivarRefinanciacion: TButton;
    Button2: TButton;
    DBListEx1: TDBListEx;
    lnCheck: TImageList;
    btnConsultarRefinanciacion: TButton;
    spCrearRecibo: TADOStoredProc;
    cdsRefinanciacionesCodigoTipoMedioPago: TSmallintField;
    spObtenerRefinanciacionComprobantes: TADOStoredProc;
    spRefinanciacionCancelarComprobante: TADOStoredProc;
    spObtenerRefinanciacionCuotas: TADOStoredProc;
    spObtenerRefinanciacionCuotasAPagar: TADOStoredProc;
    spRegistrarDetallePagoComprobante: TADOStoredProc;
    cdsRefinanciacionComprobantes: TClientDataSet;
    dspRefinanciacionComprobantes: TDataSetProvider;
    spRefinanciacionRegistrarPagoComprobante: TADOStoredProc;
    dspRefinanciacionCuotas: TDataSetProvider;
    cdsRefinanciacionCuotas: TClientDataSet;
    cdsRefinanciacionCuotasCodigoEstadoCuota: TSmallintField;
    cdsRefinanciacionCuotasDescripcionEstadoCuota: TStringField;
    cdsRefinanciacionCuotasCodigoFormaPago: TSmallintField;
    cdsRefinanciacionCuotasDescripcionFormaPago: TStringField;
    cdsRefinanciacionCuotasTitularNombre: TStringField;
    cdsRefinanciacionCuotasTitularTipoDocumento: TStringField;
    cdsRefinanciacionCuotasTitularNumeroDocumento: TStringField;
    cdsRefinanciacionCuotasCodigoBanco: TIntegerField;
    cdsRefinanciacionCuotasDescripcionBanco: TStringField;
    cdsRefinanciacionCuotasDocumentoNumero: TStringField;
    cdsRefinanciacionCuotasFechaCuota: TDateTimeField;
    cdsRefinanciacionCuotasCodigoTarjeta: TIntegerField;
    cdsRefinanciacionCuotasDescripcionTarjeta: TStringField;
    cdsRefinanciacionCuotasTarjetaCuotas: TSmallintField;
    cdsRefinanciacionCuotasImporte: TLargeintField;
    cdsRefinanciacionCuotasCodigoEntradaUsuario: TStringField;
    spObtenerRefinanciacionDatosCabecera: TADOStoredProc;
    dspRefinanciacionDatos: TDataSetProvider;
    cdsRefinanciacionDatos: TClientDataSet;
    cdsRefinanciacionComprobantesFechaEmision: TDateTimeField;
    cdsRefinanciacionComprobantesFechaVencimiento: TDateTimeField;
    cdsRefinanciacionComprobantesTipoComprobante: TStringField;
    cdsRefinanciacionComprobantesNumeroComprobante: TLargeintField;
    cdsRefinanciacionComprobantesDescriComprobante: TStringField;
    cdsRefinanciacionComprobantesSaldoPendienteDescri: TStringField;
    cdsRefinanciacionComprobantesTotalComprobanteDescri: TStringField;
    cdsRefinanciacionComprobantesDescriAPagar: TStringField;
    cdsRefinanciacionComprobantesSaldoPendienteComprobante: TLargeintField;
    cdsRefinanciacionComprobantesSaldoRefinanciado: TLargeintField;
    spActualizarRefinanciacionActivada: TADOStoredProc;
    cdsRefinanciacionDatosCodigoRefinanciacion: TIntegerField;
    cdsRefinanciacionDatosCodigoPersona: TIntegerField;
    cdsRefinanciacionDatosCodigoConvenio: TIntegerField;
    cdsRefinanciacionDatosCodigoTipoMedioPago: TSmallintField;
    cdsRefinanciacionDatosFecha: TDateTimeField;
    cdsRefinanciacionDatosCodigoEstado: TSmallintField;
    cdsRefinanciacionDatosEstadoRefinanciacion: TStringField;
    cdsRefinanciacionDatosTotalDeuda: TLargeintField;
    cdsRefinanciacionDatosTotalPagado: TLargeintField;
    cdsRefinanciacionDatosTotalDemanda: TLargeintField;
    cdsRefinanciacionDatosNotificacion: TBooleanField;
    cdsRefinanciacionDatosRepLegalCliNumeroDocumento: TStringField;
    cdsRefinanciacionDatosRepLegalCliNombre: TStringField;
    cdsRefinanciacionDatosRepLegalCliApellido: TStringField;
    cdsRefinanciacionDatosRepLegalCliApellidoMaterno: TStringField;
    cdsRefinanciacionDatosRepLegalCNNumeroDocumento: TStringField;
    cdsRefinanciacionDatosRepLegalCNNombre: TStringField;
    cdsRefinanciacionDatosRepLegalCNApellido: TStringField;
    cdsRefinanciacionDatosRepLegalCNApellidoMaterno: TStringField;
    cdsRefinanciacionDatosNumeroCausaRol: TStringField;
    cdsRefinanciacionDatosGastoNotarial: TIntegerField;
    cdsRefinanciacionDatosUsuarioCreacion: TStringField;
    cdsRefinanciacionDatosFechaCreacion: TDateTimeField;
    cdsRefinanciacionDatosUsuarioModificacion: TStringField;
    cdsRefinanciacionDatosFechaModificacion: TDateTimeField;
    cdsRefinanciacionDatosJPLNumero: TSmallintField;
    cdsRefinanciacionDatosJPLCodigoPais: TStringField;
    cdsRefinanciacionDatosJPLCodigoRegion: TStringField;
    cdsRefinanciacionDatosJPLCodigoComuna: TStringField;
    lblDeudasConvenio: TLabel;
    lblMontoDeudasConvenios: TLabel;
    lblMontoDeudasRefinanciacion: TLabel;
    lblDeudasRefinanciacion: TLabel;
    spObtenerRefinanciacionesTotalesPersona: TADOStoredProc;
    TabSheet1: TTabSheet;
    Panel3: TPanel;
    btnCobrarCuota: TButton;
    Button3: TButton;
    DBListEx2: TDBListEx;
    dspCuotasAPagar: TDataSetProvider;
    cdsCuotasAPagar: TClientDataSet;
    cdsCuotasAPagarRut: TStringField;
    cdsCuotasAPagarNombrePersona: TStringField;
    cdsCuotasAPagarCodigoRefinanciacion: TIntegerField;
    cdsCuotasAPagarCodigoCuota: TSmallintField;
    cdsCuotasAPagarCodigoEstadoCuota: TSmallintField;
    cdsCuotasAPagarDescripcionEstadoCuota: TStringField;
    cdsCuotasAPagarCodigoFormaPago: TSmallintField;
    cdsCuotasAPagarDescripcionFormaPago: TStringField;
    cdsCuotasAPagarTitularNombre: TStringField;
    cdsCuotasAPagarTitularTipoDocumento: TStringField;
    cdsCuotasAPagarTitularNumeroDocumento: TStringField;
    cdsCuotasAPagarCodigoBanco: TIntegerField;
    cdsCuotasAPagarDescripcionBanco: TStringField;
    cdsCuotasAPagarDocumentoNumero: TStringField;
    cdsCuotasAPagarFechaCuota: TDateTimeField;
    cdsCuotasAPagarImporte: TLargeintField;
    cdsCuotasAPagarImporteDesc: TStringField;
    cdsCuotasAPagarEstadoImpago: TBooleanField;
    cdsCuotasAPagarNumeroConvenio: TStringField;
    dsCuotasAPagar: TDataSource;
    cdsCuotasAPagarSaldo: TLargeintField;
    cdsCuotasAPagarSaldoDesc: TStringField;
    cdsCuotasAPagarCodigoTarjeta: TIntegerField;
    cdsCuotasAPagarDescripcionTarjeta: TStringField;
    cdsCuotasAPagarTarjetaCuotas: TSmallintField;
    spActualizarRefinanciacionCuotaPago: TADOStoredProc;
    spActualizarRefinanciacionCuotasEstados: TADOStoredProc;
    spActualizarRefinanciacionPagoAnticipado: TADOStoredProc;
    cdsRefinanciacionCuotasCodigoCuota: TSmallintField;
    cdsRefinanciacionCuotasSaldo: TLargeintField;
    cdsRefinanciacionCuotasEstadoImpago: TBooleanField;
    cdsRefinanciacionCuotasCodigoCuotaAntecesora: TSmallintField;
    cdsRefinanciacionCuotasCuotaNueva: TBooleanField;
    cdsRefinanciacionCuotasCuotaModificada: TBooleanField;
    cdsCuotasAPagarCuotaNueva: TBooleanField;
    cdsCuotasAPagarCuotaModificada: TBooleanField;
    cdsCuotasAPagarCodigoTipoMedioPago: TSmallintField;
    cdsCuotasAPagarCodigoEntradaUsuario: TStringField;
    cdsCuotasAPagarCodigoCuotaAntecesora: TSmallintField;
    spObtenerDeudaComprobantesVencidos: TADOStoredProc;
    cdsCuotasAPagarCobrar: TBooleanField;
    Panel7: TPanel;
    Label18: TLabel;
    lblCuotasCantidad: TLabel;
    Label20: TLabel;
    lblCuotasTotal: TLabel;
    Panel4: TPanel;
    vcbRefinanciacion: TVariantComboBox;
    Label1: TLabel;
    spObtenerRefinanciacionCuotasAPagarRefinanciaciones: TADOStoredProc;
    chkbxSeleccionManual: TCheckBox;
    btnDesmarcarTodos: TButton;
    spCobrar_o_DescontarReimpresion: TADOStoredProc;
    spActualizarInfraccionesPagadas: TADOStoredProc;                            // SS_637_PDO_20110714
    btnFiltrar: TButton;                                                        //SS-1014-NDR-20120123
	cdsRefinanciacionesCodigoRefinanciacionUnificada: TIntegerField;			//SS_1051_PDO
    lblEnListaAmarilla: TLabel;
    sp_ActualizarClienteMoroso: TADOStoredProc;
    cdsRefinanciacionesCodigoEstado: TIntegerField;			                                    //SS_1147_MCA_20150422
    procedure cbImpresorasFiscalesChange(Sender: TObject);
    procedure peNumeroDocumentoChange(Sender: TObject);
  	procedure cbConveniosChange(Sender: TObject);
  	procedure FormCreate(Sender: TObject);
    procedure peNumeroDocumentoButtonClick(Sender: TObject);
  	procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCobrarClick(Sender: TObject);
  	procedure cbComprobantesChange(Sender: TObject);
    procedure edCodigoBarrasChange(Sender: TObject);
    procedure edCodigoBarrasKeyPress(Sender: TObject; var Key: Char);
  	procedure cbTiposComprobantesChange(Sender: TObject);
  	procedure edNumeroComprobanteChange(Sender: TObject);
  	procedure FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
  	//procedure Timer1Timer(Sender: TObject);                                   //SS-1014-NDR-20120123
    procedure dl_ComprobantesDblClick(Sender: TObject);
    procedure dl_ComprobantesDrawText(Sender: TCustomDBListEx;Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState; var Text: String; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
    procedure dl_ComprobantesClick(Sender: TObject);
    procedure dl_ComprobantesKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormResize(Sender: TObject);
    procedure DBListEx1DrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
    procedure cdsRefinanciacionesAfterScroll(DataSet: TDataSet);
    procedure btnConsultarRefinanciacionClick(Sender: TObject);
    procedure btnActivarRefinanciacionClick(Sender: TObject);
    procedure DBListEx2DrawBackground(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var DefaultDraw: Boolean);
    procedure DBListEx2DrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
    procedure btnCobrarCuotaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure DBListEx2LinkClick(Sender: TCustomDBListEx; Column: TDBListExColumn);
    procedure vcbRefinanciacionChange(Sender: TObject);
    procedure chkbxSeleccionManualClick(Sender: TObject);
    procedure btnDesmarcarTodosClick(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);                             //SS-1014-NDR-20120123
    procedure cbConveniosDrawItem(Control: TWinControl; Index: Integer;     //SS_1120_MVI_20130820
      Rect: TRect; State: TOwnerDrawState);                                 //SS_1120_MVI_20130820

  private
    { Private declarations }
    FLogo: TBitmap;
    FTipoComprobante: AnsiString;
    FNumeroComprobante: Int64;
    FUltimoComprobante,
    FTotalCuotasCobrar: Int64;
    FUltimaBusqueda: TBusquedaCliente;
    FTotalApagar: Double;
    FNumeroPOS, FPuntoEntrega, FPuntoVenta: Integer;
    FCodigoCliente: integer;
    FCantidadComprobantesCobrar,
    FCantidadCuotasCobrar: Integer;
    // C�digo del canal de pago a setear por defecto en el form de registar pago.
    FCodigoCanalPago: Integer;
    // Monto Total de los comprobantes seleccionados para cobrar.
    FTotalComprobantesCobrar: Double;
    FTotal: Int64; //TASK_009_GLE_20161025 agrega variable
    FSaldoConvenios : int64;
	FUltimoNumeroDocumento: String;
    //Author: mbecerra
    //SS 762: que no se redimensione muy peque�o
    FMinHeight, FMinWidth : integer;
    FComprobantesEnCobranza: TStringList; // SS_637_20100927
    FAhora: TDate;
    FbtnCobrarCuotaPermisos,
    FbtnActivarRefinanciacionPermisos,
    FvcbRefinanciacionesPermisos,
    FchkbxSeleccionManualPermisos: Boolean;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    procedure MostrarDatosCliente(CodigoCLiente: Integer);
  	function  CargarComprobantes : boolean;
    procedure MostrarDatosComprobante;
    procedure CargarTiposComprobantes;
  	procedure LimpiarCampos;
    procedure LimpiarConvenios;
    procedure LimpiarDetalles;
    procedure LimpiarDatosCliente;
    procedure LimpiarListaComprobantes;
  	procedure Cobrar;
    procedure HabilitarFiltroRUT(Valor: Boolean);
    function TienePagoAutomatico(TipoComprobante: AnsiString; NumeroComprobante: Integer; var TipoMedioPago: Integer): Boolean;
    function CobrarComprobanteConPagoAutomatico(TipoComprobante: AnsiString; NumeroComprobante, TipoMedioPago: Integer): Boolean;
    function ObtenerComprobantesCobrar: TListaComprobante;
    procedure MostrarControlesSegunComprobante(TipoComprobante: Variant);
  	function VerificarComprobanteEnCarpetaLegal(ModoSilencioso: Boolean): Boolean; // SS_637_20100927
    function PoseeCanalDePago: Boolean;
    procedure MostrarMensajeRUTConvenioCuentas(NumeroDocumento: String);
    procedure MostrarMensajeTipoClienteSistema(NumeroDocumento: String);
    function ImprimirDetalleInfraccionesAnuladas(TipoComprobante: string; NroComprobante: int64): Boolean;
    procedure CargarRefinanciaciones(CodigoPersona: Integer);
    procedure CargarComboRefinanciaciones(CodigoPersona: Integer);
    procedure LimpiarRefinanciaciones;
    function ActivarRefinanciacion: Boolean;
    Procedure MostrarSaldosPersona(CodigoPersona: Integer);
    procedure VerificaComprobantesMarcados;
    procedure VerificarCuotasACobrar;
    procedure MarcarDesmarcarComprobantes(Marcar: Boolean);
    function  VerificarConvenioListaAmarilla(CodigoConvenio : Integer) : String;    //SS_660_MVI_20130729  // SS_660_CQU_20121010
   // function  VerificaPersonaListaAmarilla(iCodigoCliente : Integer) : Boolean;    //SS_1236_MCA_20141226//SS_660_MVI_20130909  //SS_660_MVI_20130729// SS_660_CQU_20130711
   function  VerificaPersonaListaAmarilla(RutCliente : string) : Boolean;       //SS_1236_MCA_20141226
  public
    { Public declarations }
    FComponentes: TObjectList;
    function Inicializar(NumeroPOS, PuntoEntrega, PuntoVenta: Integer): Boolean; overload;
  	function Inicializar(TipoComprobante: AnsiString; NumeroComprobante: Int64; NumeroPOS, PuntoEntrega, PuntoVenta: Integer): Boolean; Overload;
    function InicializarParaRefinanciaciones(RUT: AnsiString; NumeroPOS, PuntoEntrega, PuntoVenta: Integer): Boolean;//TASK_005_AUN_20170403-CU.COBO.COB.301
  end;

implementation

const
	SetPACPAT = [1, 2];

resourceString
    MSG_SIN_PERMISOS = 'Ud. no tiene permisos para trabajar con canales de pago. Consulte al administrador del sistema.';

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    flamas
  Date Created: 21/12/2004
  Description: Inicializa el Form - Carga los tipos de Comprobantes
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TformCobroComprobantes.Inicializar(NumeroPOS, PuntoEntrega, PuntoVenta: Integer): Boolean;
resourcestring
	MSG_LOGO_NO_ENCONTRADO  = 'No se ha encontrado el archivo con la imagen del logo para el recibo.';
	MSG_CAPTION             = 'Cobro de Comprobantes';
	MSG_ERROR_INICIALIZAR   = 'Error inicializando el formulario.';
begin
    try
        Result := False;

        FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
        FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
        FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
        FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
        Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

//        CenterForm(Self);
    // Se cargan las impresoras y se ejecuta el OnChange de los Tipos de Comprobantes para que muestre los componentes que son necesarios
        CargarImpresorasFiscales(DMConnections.BaseCAC, cbImpresorasFiscales, 1);
        MostrarControlesSegunComprobante(cbTiposComprobantes.Value);

    //SS 762
        FMinHeight := ClientHeight;
        FMinWidth  := ClientWidth;

        FAhora := Trunc(NowBaseSmall(DMConnections.BaseCAC));

        CargarConstantesCobranzasRefinanciacion;
        CargarConstantesCobranzasCanalesFormasPago;

        FbtnCobrarCuotaPermisos           := ExisteAcceso('cobrar_cuota_cob_comp_ref');
        FbtnActivarRefinanciacionPermisos := ExisteAcceso('activar_ref_Cob_comp_ref');
        FvcbRefinanciacionesPermisos      := ExisteAcceso('Combo_ref_cob_comp_ref');
        FchkbxSeleccionManualPermisos     := ExisteAcceso('check_seleccion_manual');

        vcbRefinanciacion.Enabled         := False;

        if not PoseeCanalDePago then begin
            //MsgBoxErr(MSG_ERROR_INICIALIZAR, MSG_SIN_PERMISOS, MSG_CAPTION, MB_ICONERROR);
            MsgBox(MSG_SIN_PERMISOS, MSG_CAPTION, MB_ICONINFORMATION + MB_OK);
            Exit;
        end;
        if not cds_Comprobantes.Active then cds_Comprobantes.Active := True;

        FTotalComprobantesCobrar := 0;
        FCantidadComprobantesCobrar := 0;
        FCantidadCuotasCobrar := 0;
        FTotalCuotasCobrar    := 0;
        lblTotalSelec.Caption:='$ 0';

        lblCuotasCantidad.Caption := '0';
        lblCuotasTotal.Caption    := '$ 0';

        lblMontoDeudasConvenios.Caption         := '$ 0'; // SS_637_PDO_20101227
        lblMontoDeudasRefinanciacion.Caption    := '$ 0'; // SS_637_PDO_20101227
        lblMontoDeudasConvenios.Font.Color      := clBlack;
        lblMontoDeudasRefinanciacion.Font.Color := clBlack;

        FCodigoCliente := -1;
        FCodigoCanalPago := CONST_CANAL_PAGO_VENTANILLA;
        // Por ahora lo cargamos a mano
        LimpiarDatosCliente;

        pcOpcionesComprobantes.ActivePage := tbsComprobantes;//TASK_005_AUN_20170403-CU.COBO.COB.301

        lbl_Estado.Caption := EmptyStr;
        CargarTiposComprobantes;
        MostrarDatosComprobante;
        FNumeroPOS := NumeroPOS;
        FPuntoEntrega := PuntoEntrega;
        FPuntoVenta := PuntoVenta;

        cds_Comprobantes.Close;
        cds_Comprobantes.CreateDataSet;
        cds_Comprobantes.Open;

//        FormStyle := fsMDIChild;
//        CenterForm(Self);

        FLogo := TBitmap.Create;
        (* Levanto el logo aqu� para que se haga una sola vez, independientemente de los reintentos
        de impresi�n que haga el operador. *)
//        if not LevantarLogo (FLogo) then MsgBox(MSG_LOGO_NO_ENCONTRADO, MSG_CAPTION, MB_ICONINFORMATION + MB_OK);

    	if edNumeroComprobante.Enabled then begin                              		// SS_1254_MBE_20150504
        	ActiveControl := edNumeroComprobante;
        end                                                                    		// SS_1254_MBE_20150504
        else begin                                                             		// SS_1254_MBE_20150504
        	// si no est� habilitado el dNumeroComprobante,   						 // SS_1254_MBE_20150504
            // entonces por definici�n est� habilitado el RUT         		 	  // SS_1254_MBE_20150504
            //
            // esto se hace x q da un error de "cannot focus a disabled...window"	// SS_1254_MBE_20150504
            ActiveControl := peNumeroDocumento;                                		// SS_1254_MBE_20150504
        end;                                                                   		// SS_1254_MBE_20150504

        chkbxSeleccionManual.Enabled := False;
        btnDesmarcarTodos.Enabled    := False;
        Result := True;
    except
	   	  on e: exception do begin
			      MsgBoxErr(MSG_ERROR_INICIALIZAR, E.Message, MSG_CAPTION, MB_ICONERROR);
			      Result := False;
        end;
    end; // except

    lblEnListaAmarilla.Visible:=False;                                                    //SS_660_MVI_20130711
end;

{******************************** Function Header ******************************
Function Name: PoseeCanalDePago
Author : vpaszkowicz
Date Created : 24/08/2007
Description : Retorna True si existe al menos un Canal de Pago habilidato para
ese usuario.
Parameters : None
Return Value : Boolean
*******************************************************************************}
function TformCobroComprobantes.PoseeCanalDePago: Boolean;
var
    sp: TADOStoredProc;
begin
    Result := False;
    sp := TADOStoredProc.Create(Nil);
    try
        sp.Connection       := DmConnections.BaseCAC;
        sp.ProcedureName    := 'ObtenerCanalesPago';
        sp.Parameters.Refresh;
        sp.Open;
        while (not sp.Eof) and not Result do begin
            if (not sp.FieldByName('Funcion').IsNull) and (ExisteAcceso(Trim(sp.FieldByName('Funcion').Value))) then begin
                Result := True;
            end;
            sp.Next;
        end;
    finally
        sp.Close;
        sp.Free;
    end;
end;
{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    flamas
  Date Created: 21/12/2004
  Description: Inicializa el Form - Carga los tipos de Comprobantes y busca
				el comprobante seleccionado
  Parameters: TipoComprobante: AnsiString; NumeroComprobante: Int64
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TformCobroComprobantes.Inicializar(TipoComprobante: AnsiString; NumeroComprobante: Int64; NumeroPOS, PuntoEntrega, PuntoVenta: Integer): Boolean;
resourcestring
	MSG_LOGO_NO_ENCONTRADO = 'No se ha encontrado el archivo con la imagen del logo para el recibo.';
	MSG_CAPTION			   = 'Cobro de Comprobantes';
	MSG_ERROR_INICIALIZAR   = 'Error inicializando el formulario.';
begin
    try
        Result := False;
        FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
        FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
        FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
        FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
        Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

//        CenterForm(Self);
    // Se cargan las impresoras y se ejecuta el OnChange de los Tipos de Comprobantes para que muestre los componentes que son necesarios
        CargarImpresorasFiscales(DMConnections.BaseCAC, cbImpresorasFiscales, 1);
        MostrarControlesSegunComprobante(cbTiposComprobantes.Value);

    //SS 762
    FMinHeight := ClientHeight;
    FMinWidth  := ClientWidth;        

        FAhora              := Trunc(NowBaseSmall(DMConnections.BaseCAC));
//        FEstadoPagado       := QueryGetValueInt(DMConnections.BaseCAC, SQL_PAGADO);
//        FFormaPagoPagare    := QueryGetValueInt(DMConnections.BaseCAC, SQL_FORMA_PAGO_PAGARE);
//        FFormaPagoEfectivo  := QueryGetValueInt(DMConnections.BaseCAC, SQL_FORMA_PAGO_EFECTIVO);
//        FEstadoReemplazado  := QueryGetValueInt(DMConnections.BaseCAC, SQL_REEMPLAZADO);

        CargarConstantesCobranzasRefinanciacion;
        CargarConstantesCobranzasCanalesFormasPago;

        FbtnCobrarCuotaPermisos           := ExisteAcceso('cobrar_cuota_cob_comp_ref');
        FbtnActivarRefinanciacionPermisos := ExisteAcceso('activar_ref_Cob_comp_ref');
        FvcbRefinanciacionesPermisos      := ExisteAcceso('Combo_ref_cob_comp_ref');

        vcbRefinanciacion.Enabled         := False;

        if not PoseeCanalDePago then begin
            //MsgBoxErr(MSG_ERROR_INICIALIZAR, MSG_SIN_PERMISOS, MSG_CAPTION, MB_ICONERROR);
            MsgBox(MSG_SIN_PERMISOS, MSG_CAPTION, MB_ICONINFORMATION + MB_OK);
            Exit;
        end;
        FTotalComprobantesCobrar := 0;
        FCantidadComprobantesCobrar := 0;
        FCantidadCuotasCobrar := 0;
        FTotalCuotasCobrar := 0;

        lblCuotasCantidad.Caption := '0';
        lblCuotasTotal.Caption    := '$ 0';

        lblMontoDeudasConvenios.Caption         := '$ 0'; // SS_637_PDO_20101227
        lblMontoDeudasRefinanciacion.Caption    := '$ 0'; // SS_637_PDO_20101227
        lblMontoDeudasConvenios.Font.Color      := clBlack;
        lblMontoDeudasRefinanciacion.Font.Color := clBlack;

        FCodigoCliente := -1;
        FCodigoCanalPago := CONST_CANAL_PAGO_VENTANILLA;

        // Por ahora lo cargamos a mano
        LimpiarDatosCliente;

        lbl_Estado.Caption := EmptyStr;
        CargarTiposComprobantes;
        cbTiposComprobantes.Value := TipoComprobante;
        edNumeroComprobante.Value := NumeroComprobante;
        FNumeroPOS := NumeroPOS;
        FPuntoEntrega := PuntoEntrega;
        FPuntoVenta := PuntoVenta;

        cds_Comprobantes.Close;
        cds_Comprobantes.CreateDataSet;
        cds_Comprobantes.Open;

        FormStyle := fsMDIChild;
        CenterForm(Self);

        Result := CargarComprobantes;

        if Result then begin

            FLogo := TBitmap.Create;
            (* Levanto el logo aqu� para que se haga una sola vez, independientemente de los reintentos
            de impresi�n que haga el operador. *)
//            if not LevantarLogo (FLogo) then MsgBox(MSG_LOGO_NO_ENCONTRADO, MSG_CAPTION, MB_ICONINFORMATION + MB_OK);
        	  edNumeroComprobante.SetFocus();
        end;
    except
        on e: exception do begin
      			MsgBoxErr(MSG_ERROR_INICIALIZAR, E.Message, MSG_CAPTION, MB_ICONERROR);
    			Result := False;
        end;
    end; // except

end;

{-----------------------------------------------------------------------------
  Function Name: FormCreate
  Author: flamas
  Date Created: 13/10/2005
  Description: centro el formulario al crearlo
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure TformCobroComprobantes.FormCreate(Sender: TObject);
begin

end;

{-----------------------------------------------------------------------------
  Procedure Name: VerificarComprobanteEnCarpetaLegal
  Author: nefernandez
  Date Created: 23/03/2007
  Description: Verificar si el comprobante pertenece a un convenio que esta
                en Carpeta Legal
  Parameters:
-----------------------------------------------------------------------------}
function TformCobroComprobantes.VerificarComprobanteEnCarpetaLegal(ModoSilencioso: Boolean): Boolean; // SS_637_20100929
resourcestring
    MSG_CONVENIO_CARPETA_LEGAL = 'Atenci�n: Este comprobante corresponde a un Convenio que est� en Cobranza Judicial.' 
                        + CHR(13) + 'NO debe recibir el pago.' + CHR(13) +
                        'Desea continual igual ?';
    MSG_CONVENIO_CARPETA_LEGAL_EMPRESA = 'Atenci�n: Este comprobante corresponde a un Convenio que est� en Cobranza Judicial.' 
                        + CHR(13) + 'Este convenio fue asignado a la empresa: "%s."'
                        + CHR(13) + 'NO debe recibir el pago.' + CHR(13) +
                        'Desea continual igual ?';
    MSG_COMPROBANTE_EN_COBRANZA = 'Comprobante %s, N�mero %s, Empresa %s'; // SS_637_20100929
var
    Empresa, Mensaje: string;
begin
    if (cds_Comprobantes.FieldByName('Cobrar').AsBoolean or ModoSilencioso) and
        (ChequearConvenioEnCarpetaLegal(cds_Comprobantes.FieldByName('CodigoConvenio').Value)) then begin
        // El usuario tiene la opcion de cobrar o no los comprobantes en carpeta legal
        Empresa := Trim(ObtenerEmpresaConvenioEnCarpetaLegal(cds_Comprobantes.FieldByName('CodigoConvenio').Value));
		// SS_637_20100929
        if not ModoSilencioso then begin
            if Empresa = '' then
                Mensaje := MSG_CONVENIO_CARPETA_LEGAL
            else Mensaje := Format(MSG_CONVENIO_CARPETA_LEGAL_Empresa, [Empresa]);
            if MsgBox(Mensaje, self.Caption, MB_ICONWARNING or MB_YESNO) <> IDYES then
                cds_Comprobantes.FieldByName('Cobrar').AsBoolean := not cds_Comprobantes.FieldByName( 'Cobrar' ).AsBoolean;
        end
        else FComprobantesEnCobranza.Add(
                Format(
                    MSG_COMPROBANTE_EN_COBRANZA,
                    [cds_Comprobantes.FieldByName('TipoComprobante').AsString,
                     cds_Comprobantes.FieldByName('NumeroComprobante').AsString,
                     Empresa]));
		// SS_637_20100929
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: LimpiarDatosCliente
  Author: flamas
  Date Created: 13/10/2005
  Description: Limpia los datos del clientes
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure TformCobroComprobantes.LimpiarDatosCliente;
begin
    lblRUT.Caption := EmptyStr;
    lblNombre.Caption := EmptyStr;
    lblDomicilio.Caption := EmptyStr;
    LimpiarRefinanciaciones;
    lblEnListaAmarilla.Visible  := False;                                         //SS_660_MVI_20130909
    lblEnListaAmarilla.Caption  := EmptyStr;                                      //SS_660_MVI_20130909
end;

{-----------------------------------------------------------------------------
  Function Name: CargarTiposComprobantes;
  Author:    flamas
  Date Created: 22/01/2005
  Description: Carga los tipos de Comprobantes
  Parameters: TipoComprobante: AnsiString;
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure TformCobroComprobantes.CargarTiposComprobantes;
resourcestring
	MSG_ALL_INVOICES = 'Todos';
begin
  	cbTiposComprobantes.Items.Clear;
    cbTiposComprobantes.Items.Add(MSG_ALL_INVOICES,  null);
    cbTiposComprobantes.Items.Add(DescriTipoComprobante(TC_NOTA_COBRO), TC_NOTA_COBRO);
    cbTiposComprobantes.Items.Add(DescriTipoComprobante(TC_FACTURA), TC_FACTURA);
  	cbTiposComprobantes.Items.Add(DescriTipoComprobante(TC_BOLETA), TC_BOLETA);
    cbTiposComprobantes.Items.Add(DescriTipoComprobante(TC_NOTA_DEBITO), TC_NOTA_DEBITO);
    // Rev.15 / 14-04-2009 / Nelson Droguett Sierra--------------------------------------
    cbTiposComprobantes.Items.Add(DescriTipoComprobante(TC_FACTURA_EXENTA), TC_FACTURA_EXENTA);
    cbTiposComprobantes.Items.Add(DescriTipoComprobante(TC_FACTURA_AFECTA), TC_FACTURA_AFECTA);
    cbTiposComprobantes.Items.Add(DescriTipoComprobante(TC_BOLETA_EXENTA), TC_BOLETA_EXENTA);
    cbTiposComprobantes.Items.Add(DescriTipoComprobante(TC_BOLETA_AFECTA), TC_BOLETA_AFECTA);
    // ----------------------------------------------------------------------------------
    cbTiposComprobantes.Items.Add(DescriTipoComprobante(TC_TRASPASO_DEBITO), TC_TRASPASO_DEBITO);		//SS_740_MBE_20110720
    cbTiposComprobantes.Items.Add(DescriTipoComprobante(TC_DEVOLUCION_DINERO), TC_DEVOLUCION_DINERO);	//SS_959_MBE_20110721

    cbTiposComprobantes.Items.Add(DescriTipoComprobante(TC_SALDO_INICIAL_DEUDOR), TC_SALDO_INICIAL_DEUDOR);	                //SS-1015-NDR-20120516
    cbTiposComprobantes.Items.Add(DescriTipoComprobante(TC_SALDO_INICIAL_ACREEDOR), TC_SALDO_INICIAL_ACREEDOR);	            //SS-1015-NDR-20120516
    cbTiposComprobantes.Items.Add(DescriTipoComprobante(TC_CUOTAS_OTRAS_CONCESIONARIAS), TC_CUOTAS_OTRAS_CONCESIONARIAS);	  //SS-1015-NDR-20120516

//    cbTiposComprobantes.Value := TC_NOTA_COBRO;							// SS_1254_MBE_20150504
    cbTiposComprobantes.Value := null;										// SS_1254_MBE_20150504
    cbTiposComprobantes.OnChange(cbTiposComprobantes);
end;



{-----------------------------------------------------------------------------
  Function Name: peNumeroDocumentoChange
  Author:    flamas
  Date Created: 20/12/2004
  Description: Actualiza los Convenios y Comprobantes Impagos de un cliente determinado
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformCobroComprobantes.peNumeroDocumentoChange(Sender: TObject);
{INICIO:	TASK_117_CFU_20170308
var
CodCliente : Integer;     //TASK_013_GLE_20161104
TERMINO:	TASK_117_CFU_20170308}
begin
    if ActiveControl <> Sender then 
    	Exit;
    edNumeroComprobante.Clear;
    LimpiarDetalles;
    LimpiarConvenios;
    LimpiarListaComprobantes;
    LimpiarRefinanciaciones;
    //if Length(Trim(peNumeroDocumento.Text)) >= 8 then begin                                                                                                                            //SS-1014-NDR-20120123
    //    FCodigoCliente := QueryGetValueInt(DMConnections.BaseCAC,                                                                                                                      //SS-1014-NDR-20120123
    //                                         Format('SELECT CodigoPersona FROM Personas WITH (NOLOCK) WHERE CodigoDocumento = ''RUT'' ' + 'AND NumeroDocumento = ''%s''',              //SS-1014-NDR-20120123
    //                                                 [iif(peNumeroDocumento.Text <> '',                                                                                                //SS-1014-NDR-20120123
    //                                                      trim(PadL(peNumeroDocumento.Text, 9, '0')),                                                                                  //SS-1014-NDR-20120123
    //                                                      '')                                                                                                                          //SS-1014-NDR-20120123
    //                                                 ]                                                                                                                                 //SS-1014-NDR-20120123
    //                                               )                                                                                                                                   //SS-1014-NDR-20120123
    //                                        );                                                                                                                                         //SS-1014-NDR-20120123
    //    if FCodigoCliente > 0 then begin                                                                                                                                               //SS-1014-NDR-20120123
    //        CargarConveniosRUT(DMConnections.BaseCAC, cbConvenios, 'RUT', peNumeroDocumento.Text, False, 0, True);                                                                     //SS-1014-NDR-20120123
    //        MostrarDatosCliente(FCodigoCliente);                                                                                                                                       //SS-1014-NDR-20120123
    //        //CargarComprobantes;                                                                                                                                                      //SS-1014-NDR-20120123
    //      end                                                                                                                                                                          //SS-1014-NDR-20120123
    //    else LimpiarDatosCliente;                                                                                                                                                      //SS-1014-NDR-20120123
    //end                                                                                                                                                                                //SS-1014-NDR-20120123
    //else                                                                                                                                                                               //SS-1014-NDR-20120123
    LimpiarDatosCliente;

    {INICIO:	TASK_117_CFU_20170308
    //TASK_013_GLE_20161104 inicia - Carga de Datos del Cliente autom�ticamente en formulario principal de Cobro Comprobantes
    CodCliente := QueryGetValueInt( DMConnections.BaseCAC,
                                            Format('SELECT CodigoPersona FROM Personas WITH (NOLOCK) WHERE CodigoDocumento = ''RUT'' ' + 'AND NumeroDocumento = ''%s''',
                                            [iif(peNumeroDocumento.Text <> '', trim(PadL(peNumeroDocumento.Text, 9, '0')), '')]));
        if CodCliente > 0 then
        begin
            CargarConveniosRUT(DMConnections.BaseCAC, cbConvenios, 'RUT', peNumeroDocumento.Text, False, 0, True);
            MostrarDatosCliente(CodCliente);
            cbConvenios.ItemIndex := 1;
            CargarComprobantes;

        end;
    }
    FCodigoCliente := QueryGetValueInt( DMConnections.BaseCAC,
                                            Format('SELECT CodigoPersona FROM Personas WITH (NOLOCK) WHERE CodigoDocumento = ''RUT'' ' + 'AND NumeroDocumento = ''%s''',
                                            [iif(peNumeroDocumento.Text <> '', trim(PadL(peNumeroDocumento.Text, 9, '0')), '')]));
        if FCodigoCliente > 0 then
        begin
            CargarConveniosRUT(DMConnections.BaseCAC, cbConvenios, 'RUT', peNumeroDocumento.Text, False, 0, True);
            MostrarDatosCliente(FCodigoCliente);
            cbConvenios.ItemIndex := 1;
            CargarComprobantes;

        end;
    
    //TERMINO:	TASK_117_CFU_20170308
    //TASK_013_GLE_2016110 termina Carga de Datos
end;

{-----------------------------------------------------------------------------
  Function Name: cbConveniosChange
  Author:    flamas
  Date Created: 21/12/2004
  Description: Procesa un Cambio de Convenio - Trae los comprobantes impagos
				del dicho Convenio
  Parameters: Sender: TObject
  Return Value: None

 Revision 5:
  Author:    jconcheyro
  Date:      22-Nov-2005
  Description: al Onchange del cbConvenios se le pone una busqueda de domicilio de facturacion kanav 1509

 -----------------------------------------------------------------------------}
procedure TformCobroComprobantes.cbConveniosChange(Sender: TObject);
resourcestring
	MSG_ERROR = 'Error';
	MSG_ERROR_GETTING_ADDRESS = 'Error obteniendo el domicilio del cliente';
    MSG_CONVENIOENLISTAAMARILLA = 'Convenio se encuentra en lista amarilla';                         //SS_660_NDR_20121002
    //SQL_ESTACONVENIOENLISTAAMARILLA = 'SELECT dbo.EstaConvenioEnListaAmarilla(''%s'',GETDATE() )'; //SS_660_CQU_20130711   //SS_660_NDR_20121002            // SS_660_CQU_20121010  //SS_660_MVI_20130711
var                                                                                                  //SS_660_NDR_20121002
    CodigoConvenio:Integer;                                                                          //SS_660_NDR_20121002
    EstadoListaAmarilla : String;                                                                    //SS_660_MVI_20130729
begin

    //lblEnListaAmarilla.Color:=clYellow;                                                            // SS_660_CQU_20130711 //SS_660_MVI_20130711
    //lblEnListaAmarilla.Visible  := QueryGetValue(   DMConnections.BaseCAC,                         // SS_660_CQU_20130711 //SS_660_MVI_20130711
    //                                                    Format(SQL_ESTACONVENIOENLISTAAMARILLA,    // SS_660_CQU_20130711 //SS_660_MVI_20130711
    //                                                            [   cbConvenios.Value              // SS_660_CQU_20130711 //SS_660_MVI_20130711
    //                                                            ]                                  // SS_660_CQU_20130711 //SS_660_MVI_20130711
    //                                                          )                                    // SS_660_CQU_20130711 //SS_660_MVI_20130711
    //                                                 )='True';                                     // SS_660_CQU_20130711 //SS_660_MVI_20130711


    CodigoConvenio:=cbConvenios.Value;                                                               //SS_660_NDR_20121002
    if ActiveControl <> Sender then Exit;
    Cargarcomprobantes;
    with spObtenerDomicilioFacturacion, Parameters do
    begin
        // Mostramos el domicilio de Facturaci�n
        Close;
        ParamByName('@CodigoConvenio').Value := cbConvenios.Value;
        try
          ExecProc;
          lblDomicilio.Caption := ParamByName('@Domicilio').Value + ' - ' + ParamByName('@Comuna').Value;
        except
          on e: exception do begin
              MsgBoxErr( MSG_ERROR_GETTING_ADDRESS, e.Message, MSG_ERROR, MB_ICONSTOP);
            end;
        end;
    end;
    //if  QueryGetValue(   DMConnections.BaseCAC,                                                              //SS_660_NDR_20121002    // SS_660_CQU_20121010
    //                     Format(SQL_ESTACONVENIOENLISTAAMARILLA,                                             //SS_660_NDR_20121002    // SS_660_CQU_20121010
    //                            [   CodigoConvenio,                                                          //SS_660_NDR_20121002    // SS_660_CQU_20121010
    //                                FormatDateTime('yyyymmdd hh:nn:ss',                                      //SS_660_NDR_20121002    // SS_660_CQU_20121010
    //                                QueryGetDateTimeValue(DMConnections.BaseCAC,'SELECT GETDATE()'))         //SS_660_NDR_20121002    // SS_660_CQU_20121010
    //                            ]                                                                            //SS_660_NDR_20121002    // SS_660_CQU_20121010
    //                           )                                                                             //SS_660_NDR_20121002    // SS_660_CQU_20121010
    //                 )='True' then                                                                           //SS_660_NDR_20121002    // SS_660_CQU_20121010
    //if VerificarConvenioListaAmarilla(CodigoConvenio) then                                                   //SS_660_MVI_20130729  //SS_660_MVI_20130729    // SS_660_CQU_20121010
    //begin                                                                                                    //SS_660_MVI_20130729
    //  ShowMsgBoxCN(Self.Caption, MSG_CONVENIOENLISTAAMARILLA, MB_ICONINFORMATION, Self);                     //SS_660_MVI_20130729
    //  lblEnListaAmarilla.Visible := True;   // SS_660_CQU_20130711                                           //SS_660_MVI_20130729
    //    lblEnListaAmarilla.Visible  := True;                                                                 //SS_660_MVI_20130729
    //    lblEnListaAmarilla.Caption  := EstadoListaAmarilla;                                                  //SS_660_MVI_20130729
  //end;
    EstadoListaAmarilla:= VerificarConvenioListaAmarilla(CodigoConvenio);                                                                //SS_660_MVI_20130729
//    if EstadoListaAmarilla <> 'NULL' then begin                                                                                        //SS_660_MVI_20130909  //SS_660_MVI_20130729
   if EstadoListaAmarilla <> '' then begin                                                                                               //SS_660_MVI_20130909  //SS_660_MVI_20130729
    lblEnListaAmarilla.Visible  := True;                                                                                                 //SS_660_MVI_20130729
    lblEnListaAmarilla.Caption  := EstadoListaAmarilla;                                                                                  //SS_660_MVI_20130729
                                                                                                                                         //SS_660_MVI_20130729
    end                                                                                                                                  //SS_660_MVI_20130729
    else begin                                                                                                                           //SS_660_MVI_20130729
                                                                                                                                         //SS_660_MVI_20130729
    lblEnListaAmarilla.Visible  := False;                                                                                                //SS_660_MVI_20130729
                                                                                                                                         //SS_660_MVI_20130729
    end;                                                                                                                                 //SS_660_MVI_20130729

end;

{-----------------------------------------------------------------------------
  Function Name: CargarComprobantes
  Author:    flamas
  Date Created: 20/12/2004
  Description: Carga los comprobantes impagos de un Convenio determinado
  Parameters: None
  Return Value: None

  Revision : 16
  Date: 17/11/2010
  Author: pdominguez
  Description : SS 527 - salir todas NK seleccionadas y advertencia anteriores impagas
        - Si la b�squeda de comprobantes es por rut, marcaremos todos los comprobantes.
        - Si se busca un comprobante en concreto, a parte de marcar ese, marcaremos
        todos los anteriores que haya.
-----------------------------------------------------------------------------}
function TformCobroComprobantes.CargarComprobantes : boolean;
const
    SQL_ObtenerPersonaComprobante = 'SELECT dbo.ObtenerPersonaComprobante(''%s'', %d, %s)';
resourcestring
    MSG_ERROR_ON_LOAD = 'Ha ocurrido un error al intentar obtener los Comprobantes Impagos.';
    MSG_COMPROBANTES_COBRANZA_JUDICIAL = 'Existen Comprobantes en Cobranza Judicial';
    MSG_MULTIPLES_COMPROBANTES_EN_COBRANZA = 'El Convenio tiene un total de %d comprobantes en Cobranza Judicial.';
var
    EncontradoComprobante: Boolean;
    EstadoListaAmarilla : string;
    ObtenerComprobantesImpagos : TADOStoredProc;
    ObtenerComprobantesImpagosOtros : TADOStoredProc;        //TASK_014_GLE_20161107 agrega variable para almacenar comprobantes seleccionados como Nota de Cobro
begin

    TPanelMensajesForm.MuestraMensaje('Cargando Comprobantes y Datos del Cliente ...', True);
    FTipoComprobante := iif (cbTiposComprobantes.Value = Null, EmptyStr, cbTiposComprobantes.Value);
    LimpiarListaComprobantes;
  	cds_Comprobantes.DisableControls;
    Result := False ;
    FTotalComprobantesCobrar := 0;
    FSaldoConvenios := 0; //Revision 10 inicializamos la variable de saldo del convenio si eligio un solo comprobante, o de todos los convenios si eligio un rut
    EncontradoComprobante := False; // Rev. 16 SS_527_20101117
  	try
        FComprobantesEnCobranza := TStringList.Create; //_637_20100927

        try
            // Se obtienen los datos
            TPanelMensajesForm.MuestraMensaje('Obteniendo Comprobantes Impagos ...');
{INICIO: TASK_041_JMA_20160711
            ObtenerComprobantesImpagos.Close;
            ObtenerComprobantesImpagos.Parameters.ParamByName('@CodigoCliente').Value := iif(FCodigoCliente <= 0, Null, FCodigoCliente);
            ObtenerComprobantesImpagos.Parameters.ParamByName('@CodigoConvenio').Value := iif(cbConvenios.Value = 0, Null, cbConvenios.Value);
            ObtenerComprobantesImpagos.Parameters.ParamByName('@TipoComprobante').Value := cbTiposComprobantes.Value;
            ObtenerComprobantesImpagos.Parameters.ParamByName('@NumeroComprobante').Value := iif((cbTiposComprobantes.Value <> TC_BOLETA) and (edNumeroComprobante.Text <> ''), edNumeroComprobante.Value, Null);
            ObtenerComprobantesImpagos.Parameters.ParamByName('@NumeroComprobanteFiscal').Value := iif((cbTiposComprobantes.Value = TC_BOLETA) and (edNumeroComprobante.Text <> ''), edNumeroComprobante.Value, Null);
            ObtenerComprobantesImpagos.Parameters.ParamByName('@CodigoImpresoraFiscal').Value := iif((cbTiposComprobantes.Value = TC_BOLETA) and (edNumeroComprobante.Text <> ''), cbImpresorasFiscales.Value, Null);
}
            ObtenerComprobantesImpagos:= TADOStoredProc.Create(nil);
            ObtenerComprobantesImpagos.Connection := DMConnections.BaseCAC;
            ObtenerComprobantesImpagos.ProcedureName := 'ObtenerComprobantesImpagos';
            ObtenerComprobantesImpagos.Parameters.Refresh;
            ObtenerComprobantesImpagos.Parameters.ParamByName('@TipoComprobanteFiscal').Value := cbTiposComprobantes.Value;
            ObtenerComprobantesImpagos.Parameters.ParamByName('@NumeroComprobanteFiscal').Value := edNumeroComprobante.Value;
            ObtenerComprobantesImpagos.Parameters.ParamByName('@CodigoCliente').Value := iif(FCodigoCliente <= 0, Null, FCodigoCliente);
            ObtenerComprobantesImpagos.Parameters.ParamByName('@CodigoConvenio').Value := iif(cbConvenios.Value = 0, Null, cbConvenios.Value);

{TERMINO: TASK_041_JMA_20160711}
            ObtenerComprobantesImpagos.Open;

            if ObtenerComprobantesImpagos.RecordCount > 0 then begin
                if FCodigoCliente <= 0 then begin
                    FCodigoCliente := ObtenerComprobantesImpagos.FieldByName('CodigoPersona').Value;
                    MostrarDatosCliente(FCodigoCliente);
                end;
                pcOpcionesComprobantes.ActivePageIndex := 0;
            end
            else begin
                if cbTiposComprobantes.Value <> Null then begin
                    TPanelMensajesForm.MuestraMensaje('Obteniendo Datos Cliente ...');
                    FCodigoCliente := QueryGetValueInt(DMConnections.BaseCAC,
                            Format(SQL_ObtenerPersonaComprobante,
                                [cbTiposComprobantes.Value, StrToIntDef(edNumeroComprobante.Text, 0), iif((cbTiposComprobantes.Value = TC_BOLETA), cbImpresorasFiscales.Value, 'NULL')]));

                    if FCodigoCliente <> -1 then begin
                        MostrarDatosCliente(FCodigoCliente);
                        //TASK_014_GLE_20161107 inicia llamado a procedimiento almacenado
                        ObtenerComprobantesImpagosOtros:= TADOStoredProc.Create(nil);
                        ObtenerComprobantesImpagosOtros.Connection := DMConnections.BaseCAC;
                        ObtenerComprobantesImpagosOtros.ProcedureName := 'ObtenerComprobantesImpagos';
                        ObtenerComprobantesImpagosOtros.Parameters.Refresh;
                        ObtenerComprobantesImpagosOtros.Parameters.ParamByName('@TipoComprobanteFiscal').Value := null;  //TASK_014_GLE_20161107  par�metro null que retorna comprobantes requeridos
                        ObtenerComprobantesImpagosOtros.Parameters.ParamByName('@NumeroComprobanteFiscal').Value := edNumeroComprobante.Value;
                        ObtenerComprobantesImpagosOtros.Parameters.ParamByName('@CodigoCliente').Value := iif(FCodigoCliente <= 0, Null, FCodigoCliente);
                        ObtenerComprobantesImpagosOtros.Parameters.ParamByName('@CodigoConvenio').Value := iif(cbConvenios.Value = 0, Null, cbConvenios.Value);
                        ObtenerComprobantesImpagosOtros.Parameters.ParamByName('@ErrorDescription').Value := Null;
                        ObtenerComprobantesImpagosOtros.Open;
                        //TASK_014_GLE_20161107 termina
                    end;
                end;

                if cdsCuotasAPagar.Active then begin
                    if cdsCuotasAPagar.RecordCount > 0 then begin
                        pcOpcionesComprobantes.ActivePageIndex := 1;
                    end;
                end;
            end;

            // Se carga el ClientDataSet
            TPanelMensajesForm.MuestraMensaje('Cargando Comprobantes Impagos ...');

            while not ObtenerComprobantesImpagos.Eof do begin
                cds_Comprobantes.Append;

                cds_Comprobantes.FieldByName('TipoComprobante').AsString := ObtenerComprobantesImpagos.FieldByName('TipoComprobante').AsString;
                cds_Comprobantes.FieldByName('NumeroComprobante').AsInteger := ObtenerComprobantesImpagos.FieldByName('NumeroComprobante').AsInteger;
                cds_Comprobantes.FieldByName('DescriComprobante').AsString := ObtenerComprobantesImpagos.FieldByName('DescriComprobante').AsString;
                cds_Comprobantes.FieldByName('FechaEmision').AsDateTime := ObtenerComprobantesImpagos.FieldByName('FechaEmision').AsDateTime;
                cds_Comprobantes.FieldByName('FechaVencimiento').AsDateTime := ObtenerComprobantesImpagos.FieldByName('FechaVencimiento').asDateTime;
                cds_Comprobantes.FieldByName('TotalComprobante').AsFloat := ObtenerComprobantesImpagos.FieldByName('TotalComprobante').AsFloat;
                cds_Comprobantes.FieldByName('TotalComprobanteDescri').AsString := ObtenerComprobantesImpagos.FieldByName('TotalComprobanteDescri').AsString;
                cds_Comprobantes.FieldByName('SaldoPendiente').AsFloat := ObtenerComprobantesImpagos.FieldByName('SaldoPendiente').AsFloat;
                cds_Comprobantes.FieldByName('SaldoPendienteDescri').AsString := ObtenerComprobantesImpagos.FieldByName('SaldoPendienteDescri').AsString;
                cds_Comprobantes.FieldByName('EstadoPago').AsString := ObtenerComprobantesImpagos.FieldByName('EstadoPago').AsString;
                cds_Comprobantes.FieldByName('DescriEstado').AsString := ObtenerComprobantesImpagos.FieldByName('DescriEstado').AsString;
                cds_Comprobantes.FieldByName('EstadoDebito').AsString := ObtenerComprobantesImpagos.FieldByName('EstadoDebito').AsString;
                cds_Comprobantes.FieldByName('UltimoComprobante').AsInteger := iif(ObtenerComprobantesImpagos.FieldByName('UltimoComprobante').IsNull, -1, ObtenerComprobantesImpagos.FieldByName('UltimoComprobante').AsInteger);
                cds_Comprobantes.FieldByName('CodigoConvenio').AsInteger := ObtenerComprobantesImpagos.FieldByName('CodigoConvenio').AsInteger;
                cds_Comprobantes.FieldByName('CodigoPersona').AsInteger := ObtenerComprobantesImpagos.FieldByName('CodigoPersona').AsInteger;
                cds_Comprobantes.FieldByName('TotalPagos').AsFloat := ObtenerComprobantesImpagos.FieldByName('TotalPagos').AsFloat;
                cds_Comprobantes.FieldByName('TotalPagosStr').AsString := ObtenerComprobantesImpagos.FieldByName('TotalPagosStr').AsString;
                //Revision 10 cargamos los nuevos valores del sp en los nuevos campos del client data set
                cds_Comprobantes.FieldByName('DescTipoMedioPAgo').AsString := ObtenerComprobantesImpagos.FieldByName('DescTipoMedioPAgo').AsString;
                cds_Comprobantes.FieldByName('DescPATPAC').AsString := ObtenerComprobantesImpagos.FieldByName('DescPATPAC').AsString;
                cds_Comprobantes.FieldByName('Rechazo').AsString := ObtenerComprobantesImpagos.FieldByName('Rechazo').AsString;
                //Fin de Revision 10
                // Si el comprobante fue cargado en el edit lo marcamos

                // Rev.15 / 14-04-2009 / Nelson Droguett Sierra ------------------------------------------------------
                cds_Comprobantes.FieldByName('TipoComprobanteFiscal').AsString := ObtenerComprobantesImpagos.FieldByName('TipoComprobanteFiscal').AsString;
                cds_Comprobantes.FieldByName('NumeroComprobanteFiscal').AsInteger := ObtenerComprobantesImpagos.FieldByName('NumeroComprobanteFiscal').AsInteger;
                cds_Comprobantes.FieldByName('DescriComprobanteFiscal').AsString := ObtenerComprobantesImpagos.FieldByName('DescriComprobanteFiscal').AsString;
                // ---------------------------------------------------------------------------------------------------

                cds_Comprobantes.FieldByName('EstadoRefinanciacion').AsInteger := ObtenerComprobantesImpagos.FieldByName('EstadoRefinanciacion').AsInteger;

                FTotal :=  ObtenerComprobantesImpagos.FieldByName('SaldoPendiente').AsInteger; //TASK_009_GLE_20161025 asigna saldo pendiente del comprobante a la variable

                if not (ObtenerComprobantesImpagos.FieldByName('EstadoRefinanciacion').AsInteger in [REFINANCIACION_ESTADO_EN_TRAMITE, REFINANCIACION_ESTADO_ACEPTADA]) then begin
                    // Rev. 16 SS_527_20101117
                    if cbTiposComprobantes.Value = NULL then begin
                        cds_Comprobantes.FieldByName('Cobrar').AsBoolean := True;
                        inc(FCantidadComprobantesCobrar);
                        FTotalComprobantesCobrar := FTotalComprobantesCobrar + cds_Comprobantes.FieldByName('SaldoPendiente').AsFloat;
                    end
                    else begin
                        if not EncontradoComprobante then begin
                            cds_Comprobantes.FieldByName('Cobrar').AsBoolean := True;
                            inc(FCantidadComprobantesCobrar);
                            FTotalComprobantesCobrar := FTotalComprobantesCobrar + cds_Comprobantes.FieldByName('SaldoPendiente').AsFloat;

                            if ((cds_Comprobantes.FieldByName('TipoComprobante').asString = cbTiposComprobantes.Value) and
                                   (cds_Comprobantes.FieldByName('NumeroComprobante').asInteger = edNumeroComprobante.ValueInt)) or
                               ((cds_Comprobantes.FieldByName('TipoComprobanteFiscal').asString = cbTiposComprobantes.Value) and
                                    (cds_Comprobantes.FieldByName('NumeroComprobanteFiscal').asInteger = edNumeroComprobante.ValueInt)) then
                                EncontradoComprobante := True;
                        end
                        else cds_Comprobantes.FieldByName('Cobrar').AsBoolean := False;
                    end;
                end;
                // Fin Rev. 16 SS_527_20101117

                // Revision 9
                VerificarComprobanteEnCarpetaLegal(True); // SS_637_20100927

                cds_Comprobantes.Post;
                ObtenerComprobantesImpagos.Next;

                FSaldoConvenios := FSaldoConvenios +  FTotal;  //TASK_009_GLE_20161025 corrige l�nea siguiente comentada
                //FSaldoConvenios := FSaldoConvenios +  ObtenerComprobantesImpagos.FieldByName('SaldoConvenio').AsInteger  //INICIO: TASK_041_JMA_20160711

            end;

            //TASK_014_GLE_20161107 inicia m�dulo que carga las notas de cobros seg�n el n�mero de comprobante
            if edNumeroComprobante.Value <> 0 then begin

                TPanelMensajesForm.MuestraMensaje('Cargando Nota de Cobro ...');
                while not ObtenerComprobantesImpagosOtros.Eof do begin
                    if ObtenerComprobantesImpagosOtros.FieldByName('NumeroComprobante').AsInteger = edNumeroComprobante.ValueInt then begin
                    cds_Comprobantes.Append;

                    cds_Comprobantes.FieldByName('TipoComprobante').AsString := ObtenerComprobantesImpagosOtros.FieldByName('TipoComprobante').AsString;
                    cds_Comprobantes.FieldByName('NumeroComprobante').AsInteger := ObtenerComprobantesImpagosOtros.FieldByName('NumeroComprobante').AsInteger;
                    cds_Comprobantes.FieldByName('DescriComprobante').AsString := ObtenerComprobantesImpagosOtros.FieldByName('DescriComprobante').AsString;
                    cds_Comprobantes.FieldByName('FechaEmision').AsDateTime := ObtenerComprobantesImpagosOtros.FieldByName('FechaEmision').AsDateTime;
                    cds_Comprobantes.FieldByName('FechaVencimiento').AsDateTime := ObtenerComprobantesImpagosOtros.FieldByName('FechaVencimiento').asDateTime;
                    cds_Comprobantes.FieldByName('TotalComprobante').AsFloat := ObtenerComprobantesImpagosOtros.FieldByName('TotalComprobante').AsFloat;
                    cds_Comprobantes.FieldByName('TotalComprobanteDescri').AsString := ObtenerComprobantesImpagosOtros.FieldByName('TotalComprobanteDescri').AsString;
                    cds_Comprobantes.FieldByName('SaldoPendiente').AsFloat := ObtenerComprobantesImpagosOtros.FieldByName('SaldoPendiente').AsFloat;
                    cds_Comprobantes.FieldByName('SaldoPendienteDescri').AsString := ObtenerComprobantesImpagosOtros.FieldByName('SaldoPendienteDescri').AsString;
                    cds_Comprobantes.FieldByName('EstadoPago').AsString := ObtenerComprobantesImpagosOtros.FieldByName('EstadoPago').AsString;
                    cds_Comprobantes.FieldByName('DescriEstado').AsString := ObtenerComprobantesImpagosOtros.FieldByName('DescriEstado').AsString;
                    cds_Comprobantes.FieldByName('EstadoDebito').AsString := ObtenerComprobantesImpagosOtros.FieldByName('EstadoDebito').AsString;
                    cds_Comprobantes.FieldByName('UltimoComprobante').AsInteger := iif(ObtenerComprobantesImpagosOtros.FieldByName('UltimoComprobante').IsNull, -1, ObtenerComprobantesImpagos.FieldByName('UltimoComprobante').AsInteger);
                    cds_Comprobantes.FieldByName('CodigoConvenio').AsInteger := ObtenerComprobantesImpagosOtros.FieldByName('CodigoConvenio').AsInteger;
                    cds_Comprobantes.FieldByName('CodigoPersona').AsInteger := ObtenerComprobantesImpagosOtros.FieldByName('CodigoPersona').AsInteger;
                    cds_Comprobantes.FieldByName('TotalPagos').AsFloat := ObtenerComprobantesImpagosOtros.FieldByName('TotalPagos').AsFloat;
                    cds_Comprobantes.FieldByName('TotalPagosStr').AsString := ObtenerComprobantesImpagosOtros.FieldByName('TotalPagosStr').AsString;

                    cds_Comprobantes.FieldByName('DescTipoMedioPAgo').AsString := ObtenerComprobantesImpagosOtros.FieldByName('DescTipoMedioPAgo').AsString;
                    cds_Comprobantes.FieldByName('DescPATPAC').AsString := ObtenerComprobantesImpagosOtros.FieldByName('DescPATPAC').AsString;
                    cds_Comprobantes.FieldByName('Rechazo').AsString := ObtenerComprobantesImpagosOtros.FieldByName('Rechazo').AsString;

                    cds_Comprobantes.FieldByName('TipoComprobanteFiscal').AsString := ObtenerComprobantesImpagosOtros.FieldByName('TipoComprobanteFiscal').AsString;
                    cds_Comprobantes.FieldByName('NumeroComprobanteFiscal').AsInteger := ObtenerComprobantesImpagosOtros.FieldByName('NumeroComprobanteFiscal').AsInteger;
                    cds_Comprobantes.FieldByName('DescriComprobanteFiscal').AsString := ObtenerComprobantesImpagosOtros.FieldByName('DescriComprobanteFiscal').AsString;
                    cds_Comprobantes.FieldByName('EstadoRefinanciacion').AsInteger := ObtenerComprobantesImpagos.FieldByName('EstadoRefinanciacion').AsInteger;

                    FTotal :=  ObtenerComprobantesImpagosOtros.FieldByName('SaldoPendiente').AsInteger;

                    FSaldoConvenios := FSaldoConvenios +  FTotal;

                    if not (ObtenerComprobantesImpagosOtros.FieldByName('EstadoRefinanciacion').AsInteger in [REFINANCIACION_ESTADO_EN_TRAMITE, REFINANCIACION_ESTADO_ACEPTADA]) then begin

                        if cbTiposComprobantes.Value = NULL then begin
                            cds_Comprobantes.FieldByName('Cobrar').AsBoolean := True;
                            inc(FCantidadComprobantesCobrar);
                            FTotalComprobantesCobrar := FTotalComprobantesCobrar + cds_Comprobantes.FieldByName('SaldoPendiente').AsFloat;
                        end
                        else begin
                            if not EncontradoComprobante then begin
                                cds_Comprobantes.FieldByName('Cobrar').AsBoolean := True;
                                inc(FCantidadComprobantesCobrar);
                                FTotalComprobantesCobrar := FTotalComprobantesCobrar + cds_Comprobantes.FieldByName('SaldoPendiente').AsFloat;

                                if ((cds_Comprobantes.FieldByName('TipoComprobante').asString = cbTiposComprobantes.Value) and
                                       (cds_Comprobantes.FieldByName('NumeroComprobante').asInteger = edNumeroComprobante.ValueInt)) or
                                   ((cds_Comprobantes.FieldByName('TipoComprobanteFiscal').asString = cbTiposComprobantes.Value) and
                                        (cds_Comprobantes.FieldByName('NumeroComprobanteFiscal').asInteger = edNumeroComprobante.ValueInt)) then
                                    EncontradoComprobante := True;
                            end
                            else cds_Comprobantes.FieldByName('Cobrar').AsBoolean := False;
                        end;
                    end;

                    cds_Comprobantes.Post;

                    end;

                    ObtenerComprobantesImpagosOtros.Next;
                end;
            end;
            //TASK_014_GLE_20161107 termina m�dulo que carga las notas de cobros

{INICIO: TASK_041_JMA_20160711
            if edNumeroComprobante.Text <> EmptyStr then begin                  //SS_1236_MCA_20141226
                EstadoListaAmarilla:= VerificarConvenioListaAmarilla(cds_Comprobantes.FieldByName('CodigoConvenio').AsInteger); //SS_1236_MCA_20141226

                if EstadoListaAmarilla <> '' then begin                         //SS_1236_MCA_20141226
                    lblEnListaAmarilla.Visible  := True;                        //SS_1236_MCA_20141226
                    lblEnListaAmarilla.Caption  := EstadoListaAmarilla;         //SS_1236_MCA_20141226
                end else begin                                                  //SS_1236_MCA_20141226
                                                                                //SS_1236_MCA_20141226
                    lblEnListaAmarilla.Visible  := False;                       //SS_1236_MCA_20141226
                                                                                //SS_1236_MCA_20141226
                end;                                                            //SS_1236_MCA_20141226
            end;                                                                //SS_1236_MCA_20141226
TERMINO: TASK_041_JMA_20160711}

            // LimpiarDatosCliente;
            LimpiarDetalles;
            if cds_Comprobantes.RecordCount <= 0 then begin
                Result := False;
                Exit;
            end else begin
                cds_Comprobantes.First;
                lbl_Estado.Caption := cds_Comprobantes.FieldByName('DescriEstado').AsString;
            end;
            //MostrarDatosCliente(cds_Comprobantes.FieldByName('CodigoPersona').AsInteger);
            // Marcamos los mas viejos al seleccionado
            // Rev 7, ya no marcamos todos los anteriores... solo el ingresado
//            cds_Comprobantes.first;
//            while (not cds_Comprobantes.eof) and (FechaEmision <> nulldate) do begin
//                if cds_Comprobantes.FieldByName('FechaEmision').AsDateTime <= FechaEmision then begin
//                    if (not TienePagoAutomatico(cds_Comprobantes.FieldByName('TipoComprobante').AsString,
//                      cds_Comprobantes.FieldByName('NumeroComprobante').AsInteger, TipoMedioPago)) then begin
//                        cds_Comprobantes.Edit;
//                        cds_Comprobantes.FieldByName('Cobrar').AsBoolean := True;
//                        cds_Comprobantes.Post;
//                        inc(FCantidadComprobantesCobrar);
//                        FTotalComprobantesCobrar := FTotalComprobantesCobrar + cds_Comprobantes.FieldByName('SaldoPendiente').AsFloat;
//                    end;
//                end;
//                cds_Comprobantes.next;
//            end;

{INICIO: TASK_041_JMA_20160711
            TPanelMensajesForm.MuestraMensaje('Obteniendo Saldos ...');
            Result := True;
            //Revision 10 cargamos la variable de saldo del convenio

            spObtenerSaldoConveniosComprobantesImpagos.Parameters.Refresh;

            spObtenerSaldoConveniosComprobantesImpagos.Parameters.ParamByName('@CodigoCliente').Value := iif(FCodigoCliente <= 0, Null, FCodigoCliente);
            spObtenerSaldoConveniosComprobantesImpagos.Parameters.ParamByName('@CodigoConvenio').Value := iif(cbConvenios.Value = 0, Null, cbConvenios.Value);
            spObtenerSaldoConveniosComprobantesImpagos.Parameters.ParamByName('@TipoComprobante').Value := cbTiposComprobantes.Value;
            spObtenerSaldoConveniosComprobantesImpagos.Parameters.ParamByName('@NumeroComprobante').Value := iif((cbTiposComprobantes.Value <> TC_BOLETA) and (edNumeroComprobante.Text <> ''), edNumeroComprobante.Value, Null);
            spObtenerSaldoConveniosComprobantesImpagos.Parameters.ParamByName('@NumeroComprobanteFiscal').Value := iif((cbTiposComprobantes.Value = TC_BOLETA) and (edNumeroComprobante.Text <> ''), edNumeroComprobante.Value, Null);
            spObtenerSaldoConveniosComprobantesImpagos.Parameters.ParamByName('@CodigoImpresoraFiscal').Value := iif((cbTiposComprobantes.Value = TC_BOLETA) and (edNumeroComprobante.Text <> ''), cbImpresorasFiscales.Value, Null);
            spObtenerSaldoConveniosComprobantesImpagos.Open;

            FSaldoConvenios := spObtenerSaldoConveniosComprobantesImpagos.FieldByName('SaldoTotal').Value;
            spObtenerSaldoConveniosComprobantesImpagos.Close;
            FSaldoConvenios := FSaldoConvenios DIV 100;
            lblTotalAPagar.Caption := FormatFloat(FORMATO_IMPORTE, FSaldoConvenios - FTotalComprobantesCobrar);
            //Fin de Revision 10
}
             //FSaldoConvenios := FSaldoConvenios DIV 100; //TASK_009_GLE_20161025 comenta esta l�nea para correcci�n de error
             lblTotalAPagar.Caption := FormatFloat(FORMATO_IMPORTE, FSaldoConvenios - FTotalComprobantesCobrar);
{TERMINO: TASK_041_JMA_20160711}

        except
            on E: Exception do MsgBoxErr(MSG_ERROR_ON_LOAD, E.Message, Caption, MB_ICONERROR);
        end;
    finally
        ObtenerComprobantesImpagos.Close;
        FreeAndNil(ObtenerComprobantesImpagos);

        //TASK_014_GLE_20161107 inicia cierre de sp
        if ObtenerComprobantesImpagosOtros <> nil then begin
            ObtenerComprobantesImpagosOtros.Close;
            FreeAndNil(ObtenerComprobantesImpagosOtros);
        end;
        //TASK_014_GLE_20161107 termina cierre de sp

        TPanelMensajesForm.OcultaPanel;
        cds_Comprobantes.EnableControls;
        lblTotalSelec.Caption   := FormatFloat(FORMATO_IMPORTE, FTotalComprobantesCobrar);
        (* Si hay comprobantes por cobrar, habilitar el bot�n de Cobrar. *)
        btnCobrar.Enabled            := (FCantidadComprobantesCobrar > 0);
        btnDesmarcarTodos.Enabled    := (FCantidadComprobantesCobrar > 0);
        chkbxSeleccionManual.Enabled := (FCantidadComprobantesCobrar > 0) and FchkbxSeleccionManualPermisos;
        chkbxSeleccionManual.Tag := 1;
        chkbxSeleccionManual.Checked := False;
        chkbxSeleccionManual.Tag := 0;

        dl_ComprobantesClick(dl_Comprobantes);

        // SS_637_20100927
        if FComprobantesEnCobranza.Count > 0 then begin
            TPanelMensajesForm.OcultaPanel;
            if FComprobantesEnCobranza.Count >= 15 then
                ShowMsgBoxCN(MSG_COMPROBANTES_COBRANZA_JUDICIAL, Format(MSG_MULTIPLES_COMPROBANTES_EN_COBRANZA,[FComprobantesEnCobranza.Count]), MB_ICONWARNING, Self)
            else
                ShowMsgBoxCN(MSG_COMPROBANTES_COBRANZA_JUDICIAL, FComprobantesEnCobranza.Text, MB_ICONWARNING, Self);
        end;
        if assigned(FComprobantesEnCobranza) then
            FreeAndNil(FComprobantesEnCobranza);
        // SS_637_20100927
        Screen.Cursor := crDefault;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: MostrarDatosCliente
  Author:    flamas
  Date Created: 20/12/2004
  Description:  Muestra los datos del Cliente
  Parameters: CodigoCliente: Integer
  Return Value: None

  Revision: 1
  Author: nefernandez
  Date: 11/02/2008
  Description: SS 631: Cuando se recuperan los datos del cliente, se verifican
  si existen observaciones a mostrar para las cuentas de dicho RUT

  Revision: 2
  Author: mpiazza
  Date: 23/01/2009
  Description: SS 777: Mensaje segun tipo de cliente y sistema
-----------------------------------------------------------------------------}
procedure TformCobroComprobantes.MostrarDatosCliente(CodigoCliente: Integer);
var
RutPersona, EstadoListaAmarilla : string;                                                                                         //SS_660_MVI_20130729
begin

  RutPersona := QueryGetValue(DMConnections.BaseCAC, Format('SELECT dbo.ObtenerRUTPersona(%d)', [CodigoCliente])); //Revision 2
    //Cargamos la informaci�n del cliente.
	lblRUT.Caption := RutPersona;
	lblNombre.Caption := QueryGetValue(DMConnections.BaseCAC, Format('SELECT dbo.ObtenerNombrePersona(%d)', [CodigoCliente]));
	lblDomicilio.Caption := QueryGetValue(DMConnections.BaseCAC, Format('SELECT dbo.ObtenerDescripcionDomicilioPrincipalPersona(%d)', [CodigoCliente]));
    //Revision 1
    if FUltimoNumeroDocumento <> Trim(lblRUT.Caption) then begin
        FUltimoNumeroDocumento := Trim(lblRUT.Caption);
        MostrarMensajeRUTConvenioCuentas(FUltimoNumeroDocumento);
        MostrarMensajeTipoClienteSistema(RutPersona);//Revision 2
    end;
    EstaClienteConMensajeEspecial(DMConnections.BaseCAC, RutPersona);           //SS_1408_MCA_20151027
    MostrarSaldosPersona(CodigoCliente);   // SS_637_PDO_20101227
    CargarComboRefinanciaciones(CodigoCLiente);
    CargarRefinanciaciones(CodigoCliente); // SS_637_PDO_20101227
    //if VerificaPersonaListaAmarilla(CodigoCliente) then //SS_660_MVI_20130729   // SS_660_CQU_20130711
    //    lblEnListaAmarilla.Visible := True;             //SS_660_MVI_20130729   // SS_660_CQU_20130711

//    EstadoListaAmarilla:= VerificaPersonaListaAmarilla(CodigoCliente);                                 //SS_660_MVI_20130909 //SS_660_MVI_20130729
//    if (EstadoListaAmarilla <> 'NULL') and (EstadoListaAmarilla <> '') then begin                      //SS_660_MVI_20130909  //SS_660_MVI_20130729
//                                                                                                       //SS_660_MVI_20130909  //SS_660_MVI_20130729
//    lblEnListaAmarilla.Visible  := True;                                                               //SS_660_MVI_20130909  //SS_660_MVI_20130729
//    lblEnListaAmarilla.Caption  := EstadoListaAmarilla;                                                //SS_660_MVI_20130909  //SS_660_MVI_20130729
//                                                                                                       //SS_660_MVI_20130909  //SS_660_MVI_20130729
//    end                                                                                                //SS_660_MVI_20130909  //SS_660_MVI_20130729
//    else begin                                                                                         //SS_660_MVI_20130909  //SS_660_MVI_20130729
//                                                                                                       //SS_660_MVI_20130909  //SS_660_MVI_20130729
//    lblEnListaAmarilla.Visible  := False;                                                              //SS_660_MVI_20130909  //SS_660_MVI_20130729
//                                                                                                       //SS_660_MVI_20130909  //SS_660_MVI_20130729
//    end;                                                                                               //SS_660_MVI_20130909
                                                                                                         //SS_660_MVI_20130909
   //VerificaPersonaListaAmarilla(CodigoCliente);                               //SS_1236_MCA_20141226    //SS_660_MVI_20130909
   VerificaPersonaListaAmarilla(RutPersona);                                    //SS_1236_MCA_20141226
end;

{-----------------------------------------------------------------------------
  Function Name: MostrarMensajeRUTConvenioCuentas
  Author: nefernandez
  Date Created: 11/02/2008
  Description:  SS 631: Se muestra un aviso con las observaciones de las cuentas
  del Cliente (RUT) en cuesti�n.
  Parameters: NumeroDocumento: String
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformCobroComprobantes.MostrarMensajeRUTConvenioCuentas(NumeroDocumento: String);
var
    Texto: AnsiString;
begin
    if ( NumeroDocumento <> '' ) then begin
        Texto := MensajeRUTConvenioCuentas(DMCOnnections.BaseCAC, NumeroDocumento);
        if Texto <> '' then
            MsgBox(Texto, Self.Caption, MB_ICONWARNING);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: MostrarMensajeTipoClienteSistema
  Author: mpiazza
  Date Created: 23/01/2009
  Description:  SS-777 Trae mensaje segun RUT y sistema de acuerdo
  al tipo cliente
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformCobroComprobantes.MostrarMensajeTipoClienteSistema(
  NumeroDocumento: String);
var
    Texto: AnsiString;
begin
    if ( NumeroDocumento <> '' ) then begin
        Texto := MensajeTipoCliente(DMCOnnections.BaseCAC, NumeroDocumento, SistemaActual);
        if Texto <> '' then
            MsgBox(Texto, Self.Caption, MB_ICONWARNING);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: MostrarDatosComprobante
  Author:    flamas
  Date Created: 20/12/2004
  Description: Muestra los datos del Comprobante
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformCobroComprobantes.MostrarDatosComprobante;
var
    EstadoDebito: char;
begin
        if cds_Comprobantes.RecordCount > 0 then begin

    	lblFecha.Caption            	:= FormatDateTime('dd/mm/yyyy', cds_Comprobantes.FieldByName('FechaEmision').AsDateTime);
    	lblFechaVencimiento.Caption 	:= FormatDateTime('dd/mm/yyyy', cds_Comprobantes.FieldByName('FechaVencimiento').AsDateTime);
        FTotalApagar                    := Round(cds_Comprobantes.FieldByName('SaldoPendiente').AsFloat);
        lbl_Estado.Caption              := cds_Comprobantes.FieldByName('DescriEstado').AsString;
        FTipoComprobante                := cds_Comprobantes.FieldByName('TipoComprobante').AsString;
        FNumeroComprobante              := cds_Comprobantes.FieldByName('NumeroComprobante').AsInteger;

        FUltimoComprobante	:= cds_Comprobantes.FieldByName('UltimoComprobante').AsInteger;

        //edNumeroComprobante.ValueInt := FNumeroComprobante;
        //revision 10 TipoMedioPago := QueryGetValueInt(DMConnections.BaseCAC,'SELECT CodigoTipoMedioPago FROM Comprobantes  WITH (NOLOCK)  ' +' WHERE TipoComprobante = ' + QuotedStr(FTipoComprobante) +' AND NumeroComprobante = ' + IntToStr(FNumeroComprobante));



        //carga datos del pago automatico...
        EstadoDebito := cds_Comprobantes.FieldByName('EstadoDebito').AsString[1];
        lblMedioPagoAutomatico.Caption := cds_Comprobantes.FieldByName('DescTipoMedioPAgo').AsString; //revision 10
        lblDescPATPAC.Caption := cds_Comprobantes.FieldByName('DescPATPAC').AsString;  //revision 10
        lblRespuestaDebito.Caption := cds_Comprobantes.FieldByName('Rechazo').AsString; //revision 10
        lblCapRespuesta.Visible := lblRespuestaDebito.Caption <> ''; //revision 10

// revision 10 elimina
//        lblMedioPagoAutomatico.Caption := QueryGetValue(DMConnections.BaseCAC, 'SELECT dbo.ObtenerDescripcionTipoMedioPago(' + IntToStr(TipoMedioPago) + ')');
//
//        case TipoMedioPago of
//            0:  lblDescPATPAC.Caption := EmptyStr;
//            1:  lblDescPATPAC.Caption := QueryGetValue( DMConnections.BaseCAC, Format ('select dbo.ObtenerDescripcionTipoTarjetaCredito( ' + '(select PAT_CodigoTipoTarjetaCredito' + ' from comprobantes  WITH (NOLOCK) ' + ' where NumeroComprobante = %d' + ' and tipocomprobante = ''%s'') )', [FNumeroComprobante, FTipoComprobante])) ;
//            2:  lblDescPATPAC.Caption := QueryGetValue( DMConnections.BaseCAC, Format ('select dbo.ObtenerDescripcionBanco( ' + '(select PAC_CodigoBanco' + ' from comprobantes  WITH (NOLOCK) ' + ' where NumeroComprobante = %d' + ' and tipocomprobante = ''%s'') )', [FNumeroComprobante, FTipoComprobante]));
//        end;
//
//        CantidadRechazos := QueryGetValueInt(DMConnections.BaseCAC,Format('select count(numerocomprobante) from pagosrechazados  WITH (NOLOCK)  ' +'where Tipocomprobante = ''%s'' ' +'and NumeroComprobante = %d ', [FTipoComprobante, FNumeroComprobante] ));
//
//        if CantidadRechazos > 0 then begin
//            lblRespuestaDebito.Caption := QueryGetValue(DMConnections.BaseCAC, format( 'select top 1 ObservacionRechazo from PagosRechazados  WITH (NOLOCK)  ' +' where TipoComprobante = ''%s'' and NumeroComprobante = %d ' ,[FTipoComprobante, FNumeroComprobante]));
//            lblCapRespuesta.Visible := True;    //no borrar
//        end
//        else begin
//            lblRespuestaDebito.Caption := EmptyStr;
//            lblCapRespuesta.Visible := False;  //no borrar
//        end;


        case EstadoDebito of
            'C': lblEstadoDebito.Caption := 'Cancelado';
            'N': lblEstadoDebito.Caption := 'No Aplica';
            'E': lblEstadoDebito.Caption := 'Enviado';
            'P': lblEstadoDebito.Caption := 'Pendiente';
        end;



    end else begin
        LimpiarDetalles;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: LimpiarDetalles;
  Author:    flamas
  Date Created: 26/01/2005
  Description: Borra los detalles del Comprobante
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformCobroComprobantes.LimpiarDetalles;
begin
  	lblFecha.Caption                := EmptyStr;
  	lblFechaVencimiento.Caption	    := EmptyStr;
    lblTotalAPagar.Caption          := EmptyStr;
    lbl_Estado.Caption              := EmptyStr;
    lblMedioPagoAutomatico.Caption  := EmptyStr;
  	lblDescPATPAC.Caption           := EmptyStr;
    lblEstadoDebito.Caption         := EmptyStr;
    lblRespuestaDebito.Caption      := EmptyStr;
  	lblCapRespuesta.visible         := False;
    lblTotalSelec.Caption           := EmptyStr;
 	btnCobrar.Enabled     		    := False;
end;

{-----------------------------------------------------------------------------
  Function Name: peNumeroDocumentoButtonClick
  Author:    flamas
  Date Created: 21/12/2004
  Description: Procesa un cambio en el RUT - Muestra los comprobantes pendientes
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformCobroComprobantes.peNumeroDocumentoButtonClick(Sender: TObject);
var
    f: TFormBuscaClientes;
begin
    Application.CreateForm(TFormBuscaClientes,F);
  	if F.Inicializa(FUltimaBusqueda) then begin
    		F.ShowModal;
        if F.ModalResult = idOk then begin
            FUltimaBusqueda := F.UltimaBusqueda;
            peNumeroDocumento.Text := f.Persona.NumeroDocumento;

	    end;
  	end;
    F.free;
end;

{-----------------------------------------------------------------------------
  Function Name: cbComprobantesChange
  Author:    flamas
  Date Created: 21/12/2004
  Description: Procesa un cambio de comprobante y muestra los datos
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformCobroComprobantes.cbComprobantesChange(Sender: TObject);
begin
    if ActiveControl <> Sender then Exit;
    MostrarDatosComprobante;
end;

{-----------------------------------------------------------------------------
  Function Name: cbTiposComprobantesChange
  Author:    flamas
  Date Created: 21/12/2004
  Description: Procesa un cambio de tipo de comprobante y muestra los datos
				del comprobante
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformCobroComprobantes.cbTiposComprobantesChange(Sender: TObject);
begin
//    tbsRefinanciaciones.TabVisible := (cbTiposComprobantes.Value = null);

    MostrarControlesSegunComprobante(cbTiposComprobantes.Value);
  	edCodigoBarras.Enabled := (cbTiposComprobantes.Value = TC_NOTA_COBRO);
    lbl_CodigoBarra.Enabled := edCodigoBarras.Enabled;

  	edNumeroComprobante.Enabled := ( cbTiposComprobantes.Value <> Null );
  	lbl_NumeroComprobante.Enabled := edNumeroComprobante.Enabled;

    peNumeroDocumento.Clear;

    LimpiarDetalles;
    LimpiarConvenios;
    LimpiarListaComprobantes;
    LimpiarRefinanciaciones;
    LimpiarDatosCliente;
    lblEnListaAmarilla.Visible := False;    // SS_660_CQU_20130711

    FCodigoCliente := -1;
    HabilitarFiltroRUT(cbTiposComprobantes.Value = Null);

    if ActiveControl <> Sender then Exit;
    if (cbTiposComprobantes.Value = Null) then edNumeroComprobante.Clear;
    if (edNumeroComprobante.Text <> '') then edNumeroComprobanteChange( Sender )
    else if (edCodigoBarras.Text <> '') then edCodigoBarrasChange( Sender );
//    else CargarComprobantes;
end;

procedure TformCobroComprobantes.cdsRefinanciacionesAfterScroll(DataSet: TDataSet);
begin
    btnActivarRefinanciacion.Enabled :=																										//SS_1051_PDO
        FbtnActivarRefinanciacionPermisos																									//SS_1051_PDO
        and cdsRefinanciacionesValidada.AsBoolean																							//SS_1051_PDO
        and (cdsRefinanciacionesCodigoEstado.AsInteger <> REFINANCIACION_ESTADO_VIGENTE)//TASK_005_AUN_20170403-CU.COBO.COB.301: No puedo activar una refinanciacion ya activada (es decir con estado = Vigente)
        and (																																//SS_1051_PDO
                (cdsRefinanciacionesCodigoRefinanciacionUnificada.AsInteger = 0)															//SS_1051_PDO
                or (cdsRefinanciacionesCodigoRefinanciacion.AsInteger = cdsRefinanciacionesCodigoRefinanciacionUnificada.AsInteger)			//SS_1051_PDO
            );																																//SS_1051_PDO
end;

procedure TformCobroComprobantes.chkbxSeleccionManualClick(Sender: TObject);
    var
        ValorActualCobrar: Boolean;
        Puntero: TBookmark;
begin
    if (not cds_Comprobantes.IsEmpty) and (chkbxSeleccionManual.Tag = 0) and (not chkbxSeleccionManual.Checked) then begin
        MarcarDesmarcarComprobantes(True);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: edNumeroComprobanteChange
  Author:    flamas
  Date Created: 21/12/2004
  Description: Procesa un cambio de comprobante y muestra los datos del comprobante
  Parameters: Sender: TObject
  Return Value: None
  -----------------------------------------------------------------------------}
procedure TformCobroComprobantes.edNumeroComprobanteChange(Sender: TObject);
begin
    if ActiveControl <> Sender then Exit;

    peNumeroDocumento.Clear;
    FCodigoCliente := -1;
    LimpiarDetalles;
    LimpiarConvenios;
    LimpiarListaComprobantes;
    LimpiarRefinanciaciones;
    LimpiarDatosCliente;
    HabilitarFiltroRUT(edNumeroComprobante.Text = EmptyStr);
    
  	//if Timer1.Enabled then begin                                              //SS-1014-NDR-20120123
    //    Timer1.Enabled := False;                                              //SS-1014-NDR-20120123
    //    Timer1.Enabled := True;                                               //SS-1014-NDR-20120123
    //end else Timer1.Enabled := True;                                          //SS-1014-NDR-20120123
end;

{----------------------------------------------------------------------------
  Function Name: edCodigoBarrasChange
  Author:    flamas
  Date Created: 21/12/2004
  Description: Procesa un cambio del c�digo de barras y muestra los datos
				del comprobante
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformCobroComprobantes.edCodigoBarrasChange(Sender: TObject);
resourcestring
    MSG_ALREADY_PAID = 'Este comprobante est� pago';
begin
    if ActiveControl <> Sender then Exit;
    peNumeroDocumento.Clear;                                                    //SS-1014-NDR-20120123
    FCodigoCliente := -1;                                                       //SS-1014-NDR-20120123
    LimpiarDetalles;                                                            //SS-1014-NDR-20120123
    LimpiarConvenios;                                                           //SS-1014-NDR-20120123
    LimpiarListaComprobantes;                                                   //SS-1014-NDR-20120123
    LimpiarRefinanciaciones;                                                    //SS-1014-NDR-20120123
    LimpiarDatosCliente;                                                        //SS-1014-NDR-20120123
    HabilitarFiltroRUT(edNumeroComprobante.Text = EmptyStr);                    //SS-1014-NDR-20120123

  	if Length(edCodigoBarras.Text) = 29 then begin
      	cbTiposComprobantes.Value := TC_NOTA_COBRO;
      	edNumeroComprobante.Value := StrToInt( Copy( edCodigoBarras.Text, 7, 9)); //StrToInt( Copy( edCodigoBarras.Text, 21, Length(edCodigoBarras.Text)));
        //CargarComprobantes;                                                   //SS-1014-NDR-20120123
    end else begin
        edNumeroComprobante.Clear;
        cbConvenios.Value           := 0;
    	peNumeroDocumento.Clear;
        FCodigoCliente := -1;
    	lblFecha.Caption            := '';
        lblFechaVencimiento.Caption	:= '';
        lblTotalAPagar.Caption      := '';
        lbl_Estado.Caption          := EmptyStr;
        LimpiarDatosCliente;
    	lblMedioPagoAutomatico.Caption  := '';
    	lblDescPATPAC.Caption           := '';
        lblEstadoDebito.Caption         := '';
        lblRespuestaDebito.Caption      := '';
    	lblCapRespuesta.visible         := False;
        btnCobrar.Enabled			:= False;
    end;
end;


{----------------------------------------------------------------------------
  Function Name: edCodigoBarrasChange
  Author:
  Date Created: 21/12/2004
  Description: Procesa un cambio del c�digo de barras y muestra los datos
				del comprobante
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformCobroComprobantes.edCodigoBarrasKeyPress(Sender: TObject; var Key: Char);
begin
	if  not (Key in [#8, #9, #13, '0'..'9']) then Key := #0;
    if (Key = #13) AND (Length(edCodigoBarras.Text) < 29) then begin
        if edCodigobarras.Text <> '' then begin
			      edCodigoBarras.Text :=  stringreplace( format('%29.0d',[ival(edCodigoBarras.Text)]), ' ' , '0',[rfReplaceAll]);
            if btnCobrar.Enabled then btnCobrar.SetFocus;
        end;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnCobrarClick
  Author:    flamas
  Date Created: 27/01/2005
  Description: Cobra un Comprobante
  Parameters: Sender: TObject
  Return Value: None

  Revision : 16
  Author: pdominguez
  Date: 17/11/2010
  Description : SS 527 - salir todas NK seleccionadas y advertencia anteriores impagas
        - Se valida que no quede ning�n comprobante impago sin marcar, anterior al �ltimo
        marcado. En el caso de que haya alguno, muestra un mensaje de advertencia.
        - S�lo se consideran los documentos cuyo TipoComprobante = 'NK'

    Author      :   CQuezadaI
    Date        :   30-Abril-2013
    Firma       :   SS_660_CQU_20121010
    Description :   Se agrega una validaci�n y se incrementa la tabulaci�n
-----------------------------------------------------------------------------}
procedure TformCobroComprobantes.btnActivarRefinanciacionClick(Sender: TObject);
    resourcestring                                                                      //SS_660_CQU_20121010
        MSG_CONVENIOENLISTAAMARILLA = 'Convenio se encuentra en lista amarilla'         //SS_660_CQU_20121010
                                        + CRLF                                          //SS_660_CQU_20121010
                                        + 'No se puede pagar por refinanciaci�n.';      //SS_660_CQU_20121010
    var
        IDBloqueo: Integer;
        ResultadoBloqueo: Integer;
        CodigoConvenio : Integer;                                                       // SS_660_CQU_20121010
begin
    CodigoConvenio := cds_Comprobantes.FieldByName('CodigoConvenio').AsInteger;         // SS_660_CQU_20121010
    //if not VerificarConvenioListaAmarilla(CodigoConvenio) then begin                    // SS_660_CQU_20121010
    //if VerificarConvenioListaAmarilla(CodigoConvenio) = 'NULL' then begin                 //SS_660_MVI_20130729       //SS_660_MBE_20130925
        try
            CambiarEstadoCursor(CURSOR_RELOJ, Self, FComponentes);
            try
                TPanelMensajesForm.MuestraMensaje('Obteniendo Bloqueo ...', True);
                IDBloqueo := 0;
                ResultadoBloqueo :=
                    ObtenerBloqueo
                        (
                            DMConnections.BaseCAC,
                            Self.ClassName,
                            cdsRefinanciacionesCodigoRefinanciacion.AsString,
                            UsuarioSistema,
                            IDBloqueo
                        );

                if ResultadoBloqueo = 0 then begin
                    ActivarRefinanciacionForm := TActivarRefinanciacionForm.Create(Self);
                    if ActivarRefinanciacionForm.Inicializar(cdsRefinanciacionesCodigoRefinanciacion.AsInteger) then begin
                        if ActivarRefinanciacionForm.ShowModal = mrOk then begin
                            if ActivarRefinanciacion then begin
                                LimpiarDatosCliente;
                                LimpiarCampos;
                            end;
                        end;
                    end;
                end;
            except
                on e: Exception do begin
                    ShowMsgBoxCN(e, Self);
                end;
            end;
        finally
            if Assigned(ActivarRefinanciacionForm) then FreeAndNil(ActivarRefinanciacionForm);
            try
                if IDBloqueo <> 0 then begin
                    TPanelMensajesForm.MuestraMensaje('Eliminando Bloqueo ...');
                    EliminarBloqueo(DMConnections.BaseCAC, IDBloqueo);
                end;
            except
                on e: Exception do begin
                    ShowMsgBoxCN(e, Self);
                end;
            end;

            TPanelMensajesForm.OcultaPanel;
            CambiarEstadoCursor(CURSOR_DEFECTO, Self, FComponentes);
        end;
//    end else                                                                            // SS_660_CQU_20121010        //SS_660_MBE_20130925
//        ShowMsgBoxCN(Self.Caption, MSG_CONVENIOENLISTAAMARILLA, MB_ICONWARNING, Self);  // SS_660_CQU_20121010        //SS_660_MBE_20130925
end;

procedure TformCobroComprobantes.btnCobrarClick(Sender: TObject);
resourcestring
    MSG_NEWER_INVOICE = 'Existe un comprobante emitido m�s nuevo.';
  	MSG_WISH_SEE_NEW_INVOICE = '�Desea ver el nuevo comprobante?';
  	MSG_SENT_FOR_AUTOMATIC_PAYMENT = 'El comprobante ha sido enviado para su d�bito autom�tico.' + #10#13 + '�Desea pagarlo de todos modos?';
  	MSG_CAPTION	= 'Cobro de Comprobantes';
    MSG_COMPROBANTES_IMPAGOS_TITULO  = 'Validaci�n Comprobantes'; // Rev. 16 SS_527_20101117
    MSG_COMPROBANTES_IMPAGOS_MENSAJE = 'Existen comprobantes anteriores NK que se encuentran Vencidos e Impagos.'; // Rev. 16 SS_527_20101117
var
    ExistenImpagos, // Rev. 16 SS_527_2010117
    EncontradoUltimoMarcado: Boolean; // Rev. 16 SS_527_2010117
begin
    // Rev. 16 SS_527_2010117
    try
        Screen.Cursor := crHourGlass;
        EmptyKeyQueue;
        Application.ProcessMessages;

        ExistenImpagos := False;
        EncontradoUltimoMarcado := False;
        ds_Comprobantes.DataSet := Nil;

        with cds_Comprobantes do begin
            cds_Comprobantes.Last;
            while (not cds_Comprobantes.Bof) and (not ExistenImpagos) do begin
                if not EncontradoUltimoMarcado then
                    EncontradoUltimoMarcado := (cds_Comprobantes.FieldByName('Cobrar').AsBoolean and (cds_Comprobantes.FieldByName('TipoComprobante').AsString = TC_NOTA_COBRO))
                else ExistenImpagos := (not cds_Comprobantes.FieldByName('Cobrar').AsBoolean) and (cds_Comprobantes.FieldByName('TipoComprobante').AsString = TC_NOTA_COBRO);

                cds_Comprobantes.Prior;
            end;
        end;
    finally
        ds_Comprobantes.DataSet := cds_Comprobantes;
        Screen.Cursor := crDefault;
    end;

    if ExistenImpagos then ShowMsgBoxCN(MSG_COMPROBANTES_IMPAGOS_TITULO, MSG_COMPROBANTES_IMPAGOS_MENSAJE, MB_ICONWARNING, Self);
    // Fin Rev. 16 SS_527_2010117

    //Rev.16 / 20-Agosto-2010 / Nelson Droguett
    try
        btnCobrar.Enabled := False;
        EmptyKeyQueue;
        Application.ProcessMessages;

        if (cds_Comprobantes.RecordCount = 1) and (FTipoComprobante = TC_NOTA_COBRO) and (FNumeroComprobante < FUltimoComprobante) then begin
            if MsgBox(MSG_NEWER_INVOICE + #10#13 + MSG_WISH_SEE_NEW_INVOICE, MSG_CAPTION, MB_ICONQUESTION or MB_DEFBUTTON2 or MB_YESNO) = IDYES then begin
                edNumeroComprobante.ValueInt := FUltimoComprobante;
                CargarComprobantes;
            end else Cobrar;
        end else Cobrar;
    finally
        btnCobrar.Enabled := True;
    end;
    //FinRef.16
end;

{-----------------------------------------------------------------------------
  Function Name: Cobrar
  Author:    flamas
  Date Created: 27/01/2005
  Description: Cobra un Comprobante
  Parameters: Sender: TObject
  Return Value: None

  Revision : 1
    Date: 11/03/2009
    Author: mpiazza
    Description:  Ref (ss795) Error en la impresion de comprobante,
                  se le agrego un except para que continue con el
                  proceso una vez informado el error
-----------------------------------------------------------------------------}
procedure TformCobroComprobantes.Cobrar;
resourcestring
	MSG_REGISTRO_CORRECTO = 'El pago se ha registrado correctamente.';
	MSG_CONFIRMAR_RECIBO = '�Desea Imprimir el Comprobante de Cancelaci�n?';
	MSG_CAPTION = 'Registrar Pago';
	MSG_IMPRESION_EXITOSA = '�La impresi�n se ha realizado con �xito?';           //SS-1006-1015-MCO-20120904
  MSG_ERROR_IMPRESION = 'Error al intentar Imprimir el comprobante';
var
	r: TformRecibo;
	f: TfrmPagoVentanilla;
	RespuestaImpresion: TRespuestaImprimir;
    ComprobantesCobrar: TListaComprobante;
    // Rev. 16 (Infractores Fase 2)
    Veces: Integer;
    Comprobante: TComprobante;
    // Fin Rev. 16 (Infractores Fase 2)
begin
    (* Armar la lista con los comprobantes a cobrar. *)
    ComprobantesCobrar := ObtenerComprobantesCobrar;
    try
        Application.CreateForm(TfrmPagoVentanilla, f);
        if f.Inicializar(ComprobantesCobrar, lblRUT.Caption, lblNombre.Caption,
                FNumeroPOS, FPuntoEntrega, FPuntoVenta, FCodigoCanalPago) then f.ShowModal;
            if f.ModalResult = mrOk then begin
                (* Almacenar el c�digo de canal de pago utilizado. *)
                FCodigoCanalPago := f.CodigoCanalPago;

                if f.ImprimirRecibo then begin
                    (* Mostrar el di�logo previo a realizar la impresi�n. *)
                    RespuestaImpresion := frmImprimir.Ejecutar(MSG_CAPTION,
                      MSG_REGISTRO_CORRECTO + CRLF + MSG_CONFIRMAR_RECIBO);
                    case RespuestaImpresion of
                        (* Si la respuesta fue Aceptar, ejecutar el reporte. *)
                        riAceptarConfigurarImpresora, riAceptar: begin
                            Application.CreateForm(TformRecibo, r);
                            try  // ss795
                              try
                                  if r.Inicializar(FLogo, f.NumeroRecibo, RespuestaImpresion = riAceptarConfigurarImpresora) then begin
                                      (* Preguntar si la impresi�n fue exitosa. *)
                                      while MsgBox(MSG_IMPRESION_EXITOSA, MSG_CAPTION, MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = mrNo do begin
                                          (* Mostrar el di�logo de Configurar Impresora para permitir al operador
                                          reintentar la impresi�n. *)
                                          if not r.Inicializar(FLogo, f.NumeroRecibo, True) then Break;
                                      end; // while
                                  end; // if
                              finally
                                  r.Release;
                              end;
                            except//ss795
                              On E: Exception do begin
                                MsgBoxErr(MSG_ERROR_IMPRESION, e.Message, Caption, MB_ICONERROR);
                              end;
                            end;
                        end;
                    end; // case

                    LimpiarDatosCliente;
                end;

                // Rev. 16 (Infractores Fase 2)
                for Veces := 0 to ComprobantesCobrar.Count - 1 do begin
                    Comprobante := TComprobante(ComprobantesCobrar.Items[Veces]);
                    if Comprobante.TieneConceptosInfractor then
                        ImprimirDetalleInfraccionesAnuladas(Comprobante.TipoComprobante, Comprobante.NumeroComprobante);
                end;
                // Fin Rev. 16 (Infractores Fase 2)

                LimpiarCampos;
            end;
        f.Free;
    finally
        ComprobantesCobrar.Free;
    end; // finally
end;


{-----------------------------------------------------------------------------
  Procedure: TformPagoComprobantes.LimpiarCampos
  Author:    gcasais
  Date:      19-Ene-2005
  Arguments: None
  Result:    None
-----------------------------------------------------------------------------}
procedure TformCobroComprobantes.LimpiarCampos;
begin
    FCantidadCuotasCobrar := 0;
  	edNumeroComprobante.Clear;
  	peNumeroDocumento.Clear;
    FCodigoCliente := -1;
  	edCodigoBarras.Clear;
  	LimpiarConvenios;
    LimpiarListaComprobantes;
  	LimpiarDetalles;
    if edCodigoBarras.Enabled then ActiveControl := edCodigoBarras
    else if edNumeroComprobante.Enabled then ActiveControl := edNumeroComprobante
    else if peNumeroDocumento.Enabled then ActiveControl := peNumeroDocumento
    else ActiveControl := cbTiposComprobantes;
    lblEnListaAmarilla.Visible := False;   // SS_660_CQU_20130711
end;

{-----------------------------------------------------------------------------
  Function Name: FormKeyDown
  Author:
  Date Created:  13/10/2005
  Description:
  Parameters:
  Return Value:
-----------------------------------------------------------------------------}
procedure TformCobroComprobantes.FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
    if Key = KEY_F9 then begin
        case pcOpcionesComprobantes.ActivePageIndex of
            0: begin
                if btnCobrar.Enabled then btnCobrar.Click;
            end;
            1: begin
                if btnCobrarCuota.Enabled then btnCobrarCuota.Click;
            end;
            2: begin
                if btnActivarRefinanciacion.Enabled then btnActivarRefinanciacion.Click;
            end;
        end;
    end;
end;

procedure TformCobroComprobantes.FormResize(Sender: TObject);
begin
	// SS 762
    if ClientHeight < FMinHeight then ClientHeight := FMinHeight;
    if ClientWidth < FMinWidth then ClientWidth := FMinWidth;
end;

{-----------------------------------------------------------------------------
  Function Name: Timer1Timer
  Author:
  Date Created:  13/10/2005
  Description:
  Parameters:
  Return Value:
-----------------------------------------------------------------------------}
//procedure TformCobroComprobantes.Timer1Timer(Sender: TObject);                //SS-1014-NDR-20120123
//begin                                                                         //SS-1014-NDR-20120123
//    Timer1.Enabled := False;                                                  //SS-1014-NDR-20120123
//    CargarComprobantes;                                                       //SS-1014-NDR-20120123
//    edCodigoBarras.Text := EmptyStr;                                          //SS-1014-NDR-20120123
//end;                                                                          //SS-1014-NDR-20120123

procedure TformCobroComprobantes.vcbRefinanciacionChange(Sender: TObject);
begin
    try
        CambiarEstadoCursor(CURSOR_RELOJ, Self, FComponentes);
        CargarRefinanciaciones(FCodigoCliente);
    finally
        CambiarEstadoCursor(CURSOR_DEFECTO, Self, FComponentes);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: LimpiarConvenios
  Author:
  Date Created:  13/10/2005
  Description:
  Parameters:
  Return Value:
-----------------------------------------------------------------------------}
procedure TformCobroComprobantes.LimpiarConvenios;
begin
    cbConvenios.Clear;
    cbConvenios.Items.Add(SELECCIONAR, 0);
    cbConvenios.ItemIndex := 0;
end;

{-----------------------------------------------------------------------------
  Function Name: HabilitarFiltroRUT
  Author:
  Date Created:  13/10/2005
  Description:
  Parameters:
  Return Value:
-----------------------------------------------------------------------------}
procedure TformCobroComprobantes.HabilitarFiltroRUT(Valor: Boolean);
begin
    peNumeroDocumento.Enabled := Valor;
    lb_CodigoCliente.Enabled := Valor;
    cbConvenios.Enabled := Valor;
    lbl_NumeroConvenio.Enabled := Valor;
end;

{-----------------------------------------------------------------------------
  Function Name: dl_ComprobantesDblClick
  Author:
  Date Created:  13/10/2005
  Description:
  Parameters:
  Return Value:
-----------------------------------------------------------------------------}
procedure TformCobroComprobantes.dl_ComprobantesDblClick(Sender: TObject);
    resourcestring
        MSG_AVISO_TITULO  = 'Validaci�n Refinanciaci�n';
        MSG_AVISO_MENSAJE = 'El comprobante no se puede seleccionar para su cobro pues est� incluido en un proceso de Refinanciaci�n.';
    var
        TipoMedioPago: Integer;
        ValorActualCobrar: Boolean;
begin
    if (not cds_Comprobantes.IsEmpty) then begin
        if not (cds_Comprobantes.FieldByName('EstadoRefinanciacion').AsInteger in [REFINANCIACION_ESTADO_EN_TRAMITE, REFINANCIACION_ESTADO_ACEPTADA]) then begin
            try
                (* Si no est� marcado para cobrar y
                TienePagoAutom�tico, entonces preguntar si lo desea cobrar. *)
                if (not cds_Comprobantes.FieldByName( 'Cobrar' ).AsBoolean)
                        and TienePagoAutomatico(cds_Comprobantes.FieldByName('TipoComprobante').AsString,
                             cds_Comprobantes.FieldByName('NumeroComprobante').AsInteger, TipoMedioPago) then begin
                    if (not CobrarComprobanteConPagoAutomatico(cds_Comprobantes.FieldByName('TipoComprobante').AsString,
                            cds_Comprobantes.FieldByName('NumeroComprobante').AsInteger, TipoMedioPago)) then begin
                        Exit;
                    end; // if
                end; // if

                with cds_Comprobantes do begin
                    ValorActualCobrar := FieldByName('Cobrar').AsBoolean;

                    Edit;
                    FieldByName('Cobrar').AsBoolean := not FieldByName( 'Cobrar' ).AsBoolean;


                    if ValorActualCobrar <> FieldByName('Cobrar').AsBoolean  then begin
                        if FieldByName('Cobrar').AsBoolean then begin
                            Inc(FCantidadComprobantesCobrar);
                            FTotalComprobantesCobrar := FTotalComprobantesCobrar + FieldByName('SaldoPendiente').AsFloat;
                            lblTotalSelec.Caption:=FormatFloat(FORMATO_IMPORTE, FTotalComprobantesCobrar);
                        end else begin
                            Dec(FCantidadComprobantesCobrar);
                            FTotalComprobantesCobrar := FTotalComprobantesCobrar - FieldByName('SaldoPendiente').AsFloat;
                            if FTotalComprobantesCobrar < 0 then begin
                                lblTotalSelec.Caption:=FormatFloat(FORMATO_IMPORTE, 0);
                                FTotalComprobantesCobrar :=0;
                            end else begin
                                lblTotalSelec.Caption:=FormatFloat(FORMATO_IMPORTE, FTotalComprobantesCobrar);
                            end;
                        end;
                    end;

                    lblTotalAPagar.Caption := FormatFloat(FORMATO_IMPORTE , FSaldoConvenios - FTotalComprobantesCobrar);
                    // Revision 9
                    VerificarComprobanteEnCarpetaLegal(False); // SS_637_20100927
                    Post;

                    if FieldByName('Cobrar').AsBoolean and (not chkbxSeleccionManual.Checked) then begin
                        VerificaComprobantesMarcados;
                    end;
                end;
            finally
                btnCobrar.Enabled := (FCantidadComprobantesCobrar > 0);
            end;
        end
        else ShowMsgBoxCN(MSG_AVISO_TITULO, MSG_AVISO_MENSAJE, MB_ICONWARNING, Self);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: dl_ComprobantesDrawText
  Author:
  Date Created:  13/10/2005
  Description:
  Parameters:
  Return Value:
-----------------------------------------------------------------------------}
procedure TformCobroComprobantes.dl_ComprobantesDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;State: TOwnerDrawState; var Text: String; var TxtRect: TRect;var ItemWidth: Integer; var DefaultDraw: Boolean);
var
    bmp: TBitMap;
begin
    with Sender do begin
        if (Column.FieldName = 'Cobrar') then begin
            Text := '';
            Canvas.FillRect(Rect);
            bmp := TBitMap.Create;
			DefaultDraw := False;
            try
                if (cds_Comprobantes.FieldByName('Cobrar').AsBoolean ) then begin
                    lnCheck.GetBitmap(0, Bmp);
                end
                else begin
                    if cds_Comprobantes.FieldByName('EstadoRefinanciacion').AsInteger in [REFINANCIACION_ESTADO_EN_TRAMITE, REFINANCIACION_ESTADO_ACEPTADA] then begin
                        lnCheck.GetBitmap(2, Bmp);
                    end
                    else begin
                        lnCheck.GetBitmap(1, Bmp);
                    end;
                end;

                Canvas.Draw((Rect.Left + Rect.Right - Bmp.Width) div 2, Rect.Top, bmp);
            finally
                bmp.Free;
            end; // finally
        end; // if

        if	(Column.FieldName = 'FechaVencimiento') and                                                     //SS_959_MBE_20110905
        	(	cds_Comprobantes.FieldByName('FechaVencimiento').IsNull or                                  //SS_959_MBE_20110905
                (cds_Comprobantes.FieldByName('FechaVencimiento').AsDateTime <= 0) )	then begin			//SS_959_MBE_20110905
            Text := '';																						//SS_959_MBE_20110905
    	end;																								//SS_959_MBE_20110905

    end; // with
end;

{-----------------------------------------------------------------------------
  Function Name: LimpiarListaComprobantes
  Author:
  Date Created:  13/10/2005
  Description:
  Parameters:
  Return Value:
-----------------------------------------------------------------------------}
procedure TformCobroComprobantes.LimpiarListaComprobantes;
begin
    FTotalComprobantesCobrar := 0;
    FCantidadComprobantesCobrar := 0;
    cds_Comprobantes.DisableControls;
    try
        cds_Comprobantes.EmptyDataSet;
    finally
    	cds_Comprobantes.EnableControls;
	    chkbxSeleccionManual.Checked := False;
    	chkbxSeleccionManual.Enabled := False;
        btnDesmarcarTodos.Enabled    := False;
    end; // finally
end;

{-----------------------------------------------------------------------------
  Function Name: dl_ComprobantesClick
  Author:
  Date Created:  13/10/2005
  Description:
  Parameters:
  Return Value:
-----------------------------------------------------------------------------}
procedure TformCobroComprobantes.dl_ComprobantesClick(Sender: TObject);
begin
    MostrarDatosComprobante;
end;

{-----------------------------------------------------------------------------
  Function Name: dl_ComprobantesKeyDown
  Author:
  Date Created:  13/10/2005
  Description:
  Parameters:
  Return Value:
-----------------------------------------------------------------------------}
procedure TformCobroComprobantes.dl_ComprobantesKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
begin
    if Key = VK_SPACE then begin
        dl_ComprobantesDblClick(Sender);
    end; // if
end;

{-----------------------------------------------------------------------------
  Function Name: CobrarComprobanteConPagoAutomatico
  Author:
  Date Created:  13/10/2005
  Description:
  Parameters:
  Return Value:
-----------------------------------------------------------------------------}
function TformCobroComprobantes.CobrarComprobanteConPagoAutomatico(TipoComprobante: AnsiString; NumeroComprobante, TipoMedioPago: Integer): Boolean;
resourcestring
    MSG_COMPROBANTE_CON_PACPAT_CAPTION  = 'El Comprobante seleccionado tiene asignado' + chr(13) + 'el medio de pago autom�tico: %s' + chr(13) + '�Desea pagarlo igualmente?';
    MSG_COMPROBANTE_CON_PACPAT_TITLE = 'Confirme';
begin
    Result := False;

// revision 10 elimina
//    if (MsgBox(FORMAT(MSG_COMPROBANTE_CON_PACPAT_CAPTION, [QueryGetValue(DMConnections.BaseCAC , 'SELECT dbo.ObtenerDescripcionTipoMedioPago(' + IntToStr(TipoMedioPago) + ')')]),
//      MSG_COMPROBANTE_CON_PACPAT_TITLE, MB_YESNO + MB_ICONQUESTION) = mrYes) then begin
//        Result := True;
//    end;

    if (MsgBox(FORMAT(MSG_COMPROBANTE_CON_PACPAT_CAPTION, [cds_Comprobantes.FieldByName('DescTipoMedioPAgo').AsString ]),
        MSG_COMPROBANTE_CON_PACPAT_TITLE, MB_YESNO + MB_ICONQUESTION) = mrYes) then begin
        Result := True;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: TienePagoAutomatico
  Author:
  Date Created: 13/10/2005
  Description:
  Parameters:
  Return Value:
-----------------------------------------------------------------------------}
function TformCobroComprobantes.TienePagoAutomatico(TipoComprobante: AnsiString; NumeroComprobante: Integer; var TipoMedioPago: Integer): Boolean;
begin
    Result := (cds_Comprobantes.FieldByName('DescPATPAC').AsString <> '') and (cds_Comprobantes.FieldByName('Rechazo').AsString = '');

// revision 10 elimina
//    Result := False;
//    TipoMedioPago := QueryGetValueInt(DMConnections.BaseCAC,'SELECT CodigoTipoMedioPago FROM Comprobantes  WITH (NOLOCK)  ' +' WHERE TipoComprobante = ' + QuotedStr(TipoComprobante) +' AND NumeroComprobante = ' + IntToStr(NumeroComprobante));
//    if ( TipoMedioPago in SetPACPAT ) and ( QueryGetValueInt(DMConnections.BaseCAC, 'SELECT ISNULL((SELECT COUNT(ID) FROM PagosRechazados  WITH (NOLOCK) ' + ' GROUP BY TipoComprobante, NumeroComprobante' + ' HAVING TipoComprobante = ' + QuotedStr(TipoComprobante) +' AND NumeroComprobante = ' + IntToStr(NumeroComprobante) +' ), 0) ' ) = 0 ) and (cds_Comprobantes.FieldByName('EstadoPago').AsString = COMPROBANTE_IMPAGO) then begin
//        Result := True;
//    end;

end;

{
    Function Name: ObtenerComprobantesCobrar
    Parameters : None
    Return Value: TListaComprobante
    Author :
    Date Created : 13/10/2005

    Description : Genera un lista con los Comprobantes a Cobrar

    Revision : 16
        Author: pdominguez
        Date: 03/06/2010
        Description: Infractores Fase 2
            - Se a�ade la asignaci�n del valor de la propiedad TieneConceptosInfractor.
}
function TformCobroComprobantes.ObtenerComprobantesCobrar: TListaComprobante;
var
    Compro: TComprobante;
const
    SQL_NI = 'SELECT dbo.ComprobanteTieneConceptosInfractor(''%s'', %d)';
begin
    Result := Nil;

    if not cds_Comprobantes.IsEmpty then begin
        Result := TListaComprobante.Create;

        cds_Comprobantes.DisableControls;
        try
            cds_Comprobantes.First;
            while not cds_Comprobantes.Eof do begin
                if cds_Comprobantes.FieldByName('Cobrar').AsBoolean then begin
                    Compro := TComprobante.Create;
                    Compro.CodigoCliente        := cds_Comprobantes.FieldByName('CodigoPersona').AsInteger;
                    Compro.TipoComprobante      := cds_Comprobantes.FieldByName('TipoComprobante').AsString;
                    Compro.NumeroComprobante    := cds_Comprobantes.FieldByName('NumeroComprobante').AsInteger;
                    Compro.FechaEmision         := cds_Comprobantes.FieldByName('FechaEmision').AsDateTime;
                    Compro.FechaVencimiento     := cds_Comprobantes.FieldByName('FechaVencimiento').AsDateTime;
                    Compro.TotalComprobante     := cds_Comprobantes.FieldByName('TotalComprobante').AsFloat;
                    Compro.TotalAPagar          := cds_Comprobantes.FieldByName('SaldoPendiente').AsFloat;
                    Compro.TotalPagos           := cds_Comprobantes.FieldByName('TotalPagos').AsFloat;
                    Compro.Pagado               := (cds_Comprobantes.FieldByName('EstadoPago').AsString = COMPROBANTE_PAGO);
                    Compro.EstadoDebito         := cds_Comprobantes.FieldByName('EstadoDebito').AsString;
                    Compro.DescriEstadoPago     := cds_Comprobantes.FieldByName('DescriEstado').AsString;
                    Compro.UltimoComprobante    := cds_Comprobantes.FieldByName('UltimoComprobante').AsInteger;
                    // Rev.15 / 14-04-2009 / Nelson Droguett Sierra.  --------------------------------------------------
                    Compro.TipoComprobanteFiscal := cds_Comprobantes.FieldByName('TipoComprobanteFiscal').AsString;
                    Compro.NumeroComprobanteFiscal := cds_Comprobantes.FieldByName('NumeroComprobanteFiscal').AsInteger;
                    //--------------------------------------------------------------------------------------------------
                    // Rev. 16 (Infractores Fase 2)
                    Compro.TieneConceptosInfractor :=
                        QueryGetValueInt(
                                DMConnections.BaseCAC,
                                Format(
                                    SQL_NI,
                                    [cds_Comprobantes.FieldByName('TipoComprobante').AsString,
                                     cds_Comprobantes.FieldByName('NumeroComprobante').AsInteger])) = 1;
                    // Fin Rev. 16 (Infractores Fase 2)
                    Result.Add(Compro);
                end; // if

                cds_Comprobantes.Next;
            end; // while
        finally
            cds_Comprobantes.EnableControls;
        end; // finally

        if (Result.Count = 0) then begin
            FreeAndNil(Result);
        end; // if
    end; // if
end;


{-----------------------------------------------------------------------------
  Function Name: btnSalirClick
  Author:    lgisuk
  Date Created: 13/10/2005
  Description: Permito salir del formulario
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure TformCobroComprobantes.btnConsultarRefinanciacionClick(Sender: TObject);
begin
    try
        try
            CambiarEstadoCursor(CURSOR_RELOJ, Self, FComponentes);
            TPanelMensajesForm.MuestraMensaje('Cargando Refinanciaci�n Seleccionada', True);

            RefinanciacionGestionForm := TRefinanciacionGestionForm.Create(Self);

            with RefinanciacionGestionForm do begin
                if Inicializar(cdsRefinanciacionesCodigoRefinanciacion.AsInteger, cdsRefinanciacionesCodigoRefinanciacionUnificada.AsInteger, 0, 0, 0, True) then begin    // SS_1051_PDO
                    ShowModal;
                end;
            end;
        except
            on e: Exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
        if Assigned(RefinanciacionGestionForm) then FreeAndNil(RefinanciacionGestionForm);
        CambiarEstadoCursor(CURSOR_DEFECTO, Self, FComponentes);
    end;
end;

procedure TformCobroComprobantes.btnDesmarcarTodosClick(Sender: TObject);
begin
    if (not cds_Comprobantes.IsEmpty) then begin
        MarcarDesmarcarComprobantes(False);
    end;
end;

procedure TformCobroComprobantes.btnFiltrarClick(Sender: TObject);                                                                                      //SS-1014-NDR-20120123
begin                                                                                                                                                   //SS-1014-NDR-20120123
    Screen.Cursor := crHourGlass;                                                                                                                       //SS-1014-NDR-20120123
    btnFiltrar.Enabled:=False;                                                                                                                          //SS-1014-NDR-20120123
                                                                                                                                                        //SS-1014-NDR-20120123
                                                                                                                                                        //SS-1014-NDR-20120123
    if Length(Trim(peNumeroDocumento.Text)) >= 8 then                                                                                                   //SS-1014-NDR-20120123
    begin                                                                                                                                               //SS-1014-NDR-20120123
        FCodigoCliente := QueryGetValueInt( DMConnections.BaseCAC,                                                                                      //SS-1014-NDR-20120123
                                            Format('SELECT CodigoPersona FROM Personas WITH (NOLOCK) WHERE CodigoDocumento = ''RUT'' ' + 'AND NumeroDocumento = ''%s''',
                                            [iif(peNumeroDocumento.Text <> '', trim(PadL(peNumeroDocumento.Text, 9, '0')), '')]));                      //SS-1014-NDR-20120123
        if FCodigoCliente > 0 then                                                                                                                      //SS-1014-NDR-20120123
        begin                                                                                                                                           //SS-1014-NDR-20120123
            CargarConveniosRUT(DMConnections.BaseCAC, cbConvenios, 'RUT', peNumeroDocumento.Text, False, 0, True);                                      //SS-1014-NDR-20120123
            MostrarDatosCliente(FCodigoCliente);                                                                                                        //SS-1014-NDR-20120123
            CargarComprobantes;                                                                                                                         //SS-1014-NDR-20120123
        end                                                                                                                                             //SS-1014-NDR-20120123
    end                                                                                                                                                 //SS-1014-NDR-20120123
    else if Trim(edNumeroComprobante.Text)<>'' then                                                                                                     //SS-1014-NDR-20120123
    begin                                                                                                                                               //SS-1014-NDR-20120123
      CargarComprobantes;                                                                                                                               //SS-1014-NDR-20120123
    end;                                                                                                                                                //SS-1014-NDR-20120123
    btnFiltrar.Enabled:=True;                                                                                                                           //SS-1014-NDR-20120123
    Screen.Cursor := crDefault;                                                                                                                         //SS-1014-NDR-20120123
end;                                                                                                                                                    //SS-1014-NDR-20120123

procedure TformCobroComprobantes.btnSalirClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 13/10/2005
  Description: Libero el Form de Memoria
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure TformCobroComprobantes.FormActivate(Sender: TObject);
begin
    TPanelMensajesForm.OcultaPanel;
end;

procedure TformCobroComprobantes.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    FLogo.Free;
    Action := caFree;

    if Assigned(RefinanciacionConsultasForm) then RefinanciacionConsultasForm.RefrescarDatos;
end;

{******************************** Function Header ******************************
Function Name: MostrarControlesSegunComprobante
Author : mlopez
Date Created : 21/07/2006
Description :
Parameters : None
Return Value : None
*******************************************************************************}
procedure TformCobroComprobantes.MostrarControlesSegunComprobante(TipoComprobante: Variant);
begin
    if TipoComprobante = TC_BOLETA then begin
        lblImpresoraFiscal.Visible := True;
        cbImpresorasFiscales.Visible := True;
        edNumeroComprobante.Width := 122;
    end else begin
        lblImpresoraFiscal.Visible := False;
        cbImpresorasFiscales.Visible := False;
        edNumeroComprobante.Width := 297;
    end;
end;

{******************************** Function Header ******************************
Function Name: cbImpresorasFiscalesChange
Author : mlopez
Date Created : 28/07/2006
Description :
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TformCobroComprobantes.cbImpresorasFiscalesChange(
  Sender: TObject);
begin
    if ActiveControl <> Sender then Exit;
    peNumeroDocumento.Clear;                                                    //SS-1014-NDR-20120123
    FCodigoCliente := -1;                                                       //SS-1014-NDR-20120123
    LimpiarDetalles;                                                            //SS-1014-NDR-20120123
    LimpiarConvenios;                                                           //SS-1014-NDR-20120123
    LimpiarListaComprobantes;                                                   //SS-1014-NDR-20120123
    LimpiarRefinanciaciones;                                                    //SS-1014-NDR-20120123
    LimpiarDatosCliente;                                                        //SS-1014-NDR-20120123
    HabilitarFiltroRUT(edNumeroComprobante.Text = EmptyStr);                    //SS-1014-NDR-20120123
   	//if Timer1.Enabled then begin                                              //SS-1014-NDR-20120123
    //    Timer1.Enabled := False;                                              //SS-1014-NDR-20120123
    //    Timer1.Enabled := True;                                               //SS-1014-NDR-20120123
    //end else Timer1.Enabled := True;                                          //SS-1014-NDR-20120123
end;

{INICIO:	TASK_117_CFU_20170308
procedure TformCobroComprobantes.CargarRefinanciaciones(CodigoPersona: Integer);
var
result : Boolean;
RutPersona : string;
begin
    try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        //TASK_013_GLE_20161103 inicio

         RutPersona := QueryGetValue(DMConnections.BaseCAC, Format('SELECT dbo.ObtenerRUTPersona(%d)', [CodigoPersona]));

         spObtenerRefinanciacionCuotasAPagar.Parameters.Refresh;
         spObtenerRefinanciacionCuotasAPagar.Parameters.ParamByName( '@CodigoPersona' ).Value := iif(CodigoPersona = 0, NUll, CodigoPersona);
         spObtenerRefinanciacionCuotasAPagar.Parameters.ParamByName( '@RUTPersona').Value := IIf(RutPersona = '', NULL, RutPersona);
         spObtenerRefinanciacionCuotasAPagar.Parameters.ParamByName( '@CodigoRefinanciacion').Value := iif(vcbRefinanciacion.Value = 0, NULL, vcbRefinanciacion.Value);;
         result := (OpenTables([spObtenerRefinanciacionCuotasAPagar]));

         spObtenerRefinanciacionCuotasAPagar.Open;
         //cdsCuotasAPagar.Open;

         if spObtenerRefinanciacionCuotasAPagar.RecordCount > 0 then begin

            while not spObtenerRefinanciacionCuotasAPagar.Eof do begin
                //CFU 20170308{
                cdsCuotasAPagar.Append;

                cdsCuotasAPagar.FieldByName('Cobro').AsBoolean := True;
                cdsCuotasAPagar.FieldByName('DescripcionFormaPago').AsString := spObtenerRefinanciacionCuotasAPagar.FieldByName('DescripcionFormaPago').AsString;
                cdsCuotasAPagar.FieldByName('FechaCuota').AsDateTime := spObtenerRefinanciacionCuotasAPagar.FieldByName('FechaCuota').AsDateTime;
                cdsCuotasAPagar.FieldByName('ImporteDesc').AsFloat := spObtenerRefinanciacionCuotasAPagar.FieldByName('ImporteDesc').AsFloat;
                cdsCuotasAPagar.FieldByName('SaldoDesc').AsFloat := spObtenerRefinanciacionCuotasAPagar.FieldByName('SaldoDesc').AsFloat;
                cdsCuotasAPagar.FieldByName('DescripcionEstadoCuota').AsString := spObtenerRefinanciacionCuotasAPagar.FieldByName('DescripcionEstadoCuota').AsString;
                cdsCuotasAPagar.FieldByName('CodigoRefinanciacion').AsInteger := spObtenerRefinanciacionCuotasAPagar.FieldByName('CodigoRefinanciacion').AsInteger;
                cdsCuotasAPagar.FieldByName('NumeroConvenio').AsInteger := spObtenerRefinanciacionCuotasAPagar.FieldByName('NumeroConvenio').AsInteger;
                cdsCuotasAPagar.FieldByName('TitularTipoDocumento').AsString := spObtenerRefinanciacionCuotasAPagar.FieldByName('TitularTipoDocumento').AsString;
                cdsCuotasAPagar.FieldByName('TitularNombre').AsString := spObtenerRefinanciacionCuotasAPagar.FieldByName('TitularNombre').AsString;
                cdsCuotasAPagar.FieldByName('DocumentoNumero').AsInteger := spObtenerRefinanciacionCuotasAPagar.FieldByName('DocumentoNumero').AsInteger;
                cdsCuotasAPagar.FieldByName('DescripcionBanco').AsString := spObtenerRefinanciacionCuotasAPagar.FieldByName('DescripcionBanco').AsString;
                cdsCuotasAPagar.FieldByName('DescripcionTarjeta').AsString := spObtenerRefinanciacionCuotasAPagar.FieldByName('DescripcionTarjeta').AsString;
                cdsCuotasAPagar.FieldByName('TarjetaCuotas').AsInteger := spObtenerRefinanciacionCuotasAPagar.FieldByName('TarjetaCuotas').AsInteger;
                //CFU 20170308
            end;
         end;

        //TASK_013_GLE_20161103 inicio

        if cdsCuotasAPagar.Active then cdsCuotasAPagar.Close;

        { //TASK_013_GLE_20161103 inicia comentario
        with spObtenerRefinanciacionCuotasAPagar do begin
            Parameters.Refresh;
            Parameters.ParamByName('@CodigoPersona').Value        := iif(CodigoPersona = 0, NUll, CodigoPersona);
            Parameters.ParamByName('@RUTPersona').Value           := NULL;
            Parameters.ParamByName('@CodigoRefinanciacion').Value := iif(vcbRefinanciacion.Value = 0, NULL, vcbRefinanciacion.Value);
        end;

        cdsCuotasAPagar.Open;
        //CFU 20170308 //TASK_013_GLE_20161103 termina comentario

        with spObtenerRefinanciacionesConsultas do begin
            Parameters.Refresh;
            with Parameters do begin
                ParamByName('@CodigoPersona').Value := iif(CodigoPersona = 0, NUll, CodigoPersona);
                ParamByName('@CodigoEstado').Value  := REFINANCIACION_ESTADO_ACEPTADA;
            end;
        end;

        cdsRefinanciaciones.Open;

        btnActivarRefinanciacion.Enabled   := FbtnActivarRefinanciacionPermisos and (cdsRefinanciaciones.RecordCount > 0) and cdsRefinanciacionesValidada.AsBoolean;
        btnConsultarRefinanciacion.Enabled := (cdsRefinanciaciones.RecordCount > 0);

        btnCobrarCuota.Enabled             := False;
    finally
        Screen.Cursor := crDefault;
    end;
end;
}

procedure TformCobroComprobantes.CargarRefinanciaciones(CodigoPersona: Integer);
begin
    try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        if cdsCuotasAPagar.Active then cdsCuotasAPagar.Close;

        with spObtenerRefinanciacionCuotasAPagar do begin
            Parameters.Refresh;
            Parameters.ParamByName('@CodigoPersona').Value        := iif(CodigoPersona = 0, NUll, CodigoPersona);
            Parameters.ParamByName('@RUTPersona').Value           := NULL;
            Parameters.ParamByName('@CodigoRefinanciacion').Value := iif(vcbRefinanciacion.Value = 0, NULL, vcbRefinanciacion.Value);
        end;

        cdsCuotasAPagar.Open;

        if cdsRefinanciaciones.Active then cdsRefinanciaciones.Close;

        with spObtenerRefinanciacionesConsultas do begin
            Parameters.Refresh;
            with Parameters do begin
                ParamByName('@CodigoPersona').Value := iif(CodigoPersona = 0, NUll, CodigoPersona);
                {INICIO:	TASK_117_CFU_20170308
                ParamByName('@CodigoEstado').Value  := REFINANCIACION_ESTADO_ACEPTADA;
                TERMINO:	TASK_117_CFU_20170308}
            end;
        end;

        cdsRefinanciaciones.Open;
        //TASK_005_AUN_20170403-CU.COBO.COB.301: No puedo activar una refinanciacion ya activada (es decir con estado = Vigente)
        btnActivarRefinanciacion.Enabled   := FbtnActivarRefinanciacionPermisos and (cdsRefinanciaciones.RecordCount > 0) and
                                              cdsRefinanciacionesValidada.AsBoolean and (cdsRefinanciacionesCodigoEstado.AsInteger <> REFINANCIACION_ESTADO_VIGENTE);
        btnConsultarRefinanciacion.Enabled := (cdsRefinanciaciones.RecordCount > 0);

        btnCobrarCuota.Enabled             := False;
    finally
        Screen.Cursor := crDefault;
    end;
end;
//TERMINO:	TASK_117_CFU_20170308

procedure TformCobroComprobantes.DBListEx1DrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
var
    bmp: TBitMap;
begin
    with Sender do begin
        if (Column.FieldName = 'Validada') then try
            Text := '';
            Canvas.FillRect(Rect);
            bmp := TBitMap.Create;
			DefaultDraw := False;

            if (cdsRefinanciaciones.FieldByName(Column.FieldName).AsBoolean ) then
                lnCheck.GetBitmap(0, Bmp)
            else
                lnCheck.GetBitmap(1, Bmp);

            Canvas.Draw((Rect.Left + Rect.Right - Bmp.Width) div 2, Rect.Top, bmp);
        finally
            bmp.Free;
        end; // finally
    end; // with
end;

procedure TformCobroComprobantes.DBListEx2DrawBackground(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var DefaultDraw: Boolean);
begin
    with Sender do begin
        if ((cdsCuotasAPagarFechaCuota.AsDateTime < FAhora) and cdsCuotasAPagarEstadoImpago.AsBoolean) or
            (cdsCuotasAPagarCodigoEstadoCuota.AsInteger in [REFINANCIACION_ESTADO_CUOTA_ANULADA, REFINANCIACION_ESTADO_CUOTA_ANULADA_POR_MORA]) then begin
            if (odSelected in State) and (odFocused in state) then Canvas.Brush.Color := clMaroon;
        end;
    end;
end;

procedure TformCobroComprobantes.DBListEx2DrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
    var
        Bmp: TBitmap;
begin
    with Sender do begin
        if ((cdsCuotasAPagarFechaCuota.AsDateTime < FAhora) and cdsCuotasAPagarEstadoImpago.AsBoolean) or
            (cdsCuotasAPagarCodigoEstadoCuota.AsInteger in [REFINANCIACION_ESTADO_CUOTA_ANULADA, REFINANCIACION_ESTADO_CUOTA_ANULADA_POR_MORA]) then begin

            if (not (odSelected in State)) or (not (odFocused in state)) then begin
                Canvas.Font.Color := clRed;
            end;

            if Column.FieldName = 'DescripcionEstadoCuota' then begin
                if cdsCuotasAPagarCodigoEstadoCuota.AsInteger in [REFINANCIACION_ESTADO_CUOTA_ANULADA, REFINANCIACION_ESTADO_CUOTA_ANULADA_POR_MORA] then
                    Canvas.Font.Style := Canvas.Font.Style + [fsBold];
            end;
        end;

        if (Column.FieldName = 'Cobrar') then begin
            Text := '';
            Canvas.FillRect(Rect);
            bmp := TBitMap.Create;
			DefaultDraw := False;
            try
                if (cdsCuotasAPagarCobrar.AsBoolean ) then begin
                    lnCheck.GetBitmap(0, Bmp);
                end
                else begin
                    lnCheck.GetBitmap(1, Bmp);
                end;

                Canvas.Draw((Rect.Left + Rect.Right - Bmp.Width) div 2, Rect.Top, bmp);
            finally
                bmp.Free;
            end;
        end;
    end;
end;

procedure TformCobroComprobantes.DBListEx2LinkClick(Sender: TCustomDBListEx; Column: TDBListExColumn);
begin
    cdsCuotasAPagar.Edit;
    cdsCuotasAPagarCobrar.AsBoolean := not cdsCuotasAPagarCobrar.AsBoolean;
    cdsCuotasAPagar.Post;

    if cdsCuotasAPagarCobrar.AsBoolean then begin
        Inc(FCantidadCuotasCobrar);
        FTotalCuotasCobrar := FTotalCuotasCobrar + cdsCuotasAPagarSaldo.AsLargeInt;
    end
    else begin
        Dec(FCantidadCuotasCobrar);
        FTotalCuotasCobrar := FTotalCuotasCobrar - cdsCuotasAPagarSaldo.AsLargeInt;
    end;

    VerificarCuotasACobrar;
end;

procedure TformCobroComprobantes.LimpiarRefinanciaciones;
begin
    if cdsRefinanciaciones.Active then cdsRefinanciaciones.Close;
    if cdsCuotasAPagar.Active then cdsCuotasAPagar.Close;

    lblCuotasCantidad.Caption := '0';
    lblCuotasTotal.Caption    := '$ 0';

    lblMontoDeudasConvenios.Caption         := '$ 0';
    lblMontoDeudasRefinanciacion.Caption    := '$ 0';
    lblMontoDeudasConvenios.Font.Color      := clBlack;
    lblMontoDeudasRefinanciacion.Font.Color := clBlack;

    btnActivarRefinanciacion.Enabled   := False;
    btnConsultarRefinanciacion.Enabled := False;
    btnCobrarCuota.Enabled             := False;

    vcbRefinanciacion.Clear;
    vcbRefinanciacion.Enabled := False;
end;

function TformCobroComprobantes.ActivarRefinanciacion: Boolean;
    resourcestring
        rs_ErrorReciboTitulo                           = 'Generar Recibo';
        rs_ErrorreciboMensaje                          = 'Se produjo un Error al Generar el Recibo.';
        rs_ErrorCancelarCamprobanteTitulo              = 'Cancelar Comprobante';
        rs_ErrorCancelarComprobanteMensaje             = 'Se produjo un Error al Cancelar el Comprobante %s - %d.';
        rs_ErrorRegistrarDetallePagoComprobanteTitulo  = 'Registrar Cuota en Detalle Pagos Comprobantes.';
        rs_ErrorRegistrarPagoComprobanteTitulo         = 'Registrar Cuota en Pagos Comprobantes.';
        rs_ErrorRegistrarPagoComprobanteMensaje        = 'Se produjo un Error al Registrar la Cuota Tipo %s de Fecha %s';
        rs_ErrorActivarRefinanciacionTitulo            = 'Activar Refinanciaci�n';
        rs_ErrorActivarRefinanciacionMensaje           = 'Se produjo un Error al Activar la Refinanciaci�n.';
        rs_DescripcionPago                             = 'Refinanciado. Tipo Cuota: %s';
        rs_ActivacionCorrectaMensaje                   = 'Se Activ� Correctamente la Refinanciaci�n Seleccionada.';
        rs_ErrorComprobanteImportesVariaronTitulo      = 'Validaci�n Importes';
        rs_ErrorComprobanteImportesVariaron            = 'El Saldo Pendiente del comprobante %s - %s, no es el mismo que el Importe Refinanciado. No se puede Activar la Refinanciaci�n.';
        rs_ErrorComprobanteImportesVariaronEnElProceso = 'El Saldo Pendiente del comprobante %s - %s, no es el mismo que el Importe Refinanciado. Cambi� el Saldo en el transcurso de la Activaci�n. No se puede Activar la Refinanciaci�n.';

	    MSG_REGISTRO_CORRECTO = 'El pago se ha registrado correctamente.';
	    MSG_CONFIRMAR_RECIBO  = '� Desea Imprimir el Comprobante de Cancelaci�n ?';
	    MSG_CAPTION           = 'Registrar Pago';
	    MSG_IMPRESION_EXITOSA = '� La impresi�n se ha realizado con �xito ?';      //SS-1006-1015-MCO-20120904
        MSG_ERROR_IMPRESION   = 'Error al intentar Imprimir el comprobante';
    var
        NumeroRecibo: Int64;
        IDDetallePagoComprobante: Integer;
	    fRecibo: TformRecibo;
    	RespuestaImpresion: TRespuestaImprimir;
        DocumentoOriginal: String;

        ImportePendienteAplicar,
        ImporteAplicado,
        ImportePagoActual: Int64;
        UltimoNumeroPago: Integer;

        RefinanciacionAActivar: Integer;							//SS_1051_PDO

    procedure RegistrarPagoComprobante;
    begin
        TPanelMensajesForm.MuestraMensaje(Format('Registrando Pago %s ...', [Format(rs_DescripcionPago, [cdsRefinanciacionCuotasDescripcionFormaPago.AsString])]));
        with spRefinanciacionRegistrarPagoComprobante do begin
            if Active then Close;
            Parameters.Refresh;

            Parameters.ParamByName('@TipoComprobante').Value          := cdsRefinanciacionComprobantesTipoComprobante.AsString;
            Parameters.ParamByName('@NumeroComprobante').Value        := cdsRefinanciacionComprobantesNumeroComprobante.AsLargeInt;
            Parameters.ParamByName('@Importe').Value                  := cdsRefinanciacionCuotasImporte.AsLargeInt * 100;
            Parameters.ParamByName('@NumeroRecibo').Value             := NumeroRecibo;
            Parameters.ParamByName('@NumeroTurno').Value              := GNumeroTurno;
            Parameters.ParamByName('@Usuario').Value                  := UsuarioSistema;
            Parameters.ParamByName('@IDDetallePagoComprobante').Value := IDDetallePagoComprobante;
            Parameters.ParamByName('@CodigoConceptoPago').Value       := ObtenerCodigoConceptoPago(DMConnections.BaseCAC, cdsRefinanciacionesCodigoTipoMedioPago.AsInteger);
            Parameters.ParamByName('@DescripcionPago').Value          := Format(rs_DescripcionPago, [cdsRefinanciacionCuotasDescripcionFormaPago.AsString]);
            Parameters.ParamByName('@NumeroPago').Value               := 0;

            ExecProc;

            if Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
                raise EErrorExceptionCN.Create(
                        rs_ErrorRegistrarPagoComprobanteTitulo,
                        Format(rs_ErrorRegistrarPagoComprobanteMensaje,
                                [cdsRefinanciacionComprobantesTipoComprobante.AsString,
                                 FormatDateTime('dd/mm/yyyy', cdsRefinanciacionCuotasFechaCuota.AsDateTime)]));
            end;

            UltimoNumeroPago  := Parameters.ParamByName('@NumeroPago').Value;
            ImportePagoActual := cdsRefinanciacionCuotasImporte.AsLargeInt * 100;
        end;
    end;

    procedure RegistrarDetallePagoComprobante;
    begin
        with spRegistrarDetallePagoComprobante do begin
            if Active then Close;
            Parameters.Refresh;
            Parameters.ParamByName('@IDDetallePagoComprobante').Value := Null;
            Parameters.ParamByName('@CodigoTipoMedioPago').Value      := cdsRefinanciacionDatosCodigoTipoMedioPago.AsInteger;
            Parameters.ParamByName('@CodigoFormaPago').Value          := cdsRefinanciacionCuotasCodigoFormaPago.AsInteger;
            Parameters.ParamByName('@ChequeCodigoBanco').Value        := iif(cdsRefinanciacionCuotasCodigoFormaPago.AsInteger in [4, 8, 10], cdsRefinanciacionCuotasCodigoBanco.AsInteger, Null);
            Parameters.ParamByName('@ChequeNumero').Value             := iif(cdsRefinanciacionCuotasCodigoFormaPago.AsInteger in [4, 8, 10], cdsRefinanciacionCuotasDocumentoNumero.AsVariant, Null);
            Parameters.ParamByName('@ChequeTitular').Value            := iif(cdsRefinanciacionCuotasCodigoFormaPago.AsInteger in [4, 8, 10], cdsRefinanciacionCuotasTitularNombre.AsVariant, Null);
            Parameters.ParamByName('@ChequeFecha').Value              := cdsRefinanciacionCuotasFechaCuota.AsDateTime;
            Parameters.ParamByName('@ChequeCodigoDocumento').Value    := iif(cdsRefinanciacionCuotasCodigoFormaPago.AsInteger in [4, 8, 10], cdsRefinanciacionCuotasTitularTipoDocumento.AsVariant, Null);
            Parameters.ParamByName('@ChequeNumeroDocumento').Value    := iif(cdsRefinanciacionCuotasCodigoFormaPago.AsInteger in [4, 8, 10], cdsRefinanciacionCuotasTitularNumeroDocumento.AsVariant, Null);
            Parameters.ParamByName('@ChequeMonto').Value              := cdsRefinanciacionCuotasImporte.AsLargeInt * 100;
            Parameters.ParamByName('@Autorizacion').Value             := iif(cdsRefinanciacionCuotasCodigoFormaPago.AsInteger in [5, 6], cdsRefinanciacionCuotasDocumentoNumero.AsVariant, Null);
            Parameters.ParamByName('@Cuotas').Value                   := cdsRefinanciacionCuotasTarjetaCuotas.AsVariant;
            Parameters.ParamByName('@NumeroPOS').Value                := iif(FNumeroPOS > 0, FNumeroPOS, Null);
            Parameters.ParamByName('@Titular').Value                  := iif(cdsRefinanciacionCuotasCodigoFormaPago.AsInteger in [5, 6], cdsRefinanciacionCuotasTitularNombre.AsVariant, Null);
            Parameters.ParamByName('@CodigoTipoTarjeta').Value        := iif(cdsRefinanciacionCuotasCodigoFormaPago.AsInteger in [6], cdsRefinanciacionCuotasCodigoTarjeta.AsVariant, Null);
            Parameters.ParamByName('@CodigoBancoEmisor').Value        := iif(cdsRefinanciacionCuotasCodigoFormaPago.AsInteger in [5, 6], cdsRefinanciacionCuotasCodigoBanco.AsVariant, Null);
            Parameters.ParamByName('@RepactacionNumero').Value        := cdsRefinanciacionDatosCodigoRefinanciacion.AsString;

            ExecProc;

            if Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
                raise EErrorExceptionCN.Create(
                        rs_ErrorRegistrarDetallePagoComprobanteTitulo,
                        Format(rs_ErrorRegistrarPagoComprobanteMensaje,
                                [cdsRefinanciacionComprobantesTipoComprobante.AsString,
                                 FormatDateTime('dd/mm/yyyy', cdsRefinanciacionCuotasFechaCuota.AsDateTime)]));
            end;

            IDDetallePagoComprobante := Parameters.ParamByName('@IDDetallePagoComprobante').Value;
        end;
    end;

    procedure RegistrarCancelacionComprobante(ImporteACancelar: Int64);
    begin
        TPanelMensajesForm.MuestraMensaje(Format('Registrando Cancelaci�n Comprobante %s - %d ...', [cdsRefinanciacionComprobantesTipoComprobante.AsString, cdsRefinanciacionComprobantesNumeroComprobante.AsInteger]));
        with spRefinanciacionCancelarComprobante do begin
            if Active then Close;
            Parameters.Refresh;
            Parameters.ParamByName('@TipoComprobante').Value        := cdsRefinanciacionComprobantesTipoComprobante.AsString;
            Parameters.ParamByName('@NumeroComprobante').Value      := cdsRefinanciacionComprobantesNumeroComprobante.AsInteger;
            Parameters.ParamByName('@Importe').Value                := ImporteACancelar;
            Parameters.ParamByName('@NumeroRecibo').Value           := NumeroRecibo;
            Parameters.ParamByName('@NumeroTurno').Value            := GNumeroTurno;
            Parameters.ParamByName('@Usuario').Value                := UsuarioSistema;
            Parameters.ParamByName('@NumeroPago').Value             := UltimoNumeroPago;
            Parameters.ParamByName('@ImporteAplicadoDelPago').Value := 0;
            ExecProc;

            if Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
                raise EErrorExceptionCN.Create(
                        rs_ErrorCancelarCamprobanteTitulo,
                        Format(rs_ErrorCancelarComprobanteMensaje,
                                [cdsRefinanciacionComprobantesTipoComprobante.AsString,
                                 cdsRefinanciacionComprobantesNumeroComprobante.AsInteger]));
            end;

            ImporteAplicado         := Parameters.ParamByName('@ImporteAplicadoDelPago').Value;
            ImportePendienteAplicar := ImporteACancelar - ImporteAplicado;
        end;
    end;

    procedure RegistrarPagoRefinanciacion;
    begin
        if not cdsRefinanciacionCuotas.Eof then begin
            RegistrarDetallePagoComprobante;
            RegistrarPagoComprobante;

            cdsRefinanciacionCuotas.Next;
        end;
    end;

    Function EsComprobanteInfracciones(pTipoComprobante: String; pNumeroComprobante: Integer): Boolean;     // SS_637_PDO_20110714
        const                                                                                               // SS_637_PDO_20110714
            SQL_NI = 'SELECT dbo.ComprobanteTieneConceptosInfractor(''%s'', %d)';                           // SS_637_PDO_20110714
    begin                                                                                                   // SS_637_PDO_20110714
        Result :=                                                                                           // SS_637_PDO_20110714
            QueryGetValueInt(                                                                               // SS_637_PDO_20110714
                DMConnections.BaseCAC,                                                                      // SS_637_PDO_20110714
                Format(                                                                                     // SS_637_PDO_20110714
                    SQL_NI,                                                                                 // SS_637_PDO_20110714
                    [pTipoComprobante,                                                                      // SS_637_PDO_20110714
                     pNumeroComprobante])) = 1;                                                             // SS_637_PDO_20110714
    end;                                                                                                    // SS_637_PDO_20110714

begin
    try
        CambiarEstadoCursor(CURSOR_RELOJ, Self, FComponentes);

        Result := False;

		//BEGIN : SS_1051_PDO----------------------------------------------------------------------------------------------------------------------
        TPanelMensajesForm.MuestraMensaje('Activando Refinanciaci�n Seleccionada ...', True);
        try
            RefinanciacionAActivar := cdsRefinanciacionesCodigoRefinanciacion.AsInteger;

            QueryExecute(DMConnections.BaseCAC, 'BEGIN TRANSACTION ActivarRefinanciacion');

            while (not cdsRefinanciaciones.Eof) and
                (
                    (cdsRefinanciacionesCodigoRefinanciacionUnificada.AsInteger = RefinanciacionAActivar)
                    or (cdsRefinanciacionesCodigoRefinanciacion.AsInteger = RefinanciacionAActivar)
                 ) do begin

                if cdsRefinanciacionDatos.Active then cdsRefinanciacionDatos.Close;

                with spObtenerRefinanciacionDatosCabecera do begin
                    Parameters.Refresh;
                    Parameters.ParamByName('@CodigoRefinanciacion').Value := cdsRefinanciacionesCodigoRefinanciacion.AsInteger;
                end;

                cdsRefinanciacionDatos.Open;


                // Generar Recibo
                TPanelMensajesForm.MuestraMensaje('Generando Recibo Comprobantes Refinanciados ...');
                with spCrearRecibo do begin
                    if Active then Close;
                    Parameters.Refresh;
                    with Parameters do begin
                        ParamByName('@FechaHora').Value           := NowBase(DMConnections.BaseCAC); //Revision 16
                        //ParamByName('@Importe').Value             := cdsRefinanciacionDatosTotalDeuda.AsInteger * 100;		//SS_1200_MCA_20140704
                        ParamByName('@Importe').Value             := Trunc(cdsRefinanciacionDatosTotalDeuda.AsFloat) * 100;     //SS_1200_MCA_20140704
                        ParamByName('@NumeroRecibo').Value        := Null;
                        ParamByName('@CodigoTipoMedioPago').Value := cdsRefinanciacionDatosCodigoTipoMedioPago.AsInteger;
                        ParamByName('@CodigoUsuario').Value       := UsuarioSistema;
                    end;

                    ExecProc;

                    if Parameters.ParamByName('@RETURN_VALUE').Value <> -1 then
                        NumeroRecibo := Parameters.ParamByName('@NumeroRecibo').Value
                    else
                        raise EErrorExceptionCN.Create(rs_ErrorReciboTitulo, rs_ErrorreciboMensaje);
                end;

                TPanelMensajesForm.MuestraMensaje('Seleccionando Comprobantes Refinanciados a Cancelar ...');
                if cdsRefinanciacionComprobantes.Active then cdsRefinanciacionComprobantes.Close;

                with spObtenerRefinanciacionComprobantes do begin
                    Parameters.Refresh;
                    Parameters.ParamByName('@CodigoRefinanciacion').Value := cdsRefinanciacionDatosCodigoRefinanciacion.AsInteger;
                end;

                cdsRefinanciacionComprobantes.Open;

                TPanelMensajesForm.MuestraMensaje('Validando Importes en Comprobantes Refinanciados a Cancelar ...');
                with cdsRefinanciacionComprobantes do begin
                    if cdsRefinanciacionComprobantesSaldoPendienteComprobante.AsLargeInt <> cdsRefinanciacionComprobantesSaldoRefinanciado.AsLargeInt then begin
                        raise EErrorExceptionCN.Create(
                                rs_ErrorComprobanteImportesVariaronTitulo,
                                Format(
                                    rs_ErrorComprobanteImportesVariaron,
                                    [cdsRefinanciacionComprobantesTipoComprobante.AsString, cdsRefinanciacionComprobantesNumeroComprobante.AsString]));
                    end;

                    Next;
                end;

                TPanelMensajesForm.MuestraMensaje('Seleccionando Cuotas a Registrar su Pago ...');
                if cdsRefinanciacionCuotas.Active then cdsRefinanciacionCuotas.Close;

                with spObtenerRefinanciacionCuotas do begin
                    Parameters.Refresh;
                    Parameters.ParamByName('@CodigoRefinanciacion').Value := cdsRefinanciacionDatosCodigoRefinanciacion.AsInteger;
                    Parameters.ParamByName('@CodigoCuota').Value          := Null;
                    Parameters.ParamByName('@SinCuotasAnuladas').Value    := 0;
                end;

                cdsRefinanciacionCuotas.Open;

                ImporteAplicado          := 0;
                ImportePendienteAplicar  := 0;
                UltimoNumeroPago         := 0;

                RegistrarPagoRefinanciacion;

                cdsRefinanciacionComprobantes.First;

                while not cdsRefinanciacionComprobantes.Eof do begin

                    if ImportePendienteAplicar <> 0 then begin
                        RegistrarCancelacionComprobante(ImportePendienteAplicar);
                    end
                    //else RegistrarCancelacionComprobante(cdsRefinanciacionComprobantesSaldoRefinanciado.AsInteger * 100); 		//SS_1200_MCA_20140704
                    else RegistrarCancelacionComprobante(cdsRefinanciacionComprobantesSaldoRefinanciado.AsLargeInt * 100);          //SS_1200_MCA_20140704

                    if (ImporteAplicado = 0) or (((ImportePagoActual - ImporteAplicado) > ImportePendienteAplicar) and (ImportePendienteAplicar > 0)) then begin
                        raise EErrorExceptionCN.Create(
                                rs_ErrorComprobanteImportesVariaronTitulo,
                                Format(
                                    rs_ErrorComprobanteImportesVariaronEnElProceso,
                                    [cdsRefinanciacionComprobantesTipoComprobante.AsString, cdsRefinanciacionComprobantesNumeroComprobante.AsString]));
                    end;

                    ImportePagoActual := ImportePagoActual - ImporteAplicado;

                    if ImportePagoActual = 0 then begin
                        RegistrarPagoRefinanciacion;
                    end;

                    if ImportePendienteAplicar = 0 then begin
                        {
                        if EsComprobanteInfracciones(cdsRefinanciacionComprobantesTipoComprobante.AsString, cdsRefinanciacionComprobantesNumeroComprobante.AsInteger) then begin    // SS_637_PDO_20110714
                            with spActualizarInfraccionesPagadas do begin                                                                                                           // SS_637_PDO_20110714
                                Parameters.ParamByName('@NumeroComprobante').Value := cdsRefinanciacionComprobantesNumeroComprobante.AsInteger;                                     // SS_637_PDO_20110714
                                Parameters.ParamByName('@TipoComprobante').Value   := cdsRefinanciacionComprobantesTipoComprobante.AsString;                                        // SS_637_PDO_20110714
                                ExecProc;                                                                                                                                           // SS_637_PDO_20110714
                            end;                                                                                                                                                    // SS_637_PDO_20110714
                        end;                                                                                                                                                        // SS_637_PDO_20110714
                        }
                        cdsRefinanciacionComprobantes.Next;
                    end;
                end;

                TPanelMensajesForm.MuestraMensaje('Activando Refinanciaci�n ...');
                with spActualizarRefinanciacionActivada,  spActualizarRefinanciacionActivada.Parameters do begin
                    if Active then Close;
                    Parameters.Refresh;
                    ParamByName('@CodigoRefinanciacion').Value := cdsRefinanciacionDatosCodigoRefinanciacion.AsInteger;
                    ParamByName('@Usuario').Value              := UsuarioSistema;

                    ExecProc;

                    if ParamByName('@RETURN_VALUE').Value <> 0 then begin
                        raise EErrorExceptionCN.Create(
                                rs_ErrorActivarRefinanciacionTitulo,
                                rs_ErrorActivarRefinanciacionMensaje);
                    end;
                end;

                TPanelMensajesForm.MuestraMensaje('Numerando Cuotas ...');                                                     // SS_1050_PDO_20120605
                RefinanciacionRenumerarCuotas(DMConnections.BaseCAC, cdsRefinanciacionDatosCodigoRefinanciacion.AsInteger);    // SS_1050_PDO_20120605

                cdsRefinanciaciones.Next;
            end;

            QueryExecute(DMConnections.BaseCAC, 'IF @@TRANCOUNT>0 BEGIN COMMIT TRANSACTION ActivarRefinanciacion END');

            Result := True;
            TPanelMensajesForm.OcultaPanel;

            ShowMsgBoxCN(rs_ErrorActivarRefinanciacionTitulo, rs_ActivacionCorrectaMensaje, MB_ICONINFORMATION, Self);
            {
            RespuestaImpresion :=
                frmImprimir.Ejecutar(
                    MSG_CAPTION,
                    MSG_REGISTRO_CORRECTO + CRLF + MSG_CONFIRMAR_RECIBO);

            case RespuestaImpresion of
                (* Si la respuesta fue Aceptar, ejecutar el reporte. *)
                riAceptarConfigurarImpresora, riAceptar: begin
                    fRecibo := TformRecibo.Create(Nil);
                    try
                        try
                            if fRecibo.Inicializar(FLogo, NumeroRecibo, RespuestaImpresion = riAceptarConfigurarImpresora) then begin
                                (* Preguntar si la impresi�n fue exitosa. *)
                                while MsgBox(MSG_IMPRESION_EXITOSA, MSG_CAPTION, MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = mrNo do begin
                                    (* Mostrar el di�logo de Configurar Impresora para permitir al operador
                                    reintentar la impresi�n. *)
                                    if not fRecibo.Inicializar(FLogo, NumeroRecibo, True) then Break;
                                end;
                            end;
                        except
                            On E: Exception do begin
                                MsgBoxErr(MSG_ERROR_IMPRESION, e.Message, Caption, MB_ICONERROR);
                            end;
                        end;
                    finally
                        if Assigned(fRecibo) then FreeAndNil(fRecibo);
                    end;
                end;
            end;
            }
        except
            on e:Exception do begin
                TPanelMensajesForm.OcultaPanel;
                QueryExecute(DMConnections.BaseCAC, 'IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION ActivarRefinanciacion END');
                ShowMsgBoxCN(e, Self);
            end;
        end;
		//END : SS_1051_PDO------------------------------------------------------------------------------------------------------------------------

    finally
        if cdsRefinanciacionDatos.Active then cdsRefinanciacionDatos.Close;
        if cdsRefinanciacionComprobantes.Active then cdsRefinanciacionComprobantes.Close;
        if cdsRefinanciacionCuotas.Active then cdsRefinanciacionCuotas.Close;

        TPanelMensajesForm.OcultaPanel;
        CambiarEstadoCursor(CURSOR_DEFECTO, Self, FComponentes);
    end;
end;

procedure TformCobroComprobantes.MostrarSaldosPersona(CodigoPersona: Integer);
    var
        TotalDeudasRefinanciacion,
        TotalDeudasConvenios,
        DeudaRefinanciacionVencida,
        DeudaComprobantesVencida: Extended;
    const
        SQLDeudaConvenios = 'SELECT dbo.ObtenerSaldoDeLosConvenios(%d)';
begin
    try
        Screen.Cursor := crHourGlass;
        EmptyKeyQueue;
        Application.ProcessMessages;

        try
            with spObtenerDeudaComprobantesVencidos do begin
                if Active then Close;
                Parameters.Refresh;

                Parameters.ParamByName('@CodigoConvenio').Value := Null;
                Parameters.ParamByName('@CodigoCliente').Value  := FCodigoCliente;
                Open;

                DeudaComprobantesVencida := FieldByName('DeudaComprobantesVencidos').AsFloat;
            end;

            with spObtenerRefinanciacionesTotalesPersona do begin
                if Active then Close;
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoPersona').Value := CodigoPersona;
                Parameters.PAramByName('@CodigoConvenio').Value := Null;
                Open;

                TotalDeudasRefinanciacion  := FieldByName('TotalDeuda').AsFloat - FieldByName('TotalPagado').AsFloat;
                DeudaRefinanciacionVencida := FieldByName('DeudaVencida').AsFloat;
            end;

            TotalDeudasConvenios := QueryGetValueInt(DMConnections.BaseCAC, Format(SQLDeudaConvenios, [CodigoPersona]));

            lblMontoDeudasConvenios.Caption := FormatFloat(FORMATO_IMPORTE, TotalDeudasConvenios);
            if DeudaComprobantesVencida > 0 then begin
                lblMontoDeudasConvenios.Font.Color := clRed;
                TPanelMensajesForm.OcultaPanel;
                ShowMsgBoxCN('Validaci�n Comprobantes Vencidos', 'El Cliente tiene Comprobantes pendientes de Pago Vencidos, en este u en otro(s) Convenios(s).', MB_ICONWARNING, Self);
            end;

            lblMontoDeudasRefinanciacion.Caption := FormatFloat(FORMATO_IMPORTE, TotalDeudasRefinanciacion);
            if DeudaRefinanciacionVencida > 0 then begin
                lblMontoDeudasRefinanciacion.Font.Color := clRed;
                TPanelMensajesForm.OcultaPanel;
                ShowMsgBoxCN('Validaci�n Refinanciaci�n', 'El Cliente tiene Cuotas Vencidas por Refinanciaci�n.', MB_ICONWARNING, Self);
            end;
        except
            on e:Exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
        Screen.Cursor := crDefault;
    end;
end;

procedure TformCobroComprobantes.btnCobrarCuotaClick(Sender: TObject);
    resourcestring
        RS_ERROR_EJECUCION_TITULO  = 'Error de Ejecuci�n';
        RS_ERROR_EJECUCION_MENSAJE = 'Error en la ejecuci�n del procedimiento %s. C�digo de Error: %d.';
        RS_MENSAJE_ERROR_CAMBIO_ESTADO = 'Se Produjo un Error al actualizar el Estado de la Cuota.';

        RS_TITULO_ACCION     = 'Pagar Cuota';
        RS_MENSAJE_ACCION    = '� Confirma el pago de la(s) Cuota(s) seleccionada(s) ?';
        RS_MENSAJE_A_CUENTA  = '� Confirma el Pago A Cuenta por un importe de %s de la Cuota seleccionada ?';
        RS_ERROR_ACCION      = 'Se Produjo un Error al efectuar el Registro del Pago de ls Cuota.';
        RS_MENSAJE_RESULTADO = 'Se Pag� la Cuota Correctamente.';
        RS_TITULO_RECIBO     = 'Impresi�n Recibo';
        RS_MENSAJE_RECIBO    = '� Se imprimi� correctamente el Recibo de la Cuota Cancelada ?';
    var
        r: TRefinanciacionReporteReciboForm;
    	RespuestaImpresion: TRespuestaImprimir;        
        NumeroRecibo: Integer;
        FPersona: TDatosPersonales;
        Mensaje: String;
        ProcesoOK: Boolean;
        PagoAceptado: TModalResult;
begin
    try
        CambiarEstadoCursor(CURSOR_RELOJ, Self, FComponentes);

        FPersona := ObtenerDatosPersonales(DMConnections.BaseCAC, EmptyStr, EmptyStr, FCodigoCliente);

        RefinanciacionEntrarEditarCuotasForm := TRefinanciacionEntrarEditarCuotasForm.Create(Self);

        with RefinanciacionEntrarEditarCuotasForm do begin
            if Inicializar(TIPO_ENTRADA_DATOS_PAGO_CUOTA, cdsCuotasAPagarCodigoTipoMedioPago.AsInteger, FPersona, True, FAhora, False, cdsCuotasAPagar) then begin
                if FCantidadCuotasCobrar > 1 then begin
                    lblAcaMonto.Enabled := False;
                end;

                PagoAceptado := ShowModal;

                if PagoAceptado = mrOk then begin
                    if FEntradaDatos.ImportePagado < FEntradaDatos.DatosCuota.Saldo then
                        Mensaje := Format(RS_MENSAJE_A_CUENTA ,[FormatFloat(FORMATO_IMPORTE, FEntradaDatos.ImportePagado)])
                    else
                        Mensaje := RS_MENSAJE_ACCION;

                    if ShowMsgBoxCN(RS_TITULO_ACCION, Mensaje, MB_ICONQUESTION, Self) = mrOk then try

                        CambiarEstadoCursor(CURSOR_RELOJ, Self, FComponentes);

                        QueryExecute(DMConnections.BaseCAC, 'BEGIN TRANSACTION ActualizarPagoCuota');

                        NumeroRecibo := 0;

                        while (not cdsCuotasAPagar.Eof) and (cdsCuotasAPagarCobrar.AsBoolean) do begin

                            with spActualizarRefinanciacionCuotaPago do begin
                                if Active then Close;
                                Parameters.Refresh;

                                with Parameters do begin
                                    ParamByName('@CodigoRefinanciacion').Value   := cdsCuotasAPagarCodigoRefinanciacion.AsInteger;
                                    ParamByName('@CodigoCuota').Value            := cdsCuotasAPagarCodigoCuota.AsInteger;
                                    //ParamByName('@Importe').Value                := iif(FCantidadCuotasCobrar > 1, cdsCuotasAPagarSaldo.AsInteger, FEntradaDatos.ImportePagado);			//SS_1200_MCA_20140704
                                    ParamByName('@Importe').Value                := iif(FCantidadCuotasCobrar > 1, cdsCuotasAPagarSaldo.AsLargeInt, FEntradaDatos.ImportePagado);			//SS_1200_MCA_20140704
                                    ParamByName('@Anticipado').Value             := iif(FEntradaDatos.ImportePagado < FEntradaDatos.DatosCuota.Saldo, 1, 0);
                                    ParamByName('@CodigoFormaPago').Value        := FEntradaDatos.CodigoFormaPago;
                                    ParamByName('@TitularNombre').Value          := iif(FEntradaDatos.TitularNombre = EmptyStr, Null, FEntradaDatos.TitularNombre);
                                    ParamByName('@TitularTipoDocumento').Value   := iif(FEntradaDatos.TitularTipoDocumento = EmptyStr, Null, FEntradaDatos.TitularTipoDocumento);
                                    ParamByName('@TitularNumeroDocumento').Value := iif(FEntradaDatos.TitularNumeroDocumento = EmptyStr, Null, FEntradaDatos.TitularNumeroDocumento);
                                    ParamByName('@DocumentoNumero').Value        := iif(FEntradaDatos.DocumentoNumero = EmptyStr, Null, FEntradaDatos.DocumentoNumero);
                                    ParamByName('@CodigoBanco').Value            := iif(FEntradaDatos.CodigoBanco = 0, Null, FEntradaDatos.CodigoBanco);
                                    ParamByName('@CodigoTarjeta').Value          := iif(FEntradaDatos.CodigoTarjeta = 0, Null, FEntradaDatos.CodigoTarjeta);
                                    ParamByName('@TarjetaCuotas').Value          := iif(FEntradaDatos.TarjetaCuotas = 0, Null, FEntradaDatos.TarjetaCuotas);
                                    ParamByName('@Usuario').Value                := UsuarioSistema;
                                    ParamByName('@NumeroTurno').Value            := GNumeroTurno;
                                    ParamByName('@FechaPago').Value              := FEntradaDatos.FechaPago;
                                    ParamByName('@NumeroRecibo').Value           := iif(NumeroRecibo = 0, Null, NumeroRecibo);
                                end;

                                ExecProc;
                            end;

                            if spActualizarRefinanciacionCuotaPago.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
                                raise EErrorExceptionCN.Create(
                                    RS_ERROR_EJECUCION_TITULO,
                                    Format(RS_ERROR_EJECUCION_MENSAJE,
                                           [spActualizarRefinanciacionCuotaPago.ProcedureName,
                                            spActualizarRefinanciacionCuotaPago.Parameters.ParamByName('@RETURN_VALUE').Value]));
                            end
                            else begin
                                if NumeroRecibo = 0 then begin
                                    NumeroRecibo := spActualizarRefinanciacionCuotaPago.Parameters.ParamByName('@NumeroRecibo').Value;
                                end;

                                if FEntradaDatos.ImportePagado = FEntradaDatos.DatosCuota.Saldo then begin
                                    with spActualizarRefinanciacionCuotasEstados do begin
                                        if Active then Close;
                                        Parameters.Refresh;

                                        Parameters.ParamByName('@CodigoRefinanciacion').Value := cdsCuotasAPagarCodigoRefinanciacion.AsInteger;
                                        Parameters.ParamByName('@CodigoCuota').Value          := cdsCuotasAPagarCodigoCuota.AsInteger;
                                        Parameters.ParamByName('@CodigoEstadoCuota').Value    := REFINANCIACION_ESTADO_CUOTA_PAGADA;
                                        Parameters.ParamByName('@Usuario').Value              := UsuarioSistema;
                                        ExecProc;

                                        if Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
                                            raise EErrorExceptionCN.Create(RS_TITULO_ACCION, RS_MENSAJE_ERROR_CAMBIO_ESTADO);
                                        end;
                                    end;
                                end;
                            end;

                            cdsCuotasAPagar.Next;
                        end;


                        QueryExecute(DMConnections.BaseCAC, 'IF @@TRANCOUNT > 0 BEGIN COMMIT TRANSACTION ActualizarPagoCuota END');
                        Application.ProcessMessages;

//                        LevantarLogo(FLogo);

                        r := TRefinanciacionReporteReciboForm.Create(Nil);
                        if r.Inicializar(FLogo, NumeroRecibo, True, False) then begin		// SS_989_PDO_20120123
                            while  MsgBox(RS_MENSAJE_RECIBO, RS_TITULO_RECIBO, MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = mrNo do begin
                                if not r.Inicializar(FLogo, NumeroRecibo, True, False) then Break;		// SS_989_PDO_20120123
                            end;
                        end;

                    except
                        on e:Exception do begin
                            QueryExecute(DMConnections.BaseCAC, 'IF @@TRANCOUNT > 0 BEGIN ROLLBACK TRANSACTION ActualizarPagoCuota END');
                            ShowMsgBoxCN(e, Self);
                        end;
                    end;
                end;
            end;
        end;
    finally

// INICIO : TASK_003_JMA_20160405
        if PagoAceptado <> mrOk then
        begin

            try

                sp_ActualizarClienteMoroso.Parameters.ParamByName('@NumeroDocumento').Value := paNullable;
                sp_ActualizarClienteMoroso.Parameters.ParamByName('@CodigoRefinanciacion').Value := cdsCuotasAPagarCodigoRefinanciacion.AsInteger;
                sp_ActualizarClienteMoroso.Parameters.ParamByName('@Usuario').Value  := UsuarioSistema;
                sp_ActualizarClienteMoroso.Parameters.ParamByName('@ErrorDescription').Value  := paNullable;
                sp_ActualizarClienteMoroso.ExecProc;

            except
                on e: Exception do begin
                    MsgBoxErr('ERROR Revisando morosidad', e.Message, 'ERROR Revisando morosidad', MB_ICONERROR);
                end;
            end;
        end;
// TERMINO


        if Assigned(RefinanciacionEntrarEditarCuotasForm) then FreeAndNil(RefinanciacionEntrarEditarCuotasForm);
        if Assigned(r) then FreeAndNil(r);

        CambiarEstadoCursor(CURSOR_DEFECTO, Self, FComponentes);

        if PagoAceptado = mrOk then begin
            LimpiarDatosCliente;
            LimpiarCampos;
        end;
    end
end;

procedure TformCobroComprobantes.VerificaComprobantesMarcados;
    var
        Puntero: TBookmark;
begin
    try
        CambiarEstadoCursor(CURSOR_RELOJ, Self, FComponentes);

        Puntero := cds_Comprobantes.GetBookmark;
        cds_Comprobantes.DisableControls;
        ds_Comprobantes.DataSet := Nil;

        case cds_Comprobantes.FieldByName('Cobrar').AsBoolean of
            // Marcar todas las Anteriores
            True: begin
                cds_Comprobantes.Prior;

                while not cds_Comprobantes.Bof do begin
                    if (not cds_Comprobantes.FieldByName('Cobrar').AsBoolean) and (not cds_Comprobantes.FieldByName('EstadoRefinanciacion').AsInteger in [REFINANCIACION_ESTADO_EN_TRAMITE, REFINANCIACION_ESTADO_ACEPTADA]) then begin //SS-1051_PDO
                        cds_Comprobantes.Edit;
                        cds_Comprobantes.FieldByName('Cobrar').AsBoolean := True;
                        cds_Comprobantes.Post;

                        Inc(FCantidadComprobantesCobrar);
                        FTotalComprobantesCobrar := FTotalComprobantesCobrar + cds_Comprobantes.FieldByName('SaldoPendiente').AsFloat;
                        lblTotalSelec.Caption:=FormatFloat(FORMATO_IMPORTE, FTotalComprobantesCobrar);
                    end;

                    cds_Comprobantes.Prior;
                end;
            end;
            // Desmarcar todas las Posteriores
            {
            False: begin
                cds_Comprobantes.Next;

                while not cds_Comprobantes.Eof do begin
                    if cds_Comprobantes.FieldByName('Cobrar').AsBoolean then begin
                        cds_Comprobantes.Edit;
                        cds_Comprobantes.FieldByName('Cobrar').AsBoolean := False;
                        cds_Comprobantes.Post;

                        Dec(FCantidadComprobantesCobrar);
                        FTotalComprobantesCobrar := FTotalComprobantesCobrar - cds_Comprobantes.FieldByName('SaldoPendiente').AsFloat;
                        if FTotalComprobantesCobrar < 0 then begin
                            lblTotalSelec.Caption:=FormatFloat(FORMATO_IMPORTE, 0);
                            FTotalComprobantesCobrar :=0;
                        end else begin
                            lblTotalSelec.Caption:=FormatFloat(FORMATO_IMPORTE, FTotalComprobantesCobrar);
                        end;
                    end;

                    cds_Comprobantes.Next;
                end;
            end;
            }
        end;


    finally
        if Assigned(Puntero) then begin
            if cds_Comprobantes.BookmarkValid(Puntero) then begin
                cds_Comprobantes.GotoBookmark(Puntero);
            end;
            cds_Comprobantes.FreeBookmark(Puntero);
        end;

        ds_Comprobantes.DataSet := cds_Comprobantes;
        cds_Comprobantes.EnableControls;

        CambiarEstadoCursor(CURSOR_DEFECTO, Self, FComponentes);
    end;
end;

procedure TformCobroComprobantes.VerificarCuotasACobrar;
    var
        Puntero: TBookmark;
begin
    try
        CambiarEstadoCursor(CURSOR_RELOJ, Self, FComponentes);

        Puntero := cdsCuotasAPagar.GetBookmark;
        cdsCuotasAPagar.DisableControls;
        dsCuotasAPagar.DataSet := Nil;

        case cdsCuotasAPagarCobrar.AsBoolean of
            // Marcar todas las Anteriores
            True: begin
                cdsCuotasAPagar.Prior;

                while not cdsCuotasAPagar.Bof do begin
                    if (not cdsCuotasAPagarCobrar.AsBoolean) then begin
                        cdsCuotasAPagar.Edit;
                        cdsCuotasAPagarCobrar.AsBoolean := True;
                        cdsCuotasAPagar.Post;

                        Inc(FCantidadCuotasCobrar);
                        FTotalCuotasCobrar := FTotalCuotasCobrar + cdsCuotasAPagarSaldo.AsLargeInt;
                    end;

                    cdsCuotasAPagar.Prior;
                end;
            end;
            // Desmarcar todas las Posteriores
            False: begin
                cdsCuotasAPagar.Next;

                while not cdsCuotasAPagar.Eof do begin
                    if cdsCuotasAPagarCobrar.AsBoolean then begin
                        cdsCuotasAPagar.Edit;
                        cdsCuotasAPagarCobrar.AsBoolean := False;
                        cdsCuotasAPagar.Post;

                        Dec(FCantidadCuotasCobrar);
                        FTotalCuotasCobrar := FTotalCuotasCobrar - cdsCuotasAPagarSaldo.AsLargeInt;
                    end;

                    cdsCuotasAPagar.Next;
                end;
            end;
        end;

    finally
        if Assigned(Puntero) then begin
            if cdsCuotasAPagar.BookmarkValid(Puntero) then begin
                cdsCuotasAPagar.GotoBookmark(Puntero);
            end;
            cdsCuotasAPagar.FreeBookmark(Puntero);
        end;

        dsCuotasAPagar.DataSet := cdsCuotasAPagar;
        cdsCuotasAPagar.EnableControls;

        lblCuotasCantidad.Caption := IntToStr(FCantidadCuotasCobrar);
        lblCuotasTotal.Caption    := FormatFloat(FORMATO_IMPORTE, FTotalCuotasCobrar);
        ActualizaListaObjetos(FComponentes, TObject(btnCobrarCuota), FbtnCobrarCuotaPermisos and (FCantidadCuotasCobrar > 0));

        CambiarEstadoCursor(CURSOR_DEFECTO, Self, FComponentes);
    end;
end;

procedure TformCobroComprobantes.CargarComboRefinanciaciones(CodigoPersona: Integer);
    resourcestring
        RS_TODAS = '<Todas>';
        RS_REFINANCIACION = '%s - (%s)';
begin
    try
        try
            vcbRefinanciacion.Clear;
            vcbRefinanciacion.Items.Add('< Todas >', 0);

            with spObtenerRefinanciacionCuotasAPagarRefinanciaciones do begin
                if Active then Close;

                Parameters.Refresh;
                Parameters.ParamByName('@CodigoPersona').Value := iif(CodigoPersona = 0, NUll, CodigoPersona);
                Parameters.ParamByName('@RUTPersona').Value    := NULL;
                Open;

                while not Eof do begin
                    vcbRefinanciacion.Items.Add(
                        Format(
                            RS_REFINANCIACION,
                            [FormatFloat('###,##0', FieldByName('CodigoRefinanciacion').AsFloat), Trim(FieldByName('NumeroConvenio').AsString)]),
                        FieldByName('CodigoRefinanciacion').AsInteger);

                    Next;
                end;
            end;

            vcbRefinanciacion.ItemIndex := 0;
            vcbRefinanciacion.Enabled   := FvcbRefinanciacionesPermisos;
        except
            on e: Exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
        if spObtenerRefinanciacionCuotasAPagarRefinanciaciones.Active then spObtenerRefinanciacionCuotasAPagarRefinanciaciones.Close;
    end;
end;

procedure TformCobroComprobantes.MarcarDesmarcarComprobantes(Marcar: Boolean);
    var
        ValorActualCobrar: Boolean;
        Puntero: TBookmark;
begin
    try
        CambiarEstadoCursor(CURSOR_RELOJ, Self);
        Puntero := cds_Comprobantes.GetBookmark;
        ds_Comprobantes.DataSet := Nil;

        cds_Comprobantes.First;

        while not cds_Comprobantes.Eof do begin
            if not (cds_Comprobantes.FieldByName('EstadoRefinanciacion').AsInteger in [REFINANCIACION_ESTADO_EN_TRAMITE, REFINANCIACION_ESTADO_ACEPTADA]) then begin
                with cds_Comprobantes do begin
                    ValorActualCobrar := FieldByName('Cobrar').AsBoolean;

                    Edit;
                    FieldByName('Cobrar').AsBoolean := Marcar;

                    if ValorActualCobrar <> FieldByName('Cobrar').AsBoolean  then begin
                        if FieldByName('Cobrar').AsBoolean then begin
                            Inc(FCantidadComprobantesCobrar);
                            FTotalComprobantesCobrar := FTotalComprobantesCobrar + FieldByName('SaldoPendiente').AsFloat;
                            lblTotalSelec.Caption:=FormatFloat(FORMATO_IMPORTE, FTotalComprobantesCobrar);
                        end else begin
                            Dec(FCantidadComprobantesCobrar);
                            FTotalComprobantesCobrar := FTotalComprobantesCobrar - FieldByName('SaldoPendiente').AsFloat;
                            if FTotalComprobantesCobrar < 0 then begin
                                lblTotalSelec.Caption:=FormatFloat(FORMATO_IMPORTE, 0);
                                FTotalComprobantesCobrar :=0;
                            end else begin
                                lblTotalSelec.Caption:=FormatFloat(FORMATO_IMPORTE, FTotalComprobantesCobrar);
                            end;
                        end;
                    end;

                    lblTotalAPagar.Caption := FormatFloat(FORMATO_IMPORTE , FSaldoConvenios - FTotalComprobantesCobrar);
                    // Revision 9
                    Post;
                end;
            end;

            cds_Comprobantes.Next;
        end;
    finally
        if cds_Comprobantes.BookmarkValid(Puntero) then begin
            cds_Comprobantes.GotoBookmark(Puntero);
        end;
        cds_Comprobantes.FreeBookmark(Puntero);
        ds_Comprobantes.DataSet := cds_Comprobantes;

        btnCobrar.Enabled := (FCantidadComprobantesCobrar > 0);
        CambiarEstadoCursor(CURSOR_DEFECTO, Self);
    end;
end;

{
    Function Name: ImprimirDetalleInfraccionesAnuladas
    Parameters : TipoComprobante: string; NroComprobante: int64
    Return Value: Boolean
    Author : pdominguez
    Date Created : 05/06/2010

    Description : Imprime el detalle de Infracciones Anuladas.
}
function TformCobroComprobantes.ImprimirDetalleInfraccionesAnuladas(TipoComprobante: string; NroComprobante: int64): Boolean;
resourcestring
    MSG_ERROR_COBRAR	= 'No se actualiz� el campo "ReImpresi�n" para el comprobante';

var
    f : TRptDetInfraccionesAnuladas;
    DescError : string;
begin
    Result := False;

    Application.CreateForm(TRptDetInfraccionesAnuladas,f);
    try
        if f.Inicializar(DMConnections.BaseCAC, TipoComprobante, NroComprobante, DescError, True) then begin  //REV.9

			//spCobrar_o_DescontarReimpresion.Close;                                    // SS_1246_CQU_20151006
			//with spCobrar_o_DescontarReimpresion do begin                             // SS_1246_CQU_20151006
  			//	Parameters.Refresh;                                                     // SS_1246_CQU_20151006
  			//	Parameters.ParamByName('@TipoComprobante').Value := TipoComprobante;    // SS_1246_CQU_20151006
  			//	Parameters.ParamByName('@NumeroComprobante').Value := NroComprobante;   // SS_1246_CQU_20151006
  			//	Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;       // SS_1246_CQU_20151006
  			//	ExecProc;                                                               // SS_1246_CQU_20151006
  			//	if Parameters.ParamByName('@RETURN_VALUE').Value < 0 then begin         // SS_1246_CQU_20151006
      		//		MsgBox(MSG_ERROR_COBRAR, Caption, MB_ICONEXCLAMATION);              // SS_1246_CQU_20151006
  			//	end;                                                                    // SS_1246_CQU_20151006

			//end;                                                                      // SS_1246_CQU_20151006

			Result := True;
        end
        else MsgBox( DescError, Caption, MB_ICONERROR );

	finally
        f.Release;
    end;
end;

{*******************************************************************************
    Procedure Name  : VerificarConvenioListaAmarilla
    Author          : CQuezadaI
    Date            : 30/04/2013
    Firma           : SS_660_CQU_20121010
    Description     : Se varifica si el convenio est� en Lista Amarilla.
*******************************************************************************}
//function TformCobroComprobantes.VerificarConvenioListaAmarilla(CodigoConvenio : Integer) : boolean;      //SS_660_MVI_20130729
function TformCobroComprobantes.VerificarConvenioListaAmarilla(CodigoConvenio : Integer) : string;      //SS_660_MVI_20130729
resourcestring
    //SQL_CONSULTACONVENIO        = 'SELECT dbo.EstaConvenioEnListaAmarilla(%d,''%s'')';        // SS_660_CQU_20130711
    //SQL_CONSULTACONVENIO        = 'SELECT dbo.EstaConvenioEnListaAmarilla(%d,''%s'', NULL)';    // SS_660_CQU_20130711
      SQL_CONSULTACONVENIO        = 'SELECT dbo.ObtenerEstadoConvenioEnListaAmarilla(%d)';    //SS_660_MVI_20130729
//var                                                                                         //SS_660_MVI_20130729
//    sFecha      : string;                                                                   //SS_660_MVI_20130729
begin
//    Result      :=  'NULL';                                                                 //SS_660_MVI_20130909  //SS_660_MVI_20130729
      Result      :=  '';                                                                     //SS_660_MVI_20130909
    if CodigoConvenio > 0 then begin
        try
            CambiarEstadoCursor(CURSOR_RELOJ, Self, FComponentes);
            try
                //sFecha      :=  FormatDateTime('yyyymmdd hh:nn:ss', SysutilsCN.NowBaseCN(DMConnections.BaseCAC));     //SS_660_MVI_20130729
                //Result      :=  SysutilsCN.QueryGetBooleanValue(                                                      //SS_660_MVI_20130729
                //                        DMConnections.BaseCAC,                                                        //SS_660_MVI_20130729
                //                        Format(SQL_CONSULTACONVENIO, [CodigoConvenio, sFecha])                        //SS_660_MVI_20130729
                //            );                                                                                        //SS_660_MVI_20130729
                Result:= QueryGetValue(   DMConnections.BaseCAC,                                                        //SS_660_MVI_20130729
                                           Format(SQL_CONSULTACONVENIO,                                                 //SS_660_MVI_20130729
                                                         [   CodigoConvenio                                             //SS_660_MVI_20130729
                                                         ]                                                              //SS_660_MVI_20130729
                                                 )                                                                      //SS_660_MVI_20130729
                                      );                                                                                //SS_660_MVI_20130729

            except
                on e: Exception do begin
                    ShowMsgBoxCN(e, Self);
                end;
            end;  
        finally
            CambiarEstadoCursor(CURSOR_DEFECTO, Self, FComponentes);
        end;
    end;
end;

{*******************************************************************************
    Procedure Name  : VerificaPersonaListaAmarilla
    Author          : CQuezadaI
    Date            : 23/07/2013
    Firma           : SS_660_CQU_20130711
    Description     : Se varifica si est� en Lista Amarilla por CodigoCliente
*******************************************************************************}
//function TformCobroComprobantes.VerificaPersonaListaAmarilla(iCodigoCliente : Integer) : Boolean;  //SS_1236_MCA_20141226
function TformCobroComprobantes.VerificaPersonaListaAmarilla(RutCliente : string) : Boolean;        //SS_1236_MCA_20141226
resourcestring
    MSG_ALERTA ='Validaci�n Cliente';                                                                 //SS_660_MVI_20130909
    MSG_ALERTA_CLIENTE_LISTA_AMARILLA='EL Cliente posee al menos un convenio en lista amarilla';      //SS_660_MVI_20130909

    //SQL_CONSULTAPERSONA = 'SELECT dbo.EstaPersonaEnListaAmarilla(%d)';								//SS_1236_MCA_20141226
   // SQL_CONSULTAPERSONA = 'SELECT TOP 1 dbo.ObtenerEstadoConvenioEnListaAmarilla(Convenio.CodigoConvenio)'                                                //SS_660_MVI_20130909  //SS_660_MVI_20130729
   //                                 + 'FROM Convenio WITH(NOLOCK) WHERE dbo.ObtenerEstadoConvenioEnListaAmarilla(Convenio.CodigoConvenio) '               //SS_660_MVI_20130909  //SS_660_MVI_20130729
   //                                 + '<> ''NULL''  and CodigoCliente = %d  order by Convenio.CodigoConvenio DESC';                                       //SS_660_MVI_20130909  //SS_660_MVI_20130729
//var
//    sFecha      : string;                                                                                             //SS_660_MVI_20130729

    SQL_ESTACONVENIOENLISTAAMARILLA = 'SELECT dbo.EstaPersonaEnListaAmarilla(dbo.ObtenerCodigoPersonaRUT(''%s''))';    	//SS_1236_MCA_20141226
    SQL_ESTAPERSONAENPROCESOLISTAAMARILLA = 'SELECT dbo.EstaPersonaEnProcesoListaAmarilla(dbo.ObtenerCodigoPersonaRUT(''%s''))';   	//SS_1236_MCA_20141226
    MSG_INHABILITADO_LISTA_AMARILLA = 'El Cliente posee al menos un convenio en lista amarilla';   			            //SS_1236_MCA_20141226
    MSG_PROCESO_LISTA_AMARILLA = 'El RUT a lo menos tiene un convenio en proceso de Lista Amarilla';					//SS_1236_MCA_20141226
Var																														//SS_1236_MCA_20141226
	EsListaAmarilla: Boolean;                                                   					                    //SS_1236_MCA_20141226
    EsProcesoListaAmarilla: string;																						//SS_1236_MCA_20141226
begin                                                                                                                   //SS_1236_MCA_20141226
    Result      :=  False;                                                                                              //SS_660_MVI_20130909     //SS_660_MVI_20130729
    //if iCodigoCliente > 0 then begin																					//SS_1236_MCA_20141226
	if RutCliente <> EmptyStr then begin																				//SS_1236_MCA_20141226
        try
            CambiarEstadoCursor(CURSOR_RELOJ, Self, FComponentes);
            try
                //sFecha      :=  FormatDateTime('yyyymmdd hh:nn:ss', SysutilsCN.NowBaseCN(DMConnections.BaseCAC));    //SS_660_MVI_20130729
                //Result      :=  (SysutilsCN.QueryGetIntegerValue(                                                    //SS_660_MVI_20130729
                //                        DMConnections.BaseCAC,                                                       //SS_660_MVI_20130729
                //                        Format(SQL_CONSULTAPERSONA, [iCodigoCliente])                                //SS_660_MVI_20130729
                //            ) = 1);                                                                                  //SS_660_MVI_20130729

                //Result:= QueryGetIntegerValue(   DMConnections.BaseCAC,                                              //SS_1236_MCA_20141226  //SS_660_MVI_20130909  //SS_660_MVI_20130729
                //                           Format(SQL_CONSULTAPERSONA,                                               //SS_1236_MCA_20141226  //SS_660_MVI_20130729
                //                                         [   iCodigoCliente                                          //SS_1236_MCA_20141226   //SS_660_MVI_20130729
                //                                         ]                                                           //SS_1236_MCA_20141226   //SS_660_MVI_20130729
                //                                 )                                                                   //SS_1236_MCA_20141226   //SS_660_MVI_20130729
                //                      ) = 1;                                                                         //SS_1236_MCA_20141226   //SS_660_MVI_20130909 //SS_660_MVI_20130729
                //if Result then ShowMsgBoxCN(MSG_ALERTA, MSG_ALERTA_CLIENTE_LISTA_AMARILLA, MB_ICONWARNING, Self);    //SS_1236_MCA_20141226   //SS_660_MVI_20130909

                EsListaAmarilla := StrToBool(QueryGetValue(DMConnections.BaseCAC,           	//SS_1236_MCA_20141226
        								Format(SQL_ESTACONVENIOENLISTAAMARILLA,     	        //SS_1236_MCA_20141226
                                        [RutCliente])));           	                            //SS_1236_MCA_20141226
                EsProcesoListaAmarilla := QueryGetValue(DMConnections.BaseCAC,           		//SS_1236_MCA_20141226
                                                Format(SQL_ESTAPERSONAENPROCESOLISTAAMARILLA,   //SS_1236_MCA_20141226
                                                [RutCliente]));           		                //SS_1236_MCA_20141226
                if EsListaAmarilla then                                                         //SS_1236_MCA_20141226
                begin        	                                                            	//SS_1236_MCA_20141226
                    ShowMsgBoxCN(self.Caption, MSG_INHABILITADO_LISTA_AMARILLA, MB_ICONWARNING, self);  	//SS_1236_MCA_20141226
                    exit;                                                                   	//SS_1236_MCA_20141226
                end;                                                                        	//SS_1236_MCA_20141226
                if EsProcesoListaAmarilla <> EmptyStr then                                      //SS_1236_MCA_20141226
                begin                                                                           //SS_1236_MCA_20141226
                    ShowMsgBoxCN(self.Caption, MSG_PROCESO_LISTA_AMARILLA,  MB_ICONWARNING, self);  	//SS_1236_MCA_20141226
                end;																			//SS_1236_MCA_20141226




            except
                on e: Exception do begin
                    ShowMsgBoxCN(e, Self);
                end;
            end;  
        finally
            CambiarEstadoCursor(CURSOR_DEFECTO, Self, FComponentes);
        end;
    end;
end;
//BEGIN:SS_1120_MVI_20130820 -------------------------------------------------
{-----------------------------------------------------------------------------
  Procedure: TformCobroComprobantes.cbConveniosDrawItem
  Date:      20-August-2013
  Description: Verifica si existe alg�n convenio de baja para llamar a la funci�n
                que colorea de rojo el item.
-----------------------------------------------------------------------------}
procedure TformCobroComprobantes.cbConveniosDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  if Pos( TXT_CONVENIO_DE_BAJA, cbConvenios.Items[Index].Caption ) > 0 then  begin

     ItemComboBoxColor(Control, Index, Rect, State, clred, true);

  end
  else begin

     ItemComboBoxColor(Control, Index, Rect, State);

  end;                                                                                    

end;
//END:SS_1120_MVI_20130820 -------------------------------------------------

{-----------------------------------------------------------------------------
  Function Name: InicializarParaRefinanciaciones
  Author:  aunanue
  Date: 04-04-2017
  Firma: TASK_005_AUN_20170403-CU.COBO.COB.301
  Description: Inicializa el formulario con el RUT pasado como par�metro para que
               se muestren las refinanciaciones asociadas al mismo.
-----------------------------------------------------------------------------}
function TformCobroComprobantes.InicializarParaRefinanciaciones(RUT: AnsiString; NumeroPOS,
  PuntoEntrega, PuntoVenta: Integer): Boolean;
begin
    Result := Inicializar(NumeroPOS, PuntoEntrega, PuntoVenta);
    if Result then begin
        peNumeroDocumento.OnChange := nil;
        try
            peNumeroDocumento.Text := RUT;
            btnFiltrar.OnClick(btnFiltrar);
            pcOpcionesComprobantes.ActivePage := tbsRefinanciaciones;
        finally
            peNumeroDocumento.OnChange := peNumeroDocumentoChange;
        end;
    end;
end;
end.
