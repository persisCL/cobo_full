{-----------------------------------------------------------------------------
 Unit Name: frmHistoricoCobranzaExterna
 Autor:    	cfuentesc
 Fecha:     07/04/2017
 Prop�sito: Visualizar todas las acciones de cobranza externas
 			incluyendo env�o a cobranza externa
 			CU.COBO.COB.201
 Firma: 	TASK_130_CFU_20170405-Gestionar_Carpeta_Deudores - Parte 2
 Historia:
-----------------------------------------------------------------------------}
unit frmHistoricoCobranzaExterna;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, VariantComboBox, ExtCtrls, ListBoxEx, DBListEx, Validate,
  DateEdit, DmiCtrls, BuscaClientes, DB, ADODB;

type
  TFormHistoricoCobranzaExterna = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Label7: TLabel;
    vcbEmpresasCobranza: TVariantComboBox;
    Label1: TLabel;
    vcbJudicial: TVariantComboBox;
    peNumeroDocumento: TPickEdit;
    Label13: TLabel;
    dedtFechaInicio: TDateEdit;
    dedtFechaFin: TDateEdit;
    Label2: TLabel;
    Label3: TLabel;
    btnLimpiarCobExtraJudicial: TButton;
    btnBuscarCobExtraJudicial: TButton;
    dlMorososCobExtraJudicial: TDBListEx;
    btnSalir: TButton;
    spObtenerHistoricoCobranzaExterna: TADOStoredProc;
    dsObtenerHistoricoCobranzaExterna: TDataSource;
    procedure btnSalirClick(Sender: TObject);
    procedure peNumeroDocumentoButtonClick(Sender: TObject);
    procedure btnBuscarCobExtraJudicialClick(Sender: TObject);
    procedure btnLimpiarCobExtraJudicialClick(Sender: TObject);
  private
    { Private declarations }
  	FUltimaBusqueda: TBusquedaCliente;
	FListaEmpresasCobranza: TVariantItems;
  public
    { Public declarations }
	function Inicializar(): Boolean;
  end;

var
  FormHistoricoCobranzaExterna: TFormHistoricoCobranzaExterna;

implementation

uses
	frmMuestraMensaje, DB_CRUDCommonProcs, DMConnection, Util, 
    UtilProc, SysUtilsCN;

{$R *.dfm}

function TFormHistoricoCobranzaExterna.Inicializar(): Boolean;
begin
	Result := False;
    try
    	CambiarEstadoCursor(CURSOR_RELOJ);
        TPanelMensajesForm.MuestraMensaje('Obteniendo Empresas de Cobranza ...', True);
        try
            FListaEmpresasCobranza := TVariantItems.Create(nil);
            DB_CRUDCommonProcs.FillVariantItemsFromStoredProc(DMConnections.BaseCAC, 'EmpresasDeCobranza_Listar',
                                                              FListaEmpresasCobranza, 'CodigoEmpresaCobranza', 'Alias', [], [],
                                                              Self.Caption, True);
            vcbEmpresasCobranza.Items.Assign(FListaEmpresasCobranza);
        except
			on E:Exception do begin
            	MsgBox(E.Message, Self.Caption, MB_ICONSTOP);
                Exit;
            end;
        end;
    finally
    	TPanelMensajesForm.OcultaPanel;
        CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
    Result := True;
end;

procedure TFormHistoricoCobranzaExterna.peNumeroDocumentoButtonClick(
  Sender: TObject);
var
    f: TFormBuscaClientes;
begin
    Application.CreateForm(TFormBuscaClientes, F);
  	if F.Inicializa(FUltimaBusqueda) then begin
    	F.ShowModal;
        if F.ModalResult = idOk then begin
            FUltimaBusqueda := F.UltimaBusqueda;
            peNumeroDocumento.Text 	:= f.Persona.NumeroDocumento;
	    end;
  	end;
    F.free;
end;

procedure TFormHistoricoCobranzaExterna.btnBuscarCobExtraJudicialClick(
  Sender: TObject);
begin
	try
        with spObtenerHistoricoCobranzaExterna, Parameters do begin
            if Active then
                Close;
            Parameters.Refresh;
            ParamByName('@EmpresaCobranza').Value	:= iif(vcbEmpresasCobranza.ItemIndex > 0, vcbEmpresasCobranza.Value, null);
            ParamByName('@Judicial').Value			:= iif(vcbJudicial.ItemIndex > 0, vcbJudicial.Value, null);
            ParamByName('@NumeroDocumento').Value	:= iif(peNumeroDocumento.Value > 0, peNumeroDocumento.Value, null);
            ParamByName('@FechaInicio').Value		:= iif(dedtFechaInicio.Date > 0, dedtFechaInicio.Date, null);
            ParamByName('@FechaFin').Value			:= iif(dedtFechaFin.Date > 0, dedtFechaFin.Date, null);
            try
            	Open;
            except
				on E:Exception do begin
                	MsgBox(E.Message, Self.Caption, MB_ICONSTOP);
                    Exit;
                end;
            end;
        end;
    finally

    end;
end;

procedure TFormHistoricoCobranzaExterna.btnLimpiarCobExtraJudicialClick(
  Sender: TObject);
begin
	spObtenerHistoricoCobranzaExterna.Close;
    vcbEmpresasCobranza.ItemIndex	:= -1;
    vcbJudicial.ItemIndex			:= -1;
    peNumeroDocumento.Clear;
    dedtFechaInicio.Clear;
    dedtFechaFin.Clear;
end;

procedure TFormHistoricoCobranzaExterna.btnSalirClick(Sender: TObject);
begin
	Close;
end;

end.
