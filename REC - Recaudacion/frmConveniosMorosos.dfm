object ConveniosMorosos: TConveniosMorosos
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  ClientHeight = 480
  ClientWidth = 492
  Color = clBtnFace
  Constraints.MinHeight = 508
  Constraints.MinWidth = 498
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  DesignSize = (
    492
    480)
  PixelsPerInch = 96
  TextHeight = 13
  object grpDatosConvenio: TGroupBox
    Left = 0
    Top = 32
    Width = 491
    Height = 57
    Anchors = [akLeft, akTop, akRight]
    Caption = 'Datos Convenio'
    TabOrder = 0
    DesignSize = (
      491
      57)
    object Label1: TLabel
      Left = 215
      Top = 28
      Width = 85
      Height = 13
      Caption = 'C'#243'digo Convenio:'
    end
    object Label6: TLabel
      Left = 16
      Top = 28
      Width = 24
      Height = 13
      Caption = 'RUT:'
    end
    object cmbConvenios: TVariantComboBox
      Left = 309
      Top = 20
      Width = 170
      Height = 21
      Style = vcsDropDownList
      Anchors = [akLeft, akTop, akRight]
      ItemHeight = 13
      TabOrder = 1
      OnChange = cmbConveniosChange
      Items = <>
    end
    object peNumeroDocumento: TPickEdit
      Left = 51
      Top = 20
      Width = 134
      Height = 21
      CharCase = ecUpperCase
      Enabled = True
      TabOrder = 0
      OnChange = peNumeroDocumentoChange
      OnKeyPress = peNumeroDocumentoKeyPress
      EditorStyle = bteTextEdit
      OnButtonClick = peNumeroDocumentoButtonClick
    end
  end
  object pnlDatos: TPanel
    Left = 0
    Top = 352
    Width = 491
    Height = 89
    Anchors = [akLeft, akTop, akRight]
    BevelInner = bvLowered
    Enabled = False
    TabOrder = 1
    object Label4: TLabel
      Left = 17
      Top = 24
      Width = 72
      Height = 13
      Caption = 'Moroso Desde:'
    end
    object Label2: TLabel
      Left = 19
      Top = 56
      Width = 70
      Height = 13
      Caption = 'Moroso Hasta:'
    end
    object dtDesde: TDateEdit
      Left = 128
      Top = 16
      Width = 89
      Height = 21
      AutoSelect = False
      Enabled = False
      TabOrder = 0
      OnChange = dtDesdeChange
      Date = -693594.000000000000000000
    end
    object dtHasta: TDateEdit
      Left = 127
      Top = 48
      Width = 90
      Height = 21
      AutoSelect = False
      Enabled = False
      TabOrder = 1
      Date = -693594.000000000000000000
    end
  end
  object pnlBotones: TPanel
    Left = 1
    Top = 440
    Width = 490
    Height = 41
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 2
    object Notebook1: TNotebook
      Left = 283
      Top = 1
      Width = 206
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'pgSalida'
        ExplicitWidth = 0
        ExplicitHeight = 0
        object btnSalir: TButton
          Left = 110
          Top = 9
          Width = 92
          Height = 25
          Caption = '&Salir'
          TabOrder = 0
          OnClick = btnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        Caption = 'pgOkCan'
        object btnCancelar: TButton
          Left = 15
          Top = 9
          Width = 92
          Height = 25
          Caption = '&Cancelar'
          Enabled = False
          TabOrder = 1
          OnClick = btnCancelarClick
        end
        object btnAceptar: TButton
          Left = 110
          Top = 9
          Width = 92
          Height = 25
          Caption = '&Aceptar'
          Enabled = False
          TabOrder = 0
          OnClick = btnAceptarClick
        end
      end
    end
  end
  object AbmToolbar: TAbmToolbar
    Left = 0
    Top = 0
    Width = 492
    Height = 33
    Habilitados = [btSalir]
    OnClose = AbmToolbarClose
  end
  object AbmList1: TAbmList
    Left = 0
    Top = 88
    Width = 490
    Height = 265
    Anchors = [akLeft, akTop, akRight]
    TabStop = True
    TabOrder = 4
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'177'#0'Fecha Desde                                   '
      #0'68'#0'Fecha Hasta')
    RefreshTime = 100
    Table = spActualizarFechasConvenioMoroso
    Style = lbOwnerDrawFixed
    OnClick = AbmList1Click
    OnDrawItem = AbmList1DrawItem
    OnInsert = AbmList1Insert
    OnDelete = AbmList1Delete
    OnEdit = AbmList1Edit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar
  end
  object DataSource1: TDataSource
    Left = 400
    Top = 120
  end
  object spActualizarFechasConvenioMoroso: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ActualizarFechasConvenioMoroso;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodConvenio'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@FecDesdeClave'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FecDesde'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FecHasta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Accion'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 432
    Top = 122
    object DateTimeField1: TDateTimeField
      FieldName = 'FechaDesde'
    end
    object DateTimeField2: TDateTimeField
      FieldName = 'FechaHasta'
      DisplayFormat = 'dd/mm/yyyy'
    end
  end
  object spObtenerCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerCliente;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end>
    Left = 432
    Top = 160
  end
end
