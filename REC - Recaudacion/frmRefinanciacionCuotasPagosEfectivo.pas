unit frmRefinanciacionCuotasPagosEfectivo;

interface

uses
  // Developer
  DMConnection, SysUtilsCN, PeaProcsCN, PeaTypes, PeaProcs, frmRefinanciacionReporteRecibo,
  UtilProc,
  // Auto
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, ListBoxEx, DBListEx, StdCtrls, ExtCtrls, ImgList;

type
  TRefinanciacionCuotasPagosEfectivoForm = class(TForm)
    pnlAcciones: TPanel;
    btnCancelar: TButton;
    GroupBox1: TGroupBox;
    DBListEx1: TDBListEx;
    spObtenerRefinanciacionCuotaPagos: TADOStoredProc;
    dsObtenerRefinanciacionCuotaPagos: TDataSource;
    lnCheck: TImageList;
    btnImprimir: TButton;
    procedure btnCancelarClick(Sender: TObject);
    procedure DBListEx1DrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
    procedure btnImprimirClick(Sender: TObject);
    procedure DBListEx1DrawBackground(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;    // SS_989_PDO_20120119
      var DefaultDraw: Boolean);                                                                                                // SS_989_PDO_20120119
    procedure spObtenerRefinanciacionCuotaPagosAfterScroll(DataSet: TDataSet);                                                  // SS_989_PDO_20120119
  private
    { Private declarations }
  public
    { Public declarations }
    function Inicializar(CodigoRefinanciacion, CodigoCuota: Integer): Boolean;    
  end;

var
  RefinanciacionCuotasPagosEfectivoForm: TRefinanciacionCuotasPagosEfectivoForm;

implementation

{$R *.dfm}

procedure TRefinanciacionCuotasPagosEfectivoForm.btnCancelarClick(Sender: TObject);
begin
    Close;
end;

procedure TRefinanciacionCuotasPagosEfectivoForm.btnImprimirClick(Sender: TObject);
    var
        FLogo: TBitmap;
        r: TRefinanciacionReporteReciboForm;
        NumeroRecibo: Integer;
begin
    try
        try
            Screen.Cursor := crHourGlass;
            Application.ProcessMessages;

            FLogo := TBitMap.Create;

//            LevantarLogo(FLogo);

            with spObtenerRefinanciacionCuotaPagos do begin
                NumeroRecibo         := FieldByName('NumeroRecibo').AsInteger;
            end;

            r := TRefinanciacionReporteReciboForm.Create(Nil);
            r.Inicializar(FLogo, NumeroRecibo, True, True);		// SS_989_PDO_20120119
        except
            on e: Exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
        if Assigned(r) then FreeAndNil(r);
        if Assigned(FLogo) then FreeAndNil(FLogo);
        
        Screen.Cursor := crDefault;
    end;
end;

// Inicio Bloque: SS_989_PDO_20120119
procedure TRefinanciacionCuotasPagosEfectivoForm.DBListEx1DrawBackground(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState; var DefaultDraw: Boolean);
begin
    with Sender do begin
        if not spObtenerRefinanciacionCuotaPagos.FieldByName('NumeroReciboAnulacion').IsNull then begin
            if (odSelected in State) then Canvas.Brush.Color := clMaroon;
        end;
    end;
end;
// Fin Bloque: SS_989_PDO_20120119

procedure TRefinanciacionCuotasPagosEfectivoForm.DBListEx1DrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
    var
        bmp: TBitMap;
begin
    if not spObtenerRefinanciacionCuotaPagos.FieldByName('NumeroReciboAnulacion').IsNull then begin       // SS_989_PDO_20120119
        if Not (odSelected in State) then begin                                                           // SS_989_PDO_20120119
            Sender.Canvas.Font.Color := clRed;                                                            // SS_989_PDO_20120119
        end;                                                                                              // SS_989_PDO_20120119
    end;                                                                                                  // SS_989_PDO_20120119

    with Sender do begin
        if (Column.FieldName = 'Anticipado') then try
            Text := '';
            Canvas.FillRect(Rect);
            bmp := TBitMap.Create;
			DefaultDraw := False;

            if (spObtenerRefinanciacionCuotaPagos.FieldByName(Column.FieldName).AsBoolean ) then
                lnCheck.GetBitmap(0, Bmp)
            else
                lnCheck.GetBitmap(1, Bmp);

            Canvas.Draw((Rect.Left + Rect.Right - Bmp.Width) div 2, Rect.Top, bmp);
        finally
            bmp.Free;
        end; // finally
    end; // with

    if (Column.FieldName = 'Importe') then begin
        Text := FormatFloat(FORMATO_IMPORTE, StrToFloat(Text)) + ' ';
    end;

    if (Column.FieldName = 'FechaPago') then begin
        Text := FormatDateTime('dd"-"mm"-"yyyy', spObtenerRefinanciacionCuotaPagos.FieldByName(Column.FieldName).AsDateTime);			 // SS_989_PDO_20120119
    end;

    if (Column.FieldName = 'FechaCreacion') then begin                                                                                   // SS_989_PDO_20120119
        Text := FormatDateTime('dd"-"mm"-"yyyy hh:nn:ss', spObtenerRefinanciacionCuotaPagos.FieldByName(Column.FieldName).AsDateTime);   // SS_989_PDO_20120119
    end;                                                                                                                                 // SS_989_PDO_20120119


    if (Column.FieldName = 'NumeroRecibo') or (Column.FieldName = 'Turno') or (Column.FieldName = 'NumeroReciboAnulacion') then begin    // SS_989_PDO_20120119
        if Text <> EmptyStr then begin                                                                                                   // SS_989_PDO_20120119
            Text := FormatFloat('#,###0', StrToFloat(Text)) + ' ';                                                                       // SS_989_PDO_20120119
        end;                                                                                                                             // SS_989_PDO_20120119
    end;
end;

function TRefinanciacionCuotasPagosEfectivoForm.Inicializar(CodigoRefinanciacion: Integer; CodigoCuota: Integer): Boolean;
begin
    try
        Result := False;
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;
        try
            with spObtenerRefinanciacionCuotaPagos do begin
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoRefinanciacion').Value := CodigoRefinanciacion;
                Parameters.ParamByName('@CodigoCuota').Value          := CodigoCuota;
                Open;
            end;

            btnImprimir.Enabled := ExisteAcceso('imprimir_Hist_Pagos_ref') and (spObtenerRefinanciacionCuotaPagos.RecordCount > 0);

            Result := True;
        Except
            on e:Exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
        Screen.Cursor := crDefault;
    end;
end;

// Inicio Bloque: SS_989_PDO_20120119
procedure TRefinanciacionCuotasPagosEfectivoForm.spObtenerRefinanciacionCuotaPagosAfterScroll(DataSet: TDataSet);
begin
    btnImprimir.Enabled := ExisteAcceso('imprimir_Hist_Pagos_ref') and (spObtenerRefinanciacionCuotaPagos.FieldByName('Importe').AsInteger > 0);
end;
// Fin Bloque: SS_989_PDO_20120119

end.
