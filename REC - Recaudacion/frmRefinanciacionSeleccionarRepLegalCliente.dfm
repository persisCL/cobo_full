object RefinanciacionSeleccionarRepresentanteCliForm: TRefinanciacionSeleccionarRepresentanteCliForm
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = ' Seleccionar / Entrar Representante Legal  '
  ClientHeight = 313
  ClientWidth = 697
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 681
    Height = 259
    Align = alCustom
    Caption = '  Seleccione un Representante Legal  '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    object DBListEx1: TDBListEx
      Left = 7
      Top = 20
      Width = 665
      Height = 141
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 100
          Header.Caption = 'Rut'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          IsLink = False
          FieldName = 'Rut'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'Nombre'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          IsLink = False
          FieldName = 'Nombre'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 175
          Header.Caption = 'Apellido'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          IsLink = False
          FieldName = 'Apellido'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'ApellidoMaterno'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          IsLink = False
          FieldName = 'ApellidoMaterno'
        end>
      DataSource = dsRepresentantes
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
      OnDblClick = DBListEx1DblClick
    end
    object GroupBox2: TGroupBox
      Left = 7
      Top = 173
      Width = 665
      Height = 76
      Caption = '  Entrar Representante Legal  '
      TabOrder = 1
      object Label3: TLabel
        Left = 18
        Top = 17
        Width = 21
        Height = 13
        Caption = 'Rut:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label1: TLabel
        Left = 168
        Top = 17
        Width = 41
        Height = 13
        Caption = 'Nombre:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label2: TLabel
        Left = 299
        Top = 17
        Width = 41
        Height = 13
        Caption = 'Apellido:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label4: TLabel
        Left = 477
        Top = 17
        Width = 84
        Height = 13
        Caption = 'Apellido Materno:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object peRut: TPickEdit
        Left = 17
        Top = 36
        Width = 143
        Height = 21
        Hint = ' Seleccionar el Cliente a Refinanciar '
        Enabled = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnChange = peRutChange
        OnKeyPress = peRutKeyPress
        EditorStyle = bteTextEdit
        OnButtonClick = peRutButtonClick
      end
      object edtNombre: TEdit
        Left = 166
        Top = 36
        Width = 130
        Height = 21
        CharCase = ecUpperCase
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        MaxLength = 60
        ParentFont = False
        TabOrder = 1
      end
      object edtApellido: TEdit
        Left = 298
        Top = 36
        Width = 173
        Height = 21
        CharCase = ecUpperCase
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        MaxLength = 60
        ParentFont = False
        TabOrder = 2
      end
      object edtApellidoMaterno: TEdit
        Left = 473
        Top = 36
        Width = 173
        Height = 21
        CharCase = ecUpperCase
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        MaxLength = 30
        ParentFont = False
        TabOrder = 3
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 273
    Width = 697
    Height = 40
    Align = alBottom
    Color = 14408667
    ParentBackground = False
    TabOrder = 1
    DesignSize = (
      697
      40)
    object btnCancelar: TButton
      Left = 614
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Cancelar'
      TabOrder = 0
      OnClick = btnCancelarClick
    end
    object btnAceptar: TButton
      Left = 535
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Aceptar'
      TabOrder = 1
      OnClick = btnAceptarClick
    end
  end
  object cdsRepresentantes: TClientDataSet
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Rut'
        DataType = ftString
        Size = 11
      end
      item
        Name = 'Nombre'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'Apellido'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'ApellidoMaterno'
        DataType = ftString
        Size = 30
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 496
    Top = 72
    Data = {
      8E0000009619E0BD0100000018000000040000000000030000008E0003527574
      0100490000000100055749445448020002000B00064E6F6D6272650100490000
      000100055749445448020002003C00084170656C6C69646F0100490000000100
      0557494454480200020014000F4170656C6C69646F4D617465726E6F01004900
      00000100055749445448020002001E000000}
  end
  object dsRepresentantes: TDataSource
    DataSet = cdsRepresentantes
    Left = 480
    Top = 96
  end
end
