{ -----------------------------------------------------------------------------
Firma       :   SS_1120_MVI_20130820
Descripcion :   Se cambia estilo del comboBox a: vcsOwnerDrawFixed,
                se crear el evento DrawItem y se llama al procedimeinto ItemComboBoxColor.

Firma       :   SS_660_MVI_20130909
Descripcion :   Se agrega un Label que indica si el convenio seleccionado est� en lista amarilla, indicando el estado.
                Adem�s se crea un mensaje de alerta, que indica si el cliente posee aunque sea un convenio en lista amarilla.

Firma       : SS_1176_NDR_20140326
Descripcion : Reversar Refinanciacion

Firma       : SS_1408_MCA_20151027
Descripcion : muestra un mensaje si el cliente se encuentre en registrado en Tabla ClientesAContactar

Firma       : TASK_005_AUN_20170403-CU.COBO.COB.301
Descripcion : Implementaci�n de modificaciones del UC para Scada. Se ocultan botones
              y se agregan nuevos en base a UC.
 -----------------------------------------------------------------------------}
unit frmRefinanciacionConsultas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, VariantComboBox, DmiCtrls, ComCtrls, ListBoxEx, DBListEx,

  // Uses Manuales por el Programador
  frmRefinanciacionGestion, BuscaClientes, PeaProcs, DMConnection, PeaTypes, PeaProcsCN,
  frmMuestraMensaje, frmRefinanciacionGestionCuotas, CobranzasResources, SysutilsCN,
  frmRefinanciacionImpresion, frmProtestarRefinanciacion, frmConveniosEnCarpetaLegal,
  frmRefinanciacionesEnTramiteAceptadas, frmBloqueosSistema, frmRefinanciacionGestionRecibos,
  DB, Provider, DBClient, ADODB, Util, UtilDB, ImgList, Contnrs, Menus, Clipbrd;

type
  TRefinanciacionConsultasForm = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    StatusBar1: TStatusBar;
    lb_CodigoCliente: TLabel;
    peRut: TPickEdit;
    Label2: TLabel;
    vcbConvenios: TVariantComboBox;
    Label3: TLabel;
    nedtRefinanciacion: TNumericEdit;
    btnFiltrar: TButton;
    vcbTipoRefinanciacion: TVariantComboBox;
    Label1: TLabel;
    Panel3: TPanel;
    Button5: TButton;
    spObtenerRefinanciacionesConsultas: TADOStoredProc;
    cdsRefinanciaciones: TClientDataSet;
    dsRefinanciaciones: TDataSource;
    dspObtenerRefinanciacionesConsultas: TDataSetProvider;
    cdsRefinanciacionesCodigoRefinanciacion: TIntegerField;
    cdsRefinanciacionesTipo: TStringField;
    cdsRefinanciacionesFecha: TDateTimeField;
    cdsRefinanciacionesEstado: TStringField;
    cdsRefinanciacionesNombrePersona: TStringField;
    cdsRefinanciacionesConvenio: TStringField;
    cdsRefinanciacionesTotalDeuda: TStringField;
    cdsRefinanciacionesTotalPagado: TStringField;
    cdsRefinanciacionesNumeroDocumento: TStringField;
    lblNombreCli: TLabel;
    lblPersoneria: TLabel;
    cdsRefinanciacionesValidada: TBooleanField;
    cdsRefinanciacionesCodigoEstado: TSmallintField;
    Panel4: TPanel;
    GroupBox2: TGroupBox;
    Panel2: TPanel;
    btnNueva: TButton;
    btnEditar: TButton;
    btnCuotas: TButton;
    DBListEx1: TDBListEx;
    btnAnular: TButton;
    PopupMenu1: TPopupMenu;
    CopiarRUTalPortapapeles1: TMenuItem;
    CopiarNrodeConvenioalPortapapeles1: TMenuItem;
    lnCheck: TImageList;
    btnATramite: TButton;
    spActualizarRefinanciacionEstado: TADOStoredProc;
    btnProtestar: TButton;
    cdsRefinanciacionesProtestada: TBooleanField;
    spObtenerRefinanciacionDatosCabecera: TADOStoredProc;
    btnRecibos: TButton;                                // SS_989_PDO_20120119
    cdsRefinanciacionesCodigoPersona: TIntegerField;    // SS_989_PDO_20120119
    cdsRefinanciacionesCodigoRefinanciacionUnificada: TIntegerField;				//SS_1051_PDO
    spActualizarRefinanciacionUnificadaEstado: TADOStoredProc;    					//SS_1051_PDO
    lblEnListaAmarilla: TLabel;														//SS_660_MVI_20130909
    btnReversar: TButton;															//SS_1176_NDR_20140326
    spReversarRefinanciacion: TADOStoredProc;
    btnPagar: TButton;    									//SS_1176_NDR_20140326
    procedure btnNuevaClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure peRutButtonClick(Sender: TObject);
    procedure vcbConveniosChange(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure peRutKeyPress(Sender: TObject; var Key: Char);
    procedure btnFiltrarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DBListEx1DblClick(Sender: TObject);
    procedure peRutChange(Sender: TObject);
    procedure DBListEx1DrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
    procedure btnCuotasClick(Sender: TObject);
    procedure cdsRefinanciacionesAfterScroll(DataSet: TDataSet);
    procedure DBListEx1Columns0HeaderClick(Sender: TObject);
    procedure vcbConveniosKeyPress(Sender: TObject; var Key: Char);
    procedure vcbTipoRefinanciacionKeyPress(Sender: TObject; var Key: Char);
    procedure nedtRefinanciacionKeyPress(Sender: TObject; var Key: Char);
    procedure CopiarRUTalPortapapeles1Click(Sender: TObject);
    procedure CopiarNrodeConvenioalPortapapeles1Click(Sender: TObject);
    procedure btnATramiteClick(Sender: TObject);
    procedure btnAnularClick(Sender: TObject);
    procedure btnProtestarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnRecibosClick(Sender: TObject);    // SS_989_PDO_20120119
    procedure vcbConveniosDrawItem(Control: TWinControl; Index: Integer;      //SS_1120_MVI_20130820
      Rect: TRect; State: TOwnerDrawState);                                   //SS_1120_MVI_20130820
    procedure btnReversarClick(Sender: TObject);
    procedure btnPagarClick(Sender: TObject);                              //SS_1176_NDR_20140326
  private
    { Private declarations }
      FUltimaBusqueda: TBusquedaCliente;
      FCodigoCliente,
      FCodigoConvenio: Integer;
      FbtnNuevaPermiso,
      FbtnEditarPermiso,
      FbtnPagarPermiso, //TASK_005_AUN_20170403-CU.COBO.COB.301
      FExisteAccesoCobroComprobante,
      FbtnAnularPermiso,
      FbtnATramitePermiso,
      FbtnCuotasPermiso,
      FbtnRecibosPermiso,   // SS_989_PDO_20120119
      FbtnReversarPermiso,  													//SS_1176_NDR_20140326
      FbtnProtestarPermiso: Boolean;
      procedure MostrarDatosCliente(CodigoCliente: Integer; Personeria: string);
      procedure ActualizarBotones;
      procedure CambiarEstadoRefinanciacion(NuevoEstado: Byte);
      procedure VerificarClienteCarpetaLegal;
      procedure VerificarRefinanciacionesNoActivadas;
      procedure ReversarRefinanciacion;
    procedure ExistePermisoCobroComprobante;
  public
    { Public declarations }
    FComponentes: TObjectList;
    function Inicializar: Boolean;
    procedure RefrescarDatos;
    function ValidarCambioEstado: Boolean;
  end;

var
  RefinanciacionConsultasForm: TRefinanciacionConsultasForm;

const
    SQLObtenerNombrePersona = 'SELECT dbo.ObtenerNombrePersona(%d)';
    MSG_TITULO_VALIDAR_ESTADO  = 'Validaci�n Estado';
    MSG_MENSAJE_VALIDAR_ESTADO = 'El Estado de la Refinanciaci�n ha variado. Es necesario Refrescar los datos.';


implementation

uses frmCobroComprobantes, Main;

{$R *.dfm}

function TRefinanciacionConsultasForm.Inicializar: Boolean;
var
    TiposRefinanciaciones: TVariantComboBox;
    Contador, Aux: Integer;
begin
    Result := False;

    try
        CambiarEstadoCursor(CURSOR_RELOJ);

        try
            //TASK_005_AUN_20170403-CU.COBO.COB.301: Inicio
            Aux := btnAnular.Left;
            btnAnular.Left := btnATramite.Left;
            btnPagar.Left := Aux;
            btnProtestar.Visible := False;
            btnATramite.Visible  := False;
            btnReversar.Visible  := False;
            //TASK_005_AUN_20170403-CU.COBO.COB.301: Fin
            CargarConstantesCobranzasRefinanciacion;
            CargarConstantesCobranzasCanalesFormasPago;													//SS_1051_PDO
            DBListExResaltaCamposIndex(DBListEx1);

            TiposRefinanciaciones := TVariantComboBox.Create(Self);
            TiposRefinanciaciones.Parent := Self;
            CargarCanalesDePago(DMConnections.BaseCAC, TiposRefinanciaciones, 0, 1);
            vcbTipoRefinanciacion.Clear;
            vcbTipoRefinanciacion.Items.Add('Todas', 0);

            for Contador := 0 to TiposRefinanciaciones.Items.Count - 1 do begin
                vcbTipoRefinanciacion.Items.Add(
                    TiposRefinanciaciones.Items[Contador].Caption,
                    TiposRefinanciaciones.Items[Contador].Value);
            end;
            vcbTipoRefinanciacion.ItemIndex := 0;//TASK_005_AUN_20170403-CU.COBO.COB.301

            lblNombreCli.Caption  := EmptyStr;
            lblPersoneria.Caption := EmptyStr;

            FbtnNuevaPermiso     := ExisteAcceso('nueva_mant_ref');
            FbtnEditarPermiso    := ExisteAcceso('editar_mant_ref');
            FbtnAnularPermiso    := ExisteAcceso('anular_mant_ref');
            FbtnProtestarPermiso := ExisteAcceso('protestar_mant_ref');
            FbtnATramitePermiso  := ExisteAcceso('a_tramite_mant_ref');
            FbtnCuotasPermiso    := ExisteAcceso('gest_cuotas_mant_ref');
            FbtnRecibosPermiso   := ExisteAcceso('gest_recibos_mant_ref');   // SS_989_PDO_20120119
            FbtnReversarPermiso  := ExisteAcceso('Reversar_Refinanciacion');   	//SS_1176_NDR_20140326

            //TASK_005_AUN_20170403-CU.COBO.COB.301: Inicio
            FbtnPagarPermiso     := ExisteAcceso('pagar_mant_ref');
            ExistePermisoCobroComprobante;
            //TASK_005_AUN_20170403-CU.COBO.COB.301: Fin

            btnNueva.Enabled     := FbtnNuevaPermiso;
            btnEditar.Enabled    := False;
            btnATramite.Enabled  := False;
            btnAnular.Enabled    := False;
            btnProtestar.Enabled := False;
            btnCuotas.Enabled    := False;
            btnRecibos.Enabled   := False;  // SS_989_PDO_20120119
            btnReversar.Enabled  := False;  //SS_1176_NDR_20140326
            btnPagar.Enabled     := False;  //TASK_005_AUN_20170403-CU.COBO.COB.301
            Result := True;
        except
            on e: Exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
        if Assigned(TiposRefinanciaciones) then FreeAndNil(TiposRefinanciaciones);

        TPanelMensajesForm.OcultaPanel;
        CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ExistePermisoCobroComprobante
  Author:  aunanue
  Date: 04-04-2017
  Firma: TASK_005_AUN_20170403-CU.COBO.COB.301
  Description: valida si este usuario tiene permisos para cobrar comprobantes
-----------------------------------------------------------------------------}
procedure TRefinanciacionConsultasForm.ExistePermisoCobroComprobante;
begin
    FExisteAccesoCobroComprobante := ExisteAcceso('mnuCobroComprobante');
end;

procedure TRefinanciacionConsultasForm.btnFiltrarClick(Sender: TObject);
var
    Intro: Char;
begin
    try
        CambiarEstadoCursor(CURSOR_RELOJ, Self, FComponentes);

        if cdsRefinanciaciones.Active then cdsRefinanciaciones.Close;

        if (Trim(peRut.Text) <> EmptyStr) and (FCodigoCliente = 0) then begin
            Intro := #13;
            peRutKeyPress(Nil, Intro);
        end
        else                                                                                             //SS_1408_MCA_20151027
            EstaClienteConMensajeEspecial(DMConnections.BaseCAC, PADL(peRut.Text, 9, '0'));              //SS_1408_MCA_20151027


        with spObtenerRefinanciacionesConsultas do begin
            Parameters.Refresh;
            with Parameters do begin
                ParamByName('@CodigoRefinanciacion').Value := iif(nedtRefinanciacion.ValueInt = 0, Null, nedtRefinanciacion.ValueInt);
                ParamByName('@CodigoPersona').Value        := iif(FCodigoCliente = 0, NUll, FCodigoCliente);
                ParamByName('@RUTPersona').Value           := iif(Trim(peRut.Text) = EmptyStr, NUll, Trim(peRut.Text));
                ParamByName('@CodigoConvenio').Value       := iif(FCodigoConvenio = 0, Null, FCodigoConvenio);
                ParamByName('@CodigoTipoMedioPago').Value  := iif(vcbTipoRefinanciacion.Value = 0, Null, vcbTipoRefinanciacion.Value);
                ParamByName('@SoloNoActivadas').Value      := 0;
            end;
        end;

        cdsRefinanciaciones.Open;
    finally
        CambiarEstadoCursor(CURSOR_DEFECTO, Self, FComponentes);
        ActualizarBotones;
        //if cdsRefinanciaciones.RecordCount > 0 then ActiveControl := DBListEx1;   // SS_1051_CQU_20130618
        if cdsRefinanciaciones.Active and (cdsRefinanciaciones.RecordCount > 0)     // SS_1051_CQU_20130618
        then ActiveControl := DBListEx1;                                            // SS_1051_CQU_20130618
    end;
end;

procedure TRefinanciacionConsultasForm.btnATramiteClick(Sender: TObject);
resourcestring
    MSG_TITULO                   = 'Confirme Proceso de la Refinanciaci�n';
    MSG_A_TRAMITE_REFINANCIACION = '� Est� seguro de devolver la Refinanciaci�n a estado En Tr�mite ?';
var
    IDBloqueo: Integer;
    ResultadoBloqueo: Integer;
begin
    if ValidarCambioEstado then try
        CambiarEstadoCursor(CURSOR_RELOJ, Self, FComponentes);
        try
            TPanelMensajesForm.MuestraMensaje('Obteniendo Bloqueo ...', True);
            IDBloqueo := 0;
            ResultadoBloqueo :=
                ObtenerBloqueo
                    (
                        DMConnections.BaseCAC,
                        'RefEnviarATramite',
                        cdsRefinanciacionesCodigoRefinanciacion.AsString,
                        UsuarioSistema,
                        IDBloqueo
                    );

            if ResultadoBloqueo = 0 then begin
                TPanelMensajesForm.OcultaPanel;
                if ShowMsgBoxCN(MSG_TITULO, MSG_A_TRAMITE_REFINANCIACION, MB_ICONQUESTION, Self) = mrOk then begin
                    CambiarEstadoRefinanciacion(REFINANCIACION_ESTADO_EN_TRAMITE);
                end;
            end;
        except
            on e: Exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
        try
            if IDBloqueo <> 0 then begin
                TPanelMensajesForm.MuestraMensaje('Eliminando Bloqueo ...', True);
                EliminarBloqueo(DMConnections.BaseCAC, IDBloqueo);
            end;
        except
            on e: Exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;

        TPanelMensajesForm.OcultaPanel;
        CambiarEstadoCursor(CURSOR_DEFECTO, Self, FComponentes);
    end
    else begin
        ShowMsgBoxCN(MSG_TITULO_VALIDAR_ESTADO, MSG_MENSAJE_VALIDAR_ESTADO, MB_ICONWARNING, Self);
        RefrescarDatos;
    end;
end;

procedure TRefinanciacionConsultasForm.btnCuotasClick(Sender: TObject);
var
    Puntero: TBookmark;
    IDBloqueo: Integer;
    ResultadoBloqueo: Integer;
begin
    if ValidarCambioEstado then try
        CambiarEstadoCursor(CURSOR_RELOJ, Self, FComponentes);

        TPanelMensajesForm.MuestraMensaje('Obteniendo Bloqueo ...', True);
        IDBloqueo := 0;
        ResultadoBloqueo :=
            ObtenerBloqueo
                (
                    DMConnections.BaseCAC,
                    TRefinanciacionGestionCuotasForm.ClassName,
                    cdsRefinanciacionesCodigoRefinanciacion.AsString,
                    UsuarioSistema,
                    IDBloqueo
                );

        if ResultadoBloqueo = 0 then begin
            TPanelMensajesForm.MuestraMensaje('Cargando Refinanciaci�n Seleccionada', True);

            Puntero := cdsRefinanciaciones.GetBookmark;
            RefinanciacionGestionCuotasForm := TRefinanciacionGestionCuotasForm.Create(Self);

            with RefinanciacionGestionCuotasForm do begin
                if Inicializar(cdsRefinanciacionesCodigoRefinanciacion.AsInteger) then begin
                    ShowModal;
                    TPanelMensajesForm.MuestraMensaje('Actualizando Datos ...', True);
                    try
                        cdsRefinanciaciones.Close;
                        cdsRefinanciaciones.Open;
                    finally
                        if Assigned(Puntero) then begin
                            if cdsRefinanciaciones.BookmarkValid(Puntero) then cdsRefinanciaciones.GotoBookmark(Puntero);
                            cdsRefinanciaciones.FreeBookmark(Puntero);
                        end;
                    end;
                end;
            end;
        end;
    finally
        if Assigned(RefinanciacionGestionForm) then FreeAndNil(RefinanciacionGestionForm);

        try
            if IDBloqueo <> 0 then begin
                TPanelMensajesForm.MuestraMensaje('Eliminando Bloqueo ...');
                EliminarBloqueo(DMConnections.BaseCAC, IDBloqueo);
            end;
        except
            on e: Exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;

        TPanelMensajesForm.OcultaPanel;
        CambiarEstadoCursor(CURSOR_DEFECTO, Self, FComponentes);
    end
    else begin
        ShowMsgBoxCN(MSG_TITULO_VALIDAR_ESTADO, MSG_MENSAJE_VALIDAR_ESTADO, MB_ICONWARNING, Self);
        RefrescarDatos;
    end;
end;

procedure TRefinanciacionConsultasForm.btnNuevaClick(Sender: TObject);
var
//        Puntero: TBookmark;
    SeguirProceso: TModalResult;
    Intro: Char;
    CodigoRefinanciacionUnificada: Integer;     // SS_1051_PDO
begin
    try
        SeguirProceso := mrOk;

        if (FCodigoCliente > 0) or ((Trim(peRut.Text) <> EmptyStr) and (FCodigoCliente = 0)) then begin
            if (Trim(peRut.Text) <> EmptyStr) and (FCodigoCliente = 0) then begin
                Intro := #13;
                peRutKeyPress(Nil, Intro);
            end;

            RefinanciacionesEnTramiteAceptadasForm := TRefinanciacionesEnTramiteAceptadasForm.Create(Self);
            with RefinanciacionesEnTramiteAceptadasForm do begin
                Inicializar(peRut.Text, lblNombreCli.Caption);
                if spObtenerRefinanciacionesConsultas.RecordCount > 0 then begin
                    SeguirProceso := ShowModal;
                end;
            end;
        end;

        if SeguirProceso = mrOk then try
            CambiarEstadoCursor(CURSOR_RELOJ, Self, FComponentes);
            TPanelMensajesForm.MuestraMensaje('Cargando Refinanciaci�n Nueva', True);


            //BEGIN : SS_1051_NDR_20130418----------------------------------------
            if Assigned(RefinanciacionesEnTramiteAceptadasForm) then
            begin
              CodigoRefinanciacionUnificada := RefinanciacionesEnTramiteAceptadasForm.FCodigoRefinanciacionUnificada;     // SS_1051_PDO
            end
            else
            begin
              CodigoRefinanciacionUnificada := 0
            end;
            //END : SS_1051_NDR_20130418----------------------------------------

            RefinanciacionGestionForm := TRefinanciacionGestionForm.Create(Self);

            with RefinanciacionGestionForm do begin
                if Inicializar(0, CodigoRefinanciacionUnificada, vcbTipoRefinanciacion.Value, FCodigoCliente, FCodigoConvenio) then begin    // SS_1051_PDO
                    if ShowModal = mrOk then begin
                        try
                            Screen.Cursor := crHourGlass;
                            Application.ProcessMessages;

                            if cdsRefinanciaciones.Active then begin
    //                            Puntero := cdsRefinanciaciones.GetBookmark;
                                cdsRefinanciaciones.Close;
                                cdsRefinanciaciones.Open;
                            end
                            else begin
                                peRut.Text := EmptyStr;
                                vcbConvenios.Clear;
                                vcbTipoRefinanciacion.ItemIndex := vcbTipoRefinanciacion.Items.IndexOfValue(0);
                                nedtRefinanciacion.ValueInt := FCodigoRefinanciacion;
                                btnFiltrarClick(Nil);
                                lblEnListaAmarilla.Visible  := False;                                         //SS_660_MVI_20130909
                                lblEnListaAmarilla.Caption  := EmptyStr;                                      //SS_660_MVI_20130909
                            end;

                        finally
                        {
                            if Assigned(Puntero) then begin
                                if cdsRefinanciaciones.BookmarkValid(Puntero) then cdsRefinanciaciones.GotoBookmark(Puntero);
                                cdsRefinanciaciones.FreeBookmark(Puntero);
                            end;
                            }
                            Screen.Cursor := crDefault;
                        end;
                    end;
                end;
            end;
        finally
            if Assigned(RefinanciacionGestionForm) then FreeAndNil(RefinanciacionGestionForm);
            TPanelMensajesForm.OcultaPanel;
            CambiarEstadoCursor(CURSOR_DEFECTO, Self, FComponentes);
            ActualizarBotones;
        end;
    finally
        if Assigned(RefinanciacionesEnTramiteAceptadasForm) then FreeAndNil(RefinanciacionesEnTramiteAceptadasForm);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnPagarClick
  Author:  aunanue
  Date: 04-04-2017
  Firma: TASK_005_AUN_20170403-CU.COBO.COB.301
  Description: llama al form de pago de comprobantes con el RUT asociado a la refinanciaci�n seleccionada.
-----------------------------------------------------------------------------}
procedure TRefinanciacionConsultasForm.btnPagarClick(Sender: TObject);
var
    f: TformCobroComprobantes;
begin
    try
        f := TformCobroComprobantes.Create(Self);
        if f.InicializarParaRefinanciaciones(Trim(cdsRefinanciacionesNumeroDocumento.AsString), Main.MainForm.NumeroPOS, Main.MainForm.PuntoEntrega, Main.MainForm.PuntoVenta) then begin
            f.ShowModal;
        end;
    finally
        if Assigned(f) then FreeAndNil(f);
    end;

end;

procedure TRefinanciacionConsultasForm.btnProtestarClick(Sender: TObject);
var
    Puntero: TBookmark;
    IDBloqueo: Integer;
    ResultadoBloqueo: Integer;
begin
    if ValidarCambioEstado then try
        CambiarEstadoCursor(CURSOR_RELOJ, Self, FComponentes);

        TPanelMensajesForm.MuestraMensaje('Obteniendo Bloqueo ...', True);
        IDBloqueo := 0;
        ResultadoBloqueo :=
            ObtenerBloqueo
                (
                    DMConnections.BaseCAC,
                    TProtestarRefinanciacionForm.ClassName,
                    cdsRefinanciacionesCodigoRefinanciacion.AsString,
                    UsuarioSistema,
                    IDBloqueo
                );

        if ResultadoBloqueo = 0 then begin
            TPanelMensajesForm.MuestraMensaje('Cargando Refinanciaci�n Seleccionada ...');

            Puntero := cdsRefinanciaciones.GetBookmark;
            ProtestarRefinanciacionForm := TProtestarRefinanciacionForm.Create(Self);

            with ProtestarRefinanciacionForm do begin
                if Inicializar(cdsRefinanciacionesCodigoRefinanciacion.AsInteger) then begin
                    ShowModal;
                    TPanelMensajesForm.MuestraMensaje('Actualizando Datos ...', True);
                    try
                        cdsRefinanciaciones.Close;
                        cdsRefinanciaciones.Open;
                    finally
                        if Assigned(Puntero) then begin
                            if cdsRefinanciaciones.BookmarkValid(Puntero) then cdsRefinanciaciones.GotoBookmark(Puntero);
                            cdsRefinanciaciones.FreeBookmark(Puntero);
                        end;
                    end;
                end;
            end;
        end;
    finally
        if Assigned(ProtestarRefinanciacionForm) then FreeAndNil(ProtestarRefinanciacionForm);

        try
            if IDBloqueo <> 0 then begin
                TPanelMensajesForm.MuestraMensaje('Eliminando Bloqueo ...');
                EliminarBloqueo(DMConnections.BaseCAC, IDBloqueo);
            end;
        except
            on e: Exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;

        TPanelMensajesForm.OcultaPanel;
        CambiarEstadoCursor(CURSOR_DEFECTO, Self, FComponentes);
    end
    else begin
        ShowMsgBoxCN(MSG_TITULO_VALIDAR_ESTADO, MSG_MENSAJE_VALIDAR_ESTADO, MB_ICONWARNING, Self);
        RefrescarDatos;
    end;
end;

// Inicio Bloque: SS_989_PDO_20120119
procedure TRefinanciacionConsultasForm.btnRecibosClick(Sender: TObject);
var
    Puntero: TBookmark;
    frmGestionRecibos: TRefinanciacionGestionRecibosForm;
begin
    try
        CambiarEstadoCursor(CURSOR_RELOJ, Self, FComponentes);

        TPanelMensajesForm.MuestraMensaje('Cargando Recibos ...', True);
        Puntero := cdsRefinanciaciones.GetBookmark;


        frmGestionRecibos := TRefinanciacionGestionRecibosForm.Create(Self);

        if frmGestionRecibos.Inicializar(cdsRefinanciacionesCodigoPersona.AsInteger, cdsRefinanciacionesNumeroDocumento.AsString, cdsRefinanciacionesNombrePersona.AsString) then begin
            frmGestionRecibos.ShowModal;

            TPanelMensajesForm.MuestraMensaje('Actualizando Datos ...', True);
            try
                cdsRefinanciaciones.Close;
                cdsRefinanciaciones.Open;
            finally
                if Assigned(Puntero) then begin
                    if cdsRefinanciaciones.BookmarkValid(Puntero) then cdsRefinanciaciones.GotoBookmark(Puntero);
                    cdsRefinanciaciones.FreeBookmark(Puntero);
                end;
            end;
        end;
    finally
        if Assigned(frmGestionRecibos) then FreeAndNil(frmGestionRecibos);

        TPanelMensajesForm.OcultaPanel;
        CambiarEstadoCursor(CURSOR_DEFECTO, Self, FComponentes);
        ActualizarBotones;
    end;
end;


//BEGIN : SS_1176_NDR_20140326 --------------------------------------------------------------------------------------
procedure TRefinanciacionConsultasForm.btnReversarClick(Sender: TObject);
resourcestring
    MSG_TITULO                    = 'Confirme Proceso de la Refinanciaci�n';
    MSG_REVERSAR_REFINANCIACION   = '� Est� seguro de REVERSAR la Refinanciaci�n seleccionada ?';
var
    IDBloqueo: Integer;
    ResultadoBloqueo: Integer;
begin
    if ValidarCambioEstado then try
        CambiarEstadoCursor(CURSOR_RELOJ, Self, FComponentes);
        try
            TPanelMensajesForm.MuestraMensaje('Obteniendo Bloqueo ...', True);
            IDBloqueo := 0;
            ResultadoBloqueo :=
                ObtenerBloqueo
                    (
                        DMConnections.BaseCAC,
                        'RefReversar',
                        cdsRefinanciacionesCodigoRefinanciacion.AsString,
                        UsuarioSistema,
                        IDBloqueo
                    );

            if ResultadoBloqueo = 0 then begin
                TPanelMensajesForm.OcultaPanel;
                if ShowMsgBoxCN(MSG_TITULO, MSG_REVERSAR_REFINANCIACION, MB_ICONQUESTION, Self) = mrOk then
                begin
                    ReversarRefinanciacion;
                end;
            end;
        except
            on e: Exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
        try
            if IDBloqueo <> 0 then begin
                TPanelMensajesForm.MuestraMensaje('Eliminando Bloqueo ...', True);
                EliminarBloqueo(DMConnections.BaseCAC, IDBloqueo);
            end;
        except
            on e: Exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;

        TPanelMensajesForm.OcultaPanel;
        CambiarEstadoCursor(CURSOR_DEFECTO, Self, FComponentes);
    end
    else begin
        ShowMsgBoxCN(MSG_TITULO_VALIDAR_ESTADO, MSG_MENSAJE_VALIDAR_ESTADO, MB_ICONWARNING, Self);
        RefrescarDatos;
    end;
end;
//END : SS_1176_NDR_20140326 --------------------------------------------------------------------------------------

// Fin Bloque: SS_989_PDO_20120119

procedure TRefinanciacionConsultasForm.btnEditarClick(Sender: TObject);
var
    Puntero: TBookmark;
    IDBloqueo,
    ResultadoBloqueo: Integer;
    ModalResult: TModalResult;
begin
    if ValidarCambioEstado then try
        try
            CambiarEstadoCursor(CURSOR_RELOJ, Self, FComponentes);


            Puntero := cdsRefinanciaciones.GetBookmark;

            IDBloqueo := 0;

            if cdsRefinanciacionesCodigoEstado.AsInteger in [REFINANCIACION_ESTADO_EN_TRAMITE, REFINANCIACION_ESTADO_VIGENTE] then begin
                TPanelMensajesForm.MuestraMensaje('Obteniendo Bloqueo ...', True);
                ResultadoBloqueo :=
                    ObtenerBloqueo
                        (
                            DMConnections.BaseCAC,
                            TRefinanciacionGestionForm.ClassName,
                            cdsRefinanciacionesCodigoRefinanciacion.AsString,
                            UsuarioSistema,
                            IDBloqueo
                        );
            end
            else ResultadoBloqueo := 0;

            TPanelMensajesForm.MuestraMensaje('Cargando Refinanciaci�n Seleccionada ...');
            RefinanciacionGestionForm := TRefinanciacionGestionForm.Create(Application);

            with RefinanciacionGestionForm do begin

                if Inicializar(cdsRefinanciacionesCodigoRefinanciacion.AsInteger, cdsRefinanciacionesCodigoRefinanciacionUnificada.AsInteger, 0, 0, 0, (ResultadoBloqueo <> 0)) then begin  // SS_1051_PDO
                    ModalResult := ShowModal;
                    if ModalResult = mrOk then begin
                        TPanelMensajesForm.MuestraMensaje('Actualizando Datos ...', True);
                        try
                            cdsRefinanciaciones.Close;
                            cdsRefinanciaciones.Open;
                        finally
                            if Assigned(Puntero) then begin
                                if cdsRefinanciaciones.BookmarkValid(Puntero) then cdsRefinanciaciones.GotoBookmark(Puntero);
                                cdsRefinanciaciones.FreeBookmark(Puntero);
                            end;
                        end;
                    end;
                end;
            end;
        except
            on e: Exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
        if Assigned(RefinanciacionGestionForm) then FreeAndNil(RefinanciacionGestionForm);

        try
            if IDBloqueo > 0 then begin
                TPanelMensajesForm.MuestraMensaje('Desbloqueando Refinanciaci�n ...', (ModalResult <> mrOk));
                EliminarBloqueo(DMConnections.BaseCAC, IDBloqueo);
            end;
        except
            on e: Exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;

        TPanelMensajesForm.OcultaPanel;
        CambiarEstadoCursor(CURSOR_DEFECTO, Self, FComponentes);
        ActualizarBotones;
    end
    else begin
        ShowMsgBoxCN(MSG_TITULO_VALIDAR_ESTADO, MSG_MENSAJE_VALIDAR_ESTADO, MB_ICONWARNING, Self);
        RefrescarDatos;
    end;
end;

procedure TRefinanciacionConsultasForm.Button5Click(Sender: TObject);
begin
    Close;
end;

procedure TRefinanciacionConsultasForm.cdsRefinanciacionesAfterScroll(DataSet: TDataSet);
begin
    ActualizarBotones;
end;

procedure TRefinanciacionConsultasForm.CopiarNrodeConvenioalPortapapeles1Click(Sender: TObject);
begin
    if cdsRefinanciaciones.Active then begin
        if cdsRefinanciaciones.RecordCount > 0 then begin
            Clipboard.AsText := Trim(cdsRefinanciacionesConvenio.AsString);
        end;
    end;
end;

procedure TRefinanciacionConsultasForm.CopiarRUTalPortapapeles1Click(Sender: TObject);
begin
    if cdsRefinanciaciones.Active then begin
        if cdsRefinanciaciones.RecordCount > 0 then begin
            Clipboard.AsText := Trim(cdsRefinanciacionesNumeroDocumento.AsString);
        end;
    end;
end;

procedure TRefinanciacionConsultasForm.DBListEx1Columns0HeaderClick(Sender: TObject);
begin
    DBListExColumnHeaderClickGenerico(Sender);
end;

procedure TRefinanciacionConsultasForm.DBListEx1DblClick(Sender: TObject);
begin
    if btnEditar.Enabled and cdsRefinanciaciones.Active then begin
        if cdsRefinanciaciones.RecordCount > 0 then btnEditarClick(Nil);
    end;
end;

procedure TRefinanciacionConsultasForm.DBListEx1DrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
var
    bmp: TBitMap;
begin
    with Sender do begin
        if (Column.FieldName = 'Validada') then try
            Text := '';
            Canvas.FillRect(Rect);
            bmp := TBitMap.Create;
			DefaultDraw := False;

            if (cdsRefinanciaciones.FieldByName(Column.FieldName).AsBoolean ) then
                lnCheck.GetBitmap(0, Bmp)
            else
                lnCheck.GetBitmap(1, Bmp);

            Canvas.Draw((Rect.Left + Rect.Right - Bmp.Width) div 2, Rect.Top, bmp);
        finally
            bmp.Free;
        end; // finally
    end; // with
end;

procedure TRefinanciacionConsultasForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TRefinanciacionConsultasForm.FormCreate(Sender: TObject);
begin
    FCodigoCliente := 0;
    FCodigoConvenio := 0;
    FFormState := FFormState - [fsVisible];
end;

procedure TRefinanciacionConsultasForm.FormDestroy(Sender: TObject);
begin
    RefinanciacionConsultasForm := Nil;
end;

procedure TRefinanciacionConsultasForm.FormShow(Sender: TObject);
begin
    SetBounds(
        ((Application.MainForm.Width - Self.Width) div 2),
        ((Application.MainForm.Height - Self.Height) div 2) - 50,
        Self.Width,
        Self.Height);
end;

procedure TRefinanciacionConsultasForm.peRutButtonClick(Sender: TObject);
    var
        BuscaClientes: TFormBuscaClientes;
begin
    try
        BuscaClientes := TFormBuscaClientes.Create(Self);
        if BuscaClientes.Inicializa(FUltimaBusqueda)then begin
            if BuscaClientes.ShowModal = mrOk then begin
                FUltimaBusqueda := BuscaClientes.UltimaBusqueda;
                peRut.Text     := BuscaClientes.Persona.NumeroDocumento;
                FCodigoCliente := BuscaClientes.Persona.CodigoPersona;

                EstaClienteConMensajeEspecial(DMConnections.BaseCAC, PADL(peRut.Text, 9, '0'));              //SS_1408_MCA_20151027
                MostrarDatosCliente(BuscaClientes.Persona.CodigoPersona, BuscaClientes.Persona.Personeria);

            	CargarConveniosRUT(DMConnections.BaseCAC, vcbConvenios, 'RUT', peRut.Text, False, 0, False, True, True);//TASK_005_AUN_20170403-CU.COBO.COB.301
                if vcbConvenios.Items.Count > 0 then begin
                    FCodigoConvenio := vcbConvenios.Value;
                    vcbConvenios.SetFocus;
                end
                else FCodigoConvenio := 0;

                VerificarClienteCarpetaLegal;
            end;
        end;
    finally
        if Assigned(BuscaClientes) then FreeAndNil(BuscaClientes);
    end;
end;

procedure TRefinanciacionConsultasForm.peRutChange(Sender: TObject);
begin
    FCodigoCliente        := 0;
    FCodigoConvenio       := 0;
    lblNombreCli.Caption  := '';
    lblPersoneria.Caption := '';
    vcbConvenios.Clear;
    lblEnListaAmarilla.Visible  := False;                                         //SS_660_MVI_20130909
    lblEnListaAmarilla.Caption  := EmptyStr;                                      //SS_660_MVI_20130909
end;

procedure TRefinanciacionConsultasForm.peRutKeyPress(Sender: TObject; var Key: Char);
resourcestring
    rs_BUSQUEDA_TITULO = 'B�squeda Persona';
    rs_BUSQUEDA_NO_ENCONTRADA = 'El Rut introducido NO Existe o NO es V�lido.';
    MSG_ALERTA ='Validaci�n Cliente';                                                                 //SS_660_MVI_20130909
    MSG_ALERTA_CLIENTE_LISTA_AMARILLA='EL Cliente posee al menos un convenio en lista amarilla';      //SS_660_MVI_20130909
    SQL_CONSULTAPERSONA = 'SELECT dbo.EstaPersonaEnListaAmarilla(%d)';                                //SS_660_MVI_20130909
var
    FPersona: TDatosPersonales;
    PersonaEnListaAmarilla: Integer;                                                                  //SS_660_MVI_20130909
begin
    if key = #13 then begin
        if Trim(peRut.Text) <> EmptyStr then begin
            FPersona := ObtenerDatosPersonales(DMConnections.BaseCAC, 'RUT', peRut.Text);

            if FPersona.CodigoPersona <> -1 then begin
                EstaClienteConMensajeEspecial(DMConnections.BaseCAC, PADL(peRut.Text, 9, '0'));             //SS_1408_MCA_20151027
                MostrarDatosCliente(FPersona.CodigoPersona, FPersona.Personeria);
                FCodigoCliente := FPersona.CodigoPersona;
                CargarConveniosRUT(DMConnections.BaseCAC, vcbConvenios, FPersona.TipoDocumento, FPersona.NumeroDocumento, False, 0, False, True, True);//TASK_005_AUN_20170403-CU.COBO.COB.301

                if vcbConvenios.Items.Count > 0 then begin
                    FCodigoConvenio := vcbConvenios.Value;
                    if vcbConvenios.Enabled then vcbConvenios.SetFocus;
                end
                else FCodigoConvenio := 0;

                if Sender = peRut then btnFiltrar.Click;
                VerificarClienteCarpetaLegal;
                PersonaEnListaAmarilla := QueryGetValueInt(DMConnections.BaseCAC, Format(SQL_CONSULTAPERSONA, [FCodigoCliente]));    //SS_660_MVI_20130909
                if PersonaEnListaAmarilla = 1 then begin                                                                             //SS_660_MVI_20130909
                   ShowMsgBoxCN(MSG_ALERTA, MSG_ALERTA_CLIENTE_LISTA_AMARILLA, MB_ICONWARNING, Self);                                //SS_660_MVI_20130909
                end;                                                                                                                 //SS_660_MVI_20130909
            end
            else begin
                FCodigoCliente := 0;
                ShowMsgBoxCN(rs_BUSQUEDA_TITULO, rs_BUSQUEDA_NO_ENCONTRADA, MB_ICONWARNING, Self);
            end
        end
        else begin
            FPersona.CodigoPersona := -1;
            if Sender = peRut then btnFiltrar.Click;
        end;
    end;
end;

procedure TRefinanciacionConsultasForm.vcbConveniosChange(Sender: TObject);
resourcestring                                                                                                                                 //SS_660_MVI_20130909
      SQL_ESTACONVENIOENLISTAAMARILLA = 'SELECT dbo.ObtenerEstadoConvenioEnListaAmarilla(%d) ';                                                //SS_660_MVI_20130729
var                                                                                                                                            //SS_660_MVI_20130909
      EstadoListaAmarilla: string;                                                                                                             //SS_660_MVI_20130909
begin
    if vcbConvenios.Value = Unassigned then
        FCodigoConvenio := 0
    else FCodigoConvenio := vcbConvenios.Value;

    if FCodigoConvenio > 0 then begin                                                                                                      //SS_660_MVI_20130909
                                                                                                                                           //SS_660_MVI_20130909
       EstadoListaAmarilla:= QueryGetStringValue(   DMConnections.BaseCAC,                                                                 //SS_660_MVI_20130729
                                               Format(SQL_ESTACONVENIOENLISTAAMARILLA,                                                     //SS_660_MVI_20130729
                                                       [   FCodigoConvenio                                                                 //SS_660_MVI_20130729
                                                       ]                                                                                   //SS_660_MVI_20130729
                                                     )                                                                                     //SS_660_MVI_20130729
                                                     );                                                                                    //SS_660_MVI_20130729
                                                                                                                                           //SS_660_MVI_20130729
        if EstadoListaAmarilla <> '' then begin                                                                                            //SS_660_MVI_20130909                                             //SS_660_MVI_20130729
        lblEnListaAmarilla.Visible  := True;                                                                                               //SS_660_MVI_20130729
        lblEnListaAmarilla.Caption  := EstadoListaAmarilla;                                                                                //SS_660_MVI_20130729
                                                                                                                                           //SS_660_MVI_20130729
        end                                                                                                                                //SS_660_MVI_20130729
        else begin                                                                                                                         //SS_660_MVI_20130729
                                                                                                                                           //SS_660_MVI_20130729
        lblEnListaAmarilla.Visible  := False;                                                                                              //SS_660_MVI_20130729
                                                                                                                                           //SS_660_MVI_20130729
        end;                                                                                                                               //SS_660_MVI_20130729
    end;

end;

//BEGIN:SS_1120_MVI_20130820 -------------------------------------------------
{-----------------------------------------------------------------------------
  Procedure: TRefinanciacionConsultasForm.vcbConveniosDrawItem
  Date:      20-August-2013
  Description: Verifica si existe alg�n convenio de baja para llamar a la funci�n
                que colorea de rojo el item.
-----------------------------------------------------------------------------}
procedure TRefinanciacionConsultasForm.vcbConveniosDrawItem(
  Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  if Pos( TXT_CONVENIO_DE_BAJA, vcbConvenios.Items[Index].Caption ) > 0 then  begin

     ItemComboBoxColor(Control, Index, Rect, State, clred, true);

  end
  else begin
     ItemComboBoxColor(Control, Index, Rect, State);
  end;
end;
//END:SS_1120_MVI_20130820 ----------------------------------------------------

procedure TRefinanciacionConsultasForm.vcbConveniosKeyPress(Sender: TObject; var Key: Char);
begin
    if key = #13 then btnFiltrar.Click;
end;

procedure TRefinanciacionConsultasForm.vcbTipoRefinanciacionKeyPress(Sender: TObject; var Key: Char);
begin
    if key = #13 then btnFiltrar.Click;
end;

procedure TRefinanciacionConsultasForm.MostrarDatosCliente(CodigoCliente: Integer; Personeria: string);
begin
    lblNombreCli.Caption := QueryGetValue(DMConnections.BaseCAC, Format(SQLObtenerNombrePersona, [CodigoCliente]));

    if Personeria = PERSONERIA_FISICA then begin
        lblPersoneria.Caption := ' - (Persona Natural)';
    end
    else begin
        if Personeria = PERSONERIA_JURIDICA then lblPersoneria.Caption := ' - (Persona Jur�dica)';
        if Personeria = PERSONERIA_AMBAS then lblPersoneria.Caption := ' - (Persona Natural y Jur�dica)';
    end;

    lblPersoneria.Left := lblNombreCli.Left + lblNombreCli.Width;
end;

procedure TRefinanciacionConsultasForm.nedtRefinanciacionKeyPress(Sender: TObject; var Key: Char);
begin
    if key = #13 then btnFiltrar.Click;
end;

procedure TRefinanciacionConsultasForm.ActualizarBotones;
var
    ExistenRefinanciaciones,
    RefinanciacionEnTramite,
    RefinanciacionAceptada,
    RefinanciacionVigente,
    RefinanciacionValidada,
    RefinanciacionUnificada,								// SS_1051_PDO
    RefinanciacionMaestra: Boolean;							// SS_1051_PDO
begin
    ExistenRefinanciaciones := cdsRefinanciaciones.Active and (cdsRefinanciaciones.RecordCount > 0);
    RefinanciacionEnTramite := cdsRefinanciacionesCodigoEstado.AsInteger = REFINANCIACION_ESTADO_EN_TRAMITE;
    RefinanciacionAceptada  := cdsRefinanciacionesCodigoEstado.AsInteger = REFINANCIACION_ESTADO_ACEPTADA;
    RefinanciacionVigente   := cdsRefinanciacionesCodigoEstado.AsInteger = REFINANCIACION_ESTADO_VIGENTE;
    RefinanciacionValidada  := cdsRefinanciacionesValidada.AsBoolean;
    RefinanciacionUnificada := cdsRefinanciacionesCodigoRefinanciacionUnificada.AsInteger <> 0;                                                                                              	// SS_1051_PDO
    RefinanciacionMaestra   := RefinanciacionUnificada and (cdsRefinanciacionesCodigoRefinanciacion.AsInteger = cdsRefinanciacionesCodigoRefinanciacionUnificada.AsInteger);					// SS_1051_PDO

    btnEditar.Enabled    := FbtnEditarPermiso and ExistenRefinanciaciones;
    btnATramite.Enabled  := FbtnATramitePermiso and ExistenRefinanciaciones and RefinanciacionAceptada and ((not RefinanciacionUnificada) or (RefinanciacionMaestra));  // SS_1051_PDO
    btnAnular.Enabled    := FbtnAnularPermiso and ExistenRefinanciaciones and RefinanciacionEnTramite  and ((not RefinanciacionUnificada) or (RefinanciacionMaestra));   // SS_1051_PDO
    btnProtestar.Enabled := FbtnProtestarPermiso and RefinanciacionVigente;
    btnCuotas.Enabled    := FbtnCuotasPermiso and ExistenRefinanciaciones and RefinanciacionVigente;
    btnRecibos.Enabled   := FbtnRecibosPermiso and ExistenRefinanciaciones;     // SS_989_PDO_20120119
    btnReversar.Enabled  := FbtnReversarPermiso and                                       //SS_1176_NDR_20140326
                            ExistenRefinanciaciones and                                   //SS_1176_NDR_20140326
                            RefinanciacionVigente and //TASK_005_AUN_20170403-CU.COBO.COB.301: Solo si est� vigente.
                            (not (RefinanciacionUnificada or RefinanciacionMaestra));     //SS_1176_NDR_20140326
    //TASK_005_AUN_20170403-CU.COBO.COB.301
    btnPagar.Enabled        := FbtnPagarPermiso and (GNumeroTurno > 0) and FbtnPagarPermiso and
                               ExistenRefinanciaciones and (RefinanciacionAceptada or RefinanciacionVigente) and
                               (cdsRefinanciacionesTotalDeuda.AsString <> cdsRefinanciacionesTotalPagado.AsString);
end;

procedure TRefinanciacionConsultasForm.CambiarEstadoRefinanciacion(NuevoEstado: Byte);
resourcestring
    RS_REFINANCIACION_A_TRAMITE       = 'Se envi� a Tr�mite correctamente la Refinanciaci�n.';
    RS_REFINANCIACION_ANULADA         = 'Se Anul� Correctamente la Refinanciaci�n.';
    RS_REFINANCIACION_A_TRAMITE_ERROR = 'Se produjo un Error al enviar a Tr�mite correctamente la Refinanciaci�n.';
    RS_REFINANCIACION_ANULADA_ERROR   = 'Se produjo un Error al Anular Correctamente la Refinanciaci�n.';
var
    Puntero: TBookmark;
begin
    try
        try
            CambiarEstadoCursor(CURSOR_RELOJ, Self, FComponentes);
            Puntero := cdsRefinanciaciones.GetBookmark;

            QueryExecute(DMConnections.BaseCAC, 'BEGIN TRANSACTION CambiarEstadoRefinanciacion');

			// BEGIN : SS_1051_PDO ---------------------------------------------------------------------------------------------
            if cdsRefinanciacionesCodigoRefinanciacionUnificada.AsInteger <> 0 then begin
                with spActualizarRefinanciacionUnificadaEstado do begin
                    if Active then Close;
                    Parameters.Refresh;

                    Parameters.ParamByName('@CodigoRefinanciacionUnificada').Value := cdsRefinanciacionesCodigoRefinanciacionUnificada.AsInteger;
                    Parameters.ParamByName('@CodigoEstado').Value                  := NuevoEstado;
                    Parameters.ParamByName('@Usuario').Value                       := UsuarioSistema;
                    ExecProc;

                    if Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
                        raise EErrorExceptionCN.Create(
                                'Cambio de Estado',
                                iif(NuevoEstado = REFINANCIACION_ESTADO_EN_TRAMITE, RS_REFINANCIACION_A_TRAMITE_ERROR, RS_REFINANCIACION_ANULADA_ERROR));
                    end;
                end;
            end
            else begin
                with spActualizarRefinanciacionEstado do begin
                    if Active then Close;
                    Parameters.Refresh;

                    Parameters.ParamByName('@CodigoRefinanciacion').Value := cdsRefinanciacionesCodigoRefinanciacion.AsInteger;
                    Parameters.ParamByName('@CodigoEstado').Value         := NuevoEstado;
                    Parameters.ParamByName('@Usuario').Value              := UsuarioSistema;
                    ExecProc;

                    if Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
                        raise EErrorExceptionCN.Create(
                                'Cambio de Estado',
                                iif(NuevoEstado = REFINANCIACION_ESTADO_EN_TRAMITE, RS_REFINANCIACION_A_TRAMITE_ERROR, RS_REFINANCIACION_ANULADA_ERROR));
                    end;
                end;
            end;
			// END : SS_1051_PDO -------------------------------------------------------------------------------------------

            QueryExecute(DMConnections.BaseCAC, 'IF @@TRANCOUNT>0 BEGIN COMMIT TRANSACTION CambiarEstadoRefinanciacion END');

            ShowMsgBoxCN('Confirmaci�n Proceso', iif(NuevoEstado = REFINANCIACION_ESTADO_EN_TRAMITE, RS_REFINANCIACION_A_TRAMITE, RS_REFINANCIACION_ANULADA), MB_ICONINFORMATION, Self);
        except
            on e: Exception do begin
                QueryExecute(DMConnections.BaseCAC, 'IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION CambiarEstadoRefinanciacion END');
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
        if cdsRefinanciaciones.Active then cdsRefinanciaciones.Close;
        cdsRefinanciaciones.Open;

        if Assigned(Puntero) then begin
            if cdsRefinanciaciones.BookmarkValid(Puntero) then cdsRefinanciaciones.GotoBookmark(Puntero);
            cdsRefinanciaciones.FreeBookmark(Puntero);
        end;

        cdsRefinanciaciones.EnableControls;

        CambiarEstadoCursor(CURSOR_DEFECTO, Self, FComponentes);
        ActualizarBotones;
    end;
end;

procedure TRefinanciacionConsultasForm.btnAnularClick(Sender: TObject);
resourcestring
    MSG_TITULO                  = 'Confirme Proceso de la Refinanciaci�n';
    MSG_ANULAR_REFINANCIACION   = '� Est� seguro de Anular la Refinanciaci�n seleccionada ?';
var
    IDBloqueo: Integer;
    ResultadoBloqueo: Integer;
begin
    if ValidarCambioEstado then try
        CambiarEstadoCursor(CURSOR_RELOJ, Self, FComponentes);
        try
            TPanelMensajesForm.MuestraMensaje('Obteniendo Bloqueo ...', True);
            IDBloqueo := 0;
            ResultadoBloqueo :=
                ObtenerBloqueo
                    (
                        DMConnections.BaseCAC,
                        'RefAnular',
                        cdsRefinanciacionesCodigoRefinanciacion.AsString,
                        UsuarioSistema,
                        IDBloqueo
                    );

            if ResultadoBloqueo = 0 then begin
                TPanelMensajesForm.OcultaPanel;
                if ShowMsgBoxCN(MSG_TITULO, MSG_ANULAR_REFINANCIACION, MB_ICONQUESTION, Self) = mrOk then begin
                    CambiarEstadoRefinanciacion(REFINANCIACION_ESTADO_ANULADA);
                end;
            end;
        except
            on e: Exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
        try
            if IDBloqueo <> 0 then begin
                TPanelMensajesForm.MuestraMensaje('Eliminando Bloqueo ...', True);
                EliminarBloqueo(DMConnections.BaseCAC, IDBloqueo);
            end;
        except
            on e: Exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;

        TPanelMensajesForm.OcultaPanel;
        CambiarEstadoCursor(CURSOR_DEFECTO, Self, FComponentes);
    end
    else begin
        ShowMsgBoxCN(MSG_TITULO_VALIDAR_ESTADO, MSG_MENSAJE_VALIDAR_ESTADO, MB_ICONWARNING, Self);
        RefrescarDatos;
    end;
end;

procedure TRefinanciacionConsultasForm.RefrescarDatos;
var
    Puntero: TBookmark;
begin
    if cdsRefinanciaciones.Active then begin
        if cdsRefinanciaciones.RecordCount > 0 then try
            CambiarEstadoCursor(CURSOR_RELOJ);
            Puntero := cdsRefinanciaciones.GetBookmark;
            cdsRefinanciaciones.DisableControls;
            cdsRefinanciaciones.Close;
            cdsRefinanciaciones.Open;
        finally
            cdsRefinanciaciones.EnableControls;

            if Assigned(Puntero) then begin
                if cdsRefinanciaciones.BookmarkValid(Puntero) then cdsRefinanciaciones.GotoBookmark(Puntero);
                cdsRefinanciaciones.FreeBookmark(Puntero);
            end;

            CambiarEstadoCursor(CURSOR_DEFECTO);
            ActualizarBotones;
        end;
    end;
end;

//BEGIN : SS_1176_NDR_20140326 ------------------------------------------------------------------------------------------
procedure TRefinanciacionConsultasForm.ReversarRefinanciacion;
resourcestring
    RS_REFINANCIACION_REVERSADA         = 'Se Revers� Correctamente la Refinanciaci�n.';
    RS_REFINANCIACION_REVERSADA_ERROR = 'Se produjo un Error al Reversar la Refinanciaci�n.';
var
    Puntero: TBookmark;
begin
    try
        try
            CambiarEstadoCursor(CURSOR_RELOJ, Self, FComponentes);
            Puntero := cdsRefinanciaciones.GetBookmark;

            QueryExecute(DMConnections.BaseCAC, 'BEGIN TRANSACTION TrxReversarRefinanciacion');

            with spReversarRefinanciacion do begin
                if Active then Close;
                Parameters.Refresh;

                Parameters.ParamByName('@CodigoRefinanciacion').Value := cdsRefinanciacionesCodigoRefinanciacion.AsInteger;
                Parameters.ParamByName('@CodigoUsuario').Value        := UsuarioSistema;
                Parameters.ParamByName('@MensajeError').Value         := null;
                ExecProc;

                if Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
                    raise EErrorExceptionCN.Create(
                            'Reversar Refinanciacion',
                            RS_REFINANCIACION_REVERSADA_ERROR + ': ' + Parameters.ParamByName('@MensajeError').Value);
                end;
            end;

            QueryExecute(DMConnections.BaseCAC, 'IF @@TRANCOUNT>0 BEGIN COMMIT TRANSACTION TrxReversarRefinanciacion END');

            ShowMsgBoxCN('Confirmaci�n Proceso', RS_REFINANCIACION_REVERSADA, MB_ICONINFORMATION, Self);
        except
            on e: Exception do begin
                QueryExecute(DMConnections.BaseCAC, 'IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION TrxReversarRefinanciacion END');
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
        if cdsRefinanciaciones.Active then cdsRefinanciaciones.Close;
        cdsRefinanciaciones.Open;

        if Assigned(Puntero) then begin
            if cdsRefinanciaciones.BookmarkValid(Puntero) then cdsRefinanciaciones.GotoBookmark(Puntero);
            cdsRefinanciaciones.FreeBookmark(Puntero);
        end;

        cdsRefinanciaciones.EnableControls;

        CambiarEstadoCursor(CURSOR_DEFECTO, Self, FComponentes);
        ActualizarBotones;
    end;
end;
//END : SS_1176_NDR_20140326 ------------------------------------------------------------------------------------------

procedure TRefinanciacionConsultasForm.VerificarClienteCarpetaLegal;
begin
    try
        CambiarEstadoCursor(CURSOR_RELOJ);
        try
            ConveniosEnCarpetaLegalForm := TConveniosEnCarpetaLegalForm.Create(Self);
            with ConveniosEnCarpetaLegalForm do begin
                if inicializar(peRut.Text, lblNombreCli.Caption) then begin
                    if spObtenerCarpetasLegalesPersona.RecordCount > 0 then ShowModal;
                end;
            end;
        except
            on e: Exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
        if Assigned(ConveniosEnCarpetaLegalForm) then FreeAndNil(ConveniosEnCarpetaLegalForm);

        CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

procedure TRefinanciacionConsultasForm.VerificarRefinanciacionesNoActivadas;
begin
    try
        CambiarEstadoCursor(CURSOR_RELOJ);
        try
            RefinanciacionesEnTramiteAceptadasForm := TRefinanciacionesEnTramiteAceptadasForm.Create(Self);
            with RefinanciacionesEnTramiteAceptadasForm do begin
                if inicializar(peRut.Text, lblNombreCli.Caption) then begin
                    if spObtenerRefinanciacionesConsultas.RecordCount > 0 then ShowModal;
                end;
            end;
        except
            on e: Exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
        if Assigned(RefinanciacionesEnTramiteAceptadasForm) then FreeAndNil(RefinanciacionesEnTramiteAceptadasForm);

        CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

function TRefinanciacionConsultasForm.ValidarCambioEstado: boolean;
begin
    try
        CambiarEstadoCursor(CURSOR_RELOJ, Self, FComponentes);
        try
            Result := False;

            with spObtenerRefinanciacionDatosCabecera do begin
                if Active then Close;

                Parameters.Refresh;
                Parameters.ParamByName('@CodigoRefinanciacion').Value := cdsRefinanciacionesCodigoRefinanciacion.AsInteger;
                Open;

                Result := cdsRefinanciacionesCodigoEstado.AsInteger = FieldByName('CodigoEstado').AsInteger;
            end;
        except
            on e: Exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
        CambiarEstadoCursor(CURSOR_DEFECTO, Self, FComponentes);
    end;
end;

end.
