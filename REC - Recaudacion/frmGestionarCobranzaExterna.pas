{
Gesti�n de Convenios enviados a cobranza externa
TASK_007_AUN_20170410-CU.COBO.COB.202 Gestionar cobranza externa
}
unit frmGestionarCobranzaExterna;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ImgList, DB, DBClient, ADODB, ListBoxEx, DBListEx, StdCtrls,
  DmiCtrls, VariantComboBox, ExtCtrls;

type
  TGestionarCobranzaExternaForm = class(TForm)
    dlConvenios: TDBListEx;
    lnCheck: TImageList;
    csConveniosACobranzaExterna: TDataSource;
    cdsConveniosACobranzaExterna: TClientDataSet;
    cdsConveniosACobranzaExternaMarcar: TBooleanField;
    cdsConveniosACobranzaExternaNumeroDocumento: TStringField;
    cdsConveniosACobranzaExternaTipoDeClienteStr: TStringField;
    cdsConveniosACobranzaExternaCodigoConvenio: TIntegerField;
    cdsConveniosACobranzaExternaPersoneriaStr: TStringField;
    cdsConveniosACobranzaExternaComunaStr: TStringField;
    cdsConveniosACobranzaExternaMontoDeuda: TFloatField;
    cdsConveniosACobranzaExternaFechaVencimiento: TDateField;
    cdsConveniosACobranzaExternaCantidadDeDocumentos: TIntegerField;
    cdsConveniosACobranzaExternaEnviadoACobranzaExterna: TBooleanField;
    cdsConveniosACobranzaExternaFechaDeEnvioACobranzaExterna: TDateField;
    cdsConveniosACobranzaExternaEstadoDeCobranzaExterna: TIntegerField;
    cdsConveniosACobranzaExternaEstadoDeCobranzaExternaStr: TStringField;
    cdsConveniosACobranzaExternaEdadDeuda: TIntegerField;
    cdsConveniosACobranzaExternaEnviadoACobranzaExternaStr: TStringField;
    gbEmpresasCobranza: TGroupBox;
    dlEmpresasCobranza: TDBListEx;
    pnDetalleEmpresaCobranza: TPanel;
    lblNombre: TLabel;
    lblTipoDeOperacionEmpresasCobranza: TLabel;
    txtTipoDeOperacionEmpresasCobranza: TVariantComboBox;
    lblComisionPorAviso: TLabel;
    txtComisionPorAviso: TNumericEdit;
    Label1: TLabel;
    lblComisionPorCobranza: TLabel;
    txtComisionPorCobranza: TNumericEdit;
    Label2: TLabel;
    lblMotivoDeTransferencia: TLabel;
    txtMotivoDeTransferencia: TEdit;
    lblNombreEmpresaCobranza: TLabel;
    PAbajo: TPanel;
    Panel1: TPanel;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    cdsEmpresasCobranza: TClientDataSet;
    dsEmpresasCobranza: TDataSource;
    EmpFieldAlias: TStringField;
    EmpFieldComunaStr: TStringField;
    EmpFieldElegiblePorEdadDeDeuda: TBooleanField;
    EmpFieldElegiblePorEdadDeDeudaStr: TStringField;
    EmpFieldElegiblePorPersoneria: TBooleanField;
    EmpFieldElegiblePorPersoneriaStr: TStringField;
    EmpFieldElegiblePorComuna: TBooleanField;
    EmpFieldElegiblePorComunaStr: TStringField;
    EmpFieldElegiblePorMontoMinimo: TBooleanField;
    EmpFieldElegiblePorMontoMinimoStr: TStringField;
    EmpFieldElegiblePorMontoMaximo: TBooleanField;
    EmpFieldElegiblePorMontoMaximoStr: TStringField;
    EmpFieldJudicial: TBooleanField;
    EmpFieldJudicialStr: TStringField;
    EmpFieldCodigoEmpresaCobranza: TIntegerField;
    lblNoExistenEmpresasDeCobranza: TLabel;
    EmpFieldCodigoTipoCliente: TIntegerField;
    EmpFieldCodTipoJudicialEmpCob: TIntegerField;
    EmpFieldCodTipoDeOpEmpCob: TIntegerField;
    EmpFieldComisionPorAviso: TFloatField;
    EmpFieldComisionPorCobranza: TFloatField;
    FieldEmpEdadDeuda: TIntegerField;
    EmpFieldMontoMinimo: TFloatField;
    EmpFieldMontoMaximo: TFloatField;
    EmpFieldCapacidadDeProceso: TIntegerField;
    EmpFieldNombre: TStringField;
    cdsConveniosACobranzaExternaCodigoTipoCliente: TIntegerField;
    EmpFieldDescripcion: TStringField;
    cdsConveniosACobranzaExternaCodigoComuna: TStringField;
    spConveniosEnCobranzaExterna_Almacenar: TADOStoredProc;
    EmpFieldCapacidadStr: TStringField;
    cdsConveniosACobranzaExternaCodigoRegion: TStringField;
    EmpField: TStringField;
    EmpFieldCodigoRegion: TStringField;
    spConveniosEnCobranzaExterna_TraspasoDeEmpresa: TADOStoredProc;
    spConveniosEnCobranzaExterna_RecuperacionDeDeuda: TADOStoredProc;
    cdsConveniosACobranzaExternaCodConveniosEnCobExt: TLargeintField;
    cdsConveniosACobranzaExternaCodTipoJudEmpCob: TIntegerField;
    cdsConveniosACobranzaExternaCodigoEmpresaCobranza: TIntegerField;
    procedure dlConveniosDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure dlConveniosLinkClick(Sender: TCustomDBListEx;
      Column: TDBListExColumn);
    procedure BtnCancelarClick(Sender: TObject);
    procedure cdsEmpresasCobranzaAfterScroll(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure txtTipoDeOperacionEmpresasCobranzaChange(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
  private
    { Private declarations }
    FCerrarConMROk: Boolean;
    FCodigoConveniosCSV: String;
    FCodigoTipoJudicialEmpresasCobranza: Integer;
    FEsTraspasoDeEmpresa, FEsRecuperacionDeDeuda: Boolean;
    FTiposDeOperacionEmpresasCobranza: TVariantItems;
    FiltroEdadDeLaDeudaMinima: Integer;
    FiltroPersonerias: set of 1..3;
    FiltroCodigoRegion, FiltroCodigoComuna: String;
    FiltroMontoMinimoDeuda, FiltroMontoMaximoDeuda: Double;
    procedure InicializarValoresParaFiltros;
    function RecargarConveniosEnLista: boolean;
    procedure ProcesarCodigoTipoJudicialEmpresasCobranza;
    procedure ProcesarTraspasoDeEmpresa;
    procedure ProcesarRecuperacionDeDeuda;
  public
    { Public declarations }
    function Inicializar(CodigoTipoDeAccionesDeCobranza: Integer; CodigoConveniosCSV: String): Boolean;
  end;

implementation

uses DB_CRUDCommonProcs, DMConnection, UtilProc, Util;

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:  aunanue
  Date: 04-04-2017
  Firma: TASK_006_AUN_20170406-CU.COBO.COB.302
  Description: Inicializa la b�squeda de planes de refinanciaci�n que cumpla con el criterio
  Par�metros: CodigoTipoDeAccionesDeCobranza: El valor del combo seleccionado.
-----------------------------------------------------------------------------}
function TGestionarCobranzaExternaForm.Inicializar(CodigoTipoDeAccionesDeCobranza: Integer;
                                                   CodigoConveniosCSV: String): Boolean;
begin
    FCerrarConMROk := False;
    FCodigoConveniosCSV := CodigoConveniosCSV;
    FEsTraspasoDeEmpresa := CodigoTipoDeAccionesDeCobranza = 12;
    if FEsTraspasoDeEmpresa then Self.Caption := Self.Caption + ' - Transferir Deuda';
    //
    FEsRecuperacionDeDeuda := CodigoTipoDeAccionesDeCobranza = 10;
    if FEsRecuperacionDeDeuda then Self.Caption := Self.Caption + ' - Recuperar Deuda';
    //
    if CodigoTipoDeAccionesDeCobranza = 6 then begin
        FCodigoTipoJudicialEmpresasCobranza := 1;
        Self.Caption := Self.Caption + ' - Enviar cobranza prejudicial';
    end else if CodigoTipoDeAccionesDeCobranza = 7 then begin
        FCodigoTipoJudicialEmpresasCobranza := 2;
        Self.Caption := Self.Caption + ' - Enviar cobranza judicial';
    end else
        FCodigoTipoJudicialEmpresasCobranza := 0;
    //        
    lblNombreEmpresaCobranza.Caption := '';
    txtComisionPorAviso.Value := 0;
    txtComisionPorCobranza.Value := 0;
    lblMotivoDeTransferencia.Visible := FEsTraspasoDeEmpresa or FEsRecuperacionDeDeuda;
    txtMotivoDeTransferencia.Visible := FEsTraspasoDeEmpresa or FEsRecuperacionDeDeuda;
    //Datos del convenio
    Result := RecargarConveniosEnLista;
    //Empresas de cobranzas
    if Result then begin
        Result := DB_CRUDCommonProcs.CreateDataSetFromStoredProcedure(DMConnections.BaseCAC,
        'dbo.EmpresasDeCobranza_ListarParaSeleccion', cdsEmpresasCobranza,
        ['@CodigoTipoJudicialEmpresasCobranza'],
        [FCodigoTipoJudicialEmpresasCobranza],
        Self.Caption);
        //TiposDeOperacionEmpresasCobranza
        DB_CRUDCommonProcs.FillVariantItemsFromStoredProc(DMConnections.BaseCAC, 'TiposDeOperacionEmpresasCobranza_Listar',
                            FTiposDeOperacionEmpresasCobranza, 'CodigoTipoDeOperacionEmpresasCobranza', 'Descripcion', [], [],
                            Self.Caption, False);
        txtTipoDeOperacionEmpresasCobranza.Items.Add('Seleccionar', -1);
        //Al inicializar no muestro ninguna empresa de cobranza
        cdsEmpresasCobranza.Filter := 'CodigoComuna = ''-1''';
        cdsEmpresasCobranza.Filtered := True;
        lblNoExistenEmpresasDeCobranza.Visible := False;
        BtnAceptar.Enabled := False;
        //
        InicializarValoresParaFiltros;
        if FEsRecuperacionDeDeuda then begin
            //en esto no aparecen las empresas de cobranza.
            gbEmpresasCobranza.Visible := False;
        end;
    end;
end;

function TGestionarCobranzaExternaForm.RecargarConveniosEnLista: boolean;
begin
    cdsConveniosACobranzaExterna.Close;
    Result := DB_CRUDCommonProcs.CreateDataSetFromStoredProcedure(DMConnections.BaseCAC,
        'dbo.ObtenerConveniosParaGestionarCobranzaExterna', cdsConveniosACobranzaExterna,
        ['@ListaConveniosCSV', '@EsTraspasoDeEmpresaORecuperacionDeDeuda', '@CodigoTipoJudicialEmpresasCobranza'],
        [FCodigoConveniosCSV, (FEsTraspasoDeEmpresa OR FEsRecuperacionDeDeuda), FCodigoTipoJudicialEmpresasCobranza],
        Self.Caption);
end;

procedure TGestionarCobranzaExternaForm.InicializarValoresParaFiltros;
begin
    FiltroEdadDeLaDeudaMinima := MaxInt;
    FiltroPersonerias := [];
    FiltroCodigoRegion := '-1';
    FiltroCodigoComuna := '-1';
    FiltroMontoMinimoDeuda := MaxInt;
    FiltroMontoMaximoDeuda := 0;
end;

procedure TGestionarCobranzaExternaForm.txtTipoDeOperacionEmpresasCobranzaChange(
  Sender: TObject);
begin
    txtComisionPorAviso.Enabled := (txtTipoDeOperacionEmpresasCobranza.Value = 1) or (txtTipoDeOperacionEmpresasCobranza.Value = 3);
    //
    txtComisionPorCobranza.Enabled := (txtTipoDeOperacionEmpresasCobranza.Value = 2) or (txtTipoDeOperacionEmpresasCobranza.Value = 3);
end;

{-----------------------------------------------------------------------------
  Function Name: ProcesarRecuperacionDeDeuda
  Author:  aunanue
  Date: 04-04-2017
  Firma: TASK_006_AUN_20170406-CU.COBO.COB.302
  Description: Proceso los que est�n marcados para recuperar la deuda, es decir volverlos a cobranza interna
-----------------------------------------------------------------------------}
procedure TGestionarCobranzaExternaForm.ProcesarRecuperacionDeDeuda;
begin
    cdsConveniosACobranzaExterna.First;
    while not cdsConveniosACobranzaExterna.Eof do begin
        if cdsConveniosACobranzaExternaMarcar.Value then begin
            spConveniosEnCobranzaExterna_RecuperacionDeDeuda.Connection.BeginTrans;
            AddParameter(spConveniosEnCobranzaExterna_RecuperacionDeDeuda.Parameters, '@CodigoConvenio', ftInteger, pdInput, cdsConveniosACobranzaExternaCodigoConvenio.Value);
            AddParameter_CodigoUsuario(spConveniosEnCobranzaExterna_RecuperacionDeDeuda.Parameters);
            spConveniosEnCobranzaExterna_RecuperacionDeDeuda.ExecProc;
            spConveniosEnCobranzaExterna_RecuperacionDeDeuda.Connection.CommitTrans;
        end;
        //siguiente registro
        cdsConveniosACobranzaExterna.Next;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ProcesarTraspasoDeEmpresa
  Author:  aunanue
  Date: 04-04-2017
  Firma: TASK_006_AUN_20170406-CU.COBO.COB.302
  Description: Proceso los que est�n para ser transferidos a otra empresa
-----------------------------------------------------------------------------}
procedure TGestionarCobranzaExternaForm.ProcesarTraspasoDeEmpresa;
begin
    cdsConveniosACobranzaExterna.First;
    while not cdsConveniosACobranzaExterna.Eof do begin
        if cdsConveniosACobranzaExternaMarcar.Value then begin
            spConveniosEnCobranzaExterna_TraspasoDeEmpresa.Connection.BeginTrans;
            AddParameter(spConveniosEnCobranzaExterna_TraspasoDeEmpresa.Parameters, '@CodigoConvenio', ftInteger, pdInput, cdsConveniosACobranzaExternaCodigoConvenio.Value);
            AddParameter(spConveniosEnCobranzaExterna_TraspasoDeEmpresa.Parameters, '@CodigoEmpresaCobranza', ftInteger, pdInput, EmpFieldCodigoEmpresaCobranza.Value);
            AddParameter_CodigoUsuario(spConveniosEnCobranzaExterna_TraspasoDeEmpresa.Parameters);
            AddParameter(spConveniosEnCobranzaExterna_TraspasoDeEmpresa.Parameters, '@CodigoTipoJudicialEmpresasCobranza', ftInteger, pdInput, EmpFieldCodTipoJudicialEmpCob.Value);
            AddParameter(spConveniosEnCobranzaExterna_TraspasoDeEmpresa.Parameters, '@CodigoTipoDeOperacionEmpresasCobranza', ftInteger, pdInput, EmpFieldCodTipoDeOpEmpCob.Value);
            AddParameter(spConveniosEnCobranzaExterna_TraspasoDeEmpresa.Parameters, '@ComisionPorAviso', ftFloat, pdInput, iif(txtComisionPorAviso.Enabled, txtComisionPorAviso.Value, 0));
            AddParameter(spConveniosEnCobranzaExterna_TraspasoDeEmpresa.Parameters, '@ComisionPorCobranza', ftFloat, pdInput, iif(txtComisionPorCobranza.Enabled, txtComisionPorCobranza.Value, 0));
            AddParameter(spConveniosEnCobranzaExterna_TraspasoDeEmpresa.Parameters, '@MontoDeuda', ftInteger, pdInput, cdsConveniosACobranzaExternaMontoDeuda.Value);
            AddParameter(spConveniosEnCobranzaExterna_TraspasoDeEmpresa.Parameters, '@CantidadDeDocumentos', ftInteger, pdInput, cdsConveniosACobranzaExternaCantidadDeDocumentos.Value);
            AddParameter(spConveniosEnCobranzaExterna_TraspasoDeEmpresa.Parameters, '@FechaVencimiento', ftDate, pdInput, cdsConveniosACobranzaExternaFechaVencimiento.Value);
            AddParameter(spConveniosEnCobranzaExterna_TraspasoDeEmpresa.Parameters, '@EdadDeuda', ftInteger, pdInput, cdsConveniosACobranzaExternaEdadDeuda.Value);
            AddParameter(spConveniosEnCobranzaExterna_TraspasoDeEmpresa.Parameters, '@MotivoDeTraspasoDeEmpresa', ftString, pdInput, txtMotivoDeTransferencia.Text, 255);
            spConveniosEnCobranzaExterna_TraspasoDeEmpresa.ExecProc;
            spConveniosEnCobranzaExterna_TraspasoDeEmpresa.Connection.CommitTrans;
        end;
        //siguiente registro
        cdsConveniosACobranzaExterna.Next;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ProcesarCodigoTipoJudicialEmpresasCobranza
  Author:  aunanue
  Date: 04-04-2017
  Firma: TASK_006_AUN_20170406-CU.COBO.COB.302
  Description: Proceso los que est�n para ser informados a empresas de cobranza con TipoJudicial: 1 o 2
-----------------------------------------------------------------------------}
procedure TGestionarCobranzaExternaForm.ProcesarCodigoTipoJudicialEmpresasCobranza;
begin
    cdsConveniosACobranzaExterna.First;
    while not cdsConveniosACobranzaExterna.Eof do begin
        if cdsConveniosACobranzaExternaMarcar.Value then begin
            spConveniosEnCobranzaExterna_Almacenar.Connection.BeginTrans;
            AddParameter(spConveniosEnCobranzaExterna_Almacenar.Parameters, '@CodigoConveniosEnCobranzaExterna', ftLargeint, pdInput, cdsConveniosACobranzaExternaCodConveniosEnCobExt.Value);
            AddParameter(spConveniosEnCobranzaExterna_Almacenar.Parameters, '@CodigoConvenio', ftInteger, pdInput, cdsConveniosACobranzaExternaCodigoConvenio.Value);
            AddParameter(spConveniosEnCobranzaExterna_Almacenar.Parameters, '@CodigoEmpresaCobranza', ftInteger, pdInput, EmpFieldCodigoEmpresaCobranza.Value);
            AddParameter_CodigoUsuario(spConveniosEnCobranzaExterna_Almacenar.Parameters);
            AddParameter(spConveniosEnCobranzaExterna_Almacenar.Parameters, '@CodigoTipoJudicialEmpresasCobranza', ftInteger, pdInput, EmpFieldCodTipoJudicialEmpCob.Value);
            AddParameter(spConveniosEnCobranzaExterna_Almacenar.Parameters, '@CodigoTipoDeOperacionEmpresasCobranza', ftInteger, pdInput, EmpFieldCodTipoDeOpEmpCob.Value);
            AddParameter(spConveniosEnCobranzaExterna_Almacenar.Parameters, '@ComisionPorAviso', ftFloat, pdInput, iif(txtComisionPorAviso.Enabled, txtComisionPorAviso.Value, 0));
            AddParameter(spConveniosEnCobranzaExterna_Almacenar.Parameters, '@ComisionPorCobranza', ftFloat, pdInput, iif(txtComisionPorCobranza.Enabled, txtComisionPorCobranza.Value, 0));
            AddParameter(spConveniosEnCobranzaExterna_Almacenar.Parameters, '@MontoDeuda', ftInteger, pdInput, cdsConveniosACobranzaExternaMontoDeuda.Value);
            AddParameter(spConveniosEnCobranzaExterna_Almacenar.Parameters, '@CantidadDeDocumentos', ftInteger, pdInput, cdsConveniosACobranzaExternaCantidadDeDocumentos.Value);
            AddParameter(spConveniosEnCobranzaExterna_Almacenar.Parameters, '@FechaVencimiento', ftDate, pdInput, cdsConveniosACobranzaExternaFechaVencimiento.Value);
            AddParameter(spConveniosEnCobranzaExterna_Almacenar.Parameters, '@EdadDeuda', ftInteger, pdInput, cdsConveniosACobranzaExternaEdadDeuda.Value);
            spConveniosEnCobranzaExterna_Almacenar.ExecProc;
            spConveniosEnCobranzaExterna_Almacenar.Connection.CommitTrans;
        end;
        //siguiente registro
        cdsConveniosACobranzaExterna.Next;
    end;
end;

procedure TGestionarCobranzaExternaForm.BtnAceptarClick(Sender: TObject);
resourcestring
    MSG_CONVENIOS_PROCESADOS_OK = 'Los convenios seleccionados fueron procesados correctamente.';
    MSG_ERROR_ENVIANDO_CONVENIOS_A_COBRANZA_EXTERNA = 'Se ha producido un error enviando los convenios seleccionados a cobranza externa.';
    MSG_ERROR_LA_COMISION_POR_AVISO_DEBE_SER_MAYOR_CERO = 'La comisi�n por aviso debe ser mayor que cero.';
    MSG_ERROR_LA_COMISION_POR_COBRANZA_DEBE_SER_MAYOR_CERO = 'La comisi�n por cobranza debe ser mayor que cero.';
begin
    if txtComisionPorAviso.Value < 0 then begin
        MsgBoxBalloon(MSG_ERROR_LA_COMISION_POR_AVISO_DEBE_SER_MAYOR_CERO, Self.Caption, MB_ICONSTOP, txtComisionPorAviso);
        Exit
    end;
    if txtComisionPorCobranza.Value < 0 then begin
        MsgBoxBalloon(MSG_ERROR_LA_COMISION_POR_COBRANZA_DEBE_SER_MAYOR_CERO, Self.Caption, MB_ICONSTOP, txtComisionPorCobranza);
        Exit
    end;

    Screen.Cursor := crHourGlass;
    cdsConveniosACobranzaExterna.DisableControls;
    cdsEmpresasCobranza.DisableControls;
    BtnAceptar.Enabled := False;
    BtnCancelar.Enabled := False;
    try
        try
            if FEsTraspasoDeEmpresa then begin
                ProcesarTraspasoDeEmpresa;
            end else if FEsRecuperacionDeDeuda then begin
                ProcesarRecuperacionDeDeuda;
            end else begin
                ProcesarCodigoTipoJudicialEmpresasCobranza;
            end;
            MsgBox(MSG_CONVENIOS_PROCESADOS_OK, Self.Caption, MB_OK);
            FCerrarConMROk := True;
            //Luego de insertar refresco los datos
            RecargarConveniosEnLista;
            //Limpio la lista de empresas.
            cdsEmpresasCobranza.Filter := 'CodigoComuna = ''-1''';
            cdsEmpresasCobranza.Filtered := True;
        except
            on e: exception do begin
                if spConveniosEnCobranzaExterna_Almacenar.Connection.InTransaction then
                    spConveniosEnCobranzaExterna_Almacenar.Connection.RollbackTrans;
                //
                MsgBoxErr(MSG_ERROR_ENVIANDO_CONVENIOS_A_COBRANZA_EXTERNA, e.Message, Self.Caption, MB_ICONSTOP);
            end;
        end;
    finally
        BtnCancelar.Enabled := True;
        cdsConveniosACobranzaExterna.EnableControls;
        cdsEmpresasCobranza.EnableControls;
        Screen.Cursor := crDefault;
        if cdsConveniosACobranzaExterna.RecordCount = 0 then begin
            //luego de procesar, si no hay m�s registros tengo que cerrar la ventana
            ModalResult := MrOk;
        end;
    end;
end;

procedure TGestionarCobranzaExternaForm.BtnCancelarClick(Sender: TObject);
begin
    if FCerrarConMROk then ModalResult := MrOk
    else Close;
end;

procedure TGestionarCobranzaExternaForm.cdsEmpresasCobranzaAfterScroll(
  DataSet: TDataSet);
var
  i: Integer;
begin
    lblNombreEmpresaCobranza.Caption := EmpFieldNombre.Value;
    txtComisionPorAviso.Value := EmpFieldComisionPorAviso.Value;
    txtComisionPorCobranza.Value := EmpFieldComisionPorCobranza.Value;
    txtTipoDeOperacionEmpresasCobranza.Items.Clear;
    for i := 0 to FTiposDeOperacionEmpresasCobranza.Count - 1 do begin
        if (EmpFieldCodTipoDeOpEmpCob.Value = FTiposDeOperacionEmpresasCobranza.Items[i].Value) or
            (EmpFieldCodTipoDeOpEmpCob.Value = 3) then
            txtTipoDeOperacionEmpresasCobranza.Items.Add(FTiposDeOperacionEmpresasCobranza.Items[i].Caption, FTiposDeOperacionEmpresasCobranza.Items[i].Value);
    end;
    txtTipoDeOperacionEmpresasCobranza.Value := EmpFieldCodTipoDeOpEmpCob.Value;
    if txtTipoDeOperacionEmpresasCobranza.Value = Unassigned then begin
        txtTipoDeOperacionEmpresasCobranza.Items.Add('Seleccionar', -1);
        txtTipoDeOperacionEmpresasCobranza.Value := -1;
    end;
    txtTipoDeOperacionEmpresasCobranza.OnChange(txtTipoDeOperacionEmpresasCobranza);
end;

procedure TGestionarCobranzaExternaForm.dlConveniosDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
var
    bmp: TBitMap;
begin
    bmp := nil;
    with Sender do begin
        if (Column.FieldName = 'Marcar') then
        try
            Text := '';
            Canvas.FillRect(Rect);
            bmp := TBitMap.Create;
			DefaultDraw := False;

            if cdsConveniosACobranzaExternaMarcar.AsBoolean then
	            lnCheck.GetBitmap(1, Bmp)
            else
            	lnCheck.GetBitmap(0, Bmp);

            Canvas.Draw((Rect.Left + Rect.Right - Bmp.Width) div 2, Rect.Top, bmp);
        finally
            if Assigned(bmp) then
                bmp.Free;
        end;
    end;
end;

procedure TGestionarCobranzaExternaForm.dlConveniosLinkClick(
  Sender: TCustomDBListEx; Column: TDBListExColumn);
var
    Bookmark: TBookmark;
    FILTRO_Personeria: string;
    FILTROS_Obligatorios: string;
    AuxCodigoRegion, AuxCodigoComunas: string;
    CantConvSeleccionados: Integer;
    CodigoEmpresaCobranza: integer;
begin
    cdsConveniosACobranzaExterna.Edit;
    cdsConveniosACobranzaExternaMarcar.Value := not cdsConveniosACobranzaExternaMarcar.Value;
    cdsConveniosACobranzaExterna.Post;
    //
    cdsConveniosACobranzaExterna.DisableControls;
    cdsEmpresasCobranza.DisableControls;
    Bookmark := cdsConveniosACobranzaExterna.GetBookmark;
    try
        CantConvSeleccionados := 0;
        CodigoEmpresaCobranza := 0;
        InicializarValoresParaFiltros;
        cdsConveniosACobranzaExterna.First;
        AuxCodigoComunas := '';
        while not cdsConveniosACobranzaExterna.Eof do begin
            if cdsConveniosACobranzaExternaMarcar.Value then begin
                //De todos los marcados, buscamos:

                //la empresa actual
                if (cdsConveniosACobranzaExternaCodigoEmpresaCobranza.Value > 0) and (CodigoEmpresaCobranza = 0) then
                    CodigoEmpresaCobranza := cdsConveniosACobranzaExternaCodigoEmpresaCobranza.Value;
                //la edad m�nima
                if cdsConveniosACobranzaExternaEdadDeuda.Value < FiltroEdadDeLaDeudaMinima then
                    FiltroEdadDeLaDeudaMinima := cdsConveniosACobranzaExternaEdadDeuda.Value;
                //la personer�a
                FiltroPersonerias := FiltroPersonerias + [cdsConveniosACobranzaExternaCodigoTipoCliente.Value];
                //La region/comuna
                if AuxCodigoRegion = '' then begin
                    AuxCodigoRegion := cdsConveniosACobranzaExternaCodigoRegion.Value;
                end else if AuxCodigoRegion <> cdsConveniosACobranzaExternaCodigoRegion.Value then begin
                    AuxCodigoRegion := '-1';
                end;
                if AuxCodigoComunas = '' then begin
                    AuxCodigoComunas := cdsConveniosACobranzaExternaCodigoComuna.Value;
                end else if AuxCodigoComunas <> cdsConveniosACobranzaExternaCodigoComuna.Value then begin
                    AuxCodigoComunas := '-1';
                end;
                //El monto m�nimo de deuda
                if cdsConveniosACobranzaExternaMontoDeuda.Value < FiltroMontoMinimoDeuda then
                    FiltroMontoMinimoDeuda := cdsConveniosACobranzaExternaMontoDeuda.Value;
                //El monto m�ximo de la deuda
                if cdsConveniosACobranzaExternaMontoDeuda.Value > FiltroMontoMaximoDeuda then
                    FiltroMontoMaximoDeuda := cdsConveniosACobranzaExternaMontoDeuda.Value;
                //
                CantConvSeleccionados := CantConvSeleccionados + 1;
            end;
            cdsConveniosACobranzaExterna.Next;
        end;
        FiltroCodigoRegion := AuxCodigoRegion;
        FiltroCodigoComuna := AuxCodigoComunas;
        //Los 3 primeros filtros son mandatorios: EdadDeuda, Personeria, Comuna.
        //Ahora filtramos las empresas de Cobranza con los valores seleccionados
        FILTRO_Personeria := ' AND CodigoTipoCliente IN (';
        if 1 in FiltroPersonerias then
            FILTRO_Personeria := FILTRO_Personeria + '1, '
        else
            FILTRO_Personeria := FILTRO_Personeria + '0, ';
        //
        if 2 in FiltroPersonerias then
            FILTRO_Personeria := FILTRO_Personeria + '2, '
        else
            FILTRO_Personeria := FILTRO_Personeria + '0, ';
        //Seteo tambi�n para ambos
        FILTRO_Personeria := FILTRO_Personeria + '3)';
        //Seteo los 3 filtros principales
        FILTROS_Obligatorios := 'CodigoEmpresaCobranza <> ' + IntToStr(CodigoEmpresaCobranza) + ' AND ' + Format('EdadDeuda < %d', [FiltroEdadDeLaDeudaMinima]) + FILTRO_Personeria + ' AND CodigoRegion = ' + Quotedstr(FiltroCodigoRegion) + ' AND CodigoComuna = ' + QuotedStr(FiltroCodigoComuna);
        cdsEmpresasCobranza.Filtered := False;
        //Primero busco que cumplan con los 5 filtros
        cdsEmpresasCobranza.Filter := FILTROS_Obligatorios + ' AND MontoMinimo <= ' +  DB_CRUDCommonProcs.FloatToStrWithDecimalPoint(FiltroMontoMinimoDeuda) + ' AND MontoMaximo >=' + DB_CRUDCommonProcs.FloatToStrWithDecimalPoint(FiltroMontoMaximoDeuda);
        cdsEmpresasCobranza.Filtered := True;
        //me fijo si tengo empresas que cumplan con los 5 criterios
        if cdsEmpresasCobranza.RecordCount > 0 then begin
            cdsEmpresasCobranza.First;
            while not cdsEmpresasCobranza.Eof do begin
                cdsEmpresasCobranza.Edit;
                cdsEmpresasCobranza.FieldByName('ElegiblePorMontoMinimo').AsBoolean := True;
                cdsEmpresasCobranza.FieldByName('ElegiblePorMontoMinimoStr').AsString := 'Si';
                cdsEmpresasCobranza.FieldByName('ElegiblePorMontoMaximo').AsBoolean := True;
                cdsEmpresasCobranza.FieldByName('ElegiblePorMontoMaximoStr').AsString := 'Si';
                cdsEmpresasCobranza.Post;
                cdsEmpresasCobranza.Next;
            end;
        end else begin
            //ninguna empresa cumple con los 5 filtros por lo tanto aplico 4 filtros
            cdsEmpresasCobranza.Filtered := False;
            cdsEmpresasCobranza.Filter := FILTROS_Obligatorios + ' AND MontoMinimo >= ' + DB_CRUDCommonProcs.FloatToStrWithDecimalPoint(FiltroMontoMinimoDeuda);
            cdsEmpresasCobranza.Filtered := True;
            //me fijo si tengo empresas que cumplan con los 4 criterios
            if cdsEmpresasCobranza.RecordCount > 0 then begin
                cdsEmpresasCobranza.First;
                while not cdsEmpresasCobranza.Eof do begin
                    cdsEmpresasCobranza.Edit;
                    cdsEmpresasCobranza.FieldByName('ElegiblePorMontoMinimo').AsBoolean := True;
                    cdsEmpresasCobranza.FieldByName('ElegiblePorMontoMinimoStr').AsString := 'Si';
                    cdsEmpresasCobranza.FieldByName('ElegiblePorMontoMaximo').AsBoolean := False;
                    cdsEmpresasCobranza.FieldByName('ElegiblePorMontoMaximoStr').AsString := 'No';
                    cdsEmpresasCobranza.Post;
                    cdsEmpresasCobranza.Next;
                end;
            end else begin
                //Si no hay empresas que cumplan con los 4 filtros entonces uso los 3 obligatorios, si no hay muestro mensaje
                cdsEmpresasCobranza.Filtered := False;
                cdsEmpresasCobranza.Filter := FILTROS_Obligatorios;
                cdsEmpresasCobranza.Filtered := True;
                if cdsEmpresasCobranza.RecordCount > 0 then begin
                    cdsEmpresasCobranza.First;
                    while not cdsEmpresasCobranza.Eof do begin
                        cdsEmpresasCobranza.Edit;
                        cdsEmpresasCobranza.FieldByName('ElegiblePorMontoMinimo').AsBoolean := False;
                        cdsEmpresasCobranza.FieldByName('ElegiblePorMontoMinimoStr').AsString := 'No';
                        cdsEmpresasCobranza.FieldByName('ElegiblePorMontoMaximo').AsBoolean := False;
                        cdsEmpresasCobranza.FieldByName('ElegiblePorMontoMaximoStr').AsString := 'No';
                        cdsEmpresasCobranza.Post;
                        cdsEmpresasCobranza.Next;
                    end;
                end;
            end;
        end;
    finally
        if Assigned(Bookmark) then begin
            cdsConveniosACobranzaExterna.GotoBookmark(Bookmark);
            cdsConveniosACobranzaExterna.FreeBookmark(Bookmark);
        end;
        cdsEmpresasCobranza.EnableControls;
        cdsConveniosACobranzaExterna.EnableControls;
    end;
    lblNoExistenEmpresasDeCobranza.Visible := (CantConvSeleccionados > 0) and (cdsEmpresasCobranza.RecordCount = 0);
    //habilito o no el bot�n de aceptar
    if FEsRecuperacionDeDeuda then begin
        BtnAceptar.Enabled := CantConvSeleccionados > 0;
    end else begin
        BtnAceptar.Enabled := (CantConvSeleccionados > 0) and (cdsEmpresasCobranza.RecordCount > 0);
    end;    
end;

procedure TGestionarCobranzaExternaForm.FormCreate(Sender: TObject);
begin
    FTiposDeOperacionEmpresasCobranza := TVariantItems.Create(nil);
end;

procedure TGestionarCobranzaExternaForm.FormDestroy(Sender: TObject);
begin
    FTiposDeOperacionEmpresasCobranza.Clear;
    FreeAndNil(FTiposDeOperacionEmpresasCobranza);
end;

end.
