object FormVentaDayPassFecha: TFormVentaDayPassFecha
  Left = 409
  Top = 249
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Ingrese la fecha del pase'
  ClientHeight = 117
  ClientWidth = 307
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object lblDescripcion: TLabel
    Left = 16
    Top = 8
    Width = 81
    Height = 13
    Caption = 'lblDescripcion'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 16
    Top = 58
    Width = 31
    Height = 13
    Caption = 'Desde'
  end
  object lblDuracion: TLabel
    Left = 16
    Top = 26
    Width = 65
    Height = 13
    Caption = 'lblDuracion'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblHasta: TLabel
    Left = 159
    Top = 59
    Width = 28
    Height = 13
    Caption = 'lhasta'
  end
  object deDesde: TDateEdit
    Left = 57
    Top = 53
    Width = 90
    Height = 21
    AutoSelect = False
    TabOrder = 0
    OnChange = deDesdeChange
    Date = -693594
  end
  object BtnAceptar: TDPSButton
    Left = 78
    Top = 86
    Width = 79
    Height = 26
    Caption = '&Aceptar'
    Default = True
    ModalResult = 1
    TabOrder = 1
    OnClick = BtnAceptarClick
  end
  object BtnCancelar: TDPSButton
    Left = 166
    Top = 86
    Width = 79
    Height = 26
    Cancel = True
    Caption = 'C&ancelar'
    ModalResult = 2
    TabOrder = 2
    OnClick = BtnCancelarClick
  end
  object qry_MaestroDayPassPatente: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'xPatente'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'xFechaDesde'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'xEstado'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end
      item
        Name = 'xFechaExcluida'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select * from maestroDayPass'
      'where Patente = :xPatente'
      '   and FechaDesde >= :xFechaDesde'
      '   and Estado = :xEstado'
      '   and FechaDesde <> :xFechaExcluida'
      'order by FechaDesde')
    Left = 272
    Top = 9
  end
end
