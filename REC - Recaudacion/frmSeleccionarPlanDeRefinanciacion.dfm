object SeleccionarPlanDeRefinanciacionForm: TSeleccionarPlanDeRefinanciacionForm
  Left = 0
  Top = 0
  ActiveControl = DBListDatos
  BorderStyle = bsSingle
  Caption = 'Seleccionar plan de refinanciaci'#243'n'
  ClientHeight = 443
  ClientWidth = 917
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object GrupoDatos: TPanel
    Left = 0
    Top = 317
    Width = 917
    Height = 87
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    TabOrder = 0
    object lblNombre: TLabel
      Left = 11
      Top = 17
      Width = 40
      Height = 13
      Caption = 'Nombre:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lblCantidadDeCuotas: TLabel
      Left = 12
      Top = 53
      Width = 111
      Height = 13
      Alignment = taRightJustify
      Caption = 'Cantidad de cuotas:'
      FocusControl = txtCantidadDeCuotas
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblPorcentajeCuotaPie: TLabel
      Left = 202
      Top = 53
      Width = 101
      Height = 13
      Caption = 'Porcentaje cuota pie:'
      FocusControl = txtPorcentajeCuotaPie
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label1: TLabel
      Left = 381
      Top = 125
      Width = 10
      Height = 13
      Caption = '%'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 366
      Top = 53
      Width = 8
      Height = 13
      Caption = '%'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object txtNombre: TLabel
      Left = 57
      Top = 17
      Width = 61
      Height = 13
      Caption = 'txtNombre'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object txtCantidadDeCuotas: TNumericEdit
      Left = 129
      Top = 50
      Width = 50
      Height = 21
      Color = 16444382
      TabOrder = 0
      BlankWhenZero = False
    end
    object txtPorcentajeCuotaPie: TNumericEdit
      Left = 310
      Top = 50
      Width = 50
      Height = 21
      TabOrder = 1
      Decimals = 2
      BlankWhenZero = False
    end
  end
  object PAbajo: TPanel
    Left = 0
    Top = 404
    Width = 917
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Panel1: TPanel
      Left = 722
      Top = 0
      Width = 195
      Height = 39
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object BtnAceptar: TButton
        Left = 13
        Top = 6
        Width = 79
        Height = 26
        Caption = '&Aceptar'
        Default = True
        TabOrder = 0
        OnClick = BtnAceptarClick
      end
      object BtnCancelar: TButton
        Left = 98
        Top = 6
        Width = 79
        Height = 26
        Cancel = True
        Caption = '&Cancelar'
        TabOrder = 1
        OnClick = BtnCancelarClick
      end
    end
  end
  object DBListDatos: TDBListEx
    Left = 0
    Top = 0
    Width = 917
    Height = 317
    Align = alClient
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        MinWidth = 150
        MaxWidth = 250
        Header.Caption = 'Nombre'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = [fsBold]
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'Nombre'
      end
      item
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        MinWidth = 150
        MaxWidth = 250
        Header.Caption = 'Cantidad de cuotas'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = [fsBold]
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'CantidadDeCuotas'
      end
      item
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        MinWidth = 150
        MaxWidth = 250
        Header.Caption = 'Porcentaje cuota pie'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = [fsBold]
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'PorcentajeCuotaPie'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 180
        MinWidth = 180
        MaxWidth = 250
        Header.Caption = 'Elegible por personer'#237'a'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = [fsBold]
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'CumpleConPersoneriaStr'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 200
        MinWidth = 200
        MaxWidth = 250
        Header.Caption = 'Elegible por monto m'#237'nimo'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = [fsBold]
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'CumpleConMontoMinimoStr'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 200
        MinWidth = 200
        MaxWidth = 250
        Header.Caption = 'Elegible por monto m'#225'nimo'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = [fsBold]
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'CumpleConMontoMaximoStr'
      end>
    DataSource = DataSource1
    DragReorder = True
    ParentColor = False
    TabOrder = 2
    TabStop = True
  end
  object spListar: TADOStoredProc
    Connection = DMConnections.BaseCAC
    AfterScroll = spListarAfterScroll
    ProcedureName = 'PlanesDeRefinanciacion_ListarParaSeleccion'
    Parameters = <>
    Left = 112
    Top = 104
  end
  object DataSource1: TDataSource
    DataSet = spListar
    Left = 144
    Top = 104
  end
  object spRefinanciacionesObtenerFechaVenCuotasParaPlanDeConvenio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'RefinanciacionesObtenerFechaVenCuotasParaPlanDeConvenio'
    Parameters = <>
    Left = 560
    Top = 360
  end
end
