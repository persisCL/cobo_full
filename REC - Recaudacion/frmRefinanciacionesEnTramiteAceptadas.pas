unit frmRefinanciacionesEnTramiteAceptadas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,

  DMConnection, SysUtilsCN, CobranzasResources,								//SS_1051_PDO

  Dialogs, StdCtrls, ListBoxEx, DBListEx, ExtCtrls, DB, ADODB, ImgList;

type
  TRefinanciacionesEnTramiteAceptadasForm = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Image1: TImage;
    DBListEx1: TDBListEx;
    btnCancelar: TButton;
    Label2: TLabel;
    btnNueva: TButton;
    dsEnTramiteAceptadas: TDataSource;
    spObtenerRefinanciacionesConsultas: TADOStoredProc;
    lnCheck: TImageList;
    btnNuevaUnificada: TButton;															//SS_1051_PDO
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure DBListEx1DrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
    procedure dsEnTramiteAceptadasDataChange(Sender: TObject; Field: TField);			//SS_1051_PDO
    procedure btnNuevaUnificadaClick(Sender: TObject);									//SS_1051_PDO
    procedure spObtenerRefinanciacionesConsultasAfterOpen(DataSet: TDataSet);			//SS_1051_PDO
  private
    { Private declarations }
  public
    { Public declarations }
    FCodigoRefinanciacionUnificada: Integer;    //SS_1051_PDO
    function inicializar(pRut, pNombre: string): boolean;
  end;

var
  RefinanciacionesEnTramiteAceptadasForm: TRefinanciacionesEnTramiteAceptadasForm;

implementation

{$R *.dfm}

procedure TRefinanciacionesEnTramiteAceptadasForm.btnNuevaUnificadaClick(													//SS_1051_PDO
  Sender: TObject);																											//SS_1051_PDO
begin																														//SS_1051_PDO
    FCodigoRefinanciacionUnificada := spObtenerRefinanciacionesConsultas.FieldByName('CodigoRefinanciacion').AsInteger;   	//SS_1051_PDO
end;																														//SS_1051_PDO

procedure TRefinanciacionesEnTramiteAceptadasForm.DBListEx1DrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
var
    bmp: TBitMap;
begin
    with Sender do begin
        if (Column.FieldName = 'Validada') then try
            Text := '';
            Canvas.FillRect(Rect);
            bmp := TBitMap.Create;
			DefaultDraw := False;

            if (spObtenerRefinanciacionesConsultas.FieldByName(Column.FieldName).AsBoolean ) then
                lnCheck.GetBitmap(0, Bmp)
            else
                lnCheck.GetBitmap(1, Bmp);

            Canvas.Draw((Rect.Left + Rect.Right - Bmp.Width) div 2, Rect.Top, bmp);
        finally
            bmp.Free;
        end; // finally
    end; // with
end;

//BEGIN : SS_1051_PDO -------------------------------------------------------------------------------------------
procedure TRefinanciacionesEnTramiteAceptadasForm.dsEnTramiteAceptadasDataChange(Sender: TObject; Field: TField);
    var                                                                                                                                    // PDO
       ActivarNuevaUnificada: Boolean;                                                                                                     // PDO
begin
    ActivarNuevaUnificada := False;                                                                                                        // PDO
                                                                                                                                           // PDO
    with spObtenerRefinanciacionesConsultas do begin                                                                                       // PDO
        if FieldByName('Validada').AsBoolean then begin                                                                                    // PDO
            if FieldByName('CodigoTipoMedioPago').AsInteger = CANAL_PAGO_AVENIMIENTO then begin                                            // PDO
                if ((FieldByName('CodigoRefinanciacionUnificada').AsInteger = 0) or                                                         // PDO
                    (FieldByName('CodigoRefinanciacionUnificada').AsInteger = FieldByName('CodigoRefinanciacion').AsInteger)) and
                    (FieldByName('CodigoEstado').AsInteger = REFINANCIACION_ESTADO_EN_TRAMITE) then begin                                   // PDO
                    ActivarNuevaUnificada := True;                                                                                         // PDO
                end;                                                                                                                       // PDO
            end;                                                                                                                           // PDO
        end;                                                                                                                               // PDO
    end;                                                                                                                                   // PDO
                                                                                                                                           // PDO
    btnNuevaUnificada.Enabled := ActivarNuevaUnificada;                                                                                // PDO
end;
//END : SS_1051_PDO -------------------------------------------------------------------------------------------

procedure TRefinanciacionesEnTramiteAceptadasForm.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
    case key of
        VK_ESCAPE: ModalResult := mrCancel;
    end;
end;

function TRefinanciacionesEnTramiteAceptadasForm.inicializar(pRut, pNombre: string): boolean;
begin
    try
        Result := False;
        CambiarEstadoCursor(CURSOR_RELOJ);

        Caption := Format(Caption, [pRut, pNombre]);
        try
            with spObtenerRefinanciacionesConsultas do begin
                Parameters.ParamByName('@CodigoRefinanciacion').Value := Null;
                Parameters.ParamByName('@CodigoPersona').Value        := Null;
                Parameters.ParamByName('@RUTPersona').Value           := pRut;
                Parameters.ParamByName('@CodigoConvenio').Value       := Null;
                Parameters.ParamByName('@CodigoTipoMedioPago').Value  := Null;
                Parameters.ParamByName('@CodigoEstado').Value         := Null;
                Parameters.ParamByName('@SoloNoActivadas').Value      := 1;

                Open;
            end;

            FCodigoRefinanciacionUnificada := 0;   // SS_1051_PDO

            Result := True;

        except
            on e: Exception do begin
                raise EErrorExceptionCN.Create('Error al inicializar TRefinanciacionesEnTramiteAceptadasForm', 'Error: ' + e.Message);
            end;
        end;
    finally
        CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

//BEGIN : SS_1051_PDO -------------------------------------------------------------------------------------------
procedure TRefinanciacionesEnTramiteAceptadasForm.spObtenerRefinanciacionesConsultasAfterOpen(DataSet: TDataSet);
    var                                                                                                                                    // PDO
       ActivarNuevaUnificada: Boolean;                                                                                                     // PDO
begin
    ActivarNuevaUnificada := False;                                                                                                        // PDO
                                                                                                                                           // PDO
    with spObtenerRefinanciacionesConsultas do begin                                                                                       // PDO
        if FieldByName('Validada').AsBoolean then begin                                                                                    // PDO
            if FieldByName('CodigoTipoMedioPago').AsInteger = CANAL_PAGO_AVENIMIENTO then begin                                            // PDO
                if ((FieldByName('CodigoRefinanciacionUnificada').AsInteger = 0) or                                                         // PDO
                    (FieldByName('CodigoRefinanciacionUnificada').AsInteger = FieldByName('CodigoRefinanciacion').AsInteger)) and
                    (FieldByName('CodigoEstado').AsInteger = REFINANCIACION_ESTADO_EN_TRAMITE) then begin                                   // PDO
                    ActivarNuevaUnificada := True;                                                                                         // PDO
                end;                                                                                                                       // PDO
            end;                                                                                                                           // PDO
        end;                                                                                                                               // PDO
    end;                                                                                                                                   // PDO
                                                                                                                                           // PDO
    btnNuevaUnificada.Enabled := ActivarNuevaUnificada;
end;
//END : SS_1051_PDO -----------------------------------------------------------------------------------------

end.
