unit frmRefinanciacionImpresion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,

  DMConnection, Util, PeaTypes, PeaProcs, PeaProcsCN, SysUtilsCN, Convenios, frmRefinanciacionEditarTelefonos,
  ConstParametrosGenerales,
  ShellApi, frmMuestraMensaje, UtilDB, CobranzasResources, frmMuestraPDF, CobranzasClasses,

  Dialogs, DB, ADODB;

type
  TRefinanciacionImpresionForm = class(TForm)
    spObtenerRefinanciacionDatosCabecera: TADOStoredProc;
    spObtenerRefinanciacionCuotas: TADOStoredProc;
    spObtenerRefinanciacionComprobantes: TADOStoredProc;
    spObtenerRefinanciacionPatentesPrecautoria: TADOStoredProc;
    spObtenerRefinanciacionDomicilio: TADOStoredProc;
    spObtenerRefinanciacionTelefonos: TADOStoredProc;
    spObtenerTelefonoPersonaSinFormato: TADOStoredProc;
    spObtenerRefinanciacionDocumentoOriginal: TADOStoredProc;
    spObtenerRefinanciacionRepresentantesCN: TADOStoredProc;
  private
    { Private declarations }
    FCodigoCanalPago: Integer;
    FNotificacion: Boolean;
    FPersona: TDatosPersonales;
    FDatosDomicilio: TDatosDomicilio;
    FDatosTelefonos: TDatosTelefonos;
    FPlantilla,
    FPlantillaAnexoCuotas,
    FPlantillaAnexoComprobantes,
    FTextoPlantilla,
    FTextoPlantillaAnexoCuotas,
    FtextoPlantillaAnexoComprobantes,
    FtextoPlantillaAnexoComprobantes2,
    FMarcaAguaTramite,
    FMarcaAguaDocOriginal,
    FMarcaAguaResto: String;
    FMarcaAguaTramiteSize,
    FMArcaAguaDocOriginalSize,
    FMarcaAguaRestoSize: Integer;
    FCodigoRefinanciacionAnterior: Integer;
  public
    { Public declarations }
    function inicializar(CodigoRefinanciacion, CodigoRefinanciacionAnterior, CodigoRefinanciacionUnificada: Integer; DocumentoGenerado: Boolean = False): Boolean; //SS_1051_PDO
    function Imprimir(DevolverDocumento: Boolean = False): TDocumentosImpresosRefinanciacion;
    procedure ImprimirDocumentoGenerado(SinMarcaDeAgua: Boolean = False);
  end;

var
  RefinanciacionImpresionForm: TRefinanciacionImpresionForm;

const
    cCaptionFormPDF = ' %s Nro.: %s, Cliente: %s, Estado: %s';

implementation

{$R *.dfm}

function TRefinanciacionImpresionForm.inicializar(CodigoRefinanciacion, CodigoRefinanciacionAnterior, CodigoRefinanciacionUnificada: Integer; DocumentoGenerado: Boolean = False): Boolean;  //SS_1051_PDO
    var
        AuxDomicilio: TDomiciliosPersona;

    const
        PLANTILLA_REFINAN_AV_PER_NAT_NOTIF = 'PLANTILLA_REFINAN_AV_PER_NAT_NOTIF';
        PLANTILLA_REFINAN_AV_PER_NAT       = 'PLANTILLA_REFINAN_AV_PER_NAT';
        PLANTILLA_REFINAN_AV_PER_JUR_NOTIF = 'PLANTILLA_REFINAN_AV_PER_JUR_NOTIF';
        PLANTILLA_REFINAN_AV_PER_JUR       = 'PLANTILLA_REFINAN_AV_PER_JUR';
        PLANTILLA_REFINAN_REPACTACION      = 'PLANTILLA_REFINAN_REPACTACION';
        PLANTILLA_REFINAN_AV_ANEXO_CUOTAS  = 'PLANTILLA_REFINAN_AV_ANEXO_CUOTAS';
        PLANTILLA_REFINAN_REP_ANEXO_CUOTAS = 'PLANTILLA_REFINAN_REP_ANEXO_CUOTAS';
        PLANTILLA_REFINAN_REP_ANEXO_COMPR  = 'PLANTILLA_REFINAN_REP_ANEXO_COMPR';
        MARCA_AGUA_REFINAN_DOC_ORG         = 'MARCA_AGUA_REFINAN_DOC_ORG';
        MARCA_AGUA_REFINAN_DOC_ORG_SIZE    = 'MARCA_AGUA_REFINAN_DOC_ORG_SIZE';
        MARCA_AGUA_REFINAN_RESTO           = 'MARCA_AGUA_REFINAN_RESTO';
        MARCA_AGUA_REFINAN_RESTO_SIZE      = 'MARCA_AGUA_REFINAN_RESTO_SIZE';
        MARCA_AGUA_REFINAN_TRAMITE         = 'MARCA_AGUA_REFINAN_TRAMITE';
        MARCA_AGUA_REFINAN_TRAMITE_SIZE    = 'MARCA_AGUA_REFINAN_TRAMITE_SIZE';

        cSufijoPlantillaAnexoCuotas        = '_AnCuot';
        cSufijoPlantillaAnexoComprobantes  = '_AnComp';
begin
    try
        Result := False;
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        try
            FCodigoRefinanciacionAnterior := CodigoRefinanciacionAnterior;

            case DocumentoGenerado of
                False: begin
                    TPanelMensajesForm.MuestraMensaje('Cargando Datos Principales ...');
                    with spObtenerRefinanciacionDatosCabecera do begin
                        Parameters.Refresh;
                        Parameters.ParamByName('@CodigoRefinanciacion').Value := CodigoRefinanciacion;
                        Parameters.ParamByName('@CodigoRefinanciacionUnificada').Value := CodigoRefinanciacionUnificada;   //SS_1051_PDO
                        Open;

                        FCodigoCanalPago := FieldByName('CodigoTipoMedioPago').AsInteger;
                        FNotificacion    := FieldByName('Notificacion').AsBoolean;
                        FPersona         := ObtenerDatosPersonales(DMConnections.BaseCAC,'','',FieldByName('CodigoPersona').AsInteger);

                        case FCodigoCanalPago of
                            38: begin // Avenimiento
                                if FPersona.Personeria = PERSONERIA_FISICA then begin
                                    if FNotificacion then begin
                                        ObtenerParametroGeneral(DMConnections.BaseCAC, PLANTILLA_REFINAN_AV_PER_NAT_NOTIF, FPlantilla);
                                    end
                                    else ObtenerParametroGeneral(DMConnections.BaseCAC, PLANTILLA_REFINAN_AV_PER_NAT, FPlantilla);
                                end
                                else begin
                                    if FNotificacion then begin
                                        ObtenerParametroGeneral(DMConnections.BaseCAC, PLANTILLA_REFINAN_AV_PER_JUR_NOTIF, FPlantilla);
                                    end
                                    else ObtenerParametroGeneral(DMConnections.BaseCAC, PLANTILLA_REFINAN_AV_PER_JUR, FPlantilla);
                                end;

                                ObtenerParametroGeneral(DMConnections.BaseCAC, PLANTILLA_REFINAN_AV_ANEXO_CUOTAS, FPlantillaAnexoCuotas);
                            end;

                            36: begin // Repactacion
                                ObtenerParametroGeneral(DMConnections.BaseCAC, PLANTILLA_REFINAN_REPACTACION, FPlantilla);
                                ObtenerParametroGeneral(DMConnections.BaseCAC, PLANTILLA_REFINAN_REP_ANEXO_CUOTAS, FPlantillaAnexoCuotas);
                                ObtenerParametroGeneral(DMConnections.BaseCAC, PLANTILLA_REFINAN_REP_ANEXO_COMPR, FPlantillaAnexoComprobantes);
                            end;
                        end;
                    end;

                    with spObtenerRefinanciacionRepresentantesCN do begin
                        Parameters.Refresh;
                        Parameters.ParamByName('@CodigoDocumento').Value := 'RUT';
                        Parameters.ParamByName('@NumeroDocumento').Value := spObtenerRefinanciacionDatosCabecera.FieldByName('RepLegalCNNumeroDocumento').AsString;
                        Parameters.ParamByName('@CodigoTipoMedioPago').Value := Null;
                        Open;
                    end;

                    TPanelMensajesForm.MuestraMensaje('Cargando Datos Domicilio ...');
                    with spObtenerRefinanciacionDomicilio do begin
                        Parameters.Refresh;
                        Parameters.ParamByName('@CodigoRefinanciacion').Value := CodigoRefinanciacion;
                        Open;

                        if RecordCount = 0 then begin
                            AuxDomicilio    := TDomiciliosPersona.Create(FPersona.CodigoPersona);
                            FDatosDomicilio := AuxDomicilio.DomicilioPrincipal;
                        end
                        else begin
                            with FDatosDomicilio do begin
                                Descripcion         := FieldByName('Calle').AsString;
                                CalleDesnormalizada := FieldByName('Calle').AsString;
                                NumeroCalle         := FieldByName('Numero').AsInteger;
                                NumeroCalleSTR      := FieldByName('Numero').AsString;
                                Piso                := FieldByName('Piso').AsInteger;
                                Dpto                := FieldByName('Dpto').AsString;
                                Detalle             := FieldByName('Detalle').AsString;
                                CodigoPais          := FieldByName('CodigoPais').AsString;
                                CodigoRegion        := FieldByName('CodigoRegion').AsString;
                                CodigoComuna        := FieldByName('CodigoComuna').AsString;
                                CodigoPostal        := FieldByName('CodigoPostal').AsString;
                                CodigoDomicilio     := -1;
                                CodigoCalle         := -1;
                                CodigoTipoCalle     := -1;
                                CodigoSegmento      := -1;
                                CodigoTipoDomicilio := 1;
                            end;
                        end;
                    end;

                    TPanelMensajesForm.MuestraMensaje('Cargando Datos Tel�fono(s) ...');
                    with spObtenerRefinanciacionTelefonos do begin
                        Parameters.Refresh;
                        Parameters.ParamByName('@CodigoRefinanciacion').Value := CodigoRefinanciacion;
                        Open;

                        if RecordCount > 0 then begin
                            while not eof do begin
                                case RecNo of
                                    1: begin
                                        FDatosTelefonos.CodigoArea1 := FieldByName('CodigoArea').AsInteger;
                                        FDatosTelefonos.Telefono1   := FieldByName('NumeroTelefono').AsString;
                                    end;
                                    2: begin
                                        FDatosTelefonos.CodigoArea2 := FieldByName('CodigoArea').AsInteger;
                                        FDatosTelefonos.Telefono2   := FieldByName('NumeroTelefono').AsString;
                                    end;
                                end;

                                Next;
                            end;
                        end
                        else begin
                            with spObtenerTelefonoPersonaSinFormato do try
                                Parameters.Refresh;
                                Parameters.ParamByName('@CodigoPersona').Value := FPersona.CodigoPersona;
                                ExecProc;

                                FDatosTelefonos.CodigoArea1 := iif(Parameters.ParamByName('@CodigoArea').Value = Null, 0, Parameters.ParamByName('@CodigoArea').Value);
                                FDatosTelefonos.Telefono1   := iif(Parameters.ParamByName('@Valor').Value = Null, EmptyStr, Trim(VarToStr(Parameters.ParamByName('@Valor').Value)));
                                FDatosTelefonos.CodigoArea2 := 0;
                            finally
                                if Active then Close;
                            end;
                        end;
                    end;

                    TPanelMensajesForm.MuestraMensaje('Cargando Datos Comprobantes ...');
                    with spObtenerRefinanciacionComprobantes do begin
                        Parameters.Refresh;
                        Parameters.ParamByName('@CodigoRefinanciacion').Value := CodigoRefinanciacion;
                        Open;
                    end;

                    TPanelMensajesForm.MuestraMensaje('Cargando Datos Cuotas ...');
                    with spObtenerRefinanciacionCuotas do begin
                        Parameters.Refresh;
                        Parameters.ParamByName('@CodigoRefinanciacion').Value := CodigoRefinanciacion;
                        Parameters.ParamByName('@CodigoRefinanciacionUnificada').Value := CodigoRefinanciacionUnificada;	//SS_1051_PDO
                        Open;
                    end;

                    TPanelMensajesForm.MuestraMensaje('Cargando Datos Patentes Precautoria ...');
                    with spObtenerRefinanciacionPatentesPrecautoria do begin
                        Parameters.Refresh;
                        Parameters.ParamByName('@CodigoRefinanciacion').Value := CodigoRefinanciacion;
                        Parameters.ParamByName('@CodigoRefinanciacionUnificada').Value := CodigoRefinanciacionUnificada;	//SS_1051_PDO
                        Open;
                    end;

                    case FCodigoCanalPago of
                        38: begin  // Avenimiento
                            if spObtenerRefinanciacionCuotas.RecordCount > 10 then begin
                                FPlantilla := FPlantilla + cSufijoPlantillaAnexoCuotas;
                            end;
                        end;
                        36: begin // Repactaci�n
                            if spObtenerRefinanciacionComprobantes.RecordCount > 15 then begin
                                FPlantilla := FPlantilla + cSufijoPlantillaAnexoComprobantes;
                            end;
                            if spObtenerRefinanciacionCuotas.RecordCount > 10 then begin
                                FPlantilla := FPlantilla + cSufijoPlantillaAnexoCuotas;
                            end;
                        end;
                    end;


                    if not FileExists(FPlantilla + '.rtf') then begin
                        raise EErrorExceptionCN.Create('Impresi�n Refinanciaci�n', 'Plantilla NO Encontrada: ' + FPlantilla + '.rtf');
                    end
                    else begin
                        FTextoPlantilla := FileToString(FPlantilla + '.rtf', fmShareDenyNone);
                    end;

                    if not FileExists(FPlantillaAnexoCuotas + '.rtf') then begin
                        raise EErrorExceptionCN.Create('Impresi�n Refinanciaci�n', 'Plantilla NO Encontrada: ' + FPlantillaAnexoCuotas + '.rtf');
                    end
                    else begin
                        FTextoPlantillaAnexoCuotas := FileToString(FPlantillaAnexoCuotas + '.rtf', fmShareDenyNone);
                    end;

                    if FCodigoCanalPago = 36 then begin
                        if not FileExists(FPlantillaAnexoComprobantes + '.rtf') then begin
                            raise EErrorExceptionCN.Create('Impresi�n Refinanciaci�n', 'Plantilla NO Encontrada: ' + FPlantillaAnexoComprobantes + '.rtf');
                        end
                        else begin
                            FtextoPlantillaAnexoComprobantes  := FileToString(FPlantillaAnexoComprobantes + '.rtf', fmShareDenyNone);
                            FtextoPlantillaAnexoComprobantes2 := FileToString(FPlantillaAnexoComprobantes + '.rtf', fmShareDenyNone);
                        end;
                    end
                    else begin
                        FtextoPlantillaAnexoComprobantes  := EmptyStr;
                        FtextoPlantillaAnexoComprobantes2 := EmptyStr;
                    end;
                end;

                True: begin
                    TPanelMensajesForm.MuestraMensaje('Cargando Documento Original ...');
                    with spObtenerRefinanciacionDocumentoOriginal do begin
                        if Active then Close;
                        Parameters.Refresh;
                        Parameters.ParamByName('@CodigoRefinanciacion').Value := CodigoRefinanciacion;
                        Open;

                        FPersona := ObtenerDatosPersonales(DMConnections.BaseCAC,'','',FieldByName('CodigoPersona').AsInteger);
                    end;
                end;
            end;

            TPanelMensajesForm.MuestraMensaje('Cargando Marcas de Agua para la Impresi�n ...');
            if not ObtenerParametroGeneral(DMConnections.BaseCAC, MARCA_AGUA_REFINAN_DOC_ORG, FMArcaAguaDocOriginal) then begin
                FMarcaAguaDocOriginal := 'Copia Original';
            end;

            if not ObtenerParametroGeneral(DMConnections.BaseCAC, MARCA_AGUA_REFINAN_DOC_ORG, FMArcaAguaDocOriginalSize) then begin
                FMArcaAguaDocOriginalSize := 108;
            end;

            if not ObtenerParametroGeneral(DMConnections.BaseCAC, MARCA_AGUA_REFINAN_TRAMITE, FMarcaAguaTramite) then begin
                FMarcaAguaTramite := 'Borrador';
            end;

            if not ObtenerParametroGeneral(DMConnections.BaseCAC, MARCA_AGUA_REFINAN_TRAMITE_SIZE, FMarcaAguaTramiteSize) then begin
                FMarcaAguaTramiteSize := 144;
            end;

            if not ObtenerParametroGeneral(DMConnections.BaseCAC, MARCA_AGUA_REFINAN_RESTO, FMarcaAguaResto) then begin
                FMarcaAguaResto := 'Copia NO V�lida';
            end;

            if not ObtenerParametroGeneral(DMConnections.BaseCAC, MARCA_AGUA_REFINAN_RESTO_SIZE, FMarcaAguaRestoSize) then begin
                FMarcaAguaRestoSize := 108;
            end;

            Result := True;
        except
            on e:Exception do begin
                TPanelMensajesForm.OcultaPanel;
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
        if Assigned(AuxDomicilio) then FreeAndNil(AuxDomicilio);    
        Screen.Cursor := crDefault;
    end;
end;

function TRefinanciacionImpresionForm.Imprimir(DevolverDocumento: Boolean = False): TDocumentosImpresosRefinanciacion;
    const
        SQLFormatearRUT             = 'SELECT dbo.FormatearRutConPuntos(''%s'')';
        SQLObtenerNumeroConvenio    = 'SELECT dbo.ObtenerNumeroConvenio(%d)';
        SQLObtenerDescripcionComuna = 'SELECT dbo.ObtenerDescripcionComuna(''%s'', ''%s'', ''%s'')';

        FormatoTelefono = '(%d) %s';
        TAG_MEDIO       = '#MEDIO_%s#';
        TAG_DOCUMENTO   = '#DOC_%s#';
        TAG_FECHA_VTO   = '#CUOTA_VTO_%s#';
        TAG_CUOTA       = '#CUOTA_%s#';

        TAG_CONVENIO    = '#CONVENIO_%s#';
        TAG_NOTA_COBRO  = '#NOTA_%s#';
        TAG_MONTO_NOTA  = '#MONT_%s#';
        TAG_VTO_NOTA    = '#VTO_%s#';

        ContinuaEnAnexo = 'Continua en Anexo N� %d';

    var
        FechaRefinanciacionLarga,
        FechaRefinanciacionCorta,
        TratoCli,
        NombreCli,
        RUTFormateado,
        NombreRepLegCli,
        NombreRepLegCN,
        Direccion,
        Comuna,
        Telefonos,
        CausaRol,
        ImporteDeuda,
        ImporteDemanda,
        Etiqueta,
        ValorCelda,
        Patentes,
        NumeroConvenio,
        JPLComuna,
        JPLNumero,
        TempFileName,
        TextoMarcaAgua,
        IDImpresion,
        EmpresaRecaudadoraNombre,
        EmpresaRecaudadoraDireccion,
        EmpresaRecaudadoraComuna,
        FicherosMergePDF: String;
        Contador,
        TextoMarcaAguaSize: Integer;
        PDFSize: Int64;
begin
    try
        Result.DocumentoPrincipal  := EmptyStr;
        Result.AnexoCuotas         := EmptyStr;
        Result.AnexoComprobantes   := EmptyStr;
        Result.AnexoComprobantes2  := EmptyStr;

        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        TPanelMensajesForm.MuestraMensaje('Imprimiendo Documento, Datos Generales ...');

        try
            FechaRefinanciacionLarga := FormatDateTime('dddddd', spObtenerRefinanciacionDatosCabecera.FieldByName('Fecha').AsDateTime);
            FechaRefinanciacionCorta := FormatDateTime('dd/mm/yyy', spObtenerRefinanciacionDatosCabecera.FieldByName('Fecha').AsDateTime);

            if FPersona.Personeria = PERSONERIA_FISICA then begin
                if FPersona.Sexo = SEXO_MASCULINO then begin
                    TratoCli := 'Don';
                end
                else if FPersona.Sexo = SEXO_FEMENINO then begin
                    TratoCli := 'Do�a';
                end
                else begin
                    TratoCli := 'Don/Do�a';
                end;

                NombreCli := Trim(Trim(FPersona.Nombre) + ' ' + Trim(FPersona.Apellido) + ' ' + Trim(FPersona.ApellidoMaterno));
            end
            else begin
                TratoCli  := 'La Empresa';
                NombreCli := FPersona.RazonSocial;
            end;

            RUTFormateado := QueryGetValue(DMConnections.BaseCAC, Format(SQLFormatearRUT, [FPersona.NumeroDocumento]));

            Direccion     :=
                Trim(FDatosDomicilio.Descripcion) + ' # ' +
                Trim(FDatosDomicilio.NumeroCalleSTR);

            Comuna        :=
                BuscarDescripcionComuna(
                    DMConnections.BaseCAC,
                    FDatosDomicilio.CodigoPais,
                    FDatosDomicilio.CodigoRegion,
                    FDatosDomicilio.CodigoComuna);

            Telefonos     :=
                Format(FormatoTelefono, [FDatosTelefonos.CodigoArea1, FDatosTelefonos.Telefono1]) +
                iif(Trim(FDatosTelefonos.Telefono2) <> EmptyStr, ', ' + Format(FormatoTelefono, [FDatosTelefonos.CodigoArea2, FDatosTelefonos.Telefono2]), '');

            NombreRepLegCN :=
                Trim(
                    Trim(spObtenerRefinanciacionDatosCabecera.FieldByName('RepLegalCNNombre').AsString) + ' ' +
                    Trim(spObtenerRefinanciacionDatosCabecera.FieldByName('RepLegalCNApellido').AsString) + ' ' +
                    Trim(spObtenerRefinanciacionDatosCabecera.FieldByName('RepLegalCNApellidoMaterno').AsString));

            ImporteDeuda   := FormatFloat(FORMATO_IMPORTE, spObtenerRefinanciacionDatosCabecera.FieldByName('TotalDeuda').AsFloat);
            ImporteDemanda := FormatFloat(FORMATO_IMPORTE, spObtenerRefinanciacionDatosCabecera.FieldByName('TotalDemanda').AsFloat);
            IDImpresion    := iif(spObtenerRefinanciacionDatosCabecera.FieldByName('CodigoEstado').AsInteger = REFINANCIACION_ESTADO_ACEPTADA, spObtenerRefinanciacionDatosCabecera.FieldByName('IDImpresion').AsString, EmptyStr);

            FTextoPlantilla := StringReplace(FTextoPlantilla, '#NOM_CLI#', NombreCli, [rfReplaceAll, rfIgnoreCase]);
            FTextoPlantilla := StringReplace(FTextoPlantilla, '#RUT_CLI#', RUTFormateado, [rfReplaceAll, rfIgnoreCase]);
            FTextoPlantilla := StringReplace(FTextoPlantilla, '#DIRECCION#', Direccion, [rfReplaceAll, rfIgnoreCase]);
            FTextoPlantilla := StringReplace(FTextoPlantilla, '#COMUNA#', Comuna, [rfReplaceAll, rfIgnoreCase]);
            FTextoPlantilla := StringReplace(FTextoPlantilla, '#TELEFONOS#', Telefonos, [rfReplaceAll, rfIgnoreCase]);
            FTextoPlantilla := StringReplace(FTextoPlantilla, '#NOM_REP_LEG_CN#', NombreRepLegCN, [rfReplaceAll, rfIgnoreCase]);
            FTextoPlantilla := StringReplace(FTextoPlantilla, '#IMP_DEUDA#', ImporteDeuda, [rfReplaceAll, rfIgnoreCase]);
            FTextoPlantilla := StringReplace(FTextoPlantilla, '#IMP_DEMANDA#', ImporteDemanda, [rfReplaceAll, rfIgnoreCase]);
            FTextoPlantilla := StringReplace(FTextoPlantilla, '#IDIMPRESION#', IDImpresion, [rfReplaceAll, rfIgnoreCase]);

            FTextoPlantillaAnexoCuotas        := StringReplace(FTextoPlantillaAnexoCuotas, '#IDIMPRESION#', IDImpresion, [rfReplaceAll, rfIgnoreCase]);
            FtextoPlantillaAnexoComprobantes  := StringReplace(FtextoPlantillaAnexoComprobantes, '#IDIMPRESION#', IDImpresion, [rfReplaceAll, rfIgnoreCase]);
            FtextoPlantillaAnexoComprobantes2 := StringReplace(FtextoPlantillaAnexoComprobantes, '#IDIMPRESION#', IDImpresion, [rfReplaceAll, rfIgnoreCase]);


            TPanelMensajesForm.MuestraMensaje('Imprimiendo Documento, Datos Cuotas ...');
            with spObtenerRefinanciacionCuotas do begin
                First;

                while not Eof do begin
                    case RecordCount of
                        1..10: begin
                            Etiqueta        := Format(TAG_MEDIO, [FormatFloat('0#', Recno)]);
                            ValorCelda      := FieldByName('DescripcionFormaPago').AsString;
                            FTextoPlantilla := StringReplace(FTextoPlantilla, Etiqueta, ValorCelda, [rfReplaceAll, rfIgnoreCase]);

                            Etiqueta        := Format(TAG_DOCUMENTO, [FormatFloat('0#', Recno)]);
                            ValorCelda      := FieldByName('DocumentoNumero').AsString;
                            FTextoPlantilla := StringReplace(FTextoPlantilla, Etiqueta, ValorCelda, [rfReplaceAll, rfIgnoreCase]);

                            Etiqueta        := Format(TAG_FECHA_VTO, [FormatFloat('0#', Recno)]);
                            ValorCelda      := FormatDateTime('dd/mm/yyyy', FieldByName('FechaCuota').AsDateTime);
                            FTextoPlantilla := StringReplace(FTextoPlantilla, Etiqueta, ValorCelda, [rfReplaceAll, rfIgnoreCase]);

                            Etiqueta        := Format(TAG_CUOTA, [FormatFloat('0#', Recno)]);
                            ValorCelda      := FormatFloat(FORMATO_IMPORTE, FieldByName('Importe').AsFloat);
                            FTextoPlantilla := StringReplace(FTextoPlantilla, Etiqueta, ValorCelda, [rfReplaceAll, rfIgnoreCase]);
                        end;
                    else
                        Etiqueta                   := Format(TAG_MEDIO, [FormatFloat('0#', Recno)]);
                        ValorCelda                 := FieldByName('DescripcionFormaPago').AsString;
                        FTextoPlantillaAnexoCuotas := StringReplace(FTextoPlantillaAnexoCuotas, Etiqueta, ValorCelda, [rfReplaceAll, rfIgnoreCase]);

                        Etiqueta                   := Format(TAG_DOCUMENTO, [FormatFloat('0#', Recno)]);
                        ValorCelda                 := FieldByName('DocumentoNumero').AsString;
                        FTextoPlantillaAnexoCuotas := StringReplace(FTextoPlantillaAnexoCuotas, Etiqueta, ValorCelda, [rfReplaceAll, rfIgnoreCase]);

                        Etiqueta                   := Format(TAG_FECHA_VTO, [FormatFloat('0#', Recno)]);
                        ValorCelda                 := FormatDateTime('dd/mm/yyyy', FieldByName('FechaCuota').AsDateTime);
                        FTextoPlantillaAnexoCuotas := StringReplace(FTextoPlantillaAnexoCuotas, Etiqueta, ValorCelda, [rfReplaceAll, rfIgnoreCase]);

                        Etiqueta                   := Format(TAG_CUOTA, [FormatFloat('0#', Recno)]);
                        ValorCelda                 := FormatFloat(FORMATO_IMPORTE, FieldByName('Importe').AsFloat);
                        FTextoPlantillaAnexoCuotas := StringReplace(FTextoPlantillaAnexoCuotas, Etiqueta, ValorCelda, [rfReplaceAll, rfIgnoreCase]);
                    end;

                    Next;
                end;

                if RecordCount < 11 then begin
                    for Contador := RecordCount + 1 to 10 do begin
                        Etiqueta        := Format(TAG_MEDIO, [FormatFloat('0#', Contador)]);
                        ValorCelda      := EmptyStr;
                        FTextoPlantilla := StringReplace(FTextoPlantilla, Etiqueta, ValorCelda, [rfReplaceAll, rfIgnoreCase]);

                        Etiqueta        := Format(TAG_DOCUMENTO, [FormatFloat('0#', Contador)]);
                        ValorCelda      := EmptyStr;
                        FTextoPlantilla := StringReplace(FTextoPlantilla, Etiqueta, ValorCelda, [rfReplaceAll, rfIgnoreCase]);

                        Etiqueta        := Format(TAG_FECHA_VTO, [FormatFloat('0#', Contador)]);
                        ValorCelda      := EmptyStr;
                        FTextoPlantilla := StringReplace(FTextoPlantilla, Etiqueta, ValorCelda, [rfReplaceAll, rfIgnoreCase]);

                        Etiqueta        := Format(TAG_CUOTA, [FormatFloat('0#', Contador)]);
                        ValorCelda      := EmptyStr;
                        FTextoPlantilla := StringReplace(FTextoPlantilla, Etiqueta, ValorCelda, [rfReplaceAll, rfIgnoreCase]);
                    end;
                end
                else begin
                    for Contador := (RecordCount + 1) to 45 do begin
                        Etiqueta                   := Format(TAG_MEDIO, [FormatFloat('0#', Contador)]);
                        ValorCelda                 := EmptyStr;
                        FTextoPlantillaAnexoCuotas := StringReplace(FTextoPlantillaAnexoCuotas, Etiqueta, ValorCelda, [rfReplaceAll, rfIgnoreCase]);

                        Etiqueta                   := Format(TAG_DOCUMENTO, [FormatFloat('0#', Contador)]);
                        ValorCelda                 := EmptyStr;
                        FTextoPlantillaAnexoCuotas := StringReplace(FTextoPlantillaAnexoCuotas, Etiqueta, ValorCelda, [rfReplaceAll, rfIgnoreCase]);

                        Etiqueta                   := Format(TAG_FECHA_VTO, [FormatFloat('0#', Contador)]);
                        ValorCelda                 := EmptyStr;
                        FTextoPlantillaAnexoCuotas := StringReplace(FTextoPlantillaAnexoCuotas, Etiqueta, ValorCelda, [rfReplaceAll, rfIgnoreCase]);

                        Etiqueta                   := Format(TAG_CUOTA, [FormatFloat('0#', Contador)]);
                        ValorCelda                 := EmptyStr;
                        FTextoPlantillaAnexoCuotas := StringReplace(FTextoPlantillaAnexoCuotas, Etiqueta, ValorCelda, [rfReplaceAll, rfIgnoreCase]);
                    end;

                end;
            end;

            TPanelMensajesForm.MuestraMensaje('Imprimiendo Documento, Datos Patentes en Precautoria ...');
            with spObtenerRefinanciacionPatentesPrecautoria do begin
                First;

                Patentes := EmptyStr;

                while not Eof do begin
                    case RecNo of
                        1: begin
                            Patentes := Trim(FieldByName('Patente').AsString);
                        end;
                    else
                        if (Recno = Recordcount) then begin
                            Patentes := Patentes + ' y ' + Trim(FieldByName('Patente').AsString);
                        end
                        else begin
                            Patentes := Patentes + ', ' + Trim(FieldByName('Patente').AsString);
                        end;
                    end;

                    Next;
                end;

                FTextoPlantilla := StringReplace(FTextoPlantilla, '#PATENTES#', iif(Patentes <> EmptyStr, Patentes, ' '), [rfReplaceAll, rfIgnoreCase]);
            end;

            case FCodigoCanalPago of
                38: begin // Avenimiento
                    TPanelMensajesForm.MuestraMensaje('Imprimiendo Documento, Datos Avenimiento ...');

                    EmpresaRecaudadoraNombre    := spObtenerRefinanciacionRepresentantesCN.FieldByName('Empresa').AsString;
                    EmpresaRecaudadoraDireccion := spObtenerRefinanciacionRepresentantesCN.FieldByName('Direccion').AsString;
                    EmpresaRecaudadoraComuna    := Trim(spObtenerRefinanciacionRepresentantesCN.FieldByName('DescripcionComuna').AsString);

                    FTextoPlantilla := StringReplace(FTextoPlantilla, '#NOM_EMP_REC#', EmpresaRecaudadoraNombre, [rfReplaceAll, rfIgnoreCase]);
                    FTextoPlantilla := StringReplace(FTextoPlantilla, '#DIR_EMP_REC#', EmpresaRecaudadoraDireccion, [rfReplaceAll, rfIgnoreCase]);
                    FTextoPlantilla := StringReplace(FTextoPlantilla, '#COMUNA_EMP_REC#', EmpresaRecaudadoraComuna, [rfReplaceAll, rfIgnoreCase]);

                    CausaRol        := spObtenerRefinanciacionDatosCabecera.FieldByName('NumeroCausaRol').AsString;
                    JPLComuna       :=
                        QueryGetValue(
                            DMConnections.BaseCAC,
                            Format(SQLObtenerDescripcionComuna,
                                [spObtenerRefinanciacionDatosCabecera.FieldByName('JPLCodigoPais').AsString,
                                 spObtenerRefinanciacionDatosCabecera.FieldByName('JPLCodigoRegion').AsString,
                                 spObtenerRefinanciacionDatosCabecera.FieldByName('JPLCodigoComuna').AsString]));
                    JPLNumero := iif(spObtenerRefinanciacionDatosCabecera.FieldByName('JPLNumero').AsInteger > 0, spObtenerRefinanciacionDatosCabecera.FieldByName('JPLNumero').AsString + ' �', EmptyStr);

                    FTextoPlantilla := StringReplace(FTextoPlantilla, '#JPL_NRO#', JPLNumero, [rfReplaceAll, rfIgnoreCase]);
                    FTextoPlantilla := StringReplace(FTextoPlantilla, '#JPL_COMUNA#', JPLComuna, [rfReplaceAll, rfIgnoreCase]);
                    FTextoPlantilla := StringReplace(FTextoPlantilla, '#CAUSA_ROL#', CausaRol, [rfReplaceAll, rfIgnoreCase]);
                    FTextoPlantilla := StringReplace(FTextoPlantilla, '#FECHA#', FechaRefinanciacionLarga, [rfReplaceAll, rfIgnoreCase]);

                    if FPersona.Personeria = PERSONERIA_JURIDICA then begin
                        NombreRepLegCli :=
                            Trim(
                                Trim(spObtenerRefinanciacionDatosCabecera.FieldByName('RepLegalCliNombre').AsString) + ' ' +
                                Trim(spObtenerRefinanciacionDatosCabecera.FieldByName('RepLegalCliApellido').AsString) + ' ' +
                                Trim(spObtenerRefinanciacionDatosCabecera.FieldByName('RepLegalCliApellidoMaterno').AsString));

                        RUTFormateado := QueryGetValue(DMConnections.BaseCAC, Format(SQLFormatearRUT, [spObtenerRefinanciacionDatosCabecera.FieldByName('RepLegalCliNumeroDocumento').AsString]));

                        FTextoPlantilla := StringReplace(FTextoPlantilla, '#NOM_REP_LEG_CLI#', NombreRepLegCli, [rfReplaceAll, rfIgnoreCase]);
                        FTextoPlantilla := StringReplace(FTextoPlantilla, '#RUT_REP_LEG_CLI#', RUTFormateado, [rfReplaceAll, rfIgnoreCase]);
                    end;
                end;

                36: begin // Repactacion
                    TPanelMensajesForm.MuestraMensaje('Imprimiendo Documento, Datos Repactaci�n ...');

                    NumeroConvenio := Trim(QueryGetValue(DMConnections.BaseCAC, Format(SQLObtenerNumeroConvenio, [spObtenerRefinanciacionDatosCabecera.FieldByName('CodigoConvenio').AsInteger])));
                    NumeroConvenio := FormatearNumeroConvenio(NumeroConvenio);

                    RUTFormateado  := Trim(spObtenerRefinanciacionDatosCabecera.FieldByName('RepLegalCNNumeroDocumento').AsString);
                    if RUTFormateado <> EmptyStr then begin
                        RUTFormateado  := QueryGetValue(DMConnections.BaseCAC, Format(SQLFormatearRUT, [RUTFormateado]))
                    end;

                    FTextoPlantilla := StringReplace(FTextoPlantilla, '#TRATO#', TratoCli, [rfReplaceAll, rfIgnoreCase]);
                    FTextoPlantilla := StringReplace(FTextoPlantilla, '#FECHA_LARGA#', FechaRefinanciacionLarga, [rfReplaceAll, rfIgnoreCase]);
                    FTextoPlantilla := StringReplace(FTextoPlantilla, '#RUT_REP_LEG_CN#', RUTFormateado, [rfReplaceAll, rfIgnoreCase]);

                    with spObtenerRefinanciacionComprobantes do begin
                        First;

                        while not Eof do begin
                            case RecordCount of
                                1..15: begin
                                    Etiqueta        := Format(TAG_CONVENIO, [FormatFloat('0#', Recno)]);
                                    ValorCelda      := NumeroConvenio;
                                    FTextoPlantilla := StringReplace(FTextoPlantilla, Etiqueta, ValorCelda, [rfReplaceAll, rfIgnoreCase]);

                                    Etiqueta        := Format(TAG_NOTA_COBRO, [FormatFloat('0#', Recno)]);
                                    ValorCelda      := FieldByName('DescriComprobante').AsString;
                                    FTextoPlantilla := StringReplace(FTextoPlantilla, Etiqueta, ValorCelda, [rfReplaceAll, rfIgnoreCase]);

                                    Etiqueta        := Format(TAG_MONTO_NOTA, [FormatFloat('0#', Recno)]);
                                    ValorCelda      := FieldByName('SaldoRefinanciadoDescri').AsString;
                                    FTextoPlantilla := StringReplace(FTextoPlantilla, Etiqueta, ValorCelda, [rfReplaceAll, rfIgnoreCase]);

                                    Etiqueta        := Format(TAG_VTO_NOTA, [FormatFloat('0#', Recno)]);
                                    ValorCelda      := FormatDateTime('dd/mm/yyyy', FieldByName('FechaVencimiento').AsDateTime);
                                    FTextoPlantilla := StringReplace(FTextoPlantilla, Etiqueta, ValorCelda, [rfReplaceAll, rfIgnoreCase]);
                                end;

                            else
                                case Recno of
                                    1..45: begin
                                        Etiqueta                         := Format(TAG_CONVENIO, [FormatFloat('0#', Recno)]);
                                        ValorCelda                       := NumeroConvenio;
                                        FtextoPlantillaAnexoComprobantes := StringReplace(FtextoPlantillaAnexoComprobantes, Etiqueta, ValorCelda, [rfReplaceAll, rfIgnoreCase]);

                                        Etiqueta                         := Format(TAG_NOTA_COBRO, [FormatFloat('0#', Recno)]);
                                        ValorCelda                       := FieldByName('DescriComprobante').AsString;
                                        FtextoPlantillaAnexoComprobantes := StringReplace(FtextoPlantillaAnexoComprobantes, Etiqueta, ValorCelda, [rfReplaceAll, rfIgnoreCase]);

                                        Etiqueta                         := Format(TAG_MONTO_NOTA, [FormatFloat('0#', Recno)]);
                                        ValorCelda                       := FieldByName('SaldoRefinanciadoDescri').AsString;
                                        FtextoPlantillaAnexoComprobantes := StringReplace(FtextoPlantillaAnexoComprobantes, Etiqueta, ValorCelda, [rfReplaceAll, rfIgnoreCase]);

                                        Etiqueta                         := Format(TAG_VTO_NOTA, [FormatFloat('0#', Recno)]);
                                        ValorCelda                       := FormatDateTime('dd/mm/yyyy', FieldByName('FechaVencimiento').AsDateTime);
                                        FtextoPlantillaAnexoComprobantes := StringReplace(FtextoPlantillaAnexoComprobantes, Etiqueta, ValorCelda, [rfReplaceAll, rfIgnoreCase]);
                                    end;
                                else
                                    Etiqueta                          := Format(TAG_CONVENIO, [FormatFloat('0#', Recno - 45)]);
                                    ValorCelda                        := NumeroConvenio;
                                    FtextoPlantillaAnexoComprobantes2 := StringReplace(FtextoPlantillaAnexoComprobantes2, Etiqueta, ValorCelda, [rfReplaceAll, rfIgnoreCase]);

                                    Etiqueta                          := Format(TAG_NOTA_COBRO, [FormatFloat('0#', Recno - 45)]);
                                    ValorCelda                        := FieldByName('DescriComprobante').AsString;
                                    FtextoPlantillaAnexoComprobantes2 := StringReplace(FtextoPlantillaAnexoComprobantes2, Etiqueta, ValorCelda, [rfReplaceAll, rfIgnoreCase]);

                                    Etiqueta                          := Format(TAG_MONTO_NOTA, [FormatFloat('0#', Recno - 45)]);
                                    ValorCelda                        := FieldByName('SaldoRefinanciadoDescri').AsString;
                                    FtextoPlantillaAnexoComprobantes2 := StringReplace(FtextoPlantillaAnexoComprobantes2, Etiqueta, ValorCelda, [rfReplaceAll, rfIgnoreCase]);

                                    Etiqueta                          := Format(TAG_VTO_NOTA, [FormatFloat('0#', Recno - 45)]);
                                    ValorCelda                        := FormatDateTime('dd/mm/yyyy', FieldByName('FechaVencimiento').AsDateTime);
                                    FtextoPlantillaAnexoComprobantes2 := StringReplace(FtextoPlantillaAnexoComprobantes2, Etiqueta, ValorCelda, [rfReplaceAll, rfIgnoreCase]);
                                end;
                            end;

                            Next;
                        end;

                        if RecordCount < 16 then begin
                            for Contador := RecordCount + 1 to 15 do begin
                                Etiqueta        := Format(TAG_CONVENIO, [FormatFloat('0#', Contador)]);
                                ValorCelda      := EmptyStr;
                                FTextoPlantilla := StringReplace(FTextoPlantilla, Etiqueta, ValorCelda, [rfReplaceAll, rfIgnoreCase]);

                                Etiqueta        := Format(TAG_NOTA_COBRO, [FormatFloat('0#', Contador)]);
                                ValorCelda      := EmptyStr;
                                FTextoPlantilla := StringReplace(FTextoPlantilla, Etiqueta, ValorCelda, [rfReplaceAll, rfIgnoreCase]);

                                Etiqueta        := Format(TAG_MONTO_NOTA, [FormatFloat('0#', Contador)]);
                                ValorCelda      := EmptyStr;
                                FTextoPlantilla := StringReplace(FTextoPlantilla, Etiqueta, ValorCelda, [rfReplaceAll, rfIgnoreCase]);

                                Etiqueta        := Format(TAG_VTO_NOTA, [FormatFloat('0#', Contador)]);
                                ValorCelda      := EmptyStr;
                                FTextoPlantilla := StringReplace(FTextoPlantilla, Etiqueta, ValorCelda, [rfReplaceAll, rfIgnoreCase]);
                            end;
                        end
                        else begin
                            if RecordCount < 46  then begin
                                for Contador := (RecordCount + 1) to 45 do begin
                                    Etiqueta                         := Format(TAG_CONVENIO, [FormatFloat('0#', Contador)]);
                                    ValorCelda                       := EmptyStr;
                                    FtextoPlantillaAnexoComprobantes := StringReplace(FtextoPlantillaAnexoComprobantes, Etiqueta, ValorCelda, [rfReplaceAll, rfIgnoreCase]);

                                    Etiqueta                         := Format(TAG_NOTA_COBRO, [FormatFloat('0#', Contador)]);
                                    ValorCelda                       := EmptyStr;
                                    FtextoPlantillaAnexoComprobantes := StringReplace(FtextoPlantillaAnexoComprobantes, Etiqueta, ValorCelda, [rfReplaceAll, rfIgnoreCase]);

                                    Etiqueta                         := Format(TAG_MONTO_NOTA, [FormatFloat('0#', Contador)]);
                                    ValorCelda                       := EmptyStr;
                                    FtextoPlantillaAnexoComprobantes := StringReplace(FtextoPlantillaAnexoComprobantes, Etiqueta, ValorCelda, [rfReplaceAll, rfIgnoreCase]);

                                    Etiqueta                         := Format(TAG_VTO_NOTA, [FormatFloat('0#', Contador)]);
                                    ValorCelda                       := EmptyStr;
                                    FtextoPlantillaAnexoComprobantes := StringReplace(FtextoPlantillaAnexoComprobantes, Etiqueta, ValorCelda, [rfReplaceAll, rfIgnoreCase]);
                                end;
                            end
                            else begin
                                for Contador := ((RecordCount + 1) - 45) to 45 do begin
                                    Etiqueta                          := Format(TAG_CONVENIO, [FormatFloat('0#', Contador)]);
                                    ValorCelda                        := EmptyStr;
                                    FtextoPlantillaAnexoComprobantes2 := StringReplace(FtextoPlantillaAnexoComprobantes2, Etiqueta, ValorCelda, [rfReplaceAll, rfIgnoreCase]);

                                    Etiqueta                          := Format(TAG_NOTA_COBRO, [FormatFloat('0#', Contador)]);
                                    ValorCelda                        := EmptyStr;
                                    FtextoPlantillaAnexoComprobantes2 := StringReplace(FtextoPlantillaAnexoComprobantes2, Etiqueta, ValorCelda, [rfReplaceAll, rfIgnoreCase]);

                                    Etiqueta                          := Format(TAG_MONTO_NOTA, [FormatFloat('0#', Contador)]);
                                    ValorCelda                        := EmptyStr;
                                    FtextoPlantillaAnexoComprobantes2 := StringReplace(FtextoPlantillaAnexoComprobantes2, Etiqueta, ValorCelda, [rfReplaceAll, rfIgnoreCase]);

                                    Etiqueta                          := Format(TAG_VTO_NOTA, [FormatFloat('0#', Contador)]);
                                    ValorCelda                        := EmptyStr;
                                    FtextoPlantillaAnexoComprobantes2 := StringReplace(FtextoPlantillaAnexoComprobantes2, Etiqueta, ValorCelda, [rfReplaceAll, rfIgnoreCase]);
                                end;
                            end;
                        end;
                    end;

                    if spObtenerRefinanciacionDatosCabecera.FieldByName('GastoNotarial').AsInteger > 0 then begin
                        FTextoPlantilla := StringReplace(FTextoPlantilla, '#GASTO_MEDIO#', 'Efectivo', [rfReplaceAll, rfIgnoreCase]);
                        FTextoPlantilla := StringReplace(FTextoPlantilla, '#FECHA_CORTA#', FechaRefinanciacionCorta, [rfReplaceAll, rfIgnoreCase]);
                        FTextoPlantilla := StringReplace(FTextoPlantilla, '#GASTO_IMP#', FormatFloat('$ #,##0', spObtenerRefinanciacionDatosCabecera.FieldByName('GastoNotarial').AsInteger), [rfReplaceAll, rfIgnoreCase]);
                    end
                    else begin
                        FTextoPlantilla := StringReplace(FTextoPlantilla, '#GASTO_MEDIO#', EmptyStr, [rfReplaceAll, rfIgnoreCase]);
                        FTextoPlantilla := StringReplace(FTextoPlantilla, '#FECHA_CORTA#', EmptyStr, [rfReplaceAll, rfIgnoreCase]);
                        FTextoPlantilla := StringReplace(FTextoPlantilla, '#GASTO_IMP#', EmptyStr, [rfReplaceAll, rfIgnoreCase]);
                    end;

                end;
            end;


            if DevolverDocumento then begin
                Result.DocumentoPrincipal  := FTextoPlantilla;
                Result.AnexoCuotas         := iif(spObtenerRefinanciacionCuotas.RecordCount > 10, FTextoPlantillaAnexoCuotas, EmptyStr);
                Result.AnexoComprobantes   := iif(FCodigoCanalPago = 36, iif(spObtenerRefinanciacionComprobantes.RecordCount > 15, FtextoPlantillaAnexoComprobantes, EmptyStr), EmptyStr);
                Result.AnexoComprobantes2  := iif(FCodigoCanalPago = 36, iif(spObtenerRefinanciacionComprobantes.RecordCount > 45, FtextoPlantillaAnexoComprobantes2, EmptyStr), EmptyStr);
            end
            else begin
                TPanelMensajesForm.MuestraMensaje('Grabando Documento Temporal ...');
                TempFileName := GetTempDir + FormatDateTime('yyyymmddhhnnsszzz', Now);
                StringToFile(FTextoPlantilla, TempFileName + '.rtf');

                if spObtenerRefinanciacionCuotas.RecordCount > 10 then begin
                    StringToFile(FTextoPlantillaAnexoCuotas, TempFileName + '_AnexoCuotas.rtf');
                end;

                if FCodigoCanalPago = 36 then begin
                    if spObtenerRefinanciacionComprobantes.RecordCount > 15 then begin
                        StringToFile(FTextoPlantillaAnexoComprobantes, TempFileName + '_AnexoComprobantes.rtf');
                    end;
                    if spObtenerRefinanciacionComprobantes.RecordCount > 45 then begin
                        StringToFile(FTextoPlantillaAnexoComprobantes2, TempFileName + '_AnexoComprobantes2.rtf');
                    end;
                end;

                TPanelMensajesForm.MuestraMensaje('Generando Documento en PDF ...');

                TextoMarcaAgua     := EmptyStr;
                TextoMarcaAguaSize := 108;

                if spObtenerRefinanciacionDatosCabecera.FieldByName('CodigoEstado').AsInteger = REFINANCIACION_ESTADO_EN_TRAMITE then begin
                    TextoMarcaAgua     := FMarcaAguaTramite;
                    TextoMarcaAguaSize := FMarcaAguaTramiteSize;
                end
                else begin
                    if spObtenerRefinanciacionDatosCabecera.FieldByName('CodigoEstado').AsInteger in [REFINANCIACION_ESTADO_VIGENTE, REFINANCIACION_ESTADO_TERMINADA] then begin
                        TextoMarcaAgua     := FMarcaAguaResto;
                        TextoMarcaAguaSize := FMarcaAguaRestoSize;
                    end;
                end;

                FicherosMergePDF := EmptyStr;

                if FileExists(TempFileName + '_AnexoComprobantes.rtf') then begin
                    PDFSize := 0;
                    TPanelMensajesForm.MuestraMensaje('Generando Anexo I Comprobantes ...');
                    Word2PDF(TempFileName + '_AnexoComprobantes.rtf', TempFileName + '_AnexoComprobantes.pdf', EmptyStr, 'Bullzip PDF Printer', TextoMarcaAgua, TextoMarcaAguaSize);

                    while GetFileSize(TempFileName + '_AnexoComprobantes.pdf') <= PDFSize do begin
                        TPanelMensajesForm.MuestraMensaje('Esperando Impresi�n Anexo I Comprobantes ...');
                        Sleep(250);
                        Application.ProcessMessages;
                    end;

                    FicherosMergePDF := FicherosMergePDF + '.|' + TempFileName + '_AnexoComprobantes.pdf';

                    if FileExists(TempFileName + '_AnexoComprobantes2.rtf') then begin
                        PDFSize := 0;
                        TPanelMensajesForm.MuestraMensaje('Generando Anexo II Comprobantes ...');
                        Word2PDF(TempFileName + '_AnexoComprobantes2.rtf', TempFileName + '_AnexoComprobantes2.pdf', EmptyStr, 'Bullzip PDF Printer', TextoMarcaAgua, TextoMarcaAguaSize);

                        while GetFileSize(TempFileName + '_AnexoComprobantes2.pdf') <= PDFSize do begin
                            TPanelMensajesForm.MuestraMensaje('Esperando Impresi�n Anexo II Comprobantes ...');
                            Sleep(250);
                            Application.ProcessMessages;
                        end;

                        FicherosMergePDF := FicherosMergePDF + '|' + TempFileName + '_AnexoComprobantes2.pdf';
                    end;
                end;

                if FileExists(TempFileName + '_AnexoCuotas.rtf') then begin
                    PDFSize := 0;
                    TPanelMensajesForm.MuestraMensaje('Generando Anexo Cuotas ...');
                    Word2PDF(TempFileName + '_AnexoCuotas.rtf', TempFileName + '_AnexoCuotas.pdf', EmptyStr, 'Bullzip PDF Printer', TextoMarcaAgua, TextoMarcaAguaSize);

                    while GetFileSize(TempFileName + '_AnexoCuotas.pdf') <= PDFSize do begin
                        TPanelMensajesForm.MuestraMensaje('Esperando Impresi�n Anexo Cuotas ...');
                        Sleep(250);
                        Application.ProcessMessages;
                    end;

                    if FicherosMergePDF = EmptyStr then begin
                        FicherosMergePDF := FicherosMergePDF + '.|' + TempFileName + '_AnexoCuotas.pdf';
                    end
                    else begin
                        FicherosMergePDF := FicherosMergePDF + '|' + TempFileName + '_AnexoCuotas.pdf';
                    end;
                end;

                PDFSize := 0;
                TPanelMensajesForm.MuestraMensaje('Generando Documento Completo Refinanciaci�n ...');
                Word2PDF(TempFileName + '.rtf', TempFileName + '.pdf', FicherosMergePDF, 'Bullzip PDF Printer', TextoMarcaAgua, TextoMarcaAguaSize);

                while GetFileSize(TempFileName + '.pdf') <= PDFSize do begin
                    TPanelMensajesForm.MuestraMensaje('Esperando Impresi�n Documento Completo Refinanciaci�n ...');
                    Sleep(500);
                    Application.ProcessMessages;
                end;

                if FileExists(TempFileName + '.rtf') then DeleteFile(TempFileName + '.rtf');
                if FileExists(TempFileName + '_AnexoCuotas.rtf') then DeleteFile(TempFileName + '_AnexoCuotas.rtf');
                if FileExists(TempFileName + '_AnexoComprobantes.rtf') then DeleteFile(TempFileName + '_AnexoComprobantes.rtf');
                if FileExists(TempFileName + '_AnexoComprobantes2.rtf') then DeleteFile(TempFileName + '_AnexoComprobantes2.rtf');

                if FileExists(TempFileName + '_AnexoCuotas.pdf') then DeleteFile(TempFileName + '_AnexoCuotas.pdf');
                if FileExists(TempFileName + '_AnexoComprobantes.pdf') then DeleteFile(TempFileName + '_AnexoComprobantes.pdf');
                if FileExists(TempFileName + '_AnexoComprobantes2.pdf') then DeleteFile(TempFileName + '_AnexoComprobantes2.pdf');

                if FileExists(TempFileName + '.pdf') then begin
                    TMuestraPDFForm.MuestraPDF(
                        TempFileName + '.pdf',
                        Format(
                            cCaptionFormPDF,
                            [spObtenerRefinanciacionDatosCabecera.FieldByName('DescripcionRefinanciacion').AsString,
                             iif(FCodigoRefinanciacionAnterior = 0, '<Nueva>', spObtenerRefinanciacionDatosCabecera.FieldByName('CodigoRefinanciacion').AsString),
                             NombreCli,
                             spObtenerRefinanciacionDatosCabecera.FieldByName('EstadoRefinanciacion').AsString]),
                        False,
                        True);

                    DeleteFile(TempFileName + '.pdf');
                end
                else begin
                    raise Exception.Create(Format('No se encontr� el Archivo PDF Temporal: %s', [TempFileName + '.pdf']));
                end;
            end;
        except
            on e:exception do begin
                raise EErrorExceptionCN.Create('Error en Impresi�n', e.Message);
            end;
        end;
    finally
        Screen.Cursor := crDefault;
    end;
end;

procedure TRefinanciacionImpresionForm.ImprimirDocumentoGenerado(SinMarcaDeAgua: Boolean = False);
    var
        TempFileName,
        TextoMarcaAgua,
        NombreCli,
        FicherosMergePDF: String;
        TextoMarcaAguaSize: Integer;
        PDFSize: Int64;
begin
    try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        TPanelMensajesForm.MuestraMensaje('Recuperando Documento Original ...');
        TempFileName := GetTempDir + FormatDateTime('yyyymmddhhnnsszzz', Now);

        FTextoPlantilla                   := spObtenerRefinanciacionDocumentoOriginal.FieldByName('DocumentoOriginal').AsString;
        FTextoPlantillaAnexoCuotas        := spObtenerRefinanciacionDocumentoOriginal.FieldByName('AnexoCuotas').AsString;
        FtextoPlantillaAnexoComprobantes  := spObtenerRefinanciacionDocumentoOriginal.FieldByName('AnexoComprobantes').AsString;
        FtextoPlantillaAnexoComprobantes2 := spObtenerRefinanciacionDocumentoOriginal.FieldByName('AnexoComprobantes2').AsString;

        StringToFile(FTextoPlantilla, TempFileName + '.rtf');

        if FTextoPlantillaAnexoCuotas <> EmptyStr then begin
            StringToFile(FTextoPlantillaAnexoCuotas, TempFileName + '_AnexoCuotas.rtf');
        end;

        if FtextoPlantillaAnexoComprobantes <> EmptyStr then begin
            StringToFile(FtextoPlantillaAnexoComprobantes, TempFileName + '_AnexoComprobantes.rtf');
            if FtextoPlantillaAnexoComprobantes2 <> EmptyStr then begin
                StringToFile(FtextoPlantillaAnexoComprobantes2, TempFileName + '_AnexoComprobantes2.rtf');
            end;
        end;

        TPanelMensajesForm.MuestraMensaje('Generando Documento en PDF ...');

        TextoMarcaAgua     := iif(SinMarcaDeAgua, EmptyStr, FMarcaAguaDocOriginal);
        TextoMarcaAguaSize := FMArcaAguaDocOriginalSize;

        FicherosMergePDF := EmptyStr;

        if FileExists(TempFileName + '_AnexoComprobantes.rtf') then begin
            PDFSize := 0;
            Word2PDF(TempFileName + '_AnexoComprobantes.rtf', TempFileName + '_AnexoComprobantes.pdf', EmptyStr, 'Bullzip PDF Printer', TextoMarcaAgua, TextoMarcaAguaSize);

            while GetFileSize(TempFileName + '_AnexoComprobantes.pdf') <= PDFSize do begin
                TPanelMensajesForm.MuestraMensaje('Esperando Impresi�n Documento en PDF ...');
                Sleep(250);
                Application.ProcessMessages;
            end;

            FicherosMergePDF := FicherosMergePDF + '.|' + TempFileName + '_AnexoComprobantes.pdf';

            if FileExists(TempFileName + '_AnexoComprobantes2.rtf') then begin
                PDFSize := 0;
                Word2PDF(TempFileName + '_AnexoComprobantes2.rtf', TempFileName + '_AnexoComprobantes2.pdf', EmptyStr, 'Bullzip PDF Printer', TextoMarcaAgua, TextoMarcaAguaSize);

                while GetFileSize(TempFileName + '_AnexoComprobantes2.pdf') <= PDFSize do begin
                    TPanelMensajesForm.MuestraMensaje('Esperando Impresi�n Documento en PDF ...');
                    Sleep(250);
                    Application.ProcessMessages;
                end;

                FicherosMergePDF := FicherosMergePDF + '|' + TempFileName + '_AnexoComprobantes2.pdf';
            end;
        end;

        if FileExists(TempFileName + '_AnexoCuotas.rtf') then begin
            PDFSize := 0;
            Word2PDF(TempFileName + '_AnexoCuotas.rtf', TempFileName + '_AnexoCuotas.pdf', EmptyStr, 'Bullzip PDF Printer', TextoMarcaAgua, TextoMarcaAguaSize);

            while GetFileSize(TempFileName + '_AnexoCuotas.pdf') <= PDFSize do begin
                TPanelMensajesForm.MuestraMensaje('Esperando Impresi�n Documento en PDF ...');
                Sleep(250);
                Application.ProcessMessages;
            end;

            if FicherosMergePDF = EmptyStr then begin
                FicherosMergePDF := FicherosMergePDF + '.|' + TempFileName + '_AnexoCuotas.pdf';
            end
            else begin
                FicherosMergePDF := FicherosMergePDF + '|' + TempFileName + '_AnexoCuotas.pdf';
            end;
        end;

        PDFSize := 0;
        Word2PDF(TempFileName + '.rtf', TempFileName + '.pdf', FicherosMergePDF, 'Bullzip PDF Printer', TextoMarcaAgua, TextoMarcaAguaSize);

        while GetFileSize(TempFileName + '.pdf') <= PDFSize do begin
            TPanelMensajesForm.MuestraMensaje('Esperando Impresi�n Documento en PDF ...');
            Sleep(250);
            Application.ProcessMessages;
        end;


        if FileExists(TempFileName + '.rtf') then DeleteFile(TempFileName + '.rtf');
        if FileExists(TempFileName + '_AnexoCuotas.rtf') then DeleteFile(TempFileName + '_AnexoCuotas.rtf');
        if FileExists(TempFileName + '_AnexoComprobantes.rtf') then DeleteFile(TempFileName + '_AnexoComprobantes.rtf');
        if FileExists(TempFileName + '_AnexoComprobantes2.rtf') then DeleteFile(TempFileName + '_AnexoComprobantes2.rtf');

        if FileExists(TempFileName + '_AnexoCuotas.pdf') then DeleteFile(TempFileName + '_AnexoCuotas.pdf');
        if FileExists(TempFileName + '_AnexoComprobantes.pdf') then DeleteFile(TempFileName + '_AnexoComprobantes.pdf');
        if FileExists(TempFileName + '_AnexoComprobantes2.pdf') then DeleteFile(TempFileName + '_AnexoComprobantes2.pdf');

        if FileExists(TempFileName + '.pdf') then begin
                
            NombreCli := Trim(Trim(FPersona.Nombre) + ' ' + Trim(FPersona.Apellido) + ' ' + Trim(FPersona.ApellidoMaterno));

            TMuestraPDFForm.MuestraPDF(
                TempFileName + '.pdf',
                Format(
                    cCaptionFormPDF,
                    [spObtenerRefinanciacionDocumentoOriginal.FieldByName('DescripcionRefinanciacion').AsString,
                     spObtenerRefinanciacionDocumentoOriginal.FieldByName('CodigoRefinanciacion').AsString,
                     NombreCli,
                     spObtenerRefinanciacionDocumentoOriginal.FieldByName('EstadoRefinanciacion').AsString]),
                False,
                True);

            DeleteFile(TempFileName + '.pdf')
        end
        else begin
            raise Exception.Create(Format('No se encontr� el Archivo PDF Temporal: %s', [TempFileName + '.pdf']));
        end;

        {
        if FileExists(TempFileName + '_AnexoComprobantes.rtf') then begin
            Word2PDF(TempFileName + '_AnexoComprobantes.rtf', TempFileName + '.pdf', EmptyStr, 'Bullzip PDF Printer', TextoMarcaAgua, TextoMarcaAguaSize);

            if FileExists(TempFileName + '_AnexoComprobantes2.rtf') then begin
                Word2PDF(TempFileName + '_AnexoComprobantes2.rtf', TempFileName + '.pdf', EmptyStr, 'Bullzip PDF Printer', TextoMarcaAgua, TextoMarcaAguaSize);
            end;
        end;

        if FileExists(TempFileName + '_AnexoCuotas.rtf') then begin
            Word2PDF(TempFileName + '_AnexoCuotas.rtf', TempFileName + '.pdf', EmptyStr, 'Bullzip PDF Printer', TextoMarcaAgua, TextoMarcaAguaSize);
        end;

        Word2PDF(TempFileName + '.rtf', TempFileName + '.pdf', EmptyStr, 'Bullzip PDF Printer', TextoMarcaAgua, TextoMarcaAguaSize);


        if FileExists(TempFileName + '.rtf') then DeleteFile(TempFileName + '.rtf');
        if FileExists(TempFileName + '_AnexoCuotas.rtf') then DeleteFile(TempFileName + '_AnexoCuotas.rtf');
        if FileExists(TempFileName + '_AnexoComprobantes.rtf') then DeleteFile(TempFileName + '_AnexoComprobantes.rtf');

        if FileExists(TempFileName + '.pdf') then begin

            NombreCli := Trim(Trim(FPersona.Nombre) + ' ' + Trim(FPersona.Apellido) + ' ' + Trim(FPersona.ApellidoMaterno));

            TMuestraPDFForm.MuestraPDF(
                TempFileName + '.pdf',
                Format(
                    cCaptionFormPDF,
                    [spObtenerRefinanciacionDocumentoOriginal.FieldByName('DescripcionRefinanciacion').AsString,
                     spObtenerRefinanciacionDocumentoOriginal.FieldByName('CodigoRefinanciacion').AsString,
                     NombreCli,
                     spObtenerRefinanciacionDocumentoOriginal.FieldByName('EstadoRefinanciacion').AsString]),
                False,
                True);

            DeleteFile(TempFileName + '.pdf')
        end;
        }
    finally
        Screen.Cursor := crDefault;
    end;
end;

end.
