{-----------------------------------------------------------------------------
 File Name: frmReimpresionComprobantesBI.pas
 Author:   jconcheyro
 Date Created: 04/12/2006
 Language: ES-AR
 Description: Reimpresion de Comprobantes Infractor
 Es una copia de la pantalla de Cobro de NI, pero no cobra
-----------------------------------------------------------------------------}
unit frmReimpresionComprobantesBI;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ListBoxEx, DBListEx, StdCtrls, DPSControls, VariantComboBox,
  DmiCtrls, DB, ADODB, DMCOnnection, utilDB, UtilFacturacion, utilProc, buscaClientes,
  PeaTypes, RStrings, DBCtrls, peaProcs, Util, PagoVentanilla, ExtCtrls, ReporteRecibo,
  FrmImprimir, DBClient, ImgList, frmRptNotaCobroInfractor;

const
	KEY_F9 	= VK_F9;
type
  TformReimpresionComprobantesBI = class(TForm)
  	gbPorCliente: TGroupBox;
    gb_DatosPersona: TGroupBox;
    Label6: TLabel;
    lblNombre: TLabel;
    lblDomicilio: TLabel;
	  Label10: TLabel;
    gbDatosComprobante: TGroupBox;
    Label2: TLabel;
  	lblFecha: TLabel;
    lblFechaVencimiento: TLabel;
    Label7: TLabel;
    lblTotalAPagar: TLabel;
    Label12: TLabel;
    ObtenerLineasNotaCobroInfraccion: TADOStoredProc;
    lbl_NumeroComprobante: TLabel;
    edNumeroComprobante: TNumericEdit;
    lbl_Estado: TLabel;
    Label11: TLabel;
    Label8: TLabel;
    lblRUT: TLabel;
    dblLineasComprobante: TDBListEx;
    dsLineasNotaCObroInfraccion: TDataSource;
    Img_Tilde: TImageList;
    btnSalir: TButton;
    ObtenerNotaCobroInfraccion: TADOStoredProc;
    Timer1: TTimer;
    Label1: TLabel;
    btnReimprimirNI: TButton;
  	procedure FormCreate(Sender: TObject);
  	procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  	procedure cbComprobantesChange(Sender: TObject);
  	procedure edNumeroComprobanteChange(Sender: TObject);
  	procedure Timer1Timer(Sender: TObject);
    procedure dblLineasComprobanteDrawText(Sender: TCustomDBListEx;Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState; var Text: String; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
    procedure btnReimprimirNIClick(Sender: TObject);
  private
    { Private declarations }
    FLogo: TBitmap;
    FTotalApagar: Double;
    FNumeroPOS, FPuntoEntrega, FPuntoVenta: Integer;
    // C�digo del canal de pago a setear por defecto en el form de registar pago.
    FCodigoCanalPago: Integer;
    // Monto Total de los comprobantes seleccionados para cobrar.
    FTotalComprobante: extended;
    function  CargarLineasComprobante : boolean;
    procedure MostrarDatosComprobante;
    procedure LimpiarDetalles;
    procedure LimpiarListaComprobantes;
  public
    { Public declarations }
    function Inicializar(NumeroPOS, PuntoEntrega, PuntoVenta: Integer): Boolean; overload;
  	function Inicializar: Boolean; Overload;
  end;

var
  formReimpresionComprobantesBI: TformReimpresionComprobantesBI;

implementation

const
	SetPACPAT = [1, 2];


{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    fmalisia
  Date Created: 08/12/2005
  Description: Inicializa el Form - Carga los tipos de Comprobantes
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TformReimpresionComprobantesBI.Inicializar(NumeroPOS, PuntoEntrega, PuntoVenta: Integer): Boolean;
resourcestring
	MSG_LOGO_NO_ENCONTRADO  = 'No se ha encontrado el archivo con la imagen del logo para el recibo.';
	MSG_CAPTION             = 'Cobro de Comprobantes';
	MSG_ERROR_INICIALIZAR   = 'Error inicializando el formulario.';
begin
    try
        FTotalComprobante := 0;
        FCodigoCanalPago := CONST_CANAL_PAGO_VENTANILLA;
        // Por ahora lo cargamos a mano
        lbl_Estado.Caption := EmptyStr;
        MostrarDatosComprobante;
        FNumeroPOS := NumeroPOS;
        FPuntoEntrega := PuntoEntrega;
        FPuntoVenta := PuntoVenta;
        FormStyle := fsMDIChild;
        CenterForm(Self);
        FLogo := TBitmap.Create;
        (* Levanto el logo aqu� para que se haga una sola vez, independientemente de los reintentos
        de impresi�n que haga el operador. *)
        if not LevantarLogo (FLogo) then MsgBox(MSG_LOGO_NO_ENCONTRADO, MSG_CAPTION, MB_ICONINFORMATION + MB_OK);
        edNumeroComprobante.SetFocus();
        Result := True;
    except
	   	  on e: exception do begin
			      MsgBoxErr(MSG_ERROR_INICIALIZAR, E.Message, MSG_CAPTION, MB_ICONERROR);
			      Result := False;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    fmalisia
  Date Created: 08/12/2005
  Description: Inicializa el Form - Carga los tipos de Comprobantes y busca
				el comprobante seleccionado
  Parameters: TipoComprobante: AnsiString; NumeroComprobante: Int64
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TformReimpresionComprobantesBI.Inicializar: Boolean;
resourcestring
	MSG_LOGO_NO_ENCONTRADO = 'No se ha encontrado el archivo con la imagen del logo para el recibo.';
	MSG_CAPTION			   = 'Reimpresi�n de Comprobantes BI';
	MSG_ERROR_INICIALIZAR   = 'Error inicializando el formulario.';
begin
    try
        Result := True;
        FTotalComprobante           := 0;
        FCodigoCanalPago := CONST_CANAL_PAGO_VENTANILLA;
        // Por ahora lo cargamos a mano
        lbl_Estado.Caption := EmptyStr;
        FormStyle := fsMDIChild;
        CenterForm(Self);

    except
        on e: exception do begin
      			MsgBoxErr(MSG_ERROR_INICIALIZAR, E.Message, MSG_CAPTION, MB_ICONERROR);
    			Result := False;
        end;
    end; // except

end;

{-----------------------------------------------------------------------------
  Function Name: FormCreate
  Author: fmalisia
  Date Created: 08/12/2005
  Description: centro el formulario al crearlo
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure TformReimpresionComprobantesBI.FormCreate(Sender: TObject);
begin
    CenterForm(Self);
end;

{-----------------------------------------------------------------------------
  Function Name: FormCreate
  Author: fmalisia
  Date Created: 08/12/2005
  Description: centro el formulario al crearlo
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TformReimpresionComprobantesBI.CargarLineasComprobante : boolean;
begin
    // Cargamos la lista de comprobantes.
    LimpiarListaComprobantes;
    try
        with ObtenerLineasNotaCobroInfraccion, Parameters do begin
            Close;
            ParamByName('@NumeroComprobante').Value := IIF( edNumeroComprobante.Text <> '', edNumeroComprobante.Value, NULL);
            Open;
            Result := True;
        end;
    except
        result := false;
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: MostrarDatosComprobante
  Author:    flamas
  Date Created: 20/12/2004
  Description: Muestra los datos del Comprobante
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformReimpresionComprobantesBI.MostrarDatosComprobante;
begin
    LimpiarDetalles;
    with ObtenerNotaCobroInfraccion do begin
        if Active = False then exit;
        lblRUT.Caption := FieldByName('NumeroDocumento').AsString;
        lblNombre.Caption := FieldByName('Apellido').AsString + ', ' + FieldByName('Nombre').AsString;
        lblDomicilio.Caption := FieldByName('Calle').AsString + ' ' +FieldByName('Numero').AsString+ ' , ' + FieldByName('DetalleDomicilio').AsString+ ' , ' + FieldByName('Comuna').AsString + ' , ' + FieldByName('Region').AsString;
        lblFecha.Caption := FormatDateTime('dd/mm/yyyy', FieldByName('FechaEmision').AsDateTime);
        lblFechaVencimiento.Caption := FormatDateTime('dd/mm/yyyy', FieldByName('FechaVencimiento').AsDateTime);
        lblTotalAPagar.Caption := FormatFloat(FORMATO_IMPORTE , FieldByName('TotalAPagar').AsFloat);
        FTotalApagar := Round(FieldByName('TotalAPagar').AsFloat);
        lbl_Estado.Caption := FieldByName('DescriEstado').AsString;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: LimpiarDetalles;
  Author:    flamas
  Date Created: 26/01/2005
  Description: Borra los detalles del Comprobante
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformReimpresionComprobantesBI.LimpiarDetalles;
begin
    lblRUT.Caption                  := EmptyStr;
  	lblNombre.Caption               := EmptyStr;
  	lblDomicilio.Caption            := EmptyStr;
  	lblFecha.Caption                := EmptyStr;
  	lblFechaVencimiento.Caption	    := EmptyStr;
    lblTotalAPagar.Caption          := EmptyStr;
    lbl_Estado.Caption              := EmptyStr;
end;

{-----------------------------------------------------------------------------
  Function Name: cbComprobantesChange
  Author:    flamas
  Date Created: 21/12/2004
  Description: Procesa un cambio de comprobante y muestra los datos
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformReimpresionComprobantesBI.cbComprobantesChange(Sender: TObject);
begin
    if ActiveControl <> Sender then Exit;
    MostrarDatosComprobante;
end;


{-----------------------------------------------------------------------------
  Function Name: edNumeroComprobanteChange
  Author:    flamas
  Date Created: 21/12/2004
  Description: Procesa un cambio de comprobante y muestra los datos del comprobante
  Parameters: Sender: TObject
  Return Value: None
  -----------------------------------------------------------------------------}
procedure TformReimpresionComprobantesBI.edNumeroComprobanteChange(Sender: TObject);
begin
    if ActiveControl <> Sender then Exit;
    LimpiarListaComprobantes;

  	if Timer1.Enabled then begin
        Timer1.Enabled := False;
        Timer1.Enabled := True;
    end else Timer1.Enabled := True;
end;

{-----------------------------------------------------------------------------
  Function Name: Timer1Timer
  Author:
  Date Created:  13/10/2005
  Description:
  Parameters:
  Return Value:
-----------------------------------------------------------------------------}
procedure TformReimpresionComprobantesBI.Timer1Timer(Sender: TObject);
begin
    LimpiarDetalles;
    Timer1.Enabled := False;
    with ObtenerNotaCobroInfraccion, Parameters do begin
        close;
        Refresh;
        ParamByName('@NumeroComprobante').value := edNumeroComprobante.value;
        open;
        if not isEmpty then begin
            MostrarDatosComprobante;
            CargarLineasComprobante;
            //Si la nota de cobro esta impaga
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: dl_ComprobantesDrawText
  Author:
  Date Created:  13/10/2005
  Description:
  Parameters:
  Return Value:
-----------------------------------------------------------------------------}
procedure TformReimpresionComprobantesBI.dblLineasComprobanteDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;State: TOwnerDrawState; var Text: String; var TxtRect: TRect;var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
    with Sender do begin
        if Column.FieldName = 'Importe' then begin
            Text := FormatFloat(FORMATO_IMPORTE , ObtenerLineasNotaCobroInfraccion.FieldByName('Importe').AsFloat);
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: LimpiarListaComprobantes
  Author: lgisuk
  Date Created:  13/10/2005
  Description:  Limpio la lista de comprobantes
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformReimpresionComprobantesBI.LimpiarListaComprobantes;
begin
    //Limpio el total de los comprobantes
    FTotalComprobante               := 0;
    //Limpio la Grilla
    with ObtenerLineasNotaCobroInfraccion, Parameters do begin
      Close;
      ParamByName('@NumeroComprobante').Value := -1;
      Open;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnSalirClick
  Author:    lgisuk
  Date Created: 13/10/2005
  Description: Permito salir del formulario
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure TformReimpresionComprobantesBI.btnReimprimirNIClick(Sender: TObject);
var
    fReporte: TfRptNotaCobroInfractor;
begin
    fReporte := TfRptNotaCobroInfractor.Create(self);
    freporte.Inicializar(edNumeroComprobante.ValueInt);
    freporte.Release;
end;

procedure TformReimpresionComprobantesBI.btnSalirClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 13/10/2005
  Description: Libero el Form de Memoria
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure TformReimpresionComprobantesBI.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    FLogo.Free;
    Action := caFree;
end;


end.
