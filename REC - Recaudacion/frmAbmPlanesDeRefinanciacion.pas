{-----------------------------------------------------------------------------
 Unit Name: frmAbmPlanesDeRefinanciacion
 Author:    aunanue
 Date:      08-03-2017
 Purpose:   Administra los planes de refinanciacion: CU.COBO.ADM.COB.101
 Firma: TASK_004_AUN_20170314-CU.COBO.ADM.COB.101
 History:
-----------------------------------------------------------------------------}
unit frmAbmPlanesDeRefinanciacion;

interface

uses
  DMConnection,
  UtilProc,
  Util,
  RStrings,
  utildb,
  PeaTypes,
  PeaProcs,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, OleCtrls,
  DmiCtrls, Mask,  ComCtrls,  validate, Dateedit, ADODB, DPSControls, ListBoxEx,
  DBListEx, Variants, VariantComboBox;

type
  TAbmPlanesDeRefinanciacionForm = class(TForm)
    GrupoDatos: TPanel;
    PAbajo: TPanel;
	Notebook: TNotebook;
    txtCodigo: TNumericEdit;
    AbmToolbar: TAbmToolbar;
    spAlmacenar: TADOStoredProc;
    DBListDatos: TAbmList;
    spEliminar: TADOStoredProc;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    spListar: TADOStoredProc;
    Label8: TLabel;
    lblNombre: TLabel;
    txtNombre: TEdit;
    txtMontoMinimoDeDeuda: TNumericEdit;
    lblMontoMinimoDeDeuda: TLabel;
    txtMontoMaximoDeDeuda: TNumericEdit;
    lblMontoMaximoDeDeuda: TLabel;
    txtCantidadDeCuotas: TNumericEdit;
    lblCantidadDeCuotas: TLabel;
    lblPersoneria: TLabel;
    txtPersoneria: TVariantComboBox;
    lblPorcentajeCuotaPie: TLabel;
    Label1: TLabel;
    txtPorcentajeCuotaPie: TNumericEdit;
    procedure FormShow(Sender: TObject);
   	procedure DBListDatosRefresh(Sender: TObject);
   	function  DBListDatosProcess(Tabla: TDataSet; var Texto: String): Boolean;
   	procedure DBListDatosDrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
	procedure DBListDatosClick(Sender: TObject);
	procedure DBListDatosInsert(Sender: TObject);
	procedure DBListDatosEdit(Sender: TObject);
	procedure DBListDatosDelete(Sender: TObject);
	procedure BtnAceptarClick(Sender: TObject);
	procedure BtnCancelarClick(Sender: TObject);
	procedure AbmToolbarClose(Sender: TObject);
	procedure BtnSalirClick(Sender: TObject);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
	{ Private declarations }
	procedure LimpiarCampos(usarCeros: boolean);
	Procedure VolverCampos;
  public
	{ Public declarations }
	function Inicializa: Boolean;
  end;

resourcestring
    STR_CAPTION = 'Planes de refinanciación';

implementation

uses DB_CRUDCommonProcs;

const NOMBRE_CAMPO_CODIGO = 'CodigoPlanRefinanciacion';

{$R *.DFM}

{-----------------------------------------------------------------------------
  Function Name: Inicializa
  Author:    aunanue
  Date:      09-03-2017
  Description:
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TAbmPlanesDeRefinanciacionForm.Inicializa:Boolean;
var
	S: TSize;
begin
	Result := False;
	FormStyle := fsMDIChild;
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
    Notebook.PageIndex := 0;
    Self.Caption := STR_CAPTION;
    DB_CRUDCommonProcs.FillVariantItemsFromStoredProc(DMConnections.BaseCAC, 'PersoneriasCobranzasRefinanciaciones_Listar',
                                                      txtPersoneria.Items, 'CodigoTipoCliente', 'Descripcion', [], [], Self.Caption, True);
    txtPersoneria.Value := -1;
    //
    if not OpenTables([spListar]) then exit;
	DBListDatos.Reload;
    ActiveControl := DBListDatos;
    //OK
	Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: LimpiarCampos
  Author:    aunanue
  Date:      09-03-2017
  Description:
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TAbmPlanesDeRefinanciacionForm.LimpiarCampos(usarCeros: boolean);
begin
	txtCodigo.Clear;
    txtNombre.Clear;
    if usarCeros then begin
        txtMontoMinimoDeDeuda.Value  := 0;
        txtMontoMaximoDeDeuda.Value  := 0;
        txtCantidadDeCuotas.ValueInt := 0;
        txtPorcentajeCuotaPie.Value  := 0;
    end else begin
        txtMontoMinimoDeDeuda.Clear;
        txtMontoMaximoDeDeuda.Clear;
        txtCantidadDeCuotas.Clear;
        txtPorcentajeCuotaPie.Clear;
    end;
    txtPersoneria.Value := -1;
end;

{-----------------------------------------------------------------------------
  Function Name: FormShow
  Author:    aunanue
  Date:      09-03-2017
  Description: Actualizo la grilla
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TAbmPlanesDeRefinanciacionForm.FormShow(Sender: TObject);
begin
	DBListDatos.Reload;
end;

{-----------------------------------------------------------------------------
  Function Name: DBListDatosRefresh
  Author:    aunanue
  Date:      09-03-2017
  Description: lipio los campos si la tabla esta vacia
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TAbmPlanesDeRefinanciacionForm.DBListDatosRefresh(Sender: TObject);
begin
	 if DBListDatos.Empty then LimpiarCampos(false);
end;

{-----------------------------------------------------------------------------
  Function Name: VolverCampos
  Author:    aunanue
  Date:      09-03-2017
  Description:
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TAbmPlanesDeRefinanciacionForm.VolverCampos;
begin
	DBListDatos.Estado  := Normal;
	DBListDatos.Enabled := True;
	ActiveControl       := DBListDatos;
	Notebook.PageIndex  := 0;
	GrupoDatos.Enabled  := False;
	DBListDatos.Reload;
end;

{-----------------------------------------------------------------------------
  Function Name: DBListDatosDrawItem
  Author:    aunanue
  Date:      09-03-2017
  Description: Muestro los Registros
  Parameters: Sender: TDBList; Tabla: TDataSet;Rect: TRect; State: TOwnerDrawState; Cols: TColPositions
  Return Value: None
-----------------------------------------------------------------------------}
procedure TAbmPlanesDeRefinanciacionForm.DBListDatosDrawItem(Sender: TDBList; Tabla: TDataSet;Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, Istr(Tabla.FieldbyName(NOMBRE_CAMPO_CODIGO).AsInteger, 10));
        TextOut(Cols[1], Rect.Top, Trim(Tabla.FieldbyName('Nombre').AsString));
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: DBListDatosClick
  Author:    aunanue
  Date:      09-03-2017
  Description: Selecciono un campo
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TAbmPlanesDeRefinanciacionForm.DBListDatosClick(Sender: TObject);
begin
	with (Sender AS TDbList).Table do begin
	   txtCodigo.Value		:= FieldByName(NOMBRE_CAMPO_CODIGO).AsInteger;
       txtNombre.Text       := FieldByName('Nombre').AsString;
       txtPersoneria.Value := FieldByName('CodigoTipoCliente').AsInteger;
       txtMontoMinimoDeDeuda.Value := FieldByName('MontoMinimo').AsFloat;
       txtMontoMaximoDeDeuda.Value := FieldByName('MontoMaximo').AsFloat;
       txtCantidadDeCuotas.ValueInt := FieldByName('CantidadDeCuotas').AsInteger;
       txtPorcentajeCuotaPie.Value := FieldByname('PorcentajeCuotaPie').AsFloat;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: DBListDatosProcess
  Author:    aunanue
  Date:      09-03-2017
  Description:
  Parameters: Tabla: TDataSet;var Texto: String
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TAbmPlanesDeRefinanciacionForm.DBListDatosProcess(Tabla: TDataSet; var Texto: String): Boolean;
begin
    Texto := Tabla.FieldByName('Nombre').AsString;
	Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: DBListDatosInsert
  Author:    aunanue
  Date:      09-03-2017
  Description: limpio los campos para que ingrese un nuevo registro
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TAbmPlanesDeRefinanciacionForm.DBListDatosInsert(Sender: TObject);
begin
	LimpiarCampos(true);
    GrupoDatos.Enabled  := True;
	DBListDatos.Enabled := False;
	Notebook.PageIndex  := 1;
	ActiveControl       := txtNombre;
end;

{-----------------------------------------------------------------------------
  Function Name: DBListDatosEdit
  Author:    aunanue
  Date:      09-03-2017
  Description:
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TAbmPlanesDeRefinanciacionForm.DBListDatosEdit(Sender: TObject);
begin
	DBListDatos.Enabled := False;
	DBListDatos.Estado  := modi;
	Notebook.PageIndex  := 1;
	GrupoDatos.Enabled  := True;
	ActiveControl := txtNombre;
end;

{-----------------------------------------------------------------------------
  Function Name: DBListDatosDelete
  Author:    aunanue
  Date:      09-03-2017
  Description: elimino un item
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TAbmPlanesDeRefinanciacionForm.DBListDatosDelete(Sender: TObject);
begin
	if MsgBox(Format(MSG_QUESTION_ELIMINAR,[STR_CAPTION]), STR_CONFIRMACION, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
        Screen.Cursor := crHourGlass;
        try
            spEliminar.Connection.BeginTrans;
            try
                AddParameter_Codigo(NOMBRE_CAMPO_CODIGO, spEliminar.Parameters, txtCodigo.ValueInt);
                AddParameter_CodigoUsuario(spEliminar.Parameters);
                AddParameter_Resultado(spEliminar.Parameters);
                spEliminar.ExecProc;
                if spEliminar.Parameters.ParamByName('@Resultado').Value > 0 then begin
                    spEliminar.Connection.CommitTrans;
                end else begin
                    spEliminar.Connection.RollbackTrans;
                    MsgBox(MSG_EXISTEN_DATOS_RELACIONADOS_BORRANDO, STR_CAPTION, MB_ICONSTOP);
                end;
            except
                On E: Exception do begin
                    if spEliminar.Connection.InTransaction then
                        spEliminar.Connection.RollbackTrans;
                    MsgBoxErr(Format(MSG_ERROR_ELIMINAR,[STR_CAPTION]), e.message, Format(MSG_CAPTION_ELIMINAR,[STR_CAPTION]), MB_ICONSTOP);
                end
            end
        finally
            VolverCampos();
	        Screen.Cursor := crDefault;
    	end;
    end else begin
        DBListDatos.Estado  := Normal;
	    DBListDatos.Enabled := True;
	    ActiveControl       := DBListDatos;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: BtnAceptarClick
  Author:    aunanue
  Date:      09-03-2017
  Description: Acepto los cambios
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TAbmPlanesDeRefinanciacionForm.BtnAceptarClick(Sender: TObject);
resourcestring
    TXT_MAYOR_QUE_EL = 'El %s debe ser mayor que %s.';
    TXT_MAYOR_QUE_LA = 'La %s debe ser mayor que cero.';
    TXT_RANGO = 'El %s debe ser valer entre 0 y 100.';
var
    BookM: TBookmark;
    Result, Duplicado: Boolean;
    i: Integer;
    TextoMontoMinimo: string;
begin
    TextoMontoMinimo := FindFocusLabelText(Self, txtMontoMinimoDeDeuda, '');
  	if not ValidateControls(
        [txtNombre,
         txtPersoneria,
         txtMontoMinimoDeDeuda, txtMontoMaximoDeDeuda,
         txtCantidadDeCuotas,
         txtPorcentajeCuotaPie],

	    [Trim(txtNombre.Text) <> '',
         txtPersoneria.ItemIndex > 0,
         txtMontoMinimoDeDeuda.Value >= 0, txtMontoMaximoDeDeuda.Value > txtMontoMinimoDeDeuda.Value,
         txtCantidadDeCuotas.ValueInt > 0,
         (txtPorcentajeCuotaPie.Value >= 0) and (txtPorcentajeCuotaPie.Value <= 100)],
	     Format(MSG_CAPTION_ACTUALIZAR, [STR_CAPTION]),

        [Format(MSG_VALIDAR_DEBE_EL, [FindFocusLabelText(Self, txtNombre)]),
         Format(MSG_VALIDAR_SELECCIONAR_UNO, [FindFocusLabelText(Self, txtPersoneria)]),
         Format(TXT_MAYOR_QUE_EL, [TextoMontoMinimo, 'cero']),
         Format(TXT_MAYOR_QUE_EL, [FindFocusLabelText(Self, txtMontoMaximoDeDeuda, ''), 'el ' + TextoMontoMinimo]),
         Format(TXT_MAYOR_QUE_LA, [FindFocusLabelText(Self, txtCantidadDeCuotas, '')]),
         Format(TXT_RANGO, [FindFocusLabelText(Self, txtPorcentajeCuotaPie, '')])
         ]) then begin
		Exit;
	end;
    Screen.Cursor := crHourGlass;
    Result := False;
    Duplicado := False;
    try
        //Validar Duplicados
        BookM := DBListDatos.Table.GetBookmark;
        try
            DBListDatos.Table.First;
            for i := 0 to DBListDatos.Table.RecordCount - 1 do begin
                if DBListDatos.Table.FieldByName(NOMBRE_CAMPO_CODIGO).AsString <> txtCodigo.Text then begin
                    if SysUtils.CompareText(Trim(DBListDatos.Table.FieldByName('Nombre').AsString), Trim(txtNombre.Text)) = 0 then begin
                        MsgBox(Format(MSG_ERROR_REGISTRO_DUPLICADO, [Trim(txtNombre.Text)]), STR_CAPTION, MB_ICONSTOP);
                        Duplicado := True;
                        break;
                    end;
                end;
                DBListDatos.Table.Next;
            end;
            DBListDatos.Table.GotoBookmark(BookM);
        finally
            DBListDatos.Table.FreeBookmark(BookM);
        end;

        if Duplicado then
            Exit;
        //
        spAlmacenar.Connection.BeginTrans;
        try
            AddParameter_Codigo(NOMBRE_CAMPO_CODIGO, spAlmacenar.Parameters, txtCodigo.ValueInt);
            AddParameter_Nombre(spAlmacenar.Parameters, Trim(txtNombre.Text));
            AddParameter(spAlmacenar.Parameters, '@CodigoTipoCliente', ftInteger, pdInput, txtPersoneria.Value);
            AddParameter(spAlmacenar.Parameters, '@MontoMinimo', ftFloat, pdInput, txtMontoMinimoDeDeuda.Value);
            AddParameter(spAlmacenar.Parameters, '@MontoMaximo', ftFloat, pdInput, txtMontoMaximoDeDeuda.Value);
            AddParameter(spAlmacenar.Parameters, '@CantidadDeCuotas', ftInteger, pdInput, txtCantidadDeCuotas.ValueInt);
            AddParameter(spAlmacenar.Parameters, '@PorcentajeCuotaPie', ftFloat, pdInput, txtPorcentajeCuotaPie.Value);
            AddParameter_CodigoUsuario(spAlmacenar.Parameters);
            AddParameter_Resultado(spAlmacenar.Parameters);
            spAlmacenar.ExecProc;
            spAlmacenar.Connection.CommitTrans;
            Result := True;
        except
            On E: Exception do begin
                if spAlmacenar.Connection.InTransaction then
                    spAlmacenar.Connection.RollbackTrans;
                //
                MsgBoxErr(Format(MSG_ERROR_ACTUALIZAR,[STR_CAPTION]), e.message, Format(MSG_CAPTION_ACTUALIZAR,[STR_CAPTION]), MB_ICONSTOP);
            end;
        end;
    finally
        if (Result) then begin
            VolverCampos;
        end;
        Screen.Cursor := crDefault;
    end;   
end;

{-----------------------------------------------------------------------------
  Function Name: BtnCancelarClick
  Author:    aunanue
  Date:      09-03-2017
  Description: permito cancelar
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TAbmPlanesDeRefinanciacionForm.BtnCancelarClick(Sender: TObject);
begin
	VolverCampos;
end;

{-----------------------------------------------------------------------------
  Function Name: AbmToolbar1Close
  Author:    aunanue
  Date:      09-03-2017
  Description: salgo del formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TAbmPlanesDeRefinanciacionForm.AbmToolbarClose(Sender: TObject);
begin
	 Close;
end;

{-----------------------------------------------------------------------------
  Function Name: BtnSalirClick
  Author:    aunanue
  Date:      09-03-2017
  Description:  salgo del formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TAbmPlanesDeRefinanciacionForm.BtnSalirClick(Sender: TObject);
begin
	 AbmToolbarClose(BtnSalir);
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    aunanue
  Date:      09-03-2017
  Description: lo libero de memoria
  Parameters: Sender: TObject; var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TAbmPlanesDeRefinanciacionForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	 Action := caFree;
end;

end.

