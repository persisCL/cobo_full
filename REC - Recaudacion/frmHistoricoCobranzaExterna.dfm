object FormHistoricoCobranzaExterna: TFormHistoricoCobranzaExterna
  Left = 0
  Top = 0
  Caption = 'Hist'#243'rico de Cobranza Externa'
  ClientHeight = 610
  ClientWidth = 1142
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1142
    Height = 51
    Align = alTop
    TabOrder = 0
    DesignSize = (
      1142
      51)
    object Label7: TLabel
      Left = 24
      Top = 6
      Width = 110
      Height = 13
      Caption = 'Empresa de Cobranzas'
    end
    object Label1: TLabel
      Left = 219
      Top = 6
      Width = 34
      Height = 13
      Caption = 'Judicial'
    end
    object Label13: TLabel
      Left = 413
      Top = 6
      Width = 60
      Height = 13
      Caption = 'RUT Cliente:'
    end
    object Label2: TLabel
      Left = 585
      Top = 6
      Width = 57
      Height = 13
      Caption = 'Fecha Inicio'
    end
    object Label3: TLabel
      Left = 705
      Top = 6
      Width = 46
      Height = 13
      Caption = 'Fecha Fin'
    end
    object vcbEmpresasCobranza: TVariantComboBox
      Left = 24
      Top = 22
      Width = 185
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 0
      Items = <>
    end
    object vcbJudicial: TVariantComboBox
      Left = 219
      Top = 22
      Width = 185
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 1
      Items = <
        item
          Caption = 'PreJudicial'
          Value = '1'
        end
        item
          Caption = 'Judicial'
          Value = '2'
        end
        item
          Caption = 'Ambas'
          Value = '0'
        end>
    end
    object peNumeroDocumento: TPickEdit
      Left = 410
      Top = 22
      Width = 156
      Height = 21
      CharCase = ecUpperCase
      Enabled = True
      TabOrder = 2
      EditorStyle = bteTextEdit
      OnButtonClick = peNumeroDocumentoButtonClick
    end
    object dedtFechaInicio: TDateEdit
      Left = 585
      Top = 22
      Width = 104
      Height = 21
      AutoSelect = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      Date = -693594.000000000000000000
    end
    object dedtFechaFin: TDateEdit
      Left = 705
      Top = 22
      Width = 104
      Height = 21
      AutoSelect = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      Date = -693594.000000000000000000
    end
    object btnLimpiarCobExtraJudicial: TButton
      Left = 962
      Top = 20
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Limpiar'
      TabOrder = 5
      OnClick = btnLimpiarCobExtraJudicialClick
    end
    object btnBuscarCobExtraJudicial: TButton
      Left = 1043
      Top = 20
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Buscar'
      Default = True
      TabOrder = 6
      OnClick = btnBuscarCobExtraJudicialClick
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 569
    Width = 1142
    Height = 41
    Align = alBottom
    TabOrder = 1
    DesignSize = (
      1142
      41)
    object btnSalir: TButton
      Left = 1043
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Salir'
      TabOrder = 0
      OnClick = btnSalirClick
    end
  end
  object dlMorososCobExtraJudicial: TDBListEx
    Left = 0
    Top = 51
    Width = 1142
    Height = 518
    Align = alClient
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Nombre de la Empresa'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = [fsBold]
        IsLink = False
        FieldName = 'NombreEmpresa'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 75
        Header.Caption = 'RUT'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = [fsBold]
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'RUT'
      end
      item
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 90
        Header.Caption = 'Monto'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = [fsBold]
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'Monto'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 100
        Header.Caption = 'Fecha de Env'#237'o'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = [fsBold]
        IsLink = False
        FieldName = 'FechaEnvio'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Tipo de Acci'#243'n'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = [fsBold]
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'TipoAccion'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Estado'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = [fsBold]
        IsLink = False
        FieldName = 'Estado'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 120
        Header.Caption = 'Monto Recuperado'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = [fsBold]
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'MontoRecuperado'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 95
        Header.Caption = 'Refinanciaci'#243'n'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = [fsBold]
        IsLink = False
        FieldName = 'Refinanciacion'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 85
        Header.Caption = 'Avenimiento'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = [fsBold]
        IsLink = False
        FieldName = 'Avenimiento'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 100
        Header.Caption = 'Fecha de Fin'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = [fsBold]
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'FechaFin'
      end>
    DataSource = dsObtenerHistoricoCobranzaExterna
    DragReorder = True
    ParentColor = False
    TabOrder = 2
    TabStop = True
  end
  object spObtenerHistoricoCobranzaExterna: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerHistoricoCobranzaExterna'
    Parameters = <>
    Left = 136
    Top = 128
  end
  object dsObtenerHistoricoCobranzaExterna: TDataSource
    DataSet = spObtenerHistoricoCobranzaExterna
    Left = 136
    Top = 180
  end
end
