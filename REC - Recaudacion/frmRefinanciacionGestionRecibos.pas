unit frmRefinanciacionGestionRecibos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, ListBoxEx, DBListEx, StdCtrls, ExtCtrls,

  DMConnection, PeaTypes, PeaProcsCN, SysUtilsCN, frmRefinanciacionReporteRecibo,
  PeaProcs, frmMuestraMensaje, Util, Contnrs;

type
  TRefinanciacionGestionRecibosForm = class(TForm)
    pnlAcciones: TPanel;
    btnCancelar: TButton;
    btnImprimir: TButton;
    GroupBox1: TGroupBox;
    DBListEx1: TDBListEx;
    spObtenerRefinanciacionRecibosCuotasPagos: TADOStoredProc;
    spAnularRefinanciacionCuotaPago: TADOStoredProc;
    dsObtenerRefinanciacionRecibosCuotasPagos: TDataSource;
    btnAnular: TButton;
    procedure DBListEx1DrawBackground(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var DefaultDraw: Boolean);
    procedure DBListEx1DrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
    procedure spObtenerRefinanciacionRecibosCuotasPagosAfterScroll(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure btnAnularClick(Sender: TObject);
  private
    { Private declarations }
    FBtnImprimirPermisos,
    FBtnAnularPermisos: Boolean;
  public
    { Public declarations }
    FComponentes: TObjectList;
    function Inicializar(pCodigoCliente: Integer; pRut, pDescripcionCliente: String): Boolean;
  end;

var
  RefinanciacionGestionRecibosForm: TRefinanciacionGestionRecibosForm;

implementation

{$R *.dfm}

procedure TRefinanciacionGestionRecibosForm.btnAnularClick(Sender: TObject);
    resourcestring
        MSG_ERROR_TURNO_TITULO  = 'Validaci�n Turno';
        MSG_ERROR_TURNO_MENSAJE = 'Debe tener un Turno abierto para poder Anular un Recibo.';
        MSG_PROCESO_TITULO      = 'Anulaci�n Recibo';
        MSG_PROCESO_MENSAJE     = '� Est� seguro de anular el recibo seleccionado ?' + CRLF + CRLF + 'Recibo Nro.: %s' + CRLF + 'Importe: %s';
        MSG_PROCESO_ERROR       = 'Se produjo un Error al anular el recibo seleccionado.';
    var
        NumeroRecibo: Integer;
        Mensaje: string;
begin
    if GNumeroTurno >= 1 then begin

        Mensaje :=
            Format(
                MSG_PROCESO_MENSAJE,
                [FormatFloat('#,###0',spObtenerRefinanciacionRecibosCuotasPagos.FieldByName('NumeroRecibo').AsFloat),
                 FormatFloat(FORMATO_IMPORTE, spObtenerRefinanciacionRecibosCuotasPagos.FieldByName('Importe').AsFloat)]);

        if ShowMsgBoxCN(MSG_PROCESO_TITULO, Mensaje, MB_ICONQUESTION, Self) = mrOk then try

            CambiarEstadoCursor(CURSOR_RELOJ, Self, FComponentes);
            NumeroRecibo := spObtenerRefinanciacionRecibosCuotasPagos.FieldByName('NumeroRecibo').AsInteger;

            try
                DMConnections.BaseCAC.BeginTrans;

                with spAnularRefinanciacionCuotaPago do begin
                    if Active then Close;
                    Parameters.Refresh;

                    Parameters.ParamByName('@NumeroRecibo').Value          := spObtenerRefinanciacionRecibosCuotasPagos.FieldByName('NumeroRecibo').AsInteger;
                    Parameters.ParamByName('@NumeroTurno').Value           := GNumeroTurno;
                    Parameters.ParamByName('@Usuario').Value               := UsuarioSistema;
                    Parameters.ParamByName('@NumeroReciboAnulacion').Value := null;

                    ExecProc;

                    if Parameters.ParamByName('@RETURN_VALUE').Value = 0 then begin
                        DMConnections.BaseCAC.CommitTrans;

                        spObtenerRefinanciacionRecibosCuotasPagos.Close;
                        spObtenerRefinanciacionRecibosCuotasPagos.Open;
                        spObtenerRefinanciacionRecibosCuotasPagos.Locate('NumeroRecibo', NumeroRecibo, []);

                        btnImprimirClick(Nil);
                    end
                    else raise EErrorExceptionCN.Create(MSG_PROCESO_TITULO, MSG_PROCESO_ERROR);
                end;
            except
                on e:Exception do begin
                    if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.RollbackTrans;
                    
                    ShowMsgBoxCN(e, Self);
                end;
            end;
        finally
            CambiarEstadoCursor(CURSOR_DEFECTO, Self, FComponentes);
            spObtenerRefinanciacionRecibosCuotasPagosAfterScroll(Nil);
        end
    end
    else ShowMsgBoxCN(MSG_ERROR_TURNO_TITULO, MSG_ERROR_TURNO_MENSAJE, MB_ICONWARNING, Self);
end;

procedure TRefinanciacionGestionRecibosForm.btnCancelarClick(Sender: TObject);
begin
    Close;
end;

procedure TRefinanciacionGestionRecibosForm.btnImprimirClick(Sender: TObject);
    var
        FLogo: TBitmap;
        r: TRefinanciacionReporteReciboForm;
        NumeroRecibo: Integer;
begin
    try
        try
            Screen.Cursor := crHourGlass;
            Application.ProcessMessages;

            FLogo := TBitMap.Create;

//            LevantarLogo(FLogo);

            with spObtenerRefinanciacionRecibosCuotasPagos do begin
                NumeroRecibo := FieldByName('NumeroRecibo').AsInteger;
            end;

            r := TRefinanciacionReporteReciboForm.Create(Nil);
            r.Inicializar(FLogo, NumeroRecibo, True, True);
        except
            on e: Exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
        if Assigned(r) then FreeAndNil(r);
        if Assigned(FLogo) then FreeAndNil(FLogo);
        
        Screen.Cursor := crDefault;
    end;
end;

procedure TRefinanciacionGestionRecibosForm.DBListEx1DrawBackground(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState; var DefaultDraw: Boolean);
begin
    with Sender do begin
        if not spObtenerRefinanciacionRecibosCuotasPagos.FieldByName('NumeroReciboAnulacion').IsNull then begin
            if (odSelected in State) then Canvas.Brush.Color := clMaroon;
        end;
    end;
end;

procedure TRefinanciacionGestionRecibosForm.DBListEx1DrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
    State: TOwnerDrawState; var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
    if not spObtenerRefinanciacionRecibosCuotasPagos.FieldByName('NumeroReciboAnulacion').IsNull then begin
        if Not (odSelected in State) then begin                                                              
            Sender.Canvas.Font.Color := clRed;
        end;                                                                                                 
    end;

    if (Column.FieldName = 'Importe') then begin
        Text := FormatFloat(FORMATO_IMPORTE, StrToFloat(Text)) + ' ';
    end;

    if (Column.FieldName = 'FechaPago') then begin
        Text := FormatDateTime('dd"-"mm"-"yyyy', spObtenerRefinanciacionRecibosCuotasPagos.FieldByName(Column.FieldName).AsDateTime);
    end;

    if (Column.FieldName = 'FechaCreacion') then begin
        Text := FormatDateTime('dd"-"mm"-"yyyy hh:nn:ss', spObtenerRefinanciacionRecibosCuotasPagos.FieldByName(Column.FieldName).AsDateTime);
    end;

    if (Column.FieldName = 'NumeroRecibo') or (Column.FieldName = 'Turno') or (Column.FieldName = 'NumeroReciboAnulacion') then begin 
        if Text <> EmptyStr then begin                                                                                                   
            Text := FormatFloat('#,###0', StrToFloat(Text)) + ' ';                                                                       
        end;
    end;
end;

procedure TRefinanciacionGestionRecibosForm.FormActivate(Sender: TObject);
begin
    TPanelMensajesForm.OcultaPanel;
end;

function TRefinanciacionGestionRecibosForm.Inicializar(pCodigoCliente: Integer; pRut, pDescripcionCliente: String): Boolean;
    resourcestring
        _Titulo = ' Gesti�n Recibos Cliente - [Rut: %s - %s]';
begin
    try
        Result := True;

        FBtnImprimirPermisos := ExisteAcceso('gest_recibos_impr_ref');
        FBtnAnularPermisos   := ExisteAcceso('gest_recibos_anul_ref');

        btnImprimir.Enabled  := False;
        btnAnular.Enabled    := False;

        with spObtenerRefinanciacionRecibosCuotasPagos do begin
            Parameters.ParamByName('@CodigoPersona').Value := pCodigoCliente;
            Open;
        end;

        Caption := Format(_Titulo, [pRut, pDescripcionCliente]);
    except
        on e:Exception do begin
            ShowMsgBoxCN(e, Self);
            Result := False;
        end;
    end;
end;

procedure TRefinanciacionGestionRecibosForm.spObtenerRefinanciacionRecibosCuotasPagosAfterScroll(DataSet: TDataSet);
begin
    btnImprimir.Enabled :=
        FBtnImprimirPermisos and
        (spObtenerRefinanciacionRecibosCuotasPagos.FieldByName('Importe').AsInteger > 0);

    btnAnular.Enabled   :=
        FBtnAnularPermisos and
        (spObtenerRefinanciacionRecibosCuotasPagos.FieldByName('Importe').AsInteger > 0) and
        (spObtenerRefinanciacionRecibosCuotasPagos.FieldByName('NumeroReciboAnulacion').IsNull);
end;

end.
