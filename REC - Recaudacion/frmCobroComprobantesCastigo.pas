{-----------------------------------------------------------------------------
 File Name      :   frmCobroComprobantesCastigo.pas
 Author         :   GLEON
 Date Created   :   25/11/2016
 Sign           :   TASK_020_GLE_20161121
 Description    :   Aplicaci�n del concepto "Castigo" de comprobantes impagos.
                    - Cuando se castiga una deuda el convenio se vuelve incobrable,
                    se contabiliza el castigo y los eventos facturables se castigan
                    (�tem 10.3 de CU.COBO.COB.202).
                    - Se debe actualizar el saldo del convenio.

 -----------------------------------------------------------------------------}
unit frmCobroComprobantesCastigo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ListBoxEx, DBListEx, StdCtrls, DPSControls, VariantComboBox,
  DmiCtrls, DB, ADODB, DMCOnnection, utilDB, UtilFacturacion, utilProc, buscaClientes,
  PeaTypes, RStrings, DBCtrls, peaProcs, Util, PagoVentanillaCastigo, ExtCtrls, ReporteRecibo,
  FrmImprimir, DBClient, ImgList, SysUtilsCN, PeaProcsCN, frmRefinanciacionGestion,
  Provider, frmRefinanciacionReporteRecibo, frmRefinanciacionEntrarEditarCuotas, frmRptDetInfraccionesAnuladas,
  FrmAnulacionRecibos, CobranzasResources, frmActivarRefinanciacion, frmRefinanciacionConsultas, Contnrs, frmMuestraMensaje;

const
	KEY_F9 	= VK_F9;

type
  TformCobroComprobantesCastigo = class(TForm)
    grpPorCliente: TGroupBox;
    lb_CodigoCliente: TLabel;
    lbl_NumeroComprobante: TLabel;
    lbl_NumeroConvenio: TLabel;
    lblMontoDeudasConvenio: TLabel;
    lblMontoDeudasRefinanciacion: TLabel;
    btnFiltrar: TButton;
    lblDeudasConvenio: TLabel;
    lblDeudasRefinanciacion: TLabel;
    grp_DatosCliente: TGroupBox;
    lbl_Nombre: TLabel;
    lblNombre: TLabel;
    lblDomicilio: TLabel;
    lbl_Domicilio: TLabel;
    lbl_NumeroDocumento: TLabel;
    lblRUT: TLabel;
    pgcComprobantesImpagos: TPageControl;
    tsComprobantes: TTabSheet;
    grp1: TGroupBox;
    lbl1: TLabel;
    lblTotalSelec: TLabel;
    dl_Comprobantes: TDBListEx;
    grpDatosComprobante: TGroupBox;
    lbl2: TLabel;
    lblFecha: TLabel;
    lblFechaVencimiento: TLabel;
    lbl3: TLabel;
    lblTotalAPagar: TLabel;
    lbl4: TLabel;
    lbl_Estado: TLabel;
    lbl5: TLabel;
    lbl6: TLabel;
    lblDescPATPAC: TLabel;
    lblEstadoDebito: TLabel;
    lbl7: TLabel;
    lblMedioPagoAutomatico: TLabel;
    lblCapRespuesta: TLabel;
    lblRespuestaDebito: TLabel;
    pnl1: TPanel;
    btnCastigar: TButton;
    btnSalir: TButton;
    tsCuotasCastigo: TTabSheet;
    pnl2: TPanel;
    btnAnularCastigo: TButton;
    btn1: TButton;
    lst1: TDBListEx;
    pnl3: TPanel;
    lbl8: TLabel;
    lblCuotasCantidad: TLabel;
    lbl9: TLabel;
    lblCuotasTotal: TLabel;
    pnl4: TPanel;
    tsRefinanciaciones: TTabSheet;
    pnl5: TPanel;
    btnActivarRefinanciacion: TButton;
    btn_Salir: TButton;
    btnConsultarRefinanciacion: TButton;
    lst2: TDBListEx;
    dsCuotasAPagar: TDataSource;
    ds_Comprobantes: TDataSource;
    cds_Comprobantes: TClientDataSet;
    cdsComprobantesCastigados: TClientDataSet;
    cdsCuotasAPagar: TClientDataSet;
    cdsCuotasAPagarRut: TStringField;
    cdsCuotasAPagarNombrePersona: TStringField;
    cdsCuotasAPagarCodigoRefinanciacion: TIntegerField;
    cdsCuotasAPagarCodigoCuota: TSmallintField;
    cdsCuotasAPagarCodigoEstadoCuota: TSmallintField;
    cdsCuotasAPagarDescripcionEstadoCuota: TStringField;
    cdsCuotasAPagarCodigoFormaPago: TSmallintField;
    cdsCuotasAPagarDescripcionFormaPago: TStringField;
    cdsCuotasAPagarTitularNombre: TStringField;
    cdsCuotasAPagarTitularTipoDocumento: TStringField;
    cdsCuotasAPagarTitularNumeroDocumento: TStringField;
    cdsCuotasAPagarCodigoBanco: TIntegerField;
    cdsCuotasAPagarDescripcionBanco: TStringField;
    cdsCuotasAPagarDocumentoNumero: TStringField;
    cdsCuotasAPagarFechaCuota: TDateTimeField;
    cdsCuotasAPagarImporte: TLargeintField;
    cdsCuotasAPagarImporteDesc: TStringField;
    cdsCuotasAPagarEstadoImpago: TBooleanField;
    cdsCuotasAPagarNumeroConvenio: TStringField;
    cdsCuotasAPagarSaldo: TLargeintField;
    cdsCuotasAPagarSaldoDesc: TStringField;
    cdsCuotasAPagarCodigoTarjeta: TIntegerField;
    cdsCuotasAPagarDescripcionTarjeta: TStringField;
    cdsCuotasAPagarTarjetaCuotas: TSmallintField;
    cdsCuotasAPagarCuotaNueva: TBooleanField;
    cdsCuotasAPagarCuotaModificada: TBooleanField;
    cdsCuotasAPagarCodigoTipoMedioPago: TSmallintField;
    cdsCuotasAPagarCodigoEntradaUsuario: TStringField;
    cdsCuotasAPagarCodigoCuotaAntecesora: TSmallintField;
    cdsCuotasAPagarCobrar: TBooleanField;
    dspCuotasAPagar: TDataSetProvider;
    lblEnListaAmarilla: TLabel;
    edNumeroComprobante: TNumericEdit;
    cbConvenios: TVariantComboBox;
    ilCheck: TImageList;
    cdsComprobantesImpagos: TClientDataSet;
    StringField1: TStringField;
    StringField2: TStringField;
    IntegerField1: TIntegerField;
    SmallintField1: TSmallintField;
    SmallintField2: TSmallintField;
    StringField3: TStringField;
    SmallintField3: TSmallintField;
    StringField4: TStringField;
    StringField5: TStringField;
    StringField6: TStringField;
    StringField7: TStringField;
    IntegerField2: TIntegerField;
    StringField8: TStringField;
    StringField9: TStringField;
    DateTimeField1: TDateTimeField;
    LargeintField1: TLargeintField;
    StringField10: TStringField;
    BooleanField1: TBooleanField;
    StringField11: TStringField;
    LargeintField2: TLargeintField;
    StringField12: TStringField;
    IntegerField3: TIntegerField;
    StringField13: TStringField;
    SmallintField4: TSmallintField;
    BooleanField2: TBooleanField;
    BooleanField3: TBooleanField;
    SmallintField5: TSmallintField;
    StringField14: TStringField;
    SmallintField6: TSmallintField;
    BooleanField4: TBooleanField;

    //***Stored Procedures
    spObtenerRefinanciacionCuotasAPagar: TADOStoredProc;
    spObtenerDeudaComprobantesVencidos: TADOStoredProc;
    spObtenerRefinanciacionesTotalesPersona: TADOStoredProc;
    spAnularPagosComprobantesCastigados: TADOStoredProc;
    sp_ActualizarClienteMoroso: TADOStoredProc;
    peNumeroDocumento: TPickEdit;
    spObtenerDomicilioFacturacion: TADOStoredProc;
    spObtenerComprobantesCastigados: TADOStoredProc;
    btnComprobantesCastigos: TButton;
    ds_ComprobantesCastigados: TDataSource;
    spActualizarEstadoConvenio: TADOStoredProc;
    spEliminarEstadoConvenio: TADOStoredProc;
    spIngresarConvenioACobranzaInterna: TADOStoredProc;

    procedure edNumeroComprobanteChange(Sender: TObject);
    procedure btnCastigarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure btnAnularCastigoClick(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);

    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure peNumeroDocumentoChange(Sender: TObject);
    procedure peNumeroDocumentoButtonClick(Sender: TObject);
    procedure dl_ComprobantesClick(Sender: TObject);
    procedure lst1Click(Sender: TObject);
    procedure cbConveniosChange(Sender: TObject);
    procedure edNumeroComprobanteEnter(Sender: TObject);
    procedure dl_ComprobantesDrawText(Sender: TCustomDBListEx;Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState; var Text: String; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);

    procedure btn1Click(Sender: TObject);
    procedure btnComprobantesCastigosClick(Sender: TObject);
    procedure lst1DrawBackground(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
        var DefaultDraw: Boolean);
    procedure lst1DrawText(Sender: TCustomDBListEx;Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState; var Text: String; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
    procedure pnl2Click(Sender: TObject);
    
  private
    { Private declarations }

    //***Variables***
    FLogo: TBitmap;
    FMinHeight, FMinWidth : integer; //para que no se redimensione muy peque�o
    FAhora: TDate;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;
    FbtnCobrarCuotaPermisos: Boolean; 
    FTotalComprobantesCobrar: Double;
    FTotal: Int64;
    FCantidadComprobantesCobrar,
    FCantidadCuotasCobrar,
    FCantidadComprobantesCastigados: Integer;
    FTotalCuotasCobrar: Int64;
    FCodigoCliente: integer;
    FCodigoCanalPago: Integer;
    FTotalApagar: Double;
    FTipoComprobante: AnsiString;
    FNumeroComprobante: Int64;
    FUltimoComprobante: Int64;
    FNumeroPOS, FPuntoEntrega, FPuntoVenta: Integer;
    FSaldoConvenios : int64;
    FComprobantesEnCobranza: TStringList;
    FUltimoNumeroDocumento: String;
    FUltimaBusqueda: TBusquedaCliente;

    //***Procedures***
    procedure LimpiarDatosCliente;
    procedure LimpiarDetalles;
    procedure MostrarDatosComprobante;
    procedure MostrarDatosComprobantesCastigados;
    procedure MostrarDatosCliente(CodigoCLiente: Integer);
    procedure MostrarMensajeRUTConvenioCuentas(NumeroDocumento: String);
    procedure MostrarMensajeTipoClienteSistema(NumeroDocumento: String);
    procedure MostrarSaldosPersona(CodigoPersona: Integer);

    procedure LimpiarConvenios;
    procedure LimpiarListaComprobantes;
    procedure HabilitarFiltroRUT(Valor: Boolean);
    procedure Castigar;
  	procedure LimpiarCampos;
    procedure VerificaComprobantesMarcados;
    procedure CargarComprobantesCastigados(CodigoPersona: Integer);

    procedure lst1LinkClick(Sender: TCustomDBListEx; Column: TDBListExColumn);
    procedure VerificarCuotasACobrar;
    procedure MarcarComprobantes(Marcar: Boolean);
    procedure cbConveniosDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);

    //***Procedure Timer1Timer(Sender: TObject);
    procedure cbComprobantesChange(Sender: TObject);
    procedure FormResize(Sender: TObject);

    //***Functions***
  	function CargarComprobantes : boolean;
  	function VerificarComprobanteEnCarpetaLegal(ModoSilencioso: Boolean): Boolean;
    function VerificaPersonaListaAmarilla(RutCliente : string) : Boolean;
    function ObtenerComprobantesCobrar: TListaComprobante;
    function ImprimirDetalleInfraccionesAnuladas(TipoComprobante: string; NroComprobante: int64): Boolean;
    function TienePagoAutomatico(TipoComprobante: AnsiString; NumeroComprobante: Integer; var TipoMedioPago: Integer): Boolean;
    function VerificarConvenioListaAmarilla(CodigoConvenio : Integer) : String;

  public
    { Public declarations }
    //***Variables***
    FComponentes: TObjectList;

    //***Functions***
    function Inicializar(TipoComprobante: AnsiString; NumeroComprobante: Int64; NumeroPOS, PuntoEntrega, PuntoVenta: Integer): Boolean; Overload;
    function Inicializar(NumeroDocumento: String; CodigoConvenio: Integer; NumeroPOS, PuntoEntrega, PuntoVenta: Integer): Boolean; Overload;	//TASK_124_CFU_20170320
    function Inicializar(NumeroPOS, PuntoEntrega, PuntoVenta: Integer): Boolean; overload;
    function PoseeCanalDePago: Boolean;
  end;

var
  formCobroComprobantesCastigo: TformCobroComprobantesCastigo;

const													//TASK_124_CFU_20170320
	CONST_ESTADO_CONVENIO_CASTIGADO	= 5;				//TASK_124_CFU_20170320

implementation

resourceString
    MSG_SIN_PERMISOS = 'Ud. no tiene permisos para trabajar con canales de pago. Consulte al administrador del sistema.';

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name :   Inicializar
  Author        :   GLEON
  Date Created  :   25/11/2016
  Description   :   Inicializa el Form - Carga los Comprobantes Impagos
                    (Se usa de referencia el formCobroComprobantes)
  Parameters    :
  Return Value  :   Boolean
-----------------------------------------------------------------------------}
function TformCobroComprobantesCastigo.Inicializar(NumeroPOS, PuntoEntrega, PuntoVenta: Integer): Boolean;
resourcestring
	MSG_LOGO_NO_ENCONTRADO  = 'No se ha encontrado el archivo con la imagen del logo para el recibo.';
	MSG_CAPTION             = 'Cobro de Comprobantes Impagos';
	MSG_ERROR_INICIALIZAR   = 'Error inicializando el formulario.';
begin
    try
        Result := False;

        FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));
        FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));
        FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));
        FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));
        Color := FColorMenu;

        FMinHeight := ClientHeight;
        FMinWidth  := ClientWidth;

        FAhora := Trunc(NowBaseSmall(DMConnections.BaseCAC));

        CargarConstantesCobranzasRefinanciacion;
        CargarConstantesCobranzasCanalesFormasPago;

        FbtnCobrarCuotaPermisos           := ExisteAcceso('cobrar_cuota_cob_comp_ref');

        if not PoseeCanalDePago then begin
            MsgBox(MSG_SIN_PERMISOS, MSG_CAPTION, MB_ICONINFORMATION + MB_OK);
            Exit;
        end;

        //Activamos los Client Data Sets
        if not cds_Comprobantes.Active then cds_Comprobantes.Active := True;
        if not cdsComprobantesCastigados.Active then cdsComprobantesCastigados.Active := True;
        
        //Inhabilitamos los botones hasta que haya un valor
        btnFiltrar.Enabled  := False;
        btnComprobantesCastigos.Enabled := False;
        btnAnularCastigo.Enabled := False;

        FTotalComprobantesCobrar := 0;
        FCantidadComprobantesCobrar := 0;
        FCantidadCuotasCobrar := 0;
        FCantidadComprobantesCastigados := 0;
        FTotalCuotasCobrar    := 0;
        lblTotalSelec.Caption:='$ 0';

        lblCuotasCantidad.Caption := '0';
        lblCuotasTotal.Caption    := '$ 0';

        lblMontoDeudasConvenio.Caption         := '$ 0';
        lblMontoDeudasRefinanciacion.Caption    := '$ 0';
        lblMontoDeudasConvenio.Font.Color      := clBlack;
        lblMontoDeudasRefinanciacion.Font.Color := clBlack;

        FCodigoCliente := -1;
        FCodigoCanalPago := CONST_CANAL_PAGO_VENTANILLA;

        LimpiarDatosCliente;
        LimpiarConvenios;   //TASK_049_GLE_20170207
        lbl_Estado.Caption := EmptyStr;

        MostrarDatosComprobante;
        FNumeroPOS := NumeroPOS;
        FPuntoEntrega := PuntoEntrega;
        FPuntoVenta := PuntoVenta;

        cds_Comprobantes.Close;
        cds_Comprobantes.CreateDataSet;
        cds_Comprobantes.Open;

        cdsComprobantesCastigados.Close;
        cdsComprobantesCastigados.CreateDataSet;
        cdsComprobantesCastigados.Open;

        //Se levanta el logo aqu� para que se haga una sola vez, independiente de los reintentos de impresi�n que haga el operador
        FLogo := TBitmap.Create;

    	if edNumeroComprobante.Enabled then begin
        	ActiveControl := edNumeroComprobante;
        end
        else begin
        	// Si no est� habilitado el dNumeroComprobante, por definici�n est� habilitado el RUT entonces
            // Esto se hace porque da un error de "cannot focus a disabled...window"
            ActiveControl := peNumeroDocumento;
        end;

        Result := True;
        TPanelMensajesForm.OcultaPanel; //TASK_049_GLE_20170207
    except
	   	  on e: exception do begin
			      MsgBoxErr(MSG_ERROR_INICIALIZAR, E.Message, MSG_CAPTION, MB_ICONERROR);
			      Result := False;
        end;
    end; // except

    lblEnListaAmarilla.Visible:=False;
end;

{******************************** Function Header ******************************
Function Name   :   PoseeCanalDePago
Author          :   vpaszkowicz
Date Created    :   24/08/2007
Description     :   Retorna True si existe al menos un Canal de Pago habilidato para
                    ese usuario.
Parameters      :   None
Return Value    :   Boolean
*******************************************************************************}
function TformCobroComprobantesCastigo.PoseeCanalDePago: Boolean;
var
    sp: TADOStoredProc;
begin
    Result := False;
    sp := TADOStoredProc.Create(Nil);
    try
        sp.Connection       := DmConnections.BaseCAC;
        sp.ProcedureName    := 'ObtenerCanalesPago';
        sp.Parameters.Refresh;
        sp.Open;
        while (not sp.Eof) and not Result do begin
            if (not sp.FieldByName('Funcion').IsNull) and (ExisteAcceso(Trim(sp.FieldByName('Funcion').Value))) then begin
                Result := True;
            end;
            sp.Next;
        end;
    finally
        sp.Close;
        sp.Free;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name :   LimpiarDatosCliente
  Author        :   flamas
  Date Created  :   13/10/2005
  Description   :   Limpia los datos del clientes
  Parameters    :   None
  Return Value  :   Boolean
-----------------------------------------------------------------------------}
procedure TformCobroComprobantesCastigo.LimpiarDatosCliente;
begin
    lblRUT.Caption := EmptyStr;
    lblNombre.Caption := EmptyStr;
    lblDomicilio.Caption := EmptyStr;
    lblEnListaAmarilla.Visible  := False;
    lblEnListaAmarilla.Caption  := EmptyStr;
end;

{-----------------------------------------------------------------------------
  Function Name :   MostrarDatosComprobante
  Author        :   flamas
  Date Created  :   20/12/2004
  Description   :   Muestra los datos del Comprobante
  Parameters    :   None
  Return Value  :   None
-----------------------------------------------------------------------------}
procedure TformCobroComprobantesCastigo.MostrarDatosComprobante;
var
    EstadoDebito: char;
begin
        if cds_Comprobantes.RecordCount > 0 then begin

    	lblFecha.Caption            	:= FormatDateTime('dd/mm/yyyy', cds_Comprobantes.FieldByName('FechaEmision').AsDateTime);
    	lblFechaVencimiento.Caption 	:= FormatDateTime('dd/mm/yyyy', cds_Comprobantes.FieldByName('FechaVencimiento').AsDateTime);
        FTotalApagar                    := Round(cds_Comprobantes.FieldByName('SaldoPendiente').AsFloat);
        lblTotalSelec.Caption           := FloatToStr(FTotalApagar);
        lbl_Estado.Caption              := cds_Comprobantes.FieldByName('DescriEstado').AsString;
        FTipoComprobante                := cds_Comprobantes.FieldByName('TipoComprobante').AsString;
        FNumeroComprobante              := cds_Comprobantes.FieldByName('NumeroComprobante').AsInteger;

        FUltimoComprobante	:= cds_Comprobantes.FieldByName('UltimoComprobante').AsInteger;

        //carga datos del pago automatico...
        EstadoDebito := cds_Comprobantes.FieldByName('EstadoDebito').AsString[1];
        lblMedioPagoAutomatico.Caption := cds_Comprobantes.FieldByName('DescTipoMedioPAgo').AsString;
        lblDescPATPAC.Caption := cds_Comprobantes.FieldByName('DescPATPAC').AsString;
        lblRespuestaDebito.Caption := cds_Comprobantes.FieldByName('Rechazo').AsString;
        lblCapRespuesta.Visible := lblRespuestaDebito.Caption <> '';

        case EstadoDebito of
            'C': lblEstadoDebito.Caption := 'Cancelado';
            'N': lblEstadoDebito.Caption := 'No Aplica';
            'E': lblEstadoDebito.Caption := 'Enviado';
            'P': lblEstadoDebito.Caption := 'Pendiente';
        end;

    end else begin
        LimpiarDetalles;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name :   MostrarDatosComprobantesCastigados
  Author        :   GLEON
  Date Created  :   09/12/2016
  Description   :   Muestra los datos de los Comprobantes Castigados
  Parameters    :   None
  Return Value  :   None
-----------------------------------------------------------------------------}
procedure TformCobroComprobantesCastigo.MostrarDatosComprobantesCastigados;

begin
        if cdsComprobantesCastigados.RecordCount > 0 then begin

        lblMontoDeudasRefinanciacion.Caption    := '$ 0';
        lblMontoDeudasRefinanciacion.Font.Color := clBlack;

        lblCuotasCantidad.Caption               := FORMATFLOAT(FORMATO_IMPORTE,cdsComprobantesCastigados.FieldByName('TotalComprobante').AsInteger);  //   /100

    	lblFecha.Caption            	        := FormatDateTime('dd/mm/yyyy', cdsComprobantesCastigados.FieldByName('FechaEmision').AsDateTime);
    	lblFechaVencimiento.Caption 	        := '';
        lblEstadoDebito.Caption                 := '';
        lblMedioPagoAutomatico.Caption          := '';
        lblTotalAPagar.Caption                  := '$ 0';

        lblDescPATPAC.Caption                   := '';
        lblRespuestaDebito.Caption              := '';

    end else begin
        LimpiarDetalles;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: LimpiarDetalles;
  Author:    flamas
  Date Created: 26/01/2005
  Description: Borra los detalles del Comprobante
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformCobroComprobantesCastigo.LimpiarDetalles;
begin
  	lblFecha.Caption                := EmptyStr;
  	lblFechaVencimiento.Caption	    := EmptyStr;
    lblTotalAPagar.Caption          := EmptyStr;
    lbl_Estado.Caption              := EmptyStr;
    lblMedioPagoAutomatico.Caption  := EmptyStr;
    lblMontoDeudasConvenio.Caption  := EmptyStr;
    lblMontoDeudasRefinanciacion.Caption := EmptyStr;
  	lblDescPATPAC.Caption           := EmptyStr;
    lblEstadoDebito.Caption         := EmptyStr;
    lblRespuestaDebito.Caption      := EmptyStr;
  	lblCapRespuesta.visible         := False;
    lblTotalSelec.Caption           := EmptyStr;
 	btnCastigar.Enabled     		:= False;
end;

{-----------------------------------------------------------------------------
  Function Name :   Inicializar
  Author        :   cfuentesc
  Date Created  :   24/03/2017
  Description   :   Inicializa el Form - Carga los Comprobantes del RUT/Convenio recibido
  Parameters    :   NumeroDocumento(RUT Cliente): string; CodigoConvenio: Integer; 
  Return Value  :   Boolean
  Firma         :   TASK_124_CFU_20170320
-----------------------------------------------------------------------------}
function TformCobroComprobantesCastigo.Inicializar(NumeroDocumento: String; CodigoConvenio: Integer; NumeroPOS, PuntoEntrega, PuntoVenta: Integer): Boolean;
resourcestring
	MSG_LOGO_NO_ENCONTRADO = 'No se ha encontrado el archivo con la imagen del logo para el recibo.';
	MSG_CAPTION			   = 'Cobro de Comprobantes';
	MSG_ERROR_INICIALIZAR  = 'Error inicializando el formulario.';
var
	i: Integer;
	strPaso: string;
begin
    try
        Result := False;
        FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));
        FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));
        FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));
        FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));
        Color := FColorMenu;

        FMinHeight := ClientHeight;
        FMinWidth  := ClientWidth;

        FAhora              := Trunc(NowBaseSmall(DMConnections.BaseCAC));

        CargarConstantesCobranzasRefinanciacion;
        CargarConstantesCobranzasCanalesFormasPago;

        FbtnCobrarCuotaPermisos           := ExisteAcceso('cobrar_cuota_cob_comp_ref');

        if not PoseeCanalDePago then begin
            MsgBox(MSG_SIN_PERMISOS, MSG_CAPTION, MB_ICONINFORMATION + MB_OK);
            Exit;
        end;

        FTotalComprobantesCobrar := 0;
        FCantidadComprobantesCobrar := 0;
        FCantidadCuotasCobrar := 0;
        FCantidadComprobantesCastigados := 0;
        FTotalCuotasCobrar := 0;

        lblCuotasCantidad.Caption := '0';
        lblCuotasTotal.Caption    := '$ 0';

        lblMontoDeudasConvenio.Caption         := '$ 0';
        lblMontoDeudasRefinanciacion.Caption    := '$ 0';
        lblMontoDeudasConvenio.Font.Color      := clBlack;
        lblMontoDeudasRefinanciacion.Font.Color := clBlack;

        FCodigoCliente := -1;
        FCodigoCanalPago := CONST_CANAL_PAGO_VENTANILLA;

        LimpiarDatosCliente;

        lbl_Estado.Caption := EmptyStr;
        peNumeroDocumento.Text	:= NumeroDocumento;
        peNumeroDocumentoChange(nil);
        for i := 0 to cbConvenios.Items.Count - 1 do begin
        	if cbConvenios.Items[i].Value = CodigoConvenio then
            	cbConvenios.ItemIndex	:= i;
        end;
        //cbConvenios.ItemIndex	:= cbConvenios.Items.IndexOf(NumeroConvenio);
        FNumeroPOS := NumeroPOS;
        FPuntoEntrega := PuntoEntrega;
        FPuntoVenta := PuntoVenta;

        cds_Comprobantes.Close;
        cds_Comprobantes.CreateDataSet;
        cds_Comprobantes.Open;

        cdsComprobantesCastigados.Close;
        cdsComprobantesCastigados.CreateDataSet;
        cdsComprobantesCastigados.Open;

        FormStyle := fsNormal;
        CenterForm(Self);

        Result := CargarComprobantes;

        if Result then
            //Se levanta el logo aqu� para que se haga una sola vez, independiente de los reintentos de impresi�n que haga el operador
            FLogo := TBitmap.Create;
    except
        on e: exception do begin
            MsgBoxErr(MSG_ERROR_INICIALIZAR, E.Message, MSG_CAPTION, MB_ICONERROR);
    		Result := False;
        end;
    end; // except
end;

{-----------------------------------------------------------------------------
  Function Name :   Inicializar
  Author        :   flamas
  Date Created  :   21/12/2004
  Description   :   Inicializa el Form - Carga los tipos de Comprobantes y busca
				    el comprobante seleccionado
  Parameters    :   TipoComprobante: AnsiString; NumeroComprobante: Int64
  Return Value  :   Boolean
-----------------------------------------------------------------------------}
function TformCobroComprobantesCastigo.Inicializar(TipoComprobante: AnsiString; NumeroComprobante: Int64; NumeroPOS, PuntoEntrega, PuntoVenta: Integer): Boolean;
resourcestring
	MSG_LOGO_NO_ENCONTRADO = 'No se ha encontrado el archivo con la imagen del logo para el recibo.';
	MSG_CAPTION			   = 'Cobro de Comprobantes';
	MSG_ERROR_INICIALIZAR  = 'Error inicializando el formulario.';
begin
    try
        Result := False;
        FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));
        FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));
        FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));
        FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));
        Color := FColorMenu;

        FMinHeight := ClientHeight;
        FMinWidth  := ClientWidth;

        FAhora              := Trunc(NowBaseSmall(DMConnections.BaseCAC));

        CargarConstantesCobranzasRefinanciacion;
        CargarConstantesCobranzasCanalesFormasPago;

        FbtnCobrarCuotaPermisos           := ExisteAcceso('cobrar_cuota_cob_comp_ref');


        if not PoseeCanalDePago then begin
            MsgBox(MSG_SIN_PERMISOS, MSG_CAPTION, MB_ICONINFORMATION + MB_OK);
            Exit;
        end;

        FTotalComprobantesCobrar := 0;
        FCantidadComprobantesCobrar := 0;
        FCantidadCuotasCobrar := 0;
        FCantidadComprobantesCastigados := 0;
        FTotalCuotasCobrar := 0;

        lblCuotasCantidad.Caption := '0';
        lblCuotasTotal.Caption    := '$ 0';

        lblMontoDeudasConvenio.Caption         := '$ 0';
        lblMontoDeudasRefinanciacion.Caption    := '$ 0';
        lblMontoDeudasConvenio.Font.Color      := clBlack;
        lblMontoDeudasRefinanciacion.Font.Color := clBlack;

        FCodigoCliente := -1;
        FCodigoCanalPago := CONST_CANAL_PAGO_VENTANILLA;

        LimpiarDatosCliente;

        lbl_Estado.Caption := EmptyStr;
        edNumeroComprobante.Value := NumeroComprobante;
        FNumeroPOS := NumeroPOS;
        FPuntoEntrega := PuntoEntrega;
        FPuntoVenta := PuntoVenta;

        cds_Comprobantes.Close;
        cds_Comprobantes.CreateDataSet;
        cds_Comprobantes.Open;

        cdsComprobantesCastigados.Close;
        cdsComprobantesCastigados.CreateDataSet;
        cdsComprobantesCastigados.Open;

        FormStyle := fsMDIChild;
        CenterForm(Self);

        Result := CargarComprobantes;

        if Result then begin
            //Se levanta el logo aqu� para que se haga una sola vez, independiente de los reintentos de impresi�n que haga el operador
            FLogo := TBitmap.Create;

            edNumeroComprobante.SetFocus();
        end;
    except
        on e: exception do begin
            MsgBoxErr(MSG_ERROR_INICIALIZAR, E.Message, MSG_CAPTION, MB_ICONERROR);
    		Result := False;
        end;
    end; // except
    //TPanelMensajesForm.OcultaPanel;  //TASK_049_GLE_20170207

end;

{-----------------------------------------------------------------------------
  Function Name :   CargarComprobantes
  Author        :   flamas
  Date Created  :   20/12/2004
  Description   :   Carga los comprobantes impagos de un Convenio determinado
  Parameters    :   None
  Return Value  :   None

  Revision      :   16
  Date          :   17/11/2010
  Author        :   pdominguez
  Description   :   - Salir todas NK seleccionadas y advertencia anteriores impagas
                    - Si la b�squeda de comprobantes es por rut, marcaremos todos los comprobantes.
                    - Si se busca un comprobante en concreto, a parte de marcar ese, marcaremos
                    todos los anteriores que haya.
-----------------------------------------------------------------------------}
function TformCobroComprobantesCastigo.CargarComprobantes : boolean;
const
    SQL_ObtenerPersonaComprobante = 'SELECT dbo.ObtenerPersonaComprobante(''%s'', %d, %s)';
resourcestring
    MSG_ERROR_ON_LOAD = 'Ha ocurrido un error al intentar obtener los Comprobantes Impagos.';
    MSG_COMPROBANTES_COBRANZA_JUDICIAL = 'Existen Comprobantes en Cobranza Judicial';
    MSG_MULTIPLES_COMPROBANTES_EN_COBRANZA = 'El Convenio tiene un total de %d comprobantes en Cobranza Judicial.';
var
    EncontradoComprobante: Boolean;
    ObtenerComprobantesImpagos : TADOStoredProc;
    ObtenerComprobantesImpagosOtros : TADOStoredProc;
begin

    //    TPanelMensajesForm.MuestraMensaje('Cargando Comprobantes y Datos del Cliente ...', True); //TASK_049_GLE_20170207
    cds_Comprobantes.DisableControls;
    Result := False ;
    FTotalComprobantesCobrar := 0;

    //Se inicializa la variable de saldo del convenio si eligi� un solo comprobante, o de todos los convenios si eligi� un rut
    FSaldoConvenios := 0;
    EncontradoComprobante := False;

  	try
        FComprobantesEnCobranza := TStringList.Create;

        try
            // Se obtienen los datos
            //TPanelMensajesForm.MuestraMensaje('Obteniendo Comprobantes Impagos ...'); //TASK_049_GLE_20170207

            ObtenerComprobantesImpagos:= TADOStoredProc.Create(nil);
            ObtenerComprobantesImpagos.Connection := DMConnections.BaseCAC;
            ObtenerComprobantesImpagos.ProcedureName := 'ObtenerComprobantesImpagos';
            ObtenerComprobantesImpagos.Close;          //TASK_049_GLE_20170207
            ObtenerComprobantesImpagos.Parameters.Refresh;
            ObtenerComprobantesImpagos.Parameters.ParamByName('@TipoComprobanteFiscal').Value := null;
            ObtenerComprobantesImpagos.Parameters.ParamByName('@NumeroComprobanteFiscal').Value := null;
            ObtenerComprobantesImpagos.Parameters.ParamByName('@CodigoCliente').Value := iif(FCodigoCliente <= 0, Null, FCodigoCliente);
            ObtenerComprobantesImpagos.Parameters.ParamByName('@CodigoConvenio').Value := iif(cbConvenios.Value = 0, Null, cbConvenios.Value);

            ObtenerComprobantesImpagos.Open;
            ObtenerComprobantesImpagos.ExecProc;     //TASK_049_GLE_20170207

            if ObtenerComprobantesImpagos.RecordCount > 0 then begin
                if FCodigoCliente <= 0 then begin
                    FCodigoCliente := ObtenerComprobantesImpagos.FieldByName('CodigoPersona').Value;
                    MostrarDatosCliente(FCodigoCliente);
                end;
                pgcComprobantesImpagos.ActivePageIndex := 0;
            end
            else begin
                //TPanelMensajesForm.MuestraMensaje('Obteniendo Datos Cliente ...');    //TASK_049_GLE_20170207
                FCodigoCliente := QueryGetValueInt( DMConnections.BaseCAC,
                                            Format('SELECT CodigoPersona FROM Personas WITH (NOLOCK) WHERE CodigoDocumento = ''RUT'' ' + 'AND NumeroDocumento = ''%s''',
                                            [iif(peNumeroDocumento.Text <> '', trim(PadL(peNumeroDocumento.Text, 9, '0')), '')]));

                if FCodigoCliente <> -1 then begin
                    MostrarDatosCliente(FCodigoCliente);

                    ObtenerComprobantesImpagosOtros:= TADOStoredProc.Create(nil);
                    ObtenerComprobantesImpagosOtros.Connection := DMConnections.BaseCAC;
                    ObtenerComprobantesImpagosOtros.ProcedureName := 'ObtenerComprobantesImpagos';
                    ObtenerComprobantesImpagosOtros.Parameters.Refresh;
                    ObtenerComprobantesImpagosOtros.Parameters.ParamByName('@TipoComprobanteFiscal').Value := null;
                    ObtenerComprobantesImpagosOtros.Parameters.ParamByName('@NumeroComprobanteFiscal').Value := null; //edNumeroComprobante.Value;
                    ObtenerComprobantesImpagosOtros.Parameters.ParamByName('@CodigoCliente').Value := iif(FCodigoCliente <= 0, Null, FCodigoCliente);
                    ObtenerComprobantesImpagosOtros.Parameters.ParamByName('@CodigoConvenio').Value := iif(cbConvenios.Value = 0, Null, cbConvenios.Value);
                    ObtenerComprobantesImpagosOtros.Parameters.ParamByName('@ErrorDescription').Value := Null;
                    ObtenerComprobantesImpagosOtros.Open;

                end;

                if cdsCuotasAPagar.Active then begin
                    if cdsCuotasAPagar.RecordCount > 0 then begin
                        pgcComprobantesImpagos.ActivePageIndex := 1;
                    end;
                end;
            end;

            // Se carga el ClientDataSet
            //TPanelMensajesForm.MuestraMensaje('Cargando Comprobantes Impagos ...'); //TASK_049_GLE_20170207

            while not ObtenerComprobantesImpagos.Eof do begin
                cds_Comprobantes.Append;

                cds_Comprobantes.FieldByName('TipoComprobante').AsString := ObtenerComprobantesImpagos.FieldByName('TipoComprobante').AsString;
                cds_Comprobantes.FieldByName('NumeroComprobante').AsInteger := ObtenerComprobantesImpagos.FieldByName('NumeroComprobante').AsInteger;
                cds_Comprobantes.FieldByName('DescriComprobante').AsString := ObtenerComprobantesImpagos.FieldByName('DescriComprobante').AsString;
                cds_Comprobantes.FieldByName('FechaEmision').AsDateTime := ObtenerComprobantesImpagos.FieldByName('FechaEmision').AsDateTime;
                cds_Comprobantes.FieldByName('FechaVencimiento').AsDateTime := ObtenerComprobantesImpagos.FieldByName('FechaVencimiento').asDateTime;
                cds_Comprobantes.FieldByName('TotalComprobante').AsFloat := ObtenerComprobantesImpagos.FieldByName('TotalComprobante').AsFloat;
                cds_Comprobantes.FieldByName('TotalComprobanteDescri').AsString := ObtenerComprobantesImpagos.FieldByName('TotalComprobanteDescri').AsString;
                cds_Comprobantes.FieldByName('SaldoPendiente').AsFloat := ObtenerComprobantesImpagos.FieldByName('SaldoPendiente').AsFloat;
                cds_Comprobantes.FieldByName('SaldoPendienteDescri').AsString := ObtenerComprobantesImpagos.FieldByName('SaldoPendienteDescri').AsString;
                cds_Comprobantes.FieldByName('EstadoPago').AsString := ObtenerComprobantesImpagos.FieldByName('EstadoPago').AsString;
                cds_Comprobantes.FieldByName('DescriEstado').AsString := ObtenerComprobantesImpagos.FieldByName('DescriEstado').AsString;
                cds_Comprobantes.FieldByName('EstadoDebito').AsString := ObtenerComprobantesImpagos.FieldByName('EstadoDebito').AsString;
                cds_Comprobantes.FieldByName('UltimoComprobante').AsInteger := iif(ObtenerComprobantesImpagos.FieldByName('UltimoComprobante').IsNull, -1, ObtenerComprobantesImpagos.FieldByName('UltimoComprobante').AsInteger);
                cds_Comprobantes.FieldByName('CodigoConvenio').AsInteger := ObtenerComprobantesImpagos.FieldByName('CodigoConvenio').AsInteger;
                cds_Comprobantes.FieldByName('CodigoPersona').AsInteger := ObtenerComprobantesImpagos.FieldByName('CodigoPersona').AsInteger;
                cds_Comprobantes.FieldByName('TotalPagos').AsFloat := ObtenerComprobantesImpagos.FieldByName('TotalPagos').AsFloat;
                cds_Comprobantes.FieldByName('TotalPagosStr').AsString := ObtenerComprobantesImpagos.FieldByName('TotalPagosStr').AsString;
                //Se cargan los nuevos valores del sp en los nuevos campos del client data set
                cds_Comprobantes.FieldByName('DescTipoMedioPAgo').AsString := ObtenerComprobantesImpagos.FieldByName('DescTipoMedioPAgo').AsString;
                cds_Comprobantes.FieldByName('DescPATPAC').AsString := ObtenerComprobantesImpagos.FieldByName('DescPATPAC').AsString;
                cds_Comprobantes.FieldByName('Rechazo').AsString := ObtenerComprobantesImpagos.FieldByName('Rechazo').AsString;
                //Si el comprobante fue cargado en el edit lo marcamos
                cds_Comprobantes.FieldByName('TipoComprobanteFiscal').AsString := ObtenerComprobantesImpagos.FieldByName('TipoComprobanteFiscal').AsString;
                cds_Comprobantes.FieldByName('NumeroComprobanteFiscal').AsInteger := ObtenerComprobantesImpagos.FieldByName('NumeroComprobanteFiscal').AsInteger;
                cds_Comprobantes.FieldByName('DescriComprobanteFiscal').AsString := ObtenerComprobantesImpagos.FieldByName('DescriComprobanteFiscal').AsString;
                cds_Comprobantes.FieldByName('EstadoRefinanciacion').AsInteger := ObtenerComprobantesImpagos.FieldByName('EstadoRefinanciacion').AsInteger;

                FTotal :=  ObtenerComprobantesImpagos.FieldByName('SaldoPendiente').AsInteger;

                if not (ObtenerComprobantesImpagos.FieldByName('EstadoRefinanciacion').AsInteger in [REFINANCIACION_ESTADO_EN_TRAMITE, REFINANCIACION_ESTADO_ACEPTADA]) then begin
                    cds_Comprobantes.FieldByName('Cobrar').AsBoolean := True;
                    inc(FCantidadComprobantesCobrar);
                    FTotalComprobantesCobrar := FTotalComprobantesCobrar + cds_Comprobantes.FieldByName('SaldoPendiente').AsFloat;

                    if not EncontradoComprobante then begin
                        cds_Comprobantes.FieldByName('Cobrar').AsBoolean := True;
                        inc(FCantidadComprobantesCobrar);
                        //FTotalComprobantesCobrar := FTotalComprobantesCobrar + cds_Comprobantes.FieldByName('SaldoPendiente').AsFloat;        //GLEON Comenta

                        if ((cds_Comprobantes.FieldByName('NumeroComprobante').asInteger = edNumeroComprobante.ValueInt)) or
                            ((cds_Comprobantes.FieldByName('NumeroComprobanteFiscal').asInteger = edNumeroComprobante.ValueInt)) then begin

                                EncontradoComprobante := True;
                        end
                        else
                        //cds_Comprobantes.FieldByName('Cobrar').AsBoolean := False; //GLEON Comenta

                    end;
                end;

                VerificarComprobanteEnCarpetaLegal(True);

                cds_Comprobantes.Post;
                ObtenerComprobantesImpagos.Next;

                FSaldoConvenios := FSaldoConvenios +  FTotal;
            end;

           if edNumeroComprobante.Value <> 0 then begin

                //TPanelMensajesForm.MuestraMensaje('Cargando Nota de Cobro ...'); //TASK_049_GLE_20170207
                //Si el Numero de Comprobante es distinto de cero, entonces limpio el cds de comprobantes
                //para agregar s�lo el buscado por el cliente
                cds_Comprobantes.EmptyDataSet;

                ObtenerComprobantesImpagosOtros:= TADOStoredProc.Create(nil);
                ObtenerComprobantesImpagosOtros.Connection := DMConnections.BaseCAC;
                ObtenerComprobantesImpagosOtros.ProcedureName := 'ObtenerComprobantesImpagos';
                ObtenerComprobantesImpagosOtros.Parameters.Refresh;
                ObtenerComprobantesImpagosOtros.Parameters.ParamByName('@TipoComprobanteFiscal').Value := null;
                ObtenerComprobantesImpagosOtros.Parameters.ParamByName('@NumeroComprobanteFiscal').Value := null; //edNumeroComprobante.Value;
                ObtenerComprobantesImpagosOtros.Parameters.ParamByName('@CodigoCliente').Value := iif(FCodigoCliente <= 0, Null, FCodigoCliente);
                ObtenerComprobantesImpagosOtros.Parameters.ParamByName('@CodigoConvenio').Value := iif(cbConvenios.Value = 0, Null, cbConvenios.Value);
                ObtenerComprobantesImpagosOtros.Parameters.ParamByName('@ErrorDescription').Value := Null;
                ObtenerComprobantesImpagosOtros.Open;

                while not ObtenerComprobantesImpagosOtros.Eof do begin

                    if ObtenerComprobantesImpagosOtros.FieldByName('NumeroComprobante').AsInteger = StrToInt(edNumeroComprobante.Text) then begin
                    cds_Comprobantes.Append;

                    cds_Comprobantes.FieldByName('TipoComprobante').AsString := ObtenerComprobantesImpagosOtros.FieldByName('TipoComprobante').AsString;
                    cds_Comprobantes.FieldByName('NumeroComprobante').AsInteger := ObtenerComprobantesImpagosOtros.FieldByName('NumeroComprobante').AsInteger;
                    cds_Comprobantes.FieldByName('DescriComprobante').AsString := ObtenerComprobantesImpagosOtros.FieldByName('DescriComprobante').AsString;
                    cds_Comprobantes.FieldByName('FechaEmision').AsDateTime := ObtenerComprobantesImpagosOtros.FieldByName('FechaEmision').AsDateTime;
                    cds_Comprobantes.FieldByName('FechaVencimiento').AsDateTime := ObtenerComprobantesImpagosOtros.FieldByName('FechaVencimiento').asDateTime;
                    cds_Comprobantes.FieldByName('TotalComprobante').AsFloat := ObtenerComprobantesImpagosOtros.FieldByName('TotalComprobante').AsFloat;
                    cds_Comprobantes.FieldByName('TotalComprobanteDescri').AsString := ObtenerComprobantesImpagosOtros.FieldByName('TotalComprobanteDescri').AsString;
                    cds_Comprobantes.FieldByName('SaldoPendiente').AsFloat := ObtenerComprobantesImpagosOtros.FieldByName('SaldoPendiente').AsFloat;
                    cds_Comprobantes.FieldByName('SaldoPendienteDescri').AsString := ObtenerComprobantesImpagosOtros.FieldByName('SaldoPendienteDescri').AsString;
                    cds_Comprobantes.FieldByName('EstadoPago').AsString := ObtenerComprobantesImpagosOtros.FieldByName('EstadoPago').AsString;
                    cds_Comprobantes.FieldByName('DescriEstado').AsString := ObtenerComprobantesImpagosOtros.FieldByName('DescriEstado').AsString;
                    cds_Comprobantes.FieldByName('EstadoDebito').AsString := ObtenerComprobantesImpagosOtros.FieldByName('EstadoDebito').AsString;
                    cds_Comprobantes.FieldByName('UltimoComprobante').AsInteger := iif(ObtenerComprobantesImpagosOtros.FieldByName('UltimoComprobante').IsNull, -1, ObtenerComprobantesImpagos.FieldByName('UltimoComprobante').AsInteger);
                    cds_Comprobantes.FieldByName('CodigoConvenio').AsInteger := ObtenerComprobantesImpagosOtros.FieldByName('CodigoConvenio').AsInteger;
                    cds_Comprobantes.FieldByName('CodigoPersona').AsInteger := ObtenerComprobantesImpagosOtros.FieldByName('CodigoPersona').AsInteger;
                    cds_Comprobantes.FieldByName('TotalPagos').AsFloat := ObtenerComprobantesImpagosOtros.FieldByName('TotalPagos').AsFloat;
                    cds_Comprobantes.FieldByName('TotalPagosStr').AsString := ObtenerComprobantesImpagosOtros.FieldByName('TotalPagosStr').AsString;

                    cds_Comprobantes.FieldByName('DescTipoMedioPAgo').AsString := ObtenerComprobantesImpagosOtros.FieldByName('DescTipoMedioPAgo').AsString;
                    cds_Comprobantes.FieldByName('DescPATPAC').AsString := ObtenerComprobantesImpagosOtros.FieldByName('DescPATPAC').AsString;
                    cds_Comprobantes.FieldByName('Rechazo').AsString := ObtenerComprobantesImpagosOtros.FieldByName('Rechazo').AsString;

                    cds_Comprobantes.FieldByName('TipoComprobanteFiscal').AsString := ObtenerComprobantesImpagosOtros.FieldByName('TipoComprobanteFiscal').AsString;
                    cds_Comprobantes.FieldByName('NumeroComprobanteFiscal').AsInteger := ObtenerComprobantesImpagosOtros.FieldByName('NumeroComprobanteFiscal').AsInteger;
                    cds_Comprobantes.FieldByName('DescriComprobanteFiscal').AsString := ObtenerComprobantesImpagosOtros.FieldByName('DescriComprobanteFiscal').AsString;
                    cds_Comprobantes.FieldByName('EstadoRefinanciacion').AsInteger := ObtenerComprobantesImpagos.FieldByName('EstadoRefinanciacion').AsInteger;

                    FTotal :=  ObtenerComprobantesImpagosOtros.FieldByName('SaldoPendiente').AsInteger;

                    FSaldoConvenios := FSaldoConvenios +  FTotal;

                    if not (ObtenerComprobantesImpagosOtros.FieldByName('EstadoRefinanciacion').AsInteger in [REFINANCIACION_ESTADO_EN_TRAMITE, REFINANCIACION_ESTADO_ACEPTADA]) then begin

                        cds_Comprobantes.FieldByName('Cobrar').AsBoolean := True;
                        inc(FCantidadComprobantesCobrar);


                        if not EncontradoComprobante then begin
                            cds_Comprobantes.FieldByName('Cobrar').AsBoolean := True;
                            inc(FCantidadComprobantesCobrar);
                            //FTotalComprobantesCobrar := FTotalComprobantesCobrar; // + cds_Comprobantes.FieldByName('SaldoPendiente').AsFloat;  //VERIFICAR

                            if ((cds_Comprobantes.FieldByName('NumeroComprobante').asInteger = edNumeroComprobante.ValueInt)) or
                                   ((cds_Comprobantes.FieldByName('NumeroComprobanteFiscal').asInteger = edNumeroComprobante.ValueInt)) then begin
                                 EncontradoComprobante := True;
                             end
                            else
                                cds_Comprobantes.FieldByName('Cobrar').AsBoolean := False;
                        end;

                    cds_Comprobantes.Post;

                    end;

                    ObtenerComprobantesImpagosOtros.Next;
                end
                else begin
                    ObtenerComprobantesImpagosOtros.Next;
                end;
            end;

           end;
           if cds_Comprobantes.RecordCount <= 0 then begin
                Result := False;
                Exit;
           end else begin
           		Result := True;
                cds_Comprobantes.First;
                 lbl_Estado.Caption := cds_Comprobantes.FieldByName('DescriEstado').AsString;
           end;

            lblTotalAPagar.Caption := FormatFloat(FORMATO_IMPORTE, FSaldoConvenios - FTotalComprobantesCobrar);     //Deuda Exigible

        except
            on E: Exception do MsgBoxErr(MSG_ERROR_ON_LOAD, E.Message, Caption, MB_ICONERROR);
        end;

    finally
        ObtenerComprobantesImpagos.Close;
         FreeAndNil(ObtenerComprobantesImpagos);

        if ObtenerComprobantesImpagosOtros <> nil then begin
            ObtenerComprobantesImpagosOtros.Close;
            FreeAndNil(ObtenerComprobantesImpagosOtros);
        end;

        //TPanelMensajesForm.OcultaPanel; //TASK_049_GLE_20170207
        cds_Comprobantes.EnableControls;
        lblTotalSelec.Caption   := FormatFloat(FORMATO_IMPORTE, FTotal);

        // Si hay comprobantes por cobrar, habilitar el bot�n de Castigo
        btnCastigar.Enabled     := (FCantidadComprobantesCobrar > 0);

        dl_ComprobantesClick(dl_Comprobantes);

        if FComprobantesEnCobranza.Count > 0 then begin
            //TPanelMensajesForm.OcultaPanel; //TASK_049_GLE_20170207
            if FComprobantesEnCobranza.Count >= 15 then
                ShowMsgBoxCN(MSG_COMPROBANTES_COBRANZA_JUDICIAL, Format(MSG_MULTIPLES_COMPROBANTES_EN_COBRANZA,[FComprobantesEnCobranza.Count]), MB_ICONWARNING, Self)
            else
                ShowMsgBoxCN(MSG_COMPROBANTES_COBRANZA_JUDICIAL, FComprobantesEnCobranza.Text, MB_ICONWARNING, Self);
        end;
        if assigned(FComprobantesEnCobranza) then
            FreeAndNil(FComprobantesEnCobranza);

        Screen.Cursor := crDefault;
    end;
    //TPanelMensajesForm.OcultaPanel;      //TASK_049_GLE_20170207
end;

{-----------------------------------------------------------------------------
  Function Name :   btnCastigarClick
  Author        :
  Date Created  :
  Description   :
  Parameters    :
  Return Value  :
-----------------------------------------------------------------------------}
procedure TformCobroComprobantesCastigo.btnCastigarClick(Sender: TObject);
resourcestring
    MSG_NEWER_INVOICE = 'Existe un comprobante emitido m�s nuevo.';
  	MSG_WISH_SEE_NEW_INVOICE = '�Desea ver el nuevo comprobante?';
  	MSG_CAPTION	= 'Cobro de Comprobantes';
    MSG_COMPROBANTES_IMPAGOS_TITULO  = 'Validaci�n Comprobantes';
    MSG_COMPROBANTES_IMPAGOS_MENSAJE = 'Existen comprobantes anteriores NK que se encuentran Vencidos e Impagos.';

    MSG_CONVENIOENLISTAAMARILLA = 'Convenio se encuentra en lista amarilla' + CRLF + 'No se puede pagar por refinanciaci�n.';
var
    ExistenImpagos, EncontradoUltimoMarcado: Boolean;

    ValorActualCobrar: Boolean;
    Puntero: TBookmark;
    Marcar: Boolean;
begin
    try
        Screen.Cursor := crHourGlass;
        EmptyKeyQueue;
        Application.ProcessMessages;

        ExistenImpagos := False;
        EncontradoUltimoMarcado := False;
        ds_Comprobantes.DataSet := Nil;
        FTotalComprobantesCobrar :=0;

        //SELECCIONAR TODOS LOS COMPROBANTES DEL CONVENIO INICIO
        Puntero := cds_Comprobantes.GetBookmark;
        cds_Comprobantes.First;

        while not cds_Comprobantes.Eof do begin
            if not (cds_Comprobantes.FieldByName('EstadoRefinanciacion').AsInteger in [REFINANCIACION_ESTADO_EN_TRAMITE, REFINANCIACION_ESTADO_ACEPTADA]) then begin
                with cds_Comprobantes do begin
                    ValorActualCobrar := FieldByName('Cobrar').AsBoolean;
                    Edit;

                    //FieldByName('Cobrar').AsBoolean := Marcar;
                    Inc(FCantidadComprobantesCobrar);

                    FTotalComprobantesCobrar := FTotalComprobantesCobrar + FieldByName('SaldoPendiente').AsFloat;

                    if FTotalComprobantesCobrar < 0 then begin
                        lblTotalSelec.Caption:=FormatFloat(FORMATO_IMPORTE, 0);
                        FTotalComprobantesCobrar :=0;
                    end
                    else begin
                        lblTotalSelec.Caption:=FormatFloat(FORMATO_IMPORTE, FTotalComprobantesCobrar);
                    end;

                    if ValorActualCobrar <> FieldByName('Cobrar').AsBoolean  then begin
                        if FieldByName('Cobrar').AsBoolean then begin
                            Inc(FCantidadComprobantesCobrar);
                            FTotalComprobantesCobrar := FTotalComprobantesCobrar + FieldByName('SaldoPendiente').AsFloat;
                            lblTotalSelec.Caption:=FormatFloat(FORMATO_IMPORTE, FTotalComprobantesCobrar);
                        end else begin
                            Dec(FCantidadComprobantesCobrar);
                            FTotalComprobantesCobrar := FTotalComprobantesCobrar - FieldByName('SaldoPendiente').AsFloat;
                            if FTotalComprobantesCobrar < 0 then begin
                                lblTotalSelec.Caption:=FormatFloat(FORMATO_IMPORTE, 0);
                                FTotalComprobantesCobrar :=0;
                            end else begin
                                lblTotalSelec.Caption:=FormatFloat(FORMATO_IMPORTE, FTotalComprobantesCobrar);
                            end;
                        end;
                    end;

                     lblTotalAPagar.Caption := FormatFloat(FORMATO_IMPORTE , FSaldoConvenios - FTotalComprobantesCobrar);      //revisar si va
                    Post;
                end;
            end;

            cds_Comprobantes.Next;
        end;

        with cds_Comprobantes do begin
            cds_Comprobantes.Last;
            while (not cds_Comprobantes.Bof) and (not ExistenImpagos) do begin
                if not EncontradoUltimoMarcado then
                    EncontradoUltimoMarcado := (cds_Comprobantes.FieldByName('Cobrar').AsBoolean and (cds_Comprobantes.FieldByName('TipoComprobante').AsString = TC_NOTA_COBRO))
                else ExistenImpagos := (not cds_Comprobantes.FieldByName('Cobrar').AsBoolean) and (cds_Comprobantes.FieldByName('TipoComprobante').AsString = TC_NOTA_COBRO);

                cds_Comprobantes.Prior;

                //marca todos los comprobantes a cobrar
                dl_Comprobantes.SelectedIndex;
            end;
        end;
    finally
        if cds_Comprobantes.BookmarkValid(Puntero) then begin
            cds_Comprobantes.GotoBookmark(Puntero);
        end;
        cds_Comprobantes.FreeBookmark(Puntero);
        ds_Comprobantes.DataSet := cds_Comprobantes;

        btnCastigar.Enabled := (FCantidadComprobantesCobrar > 0);
        Screen.Cursor := crDefault;
    end;

    if ExistenImpagos then ShowMsgBoxCN(MSG_COMPROBANTES_IMPAGOS_TITULO, MSG_COMPROBANTES_IMPAGOS_MENSAJE, MB_ICONWARNING, Self);

    try
        btnCastigar.Enabled := False;
        EmptyKeyQueue;
        Application.ProcessMessages;

        if (cds_Comprobantes.RecordCount = 1) and (FTipoComprobante = TC_NOTA_COBRO) and (FNumeroComprobante < FUltimoComprobante) then begin
            if MsgBox(MSG_NEWER_INVOICE + #10#13 + MSG_WISH_SEE_NEW_INVOICE, MSG_CAPTION, MB_ICONQUESTION or MB_DEFBUTTON2 or MB_YESNO) = IDYES then begin
                edNumeroComprobante.ValueInt := FUltimoComprobante;
                CargarComprobantes;
            end else Castigar;
        end else Castigar;
    finally
        btnCastigar.Enabled := True;

        if ShowMsgBoxCN('Castigo Comprobantes', 'Desea Castigar otro Convenio? Presione Aceptar para continuar en esta secci�n.', MB_ICONQUESTION, Self) <> mrOk then begin
            Close;
        end;

    end;
end;

{-----------------------------------------------------------------------------
  Function Name :   Cobrar
  Author        :   flamas  - Modificado GLEON
  Date Created  :   27/01/2005  - Modificado 29/11/2016
  Description   :   Cobra un Comprobante (Castiga Comprobante)
  Parameters    :   Sender: TObject
  Return Value  :   None
-----------------------------------------------------------------------------}

procedure TformCobroComprobantesCastigo.Castigar;

resourcestring
	MSG_REGISTRO_CORRECTO = 'El pago Castigo se ha registrado correctamente.';
	MSG_CONFIRMAR_RECIBO = '�Desea Imprimir el Comprobante de Cancelaci�n?';
	MSG_CAPTION = 'Registrar Pago Castigo';
	MSG_IMPRESION_EXITOSA = '�La impresi�n se ha realizado con �xito?';
    MSG_ERROR_IMPRESION = 'Error al intentar Imprimir el comprobante';
var
	r: TformRecibo;
	f: TfrmPagoVentanillaCastigo; 
	RespuestaImpresion: TRespuestaImprimir;
    ComprobantesCobrar: TListaComprobante;

    Veces: Integer;
    Comprobante: TComprobante;

begin

    (* Armar la lista con los comprobantes a cobrar. *)
    ComprobantesCobrar := ObtenerComprobantesCobrar;
    try
        Application.CreateForm(TfrmPagoVentanillaCastigo, f);
        if f.Inicializar(ComprobantesCobrar, lblRUT.Caption, lblNombre.Caption,
                FNumeroPOS, FPuntoEntrega, FPuntoVenta, FCodigoCanalPago) then
                f.ShowModal;

            if f.ModalResult = mrOk then begin
                (* Almacenar el c�digo de canal de pago utilizado. *)
                FCodigoCanalPago := f.CodigoCanalPago;

                if f.ImprimirRecibo then begin
                    (* Mostrar el di�logo previo a realizar la impresi�n. *)
                    RespuestaImpresion := frmImprimir.Ejecutar(MSG_CAPTION,
                      MSG_REGISTRO_CORRECTO + CRLF + MSG_CONFIRMAR_RECIBO);

                    case RespuestaImpresion of
                        (* Si la respuesta fue Aceptar, ejecutar el reporte. *)
                        riAceptarConfigurarImpresora, riAceptar: begin
                            Application.CreateForm(TformRecibo, r);
                            try
                              try
                                  if r.Inicializar(FLogo, f.NumeroRecibo, RespuestaImpresion = riAceptarConfigurarImpresora) then begin
                                      (* Preguntar si la impresi�n fue exitosa. *)
                                      while MsgBox(MSG_IMPRESION_EXITOSA, MSG_CAPTION, MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = mrNo do begin
                                          (* Mostrar el di�logo de Configurar Impresora para permitir al operador
                                          reintentar la impresi�n. *)
                                          if not r.Inicializar(FLogo, f.NumeroRecibo, True) then Break;
                                      end;
                                  end;
                              finally
                                  r.Release;
                              end;
                            except

                              On E: Exception do begin
                                MsgBoxErr(MSG_ERROR_IMPRESION, e.Message, Caption, MB_ICONERROR);
                              end;
                            end;
                        end;
                    end;

                    LimpiarDatosCliente;
                end;

                for Veces := 0 to ComprobantesCobrar.Count - 1 do begin
                    Comprobante := TComprobante(ComprobantesCobrar.Items[Veces]);
                    if Comprobante.TieneConceptosInfractor then
                        ImprimirDetalleInfraccionesAnuladas(Comprobante.TipoComprobante, Comprobante.NumeroComprobante);
                end;

                //INICIO	: TASK_124_CFU_20170320
                with spActualizarEstadoConvenio, Parameters do begin
                    Parameters.Refresh;
                    ParamByName('@CodigoConvenio').Value	:= cbConvenios.Value;
                    ParamByName('@EstadoConvenio').Value	:= CONST_ESTADO_CONVENIO_CASTIGADO;
                    ParamByName('@Usuario').Value			:= UsuarioSistema;
                    try
                        ExecProc;
                    except
                        on E:Exception do begin
                            MsgBox(E.Message, Caption, MB_ICONERROR);
                            Exit;
                        end;
                    end;
                end;

                with spIngresarConvenioACobranzaInterna, Parameters do begin
                    Parameters.Refresh;
                    ParamByName('@CodigoConvenio').Value	:= cbConvenios.Value;
                    ParamByName('@Usuario').Value			:= UsuarioSistema;
                    try
                        ExecProc;
                    except
                        on E: Exception do begin
                            MsgBox(E.Message, Caption, MB_ICONERROR);
                            Exit;
                        end;
                    end;
                end;
                //TERMINO	: TASK_124_CFU_20170320
                
                LimpiarCampos;
                ShowMsgBoxCN('Castigo Del Convenio', 'Castigo del Convenio Realizado con �xito.', MB_ICONEXCLAMATION, Self);
            end;
        f.Free;
    finally
        ComprobantesCobrar.Free;
    end;

end;

{-----------------------------------------------------------------------------
  Function Name :   MostrarDatosCliente
  Author        :   flamas
  Date Created  :   20/12/2004
  Description   :   Muestra los datos del Cliente
  Parameters    :   CodigoCliente
  Return Value  :   None
-----------------------------------------------------------------------------}
procedure TformCobroComprobantesCastigo.MostrarDatosCliente(CodigoCliente: Integer);
var
RutPersona : string;
begin

    RutPersona := QueryGetValue(DMConnections.BaseCAC, Format('SELECT dbo.ObtenerRUTPersona(%d)', [CodigoCliente]));

    //Cargamos la informaci�n del cliente.
	lblRUT.Caption := RutPersona;
	lblNombre.Caption := QueryGetValue(DMConnections.BaseCAC, Format('SELECT dbo.ObtenerNombrePersona(%d)', [CodigoCliente]));
	lblDomicilio.Caption := QueryGetValue(DMConnections.BaseCAC, Format('SELECT dbo.ObtenerDescripcionDomicilioPrincipalPersona(%d)', [CodigoCliente]));

    if FUltimoNumeroDocumento <> Trim(lblRUT.Caption) then begin
        FUltimoNumeroDocumento := Trim(lblRUT.Caption);
        MostrarMensajeRUTConvenioCuentas(FUltimoNumeroDocumento);
        MostrarMensajeTipoClienteSistema(RutPersona);
    end;
    EstaClienteConMensajeEspecial(DMConnections.BaseCAC, RutPersona);
    //MostrarSaldosPersona(CodigoCliente); //TASK_049_GLE_20170207

    VerificaPersonaListaAmarilla(RutPersona);
end;

{-----------------------------------------------------------------------------
  Procedure Name    :   VerificarComprobanteEnCarpetaLegal
  Author            :   nefernandez
  Date Created      :   23/03/2007
  Description       :   Verificar si el comprobante pertenece a un convenio que esta
                        en Carpeta Legal
  Parameters        :
-----------------------------------------------------------------------------}
function TformCobroComprobantesCastigo.VerificarComprobanteEnCarpetaLegal(ModoSilencioso: Boolean): Boolean;
resourcestring
    MSG_CONVENIO_CARPETA_LEGAL = 'Atenci�n: Este comprobante corresponde a un Convenio que est� en Cobranza Judicial.'
                        + CHR(13) + 'NO debe recibir el pago.' + CHR(13) +
                        'Desea continual igual ?';
    MSG_CONVENIO_CARPETA_LEGAL_EMPRESA = 'Atenci�n: Este comprobante corresponde a un Convenio que est� en Cobranza Judicial.'
                        + CHR(13) + 'Este convenio fue asignado a la empresa: "%s."'
                        + CHR(13) + 'NO debe recibir el pago.' + CHR(13) +
                        'Desea continual igual ?';
    MSG_COMPROBANTE_EN_COBRANZA = 'Comprobante %s, N�mero %s, Empresa %s';
var
    Empresa, Mensaje: string;
begin
    if (cds_Comprobantes.FieldByName('Cobrar').AsBoolean or ModoSilencioso) and
        (ChequearConvenioEnCarpetaLegal(cds_Comprobantes.FieldByName('CodigoConvenio').Value)) then begin
        // El usuario tiene la opcion de cobrar o no los comprobantes en carpeta legal
        Empresa := Trim(ObtenerEmpresaConvenioEnCarpetaLegal(cds_Comprobantes.FieldByName('CodigoConvenio').Value));

        if not ModoSilencioso then begin
            if Empresa = '' then
                Mensaje := MSG_CONVENIO_CARPETA_LEGAL
            else Mensaje := Format(MSG_CONVENIO_CARPETA_LEGAL_Empresa, [Empresa]);
            if MsgBox(Mensaje, self.Caption, MB_ICONWARNING or MB_YESNO) <> IDYES then
                cds_Comprobantes.FieldByName('Cobrar').AsBoolean := not cds_Comprobantes.FieldByName( 'Cobrar' ).AsBoolean;
        end
        else FComprobantesEnCobranza.Add(
                Format(
                    MSG_COMPROBANTE_EN_COBRANZA,
                    [cds_Comprobantes.FieldByName('TipoComprobante').AsString,
                     cds_Comprobantes.FieldByName('NumeroComprobante').AsString,
                     Empresa]));
    end;
end;

{-----------------------------------------------------------------------------
  Function Name :   dl_ComprobantesClick
  Author        :
  Date Created  :   13/10/2005
  Description   :
  Parameters    :
  Return Value  :
-----------------------------------------------------------------------------}
procedure TformCobroComprobantesCastigo.dl_ComprobantesClick(Sender: TObject);
begin
    MostrarDatosComprobante;
end;

{-----------------------------------------------------------------------------
  Function Name :   lst1Click
  Author        :   GLEON
  Date Created  :   09/12/2016
  Description   :
  Parameters    :
  Return Value  :
-----------------------------------------------------------------------------}
procedure TformCobroComprobantesCastigo.lst1Click(Sender: TObject);
begin
    MostrarDatosComprobantesCastigados;
end;

{-----------------------------------------------------------------------------
  Function Name :   edNumeroComprobanteChange
  Author        :   flamas
  Date Created  :   21/12/2004
  Description   :   Procesa un cambio de comprobante y muestra los datos del comprobante
  Parameters    :   Sender: TObject
  Return Value  :   None
  -----------------------------------------------------------------------------}
procedure TformCobroComprobantesCastigo.edNumeroComprobanteChange(Sender: TObject);

begin
    if ActiveControl <> Sender then Exit;

    btnFiltrar.Enabled  := True;
    peNumeroDocumento.Clear;
    FCodigoCliente := -1;
    LimpiarDetalles;
    LimpiarConvenios;
    LimpiarListaComprobantes;
    LimpiarDatosCliente;
    HabilitarFiltroRUT(edNumeroComprobante.Text = EmptyStr);
end;

procedure TformCobroComprobantesCastigo.edNumeroComprobanteEnter(
  Sender: TObject);
begin
    try
        Screen.Cursor := crHourGlass;

        if Trim(edNumeroComprobante.Text)<>'' then begin
            CargarComprobantes;
        end;
    finally
        Screen.Cursor := crDefault;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name :   MostrarSaldosPersona
  Author        :
  Date Created  :
  Description   :
  Parameters    :   CodigoPersona
  Return Value  :
-----------------------------------------------------------------------------}
procedure TformCobroComprobantesCastigo.MostrarSaldosPersona(CodigoPersona: Integer);
    var
        TotalDeudasRefinanciacion,
        TotalDeudasConvenios,
        DeudaRefinanciacionVencida,
        DeudaComprobantesVencida: Extended;
    const
        SQLDeudaConvenios = 'SELECT dbo.ObtenerSaldoDeLosConvenios(%d)';
begin
    try
        Screen.Cursor := crHourGlass;
        EmptyKeyQueue;
        Application.ProcessMessages;

        try
            with spObtenerDeudaComprobantesVencidos do begin
                if Active then Close;
                Parameters.Refresh;

                Parameters.ParamByName('@CodigoConvenio').Value := cbConvenios.Value;//Null;         //TASK_049_GLE_20170207
                Parameters.ParamByName('@CodigoCliente').Value  := CodigoPersona; //FCodigoCliente;  //TASK_049_GLE_20170207
                Open;

                DeudaComprobantesVencida := FieldByName('DeudaComprobantesVencidos').AsFloat;
            end;

            with spObtenerRefinanciacionesTotalesPersona do begin
                if Active then Close;
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoPersona').Value := CodigoPersona;
                Parameters.PAramByName('@CodigoConvenio').Value := cbConvenios.Value;//Null;         //TASK_049_GLE_20170207
                Open;

                TotalDeudasRefinanciacion  := FieldByName('TotalDeuda').AsFloat - FieldByName('TotalPagado').AsFloat;
                DeudaRefinanciacionVencida := FieldByName('DeudaVencida').AsFloat;
            end;

            TotalDeudasConvenios := QueryGetValueInt(DMConnections.BaseCAC, Format(SQLDeudaConvenios, [CodigoPersona]));

            //lblMontoDeudasConvenio.Caption := FormatFloat(FORMATO_IMPORTE, TotalDeudasConvenios); ////TASK_049_GLE_20170207
            if DeudaComprobantesVencida > 0 then begin
                lblMontoDeudasConvenio.Font.Color := clRed;
                lblMontoDeudasConvenio.Caption    := FormatFloat(FORMATO_IMPORTE, DeudaComprobantesVencida);        //TASK_049_GLE_20170207
                //TPanelMensajesForm.OcultaPanel;           //TASK_049_GLE_20170207
                ShowMsgBoxCN('Validaci�n Comprobantes Vencidos', 'El Cliente tiene Comprobantes pendientes de Pago Vencidos, en este u en otro(s) Convenios(s).', MB_ICONWARNING, Self);
            end else begin                                  //TASK_049_GLE_20170207
                lblMontoDeudasConvenio.Caption := '$ 0';    //TASK_049_GLE_20170207
                lblMontoDeudasConvenio.Font.Color := clBlack; //TASK_049_GLE_20170207
             end;

            //lblMontoDeudasRefinanciacion.Caption := FormatFloat(FORMATO_IMPORTE, TotalDeudasRefinanciacion);     //TASK_049_GLE_20170207
            if DeudaRefinanciacionVencida > 0 then begin
                //TPanelMensajesForm.OcultaPanel;                   //TASK_049_GLE_20170207
                lblMontoDeudasRefinanciacion.Caption    := FormatFloat(FORMATO_IMPORTE, DeudaRefinanciacionVencida); //TASK_049_GLE_20170207
                lblMontoDeudasRefinanciacion.Font.Color := clRed;//TASK_049_GLE_20170207
                ShowMsgBoxCN('Validaci�n Refinanciaci�n', 'El Cliente tiene Cuotas Vencidas por Refinanciaci�n.', MB_ICONWARNING, Self);
            end else begin                                         //TASK_049_GLE_20170207
                lblMontoDeudasRefinanciacion.Caption    := '$ 0';  //TASK_049_GLE_20170207
                lblMontoDeudasRefinanciacion.Font.Color := clBlack;//TASK_049_GLE_20170207
            end;

            if (TotalDeudasConvenios <= 0) and (DeudaRefinanciacionVencida <= 0) then begin
                MsgBox('No se encontraron Comprobantes Impagos', 'Comprobantes Impagos', MB_ICONEXCLAMATION); //TASK_049_GLE_20170207
            end;
        except
            on e:Exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
        Screen.Cursor := crDefault;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: peNumeroDocumentoChange
  Author:    flamas
  Date Created: 20/12/2004
  Description: Actualiza los Convenios y Comprobantes Impagos de un cliente determinado
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformCobroComprobantesCastigo.peNumeroDocumentoChange(Sender: TObject);
var
CodCliente : Integer;

begin
    if ActiveControl <> Sender then Exit;
    edNumeroComprobante.Clear;

    LimpiarDetalles;
    LimpiarConvenios;
    LimpiarListaComprobantes;                                                                                                                                                                            //SS-1014-NDR-20120123
    LimpiarDatosCliente;
    CodCliente := QueryGetValueInt( DMConnections.BaseCAC,
                                            Format('SELECT CodigoPersona FROM Personas WITH (NOLOCK) WHERE CodigoDocumento = ''RUT'' ' + 'AND NumeroDocumento = ''%s''',
                                            [iif(peNumeroDocumento.Text <> '', trim(PadL(peNumeroDocumento.Text, 9, '0')), '')]));
        if CodCliente > 0 then
        begin
            CargarConveniosRUT(DMConnections.BaseCAC, cbConvenios, 'RUT', peNumeroDocumento.Text, False, 0, True);
            cbConvenios.ItemIndex := 1;             //TASK_049_GLE_20170207
            MostrarDatosCliente(CodCliente);        //TASK_049_GLE_20170207
            FCodigoCliente := CodCliente;
            CargarComprobantes;                     //TASK_049_GLE_20170207
            MostrarSaldosPersona(FCodigoCliente);   //TASK_049_GLE_20170207
            btnFiltrar.Enabled:= True;
            btnComprobantesCastigos.Enabled  := True;
        end;
end;

 procedure TformCobroComprobantesCastigo.pnl2Click(Sender: TObject);
begin

end;

{*******************************************************************************
    Procedure Name  : VerificaPersonaListaAmarilla
    Author          : CQuezadaI
    Date            : 23/07/2013
    Firma           : SS_660_CQU_20130711
    Description     : Se verifica si est� en Lista Amarilla por CodigoCliente
*******************************************************************************}
function TformCobroComprobantesCastigo.VerificaPersonaListaAmarilla(RutCliente : string) : Boolean;
resourcestring
    //MSG_ALERTA ='Validaci�n Cliente';
    //MSG_ALERTA_CLIENTE_LISTA_AMARILLA='EL Cliente posee al menos un convenio en lista amarilla';

    SQL_ESTACONVENIOENLISTAAMARILLA = 'SELECT dbo.EstaPersonaEnListaAmarilla(dbo.ObtenerCodigoPersonaRUT(''%s''))';
    SQL_ESTAPERSONAENPROCESOLISTAAMARILLA = 'SELECT dbo.EstaPersonaEnProcesoListaAmarilla(dbo.ObtenerCodigoPersonaRUT(''%s''))';
    MSG_INHABILITADO_LISTA_AMARILLA = 'El Cliente posee al menos un convenio en lista amarilla';
    MSG_PROCESO_LISTA_AMARILLA = 'El RUT a lo menos tiene un convenio en proceso de Lista Amarilla';
Var
	EsListaAmarilla: Boolean;
    EsProcesoListaAmarilla: string;
begin
    Result      :=  False;

	if RutCliente <> EmptyStr then begin
        try
            CambiarEstadoCursor(CURSOR_RELOJ, Self, FComponentes);
            try

                EsListaAmarilla := StrToBool(QueryGetValue(DMConnections.BaseCAC,
        								Format(SQL_ESTACONVENIOENLISTAAMARILLA,
                                        [RutCliente])));
                EsProcesoListaAmarilla := QueryGetValue(DMConnections.BaseCAC,
                                                Format(SQL_ESTAPERSONAENPROCESOLISTAAMARILLA,
                                                [RutCliente]));
                if EsListaAmarilla then
                begin
                    ShowMsgBoxCN(self.Caption, MSG_INHABILITADO_LISTA_AMARILLA, MB_ICONWARNING, self);
                    exit;
                end;
                if EsProcesoListaAmarilla <> EmptyStr then
                begin
                    ShowMsgBoxCN(self.Caption, MSG_PROCESO_LISTA_AMARILLA,  MB_ICONWARNING, self);
                end;

            except
                on e: Exception do begin
                    ShowMsgBoxCN(e, Self);
                end;
            end;  
        finally
            CambiarEstadoCursor(CURSOR_DEFECTO, Self, FComponentes);
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name :   MostrarMensajeRUTConvenioCuentas
  Author        :   nefernandez
  Date Created  :   11/02/2008
  Description   :   SS 631: Se muestra un aviso con las observaciones de las cuentas
                    del Cliente (RUT) en cuesti�n.
  Parameters    :   NumeroDocumento: String
  Return Value  :   None
-----------------------------------------------------------------------------}
procedure TformCobroComprobantesCastigo.MostrarMensajeRUTConvenioCuentas(NumeroDocumento: String);
var
    Texto: AnsiString;
begin
    if ( NumeroDocumento <> '' ) then begin
        Texto := MensajeRUTConvenioCuentas(DMCOnnections.BaseCAC, NumeroDocumento);
        if Texto <> '' then
            MsgBox(Texto, Self.Caption, MB_ICONWARNING);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name :   MostrarMensajeTipoClienteSistema
  Author        :   mpiazza
  Date Created  :   23/01/2009
  Description   :   SS-777 Trae mensaje segun RUT y sistema de acuerdo
                    al tipo cliente
  Return Value  :   None
-----------------------------------------------------------------------------}
procedure TformCobroComprobantesCastigo.MostrarMensajeTipoClienteSistema(
  NumeroDocumento: String);
var
    Texto: AnsiString;
begin
    if ( NumeroDocumento <> '' ) then begin
        Texto := MensajeTipoCliente(DMCOnnections.BaseCAC, NumeroDocumento, SistemaActual);
        if Texto <> '' then
            MsgBox(Texto, Self.Caption, MB_ICONWARNING);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name :   LimpiarConvenios
  Author        :
  Date Created  :   13/10/2005
  Description   :
  Parameters    :
  Return Value  :
-----------------------------------------------------------------------------}
procedure TformCobroComprobantesCastigo.LimpiarConvenios;
begin
    cbConvenios.Clear;
    cbConvenios.Items.Add(SELECCIONAR, 0);
    cbConvenios.ItemIndex := 0;
end;

{-----------------------------------------------------------------------------
  Function Name: LimpiarListaComprobantes
  Author:
  Date Created:  13/10/2005
  Description:
  Parameters:
  Return Value:
-----------------------------------------------------------------------------}
procedure TformCobroComprobantesCastigo.LimpiarListaComprobantes;
begin
    FTotalComprobantesCobrar := 0;
    FCantidadComprobantesCobrar := 0;
    FCantidadComprobantesCastigados := 0;

    cds_Comprobantes.DisableControls;
    cdsComprobantesCastigados.DisableControls;
    try
        cds_Comprobantes.EmptyDataSet;
        cdsComprobantesCastigados.EmptyDataSet  ;
    finally
    	cds_Comprobantes.EnableControls;
        cdsComprobantesCastigados.EnableControls    ;

    end;
end;

{-----------------------------------------------------------------------------
  Function Name :   peNumeroDocumentoButtonClick
  Author        :   flamas
  Date Created  :   21/12/2004
  Description   :   Procesa un cambio en el RUT - Muestra los comprobantes pendientes
  Parameters    :   Sender: TObject
  Return Value  :   None
-----------------------------------------------------------------------------}
procedure TformCobroComprobantesCastigo.peNumeroDocumentoButtonClick(Sender: TObject);
var
    f: TFormBuscaClientes;
begin
    Application.CreateForm(TFormBuscaClientes,F);

  	if F.Inicializa(FUltimaBusqueda) then begin
        F.ShowModal;
        if F.ModalResult = idOk then begin
            FUltimaBusqueda := F.UltimaBusqueda;
            peNumeroDocumento.Text := f.Persona.NumeroDocumento;
	    end;
  	end;
    F.free;
end;

{-----------------------------------------------------------------------------
  Function Name :   cbComprobantesChange
  Author        :   flamas
  Date Created  :   21/12/2004
  Description   :   Procesa un cambio de comprobante y muestra los datos
  Parameters    :   Sender: TObject
  Return Value  :   None
-----------------------------------------------------------------------------}
procedure TformCobroComprobantesCastigo.cbComprobantesChange(Sender: TObject);
begin
    btnFiltrar.Enabled  := True ;
    if ActiveControl <> Sender then Exit;
    MostrarDatosComprobante;
end;

{-----------------------------------------------------------------------------
  Function Name :   HabilitarFiltroRUT
  Author        :
  Date Created  :   13/10/2005
  Description   :
  Parameters    :
  Return Value  :
-----------------------------------------------------------------------------}
procedure TformCobroComprobantesCastigo.HabilitarFiltroRUT(Valor: Boolean);
begin
    peNumeroDocumento.Enabled := Valor;
    lb_CodigoCliente.Enabled := Valor;
    cbConvenios.Enabled := Valor;
    lbl_NumeroConvenio.Enabled := Valor;
end;

{-----------------------------------------------------------------------------
 Function Name  :   ObtenerComprobantesCobrar
 Parameters     :   None
 Return Value   :   TListaComprobante
 Author         :
 Date Created   :   13/10/2005
 Description    :   Genera un lista con los Comprobantes a Cobrar

 Revision       :   16
 Author         :   pdominguez
 Date           :   03/06/2010
 Description    :   Infractores Fase 2
                    - Se a�ade la asignaci�n del valor de la propiedad TieneConceptosInfractor.
-----------------------------------------------------------------------------}
function TformCobroComprobantesCastigo.ObtenerComprobantesCobrar: TListaComprobante;
var
    Compro: TComprobante;
const
    SQL_NI = 'SELECT dbo.ComprobanteTieneConceptosInfractor(''%s'', %d)';
begin
    Result := Nil;

    if not cds_Comprobantes.IsEmpty then begin
        Result := TListaComprobante.Create;

        //cds_Comprobantes.DisableControls;
        try
            cds_Comprobantes.First;
            while not cds_Comprobantes.Eof do begin

                if cds_Comprobantes.FieldByName('Cobrar').AsBoolean then begin
                    Compro := TComprobante.Create;
                    Compro.CodigoCliente            := cds_Comprobantes.FieldByName('CodigoPersona').AsInteger;
                    Compro.TipoComprobante          := cds_Comprobantes.FieldByName('TipoComprobante').AsString;
                    Compro.NumeroComprobante        := cds_Comprobantes.FieldByName('NumeroComprobante').AsInteger;
                    Compro.FechaEmision             := cds_Comprobantes.FieldByName('FechaEmision').AsDateTime;
                    Compro.FechaVencimiento         := cds_Comprobantes.FieldByName('FechaVencimiento').AsDateTime;
                    Compro.TotalComprobante         := cds_Comprobantes.FieldByName('TotalComprobante').AsFloat;
                    Compro.TotalAPagar              := cds_Comprobantes.FieldByName('SaldoPendiente').AsFloat;
                    Compro.TotalPagos               := cds_Comprobantes.FieldByName('TotalPagos').AsFloat;
                    Compro.Pagado                   := (cds_Comprobantes.FieldByName('EstadoPago').AsString = COMPROBANTE_PAGO);
                    Compro.EstadoDebito             := cds_Comprobantes.FieldByName('EstadoDebito').AsString;
                    Compro.DescriEstadoPago         := cds_Comprobantes.FieldByName('DescriEstado').AsString;
                    Compro.UltimoComprobante        := cds_Comprobantes.FieldByName('UltimoComprobante').AsInteger;
                    Compro.TipoComprobanteFiscal    := cds_Comprobantes.FieldByName('TipoComprobanteFiscal').AsString;
                    Compro.NumeroComprobanteFiscal  := cds_Comprobantes.FieldByName('NumeroComprobanteFiscal').AsInteger;

                    // Rev. 16 (Infractores Fase 2)
                    Compro.TieneConceptosInfractor :=
                        QueryGetValueInt(
                                DMConnections.BaseCAC,
                                Format(
                                    SQL_NI,
                                    [cds_Comprobantes.FieldByName('TipoComprobante').AsString,
                                     cds_Comprobantes.FieldByName('NumeroComprobante').AsInteger])) = 1;
                    // Fin Rev. 16 (Infractores Fase 2)
                    Result.Add(Compro);
                end;
                cds_Comprobantes.Next;
            end;
        finally
            cds_Comprobantes.EnableControls;
        end;

        if (Result.Count = 0) then begin
            FreeAndNil(Result);
        end;
    end;
end;

{-----------------------------------------------------------------------------
 Function Name  :   ImprimirDetalleInfraccionesAnuladas
 Parameters     :   TipoComprobante: string; NroComprobante: int64
 Return Value   :   Boolean
 Author         :   pdominguez
 Date Created   :   05/06/2010
 Description    :   Imprime el detalle de Infracciones Anuladas.
-----------------------------------------------------------------------------}
function TformCobroComprobantesCastigo.ImprimirDetalleInfraccionesAnuladas(TipoComprobante: string; NroComprobante: int64): Boolean;
resourcestring
    MSG_ERROR_COBRAR	= 'No se actualiz� el campo "ReImpresi�n" para el comprobante';

var
    f : TRptDetInfraccionesAnuladas;
    DescError : string;
begin
    Result := False;

    Application.CreateForm(TRptDetInfraccionesAnuladas,f);
    try
        if f.Inicializar(DMConnections.BaseCAC, TipoComprobante, NroComprobante, DescError, True) then begin  //REV.9
			Result := True;
        end
        else MsgBox( DescError, Caption, MB_ICONERROR );

	finally
        f.Release;
    end;
end;

{-----------------------------------------------------------------------------
  Procedure :   TformPagoComprobantes.LimpiarCampos
  Author    :   gcasais
  Date      :   19-Ene-2005
  Arguments :   None
  Result    :   None
-----------------------------------------------------------------------------}
procedure TformCobroComprobantesCastigo.LimpiarCampos;
begin
    FCantidadCuotasCobrar := 0;
    FCantidadComprobantesCastigados := 0;
  	edNumeroComprobante.Clear;
  	peNumeroDocumento.Clear;
    FCodigoCliente := -1;
  	LimpiarConvenios;
    LimpiarListaComprobantes;
  	LimpiarDetalles;

    if edNumeroComprobante.Enabled then ActiveControl := edNumeroComprobante
    else if peNumeroDocumento.Enabled then ActiveControl := peNumeroDocumento;

    lblEnListaAmarilla.Visible := False;
    end;

{-----------------------------------------------------------------------------
  Procedure :   FormResize
  Author    :
  Date      :
  Arguments :
  Result    :
-----------------------------------------------------------------------------}
procedure TformCobroComprobantesCastigo.FormResize(Sender: TObject);
begin
    if ClientHeight < FMinHeight then ClientHeight := FMinHeight;
    if ClientWidth < FMinWidth then ClientWidth := FMinWidth;
end;

{-----------------------------------------------------------------------------
  Function Name :   TienePagoAutomatico
  Author        :
  Date Created  :   13/10/2005
  Description   :
  Parameters    :
  Return Value  :
-----------------------------------------------------------------------------}
function TformCobroComprobantesCastigo.TienePagoAutomatico(TipoComprobante: AnsiString; NumeroComprobante: Integer; var TipoMedioPago: Integer): Boolean;
begin
    Result := (cds_Comprobantes.FieldByName('DescPATPAC').AsString <> '') and (cds_Comprobantes.FieldByName('Rechazo').AsString = '');

end;

{-----------------------------------------------------------------------------
  Function Name :   VerificaComprobantesMarcados
  Author        :
  Date Created  :
  Description   :
  Parameters    :
  Return Value  :
-----------------------------------------------------------------------------}
procedure TformCobroComprobantesCastigo.VerificaComprobantesMarcados;
    var
        Puntero: TBookmark;
begin
    try
        CambiarEstadoCursor(CURSOR_RELOJ, Self, FComponentes);

        Puntero := cds_Comprobantes.GetBookmark;
        //cds_Comprobantes.DisableControls;
        ds_Comprobantes.DataSet := Nil;

        case cds_Comprobantes.FieldByName('Cobrar').AsBoolean of
            // Marcar todas las Anteriores
            True: begin
                cds_Comprobantes.Prior;

                while not cds_Comprobantes.Bof do begin
                    if (not cds_Comprobantes.FieldByName('Cobrar').AsBoolean) and (not cds_Comprobantes.FieldByName('EstadoRefinanciacion').AsInteger in [REFINANCIACION_ESTADO_EN_TRAMITE, REFINANCIACION_ESTADO_ACEPTADA]) then begin //SS-1051_PDO
                        cds_Comprobantes.Edit;
                        cds_Comprobantes.FieldByName('Cobrar').AsBoolean := True;
                        cds_Comprobantes.Post;

                        Inc(FCantidadComprobantesCobrar);
                        FTotalComprobantesCobrar := FTotalComprobantesCobrar + cds_Comprobantes.FieldByName('SaldoPendiente').AsFloat;
                        lblTotalSelec.Caption:=FormatFloat(FORMATO_IMPORTE, FTotalComprobantesCobrar);
                    end;

                    cds_Comprobantes.Prior;
                end;
            end;
          end;
     finally
        if Assigned(Puntero) then begin
            if cds_Comprobantes.BookmarkValid(Puntero) then begin
                cds_Comprobantes.GotoBookmark(Puntero);
            end;
            cds_Comprobantes.FreeBookmark(Puntero);
        end;

        ds_Comprobantes.DataSet := cds_Comprobantes;
        cds_Comprobantes.EnableControls;

        CambiarEstadoCursor(CURSOR_DEFECTO, Self, FComponentes);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name :   dl_ComprobantesDrawText
  Author        :
  Date Created  :
  Description   :
  Parameters    :
  Return Value  :
-----------------------------------------------------------------------------}
procedure TformCobroComprobantesCastigo.dl_ComprobantesDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;State: TOwnerDrawState; var Text: String; var TxtRect: TRect;var ItemWidth: Integer; var DefaultDraw: Boolean);
var
    bmp: TBitMap;
begin
    with Sender do begin
        if (Column.FieldName = 'Cobrar') then begin
            Text := '';
            Canvas.FillRect(Rect);
            bmp := TBitMap.Create;
			DefaultDraw := False;
            try
                if (cds_Comprobantes.FieldByName('Cobrar').AsBoolean ) then begin
                    ilCheck.GetBitmap(0, Bmp);
                end
                else begin
                    if cds_Comprobantes.FieldByName('EstadoRefinanciacion').AsInteger in [REFINANCIACION_ESTADO_EN_TRAMITE, REFINANCIACION_ESTADO_ACEPTADA] then begin
                        ilCheck.GetBitmap(2, Bmp);
                    end
                    else begin
                        ilCheck.GetBitmap(1, Bmp);
                    end;
                end;

                Canvas.Draw((Rect.Left + Rect.Right - Bmp.Width) div 2, Rect.Top, bmp);
            finally
                bmp.Free;
            end;
        end;

        if	(Column.FieldName = 'FechaVencimiento') and
        	(	cds_Comprobantes.FieldByName('FechaVencimiento').IsNull or
                (cds_Comprobantes.FieldByName('FechaVencimiento').AsDateTime <= 0) )	then begin
            Text := '';
    	end;

    end; // with
end;

{-----------------------------------------------------------------------------
  Function Name :   btnSalirClick
  Author        :
  Date Created  :   13/10/2005
  Description   :
  Parameters    :
  Return Value  :
-----------------------------------------------------------------------------}
procedure TformCobroComprobantesCastigo.btnSalirClick(Sender: TObject);
begin
    Close;
end;


{-----------------------------------------------------------------------------
  Function Name :   FormActivate
  Author        :
  Date Created  :
  Description   :
  Parameters    :
  Return Value  :
-----------------------------------------------------------------------------}
procedure TformCobroComprobantesCastigo.FormActivate(Sender: TObject);
begin
    //TPanelMensajesForm.OcultaPanel;   //TASK_049_GLE_20170207
end;

{-----------------------------------------------------------------------------
  Function Name :   FormClose
  Author        :
  Date Created  :
  Description   :
  Parameters    :
  Return Value  :
-----------------------------------------------------------------------------}
procedure TformCobroComprobantesCastigo.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    FLogo.Free;
    Action := caFree;

    if Assigned(RefinanciacionConsultasForm) then RefinanciacionConsultasForm.RefrescarDatos;
end;

{-----------------------------------------------------------------------------
  Function Name :   lst1DrawBackground
  Author        :
  Date Created  :
  Description   :
  Parameters    :
  Return Value  :
-----------------------------------------------------------------------------}
procedure TformCobroComprobantesCastigo.lst1DrawBackground(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState; var DefaultDraw: Boolean);
begin
    with Sender do begin
        if ((cdsCuotasAPagarFechaCuota.AsDateTime < FAhora) and cdsCuotasAPagarEstadoImpago.AsBoolean) or
            (cdsCuotasAPagarCodigoEstadoCuota.AsInteger in [REFINANCIACION_ESTADO_CUOTA_ANULADA, REFINANCIACION_ESTADO_CUOTA_ANULADA_POR_MORA]) then begin
            if (odSelected in State) and (odFocused in state) then Canvas.Brush.Color := clMaroon;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name :   lst1DrawText
  Author        :
  Date Created  :
  Description   :
  Parameters    :
  Return Value  :
-----------------------------------------------------------------------------}
procedure TformCobroComprobantesCastigo.lst1DrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;State: TOwnerDrawState; var Text: String; var TxtRect: TRect;var ItemWidth: Integer; var DefaultDraw: Boolean);
var
    bmp: TBitMap;
begin
    with Sender do begin
        if (Column.FieldName = 'Cobrar') then begin
            Text := '';
            Canvas.FillRect(Rect);
            bmp := TBitMap.Create;
			DefaultDraw := False;
            try
                if (cdsComprobantesCastigados.FieldByName('Cobrar').AsBoolean ) then begin
                    ilCheck.GetBitmap(2, Bmp);
                end
                else begin
                    if cdsComprobantesCastigados.FieldByName('EstadoPago').AsString <> 'X' then begin
                        ilCheck.GetBitmap(0, Bmp);
                    end
                    else begin
                        ilCheck.GetBitmap(1, Bmp);
                    end;
                end;

                Canvas.Draw((Rect.Left + Rect.Right - Bmp.Width) div 2, Rect.Top, bmp);
            finally
                bmp.Free;
            end;
        end;

        if	(Column.FieldName = 'FechaEmision') and
        	(	cdsComprobantesCastigados.FieldByName('FechaEmision').IsNull or
                (cdsComprobantesCastigados.FieldByName('FechaEmision').AsDateTime <= 0) )	then begin
            Text := '';
    	end;

    end; // with
end;

{-----------------------------------------------------------------------------
  Function Name :   lst1LinkClick
  Author        :
  Date Created  :
  Description   :
  Parameters    :
  Return Value  :
-----------------------------------------------------------------------------}
procedure TformCobroComprobantesCastigo.lst1LinkClick(Sender: TCustomDBListEx; Column: TDBListExColumn);
begin
    cdsCuotasAPagar.Edit;
    cdsCuotasAPagarCobrar.AsBoolean := not cdsCuotasAPagarCobrar.AsBoolean;
    cdsCuotasAPagar.Post;

    if cdsCuotasAPagarCobrar.AsBoolean then begin
        Inc(FCantidadCuotasCobrar);
        FTotalCuotasCobrar := FTotalCuotasCobrar + cdsCuotasAPagarSaldo.AsLargeInt;
    end
    else begin
        Dec(FCantidadCuotasCobrar);
        FTotalCuotasCobrar := FTotalCuotasCobrar - cdsCuotasAPagarSaldo.AsLargeInt;
    end;

    VerificarCuotasACobrar;
end;

{-----------------------------------------------------------------------------
  Function Name :   VerificarCuotasACobrar
  Author        :
  Date Created  :
  Description   :
  Parameters    :
  Return Value  :
-----------------------------------------------------------------------------}
procedure TformCobroComprobantesCastigo.VerificarCuotasACobrar;
    var
        Puntero: TBookmark;
begin
    try
        CambiarEstadoCursor(CURSOR_RELOJ, Self, FComponentes);

        Puntero := cdsCuotasAPagar.GetBookmark;  //you can return to the same record
        cdsCuotasAPagar.DisableControls;
        dsCuotasAPagar.DataSet := Nil;

        case cdsCuotasAPagarCobrar.AsBoolean of
            // Marcar todas las Anteriores
            True: begin
                cdsCuotasAPagar.Prior;

                while not cdsCuotasAPagar.Bof do begin
                    if (not cdsCuotasAPagarCobrar.AsBoolean) then begin
                        cdsCuotasAPagar.Edit;
                        cdsCuotasAPagarCobrar.AsBoolean := True;
                        cdsCuotasAPagar.Post;

                        Inc(FCantidadCuotasCobrar);
                        FTotalCuotasCobrar := FTotalCuotasCobrar + cdsCuotasAPagarSaldo.AsLargeInt;
                    end;

                    cdsCuotasAPagar.Prior;
                end;
            end;
            // Desmarcar todas las Posteriores
            False: begin
                cdsCuotasAPagar.Next;

                while not cdsCuotasAPagar.Eof do begin
                    if cdsCuotasAPagarCobrar.AsBoolean then begin
                        cdsCuotasAPagar.Edit;
                        cdsCuotasAPagarCobrar.AsBoolean := False;
                        cdsCuotasAPagar.Post;

                        Dec(FCantidadCuotasCobrar);
                        FTotalCuotasCobrar := FTotalCuotasCobrar - cdsCuotasAPagarSaldo.AsLargeInt;
                    end;

                    cdsCuotasAPagar.Next;
                end;
            end;
        end;

    finally
        if Assigned(Puntero) then begin
            if cdsCuotasAPagar.BookmarkValid(Puntero) then begin
                cdsCuotasAPagar.GotoBookmark(Puntero);
            end;
            cdsCuotasAPagar.FreeBookmark(Puntero);
        end;

        dsCuotasAPagar.DataSet := cdsCuotasAPagar;
        cdsCuotasAPagar.EnableControls;

        lblCuotasCantidad.Caption := IntToStr(FCantidadCuotasCobrar);
        lblCuotasTotal.Caption    := FormatFloat(FORMATO_IMPORTE, FTotalCuotasCobrar);
        ActualizaListaObjetos(FComponentes, TObject(btnAnularCastigo), FbtnCobrarCuotaPermisos and (FCantidadCuotasCobrar > 0));

        CambiarEstadoCursor(CURSOR_DEFECTO, Self, FComponentes);
    end;
end;

{-----------------------------------------------------------------------------
 Function Name  :   VerificarConvenioListaAmarilla
 Author         :   CQuezadaI
 Date           :   30/04/2013
 Firma          :   SS_660_CQU_20121010
 Description    :   Se varifica si el convenio est� en Lista Amarilla.
-----------------------------------------------------------------------------}
function TformCobroComprobantesCastigo.VerificarConvenioListaAmarilla(CodigoConvenio : Integer) : string;
resourcestring
      SQL_CONSULTACONVENIO        = 'SELECT dbo.ObtenerEstadoConvenioEnListaAmarilla(%d)';

begin
      Result      :=  '';
    if CodigoConvenio > 0 then begin
        try
            CambiarEstadoCursor(CURSOR_RELOJ, Self, FComponentes);
            try
               Result:= QueryGetValue(   DMConnections.BaseCAC,
                                           Format(SQL_CONSULTACONVENIO,
                                                         [   CodigoConvenio
                                                         ]
                                                 )
                                      );
             except
                on e: Exception do begin
                    ShowMsgBoxCN(e, Self);
                end;
            end;
        finally
            CambiarEstadoCursor(CURSOR_DEFECTO, Self, FComponentes);
        end;
    end;
end;



procedure TformCobroComprobantesCastigo.cbConveniosDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  if Pos( TXT_CONVENIO_DE_BAJA, cbConvenios.Items[Index].Caption ) > 0 then  begin

     ItemComboBoxColor(Control, Index, Rect, State, clred, true);
   end
  else begin
     ItemComboBoxColor(Control, Index, Rect, State);
  end;
end;
{-----------------------------------------------------------------------------
  Function Name :   CargarComprobantesCastigados
  Author        :   GLEON
  Date Created  :   28/11/2016
  Description   :   Busca y carga los comprobantes cuyo estado "pagado" sea "castigo"
  Parameters    :   CodigoPersona
  Return Value  :
-----------------------------------------------------------------------------}
procedure TformCobroComprobantesCastigo.CargarComprobantesCastigados(CodigoPersona: Integer);
resourcestring
    MSG_ERROR_ON_LOAD = 'Ha ocurrido un error al intentar obtener los Comprobantes Castigados.';
var
    EncontradoComprobante: Boolean;
    result : Boolean;
    CodConvenio : Integer;
    FTotalSaldoConvenio : Integer;
begin
    EncontradoComprobante := False;
    FTotalSaldoConvenio := 0;
    try
        try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        if CodigoPersona > 0 then begin
            if CodConvenio = 0 then begin
               CodConvenio  :=   QueryGetValueInt( DMConnections.BaseCAC,
                                Format('SELECT DISTINCT CodigoConvenio FROM Comprobantes WHERE TipoDocumento = ''RUT'' ' + 'AND NumeroDocumento = ''%s''',
                                            [iif(peNumeroDocumento.Text <> '', trim(PadL(peNumeroDocumento.Text, 9, '0')), '')]));
            end;

            if CodConvenio > 0 then begin

                with spObtenerComprobantesCastigados do begin
                    if Active then Close ;
                    Parameters.Refresh;

                    with Parameters do begin
                        ParamByName('@CodigoConvenio').Value := CodConvenio ;
                    end;
                    Open;
                    ExecProc ;
                end;

                //TPanelMensajesForm.MuestraMensaje('Cargando Comprobantes Castigados ...'); //TASK_049_GLE_20170207
               cdsComprobantesCastigados.EmptyDataSet;                                       //TASK_049_GLE_20170207
               while not spObtenerComprobantesCastigados.Eof do begin

                        cdsComprobantesCastigados.Append;

                        cdsComprobantesCastigados.FieldByName('TipoComprobante').AsString := spObtenerComprobantesCastigados.FieldByName('TipoComprobante').AsString;
                        cdsComprobantesCastigados.FieldByName('NumeroComprobante').AsInteger := spObtenerComprobantesCastigados.FieldByName('NumeroComprobante').AsInteger;
                        cdsComprobantesCastigados.FieldByName('FechaEmision').AsDateTime := spObtenerComprobantesCastigados.FieldByName('FechaEmision').AsDateTime;
                        cdsComprobantesCastigados.FieldByName('TotalComprobante').AsInteger := spObtenerComprobantesCastigados.FieldByName('TotalAPagar').AsInteger;
                        cdsComprobantesCastigados.FieldByName('EstadoPago').AsString := spObtenerComprobantesCastigados.FieldByName('EstadoPago').AsString;
                        cdsComprobantesCastigados.FieldByName('CodigoConvenio').AsInteger := spObtenerComprobantesCastigados.FieldByName('CodigoConvenio').AsInteger;

                        FTotalSaldoConvenio := FTotalSaldoConvenio  +  cdsComprobantesCastigados.FieldByName('TotalComprobante').AsInteger;

                            if not EncontradoComprobante then begin
                                cdsComprobantesCastigados.FieldByName('Cobrar').AsBoolean := True;
                                inc(FCantidadComprobantesCastigados);

                            end
                            else cdsComprobantesCastigados.FieldByName('Cobrar').AsBoolean := False;

                                cdsComprobantesCastigados.Post;
                                spObtenerComprobantesCastigados.Next;

                            lst1Click(lst1);

                        end;

                    end;

            end;

        //LimpiarDetalles;
        btnAnularCastigo.Enabled := False;

        except
            on E: Exception do MsgBoxErr(MSG_ERROR_ON_LOAD, E.Message, Caption, MB_ICONERROR);
        end;

    finally
        //lblMontoDeudasConvenio.Caption  := FormatFloat(FORMATO_IMPORTE, FTotalSaldoConvenio); //TASK_049_GLE_20170207
        lblCuotasTotal.Caption          := FormatFloat(FORMATO_IMPORTE, FTotalSaldoConvenio);
        //TPanelMensajesForm.OcultaPanel;  //TASK_049_GLE_20170207

        //Si hay comprobantes castigados, habilitar el bot�n de Anular Castigo
        btnAnularCastigo.Enabled := (FCantidadComprobantesCastigados > 0);
        Screen.Cursor := crDefault;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name :   MarcarComprobantes
  Author        :   GLEON
  Date Created  :   28/11/2016
  Description   :   Marca todos los comprobantes impagos
  Parameters    :   Marcar
  Return Value  :
-----------------------------------------------------------------------------}
procedure TformCobroComprobantesCastigo.MarcarComprobantes(Marcar: Boolean);
    var
        ValorActualCobrar: Boolean;
        Puntero: TBookmark;
begin
    try
        if (not cds_Comprobantes.IsEmpty) then begin

            CambiarEstadoCursor(CURSOR_RELOJ, Self);
            Puntero := cds_Comprobantes.GetBookmark;
            ds_Comprobantes.DataSet := Nil;

            cds_Comprobantes.First;

            while not cds_Comprobantes.Eof do begin
                if not (cds_Comprobantes.FieldByName('EstadoRefinanciacion').AsInteger in [REFINANCIACION_ESTADO_EN_TRAMITE, REFINANCIACION_ESTADO_ACEPTADA]) then begin
                    with cds_Comprobantes do begin
                        ValorActualCobrar := FieldByName('Cobrar').AsBoolean;

                        Edit;
                        FieldByName('Cobrar').AsBoolean := Marcar;

                        if ValorActualCobrar <> FieldByName('Cobrar').AsBoolean  then begin
                            if FieldByName('Cobrar').AsBoolean then begin
                                Inc(FCantidadComprobantesCobrar);
                                FTotalComprobantesCobrar := FTotalComprobantesCobrar + FieldByName('SaldoPendiente').AsFloat;
                                lblTotalSelec.Caption:=FormatFloat(FORMATO_IMPORTE, FTotalComprobantesCobrar);
                            end else begin
                                Dec(FCantidadComprobantesCobrar);
                                FTotalComprobantesCobrar := FTotalComprobantesCobrar - FieldByName('SaldoPendiente').AsFloat;
                                if FTotalComprobantesCobrar < 0 then begin
                                    lblTotalSelec.Caption:=FormatFloat(FORMATO_IMPORTE, 0);
                                    FTotalComprobantesCobrar :=0;
                                end else begin
                                    lblTotalSelec.Caption:=FormatFloat(FORMATO_IMPORTE, FTotalComprobantesCobrar);
                                end;
                            end;
                        end;

                        lblTotalAPagar.Caption := FormatFloat(FORMATO_IMPORTE , FSaldoConvenios - FTotalComprobantesCobrar);
                        Post;
                    end;
                end;
                 cds_Comprobantes.Next;
            end;
        end;

    finally
        if cds_Comprobantes.BookmarkValid(Puntero) then begin
            cds_Comprobantes.GotoBookmark(Puntero);
        end;
        cds_Comprobantes.FreeBookmark(Puntero);
        ds_Comprobantes.DataSet := cds_Comprobantes;

        btnCastigar.Enabled := (FCantidadComprobantesCobrar > 0);
        CambiarEstadoCursor(CURSOR_DEFECTO, Self);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name :   btnFiltrarClick
  Author        :
  Date Created  :
  Description   :
  Parameters    :   Sender
  Return Value  :   None
-----------------------------------------------------------------------------}
procedure TformCobroComprobantesCastigo.btnFiltrarClick(Sender: TObject);
var
Rut : string;
begin
    Screen.Cursor := crHourGlass;
    btnFiltrar.Enabled:=False;

    LimpiarDetalles;
    LimpiarConvenios;
    LimpiarListaComprobantes;

    try
    //Si el usuario busca por RUT
    if Length(Trim(peNumeroDocumento.Text)) >= 8 then
    begin
        FCodigoCliente := QueryGetValueInt( DMConnections.BaseCAC,
                                            Format('SELECT CodigoPersona FROM Personas WITH (NOLOCK) WHERE CodigoDocumento = ''RUT'' ' + 'AND NumeroDocumento = ''%s''',
                                            [iif(peNumeroDocumento.Text <> '', trim(PadL(peNumeroDocumento.Text, 9, '0')), '')]));
        if FCodigoCliente > 0 then
        begin
            CargarConveniosRUT(DMConnections.BaseCAC, cbConvenios, 'RUT', peNumeroDocumento.Text, False, 0, True);
            MostrarDatosCliente(FCodigoCliente);
            CargarComprobantes;
        end
    end
    //Si el usuario busca por N�mero de Comprobante
    else if Trim(edNumeroComprobante.Text)<>'' then
    begin
        FCodigoCliente := QueryGetValueInt( DMConnections.BaseCAC,
                                            Format('SELECT CodigoPersona FROM Comprobantes WITH (NOLOCK) WHERE TipoComprobante = ''NK'' ' + ' AND NumeroComprobante = ''%s''',
                                            [iif(edNumeroComprobante.Text <> '', trim(edNumeroComprobante.Text),'')]));
        if FCodigoCliente > 0 then
        begin
            Rut := QueryGetValue( DMConnections.BaseCAC,
                                    Format ('SELECT NumeroDocumento FROM Personas WITH (NOLOCK) WHERE CodigoPersona = ''%d'' ', [FCodigoCliente]));
            Rut := trim(PadL(Rut, 9, '0'));
            if Rut <> EmptyStr then begin
                CargarConveniosRUT(DMConnections.BaseCAC, cbConvenios, 'RUT', Rut, False, 0, True);
                MostrarDatosCliente(FCodigoCliente);
                CargarComprobantes;
            end else begin
                MessageDlg('Error Cargando Comprobantes',mtError, mbOKCancel, 0);
            end;

        end else begin
            MessageDlg('No se encontraron Comprobantes Impagos',mtError, mbOKCancel, 0);
        end;

    end;
    finally

        btnFiltrar.Enabled:=True;
        Screen.Cursor := crDefault;
    end;
end;


procedure TformCobroComprobantesCastigo.btn1Click(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name :   btnAnularCastigoClick
  Author        :   GLEON
  Date Created  :   28/11/2016
  Description   :   Anula el estado castigado de los comprobantes del convenio
  Parameters    :
  Return Value  :
-----------------------------------------------------------------------------}
procedure TformCobroComprobantesCastigo.btnAnularCastigoClick(Sender: TObject);
      resourcestring
        RS_ERROR_EJECUCION_TITULO  = 'Error de Ejecuci�n';
        RS_ERROR_EJECUCION_MENSAJE = 'Error en la ejecuci�n del procedimiento %s. C�digo de Error: %d.';
        RS_MENSAJE_ERROR_CAMBIO_ESTADO = 'Se Produjo un Error al actualizar el Estado del comprobante.';

        RS_TITULO_ACCION     = 'Anular Castigo Convenio';                                                     //'Pagar Cuota';
        RS_MENSAJE_ACCION    = '�Confirma la anulaci�n del castigo del Convenio?';                           //'� Confirma el pago de la(s) Cuota(s) seleccionada(s) ?';
        RS_ERROR_ACCION      = 'Se Produjo un Error al efectuar el Registro del Pago Castigado de la Cuota.'; //'Se Produjo un Error al efectuar el Registro del Pago de ls Cuota.';
        RS_MENSAJE_RESULTADO = 'Se Castig� la Cuota Correctamente.';
    var
        Mensaje: String;
        PagoAceptado: Boolean;
        fA: Tfrm_AnulacionRecibos;
        flag : Boolean;
begin
    FCodigoCliente := -1;
    PagoAceptado := False;
    Mensaje := RS_MENSAJE_ACCION;
    flag := true;   //TASK_054_GLE_20170222

    try

        FCodigoCliente := QueryGetValueInt( DMConnections.BaseCAC,
                                            Format('SELECT CodigoPersona FROM Personas WITH (NOLOCK) WHERE CodigoDocumento = ''RUT'' ' + 'AND NumeroDocumento = ''%s''',
                                            [iif(peNumeroDocumento.Text <> '', trim(PadL(peNumeroDocumento.Text, 9, '0')), '')]));

    	{INICIO	: TASK_124_CFU_20170320 //Corrige error
        if FCodigoCliente <> -1 then begin
        }
        if FCodigoCliente > 0 then begin
        //TERMINO	: TASK_124_CFU_20170320 //Corrige error

            CambiarEstadoCursor(CURSOR_RELOJ, Self, FComponentes);

            if ShowMsgBoxCN(RS_TITULO_ACCION, Mensaje, MB_ICONQUESTION, Self) = mrOk then begin

                //Application.CreateForm(Tfrm_AnulacionRecibos, fA);
                //fA := Tfrm_AnulacionRecibos.Create(nil);                //TASK_055_GLE_20170224   //TASK_056_GLE_20170227
                fA := Tfrm_AnulacionRecibos.Create(Self);                                           //TASK_056_GLE_20170227
                //INICIA: TASK_054_GLE_20170222
                //fA.Inicializar(peNumeroDocumento.Text);                                           //TASK_056_GLE_20170227

                if not fA.Inicializar(peNumeroDocumento.Text) then begin                            //TASK_056_GLE_20170227
                    fA.release;                                                                     //TASK_056_GLE_20170227
                end else begin //fA.show;                                                           //TASK_056_GLE_20170227
                	fA.cbMotivosAnulacion.Value := 29;

                    if fA.ShowModal = mrabort then flag:= false; //else fA.Release;                //TASK_056_GLE_20170227
               //         flag:= false;                                                             //TASK_056_GLE_20170227
               //     end                                                                           //TASK_056_GLE_20170227
               //     else begin                                                                    //TASK_056_GLE_20170227
               //         fA.Release;                                                               //TASK_056_GLE_20170227
               //     end;                                                                          //TASK_056_GLE_20170227
                end;
            {INICIO	: TASK_124_CFU_20170320 //Corrige error
        	end
        	else begin
            	ShowMsgBoxCN('Anulaci�n Castigo Convenio ', 'Verifique que el RUT sea v�lido', MB_ICONEXCLAMATION, Self);
        	end;
            }
        	end
            else
            	Exit;
        end
        else begin
            ShowMsgBoxCN('Anulaci�n Castigo Convenio ', 'Verifique que el RUT sea v�lido', MB_ICONEXCLAMATION, Self);
            Exit;
        end;
        //TERMINO	: TASK_124_CFU_20170320 //Corrige error
    finally
        if not flag then begin

            cdsComprobantesCastigados.First;
            while (not cdsComprobantesCastigados.Eof) do begin

            //Cambia estado de cada comprobante castigado
            with spAnularPagosComprobantesCastigados do begin
                if Active then Close;
                    Parameters.Refresh;

                    with Parameters do begin
                        ParamByName('@TipoComprobante').Value     := cdsComprobantesCastigados.FieldByName('TipoComprobante').AsString;;
                        ParamByName('@NumeroComprobante').Value   := cdsComprobantesCastigados.FieldByName('NumeroComprobante').AsInteger; //spObtenerComprobantesCastigados.FieldByName('NumeroComprobante').AsInteger;;
                    end;

                    ExecProc;
            end;

            if spAnularPagosComprobantesCastigados.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
                raise EErrorExceptionCN.Create(RS_ERROR_EJECUCION_TITULO,Format(RS_ERROR_EJECUCION_MENSAJE,
                                                                        [spAnularPagosComprobantesCastigados.ProcedureName,
                                                                        spAnularPagosComprobantesCastigados.Parameters.ParamByName('@RETURN_VALUE').Value]));
                end
            else begin
                PagoAceptado := True;

                //INICIO	: TASK_124_CFU_20170320
				with spEliminarEstadoConvenio, Parameters do begin
                    if Active then 
                    	Close;
                    Parameters.Refresh;
                    ParamByName('@CodigoConvenio').Value	:= cbConvenios.Value;
                    ParamByName('@EstadoConvenio').Value	:= CONST_ESTADO_CONVENIO_CASTIGADO;
                    ExecProc;

                    if Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
                        raise EErrorExceptionCN.Create(
                            'Error de Ejecuci�n',
                            Format('Error en la ejecuci�n del procedimiento %s. C�digo de Error: %s.',
                                   [ProcedureName,
                                    Parameters.ParamByName('@RETURN_VALUE').Value]));
                    end;
                end;
				//TERMINO	: TASK_124_CFU_20170320 //Corrige error
            end;

            cdsComprobantesCastigados.Next;
            end;

            //Application.ProcessMessages;     //*
        end;
        //TERMINA: TASK_054_GLE_20170222
        if PagoAceptado then
        begin
            try
                sp_ActualizarClienteMoroso.Parameters.ParamByName('@NumeroDocumento').Value := paNullable;
                sp_ActualizarClienteMoroso.Parameters.ParamByName('@CodigoRefinanciacion').Value := cdsCuotasAPagarCodigoRefinanciacion.AsInteger;
                sp_ActualizarClienteMoroso.Parameters.ParamByName('@Usuario').Value  := UsuarioSistema;
                sp_ActualizarClienteMoroso.Parameters.ParamByName('@ErrorDescription').Value  := paNullable;
                sp_ActualizarClienteMoroso.ExecProc;

            except
                on e: Exception do begin
                    MsgBoxErr('ERROR Revisando morosidad', e.Message, 'ERROR Revisando morosidad', MB_ICONERROR);
                end;
            end;
        end;

        //CambiarEstadoCursor(CURSOR_DEFECTO, Self, FComponentes);              //TASK_055_GLE_20170224

        try                                                                     //TASK_055_GLE_20170224
            if PagoAceptado then begin
                LimpiarDatosCliente;
                LimpiarCampos;
            end;

            //FreeAndNil(spAnularPagosComprobantesCastigados);                  //TASK_056_GLE_20170227 //*
            //FreeAndNil(sp_ActualizarClienteMoroso);                           //TASK_056_GLE_20170227 //*
        except
            on e: Exception do begin                                            //TASK_055_GLE_20170224
                ShowMsgBoxCN(e, Self);                                          //TASK_055_GLE_20170224
            end;                                                                //TASK_055_GLE_20170224
        end;                                                                    //TASK_055_GLE_20170224

       CambiarEstadoCursor(CURSOR_DEFECTO, Self, FComponentes);                 //TASK_055_GLE_20170224

    end;
end;

procedure TformCobroComprobantesCastigo.cbConveniosChange(Sender: TObject);
resourcestring
	MSG_ERROR = 'Error';
	MSG_ERROR_GETTING_ADDRESS = 'Error obteniendo el domicilio del cliente';
    MSG_CONVENIOENLISTAAMARILLA = 'Convenio se encuentra en lista amarilla';                         

var
    CodigoConvenio:Integer;
    EstadoListaAmarilla : String;
    TotalDeudasRefinanciacion,     //TASK_049_GLE_20170207
    TotalDeudasConvenios,          //TASK_049_GLE_20170207
    DeudaRefinanciacionVencida,    //TASK_049_GLE_20170207
    DeudaComprobantesVencida: Extended;  //TASK_049_GLE_20170207
const
    SQLDeudaConvenios = 'SELECT dbo.ObtenerSaldoDeLosConvenios(%d)';     //TASK_049_GLE_20170207
begin

    CodigoConvenio:= cbConvenios.Value;

    LimpiarListaComprobantes;

    if ActiveControl <> Sender then Exit;

    Cargarcomprobantes;

    //INICIA: TASK_049_GLE_20170207   - Actualiza montos de labels
    with spObtenerDeudaComprobantesVencidos do begin
        if Active then Close;
            Parameters.Refresh;

            Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
            Parameters.ParamByName('@CodigoCliente').Value  := FCodigoCliente;
            Open;

            DeudaComprobantesVencida := FieldByName('DeudaComprobantesVencidos').AsFloat;
    end;

    with spObtenerRefinanciacionesTotalesPersona do begin
        if Active then Close;
            Parameters.Refresh;
            Parameters.ParamByName('@CodigoPersona').Value := FCodigoCliente;
            Parameters.PAramByName('@CodigoConvenio').Value := CodigoConvenio;
            Open;

            TotalDeudasRefinanciacion  := FieldByName('TotalDeuda').AsFloat - FieldByName('TotalPagado').AsFloat;
            DeudaRefinanciacionVencida := FieldByName('DeudaVencida').AsFloat;
    end;

    if DeudaComprobantesVencida > 0 then begin
        lblMontoDeudasConvenio.Caption := FormatFloat(FORMATO_IMPORTE, DeudaComprobantesVencida);
        lblMontoDeudasConvenio.Font.Color := clRed;
        ShowMsgBoxCN('Validaci�n Comprobantes Vencidos', 'El Cliente tiene Comprobantes pendientes de Pago Vencidos, en este u en otro(s) Convenios(s).', MB_ICONWARNING, Self);
    end else begin
        lblMontoDeudasConvenio.Caption := '$ 0';
        lblMontoDeudasConvenio.Font.Color := clBlack;
    end;

    if DeudaRefinanciacionVencida > 0 then begin
        lblMontoDeudasRefinanciacion.Caption := FormatFloat(FORMATO_IMPORTE, DeudaRefinanciacionVencida);
        lblMontoDeudasRefinanciacion.Font.Color := clRed;
        ShowMsgBoxCN('Validaci�n Refinanciaci�n', 'El Cliente tiene Cuotas Vencidas por Refinanciaci�n.', MB_ICONWARNING, Self);
    end else begin
        lblMontoDeudasRefinanciacion.Caption := '$ 0';
        lblMontoDeudasRefinanciacion.Font.Color := clBlack;
    end;

    //TERMINA: TASK_049_GLE_20170207

    with spObtenerDomicilioFacturacion, Parameters do
    begin
        // Mostramos el domicilio de Facturaci�n
        Close;
        ParamByName('@CodigoConvenio').Value := cbConvenios.Value;
        try
          ExecProc;
          lblDomicilio.Caption := ParamByName('@Domicilio').Value + ' - ' + ParamByName('@Comuna').Value;
        except
          on e: exception do begin
              MsgBoxErr( MSG_ERROR_GETTING_ADDRESS, e.Message, MSG_ERROR, MB_ICONSTOP);
            end;
        end;
    end;

    EstadoListaAmarilla:= VerificarConvenioListaAmarilla(CodigoConvenio);

   if EstadoListaAmarilla <> '' then begin
    lblEnListaAmarilla.Visible  := True;
    lblEnListaAmarilla.Caption  := EstadoListaAmarilla;

    end
    else begin

    lblEnListaAmarilla.Visible  := False;

    end;

end;

procedure TformCobroComprobantesCastigo.btnComprobantesCastigosClick(
  Sender: TObject);
var
    CodPersona : Integer;
begin
    Screen.Cursor := crHourGlass;
    try
    //Verificamos si hay comprobantes castigados
    CodPersona := FCodigoCliente;
        if CodPersona <> -1 then begin
            CargarComprobantesCastigados(CodPersona);
        end
        else begin
        CodPersona := QueryGetValueInt( DMConnections.BaseCAC,
                                            Format('SELECT CodigoPersona FROM Personas WITH (NOLOCK) WHERE CodigoDocumento = ''RUT'' ' + 'AND NumeroDocumento = ''%s''',
                                            [iif(peNumeroDocumento.Text <> '', trim(PadL(peNumeroDocumento.Text, 9, '0')), '')]));
            if CodPersona <> -1 then begin
                CargarComprobantesCastigados(CodPersona);
            end
            else begin
                ShowMsgBoxCN('Carga Comprobantes Castigados', 'No se encontraron Comprobantes Castigados', MB_ICONQUESTION, Self);
            end;;
        end;
    finally

         Screen.Cursor := crDefault;
    end;
end;

end.
