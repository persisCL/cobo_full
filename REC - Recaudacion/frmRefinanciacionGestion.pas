{ -----------------------------------------------------------------------------
Firma       :   SS_1120_MVI_20130820
Descripcion :   Se cambia estilo del comboBox a: vcsOwnerDrawFixed,
                se crear el evento DrawItem y se llama al procedimeinto ItemComboBoxColor.


Firma       :   SS_1236_MCA_20141226
Descripcion : se agrega leyenda si el convenio esta en lista amarilla o en proceso.

Autor           :   CQuezadaI
Firma           :   SS_1245_CQU_20150224
Fecha           :   24-feb-2015
Descripci�n     :   Se agrega permiso para visualizar el bot�n btnSugerirImporteDemanda
                    y la llamada a la constante REFINANCIACION_FACTOR_IMPORTE_SUJERIDO
                    para obtener el valor que corresponda, seg�n ParametroGeneral

Firma       : SS_1408_MCA_20151027
Descripcion : muestra un mensaje si el cliente se encuentre en registrado en Tabla ClientesAContactar

-----------------------------------------------------------------------------}
unit frmRefinanciacionGestion;

interface

uses
  // Developer
  BuscaClientes, PeaTypes, Convenios, Util, UtilDB, DMConnection, PeaProcs, frmRefinanciacionSeleccionarRepLegalCliente,
  frmRefinanciacionSeleccionarComprobantes, frmRefinanciacionSeleccionarPatentesPrecautoria, frmRefinanciacionCalculoImportes,
  frmEditarDomicilio, frmMuestraMensaje, frmRefinanciacionEntrarEditarCuotas, frmRefinanciacionSeleccionarRepLegalCN,
  frmRefinanciacionEditarTelefonos, frmRefinanciacionImpresion, frmRefinanciacionCuotasHistorial, PeaProcsCN, SysUtilsCN,
  frmRefinanciacionCuotasPagosEfectivo, CobranzasResources, CobranzasClasses, frmConveniosEnCarpetaLegal,
  frmRefinanciacionesEnTramiteAceptadas,
  // Auto
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ExtCtrls, ListBoxEx, DBListEx, StdCtrls, DmiCtrls, VariantComboBox, CollPnl, Validate, DateEdit,
  DB, ADODB, DBClient, Provider, Contnrs;

type
  TRefinanciacionGestionForm = class(TForm)
    pnlDatosPrincipales: TPanel;
    GroupBox1: TGroupBox;
    vcbTipoRefinanciacion: TVariantComboBox;
    Label1: TLabel;
    Label2: TLabel;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    peRut: TPickEdit;
    Label4: TLabel;
    dedtFechaRefi: TDateEdit;
    lblNombreCli: TLabel;
    vcbConvenios: TVariantComboBox;
    Label7: TLabel;
    lblNumRefinanciacion: TLabel;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    lblNombreRepLegCli: TLabel;
    lblRutRepLegCliDesc: TLabel;
    btnCambiarRepLegCli: TButton;
    lblRutRepLegCli: TLabel;
    lblNombreRepLegCliDesc: TLabel;
    Label14: TLabel;
    lblRutRepLegCN: TLabel;
    lblNombreRepLegCNDesc: TLabel;
    lblNombreRepLegCN: TLabel;
    btnCambiarRepLegCN: TButton;
    GroupBox5: TGroupBox;
    lblCausaRol: TLabel;
    lblGastoNotarial: TLabel;
    nedtGastoNotarial: TNumericEdit;
    cbNotificacion: TCheckBox;
    Label5: TLabel;
    lblEstado: TLabel;
    pnlAcciones: TPanel;
    btnGrabar: TButton;
    btnCancelar: TButton;
    btnImprimir: TButton;
    pnlDatosDetalle: TPanel;
    GroupBox6: TGroupBox;
    PageControl1: TPageControl;
    tbsComprobantes: TTabSheet;
    Panel3: TPanel;
    btnEliminarComprobante: TButton;
    btnAgregarComprobantes: TButton;
    tbsCuotas: TTabSheet;
    Panel4: TPanel;
    btnEditarCuota: TButton;
    btnAgregarCuota: TButton;
    btnEliminarCuota: TButton;
    DBListEx3: TDBListEx;
    Panel7: TPanel;
    Label18: TLabel;
    lblCuotasCantidad: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    lblDeudaPagada2: TLabel;
    lblCuotasTotal: TLabel;
    Panel8: TPanel;
    Label9: TLabel;
    Label11: TLabel;
    lblTotalRefinanciado: TLabel;
    lblCantidadComprobantes: TLabel;
    spDetalleComprobante: TADOStoredProc;
    lblPersoneria: TLabel;
    lblConcesionaria: TLabel;
    cdsComprobantesRefinanciados: TClientDataSet;
    dsComprobantesRefinanciados: TDataSource;
    cdsDetalleComprobanteRefinanciado: TClientDataSet;
    dspDetalleComprobante: TDataSetProvider;
    dsDetalleComprobanteRefinanciado: TDataSource;
    cdsPatentesPrecautoria: TClientDataSet;
    dsPatentesPrecautoria: TDataSource;
    Label6: TLabel;
    lblTotalRefinanciado2: TLabel;
    cdsCuotas: TClientDataSet;
    dsCuotas: TDataSource;
    Label8: TLabel;
    Label10: TLabel;
    lblDeudaPagada: TLabel;
    lblDeudaVencida: TLabel;
    btnSugerirImportes: TButton;
    btnValidar: TButton;
    btnBuscarRut: TButton;
    spActualizarRefinanciacionDatosCabecera: TADOStoredProc;
    spObtenerRefinanciacionDatosCabecera: TADOStoredProc;
    spActualizarRefinanciacionComprobantes: TADOStoredProc;
    spObtenerRefinanciacionComprobantes: TADOStoredProc;
    dspObtenerRefinanciacionComprobantes: TDataSetProvider;
    spObtenerRefinanciacionCuotas: TADOStoredProc;
    dspObtenerRefinanciacionCuotas: TDataSetProvider;
    spActualizarRefinanciacionCuotas: TADOStoredProc;
    cdsCuotasCodigoEstadoCuota: TSmallintField;
    cdsCuotasDescripcionEstadoCuota: TStringField;
    cdsCuotasCodigoFormaPago: TSmallintField;
    cdsCuotasDescripcionFormaPago: TStringField;
    cdsCuotasTitularNombre: TStringField;
    cdsCuotasTitularTipoDocumento: TStringField;
    cdsCuotasTitularNumeroDocumento: TStringField;
    cdsCuotasCodigoBanco: TIntegerField;
    cdsCuotasDescripcionBanco: TStringField;
    cdsCuotasDocumentoNumero: TStringField;
    cdsCuotasFechaCuota: TDateTimeField;
    cdsCuotasCodigoTarjeta: TIntegerField;
    cdsCuotasDescripcionTarjeta: TStringField;
    cdsCuotasTarjetaCuotas: TSmallintField;
    cdsCuotasImporte: TLargeintField;
    cdsCuotasCodigoEntradaUsuario: TStringField;
    spObtenerRefinanciacionPatentesPrecautoria: TADOStoredProc;
    dspObtenerRefinanciacionPatentesPrecautoria: TDataSetProvider;
    cdsPatentesPrecautoriaPatente: TStringField;
    cdsPatentesPrecautoriaMarcaModeloColor: TStringField;
    cdsPatentesPrecautoriaConvenio: TStringField;
    cdsPatentesPrecautoriaCodigoEstadoCuenta: TSmallintField;
    spActualizarRefinanciacionPatentesPrecautoria: TADOStoredProc;
    GroupBox7: TGroupBox;
    lblDomicilioCliDesc: TLabel;
    lbldomicilioFactDesc: TLabel;
    lblDomicilioFact: TLabel;
    lblDomicilioCli: TLabel;
    btnRestaurarDomicilio: TButton;
    btnCambiarDomicilio: TButton;
    GroupBox8: TGroupBox;
    btnRestaurarTelefono: TButton;
    btnCambiarTelefono: TButton;
    Label13: TLabel;
    lblTelefonos: TLabel;
    spObtenerRefinanciacionDomicilio: TADOStoredProc;
    spActualizarRefinanciacionDomicilio: TADOStoredProc;
    spActualizarRefinanciacionTelefonos: TADOStoredProc;
    spObtenerRefinanciacionTelefonos: TADOStoredProc;
    spObtenerTelefonoPersonaSinFormato: TADOStoredProc;
    Panel1: TPanel;
    PageControl2: TPageControl;
    tbsDetalleComprobante: TTabSheet;
    DBListEx2: TDBListEx;
    DBListEx1: TDBListEx;
    gbDatosJPL: TGroupBox;
    lblJPL_Nro: TLabel;
    nedtJPLNro: TNumericEdit;
    lblJPL_Region: TLabel;
    vcbJPLRegion: TVariantComboBox;
    lblJPL_Comuna: TLabel;
    vcbJPLComuna: TVariantComboBox;
    cdsComprobantesRefinanciadosFechaEmision: TDateTimeField;
    cdsComprobantesRefinanciadosFechaVencimiento: TDateTimeField;
    cdsComprobantesRefinanciadosTipoComprobante: TStringField;
    cdsComprobantesRefinanciadosNumeroComprobante: TLargeintField;
    cdsComprobantesRefinanciadosDescriComprobante: TStringField;
    cdsComprobantesRefinanciadosSaldoPendienteDescri: TStringField;
    cdsComprobantesRefinanciadosTotalComprobanteDescri: TStringField;
    cdsComprobantesRefinanciadosDescriAPagar: TStringField;
    cdsComprobantesRefinanciadosSaldoPendienteComprobante: TLargeintField;
    cdsComprobantesRefinanciadosSaldoRefinanciado: TLargeintField;
    cdsComprobantesRefinanciadosSaldoRefinanciadoDescri: TStringField;
    cdsCuotasEstadoImpago: TBooleanField;
    btnHistorial: TButton;
    cdsCuotasCodigoCuota: TSmallintField;
    edtCausaRol: TEdit;
    nedtDemanda: TNumericEdit;
    lblDemanda: TLabel;
    lblDatosGastoNotarial: TLabel;
    cdsCuotasSaldo: TLargeintField;
    btnAnularCuota: TButton;
    cdsCuotasCuotaNueva: TBooleanField;
    cdsCuotasCuotaModificada: TBooleanField;
    spObtenerRefinanciacionCuotasDatosCuota: TADOStoredProc;
    spActualizarRefinanciacionCuotasEstados: TADOStoredProc;
    btnImprimirOriginal: TButton;
    btnHistorialPagos: TButton;
    cdsCuotasCodigoCuotaAntecesora: TSmallintField;
    btnSugerirImporteDemanda: TButton;
    Label12: TLabel;
    lblDeudaPendiente: TLabel;
    btnImprimirDefinitiva: TButton;
    spActualizarRefinanciacionAceptada: TADOStoredProc;
    spActualizarRefinanciacionDocumentoOriginal: TADOStoredProc;
    lblProtestada: TLabel;
    cdsCuotasNumeroCuota: TSmallintField;                                       // SS_1050_PDO_20120605
    lblEnListaAmarilla: TLabel;
    spActualizarEstadoConvenio: TADOStoredProc;
    btnDefinirPlan: TButton;
    lblNombrePlan: TLabel;
    spIngresarConvenioACobranzaInterna: TADOStoredProc;	                                                //SS_1236_MCA_20141226
    procedure vcbTipoRefinanciacionChange(Sender: TObject);
    procedure peRutButtonClick(Sender: TObject);
    procedure vcbConveniosChange(Sender: TObject);
    procedure peRutChange(Sender: TObject);
    procedure peRutKeyPress(Sender: TObject; var Key: Char);
    procedure btnCambiarRepLegCliClick(Sender: TObject);
    procedure btnCambiarRepLegCNClick(Sender: TObject);
    procedure btnAgregarComprobantesClick(Sender: TObject);
    procedure btnEliminarComprobanteClick(Sender: TObject);
    procedure DBListEx2DrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
    procedure cdsComprobantesRefinanciadosAfterScroll(DataSet: TDataSet);
    procedure btnEliminarPatentePrecautoriaClick(Sender: TObject);
    procedure btnAgregarCuotaClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure DBListEx3DrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
    procedure btnEliminarCuotaClick(Sender: TObject);
    procedure btnSugerirImportesClick(Sender: TObject);
    procedure btnEditarCuotaClick(Sender: TObject);
    procedure btnBuscarRutClick(Sender: TObject);
    procedure DBListEx3DblClick(Sender: TObject);
    procedure btnValidarClick(Sender: TObject);
    procedure btnGrabarClick(Sender: TObject);
    procedure DBListEx3DrawBackground(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var DefaultDraw: Boolean);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure btnCambiarDomicilioClick(Sender: TObject);
    procedure btnRestaurarDomicilioClick(Sender: TObject);
    procedure btnCambiarTelefonoClick(Sender: TObject);
    procedure btnRestaurarTelefonoClick(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
    procedure vcbJPLRegionChange(Sender: TObject);
    procedure btnHistorialClick(Sender: TObject);
    procedure nedtGastoNotarialChange(Sender: TObject);
    procedure dedtFechaRefiChange(Sender: TObject);
    procedure cdsCuotasAfterScroll(DataSet: TDataSet);
    procedure btnAnularCuotaClick(Sender: TObject);
    procedure btnImprimirOriginalClick(Sender: TObject);
    procedure btnHistorialPagosClick(Sender: TObject);
    procedure btnSugerirImporteDemandaClick(Sender: TObject);
    procedure DBListEx3Columns3HeaderClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnImprimirDefinitivaClick(Sender: TObject);
    procedure DBListEx1DrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
    procedure DBListEx1DrawBackground(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var DefaultDraw: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure vcbConveniosDrawItem(Control: TWinControl; Index: Integer;            //SS_1120_MVI_20130820
      Rect: TRect; State: TOwnerDrawState);
    procedure btnDefinirPlanClick(Sender: TObject);                                         //SS_1120_MVI_20130820
  private
    { Private declarations }
    FCodigoEstado,
    FCodigoConvenio,
    //FEsConvenioCN,                                                            //SS_1147_MCA_20140408
    FEsConvenioNativo,                                                            //SS_1147_MCA_20140408
    FCodigoCanalPago: Integer;
    FUltimaBusqueda: TBusquedaCliente;
    FPersona: TDatosPersonales;
    FRepresentanteLegalCli,
    FRepresentanteLegalCN: TRefinanciacionRepLegal;
    FRepresentantesLegales: TRepresentantesLegales;
    FDatosDomicilio: TDatosDomicilio;
    FDomicilioModificado: Boolean;
    FDatosTelefonos: TDatosTelefonos;
    FTelefonosModificados: Boolean;
    FCuotasCantidad: Integer;
    FCuotasTotal,
    FDeudaPagada,
    FComprobantesRefinanciadosTotal,
    FDeudaVencida,
    FTotalDeuda: Largeint;
    FFechaRefinanciacion,
    FAhora: TDate;
    FEditable,
    FCuotasEditable,
    FConsulta,
    FRefinanciacionUnificada,             // SS_1051_PDO
    FRefinanciacionMaestra: Boolean;      // SS_1051_PDO
    FMensajesInicializar: TStringList;
    FUltimoBancoEntradoCuotas: Integer;
    FUltimoNroPagareEntradoCuotas: string[20];
    FFactorImporteSujerido : Integer;   // SS_1245_CQU_20150224

    procedure MostrarDatosCliente;
    procedure MostrarDomicilioCliente;
    procedure MostrarTelefonosCliente;
    procedure LimpiarDatosCliente;
    procedure MostrarDatosConvenio;
    procedure LimpiarComprobantes;
    procedure MostrarComprobantes;
    procedure MostrarPatentesPrecautoria;
    procedure LimpiarCuotas;
    procedure MostrarCuotas;
    Procedure ActualizarTotales;
    procedure LimpiarRepresentanteLegalCN;
    procedure MostrarRepresentanteLegalCN;
    procedure LimpiarRepresentanteLegalCli;
    procedure MostrarRepresentanteLegalCli;
    function ValidarRefinanciacion(Completa, Silencioso: Boolean): Boolean;
    function ValidarRefinanciacionesUnificadas: Boolean;							//SS_1051_PDO
    procedure Grabar(ProcesoPreImpresion: Boolean);
    procedure GrabarSoloCuotas(ProcesoPreImpresion: Boolean);
    procedure ActualizarBotonesCuotas(DeshabilitarTodos: Boolean);
    function ValidarImportesEnCero: Integer;
    procedure AceptarRefinanciacion;
    procedure VerificarClienteCarpetaLegal;
    procedure VerificarRefinanciacionesEnTramiteAceptadas;
//    procedure AsignarDatosCuota(Alta: Boolean);
    function ReemplazaPorApostrofe(Cadena: String): String;
    function  VerificarConvenioListaAmarilla(CodigoConvenio : Integer) : String;    //SS_1236_MCA_20141226
    function  VerificaPersonaListaAmarilla(RutCliente : string) : Boolean;          //SS_1236_MCA_20141226

  public
    { Public declarations }
    FCodigoRefinanciacion,
    FCodigoRefinanciacionAnterior,
    FCodigoRefinanciacionUnificada: Integer;        //SS_1051_PDO
    FComponentes: TObjectList;
    function Inicializar(Refinanciacion, RefinanciacionUnificada, Tipo, CodigoCliente, CodigoConvenio: Integer; Consulta: Boolean = False): Boolean;    //SS_1051_PDO
  end;

var
  RefinanciacionGestionForm: TRefinanciacionGestionForm;

resourcestring
    rsInicializacionTitulo  = 'Mensajes Inicializaci�n.';
    rsInicializacionMensaje = 'Validaci�n: %s, Mensaje: %s';

const
    SQLArmarDomicilioCompleto 			= 'SELECT dbo.ArmarDomicilioCompleto(''%s'', %d, %d, ''%s'', ''%s'', ''%s'', ''%s'')';
	CONST_ESTADO_CONVENIO_REFINANCIADO	= 4;	//TASK_124_CFU_20170320
    
implementation

uses frmSeleccionarPlanDeRefinanciacion, UtilProc, DateUtils,
  DB_CRUDCommonProcs;

{$R *.dfm}

procedure TRefinanciacionGestionForm.btnAgregarComprobantesClick(Sender: TObject);
    resourcestring
        rsSeleccionComprobantesTitulo  = 'Selecci�n Comprobantes Impagos';
        rsSeleccionComprobantesNinguno = 'El Convenio no tiene Comprobantes Impagos para poder Refinanciar.';
begin
    try
        CambiarEstadoCursor(CURSOR_RELOJ, Self, FComponentes);

        RefinanciacionSeleccionarComprobantesForm := TRefinanciacionSeleccionarComprobantesForm.Create(Self);
        with RefinanciacionSeleccionarComprobantesForm do begin
            if Inicializar(FPersona.CodigoPersona, FCodigoConvenio, FCodigoRefinanciacion, cdsComprobantesRefinanciados) then begin
                if cdsComprobantesImpagos.RecordCount > 0 then begin
                    if ShowModal = mrOk then begin
                                                     
                        cdsComprobantesRefinanciados.EmptyDataSet;
                        if cdsDetalleComprobanteRefinanciado.Active then begin
                            cdsDetalleComprobanteRefinanciado.EmptyDataSet;
                            cdsDetalleComprobanteRefinanciado.Close;
                        end;
                        FComprobantesRefinanciadosTotal := 0;
                        
                        if FComprobantesSeleccionados > 0 then try
                            Screen.Cursor := crHourGlass;
                            Application.ProcessMessages;

                            FComprobantesRefinanciadosTotal := FComprobantesSeleccionadosTotal;

                            dsComprobantesRefinanciados.DataSet      := Nil;
                            cdsComprobantesRefinanciados.AfterScroll := Nil;

                            dsComprobantesImpagos.DataSet      := Nil;
                            cdsComprobantesImpagos.AfterScroll := Nil;
                            cdsComprobantesImpagos.First;

                            while not cdsComprobantesImpagos.Eof do begin
                                if cdsComprobantesImpagosRefinanciar.AsBoolean then begin
                                    cdsComprobantesRefinanciados.AppendRecord(
                                        [cdsComprobantesImpagosFechaEmision.AsDateTime,
                                         cdsComprobantesImpagosFechaVencimiento.AsDateTime,
                                         cdsComprobantesImpagosTipoComprobante.AsString,
                                         cdsComprobantesImpagosNumeroComprobante.AsLargeInt,
                                         cdsComprobantesImpagosDescriComprobante.AsString,
                                         cdsComprobantesImpagosSaldoPendienteDescri.AsString,
                                         cdsComprobantesImpagosTotalComprobanteDescri.AsString,
                                         cdsComprobantesImpagosDescriAPagar.AsString,
                                         cdsComprobantesImpagosSaldoPendiente.AsInteger,
                                         cdsComprobantesImpagosSaldoPendiente.AsInteger,
                                         cdsComprobantesImpagosSaldoPendienteDescri.AsString]);
                                end;
                                cdsComprobantesImpagos.Next;
                            end;
                        finally
                            dsComprobantesRefinanciados.DataSet      := cdsComprobantesRefinanciados;
                            cdsComprobantesRefinanciados.AfterScroll := cdsComprobantesRefinanciadosAfterScroll;
                            cdsComprobantesRefinanciados.First;

                            btnEliminarComprobante.Enabled := (cdsComprobantesRefinanciados.RecordCount > 0);
                            ActualizarTotales;
                            ActualizarBotonesCuotas(False);

                            Screen.Cursor := crDefault;
                            Application.ProcessMessages;
                        end;
                    end;
                end else ShowMsgBoxCN(rsSeleccionComprobantesTitulo, rsSeleccionComprobantesNinguno, MB_ICONWARNING, Self);
            end;
        end;
    finally
        if Assigned(RefinanciacionSeleccionarComprobantesForm) then FreeAndNil(RefinanciacionSeleccionarComprobantesForm);

        CambiarEstadoCursor(CURSOR_DEFECTO, Self, FComponentes);
    end;
end;

procedure TRefinanciacionGestionForm.btnCambiarRepLegCliClick(Sender: TObject);

begin
    RefinanciacionSeleccionarRepresentanteCliForm := TRefinanciacionSeleccionarRepresentanteCliForm.Create(Self);

    with RefinanciacionSeleccionarRepresentanteCliForm do try
        if Inicializar(FRepresentantesLegales, FRepresentanteLegalCli) then begin
            if ShowModal = mrOk then begin
                FRepresentanteLegalCli.NumeroDocumento := peRut.Text;
                FRepresentanteLegalCli.Nombre          := edtNombre.Text;
                FRepresentanteLegalCli.Apellido        := edtApellido.Text;
                FRepresentanteLegalCli.ApellidoMaterno := edtApellidoMaterno.Text;

                lblRutRepLegCli.Caption := peRut.Text;
                lblNombreRepLegCli.Caption :=
                    ArmarNombrePersona(
                        PERSONERIA_FISICA,
                        edtNombre.Text,
                        edtApellido.Text,
                        edtApellidoMaterno.Text)
            end;
        end;
    finally
        if Assigned(RefinanciacionSeleccionarRepresentanteCliForm) then FreeAndNil(RefinanciacionSeleccionarRepresentanteCliForm);
    end;
end;

procedure TRefinanciacionGestionForm.btnCambiarRepLegCNClick(Sender: TObject);
begin
    RefinanciacionSeleccionarRepLegalCNForm := TRefinanciacionSeleccionarRepLegalCNForm.Create(Self);

    with RefinanciacionSeleccionarRepLegalCNForm do try
        if Inicializar(vcbTipoRefinanciacion.Value, vcbTipoRefinanciacion.Text, FRepresentanteLegalCN) then begin
            if ShowModal = mrOk then begin
                FRepresentanteLegalCN.NumeroDocumento := peRut.Text;
                FRepresentanteLegalCN.Nombre          := edtNombre.Text;
                FRepresentanteLegalCN.Apellido        := edtApellido.Text;
                FRepresentanteLegalCN.ApellidoMaterno := edtApellidoMaterno.Text;

                MostrarRepresentanteLegalCN;
            end;
        end;
    finally
        if Assigned(RefinanciacionSeleccionarRepLegalCNForm) then FreeAndNil(RefinanciacionSeleccionarRepLegalCNForm);
    end;
end;

procedure TRefinanciacionGestionForm.btnCambiarTelefonoClick(Sender: TObject);
begin
    try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        RefinanciacionEditarTelefonosForm := TRefinanciacionEditarTelefonosForm.Create(Self);
        with FDatosTelefonos do begin
            if RefinanciacionEditarTelefonosForm.Inicializar(CodigoArea1, Telefono1, CodigoArea2, Telefono2) then begin
                if RefinanciacionEditarTelefonosForm.ShowModal = mrOk then begin
                    FDatosTelefonos := RefinanciacionEditarTelefonosForm.FTelefonosEditados;
                    FTelefonosModificados := True;

                    MostrarTelefonosCliente;
                end;
            end;
        end;
    finally
        if Assigned(RefinanciacionEditarTelefonosForm) then FreeAndNil(RefinanciacionEditarTelefonosForm);
        Screen.Cursor := crDefault;
    end;
end;

procedure TRefinanciacionGestionForm.btnCancelarClick(Sender: TObject);
begin
    ModalResult := mrCancel;
end;

//TASK_006_AUN_20170406-CU.COBO.COB.302
procedure TRefinanciacionGestionForm.btnDefinirPlanClick(Sender: TObject);
resourcestring
    MSG_NO_HAY_PLANES_DE_REF_CON_ESTE_CRITERIO = 'No se encontraron planes de refinanciaci�n para este cliente y su deuda';
    MSG_ERROR_OBTENIENDO_PLANES_REF = 'Ha ocurrido un error obteniendo los planes de refinanciaci�n';
    MSG_CUOTAS_CREADAS_REVISAR_FECHAS = 'Las cuotas fueron creadas con �xito. Por favor revise las fechas de las mismas antes de grabar la refinanciaci�n.';
var
    f: TSeleccionarPlanDeRefinanciacionForm;
    CodigoTipoCliente, i: Integer;
    MontoDeuda: Int64;
    TodoOkMostrarMensaje: Boolean;
begin
    try
        Screen.Cursor := crHourGlass;
        TodoOkMostrarMensaje := False;
        f := TSeleccionarPlanDeRefinanciacionForm.Create(Self);
        try
            if FPersona.Personeria = PERSONERIA_FISICA then
                CodigoTipoCliente := 1
            else
                if FPersona.Personeria = PERSONERIA_JURIDICA then
                    CodigoTipoCliente := 2
                else
                    CodigoTipoCliente := 3;
            //
            MontoDeuda := FComprobantesRefinanciadosTotal - FDeudaPagada;
            if f.Inicializar(vcbConvenios.Value, CodigoTipoCliente, MontoDeuda, cdsCuotas.RecordCount > 0) then begin
                if f.ShowModal = mrOk then begin
                    FCuotasTotal := 0;
                    FCuotasCantidad := f.CantidadDeCuotas;
                    cdsCuotas.First;
                    while not cdsCuotas.Eof do
                        cdsCuotas.Delete;
                    //
                    for i := 0 to FCuotasCantidad -1 do begin
                        cdsCuotas.AppendRecord(
                        [0,                                                 // CodigoCuota
                         REFINANCIACION_ESTADO_CUOTA_PENDIENTE,             // CodigoEstadoPago
                         REFINANCIACION_ESTADO_CUOTA_PENDIENTE_DESCRIPCION, // DescripcionEstadoPago
                         CobranzasResources.FORMA_PAGO_EFECTIVO,                     // CodigoFormaPago
                         'Efectivo',        //DescripcionFormaPago
                         '',                //TitularNombre
                         '',                //TitularTipoDocumento
                         '',                //TitularNumeroDocumento
                         0,                 //CodigoBanco
                         '',                //DescripcionBanco
                         0,                 //DocumentoNumero
                         f.VencimientoCuotas[i], //FechaCuota
                         0,                 //CodigoTarjeta
                         '',                //Descripci�n Tarjeta
                         0,                 //TarjetaCuotas
                         f.Cuotas[i],       //Importe
                         f.Cuotas[i],       //Saldo
                         'EF',              //CodigoEntradaUsuario
                         True,              //EstadoImpago
                         0,                 //CodigoCuotaAntecesora
                         true,              //CuotaNueva
                         false]);           //CuotaModificada
                    //if FEntradaDatos.Fecha < FAhora then begin
                    //    FDeudaVencida := FDeudaVencida + FEntradaDatos.Importe;
                    //end;
                    end;//while
                    lblNombrePlan.Caption := f.NombrePlan;
                    FCuotasTotal := MontoDeuda;
                    ActualizarTotales;
                    ActualizarBotonesCuotas(False);
                    TodoOkMostrarMensaje := True;
                end;
            end else begin
                UtilProc.MsgBox(MSG_NO_HAY_PLANES_DE_REF_CON_ESTE_CRITERIO, self.Caption, MB_ICONWARNING);
            end;
        finally
            F.Close;
            F.Release;
            Screen.Cursor := crDefault;
            if TodoOkMostrarMensaje then begin
                UtilProc.MsgBox(MSG_CUOTAS_CREADAS_REVISAR_FECHAS, self.Caption, MB_ICONINFORMATION);
            end;
        end;
    except
        on e: Exception do begin
            MsgBoxErr(MSG_ERROR_OBTENIENDO_PLANES_REF, e.message, Self.Caption, MB_ICONSTOP);
        end;
    end;
end;

procedure TRefinanciacionGestionForm.btnEditarCuotaClick(Sender: TObject);
begin
    try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        RefinanciacionEntrarEditarCuotasForm := TRefinanciacionEntrarEditarCuotasForm.Create(Self);

        with RefinanciacionEntrarEditarCuotasForm do begin
            if Inicializar(TIPO_ENTRADA_DATOS_CUOTA, FCodigoCanalPago, FPersona, False, FFechaRefinanciacion, FEditable, cdsCuotas) then begin
                if RefinanciacionEntrarEditarCuotasForm.ShowModal = mrOk then begin
                    if (cdsCuotasFechaCuota.AsDateTime < FAhora) and (cdsCuotasEstadoImpago.AsBoolean) then begin
                        FDeudaVencida := FDeudaVencida - cdsCuotasSaldo.AsInteger;
                    end;

                    FCuotasTotal := (FCuotasTotal - cdsCuotasSaldo.AsInteger) + FEntradaDatos.DatosCuota.Saldo;

                    cdsCuotas.Edit;

                    cdsCuotasCodigoFormaPago.AsInteger       := FEntradaDatos.CodigoFormaPago;
                    cdsCuotasDescripcionFormaPago.AsString   := FEntradaDatos.DescripcionFormaPago;
                    cdsCuotasCodigoEntradaUsuario.AsString   := FEntradaDatos.CodigoEntradaUsuario;

                    cdsCuotasTitularNombre.AsString          := FEntradaDatos.TitularNombre;
                    cdsCuotasTitularTipoDocumento.AsString   := FEntradaDatos.TitularTipoDocumento;
                    cdsCuotasTitularNumeroDocumento.AsString := FEntradaDatos.TitularNumeroDocumento;
                    cdsCuotasCodigoBanco.AsInteger           := FEntradaDatos.CodigoBanco;
                    cdsCuotasDescripcionBanco.AsString       := FEntradaDatos.DescripcionBanco;
                    cdsCuotasDocumentoNumero.AsString        := FEntradaDatos.DocumentoNumero;
                    cdsCuotasFechaCuota.AsDateTime           := FEntradaDatos.Fecha;
                    cdsCuotasCodigoTarjeta.AsInteger         := FEntradaDatos.CodigoTarjeta;
                    cdsCuotasDescripcionTarjeta.AsString     := FEntradaDatos.DescripcionTarjeta;
                    cdsCuotasTarjetaCuotas.AsInteger         := FEntradaDatos.TarjetaCuotas;
                    cdsCuotasImporte.AsInteger               := FEntradaDatos.Importe;

                    cdsCuotasCodigoCuotaAntecesora.AsInteger := FEntradaDatos.DatosCuota.CodigoCuotaAntecesora;
                    cdsCuotasSaldo.AsInteger                 := FEntradaDatos.DatosCuota.Saldo;
                    cdsCuotasCuotaModificada.AsBoolean       := True;
                    cdsCuotas.Post;
                    ActualizarBotonesCuotas(False);

                    if (cdsCuotasFechaCuota.AsDateTime < FAhora) and (cdsCuotasEstadoImpago.AsBoolean) then begin
                        FDeudaVencida := FDeudaVencida + cdsCuotasSaldo.AsInteger;
                    end;
                    ActualizarTotales;
                end;
            end;
        end;
    finally
        if Assigned(RefinanciacionEntrarEditarCuotasForm) then FreeAndNil(RefinanciacionEntrarEditarCuotasForm);

        Screen.Cursor := crDefault;
    end;

end;

procedure TRefinanciacionGestionForm.btnEliminarComprobanteClick(Sender: TObject);
    resourcestring
        rsConfirmacionTitulo  = 'Confirmaci�n Eliminaci�n.';
        rsConfirmacionMensaje = '� Esta seguro de Eliminar el Comprobante a Refinanciar Seleccionado ?';
    var
        cdsComprobantes: TClientDataSet;
begin
    if cdsComprobantesRefinanciados.RecordCount > 0 then begin
        if ShowMsgBoxCN(rsConfirmacionTitulo, rsConfirmacionMensaje, MB_ICONQUESTION, Self) = mrOk then try
            Screen.Cursor := crHourGlass;
            Application.ProcessMessages;

            cdsComprobantesRefinanciados.Delete;
            FComprobantesRefinanciadosTotal := 0;

            if cdsComprobantesRefinanciados.RecordCount > 0 then try
                cdsComprobantes      := TClientDataSet.Create(Nil);
                cdsComprobantes.Data := cdsComprobantesRefinanciados.Data;
                with cdsComprobantes do begin
                    First;
                    while not Eof do begin
                        FComprobantesRefinanciadosTotal :=
                            FComprobantesRefinanciadosTotal + FieldByName('SaldoRefinanciado').AsInteger;
                        Next;
                    end;
                end;
            finally
                if Assigned(cdsComprobantes) then FreeAndNil(cdsComprobantes);
            end;

            ActualizarTotales;
            (Sender As TButton).Enabled := (cdsComprobantesRefinanciados.RecordCount > 0);
        finally
            Screen.Cursor := crDefault;
        end;
    end;
end;

procedure TRefinanciacionGestionForm.btnEliminarCuotaClick(Sender: TObject);
resourcestring
    rsConfirmacionTitulo  = 'Confirmaci�n Eliminaci�n.';
    rsConfirmacionMensaje = '�Esta seguro de Eliminar la Cuota Seleccionada?';
begin
    if cdsCuotas.RecordCount > 0 then begin
        if ShowMsgBoxCN(rsConfirmacionTitulo, rsConfirmacionMensaje, MB_ICONQUESTION, Self) = mrOk then try
            Screen.Cursor := crHourGlass;
            Application.ProcessMessages;

            if cdsCuotasFechaCuota.AsDateTime < FAhora then begin
                FDeudaVencida := FDeudaVencida - cdsCuotasSaldo.AsLargeInt;
            end;

            FCuotasTotal := FCuotasTotal - cdsCuotasImporte.AsInteger;
            cdsCuotas.Delete;
            {
            FCuotasTotal    := 0;
            FCuotasCantidad := cdsCuotas.RecordCount;

            if cdsCuotas.RecordCount > 0 then try
                cdsCuotasTemp      := TClientDataSet.Create(Nil);
                cdsCuotasTemp.Data := cdsCuotas.Data;
                with cdsCuotasTemp do begin
                    First;
                    while not Eof do begin
                        if FieldByName('CodigoEstadoCuota').AsInteger <> EstadoAnulado then begin
                            FCuotasTotal :=
                                FCuotasTotal + FieldByName('Importe').AsInteger;
                        end;

                        Next;
                    end;
                end;
            finally
                if Assigned(cdsCuotasTemp) then FreeAndNil(cdsCuotasTemp);
            end;
            }
            ActualizarTotales;
            {
            (Sender As TButton).Enabled := (cdsCuotas.RecordCount > 0);
            btnEditarCuota.Enabled      := (Sender As TButton).Enabled;
            btnSugerirImportes.Enabled  := (Sender As TButton).Enabled;
            }
        finally
            Screen.Cursor := crDefault;
        end;
    end;
end;

procedure TRefinanciacionGestionForm.btnEliminarPatentePrecautoriaClick(Sender: TObject);
    resourcestring
        rsConfirmacionTitulo  = 'Confirmaci�n Eliminaci�n.';
        rsConfirmacionMensaje = '� Esta seguro de Eliminar la Patente en Precautoria Seleccionada ?';
begin
    if cdsPatentesPrecautoria.RecordCount > 0 then begin
        if ShowMsgBoxCN(rsConfirmacionTitulo, rsConfirmacionMensaje, MB_ICONQUESTION, Self) = mrOk then begin
            cdsPatentesPrecautoria.Delete;
            (Sender As TButton).Enabled := (cdsPatentesPrecautoria.RecordCount > 0);
        end;
    end;
end;

procedure TRefinanciacionGestionForm.btnSugerirImportesClick(Sender: TObject);
    resourcestring
        rsValidacionTitulo = 'Validaci�n Comprobantes Refinanciados';
        rsValidacionMensaje = 'No tiene Ning�n Comprobante seleccionado para Refinanciar. No se puede efectuar los c�lculos y sugerir las Cuotas.';
        RS_VALIDACION_IMPORTES_TITULO  = 'Validaci�n Importes';
        RS_VALIDACION_IMPORTES_CUOTAS_MENSAJE = 'Todas las Cuotas tienen importe distinto de CERO. No es posible Sugerir Importes.';
        RS_VALIDACION_IMPORTES_TOTALES_MENSAJE = 'El Importe de las Cuotas es Superior al Total Refinanciado. No es posible Sugerir Importes.';

    Var
        ImporteCuotaSugerido,
        ImporteCuotasEntrado,
        CuotasEntradasSinImporte: Integer;
        ImportePie: Boolean;
        Puntero: TBookmark;
begin
    if FComprobantesRefinanciadosTotal = 0 then
        ShowMsgBoxCN(rsValidacionTitulo, rsValidacionMensaje, MB_ICONEXCLAMATION, Self)
    else if FCuotasTotal > FComprobantesRefinanciadosTotal then begin
        ShowMsgBoxCN(RS_VALIDACION_IMPORTES_TITULO, RS_VALIDACION_IMPORTES_TOTALES_MENSAJE, MB_ICONINFORMATION, Self);
    end
    else try

        CambiarEstadoCursor(CURSOR_RELOJ);

        ImporteCuotasEntrado     := 0;
        CuotasEntradasSinImporte := 0;

        cdsCuotas.DisableControls;
        cdsCuotas.AfterScroll := Nil;
        dsCuotas.DataSet := Nil;
        Puntero := cdsCuotas.GetBookmark;

        cdsCuotas.First;

        while not cdsCuotas.Eof do begin
            if cdsCuotasImporte.AsInteger <> 0 then begin
                ImporteCuotasEntrado := ImporteCuotasEntrado + cdsCuotasImporte.AsInteger;
            end
            else Inc(CuotasEntradasSinImporte);

            cdsCuotas.Next;
        end;

        if CuotasEntradasSinImporte <> 0 then begin

            ImporteCuotaSugerido := (FComprobantesRefinanciadosTotal - ImporteCuotasEntrado) div CuotasEntradasSinImporte;
            ImportePie           := True;
            cdsCuotas.First;

            while not cdsCuotas.Eof do begin
                if cdsCuotasImporte.AsInteger = 0 then begin
                    cdscuotas.Edit;
                    if ImportePie then begin
                        if (ImporteCuotaSugerido * CuotasEntradasSinImporte) = (FComprobantesRefinanciadosTotal - ImporteCuotasEntrado) then begin
                            cdsCuotasImporte.AsInteger := ImporteCuotaSugerido;
                        end
                        else begin
                            cdsCuotasImporte.AsInteger := ImporteCuotaSugerido + ((FComprobantesRefinanciadosTotal - ImporteCuotasEntrado) mod CuotasEntradasSinImporte);
                        end;

                        ImportePie := False;
                    end
                    else begin
                        cdsCuotasImporte.AsInteger := ImporteCuotaSugerido;
                    end;

                    cdsCuotasSaldo.AsInteger := cdsCuotasImporte.AsInteger;
                    cdsCuotas.Post;
                    
                    FCuotasTotal := FCuotasTotal + cdsCuotasImporte.AsInteger;
                    
                    if (cdsCuotasFechaCuota.AsDateTime < FAhora) and (cdsCuotasEstadoImpago.AsBoolean) then begin
                        FDeudaVencida := FDeudaVencida + cdsCuotasSaldo.AsInteger;
                    end;
                end;

                cdsCuotas.Next;
            end;

            ActualizarTotales;
        end;
    finally
        cdsCuotas.EnableControls;
        cdsCuotas.AfterScroll := cdsCuotasAfterScroll;
        dsCuotas.DataSet      := cdsCuotas;

        if Assigned(Puntero) then begin
            if cdsCuotas.BookmarkValid(Puntero) then begin
                cdsCuotas.GotoBookmark(Puntero);
            end;
            cdsCuotas.FreeBookmark(Puntero);
        end;

        CambiarEstadoCursor(CURSOR_DEFECTO);

        if CuotasEntradasSinImporte = 0 then  begin
            ShowMsgBoxCN(RS_VALIDACION_IMPORTES_TITULO, RS_VALIDACION_IMPORTES_CUOTAS_MENSAJE, MB_ICONINFORMATION, Self);
        end;
    end;
end;

{
//TASK_006_AUN_20170406-CU.COBO.COB.302: se elimina el tab Otros datos
procedure TRefinanciacionGestionForm.btnAgregarPatentePrecautoriaClick(Sender: TObject);
begin
    try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        RefinanciacionSeleccionarPatentesPrecautoriaForm := TRefinanciacionSeleccionarPatentesPrecautoriaForm.Create(Self);

        with RefinanciacionSeleccionarPatentesPrecautoriaForm do begin
            if Inicializar(FPersona.CodigoPersona, cdsPatentesPrecautoria) then begin
                if ShowModal = mrOk then begin
                    cdsPatentesPrecautoria.EmptyDataSet;
                    btnEliminarPatentePrecautoria.Enabled := False;

                    if FTotalPatentesSeleccionadas > 0 then try
                        Screen.Cursor := crHourGlass;
                        Application.ProcessMessages;

                        dsPatentes.DataSet            := Nil;
                        dsPatentesPrecautoria.DataSet := nil;

                        with cdsPatentes do begin
                            First;
                            while not Eof do begin
                                if cdsPatentesSeleccionar.AsBoolean then begin
                                    cdsPatentesPrecautoria.AppendRecord(
                                        [cdsPatentesPatente.AsString,
                                         cdsPatentesMarcaModeloColor.AsString,
                                         cdsPatentesConvenio.AsString,
                                         cdsPatentesCodigoEstadoCuenta.AsInteger]);
                                end;

                                Next;
                            end;
                        end;
                    finally
                        dsPatentesPrecautoria.DataSet := cdsPatentesPrecautoria;
                        cdsPatentesPrecautoria.First;

                        btnEliminarPatentePrecautoria.Enabled := (cdsPatentesPrecautoria.RecordCount > 0);

                        Screen.Cursor := crDefault;
                        Application.ProcessMessages;
                    end;
                end;
            end;
        end;
    finally
        if Assigned(RefinanciacionSeleccionarPatentesPrecautoriaForm) then FreeAndNil(RefinanciacionSeleccionarPatentesPrecautoriaForm);
        Screen.Cursor := crDefault;
    end;
end;
}

procedure TRefinanciacionGestionForm.btnAnularCuotaClick(Sender: TObject);
resourcestring
    rs_Titulo  = 'Anular Cuota';
    rs_Mensaje = '� Est� seguro de Anular la cuota seleccionada ?';
begin
    if ShowMsgBoxCN(rs_Titulo, rs_Mensaje, MB_ICONQUESTION, Self) = mrOk then try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        if cdsCuotasFechaCuota.AsDateTime < FAhora then begin
            FDeudaVencida := FDeudaVencida - cdsCuotasSaldo.AsLargeInt;
        end;

        cdsCuotas.Edit;
        if cdsCuotasCodigoEstadoCuota.AsInteger = REFINANCIACION_ESTADO_CUOTA_PROTESTADO then begin
            cdsCuotasCodigoEstadoCuota.AsInteger     := REFINANCIACION_ESTADO_CUOTA_ANULADA_POR_PROTESTO;
            cdsCuotasDescripcionEstadoCuota.AsString := REFINANCIACION_ESTADO_CUOTA_ANULADA_POR_PROTESTO_DESCRIPCION;
        end
        else begin
            cdsCuotasCodigoEstadoCuota.AsInteger     := REFINANCIACION_ESTADO_CUOTA_ANULADA;
            cdsCuotasDescripcionEstadoCuota.AsString := REFINANCIACION_ESTADO_CUOTA_ANULADA_DESCRIPCION;
        end;
        cdsCuotasSaldo.AsInteger                 := 0;
        cdsCuotasCuotaModificada.AsBoolean       := True;
        cdsCuotas.Post;

        FCuotasTotal := FCuotasTotal - cdsCuotasImporte.AsInteger;

        ActualizarTotales;
        ActualizarBotonesCuotas(False);
    finally
        Screen.Cursor := crDefault;
    end;
end;

procedure TRefinanciacionGestionForm.cdsComprobantesRefinanciadosAfterScroll(DataSet: TDataSet);
begin
    try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        if cdsDetalleComprobanteRefinanciado.Active then begin
            cdsDetalleComprobanteRefinanciado.EmptyDataSet;
            cdsDetalleComprobanteRefinanciado.Close;
        end;

        if cdsComprobantesRefinanciados.RecordCount > 0 then begin
            with spDetalleComprobante do begin
                Parameters.ParamByName('@TipoComprobante').Value   := cdsComprobantesRefinanciadosTipoComprobante.AsString;
                Parameters.ParamByName('@NumeroComprobante').Value := cdsComprobantesRefinanciadosNumeroComprobante.AsInteger;
            end;

            cdsDetalleComprobanteRefinanciado.Open;
            cdsDetalleComprobanteRefinanciado.AppendRecord(['TOTAL A PAGAR', 0, cdsComprobantesRefinanciadosDescriAPagar.AsString]);
            cdsDetalleComprobanteRefinanciado.First;
        end;
    finally
        Screen.Cursor := crDefault;
        Application.ProcessMessages;
    end;
end;

procedure TRefinanciacionGestionForm.cdsCuotasAfterScroll(DataSet: TDataSet);
begin
    ActualizarBotonesCuotas(False);
end;

procedure TRefinanciacionGestionForm.DBListEx1DrawBackground(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var DefaultDraw: Boolean);
begin
    if FCodigoEstado in [REFINANCIACION_ESTADO_EN_TRAMITE, REFINANCIACION_ESTADO_ACEPTADA] then begin
        if cdsComprobantesRefinanciados.Active then begin
            if cdsComprobantesRefinanciados.RecordCount > 0 then begin
                if cdsComprobantesRefinanciadosSaldoPendienteComprobante.AsLargeInt <> cdsComprobantesRefinanciadosSaldoRefinanciado.AsLargeInt then begin
                    if (odSelected in State) and (odFocused in State) then Sender.Canvas.Brush.Color := clMaroon;
                end;
            end;
        end;
    end;
end;

procedure TRefinanciacionGestionForm.DBListEx1DrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
    if FCodigoEstado in [REFINANCIACION_ESTADO_EN_TRAMITE, REFINANCIACION_ESTADO_ACEPTADA] then begin
        if cdsComprobantesRefinanciados.Active then begin
            if cdsComprobantesRefinanciados.RecordCount > 0 then begin
                if cdsComprobantesRefinanciadosSaldoPendienteComprobante.AsLargeInt <> cdsComprobantesRefinanciadosSaldoRefinanciado.AsLargeInt then begin
                    if (Not (odSelected in State)) or ((odSelected in State) and not (odFocused in State)) then Sender.Canvas.Font.Color := clRed;

                    if Column.FieldName = 'SaldoPendienteDescri' then begin
                        Sender.Canvas.Font.Style := Sender.Canvas.Font.Style + [fsBold];
                    end;
                    if Column.FieldName = 'SaldoRefinanciadoDescri' then begin
                        Sender.Canvas.Font.Style := Sender.Canvas.Font.Style + [fsBold];
                    end;
                end;
            end;
        end;
    end;
end;

procedure TRefinanciacionGestionForm.DBListEx2DrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
    if Pos('TOTAL', UpperCase(Text)) <> 0 then Sender.Canvas.Font.Style := [fsBold];
end;

procedure TRefinanciacionGestionForm.DBListEx3Columns3HeaderClick(Sender: TObject);
begin
    DBListExColumnHeaderClickGenerico(Sender);
end;

procedure TRefinanciacionGestionForm.DBListEx3DblClick(Sender: TObject);
begin
    if btnEditarCuota.Enabled then btnEditarCuotaClick(Nil);
end;

procedure TRefinanciacionGestionForm.DBListEx3DrawBackground(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var DefaultDraw: Boolean);
begin
    with Sender do begin
        if (odSelected in State) then begin
            if ((cdsCuotasFechaCuota.AsDateTime < FAhora) and cdsCuotasEstadoImpago.AsBoolean) or
                ((cdsCuotasCodigoEstadoCuota.AsInteger in [REFINANCIACION_ESTADO_CUOTA_ANULADA, REFINANCIACION_ESTADO_CUOTA_ANULADA_POR_MORA]) and (not cdsCuotasCuotaModificada.AsBoolean)) then begin
                Canvas.Brush.Color := clMaroon;
            end
            else begin
                if FCuotasEditable and (cdsCuotasCuotaModificada.AsBoolean or cdsCuotasCuotaNueva.AsBoolean) then begin
                    Canvas.Brush.Color := clGreen;
                end;
            end;
        end;
    end;
end;

procedure TRefinanciacionGestionForm.DBListEx3DrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
    with Sender do begin
        if ((cdsCuotasFechaCuota.AsDateTime < FAhora) and cdsCuotasEstadoImpago.AsBoolean) or
            (cdsCuotasCodigoEstadoCuota.AsInteger in [REFINANCIACION_ESTADO_CUOTA_ANULADA, REFINANCIACION_ESTADO_CUOTA_ANULADA_POR_MORA]) or
            (cdsCuotasCuotaModificada.AsBoolean) or
            (cdsCuotasCuotaNueva.AsBoolean) then begin

            if ((cdsCuotasFechaCuota.AsDateTime < FAhora) and cdsCuotasEstadoImpago.AsBoolean) or
                ((cdsCuotasCodigoEstadoCuota.AsInteger in [REFINANCIACION_ESTADO_CUOTA_ANULADA, REFINANCIACION_ESTADO_CUOTA_ANULADA_POR_MORA]) and (not cdsCuotasCuotaModificada.AsBoolean)) then begin
                if Not (odSelected in State) then Canvas.Font.Color := clRed;
            end
            else begin
                if FCuotasEditable and (cdsCuotasCuotaModificada.AsBoolean or cdsCuotasCuotaNueva.AsBoolean) then begin
                    if (odSelected in State) then
                        Canvas.Font.Color := $0000E100
                    else Canvas.Font.Color := $0000A800;
                end
            end;

            if Column.FieldName = 'DescripcionFormaPago' then begin
                if FCuotasEditable then begin
                    if cdsCuotasCuotaModificada.AsBoolean or cdsCuotasCuotaNueva.AsBoolean then begin
                        Canvas.Font.Style := Canvas.Font.Style + [fsBold];
                    end;

                    if cdsCuotasCuotaNueva.AsBoolean then begin
                        Text := Trim(Text) + ' *';
                    end;
                end;
            end;

            if Column.FieldName = 'DescripcionEstadoCuota' then begin
                if cdsCuotasCodigoEstadoCuota.AsInteger in [REFINANCIACION_ESTADO_CUOTA_ANULADA, REFINANCIACION_ESTADO_CUOTA_ANULADA_POR_MORA] then
                    Canvas.Font.Style := Canvas.Font.Style + [fsBold];
            end;

            if Column.FieldName = 'CodigoCuotaAntecesora' then begin
                if cdsCuotasCodigoCuotaAntecesora.AsInteger = 0 then Text := EmptyStr;
            end;
        end;
    end;

    if (Column.FieldName = 'Importe') or (Column.FieldName = 'Saldo') then begin
        Text := FormatFloat(FORMATO_IMPORTE, StrToFloat(Text)) + ' ';
    end;
end;

procedure TRefinanciacionGestionForm.dedtFechaRefiChange(Sender: TObject);
begin
    FFechaRefinanciacion := dedtFechaRefi.Date;
end;

procedure TRefinanciacionGestionForm.FormActivate(Sender: TObject);
begin
    if Assigned(FMensajesInicializar) then try
        if FMensajesInicializar.Count > 0 then
            ShowMsgBoxCN(rsInicializacionTitulo, FMensajesInicializar.Text, MB_ICONWARNING, Self);
    finally
        FreeAndNil(FMensajesInicializar);
    end;
end;

procedure TRefinanciacionGestionForm.FormCreate(Sender: TObject);
begin
    if (Screen.WorkAreaWidth < Self.Width) or (Screen.WorkAreaHeight < Self.Height) then begin
        Self.Top    := Application.MainForm.Top;
        Self.Left   := Application.MainForm.Left;
        Self.Height := Screen.WorkAreaHeight;
        Self.Width  := Screen.WorkAreaWidth;
        
        Self.WindowState := wsMaximized;
    end;
end;

procedure TRefinanciacionGestionForm.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
    case Key of
        VK_ESCAPE: ModalResult := mrCancel;
    end;
end;

procedure TRefinanciacionGestionForm.FormShow(Sender: TObject);
begin
    TPanelMensajesForm.OcultaPanel;
end;

function TRefinanciacionGestionForm.Inicializar(Refinanciacion, RefinanciacionUnificada, Tipo, CodigoCliente, CodigoConvenio: Integer; Consulta: Boolean = False): Boolean;     //SS_1051_PDO
    resourcestring
        CAPTION_CONSULTA =   ' - Consulta  [ S�lo Lectura ]';
        CAPTION_UNIFICADA = ' - Unificada [Ref. Maestra: %d]';		//SS_1051_PDO
begin
    try
        try
            //TASK_006_AUN_20170406-CU.COBO.COB.302: inicio
            lblNombrePlan.Caption := '';
            PageControl1.ActivePage := tbsComprobantes;
            //TASK_006_AUN_20170406-CU.COBO.COB.302: fin

            CambiarEstadoCursor(CURSOR_RELOJ);

            CargarConstantesCobranzasRefinanciacion;
            CargarConstantesCobranzasCanalesFormasPago;

            TPanelMensajesForm.MuestraMensaje('inicializando Formulario Gesti�n Refinanciaciones ...');

            FMensajesInicializar          := TStringList.Create;
            FDomicilioModificado          := False;
            FTelefonosModificados         := False;
            FUltimoBancoEntradoCuotas     := 0;
            FUltimoNroPagareEntradoCuotas := EmptyStr;

            FAhora := Trunc(NowBaseSmall(DMConnections.BaseCAC));

            Result := False;

            CargarCanalesDePago(DMConnections.BaseCAC, vcbTipoRefinanciacion, 0, 1);

            cdsComprobantesRefinanciados.Open;
            cdsCuotas.Open;
            cdsPatentesPrecautoria.Open;

            LimpiarDatosCliente;
            LimpiarRepresentanteLegalCN;
            //TASK_006_AUN_20170406-CU.COBO.COB.302: se elimina el tab Otros datos
            //LimpiarPatentesPrecautoria;
            LimpiarCuotas;
            LimpiarComprobantes;

            FCodigoRefinanciacion          := Refinanciacion;
            FCodigoRefinanciacionAnterior  := Refinanciacion;
            FCodigoRefinanciacionUnificada := RefinanciacionUnificada;          																	//SS_1051_PDO
            FRefinanciacionUnificada       := (RefinanciacionUnificada <> 0);   																	//SS_1051_PDO
            FRefinanciacionMaestra         := (FCodigoRefinanciacionUnificada = 0) or (FCodigoRefinanciacionUnificada = FCodigoRefinanciacion);     //SS_1051_PDO
            Fconsulta                      := Consulta;

            if FConsulta then begin
                Caption := Caption + CAPTION_CONSULTA;
            end;

            if FCodigoRefinanciacionUnificada <> 0 then begin										//SS_1051_PDO
                Caption := Caption + Format(CAPTION_UNIFICADA, [FCodigoRefinanciacionUnificada]);	//SS_1051_PDO
            end;																					//SS_1051_PDO

            FTotalDeuda := 0;

            case FCodigoRefinanciacion of
                0: Begin // Refinanciaci�n Nueva
                    TPanelMensajesForm.MuestraMensaje('inicializando Refinanciaci�n Nueva ...');
                    FPersona          := ObtenerDatosPersonales(DMConnections.BaseCAC,'','',CodigoCliente);
                    FCodigoConvenio   := CodigoConvenio;
                    //FEsConvenioCN     := 0;
                    FEsConvenioNativo := 0;
                    FCodigoEstado     := REFINANCIACION_ESTADO_EN_TRAMITE;
                    lblEstado.Caption := QueryGetValue(DMConnections.BaseCAC,'SELECT Descripcion FROM RefinanciacionEstados WHERE CodigoEstadoRefinanciacion = 1');
                    FEditable         := True;
                    FCuotasEditable   := False;

                    lblNumRefinanciacion.Caption := '<Nueva>';
                    dedtFechaRefi.Date   := Trunc(NowBase(DMConnections.BaseCAC));
                    FFechaRefinanciacion := dedtFechaRefi.Date;

					//BEGIN : SS_1051_PDO--------------------------------------------------------------------------------------
                    if FCodigoRefinanciacionUnificada = 0 then begin
                        if Tipo <> 0 then
                            vcbTipoRefinanciacion.ItemIndex := vcbTipoRefinanciacion.Items.IndexOfValue(Tipo)
                        else vcbTipoRefinanciacion.ItemIndex := 0;
                        vcbTipoRefinanciacionChange(Nil);

                        if FPersona.CodigoPersona <> -1 then begin
                            EstaClienteConMensajeEspecial(DMConnections.BaseCAC, PADL(FPersona.NumeroDocumento, 9, '0'));    //SS_1408_MCA_20151027
                            MostrarDatosCliente;
                            CargarConveniosRUT(DMConnections.BaseCAC, vcbConvenios, FPersona.TipoDocumento, FPersona.NumeroDocumento, False, FCodigoConvenio, False, True);   //SS_1120_MVI_20130820
                            if FCodigoConvenio = 0 then begin
                                if vcbConvenios.Items.Count > 0 then FCodigoConvenio := vcbConvenios.Value;
                            end;
                        end;

                        if FCodigoConvenio <> 0 then MostrarDatosConvenio;

                        lblDatosGastoNotarial.Caption := 'Sin Gastos Notariales.';
                        btnImprimirOriginal.Enabled   := False;
                        btnImprimirDefinitiva.Enabled := False;

                        ActiveControl := vcbTipoRefinanciacion;
                    end
                    else begin
                        with spObtenerRefinanciacionDatosCabecera do begin
                            Parameters.ParamByName('@CodigoRefinanciacion').Value := FCodigoRefinanciacionUnificada;
                            Open;

                            FPersona              := ObtenerDatosPersonales(DMConnections.BaseCAC,'','', FieldByName('CodigoPersona').AsInteger);
                            FCodigoConvenio       := iif(CodigoConvenio = 0, FieldByName('CodigoConvenio').AsInteger, CodigoConvenio);
                            //FEsConvenioCN         := 0;						//SS_1147_MCA_20140408
                            FEsConvenioNativo	  := 0;							//SS_1147_MCA_20140408
                            FCodigoEstado         := REFINANCIACION_ESTADO_EN_TRAMITE;
                            FEditable             := True;
                            FCuotasEditable       := False;

                            lblNumRefinanciacion.Caption := '<Nueva>';
                            dedtFechaRefi.Date   := Trunc(NowBase(DMConnections.BaseCAC));
                            FFechaRefinanciacion := dedtFechaRefi.Date;

                            vcbTipoRefinanciacion.Value := FieldByName('CodigoTipoMedioPago').AsInteger;
                            vcbTipoRefinanciacionChange(Nil);
                            cbNotificacion.Checked := iif( FieldByName('Notificacion').AsBoolean, True, False);

                            TPanelMensajesForm.MuestraMensaje('Cargando Datos Representante Legal Cliente ...');
                            with FRepresentanteLegalCli do begin
                                NumeroDocumento := FieldByName('RepLegalCliNumeroDocumento').AsString;
                                Nombre := FieldByName('RepLegalCliNombre').AsString;
                                Apellido := FieldByName('RepLegalCliApellido').AsString;
                                ApellidoMaterno := FieldByName('RepLegalCliApellidoMaterno').AsString;
                            end;

                            TPanelMensajesForm.MuestraMensaje('Cargando Datos Representante Legal CN ...');
                            with FRepresentanteLegalCN do begin
                                NumeroDocumento := FieldByName('RepLegalCNNumeroDocumento').AsString;
                                Nombre := FieldByName('RepLegalCNNombre').AsString;
                                Apellido := FieldByName('RepLegalCNApellido').AsString;
                                ApellidoMaterno := FieldByName('RepLegalCNApellidoMaterno').AsString;
                            end;

                            if FPersona.CodigoPersona <> -1 then begin
                                TPanelMensajesForm.MuestraMensaje('Cargando Datos Domicilio ...');
                                with spObtenerRefinanciacionDomicilio do try
                                    Parameters.ParamByName('@CodigoRefinanciacion').Value := FCodigoRefinanciacion;
                                    Open;

                                    if RecordCount = 1 then begin
                                        with FDatosDomicilio do begin
                                            Descripcion         := FieldByName('Calle').AsString;
                                            CalleDesnormalizada := FieldByName('Calle').AsString;
                                            NumeroCalle         := FieldByName('Numero').AsInteger;
                                            NumeroCalleSTR      := FieldByName('Numero').AsString;
                                            Piso                := FieldByName('Piso').AsInteger;
                                            Dpto                := FieldByName('Dpto').AsString;
                                            Detalle             := FieldByName('Detalle').AsString;
                                            CodigoPais          := FieldByName('CodigoPais').AsString;
                                            CodigoRegion        := FieldByName('CodigoRegion').AsString;
                                            CodigoComuna        := FieldByName('CodigoComuna').AsString;
                                            CodigoPostal        := FieldByName('CodigoPostal').AsString;
                                            CodigoDomicilio     := -1;
                                            CodigoCalle         := -1;
                                            CodigoTipoCalle     := -1;
                                            CodigoSegmento      := -1;
                                            CodigoTipoDomicilio := 1;
                                        end;

                                        FDomicilioModificado := True;
                                    end;
                                finally
                                    if Active then Close;
                                end;

                                TPanelMensajesForm.MuestraMensaje('Cargando Datos Tel�fonos ...');
                                with spObtenerRefinanciacionTelefonos do try
                                    Parameters.ParamByName('@CodigoRefinanciacion').Value := FCodigoRefinanciacion;
                                    Open;

                                    if RecordCount > 0 then begin
                                        while not eof do begin
                                            case RecNo of
                                                1: begin
                                                    FDatosTelefonos.CodigoArea1 := FieldByName('CodigoArea').AsInteger;
                                                    FDatosTelefonos.Telefono1   := FieldByName('NumeroTelefono').AsString;
                                                end;
                                                2: begin
                                                    FDatosTelefonos.CodigoArea2 := FieldByName('CodigoArea').AsInteger;
                                                    FDatosTelefonos.Telefono2   := FieldByName('NumeroTelefono').AsString;
                                                end;
                                            end;

                                            Next;
                                        end;

                                        FTelefonosModificados := True;
                                    end;
                                finally
                                    if Active then Close;
                                end;

                                TPanelMensajesForm.MuestraMensaje('Cargando Datos Cliente ...');
                                MostrarDatosCliente;

                                TPanelMensajesForm.MuestraMensaje('Cargando Datos Convenios ...');
                                CargarConveniosRUT(DMConnections.BaseCAC, vcbConvenios, FPersona.TipoDocumento, FPersona.NumeroDocumento, False, FCodigoConvenio, False, True);     //SS_1120_MVI_20130820
                            end;

                            if FCodigoConvenio <> 0 then MostrarDatosConvenio;

                            MostrarRepresentanteLegalCN;

                            edtCausaRol.Text := FieldByName('NumeroCausaRol').AsString;

                            TPanelMensajesForm.MuestraMensaje('Cargando Datos en JPL ...');
                            nedtJPLNro.Value   := spObtenerRefinanciacionDatosCabecera.FieldByName('JPLnumero').AsInteger;
                            if spObtenerRefinanciacionDatosCabecera.FieldByName('JPLCodigoRegion').AsVariant <> Null then begin
                                vcbJPLRegion.ItemIndex := vcbJPLRegion.Items.IndexOfValue(spObtenerRefinanciacionDatosCabecera.FieldByName('JPLCodigoRegion').AsVariant);
                                if spObtenerRefinanciacionDatosCabecera.FieldByName('JPLCodigoComuna').AsVariant <> Null then begin
                                    CargarComunas(DMConnections.BaseCAC, vcbJPLComuna,PAIS_CHILE, spObtenerRefinanciacionDatosCabecera.FieldByName('JPLCodigoRegion').AsVariant, spObtenerRefinanciacionDatosCabecera.FieldByName('JPLCodigoComuna').AsVariant, True)
                                end
                                else CargarComunas(DMConnections.BaseCAC, vcbJPLComuna,PAIS_CHILE, spObtenerRefinanciacionDatosCabecera.FieldByName('JPLCodigoRegion').AsVariant, '', True);
                                vcbJPLComuna.Enabled := vcbJPLComuna.Items.Count > 0;
                            end;
                        end;

                        begin
                            vcbTipoRefinanciacion.Enabled    := False;
                            dedtFechaRefi.Enabled            := False;
                            cbNotificacion.Enabled           := False;
                            peRut.ReadOnly                   := True;
                            peRut.OnButtonClick              := Nil;
                            peRut.OnChange                   := Nil;
                            peRut.OnKeyPress                 := Nil;
                            btnBuscarRut.Enabled             := False;
                            btnCambiarRepLegCN.Enabled       := False;
                            edtCausaRol.Enabled              := False;
                            nedtJPLNro.Enabled               := False;
                            vcbJPLRegion.Enabled             := False;
                            vcbJPLComuna.Enabled             := False;

                            lblDatosGastoNotarial.Caption := 'Sin Gastos Notariales.';
                            btnImprimirOriginal.Enabled   := False;
                            btnImprimirDefinitiva.Enabled := False;
                        end;

                        ActiveControl := vcbConvenios;
                    end;
					//END : SS_1051_PDO--------------------------------------------------------------------------------------
                    Result := True;
                End;
            else // Cargar Refinanciaci�n ya Existente
                TPanelMensajesForm.MuestraMensaje(Format('inicializando Refinanciaci�n Nro. %d ...',[FCodigoRefinanciacion]));
                TPanelMensajesForm.MuestraMensaje('Cargando Datos Principales ...');
                with spObtenerRefinanciacionDatosCabecera do begin
                    Parameters.ParamByName('@CodigoRefinanciacion').Value := FCodigoRefinanciacion;
                    Open;

                    FPersona              := ObtenerDatosPersonales(DMConnections.BaseCAC,'','', FieldByName('CodigoPersona').AsInteger);
                    FCodigoConvenio       := FieldByName('CodigoConvenio').AsInteger;
                    //FEsConvenioCN         := 0;								//SS_1147_MCA_20140408
                    FEsConvenioNativo	  := 0;									//SS_1147_MCA_20140408
                    FCodigoEstado         := FieldByName('CodigoEstado').AsInteger;
                    FEditable             := (FCodigoEstado = REFINANCIACION_ESTADO_EN_TRAMITE);
                    FCuotasEditable       := (FCodigoEstado = REFINANCIACION_ESTADO_VIGENTE);
                    lblEstado.Caption     := FieldByName('EstadoRefinanciacion').AsString;
                    lblProtestada.Visible := FieldByName('Protestada').AsBoolean;

                    FTotalDeuda           := FieldByName('TotalDeuda').AsInteger;

                    lblNumRefinanciacion.Caption := IntToStr(Refinanciacion);
                    dedtFechaRefi.Date   := FieldByName('Fecha').AsDateTime;
                    FFechaRefinanciacion := FieldByName('Fecha').AsDateTime;
                    vcbTipoRefinanciacion.Value := FieldByName('CodigoTipoMedioPago').AsInteger;
                    vcbTipoRefinanciacionChange(Nil);
                    cbNotificacion.Checked := iif( FieldByName('Notificacion').AsBoolean, True, False);
                    lblNombrePlan.Caption := FieldByName('NombrePlanRefinanciacion').AsString;

                    TPanelMensajesForm.MuestraMensaje('Cargando Datos Representante Legal Cliente ...');
                    with FRepresentanteLegalCli do begin
                        NumeroDocumento := FieldByName('RepLegalCliNumeroDocumento').AsString;
                        Nombre := FieldByName('RepLegalCliNombre').AsString;
                        Apellido := FieldByName('RepLegalCliApellido').AsString;
                        ApellidoMaterno := FieldByName('RepLegalCliApellidoMaterno').AsString;
                    end;

                    TPanelMensajesForm.MuestraMensaje('Cargando Datos Representante Legal CN ...');
                    with FRepresentanteLegalCN do begin
                        NumeroDocumento := FieldByName('RepLegalCNNumeroDocumento').AsString;
                        Nombre := FieldByName('RepLegalCNNombre').AsString;
                        Apellido := FieldByName('RepLegalCNApellido').AsString;
                        ApellidoMaterno := FieldByName('RepLegalCNApellidoMaterno').AsString;
                    end;

                    if FPersona.CodigoPersona <> -1 then begin
                        TPanelMensajesForm.MuestraMensaje('Cargando Datos Domicilio ...');
                        with spObtenerRefinanciacionDomicilio do try
                            Parameters.ParamByName('@CodigoRefinanciacion').Value := FCodigoRefinanciacion;
                            Open;

                            if RecordCount = 1 then begin
                                with FDatosDomicilio do begin
                                    Descripcion         := FieldByName('Calle').AsString;
                                    CalleDesnormalizada := FieldByName('Calle').AsString;
                                    NumeroCalle         := FieldByName('Numero').AsInteger;
                                    NumeroCalleSTR      := FieldByName('Numero').AsString;
                                    Piso                := FieldByName('Piso').AsInteger;
                                    Dpto                := FieldByName('Dpto').AsString;
                                    Detalle             := FieldByName('Detalle').AsString;
                                    CodigoPais          := FieldByName('CodigoPais').AsString;
                                    CodigoRegion        := FieldByName('CodigoRegion').AsString;
                                    CodigoComuna        := FieldByName('CodigoComuna').AsString;
                                    CodigoPostal        := FieldByName('CodigoPostal').AsString;
                                    CodigoDomicilio     := -1;
                                    CodigoCalle         := -1;
                                    CodigoTipoCalle     := -1;
                                    CodigoSegmento      := -1;
                                    CodigoTipoDomicilio := 1;
                                end;

                                FDomicilioModificado := True;
                            end;
                        finally
                            if Active then Close;
                        end;

                        TPanelMensajesForm.MuestraMensaje('Cargando Datos Tel�fonos ...');
                        with spObtenerRefinanciacionTelefonos do try
                            Parameters.ParamByName('@CodigoRefinanciacion').Value := FCodigoRefinanciacion;
                            Open;

                            if RecordCount > 0 then begin
                                while not eof do begin
                                    case RecNo of
                                        1: begin
                                            FDatosTelefonos.CodigoArea1 := FieldByName('CodigoArea').AsInteger;
                                            FDatosTelefonos.Telefono1   := FieldByName('NumeroTelefono').AsString;
                                        end;
                                        2: begin
                                            FDatosTelefonos.CodigoArea2 := FieldByName('CodigoArea').AsInteger;
                                            FDatosTelefonos.Telefono2   := FieldByName('NumeroTelefono').AsString;
                                        end;
                                    end;

                                    Next;
                                end;

                                FTelefonosModificados := True;
                            end;
                        finally
                            if Active then Close;
                        end;

                        TPanelMensajesForm.MuestraMensaje('Cargando Datos Cliente ...');
                        MostrarDatosCliente;

                        TPanelMensajesForm.MuestraMensaje('Cargando Datos Convenios ...');
                        CargarConveniosRUT(DMConnections.BaseCAC, vcbConvenios, FPersona.TipoDocumento, FPersona.NumeroDocumento, False, FCodigoConvenio, False, True);   //SS_1120_MVI_20130820
                    end;

                    if FCodigoConvenio <> 0 then MostrarDatosConvenio;

                    MostrarRepresentanteLegalCN;

                    edtCausaRol.Text := FieldByName('NumeroCausaRol').AsString;
                    if FieldByName('GastoNotarial').AsInteger <> 0 then begin
                        nedtGastoNotarial.ValueInt := FieldByName('GastoNotarial').AsInteger;
                        nedtGastoNotarialChange(Nil);
                    end
                    else lblDatosGastoNotarial.Caption := 'Sin Gastos Notariales.';

                    if FieldByName('TotalDemanda').AsInteger <> 0 then nedtDemanda.ValueInt := FieldByName('TotalDemanda').AsInteger;

                    TPanelMensajesForm.MuestraMensaje('Cargando Datos Comprobantes Refinanciados ...');
                    MostrarComprobantes;
                    TPanelMensajesForm.MuestraMensaje('Cargando Datos Cuotas ...');
                    MostrarCuotas;
                    TPanelMensajesForm.MuestraMensaje('Cargando Patentes en Precautoria ...');
                    MostrarPatentesPrecautoria;

                    TPanelMensajesForm.MuestraMensaje('Cargando Datos en JPL ...');												//SS_1051_PDO
                    nedtJPLNro.Value   := spObtenerRefinanciacionDatosCabecera.FieldByName('JPLnumero').AsInteger;
                    if spObtenerRefinanciacionDatosCabecera.FieldByName('JPLCodigoRegion').AsVariant <> Null then begin
                        vcbJPLRegion.ItemIndex := vcbJPLRegion.Items.IndexOfValue(spObtenerRefinanciacionDatosCabecera.FieldByName('JPLCodigoRegion').AsVariant);
                        if spObtenerRefinanciacionDatosCabecera.FieldByName('JPLCodigoComuna').AsVariant <> Null then begin
                            CargarComunas(DMConnections.BaseCAC, vcbJPLComuna,PAIS_CHILE, spObtenerRefinanciacionDatosCabecera.FieldByName('JPLCodigoRegion').AsVariant, spObtenerRefinanciacionDatosCabecera.FieldByName('JPLCodigoComuna').AsVariant, True)
                        end
                        else CargarComunas(DMConnections.BaseCAC, vcbJPLComuna,PAIS_CHILE, spObtenerRefinanciacionDatosCabecera.FieldByName('JPLCodigoRegion').AsVariant, '', True);
                        vcbJPLComuna.Enabled := vcbJPLComuna.Items.Count > 0;
                    end;

					//BEGIN : SS_1051_PDO-----------------------------------------------------------------------------------------------------------------
                    if (not FEditable) or (FConsulta) or (FRefinanciacionUnificada) then begin
                        vcbTipoRefinanciacion.Enabled    := False;
                        dedtFechaRefi.Enabled            := False;
                        cbNotificacion.Enabled           := False;
                        peRut.ReadOnly                   := True;
                        peRut.OnButtonClick              := Nil;
                        peRut.OnChange                   := Nil;
                        peRut.OnKeyPress                 := Nil;
                        btnBuscarRut.Enabled             := False;
                        vcbConvenios.Enabled             := FEditable and (not FConsulta);
                        btnCambiarRepLegCN.Enabled       := FEditable and ((not FRefinanciacionUnificada) or FRefinanciacionMaestra) and (not FConsulta);
                        edtCausaRol.Enabled              := FEditable and ((not FRefinanciacionUnificada) or FRefinanciacionMaestra) and (not FConsulta);
                        nedtDemanda.Enabled              := FEditable and (not FConsulta);
                        btnSugerirImporteDemanda.Enabled := FEditable and (not FConsulta);
                        nedtGastoNotarial.Enabled        := FEditable and (not FConsulta);
                        nedtJPLNro.Enabled               := FEditable and ((not FRefinanciacionUnificada) or FRefinanciacionMaestra) and (not FConsulta);
                        vcbJPLRegion.Enabled             := FEditable and ((not FRefinanciacionUnificada) or FRefinanciacionMaestra) and (not FConsulta);
                        vcbJPLComuna.Enabled             := FEditable and ((not FRefinanciacionUnificada) or FRefinanciacionMaestra) and (not FConsulta);
                        btnValidar.Enabled               := (FCuotasEditable or FEditable) and (not FConsulta);
                        btnGrabar.Enabled                := (FCuotasEditable or FEditable) and (not FConsulta);
                        btnCancelar.Caption              := iif((FCuotasEditable = False) or (FConsulta), 'Salir', btnCancelar.Caption);
                    end
                    else begin
                        dedtFechaRefi.Enabled := (not FConsulta) and (ExisteAcceso('modif_fecha_gestion_ref'));
                    end;
					//END : SS_1051_PDO-----------------------------------------------------------------------------------------------------------------

                    btnImprimir.Enabled           := (not FConsulta) and (FCodigoEstado in [REFINANCIACION_ESTADO_EN_TRAMITE, REFINANCIACION_ESTADO_VIGENTE, REFINANCIACION_ESTADO_TERMINADA]);
                    btnImprimirDefinitiva.Enabled := (not FConsulta) and FRefinanciacionMaestra and (ExisteAcceso('imprimir_definitivo_gestion_ref') and (FCodigoEstado in [REFINANCIACION_ESTADO_EN_TRAMITE]));  //SS_1051_PDO
                    btnImprimirOriginal.Enabled   := (not FConsulta) and (FCodigoEstado in [REFINANCIACION_ESTADO_ACEPTADA, REFINANCIACION_ESTADO_VIGENTE, REFINANCIACION_ESTADO_TERMINADA]);
                    btnSugerirImporteDemanda.Visible := ExisteAcceso('boton_sugerir_importe_ref');  // SS_1245_CQU_20150224
                    ActiveControl := peRut;

                    Result := True;
                end;

            end;
        except
            on e:Exception do begin
                TPanelMensajesForm.OcultaPanel;
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
        if Result then begin
            ActualizarBotonesCuotas(False);
            DBListExResaltaCamposIndex(DBListEx3);
        end;
        CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

procedure TRefinanciacionGestionForm.vcbConveniosChange(Sender: TObject);
begin
    FCodigoConvenio := vcbConvenios.Value;
    MostrarDatosConvenio;
    LimpiarComprobantes;				//SS_1051_PDO
end;

//BEGIN:SS_1120_MVI_20130820 -------------------------------------------------
{-----------------------------------------------------------------------------
  Procedure: TRefinanciacionGestionForm.vcbConveniosDrawItem
  Date:      20-August-2013
  Description: Verifica si existe alg�n convenio de baja para llamar a la funci�n
                que colorea de rojo el item.
-----------------------------------------------------------------------------}
procedure TRefinanciacionGestionForm.vcbConveniosDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  if Pos( TXT_CONVENIO_DE_BAJA, vcbConvenios.Items[Index].Caption ) > 0 then  begin

     ItemComboBoxColor(Control, Index, Rect, State, clred, true);

  end
  else begin

     ItemComboBoxColor(Control, Index, Rect, State);

  end;                                                                                 

end;
//END:SS_1120_MVI_20130820 -------------------------------------------------

procedure TRefinanciacionGestionForm.vcbJPLRegionChange(Sender: TObject);
begin
    if vcbJPLRegion.Items.Count > 0 then begin
        if vcbJPLRegion.Value <> Null then try
            Screen.cursor := crHourGlass;
            Application.ProcessMessages;

            CargarComunas(DMConnections.BaseCAC, vcbJPLComuna, PAIS_CHILE, vcbJPLRegion.Value, '', True);
        finally
            Screen.Cursor := crDefault;
        end
        else vcbJPLComuna.Items.Clear;
    end
    else vcbJPLComuna.Items.Clear;

    vcbJPLComuna.Enabled := (vcbJPLComuna.Items.Count > 0);
end;

procedure TRefinanciacionGestionForm.vcbTipoRefinanciacionChange(Sender: TObject);
begin
    FCodigoCanalPago := vcbTipoRefinanciacion.Value;
    LimpiarRepresentanteLegalCN;



    case (vcbTipoRefinanciacion.Value  = CANAL_PAGO_AVENIMIENTO) of
        True: begin // Avenimiento
            cbNotificacion.Enabled        := True;
//            lblGastoNotarial.Enabled      := False;
//            nedtGastoNotarial.Enabled     := False;
//            nedtGastoNotarial.Value       := 0;
//            lblDatosGastoNotarial.Caption := 'Sin Gastos Notariales.';
            lblCausaRol.Enabled           := True;
            edtCausaRol.Enabled           := True;
            lblDemanda.Enabled            := True;
            nedtDemanda.Enabled           := True;
            gbDatosJPL.Enabled            := True;
            lblJPL_Nro.Enabled            := True;
            nedtJPLNro.Enabled            := True;
            lblJPL_Region.Enabled         := True;
            vcbJPLRegion.Enabled          := True;
            lblJPL_Comuna.Enabled         := True;
            vcbJPLComuna.Enabled          := True;
            btnSugerirImporteDemanda.Enabled := True;

            if vcbJPLRegion.Items.Count = 0 then try
                Screen.Cursor := crHourGlass;
                Application.ProcessMessages;

                CargarRegiones(DMConnections.BaseCAC, vcbJPLRegion, PAIS_CHILE, '', True);
                vcbJPLComuna.Enabled := False;
            finally
                Screen.Cursor := crDefault;
            end;
            //btnAgregarPatentePrecautoria.Enabled := (FPersona.CodigoPersona <> -1) and (FEsConvenioCN = 1);
            //TASK_006_AUN_20170406-CU.COBO.COB.302: se elimina el tab Otros datos
            //btnAgregarPatentePrecautoria.Enabled := (FPersona.CodigoPersona <> -1);
        end;
        False: begin // Repactaci�n
            cbNotificacion.Checked        := False;
            cbNotificacion.Enabled        := False;
//            lblGastoNotarial.Enabled      := True;
//            nedtGastoNotarial.Enabled     := True;
//            lblDatosGastoNotarial.Caption := '';
            lblCausaRol.Enabled           := False;
            edtCausaRol.Enabled           := False;
            edtCausaRol.Text              := EmptyStr;
            lblDemanda.Enabled            := False;
            nedtDemanda.Enabled           := False;
            nedtDemanda.Value             := 0;
            gbDatosJPL.Enabled            := False;
            lblJPL_Nro.Enabled            := False;
            nedtJPLNro.Enabled            := False;
            nedtJPLNro.Value              := 0;
            lblJPL_Region.Enabled         := False;
            vcbJPLRegion.Enabled          := False;
            vcbJPLRegion.Clear;
            lblJPL_Comuna.Enabled         := False;
            vcbJPLComuna.Enabled          := False;
            vcbJPLComuna.Items.Clear;
            btnSugerirImporteDemanda.Enabled := False;
            //TASK_006_AUN_20170406-CU.COBO.COB.302: se elimina el tab Otros datos
            //LimpiarPatentesPrecautoria;
        end;
    end;
end;

procedure TRefinanciacionGestionForm.peRutButtonClick(Sender: TObject);
    var
        BuscaClientes: TFormBuscaClientes;
begin
    try
        BuscaClientes := TFormBuscaClientes.Create(Self);
        if BuscaClientes.Inicializa(FUltimaBusqueda)then begin
            if BuscaClientes.ShowModal = mrOk then begin

                LimpiarComprobantes;
                //TASK_006_AUN_20170406-CU.COBO.COB.302: se elimina el tab Otros datos
                //LimpiarPatentesPrecautoria;
                LimpiarDatosCliente;

                FUltimaBusqueda := BuscaClientes.UltimaBusqueda;
                FPersona        := BuscaClientes.Persona;
                MostrarDatosCliente;

            	CargarConveniosRUT(DMConnections.BaseCAC, vcbConvenios, FPersona.TipoDocumento, FPersona.NumeroDocumento, False, 0, False, True);
                if vcbConvenios.Items.Count > 0 then begin
                    FCodigoConvenio := vcbConvenios.Value;
                    MostrarDatosConvenio;
                    vcbConvenios.SetFocus;
                end
                else FCodigoConvenio := 0;

                VerificarClienteCarpetaLegal;
            end;
        end;
    finally
        if Assigned(BuscaClientes) then FreeAndNil(BuscaClientes);
    end;
end;

procedure TRefinanciacionGestionForm.peRutChange(Sender: TObject);
begin
    try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;
        LimpiarDatosCliente;
        LimpiarComprobantes;
        //TASK_006_AUN_20170406-CU.COBO.COB.302: se elimina el tab Otros datos
        //LimpiarPatentesPrecautoria;
        LimpiarCuotas;
        FPersona.CodigoPersona := -1;
    finally
        Screen.Cursor := crDefault;
        Application.ProcessMessages;
    end;
end;

procedure TRefinanciacionGestionForm.peRutKeyPress(Sender: TObject; var Key: Char);
    resourcestring
        rs_BUSQUEDA_TITULO = 'B�squeda Persona';
        rs_BUSQUEDA_NO_ENCONTRADA = 'El Rut introducido NO Existe o NO es V�lido.';
begin
    if key = #13 then begin
        FPersona := ObtenerDatosPersonales(DMConnections.BaseCAC, 'RUT', peRut.Text);

        if FPersona.CodigoPersona <> -1 then begin
            MostrarDatosCliente;

            CargarConveniosRUT(DMConnections.BaseCAC, vcbConvenios, FPersona.TipoDocumento, FPersona.NumeroDocumento, False, 0, False, True);
            if vcbConvenios.Items.Count > 0 then begin
                FCodigoConvenio := vcbConvenios.Value;
                MostrarDatosConvenio;
                vcbConvenios.SetFocus;
            end
            else FCodigoConvenio := 0;

            VerificarClienteCarpetaLegal;
            VerificarRefinanciacionesEnTramiteAceptadas;
            VerificaPersonaListaAmarilla(FPersona.NumeroDocumento);			//SS_1236_MCA_20141226
        end
        else ShowMsgBoxCN(rs_BUSQUEDA_TITULO, rs_BUSQUEDA_NO_ENCONTRADA, MB_ICONWARNING, Self);
    end;
end;

procedure TRefinanciacionGestionForm.MostrarDatosCliente;
    var
        AuxDomicilio: TDomiciliosPersona;
    const
        SQLObtenerNombrePersona   = 'SELECT dbo.ObtenerNombrePersona(%d)';
begin
    try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;
        peRut.OnChange := Nil;

        peRut.Text           := FPersona.NumeroDocumento;
        lblNombreCli.Caption := QueryGetValue(DMConnections.BaseCAC, Format(SQLObtenerNombrePersona, [FPersona.CodigoPersona]));

        if FPersona.Personeria = PERSONERIA_FISICA then begin
            lblPersoneria.Caption := ' - (Persona Natural)';
            if Assigned(FRepresentantesLegales) then FreeAndNil(FRepresentantesLegales);
        end
        else begin
            if FPersona.Personeria = PERSONERIA_JURIDICA then lblPersoneria.Caption := ' - (Persona Jur�dica)';
            if FPersona.Personeria = PERSONERIA_AMBAS then lblPersoneria.Caption := ' - (Persona Natural y Jur�dica)';
        end;

        lblPersoneria.Left := lblNombreCli.Left + lblNombreCli.Width;
        //lblDomicilioCliDesc.Visible := True;

        if not FDomicilioModificado then begin
            AuxDomicilio    := TDomiciliosPersona.Create(FPersona.CodigoPersona);
            FDatosDomicilio := AuxDomicilio.DomicilioPrincipal;
        end;

        MostrarDomicilioCliente;
        MostrarTelefonosCliente;
    finally
        if Assigned(AuxDomicilio) then FreeAndNil(AuxDomicilio);
        peRut.OnChange := peRutChange;

        Screen.Cursor := crDefault;
        Application.ProcessMessages;
    end;
end;

procedure TRefinanciacionGestionForm.MostrarDatosConvenio;
    resourcestring
        rs_NO_ES_CONVENIO_CN_TITULO  = 'Validaci�n Convenio';
        rs_NO_ES_CONVENIO_CN_MENSAJE = 'El Convenio Seleccionado NO es pertenece a Costanera Norte';
        MSG_CONVENIOENLISTAAMARILLA = 'Convenio se encuentra en lista amarilla';                         //SS_1236_MCA_20141226
    var
        FRepresentanteLegal: TDatosPersonales;
        EstadoListaAmarilla : String;							//SS_1236_MCA_20141226
begin
    try
        Screen.Cursor := crHandPoint;
        Application.ProcessMessages;

//        FEsConvenioCN := QueryGetValueInt(DMConnections.BaseCAC, Format('SELECT dbo.EsConvenioCN(%d)', [FCodigoConvenio]));

        //lbldomicilioFactDesc.Visible := True;
        lblDomicilioFact.Caption := QueryGetValue(DMConnections.BaseCAC, Format('SELECT dbo.ObtenerDescripcionDomicilioFacturacion(%d)', [FCodigoConvenio]));
        lblConcesionaria.Caption := QueryGetValue(DMConnections.BaseCAC, Format('SELECT dbo.ObtenerConcesionariaConvenio(%d)', [FCodigoConvenio]));

//        if (FEsConvenioCN = 1) and (FPersona.Personeria in [PERSONERIA_JURIDICA, PERSONERIA_AMBAS]) then begin
        if (FPersona.Personeria in [PERSONERIA_JURIDICA, PERSONERIA_AMBAS]) then begin
            if Assigned(FRepresentantesLegales) then FreeAndNil(FRepresentantesLegales);
            FRepresentantesLegales := TRepresentantesLegales.Create(FCodigoConvenio, FPersona.CodigoPersona);
            if FRepresentantesLegales.CantidadRepresentantes > 0 then begin
                if (FCodigoRefinanciacion = 0) or (FRepresentanteLegalCli.NumeroDocumento = EmptyStr) then begin
                    FRepresentanteLegal := ObtenerDatosPersonales(DMConnections.BaseCAC, '', '', FRepresentantesLegales.Representantes[0].CodigoPersona);

                    FRepresentanteLegalCli.NumeroDocumento := FRepresentanteLegal.NumeroDocumento;
                    FRepresentanteLegalCli.Nombre          := FRepresentanteLegal.Nombre;
                    FRepresentanteLegalCli.Apellido        := FRepresentanteLegal.Apellido;
                    FRepresentanteLegalCli.ApellidoMaterno := FRepresentanteLegal.ApellidoMaterno;
                end;
            end;

            MostrarRepresentanteLegalCli;

            btnCambiarRepLegCli.Enabled          := True and FEditable and FRefinanciacionMaestra and (not FConsulta);				//SS_1051_PDO
            btnAgregarComprobantes.Enabled       := True and FEditable and (not FConsulta);
//            btnAgregarCuota.Enabled              := True and FEditable and (not FConsulta);
            //TASK_006_AUN_20170406-CU.COBO.COB.302: se elimina el tab Otros datos
            //btnAgregarPatentePrecautoria.Enabled := True and (vcbTipoRefinanciacion.Value = 38) and FEditable and (not FConsulta);
        end
        else begin
            LimpiarRepresentanteLegalCli;

            btnAgregarComprobantes.Enabled       := FEditable and (not FConsulta);
//            btnAgregarCuota.Enabled              := FEditable and (not FConsulta);
            //TASK_006_AUN_20170406-CU.COBO.COB.302: se elimina el tab Otros datos
            //btnAgregarPatentePrecautoria.Enabled := (vcbTipoRefinanciacion.Value = 38) and FEditable and (not FConsulta);

            {
            btnAgregarComprobantes.Enabled       := (FEsConvenioCN = 1) and FEditable;
            btnAgregarCuota.Enabled              := (FEsConvenioCN = 1) and FEditable;
            btnAgregarPatentePrecautoria.Enabled := (FEsConvenioCN = 1) and (vcbTipoRefinanciacion.Value = 38) and FEditable;

            if FEsConvenioCN = 0 then begin
                if Self.Visible then
                    ShowMsgBoxCN(rs_NO_ES_CONVENIO_CN_TITULO, rs_NO_ES_CONVENIO_CN_MENSAJE, MB_ICONWARNING, Self)
                else begin
                    FMensajesInicializar.Add(
                        Format(rsInicializacionMensaje, [rs_NO_ES_CONVENIO_CN_TITULO, rs_NO_ES_CONVENIO_CN_MENSAJE]));
                end;
            end;
            }
        end;

        EstadoListaAmarilla:= VerificarConvenioListaAmarilla(FCodigoConvenio);  //SS_1236_MCA_20141226
        if EstadoListaAmarilla <> '' then begin                                 //SS_1236_MCA_20141226
            lblEnListaAmarilla.Visible  := True;                                //SS_1236_MCA_20141226
            lblEnListaAmarilla.Caption  := EstadoListaAmarilla;                 //SS_1236_MCA_20141226
        end                                                                     //SS_1236_MCA_20141226
        else begin                                                              //SS_1236_MCA_20141226
            lblEnListaAmarilla.Visible  := False;                               //SS_1236_MCA_20141226
        end;                                                                    //SS_1236_MCA_20141226

    finally
        Screen.Cursor := crDefault;
        Application.ProcessMessages;
    end;
end;

procedure TRefinanciacionGestionForm.LimpiarDatosCliente;
    Var
        DatosDomicilio: TDatosDomicilio;
begin
    FRepresentanteLegalCli.NumeroDocumento := EmptyStr;
    FRepresentanteLegalCli.Nombre          := EmptyStr;
    FRepresentanteLegalCli.Apellido        := EmptyStr;
    FRepresentanteLegalCli.ApellidoMaterno := EmptyStr;

    FDatosDomicilio               := DatosDomicilio;
    FDomicilioModificado          := False;
    btnCambiarDomicilio.Enabled   := False;
    btnRestaurarDomicilio.Enabled := False;

    vcbConvenios.Clear;
    LimpiarComprobantes;

    lblConcesionaria.Caption       := EmptyStr;
    lblNombreCli.Caption           := EmptyStr;
    lblPersoneria.Caption          := EmptyStr;
    //lblDomicilioCliDesc.Visible    := False;
    lblDomicilioCli.Caption        := EmptyStr;
    //lbldomicilioFactDesc.Visible   := False;
    lblDomicilioFact.Caption       := EmptyStr;
    lblRutRepLegCliDesc.Enabled    := False;
    lblRutRepLegCli.Caption        := EmptyStr;
    lblNombreRepLegCliDesc.Enabled := False;
    lblNombreRepLegCli.Caption     := EmptyStr;
    btnCambiarRepLegCli.Enabled    := False;
    lblTelefonos.Caption           := EmptyStr;
    btnCambiarTelefono.Enabled     := False;
    btnRestaurarTelefono.Enabled   := False;
    lblEnListaAmarilla.Visible  := False;                                       //SS_1236_MCA_20141226
    if Assigned(FRepresentantesLegales) then FreeAndNil(FRepresentantesLegales);
end;

procedure TRefinanciacionGestionForm.LimpiarComprobantes;
begin
    //btnAgregarComprobantes.Enabled := False;					//SS_1051_PDO
    btnEliminarComprobante.Enabled := False;

    if cdsDetalleComprobanteRefinanciado.Active then cdsDetalleComprobanteRefinanciado.Close;
    if cdsComprobantesRefinanciados.Active then cdsComprobantesRefinanciados.EmptyDataSet;

    FComprobantesRefinanciadosTotal := 0;
    ActualizarTotales;
end;

{//TASK_006_AUN_20170406-CU.COBO.COB.302: se elimina el tab Otros datos
procedure TRefinanciacionGestionForm.LimpiarPatentesPrecautoria;
begin
    btnAgregarPatentePrecautoria.Enabled := False;
    btnEliminarPatentePrecautoria.Enabled := False;

    cdsPatentesPrecautoria.EmptyDataSet;
end;
}

procedure TRefinanciacionGestionForm.LimpiarCuotas;
begin

    {
    btnAgregarCuota.Enabled    := False;
    btnEditarCuota.Enabled     := False;
    btnEliminarCuota.Enabled   := False;
    btnSugerirImportes.Enabled := False;
    btnAnularCuota.Enabled     := False;
    }
    if cdsCuotas.Active then cdsCuotas.EmptyDataSet;

    ActualizarBotonesCuotas(True);

    FCuotasCantidad := 0;
    FCuotasTotal    := 0;
end;

procedure TRefinanciacionGestionForm.ActualizarTotales;
    resourcestring
        rsValidacionImportesTitulo  = 'Validaci�n Total Cuotas';
        rsValidacionImportesMensaje = 'El Importe Total de las Cuotas Entradas es Mayor que el Total de Comprobantes Refinanciados.';
begin
    // Totales Comprobantes Refinanciados
    lblTotalRefinanciado.Caption    := FormatFloat(FORMATO_IMPORTE, FComprobantesRefinanciadosTotal);
    lblTotalRefinanciado2.Caption   := lblTotalRefinanciado.Caption;
    lblCantidadComprobantes.Caption := iif(cdsComprobantesRefinanciados.Active = True, IntToStr(cdsComprobantesRefinanciados.RecordCount), 0);
    // Totales Cuotas Refinanciaci�n
    lblCuotasCantidad.Caption       := IntToStr(FCuotasCantidad);
    lblCuotasTotal.Caption          := FormatFloat(FORMATO_IMPORTE, FCuotasTotal);
    lblDeudaPagada.Caption          := FormatFloat(FORMATO_IMPORTE, FDeudaPagada);
    lblDeudaPagada2.Caption         := FormatFloat(FORMATO_IMPORTE, FDeudaPagada);
    lblDeudaPendiente.Caption       := FormatFloat(FORMATO_IMPORTE, FComprobantesRefinanciadosTotal - FDeudaPagada);
    lblDeudaVencida.Caption         := FormatFloat(FORMATO_IMPORTE, FDeudaVencida);

    if FDeudaVencida > 0 then
        lblDeudaVencida.Font.Color := clRed
    else lblDeudaVencida.Font.Color := clBlack;

    if (FComprobantesRefinanciadosTotal - FDeudaPagada) > 0 then
        lblDeudaPendiente.Font.Color := $000080FF
    else lblDeudaPendiente.Font.Color := clBlack;
    

    if FCuotasTotal <> FComprobantesRefinanciadosTotal then begin
        lblCuotasTotal.Font.Color := clRed;
        if (cdsCuotas.RecordCount > 0) and (cdsComprobantesRefinanciados.RecordCount > 0) and (FCuotasTotal > FComprobantesRefinanciadosTotal) then begin
            if Self.Visible then
                ShowMsgBoxCN(rsValidacionImportesTitulo, rsValidacionImportesMensaje, MB_ICONWARNING, Self)
            else begin
                FMensajesInicializar.Add(
                    Format(rsInicializacionMensaje,[rsValidacionImportesTitulo, rsValidacionImportesMensaje]));
            end;
        end;
    end
    else lblCuotasTotal.Font.Color := clGreen;
end;

procedure TRefinanciacionGestionForm.btnAgregarCuotaClick(Sender: TObject);
begin
    try
        CambiarEstadoCursor(CURSOR_RELOJ, Self, FComponentes);

        RefinanciacionEntrarEditarCuotasForm := TRefinanciacionEntrarEditarCuotasForm.Create(Self);

        with RefinanciacionEntrarEditarCuotasForm do begin
            if Inicializar(TIPO_ENTRADA_DATOS_CUOTA, FCodigoCanalPago, FPersona, True, FFechaRefinanciacion, FEditable, cdsCuotas) then begin
                if (FUltimoNroPagareEntradoCuotas = EmptyStr) and (FCodigoCanalPago = CANAL_PAGO_AVENIMIENTO) then begin
                    if Trim(edtCausaRol.Text) <> EmptyStr then begin
                        FUltimoNroPagareEntradoCuotas := Trim(edtCausaRol.Text);
                    end;
                end;

                FUltimoBancoEntrado     := FUltimoBancoEntradoCuotas;
                FUltimoNroPagareEntrado := FUltimoNroPagareEntradoCuotas;

                if RefinanciacionEntrarEditarCuotasForm.ShowModal = mrOk then begin
                    Inc(FCuotasCantidad);

                    cdsCuotas.AppendRecord(
                        [0,                                                 // CodigoCuota
                         REFINANCIACION_ESTADO_CUOTA_PENDIENTE,             // CodigoEstadoPago
                         REFINANCIACION_ESTADO_CUOTA_PENDIENTE_DESCRIPCION, // DescripcionEstadoPago
                         FEntradaDatos.CodigoFormaPago,                     // CodigoFormaPago
                         FEntradaDatos.DescripcionFormaPago,                // DescripcionFormaPago
                         FEntradaDatos.TitularNombre,                       // TitularNombre
                         FEntradaDatos.TitularTipoDocumento,                // TitularTipoDocumento
                         FEntradaDatos.TitularNumeroDocumento,              // TitularNumeroDocumento
                         FEntradaDatos.CodigoBanco,                         // CodigoBanco
                         FEntradaDatos.DescripcionBanco,                    // DescripcionBanco
                         FEntradaDatos.DocumentoNumero,                     // DocumentoNumero
                         FEntradaDatos.Fecha,                               // FechaCuota
                         FEntradaDatos.CodigoTarjeta,                       // CodigoTarjeta
                         FEntradaDatos.DescripcionTarjeta,                  // Descripci�n Tarjeta
                         FEntradaDatos.TarjetaCuotas,                       // TarjetaCuotas
                         FEntradaDatos.Importe,                             // Importe
                         FEntradaDatos.Importe,                             // Saldo
                         FEntradaDatos.CodigoEntradaUsuario,                // CodigoEntradaUsuario
                         FEntradaDatos.DatosCuota.EstadoImpago,             // EstadoImpago
                         FEntradaDatos.DatosCuota.CodigoCuotaAntecesora,    //CodigoCuotaAntecesora
                         FEntradaDatos.DatosCuota.CuotaNueva,               // CuotaNueva
                         FEntradaDatos.DatosCuota.CuotaModificada]);        // CuotaModificada

                    FCuotasTotal := FCuotasTotal + FEntradaDatos.Importe;
                    if FEntradaDatos.Fecha < FAhora then begin
                        FDeudaVencida := FDeudaVencida + FEntradaDatos.Importe;
                    end;

                    ActualizarTotales;
                    ActualizarBotonesCuotas(False);

                    if FEntradaDatos.CodigoBanco <> 0 then begin
                        FUltimoBancoEntradoCuotas := FEntradaDatos.CodigoBanco;
                    end;

                    if FEntradaDatos.CodigoFormaPago in [FORMA_PAGO_PAGARE, FORMA_PAGO_AVENIMIENTO] then begin
                        FUltimoNroPagareEntradoCuotas := FEntradaDatos.DocumentoNumero;
                    end;

                end;
            end;
        end;
    finally
        if Assigned(RefinanciacionEntrarEditarCuotasForm) then FreeAndNil(RefinanciacionEntrarEditarCuotasForm);

        CambiarEstadoCursor(CURSOR_DEFECTO, Self, FComponentes);
    end;
end;

procedure TRefinanciacionGestionForm.btnValidarClick(Sender: TObject);
    resourcestring
        rsValidacionTitulo  = 'Validaciones Refinanciaci�n';
        rsValidacionMensaje = 'Todas las Validaciones de la Refinanciaci�n fueron Exitosas.';
begin
    if ValidarRefinanciacion(True, False) then ShowMsgBoxCN(rsValidacionTitulo, rsValidacionMensaje, MB_ICONASTERISK, Self);
end;

procedure TRefinanciacionGestionForm.btnBuscarRutClick(Sender: TObject);
    var
        Intro: Char;
begin
    if Trim(peRut.Text) <> '' then begin
        Intro := #13;
        peRutKeyPress(Nil, Intro);
    end;
end;

function TRefinanciacionGestionForm.ValidarRefinanciacion(Completa, Silencioso: Boolean): Boolean;
    resourcestring
        rsValidacionesTitulo = 'Validaciones Refinanciaci�n';
        rsErrorFechaRefinanciacion = 'La Fecha de la Refinanciaci�n NO puede ser anterior a la Fecha Actual.';
        rsErrorEntreRut = 'Debe Introducir un Rut para poder tramitar la Refinanciaci�n.';
        rsErrorRutInvalido = 'El Rut introducido NO es V�lido.';
        rsErrorSeleccionConvenio = 'No puede refinanciar sin Seleccionar un Convenio.';
        rsErrorConvenioNoCN = 'No puede refinanciar un Convenio que no pertenece a Costanera Norte.';
        rsErrorRepresentanteLegalCliente = 'Debe Seleccionar un Representante Legal para el Cliente.';
        rsErrorRepresentanteLegalCN = 'Debe Seleccionar un Representante Legal para Costanera Norte.';
        rsErrorJPLNumero = 'Debe Indicar un N�mero para el Juzgado de Polic�a Local.';
        rsErrorJPLRegion = 'Debe Seleccionar una Regi�n para el Juzgado de Polic�a Local.';
        rsErrorJPLComuna = 'Debe Seleccionar una Comuna para el Juzgado de Polic�a Local.';
        rsErrorTotalDemanda = 'Debe Introducir un Valor para el Importe de la Demanda.';
        rsErrorComprobantes = 'Debe Seleccionar Comprobantes para efectuar la Refinanciaci�n.';
        rsErrorCuotas = 'Debe Introducir las Cuotas de la Refinanciaci�n.';
        rsErrorImportes = 'El Importe Total de las Cuotas NO coincide con el Importe Total de la Refinanciaci�n.';
        rsErrorImportesCero = 'NO pueden Existir Cuotas con el Importe a Cero.';
        rsErrorPatentesPrecautoria = 'Debe Introducir al menos una Patente en Precautoria para el tipo de Refinanciaci�n Seleccionado.';
    const
        MaxArray = 14;
    var
        vControles  : Array [1..MaxArray] of TControl;
        vCondiciones: Array [1..MaxArray] of Boolean;
        vMensajes   : Array [1..MaxArray] of String;
begin
    try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;
        
        try
            vControles[01] := dedtFechaRefi;
            vControles[02] := peRut;
            vControles[03] := peRut;
            vControles[04] := vcbConvenios;
//            vControles[05] := vcbConvenios;
            vControles[05] := btnCambiarRepLegCli;
            vControles[06] := btnCambiarRepLegCN;
            vControles[07] := nedtJPLNro;
            vControles[08] := vcbJPLRegion;
            vControles[09] := vcbJPLComuna;
            vControles[10] := nedtDemanda;
            vControles[11] := btnAgregarComprobantes;
            vControles[12] := btnAgregarCuota;
            vControles[13] := btnAgregarCuota;
            vControles[14] := btnAgregarCuota;
//            vControles[11] := btnAgregarPatentePrecautoria;

            vCondiciones[01] := (FCodigoRefinanciacion <> 0) or (dedtFechaRefi.Date >= Trunc(NowBaseSmall(DMConnections.BaseCAC))) ;
            vCondiciones[02] := Trim(peRut.Text) <> EmptyStr;
            vCondiciones[03] := (Trim(peRut.Text) = EmptyStr) or ValidarRUT(DMConnections.BaseCAC, Trim(peRut.Text));
            vCondiciones[04] := (Trim(peRut.Text) = EmptyStr) or ((vcbConvenios.Items.Count > 0));
//            vCondiciones[05] := (Trim(peRut.Text) = EmptyStr) or (vcbConvenios.Items.Count = 0) or (FEsConvenioCN = 1);
            vCondiciones[05] := (not Completa) or (not btnCambiarRepLegCli.Enabled) or ((lblRutRepLegCli.Caption <> EmptyStr) and (lblNombreRepLegCli.Caption <> EmptyStr));
            vCondiciones[06] := (not Completa) or (not btnCambiarRepLegCN.Enabled) or ((lblRutRepLegCN.Caption <> EmptyStr) and (lblNombreRepLegCN.Caption <> EmptyStr));
            vCondiciones[07] := (not Completa) or (not nedtJPLNro.Enabled) or (nedtJPLNro.ValueInt >= 0);
            vCondiciones[08] := (not Completa) or (not vcbJPLRegion.Enabled) or ((vcbJPLRegion.Items.Count > 0) and (vcbJPLRegion.Value <> Null));
            vCondiciones[09] := (not Completa) or (not vcbJPLComuna.Enabled) or ((vcbJPLComuna.Items.Count > 0) and (vcbJPLComuna.Value <> Null));
            vCondiciones[10] := (not Completa) or (not nedtDemanda.Enabled) or (nedtDemanda.Value > 0);
            vCondiciones[11] := (not btnAgregarComprobantes.Enabled) or (cdsComprobantesRefinanciados.RecordCount > 0);
            vCondiciones[12] := (not Completa) or (not btnAgregarCuota.Enabled) or (cdsCuotas.RecordCount > 0);
            vCondiciones[13] := (not Completa) or ((not btnAgregarComprobantes.Enabled) and (not FCuotasEditable)) or ((cdsComprobantesRefinanciados.RecordCount = 0) and (FCodigoRefinanciacion > 20000)) or (not btnAgregarCuota.Enabled) or (cdsCuotas.RecordCount = 0) or (FCuotasTotal = FComprobantesRefinanciadosTotal);
            vCondiciones[14] := (not Completa) or ((not btnAgregarComprobantes.Enabled) and (not FCuotasEditable)) or ((cdsComprobantesRefinanciados.RecordCount = 0) and (FCodigoRefinanciacion > 20000)) or (not btnAgregarCuota.Enabled) or (cdsCuotas.RecordCount = 0) or (FCuotasTotal <> FComprobantesRefinanciadosTotal) or ((FCuotasTotal = FComprobantesRefinanciadosTotal) and (ValidarImportesEnCero = 0));

            vMensajes[01] := rsErrorFechaRefinanciacion;
            vMensajes[02] := rsErrorEntreRut;
            vMensajes[03] := rsErrorRutInvalido;
            vMensajes[04] := rsErrorSeleccionConvenio;
//            vMensajes[05] := rsErrorConvenioNoCN;
            vMensajes[05] := rsErrorRepresentanteLegalCliente;
            vMensajes[06] := rsErrorRepresentanteLegalCN;
            vMensajes[07] := rsErrorJPLNumero;
            vMensajes[08] := rsErrorJPLRegion;
            vMensajes[09] := rsErrorJPLComuna;
            vMensajes[10] := rsErrorTotalDemanda;
            vMensajes[11] := rsErrorComprobantes;
            vMensajes[12] := rsErrorCuotas;
            vMensajes[13] := rsErrorImportes;
            vMensajes[14] := rsErrorImportesCero;

            Result := ValidateControlsCN(vControles, vCondiciones, rsValidacionesTitulo, vMensajes, Self, Silencioso);
        except
            on e:exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
        Screen.Cursor := crDefault;
    end;
end;

procedure TRefinanciacionGestionForm.btnGrabarClick(Sender: TObject);
begin
    try
        Self.Enabled := False;
        if not FCuotasEditable then
            Grabar(False)
        else GrabarSoloCuotas(False);
    finally
        Self.Enabled := True;
    end;
end;

procedure TRefinanciacionGestionForm.btnHistorialClick(Sender: TObject);
begin
    Try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        RefinanciacionCuotasHistorialForm := TRefinanciacionCuotasHistorialForm.Create(Self);
        if RefinanciacionCuotasHistorialForm.Inicializar(FCodigoRefinanciacion, cdsCuotasCodigoCuota.AsInteger) then begin
            RefinanciacionCuotasHistorialForm.ShowModal;
        end;
    Finally
        if Assigned(RefinanciacionCuotasHistorialForm) then FreeAndNil(RefinanciacionCuotasHistorialForm);
        Screen.Cursor := crDefault;
    End;
end;

procedure TRefinanciacionGestionForm.btnHistorialPagosClick(Sender: TObject);
begin
    Try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        RefinanciacionCuotasPagosEfectivoForm := TRefinanciacionCuotasPagosEfectivoForm.Create(Self);
        if RefinanciacionCuotasPagosEfectivoForm.Inicializar(FCodigoRefinanciacion, cdsCuotasCodigoCuota.AsInteger) then begin
            RefinanciacionCuotasPagosEfectivoForm.ShowModal;
        end;
    Finally
        if Assigned(RefinanciacionCuotasPagosEfectivoForm) then FreeAndNil(RefinanciacionCuotasPagosEfectivoForm);
        Screen.Cursor := crDefault;
    End;
end;

procedure TRefinanciacionGestionForm.btnImprimirClick(Sender: TObject);
begin
    if ValidarRefinanciacion(True, False) and ValidarRefinanciacionesUnificadas then try				//SS_1051_PDO
        CambiarEstadoCursor(CURSOR_RELOJ, Self, FComponentes);

        try
            if (not FConsulta) and (FEditable or FCuotasEditable) then begin

                QueryExecute(DMConnections.BaseCAC, 'BEGIN TRANSACTION ImprimirRefinanciacion');

                if FEditable then begin
                    Grabar(True);
                end
                else begin
                    if FCuotasEditable then begin
                        GrabarSoloCuotas(True);
//                        MostrarCuotas;
                    end;
                end;
            end;

            RefinanciacionImpresionForm := TRefinanciacionImpresionForm.Create(Self);
            with RefinanciacionImpresionForm do begin
                TPanelMensajesForm.MuestraMensaje('Generando Documento Refinanciaci�n ...', True);
                if inicializar(FCodigoRefinanciacion, FCodigoRefinanciacionAnterior, FCodigoRefinanciacionUnificada) then Imprimir;			//SS_1051_PDO
            end;
        except
            on e:Exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
        if (not FConsulta) and (FEditable or FCuotasEditable) then begin
            QueryExecute(DMConnections.BaseCAC, 'IF @@TRANCOUNT > 0 BEGIN ROLLBACK TRANSACTION ImprimirRefinanciacion END');

            if FCodigoRefinanciacionAnterior = 0 then begin
                FCodigoRefinanciacion        := 0;
                lblNumRefinanciacion.Caption := '<Nueva>';
            end;
        end;
        
        if Assigned(RefinanciacionImpresionForm) then FreeAndNil(RefinanciacionImpresionForm);

        CambiarEstadoCursor(CURSOR_DEFECTO, Self, FComponentes);
        TPanelMensajesForm.OcultaPanel;
    end;
end;

procedure TRefinanciacionGestionForm.btnRestaurarDomicilioClick(Sender: TObject);
    var
        AuxDomicilio: TDomiciliosPersona;
begin
    try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        AuxDomicilio    := TDomiciliosPersona.Create(FPersona.CodigoPersona);
        FDatosDomicilio := AuxDomicilio.DomicilioPrincipal;

        with FDatosDomicilio do begin
            lblDomicilioCli.Caption :=
                QueryGetValue(
                    DMConnections.BaseCAC,
                    Format(SQLArmarDomicilioCompleto,
                           [ReemplazaPorApostrofe(Trim(Descripcion)),
                            NumeroCalle,
                            Piso,
                            ReemplazaPorApostrofe(Trim(Dpto)),
                            ReemplazaPorApostrofe(Trim(Detalle)),
                            CodigoComuna,
                            CodigoRegion]));
        end;
        
        FDomicilioModificado          := False;
        btnRestaurarDomicilio.Enabled := False;
    finally
        if Assigned(AuxDomicilio) then FreeAndNil(AuxDomicilio);
        
        Screen.Cursor := crDefault;
    end;
end;

procedure TRefinanciacionGestionForm.btnRestaurarTelefonoClick(Sender: TObject);
    var
        DatosTelefonosAux: TDatosTelefonos;
begin
    try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        DatosTelefonosAux.CodigoArea1 := 0;
        DatosTelefonosAux.CodigoArea1 := 0;

        FDatosTelefonos := DatosTelefonosAux;
        FTelefonosModificados := False;

        MostrarTelefonosCliente;
    finally
        Screen.Cursor := crDefault;
    end;
end;

procedure TRefinanciacionGestionForm.LimpiarRepresentanteLegalCN;
begin
    with FRepresentanteLegalCN do begin
        NumeroDocumento := EmptyStr;
        Nombre          := EmptyStr;
        Apellido        := EmptyStr;
        ApellidoMaterno := EmptyStr;
    end;

    lblRutRepLegCN.Caption    := EmptyStr;
    lblNombreRepLegCN.Caption := EmptyStr;
end;

procedure TRefinanciacionGestionForm.MostrarRepresentanteLegalCN;
begin
    with FRepresentanteLegalCN do begin
        lblRutRepLegCN.Caption    := NumeroDocumento;
        lblNombreRepLegCN.Caption := ArmarNombrePersona(PERSONERIA_FISICA, Nombre, Apellido, ApellidoMaterno);
    end;
end;

procedure TRefinanciacionGestionForm.LimpiarRepresentanteLegalCli;
begin
    with FRepresentanteLegalCli do begin
        NumeroDocumento := EmptyStr;
        Nombre          := EmptyStr;
        Apellido        := EmptyStr;
        ApellidoMaterno := EmptyStr;
    end;

    lblRutRepLegCliDesc.Enabled    := False;
    lblRutRepLegCli.Caption        := EmptyStr;
    lblNombreRepLegCliDesc.Enabled := False;
    lblNombreRepLegCli.Caption     := EmptyStr;

    btnCambiarRepLegCli.Enabled    := False;
end;

procedure TRefinanciacionGestionForm.MostrarRepresentanteLegalCli;
begin
    with FRepresentanteLegalCli do begin
        lblRutRepLegCliDesc.Enabled    := True;
        lblNombreRepLegCliDesc.Enabled := True;

        lblRutRepLegCli.Caption    := NumeroDocumento;
        lblNombreRepLegCli.Caption := ArmarNombrePersona(PERSONERIA_FISICA, Nombre, Apellido, ApellidoMaterno);
    end;
end;

procedure TRefinanciacionGestionForm.MostrarComprobantes;
begin
    if cdsComprobantesRefinanciados.Active then cdsComprobantesRefinanciados.Close;

    with spObtenerRefinanciacionComprobantes do begin
        Parameters.Refresh;
        Parameters.ParamByName('@CodigoRefinanciacion').Value := FCodigoRefinanciacion;
    end;

    with cdsComprobantesRefinanciados do try
        cdsComprobantesRefinanciados.AfterScroll := Nil;
        dsComprobantesRefinanciados.DataSet      := Nil;
        Open;

        FComprobantesRefinanciadosTotal := 0;

        while not Eof do begin
            FComprobantesRefinanciadosTotal :=
                FComprobantesRefinanciadosTotal +
                iif(FCodigoEstado in [REFINANCIACION_ESTADO_EN_TRAMITE, REFINANCIACION_ESTADO_ACEPTADA], cdsComprobantesRefinanciadosSaldoPendienteComprobante.AsLargeInt, cdsComprobantesRefinanciadosSaldoRefinanciado.AsLargeInt);
            Next;
        end;
    finally
        cdsComprobantesRefinanciados.AfterScroll := cdsComprobantesRefinanciadosAfterScroll;
        dsComprobantesRefinanciados.DataSet      := cdsComprobantesRefinanciados;
        First;
    end;

    if (FComprobantesRefinanciadosTotal = 0) and (FCodigoRefinanciacion < 20000) then begin
        FComprobantesRefinanciadosTotal := FTotalDeuda;
    end;

    btnAgregarComprobantes.Enabled := True and FEditable and (not FConsulta);
    btnEliminarComprobante.Enabled := (cdsComprobantesRefinanciados.RecordCount > 0) and FEditable and (not FConsulta);
    ActualizarTotales;
end;

procedure TRefinanciacionGestionForm.MostrarCuotas;
begin
    if cdsCuotas.Active then cdsCuotas.Close;

    with spObtenerRefinanciacionCuotas do begin
        Parameters.Refresh;
        Parameters.ParamByName('@CodigoRefinanciacion').Value := FCodigoRefinanciacion;
        Parameters.ParamByName('@CodigoCuota').Value          := Null;
        Parameters.ParamByName('@SinCuotasAnuladas').Value    := 0;
    end;

    with cdsCuotas do try
        dsCuotas.DataSet := Nil;
        Open;

        FCuotasTotal  := 0;
        FDeudaVencida := 0;
        FDeudaPagada  := 0;

        while not Eof do begin
            if not (cdsCuotasCodigoEstadoCuota.AsInteger in [REFINANCIACION_ESTADO_CUOTA_ANULADA, REFINANCIACION_ESTADO_CUOTA_ANULADA_POR_MORA, REFINANCIACION_ESTADO_CUOTA_ANULADA_POR_PROTESTO]) then begin
                FCuotasTotal := FCuotasTotal + cdsCuotasImporte.AsLargeInt;
                FDeudaPagada := FDeudaPagada + (cdsCuotasImporte.AsLargeInt - cdsCuotasSaldo.AsInteger);

                if cdsCuotasEstadoImpago.AsBoolean then begin
                    if cdsCuotasFechaCuota.AsDateTime < FAhora then begin
                        FDeudaVencida := FDeudaVencida + cdsCuotasSaldo.AsLargeInt;
                    end;
                end;
            end;

            Next;
        end;
    finally
        dsCuotas.DataSet := cdsCuotas;
        First;
    end;

    FCuotasCantidad := cdsCuotas.RecordCount;

    ActualizarBotonesCuotas(False);
    {
    btnAgregarCuota.Enabled    := (FEditable or FcuotasEditable) and (not FConsulta);
    btnEliminarCuota.Enabled   := (cdsCuotas.RecordCount > 0) and FEditable and (not FConsulta);
    btnEditarCuota.Enabled     := (cdsCuotas.RecordCount > 0) and (FEditable or FCuotasEditable) and (not FConsulta);
    btnSugerirImportes.Enabled := (cdsCuotas.RecordCount > 0) and FEditable and (not FConsulta);
    btnHistorial.Enabled       := (cdsCuotas.RecordCount > 0) and (FCodigoEstado <> 1);
    }
    ActualizarTotales;
end;

procedure TRefinanciacionGestionForm.MostrarPatentesPrecautoria;
begin
    if cdsPatentesPrecautoria.Active then cdsPatentesPrecautoria.Close;

    with spObtenerRefinanciacionPatentesPrecautoria do begin
        Parameters.Refresh;
        Parameters.ParamByName('@CodigoRefinanciacion').Value := FCodigoRefinanciacion;
    end;

    cdsPatentesPrecautoria.Open;

    //TASK_006_AUN_20170406-CU.COBO.COB.302: se elimina el tab Otros datos
    //btnAgregarPatentePrecautoria.Enabled  := (FCodigoCanalPago = 38) and FEditable and (not Fconsulta);
    //btnEliminarPatentePrecautoria.Enabled := (FCodigoCanalPago = 38) and (cdsPatentesPrecautoria.RecordCount > 0) and FEditable and (not Fconsulta);
end;

procedure TRefinanciacionGestionForm.btnCambiarDomicilioClick(Sender: TObject);
var
    AuxDomiciliosPersona: TDomiciliosPersona;
    AuxDomicilio: TDatosDomicilio;
begin
    try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        if FDomicilioModificado then
            AuxDomicilio := FDatosDomicilio
        else begin
            AuxDomiciliosPersona := TDomiciliosPersona.Create(FPersona.CodigoPersona);
            AuxDomicilio         := AuxDomiciliosPersona.DomicilioPrincipal;
            with AuxDomicilio do begin
                CodigoDomicilio     := -1;
                CodigoCalle         := -1;
                CodigoTipoCalle     := -1;
                CodigoSegmento      := -1;
                CodigoTipoDomicilio := 1;
                CalleDesnormalizada := Descripcion;
                NumeroCalleSTR      := IntToStr(NumeroCalle);
            end;
        end;

        formEditarDomicilio := TformEditarDomicilio.Create(Self);

        if formEditarDomicilio.Inicializar(' Editar Domicilio Principal',AuxDomicilio, 1, False, False, scModi) then begin
            formEditarDomicilio.FrameDomicilio.Pnl_Medio.Visible := True;
            formEditarDomicilio.ActiveControl := formEditarDomicilio.FrameDomicilio.cb_Regiones;

            if formEditarDomicilio.ShowModal = mrOk then begin
                FDatosDomicilio                 := formEditarDomicilio.RegistroDomicilio;
                FDatosDomicilio.CodigoCalle     := -1;
                FDatosDomicilio.CodigoSegmento  := -1;
                FDatosDomicilio.CodigoTipoCalle := -1;
                MostrarDomicilioCliente;
                FDomicilioModificado          := True;
                btnRestaurarDomicilio.Enabled := True;
            end;
        end;
    finally
        if Assigned(AuxDomiciliosPersona) then FreeAndNil(AuxDomiciliosPersona);
        if Assigned(formEditarDomicilio) then FreeAndNil(formEditarDomicilio);

        Screen.Cursor := crDefault;
    end;
end;

procedure TRefinanciacionGestionForm.MostrarDomicilioCliente;
begin
    with FDatosDomicilio do begin
        lblDomicilioCli.Caption :=
            QueryGetValue(
                DMConnections.BaseCAC,
                Format(SQLArmarDomicilioCompleto,
                       [ReemplazaPorApostrofe(Trim(Descripcion)),
                        NumeroCalle,
                        Piso,
                        ReemplazaPorApostrofe(Trim(Dpto)),
                        ReemplazaPorApostrofe(Trim(Detalle)),
                        CodigoComuna,
                        CodigoRegion]));
    end;

    btnCambiarDomicilio.Enabled   := FEditable and (not FConsulta);
    btnRestaurarDomicilio.Enabled := FEditable and (not FConsulta) and FDomicilioModificado;

    //lblDomicilioCli.Caption := QueryGetValue(DMConnections.BaseCAC, Format('SELECT dbo.ObtenerDescripcionDomicilioPrincipalPersona(%d)', [FPersona.CodigoPersona]));
end;

procedure TRefinanciacionGestionForm.MostrarTelefonosCliente;
    const
        FormatoTelefono = '(%d) %s';
    var
        NumeroTelefono1,
        NumeroTelefono2: String;
begin
    NumeroTelefono1 := EmptyStr;
    NumeroTelefono2 := EmptyStr;

    if not FTelefonosModificados then begin
        with spObtenerTelefonoPersonaSinFormato do try
            Parameters.Refresh;
            Parameters.ParamByName('@CodigoPersona').Value := FPersona.CodigoPersona;
            ExecProc;

            FDatosTelefonos.CodigoArea1 := iif(Parameters.ParamByName('@CodigoArea').Value = Null, 0, Parameters.ParamByName('@CodigoArea').Value);
            FDatosTelefonos.Telefono1   := iif(Parameters.ParamByName('@Valor').Value = Null, EmptyStr, Trim(VarToStr(Parameters.ParamByName('@Valor').Value)));
            FDatosTelefonos.CodigoArea2 := 0;
        finally
            if Active then Close;
        end;
    end;

    with FDatosTelefonos do begin
        if CodigoArea1 <> 0 then NumeroTelefono1 := Format(FormatoTelefono, [CodigoArea1, Telefono1]);
        if CodigoArea2 <> 0 then NumeroTelefono2 := Format(FormatoTelefono, [CodigoArea2, Telefono2]);

        lblTelefonos.Caption := NumeroTelefono1 + iif(NumeroTelefono2 <> EmptyStr, ',  ' + NumeroTelefono2, '');
    end;

    btnCambiarTelefono.Enabled   := FEditable and (not Fconsulta);
    btnRestaurarTelefono.Enabled := FEditable and (not FConsulta) and FTelefonosModificados;
end;

procedure TRefinanciacionGestionForm.nedtGastoNotarialChange(Sender: TObject);
begin
    if (nedtGastoNotarial.Value = 0) then begin
       lblDatosGastoNotarial.Caption := 'Sin Gastos Notariales.';
    end
    else lblDatosGastoNotarial.Caption := FormatFloat(FORMATO_IMPORTE, nedtGastoNotarial.Value);
end;

procedure TRefinanciacionGestionForm.Grabar(ProcesoPreImpresion: Boolean);
    resourcestring
        rs_ErrorEjecucionTitulo  = 'Error de Ejecuci�n';
        rs_ErrorEjecucionMensaje = 'Error en la ejecuci�n del procedimiento %s. C�digo de Error: %s.';			//SS_1051_PDO
        RS_GRABANDO              = 'Grabando';
        RS_PREPARANDO            = 'Preparando';
    var
        Validada: Boolean;
        Mensaje: String;
        PunteroCuotas,
        PunteroComprobantes: TBookmark;
begin
    if ValidarRefinanciacion(False, False) then try
        pnlDatosPrincipales.Enabled := False;
        pnlDatosDetalle.Enabled     := False;
        pnlAcciones.Enabled         := False;

        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        try
            TPanelMensajesForm.MuestraMensaje('Validando Datos ...', True);

            cdsCuotas.AfterScroll := nil;
            cdsComprobantesRefinanciados.AfterScroll := nil;

            PunteroCuotas := cdsCuotas.GetBookmark;
            PunteroComprobantes := cdsComprobantesRefinanciados.GetBookmark;

            cdsCuotas.DisableControls;
            cdsComprobantesRefinanciados.DisableControls;
            cdsPatentesPrecautoria.DisableControls;

            Validada := False;
            Validada := ValidarRefinanciacion(True, True);

            if not ProcesoPreImpresion then begin
                QueryExecute(DMConnections.BaseCAC, 'BEGIN TRANSACTION GrabarRefinanciacion');
                Mensaje := RS_GRABANDO;
            end
            else Mensaje := RS_PREPARANDO;

            TPanelMensajesForm.MuestraMensaje(Format('%s Datos Principales ...', [Mensaje]));
            with spActualizarRefinanciacionDatosCabecera do begin
                if Active then Close;
                Parameters.Refresh;

                with Parameters do begin
                    ParamByName('@CodigoRefinanciacion').Value          := iif(FCodigoRefinanciacion = 0, Null, FCodigoRefinanciacion);
                    ParamByName('@CodigoPersona').Value                 := FPersona.CodigoPersona;
                    ParamByName('@CodigoConvenio').Value                := FCodigoConvenio;
                    ParamByName('@CodigoTipoMedioPago').Value           := vcbTipoRefinanciacion.Value;
                    ParamByName('@Fecha').Value                         := FFechaRefinanciacion;
                    ParamByName('@CodigoEstado').Value                  := FCodigoEstado;
                    ParamByName('@TotalDeuda').Value                    := FComprobantesRefinanciadosTotal;
                    ParamByName('@TotalPagado').Value                   := 0;
                    ParamByName('@TotalDemanda').Value                  := nedtDemanda.Value;
                    ParamByName('@Notificacion').Value                  := iif(cbNotificacion.Checked, 1, 0);
                    ParamByName('@RepLegalCliNumeroDocumento').Value    := iif(FRepresentanteLegalCli.NumeroDocumento <> EmptyStr, FRepresentanteLegalCli.NumeroDocumento, Null);
                    ParamByName('@RepLegalCliNombre').Value             := iif(FRepresentanteLegalCli.Nombre <> EmptyStr, FRepresentanteLegalCli.Nombre, Null);
                    ParamByName('@RepLegalCliApellido').Value           := iif(FRepresentanteLegalCli.Apellido <> EmptyStr, FRepresentanteLegalCli.Apellido, Null);
                    ParamByName('@RepLegalCliApellidoMaterno').Value    := iif(FRepresentanteLegalCli.ApellidoMaterno <> EmptyStr, FRepresentanteLegalCli.ApellidoMaterno, Null);
                    ParamByName('@RepLegalCNNumeroDocumento').Value     := iif(FRepresentanteLegalCN.NumeroDocumento <> EmptyStr, FRepresentanteLegalCN.NumeroDocumento, Null);
                    ParamByName('@RepLegalCNNombre').Value              := iif(FRepresentanteLegalCN.Nombre <> EmptyStr, FRepresentanteLegalCN.Nombre, Null);
                    ParamByName('@RepLegalCNApellido').Value            := iif(FRepresentanteLegalCN.Apellido <> EmptyStr, FRepresentanteLegalCN.Apellido, Null);
                    ParamByName('@RepLegalCNApellidoMaterno').Value     := iif(FRepresentanteLegalCN.ApellidoMaterno <> EmptyStr, FRepresentanteLegalCN.ApellidoMaterno, Null);
                    ParamByName('@NumeroCausaRol').Value                := iif(edtCausaRol.Text <> EmptyStr, edtCausaRol.Text, Null);
                    ParamByName('@GastoNotarial').Value                 := nedtGastoNotarial.ValueInt;
                    ParamByName('@Usuario').Value                       := UsuarioSistema;
                    ParamByName('@Validada').Value                      := Validada;
                    ParamByName('@JPLNumero').Value                     := nedtJPLNro.ValueInt;
                    ParamByName('@JPLCodigoPais').Value                 := PAIS_CHILE;
                    ParamByName('@JPLCodigoRegion').Value               := vcbJPLRegion.Value;
                    ParamByName('@JPLCodigoComuna').Value               := vcbJPLComuna.Value;
                    ParamByName('@CodigoRefinanciacionUnificada').Value := FCodigoRefinanciacionUnificada;      //SS_1051_PDO
                    ParamByName('@NombrePlanRefinanciacion').Value      := lblNombrePlan.Caption;
                end;

                ExecProc;

                if Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
                    raise EErrorExceptionCN.Create(
                        rs_ErrorEjecucionTitulo,
                        Format(rs_ErrorEjecucionMensaje,
                               [ProcedureName,
                                Parameters.ParamByName('@RETURN_VALUE').Value]));
                end;

                if FCodigoRefinanciacion = 0 then begin
                    FCodigoRefinanciacion := Parameters.ParamByName('@CodigoRefinanciacion').Value;
                    lblNumRefinanciacion.Caption := IntToStr(FCodigoRefinanciacion);
                end;
            end;

            TPanelMensajesForm.MuestraMensaje(Format('%s Comprobantes Refinanciados ...', [Mensaje]));
            if cdsComprobantesRefinanciados.RecordCount > 0 then begin
                with cdsComprobantesRefinanciados do try
                    AfterScroll := Nil;
                    DisableControls;
                    First;

                    while not Eof do begin
                        with spActualizarRefinanciacionComprobantes do begin
                            if Active then Close;
                            Parameters.Refresh;
                            with Parameters do begin
                                ParamByName('@CodigoRefinanciacion').Value := FCodigoRefinanciacion;
                                ParamByName('@TipoComprobante').Value      := cdsComprobantesRefinanciadosTipoComprobante.AsString;
                                ParamByName('@NumeroComprobante').Value    := cdsComprobantesRefinanciadosNumeroComprobante.AsLargeInt;
                                ParamByName('@SaldoPendiente').Value       := cdsComprobantesRefinanciadosSaldoPendienteComprobante.AsLargeInt;
                                ParamByName('@BorrarExistentes').Value     := iif(cdsComprobantesRefinanciados.RecNo = 1, 1, 0);
                                ParamByName('@Usuario').Value              := UsuarioSistema;
                            end;

                            ExecProc;

                            if Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
                                raise EErrorExceptionCN.Create(
                                    rs_ErrorEjecucionTitulo,
                                    Format(rs_ErrorEjecucionMensaje,
                                           [ProcedureName,
                                            Parameters.ParamByName('@RETURN_VALUE').Value]));
                            end;
                        end;

                        Next;
                    end;
                finally
                    cdsComprobantesRefinanciados.AfterScroll :=  cdsComprobantesRefinanciadosAfterScroll;
                    EnableControls;
                end;
            end
            else begin
                with spActualizarRefinanciacionComprobantes do begin
                    if Active then Close;
                    Parameters.Refresh;
                    with Parameters do begin
                        ParamByName('@CodigoRefinanciacion').Value := FCodigoRefinanciacion;
                        ParamByName('@TipoComprobante').Value      := Null;
                        ParamByName('@NumeroComprobante').Value    := Null;
                        ParamByName('@SaldoPendiente').Value       := Null;
                        ParamByName('@BorrarExistentes').Value     := 1;
                        ParamByName('@Usuario').Value              := Null;
                    end;

                    ExecProc;
                end;
            end;

            TPanelMensajesForm.MuestraMensaje(Format('%s Cuotas Refinanciaci�n ...', [Mensaje]));
            if cdsCuotas.RecordCount > 0 then begin
                with cdsCuotas do try
                    DisableControls;
                    First;

                    while not Eof do begin
                        with spActualizarRefinanciacionCuotas do begin
                            if Active then Close;
                            Parameters.Refresh;

                            with Parameters do begin
                                ParamByName('@CodigoRefinanciacion').Value   := FCodigoRefinanciacion;
                                ParamByName('@FechaCuota').Value             := cdsCuotasFechaCuota.AsDateTime;
                                ParamByName('@Importe').Value                := cdsCuotasImporte.AsLargeInt;
                                ParamByName('@Saldo').Value                  := cdsCuotasImporte.AsLargeInt;
                                ParamByName('@CodigoEstadoCuota').Value      := cdsCuotasCodigoEstadoCuota.AsInteger;
                                ParamByName('@CodigoFormaPago').Value        := cdsCuotasCodigoFormaPago.AsInteger;
                                ParamByName('@TitularNombre').Value          := iif(cdsCuotasTitularNombre.AsString <> EmptyStr, cdsCuotasTitularNombre.AsString, Null);
                                ParamByName('@TitularTipoDocumento').Value   := iif(cdsCuotasTitularTipoDocumento.AsString <> EmptyStr, cdsCuotasTitularTipoDocumento.AsString, Null);
                                ParamByName('@TitularNumeroDocumento').Value := iif(cdsCuotasTitularNumeroDocumento.AsString <> EmptyStr, cdsCuotasTitularNumeroDocumento.AsString, Null);
                                ParamByName('@DocumentoNumero').Value        := iif(cdsCuotasDocumentoNumero.AsString <> EmptyStr, cdsCuotasDocumentoNumero.AsString, Null);
                                ParamByName('@CodigoBanco').Value            := iif(cdsCuotasCodigoBanco.AsInteger <> 0, cdsCuotasCodigoBanco.AsInteger, Null);
                                ParamByName('@CodigoTarjeta').Value          := iif(cdsCuotasCodigoTarjeta.AsInteger <> 0, cdsCuotasCodigoTarjeta.AsInteger, Null);
                                ParamByName('@TarjetaCuotas').Value          := iif(cdsCuotasTarjetaCuotas.AsInteger <> 0, cdsCuotasTarjetaCuotas.AsInteger, Null);
                                ParamByName('@BorrarExistentes').Value       := iif(cdsCuotas.Recno = 1, 1, 0);
                                ParamByName('@Usuario').Value                := UsuarioSistema;
                                ParamByName('@CodigoCuota').Value            := Null;
                                ParamByName('@CodigoCuotaAntecesora').Value  := Null;
                            end;

                            ExecProc;

                            if Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
                                raise EErrorExceptionCN.Create(
                                    rs_ErrorEjecucionTitulo,
                                    Format(rs_ErrorEjecucionMensaje,
                                           [ProcedureName,
                                            Parameters.ParamByName('@RETURN_VALUE').Value]));
                            end;
                        end;

                        Next;
                    end;
                finally
                    EnableControls;
                end;
            end
            else begin
                with spActualizarRefinanciacionCuotas do begin
                    if Active then Close;
                    Parameters.Refresh;
                    with Parameters do begin
                        ParamByName('@CodigoRefinanciacion').Value   := FCodigoRefinanciacion;
                        ParamByName('@FechaCuota').Value             := Null;
                        ParamByName('@Importe').Value                := Null;
                        ParamByName('@Saldo').Value                  := Null;
                        ParamByName('@CodigoEstadoCuota').Value      := Null;
                        ParamByName('@CodigoFormaPago').Value        := Null;
                        ParamByName('@TitularNombre').Value          := Null;
                        ParamByName('@TitularTipoDocumento').Value   := Null;
                        ParamByName('@TitularNumeroDocumento').Value := Null;
                        ParamByName('@DocumentoNumero').Value        := Null;
                        ParamByName('@CodigoBanco').Value            := Null;
                        ParamByName('@CodigoTarjeta').Value          := Null;
                        ParamByName('@TarjetaCuotas').Value          := Null;
                        ParamByName('@BorrarExistentes').Value       := 1;
                        ParamByName('@Usuario').Value                := Null;
                        ParamByName('@CodigoCuota').Value            := Null;
                        ParamByName('@CodigoCuotaAntecesora').Value  := Null;                                                
                    end;

                    ExecProc;
                end;
            end;

            TPanelMensajesForm.MuestraMensaje(Format('%s Datos Patentes en Precautoria ...', [Mensaje]));
            if cdsPatentesPrecautoria.RecordCount > 0 then begin
                with cdsPatentesPrecautoria do try
                    DisableControls;
                    First;

                    while not Eof do begin
                        with spActualizarRefinanciacionPatentesPrecautoria do begin
                            if Active then Close;
                            Parameters.Refresh;

                            with Parameters do begin
                                ParamByName('@CodigoRefinanciacion').Value := FCodigoRefinanciacion;
                                ParamByName('@Patente').Value              := cdsPatentesPrecautoriaPatente.AsString;
                                ParamByName('@MarcaModeloColor').Value     := iif(cdsPatentesPrecautoriaMarcaModeloColor.AsString <> EmptyStr, cdsPatentesPrecautoriaMarcaModeloColor.AsString, Null);
                                ParamByName('@Convenio').Value             := iif(cdsPatentesPrecautoriaConvenio.AsString <> EmptyStr, cdsPatentesPrecautoriaConvenio.AsString, Null);
                                ParamByName('@CodigoEstadoCuenta').Value   := cdsPatentesPrecautoriaCodigoEstadoCuenta.AsInteger;
                                ParamByName('@BorrarExistentes').Value     := iif(cdsPatentesPrecautoria.RecNo = 1, 1, 0);
                                ParamByName('@Usuario').Value              := UsuarioSistema;
                            end;

                            ExecProc;

                            if Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
                                raise EErrorExceptionCN.Create(
                                    rs_ErrorEjecucionTitulo,
                                    Format(rs_ErrorEjecucionMensaje,
                                           [ProcedureName,
                                            Parameters.ParamByName('@RETURN_VALUE').Value]));
                            end;
                        end;

                        Next;
                    end;
                finally
                    EnableControls;
                end;
            end
            else begin
                with spActualizarRefinanciacionPatentesPrecautoria do begin
                    if Active then Close;
                    Parameters.Refresh;
                    with Parameters do begin
                        ParamByName('@CodigoRefinanciacion').Value := FCodigoRefinanciacion;
                        ParamByName('@Patente').Value              := Null;
                        ParamByName('@MarcaModeloColor').Value     := Null;
                        ParamByName('@Convenio').Value             := Null;
                        ParamByName('@CodigoEstadoCuenta').Value   := Null;
                        ParamByName('@BorrarExistentes').Value     := 1;
                        ParamByName('@Usuario').Value              := Null;
                    end;

                    ExecProc;
                end;
            end;

            TPanelMensajesForm.MuestraMensaje(Format('%s Datos Domicilio ...', [Mensaje]));
            with spActualizarRefinanciacionDomicilio do begin
                if Active then Close;
                Parameters.Refresh;

                if FDomicilioModificado then begin
                    with Parameters do begin
                        ParamByName('@CodigoRefinanciacion').Value := FCodigoRefinanciacion;
                        ParamByName('@Calle').Value                := FDatosDomicilio.Descripcion;
                        ParamByName('@Numero').Value               := FDatosDomicilio.NumeroCalle;
                        ParamByName('@Piso').Value                 := iif(FDatosDomicilio.Piso = 0, Null, FDatosDomicilio.Piso);
                        ParamByName('@Dpto').Value                 := iif(FDatosDomicilio.Dpto = EmptyStr, Null, FDatosDomicilio.Dpto);
                        ParamByName('@Detalle').Value              := iif(FDatosDomicilio.Detalle = EmptyStr, Null, FDatosDomicilio.Detalle);
                        ParamByName('@CodigoPostal').Value         := iif(FDatosDomicilio.CodigoPostal = EmptyStr, Null, FDatosDomicilio.CodigoPostal);
                        ParamByName('@CodigoPais').Value           := FDatosDomicilio.CodigoPais;
                        ParamByName('@CodigoRegion').Value         := FDatosDomicilio.CodigoRegion;
                        ParamByName('@CodigoComuna').Value         := FDatosDomicilio.CodigoComuna;
                        ParamByName('@BorrarExistentes').Value     := 1;
                        ParamByName('@Usuario').Value              := UsuarioSistema;
                    end;
                end
                else begin
                    with Parameters do begin
                        ParamByName('@CodigoRefinanciacion').Value := FCodigoRefinanciacion;
                        ParamByName('@Calle').Value                := Null;
                        ParamByName('@Numero').Value               := Null;
                        ParamByName('@Piso').Value                 := Null;
                        ParamByName('@Dpto').Value                 := Null;
                        ParamByName('@Detalle').Value              := Null;
                        ParamByName('@CodigoPostal').Value         := Null;
                        ParamByName('@CodigoPais').Value           := Null;
                        ParamByName('@CodigoRegion').Value         := Null;
                        ParamByName('@CodigoComuna').Value         := Null;
                        ParamByName('@BorrarExistentes').Value     := 1;
                        ParamByName('@Usuario').Value              := Null;
                    end;
                end;

                ExecProc;

                if Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
                    raise EErrorExceptionCN.Create(
                        rs_ErrorEjecucionTitulo,
                        Format(rs_ErrorEjecucionMensaje,
                               [ProcedureName,
                                Parameters.ParamByName('@RETURN_VALUE').Value]));
                end;
            end;

            TPanelMensajesForm.MuestraMensaje(Format('%s Datos Tel�fonos ...', [Mensaje]));
            with spActualizarRefinanciacionTelefonos do begin
                if Active then Close;
                Parameters.Refresh;

                if FTelefonosModificados then begin
                    with Parameters do begin
                        ParamByName('@CodigoRefinanciacion').Value := FCodigoRefinanciacion;
                        ParamByName('@CodigoArea').Value           := FDatosTelefonos.CodigoArea1;
                        ParamByName('@NumeroTelefono').Value       := FDatosTelefonos.Telefono1;
                        ParamByName('@BorrarExistentes').Value     := 1;
                        ParamByName('@Usuario').Value              := UsuarioSistema;
                    end;

                    ExecProc;

                    if Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
                        raise EErrorExceptionCN.Create(
                            rs_ErrorEjecucionTitulo,
                            Format(rs_ErrorEjecucionMensaje,
                                   [ProcedureName,
                                    Parameters.ParamByName('@RETURN_VALUE').Value]));
                    end;

                    if FDatosTelefonos.CodigoArea2 <> 0 then begin
                        if Active then Close;
                        Parameters.Refresh;

                        with Parameters do begin
                            ParamByName('@CodigoRefinanciacion').Value := FCodigoRefinanciacion;
                            ParamByName('@CodigoArea').Value           := FDatosTelefonos.CodigoArea2;
                            ParamByName('@NumeroTelefono').Value       := FDatosTelefonos.Telefono2;
                            ParamByName('@BorrarExistentes').Value     := 0;
                            ParamByName('@Usuario').Value              := UsuarioSistema;
                        end;

                        ExecProc;

                        if Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
                            raise EErrorExceptionCN.Create(
                                rs_ErrorEjecucionTitulo,
                                Format(rs_ErrorEjecucionMensaje,
                                       [ProcedureName,
                                        Parameters.ParamByName('@RETURN_VALUE').Value]));
                        end;
                    end;
                end
                else begin
                    with Parameters do begin
                        ParamByName('@CodigoRefinanciacion').Value := FCodigoRefinanciacion;
                        ParamByName('@CodigoArea').Value           := Null;
                        ParamByName('@NumeroTelefono').Value       := Null;
                        ParamByName('@BorrarExistentes').Value     := 1;
                        ParamByName('@Usuario').Value              := Null;
                    end;

                    ExecProc;
                end;
            end;

            //INICIO	: TASK_124_CFU_20170320
            with spActualizarEstadoConvenio, Parameters do begin
                Parameters.Refresh;
                ParamByName('@CodigoConvenio').Value	:= vcbConvenios.Value;
                ParamByName('@EstadoConvenio').Value	:= CONST_ESTADO_CONVENIO_REFINANCIADO;
                ParamByName('@Usuario').Value			:= UsuarioSistema;
                ExecProc;
                if Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
                    raise EErrorExceptionCN.Create(
                        rs_ErrorEjecucionTitulo,
                        Format(rs_ErrorEjecucionMensaje,
                               [ProcedureName,
                                Parameters.ParamByName('@RETURN_VALUE').Value]));
                end;
            end;
            //TERMINO	: TASK_124_CFU_20170320

            if not ProcesoPreImpresion then begin
                QueryExecute(DMConnections.BaseCAC, 'IF @@TRANCOUNT > 0 BEGIN COMMIT TRANSACTION GrabarRefinanciacion END');
                ModalResult := mrOk;
            end;
        except
            on e:exception do begin
                TPanelMensajesForm.OcultaPanel;
                if not ProcesoPreImpresion then begin
                    QueryExecute(DMConnections.BaseCAC, 'IF @@TRANCOUNT > 0 BEGIN ROLLBACK TRANSACTION GrabarRefinanciacion END');
                end;
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
        TPanelMensajesForm.OcultaPanel;
        Screen.Cursor := crDefault;
        if ModalResult <> mrOk then begin
            if Assigned(PunteroCuotas) then begin
                if cdsCuotas.BookmarkValid(PunteroCuotas) then cdsCuotas.GotoBookmark(PunteroCuotas);
            end;

            if Assigned(PunteroComprobantes) then begin
                if cdsComprobantesRefinanciados.BookmarkValid(PunteroComprobantes) then cdsComprobantesRefinanciados.GotoBookmark(PunteroComprobantes);
            end;

            cdsCuotas.AfterScroll := cdsCuotasAfterScroll;
            cdsComprobantesRefinanciados.AfterScroll := cdsComprobantesRefinanciadosAfterScroll;

            cdsCuotas.EnableControls;
            cdsComprobantesRefinanciados.EnableControls;
            cdsPatentesPrecautoria.EnableControls;

            pnlDatosPrincipales.Enabled := True;
            pnlDatosDetalle.Enabled     := True;
            pnlAcciones.Enabled         := True;
        end;

        cdsCuotas.FreeBookmark(PunteroCuotas);
        cdsComprobantesRefinanciados.FreeBookmark(PunteroComprobantes);
    end;
end;

procedure TRefinanciacionGestionForm.ActualizarBotonesCuotas(DeshabilitarTodos: Boolean);
var
    ExistenCuotas,
    CuotaNueva,
    CuotaModificada,
    CuotaEditable,
    CuotaAnulable,
    CuotaEnEfectivoPagare,
    PermisoAgregarCuota,
    PermisoEditarCuota,
    PermisoEliminarCuota,
    PermisoAnularCuota: Boolean;
    MontoDeuda: Int64;
begin
    ExistenCuotas         := cdsCuotas.RecordCount > 0;
    CuotaNueva            := cdsCuotasCuotaNueva.AsBoolean;
    CuotaModificada       := cdsCuotasCuotaModificada.AsBoolean;
    CuotaEditable         := not (cdsCuotasCodigoEstadoCuota.AsInteger in [REFINANCIACION_ESTADO_CUOTA_PAGADA, REFINANCIACION_ESTADO_CUOTA_ANULADA, REFINANCIACION_ESTADO_CUOTA_ANULADA_POR_MORA, REFINANCIACION_ESTADO_CUOTA_DEPOSITADO]);
    CuotaAnulable         := CuotaEditable and (cdsCuotasImporte.AsInteger = cdsCuotasSaldo.AsInteger);
    CuotaEnEfectivoPagare := cdsCuotasCodigoFormaPago.AsInteger in [FORMA_PAGO_EFECTIVO, FORMA_PAGO_PAGARE];

    PermisoAgregarCuota  := ExisteAcceso('add_cuota_gestion_ref');
    PermisoEditarCuota   := ExisteAcceso('editar_cuota_gestion_ref');
    PermisoEliminarCuota := ExisteAcceso('eliminar_cuota_gestion_ref');
    PermisoAnularCuota   := ExisteAcceso('anular_cuota_gestion_ref');

    btnAgregarCuota.Enabled     := (not FConsulta) and (FEditable or (FcuotasEditable and PermisoAgregarCuota));
    btnEliminarCuota.Enabled    := (not DeshabilitarTodos) and (not FConsulta) and (ExistenCuotas) and (FEditable or (FCuotasEditable and PermisoEliminarCuota and CuotaNueva));
    btnEditarCuota.Enabled      := (not DeshabilitarTodos) and (not FConsulta) and (ExistenCuotas) and (FEditable or (FCuotasEditable and PermisoEditarCuota and CuotaEditable));
    btnAnularCuota.Enabled      := (not DeshabilitarTodos) and (not FConsulta) and (ExistenCuotas) and (FCuotasEditable and PermisoAnularCuota) and CuotaEditable and CuotaAnulable and (not (CuotaModificada or CuotaNueva));
    btnSugerirImportes.Enabled  := (not DeshabilitarTodos) and (not FConsulta) and (ExistenCuotas) and FEditable;
    btnHistorial.Enabled        := ExistenCuotas and (not Fconsulta) and (FCodigoEstado <> REFINANCIACION_ESTADO_EN_TRAMITE);
//    btnImprimirOriginal.Enabled := not (FCodigoEstado in [REFINANCIACION_ESTADO_EN_TRAMITE, REFINANCIACION_ESTADO_ANULADA]);
    btnHistorialPagos.Enabled   := ExistenCuotas and (not FConsulta) and (FCodigoEstado <> REFINANCIACION_ESTADO_EN_TRAMITE);
    btnSugerirImporteDemanda.Visible := ExisteAcceso('boton_sugerir_importe_ref');  // SS_1245_CQU_20150224
    //TASK_006_AUN_20170406-CU.COBO.COB.302
    MontoDeuda := FComprobantesRefinanciadosTotal - FDeudaPagada;
    btnDefinirPlan.Enabled     := (not FConsulta) and (FEditable or (FcuotasEditable and PermisoAgregarCuota)) and
                                   (MontoDeuda > 0) and
                                   (FCodigoEstado = REFINANCIACION_ESTADO_EN_TRAMITE);
end;
{
procedure TRefinanciacionGestionForm.AsignarDatosCuota(Alta: Boolean);
begin
    with FEdicionDatos do begin
        if Alta then begin
            CodigoEntradaUsuario   := EmptyStr;
            TitularNombre          := EmptyStr;
            TitularTipoDocumento   := EmptyStr;
            TitularNumeroDocumento := EmptyStr;
            CodigoBanco            := 0;
            DocumentoNumero        := EmptyStr;
            FechaCuota             := NullDate;
            CodigoTarjeta          := 0;
            TarjetaCuotas          := 0;
            Importe                := 0;
            Saldo                  := 0;
            CuotaNueva             := True;
            CuotaAntecesora        := 0;
        end
        else begin
            CodigoEntradaUsuario   := cdsCuotasCodigoEntradaUsuario.AsString;
            TitularNombre          := cdsCuotasTitularNombre.AsString;
            TitularTipoDocumento   := cdsCuotasTitularTipoDocumento.AsString;
            TitularNumeroDocumento := cdsCuotasTitularNumeroDocumento.AsString;
            CodigoBanco            := cdsCuotasCodigoBanco.AsInteger;
            DocumentoNumero        := cdsCuotasDocumentoNumero.AsString;
            FechaCuota             := cdsCuotasFechaCuota.AsDateTime;
            CodigoTarjeta          := cdsCuotasCodigoTarjeta.AsInteger;
            TarjetaCuotas          := cdsCuotasTarjetaCuotas.AsInteger;
            Importe                := cdsCuotasImporte.AsInteger;
            Saldo                  := cdsCuotasSaldo.AsInteger;
            CuotaNueva             := cdsCuotasCuotaNueva.AsBoolean;
            CuotaAntecesora        := cdsCuotasCodigoCuotaAntecesora.AsInteger;
        end;
    end;
end;
}
procedure TRefinanciacionGestionForm.GrabarSoloCuotas(ProcesoPreImpresion: Boolean);
    resourcestring
        rs_Titulo                = 'Actualizaci�n Cuotas';
        rs_Mensaje               = '� Est� seguro de Grabar los cambios Realizados en las Cuotas ?';
        rs_ErrorEjecucionTitulo  = 'Error de Ejecuci�n';
        rs_ErrorEjecucionMensaje = 'Error en la ejecuci�n del procedimiento %s. C�digo de Error: %d.';
        RS_GRABANDO              = 'Actualizando';
        RS_PREPARANDO            = 'Preparando';
    var
        Mensaje: String;
        PunteroCuotas: TBookmark;
begin
    if ValidarRefinanciacion(True, False) then begin
        if (ProcesoPreImpresion ) or (ShowMsgBoxCN(rs_Titulo, rs_Mensaje, MB_ICONQUESTION, Self) = mrOk) then try
            pnlDatosPrincipales.Enabled := False;
            pnlDatosDetalle.Enabled     := False;
            pnlAcciones.Enabled         := False;

            Screen.Cursor := crHourGlass;
            Application.ProcessMessages;



            try
                if not ProcesoPreImpresion then begin
                    QueryExecute(DMConnections.BaseCAC, 'BEGIN TRANSACTION ActualizarCuotas');
                    Mensaje := RS_GRABANDO;
                end
                else Mensaje := RS_PREPARANDO;

                TPanelMensajesForm.MuestraMensaje(Format('%s Cuotas ...', [Mensaje]), True);

                PunteroCuotas := cdsCuotas.GetBookmark;
                cdsCuotas.DisableControls;
                cdsCuotas.AfterScroll := Nil;

                cdsCuotas.First;
                while not cdsCuotas.Eof do begin
                     if cdsCuotasCuotaNueva.AsBoolean then begin
                        with spActualizarRefinanciacionCuotas do begin
                            if Active then Close;
                            Parameters.Refresh;

                            with Parameters do begin
                                ParamByName('@CodigoRefinanciacion').Value   := FCodigoRefinanciacion;
                                ParamByName('@FechaCuota').Value             := cdsCuotasFechaCuota.AsDateTime;
                                ParamByName('@Importe').Value                := cdsCuotasImporte.AsInteger;
                                ParamByName('@Saldo').Value                  := cdsCuotasImporte.AsInteger;
                                ParamByName('@CodigoEstadoCuota').Value      := cdsCuotasCodigoEstadoCuota.AsInteger;
                                ParamByName('@CodigoFormaPago').Value        := cdsCuotasCodigoFormaPago.AsInteger;
                                ParamByName('@TitularNombre').Value          := iif(cdsCuotasTitularNombre.AsString <> EmptyStr, cdsCuotasTitularNombre.AsString, Null);
                                ParamByName('@TitularTipoDocumento').Value   := iif(cdsCuotasTitularTipoDocumento.AsString <> EmptyStr, cdsCuotasTitularTipoDocumento.AsString, Null);
                                ParamByName('@TitularNumeroDocumento').Value := iif(cdsCuotasTitularNumeroDocumento.AsString <> EmptyStr, cdsCuotasTitularNumeroDocumento.AsString, Null);
                                ParamByName('@DocumentoNumero').Value        := iif(cdsCuotasDocumentoNumero.AsString <> EmptyStr, cdsCuotasDocumentoNumero.AsString, Null);
                                ParamByName('@CodigoBanco').Value            := iif(cdsCuotasCodigoBanco.AsInteger <> 0, cdsCuotasCodigoBanco.AsInteger, Null);
                                ParamByName('@CodigoTarjeta').Value          := iif(cdsCuotasCodigoTarjeta.AsInteger <> 0, cdsCuotasCodigoTarjeta.AsInteger, Null);
                                ParamByName('@TarjetaCuotas').Value          := iif(cdsCuotasTarjetaCuotas.AsInteger <> 0, cdsCuotasTarjetaCuotas.AsInteger, Null);
                                ParamByName('@BorrarExistentes').Value       := 0;
                                ParamByName('@Usuario').Value                := UsuarioSistema;
                                ParamByName('@CodigoCuota').Value            := Null;
                                ParamByName('@CodigoCuotaAntecesora').Value  := iif(cdsCuotasCodigoCuotaAntecesora.AsInteger > 0, cdsCuotasCodigoCuotaAntecesora.AsInteger, Null);
                            end;

                            ExecProc;

                            if Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
                                raise EErrorExceptionCN.Create(
                                    rs_ErrorEjecucionTitulo,
                                    Format(rs_ErrorEjecucionMensaje,
                                           [ProcedureName,
                                            Parameters.ParamByName('@RETURN_VALUE').Value]));
                            end;
                        end;
                    end
                    else begin
                        if cdsCuotasCuotaModificada.AsBoolean then begin
                            with spObtenerRefinanciacionCuotasDatosCuota do begin
                                if Active then Close;
                                Parameters.Refresh;

                                with Parameters do begin
                                    ParamByName('@CodigoRefinanciacion').Value := FCodigoRefinanciacion;
                                    ParamByName('@codigoCuota').Value          := cdsCuotasCodigoCuota.AsInteger;
                                end;

                                Open;
                            end;

                            if (spObtenerRefinanciacionCuotasDatosCuota.FieldByName('CodigoCuotaAntecesora').AsInteger <> cdsCuotasCodigoCuotaAntecesora.AsInteger) or
                                (spObtenerRefinanciacionCuotasDatosCuota.FieldByName('Importe').AsInteger <> cdsCuotasImporte.AsInteger) or
                                (spObtenerRefinanciacionCuotasDatosCuota.FieldByName('Saldo').AsInteger <> cdsCuotasSaldo.AsInteger) then begin
                                with spActualizarRefinanciacionCuotas do begin
                                    if Active then Close;
                                    Parameters.Refresh;

                                    with Parameters do begin
                                        ParamByName('@CodigoRefinanciacion').Value   := FCodigoRefinanciacion;
                                        ParamByName('@FechaCuota').Value             := Null;
                                        ParamByName('@Importe').Value                := cdsCuotasImporte.AsLargeInt;
                                        ParamByName('@Saldo').Value                  := cdsCuotasSaldo.AsLargeInt;
                                        ParamByName('@CodigoEstadoCuota').Value      := Null;
                                        ParamByName('@CodigoFormaPago').Value        := Null;
                                        ParamByName('@TitularNombre').Value          := Null;
                                        ParamByName('@TitularTipoDocumento').Value   := Null;
                                        ParamByName('@TitularNumeroDocumento').Value := Null;
                                        ParamByName('@DocumentoNumero').Value        := Null;
                                        ParamByName('@CodigoBanco').Value            := Null;
                                        ParamByName('@CodigoTarjeta').Value          := Null;
                                        ParamByName('@TarjetaCuotas').Value          := Null;
                                        ParamByName('@BorrarExistentes').Value       := 0;
                                        ParamByName('@Usuario').Value                := UsuarioSistema;
                                        ParamByName('@CodigoCuota').Value            := cdsCuotasCodigoCuota.AsInteger;
                                        ParamByName('@CodigoCuotaAntecesora').Value  := iif(cdsCuotasCodigoCuotaAntecesora.AsInteger > 0, cdsCuotasCodigoCuotaAntecesora.AsInteger, Null);
                                    end;

                                    ExecProc;

                                    if Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
                                        raise EErrorExceptionCN.Create(
                                            rs_ErrorEjecucionTitulo,
                                            Format(rs_ErrorEjecucionMensaje,
                                                   [ProcedureName,
                                                    Parameters.ParamByName('@RETURN_VALUE').Value]));
                                    end;
                                end;
                            end;

                            if (spObtenerRefinanciacionCuotasDatosCuota.FieldByName('CodigoEstadoCuota').AsInteger <> cdsCuotasCodigoEstadoCuota.AsInteger) then begin
                                with spActualizarRefinanciacionCuotasEstados do begin
                                    if Active then Close;
                                    Parameters.Refresh;

                                    with Parameters do begin
                                        ParamByName('@CodigoRefinanciacion').Value := FCodigoRefinanciacion;
                                        ParamByName('@CodigoCuota').Value          := cdsCuotasCodigoCuota.AsInteger;
                                        ParamByName('@CodigoEstadoCuota').Value    := cdsCuotasCodigoEstadoCuota.AsInteger;
                                        ParamByName('@Usuario').Value              := UsuarioSistema;
                                    end;

                                    ExecProc;

                                    if Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
                                        raise EErrorExceptionCN.Create(
                                            rs_ErrorEjecucionTitulo,
                                            Format(rs_ErrorEjecucionMensaje,
                                                   [ProcedureName,
                                                    Parameters.ParamByName('@RETURN_VALUE').Value]));
                                    end;
                                end;
                            end;
                        end;
                    end;

                    cdsCuotas.Next;
                end;

                RefinanciacionRenumerarCuotas(DMConnections.BaseCAC, FCodigoRefinanciacion);    // SS_1050_PDO_20120605

                if not ProcesoPreImpresion then begin
                    QueryExecute(DMConnections.BaseCAC, 'IF @@TRANCOUNT > 0 BEGIN COMMIT TRANSACTION ActualizarCuotas END');
                    ModalResult := mrOk;
                end;
            except
                on e:Exception do begin
                    TPanelMensajesForm.OcultaPanel;
                    if not ProcesoPreImpresion then begin
                        QueryExecute(DMConnections.BaseCAC, 'IF @@TRANCOUNT > 0 BEGIN ROLLBACK TRANSACTION ActualizarCuotas END');
                    end;
                    ShowMsgBoxCN(e, Self);
                end;
            end;
        finally
            TPanelMensajesForm.OcultaPanel;
            Screen.Cursor := crDefault;

            if ModalResult <> mrOk then begin
                pnlDatosPrincipales.Enabled := True;
                pnlDatosDetalle.Enabled     := True;
                pnlAcciones.Enabled         := True;

                if Assigned(PunteroCuotas) then begin
                    if cdsCuotas.BookmarkValid(PunteroCuotas) then cdsCuotas.GotoBookmark(PunteroCuotas);
                end;

                cdsCuotas.AfterScroll := cdsCuotasAfterScroll;
                cdsCuotas.EnableControls;
            end;

            cdsCuotas.FreeBookmark(PunteroCuotas);
        end;
    end;
end;

procedure TRefinanciacionGestionForm.btnImprimirOriginalClick(Sender: TObject);
begin
    try
        CambiarEstadoCursor(CURSOR_RELOJ, Self, FComponentes);

        try
            RefinanciacionImpresionForm := TRefinanciacionImpresionForm.Create(Self);
            with RefinanciacionImpresionForm do begin
                TPanelMensajesForm.MuestraMensaje('Recuperando Documento Refinanciaci�n ...', True);
                if inicializar(FCodigoRefinanciacion, FCodigoRefinanciacionAnterior, FCodigoRefinanciacionUnificada, True) then begin		//SS_1051_PDO
                    ImprimirDocumentoGenerado(FCodigoEstado = REFINANCIACION_ESTADO_ACEPTADA);
                end;
            end;
        except
            on e:Exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
        if Assigned(RefinanciacionImpresionForm) then FreeAndNil(RefinanciacionImpresionForm);

        CambiarEstadoCursor(CURSOR_DEFECTO, Self, FComponentes);
        TPanelMensajesForm.OcultaPanel;
    end;
end;

procedure TRefinanciacionGestionForm.btnSugerirImporteDemandaClick(Sender: TObject);
begin
    if FComprobantesRefinanciadosTotal <> 0 then begin
        //nedtDemanda.ValueInt := FComprobantesRefinanciadosTotal * 40;                                     // SS_1245_CQU_20150224
        nedtDemanda.ValueInt := FComprobantesRefinanciadosTotal * REFINANCIACION_FACTOR_IMPORTE_SUJERIDO;   // SS_1245_CQU_20150224
    end;
end;

function TRefinanciacionGestionForm.ValidarImportesEnCero: Integer;
    var
        CuotasEntradasSinImporte: Integer;
        Puntero: TBookmark;
begin
    try
        CambiarEstadoCursor(CURSOR_RELOJ);

        CuotasEntradasSinImporte := 0;

        cdsCuotas.DisableControls;
        cdsCuotas.AfterScroll := Nil;
        dsCuotas.DataSet := Nil;
        Puntero := cdsCuotas.GetBookmark;

        cdsCuotas.First;

        while not cdsCuotas.Eof do begin
            if cdsCuotasImporte.AsInteger = 0 then begin
                Inc(CuotasEntradasSinImporte);
            end;

            cdsCuotas.Next;
        end;
    finally
        cdsCuotas.EnableControls;
        cdsCuotas.AfterScroll := cdsCuotasAfterScroll;
        dsCuotas.DataSet      := cdsCuotas;

        if Assigned(Puntero) then begin
            if cdsCuotas.BookmarkValid(Puntero) then begin
                cdsCuotas.GotoBookmark(Puntero);
                cdsCuotas.FreeBookmark(Puntero);
            end;
        end;
        CambiarEstadoCursor(CURSOR_DEFECTO);

        Result := CuotasEntradasSinImporte;
    end;
end;

procedure TRefinanciacionGestionForm.btnImprimirDefinitivaClick(Sender: TObject);
    resourcestring
        MSG_TITULO               = 'Confirme Proceso de la Refinanciaci�n';
        MSG_IMPRESION_DEFINITIVA = ' Va a generar el Documento definitivo de la refinanciaci�n y a cambiar su estado a Aceptada para que pueda ser Activada desde la ventana de Cobro Comprobantes. � Est� seguro de lanzar el Proceso ?';
begin
    if ValidarRefinanciacion(True, False) and ValidarRefinanciacionesUnificadas then begin						//SS_1051_PDO
        if ShowMsgBoxCN(MSG_TITULO, MSG_IMPRESION_DEFINITIVA, MB_ICONQUESTION, Self) = mrOk then try
            Self.Enabled := False;
            AceptarRefinanciacion;
        finally
            Self.Enabled := True;
        end;
    end;
end;

procedure TRefinanciacionGestionForm.AceptarRefinanciacion;
    var
        DocumentoOriginal: TDocumentosImpresosRefinanciacion;
        Puntero: TBookmark;
  i: Integer;
begin
    try
        CambiarEstadoCursor(CURSOR_RELOJ);

        try
            TPanelMensajesForm.MuestraMensaje('Aceptando Refinanciaci�n ...');

            Grabar(False);

            QueryExecute(DMConnections.BaseCAC, 'BEGIN TRANSACTION AceptarRefinanciacion');

            with spActualizarRefinanciacionAceptada do begin
                if Active then Close;
                Parameters.Refresh;

                Parameters.ParamByName('@CodigoRefinanciacion').Value := FCodigoRefinanciacion;
                Parameters.ParamByName('@CodigoRefinanciacionUnificada').Value := FCodigoRefinanciacionUnificada;			//SS_1051_PDO
                Parameters.ParamByName('@Usuario').Value              := UsuarioSistema;
                ExecProc;
            end;

            if spActualizarRefinanciacionAceptada.Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
                raise EErrorExceptionCN.Create(
                        'Aceptar Refinanciaci�n',
                        'Se Produjo un Error al Aceptar la Refinanciaci�n.');
            end
            else begin
                //TASK_006_AUN_20170406-CU.COBO.COB.302: si acept� la refinanciaci�n, pongo el convenio en cobranza interna: inicio
                DB_CRUDCommonProcs.AddParameter(spIngresarConvenioACobranzaInterna.Parameters, '@CodigoConvenio', ftInteger, pdInput, vcbConvenios.Value);
                DB_CRUDCommonProcs.AddParameter(spIngresarConvenioACobranzaInterna.Parameters, '@Usuario', ftString, pdInput, UsuarioSistema, 20);
                spIngresarConvenioACobranzaInterna.ExecProc;
                //TASK_006_AUN_20170406-CU.COBO.COB.302: si acept� la refinanciaci�n, pongo el convenio en cobranza interna: fin

                TPanelMensajesForm.MuestraMensaje('Generando Documento Refinanciaci�n ...');
                RefinanciacionImpresionForm := TRefinanciacionImpresionForm.Create(Self);

                with RefinanciacionImpresionForm do begin
                    if inicializar(FCodigoRefinanciacion, FCodigoRefinanciacion, FCodigoRefinanciacionUnificada) then begin		//SS_1051_PDO
                        DocumentoOriginal := Imprimir(True);
                    end;
                end;

                with spActualizarRefinanciacionDocumentoOriginal do begin
                    if Active then Close;
                    Parameters.Refresh;

                    Parameters.ParamByName('@CodigoRefinanciacion').Value := FCodigoRefinanciacion;
                    Parameters.ParamByName('@CodigoRefinanciacionUnificada').Value := FCodigoRefinanciacionUnificada;			//SS_1051_PDO
                    Parameters.ParamByName('@DocumentoOriginal').Value    := DocumentoOriginal.DocumentoPrincipal;
                    Parameters.ParamByName('@AnexoCuotas').Value          := iif(DocumentoOriginal.AnexoCuotas <> EmptyStr, DocumentoOriginal.AnexoCuotas, Null);
                    Parameters.ParamByName('@AnexoComprobantes').Value    := iif(DocumentoOriginal.AnexoComprobantes <> EmptyStr, DocumentoOriginal.AnexoComprobantes, Null);
                    Parameters.ParamByName('@AnexoComprobantes2').Value   := iif(DocumentoOriginal.AnexoComprobantes2 <> EmptyStr, DocumentoOriginal.AnexoComprobantes2, Null);
                    ExecProc;

                    if Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
                        raise EErrorExceptionCN.Create(
                                'Generar Documento Original',
                                'Se Produjo un Error al Almacenar el Documento Original.');
                    end;
                end;
            end;

            QueryExecute(DMConnections.BaseCAC, 'IF @@TRANCOUNT>0 BEGIN COMMIT TRANSACTION AceptarRefinanciacion END');

            ShowMsgBoxCN('Confirmaci�n Proceso', 'Se Acept� correctamente la Refinanciaci�n.', MB_ICONINFORMATION, Self);

            TPanelMensajesForm.MuestraMensaje('Imprimiendo Documento Refinanciaci�n ...');
            with RefinanciacionImpresionForm do begin
                if inicializar(FCodigoRefinanciacion, FCodigoRefinanciacion, FCodigoRefinanciacionUnificada, True) then begin			//SS_1051_PDO
                    ImprimirDocumentoGenerado(True);
                end;
            end;
        except
            on e: Exception do begin
                QueryExecute(DMConnections.BaseCAC, 'IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION AceptarRefinanciacion END');
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
        if Assigned(RefinanciacionImpresionForm) then FreeAndNil(RefinanciacionImpresionForm);

        CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

procedure TRefinanciacionGestionForm.VerificarClienteCarpetaLegal;
begin
    try
        CambiarEstadoCursor(CURSOR_RELOJ);
        
        try
            ConveniosEnCarpetaLegalForm := TConveniosEnCarpetaLegalForm.Create(Self);
            with ConveniosEnCarpetaLegalForm do begin
                if inicializar(peRut.Text, lblNombreCli.Caption) then begin
                    if spObtenerCarpetasLegalesPersona.RecordCount > 0 then ShowModal;
                end;
            end;
        except
            on e: Exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
        if Assigned(ConveniosEnCarpetaLegalForm) then FreeAndNil(ConveniosEnCarpetaLegalForm);
        
        CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;


procedure TRefinanciacionGestionForm.VerificarRefinanciacionesEnTramiteAceptadas;
    var
        SeguirProceso: TModalResult;
begin
    try
        RefinanciacionesEnTramiteAceptadasForm := TRefinanciacionesEnTramiteAceptadasForm.Create(Self);
        with RefinanciacionesEnTramiteAceptadasForm do begin
            Inicializar(peRut.Text, lblNombreCli.Caption);
            if spObtenerRefinanciacionesConsultas.RecordCount > 0 then begin
                SeguirProceso := ShowModal;
            end;
        end;

        if SeguirProceso = mrCancel then Close;
    finally
        if Assigned(RefinanciacionesEnTramiteAceptadasForm) then FreeAndNil(RefinanciacionesEnTramiteAceptadasForm);
    end;
end;

function TRefinanciacionGestionForm.ReemplazaPorApostrofe(Cadena: String): String;
    var
        Veces: Integer;
begin
    Result := EmptyStr;

    for Veces := 1 to length(Cadena) do begin
        if Cadena[Veces] = '''' then begin
            Result := Result + APOSTROFE;
        end
        else begin
            Result := Result + Cadena[Veces];
        end;
    end;
end;

//BEGIN : SS_1051_PDO------------------------------------------------------------------------------------------------------------------
function TRefinanciacionGestionForm.ValidarRefinanciacionesUnificadas: Boolean;
    const
        SQL = 'SELECT dbo.ValidarRefinanciacionesUnificadas(%d)';
        MSG_TITULO = 'Validaci�n Refinanciaciones Unificadas';
        MSG_ERROR_VALIDACION = 'Existen Refinanciaciones NO Validadas. Revise las Refinanciaciones Unificadas y reintente el Proceso.';
    Var
        RefinanciacionesSinValidar: Integer;
begin
    try
        if FCodigoRefinanciacionUnificada = 0 then begin
            Result := True
        end
        else begin
            RefinanciacionesSinValidar := QueryGetValueInt(DMConnections.BaseCAC, Format(SQL, [FCodigoRefinanciacionUnificada]));

            if RefinanciacionesSinValidar = 0 then begin
                Result := True;
            end
            else begin
                ShowMsgBoxCN(MSG_TITULO, MSG_ERROR_VALIDACION, MB_ICONERROR, Self);
                Result := False;
            end;
        end;
    Except
        on e: Exception do begin
            Result := False;
            ShowMsgBoxCN(e, Self);
        end;
    end;
end;
//END : SS_1051_PDO---------------------------------------------------------------------------------------------------------------------

function TRefinanciacionGestionForm.VerificarConvenioListaAmarilla(CodigoConvenio : Integer) : string;      //SS_1236_MCA_20141226
resourcestring                                                                                              //SS_1236_MCA_20141226
      SQL_CONSULTACONVENIO        = 'SELECT dbo.ObtenerEstadoConvenioEnListaAmarilla(%d)';                  //SS_1236_MCA_20141226
begin                                                                                                       //SS_1236_MCA_20141226
      Result      :=  '';                                                                                   //SS_1236_MCA_20141226
    if CodigoConvenio > 0 then begin                                                                        //SS_1236_MCA_20141226
        try                                                                                                 //SS_1236_MCA_20141226
            CambiarEstadoCursor(CURSOR_RELOJ, Self, FComponentes);                                          //SS_1236_MCA_20141226
            try                                                                                             //SS_1236_MCA_20141226
                Result:= QueryGetValue(   DMConnections.BaseCAC,                                            //SS_1236_MCA_20141226
                                           Format(SQL_CONSULTACONVENIO,                                     //SS_1236_MCA_20141226
                                                         [   CodigoConvenio                                 //SS_1236_MCA_20141226
                                                         ]                                                  //SS_1236_MCA_20141226
                                                 )                                                          //SS_1236_MCA_20141226
                                      );                                                                    //SS_1236_MCA_20141226
            except                                                                                          //SS_1236_MCA_20141226
                on e: Exception do begin                                                                    //SS_1236_MCA_20141226
                    ShowMsgBoxCN(e, Self);                                                                  //SS_1236_MCA_20141226
                end;                                                                                        //SS_1236_MCA_20141226
            end;                                                                                            //SS_1236_MCA_20141226
        finally                                                                                             //SS_1236_MCA_20141226
            CambiarEstadoCursor(CURSOR_DEFECTO, Self, FComponentes);                                        //SS_1236_MCA_20141226
        end;                                                                                                //SS_1236_MCA_20141226
    end;                                                                                                    //SS_1236_MCA_20141226
end;                                                                                                        //SS_1236_MCA_20141226

function TRefinanciacionGestionForm.VerificaPersonaListaAmarilla(RutCliente : string) : Boolean;            //SS_1236_MCA_20141226
resourcestring
    SQL_ESTACONVENIOENLISTAAMARILLA = 'SELECT dbo.EstaPersonaEnListaAmarilla(dbo.ObtenerCodigoPersonaRUT(''%s''))';    	//SS_1236_MCA_20141226
    SQL_ESTAPERSONAENPROCESOLISTAAMARILLA = 'SELECT dbo.EstaPersonaEnProcesoListaAmarilla(dbo.ObtenerCodigoPersonaRUT(''%s''))';   	//SS_1236_MCA_20141226
    MSG_INHABILITADO_LISTA_AMARILLA = 'El Cliente posee al menos un convenio en lista amarilla';   			            //SS_1236_MCA_20141226
    MSG_PROCESO_LISTA_AMARILLA = 'El RUT a lo menos tiene un convenio en proceso de Lista Amarilla';					//SS_1236_MCA_20141226
Var																														//SS_1236_MCA_20141226
	EsListaAmarilla: Boolean;                                                   					                    //SS_1236_MCA_20141226
    EsProcesoListaAmarilla: string;																						//SS_1236_MCA_20141226
begin                                                                                                                   //SS_1236_MCA_20141226
    Result      :=  False;                                                                                              //SS_1236_MCA_20141226
	if RutCliente <> EmptyStr then begin																				//SS_1236_MCA_20141226
        try                                                                                                             //SS_1236_MCA_20141226
            CambiarEstadoCursor(CURSOR_RELOJ, Self, FComponentes);                                                      //SS_1236_MCA_20141226
            try                                                                                                         //SS_1236_MCA_20141226
                EsListaAmarilla := StrToBool(QueryGetValue(DMConnections.BaseCAC,           	                        //SS_1236_MCA_20141226
        								Format(SQL_ESTACONVENIOENLISTAAMARILLA,                                         //SS_1236_MCA_20141226
                                        [RutCliente])));           	                                                    //SS_1236_MCA_20141226
                EsProcesoListaAmarilla := QueryGetValue(DMConnections.BaseCAC,           		                        //SS_1236_MCA_20141226
                                                Format(SQL_ESTAPERSONAENPROCESOLISTAAMARILLA,                           //SS_1236_MCA_20141226
                                                [RutCliente]));           		                                        //SS_1236_MCA_20141226
                if EsListaAmarilla then                                                                                 //SS_1236_MCA_20141226
                begin                                                                                                   //SS_1236_MCA_20141226
                    ShowMsgBoxCN(self.Caption, MSG_INHABILITADO_LISTA_AMARILLA, MB_ICONWARNING, self);                  //SS_1236_MCA_20141226
                    exit;                                                                   	                        //SS_1236_MCA_20141226
                end;                                                                        	                        //SS_1236_MCA_20141226
                if EsProcesoListaAmarilla <> EmptyStr then                                                              //SS_1236_MCA_20141226
                begin                                                                                                   //SS_1236_MCA_20141226
                    ShowMsgBoxCN(self.Caption, MSG_PROCESO_LISTA_AMARILLA,  MB_ICONWARNING, self);                      //SS_1236_MCA_20141226
                end;                                                                                                    //SS_1236_MCA_20141226
                                                                                                                        //SS_1236_MCA_20141226
                                                                                                                        //SS_1236_MCA_20141226
                                                                                                                        //SS_1236_MCA_20141226
                                                                                                                        //SS_1236_MCA_20141226
            except                                                                                                      //SS_1236_MCA_20141226
                on e: Exception do begin                                                                                //SS_1236_MCA_20141226
                    ShowMsgBoxCN(e, Self);                                                                              //SS_1236_MCA_20141226
                end;                                                                                                    //SS_1236_MCA_20141226
            end;                                                                                                        //SS_1236_MCA_20141226
        finally                                                                                                         //SS_1236_MCA_20141226
            CambiarEstadoCursor(CURSOR_DEFECTO, Self, FComponentes);                                                    //SS_1236_MCA_20141226
        end;                                                                                                            //SS_1236_MCA_20141226
    end;                                                                                                                //SS_1236_MCA_20141226
end;                                                                                                                    //SS_1236_MCA_20141226

end.
