unit VentaDayPassFecha;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, Validate, DateEdit, DBClient, VentaDayPass,
  UtilProc, Util, DB, ADODB, Peatypes;

type
  TFormVentaDayPassFecha = class(TForm)
    deDesde: TDateEdit;
    lblDescripcion: TLabel;
    BtnAceptar: TDPSButton;
    BtnCancelar: TDPSButton;
    Label2: TLabel;
    lblDuracion: TLabel;
    lblHasta: TLabel;
    qry_MaestroDayPassPatente: TADOQuery;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure deDesdeChange(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
  private
    { Private declarations }
    Duracion: integer;
  public
    { Public declarations }
    class function IngresarFechaPase(CodigoTipoDayPass,
            DiasDuracion, Importe: integer; Descripcion: AnsiString; Patente: String;
            FechaDesde: TDateTime; EsAlta: boolean = true; TablaPases: TClientDataSet = nil): boolean;
    function EsUnaFechaValida(NFD, NFH: tdatetime; EsAlta: boolean): boolean;
    function RangoFechaNoSeRepite(fechaDesde, FechaHasta: tdatetime): boolean;
    procedure MarcarEdicionRegistroActual(edicion: boolean);
  end;

resourcestring
    CAPTION_DURACION = 'Duraci�n: ';
    DIAS = ' d�as';
    DESC = 'Descripci�n: ';
    CAPTION_ALTA = 'Ingrese la fecha del pase';
    CAPTION_MODI = 'Modifique la fecha del pase';

var
  FormVentaDayPassFecha: TFormVentaDayPassFecha;
  FechaFinalDesde, FechaAnteriorDesde,
  FechaFinalHasta: TDateTime;
  ModalResultAux: TModalResult;
  FTablaPases: TClientDataSet;
  FPatente: string;

implementation

{$R *.dfm}

{ TFormVentaDayPassFecha }

class function TFormVentaDayPassFecha.IngresarFechaPase(CodigoTipoDayPass,
            DiasDuracion, Importe: integer; Descripcion: AnsiString; Patente: String;
            FechaDesde: TDateTime; EsAlta: boolean = true; TablaPases: TClientDataSet = nil): boolean;
var
    g: TFormVentaDayPassFecha;
begin
    Application.createForm(TFormVentaDayPassFecha, g);
    ModalResultAux := mrCancel;

    if EsAlta then g.Caption := CAPTION_ALTA
    else g.Caption := CAPTION_MODI;
    g.lblDescripcion.Caption := DESC + Descripcion;
    g.Duracion := iif(DiasDuracion = 0, 1, DiasDuracion);
    g.lblDuracion.Caption := CAPTION_DURACION + IntToStr(DiasDuracion) + DIAS;
    g.deDesde.Date := FechaDesde;
    FechaAnteriorDesde := FechaDesde;
    //Si la duracioni es un d�a, la fecha desde y hasta son iguales
    g.lblHasta.Caption := DateToStr(g.deDesde.Date + DiasDuracion - 1);
    FTablaPases:= TablaPases;
    FPatente := Patente;
    g.ShowModal;
    g.Free;
    result := true;
end;

procedure TFormVentaDayPassFecha.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TFormVentaDayPassFecha.deDesdeChange(Sender: TObject);
begin
    if IsValidDate(deDesde.Text) then
        lblHasta.Caption := DateToStr(deDesde.Date + self.Duracion - 1);
end;

procedure TFormVentaDayPassFecha.BtnAceptarClick(Sender: TObject);
resourcestring
    FECHA_INCORRECTA = 'La fecha no puede ser anterior a la del d�a.';
    ERROR = 'Fecha incorrecta';

begin
    if deDesde.Date >= Date then begin
        FechaFinalDesde := deDesde.Date;
        FechaFinalHasta := deDesde.Date + Duracion - 1;
        if not EsUnaFechaValida (FechaFinalDesde, FechaFinalHasta, Caption = CAPTION_ALTA) then begin
            ModalResult := mrNone;
        end
        else
            ModalResultAux := mrOk;
    end
    else begin
        ModalResult := mrNone;
        MsgBoxBalloon(FECHA_INCORRECTA, ERROR, MB_ICONSTOP, deDesde);
    end;
end;

procedure TFormVentaDayPassFecha.BtnCancelarClick(Sender: TObject);
begin
    ModalResultAux := mrCancel;
end;

function TFormVentaDayPassFecha.EsUnaFechaValida(NFD, NFH: tdatetime; EsAlta: boolean): boolean;
resourcestring
    FECHAS_MAL_INGRESADAS = 'Las fecha ingresada se superpone con una ingresada anteriormente.';
    FECHAS_MAL_INGRESADAS_BASE = 'Las fecha ingresada se superpone con alguna comprada en otra operaci�n.';
    ERROR = 'Fecha incorrecta';
var
    ok: boolean;
    mensaje: AnsiString;
    BookMark: TBookmark;
begin
(*
    //Primero chequeo contra la base
    ok := RangoFechaNoSeRepite(NFD, NFH);
    if not ok then begin
        mensaje := FECHAS_MAL_INGRESADAS_BASE;
    end
    else begin
        (*NFD: Nueva Fecha Desde
          NFH: Nueva Fecha Hasta

        Primero valido con respecto a los que ya ingres�:
          Consideraci�n: si es modificaci�n, recorro todos menos el registro.
          Eso lo determino con el campo EnEdicion
        Segundo valido con los ingresados anteriormente, en la base
        *)
        //chequear esta proxima linea si es necesaria, ver de donde viene el programa.

    ok := True;

    if (not FTablaPases.Active) or  (FTablaPases.IsEmpty) then begin
        result := true;
        exit;
    end;

    //Marco el registro actual que se esta editanto, EnEdicion = true
    if not EsAlta then
        MarcarEdicionRegistroActual(true);

    BookMark := FTablaPases.GetBookmark;
    FTablaPases.First;
    while (not FTablaPases.Eof) and (ok) do
    begin
        if (not FTablaPases.FieldValues['EnEdicion']) or (EsAlta) then begin
            ok := (NFD > FTablaPases.FieldValues['Hasta']) or
                  (NFH < FTablaPases.FieldValues['Desde']);
        end
        else //ya lo dejo desmarcado
            MarcarEdicionRegistroActual(false);

        FTablaPases.Next;
    end;
    FTablaPases.GotoBookmark(BookMark);
    FTablaPases.FreeBookmark(BookMark);
    if not ok then begin
        if not FTablaPases.FieldValues['Vendido'] then
            mensaje := FECHAS_MAL_INGRESADAS
        else
            mensaje := FECHAS_MAL_INGRESADAS_BASE;
    end;
//    end;

    if not ok then
        MsgBoxBalloon (mensaje, ERROR, MB_ICONSTOP, deDesde);

    result := ok;
end;

function TFormVentaDayPassFecha.RangoFechaNoSeRepite(fechaDesde,
  FechaHasta: tdatetime): boolean;
var
    NoEstaSuperpuesta: boolean;
begin
    NoEstaSuperpuesta := true;
    with qry_MaestroDayPassPatente do
    begin
        Close;
        Parameters.ParamByName('xPatente').Value := Trim(FPatente);
        Parameters.ParamByName('xFechaDesde').Value := date;
        Parameters.ParamByName('xEstado').Value := ESTADO_ACTIVO_DAY_PASS;
        Parameters.ParamByName('xFechaExcluida').Value := FechaAnteriorDesde;
        Open;
        First;
        while (not EOF) and (NoEstaSuperpuesta) do
        begin
            NoEstaSuperpuesta := (fechaDesde > FieldByName('FechaHasta').AsDateTime) or
                                 (fechaHasta < FieldByName('FechaDesde').AsDateTime);
            Next;
        end;
        Close;
    end;
    result := NoEstaSuperpuesta;
end;

procedure TFormVentaDayPassFecha.MarcarEdicionRegistroActual(edicion: boolean);
begin
    //Marco el registro actual como que se esta editanto, EnEdicion = true
    FTablaPases.Edit;
    FTablaPases.FieldByName('EnEdicion').AsBoolean := edicion;
    FTablaPases.Post;
end;


end.
