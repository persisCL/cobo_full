object RefinanciacionEntrarEditarCuotasForm: TRefinanciacionEntrarEditarCuotasForm
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = ' A'#241'adir / Editar Cuota Refinanciaci'#243'n'
  ClientHeight = 463
  ClientWidth = 636
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnKeyDown = FormKeyDown
  DesignSize = (
    636
    463)
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 423
    Width = 636
    Height = 40
    Align = alBottom
    Color = 14408667
    ParentBackground = False
    TabOrder = 0
    DesignSize = (
      636
      40)
    object btnCancelar: TButton
      Left = 553
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Cancelar'
      TabOrder = 1
      OnClick = btnCancelarClick
    end
    object btnAceptar: TButton
      Left = 474
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Aceptar'
      Default = True
      TabOrder = 0
      OnClick = btnAceptarClick
    end
  end
  object gbDatos: TGroupBox
    Left = 5
    Top = 8
    Width = 625
    Height = 410
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = '  Detalle Cuota Refinanciaci'#243'n  '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    TabStop = True
    DesignSize = (
      625
      410)
    object lbl_FormaPago: TLabel
      Left = 211
      Top = 19
      Width = 72
      Height = 13
      Caption = 'Forma de Pago'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblCanalDePago: TLabel
      Left = 20
      Top = 19
      Width = 69
      Height = 13
      Caption = 'Canal de Pago'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object pnlPago2: TPanel
      Left = 7
      Top = 65
      Width = 611
      Height = 53
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      object lblFechaDePago: TLabel
        Left = 13
        Top = 6
        Width = 71
        Height = 13
        Caption = 'Fecha de Pago'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblSiDeseaCambiarFechaPago: TLabel
        Left = 119
        Top = 17
        Width = 185
        Height = 27
        AutoSize = False
        Caption = 
          'Si desea cambiar la fecha  con la que se registrar'#225' el pago haga' +
          ' click'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object lblaca: TLabel
        Left = 271
        Top = 31
        Width = 17
        Height = 13
        Cursor = crHandPoint
        Caption = 'ac'#225
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsUnderline]
        ParentFont = False
        OnClick = lblacaClick
      end
      object lblSiDeseaCambiarElMontoPagado: TLabel
        Left = 468
        Top = 17
        Width = 132
        Height = 27
        AutoSize = False
        Caption = 'Si desea cambiar el monto pagado haga click'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object lblAcaMonto: TLabel
        Left = 561
        Top = 31
        Width = 17
        Height = 13
        Cursor = crHandPoint
        Caption = 'ac'#225
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsUnderline]
        ParentFont = False
        OnClick = lblAcaMontoClick
      end
      object deFechaPago: TDateEdit
        Left = 12
        Top = 23
        Width = 97
        Height = 21
        AutoSelect = False
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnChange = deFechaPagoChange
        Date = -693594.000000000000000000
      end
    end
    object gbCuotaAntecesora: TGroupBox
      Left = 7
      Top = 354
      Width = 611
      Height = 48
      Anchors = [akRight, akBottom]
      Caption = '  Cuota Antecesora  '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      DesignSize = (
        611
        48)
      object vcbCuotaAntecesora: TVariantComboBox
        Left = 15
        Top = 19
        Width = 583
        Height = 21
        Style = vcsDropDownList
        Anchors = [akRight, akBottom]
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ItemHeight = 13
        ParentFont = False
        TabOrder = 0
        OnChange = vcbCuotaAntecesoraChange
        Items = <>
      end
    end
    object edtCodigoFormaPago: TEdit
      Left = 210
      Top = 36
      Width = 34
      Height = 21
      CharCase = ecUpperCase
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 2
      ParentFont = False
      TabOrder = 0
      OnChange = edtCodigoFormaPagoChange
    end
    object edtDescFormaPago: TEdit
      Left = 245
      Top = 36
      Width = 158
      Height = 21
      TabStop = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 1
    end
    object GroupBox2: TGroupBox
      Left = 7
      Top = 121
      Width = 611
      Height = 231
      Anchors = [akRight, akBottom]
      Caption = '  Detalle Forma de Pago  '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      object nbOpciones: TNotebook
        Left = 26
        Top = 16
        Width = 556
        Height = 209
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        PageIndex = 1
        ParentFont = False
        TabOrder = 0
        object TPage
          Left = 0
          Top = 0
          Caption = 'NoDefinido'
          object Label1: TLabel
            Left = 121
            Top = 81
            Width = 291
            Height = 23
            Caption = 'Entrada de Datos NO Definida.'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
        end
        object TPage
          Left = 0
          Top = 0
          Caption = 'EfectivoRefinancia'
          object Label5: TLabel
            Left = 75
            Top = 25
            Width = 45
            Height = 13
            Alignment = taRightJustify
            Caption = 'Importe: '
          end
          object Label11: TLabel
            Left = 84
            Top = 49
            Width = 33
            Height = 13
            Alignment = taRightJustify
            Caption = 'Fecha:'
          end
          object nedtEfectivoImporte: TNumericEdit
            Left = 126
            Top = 22
            Width = 121
            Height = 21
            TabOrder = 0
          end
          object dedtEfectivo: TDateEdit
            Left = 126
            Top = 46
            Width = 90
            Height = 21
            AutoSelect = False
            TabOrder = 1
            Date = -693594.000000000000000000
          end
        end
        object TPage
          Left = 0
          Top = 0
          Caption = 'Tarjetas'
          ExplicitWidth = 0
          ExplicitHeight = 0
        end
        object TPage
          Left = 0
          Top = 0
          Caption = 'Cheque'
          object lblChequeNumero: TLabel
            Left = 49
            Top = 97
            Width = 68
            Height = 13
            Alignment = taRightJustify
            Caption = 'Nro. Cheque :'
            Color = clBtnFace
            FocusControl = txtChequeNumero
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentColor = False
            ParentFont = False
          end
          object lblChequeBanco: TLabel
            Left = 81
            Top = 73
            Width = 36
            Height = 13
            Alignment = taRightJustify
            Caption = 'Banco :'
            Color = clBtnFace
            FocusControl = cbChequeBancos
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentColor = False
            ParentFont = False
          end
          object lblChequeRUT: TLabel
            Left = 90
            Top = 49
            Width = 27
            Height = 13
            Alignment = taRightJustify
            Caption = 'RUT :'
            FocusControl = txtChequeDocumento
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object lblChequeTitular: TLabel
            Left = 80
            Top = 26
            Width = 37
            Height = 13
            Alignment = taRightJustify
            Caption = 'Titular :'
            FocusControl = txtChequeNombreTitular
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object lblChequeFecha: TLabel
            Left = 41
            Top = 122
            Width = 76
            Height = 13
            Alignment = taRightJustify
            Caption = 'Fecha Cheque :'
            Color = clBtnFace
            FocusControl = txtChequeNumero
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentColor = False
            ParentFont = False
          end
          object btnDatosClienteCheque: TSpeedButton
            Left = 490
            Top = 22
            Width = 22
            Height = 21
            Hint = 'Copia los datos del Cliente comprobante'
            Glyph.Data = {
              AA040000424DAA04000000000000360000002800000013000000130000000100
              1800000000007404000000000000000000000000000000000000D8E9ECD8E9EC
              D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9
              ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8
              E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
              D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9
              ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8
              E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
              D8E9EC840000840000840000840000840000840000840000840000840000D8E9
              ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC84
              0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000D8E9ECD8E9EC
              D8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC840000FFFF
              FF000000000000000000000000000000FFFFFF840000D8E9ECD8E9ECD8E9EC00
              0000D8E9EC000000000000000000000000000000000000840000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9
              EC000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000FFFFFF00000000000000
              0000000000000000FFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000
              FFFFFF000000000000000000000000840000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFF840000FFFFFF000000000000FFFFFF840000840000
              840000840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFF0000000000
              00000000000000840000FFFFFFFFFFFFFFFFFFFFFFFF840000FFFFFF840000D8
              E9ECD8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFF840000FFFFFFFFFFFFFFFFFFFFFFFF840000840000D8E9ECD8E9ECD8E9
              ECD8E9ECD8E9EC000000D8E9EC000000FFFFFF000000000000FFFFFF00000084
              0000840000840000840000840000840000D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
              D8E9EC000000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF0000
              00D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC00
              0000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF000000000000D8E9ECD8E9EC
              D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9
              EC000000000000000000000000000000000000D8E9ECD8E9ECD8E9ECD8E9ECD8
              E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9EC
              D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9
              ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8
              E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
              D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9
              ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8
              E9ECD8E9ECD8E9ECD8E9EC000000}
            ParentShowHint = False
            ShowHint = True
            OnClick = btnDatosClienteChequeClick
          end
          object Label2: TLabel
            Left = 27
            Top = 145
            Width = 90
            Height = 13
            Alignment = taRightJustify
            Caption = 'Nro. Repactaci'#243'n :'
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label4: TLabel
            Left = 72
            Top = 169
            Width = 45
            Height = 13
            Alignment = taRightJustify
            Caption = 'Importe :'
          end
          object txtChequeNumero: TEdit
            Left = 126
            Top = 94
            Width = 238
            Height = 21
            CharCase = ecUpperCase
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            MaxLength = 20
            ParentFont = False
            TabOrder = 3
          end
          object cbChequeBancos: TVariantComboBox
            Left = 126
            Top = 70
            Width = 237
            Height = 21
            Style = vcsDropDownList
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ItemHeight = 13
            ParentFont = False
            TabOrder = 2
            Items = <>
          end
          object txtChequeNombreTitular: TEdit
            Left = 126
            Top = 22
            Width = 361
            Height = 21
            CharCase = ecUpperCase
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            MaxLength = 100
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
          end
          object txtChequeDocumento: TEdit
            Left = 126
            Top = 46
            Width = 108
            Height = 21
            Hint = 'RUT'
            CharCase = ecUpperCase
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            MaxLength = 9
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
          end
          object deChequeFecha: TDateEdit
            Left = 126
            Top = 118
            Width = 90
            Height = 21
            AutoSelect = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 4
            Date = -693594.000000000000000000
          end
          object edtRepactacionCheque: TEdit
            Left = 126
            Top = 142
            Width = 238
            Height = 21
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 5
          end
          object nedtImporteCheque: TNumericEdit
            Left = 126
            Top = 166
            Width = 108
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 6
          end
        end
        object TPage
          Left = 0
          Top = 0
          Caption = 'Vale Vista'
          object lblValeVistaTomadoPor: TLabel
            Left = 53
            Top = 26
            Width = 64
            Height = 13
            Alignment = taRightJustify
            Caption = 'Tomado por :'
            FocusControl = txt_TomadoPorVV
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object lblValeVistaRUT: TLabel
            Left = 90
            Top = 49
            Width = 27
            Height = 13
            Alignment = taRightJustify
            Caption = 'RUT :'
            FocusControl = txt_RUTVV
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object lblValeVistaBanco: TLabel
            Left = 81
            Top = 73
            Width = 36
            Height = 13
            Alignment = taRightJustify
            Caption = 'Banco :'
            FocusControl = cb_BancosVV
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object lblValeVistaNumero: TLabel
            Left = 40
            Top = 97
            Width = 77
            Height = 13
            Alignment = taRightJustify
            Caption = 'Nro. Vale Vista :'
            FocusControl = txt_NroVV
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object lblValeVistaFecha: TLabel
            Left = 32
            Top = 122
            Width = 85
            Height = 13
            Alignment = taRightJustify
            Caption = 'Fecha Vale Vista :'
            FocusControl = de_FechaVV
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object btn_DatosClienteValeVista: TSpeedButton
            Left = 490
            Top = 22
            Width = 22
            Height = 21
            Hint = 'Copia los datos del Cliente comprobante'
            Glyph.Data = {
              AA040000424DAA04000000000000360000002800000013000000130000000100
              1800000000007404000000000000000000000000000000000000D8E9ECD8E9EC
              D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9
              ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8
              E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
              D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9
              ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8
              E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
              D8E9EC840000840000840000840000840000840000840000840000840000D8E9
              ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC84
              0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000D8E9ECD8E9EC
              D8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC840000FFFF
              FF000000000000000000000000000000FFFFFF840000D8E9ECD8E9ECD8E9EC00
              0000D8E9EC000000000000000000000000000000000000840000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9
              EC000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000FFFFFF00000000000000
              0000000000000000FFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000
              FFFFFF000000000000000000000000840000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFF840000FFFFFF000000000000FFFFFF840000840000
              840000840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFF0000000000
              00000000000000840000FFFFFFFFFFFFFFFFFFFFFFFF840000FFFFFF840000D8
              E9ECD8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFF840000FFFFFFFFFFFFFFFFFFFFFFFF840000840000D8E9ECD8E9ECD8E9
              ECD8E9ECD8E9EC000000D8E9EC000000FFFFFF000000000000FFFFFF00000084
              0000840000840000840000840000840000D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
              D8E9EC000000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF0000
              00D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC00
              0000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF000000000000D8E9ECD8E9EC
              D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9
              EC000000000000000000000000000000000000D8E9ECD8E9ECD8E9ECD8E9ECD8
              E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9EC
              D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9
              ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8
              E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
              D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9
              ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8
              E9ECD8E9ECD8E9ECD8E9EC000000}
            OnClick = btn_DatosClienteValeVistaClick
          end
          object Label6: TLabel
            Left = 72
            Top = 145
            Width = 45
            Height = 13
            Alignment = taRightJustify
            Caption = 'Importe :'
          end
          object txt_TomadoPorVV: TEdit
            Left = 126
            Top = 22
            Width = 361
            Height = 21
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
          end
          object txt_RUTVV: TEdit
            Left = 126
            Top = 46
            Width = 108
            Height = 21
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
          end
          object txt_NroVV: TEdit
            Left = 126
            Top = 94
            Width = 238
            Height = 21
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
          end
          object cb_BancosVV: TVariantComboBox
            Left = 126
            Top = 70
            Width = 237
            Height = 21
            Style = vcsDropDownList
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ItemHeight = 13
            ParentFont = False
            TabOrder = 2
            Items = <>
          end
          object de_FechaVV: TDateEdit
            Left = 126
            Top = 118
            Width = 90
            Height = 21
            AutoSelect = False
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 4
            Date = -693594.000000000000000000
          end
          object nedtImporteVV: TNumericEdit
            Left = 126
            Top = 142
            Width = 108
            Height = 21
            TabOrder = 5
          end
        end
        object TPage
          Left = 0
          Top = 0
          Caption = 'DebitoCuentaPAC'
          ExplicitWidth = 0
          ExplicitHeight = 0
        end
        object TPage
          Left = 0
          Top = 0
          Caption = 'DebitoCuentaPAT'
          ExplicitWidth = 0
          ExplicitHeight = 0
        end
        object TPage
          Left = 0
          Top = 0
          Caption = 'TarjetaCredito'
          object spbTarjetaCreditoCopiarDatosCliente: TSpeedButton
            Left = 493
            Top = 22
            Width = 22
            Height = 22
            Glyph.Data = {
              AA040000424DAA04000000000000360000002800000013000000130000000100
              1800000000007404000000000000000000000000000000000000D8E9ECD8E9EC
              D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9
              ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8
              E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
              D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9
              ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8
              E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
              D8E9EC840000840000840000840000840000840000840000840000840000D8E9
              ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC84
              0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000D8E9ECD8E9EC
              D8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC840000FFFF
              FF000000000000000000000000000000FFFFFF840000D8E9ECD8E9ECD8E9EC00
              0000D8E9EC000000000000000000000000000000000000840000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9
              EC000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000FFFFFF00000000000000
              0000000000000000FFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000
              FFFFFF000000000000000000000000840000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFF840000FFFFFF000000000000FFFFFF840000840000
              840000840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFF0000000000
              00000000000000840000FFFFFFFFFFFFFFFFFFFFFFFF840000FFFFFF840000D8
              E9ECD8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFF840000FFFFFFFFFFFFFFFFFFFFFFFF840000840000D8E9ECD8E9ECD8E9
              ECD8E9ECD8E9EC000000D8E9EC000000FFFFFF000000000000FFFFFF00000084
              0000840000840000840000840000840000D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
              D8E9EC000000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF0000
              00D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC00
              0000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF000000000000D8E9ECD8E9EC
              D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9
              EC000000000000000000000000000000000000D8E9ECD8E9ECD8E9ECD8E9ECD8
              E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9EC
              D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9
              ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8
              E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
              D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9
              ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8
              E9ECD8E9ECD8E9ECD8E9EC000000}
            OnClick = spbTarjetaCreditoCopiarDatosClienteClick
          end
          object lblTarjetaCreditoTitular: TLabel
            Left = 80
            Top = 25
            Width = 37
            Height = 13
            Alignment = taRightJustify
            Caption = 'Titular :'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object lblTarjetaCreditoTarjeta: TLabel
            Left = 75
            Top = 73
            Width = 42
            Height = 13
            Alignment = taRightJustify
            Caption = 'Tarjeta :'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object lblTarjetaCreditoNroConfirmacion: TLabel
            Left = 24
            Top = 97
            Width = 93
            Height = 13
            Alignment = taRightJustify
            Caption = 'Nro. Confirmaci'#243'n :'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object lblTarjetaCreditoBanco: TLabel
            Left = 81
            Top = 49
            Width = 36
            Height = 13
            Alignment = taRightJustify
            Caption = 'Banco :'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object lblTarjetaCreditoCuotas: TLabel
            Left = 76
            Top = 145
            Width = 41
            Height = 13
            Alignment = taRightJustify
            Caption = 'Cuotas :'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object lblTarjetaCreditoImporte: TLabel
            Left = 72
            Top = 169
            Width = 45
            Height = 13
            Alignment = taRightJustify
            Caption = 'Importe :'
          end
          object Label12: TLabel
            Left = 81
            Top = 121
            Width = 36
            Height = 13
            Alignment = taRightJustify
            Caption = 'Fecha :'
          end
          object edtTarjetaCreditoTitular: TEdit
            Left = 126
            Top = 22
            Width = 361
            Height = 21
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
          end
          object vcbTarjetaCreditoTarjetas: TVariantComboBox
            Left = 126
            Top = 70
            Width = 245
            Height = 21
            Style = vcsDropDownList
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ItemHeight = 13
            ParentFont = False
            TabOrder = 2
            Items = <>
          end
          object edtTarjetaCreditoNroConfirmacion: TEdit
            Left = 126
            Top = 94
            Width = 245
            Height = 21
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
          end
          object vcbTarjetaCreditoBancos: TVariantComboBox
            Left = 126
            Top = 46
            Width = 245
            Height = 21
            Style = vcsDropDownList
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ItemHeight = 13
            ParentFont = False
            TabOrder = 1
            Items = <>
          end
          object nedtTarjetaCreditoCuotas: TNumericEdit
            Left = 126
            Top = 142
            Width = 108
            Height = 21
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 5
            Value = 1.000000000000000000
          end
          object nedtTarjetaCreditoImporte: TNumericEdit
            Left = 126
            Top = 166
            Width = 108
            Height = 21
            TabOrder = 6
          end
          object dedtFechaTarjetaCredito: TDateEdit
            Left = 126
            Top = 118
            Width = 108
            Height = 21
            AutoSelect = False
            TabOrder = 4
            Date = -693594.000000000000000000
          end
        end
        object TPage
          Left = 0
          Top = 0
          Caption = 'TarjetaDebito'
          object lblTarjetaDebitoTitular: TLabel
            Left = 80
            Top = 25
            Width = 37
            Height = 13
            Alignment = taRightJustify
            Caption = 'Titular :'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object lblTarjetaDebitoBanco: TLabel
            Left = 81
            Top = 49
            Width = 36
            Height = 13
            Alignment = taRightJustify
            Caption = 'Banco :'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object lblTarjetaDebitoNroConfirmacion: TLabel
            Left = 24
            Top = 73
            Width = 93
            Height = 13
            Alignment = taRightJustify
            Caption = 'Nro. Confirmaci'#243'n :'
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object spbTarjetaDebitoCopiarDatosCliente: TSpeedButton
            Left = 493
            Top = 21
            Width = 22
            Height = 22
            Glyph.Data = {
              AA040000424DAA04000000000000360000002800000013000000130000000100
              1800000000007404000000000000000000000000000000000000D8E9ECD8E9EC
              D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9
              ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8
              E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
              D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9
              ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8
              E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
              D8E9EC840000840000840000840000840000840000840000840000840000D8E9
              ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC84
              0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000D8E9ECD8E9EC
              D8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC840000FFFF
              FF000000000000000000000000000000FFFFFF840000D8E9ECD8E9ECD8E9EC00
              0000D8E9EC000000000000000000000000000000000000840000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9
              EC000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000FFFFFF00000000000000
              0000000000000000FFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000
              FFFFFF000000000000000000000000840000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFF840000FFFFFF000000000000FFFFFF840000840000
              840000840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFF0000000000
              00000000000000840000FFFFFFFFFFFFFFFFFFFFFFFF840000FFFFFF840000D8
              E9ECD8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFF840000FFFFFFFFFFFFFFFFFFFFFFFF840000840000D8E9ECD8E9ECD8E9
              ECD8E9ECD8E9EC000000D8E9EC000000FFFFFF000000000000FFFFFF00000084
              0000840000840000840000840000840000D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
              D8E9EC000000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF0000
              00D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC00
              0000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF000000000000D8E9ECD8E9EC
              D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9
              EC000000000000000000000000000000000000D8E9ECD8E9ECD8E9ECD8E9ECD8
              E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9EC
              D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9
              ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8
              E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
              D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9
              ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8
              E9ECD8E9ECD8E9ECD8E9EC000000}
            OnClick = spbTarjetaDebitoCopiarDatosClienteClick
          end
          object lblTarjetaDebitoImporte: TLabel
            Left = 72
            Top = 121
            Width = 45
            Height = 13
            Alignment = taRightJustify
            Caption = 'Importe :'
          end
          object Label13: TLabel
            Left = 81
            Top = 97
            Width = 36
            Height = 13
            Alignment = taRightJustify
            Caption = 'Fecha :'
          end
          object edtTarjetaDebitoTitular: TEdit
            Left = 126
            Top = 22
            Width = 361
            Height = 21
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
          end
          object edtTarjetaDebitoNroConfirmacion: TEdit
            Left = 126
            Top = 70
            Width = 245
            Height = 21
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
          end
          object vcbTarjetaDebitoBancos: TVariantComboBox
            Left = 126
            Top = 46
            Width = 245
            Height = 21
            Style = vcsDropDownList
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ItemHeight = 13
            ParentFont = False
            TabOrder = 1
            Items = <>
          end
          object nedtTarjetaDebitoImporte: TNumericEdit
            Left = 126
            Top = 118
            Width = 108
            Height = 21
            TabOrder = 4
          end
          object dedtFechaTarjetaDebito: TDateEdit
            Left = 126
            Top = 94
            Width = 108
            Height = 21
            AutoSelect = False
            TabOrder = 3
            Date = -693594.000000000000000000
          end
        end
        object TPage
          Left = 0
          Top = 0
          Caption = 'Pagare'
          object lblPagareNumero: TLabel
            Left = 89
            Top = 97
            Width = 28
            Height = 13
            Alignment = taRightJustify
            Caption = 'Nro. :'
            Color = clBtnFace
            FocusControl = txtPagareNumero
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentColor = False
            ParentFont = False
          end
          object lblPagareBanco: TLabel
            Left = 56
            Top = 73
            Width = 61
            Height = 13
            Alignment = taRightJustify
            Caption = 'Documento :'
            Color = clBtnFace
            FocusControl = cbPagareBancos
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentColor = False
            ParentFont = False
          end
          object lblPagareRUT: TLabel
            Left = 90
            Top = 49
            Width = 27
            Height = 13
            Alignment = taRightJustify
            Caption = 'RUT :'
            FocusControl = txtPagareDocumento
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object lblPagareTitular: TLabel
            Left = 80
            Top = 25
            Width = 37
            Height = 13
            Alignment = taRightJustify
            Caption = 'Titular :'
            FocusControl = txtPagareNombreTitular
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object lblPagareFecha: TLabel
            Left = 58
            Top = 121
            Width = 59
            Height = 13
            Alignment = taRightJustify
            Caption = 'Fecha Vto. :'
            Color = clBtnFace
            FocusControl = dePagareFecha
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentColor = False
            ParentFont = False
          end
          object btnDatosClientePagare: TSpeedButton
            Left = 490
            Top = 22
            Width = 22
            Height = 21
            Hint = 'Copia los datos del Cliente comprobante'
            Glyph.Data = {
              AA040000424DAA04000000000000360000002800000013000000130000000100
              1800000000007404000000000000000000000000000000000000D8E9ECD8E9EC
              D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9
              ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8
              E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
              D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9
              ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8
              E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
              D8E9EC840000840000840000840000840000840000840000840000840000D8E9
              ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC84
              0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000D8E9ECD8E9EC
              D8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC840000FFFF
              FF000000000000000000000000000000FFFFFF840000D8E9ECD8E9ECD8E9EC00
              0000D8E9EC000000000000000000000000000000000000840000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9
              EC000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000FFFFFF00000000000000
              0000000000000000FFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000
              FFFFFF000000000000000000000000840000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFF840000FFFFFF000000000000FFFFFF840000840000
              840000840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFF0000000000
              00000000000000840000FFFFFFFFFFFFFFFFFFFFFFFF840000FFFFFF840000D8
              E9ECD8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFF840000FFFFFFFFFFFFFFFFFFFFFFFF840000840000D8E9ECD8E9ECD8E9
              ECD8E9ECD8E9EC000000D8E9EC000000FFFFFF000000000000FFFFFF00000084
              0000840000840000840000840000840000D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
              D8E9EC000000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF0000
              00D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC00
              0000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF000000000000D8E9ECD8E9EC
              D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9
              EC000000000000000000000000000000000000D8E9ECD8E9ECD8E9ECD8E9ECD8
              E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9EC
              D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9
              ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8
              E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
              D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9
              ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8
              E9ECD8E9ECD8E9ECD8E9EC000000}
            ParentShowHint = False
            ShowHint = True
            OnClick = btnDatosClientePagareClick
          end
          object Label9: TLabel
            Left = 27
            Top = 145
            Width = 90
            Height = 13
            Alignment = taRightJustify
            Caption = 'Nro. Repactaci'#243'n :'
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label3: TLabel
            Left = 72
            Top = 169
            Width = 45
            Height = 13
            Alignment = taRightJustify
            Caption = 'Importe :'
          end
          object txtPagareNumero: TEdit
            Left = 126
            Top = 94
            Width = 238
            Height = 21
            CharCase = ecUpperCase
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            MaxLength = 20
            ParentFont = False
            TabOrder = 3
          end
          object cbPagareBancos: TVariantComboBox
            Left = 126
            Top = 70
            Width = 238
            Height = 21
            Style = vcsDropDownList
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ItemHeight = 13
            ParentFont = False
            TabOrder = 2
            Items = <>
          end
          object txtPagareNombreTitular: TEdit
            Left = 126
            Top = 22
            Width = 361
            Height = 21
            CharCase = ecUpperCase
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            MaxLength = 100
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
          end
          object txtPagareDocumento: TEdit
            Left = 126
            Top = 46
            Width = 108
            Height = 21
            Hint = 'RUT'
            CharCase = ecUpperCase
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            MaxLength = 9
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
          end
          object dePagareFecha: TDateEdit
            Left = 126
            Top = 118
            Width = 90
            Height = 21
            AutoSelect = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 4
            Date = -693594.000000000000000000
          end
          object edtRepactacion: TEdit
            Left = 126
            Top = 142
            Width = 238
            Height = 21
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 5
          end
          object nedtPagareImporte: TNumericEdit
            Left = 126
            Top = 166
            Width = 108
            Height = 21
            TabOrder = 6
          end
        end
        object TPage
          Left = 0
          Top = 0
          Caption = 'Cheques'
          ExplicitWidth = 0
          ExplicitHeight = 0
        end
        object TPage
          Left = 0
          Top = 0
          Caption = 'Efectivo'
          object Label14: TLabel
            Left = 111
            Top = 44
            Width = 95
            Height = 13
            Caption = 'Importe Entregado:'
          end
          object Label15: TLabel
            Left = 167
            Top = 68
            Width = 39
            Height = 13
            Caption = 'Cambio:'
          end
          object nedtImporteEntregado: TNumericEdit
            Left = 212
            Top = 41
            Width = 121
            Height = 21
            TabOrder = 0
            OnChange = nedtImporteEntregadoChange
          end
          object nedtCambio: TNumericEdit
            Left = 212
            Top = 65
            Width = 121
            Height = 21
            Color = clInfoBk
            TabOrder = 1
          end
        end
        object TPage
          Left = 0
          Top = 0
          Caption = 'SeleccioneFormaPago'
          object Label10: TLabel
            Left = 136
            Top = 81
            Width = 249
            Height = 23
            Caption = 'Seleccione Forma de Pago'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -19
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
        end
      end
    end
    object vcbCanalesPago: TVariantComboBox
      Left = 19
      Top = 36
      Width = 180
      Height = 21
      Style = vcsDropDownList
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ItemHeight = 13
      ParentFont = False
      TabOrder = 4
      Items = <>
    end
    object pnlPago1: TPanel
      Left = 413
      Top = 13
      Width = 205
      Height = 52
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
      object lbl_Moneda: TLabel
        Left = 6
        Top = 6
        Width = 70
        Height = 13
        Caption = 'Monto a Pagar'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lbl_MontoPagado: TLabel
        Left = 100
        Top = 6
        Width = 69
        Height = 13
        Caption = 'Monto Pagado'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object neImporte: TNumericEdit
        Left = 5
        Top = 23
        Width = 90
        Height = 21
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 0
      end
      object neImportePagado: TNumericEdit
        Left = 99
        Top = 23
        Width = 90
        Height = 21
        TabStop = False
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 1
        OnChange = neImportePagadoChange
      end
    end
  end
  object spValidarFormaDePagoEnCanal: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ValidarFormaDePagoEnCanal;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@CodigoTipoMedioPago'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@CodigoEntradaUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
      end
      item
        Name = '@ValidarPagoAnticipado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
      end
      item
        Name = '@CodigoSistema'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
      end
      item
        Name = '@CodigoFormaDePago'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
      end
      item
        Name = '@DescripcionFormaDePago'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 30
      end
      item
        Name = '@EsValidaEnCanal'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
      end
      item
        Name = '@AceptaPagoParcial'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
      end
      item
        Name = '@CodigoPantalla'
        Attributes = [paNullable]
        DataType = ftSmallint
        Direction = pdInputOutput
        Precision = 5
      end
      item
        Name = '@CodigoBanco'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
      end
      item
        Name = '@CodigoTarjeta'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
      end>
    Left = 496
    Top = 296
  end
end
