object RefinanciacionConsultasForm: TRefinanciacionConsultasForm
  Left = 0
  Top = 0
  Caption = ' Mantenimimento Refinanciaciones'
  ClientHeight = 572
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 87
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      1008
      87)
    object GroupBox1: TGroupBox
      Left = 6
      Top = 4
      Width = 996
      Height = 77
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = '  Filtros B'#250'squeda Refinanciaciones  '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      DesignSize = (
        996
        77)
      object lb_CodigoCliente: TLabel
        Left = 20
        Top = 16
        Width = 60
        Height = 13
        Caption = 'RUT Cliente:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label2: TLabel
        Left = 171
        Top = 16
        Width = 49
        Height = 13
        Caption = 'Convenio:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label3: TLabel
        Left = 538
        Top = 16
        Width = 25
        Height = 13
        Caption = 'Nro.:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label1: TLabel
        Left = 390
        Top = 16
        Width = 24
        Height = 13
        Caption = 'Tipo:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblNombreCli: TLabel
        Left = 26
        Top = 55
        Width = 70
        Height = 13
        Caption = 'lblNombreCli'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblPersoneria: TLabel
        Left = 105
        Top = 55
        Width = 61
        Height = 13
        Caption = 'lblPersoneria'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblEnListaAmarilla: TLabel
        Left = 218
        Top = 0
        Width = 200
        Height = 14
        Alignment = taCenter
        AutoSize = False
        Color = clYellow
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Transparent = False
        Visible = False
      end
      object peRut: TPickEdit
        Left = 18
        Top = 30
        Width = 145
        Height = 21
        CharCase = ecUpperCase
        Enabled = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnChange = peRutChange
        OnKeyPress = peRutKeyPress
        EditorStyle = bteTextEdit
        OnButtonClick = peRutButtonClick
      end
      object vcbConvenios: TVariantComboBox
        Left = 169
        Top = 30
        Width = 203
        Height = 19
        Style = vcsOwnerDrawFixed
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ItemHeight = 13
        ParentFont = False
        TabOrder = 1
        OnChange = vcbConveniosChange
        OnDrawItem = vcbConveniosDrawItem
        OnKeyPress = vcbConveniosKeyPress
        Items = <>
      end
      object nedtRefinanciacion: TNumericEdit
        Left = 536
        Top = 30
        Width = 87
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        OnKeyPress = nedtRefinanciacionKeyPress
      end
      object btnFiltrar: TButton
        Left = 911
        Top = 28
        Width = 75
        Height = 25
        Anchors = [akRight, akBottom]
        Caption = 'Buscar'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
        OnClick = btnFiltrarClick
      end
      object vcbTipoRefinanciacion: TVariantComboBox
        Left = 388
        Top = 30
        Width = 145
        Height = 21
        Hint = ' Seleccionar Tipo de Refinanciaci'#243'n '
        Style = vcsDropDownList
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ItemHeight = 13
        ItemIndex = 0
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        OnKeyPress = vcbTipoRefinanciacionKeyPress
        Items = <
          item
            Caption = 'Todas'
            Value = 0
          end
          item
            Caption = 'Avenimiento'
            Value = 38
          end
          item
            Caption = 'Repactaci'#243'n'
            Value = 36
          end>
        Value = 0
        Text = 'Todas'
      end
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 553
    Width = 1008
    Height = 19
    Panels = <>
  end
  object Panel3: TPanel
    Left = 0
    Top = 513
    Width = 1008
    Height = 40
    Align = alBottom
    Color = 14408667
    ParentBackground = False
    TabOrder = 2
    DesignSize = (
      1008
      40)
    object Button5: TButton
      Left = 921
      Top = 9
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Salir'
      TabOrder = 0
      OnClick = Button5Click
    end
  end
  object Panel4: TPanel
    Left = 0
    Top = 87
    Width = 1008
    Height = 426
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    DesignSize = (
      1008
      426)
    object GroupBox2: TGroupBox
      Left = 6
      Top = 1
      Width = 996
      Height = 421
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = '  Resultados B'#250'squeda  '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      DesignSize = (
        996
        421)
      object Panel2: TPanel
        Left = 6
        Top = 16
        Width = 984
        Height = 34
        Anchors = [akLeft, akTop, akRight]
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Color = 14408667
        ParentBackground = False
        TabOrder = 0
        object btnNueva: TButton
          Left = 5
          Top = 5
          Width = 110
          Height = 25
          Caption = 'Nueva'
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          OnClick = btnNuevaClick
        end
        object btnEditar: TButton
          Left = 117
          Top = 5
          Width = 110
          Height = 25
          Caption = 'Editar / Consultar'
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          OnClick = btnEditarClick
        end
        object btnCuotas: TButton
          Left = 580
          Top = 5
          Width = 110
          Height = 25
          Caption = 'Gesti'#243'n Cuotas'
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          OnClick = btnCuotasClick
        end
        object btnAnular: TButton
          Left = 341
          Top = 5
          Width = 110
          Height = 25
          Caption = 'Anular'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
          OnClick = btnAnularClick
        end
        object btnATramite: TButton
          Left = 229
          Top = 5
          Width = 110
          Height = 25
          Caption = 'Enviar a Tr'#225'mite'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 4
          OnClick = btnATramiteClick
        end
        object btnProtestar: TButton
          Left = 453
          Top = 5
          Width = 110
          Height = 25
          Caption = 'Protestar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 5
          OnClick = btnProtestarClick
        end
        object btnRecibos: TButton
          Left = 693
          Top = 5
          Width = 110
          Height = 25
          Caption = 'Gesti'#243'n Recibos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 6
          OnClick = btnRecibosClick
        end
        object btnReversar: TButton
          Left = 865
          Top = 5
          Width = 110
          Height = 25
          Caption = 'Reversar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 7
          OnClick = btnReversarClick
        end
        object btnPagar: TButton
          Left = 784
          Top = 5
          Width = 110
          Height = 25
          Caption = 'Pagar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 8
          OnClick = btnPagarClick
        end
      end
      object DBListEx1: TDBListEx
        Left = 3
        Top = 52
        Width = 985
        Height = 364
        Anchors = [akLeft, akTop, akRight, akBottom]
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 45
            Header.Caption = 'Nro.'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taRightJustify
            IsLink = False
            OnHeaderClick = DBListEx1Columns0HeaderClick
            FieldName = 'CodigoRefinanciacion'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 85
            Header.Caption = 'Tipo'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clBlack
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            OnHeaderClick = DBListEx1Columns0HeaderClick
            FieldName = 'Tipo'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 80
            Header.Caption = 'Fecha'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            Sorting = csDescending
            IsLink = False
            OnHeaderClick = DBListEx1Columns0HeaderClick
            FieldName = 'Fecha'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 160
            Header.Caption = 'Estado'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            OnHeaderClick = DBListEx1Columns0HeaderClick
            FieldName = 'Estado'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 60
            Header.Caption = 'Validada'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'Validada'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 70
            Header.Caption = 'Rut'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            OnHeaderClick = DBListEx1Columns0HeaderClick
            FieldName = 'NumeroDocumento'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 175
            Header.Caption = 'Cliente'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'NombrePersona'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 110
            Header.Caption = 'Convenio'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            OnHeaderClick = DBListEx1Columns0HeaderClick
            FieldName = 'Convenio'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 90
            Header.Caption = 'Total Deuda'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taRightJustify
            IsLink = False
            FieldName = 'TotalDeuda'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 90
            Header.Caption = 'Total Pagado'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taRightJustify
            IsLink = False
            FieldName = 'TotalPagado'
          end>
        DataSource = dsRefinanciaciones
        DragReorder = True
        ParentColor = False
        PopupMenu = PopupMenu1
        TabOrder = 1
        TabStop = True
        OnDblClick = DBListEx1DblClick
        OnDrawText = DBListEx1DrawText
      end
    end
  end
  object spObtenerRefinanciacionesConsultas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerRefinanciacionesConsultas'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@RUTPersona'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoMedioPago'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoEstado'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@SoloNoActivadas'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 600
    Top = 176
  end
  object cdsRefinanciaciones: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoRefinanciacion'
        DataType = ftInteger
      end
      item
        Name = 'CodigoPersona'
        DataType = ftInteger
      end
      item
        Name = 'CodigoTipoMedioPago'
        DataType = ftSmallint
      end
      item
        Name = 'Tipo'
        Attributes = [faFixed]
        DataType = ftString
        Size = 30
      end
      item
        Name = 'Fecha'
        DataType = ftDateTime
      end
      item
        Name = 'Estado'
        Attributes = [faReadonly]
        DataType = ftString
        Size = 69
      end
      item
        Name = 'NumeroDocumento'
        Attributes = [faFixed]
        DataType = ftString
        Size = 11
      end
      item
        Name = 'Convenio'
        Attributes = [faFixed]
        DataType = ftString
        Size = 30
      end
      item
        Name = 'TotalDeuda'
        Attributes = [faReadonly]
        DataType = ftString
        Size = 20
      end
      item
        Name = 'TotalPagado'
        Attributes = [faReadonly]
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Validada'
        DataType = ftBoolean
      end
      item
        Name = 'CodigoEstado'
        DataType = ftSmallint
      end
      item
        Name = 'Protestada'
        DataType = ftBoolean
      end
      item
        Name = 'NombrePersona'
        Attributes = [faReadonly]
        DataType = ftString
        Size = 200
      end
      item
        Name = 'CodigoRefinanciacionUnificada'
        DataType = ftInteger
      end>
    IndexDefs = <
      item
        Name = 'Fecha'
        Fields = 'Fecha; CodigoRefinanciacion'
      end
      item
        Name = 'FechaDESC'
        Fields = 'Fecha; CodigoRefinanciacion'
        Options = [ixDescending]
      end
      item
        Name = 'CodigoRefinanciacion'
        Fields = 'CodigoRefinanciacion'
      end
      item
        Name = 'CodigoRefinanciacionDESC'
        Fields = 'CodigoRefinanciacion'
        Options = [ixDescending]
      end
      item
        Name = 'NumeroDocumento'
        Fields = 'NumeroDocumento'
      end
      item
        Name = 'NumeroDocumentoDESC'
        Fields = 'NumeroDocumento'
        Options = [ixDescending]
      end
      item
        Name = 'Estado'
        Fields = 'Estado'
      end
      item
        Name = 'EstadoDESC'
        Fields = 'Estado'
        Options = [ixDescending]
      end
      item
        Name = 'Tipo'
        Fields = 'Tipo'
      end
      item
        Name = 'TipoDESC'
        Fields = 'Tipo'
        Options = [ixDescending]
      end
      item
        Name = 'Convenio'
        Fields = 'Convenio'
      end
      item
        Name = 'ConvenioDESC'
        Fields = 'Convenio'
        Options = [ixDescending]
      end>
    IndexName = 'FechaDESC'
    Params = <>
    ProviderName = 'dspObtenerRefinanciacionesConsultas'
    StoreDefs = True
    AfterScroll = cdsRefinanciacionesAfterScroll
    Left = 448
    Top = 240
    object cdsRefinanciacionesCodigoRefinanciacion: TIntegerField
      FieldName = 'CodigoRefinanciacion'
    end
    object cdsRefinanciacionesCodigoPersona: TIntegerField
      FieldName = 'CodigoPersona'
    end
    object cdsRefinanciacionesTipo: TStringField
      FieldName = 'Tipo'
      FixedChar = True
      Size = 30
    end
    object cdsRefinanciacionesFecha: TDateTimeField
      FieldName = 'Fecha'
    end
    object cdsRefinanciacionesEstado: TStringField
      FieldName = 'Estado'
      Size = 30
    end
    object cdsRefinanciacionesNumeroDocumento: TStringField
      FieldName = 'NumeroDocumento'
      FixedChar = True
      Size = 11
    end
    object cdsRefinanciacionesNombrePersona: TStringField
      FieldName = 'NombrePersona'
      ReadOnly = True
      Size = 100
    end
    object cdsRefinanciacionesConvenio: TStringField
      FieldName = 'Convenio'
      ReadOnly = True
      FixedChar = True
      Size = 30
    end
    object cdsRefinanciacionesTotalDeuda: TStringField
      FieldName = 'TotalDeuda'
      ReadOnly = True
    end
    object cdsRefinanciacionesTotalPagado: TStringField
      FieldName = 'TotalPagado'
      ReadOnly = True
    end
    object cdsRefinanciacionesValidada: TBooleanField
      FieldName = 'Validada'
    end
    object cdsRefinanciacionesCodigoEstado: TSmallintField
      FieldName = 'CodigoEstado'
    end
    object cdsRefinanciacionesProtestada: TBooleanField
      FieldName = 'Protestada'
    end
    object cdsRefinanciacionesCodigoRefinanciacionUnificada: TIntegerField
      FieldName = 'CodigoRefinanciacionUnificada'
    end
  end
  object dsRefinanciaciones: TDataSource
    DataSet = cdsRefinanciaciones
    Left = 432
    Top = 257
  end
  object dspObtenerRefinanciacionesConsultas: TDataSetProvider
    DataSet = spObtenerRefinanciacionesConsultas
    Left = 560
    Top = 192
  end
  object PopupMenu1: TPopupMenu
    Left = 720
    Top = 256
    object CopiarRUTalPortapapeles1: TMenuItem
      Caption = 'Copiar RUT al Portapapeles'
      OnClick = CopiarRUTalPortapapeles1Click
    end
    object CopiarNrodeConvenioalPortapapeles1: TMenuItem
      Caption = 'Copiar Nro. de Convenio al Portapapeles'
      OnClick = CopiarNrodeConvenioalPortapapeles1Click
    end
  end
  object lnCheck: TImageList
    BkColor = clWhite
    Height = 15
    Width = 15
    Left = 119
    Top = 307
    Bitmap = {
      494C01010200040060000F000F00FFFFFF00FF10FFFFFFFFFFFFFFFF424D3600
      00000000000036000000280000003C0000000F0000000100200000000000100E
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF000000
      000000000000FFFFFF00000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000424D3E000000000000003E000000280000003C0000000F00000001000100
      00000000780000000000000000000000000000000000000000000000FFFFFF00
      FFFFFFFC00000000800300040000000080030004000000008003000400000000
      8003000400000000800300040000000080030004000000008003000400000000
      8003000400000000800300040000000080030004000000008003000400000000
      80030004000000008003000400000000FFFFFFFC000000000000000000000000
      0000000000000000000000000000}
  end
  object spActualizarRefinanciacionEstado: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarRefinanciacionEstado'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoEstado'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 864
    Top = 208
  end
  object spObtenerRefinanciacionDatosCabecera: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerRefinanciacionDatosCabecera'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 712
    Top = 336
  end
  object spActualizarRefinanciacionUnificadaEstado: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarRefinanciacionUnificadaEstado'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRefinanciacionUnificada'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoEstado'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 496
    Top = 360
  end
  object spReversarRefinanciacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CommandTimeout = 400
    ProcedureName = 'ActualizarRefinanciacionReversa'
    Parameters = <>
    Left = 888
    Top = 288
  end
end
