object AbmAccionesDeCobranzaForm: TAbmAccionesDeCobranzaForm
  Left = 106
  Top = 61
  ClientHeight = 567
  ClientWidth = 859
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GrupoDatos: TPanel
    Left = 0
    Top = 232
    Width = 859
    Height = 296
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 0
    DesignSize = (
      859
      296)
    object lblPlantillayTipoCaso: TLabel
      Left = 11
      Top = 154
      Width = 96
      Height = 13
      Caption = 'lblPlantillayTipoCaso'
      FocusControl = txtPlantillayTipoCaso
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label8: TLabel
      Left = 11
      Top = 17
      Width = 37
      Height = 13
      Alignment = taRightJustify
      Caption = 'C'#243'digo:'
      FocusControl = txtCodigo
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblTipoDeCliente: TLabel
      Left = 11
      Top = 120
      Width = 73
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tipo de cliente:'
      FocusControl = txtTipoDeCliente
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblNombre: TLabel
      Left = 149
      Top = 17
      Width = 48
      Height = 13
      Caption = 'Nombre:'
      FocusControl = txtNombre
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblMontoMinimoDeDeuda: TLabel
      Left = 11
      Top = 51
      Width = 117
      Height = 13
      Alignment = taRightJustify
      Caption = 'Monto m'#237'nimo de deuda:'
      FocusControl = txtMontoMinimoDeDeuda
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblMontoMaximoDeDeuda: TLabel
      Left = 269
      Top = 51
      Width = 121
      Height = 13
      Alignment = taRightJustify
      Caption = 'Monto m'#225'ximo de deuda:'
      FocusControl = txtMontoMaximoDeDeuda
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblEdadDeDeuda: TLabel
      Left = 528
      Top = 51
      Width = 106
      Height = 13
      Alignment = taRightJustify
      Caption = 'Edad de deuda (d'#237'as):'
      FocusControl = txtEdadDeDeuda
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblTipoDeAccionDeCobranza: TLabel
      Left = 372
      Top = 85
      Width = 83
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tipo de acci'#243'n:'
      FocusControl = txtTipoDeAccionDeCobranza
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblTipoDeCobranza: TLabel
      Left = 11
      Top = 85
      Width = 99
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tipo de cobranza:'
      FocusControl = txtTipoDeCobranza
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblCategoriaDeDeudaDeCobranza: TLabel
      Left = 359
      Top = 120
      Width = 99
      Height = 13
      Alignment = taRightJustify
      Caption = 'Categoria de deuda:'
      FocusControl = txtCategoriaDeDeudaDeCobranza
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblScriptTelefonico: TLabel
      Left = 12
      Top = 183
      Width = 79
      Height = 13
      Caption = 'Script telef'#243'nico:'
      FocusControl = txtScriptTelefonico
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object txtCodigo: TNumericEdit
      Left = 54
      Top = 14
      Width = 69
      Height = 21
      Color = clBtnFace
      Enabled = False
      ReadOnly = True
      TabOrder = 0
    end
    object txtTipoDeCliente: TVariantComboBox
      Left = 116
      Top = 117
      Width = 230
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 7
      Items = <>
    end
    object txtNombre: TEdit
      Left = 203
      Top = 14
      Width = 485
      Height = 21
      Color = 16444382
      MaxLength = 200
      TabOrder = 1
    end
    object txtMontoMinimoDeDeuda: TNumericEdit
      Left = 134
      Top = 48
      Width = 50
      Height = 21
      TabOrder = 2
      Decimals = 2
      BlankWhenZero = False
    end
    object txtMontoMaximoDeDeuda: TNumericEdit
      Left = 396
      Top = 48
      Width = 50
      Height = 21
      TabOrder = 3
      Decimals = 2
      BlankWhenZero = False
    end
    object txtEdadDeDeuda: TNumericEdit
      Left = 640
      Top = 48
      Width = 50
      Height = 21
      TabOrder = 4
      BlankWhenZero = False
    end
    object txtTipoDeAccionDeCobranza: TVariantComboBox
      Left = 462
      Top = 82
      Width = 230
      Height = 21
      Style = vcsDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 6
      OnChange = txtTipoDeAccionDeCobranzaChange
      Items = <>
    end
    object txtTipoDeCobranza: TVariantComboBox
      Left = 116
      Top = 82
      Width = 230
      Height = 21
      Style = vcsDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 5
      OnChange = txtTipoDeCobranzaChange
      Items = <>
    end
    object txtCategoriaDeDeudaDeCobranza: TVariantComboBox
      Left = 462
      Top = 117
      Width = 230
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 8
      Items = <>
    end
    object txtScriptTelefonico: TMemo
      Left = 11
      Top = 199
      Width = 839
      Height = 92
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 10
    end
    object txtPlantillayTipoCaso: TVariantComboBox
      Left = 116
      Top = 151
      Width = 576
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 9
      Items = <>
    end
  end
  object PAbajo: TPanel
    Left = 0
    Top = 528
    Width = 859
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Notebook: TNotebook
      Left = 662
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TButton
          Left = 110
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object AbmToolbar: TAbmToolbar
    Left = 0
    Top = 0
    Width = 859
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar]
    OnClose = AbmToolbarClose
  end
  object DBListDatos: TAbmList
    Left = 0
    Top = 33
    Width = 859
    Height = 199
    TabStop = True
    TabOrder = 3
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -12
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'52'#0'C'#243'digo'
      #0'207'#0'Nombre'
      #0'87'#0'Monto m'#237'nimo'
      #0'85'#0'Monto m'#225'ximo'
      #0'33'#0'Edad')
    HScrollBar = True
    RefreshTime = 100
    Table = spListar
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = DBListDatosClick
    OnProcess = DBListDatosProcess
    OnDrawItem = DBListDatosDrawItem
    OnRefresh = DBListDatosRefresh
    OnInsert = DBListDatosInsert
    OnDelete = DBListDatosDelete
    OnEdit = DBListDatosEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar
  end
  object spAlmacenar: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'AccionesDeCobranza_Almacenar'
    Parameters = <>
    Left = 412
    Top = 112
  end
  object spEliminar: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'AccionesDeCobranza_Eliminar'
    Parameters = <>
    Left = 448
    Top = 112
  end
  object spListar: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'AccionesDeCobranza_Listar'
    Parameters = <>
    Left = 376
    Top = 112
  end
  object spTiposDeAccionesDeCobranza_Listar: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'TiposDeAccionesDeCobranza_Listar'
    Parameters = <>
    Left = 408
    Top = 184
  end
end
