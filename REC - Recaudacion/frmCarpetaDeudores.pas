{-----------------------------------------------------------------------------
 Unit Name: frmCarpetaDeudores
 Autor:    	cfuentesc
 Fecha:     20/03/2017
 Prop�sito: Gestionar convenios morosos aplicando acciones de cobranzas 
 			CU.COBO.COB.201
 Firma: 	TASK_124_CFU_20170320-Gestionar_Carpeta_Deudores
 Historia:
-----------------------------------------------------------------------------}
unit frmCarpetaDeudores;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, StdCtrls, VariantComboBox, DmiCtrls, BuscaClientes,
  DB, DBClient, ADODB, DMConnection, UtilDB, PeaProcs, Util, UtilProc, 
  ListBoxEx, DBListEx, SysUtilsCN, ImgList, PeaTypes, UtilFacturacion, 
  Provider, AccionesDeCobrazaBusiness, XMLCAC;

type
  TFormaPago = (tpCastigo);

  TFormCarpetaDeudores = class(TForm)
    pcCarpetaDeudores: TPageControl;
    tsCobranzaInterna: TTabSheet;
    tsCobranzaExternaExtraJudicial: TTabSheet;
    tsCobranzaExternaJudicial: TTabSheet;
    Panel2: TPanel;
    Label1: TLabel;
    vcbAccionDeCobranzaInterna: TVariantComboBox;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    chbCandidatoInhabilitacionCobInterna: TCheckBox;
    lb_CodigoCliente: TLabel;
    peNumeroDocumentoCobInterna: TPickEdit;
    lbl_NumeroConvenio: TLabel;
    cbConveniosCobInterna: TVariantComboBox;
    spObtenerConveniosMorososCobInterna: TADOStoredProc;
    cdsObtenerConveniosMorososCobInterna: TClientDataSet;
    cdsObtenerConveniosMorososCobInternaMarcar: TBooleanField;
    cdsObtenerConveniosMorososCobInternaCodigoConvenio: TIntegerField;
    cdsObtenerConveniosMorososCobInternaNumeroDocumento: TStringField;
    cdsObtenerConveniosMorososCobInternaNombrePersona: TStringField;
    cdsObtenerConveniosMorososCobInternaNumeroConvenio: TStringField;
    cdsObtenerConveniosMorososCobInternaConcesionaria: TStringField;
    cdsObtenerConveniosMorososCobInternaTipoCliente: TStringField;
    cdsObtenerConveniosMorososCobInternaSaldo: TLargeintField;
    cdsObtenerConveniosMorososCobInternaDiasDeuda: TIntegerField;
    cdsObtenerConveniosMorososCobInternaCodigoCliente: TIntegerField;
    dsObtenerConveniosMorososCobInterna: TDataSource;
    btnLimpiarCobInterna: TButton;
    btnBuscarCobInterna: TButton;
    edMontoMinimoDeudaCobInterna: TNumericEdit;
    edEdadDeudaCobInterna: TNumericEdit;
    edMontoMaximoDeudaCobInterna: TNumericEdit;
    vcbTipoClienteCobInterna: TVariantComboBox;
    vcbCategoriaDeudaCobInterna: TVariantComboBox;
    spObtenerTiposDeCliente: TADOStoredProc;
    Panel1: TPanel;
    Panel3: TPanel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    vcbAccionDeCobranzaExtraJudicial: TVariantComboBox;
    chbCandidatoInhabilitacionCobExtraJudicial: TCheckBox;
    peNumeroDocumentoCobExtraJudicial: TPickEdit;
    cbConveniosCobExtraJudicial: TVariantComboBox;
    btnLimpiarCobExtraJudicial: TButton;
    btnBuscarCobExtraJudicial: TButton;
    edMontoMinimoDeudaCobExtraJudicial: TNumericEdit;
    edEdadDeudaCobExtraJudicial: TNumericEdit;
    edMontoMaximoDeudaCobExtraJudicial: TNumericEdit;
    cbTipoClienteCobExtraJudicial: TVariantComboBox;
    vcbCategoriaDeudaCobExtraJudicial: TVariantComboBox;
    Panel5: TPanel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    vcbAccionDeCobranzaJudicial: TVariantComboBox;
    chbCandidatoInhabilitacionCobJudicial: TCheckBox;
    peNumeroDocumentoCobJudicial: TPickEdit;
    cbConveniosCobJudicial: TVariantComboBox;
    btnLimpiarCobJudicial: TButton;
    btnBuscarCobJudicial: TButton;
    edMontoMinimoDeudaCobJudicial: TNumericEdit;
    edEdadDeudaCobJudicial: TNumericEdit;
    edMontoMaximoDeudaCobJudicial: TNumericEdit;
    cbTipoClienteCobJudicial: TVariantComboBox;
    vcbCategoriaDeudaCobJudicial: TVariantComboBox;
    btnEjecutarAccionCobInterna: TButton;
    dlMorososCobInterna: TDBListEx;
    dlMorososCobExtraJudicial: TDBListEx;
    dlMorososCobJudicial: TDBListEx;
    Panel7: TPanel;
    btnSeleccionarTodoCobInterna: TButton;
    btnLiberarSeleccionCobInterna: TButton;
    btnHistorialCobInterna: TButton;
    Panel8: TPanel;
    btnSeleccionarTodoCobExtraJudicial: TButton;
    btnLiberarSeleccionCobExtraJudicial: TButton;
    btnHistorialCobExtraJudicial: TButton;
    Panel9: TPanel;
    btnSeleccionarTodoCobJudicial: TButton;
    btnLiberarSeleccionCobJudicial: TButton;
    btnHistorialCobJudicial: TButton;
    spObtenerConveniosMorososCobExtraJudicial: TADOStoredProc;
    cdsObtenerConveniosMorososCobExtraJudicial: TClientDataSet;
    cdsObtenerConveniosMorososCobExtraJudicialMarcar: TBooleanField;
    cdsObtenerConveniosMorososCobExtraJudicialCodigoConvenio: TIntegerField;
    cdsObtenerConveniosMorososCobExtraJudicialNumeroDocumento: TStringField;
    cdsObtenerConveniosMorososCobExtraJudicialNombrePersona: TStringField;
    cdsObtenerConveniosMorososCobExtraJudicialNumeroConvenio: TStringField;
    cdsObtenerConveniosMorososCobExtraJudicialConcesionaria: TStringField;
    cdsObtenerConveniosMorososCobExtraJudicialTipoCliente: TStringField;
    cdsObtenerConveniosMorososCobExtraJudicialSaldo: TLargeintField;
    cdsObtenerConveniosMorososCobExtraJudicialDiasDeuda: TIntegerField;
    cdsObtenerConveniosMorososCobExtraJudicialCodigoCliente: TIntegerField;
    cdsObtenerConveniosMorososCobExtraJudicialFechaVencimiento: TDateField;
    cdsObtenerConveniosMorososCobExtraJudicialAccionCobranza: TStringField;
    cdsObtenerConveniosMorososCobExtraJudicialEstadoAccionCobranza: TStringField;
    cdsObtenerConveniosMorososCobExtraJudicialCantComprobantes: TIntegerField;
    dsObtenerConveniosMorososCobExtraJudicial: TDataSource;
    spObtenerConveniosMorososCobJudicial: TADOStoredProc;
    cdsObtenerConveniosMorososCobJudicial: TClientDataSet;
    cdsObtenerConveniosMorososCobJudicialMarcar: TBooleanField;
    cdsObtenerConveniosMorososCobJudicialCodigoConvenio: TIntegerField;
    cdsObtenerConveniosMorososCobJudicialNumeroDocumento: TStringField;
    cdsObtenerConveniosMorososCobJudicialNombrePersona: TStringField;
    cdsObtenerConveniosMorososCobJudicialNumeroConvenio: TStringField;
    cdsObtenerConveniosMorososCobJudicialConcesionaria: TStringField;
    cdsObtenerConveniosMorososCobJudicialTipoCliente: TStringField;
    cdsObtenerConveniosMorososCobJudicialSaldo: TLargeintField;
    cdsObtenerConveniosMorososCobJudicialDiasDeuda: TIntegerField;
    cdsObtenerConveniosMorososCobJudicialCodigoCliente: TIntegerField;
    dsObtenerConveniosMorososCobJudicial: TDataSource;
    spObtenerAccionesDeCobranza: TADOStoredProc;
    Label23: TLabel;
    cbbTipoAccionCobranzaInterna: TVariantComboBox;
    btnSeleccionarPlantillaCobInterna: TButton;
    btnSeleccionarMensajeCobInterna: TButton;
    btnExportarDatosACSVCobInterna: TButton;
    btnHistoricoCobInterna: TButton;
    btnDefinirTipoClienteCobInterna: TButton;
    Splitter1: TSplitter;
    Panel10: TPanel;
    btnSalir: TButton;
    lblAccionCobranzaInterna: TLabel;
    lblPersonaCobInterna: TLabel;
    lblPersonaCobExtraJudicial: TLabel;
    lblPersonaCobJudicial: TLabel;
    Panel4: TPanel;
    Label24: TLabel;
    Splitter2: TSplitter;
    lblAccionCobranzaExtraJudicial: TLabel;
    btnEjecutarAccionCobExtraJudicial: TButton;
    cbbTipoAccionCobranzaExtraJudicial: TVariantComboBox;
    btnExportarDatosACSVCobExtraJudicial: TButton;
    btnHistoricoCobExtraJudicial: TButton;
    btnDefinirTipoClienteCobExtraJudicial: TButton;
    Panel6: TPanel;
    Label26: TLabel;
    Splitter3: TSplitter;
    lblAccionCobranzaJudicial: TLabel;
    btnEjecutarAccionCobJudicial: TButton;
    cbbTipoAccionCobranzaJudicial: TVariantComboBox;
    btnHistoricoCobJudicial: TButton;
    btnDefinirTipoClienteCobJudicial: TButton;
    lnCheck: TImageList;
    spGenerarNotificacionesCobranza: TADOStoredProc;
    spCastigarConvenio: TADOStoredProc;
    spObtenerComprobantesImpagos: TADOStoredProc;
    cds_Comprobantes: TClientDataSet;
    spObtenerRefinanciacionesConsultas: TADOStoredProc;
    dspObtenerRefinanciacionesConsultas: TDataSetProvider;
    cdsRefinanciaciones: TClientDataSet;
    cdsRefinanciacionesCodigoRefinanciacion: TIntegerField;
    cdsRefinanciacionesCodigoPersona: TIntegerField;
    cdsRefinanciacionesTipo: TStringField;
    cdsRefinanciacionesFecha: TDateTimeField;
    cdsRefinanciacionesEstado: TStringField;
    cdsRefinanciacionesNumeroDocumento: TStringField;
    cdsRefinanciacionesNombrePersona: TStringField;
    cdsRefinanciacionesConvenio: TStringField;
    cdsRefinanciacionesTotalDeuda: TStringField;
    cdsRefinanciacionesTotalPagado: TStringField;
    cdsRefinanciacionesValidada: TBooleanField;
    cdsRefinanciacionesCodigoEstado: TSmallintField;
    cdsRefinanciacionesProtestada: TBooleanField;
    cdsRefinanciacionesCodigoRefinanciacionUnificada: TIntegerField;
    spGuardarCampannas: TADOStoredProc;
    spVerificarConvenioCandidato: TADOStoredProc;
    btnExportarDatosACSVCobJudicial: TButton;
    tsDeudores: TTabSheet;
    Panel11: TPanel;
    Label25: TLabel;
    Splitter4: TSplitter;
    lblAccionDeudores: TLabel;
    btnSeleccionarMensajeDeudores: TButton;
    btnSeleccionarPlantillaDeudores: TButton;
    btnEjecutarAccionDeudores: TButton;
    cbbTipoAccionDeudores: TVariantComboBox;
    btnExportarDatosACSVDeudores: TButton;
    btnHistoricoDeudores: TButton;
    btnDefinirTipoClienteDeudores: TButton;
    Panel12: TPanel;
    btnSeleccionarTodoDeudores: TButton;
    btnLiberarSeleccionDeudores: TButton;
    btnHistorialDeudores: TButton;
    dlDeudores: TDBListEx;
    Panel13: TPanel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    lblPersonaDeudores: TLabel;
    vcbAccionDeCobranzaDeudores: TVariantComboBox;
    chbCandidatoInhabilitacionDeudores: TCheckBox;
    peNumeroDocumentoDeudores: TPickEdit;
    cbConveniosDeudores: TVariantComboBox;
    btnLimpiarCobDeudores: TButton;
    btnBuscarDeudores: TButton;
    edMontoMinimoDeudaDeudores: TNumericEdit;
    edEdadDeudaDeudores: TNumericEdit;
    edMontoMaximoDeudaDeudores: TNumericEdit;
    vcbTipoClienteDeudores: TVariantComboBox;
    vcbCategoriaDeudaDeudores: TVariantComboBox;
    rbMostrarTodo: TRadioButton;
    rbMostrarSinAcciones: TRadioButton;
    spCategoriasDeDeudasDeCobranza_Listar: TADOStoredProc;
    spActualizarTipoClienteConvenio: TADOStoredProc;
    spObtenerConveniosMorososDeudores: TADOStoredProc;
    dsObtenerConveniosMorososDeudores: TDataSource;
    cdsObtenerConveniosMorososDeudores: TClientDataSet;
    cdsObtenerConveniosMorososDeudoresMarcar: TBooleanField;
    cdsObtenerConveniosMorososDeudoresCodigoConvenio: TIntegerField;
    cdsObtenerConveniosMorososDeudoresNumeroDocumento: TStringField;
    cdsObtenerConveniosMorososDeudoresNombrePersona: TStringField;
    cdsObtenerConveniosMorososDeudoresNumeroConvenio: TStringField;
    cdsObtenerConveniosMorososDeudoresTipoCliente: TStringField;
    cdsObtenerConveniosMorososDeudoresDiasDeuda: TIntegerField;
    cdsObtenerConveniosMorososDeudoresCantComprobantes: TIntegerField;
    cdsObtenerConveniosMorososCobInternaCantComprobantes: TIntegerField;
    cdsObtenerConveniosMorososDeudoresCandidato: TStringField;
    cdsObtenerConveniosMorososDeudoresFechaVencimiento: TDateField;
    cdsObtenerConveniosMorososDeudoresSaldo: TFloatField;
    spActualizarEstadoConvenio: TADOStoredProc;
    spTiposDeAccionesDeCobranza_Listar: TADOStoredProc;
    cdsObtenerConveniosMorososDeudoresAccion: TStringField;
    cdsObtenerConveniosMorososDeudoresEstadoAccion: TStringField;
    spGuardarAccionConvenio: TADOStoredProc;
    btnSeleccionarMensajeCobExtraJudicial: TButton;
    btnSeleccionarPlantillaCobExtraJudicial: TButton;
    btnSeleccionarMensajeCobJudicial: TButton;
    btnSeleccionarPlantillaCobJudicial: TButton;
    cdsObtenerConveniosMorososCobInternaFechaVencimiento: TDateField;
    cdsObtenerConveniosMorososCobInternaAccionCobranza: TStringField;
    cdsObtenerConveniosMorososCobInternaEstadoAccionCobranza: TStringField;
    cdsObtenerConveniosMorososCobJudicialFechaVencimiento: TDateField;
    cdsObtenerConveniosMorososCobJudicialAccionCobranza: TStringField;
    cdsObtenerConveniosMorososCobJudicialEstadoAccionCobranza: TStringField;
    strngfldObtenerConveniosMorososDeudoresEmpresaCobranza: TStringField;
    dtmfldObtenerConveniosMorososDeudoresFechaEnvioCobranza: TDateTimeField;
    strngfldObtenerConveniosMorososDeudoresRefinanciacion: TStringField;
    strngfldObtenerConveniosMorososDeudoresEstadoRefinanciacion: TStringField;
    strngfldObtenerConveniosMorososDeudoresAvenimientoJudicial: TStringField;
    strngfldObtenerConveniosMorososDeudoresEstadoAvenimiento: TStringField;
    spIngresarConvenioACobranzaInterna: TADOStoredProc;
    spEliminarConvenioDeCobranzaInterna: TADOStoredProc;
    cdsObtenerConveniosMorososDeudoresCodigoCliente: TIntegerField;
    spObtenerHistorialDeCobranza: TADOStoredProc;
    procedure peNumeroDocumentoCobInternaButtonClick(Sender: TObject);
    procedure peNumeroDocumentoCobInternaChange(Sender: TObject);
    procedure btnSeleccionarTodoCobInternaClick(Sender: TObject);
    procedure btnLiberarSeleccionCobInternaClick(Sender: TObject);
    procedure peNumeroDocumentoCobExtraJudicialButtonClick(Sender: TObject);
    procedure peNumeroDocumentoCobExtraJudicialChange(Sender: TObject);
    procedure peNumeroDocumentoCobJudicialButtonClick(Sender: TObject);
    procedure peNumeroDocumentoCobJudicialChange(Sender: TObject);
    procedure btnSeleccionarTodoCobExtraJudicialClick(Sender: TObject);
    procedure btnLiberarSeleccionCobExtraJudicialClick(Sender: TObject);
    procedure btnSeleccionarTodoCobJudicialClick(Sender: TObject);
    procedure btnLiberarSeleccionCobJudicialClick(Sender: TObject);
    procedure btnBuscarCobInternaClick(Sender: TObject);
    procedure btnBuscarCobExtraJudicialClick(Sender: TObject);
    procedure btnBuscarCobJudicialClick(Sender: TObject);
    procedure btnEjecutarAccionCobInternaClick(Sender: TObject);
    procedure cbbTipoAccionCobranzaInternaChange(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure vcbAccionDeCobranzaInternaChange(Sender: TObject);
    procedure vcbAccionDeCobranzaExtraJudicialChange(Sender: TObject);
    procedure vcbAccionDeCobranzaJudicialChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnLimpiarCobExtraJudicialClick(Sender: TObject);
    procedure btnLimpiarCobInternaClick(Sender: TObject);
    procedure btnLimpiarCobJudicialClick(Sender: TObject);
    procedure btnExportarDatosACSVCobInternaClick(Sender: TObject);
    procedure btnSeleccionarPlantillaCobInternaClick(Sender: TObject);
    procedure dlMorososCobInternaDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure dlMorososCobInternaLinkClick(Sender: TCustomDBListEx;
      Column: TDBListExColumn);
    procedure btnHistorialCobInternaClick(Sender: TObject);
    procedure btnDefinirTipoClienteCobInternaClick(Sender: TObject);
    procedure btnSeleccionarMensajeCobInternaClick(Sender: TObject);
    procedure cbbTipoAccionCobranzaExtraJudicialChange(Sender: TObject);
    procedure cbbTipoAccionCobranzaJudicialChange(Sender: TObject);
    procedure btnHistorialCobExtraJudicialClick(Sender: TObject);
    procedure btnHistorialCobJudicialClick(Sender: TObject);
    procedure btnExportarDatosACSVCobExtraJudicialClick(Sender: TObject);
    procedure btnExportarDatosACSVCobJudicialClick(Sender: TObject);
    procedure btnEjecutarAccionCobExtraJudicialClick(Sender: TObject);
    procedure btnEjecutarAccionCobJudicialClick(Sender: TObject);
    procedure btnDefinirTipoClienteCobExtraJudicialClick(Sender: TObject);
    procedure btnDefinirTipoClienteCobJudicialClick(Sender: TObject);
    procedure dlMorososCobExtraJudicialDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure dlMorososCobJudicialDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure dlMorososCobExtraJudicialLinkClick(Sender: TCustomDBListEx;
      Column: TDBListExColumn);
    procedure dlMorososCobJudicialLinkClick(Sender: TCustomDBListEx;
      Column: TDBListExColumn);
    procedure btnBuscarDeudoresClick(Sender: TObject);
    procedure btnDefinirTipoClienteDeudoresClick(Sender: TObject);
    procedure dlDeudoresDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure dlDeudoresLinkClick(Sender: TCustomDBListEx;
      Column: TDBListExColumn);
    procedure btnEjecutarAccionDeudoresClick(Sender: TObject);
    procedure btnSeleccionarPlantillaDeudoresClick(Sender: TObject);
    procedure cbbTipoAccionDeudoresChange(Sender: TObject);
    procedure vcbAccionDeCobranzaDeudoresChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnHistorialDeudoresClick(Sender: TObject);
    procedure btnLimpiarCobDeudoresClick(Sender: TObject);
    procedure peNumeroDocumentoDeudoresButtonClick(Sender: TObject);
    procedure peNumeroDocumentoDeudoresChange(Sender: TObject);
    procedure btnSeleccionarTodoDeudoresClick(Sender: TObject);
    procedure btnLiberarSeleccionDeudoresClick(Sender: TObject);
    procedure dlDeudoresDblClick(Sender: TObject);
    procedure dlMorososCobInternaDblClick(Sender: TObject);
    procedure dlMorososCobExtraJudicialDblClick(Sender: TObject);
    procedure dlMorososCobJudicialDblClick(Sender: TObject);
    procedure btnHistoricoCobExtraJudicialClick(Sender: TObject);
    procedure btnExportarDatosACSVDeudoresClick(Sender: TObject);
  private
    { Private declarations }
	FUltimaBusqueda: TBusquedaCliente;
    CodigoPlantillaDeudores, CodigoPlantillaCobInterna, 
    CodigoPlantillaCobExtraJudicial, CodigoPlantillaCobJudicial,
    CodigoMensaje, CantDeudoresMarcados, CantCobInternaMarcados, 
    CantCobExtraJudicialMarcados, CantCobJudicialMarcados: Integer;
    FAccionesDeCobranzaBiz: TAccionesDeCobranzaBusiness;
    procedure MarcarDesmarcarClientesDeudores(Marcar: Boolean);
    procedure MarcarDesmarcarClientesCobInterna(Marcar: Boolean);
    procedure MarcarDesmarcarClientesCobExtraJudicial(Marcar: Boolean);
    procedure MarcarDesmarcarClientesCobJudicial(Marcar: Boolean);
    procedure pLimpiarCobDeudores();
    procedure pLimpiarCobInterna();
    procedure pLimpiarCobExtraJudicial();
    procedure pLimpiarCobJudicial();
    //procedure AssignarValorTipoDeAccionDeCobranza;
    function TraeRegistrosDeudores: Boolean;
    function TraeRegistrosCobInterna: Boolean;
    function TraeRegistrosCobExtraJudicial: Boolean;
    function TraeRegistrosCobJudicial: Boolean;
    function CargarTiposCliente(): Boolean;
    function CargaCategoriasDeuda(): Boolean;
	function CargaAccionesDeCobranza(): Boolean;
    function EsConvenioCandidatoAInhabilitar(CodigoConvenio: Integer): Boolean;
    function fExisteRefinanciacion(CodigoCliente, CodigoConvenio: Integer; 
    	var CodigoRefinanciacion, CodigoRefinanciacionUnificada: Integer):Boolean;
  public
    { Public declarations }
	function Inicializar(): Boolean;
  end;

var
  FormCarpetaDeudores: TFormCarpetaDeudores;

const
	CONST_SEMAFORO_COBRANZA								    = 2;

    CONST_ESTADO_CONVENIO_COBRANZA_INTERNA					= 1;
    CONST_ESTADO_CONVENIO_COBRANZA_EXTRAJUDICIAL			= 2;
    CONST_ESTADO_CONVENIO_COBRANZA_JUDICIAL					= 3;
    CONST_ESTADO_CONVENIO_REFINANCIADO						= 4;
    CONST_ESTADO_CONVENIO_CASTIGADO							= 5;
    
	COD_TIPO_CAMPANNA_INTERNA	                            = 1;
    COD_MOTIVO_CAMPANNA_MOROSOS	                            = 2;
	FORMA_PAGO_CASTIGO			                            = 'CA';
	COD_NOTIFICACION_COBRANZA_EMAIL_POSTAL				    = 1;
	COD_NOTIFICACION_COBRANZA_AVISO_NOTA_COBRO			    = 2;
	COD_NOTIFICACION_COBRANZA_AVISO_CARTA_CERTIFICADA	    = 3;

	TIPO_COBRANZA_INTERNA		                            = 1;
    TIPO_COBRANZA_EXTRAJUDICIAL	                            = 2;
    TIPO_COBRANZA_JUDICIAL		                            = 3;

    ACCION_COBRANZA_AVISO_EMAIL								= 1;
    ACCION_COBRANZA_AVISO_POSTAL					        = 2;
    ACCION_COBRANZA_AVISO_TELEFONICO				        = 3;
    ACCION_COBRANZA_AVISO_NOTACOBRO					        = 4;
    ACCION_COBRANZA_AVISO_CARTA_CERTIFICADA			        = 5;
    ACCION_COBRANZA_ENVIO_COBRANZA_EXTRAJUDICIAL	        = 6;
    ACCION_COBRANZA_ENVIO_COBRANZA_JUDICIAL			        = 7;
    ACCION_COBRANZA_INHABILITACION_TAGS				        = 8;
    ACCION_COBRANZA_CASTIGO							        = 9;
    ACCION_COBRANZA_TRASPASO_COB_INTERNA			        = 10;
    ACCION_COBRANZA_REFINANCIACION					        = 11;
    ACCION_TRANSFERENCIA_ENTRE_EMPRESAS						= 12;
    
resourcestring
	MSG_NO_HAY_REGISTROS 			= 'No hay registros para ejecutar la acci�n seleccionada';
	MSG_DEBE_SELECCIONAR_ACCION		= 'Debe seleccionar una acci�n a ejecutar';
    MSG_DEBE_SELECCIONAR_PLANTILLA	= 'Debe seleccionar una plantilla';
    MSG_EXISTE_CONVENIO_NO_CANDIDATO= 'Existe a lo menos un convenio no candidato a Inhabilitaci�n.' + #13 + 'S�lo se utilizar�n los convenios candidatos';
    MSG_NO_EXISTE_CONVENIO_CANDIDATO= 'No existen convenios candidatos a Inhabilitaci�n';
    MSG_NO_HAY_SELECCION			= 'No ha seleccionado registro para Ejecutar Acci�n';
    MSG_ACCION_UNITARIA				= 'Para %s, s�lo debe seleccionar un convenio';
	MSG_ERROR_OBTENIENDO_DATOS		= 'Se produjo un error al obtener los datos de ParametrosGenerales';

implementation

uses
	frmInhabilitacionTAGs, ExportarToExcel, FrmSeleccionarPlantillaEMail,
    CobranzasResources, RStrings, frmRefinanciacionGestion, frmMuestraMensaje,
	frmCobroComprobantesCastigo, FrmSeleccionarTipoDeCliente, DB_CRUDCommonProcs,
    frmHistorialCobranzas, Main, frmHistoricoCobranzaExterna, frmGestionarCobranzaExterna;

{$R *.dfm}

procedure TFormCarpetaDeudores.FormCreate(Sender: TObject);
begin
    inherited;
    FAccionesDeCobranzaBiz := TAccionesDeCobranzaBusiness.Create(Self.Caption);
end;

function TFormCarpetaDeudores.Inicializar(): Boolean;
var
	S: TSize;
begin
    Result 							:= False;
	FormStyle						:= fsMDIChild;
    CodigoPlantillaDeudores			:= -1;
    CodigoPlantillaCobInterna		:= -1;
    CodigoPlantillaCobExtraJudicial	:= -1;
    CodigoPlantillaCobJudicial		:= -1;
    CodigoMensaje	            	:= -1;
    CantDeudoresMarcados			:= 0;
    CantCobInternaMarcados			:= 0;
    CantCobExtraJudicialMarcados	:= 0;
    CantCobJudicialMarcados			:= 0;
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
    FAccionesDeCobranzaBiz.ObtenerTodosLosTiposDeAccionesDeCobranza;
	CargarConstantesCobranzasRefinanciacion;
    pcCarpetaDeudores.ActivePageIndex	:= 0;
    TPanelMensajesForm.MuestraMensaje('Obteniendo Categor�as Deuda ...', True);
    if not CargaCategoriasDeuda() then
    	Exit;
    TPanelMensajesForm.MuestraMensaje('Obteniendo Acciones de Cobranza ...', True);
    if not CargaAccionesDeCobranza() then
    	Exit;
    TPanelMensajesForm.MuestraMensaje('Obteniendo Tipos de Cliente ...', True);
    if not CargarTiposCliente() then
    	Exit;
    TPanelMensajesForm.OcultaPanel;
    Result	:= True;
end;

{procedure TFormCarpetaDeudores.AssignarValorTipoDeAccionDeCobranza;
begin
    FAccionesDeCobranzaBiz.LlenarTiposDeAccionesDeCobranzaParaTipoDeCobranza(vcbAccionDeCobranzaInterna.Value,
        cbbTipoAccionCobranzaInterna, True);
    cbbTipoAccionCobranzaInterna.Value := -1;
    //esto es necesario porque puede que cambie el Tipo de cobranza y no exista la accion para ese tipo
    if cbbTipoAccionCobranzaInterna.Value = Unassigned then
        cbbTipoAccionCobranzaInterna.Value := -1;
end;
}

function TFormCarpetaDeudores.CargaCategoriasDeuda(): Boolean;
begin
    Result := False;
    try
		vcbCategoriaDeudaDeudores.Items.Clear;
		vcbCategoriaDeudaCobInterna.Items.Clear;
		vcbCategoriaDeudaCobExtraJudicial.Items.Clear;
		vcbCategoriaDeudaCobJudicial.Items.Clear;
		vcbCategoriaDeudaDeudores.Items.Add('Ambas', 0);
		vcbCategoriaDeudaCobInterna.Items.Add('Ambas', 0);
		vcbCategoriaDeudaCobExtraJudicial.Items.Add('Ambas', 0);
		vcbCategoriaDeudaCobJudicial.Items.Add('Ambas', 0);

        with spCategoriasDeDeudasDeCobranza_Listar do begin
            if Active then
            	Close;
            Open;
            while not Eof do begin
            	vcbCategoriaDeudaDeudores.Items.Add(FieldByName('Descripcion').AsString, FieldByName('CodigoCategoriaDeDeudaDeCobranza').AsInteger);
            	vcbCategoriaDeudaCobInterna.Items.Add(FieldByName('Descripcion').AsString, FieldByName('CodigoCategoriaDeDeudaDeCobranza').AsInteger);
            	vcbCategoriaDeudaCobExtraJudicial.Items.Add(FieldByName('Descripcion').AsString, FieldByName('CodigoCategoriaDeDeudaDeCobranza').AsInteger);
            	vcbCategoriaDeudaCobJudicial.Items.Add(FieldByName('Descripcion').AsString, FieldByName('CodigoCategoriaDeDeudaDeCobranza').AsInteger);
            	Next;
            end;
            Close;
        end;

		vcbCategoriaDeudaDeudores.ItemIndex			:= 0;
		vcbCategoriaDeudaCobInterna.ItemIndex		:= 0;
		vcbCategoriaDeudaCobExtraJudicial.ItemIndex	:= 0;
		vcbCategoriaDeudaCobJudicial.ItemIndex		:= 0;
    except
    	on E:Exception do begin
        	MsgBox(E.Message, Self.Caption, MB_ICONSTOP);
			Exit;
        end;	
    end;
    Result := True;
end;

function TFormCarpetaDeudores.CargaAccionesDeCobranza(): Boolean;
var
	i: Integer;
begin
    Result := True;
    try
    	vcbAccionDeCobranzaDeudores.Items.Clear;
    	vcbAccionDeCobranzaInterna.Items.Clear;
        vcbAccionDeCobranzaExtraJudicial.Items.Clear;
        vcbAccionDeCobranzaJudicial.Items.Clear;
        vcbAccionDeCobranzaDeudores.Items.Add('Ninguna', -1);
    	vcbAccionDeCobranzaInterna.Items.Add('Ninguna', -1);
        vcbAccionDeCobranzaExtraJudicial.Items.Add('Ninguna', -1);
        vcbAccionDeCobranzaJudicial.Items.Add('Ninguna', -1);

        with spObtenerAccionesDeCobranza, Parameters do begin
            if Active then
                Close;
            Parameters.Refresh;
            ParamByName('@CodigoTipoDeCobranza').Value	:= NULL;
            Open;
        	while not Eof do begin
            	vcbAccionDeCobranzaDeudores.Items.Add(FieldByName('Nombre').AsString, FieldByName('CodigoAccionDeCobranza').AsString);
            	Next;
            end;
            Close;
        end;
            
        with spObtenerAccionesDeCobranza, Parameters do begin
            if Active then
                Close;
            Parameters.Refresh;
            ParamByName('@CodigoTipoDeCobranza').Value	:= TIPO_COBRANZA_INTERNA;
            Open;
        	while not Eof do begin
            	vcbAccionDeCobranzaInterna.Items.Add(FieldByName('Nombre').AsString, FieldByName('CodigoAccionDeCobranza').AsString);
            	Next;
            end;
            Close;
        end;
            
        with spObtenerAccionesDeCobranza, Parameters do begin
            if Active then
                Close;
            Parameters.Refresh;
            ParamByName('@CodigoTipoDeCobranza').Value	:= TIPO_COBRANZA_EXTRAJUDICIAL;
            Open;
        	while not Eof do begin
            	vcbAccionDeCobranzaExtraJudicial.Items.Add(FieldByName('Nombre').AsString, FieldByName('CodigoAccionDeCobranza').AsString);
            	Next;
            end;
            Close;
        end;
            
        with spObtenerAccionesDeCobranza, Parameters do begin
            if Active then
                Close;
            Parameters.Refresh;
            ParamByName('@CodigoTipoDeCobranza').Value	:= TIPO_COBRANZA_JUDICIAL;
            Open;
        	while not Eof do begin
            	vcbAccionDeCobranzaJudicial.Items.Add(FieldByName('Nombre').AsString, FieldByName('CodigoAccionDeCobranza').AsString);
            	Next;
            end;
            Close;
        end;

        DB_CRUDCommonProcs.FillVariantComboBox(cbbTipoAccionDeudores, spTiposDeAccionesDeCobranza_Listar,
                                           'CodigoTipoDeAccionDeCobranza', 'Descripcion', Self.Caption, True);
        cbbTipoAccionDeudores.Value := -1;

        DB_CRUDCommonProcs.FillVariantComboBox(cbbTipoAccionCobranzaInterna, spTiposDeAccionesDeCobranza_Listar,
                                           'CodigoTipoDeAccionDeCobranza', 'Descripcion', Self.Caption, True);
        cbbTipoAccionCobranzaInterna.Value := -1;

        FAccionesDeCobranzaBiz.LlenarTiposDeAccionesDeCobranzaParaTipoDeCobranza(2, cbbTipoAccionCobranzaExtraJudicial, True);
        cbbTipoAccionCobranzaExtraJudicial.Value := -1;
        if cbbTipoAccionCobranzaExtraJudicial.Value = Unassigned then
            cbbTipoAccionCobranzaExtraJudicial.Value := -1;

        for i := 0 to cbbTipoAccionCobranzaInterna.Items.Count - 1 do begin
            if cbbTipoAccionCobranzaInterna.Items[i].Value = ACCION_COBRANZA_TRASPASO_COB_INTERNA then begin
            	cbbTipoAccionCobranzaInterna.Items.Delete(i);
                Break;
            end;
        end;

    	for i := 0 to cbbTipoAccionCobranzaExtraJudicial.Items.Count-1 do begin
        	if cbbTipoAccionCobranzaExtraJudicial.Items[i].Value = ACCION_COBRANZA_ENVIO_COBRANZA_EXTRAJUDICIAL then begin
            	cbbTipoAccionCobranzaExtraJudicial.Items.Delete(i);
                Break;
            end;
        end;

        FAccionesDeCobranzaBiz.LlenarTiposDeAccionesDeCobranzaParaTipoDeCobranza(2, cbbTipoAccionCobranzaJudicial, True);
        cbbTipoAccionCobranzaJudicial.Value := -1;
        if cbbTipoAccionCobranzaJudicial.Value = Unassigned then
            cbbTipoAccionCobranzaJudicial.Value := -1;
    	for i := cbbTipoAccionCobranzaJudicial.Items.Count-1 downto 0 do begin
        	if (cbbTipoAccionCobranzaJudicial.Items[i].Value = ACCION_COBRANZA_ENVIO_COBRANZA_JUDICIAL)
            or (cbbTipoAccionCobranzaJudicial.Items[i].Value = ACCION_COBRANZA_ENVIO_COBRANZA_EXTRAJUDICIAL) then
            	cbbTipoAccionCobranzaJudicial.Items.Delete(i);
        end;

        vcbAccionDeCobranzaDeudores.ItemIndex		:= 0;
        vcbAccionDeCobranzaInterna.ItemIndex		:= 0;
        vcbAccionDeCobranzaExtraJudicial.ItemIndex	:= 0;
        vcbAccionDeCobranzaJudicial.ItemIndex		:= 0;
    except
    	on E:Exception do begin
        	MsgBox('  Error al obtener las acciones de cobranza.' +#13+ E.Message, Self.Caption, MB_ICONSTOP);
			Exit;
        end;
    end;
    Result	:= True;
end;

function TFormCarpetaDeudores.CargarTiposCliente(): Boolean;
begin
    Result	:= False;
    try
    	vcbTipoClienteDeudores.Items.Clear;
    	vcbTipoClienteCobInterna.Items.Clear;
        cbTipoClienteCobExtraJudicial.Items.Clear;
        cbTipoClienteCobJudicial.Items.Clear;
        vcbTipoClienteDeudores.Items.Add('Todos los Tipos', -1);
    	vcbTipoClienteCobInterna.Items.Add('Todos los Tipos', -1);
        cbTipoClienteCobExtraJudicial.Items.Add('Todos los Tipos', -1);
        cbTipoClienteCobJudicial.Items.Add('Todos los Tipos', -1);
        with spObtenerTiposDeCliente, Parameters do begin
            if Active then
                Close;
            Parameters.Refresh;
            ParamByName('@Semaforo').Value	:= CONST_SEMAFORO_COBRANZA;
            Open;
        	while not Eof do begin
            	vcbTipoClienteDeudores.Items.Add(FieldByName('Descripcion').AsString, FieldByName('CodigoTipoCliente').AsString);
            	vcbTipoClienteCobInterna.Items.Add(FieldByName('Descripcion').AsString, FieldByName('CodigoTipoCliente').AsString);
                cbTipoClienteCobExtraJudicial.Items.Add(FieldByName('Descripcion').AsString, FieldByName('CodigoTipoCliente').AsString);
                cbTipoClienteCobJudicial.Items.Add(FieldByName('Descripcion').AsString, FieldByName('CodigoTipoCliente').AsString);
            	Next;
            end;
            Close;
        end;
        vcbTipoClienteDeudores.ItemIndex		:= 0;
        vcbTipoClienteCobInterna.ItemIndex		:= 0;
        cbTipoClienteCobExtraJudicial.ItemIndex	:= 0;
        cbTipoClienteCobJudicial.ItemIndex		:= 0;
    except
    	on E:Exception do begin
        	MsgBox(E.Message, Self.Caption, MB_ICONSTOP);
			Exit;
        end;
    end;
    Result	:= True;
end;

procedure TFormCarpetaDeudores.cbbTipoAccionDeudoresChange(Sender: TObject);
var
	FCodigoTipoAccionCobranza: Integer;
	BPidePlantilla, BPideMensaje: Boolean;
begin
	try
    	FCodigoTipoAccionCobranza	:= cbbTipoAccionDeudores.Value;
        BPidePlantilla	:= (QueryGetValue(DMConnections.BaseCAC, 'SELECT PidePlantilla FROM TiposDeAccionesDeCobranza NOLOCK WHERE CodigoTipoDeAccionDeCobranza = ' + IntToStr(FCodigoTipoAccionCobranza))) = 'True';
        BPideMensaje	:= (QueryGetValue(DMConnections.BaseCAC, 'SELECT PideMensaje FROM TiposDeAccionesDeCobranza NOLOCK WHERE CodigoTipoDeAccionDeCobranza = ' + IntToStr(FCodigoTipoAccionCobranza))) = 'True';
    except
    	on e:Exception do begin
            MessageDlg(e.Message, mtError, [mbOK], 0);
            BPidePlantilla	:= False;
            BPideMensaje	:= False;
        end;
    end;

    if BPidePlantilla then begin
    	btnSeleccionarPlantillaDeudores.Visible	:= True;
    	btnSeleccionarMensajeDeudores.Visible	:= False;
    end
    else if BPideMensaje then begin
    	btnSeleccionarPlantillaDeudores.Visible	:= False;
    	btnSeleccionarMensajeDeudores.Visible	:= True;
    end
    else begin
    	btnSeleccionarPlantillaDeudores.Visible	:= False;
    	btnSeleccionarMensajeDeudores.Visible	:= False;
    end;
    lblAccionDeudores.Caption	:= cbbTipoAccionDeudores.Text;
end;

procedure TFormCarpetaDeudores.cbbTipoAccionCobranzaInternaChange(Sender: TObject);
var
	FCodigoTipoAccionCobranza: Integer;
	BPidePlantilla, BPideMensaje: Boolean;
begin
	try
    	FCodigoTipoAccionCobranza	:= cbbTipoAccionCobranzaInterna.Value;
        BPidePlantilla	:= (QueryGetValue(DMConnections.BaseCAC, 'SELECT PidePlantilla FROM TiposDeAccionesDeCobranza NOLOCK WHERE CodigoTipoDeAccionDeCobranza = ' + IntToStr(FCodigoTipoAccionCobranza))) = 'True';
        BPideMensaje	:= (QueryGetValue(DMConnections.BaseCAC, 'SELECT PideMensaje FROM TiposDeAccionesDeCobranza NOLOCK WHERE CodigoTipoDeAccionDeCobranza = ' + IntToStr(FCodigoTipoAccionCobranza))) = 'True';
    except
    	on e:Exception do begin
            MessageDlg(e.Message, mtError, [mbOK], 0);
            BPidePlantilla	:= False;
            BPideMensaje	:= False;
        end;
    end;

    if BPidePlantilla then begin
    	btnSeleccionarPlantillaCobInterna.Visible	:= True;
    	btnSeleccionarMensajeCobInterna.Visible		:= False;
    end
    else if BPideMensaje then begin
    	btnSeleccionarPlantillaCobInterna.Visible	:= False;
    	btnSeleccionarMensajeCobInterna.Visible		:= True;
    end
    else begin
    	btnSeleccionarPlantillaCobInterna.Visible	:= False;
    	btnSeleccionarMensajeCobInterna.Visible		:= False;
    end;
    lblAccionCobranzaInterna.Caption	:= cbbTipoAccionCobranzaInterna.Text;
end;

procedure TFormCarpetaDeudores.cbbTipoAccionCobranzaExtraJudicialChange(
  Sender: TObject);
var
	FCodigoTipoAccionCobranza: Integer;
	BPidePlantilla, BPideMensaje: Boolean;
begin
	try
    	FCodigoTipoAccionCobranza	:= cbbTipoAccionCobranzaExtraJudicial.Value;
        BPidePlantilla	:= (QueryGetValue(DMConnections.BaseCAC, 'SELECT PidePlantilla FROM TiposDeAccionesDeCobranza NOLOCK WHERE CodigoTipoDeAccionDeCobranza = ' + IntToStr(FCodigoTipoAccionCobranza))) = 'True';
        BPideMensaje	:= (QueryGetValue(DMConnections.BaseCAC, 'SELECT PideMensaje FROM TiposDeAccionesDeCobranza NOLOCK WHERE CodigoTipoDeAccionDeCobranza = ' + IntToStr(FCodigoTipoAccionCobranza))) = 'True';
    except
    	on e:Exception do begin
            MessageDlg(e.Message, mtError, [mbOK], 0);
            BPidePlantilla	:= False;
            BPideMensaje	:= False;
        end;
    end;

    if BPidePlantilla then begin
        btnSeleccionarPlantillaCobExtraJudicial.Visible	:= True;
    	btnSeleccionarMensajeCobExtraJudicial.Visible	:= False;
    end
    else if BPideMensaje then begin
    	btnSeleccionarPlantillaCobExtraJudicial.Visible	:= False;
    	btnSeleccionarMensajeCobExtraJudicial.Visible	:= True;
    end
    else begin
    	btnSeleccionarPlantillaCobExtraJudicial.Visible	:= False;
    	btnSeleccionarMensajeCobExtraJudicial.Visible	:= False;
    end;
    lblAccionCobranzaExtraJudicial.Caption	:= cbbTipoAccionCobranzaExtraJudicial.Text;
end;

procedure TFormCarpetaDeudores.cbbTipoAccionCobranzaJudicialChange(Sender: TObject);
var
	FCodigoTipoAccionCobranza: Integer;
	BPidePlantilla, BPideMensaje: Boolean;
begin
	try
    	FCodigoTipoAccionCobranza	:= cbbTipoAccionCobranzaJudicial.Value;
        BPidePlantilla	:= (QueryGetValue(DMConnections.BaseCAC, 'SELECT PidePlantilla FROM TiposDeAccionesDeCobranza NOLOCK WHERE CodigoTipoDeAccionDeCobranza = ' + IntToStr(FCodigoTipoAccionCobranza))) = 'True';
        BPideMensaje	:= (QueryGetValue(DMConnections.BaseCAC, 'SELECT PideMensaje FROM TiposDeAccionesDeCobranza NOLOCK WHERE CodigoTipoDeAccionDeCobranza = ' + IntToStr(FCodigoTipoAccionCobranza))) = 'True';
    except
    	on e:Exception do begin
            MessageDlg(e.Message, mtError, [mbOK], 0);
            BPidePlantilla	:= False;
            BPideMensaje	:= False;
        end;
    end;

    if BPidePlantilla then begin
        btnSeleccionarPlantillaCobJudicial.Visible	:= True;
    	btnSeleccionarMensajeCobJudicial.Visible	:= False;
    end
    else if BPideMensaje then begin
    	btnSeleccionarPlantillaCobJudicial.Visible	:= False;
    	btnSeleccionarMensajeCobJudicial.Visible	:= True;
    end
    else begin
    	btnSeleccionarPlantillaCobJudicial.Visible	:= False;
    	btnSeleccionarMensajeCobJudicial.Visible	:= False;
    end;
    lblAccionCobranzaJudicial.Caption	:= cbbTipoAccionCobranzaJudicial.Text;
end;

procedure TFormCarpetaDeudores.dlDeudoresDblClick(Sender: TObject);
begin
    cdsObtenerConveniosMorososDeudores.Edit;
    cdsObtenerConveniosMorososDeudoresMarcar.AsBoolean := not cdsObtenerConveniosMorososDeudoresMarcar.AsBoolean;
    if cdsObtenerConveniosMorososDeudoresMarcar.AsBoolean then
    	CantDeudoresMarcados := CantDeudoresMarcados+1
    else
         CantDeudoresMarcados := CantDeudoresMarcados-1;
    cdsObtenerConveniosMorososDeudores.Post;
end;

procedure TFormCarpetaDeudores.dlDeudoresDrawText(Sender: TCustomDBListEx;
  Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
  var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
  var DefaultDraw: Boolean);
var
    bmp: TBitMap;
begin
    with Sender do begin
        if (Column.FieldName = 'Marcar') then 
        	try
                Text := '';
                Canvas.FillRect(Rect);
                bmp := TBitMap.Create;
                DefaultDraw := False;

                if cdsObtenerConveniosMorososDeudoresMarcar.AsBoolean then
                    lnCheck.GetBitmap(1, Bmp)
                else
                    lnCheck.GetBitmap(0, Bmp);

                Canvas.Draw((Rect.Left + Rect.Right - Bmp.Width) div 2, Rect.Top, bmp);
            finally
                bmp.Free;
            end;
    end;
end;

procedure TFormCarpetaDeudores.dlMorososCobInternaDblClick(Sender: TObject);
begin
    cdsObtenerConveniosMorososCobInterna.Edit;
    cdsObtenerConveniosMorososCobInternaMarcar.AsBoolean := not cdsObtenerConveniosMorososCobInternaMarcar.AsBoolean;
    if cdsObtenerConveniosMorososCobInternaMarcar.AsBoolean then
    	CantCobInternaMarcados	:= CantCobInternaMarcados+1
    else
    	CantCobInternaMarcados	:= CantCobInternaMarcados-1;
    cdsObtenerConveniosMorososCobInterna.Post;
end;

procedure TFormCarpetaDeudores.dlMorososCobInternaDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
var
    bmp: TBitMap;
begin
    with Sender do begin
        if (Column.FieldName = 'Marcar') then try
            Text := '';
            Canvas.FillRect(Rect);
            bmp := TBitMap.Create;
			DefaultDraw := False;

            if cdsObtenerConveniosMorososCobInternaMarcar.AsBoolean then
	            lnCheck.GetBitmap(1, Bmp)
            else
            	lnCheck.GetBitmap(0, Bmp);

            Canvas.Draw((Rect.Left + Rect.Right - Bmp.Width) div 2, Rect.Top, bmp);
        finally
            bmp.Free;
        end;
    end;
end;

procedure TFormCarpetaDeudores.dlMorososCobExtraJudicialDblClick(
  Sender: TObject);
begin
    cdsObtenerConveniosMorososCobExtraJudicial.Edit;
    cdsObtenerConveniosMorososCobExtraJudicialMarcar.AsBoolean := not cdsObtenerConveniosMorososCobExtraJudicialMarcar.AsBoolean;
    if cdsObtenerConveniosMorososCobExtraJudicialMarcar.AsBoolean then
    	CantCobExtraJudicialMarcados	:= CantCobExtraJudicialMarcados+1
    else
    	CantCobExtraJudicialMarcados	:= CantCobExtraJudicialMarcados-1;
    cdsObtenerConveniosMorososCobExtraJudicial.Post;
end;

procedure TFormCarpetaDeudores.dlMorososCobExtraJudicialDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
var
    bmp: TBitMap;
begin
    with Sender do begin
        if (Column.FieldName = 'Marcar') then try
            Text := '';
            Canvas.FillRect(Rect);
            bmp := TBitMap.Create;
			DefaultDraw := False;

            if cdsObtenerConveniosMorososCobExtraJudicialMarcar.AsBoolean then
	            lnCheck.GetBitmap(1, Bmp)
            else
            	lnCheck.GetBitmap(0, Bmp);

            Canvas.Draw((Rect.Left + Rect.Right - Bmp.Width) div 2, Rect.Top, bmp);
        finally
            bmp.Free;
        end;
    end;
end;

procedure TFormCarpetaDeudores.dlMorososCobJudicialDblClick(Sender: TObject);
begin
    cdsObtenerConveniosMorososCobJudicial.Edit;
    cdsObtenerConveniosMorososCobJudicialMarcar.AsBoolean := not cdsObtenerConveniosMorososCobJudicialMarcar.AsBoolean;
    if cdsObtenerConveniosMorososCobJudicialMarcar.AsBoolean then
    	CantCobJudicialMarcados	:= CantCobJudicialMarcados+1
    else
    	CantCobJudicialMarcados	:= CantCobJudicialMarcados-1;
    cdsObtenerConveniosMorososCobJudicial.Post;
end;

procedure TFormCarpetaDeudores.dlMorososCobJudicialDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
var
    bmp: TBitMap;
begin
    with Sender do begin
        if (Column.FieldName = 'Marcar') then try
            Text := '';
            Canvas.FillRect(Rect);
            bmp := TBitMap.Create;
			DefaultDraw := False;

            if cdsObtenerConveniosMorososCobJudicialMarcar.AsBoolean then
	            lnCheck.GetBitmap(1, Bmp)
            else
            	lnCheck.GetBitmap(0, Bmp);

            Canvas.Draw((Rect.Left + Rect.Right - Bmp.Width) div 2, Rect.Top, bmp);
        finally
            bmp.Free;
        end;
    end;
end;

procedure TFormCarpetaDeudores.dlDeudoresLinkClick(Sender: TCustomDBListEx;
  Column: TDBListExColumn);
begin
    cdsObtenerConveniosMorososDeudores.Edit;
    cdsObtenerConveniosMorososDeudoresMarcar.AsBoolean := not cdsObtenerConveniosMorososDeudoresMarcar.AsBoolean;
    if cdsObtenerConveniosMorososDeudoresMarcar.AsBoolean then
    	CantDeudoresMarcados := CantDeudoresMarcados+1
    else
         CantDeudoresMarcados := CantDeudoresMarcados-1;
    cdsObtenerConveniosMorososDeudores.Post;
end;

procedure TFormCarpetaDeudores.dlMorososCobInternaLinkClick(
  Sender: TCustomDBListEx; Column: TDBListExColumn);
begin
    cdsObtenerConveniosMorososCobInterna.Edit;
    cdsObtenerConveniosMorososCobInternaMarcar.AsBoolean := not cdsObtenerConveniosMorososCobInternaMarcar.AsBoolean;
    if cdsObtenerConveniosMorososCobInternaMarcar.AsBoolean then
    	CantCobInternaMarcados	:= CantCobInternaMarcados+1
    else
    	CantCobInternaMarcados	:= CantCobInternaMarcados-1;
    cdsObtenerConveniosMorososCobInterna.Post;
end;

procedure TFormCarpetaDeudores.dlMorososCobExtraJudicialLinkClick(
  Sender: TCustomDBListEx; Column: TDBListExColumn);
begin
    cdsObtenerConveniosMorososCobExtraJudicial.Edit;
    cdsObtenerConveniosMorososCobExtraJudicialMarcar.AsBoolean := not cdsObtenerConveniosMorososCobExtraJudicialMarcar.AsBoolean;
    if cdsObtenerConveniosMorososCobExtraJudicialMarcar.AsBoolean then
    	CantCobExtraJudicialMarcados	:= CantCobExtraJudicialMarcados+1
    else
    	CantCobExtraJudicialMarcados	:= CantCobExtraJudicialMarcados-1;
    cdsObtenerConveniosMorososCobExtraJudicial.Post;
end;

procedure TFormCarpetaDeudores.dlMorososCobJudicialLinkClick(
  Sender: TCustomDBListEx; Column: TDBListExColumn);
begin
    cdsObtenerConveniosMorososCobJudicial.Edit;
    cdsObtenerConveniosMorososCobJudicialMarcar.AsBoolean := not cdsObtenerConveniosMorososCobJudicialMarcar.AsBoolean;
    if cdsObtenerConveniosMorososCobJudicialMarcar.AsBoolean then
    	CantCobJudicialMarcados	:= CantCobJudicialMarcados+1
    else
    	CantCobJudicialMarcados	:= CantCobJudicialMarcados-1;
    cdsObtenerConveniosMorososCobJudicial.Post;
end;

procedure TFormCarpetaDeudores.btnSeleccionarPlantillaDeudoresClick(
  Sender: TObject);
var
	f: TFormSeleccionarPlantillaEMail;
    Seleccion: string;
begin
	try
        Application.CreateForm(TFormSeleccionarPlantillaEMail,f);
        if f.Inicializar() and (f.ShowModal = mrOK) then begin
            CodigoPlantillaDeudores			:= StrToInt(f.ListaCodigosPlantillas[f.rgPlantillas.ItemIndex]);
            Seleccion						:= f.rgPlantillas.Items[f.rgPlantillas.ItemIndex];
            lblAccionDeudores.Caption		:= cbbTipoAccionDeudores.Text + ' ' + Seleccion;
        end;
    finally
        f.Release;
    end;
end;

procedure TFormCarpetaDeudores.btnSeleccionarMensajeCobInternaClick(
  Sender: TObject);
var
	f: TFormSeleccionarPlantillaEMail;
    Seleccion: string;
begin
{	try
        Application.CreateForm(TFormSeleccionarPlantillaEMail,f);
        if f.Inicializar() and (f.ShowModal = mrOK) then begin
            CodigoMensaje						:= StrToInt(f.ListaCodigosPlantillas[f.rgPlantillas.ItemIndex]);
            Seleccion							:= f.rgPlantillas.Items[f.rgPlantillas.ItemIndex];
            lblAccionCobranzaInterna.Caption	:= cbbTipoAccionCobranzaInterna.Text + ' ' + Seleccion;
        end;
    finally
        f.Release;
    end;
}
end;

procedure TFormCarpetaDeudores.btnSeleccionarPlantillaCobInternaClick(
  Sender: TObject);
var
	f: TFormSeleccionarPlantillaEMail;
    Seleccion: string;
begin
	try
        Application.CreateForm(TFormSeleccionarPlantillaEMail,f);
        if f.Inicializar() and (f.ShowModal = mrOK) then begin
            CodigoPlantillaCobInterna			:= StrToInt(f.ListaCodigosPlantillas[f.rgPlantillas.ItemIndex]);
            Seleccion							:= f.rgPlantillas.Items[f.rgPlantillas.ItemIndex];
            lblAccionCobranzaInterna.Caption	:= cbbTipoAccionCobranzaInterna.Text + ' ' + Seleccion;
        end;
    finally
        f.Release;
    end;
end;

procedure TFormCarpetaDeudores.btnSeleccionarTodoDeudoresClick(Sender: TObject);
begin
	MarcarDesmarcarClientesDeudores(True);
	CantDeudoresMarcados	:= cdsObtenerConveniosMorososDeudores.RecordCount;
end;

procedure TFormCarpetaDeudores.btnLiberarSeleccionDeudoresClick(
  Sender: TObject);
begin
	MarcarDesmarcarClientesDeudores(False);
	CantDeudoresMarcados	:= 0;
end;

procedure TFormCarpetaDeudores.MarcarDesmarcarClientesDeudores(Marcar: Boolean);
var
    Puntero: TBookmark;
begin
    try
        CambiarEstadoCursor(CURSOR_RELOJ, Self);
        Puntero := cdsObtenerConveniosMorososDeudores.GetBookmark;
        dsObtenerConveniosMorososDeudores.DataSet := Nil;

        with cdsObtenerConveniosMorososDeudores do begin
        	First;
        	while not Eof do begin
                Edit;
                FieldByName('Marcar').AsBoolean := Marcar;
            	Next;
            end;
        end;

    finally
        if cdsObtenerConveniosMorososDeudores.BookmarkValid(Puntero) then begin
            cdsObtenerConveniosMorososDeudores.GotoBookmark(Puntero);
        end;
        cdsObtenerConveniosMorososDeudores.FreeBookmark(Puntero);
        dsObtenerConveniosMorososDeudores.DataSet := cdsObtenerConveniosMorososDeudores;

        CambiarEstadoCursor(CURSOR_DEFECTO, Self);
    end;
end;

procedure TFormCarpetaDeudores.btnSeleccionarTodoCobInternaClick(Sender: TObject);
begin
	MarcarDesmarcarClientesCobInterna(True);
    CantCobInternaMarcados	:= cdsObtenerConveniosMorososCobInterna.RecordCount;
end;

procedure TFormCarpetaDeudores.btnLiberarSeleccionCobInternaClick(Sender: TObject);
begin
	MarcarDesmarcarClientesCobInterna(False);
    CantCobInternaMarcados	:= 0;
end;

procedure TFormCarpetaDeudores.MarcarDesmarcarClientesCobInterna(Marcar: Boolean);
var
    Puntero: TBookmark;
begin
    try
        CambiarEstadoCursor(CURSOR_RELOJ, Self);
        Puntero := cdsObtenerConveniosMorososCobInterna.GetBookmark;
        dsObtenerConveniosMorososCobInterna.DataSet := Nil;

        with cdsObtenerConveniosMorososCobInterna do begin
        	First;
        	while not Eof do begin
                Edit;
                FieldByName('Marcar').AsBoolean := Marcar;
            	Next;
            end;
        end;

    finally
        if cdsObtenerConveniosMorososCobInterna.BookmarkValid(Puntero) then begin
            cdsObtenerConveniosMorososCobInterna.GotoBookmark(Puntero);
        end;
        cdsObtenerConveniosMorososCobInterna.FreeBookmark(Puntero);
        dsObtenerConveniosMorososCobInterna.DataSet := cdsObtenerConveniosMorososCobInterna;

        CambiarEstadoCursor(CURSOR_DEFECTO, Self);
    end;
end;

procedure TFormCarpetaDeudores.btnSeleccionarTodoCobExtraJudicialClick(Sender: TObject);
begin
	MarcarDesmarcarClientesCobExtraJudicial(True);
    CantCobExtraJudicialMarcados	:= cdsObtenerConveniosMorososCobExtraJudicial.RecordCount;
end;

procedure TFormCarpetaDeudores.btnLiberarSeleccionCobExtraJudicialClick(
  Sender: TObject);
begin
	MarcarDesmarcarClientesCobExtraJudicial(False);
    CantCobExtraJudicialMarcados	:= 0;
end;

procedure TFormCarpetaDeudores.MarcarDesmarcarClientesCobExtraJudicial(Marcar: Boolean);
var
    Puntero: TBookmark;
begin
    try
        CambiarEstadoCursor(CURSOR_RELOJ, Self);
        Puntero := cdsObtenerConveniosMorososCobExtraJudicial.GetBookmark;
        dsObtenerConveniosMorososCobExtraJudicial.DataSet := Nil;

        with cdsObtenerConveniosMorososCobExtraJudicial do begin
        	First;
        	while not Eof do begin
                Edit;
                FieldByName('Marcar').AsBoolean := Marcar;
            	Next;
            end;
        end;

    finally
        if cdsObtenerConveniosMorososCobExtraJudicial.BookmarkValid(Puntero) then begin
            cdsObtenerConveniosMorososCobExtraJudicial.GotoBookmark(Puntero);
        end;
        cdsObtenerConveniosMorososCobExtraJudicial.FreeBookmark(Puntero);
        dsObtenerConveniosMorososCobExtraJudicial.DataSet := cdsObtenerConveniosMorososCobExtraJudicial;

        CambiarEstadoCursor(CURSOR_DEFECTO, Self);
    end;
end;

procedure TFormCarpetaDeudores.btnSeleccionarTodoCobJudicialClick(
  Sender: TObject);
begin
	MarcarDesmarcarClientesCobJudicial(True);
    CantCobJudicialMarcados	:= cdsObtenerConveniosMorososCobJudicial.RecordCount;
end;

procedure TFormCarpetaDeudores.btnLiberarSeleccionCobJudicialClick(
  Sender: TObject);
begin
	MarcarDesmarcarClientesCobJudicial(False);
    CantCobJudicialMarcados	:= 0;
end;

procedure TFormCarpetaDeudores.MarcarDesmarcarClientesCobJudicial(Marcar: Boolean);
var
    Puntero: TBookmark;
begin
    try
        CambiarEstadoCursor(CURSOR_RELOJ, Self);
        Puntero := cdsObtenerConveniosMorososCobJudicial.GetBookmark;
        dsObtenerConveniosMorososCobInterna.DataSet := Nil;

        with cdsObtenerConveniosMorososCobJudicial do begin
        	First;
        	while not Eof do begin
                Edit;
                FieldByName('Marcar').AsBoolean := Marcar;
            	Next;
            end;
        end;

    finally
        if cdsObtenerConveniosMorososCobJudicial.BookmarkValid(Puntero) then begin
            cdsObtenerConveniosMorososCobJudicial.GotoBookmark(Puntero);
        end;
        cdsObtenerConveniosMorososCobJudicial.FreeBookmark(Puntero);
        dsObtenerConveniosMorososCobJudicial.DataSet := cdsObtenerConveniosMorososCobJudicial;

        CambiarEstadoCursor(CURSOR_DEFECTO, Self);
    end;
end;

procedure TFormCarpetaDeudores.peNumeroDocumentoDeudoresButtonClick(
  Sender: TObject);
var
    f: TFormBuscaClientes;
begin
    Application.CreateForm(TFormBuscaClientes, F);
  	if F.Inicializa(FUltimaBusqueda) then begin
    	F.ShowModal;
        if F.ModalResult = idOk then begin
            FUltimaBusqueda := F.UltimaBusqueda;
            peNumeroDocumentoDeudores.Text 	:= f.Persona.NumeroDocumento;
            if f.Persona.Personeria = PERSONERIA_JURIDICA then
            	lblPersonaDeudores.Caption		:= PERSONERIA_JURIDICA_DESC + ' ' + f.Persona.RazonSocial
            else if f.Persona.Personeria = PERSONERIA_FISICA then
            	lblPersonaDeudores.Caption		:= PERSONERIA_FISICA_DESC + ' ' + f.Persona.Nombre + ' ' + f.Persona.Apellido + ' ' + f.Persona.ApellidoMaterno;
	    end;
  	end;
    F.free;
end;

procedure TFormCarpetaDeudores.peNumeroDocumentoCobInternaButtonClick(Sender: TObject);
var
    f: TFormBuscaClientes;
begin
    Application.CreateForm(TFormBuscaClientes, F);
  	if F.Inicializa(FUltimaBusqueda) then begin
    	F.ShowModal;
        if F.ModalResult = idOk then begin
            FUltimaBusqueda := F.UltimaBusqueda;
            peNumeroDocumentoCobInterna.Text 	:= f.Persona.NumeroDocumento;
            if f.Persona.Personeria = PERSONERIA_JURIDICA then
            	lblPersonaCobInterna.Caption		:= PERSONERIA_JURIDICA_DESC + ' ' + f.Persona.RazonSocial
            else if f.Persona.Personeria = PERSONERIA_FISICA then
            	lblPersonaCobInterna.Caption		:= PERSONERIA_FISICA_DESC + ' ' + f.Persona.Nombre + ' ' + f.Persona.Apellido + ' ' + f.Persona.ApellidoMaterno;
	    end;
  	end;
    F.free;
end;

procedure TFormCarpetaDeudores.peNumeroDocumentoDeudoresChange(Sender: TObject);
var
	FCodigoCliente: Integer;
    strTipoPersona, strNombrePersona: string;
begin
	if cdsObtenerConveniosMorososDeudores.Active then
    	cdsObtenerConveniosMorososDeudores.Close;
    FCodigoCliente := QueryGetValueInt(DMConnections.BaseCAC,
                           Format('SELECT CodigoPersona FROM Personas WITH (NOLOCK) WHERE CodigoDocumento = ''RUT'' ' + 'AND NumeroDocumento = ''%s''',
                           [iif(peNumeroDocumentoDeudores.Text <> '', trim(PadL(peNumeroDocumentoDeudores.Text, 9, '0')), '')]));

    if FCodigoCliente > 0 then begin
        strTipoPersona		:= QueryGetValue(DMConnections.BaseCAC, 'SELECT Personeria FROM Personas NOLOCK WHERE CodigoPersona = ' + IntToStr(FCodigoCliente));
        strNombrePersona	:= QueryGetValue(DMConnections.BaseCAC, 'SELECT dbo.ArmarNombrePersona(Personeria, Apellido, ApellidoMaterno, Nombre) FROM Personas NOLOCK WHERE CodigoPersona = ' + IntToStr(FCodigoCliente));
        if strTipoPersona = PERSONERIA_JURIDICA then
        	lblPersonaDeudores.Caption	:= PERSONERIA_JURIDICA_DESC + ' ' + strNombrePersona
        else if strTipoPersona = PERSONERIA_FISICA then
        	lblPersonaDeudores.Caption	:= PERSONERIA_FISICA_DESC + ' ' + strNombrePersona;
        CargarConveniosRUT(DMConnections.BaseCAC, cbConveniosDeudores, 'RUT', peNumeroDocumentoDeudores.Text, False, 0, True);
    end;
end;

procedure TFormCarpetaDeudores.peNumeroDocumentoCobInternaChange(Sender: TObject);
var
	FCodigoCliente: Integer;
    strTipoPersona, strNombrePersona: string;
begin
	if cdsObtenerConveniosMorososCobInterna.Active then
    	cdsObtenerConveniosMorososCobInterna.Close;
    FCodigoCliente := QueryGetValueInt(DMConnections.BaseCAC,
                           Format('SELECT CodigoPersona FROM Personas WITH (NOLOCK) WHERE CodigoDocumento = ''RUT'' ' + 'AND NumeroDocumento = ''%s''',
                           [iif(peNumeroDocumentoCobInterna.Text <> '', trim(PadL(peNumeroDocumentoCobInterna.Text, 9, '0')), '')]));

    if FCodigoCliente > 0 then begin
        strTipoPersona		:= QueryGetValue(DMConnections.BaseCAC, 'SELECT Personeria FROM Personas NOLOCK WHERE CodigoPersona = ' + IntToStr(FCodigoCliente));
        strNombrePersona	:= QueryGetValue(DMConnections.BaseCAC, 'SELECT dbo.ArmarNombrePersona(Personeria, Apellido, ApellidoMaterno, Nombre) FROM Personas NOLOCK WHERE CodigoPersona = ' + IntToStr(FCodigoCliente));
        if strTipoPersona = PERSONERIA_JURIDICA then
        	lblPersonaCobInterna.Caption	:= PERSONERIA_JURIDICA_DESC + ' ' + strNombrePersona
        else if strTipoPersona = PERSONERIA_FISICA then
        	lblPersonaCobInterna.Caption	:= PERSONERIA_FISICA_DESC + ' ' + strNombrePersona;
        CargarConveniosRUT(DMConnections.BaseCAC, cbConveniosCobInterna, 'RUT', peNumeroDocumentoCobInterna.Text, False, 0, True);
    end;
end;

procedure TFormCarpetaDeudores.peNumeroDocumentoCobExtraJudicialButtonClick(
  Sender: TObject);
var
    f: TFormBuscaClientes;
begin
    Application.CreateForm(TFormBuscaClientes, F);
  	if F.Inicializa(FUltimaBusqueda) then begin
    	F.ShowModal;
        if F.ModalResult = idOk then begin
            FUltimaBusqueda := F.UltimaBusqueda;
            peNumeroDocumentoCobExtraJudicial.Text 	:= f.Persona.NumeroDocumento;
            lblPersonaCobExtraJudicial.Caption		:= f.Persona.Personeria + ' ' + f.Persona.Nombre;
	    end;
  	end;
    F.free;
end;

procedure TFormCarpetaDeudores.peNumeroDocumentoCobExtraJudicialChange(
  Sender: TObject);
var
	FCodigoCliente: Integer;
    strTipoPersona, strNombrePersona: string;
begin
	if cdsObtenerConveniosMorososCobInterna.Active then
    	cdsObtenerConveniosMorososCobInterna.Close;
    FCodigoCliente := QueryGetValueInt(DMConnections.BaseCAC,
                           Format('SELECT CodigoPersona FROM Personas WITH (NOLOCK) WHERE CodigoDocumento = ''RUT'' ' + 'AND NumeroDocumento = ''%s''',
                           [iif(peNumeroDocumentoCobExtraJudicial.Text <> '', trim(PadL(peNumeroDocumentoCobExtraJudicial.Text, 9, '0')), '')]));
    if FCodigoCliente > 0 then begin
        strTipoPersona		:= QueryGetValue(DMConnections.BaseCAC, 'SELECT Personeria FROM Personas NOLOCK WHERE CodigoPersona = ' + IntToStr(FCodigoCliente));
        strNombrePersona	:= QueryGetValue(DMConnections.BaseCAC, 'SELECT dbo.ArmarNombrePersona(Personeria, Apellido, ApellidoMaterno, Nombre) FROM Personas NOLOCK WHERE CodigoPersona = ' + IntToStr(FCodigoCliente));
        if strTipoPersona = PERSONERIA_JURIDICA then
        	lblPersonaCobExtraJudicial.Caption	:= PERSONERIA_JURIDICA_DESC + ' ' + strNombrePersona
        else if strTipoPersona = PERSONERIA_FISICA then
        	lblPersonaCobExtraJudicial.Caption	:= PERSONERIA_FISICA_DESC + ' ' + strNombrePersona;
        CargarConveniosRUT(DMConnections.BaseCAC, cbConveniosCobExtraJudicial, 'RUT', peNumeroDocumentoCobExtraJudicial.Text, False, 0, True);
    end;
end;

procedure TFormCarpetaDeudores.peNumeroDocumentoCobJudicialButtonClick(
  Sender: TObject);
var
    f: TFormBuscaClientes;
begin
    Application.CreateForm(TFormBuscaClientes, F);
  	if F.Inicializa(FUltimaBusqueda) then begin
    	F.ShowModal;
        if F.ModalResult = idOk then begin
            FUltimaBusqueda := F.UltimaBusqueda;
            peNumeroDocumentoCobJudicial.Text 	:= f.Persona.NumeroDocumento;
            lblPersonaCobJudicial.Caption		:= f.Persona.Personeria + ' ' + f.Persona.Nombre;
	    end;
  	end;
    F.free;
end;

procedure TFormCarpetaDeudores.peNumeroDocumentoCobJudicialChange(
  Sender: TObject);
var
	FCodigoCliente: Integer;
    strTipoPersona, strNombrePersona: string;
begin
	if cdsObtenerConveniosMorososCobInterna.Active then
    	cdsObtenerConveniosMorososCobInterna.Close;
    FCodigoCliente := QueryGetValueInt(DMConnections.BaseCAC,
                           Format('SELECT CodigoPersona FROM Personas WITH (NOLOCK) WHERE CodigoDocumento = ''RUT'' ' + 'AND NumeroDocumento = ''%s''',
                           [iif(peNumeroDocumentoCobJudicial.Text <> '', trim(PadL(peNumeroDocumentoCobJudicial.Text, 9, '0')), '')]));
    if FCodigoCliente > 0 then begin
        strTipoPersona		:= QueryGetValue(DMConnections.BaseCAC, 'SELECT Personeria FROM Personas NOLOCK WHERE CodigoPersona = ' + IntToStr(FCodigoCliente));
        strNombrePersona	:= QueryGetValue(DMConnections.BaseCAC, 'SELECT dbo.ArmarNombrePersona(Personeria, Apellido, ApellidoMaterno, Nombre) FROM Personas NOLOCK WHERE CodigoPersona = ' + IntToStr(FCodigoCliente));
        if strTipoPersona = PERSONERIA_JURIDICA then
        	lblPersonaCobJudicial.Caption	:= PERSONERIA_JURIDICA_DESC + ' ' + strNombrePersona
        else if strTipoPersona = PERSONERIA_FISICA then
        	lblPersonaCobJudicial.Caption	:= PERSONERIA_FISICA_DESC + ' ' + strNombrePersona;
        CargarConveniosRUT(DMConnections.BaseCAC, cbConveniosCobJudicial, 'RUT', peNumeroDocumentoCobJudicial.Text, False, 0, True);
    end;
end;

procedure TFormCarpetaDeudores.vcbAccionDeCobranzaDeudoresChange(
  Sender: TObject);
begin
	if vcbAccionDeCobranzaDeudores.ItemIndex > 0 then begin
        edMontoMinimoDeudaDeudores.Value	:= StrToFloat(QueryGetValue(DMConnections.BaseCAC, 'select MontoMinimoDeDeuda from AccionesDeCobranza WITH (NOLOCK) where CodigoAccionDeCobranza = '+vcbAccionDeCobranzaDeudores.Value));
        edMontoMaximoDeudaDeudores.Value	:= StrToFloat(QueryGetValue(DMConnections.BaseCAC, 'select MontoMaximoDeDeuda from AccionesDeCobranza WITH (NOLOCK) where CodigoAccionDeCobranza = '+vcbAccionDeCobranzaDeudores.Value));
        edEdadDeudaDeudores.Value			:= StrToFloat(QueryGetValue(DMConnections.BaseCAC, 'select EdadDeDeuda from AccionesDeCobranza WITH (NOLOCK) where CodigoAccionDeCobranza = '+vcbAccionDeCobranzaDeudores.Value));
//        CodigoPlantillaDeudores				:= QueryGetIntegerValue(DMConnections.BaseCAC, 'select CodigoPlantilla from AccionesDeCobranza WITH (NOLOCK) where CodigoAccionDeCobranza = '+vcbAccionDeCobranzaDeudores.Value);
        vcbTipoClienteDeudores.Value		:= QueryGetIntegerValue(DMConnections.BaseCAC, 'select CodigoTipoCliente from AccionesDeCobranza WITH (NOLOCK) where CodigoAccionDeCobranza = '+vcbAccionDeCobranzaDeudores.Value);
        vcbCategoriaDeudaDeudores.Value		:= QueryGetIntegerValue(DMConnections.BaseCAC, 'select CodigoCategoriaDeDeudaDeCobranza from AccionesDeCobranza WITH (NOLOCK) where CodigoAccionDeCobranza = '+vcbAccionDeCobranzaDeudores.Value);
        cbbTipoAccionDeudores.Value			:= QueryGetIntegerValue(DMConnections.BaseCAC, 'select CodigoTipoDeAccionDeCobranza from AccionesDeCobranza WITH (NOLOCK) where CodigoAccionDeCobranza = '+vcbAccionDeCobranzaDeudores.Value);
    end
    else
    	pLimpiarCobDeudores;
end;

procedure TFormCarpetaDeudores.vcbAccionDeCobranzaInternaChange(
  Sender: TObject);
begin
	if vcbAccionDeCobranzaInterna.ItemIndex > 0 then begin
        edMontoMinimoDeudaCobInterna.Value	:= StrToFloat(QueryGetValue(DMConnections.BaseCAC, 'select MontoMinimoDeDeuda from AccionesDeCobranza WITH (NOLOCK) where CodigoAccionDeCobranza = '+vcbAccionDeCobranzaInterna.Value));
        edMontoMaximoDeudaCobInterna.Value	:= StrToFloat(QueryGetValue(DMConnections.BaseCAC, 'select MontoMaximoDeDeuda from AccionesDeCobranza WITH (NOLOCK) where CodigoAccionDeCobranza = '+vcbAccionDeCobranzaInterna.Value));
        edEdadDeudaCobInterna.Value			:= StrToFloat(QueryGetValue(DMConnections.BaseCAC, 'select EdadDeDeuda from AccionesDeCobranza WITH (NOLOCK) where CodigoAccionDeCobranza = '+vcbAccionDeCobranzaInterna.Value));
//        CodigoPlantillaCobInterna			:= QueryGetIntegerValue(DMConnections.BaseCAC, 'select CodigoPlantilla from AccionesDeCobranza WITH (NOLOCK) where CodigoAccionDeCobranza = '+vcbAccionDeCobranzaInterna.Value);
        vcbTipoClienteCobInterna.Value		:= QueryGetIntegerValue(DMConnections.BaseCAC, 'select CodigoTipoCliente from AccionesDeCobranza WITH (NOLOCK) where CodigoAccionDeCobranza = '+vcbAccionDeCobranzaInterna.Value);
        vcbCategoriaDeudaCobInterna.Value	:= QueryGetIntegerValue(DMConnections.BaseCAC, 'select CodigoCategoriaDeDeudaDeCobranza from AccionesDeCobranza WITH (NOLOCK) where CodigoAccionDeCobranza = '+vcbAccionDeCobranzaInterna.Value);
        cbbTipoAccionCobranzaInterna.Value	:= QueryGetIntegerValue(DMConnections.BaseCAC, 'select CodigoTipoDeAccionDeCobranza from AccionesDeCobranza WITH (NOLOCK) where CodigoAccionDeCobranza = '+vcbAccionDeCobranzaInterna.Value);
    end
    else
    	pLimpiarCobInterna;
end;

procedure TFormCarpetaDeudores.vcbAccionDeCobranzaExtraJudicialChange(
  Sender: TObject);
begin
	if vcbAccionDeCobranzaExtraJudicial.ItemIndex > 0 then begin
        edMontoMinimoDeudaCobExtraJudicial.Value	:= StrToFloat(QueryGetValue(DMConnections.BaseCAC, 'select MontoMinimoDeDeuda from AccionesDeCobranza WITH (NOLOCK) where CodigoAccionDeCobranza = '+vcbAccionDeCobranzaExtraJudicial.Value));
        edMontoMaximoDeudaCobExtraJudicial.Value	:= StrToFloat(QueryGetValue(DMConnections.BaseCAC, 'select MontoMaximoDeDeuda from AccionesDeCobranza WITH (NOLOCK) where CodigoAccionDeCobranza = '+vcbAccionDeCobranzaExtraJudicial.Value));
        edEdadDeudaCobExtraJudicial.Value			:= StrToFloat(QueryGetValue(DMConnections.BaseCAC, 'select EdadDeDeuda from AccionesDeCobranza WITH (NOLOCK) where CodigoAccionDeCobranza = '+vcbAccionDeCobranzaExtraJudicial.Value));
        cbTipoClienteCobExtraJudicial.ItemIndex		:= QueryGetIntegerValue(DMConnections.BaseCAC, 'select CodigoTipoCliente from AccionesDeCobranza WITH (NOLOCK) where CodigoAccionDeCobranza = '+vcbAccionDeCobranzaExtraJudicial.Value);
        vcbCategoriaDeudaCobExtraJudicial.Value		:= QueryGetIntegerValue(DMConnections.BaseCAC, 'select CodigoCategoriaDeDeudaDeCobranza from AccionesDeCobranza WITH (NOLOCK) where CodigoAccionDeCobranza = '+vcbAccionDeCobranzaExtraJudicial.Value);
        cbbTipoAccionCobranzaExtraJudicial.Value 	:= QueryGetIntegerValue(DMConnections.BaseCAC, 'select CodigoTipoDeAccionDeCobranza from AccionesDeCobranza WITH (NOLOCK) where CodigoAccionDeCobranza = '+vcbAccionDeCobranzaExtraJudicial.Value);
    end
    else
    	pLimpiarCobExtraJudicial;
end;

procedure TFormCarpetaDeudores.vcbAccionDeCobranzaJudicialChange(
  Sender: TObject);
begin
	if vcbAccionDeCobranzaJudicial.ItemIndex > 0 then begin
        edMontoMinimoDeudaCobJudicial.Value	:= StrToFloat(QueryGetValue(DMConnections.BaseCAC, 'select MontoMinimoDeDeuda from AccionesDeCobranza WITH (NOLOCK) where CodigoAccionDeCobranza = '+vcbAccionDeCobranzaJudicial.Value));
        edMontoMaximoDeudaCobJudicial.Value	:= StrToFloat(QueryGetValue(DMConnections.BaseCAC, 'select MontoMaximoDeDeuda from AccionesDeCobranza WITH (NOLOCK) where CodigoAccionDeCobranza = '+vcbAccionDeCobranzaJudicial.Value));
        edEdadDeudaCobJudicial.Value		:= StrToFloat(QueryGetValue(DMConnections.BaseCAC, 'select EdadDeDeuda from AccionesDeCobranza WITH (NOLOCK) where CodigoAccionDeCobranza = '+vcbAccionDeCobranzaJudicial.Value));
        cbTipoClienteCobJudicial.ItemIndex	:= QueryGetIntegerValue(DMConnections.BaseCAC, 'select CodigoTipoCliente from AccionesDeCobranza WITH (NOLOCK) where CodigoAccionDeCobranza = '+vcbAccionDeCobranzaJudicial.Value);
        vcbCategoriaDeudaCobJudicial.Value	:= QueryGetIntegerValue(DMConnections.BaseCAC, 'select CodigoCategoriaDeDeudaDeCobranza from AccionesDeCobranza WITH (NOLOCK) where CodigoAccionDeCobranza = '+vcbAccionDeCobranzaJudicial.Value);
        cbbTipoAccionCobranzaJudicial.Value	:= QueryGetIntegerValue(DMConnections.BaseCAC, 'select CodigoTipoDeAccionDeCobranza from AccionesDeCobranza WITH (NOLOCK) where CodigoAccionDeCobranza = '+vcbAccionDeCobranzaJudicial.Value);
    end
    else
    	pLimpiarCobJudicial;
end;

procedure TFormCarpetaDeudores.btnLimpiarCobDeudoresClick(Sender: TObject);
begin
	pLimpiarCobDeudores();
end;

procedure TFormCarpetaDeudores.pLimpiarCobDeudores();
begin
    edMontoMinimoDeudaDeudores.Clear;
    edMontoMaximoDeudaDeudores.Clear;
    peNumeroDocumentoDeudores.Clear;
    cbConveniosDeudores.Items.Clear;
    edEdadDeudaDeudores.Clear;
    lblPersonaDeudores.Caption						:= '';
    lblAccionDeudores.Caption						:= '';
	vcbAccionDeCobranzaDeudores.ItemIndex			:= 0;
    vcbTipoClienteDeudores.ItemIndex				:= -1;
    cbbTipoAccionDeudores.ItemIndex					:= -1;
    vcbCategoriaDeudaDeudores.ItemIndex				:= -1;
    chbCandidatoInhabilitacionDeudores.Checked		:= False;
end;

procedure TFormCarpetaDeudores.btnLimpiarCobInternaClick(Sender: TObject);
begin
	pLimpiarCobInterna();
end;

procedure TFormCarpetaDeudores.pLimpiarCobInterna();
begin
    edMontoMinimoDeudaCobInterna.Clear;
    edMontoMaximoDeudaCobInterna.Clear;
    peNumeroDocumentoCobInterna.Clear;
    cbConveniosCobInterna.Items.Clear;
    edEdadDeudaCobInterna.Clear;
    lblPersonaCobInterna.Caption					:= '';
    lblAccionCobranzaInterna.Caption				:= '';
	vcbAccionDeCobranzaInterna.ItemIndex			:= 0;
    vcbTipoClienteCobInterna.ItemIndex				:= -1;
    cbbTipoAccionCobranzaInterna.ItemIndex			:= -1;
    vcbCategoriaDeudaCobInterna.ItemIndex			:= -1;
    chbCandidatoInhabilitacionCobInterna.Checked	:= False;
end;

procedure TFormCarpetaDeudores.btnLimpiarCobExtraJudicialClick(Sender: TObject);
begin
	pLimpiarCobExtraJudicial();
end;

procedure TFormCarpetaDeudores.pLimpiarCobExtraJudicial();
begin
    edMontoMinimoDeudaCobExtraJudicial.Clear;
    edMontoMaximoDeudaCobExtraJudicial.Clear;
    peNumeroDocumentoCobExtraJudicial.Clear;
    cbConveniosCobExtraJudicial.Items.Clear;
    edEdadDeudaCobExtraJudicial.Clear;
    lblPersonaCobExtraJudicial.Caption			:= '';
	vcbAccionDeCobranzaExtraJudicial.ItemIndex	:= 0;
    cbTipoClienteCobExtraJudicial.ItemIndex		:= -1;
    cbbTipoAccionCobranzaExtraJudicial.ItemIndex:= -1;
    vcbCategoriaDeudaCobExtraJudicial.ItemIndex	:= -1;
    chbCandidatoInhabilitacionCobExtraJudicial.Checked	:= False;
end;

procedure TFormCarpetaDeudores.btnLimpiarCobJudicialClick(Sender: TObject);
begin
	pLimpiarCobJudicial();
end;

procedure TFormCarpetaDeudores.pLimpiarCobJudicial();
begin
    edMontoMinimoDeudaCobJudicial.Clear;
    edMontoMaximoDeudaCobJudicial.Clear;
    peNumeroDocumentoCobJudicial.Clear;
    cbConveniosCobJudicial.Items.Clear;
    edEdadDeudaCobJudicial.Clear;
    lblPersonaCobJudicial.Caption					:= '';
    cbTipoClienteCobJudicial.ItemIndex				:= -1;
	vcbAccionDeCobranzaJudicial.ItemIndex			:= 0;
    vcbCategoriaDeudaCobJudicial.ItemIndex			:= -1;
    cbbTipoAccionCobranzaJudicial.ItemIndex			:= -1;
    chbCandidatoInhabilitacionCobJudicial.Checked	:= False;
end;

procedure TFormCarpetaDeudores.btnBuscarDeudoresClick(Sender: TObject);
var
	MontoMinimoDeuda: Double;
    EdadDeuda		: Integer;
begin
	try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;
		CantDeudoresMarcados	:= 0;
    	with spObtenerConveniosMorososDeudores do begin
            Close;
            Parameters.Clear;
            Parameters.Refresh;
            if chbCandidatoInhabilitacionDeudores.Checked then begin
                try
                    MontoMinimoDeuda	:= StrToFloat(QueryGetValue(DMConnections.BaseBO_Master, 'SELECT Valor FROM ParametrosGenerales NOLOCK WHERE Nombre = ''MONTO_MINIMO_INGRESAR_CONVENIO_LISTA_AMARILLA'''));
                    EdadDeuda			:= StrToInt(QueryGetValue(DMConnections.BaseBO_Master, 'SELECT Valor FROM ParametrosGenerales NOLOCK WHERE Nombre = ''CANT_DIAS_INGRESAR_CONVENIO_LISTA_AMARILLA'''));
                except
                    MsgBox(MSG_ERROR_OBTENIENDO_DATOS, Caption, MB_ICONERROR);
                    Exit;
                end;
            end
            else begin
                MontoMinimoDeuda	:= edMontoMinimoDeudaDeudores.Value;
                EdadDeuda			:= Trunc(edEdadDeudaDeudores.Value);
            end;

            Parameters.ParamByName('@CodigoTipoCliente').Value	        := iif(vcbTipoClienteDeudores.ItemIndex < 1, null, vcbTipoClienteDeudores.Value);
            Parameters.ParamByName('@MontoMinimoDeuda').Value	        := MontoMinimoDeuda;
            Parameters.ParamByName('@MontoMaximoDeuda').Value	        := iif(edMontoMaximoDeudaDeudores.Value < 1, null, edMontoMaximoDeudaDeudores.Value);
            Parameters.ParamByName('@DiasDeuda').Value			        := EdadDeuda;
            Parameters.ParamByName('@CategoriaDeuda').Value		        := iif(vcbCategoriaDeudaDeudores.ItemIndex < 1, null, vcbCategoriaDeudaDeudores.Value);
            Parameters.ParamByName('@NumeroDocumento').Value	        := iif(Trim(peNumeroDocumentoDeudores.Text) = '', null, Trim(peNumeroDocumentoDeudores.Text));
            Parameters.ParamByName('@CodigoConvenio').Value		        := iif(cbConveniosDeudores.Value = 0, Null, cbConveniosDeudores.Value);
        end;

        TraeRegistrosDeudores;

    finally
        Screen.Cursor := crDefault;
    end;
end;

function TFormCarpetaDeudores.TraeRegistrosDeudores: Boolean;
resourcestring
   	MSG_ERROR_SELECT = 'Error Consultando Registros';
var
	Resultado     : Boolean;
	Retorno, i    : Integer;
	MensajeError  : string;
begin
  	try
    	spObtenerConveniosMorososDeudores.Open;
    	Retorno := spObtenerConveniosMorososDeudores.Parameters.ParamByName('@RETURN_VALUE').Value;

    	if Retorno <> 0 then begin
      		MensajeError := spObtenerConveniosMorososDeudores.Parameters.ParamByName('@ErrorDescription').Value;
      		Application.MessageBox(PChar(MensajeError),'Problema', MB_ICONERROR);
        end;

    	cdsObtenerConveniosMorososDeudores.DisableControls;

    	cdsObtenerConveniosMorososDeudores.Active   := False;

    	for i := 0 to cdsObtenerConveniosMorososDeudores.Fields.Count - 1 do
      		cdsObtenerConveniosMorososDeudores.Fields[i].ReadOnly := False;

    	try
      		cdsObtenerConveniosMorososDeudores.EmptyDataSet;
    	except
    	end;

    	cdsObtenerConveniosMorososDeudores.CreateDataSet;
        
    	cdsObtenerConveniosMorososDeudores.Active   := True;
    	cdsObtenerConveniosMorososDeudores.ReadOnly := False;

    	spObtenerConveniosMorososDeudores.First;
    	while not spObtenerConveniosMorososDeudores.eof do begin
      		cdsObtenerConveniosMorososDeudores.Append;

			cdsObtenerConveniosMorososDeudores.FieldByName('CodigoConvenio').Value	        := spObtenerConveniosMorososDeudores.FieldByName('CodigoConvenio').Value;
			cdsObtenerConveniosMorososDeudores.FieldByName('NumeroDocumento').Value	        := spObtenerConveniosMorososDeudores.FieldByName('NumeroDocumento').Value;
			cdsObtenerConveniosMorososDeudores.FieldByName('CodigoCliente').Value	        := spObtenerConveniosMorososDeudores.FieldByName('CodigoCliente').Value;
			cdsObtenerConveniosMorososDeudores.FieldByName('NombrePersona').Value	        := spObtenerConveniosMorososDeudores.FieldByName('NombrePersona').Value;
			cdsObtenerConveniosMorososDeudores.FieldByName('NumeroConvenio').Value	        := spObtenerConveniosMorososDeudores.FieldByName('NumeroConvenio').Value;
			cdsObtenerConveniosMorososDeudores.FieldByName('Candidato').Value			    := spObtenerConveniosMorososDeudores.FieldByName('Candidato').Value;
			cdsObtenerConveniosMorososDeudores.FieldByName('TipoDeCliente').Value    	    := spObtenerConveniosMorososDeudores.FieldByName('TipoDeCliente').Value;
			cdsObtenerConveniosMorososDeudores.FieldByName('Saldo').Value				    := spObtenerConveniosMorososDeudores.FieldByName('Saldo').Value;
			cdsObtenerConveniosMorososDeudores.FieldByName('DiasDeuda').Value			    := spObtenerConveniosMorososDeudores.FieldByName('DiasDeuda').Value;
			cdsObtenerConveniosMorososDeudores.FieldByName('CantidadComprobantes').Value	:= spObtenerConveniosMorososDeudores.FieldByName('CantidadComprobantes').Value;
			cdsObtenerConveniosMorososDeudores.FieldByName('FechaVencimiento').Value		:= spObtenerConveniosMorososDeudores.FieldByName('FechaVencimiento').Value;
            cdsObtenerConveniosMorososDeudores.FieldByName('AccionCobranza').Value			:= spObtenerConveniosMorososDeudores.FieldByName('AccionCobranza').Value;
            cdsObtenerConveniosMorososDeudores.FieldByName('EstadoAccionCobranza').Value	:= spObtenerConveniosMorososDeudores.FieldByName('EstadoAccionCobranza').Value;

            cdsObtenerConveniosMorososDeudores.FieldByName('EmpresaCobranza').Value			:= spObtenerConveniosMorososDeudores.FieldByName('EmpresaCobranza').Value;
            cdsObtenerConveniosMorososDeudores.FieldByName('FechaEnvioCobranza').Value		:= spObtenerConveniosMorososDeudores.FieldByName('FechaEnvioCobranza').Value;
            cdsObtenerConveniosMorososDeudores.FieldByName('Refinanciacion').Value			:= spObtenerConveniosMorososDeudores.FieldByName('Refinanciacion').Value;
            cdsObtenerConveniosMorososDeudores.FieldByName('EstadoRefinanciacion').Value	:= spObtenerConveniosMorososDeudores.FieldByName('EstadoRefinanciacion').Value;
            cdsObtenerConveniosMorososDeudores.FieldByName('AvenimientoJudicial').Value		:= spObtenerConveniosMorososDeudores.FieldByName('AvenimientoJudicial').Value;
            cdsObtenerConveniosMorososDeudores.FieldByName('EstadoAvenimiento').Value		:= spObtenerConveniosMorososDeudores.FieldByName('EstadoAvenimiento').Value;
            
      		cdsObtenerConveniosMorososDeudores.Post;
      		spObtenerConveniosMorososDeudores.Next;
    	end;

    	spObtenerConveniosMorososDeudores.Close;
    	cdsObtenerConveniosMorososDeudores.First;
    	cdsObtenerConveniosMorososDeudores.EnableControls;

    	Resultado := True;

  	except
    	on E : Exception do begin
      		MsgBoxErr(MSG_ERROR_SELECT, E.Message, Caption, MB_ICONERROR);
      		Resultado := False;
    	end;
  	end;

  Result := Resultado;
end;

procedure TFormCarpetaDeudores.btnBuscarCobInternaClick(Sender: TObject);
var
	MontoMinimoDeuda: Double;
    EdadDeuda		: Integer;
begin
	try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;
		CantCobInternaMarcados	:= 0;
    	with spObtenerConveniosMorososCobInterna do begin
            Close;
            Parameters.Clear;
            Parameters.Refresh;
            if chbCandidatoInhabilitacionCobInterna.Checked then begin
                try
                    MontoMinimoDeuda	:= StrToFloat(QueryGetValue(DMConnections.BaseBO_Master, 'SELECT Valor FROM ParametrosGenerales NOLOCK WHERE Nombre = ''MONTO_MINIMO_INGRESAR_CONVENIO_LISTA_AMARILLA'''));
                    EdadDeuda			:= StrToInt(QueryGetValue(DMConnections.BaseBO_Master, 'SELECT Valor FROM ParametrosGenerales NOLOCK WHERE Nombre = ''CANT_DIAS_INGRESAR_CONVENIO_LISTA_AMARILLA'''));
                except
                    MsgBox(MSG_ERROR_OBTENIENDO_DATOS, Caption, MB_ICONERROR);
                    Exit;
                end;

            end
            else begin
                MontoMinimoDeuda	:= edMontoMinimoDeudaCobInterna.Value;
                EdadDeuda			:= Trunc(edEdadDeudaCobInterna.Value);
            end;

            Parameters.ParamByName('@CodigoTipoCliente').Value	        := iif(vcbTipoClienteCobInterna.ItemIndex < 1, null, vcbTipoClienteCobInterna.Value);
            Parameters.ParamByName('@MontoMinimoDeuda').Value	        := MontoMinimoDeuda;
            Parameters.ParamByName('@MontoMaximoDeuda').Value	        := iif(edMontoMaximoDeudaCobInterna.Value < 1, null, edMontoMaximoDeudaCobInterna.Value);
            Parameters.ParamByName('@DiasDeuda').Value			        := EdadDeuda;
            Parameters.ParamByName('@CategoriaDeuda').Value		        := iif(vcbCategoriaDeudaCobInterna.ItemIndex < 1, null, vcbCategoriaDeudaCobInterna.Value);
            Parameters.ParamByName('@NumeroDocumento').Value	        := iif(Trim(peNumeroDocumentoCobInterna.Text) = '', null, Trim(peNumeroDocumentoCobInterna.Text));
            Parameters.ParamByName('@CodigoConvenio').Value		        := iif(cbConveniosCobInterna.Value = 0, Null, cbConveniosCobInterna.Value);

        end;

        TraeRegistrosCobInterna;

    finally
        Screen.Cursor := crDefault;
    end;
end;

function TFormCarpetaDeudores.TraeRegistrosCobInterna;
resourcestring
   	MSG_ERROR_SELECT = 'Error Consultando Registros';
var
	Resultado     : Boolean;
	Retorno, i    : Integer;
	MensajeError  : string;
begin
  	try
    	spObtenerConveniosMorososCobInterna.Open;
    	Retorno := spObtenerConveniosMorososCobInterna.Parameters.ParamByName('@RETURN_VALUE').Value;

    	if Retorno <> 0 then begin
      		MensajeError := spObtenerConveniosMorososCobInterna.Parameters.ParamByName('@ErrorDescription').Value;
      		Application.MessageBox(PChar(MensajeError),'Problema', MB_ICONERROR);
        end;

    	cdsObtenerConveniosMorososCobInterna.DisableControls;

    	cdsObtenerConveniosMorososCobInterna.Active   := False;

    	for i := 0 to cdsObtenerConveniosMorososCobInterna.Fields.Count - 1 do
      		cdsObtenerConveniosMorososCobInterna.Fields[i].ReadOnly := False;

    	try
      		cdsObtenerConveniosMorososCobInterna.EmptyDataSet;
    	except
    	end;

    	cdsObtenerConveniosMorososCobInterna.CreateDataSet;
    	cdsObtenerConveniosMorososCobInterna.Active   := True;
    	cdsObtenerConveniosMorososCobInterna.ReadOnly := False;

    	spObtenerConveniosMorososCobInterna.First;
    	while not spObtenerConveniosMorososCobInterna.eof do begin
      		cdsObtenerConveniosMorososCobInterna.Append;

			cdsObtenerConveniosMorososCobInterna.FieldByName('NumeroDocumento').Value	    := spObtenerConveniosMorososCobInterna.FieldByName('NumeroDocumento').Value;
			cdsObtenerConveniosMorososCobInterna.FieldByName('NombrePersona').Value	        := spObtenerConveniosMorososCobInterna.FieldByName('NombrePersona').Value;
			cdsObtenerConveniosMorososCobInterna.FieldByName('NumeroConvenio').Value	    := spObtenerConveniosMorososCobInterna.FieldByName('NumeroConvenio').Value;
			cdsObtenerConveniosMorososCobInterna.FieldByName('TipoCliente').Value	    	:= spObtenerConveniosMorososCobInterna.FieldByName('TipoDeCliente').Value;
			cdsObtenerConveniosMorososCobInterna.FieldByName('Candidato').Value			    := spObtenerConveniosMorososCobInterna.FieldByName('Candidato').Value;
			cdsObtenerConveniosMorososCobInterna.FieldByName('Saldo').Value				    := spObtenerConveniosMorososCobInterna.FieldByName('Saldo').Value;
			cdsObtenerConveniosMorososCobInterna.FieldByName('CantComprobantes').Value	    := spObtenerConveniosMorososCobInterna.FieldByName('CantidadComprobantes').Value;
			cdsObtenerConveniosMorososCobInterna.FieldByName('DiasDeuda').Value			    := spObtenerConveniosMorososCobInterna.FieldByName('DiasDeuda').Value;
			cdsObtenerConveniosMorososCobInterna.FieldByName('FechaVencimiento').Value		:= spObtenerConveniosMorososCobInterna.FieldByName('FechaVencimiento').Value;
            cdsObtenerConveniosMorososCobInterna.FieldByName('AccionCobranza').Value		:= spObtenerConveniosMorososCobInterna.FieldByName('AccionCobranza').Value;
            cdsObtenerConveniosMorososCobInterna.FieldByName('EstadoAccionCobranza').Value	:= spObtenerConveniosMorososCobInterna.FieldByName('EstadoAccionCobranza').Value;
			cdsObtenerConveniosMorososCobInterna.FieldByName('CodigoCliente').Value	        := spObtenerConveniosMorososCobInterna.FieldByName('CodigoCliente').Value;
			cdsObtenerConveniosMorososCobInterna.FieldByName('CodigoConvenio').Value	    := spObtenerConveniosMorososCobInterna.FieldByName('CodigoConvenio').Value;

      		cdsObtenerConveniosMorososCobInterna.Post;
      		spObtenerConveniosMorososCobInterna.Next;
    	end;

    	for i := 1 to cdsObtenerConveniosMorososCobInterna.Fields.Count - 1 do
      		cdsObtenerConveniosMorososCobInterna.Fields[i].ReadOnly := True;

    	spObtenerConveniosMorososCobInterna.Close;
    	cdsObtenerConveniosMorososCobInterna.First;
    	cdsObtenerConveniosMorososCobInterna.EnableControls;

    	Resultado := True;

  	except
    	on E : Exception do begin
      		MsgBoxErr(MSG_ERROR_SELECT, E.Message, Caption, MB_ICONERROR);
      		Resultado := False;
    	end;
  	end;

  Result := Resultado;
end;

procedure TFormCarpetaDeudores.btnBuscarCobExtraJudicialClick(Sender: TObject);
var
	MontoMinimoDeuda: Double;
    EdadDeuda		: Integer;
begin
	try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;
        CantCobExtraJudicialMarcados	:= 0;
    	with spObtenerConveniosMorososCobExtraJudicial do begin
            Close;
            Parameters.Clear;
            Parameters.Refresh;
            if chbCandidatoInhabilitacionCobExtraJudicial.Checked then begin
                try
                    MontoMinimoDeuda	:= StrToFloat(QueryGetValue(DMConnections.BaseBO_Master, 'SELECT Valor FROM ParametrosGenerales NOLOCK WHERE Nombre = ''MONTO_MINIMO_INGRESAR_CONVENIO_LISTA_AMARILLA'''));
                    EdadDeuda			:= StrToInt(QueryGetValue(DMConnections.BaseBO_Master, 'SELECT Valor FROM ParametrosGenerales NOLOCK WHERE Nombre = ''CANT_DIAS_INGRESAR_CONVENIO_LISTA_AMARILLA'''));
                except
                    MsgBox(MSG_ERROR_OBTENIENDO_DATOS, Caption, MB_ICONERROR);
                    Exit;
                end;

            end
            else begin
                MontoMinimoDeuda	:= edMontoMinimoDeudaCobExtraJudicial.Value;
                EdadDeuda			:= Trunc(edEdadDeudaCobExtraJudicial.Value);
            end;
            Parameters.ParamByName('@CodigoTipoCliente').Value	:= iif(cbTipoClienteCobExtraJudicial.ItemIndex < 1, null, cbTipoClienteCobExtraJudicial.Value);
            Parameters.ParamByName('@MontoMinimoDeuda').Value	:= MontoMinimoDeuda;
            Parameters.ParamByName('@MontoMaximoDeuda').Value	:= iif(edMontoMaximoDeudaCobExtraJudicial.Value < 1, null, edMontoMaximoDeudaCobExtraJudicial.Value);
            Parameters.ParamByName('@DiasDeuda').Value			:= EdadDeuda;
            Parameters.ParamByName('@CategoriaDeuda').Value		:= iif(vcbCategoriaDeudaCobExtraJudicial.ItemIndex < 1, null, vcbCategoriaDeudaCobExtraJudicial.Value);
            Parameters.ParamByName('@NumeroDocumento').Value	:= iif(Trim(peNumeroDocumentoCobExtraJudicial.Text) = '', null, Trim(peNumeroDocumentoCobExtraJudicial.Text));
            Parameters.ParamByName('@CodigoConvenio').Value		:= iif(cbConveniosCobExtraJudicial.Value = 0, Null, cbConveniosCobExtraJudicial.Value);
            Parameters.ParamByName('@EstadoConvenio').Value		:= CONST_ESTADO_CONVENIO_COBRANZA_EXTRAJUDICIAL;

        end;

        TraeRegistrosCobExtraJudicial;

    finally
        Screen.Cursor := crDefault;
    end;
end;

function TFormCarpetaDeudores.TraeRegistrosCobExtraJudicial;
resourcestring
   	MSG_ERROR_SELECT = 'Error Consultando Registros';
var
	Resultado     : Boolean;
	Retorno, i    : Integer;
	MensajeError  : string;
begin
  	try
    	spObtenerConveniosMorososCobExtraJudicial.Open;
    	Retorno := spObtenerConveniosMorososCobExtraJudicial.Parameters.ParamByName('@RETURN_VALUE').Value;

    	if Retorno <> 0 then begin
      		MensajeError := spObtenerConveniosMorososCobExtraJudicial.Parameters.ParamByName('@ErrorDescription').Value;
      		Application.MessageBox(PChar(MensajeError),'Problema', MB_ICONERROR);
        end;

    	cdsObtenerConveniosMorososCobExtraJudicial.DisableControls;

    	cdsObtenerConveniosMorososCobExtraJudicial.Active   := False;

    	for i := 0 to cdsObtenerConveniosMorososCobExtraJudicial.Fields.Count - 1 do
      		cdsObtenerConveniosMorososCobExtraJudicial.Fields[i].ReadOnly := False;

    	try
      		cdsObtenerConveniosMorososCobExtraJudicial.EmptyDataSet;
    	except
    	end;

    	cdsObtenerConveniosMorososCobExtraJudicial.CreateDataSet;
    	cdsObtenerConveniosMorososCobExtraJudicial.Active   := True;
    	cdsObtenerConveniosMorososCobExtraJudicial.ReadOnly := False;

    	spObtenerConveniosMorososCobExtraJudicial.First;
    	while not spObtenerConveniosMorososCobExtraJudicial.eof do begin
      		cdsObtenerConveniosMorososCobExtraJudicial.Append;

			cdsObtenerConveniosMorososCobExtraJudicial.FieldByName('NumeroDocumento').Value	        := spObtenerConveniosMorososCobExtraJudicial.FieldByName('NumeroDocumento').Value;
			cdsObtenerConveniosMorososCobExtraJudicial.FieldByName('NombrePersona').Value	        := spObtenerConveniosMorososCobExtraJudicial.FieldByName('NombrePersona').Value;
			cdsObtenerConveniosMorososCobExtraJudicial.FieldByName('NumeroConvenio').Value	        := spObtenerConveniosMorososCobExtraJudicial.FieldByName('NumeroConvenio').Value;
			cdsObtenerConveniosMorososCobExtraJudicial.FieldByName('TipoCliente').Value	    	    := spObtenerConveniosMorososCobExtraJudicial.FieldByName('TipoDeCliente').Value;
			cdsObtenerConveniosMorososCobExtraJudicial.FieldByName('Candidato').Value			    := spObtenerConveniosMorososCobExtraJudicial.FieldByName('Candidato').Value;
			cdsObtenerConveniosMorososCobExtraJudicial.FieldByName('Saldo').Value				    := spObtenerConveniosMorososCobExtraJudicial.FieldByName('Saldo').Value;
			cdsObtenerConveniosMorososCobExtraJudicial.FieldByName('CantComprobantes').Value	    := spObtenerConveniosMorososCobExtraJudicial.FieldByName('CantidadComprobantes').Value;
			cdsObtenerConveniosMorososCobExtraJudicial.FieldByName('DiasDeuda').Value			    := spObtenerConveniosMorososCobExtraJudicial.FieldByName('DiasDeuda').Value;
			cdsObtenerConveniosMorososCobExtraJudicial.FieldByName('FechaVencimiento').Value	    := spObtenerConveniosMorososCobExtraJudicial.FieldByName('FechaVencimiento').Value;
            cdsObtenerConveniosMorososCobExtraJudicial.FieldByName('AccionCobranza').Value			:= spObtenerConveniosMorososCobExtraJudicial.FieldByName('AccionCobranza').Value;
            cdsObtenerConveniosMorososCobExtraJudicial.FieldByName('EstadoAccionCobranza').Value	:= spObtenerConveniosMorososCobExtraJudicial.FieldByName('EstadoAccionCobranza').Value;
			cdsObtenerConveniosMorososCobExtraJudicial.FieldByName('CodigoCliente').Value	        := spObtenerConveniosMorososCobExtraJudicial.FieldByName('CodigoCliente').Value;
			cdsObtenerConveniosMorososCobExtraJudicial.FieldByName('CodigoConvenio').Value	    	:= spObtenerConveniosMorososCobExtraJudicial.FieldByName('CodigoConvenio').Value;

      		cdsObtenerConveniosMorososCobExtraJudicial.Post;
      		spObtenerConveniosMorososCobExtraJudicial.Next;
    	end;

    	for i := 1 to cdsObtenerConveniosMorososCobExtraJudicial.Fields.Count - 1 do
      		cdsObtenerConveniosMorososCobExtraJudicial.Fields[i].ReadOnly := True;

    	spObtenerConveniosMorososCobExtraJudicial.Close;
    	cdsObtenerConveniosMorososCobExtraJudicial.First;
    	cdsObtenerConveniosMorososCobExtraJudicial.EnableControls;

    	Resultado := True;

  	except
    	on E : Exception do begin
      		MsgBoxErr(MSG_ERROR_SELECT, E.Message, Caption, MB_ICONERROR);
      		Resultado := False;
    	end;
  	end;

  Result := Resultado;
end;

procedure TFormCarpetaDeudores.btnBuscarCobJudicialClick(Sender: TObject);
var
	MontoMinimoDeuda: Double;
    EdadDeuda		: Integer;
begin
	try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;
		CantCobJudicialMarcados	:= 0;
    	with spObtenerConveniosMorososCobJudicial do begin
            Close;
            Parameters.Clear;
            Parameters.Refresh;
            if chbCandidatoInhabilitacionCobJudicial.Checked then begin
                try
                    MontoMinimoDeuda	:= StrToFloat(QueryGetValue(DMConnections.BaseBO_Master, 'SELECT Valor FROM ParametrosGenerales NOLOCK WHERE Nombre = ''MONTO_MINIMO_INGRESAR_CONVENIO_LISTA_AMARILLA'''));
                    EdadDeuda			:= StrToInt(QueryGetValue(DMConnections.BaseBO_Master, 'SELECT Valor FROM ParametrosGenerales NOLOCK WHERE Nombre = ''CANT_DIAS_INGRESAR_CONVENIO_LISTA_AMARILLA'''));
                except
                    MsgBox(MSG_ERROR_OBTENIENDO_DATOS, Caption, MB_ICONERROR);
                    Exit;
                end;

            end
            else begin
                MontoMinimoDeuda	:= edMontoMinimoDeudaCobJudicial.Value;
                EdadDeuda			:= Trunc(edEdadDeudaCobJudicial.Value);

            end;
            Parameters.ParamByName('@CodigoTipoCliente').Value	:= iif(cbTipoClienteCobJudicial.ItemIndex < 1, null, cbTipoClienteCobJudicial.Value);
            Parameters.ParamByName('@MontoMinimoDeuda').Value	:= MontoMinimoDeuda;
            Parameters.ParamByName('@MontoMaximoDeuda').Value	:= iif(edMontoMaximoDeudaCobJudicial.Value < 1, null, edMontoMaximoDeudaCobJudicial.Value);
            Parameters.ParamByName('@DiasDeuda').Value			:= EdadDeuda;
            Parameters.ParamByName('@CategoriaDeuda').Value		:= iif(vcbCategoriaDeudaCobJudicial.ItemIndex < 1, null, vcbCategoriaDeudaCobJudicial.Value);
            Parameters.ParamByName('@NumeroDocumento').Value	:= iif(Trim(peNumeroDocumentoCobJudicial.Text) = '', null, Trim(peNumeroDocumentoCobJudicial.Text));
            Parameters.ParamByName('@CodigoConvenio').Value		:= iif(cbConveniosCobJudicial.Value = 0, Null, cbConveniosCobJudicial.Value);
            Parameters.ParamByName('@EstadoConvenio').Value		:= CONST_ESTADO_CONVENIO_COBRANZA_JUDICIAL;

        end;

        TraeRegistrosCobJudicial;

    finally
        Screen.Cursor := crDefault;
    end;
end;

procedure TFormCarpetaDeudores.btnDefinirTipoClienteDeudoresClick(
  Sender: TObject);
resourcestring
	MSG_NO_HAY_REGISTROS 	= 'No hay registros para definir Tipo de Cliente';
    MSG_NO_HAY_SELECCION	= 'No ha seleccionado registros para Definir Tipo de Cliente';
    MSG_ACCION_UNITARIA		= 'Para definir tipo de cliente, s�lo debe seleccionar un convenio';
    MSG_CONFIRMA_SELECCION	= '�Seguro asigna el tipo de cliente ''%s'' al cliente ''%s''?';
    MSG_DEFINE_TIPO_CLIENTE_TITLE	= 'Definir Tipo de Cliente';
var
	CodigoTipoDeCliente: Integer;
    Seleccion: string;
    f: TFormSeleccionarTipoCliente;
begin
	try
    	Screen.Cursor 	:= crHourGlass;
        cdsObtenerConveniosMorososCobInterna.DisableControls;

        if (not cdsObtenerConveniosMorososDeudores.Active) or (cdsObtenerConveniosMorososDeudores.RecordCount=0) then begin
            MsgBox(MSG_NO_HAY_REGISTROS, Caption, MB_ICONINFORMATION);
            Exit;
        end;
    
		if CantDeudoresMarcados = 0 then begin
            MsgBox(MSG_NO_HAY_SELECCION, Caption, MB_ICONINFORMATION);
            Exit;
        end;

        if CantDeudoresMarcados > 1 then begin
            MsgBox(MSG_ACCION_UNITARIA, Caption, MB_ICONINFORMATION);
            Exit;
        end;

        Application.CreateForm(TFormSeleccionarTipoCliente,f);
        if f.Inicializar() and (f.ShowModal = mrOK) then begin
            CodigoTipoDeCliente				:= StrToInt(f.ListaCodigosTiposCliente[f.rgTiposCliente.ItemIndex]);
            Seleccion						:= f.rgTiposCliente.Items[f.rgTiposCliente.ItemIndex];
            with spActualizarTipoClienteConvenio, Parameters do begin
                if Active then
                	Close;
                Parameters.Refresh;
                ParamByName('@CodigoConvenio').Value	:= cdsObtenerConveniosMorososDeudoresCodigoConvenio.AsInteger;
                ParamByName('@CodigoTipoCliente').Value	:= CodigoTipoDeCliente;
                ParamByName('@Usuario').Value			:= UsuarioSistema;
                try
                	ExecProc;
                except
					on E:Exception do begin
                        MsgBox(E.Message, Caption, MB_ICONERROR);
                        Exit;
                    end;
                end;
            end;
        end;
    finally
    	cdsObtenerConveniosMorososCobInterna.EnableControls;
		Screen.Cursor := crDefault;
        btnBuscarDeudoresClick(nil);
    end;
end;

procedure TFormCarpetaDeudores.btnDefinirTipoClienteCobInternaClick(
  Sender: TObject);
resourcestring
	MSG_NO_HAY_REGISTROS 	= 'No hay registros para definir Tipo de Cliente';
    MSG_NO_HAY_SELECCION	= 'No ha seleccionado registros para Definir Tipo de Cliente';
    MSG_ACCION_UNITARIA		= 'Para definir tipo de cliente, s�lo debe seleccionar un convenio';
    MSG_CONFIRMA_SELECCION	= '�Seguro asigna el tipo de cliente ''%s'' al cliente ''%s''?';
    MSG_DEFINE_TIPO_CLIENTE_TITLE	= 'Definir Tipo de Cliente';
var
	CodigoTipoDeCliente: Integer;
    Seleccion: string;
    f: TFormSeleccionarTipoCliente;
begin
	if (not cdsObtenerConveniosMorososCobInterna.Active) or (cdsObtenerConveniosMorososCobInterna.RecordCount=0) then begin
    	MsgBox(MSG_NO_HAY_REGISTROS, Caption, MB_ICONINFORMATION);
        Exit;
    end;
    
    if CantCobInternaMarcados = 0 then begin
    	MsgBox(MSG_NO_HAY_SELECCION, Caption, MB_ICONINFORMATION);
        Exit;
    end;

    if CantCobInternaMarcados > 1 then begin
    	MsgBox(MSG_ACCION_UNITARIA, Caption, MB_ICONINFORMATION);
        Exit;
    end;

	try
        Application.CreateForm(TFormSeleccionarTipoCliente,f);
        if f.Inicializar() and (f.ShowModal = mrOK) then begin
            CodigoTipoDeCliente				:= StrToInt(f.ListaCodigosTiposCliente[f.rgTiposCliente.ItemIndex]);
            Seleccion						:= f.rgTiposCliente.Items[f.rgTiposCliente.ItemIndex];
            with spActualizarTipoClienteConvenio, Parameters do begin
                if Active then
                	Close;
                Parameters.Refresh;
                ParamByName('@CodigoConvenio').Value	:= cdsObtenerConveniosMorososCobInternaCodigoConvenio.AsInteger;
                ParamByName('@CodigoTipoCliente').Value	:= CodigoTipoDeCliente;
                ParamByName('@Usuario').Value			:= UsuarioSistema;
                try
                	ExecProc;
                except
					on E:Exception do begin
                        MsgBox(E.Message, Caption, MB_ICONERROR);
                        Exit;
                    end;
                end;
            end;
        end;
    finally
        f.Release;
    end;
end;

procedure TFormCarpetaDeudores.btnDefinirTipoClienteCobExtraJudicialClick(
  Sender: TObject);
resourcestring
	MSG_NO_HAY_REGISTROS 	= 'No hay registros para definir Tipo de Cliente';
    MSG_NO_HAY_SELECCION	= 'No ha seleccionado registros para Definir Tipo de Cliente';
    MSG_ACCION_UNITARIA		= 'Para definir tipo de cliente, s�lo debe seleccionar un convenio';
begin
	if (not cdsObtenerConveniosMorososCobExtraJudicial.Active) or (cdsObtenerConveniosMorososCobExtraJudicial.RecordCount=0) then begin
    	MsgBox(MSG_NO_HAY_REGISTROS, Caption, MB_ICONINFORMATION);
        Exit;
    end;
    
    if CantCobExtraJudicialMarcados = 0 then begin
    	MsgBox(MSG_NO_HAY_SELECCION, Caption, MB_ICONINFORMATION);
        Exit;
    end;

    if CantCobExtraJudicialMarcados > 1 then begin
    	MsgBox(MSG_ACCION_UNITARIA, Caption, MB_ICONINFORMATION);
        Exit;
    end;
end;

procedure TFormCarpetaDeudores.btnDefinirTipoClienteCobJudicialClick(
  Sender: TObject);
resourcestring
	MSG_NO_HAY_REGISTROS 	= 'No hay registros para definir Tipo de Cliente';
    MSG_NO_HAY_SELECCION	= 'No ha seleccionado registros para Definir Tipo de Cliente';
    MSG_ACCION_UNITARIA		= 'Para definir tipo de cliente, s�lo debe seleccionar un convenio';
begin
	if (not cdsObtenerConveniosMorososCobJudicial.Active) or (cdsObtenerConveniosMorososCobJudicial.RecordCount=0) then begin
    	MsgBox(MSG_NO_HAY_REGISTROS, Caption, MB_ICONINFORMATION);
        Exit;
    end;
    
    if CantCobJudicialMarcados = 0 then begin
    	MsgBox(MSG_NO_HAY_SELECCION, Caption, MB_ICONINFORMATION);
        Exit;
    end;

    if CantCobJudicialMarcados > 1 then begin
    	MsgBox(MSG_ACCION_UNITARIA, Caption, MB_ICONINFORMATION);
        Exit;
    end;
end;

function TFormCarpetaDeudores.TraeRegistrosCobJudicial;
resourcestring
   	MSG_ERROR_SELECT = 'Error Consultando Registros';
var
	Resultado     : Boolean;
	Retorno, i    : Integer;
	MensajeError  : string;
begin
  	try
    	spObtenerConveniosMorososCobJudicial.Open;
    	Retorno := spObtenerConveniosMorososCobJudicial.Parameters.ParamByName('@RETURN_VALUE').Value;

    	if Retorno <> 0 then begin
      		MensajeError := spObtenerConveniosMorososCobJudicial.Parameters.ParamByName('@ErrorDescription').Value;
      		Application.MessageBox(PChar(MensajeError),'Problema', MB_ICONERROR);
        end;

    	cdsObtenerConveniosMorososCobJudicial.DisableControls;

    	cdsObtenerConveniosMorososCobJudicial.Active   := False;

    	for i := 0 to cdsObtenerConveniosMorososCobJudicial.Fields.Count - 1 do
      		cdsObtenerConveniosMorososCobJudicial.Fields[i].ReadOnly := False;

    	try
      		cdsObtenerConveniosMorososCobJudicial.EmptyDataSet;
    	except
    	end;

    	cdsObtenerConveniosMorososCobJudicial.CreateDataSet;
    	cdsObtenerConveniosMorososCobJudicial.Active   := True;
    	cdsObtenerConveniosMorososCobJudicial.ReadOnly := False;

    	spObtenerConveniosMorososCobJudicial.First;
    	while not spObtenerConveniosMorososCobJudicial.eof do begin
      		cdsObtenerConveniosMorososCobJudicial.Append;

			cdsObtenerConveniosMorososCobJudicial.FieldByName('NumeroDocumento').Value	    := spObtenerConveniosMorososCobJudicial.FieldByName('NumeroDocumento').Value;
			cdsObtenerConveniosMorososCobJudicial.FieldByName('NombrePersona').Value	    := spObtenerConveniosMorososCobJudicial.FieldByName('NombrePersona').Value;
			cdsObtenerConveniosMorososCobJudicial.FieldByName('NumeroConvenio').Value	    := spObtenerConveniosMorososCobJudicial.FieldByName('NumeroConvenio').Value;
			cdsObtenerConveniosMorososCobJudicial.FieldByName('TipoCliente').Value	    	:= spObtenerConveniosMorososCobJudicial.FieldByName('TipoDeCliente').Value;
			cdsObtenerConveniosMorososCobJudicial.FieldByName('Candidato').Value			:= spObtenerConveniosMorososCobJudicial.FieldByName('Candidato').Value;
			cdsObtenerConveniosMorososCobJudicial.FieldByName('Saldo').Value				:= spObtenerConveniosMorososCobJudicial.FieldByName('Saldo').Value;
			cdsObtenerConveniosMorososCobJudicial.FieldByName('CantComprobantes').Value	    := spObtenerConveniosMorososCobJudicial.FieldByName('CantidadComprobantes').Value;
			cdsObtenerConveniosMorososCobJudicial.FieldByName('DiasDeuda').Value			:= spObtenerConveniosMorososCobJudicial.FieldByName('DiasDeuda').Value;
			cdsObtenerConveniosMorososCobJudicial.FieldByName('FechaVencimiento').Value	    := spObtenerConveniosMorososCobJudicial.FieldByName('FechaVencimiento').Value;
            cdsObtenerConveniosMorososCobJudicial.FieldByName('AccionCobranza').Value		:= spObtenerConveniosMorososCobJudicial.FieldByName('AccionCobranza').Value;
            cdsObtenerConveniosMorososCobJudicial.FieldByName('EstadoAccionCobranza').Value	:= spObtenerConveniosMorososCobJudicial.FieldByName('EstadoAccionCobranza').Value;
			cdsObtenerConveniosMorososCobJudicial.FieldByName('CodigoCliente').Value	    := spObtenerConveniosMorososCobJudicial.FieldByName('CodigoCliente').Value;
			cdsObtenerConveniosMorososCobJudicial.FieldByName('CodigoConvenio').Value	    := spObtenerConveniosMorososCobJudicial.FieldByName('CodigoConvenio').Value;

      		cdsObtenerConveniosMorososCobJudicial.Post;
      		spObtenerConveniosMorososCobJudicial.Next;
    	end;

    	for i := 1 to cdsObtenerConveniosMorososCobJudicial.Fields.Count - 1 do
      		cdsObtenerConveniosMorososCobJudicial.Fields[i].ReadOnly := True;

    	spObtenerConveniosMorososCobJudicial.Close;
    	cdsObtenerConveniosMorososCobJudicial.First;
    	cdsObtenerConveniosMorososCobJudicial.EnableControls;

    	Resultado := True;

  	except
    	on E : Exception do begin
      		MsgBoxErr(MSG_ERROR_SELECT, E.Message, Caption, MB_ICONERROR);
      		Resultado := False;
    	end;
  	end;

  Result := Resultado;
end;

procedure TFormCarpetaDeudores.btnEjecutarAccionDeudoresClick(Sender: TObject);
var
    Puntero: TBookmark;
    fCastigo: TformCobroComprobantesCastigo;
    fCobranzaExterna: TGestionarCobranzaExternaForm;
    TListaConvenios: TStringList;
    bExisteConvenioNoCandidato: Boolean;
    strConveniosCSV: string;
    i, FCodigoCliente, FCodigoConvenio, IDBloqueo, ResultadoBloqueo, 
    CodigoRefinanciacion, CodigoRefinanciacionUnificada: Integer;
begin
	if (not cdsObtenerConveniosMorososDeudores.Active) or (cdsObtenerConveniosMorososDeudores.RecordCount=0) then begin
    	MsgBox(MSG_NO_HAY_REGISTROS, Caption, MB_ICONINFORMATION);
        Exit;
    end;

    if cbbTipoAccionDeudores.ItemIndex < 0 then begin
        MsgBox(MSG_DEBE_SELECCIONAR_ACCION, Caption, MB_ICONSTOP);
        Exit;
    end;

    Puntero := cdsObtenerConveniosMorososDeudores.GetBookmark;
    if CantDeudoresMarcados = 0 then begin
    	MsgBox(MSG_NO_HAY_SELECCION, Caption, MB_ICONINFORMATION);
        Exit;
    end;

	try
    	Screen.Cursor 	:= crHourGlass;
        cdsObtenerConveniosMorososDeudores.DisableControls;

        if (cbbTipoAccionDeudores.Value = ACCION_COBRANZA_AVISO_EMAIL) 
        or (cbbTipoAccionDeudores.Value = ACCION_COBRANZA_AVISO_POSTAL) 
        or (cbbTipoAccionDeudores.Value = ACCION_COBRANZA_AVISO_CARTA_CERTIFICADA) then begin
            if CodigoPlantillaDeudores < 0 then begin
                MsgBox(MSG_DEBE_SELECCIONAR_PLANTILLA, Caption, MB_ICONERROR);
                Exit;
            end;
        
            cdsObtenerConveniosMorososDeudores.First;
            while (not cdsObtenerConveniosMorososDeudores.Eof) do begin
                if cdsObtenerConveniosMorososDeudoresMarcar.AsBoolean then begin

                    with spGenerarNotificacionesCobranza, Parameters do begin
                        if Active then
                            Close;
                        Parameters.Refresh;
                        ParamByName('@CodigoConvenio').Value	:= cdsObtenerConveniosMorososDeudoresCodigoConvenio.AsInteger;
                        ParamByName('@CodigoPlantilla').Value	:= CodigoPlantillaDeudores;
                        if (cbbTipoAccionDeudores.Value = ACCION_COBRANZA_AVISO_EMAIL) 
                        or (cbbTipoAccionDeudores.Value = ACCION_COBRANZA_AVISO_POSTAL) then
                            ParamByName('@CodigoAccionAviso').Value	:= COD_NOTIFICACION_COBRANZA_EMAIL_POSTAL
                        else 
                            ParamByName('@CodigoAccionAviso').Value	:= COD_NOTIFICACION_COBRANZA_AVISO_CARTA_CERTIFICADA;
                    end;

                    with spIngresarConvenioACobranzaInterna, Parameters do begin
                        Parameters.Refresh;
                        ParamByName('@CodigoConvenio').Value	:= cdsObtenerConveniosMorososDeudoresCodigoConvenio.AsInteger;
                        ParamByName('@Usuario').Value			:= UsuarioSistema;
                        try
                            ExecProc;
                        except
                            on E: Exception do begin
                                MsgBox(E.Message, Caption, MB_ICONERROR);
                                Exit;
                            end;
                        end;
                    end;

                    with spGuardarAccionConvenio, Parameters do begin
                        Parameters.Refresh;
                        ParamByName('@CodigoConvenio').Value				:= cdsObtenerConveniosMorososDeudoresCodigoConvenio.AsInteger;
                        ParamByName('@Accion').Value						:= vcbAccionDeCobranzaDeudores.Text;
                        ParamByName('@TipoAccion').Value					:= cbbTipoAccionDeudores.Text;
                        ParamByName('@EstadoAccion').Value					:= 'Enviado';
                        ParamByName('@CodigoPlantilla').Value				:= CodigoPlantillaDeudores;
                        ParamByName('@CodigoMensaje').Value					:= null;
                        ParamByName('@CodigoEmpresaCobranza').Value			:= null;
                        ParamByName('@CodigoRefinanciacion').Value			:= null;
                        ParamByName('@CodigoAvenimiento').Value				:= null;
                        ParamByName('@FechaInicioAccion').Value				:= Date;
                        ParamByName('@FechaFinAccion').Value				:= null;
                        ParamByName('@Usuario').Value						:= UsuarioSistema;
                        try
        					ExecProc;
                        except
                            on E:Exception do begin
                                MsgBox(E.Message, Caption, MB_ICONERROR);
                                Exit;
                            end;
                        end;
                    end;
                end;

                cdsObtenerConveniosMorososDeudores.Next;
            end;
    		btnBuscarDeudoresClick(nil);
    	end
        
        else if cbbTipoAccionDeudores.Value = ACCION_COBRANZA_AVISO_TELEFONICO then begin
            cdsObtenerConveniosMorososDeudores.First;
            //Debo crear s�lo una campa�a con los par�metros seleccionados
            while (not cdsObtenerConveniosMorososDeudores.Eof) do begin
                if cdsObtenerConveniosMorososDeudoresMarcar.AsBoolean then
                    with spGuardarCampannas, Parameters do begin
                        if Active then
                            Close;
                        Parameters.Refresh;
                    
                        ParamByName('@CodigoTipoCampanna').Value			:= COD_TIPO_CAMPANNA_INTERNA;
                        ParamByName('@CodigoMotivoCampanna').Value	        := COD_MOTIVO_CAMPANNA_MOROSOS;
                        ParamByName('@DescripcionCampanna').Value	        := cbbTipoAccionDeudores.Text;

                        ParamByName('@Recursiva').Value						:= False;
                        ParamByName('@DiaEnvio').Value						:= Null;

                        ParamByName('@MedioContacto').Value					:= 'Tel�fono';
                        {ParamByName('@FechaInicioVigencia').Value  			:= Trunc(txtInicioVigencia.Date);
                        ParamByName('@FechaTerminoVigencia').Value 			:= iif(txtTerminoVigencia.Visible, Trunc(txtTerminoVigencia.Date), null);
                        ParamByName('@Prioridad').Value						:= cbbPrioridad.Text;
                        ParamByName('@CodigoAreaAtencionDeCaso').Value		:= cbbAreaAsignada.Value;}
                        ParamByName('@ArchivoGenerado').Value				:= null;
                        ParamByName('@MedioDePago').Value					:= null;
                        ParamByName('@RechazosConsecutivos').Value			:= null;
                        
                        ParamByName('@TiempoMorosidadFactura').Value		:= edEdadDeudaDeudores.Value;
                        ParamByName('@ImporteFactura').Value				:= edMontoMinimoDeudaDeudores.Text;
                        ParamByName('@ImporteTotalFacturasVencidas').Value	:= Null;
                        ParamByName('@CantidadFacturasVencidas').Value		:= Null;

                        ParamByName('@DiasInfraccion').Value	            := null;
                        ParamByName('@ImporteInfraccion').Value	            := null;
                        ParamByName('@CodigoTipoDefectoTAG').Value		    := null;
                        ParamByName('@CodigoConcesionaria').Value		    := null;
                        ParamByName('@CodigoComuna').Value	                := null;
                        ParamByName('@CantidadTAGs').Value	                := null;
                        ParamByName('@ValorFactura').Value	                := null;
                        ParamByName('@UsuarioCreacion').Value				:= UsuarioSistema;
                        try
                        	ExecProc;
                        except
                        	on E:Exception do begin
                            	MsgBox(E.Message, Caption, MB_ICONERROR);
                                Exit;
                            end;
                        end;
                    end;

                    with spIngresarConvenioACobranzaInterna, Parameters do begin
                        Parameters.Refresh;
                        ParamByName('@CodigoConvenio').Value	:= cdsObtenerConveniosMorososDeudoresCodigoConvenio.AsInteger;
                        ParamByName('@Usuario').Value			:= UsuarioSistema;
                        try
                            ExecProc;
                        except
                            on E: Exception do begin
                                MsgBox(E.Message, Caption, MB_ICONERROR);
                                Exit;
                            end;
                        end;
                    end;

                    with spGuardarAccionConvenio, Parameters do begin
                        Parameters.Refresh;
                        ParamByName('@CodigoConvenio').Value				:= cdsObtenerConveniosMorososDeudoresCodigoConvenio.AsInteger;
                        ParamByName('@Accion').Value						:= vcbAccionDeCobranzaDeudores.Text;
                        ParamByName('@TipoAccion').Value					:= cbbTipoAccionDeudores.Text;
                        ParamByName('@EstadoAccion').Value					:= 'Enviado';
                        ParamByName('@CodigoPlantilla').Value				:= CodigoPlantillaDeudores;
                        ParamByName('@CodigoMensaje').Value					:= null;
                        ParamByName('@CodigoEmpresaCobranza').Value			:= null;
                        ParamByName('@CodigoRefinanciacion').Value			:= null;
                        ParamByName('@CodigoAvenimiento').Value				:= null;
                        ParamByName('@FechaInicioAccion').Value				:= Date;
                        ParamByName('@FechaFinAccion').Value				:= null;
                        ParamByName('@Usuario').Value						:= UsuarioSistema;
                        try
        					ExecProc;
                        except
                            on E:Exception do begin
                                MsgBox(E.Message, Caption, MB_ICONERROR);
                                Exit;
                            end;
                        end;
                    end;
                cdsObtenerConveniosMorososDeudores.Next;
            end;
            btnBuscarDeudoresClick(nil);
        end

        else if cbbTipoAccionDeudores.Value = ACCION_COBRANZA_TRASPASO_COB_INTERNA then begin
            cdsObtenerConveniosMorososDeudores.DisableControls;
			cdsObtenerConveniosMorososDeudores.First;
            while not cdsObtenerConveniosMorososDeudores.Eof do begin
            	if cdsObtenerConveniosMorososDeudoresMarcar.AsBoolean then begin
                    with spIngresarConvenioACobranzaInterna, Parameters do begin
                        Parameters.Refresh;
                        ParamByName('@CodigoConvenio').Value	:= cdsObtenerConveniosMorososDeudoresCodigoConvenio.AsInteger;
                        ParamByName('@Usuario').Value			:= UsuarioSistema;
                        try
                            ExecProc;
                        except
                            on E: Exception do begin
                                MsgBox(E.Message, Caption, MB_ICONERROR);
                                Exit;
                            end;
                        end;
                    end;
                end;
                cdsObtenerConveniosMorososDeudores.Next;
            end;
            cdsObtenerConveniosMorososDeudores.EnableControls;

        	MsgBox('Convenios Traspasados a Cobranza Interna', Caption, MB_ICONINFORMATION);
            btnBuscarDeudoresClick(nil);
        end

        else if cbbTipoAccionDeudores.Value = ACCION_COBRANZA_CASTIGO then begin

            if CantDeudoresMarcados > 1 then begin
                MsgBox(Format(MSG_ACCION_UNITARIA, ['Castigo']), Caption, MB_ICONINFORMATION);
                Exit;
            end;

        	try
                cdsObtenerConveniosMorososDeudores.DisableControls;
                cdsObtenerConveniosMorososDeudores.First;
                while not cdsObtenerConveniosMorososDeudores.Eof do begin
                    if cdsObtenerConveniosMorososDeudoresMarcar.AsBoolean then begin
                        fCastigo := TformCobroComprobantesCastigo.Create(self);
                        fCastigo.Inicializar(cdsObtenerConveniosMorososDeudoresNumeroDocumento.AsString, 
                                             cdsObtenerConveniosMorososDeudoresCodigoConvenio.AsInteger, Main.MainForm.NumeroPOS, Main.MainForm.PuntoEntrega, Main.MainForm.PuntoVenta);
                        try
                            fCastigo.ShowModal;
                        except
                            fCastigo.Show;
                        end;

                        Break;
                    end;
                    cdsObtenerConveniosMorososDeudores.Next;
                end;
            finally
            	if Assigned(fCastigo) then
                	fCastigo.Release;
                cdsObtenerConveniosMorososDeudores.EnableControls;
                end;

			btnBuscarDeudoresClick(nil);
        end

        else if cbbTipoAccionDeudores.Value = ACCION_COBRANZA_INHABILITACION_TAGS then begin
            if CodigoPlantillaDeudores < 0 then begin
                MsgBox(MSG_DEBE_SELECCIONAR_PLANTILLA, Caption, MB_ICONERROR);
                Exit;
            end;

    		bExisteConvenioNoCandidato	:= False;
        	TListaConvenios 			:= TStringList.Create;
            cdsObtenerConveniosMorososDeudores.DisableControls;
			cdsObtenerConveniosMorososDeudores.First;
            while not cdsObtenerConveniosMorososDeudores.Eof do begin
            	if cdsObtenerConveniosMorososDeudoresMarcar.AsBoolean then begin
                	if EsConvenioCandidatoAInhabilitar(cdsObtenerConveniosMorososDeudoresCodigoConvenio.AsInteger) then
                		TListaConvenios.Add(cdsObtenerConveniosMorososDeudoresCodigoConvenio.AsString)
                    else
                    	bExisteConvenioNoCandidato := True;
                end;
                cdsObtenerConveniosMorososDeudores.Next;
            end;
            cdsObtenerConveniosMorososDeudores.EnableControls;

            if (bExisteConvenioNoCandidato) and (TListaConvenios.Count>0) then
                MsgBox(MSG_EXISTE_CONVENIO_NO_CANDIDATO, Caption, MB_ICONINFORMATION)
            else if TListaConvenios.Count = 0 then begin
            	MsgBox(MSG_NO_EXISTE_CONVENIO_CANDIDATO, Caption, MB_ICONSTOP);
                Exit;
            	end;

            FormInhabilitacionTAGs := TFormInhabilitacionTAGs.Create(Self);
            with FormInhabilitacionTAGs do begin
            	if Inicializar(TListaConvenios, CodigoPlantillaDeudores, vcbAccionDeCobranzaDeudores.Text, True) then begin
                    if FormInhabilitacionTAGs.ShowModal = mrOk then begin
                    	try
                            Screen.Cursor := crHourGlass;
                            Application.ProcessMessages;
                        
                        	if cdsObtenerConveniosMorosos.Active then begin
                            	cdsObtenerConveniosMorosos.Close;
                                cdsObtenerConveniosMorosos.Open
                            end;
                        finally
							Screen.Cursor := crDefault;
                        end;
                    end;

                end;
            end;
            TListaConvenios.Free;
            btnBuscarDeudoresClick(nil);
        end

        else if (cbbTipoAccionDeudores.Value = ACCION_COBRANZA_ENVIO_COBRANZA_EXTRAJUDICIAL)
        or (cbbTipoAccionDeudores.Value = ACCION_COBRANZA_ENVIO_COBRANZA_JUDICIAL) 
        or (cbbTipoAccionDeudores.Value = ACCION_TRANSFERENCIA_ENTRE_EMPRESAS) then begin

        	TListaConvenios	:= TStringList.Create;
            cdsObtenerConveniosMorososDeudores.First;
            while (not cdsObtenerConveniosMorososDeudores.Eof) do begin
                if cdsObtenerConveniosMorososDeudoresMarcar.AsBoolean then
                	TListaConvenios.Add(IntToStr(cdsObtenerConveniosMorososDeudoresCodigoConvenio.AsInteger));
                cdsObtenerConveniosMorososDeudores.Next;
            end;
            
            strConveniosCSV	:= '';
            for i := 0 to TListaConvenios.Count - 1 do
                strConveniosCSV	:= strConveniosCSV + TListaConvenios[i] + ',';
            strConveniosCSV := Copy(strConveniosCSV, 1, Length(strConveniosCSV)-1);

            fCobranzaExterna := TGestionarCobranzaExternaForm.Create(self);
            fCobranzaExterna.Inicializar(cbbTipoAccionDeudores.Value, strConveniosCSV);
            if fCobranzaExterna.ShowModal = mrOk then begin
                //si se procesaron registros entonces hago refresh
                btnBuscarDeudores.OnClick(btnBuscarDeudores);
            end;
        end

    	else if cbbTipoAccionDeudores.Value = ACCION_COBRANZA_REFINANCIACION then begin
        
            if CantDeudoresMarcados > 1 then begin
                MsgBox(Format(MSG_ACCION_UNITARIA, ['Refinanciaci�n']), Caption, MB_ICONINFORMATION);
                Exit;
            end;

            FCodigoCliente	:= cdsObtenerConveniosMorososDeudoresCodigoCliente.AsInteger;
            FCodigoConvenio	:= cdsObtenerConveniosMorososDeudoresCodigoConvenio.AsInteger;

            CodigoRefinanciacion			:= 0;
            CodigoRefinanciacionUnificada	:= 0;
            //IDBloqueo 						:= 0;
			if fExisteRefinanciacion(FCodigoCliente, FCodigoConvenio, CodigoRefinanciacion, CodigoRefinanciacionUnificada) then begin
{            	if CodigoRefinanciacion <> 0 then begin
            		TPanelMensajesForm.MuestraMensaje('Obteniendo Bloqueo ...', True);
                    ResultadoBloqueo := ObtenerBloqueo(DMConnections.BaseCAC,
                                                       TRefinanciacionGestionForm.ClassName,
                                                       cdsRefinanciacionesCodigoRefinanciacion.AsString,
                                                       UsuarioSistema, IDBloqueo);
                end;
}            end;
            
            RefinanciacionGestionForm := TRefinanciacionGestionForm.Create(Self);

            with spObtenerRefinanciacionesConsultas, Parameters do begin
                Parameters.Refresh;
                ParamByName('@CodigoRefinanciacion').Value := Null;
                ParamByName('@CodigoPersona').Value        := cdsObtenerConveniosMorososDeudoresCodigoCliente.AsInteger;
                ParamByName('@RUTPersona').Value           := cdsObtenerConveniosMorososDeudoresNumeroDocumento.AsString;
                ParamByName('@CodigoConvenio').Value       := cdsObtenerConveniosMorososDeudoresCodigoConvenio.AsInteger;
                ParamByName('@CodigoTipoMedioPago').Value  := Null;
                ParamByName('@SoloNoActivadas').Value      := 0;
            end;

            cdsRefinanciaciones.Open;
            with RefinanciacionGestionForm do begin
                if Inicializar(CodigoRefinanciacion, CodigoRefinanciacionUnificada, 
                			   vcbTipoRefinanciacion.Value, FCodigoCliente, FCodigoConvenio, (ResultadoBloqueo <> 0)) then begin
                    if ShowModal = mrOk then begin
                        try
                            Screen.Cursor := crHourGlass;
                            Application.ProcessMessages;

                            if cdsRefinanciaciones.Active then begin
                                cdsRefinanciaciones.Close;
                                cdsRefinanciaciones.Open;
                            end
                            else begin
                                peRut.Text := EmptyStr;
                                vcbConvenios.Clear;
                                vcbTipoRefinanciacion.ItemIndex := vcbTipoRefinanciacion.Items.IndexOfValue(0);
                                lblEnListaAmarilla.Visible  := False;
                                lblEnListaAmarilla.Caption  := EmptyStr;
                            end;

                        finally
                            Screen.Cursor := crDefault;
                        end;
                    end;
                end;
                try
                    if IDBloqueo > 0 then begin
                        TPanelMensajesForm.MuestraMensaje('Desbloqueando Refinanciaci�n ...', (ModalResult <> mrOk));
                        EliminarBloqueo(DMConnections.BaseCAC, IDBloqueo);
                    end;
                except
                    on e: Exception do begin
                        MsgBox(e.Message, Caption, MB_ICONERROR);
                    end;
                end;
            end;
            btnBuscarDeudoresClick(nil);
        end;

    finally
        if cdsObtenerConveniosMorososDeudores.BookmarkValid(Puntero) then begin
            cdsObtenerConveniosMorososDeudores.GotoBookmark(Puntero);
        end;
        cdsObtenerConveniosMorososDeudores.FreeBookmark(Puntero);

    	cdsObtenerConveniosMorososDeudores.EnableControls;
		Screen.Cursor := crDefault;
    end;
	
end;

procedure TFormCarpetaDeudores.btnEjecutarAccionCobInternaClick(
  Sender: TObject);
var
    Puntero: TBookmark;
    fCastigo: TformCobroComprobantesCastigo;
    fCobranzaExterna: TGestionarCobranzaExternaForm;
    TListaConvenios: TStringList;
    bExisteConvenioNoCandidato: Boolean;
    i, FCodigoCliente, FCodigoConvenio: Integer;
    strConveniosCSV: String;
begin
	if (not cdsObtenerConveniosMorososCobInterna.Active) or (cdsObtenerConveniosMorososCobInterna.RecordCount=0) then begin
    	MsgBox(MSG_NO_HAY_REGISTROS, Caption, MB_ICONINFORMATION);
        Exit;
    end;

    if cbbTipoAccionCobranzaInterna.ItemIndex < 0 then begin
        MsgBox(MSG_DEBE_SELECCIONAR_ACCION, Caption, MB_ICONSTOP);
        Exit;
    end;

    if CantCobInternaMarcados = 0 then begin
    	MsgBox(MSG_NO_HAY_SELECCION, Caption, MB_ICONINFORMATION);
        Exit;
    end;

    Puntero := cdsObtenerConveniosMorososCobInterna.GetBookmark;
	try
    	Screen.Cursor 	:= crHourGlass;
        cdsObtenerConveniosMorososCobInterna.DisableControls;

        if (cbbTipoAccionCobranzaInterna.Value = ACCION_COBRANZA_AVISO_EMAIL) 
        or (cbbTipoAccionCobranzaInterna.Value = ACCION_COBRANZA_AVISO_POSTAL) 
        or (cbbTipoAccionCobranzaInterna.Value = ACCION_COBRANZA_AVISO_CARTA_CERTIFICADA) then begin
            if CodigoPlantillaCobInterna < 0 then begin
                MsgBox(MSG_DEBE_SELECCIONAR_PLANTILLA, Caption, MB_ICONERROR);
                Exit;
            end;
        
            cdsObtenerConveniosMorososCobInterna.First;
            while (not cdsObtenerConveniosMorososCobInterna.Eof) do begin
                if cdsObtenerConveniosMorososCobInternaMarcar.AsBoolean then
                    with spGenerarNotificacionesCobranza, Parameters do begin
                        if Active then
                            Close;
                        Parameters.Refresh;
                        ParamByName('@CodigoConvenio').Value	:= cdsObtenerConveniosMorososCobInternaCodigoConvenio.AsInteger;
                        ParamByName('@CodigoPlantilla').Value	:= CodigoPlantillaCobInterna;
                        if (cbbTipoAccionCobranzaInterna.Value = ACCION_COBRANZA_AVISO_EMAIL) 
                        or (cbbTipoAccionCobranzaInterna.Value = ACCION_COBRANZA_AVISO_POSTAL) then
                            ParamByName('@CodigoAccionAviso').Value	:= COD_NOTIFICACION_COBRANZA_EMAIL_POSTAL
                        else 
                            ParamByName('@CodigoAccionAviso').Value	:= COD_NOTIFICACION_COBRANZA_AVISO_CARTA_CERTIFICADA;
                    end;
                cdsObtenerConveniosMorososCobInterna.Next;
            end;

        end

        else if cbbTipoAccionCobranzaInterna.Value = ACCION_COBRANZA_AVISO_TELEFONICO then begin
            cdsObtenerConveniosMorososCobInterna.First;
            //Debo crear s�lo una campa�a con los par�metros seleccionados
            while (not cdsObtenerConveniosMorososCobInterna.Eof) do begin
                if cdsObtenerConveniosMorososCobInternaMarcar.AsBoolean then
                    with spGuardarCampannas, Parameters do begin
                        if Active then
                            Close;
                        Parameters.Refresh;
                    
                        ParamByName('@CodigoTipoCampanna').Value			:= COD_TIPO_CAMPANNA_INTERNA;
                        ParamByName('@CodigoMotivoCampanna').Value	        := COD_MOTIVO_CAMPANNA_MOROSOS;
                        ParamByName('@DescripcionCampanna').Value	        := cbbTipoAccionCobranzaInterna.Text;

                        ParamByName('@Recursiva').Value						:= False;
                        ParamByName('@DiaEnvio').Value						:= Null;

                        ParamByName('@MedioContacto').Value					:= 'Tel�fono';
                        {ParamByName('@FechaInicioVigencia').Value  			:= Trunc(txtInicioVigencia.Date);
                        ParamByName('@FechaTerminoVigencia').Value 			:= iif(txtTerminoVigencia.Visible, Trunc(txtTerminoVigencia.Date), null);
                        ParamByName('@Prioridad').Value						:= cbbPrioridad.Text;
                        ParamByName('@CodigoAreaAtencionDeCaso').Value		:= cbbAreaAsignada.Value;}
                        ParamByName('@ArchivoGenerado').Value				:= null;
                        ParamByName('@MedioDePago').Value					:= null;
                        ParamByName('@RechazosConsecutivos').Value			:= null;
                        
                        ParamByName('@TiempoMorosidadFactura').Value		:= edEdadDeudaCobInterna.Text;
                        ParamByName('@ImporteFactura').Value				:= edMontoMinimoDeudaCobInterna.Text;
                        ParamByName('@ImporteTotalFacturasVencidas').Value	:= Null;
                        ParamByName('@CantidadFacturasVencidas').Value		:= Null;

                        ParamByName('@DiasInfraccion').Value	            := null;
                        ParamByName('@ImporteInfraccion').Value	            := null;
                        ParamByName('@CodigoTipoDefectoTAG').Value		    := null;
                        ParamByName('@CodigoConcesionaria').Value		    := null;
                        ParamByName('@CodigoComuna').Value	                := null;
                        ParamByName('@CantidadTAGs').Value	                := null;
                        ParamByName('@ValorFactura').Value	                := null;
                        ParamByName('@UsuarioCreacion').Value				:= UsuarioSistema;
                        try
                        	ExecProc;
                        except
                        	on E:Exception do begin
                            	MsgBox(E.Message, Caption, MB_ICONERROR);
                                Exit;
                            end;
                        end;
                    end;
                cdsObtenerConveniosMorososCobInterna.Next;
            end;
        end

        else if (cbbTipoAccionCobranzaInterna.Value = ACCION_COBRANZA_AVISO_NOTACOBRO) then begin
            if CodigoMensaje < 0 then begin
                MsgBox(MSG_DEBE_SELECCIONAR_PLANTILLA, Caption, MB_ICONERROR);
                Exit;
            end;

            cdsObtenerConveniosMorososCobInterna.First;
            while (not cdsObtenerConveniosMorososCobInterna.Eof) do begin
                if cdsObtenerConveniosMorososCobInternaMarcar.AsBoolean then
                    with spGenerarNotificacionesCobranza, Parameters do begin
                        if Active then
                            Close;
                        Parameters.Refresh;
                        ParamByName('@CodigoConvenio').Value	:= cdsObtenerConveniosMorososCobInternaCodigoConvenio.AsInteger;
                        ParamByName('@CodigoPlantilla').Value	:= CodigoPlantillaCobInterna;
                        ParamByName('@CodigoAccionAviso').Value	:= COD_NOTIFICACION_COBRANZA_AVISO_NOTA_COBRO;
                    end;
                cdsObtenerConveniosMorososCobInterna.Next;
            end;
        end
    
        else if (cbbTipoAccionCobranzaInterna.Value = ACCION_COBRANZA_ENVIO_COBRANZA_EXTRAJUDICIAL)
        or (cbbTipoAccionCobranzaInterna.Value = ACCION_COBRANZA_ENVIO_COBRANZA_JUDICIAL) 
        or (cbbTipoAccionCobranzaInterna.Value = ACCION_TRANSFERENCIA_ENTRE_EMPRESAS) then begin

        	TListaConvenios	:= TStringList.Create;
            cdsObtenerConveniosMorososCobInterna.First;
            while (not cdsObtenerConveniosMorososCobInterna.Eof) do begin
                if cdsObtenerConveniosMorososCobInternaMarcar.AsBoolean then
                	TListaConvenios.Add(IntToStr(cdsObtenerConveniosMorososCobInternaCodigoConvenio.AsInteger));
                cdsObtenerConveniosMorososCobInterna.Next;
            end;
            
            strConveniosCSV	:= '';
            for i := 0 to TListaConvenios.Count - 1 do
                strConveniosCSV	:= strConveniosCSV + TListaConvenios[i] + ',';
            strConveniosCSV := Copy(strConveniosCSV, 1, Length(strConveniosCSV)-1);

            fCobranzaExterna := TGestionarCobranzaExternaForm.Create(self);
            fCobranzaExterna.Inicializar(cbbTipoAccionCobranzaInterna.Value, strConveniosCSV);
            if fCobranzaExterna.ShowModal = mrOk then begin
                //si se procesaron registros entonces hago refresh
                
                btnBuscarCobInterna.OnClick(btnBuscarCobInterna);
            end;
        end

        else if cbbTipoAccionCobranzaInterna.Value = ACCION_COBRANZA_INHABILITACION_TAGS then begin
            if CodigoPlantillaCobInterna < 0 then begin
                MsgBox(MSG_DEBE_SELECCIONAR_PLANTILLA, Caption, MB_ICONERROR);
                Exit;
            end;

    		bExisteConvenioNoCandidato	:= False;
        	TListaConvenios 			:= TStringList.Create;
			cdsObtenerConveniosMorososCobInterna.First;
            while not cdsObtenerConveniosMorososCobInterna.Eof do begin
            	if cdsObtenerConveniosMorososCobInternaMarcar.AsBoolean then begin
                	if EsConvenioCandidatoAInhabilitar(cdsObtenerConveniosMorososCobInternaCodigoConvenio.AsInteger) then
                		TListaConvenios.Add(cdsObtenerConveniosMorososCobInternaCodigoConvenio.AsString)
                    else
                    	bExisteConvenioNoCandidato := True;
                end;
                cdsObtenerConveniosMorososCobInterna.Next;
            end;

            if (bExisteConvenioNoCandidato) and (TListaConvenios.Count>0) then
                MsgBox(MSG_EXISTE_CONVENIO_NO_CANDIDATO, Caption, MB_ICONINFORMATION)
            else if TListaConvenios.Count = 0 then begin
            	MsgBox(MSG_NO_EXISTE_CONVENIO_CANDIDATO, Caption, MB_ICONSTOP);
                Exit;
            	end;

            FormInhabilitacionTAGs := TFormInhabilitacionTAGs.Create(Self);
            with FormInhabilitacionTAGs do begin
            	if Inicializar(TListaConvenios, CodigoPlantillaCobInterna, vcbAccionDeCobranzaInterna.Text, False) then begin
                    if ShowModal = mrOk then begin
                    	try
                            Screen.Cursor := crHourGlass;
                            Application.ProcessMessages;
                        
                        	if cdsObtenerConveniosMorosos.Active then begin
                            	cdsObtenerConveniosMorosos.Close;
                                cdsObtenerConveniosMorosos.Open
                            end;
                        finally
							Screen.Cursor := crDefault;
                        end;
                    end;

                end;
            end;
            TListaConvenios.Free;
        end

        else if cbbTipoAccionCobranzaInterna.Value = ACCION_COBRANZA_CASTIGO then begin

            if CantCobInternaMarcados > 1 then begin
                MsgBox(Format(MSG_ACCION_UNITARIA, ['Castigo']), Caption, MB_ICONINFORMATION);
                Exit;
            end;

            fCastigo := TformCobroComprobantesCastigo.Create(self);
            fCastigo.Inicializar(cdsObtenerConveniosMorososCobInternaNumeroDocumento.AsString, 
                                 cdsObtenerConveniosMorososCobInternaCodigoConvenio.AsInteger, Main.MainForm.NumeroPOS, Main.MainForm.PuntoEntrega, Main.MainForm.PuntoVenta);
            try
                fCastigo.ShowModal;
                if ModalResult = mrOk then begin
                    with spEliminarConvenioDeCobranzaInterna, Parameters do begin
                        Parameters.Refresh;
                        ParamByName('@CodigoConvenio').Value	:= cdsObtenerConveniosMorososCobInternaCodigoConvenio.AsInteger;
                        try
                            ExecProc;
                        except
                            on E:Exception do begin
                                MsgBox(E.Message, Caption, MB_ICONERROR);
                                Exit;
                            end;
                        end;
                    end;
                    btnBuscarCobInternaClick(nil);
                end;
            except
                fCastigo.Show;
            end;
        end

    	else if cbbTipoAccionCobranzaInterna.Value = ACCION_COBRANZA_REFINANCIACION then begin
        
            if CantCobInternaMarcados > 1 then begin
                MsgBox(Format(MSG_ACCION_UNITARIA, ['Refinanciaci�n']), Caption, MB_ICONINFORMATION);
                Exit;
            end;

            FCodigoCliente	:= cdsObtenerConveniosMorososCobInternaCodigoCliente.AsInteger;
            FCodigoConvenio	:= cdsObtenerConveniosMorososCobInternaCodigoConvenio.AsInteger;

            RefinanciacionGestionForm := TRefinanciacionGestionForm.Create(Self);

            with spObtenerRefinanciacionesConsultas, Parameters do begin
                Parameters.Refresh;
                ParamByName('@CodigoRefinanciacion').Value := Null;
                ParamByName('@CodigoPersona').Value        := cdsObtenerConveniosMorososCobInternaCodigoCliente.AsInteger;
                ParamByName('@RUTPersona').Value           := cdsObtenerConveniosMorososCobInternaNumeroDocumento.AsString;
                ParamByName('@CodigoConvenio').Value       := cdsObtenerConveniosMorososCobInternaCodigoConvenio.AsInteger;
                ParamByName('@CodigoTipoMedioPago').Value  := Null;
                ParamByName('@SoloNoActivadas').Value      := 0;
            end;

            cdsRefinanciaciones.Open;
            with RefinanciacionGestionForm do begin
                if Inicializar(0, 0, vcbTipoRefinanciacion.Value, FCodigoCliente, FCodigoConvenio) then begin
                    if ShowModal = mrOk then begin
                        try
                            Screen.Cursor := crHourGlass;
                            Application.ProcessMessages;

                            if cdsRefinanciaciones.Active then begin
                                cdsRefinanciaciones.Close;
                                cdsRefinanciaciones.Open;
                            end
                            else begin
                                peRut.Text := EmptyStr;
                                vcbConvenios.Clear;
                                vcbTipoRefinanciacion.ItemIndex := vcbTipoRefinanciacion.Items.IndexOfValue(0);
                                lblEnListaAmarilla.Visible  := False;
                                lblEnListaAmarilla.Caption  := EmptyStr;
                            end;

                            with spEliminarConvenioDeCobranzaInterna, Parameters do begin
                                Parameters.Refresh;
                                ParamByName('@CodigoConvenio').Value	:= cdsObtenerConveniosMorososCobInternaCodigoConvenio.AsInteger;
                                try
                                    ExecProc;
                                except
                                    on E:Exception do begin
                                        MsgBox(E.Message, Caption, MB_ICONERROR);
                                        Exit;
                                    end;
                                end;
                            end;
                        finally
                            Screen.Cursor := crDefault;
                        end;
                    end;
                end;
            end;
        end;

    finally
        if cdsObtenerConveniosMorososCobInterna.BookmarkValid(Puntero) then begin
            cdsObtenerConveniosMorososCobInterna.GotoBookmark(Puntero);
        end;
        cdsObtenerConveniosMorososCobInterna.FreeBookmark(Puntero);

    	cdsObtenerConveniosMorososCobInterna.EnableControls;
		Screen.Cursor := crDefault;
    end;
    
end;

function TFormCarpetaDeudores.EsConvenioCandidatoAInhabilitar(CodigoConvenio: Integer): Boolean;
begin
	Result := False;
    try
    	with spVerificarConvenioCandidato, Parameters do begin
            if Active then
            	Close;
            Parameters.Refresh;
            ParamByName('@CodigoConvenio').Value		    := CodigoConvenio;
            Open;
            Result	:= not Eof;
            Close;
        end;
    except
		on E:Exception do begin
        	MsgBox('Error al verificar Convenio: ' + E.Message, Caption, MB_ICONERROR);
            Exit;
        end;
    end;
end;

function TFormCarpetaDeudores.fExisteRefinanciacion(CodigoCliente, CodigoConvenio: Integer; 
	var CodigoRefinanciacion, CodigoRefinanciacionUnificada: Integer):Boolean;
begin
	CodigoRefinanciacion			:= QueryGetIntegerValue(DMConnections.BaseCAC, 'SELECT CodigoRefinanciacion FROM Refinanciaciones NOLOCK WHERE CodigoPersona = ' + IntToStr(CodigoCliente) + ' AND CodigoConvenio = ' + IntToStr(CodigoConvenio) + ' AND CodigoEstado IN (' + IntToStr(REFINANCIACION_ESTADO_EN_TRAMITE) + ' , ' + IntToStr(REFINANCIACION_ESTADO_VIGENTE) + ')');
	CodigoRefinanciacionUnificada	:= QueryGetIntegerValue(DMConnections.BaseCAC, 'SELECT CodigoRefinanciacionUnificada FROM Refinanciaciones NOLOCK WHERE CodigoPersona = ' + IntToStr(CodigoCliente) + ' AND CodigoConvenio = ' + IntToStr(CodigoConvenio) + ' AND CodigoEstado IN (' + IntToStr(REFINANCIACION_ESTADO_EN_TRAMITE) + ' , ' + IntToStr(REFINANCIACION_ESTADO_VIGENTE) + ')');
    Result := (CodigoRefinanciacion <> 0);
end;

procedure TFormCarpetaDeudores.btnEjecutarAccionCobExtraJudicialClick(
  Sender: TObject);
var
    fCastigo: TformCobroComprobantesCastigo;
    fCobranzaExterna: TGestionarCobranzaExternaForm;
    TListaConvenios: TStringList;
    bExisteConvenioNoCandidato: Boolean;
    strConveniosCSV: String;
    i, FCodigoCliente, FCodigoConvenio: Integer;
begin
	if (not cdsObtenerConveniosMorososCobExtraJudicial.Active) or (cdsObtenerConveniosMorososCobExtraJudicial.RecordCount=0) then begin
    	MsgBox(MSG_NO_HAY_REGISTROS, Caption, MB_ICONINFORMATION);
        Exit;
    end;

    if cbbTipoAccionCobranzaExtraJudicial.ItemIndex < 0 then begin
        MsgBox(MSG_DEBE_SELECCIONAR_ACCION, Caption, MB_ICONSTOP);
        Exit;
    end;

    if CantCobExtraJudicialMarcados = 0 then begin
    	MsgBox(MSG_NO_HAY_SELECCION, Caption, MB_ICONINFORMATION);
        Exit;
    end;
    
	try
    	Screen.Cursor 	:= crHourGlass;
        cdsObtenerConveniosMorososCobExtraJudicial.DisableControls;

        if (cbbTipoAccionCobranzaExtraJudicial.Value = ACCION_COBRANZA_ENVIO_COBRANZA_JUDICIAL) 
        or (cbbTipoAccionCobranzaExtraJudicial.Value = ACCION_TRANSFERENCIA_ENTRE_EMPRESAS) then begin

        	TListaConvenios	:= TStringList.Create;
            cdsObtenerConveniosMorososCobExtraJudicial.First;
            while (not cdsObtenerConveniosMorososCobExtraJudicial.Eof) do begin
                if cdsObtenerConveniosMorososCobExtraJudicialMarcar.AsBoolean then
                	TListaConvenios.Add(IntToStr(cdsObtenerConveniosMorososCobExtraJudicialCodigoConvenio.AsInteger));
                cdsObtenerConveniosMorososCobExtraJudicial.Next;
            end;
            
            strConveniosCSV	:= '';
            for i := 0 to TListaConvenios.Count - 1 do
                strConveniosCSV	:= strConveniosCSV + TListaConvenios[i] + ',';
            strConveniosCSV := Copy(strConveniosCSV, 1, Length(strConveniosCSV)-1);

            fCobranzaExterna := TGestionarCobranzaExternaForm.Create(self);
            fCobranzaExterna.Inicializar(cbbTipoAccionCobranzaExtraJudicial.Value, strConveniosCSV);
            if fCobranzaExterna.ShowModal = mrOk then begin
                //si se procesaron registros entonces hago refresh
                btnBuscarCobExtraJudicial.OnClick(btnBuscarCobExtraJudicial);
            end;
        end
    
        else if cbbTipoAccionCobranzaExtraJudicial.Value = ACCION_COBRANZA_INHABILITACION_TAGS then begin
            if CodigoPlantillaCobExtraJudicial < 0 then begin
                MsgBox(MSG_DEBE_SELECCIONAR_PLANTILLA, Caption, MB_ICONERROR);
                Exit;
            end;

    		bExisteConvenioNoCandidato	:= False;
        	TListaConvenios 			:= TStringList.Create;
			cdsObtenerConveniosMorososCobExtraJudicial.First;
            while not cdsObtenerConveniosMorososCobExtraJudicial.Eof do begin
            	if cdsObtenerConveniosMorososCobExtraJudicialMarcar.AsBoolean then begin
                	if EsConvenioCandidatoAInhabilitar(cdsObtenerConveniosMorososCobExtraJudicialCodigoConvenio.AsInteger) then
                		TListaConvenios.Add(cdsObtenerConveniosMorososCobExtraJudicialCodigoConvenio.AsString)
                    else
                    	bExisteConvenioNoCandidato := True;
                end;
                cdsObtenerConveniosMorososCobExtraJudicial.Next;
            end;

            if (bExisteConvenioNoCandidato) and (TListaConvenios.Count>0) then
                MsgBox(MSG_EXISTE_CONVENIO_NO_CANDIDATO, Caption, MB_ICONINFORMATION)
            else if TListaConvenios.Count = 0 then begin
            	MsgBox(MSG_NO_EXISTE_CONVENIO_CANDIDATO, Caption, MB_ICONSTOP);
                Exit;
            	end;

            FormInhabilitacionTAGs := TFormInhabilitacionTAGs.Create(Self);
            with FormInhabilitacionTAGs do begin
            	if Inicializar(TListaConvenios, CodigoPlantillaCobExtraJudicial, vcbAccionDeCobranzaExtraJudicial.Text, False) then begin
                    if ShowModal = mrOk then begin
                    	try
                            Screen.Cursor := crHourGlass;
                            Application.ProcessMessages;
                        
                        	if cdsObtenerConveniosMorosos.Active then begin
                            	cdsObtenerConveniosMorosos.Close;
                                cdsObtenerConveniosMorosos.Open
                            end;
                        finally
							Screen.Cursor := crDefault;
                        end;
                    end;

                end;
            end;
            TListaConvenios.Free;
        end

        else if cbbTipoAccionCobranzaExtraJudicial.Value = ACCION_COBRANZA_TRASPASO_COB_INTERNA then begin
            cdsObtenerConveniosMorososCobExtraJudicial.DisableControls;
			cdsObtenerConveniosMorososCobExtraJudicial.First;
            while not cdsObtenerConveniosMorososCobExtraJudicial.Eof do begin
            	if cdsObtenerConveniosMorososCobExtraJudicialMarcar.AsBoolean then begin
                    with spIngresarConvenioACobranzaInterna, Parameters do begin
                        Parameters.Refresh;
                        ParamByName('@CodigoConvenio').Value	:= cdsObtenerConveniosMorososCobExtraJudicialCodigoConvenio.AsInteger;
                        ParamByName('@Usuario').Value			:= UsuarioSistema;
                        try
                            ExecProc;
                        except
                            on E: Exception do begin
                                MsgBox(E.Message, Caption, MB_ICONERROR);
                                Exit;
                            end;
                        end;
                    end;
                end;
                cdsObtenerConveniosMorososCobExtraJudicial.Next;
            end;
            cdsObtenerConveniosMorososCobExtraJudicial.EnableControls;

        	MsgBox('Convenios Traspasados a Cobranza Interna', Caption, MB_ICONINFORMATION);
        end

        else if cbbTipoAccionCobranzaExtraJudicial.Value = ACCION_COBRANZA_CASTIGO then begin

            if CantCobInternaMarcados > 1 then begin
                MsgBox(Format(MSG_ACCION_UNITARIA, ['Castigo']), Caption, MB_ICONINFORMATION);
                Exit;
            end;

            fCastigo := TformCobroComprobantesCastigo.Create(self);
            fCastigo.Inicializar(cdsObtenerConveniosMorososCobExtraJudicialNumeroDocumento.AsString, 
                                 cdsObtenerConveniosMorososCobExtraJudicialCodigoConvenio.AsInteger, Main.MainForm.NumeroPOS, Main.MainForm.PuntoEntrega, Main.MainForm.PuntoVenta);
            try
                fCastigo.ShowModal;
                if ModalResult = mrOk then
                    btnBuscarCobInternaClick(nil);
            except
                fCastigo.Show;
            end;
        end

    	else if cbbTipoAccionCobranzaExtraJudicial.Value = ACCION_COBRANZA_REFINANCIACION then begin
        
            if CantCobInternaMarcados > 1 then begin
                MsgBox(Format(MSG_ACCION_UNITARIA, ['Refinanciaci�n']), Caption, MB_ICONINFORMATION);
                Exit;
            end;

            FCodigoCliente	:= cdsObtenerConveniosMorososCobExtraJudicialCodigoCliente.AsInteger;
            FCodigoConvenio	:= cdsObtenerConveniosMorososCobExtraJudicialCodigoConvenio.AsInteger;

            RefinanciacionGestionForm := TRefinanciacionGestionForm.Create(Self);

            with spObtenerRefinanciacionesConsultas, Parameters do begin
                Parameters.Refresh;
                ParamByName('@CodigoRefinanciacion').Value := Null;
                ParamByName('@CodigoPersona').Value        := cdsObtenerConveniosMorososCobExtraJudicialCodigoCliente.AsInteger;
                ParamByName('@RUTPersona').Value           := cdsObtenerConveniosMorososCobExtraJudicialNumeroDocumento.AsString;
                ParamByName('@CodigoConvenio').Value       := cdsObtenerConveniosMorososCobExtraJudicialCodigoConvenio.AsInteger;
                ParamByName('@CodigoTipoMedioPago').Value  := Null;
                ParamByName('@SoloNoActivadas').Value      := 0;
            end;

            cdsRefinanciaciones.Open;
            with RefinanciacionGestionForm do begin
                if Inicializar(0, 0, vcbTipoRefinanciacion.Value, FCodigoCliente, FCodigoConvenio) then begin
                    if ShowModal = mrOk then begin
                        try
                            Screen.Cursor := crHourGlass;
                            Application.ProcessMessages;

                            if cdsRefinanciaciones.Active then begin
                                cdsRefinanciaciones.Close;
                                cdsRefinanciaciones.Open;
                            end
                            else begin
                                peRut.Text := EmptyStr;
                                vcbConvenios.Clear;
                                vcbTipoRefinanciacion.ItemIndex := vcbTipoRefinanciacion.Items.IndexOfValue(0);
                                lblEnListaAmarilla.Visible  := False;
                                lblEnListaAmarilla.Caption  := EmptyStr;
                            end;

                        finally
                            Screen.Cursor := crDefault;
                        end;
                    end;
                end;
            end;
        end;

        //btnBuscarCobInternaClick(nil);
    finally
    	cdsObtenerConveniosMorososCobExtraJudicial.EnableControls;
		Screen.Cursor := crDefault;
    end;
end;

procedure TFormCarpetaDeudores.btnEjecutarAccionCobJudicialClick(
  Sender: TObject);
var
    fCastigo: TformCobroComprobantesCastigo;
    fCobranzaExterna: TGestionarCobranzaExternaForm;
    TListaConvenios: TStringList;
    bExisteConvenioNoCandidato: Boolean;
    strConveniosCSV: string;
    i, FCodigoCliente, FCodigoConvenio: Integer;
begin
	if (not cdsObtenerConveniosMorososCobJudicial.Active) or (cdsObtenerConveniosMorososCobJudicial.RecordCount=0) then begin
    	MsgBox(MSG_NO_HAY_REGISTROS, Caption, MB_ICONINFORMATION);
        Exit;
    end;

    if cbbTipoAccionCobranzaJudicial.ItemIndex < 0 then begin
        MsgBox(MSG_DEBE_SELECCIONAR_ACCION, Caption, MB_ICONSTOP);
        Exit;
    end;

    if CantCobJudicialMarcados = 0 then begin
    	MsgBox(MSG_NO_HAY_SELECCION, Caption, MB_ICONINFORMATION);
        Exit;
    end;
    
	try
    	Screen.Cursor 	:= crHourGlass;
        cdsObtenerConveniosMorososCobJudicial.DisableControls;

        if cbbTipoAccionCobranzaJudicial.Value = ACCION_COBRANZA_INHABILITACION_TAGS then begin
            if CodigoPlantillaCobJudicial < 0 then begin
                MsgBox(MSG_DEBE_SELECCIONAR_PLANTILLA, Caption, MB_ICONERROR);
                Exit;
            end;

    		bExisteConvenioNoCandidato	:= False;
        	TListaConvenios 			:= TStringList.Create;
			cdsObtenerConveniosMorososCobJudicial.First;
            while not cdsObtenerConveniosMorososCobJudicial.Eof do begin
            	if cdsObtenerConveniosMorososCobJudicialMarcar.AsBoolean then begin
                	if EsConvenioCandidatoAInhabilitar(cdsObtenerConveniosMorososCobJudicialCodigoConvenio.AsInteger) then
                		TListaConvenios.Add(cdsObtenerConveniosMorososCobJudicialCodigoConvenio.AsString)
                    else
                    	bExisteConvenioNoCandidato := True;
                end;
                cdsObtenerConveniosMorososCobJudicial.Next;
            end;

            if (bExisteConvenioNoCandidato) and (TListaConvenios.Count>0) then
                MsgBox(MSG_EXISTE_CONVENIO_NO_CANDIDATO, Caption, MB_ICONINFORMATION)
            else if TListaConvenios.Count = 0 then begin
            	MsgBox(MSG_NO_EXISTE_CONVENIO_CANDIDATO, Caption, MB_ICONSTOP);
                Exit;
            	end;

            FormInhabilitacionTAGs := TFormInhabilitacionTAGs.Create(Self);
            with FormInhabilitacionTAGs do begin
            	if Inicializar(TListaConvenios, CodigoPlantillaCobJudicial, vcbAccionDeCobranzaJudicial.Text, False) then begin
                    if ShowModal = mrOk then begin
                    	try
                            Screen.Cursor := crHourGlass;
                            Application.ProcessMessages;
                        
                        	if cdsObtenerConveniosMorosos.Active then begin
                            	cdsObtenerConveniosMorosos.Close;
                                cdsObtenerConveniosMorosos.Open
                            end;
                        finally
							Screen.Cursor := crDefault;
                        end;
                    end;

                end;
            end;
            TListaConvenios.Free;
        end

        else if cbbTipoAccionCobranzaJudicial.Value = ACCION_COBRANZA_TRASPASO_COB_INTERNA then begin
            cdsObtenerConveniosMorososCobJudicial.DisableControls;
			cdsObtenerConveniosMorososCobJudicial.First;
            while not cdsObtenerConveniosMorososCobJudicial.Eof do begin
            	if cdsObtenerConveniosMorososCobJudicialMarcar.AsBoolean then begin
                    with spIngresarConvenioACobranzaInterna, Parameters do begin
                        Parameters.Refresh;
                        ParamByName('@CodigoConvenio').Value	:= cdsObtenerConveniosMorososCobJudicialCodigoConvenio.AsInteger;
                        ParamByName('@Usuario').Value			:= UsuarioSistema;
                        try
                            ExecProc;
                        except
                            on E: Exception do begin
                                MsgBox(E.Message, Caption, MB_ICONERROR);
                                Exit;
                            end;
                        end;
                    end;
                end;
                cdsObtenerConveniosMorososCobJudicial.Next;
            end;
            cdsObtenerConveniosMorososCobJudicial.EnableControls;

        	MsgBox('Convenios Traspasados a Cobranza Interna', Caption, MB_ICONINFORMATION);
        end

        else if cbbTipoAccionCobranzaJudicial.Value = ACCION_TRANSFERENCIA_ENTRE_EMPRESAS then begin
        
        	TListaConvenios	:= TStringList.Create;
            cdsObtenerConveniosMorososCobInterna.First;
            while (not cdsObtenerConveniosMorososCobInterna.Eof) do begin
                if cdsObtenerConveniosMorososCobInternaMarcar.AsBoolean then
                	TListaConvenios.Add(IntToStr(cdsObtenerConveniosMorososCobInternaCodigoConvenio.AsInteger));
                cdsObtenerConveniosMorososCobInterna.Next;
            end;
            
            strConveniosCSV	:= '';
            for i := 0 to TListaConvenios.Count - 1 do
                strConveniosCSV	:= strConveniosCSV + TListaConvenios[i] + ',';
            strConveniosCSV := Copy(strConveniosCSV, 1, Length(strConveniosCSV)-1);

            fCobranzaExterna := TGestionarCobranzaExternaForm.Create(self);
            fCobranzaExterna.Inicializar(cbbTipoAccionCobranzaJudicial.Value, strConveniosCSV);
            if fCobranzaExterna.ShowModal = mrOk then begin
                //si se procesaron registros entonces hago refresh
                btnBuscarCobJudicial.OnClick(btnBuscarCobJudicial);
            end;

        end

        else if cbbTipoAccionCobranzaJudicial.Value = ACCION_COBRANZA_CASTIGO then begin

            if CantCobInternaMarcados > 1 then begin
                MsgBox(Format(MSG_ACCION_UNITARIA, ['Castigo']), Caption, MB_ICONINFORMATION);
                Exit;
            end;

            fCastigo := TformCobroComprobantesCastigo.Create(self);
            fCastigo.Inicializar(cdsObtenerConveniosMorososCobJudicialNumeroDocumento.AsString, 
                                 cdsObtenerConveniosMorososCobJudicialCodigoConvenio.AsInteger, Main.MainForm.NumeroPOS, Main.MainForm.PuntoEntrega, Main.MainForm.PuntoVenta);
            try
                fCastigo.ShowModal;
                if ModalResult = mrOk then
                    btnBuscarCobInternaClick(nil);
            except
                fCastigo.Show;
            end;
        end

    	else if cbbTipoAccionCobranzaJudicial.Value = ACCION_COBRANZA_REFINANCIACION then begin
        
            if CantCobInternaMarcados > 1 then begin
                MsgBox(Format(MSG_ACCION_UNITARIA, ['Refinanciaci�n']), Caption, MB_ICONINFORMATION);
                Exit;
            end;

            FCodigoCliente	:= cdsObtenerConveniosMorososCobJudicialCodigoCliente.AsInteger;
            FCodigoConvenio	:= cdsObtenerConveniosMorososCobJudicialCodigoConvenio.AsInteger;

            RefinanciacionGestionForm := TRefinanciacionGestionForm.Create(Self);

            with spObtenerRefinanciacionesConsultas, Parameters do begin
                Parameters.Refresh;
                ParamByName('@CodigoRefinanciacion').Value := Null;
                ParamByName('@CodigoPersona').Value        := cdsObtenerConveniosMorososCobJudicialCodigoCliente.AsInteger;
                ParamByName('@RUTPersona').Value           := cdsObtenerConveniosMorososCobJudicialNumeroDocumento.AsString;
                ParamByName('@CodigoConvenio').Value       := cdsObtenerConveniosMorososCobJudicialCodigoConvenio.AsInteger;
                ParamByName('@CodigoTipoMedioPago').Value  := Null;
                ParamByName('@SoloNoActivadas').Value      := 0;
            end;

            cdsRefinanciaciones.Open;
            with RefinanciacionGestionForm do begin
                if Inicializar(0, 0, vcbTipoRefinanciacion.Value, FCodigoCliente, FCodigoConvenio) then begin
                    if ShowModal = mrOk then begin
                        try
                            Screen.Cursor := crHourGlass;
                            Application.ProcessMessages;

                            if cdsRefinanciaciones.Active then begin
                                cdsRefinanciaciones.Close;
                                cdsRefinanciaciones.Open;
                            end
                            else begin
                                peRut.Text := EmptyStr;
                                vcbConvenios.Clear;
                                vcbTipoRefinanciacion.ItemIndex := vcbTipoRefinanciacion.Items.IndexOfValue(0);
                                lblEnListaAmarilla.Visible  := False;
                                lblEnListaAmarilla.Caption  := EmptyStr;
                            end;

                        finally
                            Screen.Cursor := crDefault;
                        end;
                    end;
                end;
            end;
        end;

        //btnBuscarCobInternaClick(nil);
    finally
    	cdsObtenerConveniosMorososCobJudicial.EnableControls;
		Screen.Cursor := crDefault;
    end;
end;

procedure TFormCarpetaDeudores.btnExportarDatosACSVCobInternaClick(
  Sender: TObject);
resourcestring
	MSG_NO_HAY_REGISTROS = 'No hay registros para exportar';
begin
	if (not cdsObtenerConveniosMorososCobInterna.Active) or (cdsObtenerConveniosMorososCobInterna.RecordCount=0) then begin
    	MsgBox(MSG_NO_HAY_REGISTROS, Caption, MB_ICONINFORMATION);
        Exit;
    end;

    cdsObtenerConveniosMorososCobInterna.DisableControls;
    //Indicar en la funci�n el DataSet a exportar y Prefijo para nombre de Archivo a guardar
    SaveToCSV(cdsObtenerConveniosMorososCobInterna, 'Convenios_Morosos_En_Cobranza_Interna');
    cdsObtenerConveniosMorososCobInterna.EnableControls;
end;

procedure TFormCarpetaDeudores.btnExportarDatosACSVCobExtraJudicialClick(
  Sender: TObject);
resourcestring
	MSG_NO_HAY_REGISTROS = 'No hay registros para exportar';
begin
	if (not cdsObtenerConveniosMorososCobExtraJudicial.Active) or (cdsObtenerConveniosMorososCobExtraJudicial.RecordCount=0) then begin
    	MsgBox(MSG_NO_HAY_REGISTROS, Caption, MB_ICONINFORMATION);
        Exit;
    end;

    cdsObtenerConveniosMorososCobExtraJudicial.DisableControls;
    SaveToCSV(cdsObtenerConveniosMorososCobExtraJudicial, 'Convenios_Morosos_En_Cobranza_PreJudicial');
    cdsObtenerConveniosMorososCobExtraJudicial.EnableControls;
end;

procedure TFormCarpetaDeudores.btnExportarDatosACSVCobJudicialClick(
  Sender: TObject);
resourcestring
	MSG_NO_HAY_REGISTROS = 'No hay registros para exportar';
begin
	if (not cdsObtenerConveniosMorososCobJudicial.Active) or (cdsObtenerConveniosMorososCobJudicial.RecordCount=0) then begin
    	MsgBox(MSG_NO_HAY_REGISTROS, Caption, MB_ICONINFORMATION);
        Exit;
    end;

    cdsObtenerConveniosMorososCobJudicial.DisableControls;
    SaveToCSV(cdsObtenerConveniosMorososCobJudicial, 'Convenios_Morosos_En_Cobranza_Judicial');
    cdsObtenerConveniosMorososCobJudicial.EnableControls;
end;


procedure TFormCarpetaDeudores.btnHistorialDeudoresClick(Sender: TObject);
resourcestring
	MSG_NO_HAY_SELECCION = 'No ha seleccionado un Convenio para ver su historial de cobranza';
	MSG_NO_HAY_REGISTROS = 'El Convenio seleccionado no tiene historial de cobranza';
var
	f: TFormHistorialCobranzas;
begin
	if (not cdsObtenerConveniosMorososDeudores.Active) or (cdsObtenerConveniosMorososDeudores.RecordCount=0) then begin
    	MsgBox(MSG_NO_HAY_SELECCION, Caption, MB_ICONINFORMATION);
        Exit;
    end;

    if CantDeudoresMarcados > 1 then begin
    	MsgBox(Format(MSG_ACCION_UNITARIA, ['Historial de Cobranza']), Caption, MB_ICONINFORMATION);
        Exit;
    end;

	try
    	cdsObtenerConveniosMorososDeudores.DisableControls;
    	cdsObtenerConveniosMorososDeudores.First;
    	while not cdsObtenerConveniosMorososDeudores.EOF do begin
			if cdsObtenerConveniosMorososDeudoresMarcar.AsBoolean then begin
            	with spObtenerHistorialDeCobranza, Parameters do begin
                    if Active then
                    	Close;
                    Parameters.Refresh;
                	ParamByName('@CodigoConvenio').Value	:= cdsObtenerConveniosMorososDeudoresCodigoConvenio.AsInteger;
                    Open;
                    if Not Eof then begin
                        Application.CreateForm(TFormHistorialCobranzas,f);
                        if not f.Inicializar(cdsObtenerConveniosMorososDeudoresCodigoConvenio.AsInteger, cdsObtenerConveniosMorososDeudoresNumeroConvenio.AsString) then
                            Exit;
                        f.ShowModal;
                    end
                    else begin
                        MsgBox(MSG_NO_HAY_REGISTROS, Caption, MB_ICONINFORMATION);
                        Exit;
                    end;
                    Close;
                end;
                Break;
            end;
            cdsObtenerConveniosMorososDeudores.Next;
        end;
    finally
    	if Assigned(f) then
			f.Release;
        cdsObtenerConveniosMorososDeudores.EnableControls;
    end;

end;

procedure TFormCarpetaDeudores.btnHistorialCobInternaClick(Sender: TObject);
resourcestring
	MSG_NO_HAY_SELECCION = 'No ha seleccionado un Convenio para ver su historial de cobranza';
	MSG_NO_HAY_REGISTROS = 'El Convenio seleccionado no tiene historial de cobranza';
var
	f: TFormHistorialCobranzas;
begin
	if (not cdsObtenerConveniosMorososCobInterna.Active) or (cdsObtenerConveniosMorososCobInterna.RecordCount=0) then begin
    	MsgBox(MSG_NO_HAY_SELECCION, Caption, MB_ICONINFORMATION);
        Exit;
    end;

    if CantCobInternaMarcados > 1 then begin
    	MsgBox(Format(MSG_ACCION_UNITARIA, ['Historial de Cobranza']), Caption, MB_ICONINFORMATION);
        Exit;
    end;

	try
    	cdsObtenerConveniosMorososCobInterna.DisableControls;
    	cdsObtenerConveniosMorososCobInterna.First;
    	while not cdsObtenerConveniosMorososCobInterna.Eof do begin
			if cdsObtenerConveniosMorososCobInternaMarcar.AsBoolean then begin
            	with spObtenerHistorialDeCobranza, Parameters do begin
                    if Active then
                    	Close;
                    Parameters.Refresh;
                	ParamByName('@CodigoConvenio').Value	:= cdsObtenerConveniosMorososCobInternaCodigoConvenio.AsInteger;
                    Open;
                    if Not Eof then begin
                        Application.CreateForm(TFormHistorialCobranzas,f);
                        if not f.Inicializar(cdsObtenerConveniosMorososCobInternaCodigoConvenio.AsInteger, cdsObtenerConveniosMorososCobInternaNumeroConvenio.AsString) then
                            Exit;
                        f.ShowModal;
                    end
                    else begin
                        MsgBox(MSG_NO_HAY_REGISTROS, Caption, MB_ICONINFORMATION);
                        Exit;
                    end;
                    Close;
                end;
                Break;
            end;
            cdsObtenerConveniosMorososCobInterna.Next;
        end;
    finally
    	if Assigned(f) then
			f.Release;
        cdsObtenerConveniosMorososCobInterna.EnableControls;
    end;
end;

procedure TFormCarpetaDeudores.btnHistorialCobExtraJudicialClick(
  Sender: TObject);
resourcestring
	MSG_NO_HAY_SELECCION = 'No ha seleccionado un Convenio para ver su historial de cobranza';
	MSG_NO_HAY_REGISTROS = 'El Convenio seleccionado no tiene historial de cobranza';
var
	f: TFormHistorialCobranzas;
begin
	if (not cdsObtenerConveniosMorososCobExtraJudicial.Active) or (cdsObtenerConveniosMorososCobExtraJudicial.RecordCount=0) then begin
    	MsgBox(MSG_NO_HAY_SELECCION, Caption, MB_ICONINFORMATION);
        Exit;
    end;

    if CantCobExtraJudicialMarcados > 1 then begin
    	MsgBox(Format(MSG_ACCION_UNITARIA, ['Historial de Cobranza']), Caption, MB_ICONINFORMATION);
        Exit;
    end;

	try
    	cdsObtenerConveniosMorososCobExtraJudicial.DisableControls;
    	cdsObtenerConveniosMorososCobExtraJudicial.First;
    	while not cdsObtenerConveniosMorososCobExtraJudicial.EOF do begin
			if cdsObtenerConveniosMorososCobExtraJudicialMarcar.AsBoolean then begin
            	with spObtenerHistorialDeCobranza, Parameters do begin
                    if Active then
                    	Close;
                    Parameters.Refresh;
                	ParamByName('@CodigoConvenio').Value	:= cdsObtenerConveniosMorososCobExtraJudicialCodigoConvenio.AsInteger;
                    Open;
                    if Not Eof then begin
                        Application.CreateForm(TFormHistorialCobranzas,f);
                        if not f.Inicializar(cdsObtenerConveniosMorososCobExtraJudicialCodigoConvenio.AsInteger, cdsObtenerConveniosMorososCobExtraJudicialNumeroConvenio.AsString) then
                            Exit;
                        f.ShowModal;
                    end
                    else begin
                        MsgBox(MSG_NO_HAY_REGISTROS, Caption, MB_ICONINFORMATION);
                        Exit;
                    end;
                    Close;
                end;
                Break;
            end;
        	cdsObtenerConveniosMorososCobExtraJudicial.Next;
        end;
    finally
    	if Assigned(f) then
			f.Release;
        cdsObtenerConveniosMorososCobExtraJudicial.EnableControls;
    end;
end;

procedure TFormCarpetaDeudores.btnHistorialCobJudicialClick(Sender: TObject);
resourcestring
	MSG_NO_HAY_SELECCION = 'No ha seleccionado un Convenio para ver su historial de cobranza';
	MSG_NO_HAY_REGISTROS = 'El Convenio seleccionado no tiene historial de cobranza';
var
	f: TFormHistorialCobranzas;
begin
	if (not cdsObtenerConveniosMorososCobJudicial.Active) or (cdsObtenerConveniosMorososCobJudicial.RecordCount=0) then begin
    	MsgBox(MSG_NO_HAY_SELECCION, Caption, MB_ICONINFORMATION);
        Exit;
    end;

    if CantCobJudicialMarcados > 1 then begin
    	MsgBox(Format(MSG_ACCION_UNITARIA, ['Historial de Cobranza']), Caption, MB_ICONINFORMATION);
        Exit;
    end;

	try
    	cdsObtenerConveniosMorososCobJudicial.DisableControls;
    	cdsObtenerConveniosMorososCobJudicial.First;
    	while not cdsObtenerConveniosMorososCobJudicial.EOF do begin
			if cdsObtenerConveniosMorososCobJudicialMarcar.AsBoolean then begin
            	with spObtenerHistorialDeCobranza, Parameters do begin
                    if Active then
                    	Close;
                    Parameters.Refresh;
                	ParamByName('@CodigoConvenio').Value	:= cdsObtenerConveniosMorososCobJudicialCodigoConvenio.AsInteger;
                    Open;
                    if Not Eof then begin
                        Application.CreateForm(TFormHistorialCobranzas,f);
                        if not f.Inicializar(cdsObtenerConveniosMorososCobJudicialCodigoConvenio.AsInteger, cdsObtenerConveniosMorososCobJudicialNumeroConvenio.AsString) then
                            Exit;
                        f.ShowModal;
                    end
                    else begin
                        MsgBox(MSG_NO_HAY_REGISTROS, Caption, MB_ICONINFORMATION);
                        Exit;
                    end;
                    Close;
                end;
                Break;
            end;
            cdsObtenerConveniosMorososCobJudicial.Next
        end;
    finally
    	if Assigned(f) then
			f.Release;
        cdsObtenerConveniosMorososCobJudicial.EnableControls;
    end;
end;

procedure TFormCarpetaDeudores.btnSalirClick(Sender: TObject);
begin
	Close;
end;

procedure TFormCarpetaDeudores.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree;
end;

procedure TFormCarpetaDeudores.FormDestroy(Sender: TObject);
begin
	FreeAndNil(FAccionesDeCobranzaBiz);
	inherited;
end;

procedure TFormCarpetaDeudores.btnHistoricoCobExtraJudicialClick(
  Sender: TObject);
var
	f: TFormHistoricoCobranzaExterna;
begin
	try
	    Application.CreateForm(TFormHistoricoCobranzaExterna, f);
        if not f.Inicializar() then
        	Exit;
    	f.ShowModal
    finally
		f.Release;
    end;
end;

procedure TFormCarpetaDeudores.btnExportarDatosACSVDeudoresClick(
  Sender: TObject);
resourcestring
	MSG_NO_HAY_REGISTROS = 'No hay registros para exportar';
begin
	if (not cdsObtenerConveniosMorososDeudores.Active) or (cdsObtenerConveniosMorososDeudores.RecordCount=0) then begin
    	MsgBox(MSG_NO_HAY_REGISTROS, Caption, MB_ICONINFORMATION);
        Exit;
    end;

    cdsObtenerConveniosMorososDeudores.DisableControls;
    //Indicar en la funci�n el DataSet a exportar y Prefijo para nombre de Archivo a guardar
    SaveToCSV(cdsObtenerConveniosMorososDeudores, 'Convenios_Morosos');
    cdsObtenerConveniosMorososDeudores.EnableControls;
end;

end.

