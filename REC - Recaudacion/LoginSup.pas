{********************************** Unit Header ********************************
File Name : LoginSup.pas
Author :   Juan Lobos - JLO
Date Created: 25-04-2016
Description : Login para credenciales especificas o especiales.

*******************************************************************************}

unit LoginSup;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, ADODB, Provider, DB, DBClient,
  Grids, DBGrids, DMConnection, UtilProc, PeaTypes, UtilDB, Util, RStrings,
  PeaProcs, ComCtrls, DmiCtrls, Menus, FrmAperturaCierrePuntoVenta;

type
  TFrmConfirmacionSupervisor = class(TForm)
    pnl1: TPanel;
    lbl1: TLabel;
    lbl2: TLabel;
    img1: TImage;
    edt_password: TEdit;
    btn_ok: TButton;
    btn_cancel: TButton;
    spValidarLogin_Sup: TADOStoredProc;
    edt_supervisor: TEdit;
    procedure btn_cancelClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure edt_supervisorChange(Sender: TObject);
    procedure btn_okClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    SupervisorLogin: AnsiString;  //TASK_031_GLE_20170111
  end;

var
  FrmConfirmacionSupervisor: TFrmConfirmacionSupervisor;

implementation


{$R *.dfm}

procedure TFrmConfirmacionSupervisor.btn_cancelClick(Sender: TObject);
begin
    FrmAperturaCierrePuntoVenta.InfoValidada := 1;
    Close;
end;

procedure TFrmConfirmacionSupervisor.btn_okClick(Sender: TObject);

begin

    try
        with spValidarLogin_Sup, Parameters  do begin

            ParamByName('@CodigoUsuario').Value := edt_supervisor.text;
            ParamByName('@Password').Value 	:= edt_password.text;
            ParamByName('@CodigoGrupo').Value := 'Supervisores';
            ParamByName('@ErrorDescription').Value 	 := NULL;

            ExecProc;

            if Parameters.ParamByName('@RETURN_VALUE').Value = 0 then begin
               FrmAperturaCierrePuntoVenta.InfoValidada:=0;
               FrmAperturaCierrePuntoVenta.Supervisor:= edt_supervisor.Text;
               SupervisorLogin  := edt_supervisor.Text; //TASK_031_GLE_20170111
               ModalResult := mrOK;
               end else begin
               MsgBox(ParamByName('@ErrorDescription').Value);
               Exit;
            end;
        end;

    except
    	on e: Exception do begin
           	MsgBoxErr(MSG_ERROR_TURNO_GUARDAR, e.message, self.caption, MB_ICONSTOP);
    	end;
    end;
end;

procedure TFrmConfirmacionSupervisor.edt_supervisorChange(Sender: TObject);
begin
    btn_ok.Enabled  := edt_supervisor.text   <> '';
end;

procedure TFrmConfirmacionSupervisor.FormCreate(Sender: TObject);
begin
    CenterForm(Self);
end;

end.
