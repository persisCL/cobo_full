{-------------------------------------------------------------------------------
 File Name: FrmReimpresionNotaCobroInfractores.pas
 Author: SGuastoni
 Date Created:
 Language: ES-AR
 Description: Reimpresion de notas de cobro de infractores

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

-------------------------------------------------------------------------------}
unit FrmReimpresionNotaCobroInfractores;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Validate, DateEdit, ListBoxEx, DBListEx, StdCtrls, DmiCtrls, ExtCtrls,
  DB, ADODB,DateUtils, UtilProc, Util,PeaTypes,DMConnection, BuscaClientes;

type
  TReimpresionNotaCobroInfractoresForm = class(TForm)
    Panel1: TPanel;
    pnlFiltros: TPanel;
    pnlDetalle: TPanel;
    pnlPrincipal: TPanel;
    dbListNI: TDBListEx;
    spObtenerListadoNotaInfractores: TADOStoredProc;
    dsListarNI: TDataSource;
    spObtenerListadoNotaInfractoresDetalle: TADOStoredProc;
    dsListarNIDetalle: TDataSource;
    GroupBox1: TGroupBox;
    lblNIFiltro: TLabel;
    neNI: TNumericEdit;
    lblNroRut: TLabel;
    btnBuscar: TButton;
    btnLimpiar: TButton;
    pnlDetalleImporte: TPanel;
    dbListDetalleNI: TDBListEx;
    pnlDetalleNombres: TPanel;
    lblDomicilioNI: TLabel;
    lblNombreNI: TLabel;
    pnlBotones: TPanel;
    btnImprimir: TButton;
    btnSalir: TButton;
    gbFechaEmision: TGroupBox;
    gbFechaVencimiento: TGroupBox;
    lblEmisionDesde: TLabel;
    deEmiDesde: TDateEdit;
    lblEmisionHasta: TLabel;
    deEmiHasta: TDateEdit;
    lblVencimientoDesde: TLabel;
    deVenDesde: TDateEdit;
    lblVencimientoHasta: TLabel;
    deVenHasta: TDateEdit;
    edNombre: TLabel;
    edDomicilio: TLabel;
    neRUT: TPickEdit;
    procedure btnBuscarClick(Sender: TObject);
    procedure spObtenerListadoNotaInfractoresAfterScroll(DataSet: TDataSet);
    procedure btnLimpiarClick(Sender: TObject);
    procedure neNIEnter(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSalirClick(Sender: TObject);
    procedure dbListNIDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn;
      Rect: TRect; State: TOwnerDrawState; var Text: string; var TxtRect: TRect;
      var ItemWidth: Integer; var DefaultDraw: Boolean);
    procedure dbListDetalleNIDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure neRUTButtonClick(Sender: TObject);
  private
    { Private declarations }
    FUltimaBusqueda: TBusquedaCliente;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    procedure IniciarBusqueda;
    procedure BuscarDetalle(NumeroNI:Integer);
    function validarIngresos:Boolean;
  public
    { Public declarations }
    function Inicializar(Caption:string):Boolean;
  end;

var
  ReimpresionNotaCobroInfractoresForm: TReimpresionNotaCobroInfractoresForm;

implementation

uses frmRptNotaCobroInfractor;

{$R *.dfm}

{ TFormReimpresionInfracciones }

function TReimpresionNotaCobroInfractoresForm.Inicializar(Caption:string): Boolean;
begin
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216
    Self.Caption := Caption;
    Result := True;
end;

procedure TReimpresionNotaCobroInfractoresForm.btnBuscarClick(Sender: TObject);
begin
    if validarIngresos then IniciarBusqueda;
end;

procedure TReimpresionNotaCobroInfractoresForm.btnImprimirClick(Sender: TObject);
var
    fReporte: TfRptNotaCobroInfractor;
begin
    fReporte := TfRptNotaCobroInfractor.Create(self);
    try
        freporte.Inicializar(spObtenerListadoNotaInfractores.FieldByName('NumeroComprobante').AsInteger);
    finally
        freporte.Release;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: LimpiarClick
  Author:  SGuastoni
  Date Created:
  Description:  Limpia los componentes
  Parameters:
  Return Value:
-----------------------------------------------------------------------------}
procedure TReimpresionNotaCobroInfractoresForm.btnLimpiarClick(Sender: TObject);
begin
    neNI.Clear;
    neRut.Clear;
    deEmiDesde.Clear;
    deEmiHasta.Clear;
    deVenDesde.Clear;
    deVenHasta.Clear;
    spObtenerListadoNotaInfractores.Close;
    spObtenerListadoNotaInfractoresDetalle.Close;
    edNombre.Caption := '';
    edDomicilio.Caption := '';
    btnImprimir.Enabled := false;
end;

{-----------------------------------------------------------------------------
  Function Name: BuscarDetalle
  Author:  SGuastoni
  Date Created:
  Description:  Busca el detalle de una factura de cobro sengun un numero de doc.
  Parameters: NumeroNI:Integer
  Return Value:
-----------------------------------------------------------------------------}
procedure TReimpresionNotaCobroInfractoresForm.BuscarDetalle(NumeroNI:Integer);
resourcestring
    MSG_ERROR_CONSULTA = 'Error de conexi�n';
begin
    edNombre.Caption := spObtenerListadoNotaInfractores.FieldByName('Apellido').AsString;
    edDomicilio.Caption := spObtenerListadoNotaInfractores.FieldByName('Calle').AsString;
    spObtenerListadoNotaInfractoresDetalle.Close;
    spObtenerListadoNotaInfractoresDetalle.Parameters.ParamByName('@NumeroNI').Value := NumeroNI;
    try
        spObtenerListadoNotaInfractoresDetalle.Open;
    except
		on e: exception do begin
			MsgBoxErr(MSG_ERROR_CONSULTA, e.Message,
			  Self.Caption, MB_ICONSTOP);
			Exit;
		end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: IniciarBusqueda
  Author:  SGuastoni
  Date Created:
  Description:  Activa los filtros de busqueda y ejecuta Store
  Parameters:
  Return Value:
-----------------------------------------------------------------------------}
procedure TReimpresionNotaCobroInfractoresForm.IniciarBusqueda;
resourcestring
    MSG_NO_SE_ENCUENTRAN_DATOS = 'No se encuentran datos con los filtros de b�squeda ingresados';
    MSG_ERROR_CONSULTA = 'Error de conexi�n';
begin
    spObtenerListadoNotaInfractores.Close;

    with spObtenerListadoNotaInfractores.Parameters do begin

        if (Length(Trim(neNI.Text))> 0)  then ParamByName('@NumeroNI').Value      := neNI.ValueInt                  else ParamByName('@NumeroNI').Value := null;
        if (deEmiDesde.Date <> NullDate) then ParamByName('@FechaEmiDesde').Value := StartOfTheDay(deEmiDesde.Date) else ParamByName('@FechaEmiDesde').Value := null;
        if (deEmiHasta.Date <> NullDate) then ParamByName('@FechaEmiHasta').Value := EndOfTheDay(deEmiHasta.Date)   else ParamByName('@FechaEmiHasta').Value := null;
        if (deVenDesde.Date <> NullDate) then ParamByName('@FechaVenDesde').Value := StartOfTheDay(deVenDesde.Date) else ParamByName('@FechaVenDesde').Value := null;
        if (deVenHasta.Date <> NullDate) then ParamByName('@FechaVenHasta').Value := EndOfTheDay(deVenHasta.Date)   else ParamByName('@FechaVenHasta').Value := null;
        if (Length(Trim(neRut.Text))> 0) then ParamByName('@NumeroRUT').Value     := Trim(neRut.Text)               else ParamByName('@NumeroRUT').Value := null;

    end;

    try
        spObtenerListadoNotaInfractores.Open;
    except
		on e: exception do begin
			MsgBoxErr(MSG_ERROR_CONSULTA, e.Message,
			  Self.Caption, MB_ICONSTOP);
			Exit;
		end;
    end;

    if not spObtenerListadoNotaInfractores.IsEmpty then begin
        btnImprimir.Enabled := True;
        dbListNI.SetFocus;
        BuscarDetalle(spObtenerListadoNotaInfractores.FieldByName('NumeroComprobante').AsInteger);
        btnBuscar.Enabled := False;
    end else begin
        btnImprimir.Enabled := False;
        MsgBox(MSG_NO_SE_ENCUENTRAN_DATOS, Self.Caption, MB_ICONINFORMATION);
        spObtenerListadoNotaInfractoresDetalle.Close;
        edNombre.Caption := '';
        edDomicilio.Caption := '';
    end;

end;

{-----------------------------------------------------------------------------
  Function Name: ValidarIngresos
  Author:  SGuastoni
  Date Created:
  Description:  Valida los datos ingresados por el usuario.
  Parameters:
  Return Value:
-----------------------------------------------------------------------------}
function TReimpresionNotaCobroInfractoresForm.validarIngresos: Boolean;
resourcestring
    MSG_FALTA_INGRESAR_DATOS = 'Debe ingresar al menos un filtro de busqueda';
    MSG_FECHA_INVALIDA = 'Ingrese una Fecha desde v�lida para realizar la b�squeda';
    MSG_FECHA_HASTA_INVALIDA= 'La Fecha ingresada no puede ser menor a la fecha desde';
begin
    Result := True;

        if (deEmiDesde.Date <> NullDate ) then begin

            if (deEmiDesde.Date < MINIMA_FECHA_VALIDA) then begin
                MsgBoxBalloon(MSG_FECHA_INVALIDA, Caption, MB_ICONWARNING, deEmiDesde);
                Result := False;
                Exit;
            end;
            if (deEmiHasta.Date <> NullDate ) then begin
                if deEmiDesde.Date > deEmiHasta.Date then begin
                    MsgBoxBalloon(MSG_FECHA_HASTA_INVALIDA, Caption, MB_ICONWARNING, deEmiHasta);
                    Result := False;
                    Exit;
                end;
            end;
        end;

        if (deVenDesde.Date <> NullDate) then begin
            if (deVenDesde.Date < MINIMA_FECHA_VALIDA) then begin
                MsgBoxBalloon(MSG_FECHA_INVALIDA, Caption, MB_ICONWARNING, deVenDesde);
                Result := False;
                Exit;
            end;
            if (deVenHasta.Date <> NullDate) then begin
                if deVenDesde.Date > deVenHasta.Date then begin
                    MsgBoxBalloon(MSG_FECHA_HASTA_INVALIDA, Caption, MB_ICONWARNING, deVenHasta);
                    Result := False;
                    Exit;
                end;
            end;
        end;

        if (Length(Trim(neRUT.Text))= 0) and
        (Length(Trim(neNI.Text))= 0)  and
        (deEmiDesde.Date = NullDate) and
        (deEmiHasta.Date = NullDate) and
        (deVenDesde.Date = NullDate) and
        (deVenHasta.Date = NullDate) then begin
            MsgBoxBalloon(MSG_FALTA_INGRESAR_DATOS, Caption, MB_ICONWARNING, btnBuscar );
            Result := False;
            exit;
        end;

end;

procedure TReimpresionNotaCobroInfractoresForm.spObtenerListadoNotaInfractoresAfterScroll(DataSet: TDataSet);
begin
    if spObtenerListadoNotaInfractores.Active then begin
        if (spObtenerListadoNotaInfractores.FieldByName('NumeroComprobante').AsInteger <> 0) then BuscarDetalle(spObtenerListadoNotaInfractores.FieldByName('NumeroComprobante').AsInteger);
    end;

end;

procedure TReimpresionNotaCobroInfractoresForm.neNIEnter(Sender: TObject);
begin
    btnBuscar.Enabled := True;
end;

procedure TReimpresionNotaCobroInfractoresForm.neRUTButtonClick(
  Sender: TObject);
var
    f: TFormBuscaClientes;
begin
    Application.CreateForm(TFormBuscaClientes,F);
  	if F.Inicializa(FUltimaBusqueda) then begin
    		F.ShowModal;
        if F.ModalResult = idOk then begin
            FUltimaBusqueda := F.UltimaBusqueda;
            neRUT.Text := f.Persona.NumeroDocumento;
		    end;
  	end;
    F.free;
end;

procedure TReimpresionNotaCobroInfractoresForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TReimpresionNotaCobroInfractoresForm.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TReimpresionNotaCobroInfractoresForm.dbListDetalleNIDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
    with Sender do begin
        if Column.FieldName = 'Importe' then begin
            Text := FormatFloat(FORMATO_IMPORTE , spObtenerListadoNotaInfractoresDetalle.FieldByName('Importe').AsFloat);
        end;
    end;
end;

procedure TReimpresionNotaCobroInfractoresForm.dbListNIDrawText(Sender: TCustomDBListEx;
  Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
  var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
  var DefaultDraw: Boolean);
begin
    with Sender do begin
        if Column.FieldName = 'TotalComprobante' then begin
            Text := FormatFloat(FORMATO_IMPORTE , spObtenerListadoNotaInfractores.FieldByName('TotalComprobante').AsFloat);
        end;
    end;
end;

end.
