unit frm_AutorizacionPagoNotasCredito;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, ListBoxEx, DBListEx,UtilProc,DmConnection,
  DB, ADODB, DmiCtrls, Validate, DateEdit,peaprocs,util,UtilDB,Abm_obj,RStrings,
  Peatypes, VariantComboBox,  DBClient, ImgList;

type
  TfrmAutorizacioPagoNotasCredito = class(TForm)
    Panel1: TPanel;
	GroupBox1: TGroupBox;
    Panel2: TPanel;
    dbl_NotasCredito: TDBListEx;
    spObtenerNotasCreditoAAutorizar: TADOStoredProc;
    Panel3: TPanel;
    Panel4: TPanel;
	Label1: TLabel;
	txt_FechaDesde: TDateEdit;
    txt_FechaHasta: TDateEdit;
    Label2: TLabel;
    dsNotasCreditoAAutorizar: TDataSource;
    cldsNotasCredito: TClientDataSet;
    cldsNotasCreditoTipoComprobante: TStringField;
    cldsNotasCreditoDescTipoComprobante: TStringField;
    cldsNotasCreditoNumeroComprobante: TLargeintField;
    cldsNotasCreditoFechaEmision: TDateTimeField;
    cldsNotasCreditoNumeroConvenio: TStringField;
    cldsNotasCreditoMonto: TStringField;
    cldsNotasCreditoNombreCliente: TStringField;
    cldsNotasCreditoAutorizado: TBooleanField;
    cldsNotasCreditoUsuario: TStringField;
    cldsNotasCreditoFechaAutorizacion: TDateTimeField;
    cldsNotasCreditoEstabaAutorizado: TBooleanField;
    Imagenes: TImageList;
    Label3: TLabel;
    spAutorizarPagoNotaCredito: TADOStoredProc;
    btn_Filtrar: TButton;
    btn_Limpiar: TButton;
    BtnSalir: TButton;
    btnGuardarCambios: TButton;
    cldsNotasCreditoMontoIndex: TCurrencyField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
	procedure btn_FiltrarClick(Sender: TObject);
    procedure btn_LimpiarClick(Sender: TObject);
    procedure dbl_NotasCreditoDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: String; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure dbl_NotasCreditoDblClick(Sender: TObject);
    procedure btnGuardarCambiosClick(Sender: TObject);
    procedure dbl_NotasCreditoColumns0HeaderClick(Sender: TObject);
  private
	{ Private declarations }
    FPuntoEntrega, FPuntoVenta: Integer;
    function HayRegistrosModificados : boolean;
    function ValidarDatosTurno: Boolean;
  public
	{ Public declarations }
	function Inicializar(PuntoEntrega, PuntoVenta: Integer): Boolean;
  end;

var
  frmAutorizacioPagoNotasCredito: TfrmAutorizacioPagoNotasCredito;

implementation

uses Main;

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    flamas
  Date Created: 24/05/2005
  Description: Inicializa el formulario
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TfrmAutorizacioPagoNotasCredito.Inicializar(PuntoEntrega, PuntoVenta: Integer): Boolean;
resourcestring
	MSG_ERROR_INITIALIZING = 'Error inicializando el formulario de Autorizaciones';
Var S: TSize;
begin
	result := True;
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
    try
        FPuntoEntrega := PuntoEntrega;
        FPuntoVenta := PuntoVenta;

		cldsNotasCredito.CreateDataSet;
		cldsNotasCredito.Open;
        // Crea un �ndice Descendente por Numero de Comprobante
        cldsNotasCredito.AddIndex('Index1', 'NumeroComprobante', [],'NumeroComprobante','',0);
        cldsNotasCredito.IndexName := 'Index1';
	except
	   	on e: exception do begin
			MsgBoxErr(MSG_ERROR_INITIALIZING, e.message, caption, MB_ICONERROR);
			result := False;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: BtnSalirClick
  Author:    flamas
  Date Created: 24/05/2005
  Description: Cierra el formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmAutorizacioPagoNotasCredito.BtnSalirClick(Sender: TObject);
begin
	Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    flamas
  Date Created: 24/05/2005
  Description:
  Parameters: Sender: TObject; var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmAutorizacioPagoNotasCredito.FormClose(Sender: TObject;  var Action: TCloseAction);
begin
	 action := caFree;
end;

{-----------------------------------------------------------------------------
  Function Name: btn_FiltrarClick
  Author:    flamas
  Date Created: 24/05/2005
  Description:	Carga las Notas de Cr�dito
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmAutorizacioPagoNotasCredito.btn_FiltrarClick(Sender: TObject);
resourcestring
    MSG_ERROR_LOADING_INVOICES 	= 'Error cargando Notas de Cr�dito';
begin
	// Valida que la fecha Desde no sea mayor que la fecha Hasta
	if (txt_FechaDesde.Date<>nulldate) and (txt_FechaHasta.Date<>nulldate) and (txt_FechaDesde.Date>txt_FechaHasta.Date) then begin
        MsgBoxBalloon(MSG_VALIDAR_ORDEN_FECHA,Caption,MB_ICONSTOP,txt_FechaDesde);
        exit;
    end;

	try
		btnGuardarCambios.Enabled := False;
		cldsNotasCredito.DisableControls;
        cldsNotasCredito.EmptyDataSet;
        // Ejecuta el SP que trae los datos
		with spObtenerNotasCreditoAAutorizar do begin
        	close;
        	Parameters.Refresh;
			Parameters.ParamByName('@FechaDesde').Value		:= iif(txt_FechaDesde.Date=nulldate,null,txt_FechaDesde.Date);
			Parameters.ParamByName('@FechaHasta').Value		:= iif(txt_FechaHasta.Date=nulldate,null,txt_FechaHasta.Date);
		end;

		if not(OpenTables([spObtenerNotasCreditoAAutorizar])) then
	       	MsgBox(MSG_ERROR_LOADING_INVOICES,Caption, MB_ICONERROR)
        else begin
        	// Carga el ClientDataSet con los Datos
        	while not spObtenerNotasCreditoAAutorizar.EOF do begin
                cldsNotasCredito.Insert;
                cldsNotasCreditoAutorizado.AsBoolean		  	:= spObtenerNotasCreditoAAutorizar.FieldByName('Autorizado').AsBoolean;
                cldsNotasCreditoEstabaAutorizado.AsBoolean	  	:= spObtenerNotasCreditoAAutorizar.FieldByName('Autorizado').AsBoolean;
                cldsNotasCreditoTipoComprobante.AsString 	  	:= spObtenerNotasCreditoAAutorizar.FieldByName('TipoComprobante').AsString;
                cldsNotasCreditoDescTipoComprobante.AsString  	:= spObtenerNotasCreditoAAutorizar.FieldByName('DescTipoComprobante').AsString;
                cldsNotasCreditoNumeroComprobante.Value   	    := spObtenerNotasCreditoAAutorizar.FieldByName('NumeroComprobante').Value;
                cldsNotasCreditoNumeroConvenio.AsString 	  	:= spObtenerNotasCreditoAAutorizar.FieldByName('NumeroConvenio').AsString;
                cldsNotasCreditoFechaEmision.AsDateTime 	  	:= spObtenerNotasCreditoAAutorizar.FieldByName('FechaEmision').AsDateTime;
                cldsNotasCreditoMonto.AsString 				  	:= spObtenerNotasCreditoAAutorizar.FieldByName('Monto').AsString;
                cldsNotasCreditoMontoIndex.AsCurrency  		  	:= spObtenerNotasCreditoAAutorizar.FieldByName('MontoIndex').Value;
                cldsNotasCreditoUsuario.AsString			  	:= spObtenerNotasCreditoAAutorizar.FieldByName('Usuario').AsString;
                cldsNotasCreditoFechaAutorizacion.AsString	  	:= spObtenerNotasCreditoAAutorizar.FieldByName('FechaAutorizacion').AsString;
                cldsNotasCreditoNombreCliente.AsString		  	:= spObtenerNotasCreditoAAutorizar.FieldByName('NombreCliente').AsString;
				cldsNotasCredito.Post;
				spObtenerNotasCreditoAAutorizar.Next;
            end;
        end;
	finally
		cldsNotasCredito.EnableControls;
    end;
	dbl_NotasCredito.SetFocus;
end;

{-----------------------------------------------------------------------------
  Function Name: btn_LimpiarClick
  Author:    flamas
  Date Created: 24/05/2005
  Description: Limpia los campos
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmAutorizacioPagoNotasCredito.btn_LimpiarClick(Sender: TObject);
begin
	txt_FechaDesde.Clear;
	txt_FechaHasta.Clear;
	spObtenerNotasCreditoAAutorizar.close;
	txt_FechaDesde.SetFocus;
end;


{-----------------------------------------------------------------------------
  Function Name: dbl_NotasCreditoDrawText
  Author:    flamas
  Date Created: 24/05/2005
  Description: Grafica el tilde
  Parameters: Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState; var Text: String; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmAutorizacioPagoNotasCredito.dbl_NotasCreditoDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: String; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
var
	bmp: TBitMap;
begin
    with Sender do begin
        if (Column.FieldName = 'Autorizado') then begin
            Text := '';
            canvas.FillRect(rect);
            bmp := TBitMap.Create;
            if (cldsNotasCredito.FieldByName('Autorizado').AsBoolean ) then
          	    Imagenes.GetBitmap(0, Bmp)
            else
                Imagenes.GetBitmap(1, Bmp);

            canvas.Draw((rect.Left + rect.Right - Bmp.Width) div 2, rect.Top,bmp);
            bmp.Free;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: dbl_NotasCreditoDblClick
  Author:    flamas
  Date Created: 24/05/2005
  Description: Autoriza o Desautoriza un Pago
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmAutorizacioPagoNotasCredito.dbl_NotasCreditoDblClick(Sender: TObject);
begin
	if cldsNotasCredito.IsEmpty or cldsNotasCreditoEstabaAutorizado.AsBoolean then Exit;

	with cldsNotasCredito do begin
    	Edit;
        FieldByName( 'Autorizado' ).AsBoolean := not FieldByName( 'Autorizado' ).AsBoolean;
		FieldByName( 'Usuario' ).AsString := UsuarioSistema;
		FieldByName( 'FechaAutorizacion' ).AsDateTime := StrToDate(DateToStr(NowBase(DMConnections.BaseCAC)));
        Post;
    end;

    btnGuardarCambios.Enabled := HayRegistrosModificados;
end;

{-----------------------------------------------------------------------------
  Function Name: HayRegistrosModificados
  Author:    flamas
  Date Created: 24/05/2005
  Description: Determina si hay registros que han sido Modificados;
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
function TfrmAutorizacioPagoNotasCredito.HayRegistrosModificados : boolean;
var
    Bmk : TBookmark;
begin
	result := False;
	with cldsNotasCredito do begin
		if not IsEmpty then begin
            Bmk := GetBookmark;
            DisableControls;
            First;
            while not EOF do begin
            	if (cldsNotasCreditoAutorizado.AsBoolean <> cldsNotasCreditoEstabaAutorizado.AsBoolean) then begin
                	Result := True;
                	Break;
                end;
            	Next;
            end;

            GotoBookmark(Bmk);
            EnableControls;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnGuardarCambiosClick
  Author:    flamas
  Date Created: 24/05/2005
  Description: Graba los cambios en la base de datos
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmAutorizacioPagoNotasCredito.btnGuardarCambiosClick(Sender: TObject);
resourcestring
	MSG_ERROR_AUTHORIZING 		= 'Error al autorizar el Pago de la Nota de Cr�dito %d';
    MSG_CONFIRM_AUTHORIZATIONS 	= 'Desea guardar las Autorizaciones realizadas ?';
    MSG_AUTHORIZATION_COMPLETED = 'Los pagos se autorizaron correctamente' + crlf +
    								'Desea continuar realizando autorizaciones ?';
var
    Bmk    : TBookmark;
    fError : boolean;
begin
	if (MsgBox(MSG_CONFIRM_AUTHORIZATIONS, Caption, MB_YESNO + MB_ICONQUESTION) <> ID_YES) then Exit;

    if not ValidarDatosTurno then Exit;

    if not cldsNotasCredito.IsEmpty then begin
		with cldsNotasCredito do begin
			fError := False;
            Bmk := GetBookmark;
            DisableControls;
            First;
            while not EOF do begin
            	if (cldsNotasCreditoAutorizado.AsBoolean <> cldsNotasCreditoEstabaAutorizado.AsBoolean) then begin
                	try
						// Actualiza las Autorizaciones
                    	spAutorizarPagoNotaCredito.Parameters.ParamByName('@TipoComprobante').Value 		:= cldsNotasCreditoTipoComprobante.Value;
                        spAutorizarPagoNotaCredito.Parameters.ParamByName('@NumeroComprobante').Value 		:= cldsNotasCreditoNumeroComprobante.Value;
                        spAutorizarPagoNotaCredito.Parameters.ParamByName('@Autorizado').Value 				:= cldsNotasCreditoAutorizado.Value;
                        spAutorizarPagoNotaCredito.Parameters.ParamByName('@CodigoUsuario').Value 			:= cldsNotasCreditoUsuario.Value;
                        spAutorizarPagoNotaCredito.Parameters.ParamByName('@FechaHoraActualizacion').Value 	:= cldsNotasCreditoFechaAutorizacion.Value;
                        spAutorizarPagoNotaCredito.ExecProc;

    					// Marca e� Registro como no modificado
	   					Edit;
        				FieldByName( 'EstabaAutorizado' ).AsBoolean := FieldByName( 'Autorizado' ).AsBoolean;
        				Post;
                    except
                    	on e: exception do begin
							MsgBoxErr(Format(MSG_ERROR_AUTHORIZING, [cldsNotasCreditoNumeroComprobante.AsInteger]), e.message, caption, MB_ICONERROR);
                            fError := True;
                            Break;
                        end;
                    end;
                end;
            	Next;
            end;
            GotoBookmark(Bmk);
            EnableControls;
        end;
        
		if not fError then begin
           	if (MsgBox(MSG_AUTHORIZATION_COMPLETED, Caption, MB_YESNO + MB_ICONQUESTION) <> ID_YES) then Close;
        end;
    end;

    btnGuardarCambios.Enabled := HayRegistrosModificados;
end;

{-----------------------------------------------------------------------------
  Function Name: dbl_NotasCreditoColumns0HeaderClick
  Author:    flamas
  Date Created: 24/05/2005
  Description:	Ordena Por Columnas
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmAutorizacioPagoNotasCredito.dbl_NotasCreditoColumns0HeaderClick(Sender: TObject);
begin
	if cldsNotasCredito.IsEmpty then Exit;

	if TDBListExColumn(sender).Sorting = csAscending then
        TDBListExColumn(sender).Sorting := csDescending
   	else TDBListExColumn(sender).Sorting := csAscending;

    cldsNotasCredito.IndexName := '';
	cldsNotasCredito.DeleteIndex('Index1');
	if (TDBListExColumn(sender).Sorting = csAscending) then begin
        if TDBListExColumn(Sender).FieldName = 'Monto' then
           	cldsNotasCredito.AddIndex('Index1', 'MontoIndex', [])
        else cldsNotasCredito.AddIndex('Index1', TDBListExColumn(Sender).FieldName, [])
    end else begin
        if TDBListExColumn(Sender).FieldName = 'Monto' then
            cldsNotasCredito.AddIndex('Index1', 'MontoIndex', [], 'MontoIndex')
        else cldsNotasCredito.AddIndex('Index1', TDBListExColumn(Sender).FieldName, [], TDBListExColumn(Sender).FieldName);
    end;
    cldsNotasCredito.IndexName := 'Index1';
end;

function TfrmAutorizacioPagoNotasCredito.ValidarDatosTurno: Boolean;
var
    DatosTurno: TDatosTurno;
begin
    (* Verificar que el turno que se est� por utilizar est� abierto y sea del
    usuario logueado. *)
    if VerTurnoAbierto(DMConnections.BaseCAC, FPuntoEntrega, FPuntoVenta, UsuarioSistema, DatosTurno) then begin
        GNumeroTurno := DatosTurno.NumeroTurno;
        Result := True;
    end else begin
        if (DatosTurno.NumeroTurno <> -1)
                and ((DatosTurno.Estado = TURNO_ESTADO_ABIERTO)
                and (Trim(DatosTurno.CodigoUsuario) <> (UpperCase(UsuarioSistema)))) then begin
            MsgBox(Format(MSG_ERROR_TURNO_ABIERTO_OTRO_USUARIO, [DatosTurno.CodigoUsuario]),
                Self.Caption, MB_ICONSTOP);
        end else begin
            MsgBox(MSG_ERROR_TURNO_CERRADO, Self.Caption, MB_ICONSTOP);
        end;
        Result := False;
    end;
end;

end.



