object FormConfigurarPuesto: TFormConfigurarPuesto
  Left = 280
  Top = 169
  BorderStyle = bsDialog
  Caption = 'Configuraci'#243'n del Puesto de Trabajo'
  ClientHeight = 245
  ClientWidth = 517
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object gb_Antena: TGroupBox
    Left = 8
    Top = 148
    Width = 489
    Height = 57
    Caption = ' Antena de TAG '
    TabOrder = 0
    object Label1: TLabel
      Left = 11
      Top = 25
      Width = 49
      Height = 13
      Caption = 'Biblioteca:'
    end
    object txt_bibLIC: TEdit
      Left = 68
      Top = 24
      Width = 269
      Height = 21
      TabOrder = 0
    end
    object btn_setupLIC: TDPSButton
      Left = 342
      Top = 22
      Caption = 'Con&figurar ...'
      TabOrder = 1
      OnClick = btn_setupLICClick
    end
  end
  object btn_Aceptar: TDPSButton
    Left = 342
    Top = 212
    Caption = '&Aceptar'
    Default = True
    TabOrder = 1
    OnClick = btn_AceptarClick
  end
  object btn_Cancelar: TDPSButton
    Left = 422
    Top = 212
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 2
  end
  object gb_PuestoDeTrabajo: TGroupBox
    Left = 8
    Top = 16
    Width = 489
    Height = 117
    Caption = ' Puesto de Trabajo '
    TabOrder = 3
    object Label2: TLabel
      Left = 16
      Top = 27
      Width = 110
      Height = 13
      Caption = 'Operadores Log'#237'sticos:'
    end
    object Label3: TLabel
      Left = 16
      Top = 74
      Width = 77
      Height = 13
      Caption = 'Punto de Venta:'
    end
    object Label4: TLabel
      Left = 16
      Top = 49
      Width = 76
      Height = 13
      Caption = 'Lugar de Venta:'
    end
    object cbOperadoresLogisticos: TComboBox
      Left = 131
      Top = 19
      Width = 318
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
      OnChange = cbOperadoresLogisticosChange
    end
    object cbPuntosVenta: TComboBox
      Left = 131
      Top = 68
      Width = 318
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 2
    end
    object cbLugaresVenta: TComboBox
      Left = 131
      Top = 43
      Width = 318
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 1
      OnChange = cbLugaresVentaChange
    end
  end
  object LIC: TTagReader
    Active = False
    DLL = 'COMBTECH.DLL'
    ID = 1
    PollingMode = pmSinglethreaded
    Left = 5
    Top = 211
  end
end
