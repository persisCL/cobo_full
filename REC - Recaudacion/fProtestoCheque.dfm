object frmProtestoCheque: TfrmProtestoCheque
  Left = 271
  Top = 116
  BorderStyle = bsSingle
  Caption = 'Anulaci'#243'n de Cheques Protestados'
  ClientHeight = 553
  ClientWidth = 644
  Color = clBtnFace
  Constraints.MinHeight = 578
  Constraints.MinWidth = 650
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poOwnerFormCenter
  Visible = True
  OnClose = FormClose
  DesignSize = (
    644
    553)
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 7
    Top = 159
    Width = 628
    Height = 298
    Anchors = [akLeft, akTop, akRight, akBottom]
  end
  object Label4: TLabel
    Left = 16
    Top = 165
    Width = 42
    Height = 13
    Caption = 'Cheques'
  end
  object Label5: TLabel
    Left = 15
    Top = 314
    Width = 110
    Height = 13
    Caption = 'Pagos a Comprobantes'
  end
  object dblCheques: TDBListEx
    Left = 11
    Top = 184
    Width = 322
    Height = 164
    Anchors = [akLeft, akTop, akRight]
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 100
        Header.Caption = 'Nro. Cheque'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'ChequeNumero'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 100
        Header.Caption = 'Fecha'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'ChequeFecha'
      end
      item
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 100
        Header.Caption = 'Importe'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taRightJustify
        IsLink = False
        FieldName = 'ChequeMonto'
      end>
    DataSource = dsLIstaCheques
    DragReorder = True
    ParentColor = False
    TabOrder = 1
    TabStop = True
    OnClick = dblChequesClick
    OnDrawText = dblChequesDrawText
  end
  object btnSalir: TButton
    Left = 561
    Top = 520
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Salir'
    TabOrder = 6
    OnClick = btnSalirClick
    ExplicitLeft = 562
  end
  object btnAnular: TButton
    Left = 10
    Top = 519
    Width = 205
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = '&Anular'
    Enabled = False
    TabOrder = 5
    OnClick = btnAnularClick
  end
  object GroupBox1: TGroupBox
    Left = 7
    Top = 1
    Width = 628
    Height = 152
    Anchors = [akLeft, akTop, akRight]
    Caption = 'B'#250'squeda de Cheque'
    TabOrder = 0
    object Label1: TLabel
      Left = 9
      Top = 21
      Width = 72
      Height = 13
      Caption = 'RUT del Titular'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 8
      Top = 75
      Width = 65
      Height = 13
      Caption = 'Banco Emisor'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 320
      Top = 75
      Width = 77
      Height = 13
      Caption = 'N'#250'mero Cheque'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Bevel2: TBevel
      Left = 6
      Top = 16
      Width = 315
      Height = 47
    end
    object Bevel3: TBevel
      Left = 324
      Top = 16
      Width = 293
      Height = 47
    end
    object Label15: TLabel
      Left = 338
      Top = 40
      Width = 31
      Height = 13
      Caption = 'Desde'
    end
    object Label16: TLabel
      Left = 479
      Top = 40
      Width = 28
      Height = 13
      Caption = 'Hasta'
    end
    object Label17: TLabel
      Left = 339
      Top = 18
      Width = 87
      Height = 13
      Caption = 'Fecha del Cheque'
    end
    object Bevel4: TBevel
      Left = 4
      Top = 68
      Width = 613
      Height = 47
    end
    object peNumeroDocumento: TPickEdit
      Left = 9
      Top = 36
      Width = 296
      Height = 21
      CharCase = ecUpperCase
      Enabled = True
      TabOrder = 0
      EditorStyle = bteTextEdit
      OnButtonClick = peNumeroDocumentoButtonClick
    end
    object cbbancos: TVariantComboBox
      Left = 8
      Top = 88
      Width = 305
      Height = 21
      Style = vcsDropDownList
      Color = clWhite
      ItemHeight = 13
      TabOrder = 3
      Items = <>
    end
    object deFechaDesde: TDateEdit
      Left = 372
      Top = 36
      Width = 94
      Height = 21
      AutoSelect = False
      TabOrder = 1
      OnExit = deFechaDesdeExit
      Date = -693594.000000000000000000
    end
    object deFEchaHasta: TDateEdit
      Left = 512
      Top = 36
      Width = 94
      Height = 21
      AutoSelect = False
      TabOrder = 2
      Date = -693594.000000000000000000
    end
    object neNumeroCheque: TEdit
      Left = 320
      Top = 89
      Width = 291
      Height = 21
      MaxLength = 20
      TabOrder = 4
      OnKeyPress = neNumeroChequeKeyPress
    end
    object btnBuscar: TButton
      Left = 542
      Top = 118
      Width = 75
      Height = 25
      Caption = '&Buscar'
      TabOrder = 5
      OnClick = btnBuscarClick
    end
  end
  object DBListEx2: TDBListEx
    Left = 11
    Top = 354
    Width = 620
    Height = 100
    Anchors = [akLeft, akTop, akRight, akBottom]
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 80
        Header.Caption = 'Nro. Pago'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'NumeroPago'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 150
        Header.Caption = 'Tipo Comprobante'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'DescTipoComp'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 150
        Header.Caption = 'N'#250'mero Comprobante'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'NumeroComprobante'
      end
      item
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 150
        Header.Caption = 'Importe'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'DescImporte'
      end>
    DataSource = dsComprobantes
    DragReorder = False
    ParentColor = False
    TabOrder = 2
    TabStop = True
  end
  object GroupBox2: TGroupBox
    Left = 341
    Top = 161
    Width = 290
    Height = 187
    Anchors = [akTop, akRight]
    Caption = 'Datos del Cheque Seleccionado:'
    TabOrder = 3
    object Label6: TLabel
      Left = 5
      Top = 16
      Width = 41
      Height = 13
      Caption = 'Titular:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 5
      Top = 31
      Width = 69
      Height = 13
      Caption = 'Documento:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 5
      Top = 46
      Width = 41
      Height = 13
      Caption = 'Banco:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label9: TLabel
      Left = 5
      Top = 61
      Width = 72
      Height = 13
      Caption = 'Nro.Cheque:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label10: TLabel
      Left = 5
      Top = 76
      Width = 40
      Height = 13
      Caption = 'Fecha:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label11: TLabel
      Left = 5
      Top = 91
      Width = 40
      Height = 13
      Caption = 'Monto:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblTitular: TLabel
      Left = 80
      Top = 16
      Width = 204
      Height = 13
      AutoSize = False
      Caption = 'Titular:'
    end
    object lblTipoDocumento: TLabel
      Left = 80
      Top = 31
      Width = 36
      Height = 13
      AutoSize = False
      Caption = 'Tipo'
    end
    object lblBanco: TLabel
      Left = 80
      Top = 46
      Width = 204
      Height = 13
      AutoSize = False
      Caption = 'Banco:'
    end
    object lblNumeroCheque: TLabel
      Left = 80
      Top = 61
      Width = 204
      Height = 13
      AutoSize = False
      Caption = 'Nro.:'
    end
    object lblFecha: TLabel
      Left = 80
      Top = 76
      Width = 204
      Height = 13
      AutoSize = False
      Caption = 'Fecha:'
    end
    object lblMonto: TLabel
      Left = 80
      Top = 91
      Width = 204
      Height = 13
      AutoSize = False
      Caption = 'Monto:'
    end
    object lblNumeroDocumento: TLabel
      Left = 119
      Top = 31
      Width = 164
      Height = 13
      AutoSize = False
      Caption = 'NumeroDoc'
    end
    object lblEstado2: TLabel
      Left = 5
      Top = 136
      Width = 44
      Height = 13
      Caption = 'Estado:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblEstado: TLabel
      Left = 80
      Top = 136
      Width = 43
      Height = 13
      Caption = 'lblEstado'
    end
    object lblMensaje: TLabel
      Left = 6
      Top = 149
      Width = 278
      Height = 28
      Alignment = taCenter
      AutoSize = False
      Caption = 'Mensaje'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblRecibo: TLabel
      Left = 6
      Top = 106
      Width = 45
      Height = 13
      Caption = 'Recibo:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblMontoRecibo: TLabel
      Left = 6
      Top = 121
      Width = 84
      Height = 13
      Caption = 'Monto Recibo:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblRecibo2: TLabel
      Left = 80
      Top = 106
      Width = 34
      Height = 13
      Caption = 'Recibo'
    end
    object lblMontoRecibo2: TLabel
      Left = 96
      Top = 121
      Width = 65
      Height = 13
      Caption = 'MontoREcibo'
    end
  end
  object gbAnulacion: TGroupBox
    Left = 6
    Top = 456
    Width = 629
    Height = 54
    Anchors = [akLeft, akRight, akBottom]
    Caption = ' Anulaci'#243'n '
    Enabled = False
    TabOrder = 4
    DesignSize = (
      629
      54)
    object Label12: TLabel
      Left = 10
      Top = 22
      Width = 100
      Height = 13
      Anchors = [akLeft, akBottom]
      Caption = 'Fecha Anulaci'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label13: TLabel
      Left = 240
      Top = 22
      Width = 103
      Height = 13
      Caption = 'Motivo Anulaci'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object deFechaAnulacion: TDateEdit
      Left = 115
      Top = 18
      Width = 94
      Height = 21
      AutoSelect = False
      TabOrder = 0
      Date = -693594.000000000000000000
    end
    object cbMotivosAnulacion: TVariantComboBox
      Left = 348
      Top = 18
      Width = 267
      Height = 21
      Style = vcsDropDownList
      Anchors = [akLeft, akTop, akRight]
      ItemHeight = 13
      TabOrder = 1
      Items = <>
    end
  end
  object spObtenerListaCheques: TADOStoredProc
    Connection = DMConnections.BaseCAC
    AfterOpen = spObtenerListaChequesAfterOpen
    AfterClose = spObtenerListaChequesAfterClose
    ProcedureName = 'ObtenerChequesPendientes;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodBanco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroCheque'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@NumRUT'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@FechaDesde'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaHasta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 46
    Top = 166
  end
  object spObtenerListaComprobantes: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerComprobantesPorCheque'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@ChequeCodigoBanco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ChequeNumero'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@ChequeTitular'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@ChequeFecha'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@ChequeCodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@ChequeNumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@ChequeMonto'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 10
        Value = Null
      end>
    Left = 35
    Top = 298
  end
  object dsLIstaCheques: TDataSource
    DataSet = spObtenerListaCheques
    Left = 124
    Top = 166
  end
  object dsComprobantes: TDataSource
    DataSet = spObtenerListaComprobantes
    Left = 64
    Top = 296
  end
  object spAnularCheque: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'AnularCheque;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@ChequeCodigoBanco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ChequeNumero'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@ChequeTitular'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@ChequeFecha'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@ChequeCodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@ChequeNumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@ChequeMonto'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaAnulacion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoMotivoAnulacion'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@NumeroMovimiento'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end>
    Left = 224
    Top = 512
  end
  object spObtenerMotivosAnulacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerMotivosAnulacionPagos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@OrigenPedido'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 371
    Top = 504
  end
  object spCrearNotaDebitoDK: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'CrearNotaDebitoDK;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Importe'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoMedioPago'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@NumeroComprobanteNIPagado'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@NumeroNotaDebitoDK'
        Attributes = [paNullable]
        DataType = ftLargeint
        Direction = pdInputOutput
        Precision = 19
        Value = Null
      end
      item
        Name = '@NumeroMovimiento'
        Attributes = [paNullable]
        DataType = ftLargeint
        Direction = pdOutput
        Precision = 19
        Value = Null
      end>
    Left = 263
    Top = 512
  end
end
