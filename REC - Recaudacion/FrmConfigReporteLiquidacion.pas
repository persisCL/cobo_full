unit FrmConfigReporteLiquidacion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DB, ADODB,Util, UtilDb, ExtCtrls, DPSControls, DmConnection, UtilProc;

type
  TFormConfigReporteLiquidacion = class(TForm)
    Label2: TLabel;
    Label1: TLabel;
    cb_Turnos: TComboBox;
    btn_Aceptar: TDPSButton;
    btn_Salir: TDPSButton;
    Bevel1: TBevel;
    cb_Usuarios: TComboBox;
    UsuariosTurnosAbierto: TADOStoredProc;
	procedure cb_UsuariosChange(Sender: TObject);
    procedure cb_TurnosChange(Sender: TObject);
  private
	{ Private declarations }
	FLiquidados: Boolean;
	procedure SetCodigoUsuario(const Value: AnsiString);
	function  GetCodigoUsuario: AnsiString;
	function GetNumeroTurno: Integer;
	procedure RevisarBotones;
    procedure SetNumeroTurno(const Value: Integer);
  public
	{ Public declarations }
	function Inicializa(Liquidados: Boolean = True): Boolean;
	property NumeroTurno: Integer read GetNumeroTurno Write SetNumeroTurno;
	property CodigoUsuario: AnsiString read GetCodigoUsuario write SetCodigoUsuario;
  end;

var
  FormConfigReporteLiquidacion: TFormConfigReporteLiquidacion;

implementation

{$R *.dfm}

{ TForm1 }

function TFormConfigReporteLiquidacion.Inicializa(Liquidados: Boolean = True): Boolean;
var
    CodigoUsrAnt: String;
begin
	Result := False;
	FLiquidados := Liquidados;
	if not Opentables([UsuariosTurnosAbierto])then Exit;
    CodigoUsrAnt := '';
	While not UsuariosTurnosAbierto.Eof do begin
        if CodigoUsrAnt <> UsuariosTurnosAbierto.FieldByName('CodigoUsuario').AsString then begin
    		cb_Usuarios.Items.Add(
	    	  PadR(UsuariosTurnosAbierto.FieldByName('NombreCompleto').AsString, 300, ' ') +
		      PadR(UsuariosTurnosAbierto.FieldByName('CodigoUsuario').AsString, 20, ' '));
              CodigoUsrAnt := UsuariosTurnosAbierto.FieldByName('CodigoUsuario').AsString;
        end;
		UsuariosTurnosAbierto.Next;
	end;
    if ((UsuariosTurnosAbierto.Active) and (UsuariosTurnosAbierto.RecordCount > 0)) then begin
        cb_Usuarios.ItemIndex := 0;
        UsuariosTurnosAbierto.Close;
        cb_UsuariosChange(nil);
    end
    else begin
        if ((UsuariosTurnosAbierto.Active) and not (UsuariosTurnosAbierto.RecordCount > 0)) then begin
            MsgBox('No hay turnos para liquidar.', 'Atenci�n', MB_ICONINFORMATION);
            Result := False;
            Exit;
        end;
    end;
    Result := True;
end;

procedure TFormConfigReporteLiquidacion.cb_UsuariosChange(Sender: TObject);
resourcestring
    MSG_APERTURA = 'Apertura: ';
    MSG_ABIERTO  = 'Abierto';
    MSG_CIERRE   = 'Cierre: ';
    MSG_TURNO    = 'Turno ';
var
	Ok: Boolean;
begin
	cb_Turnos.Items.Clear;
    if not UsuariosTurnosAbierto.Active then begin
        UsuariosTurnosAbierto.Parameters.ParamByName('@CodigoUsuario').Value := CodigoUsuario;
        if not Opentables([UsuariosTurnosAbierto]) then Exit;
        While not UsuariosTurnosAbierto.Eof do begin
            Ok := iif(UsuariosTurnosAbierto.FieldByName('FechaHoraLiquidacion').IsNull,
              not FLiquidados, FLiquidados);
            if Ok then begin
                cb_Turnos.Items.Add(
                  MSG_TURNO + UsuariosTurnosAbierto.FieldByName('NumeroTurno').AsString + '   '  +
                  PadR(MSG_APERTURA +  FormatDateTime('dd/mm/yyyy, hh:mm',
                  UsuariosTurnosAbierto.FieldByName('FechaHoraApertura').AsDateTime), 35, ' ') +
    			  PadR( iif(UsuariosTurnosAbierto.FieldByName('FechaHoraCierre').AsDateTime <= 0,
                        MSG_ABIERTO, MSG_CIERRE + FormatDateTime('dd/mm/yyyy, hh:mm',
		    	        UsuariosTurnosAbierto.FieldByName('FechaHoraCierre').AsDateTime)), 35, ' ') + ' ' +
                  PadL(Istr(UsuariosTurnosAbierto.FieldByName('NumeroTurno').AsInteger, 10), 200, ' ')
                  );

            end;
            UsuariosTurnosAbierto.Next;
        end;
        if ((UsuariosTurnosAbierto.Active) and (UsuariosTurnosAbierto.RecordCount > 0)) then begin
            cb_Turnos.ItemIndex := 0;
        end;
        UsuariosTurnosAbierto.Close;
    end;
	RevisarBotones;
end;

procedure TFormConfigReporteLiquidacion.SetCodigoUsuario(const Value: AnsiString);
Var
	i: Integer;
begin
	for i := 0 to cb_Usuarios.Items.Count - 1 do begin
		if UpperCase(Trim(StrRight(cb_Usuarios.Items[i], 20))) = UpperCase(Value) then begin
			cb_Usuarios.ItemIndex := i;
			cb_Usuarios.OnChange(cb_Usuarios);
			Exit;
		end;
	end;
end;

function TFormConfigReporteLiquidacion.GetCodigoUsuario: AnsiString;
begin
	Result := Trim(StrRight(cb_Usuarios.Text, 20));
end;

function TFormConfigReporteLiquidacion.GetNumeroTurno: Integer;
begin
	Result := IVal(StrRight(cb_Turnos.Text, 20));
end;

procedure TFormConfigReporteLiquidacion.cb_TurnosChange(Sender: TObject);
begin
	RevisarBotones;
end;

procedure TFormConfigReporteLiquidacion.RevisarBotones;
begin
	btn_Aceptar.Enabled := (cb_Usuarios.ItemIndex >= 0) and (cb_Turnos.ItemIndex >= 0);
end;

procedure TFormConfigReporteLiquidacion.SetNumeroTurno(const Value: Integer);
Var
	i: Integer;
begin
	for i := 0 to cb_Turnos.Items.Count - 1 do begin
		if IVal(StrRight(cb_Turnos.Items[i], 20)) = Value then begin
			cb_Turnos.ItemIndex := i;
			cb_Turnos.OnChange(cb_Turnos);
			Exit;
		end;
	end;
end;

end.
