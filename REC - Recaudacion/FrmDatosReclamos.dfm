object NavWindowDatosReclamos: TNavWindowDatosReclamos
  Left = 140
  Top = 152
  Width = 783
  Height = 504
  HorzScrollBar.Color = clBtnFace
  HorzScrollBar.ParentColor = False
  Caption = 'Ordenes de Servicio'
  Color = clBtnFace
  Constraints.MinHeight = 504
  Constraints.MinWidth = 750
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 249
    Width = 775
    Height = 8
    Cursor = crVSplit
    Align = alTop
  end
  object Grilla: TDPSGrid
    Left = 0
    Top = 0
    Width = 775
    Height = 249
    Align = alTop
    DataSource = ds_qry_OS
    Options = [dgTitles, dgColumnResize, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = GrillaDblClick
    OnLinkClick = GrillaLinkClick
    Columns = <
      item
        Expanded = False
        FieldName = 'VerCliente'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsUnderline]
        Title.Caption = 'Cliente'
        Title.Color = 16444382
        Width = 42
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'CodigoOrdenServicio'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsUnderline]
        Title.Caption = 'Orden Servicio'
        Title.Color = 16444382
        Width = 78
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Descripcion'
        Title.Color = 16444382
        Width = 201
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'EstadoOrden'
        Title.Caption = 'Estado'
        Title.Color = 16444382
        Width = 63
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FechaHoraCreacion'
        Title.Caption = 'Fecha creaci'#243'n'
        Title.Color = 16444382
        Width = 81
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FechaHoraFin'
        Title.Caption = 'Fecha finalizaci'#243'n'
        Title.Color = 16444382
        Width = 92
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'DescripcionPrioridad'
        Title.Caption = 'Prioridad'
        Title.Color = 16444382
        Width = 57
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FechaCompromiso'
        Title.Caption = 'Compromiso'
        Title.Color = 16444382
        Width = 134
        Visible = True
      end>
  end
  object Display: TWorkflowDisplay
    Left = 0
    Top = 257
    Width = 775
    Height = 220
    Align = alClient
    WorkflowID = 4
    ServiceOrder = 21
    Connection = DMConnections.BaseCAC
    WorkflowVersion = 0
  end
  object ds_qry_OS: TDataSource
    DataSet = qry_OSTodas
    OnDataChange = ds_qry_OSDataChange
    Left = 120
    Top = 376
  end
  object qry_OSTodas: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'CLIENTE'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'TipoContacto'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'Estado1'
        Attributes = [paSigned, paNullable]
        DataType = ftString
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'Estado2'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end>
    SQL.Strings = (
      'select'
      #9#39'Ver ...'#39'  As VerCliente,'
      #9'Comunicaciones.CodigoContacto AS CodigoCliente,'
      #9'OrdenesServicio.CodigoOrdenServicio,'
      #9'TiposOrdenServicio.Descripcion,'
      #9'OrdenesServicio.Estado,'
      #9'OrdenesServicio.FechaHoraCreacion,'
      #9'OrdenesServicio.FechaHoraFin,'
      #9'OrdenesServicio.Prioridad,'
      #9'OrdenesServicio.FechaCompromiso,'
      #9'OrdenesServicioWorkFlows.CodigoWorkflow,'
      #9'OrdenesServicioWorkFlows.Version,'
      
        '    dbo.PrioridadToString(OrdenesServicio.Prioridad) AS Descripc' +
        'ionPrioridad,'
      '    dbo.EstadoOSToString(OrdenesServicio.Estado) AS EstadoOrden'
      'From'
      #9'Comunicaciones,'
      #9'OrdenesServicio,'
      #9'TiposOrdenServicio,'
      #9'OrdenesServicioWorkflows,'
      #9'WorkFlows'
      'Where'
      #9'Comunicaciones.CodigoContacto = :CLIENTE and'
      #9'Comunicaciones.TipoContacto'#9'=   :TipoContacto and'
      
        #9'OrdenesServicio.CodigoComunicacion = Comunicaciones.CodigoComun' +
        'icacion and'
      
        '    ((:Estado1 Is null) or (Rtrim(OrdenesServicio.Estado) = RTri' +
        'm(:Estado2))) and    '
      
        #9'OrdenesServicio.TipoOrdenServicio = TiposOrdenServicio.TipoOrde' +
        'nServicio  and'
      
        #9'OrdenesServicio.CodigoOrdenServicio = OrdenesServicioWorkflows.' +
        'CodigoOrdenServicio and'
      
        #9'OrdenesServicioWorkflows.CodigoWorkFlow = WorkFlows.CodigoWorkf' +
        'low and'
      #9'OrdenesServicioWorkflows.Version = WorkFlows.VersionActual'
      ''
      'Order by OrdenesServicio.FechaHoraCreacion')
    Left = 160
    Top = 376
  end
end
