unit PagoVentanillaCastigo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, VariantComboBox, ADODB, DMConnection, ExtCtrls,
  Validate, DateEdit, DmiCtrls, Buttons, ComCtrls, Util, UtilProc, DB,
  PeaProcs, PeaTypes, UtilDB, ConstParametrosGenerales, RStrings, UtilFacturacion,
  ListBoxEx, DBListEx, FrmAgregarCheque, DBClient, Mask, TimeEdit, SysUtilsCN, XMLDoc, XMLIntf, Diccionario;

type
  TFormaPago = (tpCastigo);

  TfrmPagoVentanillaCastigo = class(TForm)
    Bevel1: TBevel;
    Bevel2: TBevel;
    btnAceptar: TButton;
    btnCerrar: TButton;
    cb_CanalesDePago: TVariantComboBox;
    cdsCheques: TClientDataSet;
    CerrarPagoComprobante: TADOStoredProc;
    CrearRecibo: TADOStoredProc;
    deFechaPago: TDateEdit;
    DsCheques: TDataSource;
    lbl_CanalDePago: TLabel;
    lbl_FormaPago: TLabel;
    lbl_Moneda: TLabel;
    lblFechaDePago: TLabel;
    neImporte: TNumericEdit;
    RegistrarPagoNotaCobroInfraccion: TADOStoredProc;
    sp_RegistrarDetallePagoComprobante: TADOStoredProc;
    sp_RegistrarPagoComprobante: TADOStoredProc;
    edtCodigoFormaPago: TEdit;
    edtDescFormaPago: TEdit;
    spActualizarInfraccionesPagadas: TADOStoredProc;
    spCastigarConvenio: TADOStoredProc;
    sp_ActualizarClienteMoroso: TADOStoredProc;
    lbl1: TLabel;
    spValidarFormaDePagoEnCanal: TADOStoredProc;
    sp_ValidarFormaDePagoEnCanal: TADOStoredProc;
    procedure btnAceptarClick(Sender: TObject);
    procedure btnCerrarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure chkNoCalcularInteresesClick(Sender: TObject);
    procedure cb_CanalesDePagoChange(Sender: TObject);
    procedure edtCodigoFormaPagoChange(Sender: TObject);
  private
    FCodigoCanalPago: Integer;
    FCodigoFormaPago: Integer;
    FCodigoPantalla:Integer;
    FComprobantesCobrar: TListaComprobante;
    FFormaPagoActual: TFormaPago;
    FImporte, FImportePagado: Double;
    FImporteComprobantesPagados: Int64;
    FImprimirRecibo : boolean;
    FNumeroPOS, FPuntoEntrega, FPuntoVenta: Integer;
    FNumeroRecibo: Int64;
    FPagoNotaCobroInfraccion: Boolean;
    FRUT : string;
    FTitular : string;
    FFechaHoraEmision : TDateTime;
    FNoCalcularIntereses: Boolean;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;
  	function CalcularImportePagado(i: Integer): Int64;
    function GenerarPago(NumeroRecibo: Integer; FormaPago: TFormaPago; var MensajeError: String): Boolean;
   	function ObtenerImporte(Comprobantes: TListaComprobante): Double;
   	function ValidarDatos(FormaPago: TFormaPago):Boolean;
  	function ValidarDatosTurno: Boolean;
  	procedure CerrarRegistroPagoComprobante(TipoComprobante: AnsiString; NumeroComprobante: Int64; FechaPago: TDateTime);

  public
    function ImprimirRecibo : Boolean;
    function Inicializar(ComprobantesCobrar: TListaComprobante; RUT, Titular: String; NumeroPOS, PuntoEntrega, PuntoVenta, CodigoCanalDePago: Integer; PagoNotaCobroInfraccion: Boolean = False): Boolean;
    function NumeroRecibo : int64;
    property CodigoCanalPago: Integer read FCodigoCanalPago;
    function GenerarPagoComprobante(FormaPago: TFormaPago; var MensajeError: String; var NumeroRecibo: Int64): Boolean;
  end;

var
  frm_PagoVentanillaCastigo: TfrmPagoVentanillaCastigo;

implementation

uses Contnrs;

{$R *.dfm}
{-----------------------------------------------------------------------------
  Function Name :   Inicializar
  Author        :   GLEON
  Date Created  :   29/11/2016
  Description   :   Inicializa formulario de pago de comprobantes por ventanilla
                    s�lo habilitado para forma de pago Castigo
  Parameters    :   ComprobantesCobrar, RUT, Titular, NumeroPOS, PuntoEntrega,
                    PuntoVenta, CodigoCanalDePago, PagoNotaCobroInfraccion
  Return Value  :   boolean
-----------------------------------------------------------------------------}
function TfrmPagoVentanillaCastigo.Inicializar(ComprobantesCobrar: TListaComprobante; RUT, Titular: String; NumeroPOS, PuntoEntrega, PuntoVenta, CodigoCanalDePago: Integer; PagoNotaCobroInfraccion: Boolean = False): Boolean;
resourcestring
    MSG_POS = 'POSNET : ';
    MSG_LISTA_COMPROBANTES_VACIA = 'No hay comprobantes para cobrar.';
    MSG_COBRO_COMPROBANTES = 'Cobro de Comprobantes';
const
    SQL_ObtenerTipoMedioPago = 'SELECT Descripcion FROM TipoMedioPago where CodigoTipoMedioPago = 10';
var
    TipoMedioPago : string;
begin
    Result := True;
    TipoMedioPago := '';
    //lblAcaMonto.Enabled := False;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));
    Color := FColorMenu;

    FPagoNotaCobroInfraccion := PagoNotaCobroInfraccion;
    FComprobantesCobrar := ComprobantesCobrar;

    if FComprobantesCobrar = nil then begin
        MsgBox(MSG_LISTA_COMPROBANTES_VACIA, Caption, MB_ICONSTOP);
        Result := False;
        Exit;
    end;

    FCodigoCanalPago := CodigoCanalDePago;
    FNumeroPOS := NumeroPOS;
    FPuntoEntrega := PuntoEntrega;
    FPuntoVenta := PuntoVenta;
	FImporteComprobantesPagados := 0;
    FNoCalcularIntereses := True;

    //nbOpciones.PageIndex := CONST_PAGE_CASTIGO;
    FCodigoPantalla := 11;


    CargarCanalesDePago(DMConnections.BaseCAC, cb_CanalesDePago, FCodigoCanalPago, 0);

    TipoMedioPago := QueryGetValue(DMConnections.BaseCAC, SQL_ObtenerTipoMedioPago, 0);
    cb_CanalesDePago.Items.IndexOf(TipoMedioPago);
    edtCodigoFormaPago.Text := 'CA';


    try
        deFechaPago.Date    := SysUtilsCN.NowBaseCN(DMConnections.BaseCAC);

        FFechaHoraEmision   := TDate(deFechaPago.Date);

        if FCodigoCanalPago < 0 then FCodigoCanalPago := 0;
        //CargarCanalesDePago(DMConnections.BaseCAC, cb_CanalesDePago, FCodigoCanalPago,0);
        //FiltrarCanalesDePago;
        //cb_CanalesDePagoChange(cb_CanalesDePago);
    except
        Result := False;
    end;

    if Result then begin
        FNumeroRecibo := 0;
        FImprimirRecibo := False;
    	FRUT := RUT;
        FTitular := Titular;
        FImporte := ObtenerImporte(FComprobantesCobrar);
        neImporte.Value := FImporte;
        FImportePagado := FImporte;
        
        FImprimirRecibo := False;

        if FComprobantesCobrar.Count > 1 then begin
            Caption := MSG_COBRO_COMPROBANTES;
        end else begin
            Caption := Caption + Space(1) + '-' + Space(1) +
                        DescriTipoComprobante( TComprobante(FComprobantesCobrar.Items[0]).TipoComprobante ) + ':' + Space (1) +
                        IntToStr(TComprobante(FComprobantesCobrar.Items[0]).NumeroComprobante);
        end;
        Caption := Caption + Space(1) + '-' + Space(1) + MSG_POS + IntToStr(NumeroPOS);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name :   btnAceptarClick
  Author        :
  Date Created  :
  Description   :   Registra el pago del comprobante e imprime el comprobante de cancelaci�n
  Parameters    :   Sender
  Return Value  :   None
-----------------------------------------------------------------------------}
procedure TfrmPagoVentanillaCastigo.btnAceptarClick(Sender: TObject);
resourcestring
    MSG_ERROR = 'Error. El Castigo no ha sido registrado.';
    MSG_FALTA_MONTO = 'Debe ingresar un monto';
    MSG_SELECCIONAR_FORMA_PAGO = 'Debe seleccionar una forma de pago';
    MSG_SELECCIONAR_CANAL_PAGO = 'Debe seleccionar un canal de pago';
    MSG_REGISTRO_CORRECTO = 'El Castigo se ha registrado correctamente';
    MSG_CONFIRMAR_RECIBO = 'Desea Imprimir el Comprobante de Cancelaci�n ?';
    MSG_CAPTION = 'Registrar Pago Castigo';
    MSG_CONFIRMAR_PAGO = 'Confirma registrar este pago Castigo ?';
    MSG_ERROR_PAGOS_FUTUROS = 'La fecha de pago no puede ser posterior a la fecha actual';
    MSG_ERROR_PAGOS_PASADOS = 'La fecha de pago no puede ser inferior a %d d�as.';
var
    NumeroRecibo: Int64;
    MensajeError: String;
    FFechaHoraEmision, FFechaActual: TDateTime;
    DiasFechaMinimaPago: integer;

begin
    try
        btnAceptar.Enabled := False;
        EmptyKeyQueue;
        Application.ProcessMessages;

        (* Verificar que se haya seleccionado el Canal y la Forma de Pago. *)
        if not ValidateControls([cb_CanalesDePago, edtCodigoFormaPago], // Rev. 1 (SS 809)
                [cb_CanalesDePago.ItemIndex >= 0, edtCodigoFormaPago.Tag = 1],
                MSG_CAPTION,
                [MSG_SELECCIONAR_CANAL_PAGO, MSG_SELECCIONAR_FORMA_PAGO, MSG_FALTA_MONTO]) then Exit;

        if not ValidarDatos(FFormaPagoActual) then Exit;

        if not ValidarDatosTurno then Exit;

        FFechaHoraEmision := TDate(deFechaPago.Date);

        FFechaActual := SysUtilsCN.NowBaseCN(DMConnections.BaseCAC);
        ObtenerParametroGeneral(DMConnections.BaseCAC, 'DIAS_FECHA_MINIMA_PAGO_VENTANILLA', DiasFechaMinimaPago);

        if FFechaHoraEmision > FFechaActual then begin
            MsgBoxBalloon(MSG_ERROR_PAGOS_FUTUROS, Caption, MB_ICONSTOP, deFechaPago);
            exit;
        end;

        if FFechaHoraEmision <= FFechaActual - DiasFechaMinimaPago then begin
            MsgBoxBalloon(Format(MSG_ERROR_PAGOS_PASADOS, [DiasFechaMinimaPago]), Caption, MB_ICONSTOP, deFechaPago);
            exit;
        end;

        if MsgBox(MSG_CONFIRMAR_PAGO, MSG_CAPTION, MB_ICONQUESTION or MB_DEFBUTTON2 or MB_YESNO) <> IDYES then Exit;

        try

            IF (GenerarPagoComprobante(FFormaPagoActual, MensajeError, NumeroRecibo)) then
            begin

                //FImprimirRecibo := True;
                FNumeroRecibo := NumeroRecibo;
                ModalResult := mrOk;
                cb_CanalesDePago.ItemIndex := 0;
            end
            else
            begin
                MsgBoxErr(MSG_ERROR, MensajeError, MSG_ERROR, MB_ICONERROR);
                ModalResult := mrAbort;
            end;

        except
            on e: Exception do begin
                MsgBoxErr(MSG_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
            end;
        end;

    finally
        EmptyKeyQueue;
        Application.ProcessMessages;
        if ModalResult <> mrOk then
            btnAceptar.Enabled := True;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name :   ValidarDatos
  Author        :
  Date Created  :
  Description   :
  Parameters    :   FormaPago
  Return Value  :   Boolean
-----------------------------------------------------------------------------}
function TfrmPagoVentanillaCastigo.ValidarDatos(FormaPago: TFormaPago): Boolean;
resourcestring
    MSG_IMPORTE_PAGADO_MENOR_A_CERO = 'El importe pagado debe ser mayor que cero.';
    MSG_CAPTION = 'Cobro de Comprobante';
    MSG_IMPORTE_RECIBIDO_INFERIOR_A_IMPORTE_PAGAR = 'El monto recibibo no puede ser menor que el monto a pagar.';

begin
    Result := False;

    //Validamos que lo que se desea pagar sea un monto al menos mayor a cero
    {
    if (FComprobantesCobrar.Count = 1) and (not ValidateControls([(FImportePagado > 0)])
      MSG_CAPTION, [MSG_IMPORTE_PAGADO_MENOR_A_CERO])) then begin
        Exit;
    end;
    }
    // S�lo resta pagar seg�n la forma elegida.

    case FormaPago of
        tpCastigo: begin

           Result   := True ;
        end
        else
           Result := False;
    end;

end;

{-----------------------------------------------------------------------------
  Function Name :   ImprimirRecibo
  Author        :
  Date Created  :
  Description   :
  Parameters    :
  Return Value  :   Boolean
-----------------------------------------------------------------------------}
function TfrmPagoVentanillaCastigo.ImprimirRecibo : boolean;
begin
	result := FImprimirRecibo;
end;

{-----------------------------------------------------------------------------
  Function Name :   NumeroRecibo
  Author        :
  Date Created  :
  Description   :
  Parameters    :
  Return Value  :   INt64
-----------------------------------------------------------------------------}
function TfrmPagoVentanillaCastigo.NumeroRecibo : INt64;
begin
	result := FNumeroRecibo;
end;

 {-----------------------------------------------------------------------------
  Function Name :   ValidarDatosTurno
  Author        :
  Date Created  :
  Description   :
  Parameters    :
  Return Value  :   Boolean
-----------------------------------------------------------------------------}
function TfrmPagoVentanillaCastigo.ValidarDatosTurno: Boolean;
var
    DatosTurno: TDatosTurno;
begin
    (* Verificar que el turno que se est� por utilizar est� abierto y sea del
    usuario logueado. *)
    if VerTurnoAbierto(DMConnections.BaseCAC, FPuntoEntrega, FPuntoVenta, UsuarioSistema, DatosTurno) then begin
        GNumeroTurno := DatosTurno.NumeroTurno;
        Result := True;
    end else begin
        if (DatosTurno.NumeroTurno <> -1)
                and ((DatosTurno.Estado = TURNO_ESTADO_ABIERTO)
                and (Trim(DatosTurno.CodigoUsuario) <> (UpperCase(UsuarioSistema)))) then begin
            MsgBox(Format(MSG_ERROR_TURNO_ABIERTO_OTRO_USUARIO, [DatosTurno.CodigoUsuario]),
                Self.Caption, MB_ICONSTOP);
        end else begin
            MsgBox(MSG_ERROR_TURNO_CERRADO, Self.Caption, MB_ICONSTOP);
        end;
        Result := False;
    end;

end;

 {-----------------------------------------------------------------------------
  Function Name :   ObtenerImporte
  Author        :
  Date Created  :
  Description   :
  Parameters    :   Comprobantes
  Return Value  :   Double
-----------------------------------------------------------------------------}
function TfrmPagoVentanillaCastigo.ObtenerImporte(Comprobantes: TListaComprobante): Double;
var
    i: Integer;
begin
    Result := 0;
    for i := 0 to Comprobantes.Count - 1 do begin
        Result := Result + TComprobante(Comprobantes.Items[i]).TotalAPagar;
    end;
end;

 {-----------------------------------------------------------------------------
  Function Name :   CerrarRegistroPagoComprobante
  Author        :
  Date Created  :
  Description   :
  Parameters    :   TipoComprobante; NumeroComprobante; FechaPago
  Return Value  :   None
-----------------------------------------------------------------------------}
procedure TfrmPagoVentanillaCastigo.CerrarRegistroPagoComprobante(TipoComprobante: AnsiString; NumeroComprobante: Int64; FechaPago: TDateTime);
resourcestring
    MSG_ERROR_CIERRE = 'Error completando el registro del pago';
begin
   //Se cierra el comprobante
   try
        with CerrarPagoComprobante.Parameters do begin
            ParamByName('@TipoComprobante').Value 	:= TipoComprobante;
            ParamByName('@NumeroComprobante').Value := NumeroComprobante;
            ParamByName('@FechaHora').Value		    := FechaPago;
         end;
         CerrarPagoComprobante.ExecProc;
    except
        on e: Exception do begin
            raise Exception.Create(MSG_ERROR_CIERRE + CRLF + e.Message);
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name :   FormShow
  Author        :
  Date Created  :
  Description   :
  Parameters    :   Sender
  Return Value  :   None
-----------------------------------------------------------------------------}
procedure TfrmPagoVentanillaCastigo.FormShow(Sender: TObject);
begin
    EmptyKeyQueue;
end;

{-----------------------------------------------------------------------------
  Function Name :   CalcularImportePagado
  Author        :
  Date Created  :
  Description   :
  Parameters    :   i
  Return Value  :   Int64
-----------------------------------------------------------------------------}
function TfrmPagoVentanillaCastigo.CalcularImportePagado(i: Integer): Int64;
begin
	// Primero chequeamos si es el �timo comprobante de la lista
	// Si es el �ltimo retornamos la diferencia entre la suma de los comprobantes ya pagados y lo indicado
	if (FImporteComprobantesPagados) > 0 then begin
		if ((FImporteComprobantesPagados) <  (trunc(TComprobante(FComprobantesCobrar.Items[i]).TotalAPagar) * 100)) or
		  (i = FComprobantesCobrar.Count - 1) then begin
			result := (FImporteComprobantesPagados);
		end else begin
			result := trunc(TComprobante(FComprobantesCobrar.Items[i]).TotalAPagar) * 100;
		end;
		FImporteComprobantesPagados := FImporteComprobantesPagados +
		  trunc(TComprobante(FComprobantesCobrar.Items[i]).TotalAPagar) * 100;
	end else begin
		result := 0;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name :   btnCerrarClick
  Author        :
  Date Created  :
  Description   :
  Parameters    :   Sender
  Return Value  :   None
-----------------------------------------------------------------------------}
procedure TfrmPagoVentanillaCastigo.btnCerrarClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name :   GenerarPago
  Author        :
  Date Created  :
  Description   :
  Parameters    :   NumeroRecibo; FormaPago; MensajeError
  Return Value  :   Boolean
-----------------------------------------------------------------------------}
function TfrmPagoVentanillaCastigo.GenerarPago(NumeroRecibo: Integer; FormaPago:TFormaPago; var MensajeError: String): Boolean;
resourcestring
    MSG_PAGO_CASTIGO = 'Pago Castigo';

    MSG_ERROR_PAGO_CASTIGO = 'Error registrando un pago Castigo';

    MSG_ERROR_ID_DETALLE_IS_NULL = 'No se pudo registrar el Detalle del Pago';
    MSG_ERROR_NUMERO_PAGO_IS_NULL = 'No se pudo registrar del Pago del Comprobante'#10#13'%s %d';
    MSG_ERROR_NO_BALANCEAN_PAGOS_CON_RECIBO = 'El Recibo %d es de un importe de %f y la suma de sus pagos asociados es de %f';
var
	I: Integer;
	Comprobante: TComprobante;
	MensajeDeError: string;
    ImportePago, SumatoriaPagosControlRecibo : Int64;
begin
	Result := False;
	FImporteComprobantesPagados := 0;
	MensajeError := '';
	SumatoriaPagosControlRecibo := 0;

    try

        sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@CodigoTipoMedioPago').Value := FCodigoCanalPago;
        sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@CodigoFormaPago').Value     := FCodigoFormaPago;

        sp_RegistrarDetallePagoComprobante.ExecProc;
        if sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@IDDetallePagoComprobante').Value = Null then begin
            MensajeError := MSG_ERROR_ID_DETALLE_IS_NULL;
            Exit;
        end;

    except
        on E: Exception do begin
            MensajeError := MSG_ERROR_ID_DETALLE_IS_NULL + CRLF + E.Message;
            Exit;
        end;
    end;

    if not FPagoNotaCobroInfraccion then begin      // NO SE PAGA NOTA DE COBRO INFRACCION
        for I := 0 to (FComprobantesCobrar.Count - 1) do begin

            //Comprobante := TComprobante(FComprobantesCobrar.Items[I]);
            ImportePago := CalcularImportePagado(i);
            SumatoriaPagosControlRecibo := SumatoriaPagosControlRecibo + ImportePago;

            if ImportePago > 0 then begin
                try
                    sp_RegistrarPagoComprobante.Parameters.ParamByName('@IDDetallePagoComprobante').Value := sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@IDDetallePagoComprobante').Value;
                    sp_RegistrarPagoComprobante.Parameters.ParamByName('@TipoComprobante').Value          := null; //Comprobante.TipoComprobante;      //GLEON
                    sp_RegistrarPagoComprobante.Parameters.ParamByName('@NumeroComprobante').Value        := null; //Comprobante.NumeroComprobante;    //GLEON
                    sp_RegistrarPagoComprobante.Parameters.ParamByName('@FechaHora').Value                := FFechaHoraEmision;
                    sp_RegistrarPagoComprobante.Parameters.ParamByName('@NumeroRecibo').Value             := NumeroRecibo;
                    sp_RegistrarPagoComprobante.Parameters.ParamByName('@CodigoConceptoPago').Value       := ObtenerCodigoConceptoPago(DMConnections.BaseCAC, FCodigoCanalPago);

                    case FormaPago of
                        tpCastigo :sp_RegistrarPagoComprobante.Parameters.ParamByName('@DescripcionPago').Value := MSG_PAGO_CASTIGO;
                    end;
                    sp_RegistrarPagoComprobante.Parameters.ParamByName('@Importe').Value     := ImportePago;
                    sp_RegistrarPagoComprobante.Parameters.ParamByName('@NumeroTurno').Value := GNumeroTurno;
                    sp_RegistrarPagoComprobante.Parameters.ParamByName('@Usuario').Value     := UsuarioSistema;

                    if I < (FComprobantesCobrar.Count - 1) then //si no se paga el �ltimo, se paga puntualmente este solo
                        sp_RegistrarPagoComprobante.Parameters.ParamByName('@PagarSoloComprobanteSolicitado').Value     := 1
                    else
                        sp_RegistrarPagoComprobante.Parameters.ParamByName('@PagarSoloComprobanteSolicitado').Value     := 2;
                        // cuando es el ultimo, se paga primero ese y luego el resto de la lista de pendientes
                        sp_RegistrarPagoComprobante.Parameters.ParamByName('@NoGenerarIntereses').Value := FNoCalcularIntereses;
                        sp_RegistrarPagoComprobante.Parameters.ParamByName('@NumeroPago').Value := null;

                        sp_RegistrarPagoComprobante.ExecProc;

                        {
                    if (sp_RegistrarPagoComprobante.Parameters.ParamByName('@NumeroPago').Value <> null) then
                        CerrarRegistroPagoComprobante(Comprobante.TipoComprobante, Comprobante.NumeroComprobante, FFechaHoraEmision)
                    else begin
                        MensajeError := Format(MSG_ERROR_NUMERO_PAGO_IS_NULL, [DescriTipoComprobante(Comprobante.TipoComprobante), Comprobante.NumeroComprobante]);
                        Exit;
                    end;
                        } //GLEON
                except
                    on e: Exception do begin
                        case FormaPago of
                            tpCastigo      : MensajeDeError := MSG_ERROR_PAGO_CASTIGO;
                         end;
                        MensajeError :=  MensajeDeError + CRLF + e.Message ;
                        Exit;
                    end;
                end;
            end;
        end;
        if (SumatoriaPagosControlRecibo <> (FImportePagado * 100)) then begin
            //la sumatoria de los pagos difiere del importe del recibo creado
            MensajeError := Format(MSG_ERROR_NO_BALANCEAN_PAGOS_CON_RECIBO, [NumeroRecibo, (SumatoriaPagosControlRecibo/100), FImportePagado]);
            Exit;
        end;

    end else begin      // SE PAGA NOTA DE COBRO INFRACCION
        Comprobante := TComprobante(FComprobantesCobrar.Items[0]);
        try
            RegistrarPagoNotaCobroInfraccion.Parameters.Refresh;
            RegistrarPagoNotaCobroInfraccion.Parameters.ParamByName('@IDDetallePagoComprobante').Value := sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@IDDetallePagoComprobante').Value;
            RegistrarPagoNotaCobroInfraccion.Parameters.ParamByName('@TipoComprobante').Value          := Comprobante.TipoComprobante;
            RegistrarPagoNotaCobroInfraccion.Parameters.ParamByName('@NumeroComprobante').Value        := Comprobante.NumeroComprobante;
            RegistrarPagoNotaCobroInfraccion.Parameters.ParamByName('@FechaHora').Value                := FFechaHoraEmision;  {deFechaPago.Date;} //Revision 16
            RegistrarPagoNotaCobroInfraccion.Parameters.ParamByName('@NumeroRecibo').Value             := NumeroRecibo;
            RegistrarPagoNotaCobroInfraccion.Parameters.ParamByName('@CodigoConceptoPago').Value       := ObtenerCodigoConceptoPago(DMConnections.BaseCAC, FCodigoCanalPago);
            case FormaPago of
                tpCastigo: RegistrarPagoNotaCobroInfraccion.Parameters.ParamByName('@DescripcionPago').Value := MSG_PAGO_CASTIGO;
            end;
            RegistrarPagoNotaCobroInfraccion.Parameters.ParamByName('@Importe').Value     := neImporte.ValueInt * 100;
            RegistrarPagoNotaCobroInfraccion.Parameters.ParamByName('@NumeroTurno').Value := GNumeroTurno;
            RegistrarPagoNotaCobroInfraccion.Parameters.ParamByName('@Usuario').Value     := UsuarioSistema;
            RegistrarPagoNotaCobroInfraccion.Parameters.ParamByName('@NumeroPago').Value  := 0;
            RegistrarPagoNotaCobroInfraccion.ExecProc;
        except
            on e: Exception do begin
                case FormaPago of
                    tpCastigo: MensajeDeError := MSG_ERROR_PAGO_CASTIGO;
                end;
                MensajeError := MensajeDeError + CRLF + e.Message;
                Exit;
            end;
        end;

    end;

    Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name :   chkNoCalcularInteresesClick
  Author        :
  Date Created  :
  Description   :
  Parameters    :   Sender
  Return Value  :   None
-----------------------------------------------------------------------------}
procedure TfrmPagoVentanillaCastigo.chkNoCalcularInteresesClick(Sender: TObject);
begin
    FNoCalcularIntereses := True;
end;

procedure TfrmPagoVentanillaCastigo.edtCodigoFormaPagoChange(Sender: TObject);
resourcestring
    MSG_FORMA_DE_PAGO_ERRONEA = 'La Forma de Pago NO es V�lida para el Canal seleccionado.';
    MSG_FORMA_DE_PAGO_NO_EXISTE = 'La Forma de Pago NO Existe.';
    MSG_CAPTION_VALIDACION = 'Validar Forma de Pago';
    MSG_ERROR_VALIDACION = 'Se produjo un Error al Validar Forma de Pago';
    MSG_ERROR_VALIDACION_ERROR = 'Error: %s';
const
    SQLValidarFormaDePagoEnCanal = 'SELECT dbo.ValidarFormaDePagoEnCanal(%s, ''%s'')';

    {******************************** Procedure Header *****************************
    Procedure Name  : ResetearControlesFormaPago
    Author          : pdominguez
    Date Created    : 12/08/2009
    Parameters      : None
    Description     : SS 809
        - Reseta los valores e indicadores para la forma de pago cuando no est�
        validada.
        - Utilizamos el Tag como Flag para saber si est� validada o no
        la forma de pago.
    *******************************************************************************}
    procedure ResetearControlesFormaPago;
    begin
        FCodigoFormaPago       := 0;
        //nbOpciones.Visible     := False;
        edtCodigoFormaPago.Tag := 0;
    end;
begin
    case Length(edtCodigoFormaPago.Text) of
        2: try
            with sp_ValidarFormaDePagoEnCanal do begin
                if Active then Close;

                Parameters.ParamByName('@CodigoTipoMedioPago').Value := cb_CanalesDePago.Value;
                Parameters.ParamByName('@CodigoEntradaUsuario').Value := edtCodigoFormaPago.Text;

                Parameters.ParamByName('@ValidarPagoAnticipado').Value := 0;
                Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;
                Parameters.ParamByName('@CodigoSistema').Value := SistemaActual;

                Parameters.ParamByName('@CodigoFormaDePago').Value := Null;         //14 -- FormasPago
                Parameters.ParamByName('@DescripcionFormaDePago').Value := Null;    //Castigo -- FormasPago
                Parameters.ParamByName('@EsValidaEnCanal').Value := Null;
                Parameters.ParamByName('@AceptaPagoParcial').Value := Null;
                Parameters.ParamByName('@CodigoPantalla').Value := Null;
                Parameters.ParamByName('@CodigoBanco').Value := Null;
                Parameters.ParamByName('@CodigoTarjeta').Value := Null;
                ExecProc;

                edtDescFormaPago.Text := Parameters.ParamByName('@DescripcionFormaDePago').Value;

                if Parameters.ParamByName('@EsValidaEnCanal').Value = 0 then begin
                    ResetearControlesFormaPago;
                    if Parameters.ParamByName('@CodigoFormaDePago').Value = 0 then
                        MsgBox(MSG_FORMA_DE_PAGO_NO_EXISTE, MSG_CAPTION_VALIDACION, MB_ICONERROR)
                    else MsgBox(MSG_FORMA_DE_PAGO_ERRONEA, MSG_CAPTION_VALIDACION,MB_ICONERROR);
                end else begin
                    FCodigoFormaPago := Parameters.ParamByName('@CodigoFormaDePago').Value;
                     FCodigoPantalla := Parameters.ParamByName('@CodigoPantalla').Value;

                    edtCodigoFormaPago.Tag := 1;
                end;
            end;
        except
            on E:Exception do begin
                ResetearControlesFormaPago;
                MsgBoxErr(
                    MSG_ERROR_VALIDACION,
                    Format(MSG_ERROR_VALIDACION_ERROR,[E.Message]),
                    MSG_CAPTION_VALIDACION,
                    MB_ICONERROR);
            end;
        end;
    else
        ResetearControlesFormaPago;
        edtDescFormaPago.Text  := '';
    end;
end;

{-----------------------------------------------------------------------------
  Function Name :   GenerarPagoComprobante
  Author        :
  Date Created  :
  Description   :
  Parameters    :   FormaPago; MensajeError; NumeroRecibo
  Return Value  :   Boolean
-----------------------------------------------------------------------------}
function TfrmPagoVentanillaCastigo.GenerarPagoComprobante(FormaPago: TFormaPago; var MensajeError: String; var NumeroRecibo: Int64): Boolean;
resourcestring
    MSG_PAGO_CASTIGO = 'Pago Castigo';
var
	Comprobante: TComprobante;

begin
	Result := False;
	MensajeError := '';

    try
        spCastigarConvenio.Parameters.Refresh;
           try
            spCastigarConvenio.Parameters.ParamByName('@FechaHora').Value := FFechaHoraEmision;
            spCastigarConvenio.Parameters.ParamByName('@Importe').Value := FImportePagado * 100;
            //spCastigarConvenio.Parameters.ParamByName('@EstadoPago').Value  := 'X'; //EstadoPago X - Castigado
            spCastigarConvenio.Parameters.ParamByName('@CodigoTipoMedioPago').Value := FCodigoCanalPago;
            spCastigarConvenio.Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;
            spCastigarConvenio.Parameters.ParamByName('@CodigoFormaPago').Value := FCodigoFormaPago;

            if FormaPago = tpCastigo then
                spCastigarConvenio.Parameters.ParamByName('@DescripcionPago').Value := MSG_PAGO_CASTIGO;

                spCastigarConvenio.Parameters.ParamByName('@NumeroTurno').Value := IntToStr(GNumeroTurno);
                spCastigarConvenio.Parameters.ParamByName('@NoGenerarIntereses').Value := BoolToStr(FNoCalcularIntereses);
                spCastigarConvenio.Parameters.ParamByName('@CodigoConceptoPago').Value := IntToStr(ObtenerCodigoConceptoPago(DMConnections.BaseCAC, FCodigoCanalPago));

            if FComprobantesCobrar.Count>0 then
                begin
                    Comprobante := TComprobante(FComprobantesCobrar.Items[0]);
                    spCastigarConvenio.Parameters.ParamByName('@TipoComprobante').Value := Comprobante.TipoComprobante;
                    spCastigarConvenio.Parameters.ParamByName('@NumeroComprobante').Value := IntToStr(Comprobante.NumeroComprobante);
                end;
            spCastigarConvenio.Parameters.ParamByName('@NumeroRecibo').Value := null;

            spCastigarConvenio.ExecProc;

            if spCastigarConvenio.Parameters.ParamByName('@RETURN_VALUE').Value = 0 then
                begin
                    Result:= True;
                    NumeroRecibo := iif(spCastigarConvenio.Parameters.ParamByName('@NumeroRecibo').Value <> null, spCastigarConvenio.Parameters.ParamByName('@NumeroRecibo').Value, 0);
                end
            else
                begin
                    MensajeError := spCastigarConvenio.Parameters.ParamByName('@ErrorDescription').Value;
                end;

        except
            on E: Exception do begin
                MensajeError := e.Message;
            end;
        end;
    finally
        spCastigarConvenio.Free;

    end;
end;

 {******************************** Function Header ******************************
Function Name: cb_CanalesDePagoChange
Author :
Date Created :
Description :
Parameters : Sender: TObject
Return Value : None

Revision : 1
    Author : pdominguez
    Date   : 12/08/2009
    Description : SS 809
        - Se inicializa la forma de pago al cambiar el canal.
        - Se inhabilitan las referencias al objeto eliminado cb_FormasPago.
        - Se pasa el Foco al objeto edtCodigoFormaPago.
*******************************************************************************}
procedure TfrmPagoVentanillaCastigo.cb_CanalesDePagoChange(Sender: TObject);
begin
    FCodigoCanalPago := Integer(cb_CanalesDePago.Value);
    edtCodigoFormaPago.Text := 'CA'; 
end;
end.


