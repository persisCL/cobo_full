object FormEditarDebito: TFormEditarDebito
  Left = 332
  Top = 261
  BorderStyle = bsDialog
  Caption = 'D'#233'bito Autom'#225'tico'
  ClientHeight = 141
  ClientWidth = 337
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 8
    Top = 8
    Width = 321
    Height = 97
  end
  object Label22: TLabel
    Left = 25
    Top = 24
    Width = 64
    Height = 13
    Caption = 'Debitar de:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbl_entidad: TLabel
    Left = 25
    Top = 49
    Width = 45
    Height = 13
    Caption = 'Tarjeta:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label23: TLabel
    Left = 25
    Top = 75
    Width = 48
    Height = 13
    Caption = 'N'#250'mero:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object cb_tipodebito: TComboBox
    Left = 93
    Top = 21
    Width = 181
    Height = 21
    Style = csDropDownList
    Color = 16444382
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 0
    Text = 'Tarjeta de Cr'#233'dito'
    OnChange = cb_tipodebitoChange
    Items.Strings = (
      'Tarjeta de Cr'#233'dito'
      'Cuenta Bancaria')
  end
  object cb_entidaddebito: TComboBox
    Left = 93
    Top = 46
    Width = 181
    Height = 21
    Style = csDropDownList
    Color = 16444382
    ItemHeight = 13
    TabOrder = 1
  end
  object txt_cuentadebito: TEdit
    Left = 93
    Top = 71
    Width = 139
    Height = 21
    Color = 16444382
    MaxLength = 16
    TabOrder = 2
  end
  object OPButton1: TDPSButton
    Left = 176
    Top = 112
    Caption = '&Aceptar'
    Default = True
    TabOrder = 3
    OnClick = OPButton1Click
  end
  object OPButton2: TDPSButton
    Left = 256
    Top = 112
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 4
  end
end
