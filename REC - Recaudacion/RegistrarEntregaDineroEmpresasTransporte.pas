unit RegistrarEntregaDineroEmpresasTransporte;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, ListBoxEx, DBListEx, ImgList, ComCtrls, ToolWin,
  RegistroDineroEntregadoEmpresaTransporte, UtilProc, DB, VariantComboBox,
  Validate, DateEdit, Bancos, PeaProcs;

type
  TfrmRegistrarEntregaDineroEmpTransporte = class(TForm)
    pnlOpciones: TPanel;
    tlbBotonera: TToolBar;
    btnSalir: TToolButton;
    btn1: TToolButton;
    btnAgregar: TToolButton;
    btnEliminar: TToolButton;
    btnEditar: TToolButton;
    btn2: TToolButton;
    btnImprimir: TToolButton;
    btnBuscar: TToolButton;
    ilActivos: TImageList;
    dblRecibos: TDBListEx;
    pnlGuardar: TPanel;
    btnAceptar: TButton;
    btnCancelar: TButton;
    pnlEdicion: TPanel;
    lblFecha: TLabel;
    lblBanco: TLabel;
    lblFolio: TLabel;
    lblNumCuentaCorriente: TLabel;
    lblMonto: TLabel;
    lblEmpresaTransporte: TLabel;
    edtFecha: TDateEdit;
    cbbBancos: TVariantComboBox;
    txtFolio: TEdit;
    txtNumCuentaCorriente: TEdit;
    txtMonto: TEdit;
    txtEmpresaTransporte: TEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSalirClick(Sender: TObject);
    procedure btnAgregarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure txtMontoKeyPress(Sender: TObject; var Key: Char);
    procedure dblRecibosDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure btnAceptarClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure btnEliminarClick(Sender: TObject);
  private
    { Private declarations }
    oRegistros : TRegistroDineroTransporte;
    FPuntoVenta:Integer;
    FUsuarioSistema: string;
    procedure CargarBancos();
    procedure CargarRegistros();
    procedure dblRecibos_Changed(DataSet: TDataSet);
    procedure PermitirEdicion(permitir: Boolean);
    procedure LimpiarCampos();
  public
    { Public declarations }
    function Inicializar(PuntoVenta:Integer;UsuarioSistema: string): Boolean;
  end;

var
  frmRegistrarEntregaDineroEmpTransporte: TfrmRegistrarEntregaDineroEmpTransporte;

implementation

{$R *.dfm}

function TfrmRegistrarEntregaDineroEmpTransporte.Inicializar(PuntoVenta:Integer; UsuarioSistema: string): Boolean;
begin
    Result := False;
    if PuntoVenta = -1 then
    begin
        MsgBox('Configure el Punto de Venta', 'Error de configuración', MB_OK);
        Exit;
    end;
    FPuntoVenta := PuntoVenta;
    FUsuarioSistema := UsuarioSistema;
    CargarBancos();
    CargarRegistros();
    dblRecibos_Changed(nil);
    Result:= True;
end;

procedure TfrmRegistrarEntregaDineroEmpTransporte.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    FreeAndNil(oRegistros);
    dblRecibos.DataSource.Free;
    Action := caFree;
end;

procedure TfrmRegistrarEntregaDineroEmpTransporte.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TfrmRegistrarEntregaDineroEmpTransporte.LimpiarCampos();
begin
    edtFecha.Clear;
    cbbBancos.ItemIndex := -1;
    txtFolio.Clear;
    txtNumCuentaCorriente.Clear;
    txtMonto.Clear;
    txtEmpresaTransporte.Clear;
end;

procedure TfrmRegistrarEntregaDineroEmpTransporte.CargarBancos;
var
    oBancos : TBanco;
begin
    try
        try

            cbbBancos.Items.Clear;
            oBancos := TBanco.Create;
            oBancos.Obtener(True);
            while not obancos.ClientDataSet.Eof do
            begin
                cbbBancos.Items.InsertItem(oBancos.Descripcion, oBancos.CodigoBanco);
                oBancos.ClientDataSet.Next;
            end;

            cbbBancos.ItemIndex := -1;            

        except
            on e: Exception do
                MsgBoxErr('Error cargando bancos', e.Message, 'Error de carga de datos', MB_ICONERROR);
        end;
    finally
        FreeAndNil(oBancos);
    end;
end;

procedure TfrmRegistrarEntregaDineroEmpTransporte.CargarRegistros();
begin
    try

        if not Assigned(oRegistros) then
            oRegistros := TRegistroDineroTransporte.Create;

        if not Assigned(dblRecibos.DataSource) then
            dblRecibos.DataSource := TDataSource.Create(nil);

        dblRecibos.DataSource.DataSet := oRegistros.Obtener(false);
        oRegistros.ClientDataSet.AfterScroll := dblRecibos_Changed;

    except
        on e: Exception do
            MsgBoxErr('Error cargando registros', e.Message, 'Error de carga de datos', MB_ICONERROR);
    end;
end;

procedure TfrmRegistrarEntregaDineroEmpTransporte.dblRecibosDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
  var
    field: ^TField;
begin
    if Assigned(oRegistros) and Assigned(oRegistros.ClientDataSet) and (oRegistros.ClientDataSet.RecordCount > 0) then
    begin
        if Column.FieldName = 'Monto' then
            Text := Format('%f', [StrToFloat(Text)]);
    end;       
end;

procedure TfrmRegistrarEntregaDineroEmpTransporte.dblRecibos_Changed(DataSet: TDataSet);
begin
    if Assigned(oRegistros) and Assigned(oRegistros.ClientDataSet) and (oRegistros.ClientDataSet.RecordCount > 0) then
    begin
       edtFecha.Date := oRegistros.Fecha;
       cbbBancos.Value := oRegistros.CodigoBanco;
       txtFolio.Text := oRegistros.Folio;
       txtNumCuentaCorriente.Text := oRegistros.NumCuentaCorriente;
       txtMonto.Text := Format('%f', [oRegistros.Monto]);
       txtEmpresaTransporte.Text := oRegistros.EmpresaTransporteValores;
    end;
end;                 


procedure TfrmRegistrarEntregaDineroEmpTransporte.txtMontoKeyPress(
  Sender: TObject; var Key: Char);
begin
    if not (Key in ['0'..'9','.',#8]) then
       Key := #0;
end;



procedure TfrmRegistrarEntregaDineroEmpTransporte.btnAgregarClick(Sender: TObject);
begin
    LimpiarCampos();
    PermitirEdicion(True);
    dblRecibos.Tag := 1;
end;

procedure TfrmRegistrarEntregaDineroEmpTransporte.btnEditarClick(Sender: TObject);
begin
     PermitirEdicion(True);
     dblRecibos.Tag := 2;
end;

procedure TfrmRegistrarEntregaDineroEmpTransporte.btnEliminarClick(Sender: TObject);
begin
    try
        if MsgBox('¿Confirma eliminar el registro?', 'Confirmación', MB_YESNO) = mrYes then
        begin
            oRegistros.Eliminar(oRegistros.ID, FUsuarioSistema);
            CargarRegistros;
        end;
    except
        on e:Exception do
            MsgBoxErr('Error eliminando registro', e.Message, 'Error', MB_ICONERROR);

    end;
end;

procedure TfrmRegistrarEntregaDineroEmpTransporte.PermitirEdicion(permitir: Boolean);
begin
     dblRecibos.Enabled := not permitir;
     pnlOpciones.Enabled := not permitir;

     edtFecha.Enabled := permitir;
     cbbBancos.Enabled := permitir;
     txtFolio.Enabled := permitir;
     txtNumCuentaCorriente.Enabled := permitir;
     txtMonto.Enabled := permitir;
     txtEmpresaTransporte.Enabled := permitir;

     btnAceptar.Enabled := permitir;
     btnCancelar.Enabled := permitir;
end;

procedure TfrmRegistrarEntregaDineroEmpTransporte.btnAceptarClick(Sender: TObject);
var
    Id: Integer;
begin
    try
        if not  ValidateControls([edtFecha,
                                    cbbBancos,
                                    txtFolio,
                                    txtNumCuentaCorriente,
                                    txtMonto,
                                    txtEmpresaTransporte],
                                 [datetostr(edtFecha.Date) <> '00/00/0000',    
                                    cbbBancos.ItemIndex > -1,
                                    Trim(txtFolio.Text) <> '',
                                    Trim(txtNumCuentaCorriente.Text) <> '',
                                    Trim(txtMonto.Text) <> '',
                                    Trim(txtEmpresaTransporte.Text) <> ''],
                                 'Validación',
                                 ['Ingrese Fecha', 
                                    'Seleccione un banco',
                                    'Ingrese Folio',
                                    'Ingrese Número de Cuenta Corriente',
                                    'Ingrese Monto',
                                    'Ingrese el nombre de la empresa de transporte de valores']) then
            Exit;

        if dblRecibos.Tag = 1 then
        begin
            Id := oRegistros.Agregar(edtFecha.Date, cbbBancos.Value, Trim(txtFolio.Text),
            Trim(txtNumCuentaCorriente.Text), StrToFloat(Trim(txtMonto.Text)), Trim(txtEmpresaTransporte.Text),
            FPuntoVenta, FUsuarioSistema);
        end
        else if dblRecibos.Tag = 2 then
        begin
            id := oRegistros.ID;

            oRegistros.Modificar(edtFecha.Date, cbbBancos.Value, Trim(txtFolio.Text),
            Trim(txtNumCuentaCorriente.Text), StrToFloat(Trim(txtMonto.Text)), Trim(txtEmpresaTransporte.Text),
            FPuntoVenta, False, FUsuarioSistema, oRegistros.ID);  
        end;

        CargarRegistros();

        if Id > 0 then
        begin
            while not oRegistros.ClientDataSet.Eof do
            begin
                if oRegistros.ID = id then
                    Break;
                oRegistros.ClientDataSet.Next;
            end;
        end;

        PermitirEdicion(False);
        dblRecibos.Tag := 0;
        dblRecibos_Changed(nil);
    except
        on e:Exception do
            MsgBoxErr('Error guardando cambios', e.Message, 'Error', MB_ICONERROR);
    end;
end;

procedure TfrmRegistrarEntregaDineroEmpTransporte.btnCancelarClick(Sender: TObject);
begin
    LimpiarCampos;
    PermitirEdicion(False);
    dblRecibos_Changed(nil);
end;




end.
