object formCobroComprobantes: TformCobroComprobantes
  Left = 338
  Top = 108
  Anchors = []
  BorderStyle = bsDialog
  Caption = 'Cobro de Comprobantes'
  ClientHeight = 590
  ClientWidth = 865
  Color = clBtnFace
  Constraints.MinHeight = 561
  Constraints.MinWidth = 670
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object gbPorCliente: TGroupBox
    Left = 0
    Top = 0
    Width = 865
    Height = 96
    Align = alTop
    Anchors = [akLeft, akRight, akBottom]
    Caption = ' B'#250'squeda Comprobante '
    Color = clBtnFace
    ParentColor = False
    TabOrder = 0
    object lb_CodigoCliente: TLabel
      Left = 12
      Top = 50
      Width = 61
      Height = 13
      Caption = 'RUT Cliente:'
    end
    object lbl_NumeroConvenio: TLabel
      Left = 179
      Top = 54
      Width = 103
      Height = 13
      Caption = 'N'#250'mero de Convenio:'
    end
    object Label3: TLabel
      Left = 14
      Top = 16
      Width = 97
      Height = 13
      Caption = 'Tipos Comprobantes'
    end
    object lbl_NumeroComprobante: TLabel
      Left = 183
      Top = 8
      Width = 103
      Height = 13
      Caption = 'N'#250'mero Comprobante'
    end
    object lbl_CodigoBarra: TLabel
      Left = 484
      Top = 8
      Width = 81
      Height = 13
      Caption = 'C'#243'digo de Barras'
    end
    object lblImpresoraFiscal: TLabel
      Left = 322
      Top = 8
      Width = 76
      Height = 13
      Caption = 'Impresora Fiscal'
      Visible = False
    end
    object lblDeudasConvenio: TLabel
      Left = 498
      Top = 60
      Width = 88
      Height = 13
      Alignment = taRightJustify
      Caption = 'Deuda Convenios:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblMontoDeudasConvenios: TLabel
      Left = 591
      Top = 60
      Width = 102
      Height = 13
      Cursor = crHandPoint
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'lblMontoDeudasConvenios'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ShowHint = False
    end
    object lblMontoDeudasRefinanciacion: TLabel
      Left = 591
      Top = 76
      Width = 102
      Height = 13
      Cursor = crHandPoint
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'lblMontoDeudasRefinanciacion'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ShowHint = False
    end
    object lblDeudasRefinanciacion: TLabel
      Left = 474
      Top = 76
      Width = 112
      Height = 13
      Alignment = taRightJustify
      Caption = 'Deudas Refinanciaci'#243'n:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblEnListaAmarilla: TLabel
      Left = 150
      Top = 3
      Width = 200
      Height = 14
      Alignment = taCenter
      AutoSize = False
      Color = clYellow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      Transparent = False
      Visible = False
    end
    object peNumeroDocumento: TPickEdit
      Left = 12
      Top = 67
      Width = 161
      Height = 21
      CharCase = ecUpperCase
      Enabled = True
      TabOrder = 4
      OnChange = peNumeroDocumentoChange
      EditorStyle = bteTextEdit
      OnButtonClick = peNumeroDocumentoButtonClick
    end
    object cbConvenios: TVariantComboBox
      Left = 179
      Top = 68
      Width = 209
      Height = 19
      Style = vcsOwnerDrawFixed
      ItemHeight = 13
      TabOrder = 5
      OnChange = cbConveniosChange
      OnDrawItem = cbConveniosDrawItem
      Items = <>
    end
    object edNumeroComprobante: TNumericEdit
      Left = 183
      Top = 27
      Width = 295
      Height = 21
      TabOrder = 1
      OnChange = edNumeroComprobanteChange
    end
    object edCodigoBarras: TEdit
      Left = 484
      Top = 27
      Width = 217
      Height = 21
      MaxLength = 29
      TabOrder = 3
      OnChange = edCodigoBarrasChange
      OnKeyPress = edCodigoBarrasKeyPress
    end
    object cbTiposComprobantes: TVariantComboBox
      Left = 12
      Top = 27
      Width = 165
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 0
      OnChange = cbTiposComprobantesChange
      Items = <>
    end
    object cbImpresorasFiscales: TVariantComboBox
      Left = 311
      Top = 27
      Width = 167
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 2
      Visible = False
      OnChange = cbImpresorasFiscalesChange
      Items = <>
    end
    object btnFiltrar: TButton
      Left = 778
      Top = 29
      Width = 75
      Height = 25
      Caption = 'Filtrar'
      Default = True
      TabOrder = 6
      OnClick = btnFiltrarClick
    end
  end
  object gb_DatosCliente: TGroupBox
    Left = 0
    Top = 96
    Width = 865
    Height = 70
    Align = alTop
    Caption = ' Datos del Cliente '
    TabOrder = 1
    object Label6: TLabel
      Left = 17
      Top = 32
      Width = 48
      Height = 13
      Caption = 'Nombre:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblNombre: TLabel
      Left = 91
      Top = 31
      Width = 72
      Height = 13
      Caption = 'Apellido Cliente'
    end
    object lblDomicilio: TLabel
      Left = 90
      Top = 49
      Width = 74
      Height = 13
      Caption = 'DomicilioCliente'
    end
    object Label10: TLabel
      Left = 17
      Top = 49
      Width = 56
      Height = 13
      Caption = 'Domicilio:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 17
      Top = 15
      Width = 31
      Height = 13
      Caption = 'RUT:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblRUT: TLabel
      Left = 91
      Top = 14
      Width = 23
      Height = 13
      Caption = 'RUT'
    end
  end
  object pcOpcionesComprobantes: TPageControl
    Left = 0
    Top = 166
    Width = 865
    Height = 424
    ActivePage = tbsRefinanciaciones
    Align = alClient
    TabOrder = 2
    object tbsComprobantes: TTabSheet
      Caption = ' Comprobantes Impagos '
      DesignSize = (
        857
        396)
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 857
        Height = 238
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 0
        DesignSize = (
          857
          238)
        object Label15: TLabel
          Left = 410
          Top = 216
          Width = 115
          Height = 13
          Anchors = [akLeft, akBottom]
          Caption = 'Total Seleccionado:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ExplicitTop = 214
        end
        object lblTotalSelec: TLabel
          Left = 530
          Top = 216
          Width = 90
          Height = 13
          Alignment = taRightJustify
          Anchors = [akLeft, akBottom]
          AutoSize = False
          Caption = 'TotalSeleccionado'
          ExplicitTop = 214
        end
        object dl_Comprobantes: TDBListEx
          Left = 3
          Top = 7
          Width = 849
          Height = 203
          Anchors = [akLeft, akTop, akRight, akBottom]
          BorderStyle = bsSingle
          Columns = <
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 50
              Header.Caption = 'Cobrar'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'Cobrar'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 95
              Header.Caption = 'Fecha Emisi'#243'n'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'FechaEmision'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 95
              Header.Caption = 'Vencimiento'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'FechaVencimiento'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 150
              Header.Caption = 'Comprobante'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'DescriComprobante'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 150
              Header.Caption = 'Comprobante Fiscal'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              IsLink = False
              FieldName = 'DescriComprobanteFiscal'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 80
              Header.Caption = 'Saldo'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              Header.Alignment = taRightJustify
              IsLink = False
              FieldName = 'SaldoPendienteDescri'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 80
              Header.Caption = 'Total'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              Header.Alignment = taRightJustify
              IsLink = False
              FieldName = 'TotalComprobanteDescri'
            end>
          DataSource = ds_Comprobantes
          DragReorder = True
          ParentColor = False
          TabOrder = 0
          TabStop = True
          OnClick = dl_ComprobantesClick
          OnDblClick = dl_ComprobantesDblClick
          OnDrawText = dl_ComprobantesDrawText
          OnKeyDown = dl_ComprobantesKeyDown
        end
        object chkbxSeleccionManual: TCheckBox
          Left = 154
          Top = 214
          Width = 130
          Height = 16
          Caption = 'Selecci'#243'n Manual'
          TabOrder = 1
          OnClick = chkbxSeleccionManualClick
        end
        object btnDesmarcarTodos: TButton
          Left = 10
          Top = 211
          Width = 130
          Height = 21
          Caption = 'Desmarcar Todos'
          TabOrder = 2
          OnClick = btnDesmarcarTodosClick
        end
      end
      object gbDatosComprobante: TGroupBox
        Left = 0
        Top = 238
        Width = 857
        Height = 117
        Align = alBottom
        Caption = ' Datos del Comprobante seleccionado '
        TabOrder = 1
        DesignSize = (
          857
          117)
        object Label2: TLabel
          Left = 12
          Top = 18
          Width = 105
          Height = 13
          Caption = 'Fecha de Emisi'#243'n:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lblFecha: TLabel
          Left = 181
          Top = 18
          Width = 91
          Height = 13
          AutoSize = False
          Caption = 'Fecha'
        end
        object lblFechaVencimiento: TLabel
          Left = 181
          Top = 36
          Width = 91
          Height = 13
          AutoSize = False
          Caption = 'Fecha Vencimiento'
        end
        object Label7: TLabel
          Left = 12
          Top = 36
          Width = 131
          Height = 13
          Caption = 'Fecha de Vencimiento:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lblTotalAPagar: TLabel
          Left = 530
          Top = 18
          Width = 90
          Height = 13
          Alignment = taRightJustify
          Anchors = [akLeft, akBottom]
          AutoSize = False
          Caption = 'Total a Pagar'
        end
        object Label12: TLabel
          Left = 430
          Top = 18
          Width = 94
          Height = 13
          Caption = 'Deuda Exigible :'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbl_Estado: TLabel
          Left = 535
          Top = 68
          Width = 195
          Height = 13
          AutoSize = False
          Caption = 'lblEstado'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label11: TLabel
          Left = 464
          Top = 68
          Width = 44
          Height = 13
          Caption = 'Estado:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label13: TLabel
          Left = 12
          Top = 56
          Width = 157
          Height = 13
          Caption = 'Medio de Pago Autom'#225'tico:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lblDescPATPAC: TLabel
          Left = 269
          Top = 56
          Width = 77
          Height = 13
          Caption = 'lblDescPATPAC'
        end
        object lblEstadoDebito: TLabel
          Left = 181
          Top = 76
          Width = 74
          Height = 13
          Caption = 'lblEstadoDebito'
        end
        object Label14: TLabel
          Left = 12
          Top = 76
          Width = 106
          Height = 13
          Caption = 'Estado del D'#233'bito:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lblMedioPagoAutomatico: TLabel
          Left = 181
          Top = 56
          Width = 75
          Height = 13
          AutoSize = False
          Caption = 'lblMedioPagoAutomatico'
        end
        object lblCapRespuesta: TLabel
          Left = 13
          Top = 96
          Width = 106
          Height = 13
          Caption = 'Pago Rechazado: '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lblRespuestaDebito: TLabel
          Left = 182
          Top = 96
          Width = 92
          Height = 13
          Caption = 'lblRespuestaDebito'
        end
      end
      object Panel1: TPanel
        Left = 0
        Top = 355
        Width = 857
        Height = 41
        Align = alBottom
        TabOrder = 2
        DesignSize = (
          857
          41)
        object btnCobrar: TButton
          Left = 5
          Top = 11
          Width = 196
          Height = 25
          Anchors = [akLeft, akBottom]
          Caption = 'Cobrar (F9)'
          TabOrder = 0
          OnClick = btnCobrarClick
        end
        object btnSalir: TButton
          Left = 774
          Top = 11
          Width = 75
          Height = 25
          Anchors = [akRight, akBottom]
          Cancel = True
          Caption = '&Salir'
          TabOrder = 1
          OnClick = btnSalirClick
        end
      end
    end
    object TabSheet1: TTabSheet
      Caption = ' Cuotas Refinanciaciones Impagas '
      ImageIndex = 2
      object Panel3: TPanel
        Left = 0
        Top = 355
        Width = 857
        Height = 41
        Align = alBottom
        TabOrder = 0
        DesignSize = (
          857
          41)
        object btnCobrarCuota: TButton
          Left = 8
          Top = 11
          Width = 196
          Height = 25
          Anchors = [akLeft, akBottom]
          Caption = 'Cobrar Cuota (F9)'
          TabOrder = 0
          OnClick = btnCobrarCuotaClick
        end
        object Button3: TButton
          Left = 774
          Top = 11
          Width = 75
          Height = 25
          Anchors = [akRight, akBottom]
          Cancel = True
          Caption = '&Salir'
          TabOrder = 1
          OnClick = btnSalirClick
        end
      end
      object DBListEx2: TDBListEx
        Left = 0
        Top = 34
        Width = 857
        Height = 291
        Align = alClient
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 50
            Header.Caption = 'Cobrar'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = True
            FieldName = 'Cobrar'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 95
            Header.Caption = 'Forma Pago'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'DescripcionFormaPago'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 75
            Header.Caption = 'Fecha'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'FechaCuota'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 90
            Header.Caption = 'Importe'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taRightJustify
            IsLink = False
            FieldName = 'ImporteDesc'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 90
            Header.Caption = 'Saldo'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taRightJustify
            IsLink = False
            FieldName = 'SaldoDesc'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 100
            Header.Caption = 'Estado'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'DescripcionEstadoCuota'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 55
            Header.Caption = 'Refinanciaci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taRightJustify
            IsLink = False
            FieldName = 'CodigoRefinanciacion'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 120
            Header.Caption = 'Convenio'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'NumeroConvenio'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 90
            Header.Caption = 'Rut'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'TitularTipoDocumento'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'Titular'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'TitularNombre'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 120
            Header.Caption = 'Nro. Doc.'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'DocumentoNumero'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 175
            Header.Caption = 'Banco'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'DescripcionBanco'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 175
            Header.Caption = 'Tipo Tarjeta'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'DescripcionTarjeta'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 100
            Header.Caption = 'Cuotas Tarjeta'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taRightJustify
            IsLink = False
            FieldName = 'TarjetaCuotas'
          end>
        DataSource = dsCuotasAPagar
        DragReorder = True
        ParentColor = False
        TabOrder = 1
        TabStop = True
        OnDrawBackground = DBListEx2DrawBackground
        OnDrawText = DBListEx2DrawText
        OnLinkClick = DBListEx2LinkClick
      end
      object Panel7: TPanel
        Left = 0
        Top = 325
        Width = 857
        Height = 30
        Align = alBottom
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Color = 14408667
        ParentBackground = False
        TabOrder = 2
        DesignSize = (
          857
          30)
        object Label18: TLabel
          Left = 495
          Top = 9
          Width = 81
          Height = 13
          Alignment = taRightJustify
          Anchors = [akRight, akBottom]
          Caption = 'Cantidad Cuotas:'
        end
        object lblCuotasCantidad: TLabel
          Left = 573
          Top = 9
          Width = 36
          Height = 13
          Alignment = taRightJustify
          Anchors = [akRight, akBottom]
          AutoSize = False
          Caption = '0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label20: TLabel
          Left = 644
          Top = 9
          Width = 95
          Height = 13
          Anchors = [akRight, akBottom]
          Caption = 'Total Seleccionado:'
        end
        object lblCuotasTotal: TLabel
          Left = 761
          Top = 9
          Width = 80
          Height = 13
          Alignment = taRightJustify
          Anchors = [akRight, akBottom]
          AutoSize = False
          Caption = '$ 0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ExplicitLeft = 802
        end
      end
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 857
        Height = 34
        Align = alTop
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Color = 14408667
        ParentBackground = False
        TabOrder = 3
        object Label1: TLabel
          Left = 17
          Top = 11
          Width = 73
          Height = 13
          Caption = 'Refinanciaci'#243'n:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object vcbRefinanciacion: TVariantComboBox
          Left = 96
          Top = 7
          Width = 217
          Height = 21
          Style = vcsDropDownList
          ItemHeight = 13
          TabOrder = 0
          OnChange = vcbRefinanciacionChange
          Items = <>
        end
      end
    end
    object tbsRefinanciaciones: TTabSheet
      Caption = ' Refinanciaciones '
      ImageIndex = 1
      object Panel2: TPanel
        Left = 0
        Top = 355
        Width = 857
        Height = 41
        Align = alBottom
        TabOrder = 0
        DesignSize = (
          857
          41)
        object btnActivarRefinanciacion: TButton
          Left = 2
          Top = 11
          Width = 196
          Height = 25
          Anchors = [akLeft, akBottom]
          Caption = 'Activar Refinanciaci'#243'n (F9)'
          TabOrder = 0
          OnClick = btnActivarRefinanciacionClick
        end
        object Button2: TButton
          Left = 774
          Top = 11
          Width = 75
          Height = 25
          Anchors = [akRight, akBottom]
          Cancel = True
          Caption = '&Salir'
          TabOrder = 1
          OnClick = btnSalirClick
        end
        object btnConsultarRefinanciacion: TButton
          Left = 204
          Top = 11
          Width = 75
          Height = 25
          Caption = 'Consultar'
          TabOrder = 2
          OnClick = btnConsultarRefinanciacionClick
        end
      end
      object DBListEx1: TDBListEx
        Left = 0
        Top = 0
        Width = 857
        Height = 355
        Align = alClient
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 50
            Header.Caption = 'Nro.'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taRightJustify
            IsLink = False
            FieldName = 'CodigoRefinanciacion'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 125
            Header.Caption = 'Tipo'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'Tipo'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 85
            Header.Caption = 'Fecha'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'Fecha'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 200
            Header.Caption = 'Estado'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'Estado'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 65
            Header.Caption = 'Validada'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'Validada'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'Convenio'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'Convenio'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 95
            Header.Caption = 'Total Deuda'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taRightJustify
            IsLink = False
            FieldName = 'TotalDeuda'
          end>
        DataSource = dsRefinanciaciones
        DragReorder = True
        ParentColor = False
        TabOrder = 1
        TabStop = True
        OnDrawText = DBListEx1DrawText
      end
    end
  end
  object ds_Comprobantes: TDataSource
    DataSet = cds_Comprobantes
    Left = 32
    Top = 252
  end
  object Img_Tilde: TImageList
    Left = 728
    Top = 540
    Bitmap = {
      494C010102000400F00010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF008C8C9400B5B5B500F7F7F700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00E7E7
      E7003939420018212100ADADAD00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00C6C6
      C600182121000000000031313900F7F7F700FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF003942
      4200181821003942420018182100ADB5B500FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF0063636B001818
      210031313900E7E7E7003131390029293100FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00C6C6C6003939
      4200FFFFFF00FFFFFF00A5A5A500212129004A525200FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF006B6B7300212929007B848400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF007373730021212900C6C6C600F7F7
      F700FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00F7F7F7005A5A5A0031313900D6DE
      DE00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0042424A001821
      2100949C9C00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF009C9C
      9C002129290094949C00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00E7E7E70042424A00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF000000008001800100000000
      8001800100000000800180010000000084018001000000008001800100000000
      8001800100000000800180010000000080018001000000008001800100000000
      8001800100000000800180010000000080018001000000008001800100000000
      8001800100000000FFFFFFFF0000000000000000000000000000000000000000
      000000000000}
  end
  object spObtenerDomicilioFacturacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerDomicilioFacturacion;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Domicilio'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 255
        Value = ''
      end
      item
        Name = '@Comuna'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = ''
      end
      item
        Name = '@Region'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = ''
      end>
    Left = 508
    Top = 531
  end
  object spObtenerSaldoConveniosComprobantesImpagos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    CommandTimeout = 300
    ProcedureName = 'ObtenerSaldoConveniosComprobantesImpagos;1'
    Parameters = <>
    Left = 588
    Top = 131
  end
  object cds_Comprobantes: TClientDataSet
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Cobrar'
        DataType = ftBoolean
      end
      item
        Name = 'TipoComprobante'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'NumeroComprobante'
        DataType = ftLargeint
      end
      item
        Name = 'DescriComprobante'
        DataType = ftString
        Size = 35
      end
      item
        Name = 'CodigoConvenio'
        DataType = ftInteger
      end
      item
        Name = 'CodigoPersona'
        DataType = ftInteger
      end
      item
        Name = 'TotalComprobante'
        DataType = ftInteger
      end
      item
        Name = 'EstadoPago'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EstadoDebito'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'DescriEstado'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'UltimoComprobante'
        DataType = ftLargeint
      end
      item
        Name = 'FechaEmision'
        DataType = ftDateTime
      end
      item
        Name = 'FechaVencimiento'
        DataType = ftDateTime
      end
      item
        Name = 'TotalPagos'
        DataType = ftInteger
      end
      item
        Name = 'TotalPagosStr'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'SaldoPendiente'
        DataType = ftInteger
      end
      item
        Name = 'SaldoPendienteDescri'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'TotalComprobanteDescri'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'DescTipoMedioPAgo'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'DescPATPAC'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'Rechazo'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'TipoComprobanteFiscal'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'NumeroComprobanteFiscal'
        DataType = ftLargeint
      end
      item
        Name = 'DescriComprobanteFiscal'
        DataType = ftString
        Size = 35
      end
      item
        Name = 'EstadoRefinanciacion'
        DataType = ftSmallint
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 124
    Top = 252
    Data = {
      100300009619E0BD010000001800000019000000000003000000100306436F62
      72617202000300000000000F5469706F436F6D70726F62616E74650100490000
      000100055749445448020002001400114E756D65726F436F6D70726F62616E74
      65080001000000000011446573637269436F6D70726F62616E74650100490000
      0001000557494454480200020023000E436F6469676F436F6E76656E696F0400
      0100000000000D436F6469676F506572736F6E61040001000000000010546F74
      616C436F6D70726F62616E746504000100000000000A45737461646F5061676F
      01004900000001000557494454480200020001000C45737461646F4465626974
      6F01004900000001000557494454480200020001000C44657363726945737461
      646F010049000000010005574944544802000200140011556C74696D6F436F6D
      70726F62616E746508000100000000000C4665636861456D6973696F6E080008
      000000000010466563686156656E63696D69656E746F08000800000000000A54
      6F74616C5061676F7304000100000000000D546F74616C5061676F7353747201
      004900000001000557494454480200020014000E53616C646F50656E6469656E
      746504000100000000001453616C646F50656E6469656E746544657363726901
      0049000000010005574944544802000200140016546F74616C436F6D70726F62
      616E746544657363726901004900000001000557494454480200020014001144
      6573635469706F4D6564696F5041676F01004900000001000557494454480200
      020032000A446573635041545041430100490000000100055749445448020002
      0032000752656368617A6F010049000000010005574944544802000200320015
      5469706F436F6D70726F62616E746546697363616C0100490000000100055749
      445448020002001400174E756D65726F436F6D70726F62616E74654669736361
      6C080001000000000017446573637269436F6D70726F62616E74654669736361
      6C01004900000001000557494454480200020023001445737461646F52656669
      6E616E63696163696F6E02000100000000000000}
  end
  object spObtenerRefinanciacionesConsultas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerRefinanciacionesConsultas'
    Parameters = <>
    Left = 440
    Top = 328
  end
  object dspObtenerRefinanciacionesConsultas: TDataSetProvider
    DataSet = spObtenerRefinanciacionesConsultas
    Left = 544
    Top = 224
  end
  object cdsRefinanciaciones: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'CodigoRefinanciacion'
        Fields = 'CodigoRefinanciacion'
      end>
    IndexName = 'CodigoRefinanciacion'
    Params = <>
    ProviderName = 'dspObtenerRefinanciacionesConsultas'
    StoreDefs = True
    AfterScroll = cdsRefinanciacionesAfterScroll
    Left = 576
    Top = 408
    object cdsRefinanciacionesCodigoRefinanciacion: TIntegerField
      FieldName = 'CodigoRefinanciacion'
    end
    object cdsRefinanciacionesCodigoTipoMedioPago: TSmallintField
      FieldName = 'CodigoTipoMedioPago'
    end
    object cdsRefinanciacionesTipo: TStringField
      FieldName = 'Tipo'
      FixedChar = True
      Size = 30
    end
    object cdsRefinanciacionesFecha: TDateTimeField
      FieldName = 'Fecha'
    end
    object cdsRefinanciacionesEstado: TStringField
      FieldName = 'Estado'
      Size = 30
    end
    object cdsRefinanciacionesNumeroDocumento: TStringField
      FieldName = 'NumeroDocumento'
      FixedChar = True
      Size = 11
    end
    object cdsRefinanciacionesNombrePersona: TStringField
      FieldName = 'NombrePersona'
      ReadOnly = True
      Size = 100
    end
    object cdsRefinanciacionesConvenio: TStringField
      FieldName = 'Convenio'
      ReadOnly = True
      FixedChar = True
      Size = 30
    end
    object cdsRefinanciacionesTotalDeuda: TStringField
      FieldName = 'TotalDeuda'
      ReadOnly = True
    end
    object cdsRefinanciacionesTotalPagado: TStringField
      FieldName = 'TotalPagado'
      ReadOnly = True
    end
    object cdsRefinanciacionesValidada: TBooleanField
      FieldName = 'Validada'
    end
    object cdsRefinanciacionesCodigoRefinanciacionUnificada: TIntegerField
      FieldName = 'CodigoRefinanciacionUnificada'
    end
    object cdsRefinanciacionesCodigoEstado: TIntegerField
      FieldName = 'CodigoEstado'
    end
  end
  object dsRefinanciaciones: TDataSource
    DataSet = cdsRefinanciaciones
    Left = 800
    Top = 256
  end
  object lnCheck: TImageList
    BkColor = clWhite
    Height = 15
    Width = 15
    Left = 687
    Top = 539
    Bitmap = {
      494C010103000400F0000F000F00FFFFFF00FF10FFFFFFFFFFFFFFFF424D3600
      00000000000036000000280000003C0000000F0000000100200000000000100E
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF000000FF000000FF000000FF000000FF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      FF000000FF000000FF000000FF000000FF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000FF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF000000FF00FFFFFF000000FF000000FF000000
      FF000000FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      FF00FFFFFF00FFFFFF000000FF000000FF000000FF000000FF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000FF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF000000
      000000000000FFFFFF00000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF000000FF00FFFFFF00FFFFFF00FFFFFF000000
      FF000000FF000000FF000000FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF000000FF000000FF000000
      FF00FFFFFF00FFFFFF00FFFFFF000000FF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF000000FF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000FF000000FF000000FF000000FF00FFFFFF00FFFFFF000000
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF000000
      FF000000FF000000FF00FFFFFF000000FF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF000000FF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000FF000000FF000000FF000000FF000000
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000FF000000FF000000FF000000FF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF000000FF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF000000FF000000
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000424D3E000000000000003E000000280000003C0000000F00000001000100
      00000000780000000000000000000000000000000000000000000000FFFFFF00
      FFFFFFFFFFF80000800300060008000080030006000800008003000600080000
      8003000600080000800300060008000080030006000800008003000600080000
      8003000600080000800300060008000080030006000800008003000600080000
      80030006000800008003000600080000FFFFFFFFFFF800000000000000000000
      0000000000000000000000000000}
  end
  object spCrearRecibo: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CrearRecibo'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaHora'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Importe'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@NumeroRecibo'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoMedioPago'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 384
    Top = 536
  end
  object spObtenerRefinanciacionComprobantes: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerRefinanciacionComprobantes'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 752
    Top = 160
  end
  object spRefinanciacionCancelarComprobante: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'RefinanciacionCancelarComprobante'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@Importe'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@NumeroRecibo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@NumeroPago'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ImporteAplicadoDelPago'
        Attributes = [paNullable]
        DataType = ftLargeint
        Direction = pdInputOutput
        Precision = 19
        Value = Null
      end>
    Left = 752
    Top = 64
  end
  object spObtenerRefinanciacionCuotas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerRefinanciacionCuotas'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCuota'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@SinCuotasAnuladas'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 480
    Top = 272
  end
  object spRegistrarDetallePagoComprobante: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'RegistrarDetallePagoComprobante'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@IDDetallePagoComprobante'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoMedioPago'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoFormaPago'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@PAC_CodigoBanco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PAC_CodigoTipoCuentaBancaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@PAC_NroCuentaBancaria'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@PAC_Sucursal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@PAC_Titular'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@PAT_CodigoTipoTarjetaCredito'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@PAT_NumeroTarjetaCredito'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@PAT_FechaVencimiento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 5
        Value = Null
      end
      item
        Name = '@PAT_CodigoEmisorTarjetaCredito'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@PAT_Titular'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@ChequeCodigoBanco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ChequeNumero'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@ChequeTitular'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@ChequeFecha'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@ChequeCodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@ChequeNumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@ChequeMonto'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@Cupon'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Autorizacion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@Cuotas'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@NumeroPOS'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Web_NumeroFinalTarjetaCredito'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@Web_TipoVenta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@Titular'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@CodigoTipoTarjeta'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoBancoEmisor'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@RepactacionNumero'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 256
    Top = 328
  end
  object cdsRefinanciacionComprobantes: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspRefinanciacionComprobantes'
    Left = 576
    Top = 296
    object cdsRefinanciacionComprobantesFechaEmision: TDateTimeField
      FieldName = 'FechaEmision'
    end
    object cdsRefinanciacionComprobantesFechaVencimiento: TDateTimeField
      FieldName = 'FechaVencimiento'
    end
    object cdsRefinanciacionComprobantesTipoComprobante: TStringField
      FieldName = 'TipoComprobante'
      FixedChar = True
      Size = 2
    end
    object cdsRefinanciacionComprobantesNumeroComprobante: TLargeintField
      FieldName = 'NumeroComprobante'
    end
    object cdsRefinanciacionComprobantesDescriComprobante: TStringField
      FieldName = 'DescriComprobante'
      Size = 78
    end
    object cdsRefinanciacionComprobantesSaldoPendienteDescri: TStringField
      FieldName = 'SaldoPendienteDescri'
    end
    object cdsRefinanciacionComprobantesTotalComprobanteDescri: TStringField
      FieldName = 'TotalComprobanteDescri'
    end
    object cdsRefinanciacionComprobantesDescriAPagar: TStringField
      FieldName = 'DescriAPagar'
    end
    object cdsRefinanciacionComprobantesSaldoPendienteComprobante: TLargeintField
      FieldName = 'SaldoPendienteComprobante'
    end
    object cdsRefinanciacionComprobantesSaldoRefinanciado: TLargeintField
      FieldName = 'SaldoRefinanciado'
    end
  end
  object dspRefinanciacionComprobantes: TDataSetProvider
    DataSet = spObtenerRefinanciacionComprobantes
    Left = 704
    Top = 280
  end
  object spRefinanciacionRegistrarPagoComprobante: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'RefinanciacionRegistrarPagoComprobante'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@Importe'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@NumeroRecibo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@IDDetallePagoComprobante'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConceptoPago'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@DescripcionPago'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@NumeroPago'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 256
    Top = 424
  end
  object dspRefinanciacionCuotas: TDataSetProvider
    DataSet = spObtenerRefinanciacionCuotas
    Left = 704
    Top = 232
  end
  object cdsRefinanciacionCuotas: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspRefinanciacionCuotas'
    Left = 576
    Top = 352
    object cdsRefinanciacionCuotasCodigoCuota: TSmallintField
      FieldName = 'CodigoCuota'
    end
    object cdsRefinanciacionCuotasCodigoEstadoCuota: TSmallintField
      FieldName = 'CodigoEstadoCuota'
    end
    object cdsRefinanciacionCuotasDescripcionEstadoCuota: TStringField
      FieldName = 'DescripcionEstadoCuota'
      Size = 30
    end
    object cdsRefinanciacionCuotasCodigoFormaPago: TSmallintField
      FieldName = 'CodigoFormaPago'
    end
    object cdsRefinanciacionCuotasDescripcionFormaPago: TStringField
      FieldName = 'DescripcionFormaPago'
      Size = 30
    end
    object cdsRefinanciacionCuotasTitularNombre: TStringField
      FieldName = 'TitularNombre'
      Size = 100
    end
    object cdsRefinanciacionCuotasTitularTipoDocumento: TStringField
      FieldName = 'TitularTipoDocumento'
      FixedChar = True
      Size = 4
    end
    object cdsRefinanciacionCuotasTitularNumeroDocumento: TStringField
      FieldName = 'TitularNumeroDocumento'
      Size = 11
    end
    object cdsRefinanciacionCuotasCodigoBanco: TIntegerField
      FieldName = 'CodigoBanco'
    end
    object cdsRefinanciacionCuotasDescripcionBanco: TStringField
      FieldName = 'DescripcionBanco'
      Size = 50
    end
    object cdsRefinanciacionCuotasDocumentoNumero: TStringField
      FieldName = 'DocumentoNumero'
    end
    object cdsRefinanciacionCuotasFechaCuota: TDateTimeField
      FieldName = 'FechaCuota'
    end
    object cdsRefinanciacionCuotasCodigoTarjeta: TIntegerField
      FieldName = 'CodigoTarjeta'
    end
    object cdsRefinanciacionCuotasDescripcionTarjeta: TStringField
      FieldName = 'DescripcionTarjeta'
      FixedChar = True
      Size = 50
    end
    object cdsRefinanciacionCuotasTarjetaCuotas: TSmallintField
      FieldName = 'TarjetaCuotas'
    end
    object cdsRefinanciacionCuotasImporte: TLargeintField
      FieldName = 'Importe'
    end
    object cdsRefinanciacionCuotasSaldo: TLargeintField
      FieldName = 'Saldo'
    end
    object cdsRefinanciacionCuotasCodigoEntradaUsuario: TStringField
      FieldName = 'CodigoEntradaUsuario'
      FixedChar = True
      Size = 2
    end
    object cdsRefinanciacionCuotasEstadoImpago: TBooleanField
      FieldName = 'EstadoImpago'
    end
    object cdsRefinanciacionCuotasCodigoCuotaAntecesora: TSmallintField
      FieldName = 'CodigoCuotaAntecesora'
    end
    object cdsRefinanciacionCuotasCuotaNueva: TBooleanField
      FieldKind = fkInternalCalc
      FieldName = 'CuotaNueva'
    end
    object cdsRefinanciacionCuotasCuotaModificada: TBooleanField
      FieldKind = fkInternalCalc
      FieldName = 'CuotaModificada'
    end
  end
  object spObtenerRefinanciacionDatosCabecera: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerRefinanciacionDatosCabecera'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 744
    Top = 392
  end
  object dspRefinanciacionDatos: TDataSetProvider
    DataSet = spObtenerRefinanciacionDatosCabecera
    Left = 704
    Top = 336
  end
  object cdsRefinanciacionDatos: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspRefinanciacionDatos'
    Left = 576
    Top = 464
    object cdsRefinanciacionDatosCodigoRefinanciacion: TIntegerField
      FieldName = 'CodigoRefinanciacion'
    end
    object cdsRefinanciacionDatosCodigoPersona: TIntegerField
      FieldName = 'CodigoPersona'
    end
    object cdsRefinanciacionDatosCodigoConvenio: TIntegerField
      FieldName = 'CodigoConvenio'
    end
    object cdsRefinanciacionDatosCodigoTipoMedioPago: TSmallintField
      FieldName = 'CodigoTipoMedioPago'
    end
    object cdsRefinanciacionDatosFecha: TDateTimeField
      FieldName = 'Fecha'
    end
    object cdsRefinanciacionDatosCodigoEstado: TSmallintField
      FieldName = 'CodigoEstado'
    end
    object cdsRefinanciacionDatosEstadoRefinanciacion: TStringField
      FieldName = 'EstadoRefinanciacion'
      Size = 30
    end
    object cdsRefinanciacionDatosTotalDeuda: TLargeintField
      FieldName = 'TotalDeuda'
      ReadOnly = True
    end
    object cdsRefinanciacionDatosTotalPagado: TLargeintField
      FieldName = 'TotalPagado'
      ReadOnly = True
    end
    object cdsRefinanciacionDatosTotalDemanda: TLargeintField
      FieldName = 'TotalDemanda'
      ReadOnly = True
    end
    object cdsRefinanciacionDatosNotificacion: TBooleanField
      FieldName = 'Notificacion'
    end
    object cdsRefinanciacionDatosRepLegalCliNumeroDocumento: TStringField
      FieldName = 'RepLegalCliNumeroDocumento'
      Size = 11
    end
    object cdsRefinanciacionDatosRepLegalCliNombre: TStringField
      FieldName = 'RepLegalCliNombre'
      Size = 60
    end
    object cdsRefinanciacionDatosRepLegalCliApellido: TStringField
      FieldName = 'RepLegalCliApellido'
      Size = 60
    end
    object cdsRefinanciacionDatosRepLegalCliApellidoMaterno: TStringField
      FieldName = 'RepLegalCliApellidoMaterno'
      Size = 30
    end
    object cdsRefinanciacionDatosRepLegalCNNumeroDocumento: TStringField
      FieldName = 'RepLegalCNNumeroDocumento'
      Size = 11
    end
    object cdsRefinanciacionDatosRepLegalCNNombre: TStringField
      FieldName = 'RepLegalCNNombre'
      Size = 60
    end
    object cdsRefinanciacionDatosRepLegalCNApellido: TStringField
      FieldName = 'RepLegalCNApellido'
      Size = 60
    end
    object cdsRefinanciacionDatosRepLegalCNApellidoMaterno: TStringField
      FieldName = 'RepLegalCNApellidoMaterno'
      Size = 30
    end
    object cdsRefinanciacionDatosNumeroCausaRol: TStringField
      FieldName = 'NumeroCausaRol'
    end
    object cdsRefinanciacionDatosGastoNotarial: TIntegerField
      FieldName = 'GastoNotarial'
      ReadOnly = True
    end
    object cdsRefinanciacionDatosUsuarioCreacion: TStringField
      FieldName = 'UsuarioCreacion'
    end
    object cdsRefinanciacionDatosFechaCreacion: TDateTimeField
      FieldName = 'FechaCreacion'
    end
    object cdsRefinanciacionDatosUsuarioModificacion: TStringField
      FieldName = 'UsuarioModificacion'
    end
    object cdsRefinanciacionDatosFechaModificacion: TDateTimeField
      FieldName = 'FechaModificacion'
    end
    object cdsRefinanciacionDatosJPLNumero: TSmallintField
      FieldName = 'JPLNumero'
    end
    object cdsRefinanciacionDatosJPLCodigoPais: TStringField
      FieldName = 'JPLCodigoPais'
      FixedChar = True
      Size = 3
    end
    object cdsRefinanciacionDatosJPLCodigoRegion: TStringField
      FieldName = 'JPLCodigoRegion'
      FixedChar = True
      Size = 3
    end
    object cdsRefinanciacionDatosJPLCodigoComuna: TStringField
      FieldName = 'JPLCodigoComuna'
      FixedChar = True
      Size = 3
    end
  end
  object spActualizarRefinanciacionActivada: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarRefinanciacionActivada'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 744
    Top = 448
  end
  object spObtenerRefinanciacionesTotalesPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerRefinanciacionesTotalesPersona;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 256
    Top = 272
  end
  object spObtenerRefinanciacionCuotasAPagar: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerRefinanciacionCuotasAPagar'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@RUTPersona'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 48
    Top = 472
  end
  object dspCuotasAPagar: TDataSetProvider
    DataSet = spObtenerRefinanciacionCuotasAPagar
    Left = 48
    Top = 376
  end
  object cdsCuotasAPagar: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspCuotasAPagar'
    Left = 48
    Top = 424
    object cdsCuotasAPagarRut: TStringField
      FieldName = 'Rut'
      FixedChar = True
      Size = 11
    end
    object cdsCuotasAPagarNombrePersona: TStringField
      FieldName = 'NombrePersona'
      ReadOnly = True
      Size = 100
    end
    object cdsCuotasAPagarCodigoRefinanciacion: TIntegerField
      FieldName = 'CodigoRefinanciacion'
    end
    object cdsCuotasAPagarCodigoCuota: TSmallintField
      FieldName = 'CodigoCuota'
    end
    object cdsCuotasAPagarCodigoEstadoCuota: TSmallintField
      FieldName = 'CodigoEstadoCuota'
    end
    object cdsCuotasAPagarDescripcionEstadoCuota: TStringField
      FieldName = 'DescripcionEstadoCuota'
      Size = 30
    end
    object cdsCuotasAPagarCodigoFormaPago: TSmallintField
      FieldName = 'CodigoFormaPago'
    end
    object cdsCuotasAPagarDescripcionFormaPago: TStringField
      FieldName = 'DescripcionFormaPago'
      Size = 30
    end
    object cdsCuotasAPagarTitularNombre: TStringField
      FieldName = 'TitularNombre'
      Size = 100
    end
    object cdsCuotasAPagarTitularTipoDocumento: TStringField
      FieldName = 'TitularTipoDocumento'
      FixedChar = True
      Size = 4
    end
    object cdsCuotasAPagarTitularNumeroDocumento: TStringField
      FieldName = 'TitularNumeroDocumento'
      Size = 11
    end
    object cdsCuotasAPagarCodigoBanco: TIntegerField
      FieldName = 'CodigoBanco'
    end
    object cdsCuotasAPagarDescripcionBanco: TStringField
      FieldName = 'DescripcionBanco'
      Size = 50
    end
    object cdsCuotasAPagarDocumentoNumero: TStringField
      FieldName = 'DocumentoNumero'
    end
    object cdsCuotasAPagarFechaCuota: TDateTimeField
      FieldName = 'FechaCuota'
    end
    object cdsCuotasAPagarImporte: TLargeintField
      FieldName = 'Importe'
      ReadOnly = True
    end
    object cdsCuotasAPagarImporteDesc: TStringField
      FieldName = 'ImporteDesc'
      ReadOnly = True
    end
    object cdsCuotasAPagarEstadoImpago: TBooleanField
      FieldName = 'EstadoImpago'
    end
    object cdsCuotasAPagarNumeroConvenio: TStringField
      FieldName = 'NumeroConvenio'
      ReadOnly = True
      FixedChar = True
      Size = 30
    end
    object cdsCuotasAPagarSaldo: TLargeintField
      FieldName = 'Saldo'
      ReadOnly = True
    end
    object cdsCuotasAPagarSaldoDesc: TStringField
      FieldName = 'SaldoDesc'
      ReadOnly = True
    end
    object cdsCuotasAPagarCodigoTarjeta: TIntegerField
      FieldName = 'CodigoTarjeta'
    end
    object cdsCuotasAPagarDescripcionTarjeta: TStringField
      FieldName = 'DescripcionTarjeta'
      FixedChar = True
      Size = 50
    end
    object cdsCuotasAPagarTarjetaCuotas: TSmallintField
      FieldName = 'TarjetaCuotas'
    end
    object cdsCuotasAPagarCuotaNueva: TBooleanField
      FieldKind = fkInternalCalc
      FieldName = 'CuotaNueva'
    end
    object cdsCuotasAPagarCuotaModificada: TBooleanField
      FieldKind = fkInternalCalc
      FieldName = 'CuotaModificada'
    end
    object cdsCuotasAPagarCodigoTipoMedioPago: TSmallintField
      FieldName = 'CodigoTipoMedioPago'
    end
    object cdsCuotasAPagarCodigoEntradaUsuario: TStringField
      FieldName = 'CodigoEntradaUsuario'
      FixedChar = True
      Size = 2
    end
    object cdsCuotasAPagarCodigoCuotaAntecesora: TSmallintField
      FieldName = 'CodigoCuotaAntecesora'
    end
    object cdsCuotasAPagarCobrar: TBooleanField
      FieldKind = fkInternalCalc
      FieldName = 'Cobrar'
    end
  end
  object dsCuotasAPagar: TDataSource
    DataSet = cdsCuotasAPagar
    Left = 48
    Top = 328
  end
  object spActualizarRefinanciacionCuotaPago: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarRefinanciacionCuotaPago'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCuota'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@Importe'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@Anticipado'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaPago'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Autorizacion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@NumeroRecibo'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 440
    Top = 384
  end
  object spActualizarRefinanciacionCuotasEstados: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarRefinanciacionCuotasEstados'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCuota'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@CodigoEstadoCuota'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 440
    Top = 480
  end
  object spActualizarRefinanciacionPagoAnticipado: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarRefinanciacionPagoAnticipado'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCuota'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@Importe'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaPago'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@NumeroRecibo'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 440
    Top = 440
  end
  object spObtenerDeudaComprobantesVencidos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDeudaComprobantesVencidos'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 400
    Top = 112
  end
  object spObtenerRefinanciacionCuotasAPagarRefinanciaciones: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerRefinanciacionCuotasAPagarRefinanciaciones'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@RUTPersona'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end>
    Left = 752
    Top = 112
  end
  object spCobrar_o_DescontarReimpresion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'Cobrar_o_DescontarReimpresion'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 400
    Top = 64
  end
  object spActualizarInfraccionesPagadas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarInfraccionesPagadas'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end>
    Left = 256
    Top = 376
  end
  object sp_ActualizarClienteMoroso: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarClienteMoroso'
    Parameters = <
      item
        Name = '@NumeroDocumento'
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@CodigoRefinanciacion'
        DataType = ftInteger
        Value = Null
      end
      item
        Name = '@Usuario'
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@ErrorDescription'
        DataType = ftString
        Direction = pdOutput
        Size = -1
        Value = Null
      end>
    Left = 264
    Top = 536
  end
end
