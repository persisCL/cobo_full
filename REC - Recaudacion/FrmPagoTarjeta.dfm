object FormPagoTarjeta: TFormPagoTarjeta
  Left = 223
  Top = 325
  BorderStyle = bsDialog
  Caption = 'Gestion de Datos de Medios de Pago'
  ClientHeight = 248
  ClientWidth = 702
  Color = clBtnFace
  Constraints.MinHeight = 275
  Constraints.MinWidth = 710
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Notebook: TNotebook
    Left = 0
    Top = 0
    Width = 702
    Height = 75
    Align = alTop
    PageIndex = 1
    TabOrder = 0
    object TPage
      Left = 0
      Top = 0
      Caption = 'TCREDITO'
      object GroupBoxPAT: TGroupBox
        Left = 0
        Top = 0
        Width = 702
        Height = 75
        Align = alClient
        Caption = 'Tarjeta'
        TabOrder = 0
        object Label1: TLabel
          Left = 37
          Top = 21
          Width = 92
          Height = 13
          Caption = '&Tipo de Tarjeta:'
          Color = clBtnFace
          FocusControl = cb_TipoTarjeta
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
        end
        object lblNumeroTarjetaCredito: TLabel
          Left = 37
          Top = 48
          Width = 48
          Height = 13
          Caption = '&N'#250'mero:'
          FocusControl = txt_NroTarjetaCredito
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lblFechaVencimiento: TLabel
          Left = 366
          Top = 48
          Width = 167
          Height = 13
          Caption = '&Fecha Vencimiento (MM/AA):'
          FocusControl = txt_FechaVencimiento
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label2: TLabel
          Left = 366
          Top = 21
          Width = 104
          Height = 13
          Caption = '&Emisor de Tarjeta:'
          Color = clBtnFace
          FocusControl = cb_EmisoresTarjetasCreditos
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
        end
        object cb_TipoTarjeta: TComboBox
          Left = 137
          Top = 17
          Width = 200
          Height = 21
          Style = csDropDownList
          Color = 16444382
          ItemHeight = 13
          TabOrder = 0
        end
        object txt_NroTarjetaCredito: TEdit
          Left = 137
          Top = 44
          Width = 198
          Height = 21
          Hint = 'N'#250'mero de documento'
          CharCase = ecUpperCase
          Color = 16444382
          MaxLength = 20
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
        object txt_FechaVencimiento: TMaskEdit
          Left = 544
          Top = 44
          Width = 37
          Height = 21
          Color = 16444382
          EditMask = '00\/00;1; '
          MaxLength = 5
          TabOrder = 3
          Text = '00/00'
        end
        object cb_EmisoresTarjetasCreditos: TComboBox
          Left = 477
          Top = 17
          Width = 200
          Height = 21
          Style = csDropDownList
          Color = 16444382
          ItemHeight = 13
          TabOrder = 1
        end
      end
    end
    object TPage
      Left = 0
      Top = 0
      HelpContext = 1
      Caption = 'TDEBITO'
      object GroupBoxPAC: TGroupBox
        Left = 0
        Top = 0
        Width = 702
        Height = 75
        Align = alClient
        Caption = 'Cuenta'
        TabOrder = 0
        object Label7: TLabel
          Left = 37
          Top = 24
          Width = 92
          Height = 13
          Caption = '&Tipo de Cuenta:'
          Color = clBtnFace
          FocusControl = cb_TipoCuenta
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
        end
        object Label8: TLabel
          Left = 359
          Top = 24
          Width = 41
          Height = 13
          Caption = '&Banco:'
          Color = clBtnFace
          FocusControl = cb_Banco
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
        end
        object Label9: TLabel
          Left = 37
          Top = 48
          Width = 92
          Height = 13
          Caption = '&N'#250'mero Cuenta:'
          Color = clBtnFace
          FocusControl = txt_NumeroCuenta
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
        end
        object lblSucursalCheque: TLabel
          Left = 358
          Top = 48
          Width = 76
          Height = 13
          Caption = '&Nro. Cheque:'
          Color = clBtnFace
          FocusControl = txt_Sucursal
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
        end
        object cb_TipoCuenta: TComboBox
          Left = 136
          Top = 19
          Width = 198
          Height = 21
          Style = csDropDownList
          Color = 16444382
          ItemHeight = 13
          TabOrder = 0
          OnClick = cb_TipoCuentaClick
        end
        object cb_Banco: TComboBox
          Left = 436
          Top = 19
          Width = 238
          Height = 21
          Style = csDropDownList
          Color = 16444382
          ItemHeight = 13
          TabOrder = 1
        end
        object txt_NumeroCuenta: TEdit
          Left = 136
          Top = 44
          Width = 198
          Height = 21
          CharCase = ecUpperCase
          Color = 16444382
          MaxLength = 30
          TabOrder = 2
        end
        object txt_Sucursal: TEdit
          Left = 436
          Top = 44
          Width = 238
          Height = 21
          CharCase = ecUpperCase
          Color = 16444382
          MaxLength = 30
          TabOrder = 3
        end
      end
    end
  end
  object gb_DatosPersonales: TGroupBox
    Left = 0
    Top = 75
    Width = 702
    Height = 48
    Align = alTop
    Caption = 'Datos del Mandante'
    TabOrder = 1
    object Label3: TLabel
      Left = 36
      Top = 21
      Width = 41
      Height = 13
      Caption = 'Titular:'
      FocusControl = txt_NombreTitular
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblRUT: TLabel
      Left = 516
      Top = 21
      Width = 31
      Height = 13
      Caption = 'RUT:'
      FocusControl = txt_NombreTitular
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object txt_NombreTitular: TEdit
      Left = 137
      Top = 17
      Width = 344
      Height = 21
      Hint = 'Nombre del titular de la tarjeta de cr'#233'dito'
      CharCase = ecUpperCase
      Color = 16444382
      MaxLength = 100
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
    end
    object txtDocumento: TEdit
      Left = 568
      Top = 17
      Width = 108
      Height = 21
      Hint = 'RUT'
      CharCase = ecUpperCase
      Color = 16444382
      MaxLength = 100
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Visible = False
      OnKeyPress = txtDocumentoKeyPress
    end
  end
  object pnlBotton: TPanel
    Left = 0
    Top = 213
    Width = 702
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 4
    object Panel1: TPanel
      Left = 473
      Top = 0
      Width = 229
      Height = 35
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object btnSalir: TDPSButton
        Left = 125
        Top = 4
        Width = 92
        Hint = 'Salir de la Carga'
        Cancel = True
        Caption = '&Salir'
        ModalResult = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object btnAceptar: TDPSButton
        Left = 18
        Top = 4
        Width = 95
        Hint = 'Guardar Datos'
        Caption = '&Aceptar'
        Default = True
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = btnAceptarClick
      end
    end
  end
  object GroupBoxDatosPago: TGroupBox
    Left = 0
    Top = 165
    Width = 702
    Height = 42
    Align = alTop
    Caption = 'Datos del pago'
    TabOrder = 3
    object Label11: TLabel
      Left = 36
      Top = 19
      Width = 44
      Height = 13
      Caption = 'Monto: '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbl_Cuotas: TLabel
      Left = 370
      Top = 19
      Width = 44
      Height = 13
      Caption = 'Cuotas:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 442
      Top = 19
      Width = 108
      Height = 13
      Caption = 'Fecha del Cheque:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object deFechaCheque: TDateEdit
      Left = 568
      Top = 14
      Width = 108
      Height = 21
      AutoSelect = False
      AutoSize = False
      Color = 16444382
      TabOrder = 2
      Date = -693594
    end
    object txt_MontoTarjeta: TNumericEdit
      Left = 137
      Top = 14
      Width = 97
      Height = 21
      Color = 16444382
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 8
      ParentFont = False
      TabOrder = 0
      Decimals = 0
    end
    object txt_Cuotas: TNumericEdit
      Left = 433
      Top = 14
      Width = 49
      Height = 21
      Color = 16444382
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 2
      ParentFont = False
      TabOrder = 1
      Decimals = 0
      Value = 1
    end
  end
  object gb_Autorizacion: TGroupBox
    Left = 0
    Top = 123
    Width = 702
    Height = 42
    Align = alTop
    Caption = 'Datos de Autorizaci'#243'n'
    TabOrder = 2
    object lblFechaCheque: TLabel
      Left = 36
      Top = 19
      Width = 75
      Height = 13
      Caption = 'Autorizaci'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 370
      Top = 19
      Width = 41
      Height = 13
      Caption = 'Cup'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object edCupon: TNumericEdit
      Left = 433
      Top = 14
      Width = 129
      Height = 21
      Color = 16444382
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 20
      ParentFont = False
      TabOrder = 1
      Decimals = 0
    end
    object edAutorizacion: TEdit
      Left = 137
      Top = 14
      Width = 208
      Height = 21
      Hint = 'Nombre del titular de la tarjeta de cr'#233'dito'
      CharCase = ecUpperCase
      Color = 16444382
      MaxLength = 100
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
    end
  end
end
