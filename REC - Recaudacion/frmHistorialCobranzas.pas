{-----------------------------------------------------------------------------
 Unit Name: frmHistorialCobranzas
 Autor:    	cfuentesc
 Fecha:     05/04/2017
 Propůsito: Visualizar todas las acciones de cobranza efectuadas 
 			sobre un convenio en particular
 			CU.COBO.COB.201
 Firma: 	TASK_124_CFU_20170320-Gestionar_Carpeta_Deudores
 Historia:
-----------------------------------------------------------------------------}
unit frmHistorialCobranzas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ListBoxEx, DBListEx, DB, ADODB;

type
  TFormHistorialCobranzas = class(TForm)
    Panel1: TPanel;
    btnSalir: TButton;
    DBLHistorialCobranza: TDBListEx;
    spObtenerHistorialDeCobranza: TADOStoredProc;
    dsObtenerHistorialDeCobranzaConvenio: TDataSource;
    procedure btnSalirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
	function Inicializar(CodigoConvenio: Integer; NumeroConvenio: string): Boolean;
  end;

var
  FormHistorialCobranzas: TFormHistorialCobranzas;

implementation

uses
	frmMuestraMensaje, UtilProc, DMConnection, SysUtilsCN;

{$R *.dfm}

procedure TFormHistorialCobranzas.btnSalirClick(Sender: TObject);
begin
	Close;
end;

function TFormHistorialCobranzas.Inicializar(CodigoConvenio: Integer; NumeroConvenio: string): Boolean;
resourcestring
	MSG_CAPTION             = 'Historial de Cobranzas';
	MSG_ERROR_INICIALIZAR   = 'Error inicializando el formulario.';
begin
	Result := False;
	try
    	CambiarEstadoCursor(CURSOR_RELOJ);
    	spObtenerHistorialDeCobranza.DisableControls;
        try
            Result 	:= False;
            Caption	:= Caption + NumeroConvenio;
            with spObtenerHistorialDeCobranza, Parameters do begin
            	if Active then
                	Close;
                Parameters.Refresh;
                ParamByName('@CodigoConvenio').Value	:= CodigoConvenio;
                Prepared := True;
                Open;
            end;
        except
              on e: exception do begin
                  MsgBoxErr(MSG_ERROR_INICIALIZAR, E.Message, MSG_CAPTION, MB_ICONERROR);
                  Exit;
            end;
        end;
        Result := True;
    finally
    	spObtenerHistorialDeCobranza.EnableControls;
        CambiarEstadoCursor(CURSOR_DEFECTO);
    end
end;

end.
