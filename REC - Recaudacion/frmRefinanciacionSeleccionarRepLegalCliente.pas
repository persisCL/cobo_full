unit frmRefinanciacionSeleccionarRepLegalCliente;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ListBoxEx, DBListEx, DmiCtrls,
  frmRefinanciacionSeleccionarRepLegalCN,
  BuscaClientes, UtilDB, DMConnection, PeaTypes, PeaProcs, PeaProcsCN, Convenios, DB, DBClient;

type
  TRefinanciacionSeleccionarRepresentanteCliForm = class(TForm)
    GroupBox1: TGroupBox;
    DBListEx1: TDBListEx;
    Panel1: TPanel;
    btnCancelar: TButton;
    btnAceptar: TButton;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    peRut: TPickEdit;
    cdsRepresentantes: TClientDataSet;
    dsRepresentantes: TDataSource;
    Label1: TLabel;
    edtNombre: TEdit;
    edtApellido: TEdit;
    Label2: TLabel;
    Label4: TLabel;
    edtApellidoMaterno: TEdit;
    procedure peRutButtonClick(Sender: TObject);
    procedure peRutKeyPress(Sender: TObject; var Key: Char);
    procedure DBListEx1DblClick(Sender: TObject);
    procedure peRutChange(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
    FUltimaBusqueda: TBusquedaCliente;
  public
    { Public declarations }
    function Inicializar(Representantes: TRepresentantesLegales; Representante: TRefinanciacionRepLegal): Boolean;
  end;

var
  RefinanciacionSeleccionarRepresentanteCliForm: TRefinanciacionSeleccionarRepresentanteCliForm;

implementation

{$R *.dfm}


procedure TRefinanciacionSeleccionarRepresentanteCliForm.peRutButtonClick(Sender: TObject);
    var
        BuscaClientes: TFormBuscaClientes;
begin
    try
        peRut.OnChange := Nil;
        BuscaClientes := TFormBuscaClientes.Create(Self);
        if BuscaClientes.Inicializa(FUltimaBusqueda)then begin
            if BuscaClientes.ShowModal = mrOk then begin

                FUltimaBusqueda         := BuscaClientes.UltimaBusqueda;
                peRut.Text              := BuscaClientes.Persona.NumeroDocumento;
                edtNombre.Text          := BuscaClientes.Persona.Nombre;
                edtApellido.Text        := BuscaClientes.Persona.Apellido;
                edtApellidoMaterno.Text := BuscaClientes.Persona.ApellidoMaterno;
                btnAceptar.Enabled      := True;
            end;
        end;
    finally
        if Assigned(BuscaClientes) then FreeAndNil(BuscaClientes);
        peRut.OnChange := peRutChange;
    end;
end;

procedure TRefinanciacionSeleccionarRepresentanteCliForm.peRutChange(Sender: TObject);
begin
    edtNombre.Text             := '';
    edtApellido.Text           := '';
    edtApellidoMaterno.Text    := '';
    btnAceptar.Enabled         := False;
    edtNombre.Enabled          := False;
    edtApellido.Enabled        := False;
    edtApellidoMaterno.Enabled := False;
end;

procedure TRefinanciacionSeleccionarRepresentanteCliForm.peRutKeyPress(Sender: TObject; var Key: Char);
    var
        FPersona: TDatosPersonales;
    resourcestring
        rs_BUSQUEDA_TITULO = 'B�squeda Persona';
        rs_BUSQUEDA_NO_ENCONTRADA = 'El Rut introducido NO es Correcto.';
begin
    if key = #13 then begin
        FPersona := ObtenerDatosPersonales(DMConnections.BaseCAC, 'RUT', peRut.Text);

        if FPersona.CodigoPersona <> -1 then begin
            edtNombre.Text          := FPersona.Nombre;
            edtApellido.Text        := FPersona.Apellido;
            edtApellidoMaterno.Text := FPersona.ApellidoMaterno;
            btnAceptar.Enabled      := True;
        end
        else begin
            if ValidarRUT(DMConnections.BaseCAC, peRut.Text) then begin
                btnAceptar.Enabled         := True;
                edtNombre.Enabled          := True;
                edtApellido.Enabled        := True;
                edtApellidoMaterno.Enabled := True;
                edtNombre.SetFocus;
            end
            else begin
                btnAceptar.Enabled         := False;
                edtNombre.Enabled          := False;
                edtApellido.Enabled        := False;
                edtApellidoMaterno.Enabled := False;
                ShowMsgBoxCN(rs_BUSQUEDA_TITULO, rs_BUSQUEDA_NO_ENCONTRADA, MB_ICONWARNING, Self);
            end;
        end;
    end;
end;

procedure TRefinanciacionSeleccionarRepresentanteCliForm.btnAceptarClick(Sender: TObject);
    resourcestring
        rsTituloValidaciones    = 'Validaci�n Datos Representante Legal';
        rsErrorControl_Nombre   = 'El campo Nombre no puede estar en Blanco.';
        rsErrorControl_Apellido = 'El campo Apellido no puede estar en Blanco.';

    function ValidarCondiciones: boolean;
        var
            vControles  : Array [1..2] of TControl;
            vCondiciones: Array [1..2] of Boolean;
            vMensajes   : Array [1..2] of String;
    begin

        vControles[1]   := edtNombre;
        vControles[2]   := edtApellido;

        vCondiciones[1] := (Trim(edtNombre.Text) <> '');
        vCondiciones[2] := (Trim(edtApellido.Text) <> '');

        vMensajes[1]    := rsErrorControl_Nombre;
        vMensajes[2]    := rsErrorControl_Apellido;

        Result := ValidateControlsCN(vControles, vCondiciones, rsTituloValidaciones, vMensajes, Self);
    end;
begin
    if ValidarCondiciones then ModalResult := mrOk;
end;

procedure TRefinanciacionSeleccionarRepresentanteCliForm.btnCancelarClick(Sender: TObject);
begin
    ModalResult := mrCancel;
end;

procedure TRefinanciacionSeleccionarRepresentanteCliForm.DBListEx1DblClick(Sender: TObject);
begin
    if cdsRepresentantes.RecordCount > 0 then begin
        peRut.Text              := Trim(cdsRepresentantes.FieldByName('Rut').AsString);
        edtNombre.Text          := cdsRepresentantes.FieldByName('Nombre').AsString;
        edtApellido.Text        := cdsRepresentantes.FieldByName('Apellido').AsString;
        edtApellidoMaterno.Text := cdsRepresentantes.FieldByName('ApellidoMaterno').AsString;
        btnAceptar.Enabled      := True;
    end;
end;

procedure TRefinanciacionSeleccionarRepresentanteCliForm.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
    case Key of
        VK_ESCAPE: ModalResult := mrCancel;
    end;
end;

function TRefinanciacionSeleccionarRepresentanteCliForm.Inicializar(Representantes: TRepresentantesLegales; Representante: TRefinanciacionRepLegal): Boolean;
    var
        vl_Indice: Integer;
begin
    edtNombre.Enabled          := False;
    edtApellido.Enabled        := False;
    edtApellidoMaterno.Enabled := False;
    cdsRepresentantes.Open;

    if Representante.NumeroDocumento <> EmptyStr then begin
        peRut.Text              := Representante.NumeroDocumento;
        edtNombre.Text          := Representante.Nombre;
        edtApellido.Text        := Representante.Apellido;
        edtApellidoMaterno.Text := Representante.ApellidoMaterno;
        btnAceptar.Enabled      := True;
    end
    else btnAceptar.Enabled := False;

    if Representantes <> Nil then begin
        if Representantes.CantidadRepresentantes > 0 then begin
            for vl_Indice := 0 to Representantes.CantidadRepresentantes - 1 do begin
                cdsRepresentantes.AppendRecord(
                    [Representantes.Representantes[vl_Indice].Datos.NumeroDocumento,
                     Representantes.Representantes[vl_Indice].Datos.Nombre,
                     Representantes.Representantes[vl_Indice].Datos.Apellido,
                     Representantes.Representantes[vl_Indice].Datos.ApellidoMaterno]);
            end;
        end;
    end;

    Result := True;
end;

end.
