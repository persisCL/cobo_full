{-----------------------------------------------------------------------------
  Firma       :   SS_1120_MVI_20130820
  Descripcion :   Se modifica crea el procedimiento OnDrawItem para que llame
                  al procedimiento ItemComboBoxColor para pintar los convenios de baja.
                  Al llamar al procedimeinto CargarConveniosRUT se cambia para que traiga
                  los convenios en formato.
----------------------------------------------------------------------------}
unit frmProtestarRefinanciacion;

interface

uses
  // Developer
  frmMuestraMensaje, DMConnection, PeaProcs, PeaTypes, Util, Convenios, UtilDB, frmRefinanciacionCuotasHistorial,
  PeaProcsCN, SysUtilsCN, frmRefinanciacionPactarMora, 
  frmRefinanciacionCuotasPagosEfectivo, frmRefinanciacionEntrarEditarCuotas,
  frmRefinanciacionReporteRecibo, CobranzasResources,
  // Auto
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ListBoxEx, DBListEx, StdCtrls, ExtCtrls, ComCtrls, Validate, DateEdit, DmiCtrls, VariantComboBox, DB, DBClient, Provider,
  ADODB, ImgList, Contnrs;

type
  TProtestarRefinanciacionForm = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    lblNumRefinanciacion: TLabel;
    Label5: TLabel;
    lblEstado: TLabel;
    Label6: TLabel;
    lblTotalRefinanciado2: TLabel;
    vcbTipoRefinanciacion: TVariantComboBox;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    lblNombreCli: TLabel;
    Label7: TLabel;
    lblPersoneria: TLabel;
    lblConcesionaria: TLabel;
    peRut: TPickEdit;
    vcbConvenios: TVariantComboBox;
    dedtFechaRefi: TDateEdit;
    cbNotificacion: TCheckBox;
    GroupBox6: TGroupBox;
    PageControl1: TPageControl;
    tbsCuotas: TTabSheet;
    DBListEx3: TDBListEx;
    Panel7: TPanel;
    Label18: TLabel;
    lblCuotasCantidad: TLabel;
    Label20: TLabel;
    lblCuotasTotal: TLabel;
    pnlAcciones: TPanel;
    btnCancelar: TButton;
    spObtenerRefinanciacionCuotas: TADOStoredProc;
    dspObtenerRefinanciacionCuotas: TDataSetProvider;
    cdsCuotas: TClientDataSet;
    cdsCuotasCodigoEstadoCuota: TSmallintField;
    cdsCuotasDescripcionEstadoCuota: TStringField;
    cdsCuotasCodigoFormaPago: TSmallintField;
    cdsCuotasDescripcionFormaPago: TStringField;
    cdsCuotasTitularNombre: TStringField;
    cdsCuotasTitularTipoDocumento: TStringField;
    cdsCuotasTitularNumeroDocumento: TStringField;
    cdsCuotasCodigoBanco: TIntegerField;
    cdsCuotasDescripcionBanco: TStringField;
    cdsCuotasDocumentoNumero: TStringField;
    cdsCuotasFechaCuota: TDateTimeField;
    cdsCuotasCodigoTarjeta: TIntegerField;
    cdsCuotasDescripcionTarjeta: TStringField;
    cdsCuotasTarjetaCuotas: TSmallintField;
    cdsCuotasImporte: TLargeintField;
    dsCuotas: TDataSource;
    spObtenerRefinanciacionDatosCabecera: TADOStoredProc;
    cdsCuotasCodigoCuota: TSmallintField;
    btnProtestarRefinanciacion: TButton;
    lnCheck: TImageList;
    cdsCuotasProtestar: TBooleanField;
    spActualizarRefinanciacionCuotasEstados: TADOStoredProc;
    cdsCuotasSaldo: TLargeintField;
    spActualizarRefinanciacionProtestada: TADOStoredProc;
    lblProtestada: TLabel;
    procedure btnCancelarClick(Sender: TObject);
    procedure DBListEx3LinkClick(Sender: TCustomDBListEx; Column: TDBListExColumn);
    procedure DBListEx3DrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
    procedure btnProtestarRefinanciacionClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure vcbConveniosDrawItem(Control: TWinControl; Index: Integer;              //SS_1120_MVI_20130820
      Rect: TRect; State: TOwnerDrawState);                                           //SS_1120_MVI_20130820
  private
    { Private declarations }
    FPersona: TDatosPersonales;
    FCodigoRefinanciacion,
    FCodigoConvenio: Integer;
    FCuotasCantidad: Integer;
    FCuotasTotal,
    FComprobantesRefinanciadosTotal: Largeint;
    FAhora: TDate;
    FEdicionDatos: TEdicionDatos;
    FCodigoCanalPago: Integer;
    procedure MostrarDatosCliente;
    procedure MostrarCuotas;
    procedure ActualizarTotales;
    procedure MostrarDatosConvenio;
    procedure VerificaChecks;
  public
    { Public declarations }
    FComponentes: TObjectList;
    FAlgunaCuotaProtestada: Boolean;
    function Inicializar(Refinanciacion: Integer): Boolean;
  end;

var
  ProtestarRefinanciacionForm: TProtestarRefinanciacionForm;


implementation

{$R *.dfm}

procedure TProtestarRefinanciacionForm.btnProtestarRefinanciacionClick(Sender: TObject);
    resourcestring
        rs_ErrorEjecucionTitulo  = 'Error de Ejecuci�n';
        rs_ErrorEjecucionMensaje = 'Error en la ejecuci�n del procedimiento %s. C�digo de Error: %d.';

        rs_PROCESO_TITULO     = 'Protestar Refinanciaci�n';
        rs_PROCESO_MENSAJE    = '� Est� seguro de Protestar los Pagar�s de las Cuotas Seleccionadas ?';
        rs_PROCESO_MENSAJE_OK = 'Se Protestaron correctamente los Pagar�s de las Cuotas Seleccionadas.';
begin
    if ShowMsgBoxCN(rs_PROCESO_TITULO, rs_PROCESO_MENSAJE, MB_ICONQUESTION, Self) = mrOk then try
        CambiarEstadoCursor(CURSOR_RELOJ, Self, FComponentes);
        try

            cdsCuotas.DisableControls;
            cdsCuotas.First;

            QueryExecute(DMConnections.BaseCAC, 'BEGIN TRANSACTION ProtestarCuotas');

            while not cdsCuotas.Eof do begin
                if cdsCuotasProtestar.AsBoolean then begin
                    with spActualizarRefinanciacionCuotasEstados do begin
                        if Active then Close;
                        Parameters.Refresh;

                        with Parameters do begin
                            ParamByName('@CodigoRefinanciacion').Value := FCodigoRefinanciacion;
                            ParamByName('@CodigoCuota').Value          := cdsCuotasCodigoCuota.AsInteger;
                            ParamByName('@CodigoEstadoCuota').Value    := REFINANCIACION_ESTADO_CUOTA_PROTESTADO;
                            ParamByName('@Usuario').Value              := UsuarioSistema;
                        end;

                        ExecProc;

                        if Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
                            raise EErrorExceptionCN.Create(
                                rs_ErrorEjecucionTitulo,
                                Format(rs_ErrorEjecucionMensaje,
                                       [ProcedureName,
                                        Parameters.ParamByName('@RETURN_VALUE').Value]));
                        end
                        else begin
                            with spActualizarRefinanciacionProtestada do begin
                                if Active then Close;
                                Parameters.Refresh;

                                Parameters.ParamByName('@CodigoRefinanciacion').Value := FCodigoRefinanciacion;

                                ExecProc;

                                if Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
                                    raise EErrorExceptionCN.Create(
                                        rs_ErrorEjecucionTitulo,
                                        Format(rs_ErrorEjecucionMensaje,
                                               [ProcedureName,
                                                Parameters.ParamByName('@RETURN_VALUE').Value]));

                                end;
                            end;
                        end;
                    end;

                    cdsCuotas.Edit;
                    cdsCuotasProtestar.AsBoolean := False;
                    cdsCuotas.Post;
                end;

                cdsCuotas.Next;
            end;

            QueryExecute(DMConnections.BaseCAC, 'IF @@TRANCOUNT > 0 BEGIN COMMIT TRANSACTION ProtestarCuotas END');

            if cdsCuotas.Active then cdsCuotas.Close;
            cdsCuotas.Open;
            lblProtestada.Visible := True;

            ShowMsgBoxCN(rs_PROCESO_TITULO, rs_PROCESO_MENSAJE_OK, MB_ICONINFORMATION, Self);

        except
            on e: Exception do begin
            QueryExecute(DMConnections.BaseCAC, 'IF @@TRANCOUNT > 0 BEGIN ROLLBACK TRANSACTION ProtestarCuotas END');
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
        cdsCuotas.EnableControls;
        
        CambiarEstadoCursor(CURSOR_DEFECTO, Self, FComponentes);
    end;
end;

procedure TProtestarRefinanciacionForm.btnCancelarClick(Sender: TObject);
begin
    Close;
end;

procedure TProtestarRefinanciacionForm.DBListEx3DrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
var
    bmp: TBitMap;
begin
    with Sender do begin
        if (Column.FieldName = 'Importe') or (Column.FieldName = 'Saldo') then begin
            Text := FormatFloat(FORMATO_IMPORTE, Sender.DataSource.DataSet.FieldByName(Column.FieldName).AsInteger);
        end;

        if (Column.FieldName = 'Protestar') then begin
            Text := '';
            Canvas.FillRect(Rect);
            bmp := TBitMap.Create;
			DefaultDraw := False;
            try
                if (cdsCuotasProtestar.AsBoolean ) then begin
                    lnCheck.GetBitmap(0, Bmp);
                end
                else begin
                    if (cdsCuotasCodigoEstadoCuota.AsInteger = REFINANCIACION_ESTADO_CUOTA_PENDIENTE) and
                        (cdsCuotasCodigoFormaPago.AsInteger = FORMA_PAGO_PAGARE) and
                        (cdsCuotasImporte.AsLargeInt = cdsCuotasSaldo.AsLargeInt) then begin
                        lnCheck.GetBitmap(1, Bmp);
                    end
                    else lnCheck.GetBitmap(3, Bmp);
                end;

                Canvas.Draw((Rect.Left + Rect.Right - Bmp.Width) div 2, Rect.Top, bmp);
            finally
                bmp.Free;
            end; // finally
        end; // if
    end; // with
end;

procedure TProtestarRefinanciacionForm.DBListEx3LinkClick(Sender: TCustomDBListEx; Column: TDBListExColumn);
begin
{
    if  (cdsCuotasCodigoEstadoCuota.AsInteger = REFINANCIACION_ESTADO_CUOTA_PENDIENTE) and
        (cdsCuotasCodigoFormaPago.AsInteger = FORMA_PAGO_PAGARE) and
        (cdsCuotasImporte.AsLargeInt = cdsCuotasSaldo.AsLargeInt) then begin

            cdsCuotas.Edit;
            cdsCuotasProtestar.AsBoolean := not cdsCuotasProtestar.AsBoolean;
            cdsCuotas.Post;

            VerificaChecks;
    end
    else ShowMsgBoxCN('Validaci�n Protesto Cuota', 'La Cuota NO se encuentra en estado Pendiente o NO es un Pagar� o Tiene Pagos a Cuenta.' + CRLF + CRLF + ' No se puede Protestar la Cuota.', MB_ICONWARNING, Self);
    }
end;

procedure TProtestarRefinanciacionForm.FormShow(Sender: TObject);
begin
    TPanelMensajesForm.OcultaPanel;
end;

function TProtestarRefinanciacionForm.Inicializar(Refinanciacion: Integer): Boolean;
begin
    try
        try
            Result := False;
            CambiarEstadoCursor(CURSOR_RELOJ);

            CargarConstantesCobranzasRefinanciacion;
            CargarConstantesCobranzasCanalesFormasPago;

            TPanelMensajesForm.MuestraMensaje('inicializando Formulario Protestar Refinanciaci�n ...');

            FAhora := Trunc(NowBaseSmall(DMConnections.BaseCAC));

            CargarCanalesDePago(DMConnections.BaseCAC, vcbTipoRefinanciacion, 0, 1);

            cdsCuotas.Open;

            FCodigoRefinanciacion := Refinanciacion;

            TPanelMensajesForm.MuestraMensaje(Format('inicializando Refinanciaci�n Nro. %d ...',[FCodigoRefinanciacion]));
            TPanelMensajesForm.MuestraMensaje('Cargando Datos Principales ...');

            with spObtenerRefinanciacionDatosCabecera do begin
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoRefinanciacion').Value := FCodigoRefinanciacion;
                Open;

                FPersona              := ObtenerDatosPersonales(DMConnections.BaseCAC,'','', FieldByName('CodigoPersona').AsInteger);
                FCodigoConvenio       := FieldByName('CodigoConvenio').AsInteger;
                lblEstado.Caption     := FieldByName('EstadoRefinanciacion').AsString;
                lblProtestada.Visible := FieldByName('Protestada').AsBoolean;

                FComprobantesRefinanciadosTotal := FieldByName('TotalDeuda').AsInteger;

                lblNumRefinanciacion.Caption := IntToStr(Refinanciacion);
                dedtFechaRefi.Date           := FieldByName('Fecha').AsDateTime;
                FCodigoCanalPago             := FieldByName('CodigoTipoMedioPago').AsInteger;
                vcbTipoRefinanciacion.Value  := FCodigoCanalPago;
                cbNotificacion.Checked       := iif( FieldByName('Notificacion').AsBoolean, True, False);

                if FPersona.CodigoPersona <> -1 then begin
                    TPanelMensajesForm.MuestraMensaje('Cargando Datos Cliente ...');
                    MostrarDatosCliente;

                    TPanelMensajesForm.MuestraMensaje('Cargando Datos Convenios ...');
                    CargarConveniosRUT(DMConnections.BaseCAC, vcbConvenios, FPersona.TipoDocumento, FPersona.NumeroDocumento, False, FCodigoConvenio, False, True);   ////SS_1120_MVI_20130820

                    if FCodigoConvenio <> 0 then MostrarDatosConvenio;
                end;

                TPanelMensajesForm.MuestraMensaje('Cargando Datos Cuotas ...');
                MostrarCuotas;

                btnProtestarRefinanciacion.Enabled := FAlgunaCuotaProtestada;
            end;

            Result := True;
        except
            on e:Exception do begin
                raise EErrorExceptionCN.Create('TActivarRefinancicionForm.Inicializar', e.Message);
            end;
        end;
    finally
        CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

procedure TProtestarRefinanciacionForm.MostrarDatosCliente;
    const
        SQLObtenerNombrePersona   = 'SELECT dbo.ObtenerNombrePersona(%d)';
begin
    peRut.Text           := FPersona.NumeroDocumento;
    lblNombreCli.Caption := QueryGetValue(DMConnections.BaseCAC, Format(SQLObtenerNombrePersona, [FPersona.CodigoPersona]));

    if FPersona.Personeria = PERSONERIA_FISICA then begin
        lblPersoneria.Caption := ' - (Persona Natural)';
    end
    else begin
        if FPersona.Personeria = PERSONERIA_JURIDICA then lblPersoneria.Caption := ' - (Persona Jur�dica)';
        if FPersona.Personeria = PERSONERIA_AMBAS then lblPersoneria.Caption := ' - (Persona Natural y Jur�dica)';
    end;

    lblPersoneria.Left := lblNombreCli.Left + lblNombreCli.Width;
end;

procedure TProtestarRefinanciacionForm.MostrarCuotas;
begin
    if cdsCuotas.Active then cdsCuotas.Close;

    with spObtenerRefinanciacionCuotas do begin
        Parameters.Refresh;
        Parameters.ParamByName('@CodigoRefinanciacion').Value := FCodigoRefinanciacion;
        Parameters.ParamByName('@CodigoCuota').Value          := Null;
        Parameters.ParamByName('@SinCuotasAnuladas').Value    := 0;
    end;

    with cdsCuotas do try
        dsCuotas.DataSet := Nil;
        Open;

        FCuotasTotal  := 0;
        FAlgunaCuotaProtestada := False;

        while not Eof do begin
            if not (cdsCuotasCodigoEstadoCuota.AsInteger in [REFINANCIACION_ESTADO_CUOTA_ANULADA, REFINANCIACION_ESTADO_CUOTA_ANULADA_POR_MORA, REFINANCIACION_ESTADO_CUOTA_ANULADA_POR_PROTESTO]) then begin
                FCuotasTotal := FCuotasTotal + cdsCuotasImporte.AsLargeInt;
            end;

            if  (cdsCuotasCodigoEstadoCuota.AsInteger = REFINANCIACION_ESTADO_CUOTA_PENDIENTE) and
                (cdsCuotasCodigoFormaPago.AsInteger = FORMA_PAGO_PAGARE) and
                (cdsCuotasImporte.AsLargeInt = cdsCuotasSaldo.AsLargeInt) then begin

                    cdsCuotas.Edit;
                    cdsCuotasProtestar.AsBoolean := not cdsCuotasProtestar.AsBoolean;
                    cdsCuotas.Post;

                    FAlgunaCuotaProtestada := True;
            end;

            Next;
        end;
    finally
        dsCuotas.DataSet := cdsCuotas;
        First;
    end;

    FCuotasCantidad := cdsCuotas.RecordCount;

    ActualizarTotales;
end;

procedure TProtestarRefinanciacionForm.ActualizarTotales;
    resourcestring
        rsValidacionImportesTitulo  = 'Validaci�n Total Cuotas';
        rsValidacionImportesMensaje = 'El Importe Total de las Cuotas Entradas es Mayor que el Total de Comprobantes Refinanciados.';
begin
    // Totales Comprobantes Refinanciados
    lblTotalRefinanciado2.Caption := FormatFloat(FORMATO_IMPORTE, FComprobantesRefinanciadosTotal);
    // Totales Cuotas Refinanciaci�n
    lblCuotasCantidad.Caption     := IntToStr(FCuotasCantidad);
    lblCuotasTotal.Caption        := FormatFloat(FORMATO_IMPORTE, FCuotasTotal);

    if FCuotasTotal <> FComprobantesRefinanciadosTotal then
        lblCuotasTotal.Font.Color := clRed
    else lblCuotasTotal.Font.Color := clGreen;
end;

procedure TProtestarRefinanciacionForm.MostrarDatosConvenio;
begin
    lblConcesionaria.Caption := QueryGetValue(DMConnections.BaseCAC, Format('SELECT dbo.ObtenerConcesionariaConvenio(%d)', [FCodigoConvenio]));
end;

//BEGIN:SS_1120_MVI_20130820 -------------------------------------------------
{-----------------------------------------------------------------------------
  Procedure: TProtestarRefinanciacionForm.vcbConveniosDrawItem
  Date:      20-August-2013
  Description: Verifica si existe alg�n convenio de baja para llamar a la funci�n
                que colorea de rojo el item.
-----------------------------------------------------------------------------}
procedure TProtestarRefinanciacionForm.vcbConveniosDrawItem(
  Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  if Pos( TXT_CONVENIO_DE_BAJA, vcbConvenios.Items[Index].Caption ) > 0 then  begin

     ItemComboBoxColor(Control, Index, Rect, State, clred, true);

  end
  else begin

     ItemComboBoxColor(Control, Index, Rect, State);

  end;                                                                                

end;
//END:SS_1120_MVI_20130820 -------------------------------------------------

procedure TProtestarRefinanciacionForm.VerificaChecks;
    var
        Puntero: TBookmark;
        AlgunaCuotaProtestada: Boolean;
begin
    try
        CambiarEstadoCursor(CURSOR_RELOJ);

        with cdsCuotas do begin
            Puntero := GetBookmark;
            DisableControls;

            AlgunaCuotaProtestada := False;
            First;

            while (not Eof) and (not AlgunaCuotaProtestada) do begin
                AlgunaCuotaProtestada := cdsCuotasProtestar.AsBoolean;

                Next;
            end;

            if Assigned(Puntero) then begin
                if BookmarkValid(Puntero) then GotoBookmark(Puntero);
                FreeBookmark(Puntero);
            end;

            EnableControls;
        end;
    finally
        CambiarEstadoCursor(CURSOR_DEFECTO);

        btnProtestarRefinanciacion.Enabled := AlgunaCuotaProtestada;
    end;
end;

end.
