{

Etiqueta    : 20160315 MGO
Descripci�n : Se utiliza InstallIni en vez de ApplicationIni

}
unit ConfigurarPuesto;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, DeclTag, DeclHard, DeclCtl, Util, UtilProc, Peaprocs, DMConnection,
  DPSControls;

type
  TFormConfigurarPuesto = class(TForm)
    gb_Antena: TGroupBox;
    Label1: TLabel;
    txt_bibLIC: TEdit;
    btn_setupLIC: TDPSButton;
    btn_Aceptar: TDPSButton;
    btn_Cancelar: TDPSButton;
    LIC: TTagReader;
    gb_PuestoDeTrabajo: TGroupBox;
    cbOperadoresLogisticos: TComboBox;
    cbPuntosVenta: TComboBox;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    cbLugaresVenta: TComboBox;
    procedure btn_setupLICClick(Sender: TObject);
	procedure btn_AceptarClick(Sender: TObject);
    procedure cbOperadoresLogisticosChange(Sender: TObject);
    procedure cbLugaresVentaChange(Sender: TObject);
  private
	{ Private declarations }
  public
	{ Public declarations }
	Function Inicializa: Boolean;
  end;

var
  FormConfigurarPuesto: TFormConfigurarPuesto;

implementation

{$R *.DFM}

resourcestring
    CAPTION_CONFIGURAR = 'Configuraci�n de Puesto de Trabajo';

{ TForm1 }

function TFormConfigurarPuesto.Inicializa: Boolean;
var
    CodigoOperador, CodigoLugarVenta, CodigoPuntoVenta: integer;
begin
    { INICIO : 20160315 MGO
    CodigoOperador 		:= ApplicationIni.ReadInteger('Puesto', 'Operador', -1);
	CodigoLugarVenta	:= ApplicationIni.ReadInteger('Puesto', 'LugarVenta', -1);
	CodigoPuntoVenta	:= ApplicationIni.ReadInteger('Puesto', 'PuntoVenta', -1);

    LIC.DLL 		:= ApplicationIni.ReadString('Bibliotecas', 'LIC', LIC.DLL);
	}
    CodigoOperador 		:= InstallIni.ReadInteger('Puesto', 'Operador', -1);
	CodigoLugarVenta	:= InstallIni.ReadInteger('Puesto', 'LugarVenta', -1);
	CodigoPuntoVenta	:= InstallIni.ReadInteger('Puesto', 'PuntoVenta', -1);

    LIC.DLL 		:= InstallIni.ReadString('Bibliotecas', 'LIC', LIC.DLL);
	// FIN : 20160315 MGO
    Screen.Cursor 	:= crHourGlass;
	btn_setupLIC.Enabled := False;//LIC.CanSetup;
    Screen.Cursor   := crDefault;
	txt_bibLIC.text := LIC.DLL;
	Result := True;
end;

procedure TFormConfigurarPuesto.btn_setupLICClick(Sender: TObject);
resourcestring
    MSG_REINICIAR = 'Los cambios tendr�n efecto cuando reinicie el programa.';
begin
	if LIC.Setup then MsgBox(MSG_REINICIAR, CAPTION_CONFIGURAR, MB_ICONWARNING);
end;

procedure TFormConfigurarPuesto.btn_AceptarClick(Sender: TObject);
resourcestring
    MSG_CERRAR_TURNO = 'Deber� cerrar el turno y reingresar en el sistema.';
begin
    { INICIO : 20160315 MGO
	ApplicationIni.WriteString('Bibliotecas', 'LIC', txt_bibLIC.text);

    ApplicationIni.WriteInteger('Puesto', 'Operador', Ival(StrRight(cbOperadoresLogisticos.Text, 10)));
    ApplicationIni.WriteInteger('Puesto', 'LugarVenta', Ival(StrRight(cbLugaresVenta.Text, 10)));
    ApplicationIni.WriteInteger('Puesto', 'PuntoVenta', Ival(StrRight(cbPuntosVenta.Text, 10)));
    }
    InstallIni.WriteString('Bibliotecas', 'LIC', txt_bibLIC.text);
    
    InstallIni.WriteInteger('Puesto', 'Operador', Ival(StrRight(cbOperadoresLogisticos.Text, 10)));
    InstallIni.WriteInteger('Puesto', 'LugarVenta', Ival(StrRight(cbLugaresVenta.Text, 10)));
    InstallIni.WriteInteger('Puesto', 'PuntoVenta', Ival(StrRight(cbPuntosVenta.Text, 10)));
    // FIN : 20160315 MGO

    MsgBox(MSG_CERRAR_TURNO, CAPTION_CONFIGURAR, MB_ICONINFORMATION);
    ModalResult := mrOk;
end;

procedure TFormConfigurarPuesto.cbOperadoresLogisticosChange(Sender: TObject);
begin
    (** FIXME 12/08/2004 Comentado para que compile. Damian. Corregir o borrar!
    CargarLugaresDeVenta(DMConnections.BaseCAC, cbLugaresVenta, Ival(StrRight(cbOperadoresLogisticos.Text, 10)));
    CargarPuntosVenta(DMConnections.BaseCAC, cbPuntosVenta, Ival(StrRight(cbLugaresVenta.Text, 10)));
    *)
end;

procedure TFormConfigurarPuesto.cbLugaresVentaChange(Sender: TObject);
begin
    (** FIXME 12/08/2004 Comentado para que compile. Damian. Corregir o borrar!
    CargarPuntosVenta(DMConnections.BaseCAC, cbPuntosVenta, Ival(StrRight(cbLugaresVenta.Text, 10)));*)
end;

end.
