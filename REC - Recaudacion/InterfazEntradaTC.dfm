object FrmInterfazEntradaTC: TFrmInterfazEntradaTC
  Left = 295
  Top = 197
  BorderStyle = bsDialog
  Caption = 'Interfaz de Entrada de Tarjetas de Cr'#233'dito'
  ClientHeight = 140
  ClientWidth = 457
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 457
    Height = 105
    BevelOuter = bvLowered
    TabOrder = 0
    object Label4: TLabel
      Left = 10
      Top = 50
      Width = 113
      Height = 13
      Caption = '&Tarjetas de Cr'#233'dito:'
      FocusControl = cbTarjetasCredito
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 10
      Top = 18
      Width = 114
      Height = 13
      Caption = '&Archivo de Entrada:'
      FocusControl = peArchivo
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object cbTarjetasCredito: TComboBox
      Left = 144
      Top = 47
      Width = 305
      Height = 19
      Style = csOwnerDrawFixed
      ItemHeight = 13
      TabOrder = 1
    end
    object ProgressBar: TProgressBar
      Left = 13
      Top = 80
      Width = 433
      Height = 16
      Min = 0
      Max = 100
      TabOrder = 2
    end
    object peArchivo: TPickEdit
      Left = 145
      Top = 14
      Width = 303
      Height = 21
      Enabled = True
      TabOrder = 0
      OnChange = peArchivoChange
      Decimals = 0
      EditorStyle = bteTextEdit
      OnButtonClick = peArchivoButtonClick
    end
  end
  object btnAceptar: TDPSButton
    Left = 297
    Top = 111
    Caption = '&Aceptar'
    Default = True
    ModalResult = 1
    TabOrder = 1
    OnClick = btnAceptarClick
  end
  object btnCancelar: TDPSButton
    Left = 377
    Top = 111
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 2
    OnClick = btnCancelarClick
  end
  object OpenDialog: TOpenDialog
    DefaultExt = 'TXT'
    Filter = 'Archivo de texto|*.TXT'
    Options = [ofReadOnly, ofHideReadOnly, ofNoChangeDir, ofEnableSizing]
    Left = 56
    Top = 8
  end
end
