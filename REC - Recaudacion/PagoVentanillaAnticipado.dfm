object frmPagoVentanillaAnticipado: TfrmPagoVentanillaAnticipado
  Left = 261
  Top = 130
  ActiveControl = edtCodigoFormaPago
  BorderStyle = bsDialog
  Caption = 'Cobro Anticipado'
  ClientHeight = 404
  ClientWidth = 683
  Color = clBtnFace
  Constraints.MinHeight = 429
  Constraints.MinWidth = 572
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  DesignSize = (
    683
    404)
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 8
    Top = 8
    Width = 667
    Height = 100
  end
  object lbl_FormaPago: TLabel
    Left = 226
    Top = 16
    Width = 71
    Height = 13
    Caption = 'Forma de pago'
  end
  object Label2: TLabel
    Left = 28
    Top = 60
    Width = 73
    Height = 13
    Caption = 'Fecha de Pago'
  end
  object Label3: TLabel
    Left = 132
    Top = 73
    Width = 185
    Height = 27
    AutoSize = False
    Caption = 
      'Si desea cambiar la fecha  con la que se registrar'#225' el pago haga' +
      ' click'
    Transparent = True
    WordWrap = True
  end
  object lblaca: TLabel
    Left = 287
    Top = 86
    Width = 18
    Height = 13
    Cursor = crHandPoint
    Caption = 'ac'#225
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsUnderline]
    ParentFont = False
    OnClick = lblacaClick
  end
  object lbl_CanalDePago: TLabel
    Left = 28
    Top = 16
    Width = 69
    Height = 13
    Caption = 'Canal de pago'
  end
  object lbl_MontoPagado: TLabel
    Left = 437
    Top = 16
    Width = 76
    Height = 13
    Caption = 'Monto Pagado: '
  end
  object Bevel2: TBevel
    Left = 8
    Top = 116
    Width = 667
    Height = 243
  end
  object btnCerrar: TButton
    Left = 601
    Top = 374
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Cerrar'
    TabOrder = 6
    OnClick = btnCerrarClick
  end
  object deFechaPago: TDateEdit
    Left = 28
    Top = 76
    Width = 97
    Height = 21
    AutoSelect = False
    Enabled = False
    TabOrder = 3
    OnChange = deFechaPagoChange
    Date = -693594.000000000000000000
  end
  object btnAceptar: TButton
    Left = 523
    Top = 374
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Aceptar'
    Default = True
    TabOrder = 5
    OnClick = btnAceptarClick
  end
  object nbOpciones: TNotebook
    Left = 28
    Top = 128
    Width = 629
    Height = 217
    TabOrder = 4
    Visible = False
    object TPage
      Left = 0
      Top = 0
      Caption = 'NoDefinido'
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Label29: TLabel
        Left = 162
        Top = 79
        Width = 286
        Height = 24
        Caption = 'Entrada de Datos NO Definida.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
    object TPage
      Left = 0
      Top = 0
      Caption = 'Efectivo'
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Label20: TLabel
        Left = 81
        Top = 75
        Width = 140
        Height = 13
        Caption = 'Ingrese Monto Recibido:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label1: TLabel
        Left = 179
        Top = 97
        Width = 41
        Height = 13
        Caption = 'Vuelto:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = 80
        Top = 24
        Width = 349
        Height = 14
        Caption = 'Complete los siguientes datos para completar y registrar el pago'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblCambio: TLabel
        Left = 224
        Top = 74
        Width = 105
        Height = 10
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object nePagacon: TNumericEdit
        Left = 224
        Top = 69
        Width = 105
        Height = 21
        MaxLength = 8
        TabOrder = 0
        OnChange = nePagaconChange
        OnKeyPress = neImporteKeyPress
      end
    end
    object TPage
      Left = 0
      Top = 0
      Caption = 'Tarjetas'
      ExplicitWidth = 0
      ExplicitHeight = 0
      object lblFechaCheque: TLabel
        Left = 102
        Top = 67
        Width = 75
        Height = 13
        Caption = 'Autorizaci'#243'n:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label5: TLabel
        Left = 102
        Top = 107
        Width = 41
        Height = 13
        Caption = 'Cup'#243'n:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 80
        Top = 24
        Width = 349
        Height = 14
        Caption = 'Complete los siguientes datos para completar y registrar el pago'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label7: TLabel
        Left = 102
        Top = 146
        Width = 44
        Height = 13
        Caption = 'Cuotas:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object edAutorizacion: TEdit
        Left = 199
        Top = 62
        Width = 208
        Height = 21
        CharCase = ecUpperCase
        Color = 16444382
        MaxLength = 20
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
      end
      object edCupon: TNumericEdit
        Left = 199
        Top = 102
        Width = 129
        Height = 21
        Color = 16444382
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        OnKeyPress = neImporteKeyPress
      end
      object edCuotas: TNumericEdit
        Left = 199
        Top = 142
        Width = 129
        Height = 21
        Color = 16444382
        TabOrder = 2
        OnKeyPress = neImporteKeyPress
        Value = 1.000000000000000000
      end
    end
    object TPage
      Left = 0
      Top = 0
      Caption = 'Cheque'
      object lblSucursalCheque: TLabel
        Left = 30
        Top = 131
        Width = 76
        Height = 13
        Caption = '&Nro. Cheque:'
        Color = clBtnFace
        FocusControl = txtNumeroCheque
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object Label8: TLabel
        Left = 30
        Top = 107
        Width = 41
        Height = 13
        Caption = '&Banco:'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object Label10: TLabel
        Left = 105
        Top = 24
        Width = 349
        Height = 14
        Caption = 'Complete los siguientes datos para completar y registrar el pago'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblRUT: TLabel
        Left = 30
        Top = 84
        Width = 31
        Height = 13
        Caption = 'RUT:'
        FocusControl = txtNombreTitular
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label11: TLabel
        Left = 30
        Top = 61
        Width = 41
        Height = 13
        Caption = 'Titular:'
        FocusControl = txtNombreTitular
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label12: TLabel
        Left = 30
        Top = 156
        Width = 87
        Height = 13
        Caption = 'Fecha Cheque:'
        Color = clBtnFace
        FocusControl = txtNumeroCheque
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object btnDatosClienteCheque: TSpeedButton
        Left = 516
        Top = 57
        Width = 22
        Height = 21
        Hint = 'Copia los datos del Cliente'
        Glyph.Data = {
          AA040000424DAA04000000000000360000002800000013000000130000000100
          1800000000007404000000000000000000000000000000000000D8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9
          ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9
          ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
          D8E9EC840000840000840000840000840000840000840000840000840000D8E9
          ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC84
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000D8E9ECD8E9EC
          D8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC840000FFFF
          FF000000000000000000000000000000FFFFFF840000D8E9ECD8E9ECD8E9EC00
          0000D8E9EC000000000000000000000000000000000000840000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9
          EC000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000FFFFFF00000000000000
          0000000000000000FFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000
          FFFFFF000000000000000000000000840000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFF840000FFFFFF000000000000FFFFFF840000840000
          840000840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFF0000000000
          00000000000000840000FFFFFFFFFFFFFFFFFFFFFFFF840000FFFFFF840000D8
          E9ECD8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF840000FFFFFFFFFFFFFFFFFFFFFFFF840000840000D8E9ECD8E9ECD8E9
          ECD8E9ECD8E9EC000000D8E9EC000000FFFFFF000000000000FFFFFF00000084
          0000840000840000840000840000840000D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
          D8E9EC000000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF0000
          00D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC00
          0000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF000000000000D8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9
          EC000000000000000000000000000000000000D8E9ECD8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9
          ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9
          ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9EC000000}
        ParentShowHint = False
        ShowHint = True
        OnClick = btnDatosClienteChequeClick
      end
      object Label30: TLabel
        Left = 30
        Top = 179
        Width = 105
        Height = 13
        Caption = 'Nro. Repactaci'#243'n:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object txtNumeroCheque: TEdit
        Left = 152
        Top = 128
        Width = 238
        Height = 21
        CharCase = ecUpperCase
        Color = clWhite
        MaxLength = 20
        TabOrder = 3
      end
      object cbbancos: TVariantComboBox
        Left = 152
        Top = 104
        Width = 237
        Height = 21
        Style = vcsDropDownList
        Color = clWhite
        ItemHeight = 13
        TabOrder = 2
        OnChange = cbbancosChange
        Items = <>
      end
      object txtNombreTitular: TEdit
        Left = 152
        Top = 57
        Width = 361
        Height = 21
        Hint = 'Nombre del titular de la tarjeta de cr'#233'dito'
        CharCase = ecUpperCase
        Color = clWhite
        MaxLength = 100
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
      end
      object txtDocumento: TEdit
        Left = 152
        Top = 81
        Width = 108
        Height = 21
        Hint = 'RUT'
        CharCase = ecUpperCase
        Color = clWhite
        MaxLength = 9
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object deFechaCheque: TDateEdit
        Left = 152
        Top = 152
        Width = 90
        Height = 21
        AutoSelect = False
        TabOrder = 4
        Date = -693594.000000000000000000
      end
      object edtRepactacionCheque: TEdit
        Left = 152
        Top = 176
        Width = 238
        Height = 21
        TabOrder = 5
      end
    end
    object TPage
      Left = 0
      Top = 0
      Caption = 'Vale Vista'
      object Label13: TLabel
        Left = 116
        Top = 24
        Width = 349
        Height = 14
        Caption = 'Complete los siguientes datos para completar y registrar el pago'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label14: TLabel
        Left = 30
        Top = 61
        Width = 72
        Height = 13
        Caption = 'Tomado por:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label15: TLabel
        Left = 30
        Top = 84
        Width = 31
        Height = 13
        Caption = 'RUT:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label16: TLabel
        Left = 30
        Top = 107
        Width = 41
        Height = 13
        Caption = 'Banco:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label17: TLabel
        Left = 30
        Top = 131
        Width = 90
        Height = 13
        Caption = 'Nro. Vale Vista:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label19: TLabel
        Left = 30
        Top = 156
        Width = 101
        Height = 13
        Caption = 'Fecha Vale Vista:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object btn_DatosClienteValeVista: TSpeedButton
        Left = 516
        Top = 57
        Width = 22
        Height = 21
        Hint = 'Copia los datos del Cliente'
        Glyph.Data = {
          AA040000424DAA04000000000000360000002800000013000000130000000100
          1800000000007404000000000000000000000000000000000000D8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9
          ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9
          ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
          D8E9EC840000840000840000840000840000840000840000840000840000D8E9
          ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC84
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000D8E9ECD8E9EC
          D8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC840000FFFF
          FF000000000000000000000000000000FFFFFF840000D8E9ECD8E9ECD8E9EC00
          0000D8E9EC000000000000000000000000000000000000840000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9
          EC000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000FFFFFF00000000000000
          0000000000000000FFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000
          FFFFFF000000000000000000000000840000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFF840000FFFFFF000000000000FFFFFF840000840000
          840000840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFF0000000000
          00000000000000840000FFFFFFFFFFFFFFFFFFFFFFFF840000FFFFFF840000D8
          E9ECD8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF840000FFFFFFFFFFFFFFFFFFFFFFFF840000840000D8E9ECD8E9ECD8E9
          ECD8E9ECD8E9EC000000D8E9EC000000FFFFFF000000000000FFFFFF00000084
          0000840000840000840000840000840000D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
          D8E9EC000000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF0000
          00D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC00
          0000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF000000000000D8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9
          EC000000000000000000000000000000000000D8E9ECD8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9
          ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9
          ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9EC000000}
        OnClick = btn_DatosClienteValeVistaClick
      end
      object txt_TomadoPorVV: TEdit
        Left = 152
        Top = 57
        Width = 361
        Height = 21
        MaxLength = 100
        TabOrder = 0
      end
      object txt_RUTVV: TEdit
        Left = 152
        Top = 81
        Width = 108
        Height = 21
        MaxLength = 11
        TabOrder = 1
      end
      object txt_NroVV: TEdit
        Left = 152
        Top = 128
        Width = 238
        Height = 21
        MaxLength = 20
        TabOrder = 3
      end
      object cb_BancosVV: TVariantComboBox
        Left = 152
        Top = 104
        Width = 237
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 13
        TabOrder = 2
        OnChange = cb_BancosVVChange
        Items = <>
      end
      object de_FechaVV: TDateEdit
        Left = 152
        Top = 152
        Width = 90
        Height = 21
        AutoSelect = False
        TabOrder = 4
        Date = -693594.000000000000000000
      end
    end
    object TPage
      Left = 0
      Top = 0
      Caption = 'DebitoCuentaPAC'
      object lbl_DebitoCuentaPAC: TLabel
        Left = 202
        Top = 24
        Width = 105
        Height = 14
        Caption = 'D'#233'bito Cuenta PAC'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lbl_CodigoBancoPAC: TLabel
        Left = 63
        Top = 61
        Width = 41
        Height = 13
        Caption = 'Banco:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbl_NroCuentaBancariaPAC: TLabel
        Left = 63
        Top = 85
        Width = 117
        Height = 13
        Caption = 'N'#186' Cuenta Bancaria:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbl_SucursalPAC: TLabel
        Left = 63
        Top = 109
        Width = 54
        Height = 13
        Caption = 'Sucursal:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbl_TitularPAC: TLabel
        Left = 63
        Top = 133
        Width = 41
        Height = 13
        Caption = 'Titular:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object txt_NroCuentaBancariaPAC: TEdit
        Left = 193
        Top = 81
        Width = 121
        Height = 21
        TabOrder = 1
      end
      object txt_SucursalPAC: TEdit
        Left = 193
        Top = 105
        Width = 253
        Height = 21
        TabOrder = 2
      end
      object txt_TitularPAC: TEdit
        Left = 193
        Top = 129
        Width = 253
        Height = 21
        TabOrder = 3
      end
      object cb_BancosPAC: TVariantComboBox
        Left = 193
        Top = 57
        Width = 253
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 13
        TabOrder = 0
        Items = <>
      end
    end
    object TPage
      Left = 0
      Top = 0
      Caption = 'DebitoCuentaPAT'
      object lbl_DebitoCuentaPAT: TLabel
        Left = 202
        Top = 24
        Width = 106
        Height = 14
        Caption = 'D'#233'bito Cuenta PAT'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lbl_CodigoTipoTarjetaCreditoPAT: TLabel
        Left = 62
        Top = 61
        Width = 118
        Height = 13
        Caption = 'Tipo Tarjeta Cr'#233'dito:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbl_NumeroTarjetaCreditoPAT: TLabel
        Left = 62
        Top = 85
        Width = 107
        Height = 13
        Caption = 'N'#186' Tarjeta Cr'#233'dito:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbl_FechaVencimientoPAT: TLabel
        Left = 62
        Top = 109
        Width = 113
        Height = 13
        Caption = 'Fecha Vencimiento:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbl_CodigoEmisorTarjetaCreditoPAT: TLabel
        Left = 62
        Top = 133
        Width = 130
        Height = 13
        Caption = 'Emisor Tarjeta Cr'#233'dito:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object txt_NumeroTarjetaCreditoPAT: TEdit
        Left = 202
        Top = 81
        Width = 158
        Height = 21
        MaxLength = 20
        TabOrder = 1
      end
      object txt_FechaVencimiento: TDateEdit
        Left = 202
        Top = 105
        Width = 90
        Height = 21
        AutoSelect = False
        TabOrder = 2
        Date = -693594.000000000000000000
      end
      object cb_TipoTarjetaCreditoPAT: TVariantComboBox
        Left = 202
        Top = 57
        Width = 159
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 13
        TabOrder = 0
        Items = <>
      end
      object cb_EmisorTarjetaCreditoPAT: TVariantComboBox
        Left = 202
        Top = 129
        Width = 246
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 13
        TabOrder = 3
        Items = <>
      end
    end
    object TPage
      Left = 0
      Top = 0
      Caption = 'TarjetaCredito'
      object Label23: TLabel
        Left = 95
        Top = 53
        Width = 41
        Height = 13
        Caption = 'Titular:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label24: TLabel
        Left = 95
        Top = 83
        Width = 41
        Height = 13
        Caption = 'Banco:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label25: TLabel
        Left = 92
        Top = 110
        Width = 45
        Height = 13
        Caption = 'Tarjeta:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label26: TLabel
        Left = 30
        Top = 137
        Width = 106
        Height = 13
        Caption = 'Nro. Confirmaci'#243'n:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label27: TLabel
        Left = 92
        Top = 164
        Width = 44
        Height = 13
        Caption = 'Cuotas:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label28: TLabel
        Left = 132
        Top = 16
        Width = 349
        Height = 14
        Caption = 'Complete los siguientes datos para completar y registrar el pago'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object spbTarjetaCreditoCopiarDatosCliente: TSpeedButton
        Left = 487
        Top = 51
        Width = 23
        Height = 22
        Hint = 'Copia los datos del Cliente'
        Glyph.Data = {
          AA040000424DAA04000000000000360000002800000013000000130000000100
          1800000000007404000000000000000000000000000000000000D8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9
          ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9
          ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
          D8E9EC840000840000840000840000840000840000840000840000840000D8E9
          ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC84
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000D8E9ECD8E9EC
          D8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC840000FFFF
          FF000000000000000000000000000000FFFFFF840000D8E9ECD8E9ECD8E9EC00
          0000D8E9EC000000000000000000000000000000000000840000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9
          EC000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000FFFFFF00000000000000
          0000000000000000FFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000
          FFFFFF000000000000000000000000840000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFF840000FFFFFF000000000000FFFFFF840000840000
          840000840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFF0000000000
          00000000000000840000FFFFFFFFFFFFFFFFFFFFFFFF840000FFFFFF840000D8
          E9ECD8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF840000FFFFFFFFFFFFFFFFFFFFFFFF840000840000D8E9ECD8E9ECD8E9
          ECD8E9ECD8E9EC000000D8E9EC000000FFFFFF000000000000FFFFFF00000084
          0000840000840000840000840000840000D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
          D8E9EC000000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF0000
          00D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC00
          0000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF000000000000D8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9
          EC000000000000000000000000000000000000D8E9ECD8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9
          ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9
          ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9EC000000}
        ParentShowHint = False
        ShowHint = True
        OnClick = spbTarjetaCreditoCopiarDatosClienteClick
      end
      object edtTarjetaCreditoTitular: TEdit
        Left = 160
        Top = 52
        Width = 321
        Height = 21
        TabOrder = 0
      end
      object vcbTarjetaCreditoBancos: TVariantComboBox
        Left = 160
        Top = 79
        Width = 273
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 13
        TabOrder = 1
        Items = <>
      end
      object vcbTarjetaCreditoTarjetas: TVariantComboBox
        Left = 160
        Top = 106
        Width = 273
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 13
        TabOrder = 2
        Items = <>
      end
      object edtTarjetaCreditoNroConfirmacion: TEdit
        Left = 160
        Top = 133
        Width = 121
        Height = 21
        TabOrder = 3
      end
      object neTarjetaCreditoCuotas: TNumericEdit
        Left = 160
        Top = 160
        Width = 121
        Height = 21
        TabOrder = 4
      end
    end
    object TPage
      Left = 0
      Top = 0
      Caption = 'Tarjeta Debito'
      object Label9: TLabel
        Left = 108
        Top = 72
        Width = 41
        Height = 13
        Caption = 'Titular:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label18: TLabel
        Left = 108
        Top = 99
        Width = 41
        Height = 13
        Caption = 'Banco:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label21: TLabel
        Left = 24
        Top = 128
        Width = 125
        Height = 13
        Caption = 'Numero Confirmaci'#243'n:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object spbTarjetaDebitoCopiarDatosCliente: TSpeedButton
        Left = 517
        Top = 67
        Width = 23
        Height = 22
        Hint = 'Copia los datos del Cliente'
        Glyph.Data = {
          AA040000424DAA04000000000000360000002800000013000000130000000100
          1800000000007404000000000000000000000000000000000000D8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9
          ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9
          ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
          D8E9EC840000840000840000840000840000840000840000840000840000D8E9
          ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC84
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000D8E9ECD8E9EC
          D8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC840000FFFF
          FF000000000000000000000000000000FFFFFF840000D8E9ECD8E9ECD8E9EC00
          0000D8E9EC000000000000000000000000000000000000840000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9
          EC000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000FFFFFF00000000000000
          0000000000000000FFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000
          FFFFFF000000000000000000000000840000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFF840000FFFFFF000000000000FFFFFF840000840000
          840000840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFF0000000000
          00000000000000840000FFFFFFFFFFFFFFFFFFFFFFFF840000FFFFFF840000D8
          E9ECD8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF840000FFFFFFFFFFFFFFFFFFFFFFFF840000840000D8E9ECD8E9ECD8E9
          ECD8E9ECD8E9EC000000D8E9EC000000FFFFFF000000000000FFFFFF00000084
          0000840000840000840000840000840000D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
          D8E9EC000000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF0000
          00D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC00
          0000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF000000000000D8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9
          EC000000000000000000000000000000000000D8E9ECD8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9
          ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9
          ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9EC000000}
        ParentShowHint = False
        ShowHint = True
        OnClick = spbTarjetaDebitoCopiarDatosClienteClick
      end
      object Label22: TLabel
        Left = 116
        Top = 16
        Width = 349
        Height = 14
        Caption = 'Complete los siguientes datos para completar y registrar el pago'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object edtTarjetaDebitoTitular: TEdit
        Left = 160
        Top = 68
        Width = 351
        Height = 21
        TabOrder = 0
      end
      object edtTarjetaDebitoNroConfirmacion: TEdit
        Left = 160
        Top = 124
        Width = 273
        Height = 21
        TabOrder = 1
      end
      object vcbTarjetaDebitoBancos: TVariantComboBox
        Left = 160
        Top = 95
        Width = 273
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 13
        TabOrder = 2
        Items = <>
      end
    end
    object TPage
      Left = 0
      Top = 0
      Caption = 'Pagare'
      object lblPagareTitular: TLabel
        Left = 13
        Top = 60
        Width = 41
        Height = 13
        Caption = 'Titular:'
        FocusControl = txtPagareNombreTitular
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblPagareRUT: TLabel
        Left = 13
        Top = 84
        Width = 31
        Height = 13
        Caption = 'RUT:'
        FocusControl = txtPagareDocumento
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblPagareBanco: TLabel
        Left = 13
        Top = 107
        Width = 69
        Height = 13
        Caption = 'Documento:'
        FocusControl = cbPagareBancos
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblPagareNumero: TLabel
        Left = 13
        Top = 131
        Width = 29
        Height = 13
        Caption = 'Nro.:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblPagareFecha: TLabel
        Left = 13
        Top = 156
        Width = 40
        Height = 13
        Caption = 'Fecha:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblPagareRepactacion: TLabel
        Left = 13
        Top = 179
        Width = 105
        Height = 13
        Caption = 'Nro. Repactaci'#243'n:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label37: TLabel
        Left = 80
        Top = 24
        Width = 349
        Height = 14
        Caption = 'Complete los siguientes datos para completar y registrar el pago'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object btnDatosClientePagare: TSpeedButton
        Left = 498
        Top = 57
        Width = 22
        Height = 21
        Glyph.Data = {
          AA040000424DAA04000000000000360000002800000013000000130000000100
          1800000000007404000000000000000000000000000000000000D8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9
          ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9
          ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
          D8E9EC840000840000840000840000840000840000840000840000840000D8E9
          ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC84
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000D8E9ECD8E9EC
          D8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC840000FFFF
          FF000000000000000000000000000000FFFFFF840000D8E9ECD8E9ECD8E9EC00
          0000D8E9EC000000000000000000000000000000000000840000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9
          EC000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000FFFFFF00000000000000
          0000000000000000FFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000
          FFFFFF000000000000000000000000840000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFF840000FFFFFF000000000000FFFFFF840000840000
          840000840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFF0000000000
          00000000000000840000FFFFFFFFFFFFFFFFFFFFFFFF840000FFFFFF840000D8
          E9ECD8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF840000FFFFFFFFFFFFFFFFFFFFFFFF840000840000D8E9ECD8E9ECD8E9
          ECD8E9ECD8E9EC000000D8E9EC000000FFFFFF000000000000FFFFFF00000084
          0000840000840000840000840000840000D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
          D8E9EC000000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF0000
          00D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC00
          0000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF000000000000D8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9
          EC000000000000000000000000000000000000D8E9ECD8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9
          ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9
          ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9EC000000}
        OnClick = btnDatosClientePagareClick
      end
      object txtPagareNombreTitular: TEdit
        Left = 134
        Top = 57
        Width = 361
        Height = 21
        TabOrder = 0
      end
      object txtPagareDocumento: TEdit
        Left = 134
        Top = 81
        Width = 121
        Height = 21
        TabOrder = 1
      end
      object cbPagareBancos: TVariantComboBox
        Left = 134
        Top = 104
        Width = 237
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 13
        TabOrder = 2
        OnChange = cbPagareBancosChange
        Items = <>
      end
      object txtPagareNumero: TEdit
        Left = 134
        Top = 128
        Width = 238
        Height = 21
        TabOrder = 3
      end
      object dePagareFecha: TDateEdit
        Left = 134
        Top = 152
        Width = 90
        Height = 21
        AutoSelect = False
        TabOrder = 4
        Date = -693594.000000000000000000
      end
      object edtPagareRepactacion: TEdit
        Left = 134
        Top = 176
        Width = 238
        Height = 21
        TabOrder = 5
      end
    end
  end
  object cb_CanalesDePago: TVariantComboBox
    Left = 28
    Top = 33
    Width = 180
    Height = 21
    Style = vcsDropDownList
    ItemHeight = 13
    TabOrder = 0
    OnChange = cb_CanalesDePagoChange
    Items = <>
  end
  object ne_ImportePagado: TNumericEdit
    Left = 437
    Top = 33
    Width = 115
    Height = 21
    MaxLength = 8
    TabOrder = 2
    OnChange = ne_ImportePagadoChange
    OnKeyPress = neImporteKeyPress
  end
  object chkNoCalcularIntereses: TCheckBox
    Left = 348
    Top = 72
    Width = 193
    Height = 25
    TabStop = False
    Caption = 'No calcular intereses en la pr'#243'xima facturaci'#243'n'
    TabOrder = 7
    WordWrap = True
    OnClick = chkNoCalcularInteresesClick
  end
  object edtCodigoFormaPago: TEdit
    Left = 221
    Top = 35
    Width = 34
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 2
    TabOrder = 1
    OnChange = edtCodigoFormaPagoChange
  end
  object edtDescFormaPago: TEdit
    Left = 261
    Top = 33
    Width = 158
    Height = 21
    TabStop = False
    ReadOnly = True
    TabOrder = 8
  end
  object CrearRecibo: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CrearRecibo'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaHora'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Importe'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroRecibo'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoMedioPago'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 45
    Top = 334
  end
  object _CerrarPagoComprobante: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CerrarPagoComprobante'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@FechaHora'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 77
    Top = 334
  end
  object sp_RegistrarDetallePagoComprobante: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorLocation = clUseServer
    LockType = ltBatchOptimistic
    CommandTimeout = 60
    ProcedureName = 'RegistrarDetallePagoComprobante;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@IDDetallePagoComprobante'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoMedioPago'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoFormaPago'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@PAC_CodigoBanco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PAC_CodigoTipoCuentaBancaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@PAC_NroCuentaBancaria'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@PAC_Sucursal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@PAC_Titular'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@PAT_CodigoTipoTarjetaCredito'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@PAT_NumeroTarjetaCredito'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@PAT_FechaVencimiento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 5
        Value = Null
      end
      item
        Name = '@PAT_CodigoEmisorTarjetaCredito'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@PAT_Titular'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@ChequeCodigoBanco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ChequeNumero'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@ChequeTitular'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@ChequeFecha'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@ChequeCodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@ChequeNumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@ChequeMonto'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@Cupon'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Autorizacion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@Cuotas'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@NumeroPOS'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Web_NumeroFinalTarjetaCredito'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@Web_TipoVenta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@Titular'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@CodigoTipoTarjeta'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoBancoEmisor'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@RepactacionNumero'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 45
    Top = 366
  end
  object sp_RegistrarPagoAnticipado: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorLocation = clUseServer
    LockType = ltBatchOptimistic
    CommandTimeout = 60
    ProcedureName = 'RegistrarPagoAnticipado;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroPago'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaHora'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Importe'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroRecibo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@IDDetallePagoComprobante'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NoCalcularIntereses'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 77
    Top = 366
  end
  object spValidarFormaDePagoEnCanal: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ValidarFormaDePagoEnCanal'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoMedioPago'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoEntradaUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@ValidarPagoAnticipado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@CodigoSistema'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoFormaDePago'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@DescripcionFormaDePago'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 30
        Value = Null
      end
      item
        Name = '@EsValidaEnCanal'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@AceptaPagoParcial'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPantalla'
        Attributes = [paNullable]
        DataType = ftSmallint
        Direction = pdInputOutput
        Precision = 5
        Value = Null
      end
      item
        Name = '@CodigoBanco'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTarjeta'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 232
    Top = 348
  end
  object sp_ActualizarClienteMoroso: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarClienteMoroso'
    Parameters = <
      item
        Name = '@CodigoRefinanciacion'
        DataType = ftInteger
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@Usuario'
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@ErrorDescription'
        DataType = ftString
        Direction = pdOutput
        Size = -1
        Value = Null
      end>
    Left = 344
    Top = 368
  end
end
