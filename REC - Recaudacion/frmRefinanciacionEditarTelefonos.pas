unit frmRefinanciacionEditarTelefonos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, FreTelefono, StdCtrls, ExtCtrls, PeaTypes, Util;

type
  TDatosTelefonos = record
    CodigoArea1,
    CodigoArea2: Integer;
    Telefono1,
    Telefono2: String;
  end;

type
  TRefinanciacionEditarTelefonosForm = class(TForm)
    Panel1: TPanel;
    btnCancelar: TButton;
    btnGrabar: TButton;
    GroupBox1: TGroupBox;
    FrameTelefono1: TFrameTelefono;
    FrameTelefono2: TFrameTelefono;
    procedure btnCancelarClick(Sender: TObject);
    procedure btnGrabarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    FTelefonosEditados: TDatosTelefonos;
    function Inicializar(CodigoArea1: Integer; Telefono1: String; CodigoArea2: Integer; Telefono2: String): Boolean;
  end;

var
  RefinanciacionEditarTelefonosForm: TRefinanciacionEditarTelefonosForm;

implementation

{$R *.dfm}

procedure TRefinanciacionEditarTelefonosForm.btnCancelarClick(Sender: TObject);
begin
    ModalResult := mrCancel;
end;

procedure TRefinanciacionEditarTelefonosForm.btnGrabarClick(Sender: TObject);
begin
    if FrameTelefono1.ValidarTelefono and FrameTelefono2.ValidarTelefono then begin
        FTelefonosEditados.CodigoArea1 := StrToint(Trim(Copy(FrameTelefono1.cbCodigosArea.Items[FrameTelefono1.cbCodigosArea.ItemIndex], 1, 25)));
        FTelefonosEditados.Telefono1   := FrameTelefono1.txtTelefono.Text;

        if Trim(FrameTelefono2.txtTelefono.Text) <> EmptyStr then begin
            FTelefonosEditados.CodigoArea2 := StrToint(Trim(Copy(FrameTelefono2.cbCodigosArea.Items[FrameTelefono2.cbCodigosArea.ItemIndex], 1, 25)));
            FTelefonosEditados.Telefono2   := FrameTelefono2.txtTelefono.Text;
        end;

        ModalResult := mrOk;
    end;
end;

procedure TRefinanciacionEditarTelefonosForm.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
    case Key of
        VK_ESCAPE: ModalResult := mrCancel;  
    end;
end;

function TRefinanciacionEditarTelefonosForm.Inicializar(CodigoArea1: Integer; Telefono1: string; CodigoArea2: Integer; Telefono2: string): Boolean;
    var
        CodArea1, CodArea2: Integer;
begin
    FrameTelefono1.Inicializa('Tel�fono 1 :',True,False, False);
    FrameTelefono2.Inicializa('Tel�fono 2 :',False,False, False);

    CodArea1 := iif(CodigoArea1 = 0, CODIGO_AREA_SANTIAGO, CodigoArea1);
    CodArea2 := iif(CodigoArea2 = 0, CODIGO_AREA_SANTIAGO, CodigoArea2);

    FrameTelefono1.CargarTelefono(CodArea1, Telefono1, -1);
    FrameTelefono2.CargarTelefono(CodArea2, Telefono2, -1);

    ActiveControl := FrameTelefono1.cbCodigosArea;

    Result := True;
end;

end.
