unit frmRefinanciacionSeleccionarComprobantes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ListBoxEx, DBListEx, StdCtrls, ExtCtrls, DBClient, Provider, DB, ADODB, ImgList, ComCtrls,

  SysUtilsCN;

type
  TRefinanciacionSeleccionarComprobantesForm = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    DBListEx1: TDBListEx;
    btnCancelar: TButton;
    btnGrabar: TButton;
    lblCantidadComprobantesSeleccionados: TLabel;
    Label2: TLabel;
    lblTotalComprobantesSeleccionadosTotal: TLabel;
    spObtenerRefinanciacionComprobantesImpagos: TADOStoredProc;
    dsComprobantesImpagos: TDataSource;
    dspComprobantesImpagos: TDataSetProvider;
    cdsComprobantesImpagos: TClientDataSet;
    cdsComprobantesImpagosFechaEmision: TDateTimeField;
    cdsComprobantesImpagosFechaVencimiento: TDateTimeField;
    cdsComprobantesImpagosTipoComprobante: TStringField;
    cdsComprobantesImpagosNumeroComprobante: TLargeintField;
    cdsComprobantesImpagosDescriComprobante: TStringField;
    cdsComprobantesImpagosCodigoConvenio: TIntegerField;
    cdsComprobantesImpagosCodigoPersona: TIntegerField;
    cdsComprobantesImpagosTotalComprobante: TLargeintField;
    cdsComprobantesImpagosTotalComprobanteDescri: TStringField;
    cdsComprobantesImpagosSaldoPendiente: TLargeintField;
    cdsComprobantesImpagosSaldoPendienteDescri: TStringField;
    cdsComprobantesImpagosTipoComprobanteFiscal: TStringField;
    cdsComprobantesImpagosNumeroComprobanteFiscal: TLargeintField;
    cdsComprobantesImpagosDescriComprobanteFiscal: TStringField;
    cdsComprobantesImpagosRefinanciar: TBooleanField;
    lnCheck: TImageList;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    DBListEx2: TDBListEx;
    spDetalleComprobante: TADOStoredProc;
    cdsDetalleComprobante: TClientDataSet;
    dspDetalleComprobante: TDataSetProvider;
    cdsDetalleComprobanteDescripcion: TStringField;
    cdsDetalleComprobanteImporte: TLargeintField;
    cdsDetalleComprobanteDescImporte: TStringField;
    dsDetalleComprobante: TDataSource;
    cdsComprobantesImpagosDescriAPagar: TStringField;
    chkMarcarTodos: TCheckBox;
    procedure DBListEx1LinkClick(Sender: TCustomDBListEx; Column: TDBListExColumn);
    procedure DBListEx1DrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
    procedure cdsComprobantesImpagosAfterScroll(DataSet: TDataSet);
    procedure DBListEx2DrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
    procedure chkMarcarTodosClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnGrabarClick(Sender: TObject);
    procedure DBListEx1KeyPress(Sender: TObject; var Key: Char);
    procedure DBListEx1DblClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
    Procedure ActualizarTotales;
  public
    { Public declarations }
    FComprobantesSeleccionados: Integer;
    FComprobantesSeleccionadosTotal: Largeint;
    function Inicializar(CodigoCliente, CodigoConvenio, pCodigoRefinanciacion: Integer; cdsComprobantesRefinanciados: TClientDataSet): Boolean;
  end;

var
  RefinanciacionSeleccionarComprobantesForm: TRefinanciacionSeleccionarComprobantesForm;

implementation

{$R *.dfm}

uses
    DMConnection, PeaTypes, Util, PeaProcsCN;

procedure TRefinanciacionSeleccionarComprobantesForm.btnCancelarClick(Sender: TObject);
begin
    ModalResult := mrCancel;
end;

procedure TRefinanciacionSeleccionarComprobantesForm.btnGrabarClick(Sender: TObject);
    resourcestring
        rsValidacionTitulo  = 'Comprobantes Seleccionados';
        rsValidacionMensaje = 'Esta selecionando menos de 3 Comprobantes para Refinanciar.';
begin
    if FComprobantesSeleccionados < 3 then begin
        ShowMsgBoxCN(rsValidacionTitulo, rsValidacionMensaje, MB_ICONWARNING, Self);
    end;

    ModalResult := mrOk;
end;

procedure TRefinanciacionSeleccionarComprobantesForm.cdsComprobantesImpagosAfterScroll(DataSet: TDataSet);
begin
    try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        if cdsDetalleComprobante.Active then begin
            cdsDetalleComprobante.EmptyDataSet;
            cdsDetalleComprobante.Close;
        end;

        with spDetalleComprobante do begin
            Parameters.ParamByName('@TipoComprobante').Value   := cdsComprobantesImpagosTipoComprobante.AsString;
            Parameters.ParamByName('@NumeroComprobante').Value := cdsComprobantesImpagosNumeroComprobante.AsInteger;
        end;

        cdsDetalleComprobante.Open;
        cdsDetalleComprobante.AppendRecord(['TOTAL A PAGAR', 0, cdsComprobantesImpagosDescriAPagar.AsString]);
        cdsDetalleComprobante.First;
    finally
        Screen.Cursor := crDefault;
        Application.ProcessMessages;
    end;
end;

procedure TRefinanciacionSeleccionarComprobantesForm.chkMarcarTodosClick(Sender: TObject);
    var
        Puntero: TBookmark;
begin
    Try
        dsComprobantesImpagos.DataSet      := Nil;
        cdsComprobantesImpagos.AfterScroll := Nil;

        Puntero := cdsComprobantesImpagos.GetBookmark;
        cdsComprobantesImpagos.First;

        FComprobantesSeleccionados      := iif(chkMarcarTodos.Checked, cdsComprobantesImpagos.RecordCount, 0);
        FComprobantesSeleccionadosTotal := 0;

        while not cdsComprobantesImpagos.Eof do begin
            cdsComprobantesImpagos.Edit;
            cdsComprobantesImpagosRefinanciar.AsBoolean := chkMarcarTodos.Checked;
            cdsComprobantesImpagos.Post;

            if chkMarcarTodos.Checked then begin
                FComprobantesSeleccionadosTotal :=
                    FComprobantesSeleccionadosTotal + cdsComprobantesImpagosSaldoPendiente.AsLargeInt;
            end;
            cdsComprobantesImpagos.Next;
        end;
    Finally
        if Assigned(Puntero) then begin
            if cdsComprobantesImpagos.BookmarkValid(Puntero) then cdsComprobantesImpagos.GotoBookmark(Puntero);
            cdsComprobantesImpagos.FreeBookmark(Puntero);
        end;
        cdsComprobantesImpagos.AfterScroll := cdsComprobantesImpagosAfterScroll;
        dsComprobantesImpagos.DataSet      := cdsComprobantesImpagos;
        ActualizarTotales;
    End;
end;

procedure TRefinanciacionSeleccionarComprobantesForm.DBListEx1DblClick(Sender: TObject);
begin
    if cdsComprobantesImpagos.RecordCount > 0 then DBListEx1LinkClick(Nil, Nil);

end;

procedure TRefinanciacionSeleccionarComprobantesForm.DBListEx1DrawText(Sender: TCustomDBListEx; Column: TDBListExColumn;
  Rect: TRect; State: TOwnerDrawState; var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
var
    bmp: TBitMap;
begin
    with Sender do begin
        if (Column.FieldName = 'Refinanciar') then try
            Text := '';
            Canvas.FillRect(Rect);
            bmp := TBitMap.Create;
			DefaultDraw := False;

            if (cdsComprobantesImpagos.FieldByName(Column.FieldName).AsBoolean ) then
                lnCheck.GetBitmap(0, Bmp)
            else
                lnCheck.GetBitmap(1, Bmp);

            Canvas.Draw((Rect.Left + Rect.Right - Bmp.Width) div 2, Rect.Top, bmp);
        finally
            bmp.Free;
        end; // finally
    end; // with
end;

procedure TRefinanciacionSeleccionarComprobantesForm.DBListEx1KeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #32 then begin
        if cdsComprobantesImpagos.RecordCount > 0 then DBListEx1LinkClick(Nil, Nil);
    end;
end;

procedure TRefinanciacionSeleccionarComprobantesForm.DBListEx1LinkClick(Sender: TCustomDBListEx; Column: TDBListExColumn);
begin
    try
        cdsComprobantesImpagos.Edit;
        cdsComprobantesImpagosRefinanciar.AsBoolean := not cdsComprobantesImpagosRefinanciar.AsBoolean;
    finally
        cdsComprobantesImpagos.Post;

        if cdsComprobantesImpagosRefinanciar.AsBoolean then begin
            Inc(FComprobantesSeleccionados);
            FComprobantesSeleccionadosTotal :=
                FComprobantesSeleccionadosTotal + cdsComprobantesImpagosSaldoPendiente.AsLargeInt;
        end
        else begin
            Dec(FComprobantesSeleccionados);
            FComprobantesSeleccionadosTotal :=
                FComprobantesSeleccionadosTotal - cdsComprobantesImpagosSaldoPendiente.AsLargeInt;
        end;

        ActualizarTotales;
    end;
end;

function TRefinanciacionSeleccionarComprobantesForm.Inicializar(CodigoCliente, CodigoConvenio, pCodigoRefinanciacion: Integer; cdsComprobantesRefinanciados: TClientDataSet): Boolean;
    const
        Comprobante = 'No Encontrado. Tipo: %s, Numero: %s';
    var
        cdsRefinanciados: TclientDataSet;
        SearchList: Variant;
begin
    try
        CambiarEstadoCursor(CURSOR_RELOJ);
        Result := False;

        with spObtenerRefinanciacionComprobantesImpagos do begin
            Parameters.ParamByName('@CodigoConvenio').Value       := CodigoConvenio;
            Parameters.ParamByName('@CodigoCliente').Value        := CodigoCliente;
            Parameters.ParamByName('@CodigoRefinanciacion').Value := pCodigoRefinanciacion;
        end;
        cdsComprobantesImpagos.Open;

        FComprobantesSeleccionados      := 0;
        FComprobantesSeleccionadosTotal := 0;
        SearchList := VarArrayCreate([0, 1], VarVariant);

        if cdsComprobantesRefinanciados.RecordCount > 0 then try
            dsComprobantesImpagos.DataSet      := Nil;
            cdsComprobantesImpagos.AfterScroll := Nil;
            cdsRefinanciados := TClientDataSet.Create(Nil);
            cdsRefinanciados.Data := cdsComprobantesRefinanciados.Data;


            cdsComprobantesImpagos.First;

            while not cdsComprobantesImpagos.Eof do begin
                {
                SearchList[0] := cdsComprobantesImpagos.FieldByName('TipoComprobante').AsVariant;
                SearchList[1] := cdsComprobantesImpagos.FieldByName('NumeroComprobante').AsVariant;

                if cdsRefinanciados.Locate('TipoComprobante;NumeroComprobante', SearchList, []) then begin
                    cdsComprobantesImpagos.Edit;
                    cdsComprobantesImpagosRefinanciar.AsBoolean := True;
                    cdsComprobantesImpagos.Post;
                    Inc(FComprobantesSeleccionados);
                    FComprobantesSeleccionadosTotal := FComprobantesSeleccionadosTotal + cdsComprobantesImpagosSaldoPendiente.AsLargeInt;
                end;
                }

                cdsRefinanciados.First;
                while not cdsRefinanciados.Eof do begin
                    if (cdsComprobantesImpagosTipoComprobante.AsString = cdsRefinanciados.FieldByName('TipoComprobante').AsString) and
                        (cdsComprobantesImpagosNumeroComprobante.AsInteger = cdsRefinanciados.FieldByName('NumeroComprobante').AsInteger) then begin
                        cdsComprobantesImpagos.Edit;
                        cdsComprobantesImpagosRefinanciar.AsBoolean := True;
                        cdsComprobantesImpagos.Post;
                        Inc(FComprobantesSeleccionados);
                        FComprobantesSeleccionadosTotal := FComprobantesSeleccionadosTotal + cdsComprobantesImpagosSaldoPendiente.AsLargeInt;
                        Break;
                    end;

                    cdsRefinanciados.Next;
                end;

                cdsComprobantesImpagos.Next;
            end;


           {
            with cdsComprobantesRefinanciados do begin
                First;
                while not Eof do begin
                    if cdsComprobantesImpagos.Locate(
                        'TipoComprobante;NumeroComprobante',
                        VarArrayOf([Trim(FieldByName('TipoComprobante').AsString), Trim(FieldByName('NumeroComprobante').AsString)]),
                        []) then begin
                        cdsComprobantesImpagos.Edit;
                        cdsComprobantesImpagosRefinanciar.AsBoolean := True;
                        cdsComprobantesImpagos.Post;
                        Inc(FComprobantesSeleccionados);
                        FComprobantesSeleccionadosTotal :=
                            FComprobantesSeleccionadosTotal + cdsComprobantesImpagosSaldoPendiente.AsLargeInt;
                    end
                    else ShowMessage(Format(Comprobante, [FieldByName('TipoComprobante').AsString, FieldByName('NumeroComprobante').AsString]));

                    Next;
                end;
            end;
            }
        finally
            cdsComprobantesImpagos.AfterScroll := cdsComprobantesImpagosAfterScroll;
            dsComprobantesImpagos.DataSet      := cdsComprobantesImpagos;
            cdsComprobantesImpagos.First;

            if Assigned(cdsRefinanciados) then FreeAndNil(cdsRefinanciados);

            ActualizarTotales;
        end;

        Result := True;
    finally
        if Assigned(cdsRefinanciados) then FreeAndNil(cdsRefinanciados);

        CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

procedure TRefinanciacionSeleccionarComprobantesForm.DBListEx2DrawText(Sender: TCustomDBListEx; Column: TDBListExColumn;
  Rect: TRect; State: TOwnerDrawState; var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
    if Pos('TOTAL', UpperCase(Text)) <> 0 then Sender.Canvas.Font.Style := [fsBold];
end;

procedure TRefinanciacionSeleccionarComprobantesForm.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
    case Key of
        VK_ESCAPE: ModalResult := mrCancel;
    end;
end;

procedure TRefinanciacionSeleccionarComprobantesForm.ActualizarTotales;
    resourcestring
        rs_NingunComprobanteSeleccionado = 'Ning�n Comprobante Seleccionado.';
        rs_CantidadComprobantesSeleccionados = 'Comprobantes Seleccionados: %d de %d.';
begin
    lblTotalComprobantesSeleccionadosTotal.Caption := FormatFloat(FORMATO_IMPORTE, FComprobantesSeleccionadosTotal);
    if FComprobantesSeleccionados = 0 then
        lblCantidadComprobantesSeleccionados.Caption := rs_NingunComprobanteSeleccionado
    else begin
        lblCantidadComprobantesSeleccionados.Caption :=
            Format(rs_CantidadComprobantesSeleccionados,
                    [FComprobantesSeleccionados,
                     cdsComprobantesImpagos.RecordCount]);
    end;
end;

end.
