object FormPagoCheque: TFormPagoCheque
  Left = 193
  Top = 234
  BorderStyle = bsDialog
  Caption = 'Datos del Pago con Cheque'
  ClientHeight = 232
  ClientWidth = 616
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 5
    Top = 30
    Width = 605
    Height = 165
  end
  object Label1: TLabel
    Left = 25
    Top = 77
    Width = 45
    Height = 13
    Caption = 'Cuenta:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 25
    Top = 106
    Width = 45
    Height = 13
    Caption = 'Titular :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 25
    Top = 48
    Width = 41
    Height = 13
    Caption = 'Banco:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label11: TLabel
    Left = 25
    Top = 159
    Width = 48
    Height = 13
    Caption = 'Monto : '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label10: TLabel
    Left = 12
    Top = 8
    Width = 206
    Height = 13
    Caption = 'El importe m'#225'ximo que puede abonar es de '
  end
  object lbl_ImporteMaximo: TLabel
    Left = 226
    Top = 8
    Width = 48
    Height = 13
    Caption = '4545454$'
  end
  object Label2: TLabel
    Left = 323
    Top = 77
    Width = 48
    Height = 13
    Caption = 'Cheque:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 27
    Top = 134
    Width = 35
    Height = 13
    Caption = 'RUT :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object txtTitular: TEdit
    Left = 104
    Top = 101
    Width = 497
    Height = 22
    CharCase = ecUpperCase
    Color = 16444382
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    MaxLength = 40
    ParentFont = False
    TabOrder = 3
    OnChange = HabilitarBotones
  end
  object cbEntidadesBancarias: TComboBox
    Left = 104
    Top = 45
    Width = 209
    Height = 22
    Style = csDropDownList
    Color = 16444382
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    ItemHeight = 14
    ParentFont = False
    TabOrder = 0
    OnChange = HabilitarBotones
  end
  object txtMontoCheque: TNumericEdit
    Left = 104
    Top = 154
    Width = 97
    Height = 22
    Color = clBtnFace
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    MaxLength = 8
    ParentFont = False
    ReadOnly = True
    TabOrder = 4
    OnChange = HabilitarBotones
    Decimals = 0
  end
  object txtNumeroCuenta: TEdit
    Left = 104
    Top = 72
    Width = 209
    Height = 21
    Color = 16444382
    MaxLength = 16
    TabOrder = 1
    OnChange = HabilitarBotones
    OnKeyPress = txtNumeroCuentaKeyPress
  end
  object btn_Aceptar: TDPSButton
    Left = 459
    Top = 202
    Caption = '&Aceptar'
    Default = True
    TabOrder = 5
    OnClick = btn_AceptarClick
  end
  object btn_Cancelar: TDPSButton
    Left = 535
    Top = 202
    Cancel = True
    Caption = '&Cancelar'
    TabOrder = 6
    OnClick = btn_CancelarClick
  end
  object txtNumeroCheque: TEdit
    Left = 392
    Top = 72
    Width = 161
    Height = 21
    Color = 16444382
    MaxLength = 16
    TabOrder = 2
    OnChange = HabilitarBotones
    OnKeyPress = txtNumeroCuentaKeyPress
  end
end
