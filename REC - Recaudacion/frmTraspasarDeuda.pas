{***********************************************
    	frmTraspasarDeuda

 Author: mbecerra
 Date: 28-Julio-2009
 Description: 	Implementa la funcionalidad requerida por la SS 740,
				traspaso de Deuda (de un convenio a otro).


Author		: mvillarroel
Date		: 07-Julio-2013
Firma   : SS_660_MVI_20130711
Description	:		(Ref: SS 660)
                Se agrega validaci�n para indicar si un Convenio est� en Lista Amarilla

Firma       : SS_660_CQU_20130711
Descripcion : Se alinea el Label de Lista Amarilla.
              Se agrega funci�n para validar si el conveino est� en lista amarilla

Firma       : SS_660_MVI_20130729
Descripcion : Se cambia texto y se agrega estado del convenio en lista amarilla

Firma       :   SS_1120_MVI_20130820
Descripcion :   Se cambia el llamado a llenar el combo de convenios de origen y destino para que utilicen el procedimiento CargarConveniosRUT.
                Se cambia el procedimiento que se llama en el OnDrawItem para llamar al ItemComboBoxColor para el convenios de destino y origen.
                Adem�s se comenta el procedimiento ColoreaItem ya que el nuevo procedimiento realizar� la tarea de colorear los convenios de baja.

Firma       :   SS_660_MVI_20130909
Descripcion :   Se realiza la limpieza del label lista amarilla, dentro de la limpieza de los convenios.
                y se modifica validaci�n para saber si se obtiene resultado del SP ObtenerEstadoConvenioEnListaAmarilla.
                Se acorta tama�o del lblEnListaAmarilla y se elimina texto inicial, ya que siempre es reemplazado.

Firma       : SS_1408_MCA_20151027
Descripcion : muestra un mensaje si el cliente se encuentre en registrado en Tabla ClientesAContactar


************************************************************* }

unit frmTraspasarDeuda;

interface

uses
	PeaProcs,
    PeaTypes,
	UtilProc,
    UtilDb,
    Util,
    DMConnection,
    BuscaClientes,
    SysUtilsCN,     // SS_660_CQU_20130711
    PeaProcsCN,     // SS_660_CQU_20130711
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DmiCtrls, StdCtrls, VariantComboBox, DB, ADODB, ExtCtrls;

type
  TTraspasarDeudaForm = class(TForm)
    spObtenerConvenios: TADOStoredProc;
    spTraspasarEntreConvenios: TADOStoredProc;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    lblNombreOrigen: TLabel;
    lblDireccionOrigen: TLabel;
    lblPersoneriaOrigen: TLabel;
    Label2: TLabel;
    vcbConvenioOrigen: TVariantComboBox;
    Label9: TLabel;
    lblEstadoOrigen: TLabel;
    Label10: TLabel;
    lblSaldoOrigen: TLabel;
    Label13: TLabel;
    lblFormaOrigen: TLabel;
    GroupBox2: TGroupBox;
    Label6: TLabel;
    lblInteresesOrigen: TLabel;
    lblTtosOrigen: TLabel;
    Label7: TLabel;
    Label3: TLabel;
    vcbConvenioDestino: TVariantComboBox;
    Label20: TLabel;
    Label11: TLabel;
    lblEstadoDestino: TLabel;
    lblSaldoDestino: TLabel;
    Label12: TLabel;
    Label14: TLabel;
    lblFormaDestino: TLabel;
    Label22: TLabel;
    lblNombreDestino: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    lblPersoneriaDestino: TLabel;
    lblDireccionDestino: TLabel;
    lblInteresesDestino: TLabel;
    lblTtosDestino: TLabel;
    GroupBox3: TGroupBox;
    Label8: TLabel;
    neMonto: TNumericEdit;
    btnAceptar: TButton;
    btnCancelar: TButton;
    edtRutOrigen: TPickEdit;
    edtRutDestino: TPickEdit;
    spObtenerDatosCliente: TADOStoredProc;
    Shape1: TShape;
    Shape2: TShape;
    Label1: TLabel;
    lblEnListaAmarilla: TLabel;                                                //SS_660_MVI_20130711
    lblEnListaAmarillaDestino: TLabel;                                         //SS_660_CQU_20130711
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ValidaBusca(Sender: TObject; var Key: Char);
    procedure btnCancelarClick(Sender: TObject);
    procedure TraerSaldoConvenio(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
    procedure VerPosiblesRut(Sender: TObject);
    procedure vcbConvenioOrigenDrawItem(Control: TWinControl; Index: Integer;       //SS_1120_MVI_20130820
      Rect: TRect; State: TOwnerDrawState);                                         //SS_1120_MVI_20130820
    procedure vcbConvenioDestinoDrawItem(Control: TWinControl; Index: Integer;      //SS_1120_MVI_20130820
      Rect: TRect; State: TOwnerDrawState);                                         //SS_1120_MVI_20130820
//    procedure ColoreaItem(Control: TWinControl; Index: Integer;                   //SS_1120_MVI_20130820
//      Rect: TRect; State: TOwnerDrawState);                                       //SS_1120_MVI_20130820
  private
    { Private declarations }
    FEstadoConvenioVigente,
    FEstadoConvenioBaja : byte;
    FListaOrigen, FListaDestino : TStringList;
    FSaldoOrigen, FSaldoDestino : int64;
    FUltimaBusqueda: TBusquedaCliente;
  public
    { Public declarations }
    function Inicializar(sCaption : string) : boolean;
    function Validaciones : boolean;
    function TieneTransitosSinFacturar(CodigoConvenio : Int64) : boolean;
    function TieneInteresesSinFacturar(CodigoConvenio : Int64) : boolean;
    procedure BuscarConvenios(Rut : string; vcb : TVariantComboBox; Lista : TStringList);
    procedure BuscarDatosCliente(Rut : string; cual : byte);
    procedure PoneEstado(etiqueta : TLabel; Estado : byte);
    procedure LimpiaLabels(cual : byte);
    procedure TraspasarDeuda;
    function  VerificarConvenioListaAmarilla(CodigoConvenio : Integer) : String; //SS_660_MVI_20130729    // SS_660_CQU_20121010
  end;

var
  TraspasarDeudaForm: TTraspasarDeudaForm;

implementation

{$R *.dfm}

const
    cLimpiarNombreOrigen = $1;
    cLimpiarNombreDestino = $2;
    cLimpiarConvenioOrigen = $4;
    cLimpiarConvenioDestino = $8;
    cLimpiarIndicadoresOrigen = $10;
    cLimpiarIndicadoresDestino = $20;
    cLimpiarTodo = $FF;
    cLimpiarTodoOrigen = cLimpiarNombreOrigen + cLimpiarConvenioOrigen + cLimpiarIndicadoresOrigen;
    cLimpiarTodoDestino = cLimpiarNombreDestino + cLimpiarConvenioDestino + cLimpiarIndicadoresDestino;

{**********************************************************
        			Inicializar

 Author: mbecerra
 Date: 28-Julio-2009
 Description: 	Inicializa el Form de traspaso de deudas

*********************************************************** }
function TTraspasarDeudaForm.Inicializar;
resourcestring
    MSG_SQL_BAJA = 'SELECT dbo.CONST_ESTADO_CONVENIO_BAJA()';
    MSG_SQL_ALTA = 'SELECT dbo.CONST_ESTADO_CONVENIO_ALTA()';

begin
	FListaOrigen := TStringList.Create;
    FListaDestino := TStringList.Create;

    Result := True;
    Caption := sCaption;
    CenterForm(Self);

    edtRutOrigen.Text := '';
    edtRutDestino.Text := '';
    neMonto.Value := 0;
    LimpiaLabels(cLimpiarTodo);
    FEstadoConvenioVigente	:= QueryGetValueInt(DMConnections.BaseCAC, MSG_SQL_ALTA);
    FEstadoConvenioBaja		:= QueryGetValueInt(DMConnections.BaseCAC, MSG_SQL_BAJA);
    lblEnListaAmarilla.Visible:=False;                                                    //SS_660_MVI_20130711
    lblEnListaAmarillaDestino.Visible:=False;                                             // SS_660_CQU_20130711
end;

{**********************************************************
        			TieneTransitosSinFacturar

 Author: mbecerra
 Date: 28-Julio-2009
 Description: 	Retorna True/False si el convenio tiene/no tiene transitos sin facturar.
*********************************************************** }
function TTraspasarDeudaForm.TieneTransitosSinFacturar;
resourcestring
    MSG_ERROR = 'Ocurri� un error al intentar obtener Tr�nsitos sin facturar para el convenio %d';
    MSG_SQL = 'IF EXISTS( SELECT 1 FROM	ItemsPeajeCuenta WITH (nolock) WHERE CodigoConvenio = %d AND NumeroMovimiento IS NULL ) SELECT 1 ELSE SELECT 0';

var
	Valor : integer;
begin
	Result := False;
	try
        Valor := QueryGetValueInt(DMConnections.BaseCAC, Format(MSG_SQL, [CodigoConvenio]) );
        Result := (Valor = 1);
    except on e:exception do begin
            MsgBoxErr(Format(MSG_ERROR, [CodigoConvenio]), e.Message, Caption, MB_ICONERROR);
    	end;
    end;
end;

{**********************************************************
        			TieneInteresesSinFacturar
 Author: mbecerra
 Date: 28-Julio-2009
 Description: 	Retorna True/False si el convenio tiene/no tiene intereses sin facturar.
*********************************************************** }
function TTraspasarDeudaForm.TieneInteresesSinFacturar;
resourcestring
    MSG_ERROR = 'Ocurri� un error al intentar obtener los intereses sin facturar para el convenio %d';
    MSG_SQL = 'IF EXISTS( SELECT 1 FROM	MovimientosCuentas WITH (nolock) WHERE CodigoConvenio = %d AND LoteFacturacion IS NULL  AND CodigoConcepto IN (12,52)) SELECT 1 ELSE SELECT 0';

var
	Valor : integer;
begin
	Result := False;
	try
        Valor := QueryGetValueInt(DMConnections.BaseCAC, Format(MSG_SQL, [CodigoConvenio]) );
        Result := (Valor = 1);
    except on e:exception do begin
            MsgBoxErr(Format(MSG_ERROR, [CodigoConvenio]), e.Message, Caption, MB_ICONERROR);
    	end;
    end;
end;

{**********************************************************
        			PoneEstado
 Author: mbecerra
 Date: 28-Julio-2009
 Description: 	Despliega el estado del convenio.
*********************************************************** }
procedure TTraspasarDeudaForm.PoneEstado;
resourcestring
	MSG_E_ALTA  = 'Vigente';
    MSG_E_BAJA  = 'Baja';

begin
    if Estado = FEstadoConvenioVigente then begin
        etiqueta.Font.Color := clBlack;
        etiqueta.Caption := MSG_E_ALTA;
    end
    else begin
    	etiqueta.Font.Color := clRed;
        etiqueta.Caption := MSG_E_BAJA;
    end;

end;

{**********************************************************
        			LimpiaLabels
 Author: mbecerra
 Date: 28-Julio-2009
 Description: 	Limpia las etiquetas de Saldo, Estado y Forma de Pago

            	El par�metro cual indica cual limpiar, es un OR-Byte:
                1 = datos del rut de origen
                2 = datos del rut de destino
                4 = datos del convenio de origen
                8 = datos del convenio de destino
                16 = Poner invisible los indicadores Origen
                32 = Poner invisible los indicadores Destino
*********************************************************** }
procedure TTraspasarDeudaForm.LimpiaLabels(cual: Byte);
begin
	if (cual and cLimpiarNombreOrigen) = cLimpiarNombreOrigen then begin
    	lblNombreOrigen.Caption := '';
        lblDireccionOrigen.Caption := '';
        lblPersoneriaOrigen.Caption := '';
    end;

    if (cual and cLimpiarNombreDestino) = cLimpiarNombreDestino then begin
        lblNombreDestino.Caption := '';
        lblDireccionDestino.Caption := '';
        lblPersoneriaDestino.Caption := '';
    end;

    if (cual and cLimpiarConvenioOrigen) = cLimpiarConvenioOrigen then begin
    	lblEstadoOrigen.Caption := '';
        lblSaldoOrigen.Caption := '';
        lblFormaOrigen.Caption := '';
        FSaldoOrigen := 0;
        lblEnListaAmarilla.Visible  := False;                                         //SS_660_MVI_20130909
        lblEnListaAmarilla.Caption  := EmptyStr;                                      //SS_660_MVI_20130909

    end;

    if (cual and cLimpiarConvenioDestino) = cLimpiarConvenioDestino then begin
    	lblEstadoDestino.Caption := '';
        lblSaldoDestino.Caption := '';
        lblFormaDestino.Caption := '';
        FSaldoDestino := 0;
        lblEnListaAmarillaDestino.Visible  := False;                                         //SS_660_MVI_20130909
        lblEnListaAmarillaDestino.Caption  := EmptyStr;                                      //SS_660_MVI_20130909
    end;

    if (cual and cLimpiarIndicadoresOrigen) = cLimpiarIndicadoresOrigen then begin
        lblInteresesOrigen.Visible := False;
        lblTtosOrigen.Visible := False;
    end;

    if (cual and cLimpiarIndicadoresDestino) = cLimpiarIndicadoresDestino then begin
        lblInteresesDestino.Visible := False;
        lblTtosDestino.Visible := False;
    end;

end;

{**********************************************************
        			TraerSaldoConvenio
 Author: mbecerra
 Date: 28-Julio-2009
 Description: 	Obtiene el saldo del convenio para el convenio seleccionado,
            	y lo despliega, junto con el Estado y la Forma de pago
*********************************************************** }
procedure TTraspasarDeudaForm.TraerSaldoConvenio(Sender: TObject);
resourcestring
	MSG_ERROR	= 'Error al intentar obtener el saldo del convenio';
    MSG_SQL		= 'SELECT dbo.ObtenerSaldoDelConvenio( %d )';
    MSG_SALDO	= '%.0n';
    //SQL_ESTACONVENIOENLISTAAMARILLA = 'SELECT dbo.EstaConvenioEnListaAmarilla( ''%s'' ,GETDATE() )';      // SS_660_CQU_20130711  //SS_660_MVI_20130711
var
	Saldo : int64;
    Texto, Estado, FormaPago, EstadoListaAmarilla : string;                                                                                //SS_660_MVI_20130729
    Indice : integer;
    TieneTransitos, TieneIntereses : boolean;
begin
    try

        lblEnListaAmarilla.Color:=clYellow;                                                                                                  //SS_660_MVI_20130711
        //lblEnListaAmarilla.Visible  := QueryGetValue(   DMConnections.BaseCAC,                                                             // SS_660_CQU_20130711 //SS_660_MVI_20130711
        //                                                Format(SQL_ESTACONVENIOENLISTAAMARILLA,                                            // SS_660_CQU_20130711 //SS_660_MVI_20130711
        //                                                        [   TVariantComboBox(Sender).Value                                         // SS_660_CQU_20130711 //SS_660_MVI_20130711
        //                                                        ]                                                                          // SS_660_CQU_20130711 //SS_660_MVI_20130711
        //                                                      )                                                                            // SS_660_CQU_20130711 //SS_660_MVI_20130711
        //                                             )='True';                                                                             // SS_660_CQU_20130711 //SS_660_MVI_20130711


        if TVariantComboBox(Sender).ItemIndex >= 0 then begin
            Saldo := TVariantComboBox(Sender).Value;
            TieneTransitos := TieneTransitosSinFacturar(Saldo);
            TieneIntereses := TieneInteresesSinFacturar(Saldo);
            Saldo := QueryGetValueInt(DMConnections.BaseCAC, Format(MSG_SQL, [Saldo]) );
            Texto := Format(MSG_SALDO,[ Saldo div 100 * 1.0 ]);
            Indice := TVariantComboBox(Sender).ItemIndex;
            if TVariantComboBox(Sender).Tag = 10 then begin   {Origen}
                FSaldoOrigen := Saldo div 100;
                lblSaldoOrigen.Caption := Texto;
                Estado := Trim(StrLeft(FListaOrigen[Indice],200));
                FormaPago := Trim(StrRight(FListaOrigen[Indice],200));
                PoneEstado(lblEstadoOrigen, StrToInt(Estado));
                lblFormaOrigen.Caption := FormaPago;
                lblTtosOrigen.Visible := TieneTransitos;
                lblInteresesOrigen.Visible := TieneIntereses;
                neMonto.ValueInt := Abs(FSaldoOrigen);
                //lblEnListaAmarilla.Visible := VerificarConvenioListaAmarilla(TVariantComboBox(Sender).Value);  //SS_660_MVI_20130729 // SS_660_CQU_20130711
                EstadoListaAmarilla := VerificarConvenioListaAmarilla(TVariantComboBox(Sender).Value);                                               //SS_660_MVI_20130729
//                if EstadoListaAmarilla <> 'NULL' then begin                                                                                        //SS_660_MVI_20130909  //SS_660_MVI_20130729
               if EstadoListaAmarilla <> '' then begin                                                                                               //SS_660_MVI_20130909  //SS_660_MVI_20130729
                lblEnListaAmarilla.Visible  := True;                                                                                                 //SS_660_MVI_20130729
                lblEnListaAmarilla.Caption  := EstadoListaAmarilla;                                                                                  //SS_660_MVI_20130729
                                                                                                                                                     //SS_660_MVI_20130729
                end                                                                                                                                  //SS_660_MVI_20130729
                else begin                                                                                                                           //SS_660_MVI_20130729
                                                                                                                                                     //SS_660_MVI_20130729
                lblEnListaAmarilla.Visible  := False;                                                                                                //SS_660_MVI_20130729
                                                                                                                                                     //SS_660_MVI_20130729
                end;

                //TASK_012_GLE_20161103 inicia - Completa Cuenta Destino con los mismos datos de Origen
                lblSaldoDestino.Caption := Texto;
                PoneEstado(lblEstadoDestino, StrToInt(Estado));
                lblFormaDestino.Caption := FormaPago;
                lblTtosDestino.Visible := TieneTransitos;
                lblInteresesDestino.Visible := TieneIntereses;

                vcbConvenioDestino.ItemIndex := 0;
                //TASK_012_GLE_20161103 termina
            end
            else begin                                       {Destino}
                FSaldoDestino := Saldo div 100;
                lblSaldoDestino.Caption := Texto;
                Estado := Trim(StrLeft(FListaDestino[Indice],200));
                FormaPago := Trim(StrRight(FListaDestino[Indice],200));
                PoneEstado(lblEstadoDestino, StrToInt(Estado));
                lblFormaDestino.Caption := FormaPago;
                lblTtosDestino.Visible := TieneTransitos;
                lblInteresesDestino.Visible := TieneIntereses;
                //lblEnListaAmarillaDestino.Visible := VerificarConvenioListaAmarilla(TVariantComboBox(Sender).Value); //SS_660_MVI_20130729 // SS_660_CQU_20130711
                EstadoListaAmarilla := VerificarConvenioListaAmarilla(TVariantComboBox(Sender).Value);                                               //SS_660_MVI_20130729
//                if EstadoListaAmarilla <> 'NULL' then begin                                                                                        //SS_660_MVI_20130909  //SS_660_MVI_20130729
               if EstadoListaAmarilla <> '' then begin                                                                                               //SS_660_MVI_20130909  //SS_660_MVI_20130729
                lblEnListaAmarillaDestino.Visible  := True;                                                                                                 //SS_660_MVI_20130729
                lblEnListaAmarillaDestino.Caption  := EstadoListaAmarilla;                                                                                  //SS_660_MVI_20130729
                                                                                                                                                     //SS_660_MVI_20130729
                end                                                                                                                                  //SS_660_MVI_20130729
                else begin                                                                                                                           //SS_660_MVI_20130729
                                                                                                                                                     //SS_660_MVI_20130729
                lblEnListaAmarillaDestino.Visible  := False;                                                                                                //SS_660_MVI_20130729
                                                                                                                                                     //SS_660_MVI_20130729
                end;
            end;
        end
        else begin
            LimpiaLabels(cLimpiarTodo);
        end;

    except on e:exception do begin
        	MsgBoxErr(MSG_ERROR, e.Message, Caption, MB_ICONERROR);
    	end;
    end;

end;

{**********************************************************
        			Validaciones
 Author: mbecerra
 Date: 28-Julio-2009
 Description: 	Realiza las validaciones necesarias antes de permitir el traspaso
 				de deuda entre convenios
*********************************************************** }
function TTraspasarDeudaForm.Validaciones;
resourcestring
    MSG_FALTA_RUT	= 'No ha ingresado el Rut';
    MSG_FALTA_CONV	= 'No ha seleccionado el Convenio';
    MSG_FALTA_MONTO	= 'El Monto a trasferir debe ser mayor que cero';
    MSG_SALDO_CERO	= 'El convenio desde donde se sacar� el monto tiene saldo cero';
    MSG_SALDO_NOT	= 'No est� el saldo del convenio';
    MSG_IGUALES		= 'No puede indicar el mismo convenio para el traspaso de Deudas';
    MSG_SALDO_INS	= 'El monto a transferir excede el saldo del convenio';

begin
	{validaciones generales}
    Result := ValidateControls(	[	edtRutOrigen, edtRutDestino,
    							 	vcbConvenioOrigen, vcbConvenioDestino,
    								neMonto, vcbConvenioOrigen
    							],
                            	[	edtRutOrigen.Text <> '', edtRutDestino.Text <> '',
                                 	vcbConvenioOrigen.Text <> '', vcbConvenioDestino.Text <> '',
                                 	neMonto.Value > 0, lblSaldoOrigen.Caption <> ''
                                ],
                                Caption,
                                [	MSG_FALTA_RUT, MSG_FALTA_RUT, MSG_FALTA_CONV,
                                 	MSG_FALTA_CONV, MSG_FALTA_MONTO, MSG_SALDO_NOT
                                ]
    						);

    {validaciones de saldos}
    if Result then begin
    	Result := ValidateControls(
        						[ vcbConvenioOrigen, vcbConvenioDestino, vcbConvenioOrigen ],
                                [ FSaldoOrigen <> 0,
                                  vcbConvenioOrigen.Value <> vcbConvenioDestino.Value,
                                  Abs(FSaldoOrigen) >= neMonto.ValueInt
                                ],
                                Caption,
                                [ MSG_SALDO_CERO, MSG_IGUALES, MSG_SALDO_INS ]
    						);

    end;

    { Las validaciones de negocio se las dejamos al SP, ya que
      se pretende usar el SP de manera independiente a la aplicaci�n Delphi}
end;


{**********************************************************
        			TraspasarDeuda
 Author: mbecerra
 Date: 28-Julio-2009
 Description: 	Realiza el traspaso de deuda entre convenios
*********************************************************** }
procedure TTraspasarDeudaForm.TraspasarDeuda;
resourcestring
    MSG_ERROR = 'Error al traspasar la deuda';
    MSG_EXITO = 'El Traspaso entre convenios se ha efectuado exitosamente';
	MSG_SEGURO = 'Se traspasar� un monto de $ %.0n desde el convenio %s al convenio %s �Desea continuar?';

begin
    if MsgBox(	Format(MSG_SEGURO, [neMonto.Value, Trim(vcbConvenioOrigen.Text), Trim(vcbConvenioDestino.Text)]),
    			Caption, MB_YESNO + MB_ICONQUESTION) = IDNO then Exit;
	try
        spTraspasarEntreConvenios.Close;
        with spTraspasarEntreConvenios do begin
        	Parameters.Refresh;
        	Parameters.ParamByName('@CodigoConvenioOrigen').Value		:= vcbConvenioOrigen.Value;
        	Parameters.ParamByName('@CodigoConvenioDestino').Value		:= vcbConvenioDestino.Value;
			Parameters.ParamByName('@MontoATraspasar').Value			:= neMonto.ValueInt;
        	Parameters.ParamByName('@Usuario').Value 					:= UsuarioSistema;
			Parameters.ParamByName('@MensajeError').Value				:= '';
        	ExecProc;
        	if Parameters.ParamByName('@MensajeError').Value <> '' then begin
        		MsgBox(Parameters.ParamByName('@MensajeError').Value, Caption, MB_ICONWARNING);
        	end
        	else begin
        		MsgBox(MSG_EXITO, Caption, MB_ICONINFORMATION);
            	vcbConvenioOrigen.ItemIndex := -1;
	            vcbConvenioDestino.ItemIndex := -1;
	            LimpiaLabels(cLimpiarTodo);
                edtRutOrigen.Text := '';
                edtRutDestino.Text := '';
                neMonto.Value := 0;

	        end;
        end;

    except on e:exception do begin
        	MsgBoxErr(MSG_ERROR, e.Message, Caption, MB_ICONERROR);
    	end;
    end;
end;

{**********************************************************
        			btnAceptarClick
 Author: mbecerra
 Date: 28-Julio-2009
 Description: 	Invoca a la funci�n Traspaso de Deuda posterior a las validaciones.
*********************************************************** }
procedure TTraspasarDeudaForm.btnAceptarClick(Sender: TObject);
begin
    if Validaciones() then TraspasarDeuda();

end;


{**********************************************************
        			btnCancelarClick
 Author: mbecerra
 Date: 28-Julio-2009
 Description: 	Cierra el formulario
*********************************************************** }
procedure TTraspasarDeudaForm.btnCancelarClick(Sender: TObject);
begin
	Close;
end;

{**********************************************************
        			BuscarDatosCliente
 Author: mbecerra
 Date: 28-Julio-2009
 Description: 	Busca los datos del cliente, de acuerdo al rut ingresado como par�metro

*********************************************************** }
procedure TTraspasarDeudaForm.BuscarDatosCliente;
resourcestring
	MSG_NO_ENCONTRADO = 'No se encontraron datos asociados al Rut %s';
    MSG_ERROR = 'Ocurri� un error al intentar obtener los datos del Rut %s';

begin
	if cual = 1 then LimpiaLabels(cLimpiarNombreOrigen)
    else LimpiaLabels(cLimpiarNombreDestino);

	try

    	spObtenerDatosCliente.Close;
    	with spObtenerDatosCliente do begin
        	Parameters.Refresh;
            Parameters.ParamByName('@CodigoDocumento').Value := 'RUT';
	        Parameters.ParamByName('@NumeroDocumento').Value := Rut;
	        Open;
	        if IsEmpty then  MsgBox(Format(MSG_NO_ENCONTRADO, [Rut]), Caption, MB_ICONEXCLAMATION)
	        else begin
            	if cual = 1 then begin
                    lblNombreOrigen.Caption		:= FieldByName('Nombre').AsString;
                    lblDireccionOrigen.Caption	:= FieldByName('DescripcionDomicilio').AsString + ' ' + FieldByName('DescripcionComuna').AsString;
                    lblPersoneriaOrigen.Caption	:= FieldByName('PersoneriaDescripcion').AsString;

                    //TASK_012_GLE_20161103 inicia - Carga los valores de la cuenta
                    lblNombreDestino.Caption	 := FieldByName('Nombre').AsString;
                    lblDireccionDestino.Caption	 := FieldByName('DescripcionDomicilio').AsString + ' ' + FieldByName('DescripcionComuna').AsString;
                    lblPersoneriaDestino.Caption := FieldByName('PersoneriaDescripcion').AsString;
                    vcbConvenioDestino.ItemIndex := 0;
                    //TASK_012_GLE_20161103 termina
                end
                else begin
                	lblNombreDestino.Caption		:= FieldByName('Nombre').AsString;
                    lblDireccionDestino.Caption		:= FieldByName('DescripcionDomicilio').AsString + ' ' + FieldByName('DescripcionComuna').AsString;
                    lblPersoneriaDestino.Caption	:= FieldByName('PersoneriaDescripcion').AsString;
                end;

	        end;
    	end;

        spObtenerDatosCliente.Close;

    except on e:exception do begin
        	MsgBoxErr(Format(MSG_ERROR, [Rut]), e.Message, Caption, MB_ICONERROR);
    	end;
    end;

end;

{**********************************************************
        			BuscarConvenios
 Author: mbecerra
 Date: 28-Julio-2009
 Description: 	Busca los convenios para el Rut Ingresado

*********************************************************** }
procedure TTraspasarDeudaForm.BuscarConvenios;
resourcestring
	MSG_NO_HAY_DATOS = 'No se encontraron convenios para el %s Indicado';
    MSG_ERROR = 'Error al obtener los convenios asociados al %s';

begin
	try
    	vcb.Clear;
        Lista.Clear;
    	spObtenerConvenios.Close;
        spObtenerConvenios.Parameters.Refresh;
        spObtenerConvenios.Parameters.ParamByName('@CodigoDocumento').Value := 'RUT';
    	spObtenerConvenios.Parameters.ParamByName('@NumeroDocumento').Value := Rut;
        spObtenerConvenios.Parameters.ParamByName('@SoloActivos').Value := False;
    	spObtenerConvenios.Open;
    	if not spObtenerConvenios.IsEmpty then begin
        	while not spObtenerConvenios.Eof do begin
                //vcb.Items.Add(	spObtenerConvenios.FieldByName('NumeroConvenio').AsString,            //SS_1120_MVI_20130820
                //                 	spObtenerConvenios.FieldByName('CodigoConvenio').AsString );          //SS_1120_MVI_20130820
                Lista.Add(	spObtenerConvenios.FieldByName('CodigoEstadoConvenio').AsString +
                			Space(200) +
                            spObtenerConvenios.FieldByName('DescripMedioPago').AsString );

        		spObtenerConvenios.Next;
        	end;

        	spObtenerConvenios.Close;
            EstaClienteConMensajeEspecial(DMCOnnections.BaseCAC,PadL(Rut, 9, '0'));     //SS_1408_MCA_20151027
	        CargarConveniosRUT(DMCOnnections.BaseCAC,vcb, 'RUT',PadL(Rut, 9, '0' ));  //SS_1120_MVI_20130820
            if vcb.Items.Count > 0 then begin
            	vcb.SetFocus;
                vcb.ItemIndex := 0;
                if Assigned(vcb.OnChange) then vcb.OnChange(vcb);
            end;

    	end
    	else MsgBox(Format(MSG_NO_HAY_DATOS,[Rut]), Caption, MB_ICONEXCLAMATION);
    except on e:exception do begin
        	MsgBoxErr(Format(MSG_ERROR,[Rut]), e.Message, Caption, MB_ICONERROR);
    	end;
    end;

end;

{**********************************************************
        			ValidaBusca
 Author: mbecerra
 Date: 28-Julio-2009
 Description: 	Valida el Rut ingresado, busca los convenios asociados y
                limpia las etiquetas relacionadas al convenio.

*********************************************************** }
procedure TTraspasarDeudaForm.ValidaBusca(Sender: TObject; var Key: Char);
resourcestring
	MSG_RUT = 'El Rut %s ingresado es inv�lido';

begin
    if Key = #13 then begin
    	Key := #0;
        if ValidarRUT(DMConnections.BaseCAC, TPickEdit(Sender).Text) then begin
            if ActiveControl = edtRutOrigen then begin
            	LimpiaLabels(cLimpiarConvenioOrigen + cLimpiarIndicadoresOrigen);
            	BuscarDatosCliente(edtRutOrigen.Text, 1);
            	BuscarConvenios(edtRutOrigen.Text, vcbConvenioOrigen, FListaOrigen);
                // 12-Dic-2011
                // cada vez que se escribe el rut de origen, se limpia el rut destino
                //if edtRutDestino.Text = '' then begin
                	edtRutDestino.Text := edtRutOrigen.Text;
                    vcbConvenioDestino.Items := vcbConvenioOrigen.Items;  //TASK_012_GLE_20161103     Agrega
                    vcbConvenioDestino.ItemIndex := 0;                    //TASK_012_GLE_20161103     Agrega
                	//LimpiaLabels(cLimpiarTodoDestino);                  //TASK_012_GLE_20161103     Comenta
                    //vcbConvenioDestino.Clear;                           //TASK_012_GLE_20161103     Comenta
                //end;

            end
            else if ActiveControl = edtRutDestino then begin
            	LimpiaLabels(cLimpiarTodoDestino);
            	BuscarConvenios(edtRutDestino.Text, vcbConvenioDestino, FListaDestino);
                BuscarDatosCliente(edtRutDestino.Text, 2);
            end;

        end
        else begin
            MsgBox(Format(MSG_RUT,[TPickEdit(Sender).Text]), Caption, MB_ICONEXCLAMATION);
        end;
    end
    else begin
    	if not (Key in ['0'..'9', 'K', 'k', Char(VK_BACK)]) then Key := #0
        else begin
        	if ActiveControl = edtRutOrigen then begin
                vcbConvenioOrigen.Clear;
                // 12-Dic-2011
                // cada vez que se escribe el rut de origen, se limpia el rut destino
            	LimpiaLabels(cLimpiarTodoOrigen + cLimpiarTodoDestino);
                edtRutDestino.Clear;
                vcbConvenioDestino.Clear;
                neMonto.ValueInt := 0;
            end
            else begin
                vcbConvenioDestino.Clear;
                LimpiaLabels(cLimpiarTodoDestino);
            end;
        end;

    end;

end;

{**********************************************************
        			FormClose
 Author: mbecerra
 Date: 28-Julio-2009
 Description: 	Evento invocado al cierre del formulario
*********************************************************** }
procedure TTraspasarDeudaForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	FListaOrigen.Free;
    FListaDestino.Free;
	Action := caFree;
end;


{**********************************************************
        			VerPosiblesRut
 Author: mbecerra
 Date: 03-Octubre-2008
 Description: 	Invoca a la ventana de b�squeda de clientes, devolviendo el
            	Rut del cliente seleccionado

*********************************************************** }
procedure TTraspasarDeudaForm.VerPosiblesRut(Sender: TObject);
var
	f : TFormBuscaClientes;
    TeclaEnter : Char;
begin
	Application.CreateForm(TFormBuscaClientes, f);
    if f.Inicializa(FUltimaBusqueda) then begin
        f.ShowModal;
        if f.ModalResult = IDOK then begin
            FUltimaBusqueda := f.UltimaBusqueda;
            TPickEdit(Sender).Text := f.Persona.NumeroDocumento;
            TeclaEnter := #13;
            ValidaBusca(Sender, TeclaEnter);
        end;
    end;

    f.Free;
end;


{**********************************************************
        			ColoreaItem
 Author: mbecerra
 Date: 28-Julio-2009
 Description: 	Despliega el Item de la ComboBox con el color seg�n el estado del
 				Convenio: Rojo = Baja,  Negro = Activo 
*********************************************************** }
//procedure TTraspasarDeudaForm.ColoreaItem(Control: TWinControl;                                                   //SS_1120_MVI_20130820
//  Index: Integer; Rect: TRect; State: TOwnerDrawState);                                                           //SS_1120_MVI_20130820
//var                                                                                                               //SS_1120_MVI_20130820
//	Estado : string;                                                                                                //SS_1120_MVI_20130820
//begin                                                                                                             //SS_1120_MVI_20130820
//    with (Control as TVariantComboBox) do begin                                                                   //SS_1120_MVI_20130820
//    	if Tag = 10 then Estado := Trim(StrLeft(FListaOrigen[Index],200))                                           //SS_1120_MVI_20130820
//	    else Estado := Trim(StrLeft(FListaDestino[Index],200));                                                     //SS_1120_MVI_20130820
                                                                                                                    //SS_1120_MVI_20130820
//    	if StrToInt(Estado) = FEstadoConvenioBaja then Canvas.Font.Color := clRed                                   //SS_1120_MVI_20130820
//    	else if odSelected in State then TVariantComboBox(Control).Canvas.Font.Color := clHighlightText             //SS_1120_MVI_20130820
//    	else TVariantComboBox(Control).Canvas.Font.Color := clWindowText;                                           //SS_1120_MVI_20130820
                                                                                                                    //SS_1120_MVI_20130820
//    	Canvas.FillRect(Rect);                                                                                      //SS_1120_MVI_20130820
//    	Canvas.TextOut(Rect.Left + 5, Rect.Top, Items[Index].Caption);                                              //SS_1120_MVI_20130820
//    end;                                                                                                          //SS_1120_MVI_20130820
//end;                                                                                                              //SS_1120_MVI_20130820

{*******************************************************************************
    Procedure Name  : VerificarConvenioListaAmarilla
    Author          : CQuezadaI
    Date            : 30/04/2013
    Firma           : SS_660_CQU_20121010
    Description     : Se varifica si el convenio est� en Lista Amarilla.
*******************************************************************************}
//function TTraspasarDeudaForm.VerificarConvenioListaAmarilla(CodigoConvenio : Integer) : boolean;  //SS_660_MVI_20130729
function TTraspasarDeudaForm.VerificarConvenioListaAmarilla(CodigoConvenio : Integer) : string;
resourcestring
    //SQL_CONSULTACONVENIO        = 'SELECT dbo.EstaConvenioEnListaAmarilla(%d,''%s'')';        // SS_660_CQU_20130711
    //SQL_CONSULTACONVENIO        = 'SELECT dbo.EstaConvenioEnListaAmarilla(%d,''%s'', NULL)';  //SS_660_MVI_20130729  // SS_660_CQU_20130711
    SQL_CONSULTACONVENIO        = 'SELECT dbo.ObtenerEstadoConvenioEnListaAmarilla(%d)';    //SS_660_MVI_20130729
//var                                                                                       //SS_660_MVI_20130729
   // sFecha      : string;                                                                 //SS_660_MVI_20130729
begin
//    Result      :=  'NULL';                                                               //SS_660_MVI_20130909
      Result      :=  '';                                                                   //SS_660_MVI_20130909
    if CodigoConvenio > 0 then begin
        try
            Cursor := CURSOR_RELOJ;
            try
                //sFecha      :=  FormatDateTime('yyyymmdd hh:nn:ss', SysutilsCN.NowBaseCN(DMConnections.BaseCAC));                             //SS_660_MVI_20130729
                //Result      :=  SysutilsCN.QueryGetBooleanValue(                                                                              //SS_660_MVI_20130729
                //                        DMConnections.BaseCAC,                                                                                //SS_660_MVI_20130729
                //                        Format(SQL_CONSULTACONVENIO, [CodigoConvenio, sFecha])                                                //SS_660_MVI_20130729
                //            );
                 Result:= QueryGetValue(   DMConnections.BaseCAC,                                                                        //SS_660_MVI_20130729
                                                 Format(SQL_CONSULTACONVENIO,                                                            //SS_660_MVI_20130729
                                                         [   CodigoConvenio                                                    //SS_660_MVI_20130729
                                                         ]                                                                               //SS_660_MVI_20130729
                                                       )                                                                                 //SS_660_MVI_20130729
                                                       );                                                                                //SS_660_MVI_20130729
                                                                                                                                         //SS_660_MVI_20130729
                                                                                                                                         //SS_660_MVI_20130729
            except
                on e: Exception do begin
                    ShowMsgBoxCN(e, Self);
                end;
            end;
        finally
            Cursor := CURSOR_DEFECTO;
        end;
    end;
end;
//BEGIN:SS_1120_MVI_20130820 -------------------------------------------------
{-----------------------------------------------------------------------------
  Procedure: TTraspasarDeudaForm.vcbConvenioDestinoDrawItem
  Date:      20-August-2013
  Description: Verifica si existe alg�n convenio de baja para llamar a la funci�n
                que colorea de rojo el item.
-----------------------------------------------------------------------------}
procedure TTraspasarDeudaForm.vcbConvenioDestinoDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  if Pos( TXT_CONVENIO_DE_BAJA, vcbConvenioDestino.Items[Index].Caption ) > 0 then  begin     //SS_1120_MVI_20130820
                                                                                              //SS_1120_MVI_20130820
     ItemComboBoxColor(Control, Index, Rect, State, clred, true);                             //SS_1120_MVI_20130820
                                                                                              //SS_1120_MVI_20130820
  end                                                                                         //SS_1120_MVI_20130820
  else begin                                                                                  //SS_1120_MVI_20130820
                                                                                              //SS_1120_MVI_20130820
     ItemComboBoxColor(Control, Index, Rect, State);                                          //SS_1120_MVI_20130820
                                                                                              //SS_1120_MVI_20130820
  end;                                                                                        //SS_1120_MVI_20130820

end;

{-----------------------------------------------------------------------------
  Procedure: TTraspasarDeudaForm.vcbConvenioOrigenDrawItem
  Date:      20-August-2013
  Description: Verifica si existe alg�n convenio de baja para llamar a la funci�n
                que colorea de rojo el item.
-----------------------------------------------------------------------------}
procedure TTraspasarDeudaForm.vcbConvenioOrigenDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  if Pos( TXT_CONVENIO_DE_BAJA, vcbConvenioOrigen.Items[Index].Caption ) > 0 then  begin

     ItemComboBoxColor(Control, Index, Rect, State, clred, true);

  end
  else begin

     ItemComboBoxColor(Control, Index, Rect, State);

  end;                                                                                     

end;
//END:SS_1120_MVI_20130820 ----------------------------------------------------
end.
