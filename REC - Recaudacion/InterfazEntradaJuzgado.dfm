object FrmInterfazEntradaJuzgado: TFrmInterfazEntradaJuzgado
  Left = 304
  Top = 320
  BorderStyle = bsDialog
  Caption = 'Interfaz de Entrada del Juzgado Local'
  ClientHeight = 105
  ClientWidth = 457
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 457
    Height = 73
    BevelOuter = bvLowered
    TabOrder = 0
    object Label1: TLabel
      Left = 10
      Top = 18
      Width = 114
      Height = 13
      Caption = '&Archivo de Entrada:'
      FocusControl = peArchivo
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object ProgressBar: TProgressBar
      Left = 13
      Top = 45
      Width = 433
      Height = 16
      Min = 0
      Max = 100
      TabOrder = 1
    end
    object peArchivo: TPickEdit
      Left = 145
      Top = 14
      Width = 303
      Height = 21
      Enabled = True
      TabOrder = 0
      OnChange = peArchivoChange
      Decimals = 0
      EditorStyle = bteTextEdit
      OnButtonClick = peArchivoButtonClick
    end
  end
  object btnAceptar: TDPSButton
    Left = 297
    Top = 78
    Caption = '&Aceptar'
    Default = True
    ModalResult = 1
    TabOrder = 1
    OnClick = btnAceptarClick
  end
  object btnCancelar: TDPSButton
    Left = 377
    Top = 78
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 2
    OnClick = btnCancelarClick
  end
  object InterfazEntradaJuzgado: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'InterfazEntradaJuzgado'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@NumeroHistorialInfraccion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroExpediente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Resolucion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@ExpedienteActual'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@ResolucionActual'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 2
        Value = Null
      end
      item
        Name = '@FechaTramiteJudicial'
        Attributes = [paNullable]
        DataType = ftDateTime
        Direction = pdInputOutput
        Value = Null
      end>
    Left = 72
    Top = 48
  end
  object OpenDialog: TOpenDialog
    DefaultExt = 'TXT'
    Filter = 'Archivo de texto|*.TXT|Archivos Excel|*.xls'
    Left = 104
    Top = 48
  end
end
