object RefinanciacionEditarTelefonosForm: TRefinanciacionEditarTelefonosForm
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = ' Editar Tel'#233'fonos Refinanciaci'#243'n '
  ClientHeight = 138
  ClientWidth = 667
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnKeyDown = FormKeyDown
  DesignSize = (
    667
    138)
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 98
    Width = 667
    Height = 40
    Align = alBottom
    Color = 14408667
    ParentBackground = False
    TabOrder = 0
    DesignSize = (
      667
      40)
    object btnCancelar: TButton
      Left = 584
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Cancelar'
      TabOrder = 0
      OnClick = btnCancelarClick
    end
    object btnGrabar: TButton
      Left = 506
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Aceptar'
      TabOrder = 1
      OnClick = btnGrabarClick
    end
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 651
    Height = 84
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = '  Editar Tel'#233'fonos  '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    inline FrameTelefono1: TFrameTelefono
      Left = 25
      Top = 20
      Width = 612
      Height = 25
      TabOrder = 0
      TabStop = True
      ExplicitLeft = 25
      ExplicitTop = 20
    end
    inline FrameTelefono2: TFrameTelefono
      Left = 25
      Top = 44
      Width = 612
      Height = 25
      TabOrder = 1
      TabStop = True
      ExplicitLeft = 25
      ExplicitTop = 44
    end
  end
end
