unit frmRefinanciacionSeleccionarRepLegalCN;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, StdCtrls, ExtCtrls, DmiCtrls, ListBoxEx, DBListEx,
  BuscaClientes, PeaTypes,
  DMConnection, Util, PeaProcs, PeaProcsCN, Provider, DBClient, VariantComboBox,
  SysUtilsCN;

type
  TRefinanciacionRepLegal = record
    NumeroDocumento,
    Nombre,
    Apellido,
    ApellidoMaterno: string;
  end;

type
  TRefinanciacionSeleccionarRepLegalCNForm = class(TForm)
    GroupBox1: TGroupBox;
    DBListEx1: TDBListEx;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    Label1: TLabel;
    edtNombre: TEdit;
    Panel1: TPanel;
    btnCancelar: TButton;
    btnAceptar: TButton;
    chbTodos: TCheckBox;
    edtApellido: TEdit;
    edtApellidoMaterno: TEdit;
    Label2: TLabel;
    Label4: TLabel;
    spObtenerRefinanciacionRepresentantesCN: TADOStoredProc;
    dsRepresentantesCN: TDataSource;
    cdsRepresentantesCN: TClientDataSet;
    dspObtenerRefinanciacionRepresentantesCN: TDataSetProvider;
    cdsRepresentantesCNCodigoRepresentante: TAutoIncField;
    cdsRepresentantesCNApellido: TStringField;
    cdsRepresentantesCNApellidoMaterno: TStringField;
    cdsRepresentantesCNNombre: TStringField;
    cdsRepresentantesCNCodigoDocumento: TStringField;
    cdsRepresentantesCNNumeroDocumento: TStringField;
    cdsRepresentantesCNCodigoTipoMedioPago: TSmallintField;
    spObtenerRefinanciacionRepresentanteCN: TADOStoredProc;
    spActualizarRefinanciacionRepresentantesCN: TADOStoredProc;
    cdsRepresentantesCNTipoMedioPagoDesc: TStringField;
    peRut: TPickEdit;
    GroupBox3: TGroupBox;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    edtEmpresaNombre: TEdit;
    edtEmpresaDireccion: TEdit;
    vcbEmpresaRegion: TVariantComboBox;
    vcbEmpresaComuna: TVariantComboBox;
    cdsRepresentantesCNEmpresa: TStringField;
    cdsRepresentantesCNDireccion: TStringField;
    cdsRepresentantesCNCodigoPais: TStringField;
    cdsRepresentantesCNCodigoRegion: TStringField;
    cdsRepresentantesCNCodigoComuna: TStringField;
    procedure DBListEx1DblClick(Sender: TObject);
    procedure chbTodosClick(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure peRutChange(Sender: TObject);
    procedure peRutButtonClick(Sender: TObject);
    procedure peRutKeyPress(Sender: TObject; var Key: Char);
    procedure vcbEmpresaRegionChange(Sender: TObject);
  private
    { Private declarations }
    FTipoMedioPago: Integer;
    FUltimaBusqueda: TBusquedaCliente;
    FPermisosModificarRepLegalCN: Boolean;

    procedure LimpiarCampos;
    procedure TraerDatos;
  public
    { Public declarations }
    function Inicializar(TipoMedioPago: Integer; TipoMedioPagoDesc: String; Representante: TRefinanciacionRepLegal): Boolean;
  end;

var
  RefinanciacionSeleccionarRepLegalCNForm: TRefinanciacionSeleccionarRepLegalCNForm;

implementation

{$R *.dfm}

procedure TRefinanciacionSeleccionarRepLegalCNForm.btnAceptarClick(Sender: TObject);
    resourcestring
        rsTituloValidaciones    = 'Validaci�n Datos Representante CN';
        rsErrorControl_Rut      = 'El Rut introducido NO es Correcto.';
        rsErrorControl_Nombre   = 'El campo Nombre no puede estar en Blanco.';
        rsErrorControl_Apellido = 'El campo Apellido no puede estar en Blanco.';

        rsRepresentanteAlta     = 'Alta Representante Legal CN';
        rsRepresentanteNoExiste = 'El Representante introducido NO Existe en la Base de Datos. � Desea Darlo de Alta ?';

        rsErrorActualizacionTitulo = 'Error Alta / Actualizaci�n';
        rsErrorActualizacion       = 'Se produjo un Error al dar de Alta / Actualizar los datos del Representante.' + CRLF + CRLF + 'Error: %s.';

    function ValidarCondiciones: boolean;
        var
            vControles  : Array [1..3] of TControl;
            vCondiciones: Array [1..3] of Boolean;
            vMensajes   : Array [1..3] of String;
    begin

        vControles[1]   := peRut;
        vControles[2]   := edtNombre;
        vControles[3]   := edtApellido;

        vCondiciones[1] := ValidarRUT(DMConnections.BaseCAC, Trim(peRut.Text));
        vCondiciones[2] := (Trim(edtNombre.Text) <> '');
        vCondiciones[3] := (Trim(edtApellido.Text) <> '');

        vMensajes[1]    := rsErrorControl_Rut;
        vMensajes[2]    := rsErrorControl_Nombre;
        vMensajes[3]    := rsErrorControl_Apellido;

        Result := ValidateControlsCN(vControles, vCondiciones, rsTituloValidaciones, vMensajes, Self);
    end;
begin
    if ValidarCondiciones then try
        with spObtenerRefinanciacionRepresentanteCN do begin
            if Active then Close;
            Parameters.ParamByName('@CodigoDocumento').Value := 'RUT';
            Parameters.ParamByName('@NumeroDocumento').Value := Trim(peRut.Text);
            Open;
        end;
        try
            if spObtenerRefinanciacionRepresentanteCN.RecordCount = 0 then begin
                if ShowMsgBoxCN(rsRepresentanteAlta, rsRepresentanteNoExiste, MB_ICONQUESTION, Self) = mrOk then begin
                    with spActualizarRefinanciacionRepresentantesCN do begin
                        Parameters.ParamByName('@CodigoRepresentante').Value := Null;
                        Parameters.ParamByName('@Apellido').Value            := Trim(edtApellido.Text);
                        Parameters.ParamByName('@ApellidoMaterno').Value     := iif(Trim(edtApellidoMaterno.Text) = '', Null, Trim(edtApellidoMaterno.Text));
                        Parameters.ParamByName('@Nombre').Value              := Trim(edtNombre.Text);
                        Parameters.ParamByName('@CodigoDocumento').Value     := 'RUT';
                        Parameters.ParamByName('@NumeroDocumento').Value     := Trim(peRut.Text);
                        Parameters.ParamByName('@CodigoTipoMedioPago').Value := FTipoMedioPago;
                        Parameters.ParamByName('@Empresa').Value             := iif(Trim(edtEmpresaNombre.Text) = EmptyStr, Null, Trim(edtEmpresaNombre.Text));
                        Parameters.ParamByName('@Direccion').Value           := iif(Trim(edtEmpresaDireccion.Text) = EmptyStr, Null, Trim(edtEmpresaDireccion.Text));
                        Parameters.ParamByName('@CodigoPais').Value          := iif(vcbEmpresaRegion.ItemIndex > 0, PAIS_CHILE, Null);
                        Parameters.ParamByName('@CodigoRegion').Value        := iif(vcbEmpresaRegion.ItemIndex > 0, vcbEmpresaRegion.Value, Null);
                        Parameters.ParamByName('@CodigoComuna').Value        := iif(vcbEmpresaComuna.ItemIndex > 0, vcbEmpresaComuna.Value, Null);
                        Parameters.ParamByName('@Usuario').Value             := UsuarioSistema;
                        ExecProc;
                    end;
                end;
            end
            else begin
                with spObtenerRefinanciacionRepresentanteCN do begin
                    if (Trim(FieldByName('Nombre').AsString) <> Trim(edtNombre.Text)) or
                        (Trim(FieldByName('Apellido').AsString) <> Trim(edtApellido.Text)) or
                        (Trim(FieldByName('ApellidoMaterno').AsString) <> Trim(edtApellidoMaterno.Text)) or
                        (FieldByName('Empresa').AsString <> Trim(edtEmpresaNombre.Text)) or
                        (FieldByName('Direccion').AsString <> Trim(edtEmpresaDireccion.Text)) or
                        (FieldByName('CodigoRegion').AsString <> iif(vcbEmpresaRegion.ItemIndex > 0, vcbEmpresaRegion.Value, EmptyStr)) or
                        (FieldByName('CodigoComuna').AsString <> iif(vcbEmpresaComuna.ItemIndex > 0, vcbEmpresaComuna.Value, EmptyStr)) then begin
                        with spActualizarRefinanciacionRepresentantesCN do begin
                            Parameters.ParamByName('@CodigoRepresentante').Value := spObtenerRefinanciacionRepresentanteCN.FieldByName('CodigoRepresentante').AsInteger;
                            Parameters.ParamByName('@Apellido').Value            := Trim(edtApellido.Text);
                            Parameters.ParamByName('@ApellidoMaterno').Value     := iif(Trim(edtApellidoMaterno.Text) = '', Null, Trim(edtApellidoMaterno.Text));
                            Parameters.ParamByName('@Nombre').Value              := Trim(edtNombre.Text);
                            Parameters.ParamByName('@CodigoDocumento').Value     := 'RUT';
                            Parameters.ParamByName('@NumeroDocumento').Value     := Trim(peRut.Text);
                            Parameters.ParamByName('@CodigoTipoMedioPago').Value := Null;
                            Parameters.ParamByName('@Empresa').Value             := iif(Trim(edtEmpresaNombre.Text) = EmptyStr, Null, Trim(edtEmpresaNombre.Text));
                            Parameters.ParamByName('@Direccion').Value           := iif(Trim(edtEmpresaDireccion.Text) = EmptyStr, Null, Trim(edtEmpresaDireccion.Text));
                            Parameters.ParamByName('@CodigoPais').Value          := iif(vcbEmpresaRegion.ItemIndex > 0, PAIS_CHILE, Null);
                            Parameters.ParamByName('@CodigoRegion').Value        := iif(vcbEmpresaRegion.ItemIndex > 0, vcbEmpresaRegion.Value, Null);
                            Parameters.ParamByName('@CodigoComuna').Value        := iif(vcbEmpresaComuna.ItemIndex > 0, vcbEmpresaComuna.Value, Null);
                            Parameters.ParamByName('@Usuario').Value             := UsuarioSistema;
                            ExecProc;
                        end;
                    end;
                end;
            end;
        except
            on e:Exception do begin
                ShowMsgBoxCN(rsErrorActualizacionTitulo, Format(rsErrorActualizacion, [e.Message]), MB_ICONERROR, Self);
            end;
        end;
    finally
        ModalResult := mrOk;
    end;
end;

procedure TRefinanciacionSeleccionarRepLegalCNForm.btnCancelarClick(Sender: TObject);
begin
    ModalResult := mrCancel;
end;

procedure TRefinanciacionSeleccionarRepLegalCNForm.chbTodosClick(Sender: TObject);
    var
        Puntero: TBookmark;
begin
    try
        CambiarEstadoCursor(CURSOR_RELOJ);
        Puntero := cdsRepresentantesCN.GetBookmark;

        if cdsRepresentantesCN.Active then cdsRepresentantesCN.Close;

        with spObtenerRefinanciacionRepresentantesCN do begin
            Parameters.Refresh;
            Parameters.ParamByName('@CodigoDocumento').Value     := Null;
            Parameters.ParamByName('@NumeroDocumento').Value     := Null;
            Parameters.ParamByName('@CodigoTipoMedioPago').Value := iif(chbTodos.Checked, Null, FTipoMedioPago);
        end;

        cdsRepresentantesCN.Open;
    finally
        if Assigned(Puntero) then begin
            if cdsRepresentantesCN.BookmarkValid(Puntero) then begin
                cdsRepresentantesCN.GotoBookmark(Puntero);
            end;
            cdsRepresentantesCN.FreeBookmark(Puntero);
        end;

        CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

procedure TRefinanciacionSeleccionarRepLegalCNForm.DBListEx1DblClick(Sender: TObject);
begin
    if cdsRepresentantesCN.RecordCount > 0 then begin
        peRut.Text              := cdsRepresentantesCNNumeroDocumento.AsString;
        edtNombre.Text          := cdsRepresentantesCNNombre.AsString;
        edtApellido.Text        := cdsRepresentantesCNApellido.AsString;
        edtApellidoMaterno.Text := cdsRepresentantesCNApellidoMaterno.AsString;

        edtNombre.Enabled          := False;
        edtApellido.Enabled        := False;
        edtApellidoMaterno.Enabled := False;

        edtEmpresaNombre.Text := cdsRepresentantesCNEmpresa.AsString;
        edtEmpresaDireccion.Text := cdsRepresentantesCNDireccion.AsString;

        if not cdsRepresentantesCNCodigoRegion.IsNull then begin
            vcbEmpresaRegion.ItemIndex := vcbEmpresaRegion.Items.IndexOfValue(cdsRepresentantesCNCodigoRegion.AsString);
            CargarComunas(DMConnections.BaseCAC, vcbEmpresaComuna, PAIS_CHILE, cdsRepresentantesCNCodigoRegion.AsString, cdsRepresentantesCNCodigoComuna.AsString, True);
            vcbEmpresaComuna.Enabled := FPermisosModificarRepLegalCN;
        end
        else begin
            vcbEmpresaRegion.ItemIndex := 0;
            vcbEmpresaComuna.Clear;
            vcbEmpresaComuna.Enabled := False;
        end;

        btnAceptar.Enabled := True;
    end;
end;

procedure TRefinanciacionSeleccionarRepLegalCNForm.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
    case Key of
        VK_ESCAPE: ModalResult := mrCancel;
    end;
end;

function TRefinanciacionSeleccionarRepLegalCNForm.Inicializar(TipoMedioPago: Integer; TipoMedioPagoDesc: String; Representante: TRefinanciacionRepLegal): Boolean;
begin
    FTipoMedioPago             := TipoMedioPago;
    edtNombre.Enabled          := False;
    edtApellido.Enabled        := False;
    edtApellidoMaterno.Enabled := False;
    Caption := Format(Caption, [TipoMedioPagoDesc]);

    FPermisosModificarRepLegalCN := ExisteAcceso('modif_rep_legal_cn_gestion_ref');

    CargarRegiones(DMConnections.BaseCAC, vcbEmpresaRegion, PAIS_CHILE, EmptyStr, True);

    with spObtenerRefinanciacionRepresentantesCN do begin
        Parameters.ParamByName('@CodigoTipoMedioPago').Value := FTipoMedioPago;
    end;

    cdsRepresentantesCN.Open;

    if Representante.NumeroDocumento <> EmptyStr then begin
        peRut.Text              := Representante.NumeroDocumento;
        edtNombre.Text          := Representante.Nombre;
        edtApellido.Text        := Representante.Apellido;
        edtApellidoMaterno.Text := Representante.ApellidoMaterno;

        if cdsRepresentantesCN.Locate('NumeroDocumento', Representante.NumeroDocumento, []) then begin
            edtEmpresaNombre.Text    := cdsRepresentantesCNEmpresa.AsString;
            edtEmpresaDireccion.Text := cdsRepresentantesCNDireccion.AsString;

            if not cdsRepresentantesCNCodigoRegion.IsNull then begin
                vcbEmpresaRegion.ItemIndex := vcbEmpresaRegion.Items.IndexOfValue(cdsRepresentantesCNCodigoRegion.AsString);
                CargarComunas(DMConnections.BaseCAC, vcbEmpresaComuna, PAIS_CHILE, cdsRepresentantesCNCodigoRegion.AsString, cdsRepresentantesCNCodigoComuna.AsString, True);
            end
            else begin
                vcbEmpresaRegion.ItemIndex := 0;
            end;
        end
        else TraerDatos;

        btnAceptar.Enabled := FPermisosModificarRepLegalCN;
    end;

    peRut.Enabled := FPermisosModificarRepLegalCN;

    edtEmpresaNombre.Enabled    := FPermisosModificarRepLegalCN;
    edtEmpresaDireccion.Enabled := FPermisosModificarRepLegalCN;
    vcbEmpresaRegion.Enabled    := FPermisosModificarRepLegalCN;
    vcbEmpresaComuna.Enabled    := FPermisosModificarRepLegalCN and (vcbEmpresaComuna.Items.Count > 0);

    Result := True;
end;

procedure TRefinanciacionSeleccionarRepLegalCNForm.peRutButtonClick(Sender: TObject);
    var
        BuscaClientes: TFormBuscaClientes;
begin
    try
        peRut.OnChange := Nil;
        BuscaClientes := TFormBuscaClientes.Create(Self);
        if BuscaClientes.Inicializa(FUltimaBusqueda)then begin
            if BuscaClientes.ShowModal = mrOk then begin
                LimpiarCampos;

                FUltimaBusqueda         := BuscaClientes.UltimaBusqueda;
                peRut.Text              := BuscaClientes.Persona.NumeroDocumento;
                edtNombre.Text          := BuscaClientes.Persona.Nombre;
                edtApellido.Text        := BuscaClientes.Persona.Apellido;
                edtApellidoMaterno.Text := BuscaClientes.Persona.ApellidoMaterno;

                TraerDatos;
                btnAceptar.Enabled      := True;
            end;
        end;
    finally
        if Assigned(BuscaClientes) then FreeAndNil(BuscaClientes);
        peRut.OnChange := peRutChange;
    end;
end;

procedure TRefinanciacionSeleccionarRepLegalCNForm.peRutChange(Sender: TObject);
begin
    LimpiarCampos;
end;

procedure TRefinanciacionSeleccionarRepLegalCNForm.peRutKeyPress(Sender: TObject; var Key: Char);
    var
        FPersona: TDatosPersonales;
    resourcestring
        rs_BUSQUEDA_TITULO = 'B�squeda Persona';
        rs_BUSQUEDA_NO_ENCONTRADA = 'El Rut introducido NO es Correcto.';
begin
    if key = #13 then begin
        FPersona := ObtenerDatosPersonales(DMConnections.BaseCAC, 'RUT', peRut.Text);

        if FPersona.CodigoPersona <> -1 then begin
            LimpiarCampos;

            edtNombre.Text          := FPersona.Nombre;
            edtApellido.Text        := FPersona.Apellido;
            edtApellidoMaterno.Text := FPersona.ApellidoMaterno;

            TraerDatos;
            btnAceptar.Enabled      := True;
        end
        else begin
            LimpiarCampos;

            if ValidarRUT(DMConnections.BaseCAC, peRut.Text) then begin
                btnAceptar.Enabled         := True;
                edtNombre.Enabled          := True;
                edtApellido.Enabled        := True;
                edtApellidoMaterno.Enabled := True;
                edtNombre.SetFocus;
            end
            else begin
                btnAceptar.Enabled         := False;
                edtNombre.Enabled          := False;
                edtApellido.Enabled        := False;
                edtApellidoMaterno.Enabled := False;
                ShowMsgBoxCN(rs_BUSQUEDA_TITULO, rs_BUSQUEDA_NO_ENCONTRADA, MB_ICONWARNING, Self);
            end;
        end;
    end;
end;

procedure TRefinanciacionSeleccionarRepLegalCNForm.vcbEmpresaRegionChange(Sender: TObject);
begin
    if vcbEmpresaRegion.Items.Count > 0 then begin
        if vcbEmpresaRegion.Value <> Null then try
            CambiarEstadoCursor(CURSOR_RELOJ);

            CargarComunas(DMConnections.BaseCAC, vcbEmpresaComuna, PAIS_CHILE, vcbEmpresaRegion.Value, '', True);
        finally
            CambiarEstadoCursor(CURSOR_DEFECTO);
        end
        else vcbEmpresaComuna.Items.Clear;
    end
    else vcbEmpresaComuna.Items.Clear;

    vcbEmpresaComuna.Enabled := FPermisosModificarRepLegalCN and (vcbEmpresaComuna.Items.Count > 0);
end;

procedure TRefinanciacionSeleccionarRepLegalCNForm.LimpiarCampos;
begin
    edtNombre.Text             := EmptyStr;
    edtApellido.Text           := EmptyStr;
    edtApellidoMaterno.Text    := EmptyStr;

    edtEmpresaNombre.Text      := EmptyStr;
    edtEmpresaDireccion.Text   := EmptyStr;
    vcbEmpresaRegion.ItemIndex := 0;
    vcbEmpresaComuna.Items.Clear;
    vcbEmpresaComuna.Enabled   := False;

    btnAceptar.Enabled         := False;
    edtNombre.Enabled          := False;
    edtApellido.Enabled        := False;
    edtApellidoMaterno.Enabled := False;
end;

procedure TRefinanciacionSeleccionarRepLegalCNForm.TraerDatos;
begin
    try
        CambiarEstadoCursor(CURSOR_RELOJ);

        with spObtenerRefinanciacionRepresentanteCN do begin
            if Active then Close;
            Parameters.ParamByName('@CodigoDocumento').Value := 'RUT';
            Parameters.ParamByName('@NumeroDocumento').Value := Trim(peRut.Text);
            Open;
        end;

        if spObtenerRefinanciacionRepresentanteCN.RecordCount = 1 then begin
            edtEmpresaNombre.Text    := spObtenerRefinanciacionRepresentanteCN.FieldByName('Empresa').AsString;
            edtEmpresaDireccion.Text := spObtenerRefinanciacionRepresentanteCN.FieldByName('Direccion').AsString;

            if not spObtenerRefinanciacionRepresentanteCN.FieldByName('CodigoRegion').IsNull then begin
                vcbEmpresaRegion.ItemIndex := vcbEmpresaRegion.Items.IndexOfValue(spObtenerRefinanciacionRepresentanteCN.FieldByName('CodigoRegion').AsString);
                if not spObtenerRefinanciacionRepresentanteCN.FieldByName('CodigoComuna').IsNull then begin
                    CargarComunas(DMConnections.BaseCAC, vcbEmpresaComuna, PAIS_CHILE, spObtenerRefinanciacionRepresentanteCN.FieldByName('CodigoRegion').AsString, spObtenerRefinanciacionRepresentanteCN.FieldByName('CodigoComuna').AsString, True);
                    vcbEmpresaComuna.Enabled := FPermisosModificarRepLegalCN;
                end
                else begin
                    vcbEmpresaComuna.Clear;
                    vcbEmpresaComuna.Enabled := False;
                end;
            end
            else begin
                vcbEmpresaRegion.ItemIndex := 0;
                vcbEmpresaComuna.Clear;
                vcbEmpresaComuna.Enabled := False;
            end;
        end;
    finally
        CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

end.
