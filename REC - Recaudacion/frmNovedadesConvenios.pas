unit frmNovedadesConvenios;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, UtilProc, RStrings, StrUtils, PeaProcs, Grids, DBGrids, DBClient, DB, ADODB, DMConnection, Util;

type
  TfrmNovedadesConvenios = class(TForm)
    btnAplicar: TButton;
    btnSalir: TButton;
    txt_Ruta: TEdit;
    lblRuta: TLabel;
    btnBuscar: TButton;
    dbgrdNovedades: TDBGrid;
    cdsNovedades: TClientDataSet;
    dsNovedades: TDataSource;
    btnFormato: TButton;
    procedure btnBuscarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);      
    procedure btnAplicarClick(Sender: TObject);
    procedure btnFormatoClick(Sender: TObject);
  private
    function LeerArchivo(rutaArchivo: string): TStringList;
    procedure CargarGrid(Datos: TStringList);
  public
    { Public declarations }
  end;

var
  frm_NovedadesConvenios: TfrmNovedadesConvenios;

implementation

{$R *.dfm}

procedure TfrmNovedadesConvenios.btnBuscarClick(Sender: TObject);
var
    OD : TOpenDialog;
    Datos: TStringList;
    I: Integer;
    repetido: string;
begin
    OD := nil;
    try
        txt_Ruta.Clear;
        if cdsNovedades.Active then
            cdsNovedades.EmptyDataSet;

        OD := TOpenDialog.Create(self);
        OD.Filter := 'CSV files only|*.csv';
        OD.Title := 'Buscar Archivo Novedades';
        OD.Execute;
        if OD.Files.Count>0 then
        begin

            Datos := LeerArchivo(OD.Files[0]);

            if Datos.Count>0 then
            begin
               txt_Ruta.Text := OD.Files[0];

                CargarGrid(Datos);

                repetido:= Datos[0];
                for I := 1 to Datos.Count - 1 do
                begin
                    if repetido = Datos[i] then
                    begin
                        MsgBox('Existen un numero de convenio repetido en la lista');
                        Break;
                    end;      
                end;

                btnAplicar.Enabled:= True;
            end;

        end;
    finally
        OD.Free;
    end; 
end;



function TfrmNovedadesConvenios.LeerArchivo(rutaArchivo: string): TStringList;
var
    myFile : TextFile;
    texto: string;
    campos : TStrings;
    i: Integer;
    errores: string;
begin

    try
        if not FileExists(rutaArchivo) then
        begin
             MsgBoxErr('No existe el archivo', 'No se encuentra el archivo en la ruta especificada', 'Error de Carga', MB_ICONSTOP);
             Result := nil;
             Exit;
        end;

        AssignFile(myFile, rutaArchivo);
        Reset(myFile);

        Result:= TStringList.Create;
        i:=0;
        errores := '';
        while not Eof(myFile) do
        begin
            ReadLn(myFile, texto);

            if (i=0) and (texto <> 'NumeroConvenio;Novedades') then
            begin
                MsgBox('La primera linea del archivo debe ser NumeroConvenio;Novedades');
                Break;
            end
            else if (i>0) then
            begin
                campos := Split(';', texto);

                if campos.Count = 2 then
                begin
                    result.AddObject(campos[0], TObject(campos[1]));
                end
                else
                begin
                    if errores <> '' then
                        errores := errores + CR;
                    errores := errores + texto;
                end;
            end;

            i:= i + 1;

        end;

        if errores <> '' then
            MsgBox('Las siguientes lineas no cumplen el formato: ' +  CR + errores);

    finally
        CloseFile(myFile);
    end;

end;

procedure TfrmNovedadesConvenios.CargarGrid(Datos: TStringList);
var
    i: Integer;
begin
    cdsNovedades.Open;
    cdsNovedades.EmptyDataSet;
    for i := 0 to Datos.Count - 1 do
    begin
        cdsNovedades.InsertRecord([Datos[i], string(Datos.Objects[i])]);
    end;  
end;

procedure TfrmNovedadesConvenios.btnAplicarClick(Sender: TObject);
var
    SP : TADOStoredProc;
    I, RuturnValue: Integer;
    novedades, ErrorDescription: string;
begin

    if cdsNovedades.RecordCount>0 then
    begin
        novedades := '';
        cdsNovedades.First;
        for I := 0 to cdsNovedades.RecordCount - 1 do
        begin
            if novedades <> '' then
                novedades := novedades + '|';

            novedades := novedades + cdsNovedades.FieldByName('fldNumeroConvenio').Value +
                        ';' + cdsNovedades.FieldByName('fldTextoNovedad').Value;

            cdsNovedades.Next;
        end;

        if not Assigned(SP) then
        begin
            SP:= TADOStoredProc.Create(nil);
            SP.Connection := DMConnections.BaseCAC;
            SP.ProcedureName := 'INSERT_NovedadesConvenio';
        end;

        SP.Parameters.Refresh;
        SP.Parameters.ParamByName('@Novedades').Value := novedades;
        SP.Parameters.ParamByName('@NombreArchivo').Value := ExtractFileName(txt_Ruta.Text);
        SP.Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
        SP.ExecProc;

        if SP.Parameters.ParamByName('@Return_Value').Value = 0 then
        begin
            MsgBox('Novedades registradas correctamente');
              btnAplicar.Enabled:= false;
        end
        else
            MsgBox('Error registrando novedades');
    end;

end;



procedure TfrmNovedadesConvenios.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TfrmNovedadesConvenios.btnFormatoClick(Sender: TObject);
var
    SD : TSaveDialog;
    miArchivo: TextFile;
begin
    SD:= nil;
    try

        SD:= TSaveDialog.Create(nil);
        SD.Filter := 'CSV files only|*.csv';
        SD.Title := 'Guardar Patron Ejemplo';
        SD.Execute();

        if SD.Files.Count > 0 then
        begin
            if DirectoryExists(ExtractFileDir(SD.Files[0])) and (Length(ExtractFileName(SD.Files[0])) > 2) then
            begin
                if ExtractFileExt(SD.Files[0])<> '.csv' then
                    SD.Files[0] :=SD.Files[0] + '.csv';

                if FileExists(SD.Files[0]) then
                begin        
                    if MessageDlg('¿Sobreescribir ' + SD.Files[0] + '?', mtWarning, [mbYes, mbNo], 0) = mrNo then
                        Exit;
                end;

                AssignFile(miArchivo, SD.Files[0]);

                Rewrite(miArchivo);


                WriteLn(miArchivo, 'NumeroConvenio;Novedades');
                WriteLn(miArchivo, '00000001;Novedad 01');
                WriteLn(miArchivo, '00000002;Novedad 02');
                WriteLn(miArchivo, '00000003;Novedad 03');
                WriteLn(miArchivo, '00000004;Novedad 04');
                WriteLn(miArchivo, 'Cambiar los ejemplos por los datos y eliminar esta linea');

            end;
        end;

    finally
        if TTextRec(miArchivo).Handle <> 0 then
            CloseFile(miArchivo);
        SD.Free;
    end;
end;

end.
