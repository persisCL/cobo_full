unit VentaDayPass;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, Grids, DBGrids, RStrings,
  VariantComboBox, MaskCombo, DmiCtrls, UtilProc, PeaProcs, UtilDB, DB,
  ADODB, DBClient, Validate, DateEdit, Peatypes, Util, ListBoxEx, DBListEx, Varios, FrmImprimirDayPass;

type
  TFormVentaDayPass = class(TForm)
    GBDatosPase: TGroupBox;
    GBPrincipal: TGroupBox;
    PanelAbajo: TPanel;
    Label2: TLabel;
    cb_TipoVehiculo: TVariantComboBox;
    Label13: TLabel;
    Label38: TLabel;
    GBPasesSeleccionados: TGroupBox;
    Panel1: TPanel;
    Label4: TLabel;
    Label1: TLabel;
    LblCategoria: TLabel;
    LblCantidadPases: TLabel;
    lblMontoTotal: TLabel;
    Label5: TLabel;
    lblCantidadPasesVenta: TLabel;
    qry_PasesDisponibles: TADOQuery;
    DSPasesDisponibles: TDataSource;
    TablaPases: TClientDataSet;
    DSPases: TDataSource;
    BtnAceptar: TDPSButton;
    BtnCancelar: TDPSButton;
    BtnSalir: TDPSButton;
    Panel3: TPanel;
    BtnModificar: TDPSButton;
    btnEliminar: TDPSButton;
    Panel4: TPanel;
    InsertarMaestroDayPass: TADOStoredProc;
    GrillaPases: TDBListEx;
    txtPatente: TEdit;
    cbConAcoplado: TCheckBox;
    btnSeleccionar: TDPSButton;
    ObtenerDayPassPatente: TADOStoredProc;
    spActualizarMaestroDayPass: TADOStoredProc;
    Panel2: TPanel;
    Label3: TLabel;
    procedure BtnCancelarClick(Sender: TObject);
    procedure cb_TipoVehiculoChange(Sender: TObject);
    procedure BtnSalirClick(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure btnSeleccionarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnModificarClick(Sender: TObject);
    procedure btnEliminarClick(Sender: TObject);
    procedure GrillaPasesPosiblesDblClick(Sender: TObject);
    procedure GrillaPasesDblClick(Sender: TObject);
    procedure txtPatenteChange(Sender: TObject);
    procedure cbConAcopladoClick(Sender: TObject);
    procedure txtPatenteExit(Sender: TObject);
    procedure GrillaPasesDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: String; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
  private
    function GetCodigoTipo: integer;
  private
    { Private declarations }
    FNumeroTurno: integer;
    Categoria,
    CantidadDayPassVenta, CantidadDayPassPatente, CantidadDayPassPorAnio: integer;
    MontoTotal: integer;
    property CodigoTipo: integer read GetCodigoTipo;
//    function ValidarCantidadPases: boolean;
    procedure Consultar;
    function PuedeGrabar: boolean;
    function GrabarVenta: boolean;
    procedure GrabarMaestroDayPass (NumeroComprobante: integer;
                                    TipoComprobante: AnsiString);
    procedure ActualizarMaestroDayPass (NumeroDayPass: Longint; FechaDesde: TDateTime; Patente: string);
    procedure LimpiarPantalla;
    procedure LimpiarDatosVenta;
    function PuedeSeleccionarItem(Alta: boolean = True): boolean;
    procedure SeleccionarItem(EsSeleccion: boolean); //Seleccion o Modificacion
    function DameFechaDesdeProxima(Tabla: TClientDataSet): tdatetime;
    procedure OcultarColumnasGrilla;
    function BuscarPatenteCuenta(Patente : String): boolean;

  public
    { Public declarations }
    Property NumeroTurno: integer Read FNumeroTurno write FNumeroTurno;
    function Inicializa(MDIChild: Boolean): boolean;
  end;

resourcestring
    SIN_ESPECIFICAR = '<sin especificar>';
    MSG_CANTIDAD_SUPERADA = 'La patente indicada no puede adquirir mas pases ' +
                            'diarios porque supera la cantidad establecida por a�o.';
    MSG_PATENTE_EXISTE_CONVENIO = 'La patente ingresada se encuentra registrada como una cuenta. ' + #13#10 +
                                  'No puede adquirir Pases Diarios.';
    MSG_INGRESAR_PATENTE = 'Debe ingresar una patente.';
var
  FormVentaDayPass: TFormVentaDayPass;

implementation

uses DMConnection, VentaDayPassFecha, FrmFormaPago;

{$R *.dfm}

{ TFormVentaDayPass }

function TFormVentaDayPass.Inicializa(MDIChild: Boolean): boolean;
resourcestring
    CAPTION_GRILLA_PASES_POSIBLES = '<Doble click para seleccionar el pase>';
    CAPTION_GRILLA_PASES_ELEGIDOS = '<Doble click para modificar el pase>';
var
    s : TSize;
begin

    if MDIChild then begin
        S := GetFormClientSize(Application.MainForm);
        SetBounds(0, 0, S.cx, S.cy);
    end else begin
        FormStyle := fsNormal;
        Visible := False;
    end;

    CargarVehiculosTipos(DMConnections.BaseCAC, cb_TipoVehiculo, False, 1);
    cbConAcoplado.Checked := false;
    cb_TipoVehiculoChange(nil);
    TablaPases.CreateDataSet;
    OcultarColumnasGrilla;
    MontoTotal := 0;
    CantidadDayPassVenta   := 0;
    CantidadDayPassPatente := 0;
    CantidadDayPassPorAnio := QueryGetValueInt(DMConnections.BaseCAC,
                              ' SELECT IsNull(CantidadDayPassPorAnio,0) FROM Parametros ' +
                              ' WHERE ID = (SELECT MAX(ID) FROM PARAMETROS) ');
    txtPatente.SetFocus;
    result := True;

    if GNumeroTurno < 1 then begin
         MsgBox(MSG_ERRORABRIRTURNO, 'Error',MB_ICONSTOP);
         result := False;
    end;


end;

procedure TFormVentaDayPass.BtnCancelarClick(Sender: TObject);
begin
    if (cb_TipoVehiculo.ItemIndex > 0) or (txtPatente.Text <> '') then begin
        LimpiarPantalla;
    end;
end;

procedure TFormVentaDayPass.cb_TipoVehiculoChange(Sender: TObject);
begin
    if (cb_TipoVehiculo.ItemIndex = -1) or (cb_TipoVehiculo.ItemIndex = 0) then begin
        LblCategoria.Caption := SIN_ESPECIFICAR;
        Categoria := 0;
    end
    else begin
        cbConAcoplado.Enabled:=ObtenerCategoria(DMConnections.BaseCAC,CodigoTipo,false)<>ObtenerCategoria(DMConnections.BaseCAC,CodigoTipo,true);
        cbConAcoplado.Checked:=False;

        Categoria := ObtenerCategoria(DMConnections.BaseCAC,self.CodigoTipo, cbConAcoplado.Checked);
        LblCategoria.Caption := QueryGetValue(DMConnections.BaseCAC,
                              ' SELECT Descripcion from Categorias  ' +
                              '  WHERE Categoria = '+ IntToStr(categoria));
    end;
    LimpiarDatosVenta;
    txtPatenteExit(nil);
end;

procedure TFormVentaDayPass.Consultar;
begin
    //Traigo la lista de pases disponibles
    qry_PasesDisponibles.Close;
    qry_PasesDisponibles.Parameters.ParamByName('xCategoria').Value := Categoria;
    qry_PasesDisponibles.Open;
    qry_PasesDisponibles.First;
end;

(*
function TFormVentaDayPass.ValidarCantidadPases: boolean;
resourcestring
    MSG_PATENTE_FORMATO = 'El formato de la patente ingresada no es v�lido.';
var
    AnioActual: Smallint;
begin
    result := false;

    //Verifico si la patente adquiri� alg�n DayPass anteriormente
    //(y compruebo con el l�mite CantidadDayPassPorAnio definido en Parametros)
    //CantidadDayPassPorAnio
    AnioActual := QueryGetValueInt(DMConnections.BaseCAC, ' SELECT YEAR(GETDATE()) AnioActual ');

    CantidadDayPassPatente := ObtenerCantidadPasesPatenteAnioActual(DMConnections.BaseCAC,
                                txtPatente.Text, AnioActual);

    if (CantidadDayPassPatente >= CantidadDayPassPorAnio) then begin
        MsgBox(MSG_CANTIDAD_SUPERADA,Caption,MB_ICONSTOP);
        exit;
    end;
    LblCantidadPases.Caption := IntToStr(CantidadDayPassPatente);
    result := true;
end;
*)

procedure TFormVentaDayPass.BtnSalirClick(Sender: TObject);
begin
    self.Close;
end;

procedure TFormVentaDayPass.BtnAceptarClick(Sender: TObject);
begin
    if PuedeGrabar then begin
        if GrabarVenta then LimpiarPantalla;
    end;
end;

function TFormVentaDayPass.GrabarVenta: boolean;
resourcestring
    ERROR_TRANSACCION = 'No se pudo terminar la transacci�n';
    CAPTION = 'Error al grabar la venta';
    DATOS_OK = 'La operaci�n se realiz� con �xito.';
    NRO_FACTURA = 'N�mero de comprobante: ';
    MSG_PAGO_REGISTRADO = 'El pago se ha registrado correctamente.';
    CAPTION_PAGAR_COMPROBANTE = 'Pagar Comprobante';
var
    NumeroComprobante: integer; //**VER ESTE TIPO DE DATO
	f : TFormFormaPago;
    ok: boolean;
    fReporte: TFormImprimirDayPass;
    NumerosDayPass: TArrayLong;
begin
    ok := false;
    NumeroComprobante := -1;
    if MontoTotal > 0 then begin
        //1- Grabamos la factura
        Application.CreateForm(TFormFormaPago, f);
        f.TipoComprobante	:= TC_TICKET;
        f.NumeroComprobante	:= -1;
        f.CodigoPersona     := -1;
        f.DescriComprobante	:= QueryGetValue(DMConnections.BaseCAC,
                               Format('SELECT dbo.ObtenerDescripcionTipoComprobante(''%s'')',
                               [TC_TICKET]));
        f.MontoTotal		:= MontoTotal;
        //f.NumeroTurno 		:= fnumeroturno;
        f.Titular			:= '';
        if (f.inicializar()) and (f.ShowModal = mrOk) then begin
            NumeroComprobante := f.NumeroComprobante;
            ok := true;
            f.Release;
        end
        else begin
            f.Release;
            result := false;
            Exit;
        end;

    end;

    if ((ok) or (strtoint(LblCantidadPases.Caption) > 0)) then begin
        try
            ok := false;
            DMConnections.BaseCAC.BeginTrans;
            TablaPases.First;
            while not TablaPases.Eof do
            begin
                if not TablaPases.FieldByName('Vendido').AsBoolean then
                    GrabarMaestroDayPass(NumeroComprobante, TC_TICKET)
                else begin
                    if TablaPases.FieldByName('Desde').AsDateTime <> TablaPases.FieldByName('DesdeAnterior').AsDateTime then begin
                        ActualizarMaestroDayPass(TablaPases.FieldByName('NumeroDayPass').AsInteger, TablaPases.FieldByName('Desde').AsDateTime,
                        txtPatente.Text);
                        SetLength(NumerosDayPass, Length(NumerosDayPass) + 1);
                        NumerosDayPass[Length(NumerosDayPass)-1] := TablaPases.FieldByName('NumeroDayPass').AsInteger;
                    end;
                end;
                TablaPases.Next;
            end;
            DMConnections.BaseCAC.CommitTrans;
            ok := True;
        except
            on E: exception do begin
                MsgBoxErr(ERROR_TRANSACCION, E.Message, CAPTION, MB_ICONSTOP);
                DMConnections.BaseCAC.RollbackTrans;
            end;
        end;
    end;

    if ((ok) and (strtoint(lblCantidadPasesVenta.Caption) > 0)) then begin
        Application.CreateForm(TFormImprimirDayPass, fReporte);
        fReporte.Execute(NumeroComprobante, nil, MSG_PASES_DIARIOS, True);
        fReporte.Release;
    end;

    if ((ok) and (Length(NumerosDayPass)  > 0)) then begin
        Application.CreateForm(TFormImprimirDayPass, fReporte);
        fReporte.Execute(-1, NumerosDayPass, MSG_PASES_DIARIOS_MODIF, True);
        fReporte.Release;
    end;

    result := ok and ((strtoint(lblCantidadPasesVenta.Caption) > 0) or (Length(NumerosDayPass)  > 0));
end;

procedure TFormVentaDayPass.LimpiarPantalla;
begin
    LblCategoria.Caption := SIN_ESPECIFICAR;
    Categoria := 0;
    txtPatente.Text := '';
    LblCantidadPases.Caption := '0';
    CargarVehiculosTipos(DMConnections.BaseCAC, cb_TipoVehiculo, False, 1);
    cbConAcoplado.Checked := false;
    cb_TipoVehiculoChange(nil);
    LimpiarDatosVenta;
    txtPatente.SetFocus;
end;

function TFormVentaDayPass.PuedeGrabar: boolean;
resourcestring
    NO_INGRESO_ITEMS    = 'Debe seleccionar al menos un pase.';
    FALTAN_DATOS        = 'Debe seleccionar el tipo de veh�culo y la patente';
    ERROR               = 'No puede completar la operaci�n';
var
    mensaje: AnsiString;
begin

    mensaje := '';
    mensaje := iif((cb_TipoVehiculo.ItemIndex < 0) or (Trim(txtPatente.Text) = ''),
                    FALTAN_DATOS,  '');

    if (mensaje = '') and (BuscarPatenteCuenta(Trim(txtPatente.Text))) then
        mensaje := MSG_PATENTE_EXISTE_CONVENIO;

    //*** FIXME Chequear si el TICKET requiere que se abra un TURNO.
    if mensaje = '' then begin
        mensaje := iif((TablaPases.RecordCount <= 0), NO_INGRESO_ITEMS, '');
        if (mensaje = '') and (GNumeroTurno < 1) then
            mensaje := MSG_ERRORABRIRTURNO;
    end;
    //Si hubo algun error lo muestro
    if mensaje <> '' then
        MsgBox(mensaje, 'Error',MB_ICONSTOP);
    result := mensaje = '';
end;

procedure TFormVentaDayPass.btnSeleccionarClick(Sender: TObject);
begin
    if PuedeSeleccionarItem then begin
        SeleccionarItem(true);
        LblCantidadPasesVenta.Caption := IntToStr(TablaPases.RecordCount - strtoint(LblCantidadPases.Caption));
        lblMontoTotal.Caption := formatFloat(FORMATO_IMPORTE, MontoTotal);
    end;
end;

function TFormVentaDayPass.PuedeSeleccionarItem(Alta: boolean): boolean;
resourcestring
    CAPTION = 'L�mite de pases por a�o';
var
    CantidadPasesVenta, AnioActual: smallint;
begin
    //Validar que no exceda la cantidad de Items, entre los ya comprados
    //y los comprados anteriormente
    AnioActual := QueryGetValueInt(DMConnections.BaseCAC, ' SELECT YEAR(GETDATE()) AnioActual ');
    if not TablaPases.Active then CantidadPasesVenta := 0
    else CantidadPasesVenta := TablaPases.RecordCount - strtoint(LblCantidadPases.Caption);

    CantidadDayPassPatente := ObtenerCantidadPasesPatenteAnioActual(DMConnections.BaseCAC,
                                txtPatente.Text, AnioActual);

    if ((Alta)and (CantidadPasesVenta + CantidadDayPassPatente >= CantidadDayPassPorAnio)) then begin
        MsgBox(MSG_CANTIDAD_SUPERADA, CAPTION, MB_ICONSTOP);
        result := false;
        exit;
    end;

    if (Trim(txtPatente.Text) = '') then begin
        MsgBoxBalloon(MSG_INGRESAR_PATENTE, self.caption, MB_ICONSTOP, txtPatente);
        result := false;
        exit;
    end;

    if BuscarPatenteCuenta(Trim(txtPatente.Text)) then begin
        MsgBoxBalloon(MSG_PATENTE_EXISTE_CONVENIO, self.caption, MB_ICONSTOP, txtPatente);
        result := false;
        Exit;
    end;

    result := true;
end;

procedure TFormVentaDayPass.SeleccionarItem(EsSeleccion: boolean);
begin
    //En la modificacion toma la fecha sobre la que hizo click
    //pero en el ingreso (seleccion, habr�a que tomar la ultima fecha,
    //sumarle una y que esa sea la fecha desde

    //EsSeleccion indica si es Alta, sino es una modificacion
    if EsSeleccion then begin
        self.Consultar;
        if qry_PasesDisponibles.IsEmpty then Exit;
        TFormVentaDayPassFecha.IngresarFechaPase(qry_PasesDisponibles.FieldByName('Codigo').AsInteger,
            qry_PasesDisponibles.FieldByName('Duracion').AsInteger,
            qry_PasesDisponibles.FieldByName('Importe').AsInteger,
            qry_PasesDisponibles.FieldByName('Descripcion').AsString,
            Trim(txtPatente.Text),
            DameFechaDesdeProxima(TablaPases), EsSeleccion, TablaPases);
    end
    else begin
        TFormVentaDayPassFecha.IngresarFechaPase(TablaPases.fieldvalues['Codigo'],
            TablaPases.fieldvalues['Duracion'],
            TablaPases.fieldvalues['Importe'],
            TablaPases.fieldvalues['Descripcion'],
            Trim(txtPatente.Text),
            TablaPases.fieldvalues['Desde'], EsSeleccion, TablaPases);
    end;
    //En una variable ModalResultAux guardo el resultado de la pantalla.
    if ModalResultAux = mrCancel then exit;

        with TablaPases do
        begin
            if not TablaPases.Active then begin
                TablaPases.Open;
            end;

        if EsSeleccion then begin
            Append;
            FieldByName('Codigo').AsInteger := qry_PasesDisponibles.FieldByName('Codigo').AsInteger;
            FieldByName('Descripcion').AsString := qry_PasesDisponibles.FieldByName('Descripcion').AsString;
            FieldByName('Importe').AsInteger := qry_PasesDisponibles.FieldByName('Importe').AsInteger;
            FieldByName('Desde').AsDateTime := FechaFinalDesde;
            FieldByName('Hasta').AsDateTime := FechaFinalHasta;
            FieldByName('FechaVencimiento').AsDateTime := qry_PasesDisponibles.FieldByName('FechaHasta').AsDateTime;
            FieldByName('DiasVencimientoActivacion').AsDateTime := now +
                        qry_PasesDisponibles.FieldByName('DiasVencimientoActivacion').AsInteger;
            FieldByName('Duracion').AsInteger := qry_PasesDisponibles.FieldByName('Duracion').AsInteger;
            FieldByName('EnEdicion').AsBoolean := false;
            FieldByName('DescImporte').AsString := qry_PasesDisponibles.FieldByName('DescImporte').AsString;
            FieldByName('Categoria').AsInteger := qry_PasesDisponibles.FieldByName('CodigoCategoria').AsInteger;
//            FieldByName('FechaHoraCompra').AsDateTime := null;
            FieldByName('Vendido').AsBoolean := False;
            Post;
            MontoTotal := MontoTotal + qry_PasesDisponibles.FieldByName('Importe').AsInteger;
        end
        else begin
            Edit;
            FieldByName('Desde').AsDateTime := FechaFinalDesde;
            FieldByName('Hasta').AsDateTime := FechaFinalHasta;
            Post;
        end;

    end;
end;

procedure TFormVentaDayPass.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

function TFormVentaDayPass.DameFechaDesdeProxima(Tabla: TClientDataSet): tdatetime;
var
    fmax: tdatetime;
begin
    //Tiene que ser mayor a hoy, asi que con eso va bien...
    fmax := date -1;
    //si no esta activa o esta vacia, tomo la fecha de ma�ana
    if (not Tabla.Active) or
        (Tabla.IsEmpty) then
        result := date
    else begin
        Tabla.First;
        while not Tabla.Eof do
        begin
            if TablaPases.FieldValues['Hasta'] > fmax then
                fmax := TablaPases.FieldValues['Hasta'];
            Tabla.Next;
        end;
        result := fmax + 1;
    end;
end;

procedure TFormVentaDayPass.BtnModificarClick(Sender: TObject);
begin
    if not (TablaPases.FieldByName('Desde').AsDateTime > NowBase(DMConnections.BaseCAC)) then Exit;

    if (not TablaPases.Active) or (TablaPases.IsEmpty) then exit;
    if PuedeSeleccionarItem(False) then begin
        SeleccionarItem(false);
    end;
end;

procedure TFormVentaDayPass.btnEliminarClick(Sender: TObject);
begin
    if TablaPases.FieldByName('Vendido').AsBoolean then Exit;

    if (not TablaPases.Active) or (TablaPases.IsEmpty) then exit;
    MontoTotal := MontoTotal - TablaPases.fieldvalues['Importe'];
    TablaPases.Delete;
    LblCantidadPasesVenta.Caption := IntToStr(TablaPases.RecordCount - strtoint(LblCantidadPases.Caption));
    lblMontoTotal.Caption := formatFloat(FORMATO_IMPORTE, MontoTotal);
end;

procedure TFormVentaDayPass.GrabarMaestroDayPass(NumeroComprobante: integer;
                                    TipoComprobante: AnsiString);
begin
    (*Graba el item actual de la Tabla Pases en la tabla de MaestroDayPass*)
    with InsertarMaestroDayPass.Parameters do
    begin
        InsertarMaestroDayPass.Parameters.Refresh;
        ParamByName('@Estado').Value := ESTADO_ACTIVO_DAY_PASS;
        ParamByName('@CodigoTipoDayPass').value := TablaPases.FieldValues['Codigo'];  // tinyint,
        ParamByName('@FechaDesde').Value := TablaPases.FieldValues['Desde'];
        ParamByName('@FechaHasta').Value := TablaPases.FieldValues['Hasta'];
        ParamByName('@FechaVencimiento').Value := TablaPases.FieldValues['FechaVencimiento'];
        ParamByName('@FechaHoraCompra').Value := now;
        ParamByName('@Patente').Value := Trim(txtPatente.Text);
        ParamByName('@Importe').Value := TablaPases.FieldValues['Importe'];
        ParamByName('@DiasVencimientoActivacion').Value :=  TablaPases.FieldValues['DiasVencimientoActivacion'];
        ParamByName('@Categoria').Value := categoria;
        ParamByName('@TipoCompromante').Value := TipoComprobante;
        ParamByName('@NumeroComprobante').Value := NumeroComprobante;
        ParamByName('@Usuario').Value := UsuarioSistema;
        InsertarMaestroDayPass.ExecProc;
    end;
end;

procedure TFormVentaDayPass.OcultarColumnasGrilla;
resourcestring
    CODIGO      = 'C�digo';
    DESCRIPCION = 'Descripci�n';
    IMPORTE     = 'Importe';
begin
    {
    GrillaPases.Columns[0].Title.Caption := CODIGO;
    GrillaPases.Columns[1].Title.Caption := DESCRIPCION;
    GrillaPases.Columns[1].Width := 300;
    GrillaPases.Columns[2].Title.Caption := IMPORTE;
    GrillaPases.Columns[2].Alignment := taRightJustify;
    GrillaPases.Columns[2].Width := 85;
    GrillaPases.Columns[3].Width := 75;
    GrillaPases.Columns[4].Width := 75;
    GrillaPases.Columns[5].Visible := false;
    GrillaPases.Columns[6].Visible := false;
    GrillaPases.Columns[7].Visible := false;
    GrillaPases.Columns[8].Visible := false;
    GrillaPases.Columns[9].Visible := false;
    }
    GrillaPases.Columns[0].Header.Caption := CODIGO;
    GrillaPases.Columns[0].Header.Font.color := clWindowText;
    //GrillaPases.Columns[0].Width := 75;
    GrillaPases.Columns[1].Header.Caption := DESCRIPCION;
    //GrillaPases.Columns[1].Width := 300;
    GrillaPases.Columns[2].Header.Caption := IMPORTE;
    GrillaPases.Columns[2].Alignment := taRightJustify;
    //GrillaPases.Columns[2].Width := 85;
    //GrillaPases.Columns[3].Width := 75;
    //GrillaPases.Columns[4].Width := 75;
    //GrillaPases.Columns[5].Width := 0;// GrillaPases.Columns[5].Visible := false;
    GrillaPases.Columns[6].Width := 0;// GrillaPases.Columns[6].Visible := false;
    GrillaPases.Columns[7].Width := 0;// GrillaPases.Columns[7].Visible := false;
    GrillaPases.Columns[8].Width := 0;// GrillaPases.Columns[8].Visible := false;
    GrillaPases.Columns[9].Width := 0;// GrillaPases.Columns[9].Visible := false;
    GrillaPases.Columns[10].Width := 0;// GrillaPases.Columns[9].Visible := false;
end;

procedure TFormVentaDayPass.LimpiarDatosVenta;
begin
    qry_PasesDisponibles.Close;
    if TablaPases.Active then TablaPases.EmptyDataSet;
    MontoTotal := 0;
    lblCantidadPasesVenta.caption := '0';
    lblMontoTotal.Caption := '0';
    LblCantidadPases.Caption := '0';
end;

procedure TFormVentaDayPass.GrillaPasesPosiblesDblClick(Sender: TObject);
begin
    btnSeleccionarClick(sender);
end;

procedure TFormVentaDayPass.GrillaPasesDblClick(Sender: TObject);
begin
    BtnModificarClick(sender);
end;

procedure TFormVentaDayPass.txtPatenteChange(Sender: TObject);
begin
    LimpiarDatosVenta;
    if Length(trim(txtPatente.Text)) >= 3 then
        txtPatenteExit(nil);
end;

function TFormVentaDayPass.GetCodigoTipo: integer;
begin
    Result := Ival(cb_TipoVehiculo.Value);
end;

procedure TFormVentaDayPass.cbConAcopladoClick(Sender: TObject);
begin
    if (cb_TipoVehiculo.ItemIndex >= 0) then begin
        Categoria := ObtenerCategoria(DMConnections.BaseCAC,self.CodigoTipo, cbConAcoplado.Checked);
        LblCategoria.Caption := QueryGetValue(DMConnections.BaseCAC,
                              ' SELECT Descripcion from Categorias  ' +
                              '  WHERE Categoria = '+ IntToStr(categoria));
    end;
    LimpiarDatosVenta;
    txtPatenteExit(nil);
end;

procedure TFormVentaDayPass.txtPatenteExit(Sender: TObject);
var
    AnioActual: Smallint;
begin
    if (Trim(txtPatente.Text) <> '') and (BuscarPatenteCuenta(Trim(txtPatente.Text))) then begin
        MsgBoxBalloon(MSG_PATENTE_EXISTE_CONVENIO, caption, MB_ICONSTOP, txtPatente);
        Exit;
    end;

    AnioActual := QueryGetValueInt(DMConnections.BaseCAC, ' SELECT YEAR(GETDATE()) AnioActual ');

    LblCantidadPases.Caption := inttostr(ObtenerCantidadPasesPatenteAnioActual(DMConnections.BaseCAC,
                                txtPatente.Text, AnioActual));


    if TablaPases.Active then begin
        TablaPases.First;
        while not TablaPases.Eof do begin
            if TablaPases.FieldByName('Vendido').AsBoolean then TablaPases.Delete
            else TablaPases.Next;
        end;
    end;

    ObtenerDayPassPatente.Close;
    ObtenerDayPassPatente.Parameters.ParamByName('@Patente').Value := Trim(txtPatente.Text);
    ObtenerDayPassPatente.Open;

    ObtenerDayPassPatente.First;
    while not ObtenerDayPassPatente.Eof do begin
        TablaPases.Append;
        TablaPases.FieldByName('Codigo').AsInteger := ObtenerDayPassPatente.FieldByName('CodigoTipoDayPass').AsInteger;
        TablaPases.FieldByName('Descripcion').AsString := ObtenerDayPassPatente.FieldByName('Descripcion').AsString;
        TablaPases.FieldByName('Importe').AsInteger := ObtenerDayPassPatente.FieldByName('Importe').AsInteger;
        TablaPases.FieldByName('Desde').AsDateTime := ObtenerDayPassPatente.FieldByName('FechaDesde').AsDateTime;
        TablaPases.FieldByName('DesdeAnterior').AsDateTime := ObtenerDayPassPatente.FieldByName('FechaDesde').AsDateTime;
        TablaPases.FieldByName('Hasta').AsDateTime := ObtenerDayPassPatente.FieldByName('FechaHasta').AsDateTime;
        TablaPases.FieldByName('FechaVencimiento').AsDateTime := ObtenerDayPassPatente.FieldByName('FechaVencimiento').AsDateTime;
        TablaPases.FieldByName('DiasVencimientoActivacion').AsDateTime := ObtenerDayPassPatente.FieldByName('DiasVencimientoActivacion').AsDateTime;
        TablaPases.FieldByName('Duracion').AsInteger := ObtenerDayPassPatente.FieldByName('Duracion').AsInteger;
        TablaPases.FieldByName('EnEdicion').AsBoolean := false;
        TablaPases.FieldByName('DescImporte').AsString := ObtenerDayPassPatente.FieldByName('DescImporte').AsString;
        TablaPases.FieldByName('Utilizado').AsBoolean := ObtenerDayPassPatente.FieldByName('Utilizado').AsBoolean;
        TablaPases.FieldByName('NumeroDayPass').AsInteger := ObtenerDayPassPatente.FieldByName('NumeroDayPass').AsInteger;
        TablaPases.FieldByName('Vendido').AsBoolean := True;
        TablaPases.FieldByName('Categoria').AsInteger := ObtenerDayPassPatente.FieldByName('Categoria').AsInteger;
        TablaPases.FieldByName('FechaHoraCompra').AsDateTime := ObtenerDayPassPatente.FieldByName('FechaHoraCompra').AsDateTime;
        TablaPases.Post;
        ObtenerDayPassPatente.Next;
    end;
    if TablaPases.Active then TablaPases.First;
end;

function TFormVentaDayPass.BuscarPatenteCuenta(Patente : String): boolean;
var
    TipoPatente: Variant;
begin
(*    if not CaracteresIngresadosSonDePatenteChilena(Trim(Patente)) then
        TipoPatente:= null
    else
      TipoPatente := PATENTE_CHILE;
*)
    TipoPatente := PATENTE_CHILE;

    if (QueryGetValue(DMConnections.BaseCAC, format('Exec ExisteCuenta ''%s'', ''%s''',
            [Trim(Patente), TipoPatente])) = '0') then
        result := False
    else
        result := True;
end;
procedure TFormVentaDayPass.GrillaPasesDrawText(Sender: TCustomDBListEx;
  Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
  var Text: String; var TxtRect: TRect; var ItemWidth: Integer;
  var DefaultDraw: Boolean);
begin
    if Column.FieldName = 'Utilizado' then begin
       if Trim(Text)= '1' then Text := MSG_SI
       else Text := MSG_NO;
    end;

    if (not TablaPases.FieldByName('Vendido').AsBoolean)  then begin
        if odSelected in State then
            Sender.Canvas.Font.Color := clWindow
        else
            Sender.Canvas.Font.Color := clWindowText;
    end else begin
        if odSelected in State then
            Sender.Canvas.Font.Color := clYellow
        else
            Sender.Canvas.Font.Color := clRed;
    end;
end;

procedure TFormVentaDayPass.ActualizarMaestroDayPass(
  NumeroDayPass: Integer; FechaDesde: TDateTime; Patente: string);
begin
    spActualizarMaestroDayPass.Close;
    spActualizarMaestroDayPass.Parameters.ParamByName('@FechaDesde').Value := FechaDesde;
    spActualizarMaestroDayPass.Parameters.ParamByName('@Patente').Value := Patente;
    spActualizarMaestroDayPass.Parameters.ParamByName('@NumeroDayPass').Value := NumeroDayPass;
    spActualizarMaestroDayPass.ExecProc;
end;

end.
