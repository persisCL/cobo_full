object FrmInterfazEntradaBancos: TFrmInterfazEntradaBancos
  Left = 234
  Top = 277
  BorderStyle = bsDialog
  Caption = 'Interfaz de Entrada de Bancos'
  ClientHeight = 140
  ClientWidth = 457
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 457
    Height = 105
    BevelOuter = bvLowered
    TabOrder = 0
    object Label4: TLabel
      Left = 10
      Top = 50
      Width = 102
      Height = 13
      Caption = '&Entidad Bancaria:'
      FocusControl = cbBancos
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 10
      Top = 18
      Width = 114
      Height = 13
      Caption = '&Archivo de Entrada:'
      FocusControl = peArchivo
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object cbBancos: TComboBox
      Left = 144
      Top = 47
      Width = 305
      Height = 19
      Style = csOwnerDrawFixed
      ItemHeight = 13
      TabOrder = 1
    end
    object ProgressBar: TProgressBar
      Left = 13
      Top = 80
      Width = 433
      Height = 16
      Min = 0
      Max = 100
      TabOrder = 2
    end
    object peArchivo: TPickEdit
      Left = 145
      Top = 14
      Width = 303
      Height = 21
      Enabled = True
      TabOrder = 0
      OnChange = peArchivoChange
      Decimals = 0
      EditorStyle = bteTextEdit
      OnButtonClick = peArchivoButtonClick
    end
  end
  object btnAceptar: TDPSButton
    Left = 297
    Top = 111
    Caption = '&Aceptar'
    Default = True
    ModalResult = 1
    TabOrder = 1
    OnClick = btnAceptarClick
  end
  object btnCancelar: TDPSButton
    Left = 377
    Top = 111
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 2
    OnClick = btnCancelarClick
  end
  object InterfazEntradaDebitoAutomatico: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'InterfazEntradaDebitoAutomatico'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 11
        Value = Null
      end
      item
        Name = '@Importe'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Value = Null
      end
      item
        Name = '@TipoDebito'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@CodigoEntidad'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CuentaDebito'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = Null
      end>
    Left = 72
    Top = 48
  end
  object OpenDialog: TOpenDialog
    DefaultExt = 'TXT'
    Filter = 'Archivo de texto|*.TXT'
    Left = 104
    Top = 48
  end
end
