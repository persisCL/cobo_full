unit FrmConfigReporteEstadoCaja;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, DB, ADODB,Util, UtilDb;

type
  TFormConfigReporteEstadoCaja = class(TForm)
    Label2: TLabel;
    Label1: TLabel;
    lbl_Usuario: TLabel;
    cb_Turnos: TComboBox;
    qry_TurnosUsuario: TADOQuery;
    btn_Aceptar: TDPSButton;
    btn_Salir: TDPSButton;
    procedure btn_AceptarClick(Sender: TObject);
    procedure btn_SalirClick(Sender: TObject);
  private
    { Private declarations }
    FNumeroTurno: integer;
  public
    { Public declarations }
    function Inicializa: Boolean;
    property NumeroTurno: integer read FNumeroTurno write FNumeroTurno ;
  end;

var
  FormConfigReporteEstadoCaja: TFormConfigReporteEstadoCaja;

implementation

{$R *.dfm}

{ TForm1 }


function TFormConfigReporteEstadoCaja.Inicializa: Boolean;
resourcestring
    MSG_APERTURA = 'Apertura: ';
    MSG_ABIERTO  = 'Abierto';
    MSG_CIERRE   = 'Cierre: ';
begin
	qry_TurnosUsuario.Parameters.ParamByName('Usuario').Value := UsuarioSistema;
	Result := Opentables([qry_TurnosUsuario]);
    While not qry_TurnosUsuario.Eof do begin
        cb_Turnos.Items.Add(PadR(MSG_APERTURA +  FormatDateTime('dd/mm/yyyy, hh:mm', qry_TurnosUsuario.FieldByName('FechaHoraApertura').AsDateTime), 40, ' ') +
		  PadR(iif(qry_TurnosUsuario.FieldByName('FechaHoraCierre').AsDateTime <=0, 'Abierto',
            MSG_CIERRE + FormatDateTime('dd/mm/yyyy, hh:mm', qry_TurnosUsuario.FieldByName('FechaHoraCierre').AsDateTime)), 40, ' ') + ' ' +
          PadL(Istr(qry_TurnosUsuario.FieldByName('NumeroTurno').AsInteger, 10), 200, ' ')
        );
		qry_TurnosUsuario.Next;
    end;
    cb_Turnos.ItemIndex := iif(qry_TurnosUsuario.RecordCount > 0, 0, -1);
    qry_TurnosUsuario.Close;
    lbl_Usuario.Caption := UsuarioSistema;
end;

procedure TFormConfigReporteEstadoCaja.btn_AceptarClick(Sender: TObject);
begin
	FNumeroTurno	:= Ival(StrRight(cb_Turnos.Text,10));
	ModalResult 	:= mrOk;
end;

procedure TFormConfigReporteEstadoCaja.btn_SalirClick(Sender: TObject);
begin
	Close;
end;

end.
