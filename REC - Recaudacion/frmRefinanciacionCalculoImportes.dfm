object RefinanciacionCalculoImportesForm: TRefinanciacionCalculoImportesForm
  Left = 0
  Top = 0
  ActiveControl = nedtImporteCuota
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = '  C'#225'lculo Importes Cuotas  '
  ClientHeight = 288
  ClientWidth = 389
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnKeyDown = FormKeyDown
  DesignSize = (
    389
    288)
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 248
    Width = 389
    Height = 40
    Align = alBottom
    Color = 14408667
    ParentBackground = False
    TabOrder = 0
    DesignSize = (
      389
      40)
    object btnCancelar: TButton
      Left = 306
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Cancelar'
      TabOrder = 0
      OnClick = btnCancelarClick
    end
    object btnAceptar: TButton
      Left = 227
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Aceptar'
      TabOrder = 1
      OnClick = btnAceptarClick
    end
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 373
    Height = 234
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = '  Detalle Importes Sugeridos  '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    DesignSize = (
      373
      234)
    object DBListEx1: TDBListEx
      Left = 6
      Top = 18
      Width = 360
      Height = 157
      Anchors = [akLeft, akTop, akRight, akBottom]
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          Header.Caption = 'Forma de Pago'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          IsLink = False
          FieldName = 'DescripcionFormaPago'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 80
          Header.Caption = 'Fecha'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'FechaCuota'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 100
          Header.Caption = 'Importe'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          Header.Alignment = taRightJustify
          IsLink = False
          FieldName = 'Importe'
        end>
      DataSource = dsCuotasCalculo
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
      OnClick = DBListEx1Click
      OnDrawText = DBListEx1DrawText
    end
    object GroupBox2: TGroupBox
      Left = 7
      Top = 179
      Width = 359
      Height = 47
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = '  Cambiar Importe  '
      TabOrder = 1
      object Label1: TLabel
        Left = 24
        Top = 19
        Width = 100
        Height = 13
        Caption = 'Introduzca Importe :'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object nedtImporteCuota: TNumericEdit
        Left = 130
        Top = 16
        Width = 103
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnKeyPress = nedtImporteCuotaKeyPress
      end
    end
  end
  object cdsCuotasCalculo: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    AfterScroll = cdsCuotasCalculoAfterScroll
    Left = 312
    Top = 176
  end
  object dsCuotasCalculo: TDataSource
    DataSet = cdsCuotasCalculo
    Left = 296
    Top = 192
  end
end
