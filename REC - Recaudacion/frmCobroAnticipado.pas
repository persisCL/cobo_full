{-----------------------------------------------------------------------------
 File Name: frmCobroComprobantes.pas
 Author:    Modificado por mlopez
 Date Created: 10/08/2005
 Language: ES-AR
 Description:

Revision 1:
Author : jjofre
Date Created: 09/03/2010
Description : SS 146 - Mensaje Convenio en carpeta legal Cobro Anticipado.
            Se agrega una alerta cuando el convenio elegido para el cobro
            anticipado se encuentra en proceso legal, alertando al operador
            de dicha situacion.

  Firma       : SS_660_CQU_20121010
  Description : Verifica si el convenio seleccionado se encuentra en lista amarilla

Firma       : SS_660_CQU_20130711
Descripcion : Se alinea el Label de Lista Amarilla.
              Se agrega el par�metro CodigoConcesionaria en la funci�n EstaConvenioEnListaAmarilla
              Se elimina el MsgBox() por un Label.

Firma       : SS_660_MVI_20130729
Descripcion : Se cambia texto y se agrega estado del convenio en lista amarilla

Firma       :  SS_1120_MVI_20130820
Descripcion :  Se cambia estilo del comboBox a: vcsOwnerDrawFixed,
               Se crear el evento DrawItem y se llama al procedimeinto ItemComboBoxColor.

Firma       :   SS_660_MVI_20130909
Descripcion :   Se modifica validaci�n para saber si se obtiene resultado del SP ObtenerEstadoConvenioEnListaAmarilla.
                Se acorta tama�o del lblEnListaAmarilla y se elimina texto inicial, ya que siempre es reemplazado.

Firma		:	SS_660_MCA_20140114
Decripcion	:	cuando el convenio esta en lista amarilla y este realiza un pago anticipado, la etiqueta que identifica al convenio en LA no se ocultaba.
            	por lo que se corrigio ese detalle

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

Firma       : SS_1408_MCA_20151027
Descripcion : muestra un mensaje si el cliente se encuentre en registrado en Tabla ClientesAContactar

-----------------------------------------------------------------------------}
unit frmCobroAnticipado;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ListBoxEx, DBListEx, StdCtrls, DPSControls, VariantComboBox,
  DmiCtrls, DB, ADODB, DMCOnnection, utilDB, utilProc, buscaClientes,
  PeaTypes, RStrings, DBCtrls, peaProcs, Util, PagoVentanillaAnticipado, ExtCtrls, ReporteRecibo,
  FrmImprimir, DBClient, ImgList,
  SysUtilsCN,   // SS_660_CQU_20121010
  PeaProcsCN;   //SS_660_MVI_20130909

const
	KEY_F9 	= VK_F9;
    
type
  TformCobroAnticipado = class(TForm)
  	gbPorCliente: TGroupBox;
    lb_CodigoCliente: TLabel;
    lbl_NumeroConvenio: TLabel;
    pe_NumeroDocumento: TPickEdit;
    cb_Convenios: TVariantComboBox;
    gb_DatosCliente: TGroupBox;
    Label6: TLabel;
    lblNombre: TLabel;
    lblDomicilio: TLabel;
	  Label10: TLabel;
    sp_ObtenerConvenio: TADOStoredProc;
    Label8: TLabel;
    lblRUT: TLabel;
	  Timer1: TTimer;
    cds_Comprobantes: TClientDataSet;
    ds_Comprobantes: TDataSource;
    Img_Tilde: TImageList;
    btn_Cobrar: TButton;
    btn_Salir: TButton;
    lblEnListaAmarilla: TLabel; // SS_660_CQU_20130711
    procedure pe_NumeroDocumentoChange(Sender: TObject);
	procedure cb_ConveniosChange(Sender: TObject);
	procedure FormCreate(Sender: TObject);
    procedure pe_NumeroDocumentoButtonClick(Sender: TObject);
	procedure btn_SalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btn_CobrarClick(Sender: TObject);
    procedure ed_NumeroComprobanteChange(Sender: TObject);
	procedure FormKeyDown(Sender: TObject; var Key: Word;
	  Shift: TShiftState);
	procedure Timer1Timer(Sender: TObject);
    procedure dl_ComprobantesDblClick(Sender: TObject);
    procedure dl_ComprobantesDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: String; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure dl_ComprobantesKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cb_ConveniosDrawItem(Control: TWinControl; Index: Integer;                //SS_1120_MVI_20130820
      Rect: TRect; State: TOwnerDrawState);                                             //SS_1120_MVI_20130820
  private
	{ Private declarations }
	FLogo: TBitmap;
	FUltimaBusqueda: TBusquedaCliente;
	FNumeroPOS, FPuntoEntrega, FPuntoVenta: Integer;
    FCodigoCliente: integer;
    FCantidadComprobantesCobrar: Integer;
    FCodigoCanalPago: Integer;
    FTotalComprobantesCobrar: Real;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    procedure MostrarDatosCliente(CodigoCLiente: Integer);
	procedure LimpiarCampos;
    procedure LimpiarConvenios;
    procedure LimpiarDetalles;
    procedure LimpiarDatosCliente;
    procedure LimpiarListaComprobantes;
	procedure Cobrar;
    function TienePagoAutomatico(TipoComprobante: AnsiString; NumeroComprobante: Integer;
        var TipoMedioPago: Integer): Boolean;
    function CobrarComprobanteConPagoAutomatico(TipoComprobante: AnsiString;
        NumeroComprobante, TipoMedioPago: Integer): Boolean;
  public
    { Public declarations }
    function Inicializar(NumeroPOS, PuntoEntrega, PuntoVenta: Integer): Boolean; overload;
	function Inicializar(TipoComprobante: AnsiString; NumeroComprobante: Int64;
        NumeroPOS, PuntoEntrega, PuntoVenta: Integer): Boolean; Overload;
  procedure MostrarMensajeTipoClienteSistema(NumeroDocumento: String);
  end;

var
  formCobroAnticipado: TformCobroAnticipado;

implementation

const
	SetPACPAT = [1, 2];

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    mlopez
  Date Created: 10/08/2005
  Description: Inicializa el Form - Carga los tipos de Comprobantes
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TformCobroAnticipado.Inicializar(NumeroPOS, PuntoEntrega, PuntoVenta: Integer): Boolean;
resourcestring
	MSG_LOGO_NO_ENCONTRADO  = 'No se ha encontrado el archivo con la imagen del logo para el recibo.';
	MSG_CAPTION             = 'Cobro de Comprobantes';
	MSG_ERROR_INICIALIZAR   = 'Error inicializando el formulario.';
begin
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

    try
        FTotalComprobantesCobrar := 0;
        FCantidadComprobantesCobrar := 0;
        FCodigoCliente := -1;
        FCodigoCanalPago := CONST_CANAL_PAGO_VENTANILLA;
        // Por ahora lo cargamos a mano
        LimpiarDatosCliente;

        FNumeroPOS := NumeroPOS;
        FPuntoEntrega := PuntoEntrega;
        FPuntoVenta := PuntoVenta;

        cds_Comprobantes.Close;
        cds_Comprobantes.CreateDataSet;
        cds_Comprobantes.Open;

        FormStyle := fsMDIChild;
        CenterForm(Self);

        FLogo := TBitmap.Create;
        (* Levanto el logo aqu� para que se haga una sola vez, independientemente de los reintentos
        de impresi�n que haga el operador. *)
        if not LevantarLogo (FLogo) then MsgBox(MSG_LOGO_NO_ENCONTRADO, MSG_CAPTION, MB_ICONINFORMATION + MB_OK);

        pe_NumeroDocumento.SetFocus;
        Result := True;
    except
	   	on e: exception do begin
			MsgBoxErr(MSG_ERROR_INICIALIZAR, E.Message, MSG_CAPTION, MB_ICONERROR);
			Result := False;
        end;
    end; // except
end;

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    mlopez
  Date Created: 10/08/2005
  Description: Inicializa el Form - Carga los tipos de Comprobantes y busca
				el comprobante seleccionado
  Parameters: TipoComprobante: AnsiString; NumeroComprobante: Int64
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TformCobroAnticipado.Inicializar(TipoComprobante: AnsiString;
    NumeroComprobante: Int64; NumeroPOS, PuntoEntrega, PuntoVenta: Integer): Boolean;
resourcestring
	MSG_LOGO_NO_ENCONTRADO = 'No se ha encontrado el archivo con la imagen del logo para el recibo.';
	MSG_CAPTION			   = 'Cobro de Comprobantes';
	MSG_ERROR_INICIALIZAR   = 'Error inicializando el formulario.';
begin
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

    try
		Result := True;
        FTotalComprobantesCobrar := 0;
        FCantidadComprobantesCobrar := 0;
        FCodigoCliente := -1;
        FCodigoCanalPago := CONST_CANAL_PAGO_VENTANILLA;

        // Por ahora lo cargamos a mano
        LimpiarDatosCliente;

        FNumeroPOS := NumeroPOS;
        FPuntoEntrega := PuntoEntrega;
        FPuntoVenta := PuntoVenta;

        cds_Comprobantes.Close;
        cds_Comprobantes.CreateDataSet;
        cds_Comprobantes.Open;

        FormStyle := fsMDIChild;
        CenterForm(Self);

		FLogo := TBitmap.Create;
        (* Levanto el logo aqu� para que se haga una sola vez, independientemente de los reintentos
        de impresi�n que haga el operador. *)
        if not LevantarLogo (FLogo) then MsgBox(MSG_LOGO_NO_ENCONTRADO, MSG_CAPTION, MB_ICONINFORMATION + MB_OK);
        pe_NumeroDocumento.SetFocus;
    except
	   	on e: exception do begin
			MsgBoxErr(MSG_ERROR_INICIALIZAR, E.Message, MSG_CAPTION, MB_ICONERROR);
			Result := False;
        end;
    end;
end;

procedure TformCobroAnticipado.FormCreate(Sender: TObject);
begin
    CenterForm(Self);
end;

{-----------------------------------------------------------------------------
  Function Name: peNumeroDocumentoChange
  Author:    mlopez
  Date Created: 20/12/2004
  Description: Actualiza los Convenios y Comprobantes Impagos de un cliente determinado
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformCobroAnticipado.pe_NumeroDocumentoChange(Sender: TObject);
resourcestring                                                                                        //SS_660_MVI_20130909
    MSG_ALERTA ='Validaci�n Cliente';                                                                 //SS_660_MVI_20130909
    MSG_ALERTA_CLIENTE_LISTA_AMARILLA='EL Cliente posee al menos un convenio en lista amarilla';      //SS_660_MVI_20130909
    SQL_CONSULTAPERSONA = 'SELECT dbo.EstaPersonaEnListaAmarilla(%d)';                                //SS_660_MVI_20130909
var                                                                                                   //SS_660_MVI_20130909
    PersonaEnListaAmarilla: Integer;                                                                  //SS_660_MVI_20130909
begin
    if ActiveControl <> Sender then Exit;
    LimpiarDetalles;
    LimpiarConvenios;
    LimpiarListaComprobantes;

    FCodigoCliente := QueryGetValueInt(DMConnections.BaseCAC, Format(
      'SELECT CodigoPersona FROM Personas  WITH (NOLOCK) WHERE CodigoDocumento = ''RUT'' ' +
      'AND NumeroDocumento = ''%s''', [iif(pe_NumeroDocumento.Text <> '',
      								   Trim(PadL(pe_NumeroDocumento.Text, 9, '0')), '')]));

	if FCodigoCliente > 0 then begin
		CargarConveniosRUT(DMConnections.BaseCAC, cb_Convenios, 'RUT', pe_NumeroDocumento.Text, False, 0, True);
        EstaClienteConMensajeEspecial(DMConnections.BaseCAC, pe_NumeroDocumento.Text);                              //SS_1408_MCA_20151027
    	MostrarDatosCliente(FCodigoCliente);

    PersonaEnListaAmarilla := QueryGetValueInt(DMConnections.BaseCAC, Format(SQL_CONSULTAPERSONA, [FCodigoCliente]));  //SS_660_MVI_20130909
    if PersonaEnListaAmarilla = 1 then begin                                                                             //SS_660_MVI_20130909
       ShowMsgBoxCN(MSG_ALERTA, MSG_ALERTA_CLIENTE_LISTA_AMARILLA, MB_ICONWARNING, Self);                                //SS_660_MVI_20130909
    end;                                                                                                                 //SS_660_MVI_20130909

    end;
end;

{-----------------------------------------------------------------------------
  Function Name: cb_ConveniosChange
  Author:    mlopez
  Date Created: 10/08/2005
  Description: Procesa un Cambio de Convenio - Trae los comprobantes impagos
				del dicho Convenio
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformCobroAnticipado.cb_ConveniosChange(Sender: TObject);
resourcestring
    //Rev 1
    MSG_CONVENIO_CARPETA_LEGAL_COBRO_ANTICIPADO = 'Atenci�n: Este Convenio est� en Cobranza Judicial.'
                        + CHR(13) + 'NO debe recibir el pago.' + CHR(13) +
                        'Desea continual igual ?';
    MSG_CONVENIO_CARPETA_LEGAL_EMPRESA_COBRO_ANTICIPADO = 'Atenci�n: Este Convenio est� en Cobranza Judicial.'
                        + CHR(13) + 'Este convenio fue asignado a la empresa: "%s."'
                        + CHR(13) + 'NO debe recibir el pago.' + CHR(13) +
                        'Desea continual igual ?';
    MSG_CONVENIOENLISTAAMARILLA = 'Convenio se encuentra en lista amarilla';                            //SS_660_CQU_20121010
    //SQL_ESTACONVENIOENLISTAAMARILLA = 'SELECT dbo.EstaConvenioEnListaAmarilla(%d,''%s'')';            //SS_660_CQU_20130711  //SS_660_CQU_20121010
    //SQL_ESTACONVENIOENLISTAAMARILLA = 'SELECT dbo.EstaConvenioEnListaAmarilla(%d,''%s'', NULL)';      //SS_660_MVI_20130729  //SS_660_CQU_20130711
      SQL_ESTACONVENIOENLISTAAMARILLA = 'SELECT dbo.ObtenerEstadoConvenioEnListaAmarilla(%d) ';         //SS_660_MVI_20130729
var                                                                                                     //SS_660_CQU_20121010
    CodigoConvenio:Integer;                                                                             //SS_660_CQU_20121010
    Empresa,Mensaje,EstadoListaAmarilla : string;                                                       //SS_660_MVI_20130729
begin
    if ActiveControl <> Sender then Exit;

    CodigoConvenio:=cb_Convenios.Value;                                                                 //SS_660_MVI_20130729   //SS_660_CQU_20121010
   // if  QueryGetValue(   DMConnections.BaseCAC,                                                       //SS_660_MVI_20130729     //SS_660_CQU_20121010
   //                     Format(SQL_ESTACONVENIOENLISTAAMARILLA,                                       //SS_660_MVI_20130729    //SS_660_CQU_20121010
   //                             [   CodigoConvenio,                                                   //SS_660_MVI_20130729     //SS_660_CQU_20121010
   //                                FormatDateTime('yyyymmdd hh:nn:ss',                                //SS_660_MVI_20130729    //SS_660_CQU_20121010
   //                                 QueryGetDateTimeValue(DMConnections.BaseCAC,'SELECT GETDATE()'))  //SS_660_MVI_20130729     //SS_660_CQU_20121010
   //                             ]                                                                     //SS_660_MVI_20130729     //SS_660_CQU_20121010
   //                            )                                                                      //SS_660_MVI_20130729     //SS_660_CQU_20121010
   //                  )='True' then                                                                    //SS_660_MVI_20130729     //SS_660_CQU_20121010
   // begin                                                                                             //SS_660_MVI_20130729     //SS_660_CQU_20121010
   //   //MsgBox(MSG_CONVENIOENLISTAAMARILLA, Self.Caption, MB_ICONINFORMATION);                        //SS_660_MVI_20130729     //SS_660_CQU_20130711  //SS_660_CQU_20121010
   //   //Exit;                                                                                         //SS_660_MVI_20130729     //SS_660_CQU_20130711  //SS_660_CQU_20121010
   //     lblEnListaAmarilla.Visible := True;                                                           //SS_660_MVI_20130729     //SS_660_CQU_20130711
   // //end;                                                                                            //SS_660_MVI_20130729     //SS_660_CQU_20130711  //SS_660_CQU_20121010
   // end else lblEnListaAmarilla.Visible := False;                                                     //SS_660_MVI_20130729     //SS_660_CQU_20130711

    EstadoListaAmarilla:= QueryGetValue(   DMConnections.BaseCAC,                                                                      //SS_660_MVI_20130729
                                           Format(SQL_ESTACONVENIOENLISTAAMARILLA,                                                     //SS_660_MVI_20130729
                                                   [   CodigoConvenio                                                                  //SS_660_MVI_20130729
                                                   ]                                                                                   //SS_660_MVI_20130729
                                                 )                                                                                     //SS_660_MVI_20130729
                                                 );                                                                                    //SS_660_MVI_20130729
                                                                                                                                       //SS_660_MVI_20130729
//    if EstadoListaAmarilla <> 'NULL' then begin                                                                                      //SS_660_MVI_20130909    //SS_660_MVI_20130729
    if EstadoListaAmarilla <> '' then begin                                                                                            //SS_660_MVI_20130909    //SS_660_MVI_20130729
    lblEnListaAmarilla.Visible  := True;                                                                                                 //SS_660_MVI_20130729
    lblEnListaAmarilla.Caption  := EstadoListaAmarilla;                                                                                  //SS_660_MVI_20130729
                                                                                                                                         //SS_660_MVI_20130729
    end                                                                                                                                  //SS_660_MVI_20130729
    else begin                                                                                                                           //SS_660_MVI_20130729
                                                                                                                                         //SS_660_MVI_20130729
    lblEnListaAmarilla.Visible  := False;                                                                                                //SS_660_MVI_20130729
                                                                                                                                         //SS_660_MVI_20130729
    end;

    if ChequearConvenioEnCarpetaLegal(cb_Convenios.Value) then begin
        Empresa := Trim(ObtenerEmpresaConvenioEnCarpetaLegal(cb_Convenios.Value));

        if Empresa = '' then Mensaje := MSG_CONVENIO_CARPETA_LEGAL_COBRO_ANTICIPADO
        else Mensaje := Format(MSG_CONVENIO_CARPETA_LEGAL_EMPRESA_COBRO_ANTICIPADO, [Empresa]);

        if MsgBox(Mensaje, self.Caption, MB_ICONWARNING or MB_YESNO) = IDYES then begin
            btn_Cobrar.Enabled := True
        end
        else btn_Cobrar.Enabled := False;

    end else begin
        btn_Cobrar.Enabled := (cb_Convenios.ItemIndex >= 1);
    end;
    //End Rev 1
end;

{-----------------------------------------------------------------------------
  Function Name: MostrarDatosCliente
  Author:    mlopez
  Date Created: 20/12/2004
  Description:  Muestra los datos del Cliente
  Parameters: CodigoCliente: Integer
  Return Value: None

  Revision: 1
  Author: mpiazza
  Date: 23/01/2009
  Description: SS 777: Mensaje segun tipo de cliente y sistema
-----------------------------------------------------------------------------}
procedure TformCobroAnticipado.MostrarDatosCliente(CodigoCliente: Integer);
begin
	// Cargamos la informaci�n del cliente.
    lblRUT.Caption := QueryGetValue(DMConnections.BaseCAC, Format(
      'SELECT dbo.ObtenerRUTPersona(%d)', [CodigoCliente]));
    lblNombre.Caption := QueryGetValue(DMConnections.BaseCAC, Format(
	  'SELECT dbo.ObtenerNombrePersona(%d)', [CodigoCliente]));
	lblDomicilio.Caption := QueryGetValue(DMConnections.BaseCAC, Format(
      'SELECT dbo.ObtenerDescripcionDomicilioPrincipalPersona(%d)', [CodigoCliente]));
    MostrarMensajeTipoClienteSistema(QueryGetValue(DMConnections.BaseCAC, Format(
      'SELECT dbo.ObtenerRUTPersona(%d)', [CodigoCliente])));//Revision 1
    lblEnListaAmarilla.Visible  := False;                                               //SS_660_MVI_20130729
end;

{-----------------------------------------------------------------------------
  Function Name: MostrarMensajeTipoClienteSistema
  Author: mpiazza
  Date Created: 23/01/2009
  Description:  SS-777 Trae mensaje segun RUT y sistema de acuerdo
  al tipo cliente
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformCobroAnticipado.MostrarMensajeTipoClienteSistema(
  NumeroDocumento: String);
var
    Texto: AnsiString;
begin
    if ( NumeroDocumento <> '' ) then begin
        Texto := MensajeTipoCliente(DMCOnnections.BaseCAC, NumeroDocumento, SistemaActual);
        if Texto <> '' then
            MsgBox(Texto, Self.Caption, MB_ICONWARNING);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: LimpiarDetalles;
  Author:    mlopez
  Date Created: 26/01/2005
  Description: Borra los detalles del Comprobante
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformCobroAnticipado.LimpiarDetalles;
begin
	btn_Cobrar.Enabled := False;
end;

{-----------------------------------------------------------------------------
  Function Name: pe_NumeroDocumentoButtonClick
  Author:    mlopez
  Date Created: 10/08/2005
  Description: Procesa un cambio en el RUT - Muestra los comprobantes pendientes
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformCobroAnticipado.pe_NumeroDocumentoButtonClick(Sender: TObject);
var
    f: TFormBuscaClientes;
begin
    Application.CreateForm(TFormBuscaClientes, F);
	if F.Inicializa(FUltimaBusqueda) then begin
		F.ShowModal;
        if F.ModalResult = idOk then begin
            FUltimaBusqueda := F.UltimaBusqueda;
            pe_NumeroDocumento.Text := f.Persona.NumeroDocumento;
		end;
	end;
    F.free;
end;

{-----------------------------------------------------------------------------
  Function Name: ed_NumeroComprobanteChange
  Author:    mlopez
  Date Created: 10/08/2005
  Description: Procesa un cambio de comprobante y muestra los datos del comprobante
  Parameters: Sender: TObject
  Return Value: None
  -----------------------------------------------------------------------------}
procedure TformCobroAnticipado.ed_NumeroComprobanteChange(Sender: TObject);
begin
    if ActiveControl <> Sender then Exit;
    pe_NumeroDocumento.Clear;
    FCodigoCliente := -1;
    LimpiarConvenios;
    LimpiarListaComprobantes;
	if Timer1.Enabled then begin
        Timer1.Enabled := False;
        Timer1.Enabled := True;
    end else Timer1.Enabled := True;
end;

{-----------------------------------------------------------------------------
  Function Name: btn_CobrarClick
  Author:    mlopez
  Date Created: 10/08/2005
  Description: Cobra un Comprobante
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformCobroAnticipado.btn_CobrarClick(Sender: TObject);
begin
    Cobrar;
end;

{-----------------------------------------------------------------------------
  Function Name: Cobrar
  Author:    mlopez
  Date Created: 10/08/2005
  Description: Cobra un Comprobante
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformCobroAnticipado.Cobrar;
resourcestring
	MSG_REGISTRO_CORRECTO = 'El pago se ha registrado correctamente.';
	MSG_CONFIRMAR_RECIBO = '�Desea Imprimir el Comprobante de Cancelaci�n?';
	MSG_CAPTION = 'Registrar Pago';
	MSG_IMPRESION_EXITOSA = '�La impresi�n se ha realizado con �xito?';
var
	r: TformRecibo;
	f: TfrmPagoVentanillaAnticipado;
	RespuestaImpresion: TRespuestaImprimir;
begin
    Application.CreateForm(TfrmPagoVentanillaAnticipado, f);
    if f.Inicializar(lblRUT.Caption, lblNombre.Caption, FNumeroPOS, FPuntoEntrega, FPuntoVenta,
                     FCodigoCanalPago, cb_Convenios.Value) then begin
        f.ShowModal;
        if f.ModalResult = mrOk then begin
            (* Almacenar el c�digo de canal de pago utilizado. *)
            FCodigoCanalPago := f.CodigoCanalPago;

            if f.ImprimirRecibo then begin
                (* Mostrar el di�logo previo a realizar la impresi�n. *)
                RespuestaImpresion := frmImprimir.Ejecutar(MSG_CAPTION,
                  MSG_REGISTRO_CORRECTO + CRLF + MSG_CONFIRMAR_RECIBO);
                case RespuestaImpresion of
                    (* Si la respuesta fue Aceptar, ejecutar el reporte. *)
                    riAceptarConfigurarImpresora, riAceptar: begin
                        Application.CreateForm(TformRecibo, r);

                        try
                            if r.Inicializar(FLogo, f.NumeroRecibo, RespuestaImpresion = riAceptarConfigurarImpresora) then begin
                                (* Preguntar si la impresi�n fue exitosa. *)
                                while MsgBox(MSG_IMPRESION_EXITOSA, MSG_CAPTION, MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = mrNo do begin
                                    (* Mostrar el di�logo de Configurar Impresora para permitir al operador
                                    reintentar la impresi�n. *)
                                    if not r.Inicializar(FLogo, f.NumeroRecibo, True) then Break;
                                end;
                            end;
                        finally
                            r.Release;
                        end
                    end;
                end;
                LimpiarDatosCliente;
            end;
            LimpiarCampos;
        end;
    end;
end;

procedure TformCobroAnticipado.btn_SalirClick(Sender: TObject);
begin
	Close;
end;

procedure TformCobroAnticipado.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	FLogo.Free;
	Action := caFree;
end;

{-----------------------------------------------------------------------------
  Procedure: TformPagoComprobantes.LimpiarCampos
  Author:    gcasais
  Date:      19-Ene-2005
  Arguments: None
  Result:    None
-----------------------------------------------------------------------------}
procedure TformCobroAnticipado.LimpiarCampos;
begin
	pe_NumeroDocumento.Clear;
    FCodigoCliente := -1;
	LimpiarConvenios;
    LimpiarListaComprobantes;
	LimpiarDetalles;
end;

procedure TformCobroAnticipado.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if (Key = KEY_F9) and btn_Cobrar.Enabled then btn_Cobrar.Click;
end;

procedure TformCobroAnticipado.Timer1Timer(Sender: TObject);
begin
    Timer1.Enabled := False;
end;

procedure TformCobroAnticipado.LimpiarConvenios;
begin
    cb_Convenios.Clear;
    cb_Convenios.Items.Add(SELECCIONAR, 0);
    cb_Convenios.ItemIndex := 0;
end;

procedure TformCobroAnticipado.dl_ComprobantesDblClick(Sender: TObject);
var
    TipoMedioPago: Integer;
begin
	if cds_Comprobantes.IsEmpty then Exit;

    (* Si no est� marcado para cobrar y
    TienePagoAutom�tico, entonces preguntar si lo desea cobrar. *)
    if (not cds_Comprobantes.FieldByName('Cobrar').AsBoolean)
            and TienePagoAutomatico(cds_Comprobantes.FieldByName('TipoComprobante').AsString,
                 cds_Comprobantes.FieldByName('NumeroComprobante').AsInteger, TipoMedioPago) then begin
        if (not CobrarComprobanteConPagoAutomatico(cds_Comprobantes.FieldByName('TipoComprobante').AsString,
                cds_Comprobantes.FieldByName('NumeroComprobante').AsInteger, TipoMedioPago)) then begin
            Exit;
        end;
    end;


	with cds_Comprobantes do begin
    	Edit;
        FieldByName('Cobrar').AsBoolean := not FieldByName('Cobrar').AsBoolean;
        Post;

        if FieldByName('Cobrar').AsBoolean then begin
            Inc(FCantidadComprobantesCobrar);

            (* Incrementar el monto total de comprobantes. *)
            FTotalComprobantesCobrar := FTotalComprobantesCobrar + FieldByName('TotalAPagar').AsFloat;
        end else begin
            Dec(FCantidadComprobantesCobrar);

            (* Decrementar el monto total de comprobantes. *)
            FTotalComprobantesCobrar := FTotalComprobantesCobrar - FieldByName('TotalAPagar').AsFloat;
        end;
    end;

    btn_Cobrar.Enabled := (FCantidadComprobantesCobrar > 0);
end;

procedure TformCobroAnticipado.dl_ComprobantesDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: String; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
var
	bmp: TBitMap;
begin
    with Sender do begin
        if (Column.FieldName = 'Cobrar') then begin
            Text := '';
            Canvas.FillRect(Rect);
            bmp := TBitMap.Create;
            try
                if (cds_Comprobantes.FieldByName('Cobrar').AsBoolean ) then
                    Img_Tilde.GetBitmap(0, Bmp)
                else
                    Img_Tilde.GetBitmap(1, Bmp);

                Canvas.Draw((Rect.Left + Rect.Right - Bmp.Width) div 2, Rect.Top, bmp);
            finally
                bmp.Free;
            end;
        end;

        if Column.FieldName = 'TotalAPagar' then begin
            Text := FormatFloat(FORMATO_IMPORTE , cds_Comprobantes.FieldByName('TotalAPagar').AsFloat);
        end;
    end;
end;

procedure TformCobroAnticipado.LimpiarListaComprobantes;
begin
    FTotalComprobantesCobrar := 0;
    FCantidadComprobantesCobrar := 0;
	cds_Comprobantes.DisableControls;
    try
        cds_Comprobantes.EmptyDataSet;
    finally
    	cds_Comprobantes.EnableControls;
    end; 
end;

procedure TformCobroAnticipado.dl_ComprobantesKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    if Key = VK_SPACE then begin
        dl_ComprobantesDblClick(Sender);
    end;
end;

function TformCobroAnticipado.CobrarComprobanteConPagoAutomatico(
  TipoComprobante: AnsiString; NumeroComprobante, TipoMedioPago: Integer): Boolean;
resourcestring
    MSG_COMPROBANTE_CON_PACPAT_CAPTION  = 'El Comprobante seleccionado tiene asignado' + chr(13)
                                        + 'el medio de pago autom�tico: %s' + chr(13)
                                        + '�Desea pagarlo igualmente?';
    MSG_COMPROBANTE_CON_PACPAT_TITLE = 'Confirme';
begin
    Result := False;
    if (MsgBox(FORMAT(MSG_COMPROBANTE_CON_PACPAT_CAPTION, [QueryGetValue(DMConnections.BaseCAC ,
            'SELECT dbo.ObtenerDescripcionTipoMedioPago(' + IntToStr(TipoMedioPago) + ')')]),
            MSG_COMPROBANTE_CON_PACPAT_TITLE, MB_YESNO + MB_ICONQUESTION) = mrYes) then begin
        Result := True;
    end;
end;


function TformCobroAnticipado.TienePagoAutomatico(
  TipoComprobante: AnsiString; NumeroComprobante: Integer; var TipoMedioPago: Integer): Boolean;
begin
    Result := False;

    TipoMedioPago := QueryGetValueInt(DMConnections.BaseCAC,'SELECT CodigoTipoMedioPago FROM Comprobantes WITH (NOLOCK)  '
                        +' WHERE TipoComprobante = ' + QuotedStr(TipoComprobante)
                        +' AND NumeroComprobante = ' + IntToStr(NumeroComprobante));

    if ( TipoMedioPago in SetPACPAT )
            and ( QueryGetValueInt(DMConnections.BaseCAC, 'SELECT ISNULL((SELECT COUNT(ID) FROM PagosRechazados  WITH (NOLOCK)  '
                    + ' GROUP BY TipoComprobante, NumeroComprobante'
                    + ' HAVING TipoComprobante = ' + QuotedStr(TipoComprobante)
                        +' AND NumeroComprobante = ' + IntToStr(NumeroComprobante)
                    +' ), 0) ' ) = 0 )
            and (cds_Comprobantes.FieldByName('EstadoPago').AsString = COMPROBANTE_IMPAGO) then begin

        Result := True;

    end;
end;

procedure TformCobroAnticipado.LimpiarDatosCliente;
begin
    lblRUT.Caption := EmptyStr;
    lblNombre.Caption := EmptyStr;
    lblDomicilio.Caption := EmptyStr;
    lblEnListaAmarilla.Visible  := False;				//SS_660_MCA_20140114
end;

//BEGIN:SS_1120_MVI_20130820 -------------------------------------------------
{-----------------------------------------------------------------------------
  Procedure: TformCobroAnticipado.cb_ConveniosDrawItem
  Date:      20-August-2013
  Description: Verifica si existe alg�n convenio de baja para llamar a la funci�n
                que colorea de rojo el item.
-----------------------------------------------------------------------------}
procedure TformCobroAnticipado.cb_ConveniosDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  if Pos( TXT_CONVENIO_DE_BAJA, cb_Convenios.Items[Index].Caption ) > 0 then  begin

     ItemComboBoxColor(Control, Index, Rect, State, clred, true);

  end
  else begin

     ItemComboBoxColor(Control, Index, Rect, State);

  end;                                                                                      

end;
//END:SS_1120_MVI_20130820 ----------------------------------------------------

end.
