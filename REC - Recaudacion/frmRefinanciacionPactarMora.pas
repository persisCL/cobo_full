unit frmRefinanciacionPactarMora;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,

  PeaProcsCN, SysUtilsCN,

  Dialogs, StdCtrls, Validate, DateEdit, ExtCtrls;

type
  TRefinanciacionPactarMoraForm = class(TForm)
    pnlAcciones: TPanel;
    btnCancelar: TButton;
    GroupBox1: TGroupBox;
    Button1: TButton;
    dedtNuevaFecha: TDateEdit;
    Label1: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
  private
    { Private declarations }
    FAhora: TDateTime;
  public
    { Public declarations }
    function Inicializar(Ahora, Fecha: TDateTime): Boolean;
  end;

var
  RefinanciacionPactarMoraForm: TRefinanciacionPactarMoraForm;

implementation

{$R *.dfm}

procedure TRefinanciacionPactarMoraForm.btnCancelarClick(Sender: TObject);
begin
    ModalResult := mrCancel;
end;

procedure TRefinanciacionPactarMoraForm.Button1Click(Sender: TObject);
begin
    if dedtNuevaFecha.Date < FAhora then begin
        ShowMsgBoxCN('Validación Fecha', 'La Fecha NO puede ser inferior a la Fecha Actual.', MB_ICONEXCLAMATION, Self)
    end
    else ModalResult := mrOk;
end;

function TRefinanciacionPactarMoraForm.Inicializar(Ahora, Fecha: TDateTime): Boolean;
begin
    try
        FAhora := Ahora;
        dedtNuevaFecha.Date := Fecha;
        Result := True;
    except
        Result := False;
    end;
end;

end.
