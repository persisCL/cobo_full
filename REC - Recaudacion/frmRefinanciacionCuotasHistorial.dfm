object RefinanciacionCuotasHistorialForm: TRefinanciacionCuotasHistorialForm
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = ' Historial Estados Cuota'
  ClientHeight = 258
  ClientWidth = 687
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  DesignSize = (
    687
    258)
  PixelsPerInch = 96
  TextHeight = 13
  object pnlAcciones: TPanel
    Left = 0
    Top = 218
    Width = 687
    Height = 40
    Align = alBottom
    Anchors = []
    Color = 14408667
    ParentBackground = False
    TabOrder = 0
    ExplicitLeft = -531
    ExplicitTop = 176
    ExplicitWidth = 957
    DesignSize = (
      687
      40)
    object btnCancelar: TButton
      Left = 607
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Salir'
      TabOrder = 0
      OnClick = btnCancelarClick
      ExplicitLeft = 877
    end
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 671
    Height = 204
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = '  Historial Estados Cuota  '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    ExplicitWidth = 451
    ExplicitHeight = 185
    DesignSize = (
      671
      204)
    object DBListEx1: TDBListEx
      Left = 6
      Top = 17
      Width = 658
      Height = 180
      Anchors = [akLeft, akTop, akRight, akBottom]
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 115
          Header.Caption = 'Estado'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          IsLink = False
          FieldName = 'Descripcion'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 120
          Header.Caption = 'Usuario Creaci'#243'n'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          IsLink = False
          FieldName = 'UsuarioCreacion'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 135
          Header.Caption = 'Fecha Creaci'#243'n'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'FechaCreacion'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 130
          Header.Caption = 'Usuario Modificaci'#243'n'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          IsLink = False
          FieldName = 'UsuarioModificacion'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 135
          Header.Caption = 'Fecha Modificaci'#243'n'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'FechaModificacion'
        end>
      DataSource = dsCuotasHistorico
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
      ExplicitWidth = 438
      ExplicitHeight = 161
    end
  end
  object cdsCuotasHistorico: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspObtenerRefinanciacionCuotasEstadosHistorico'
    Left = 312
    Top = 88
    object cdsCuotasHistoricoCodigoEstadoCuota: TSmallintField
      FieldName = 'CodigoEstadoCuota'
    end
    object cdsCuotasHistoricoDescripcion: TStringField
      FieldName = 'Descripcion'
      Size = 30
    end
    object cdsCuotasHistoricoUsuarioCreacion: TStringField
      FieldName = 'UsuarioCreacion'
    end
    object cdsCuotasHistoricoFechaCreacion: TDateTimeField
      FieldName = 'FechaCreacion'
    end
    object cdsCuotasHistoricoUsuarioModificacion: TStringField
      FieldName = 'UsuarioModificacion'
    end
    object cdsCuotasHistoricoFechaModificacion: TDateTimeField
      FieldName = 'FechaModificacion'
    end
  end
  object dsCuotasHistorico: TDataSource
    DataSet = cdsCuotasHistorico
    Left = 272
    Top = 112
  end
  object dspObtenerRefinanciacionCuotasEstadosHistorico: TDataSetProvider
    DataSet = spObtenerRefinanciacionCuotasEstadosHistorico
    Left = 352
    Top = 64
  end
  object spObtenerRefinanciacionCuotasEstadosHistorico: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerRefinanciacionCuotasEstadosHistorico'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCuota'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end>
    Left = 368
    Top = 48
  end
end
