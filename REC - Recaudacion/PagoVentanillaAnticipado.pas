{-----------------------------------------------------------------------------
 File Name: PagoVentanilla.pas
 Author:    mlopez
 Date Created: 10/08/2005
 Language: ES-AR
 Description:

 Revision 1:
 Author:   jconcheyro
 Date Created: 02/11/2005
 Kanav:        1455
 Description: solicitan unificar la forma de pago con tarjeta para WebPay y Cajas Lider
 al formato de WebPay

  Revision 2:
  Author : jconcheyro
  Date Created : 22/11/2005
  Kanav 1510
  Description :  daba un error cuando se hab�a guardado en la base un valor en PAT_FECHAVENCIMIENTO
  con un separador de fecha diferente a /

  Revision 4:
  Author : nefernandez
  Date : 16/03/2007
  Description : Se pasa el parametro CodigoUsuario al SP "CrearRecibo"
  (para Auditoria)

  Revision 5:
  Author : nefernandez
  Date : 22/08/2007
  Description :
    * Cambio en "CrearRecibo" del par�metro "@Importe" de ftInteger a ftLargeint
    * Cambio en "sp_RegistrarDetallePagoComprobante" del par�metro "@ChequeMonto"
    de ftInteger a ftLargeint

  Revision 6:
    Date: 24/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura


  Revision 7:
  Author: mbecerra
  Date: 14-Abril-2009
  Description:	(Ref. Facturaci�n Electr�nica)
                Se cambiaron instrcciones de Delphi  dmconnections.basecac.Commit
                por instrucciones expl�citas.

  Revision 8:
        12-Junio-2009
        	1.-		Se cambia la fecha Nowbase (en GenerarRecibo) por la fecha
                	ingresada en la componente eFechaPago.Date

        15-Junio-2009
        	1.- 	(Ref. SS 809)
                    Se agregan las formas de pago tarjeta D�bito y tarjeta cr�dito

            2.-		Se quita la forma de pago "Pagar�" en un pago anticipado.

  Revision : 9
    Author : pdominguez
    Date   : 12/08/2009
    Description : SS 809
            - Se eliminaron los siguientes objetos / constantes/ variables:
                cb_FormasPagoChange: TVariantComboBox

            - Se crearon los siguientes objetos / constantes/ variables:
                edtCodigoFormaPago: TEdit,
                edtDescFormaPago: TEdit
                spValidarFormaDePagoEnCanal: TADOStoredProc

            - Se eliminaron los siguientes procedimientos / funciones:
                FormaDePagoCargada,
                FiltrarFormasDePago

            - Se modificarion / crearon los siguientes procedimientos / funciones:
                cb_FormasPagoChange,
                btnAceptarClick,
                edtCodigoFormaPagoChange,
                deFechaPagoChange,
                cb_CanalesDePagoChange

Revision : 10
    Author : pdominguez
    Date   : 22/09/2009
    Description : SS 809
        - Se modificaron/a�adieron los siguientes objetos/constantes/variables:
            - Se eliminaron las constantes CONST_PAGE_XXXXXX.
            - Se a�adieron los par�metros @CodigoPantalla, @CodigoBanco y @CodigoTarjeta
            en el SP ValidarFormaDePagoEnCanal.
            - Se crean las variables FCodigoPantalla y FCodigoTarjeta
            - Se a�ade una p�gina nueva "NoDefinido" al objeto nbOpciones: TNotebook

        - Se modificaron/a�adieron los siguientes procedimientos/funciones:
            Inicializar,
            cb_FormasPagoChange,
            edtCodigoFormaPagoChange

Revision : 11
    Author : pdominguez
    Date   : 28/09/2009
    Description : SS 834
        - Se modificaron/a�adieron los siguientes objetos/constantes/variables:
            Se a�ade el objeto edtRepactacionCheque: TEdit,
            Se a�ade el par�metro @RepactacionNumero al sp sp_RegistrarDetallePagoComprobante.

        - Se modificaron/a�adieron los siguientes procedimientos/funciones:
            ValidarDatos,
            GenerarPagoCheque

Revision : 12
    Author : pdominguez
    Date   : 26/10/2009
    Description : SS 834
        - Se modificaron/a�adieron los siguientes objetos/constantes/variables:
            TFormaPago
            FFechaMaximaPagare, FFechaMinimaPagare: TDate;
            Se a�adi� la p�gina Pagare al objeto nbOpciones: TNoteBook;
                - Objetos de la p�gina nueva:
                    label37, lblPagareTitular, lblPagareRUT, lblPagareBanco,
                    lblPagareNumero, lblPagareFecha, lblPagareRepactacion: TLabel.
                    txtPagareNombreTitular, txtPagareDocumento, txtPagareNumero,
                    edtPagareRepactacion: TEdit;
                    btnDatosClientePagare: TSpeedButton;
                    cbPagareBancos: TVariantComboBox;
                    dePagareFecha: TDateEdit;

        - Se modificaron/a�adieron los siguientes procedimientos/funciones:
            btnDatosClientePagareClick,
            ValidarDatos,
            GenerarPagoPagare,
            cbPagareBancosChange,
            RegistrarPago,
            cb_FormasPagoChange

Revision : 13
    Author : Nelson Droguett Sierra
    Date   : 20-Agosto-2010
    Description : (Ref. SS-914)
            	Se desactiva y reactiva el boton aceptar, para evitar mas de un
                click que provoque duplicidad de recibos.

Firma       : SS_295_437_NDR_20121030
Description : No calcular intereses (si cambia el checkbox, esto se refleja en la variable FNoCalcularIntereses)

Firma       : SS_1300_MCA_20150610
Descripcion : se agrega restriccion que la fecha de pago no puede ser posterior a la de hoy ni menor
                que la fecha actual - los dias definidos en el parametro general DIAS_FECHA_MINIMA_PAGO_VENTANILLA

Firma		:	SS_1410_MCA_20151027
Descripcion	:	se agregan parametros CodiugoUsuario y CodigoSistema, para validar si el usuario tiene acceso a una funcion del sistema
-----------------------------------------------------------------------------}
unit PagoVentanillaAnticipado;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, VariantComboBox, ADODB, DMConnection, ExtCtrls,
  Validate, DateEdit, DmiCtrls, Buttons, ComCtrls, Util, UtilProc, DB,
  PeaProcs, PeaTypes, UtilDB, ConstParametrosGenerales, RStrings, UtilFacturacion,
  SysUtilsCN;

type
  TFormaPago = (tpEfectivo, tpTarjetaCredito, tpTarjetaDebito, tpCheque,
                tpValeVista, tpDebitoCuenta, tpPagare); // Rev. 12 (SS 834)

  TfrmPagoVentanillaAnticipado = class(TForm)
    btnCerrar: TButton;
    Bevel1: TBevel;
    lbl_FormaPago: TLabel;
    deFechaPago: TDateEdit;
    Label2: TLabel;
    Label3: TLabel;
    lblaca: TLabel;
    btnAceptar: TButton;
    nbOpciones: TNotebook;
    lblFechaCheque: TLabel;
    edAutorizacion: TEdit;
    Label5: TLabel;
    edCupon: TNumericEdit;
    lblSucursalCheque: TLabel;
    txtNumeroCheque: TEdit;
    Label8: TLabel;
    Label6: TLabel;
    Label10: TLabel;
    cbbancos: TVariantComboBox;
    txtNombreTitular: TEdit;
    txtDocumento: TEdit;
    lblRUT: TLabel;
    Label11: TLabel;
    CrearRecibo: TADOStoredProc;
    _CerrarPagoComprobante: TADOStoredProc;
    deFechaCheque: TDateEdit;
    Label12: TLabel;
    btnDatosClienteCheque: TSpeedButton;
    edCuotas: TNumericEdit;
    Label7: TLabel;
    Label13: TLabel;
    txt_TomadoPorVV: TEdit;
    txt_RUTVV: TEdit;
    txt_NroVV: TEdit;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    cb_BancosVV: TVariantComboBox;
    de_FechaVV: TDateEdit;
    Label19: TLabel;
    btn_DatosClienteValeVista: TSpeedButton;
    lbl_CanalDePago: TLabel;
    cb_CanalesDePago: TVariantComboBox;
    lbl_DebitoCuentaPAC: TLabel;
    lbl_DebitoCuentaPAT: TLabel;
    lbl_CodigoBancoPAC: TLabel;
    lbl_NroCuentaBancariaPAC: TLabel;
    lbl_SucursalPAC: TLabel;
    lbl_TitularPAC: TLabel;
    txt_NroCuentaBancariaPAC: TEdit;
    txt_SucursalPAC: TEdit;
    txt_TitularPAC: TEdit;
    lbl_CodigoTipoTarjetaCreditoPAT: TLabel;
    lbl_NumeroTarjetaCreditoPAT: TLabel;
    lbl_FechaVencimientoPAT: TLabel;
    lbl_CodigoEmisorTarjetaCreditoPAT: TLabel;
    txt_NumeroTarjetaCreditoPAT: TEdit;
    txt_FechaVencimiento: TDateEdit;
    cb_BancosPAC: TVariantComboBox;
    cb_TipoTarjetaCreditoPAT: TVariantComboBox;
    cb_EmisorTarjetaCreditoPAT: TVariantComboBox;
    lbl_MontoPagado: TLabel;
    ne_ImportePagado: TNumericEdit;
    Bevel2: TBevel;
    nePagacon: TNumericEdit;
    Label20: TLabel;
    sp_RegistrarDetallePagoComprobante: TADOStoredProc;
    sp_RegistrarPagoAnticipado: TADOStoredProc;
    Label1: TLabel;
    //neCambio: TNumericEdit;  //TASK_011_GLE_20161103 comenta original
    lblCambio: TLabel;         //TASK_011_GLE_20161103 modifica tipo de control
    Label4: TLabel;
    chkNoCalcularIntereses: TCheckBox;
    Label9: TLabel;
    Label18: TLabel;
    Label21: TLabel;
    edtTarjetaDebitoTitular: TEdit;
    edtTarjetaDebitoNroConfirmacion: TEdit;
    vcbTarjetaDebitoBancos: TVariantComboBox;
    spbTarjetaDebitoCopiarDatosCliente: TSpeedButton;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    edtTarjetaCreditoTitular: TEdit;
    vcbTarjetaCreditoBancos: TVariantComboBox;
    vcbTarjetaCreditoTarjetas: TVariantComboBox;
    edtTarjetaCreditoNroConfirmacion: TEdit;
    neTarjetaCreditoCuotas: TNumericEdit;
    Label28: TLabel;
    spbTarjetaCreditoCopiarDatosCliente: TSpeedButton;
    edtCodigoFormaPago: TEdit;
    edtDescFormaPago: TEdit;
    spValidarFormaDePagoEnCanal: TADOStoredProc;
    Label29: TLabel;
    edtRepactacionCheque: TEdit;
    Label30: TLabel;
    lblPagareTitular: TLabel;
    lblPagareRUT: TLabel;
    lblPagareBanco: TLabel;
    lblPagareNumero: TLabel;
    lblPagareFecha: TLabel;
    lblPagareRepactacion: TLabel;
    Label37: TLabel;
    txtPagareNombreTitular: TEdit;
    btnDatosClientePagare: TSpeedButton;
    txtPagareDocumento: TEdit;
    cbPagareBancos: TVariantComboBox;
    txtPagareNumero: TEdit;
    dePagareFecha: TDateEdit;
    edtPagareRepactacion: TEdit;
    sp_ActualizarClienteMoroso: TADOStoredProc;
    procedure ne_ImportePagadoChange(Sender: TObject);
    procedure btnCerrarClick(Sender: TObject);
    procedure cb_FormasPagoChange(Sender: TObject);
    procedure lblacaClick(Sender: TObject);
    procedure cbbancosChange(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
    procedure btnDatosClienteChequeClick(Sender: TObject);
    procedure deFechaPagoChange(Sender: TObject);
    procedure cb_BancosVVChange(Sender: TObject);
    procedure btn_DatosClienteValeVistaClick(Sender: TObject);
    procedure cb_CanalesDePagoChange(Sender: TObject);
    procedure nePagaconChange(Sender: TObject);
    procedure neImporteKeyPress(Sender: TObject; var Key: Char);
    procedure spbTarjetaDebitoCopiarDatosClienteClick(Sender: TObject);
    procedure spbTarjetaCreditoCopiarDatosClienteClick(Sender: TObject);
    procedure edtCodigoFormaPagoChange(Sender: TObject);
    procedure btnDatosClientePagareClick(Sender: TObject);
    procedure cbPagareBancosChange(Sender: TObject);
    procedure chkNoCalcularInteresesClick(Sender: TObject);                     //SS_295_437_NDR_20121030
  private
  	FRUT: String;
    FTitular: String;
    FNumeroRecibo: Int64;
    FImportePagado: Double;
    FCodigoBanco: Integer;
    FCodigoPantalla,         // Rev. 10 (SS 809)
    FCodigoTarjeta: Integer; // Rev. 10 (SS 809)
    FCodigoConvenio: Integer;
    FCodigoFormaPago: Integer;
    FFormaPagoActual: TFormaPago;
    FImprimirRecibo: Boolean;
    FNumeroPOS, FPuntoEntrega, FPuntoVenta: Integer;
    FFechaMinimaCheque, FFechamaximaCheque: TDate;
    FCodigoCanalPago: Integer;
    FNoCalcularIntereses: Boolean;
    FFechaMaximaPagare,         // Rev. 12 (SS 834)
    FFechaMinimaPagare: TDate;  // Rev. 12 (SS 834)
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    function CargarBancos(Combo: TVariantComboBox): Boolean;
    function RegistrarPago(FormaPago: TFormaPago; NumeroRecibo: Integer; var MensajeError: String): Boolean;
    function GenerarRecibo(var NumeroRecibo: Int64): Boolean;
    function ValidarDatos(FormaPago: TFormaPago):Boolean;
    function ValidarDatosTurno: Boolean;
    //procedure FiltrarCanalesDePago;
    //function CanalDePagoCargado(CodigoCanalPago: Byte): Boolean;
    procedure MostrarDatosPACConvenio;
    //procedure MostrarDatosPATConvenio;
    function GenerarPagoEfectivo(NumeroRecibo: Integer; var MensajeError: String): Boolean;
    function GenerarPagoTarjetaCredito(NumeroRecibo: Integer; var MensajeError: String): Boolean;
    function GenerarPagoTarjetaDebito(NumeroRecibo: Integer; var MensajeError: String): Boolean;
    function GenerarPagoDebitoCuenta(NumeroRecibo: Integer; var MensajeError: String): Boolean;
    function GenerarPagoCheque(NumeroRecibo: Integer; var MensajeError: String): Boolean;
    function GenerarPagoValeVista(NumeroRecibo: Integer; var MensajeError: String): Boolean;
    function GenerarPagoPagare(NumeroRecibo: Integer; var MensajeError: String): Boolean;
  public
    property CodigoCanalPago: Integer read FCodigoCanalPago;
    function Inicializar(RUT, Titular: String; NumeroPOS, PuntoEntrega,
      PuntoVenta, CodigoCanalDePago, CodigoConvenio: Integer): Boolean;
    function ImprimirRecibo : Boolean;
    function NumeroRecibo : int64;
  end;

var
  frmPagoVentanillaAnticipado: TfrmPagoVentanillaAnticipado;

implementation

uses Contnrs;

{$R *.dfm}

{ TfrmPagoVentanilla }
{-----------------------------------------------------------------------------
  Procedure: TfrmPagoVentanilla.Inicializar
  Author:    mlopez
  Date:      10/08/2005
  Arguments:
    - ComprobantesCobrar: TListaComprobante. Lista de comprobantes a cobrar. No
        debe ser nil.
    - Importe: Double.
  Result:    Boolean

  Revision : 1
      Author : pdominguez
      Date   : 22/09/2009
      Description : SS 809
            - Se inhabilitan las referencias a la carga de los combos de bancos
            y tarjetas.
-----------------------------------------------------------------------------}
function TfrmPagoVentanillaAnticipado.Inicializar(RUT, Titular: String;
    NumeroPOS, PuntoEntrega, PuntoVenta, CodigoCanalDePago, CodigoConvenio: Integer): Boolean;
resourcestring
    MSG_POS = 'POSNET : ';
    MSG_COBRO_COMPROBANTES = 'Cobro Anticipado';
begin
    Result := True;
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

    FCodigoCanalPago := CodigoCanalDePago;
    FNumeroPOS := NumeroPOS;
    FPuntoEntrega := PuntoEntrega;
    FPuntoVenta := PuntoVenta;
    FImportePagado := 0;
    FCodigoConvenio := CodigoConvenio;
    FNoCalcularIntereses := chkNoCalcularIntereses.Checked;
    lblACA.Enabled := ExisteAcceso('modif_fecha_cobro');
    nbOpciones.PageIndex := CONST_PAGE_EFECTIVO;

//    Result := CargarBancos(cbBancos) and CargarBancos(cb_BancosVV);
//    if not Result then Exit;

    try
        if FCodigoCanalPago < 0 then FCodigoCanalPago := 0;
        CargarCanalesDePago(DMConnections.BaseCAC, cb_CanalesDePago, FCodigoCanalPago, 0); // SS_637_20101123
 //       FiltrarCanalesDePago;
        cb_CanalesDePagoChange(cb_CanalesDePago);

//        CargarBancosPAC(DMConnections.BaseCAC, cb_BancosPAC);
//        CargarTiposTarjetasCredito(DMConnections.BaseCAC, cb_TipoTarjetaCreditoPAT, 0, True);
//        CargarEmisoresTarjetasCredito(DMConnections.BaseCAC, cb_EmisorTarjetaCreditoPAT, 0, True);
    except
        Result := False;
    end;

    if Result then begin
        FNumeroRecibo := 0;
        FImprimirRecibo := False;
    	FRUT := RUT;
        FTitular := Titular;
        deFechaPago.Date := StrToDate(DateToStr(NowBase(DMConnections.BaseCAC)));
        ne_ImportePagado.Value := FImportePagado;
        FImprimirRecibo := False;
        deFechaCheque.Date := StrToDate(DateToStr(NowBase(DMConnections.BaseCAC)));
        de_FechaVV.Date := StrToDate(DateToStr(NowBase(DMConnections.BaseCAC)));

        Caption := Caption + Space(1) + '-' + Space(1) + MSG_POS + IntToStr(NumeroPOS);

    end;
end;

procedure TfrmPagoVentanillaAnticipado.btnCerrarClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Procedure: TfrmPagoVentanilla.cb_FormasPagoChange
  Author:    mlopez
  Date:      10/08/2005
  Arguments: Sender: TObject
  Result:    None
  Revision 1
  Description: solicitan unificar la forma de pago con tarjeta para WebPay y Cajas Lider
  al formato de WebPay Kanav 1455

  Revision : 1
      Author : pdominguez
      Date   : 12/08/2009
      Description : SS 809
        - Se inhabilitan las referencias al objeto eliminado cb_FormasPago.
        - No se pasa el foco al primer control de la forma de pago entrada.

  Revision : 2
      Author : pdominguez
      Date   : 22/09/2009
      Description : SS 809
        - Se implementa la visualizaci�n de las entradas de los datos por
        asociaci�n con la forma de pago. Se sustituye el anidamiento de
        cl�usulas IF por un CASE.

  Revision : 3
    Author : pdominguez
    Date   : 27/10/2009
    Description : SS 834
        - Se a�ade la entrada de datos para las Formas de Pago tipo Pagar�.
-----------------------------------------------------------------------------}
procedure TfrmPagoVentanillaAnticipado.cb_FormasPagoChange(Sender: TObject);
Resourcestring
    MSG_ERROR_FORMA_NO_DEFINIDA = 'La forma de Pago especificada NO tiene definida su entrada de Datos.';
var
    MaxDias: Integer;
    MinDias: Integer;
    FFechaPago: TDateTime;
begin
    //FCodigoFormaPago := cb_FormasPago.Value; // Rev. 1 (SS 809)
    ne_ImportePagado.Value := FImportePagado;

    case FCodigoPantalla of
        CONST_PAGE_TARJETA_DEBITO: begin
            {REV.8 (SS 809)
            nbOpciones.PageIndex := CONST_PAGE_TARJETA;
            FFormaPagoActual := tpTarjetaDebito;
            if Sender = cb_FormasPago then ActiveControl := edAutorizacion;}

            vcbTarjetaDebitoBancos.Clear;
            CargarBancosPagoVentanilla(DMConnections.BaseCAC,vcbTarjetaDebitoBancos,FCodigoBanco,FCodigoCanalPago,True);
            nbOpciones.PageIndex := CONST_PAGE_TARJETA_DEBITO;
            FFormaPagoActual := tpTarjetaDebito;
            //if Sender = cb_FormasPago then ActiveControl := edtTarjetaDebitoTitular; // Rev. 1 (SS 809)

            //FIN REV.8 SS 809
        end;

        CONST_PAGE_TARJETA_CREDITO: begin
            FFormaPagoActual := tpTarjetaCredito;
            //REV.8 SS 809
        	vcbTarjetaCreditoTarjetas.Clear;
            CargarTiposTarjetasCreditoVentanilla(DMConnections.BaseCAC,vcbTarjetaCreditoTarjetas,FCodigoTarjeta,True,True);
            vcbTarjetaCreditoBancos.Clear;
            CargarBancosPagoVentanilla(DMConnections.BaseCAC,vcbTarjetaCreditoBancos,FCodigoBanco,FCodigoCanalPago,True);
            nbOpciones.PageIndex := CONST_PAGE_TARJETA_CREDITO;
//        if Sender = cb_FormasPago then ActiveControl := edtTarjetaCreditoTitular;
        //FIN REV.8 SS 809
        end;

        CONST_PAGE_DEBITO_CUENTA_PAC: begin
            FFormaPagoActual := tpDebitoCuenta;
            if FCodigoCanalPago = CONST_CANAL_PAGO_PAC then begin
                nbOpciones.PageIndex := CONST_PAGE_DEBITO_CUENTA_PAC;
                CargarBancosPAC(DMConnections.BaseCAC, cb_BancosPAC,FCodigoBanco,True);
                (* Mostrar los datos del comprobante. *)
                MostrarDatosPACConvenio;
                //if Sender = cb_FormasPago then ActiveControl := cb_BancosPAC; // Rev. 1 (SS 809)
            end else begin
                CargarTiposTarjetasCredito(DMConnections.BaseCAC, cb_TipoTarjetaCreditoPAT, FCodigoTarjeta, True);
                CargarEmisoresTarjetasCredito(DMConnections.BaseCAC, cb_EmisorTarjetaCreditoPAT, FCodigoBanco, True);
                nbOpciones.PageIndex := CONST_PAGE_DEBITO_CUENTA_PAT;
                //if Sender = cb_FormasPago then ActiveControl := cb_TipoTarjetaCreditoPAT; // Rev. 1 (SS 809)
            end;
        end;

        CONST_PAGE_CHEQUE: begin
            nbOpciones.PageIndex := CONST_PAGE_CHEQUE;
            FFormaPagoActual := tpCheque;
            CargarBancosPagoVentanilla(DMConnections.BaseCAC,cbBancos,FCodigoBanco,FCodigoCanalPago,True);
            //if Sender = cb_FormasPago then ActiveControl := txtNombreTitular; // Rev. 1 (SS 809)

            FFechaPago := Trunc(deFechaPago.Date);
            ObtenerParametroGeneral(DMConnections.BaseCAC, 'DIAS_FECHA_MINIMA_CHEQUE', MinDias);
            ObtenerParametroGeneral(DMConnections.BaseCAC, 'DIAS_FECHA_MAXIMA_CHEQUE', MaxDias);

            FFechaMaximaCheque := FFechaPago + MaxDias + 1;
            FFechaMinimaCheque := FFechaPago - MinDias;
        end;

        CONST_PAGE_EFECTIVO: begin
            nbOpciones.PageIndex := CONST_PAGE_EFECTIVO;
            //ActiveControl := nePagacon; // Rev. 1 (SS 809)
            FFormaPagoActual := tpEfectivo;
        end;

        CONST_PAGE_VALE_VISTA: begin
            nbOpciones.PageIndex := CONST_PAGE_VALE_VISTA;
            FFormaPagoActual := tpValeVista;
            CargarBancosPagoVentanilla(DMConnections.BaseCAC,cb_BancosVV,FCodigoBanco,FCodigoCanalPago,True);
            //if Sender = cb_FormasPago then ActiveControl := txt_TomadoPorVV; // Rev. 1 (SS 809)

            FFechaPago := Trunc(de_FechaVV.Date);
            ObtenerParametroGeneral(DMConnections.BaseCAC, 'DIAS_FECHA_MINIMA_CHEQUE', MinDias);
            ObtenerParametroGeneral(DMConnections.BaseCAC, 'DIAS_FECHA_MAXIMA_CHEQUE', MaxDias);
            FFechaMaximaCheque := FFechaPago + MaxDias + 1;
            FFechaMinimaCheque := FFechaPago - MinDias;
        end;
        // Rev. 3 (SS 834)
        CONST_PAGE_PAGARE: begin
            nbOpciones.PageIndex := CONST_PAGE_PAGARE;
            FFormaPagoActual := tpPagare;
            cbPagareBancos.Clear;
            CargarBancosPagoVentanilla(DMConnections.BaseCAC,cbPagareBancos,FCodigoBanco,FCodigoCanalPago,True);
            txtPagareDocumento.Text := '';
            txtPagareNombreTitular.Text := '';
            txtPagareNumero.Text := '';
            dePagareFecha.Date := StrToDate(DateToStr(NowBase(DMConnections.BaseCAC)));
            edtPagareRepactacion.Text := '';
            FFechaPago := Trunc(deFechaPago.Date);
            ObtenerParametroGeneral(DMConnections.BaseCAC, 'DIAS_FECHA_MINIMA_PAGARE', MinDias);
            ObtenerParametroGeneral(DMConnections.BaseCAC, 'DIAS_FECHA_MAXIMA_PAGARE', MaxDias);
            FFechaMaximaPagare := FFechaPago + MaxDias + 1;
            FFechaMinimaPagare := FFechaPago - MinDias;
        end;
        // Fin Rev. 3 (SS 834)
    else
        nbOpciones.PageIndex := CONST_PAGE_NO_DEFINIDO;
        FCodigoFormaPago        := 0;
        edtCodigoFormaPago.Tag  := 0;
        MsgBox(MSG_ERROR_FORMA_NO_DEFINIDA,Caption,MB_ICONSTOP);
    end;
end;

{-----------------------------------------------------------------------------
  Procedure: TfrmPagoVentanilla.lblacaClick
  Author:    mlopez
  Date:      10/08/2005
  Arguments: Sender: TObject
  Result:    None
-----------------------------------------------------------------------------}
procedure TfrmPagoVentanillaAnticipado.lblacaClick(Sender: TObject);
begin
    deFechaPago.Enabled := True;
    lblAca.Enabled := False;
end;

{-----------------------------------------------------------------------------
  Procedure: TfrmPagoVentanilla.CargarBancos
  Author:    mlopez
  Date:      10/08/2005
  Arguments: Combo: TVariantComboBox
  Result:    Boolean
-----------------------------------------------------------------------------}
function TfrmPagoVentanillaAnticipado.CargarBancos(Combo: TVariantComboBox): Boolean;
var
    Qry:TADOQuery;
begin
    Result := True;

    Qry := TADOQuery.Create(nil);
    Qry.Connection := DMConnections.BaseCAC;
    (* Obtener los bancos que est�n habilitados para el Pago en Ventanilla. *)
    Qry.SQL.Add('SELECT CodigoBanco As Codigo, Descripcion As Descripcion'
        + ' From Bancos  WITH (NOLOCK) '
		+ ' Where (Activo = 1) And (HabilitadoPagoVentanilla = 1)'
		+ ' Order By Descripcion');
    try
        try
            Qry.Open;
            if Qry.IsEmpty then begin
                Qry.Close;
                Exit;
            end;
            while not Qry.Eof do begin
                Combo.Items.Add(Qry.FieldByName('Descripcion').AsString, Qry.FieldByName('Codigo').AsInteger);
                Qry.Next;
            end;
            Combo.ItemIndex := 0;
            Combo.OnChange(Self);
        except
            Result := False;
        end;
    finally
        FreeAndNil(Qry);
    end;
end;

{-----------------------------------------------------------------------------
  Procedure: TfrmPagoVentanilla.cbbancosChange
  Author:    mlopez
  Date:      10/08/2005
  Arguments: Sender: TObject
  Result:    None
-----------------------------------------------------------------------------}
procedure TfrmPagoVentanillaAnticipado.cbbancosChange(Sender: TObject);
begin
    FCodigoBanco := Integer(cbBancos.Value);
end;

{******************************* Procedure Header ******************************
Procedure Name: cbPagareBancosChange
Author : pdominguez
Date Created : 27/10/2009
Parameters : Sender: TObject
Description : SS 834
    - Almacena el identificador del Banco seleccionado en el combo para la entrada
    de datos Pagar�.
*******************************************************************************}
procedure TfrmPagoVentanillaAnticipado.cbPagareBancosChange(Sender: TObject);
begin
    FCodigoBanco := Integer(cbPagareBancos.Value);
end;

{-----------------------------------------------------------------------------
  Procedure: TfrmPagoVentanilla.GenerarRecibo
  Author:    mlopez
  Date:      10/08/2005
  Arguments: var NumeroRecibo: Int64
  Result:    Boolean

  Revision : 2
    Author : pdominguez
    Date   : 12/08/2009
    Description : SS 809
        - Se incluye el objeto edtCodigoFormaPago, en la validaci�n de los
        controles.
-----------------------------------------------------------------------------}
function TfrmPagoVentanillaAnticipado.GenerarRecibo(var NumeroRecibo: Int64): Boolean;
resourcestring
    MSG_ERROR_RECIBO = 'Ha ocurrido un error creando el comprobante de cancelaci�n';
begin
    try
        // Creamos el recibo
        //REV.6  12-Junio-2009 CrearRecibo.Parameters.ParamByName('@FechaHora').Value := StrToDate(DateToStr(NowBase(DMCOnnections.BaseCAC)));
        CrearRecibo.Parameters.ParamByName('@FechaHora').Value := deFechaPago.Date;
        CrearRecibo.Parameters.ParamByName('@Importe').Value := FImportePagado * 100;
        CrearRecibo.Parameters.ParamByName('@NumeroRecibo').Value := Null;
        CrearRecibo.Parameters.ParamByName('@CodigoTipoMedioPago').Value := FCodigoCanalPago;
        // Revision 4
        CrearRecibo.Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;
        CrearRecibo.ExecProc;
        NumeroRecibo := CrearRecibo.Parameters.ParamByName('@NumeroRecibo').Value;
        Result := True;
    except
        on e: Exception do begin
            raise Exception.Create(MSG_ERROR_RECIBO + CRLF + e.Message);
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Procedure: TfrmPagoVentanilla.btnAceptarClick
  Author:    mlopez
  Date:      10/08/2005
  Arguments: Sender: TObject
  Result:    None

  Revision : 1
    Author : pdominguez
    Date   : 12/08/2009
    Description : SS 809
        - Se incluye el objeto edtCodigoFormaPago, en la validaci�n de los
        controles.
-----------------------------------------------------------------------------}
procedure TfrmPagoVentanillaAnticipado.btnAceptarClick(Sender: TObject);
resourcestring
    MSG_ERROR = 'Error. El Pago no ha sido registrado';
    MSG_REGISTRO_CORRECTO = 'El pago se ha registrado correctamente';
    MSG_CONFIRMAR_RECIBO = 'Desea Imprimir el Comprobante de Cancelaci�n?';
    MSG_CAPTION = 'Registrar Pago';
    MSG_CONFIRMAR_PAGO = 'Confirma registrar este pago?';
    MSG_SELECCIONAR_CANAL_PAGO = 'Debe seleccionar un canal de pago';
    MSG_SELECCIONAR_FORMA_PAGO = 'Debe seleccionar una forma de pago';
    MSG_FALTA_MONTO = 'Debe ingresar un monto';
    MSG_ERROR_PAGOS_FUTUROS = 'La fecha de pago no puede ser posterior a la fecha actual';                  //SS_1300_MCA_20150610
    MSG_ERROR_PAGOS_PASADOS = 'La fecha de pago no puede ser inferior a %d d�as.';                          //SS_1300_MCA_20150610

var
    NumeroRecibo: Int64;
    MensajeError: String;
    FFechaHoraEmision, FFechaActual : TDateTime;                                        //SS_1300_MCA_20150610
    DiasFechaMinimaPago     : integer;                                                  //SS_1300_MCA_20150610
begin
    //Rev.13 / 20-Agosto-2010 / Nelson Droguett Sierra ------------------------------
    try
        btnAceptar.Enabled := False;
        EmptyKeyQueue; // SS_914_20100930
        Application.ProcessMessages;

        (* Verificar que se haya seleccionado el Canal y la Forma de Pago. *)
        if not ValidateControls([cb_CanalesDePago, edtCodigoFormaPago, ne_ImportePagado], // Rev. 1 (SS 809)
                [cb_CanalesDePago.ItemIndex >= 0, edtCodigoFormaPago.Tag = 1, ne_ImportePagado.Value > 0],
                MSG_CAPTION,
                [MSG_SELECCIONAR_CANAL_PAGO, MSG_SELECCIONAR_FORMA_PAGO, MSG_FALTA_MONTO]) then Exit;

        if FImportePagado <= 0 then Exit;

        if not ValidarDatos(FFormaPagoActual) then Exit;

        if MsgBox(MSG_CONFIRMAR_PAGO, MSG_CAPTION,
          MB_ICONQUESTION or MB_DEFBUTTON2 or MB_YESNO) <> IDYES then Exit;

        if not ValidarDatosTurno then Exit;

        FFechaHoraEmision := TDate(deFechaPago.Date); //    Revision 16                                             //SS_1300_MCA_20150610

        FFechaActual := SysUtilsCN.NowBaseCN(DMConnections.BaseCAC);                                                //SS_1300_MCA_20150610
        ObtenerParametroGeneral(DMConnections.BaseCAC, 'DIAS_FECHA_MINIMA_PAGO_VENTANILLA', DiasFechaMinimaPago);   //SS_1300_MCA_20150610

        if FFechaHoraEmision > FFechaActual then begin                                                              //SS_1300_MCA_20150610
            MsgBoxBalloon(MSG_ERROR_PAGOS_FUTUROS, Caption, MB_ICONSTOP, deFechaPago);                              //SS_1300_MCA_20150610
            //deFechaPago.Date := FFechaActual;                                                                     //SS_1300_MCA_20150610
            exit;                                                                                                   //SS_1300_MCA_20150610
        end;                                                                                                        //SS_1300_MCA_20150610

        if FFechaHoraEmision <= FFechaActual - DiasFechaMinimaPago then begin                                       //SS_1300_MCA_20150610
            MsgBoxBalloon(Format(MSG_ERROR_PAGOS_PASADOS, [DiasFechaMinimaPago]), Caption, MB_ICONSTOP, deFechaPago); //SS_1300_MCA_20150610
            //deFechaPago.Date := FFechaActual;                                                                     //SS_1300_MCA_20150610
            exit;                                                                                                   //SS_1300_MCA_20150610
        end;                                                                                                        //SS_1300_MCA_20150610


        //REV 6
        //DMConnections.BaseCAC.BeginTrans;
        QueryExecute(DMConnections.BaseCAC, 'BEGIN TRAN spPagoAnticipado');
        try
            if GenerarRecibo(NumeroRecibo) then begin
                if RegistrarPago(FFormaPagoActual, NumeroRecibo, MensajeError) then begin
                    //REV 6 DMConnections.BaseCAC.CommitTrans;
                    QueryExecute(DMConnections.BaseCAC, 'COMMIT TRAN spPagoAnticipado');
                    FImprimirRecibo := True;
                    FNumeroRecibo := NumeroRecibo;
                    ModalResult := mrOk;
                end else begin
                    //REV 6 DMConnections.BaseCAC.RollbackTrans;
                    QueryExecute(DMConnections.BaseCAC, 'ROLLBACK TRAN spPagoAnticipado');
                    MsgBox(MensajeError, MSG_ERROR, MB_ICONERROR);
                    ModalResult := mrAbort;
                end;
            end;
        except
            on e: Exception do begin
                //REV 6 DMConnections.BaseCAC.RollbackTrans;
                QueryExecute(DMConnections.BaseCAC, 'ROLLBACK TRAN spPagoAnticipado');
                MsgBoxErr(MSG_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
            end;
        end;
    finally

// INICIO : TASK_003_JMA_20160405
        if ModalResult <> mrOk then
        begin

            try

                sp_ActualizarClienteMoroso.Parameters.ParamByName('@NumeroDocumento').Value := FRUT;
                sp_ActualizarClienteMoroso.Parameters.ParamByName('@CodigoRefinanciacion').Value := paNullable;
                sp_ActualizarClienteMoroso.Parameters.ParamByName('@Usuario').Value  := UsuarioSistema;
                sp_ActualizarClienteMoroso.Parameters.ParamByName('@ErrorDescription').Value  := paNullable;
                sp_ActualizarClienteMoroso.ExecProc;

            except
                on e: Exception do begin
                    MsgBoxErr('ERROR Revisando morosidad', e.Message, 'ERROR Revisando morosidad', MB_ICONERROR);
                end;
            end;
        end;
// TERMINO



        // SS_914_20100930
        EmptyKeyQueue;
        Application.ProcessMessages;
        if ModalResult <> mrOk then btnAceptar.Enabled := True;
        // Fin Rev. SS_914_20100930
    end;
    //FinRev.13---------------------------------------------------------------------
end;

{-----------------------------------------------------------------------------
  Procedure: TfrmPagoVentanilla.RegistrarPago
  Author:    mlopez
  Date:      10/08/2005
  Arguments: TipoPago: TTiposPago; NumeroRecibo: Integer
  Result:    Boolean

  Revision : 1
      Author : pdominguez
      Date   : 27/10/2009
      Description : SS 834
        - Se a�ade la Forma de Pago Pagar�, a los registros de pago.
-----------------------------------------------------------------------------}
function TfrmPagoVentanillaAnticipado.RegistrarPago(FormaPago: TFormaPago; NumeroRecibo: Integer; var MensajeError: String): Boolean;
begin
    Result := False;
    case FormaPago of
        tpEfectivo:			Result := GenerarPagoEfectivo(NumeroRecibo, MensajeError);
        tpTarjetaCredito:	Result := GenerarPagoTarjetaCredito(NumeroRecibo, MensajeError);
        tpTarjetaDebito:	Result := GenerarPagoTarjetaDebito(NumeroRecibo, MensajeError);
        tpDebitoCuenta:		Result := GenerarPagoDebitoCuenta(NumeroRecibo, MensajeError);
        tpCheque:			Result := GenerarPagoCheque(NumeroRecibo, MensajeError);
        tpValeVista:		Result := GenerarPagoValeVista(NumeroRecibo, MensajeError);
        tpPagare:           Result := GenerarPagoPagare(NumeroRecibo, MensajeError);    // Rev. 1 (SS 834)
    end;          
end;

procedure TfrmPagoVentanillaAnticipado.spbTarjetaCreditoCopiarDatosClienteClick(
  Sender: TObject);
begin
	edtTarjetaCreditoTitular.Text := FTitular;
end;

procedure TfrmPagoVentanillaAnticipado.spbTarjetaDebitoCopiarDatosClienteClick(
  Sender: TObject);
begin
	edtTarjetaDebitoTitular.Text := FTitular;
end;

{******************************** Function Header ******************************
Procedure Name: ValidarDatos
Author :
Date Created :
Parameters : FormaPago: TFormaPago
Return Value : Boolean
Description :

Revision : 1
    Author : pdominguez
    Date   : 28/09/2009
    Description : SS 834
        - Se a�ade la validaci�n del campo N�mero Repactaci�n para la forma de
        pago "Cheque".
        - Se a�ade la validaci�n del campo Banco para la forma de pago "Cheque".

Revision : 2
    Author : pdominguez
    Date   : 26/10/2009
    Description : SS 834
        - Se a�ade la validaci�n para las formas de pago tipo Pagar�.
*******************************************************************************}
function TfrmPagoVentanillaAnticipado.ValidarDatos(FormaPago: TFormaPago): Boolean;
resourcestring
    MSG_CAPTION_VALIDACION_TARJETA		= 'Datos de la Tarjeta de Cr�dito/D�bito';
    MSG_ERROR_AUTORIZACION				= 'Ingrese un c�digo de Autorizaci�n/Confirmaci�n';
    MSG_ERROR_CUPON						= 'Ingrese un n�mero de cup�n';
    MSG_ERROR_CUOTAS					= 'Ingrese un n�mero de cuotas';
    MSG_ERROR_TIPO_CUENTA				= 'Ingrese un n�mero de cuenta';
    MSG_ERROR_NOMBRE_TITULAR			= 'Ingrese el nombre del titular';
    MSG_ERROR_DOCUMENTO					= 'Debe ingresar el RUT';
    MSG_ERROR_RUT_INVALIDO				= 'El RUT ingresado es inv�lido';
    MSG_CAPTION_VALIDACION_CHEQUE		= 'Datos del pago con cheque';
    MSG_ERROR_NUMERO_CHEQUE				= 'Ingrese el n�mero de cheque';
    MSG_ERROR_IMPORTE_CHEQUE			= 'El importe del cheque debe ser superior o igual al monto a pagar.';
    MSG_ERROR_FECHA_CHEQUE_INVALIDA		= 'La fecha del cheque no es v�lida';
    MSG_ERROR_FECHA_CHEQUE_RANGO		= 'La fecha del cheque debe estar entre el %s y el %s ';
    MSG_CAPTION_VALIDACION_VALE_VISTA	= 'Datos del pago con vale vista';
    MSG_ERROR_NOMBRE_TOMADO_POR			= 'Ingrese el nombre de quien toma el vale vista';
    MSG_ERROR_NUMERO_VALE_VISTA			= 'Ingrese el n�mero de vale vista';
    MSG_ERROR_FECHA_VALE_VISTA_INVALIDA = 'La fecha del vale vista no es v�lida';
    MSG_ERROR_FECHA_VALE_VISTA_RANGO	= 'La fecha del vale vista debe estar entre el %s y el %s ';
    MSG_CAPTION_DEBITO_CUENTA_PAC		= 'Datos del D�bito de Cuenta PAC.';
    MSG_ERROR_BANCO						= 'Debe seleccionar un banco.';
    MSG_ERROR_NUMERO_CUENTA_BANCARIA	= 'Debe ingresar una cuenta bancaria.';
    MSG_CAPTION							= 'Cobro de Comprobante';
    MSG_IMPORTE_PAGADO_INFERIOR_A_IMPORTE_PAGAR = 'El importe pagado debe ser superior o igual al importe a pagar.';
    MSG_IMPORTE_RECIBIDO_INFERIOR_A_IMPORTE_PAGAR = 'El importe recibibo debe ser superior o igual al importe a pagar.';
    //SS 809
    MSG_DEBE_TARJETA					= 'Debe seleccionar una tarjeta';
    MSG_ERROR_REPACTACION_DUPLICADO = 'N�mero de Repactaci�n duplicado';   // Rev. 1 (SS 834)
    // Rev. 2 (SS 834)
    MSG_CAPTION_VALIDACION_PAGARE   = 'Datos del pago con pagar�';
    MSG_ERROR_BANCO_PAGARE          = 'Debe seleccionar un banco';
    MSG_ERROR_NUMERO_PAGARE         = 'Ingrese el n�mero de pagar�';
    MSG_ERROR_FECHA_PAGARE_INVALIDA = 'La fecha del pagar� no es v�lida';
    MSG_ERROR_FECHA_PAGARE_RANGO    = 'La fecha del pagar� debe estar entre el %s y el %s ';
    MSG_ERROR_REPACTACION_REQUERIDO = 'Ingrese un N�mero de Repactaci�n';
    // Fin Rev. 2 (SS 834)
begin
    FImportePagado := ne_ImportePagado.ValueInt;

{//.    if (FComprobantesCobrar.Count = 1) and
           (not ValidateControls([ne_ImportePagado],
                [(FImportePagado >= neImporte.ValueInt)],
                MSG_CAPTION,
                [MSG_IMPORTE_PAGADO_INFERIOR_A_IMPORTE_PAGAR])) then begin
        Result := False;
        Exit;
    end; // if}

    case FormaPago of
        tpEfectivo: begin
            Result := ValidateControls([nePagaCon], [ne_ImportePagado.ValueInt <= nePagacon.ValueInt], MSG_CAPTION, [MSG_IMPORTE_RECIBIDO_INFERIOR_A_IMPORTE_PAGAR])
        end;
        tpCheque :
            begin
                Result := ValidateControls([
                  txtNombreTitular, txtDocumento, txtDocumento, cbbancos, txtNumeroCheque,
                  deFechaCheque, deFechaCheque, edtRepactacionCheque], [ // Rev. 1 (SS 834)
                  (txtNombreTitular.Text <> ''), (txtDocumento.text <> ''),
                  (ValidarRUT(DMConnections.BaseCAC,trim(txtDocumento.text))),
                  (cbbancos.Value <> 0), // Rev. 1 (SS 834)
                  (txtNumeroCheque.text <> ''),
                  (IsValidDate(DateTimeToStr(deFechaCheque.Date))),
                  ((deFechaCheque.Date >= FFechaMinimaCheque) AND (deFechaCheque.Date < FFechaMaximaCheque)),
                             (QueryGetValue(
                                    DMConnections.BaseCAC,
                                    Format('select dbo.ExisteRepactacionNumeroFormaPago(''%s'', %d)',[edtRepactacionCheque.Text, FCodigoFormaPago])) = 'False')],  // Rev. 1 (SS 834)
                  MSG_CAPTION_VALIDACION_CHEQUE, [
                  MSG_ERROR_NOMBRE_TITULAR, MSG_ERROR_DOCUMENTO, MSG_ERROR_RUT_INVALIDO,
                  MSG_ERROR_BANCO,  // Rev. 1 (SS 834)
                  MSG_ERROR_NUMERO_CHEQUE,
                  MSG_ERROR_FECHA_CHEQUE_INVALIDA,
                  Format(MSG_ERROR_FECHA_CHEQUE_RANGO, [FormatDateTime('dd/mm/yyyy',FFechaMinimaCheque), FormatDateTime('dd/mm/yyyy',FFechaMaximaCheque-1)]),
                  MSG_ERROR_REPACTACION_DUPLICADO]); // Rev. 1 (SS 834)
            end;
        tpValeVista :
            begin
                Result := ValidateControls([
                  txt_TomadoPorVV, txt_RUTVV, txt_RUTVV, txt_NroVV, de_FechaVV, de_FechaVV], [
                  (txt_TomadoPorVV.Text <> ''),(txt_RUTVV.Text <> ''), (ValidarRUT(DMConnections.BaseCAC,trim(txt_RUTVV.Text))),
                  (txt_NroVV.Text <> ''), (IsValidDate(DateTimeToStr(de_FechaVV.Date))),
                  ((de_FechaVV.Date >= FFechaMinimaCheque) AND (de_FechaVV.Date < FFechaMaximaCheque))],
                  MSG_CAPTION_VALIDACION_VALE_VISTA, [
                  MSG_ERROR_NOMBRE_TITULAR, MSG_ERROR_DOCUMENTO, MSG_ERROR_RUT_INVALIDO,
                  MSG_ERROR_NUMERO_VALE_VISTA, MSG_ERROR_FECHA_VALE_VISTA_INVALIDA,
                  Format(MSG_ERROR_FECHA_VALE_VISTA_RANGO, [FormatDateTime('dd/mm/yyyy',FFechaMinimaCheque), FormatDateTime('dd/mm/yyyy',FFechaMaximaCheque-1)])]);
            end;
        tpTarjetaDebito:
            begin
            	{REV.8 SS 809
                Result := ValidateControls([edAutorizacion, edCupon, edCuotas],
                  [(edAutorizacion.text <> ''), (edCupon.text <> ''), (edCupon.text <> '')],
                    MSG_CAPTION_VALIDACION_TARJETA,[MSG_ERROR_AUTORIZACION, MSG_ERROR_CUPON, MSG_ERROR_CUOTAS]);}

                Result := ValidateControls(
                            	[	edtTarjetaDebitoTitular,
                                	vcbTarjetaDebitoBancos,
                                    edtTarjetaDebitoNroConfirmacion ],
                                [	edtTarjetaDebitoTitular.Text <> '',
                                	vcbTarjetaDebitoBancos.ItemIndex > 0,
                                    edtTarjetaDebitoNroConfirmacion.Text <> '' ],
                                Caption,
                                [	MSG_ERROR_NOMBRE_TITULAR,
                                	MSG_ERROR_BANCO,
                                    MSG_ERROR_AUTORIZACION ]
                			);


            end;

        tpTarjetaCredito:
            begin
//                Revision 1 Al unificar la pantalla se unifica la validacion de datos
//                Result := True;
//                if FCodigoCanalPago <> CONST_CANAL_PAGO_LIDER then
{REV.8 SS 809
                begin
                    Result := ValidateControls([edAutorizacion,
                                edCupon,
                                edCuotas],
                                [(edAutorizacion.text <> ''),
                                (edCupon.text <> ''),
                                (edCuotas.text <> '')],  //Revision 1 decia edCupon de nuevo
                                MSG_CAPTION_VALIDACION_TARJETA,
                                [MSG_ERROR_AUTORIZACION,
                                MSG_ERROR_CUPON,
                                MSG_ERROR_CUOTAS]);
                end;}
                Result := ValidateControls(
                            	[	edtTarjetaCreditoTitular,
                                	vcbTarjetaCreditoBancos,
                                    vcbTarjetaCreditoTarjetas,
                                    edtTarjetaCreditoNroConfirmacion,
                                    neTarjetaCreditoCuotas   ],
                                [	edtTarjetaCreditoTitular.Text <> '',
                                	vcbTarjetaCreditoBancos.ItemIndex > 0,
                                    vcbTarjetaCreditoTarjetas.ItemIndex > 0,
                                    edtTarjetaCreditoNroConfirmacion.Text <> '',
                                    neTarjetaCreditoCuotas.Value > 0 ],
                                Caption,
                                [	MSG_ERROR_NOMBRE_TITULAR,
                                	MSG_ERROR_BANCO,
                                    MSG_DEBE_TARJETA,
                                    MSG_ERROR_AUTORIZACION,
                                    MSG_ERROR_CUOTAS ]
                			);

            end;

        tpDebitoCuenta:
            begin
                if FCodigoCanalPago = CONST_CANAL_PAGO_PAC then begin
                    Result := ValidateControls([cb_BancosPAC,
                                txt_NroCuentaBancariaPAC],
                                [(cb_BancosPAC.Itemindex >= 0),
                                Trim(txt_NroCuentaBancariaPAC.Text) <> EmptyStr],
                                MSG_CAPTION_DEBITO_CUENTA_PAC,
                                [MSG_ERROR_BANCO,
                                MSG_ERROR_NUMERO_CUENTA_BANCARIA]);
                end else begin
                    Result := True;
                end;
            end;
        // Rev. 2 (SS 834)
        tpPagare:
            begin
                Result :=
                    ValidateControls(
                        [txtPagareNombreTitular,
                         txtPagareDocumento,
                         txtPagareDocumento,
                         cbPagareBancos,
                         txtPagareNumero,
                         dePagareFecha,
                         dePagareFecha,
                         edtPagareRepactacion,
                         edtPagareRepactacion],
                        [(txtPagareNombreTitular.Text <> EmptyStr),
                         (txtPagareDocumento.Text <> EmptyStr),
                         (ValidarRUT(DMConnections.BaseCAC, Trim(txtPagareDocumento.Text))),
                         cbPagareBancos.Value <> 0,
                         (txtPagareNumero.Text <> EmptyStr),
                         (IsValidDate(DateTimeToStr(dePagareFecha.Date))),
                         ((dePagareFecha.Date >= FFechaMinimaPagare) AND (dePagareFecha.Date < FFechaMaximaPagare)),
                         (Trim(edtPagareRepactacion.Text) <> ''),
                         (QueryGetValue(
                                DMConnections.BaseCAC,
                                Format('select dbo.ExisteRepactacionNumeroFormaPago(''%s'', %d)',[edtPagareRepactacion.Text, FCodigoFormaPago])) = 'False')],
                         MSG_CAPTION_VALIDACION_PAGARE,
                         [MSG_ERROR_NOMBRE_TITULAR,
                          MSG_ERROR_DOCUMENTO,
                          MSG_ERROR_RUT_INVALIDO,
                          MSG_ERROR_BANCO_PAGARE,
                          MSG_ERROR_NUMERO_PAGARE,
                          MSG_ERROR_FECHA_PAGARE_INVALIDA,
                          Format(MSG_ERROR_FECHA_PAGARE_RANGO, [FormatDateTime('dd/mm/yyyy', FFechaMinimaPagare),
                          FormatDateTime('dd/mm/yyyy', FFechaMaximaPagare - 1)]),
                          MSG_ERROR_REPACTACION_REQUERIDO,
                          MSG_ERROR_REPACTACION_DUPLICADO]);

            end;
        // Fin Rev. 2 (SS 834)
        else Result := False;
    end;
end;

{-----------------------------------------------------------------------------
  Procedure: ImprimioRecibo
  Author:    mlopez
  Date:      10/08/2005
  Description: Devuelve True si imprimi� el recibo
  Arguments:
  Result:    Boolean
-----------------------------------------------------------------------------}
function TfrmPagoVentanillaAnticipado.ImprimirRecibo : boolean;
begin
	Result := FImprimirRecibo;
end;

{-----------------------------------------------------------------------------
  Function Name: NumeroRecibo
  Author:    mlopez
  Date Created: 10/08/2005
  Description: Devuelve el Numero de Recibo generado...
  Parameters: None
  Return Value: INt64
-----------------------------------------------------------------------------}
function TfrmPagoVentanillaAnticipado.NumeroRecibo : INt64;
begin
	result := FNumeroRecibo;
end;

procedure TfrmPagoVentanillaAnticipado.btnDatosClienteChequeClick(Sender: TObject);
begin
    txtNombreTitular.Text := FTitular;
	txtDocumento.Text := FRut;
end;

{******************************* Procedure Header ******************************
Procedure Name: btnDatosClientePagareClick
Author :
Date Created :
Parameters : Sender: TObject
Description :

Revision : 1
    Author : pdominguez
    Date   : 26/10/2009
    Description : SS 834
        - Asigna los datos del Titular para el registro del Pago.
*******************************************************************************}
procedure TfrmPagoVentanillaAnticipado.btnDatosClientePagareClick(Sender: TObject);
begin
    txtPagareNombreTitular.Text := FTitular;
  	txtPagareDocumento.Text     := FRut;
end;

{******************************** Function Header ******************************
Function Name: deFechaPagoChange
Author :
Date Created :
Description :
Parameters : Sender: TObject
Return Value : None

Revision : 1
    Author : pdominguez
    Date   : 12/08/2009
    Description : SS 809
            - Se inhabilitan las referencias al objeto eliminado cb_FormasPago.
*******************************************************************************}
procedure TfrmPagoVentanillaAnticipado.deFechaPagoChange(Sender: TObject);
var
    MaxDias,
    MinDias,
    FCodigoFormaPago    : integer;
    FFechaPago: TDateTime;
begin
    if Sender <> ActiveControl then exit;
//    FCodigoFormaPago := Integer(cb_FormasPago.Value);
    if FCodigoFormaPago <> 4 then Exit;

    // Recalculo las fechas minimas y maximas para validar un cheque!
    FFechaPago := Trunc(deFechaPago.Date);
    ObtenerParametroGeneral(DMConnections.BaseCAC,'DIAS_FECHA_MINIMA_CHEQUE',MinDias);
    ObtenerParametroGeneral(DMConnections.BaseCAC,'DIAS_FECHA_MAXIMA_CHEQUE',MaxDias);
    FFechaMaximaCheque := FFechaPago + MaxDias + 1;
    FFechaMinimaCheque := FFechaPago - MinDias;
end;

{******************************** Procedure Header *****************************
Procedure Name: edtCodigoFormaPagoChange
Author : pdominguez
Date Created : 12/08/2009
Parameters : Sender: TObject
Description : SS 809
    - Valida la forma de pago introducida por el usuario.

Revision : 1
    Author : pdominguez
    Date   : 17/09/2009
    Description : SS 809
        - Se a�ade la asignaci�n del valor a los parametros @CodigoPantalla,
        @CodigoBanco y @CodigoTarjeta al SP spValidarFormaDePagoEnCanal.
        - Se a�ade la asignaci�n a las variables FCodigoPantalla, FCodigoBanco y
        FCodigoTarjeta de los valores obtenidos por el SP spValidarFormaDePagoEnCanal.
*******************************************************************************}
procedure TfrmPagoVentanillaAnticipado.edtCodigoFormaPagoChange(Sender: TObject);
    resourcestring
        MSG_FORMA_DE_PAGO_ERRONEA = 'La Forma de Pago NO es V�lida para el Canal seleccionado.';
        MSG_FORMA_DE_PAGO_NO_EXISTE = 'La Forma de Pago NO Existe.';
        MSG_CAPTION_VALIDACION = 'Validar Forma de Pago';
        MSG_ERROR_VALIDACION = 'Se produjo un Error al Validar Forma de Pago';
        MSG_ERROR_VALIDACION_ERROR = 'Error: %s';
    const
        SQLValidarFormaDePagoEnCanal = 'SELECT dbo.ValidarFormaDePagoEnCanal(%s, ''%s'')';

    {******************************** Procedure Header *****************************
    Procedure Name: ResetearControlesFormaPago
    Author : pdominguez
    Date Created : 12/08/2009
    Parameters : None
    Description : SS 809
        - Reseta los valores e indicadores para la forma de pago cuando no est�
        validada.
        - Utilizamos el Tag como Flag para saber si est� validada o no
        la forma de pago.
    *******************************************************************************}
    procedure ResetearControlesFormaPago;
    begin
        FCodigoFormaPago       := 0;
        nbOpciones.Visible     := False;
        edtCodigoFormaPago.Tag := 0;
    end;
begin
    case Length(edtCodigoFormaPago.Text) of
        2: try
            with spValidarFormaDePagoEnCanal do begin
                if Active then Close;

                Parameters.ParamByName('@CodigoTipoMedioPago').Value := cb_CanalesDePago.Value;
                Parameters.ParamByName('@CodigoEntradaUsuario').Value := edtCodigoFormaPago.Text;
                // Asignamos 1 ya que el SP ha de validar si la forma de pago es admitida en
                // el Pago Anticipado.
                Parameters.ParamByName('@ValidarPagoAnticipado').Value := 1;
                Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;					//SS_1410_MCA_20151027
                Parameters.ParamByName('@CodigoSistema').Value := SistemaActual;					//SS_1410_MCA_20151027

                Parameters.ParamByName('@CodigoFormaDePago').Value := Null;
                Parameters.ParamByName('@DescripcionFormaDePago').Value := Null;
                Parameters.ParamByName('@EsValidaEnCanal').Value := Null;
                Parameters.ParamByName('@AceptaPagoParcial').Value := Null;
                Parameters.ParamByName('@CodigoPantalla').Value := Null; // Rev. 1 (SS 809)
                Parameters.ParamByName('@CodigoBanco').Value := Null;    // Rev. 1 (SS 809)
                Parameters.ParamByName('@CodigoTarjeta').Value := Null;  // Rev. 1 (SS 809)
                ExecProc;

                edtDescFormaPago.Text := Parameters.ParamByName('@DescripcionFormaDePago').Value;

                if Parameters.ParamByName('@EsValidaEnCanal').Value = 0 then begin
                    ResetearControlesFormaPago;
                    if Parameters.ParamByName('@CodigoFormaDePago').Value = 0 then
                        MsgBox(MSG_FORMA_DE_PAGO_NO_EXISTE, MSG_CAPTION_VALIDACION, MB_ICONERROR)
                    else MsgBox(MSG_FORMA_DE_PAGO_ERRONEA, MSG_CAPTION_VALIDACION,MB_ICONERROR);
                end else begin
                    FCodigoFormaPago := Parameters.ParamByName('@CodigoFormaDePago').Value;
                    // Rev. 1 (SS 809)
                    FCodigoPantalla := Parameters.ParamByName('@CodigoPantalla').Value;
                    FCodigoBanco    := Parameters.ParamByName('@CodigoBanco').Value;
                    FCodigoTarjeta  := Parameters.ParamByName('@CodigoTarjeta').Value;
                    // Fin Rev. 1 (SS 809)
                    nbOpciones.Visible := True;
                    edtCodigoFormaPago.Tag := 1;
                    cb_FormasPagoChange(edtCodigoFormaPago);
                    ne_ImportePagado.SetFocus;
                end;
            end;
        except
            on E:Exception do begin
                ResetearControlesFormaPago;
                MsgBoxErr(
                    MSG_ERROR_VALIDACION,
                    Format(MSG_ERROR_VALIDACION_ERROR,[E.Message]),
                    MSG_CAPTION_VALIDACION,
                    MB_ICONERROR);
            end;
        end;
    else
        ResetearControlesFormaPago;
        edtDescFormaPago.Text  := '';
    end;
end;

procedure TfrmPagoVentanillaAnticipado.cb_BancosVVChange(Sender: TObject);
begin
    FCodigoBanco := Integer(cb_BancosVV.Value);
end;

procedure TfrmPagoVentanillaAnticipado.btn_DatosClienteValeVistaClick(Sender: TObject);
begin
    txt_TomadoPorVV.Text := FTitular;
	txt_RUTVV.Text := FRut;
end;

function TfrmPagoVentanillaAnticipado.ValidarDatosTurno: Boolean;
var
    DatosTurno: TDatosTurno;
begin
    (* Verificar que el turno que se est� por utilizar est� abierto y sea del
    usuario logueado. *)
    if VerTurnoAbierto(DMConnections.BaseCAC, FPuntoEntrega, FPuntoVenta, UsuarioSistema, DatosTurno) then begin
        GNumeroTurno := DatosTurno.NumeroTurno;
        Result := True;
    end else begin
        if (DatosTurno.NumeroTurno <> -1)
                and ((DatosTurno.Estado = TURNO_ESTADO_ABIERTO)
                and (Trim(DatosTurno.CodigoUsuario) <> (UpperCase(UsuarioSistema)))) then begin
            MsgBox(Format(MSG_ERROR_TURNO_ABIERTO_OTRO_USUARIO, [DatosTurno.CodigoUsuario]),
                Self.Caption, MB_ICONSTOP);
        end else begin
            MsgBox(MSG_ERROR_TURNO_CERRADO, Self.Caption, MB_ICONSTOP);
        end;
        Result := False;
    end;

end;


{******************************** Function Header ******************************
Function Name: cb_CanalesDePagoChange
Author :
Date Created :
Description :
Parameters : Sender: TObject
Return Value : None

Revision : 1
    Author : pdominguez
    Date   : 12/08/2009
    Description : SS 809
        - Se inicializa la forma de pago al cambiar el canal.
        - Se inhabilitan las referencias al objeto eliminado cb_FormasPago.
        - Se pasa el Foco al objeto edtCodigoFormaPago.
*******************************************************************************}
procedure TfrmPagoVentanillaAnticipado.cb_CanalesDePagoChange(Sender: TObject);
begin
    FCodigoCanalPago := Integer(cb_CanalesDePago.Value);
    edtCodigoFormaPago.Text := ''; // SS 809
    ne_ImportePagado.Value := FImportePagado;

    (* Cargar el combo de Medio de Pago. *)
//    cb_FormasPago.Items.Clear;
{    if cb_CanalesDePago.ItemIndex >= 0 then begin
        CargarFormasPago(DMConnections.BaseCAC, cb_FormasPago, cb_CanalesDePago.Value);
        FiltrarFormasDePago((cbbancos.Items.Count > 0), (FNumeroPOS > 0));
        cb_FormasPagoChange(cb_CanalesDePago);
    end;}

    if Self.Visible then edtCodigoFormaPago.SetFocus;
end;

{-----------------------------------------------------------------------------
  Procedure: TfrmPagoVentanilla.MostrarDatosPACComprobante
  Author:    mlopez
  Date:      10/08/2005
  Arguments: NumeroComprobante: Integer; TipoComprobante: AnsiString
  Result:    None
  Description: Muestra los datos PAC de un comprobante.
-----------------------------------------------------------------------------}
procedure TfrmPagoVentanillaAnticipado.MostrarDatosPACConvenio;
var
    sp: TADOStoredProc;
begin
    cb_BancosPAC.Value := -1;
    txt_NroCuentaBancariaPAC.Clear;
    txt_SucursalPAC.Clear;
    txt_TitularPAC.Clear;

    sp := TADOStoredProc.Create(nil);
    try
        sp.Connection := DMConnections.BaseCAC;
        sp.ProcedureName := 'ObtenerDatosPACConvenio';
        sp.Parameters.Refresh;
        sp.Parameters.ParamByName('@CodigoConvenio').Value := FCodigoConvenio;
        sp.Open;
        if sp.RecordCount > 0 then begin
            cb_BancosPAC.Value := sp.FieldByName('CodigoBanco').AsInteger;
            txt_NroCuentaBancariaPAC.Text := sp.FieldByName('NroCuentaBancaria').AsString;
            txt_SucursalPAC.Text := sp.FieldByName('Sucursal').AsString;
            txt_TitularPAC.Text := EmptyStr;
        end;
    finally
        sp.Close;
        sp.Free;
    end;
end;


{procedure TfrmPagoVentanillaAnticipado.CerrarRegistroPagoComprobante(TipoComprobante: AnsiString;
    NumeroComprobante: Int64; FechaPago: TDateTime);
resourcestring
    MSG_ERROR_CIERRE = 'Error completando el registro del pago';
begin
{   // Por �ltimo, cerramos el comprobante
   try
        with CerrarPagoComprobante.Parameters do begin
            ParamByName('@TipoComprobante').Value 	:= TipoComprobante;
            ParamByName('@NumeroComprobante').Value := NumeroComprobante;
            ParamByName('@FechaHora').Value		    := FechaPago;
         end;
         CerrarPagoComprobante.ExecProc;
    except
        on e: Exception do begin
            raise Exception.Create(MSG_ERROR_CIERRE + CRLF + e.Message);
        end;
    end;
end;}



{-----------------------------------------------------------------------------
  Function Name: nePagaconChange
  Author:    mlopez
  Date Created: 10/08/2005
  Description:  Calcula el vuelto
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmPagoVentanillaAnticipado.nePagaconChange(Sender: TObject);
//TASK_011_GLE_20161103 - agrega variable para calcular vuelto
var
vuelto : Double;
begin
	if ( nePagacon.Value >= ne_ImportePagado.Value ) then begin
       	vuelto := nePagacon.Value - ne_ImportePagado.Value;    //TASK_011_GLE_20161103 - variable calcula vuelto
        lblCambio.Caption := FloatToStr(vuelto);               //TASK_011_GLE_20161103 - vuelto se muestra en Label
    end
    else begin
    	lblCambio.Caption := '';                               //TASK_011_GLE_20161103 - vac�a label de vuelto
    end;
end;

procedure TfrmPagoVentanillaAnticipado.neImporteKeyPress(Sender: TObject;
  var Key: Char);
begin
    if Key = '-' then Key := #0;
end;

{----------------------------------------------------------------------------
  Procedure: TfrmPagoVentanilla.GenerarPagoEfectivo
  Author:    mlopez
  Date:      10/08/2005
  Description: Registra el pago de comprobante a traves del medio de pago "Efectivo"
  Arguments: NumeroRecibo: Integer; var MensajeError: String
  Result:    Boolean
-----------------------------------------------------------------------------}
function TfrmPagoVentanillaAnticipado.GenerarPagoEfectivo(NumeroRecibo: Integer; var MensajeError: String): Boolean;
resourcestring
    MSG_PAGO_EFECTIVO = 'Pago en efectivo';
    MSG_ERROR_PAGO_EFECTIVO = 'Error registrando un pago en efectivo';
    MSG_ERROR_ID_DETALLE_IS_NULL = 'No se pudo registrar el Detalle del Pago';
    MSG_ERROR_NUMERO_PAGO_IS_NULL = 'No se pudo registrar del Pago del Comprobante'#10#13'%s %d';
begin
    Result := False;
    MensajeError := '';
    with sp_RegistrarDetallePagoComprobante.Parameters do begin
        ParamByName('@CodigoTipoMedioPago').Value := FCodigoCanalPago;
        ParamByName('@CodigoFormaPago').Value := FCodigoFormaPago;
    end;
    sp_RegistrarDetallePagoComprobante.ExecProc;

    if sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@IDDetallePagoComprobante').Value = Null then begin
        MensajeError := MSG_ERROR_ID_DETALLE_IS_NULL;
        Exit;
    end;

    try
        with sp_RegistrarPagoAnticipado.Parameters do begin
            ParamByName('@FechaHora').Value := deFechaPago.Date;
            ParamByName('@CodigoConvenio').Value := FCodigoConvenio;
            ParamByName('@Importe').Value := FImportePagado * 100;
            ParamByName('@NumeroRecibo').Value := NumeroRecibo;
            ParamByName('@NumeroTurno').Value := GNumeroTurno;
            ParamByName('@Usuario').Value := UsuarioSistema;
            ParamByName('@IDDetallePagoComprobante').Value := sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@IDDetallePagoComprobante').Value;
            ParamByName('@NoCalcularIntereses').Value := FNoCalcularIntereses;
        end;
        sp_RegistrarPagoAnticipado.ExecProc;
    except
        on e: Exception do begin
            raise Exception.Create(MSG_ERROR_PAGO_EFECTIVO + CRLF + e.Message);
        end;
    end;
    Result := True;
end;

{-----------------------------------------------------------------------------
  Procedure: TfrmPagoVentanilla.GenerarPagoTarjetaCredito
  Author:    mlopez
  Date:      10/08/2005
  Description: Registra el pago de comprobante a traves del medio de pago "Tarjeta de Credito"
  Arguments: NumeroRecibo: Integer; var MensajeError: String
  Result:    Boolean
-----------------------------------------------------------------------------}
function TfrmPagoVentanillaAnticipado.GenerarPagoTarjetaCredito(NumeroRecibo: Integer; var MensajeError: String): Boolean;
resourcestring
    MSG_PAGO_TARJETA_CREDITO  = 'Pago con tarjeta de credito';
    MSG_ERROR_PAGO_TARJETA = 'Error registrando un pago con tarjeta de cr�dito/d�bito';
    MSG_ERROR_ID_DETALLE_IS_NULL = 'No se pudo registrar el Detalle del Pago';
    MSG_ERROR_NUMERO_PAGO_IS_NULL = 'No se pudo registrar del Pago del Comprobante'#10#13'%s %d';
begin
    Result := False;
    MensajeError := '';
    with sp_RegistrarDetallePagoComprobante.Parameters do begin
        ParamByName('@CodigoTipoMedioPago').Value := FCodigoCanalPago;
        ParamByName('@CodigoFormaPago').Value := FCodigoFormaPago;

//        Revision 1 se unifica la forma de pago con tarjeta
//        if FCodigoCanalPago = CONST_CANAL_PAGO_LIDER then begin
//            (* Si el Canal de Pago es Lider y la Forma de Pago es Tarjeta,
//            setear la Forma de Pago como Efectivo. *)
//            ParamByName('@CodigoFormaPago').Value                   := CONST_FORMA_PAGO_TARJETA_CREDITO;
//            ParamByName('@PAT_CodigoTipoTarjetaCredito').Value      := iif(cb_TipoTarjetaCreditoPAT.Value = 0, Null, cb_TipoTarjetaCreditoPAT.Value);
//            ParamByName('@PAT_NumeroTarjetaCredito').Value          := iif(Trim(txt_NumeroTarjetaCreditoPAT.Text) = EmptyStr, Null, Trim(txt_NumeroTarjetaCreditoPAT.Text));
//            ParamByName('@PAT_FechaVencimiento').Value              := iif(txt_FechaVencimiento.Date <= 0, Null, txt_FechaVencimiento.Date);
//            ParamByName('@PAT_CodigoEmisorTarjetaCredito').Value    := iif(cb_EmisorTarjetaCreditoPAT.Value = 0, Null, cb_EmisorTarjetaCreditoPAT.Value);
//        end else

  { REV.8  SS 809
        begin
            ParamByName('@Cupon').Value 				:= IStr0(edCupon.ValueInt);
            ParamByName('@Autorizacion').Value    := Trim(UpperCase(edAutorizacion.Text));
            ParamByName('@Cuotas').Value                := edCuotas.Value;
            if (FNumeroPOS > 0) then begin
                ParamByName('@NumeroPOS').Value := FNumeroPOS;
            end else begin
                ParamByName('@NumeroPOS').Value := Null;
            end;
        end;     }

        ParamByName('@Titular').Value           := Trim(edtTarjetaCreditoTitular.Text);
        ParamByName('@CodigoBancoEmisor').Value := vcbTarjetaCreditoBancos.Value;
        ParamByName('@CodigoTipoTarjeta').Value := vcbTarjetaCreditoTarjetas.Value;
        ParamByName('@Autorizacion').Value      := Trim(UpperCase(edtTarjetaCreditoNroConfirmacion.Text));
        ParamByName('@Cuotas').Value            := neTarjetaCreditoCuotas.Value;
        if (FNumeroPOS > 0) then ParamByName('@NumeroPOS').Value := FNumeroPOS
        else ParamByName('@NumeroPOS').Value := Null;
    end;
    sp_RegistrarDetallePagoComprobante.ExecProc;


    if sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@IDDetallePagoComprobante').Value = Null then begin
        MensajeError := MSG_ERROR_ID_DETALLE_IS_NULL;
        Exit;
    end;

    try
        with sp_RegistrarPagoAnticipado.Parameters do begin
//            ParamByName('@TipoComprobante').Value := Comprobante.TipoComprobante;
//            ParamByName('@NumeroComprobante').Value := Comprobante.NumeroComprobante;
            ParamByName('@FechaHora').Value := deFechaPago.Date;
            ParamByName('@CodigoConvenio').Value := FCodigoConvenio;
            ParamByName('@Importe').Value := FImportePagado * 100;
            ParamByName('@NumeroRecibo').Value := NumeroRecibo;
            ParamByName('@NumeroTurno').Value := GNumeroTurno;
            ParamByName('@Usuario').Value := UsuarioSistema;
            ParamByName('@IDDetallePagoComprobante').Value := sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@IDDetallePagoComprobante').Value;
            ParamByName('@NoCalcularIntereses').Value := FNoCalcularIntereses;
            //            ParamByName('@CodigoConceptoPago').Value := ObtenerCodigoConceptoPago(DMConnections.BaseCAC, FCodigoCanalPago);
//            ParamByName('@DescripcionPago').Value := MSG_PAGO_TARJETA_CREDITO;
        end;
        sp_RegistrarPagoAnticipado.ExecProc;
    except
        on e: Exception do begin
            raise Exception.Create(MSG_ERROR_PAGO_TARJETA + CRLF + e.Message);
        end;
    end;
    Result := True;
end;

{-----------------------------------------------------------------------------
  Procedure: TfrmPagoVentanilla.GenerarPagoTarjetaDebito
  Author:    mlopez
  Date:      10/08/2005
  Description: Registra el pago de comprobante a traves del medio de pago "Tarjeta de Debito"
  Arguments: NumeroRecibo: Integer; var MensajeError: String
  Result:    Boolean
-----------------------------------------------------------------------------}
function TfrmPagoVentanillaAnticipado.GenerarPagoTarjetaDebito(NumeroRecibo: Integer; var MensajeError: String): Boolean;
resourcestring
    MSG_PAGO_TARJETA_DEBITO  = 'Pago con tarjeta de d�bito';
    MSG_ERROR_PAGO_TARJETA = 'Error registrando un pago con tarjeta de cr�dito/d�bito';
    MSG_ERROR_ID_DETALLE_IS_NULL = 'No se pudo registrar el Detalle del Pago';
    MSG_ERROR_NUMERO_PAGO_IS_NULL = 'No se pudo registrar del Pago del Comprobante'#10#13'%s %d';
begin
    Result := False;
    MensajeError := '';
    with sp_RegistrarDetallePagoComprobante.Parameters do begin
        {REV.8  SS 809
        ParamByName('@Cupon').Value := IStr0(edCupon.ValueInt);
        ParamByName('@Autorizacion').Value := Trim(UpperCase(edAutorizacion.Text));
        ParamByName('@Cuotas').Value := edCuotas.Value;   }

        ParamByName('@CodigoTipoMedioPago').Value					:= FCodigoCanalPago;
        ParamByName('@CodigoFormaPago').Value						:= FCodigoFormaPago;
        ParamByName('@Titular').Value           					:= Trim(edtTarjetaDebitoTitular.Text);
        ParamByName('@CodigoBancoEmisor').Value						:= vcbTarjetaDebitoBancos.Value;
        ParamByName('@Autorizacion').Value							:= Trim(UpperCase(edtTarjetaDebitoNroConfirmacion.Text));
        if (FNumeroPOS > 0) then ParamByName('@NumeroPOS').Value	:= FNumeroPOS
        else ParamByName('@NumeroPOS').Value 						:= Null;
    end;
    sp_RegistrarDetallePagoComprobante.ExecProc;

    if sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@IDDetallePagoComprobante').Value = Null then begin
        MensajeError := MSG_ERROR_ID_DETALLE_IS_NULL;
        Exit;
    end;

    try
        with sp_RegistrarPagoAnticipado.Parameters do begin
//            ParamByName('@TipoComprobante').Value := Comprobante.TipoComprobante;
//            ParamByName('@NumeroComprobante').Value := Comprobante.NumeroComprobante;
            ParamByName('@FechaHora').Value := deFechaPago.Date;
            ParamByName('@CodigoConvenio').Value := FCodigoConvenio;
            ParamByName('@Importe').Value := FImportePagado * 100;
            ParamByName('@NumeroRecibo').Value := NumeroRecibo;
//            ParamByName('@CodigoConceptoPago').Value := ObtenerCodigoConceptoPago(DMConnections.BaseCAC, FCodigoCanalPago);
//            ParamByName('@DescripcionPago').Value := MSG_PAGO_TARJETA_DEBITO;
            ParamByName('@NumeroTurno').Value := GNumeroTurno;
            ParamByName('@Usuario').Value := UsuarioSistema;
            ParamByName('@IDDetallePagoComprobante').Value := sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@IDDetallePagoComprobante').Value;
            ParamByName('@NoCalcularIntereses').Value := FNoCalcularIntereses;
        end;
        sp_RegistrarPagoAnticipado.ExecProc;
    except
        on e: Exception do begin
            raise Exception.Create(MSG_ERROR_PAGO_TARJETA + CRLF + e.Message);
        end;
    end;
    Result := True;
end;

{-----------------------------------------------------------------------------
  Procedure: TfrmPagoVentanilla.GenerarPagoDebitoCuenta
  Author:    mlopez
  Date:      10/08/2005
  Description: Registra el pago de comprobante a traves del medio de pago "Cuenta de Debito"
  Arguments: NumeroRecibo: Integer; var MensajeError: String
  Result:    Boolean
-----------------------------------------------------------------------------}
function TfrmPagoVentanillaAnticipado.GenerarPagoDebitoCuenta(NumeroRecibo: Integer; var MensajeError: String): Boolean;
resourcestring
    MSG_PAGO_DEBITO_CUENTA = 'Pago con d�bito cuenta';
    MSG_ERROR_ID_DETALLE_IS_NULL = 'No se pudo registrar el Detalle del Pago';
    MSG_ERROR_NUMERO_PAGO_IS_NULL = 'No se pudo registrar del Pago del Comprobante'#10#13'%s %d';
begin
    Result := False;
    MensajeError := '';
    with sp_RegistrarDetallePagoComprobante.Parameters do begin
        ParamByName('@CodigoTipoMedioPago').Value := FCodigoCanalPago;
        ParamByName('@CodigoFormaPago').Value := FCodigoFormaPago;
        if FCodigoCanalPago = CONST_CANAL_PAGO_PAC then begin
            ParamByName('@PAC_CodigoBanco').Value := cb_BancosPAC.Value;
            ParamByName('@PAC_NroCuentaBancaria').Value	:= Trim(txt_NroCuentaBancariaPAC.Text);
            ParamByName('@PAC_Sucursal').Value := Trim(txt_SucursalPAC.Text);
            ParamByName('@PAC_Titular').Value := Trim(txt_TitularPAC.Text);
        end else begin // FCodigoCanalPago = CONST_CANAL_PAGO_PAT
            ParamByName('@PAT_CodigoTipoTarjetaCredito').Value := iif(cb_TipoTarjetaCreditoPAT.Value = 0, Null, cb_TipoTarjetaCreditoPAT.Value);
            ParamByName('@PAT_NumeroTarjetaCredito').Value := iif(Trim(txt_NumeroTarjetaCreditoPAT.Text) = EmptyStr, Null, Trim(txt_NumeroTarjetaCreditoPAT.Text));
            ParamByName('@PAT_FechaVencimiento').Value := iif(txt_FechaVencimiento.Date <= 0, Null, txt_FechaVencimiento.Date);
            ParamByName('@PAT_CodigoEmisorTarjetaCredito').Value := iif(cb_EmisorTarjetaCreditoPAT.Value = 0, Null, cb_EmisorTarjetaCreditoPAT.Value);
        end;
    end;
    sp_RegistrarDetallePagoComprobante.ExecProc;


    if sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@IDDetallePagoComprobante').Value = Null then begin
        MensajeError := MSG_ERROR_ID_DETALLE_IS_NULL;
        Exit;
    end;

    try
        with sp_RegistrarPagoAnticipado.Parameters do begin
//            ParamByName('@TipoComprobante').Value := Comprobante.TipoComprobante;
//            ParamByName('@NumeroComprobante').Value := Comprobante.NumeroComprobante;
            ParamByName('@FechaHora').Value := deFechaPago.Date;
            ParamByName('@CodigoConvenio').Value := FCodigoConvenio;
            ParamByName('@Importe').Value := FImportePagado * 100;
            ParamByName('@NumeroRecibo').Value := NumeroRecibo;
//            ParamByName('@CodigoConceptoPago').Value := ObtenerCodigoConceptoPago(DMConnections.BaseCAC, FCodigoCanalPago);
//            ParamByName('@DescripcionPago').Value := MSG_PAGO_DEBITO_CUENTA;
            ParamByName('@NumeroTurno').Value := GNumeroTurno;
            ParamByName('@Usuario').Value := UsuarioSistema;
            ParamByName('@IDDetallePagoComprobante').Value := sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@IDDetallePagoComprobante').Value;
            ParamByName('@NoCalcularIntereses').Value := FNoCalcularIntereses;
        end;
        sp_RegistrarPagoAnticipado.ExecProc;
    except
        on e: Exception do begin
            raise Exception.Create(MSG_PAGO_DEBITO_CUENTA + CRLF + e.Message);
        end;
    end;
    Result := True;
end;

{-----------------------------------------------------------------------------
  Procedure: TfrmPagoVentanilla.GenerarPagoCheque
  Author:    mlopez
  Date:      10/08/2005
  Description: Registra el pago de comprobante a traves del medio de pago "Cheque"
  Arguments: NumeroRecibo: Integer; var MensajeError: String
  Result:    Boolean

  Revision : 1
    Author : pdominguez
    Date   : 28/09/2009
    Description : SS 834
        - Se a�ade la asignaci�n del valor al par�metro @RepactacionNumero del
        SP sp_RegistrarDetallePagoComprobante.
-----------------------------------------------------------------------------}
function TfrmPagoVentanillaAnticipado.GenerarPagoCheque(NumeroRecibo: Integer; var MensajeError: String): Boolean;
resourcestring
    MSG_ERROR_PAGO_CHEQUE = 'Error registrando un pago con cheque';
    MSG_PAGO_CHEQUE = 'Pago con cheque';
    MSG_ERROR_ID_DETALLE_IS_NULL = 'No se pudo registrar el Detalle del Pago';
    MSG_ERROR_NUMERO_PAGO_IS_NULL = 'No se pudo registrar del Pago del Comprobante'#10#13'%s %d';
begin
    Result := False;
    MensajeError := '';
    with sp_RegistrarDetallePagoComprobante.Parameters do begin
        ParamByName('@CodigoTipoMedioPago').Value := FCodigoCanalPago;
        ParamByName('@CodigoFormaPago').Value := FCodigoFormaPago;
        ParamByName('@ChequeCodigoBanco').Value	:= FCodigoBanco;
        ParamByName('@ChequeNumero').Value	:= Trim(UpperCase(txtNumeroCheque.Text));
        ParamByName('@ChequeTitular').Value := Trim(UpperCase(txtNombreTitular.Text));
        ParamByName('@ChequeCodigoDocumento').Value	:= 'RUT';
        ParamByName('@ChequeNumeroDocumento').Value	:= PADL(Trim(txtDocumento.Text), 9, '0');
        ParamByName('@ChequeMonto').Value := FImportePagado * 100;
        ParamByName('@ChequeFecha').Value := deFechaCheque.Date;
        ParamByName('@RepactacionNumero').Value := iif(Trim(edtRepactacionCheque.Text) <> '',Trim(edtRepactacionCheque.Text),NULL); // Rev. 1 (SS 834)
    end;
    sp_RegistrarDetallePagoComprobante.ExecProc;

    if sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@IDDetallePagoComprobante').Value = Null then begin
        MensajeError := MSG_ERROR_ID_DETALLE_IS_NULL;
        Exit;
    end;

    try
        with sp_RegistrarPagoAnticipado.Parameters do begin
//            ParamByName('@TipoComprobante').Value := Comprobante.TipoComprobante;
//            ParamByName('@NumeroComprobante').Value := Comprobante.NumeroComprobante;
            ParamByName('@FechaHora').Value := deFechaPago.Date;
            ParamByName('@CodigoConvenio').Value := FCodigoConvenio;
            ParamByName('@Importe').Value := FImportePagado * 100;
            ParamByName('@NumeroRecibo').Value := NumeroRecibo;
//            ParamByName('@CodigoConceptoPago').Value := ObtenerCodigoConceptoPago(DMConnections.BaseCAC, FCodigoCanalPago);
//            ParamByName('@DescripcionPago').Value := MSG_PAGO_CHEQUE;
            ParamByName('@NumeroTurno').Value := GNumeroTurno;
            ParamByName('@Usuario').Value := UsuarioSistema;
            ParamByName('@IDDetallePagoComprobante').Value := sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@IDDetallePagoComprobante').Value;
            ParamByName('@NoCalcularIntereses').Value := FNoCalcularIntereses;
        end;
        sp_RegistrarPagoAnticipado.ExecProc;
    except
        on e: Exception do begin
            raise Exception.Create(MSG_ERROR_PAGO_CHEQUE + CRLF + e.Message);
        end;
    end;
    Result := True;
end;

{-----------------------------------------------------------------------------
  Procedure: TfrmPagoVentanilla.GenerarPagoValeVista
  Author:    mlopez
  Date:      10/08/2005
  Description: Registra el pago de comprobante a traves del medio de pago "Vale Vista"
  Arguments: NumeroRecibo: Integer; var MensajeError: String
  Result:    Boolean
-----------------------------------------------------------------------------}
function TfrmPagoVentanillaAnticipado.GenerarPagoValeVista(NumeroRecibo: Integer; var MensajeError: String): Boolean;
resourcestring
    MSG_ERROR_PAGO_VALE_VISTA = 'Error registrando un pago con vale vista';
    MSG_PAGO_VALE_VISTA = 'Pago con vale vista';
    MSG_ERROR_ID_DETALLE_IS_NULL = 'No se pudo registrar el Detalle del Pago';
    MSG_ERROR_NUMERO_PAGO_IS_NULL = 'No se pudo registrar del Pago del Comprobante'#10#13'%s %d';
begin
    Result := False;
    MensajeError := '';
    with sp_RegistrarDetallePagoComprobante.Parameters do begin
        ParamByName('@CodigoTipoMedioPago').Value := FCodigoCanalPago;
        ParamByName('@CodigoFormaPago').Value := FCodigoFormaPago;
        ParamByName('@ChequeCodigoBanco').Value := FCodigoBanco;
        ParamByName('@ChequeNumero').Value := Trim(UpperCase(txt_NroVV.Text));
        ParamByName('@ChequeTitular').Value := Trim(UpperCase(txt_TomadoPorVV.Text));
        ParamByName('@ChequeCodigoDocumento').Value := 'RUT';
        ParamByName('@ChequeNumeroDocumento').Value := PADL(Trim(txt_RUTVV.Text), 9, '0');
        ParamByName('@ChequeMonto').Value := FImportePagado * 100;
        ParamByName('@ChequeFecha').Value := de_FechaVV.Date;
    end;
    sp_RegistrarDetallePagoComprobante.ExecProc;


    if sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@IDDetallePagoComprobante').Value = Null then begin
        MensajeError := MSG_ERROR_ID_DETALLE_IS_NULL;
        Exit;
    end;

    try
        with sp_RegistrarPagoAnticipado.Parameters do begin
//            ParamByName('@TipoComprobante').Value := Comprobante.TipoComprobante;
//            ParamByName('@NumeroComprobante').Value := Comprobante.NumeroComprobante;
            ParamByName('@FechaHora').Value := deFechaPago.Date;
            ParamByName('@CodigoConvenio').Value := FCodigoConvenio;
            ParamByName('@Importe').Value := FImportePagado * 100;
            ParamByName('@NumeroRecibo').Value := NumeroRecibo;
//            ParamByName('@CodigoConceptoPago').Value := ObtenerCodigoConceptoPago(DMConnections.BaseCAC, FCodigoCanalPago);
//            ParamByName('@DescripcionPago').Value := MSG_PAGO_VALE_VISTA;
            ParamByName('@NumeroTurno').Value := GNumeroTurno;
            ParamByName('@Usuario').Value := UsuarioSistema;
            ParamByName('@IDDetallePagoComprobante').Value := sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@IDDetallePagoComprobante').Value;
            ParamByName('@NoCalcularIntereses').Value := FNoCalcularIntereses;
        end;
        sp_RegistrarPagoAnticipado.ExecProc;
    except
        on e: Exception do begin
            raise Exception.Create(MSG_ERROR_PAGO_VALE_VISTA + CRLF + e.Message);
        end;
    end;
    Result := True;
end;

{******************************** Function Header ******************************
Procedure Name: GenerarPagoPagare
Author : pdominguez
Date Created : 27/10/2009
Parameters : NumeroRecibo: Integer; var MensajeError: String
Return Value : Boolean
Description : SS 834
    - Genera el pago efectuado, para las formas de pago tipo Pagar�.
*******************************************************************************}
function TfrmPagoVentanillaAnticipado.GenerarPagoPagare(NumeroRecibo: Integer; var MensajeError: String): Boolean;
resourcestring
    MSG_ERROR_PAGO_PAGARE = 'Error registrando el pago con Pagar�';
    MSG_ERROR_ID_DETALLE_IS_NULL = 'No se pudo registrar el Detalle del Pago';
    MSG_ERROR_NUMERO_PAGO_IS_NULL = 'No se pudo registrar del Pago del Comprobante'#10#13'%s %d';
begin
    Result := False;
    MensajeError := '';

    with sp_RegistrarDetallePagoComprobante, Parameters do begin
        ParamByName('@CodigoTipoMedioPago').Value := FCodigoCanalPago;
        ParamByName('@CodigoFormaPago').Value := FCodigoFormaPago;
        ParamByName('@ChequeCodigoBanco').Value := FCodigoBanco;
        ParamByName('@ChequeNumero').Value := Trim(UpperCase(txtPagareNumero.Text));
        ParamByName('@ChequeTitular').Value := Trim(UpperCase(txtPagareNombreTitular.Text));
        ParamByName('@ChequeCodigoDocumento').Value := 'RUT';
        ParamByName('@ChequeNumeroDocumento').Value := PADL(Trim(txtPagareDocumento.Text), 9, '0');
        ParamByName('@ChequeMonto').Value := FImportePagado * 100;
        ParamByName('@ChequeFecha').Value := dePagareFecha.Date;
        ParamByName('@RepactacionNumero').Value := iif(Trim(edtPagareRepactacion.Text) <> '',Trim(edtPagareRepactacion.Text),NULL);
        ExecProc;
    end;

    if sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@IDDetallePagoComprobante').Value = Null then begin
        MensajeError := MSG_ERROR_ID_DETALLE_IS_NULL;
        Exit;
    end
    else try
        with sp_RegistrarPagoAnticipado, Parameters do begin
            ParamByName('@FechaHora').Value := deFechaPago.Date;
            ParamByName('@CodigoConvenio').Value := FCodigoConvenio;
            ParamByName('@Importe').Value := FImportePagado * 100;
            ParamByName('@NumeroRecibo').Value := NumeroRecibo;
            ParamByName('@NumeroTurno').Value := GNumeroTurno;
            ParamByName('@Usuario').Value := UsuarioSistema;
            ParamByName('@IDDetallePagoComprobante').Value := sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@IDDetallePagoComprobante').Value;
            ParamByName('@NoCalcularIntereses').Value := FNoCalcularIntereses;
            ExecProc;
        end;
    except
        on e: Exception do begin
            raise Exception.Create(MSG_ERROR_PAGO_PAGARE + CRLF + e.Message);
        end;
    end;
    Result := True;
end;

procedure TfrmPagoVentanillaAnticipado.ne_ImportePagadoChange(
  Sender: TObject);
begin
    FImportePagado := ne_ImportePagado.Value;
end;

procedure TfrmPagoVentanillaAnticipado.chkNoCalcularInteresesClick(             //SS_295_437_NDR_20121030
  Sender: TObject);                                                             //SS_295_437_NDR_20121030
begin                                                                           //SS_295_437_NDR_20121030
    FNoCalcularIntereses := chkNoCalcularIntereses.Checked;                     //SS_295_437_NDR_20121030
end;                                                                            //SS_295_437_NDR_20121030

end.


