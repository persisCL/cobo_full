unit frmRefinanciacionCuotasHistorial;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,

  DMConnection, PeaProcsCN, SysUtilsCN,

  Dialogs, StdCtrls, ExtCtrls, DB, ADODB, Provider, DBClient, ListBoxEx, DBListEx;

type
  TRefinanciacionCuotasHistorialForm = class(TForm)
    pnlAcciones: TPanel;
    btnCancelar: TButton;
    GroupBox1: TGroupBox;
    DBListEx1: TDBListEx;
    cdsCuotasHistorico: TClientDataSet;
    dsCuotasHistorico: TDataSource;
    dspObtenerRefinanciacionCuotasEstadosHistorico: TDataSetProvider;
    spObtenerRefinanciacionCuotasEstadosHistorico: TADOStoredProc;
    cdsCuotasHistoricoCodigoEstadoCuota: TSmallintField;
    cdsCuotasHistoricoDescripcion: TStringField;
    cdsCuotasHistoricoUsuarioCreacion: TStringField;
    cdsCuotasHistoricoFechaCreacion: TDateTimeField;
    cdsCuotasHistoricoUsuarioModificacion: TStringField;
    cdsCuotasHistoricoFechaModificacion: TDateTimeField;
    procedure btnCancelarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function Inicializar(CodigoRefinanciacion, CodigoCuota: Integer): Boolean;
  end;

var
  RefinanciacionCuotasHistorialForm: TRefinanciacionCuotasHistorialForm;

implementation

{$R *.dfm}

procedure TRefinanciacionCuotasHistorialForm.btnCancelarClick(Sender: TObject);
begin
    Close;
end;

function TRefinanciacionCuotasHistorialForm.Inicializar(CodigoRefinanciacion: Integer; CodigoCuota: Integer): Boolean;
begin
    try
        Result := False;
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;
        try
            with spObtenerRefinanciacionCuotasEstadosHistorico do begin
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoRefinanciacion').Value := CodigoRefinanciacion;
                Parameters.ParamByName('@CodigoCuota').Value          := CodigoCuota;
            end;

            cdsCuotasHistorico.Open;
            Result := True;
        Except
            on e:Exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
        Screen.Cursor := crDefault;
    end;
end;

end.
