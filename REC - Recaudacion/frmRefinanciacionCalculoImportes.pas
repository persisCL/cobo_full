unit frmRefinanciacionCalculoImportes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, DmiCtrls, DB, ListBoxEx, DBListEx, DBClient;

type
  TRefinanciacionCalculoImportesForm = class(TForm)
    Panel1: TPanel;
    btnCancelar: TButton;
    btnAceptar: TButton;
    GroupBox1: TGroupBox;
    cdsCuotasCalculo: TClientDataSet;
    DBListEx1: TDBListEx;
    dsCuotasCalculo: TDataSource;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    nedtImporteCuota: TNumericEdit;
    procedure cdsCuotasCalculoAfterScroll(DataSet: TDataSet);
    procedure nedtImporteCuotaKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
    procedure DBListEx1DrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
    procedure DBListEx1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  RefinanciacionCalculoImportesForm: TRefinanciacionCalculoImportesForm;

implementation

{$R *.dfm}

uses PeaProcsCN, PeaTypes;

procedure TRefinanciacionCalculoImportesForm.btnAceptarClick(Sender: TObject);
begin
    ModalResult := mrOk;
end;

procedure TRefinanciacionCalculoImportesForm.btnCancelarClick(Sender: TObject);
begin
    ModalResult := mrCancel;
end;

procedure TRefinanciacionCalculoImportesForm.cdsCuotasCalculoAfterScroll(DataSet: TDataSet);
begin
    nedtImporteCuota.ValueInt := cdsCuotasCalculo.FieldByName('Importe').AsInteger;
end;

procedure TRefinanciacionCalculoImportesForm.DBListEx1Click(Sender: TObject);
begin
    nedtImporteCuota.SetFocus;
    nedtImporteCuota.SelectAll;
end;

procedure TRefinanciacionCalculoImportesForm.DBListEx1DrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
    if Column.FieldName = 'Importe' then begin
        Text := FormatFloat(FORMATO_IMPORTE, StrToFloat(Text));
    end;
end;

procedure TRefinanciacionCalculoImportesForm.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
    case Key of
        VK_ESCAPE: ModalResult := mrCancel;
    end;
end;

procedure TRefinanciacionCalculoImportesForm.nedtImporteCuotaKeyPress(Sender: TObject; var Key: Char);
    resourcestring
        rsValidacionTitulo  = 'Validación Importe';
        rsValidacionMensaje = 'EL Importe introducido debe ser mayor de Cero.';
begin
    if key = #13 then begin
        if nedtImporteCuota.ValueInt > 0 then begin
            cdsCuotasCalculo.Edit;
            cdsCuotasCalculo.FieldByName('Importe').AsInteger := nedtImporteCuota.ValueInt;
            cdsCuotasCalculo.Post;

            cdsCuotasCalculo.Next;
            if cdsCuotasCalculo.Eof then
                btnAceptar.SetFocus
            else nedtImporteCuota.SelectAll;
        end
        else ShowMsgBoxCN(rsValidacionTitulo, rsValidacionMensaje, MB_ICONERROR, Self);
    end;
end;

end.
