 {********************************** Unit Header ********************************
File Name : DetalleTurnoCierre.pas
Author :   Juan Lobos - JLO
Date Created: 20-04-2016
Description : Cuadro Resumen presentado al existir descuadres en el cierre de turno.

*******************************************************************************}
unit DetalleTurnoCierre;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, ADODB, Provider, DB, DBClient,
  Grids, DBGrids, DMConnection, UtilProc, PeaTypes, UtilDB, Util, RStrings,
  PeaProcs, ComCtrls, DmiCtrls, Menus, FrmAperturaCierrePuntoVenta,
  LoginSup;

type
  TDetalleCierreTurno = class(TForm)
    spObtenerCuadraturaCierre: TADOStoredProc;
    GrillaResumenDescuadre: TStringGrid;
    btn_ok: TButton;
    btn_cancel: TButton;
    lbl1: TLabel;
    lbl2: TLabel;
    procedure btn_cancelClick(Sender: TObject);
    procedure btn_okClick(Sender: TObject);

  private
    { Private declarations }
  public
   function Inicializar(Nturno:Integer):boolean;
  end;

var
  DetalleCierreTurno: TDetalleCierreTurno;

implementation

{$R *.dfm}

procedure TDetalleCierreTurno.btn_okClick(Sender: TObject);
var
    SP: TFrmConfirmacionSupervisor;
begin
    Application.CreateForm(TFrmConfirmacionSupervisor, SP);
    SP.ShowModal;

    if SP.ModalResult = mrOk then begin
        FrmAperturaCierrePuntoVenta.InfoValidada := 0;
    end;
    Close;
end;

procedure TDetalleCierreTurno.btn_cancelClick(Sender: TObject);
begin
    FrmAperturaCierrePuntoVenta.InfoValidada := 1;

    Close;
end;

function TDetalleCierreTurno.Inicializar(Nturno:Integer):boolean;
resourcestring
	CAPTION_PAGO            = 'Pago';
	CAPTION_APERTURA        = 'Apertura';
	CAPTION_SISTEMA         = 'Sistema';
    CAPTION_LIQUIDADO       = 'Liquidado';
    CAPTION_DIFERENCIA      = 'Diferencia';


var
    i : Integer;
begin
    try
     CenterForm(Self);

     spObtenerCuadraturaCierre.Parameters.ParamByName( '@NumeroTurno' ).Value := Nturno;
     spObtenerCuadraturaCierre.Parameters.ParamByName( '@Detalle').Value := 1;
     result := (OpenTables([spObtenerCuadraturaCierre]));


	  GrillaResumenDescuadre.Cells[0,0] := CAPTION_PAGO;
	  GrillaResumenDescuadre.Cells[1,0] := CAPTION_APERTURA;
	  GrillaResumenDescuadre.Cells[2,0] := CAPTION_SISTEMA;
      GrillaResumenDescuadre.Cells[3,0] := CAPTION_LIQUIDADO;
      GrillaResumenDescuadre.Cells[4,0] := CAPTION_DIFERENCIA;

      with spObtenerCuadraturaCierre, GrillaResumenDescuadre do begin
          i:= 0;
		  RowCount :=  RecordCount + 1;

          while not Eof do begin
              inc(i);
              Cells[0,i] := FieldByName('Pago').AsString;
              {INICIO		: 20170117 CFU TASK_087_CFU_20170117-CRM_Error_Cierre_Turno
              Cells[1,i] := '$' + PadL(FloatToStrF(FieldByName('Apertura').AsFloat, ffFixed,8, 0), 9, ' ');
              Cells[2,i] := '$' + PadL(FloatToStrF(FieldByName('Sistema').AsFloat, ffFixed,8, 0), 9, ' ');
              Cells[3,i] := '$' + PadL(FloatToStrF(FieldByName('Liquidado').AsFloat, ffFixed,8, 0), 9, ' ');
              Cells[4,i] := '$' + PadL(FloatToStrF(FieldByName('Diferencia').AsFloat, ffFixed,8, 0), 9, ' ');
              }

              Cells[1,i] := '$' + PadL(FloatToStrF(FieldByName('Apertura').AsFloat, ffNumber,8, 0), 9, ' ');
              Cells[2,i] := '$' + PadL(FloatToStrF(FieldByName('Sistema').AsFloat, ffNumber,8, 0), 9, ' ');
              {INICIO		: 20170117 CFU TASK_087_CFU_20170117-CRM_Error_Cierre_Turno-1
              Cells[3,i] := '$' + PadL(FloatToStrF(FieldByName('Liquidado').AsFloat, ffNumber,12, 0), 13, ' ');
              Cells[4,i] := '$' + PadL(FloatToStrF(FieldByName('Diferencia').AsFloat, ffNumber,12, 0), 13, ' ');
              //TERMINO		: 20170117 CFU TASK_087_CFU_20170117-CRM_Error_Cierre_Turno
              }
              Cells[3,i] := '$' + FloatToStrF(FieldByName('Liquidado').AsFloat, ffNumber, 18, 0);
              Cells[4,i] := '$' + FloatToStrF(FieldByName('Diferencia').AsFloat, ffNumber, 18, 0);
              //TERMINO		: 20170117 CFU TASK_087_CFU_20170117-CRM_Error_Cierre_Turno-1
              if i = 1 then begin
              FrmAperturaCierrePuntoVenta.DifEfectivo  := FieldByName('Diferencia').AsInteger;
              end;

              Next;
          end;

          Close;
          Row := 1;
          Col := 2;
      end;

     except
     end;

end;
end.

