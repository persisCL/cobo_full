object TraspasarDeudaForm: TTraspasarDeudaForm
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'TraspasarDeudaForm'
  ClientHeight = 525
  ClientWidth = 663
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Arial'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 14
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 663
    Height = 217
    Align = alTop
    TabOrder = 0
    object Label5: TLabel
      Left = 16
      Top = 17
      Width = 110
      Height = 16
      Caption = 'Convenio Origen:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label15: TLabel
      Left = 132
      Top = 19
      Width = 182
      Height = 13
      Caption = 'Desde el cual se va a sacar el Importe'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label16: TLabel
      Left = 78
      Top = 90
      Width = 40
      Height = 14
      Caption = 'Nombre:'
    end
    object Label17: TLabel
      Left = 70
      Top = 102
      Width = 48
      Height = 14
      Caption = 'Direcci'#243'n:'
    end
    object Label18: TLabel
      Left = 63
      Top = 114
      Width = 55
      Height = 14
      Caption = 'Personer'#237'a:'
    end
    object lblNombreOrigen: TLabel
      Left = 121
      Top = 90
      Width = 79
      Height = 14
      Caption = 'lblNombreOrigen'
    end
    object lblDireccionOrigen: TLabel
      Left = 121
      Top = 102
      Width = 87
      Height = 14
      Caption = 'lblDireccionOrigen'
    end
    object lblPersoneriaOrigen: TLabel
      Left = 121
      Top = 114
      Width = 94
      Height = 14
      Caption = 'lblPersoneriaOrigen'
    end
    object Label2: TLabel
      Left = 362
      Top = 40
      Width = 64
      Height = 16
      Caption = 'Convenio:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label9: TLabel
      Left = 82
      Top = 141
      Width = 36
      Height = 14
      Caption = 'Estado:'
    end
    object lblEstadoOrigen: TLabel
      Left = 124
      Top = 140
      Width = 87
      Height = 14
      Caption = 'lblEstadoOrigen'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label10: TLabel
      Left = 88
      Top = 153
      Width = 30
      Height = 14
      Caption = 'Saldo:'
    end
    object lblSaldoOrigen: TLabel
      Left = 121
      Top = 153
      Width = 69
      Height = 14
      Caption = 'lblSaldoOrigen'
    end
    object Label13: TLabel
      Left = 58
      Top = 165
      Width = 60
      Height = 14
      Caption = 'Forma Pago:'
    end
    object lblFormaOrigen: TLabel
      Left = 121
      Top = 165
      Width = 72
      Height = 14
      Caption = 'lblFormaOrigen'
    end
    object lblInteresesOrigen: TLabel
      Left = 330
      Top = 141
      Width = 239
      Height = 14
      Caption = 'Convenio tiene intereses pendientes de Facturar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsItalic]
      ParentFont = False
    end
    object lblTtosOrigen: TLabel
      Left = 330
      Top = 152
      Width = 239
      Height = 14
      Caption = 'Convenio tiene Tr'#225'nsitos pendientes de Facturar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsItalic]
      ParentFont = False
    end
    object Shape2: TShape
      Left = 63
      Top = 133
      Width = 310
      Height = 1
    end
    object Label1: TLabel
      Left = 88
      Top = 40
      Width = 76
      Height = 16
      Caption = 'Rut Cliente:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblEnListaAmarilla: TLabel
      Left = 362
      Top = 89
      Width = 200
      Height = 14
      Alignment = taCenter
      AutoSize = False
      Color = clYellow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      Transparent = False
      Visible = False
    end
    object vcbConvenioOrigen: TVariantComboBox
      Tag = 10
      Left = 362
      Top = 61
      Width = 247
      Height = 22
      Style = vcsOwnerDrawFixed
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ItemHeight = 16
      ParentFont = False
      TabOrder = 1
      OnChange = TraerSaldoConvenio
      OnDrawItem = vcbConvenioOrigenDrawItem
      Items = <>
    end
    object edtRutOrigen: TPickEdit
      Left = 88
      Top = 62
      Width = 220
      Height = 24
      Enabled = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnKeyPress = ValidaBusca
      EditorStyle = bteTextEdit
      OnButtonClick = VerPosiblesRut
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 217
    Width = 663
    Height = 208
    Align = alTop
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object Label6: TLabel
      Left = 16
      Top = 17
      Width = 117
      Height = 16
      Caption = 'Convenio Destino:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 88
      Top = 40
      Width = 76
      Height = 16
      Caption = 'Rut Cliente:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 362
      Top = 40
      Width = 64
      Height = 16
      Caption = 'Convenio:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label20: TLabel
      Left = 139
      Top = 19
      Width = 170
      Height = 13
      Caption = 'Al cual se le va a asignar el Importe'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label11: TLabel
      Left = 82
      Top = 141
      Width = 36
      Height = 14
      Caption = 'Estado:'
    end
    object lblEstadoDestino: TLabel
      Left = 121
      Top = 141
      Width = 92
      Height = 14
      Caption = 'lblEstadoDestino'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblSaldoDestino: TLabel
      Left = 121
      Top = 153
      Width = 73
      Height = 14
      Caption = 'lblSaldoDestino'
    end
    object Label12: TLabel
      Left = 88
      Top = 153
      Width = 30
      Height = 14
      Caption = 'Saldo:'
    end
    object Label14: TLabel
      Left = 58
      Top = 165
      Width = 60
      Height = 14
      Caption = 'Forma Pago:'
    end
    object lblFormaDestino: TLabel
      Left = 121
      Top = 165
      Width = 76
      Height = 14
      Caption = 'lblFormaDestino'
    end
    object Label22: TLabel
      Left = 78
      Top = 90
      Width = 40
      Height = 14
      Caption = 'Nombre:'
    end
    object lblNombreDestino: TLabel
      Left = 121
      Top = 90
      Width = 83
      Height = 14
      Caption = 'lblNombreDestino'
    end
    object Label24: TLabel
      Left = 70
      Top = 102
      Width = 48
      Height = 14
      Caption = 'Direcci'#243'n:'
    end
    object Label25: TLabel
      Left = 63
      Top = 114
      Width = 55
      Height = 14
      Caption = 'Personer'#237'a:'
    end
    object lblPersoneriaDestino: TLabel
      Left = 121
      Top = 114
      Width = 98
      Height = 14
      Caption = 'lblPersoneriaDestino'
    end
    object lblDireccionDestino: TLabel
      Left = 121
      Top = 102
      Width = 91
      Height = 14
      Caption = 'lblDireccionDestino'
    end
    object lblInteresesDestino: TLabel
      Left = 330
      Top = 141
      Width = 239
      Height = 14
      Caption = 'Convenio tiene intereses pendientes de Facturar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsItalic]
      ParentFont = False
    end
    object lblTtosDestino: TLabel
      Left = 330
      Top = 152
      Width = 239
      Height = 14
      Caption = 'Convenio tiene Tr'#225'nsitos pendientes de Facturar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsItalic]
      ParentFont = False
    end
    object Shape1: TShape
      Left = 63
      Top = 133
      Width = 310
      Height = 1
    end
    object lblEnListaAmarillaDestino: TLabel
      Left = 362
      Top = 89
      Width = 200
      Height = 14
      Alignment = taCenter
      AutoSize = False
      Color = clYellow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      Transparent = False
      Visible = False
    end
    object vcbConvenioDestino: TVariantComboBox
      Tag = 20
      Left = 362
      Top = 61
      Width = 247
      Height = 22
      Style = vcsOwnerDrawFixed
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ItemHeight = 16
      ParentFont = False
      TabOrder = 1
      OnChange = TraerSaldoConvenio
      OnDrawItem = vcbConvenioDestinoDrawItem
      Items = <>
    end
    object edtRutDestino: TPickEdit
      Left = 88
      Top = 61
      Width = 220
      Height = 24
      Enabled = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnKeyPress = ValidaBusca
      EditorStyle = bteTextEdit
      OnButtonClick = VerPosiblesRut
    end
  end
  object GroupBox3: TGroupBox
    Left = 0
    Top = 425
    Width = 663
    Height = 100
    Align = alClient
    TabOrder = 2
    object Label8: TLabel
      Left = 55
      Top = 45
      Width = 127
      Height = 16
      Caption = 'Monto a Traspasar:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object neMonto: TNumericEdit
      Left = 188
      Top = 42
      Width = 121
      Height = 22
      TabOrder = 0
    end
    object btnAceptar: TButton
      Left = 413
      Top = 41
      Width = 75
      Height = 25
      Caption = '&Aceptar'
      TabOrder = 1
      OnClick = btnAceptarClick
    end
    object btnCancelar: TButton
      Left = 494
      Top = 41
      Width = 75
      Height = 25
      Cancel = True
      Caption = '&Cancelar'
      TabOrder = 2
      OnClick = btnCancelarClick
    end
  end
  object spObtenerConvenios: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    CommandTimeout = 60
    ProcedureName = 'ObtenerConvenios;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@SoloActivos'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@SoloContanera'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 448
    Top = 173
  end
  object spTraspasarEntreConvenios: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CommandTimeout = 60
    ProcedureName = 'TraspasarEntreConvenios;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenioOrigen'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenioDestino'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@MontoATraspasar'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@MensajeError'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = Null
      end>
    Left = 520
    Top = 189
  end
  object spObtenerDatosCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    CommandTimeout = 60
    ProcedureName = 'ObtenerDatosCliente;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 600
    Top = 205
  end
end
