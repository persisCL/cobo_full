unit UtilCobranzas;

interface
uses DMConnection, Math,  VariantComboBox, DateUtils, RStrings, Variants,
     SysUtils, PeaTypes, Menus, Util, UtilDB;

resourcestring
    DESC_MEDIO_PAGO_AUTOMATICO_NINGUNO  =   'Ninguno';
    DESC_MEDIO_PAGO_AUTOMATICO_PAT      =   'Autom�tico PAT';
    DESC_MEDIO_PAGO_AUTOMATICO_PAC      =   'Autom�tico PAC';
    DESC_MEDIO_PAGO_EFECTIVO            =   'Efectivo';
    DESC_MEDIO_PAGO_CHEQUE              =   'Cheque';
    DESC_MEDIO_PAGO_TARJETA_DEBITO      =   'Tarjeta de D�bito';
    DESC_MEDIO_PAGO_TARJETA_CREDITO     =   'Tarjeta de Cr�dito';

const
    DESC_MEDIO_PAGO: array[0..6] of string =   (DESC_MEDIO_PAGO_AUTOMATICO_NINGUNO,
                                                DESC_MEDIO_PAGO_AUTOMATICO_PAT,
                                                DESC_MEDIO_PAGO_AUTOMATICO_PAC,
                                                DESC_MEDIO_PAGO_EFECTIVO,
                                                DESC_MEDIO_PAGO_CHEQUE,
                                                DESC_MEDIO_PAGO_TARJETA_DEBITO ,  
                                                DESC_MEDIO_PAGO_TARJETA_CREDITO );

                                               {  ( 'Ninguno'
                                                 ,'Autom�tico PAT'
                                                 ,'Autom�tico PAC'
                                                 ,'Efectivo'
                                                 ,'Cheque'
                                                 ,'Tarjeta de D�bito'
                                                 ,'Tarjeta de Cr�dito');}

// Tipos Medios de Pagos
Type

  TTDEBITO = record
    TipoCuenta          : Integer;
    CodigoBanco         : Integer;
    NroCuentaBancaria   : String[30];
    Sucursal            : String[50];
  end;

  TCHEQUE = record
    TipoCuenta          : Integer;
    CodigoBanco         : Integer;
    NroCuentaBancaria   : String[30];
    Sucursal            : String[50];
    NumeroCheque        : String[20];
    FechaCheque         : TDateTime;
  end;

  TTCREDITO = record
    TipoTarjeta         : Integer;
    EmisorTarjetaCredito: Integer;
    NroTarjeta          : String[20];
    FechaVencimiento    : String[5];
    Cuotas              : integer;
  end;

  TTipoMedioPago = (NINGUNO, MEDIO_PAGO_AUTO_PAT, MEDIO_PAGO_AUTO_PAC, MEDIO_PAGO_EFECTIVO, MEDIO_PAGO_CHEQUE, MEDIO_PAGO_TARJETA_DEBITO, MEDIO_PAGO_TARJETA_CREDITO);
  TDatosMedioPago = record
    RUT: String[11];
    Nombre: String[100];
    Telefono:String[30];
    CodigoArea: integer;
    CodigoPersona:Integer;
    //Datos utilizados para efectuar un pago, no se piden en el alta de un convenio
    Cupon: integer;
    Autorizacion: string[20];
    Monto: integer;
    Case TipoMedioPago: TTipoMedioPago OF
                    //MEDIO_PAGO_EFECTIVO:  ;
                    MEDIO_PAGO_CHEQUE:          (    Cheque: TCHEQUE;           );
                    MEDIO_PAGO_TARJETA_DEBITO:  (    TarjetaDebito: TTDEBITO;   );
                    MEDIO_PAGO_TARJETA_CREDITO: (    TarjetaCredito: TTCREDITO; );
   end;

{-----------------------------------------------------------------------------
  Function Name: ValidarPOSNET
  Author:    ndonadio
  Date Created: 08+02+2005
  Description: Devuelve el numero de POS asociado al Pto de Entrega
                configurado.
  Parameters: None
  Return Value: integer
                0: No usa POS. Hay POS Asociado.
               -1: No usa POS. No hay POS Asociado.
               -2: ERROR. Usa POS. No hay POS Asociado.
               >0: Usa POS. El Nro. de POS Asociado.
-----------------------------------------------------------------------------}
implementation
end.
