object FrmInterfazEntradaPatentes: TFrmInterfazEntradaPatentes
  Left = 356
  Top = 251
  BorderStyle = bsDialog
  Caption = 'Interfaz de Entrada de Registro de Patentes'
  ClientHeight = 127
  ClientWidth = 457
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 457
    Height = 88
    BevelOuter = bvLowered
    TabOrder = 0
    object Label1: TLabel
      Left = 11
      Top = 18
      Width = 114
      Height = 13
      Caption = '&Archivo de Entrada:'
      FocusControl = peArchivo
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object ProgressBar: TProgressBar
      Left = 13
      Top = 56
      Width = 433
      Height = 17
      Min = 0
      Max = 100
      TabOrder = 1
    end
    object peArchivo: TPickEdit
      Left = 145
      Top = 14
      Width = 303
      Height = 21
      Enabled = True
      TabOrder = 0
      OnChange = peArchivoChange
      Decimals = 0
      EditorStyle = bteTextEdit
      OnButtonClick = peArchivoButtonClick
    end
  end
  object btnAceptar: TDPSButton
    Left = 297
    Top = 96
    Height = 26
    Caption = '&Aceptar'
    Default = True
    ModalResult = 1
    TabOrder = 1
    OnClick = btnAceptarClick
  end
  object btnCancelar: TDPSButton
    Left = 377
    Top = 96
    Height = 26
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 2
    OnClick = btnCancelarClick
  end
  object InterfazEntradaRegistroPatentes: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'InterfazEntradaRegistroPatentes'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@Apellido'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@Nombre'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@Domicilio'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@CodigoPostal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@Comuna'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@Region'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@Pais'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end>
    Left = 8
    Top = 93
  end
  object OpenDialog: TOpenDialog
    DefaultExt = 'TXT'
    Filter = 'Archivo de texto|*.TXT'
    Left = 40
    Top = 93
  end
end
