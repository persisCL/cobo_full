unit frmConveniosMorosos;
{
Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)
}
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BuscaClientes, StdCtrls, UtilDB, Util, VariantComboBox, PeaProcs,
  DMConnection, Validate, DateEdit, DB, ADODB, UtilProc, DmiCtrls, ListBoxEx,
  DBListEx, ExtCtrls, Abm_obj, DbList, RStrings;

type
  TConveniosMorosos = class(TForm)
    grpDatosConvenio: TGroupBox;
    Label1: TLabel;
    cmbConvenios: TVariantComboBox;
    peNumeroDocumento: TPickEdit;
    Label6: TLabel;
    pnlDatos: TPanel;
    Label4: TLabel;
    dtDesde: TDateEdit;
    Label2: TLabel;
    dtHasta: TDateEdit;
    pnlBotones: TPanel;
    Notebook1: TNotebook;
    DataSource1: TDataSource;
    AbmToolbar: TAbmToolbar;
    AbmList1: TAbmList;
    btnSalir: TButton;
    btnCancelar: TButton;
    btnAceptar: TButton;
    spActualizarFechasConvenioMoroso: TADOStoredProc;
    DateTimeField1: TDateTimeField;
    DateTimeField2: TDateTimeField;
    spObtenerCliente: TADOStoredProc;
    procedure peNumeroDocumentoKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure peNumeroDocumentoChange(Sender: TObject);
    procedure AbmToolbarClose(Sender: TObject);
    procedure AbmList1DrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure AbmList1Edit(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure AbmList1Insert(Sender: TObject);
    procedure AbmList1Click(Sender: TObject);
    procedure AbmList1Delete(Sender: TObject);
    procedure cmbConveniosChange(Sender: TObject);
    procedure peNumeroDocumentoButtonClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure dtDesdeChange(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
    procedure MostrarDatos(NroDoc: String);
    procedure EstadoForm(Activado: TAbmState);

  private
    FUltimaBusqueda: TBusquedaCliente;
    FCodigoPersona: integer;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
  public
    function Inicializar(Titulo: string): boolean;
  end;


implementation

ResourceString
    MSG_AVISO           = 'Atenci�n.';
    MSG_ERROR_SP = 'Error al intentar ejecutar SP %s, con c�digo de acci�n %s';

var
	f:  TFormBuscaClientes;
    FechaClave: TDate;          //para buscar por la clave CodigoContrato - FechaDesde en tbl ConvenioMorosos
    Cliente: string;

{$R *.dfm}

{ TConveniosMorosos }

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    ddiaz
  Date Created: 15/05/2006
  Description:
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}

function TConveniosMorosos.Inicializar(Titulo: String): boolean;
begin
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216
    self.Caption:=Titulo;
    FormStyle := fsMDIChild;
    Notebook1.ActivePage:='pgSalida';
    Notebook1.PageIndex:=0;
    spActualizarFechasConvenioMoroso.Parameters.Refresh;
    Result:=true;
end;

{-----------------------------------------------------------------------------
  Procedure Name: btnAceptarClick
  Author:    ddiaz
  Date Created: 16/05/2006
  Description: ejecuta el sp con la l�gica para procesamiento de las acciones.
  Parameters:
-----------------------------------------------------------------------------}
procedure TConveniosMorosos.btnAceptarClick(Sender: TObject);
ResourceString
    MSG_ERROR_SOLAPA    = 'El rango de fechas ingresado se solapa con uno ya existente.';
    MSG_RANGO_OCUPADO   = 'Rangos de fechas en solapamiento:';
    MSG_ERROR_FECHAS    = 'La [Fecha Hasta] no puede ser inferior a la [Fecha Desde].';
var
    ListaDeRangos: TStringList;
begin

    if (dtHasta.Date < dtDesde.Date) and (not dtHasta.IsEmpty) then begin
        MsgBoxErr(MSG_ERROR_FECHAS, MSG_AVISO, MSG_AVISO, MB_ICONWARNING);
        dtHasta.SetFocus;
        Exit;
    end;

    if AbmList1.Estado<>Normal then begin
        with spActualizarFechasConvenioMoroso, Parameters do begin
        try
                Close;
                ParamByName('@CodConvenio').Value   :=cmbConvenios.Value;
                ParamByName('@FecDesdeClave').Value :=VarToDateTime(iif(AbmList1.Estado=Modi, FechaClave, dtDesde.Date)); //FechaClave: fecha para la busqueda en el SP
                ParamByName('@FecDesde').Value      :=dtDesde.Date;
                ParamByName('@FecHasta').Value      :=iif(dtHasta.Date<1, null, dtHasta.Date) ;
                ParamByName('@Accion').Value        :=AbmList1.Estado;
                open;
                if ParamByName('@RETURN_VALUE').Value = 1 then begin
                    ListaDeRangos:=TStringList.Create;
                    ListaDeRangos.Append(MSG_RANGO_OCUPADO);
                    while not Eof do begin
                        ListaDeRangos.Append(   FieldByName('FechaDesde').AsString+'   '+
                                                iif(FieldByName('FechaHasta').AsString='', 'En adelante', FieldByName('FechaHasta').AsString)+
                                                chr(13)
                                            );
                        Next;
                    end;
                    ListaDeRangos.Append(chr(13));
                    MsgBoxErr(MSG_ERROR_SOLAPA, ListaDeRangos.Text, MSG_AVISO, MB_ICONWARNING);
                end;
            AbmList1.Estado:=Normal;
            MostrarDatos(peNumeroDocumento.Text);
            except
                on E: Exception do begin
                    MsgBoxErr(Format(MSG_ERROR_SP,[ProcedureName, VarToStr(AbmList1.Estado) ]), E.Message, Self.Caption, MB_ICONERROR);
                    Exit;
                end;
            end;
        end;
    end;
end;

procedure TConveniosMorosos.dtDesdeChange(Sender: TObject);
begin
    if Abmlist1.Estado<>Normal then begin
        if dtDesde.Date >= 1 then begin
            dtHasta.Enabled:=True;
            btnAceptar.Enabled:=True; end
        else begin
            dtHasta.Enabled:=False;
            btnAceptar.Enabled:=False;
            btnCancelar.Enabled:=True;
        end;
    end
end;

procedure TConveniosMorosos.btnCancelarClick(Sender: TObject);
begin
    AbmList1.Estado:=Normal;
    EstadoForm(Normal);
end;

procedure TConveniosMorosos.peNumeroDocumentoButtonClick(Sender: TObject);
begin
    Application.CreateForm(TFormBuscaClientes, f);
    FUltimaBusqueda.RUT:=peNumeroDocumento.Text;
    if f.Inicializa(FUltimaBusqueda)then begin
    	if (f.ShowModal = mrok) then begin
            FCodigoPersona := QueryGetValueInt(DMConnections.BaseCAC, Format(
                'SELECT CodigoPersona FROM Personas (NOLOCK) WHERE CodigoDocumento = ''RUT'' AND NumeroDocumento = ''%s''',
                [PadL(Trim(FUltimaBusqueda.RUT), 9, '0')]));
            Cliente:=f.Persona.Apellido + ', ' +f.Persona.Nombre;
            CargarConveniosRUT(DMConnections.BaseCAC, cmbConvenios, 'RUT', f.Persona.NumeroDocumento);
            MostrarDatos(Trim(f.Persona.NumeroDocumento));
	   	end;
    end;
end;


procedure TConveniosMorosos.MostrarDatos(NroDoc: String);
resourcestring
    MSG_NO_EXISTEN = 'No existen registros de morosidad para el cliente %s en el convenio %s.';
begin
    peNumeroDocumento.Text:=NroDoc;
    with spActualizarFechasConvenioMoroso, Parameters do begin
        try
        Close;
        ParamByName('@CodConvenio').Value:=cmbConvenios.Value;
        ParamByName('@FecDesdeClave').Value  :=null;
        ParamByName('@FecDesde').Value  :=null;
        ParamByName('@FecHasta').Value  :=null;
        ParamByName('@Accion').Value    :=AbmList1.Estado;
        open;
            AbmList1.Refresh;
            if (Recordset.RecordCount = 0) then
                MsgBox(Format(MSG_NO_EXISTEN, [Cliente, cmbConvenios.Text]) , MSG_AVISO, MB_OK);
        except
            on E: Exception do begin
                MsgBoxErr(Format(MSG_ERROR_SP,[ProcedureName, VarToStr(AbmList1.Estado)]), E.Message, Self.Caption, MB_ICONERROR);
                Exit;
            end;
        end;
    end;
    EstadoForm(AbmList1.Estado);
end;

procedure TConveniosMorosos.cmbConveniosChange(Sender: TObject);
begin
    MostrarDatos(peNumeroDocumento.Text);
end;

procedure TConveniosMorosos.AbmList1Delete(Sender: TObject);
resourcestring
    MSG_PREGUNTA_ELIMINAR = '�Est� seguro que desea eliminar el registro con el rango de fecha  %s  a  %s  ?';
var
    FechaDesde: TDateTime;
begin
        if MsgBox(Format(MSG_PREGUNTA_ELIMINAR,[DateTimeToStr(dtDesde.Date), DateTimeToStr(dtHasta.Date)]) , MSG_AVISO,
                          MB_YESNO + MB_ICONQUESTION) = IDYES then begin
            FechaDesde:=Abmlist1.Table.FieldByName('FechaDesde').Value;
            with spActualizarFechasConvenioMoroso, Parameters do begin
                try
                Close;
                ParamByName('@CodConvenio').Value:=cmbConvenios.Value;
                ParamByName('@FecDesdeClave').Value  :=null;
                ParamByName('@FecDesde').Value  := FechaDesde;
                ParamByName('@FecHasta').Value  :=null;
                ParamByName('@Accion').Value    :=AbmList1.Estado;
                ExecProc;
                except
                    on E: Exception do begin
                        MsgBoxErr(Format(MSG_ERROR_SP,[ProcedureName, VarToStr(AbmList1.Estado)]), E.Message, Self.Caption, MB_ICONERROR);
                        Exit;
                    end;
                end;
            end;
        end;
        AbmList1.Estado:=Normal;
        MostrarDatos(peNumeroDocumento.Text);
end;

procedure TConveniosMorosos.AbmList1Click(Sender: TObject);
begin
    dtDesde.Date:=AbmList1.Table.FieldByName('FechaDesde').Value;
    dtHasta.Date:=iif(AbmList1.Table.FieldByName('FechaHasta').Value<> null,
                        AbmList1.Table.FieldByName('FechaHasta').Value,0);
end;

procedure TConveniosMorosos.AbmList1Insert(Sender: TObject);
begin
    EstadoForm(Alta);
    dtDesde.SetFocus;
end;

procedure TConveniosMorosos.EstadoForm(Activado: TAbmState);
begin
    AbmList1.Reload;
    if Activado=Normal then begin
        pnlDatos.Enabled:=False;
        dtDesde.Enabled:=False;
        dtHasta.Enabled:=False;
        btnAceptar.Enabled:=False;
        btnCancelar.Enabled:=False;
        peNumeroDocumento.SetFocus;
        Notebook1.PageIndex:=0;
        Notebook1.ActivePage:='pgSalida';
        if  spActualizarFechasConvenioMoroso.Active then
            if spActualizarFechasConvenioMoroso.RecordCount>0 then
                AbmToolbar.Habilitados:=[btAlta, btBaja, btModi, btSalir]
            else
                AbmToolbar.Habilitados:=[btAlta, btSalir]
        else
            AbmToolbar.Habilitados:=[btSalir];
    end else begin
        pnlDatos.Enabled:=True;
        dtDesde.Enabled:=True;
        dtHasta.Enabled:=True;
        btnAceptar.Enabled:=True;
        btnCancelar.Enabled:=True;
        Notebook1.PageIndex:=1;
        Notebook1.ActivePage:='pgOKCan';
        AbmToolbar.Habilitados:=[];
    end;

end;

procedure TConveniosMorosos.AbmList1Edit(Sender: TObject);
begin
    EstadoForm(Modi);
    FechaClave:=dtDesde.Date;
    dtDesde.SetFocus;
end;

procedure TConveniosMorosos.btnSalirClick(Sender: TObject);
begin
    self.Release
end;


procedure TConveniosMorosos.AbmList1DrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	with Sender.Canvas, tabla do begin
        if FieldByName('FechaDesde').Value <> null then begin
            FillRect(Rect);
            TextOut(Cols[0], Rect.Top, FormatDateTime('dd/mm/yyyy',FieldByName('FechaDesde').Value));
            TextOut(Cols[1], Rect.Top, iif(FieldByName('FechaHasta').Value <> null,
                                          FieldByName('FechaHasta').Value, 'En adelante'));
        end;
	end;
end;


procedure TConveniosMorosos.AbmToolbarClose(Sender: TObject);
begin
    self.Release
end;

procedure TConveniosMorosos.peNumeroDocumentoChange(Sender: TObject);
begin
    AbmToolbar.Habilitados:=[btSalir];
    spActualizarFechasConvenioMoroso.Close;
    EstadoForm(Normal);
end;


procedure TConveniosMorosos.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action:=caFree;
end;

procedure TConveniosMorosos.peNumeroDocumentoKeyPress(Sender: TObject; var Key: Char);
begin
    if key=chr(13) then begin
        with spObtenerCliente, Parameters do begin
    close;
          ParamByName('@CodigoDocumento').Value:='RUT';
          ParamByName('@NumeroDocumento').Value:=Trim(peNumeroDocumento.Text);
          open;
          if Recordset.RecordCount = 0 then
              peNumeroDocumentoButtonClick(sender)
          else begin
              Cliente:= trim(FieldByName('Apellido').Value) + ', '+ trim(FieldByName('Nombre').Value);
              CargarConveniosRUT(DMConnections.BaseCAC, cmbConvenios, 'RUT', peNumeroDocumento.Text);
              MostrarDatos(peNumeroDocumento.Text);
              self.Caption:= 'Convenios Morosos' +' - '+ Cliente;
          end;
        end;
    end;

end;

end.
