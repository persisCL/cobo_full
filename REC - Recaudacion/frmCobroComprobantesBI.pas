{-----------------------------------------------------------------------------
 File Name: frmCobroComprobantesBI.pas
 Author:   fmalisia
 Date Created: 08/12/2005
 Language: ES-AR
 Description: Cobro de Comprobantes Infractor

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)


-----------------------------------------------------------------------------}
unit frmCobroComprobantesBI;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ListBoxEx, DBListEx, StdCtrls, DPSControls, VariantComboBox,
  DmiCtrls, DB, ADODB, DMCOnnection, utilDB, UtilFacturacion, utilProc, buscaClientes,
  PeaTypes, RStrings, DBCtrls, peaProcs, Util, PagoVentanilla, ExtCtrls, ReporteRecibo,
  FrmImprimir, DBClient, ImgList, frmRptNotaCobroInfractor;

const
	KEY_F9 	= VK_F9;
type
  TformCobroComprobantesBI = class(TForm)
  	gbPorCliente: TGroupBox;
    gb_DatosPersona: TGroupBox;
    Label6: TLabel;
    lblNombre: TLabel;
    lblDomicilio: TLabel;
	  Label10: TLabel;
    gbDatosComprobante: TGroupBox;
    Label2: TLabel;
  	lblFecha: TLabel;
    lblFechaVencimiento: TLabel;
    Label7: TLabel;
    lblTotalAPagar: TLabel;
    Label12: TLabel;
    ObtenerLineasNotaCobroInfraccion: TADOStoredProc;
    lbl_NumeroComprobante: TLabel;
    edNumeroComprobante: TNumericEdit;
    lbl_Estado: TLabel;
    Label11: TLabel;
    Label8: TLabel;
    lblRUT: TLabel;
    dblLineasComprobante: TDBListEx;
    dsLineasNotaCObroInfraccion: TDataSource;
    Img_Tilde: TImageList;
    lbl_TitTotalComprobantes: TLabel;
    lbl_TotalComprobantes: TLabel;
    btnCobrar: TButton;
    btnSalir: TButton;
    ObtenerNotaCobroInfraccion: TADOStoredProc;
    Timer1: TTimer;
    Label1: TLabel;
    btnReimprimirNI: TButton;
  	procedure FormCreate(Sender: TObject);
  	procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCobrarClick(Sender: TObject);
  	procedure cbComprobantesChange(Sender: TObject);
  	procedure edNumeroComprobanteChange(Sender: TObject);
  	procedure FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
  	procedure Timer1Timer(Sender: TObject);
    procedure dblLineasComprobanteDrawText(Sender: TCustomDBListEx;Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState; var Text: String; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
    procedure btnReimprimirNIClick(Sender: TObject);
  private
    { Private declarations }
    FLogo: TBitmap;
    FTotalApagar: Double;
    FNumeroPOS, FPuntoEntrega, FPuntoVenta: Integer;
    // C�digo del canal de pago a setear por defecto en el form de registar pago.
    FCodigoCanalPago: Integer;
    // Monto Total de los comprobantes seleccionados para cobrar.
    FTotalComprobante: extended;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    function  CargarLineasComprobante : boolean;
    procedure MostrarDatosComprobante;
  	procedure LimpiarCampos;
    procedure LimpiarDetalles;
    procedure LimpiarListaComprobantes;
    function ObtenerComprobantesCobrar: TListaComprobante;
  	procedure Cobrar;
  public
    { Public declarations }
    function Inicializar(NumeroPOS, PuntoEntrega, PuntoVenta: Integer): Boolean; overload;
  	function Inicializar(TipoComprobante: AnsiString; NumeroComprobante: Int64; NumeroPOS, PuntoEntrega, PuntoVenta: Integer): Boolean; Overload;
  end;

var
  formCobroComprobantesBI: TformCobroComprobantesBI;

implementation

const
	SetPACPAT = [1, 2];


{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    fmalisia
  Date Created: 08/12/2005
  Description: Inicializa el Form - Carga los tipos de Comprobantes
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TformCobroComprobantesBI.Inicializar(NumeroPOS, PuntoEntrega, PuntoVenta: Integer): Boolean;
resourcestring
	MSG_LOGO_NO_ENCONTRADO  = 'No se ha encontrado el archivo con la imagen del logo para el recibo.';
	MSG_CAPTION             = 'Cobro de Comprobantes';
	MSG_ERROR_INICIALIZAR   = 'Error inicializando el formulario.';
begin
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

    try
        FTotalComprobante := 0;
        FCodigoCanalPago := CONST_CANAL_PAGO_VENTANILLA;
        // Por ahora lo cargamos a mano
        lbl_Estado.Caption := EmptyStr;
        MostrarDatosComprobante;
        FNumeroPOS := NumeroPOS;
        FPuntoEntrega := PuntoEntrega;
        FPuntoVenta := PuntoVenta;
        FormStyle := fsMDIChild;
        CenterForm(Self);
        FLogo := TBitmap.Create;
        (* Levanto el logo aqu� para que se haga una sola vez, independientemente de los reintentos
        de impresi�n que haga el operador. *)
        if not LevantarLogo (FLogo) then MsgBox(MSG_LOGO_NO_ENCONTRADO, MSG_CAPTION, MB_ICONINFORMATION + MB_OK);
        edNumeroComprobante.SetFocus();
        Result := True;
    except
	   	  on e: exception do begin
			      MsgBoxErr(MSG_ERROR_INICIALIZAR, E.Message, MSG_CAPTION, MB_ICONERROR);
			      Result := False;
        end;
    end; 
end;

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    fmalisia
  Date Created: 08/12/2005
  Description: Inicializa el Form - Carga los tipos de Comprobantes y busca
				el comprobante seleccionado
  Parameters: TipoComprobante: AnsiString; NumeroComprobante: Int64
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TformCobroComprobantesBI.Inicializar(TipoComprobante: AnsiString; NumeroComprobante: Int64; NumeroPOS, PuntoEntrega, PuntoVenta: Integer): Boolean;
resourcestring
	MSG_LOGO_NO_ENCONTRADO = 'No se ha encontrado el archivo con la imagen del logo para el recibo.';
	MSG_CAPTION			   = 'Cobro de Comprobantes';
	MSG_ERROR_INICIALIZAR   = 'Error inicializando el formulario.';
begin
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216
    
    try
        FTotalComprobante           := 0;
        FCodigoCanalPago := CONST_CANAL_PAGO_VENTANILLA;

        // Por ahora lo cargamos a mano
        lbl_Estado.Caption := EmptyStr;

        edNumeroComprobante.Value := NumeroComprobante;
        FNumeroPOS := NumeroPOS;
        FPuntoEntrega := PuntoEntrega;
        FPuntoVenta := PuntoVenta;

        FormStyle := fsMDIChild;
        CenterForm(Self);

        Result := CargarLineasComprobante;

        if Result then begin

            FLogo := TBitmap.Create;
            (* Levanto el logo aqu� para que se haga una sola vez, independientemente de los reintentos
            de impresi�n que haga el operador. *)
            if not LevantarLogo (FLogo) then MsgBox(MSG_LOGO_NO_ENCONTRADO, MSG_CAPTION, MB_ICONINFORMATION + MB_OK);
        	  edNumeroComprobante.SetFocus();
        end;
    except
        on e: exception do begin
      			MsgBoxErr(MSG_ERROR_INICIALIZAR, E.Message, MSG_CAPTION, MB_ICONERROR);
    			Result := False;
        end;
    end; // except

end;

{-----------------------------------------------------------------------------
  Function Name: FormCreate
  Author: fmalisia
  Date Created: 08/12/2005
  Description: centro el formulario al crearlo
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure TformCobroComprobantesBI.FormCreate(Sender: TObject);
begin
    CenterForm(Self);
end;

{-----------------------------------------------------------------------------
  Function Name: FormCreate
  Author: fmalisia
  Date Created: 08/12/2005
  Description: centro el formulario al crearlo
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TformCobroComprobantesBI.CargarLineasComprobante : boolean;
begin
    // Cargamos la lista de comprobantes.
    LimpiarListaComprobantes;
    try
        with ObtenerLineasNotaCobroInfraccion, Parameters do begin
            Close;
            ParamByName('@NumeroComprobante').Value := IIF( edNumeroComprobante.Text <> '', edNumeroComprobante.Value, NULL);
            Open;
            Result := True;
        end;
    except
        result := false;
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: MostrarDatosComprobante
  Author:    flamas
  Date Created: 20/12/2004
  Description: Muestra los datos del Comprobante
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformCobroComprobantesBI.MostrarDatosComprobante;
begin
    LimpiarDetalles;
    with ObtenerNotaCobroInfraccion do begin
        if Active = False then exit;
        lblRUT.Caption := FieldByName('NumeroDocumento').AsString;
        lblNombre.Caption := FieldByName('Apellido').AsString + ', ' + FieldByName('Nombre').AsString;
        lblDomicilio.Caption := FieldByName('Calle').AsString + ' ' +FieldByName('Numero').AsString+ ' , ' + FieldByName('DetalleDomicilio').AsString+ ' , ' + FieldByName('Comuna').AsString + ' , ' + FieldByName('Region').AsString;
        lblFecha.Caption := FormatDateTime('dd/mm/yyyy', FieldByName('FechaEmision').AsDateTime);
        lblFechaVencimiento.Caption := FormatDateTime('dd/mm/yyyy', FieldByName('FechaVencimiento').AsDateTime);
        lblTotalAPagar.Caption := FormatFloat(FORMATO_IMPORTE , FieldByName('TotalAPagar').AsFloat);
        FTotalApagar := Round(FieldByName('TotalAPagar').AsFloat);
        lbl_Estado.Caption := FieldByName('DescriEstado').AsString;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: LimpiarDetalles;
  Author:    flamas
  Date Created: 26/01/2005
  Description: Borra los detalles del Comprobante
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformCobroComprobantesBI.LimpiarDetalles;
begin
    lblRUT.Caption                  := EmptyStr;
  	lblNombre.Caption               := EmptyStr;
  	lblDomicilio.Caption            := EmptyStr;
  	lblFecha.Caption                := EmptyStr;
  	lblFechaVencimiento.Caption	    := EmptyStr;
    lblTotalAPagar.Caption          := EmptyStr;
    lbl_Estado.Caption              := EmptyStr;
  	btnCobrar.Enabled     		    := False;
end;

{-----------------------------------------------------------------------------
  Function Name: cbComprobantesChange
  Author:    flamas
  Date Created: 21/12/2004
  Description: Procesa un cambio de comprobante y muestra los datos
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformCobroComprobantesBI.cbComprobantesChange(Sender: TObject);
begin
    if ActiveControl <> Sender then Exit;
    MostrarDatosComprobante;
end;


{-----------------------------------------------------------------------------
  Function Name: edNumeroComprobanteChange
  Author:    flamas
  Date Created: 21/12/2004
  Description: Procesa un cambio de comprobante y muestra los datos del comprobante
  Parameters: Sender: TObject
  Return Value: None
  -----------------------------------------------------------------------------}
procedure TformCobroComprobantesBI.edNumeroComprobanteChange(Sender: TObject);
begin
    if ActiveControl <> Sender then Exit;
    BTNCobrar.Enabled := False;
    LimpiarListaComprobantes;

  	if Timer1.Enabled then begin
        Timer1.Enabled := False;
        Timer1.Enabled := True;
    end else Timer1.Enabled := True;
end;

{-----------------------------------------------------------------------------
  Function Name: btnCobrarClick
  Author:    flamas
  Date Created: 27/01/2005
  Description: Cobra un Comprobante
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformCobroComprobantesBI.btnCobrarClick(Sender: TObject);
resourcestring
    MSG_NEWER_INVOICE = 'Existe un comprobante emitido m�s nuevo.';
  	MSG_WISH_SEE_NEW_INVOICE = 'Desea ver el nuevo comprobante ?';
  	MSG_SENT_FOR_AUTOMATIC_PAYMENT = 'El comprobante ha sido enviado para su d�bito autom�tico.' + #10#13 + '� Desea pagarlo de todos modos ?';
  	MSG_CAPTION	= 'Cobro de Comprobantes';
begin
    Cobrar;
end;

{-----------------------------------------------------------------------------
  Function Name: ObtenerComprobantesCobrar
  Author:
  Date Created:  13/10/2005
  Description:
  Parameters:
  Return Value:
-----------------------------------------------------------------------------}
function TformCobroComprobantesBI.ObtenerComprobantesCobrar: TListaComprobante;
var
    Compro: TComprobante;
begin
    Result := Nil;

    Result := TListaComprobante.Create;

    Compro := TComprobante.Create;
    with ObtenerNotaCobroInfraccion do begin
        Compro.CodigoCliente        := FieldByName('CodigoPersona').AsInteger;
        Compro.TipoComprobante      := FieldByName('TipoComprobante').AsString;
        Compro.NumeroComprobante    := FieldByName('NumeroComprobante').AsInteger;
        Compro.FechaEmision         := FieldByName('FechaEmision').AsDateTime;
        Compro.FechaVencimiento     := FieldByName('FechaVencimiento').AsDateTime;
        Compro.TotalComprobante     := FieldByName('TotalComprobante').AsFloat;
        Compro.TotalAPagar          := FieldByName('TotalAPagar').AsFloat;
        Compro.TotalPagos           := FieldByName('TotalPagos').AsFloat;
        Compro.Pagado               := (FieldByName('EstadoPago').AsString = COMPROBANTE_PAGO);
        Compro.EstadoDebito         := FieldByName('EstadoDebito').AsString;
        Compro.DescriEstadoPago     := FieldByName('DescriEstado').AsString;
        Compro.UltimoComprobante    := FieldByName('UltimoComprobante').AsInteger;
        Result.Add(Compro);

        if (Result.Count = 0) then begin
            FreeAndNil(Result);
        end;
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: Cobrar
  Author:    flamas
  Date Created: 27/01/2005
  Description: Cobra un Comprobante
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformCobroComprobantesBI.Cobrar;
resourcestring
	MSG_REGISTRO_CORRECTO = 'El pago se ha registrado correctamente.';
	MSG_CONFIRMAR_RECIBO = '�Desea Imprimir el Comprobante de Cancelaci�n?';
	MSG_CAPTION = 'Registrar Pago';
	MSG_IMPRESION_EXITOSA = '�La impresi�n se ha realizado con �xito?';
var


	r: TformRecibo;
	f: TfrmPagoVentanilla;
	RespuestaImpresion: TRespuestaImprimir;
    ComprobantesCobrar: TListaComprobante;
begin
    (* Armar la lista con los comprobantes a cobrar. *)
    ComprobantesCobrar := ObtenerComprobantesCobrar;
    try
        Application.CreateForm(TfrmPagoVentanilla, f);
        if f.Inicializar(ComprobantesCobrar, lblRUT.Caption, lblNombre.Caption,
                FNumeroPOS, FPuntoEntrega, FPuntoVenta, FCodigoCanalPago, True) then f.ShowModal;
            if f.ModalResult = mrOk then begin
                (* Almacenar el c�digo de canal de pago utilizado. *)
                FCodigoCanalPago := f.CodigoCanalPago;

                if f.ImprimirRecibo then begin
                    (* Mostrar el di�logo previo a realizar la impresi�n. *)
                    RespuestaImpresion := frmImprimir.Ejecutar(MSG_CAPTION,
                      MSG_REGISTRO_CORRECTO + CRLF + MSG_CONFIRMAR_RECIBO);
                    case RespuestaImpresion of
                        (* Si la respuesta fue Aceptar, ejecutar el reporte. *)
                        riAceptarConfigurarImpresora, riAceptar: begin
                            Application.CreateForm(TformRecibo, r);

                            try
                                if r.Inicializar(FLogo, f.NumeroRecibo, RespuestaImpresion = riAceptarConfigurarImpresora) then begin
                                    (* Preguntar si la impresi�n fue exitosa. *)
                                    while MsgBox(MSG_IMPRESION_EXITOSA, MSG_CAPTION, MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = mrNo do begin
                                        (* Mostrar el di�logo de Configurar Impresora para permitir al operador
                                        reintentar la impresi�n. *)
                                        if not r.Inicializar(FLogo, f.NumeroRecibo, True) then Break;
                                    end; // while
                                end; // if
                            finally
                                r.Release;
                            end
                        end;
                    end; // case
                end;
                LimpiarCampos;
            end;
        f.Free;
    finally
        ComprobantesCobrar.Free;
    end; // finally
end;


{-----------------------------------------------------------------------------
  Procedure: TformPagoComprobantes.LimpiarCampos
  Author:    gcasais
  Date:      19-Ene-2005
  Arguments: None
  Result:    None
-----------------------------------------------------------------------------}
procedure TformCobroComprobantesBI.LimpiarCampos;
begin
  	edNumeroComprobante.Clear;
    LimpiarListaComprobantes;
  	LimpiarDetalles;
    edNumeroComprobante.SetFocus;
end;

{-----------------------------------------------------------------------------
  Function Name: FormKeyDown
  Author:
  Date Created:  13/10/2005
  Description:
  Parameters:
  Return Value:
-----------------------------------------------------------------------------}
procedure TformCobroComprobantesBI.FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
    if (Key = KEY_F9) and btnCobrar.Enabled then btnCobrar.Click;
end;

{-----------------------------------------------------------------------------
  Function Name: Timer1Timer
  Author:
  Date Created:  13/10/2005
  Description:
  Parameters:
  Return Value:
-----------------------------------------------------------------------------}
procedure TformCobroComprobantesBI.Timer1Timer(Sender: TObject);
begin
    LimpiarDetalles;
    Timer1.Enabled := False;
    with ObtenerNotaCobroInfraccion, Parameters do begin
        close;
        Refresh;
        ParamByName('@NumeroComprobante').value := edNumeroComprobante.value;
        open;
        if not isEmpty then begin
            MostrarDatosComprobante;
            CargarLineasComprobante;
            //Muestro el total a pagar
            lbl_TotalComprobantes.Caption := formatFloat(FORMATO_IMPORTE, FieldByName('TotalAPagar').AsFloat - FieldByName('TotalPagos').AsFloat);
            //Si la nota de cobro esta impaga
            if Fieldbyname('EstadoPago').AsString = 'I' then begin
                //Permito pagar el comprobante
                BTNCobrar.Enabled := True;
            end;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: dl_ComprobantesDrawText
  Author:
  Date Created:  13/10/2005
  Description:
  Parameters:
  Return Value:
-----------------------------------------------------------------------------}
procedure TformCobroComprobantesBI.dblLineasComprobanteDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;State: TOwnerDrawState; var Text: String; var TxtRect: TRect;var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
    with Sender do begin
        if Column.FieldName = 'Importe' then begin
            Text := FormatFloat(FORMATO_IMPORTE , ObtenerLineasNotaCobroInfraccion.FieldByName('Importe').AsFloat);
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: LimpiarListaComprobantes
  Author: lgisuk
  Date Created:  13/10/2005
  Description:  Limpio la lista de comprobantes
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformCobroComprobantesBI.LimpiarListaComprobantes;
begin
    //Limpio el total de los comprobantes
    FTotalComprobante               := 0;
    lbl_TotalComprobantes.Caption   := '';
    //Limpio la Grilla
    with ObtenerLineasNotaCobroInfraccion, Parameters do begin
      Close;
      ParamByName('@NumeroComprobante').Value := -1;
      Open;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnSalirClick
  Author:    lgisuk
  Date Created: 13/10/2005
  Description: Permito salir del formulario
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure TformCobroComprobantesBI.btnReimprimirNIClick(Sender: TObject);
var
    fReporte: TfRptNotaCobroInfractor;
begin
    fReporte := TfRptNotaCobroInfractor.Create(self);
    freporte.Inicializar(edNumeroComprobante.ValueInt);
    freporte.Release;
end;

procedure TformCobroComprobantesBI.btnSalirClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 13/10/2005
  Description: Libero el Form de Memoria
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure TformCobroComprobantesBI.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    FLogo.Free;
    Action := caFree;
end;


end.
