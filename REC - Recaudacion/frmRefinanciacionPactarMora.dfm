object RefinanciacionPactarMoraForm: TRefinanciacionPactarMoraForm
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = ' Pactar Mora Cuota'
  ClientHeight = 115
  ClientWidth = 277
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  DesignSize = (
    277
    115)
  PixelsPerInch = 96
  TextHeight = 13
  object pnlAcciones: TPanel
    Left = 0
    Top = 75
    Width = 277
    Height = 40
    Align = alBottom
    Anchors = []
    Color = 14408667
    ParentBackground = False
    TabOrder = 0
    DesignSize = (
      277
      40)
    object btnCancelar: TButton
      Left = 197
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Cancelar'
      TabOrder = 0
      OnClick = btnCancelarClick
    end
    object Button1: TButton
      Left = 118
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Aceptar'
      TabOrder = 1
      OnClick = Button1Click
    end
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 261
    Height = 61
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = '  Introduzca la Nueva Fecha de la Cuota  '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    object Label1: TLabel
      Left = 49
      Top = 28
      Width = 33
      Height = 13
      Caption = 'Fecha:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object dedtNuevaFecha: TDateEdit
      Left = 96
      Top = 24
      Width = 90
      Height = 21
      AutoSelect = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      Date = -693594.000000000000000000
    end
  end
end
