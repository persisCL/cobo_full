object FrmTratamientoMorosos: TFrmTratamientoMorosos
  Left = 199
  Top = 121
  Width = 746
  Height = 538
  Anchors = []
  Caption = 'Tratamiento de Morosos'
  Color = clBtnFace
  Constraints.MinHeight = 465
  Constraints.MinWidth = 650
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  Visible = True
  OnClose = FormClose
  DesignSize = (
    738
    504)
  PixelsPerInch = 96
  TextHeight = 13
  object btnSalir: TDPSButton
    Left = 658
    Top = 472
    Anchors = [akRight, akBottom]
    Caption = '&Salir'
    TabOrder = 4
    OnClick = btnSalirClick
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 738
    Height = 466
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelOuter = bvLowered
    TabOrder = 0
    DesignSize = (
      738
      466)
    object GroupBox2: TGroupBox
      Left = 6
      Top = 66
      Width = 726
      Height = 195
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = ' Comprobantes en Mora '
      TabOrder = 1
      DesignSize = (
        726
        195)
      object dbgComprobantesMorosos: TDPSGrid
        Left = 8
        Top = 22
        Width = 710
        Height = 161
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataSource = dsComprobantesMorosos
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Options = [dgTitles, dgColumnResize, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgMultiSelect]
        ParentFont = False
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Apellido'
            Width = 160
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nombre'
            Width = 160
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Recurrente'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DescripcionAccion'
            Title.Caption = 'Acci'#243'n'
            Width = 160
            Visible = True
          end
          item
            Alignment = taRightJustify
            Expanded = False
            FieldName = 'TipoComprobante'
            Title.Caption = 'Tipo'
            Width = 30
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NumeroComprobante'
            Title.Caption = 'N'#250'mero'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Fecha'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Vencimiento'
            Title.Caption = 'Notificaci'#243'n'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ImporteTotal'
            Title.Caption = 'Monto'
            Width = 80
            Visible = True
          end>
      end
    end
    object GroupBox1: TGroupBox
      Left = 6
      Top = 1
      Width = 726
      Height = 64
      Anchors = [akLeft, akTop, akRight]
      Caption = ' Par'#225'metros de B'#250'squeda '
      TabOrder = 0
      object Label5: TLabel
        Left = 10
        Top = 16
        Width = 75
        Height = 13
        Caption = 'Tipo de Acci'#243'n:'
      end
      object cbTiposAcciones: TComboBox
        Left = 10
        Top = 30
        Width = 320
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 0
        OnClick = cbTiposAccionesClick
      end
    end
    object GroupBox3: TGroupBox
      Left = 4
      Top = 264
      Width = 726
      Height = 193
      Anchors = [akLeft, akRight, akBottom]
      Caption = ' Informaci'#243'n Hist'#243'rica '
      TabOrder = 2
      DesignSize = (
        726
        193)
      object dbgInformacionHistorica: TDPSGrid
        Left = 8
        Top = 22
        Width = 710
        Height = 162
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataSource = dsInformacionHistorica
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Options = [dgTitles, dgColumnResize, dgTabs, dgRowSelect, dgAlwaysShowSelection]
        ParentFont = False
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'DescripcionAccion'
            Title.Caption = 'Acci'#243'n'
            Width = 320
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AccionAdicional'
            Title.Caption = 'Adicional'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DiasPostergacion'
            Title.Caption = 'D'#237'as de Postergaci'#243'n'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Fecha'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MedioContacto'
            Title.Caption = 'Medio de Comunicaci'#243'n'
            Width = 160
            Visible = True
          end>
      end
    end
  end
  object btnEjecutar: TDPSButton
    Left = 8
    Top = 472
    Width = 107
    Anchors = [akLeft, akBottom]
    Caption = '&Ejecutar'
    Enabled = False
    TabOrder = 1
    OnClick = btnEjecutarClick
  end
  object btnPostergar: TDPSButton
    Left = 120
    Top = 472
    Width = 107
    Anchors = [akLeft, akBottom]
    Caption = '&Postergar'
    Enabled = False
    TabOrder = 2
    OnClick = btnPostergarClick
  end
  object btnAccionAdicional: TDPSButton
    Left = 232
    Top = 472
    Width = 107
    Anchors = [akLeft, akBottom]
    Caption = '&Acci'#243'n Adicional'
    Enabled = False
    TabOrder = 3
    OnClick = btnAccionAdicionalClick
  end
  object dsComprobantesMorosos: TDataSource
    DataSet = ObtenerMorosos
    OnDataChange = dsComprobantesMorososDataChange
    Left = 592
    Top = 472
  end
  object ObtenerMorosos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerMorosos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@IDConfiguracionNotificacion'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 560
    Top = 472
  end
  object dsInformacionHistorica: TDataSource
    DataSet = qryInformacionHistorica
    Left = 528
    Top = 472
  end
  object qryInformacionHistorica: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'TipoComprobante'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = 'F'
      end
      item
        Name = 'NumeroComprobante'
        Attributes = [paSigned]
        DataType = ftBCD
        Precision = 18
        Size = 19
        Value = 10000c
      end>
    SQL.Strings = (
      'SELECT '
      'ConfiguracionNotificaciones.DescripcionAccion,'
      'CASE HistorialMorosos.AccionAdicional'
      #9'WHEN 0 THEN '#39'No'#39
      #9'WHEN 1 THEN '#39'S'#237#39
      'END AS AccionAdicional,'
      'HistorialMorosos.DiasPostergacion,'
      'CONVERT (char (10), HistorialMorosos.FechaHora, 103) AS Fecha,'
      'MediosContacto.Descripcion AS MedioContacto'
      
        'FROM HistorialMorosos, ConfiguracionNotificaciones, MediosContac' +
        'to'
      'WHERE '
      
        'HistorialMorosos.CodigoMedioContacto *= MediosContacto.CodigoMed' +
        'ioContacto'
      'AND'
      'HistorialMorosos.IDAccion = ConfiguracionNotificaciones.ID'
      'AND'
      'HistorialMorosos.TipoComprobante = :TipoComprobante'
      'AND'
      'HistorialMorosos.NumeroComprobante = :NumeroComprobante')
    Left = 496
    Top = 472
  end
  object CrearHistorialMoroso: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'CrearHistorialMoroso;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 18
        Value = Null
      end
      item
        Name = '@IDAccion'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@AccionAdicional'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@Postergacion'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@CodigoMedioContacto'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 624
    Top = 472
  end
  object PopupMenu1: TPopupMenu
    Left = 464
    Top = 472
  end
  object qryConfiguracionNotificaciones: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'IDConfiguracionNotificacion'
        DataType = ftWord
        Precision = 3
        Size = 1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT ID, DescripcionAccion'
      'FROM ConfiguracionNotificaciones'
      'WHERE ID > :IDConfiguracionNotificacion'
      'ORDER  BY ID')
    Left = 432
    Top = 472
  end
  object InhabilitarTAG: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'InhabilitarTAG;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@ContextMark'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@ContractSerialNumber'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 11
        Value = Null
      end
      item
        Name = '@Motivo'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 400
    Top = 472
  end
  object qry_TAGS: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end
      item
        Name = 'NumeroComprobante'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        Precision = 18
        Size = 19
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT TagsAsignados.ContextMark, TagsAsignados.ContractSerialNu' +
        'mber'
      'FROM TagsAsignados, LotesFacturacion'
      'WHERE '
      'TagsAsignados.CodigoCuenta = LotesFacturacion.CodigoCuenta'
      'AND'
      'LotesFacturacion.TipoComprobante = :TipoComprobante'
      'AND'
      'LotesFacturacion.NumeroComprobante = :NumeroComprobante')
    Left = 368
    Top = 472
  end
  object ActualizarActionListEnPorceso: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ActualizarActionListEnPorceso;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@ContextMark'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@ContractSerialNumber'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 18
        Value = Null
      end
      item
        Name = '@ClearBlackList'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@SetBlackList'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@ClearGreenList'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@SetGreenList'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@ClearGrayList'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@SetGrayList'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@ClearYellowList'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@SetYellowList'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@ExceptionHandling'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@MMINotOk'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@MMIContactOperator'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 336
    Top = 472
  end
end
