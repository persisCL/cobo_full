object DetalleCierreTurno: TDetalleCierreTurno
  Left = 0
  Top = 0
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'Detalle Cierre Turno'
  ClientHeight = 252
  ClientWidth = 602
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object lbl1: TLabel
    Left = 8
    Top = 8
    Width = 196
    Height = 13
    Caption = 'Se encontraron las siguientes diferencias:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lbl2: TLabel
    Left = 8
    Top = 186
    Width = 302
    Height = 13
    Caption = 
      'Para cerrar el turno necesitar'#225' la confirmaci'#243'n de un Supervisor' +
      '.'
    Color = clBtnFace
    Font.Charset = ANSI_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object GrillaResumenDescuadre: TStringGrid
    Left = 8
    Top = 27
    Width = 585
    Height = 153
    DefaultRowHeight = 18
    FixedColor = 14732467
    FixedCols = 0
    RowCount = 6
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    ScrollBars = ssNone
    TabOrder = 0
    ColWidths = (
      136
      100
      100
      120
      120)
  end
  object btn_ok: TButton
    Left = 437
    Top = 219
    Width = 75
    Height = 25
    Caption = 'Aceptar'
    TabOrder = 1
    OnClick = btn_okClick
  end
  object btn_cancel: TButton
    Left = 518
    Top = 219
    Width = 75
    Height = 25
    Caption = 'Cancelar'
    TabOrder = 2
    OnClick = btn_cancelClick
  end
  object spObtenerCuadraturaCierre: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerCuadraturaCierre'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@Detalle'
        DataType = ftBoolean
        Value = False
      end>
    Top = 224
  end
end
