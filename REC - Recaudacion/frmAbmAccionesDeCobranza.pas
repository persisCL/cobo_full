{-----------------------------------------------------------------------------
 Unit Name: frmAbmAccionesDeCobranza
 Author:    aunanue
 Date:      08-03-2017
 Purpose:   Administra las acciones de cobranzas: CU.COBO.ADM.COB.104
 Firma: TASK_003_AUN_20170313-CU.COBO.ADM.COB.104
 History:
-----------------------------------------------------------------------------}
unit frmAbmAccionesDeCobranza;

interface

uses
  DMConnection,
  UtilProc,
  Util,
  RStrings,
  utildb,
  PeaTypes,
  PeaProcs,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, OleCtrls,
  DmiCtrls, Mask,  ComCtrls,  validate, Dateedit, ADODB, DPSControls, ListBoxEx,
  DBListEx, Variants, VariantComboBox, AccionesDeCobrazaBusiness;

type
  TAbmAccionesDeCobranzaForm = class(TForm)
    GrupoDatos: TPanel;
    PAbajo: TPanel;
	Notebook: TNotebook;
    txtCodigo: TNumericEdit;
    AbmToolbar: TAbmToolbar;
    spAlmacenar: TADOStoredProc;
    lblPlantillayTipoCaso: TLabel;
    DBListDatos: TAbmList;
    spEliminar: TADOStoredProc;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    spListar: TADOStoredProc;
    Label8: TLabel;
    lblTipoDeCliente: TLabel;
    txtTipoDeCliente: TVariantComboBox;
    lblNombre: TLabel;
    txtNombre: TEdit;
    txtMontoMinimoDeDeuda: TNumericEdit;
    lblMontoMinimoDeDeuda: TLabel;
    txtMontoMaximoDeDeuda: TNumericEdit;
    lblMontoMaximoDeDeuda: TLabel;
    txtEdadDeDeuda: TNumericEdit;
    lblEdadDeDeuda: TLabel;
    lblTipoDeAccionDeCobranza: TLabel;
    txtTipoDeAccionDeCobranza: TVariantComboBox;
    spTiposDeAccionesDeCobranza_Listar: TADOStoredProc;
    lblTipoDeCobranza: TLabel;
    txtTipoDeCobranza: TVariantComboBox;
    lblCategoriaDeDeudaDeCobranza: TLabel;
    txtCategoriaDeDeudaDeCobranza: TVariantComboBox;
    txtScriptTelefonico: TMemo;
    txtPlantillayTipoCaso: TVariantComboBox;
    lblScriptTelefonico: TLabel;
    procedure FormShow(Sender: TObject);
   	procedure DBListDatosRefresh(Sender: TObject);
   	function  DBListDatosProcess(Tabla: TDataSet; var Texto: String): Boolean;
   	procedure DBListDatosDrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
	procedure DBListDatosClick(Sender: TObject);
	procedure DBListDatosInsert(Sender: TObject);
	procedure DBListDatosEdit(Sender: TObject);
	procedure DBListDatosDelete(Sender: TObject);
	procedure BtnAceptarClick(Sender: TObject);
	procedure BtnCancelarClick(Sender: TObject);
	procedure AbmToolbarClose(Sender: TObject);
	procedure BtnSalirClick(Sender: TObject);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure txtTipoDeCobranzaChange(Sender: TObject);
    procedure txtTipoDeAccionDeCobranzaChange(Sender: TObject);
  private
	{ Private declarations }
    FListaCasos_DEU: TVariantItems;
    FPlantillas: TVariantItems;
    FAccionesDeCobranzaBiz: TAccionesDeCobranzaBusiness;
	procedure LimpiarCampos;
	Procedure VolverCampos;
    procedure AsignarValorTipoDeAccionDeCobranza;
    procedure AsignarComunicacionParaTipoDeAccionDeCobranza;
  public
	{ Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
	function Inicializa: Boolean;
  end;

resourcestring
    STR_CAPTION = 'Acciones de cobranza';

implementation

uses DB_CRUDCommonProcs;

const NOMBRE_CAMPO_CODIGO = 'CodigoAccionDeCobranza';

{$R *.DFM}

{-----------------------------------------------------------------------------
  Function Name: Inicializa
  Author:    aunanue
  Date:      09-03-2017
  Description:
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TAbmAccionesDeCobranzaForm.Inicializa:Boolean;
var
	S: TSize;
begin
	Result := False;
	FormStyle := fsMDIChild;
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
    Notebook.PageIndex := 0;
    Self.Caption := STR_CAPTION;
	//
    DB_CRUDCommonProcs.FillVariantItemsFromStoredProc(DMConnections.BaseCAC, 'ObtenerTiposDeCliente',
                                                      txtTipoDeCliente.Items, 'CodigoTipoCliente', 'Descripcion', ['@Semaforo'], [2], Self.Caption, True);
    txtTipoDeCliente.Value := -1;
    //
    FAccionesDeCobranzaBiz.ObtenerTodosLosTiposDeAccionesDeCobranza;
    //Esto lo hago para poner el "Seleccionar" como �tem.
    FAccionesDeCobranzaBiz.LlenarTiposDeAccionesDeCobranzaParaTipoDeCobranza(0, txtTipoDeAccionDeCobranza, true);
    txtTipoDeAccionDeCobranza.Value := -1;
    //
    DB_CRUDCommonProcs.FillVariantItemsFromStoredProc(DMConnections.BaseCAC, 'dbo.TiposDeCobranza_Listar', txtTipoDeCobranza.Items,
                                                      'CodigoTipoDeCobranza', 'Descripcion', [], [], Self.Caption, True);
    txtTipoDeCobranza.Value := -1;
    //
    DB_CRUDCommonProcs.FillVariantItemsFromStoredProc(DMConnections.BaseCAC, 'dbo.CategoriasDeDeudasDeCobranza_Listar', txtCategoriaDeDeudaDeCobranza.Items,
                                                     'CodigoCategoriaDeDeudaDeCobranza', 'Descripcion', [], [], Self.Caption, True);
    txtCategoriaDeDeudaDeCobranza.Value := -1;
    //
    DB_CRUDCommonProcs.FillVariantItemsFromStoredProc(DMConnections.BaseCAC, 'dbo.TiposOrdenServicio_Listar_DEU',
                                                      FListaCasos_DEU, 'TipoOrdenServicio', 'Descripcion', [], [], Self.Caption, True);
    DB_CRUDCommonProcs.FillVariantItemsFromStoredProc(DMConnections.BaseCAC, 'dbo.ObtenerPlantillasEMAIL',
                                                      FPlantillas, 'CodigoPlantilla', 'Asunto', [], [], Self.Caption, True);
    //
    lblPlantillayTipoCaso.Visible := false;
    txtPlantillayTipoCaso.Visible := false;
    lblScriptTelefonico.Visible := false;
    txtScriptTelefonico.Visible := false;
    //    
    if not OpenTables([spListar]) then exit;
	DBListDatos.Reload;
    ActiveControl := DBListDatos;
    //OK
	Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: LimpiarCampos
  Author:    aunanue
  Date:      09-03-2017
  Description:
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TAbmAccionesDeCobranzaForm.LimpiarCampos;
begin
	txtCodigo.Clear;
    txtNombre.Clear;
    txtMontoMinimoDeDeuda.Value := 0;
    txtMontoMaximoDeDeuda.Value := 0;
    txtEdadDeDeuda.ValueInt     := 0;
    txtTipoDeAccionDeCobranza.Value := -1;
    txtTipoDeCliente.Value := -1;
    txtTipoDeCobranza.Value := -1;
    txtCategoriaDeDeudaDeCobranza.Value := -1;
    txtPlantillayTipoCaso.Value := -1;
	txtScriptTelefonico.Clear;
    lblPlantillayTipoCaso.Visible := false;
    txtPlantillayTipoCaso.Visible := false;
    lblScriptTelefonico.Visible := false;
    txtScriptTelefonico.Visible := false;
end;

{-----------------------------------------------------------------------------
  Function Name: txtTipoDeAccionDeCobranzaChange
  Author:    aunanue
  Date:      09-03-2017
  Description: Actualizo la forma de comunicarme en base al tipo de acci�n seleccionada.
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TAbmAccionesDeCobranzaForm.txtTipoDeAccionDeCobranzaChange(
  Sender: TObject);
begin
    AsignarComunicacionParaTipoDeAccionDeCobranza;
end;                            

{-----------------------------------------------------------------------------
  Function Name: txtTipoDeAccionDeCobranzaChange
  Author:    aunanue
  Date:      09-03-2017
  Description: Actualizo la forma de comunicarme en base al tipo de acci�n seleccionada.
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TAbmAccionesDeCobranzaForm.AsignarComunicacionParaTipoDeAccionDeCobranza;
resourcestring
    TXT_TIPO_CASO = 'Tipo de caso: ';
    TXT_PLANTILLA = 'Plantilla: ';    
begin
    if FAccionesDeCobranzaBiz.EsAccionParaEnvioPostalOEmail(txtTipoDeAccionDeCobranza.Value) then begin
        lblPlantillayTipoCaso.Caption := TXT_PLANTILLA;
        lblPlantillayTipoCaso.Visible := true;
        txtPlantillayTipoCaso.Items.Clear;
        txtPlantillayTipoCaso.Items.Assign(FPlantillas);        
        if txtCodigo.ValueInt > 0 then begin
            txtPlantillayTipoCaso.Value := DBListDatos.Table.FieldByName('CodigoPlantilla').AsInteger;        
        end else begin
            txtPlantillayTipoCaso.Value := -1;
        end;
        txtPlantillayTipoCaso.Visible := true;                
        lblScriptTelefonico.Visible := false;
        txtScriptTelefonico.Visible := false;
        txtPlantillayTipoCaso.Enabled := true;
    end else if FAccionesDeCobranzaBiz.EsAccionParaLlamadoTelefonico(txtTipoDeAccionDeCobranza.Value) then begin
        lblPlantillayTipoCaso.Caption := TXT_TIPO_CASO;
        lblPlantillayTipoCaso.Visible := true;
        txtPlantillayTipoCaso.Items.Clear;
        txtPlantillayTipoCaso.Items.Assign(FListaCasos_DEU);
        if txtCodigo.ValueInt > 0 then begin
            txtPlantillayTipoCaso.Value := DBListDatos.Table.FieldByName('CodigoTipoDeCaso').AsInteger;            
            txtPlantillayTipoCaso.Enabled := txtPlantillayTipoCaso.Items.Count > 2;
            if not txtPlantillayTipoCaso.Enabled then begin
                //si est� deshabilitado entonces tengo que seleccionar el Segundo item
                //ya que el primero es "Seleccionar".
                txtPlantillayTipoCaso.ItemIndex := 1;
            end;      
        end else begin;
            if txtPlantillayTipoCaso.Items.Count <= 2 then begin
                txtPlantillayTipoCaso.ItemIndex := 1;
                txtPlantillayTipoCaso.Enabled := false;
            end else begin
                txtPlantillayTipoCaso.Enabled := true;
            end;
        end;
        txtPlantillayTipoCaso.Visible := true;
        lblScriptTelefonico.Visible := true;
        txtScriptTelefonico.Visible := true;
    end else begin        
        lblPlantillayTipoCaso.Visible := false;
        txtPlantillayTipoCaso.Visible := false;
        lblScriptTelefonico.Visible := false;
        txtScriptTelefonico.Visible := false;
    end;
end;                            

{-----------------------------------------------------------------------------
  Function Name: AsignarValorTipoDeAccionDeCobranza
  Author:    aunanue
  Date:      09-03-2017
  Description: Actualizo la lista de Tipos de acciones en base al tipo de cobranza
-----------------------------------------------------------------------------}
procedure TAbmAccionesDeCobranzaForm.AsignarValorTipoDeAccionDeCobranza;
begin
    FAccionesDeCobranzaBiz.LlenarTiposDeAccionesDeCobranzaParaTipoDeCobranza(txtTipoDeCobranza.Value,
        txtTipoDeAccionDeCobranza, true);
    txtTipoDeAccionDeCobranza.Value := iif(txtCodigo.ValueInt > 0, DBListDatos.Table.FieldByName('CodigoTipoDeAccionDeCobranza').AsInteger,
                                           -1);
    //esto es necesario porque puede que cambie el Tipo de cobranza y no exista la accion para ese tipo
    if txtTipoDeAccionDeCobranza.Value = Unassigned then
        txtTipoDeAccionDeCobranza.Value := -1;

    //Despu�s de cambiar el TipoDeAccionDeCobranza tengo que mostrar las opciones de comunicaci�n
    AsignarComunicacionParaTipoDeAccionDeCobranza;
end;

{-----------------------------------------------------------------------------
  Function Name: txtTipoDeCobranzaChange
  Author:    aunanue
  Date:      09-03-2017
  Description: Actualizo la lista de Tipos de acciones en base al tipo de cobranza
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TAbmAccionesDeCobranzaForm.txtTipoDeCobranzaChange(Sender: TObject);
begin
    AsignarValorTipoDeAccionDeCobranza;
end;

{-----------------------------------------------------------------------------
  Function Name: FormShow
  Author:    aunanue
  Date:      09-03-2017
  Description: Actualizo la grilla
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TAbmAccionesDeCobranzaForm.FormShow(Sender: TObject);
begin
	DBListDatos.Reload;
end;

{-----------------------------------------------------------------------------
  Function Name: DBListDatosRefresh
  Author:    aunanue
  Date:      09-03-2017
  Description: lipio los campos si la tabla esta vacia
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TAbmAccionesDeCobranzaForm.DBListDatosRefresh(Sender: TObject);
begin
	 if DBListDatos.Empty then LimpiarCampos;
end;

{-----------------------------------------------------------------------------
  Function Name: VolverCampos
  Author:    aunanue
  Date:      09-03-2017
  Description:
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TAbmAccionesDeCobranzaForm.VolverCampos;
begin
	DBListDatos.Estado  := Normal;
	DBListDatos.Enabled := True;
	ActiveControl       := DBListDatos;
	Notebook.PageIndex  := 0;
	GrupoDatos.Enabled  := False;
	DBListDatos.Reload;
end;

{-----------------------------------------------------------------------------
  Function Name: DBListDatosDrawItem
  Author:    aunanue
  Date:      09-03-2017
  Description: Muestro los Registros
  Parameters: Sender: TDBList; Tabla: TDataSet;Rect: TRect; State: TOwnerDrawState; Cols: TColPositions
  Return Value: None
-----------------------------------------------------------------------------}
procedure TAbmAccionesDeCobranzaForm.DBListDatosDrawItem(Sender: TDBList; Tabla: TDataSet;Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, Istr(Tabla.FieldbyName(NOMBRE_CAMPO_CODIGO).AsInteger, 10));
        TextOut(Cols[1], Rect.Top, Trim(Tabla.FieldbyName('Nombre').AsString));
        TextOut(Cols[2], Rect.Top, FloatToStrWithDecimalPoint(Tabla.FieldbyName('MontoMinimoDeDeuda').AsFloat));
        TextOut(Cols[3], Rect.Top, FloatToStrWithDecimalPoint(Tabla.FieldbyName('MontoMaximoDeDeuda').AsFloat));
        TextOut(Cols[4], Rect.Top, Trim(Tabla.FieldbyName('EdadDeDeuda').AsString));
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: DBListDatosClick
  Author:    aunanue
  Date:      09-03-2017
  Description: Selecciono un campo
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TAbmAccionesDeCobranzaForm.DBListDatosClick(Sender: TObject);
begin
	with (Sender AS TDbList).Table do begin
	   txtCodigo.Value		:= FieldByName(NOMBRE_CAMPO_CODIGO).AsInteger;
       txtNombre.Text       := FieldByName('Nombre').AsString;
       txtMontoMinimoDeDeuda.Value := FieldByName('MontoMinimoDeDeuda').AsFloat;
       txtMontoMaximoDeDeuda.Value := FieldByName('MontoMaximoDeDeuda').AsFloat;
       txtEdadDeDeuda.ValueInt := FieldByName('EdadDeDeuda').AsInteger;
       txtTipoDeCliente.Value := FieldByName('CodigoTipoCliente').AsInteger;
       txtTipoDeCobranza.Value := FieldByName('CodigoTipoDeCobranza').AsInteger;
       txtCategoriaDeDeudaDeCobranza.Value := FieldByName('CodigoCategoriaDeDeudaDeCobranza').AsInteger;
	   txtScriptTelefonico.Text	:= FieldByname('Descripcion').AsString;
       AsignarValorTipoDeAccionDeCobranza;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: DBListDatosProcess
  Author:    aunanue
  Date:      09-03-2017
  Description:
  Parameters: Tabla: TDataSet;var Texto: String
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TAbmAccionesDeCobranzaForm.DBListDatosProcess(Tabla: TDataSet; var Texto: String): Boolean;
begin
    Texto := Tabla.FieldByName('Nombre').AsString;
	Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: DBListDatosInsert
  Author:    aunanue
  Date:      09-03-2017
  Description: limpio los campos para que ingrese un nuevo registro
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TAbmAccionesDeCobranzaForm.DBListDatosInsert(Sender: TObject);
begin
	LimpiarCampos;
    GrupoDatos.Enabled  := True;
	DBListDatos.Enabled := False;
	Notebook.PageIndex  := 1;
	ActiveControl       := txtNombre;
end;

{-----------------------------------------------------------------------------
  Function Name: DBListDatosEdit
  Author:    aunanue
  Date:      09-03-2017
  Description:
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TAbmAccionesDeCobranzaForm.DBListDatosEdit(Sender: TObject);
begin
	DBListDatos.Enabled := False;
	DBListDatos.Estado  := modi;
	Notebook.PageIndex  := 1;
	GrupoDatos.Enabled  := True;
	ActiveControl := txtNombre;
end;

{-----------------------------------------------------------------------------
  Function Name: DBListDatosDelete
  Author:    aunanue
  Date:      09-03-2017
  Description: elimino un item
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TAbmAccionesDeCobranzaForm.DBListDatosDelete(Sender: TObject);
begin
	If MsgBox(Format(MSG_QUESTION_ELIMINAR,[STR_CAPTION]), STR_CONFIRMACION, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
        Screen.Cursor := crHourGlass;
        try
            spEliminar.Connection.BeginTrans;
            try
                AddParameter_Codigo(NOMBRE_CAMPO_CODIGO, spEliminar.Parameters, txtCodigo.ValueInt);
                AddParameter_CodigoUsuario(spEliminar.Parameters);
                AddParameter_Resultado(spEliminar.Parameters);
                spEliminar.ExecProc;
                if spEliminar.Parameters.ParamByName('@Resultado').Value > 0 then begin
                    spEliminar.Connection.CommitTrans;
                end else begin
                    spEliminar.Connection.RollbackTrans;
                    MsgBox(MSG_EXISTEN_DATOS_RELACIONADOS_BORRANDO, STR_CAPTION, MB_ICONSTOP);
                end;
            except
                On E: Exception do begin
                    if spEliminar.Connection.InTransaction then
                        spEliminar.Connection.RollbackTrans;
                    MsgBoxErr(Format(MSG_ERROR_ELIMINAR,[STR_CAPTION]), e.message, Format(MSG_CAPTION_ELIMINAR,[STR_CAPTION]), MB_ICONSTOP);
                end
            end
        finally
            VolverCampos();
	        Screen.Cursor := crDefault;
    	end;
    end else begin
        DBListDatos.Estado  := Normal;
	    DBListDatos.Enabled := True;
	    ActiveControl       := DBListDatos;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: BtnAceptarClick
  Author:    aunanue
  Date:      09-03-2017
  Description: Acepto los cambios
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TAbmAccionesDeCobranzaForm.BtnAceptarClick(Sender: TObject);
resourcestring
    TXT_MAYOR_QUE_EL = 'El %s debe ser mayor que cero.';
    TXT_MAYOR_QUE_LA = 'La %s debe ser mayor que cero.';
var
    BookM: TBookmark;
    Result, Duplicado: Boolean;
    i: Integer;
begin
  	if not ValidateControls(
        [txtNombre,
         txtMontoMinimoDeDeuda, txtMontoMaximoDeDeuda, txtEdadDeDeuda,         
         txtTipoDeCobranza,
         txtTipoDeAccionDeCobranza],
	    [Trim(txtNombre.Text) <> '',
         txtMontoMinimoDeDeuda.Value >= 0, txtMontoMaximoDeDeuda.Value >= 0, txtEdadDeDeuda.ValueInt >= 0,         
         txtTipoDeCobranza.ItemIndex > 0,
         txtTipoDeAccionDeCobranza.ItemIndex > 0],
	     Format(MSG_CAPTION_ACTUALIZAR, [STR_CAPTION]),
        [Format(MSG_VALIDAR_DEBE_EL, [FindFocusLabelText(Self, txtNombre)]),
         Format(TXT_MAYOR_QUE_EL, [FindFocusLabelText(Self, txtMontoMinimoDeDeuda)]),
         Format(TXT_MAYOR_QUE_EL, [FindFocusLabelText(Self, txtMontoMaximoDeDeuda)]),
         Format(TXT_MAYOR_QUE_LA, [FindFocusLabelText(Self, txtEdadDeDeuda)]),         
         Format(MSG_VALIDAR_SELECCIONAR_UNO, [FindFocusLabelText(Self, txtTipoDeCobranza)]),         
         Format(MSG_VALIDAR_SELECCIONAR_UNO, [FindFocusLabelText(Self, txtTipoDeAccionDeCobranza)])
         ]) then begin
		Exit;
	end;
    Screen.Cursor := crHourGlass;
    Result := False;
    Duplicado := False;
    try
        //Validar Duplicados
        BookM := DBListDatos.Table.GetBookmark;
        try
            DBListDatos.Table.First;
            for i := 0 to DBListDatos.Table.RecordCount - 1 do begin
                if DBListDatos.Table.FieldByName(NOMBRE_CAMPO_CODIGO).AsString <> txtCodigo.Text then begin
                    if SysUtils.CompareText(Trim(DBListDatos.Table.FieldByName('Nombre').AsString), Trim(txtNombre.Text)) = 0 then begin
                        MsgBox(Format(MSG_ERROR_REGISTRO_DUPLICADO, [Trim(txtNombre.Text)]), STR_CAPTION, MB_ICONSTOP);
                        Duplicado := True;
                        break;
                    end;
                end;
                DBListDatos.Table.Next;
            end;
            DBListDatos.Table.GotoBookmark(BookM);
        finally
            DBListDatos.Table.FreeBookmark(BookM);
        end;

        if Duplicado then
            Exit;
        //
        spAlmacenar.Connection.BeginTrans;
        try
            AddParameter_Codigo(NOMBRE_CAMPO_CODIGO, spAlmacenar.Parameters, txtCodigo.ValueInt);
            AddParameter_Nombre(spAlmacenar.Parameters, Trim(txtNombre.Text));
            AddParameter(spAlmacenar.Parameters, '@MontoMinimoDeDeuda', ftFloat, pdInput, txtMontoMinimoDeDeuda.Value);
            AddParameter(spAlmacenar.Parameters, '@MontoMaximoDeDeuda', ftFloat, pdInput, txtMontoMaximoDeDeuda.Value);
            AddParameter(spAlmacenar.Parameters, '@EdadD eDeuda', ftInteger, pdInput, txtEdadDeDeuda.ValueInt);
            AddParameter(spAlmacenar.Parameters, '@CodigoTipoDeAccionDeCobranza', ftInteger, pdInput, txtTipoDeAccionDeCobranza.Value);
            AddParameter(spAlmacenar.Parameters, '@CodigoTipoCliente', ftInteger, pdInput, txtTipoDeCliente.Value);
            AddParameter(spAlmacenar.Parameters, '@CodigoTipoDeCobranza', ftInteger, pdInput, txtTipoDeCobranza.Value);            
            AddParameter(spAlmacenar.Parameters, '@CodigoCategoriaDeDeudaDeCobranza', ftInteger, pdInput, txtCategoriaDeDeudaDeCobranza.Value);
            if txtScriptTelefonico.Visible then begin
                AddParameter(spAlmacenar.Parameters, '@CodigoTipoDeCaso', ftInteger, pdInput, txtPlantillayTipoCaso.Value);                
                AddParameter(spAlmacenar.Parameters, '@CodigoPlantilla', ftInteger, pdInput, Null);
                AddParameter_Descripcion(spAlmacenar.Parameters, Trim(txtScriptTelefonico.Text), True);
            end else begin                
                AddParameter(spAlmacenar.Parameters, '@CodigoTipoDeCaso', ftInteger, pdInput, Null);
                AddParameter(spAlmacenar.Parameters, '@CodigoPlantilla', ftInteger, pdInput, txtPlantillayTipoCaso.Value);
                AddParameter_Descripcion(spAlmacenar.Parameters, '');                            
            end;            
            AddParameter_CodigoUsuario(spAlmacenar.Parameters);
            AddParameter_Resultado(spAlmacenar.Parameters);
            spAlmacenar.ExecProc;
            spAlmacenar.Connection.CommitTrans;
            Result := True;
        except
            On E: Exception do begin
                if spAlmacenar.Connection.InTransaction then
                    spAlmacenar.Connection.RollbackTrans;
                //
                MsgBoxErr(Format(MSG_ERROR_ACTUALIZAR,[STR_CAPTION]), e.message, Format(MSG_CAPTION_ACTUALIZAR,[STR_CAPTION]), MB_ICONSTOP);
            end;
        end;
    finally
        if (Result) then begin
            VolverCampos;
        end;
        Screen.Cursor := crDefault;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: BtnCancelarClick
  Author:    aunanue
  Date:      09-03-2017
  Description: permito cancelar
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TAbmAccionesDeCobranzaForm.BtnCancelarClick(Sender: TObject);
begin
	VolverCampos;
end;

{-----------------------------------------------------------------------------
  Function Name: AbmToolbar1Close
  Author:    aunanue
  Date:      09-03-2017
  Description: salgo del formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TAbmAccionesDeCobranzaForm.AbmToolbarClose(Sender: TObject);
begin
	 Close;
end;

{-----------------------------------------------------------------------------
  Function Name: BtnSalirClick
  Author:    aunanue
  Date:      09-03-2017
  Description:  salgo del formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TAbmAccionesDeCobranzaForm.BtnSalirClick(Sender: TObject);
begin
	 AbmToolbarClose(BtnSalir);
end;

{-----------------------------------------------------------------------------
  Function Name: Create
  Author:    aunanue
  Date:      09-03-2017
  Description: creo objetos que uso en el Form
-----------------------------------------------------------------------------}
constructor TAbmAccionesDeCobranzaForm.Create(AOwner: TComponent);
begin
    inherited;
    FAccionesDeCobranzaBiz := TAccionesDeCobranzaBusiness.Create(Self.Caption);
    FListaCasos_DEU := TVariantItems.Create(nil);
    FPlantillas := TVariantItems.Create(nil);
end;

{-----------------------------------------------------------------------------
  Function Name: Destroy
  Author:    aunanue                               
  Date:      09-03-2017
  Description: libero objetos creados
-----------------------------------------------------------------------------}
destructor TAbmAccionesDeCobranzaForm.Destroy;
begin
    FPlantillas.Clear;
    FreeAndNil(FPlantillas);
    FListaCasos_DEU.Clear;
    FreeAndNil(FListaCasos_DEU);
    FreeAndNil(FAccionesDeCobranzaBiz);
    inherited;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    aunanue
  Date:      09-03-2017
  Description: lo libero de memoria
  Parameters: Sender: TObject; var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TAbmAccionesDeCobranzaForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	 Action := caFree;
end;

end.

