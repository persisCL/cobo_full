object FormConfigReporteLiquidacion: TFormConfigReporteLiquidacion
  Left = 365
  Top = 188
  ActiveControl = cb_Usuarios
  BorderStyle = bsDialog
  Caption = 'Reporte de Liquidaci'#243'n'
  ClientHeight = 114
  ClientWidth = 454
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 14
    Top = 46
    Width = 31
    Height = 13
    Caption = 'Turno:'
  end
  object Label1: TLabel
    Left = 14
    Top = 22
    Width = 39
    Height = 13
    Caption = 'Usuario:'
  end
  object Bevel1: TBevel
    Left = 8
    Top = 6
    Width = 441
    Height = 68
  end
  object cb_Turnos: TComboBox
    Left = 57
    Top = 41
    Width = 385
    Height = 21
    Style = csDropDownList
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ItemHeight = 13
    ParentFont = False
    TabOrder = 1
    OnChange = cb_TurnosChange
  end
  object btn_Aceptar: TDPSButton
    Left = 285
    Top = 85
    Width = 78
    Caption = '&Aceptar'
    Default = True
    Enabled = False
    ModalResult = 1
    TabOrder = 2
  end
  object btn_Salir: TDPSButton
    Left = 369
    Top = 85
    Cancel = True
    Caption = '&Salir'
    ModalResult = 2
    TabOrder = 3
  end
  object cb_Usuarios: TComboBox
    Left = 57
    Top = 17
    Width = 384
    Height = 21
    Style = csDropDownList
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ItemHeight = 13
    ParentFont = False
    TabOrder = 0
    OnChange = cb_UsuariosChange
  end
  object UsuariosTurnosAbierto: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'UsuariosTurnosAbierto'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
      end>
    Left = 8
    Top = 80
  end
end
