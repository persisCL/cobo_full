{-------------------------------------------------------------------------------


Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.

--------------------------------------------------------------------------------}

unit frmRefinanciacionChequesDepositadosRpt;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DMConnection, DB, ADODB, ppVar, ppCtrls, ppPrnabl, ppClass, ppBands, ppCache, ppDB, ppDBPipe, ppComm, ppRelatv, ppProd,
  ppReport, UtilRB, SysUtilsCN, ppParameter,
  ConstParametrosGenerales, UtilProc;                                           //SS_1147_NDR_20140710

type
  TRefinanciacionChequesDepositadosRptForm = class(TForm)
    spRefinanciacionObtenerChequesDepositados: TADOStoredProc;
    RBInterface1: TRBInterface;
    ppReport1: TppReport;
    ppDBPipeline1: TppDBPipeline;
    dsRefinanciacionObtenerChequesDepositados: TDataSource;
    ppHeaderBand1: TppHeaderBand;
    ppDetailBand1: TppDetailBand;
    ppImage2: TppImage;
    ppLine19: TppLine;
    ppLtitulo: TppLabel;
    ppLabel38: TppLabel;
    ppSystemVariable1: TppSystemVariable;
    ppLabel39: TppLabel;
    ppSystemVariable10: TppSystemVariable;
    pptNombreArchivo: TppLabel;
    ppNumeroDeposito: TppLabel;
    pptFechaProcesamiento: TppLabel;
    ppFechaProcesamiento: TppLabel;
    pptUsuario: TppLabel;
    ppUsuario: TppLabel;
    pptLineas: TppLabel;
    ppCantidadCheques: TppLabel;
    ppLabel20: TppLabel;
    ppLine8: TppLine;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppDBText7: TppDBText;
    ppDBText8: TppDBText;
    ppLabel2: TppLabel;
    ppDBText9: TppDBText;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLabel10: TppLabel;
    ppLabel11: TppLabel;
    ppLine1: TppLine;
    spObtenerRefinanciacionDespositoMonto: TADOStoredProc;
    ppMontoDeposito: TppLabel;
    ppLabel1: TppLabel;
    ppDBText1: TppDBText;
    procedure RBInterface1Execute(Sender: TObject; var Cancelled: Boolean);
  private
    { Private declarations }
    FNumeroDeposito: Integer;
  public
    { Public declarations }
    Procedure Inicializar(NumeroDeposito: Integer);
  end;

var
  RefinanciacionChequesDepositadosRptForm: TRefinanciacionChequesDepositadosRptForm;

implementation

{$R *.dfm}

procedure TRefinanciacionChequesDepositadosRptForm.Inicializar(NumeroDeposito: Integer);
var                                                                             //SS_1147_NDR_20140710
  RutaLogo:AnsiString;                                                          //SS_1147_NDR_20140710
begin
    ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
    try                                                                                                 //SS_1147_NDR_20140710
      ppImage2.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
    except                                                                                              //SS_1147_NDR_20140710
      On E: Exception do begin                                                                          //SS_1147_NDR_20140710
        Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
        MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
        Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
      end;                                                                                              //SS_1147_NDR_20140710
    end;                                                                                                //SS_1147_NDR_20140710
    FNumeroDeposito := NumeroDeposito;
end;

procedure TRefinanciacionChequesDepositadosRptForm.RBInterface1Execute(Sender: TObject; var Cancelled: Boolean);
    resourcestring
        MSG_TITULO_ERROR = 'Reporte de Cheques Depositados';
        MSG_ERROR        = 'Se produjo un error al obtener los datos del reporte. Error: %s';
begin
    try

        with spObtenerRefinanciacionDespositoMonto, Parameters do begin
            if Active then Close;
            Parameters.Refresh;
            ParamByName('@IDComprobanteDeposito').Value := FNumeroDeposito;
            ParamByName('@DepositoMonto').Value := NULL;
            ExecProc;
            ppMontoDeposito.Caption := ParamByName('@DepositoMonto').Value;
        end;

        with spRefinanciacionObtenerChequesDepositados, Parameters do begin
            if Active then Close;
            Parameters.Refresh;
            ParamByName('@CodigoRefinanciacion').Value     := NULL;
            ParamByName('@IDComprobanteDeposito').Value    := FNumeroDeposito;
            ParamByName('@NoComprobarEstadoCheques').Value := 1; // Devolver� todos los cheques incluidos en el dep�sito, independiente de su estado.
            Open;

            ppUsuario.Caption            := FieldByName('UsuarioCreacion').AsString;
            ppFechaProcesamiento.Caption := FieldByName('FechaDeposito').AsString;
            ppCantidadCheques.Caption    := IntToStr(RecordCount);
        end;

        ppNumeroDeposito.Caption := IntToStr(FNumeroDeposito);
    except
        on e: Exception do begin
            raise EErrorExceptionCN.Create(MSG_TITULO_ERROR, Format(MSG_ERROR, [e.Message]));
        end;
    end;
end;

end.
