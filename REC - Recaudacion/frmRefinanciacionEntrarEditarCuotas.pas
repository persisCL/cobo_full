unit frmRefinanciacionEntrarEditarCuotas;

{
 Firma           : SS_1410_MCA_20151027
Descripcion     : se agrega una nueva forma de pago para refinanciacion.
}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,

  DMConnection, SysUtilsCN, PeaProcsCN, PeaProcs, ConstParametrosGenerales, UtilProc, UtilDB, Util,
  PeaTypes, CobranzasClasses, CobranzasRoutines, CobranzasResources, Contnrs,

  Dialogs, StdCtrls, ExtCtrls, ListBoxEx, DBListEx, Mask, DmiCtrls, Validate, DateEdit,
  VariantComboBox, Buttons, DB, ADODB, DBClient;


type
   TEdicionDatos = record
    CodigoEntradaUsuario: string[02];
    TitularNombre: string[100];
    TitularTipoDocumento: string[04];
    TitularNumeroDocumento: string[11];
    CodigoBanco: Integer;
    DocumentoNumero: string[20];
    FechaCuota: TDate;
    CodigoTarjeta,
    TarjetaCuotas,
    Importe,
    Saldo: Integer;
    CuotaNueva: Boolean;
    CuotaAntecesora: Integer;
    FormaDePagoDesc: string[30];
    BancoDesc,
    TarjetaDesc: string[50];
    CodigoFormaPago: Byte;
  end;

type
  TRefinanciacionEntrarEditarCuotasForm = class(TForm)
    Panel1: TPanel;
    btnCancelar: TButton;
    btnAceptar: TButton;
    gbDatos: TGroupBox;
    edtCodigoFormaPago: TEdit;
    edtDescFormaPago: TEdit;
    lbl_FormaPago: TLabel;
    GroupBox2: TGroupBox;
    nbOpciones: TNotebook;
    Label1: TLabel;
    lblChequeNumero: TLabel;
    lblChequeBanco: TLabel;
    lblChequeRUT: TLabel;
    lblChequeTitular: TLabel;
    lblChequeFecha: TLabel;
    btnDatosClienteCheque: TSpeedButton;
    Label2: TLabel;
    txtChequeNumero: TEdit;
    cbChequeBancos: TVariantComboBox;
    txtChequeNombreTitular: TEdit;
    txtChequeDocumento: TEdit;
    deChequeFecha: TDateEdit;
    edtRepactacionCheque: TEdit;
    lblValeVistaTomadoPor: TLabel;
    lblValeVistaRUT: TLabel;
    lblValeVistaBanco: TLabel;
    lblValeVistaNumero: TLabel;
    lblValeVistaFecha: TLabel;
    btn_DatosClienteValeVista: TSpeedButton;
    txt_TomadoPorVV: TEdit;
    txt_RUTVV: TEdit;
    txt_NroVV: TEdit;
    cb_BancosVV: TVariantComboBox;
    de_FechaVV: TDateEdit;
    spbTarjetaCreditoCopiarDatosCliente: TSpeedButton;
    lblTarjetaCreditoTitular: TLabel;
    lblTarjetaCreditoTarjeta: TLabel;
    lblTarjetaCreditoNroConfirmacion: TLabel;
    lblTarjetaCreditoBanco: TLabel;
    lblTarjetaCreditoCuotas: TLabel;
    edtTarjetaCreditoTitular: TEdit;
    vcbTarjetaCreditoTarjetas: TVariantComboBox;
    edtTarjetaCreditoNroConfirmacion: TEdit;
    vcbTarjetaCreditoBancos: TVariantComboBox;
    nedtTarjetaCreditoCuotas: TNumericEdit;
    lblTarjetaDebitoTitular: TLabel;
    lblTarjetaDebitoBanco: TLabel;
    lblTarjetaDebitoNroConfirmacion: TLabel;
    spbTarjetaDebitoCopiarDatosCliente: TSpeedButton;
    edtTarjetaDebitoTitular: TEdit;
    edtTarjetaDebitoNroConfirmacion: TEdit;
    vcbTarjetaDebitoBancos: TVariantComboBox;
    lblPagareNumero: TLabel;
    lblPagareBanco: TLabel;
    lblPagareRUT: TLabel;
    lblPagareTitular: TLabel;
    lblPagareFecha: TLabel;
    btnDatosClientePagare: TSpeedButton;
    Label9: TLabel;
    txtPagareNumero: TEdit;
    cbPagareBancos: TVariantComboBox;
    txtPagareNombreTitular: TEdit;
    txtPagareDocumento: TEdit;
    dePagareFecha: TDateEdit;
    edtRepactacion: TEdit;
    nedtImporteCheque: TNumericEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    nedtEfectivoImporte: TNumericEdit;
    nedtPagareImporte: TNumericEdit;
    nedtImporteVV: TNumericEdit;
    Label6: TLabel;
    nedtTarjetaCreditoImporte: TNumericEdit;
    lblTarjetaCreditoImporte: TLabel;
    nedtTarjetaDebitoImporte: TNumericEdit;
    lblTarjetaDebitoImporte: TLabel;
    Label10: TLabel;
    spValidarFormaDePagoEnCanal: TADOStoredProc;
    dedtEfectivo: TDateEdit;
    Label11: TLabel;
    gbCuotaAntecesora: TGroupBox;
    vcbCuotaAntecesora: TVariantComboBox;
    dedtFechaTarjetaCredito: TDateEdit;
    Label12: TLabel;
    Label13: TLabel;
    dedtFechaTarjetaDebito: TDateEdit;
    vcbCanalesPago: TVariantComboBox;
    lblCanalDePago: TLabel;
    pnlPago2: TPanel;
    lblFechaDePago: TLabel;
    deFechaPago: TDateEdit;
    lblSiDeseaCambiarFechaPago: TLabel;
    lblaca: TLabel;
    lblSiDeseaCambiarElMontoPagado: TLabel;
    lblAcaMonto: TLabel;
    pnlPago1: TPanel;
    lbl_Moneda: TLabel;
    neImporte: TNumericEdit;
    lbl_MontoPagado: TLabel;
    neImportePagado: TNumericEdit;
    nedtImporteEntregado: TNumericEdit;
    Label14: TLabel;
    Label15: TLabel;
    nedtCambio: TNumericEdit;
    procedure edtCodigoFormaPagoChange(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure spbTarjetaDebitoCopiarDatosClienteClick(Sender: TObject);
    procedure spbTarjetaCreditoCopiarDatosClienteClick(Sender: TObject);
    procedure btnDatosClientePagareClick(Sender: TObject);
    procedure btn_DatosClienteValeVistaClick(Sender: TObject);
    procedure btnDatosClienteChequeClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
    procedure vcbCuotaAntecesoraChange(Sender: TObject);
    procedure deFechaPagoChange(Sender: TObject);
    procedure lblacaClick(Sender: TObject);
    procedure lblAcaMontoClick(Sender: TObject);
    procedure neImportePagadoChange(Sender: TObject);
    procedure nedtImporteEntregadoChange(Sender: TObject);
  private
    { Private declarations }
    FCodigoBancoPorDefecto,
    FCodigoTarjetaPorDefecto: Integer;
    FAhora,
    FFechamaximaCheque,
    FFechaMinimaCheque,
    FFechaMaximaPagare,
    FFechaMinimaPagare: TDate;
    FAltaDatos,
    FEdicionCuotaNueva,
    FEnTramite,
    FEntradaPago: Boolean;
  public
    { Public declarations }
    FCodigoPantalla: Integer;
    FEntradaDatos: TEntradaDatos;
    FComponentes: TObjectList;
    FUltimoBancoEntrado: Integer;
    FUltimoNroPagareEntrado: string[20];
    
    function Inicializar(
                pTipoEntradaDatos,
                pCodigoCanalPago   : Integer;
                pPersona           : TDatosPersonales;
                pAlta              : Boolean;
                pFechaPago         : TDate;
                pEnTramite         : Boolean;
                pCuotas            : TClientDataSet): Boolean;

    function ValidarDatosPagare: Boolean;
    function ValidarDatosCheque: Boolean;
    function ValidarDatosValeVista: Boolean;
    function ValidarDatosTarjetaCredito: Boolean;
    function ValidarDatosTarjetaDebito: Boolean;
    function ValidarDatosEfectivo: Boolean;
  end;

var
  RefinanciacionEntrarEditarCuotasForm: TRefinanciacionEntrarEditarCuotasForm;

resourcestring
    MSG_ERROR_NOMBRE_TITULAR         = 'Ingrese el Nombre del Titular.';
    MSG_ERROR_DOCUMENTO              = 'Debe Ingresar el RUT';
    MSG_ERROR_RUT_INVALIDO           = 'El RUT ingresado es Inv�lido.';
    MSG_ERROR_BANCO                  = 'Debe seleccionar un banco.';
    MSG_ERROR_IMPORTE                = 'No puede ingresar un Importe menor que Cero.';
    MSG_ERROR_IMPORTE_PAGADO         = 'No puede ingresar un Importe superior al Total del Importe a Pagar';
    MSG_ERROR_IMPORTE_ENTREGADO      = 'El Importe Entregado debe ser igual o superior al Total del Importe a Pagar';   // SS_637_PDO_20110503
    MSG_ERROR_AUTORIZACION           = 'Ingrese un C�digo de Autorizaci�n.';
    MSG_ERROR_AUTORIZACION_DUPLICADO = 'C�digo de Autorizaci�n Duplicado.';
    MSG_ERROR_FECHA_PAGO             = 'La Fecha de Pago NO puede ser superior a la Fecha Actual';

implementation

{$R *.dfm}

procedure TRefinanciacionEntrarEditarCuotasForm.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
    case Key of
        VK_ESCAPE: ModalResult := mrCancel;
    end;
end;

function TRefinanciacionEntrarEditarCuotasForm.Inicializar
    (pTipoEntradaDatos,
     pCodigoCanalPago   : Integer;
     pPersona           : TDatosPersonales;
     pAlta              : Boolean;
     pFechaPago         : TDate;
     pEnTramite         : Boolean;
     pCuotas            : TClientDataSet): Boolean;

    resourcestring
        RS_SIN_CUOTA_ANTECESORA   = 'Sin Cuota Antecesora.';
        RS_DATOS_CUOTA_ANTECESORA = 'ID: %s - Fecha Cuota: %s - Importe: %s - %s';

        RS_CATION_PAGO_CUOTA      = ' Pago Cuota Refinanciaci�n';
        RS_DETALLE_PAGO_CUOTA     = '  Detalle Pago Cuota Refinanciaci�n  ';

    const
        ALTURA_PAGO = 495;
        ALTURA_CUOTA_REFINANCIACION = 440;

    var
        CuotasTmp: TClientDataSet;
        CuotaAntecesoraDesc: String;
begin
    try
        try
            Result := False;

            CambiarEstadoCursor(CURSOR_RELOJ);

            FUltimoBancoEntrado     := 0;
            FUltimoNroPagareEntrado := EmptyStr;

            AsignarEntradaDatos
                (pTipoEntradaDatos,
                 pCodigoCanalPago,
                 FEntradaDatos,
                 pFechaPago,
                 pPersona,
                 pCuotas,
                 pAlta,
                 pEnTramite);

            FEntradaPago := pTipoEntradaDatos in [TIPO_ENTRADA_DATOS_PAGO_CUOTA, TIPO_ENTRADA_DATOS_PAGO_COMPROBANTES];

            case pTipoEntradaDatos of
                TIPO_ENTRADA_DATOS_CUOTA, TIPO_ENTRADA_DATOS_PAGO_CUOTA: begin
                    Height := iif(pTipoEntradaDatos = TIPO_ENTRADA_DATOS_CUOTA, ALTURA_CUOTA_REFINANCIACION, ALTURA_PAGO);

                    pnlPago1.Visible          := (pTipoEntradaDatos = TIPO_ENTRADA_DATOS_PAGO_CUOTA);
                    pnlPago2.Visible          := (pTipoEntradaDatos = TIPO_ENTRADA_DATOS_PAGO_CUOTA);
                    gbCuotaAntecesora.Visible := (pTipoEntradaDatos = TIPO_ENTRADA_DATOS_CUOTA);

                    vcbCanalesPago.Enabled    := False;

                    CargarCanalesDePago(DMConnections.BaseCAC, vcbCanalesPago, pCodigoCanalPago, 1);

                    vcbCuotaAntecesora.Items.Add(RS_SIN_CUOTA_ANTECESORA, 0);
                    vcbCuotaAntecesora.Enabled := not FEntradaDatos.DatosCuota.EnTramite;

                    if pTipoEntradaDatos = TIPO_ENTRADA_DATOS_CUOTA then begin
                        CuotasTmp      := TClientDataSet.Create(Nil);
                        CuotasTmp.Data := pCuotas.Data;

                        with CuotasTmp do begin
                            while not Eof do begin
                                if FieldByName('CodigoEstadoCuota').AsInteger in [REFINANCIACION_ESTADO_CUOTA_ANULADA, REFINANCIACION_ESTADO_CUOTA_ANULADA_POR_MORA, REFINANCIACION_ESTADO_CUOTA_ANULADA_POR_PROTESTO] then begin

                                    CuotaAntecesoraDesc :=
                                        Format(RS_DATOS_CUOTA_ANTECESORA,
                                                [PadL(FieldByName('CodigoCuota').AsString, 3, ' '),
                                                 FormatDateTime('dd/mm/yyyy', FieldByName('FechaCuota').AsDateTime),
                                                 PadL(FormatFloat(FORMATO_IMPORTE, FieldByName('Importe').AsFloat), 15, ' '),
                                                 Trim(FieldByName('DescripcionFormaPago').AsString)]);

                                    vcbCuotaAntecesora.Items.Add(CuotaAntecesoraDesc, FieldByName('CodigoCuota').AsInteger);
                                end;

                                Next;
                            end;
                        end;

                        if FEntradaDatos.DatosCuota.CodigoCuotaAntecesora <> 0 then begin
                            vcbCuotaAntecesora.ItemIndex := vcbCuotaAntecesora.Items.IndexOfValue(FEntradaDatos.DatosCuota.CodigoCuotaAntecesora);
                        end
                        else vcbCuotaAntecesora.ItemIndex := 0;
                    end
                    else begin
                        Caption         := RS_CATION_PAGO_CUOTA;
                        gbDatos.Caption := RS_DETALLE_PAGO_CUOTA;

                        gbCuotaAntecesora.Visible := False;

                        neImporte.Value       := FEntradaDatos.DatosCuota.Saldo;
                        neImportePagado.Value := FEntradaDatos.DatosCuota.Saldo;

                        lblaca.Enabled        := ExisteAcceso('modif_fecha_cobro_ref');
                        lblAcaMonto.Enabled   := ExisteAcceso('modif_monto_pagado_ref');
                    end;
                end;

                TIPO_ENTRADA_DATOS_PAGO_COMPROBANTES: begin
                    CargarCanalesDePago(DMConnections.BaseCAC, vcbCanalesPago, pCodigoCanalPago, 0);
                end;
            end;

            nbOpciones.PageIndex := PAGINA_ENTRADA_DATOS_SELECCIONE; // Seleccione Forma de Pago
            FAltaDatos           := pAlta;
            FEdicionCuotaNueva   := FEntradaDatos.DatosCuota.CuotaNueva;
            FEnTramite           := FEntradaDatos.DatosCuota.EnTramite;

            FAhora           := Trunc(NowBaseSmall(DMConnections.BaseCAC));
            deFechaPago.Date := pFechaPago;

            if (not FAltaDatos) then begin
                if (not FEdicionCuotaNueva) and (not FEnTramite) and (not FEntradaPago) then begin
                    edtCodigoFormaPago.Enabled := False;
                    edtDescFormaPago.Enabled   := False;
                end;
                edtCodigoFormaPago.Text    := FEntradaDatos.CodigoEntradaUsuario;
        //        edtCodigoFormaPagoChange(Nil);
            end
            else begin
                ActiveControl := edtCodigoFormaPago;
            end;

            Result := True;
        except
            on e:Exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end
    finally
        if Assigned(CuotasTmp) then FreeAndNil(CuotasTmp);

        CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

procedure TRefinanciacionEntrarEditarCuotasForm.spbTarjetaCreditoCopiarDatosClienteClick(Sender: TObject);
begin
    edtTarjetaCreditoTitular.Text := FEntradaDatos.PersonaNombreCompleto;
end;

procedure TRefinanciacionEntrarEditarCuotasForm.spbTarjetaDebitoCopiarDatosClienteClick(Sender: TObject);
begin
    edtTarjetaDebitoTitular.Text := FEntradaDatos.PersonaNombreCompleto;
end;

procedure TRefinanciacionEntrarEditarCuotasForm.btnAceptarClick(Sender: TObject);
begin
    if edtCodigoFormaPago.Tag = 1 then begin
        case FCodigoPantalla of
            PAGINA_ENTRADA_DATOS_EFECTIVO_CAMBIO: begin
                if ValidarDatosEfectivo then begin
                    with FEntradaDatos do begin
                        TitularNombre          := EmptyStr;
                        TitularTipoDocumento   := EmptyStr;
                        TitularNumeroDocumento := EmptyStr;
                        CodigoBanco            := 0;
                        DocumentoNumero        := EmptyStr;
                        Fecha                  := NullDate;
                        CodigoTarjeta          := 0;
                        TarjetaCuotas          := 0;

                        DatosCuota.Saldo       := DatosCuota.Saldo;

                        DescripcionFormaPago   := edtDescFormaPago.Text;
                        DescripcionBanco       := EmptyStr;
                        DescripcionTarjeta     := EmptyStr;
                        end;

                    ModalResult := mrOk;
                end;
            end;

            PAGINA_ENTRADA_DATOS_EFECTIVO:
                if ValidarDatosEfectivo then begin
                    with FEntradaDatos do begin
                        TitularNombre          := EmptyStr;
                        TitularTipoDocumento   := EmptyStr;
                        TitularNumeroDocumento := EmptyStr;
                        CodigoBanco            := 0;
                        DocumentoNumero        := EmptyStr;
                        Fecha                  := dedtEfectivo.Date;
                        CodigoTarjeta          := 0;
                        TarjetaCuotas          := 0;
                        Importe                := nedtEfectivoImporte.ValueInt;

                        DatosCuota.Saldo       := iif(nedtEfectivoImporte.Enabled, Importe, DatosCuota.Saldo);

                        DescripcionFormaPago   := edtDescFormaPago.Text;
                        DescripcionBanco       := EmptyStr;
                        DescripcionTarjeta     := EmptyStr;
                    end;

                    ModalResult := mrOk;
                end;

            PAGINA_ENTRADA_DATOS_CHEQUE:
                if ValidarDatosCheque then begin
                    with FEntradaDatos do begin
                        TitularNombre          := txtChequeNombreTitular.Text;
                        TitularTipoDocumento   := TIPO_DOCUMENTO_RUT;
                        TitularNumeroDocumento := txtChequeDocumento.Text;
                        CodigoBanco            := cbChequeBancos.Value;
                        DocumentoNumero        := txtChequeNumero.Text;
                        Fecha                  := deChequeFecha.Date;
                        CodigoTarjeta          := 0;
                        TarjetaCuotas          := 0;
                        Importe                := nedtImporteCheque.ValueInt;
                        DatosCuota.Saldo       := iif(nedtImporteCheque.Enabled, Importe, DatosCuota.Saldo);

                        DescripcionFormaPago   := edtDescFormaPago.Text;
                        DescripcionBanco       := cbChequeBancos.Text;
                        DescripcionTarjeta     := EmptyStr;
                    end;

                    ModalResult := mrOk;
                end;

            PAGINA_ENTRADA_DATOS_VALE_VISTA:
                if ValidarDatosValeVista then begin
                    with FEntradaDatos do begin
                        TitularNombre          := txt_TomadoPorVV.Text;
                        TitularTipoDocumento   := TIPO_DOCUMENTO_RUT;
                        TitularNumeroDocumento := txt_RUTVV.Text;
                        CodigoBanco            := cb_BancosVV.Value;
                        DocumentoNumero        := txt_NroVV.Text;
                        Fecha                  := de_FechaVV.Date;
                        CodigoTarjeta          := 0;
                        TarjetaCuotas          := 0;
                        Importe                := nedtImporteVV.ValueInt;
                        DatosCuota.Saldo       := iif(nedtImporteVV.Enabled, Importe, DatosCuota.Saldo);

                        DescripcionFormaPago   := edtDescFormaPago.Text;
                        DescripcionBanco       := cb_BancosVV.Text;
                        DescripcionTarjeta     := EmptyStr;
                    end;

                    ModalResult := mrOk;
                end;

            PAGINA_ENTRADA_DATOS_TARJETA_CREDITO:
                if ValidarDatosTarjetaCredito then begin
                    with FEntradaDatos do begin
                        TitularNombre          := edtTarjetaCreditoTitular.Text;
                        TitularTipoDocumento   := EmptyStr;
                        TitularNumeroDocumento := EmptyStr;
                        CodigoBanco            := vcbTarjetaCreditoBancos.Value;
                        DocumentoNumero        := iif(edtTarjetaCreditoNroConfirmacion.Text <> EmptyStr, edtTarjetaCreditoNroConfirmacion.Text, EmptyStr);
                        Fecha                  := dedtFechaTarjetaCredito.Date;
                        CodigoTarjeta          := vcbTarjetaCreditoTarjetas.Value;
                        TarjetaCuotas          := nedtTarjetaCreditoCuotas.ValueInt;
                        Importe                := nedtTarjetaCreditoImporte.ValueInt;
                        DatosCuota.Saldo       := iif(nedtTarjetaCreditoImporte.Enabled, Importe, DatosCuota.Saldo);

                        DescripcionFormaPago   := edtDescFormaPago.Text;
                        DescripcionBanco       := vcbTarjetaCreditoBancos.Text;
                        DescripcionTarjeta     := vcbTarjetaCreditoTarjetas.Text;
                    end;

                    ModalResult := mrOk;
                end;

            PAGINA_ENTRADA_DATOS_TARJETA_DEBITO:
                if ValidarDatosTarjetaDebito then begin
                    with FEntradaDatos do begin
                        TitularNombre          := edtTarjetaDebitoTitular.Text;
                        TitularTipoDocumento   := EmptyStr;
                        TitularNumeroDocumento := EmptyStr;
                        CodigoBanco            := vcbTarjetaDebitoBancos.Value;
                        DocumentoNumero        := iif(edtTarjetaCreditoNroConfirmacion.Text <> EmptyStr, edtTarjetaDebitoNroConfirmacion.Text, EmptyStr);;
                        Fecha                  := dedtFechaTarjetaDebito.Date;
                        CodigoTarjeta          := 0;
                        TarjetaCuotas          := 0;
                        Importe                := nedtTarjetaDebitoImporte.ValueInt;
                        DatosCuota.Saldo       := iif(nedtTarjetaDebitoImporte.Enabled, Importe, DatosCuota.Saldo);

                        DescripcionFormaPago   := edtDescFormaPago.Text;
                        DescripcionBanco       := vcbTarjetaDebitoBancos.Text;
                        DescripcionTarjeta     := EmptyStr;
                    end;

                    ModalResult := mrOk;
                end;

            PAGINA_ENTRADA_DATOS_PAGARE:
                if ValidarDatosPagare then begin
                    with FEntradaDatos do begin
                        TitularNombre          := txtPagareNombreTitular.Text;
                        TitularTipoDocumento   := TIPO_DOCUMENTO_RUT;
                        TitularNumeroDocumento := txtPagareDocumento.Text;
                        CodigoBanco            := cbPagareBancos.Value;
                        DocumentoNumero        := txtPagareNumero.Text;
                        Fecha                  := dePagareFecha.Date;
                        CodigoTarjeta          := 0;
                        TarjetaCuotas          := 0;
                        Importe                := nedtPagareImporte.ValueInt;
                        DatosCuota.Saldo       := iif(nedtPagareImporte.Enabled, Importe, DatosCuota.Saldo);

                        DescripcionFormaPago   := edtDescFormaPago.Text;
                        DescripcionBanco       := cbPagareBancos.Text;
                        DescripcionTarjeta     := EmptyStr;
                    end;

                    ModalResult := mrOk;
                end;
        end;
    end
    else begin
        ShowMsgBoxCN('Validaci�n Forma de Pago', 'Debe indicar una Forma de Pago.', MB_ICONWARNING, Self);
        ActiveControl := edtCodigoFormaPago;
    end;
end;

procedure TRefinanciacionEntrarEditarCuotasForm.btnCancelarClick(Sender: TObject);
begin
    ModalResult := mrCancel;
end;

procedure TRefinanciacionEntrarEditarCuotasForm.btnDatosClienteChequeClick(Sender: TObject);
begin
    try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        txtChequeNombreTitular.Text := FEntradaDatos.PersonaNombreCompleto;
        txtChequeDocumento.Text     := FEntradaDatos.Persona.NumeroDocumento;
    finally
        Screen.Cursor := crDefault;
    end;
end;

procedure TRefinanciacionEntrarEditarCuotasForm.btnDatosClientePagareClick(Sender: TObject);
begin
    try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        txtPagareNombreTitular.Text := FEntradaDatos.PersonaNombreCompleto;
        txtPagareDocumento.Text     := FEntradaDatos.Persona.NumeroDocumento;
    finally
        Screen.Cursor := crDefault;
    end;
end;

procedure TRefinanciacionEntrarEditarCuotasForm.btn_DatosClienteValeVistaClick(Sender: TObject);
begin
    try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        txt_TomadoPorVV.Text := FEntradaDatos.PersonaNombreCompleto;
        txt_RUTVV.Text       := FEntradaDatos.Persona.NumeroDocumento;
    finally
        Screen.Cursor := crDefault;
    end;
end;

procedure TRefinanciacionEntrarEditarCuotasForm.deFechaPagoChange(Sender: TObject);
begin
    FEntradaDatos.FechaPago := deFechaPago.Date;
end;

procedure TRefinanciacionEntrarEditarCuotasForm.edtCodigoFormaPagoChange(Sender: TObject);
    resourcestring
        MSG_FORMA_DE_PAGO_ERRONEA   = 'La Forma de Pago NO es V�lida para el Canal seleccionado.';
        MSG_FORMA_DE_PAGO_NO_EXISTE = 'La Forma de Pago NO Existe.';
        MSG_CAPTION_VALIDACION      = 'Validar Forma de Pago';
        MSG_ERROR_VALIDACION        = 'Se produjo un Error al Validar Forma de Pago. Error: %s.';
        MSG_ERROR_FORMA_NO_DEFINIDA = 'La forma de Pago especificada NO tiene definida su entrada de Datos.';
    var
        MaxDias: Integer;
        MinDias: Integer;
        ControlAActivar: TWinControl;

    procedure ResetearControlesFormaPago;
    begin
        FEntradaDatos.CodigoFormaPago := 0;
        nbOpciones.PageIndex          := PAGINA_ENTRADA_DATOS_SELECCIONE; // Seleccione Forma de Pago
        edtCodigoFormaPago.Tag        := 0;
    end;
begin
    case Length(edtCodigoFormaPago.Text) of
        2: try
            with spValidarFormaDePagoEnCanal do begin
                if Active then Close;
                Parameters.ParamByName('@CodigoTipoMedioPago').Value  := FEntradaDatos.CodigoCanalPago;
                Parameters.ParamByName('@CodigoEntradaUsuario').Value := edtCodigoFormaPago.Text;
                // Asignamos 0 ya que el SP no estamos en la ventana de Pago Anticipado
                Parameters.ParamByName('@ValidarPagoAnticipado').Value := 0;
                Parameters.ParamByName('@CodigoUsuario').Value          := UsuarioSistema;						//SS_1410_MCA_20151027
                Parameters.ParamByName('@CodigoSistema').Value          := SistemaActual;						//SS_1410_MCA_20151027
                Parameters.ParamByName('@CodigoFormaDePago').Value      := Null;
                Parameters.ParamByName('@DescripcionFormaDePago').Value := Null;
                Parameters.ParamByName('@EsValidaEnCanal').Value        := Null;
                Parameters.ParamByName('@AceptaPagoParcial').Value      := Null;
                Parameters.ParamByName('@CodigoPantalla').Value         := Null;
                Parameters.ParamByName('@CodigoBanco').Value            := Null;
                Parameters.ParamByName('@CodigoTarjeta').Value          := Null;

                ExecProc;

                edtDescFormaPago.Text := Parameters.ParamByName('@DescripcionFormaDePago').Value;

                if Parameters.ParamByName('@EsValidaEnCanal').Value = 0 then begin

                    ResetearControlesFormaPago;

                    if Parameters.ParamByName('@CodigoFormaDePago').Value = 0 then begin
                        ShowMsgBoxCN(MSG_CAPTION_VALIDACION, MSG_FORMA_DE_PAGO_NO_EXISTE, MB_ICONERROR, Self)
                    end
                    else ShowMsgBoxCN(MSG_CAPTION_VALIDACION, MSG_FORMA_DE_PAGO_ERRONEA, MB_ICONERROR, Self);

                    ControlAActivar := edtCodigoFormaPago;
                end else try

                    FEntradaDatos.CodigoFormaPago := Parameters.ParamByName('@CodigoFormaDePago').Value;

                    if (not FEntradaPago) or                                        // SS_637_PDO_20110503
                        (FEntradaPago and                                           // SS_637_PDO_20110503
                            (FEntradaDatos.CodigoFormaPago in                       // SS_637_PDO_20110503
                                [FORMA_PAGO_EFECTIVO,                               // SS_637_PDO_20110503
                                 FORMA_PAGO_VALE_VISTA,                             // SS_637_PDO_20110503
                                 FORMA_PAGO_CHEQUE,                                 // SS_637_PDO_20110503
                                 FORMA_PAGO_TARJETA_CREDITO,                        // SS_637_PDO_20110503
                                 FORMA_PAGO_TARJETA_DEBITO,                         // SS_637_PDO_20110503
                                 FORMA_PAGO_DEPOSITO_ANTICIPADO,                    // SS_637_PDO_20110503
                                 FORMA_PAGO_FONDO_NO_RECUPERABLE])) then begin      //SS_1410_MCA_20151027

                        FEntradaDatos.CodigoEntradaUsuario := edtCodigoFormaPago.Text;
                        FCodigoPantalla                    := Parameters.ParamByName('@CodigoPantalla').Value;
                        FCodigoBancoPorDefecto             := Parameters.ParamByName('@CodigoBanco').Value;
                        FCodigoTarjetaPorDefecto           := Parameters.ParamByName('@CodigoTarjeta').Value;
                        edtCodigoFormaPago.Tag             := 1;

                        case FCodigoPantalla of

                            PAGINA_ENTRADA_DATOS_TARJETA_DEBITO: begin

                                nbOpciones.PageIndex := PAGINA_ENTRADA_DATOS_TARJETA_DEBITO;
                                CambiarEstadoCursor(CURSOR_RELOJ, Self, FComponentes);

                                if vcbTarjetaDebitoBancos.Items.Count = 0 then begin
                                    CargarBancosPagoVentanilla(DMConnections.BaseCAC, vcbTarjetaDebitoBancos, iif(FAltaDatos, iif(FUltimoBancoEntrado <> 0, FUltimoBancoEntrado, FCodigoBancoPorDefecto), FEntradaDatos.CodigoBanco), FEntradaDatos.CodigoCanalPago,True);
                                end;

                                edtTarjetaDebitoTitular.Text         := iif(FAltaDatos, iif(FEntradaPago, EmptyStr, FEntradaDatos.PersonaNombreCompleto), FEntradaDatos.TitularNombre);
                                edtTarjetaDebitoNroConfirmacion.Text := iif(edtTarjetaDebitoNroConfirmacion.Enabled, iif(FAltaDatos, EmptyStr, FEntradaDatos.DocumentoNumero), EmptyStr);
                                dedtFechaTarjetaDebito.Date          := iif(FEntradaDatos.DatosCuota.EnTramite, iif(FAltaDatos, FEntradaDatos.FechaPago, FEntradaDatos.Fecha), iif(FAltaDatos, FAhora, FEntradaDatos.Fecha));
                                nedtTarjetaDebitoImporte.Value       := iif(FAltaDatos, 0, FEntradaDatos.Importe);

                                if FAltaDatos or FEntradaDatos.DatosCuota.CuotaNueva or FEntradaDatos.DatosCuota.EnTramite or FEntradaPago then begin
                                    ActualizaListaObjetos(FComponentes, TObject(lblTarjetaDebitoImporte), (not FEntradaPago));
                                    ActualizaListaObjetos(FComponentes, TObject(nedtTarjetaDebitoImporte), (not FEntradaPago));
                                    ActualizaListaObjetos(FComponentes, TObject(dedtFechaTarjetaDebito), (not FEntradaPago));
                                    ActualizaListaObjetos(FComponentes, TObject(lblTarjetaDebitoNroConfirmacion), FEntradaPago);
                                    ActualizaListaObjetos(FComponentes, TObject(edtTarjetaDebitoNroConfirmacion), FEntradaPago);
                                    {
                                    nedtTarjetaDebitoImporte.Enabled        := (not FEntradaPago);
                                    dedtFechaTarjetaDebito.Enabled          := (not FEntradaPago);
                                    lblTarjetaDebitoNroConfirmacion.Enabled := FEntradaPago;
                                    edtTarjetaDebitoNroConfirmacion.Enabled := FEntradaPago;
                                    }
                                    ControlAActivar := iif(FAltaDatos, iif(edtTarjetaDebitoTitular.Text = EmptyStr, edtTarjetaDebitoTitular, iif(vcbTarjetaDebitoBancos.Value = 0, vcbTarjetaDebitoBancos, dedtFechaTarjetaDebito)), edtTarjetaDebitoTitular);
                                end
                                else begin
                                    ActualizaListaObjetos(FComponentes, TObject(vcbTarjetaDebitoBancos), False);
                                    ActualizaListaObjetos(FComponentes, TObject(edtTarjetaDebitoTitular), False);
                                    ActualizaListaObjetos(FComponentes, TObject(edtTarjetaDebitoNroConfirmacion), False);
                                    ActualizaListaObjetos(FComponentes, TObject(dedtFechaTarjetaDebito), False);
                                    ActualizaListaObjetos(FComponentes, TObject(spbTarjetaDebitoCopiarDatosCliente), False);
                                    ActualizaListaObjetos(FComponentes, TObject(lblTarjetaDebitoImporte), (FEntradaDatos.Importe = FEntradaDatos.DatosCuota.Saldo));
                                    ActualizaListaObjetos(FComponentes, TObject(nedtTarjetaDebitoImporte), (FEntradaDatos.Importe = FEntradaDatos.DatosCuota.Saldo));
                                    {
                                    vcbTarjetaDebitoBancos.Enabled             := False;
                                    edtTarjetaDebitoTitular.Enabled            := False;
                                    edtTarjetaDebitoNroConfirmacion.Enabled    := False;
                                    dedtFechaTarjetaDebito.Enabled             := False;
                                    spbTarjetaDebitoCopiarDatosCliente.Enabled := False;
                                    nedtTarjetaDebitoImporte.Enabled           := (FEntradaDatos.Importe = FEntradaDatos.DatosCuota.Saldo);
                                    }
                                    ControlAActivar := iif(ExisteEnListaObjetos(FComponentes, TObject(nedtTarjetaDebitoImporte)), nedtTarjetaDebitoImporte, vcbCuotaAntecesora);
                                end;
                            end;

                            PAGINA_ENTRADA_DATOS_TARJETA_CREDITO: begin

                                nbOpciones.PageIndex := PAGINA_ENTRADA_DATOS_TARJETA_CREDITO;
                                CambiarEstadoCursor(CURSOR_RELOJ, Self, FComponentes);

                                if vcbTarjetaCreditoBancos.Items.Count = 0 then begin
                                    CargarBancosPagoVentanilla(
                                        DMConnections.BaseCAC,
                                        vcbTarjetaCreditoBancos,
                                        iif(FAltaDatos, iif(FUltimoBancoEntrado <> 0, FUltimoBancoEntrado, FCodigoBancoPorDefecto), FEntradaDatos.CodigoBanco),
                                        FEntradaDatos.CodigoCanalPago,
                                        True);
                                end;
                                if vcbTarjetaCreditoTarjetas.Items.Count = 0 then begin
                                    CargarTiposTarjetasCreditoVentanilla(DMConnections.BaseCAC,vcbTarjetaCreditoTarjetas,iif(FAltaDatos, FCodigoTarjetaPorDefecto, FEntradaDatos.CodigoTarjeta),True,True);
                                end;

                                edtTarjetaCreditoTitular.Text         := iif(FAltaDatos, iif(FEntradaPago, EmptyStr, FEntradaDatos.PersonaNombreCompleto), FEntradaDatos.TitularNombre);
                                edtTarjetaCreditoNroConfirmacion.Text := iif(edtTarjetaCreditoNroConfirmacion.Enabled, iif(FAltaDatos, EmptyStr, FEntradaDatos.DocumentoNumero), EmptyStr);
                                dedtFechaTarjetaCredito.Date          := iif(FEntradaDatos.DatosCuota.EnTramite, iif(FAltaDatos, FEntradaDatos.FechaPago, FEntradaDatos.Fecha), iif(FAltaDatos, FAhora, FEntradaDatos.Fecha));
                                nedtTarjetaCreditoCuotas.Value        := iif(FAltaDatos, 1, FEntradaDatos.TarjetaCuotas);
                                nedtTarjetaCreditoImporte.Value       := iif(FAltaDatos, 0, FEntradaDatos.Importe);

                                if FAltaDatos or FEntradaDatos.DatosCuota.CuotaNueva or FEntradaDatos.DatosCuota.EnTramite or FEntradaPago then begin
                                    ActualizaListaObjetos(FComponentes, TObject(lblTarjetaCreditoImporte), (not FEntradaPago));
                                    ActualizaListaObjetos(FComponentes, TObject(nedtTarjetaCreditoImporte), (not FEntradaPago));
                                    ActualizaListaObjetos(FComponentes, TObject(dedtFechaTarjetaCredito), (not FEntradaPago));
                                    ActualizaListaObjetos(FComponentes, TObject(lblTarjetaCreditoNroConfirmacion), FEntradaPago);
                                    ActualizaListaObjetos(FComponentes, TObject(edtTarjetaCreditoNroConfirmacion), FEntradaPago);
                                    {
                                    nedtTarjetaCreditoImporte.Enabled        := (not FEntradaPago);
                                    dedtFechaTarjetaCredito.Enabled          := (not FEntradaPago);
                                    lblTarjetaCreditoNroConfirmacion.Enabled := FEntradaPago;
                                    edtTarjetaCreditoNroConfirmacion.Enabled := FEntradaPago;
                                    }
                                    ControlAActivar := iif(FAltaDatos, iif(edtTarjetaCreditoTitular.Text = EmptyStr, edtTarjetaCreditoTitular, iif(vcbTarjetaCreditoBancos.Value = 0, vcbTarjetaCreditoBancos, vcbTarjetaCreditoTarjetas)), edtTarjetaCreditoTitular);
                                end
                                else begin
                                    ActualizaListaObjetos(FComponentes, TObject(vcbTarjetaCreditoBancos), False);
                                    ActualizaListaObjetos(FComponentes, TObject(vcbTarjetaCreditoTarjetas), False);
                                    ActualizaListaObjetos(FComponentes, TObject(edtTarjetaCreditoTitular), False);
                                    ActualizaListaObjetos(FComponentes, TObject(edtTarjetaCreditoNroConfirmacion), False);
                                    ActualizaListaObjetos(FComponentes, TObject(nedtTarjetaCreditoCuotas), False);
                                    ActualizaListaObjetos(FComponentes, TObject(dedtFechaTarjetaCredito), False);
                                    ActualizaListaObjetos(FComponentes, TObject(spbTarjetaCreditoCopiarDatosCliente), False);
                                    ActualizaListaObjetos(FComponentes, TObject(lblTarjetaCreditoImporte), (FEntradaDatos.Importe = FEntradaDatos.DatosCuota.Saldo));
                                    ActualizaListaObjetos(FComponentes, TObject(nedtTarjetaCreditoImporte), (FEntradaDatos.Importe = FEntradaDatos.DatosCuota.Saldo));
                                    {
                                    vcbTarjetaCreditoBancos.Enabled             := False;
                                    vcbTarjetaCreditoTarjetas.Enabled           := False;
                                    edtTarjetaCreditoTitular.Enabled            := False;
                                    edtTarjetaCreditoNroConfirmacion.Enabled    := False;
                                    nedtTarjetaCreditoCuotas.Enabled            := False;
                                    dedtFechaTarjetaCredito.Enabled             := False;
                                    spbTarjetaCreditoCopiarDatosCliente.Enabled := False;
                                    nedtTarjetaCreditoImporte.Enabled           := (FEntradaDatos.Importe = FEntradaDatos.DatosCuota.Saldo);
                                    }
                                    ControlAActivar := iif(ExisteEnListaObjetos(FComponentes, TObject(nedtTarjetaCreditoImporte)), nedtTarjetaCreditoImporte, vcbCuotaAntecesora);
                                end;
                            end;

                            PAGINA_ENTRADA_DATOS_CHEQUE: begin

                                nbOpciones.PageIndex := PAGINA_ENTRADA_DATOS_CHEQUE;
                                CambiarEstadoCursor(CURSOR_RELOJ, Self, FComponentes);

                                if cbChequeBancos.Items.Count = 0 then begin
                                    CargarBancosPagoVentanilla(
                                        DMConnections.BaseCAC,
                                        cbChequeBancos,
                                        iif(FAltaDatos, iif(FUltimoBancoEntrado <> 0, FUltimoBancoEntrado, FCodigoBancoPorDefecto), FEntradaDatos.CodigoBanco),
                                        FEntradaDatos.CodigoCanalPago,
                                        True);
                                end;

                                txtChequeNombreTitular.Text := iif(FAltaDatos, iif(FEntradaPago, EmptyStr, FEntradaDatos.PersonaNombreCompleto), FEntradaDatos.TitularNombre);
                                txtChequeDocumento.Text     := iif(FAltaDatos, iif(FEntradaPago, EmptyStr, FEntradaDatos.Persona.NumeroDocumento), FEntradaDatos.TitularNumeroDocumento);
                                txtChequeNumero.Text        := iif(FAltaDatos, EmptyStr, FEntradaDatos.DocumentoNumero);
                                deChequeFecha.Date          := iif(FEntradaDatos.DatosCuota.EnTramite, iif(FAltaDatos, FEntradaDatos.FechaPago, FEntradaDatos.Fecha), iif(FAltaDatos, FAhora, FEntradaDatos.Fecha));
                                edtRepactacionCheque.Text   := '';
                                nedtImporteCheque.Value     := iif(FAltaDatos, 0, FEntradaDatos.Importe);

                                ObtenerParametroGeneral(DMConnections.BaseCAC, 'DIAS_FECHA_MINIMA_CHEQUE', MinDias);
                                ObtenerParametroGeneral(DMConnections.BaseCAC, 'DIAS_FECHA_MAXIMA_CHEQUE', MaxDias);
                                FFechaMaximaCheque := iif(FEntradaDatos.DatosCuota.EnTramite, FEntradaDatos.FechaPago, FAhora) + MaxDias + 1;
                                FFechaMinimaCheque := iif(FEntradaDatos.DatosCuota.EnTramite, FEntradaDatos.FechaPago, FAhora) - MinDias;

                                if FAltaDatos or FEntradaDatos.DatosCuota.CuotaNueva or FEntradaDatos.DatosCuota.EnTramite or FEntradaPago then begin
                                    ActualizaListaObjetos(FComponentes, TObject(nedtImporteCheque), (not FEntradaPago));
                                    ActualizaListaObjetos(FComponentes, TObject(deChequeFecha), (not FEntradaPago));
                                    {
                                    nedtImporteCheque.Enabled := (not FEntradaPago);
                                    deChequeFecha.Enabled     := (not FEntradaPago);
                                    }
                                    ControlAActivar := iif(FAltaDatos, iif(txtChequeNombreTitular.Text = EmptyStr, txtChequeNombreTitular, iif(cbChequeBancos.Value = 0, cbChequeBancos, txtChequeNumero)), txtChequeNombreTitular);
                                end
                                else begin
                                    ActualizaListaObjetos(FComponentes, TObject(cbChequeBancos), False);
                                    ActualizaListaObjetos(FComponentes, TObject(txtChequeNombreTitular), False);
                                    ActualizaListaObjetos(FComponentes, TObject(txtChequeDocumento), False);
                                    ActualizaListaObjetos(FComponentes, TObject(txtChequeNumero), False);
                                    ActualizaListaObjetos(FComponentes, TObject(deChequeFecha), False);
                                    ActualizaListaObjetos(FComponentes, TObject(btnDatosClienteCheque), False);
                                    ActualizaListaObjetos(FComponentes, TObject(nedtImporteCheque), (FEntradaDatos.Importe = FEntradaDatos.DatosCuota.Saldo));
                                    {
                                    cbChequeBancos.Enabled         := False;
                                    txtChequeNombreTitular.Enabled := False;
                                    txtChequeDocumento.Enabled     := False;
                                    txtChequeNumero.Enabled        := False;
                                    deChequeFecha.Enabled          := False;
                                    btnDatosClienteCheque.Enabled  := False;
                                    nedtImporteCheque.Enabled      := (FEntradaDatos.Importe = FEntradaDatos.DatosCuota.Saldo);
                                    }
                                    ControlAActivar := iif(ExisteEnListaObjetos(FComponentes, TObject(nedtImporteCheque)), nedtImporteCheque, vcbCuotaAntecesora);
                                end;
                            end;

                            PAGINA_ENTRADA_DATOS_EFECTIVO: begin

                                nbOpciones.PageIndex := iif(FEntradaPago, PAGINA_ENTRADA_DATOS_EFECTIVO_CAMBIO, PAGINA_ENTRADA_DATOS_EFECTIVO);
                                FCodigoPantalla := nbOpciones.PageIndex;
                                
                                CambiarEstadoCursor(CURSOR_RELOJ, Self, FComponentes);

                                if FEntradaPago then begin
                                    ControlAActivar := nedtImporteEntregado;
                                end
                                else begin
                                    nedtEfectivoImporte.Value := iif(FAltaDatos, 0, FEntradaDatos.Importe);
                                    dedtEfectivo.Date         := iif(FEntradaDatos.DatosCuota.EnTramite, iif(FAltaDatos, FEntradaDatos.FechaPago, FEntradaDatos.Fecha), iif(FAltaDatos, FAhora, FEntradaDatos.Fecha));

                                    if (FAltaDatos or FEntradaDatos.DatosCuota.CuotaNueva or FEntradaDatos.DatosCuota.EnTramite or FEntradaPago) then begin
                                        ActualizaListaObjetos(FComponentes, TObject(nedtEfectivoImporte), (not FEntradaPago));
                                        ActualizaListaObjetos(FComponentes, TObject(dedtEfectivo), (not FEntradaPago));
                                        {
                                        nedtEfectivoImporte.Enabled := (not FEntradaPago);
                                        dedtEfectivo.Enabled        := (not FEntradaPago);
                                        }
                                        ControlAActivar := iif(FEntradaPago, btnAceptar, iif(ExisteEnListaObjetos(FComponentes, TObject(nedtEfectivoImporte)), nedtEfectivoImporte, dedtEfectivo));
                                    end
                                    else begin
                                        ActualizaListaObjetos(FComponentes, TObject(dedtEfectivo), False);
                                        ActualizaListaObjetos(FComponentes, TObject(nedtEfectivoImporte), (FEntradaDatos.Importe = FEntradaDatos.DatosCuota.Saldo));
                                        {
                                        dedtEfectivo.Enabled        := False;
                                        nedtEfectivoImporte.Enabled := (FEntradaDatos.Importe = FEntradaDatos.DatosCuota.Saldo);
                                        }
                                        ControlAActivar := iif(ExisteEnListaObjetos(FComponentes, TObject(nedtEfectivoImporte)), nedtEfectivoImporte, vcbCuotaAntecesora);
                                    end;
                                end;
                            end;

                            PAGINA_ENTRADA_DATOS_VALE_VISTA: begin

                                nbOpciones.PageIndex := PAGINA_ENTRADA_DATOS_VALE_VISTA;
                                CambiarEstadoCursor(CURSOR_RELOJ, Self, FComponentes);

                                if cb_BancosVV.Items.Count = 0 then begin
                                    CargarBancosPagoVentanilla(
                                        DMConnections.BaseCAC,
                                        cb_BancosVV,
                                        iif(FAltaDatos, iif(FUltimoBancoEntrado <> 0, FUltimoBancoEntrado, FCodigoBancoPorDefecto), FEntradaDatos.CodigoBanco),
                                        FEntradaDatos.CodigoCanalPago,
                                        True);
                                end;

                                txt_TomadoPorVV.Text := iif(FAltaDatos, iif(FEntradaPago, EmptyStr, FEntradaDatos.PersonaNombreCompleto), FEntradaDatos.TitularNombre);
                                txt_RUTVV.Text       := iif(FAltaDatos, iif(FEntradaPago, EmptyStr, FEntradaDatos.Persona.NumeroDocumento), FEntradaDatos.TitularNumeroDocumento);
                                txt_NroVV.Text       := iif(FAltaDatos, EmptyStr, FEntradaDatos.DocumentoNumero);
                                de_FechaVV.Date      := iif(FAltaDatos, FAhora, FEntradaDatos.Fecha);
                                nedtImporteVV.Value  := iif(FAltaDatos, 0, FEntradaDatos.Importe);

                                if FAltaDatos or FEntradaDatos.DatosCuota.CuotaNueva or FEntradaDatos.DatosCuota.EnTramite or FEntradaPago then begin
                                    ActualizaListaObjetos(FComponentes, TObject(nedtImporteVV), (not FEntradaPago));

                                    //nedtImporteVV.Enabled := (not FEntradaPago);
                                    //de_FechaVV.Enabled    := (not FEntradaPago);

                                    ControlAActivar := iif(FAltaDatos, iif(txt_TomadoPorVV.Text = EmptyStr, txt_TomadoPorVV, iif(cb_BancosVV.Value = 0, cb_BancosVV, txt_NroVV)), txt_TomadoPorVV);
                                end
                                else begin
                                    ActualizaListaObjetos(FComponentes, TObject(cb_BancosVV), False);
                                    ActualizaListaObjetos(FComponentes, TObject(txt_TomadoPorVV), False);
                                    ActualizaListaObjetos(FComponentes, TObject(txt_RUTVV), False);
                                    ActualizaListaObjetos(FComponentes, TObject(txt_NroVV), False);
                                    ActualizaListaObjetos(FComponentes, TObject(de_FechaVV), False);
                                    ActualizaListaObjetos(FComponentes, TObject(btn_DatosClienteValeVista), False);
                                    ActualizaListaObjetos(FComponentes, TObject(nedtImporteVV), (FEntradaDatos.Importe = FEntradaDatos.DatosCuota.Saldo));
                                    {
                                    cb_BancosVV.Enabled               := False;
                                    txt_TomadoPorVV.Enabled           := False;
                                    txt_RUTVV.Enabled                 := False;
                                    txt_NroVV.Enabled                 := False;
                                    de_FechaVV.Enabled                := False;
                                    btn_DatosClienteValeVista.Enabled := False;
                                    nedtImporteVV.Enabled             := (FEntradaDatos.Importe = FEntradaDatos.DatosCuota.Saldo);
                                    }
                                    ControlAActivar := iif(ExisteEnListaObjetos(FComponentes, TObject(nedtImporteVV)), nedtImporteVV, vcbCuotaAntecesora);
                                end;
                            end;

                            PAGINA_ENTRADA_DATOS_PAGARE: begin

                                nbOpciones.PageIndex := PAGINA_ENTRADA_DATOS_PAGARE;
                                CambiarEstadoCursor(CURSOR_RELOJ, Self, FComponentes);

                                if cbPagareBancos.Items.Count = 0 then begin
                                    CargarBancosPagoVentanilla(
                                        DMConnections.BaseCAC,
                                        cbPagareBancos,
                                        iif(FAltaDatos, iif(FUltimoBancoEntrado <> 0, FUltimoBancoEntrado, FCodigoBancoPorDefecto), FEntradaDatos.CodigoBanco),
                                        FEntradaDatos.CodigoCanalPago,
                                        True);
                                end;

                                txtPagareNombreTitular.Text := iif(FAltaDatos, iif(FEntradaPago, EmptyStr, FEntradaDatos.PersonaNombreCompleto), FEntradaDatos.TitularNombre);
                                txtPagareDocumento.Text     := iif(FAltaDatos, iif(FEntradaPago, EmptyStr, FEntradaDatos.Persona.NumeroDocumento), FEntradaDatos.TitularNumeroDocumento);
                                txtPagareNumero.Text        := iif(FAltaDatos, iif(FUltimoNroPagareEntrado = EmptyStr, EmptyStr, FUltimoNroPagareEntrado), FEntradaDatos.DocumentoNumero);
                                dePagareFecha.Date          := iif(FEntradaDatos.DatosCuota.EnTramite, iif(FAltaDatos, FEntradaDatos.FechaPago, FEntradaDatos.Fecha), iif(FAltaDatos, FAhora, FEntradaDatos.Fecha));
                                edtRepactacion.Text         := '';
                                nedtPagareImporte.Value     := iif(FAltaDatos, 0, FEntradaDatos.Importe);
                            
                                ObtenerParametroGeneral(DMConnections.BaseCAC, 'DIAS_FECHA_MINIMA_PAGARE', MinDias);
                                ObtenerParametroGeneral(DMConnections.BaseCAC, 'DIAS_FECHA_MAXIMA_PAGARE', MaxDias);
                                FFechaMaximaPagare := iif(FEntradaDatos.DatosCuota.EnTramite, FEntradaDatos.FechaPago, FAhora) + MaxDias + 1;
                                FFechaMinimaPagare := iif(FEntradaDatos.DatosCuota.EnTramite, FEntradaDatos.FechaPago, FAhora) - MinDias;

                                if FAltaDatos or FEntradaDatos.DatosCuota.CuotaNueva or FEntradaDatos.DatosCuota.EnTramite or FEntradaPago then begin
                                    ActualizaListaObjetos(FComponentes, TObject(nedtPagareImporte), (not FEntradaPago));
                                    ActualizaListaObjetos(FComponentes, TObject(dePagareFecha), (not FEntradaPago));
                                    {
                                    nedtPagareImporte.Enabled := (not FEntradaPago);
                                    dePagareFecha.Enabled     := (not FEntradaPago);
                                    }
                                    ControlAActivar := iif(FAltaDatos, iif(txtPagareNombreTitular.Text = EmptyStr, txtPagareNombreTitular, iif(cbPagareBancos.Value = 0, cbPagareBancos, iif(txtPagareNumero.Text = EmptyStr, txtPagareNumero, dePagareFecha))), txtPagareNombreTitular);
                                end
                                else begin
                                    ActualizaListaObjetos(FComponentes, TObject(cbPagareBancos), False);
                                    ActualizaListaObjetos(FComponentes, TObject(txtPagareNombreTitular), False);
                                    ActualizaListaObjetos(FComponentes, TObject(txtPagareDocumento), False);
                                    ActualizaListaObjetos(FComponentes, TObject(txtPagareNumero), False);
                                    ActualizaListaObjetos(FComponentes, TObject(dePagareFecha), False);
                                    ActualizaListaObjetos(FComponentes, TObject(btnDatosClientePagare), False);
                                    ActualizaListaObjetos(FComponentes, TObject(nedtPagareImporte), (FEntradaDatos.Importe = FEntradaDatos.DatosCuota.Saldo));
                                    {
                                    cbPagareBancos.Enabled         := False;
                                    txtPagareNombreTitular.Enabled := False;
                                    txtPagareDocumento.Enabled     := False;
                                    txtPagareNumero.Enabled        := False;
                                    dePagareFecha.Enabled          := False;
                                    btnDatosClientePagare.Enabled  := False;
                                    nedtPagareImporte.Enabled      := (FEntradaDatos.Importe = FEntradaDatos.DatosCuota.Saldo);
                                    }
                                    ControlAActivar := iif(ExisteEnListaObjetos(FComponentes, TObject(nedtPagareImporte)), nedtPagareImporte, vcbCuotaAntecesora);
                                end;
                            end;
                        else
                            nbOpciones.PageIndex          := PAGINA_ENTRADA_DATOS_NO_DEFINIDA;
                            FEntradaDatos.CodigoFormaPago := 0;
                            edtCodigoFormaPago.Tag        := 0;
                        
                            ShowMsgBoxCN(MSG_CAPTION_VALIDACION, MSG_ERROR_FORMA_NO_DEFINIDA, MB_ICONERROR, Self);
                        end;
                    end
                    else begin
                        ResetearControlesFormaPago;
                        ShowMsgBoxCN(MSG_CAPTION_VALIDACION, MSG_FORMA_DE_PAGO_ERRONEA, MB_ICONERROR, Self);
                        ControlAActivar := edtCodigoFormaPago;
                    end;
                finally
                    CambiarEstadoCursor(CURSOR_DEFECTO, Self, FComponentes);
                    ActiveControl := ControlAActivar;
                end;
            end;
        except
            on E:Exception do begin
                ResetearControlesFormaPago;
                ShowMsgBoxCN(MSG_CAPTION_VALIDACION, Format(MSG_ERROR_VALIDACION,[E.Message]), MB_ICONERROR, Self);
            end;
        end;
    else
        ResetearControlesFormaPago;
        edtDescFormaPago.Text := '';
    end;
end;

function TRefinanciacionEntrarEditarCuotasForm.ValidarDatosPagare: Boolean;
    resourcestring
        rsTituloValidaciones            = 'Validaci�n Datos %s';
        MSG_ERROR_NUMERO_PAGARE         = 'Ingrese el N�mero de %s';
        MSG_ERROR_FECHA_PAGARE_INVALIDA = 'La Fecha del %s no es V�lida';
        MSG_ERROR_FECHA_PAGARE_RANGO    = 'La Fecha del %s debe estar entre el %s y el %s ';

    var
        vControles  : Array [1..10] of TControl;
        vCondiciones: Array [1..10] of Boolean;
        vMensajes   : Array [1..10] of String;
begin
    try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;
        try
            vControles[1]   := txtPagareNombreTitular;
            vControles[2]   := txtPagareDocumento;
            vControles[3]   := txtPagareDocumento;
            vControles[4]   := cbPagareBancos;
            vControles[5]   := txtPagareNumero;
            vControles[6]   := dePagareFecha;
            vControles[7]   := dePagareFecha;
            vControles[8]   := nedtPagareImporte;
            vControles[9]   := neImportePagado;
            vControles[10]  := deFechaPago;

            vCondiciones[1] := (not txtPagareNombreTitular.Enabled) or (txtPagareNombreTitular.Text <> EmptyStr);
            vCondiciones[2] := (not txtPagareDocumento.Enabled) or (txtPagareDocumento.Text <> EmptyStr);
            vCondiciones[3] := (not txtPagareDocumento.Enabled) or ValidarRUT(DMConnections.BaseCAC, Trim(txtPagareDocumento.Text));
            vCondiciones[4] := (not cbPagareBancos.Enabled) or (cbPagareBancos.Value <> 0);
            vCondiciones[5] := (not txtPagareNumero.Enabled) or (txtPagareNumero.Text <> EmptyStr) or (FEntradaDatos.CodigoFormaPago = FORMA_PAGO_AVENIMIENTO);
            vCondiciones[6] := (not dePagareFecha.Enabled) or (not FEntradaPago) or IsValidDate(DateTimeToStr(dePagareFecha.Date));
            vCondiciones[7] := (not dePagareFecha.Enabled) or (not FEntradaPago) or (dePagareFecha.Date >= FFechaMinimaPagare) and (dePagareFecha.Date < FFechaMaximaPagare);
            vCondiciones[8] := (not nedtPagareImporte.Enabled) or (nedtPagareImporte.Value >= 0);
            vCondiciones[9]  := (not FEntradaPago) or (neImportePagado.ValueInt <= neImporte.ValueInt);
            vCondiciones[10] := (not FEntradaPago) or (deFechaPago.Date <= FAhora);

            vMensajes[1]    := MSG_ERROR_NOMBRE_TITULAR;
            vMensajes[2]    := MSG_ERROR_DOCUMENTO;
            vMensajes[3]    := MSG_ERROR_RUT_INVALIDO;
            vMensajes[4]    := MSG_ERROR_BANCO;
            vMensajes[5]    := Format(MSG_ERROR_NUMERO_PAGARE, [edtDescFormaPago.Text]);
            vMensajes[6]    := Format(MSG_ERROR_FECHA_PAGARE_INVALIDA, [edtDescFormaPago.Text]);
            vMensajes[7]    := Format(MSG_ERROR_FECHA_PAGARE_RANGO, [edtDescFormaPago.Text, FormatDateTime('dd/mm/yyyy', FFechaMinimaPagare), FormatDateTime('dd/mm/yyyy', FFechaMaximaPagare - 1)]);
            vMensajes[8]    := MSG_ERROR_IMPORTE;
            vMensajes[9]    := MSG_ERROR_IMPORTE_PAGADO;
            vMensajes[10]   := MSG_ERROR_FECHA_PAGO;
            
            Result := ValidateControlsCN(vControles, vCondiciones, Format(rsTituloValidaciones, [edtDescFormaPago.Text]), vMensajes, Self);
        except
            on e:exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
        Screen.Cursor := crDefault;
    end;
end;

function TRefinanciacionEntrarEditarCuotasForm.ValidarDatosCheque: Boolean;
    resourcestring
        rsTituloValidaciones            = 'Validaci�n Datos Cheque.';
        MSG_ERROR_NUMERO_CHEQUE         = 'Ingrese el N�mero de Cheque.';
        MSG_ERROR_FECHA_CHEQUE_INVALIDA = 'La Fecha del Cheque no es V�lida.';
        MSG_ERROR_FECHA_CHEQUE_RANGO    = 'La Fecha del Cheque debe estar entre el %s y el %s.';

    var
        vControles  : Array [1..10] of TControl;
        vCondiciones: Array [1..10] of Boolean;
        vMensajes   : Array [1..10] of String;
begin
    try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;
        try
            vControles[1]   := txtChequeNombreTitular;
            vControles[2]   := txtChequeDocumento;
            vControles[3]   := txtChequeDocumento;
            vControles[4]   := cbChequeBancos;
            vControles[5]   := txtChequeNumero;
            vControles[6]   := deChequeFecha;
            vControles[7]   := deChequeFecha;
            vControles[8]   := nedtImporteCheque;
            vControles[9]   := neImportePagado;
            vControles[10]  := deFechaPago;

            vCondiciones[1]  := (not txtChequeNombreTitular.Enabled) or (txtChequeNombreTitular.Text <> EmptyStr);
            vCondiciones[2]  := (not txtChequeDocumento.Enabled) or (txtChequeDocumento.Text <> EmptyStr);
            vCondiciones[3]  := (not txtChequeDocumento.Enabled) or ValidarRUT(DMConnections.BaseCAC, Trim(txtChequeDocumento.Text));
            vCondiciones[4]  := (not cbChequeBancos.Enabled) or (cbChequeBancos.Value <> 0);
            vCondiciones[5]  := (not txtChequeNumero.Enabled) or (txtChequeNumero.Text <> EmptyStr);
            vCondiciones[6]  := (not deChequeFecha.Enabled) or (not FEntradaPago) or IsValidDate(DateTimeToStr(deChequeFecha.Date));
            vCondiciones[7]  := (not deChequeFecha.Enabled) or (not FEntradaPago) or (deChequeFecha.Date >= FFechaMinimaCheque) and (deChequeFecha.Date < FFechaMaximaCheque);
            vCondiciones[8]  := (not nedtImporteCheque.Enabled) or (nedtImporteCheque.Value >= 0);
            vCondiciones[9]  := (not FEntradaPago) or (neImportePagado.ValueInt <= neImporte.ValueInt);
            vCondiciones[10] := (not FEntradaPago) or (deFechaPago.Date <= FAhora);

            vMensajes[1]    := MSG_ERROR_NOMBRE_TITULAR;
            vMensajes[2]    := MSG_ERROR_DOCUMENTO;
            vMensajes[3]    := MSG_ERROR_RUT_INVALIDO;
            vMensajes[4]    := MSG_ERROR_BANCO;
            vMensajes[5]    := MSG_ERROR_NUMERO_CHEQUE;
            vMensajes[6]    := MSG_ERROR_FECHA_CHEQUE_INVALIDA;
            vMensajes[7]    := Format(MSG_ERROR_FECHA_CHEQUE_RANGO, [FormatDateTime('dd/mm/yyyy',FFechaMinimaCheque), FormatDateTime('dd/mm/yyyy',FFechaMaximaCheque - 1)]);
            vMensajes[8]    := MSG_ERROR_IMPORTE;
            vMensajes[9]    := MSG_ERROR_IMPORTE_PAGADO;
            vMensajes[10]   := MSG_ERROR_FECHA_PAGO;

            Result := ValidateControlsCN(vControles, vCondiciones, rsTituloValidaciones, vMensajes, Self);
        except
            on e:exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
        Screen.Cursor := crDefault;
    end;
end;

function TRefinanciacionEntrarEditarCuotasForm.ValidarDatosValeVista: Boolean;
    resourcestring
        rsTituloValidaciones        = 'Validaci�n Datos Vale Vista.';
        MSG_ERROR_NUMERO_VALE_VISTA = 'Ingrese el N�mero de Vale Vista.';

    var
        vControles  : Array [1..8] of TControl;
        vCondiciones: Array [1..8] of Boolean;
        vMensajes   : Array [1..8] of String;
begin
    try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;
        try
            vControles[1]   := txt_TomadoPorVV;
            vControles[2]   := txt_RUTVV;
            vControles[3]   := txt_RUTVV;
            vControles[4]   := cb_BancosVV;
            vControles[5]   := txt_NroVV;
            vControles[6]   := nedtImporteVV;
            vControles[7]   := neImportePagado;
            vControles[8]   := deFechaPago;

            vCondiciones[1] := (not txt_TomadoPorVV.Enabled) or (txt_TomadoPorVV.Text <> EmptyStr);
            vCondiciones[2] := (not txt_RUTVV.Enabled) or (txt_RUTVV.Text <> EmptyStr);
            vCondiciones[3] := (not txt_RUTVV.Enabled) or ValidarRUT(DMConnections.BaseCAC, Trim(txt_RUTVV.Text));
            vCondiciones[4] := (not cb_BancosVV.Enabled) or (cb_BancosVV.Value <> 0);
            vCondiciones[5] := (not txt_NroVV.Enabled) or (txt_NroVV.Text <> EmptyStr);
            vCondiciones[6] := (not nedtImporteVV.Enabled) or (nedtImporteVV.Value >= 0);
            vCondiciones[7] := (not FEntradaPago) or (neImportePagado.ValueInt <= neImporte.ValueInt);
            vCondiciones[8] := (not FEntradaPago) or (deFechaPago.Date <= FAhora);

            vMensajes[1]    := MSG_ERROR_NOMBRE_TITULAR;
            vMensajes[2]    := MSG_ERROR_DOCUMENTO;
            vMensajes[3]    := MSG_ERROR_RUT_INVALIDO;
            vMensajes[4]    := MSG_ERROR_BANCO;
            vMensajes[5]    := MSG_ERROR_NUMERO_VALE_VISTA;
            vMensajes[6]    := MSG_ERROR_IMPORTE;
            vMensajes[7]    := MSG_ERROR_IMPORTE_PAGADO;
            vMensajes[8]    := MSG_ERROR_FECHA_PAGO;

            Result := ValidateControlsCN(vControles, vCondiciones, rsTituloValidaciones, vMensajes, Self);
        except
            on e:exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
        Screen.Cursor := crDefault;
    end;
end;

function TRefinanciacionEntrarEditarCuotasForm.ValidarDatosTarjetaCredito: Boolean;
    resourcestring
        rsTituloValidaciones              = 'Validaci�n Datos Tarjeta Cr�dito.';
        MSG_ERROR_TARJETA_CREDITO_TARJETA = 'Debe Seleccionar un Tipo de Tarjeta de Cr�dito.';
        MSG_ERROR_CUOTAS                  = 'Ingrese un N�mero de Cuotas.';

    var
        vControles  : Array [1..9] of TControl;
        vCondiciones: Array [1..9] of Boolean;
        vMensajes   : Array [1..9] of String;
begin
    try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;
        try
            vControles[1]   := edtTarjetaCreditoTitular;
            vControles[2]   := vcbTarjetaCreditoBancos;
            vControles[3]   := vcbTarjetaCreditoTarjetas;
            vControles[4]   := edtTarjetaCreditoNroConfirmacion;
            vControles[5]   := edtTarjetaCreditoNroConfirmacion;
            vControles[6]   := nedtTarjetaCreditoCuotas;
            vControles[7]   := nedtTarjetaCreditoImporte;
            vControles[8]   := neImportePagado;
            vControles[9]   := deFechaPago;

            vCondiciones[1] := (not edtTarjetaCreditoTitular.Enabled) or (edtTarjetaCreditoTitular.Text <> EmptyStr);
            vCondiciones[2] := (not vcbTarjetaCreditoBancos.Enabled) or (vcbTarjetaCreditoBancos.Value <> 0);
            vCondiciones[3] := (not vcbTarjetaCreditoTarjetas.Enabled) or (vcbTarjetaCreditoTarjetas.Value <> 0);
            vCondiciones[4] := (not edtTarjetaCreditoNroConfirmacion.Enabled) or (edtTarjetaCreditoNroConfirmacion.Text <> '');
            vCondiciones[5] :=
                (not edtTarjetaCreditoNroConfirmacion.Enabled) or
                (edtTarjetaCreditoNroConfirmacion.Text = '') or
                (QueryGetValue(DMConnections.BaseCAC, format('select dbo.ExisteCodigoAutorizacionWebPay(''%s'')',[edtTarjetaCreditoNroConfirmacion.text])) = 'False');
            vCondiciones[6] := (not nedtTarjetaCreditoCuotas.Enabled) or (nedtTarjetaCreditoCuotas.Text <> '');
            vCondiciones[7] := (not nedtTarjetaCreditoImporte.Enabled) or (nedtTarjetaCreditoImporte.Value >= 0);
            vCondiciones[8] := (not FEntradaPago) or (neImportePagado.ValueInt <= neImporte.ValueInt);
            vCondiciones[9] := (not FEntradaPago) or (deFechaPago.Date <= FAhora);

            vMensajes[1]    := MSG_ERROR_NOMBRE_TITULAR;
            vMensajes[2]    := MSG_ERROR_BANCO;
            vMensajes[3]    := MSG_ERROR_TARJETA_CREDITO_TARJETA;
            vMensajes[4]    := MSG_ERROR_AUTORIZACION;
            vMensajes[5]    := MSG_ERROR_AUTORIZACION_DUPLICADO;
            vMensajes[6]    := MSG_ERROR_CUOTAS;
            vMensajes[7]    := MSG_ERROR_IMPORTE;
            vMensajes[8]    := MSG_ERROR_IMPORTE_PAGADO;
            vMensajes[9]    := MSG_ERROR_FECHA_PAGO;

            Result := ValidateControlsCN(vControles, vCondiciones, rsTituloValidaciones, vMensajes, Self);
        except
            on e:exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
        Screen.Cursor := crDefault;
    end;
end;

function TRefinanciacionEntrarEditarCuotasForm.ValidarDatosTarjetaDebito: Boolean;
    resourcestring
        rsTituloValidaciones = 'Validaci�n Datos Tarjeta D�bito.';

    var
        vControles  : Array [1..7] of TControl;
        vCondiciones: Array [1..7] of Boolean;
        vMensajes   : Array [1..7] of String;
begin
    try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;
        try
            vControles[1]   := edtTarjetaDebitoTitular;
            vControles[2]   := vcbTarjetaDebitoBancos;
            vControles[3]   := edtTarjetaDebitoNroConfirmacion;
            vControles[4]   := edtTarjetaDebitoNroConfirmacion;
            vControles[5]   := nedtTarjetaDebitoImporte;
            vControles[6]   := neImportePagado;
            vControles[7]   := deFechaPago;

            vCondiciones[1] := (not edtTarjetaDebitoTitular.Enabled) or (edtTarjetaDebitoTitular.Text <> EmptyStr);
            vCondiciones[2] := (not vcbTarjetaDebitoBancos.Enabled) or (vcbTarjetaDebitoBancos.Value <> 0);
            vCondiciones[3] := (not edtTarjetaDebitoNroConfirmacion.Enabled) or (edtTarjetaDebitoNroConfirmacion.Text <> '');
            vCondiciones[4] :=
                (not edtTarjetaDebitoNroConfirmacion.Enabled) or
                (edtTarjetaDebitoNroConfirmacion.Text = '') or
                (QueryGetValue(DMConnections.BaseCAC, format('select dbo.ExisteCodigoAutorizacionWebPay(''%s'')',[edtTarjetaDebitoNroConfirmacion.text])) = 'False');
            vCondiciones[5] := (not nedtTarjetaDebitoImporte.Enabled) or (nedtTarjetaDebitoImporte.Value >= 0);
            vCondiciones[6] := (not FEntradaPago) or (neImportePagado.ValueInt <= neImporte.ValueInt);
            vCondiciones[7] := (not FEntradaPago) or (deFechaPago.Date <= FAhora);

            vMensajes[1]    := MSG_ERROR_NOMBRE_TITULAR;
            vMensajes[2]    := MSG_ERROR_BANCO;
            vMensajes[3]    := MSG_ERROR_AUTORIZACION;
            vMensajes[4]    := MSG_ERROR_AUTORIZACION_DUPLICADO;
            vMensajes[5]    := MSG_ERROR_IMPORTE;
            vMensajes[6]    := MSG_ERROR_IMPORTE_PAGADO;
            vMensajes[7]    := MSG_ERROR_FECHA_PAGO;

            Result := ValidateControlsCN(vControles, vCondiciones, rsTituloValidaciones, vMensajes, Self);
        except
            on e:exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
        Screen.Cursor := crDefault;
    end;
end;

function TRefinanciacionEntrarEditarCuotasForm.ValidarDatosEfectivo: Boolean;
    resourcestring
        rsTituloValidaciones = 'Validaci�n Datos Efectivo.';

    var
        vControles  : Array [1..4] of TControl;
        vCondiciones: Array [1..4] of Boolean;
        vMensajes   : Array [1..4] of String;
begin
    vControles[1]   := nedtEfectivoImporte;
    vControles[2]   := neImportePagado;
    vControles[3]   := nedtImporteEntregado;    // SS_637_PDO_20110503
    vControles[4]   := deFechaPago;

    vCondiciones[1] := (not nedtEfectivoImporte.Enabled) or (nedtEfectivoImporte.Value >= 0);
    vCondiciones[2] := (not FEntradaPago) or (neImportePagado.ValueInt <= neImporte.ValueInt);
    vCondiciones[3] := (not FEntradaPago) or (nedtImporteEntregado.ValueInt >= neImportePagado.ValueInt);    // SS_637_PDO_20110503
    vCondiciones[4] := (not FEntradaPago) or (deFechaPago.Date <= FAhora);

    vMensajes[1]    := MSG_ERROR_IMPORTE;
    vMensajes[2]    := MSG_ERROR_IMPORTE_PAGADO;
    vMensajes[3]    := MSG_ERROR_IMPORTE_ENTREGADO;     // SS_637_PDO_20110503
    vMensajes[4]    := MSG_ERROR_FECHA_PAGO;

    Result := ValidateControlsCN(vControles, vCondiciones, rsTituloValidaciones, vMensajes, Self);
end;

procedure TRefinanciacionEntrarEditarCuotasForm.vcbCuotaAntecesoraChange(Sender: TObject);
begin
    FEntradaDatos.DatosCuota.CodigoCuotaAntecesora := vcbCuotaAntecesora.Value;
end;

procedure TRefinanciacionEntrarEditarCuotasForm.lblacaClick(Sender: TObject);
begin
    deFechaPago.Enabled := True;
    lblaca.Enabled      := False;
end;

procedure TRefinanciacionEntrarEditarCuotasForm.lblAcaMontoClick(Sender: TObject);
begin
    neImportePagado.Enabled := True;
    lblAcaMonto.Enabled     := False;
end;

procedure TRefinanciacionEntrarEditarCuotasForm.nedtImporteEntregadoChange(Sender: TObject);
begin
    if nedtImporteEntregado.ValueInt > neImportePagado.ValueInt then begin
        nedtCambio.ValueInt := (nedtImporteEntregado.ValueInt - neImportePagado.ValueInt);
    end
    else begin
        nedtCambio.ValueInt := 0;
    end;
end;

procedure TRefinanciacionEntrarEditarCuotasForm.neImportePagadoChange(Sender: TObject);
begin
    FEntradaDatos.ImportePagado := neImportePagado.ValueInt;
end;

initialization

finalization

end.


