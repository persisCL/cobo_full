{********************************** Unit Header ********************************
				frmBuscarTraspasos
Author: mbecerra
Date: 28-Julio-2009
Description :		(Ref SS 740)
            	El formulario se encarga de buscar los traspasoa realizados.

Firma       :  SS_1120_MVI_20130820
Descripcion :  Se cambia estilo del comboBox a: vcsOwnerDrawFixed,
               Se cambia el llamado a llenar el combo de convenios para que utilice el procedimiento  CargarConveniosRUT.
               Se crear el evento DrawItem y se llama al procedimeinto ItemComboBoxColor.
               Se elimina validaci�n de que convenio exista, ya que los datos son obtenidos desde un Combo llenado por un SP.
*******************************************************************************}
unit frmBuscarTraspasos;

interface

uses
    PeaProcs,
	UtilProc,
    UtilDb,
    Util,
    DMConnection,
	BuscaClientes,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DmiCtrls, Validate, DateEdit, ListBoxEx, DBListEx, DB,
  ADODB, VariantComboBox, PeaTypes;

type
  TBuscarTraspasosForm = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    deFechaDesde: TDateEdit;
    deFechaHasta: TDateEdit;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    neNumDesde: TNumericEdit;
    neNumHasta: TNumericEdit;
    peRut: TPickEdit;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    neMontoDesde: TNumericEdit;
    neMontoHasta: TNumericEdit;
    btnBuscar: TButton;
    GroupBox2: TGroupBox;
    dbListadoTraspasos: TDBListEx;
    dsListado: TDataSource;
    spObtenerTraspasos: TADOStoredProc;
    vcbConvenios: TVariantComboBox;
    spObtenerConvenios: TADOStoredProc;
    btnLimpiar: TButton;
    btnSalir: TButton;
    procedure peRutButtonClick(Sender: TObject);
    procedure btnBuscarClick(Sender: TObject);
    procedure vcbConveniosChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ValidaTeclaBusca(Sender: TObject; var Key: Char);
    procedure OrdenarPorColumna(Sender: TObject);
    procedure btnLimpiarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure vcbConveniosDrawItem(Control: TWinControl; Index: Integer;            //SS_1120_MVI_20130820
      Rect: TRect; State: TOwnerDrawState);                                         //SS_1120_MVI_20130820
  private
    { Private declarations }
    FUltimaBusqueda: TBusquedaCliente;
    FCodigoConvenio : int64;
  public
    { Public declarations }
    function Inicializar(sCaption : string) : boolean;
    function Validaciones : boolean;
    procedure Limpiar;
    procedure BuscarConvenios(Rut : string; vcb : TVariantComboBox);
    procedure BuscarTraspasos;

  end;

var
  BuscarTraspasosForm: TBuscarTraspasosForm;

implementation

{$R *.dfm}

{ *****************************************************
    			    Inicializar

Author: mbecerra
Date: 28-Julio-2009
Description:	Inicializa el formulario.

*******************************************************}
function TBuscarTraspasosForm.Inicializar;
begin
	Result := True;
    Caption := sCaption;
    CenterForm(Self);
    Limpiar();
end;

{ *****************************************************
    			    Limpiar

Author: mbecerra
Date: 28-Julio-2009
Description:	Limpia los filtros del formulario.

*******************************************************}
procedure TBuscarTraspasosForm.Limpiar;
begin
    deFechaDesde.Date := 0;
    deFechaHasta.Date := 0;
    neNumDesde.Value := 0;
    neNumHasta.Value := 0;
    neMontoDesde.Value := 0;
    neMontoHasta.Value := 0;
    peRut.Text := '';
    vcbConvenios.Clear;
    vcbConvenios.Text := '';
end;

{ *****************************************************
    			    OrdenarPorColumna

Author: mbecerra
Date: 28-Julio-2009
Description:	Ordena los datos de la grilla por la columna seleccionada.

*******************************************************}
procedure TBuscarTraspasosForm.OrdenarPorColumna(Sender: TObject);
begin
	if not TCustomDBListEx(TDBListExColumn(sender).Columns.List).DataSource.DataSet.Active then
        exit
    else
        OrdenarGridPorColumna(TDBListExColumn(sender).Columns.List,
                                TDBListExColumn(sender),
                                TDBListExColumn(sender).FieldName);
end;

{**********************************************************
        			BuscarConvenios
 Author: mbecerra
 Date: 28-Julio-2009
 Description: 	Busca los convenios para el Rut Ingresado

*********************************************************** }
procedure TBuscarTraspasosForm.btnLimpiarClick(Sender: TObject);
begin
	Limpiar();
end;

procedure TBuscarTraspasosForm.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TBuscarTraspasosForm.BuscarConvenios;
resourcestring
	MSG_NO_HAY_DATOS = 'No se encontraron convenios para el Rut %s Indicado';
    MSG_ERROR = 'Error al obtener los convenios asociados al Rut %s';

begin
	try
    	vcb.Clear;
   //  	spObtenerConvenios.Close;                                                                             //SS_1120_MVI_20130820
   //     spObtenerConvenios.Parameters.Refresh;                                                              //SS_1120_MVI_20130820
   //     spObtenerConvenios.Parameters.ParamByName('@CodigoDocumento').Value := 'RUT';                       //SS_1120_MVI_20130820
   // 	spObtenerConvenios.Parameters.ParamByName('@NumeroDocumento').Value := Rut;                           //SS_1120_MVI_20130820
   //     spObtenerConvenios.Parameters.ParamByName('@SoloActivos').Value := False;                           //SS_1120_MVI_20130820
   // 	spObtenerConvenios.Open;                                                                              //SS_1120_MVI_20130820
   // 	if not spObtenerConvenios.IsEmpty then begin                                                          //SS_1120_MVI_20130820
   //     	while not spObtenerConvenios.Eof do begin                                                         //SS_1120_MVI_20130820
	 //           vcb.Items.Add(	Trim(spObtenerConvenios.FieldByName('NumeroConvenio').AsString),              //SS_1120_MVI_20130820
   //                         	spObtenerConvenios.FieldByName('CodigoConvenio').AsInteger );                 //SS_1120_MVI_20130820
   //     		spObtenerConvenios.Next;                                                                        //SS_1120_MVI_20130820
   //     	end;                                                                                              //SS_1120_MVI_20130820
                                                                                                              //SS_1120_MVI_20130820
   //     	spObtenerConvenios.Close;                                                                         //SS_1120_MVI_20130820
                                                                                                              //SS_1120_MVI_20130820
   // 	end                                                                                                   //SS_1120_MVI_20130820
   // 	else MsgBox(Format(MSG_NO_HAY_DATOS,[Rut]), Caption, MB_ICONEXCLAMATION);                             //SS_1120_MVI_20130820
                                                                                                              //SS_1120_MVI_20130820
        CargarConveniosRUT(DMCOnnections.BaseCAC,vcb, 'RUT',                                                  //SS_1120_MVI_20130820
        PadL(Rut, 9, '0' ), False, 0, True);                                                                  //SS_1120_MVI_20130820
        if vcb.Items.Count = 0 then begin                                                                     //SS_1120_MVI_20130820
          MsgBox(Format(MSG_NO_HAY_DATOS,[Rut]), Caption, MB_ICONEXCLAMATION);                                //SS_1120_MVI_20130820
        end;                                                                                                  //SS_1120_MVI_20130820
    except on e:exception do begin
        	MsgBoxErr(Format(MSG_ERROR,[Rut]), e.Message, Caption, MB_ICONERROR);
    	end;
    end;

end;

{ *****************************************************
    			    Validaciones

Author: mbecerra
Date: 28-Julio-2009
Description:	Valida los filtros ingresados.
*******************************************************}
function TBuscarTraspasosForm.Validaciones;
resourcestring
	MSG_LA_1	= 'La';
    MSG_LA_2	= 'la';
    MSG_EL_1	= 'El';
    MSG_EL_2	= 'el';
	MSG_BASE	= '%s %s DESDE no puede ser superior a %s %s HASTA';
	MSG_FECHA	= 'Fecha';
    MSG_NUMERO	= 'N�mero';
    MSG_MONTO	= 'Monto';
    MSG_RUT		= 'El Rut %s ingresado es inv�lido';
    MSG_CONV	= 'El convenio ingresado no pertenece al Rut %s';
    MSG_EXISTE	= 'El convenio no existe';
    MSG_SQL		= 'SELECT dbo.ObtenerCodigoConvenio(''%s'')';

var
	Mensaje : string;
begin
    Result := True;
    {validar que la fecha hasta no sea inferior a la fecha desde}
    if	Result and (deFechaDesde.Date > 0) and (deFechaHasta.Date > 0) then begin
    	Mensaje := Format(MSG_BASE, [MSG_LA_1, MSG_FECHA, MSG_LA_2, MSG_FECHA]);
    	Result := ValidateControls([deFechaDesde], [deFechaDesde.Date <= deFechaHasta.Date], Caption, [Mensaje]);
    end;

    {validar que el n�mero hasta no sea inferior al n�mero desde}
    if Result and (neNumDesde.Value > 0) and (neNumHasta.Value > 0) then begin
        Mensaje := Format(MSG_BASE, [MSG_EL_1, MSG_NUMERO, MSG_EL_2, MSG_NUMERO]);
    	Result := ValidateControls([neNumDesde], [neNumDesde.Value <= neNumHasta.Value], Caption, [Mensaje]);
    end;

    {validar que el monto hasta no sea inferior al monto desde}
    if Result and (neMontoDesde.Value > 0) and (neMontoHasta.Value > 0) then begin
        Mensaje := Format(MSG_BASE, [MSG_EL_1, MSG_MONTO, MSG_EL_2, MSG_MONTO]);
    	Result := ValidateControls([neMontoDesde], [neMontoDesde.Value <= neMontoHasta.Value], Caption, [Mensaje]);
    end;

    {validar el Rut, y si ha ingresado un n�mero de convenio, que �ste pertenezca al rut}
    if Result and (peRut.Text <> '') then begin
		Result := ValidateControls([peRut], [ ValidarRUT(DMConnections.BaseCAC, peRut.Text)], Caption, [Format(MSG_RUT,[peRut.Text])]);
		if Result then begin
			Mensaje := vcbConvenios.Text;
            BuscarConvenios(peRut.Text, vcbConvenios);
            if Mensaje <> '' then begin
                vcbConvenios.Text := Mensaje;
            	Result := ValidateControls([vcbConvenios], [vcbConvenios.Items.IndexOf(Mensaje) >= 0], Caption, [Format(MSG_CONV,[peRut.Text])]);
            end;
        end;

    end;

    {validar que el convenio exista}
//    if Result and (vcbConvenios.Text <> '' )then begin                                                              //SS_1120_MVI_20130820
//    	FCodigoConvenio := QueryGetValueInt(DMConnections.BaseCAC, Format(MSG_SQL,[vcbConvenios.Text]));              //SS_1120_MVI_20130820
//        Result := ValidateControls([vcbConvenios], [FCodigoConvenio > 0], Caption, [MSG_EXISTE]);                   //SS_1120_MVI_20130820
//    end;                                                                                                            //SS_1120_MVI_20130820


end;

{ *****************************************************
    			    BuscarTraspasos

Author: mbecerra
Date: 28-Julio-2009
Description:	Despliega un listado con los traspasos encontrados de acuerdo a los
                filtros ingresados.

*******************************************************}
procedure TBuscarTraspasosForm.BuscarTraspasos;
resourcestring
    MSG_ERROR  = 'Ocurri� un error al intentar obtener los traspasos';
    MSG_NOENC  = 'No se encontraron Comprobantes que cumplan los criterios de selecci�n';
    
begin
    Screen.Cursor := crHourGlass;

	try
    	spObtenerTraspasos.Close;
    	with spObtenerTraspasos do begin
        	Parameters.Refresh;
            Parameters.ParamByName('@MensajeError').Value := '';
          //  if vcbConvenios.Text <> '' then Parameters.ParamByName('@CodigoConvenio').Value := FCodigoConvenio        //SS_1120_MVI_20130820
            if vcbConvenios.Value > 0 then Parameters.ParamByName('@CodigoConvenio').Value := FCodigoConvenio           //SS_1120_MVI_20130820
            else Parameters.ParamByName('@CodigoConvenio').Value := NULL;

            if peRut.Text <> '' then Parameters.ParamByName('@RutCliente').Value := peRut.Text
            else Parameters.ParamByName('@RutCliente').Value := NULL;

            if deFechaDesde.Date > 0 then Parameters.ParamByName('@FechaDesde').Value := deFechaDesde.Date
            else Parameters.ParamByName('@FechaDesde').Value := NULL;

            if deFechaHasta.Date > 0 then Parameters.ParamByName('@FechaHasta').Value := deFechaHasta.Date 
            else Parameters.ParamByName('@FechaHasta').Value := NULL;

        	if neNumDesde.ValueInt <> 0 then Parameters.ParamByName('@NumeroTraspasoDesde').Value := neNumDesde.ValueInt
            else Parameters.ParamByName('@NumeroTraspasoDesde').Value := NULL;

            if neNumHasta.ValueInt <> 0 then Parameters.ParamByName('@NumeroTraspasoHasta').Value := neNumHasta.ValueInt
            else Parameters.ParamByName('@NumeroTraspasoHasta').Value := NULL;

            if neMontoDesde.ValueInt <> 0 then Parameters.ParamByName('@MontoDesde').Value := neMontoDesde.ValueInt
            else Parameters.ParamByName('@MontoDesde').Value := NULL;

            if neMontoHasta.ValueInt <> 0 then Parameters.ParamByName('@MontoHasta').Value := neMontoHasta.ValueInt
            else Parameters.ParamByName('@MontoHasta').Value := NULL;

            spObtenerTraspasos.Open;
            if spObtenerTraspasos.IsEmpty then MsgBox(MSG_NOENC, Caption, MB_ICONEXCLAMATION)
            else OrdenarPorColumna(dbListadoTraspasos.Columns[0]);
    	end;
    except on e:exception do begin
        	MsgBoxErr(MSG_ERROR, e.message, Caption, MB_ICONERROR);
    	end;
    end;

    Screen.Cursor := crDefault;
end;

{ *****************************************************
    			    btnBuscarClick

Author: mbecerra
Date: 28-Julio-2009
Description:	Valida los filtros ingresados y luego invoca a la b�squeda de registros.
*******************************************************}
procedure TBuscarTraspasosForm.btnBuscarClick(Sender: TObject);
begin
	if Validaciones() then BuscarTraspasos();

end;

{ *****************************************************
    			    peRutButtonClick

Author: mbecerra
Date: 28-Julio-2009
Description:	Muestra la ventana que permite la b�squeda de clientes.

*******************************************************}
procedure TBuscarTraspasosForm.peRutButtonClick(Sender: TObject);
var
	f : TFormBuscaClientes;
    TeclaEnter : Char;
begin
	Application.CreateForm(TFormBuscaClientes, f);
    if f.Inicializa(FUltimaBusqueda) then begin
        f.ShowModal;
        if f.ModalResult = IDOK then begin
            FUltimaBusqueda := f.UltimaBusqueda;
            peRut.Text := f.Persona.NumeroDocumento;
            TeclaEnter := #13;
            ValidaTeclaBusca(Sender, TeclaEnter);
        end;
    end;

    f.Free;

end;

{ *****************************************************
    			    ValidaTeclaBusca

Author: mbecerra
Date: 28-Julio-2009
Description:	Valida que la tecla presionada pertenezca a un rut, y adem�s,
				si se presiona la tecla ENTER, se buscan los convenios asociados a ese Rut.

*******************************************************}
procedure TBuscarTraspasosForm.ValidaTeclaBusca(Sender: TObject; var Key: Char);
begin
	if Key in ['0'..'9', 'K','k', Char(VK_BACK)] then vcbConvenios.Clear
    else begin
    	if Key = #13 then begin
        	BuscarConvenios(peRut.Text, vcbConvenios);
            if vcbConvenios.Items.Count > 0 then begin
            	vcbConvenios.SetFocus;
                vcbConvenios.Perform(CB_SHOWDROPDOWN, 1, 0);
            end;
        end;
    	Key := #0;
    end;

end;

{ *****************************************************
    			    vcbConveniosChange

Author: mbecerra
Date: 28-Julio-2009
Description:	Asigna el c�digo del convenio, si el usuario lo selecciona.
            	De lo contrario siempre asigna cero a la variable que indica que hay un convenio seleccionado
*******************************************************}
procedure TBuscarTraspasosForm.vcbConveniosChange(Sender: TObject);
begin
	if vcbConvenios.ItemIndex >= 0 then FCodigoConvenio := vcbConvenios.Value
    else FCodigoConvenio := 0;

end;

//BEGIN:SS_1120_MVI_20130820 -------------------------------------------------
{-----------------------------------------------------------------------------
  Procedure: TBuscarTraspasosForm.vcbConveniosDrawItem
  Date:      20-August-2013
  Description: Verifica si existe alg�n convenio de baja para llamar a la funci�n
                que colorea de rojo el item.
-----------------------------------------------------------------------------}
procedure TBuscarTraspasosForm.vcbConveniosDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  if Pos( TXT_CONVENIO_DE_BAJA, vcbConvenios.Items[Index].Caption ) > 0 then  begin

     ItemComboBoxColor(Control, Index, Rect, State, clred, true);

  end
  else begin

     ItemComboBoxColor(Control, Index, Rect, State);

  end;

end;
//END:SS_1120_MVI_20130820 ----------------------------------------------------

{ *****************************************************
    			    FormClose

Author: mbecerra
Date: 23-Octubre-2008
Description:	Cierra el formulario MDI
*******************************************************}
procedure TBuscarTraspasosForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree;
end;

end.

