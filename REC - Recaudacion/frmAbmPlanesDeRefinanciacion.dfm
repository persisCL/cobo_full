object AbmPlanesDeRefinanciacionForm: TAbmPlanesDeRefinanciacionForm
  Left = 106
  Top = 61
  ClientHeight = 567
  ClientWidth = 725
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GrupoDatos: TPanel
    Left = 0
    Top = 376
    Width = 725
    Height = 152
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 0
    object Label8: TLabel
      Left = 11
      Top = 11
      Width = 37
      Height = 13
      Alignment = taRightJustify
      Caption = 'C'#243'digo:'
      FocusControl = txtCodigo
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblNombre: TLabel
      Left = 11
      Top = 49
      Width = 48
      Height = 13
      Caption = 'Nombre:'
      FocusControl = txtNombre
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblMontoMinimoDeDeuda: TLabel
      Left = 11
      Top = 85
      Width = 84
      Height = 13
      Alignment = taRightJustify
      Caption = 'Monto m'#237'nimo:'
      FocusControl = txtMontoMinimoDeDeuda
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblMontoMaximoDeDeuda: TLabel
      Left = 237
      Top = 85
      Width = 88
      Height = 13
      Alignment = taRightJustify
      Caption = 'Monto m'#225'ximo:'
      FocusControl = txtMontoMaximoDeDeuda
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblCantidadDeCuotas: TLabel
      Left = 11
      Top = 125
      Width = 111
      Height = 13
      Alignment = taRightJustify
      Caption = 'Cantidad de cuotas:'
      FocusControl = txtCantidadDeCuotas
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblPersoneria: TLabel
      Left = 258
      Top = 49
      Width = 64
      Height = 13
      Alignment = taRightJustify
      Caption = 'Personeria:'
      FocusControl = txtPersoneria
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblPorcentajeCuotaPie: TLabel
      Left = 202
      Top = 125
      Width = 123
      Height = 13
      Caption = 'Porcentaje cuota pie:'
      FocusControl = txtPorcentajeCuotaPie
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 381
      Top = 125
      Width = 10
      Height = 13
      Caption = '%'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object txtCodigo: TNumericEdit
      Left = 64
      Top = 8
      Width = 50
      Height = 21
      Color = clBtnFace
      Enabled = False
      ReadOnly = True
      TabOrder = 0
    end
    object txtNombre: TEdit
      Left = 65
      Top = 46
      Width = 150
      Height = 21
      Color = 16444382
      MaxLength = 200
      TabOrder = 1
    end
    object txtMontoMinimoDeDeuda: TNumericEdit
      Left = 101
      Top = 82
      Width = 50
      Height = 21
      Color = 16444382
      TabOrder = 3
      Decimals = 2
      BlankWhenZero = False
    end
    object txtMontoMaximoDeDeuda: TNumericEdit
      Left = 331
      Top = 82
      Width = 50
      Height = 21
      Color = 16444382
      TabOrder = 4
      Decimals = 2
      BlankWhenZero = False
    end
    object txtCantidadDeCuotas: TNumericEdit
      Left = 129
      Top = 122
      Width = 50
      Height = 21
      Color = 16444382
      TabOrder = 5
      BlankWhenZero = False
    end
    object txtPersoneria: TVariantComboBox
      Left = 331
      Top = 46
      Width = 200
      Height = 21
      Style = vcsDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 2
      Items = <>
    end
    object txtPorcentajeCuotaPie: TNumericEdit
      Left = 331
      Top = 122
      Width = 50
      Height = 21
      Color = 16444382
      TabOrder = 6
      Decimals = 2
      BlankWhenZero = False
    end
  end
  object PAbajo: TPanel
    Left = 0
    Top = 528
    Width = 725
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Notebook: TNotebook
      Left = 528
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TButton
          Left = 110
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object AbmToolbar: TAbmToolbar
    Left = 0
    Top = 0
    Width = 725
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir, btBuscar]
    OnClose = AbmToolbarClose
  end
  object DBListDatos: TAbmList
    Left = 0
    Top = 33
    Width = 725
    Height = 343
    TabStop = True
    TabOrder = 3
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -12
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'52'#0'C'#243'digo'
      #0'207'#0'Nombre')
    HScrollBar = True
    RefreshTime = 100
    Table = spListar
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = DBListDatosClick
    OnProcess = DBListDatosProcess
    OnDrawItem = DBListDatosDrawItem
    OnRefresh = DBListDatosRefresh
    OnInsert = DBListDatosInsert
    OnDelete = DBListDatosDelete
    OnEdit = DBListDatosEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar
  end
  object spAlmacenar: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'PlanesDeRefinanciacion_Almacenar'
    Parameters = <>
    Left = 412
    Top = 112
  end
  object spEliminar: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'PlanesDeRefinanciacion_Eliminar'
    Parameters = <>
    Left = 448
    Top = 112
  end
  object spListar: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'PlanesDeRefinanciacion_Listar'
    Parameters = <>
    Left = 376
    Top = 112
  end
end
