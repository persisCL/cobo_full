object FrmInterfazSalidaJuzgado: TFrmInterfazSalidaJuzgado
  Left = 294
  Top = 287
  BorderStyle = bsDialog
  Caption = 'Interfaz de Salida para el Juzgado Local'
  ClientHeight = 91
  ClientWidth = 455
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  DesignSize = (
    455
    91)
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 455
    Height = 54
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelOuter = bvLowered
    TabOrder = 0
    DesignSize = (
      455
      54)
    object Label1: TLabel
      Left = 13
      Top = 8
      Width = 231
      Height = 13
      Caption = 'Lanzar interfaz con el Juzgado de Policia Local ?'
    end
    object ProgressBar: TProgressBar
      Left = 13
      Top = 27
      Width = 432
      Height = 16
      Anchors = [akLeft, akBottom]
      Min = 0
      Max = 100
      TabOrder = 0
    end
  end
  object btnAceptar: TDPSButton
    Left = 294
    Top = 62
    Anchors = [akRight, akBottom]
    Caption = '&Aceptar'
    Default = True
    ModalResult = 1
    TabOrder = 1
    OnClick = btnAceptarClick
  end
  object btnCancelar: TDPSButton
    Left = 374
    Top = 62
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 2
    OnClick = btnCancelarClick
  end
end
