object frm_PagoNotasCredito: Tfrm_PagoNotasCredito
  Left = 239
  Top = 169
  BorderStyle = bsDialog
  Caption = 'Pago de Nota de Cr'#233'dito'
  ClientHeight = 322
  ClientWidth = 660
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  DesignSize = (
    660
    322)
  PixelsPerInch = 96
  TextHeight = 13
  object gbPorCliente: TGroupBox
    Left = 3
    Top = 2
    Width = 649
    Height = 101
    Anchors = [akLeft, akTop, akRight]
    Caption = ' B'#250'squeda Comprobante '
    Color = clBtnFace
    ParentColor = False
    TabOrder = 0
    object lb_CodigoCliente: TLabel
      Left = 12
      Top = 56
      Width = 61
      Height = 13
      Caption = 'RUT Cliente:'
    end
    object Label5: TLabel
      Left = 240
      Top = 56
      Width = 103
      Height = 13
      Caption = 'N'#250'mero de Convenio:'
    end
    object Label1: TLabel
      Left = 469
      Top = 55
      Width = 85
      Height = 13
      Caption = 'Notas de Cr'#233'dito :'
    end
    object Label4: TLabel
      Left = 241
      Top = 16
      Width = 74
      Height = 13
      Caption = 'Nota de Cr'#233'dito'
    end
    object Label9: TLabel
      Left = 12
      Top = 16
      Width = 97
      Height = 13
      Caption = 'Tipos Comprobantes'
    end
    object peNumeroDocumento: TPickEdit
      Left = 12
      Top = 70
      Width = 203
      Height = 21
      CharCase = ecUpperCase
      Enabled = True
      TabOrder = 1
      OnChange = peNumeroDocumentoChange
      EditorStyle = bteTextEdit
      OnButtonClick = peNumeroDocumentoButtonClick
    end
    object cbConvenios: TVariantComboBox
      Left = 240
      Top = 70
      Width = 209
      Height = 21
      Style = vcsDropDownList
      DroppedWidth = 0
      ItemHeight = 13
      TabOrder = 2
      OnChange = cbConveniosChange
      Items = <>
    end
    object cbComprobantes: TComboBox
      Left = 470
      Top = 70
      Width = 170
      Height = 21
      Style = csDropDownList
      ItemHeight = 0
      TabOrder = 3
      OnChange = cbComprobantesChange
    end
    object edNumeroComprobante: TNumericEdit
      Left = 241
      Top = 31
      Width = 143
      Height = 21
      TabOrder = 0
      OnChange = edNumeroComprobanteChange
    end
    object cbTiposComprobantes: TVariantComboBox
      Left = 12
      Top = 31
      Width = 203
      Height = 21
      Style = vcsDropDownList
      DroppedWidth = 0
      ItemHeight = 13
      TabOrder = 4
      OnChange = cbTiposComprobantesChange
      Items = <>
    end
  end
  object GroupBox1: TGroupBox
    Left = 4
    Top = 105
    Width = 650
    Height = 60
    Anchors = [akLeft, akTop, akRight]
    Caption = ' Datos del Cliente '
    TabOrder = 3
    DesignSize = (
      650
      60)
    object Label6: TLabel
      Left = 17
      Top = 18
      Width = 48
      Height = 13
      Anchors = [akLeft, akBottom]
      Caption = 'Nombre:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblNombre: TLabel
      Left = 91
      Top = 18
      Width = 72
      Height = 13
      Anchors = [akLeft, akTop, akRight]
      Caption = 'Apellido Cliente'
    end
    object lblDomicilio: TLabel
      Left = 90
      Top = 39
      Width = 74
      Height = 13
      Anchors = [akLeft, akTop, akRight]
      Caption = 'DomicilioCliente'
    end
    object Label10: TLabel
      Left = 16
      Top = 39
      Width = 56
      Height = 13
      Caption = 'Domicilio:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object gbDatosComprobante: TGroupBox
    Left = 4
    Top = 166
    Width = 650
    Height = 110
    Anchors = [akLeft, akTop, akRight]
    Caption = ' Datos de la Nota de Cr'#233'dito '
    TabOrder = 4
    object Label2: TLabel
      Left = 10
      Top = 50
      Width = 105
      Height = 13
      Caption = 'Fecha de Emisi'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblFecha: TLabel
      Left = 147
      Top = 50
      Width = 106
      Height = 13
      AutoSize = False
      Caption = 'Fecha'
    end
    object lblFechaVencimiento: TLabel
      Left = 147
      Top = 70
      Width = 148
      Height = 13
      AutoSize = False
      Caption = 'Fecha Vencimiento'
    end
    object Label7: TLabel
      Left = 10
      Top = 70
      Width = 131
      Height = 13
      Caption = 'Fecha de Vencimiento:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblTotalAPagar: TLabel
      Left = 502
      Top = 50
      Width = 77
      Height = 13
      AutoSize = False
      Caption = 'Total a Pagar'
    end
    object Label12: TLabel
      Left = 404
      Top = 50
      Width = 95
      Height = 13
      Caption = 'Importe a Pagar:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblPagado: TLabel
      Left = 502
      Top = 70
      Width = 47
      Height = 13
      Caption = 'lblPagado'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label11: TLabel
      Left = 405
      Top = 70
      Width = 48
      Height = 13
      Caption = 'Pagado:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 11
      Top = 24
      Width = 91
      Height = 13
      Caption = 'Fecha de Pago:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 10
      Top = 89
      Width = 75
      Height = 13
      Caption = 'Autorizaci'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblPagoAutorizado: TLabel
      Left = 147
      Top = 89
      Width = 94
      Height = 13
      Caption = 'Pago Autorizado'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object deFechaPago: TDateEdit
      Left = 109
      Top = 20
      Width = 110
      Height = 21
      AutoSelect = False
      TabOrder = 0
      Date = -693594.000000000000000000
    end
  end
  object btnPago: TButton
    Left = 5
    Top = 289
    Width = 196
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Pagar (F9)'
    Default = True
    TabOrder = 1
    OnClick = btnPagoClick
  end
  object btnSalir: TButton
    Left = 579
    Top = 289
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Salir'
    TabOrder = 2
    OnClick = btnSalirClick
  end
  object ObtenerNotasCreditoImpagas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerNotasCreditoImpagas;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 292
    Top = 254
  end
  object CerrarPagoComprobante: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CerrarPagoComprobante'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@FechaHora'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 259
    Top = 255
  end
  object sp_RegistrarUnDetallePagoComprobante: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorLocation = clUseServer
    LockType = ltBatchOptimistic
    CommandTimeout = 60
    ProcedureName = 'RegistrarDetallePagoComprobante;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@IDDetallePagoComprobante'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoMedioPago'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoFormaPago'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@PAC_CodigoBanco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PAC_CodigoTipoCuentaBancaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@PAC_NroCuentaBancaria'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@PAC_Sucursal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@PAC_Titular'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@PAT_CodigoTipoTarjetaCredito'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@PAT_NumeroTarjetaCredito'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@PAT_FechaVencimiento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 5
        Value = Null
      end
      item
        Name = '@PAT_CodigoEmisorTarjetaCredito'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@PAT_Titular'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@ChequeCodigoBanco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ChequeNumero'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@ChequeTitular'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@ChequeFecha'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@ChequeCodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@ChequeNumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@ChequeMonto'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 10
        Value = Null
      end
      item
        Name = '@Cupon'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Autorizacion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@Cuotas'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@NumeroPOS'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Web_NumeroFinalTarjetaCredito'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@Web_TipoVenta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end>
    Left = 258
    Top = 286
  end
  object sp_RegistrarUnPagoComprobante: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorLocation = clUseServer
    LockType = ltBatchOptimistic
    CommandTimeout = 60
    ProcedureName = 'RegistrarPagoComprobante;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroPago'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaHora'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@Importe'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 10
        Value = Null
      end
      item
        Name = '@EstadoPago'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@NumeroRecibo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@IDDetallePagoComprobante'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConceptoPago'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@DescripcionPago'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end>
    Left = 293
    Top = 286
  end
end
