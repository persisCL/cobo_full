unit frmRefinanciacionABMRepLegalCN;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,

  PeaPRocsCN, SysUtilsCN, PeaTypes, DMConnection, PeaProcs, BuscaClientes, Util,

  Dialogs, ListBoxEx, DBListEx, DmiCtrls, StdCtrls, DB, DBClient, Provider, ADODB, ExtCtrls, VariantComboBox;

type
  TRefinanciacionABMRepLegalCNForm = class(TForm)
    pnlAcciones: TPanel;
    btnEditar: TButton;
    btnAgregar: TButton;
    spObtenerRefinanciacionRepresentantesCN: TADOStoredProc;
    dspObtenerRefinanciacionRepresentantesCN: TDataSetProvider;
    cdsRepresentantesCN: TClientDataSet;
    cdsRepresentantesCNCodigoRepresentante: TAutoIncField;
    cdsRepresentantesCNApellido: TStringField;
    cdsRepresentantesCNApellidoMaterno: TStringField;
    cdsRepresentantesCNNombre: TStringField;
    cdsRepresentantesCNCodigoDocumento: TStringField;
    cdsRepresentantesCNNumeroDocumento: TStringField;
    cdsRepresentantesCNCodigoTipoMedioPago: TSmallintField;
    cdsRepresentantesCNTipoMedioPagoDesc: TStringField;
    dsRepresentantesCN: TDataSource;
    pnlSalir: TPanel;
    btnSalir: TButton;
    pnlEntradaDatos: TPanel;
    Panel3: TPanel;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    edtNombre: TEdit;
    edtApellido: TEdit;
    edtApellidoMaterno: TEdit;
    peRut: TPickEdit;
    DBListEx1: TDBListEx;
    btnEliminar: TButton;
    vcbTiposRefinanciacion: TVariantComboBox;
    btnAceptar: TButton;
    btnCancelar: TButton;
    Label5: TLabel;
    spObtenerRefinanciacionRepresentanteCN: TADOStoredProc;
    spActualizarRefinanciacionRepresentantesCN: TADOStoredProc;
    spEliminarRefinanciacionRepresentantesCN: TADOStoredProc;
    GroupBox1: TGroupBox;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    edtEmpresaNombre: TEdit;
    edtEmpresaDireccion: TEdit;
    vcbEmpresaRegion: TVariantComboBox;
    vcbEmpresaComuna: TVariantComboBox;
    cdsRepresentantesCNEmpresa: TStringField;
    cdsRepresentantesCNDireccion: TStringField;
    cdsRepresentantesCNCodigoPais: TStringField;
    cdsRepresentantesCNCodigoRegion: TStringField;
    cdsRepresentantesCNCodigoComuna: TStringField;
    procedure peRutKeyPress(Sender: TObject; var Key: Char);
    procedure peRutButtonClick(Sender: TObject);
    procedure peRutChange(Sender: TObject);
    procedure btnAgregarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
    procedure btnEliminarClick(Sender: TObject);
    procedure vcbEmpresaRegionChange(Sender: TObject);
  private
    { Private declarations }
    FUltimaBusqueda: TBusquedaCliente;
    procedure LimpiaCampos;
    procedure HabilitaEntradaDatos(Editar: Boolean);
    procedure OcultarEntradaDatos;
    procedure ActualizarBotones;
  public
    { Public declarations }
    function Inicializar: Boolean;
  end;

var
  RefinanciacionABMRepLegalCNForm: TRefinanciacionABMRepLegalCNForm;

implementation

{$R *.dfm}

procedure TRefinanciacionABMRepLegalCNForm.btnAceptarClick(Sender: TObject);
    resourcestring
        rsTituloValidaciones    = 'Validaci�n Datos Representante CN';
        rsErrorControl_Rut      = 'El Rut introducido NO es Correcto.';
        rsErrorControl_Nombre   = 'El campo Nombre no puede estar en Blanco.';
        rsErrorControl_Apellido = 'El campo Apellido no puede estar en Blanco.';

        rsRepresentanteAlta     = 'Alta Representante Legal CN';
        rsRepresentanteNoExiste = 'El Representante introducido NO Existe en la Base de Datos. � Desea Darlo de Alta ?';

        rsErrorActualizacionTitulo = 'Error Alta / Actualizaci�n';
        rsErrorActualizacion       = 'Se produjo un Error al dar de Alta / Actualizar los datos del Representante.' + CRLF + CRLF + 'Error: %s.';

    function ValidarCondiciones: boolean;
        var
            vControles  : Array [1..3] of TControl;
            vCondiciones: Array [1..3] of Boolean;
            vMensajes   : Array [1..3] of String;
    begin

        vControles[1]   := peRut;
        vControles[2]   := edtNombre;
        vControles[3]   := edtApellido;

        vCondiciones[1] := ValidarRUT(DMConnections.BaseCAC, Trim(peRut.Text));
        vCondiciones[2] := (Trim(edtNombre.Text) <> '');
        vCondiciones[3] := (Trim(edtApellido.Text) <> '');

        vMensajes[1]    := rsErrorControl_Rut;
        vMensajes[2]    := rsErrorControl_Nombre;
        vMensajes[3]    := rsErrorControl_Apellido;

        Result := ValidateControlsCN(vControles, vCondiciones, rsTituloValidaciones, vMensajes, Self);
    end;
begin
    if ValidarCondiciones then try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        with spObtenerRefinanciacionRepresentanteCN do begin
            if Active then Close;
            Parameters.ParamByName('@CodigoDocumento').Value := 'RUT';
            Parameters.ParamByName('@NumeroDocumento').Value := Trim(peRut.Text);
            Open;
        end;
        try
            if spObtenerRefinanciacionRepresentanteCN.RecordCount = 0 then begin
                if ShowMsgBoxCN(rsRepresentanteAlta, rsRepresentanteNoExiste, MB_ICONQUESTION, Self) = mrOk then begin
                    Screen.Cursor := crHourGlass;
                    Application.ProcessMessages;
                    
                    with spActualizarRefinanciacionRepresentantesCN do begin
                        Parameters.ParamByName('@CodigoRepresentante').Value := Null;
                        Parameters.ParamByName('@Apellido').Value            := Trim(edtApellido.Text);
                        Parameters.ParamByName('@ApellidoMaterno').Value     := iif(Trim(edtApellidoMaterno.Text) = '', Null, Trim(edtApellidoMaterno.Text));
                        Parameters.ParamByName('@Nombre').Value              := Trim(edtNombre.Text);
                        Parameters.ParamByName('@CodigoDocumento').Value     := 'RUT';
                        Parameters.ParamByName('@NumeroDocumento').Value     := Trim(peRut.Text);
                        Parameters.ParamByName('@CodigoTipoMedioPago').Value := vcbTiposRefinanciacion.Value;
                        Parameters.ParamByName('@Empresa').Value             := iif(Trim(edtEmpresaNombre.Text) = EmptyStr, Null, Trim(edtEmpresaNombre.Text));
                        Parameters.ParamByName('@Direccion').Value           := iif(Trim(edtEmpresaDireccion.Text) = EmptyStr, Null, Trim(edtEmpresaDireccion.Text));
                        Parameters.ParamByName('@CodigoPais').Value          := iif(vcbEmpresaRegion.ItemIndex > 0, PAIS_CHILE, Null);
                        Parameters.ParamByName('@CodigoRegion').Value        := iif(vcbEmpresaRegion.ItemIndex > 0, vcbEmpresaRegion.Value, Null);
                        Parameters.ParamByName('@CodigoComuna').Value        := iif(vcbEmpresaComuna.ItemIndex > 0, vcbEmpresaComuna.Value, Null);
                        Parameters.ParamByName('@Usuario').Value             := UsuarioSistema;
                        ExecProc;
                    end;
                end;
            end
            else begin
                with spObtenerRefinanciacionRepresentanteCN do begin
                    if (Trim(FieldByName('Nombre').AsString) <> Trim(edtNombre.Text)) or
                        (Trim(FieldByName('Apellido').AsString) <> Trim(edtApellido.Text)) or
                        (Trim(FieldByName('ApellidoMaterno').AsString) <> Trim(edtApellidoMaterno.Text)) or
                        (FieldByName('CodigoTipoMedioPago').AsInteger <> vcbTiposRefinanciacion.Value) or
                        (FieldByName('Empresa').AsString <> Trim(edtEmpresaNombre.Text)) or
                        (FieldByName('Direccion').AsString <> Trim(edtEmpresaDireccion.Text)) or
                        (FieldByName('CodigoRegion').AsString <> iif(vcbEmpresaRegion.ItemIndex > 0, vcbEmpresaRegion.Value, EmptyStr)) or
                        (FieldByName('CodigoComuna').AsString <> iif(vcbEmpresaComuna.ItemIndex > 0, vcbEmpresaComuna.Value, EmptyStr)) then begin
                        with spActualizarRefinanciacionRepresentantesCN do begin
                            Parameters.ParamByName('@CodigoRepresentante').Value := spObtenerRefinanciacionRepresentanteCN.FieldByName('CodigoRepresentante').AsInteger;
                            Parameters.ParamByName('@Apellido').Value            := Trim(edtApellido.Text);
                            Parameters.ParamByName('@ApellidoMaterno').Value     := iif(Trim(edtApellidoMaterno.Text) = '', Null, Trim(edtApellidoMaterno.Text));
                            Parameters.ParamByName('@Nombre').Value              := Trim(edtNombre.Text);
                            Parameters.ParamByName('@CodigoDocumento').Value     := 'RUT';
                            Parameters.ParamByName('@NumeroDocumento').Value     := Trim(peRut.Text);
                            Parameters.ParamByName('@CodigoTipoMedioPago').Value := vcbTiposRefinanciacion.Value;
                            Parameters.ParamByName('@Empresa').Value             := iif(Trim(edtEmpresaNombre.Text) = EmptyStr, Null, Trim(edtEmpresaNombre.Text));
                            Parameters.ParamByName('@Direccion').Value           := iif(Trim(edtEmpresaDireccion.Text) = EmptyStr, Null, Trim(edtEmpresaDireccion.Text));
                            Parameters.ParamByName('@CodigoPais').Value          := iif(vcbEmpresaRegion.ItemIndex > 0, PAIS_CHILE, Null);
                            Parameters.ParamByName('@CodigoRegion').Value        := iif(vcbEmpresaRegion.ItemIndex > 0, vcbEmpresaRegion.Value, Null);
                            Parameters.ParamByName('@CodigoComuna').Value        := iif(vcbEmpresaComuna.ItemIndex > 0, vcbEmpresaComuna.Value, Null);
                            Parameters.ParamByName('@Usuario').Value             := UsuarioSistema;
                            ExecProc;
                        end;
                    end;
                end;
            end;

            OcultarEntradaDatos;
            cdsRepresentantesCN.Refresh;
            ActualizarBotones;
        except
            on e:Exception do begin
                ShowMsgBoxCN(rsErrorActualizacionTitulo, Format(rsErrorActualizacion, [e.Message]), MB_ICONERROR, Self);
            end;
        end;
    finally
        Screen.Cursor := crDefault;
    end;
end;

procedure TRefinanciacionABMRepLegalCNForm.btnAgregarClick(Sender: TObject);
begin
    HabilitaEntradaDatos(False);
end;

procedure TRefinanciacionABMRepLegalCNForm.btnCancelarClick(Sender: TObject);
begin
    OcultarEntradaDatos;
end;

procedure TRefinanciacionABMRepLegalCNForm.btnEditarClick(Sender: TObject);
begin
    HabilitaEntradaDatos(True);
end;

procedure TRefinanciacionABMRepLegalCNForm.btnEliminarClick(Sender: TObject);
    resourcestring
        rs_Titulo  = 'Eliminar Representante';
        rs_mensaje = '� Esta seguro de Eliminar el Representante Legal seleccionado ?';
begin
    if ShowMsgBoxCN(rs_Titulo, rs_mensaje, MB_ICONQUESTION, Self) = mrOk then try
        try
            Screen.Cursor := crHourGlass;
            Application.ProcessMessages;

            with spEliminarRefinanciacionRepresentantesCN do begin
                if Active then begin
                    Close;
                end;
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoDocumento').Value := cdsRepresentantesCNCodigoDocumento.AsString;
                Parameters.ParamByName('@NumeroDocumento').Value := cdsRepresentantesCNNumeroDocumento.AsString;

                ExecProc;
            end;

            cdsRepresentantesCN.Refresh;
            ActualizarBotones;
        except
            on e:Exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
        Screen.Cursor := crDefault;
    end;
end;

procedure TRefinanciacionABMRepLegalCNForm.btnSalirClick(Sender: TObject);
begin
    Close;
end;

function TRefinanciacionABMRepLegalCNForm.Inicializar: Boolean;
begin
    Result := False;
    try
        cdsRepresentantesCN.Open;
        CargarCanalesDePago(DMConnections.BaseCAC, vcbTiposRefinanciacion, 0, 1);
        CargarRegiones(DMConnections.BaseCAC, vcbEmpresaRegion, PAIS_CHILE, EmptyStr, True);
        ActualizarBotones;
        
        Result := True;
    Except
        on e:Exception do begin
            ShowMsgBoxCN(e, Self);
        end;
    end;
end;

procedure TRefinanciacionABMRepLegalCNForm.peRutButtonClick(Sender: TObject);
    var
        BuscaClientes: TFormBuscaClientes;
begin
    try
        peRut.OnChange := Nil;
        BuscaClientes := TFormBuscaClientes.Create(Self);
        if BuscaClientes.Inicializa(FUltimaBusqueda)then begin
            if BuscaClientes.ShowModal = mrOk then begin
                FUltimaBusqueda         := BuscaClientes.UltimaBusqueda;
                peRut.Text              := BuscaClientes.Persona.NumeroDocumento;
                edtNombre.Text          := BuscaClientes.Persona.Nombre;
                edtApellido.Text        := BuscaClientes.Persona.Apellido;
                edtApellidoMaterno.Text := BuscaClientes.Persona.ApellidoMaterno;
                btnAceptar.Enabled      := True;
            end;
        end;
    finally
        if Assigned(BuscaClientes) then FreeAndNil(BuscaClientes);
        peRut.OnChange := peRutChange;
    end;
end;

procedure TRefinanciacionABMRepLegalCNForm.peRutChange(Sender: TObject);
begin
    if ActiveControl = Sender then LimpiaCampos;
end;

procedure TRefinanciacionABMRepLegalCNForm.peRutKeyPress(Sender: TObject; var Key: Char);
    var
        FPersona: TDatosPersonales;
    resourcestring
        rs_BUSQUEDA_TITULO = 'B�squeda Persona';
        rs_BUSQUEDA_NO_ENCONTRADA = 'El Rut introducido NO es Correcto.';
begin
    if key = #13 then begin
        FPersona := ObtenerDatosPersonales(DMConnections.BaseCAC, 'RUT', peRut.Text);

        if FPersona.CodigoPersona <> -1 then begin
            edtNombre.Text          := FPersona.Nombre;
            edtApellido.Text        := FPersona.Apellido;
            edtApellidoMaterno.Text := FPersona.ApellidoMaterno;
            btnAceptar.Enabled      := True;
        end
        else begin
            if ValidarRUT(DMConnections.BaseCAC, peRut.Text) then begin
                btnAceptar.Enabled         := True;
                edtNombre.Enabled          := True;
                edtApellido.Enabled        := True;
                edtApellidoMaterno.Enabled := True;
                edtNombre.SetFocus;
            end
            else begin
                btnAceptar.Enabled         := False;
                edtNombre.Enabled          := False;
                edtApellido.Enabled        := False;
                edtApellidoMaterno.Enabled := False;
                ShowMsgBoxCN(rs_BUSQUEDA_TITULO, rs_BUSQUEDA_NO_ENCONTRADA, MB_ICONWARNING, Self);
            end;
        end;
    end;
end;

procedure TRefinanciacionABMRepLegalCNForm.LimpiaCampos;
begin
    edtNombre.Text             := EmptyStr;
    edtApellido.Text           := EmptyStr;
    edtApellidoMaterno.Text    := EmptyStr;

    edtEmpresaNombre.Text      := EmptyStr;
    edtEmpresaDireccion.Text   := EmptyStr;
    vcbEmpresaRegion.ItemIndex := 0;
    vcbEmpresaComuna.Items.Clear;
    vcbEmpresaComuna.Enabled   := False;
end;

procedure TRefinanciacionABMRepLegalCNForm.HabilitaEntradaDatos(Editar: Boolean);
begin
    LimpiaCampos;

    peRut.Enabled              := Not Editar;
    edtNombre.Enabled          := Editar;
    edtApellido.Enabled        := Editar;
    edtApellidoMaterno.Enabled := Editar;

    DBListEx1.Enabled   := False;
    btnAgregar.Enabled  := False;
    btnEditar.Enabled   := False;
    btnEliminar.Enabled := False;
    btnSalir.Enabled    := False;

    pnlEntradaDatos.Visible := True;

    if Editar then begin
        peRut.Text              := cdsRepresentantesCNNumeroDocumento.AsString;
        edtNombre.Text          := cdsRepresentantesCNNombre.AsString;
        edtApellido.Text        := cdsRepresentantesCNApellido.AsString;
        edtApellidoMaterno.Text := cdsRepresentantesCNApellidoMaterno.AsString;

        vcbTiposRefinanciacion.ItemIndex := vcbTiposRefinanciacion.Items.IndexOfValue(cdsRepresentantesCNCodigoTipoMedioPago.AsInteger);

        edtEmpresaNombre.Text      := cdsRepresentantesCNEmpresa.AsString;
        edtEmpresaDireccion.Text   := cdsRepresentantesCNDireccion.AsString;

        if not cdsRepresentantesCNCodigoRegion.IsNull then begin
            vcbEmpresaRegion.ItemIndex := vcbEmpresaRegion.Items.IndexOfValue(cdsRepresentantesCNCodigoRegion.AsString);
        end
        else vcbEmpresaRegion.ItemIndex := 0;

        if not cdsRepresentantesCNCodigoComuna.IsNull then try
            CambiarEstadoCursor(CURSOR_RELOJ);
            CargarComunas(DMConnections.BaseCAC, vcbEmpresaComuna, PAIS_CHILE, vcbEmpresaRegion.Value, cdsRepresentantesCNCodigoComuna.AsString, True);
        finally
            vcbEmpresaComuna.Enabled := True;
            CambiarEstadoCursor(CURSOR_DEFECTO);
        end;

        ActiveControl := edtNombre;
    end
    else begin
        peRut.Text := EmptyStr;
        vcbTiposRefinanciacion.ItemIndex := 0;
        ActiveControl := peRut;
    end;
end;

procedure TRefinanciacionABMRepLegalCNForm.OcultarEntradaDatos;
begin
    pnlEntradaDatos.Visible := False;

    btnAgregar.Enabled := True;
    btnSalir.Enabled   := True;
    DBListEx1.Enabled  := True;
    
    ActualizarBotones;
end;

procedure TRefinanciacionABMRepLegalCNForm.ActualizarBotones;
begin
    btnEditar.Enabled  := cdsRepresentantesCN.RecordCount > 0;
    btnEliminar.Enabled := cdsRepresentantesCN.RecordCount > 0;
end;

procedure TRefinanciacionABMRepLegalCNForm.vcbEmpresaRegionChange(Sender: TObject);
begin
    if vcbEmpresaRegion.Items.Count > 0 then begin
        if vcbEmpresaRegion.Value <> Null then try
            CambiarEstadoCursor(CURSOR_RELOJ);

            CargarComunas(DMConnections.BaseCAC, vcbEmpresaComuna, PAIS_CHILE, vcbEmpresaRegion.Value, '', True);
        finally
            CambiarEstadoCursor(CURSOR_DEFECTO);
        end
        else vcbEmpresaComuna.Items.Clear;
    end
    else vcbEmpresaComuna.Items.Clear;

    vcbEmpresaComuna.Enabled := (vcbEmpresaComuna.Items.Count > 0);
end;

end.
