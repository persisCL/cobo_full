{-----------------------------------------------------------------------------
  Firma       :   SS_1120_MVI_20130820
  Descripcion :   Se crea el procedimiento OnDrawItem para que llame
                  al procedimiento ItemComboBoxColor para pintar los convenios de baja.
----------------------------------------------------------------------------}
unit frmRefinanciacionGestionCuotas;

interface

uses
  // Developer
  frmMuestraMensaje, DMConnection, PeaProcs, PeaTypes, Util, Convenios, UtilDB, frmRefinanciacionCuotasHistorial,
  PeaProcsCN, SysUtilsCN, frmRefinanciacionPactarMora, 
  frmRefinanciacionCuotasPagosEfectivo, frmRefinanciacionEntrarEditarCuotas,
  frmRefinanciacionReporteRecibo, CobranzasResources,
  // Auto
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ListBoxEx, DBListEx, StdCtrls, ExtCtrls, ComCtrls, Validate, DateEdit, DmiCtrls, VariantComboBox, DB, DBClient, Provider,
  ADODB;

type
  TRefinanciacionGestionCuotasForm = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    lblNumRefinanciacion: TLabel;
    Label5: TLabel;
    lblEstado: TLabel;
    Label6: TLabel;
    lblTotalRefinanciado2: TLabel;
    Label8: TLabel;
    Label10: TLabel;
    lblDeudaPagada: TLabel;
    lblDeudaVencida: TLabel;
    vcbTipoRefinanciacion: TVariantComboBox;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    lblNombreCli: TLabel;
    Label7: TLabel;
    lblPersoneria: TLabel;
    lblConcesionaria: TLabel;
    peRut: TPickEdit;
    vcbConvenios: TVariantComboBox;
    GroupBox7: TGroupBox;
    lblDomicilioCliDesc: TLabel;
    lbldomicilioFactDesc: TLabel;
    lblDomicilioFact: TLabel;
    lblDomicilioCli: TLabel;
    dedtFechaRefi: TDateEdit;
    cbNotificacion: TCheckBox;
    GroupBox6: TGroupBox;
    PageControl1: TPageControl;
    tbsCuotas: TTabSheet;
    Panel4: TPanel;
    btnPactarMora: TButton;
    DBListEx3: TDBListEx;
    Panel7: TPanel;
    Label18: TLabel;
    lblCuotasCantidad: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    lblDeudaPagada2: TLabel;
    lblCuotasTotal: TLabel;
    pnlAcciones: TPanel;
    btnCancelar: TButton;
    spObtenerRefinanciacionCuotas: TADOStoredProc;
    dspObtenerRefinanciacionCuotas: TDataSetProvider;
    cdsCuotas: TClientDataSet;
    cdsCuotasCodigoEstadoCuota: TSmallintField;
    cdsCuotasDescripcionEstadoCuota: TStringField;
    cdsCuotasCodigoFormaPago: TSmallintField;
    cdsCuotasDescripcionFormaPago: TStringField;
    cdsCuotasTitularNombre: TStringField;
    cdsCuotasTitularTipoDocumento: TStringField;
    cdsCuotasTitularNumeroDocumento: TStringField;
    cdsCuotasCodigoBanco: TIntegerField;
    cdsCuotasDescripcionBanco: TStringField;
    cdsCuotasDocumentoNumero: TStringField;
    cdsCuotasFechaCuota: TDateTimeField;
    cdsCuotasCodigoTarjeta: TIntegerField;
    cdsCuotasDescripcionTarjeta: TStringField;
    cdsCuotasTarjetaCuotas: TSmallintField;
    cdsCuotasImporte: TLargeintField;
    cdsCuotasCodigoEntradaUsuario: TStringField;
    cdsCuotasEstadoImpago: TBooleanField;
    dsCuotas: TDataSource;
    spObtenerRefinanciacionDatosCabecera: TADOStoredProc;
    spObtenerRefinanciacionDomicilio: TADOStoredProc;
    btnHistorialEstados: TButton;
    cdsCuotasCodigoCuota: TSmallintField;
    btnCobranza: TButton;
    spActualizarRefinanciacionCuotasEstados: TADOStoredProc;
    cdsCuotasSaldo: TLargeintField;
    btnHistorialPagos: TButton;
    spActualizarRefinanciacionPagoAnticipado: TADOStoredProc;
    cdsCuotasCodigoCuotaAntecesora: TSmallintField;
    spActualizarRefinanciacionCuotas: TADOStoredProc;
    spActualizarRefinanciacionCuotasSaldo: TADOStoredProc;
    spActualizarRefinanciacionCuotaPago: TADOStoredProc;
    cdsCuotasCuotaNueva: TBooleanField;
    cdsCuotasCuotaModificada: TBooleanField;
    Label9: TLabel;
    lblDeudaPendiente: TLabel;
    lblProtestada: TLabel;
    cdsCuotasNumeroCuota: TSmallintField;	// SS_1050_PDO_20120605
    procedure FormActivate(Sender: TObject);
    procedure DBListEx3DrawBackground(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var DefaultDraw: Boolean);
    procedure DBListEx3DrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnHistorialEstadosClick(Sender: TObject);
    procedure cdsCuotasAfterScroll(DataSet: TDataSet);
    procedure btnCobranzaClick(Sender: TObject);
    procedure btnPactarMoraClick(Sender: TObject);
    procedure btnHistorialPagosClick(Sender: TObject);
    function ReemplazaPorApostrofe(Cadena: String): String;    
    procedure vcbConveniosDrawItem(Control: TWinControl; Index: Integer;                                                          //SS_1120_MVI_20130820
      Rect: TRect; State: TOwnerDrawState);                                                                                       //SS_1120_MVI_20130820
  private
    { Private declarations }
    FDatosDomicilio: TDatosDomicilio;
    FPersona: TDatosPersonales;
    FCodigoRefinanciacion,
    FCodigoConvenio: Integer;
    FEditable,
    FDomicilioModificado: Boolean;
    FCuotasCantidad: Integer;
    FCuotasTotal,
    FDeudaPagada,
    FComprobantesRefinanciadosTotal,    
    FDeudaVencida: Largeint;
    FAhora: TDate;
    FEdicionDatos: TEdicionDatos;
    FCodigoCanalPago: Integer;
    procedure MostrarDatosCliente;
    procedure MostrarDomicilioCliente;
    procedure MostrarCuotas;
    procedure ActualizarTotales;
    procedure MostrarDatosConvenio;
    Procedure ActualizarBotones;
  public
    { Public declarations }
    function Inicializar(Refinanciacion: Integer): Boolean;
  end;

var
  RefinanciacionGestionCuotasForm: TRefinanciacionGestionCuotasForm;

const
    SQLArmarDomicilioCompleto      = 'SELECT dbo.ArmarDomicilioCompleto(''%s'', %d, %d, ''%s'', ''%s'', ''%s'', ''%s'')';

resourcestring
    RS_TITULO_AVISO_TURNO          = 'Apertura de Turno';
    RS_MENSAJE_AVISO_TURNO         = 'Para realizar la Operaci�n es necesario tener un Turno Abierto.';
    RS_MENSAJE_ERROR_CAMBIO_ESTADO = 'Se Produjo un Error al actualizar el Estado de la Cuota.';

implementation

{$R *.dfm}

procedure TRefinanciacionGestionCuotasForm.btnCancelarClick(Sender: TObject);
begin
    Close;
end;

procedure TRefinanciacionGestionCuotasForm.btnCobranzaClick(Sender: TObject);
    resourcestring
        RS_TITULO_ACCION     = 'Enviar a Cobranza';
        RS_MENSAJE_RESULTADO = 'Se Envi� a Cobranza la Cuota Correctamente.';
    var
        Puntero: TBookmark;
begin
    if ShowMsgBoxCN(RS_TITULO_ACCION, '� Est� seguro de Enviar a Cobranza la Cuota Seleccionada ?', MB_ICONQUESTION, Self) = mrOk then try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        try
            with spActualizarRefinanciacionCuotasEstados do begin
                if Active then Close;
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoRefinanciacion').Value := FCodigoRefinanciacion;
                Parameters.ParamByName('@CodigoCuota').Value          := cdsCuotasCodigoCuota.AsInteger;
                Parameters.ParamByName('@CodigoEstadoCuota').Value    := REFINANCIACION_ESTADO_CUOTA_EN_COBRANZA;
                Parameters.ParamByName('@Usuario').Value              := UsuarioSistema;

                ExecProc;

                if Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
                    raise EErrorExceptionCN.Create(RS_TITULO_ACCION, RS_MENSAJE_ERROR_CAMBIO_ESTADO);
                end
                else try
                    Puntero := cdsCuotas.GetBookmark;
                    cdsCuotas.Close;
                    MostrarCuotas;
                finally
                    if Assigned(Puntero) then begin
                        if cdsCuotas.BookmarkValid(Puntero) then cdsCuotas.GotoBookmark(Puntero);
                        cdsCuotas.FreeBookmark(Puntero);
                    end;
                end;

                ShowMsgBoxCN(RS_TITULO_ACCION, RS_MENSAJE_RESULTADO, MB_ICONINFORMATION, Self);
            end;
        except
            on e:Exception do begin
                ShowMsgBoxCN(e, Self);
            end;
        end;
    finally
        Screen.Cursor := crDefault;
    end;
end;

procedure TRefinanciacionGestionCuotasForm.btnHistorialEstadosClick(Sender: TObject);
begin
    Try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        RefinanciacionCuotasHistorialForm := TRefinanciacionCuotasHistorialForm.Create(Self);
        if RefinanciacionCuotasHistorialForm.Inicializar(FCodigoRefinanciacion, cdsCuotasCodigoCuota.AsInteger) then begin
            RefinanciacionCuotasHistorialForm.ShowModal;
        end;
    Finally
        if Assigned(RefinanciacionCuotasHistorialForm) then FreeAndNil(RefinanciacionCuotasHistorialForm);
        Screen.Cursor := crDefault;
    End;
end;

procedure TRefinanciacionGestionCuotasForm.btnHistorialPagosClick(Sender: TObject);
begin
    Try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        RefinanciacionCuotasPagosEfectivoForm := TRefinanciacionCuotasPagosEfectivoForm.Create(Self);
        if RefinanciacionCuotasPagosEfectivoForm.Inicializar(FCodigoRefinanciacion, cdsCuotasCodigoCuota.AsInteger) then begin
            RefinanciacionCuotasPagosEfectivoForm.ShowModal;
        end;
    Finally
        if Assigned(RefinanciacionCuotasPagosEfectivoForm) then FreeAndNil(RefinanciacionCuotasPagosEfectivoForm);
        Screen.Cursor := crDefault;
    End;
end;

procedure TRefinanciacionGestionCuotasForm.btnPactarMoraClick(Sender: TObject);
    resourcestring
        RS_ERROR_EJECUCION_TITULO  = 'Error de Ejecuci�n';
        RS_ERROR_EJECUCION_MENSAJE = 'Error en la ejecuci�n del procedimiento %s. C�digo de Error: %d.';

        RS_TITULO_ACCION     = 'Pactar Mora';
        RS_ERROR_ACCION      = 'Se Produjo un Error al actualizar la Fecha de la Cuota.';
        RS_MENSAJE_RESULTADO = 'Se Pact� la Mora de la Cuota Correctamente.';
    var
        Puntero: TBookmark;
        FechaSugerida: TDateTime;
        ImporteMoraPactada: Integer;
        ImporteCuotaAnulada: Integer;   // SS_1050_PDO_20120605
        EstadoCuotaActual: Byte;
begin
    Try
        if cdsCuotasFechaCuota.AsDateTime < FAhora then
            FechaSugerida := FAhora
        else FechaSugerida := cdsCuotasFechaCuota.AsDateTime;

        RefinanciacionPactarMoraForm := TRefinanciacionPactarMoraForm.Create(Self);
        if RefinanciacionPactarMoraForm.Inicializar(FAhora, FechaSugerida) then begin
            if RefinanciacionPactarMoraForm.ShowModal = mrok then try
                if ShowMsgBoxCN(RS_TITULO_ACCION, '� Est� seguro de cambiar la Fecha a la Cuota Seleccionada ?', MB_ICONQUESTION, Self) = mrOk then begin
                    Screen.Cursor := crHourGlass;
                    Application.ProcessMessages;

                    QueryExecute(DMConnections.BaseCAC, 'BEGIN TRANSACTION PactarMora');

                    if cdsCuotasImporte.AsLargeInt = cdsCuotasSaldo.AsLargeInt then begin
                        ImporteMoraPactada := cdsCuotasImporte.AsLargeInt
                    end
                    else ImporteMoraPactada := cdsCuotasSaldo.AsLargeInt;

                    with spActualizarRefinanciacionCuotas do begin
                        if Active then Close;
                        Parameters.Refresh;

                        with Parameters do begin
                            ParamByName('@CodigoRefinanciacion').Value   := FCodigoRefinanciacion;
                            ParamByName('@FechaCuota').Value             := RefinanciacionPactarMoraForm.dedtNuevaFecha.Date;
                            ParamByName('@Importe').Value                := ImporteMoraPactada;
                            ParamByName('@Saldo').Value                  := Null;
                            ParamByName('@CodigoEstadoCuota').Value      := REFINANCIACION_ESTADO_CUOTA_MORA_PACTADA;
                            ParamByName('@CodigoFormaPago').Value        := cdsCuotasCodigoFormaPago.AsInteger;
                            ParamByName('@TitularNombre').Value          := iif(cdsCuotasTitularNombre.AsString <> EmptyStr, cdsCuotasTitularNombre.AsString, Null);
                            ParamByName('@TitularTipoDocumento').Value   := iif(cdsCuotasTitularTipoDocumento.AsString <> EmptyStr, cdsCuotasTitularTipoDocumento.AsString, Null);
                            ParamByName('@TitularNumeroDocumento').Value := iif(cdsCuotasTitularNumeroDocumento.AsString <> EmptyStr, cdsCuotasTitularNumeroDocumento.AsString, Null);
                            ParamByName('@DocumentoNumero').Value        := iif(cdsCuotasDocumentoNumero.AsString <> EmptyStr, cdsCuotasDocumentoNumero.AsString, Null);
                            ParamByName('@CodigoBanco').Value            := iif(cdsCuotasCodigoBanco.AsInteger <> 0, cdsCuotasCodigoBanco.AsInteger, Null);
                            ParamByName('@CodigoTarjeta').Value          := iif(cdsCuotasCodigoTarjeta.AsInteger <> 0, cdsCuotasCodigoTarjeta.AsInteger, Null);
                            ParamByName('@TarjetaCuotas').Value          := iif(cdsCuotasTarjetaCuotas.AsInteger <> 0, cdsCuotasTarjetaCuotas.AsInteger, Null);
                            ParamByName('@BorrarExistentes').Value       := 0;
                            ParamByName('@Usuario').Value                := UsuarioSistema;
                            ParamByName('@CodigoCuota').Value            := Null;
                            ParamByName('@CodigoCuotaAntecesora').Value  := cdsCuotasCodigoCuota.AsInteger;
                        end;

                        ExecProc;

                        if Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
                            raise EErrorExceptionCN.Create(
                                RS_ERROR_EJECUCION_TITULO,
                                Format(RS_ERROR_EJECUCION_MENSAJE,
                                       [ProcedureName,
                                        Parameters.ParamByName('@RETURN_VALUE').Value]));
                        end;
                    end;

                    if cdsCuotasImporte.AsLargeInt <> cdsCuotasSaldo.AsLargeInt then begin                  // SS_1050_PDO_20120605
                        ImporteCuotaAnulada := cdsCuotasImporte.AsLargeInt - cdsCuotasSaldo.AsLargeInt;     // SS_1050_PDO_20120605
                        EstadoCuotaActual   := REFINANCIACION_ESTADO_CUOTA_PAGADA;                          // SS_1050_PDO_20120605
                    end                                                                                     // SS_1050_PDO_20120605
                    else begin                                                                              // SS_1050_PDO_20120605
                        ImporteCuotaAnulada := cdsCuotasImporte.AsLargeInt;                                 // SS_1050_PDO_20120605
                        EstadoCuotaActual   := REFINANCIACION_ESTADO_CUOTA_ANULADA_POR_MORA;                // SS_1050_PDO_20120605
                    end;                                                                                    // SS_1050_PDO_20120605

                        with spActualizarRefinanciacionCuotas do begin
                            if Active then Close;
                            Parameters.Refresh;

                            with Parameters do begin
                                ParamByName('@CodigoRefinanciacion').Value   := FCodigoRefinanciacion;
                                ParamByName('@FechaCuota').Value             := Null;
                            ParamByName('@Importe').Value                := ImporteCuotaAnulada;            // SS_1050_PDO_20120605
                                ParamByName('@Saldo').Value                  := 0;
                                ParamByName('@CodigoEstadoCuota').Value      := Null;
                                ParamByName('@CodigoFormaPago').Value        := Null;
                                ParamByName('@TitularNombre').Value          := Null;
                                ParamByName('@TitularTipoDocumento').Value   := Null;
                                ParamByName('@TitularNumeroDocumento').Value := Null;
                                ParamByName('@DocumentoNumero').Value        := Null;
                                ParamByName('@CodigoBanco').Value            := Null;
                                ParamByName('@CodigoTarjeta').Value          := Null;
                                ParamByName('@TarjetaCuotas').Value          := Null;
                                ParamByName('@BorrarExistentes').Value       := 0;
                                ParamByName('@Usuario').Value                := Null;
                                ParamByName('@CodigoCuota').Value            := cdsCuotasCodigoCuota.AsInteger;
                                ParamByName('@CodigoCuotaAntecesora').Value  := Null;
                            end;

                            ExecProc;

                            if Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
                                raise EErrorExceptionCN.Create(
                                    RS_ERROR_EJECUCION_TITULO,
                                    Format(RS_ERROR_EJECUCION_MENSAJE,
                                           [ProcedureName,
                                            Parameters.ParamByName('@RETURN_VALUE').Value]));
                            end;
                        end;


                    with spActualizarRefinanciacionCuotasEstados do begin
                        if Active then Close;
                        Parameters.Refresh;

                        Parameters.ParamByName('@CodigoRefinanciacion').Value := FCodigoRefinanciacion;
                        Parameters.ParamByName('@CodigoCuota').Value          := cdsCuotasCodigoCuota.AsInteger;
                        Parameters.ParamByName('@CodigoEstadoCuota').Value    := EstadoCuotaActual;
                        Parameters.ParamByName('@Usuario').Value              := UsuarioSistema;

                        ExecProc;

                        if Parameters.ParamByName('@RETURN_VALUE').Value <> 0 then begin
                            raise EErrorExceptionCN.Create(RS_TITULO_ACCION, RS_MENSAJE_ERROR_CAMBIO_ESTADO);
                        end;
                    end;

                    RefinanciacionRenumerarCuotas(DMConnections.BaseCAC, FCodigoRefinanciacion);    // SS_1050_PDO_20120605

                    QueryExecute(DMConnections.BaseCAC, 'IF @@TRANCOUNT > 0 BEGIN COMMIT TRANSACTION PactarMora END');
                    try
                            Puntero := cdsCuotas.GetBookmark;
                            cdsCuotas.Close;
                            MostrarCuotas;
                        finally
                            if Assigned(Puntero) then begin
                                if cdsCuotas.BookmarkValid(Puntero) then cdsCuotas.GotoBookmark(Puntero);
                                cdsCuotas.FreeBookmark(Puntero);
                            end;
                        end;


                    ShowMsgBoxCN(RS_TITULO_ACCION, RS_MENSAJE_RESULTADO, MB_ICONINFORMATION, Self);
                end;
            except
                on e:Exception do begin
                    QueryExecute(DMConnections.BaseCAC, 'IF @@TRANCOUNT > 0 BEGIN ROLLBACK TRANSACTION PactarMora END');
                    ShowMsgBoxCN(e, Self);
                end;
            end;
        end;
    Finally
        if Assigned(RefinanciacionPactarMoraForm) then FreeAndNil(RefinanciacionPactarMoraForm);
        Screen.Cursor := crDefault;
    End;
end;

procedure TRefinanciacionGestionCuotasForm.cdsCuotasAfterScroll(DataSet: TDataSet);
begin
    ActualizarBotones;
end;

procedure TRefinanciacionGestionCuotasForm.DBListEx3DrawBackground(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var DefaultDraw: Boolean);
begin
    with Sender do begin
        if ((cdsCuotasFechaCuota.AsDateTime < FAhora) and cdsCuotasEstadoImpago.AsBoolean) or
            (cdsCuotasCodigoEstadoCuota.AsInteger in [REFINANCIACION_ESTADO_CUOTA_ANULADA, REFINANCIACION_ESTADO_CUOTA_ANULADA_POR_MORA]) then begin        
            if (odSelected in State) then Canvas.Brush.Color := clMaroon;
        end;
    end;
end;

procedure TRefinanciacionGestionCuotasForm.DBListEx3DrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
    with Sender do begin
        if ((cdsCuotasFechaCuota.AsDateTime < FAhora) and cdsCuotasEstadoImpago.AsBoolean) or
            (cdsCuotasCodigoEstadoCuota.AsInteger in [REFINANCIACION_ESTADO_CUOTA_ANULADA, REFINANCIACION_ESTADO_CUOTA_ANULADA_POR_MORA]) then begin

            if Not (odSelected in State) then begin
                Canvas.Font.Color := clRed;
            end;

            if Column.FieldName = 'DescripcionEstadoCuota' then begin
                if cdsCuotasCodigoEstadoCuota.AsInteger in [REFINANCIACION_ESTADO_CUOTA_ANULADA, REFINANCIACION_ESTADO_CUOTA_ANULADA_POR_MORA] then
                    Canvas.Font.Style := Canvas.Font.Style + [fsBold];
            end;
        end;
    end;

    if (Column.FieldName = 'Importe') or (Column.FieldName = 'Saldo') then begin
        Text := FormatFloat(FORMATO_IMPORTE, StrToFloat(Text)) + ' ';
    end;
end;

procedure TRefinanciacionGestionCuotasForm.FormActivate(Sender: TObject);
begin
    TPanelMensajesForm.OcultaPanel;
end;

function TRefinanciacionGestionCuotasForm.Inicializar(Refinanciacion: Integer): Boolean;
begin
    try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        CargarConstantesCobranzasRefinanciacion;
        CargarConstantesCobranzasCanalesFormasPago;

        TPanelMensajesForm.MuestraMensaje('inicializando Formulario Gesti�n Cuotas ...');

        Result := False;
        vcbTipoRefinanciacion.Enabled := False;
        dedtFechaRefi.Enabled         := False;
        cbNotificacion.Enabled        := False;
        peRut.ReadOnly                := True;
        vcbConvenios.Enabled          := False;

        FAhora := Trunc(NowBaseSmall(DMConnections.BaseCAC));

        CargarCanalesDePago(DMConnections.BaseCAC, vcbTipoRefinanciacion, 0, 1);

        cdsCuotas.Open;

        FCodigoRefinanciacion := Refinanciacion;

        TPanelMensajesForm.MuestraMensaje(Format('inicializando Refinanciaci�n Nro. %d ...',[FCodigoRefinanciacion]));
        TPanelMensajesForm.MuestraMensaje('Cargando Datos Principales ...');

        with spObtenerRefinanciacionDatosCabecera do begin
            Parameters.Refresh;
            Parameters.ParamByName('@CodigoRefinanciacion').Value := FCodigoRefinanciacion;
            Open;

            FEditable := FieldByName('CodigoEstado').AsInteger = REFINANCIACION_ESTADO_VIGENTE;

            FPersona              := ObtenerDatosPersonales(DMConnections.BaseCAC,'','', FieldByName('CodigoPersona').AsInteger);
            FCodigoConvenio       := FieldByName('CodigoConvenio').AsInteger;
            lblEstado.Caption     := FieldByName('EstadoRefinanciacion').AsString;
            lblProtestada.Visible := FieldByName('Protestada').AsBoolean;


            FComprobantesRefinanciadosTotal := FieldByName('TotalDeuda').AsInteger;
            FDeudaPagada                    := FieldByName('TotalPagado').AsInteger;

            lblNumRefinanciacion.Caption := IntToStr(Refinanciacion);
            dedtFechaRefi.Date := FieldByName('Fecha').AsDateTime;
            FCodigoCanalPago := FieldByName('CodigoTipoMedioPago').AsInteger;
            vcbTipoRefinanciacion.Value := FCodigoCanalPago;
            cbNotificacion.Checked := iif( FieldByName('Notificacion').AsBoolean, True, False);

            if FPersona.CodigoPersona <> -1 then begin
                TPanelMensajesForm.MuestraMensaje('Cargando Datos Domicilio ...');
                with spObtenerRefinanciacionDomicilio do try
                    Parameters.ParamByName('@CodigoRefinanciacion').Value := FCodigoRefinanciacion;
                    Open;

                    if RecordCount = 1 then begin
                        with FDatosDomicilio do begin
                            Descripcion         := FieldByName('Calle').AsString;
                            CalleDesnormalizada := FieldByName('Calle').AsString;
                            NumeroCalle         := FieldByName('Numero').AsInteger;
                            NumeroCalleSTR      := FieldByName('Numero').AsString;
                            Piso                := FieldByName('Piso').AsInteger;
                            Dpto                := FieldByName('Dpto').AsString;
                            Detalle             := FieldByName('Detalle').AsString;
                            CodigoPais          := FieldByName('CodigoPais').AsString;
                            CodigoRegion        := FieldByName('CodigoRegion').AsString;
                            CodigoComuna        := FieldByName('CodigoComuna').AsString;
                            CodigoPostal        := FieldByName('CodigoPostal').AsString;
                            CodigoDomicilio     := -1;
                            CodigoCalle         := -1;
                            CodigoTipoCalle     := -1;
                            CodigoSegmento      := -1;
                            CodigoTipoDomicilio := 1;
                        end;

                        FDomicilioModificado := True;
                    end;
                finally
                    if Active then Close;
                end;

                TPanelMensajesForm.MuestraMensaje('Cargando Datos Cliente ...');
                MostrarDatosCliente;

                TPanelMensajesForm.MuestraMensaje('Cargando Datos Convenios ...');
                CargarConveniosRUT(DMConnections.BaseCAC, vcbConvenios, FPersona.TipoDocumento, FPersona.NumeroDocumento, False, FCodigoConvenio, False, True);         //SS_1120_MVI_20130820

                if FCodigoConvenio <> 0 then MostrarDatosConvenio;
            end;

            TPanelMensajesForm.MuestraMensaje('Cargando Datos Cuotas ...');
            MostrarCuotas;
        end;

        ActualizarBotones;

        Result := True;
    finally
        Screen.Cursor := crDefault;
    end;
end;

procedure TRefinanciacionGestionCuotasForm.MostrarDatosCliente;
    var
        AuxDomicilio: TDomiciliosPersona;
    const
        SQLObtenerNombrePersona   = 'SELECT dbo.ObtenerNombrePersona(%d)';
begin
    try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        peRut.Text           := FPersona.NumeroDocumento;
        lblNombreCli.Caption := QueryGetValue(DMConnections.BaseCAC, Format(SQLObtenerNombrePersona, [FPersona.CodigoPersona]));

        if FPersona.Personeria = PERSONERIA_FISICA then begin
            lblPersoneria.Caption := ' - (Persona Natural)';
        end
        else begin
            if FPersona.Personeria = PERSONERIA_JURIDICA then lblPersoneria.Caption := ' - (Persona Jur�dica)';
            if FPersona.Personeria = PERSONERIA_AMBAS then lblPersoneria.Caption := ' - (Persona Natural y Jur�dica)';
        end;

        lblPersoneria.Left := lblNombreCli.Left + lblNombreCli.Width;
        //lblDomicilioCliDesc.Visible := True;

        if not FDomicilioModificado then begin
            AuxDomicilio    := TDomiciliosPersona.Create(FPersona.CodigoPersona);
            FDatosDomicilio := AuxDomicilio.DomicilioPrincipal;
        end;

        MostrarDomicilioCliente;
    finally
        if Assigned(AuxDomicilio) then FreeAndNil(AuxDomicilio);

        Screen.Cursor := crDefault;
        Application.ProcessMessages;
    end;
end;

procedure TRefinanciacionGestionCuotasForm.MostrarDomicilioCliente;
begin
    with FDatosDomicilio do begin
        lblDomicilioCli.Caption :=
            QueryGetValue(
                DMConnections.BaseCAC,
                Format(SQLArmarDomicilioCompleto,
                       [ReemplazaPorApostrofe(Trim(Descripcion)),
                        NumeroCalle,
                        Piso,
                        ReemplazaPorApostrofe(Trim(Dpto)),
                        ReemplazaPorApostrofe(Trim(Detalle)),
                        CodigoComuna,
                        CodigoRegion]));
    end;
end;

procedure TRefinanciacionGestionCuotasForm.MostrarCuotas;
begin
    if cdsCuotas.Active then cdsCuotas.Close;

    with spObtenerRefinanciacionCuotas do begin
        Parameters.Refresh;
        Parameters.ParamByName('@CodigoRefinanciacion').Value := FCodigoRefinanciacion;
        Parameters.ParamByName('@CodigoCuota').Value          := Null;
        Parameters.ParamByName('@SinCuotasAnuladas').Value    := 0;
    end;

    with cdsCuotas do try
        dsCuotas.DataSet := Nil;
        Open;

        FCuotasTotal  := 0;
        FDeudaVencida := 0;
        FDeudaPagada  := 0;

        while not Eof do begin
            if not (cdsCuotasCodigoEstadoCuota.AsInteger in [REFINANCIACION_ESTADO_CUOTA_ANULADA, REFINANCIACION_ESTADO_CUOTA_ANULADA_POR_MORA, REFINANCIACION_ESTADO_CUOTA_ANULADA_POR_PROTESTO]) then begin
                FCuotasTotal := FCuotasTotal + cdsCuotasImporte.AsLargeInt;
                FDeudaPagada := FDeudaPagada + (cdsCuotasImporte.AsLargeInt - cdsCuotasSaldo.AsLargeInt);

                if cdsCuotasEstadoImpago.AsBoolean then begin
                    if cdsCuotasFechaCuota.AsDateTime < FAhora then begin
                        FDeudaVencida := FDeudaVencida + cdsCuotasSaldo.AsLargeInt;
                    end;
                end;
            end;

            Next;
        end;
    finally
        dsCuotas.DataSet := cdsCuotas;
        First;
    end;

    FCuotasCantidad := cdsCuotas.RecordCount;

    ActualizarTotales;
end;

procedure TRefinanciacionGestionCuotasForm.ActualizarTotales;
    resourcestring
        rsValidacionImportesTitulo  = 'Validaci�n Total Cuotas';
        rsValidacionImportesMensaje = 'El Importe Total de las Cuotas Entradas es Mayor que el Total de Comprobantes Refinanciados.';
begin
    // Totales Comprobantes Refinanciados
    lblTotalRefinanciado2.Caption := FormatFloat(FORMATO_IMPORTE, FComprobantesRefinanciadosTotal);
    // Totales Cuotas Refinanciaci�n
    lblCuotasCantidad.Caption     := IntToStr(FCuotasCantidad);
    lblCuotasTotal.Caption        := FormatFloat(FORMATO_IMPORTE, FCuotasTotal);
    lblDeudaPagada.Caption        := FormatFloat(FORMATO_IMPORTE, FDeudaPagada);
    lblDeudaPagada2.Caption       := FormatFloat(FORMATO_IMPORTE, FDeudaPagada);
    lblDeudaPendiente.Caption     := FormatFloat(FORMATO_IMPORTE, FComprobantesRefinanciadosTotal - FDeudaPagada);
    lblDeudaVencida.Caption       := FormatFloat(FORMATO_IMPORTE, FDeudaVencida);

    if FDeudaVencida > 0 then
        lblDeudaVencida.Font.Color := clRed
    else lblDeudaVencida.Font.Color := clBlack;

    if (FComprobantesRefinanciadosTotal - FDeudaPagada) > 0 then
        lblDeudaPendiente.Font.Color := $000080FF
    else lblDeudaPendiente.Font.Color := clBlack;

    if FCuotasTotal <> FComprobantesRefinanciadosTotal then
        lblCuotasTotal.Font.Color := clRed
    else lblCuotasTotal.Font.Color := clGreen;
end;

procedure TRefinanciacionGestionCuotasForm.MostrarDatosConvenio;
begin
    try
        Screen.Cursor := crHandPoint;
        Application.ProcessMessages;

        lblDomicilioFact.Caption := QueryGetValue(DMConnections.BaseCAC, Format('SELECT dbo.ObtenerDescripcionDomicilioFacturacion(%d)', [FCodigoConvenio]));
        lblConcesionaria.Caption := QueryGetValue(DMConnections.BaseCAC, Format('SELECT dbo.ObtenerConcesionariaConvenio(%d)', [FCodigoConvenio]));

    finally
        Screen.Cursor := crDefault;
    end;
end;

procedure TRefinanciacionGestionCuotasForm.ActualizarBotones;
    var
        EstadoCuotaOperable,
        CuotaEnEfectivoPagare,
        CuotaEnValeVista,
        CuotaEnCheque,
        VerHistorialPagos: Boolean;
begin
    EstadoCuotaOperable   := not (cdsCuotasCodigoEstadoCuota.AsInteger in [REFINANCIACION_ESTADO_CUOTA_ANULADA, REFINANCIACION_ESTADO_CUOTA_ANULADA_POR_MORA, REFINANCIACION_ESTADO_CUOTA_PAGADA, REFINANCIACION_ESTADO_CUOTA_DEPOSITADO]);
    CuotaEnEfectivoPagare := cdsCuotasCodigoFormaPago.AsInteger in [FORMA_PAGO_EFECTIVO, FORMA_PAGO_PAGARE];
    CuotaEnValeVista      := cdsCuotasCodigoFormaPago.AsInteger = FORMA_PAGO_VALE_VISTA;
    CuotaEnCheque         := cdsCuotasCodigoFormaPago.AsInteger = FORMA_PAGO_CHEQUE;
    VerHistorialPagos     := cdsCuotas.RecordCount > 0;

//    btnPagoAnticipado.Enabled := FEditable and EstadoCuotaOperable and CuotaEnEfectivoPagare;
    btnPactarMora.Enabled     := FEditable and EstadoCuotaOperable and (not CuotaEnValeVista);
    btnCobranza.Enabled       := FEditable and EstadoCuotaOperable and (not CuotaEnValeVista);

    btnHistorialPagos.Enabled := VerHistorialPagos;
end;

function TRefinanciacionGestionCuotasForm.ReemplazaPorApostrofe(Cadena: String): String;
    var
        Veces: Integer;
begin
    Result := EmptyStr;

    for Veces := 1 to length(Cadena) do begin
        if Cadena[Veces] = '''' then begin
            Result := Result + APOSTROFE;
        end
        else begin
            Result := Result + Cadena[Veces];
        end;
    end;
end;

//BEGIN:SS_1120_MVI_20130820 -------------------------------------------------
{-----------------------------------------------------------------------------
  Procedure: TRefinanciacionGestionCuotasForm.vcbConveniosDrawItem
  Date:      20-August-2013
  Description: Verifica si existe alg�n convenio de baja para llamar a la funci�n
                que colorea de rojo el item.
-----------------------------------------------------------------------------}
procedure TRefinanciacionGestionCuotasForm.vcbConveniosDrawItem(
  Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  if Pos( TXT_CONVENIO_DE_BAJA, vcbConvenios.Items[Index].Caption ) > 0 then  begin

     ItemComboBoxColor(Control, Index, Rect, State, clred, true);

  end
  else begin

     ItemComboBoxColor(Control, Index, Rect, State);

  end;

end;
//END:SS_1120_MVI_20130820 ---------------------------------------------------

end.
