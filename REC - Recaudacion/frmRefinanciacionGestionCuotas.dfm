object RefinanciacionGestionCuotasForm: TRefinanciacionGestionCuotasForm
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = ' Gesti'#243'n Cuotas Refinanciaci'#243'n'
  ClientHeight = 635
  ClientWidth = 957
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  DesignSize = (
    957
    635)
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 941
    Height = 225
    Anchors = [akLeft, akTop, akRight]
    Caption = '  Datos Principales Refinanciaci'#243'n  '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    DesignSize = (
      941
      225)
    object Label1: TLabel
      Left = 24
      Top = 23
      Width = 24
      Height = 13
      Caption = 'Tipo:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 214
      Top = 23
      Width = 41
      Height = 13
      Caption = 'N'#250'mero:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 343
      Top = 23
      Width = 33
      Height = 13
      Caption = 'Fecha:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblNumRefinanciacion: TLabel
      Left = 259
      Top = 23
      Width = 64
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'NumRefi'
    end
    object Label5: TLabel
      Left = 553
      Top = 23
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = 'Estado :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblEstado: TLabel
      Left = 595
      Top = 23
      Width = 93
      Height = 13
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      AutoSize = False
      Caption = '<Estado>'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 714
      Top = 23
      Width = 117
      Height = 13
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = 'Importe Refinanciaci'#243'n :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblTotalRefinanciado2: TLabel
      Left = 848
      Top = 23
      Width = 80
      Height = 13
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      AutoSize = False
      Caption = '$ 0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 747
      Top = 37
      Width = 84
      Height = 13
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = 'Importe Pagado :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label10: TLabel
      Left = 746
      Top = 67
      Width = 85
      Height = 13
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = 'Importe Vencido :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblDeudaPagada: TLabel
      Left = 848
      Top = 37
      Width = 80
      Height = 13
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      AutoSize = False
      Caption = '$ 0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblDeudaVencida: TLabel
      Left = 848
      Top = 67
      Width = 80
      Height = 13
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      AutoSize = False
      Caption = '$ 0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label9: TLabel
      Left = 735
      Top = 52
      Width = 96
      Height = 13
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = 'Importe Pendiente :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblDeudaPendiente: TLabel
      Left = 848
      Top = 52
      Width = 80
      Height = 13
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      AutoSize = False
      Caption = '$ 0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblProtestada: TLabel
      Left = 626
      Top = 38
      Width = 63
      Height = 13
      Caption = 'Protestada'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object vcbTipoRefinanciacion: TVariantComboBox
      Left = 54
      Top = 20
      Width = 145
      Height = 21
      Hint = ' Seleccionar Tipo de Refinanciaci'#243'n '
      Style = vcsDropDownList
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ItemHeight = 13
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Items = <
        item
          Caption = 'Avenimiento'
          Value = 38
        end
        item
          Caption = 'Repactaci'#243'n'
          Value = 36
        end>
    end
    object GroupBox2: TGroupBox
      Left = 8
      Top = 88
      Width = 926
      Height = 128
      Anchors = [akLeft, akRight, akBottom]
      Caption = '  Datos Cliente  '
      TabOrder = 3
      DesignSize = (
        926
        128)
      object Label3: TLabel
        Left = 21
        Top = 23
        Width = 21
        Height = 13
        Caption = 'Rut:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblNombreCli: TLabel
        Left = 48
        Top = 45
        Width = 70
        Height = 13
        Caption = 'lblNombreCli'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label7: TLabel
        Left = 206
        Top = 23
        Width = 49
        Height = 13
        Caption = 'Convenio:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblPersoneria: TLabel
        Left = 127
        Top = 45
        Width = 61
        Height = 13
        Caption = 'lblPersoneria'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblConcesionaria: TLabel
        Left = 475
        Top = 23
        Width = 92
        Height = 13
        Caption = 'lblConcesionaria'
      end
      object peRut: TPickEdit
        Left = 48
        Top = 18
        Width = 143
        Height = 21
        Hint = ' Seleccionar el Cliente a Refinanciar '
        Enabled = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        EditorStyle = bteTextEdit
      end
      object vcbConvenios: TVariantComboBox
        Left = 261
        Top = 18
        Width = 208
        Height = 19
        Style = vcsOwnerDrawFixed
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ItemHeight = 13
        ParentFont = False
        TabOrder = 1
        OnDrawItem = vcbConveniosDrawItem
        Items = <>
      end
      object GroupBox7: TGroupBox
        Left = 8
        Top = 68
        Width = 911
        Height = 52
        Anchors = [akLeft, akRight, akBottom]
        Caption = '  Domicilios  '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        object lblDomicilioCliDesc: TLabel
          Left = 23
          Top = 17
          Width = 71
          Height = 13
          Caption = 'Domicilio Part.:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object lbldomicilioFactDesc: TLabel
          Left = 22
          Top = 32
          Width = 72
          Height = 13
          Caption = 'Domicilio Fact.:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object lblDomicilioFact: TLabel
          Left = 98
          Top = 32
          Width = 71
          Height = 13
          Caption = 'lblDomicilioFact'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object lblDomicilioCli: TLabel
          Left = 98
          Top = 17
          Width = 77
          Height = 13
          Caption = 'lblDomicilioCli'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
      end
    end
    object dedtFechaRefi: TDateEdit
      Left = 385
      Top = 20
      Width = 104
      Height = 21
      AutoSelect = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      Date = -693594.000000000000000000
    end
    object cbNotificacion: TCheckBox
      Left = 54
      Top = 44
      Width = 158
      Height = 17
      Hint = ' Marcar si es Avenimiento con Notificaci'#243'n '
      Caption = 'Avenimiento con Notificaci'#243'n'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
    end
  end
  object GroupBox6: TGroupBox
    Left = 8
    Top = 236
    Width = 941
    Height = 353
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = '  Detalle Refinanciaci'#243'n  '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    DesignSize = (
      941
      353)
    object PageControl1: TPageControl
      Left = 8
      Top = 18
      Width = 926
      Height = 328
      ActivePage = tbsCuotas
      Anchors = [akLeft, akTop, akRight, akBottom]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      HotTrack = True
      ParentFont = False
      TabOrder = 0
      object tbsCuotas: TTabSheet
        Caption = ' Cuotas '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ImageIndex = 1
        ParentFont = False
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 918
          Height = 34
          Align = alTop
          BevelInner = bvRaised
          BevelOuter = bvLowered
          Color = 14408667
          ParentBackground = False
          TabOrder = 0
          object btnPactarMora: TButton
            Left = 5
            Top = 5
            Width = 84
            Height = 25
            Caption = 'Pactar Mora'
            TabOrder = 0
            OnClick = btnPactarMoraClick
          end
          object btnHistorialEstados: TButton
            Left = 208
            Top = 5
            Width = 96
            Height = 25
            Caption = 'Historial Estados'
            TabOrder = 1
            OnClick = btnHistorialEstadosClick
          end
          object btnCobranza: TButton
            Left = 91
            Top = 5
            Width = 105
            Height = 25
            Caption = 'Enviar a Cobranza'
            TabOrder = 2
            OnClick = btnCobranzaClick
          end
          object btnHistorialPagos: TButton
            Left = 306
            Top = 5
            Width = 96
            Height = 25
            Caption = 'Historial Pagos'
            TabOrder = 3
            OnClick = btnHistorialPagosClick
          end
        end
        object DBListEx3: TDBListEx
          Left = 0
          Top = 34
          Width = 918
          Height = 236
          Align = alClient
          BorderStyle = bsSingle
          Columns = <
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 30
              Header.Caption = 'ID'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'CodigoCuota'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              Width = 35
              Header.Caption = 'Ant.'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'CodigoCuotaAntecesora'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 120
              Header.Caption = 'Forma de Pago'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              IsLink = False
              FieldName = 'DescripcionFormaPago'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 35
              Header.Caption = 'Nro.'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              Header.Alignment = taRightJustify
              IsLink = False
              FieldName = 'NumeroCuota'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 75
              Header.Caption = 'Fecha'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'FechaCuota'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 90
              Header.Caption = 'Importe'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              Header.Alignment = taRightJustify
              IsLink = False
              FieldName = 'Importe'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 90
              Header.Caption = 'Saldo'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              Header.Alignment = taRightJustify
              IsLink = False
              FieldName = 'Saldo'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 115
              Header.Caption = 'Estado'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              IsLink = False
              FieldName = 'DescripcionEstadoCuota'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 90
              Header.Caption = 'Rut'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              IsLink = False
              FieldName = 'TitularNumeroDocumento'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 150
              Header.Caption = 'Titular'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              IsLink = False
              FieldName = 'TitularNombre'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 120
              Header.Caption = 'Nro. Doc.'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              IsLink = False
              FieldName = 'DocumentoNumero'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 175
              Header.Caption = 'Banco'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              IsLink = False
              FieldName = 'DescripcionBanco'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 175
              Header.Caption = 'Tipo Tarjeta'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              IsLink = False
              FieldName = 'DescripcionTarjeta'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 100
              Header.Caption = 'Cuotas Tarjeta'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              Header.Alignment = taRightJustify
              IsLink = False
              FieldName = 'TarjetaCuotas'
            end>
          DataSource = dsCuotas
          DragReorder = True
          ParentColor = False
          TabOrder = 1
          TabStop = True
          OnDrawBackground = DBListEx3DrawBackground
          OnDrawText = DBListEx3DrawText
        end
        object Panel7: TPanel
          Left = 0
          Top = 270
          Width = 918
          Height = 30
          Align = alBottom
          BevelInner = bvRaised
          BevelOuter = bvLowered
          Color = 14408667
          ParentBackground = False
          TabOrder = 2
          DesignSize = (
            918
            30)
          object Label18: TLabel
            Left = 456
            Top = 9
            Width = 38
            Height = 13
            Anchors = [akRight, akBottom]
            Caption = 'Cuotas:'
            ExplicitLeft = 406
          end
          object lblCuotasCantidad: TLabel
            Left = 489
            Top = 9
            Width = 36
            Height = 13
            Alignment = taRightJustify
            Anchors = [akRight, akBottom]
            AutoSize = False
            Caption = '0'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            ExplicitLeft = 439
          end
          object Label20: TLabel
            Left = 760
            Top = 9
            Width = 69
            Height = 13
            Anchors = [akRight, akBottom]
            Caption = 'Importe Total:'
            ExplicitLeft = 710
          end
          object Label21: TLabel
            Left = 562
            Top = 9
            Width = 81
            Height = 13
            Anchors = [akRight, akBottom]
            Caption = 'Importe Pagado:'
            ExplicitLeft = 512
          end
          object lblDeudaPagada2: TLabel
            Left = 655
            Top = 9
            Width = 80
            Height = 13
            Alignment = taRightJustify
            Anchors = [akRight, akBottom]
            AutoSize = False
            Caption = '$ 0'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            ExplicitLeft = 605
          end
          object lblCuotasTotal: TLabel
            Left = 830
            Top = 9
            Width = 80
            Height = 13
            Alignment = taRightJustify
            Anchors = [akRight, akBottom]
            AutoSize = False
            Caption = '$ 0'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            ExplicitLeft = 780
          end
        end
      end
    end
  end
  object pnlAcciones: TPanel
    Left = 0
    Top = 595
    Width = 957
    Height = 40
    Align = alBottom
    Anchors = []
    Color = 14408667
    ParentBackground = False
    TabOrder = 2
    DesignSize = (
      957
      40)
    object btnCancelar: TButton
      Left = 877
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Salir'
      TabOrder = 0
      OnClick = btnCancelarClick
    end
  end
  object spObtenerRefinanciacionCuotas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerRefinanciacionCuotas'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCuota'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@SinCuotasAnuladas'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 784
    Top = 336
  end
  object dspObtenerRefinanciacionCuotas: TDataSetProvider
    DataSet = spObtenerRefinanciacionCuotas
    Left = 760
    Top = 360
  end
  object cdsCuotas: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoCuota'
        DataType = ftSmallint
      end
      item
        Name = 'CodigoEstadoCuota'
        DataType = ftSmallint
      end
      item
        Name = 'DescripcionEstadoCuota'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'CodigoFormaPago'
        DataType = ftSmallint
      end
      item
        Name = 'DescripcionFormaPago'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'TitularNombre'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'TitularTipoDocumento'
        Attributes = [faFixed]
        DataType = ftString
        Size = 4
      end
      item
        Name = 'TitularNumeroDocumento'
        DataType = ftString
        Size = 11
      end
      item
        Name = 'CodigoBanco'
        DataType = ftInteger
      end
      item
        Name = 'DescripcionBanco'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'DocumentoNumero'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'FechaCuota'
        DataType = ftDateTime
      end
      item
        Name = 'CodigoTarjeta'
        DataType = ftInteger
      end
      item
        Name = 'DescripcionTarjeta'
        Attributes = [faFixed]
        DataType = ftString
        Size = 50
      end
      item
        Name = 'TarjetaCuotas'
        DataType = ftSmallint
      end
      item
        Name = 'Importe'
        DataType = ftLargeint
      end
      item
        Name = 'Saldo'
        DataType = ftLargeint
      end
      item
        Name = 'CodigoEntradaUsuario'
        Attributes = [faFixed]
        DataType = ftString
        Size = 2
      end
      item
        Name = 'EstadoImpago'
        DataType = ftBoolean
      end
      item
        Name = 'CodigoCuotaAntecesora'
        DataType = ftSmallint
      end
      item
        Name = 'NumeroCuota'
        DataType = ftSmallint
      end>
    IndexDefs = <
      item
        Name = 'FechaCuota'
        Fields = 'FechaCuota'
      end>
    IndexName = 'FechaCuota'
    Params = <>
    ProviderName = 'dspObtenerRefinanciacionCuotas'
    StoreDefs = True
    AfterScroll = cdsCuotasAfterScroll
    Left = 712
    Top = 376
    object cdsCuotasCodigoCuota: TSmallintField
      FieldName = 'CodigoCuota'
    end
    object cdsCuotasCodigoEstadoCuota: TSmallintField
      FieldName = 'CodigoEstadoCuota'
    end
    object cdsCuotasDescripcionEstadoCuota: TStringField
      FieldName = 'DescripcionEstadoCuota'
      Size = 30
    end
    object cdsCuotasCodigoFormaPago: TSmallintField
      FieldName = 'CodigoFormaPago'
    end
    object cdsCuotasDescripcionFormaPago: TStringField
      FieldName = 'DescripcionFormaPago'
      Size = 30
    end
    object cdsCuotasTitularNombre: TStringField
      FieldName = 'TitularNombre'
      Size = 100
    end
    object cdsCuotasTitularTipoDocumento: TStringField
      FieldName = 'TitularTipoDocumento'
      FixedChar = True
      Size = 4
    end
    object cdsCuotasTitularNumeroDocumento: TStringField
      FieldName = 'TitularNumeroDocumento'
      Size = 11
    end
    object cdsCuotasCodigoBanco: TIntegerField
      FieldName = 'CodigoBanco'
    end
    object cdsCuotasDescripcionBanco: TStringField
      FieldName = 'DescripcionBanco'
      Size = 50
    end
    object cdsCuotasDocumentoNumero: TStringField
      FieldName = 'DocumentoNumero'
    end
    object cdsCuotasFechaCuota: TDateTimeField
      FieldName = 'FechaCuota'
    end
    object cdsCuotasCodigoTarjeta: TIntegerField
      FieldName = 'CodigoTarjeta'
    end
    object cdsCuotasDescripcionTarjeta: TStringField
      FieldName = 'DescripcionTarjeta'
      FixedChar = True
      Size = 50
    end
    object cdsCuotasTarjetaCuotas: TSmallintField
      FieldName = 'TarjetaCuotas'
    end
    object cdsCuotasImporte: TLargeintField
      FieldName = 'Importe'
    end
    object cdsCuotasSaldo: TLargeintField
      FieldName = 'Saldo'
    end
    object cdsCuotasCodigoEntradaUsuario: TStringField
      FieldName = 'CodigoEntradaUsuario'
      FixedChar = True
      Size = 2
    end
    object cdsCuotasEstadoImpago: TBooleanField
      FieldName = 'EstadoImpago'
    end
    object cdsCuotasCodigoCuotaAntecesora: TSmallintField
      FieldName = 'CodigoCuotaAntecesora'
    end
    object cdsCuotasCuotaNueva: TBooleanField
      FieldKind = fkInternalCalc
      FieldName = 'CuotaNueva'
    end
    object cdsCuotasCuotaModificada: TBooleanField
      FieldKind = fkInternalCalc
      FieldName = 'CuotaModificada'
    end
    object cdsCuotasNumeroCuota: TSmallintField
      FieldName = 'NumeroCuota'
    end
  end
  object dsCuotas: TDataSource
    DataSet = cdsCuotas
    Left = 696
    Top = 392
  end
  object spObtenerRefinanciacionDatosCabecera: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerRefinanciacionDatosCabecera'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 568
    Top = 456
  end
  object spObtenerRefinanciacionDomicilio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerRefinanciacionDomicilio'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 528
    Top = 520
  end
  object spActualizarRefinanciacionCuotasEstados: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarRefinanciacionCuotasEstados'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCuota'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@CodigoEstadoCuota'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 368
    Top = 416
  end
  object spActualizarRefinanciacionPagoAnticipado: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarRefinanciacionPagoAnticipado'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCuota'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@Importe'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaPago'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@NumeroRecibo'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 360
    Top = 472
  end
  object spActualizarRefinanciacionCuotas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarRefinanciacionCuotas'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaCuota'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Importe'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@Saldo'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CodigoEstadoCuota'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@CodigoFormaPago'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@TitularNombre'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@TitularTipoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@TitularNumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@DocumentoNumero'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@CodigoBanco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTarjeta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TarjetaCuotas'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@BorrarExistentes'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@CodigoCuota'
        Attributes = [paNullable]
        DataType = ftSmallint
        Direction = pdInputOutput
        Precision = 5
        Value = Null
      end
      item
        Name = '@CodigoCuotaAntecesora'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end>
    Left = 264
    Top = 352
  end
  object spActualizarRefinanciacionCuotasSaldo: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarRefinanciacionCuotasSaldo'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCuota'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@Saldo'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end>
    Left = 120
    Top = 408
  end
  object spActualizarRefinanciacionCuotaPago: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarRefinanciacionCuotaPago'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCuota'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@Importe'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@Anticipado'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@CodigoFormaPago'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@TitularNombre'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@TitularTipoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@TitularNumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@DocumentoNumero'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@CodigoBanco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTarjeta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TarjetaCuotas'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaPago'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@NumeroRecibo'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 776
    Top = 440
  end
end
