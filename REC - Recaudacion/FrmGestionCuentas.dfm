object FormGestionCuentas: TFormGestionCuentas
  Left = 145
  Top = 148
  Width = 718
  Height = 483
  Caption = 'Gesti'#243'n de Cuentas'
  Color = clBtnFace
  Constraints.MinHeight = 457
  Constraints.MinWidth = 627
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poMainFormCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Grilla: TDPSGrid
    Left = 0
    Top = 81
    Width = 710
    Height = 334
    Align = alClient
    DataSource = DataSource1
    Options = [dgTitles, dgColumnResize, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    PopupMenu = pop_Cuentas
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = GrillaDblClick
    Columns = <
      item
        Expanded = False
        FieldName = 'CodigoCuenta'
        Title.Caption = 'Cuenta'
        Title.Color = 14732467
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DescriTipoPago'
        Title.Caption = 'Tipo de Pago'
        Title.Color = 14732467
        Width = 304
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Vehiculo'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Title.Color = 14732467
        Width = 237
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EstadoCuenta'
        Title.Caption = 'Estado'
        Title.Color = 14732467
        Visible = True
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 710
    Height = 81
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object Label5: TLabel
      Left = 8
      Top = 20
      Width = 58
      Height = 13
      Caption = 'Documento:'
    end
    object Label7: TLabel
      Left = 8
      Top = 49
      Width = 40
      Height = 13
      Caption = 'Apellido:'
    end
    object Label3: TLabel
      Left = 312
      Top = 49
      Width = 40
      Height = 13
      Caption = 'Nombre:'
    end
    object txt_Apellido: TEdit
      Left = 70
      Top = 44
      Width = 235
      Height = 21
      MaxLength = 40
      TabOrder = 2
      OnChange = ActualizarBusqueda
    end
    object txt_Nombre: TEdit
      Left = 360
      Top = 44
      Width = 257
      Height = 21
      MaxLength = 40
      TabOrder = 3
      OnChange = ActualizarBusqueda
    end
    object btn_Buscar: TOPButton
      Left = 360
      Top = 13
      Caption = '&Buscar'
      TabOrder = 1
      OnClick = btn_BuscarClick
    end
    object cbDocumento: TMaskCombo
      Left = 72
      Top = 16
      Width = 233
      Height = 21
      OnChange = cbDocumentoChange
      Items = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Color = clWindow
      ComboWidth = 80
      ItemIndex = -1
      Enabled = False
      OmitCharacterRequired = True
      TabOrder = 0
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 415
    Width = 710
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    DesignSize = (
      710
      41)
    object btn_editar: TOPButton
      Left = 543
      Top = 8
      Anchors = [akRight, akBottom]
      Caption = '&Editar'
      Default = True
      Enabled = False
      TabOrder = 0
      OnClick = btn_editarClick
    end
    object btn_Close: TOPButton
      Left = 631
      Top = 8
      Anchors = [akRight, akBottom]
      Caption = '&Salir'
      TabOrder = 1
      OnClick = btn_CloseClick
    end
  end
  object DataSource1: TDataSource
    DataSet = ObtenerDatosCuentasCliente
    Left = 576
    Top = 203
  end
  object pop_Cuentas: TPopupMenu
    OnPopup = pop_CuentasPopup
    Left = 640
    Top = 203
    object mnu_cierre: TMenuItem
      Caption = 'Solicitar Cierre de Cuenta...'
      OnClick = mnu_cierreClick
    end
    object mnu_inh_tag: TMenuItem
      Caption = 'Inhabilitar Tag'
      OnClick = mnu_inh_tagClick
    end
    object mnu_reh_tag: TMenuItem
      Caption = 'Rehabilitar Tag'
      OnClick = mnu_reh_tagClick
    end
  end
  object BuscarClienteContacto: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'BuscarClienteContacto;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@Apellido'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@Nombre'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@CodigoCuenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@SoloClientes'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 148
    Top = 245
  end
  object ObtenerDatosCuentasCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDatosCuentasCliente;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 608
    Top = 203
  end
end
