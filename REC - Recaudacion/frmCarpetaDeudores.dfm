object FormCarpetaDeudores: TFormCarpetaDeudores
  Left = 0
  Top = 0
  Caption = 'Carpeta Deudores'
  ClientHeight = 746
  ClientWidth = 1258
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object pcCarpetaDeudores: TPageControl
    Left = 0
    Top = 0
    Width = 1258
    Height = 705
    ActivePage = tsDeudores
    Align = alClient
    TabOrder = 0
    object tsDeudores: TTabSheet
      Caption = 'Deudores'
      ImageIndex = 3
      object Panel11: TPanel
        Left = 0
        Top = 602
        Width = 1250
        Height = 75
        Align = alBottom
        TabOrder = 1
        DesignSize = (
          1250
          75)
        object Label25: TLabel
          Left = 16
          Top = 16
          Width = 133
          Height = 13
          Caption = 'Tipo de Acci'#243'n de Cobranza'
        end
        object Splitter4: TSplitter
          Left = 1
          Top = 1
          Height = 73
          ExplicitLeft = 464
          ExplicitTop = 80
          ExplicitHeight = 100
        end
        object lblAccionDeudores: TLabel
          Left = 16
          Top = 40
          Width = 18
          Height = 13
          Caption = '      '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsItalic]
          ParentFont = False
        end
        object btnSeleccionarMensajeDeudores: TButton
          Left = 380
          Top = 10
          Width = 120
          Height = 25
          Caption = 'Seleccionar Mensaje'
          TabOrder = 2
          Visible = False
          OnClick = btnSeleccionarMensajeCobInternaClick
        end
        object btnSeleccionarPlantillaDeudores: TButton
          Left = 380
          Top = 10
          Width = 120
          Height = 25
          Caption = 'Seleccionar Plantilla'
          TabOrder = 1
          Visible = False
          OnClick = btnSeleccionarPlantillaDeudoresClick
        end
        object btnEjecutarAccionDeudores: TButton
          Left = 500
          Top = 10
          Width = 155
          Height = 25
          Caption = 'Ejecutar Acci'#243'n de Cobranza'
          TabOrder = 4
          OnClick = btnEjecutarAccionDeudoresClick
        end
        object cbbTipoAccionDeudores: TVariantComboBox
          Left = 155
          Top = 12
          Width = 222
          Height = 21
          Style = vcsDropDownList
          ItemHeight = 13
          TabOrder = 0
          OnChange = cbbTipoAccionDeudoresChange
          Items = <>
        end
        object btnExportarDatosACSVDeudores: TButton
          Left = 1110
          Top = 10
          Width = 120
          Height = 25
          Anchors = [akTop, akRight]
          Caption = 'Exportar datos a CSV'
          TabOrder = 3
          OnClick = btnExportarDatosACSVDeudoresClick
        end
        object btnHistoricoDeudores: TButton
          Left = 796
          Top = 10
          Width = 153
          Height = 25
          Anchors = [akTop, akRight]
          Caption = 'Hist'#243'rico de Cobranza Externa'
          TabOrder = 5
          OnClick = btnHistoricoCobExtraJudicialClick
        end
        object btnDefinirTipoClienteDeudores: TButton
          Left = 951
          Top = 10
          Width = 153
          Height = 25
          Anchors = [akTop, akRight]
          Caption = 'Definir Tipo de Cliente'
          TabOrder = 6
          OnClick = btnDefinirTipoClienteDeudoresClick
        end
      end
      object Panel12: TPanel
        Left = 0
        Top = 570
        Width = 1250
        Height = 32
        Align = alBottom
        BevelInner = bvRaised
        BevelKind = bkFlat
        TabOrder = 2
        object btnSeleccionarTodoDeudores: TButton
          Left = 20
          Top = 2
          Width = 120
          Height = 25
          Caption = 'Seleccionar todo'
          TabOrder = 0
          OnClick = btnSeleccionarTodoDeudoresClick
        end
        object btnLiberarSeleccionDeudores: TButton
          Left = 145
          Top = 2
          Width = 120
          Height = 25
          Caption = 'Liberar selecci'#243'n'
          TabOrder = 1
          OnClick = btnLiberarSeleccionDeudoresClick
        end
        object btnHistorialDeudores: TButton
          Left = 270
          Top = 2
          Width = 120
          Height = 25
          Caption = 'Historial de cobranza'
          TabOrder = 2
          OnClick = btnHistorialDeudoresClick
        end
      end
      object dlDeudores: TDBListEx
        Left = 0
        Top = 121
        Width = 1250
        Height = 449
        Align = alClient
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 50
            Header.Caption = 'Acci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = True
            FieldName = 'Marcar'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 100
            Header.Caption = 'RUT'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'NumeroDocumento'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 200
            Header.Caption = 'Cliente'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'NombrePersona'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 110
            Header.Caption = 'Convenio'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'NumeroConvenio'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 120
            Header.Caption = 'Tipo de Cliente'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'TipoDeCliente'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 160
            Header.Caption = 'Candidato a Inhabilitaci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'Candidato'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 110
            Header.Caption = 'Monto de Deuda'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'Saldo'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 170
            Header.Caption = 'Cantidad de Comprobantes'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'CantidadComprobantes'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 120
            Header.Caption = 'Edad deuda (d'#237'as)'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'DiasDeuda'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 140
            Header.Caption = 'Fecha de Vencimiento'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'FechaVencimiento'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = #218'ltima Acci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'AccionCobranza'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 120
            Header.Caption = 'Estado de Acci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'EstadoAccionCobranza'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'Empresa de Cobranza'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'EmpresaCobranza'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 100
            Header.Caption = 'Fecha de Env'#237'o'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'FechaEnvioCobranza'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 110
            Header.Caption = 'Refinanciaci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'Refinanciacion'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'Estado de Refinanciaci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'EstadoRefinanciacion'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 135
            Header.Caption = 'Avenimiento Judicial'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'AvenimientoJudicial'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'Estado de Avenimiento'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'EstadoAvenimiento'
          end>
        DataSource = dsObtenerConveniosMorososDeudores
        DragReorder = True
        ParentColor = False
        TabOrder = 3
        TabStop = True
        OnDblClick = dlDeudoresDblClick
        OnDrawText = dlDeudoresDrawText
        OnLinkClick = dlDeudoresLinkClick
      end
      object Panel13: TPanel
        Left = 0
        Top = 0
        Width = 1250
        Height = 121
        Align = alTop
        TabOrder = 0
        ExplicitTop = -6
        DesignSize = (
          1250
          121)
        object Label28: TLabel
          Left = 24
          Top = 15
          Width = 95
          Height = 13
          Caption = 'Acci'#243'n de Cobranza'
        end
        object Label29: TLabel
          Left = 218
          Top = 15
          Width = 65
          Height = 13
          Caption = 'Monto M'#237'nimo'
        end
        object Label30: TLabel
          Left = 328
          Top = 15
          Width = 69
          Height = 13
          Caption = 'Monto M'#225'ximo'
        end
        object Label31: TLabel
          Left = 416
          Top = 15
          Width = 102
          Height = 13
          Caption = 'Edad de deuda (d'#237'as)'
        end
        object Label32: TLabel
          Left = 521
          Top = 15
          Width = 71
          Height = 13
          Caption = 'Tipo de Cliente'
        end
        object Label33: TLabel
          Left = 24
          Top = 57
          Width = 96
          Height = 13
          Caption = 'Categor'#237'a de Deuda'
        end
        object Label34: TLabel
          Left = 175
          Top = 57
          Width = 60
          Height = 13
          Caption = 'RUT Cliente:'
        end
        object Label35: TLabel
          Left = 341
          Top = 57
          Width = 104
          Height = 13
          Caption = 'N'#250'mero de Convenio:'
        end
        object lblPersonaDeudores: TLabel
          Left = 554
          Top = 76
          Width = 16
          Height = 14
          Caption = '    '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = [fsItalic]
          ParentFont = False
        end
        object vcbAccionDeCobranzaDeudores: TVariantComboBox
          Left = 24
          Top = 31
          Width = 185
          Height = 21
          Style = vcsDropDownList
          ItemHeight = 13
          TabOrder = 0
          OnChange = vcbAccionDeCobranzaDeudoresChange
          Items = <>
        end
        object chbCandidatoInhabilitacionDeudores: TCheckBox
          Left = 704
          Top = 33
          Width = 150
          Height = 17
          Caption = 'Candidato a Inhabilitaci'#243'n'
          TabOrder = 5
        end
        object peNumeroDocumentoDeudores: TPickEdit
          Left = 175
          Top = 73
          Width = 156
          Height = 21
          CharCase = ecUpperCase
          Enabled = True
          TabOrder = 7
          OnChange = peNumeroDocumentoDeudoresChange
          EditorStyle = bteTextEdit
          OnButtonClick = peNumeroDocumentoDeudoresButtonClick
        end
        object cbConveniosDeudores: TVariantComboBox
          Left = 339
          Top = 73
          Width = 204
          Height = 19
          Style = vcsOwnerDrawFixed
          ItemHeight = 13
          TabOrder = 8
          Items = <>
        end
        object btnLimpiarCobDeudores: TButton
          Left = 1009
          Top = 29
          Width = 75
          Height = 25
          Anchors = [akTop, akRight]
          Caption = 'Limpiar'
          TabOrder = 11
          OnClick = btnLimpiarCobDeudoresClick
        end
        object btnBuscarDeudores: TButton
          Left = 1090
          Top = 29
          Width = 75
          Height = 25
          Anchors = [akTop, akRight]
          Caption = 'Buscar'
          Default = True
          TabOrder = 12
          OnClick = btnBuscarDeudoresClick
        end
        object edMontoMinimoDeudaDeudores: TNumericEdit
          Left = 218
          Top = 31
          Width = 90
          Height = 21
          MaxLength = 18
          TabOrder = 1
          Decimals = 2
        end
        object edEdadDeudaDeudores: TNumericEdit
          Left = 416
          Top = 31
          Width = 90
          Height = 21
          TabOrder = 3
        end
        object edMontoMaximoDeudaDeudores: TNumericEdit
          Left = 314
          Top = 31
          Width = 90
          Height = 21
          MaxLength = 18
          TabOrder = 2
          Decimals = 2
        end
        object vcbTipoClienteDeudores: TVariantComboBox
          Left = 524
          Top = 31
          Width = 165
          Height = 21
          Style = vcsDropDownList
          ItemHeight = 13
          TabOrder = 4
          Items = <>
        end
        object vcbCategoriaDeudaDeudores: TVariantComboBox
          Left = 24
          Top = 73
          Width = 145
          Height = 21
          Style = vcsDropDownList
          ItemHeight = 13
          TabOrder = 6
          Items = <
            item
              Caption = 'Todas'
              Value = '0'
            end
            item
              Caption = 'Urbana'
              Value = '1'
            end
            item
              Caption = 'Interurbana'
              Value = '2'
            end>
        end
        object rbMostrarTodo: TRadioButton
          Left = 24
          Top = 98
          Width = 206
          Height = 17
          Caption = 'Mostrar todos los convenios deudores'
          TabOrder = 9
        end
        object rbMostrarSinAcciones: TRadioButton
          Left = 236
          Top = 98
          Width = 241
          Height = 17
          Caption = 'Mostrar Convenios sin acciones de cobranza'
          TabOrder = 10
        end
      end
    end
    object tsCobranzaInterna: TTabSheet
      Caption = 'Cobranza Interna'
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 1250
        Height = 105
        Align = alTop
        TabOrder = 0
        DesignSize = (
          1250
          105)
        object Label1: TLabel
          Left = 24
          Top = 15
          Width = 95
          Height = 13
          Caption = 'Acci'#243'n de Cobranza'
        end
        object Label2: TLabel
          Left = 218
          Top = 15
          Width = 65
          Height = 13
          Caption = 'Monto M'#237'nimo'
        end
        object Label3: TLabel
          Left = 328
          Top = 15
          Width = 69
          Height = 13
          Caption = 'Monto M'#225'ximo'
        end
        object Label4: TLabel
          Left = 416
          Top = 15
          Width = 102
          Height = 13
          Caption = 'Edad de deuda (d'#237'as)'
        end
        object Label5: TLabel
          Left = 521
          Top = 15
          Width = 71
          Height = 13
          Caption = 'Tipo de Cliente'
        end
        object Label6: TLabel
          Left = 24
          Top = 57
          Width = 96
          Height = 13
          Caption = 'Categor'#237'a de Deuda'
        end
        object lb_CodigoCliente: TLabel
          Left = 175
          Top = 57
          Width = 60
          Height = 13
          Caption = 'RUT Cliente:'
        end
        object lbl_NumeroConvenio: TLabel
          Left = 341
          Top = 57
          Width = 104
          Height = 13
          Caption = 'N'#250'mero de Convenio:'
        end
        object lblPersonaCobInterna: TLabel
          Left = 554
          Top = 76
          Width = 16
          Height = 14
          Caption = '    '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = [fsItalic]
          ParentFont = False
        end
        object vcbAccionDeCobranzaInterna: TVariantComboBox
          Left = 24
          Top = 31
          Width = 185
          Height = 21
          Style = vcsDropDownList
          ItemHeight = 13
          TabOrder = 0
          OnChange = vcbAccionDeCobranzaInternaChange
          Items = <>
        end
        object chbCandidatoInhabilitacionCobInterna: TCheckBox
          Left = 704
          Top = 33
          Width = 150
          Height = 17
          Caption = 'Candidato a Inhabilitaci'#243'n'
          TabOrder = 5
        end
        object peNumeroDocumentoCobInterna: TPickEdit
          Left = 175
          Top = 73
          Width = 156
          Height = 21
          CharCase = ecUpperCase
          Enabled = True
          TabOrder = 7
          OnChange = peNumeroDocumentoCobInternaChange
          EditorStyle = bteTextEdit
          OnButtonClick = peNumeroDocumentoCobInternaButtonClick
        end
        object cbConveniosCobInterna: TVariantComboBox
          Left = 339
          Top = 73
          Width = 204
          Height = 19
          Style = vcsOwnerDrawFixed
          ItemHeight = 13
          TabOrder = 8
          Items = <>
        end
        object btnLimpiarCobInterna: TButton
          Left = 1009
          Top = 29
          Width = 75
          Height = 25
          Anchors = [akTop, akRight]
          Caption = 'Limpiar'
          TabOrder = 9
          OnClick = btnLimpiarCobInternaClick
        end
        object btnBuscarCobInterna: TButton
          Left = 1090
          Top = 29
          Width = 75
          Height = 25
          Anchors = [akTop, akRight]
          Caption = 'Buscar'
          Default = True
          TabOrder = 10
          OnClick = btnBuscarCobInternaClick
        end
        object edMontoMinimoDeudaCobInterna: TNumericEdit
          Left = 218
          Top = 31
          Width = 90
          Height = 21
          MaxLength = 18
          TabOrder = 1
          Decimals = 2
        end
        object edEdadDeudaCobInterna: TNumericEdit
          Left = 416
          Top = 31
          Width = 90
          Height = 21
          TabOrder = 3
        end
        object edMontoMaximoDeudaCobInterna: TNumericEdit
          Left = 314
          Top = 31
          Width = 90
          Height = 21
          MaxLength = 18
          TabOrder = 2
          Decimals = 2
        end
        object vcbTipoClienteCobInterna: TVariantComboBox
          Left = 524
          Top = 31
          Width = 165
          Height = 21
          Style = vcsDropDownList
          ItemHeight = 13
          TabOrder = 4
          Items = <>
        end
        object vcbCategoriaDeudaCobInterna: TVariantComboBox
          Left = 24
          Top = 73
          Width = 145
          Height = 21
          Style = vcsDropDownList
          ItemHeight = 13
          TabOrder = 6
          Items = <
            item
              Caption = 'Todas'
              Value = '0'
            end
            item
              Caption = 'Urbana'
              Value = '1'
            end
            item
              Caption = 'Interurbana'
              Value = '2'
            end>
        end
      end
      object Panel1: TPanel
        Left = 0
        Top = 602
        Width = 1250
        Height = 75
        Align = alBottom
        TabOrder = 1
        DesignSize = (
          1250
          75)
        object Label23: TLabel
          Left = 16
          Top = 16
          Width = 133
          Height = 13
          Caption = 'Tipo de Acci'#243'n de Cobranza'
        end
        object Splitter1: TSplitter
          Left = 1
          Top = 1
          Height = 73
          ExplicitLeft = 464
          ExplicitTop = 80
          ExplicitHeight = 100
        end
        object lblAccionCobranzaInterna: TLabel
          Left = 16
          Top = 40
          Width = 12
          Height = 13
          Caption = '    '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsItalic]
          ParentFont = False
        end
        object btnSeleccionarMensajeCobInterna: TButton
          Left = 380
          Top = 10
          Width = 120
          Height = 25
          Caption = 'Seleccionar Mensaje'
          TabOrder = 2
          Visible = False
          OnClick = btnSeleccionarMensajeCobInternaClick
        end
        object btnSeleccionarPlantillaCobInterna: TButton
          Left = 380
          Top = 10
          Width = 120
          Height = 25
          Caption = 'Seleccionar Plantilla'
          TabOrder = 1
          Visible = False
          OnClick = btnSeleccionarPlantillaCobInternaClick
        end
        object btnEjecutarAccionCobInterna: TButton
          Left = 500
          Top = 10
          Width = 155
          Height = 25
          Caption = 'Ejecutar Acci'#243'n de Cobranza'
          TabOrder = 3
          OnClick = btnEjecutarAccionCobInternaClick
        end
        object cbbTipoAccionCobranzaInterna: TVariantComboBox
          Left = 155
          Top = 12
          Width = 222
          Height = 21
          Style = vcsDropDownList
          ItemHeight = 13
          TabOrder = 0
          OnChange = cbbTipoAccionCobranzaInternaChange
          Items = <>
        end
        object btnExportarDatosACSVCobInterna: TButton
          Left = 1110
          Top = 10
          Width = 120
          Height = 25
          Anchors = [akTop, akRight]
          Caption = 'Exportar datos a CSV'
          TabOrder = 4
          OnClick = btnExportarDatosACSVCobInternaClick
        end
        object btnHistoricoCobInterna: TButton
          Left = 796
          Top = 10
          Width = 153
          Height = 25
          Anchors = [akTop, akRight]
          Caption = 'Hist'#243'rico de Cobranza Externa'
          TabOrder = 5
          OnClick = btnHistoricoCobExtraJudicialClick
        end
        object btnDefinirTipoClienteCobInterna: TButton
          Left = 951
          Top = 10
          Width = 153
          Height = 25
          Anchors = [akTop, akRight]
          Caption = 'Definir Tipo de Cliente'
          TabOrder = 6
          OnClick = btnDefinirTipoClienteCobInternaClick
        end
      end
      object dlMorososCobInterna: TDBListEx
        Left = 0
        Top = 105
        Width = 1250
        Height = 465
        Align = alClient
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 50
            Header.Caption = 'Acci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = True
            FieldName = 'Marcar'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 100
            Header.Caption = 'RUT'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'NumeroDocumento'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 200
            Header.Caption = 'Cliente'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'NombrePersona'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 110
            Header.Caption = 'Convenio'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'NumeroConvenio'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 120
            Header.Caption = 'Tipo de Cliente'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'TipoCliente'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 160
            Header.Caption = 'Candidato a Inhabilitaci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'Candidato'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 110
            Header.Caption = 'Monto de Deuda'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'Saldo'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 170
            Header.Caption = 'Cantidad de Comprobantes'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'CantComprobantes'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'Edad de la deuda en d'#237'as'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'DiasDeuda'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 140
            Header.Caption = 'Fecha de Vencimiento'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'FechaVencimiento'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = #218'ltima Acci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'AccionCobranza'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 120
            Header.Caption = 'Estado de Acci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'EstadoAccionCobranza'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'Empresa de Cobranza'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 100
            Header.Caption = 'Fecha de Env'#237'o'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 110
            Header.Caption = 'Refinanciaci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'Estado de Refinanciaci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 135
            Header.Caption = 'Avenimiento Judicial'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'Estado de Avenimiento'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
          end>
        DataSource = dsObtenerConveniosMorososCobInterna
        DragReorder = True
        ParentColor = False
        TabOrder = 2
        TabStop = True
        OnDblClick = dlMorososCobInternaDblClick
        OnDrawText = dlMorososCobInternaDrawText
        OnLinkClick = dlMorososCobInternaLinkClick
        ExplicitLeft = 168
        ExplicitTop = 101
      end
      object Panel7: TPanel
        Left = 0
        Top = 570
        Width = 1250
        Height = 32
        Align = alBottom
        BevelInner = bvRaised
        BevelKind = bkFlat
        TabOrder = 3
        object btnSeleccionarTodoCobInterna: TButton
          Left = 20
          Top = 2
          Width = 120
          Height = 25
          Caption = 'Seleccionar todo'
          TabOrder = 0
          OnClick = btnSeleccionarTodoCobInternaClick
        end
        object btnLiberarSeleccionCobInterna: TButton
          Left = 145
          Top = 2
          Width = 120
          Height = 25
          Caption = 'Liberar selecci'#243'n'
          TabOrder = 1
          OnClick = btnLiberarSeleccionCobInternaClick
        end
        object btnHistorialCobInterna: TButton
          Left = 270
          Top = 2
          Width = 120
          Height = 25
          Caption = 'Historial de cobranza'
          TabOrder = 2
          OnClick = btnHistorialCobInternaClick
        end
      end
    end
    object tsCobranzaExternaExtraJudicial: TTabSheet
      Caption = 'Cobranza Externa PreJudicial'
      ImageIndex = 1
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 1250
        Height = 105
        Align = alTop
        TabOrder = 0
        DesignSize = (
          1250
          105)
        object Label7: TLabel
          Left = 24
          Top = 15
          Width = 95
          Height = 13
          Caption = 'Acci'#243'n de Cobranza'
        end
        object Label8: TLabel
          Left = 218
          Top = 15
          Width = 65
          Height = 13
          Caption = 'Monto M'#237'nimo'
        end
        object Label9: TLabel
          Left = 328
          Top = 15
          Width = 69
          Height = 13
          Caption = 'Monto M'#225'ximo'
        end
        object Label10: TLabel
          Left = 416
          Top = 15
          Width = 102
          Height = 13
          Caption = 'Edad de deuda (d'#237'as)'
        end
        object Label11: TLabel
          Left = 527
          Top = 15
          Width = 71
          Height = 13
          Caption = 'Tipo de Cliente'
        end
        object Label12: TLabel
          Left = 24
          Top = 57
          Width = 96
          Height = 13
          Caption = 'Categor'#237'a de Deuda'
        end
        object Label13: TLabel
          Left = 178
          Top = 57
          Width = 60
          Height = 13
          Caption = 'RUT Cliente:'
        end
        object Label14: TLabel
          Left = 344
          Top = 57
          Width = 104
          Height = 13
          Caption = 'N'#250'mero de Convenio:'
        end
        object lblPersonaCobExtraJudicial: TLabel
          Left = 554
          Top = 76
          Width = 16
          Height = 14
          Caption = '    '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = [fsItalic]
          ParentFont = False
        end
        object vcbAccionDeCobranzaExtraJudicial: TVariantComboBox
          Left = 24
          Top = 31
          Width = 185
          Height = 21
          Style = vcsDropDownList
          ItemHeight = 13
          TabOrder = 0
          OnChange = vcbAccionDeCobranzaExtraJudicialChange
          Items = <>
        end
        object chbCandidatoInhabilitacionCobExtraJudicial: TCheckBox
          Left = 704
          Top = 33
          Width = 150
          Height = 17
          Caption = 'Candidato a Inhabilitaci'#243'n'
          TabOrder = 5
        end
        object peNumeroDocumentoCobExtraJudicial: TPickEdit
          Left = 175
          Top = 73
          Width = 156
          Height = 21
          CharCase = ecUpperCase
          Enabled = True
          TabOrder = 7
          OnChange = peNumeroDocumentoCobExtraJudicialChange
          EditorStyle = bteTextEdit
          OnButtonClick = peNumeroDocumentoCobExtraJudicialButtonClick
        end
        object cbConveniosCobExtraJudicial: TVariantComboBox
          Left = 342
          Top = 74
          Width = 204
          Height = 19
          Style = vcsOwnerDrawFixed
          ItemHeight = 13
          TabOrder = 8
          Items = <>
        end
        object btnLimpiarCobExtraJudicial: TButton
          Left = 1009
          Top = 29
          Width = 75
          Height = 25
          Anchors = [akTop, akRight]
          Caption = 'Limpiar'
          TabOrder = 9
          OnClick = btnLimpiarCobExtraJudicialClick
        end
        object btnBuscarCobExtraJudicial: TButton
          Left = 1090
          Top = 29
          Width = 75
          Height = 25
          Anchors = [akTop, akRight]
          Caption = 'Buscar'
          Default = True
          TabOrder = 10
          OnClick = btnBuscarCobExtraJudicialClick
        end
        object edMontoMinimoDeudaCobExtraJudicial: TNumericEdit
          Left = 218
          Top = 31
          Width = 90
          Height = 21
          MaxLength = 18
          TabOrder = 1
          Decimals = 2
        end
        object edEdadDeudaCobExtraJudicial: TNumericEdit
          Left = 416
          Top = 31
          Width = 99
          Height = 21
          TabOrder = 3
        end
        object edMontoMaximoDeudaCobExtraJudicial: TNumericEdit
          Left = 314
          Top = 31
          Width = 90
          Height = 21
          MaxLength = 18
          TabOrder = 2
          Decimals = 2
        end
        object cbTipoClienteCobExtraJudicial: TVariantComboBox
          Left = 527
          Top = 31
          Width = 165
          Height = 21
          Style = vcsDropDownList
          ItemHeight = 13
          TabOrder = 4
          Items = <>
        end
        object vcbCategoriaDeudaCobExtraJudicial: TVariantComboBox
          Left = 24
          Top = 73
          Width = 145
          Height = 21
          Style = vcsDropDownList
          ItemHeight = 13
          TabOrder = 6
          Items = <
            item
              Caption = 'Todas'
              Value = '0'
            end
            item
              Caption = 'Urbana'
              Value = '1'
            end
            item
              Caption = 'Interurbana'
              Value = '2'
            end>
        end
      end
      object dlMorososCobExtraJudicial: TDBListEx
        Left = 0
        Top = 105
        Width = 1250
        Height = 465
        Align = alClient
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 50
            Header.Caption = 'Acci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = True
            FieldName = 'Accion'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 100
            Header.Caption = 'RUT'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'NumeroDocumento'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 200
            Header.Caption = 'Cliente'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'NombrePersona'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 110
            Header.Caption = 'Convenio'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'NumeroConvenio'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 120
            Header.Caption = 'Tipo de Cliente'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'TipoCliente'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 160
            Header.Caption = 'Candidato a Inhabilitaci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'Candidato'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 110
            Header.Caption = 'Monto de Deuda'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'Saldo'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 170
            Header.Caption = 'Cantidad de Comprobantes'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'CantComprobantes'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'Edad de la deuda en d'#237'as'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'DiasDeuda'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 140
            Header.Caption = 'Fecha de Vencimiento'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'FechaVencimiento'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = #218'ltima Acci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'AccionCobranza'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 120
            Header.Caption = 'Estado de Acci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'EstadoAccionCobranza'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'Empresa de Cobranza'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 100
            Header.Caption = 'Fecha de Env'#237'o'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 110
            Header.Caption = 'Refinanciaci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'Estado de Refinanciaci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 135
            Header.Caption = 'Avenimiento Judicial'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'Estado de Avenimiento'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
          end>
        DataSource = dsObtenerConveniosMorososCobExtraJudicial
        DragReorder = True
        ParentColor = False
        TabOrder = 1
        TabStop = True
        OnDblClick = dlMorososCobExtraJudicialDblClick
        OnDrawText = dlMorososCobExtraJudicialDrawText
        OnLinkClick = dlMorososCobExtraJudicialLinkClick
      end
      object Panel8: TPanel
        Left = 0
        Top = 570
        Width = 1250
        Height = 32
        Align = alBottom
        BevelInner = bvRaised
        BevelKind = bkFlat
        TabOrder = 2
        object btnSeleccionarTodoCobExtraJudicial: TButton
          Left = 20
          Top = 2
          Width = 120
          Height = 25
          Caption = 'Seleccionar todo'
          TabOrder = 0
          OnClick = btnSeleccionarTodoCobExtraJudicialClick
        end
        object btnLiberarSeleccionCobExtraJudicial: TButton
          Left = 145
          Top = 2
          Width = 120
          Height = 25
          Caption = 'Liberar selecci'#243'n'
          TabOrder = 1
          OnClick = btnLiberarSeleccionCobExtraJudicialClick
        end
        object btnHistorialCobExtraJudicial: TButton
          Left = 270
          Top = 2
          Width = 120
          Height = 25
          Caption = 'Historial de cobranza'
          TabOrder = 2
          OnClick = btnHistorialCobExtraJudicialClick
        end
      end
      object Panel4: TPanel
        Left = 0
        Top = 602
        Width = 1250
        Height = 75
        Align = alBottom
        TabOrder = 3
        DesignSize = (
          1250
          75)
        object Label24: TLabel
          Left = 16
          Top = 16
          Width = 133
          Height = 13
          Caption = 'Tipo de Acci'#243'n de Cobranza'
        end
        object Splitter2: TSplitter
          Left = 1
          Top = 1
          Height = 73
          ExplicitLeft = 464
          ExplicitTop = 80
          ExplicitHeight = 100
        end
        object lblAccionCobranzaExtraJudicial: TLabel
          Left = 16
          Top = 40
          Width = 12
          Height = 13
          Caption = '    '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsItalic]
          ParentFont = False
        end
        object btnEjecutarAccionCobExtraJudicial: TButton
          Left = 500
          Top = 10
          Width = 155
          Height = 25
          Caption = 'Ejecutar Acci'#243'n de Cobranza'
          TabOrder = 1
          OnClick = btnEjecutarAccionCobExtraJudicialClick
        end
        object cbbTipoAccionCobranzaExtraJudicial: TVariantComboBox
          Left = 155
          Top = 12
          Width = 222
          Height = 21
          Style = vcsDropDownList
          ItemHeight = 13
          TabOrder = 0
          OnChange = cbbTipoAccionCobranzaExtraJudicialChange
          Items = <>
        end
        object btnExportarDatosACSVCobExtraJudicial: TButton
          Left = 1110
          Top = 10
          Width = 120
          Height = 25
          Anchors = [akTop, akRight]
          Caption = 'Exportar datos a CSV'
          TabOrder = 2
          OnClick = btnExportarDatosACSVCobExtraJudicialClick
        end
        object btnHistoricoCobExtraJudicial: TButton
          Left = 796
          Top = 10
          Width = 153
          Height = 25
          Anchors = [akTop, akRight]
          Caption = 'Hist'#243'rico de Cobranza Externa'
          TabOrder = 3
          OnClick = btnHistoricoCobExtraJudicialClick
        end
        object btnDefinirTipoClienteCobExtraJudicial: TButton
          Left = 951
          Top = 10
          Width = 153
          Height = 25
          Anchors = [akTop, akRight]
          Caption = 'Definir Tipo de Cliente'
          TabOrder = 4
          OnClick = btnDefinirTipoClienteCobExtraJudicialClick
        end
        object btnSeleccionarMensajeCobExtraJudicial: TButton
          Left = 380
          Top = 10
          Width = 120
          Height = 25
          Caption = 'Seleccionar Mensaje'
          TabOrder = 5
          Visible = False
          OnClick = btnSeleccionarMensajeCobInternaClick
        end
        object btnSeleccionarPlantillaCobExtraJudicial: TButton
          Left = 380
          Top = 10
          Width = 120
          Height = 25
          Caption = 'Seleccionar Plantilla'
          TabOrder = 6
          Visible = False
          OnClick = btnSeleccionarPlantillaCobInternaClick
        end
      end
    end
    object tsCobranzaExternaJudicial: TTabSheet
      Caption = 'Cobranza Externa Judicial'
      ImageIndex = 2
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 1250
        Height = 105
        Align = alTop
        TabOrder = 0
        DesignSize = (
          1250
          105)
        object Label15: TLabel
          Left = 24
          Top = 15
          Width = 95
          Height = 13
          Caption = 'Acci'#243'n de Cobranza'
        end
        object Label16: TLabel
          Left = 218
          Top = 15
          Width = 65
          Height = 13
          Caption = 'Monto M'#237'nimo'
        end
        object Label17: TLabel
          Left = 328
          Top = 15
          Width = 69
          Height = 13
          Caption = 'Monto M'#225'ximo'
        end
        object Label18: TLabel
          Left = 416
          Top = 15
          Width = 102
          Height = 13
          Caption = 'Edad de deuda (d'#237'as)'
        end
        object Label19: TLabel
          Left = 521
          Top = 15
          Width = 71
          Height = 13
          Caption = 'Tipo de Cliente'
        end
        object Label20: TLabel
          Left = 24
          Top = 57
          Width = 96
          Height = 13
          Caption = 'Categor'#237'a de Deuda'
        end
        object Label21: TLabel
          Left = 175
          Top = 57
          Width = 60
          Height = 13
          Caption = 'RUT Cliente:'
        end
        object Label22: TLabel
          Left = 341
          Top = 57
          Width = 104
          Height = 13
          Caption = 'N'#250'mero de Convenio:'
        end
        object lblPersonaCobJudicial: TLabel
          Left = 554
          Top = 75
          Width = 16
          Height = 14
          Caption = '    '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = [fsItalic]
          ParentFont = False
        end
        object vcbAccionDeCobranzaJudicial: TVariantComboBox
          Left = 24
          Top = 31
          Width = 185
          Height = 21
          Style = vcsDropDownList
          ItemHeight = 13
          TabOrder = 0
          OnChange = vcbAccionDeCobranzaJudicialChange
          Items = <>
        end
        object chbCandidatoInhabilitacionCobJudicial: TCheckBox
          Left = 704
          Top = 33
          Width = 150
          Height = 17
          Caption = 'Candidato a Inhabilitaci'#243'n'
          TabOrder = 5
        end
        object peNumeroDocumentoCobJudicial: TPickEdit
          Left = 175
          Top = 72
          Width = 156
          Height = 21
          CharCase = ecUpperCase
          Enabled = True
          TabOrder = 7
          OnChange = peNumeroDocumentoCobJudicialChange
          EditorStyle = bteTextEdit
          OnButtonClick = peNumeroDocumentoCobJudicialButtonClick
        end
        object cbConveniosCobJudicial: TVariantComboBox
          Left = 339
          Top = 73
          Width = 204
          Height = 19
          Style = vcsOwnerDrawFixed
          ItemHeight = 13
          TabOrder = 8
          Items = <>
        end
        object btnLimpiarCobJudicial: TButton
          Left = 1009
          Top = 29
          Width = 75
          Height = 25
          Anchors = [akTop, akRight]
          Caption = 'Limpiar'
          TabOrder = 9
          OnClick = btnLimpiarCobJudicialClick
        end
        object btnBuscarCobJudicial: TButton
          Left = 1090
          Top = 29
          Width = 75
          Height = 25
          Anchors = [akTop, akRight]
          Caption = 'Buscar'
          Default = True
          TabOrder = 10
          OnClick = btnBuscarCobJudicialClick
        end
        object edMontoMinimoDeudaCobJudicial: TNumericEdit
          Left = 217
          Top = 31
          Width = 90
          Height = 21
          MaxLength = 18
          TabOrder = 1
          Decimals = 2
        end
        object edEdadDeudaCobJudicial: TNumericEdit
          Left = 416
          Top = 31
          Width = 99
          Height = 21
          TabOrder = 3
        end
        object edMontoMaximoDeudaCobJudicial: TNumericEdit
          Left = 317
          Top = 31
          Width = 90
          Height = 21
          MaxLength = 18
          TabOrder = 2
          Decimals = 2
        end
        object cbTipoClienteCobJudicial: TVariantComboBox
          Left = 524
          Top = 31
          Width = 165
          Height = 21
          Style = vcsDropDownList
          ItemHeight = 13
          TabOrder = 4
          Items = <>
        end
        object vcbCategoriaDeudaCobJudicial: TVariantComboBox
          Left = 24
          Top = 72
          Width = 145
          Height = 21
          Style = vcsDropDownList
          ItemHeight = 13
          TabOrder = 6
          Items = <
            item
              Caption = 'Todas'
              Value = '0'
            end
            item
              Caption = 'Urbana'
              Value = '1'
            end
            item
              Caption = 'Interurbana'
              Value = '2'
            end>
        end
      end
      object dlMorososCobJudicial: TDBListEx
        Left = 0
        Top = 105
        Width = 1250
        Height = 464
        Align = alClient
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 50
            Header.Caption = 'Acci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = True
            FieldName = 'Accion'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 100
            Header.Caption = 'RUT'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'NumeroDocumento'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 200
            Header.Caption = 'Cliente'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'NombrePersona'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 110
            Header.Caption = 'Convenio'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'NumeroConvenio'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 120
            Header.Caption = 'Tipo de Cliente'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'TipoCliente'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 160
            Header.Caption = 'Candidato a Inhabilitaci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'Candidato'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 110
            Header.Caption = 'Monto de Deuda'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'Saldo'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 170
            Header.Caption = 'Cantidad de Comprobantes'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'CantComprobantes'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'Edad de la deuda en d'#237'as'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'DiasDeuda'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 140
            Header.Caption = 'Fecha de Vencimiento'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'FechaVencimiento'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = #218'ltima Acci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'AccionCobranza'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 120
            Header.Caption = 'Estado de Acci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'EstadoAccionCobranza'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'Empresa de Cobranza'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 100
            Header.Caption = 'Fecha de Env'#237'o'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 110
            Header.Caption = 'Refinanciaci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'Estado de Refinanciaci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 135
            Header.Caption = 'Avenimiento Judicial'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'Estado de Avenimiento'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
          end>
        DataSource = dsObtenerConveniosMorososCobJudicial
        DragReorder = True
        ParentColor = False
        TabOrder = 1
        TabStop = True
        OnDblClick = dlMorososCobJudicialDblClick
        OnDrawText = dlMorososCobJudicialDrawText
        OnLinkClick = dlMorososCobJudicialLinkClick
      end
      object Panel9: TPanel
        Left = 0
        Top = 569
        Width = 1250
        Height = 33
        Align = alBottom
        BevelInner = bvRaised
        BevelKind = bkFlat
        TabOrder = 2
        object btnSeleccionarTodoCobJudicial: TButton
          Left = 20
          Top = 2
          Width = 120
          Height = 25
          Caption = 'Seleccionar todo'
          TabOrder = 0
          OnClick = btnSeleccionarTodoCobJudicialClick
        end
        object btnLiberarSeleccionCobJudicial: TButton
          Left = 145
          Top = 2
          Width = 120
          Height = 25
          Caption = 'Liberar selecci'#243'n'
          TabOrder = 1
          OnClick = btnLiberarSeleccionCobJudicialClick
        end
        object btnHistorialCobJudicial: TButton
          Left = 270
          Top = 2
          Width = 120
          Height = 25
          Caption = 'Historial de cobranza'
          TabOrder = 2
          OnClick = btnHistorialCobJudicialClick
        end
      end
      object Panel6: TPanel
        Left = 0
        Top = 602
        Width = 1250
        Height = 75
        Align = alBottom
        TabOrder = 3
        DesignSize = (
          1250
          75)
        object Label26: TLabel
          Left = 16
          Top = 16
          Width = 133
          Height = 13
          Caption = 'Tipo de Acci'#243'n de Cobranza'
        end
        object Splitter3: TSplitter
          Left = 1
          Top = 1
          Height = 73
          ExplicitLeft = 464
          ExplicitTop = 80
          ExplicitHeight = 100
        end
        object lblAccionCobranzaJudicial: TLabel
          Left = 16
          Top = 40
          Width = 12
          Height = 13
          Caption = '    '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsItalic]
          ParentFont = False
        end
        object btnEjecutarAccionCobJudicial: TButton
          Left = 500
          Top = 10
          Width = 155
          Height = 25
          Caption = 'Ejecutar Acci'#243'n de Cobranza'
          TabOrder = 1
          OnClick = btnEjecutarAccionCobJudicialClick
        end
        object cbbTipoAccionCobranzaJudicial: TVariantComboBox
          Left = 155
          Top = 12
          Width = 222
          Height = 21
          Style = vcsDropDownList
          ItemHeight = 13
          TabOrder = 0
          OnChange = cbbTipoAccionCobranzaJudicialChange
          Items = <>
        end
        object btnHistoricoCobJudicial: TButton
          Left = 796
          Top = 10
          Width = 153
          Height = 25
          Anchors = [akTop, akRight]
          Caption = 'Hist'#243'rico de Cobranza Externa'
          TabOrder = 2
          OnClick = btnHistoricoCobExtraJudicialClick
        end
        object btnDefinirTipoClienteCobJudicial: TButton
          Left = 951
          Top = 10
          Width = 153
          Height = 25
          Anchors = [akTop, akRight]
          Caption = 'Definir Tipo de Cliente'
          TabOrder = 3
          OnClick = btnDefinirTipoClienteCobJudicialClick
        end
        object btnExportarDatosACSVCobJudicial: TButton
          Left = 1110
          Top = 10
          Width = 120
          Height = 25
          Anchors = [akTop, akRight]
          Caption = 'Exportar datos a CSV'
          TabOrder = 4
          OnClick = btnExportarDatosACSVCobJudicialClick
        end
        object btnSeleccionarMensajeCobJudicial: TButton
          Left = 380
          Top = 10
          Width = 120
          Height = 25
          Caption = 'Seleccionar Mensaje'
          TabOrder = 5
          Visible = False
          OnClick = btnSeleccionarMensajeCobInternaClick
        end
        object btnSeleccionarPlantillaCobJudicial: TButton
          Left = 380
          Top = 10
          Width = 120
          Height = 25
          Caption = 'Seleccionar Plantilla'
          TabOrder = 6
          Visible = False
          OnClick = btnSeleccionarPlantillaCobInternaClick
        end
      end
    end
  end
  object Panel10: TPanel
    Left = 0
    Top = 705
    Width = 1258
    Height = 41
    Align = alBottom
    TabOrder = 1
    DesignSize = (
      1258
      41)
    object btnSalir: TButton
      Left = 1159
      Top = 7
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Salir'
      TabOrder = 0
      OnClick = btnSalirClick
    end
  end
  object spObtenerConveniosMorososCobInterna: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CommandTimeout = 90
    ProcedureName = 'ObtenerConveniosMorososEnCobranzaInterna'
    Parameters = <>
    Left = 328
    Top = 208
  end
  object cdsObtenerConveniosMorososCobInterna: TClientDataSet
    Aggregates = <>
    Params = <>
    ReadOnly = True
    Left = 328
    Top = 248
    object cdsObtenerConveniosMorososCobInternaMarcar: TBooleanField
      FieldName = 'Marcar'
    end
    object cdsObtenerConveniosMorososCobInternaNumeroDocumento: TStringField
      FieldName = 'NumeroDocumento'
    end
    object cdsObtenerConveniosMorososCobInternaNombrePersona: TStringField
      DisplayWidth = 1000
      FieldName = 'NombrePersona'
      Size = 200
    end
    object cdsObtenerConveniosMorososCobInternaNumeroConvenio: TStringField
      DisplayWidth = 100
      FieldName = 'NumeroConvenio'
      Size = 30
    end
    object cdsObtenerConveniosMorososCobInternaTipoCliente: TStringField
      DisplayWidth = 100
      FieldName = 'TipoCliente'
      Size = 30
    end
    object cdsObtenerConveniosMorososCobInternaConcesionaria: TStringField
      DisplayWidth = 100
      FieldName = 'Candidato'
    end
    object cdsObtenerConveniosMorososCobInternaSaldo: TLargeintField
      FieldName = 'Saldo'
    end
    object cdsObtenerConveniosMorososCobInternaCantComprobantes: TIntegerField
      FieldName = 'CantComprobantes'
    end
    object cdsObtenerConveniosMorososCobInternaDiasDeuda: TIntegerField
      FieldName = 'DiasDeuda'
    end
    object cdsObtenerConveniosMorososCobInternaFechaVencimiento: TDateField
      DisplayWidth = 20
      FieldName = 'FechaVencimiento'
    end
    object cdsObtenerConveniosMorososCobInternaCodigoCliente: TIntegerField
      DisplayWidth = 50
      FieldName = 'CodigoCliente'
    end
    object cdsObtenerConveniosMorososCobInternaCodigoConvenio: TIntegerField
      DisplayWidth = 50
      FieldName = 'CodigoConvenio'
    end
    object cdsObtenerConveniosMorososCobInternaAccionCobranza: TStringField
      DisplayWidth = 100
      FieldName = 'AccionCobranza'
      Size = 100
    end
    object cdsObtenerConveniosMorososCobInternaEstadoAccionCobranza: TStringField
      DisplayWidth = 100
      FieldName = 'EstadoAccionCobranza'
      Size = 100
    end
  end
  object dsObtenerConveniosMorososCobInterna: TDataSource
    DataSet = cdsObtenerConveniosMorososCobInterna
    Left = 328
    Top = 300
  end
  object spObtenerTiposDeCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerTiposDeCliente'
    Parameters = <>
    Left = 248
    Top = 160
  end
  object spObtenerConveniosMorososCobExtraJudicial: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerConveniosMorososConEstado'
    Parameters = <>
    Left = 568
    Top = 224
  end
  object cdsObtenerConveniosMorososCobExtraJudicial: TClientDataSet
    Aggregates = <>
    Params = <>
    ReadOnly = True
    Left = 568
    Top = 264
    object cdsObtenerConveniosMorososCobExtraJudicialMarcar: TBooleanField
      FieldName = 'Accion'
    end
    object cdsObtenerConveniosMorososCobExtraJudicialNumeroDocumento: TStringField
      FieldName = 'NumeroDocumento'
    end
    object cdsObtenerConveniosMorososCobExtraJudicialNombrePersona: TStringField
      DisplayWidth = 200
      FieldName = 'NombrePersona'
      Size = 200
    end
    object cdsObtenerConveniosMorososCobExtraJudicialNumeroConvenio: TStringField
      DisplayWidth = 30
      FieldName = 'NumeroConvenio'
      Size = 30
    end
    object cdsObtenerConveniosMorososCobExtraJudicialTipoCliente: TStringField
      DisplayWidth = 50
      FieldName = 'TipoCliente'
      Size = 50
    end
    object cdsObtenerConveniosMorososCobExtraJudicialConcesionaria: TStringField
      FieldName = 'Candidato'
    end
    object cdsObtenerConveniosMorososCobExtraJudicialSaldo: TLargeintField
      FieldName = 'Saldo'
    end
    object cdsObtenerConveniosMorososCobExtraJudicialCantComprobantes: TIntegerField
      FieldName = 'CantComprobantes'
    end
    object cdsObtenerConveniosMorososCobExtraJudicialDiasDeuda: TIntegerField
      FieldName = 'DiasDeuda'
    end
    object cdsObtenerConveniosMorososCobExtraJudicialCodigoCliente: TIntegerField
      FieldName = 'CodigoCliente'
    end
    object cdsObtenerConveniosMorososCobExtraJudicialCodigoConvenio: TIntegerField
      FieldName = 'CodigoConvenio'
    end
    object cdsObtenerConveniosMorososCobExtraJudicialFechaVencimiento: TDateField
      FieldName = 'FechaVencimiento'
    end
    object cdsObtenerConveniosMorososCobExtraJudicialAccionCobranza: TStringField
      DisplayWidth = 200
      FieldName = 'AccionCobranza'
      Size = 200
    end
    object cdsObtenerConveniosMorososCobExtraJudicialEstadoAccionCobranza: TStringField
      DisplayWidth = 200
      FieldName = 'EstadoAccionCobranza'
      Size = 200
    end
  end
  object dsObtenerConveniosMorososCobExtraJudicial: TDataSource
    DataSet = cdsObtenerConveniosMorososCobExtraJudicial
    Left = 568
    Top = 316
  end
  object spObtenerConveniosMorososCobJudicial: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerConveniosMorososConEstado'
    Parameters = <>
    Left = 848
    Top = 216
  end
  object cdsObtenerConveniosMorososCobJudicial: TClientDataSet
    Aggregates = <>
    Params = <>
    ReadOnly = True
    Left = 848
    Top = 256
    object cdsObtenerConveniosMorososCobJudicialMarcar: TBooleanField
      FieldName = 'Accion'
    end
    object cdsObtenerConveniosMorososCobJudicialNumeroDocumento: TStringField
      FieldName = 'NumeroDocumento'
    end
    object cdsObtenerConveniosMorososCobJudicialNombrePersona: TStringField
      DisplayWidth = 200
      FieldName = 'NombrePersona'
      Size = 200
    end
    object cdsObtenerConveniosMorososCobJudicialNumeroConvenio: TStringField
      DisplayWidth = 30
      FieldName = 'NumeroConvenio'
      Size = 30
    end
    object cdsObtenerConveniosMorososCobJudicialTipoCliente: TStringField
      DisplayWidth = 50
      FieldName = 'TipoCliente'
      Size = 50
    end
    object cdsObtenerConveniosMorososCobJudicialConcesionaria: TStringField
      FieldName = 'Candidato'
    end
    object cdsObtenerConveniosMorososCobJudicialDiasDeuda: TIntegerField
      FieldName = 'DiasDeuda'
    end
    object cdsObtenerConveniosMorososCobJudicialSaldo: TLargeintField
      FieldName = 'Saldo'
    end
    object cdsObtenerConveniosMorososCobJudicialCodigoCliente: TIntegerField
      FieldName = 'CodigoCliente'
    end
    object cdsObtenerConveniosMorososCobJudicialCodigoConvenio: TIntegerField
      FieldName = 'CodigoConvenio'
    end
    object cdsObtenerConveniosMorososCobJudicialFechaVencimiento: TDateField
      FieldName = 'FechaVencimiento'
    end
    object cdsObtenerConveniosMorososCobJudicialAccionCobranza: TStringField
      DisplayWidth = 200
      FieldName = 'AccionCobranza'
      Size = 200
    end
    object cdsObtenerConveniosMorososCobJudicialEstadoAccionCobranza: TStringField
      DisplayWidth = 200
      FieldName = 'EstadoAccionCobranza'
      Size = 200
    end
  end
  object dsObtenerConveniosMorososCobJudicial: TDataSource
    DataSet = cdsObtenerConveniosMorososCobJudicial
    Left = 848
    Top = 308
  end
  object spObtenerAccionesDeCobranza: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerAccionesDeCobranza'
    Parameters = <>
    Left = 416
    Top = 152
  end
  object lnCheck: TImageList
    BkColor = clWhite
    Height = 15
    Width = 15
    Left = 23
    Top = 163
    Bitmap = {
      494C01010200040030020F000F00FFFFFF00FF10FFFFFFFFFFFFFFFF424D3600
      00000000000036000000280000003C0000000F0000000100200000000000100E
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00000000000000
      0000FFFFFF00000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000424D3E000000000000003E000000280000003C0000000F00000001000100
      00000000780000000000000000000000000000000000000000000000FFFFFF00
      FFFFFFFC00000000800300040000000080030004000000008003000400000000
      8003000400000000800300040000000080030004000000008003000400000000
      8003000400000000800300040000000080030004000000008003000400000000
      80030004000000008003000400000000FFFFFFFC000000000000000000000000
      0000000000000000000000000000}
  end
  object spGenerarNotificacionesCobranza: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'GenerarNotificacionesCobranza'
    Parameters = <>
    Left = 176
    Top = 392
  end
  object spCastigarConvenio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CastigarConvenio'
    Parameters = <>
    Left = 560
    Top = 512
  end
  object spObtenerComprobantesImpagos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerComprobantesImpagos'
    Parameters = <>
    Left = 648
    Top = 488
  end
  object cds_Comprobantes: TClientDataSet
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Cobrar'
        DataType = ftBoolean
      end
      item
        Name = 'TipoComprobante'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'NumeroComprobante'
        DataType = ftLargeint
      end
      item
        Name = 'DescriComprobante'
        DataType = ftString
        Size = 35
      end
      item
        Name = 'CodigoConvenio'
        DataType = ftInteger
      end
      item
        Name = 'CodigoPersona'
        DataType = ftInteger
      end
      item
        Name = 'TotalComprobante'
        DataType = ftInteger
      end
      item
        Name = 'EstadoPago'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EstadoDebito'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'DescriEstado'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'UltimoComprobante'
        DataType = ftLargeint
      end
      item
        Name = 'FechaEmision'
        DataType = ftDateTime
      end
      item
        Name = 'FechaVencimiento'
        DataType = ftDateTime
      end
      item
        Name = 'TotalPagos'
        DataType = ftInteger
      end
      item
        Name = 'TotalPagosStr'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'SaldoPendiente'
        DataType = ftInteger
      end
      item
        Name = 'SaldoPendienteDescri'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'TotalComprobanteDescri'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'DescTipoMedioPAgo'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'DescPATPAC'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'Rechazo'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'TipoComprobanteFiscal'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'NumeroComprobanteFiscal'
        DataType = ftLargeint
      end
      item
        Name = 'DescriComprobanteFiscal'
        DataType = ftString
        Size = 35
      end
      item
        Name = 'EstadoRefinanciacion'
        DataType = ftSmallint
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 652
    Top = 540
    Data = {
      100300009619E0BD010000001800000019000000000003000000100306436F62
      72617202000300000000000F5469706F436F6D70726F62616E74650100490000
      000100055749445448020002001400114E756D65726F436F6D70726F62616E74
      65080001000000000011446573637269436F6D70726F62616E74650100490000
      0001000557494454480200020023000E436F6469676F436F6E76656E696F0400
      0100000000000D436F6469676F506572736F6E61040001000000000010546F74
      616C436F6D70726F62616E746504000100000000000A45737461646F5061676F
      01004900000001000557494454480200020001000C45737461646F4465626974
      6F01004900000001000557494454480200020001000C44657363726945737461
      646F010049000000010005574944544802000200140011556C74696D6F436F6D
      70726F62616E746508000100000000000C4665636861456D6973696F6E080008
      000000000010466563686156656E63696D69656E746F08000800000000000A54
      6F74616C5061676F7304000100000000000D546F74616C5061676F7353747201
      004900000001000557494454480200020014000E53616C646F50656E6469656E
      746504000100000000001453616C646F50656E6469656E746544657363726901
      0049000000010005574944544802000200140016546F74616C436F6D70726F62
      616E746544657363726901004900000001000557494454480200020014001144
      6573635469706F4D6564696F5041676F01004900000001000557494454480200
      020032000A446573635041545041430100490000000100055749445448020002
      0032000752656368617A6F010049000000010005574944544802000200320015
      5469706F436F6D70726F62616E746546697363616C0100490000000100055749
      445448020002001400174E756D65726F436F6D70726F62616E74654669736361
      6C080001000000000017446573637269436F6D70726F62616E74654669736361
      6C01004900000001000557494454480200020023001445737461646F52656669
      6E616E63696163696F6E02000100000000000000}
  end
  object spObtenerRefinanciacionesConsultas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerRefinanciacionesConsultas'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@RUTPersona'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoMedioPago'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoEstado'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@SoloNoActivadas'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 1016
    Top = 376
  end
  object dspObtenerRefinanciacionesConsultas: TDataSetProvider
    DataSet = spObtenerRefinanciacionesConsultas
    Left = 1016
    Top = 432
  end
  object cdsRefinanciaciones: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoRefinanciacion'
        DataType = ftInteger
      end
      item
        Name = 'CodigoPersona'
        DataType = ftInteger
      end
      item
        Name = 'CodigoTipoMedioPago'
        DataType = ftSmallint
      end
      item
        Name = 'Tipo'
        Attributes = [faFixed]
        DataType = ftString
        Size = 30
      end
      item
        Name = 'Fecha'
        DataType = ftDateTime
      end
      item
        Name = 'Estado'
        Attributes = [faReadonly]
        DataType = ftString
        Size = 69
      end
      item
        Name = 'NumeroDocumento'
        Attributes = [faFixed]
        DataType = ftString
        Size = 11
      end
      item
        Name = 'Convenio'
        Attributes = [faFixed]
        DataType = ftString
        Size = 30
      end
      item
        Name = 'TotalDeuda'
        Attributes = [faReadonly]
        DataType = ftString
        Size = 20
      end
      item
        Name = 'TotalPagado'
        Attributes = [faReadonly]
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Validada'
        DataType = ftBoolean
      end
      item
        Name = 'CodigoEstado'
        DataType = ftSmallint
      end
      item
        Name = 'Protestada'
        DataType = ftBoolean
      end
      item
        Name = 'NombrePersona'
        Attributes = [faReadonly]
        DataType = ftString
        Size = 200
      end
      item
        Name = 'CodigoRefinanciacionUnificada'
        DataType = ftInteger
      end>
    IndexDefs = <
      item
        Name = 'Fecha'
        Fields = 'Fecha; CodigoRefinanciacion'
      end
      item
        Name = 'FechaDESC'
        Fields = 'Fecha; CodigoRefinanciacion'
        Options = [ixDescending]
      end
      item
        Name = 'CodigoRefinanciacion'
        Fields = 'CodigoRefinanciacion'
      end
      item
        Name = 'CodigoRefinanciacionDESC'
        Fields = 'CodigoRefinanciacion'
        Options = [ixDescending]
      end
      item
        Name = 'NumeroDocumento'
        Fields = 'NumeroDocumento'
      end
      item
        Name = 'NumeroDocumentoDESC'
        Fields = 'NumeroDocumento'
        Options = [ixDescending]
      end
      item
        Name = 'Estado'
        Fields = 'Estado'
      end
      item
        Name = 'EstadoDESC'
        Fields = 'Estado'
        Options = [ixDescending]
      end
      item
        Name = 'Tipo'
        Fields = 'Tipo'
      end
      item
        Name = 'TipoDESC'
        Fields = 'Tipo'
        Options = [ixDescending]
      end
      item
        Name = 'Convenio'
        Fields = 'Convenio'
      end
      item
        Name = 'ConvenioDESC'
        Fields = 'Convenio'
        Options = [ixDescending]
      end>
    IndexName = 'FechaDESC'
    Params = <>
    ProviderName = 'dspObtenerRefinanciacionesConsultas'
    StoreDefs = True
    Left = 1024
    Top = 480
    object cdsRefinanciacionesCodigoRefinanciacion: TIntegerField
      FieldName = 'CodigoRefinanciacion'
    end
    object cdsRefinanciacionesCodigoPersona: TIntegerField
      FieldName = 'CodigoPersona'
    end
    object cdsRefinanciacionesTipo: TStringField
      FieldName = 'Tipo'
      FixedChar = True
      Size = 30
    end
    object cdsRefinanciacionesFecha: TDateTimeField
      FieldName = 'Fecha'
    end
    object cdsRefinanciacionesEstado: TStringField
      FieldName = 'Estado'
      Size = 30
    end
    object cdsRefinanciacionesNumeroDocumento: TStringField
      FieldName = 'NumeroDocumento'
      FixedChar = True
      Size = 11
    end
    object cdsRefinanciacionesNombrePersona: TStringField
      FieldName = 'NombrePersona'
      ReadOnly = True
      Size = 100
    end
    object cdsRefinanciacionesConvenio: TStringField
      FieldName = 'Convenio'
      ReadOnly = True
      FixedChar = True
      Size = 30
    end
    object cdsRefinanciacionesTotalDeuda: TStringField
      FieldName = 'TotalDeuda'
      ReadOnly = True
    end
    object cdsRefinanciacionesTotalPagado: TStringField
      FieldName = 'TotalPagado'
      ReadOnly = True
    end
    object cdsRefinanciacionesValidada: TBooleanField
      FieldName = 'Validada'
    end
    object cdsRefinanciacionesCodigoEstado: TSmallintField
      FieldName = 'CodigoEstado'
    end
    object cdsRefinanciacionesProtestada: TBooleanField
      FieldName = 'Protestada'
    end
    object cdsRefinanciacionesCodigoRefinanciacionUnificada: TIntegerField
      FieldName = 'CodigoRefinanciacionUnificada'
    end
  end
  object spGuardarCampannas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'GuardarCampannas'
    Parameters = <>
    Left = 176
    Top = 448
  end
  object spVerificarConvenioCandidato: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'VerificarConvenioCandidatoAInhabilitacion'
    Parameters = <>
    Left = 320
    Top = 368
  end
  object spCategoriasDeDeudasDeCobranza_Listar: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CategoriasDeDeudasDeCobranza_Listar'
    Parameters = <>
    Left = 816
    Top = 440
  end
  object spActualizarTipoClienteConvenio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarTipoClienteConvenio'
    Parameters = <>
    Left = 320
    Top = 488
  end
  object spObtenerConveniosMorososDeudores: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CommandTimeout = 90
    ProcedureName = 'ObtenerConveniosMorosos'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@MontoMinimoDeuda'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@DiasDeuda'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ErrorDescription'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 4000
        Value = Null
      end>
    Left = 120
    Top = 216
  end
  object cdsObtenerConveniosMorososDeudores: TClientDataSet
    Aggregates = <>
    Params = <>
    ReadOnly = True
    Left = 120
    Top = 256
    object cdsObtenerConveniosMorososDeudoresMarcar: TBooleanField
      FieldName = 'Marcar'
      ReadOnly = True
    end
    object cdsObtenerConveniosMorososDeudoresNumeroDocumento: TStringField
      DisplayLabel = 'RUT'
      FieldName = 'NumeroDocumento'
    end
    object cdsObtenerConveniosMorososDeudoresNombrePersona: TStringField
      DisplayLabel = 'Cliente'
      DisplayWidth = 200
      FieldName = 'NombrePersona'
      Size = 200
    end
    object cdsObtenerConveniosMorososDeudoresNumeroConvenio: TStringField
      DisplayLabel = 'Convenio'
      DisplayWidth = 200
      FieldName = 'NumeroConvenio'
      Size = 30
    end
    object cdsObtenerConveniosMorososDeudoresTipoCliente: TStringField
      DisplayLabel = 'Tipo de Cliente'
      DisplayWidth = 100
      FieldName = 'TipoDeCliente'
      Size = 100
    end
    object cdsObtenerConveniosMorososDeudoresCandidato: TStringField
      DisplayLabel = 'Candidato a Inhabilitaci'#243'n'
      FieldName = 'Candidato'
    end
    object cdsObtenerConveniosMorososDeudoresSaldo: TFloatField
      DisplayLabel = 'Monto de Deuda'
      FieldName = 'Saldo'
    end
    object cdsObtenerConveniosMorososDeudoresCantComprobantes: TIntegerField
      DisplayLabel = 'Cantidad de Comprobantes'
      FieldName = 'CantidadComprobantes'
    end
    object cdsObtenerConveniosMorososDeudoresDiasDeuda: TIntegerField
      DisplayLabel = 'Edad Deuda'
      FieldName = 'DiasDeuda'
    end
    object cdsObtenerConveniosMorososDeudoresFechaVencimiento: TDateField
      DisplayLabel = 'Fecha de Vencimiento'
      DisplayWidth = 20
      FieldName = 'FechaVencimiento'
      DisplayFormat = 'dd/mm/yyyy'
      EditMask = '!99/99/0000;1;_'
    end
    object cdsObtenerConveniosMorososDeudoresAccion: TStringField
      DisplayLabel = 'Acci'#243'n de Cobranza'
      DisplayWidth = 100
      FieldName = 'AccionCobranza'
      Size = 100
    end
    object cdsObtenerConveniosMorososDeudoresEstadoAccion: TStringField
      DisplayLabel = 'Estado Acci'#243'n  de Cobranza'
      DisplayWidth = 100
      FieldName = 'EstadoAccionCobranza'
      Size = 100
    end
    object strngfldObtenerConveniosMorososDeudoresEmpresaCobranza: TStringField
      DisplayLabel = 'Empresa de Cobranza'
      DisplayWidth = 200
      FieldName = 'EmpresaCobranza'
      Size = 200
    end
    object dtmfldObtenerConveniosMorososDeudoresFechaEnvioCobranza: TDateTimeField
      DisplayLabel = 'Fecha Env'#237'o a Cobranza'
      FieldName = 'FechaEnvioCobranza'
    end
    object strngfldObtenerConveniosMorososDeudoresRefinanciacion: TStringField
      DisplayLabel = 'Refinanciaci'#243'n'
      FieldName = 'Refinanciacion'
    end
    object strngfldObtenerConveniosMorososDeudoresEstadoRefinanciacion: TStringField
      DisplayLabel = 'Estado Refinanciaci'#243'n'
      DisplayWidth = 200
      FieldName = 'EstadoRefinanciacion'
      Size = 200
    end
    object strngfldObtenerConveniosMorososDeudoresAvenimientoJudicial: TStringField
      DisplayLabel = 'Avenimiento Judicial'
      FieldName = 'AvenimientoJudicial'
    end
    object strngfldObtenerConveniosMorososDeudoresEstadoAvenimiento: TStringField
      DisplayLabel = 'Estado Avenimiento'
      DisplayWidth = 200
      FieldName = 'EstadoAvenimiento'
      Size = 200
    end
    object cdsObtenerConveniosMorososDeudoresCodigoCliente: TIntegerField
      FieldName = 'CodigoCliente'
      Visible = False
    end
    object cdsObtenerConveniosMorososDeudoresCodigoConvenio: TIntegerField
      FieldName = 'CodigoConvenio'
      Visible = False
    end
  end
  object dsObtenerConveniosMorososDeudores: TDataSource
    DataSet = cdsObtenerConveniosMorososDeudores
    Left = 120
    Top = 308
  end
  object spActualizarEstadoConvenio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarEstadoConvenio'
    Parameters = <>
    Left = 592
    Top = 664
  end
  object spTiposDeAccionesDeCobranza_Listar: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'TiposDeAccionesDeCobranza_Listar'
    Parameters = <>
    Left = 712
    Top = 168
  end
  object spGuardarAccionConvenio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'GuardarAccionConvenio'
    Parameters = <>
    Left = 432
    Top = 664
  end
  object spIngresarConvenioACobranzaInterna: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'IngresarConvenioACobranzaInterna'
    Parameters = <>
    Left = 80
    Top = 464
  end
  object spEliminarConvenioDeCobranzaInterna: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarConvenioDeCobranzaInterna'
    Parameters = <>
    Left = 80
    Top = 512
  end
  object spObtenerHistorialDeCobranza: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerHistorialDeCobranzaConvenio'
    Parameters = <>
    Left = 320
    Top = 544
  end
end
