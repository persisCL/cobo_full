object GestionarCobranzaExternaForm: TGestionarCobranzaExternaForm
  Left = 293
  Top = 219
  Caption = 'Gestionar cobranza externa'
  ClientHeight = 545
  ClientWidth = 1258
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object dlConvenios: TDBListEx
    Left = 0
    Top = 0
    Width = 1258
    Height = 233
    Align = alClient
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 60
        MinWidth = 60
        MaxWidth = 60
        Header.Caption = 'Marcar'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = [fsBold]
        Header.Alignment = taCenter
        IsLink = True
        FieldName = 'Marcar'
      end
      item
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 100
        MinWidth = 100
        MaxWidth = 150
        Header.Caption = 'RUT del cliente'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = [fsBold]
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'NumeroDocumento'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 100
        MinWidth = 100
        MaxWidth = 150
        Header.Caption = 'Tipo de cliente'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = [fsBold]
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'TipoDeClienteStr'
      end
      item
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 70
        MinWidth = 70
        MaxWidth = 100
        Header.Caption = 'Convenio'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = [fsBold]
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'CodigoConvenio'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 100
        MaxWidth = 250
        Header.Caption = 'Personer'#237'a'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = [fsBold]
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'PersoneriaStr'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 180
        MinWidth = 180
        MaxWidth = 250
        Header.Caption = 'Comuna'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = [fsBold]
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'ComunaStr'
      end
      item
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 110
        MinWidth = 110
        MaxWidth = 200
        Header.Caption = 'Monto de deuda'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = [fsBold]
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'MontoDeuda'
      end
      item
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 140
        MinWidth = 140
        MaxWidth = 200
        Header.Caption = 'Edad de deuda (d'#237'as)'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = [fsBold]
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'EdadDeuda'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 140
        MinWidth = 140
        MaxWidth = 200
        Header.Caption = 'Fecha de vencimiento'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = [fsBold]
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'FechaVencimiento'
      end
      item
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 160
        MinWidth = 160
        MaxWidth = 200
        Header.Caption = 'Cantidad de documentos'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = [fsBold]
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'CantidadDeDocumentos'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 160
        MinWidth = 160
        MaxWidth = 350
        Header.Caption = 'Enviado a cobranza ext.'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = [fsBold]
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'EnviadoACobranzaExternaStr'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 200
        MinWidth = 200
        MaxWidth = 200
        Header.Caption = 'Fecha de env'#237'o a cobranza ext.'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = [fsBold]
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'FechaDeEnvioACobranzaExterna'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 200
        MinWidth = 200
        MaxWidth = 200
        Header.Caption = 'Estado de Cobranza ext.'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = [fsBold]
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'EstadoDeCobranzaExternaStr'
      end>
    DataSource = csConveniosACobranzaExterna
    DragReorder = True
    ParentColor = False
    TabOrder = 0
    TabStop = True
    OnDrawText = dlConveniosDrawText
    OnLinkClick = dlConveniosLinkClick
  end
  object gbEmpresasCobranza: TGroupBox
    Left = 0
    Top = 233
    Width = 1258
    Height = 273
    Align = alBottom
    Caption = ' Empresas de Cobranza '
    TabOrder = 1
    object dlEmpresasCobranza: TDBListEx
      Left = 2
      Top = 15
      Width = 1254
      Height = 158
      Align = alTop
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          MinWidth = 150
          MaxWidth = 1000
          Header.Caption = 'Alias'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'Alias'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 180
          MinWidth = 180
          MaxWidth = 1000
          Header.Caption = 'Comuna'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'ComunaStr'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 100
          MinWidth = 100
          MaxWidth = 150
          Header.Caption = 'Judicial'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'JudicialStr'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          MinWidth = 150
          MaxWidth = 150
          Header.Caption = 'Capacidad'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'CapacidadStr'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 180
          MinWidth = 180
          MaxWidth = 180
          Header.Caption = 'Elegible por edad de Deuda'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'ElegiblePorEdadDeDeudaStr'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 180
          MinWidth = 180
          MaxWidth = 180
          Header.Caption = 'Elegible por personer'#237'a'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'ElegiblePorPersoneriaStr'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 150
          MinWidth = 150
          MaxWidth = 150
          Header.Caption = 'Elegible por comuna'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'ElegiblePorComunaStr'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 200
          MinWidth = 200
          MaxWidth = 200
          Header.Caption = 'Elegible por monto m'#237'nimo'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'ElegiblePorMontoMinimoStr'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 200
          MinWidth = 200
          MaxWidth = 200
          Header.Caption = 'Elegible por monto m'#225'ximo'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'ElegiblePorMontoMaximoStr'
        end>
      DataSource = dsEmpresasCobranza
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
    end
    object pnDetalleEmpresaCobranza: TPanel
      Left = 2
      Top = 173
      Width = 1254
      Height = 98
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      DesignSize = (
        1254
        98)
      object lblNombre: TLabel
        Left = 8
        Top = 6
        Width = 40
        Height = 13
        Caption = 'Nombre:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lblTipoDeOperacionEmpresasCobranza: TLabel
        Left = 8
        Top = 33
        Width = 108
        Height = 13
        Caption = 'Tipo de operaci'#243'n:'
        FocusControl = txtTipoDeOperacionEmpresasCobranza
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblComisionPorAviso: TLabel
        Left = 414
        Top = 33
        Width = 111
        Height = 13
        Caption = 'Comisi'#243'n por aviso:'
        FocusControl = txtComisionPorAviso
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label1: TLabel
        Left = 582
        Top = 33
        Width = 10
        Height = 13
        Caption = '%'
        FocusControl = txtComisionPorAviso
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblComisionPorCobranza: TLabel
        Left = 611
        Top = 33
        Width = 133
        Height = 13
        Caption = 'Comisi'#243'n por cobranza:'
        FocusControl = txtComisionPorCobranza
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 802
        Top = 33
        Width = 10
        Height = 13
        Caption = '%'
        FocusControl = txtComisionPorAviso
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblMotivoDeTransferencia: TLabel
        Left = 8
        Top = 72
        Width = 114
        Height = 13
        Caption = 'Motivo de transferencia:'
        FocusControl = txtMotivoDeTransferencia
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lblNombreEmpresaCobranza: TLabel
        Left = 57
        Top = 6
        Width = 832
        Height = 13
        Anchors = [akLeft, akTop, akRight]
        AutoSize = False
        Caption = 'lblNombre'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lblNoExistenEmpresasDeCobranza: TLabel
        Left = 909
        Top = 6
        Width = 326
        Height = 13
        Align = alCustom
        Anchors = [akTop, akRight]
        Caption = 
          'No existen empresas de cobranza para los convenios seleccionados' +
          '.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object txtTipoDeOperacionEmpresasCobranza: TVariantComboBox
        Left = 128
        Top = 30
        Width = 277
        Height = 21
        Style = vcsDropDownList
        Color = 16444382
        ItemHeight = 13
        TabOrder = 0
        OnChange = txtTipoDeOperacionEmpresasCobranzaChange
        Items = <>
      end
      object txtComisionPorAviso: TNumericEdit
        Left = 529
        Top = 30
        Width = 50
        Height = 21
        Color = 16444382
        TabOrder = 1
        Decimals = 2
        BlankWhenZero = False
      end
      object txtComisionPorCobranza: TNumericEdit
        Left = 749
        Top = 30
        Width = 50
        Height = 21
        Color = 16444382
        TabOrder = 2
        Decimals = 2
        BlankWhenZero = False
      end
      object txtMotivoDeTransferencia: TEdit
        Left = 128
        Top = 69
        Width = 671
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 255
        ParentFont = False
        TabOrder = 3
      end
    end
  end
  object PAbajo: TPanel
    Left = 0
    Top = 506
    Width = 1258
    Height = 39
    Align = alBottom
    TabOrder = 2
    object Panel1: TPanel
      Left = 1062
      Top = 1
      Width = 195
      Height = 37
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object BtnAceptar: TButton
        Left = 13
        Top = 6
        Width = 79
        Height = 26
        Caption = '&Aceptar'
        Default = True
        TabOrder = 0
        OnClick = BtnAceptarClick
      end
      object BtnCancelar: TButton
        Left = 98
        Top = 6
        Width = 79
        Height = 26
        Cancel = True
        Caption = '&Cancelar'
        TabOrder = 1
        OnClick = BtnCancelarClick
      end
    end
  end
  object lnCheck: TImageList
    BkColor = clWhite
    Height = 15
    Width = 15
    Left = 119
    Top = 107
    Bitmap = {
      494C0101020004009C010F000F00FFFFFF00FF10FFFFFFFFFFFFFFFF424D3600
      00000000000036000000280000003C0000000F0000000100200000000000100E
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00000000000000
      0000FFFFFF00000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000424D3E000000000000003E000000280000003C0000000F00000001000100
      00000000780000000000000000000000000000000000000000000000FFFFFF00
      FFFFFFFC00000000800300040000000080030004000000008003000400000000
      8003000400000000800300040000000080030004000000008003000400000000
      8003000400000000800300040000000080030004000000008003000400000000
      80030004000000008003000400000000FFFFFFFC000000000000000000000000
      0000000000000000000000000000}
  end
  object csConveniosACobranzaExterna: TDataSource
    DataSet = cdsConveniosACobranzaExterna
    Left = 64
    Top = 156
  end
  object cdsConveniosACobranzaExterna: TClientDataSet
    Aggregates = <>
    FilterOptions = [foCaseInsensitive]
    FieldDefs = <>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 100
    Top = 156
    object cdsConveniosACobranzaExternaCodConveniosEnCobExt: TLargeintField
      FieldName = 'CodConveniosEnCobExt'
    end
    object cdsConveniosACobranzaExternaMarcar: TBooleanField
      FieldName = 'Marcar'
    end
    object cdsConveniosACobranzaExternaNumeroDocumento: TStringField
      FieldName = 'NumeroDocumento'
      Size = 255
    end
    object cdsConveniosACobranzaExternaTipoDeClienteStr: TStringField
      FieldName = 'TipoDeClienteStr'
      Size = 255
    end
    object cdsConveniosACobranzaExternaCodigoConvenio: TIntegerField
      FieldName = 'CodigoConvenio'
    end
    object cdsConveniosACobranzaExternaCodigoTipoCliente: TIntegerField
      FieldName = 'CodigoTipoCliente'
    end
    object cdsConveniosACobranzaExternaPersoneriaStr: TStringField
      FieldName = 'PersoneriaStr'
      Size = 255
    end
    object cdsConveniosACobranzaExternaCodigoRegion: TStringField
      FieldName = 'CodigoRegion'
      Size = 3
    end
    object cdsConveniosACobranzaExternaCodigoComuna: TStringField
      FieldName = 'CodigoComuna'
      Size = 3
    end
    object cdsConveniosACobranzaExternaComunaStr: TStringField
      FieldName = 'ComunaStr'
      Size = 255
    end
    object cdsConveniosACobranzaExternaMontoDeuda: TFloatField
      FieldName = 'MontoDeuda'
    end
    object cdsConveniosACobranzaExternaEdadDeuda: TIntegerField
      FieldName = 'EdadDeuda'
    end
    object cdsConveniosACobranzaExternaFechaVencimiento: TDateField
      FieldName = 'FechaVencimiento'
    end
    object cdsConveniosACobranzaExternaCantidadDeDocumentos: TIntegerField
      FieldName = 'CantidadDeDocumentos'
    end
    object cdsConveniosACobranzaExternaCodigoEmpresaCobranza: TIntegerField
      FieldName = 'CodigoEmpresaCobranza'
    end
    object cdsConveniosACobranzaExternaEnviadoACobranzaExterna: TBooleanField
      FieldName = 'EnviadoACobranzaExterna'
    end
    object cdsConveniosACobranzaExternaEnviadoACobranzaExternaStr: TStringField
      FieldName = 'EnviadoACobranzaExternaStr'
      Size = 500
    end
    object cdsConveniosACobranzaExternaFechaDeEnvioACobranzaExterna: TDateField
      FieldName = 'FechaDeEnvioACobranzaExterna'
    end
    object cdsConveniosACobranzaExternaEstadoDeCobranzaExterna: TIntegerField
      FieldName = 'EstadoDeCobranzaExterna'
    end
    object cdsConveniosACobranzaExternaEstadoDeCobranzaExternaStr: TStringField
      FieldName = 'EstadoDeCobranzaExternaStr'
      Size = 255
    end
    object cdsConveniosACobranzaExternaCodTipoJudEmpCob: TIntegerField
      FieldName = 'CodTipoJudEmpCob'
    end
  end
  object cdsEmpresasCobranza: TClientDataSet
    Aggregates = <>
    Params = <>
    AfterScroll = cdsEmpresasCobranzaAfterScroll
    Left = 208
    Top = 320
    object EmpFieldCodigoEmpresaCobranza: TIntegerField
      FieldName = 'CodigoEmpresaCobranza'
    end
    object EmpFieldNombre: TStringField
      FieldName = 'Nombre'
      Size = 255
    end
    object EmpFieldAlias: TStringField
      FieldName = 'Alias'
      Size = 255
    end
    object EmpFieldCodigoRegion: TStringField
      FieldName = 'CodigoRegion'
      Size = 3
    end
    object EmpField: TStringField
      FieldName = 'CodigoComuna'
      Size = 3
    end
    object EmpFieldComunaStr: TStringField
      FieldName = 'ComunaStr'
      Size = 255
    end
    object EmpFieldCapacidadStr: TStringField
      FieldName = 'CapacidadStr'
      Size = 255
    end
    object EmpFieldJudicial: TBooleanField
      FieldName = 'Judicial'
    end
    object EmpFieldJudicialStr: TStringField
      FieldName = 'JudicialStr'
      Size = 255
    end
    object EmpFieldElegiblePorEdadDeDeuda: TBooleanField
      FieldName = 'ElegiblePorEdadDeDeuda'
    end
    object EmpFieldElegiblePorEdadDeDeudaStr: TStringField
      FieldName = 'ElegiblePorEdadDeDeudaStr'
    end
    object EmpFieldElegiblePorPersoneria: TBooleanField
      FieldName = 'ElegiblePorPersoneria'
    end
    object EmpFieldElegiblePorPersoneriaStr: TStringField
      FieldName = 'ElegiblePorPersoneriaStr'
      Size = 255
    end
    object EmpFieldElegiblePorComuna: TBooleanField
      FieldName = 'ElegiblePorComuna'
    end
    object EmpFieldElegiblePorComunaStr: TStringField
      FieldName = 'ElegiblePorComunaStr'
      Size = 255
    end
    object EmpFieldElegiblePorMontoMinimo: TBooleanField
      FieldName = 'ElegiblePorMontoMinimo'
    end
    object EmpFieldElegiblePorMontoMinimoStr: TStringField
      FieldName = 'ElegiblePorMontoMinimoStr'
      Size = 255
    end
    object EmpFieldElegiblePorMontoMaximo: TBooleanField
      FieldName = 'ElegiblePorMontoMaximo'
    end
    object EmpFieldElegiblePorMontoMaximoStr: TStringField
      FieldName = 'ElegiblePorMontoMaximoStr'
      Size = 255
    end
    object EmpFieldCodigoTipoCliente: TIntegerField
      FieldName = 'CodigoTipoCliente'
    end
    object EmpFieldCodTipoJudicialEmpCob: TIntegerField
      FieldName = 'CodTipoJudicialEmpCob'
    end
    object EmpFieldCodTipoDeOpEmpCob: TIntegerField
      FieldName = 'CodTipoDeOpEmpCob'
    end
    object EmpFieldComisionPorAviso: TFloatField
      FieldName = 'ComisionPorAviso'
    end
    object EmpFieldComisionPorCobranza: TFloatField
      FieldName = 'ComisionPorCobranza'
    end
    object FieldEmpEdadDeuda: TIntegerField
      FieldName = 'EdadDeuda'
    end
    object EmpFieldMontoMinimo: TFloatField
      FieldName = 'MontoMinimo'
    end
    object EmpFieldMontoMaximo: TFloatField
      FieldName = 'MontoMaximo'
    end
    object EmpFieldCapacidadDeProceso: TIntegerField
      FieldName = 'CapacidadDeProceso'
    end
    object EmpFieldDescripcion: TStringField
      FieldName = 'Descripcion'
      Size = 255
    end
  end
  object dsEmpresasCobranza: TDataSource
    DataSet = cdsEmpresasCobranza
    Left = 176
    Top = 320
  end
  object spConveniosEnCobranzaExterna_Almacenar: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ConveniosEnCobranzaExterna_Almacenar'
    Parameters = <>
    Left = 1067
    Top = 470
  end
  object spConveniosEnCobranzaExterna_TraspasoDeEmpresa: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ConveniosEnCobranzaExterna_TraspasoDeEmpresa'
    Parameters = <>
    Left = 1101
    Top = 470
  end
  object spConveniosEnCobranzaExterna_RecuperacionDeDeuda: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ConveniosEnCobranzaExterna_RecuperacionDeDeuda'
    Parameters = <>
    Left = 1136
    Top = 470
  end
end
