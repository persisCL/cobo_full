object formCobroComprobantesCastigo: TformCobroComprobantesCastigo
  Left = 338
  Top = 108
  Anchors = []
  BorderStyle = bsSingle
  Caption = 'Cobro de Comprobantes Castigados'
  ClientHeight = 590
  ClientWidth = 865
  Color = clBtnFace
  Constraints.MinHeight = 561
  Constraints.MinWidth = 670
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object grpPorCliente: TGroupBox
    Left = 0
    Top = 0
    Width = 865
    Height = 113
    Align = alTop
    Caption = 'B'#250'squeda Comprobante'
    Color = clBtnFace
    ParentColor = False
    TabOrder = 0
    object lb_CodigoCliente: TLabel
      Left = 16
      Top = 21
      Width = 61
      Height = 13
      Caption = 'RUT Cliente:'
    end
    object lbl_NumeroComprobante: TLabel
      Left = 239
      Top = 24
      Width = 103
      Height = 13
      Caption = 'N'#250'mero Comprobante'
    end
    object lbl_NumeroConvenio: TLabel
      Left = 16
      Top = 65
      Width = 85
      Height = 13
      Caption = 'N'#250'mero Convenio'
    end
    object lblMontoDeudasConvenio: TLabel
      Left = 559
      Top = 41
      Width = 143
      Height = 13
      Caption = 'lblMontoDeudasConvenio'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblMontoDeudasRefinanciacion: TLabel
      Left = 559
      Top = 65
      Width = 173
      Height = 13
      Caption = 'lblMontoDeudasRefinanciacion'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblDeudasConvenio: TLabel
      Left = 441
      Top = 41
      Width = 88
      Height = 13
      Alignment = taRightJustify
      Caption = 'Deuda Convenios:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblDeudasRefinanciacion: TLabel
      Left = 441
      Top = 65
      Width = 112
      Height = 13
      Alignment = taRightJustify
      Caption = 'Deudas Refinanciaci'#243'n:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblEnListaAmarilla: TLabel
      Left = 239
      Top = 84
      Width = 200
      Height = 14
      Alignment = taCenter
      AutoSize = False
      Color = clYellow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      Transparent = False
      Visible = False
    end
    object btnFiltrar: TButton
      Left = 760
      Top = 36
      Width = 73
      Height = 25
      Caption = 'Filtrar'
      TabOrder = 0
      OnClick = btnFiltrarClick
    end
    object edNumeroComprobante: TNumericEdit
      Left = 239
      Top = 38
      Width = 196
      Height = 21
      TabOrder = 2
      OnChange = edNumeroComprobanteChange
      OnEnter = edNumeroComprobanteEnter
    end
    object cbConvenios: TVariantComboBox
      Left = 24
      Top = 81
      Width = 209
      Height = 19
      Style = vcsOwnerDrawFixed
      ItemHeight = 13
      TabOrder = 3
      OnChange = cbConveniosChange
      Items = <>
    end
    object peNumeroDocumento: TPickEdit
      Left = 24
      Top = 38
      Width = 209
      Height = 21
      CharCase = ecUpperCase
      Enabled = True
      TabOrder = 1
      OnChange = peNumeroDocumentoChange
      EditorStyle = bteTextEdit
      OnButtonClick = peNumeroDocumentoButtonClick
    end
  end
  object grp_DatosCliente: TGroupBox
    Left = 0
    Top = 113
    Width = 865
    Height = 70
    Align = alTop
    Caption = ' Datos del Cliente '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object lbl_Nombre: TLabel
      Left = 17
      Top = 32
      Width = 48
      Height = 13
      Caption = 'Nombre:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblNombre: TLabel
      Left = 91
      Top = 31
      Width = 72
      Height = 13
      Caption = 'Apellido Cliente'
    end
    object lblDomicilio: TLabel
      Left = 90
      Top = 49
      Width = 74
      Height = 13
      Caption = 'DomicilioCliente'
    end
    object lbl_Domicilio: TLabel
      Left = 17
      Top = 49
      Width = 56
      Height = 13
      Caption = 'Domicilio:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbl_NumeroDocumento: TLabel
      Left = 17
      Top = 15
      Width = 31
      Height = 13
      Caption = 'RUT:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblRUT: TLabel
      Left = 91
      Top = 14
      Width = 23
      Height = 13
      Caption = 'RUT'
    end
  end
  object pgcComprobantesImpagos: TPageControl
    Left = 0
    Top = 183
    Width = 865
    Height = 407
    ActivePage = tsComprobantes
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    object tsComprobantes: TTabSheet
      Caption = ' Comprobantes Impagos '
      DesignSize = (
        857
        379)
      object grp1: TGroupBox
        Left = 0
        Top = 0
        Width = 857
        Height = 221
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 0
        DesignSize = (
          857
          221)
        object lbl1: TLabel
          Left = 409
          Top = 199
          Width = 115
          Height = 13
          Anchors = [akLeft, akBottom]
          Caption = 'Total Seleccionado:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lblTotalSelec: TLabel
          Left = 530
          Top = 199
          Width = 90
          Height = 13
          Alignment = taRightJustify
          Anchors = [akLeft, akBottom]
          AutoSize = False
          Caption = 'TotalSeleccionado'
          ExplicitTop = 214
        end
        object dl_Comprobantes: TDBListEx
          Left = 3
          Top = 7
          Width = 849
          Height = 186
          Anchors = [akLeft, akTop, akRight, akBottom]
          BorderStyle = bsSingle
          Columns = <
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 50
              Header.Caption = 'Cobrar'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'Cobrar'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 95
              Header.Caption = 'Fecha Emisi'#243'n'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'FechaEmision'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 95
              Header.Caption = 'Vencimiento'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'FechaVencimiento'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 150
              Header.Caption = 'Comprobante'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'DescriComprobante'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 150
              Header.Caption = 'Comprobante Fiscal'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              IsLink = False
              FieldName = 'DescriComprobanteFiscal'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 80
              Header.Caption = 'Saldo'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              Header.Alignment = taRightJustify
              IsLink = False
              FieldName = 'SaldoPendienteDescri'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 80
              Header.Caption = 'Total'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              Header.Alignment = taRightJustify
              IsLink = False
              FieldName = 'TotalComprobanteDescri'
            end>
          DataSource = ds_Comprobantes
          DragReorder = True
          ParentColor = False
          TabOrder = 0
          TabStop = True
          OnClick = dl_ComprobantesClick
          OnDrawText = dl_ComprobantesDrawText
        end
      end
      object grpDatosComprobante: TGroupBox
        Left = 0
        Top = 221
        Width = 857
        Height = 117
        Align = alBottom
        Caption = ' Datos del Comprobante seleccionado '
        TabOrder = 1
        DesignSize = (
          857
          117)
        object lbl2: TLabel
          Left = 12
          Top = 18
          Width = 105
          Height = 13
          Caption = 'Fecha de Emisi'#243'n:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lblFecha: TLabel
          Left = 181
          Top = 18
          Width = 91
          Height = 13
          AutoSize = False
          Caption = 'Fecha'
        end
        object lblFechaVencimiento: TLabel
          Left = 181
          Top = 36
          Width = 91
          Height = 13
          AutoSize = False
          Caption = 'Fecha Vencimiento'
        end
        object lbl3: TLabel
          Left = 12
          Top = 36
          Width = 131
          Height = 13
          Caption = 'Fecha de Vencimiento:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lblTotalAPagar: TLabel
          Left = 530
          Top = 18
          Width = 90
          Height = 13
          Alignment = taRightJustify
          Anchors = [akLeft, akBottom]
          AutoSize = False
          Caption = 'Total a Pagar'
        end
        object lbl4: TLabel
          Left = 430
          Top = 18
          Width = 94
          Height = 13
          Caption = 'Deuda Exigible :'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbl_Estado: TLabel
          Left = 535
          Top = 68
          Width = 195
          Height = 13
          AutoSize = False
          Caption = 'lblEstado'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lbl5: TLabel
          Left = 464
          Top = 68
          Width = 44
          Height = 13
          Caption = 'Estado:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbl6: TLabel
          Left = 12
          Top = 56
          Width = 157
          Height = 13
          Caption = 'Medio de Pago Autom'#225'tico:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lblDescPATPAC: TLabel
          Left = 269
          Top = 56
          Width = 77
          Height = 13
          Caption = 'lblDescPATPAC'
        end
        object lblEstadoDebito: TLabel
          Left = 181
          Top = 76
          Width = 74
          Height = 13
          Caption = 'lblEstadoDebito'
        end
        object lbl7: TLabel
          Left = 12
          Top = 76
          Width = 106
          Height = 13
          Caption = 'Estado del D'#233'bito:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lblMedioPagoAutomatico: TLabel
          Left = 181
          Top = 56
          Width = 75
          Height = 13
          AutoSize = False
          Caption = 'lblMedioPagoAutomatico'
        end
        object lblCapRespuesta: TLabel
          Left = 13
          Top = 96
          Width = 106
          Height = 13
          Caption = 'Pago Rechazado: '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lblRespuestaDebito: TLabel
          Left = 182
          Top = 96
          Width = 92
          Height = 13
          Caption = 'lblRespuestaDebito'
        end
      end
      object pnl1: TPanel
        Left = 0
        Top = 338
        Width = 857
        Height = 41
        Align = alBottom
        TabOrder = 2
        DesignSize = (
          857
          41)
        object btnSalir: TButton
          Left = 754
          Top = 6
          Width = 75
          Height = 25
          Anchors = [akRight, akBottom]
          Cancel = True
          Caption = '&Salir'
          TabOrder = 1
          OnClick = btnSalirClick
        end
        object btnCastigar: TButton
          Left = 2
          Top = 6
          Width = 196
          Height = 25
          Anchors = [akLeft, akBottom]
          Caption = 'Castigar Convenio'
          TabOrder = 0
          OnClick = btnCastigarClick
        end
      end
    end
    object tsCuotasCastigo: TTabSheet
      Caption = ' Cuotas Castigadas'
      ImageIndex = 2
      object pnl2: TPanel
        Left = 0
        Top = 338
        Width = 857
        Height = 41
        Align = alBottom
        TabOrder = 0
        OnClick = pnl2Click
        DesignSize = (
          857
          41)
        object btnAnularCastigo: TButton
          Left = 13
          Top = 6
          Width = 196
          Height = 25
          Anchors = [akLeft, akBottom]
          Caption = 'Anular Castigo'
          TabOrder = 0
          OnClick = btnAnularCastigoClick
        end
        object btn1: TButton
          Left = 774
          Top = 11
          Width = 75
          Height = 25
          Anchors = [akRight, akBottom]
          Cancel = True
          Caption = '&Salir'
          TabOrder = 1
          OnClick = btn1Click
        end
      end
      object lst1: TDBListEx
        Left = 0
        Top = 34
        Width = 857
        Height = 274
        Align = alClient
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 75
            Header.Caption = 'Castigado'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = True
            FieldName = 'Cobrar'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 140
            Header.Caption = 'Tipo Comprobante'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'TipoComprobante'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'N'#250'mero Comprobante'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'NumeroComprobante'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 100
            Header.Caption = 'Fecha Emisi'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'FechaEmision'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 100
            Header.Caption = 'Total Pagado'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'TotalComprobante'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 100
            Header.Caption = 'Estado Pago'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'EstadoPago'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 120
            Header.Caption = 'C'#243'digo Convenio'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'CodigoConvenio'
          end>
        DataSource = ds_ComprobantesCastigados
        DragReorder = True
        ParentColor = False
        TabOrder = 1
        TabStop = True
        OnClick = lst1Click
        OnDrawText = lst1DrawText
      end
      object pnl3: TPanel
        Left = 0
        Top = 308
        Width = 857
        Height = 30
        Align = alBottom
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Color = 14408667
        ParentBackground = False
        TabOrder = 2
        DesignSize = (
          857
          30)
        object lbl8: TLabel
          Left = 456
          Top = 11
          Width = 93
          Height = 13
          Alignment = taRightJustify
          Anchors = [akRight, akBottom]
          Caption = 'Total Comprobante:'
        end
        object lblCuotasCantidad: TLabel
          Left = 565
          Top = 11
          Width = 52
          Height = 13
          Alignment = taRightJustify
          Anchors = [akRight, akBottom]
          AutoSize = False
          Caption = '0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbl9: TLabel
          Left = 660
          Top = 11
          Width = 95
          Height = 13
          Anchors = [akRight, akBottom]
          Caption = 'Total Seleccionado:'
        end
        object lblCuotasTotal: TLabel
          Left = 761
          Top = 9
          Width = 80
          Height = 13
          Alignment = taRightJustify
          Anchors = [akRight, akBottom]
          AutoSize = False
          Caption = '$ 0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ExplicitLeft = 802
        end
      end
      object pnl4: TPanel
        Left = 0
        Top = 0
        Width = 857
        Height = 34
        Align = alTop
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Color = 14408667
        ParentBackground = False
        TabOrder = 3
        object btnComprobantesCastigos: TButton
          Left = 22
          Top = 8
          Width = 199
          Height = 20
          Caption = 'Consultar Comprobantes Castigados'
          TabOrder = 0
          OnClick = btnComprobantesCastigosClick
        end
      end
    end
    object tsRefinanciaciones: TTabSheet
      Caption = ' Refinanciaciones '
      ImageIndex = 1
      TabVisible = False
      object pnl5: TPanel
        Left = 0
        Top = 338
        Width = 857
        Height = 41
        Align = alBottom
        TabOrder = 0
        DesignSize = (
          857
          41)
        object btnActivarRefinanciacion: TButton
          Left = 2
          Top = 11
          Width = 196
          Height = 25
          Anchors = [akLeft, akBottom]
          Caption = 'Activar Refinanciaci'#243'n (F9)'
          TabOrder = 0
        end
        object btn_Salir: TButton
          Left = 774
          Top = 11
          Width = 75
          Height = 25
          Anchors = [akRight, akBottom]
          Cancel = True
          Caption = '&Salir'
          TabOrder = 1
        end
        object btnConsultarRefinanciacion: TButton
          Left = 204
          Top = 11
          Width = 75
          Height = 25
          Caption = 'Consultar'
          TabOrder = 2
        end
      end
      object lst2: TDBListEx
        Left = 0
        Top = 0
        Width = 857
        Height = 338
        Align = alClient
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 50
            Header.Caption = 'Nro.'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taRightJustify
            IsLink = False
            FieldName = 'CodigoRefinanciacion'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 125
            Header.Caption = 'Tipo'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'Tipo'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 85
            Header.Caption = 'Fecha'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'Fecha'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 200
            Header.Caption = 'Estado'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'Estado'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 65
            Header.Caption = 'Validada'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'Validada'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 150
            Header.Caption = 'Convenio'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            IsLink = False
            FieldName = 'Convenio'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 95
            Header.Caption = 'Total Deuda'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = [fsBold]
            Header.Alignment = taRightJustify
            IsLink = False
            FieldName = 'TotalDeuda'
          end>
        DragReorder = True
        ParentColor = False
        TabOrder = 1
        TabStop = True
      end
    end
  end
  object dsCuotasAPagar: TDataSource
    DataSet = cdsCuotasAPagar
    Left = 48
    Top = 256
  end
  object ds_Comprobantes: TDataSource
    DataSet = cds_Comprobantes
    Left = 160
    Top = 244
  end
  object cds_Comprobantes: TClientDataSet
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Cobrar'
        DataType = ftBoolean
      end
      item
        Name = 'TipoComprobante'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'NumeroComprobante'
        DataType = ftLargeint
      end
      item
        Name = 'DescriComprobante'
        DataType = ftString
        Size = 35
      end
      item
        Name = 'CodigoConvenio'
        DataType = ftInteger
      end
      item
        Name = 'CodigoPersona'
        DataType = ftInteger
      end
      item
        Name = 'TotalComprobante'
        DataType = ftInteger
      end
      item
        Name = 'EstadoPago'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EstadoDebito'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'DescriEstado'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'UltimoComprobante'
        DataType = ftLargeint
      end
      item
        Name = 'FechaEmision'
        DataType = ftDateTime
      end
      item
        Name = 'FechaVencimiento'
        DataType = ftDateTime
      end
      item
        Name = 'TotalPagos'
        DataType = ftInteger
      end
      item
        Name = 'TotalPagosStr'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'SaldoPendiente'
        DataType = ftInteger
      end
      item
        Name = 'SaldoPendienteDescri'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'TotalComprobanteDescri'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'DescTipoMedioPAgo'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'DescPATPAC'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'Rechazo'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'TipoComprobanteFiscal'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'NumeroComprobanteFiscal'
        DataType = ftLargeint
      end
      item
        Name = 'DescriComprobanteFiscal'
        DataType = ftString
        Size = 35
      end
      item
        Name = 'EstadoRefinanciacion'
        DataType = ftSmallint
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 164
    Top = 300
    Data = {
      100300009619E0BD010000001800000019000000000003000000100306436F62
      72617202000300000000000F5469706F436F6D70726F62616E74650100490000
      000100055749445448020002001400114E756D65726F436F6D70726F62616E74
      65080001000000000011446573637269436F6D70726F62616E74650100490000
      0001000557494454480200020023000E436F6469676F436F6E76656E696F0400
      0100000000000D436F6469676F506572736F6E61040001000000000010546F74
      616C436F6D70726F62616E746504000100000000000A45737461646F5061676F
      01004900000001000557494454480200020001000C45737461646F4465626974
      6F01004900000001000557494454480200020001000C44657363726945737461
      646F010049000000010005574944544802000200140011556C74696D6F436F6D
      70726F62616E746508000100000000000C4665636861456D6973696F6E080008
      000000000010466563686156656E63696D69656E746F08000800000000000A54
      6F74616C5061676F7304000100000000000D546F74616C5061676F7353747201
      004900000001000557494454480200020014000E53616C646F50656E6469656E
      746504000100000000001453616C646F50656E6469656E746544657363726901
      0049000000010005574944544802000200140016546F74616C436F6D70726F62
      616E746544657363726901004900000001000557494454480200020014001144
      6573635469706F4D6564696F5041676F01004900000001000557494454480200
      020032000A446573635041545041430100490000000100055749445448020002
      0032000752656368617A6F010049000000010005574944544802000200320015
      5469706F436F6D70726F62616E746546697363616C0100490000000100055749
      445448020002001400174E756D65726F436F6D70726F62616E74654669736361
      6C080001000000000017446573637269436F6D70726F62616E74654669736361
      6C01004900000001000557494454480200020023001445737461646F52656669
      6E616E63696163696F6E02000100000000000000}
  end
  object cdsCuotasAPagar: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspCuotasAPagar'
    Left = 48
    Top = 352
    object cdsCuotasAPagarRut: TStringField
      FieldName = 'Rut'
      FixedChar = True
      Size = 11
    end
    object cdsCuotasAPagarNombrePersona: TStringField
      FieldName = 'NombrePersona'
      ReadOnly = True
      Size = 100
    end
    object cdsCuotasAPagarCodigoRefinanciacion: TIntegerField
      FieldName = 'CodigoRefinanciacion'
    end
    object cdsCuotasAPagarCodigoCuota: TSmallintField
      FieldName = 'CodigoCuota'
    end
    object cdsCuotasAPagarCodigoEstadoCuota: TSmallintField
      FieldName = 'CodigoEstadoCuota'
    end
    object cdsCuotasAPagarDescripcionEstadoCuota: TStringField
      FieldName = 'DescripcionEstadoCuota'
      Size = 30
    end
    object cdsCuotasAPagarCodigoFormaPago: TSmallintField
      FieldName = 'CodigoFormaPago'
    end
    object cdsCuotasAPagarDescripcionFormaPago: TStringField
      FieldName = 'DescripcionFormaPago'
      Size = 30
    end
    object cdsCuotasAPagarTitularNombre: TStringField
      FieldName = 'TitularNombre'
      Size = 100
    end
    object cdsCuotasAPagarTitularTipoDocumento: TStringField
      FieldName = 'TitularTipoDocumento'
      FixedChar = True
      Size = 4
    end
    object cdsCuotasAPagarTitularNumeroDocumento: TStringField
      FieldName = 'TitularNumeroDocumento'
      Size = 11
    end
    object cdsCuotasAPagarCodigoBanco: TIntegerField
      FieldName = 'CodigoBanco'
    end
    object cdsCuotasAPagarDescripcionBanco: TStringField
      FieldName = 'DescripcionBanco'
      Size = 50
    end
    object cdsCuotasAPagarDocumentoNumero: TStringField
      FieldName = 'DocumentoNumero'
    end
    object cdsCuotasAPagarFechaCuota: TDateTimeField
      FieldName = 'FechaCuota'
    end
    object cdsCuotasAPagarImporte: TLargeintField
      FieldName = 'Importe'
      ReadOnly = True
    end
    object cdsCuotasAPagarImporteDesc: TStringField
      FieldName = 'ImporteDesc'
      ReadOnly = True
    end
    object cdsCuotasAPagarEstadoImpago: TBooleanField
      FieldName = 'EstadoImpago'
    end
    object cdsCuotasAPagarNumeroConvenio: TStringField
      FieldName = 'NumeroConvenio'
      ReadOnly = True
      FixedChar = True
      Size = 30
    end
    object cdsCuotasAPagarSaldo: TLargeintField
      FieldName = 'Saldo'
      ReadOnly = True
    end
    object cdsCuotasAPagarSaldoDesc: TStringField
      FieldName = 'SaldoDesc'
      ReadOnly = True
    end
    object cdsCuotasAPagarCodigoTarjeta: TIntegerField
      FieldName = 'CodigoTarjeta'
    end
    object cdsCuotasAPagarDescripcionTarjeta: TStringField
      FieldName = 'DescripcionTarjeta'
      FixedChar = True
      Size = 50
    end
    object cdsCuotasAPagarTarjetaCuotas: TSmallintField
      FieldName = 'TarjetaCuotas'
    end
    object cdsCuotasAPagarCuotaNueva: TBooleanField
      FieldKind = fkInternalCalc
      FieldName = 'CuotaNueva'
    end
    object cdsCuotasAPagarCuotaModificada: TBooleanField
      FieldKind = fkInternalCalc
      FieldName = 'CuotaModificada'
    end
    object cdsCuotasAPagarCodigoTipoMedioPago: TSmallintField
      FieldName = 'CodigoTipoMedioPago'
    end
    object cdsCuotasAPagarCodigoEntradaUsuario: TStringField
      FieldName = 'CodigoEntradaUsuario'
      FixedChar = True
      Size = 2
    end
    object cdsCuotasAPagarCodigoCuotaAntecesora: TSmallintField
      FieldName = 'CodigoCuotaAntecesora'
    end
    object cdsCuotasAPagarCobrar: TBooleanField
      FieldKind = fkInternalCalc
      FieldName = 'Cobrar'
    end
  end
  object dspCuotasAPagar: TDataSetProvider
    DataSet = spObtenerRefinanciacionCuotasAPagar
    Left = 48
    Top = 304
  end
  object spObtenerRefinanciacionCuotasAPagar: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerRefinanciacionCuotasAPagar'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@RUTPersona'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 192
    Top = 352
  end
  object cdsComprobantesImpagos: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspCuotasAPagar'
    Left = 312
    Top = 328
    object StringField1: TStringField
      FieldName = 'Rut'
      FixedChar = True
      Size = 11
    end
    object StringField2: TStringField
      FieldName = 'NombrePersona'
      ReadOnly = True
      Size = 100
    end
    object IntegerField1: TIntegerField
      FieldName = 'CodigoRefinanciacion'
    end
    object SmallintField1: TSmallintField
      FieldName = 'CodigoCuota'
    end
    object SmallintField2: TSmallintField
      FieldName = 'CodigoEstadoCuota'
    end
    object StringField3: TStringField
      FieldName = 'DescripcionEstadoCuota'
      Size = 30
    end
    object SmallintField3: TSmallintField
      FieldName = 'CodigoFormaPago'
    end
    object StringField4: TStringField
      FieldName = 'DescripcionFormaPago'
      Size = 30
    end
    object StringField5: TStringField
      FieldName = 'TitularNombre'
      Size = 100
    end
    object StringField6: TStringField
      FieldName = 'TitularTipoDocumento'
      FixedChar = True
      Size = 4
    end
    object StringField7: TStringField
      FieldName = 'TitularNumeroDocumento'
      Size = 11
    end
    object IntegerField2: TIntegerField
      FieldName = 'CodigoBanco'
    end
    object StringField8: TStringField
      FieldName = 'DescripcionBanco'
      Size = 50
    end
    object StringField9: TStringField
      FieldName = 'DocumentoNumero'
    end
    object DateTimeField1: TDateTimeField
      FieldName = 'FechaCuota'
    end
    object LargeintField1: TLargeintField
      FieldName = 'Importe'
      ReadOnly = True
    end
    object StringField10: TStringField
      FieldName = 'ImporteDesc'
      ReadOnly = True
    end
    object BooleanField1: TBooleanField
      FieldName = 'EstadoImpago'
    end
    object StringField11: TStringField
      FieldName = 'NumeroConvenio'
      ReadOnly = True
      FixedChar = True
      Size = 30
    end
    object LargeintField2: TLargeintField
      FieldName = 'Saldo'
      ReadOnly = True
    end
    object StringField12: TStringField
      FieldName = 'SaldoDesc'
      ReadOnly = True
    end
    object IntegerField3: TIntegerField
      FieldName = 'CodigoTarjeta'
    end
    object StringField13: TStringField
      FieldName = 'DescripcionTarjeta'
      FixedChar = True
      Size = 50
    end
    object SmallintField4: TSmallintField
      FieldName = 'TarjetaCuotas'
    end
    object BooleanField2: TBooleanField
      FieldKind = fkInternalCalc
      FieldName = 'CuotaNueva'
    end
    object BooleanField3: TBooleanField
      FieldKind = fkInternalCalc
      FieldName = 'CuotaModificada'
    end
    object SmallintField5: TSmallintField
      FieldName = 'CodigoTipoMedioPago'
    end
    object StringField14: TStringField
      FieldName = 'CodigoEntradaUsuario'
      FixedChar = True
      Size = 2
    end
    object SmallintField6: TSmallintField
      FieldName = 'CodigoCuotaAntecesora'
    end
    object BooleanField4: TBooleanField
      FieldKind = fkInternalCalc
      FieldName = 'Cobrar'
    end
  end
  object ilCheck: TImageList
    BkColor = clWhite
    Left = 808
    Top = 280
    Bitmap = {
      494C0101030008009C0010001000FFFFFF00FF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF000000FF000000FF000000
      FF000000FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000FF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF000000FF000000FF000000
      FF000000FF000000FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000FF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF000000FF00FFFFFF000000
      FF000000FF000000FF000000FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000FF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF000000FF00FFFFFF00FFFF
      FF000000FF000000FF000000FF000000FF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000FF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF000000
      000000000000FFFFFF00000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF000000FF00FFFFFF00FFFF
      FF00FFFFFF000000FF000000FF000000FF000000FF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000FF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF000000FF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000FF000000FF000000FF000000FF00FFFFFF00FFFF
      FF00FFFFFF000000FF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF000000FF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000FF000000FF000000FF000000FF00FFFF
      FF00FFFFFF000000FF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF000000FF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF000000FF000000FF000000
      FF00FFFFFF000000FF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF000000FF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF000000FF000000
      FF000000FF000000FF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF000000FF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF000000
      FF000000FF000000FF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF000000FF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      FF000000FF000000FF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFF00000001000100010000
      0001000100010000000100010001000000010001000100000001000100010000
      0001000100010000000100010001000000010001000100000001000100010000
      0001000100010000000100010001000000010001000100000001000100010000
      0001000100010000000100010001000000000000000000000000000000000000
      000000000000}
  end
  object spObtenerDeudaComprobantesVencidos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDeudaComprobantesVencidos'
    Parameters = <>
    Left = 416
    Top = 352
  end
  object spObtenerRefinanciacionesTotalesPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerRefinanciacionesTotalesPersona'
    Parameters = <>
    Left = 704
    Top = 352
  end
  object spAnularPagosComprobantesCastigados: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'AnularPagosComprobantesCastigados'
    Parameters = <>
    Left = 520
    Top = 248
  end
  object sp_ActualizarClienteMoroso: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarClienteMoroso'
    Parameters = <
      item
        Name = '@NumeroDocumento'
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@CodigoRefinanciacion'
        DataType = ftInteger
        Value = Null
      end
      item
        Name = '@Usuario'
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@ErrorDescription'
        DataType = ftString
        Direction = pdOutput
        Size = -1
        Value = Null
      end>
    Left = 520
    Top = 304
  end
  object spObtenerDomicilioFacturacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerDomicilioFacturacion;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Domicilio'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 255
        Value = ''
      end
      item
        Name = '@Comuna'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = ''
      end
      item
        Name = '@Region'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = ''
      end>
    Left = 708
    Top = 243
  end
  object spObtenerComprobantesCastigados: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerComprobantesCastigadosxConvenio'
    Parameters = <>
    Left = 712
    Top = 304
  end
  object cdsComprobantesCastigados: TClientDataSet
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Cobrar'
        DataType = ftBoolean
      end
      item
        Name = 'TipoComprobante'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'NumeroComprobante'
        DataType = ftLargeint
      end
      item
        Name = 'DescriComprobante'
        DataType = ftString
        Size = 35
      end
      item
        Name = 'CodigoConvenio'
        DataType = ftInteger
      end
      item
        Name = 'CodigoPersona'
        DataType = ftInteger
      end
      item
        Name = 'TotalComprobante'
        DataType = ftInteger
      end
      item
        Name = 'EstadoPago'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EstadoDebito'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'DescriEstado'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'UltimoComprobante'
        DataType = ftLargeint
      end
      item
        Name = 'FechaEmision'
        DataType = ftDateTime
      end
      item
        Name = 'FechaVencimiento'
        DataType = ftDateTime
      end
      item
        Name = 'TotalPagos'
        DataType = ftInteger
      end
      item
        Name = 'TotalPagosStr'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'SaldoPendiente'
        DataType = ftInteger
      end
      item
        Name = 'SaldoPendienteDescri'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'TotalComprobanteDescri'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'DescTipoMedioPAgo'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'DescPATPAC'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'Rechazo'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'TipoComprobanteFiscal'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'NumeroComprobanteFiscal'
        DataType = ftLargeint
      end
      item
        Name = 'DescriComprobanteFiscal'
        DataType = ftString
        Size = 35
      end
      item
        Name = 'EstadoRefinanciacion'
        DataType = ftSmallint
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 252
    Top = 276
    Data = {
      100300009619E0BD010000001800000019000000000003000000100306436F62
      72617202000300000000000F5469706F436F6D70726F62616E74650100490000
      000100055749445448020002001400114E756D65726F436F6D70726F62616E74
      65080001000000000011446573637269436F6D70726F62616E74650100490000
      0001000557494454480200020023000E436F6469676F436F6E76656E696F0400
      0100000000000D436F6469676F506572736F6E61040001000000000010546F74
      616C436F6D70726F62616E746504000100000000000A45737461646F5061676F
      01004900000001000557494454480200020001000C45737461646F4465626974
      6F01004900000001000557494454480200020001000C44657363726945737461
      646F010049000000010005574944544802000200140011556C74696D6F436F6D
      70726F62616E746508000100000000000C4665636861456D6973696F6E080008
      000000000010466563686156656E63696D69656E746F08000800000000000A54
      6F74616C5061676F7304000100000000000D546F74616C5061676F7353747201
      004900000001000557494454480200020014000E53616C646F50656E6469656E
      746504000100000000001453616C646F50656E6469656E746544657363726901
      0049000000010005574944544802000200140016546F74616C436F6D70726F62
      616E746544657363726901004900000001000557494454480200020014001144
      6573635469706F4D6564696F5041676F01004900000001000557494454480200
      020032000A446573635041545041430100490000000100055749445448020002
      0032000752656368617A6F010049000000010005574944544802000200320015
      5469706F436F6D70726F62616E746546697363616C0100490000000100055749
      445448020002001400174E756D65726F436F6D70726F62616E74654669736361
      6C080001000000000017446573637269436F6D70726F62616E74654669736361
      6C01004900000001000557494454480200020023001445737461646F52656669
      6E616E63696163696F6E02000100000000000000}
  end
  object ds_ComprobantesCastigados: TDataSource
    DataSet = cdsComprobantesCastigados
    Left = 256
    Top = 224
  end
  object spActualizarEstadoConvenio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarEstadoConvenio'
    Parameters = <>
    Left = 304
    Top = 439
  end
  object spEliminarEstadoConvenio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarEstadoConvenio'
    Parameters = <>
    Left = 304
    Top = 472
  end
  object spIngresarConvenioACobranzaInterna: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'IngresarConvenioACobranzaInterna'
    Parameters = <>
    Left = 728
    Top = 448
  end
end
