unit StartupSAU;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  jpeg, ExtCtrls, StdCtrls;

type
  TFormStartup = class(TForm)
    Bevel1: TBevel;
    Bevel2: TBevel;
    Bevel3: TBevel;
    Bevel4: TBevel;
    Image1: TImage;
    lblTitulo: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormStartup: TFormStartup;

implementation

{$R *.DFM}

procedure TFormStartup.FormCreate(Sender: TObject);
resourcestring
    LABEL_TITULO_COB = 'Cobranzas';
begin
    lblTitulo.Caption := LABEL_TITULO_COB;
end;

end.
