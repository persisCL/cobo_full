object FormAnaliticoCierreDeTurno: TFormAnaliticoCierreDeTurno
  Left = 437
  Top = 228
  BorderStyle = bsDialog
  Caption = 'Informe Anal'#237'tico de Cierre de Turno'
  ClientHeight = 168
  ClientWidth = 338
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object bv_Datos: TBevel
    Left = 8
    Top = 8
    Width = 322
    Height = 124
  end
  object lbl_NumeroTurno: TLabel
    Left = 71
    Top = 76
    Width = 58
    Height = 13
    Caption = 'N'#176' de Turno'
  end
  object lbl_Usuario: TLabel
    Left = 71
    Top = 51
    Width = 39
    Height = 13
    Caption = 'Usuario:'
  end
  object pnl_Botones: TPanel
    Left = 0
    Top = 137
    Width = 338
    Height = 31
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    DesignSize = (
      338
      31)
    object btn_Aceptar: TButton
      Left = 174
      Top = 3
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '&Aceptar'
      Default = True
      TabOrder = 0
      OnClick = btn_AceptarClick
    end
    object btn_Cancelar: TButton
      Left = 254
      Top = 3
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = '&Cancelar'
      TabOrder = 1
      OnClick = btn_CancelarClick
    end
  end
  object txt_NumeroTurno: TBuscaTabEdit
    Left = 136
    Top = 72
    Width = 130
    Height = 21
    Hint = 'N'#250'mero del Turno para el cual obtener el informe'
    AutoSize = False
    Color = 16444382
    Enabled = True
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    Decimals = 0
    EditorStyle = bteNumericEdit
    OnButtonClick = txt_NumeroTurnoButtonClick
    BuscaTabla = bt_Turnos
  end
  object cb_Usuario: TComboBox
    Left = 136
    Top = 47
    Width = 130
    Height = 21
    Hint = 'Seleccione un usuario para poder seleccionar sus turnos'
    Style = csDropDownList
    ItemHeight = 13
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
  end
  object sp_EsTurnoCerrado: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EsTurnoCerrado;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ExisteTurno'
        Attributes = [paNullable]
        DataType = ftBoolean
        Direction = pdInputOutput
        Value = Null
      end
      item
        Name = '@EsCerrado'
        Attributes = [paNullable]
        DataType = ftBoolean
        Direction = pdInputOutput
        Value = Null
      end
      item
        Name = '@UsuarioTurno'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 20
        Value = Null
      end
      item
        Name = '@PuntoVenta'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 60
        Value = Null
      end>
    Left = 6
    Top = 140
  end
  object bt_Turnos: TBuscaTabla
    Caption = 'Turnos Cerrados'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    HelpContext = 0
    Dataset = sp_ObtenerListaTurnos
    OnProcess = bt_TurnosProcess
    OnSelect = bt_TurnosSelect
    Left = 40
    Top = 140
  end
  object sp_ObtenerListaTurnos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerListaTurnos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = ''
      end>
    Left = 76
    Top = 140
  end
end
