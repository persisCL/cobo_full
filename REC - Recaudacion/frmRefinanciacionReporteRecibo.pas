{-----------------------------------------------------------------------------
  Unit:      Reporte Recibo
  Author:    gcasais
  Date:      26-Abr-2005
------------------------------------------------------------------------------
  IMPORTANTE!!!!!
  Para que el reporte se imprima con el tama�o adecuado, la fuente utilizada
  debe ser FontB11 de tama�o 9. Esta es una fuente que tiene el driver de la
  impresora Epson Tm-T88 III Receipt. Por lo tanto, al recompilar esta unit se
  debe tener instalada la fuente.



Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.

Firma       :   SS_1147_MCA_20150216
Descripcion :   se reemplaza execProc x Open dado que daba error al abrir el sp

-----------------------------------------------------------------------------}
unit frmRefinanciacionReporteRecibo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UtilRB, ppCtrls, ppBands, ppStrtch, ppRegion, ppClass, ppVar,
  ppPrnabl, ppCache, ppProd, ppReport, ppDB, ppDBBDE, ppComm, ppRelatv,
  ppDBPipe, DB, ADODB, peaTypes, DMConnection, RBSetup,
  pptypes, peaprocs, ExtCtrls, jpeg, ppBarCod, ppParameter, ppSubRpt,
  ppModule, raCodMod, UtilProc, RStrings, Util, UtilDB, ConstParametrosGenerales,
  Printers;

type
    TRefinanciacionReporteReciboForm = class(TForm)
    dsDatosRecibo: TDataSource;
  	rbi_Recibo: TRBInterface;
  	ppDatosRecibo: TppDBPipeline;
    spObtenerRefinanciacionCuotaPagoRecibo: TADOStoredProc;
    rp_Recibo: TppReport;
    ppHeaderBand1: TppHeaderBand;
    pptxt_Domicilio: TppDBText;
    pplbl_Concesionaria: TppLabel;
    pplbl_RUTConcesionaria: TppLabel;
    pplbl_DireccionConcesionaria: TppLabel;
    pptxt_NumeroDocumento: TppDBText;
    pptxt_DescripcionComuna: TppDBText;
    pptxt_DescripcionRegion: TppDBText;
    pplbl_LocalidadConcesionaria: TppLabel;
    pptxt_Nombre: TppDBText;
    pptxt_NumeroRecibo: TppDBText;
    pplbl_Nombre: TppLabel;
    pplbl_NumeroRUT: TppLabel;
    pplbl_Domicilio: TppLabel;
    pplbl_Comuna: TppLabel;
    pplbl_Region: TppLabel;
    pplbl_DatosCliente: TppLabel;
    pplbl_NumeroRecibo: TppLabel;
    pplbl_Fecha: TppLabel;
    pptxt_Fecha: TppDBText;
  	ppDetailBand3: TppDetailBand;
    pptxt_TipoComprobante: TppDBText;
    pptxt_ImporteCancelado: TppDBText;
    pptxt_NumeroComprobanteCancelado: TppDBText;
  	ppFooterBand1: TppFooterBand;
  	ppSummaryBand3: TppSummaryBand;
    pplbl_TotalCancelado: TppLabel;
  	ppParameterList1: TppParameterList;
    pplbl_FirmaConcesionaria: TppLabel;
  	ppLogo: TppImage;
    pplblLineaDatosCliente: TppLabel;
    pplbl_LineaTotalCancelado: TppLabel;
    pplbl_LineaFirmaConcesionaria: TppLabel;
    ppLabel1: TppLabel;
    ppDBText1: TppDBText;
    ppDomicilioMuestra: TppLabel;
    pplblSeparador: TppLabel;
    ppDBText2: TppDBText;
    ppDBCalc1: TppDBCalc;
    pplblAnulado: TppLabel;     						//SS_989_PDO_20120119
    spObtenerMaestroConcesionaria: TADOStoredProc;     	//SS_1147_NDR_20140710
	  procedure rbi_ReciboExecute(Sender: TObject; var Cancelled: Boolean);
  private
	  { Private declarations }
	  FNumeroRecibo : Integer;
  public
	  { Public declarations }
	  function Inicializar (Logo: TBitmap; NumeroRecibo: Integer; MostrarConfigurarImpresora: Boolean; ModalPreview: Boolean): Boolean;  // SS_989_PDO_20120119
  end;

implementation

uses PeaProcsCN, SysUtilsCN;

{$R *.dfm}
{-----------------------------------------------------------------------------
  Procedure: Inicializar
  Author:    gcasais
  Date:      26-Abr-2005
  Comments:  Inicializaci�n de este formulario
  Result:    None

Revision 1:
    Author : ggomez
    Date : 22/06/2006
    Description :
        - Correg� ortograf�a del resourcestring.
        - Agregu� comentarios.
        - Agregu� nolock al query que obtiene la Fecha de Creaci�n.
        - Modifiqu� para que informe de los errores en forma m�s detallada y
        usando el MsgBoxErr.
------------------------------------------------------------------------------}
function TRefinanciacionReporteReciboForm.Inicializar(
    Logo: TBitmap;
    NumeroRecibo: Integer;
    MostrarConfigurarImpresora: Boolean;
    ModalPreview: Boolean): Boolean;

resourcestring
    MSG_ERROR_OBTENCION_DATOS_CONFIG_REPORTE = 'Error al obtener datos de configuraci�n del reporte.'
                                                + CRLF + 'Consulte al Administrador de Sistemas';
    MSG_ERROR_OBTENER_FECHA_CORTE_RUT = 'Ha ocurrido un error al obtener la fecha desde la cual la concesionaria cambio el RUT.';
    MSG_ERROR_OBTENER_RUT_ANTERIOR  = 'Ha ocurrido un error al obtener el RUT Anterior de la concesionaria.';
    MSG_ERROR_OBTENER_RUT           = 'Ha ocurrido un error al obtener el RUT de la concesionaria.';
    MSG_RUT                         = 'RUT: ';
var
    Cancelar: boolean;
    FechaCreacion , FechaDeCorte : TDateTime;
    Rut : String;
    sRazonSocial,sRut,sDireccion,sLocalidad: AnsiString;                                        //SS_1147_NDR_20140710
begin
    try
        try
            Screen.Cursor := crHourGlass;
            Application.ProcessMessages;

            with spObtenerMaestroConcesionaria do                                                               //SS_1147_NDR_20140710
            begin                                                                                               //SS_1147_NDR_20140710
              Close;                                                                                            //SS_1147_NDR_20140710
              Parameters.Refresh;                                                                               //SS_1147_NDR_20140710
              Parameters.ParamByName('@CodigoConcesionaria').Value := ObtenerCodigoConcesionariaNativa;         //SS_1147_NDR_20140710
              //ExecProc;                                                                                       //SS_1147_MCA_20150226  //SS_1147_NDR_20140710
              Open;                                                                                             //SS_1147_MCA_20150226
              sRazonSocial  := FieldByName('RazonSocialConcesionaria').AsString;                                //SS_1147_NDR_20140710
              sRut          := FormatFloat('#.#',FieldByName('RutConcesionaria').AsInteger) + '-' +             //SS_1147_NDR_20140710
                               FieldByName('DigitoVerificadorConcesionaria').AsString;                          //SS_1147_NDR_20140710
              sDireccion    := FieldByName('DireccionConcesionaria').AsString;                                  //SS_1147_NDR_20140710
              sLocalidad    := FieldByName('Comuna').AsString + '  ' +                                          //SS_1147_NDR_20140710
                               FieldByName('Region').AsString+ '  ' +                                           //SS_1147_NDR_20140710
                               FieldByName('Pais').AsString;                                                    //SS_1147_NDR_20140710
                                                                                                                //SS_1147_NDR_20140710
              pplbl_Concesionaria.Caption := sRazonSocial;                                                      //SS_1147_NDR_20140710
              pplbl_RUTConcesionaria.Caption := sRut;                                                           //SS_1147_NDR_20140710
              pplbl_DireccionConcesionaria.Caption := sDireccion;                                               //SS_1147_NDR_20140710
              pplbl_LocalidadConcesionaria.Caption := sLocalidad;                                               //SS_1147_NDR_20140710
              pplbl_FirmaConcesionaria.Caption := iif(ObtenerCodigoConcesionariaNativa = CODIGO_VS, 'p. Vespucio Sur', 'p. Costanera Norte');       //SS_1147_MCA_20150428
            end;                                                                                                //SS_1147_NDR_20140710
            Result := False;

            with spObtenerRefinanciacionCuotaPagoRecibo do begin
                Parameters.Refresh;
                //Parameters.ParamByName('@CodigoRefinanciacion').Value := CodigoRefinanciacion;        // SS_989_PDO_20120119
                //Parameters.ParamByName('@CodigoCuota').Value          := CodigoCuota;                 // SS_989_PDO_20120119
                Parameters.ParamByName('@NumeroRecibo').Value         := NumeroRecibo;
                Open;

                if not FieldByName('Domicilio2').IsNull then begin
                    pptxt_Domicilio.DataField := 'Domicilio2';
                    pptxt_DescripcionComuna.DataField := 'Comuna2';
                    pptxt_DescripcionRegion.DataField := 'Region2';
                end;

                pplblAnulado.Visible := FieldByName('ReciboAnulado').AsInteger = 1;                     // SS_989_PDO_20120119

                FechaCreacion := FieldByName('FechaEmisionRecibo').AsDateTime;
            end;

            ObtenerParametroGeneral(spObtenerRefinanciacionCuotaPagoRecibo.Connection, 'FECHA_DE_CORTE_RUT', FechaDeCorte);

            if ( FechaCreacion < FechaDeCorte )then begin
                ObtenerParametroGeneral(spObtenerRefinanciacionCuotaPagoRecibo.Connection, 'RUT_EMPRESA_ANTERIOR', Rut);
            end
            else begin
                ObtenerParametroGeneral(spObtenerRefinanciacionCuotaPagoRecibo.Connection, 'RUT_EMPRESA', Rut);
            end;

            pplbl_RUTConcesionaria.Caption := MSG_RUT + Rut;

            if Assigned(Logo) then begin
                //lo muestra
                ppLogo.Picture.Bitmap := Logo;
            end else begin
                //oculta la imagente
                ppLogo.Picture := nil;
                ppLogo.Visible := False;
            end;

            rbi_Recibo.ModalPreview := ModalPreview;
            //Permito configurar la impresora antes de generar el reporte
            if not MostrarConfigurarImpresora then rbi_ReciboExecute(nil, Cancelar);
            //Genero el Reporte
            Result := rbi_Recibo.Execute(MostrarConfigurarImpresora);
            
        except
            on e:Exception do begin
                raise EErrorExceptionCN.Create('Inicializar Recibo', e.Message);
            end;
        end;
    finally
        Screen.Cursor := crDefault;
    end;
end;

{-----------------------------------------------------------------------------
  Procedure: rbi_ReciboExecute
  Author:    gcasais
  Date:      26-Abr-2005
  Comments:  Inicializaci�n de este formulario
  Result:    None
------------------------------------------------------------------------------}
procedure TRefinanciacionReporteReciboForm.rbi_ReciboExecute(Sender: TObject; var Cancelled: Boolean);
resourcestring
    MSG_ERROR_AL_IMPRIMIR_RECIBO = 'Error al imprimir el recibo';
    MSG_ERROR_OBTENER_DATOS_RECIBO = 'Error al obtener los Datos del Recibo';
    MSG_ERROR_OBTENER_DATOS_MEDIO_PAGO = 'Error al obtener los datos del Medio de Pago';
begin
    try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        //Ajusta la posici�n del domicilio
        ppDomicilioMuestra.Visible := True;
        ppDomicilioMuestra.Caption := pptxt_Domicilio.Text;
        (* Si el Domicilio no entra en una l�nea, aumentar el height del
        componente del domicilio y bajar el resto de los controles. *)
        if ppDomicilioMuestra.Width > pptxt_Domicilio.Width then begin
            //Bajar los labels
            pplbl_Comuna.Top :=
                pplbl_Comuna.Top + pptxt_Domicilio.Height;
            pplbl_Region.Top :=
                pplbl_Region.Top + pptxt_Domicilio.Height;
            pplblSeparador.top :=
                pplblSeparador.top + pptxt_Domicilio.Height;
            pptxt_TipoComprobante.top :=
                pptxt_TipoComprobante.Top + pptxt_Domicilio.Height;
            pptxt_NumeroComprobanteCancelado.Top :=
                pptxt_NumeroComprobanteCancelado.Top + pptxt_Domicilio.Height;
//            pplbl_Convenio.Top := pplbl_Convenio.Top + pptxt_Domicilio.Height;
            //Bajar los dbText
            pptxt_DescripcionComuna.Top := pplbl_Comuna.Top;
            pptxt_DescripcionRegion.Top := pplbl_Region.Top;
//            pptxt_NumeroConvenio.Top := pplbl_Convenio.Top;
            //Aumentar el height del dbText del domicilio
            pptxt_Domicilio.Height := pptxt_Domicilio.Height * 2;
            pptxt_Domicilio.WordWrap := True;
            pptxt_Domicilio.AutoSize := False;
        end;

        ppDomicilioMuestra.Visible := False;
    finally
        Screen.Cursor := crDefault;
    end;
end;

initialization

finalization

end.

