{-------------------------------------------------------------------------------


Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.

--------------------------------------------------------------------------------}

unit FrmRepoEstadodeCaja;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ADODB, DB, ppCtrls, ppDB, ppDBPipe, ppBands, ppStrtch, ppRegion,
  ppClass, ppVar, ppPrnabl, ppCache, ppComm, ppRelatv, ppProd, ppReport,
  UtilRB, DMConnection, StdCtrls, ppSubRpt, Utildb, Util, utilproc,
  ppMemo, FrmConfigReporteEstadoCaja, ppParameter;

type
  TFormEstadoDeCaja = class(TForm)
    rbi_EstadoDeCaja: TRBInterface;
    ppDetalleCobrosEfectivo: TppDBPipeline;
    dsCobroEfectivo: TDataSource;
    ReporteEstadoCajaEfectivo: TADOStoredProc;
    rp_EstadoDeCaja: TppReport;
    ppHeaderBand3: TppHeaderBand;
    ppImage1: TppImage;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLine1: TppLine;
    pplblUsuario: TppLabel;
    ppDetailBand3: TppDetailBand;
    ppFooterBand3: TppFooterBand;
    ppLine3: TppLine;
    ppLabel4: TppLabel;
    ppSystemVariable3: TppSystemVariable;
    ppDetalleCobrosTarjeta: TppDBPipeline;
    dsCobrosTarjeta: TDataSource;
    ReporteEstadoCajaTarjeta: TADOStoredProc;
    ppRegionEfectivo: TppRegion;
    ppSubReportEfectivo: TppSubReport;
    ppChildReport1: TppChildReport;
    ppTitleBand1: TppTitleBand;
    ppShape3: TppShape;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLabel8: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    ppSummaryBand1: TppSummaryBand;
    ppShape1: TppShape;
    ppLabel9: TppLabel;
    ppSumaEfectivo: TppDBCalc;
    ppRegionTarjeta: TppRegion;
    ppSubReportTarjeta: TppSubReport;
    ppChildReport2: TppChildReport;
    ppTitleBand2: TppTitleBand;
    ppDetailBand2: TppDetailBand;
    ppSummaryBand2: TppSummaryBand;
    ppShape2: TppShape;
    ppLabel14: TppLabel;
    ppLabel15: TppLabel;
    ppLabel16: TppLabel;
    ppLabel17: TppLabel;
    ppLabel18: TppLabel;
    ppLabel19: TppLabel;
    ppLabel20: TppLabel;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppDBText7: TppDBText;
    ppDBText8: TppDBText;
    ppDBText9: TppDBText;
    ppShape4: TppShape;
    ppLabel22: TppLabel;
    ppSumaTarjeta: TppDBCalc;
    ppDetalleAjustes: TppDBPipeline;
    dsDetalleAjustes: TDataSource;
    ReporteEstadoCajaAjustes: TADOStoredProc;
    ppRegionAjustes: TppRegion;
    ppSubReportDetalleAjustes: TppSubReport;
    ppChildReport3: TppChildReport;
    ppTitleBand3: TppTitleBand;
    ppDetailBand4: TppDetailBand;
    ppSummaryBand3: TppSummaryBand;
    ppShape5: TppShape;
    ppLabel24: TppLabel;
    ppLabel25: TppLabel;
    ppLabel26: TppLabel;
    ppLabel27: TppLabel;
    ppLabel28: TppLabel;
    ppLabel29: TppLabel;
    ppLabel30: TppLabel;
    ppDBText10: TppDBText;
    ppDBText11: TppDBText;
    ppDBText12: TppDBText;
    ppDBText13: TppDBText;
    ppDBMemo1: TppDBMemo;
    ppDBText14: TppDBText;
    ppShape6: TppShape;
    ppLabel31: TppLabel;
    ppSumaAjustes: TppDBCalc;
    ppLabel33: TppLabel;
    pplblTurno: TppLabel;
    ppLabel34: TppLabel;
    pplblFecha: TppLabel;
    qry_DetallesTurno: TADOQuery;
    qry_DetallesTurnoNumeroTurno: TIntegerField;
    qry_DetallesTurnoCodigoUsuario: TStringField;
    qry_DetallesTurnoFechaHoraApertura: TDateTimeField;
    qry_DetallesTurnoFechaHoraCierre: TDateTimeField;
    qry_DetallesTurnoMontoApertura: TBCDField;
    ppLine2: TppLine;
    ppRegion1: TppRegion;
    ppLabel35: TppLabel;
    ppDetalleTotal: TppDBPipeline;
    dsDetalleTotal: TDataSource;
    ReporteEstadoCajaTotal: TADOStoredProc;
    ppDBCalc1: TppDBCalc;
    ppLabel12: TppLabel;
    ppDBCalc2: TppDBCalc;
    procedure rbi_EstadoDeCajaSelect(Sender: TObject);
    procedure ReporteEstadoCajaEfectivoBeforeOpen(DataSet: TDataSet);
    procedure ReporteEstadoCajaTarjetaBeforeOpen(DataSet: TDataSet);
    procedure ReporteEstadoCajaAjustesBeforeOpen(DataSet: TDataSet);
    procedure qry_DetallesTurnoBeforeOpen(DataSet: TDataSet);
    procedure rp_EstadoDeCajaBeforePrint(Sender: TObject);
    procedure rp_EstadoDeCajaCancel(Sender: TObject);
    procedure ReporteEstadoCajaTotalBeforeOpen(DataSet: TDataSet);
  private
    { Private declarations }
    FNumeroTurno: integer;
    rptNumeroTurno: integer;
  public
    { Public declarations }
    function Inicializa: Boolean;
    function Ejecutar:Boolean;
    property NumeroTurno: integer write FNumeroTurno;
  end;

var
  FormEstadoDeCaja: TFormEstadoDeCaja;

implementation

{$R *.dfm}

function TFormEstadoDeCaja.Ejecutar: Boolean;
resourcestring
    CAPTION_REPORTE = 'Reporte Estado de Caja';
    MSG_EJECUTAR_REPORTE = 'No se pudo ejecutar el reporte de Estado de Caja.';
begin
	Result := False;
    try
        try
            rbi_EstadoDeCaja.Execute(True);
            Result := True;
        except
            On E: Exception do begin
                MsgBoxErr(MSG_EJECUTAR_REPORTE, e.message, CAPTION_REPORTE, MB_ICONSTOP);
            end;
        end;
    finally
        ReporteEstadoCajaEfectivo.Close;
        ReporteEstadoCajaTarjeta.Close;
        ReporteEstadoCajaAjustes.Close;
        ReporteEstadoCajaTotal.Close;
    end;
end;

function TFormEstadoDeCaja.Inicializa: Boolean;
var                                                                             //SS_1147_NDR_20140710
  RutaLogo:AnsiString;                                                          //SS_1147_NDR_20140710
begin
  ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
  try                                                                                                 //SS_1147_NDR_20140710
    ppImage1.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
  except                                                                                              //SS_1147_NDR_20140710
    On E: Exception do begin                                                                          //SS_1147_NDR_20140710
      Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
      MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
      Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
    end;                                                                                              //SS_1147_NDR_20140710
  end;                                                                                                //SS_1147_NDR_20140710

  if ObtenerCodigoConcesionariaNativa = CODIGO_CN then                                                //SS_1147_NDR_20140710
  begin                                                                                               //SS_1147_NDR_20140710
    ppLabel2.Caption :='Av. Presidente Kennedy 5757, Torre Oriente, Oficina 802';                     //SS_1147_NDR_20140710                                                                                //SS_1147_NDR_20140710
  end                                                                                                 //SS_1147_NDR_20140710
  else if Parameters.ParamByName('@CodigoConcesionaria').Value = CODIGO_VS then                       //SS_1147_NDR_20140710
  begin                                                                                               //SS_1147_NDR_20140710
    ppLabel2.Caption := 'Direccion Sucursal VS';                                                      //SS_1147_NDR_20140710
  end;                                                                                                //SS_1147_NDR_20140710

	rptNumeroTurno := FNumeroTurno;
	ReporteEstadoCajaEfectivo.Close;
    ReporteEstadoCajaTarjeta.Close;
    ReporteEstadoCajaAjustes.Close;
    ReporteEstadoCajaTotal.Close;
	Result := True;
end;


procedure TFormEstadoDeCaja.rbi_EstadoDeCajaSelect(Sender: TObject);
Var
	f: TFormConfigReporteEstadoCaja;
begin
	Application.CreateForm(TFormConfigReporteEstadoCaja, f);
    f.NumeroTurno :=  FNumeroTurno;
	if f.Inicializa and ( f.ShowModal = mrOK)  then begin
    	rptNumeroTurno := f.NumeroTurno;
    end;
	f.Release;
end;

procedure TFormEstadoDeCaja.ReporteEstadoCajaEfectivoBeforeOpen(
  DataSet: TDataSet);
begin
    ReporteEstadoCajaEfectivo.Close;
	ReporteEstadoCajaEfectivo.Parameters.ParamByName('@NumeroTurno').Value 		:= rptNumeroTurno;
end;

procedure TFormEstadoDeCaja.ReporteEstadoCajaTarjetaBeforeOpen(
  DataSet: TDataSet);
begin
	ReporteEstadoCajaTarjeta.Close;
	ReporteEstadoCajaTarjeta.Parameters.ParamByName('@NumeroTurno').Value 		:= rptNumeroTurno;
end;

procedure TFormEstadoDeCaja.ReporteEstadoCajaAjustesBeforeOpen(
  DataSet: TDataSet);
begin
	ReporteEstadoCajaAjustes.Close;
	ReporteEstadoCajaAjustes.Parameters.ParamByName('@NumeroTurno').Value	:= rptNumeroTurno;
end;

procedure TFormEstadoDeCaja.qry_DetallesTurnoBeforeOpen(DataSet: TDataSet);
begin
	qry_DetallesTurno.Close;
	qry_DetallesTurno.Parameters.ParamByName('Turno').Value	:= rptNumeroTurno;
end;

procedure TFormEstadoDeCaja.rp_EstadoDeCajaBeforePrint(Sender: TObject);
resourcestring
    MSG_APERTURA = 'Apertura: ';
    MSG_ABIERTO  = 'Abierto';
    MSG_CIERRE   = 'Cierre: ';
begin
    ReporteEstadoCajaEfectivo.Active 	:= True;
    ReporteEstadoCajaTarjeta.Active 	:= True;
    ReporteEstadoCajaAjustes.Active		:= True;
    ReporteEstadoCajaTotal.Active		:= True;
    qry_DetallesTurno.Open;
    pplblTurno.Caption	:= Trim(Istr(rptNumeroTurno));
    pplblFecha.Caption:= PadR(MSG_APERTURA +  FormatDateTime('dd/mm/yyyy, hh:mm', qry_DetallesTurno.FieldByName('FechaHoraApertura').AsDateTime), 40, ' ') +
        PadR( iif(qry_DetallesTurno.FieldByName('FechaHoraCierre').AsDateTime <= 0, MSG_ABIERTO, MSG_CIERRE + FormatDateTime('dd/mm/yyyy, hh:mm', qry_DetallesTurno.FieldByName('FechaHoraCierre').AsDateTime)), 40, ' ');
    pplblUsuario.Caption := UsuarioSistema;
end;

procedure TFormEstadoDeCaja.rp_EstadoDeCajaCancel(Sender: TObject);
begin
    ReporteEstadoCajaEfectivo.Close;
    ReporteEstadoCajaTarjeta.Close;
    ReporteEstadoCajaAjustes.Close;
    ReporteEstadoCajaTotal.Close;
end;

procedure TFormEstadoDeCaja.ReporteEstadoCajaTotalBeforeOpen(
  DataSet: TDataSet);
begin
	ReporteEstadoCajaTotal.Close;
	ReporteEstadoCajaTotal.Parameters.ParamByName('@NumeroTurno').Value	:= rptNumeroTurno;
end;

end.
