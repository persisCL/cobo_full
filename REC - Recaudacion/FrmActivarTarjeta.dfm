object FormActivarTarjeta: TFormActivarTarjeta
  Left = 264
  Top = 229
  BorderStyle = bsDialog
  Caption = 'Activar Tarjeta Prepaga'
  ClientHeight = 294
  ClientWidth = 526
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  DesignSize = (
    526
    294)
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 10
    Top = 8
    Width = 507
    Height = 249
    Anchors = [akLeft, akTop, akRight, akBottom]
  end
  object Label2: TLabel
    Left = 22
    Top = 24
    Width = 90
    Height = 13
    Caption = 'Cuentas Prepagas:'
  end
  object Label3: TLabel
    Left = 22
    Top = 51
    Width = 91
    Height = 13
    Caption = 'N'#250'mero de Tarjeta:'
  end
  object Label4: TLabel
    Left = 22
    Top = 78
    Width = 30
    Height = 13
    Caption = 'Clave:'
  end
  object Label1: TLabel
    Left = 24
    Top = 184
    Width = 124
    Height = 13
    Caption = 'Saldo actual de la cuenta:'
  end
  object lSaldoActual: TLabel
    Left = 185
    Top = 184
    Width = 104
    Height = 15
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'lSaldoActual'
  end
  object Label5: TLabel
    Left = 24
    Top = 208
    Width = 74
    Height = 13
    Caption = 'Importe Tarjeta:'
  end
  object lImporteTarjeta: TLabel
    Left = 185
    Top = 208
    Width = 104
    Height = 16
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'lSaldo'
  end
  object lProximoSaldo: TLabel
    Left = 185
    Top = 232
    Width = 104
    Height = 16
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'lSaldo'
  end
  object Label8: TLabel
    Left = 24
    Top = 232
    Width = 130
    Height = 13
    Caption = 'Pr'#243'ximo saldo de la cuenta:'
  end
  object Label6: TLabel
    Left = 24
    Top = 112
    Width = 98
    Height = 13
    Caption = 'Estado de la Tarjeta:'
  end
  object LEstadoTarjeta: TLabel
    Left = 137
    Top = 112
    Width = 88
    Height = 13
    AutoSize = False
    Caption = 'lTarjetaEstado'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label7: TLabel
    Left = 24
    Top = 136
    Width = 85
    Height = 13
    Caption = 'Fecha activaci'#243'n:'
  end
  object LFechaActivacion: TLabel
    Left = 137
    Top = 136
    Width = 368
    Height = 13
    AutoSize = False
    Caption = 'lSaldoActual'
  end
  object LCuentaActivada: TLabel
    Left = 137
    Top = 156
    Width = 82
    Height = 13
    Caption = 'LCuentaActivada'
  end
  object cb_Cuentas: TComboBox
    Left = 120
    Top = 19
    Width = 325
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 0
    OnChange = cb_CuentasChange
  end
  object btnAceptar: TDPSButton
    Left = 362
    Top = 264
    Anchors = [akRight, akBottom]
    Caption = '&Aceptar'
    Default = True
    Enabled = False
    TabOrder = 3
    OnClick = btnAceptarClick
  end
  object btn_Salir: TDPSButton
    Left = 441
    Top = 264
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Salir'
    TabOrder = 4
    OnClick = btn_SalirClick
  end
  object txt_NumeroTarjeta: TNumericEdit
    Left = 120
    Top = 46
    Width = 121
    Height = 21
    MaxLength = 8
    TabOrder = 1
    OnChange = txt_NumeroTarjetaChange
    Decimals = 0
  end
  object txt_Clave: TEdit
    Left = 120
    Top = 74
    Width = 121
    Height = 21
    MaxLength = 4
    TabOrder = 2
    OnChange = txt_ClaveChange
  end
  object ActivarTarjetaPrepaga: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActivarTarjetaPrepaga'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroTarjeta'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCuenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@DescriError'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 255
        Value = Null
      end>
    Left = 356
    Top = 48
  end
  object qryObtenerSaldoCuenta: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'CodigoCuenta'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'
      
        '  Saldo = CAST(SUM(MovimientosCuentas.importe) AS DECIMAL(14,2))' +
        ' / 100,'
      
        '  DescriSaldo = dbo.FloatToStr(CAST(SUM(MovimientosCuentas.impor' +
        'te) AS DECIMAL(14,2)) / 100)'
      'FROM'
      '  MovimientosCuentas  WITH (NOLOCK) '
      'WHERE'
      
        '  (MovimientosCuentas.CodigoConcepto = 19 OR MovimientosCuentas.' +
        'CodigoConcepto = 20) AND'
      '  MovimientosCuentas.CodigoCuenta = :CodigoCuenta')
    Left = 432
    Top = 48
  end
  object ObtenerTarjeta: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerTarjeta'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@NumeroTarjeta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 395
    Top = 48
  end
end
