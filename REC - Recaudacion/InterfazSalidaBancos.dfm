object FrmInterfazSalidaBancos: TFrmInterfazSalidaBancos
  Left = 266
  Top = 281
  BorderStyle = bsDialog
  Caption = 'Interfaz de Salida de Bancos'
  ClientHeight = 103
  ClientWidth = 457
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 457
    Height = 65
    BevelOuter = bvLowered
    TabOrder = 0
    object Label4: TLabel
      Left = 10
      Top = 10
      Width = 102
      Height = 13
      Caption = '&Entidad Bancaria:'
      FocusControl = cbBancos
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object cbBancos: TComboBox
      Left = 144
      Top = 7
      Width = 305
      Height = 19
      Style = csOwnerDrawFixed
      ItemHeight = 13
      TabOrder = 0
    end
    object ProgressBar: TProgressBar
      Left = 13
      Top = 40
      Width = 433
      Height = 16
      Min = 0
      Max = 100
      TabOrder = 1
    end
  end
  object btnAceptar: TDPSButton
    Left = 296
    Top = 72
    Caption = '&Aceptar'
    Default = True
    ModalResult = 1
    TabOrder = 1
    OnClick = btnAceptarClick
  end
  object btnCancelar: TDPSButton
    Left = 376
    Top = 72
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 2
    OnClick = btnCancelarClick
  end
  object InterfazSalidaDebitoAutomatico: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'InterfazSalidaDebitoAutomatico'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoDebito'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@CodigoEntidad'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 168
    Top = 56
  end
  object UpdateFechaDebitado: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'FechaDebitado'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'TipoComprobante'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end
      item
        Name = 'NumeroComprobante'
        Attributes = [paSigned]
        DataType = ftBCD
        Precision = 18
        Size = 19
        Value = Null
      end>
    SQL.Strings = (
      'UPDATE COMPROBANTES'
      ' SET FechaDebitado = :FechaDebitado'
      'WHERE TipoComprobante = :TipoComprobante'
      'AND NumeroComprobante = :NumeroComprobante'
      '         ')
    Left = 200
    Top = 56
  end
end
