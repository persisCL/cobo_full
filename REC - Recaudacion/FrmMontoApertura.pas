unit FrmMontoApertura;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DmiCtrls, ExtCtrls, DPSControls, PeaProcs;

type
  TFormMontoApertura = class(TForm)
    btn_Aceptar: TDPSButton;
    btn_Omitir: TDPSButton;
    Bevel1: TBevel;
    txt_Monto: TNumericEdit;
    Label2: TLabel;
    Label1: TLabel;
    procedure btn_AceptarClick(Sender: TObject);
    procedure btn_OmitirClick(Sender: TObject);
  private
    FNumeroTurno: Integer;

    { Private declarations }
  public
    { Public declarations }
    function inicializar(NumeroTurno: Integer; FormCaption: ShortString): Boolean;
  end;

var
  FormMontoApertura: TFormMontoApertura;

implementation

uses DMConnection;

{$R *.dfm}

procedure TFormMontoApertura.btn_AceptarClick(Sender: TObject);
begin
    if not ActualizarMontoTurno(DMConnections.BaseCAC, FNumeroTurno, txt_Monto.Value) then
        Exit;

	ModalResult := mrOk;
end;

procedure TFormMontoApertura.btn_OmitirClick(Sender: TObject);
begin
	Close;
end;

function TFormMontoApertura.inicializar(NumeroTurno: Integer; FormCaption: ShortString): Boolean;
begin
    FNumeroTurno := NumeroTurno;

    Caption := FormCaption;

	Result := True;
end;

end.
