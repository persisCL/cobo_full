object FormVentaDayPass: TFormVentaDayPass
  Left = 124
  Top = 159
  BorderStyle = bsSingle
  Caption = 'Venta de Pases'
  ClientHeight = 323
  ClientWidth = 742
  Color = clBtnFace
  Constraints.MinHeight = 350
  Constraints.MinWidth = 750
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poMainFormCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object GBDatosPase: TGroupBox
    Left = 0
    Top = 0
    Width = 742
    Height = 74
    Align = alTop
    Caption = ' Datos del pase '
    TabOrder = 0
    object Label2: TLabel
      Left = 11
      Top = 22
      Width = 102
      Height = 13
      Caption = '&Tipo de veh'#237'culo:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label13: TLabel
      Left = 12
      Top = 47
      Width = 49
      Height = 13
      Caption = 'Patente:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label38: TLabel
      Left = 428
      Top = 22
      Width = 61
      Height = 13
      Caption = 'Categor'#237'a:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 354
      Top = 47
      Width = 238
      Height = 13
      Caption = 'Pases diarios adquiridos con anterioridad:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LblCategoria: TLabel
      Left = 494
      Top = 22
      Width = 97
      Height = 13
      Caption = '<sin especificar>'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LblCantidadPases: TLabel
      Left = 604
      Top = 47
      Width = 8
      Height = 13
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object cb_TipoVehiculo: TVariantComboBox
      Left = 126
      Top = 17
      Width = 223
      Height = 21
      Hint = 'Tipo de veh'#237'culo'
      Style = vcsDropDownList
      Color = 16444382
      ItemHeight = 13
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnChange = cb_TipoVehiculoChange
      Items = <>
    end
    object Panel4: TPanel
      Left = 645
      Top = 15
      Width = 95
      Height = 57
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 3
    end
    object txtPatente: TEdit
      Left = 126
      Top = 42
      Width = 108
      Height = 21
      Hint = 'Patente'
      CharCase = ecUpperCase
      Color = 16444382
      MaxLength = 10
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnChange = txtPatenteChange
    end
    object cbConAcoplado: TCheckBox
      Left = 352
      Top = 20
      Width = 66
      Height = 17
      Caption = 'Acoplado'
      Enabled = False
      TabOrder = 1
      OnClick = cbConAcopladoClick
    end
  end
  object GBPrincipal: TGroupBox
    Left = 0
    Top = 74
    Width = 742
    Height = 208
    Align = alClient
    TabOrder = 1
    object GBPasesSeleccionados: TGroupBox
      Left = 2
      Top = 15
      Width = 738
      Height = 191
      Align = alClient
      Caption = 'Pases seleccionados'
      TabOrder = 0
      object Panel1: TPanel
        Left = 2
        Top = 159
        Width = 734
        Height = 30
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        DesignSize = (
          734
          30)
        object Label4: TLabel
          Left = 537
          Top = 6
          Width = 69
          Height = 13
          Anchors = [akRight, akBottom]
          Caption = 'Monto total:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lblMontoTotal: TLabel
          Left = 613
          Top = 6
          Width = 8
          Height = 13
          Anchors = [akRight, akBottom]
          Caption = '0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label5: TLabel
          Left = 321
          Top = 6
          Width = 167
          Height = 13
          Anchors = [akRight, akBottom]
          Caption = 'Cantidad de pases a adquirir:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lblCantidadPasesVenta: TLabel
          Left = 503
          Top = 7
          Width = 8
          Height = 13
          Anchors = [akRight, akBottom]
          Caption = '0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label3: TLabel
          Left = 35
          Top = 13
          Width = 107
          Height = 13
          Caption = 'Vendido Anteriormente'
        end
        object Panel2: TPanel
          Left = 16
          Top = 12
          Width = 12
          Height = 14
          BevelOuter = bvNone
          Color = clRed
          TabOrder = 0
        end
      end
      object Panel3: TPanel
        Left = 653
        Top = 15
        Width = 83
        Height = 144
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtnModificar: TDPSButton
          Left = 5
          Top = 58
          Width = 70
          Hint = 'Modificar'
          Caption = '&Modificar'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtnModificarClick
        end
        object btnEliminar: TDPSButton
          Left = 5
          Top = 90
          Width = 70
          Hint = 'Eliminar'
          Caption = '&Eliminar'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = btnEliminarClick
        end
        object btnSeleccionar: TDPSButton
          Left = 5
          Top = 26
          Width = 67
          Hint = 'Agregar un pase para la venta'
          Caption = '&Agregar'
          ModalResult = 1
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = btnSeleccionarClick
        end
      end
      object GrillaPases: TDBListEx
        Left = 2
        Top = 15
        Width = 651
        Height = 144
        Hint = '< Doble click para modificar el pase >'
        Align = alClient
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 75
            Header.Caption = 'C'#243'digo'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Codigo'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 300
            Header.Caption = 'Descripci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Descripcion'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 90
            Header.Caption = 'Importe'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'DescImporte'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 75
            Header.Caption = 'Desde'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Desde'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 75
            Header.Caption = 'Hasta'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Hasta'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 55
            Header.Caption = 'Utilizado'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'Utilizado'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 0
            Header.Caption = 'EnEdicion'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'EnEdicion'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 0
            Header.Caption = 'DiasVencimientoActivacion'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'DiasVencimientoActivacion'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 0
            Header.Caption = 'Duracion'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Duracion'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 0
            Header.Caption = 'FechaVencimiento'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'FechaVencimiento'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 0
            Header.Caption = 'Importe'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Importe'
          end
          item
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 60
            Header.Caption = 'Categor'#237'a'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = False
            FieldName = 'Categoria'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 150
            Header.Caption = 'Fecha de Venta'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'FechaHoraCompra'
          end>
        DataSource = DSPases
        DragReorder = True
        ParentColor = False
        TabOrder = 2
        TabStop = True
        OnDblClick = GrillaPasesDblClick
        OnDrawText = GrillaPasesDrawText
      end
    end
  end
  object PanelAbajo: TPanel
    Left = 0
    Top = 282
    Width = 742
    Height = 41
    Align = alBottom
    BevelInner = bvLowered
    TabOrder = 2
    DesignSize = (
      742
      41)
    object BtnAceptar: TDPSButton
      Left = 494
      Top = 8
      Width = 79
      Height = 26
      Hint = 'Vender'
      Anchors = [akTop, akRight]
      Caption = '&Aceptar'
      Default = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtnAceptarClick
    end
    object BtnCancelar: TDPSButton
      Left = 576
      Top = 8
      Width = 79
      Height = 26
      Hint = 'Reinicia la pantalla para un nuevo ingreso'
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = 'C&ancelar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtnCancelarClick
    end
    object BtnSalir: TDPSButton
      Left = 661
      Top = 8
      Width = 70
      Height = 26
      Hint = 'Salir'
      Anchors = [akRight, akBottom]
      Caption = '&Salir'
      ModalResult = 1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = BtnSalirClick
    end
  end
  object qry_PasesDisponibles: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'xCategoria'
        DataType = ftWord
        Precision = 3
        Size = 1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT A.CodigoTipoDayPass as Codigo,'
      ' A.Descripcion as Descripcion, A.DiasDuracion as Duracion,'
      ' B.Importe, A.DiasVencimientoActivacion,'
      ' A.FechaDesde, A.FechaHasta,'
      #39'$ '#39' + Cast(Importe as Varchar(20)) as DescImporte,'
      'dbo.DescripcionDias(A.DiasDuracion) as DescDuracion,'
      'B.CodigoCategoria'
      'FROM TiposDayPass A, TiposDayPassCategorias B'
      'WHERE B.CodigoCategoria = :xCategoria'
      '  AND A.CodigoTipoDayPass = B.CodigoTipoDayPass'
      '  AND A.Activo = 1'
      ''
      '')
    Left = 248
    Top = 152
  end
  object DSPasesDisponibles: TDataSource
    DataSet = qry_PasesDisponibles
    Left = 277
    Top = 152
  end
  object TablaPases: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Codigo'
        DataType = ftInteger
      end
      item
        Name = 'Descripcion'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'DescImporte'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Desde'
        DataType = ftDate
      end
      item
        Name = 'Hasta'
        DataType = ftDate
      end
      item
        Name = 'EnEdicion'
        DataType = ftBoolean
      end
      item
        Name = 'DiasVencimientoActivacion'
        DataType = ftDate
      end
      item
        Name = 'FechaVencimiento'
        DataType = ftDate
      end
      item
        Name = 'Duracion'
        DataType = ftSmallint
      end
      item
        Name = 'Importe'
        DataType = ftInteger
      end
      item
        Name = 'Utilizado'
        DataType = ftBoolean
      end
      item
        Name = 'Vendido'
        DataType = ftBoolean
      end
      item
        Name = 'NumeroDayPass'
        DataType = ftLargeint
      end
      item
        Name = 'DesdeAnterior'
        DataType = ftDateTime
      end
      item
        Name = 'Categoria'
        DataType = ftInteger
      end
      item
        Name = 'FechaHoraCompra'
        DataType = ftDateTime
      end>
    IndexDefs = <
      item
        Name = 'OrdenByDesde'
        Fields = 'desde'
      end>
    IndexName = 'OrdenByDesde'
    Params = <>
    StoreDefs = True
    Left = 248
    Top = 181
  end
  object DSPases: TDataSource
    DataSet = TablaPases
    Left = 277
    Top = 181
  end
  object InsertarMaestroDayPass: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'InsertarMaestroDayPass;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Estado'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@CodigoTipoDayPass'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@FechaDesde'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaHasta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaVencimiento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaHoraCompra'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@Importe'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@DiasVencimientoActivacion'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Categoria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@TipoCompromante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 18
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 308
    Top = 181
  end
  object ObtenerDayPassPatente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDayPassPatente'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end>
    Left = 308
    Top = 152
  end
  object spActualizarMaestroDayPass: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarMaestroDayPass'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaDesde'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@NumeroDayPass'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end>
    Left = 341
    Top = 181
  end
end
