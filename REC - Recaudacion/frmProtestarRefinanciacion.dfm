object ProtestarRefinanciacionForm: TProtestarRefinanciacionForm
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = 'Protestar Refinanciaci'#243'n'
  ClientHeight = 544
  ClientWidth = 992
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  DesignSize = (
    992
    544)
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 8
    Top = 6
    Width = 976
    Height = 147
    Anchors = [akLeft, akTop, akRight]
    Caption = '  Datos Principales Refinanciaci'#243'n  '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    DesignSize = (
      976
      147)
    object Label1: TLabel
      Left = 24
      Top = 23
      Width = 24
      Height = 13
      Caption = 'Tipo:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 214
      Top = 23
      Width = 41
      Height = 13
      Caption = 'N'#250'mero:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 343
      Top = 23
      Width = 33
      Height = 13
      Caption = 'Fecha:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblNumRefinanciacion: TLabel
      Left = 259
      Top = 23
      Width = 64
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'NumRefi'
    end
    object Label5: TLabel
      Left = 588
      Top = 23
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = 'Estado :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ExplicitLeft = 553
    end
    object lblEstado: TLabel
      Left = 630
      Top = 23
      Width = 93
      Height = 13
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      AutoSize = False
      Caption = '<Estado>'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ExplicitLeft = 595
    end
    object Label6: TLabel
      Left = 749
      Top = 23
      Width = 117
      Height = 13
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = 'Importe Refinanciaci'#243'n :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ExplicitLeft = 714
    end
    object lblTotalRefinanciado2: TLabel
      Left = 883
      Top = 23
      Width = 80
      Height = 13
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      AutoSize = False
      Caption = '$ 0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ExplicitLeft = 848
    end
    object lblProtestada: TLabel
      Left = 660
      Top = 38
      Width = 63
      Height = 13
      Anchors = [akTop, akRight]
      Caption = 'Protestada'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
      ExplicitLeft = 711
    end
    object vcbTipoRefinanciacion: TVariantComboBox
      Left = 54
      Top = 20
      Width = 145
      Height = 21
      Hint = ' Seleccionar Tipo de Refinanciaci'#243'n '
      Style = vcsDropDownList
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ItemHeight = 13
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Items = <
        item
          Caption = 'Avenimiento'
          Value = 38
        end
        item
          Caption = 'Repactaci'#243'n'
          Value = 36
        end>
    end
    object GroupBox2: TGroupBox
      Left = 12
      Top = 67
      Width = 961
      Height = 70
      Anchors = [akLeft, akRight, akBottom]
      Caption = '  Datos Cliente  '
      TabOrder = 3
      object Label3: TLabel
        Left = 21
        Top = 23
        Width = 21
        Height = 13
        Caption = 'Rut:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblNombreCli: TLabel
        Left = 48
        Top = 45
        Width = 70
        Height = 13
        Caption = 'lblNombreCli'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label7: TLabel
        Left = 206
        Top = 23
        Width = 49
        Height = 13
        Caption = 'Convenio:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblPersoneria: TLabel
        Left = 127
        Top = 45
        Width = 61
        Height = 13
        Caption = 'lblPersoneria'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblConcesionaria: TLabel
        Left = 475
        Top = 23
        Width = 92
        Height = 13
        Caption = 'lblConcesionaria'
      end
      object peRut: TPickEdit
        Left = 48
        Top = 18
        Width = 143
        Height = 21
        Hint = ' Seleccionar el Cliente a Refinanciar '
        Enabled = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        EditorStyle = bteTextEdit
      end
      object vcbConvenios: TVariantComboBox
        Left = 261
        Top = 18
        Width = 208
        Height = 19
        Style = vcsOwnerDrawFixed
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ItemHeight = 13
        ParentFont = False
        TabOrder = 1
        OnDrawItem = vcbConveniosDrawItem
        Items = <>
      end
    end
    object dedtFechaRefi: TDateEdit
      Left = 385
      Top = 20
      Width = 104
      Height = 21
      AutoSelect = False
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      Date = -693594.000000000000000000
    end
    object cbNotificacion: TCheckBox
      Left = 54
      Top = 44
      Width = 158
      Height = 17
      Hint = ' Marcar si es Avenimiento con Notificaci'#243'n '
      Caption = 'Avenimiento con Notificaci'#243'n'
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
    end
  end
  object GroupBox6: TGroupBox
    Left = 8
    Top = 159
    Width = 976
    Height = 339
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = '  Detalle Refinanciaci'#243'n  '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    DesignSize = (
      976
      339)
    object PageControl1: TPageControl
      Left = 8
      Top = 18
      Width = 961
      Height = 314
      ActivePage = tbsCuotas
      Anchors = [akLeft, akTop, akRight, akBottom]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      HotTrack = True
      ParentFont = False
      TabOrder = 0
      object tbsCuotas: TTabSheet
        Caption = ' Cuotas '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ImageIndex = 1
        ParentFont = False
        object DBListEx3: TDBListEx
          Left = 0
          Top = 0
          Width = 953
          Height = 256
          Align = alClient
          BorderStyle = bsSingle
          Columns = <
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 70
              Header.Caption = 'Protestar'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              IsLink = True
              FieldName = 'Protestar'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 30
              Header.Caption = 'ID'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'CodigoCuota'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 120
              Header.Caption = 'Forma de Pago'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              IsLink = False
              FieldName = 'DescripcionFormaPago'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 150
              Header.Caption = 'Estado'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              IsLink = False
              FieldName = 'DescripcionEstadoCuota'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 75
              Header.Caption = 'Fecha'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'FechaCuota'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 90
              Header.Caption = 'Importe'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              Header.Alignment = taRightJustify
              IsLink = False
              FieldName = 'Importe'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 90
              Header.Caption = 'Saldo'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              Header.Alignment = taRightJustify
              IsLink = False
              FieldName = 'Saldo'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 90
              Header.Caption = 'Rut'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              Header.Alignment = taRightJustify
              IsLink = False
              FieldName = 'TitularNumeroDocumento'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 150
              Header.Caption = 'Titular'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              IsLink = False
              FieldName = 'TitularNombre'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 120
              Header.Caption = 'Nro. Doc.'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              IsLink = False
              FieldName = 'DocumentoNumero'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 175
              Header.Caption = 'Banco'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              IsLink = False
              FieldName = 'DescripcionBanco'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 175
              Header.Caption = 'Tipo Tarjeta'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              IsLink = False
              FieldName = 'DescripcionTarjeta'
            end
            item
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Width = 100
              Header.Caption = 'Cuotas Tarjeta'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -11
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = [fsBold]
              Header.Alignment = taRightJustify
              IsLink = False
              FieldName = 'TarjetaCuotas'
            end>
          DataSource = dsCuotas
          DragReorder = True
          ParentColor = False
          TabOrder = 0
          TabStop = True
          OnDrawText = DBListEx3DrawText
          OnLinkClick = DBListEx3LinkClick
        end
        object Panel7: TPanel
          Left = 0
          Top = 256
          Width = 953
          Height = 30
          Align = alBottom
          BevelInner = bvRaised
          BevelOuter = bvLowered
          Color = 14408667
          ParentBackground = False
          TabOrder = 1
          DesignSize = (
            953
            30)
          object Label18: TLabel
            Left = 691
            Top = 9
            Width = 38
            Height = 13
            Anchors = [akRight, akBottom]
            Caption = 'Cuotas:'
            ExplicitLeft = 742
          end
          object lblCuotasCantidad: TLabel
            Left = 724
            Top = 9
            Width = 36
            Height = 13
            Alignment = taRightJustify
            Anchors = [akRight, akBottom]
            AutoSize = False
            Caption = '0'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            ExplicitLeft = 775
          end
          object Label20: TLabel
            Left = 795
            Top = 9
            Width = 69
            Height = 13
            Anchors = [akRight, akBottom]
            Caption = 'Importe Total:'
            ExplicitLeft = 710
          end
          object lblCuotasTotal: TLabel
            Left = 865
            Top = 9
            Width = 80
            Height = 13
            Alignment = taRightJustify
            Anchors = [akRight, akBottom]
            AutoSize = False
            Caption = '$ 0'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            ExplicitLeft = 780
          end
        end
      end
    end
  end
  object pnlAcciones: TPanel
    Left = 0
    Top = 504
    Width = 992
    Height = 40
    Align = alBottom
    Anchors = []
    Color = 14408667
    ParentBackground = False
    TabOrder = 2
    DesignSize = (
      992
      40)
    object btnCancelar: TButton
      Left = 909
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Salir'
      TabOrder = 0
      OnClick = btnCancelarClick
    end
    object btnProtestarRefinanciacion: TButton
      Left = 8
      Top = 8
      Width = 196
      Height = 25
      Caption = 'Protestar Refinanciaci'#243'n'
      Enabled = False
      TabOrder = 1
      OnClick = btnProtestarRefinanciacionClick
    end
  end
  object spObtenerRefinanciacionCuotas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerRefinanciacionCuotas'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCuota'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@SinCuotasAnuladas'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 784
    Top = 288
  end
  object dspObtenerRefinanciacionCuotas: TDataSetProvider
    DataSet = spObtenerRefinanciacionCuotas
    Left = 760
    Top = 312
  end
  object cdsCuotas: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoCuota'
        DataType = ftSmallint
      end
      item
        Name = 'CodigoEstadoCuota'
        DataType = ftSmallint
      end
      item
        Name = 'DescripcionEstadoCuota'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'CodigoFormaPago'
        DataType = ftSmallint
      end
      item
        Name = 'DescripcionFormaPago'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'TitularNombre'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'TitularTipoDocumento'
        Attributes = [faFixed]
        DataType = ftString
        Size = 4
      end
      item
        Name = 'TitularNumeroDocumento'
        DataType = ftString
        Size = 11
      end
      item
        Name = 'CodigoBanco'
        DataType = ftInteger
      end
      item
        Name = 'DescripcionBanco'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'DocumentoNumero'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'FechaCuota'
        DataType = ftDateTime
      end
      item
        Name = 'CodigoTarjeta'
        DataType = ftInteger
      end
      item
        Name = 'DescripcionTarjeta'
        Attributes = [faFixed]
        DataType = ftString
        Size = 50
      end
      item
        Name = 'TarjetaCuotas'
        DataType = ftSmallint
      end
      item
        Name = 'Importe'
        DataType = ftLargeint
      end
      item
        Name = 'Saldo'
        DataType = ftLargeint
      end
      item
        Name = 'CodigoEntradaUsuario'
        Attributes = [faFixed]
        DataType = ftString
        Size = 2
      end
      item
        Name = 'EstadoImpago'
        DataType = ftBoolean
      end
      item
        Name = 'CodigoCuotaAntecesora'
        DataType = ftSmallint
      end>
    IndexDefs = <
      item
        Name = 'FechaCuota'
        Fields = 'FechaCuota'
      end>
    IndexName = 'FechaCuota'
    Params = <>
    ProviderName = 'dspObtenerRefinanciacionCuotas'
    StoreDefs = True
    Left = 712
    Top = 328
    object cdsCuotasCodigoCuota: TSmallintField
      FieldName = 'CodigoCuota'
    end
    object cdsCuotasCodigoEstadoCuota: TSmallintField
      FieldName = 'CodigoEstadoCuota'
    end
    object cdsCuotasDescripcionEstadoCuota: TStringField
      FieldName = 'DescripcionEstadoCuota'
      Size = 30
    end
    object cdsCuotasCodigoFormaPago: TSmallintField
      FieldName = 'CodigoFormaPago'
    end
    object cdsCuotasDescripcionFormaPago: TStringField
      FieldName = 'DescripcionFormaPago'
      Size = 30
    end
    object cdsCuotasTitularNombre: TStringField
      FieldName = 'TitularNombre'
      Size = 100
    end
    object cdsCuotasTitularTipoDocumento: TStringField
      FieldName = 'TitularTipoDocumento'
      FixedChar = True
      Size = 4
    end
    object cdsCuotasTitularNumeroDocumento: TStringField
      FieldName = 'TitularNumeroDocumento'
      Size = 11
    end
    object cdsCuotasCodigoBanco: TIntegerField
      FieldName = 'CodigoBanco'
    end
    object cdsCuotasDescripcionBanco: TStringField
      FieldName = 'DescripcionBanco'
      Size = 50
    end
    object cdsCuotasDocumentoNumero: TStringField
      FieldName = 'DocumentoNumero'
    end
    object cdsCuotasFechaCuota: TDateTimeField
      FieldName = 'FechaCuota'
    end
    object cdsCuotasCodigoTarjeta: TIntegerField
      FieldName = 'CodigoTarjeta'
    end
    object cdsCuotasDescripcionTarjeta: TStringField
      FieldName = 'DescripcionTarjeta'
      FixedChar = True
      Size = 50
    end
    object cdsCuotasTarjetaCuotas: TSmallintField
      FieldName = 'TarjetaCuotas'
    end
    object cdsCuotasImporte: TLargeintField
      FieldName = 'Importe'
    end
    object cdsCuotasSaldo: TLargeintField
      FieldName = 'Saldo'
    end
    object cdsCuotasProtestar: TBooleanField
      FieldKind = fkInternalCalc
      FieldName = 'Protestar'
    end
  end
  object dsCuotas: TDataSource
    DataSet = cdsCuotas
    Left = 696
    Top = 344
  end
  object spObtenerRefinanciacionDatosCabecera: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerRefinanciacionDatosCabecera'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 536
    Top = 280
  end
  object lnCheck: TImageList
    BkColor = clWhite
    Height = 15
    Width = 15
    Left = 119
    Top = 307
    Bitmap = {
      494C01010400090014000F000F00FFFFFF00FF10FFFFFFFFFFFFFFFF424D3600
      00000000000036000000280000003C0000001E0000000100200000000000201C
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      FF000000FF000000FF000000FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000FF00FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF000000FF000000FF000000FF000000FF000000
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      FF00FFFFFF00FFFFFF0000000000FFFFFF008080800080808000808080008080
      80008080800080808000808080008080800080808000FFFFFF0000000000FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      FF00FFFFFF000000FF000000FF000000FF000000FF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000FF00FFFFFF00FFFFFF0000000000FFFF
      FF00808080008080800080808000808080008080800080808000808080008080
      800080808000FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF000000FF00FFFFFF00FFFFFF000000FF000000
      FF000000FF000000FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      FF00FFFFFF00FFFFFF0000000000FFFFFF008080800080808000808080008080
      80008080800080808000808080008080800080808000FFFFFF0000000000FFFF
      FF00FFFFFF0000000000FFFFFF000000000000000000FFFFFF00000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      FF00FFFFFF00FFFFFF00FFFFFF000000FF000000FF000000FF000000FF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000FF00FFFFFF00FFFFFF0000000000FFFF
      FF00808080008080800080808000808080008080800080808000808080008080
      800080808000FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF000000FF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000FF000000FF000000FF000000FF00FFFFFF00FFFFFF00FFFFFF000000
      FF00FFFFFF00FFFFFF0000000000FFFFFF008080800080808000808080008080
      80008080800080808000808080008080800080808000FFFFFF0000000000FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF000000FF000000
      FF000000FF00FFFFFF00FFFFFF000000FF00FFFFFF00FFFFFF0000000000FFFF
      FF00808080008080800080808000808080008080800080808000808080008080
      800080808000FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF000000FF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000FF000000FF000000FF000000FF00FFFFFF000000
      FF00FFFFFF00FFFFFF0000000000FFFFFF008080800080808000808080008080
      80008080800080808000808080008080800080808000FFFFFF0000000000FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      FF000000FF000000FF000000FF000000FF00FFFFFF00FFFFFF0000000000FFFF
      FF00808080008080800080808000808080008080800080808000808080008080
      800080808000FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF000000FF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF000000FF000000FF000000
      FF00FFFFFF00FFFFFF0000000000FFFFFF008080800080808000808080008080
      80008080800080808000808080008080800080808000FFFFFF0000000000FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000FF000000FF000000FF00FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00424D3E000000000000003E000000
      280000003C0000001E0000000100010000000000F00000000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FFFFFFFFFFFFFFF080030006000C001080030006000C0010
      80030006000C001080030006000C001080030006000C001080030006000C0010
      80030006000C001080030006000C001080030006000C001080030006000C0010
      80030006000C001080030006000C001080030006000C0010FFFFFFFFFFFFFFF0
      00000000000000000000000000000000000000000000}
  end
  object spActualizarRefinanciacionCuotasEstados: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarRefinanciacionCuotasEstados'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCuota'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@CodigoEstadoCuota'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 400
    Top = 360
  end
  object spActualizarRefinanciacionProtestada: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarRefinanciacionProtestada'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRefinanciacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 568
    Top = 376
  end
end
