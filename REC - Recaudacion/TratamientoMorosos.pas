unit TratamientoMorosos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Dialogs, StdCtrls, validate,
  Dateedit, Buttons, ExtCtrls, utilProc, util, ADODB, DMConnection, ComCtrls, BuscaClientes, navigator,
  DPSGrid, ImgList, Grids, DBGrids, DB, peaProcs, UtilDB, DBClient, DmiCtrls, peatypes, CACProcs,
  DPSControls, Menus;

type
  TFrmTratamientoMorosos = class(TForm)
    btnSalir: TDPSButton;
    Panel1: TPanel;
    GroupBox2: TGroupBox;
    GroupBox1: TGroupBox;
    cbTiposAcciones: TComboBox;
    dbgComprobantesMorosos: TDPSGrid;
    Label5: TLabel;
    dsComprobantesMorosos: TDataSource;
    ObtenerMorosos: TADOStoredProc;
    dsInformacionHistorica: TDataSource;
    qryInformacionHistorica: TADOQuery;
    CrearHistorialMoroso: TADOStoredProc;
    btnEjecutar: TDPSButton;
    btnPostergar: TDPSButton;
    GroupBox3: TGroupBox;
    dbgInformacionHistorica: TDPSGrid;
    btnAccionAdicional: TDPSButton;
    PopupMenu1: TPopupMenu;
    qryConfiguracionNotificaciones: TADOQuery;
    InhabilitarTAG: TADOStoredProc;
    qry_TAGS: TADOQuery;
    ActualizarActionListEnPorceso: TADOStoredProc;
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure btnSalirClick(Sender: TObject);
    procedure cbTiposAccionesClick(Sender: TObject);
    procedure dsComprobantesMorososDataChange(Sender: TObject; Field: TField);
    procedure btnEjecutarClick(Sender: TObject);
    procedure btnPostergarClick(Sender: TObject);
    procedure btnAccionAdicionalClick(Sender: TObject);
    procedure PopUpMenu1ItemClick(Sender: TObject);
  private
	{ Private declarations }
    procedure ActivarBotones();
    procedure CrearRegistroHistorialMoroso(TipoComprobante: ShortString; NumeroComprobante: integer; IDAccion: integer; AccionAdicional: boolean; Postergar: boolean; CodigoMedioContacto: integer; CodigoPersona: integer);
  public
	{ Public declarations }
	function Inicializar(): Boolean;
  end;

implementation

{$R *.dfm}

{ TfrmAnularReimprimirFactura }

function TFrmTratamientoMorosos.Inicializar(): Boolean;
var
    S: TSize;
begin
  	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
    CargarConfiguracionNotificaciones(DMConnections.BaseCAC, cbTiposAcciones);
	Result := True;
end;

procedure TFrmTratamientoMorosos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree;
end;

procedure TFrmTratamientoMorosos.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TFrmTratamientoMorosos.cbTiposAccionesClick(Sender: TObject);
resourcestring
    MSG_ERROR = 'Error al obtener los comprobantes';
var
    Item: TMenuItem;
begin
    Screen.Cursor := crHourGlass;
    PopUpMenu1.Items.Clear;
    qryConfiguracionNotificaciones.Close;
    qryConfiguracionNotificaciones.Parameters.ParamByName('IDConfiguracionNotificacion').Value := Ival(StrRight(cbTiposAcciones.Text, 20));
	try
		qryConfiguracionNotificaciones.Open;
        while not (qryConfiguracionNotificaciones.Eof = True) do
        begin
            Item := TMenuItem.Create(Self);
            Item.Caption := qryConfiguracionNotificaciones.Fields.FieldByName('DescripcionAccion').AsString;
            Item.Tag := qryConfiguracionNotificaciones.Fields.FieldByName('ID').AsInteger;
            Item.OnClick := PopUpMenu1ItemClick;
            PopUpMenu1.Items.Add(Item);
            qryConfiguracionNotificaciones.Next;
        end;
        qryConfiguracionNotificaciones.Close
	except
	end;
	ObtenerMorosos.Close;
	ObtenerMorosos.Parameters.ParamByName('@IDConfiguracionNotificacion').Value := Ival(StrRight(cbTiposAcciones.Text, 20));
	try
		ObtenerMorosos.Open;
        ActivarBotones;
	except
		on e: exception do begin
            Screen.Cursor := crDefault;
			MsgBoxErr(MSG_ERROR, e.Message, Self.Caption, MB_ICONSTOP);
            Exit;
		end;
	end;
    Screen.Cursor := crDefault
end;

procedure TFrmTratamientoMorosos.dsComprobantesMorososDataChange(
  Sender: TObject; Field: TField);
resourcestring
    MSG_ERROR = 'Error al obtener el historial del comprobante';
begin
    Screen.Cursor := crHourGlass;
	qryInformacionHistorica.Close;
	qryInformacionHistorica.Parameters.ParamByName('TipoComprobante').Value := ObtenerMorosos.Fields.FieldByName('TipoComprobante').Value;
	qryInformacionHistorica.Parameters.ParamByName('NumeroComprobante').Value := ObtenerMorosos.Fields.FieldByName('NumeroComprobante').Value;
	try
		qryInformacionHistorica.Open;
        Screen.Cursor := crDefault
	except
		on e: exception do begin
            Screen.Cursor := crDefault;
			MsgBoxErr(MSG_ERROR, e.Message, Self.Caption, MB_ICONSTOP);
		end;
	end;
end;

procedure TFrmTratamientoMorosos.btnEjecutarClick(Sender: TObject);
var
    i: integer;
begin
    ObtenerMorosos.DisableControls;
    for i := 0 to dbgComprobantesMorosos.SelectedRows.Count - 1 do begin
        ObtenerMorosos.GotoBookmark(Pointer(dbgComprobantesMorosos.SelectedRows[i]));
        with ObtenerMorosos.Fields do
        begin
            CrearRegistroHistorialMoroso(FieldByName('TipoComprobante').AsString,
              FieldByName('NumeroComprobante').AsInteger, FieldByName('IDAccion').AsInteger, False, False,
              FieldByName('CodigoMedioContacto').AsInteger, FieldByName('CodigoPersona').AsInteger);
        end;
    end;
    ObtenerMorosos.EnableControls;
    cbTiposAccionesClick(Sender);
end;

procedure TFrmTratamientoMorosos.btnPostergarClick(Sender: TObject);
var
    i: integer;
begin
    ObtenerMorosos.DisableControls;
    for i := 0 to dbgComprobantesMorosos.SelectedRows.Count - 1 do begin
        ObtenerMorosos.GotoBookmark(Pointer(dbgComprobantesMorosos.SelectedRows[i]));
        with ObtenerMorosos.Fields do
        begin
            CrearRegistroHistorialMoroso(FieldByName('TipoComprobante').AsString,
              FieldByName('NumeroComprobante').AsInteger, FieldByName('IDAccion').AsInteger, False, True,
              FieldByName('CodigoMedioContacto').AsInteger, FieldByName('CodigoPersona').AsInteger);
        end;
    end;
    ObtenerMorosos.EnableControls;
    cbTiposAccionesClick(Sender);
end;

procedure TFrmTratamientoMorosos.btnAccionAdicionalClick(Sender: TObject);
begin
    PopUpMenu1.PopUp(Mouse.CursorPos.X, Mouse.CursorPos.Y);
end;

procedure TFrmTratamientoMorosos.CrearRegistroHistorialMoroso(TipoComprobante: ShortString;
  NumeroComprobante: integer; IDAccion: integer; AccionAdicional: boolean; Postergar: boolean;
  CodigoMedioContacto: integer; CodigoPersona: integer);
resourcestring
    MSG_ERROR = 'Error al ejecutar la acci�n';
var
    TextoAEnviar: AnsiString;
//    Legajo: Integer;
begin
    Screen.Cursor := crHourGlass;
(*    Legajo := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT CodigoPersona FROM MaestroPersonal WHERE CodigoUsuario = ''' + UsuarioSistema + ''''); *)
    TextoAEnviar := QueryGetValue(DMConnections.BaseCAC, 'SELECT TextoAEnviar FROM ConfiguracionNotificaciones WHERE ID = ' + IntToStr(ObtenerMorosos.Fields.FieldByName('IDAccion').Value));
	CrearHistorialMoroso.Parameters.ParamByName('@TipoComprobante').Value := TipoComprobante;
	CrearHistorialMoroso.Parameters.ParamByName('@NumeroComprobante').Value := NumeroComprobante;
	CrearHistorialMoroso.Parameters.ParamByName('@IDAccion').Value := IDAccion;
	CrearHistorialMoroso.Parameters.ParamByName('@AccionAdicional').Value := AccionAdicional;
	CrearHistorialMoroso.Parameters.ParamByName('@Postergacion').Value := Postergar;
	CrearHistorialMoroso.Parameters.ParamByName('@CodigoMedioContacto').Value := CodigoMedioContacto;
	CrearHistorialMoroso.Parameters.ParamByName('@CodigoPersona').Value := CodigoPersona;
	try
		CrearHistorialMoroso.ExecProc;
        case IDAccion of
            ACCION_ENVIO_LISTA_AMARILLA:
            begin
                qry_TAGS.Parameters.ParamByName('TipoComprobante').Value := ObtenerMorosos.Fields.FieldByName('TipoComprobante').Value;
                qry_TAGS.Parameters.ParamByName('NumeroComprobante').Value := ObtenerMorosos.Fields.FieldByName('NumeroComprobante').Value;
                qry_TAGS.Open;
                while not (qry_TAGS.Eof = True) do
                begin
                    ActualizarActionListEnPorceso.Parameters.ParamByName('@ContextMark').Value := qry_TAGS.Fields.FieldByName('ContextMark').Value;
                    ActualizarActionListEnPorceso.Parameters.ParamByName('@ContractSerialNumber').Value := qry_TAGS.Fields.FieldByName('ContractSerialNumber').Value;
                    ActualizarActionListEnPorceso.Parameters.ParamByName('@ClearBlackList').Value := NULL;
                    ActualizarActionListEnPorceso.Parameters.ParamByName('@SetBlackList').Value := NULL;
                    ActualizarActionListEnPorceso.Parameters.ParamByName('@ClearGreenList').Value := NULL;
                    ActualizarActionListEnPorceso.Parameters.ParamByName('@SetGreenList').Value := NULL;
                    ActualizarActionListEnPorceso.Parameters.ParamByName('@ClearGrayList').Value := NULL;
                    ActualizarActionListEnPorceso.Parameters.ParamByName('@SetGrayList').Value := NULL;
                    ActualizarActionListEnPorceso.Parameters.ParamByName('@ClearYellowList').Value := NULL;
                    ActualizarActionListEnPorceso.Parameters.ParamByName('@SetYellowList').Value := True;
                    ActualizarActionListEnPorceso.Parameters.ParamByName('@ExceptionHandling').Value := NULL;
                    ActualizarActionListEnPorceso.Parameters.ParamByName('@MMINotOk').Value := NULL;
                    ActualizarActionListEnPorceso.Parameters.ParamByName('@MMIContactOperator').Value := NULL;
                    ActualizarActionListEnPorceso.ExecProc;
                    qry_TAGS.Next;
                end;
                qry_TAGS.Close;
            end;
            ACCION_ENVIO_LISTA_NEGRA:
            begin
                qry_TAGS.Parameters.ParamByName('TipoComprobante').Value := ObtenerMorosos.Fields.FieldByName('TipoComprobante').Value;
                qry_TAGS.Parameters.ParamByName('NumeroComprobante').Value := ObtenerMorosos.Fields.FieldByName('NumeroComprobante').Value;
                qry_TAGS.Open;
                while not (qry_TAGS.Eof = True) do
                begin
                    InhabilitarTAG.Parameters.ParamByName('@ContextMark').Value := qry_TAGS.Fields.FieldByName('ContextMark').Value;
                    InhabilitarTAG.Parameters.ParamByName('@ContractSerialNumber').Value := qry_TAGS.Fields.FieldByName('ContractSerialNumber').Value;
                    InhabilitarTAG.Parameters.ParamByName('@Motivo').Value := MB_CUENTAMOROSA;
                    InhabilitarTAG.ExecProc;
                    qry_TAGS.Next;
                end;
                qry_TAGS.Close;
            end;
        end;
        (*** FIXME: completar
        If (ActualizarComunicacion(Legajo,UsuarioSistema,FechaHoraComunicacion,NullDate,0,0,'F',nil,CodigoComunicacion,MsgError) = True) then
        begin
            case ObtenerMorosos.Fields.FieldByName('CodigoMedioContacto').AsInteger of
                MC_EMAIL: TipoOS := OS_ENVIAR_MAIL_CLIENTE;
                MC_LETTER: TipoOS := OS_ENVIAR_CARTA_CLIENTE;
                MC_FAX: TipoOS := OS_ENVIAR_FAX_CLIENTE;
                MC_VOICE_MAIL: TipoOS := OS_ENVIAR_VOICE_MAIL_CLIENTE;
                else TipoOS := OS_LLAMAR_POR_TELEFONO_CLIENTE;
            end;
            GenerarOrdenServicioMorosos(DMConnections.BaseCAC, CodigoComunicacion, TipoOS, 5, TextoAEnviar, ObtenerMorosos.Fields.FieldByName('CodigoPersona').Value, DescriError);
            If not (Trim(DescriError) = '') then
            begin
                Screen.Cursor := crDefault;
                MsgBoxErr(MSG_ERROR, Trim(DescriError), Self.Caption, MB_ICONSTOP);
                Exit;
            end;
        end;
        *)
        Screen.Cursor := crDefault
	except
		on e: exception do begin
            Screen.Cursor := crDefault;
			MsgBoxErr(MSG_ERROR, e.Message, Self.Caption, MB_ICONSTOP);
		end;
	end;
end;

procedure TFrmTratamientoMorosos.ActivarBotones();
begin
    If (dsComprobantesMorosos.DataSet.RecordCount > 0) then
    begin
        btnEjecutar.Enabled := True;
        btnPostergar.Enabled := True;
        If (PopUpMenu1.Items.Count > 0) then btnAccionAdicional.Enabled := True else btnAccionAdicional.Enabled := False;
    end
    else begin
        btnEjecutar.Enabled := False;
        btnPostergar.Enabled := False;
        btnAccionAdicional.Enabled := False;
    end;
end;

procedure TFrmTratamientoMorosos.PopUpMenu1ItemClick(Sender: TObject);
var
    i: integer;
begin
    ObtenerMorosos.DisableControls;
    for i := 0 to dbgComprobantesMorosos.SelectedRows.Count - 1 do begin
        ObtenerMorosos.GotoBookmark(Pointer(dbgComprobantesMorosos.SelectedRows[i]));
        with ObtenerMorosos.Fields do
        begin
            CrearRegistroHistorialMoroso(FieldByName('TipoComprobante').AsString,
              FieldByName('NumeroComprobante').AsInteger, FieldByName('IDAccion').AsInteger, True, False,
              FieldByName('CodigoMedioContacto').AsInteger, FieldByName('CodigoPersona').AsInteger);
        end;
    end;
    ObtenerMorosos.EnableControls;
    cbTiposAccionesClick(Sender);
end;

end.
