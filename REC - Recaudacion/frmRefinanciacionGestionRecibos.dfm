object RefinanciacionGestionRecibosForm: TRefinanciacionGestionRecibosForm
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = ' Gesti'#243'n Recibos Cliente'
  ClientHeight = 342
  ClientWidth = 830
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnActivate = FormActivate
  DesignSize = (
    830
    342)
  PixelsPerInch = 96
  TextHeight = 13
  object pnlAcciones: TPanel
    Left = 0
    Top = 302
    Width = 830
    Height = 40
    Align = alBottom
    Anchors = []
    Color = 14408667
    ParentBackground = False
    TabOrder = 0
    ExplicitLeft = -10
    ExplicitTop = 162
    ExplicitWidth = 1012
    DesignSize = (
      830
      40)
    object btnCancelar: TButton
      Left = 749
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Salir'
      TabOrder = 0
      OnClick = btnCancelarClick
      ExplicitLeft = 931
    end
    object btnImprimir: TButton
      Left = 8
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Imprimir'
      TabOrder = 1
      OnClick = btnImprimirClick
    end
    object btnAnular: TButton
      Left = 89
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Anular'
      TabOrder = 2
      OnClick = btnAnularClick
    end
  end
  object GroupBox1: TGroupBox
    Left = 4
    Top = 8
    Width = 826
    Height = 290
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = '  Gesti'#243'n Recibos  '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    ExplicitWidth = 1008
    ExplicitHeight = 206
    DesignSize = (
      826
      290)
    object DBListEx1: TDBListEx
      Left = 6
      Top = 17
      Width = 813
      Height = 266
      Anchors = [akLeft, akTop, akRight, akBottom]
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 80
          Header.Caption = 'Nro. Recibo'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          Header.Alignment = taRightJustify
          IsLink = False
          FieldName = 'NumeroRecibo'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 100
          Header.Caption = 'Tipo'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          IsLink = False
          FieldName = 'TipoRecibo'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 90
          Header.Caption = 'Fecha Pago'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'FechaPago'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 100
          Header.Caption = 'Importe'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          Header.Alignment = taRightJustify
          IsLink = False
          FieldName = 'Importe'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 80
          Header.Caption = 'Nro. Turno'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          Header.Alignment = taRightJustify
          IsLink = False
          FieldName = 'Turno'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 110
          Header.Caption = 'Usuario Creaci'#243'n'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          IsLink = False
          FieldName = 'UsuarioCreacion'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 110
          Header.Caption = 'Fecha Creaci'#243'n'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'FechaCreacion'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 120
          Header.Caption = 'Nro. Rec. Anulaci'#243'n'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = [fsBold]
          Header.Alignment = taRightJustify
          IsLink = False
          FieldName = 'NumeroReciboAnulacion'
        end>
      DataSource = dsObtenerRefinanciacionRecibosCuotasPagos
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
      OnDrawBackground = DBListEx1DrawBackground
      OnDrawText = DBListEx1DrawText
      ExplicitWidth = 985
      ExplicitHeight = 182
    end
  end
  object spObtenerRefinanciacionRecibosCuotasPagos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    AfterScroll = spObtenerRefinanciacionRecibosCuotasPagosAfterScroll
    ProcedureName = 'ObtenerRefinanciacionRecibosCuotasPagos'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 484
    Top = 76
  end
  object spAnularRefinanciacionCuotaPago: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'AnularRefinanciacionCuotaPago'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroRecibo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@NumeroReciboAnulacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 656
    Top = 124
  end
  object dsObtenerRefinanciacionRecibosCuotasPagos: TDataSource
    DataSet = spObtenerRefinanciacionRecibosCuotasPagos
    Left = 432
    Top = 96
  end
end
