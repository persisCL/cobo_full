{-----------------------------------------------------------------------------
  Firma       :   SS_1120_MVI_20130820
  Descripcion :   Se crea el procedimiento OnDrawItem para que llame
                  al procedimiento ItemComboBoxColor para pintar los convenios de baja.
----------------------------------------------------------------------------}
unit frmActivarRefinanciacion;

interface

uses
  // Developer
  frmMuestraMensaje, DMConnection, PeaProcs, PeaTypes, Util, Convenios, UtilDB, frmRefinanciacionCuotasHistorial,
  PeaProcsCN, SysUtilsCN, frmRefinanciacionPactarMora, 
  frmRefinanciacionCuotasPagosEfectivo, frmRefinanciacionEntrarEditarCuotas,
  frmRefinanciacionReporteRecibo, CobranzasResources,
  // Auto
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ListBoxEx, DBListEx, StdCtrls, ExtCtrls, ComCtrls, Validate, DateEdit, DmiCtrls, VariantComboBox, DB, DBClient, Provider,
  ADODB, ImgList;

type
  TActivarRefinanciacionForm = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    lblNumRefinanciacion: TLabel;
    Label5: TLabel;
    lblEstado: TLabel;
    Label6: TLabel;
    lblTotalRefinanciado2: TLabel;
    vcbTipoRefinanciacion: TVariantComboBox;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    lblNombreCli: TLabel;
    Label7: TLabel;
    lblPersoneria: TLabel;
    lblConcesionaria: TLabel;
    peRut: TPickEdit;
    vcbConvenios: TVariantComboBox;
    dedtFechaRefi: TDateEdit;
    cbNotificacion: TCheckBox;
    GroupBox6: TGroupBox;
    PageControl1: TPageControl;
    tbsCuotas: TTabSheet;
    DBListEx3: TDBListEx;
    Panel7: TPanel;
    Label18: TLabel;
    lblCuotasCantidad: TLabel;
    Label20: TLabel;
    lblCuotasTotal: TLabel;
    pnlAcciones: TPanel;
    btnCancelar: TButton;
    spObtenerRefinanciacionCuotas: TADOStoredProc;
    dspObtenerRefinanciacionCuotas: TDataSetProvider;
    cdsCuotas: TClientDataSet;
    cdsCuotasCodigoEstadoCuota: TSmallintField;
    cdsCuotasDescripcionEstadoCuota: TStringField;
    cdsCuotasCodigoFormaPago: TSmallintField;
    cdsCuotasDescripcionFormaPago: TStringField;
    cdsCuotasTitularNombre: TStringField;
    cdsCuotasTitularTipoDocumento: TStringField;
    cdsCuotasTitularNumeroDocumento: TStringField;
    cdsCuotasCodigoBanco: TIntegerField;
    cdsCuotasDescripcionBanco: TStringField;
    cdsCuotasDocumentoNumero: TStringField;
    cdsCuotasFechaCuota: TDateTimeField;
    cdsCuotasCodigoTarjeta: TIntegerField;
    cdsCuotasDescripcionTarjeta: TStringField;
    cdsCuotasTarjetaCuotas: TSmallintField;
    cdsCuotasImporte: TLargeintField;
    dsCuotas: TDataSource;
    spObtenerRefinanciacionDatosCabecera: TADOStoredProc;
    cdsCuotasCodigoCuota: TSmallintField;
    btnActivarRefinanciacion: TButton;
    cdsCuotasValidada: TBooleanField;
    lnCheck: TImageList;
    cdsCuotasCodigoRefinanciacion: TIntegerField;		//SS_1051_PDO
    procedure btnCancelarClick(Sender: TObject);
    procedure DBListEx3LinkClick(Sender: TCustomDBListEx; Column: TDBListExColumn);
    procedure DBListEx3DrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
    procedure btnActivarRefinanciacionClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure vcbConveniosDrawItem(Control: TWinControl; Index: Integer;                //SS_1120_MVI_20130820
      Rect: TRect; State: TOwnerDrawState);                                             //SS_1120_MVI_20130820
  private
    { Private declarations }
    FPersona: TDatosPersonales;
    FCodigoRefinanciacion,
    FCodigoConvenio: Integer;
    FCuotasCantidad: Integer;
    FCuotasTotal,
    FComprobantesRefinanciadosTotal: Largeint;
    FAhora: TDate;
    FEdicionDatos: TEdicionDatos;
    FCodigoCanalPago: Integer;
    procedure MostrarDatosCliente;
    procedure MostrarCuotas;
    procedure ActualizarTotales;
    procedure MostrarDatosConvenio;
    procedure VerificaChecks;
  public
    { Public declarations }
    function Inicializar(Refinanciacion: Integer): Boolean;
  end;

var
  ActivarRefinanciacionForm: TActivarRefinanciacionForm;


implementation

{$R *.dfm}

procedure TActivarRefinanciacionForm.btnActivarRefinanciacionClick(Sender: TObject);
begin
    ModalResult := mrOk;
end;

procedure TActivarRefinanciacionForm.btnCancelarClick(Sender: TObject);
begin
    ModalResult := mrCancel;
end;

procedure TActivarRefinanciacionForm.DBListEx3DrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
var
    bmp: TBitMap;
begin
    with Sender do begin
        if Column.FieldName = 'Importe' then begin
            Text := FormatFloat(FORMATO_IMPORTE, Sender.DataSource.DataSet.FieldByName(Column.FieldName).AsInteger);
        end;

        if (Column.FieldName = 'Validada') then begin
            Text := '';
            Canvas.FillRect(Rect);
            bmp := TBitMap.Create;
			DefaultDraw := False;
            try
                if (cdsCuotasValidada.AsBoolean ) then begin
                    lnCheck.GetBitmap(0, Bmp);
                end
                else begin
                    lnCheck.GetBitmap(1, Bmp);
                end;

                Canvas.Draw((Rect.Left + Rect.Right - Bmp.Width) div 2, Rect.Top, bmp);
            finally
                bmp.Free;
            end; // finally
        end; // if
    end; // with
end;

procedure TActivarRefinanciacionForm.DBListEx3LinkClick(Sender: TCustomDBListEx; Column: TDBListExColumn);
begin
    cdsCuotas.Edit;
    cdsCuotasValidada.AsBoolean := not cdsCuotasValidada.AsBoolean;
    cdsCuotas.Post;

    VerificaChecks;
end;

procedure TActivarRefinanciacionForm.FormShow(Sender: TObject);
begin
    TPanelMensajesForm.OcultaPanel;
end;

function TActivarRefinanciacionForm.Inicializar(Refinanciacion: Integer): Boolean;
begin
    try
        try
            Result := False;
            CambiarEstadoCursor(CURSOR_RELOJ);

            CargarConstantesCobranzasRefinanciacion;
            CargarConstantesCobranzasCanalesFormasPago;

            TPanelMensajesForm.MuestraMensaje('inicializando Formulario Activar Refinanciación ...');

            FAhora := Trunc(NowBaseSmall(DMConnections.BaseCAC));

            CargarCanalesDePago(DMConnections.BaseCAC, vcbTipoRefinanciacion, 0, 1);

            cdsCuotas.Open;

            FCodigoRefinanciacion := Refinanciacion;

            TPanelMensajesForm.MuestraMensaje(Format('inicializando Refinanciación Nro. %d ...',[FCodigoRefinanciacion]));
            TPanelMensajesForm.MuestraMensaje('Cargando Datos Principales ...');

            with spObtenerRefinanciacionDatosCabecera do begin
                Parameters.Refresh;
                Parameters.ParamByName('@CodigoRefinanciacion').Value := FCodigoRefinanciacion;
                Open;

                FPersona          := ObtenerDatosPersonales(DMConnections.BaseCAC,'','', FieldByName('CodigoPersona').AsInteger);
                FCodigoConvenio   := FieldByName('CodigoConvenio').AsInteger;
                lblEstado.Caption := FieldByName('EstadoRefinanciacion').AsString;

                FComprobantesRefinanciadosTotal := FieldByName('TotalDeuda').AsInteger;

                lblNumRefinanciacion.Caption := IntToStr(Refinanciacion);
                dedtFechaRefi.Date           := FieldByName('Fecha').AsDateTime;
                FCodigoCanalPago             := FieldByName('CodigoTipoMedioPago').AsInteger;
                vcbTipoRefinanciacion.Value  := FCodigoCanalPago;
                cbNotificacion.Checked       := iif( FieldByName('Notificacion').AsBoolean, True, False);

                if FPersona.CodigoPersona <> -1 then begin
                    TPanelMensajesForm.MuestraMensaje('Cargando Datos Cliente ...');
                    MostrarDatosCliente;

                    TPanelMensajesForm.MuestraMensaje('Cargando Datos Convenios ...');
                    CargarConveniosRUT(DMConnections.BaseCAC, vcbConvenios, FPersona.TipoDocumento, FPersona.NumeroDocumento, False, FCodigoConvenio, False, True);       //SS_1120_MVI_20130820

                    if FCodigoConvenio <> 0 then MostrarDatosConvenio;
                end;

                TPanelMensajesForm.MuestraMensaje('Cargando Datos Cuotas ...');
                MostrarCuotas;
            end;

            Result := True;
        except
            on e:Exception do begin
                raise EErrorExceptionCN.Create('TActivarRefinancicionForm.Inicializar', e.Message);
            end;
        end;
    finally
        CambiarEstadoCursor(CURSOR_DEFECTO);
    end;
end;

procedure TActivarRefinanciacionForm.MostrarDatosCliente;
    const
        SQLObtenerNombrePersona   = 'SELECT dbo.ObtenerNombrePersona(%d)';
begin
    peRut.Text           := FPersona.NumeroDocumento;
    lblNombreCli.Caption := QueryGetValue(DMConnections.BaseCAC, Format(SQLObtenerNombrePersona, [FPersona.CodigoPersona]));

    if FPersona.Personeria = PERSONERIA_FISICA then begin
        lblPersoneria.Caption := ' - (Persona Natural)';
    end
    else begin
        if FPersona.Personeria = PERSONERIA_JURIDICA then lblPersoneria.Caption := ' - (Persona Jurídica)';
        if FPersona.Personeria = PERSONERIA_AMBAS then lblPersoneria.Caption := ' - (Persona Natural y Jurídica)';
    end;

    lblPersoneria.Left := lblNombreCli.Left + lblNombreCli.Width;
end;

procedure TActivarRefinanciacionForm.MostrarCuotas;
begin
    if cdsCuotas.Active then cdsCuotas.Close;

    with spObtenerRefinanciacionCuotas do begin
        Parameters.Refresh;
        Parameters.ParamByName('@CodigoRefinanciacion').Value := FCodigoRefinanciacion;
//        Parameters.ParamByName('@CodigoCuota').Value          := Null;		//SS_1051_PDO
//        Parameters.ParamByName('@SinCuotasAnuladas').Value    := 0;			//SS_1051_PDO
    end;

    with cdsCuotas do try
        dsCuotas.DataSet := Nil;
        Open;

        FCuotasTotal  := 0;

        while not Eof do begin
            FCuotasTotal := FCuotasTotal + cdsCuotasImporte.AsLargeInt;

            Next;
        end;
    finally
        dsCuotas.DataSet := cdsCuotas;
        First;
    end;

    FCuotasCantidad := cdsCuotas.RecordCount;

    ActualizarTotales;
end;

procedure TActivarRefinanciacionForm.ActualizarTotales;
    resourcestring
        rsValidacionImportesTitulo  = 'Validación Total Cuotas';
        rsValidacionImportesMensaje = 'El Importe Total de las Cuotas Entradas es Mayor que el Total de Comprobantes Refinanciados.';
begin
    // Totales Comprobantes Refinanciados
    lblTotalRefinanciado2.Caption := FormatFloat(FORMATO_IMPORTE, FComprobantesRefinanciadosTotal);
    // Totales Cuotas Refinanciación
    lblCuotasCantidad.Caption     := IntToStr(FCuotasCantidad);
    lblCuotasTotal.Caption        := FormatFloat(FORMATO_IMPORTE, FCuotasTotal);

    if FCuotasTotal <> FComprobantesRefinanciadosTotal then
        lblCuotasTotal.Font.Color := clRed
    else lblCuotasTotal.Font.Color := clGreen;
end;

procedure TActivarRefinanciacionForm.MostrarDatosConvenio;
begin
    lblConcesionaria.Caption := QueryGetValue(DMConnections.BaseCAC, Format('SELECT dbo.ObtenerConcesionariaConvenio(%d)', [FCodigoConvenio]));
end;

//BEGIN:SS_1120_MVI_20130820 -------------------------------------------------
{-----------------------------------------------------------------------------
  Procedure: TActivarRefinanciacionForm.vcbConveniosDrawItem
  Date:      20-August-2013
  Description: Verifica si existe algún convenio de baja para llamar a la función
                que colorea de rojo el item.
-----------------------------------------------------------------------------}
procedure TActivarRefinanciacionForm.vcbConveniosDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  if Pos( TXT_CONVENIO_DE_BAJA, vcbConvenios.Items[Index].Caption ) > 0 then  begin

     ItemComboBoxColor(Control, Index, Rect, State, clred, true);

  end
  else begin

     ItemComboBoxColor(Control, Index, Rect, State);

  end;                                                                                 
end;
//END:SS_1120_MVI_20130820 -------------------------------------------------

procedure TActivarRefinanciacionForm.VerificaChecks;
    var
        Puntero: TBookmark;
        AlgunaCuotaSinValidar: Boolean;
begin
    try
        CambiarEstadoCursor(CURSOR_RELOJ);

        with cdsCuotas do begin
            Puntero := GetBookmark;
            DisableControls;

            AlgunaCuotaSinValidar := False;
            First;

            while (not Eof) and (not AlgunaCuotaSinValidar) do begin
                AlgunaCuotaSinValidar := not cdsCuotasValidada.AsBoolean;

                Next;
            end;

            if Assigned(Puntero) then begin
                if BookmarkValid(Puntero) then GotoBookmark(Puntero);
                FreeBookmark(Puntero);
            end;

            EnableControls;
        end;
    finally
        CambiarEstadoCursor(CURSOR_DEFECTO);

        btnActivarRefinanciacion.Enabled := not AlgunaCuotaSinValidar;
    end;
end;

end.
