{-----------------------------------------------------------------------------
 File Name: fProtestoCheque.pas
 Author:    ndonadio
 Date Created: 21/06/2005
 Language: ES-AR
 Description: Tthis unit allows users to mark a check as a rejected one.

 Revision: 1
 Author: nefernandez
 Date: 09/03/2007
 Description: Se pasa el parametro CodigoUsuario al SP CrearNotaDebitoDK
              (para Auditoria)

Revision 2
Author: FSandi
Date: 03-10-2007
Description: SS 316 - Ahora listan solamente los motivos de anulaci�n correspondientes a esta pantalla

Revision 3:
    Date: 24/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura

Firma       : SS_330_NDR_20150818
Descripcion : Enviar la Fecha de Anulacion en la Creacion de la nota de debito al SP que requiere el par�metro pero no se envia
            : En los envios de fecha cambiar las funciones StrToDate y DateToStr por FormatDateTime

Firma       : SS_330_MGO_20151028
Descripcion : Se reemplaza uso de DateToStr por FormatDateTime
-----------------------------------------------------------------------------}
unit fProtestoCheque;

interface

uses
  // Generales
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DmiCtrls,
  //
  buscaClientes, VariantComboBox, DB, ADODB, ExtCtrls, ListBoxEx,
  DBListEx, DMConnection, Util, UtilProc, UtilDB , PeaTypes, PeaProcs,
  rStrings, Validate, DateEdit ;

type
  TfrmProtestoCheque = class(TForm)
    btnSalir: TButton;
    btnAnular: TButton;
    GroupBox1: TGroupBox;
    peNumeroDocumento: TPickEdit;
    cbbancos: TVariantComboBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    dblCheques: TDBListEx;
    Label4: TLabel;
    DBListEx2: TDBListEx;
    GroupBox2: TGroupBox;
    Label5: TLabel;
    Bevel1: TBevel;
    spObtenerListaCheques: TADOStoredProc;
    spObtenerListaComprobantes: TADOStoredProc;
    dsLIstaCheques: TDataSource;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    lblTitular: TLabel;
    lblTipoDocumento: TLabel;
    lblBanco: TLabel;
    lblNumeroCheque: TLabel;
    lblFecha: TLabel;
    lblMonto: TLabel;
    lblNumeroDocumento: TLabel;
    dsComprobantes: TDataSource;
    gbAnulacion: TGroupBox;
    Label12: TLabel;
    Label13: TLabel;
    deFechaAnulacion: TDateEdit;
    cbMotivosAnulacion: TVariantComboBox;
    spAnularCheque: TADOStoredProc;
    lblEstado2: TLabel;
    lblEstado: TLabel;
    Bevel2: TBevel;
    deFechaDesde: TDateEdit;
    deFEchaHasta: TDateEdit;
    Bevel3: TBevel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Bevel4: TBevel;
    spObtenerMotivosAnulacion: TADOStoredProc;
    lblMensaje: TLabel;
    neNumeroCheque: TEdit;
    spCrearNotaDebitoDK: TADOStoredProc;
    lblRecibo: TLabel;
    lblMontoRecibo: TLabel;
    lblRecibo2: TLabel;
    lblMontoRecibo2: TLabel;
    btnBuscar: TButton;
    procedure deFechaDesdeExit(Sender: TObject);
    procedure neNumeroChequeKeyPress(Sender: TObject; var Key: Char);
    procedure peNumeroDocumentoButtonClick(Sender: TObject);
    //procedure cbbancosChange(Sender: TObject);                                // TASK_145_MGO_20170302
    //procedure peNumeroDocumentoChange(Sender: TObject);                       // TASK_145_MGO_20170302
    //procedure neNumeroChequeChange(Sender: TObject);                          // TASK_145_MGO_20170302
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSalirClick(Sender: TObject);
    procedure dblChequesClick(Sender: TObject);
    procedure dblChequesDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: String; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure btnAnularClick(Sender: TObject);
    procedure spObtenerListaChequesAfterOpen(DataSet: TDataSet);
    procedure spObtenerListaChequesAfterClose(DataSet: TDataSet);
    //procedure deFechaDesdeChange(Sender: TObject);                            // TASK_145_MGO_20170302
    //procedure timerRUTTimer(Sender: TObject);                                 // TASK_145_MGO_20170302
    procedure btnBuscarClick(Sender: TObject);									// TASK_145_MGO_20170302
  private
    { Private declarations }
    FCodigoBanco: integer;
    FMontoRecibo: int64;
    FUltimaBusqueda: TBusquedaCliente;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    function  CargarBancos(Combo: TVariantComboBox; var Error: AnsiString): Boolean;
    function  ObtenerMotivosAnulacion(var Error: AnsiString) : boolean;
    procedure ReloadData;
    procedure CargarComprobantes;
    procedure CleanControls;
    procedure CleanSearch;
    procedure Buscar;
    function GenerarDebitoDK: boolean;

  public
    { Public declarations }
    function Inicializar: boolean;
  end;

  TTipoBusqueda = (tbRUT, tbCheque, tbFecha);

var
  frmProtestoCheque: TfrmProtestoCheque;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    ndonadio
  Date Created: 21/06/2005
  Description:
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
function TfrmProtestoCheque.Inicializar: boolean;
resourcestring
    MSG_ERROR_GENERIC   = 'No se puede inicializar el formulario.';
    CAP_ERROR           = 'Error de inicializaci�n.';
var
    Error: AnsiString;
begin
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

    CenterForm(self);
    Result := CargarBancos(cbBancos, Error) AND ObtenerMotivosAnulacion(Error);
    if NOT Result then begin
        MsgBoxErr(MSG_ERROR_GENERIC,Error,CAP_ERROR,MB_ICONERROR);
        Exit;
    end;
    CleanControls;
    cbBancos.ItemIndex := -1;
end;

{-----------------------------------------------------------------------------
  Function Name: peNumeroDocumentoButtonClick
  Author:    ndonadio
  Date Created: 21/06/2005
  Description:
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmProtestoCheque.peNumeroDocumentoButtonClick(Sender: TObject);
var
    f: TFormBuscaClientes;
begin
    Application.CreateForm(TFormBuscaClientes,F);
	if F.Inicializa(FUltimaBusqueda) then begin
        F.ShowModal;
        if F.ModalResult = idOk then begin
            FUltimaBusqueda := F.UltimaBusqueda;
            peNumeroDocumento.Text := f.Persona.NumeroDocumento;
            neNumeroCheque.Clear;
            deFechaDesde.Clear;
            deFechaHasta.Clear;
        end;
	end;
    F.free;
end;

{-----------------------------------------------------------------------------
  Procedure: TfrmProtestoCheque.CargarBancos
  Author:    gcasais
  Date:      19-Ene-2005
  Arguments: Combo: TVariantComboBox
  Result:    Boolean

 Revision 1:
  Author:    ggomez
  Date:      19-Abr-2005
  Description:
    - Modifiqu� la sentencia SQL utilizada para cargar los bancos para
    que s�lo cargue los bancos que tiene el campo HabilitadoPagoVentanilla en
    True.
    - Modifiqu� que el valor de retorno sea False s�lo si ocurre un error.
 Revision 2:
  Function Name: CargarBancos
  Author:    ndonadio
  Date Created: 22/06/2005
  Description:
        - Se agrego un parametro para almacenar el mensaje de error...
  Parameters: Combo: TVariantComboBox; var Error: AnsiString

Revision 3:
    Date: 24/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

 ----------------------------------------------------------------------------}
function TfrmProtestoCheque.CargarBancos(Combo: TVariantComboBox; var Error: AnsiString): Boolean;
var
    Qry:TADOQuery;
begin
    Result := True;

    //cargamos un banco en blanco por si necesita cancelar la busqueda por banco

    Combo.Items.Add('', 0 );
    Qry := TADOQuery.Create(nil);
    Qry.Connection := DMConnections.BaseCAC;
    (* Obtener los bancos que est�n habilitados para el Pago en Ventanilla. *)
    Qry.SQL.Add('SELECT CodigoBanco As Codigo, Descripcion As Descripcion'
        + ' From Bancos  WITH (NOLOCK) '
		+ ' Where (Activo = 1) And (HabilitadoPagoVentanilla = 1)'
		+ ' Order By Descripcion');
    try
        try
            Qry.Open;
            if Qry.IsEmpty then begin
                Qry.Close;
                Exit;
            end;
            while not Qry.Eof do begin
                Combo.Items.Add(Qry.FieldByName('Descripcion').AsString, Qry.FieldByName('Codigo').AsInteger);
                Qry.Next;
            end;
            Combo.ItemIndex := 0;
            //Combo.OnChange(Self);                                             // TASK_145_MGO_20170302
        except
            on e: Exception do begin
                Error := e.Message;
                Result := False;
            end;
        end;
    finally
        FreeAndNil(Qry);
    end;
end;
(* INICIO : TASK_145_MGO_20170302
{-----------------------------------------------------------------------------
  Procedure: TfrmProtestoCheque.cbbancosChange
  Author:    gcasais
  Date:      19-Ene-2005
  Arguments: Sender: TObject
  Result:    None
-----------------------------------------------------------------------------}
procedure TfrmProtestoCheque.cbbancosChange(Sender: TObject);
begin
    if ActiveControl <> Sender then Exit;
    FCodigoBanco := Integer(cbBancos.Value);
    CleanControls;
    peNumeroDocumento.Clear;
    deFechaDesde.Clear;
    deFechaHasta.Clear;
    neNumeroCheque.SelLength := 0;
    neNumeroCheque.SelStart := Length(neNumeroCheque.text);
    neNumeroCheque.SetFocus;
    timerRUT.Enabled := False;
    timerFecha.Enabled := False;
    timerCheque.Enabled := False;
    if TRIM(neNumeroCheque.text) <> '' then timerRUTTimer(self);
end;

{-----------------------------------------------------------------------------
  Function Name: peNumeroDocumentoChange
  Author:    ndonadio
  Date Created: 21/06/2005
  Description:
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmProtestoCheque.peNumeroDocumentoChange(Sender: TObject);
begin
    if ActiveControl <> Sender then Exit;
    cbBancos.ItemIndex := -1;
    neNumeroCheque.Clear;
    deFechaDesde.Clear;
    deFechaHasta.Clear;
    timerRUT.Enabled := False;
    timerFecha.Enabled := False; 
    timerCheque.Enabled := False;
    timerRUT.Enabled := True;    
    CleanControls;
end;

{-----------------------------------------------------------------------------
  Function Name: neNumeroChequeChange
  Author:    ndonadio
  Date Created: 21/06/2005
  Description:
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmProtestoCheque.neNumeroChequeChange(Sender: TObject);
begin
    if ActiveControl <> Sender then Exit;
    peNumeroDocumento.Clear;
    deFechaDesde.Clear;
    deFechaHasta.Clear;
    timerRUT.Enabled := False;
    timerFecha.Enabled := False;
    timerCheque.Enabled := False;
    timerCheque.Enabled := True;
    CleanControls;
end;
*) // FIN : TASK_145_MGO_20170302

{-----------------------------------------------------------------------------
  Function Name: ReloadData
  Author:    ndonadio
  Date Created: 21/06/2005
  Description:
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmProtestoCheque.ReloadData;
begin

    if spObtenerListaCheques.Active then spObtenerListaCheques.Requery([]) else Exit;

    if spObtenerListaCheques.IsEmpty then begin
        spObtenerListaCheques.Close;
        if spObtenerListaComprobantes.Active then  spObtenerListaComprobantes.Close;
        Exit;
    end;
    dblCheques.SelectedIndex := 0;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    ndonadio
  Date Created: 21/06/2005
  Description:
  Parameters: Sender: TObject; var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmProtestoCheque.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

// INICIO : TASK_145_MGO_20170302
procedure TfrmProtestoCheque.btnBuscarClick(Sender: TObject);
begin
    Buscar;
end;
// FIN : TASK_145_MGO_20170302

{-----------------------------------------------------------------------------
  Function Name: btnSalirClick
  Author:    ndonadio
  Date Created: 21/06/2005
  Description:
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmProtestoCheque.btnSalirClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name: dblChequesClick
  Author:    ndonadio
  Date Created: 21/06/2005
  Description:
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmProtestoCheque.dblChequesClick(Sender: TObject);
resourcestring
    MSG_HAS_NEWER_INVOICES		= 'Existen notas de cobro emitidas'#10#13'posteriores al recibo.';
var
  TipoComprobanteRecibo: string;
begin
    if (spObtenerListaCheques.IsEmpty) then Exit;

    TipoComprobanteRecibo := (QueryGetValue(DMConnections.BaseCAC, 'SELECT DBO.CONST_TIPO_COMPROBANTE_RECIBO ()'));

    lblTitular.Caption := spObtenerListaCheques.FieldByName('ChequeTitular').AsString;
    lblTipoDocumento.Caption := spObtenerListaCheques.FieldByName('ChequeCodigoDocumento').AsString;
    lblNumeroDocumento.Caption := spObtenerListaCheques.FieldByName('ChequeNumeroDocumento').AsString;
    lblNumeroCheque.Caption := spObtenerListaCheques.FieldByName('ChequeNumero').AsString;
    lblFecha.Caption := spObtenerListaCheques.FieldByName('ChequeFecha').AsString;
    lblMonto.Caption := FormatFloat(FORMATO_IMPORTE,spObtenerListaCheques.FieldByName('ChequeMonto').AsFloat/100);

    lblBanco.Caption := QueryGetValue(DMConnections.BaseCAC, Format(
                        'SELECT Descripcion FROM Bancos WITH(NOLOCK) Where CodigoBanco = %d'
                        ,[spObtenerListaCheques.FieldByName('ChequeCodigoBanco').AsInteger]));
    lblRecibo2.Caption :=  spObtenerListaCheques.FieldByName('NumeroRecibo').AsString;


	FMontoRecibo := QueryGetValueInt(DMConnections.BaseCAC, Format(
                        'SELECT TotalAPagar FROM Comprobantes WITH(NOLOCK) Where TipoComprobante = ' + QuotedStr(TipoComprobanteRecibo) +   ' and NumeroComprobante = %d'
                        ,[ spObtenerListaCheques.FieldByName('NumeroRecibo').AsInteger]));
    lblMontoRecibo2.Caption := FormatFloat(FORMATO_IMPORTE , FMontoRecibo / 100 );


	if (spObtenerListaCheques.FieldByName('Estado').AsString = 'A') then begin
    	lblEstado2.Visible := True;
    	lblEstado.Visible := True;
    	lblEstado.Caption := 'Anulado';
    end else begin
    	lblEstado2.Visible := False;
    	lblEstado.Visible := False;
    end;

  // Muestra un MEnsaje si tiene Notas de Cobro posteriores
  if (spObtenerListaCheques.FieldByName('TieneNotasCobroPosteriores').AsBoolean) then begin
      lblMensaje.Caption := MSG_HAS_NEWER_INVOICES;
      lblMensaje.Visible := True;
  end else begin
      lblMensaje.Caption := '';
      lblMensaje.Visible := False;
  end;

    btnAnular.Enabled := (spObtenerListaCheques.FieldByName('Estado').AsString <> 'A');

    CargarComprobantes;
end;

{-----------------------------------------------------------------------------
  Function Name: dblChequesDrawText
  Author:    ndonadio
  Date Created: 21/06/2005
  Description:
  Parameters: Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState; var Text: String; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmProtestoCheque.dblChequesDrawText(Sender: TCustomDBListEx;
  Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
  var Text: String; var TxtRect: TRect; var ItemWidth: Integer;
  var DefaultDraw: Boolean);
begin
    // si es el importe, le doy formato...

    if Column.FieldName = 'ChequeMonto' then Text := FormatFloat(FORMATO_IMPORTE,spObtenerListaCheques.FieldByName('ChequeMonto').AsFloat/100);
end;

{-----------------------------------------------------------------------------
  Function Name: CargarComprobantes
  Author:    ndonadio
  Date Created: 21/06/2005
  Description:  Carga la lista de comprobantes...
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmProtestoCheque.CargarComprobantes;
begin

    if spObtenerListaComprobantes.Active then spObtenerListaComprobantes.Close;

    spObtenerListaComprobantes.Parameters.ParamByName('@ChequeCodigoBanco').Value   := spObtenerListaCheques.FieldByName('ChequeCodigoBanco').asInteger;
    spObtenerListaComprobantes.Parameters.ParamByName('@ChequeNumero').Value        := spObtenerListaCheques.FieldByName('ChequeNumero').asString;
    spObtenerListaComprobantes.Parameters.ParamByName('@ChequeTitular').Value       := spObtenerListaCheques.FieldByName('ChequeTitular').asString;
    spObtenerListaComprobantes.Parameters.ParamByName('@ChequeFecha').Value         := iif(not spObtenerListaCheques.FieldByName('ChequeFecha').IsNull, spObtenerListaCheques.FieldByName('ChequeFecha').asDateTime, NULL);
    spObtenerListaComprobantes.Parameters.ParamByName('@ChequeCodigoDocumento').Value := spObtenerListaCheques.FieldByName('ChequeCodigoDocumento').asString;
    spObtenerListaComprobantes.Parameters.ParamByName('@ChequeNumeroDocumento').Value := spObtenerListaCheques.FieldByName('ChequeNumeroDocumento').asString;
    spObtenerListaComprobantes.Parameters.ParamByName('@ChequeMonto').Value         := spObtenerListaCheques.FieldByName('ChequeMonto').asVariant;

    spObtenerListaComprobantes.Open;
end;

{-----------------------------------------------------------------------------
  Function Name: btnAnularClick
  Author:    ndonadio
  Date Created: 21/06/2005
  Description:
  Parameters: Sender: TObject
  Return Value: None

  Revision: 1
  Author: nefernandez
  Date: 23/08/2007
  Description: SS 548: Cambio del AsInteger a AsVariant para el campo "ChequeMonto"
-----------------------------------------------------------------------------}
procedure TfrmProtestoCheque.btnAnularClick(Sender: TObject);
resourcestring
    MSG_ERROR 								= 'Error';
    MSG_ERROR_ANULANDO_CHEQUE				= 'Error al anular el Cheque';
    MSG_CHEQUE_ANULADO_CORRECTAMENTE		= 'El cheque Nro. %s se anul� correctamente';
    MSG_CONFIRMAR_ANULACION 				= 'Desea anular el cheque ?';
    MSG_CAPTION 							= 'Anulaci�n de Cheque';
    MSG_FECHA_ANULACION_MAYOR_CHEQUE 		= 'La fecha de anulaci�n debe ser posterior a la del cheque.';
    MSG_DEBE_SELECCIONAR_MOTIVO_ANULACION 	= 'Debe seleccionar un motivo de anulaci�n.';
    MSG_ANULA_OTRO_CHEQUE 					= 'Desea anular otro cheque ?';
var
    FNumeroCheque:  AnsiString;

begin
	// Valida los datos para anular
    if not ValidateControls(
      [deFechaAnulacion,
       cbMotivosAnulacion],
      [deFechaAnulacion.Date >= spObtenerListaCheques.FieldByName('ChequeFecha').AsDateTime,
       cbMotivosAnulacion.ItemIndex <> -1],
      caption,
      [MSG_FECHA_ANULACION_MAYOR_CHEQUE,
       MSG_DEBE_SELECCIONAR_MOTIVO_ANULACION]) then begin
        exit;
    end;

    if MsgBox(MSG_CONFIRMAR_ANULACION, MSG_CAPTION, MB_ICONQUESTION or MB_DEFBUTTON2 or MB_YESNO) <> IDYES then Exit;

    Screen.Cursor := crHourGlass;
    FNumeroCheque := spObtenerListaCheques.FieldByName('ChequeNumero').AsString;
    DMConnections.BaseCAC.BeginTrans;
    try
        try
            if not GenerarDebitoDK then begin
                Screen.Cursor := crDefault;
                Exit;
            end;
            spAnularCheque.Parameters.ParamByName('@ChequeNumero').Value 			:= FNumeroCheque;
            spAnularCheque.Parameters.ParamByName('@ChequeCodigoBanco').Value       := spObtenerListaCheques.FieldByName('ChequeCodigoBanco').AsInteger;
            spAnularCheque.Parameters.ParamByName('@ChequeNumeroDocumento').Value   := spObtenerListaCheques.FieldByName('ChequeNumeroDocumento').AsString;
            spAnularCheque.Parameters.ParamByName('@ChequeFecha').Value             := spObtenerListaCheques.FieldByName('ChequeFecha').AsString;
            spAnularCheque.Parameters.ParamByName('@ChequeMonto').Value             := spObtenerListaCheques.FieldByName('ChequeMonto').AsVariant;
            spAnularCheque.Parameters.ParamByName('@ChequeCodigoDocumento').Value   := spObtenerListaCheques.FieldByName('ChequeCodigoDocumento').AsString;
            spAnularCheque.Parameters.ParamByName('@ChequeTitular').Value           := spObtenerListaCheques.FieldByName('ChequeTitular').AsString;
            //spAnularCheque.Parameters.ParamByName('@FechaAnulacion').Value 			:= StrToDate(DateToStr(deFechaAnulacion.Date));                                  //SS_330_MGO_20151028
            spAnularCheque.Parameters.ParamByName('@FechaAnulacion').Value 			:= StrToDate(FormatDateTime('dd-mm-yyyy', deFechaAnulacion.Date));                 //SS_330_MGO_20151028
            spAnularCheque.Parameters.ParamByName('@CodigoMotivoAnulacion').Value 	:= cbMotivosAnulacion.Value;
            spAnularCheque.Parameters.ParamByName('@CodigoUsuario').Value 			:= UsuarioSistema;
            spAnularCheque.Parameters.ParamByName('@NumeroMovimiento').Value 		:= spCrearNotaDebitoDK.Parameters.ParamByName('@NumeroMovimiento').Value;
            spAnularCheque.Parameters.ParamByName('@TipoComprobante').Value 		:= (QueryGetValue(DMConnections.BaseCAC, 'SELECT DBO.CONST_TIPO_COMPROBANTE_NOTA_DEBITO_DK ()'));
            spAnularCheque.Parameters.ParamByName('@NumeroComprobante').Value 		:= spCrearNotaDebitoDK.Parameters.ParamByName('@NumeroNotaDebitoDK').Value;

            spAnularCheque.ExecProc;
            DMConnections.BaseCAC.CommitTrans;
            MsgBox(  Format(MSG_CHEQUE_ANULADO_CORRECTAMENTE, [FNumeroCheque]),MSG_CAPTION, MB_ICONINFORMATION);

        except
            on e: Exception do begin
                DMConnections.BaseCAC.RollbackTrans;
                MsgBoxErr(MSG_ERROR_ANULANDO_CHEQUE, e.Message, MSG_ERROR, MB_ICONERROR);
            end;
        end;
    finally
        Screen.Cursor := crDefault;
        if MsgBox(MSG_ANULA_OTRO_CHEQUE, MSG_CAPTION, MB_ICONQUESTION or MB_DEFBUTTON2 or MB_YESNO) <> IDYES then Close
        else CleanSearch;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ObtenerMotivosAnulacion
  Author:    flamas
  Date Created: 17/05/2005
  Description:
  Parameters:
  Return Value: boolean
 Revision 1:
  Function Name: ObtenerMotivosAnulacion
  Author:    ndonadio
  Date Created: 22/06/2005
  Description:
        - Se agrego parametro para devolver el mensaje de error...
  Parameters: var Error: AnsiString
 Revision:
  Author:   jconcheyro
  Date Created: 26/10/2005
  Description: Se reapunta el spObtenerMotivosAnulacion hacia un nuevo Store que trae
  especificamente el solicitado.

-----------------------------------------------------------------------------}
function  TfrmProtestoCheque.ObtenerMotivosAnulacion(var Error: AnsiString) : boolean;
resourcestring
	MSG_ERROR_LOADING_VOID_CAUSES 	= 'Error obteniendo motivos de anulaci�n';

begin
	result := False;
	try
        //Revision 2
        spObtenerMotivosAnulacion.Close;
        spObtenerMotivosAnulacion.Parameters.ParamByName('@OrigenPedido').Value := 'Cheque';
    	spObtenerMotivosAnulacion.Open;
        spObtenerMotivosAnulacion.First;
        while not spObtenerMotivosAnulacion.Eof do begin
	        	cbMotivosAnulacion.Items.Add( spObtenerMotivosAnulacion.FieldByName('Descripcion').AsString, spObtenerMotivosAnulacion.FieldByName('CodigoMotivoAnulacion').AsInteger);
            spObtenerMotivosAnulacion.Next;
        end;
        //Fin de Revision 2
    	spObtenerMotivosAnulacion.Close;
        cbMotivosAnulacion.ItemIndex := 0;
        result := cbMotivosAnulacion.Items.Count > 0;
    except
        on e: Exception do
            Error := MSG_ERROR_LOADING_VOID_CAUSES + CRLF + e.Message;
    end;

end;

{-----------------------------------------------------------------------------
  Function Name: spObtenerListaChequesAfterOpen
  Author: ndonadio
  Date Created: 23/06/2005
  Description: Actualiza los Datos del Cheque
  Parameters: DataSet: TDataSet
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmProtestoCheque.spObtenerListaChequesAfterOpen(DataSet: TDataSet);
begin
      gbAnulacion.Enabled := NOT spObtenerListaCheques.isEmpty;
      btnAnular.Enabled := NOT spObtenerListaCheques.IsEmpty ;
      dblChequesClick(Self);
end;

{-----------------------------------------------------------------------------
  Function Name: spObtenerListaChequesAfterClose
  Author:    ndonadio
  Date Created: 23/06/2005
  Description:
  Parameters: DataSet: TDataSet
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmProtestoCheque.spObtenerListaChequesAfterClose(
  DataSet: TDataSet);
begin
    btnAnular.Enabled := False;
    gbAnulacion.Enabled := False;
end;


{-----------------------------------------------------------------------------
  Function Name: CleanControls
  Author:    ndonadio
  Date Created: 23/06/2005
  Description: Borra los labels
  Parameters: None
  Return Value: None

  Revision 1
  Author: jconcheyro
  Date Created: 26/10/2005
  Description: Saco un seteo del cbMotivosAnulacion.ItemIndex := 1, ya no hace falta
-----------------------------------------------------------------------------}
procedure TfrmProtestoCheque.CleanControls;
begin
    if spObtenerListaCheques.Active then spObtenerListaCheques.Close ;
    if spObtenerListaComprobantes.Active then spObtenerListaComprobantes.Close;
    lblTitular.caption := '';
    lblTipoDocumento.caption := '';
    lblNumeroDocumento.caption := '';
    lblBanco.caption := '';
    lblNumeroCheque.caption := '';
    lblFecha.caption := '';
    lblMonto.caption := '';
    lblEstado.caption := '';
    lblRecibo2.Caption := '';
    lblMontoRecibo2.Caption := '';
    lblMensaje.Caption := '';
    deFechaAnulacion.Date := NowBase(DMCOnnections.BaseCAC);
end;

{-----------------------------------------------------------------------------
  Function Name: btnBuscarClick
  Author:    ndonadio
  Date Created: 22/06/2005
  Description: Activa la busqueda de cheques...
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmProtestoCheque.Buscar;
resourcestring
    MSG_START_DATE_MUST_BE_LOWER_THAN_END_DATE = 'La fecha inicial debe ser menor que la final';
	MSG_START_DATE_MUST_BE_HIGHER_THAN_01012001 = 'La fecha inicial debe ser mayor al 01/01/2001';
    MSG_SIN_CRITERIOS = 'Debe ingresar un criterio de b�squeda';                // TASK_145_MGO_20170302
    MSG_ERROR_CAPTION = 'Ha ocurrido un error al obtener los cheques';          // TASK_145_MGO_20170302
//var                                                                           // TASK_145_MGO_20170302
//    tipoBusqueda: TTipoBusqueda;                                              // TASK_145_MGO_20170302
begin
    { INICIO : TASK_145_MGO_20170302
    if TRIM(peNumeroDocumento.Text) <> '' then begin
        // Busqueda por Documento
        tipoBusqueda := tbRUT;
    end
    else if TRIM(neNumeroCheque.Text) <> '' then begin
                // Busqueda por Nro de Cheque
                tipoBusqueda := tbCheque;
        end
        else if ((deFechaDesde.Date <> nulldate) AND (deFechaHasta.Date <> nulldate)) then begin
                    tipoBusqueda := tbFecha;
            end
            else Exit;


    spObtenerListaCheques.Parameters.ParamByName('@NumRUT').Value       := null;
    spObtenerListaCheques.Parameters.ParamByName('@CodBanco').Value     := null;
    spObtenerListaCheques.Parameters.ParamByName('@NumeroCheque').Value := null;
    spObtenerListaCheques.Parameters.ParamByName('@FechaDesde').Value   := null;
    spObtenerListaCheques.Parameters.ParamByName('@FechaHasta').Value   := null;

    case tipoBUsqueda of
        tbRUT: begin
            spObtenerListaCheques.Parameters.ParamByName('@NumRUT').Value  := iif(peNumeroDocumento.Text <> '', trim(PadL(peNumeroDocumento.Text, 9, '0')), NULL);
        end;
        tbCheque: begin
            if (cbBancos.ItemIndex = -1) OR (TRIM(neNumeroCheque.Text)= '') then Exit;
            spObtenerListaCheques.Parameters.ParamByName('@CodBanco').Value  := FCodigoBanco;
            spObtenerListaCheques.Parameters.ParamByName('@NumeroCheque').Value  := neNumeroCheque.Text;
        end;
        tbFecha: begin
            if (deFechaDesde.Date = nullDate) OR (deFechaHasta.Date = nullDate) then Exit;
            if NOT ValidateControls([deFechaDesde],[(deFechaDesde.Date <= deFechaHasta.Date )],
                    'Criterios de B�squeda incorrectos',[MSG_START_DATE_MUST_BE_LOWER_THAN_END_DATE]) then Exit;

            if NOT ValidateControls([deFechaDesde],[(deFechaDesde.Date >= 36892 )],
                    'Criterios de B�squeda incorrectos',[MSG_START_DATE_MUST_BE_HIGHER_THAN_01012001]) then Exit;


            spObtenerListaCheques.Parameters.ParamByName('@FechaDesde').Value  := deFechaDesde.Date;
            spObtenerListaCheques.Parameters.ParamByName('@FechaHasta').Value  := deFechaHasta.Date;
        end;
    end;
    }

    if (Trim(peNumeroDocumento.Text) = EmptyStr)
      and (Trim(neNumeroCheque.Text) = EmptyStr)
      and (deFechaDesde.Date = NullDate)
      and (deFechaHasta.Date = NullDate) then begin
        MsgBox(MSG_SIN_CRITERIOS, 'Error', MB_ICONSTOP);
        Exit;
    end;

    if deFechaDesde.Date > deFechaHasta.Date then begin
        MsgBoxBalloon(MSG_START_DATE_MUST_BE_LOWER_THAN_END_DATE, 'Error', MB_ICONSTOP, deFechaDesde);
        Exit;
    end;

    try
        if  spObtenerListaCheques.Active then  spObtenerListaCheques.Close;

        if Trim(peNumeroDocumento.Text) <> EmptyStr then
            spObtenerListaCheques.Parameters.ParamByName('@NumRUT').Value := peNumeroDocumento.Text;
        if Trim(neNumeroCheque.Text) <> EmptyStr then begin
            spObtenerListaCheques.Parameters.ParamByName('@CodBanco').Value := cbBancos.Value;
            spObtenerListaCheques.Parameters.ParamByName('@NumeroCheque').Value := neNumeroCheque.Text;
        end;
        if deFechaDesde.Date <> NullDate then
            spObtenerListaCheques.Parameters.ParamByName('@FechaDesde').Value := deFechaDesde.Date;
        if deFechaHasta.Date <> NullDate then
            spObtenerListaCheques.Parameters.ParamByName('@FechaHasta').Value := deFechaHasta.Date;

        spObtenerListaCheques.Open;
    except
        on e: Exception do begin
            MsgBoxErr(MSG_ERROR_CAPTION, e.Message, 'Error', MB_ICONERROR);
            Exit;
        end;
    end;
    // FIN : TASK_145_MGO_20170302
    ReloadData;
end;

(* INICIO : TASK_145_MGO_20170302
{-----------------------------------------------------------------------------
  Function Name: deFechaDesdeChange
  Author:    ndonadio
  Date Created: 23/06/2005
  Description: Comienza a Buscar
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmProtestoCheque.deFechaDesdeChange(Sender: TObject);
begin
    if ActiveControl <> Sender then Exit;
    peNumeroDocumento.Clear;
    neNUmeroCheque.Clear ;
    cbBancos.ItemIndex := -1;
    timerRUT.Enabled := False;
    timerFecha.Enabled := False;                                            
    timerCheque.Enabled := False;                                           
    timerFecha.Enabled := True;                                             
    CleanControls;
end;

{-----------------------------------------------------------------------------
  Function Name: timerRUTTimer
  Author:    ndonadio
  Date Created: 23/06/2005
  Description: Busca los Cheques
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmProtestoCheque.timerRUTTimer(Sender: TObject);
begin
    //Dehabilito los timers...
    timerCheque.Enabled := False;
    timerFecha.Enabled := False;
    timerRUT.Enabled := False;

    //Hago la b�squeda...
    Buscar;
end;
*) // FIN : TASK_145_MGO_20170302

{-----------------------------------------------------------------------------
  Function Name: CleanSearch
  Author:    ndonadio
  Date Created: 23/06/2005
  Description: Borra los Datos
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmProtestoCheque.CleanSearch;
begin
    peNumeroDocumento.Clear;
    neNUmeroCheque.Clear ;
    deFechaDesde.Clear;
    deFechaHasta.Clear;
    cbBancos.ItemIndex := -1;
    CleanControls;
end;


procedure TfrmProtestoCheque.neNumeroChequeKeyPress(Sender: TObject; var Key: Char);
begin
	if  not (Key in [#8, #9, #13, #27, '0'..'9']) then Key := #0;
end;

{-----------------------------------------------------------------------------
Revision: 1
Author: nefernandez
Date: 22/08/2007
Description: SS 548: Cambio de la variable NumeroDebitoDK de Integer a Int64.
-----------------------------------------------------------------------------}
function TfrmProtestoCheque.GenerarDebitoDK: boolean;
resourcestring
	MSG_ERROR_GENERANDO_DEBITO_DK  			= 'Error generando Debito';
	MSG_ERROR								= 'Error';
	MSG_DEBITODK_GENERADO_CORRECTAMENTE     = 'El Debito N� %d se gener� correctamente';
    MSG_CAPTION                             = 'Generar Debito';
var NumeroDebitoDK: int64;
begin
	try
    Result := True;
    spCrearNotaDebitoDK.Close;                                                                                                                      //SS_330_NDR_20150818
    spCrearNotaDebitoDK.Parameters.Refresh;                                                                                                         //SS_330_NDR_20150818
	spCrearNotaDebitoDK.Parameters.ParamByName('@CodigoConvenio').Value             := spObtenerListaCheques.FieldByName('CodigoConvenio').AsInteger;
    spCrearNotaDebitoDK.Parameters.ParamByName('@Importe').Value                    := FMontoRecibo / 100;    //el store lo hace *100
	spCrearNotaDebitoDK.Parameters.ParamByName('@CodigoTipoMedioPago').Value        := Null;
	spCrearNotaDebitoDK.Parameters.ParamByName('@NumeroComprobanteNIPagado').Value  := 0;
    //Revision 1
	spCrearNotaDebitoDK.Parameters.ParamByName('@CodigoUsuario').Value              := UsuarioSistema;
	spCrearNotaDebitoDK.Parameters.ParamByName('@FechaAnulacion').Value 			:= deFechaAnulacion.Date;  										//SS_330_NDR_20150818
    spCrearNotaDebitoDK.ExecProc;
    NumeroDebitoDK := spCrearNotaDebitoDK.Parameters.ParamByName('@NumeroNotaDebitoDK').Value ;
		MsgBox(Format(MSG_DEBITODK_GENERADO_CORRECTAMENTE, [NumeroDebitoDK ]),MSG_CAPTION, MB_ICONINFORMATION);
	except
		on e: Exception do begin
	    	Result := False;
			MsgBoxErr(MSG_ERROR_GENERANDO_DEBITO_DK, e.Message, MSG_ERROR, MB_ICONERROR);
        end;
  end;
end;



procedure TfrmProtestoCheque.deFechaDesdeExit(Sender: TObject);
resourcestring
	MSG_ERROR_FECHA_INVALIDA  			= 'Error fecha inv�lida';
	MSG_ERROR								= 'Error';
begin
//    if deFechaDesde.Date < now - 1000 then
//	MsgBoxErr(MSG_ERROR_FECHA_INVALIDA, MSG_ERROR_FECHA_INVALIDA, MSG_ERROR, MB_ICONERROR);
end;

end.
