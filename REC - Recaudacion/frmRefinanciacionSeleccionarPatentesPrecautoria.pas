unit frmRefinanciacionSeleccionarPatentesPrecautoria;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,

  DMConnection, DB, Provider, DBClient, ADODB, ListBoxEx, DBListEx, StdCtrls, ExtCtrls, ImgList;

type
  TRefinanciacionSeleccionarPatentesPrecautoriaForm = class(TForm)
    Panel1: TPanel;
    btnCancelar: TButton;
    btnAceptar: TButton;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    edtPatente: TEdit;
    Label1: TLabel;
    lblDescVehiculo: TLabel;
    DBListEx1: TDBListEx;
    btnAgregarPatente: TButton;
    spObtenerCuentasPersona: TADOStoredProc;
    cdsPatentes: TClientDataSet;
    dsPatentes: TDataSource;
    dspObtenerCuentasCartola: TDataSetProvider;
    cdsPatentesPatente: TStringField;
    cdsPatentesMarcaModeloColor: TStringField;
    cdsPatentesSeleccionar: TBooleanField;
    lnCheck: TImageList;
    spObtenerDatosVehiculo: TADOStoredProc;
    chkMarcarTodos: TCheckBox;
    cdsPatentesConvenio: TStringField;
    cdsPatentesCodigoEstadoCuenta: TSmallintField;
    procedure DBListEx1LinkClick(Sender: TCustomDBListEx; Column: TDBListExColumn);
    procedure DBListEx1DrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
    procedure edtPatenteKeyPress(Sender: TObject; var Key: Char);
    procedure edtPatenteChange(Sender: TObject);
    procedure btnAgregarPatenteClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
    procedure chkMarcarTodosClick(Sender: TObject);
    procedure DBListEx1DrawBackground(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var DefaultDraw: Boolean);
    procedure DBListEx1KeyPress(Sender: TObject; var Key: Char);
    procedure DBListEx1DblClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    FTotalPatentesSeleccionadas: Integer;
    function Inicializar(CodigoPersona: Integer; cdsPatentesPrecautoria: TClientDataSet): Boolean;

  end;

var
  RefinanciacionSeleccionarPatentesPrecautoriaForm: TRefinanciacionSeleccionarPatentesPrecautoriaForm;

implementation

{$R *.dfm}

Uses
    PeaProcs, PeaProcsCN, PeaTypes, Util;

procedure TRefinanciacionSeleccionarPatentesPrecautoriaForm.btnAceptarClick(Sender: TObject);
begin
    ModalResult := mrOk;
end;

procedure TRefinanciacionSeleccionarPatentesPrecautoriaForm.btnAgregarPatenteClick(Sender: TObject);
begin
    with cdsPatentes do begin
        if not Locate('Patente', Trim(edtPatente.Text), []) then begin
            AppendRecord(
                [Trim(edtPatente.Text),
                 lblDescVehiculo.Caption,
                 '',
                 True,
                 1]);
            Inc(FTotalPatentesSeleccionadas);
        end;
    end;

    edtPatente.Text := '';
    edtPatente.SetFocus;
end;

procedure TRefinanciacionSeleccionarPatentesPrecautoriaForm.btnCancelarClick(Sender: TObject);
begin
    ModalResult := mrCancel;
end;

procedure TRefinanciacionSeleccionarPatentesPrecautoriaForm.chkMarcarTodosClick(Sender: TObject);
    var
        Puntero: TBookmark;
begin
    Try
        dsPatentes.DataSet := Nil;
        Puntero := cdsPatentes.GetBookmark;
        cdsPatentes.First;

        while not cdsPatentes.Eof do begin
            cdsPatentes.Edit;
            cdsPatentesSeleccionar.AsBoolean := chkMarcarTodos.Checked;
            cdsPatentes.Post;

            if chkMarcarTodos.Checked then
                Inc(FTotalPatentesSeleccionadas)
            else Dec(FTotalPatentesSeleccionadas);

            cdsPatentes.Next;
        end;
    Finally
        if Assigned(Puntero) then begin
            if cdsPatentes.BookmarkValid(Puntero) then cdsPatentes.GotoBookmark(Puntero);
            cdsPatentes.FreeBookmark(Puntero);
        end;
        dsPatentes.DataSet := cdsPatentes;
    End;
end;

procedure TRefinanciacionSeleccionarPatentesPrecautoriaForm.DBListEx1DblClick(Sender: TObject);
begin
    if cdsPatentes.RecordCount > 0 then DBListEx1LinkClick(Nil, Nil);
end;

procedure TRefinanciacionSeleccionarPatentesPrecautoriaForm.DBListEx1DrawBackground(Sender: TCustomDBListEx;
  Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState; var DefaultDraw: Boolean);
begin
    with Sender do begin
        if cdsPatentesCodigoEstadoCuenta.AsInteger = ESTADO_CUENTA_VEHICULO_BAJA then begin
            if (odSelected in State) then Canvas.Brush.Color := clMaroon;
        end;
    end;
end;

procedure TRefinanciacionSeleccionarPatentesPrecautoriaForm.DBListEx1DrawText(Sender: TCustomDBListEx; Column: TDBListExColumn;
  Rect: TRect; State: TOwnerDrawState; var Text: string; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean);
var
    bmp: TBitMap;
begin
    with Sender do begin
        if cdsPatentesCodigoEstadoCuenta.AsInteger = ESTADO_CUENTA_VEHICULO_BAJA then begin
            if Not (odSelected in State) then Canvas.Font.Color := clRed;
        end;

        if (Column.FieldName = 'Seleccionar') then try
            Text := '';
            Canvas.FillRect(Rect);
            bmp := TBitMap.Create;
			DefaultDraw := False;

            if (cdsPatentes.FieldByName(Column.FieldName).AsBoolean ) then
                lnCheck.GetBitmap(0, Bmp)
            else
                lnCheck.GetBitmap(1, Bmp);

            Canvas.Draw((Rect.Left + Rect.Right - Bmp.Width) div 2, Rect.Top, bmp);
        finally
            bmp.Free;
        end; // finally
    end; // with
end;

procedure TRefinanciacionSeleccionarPatentesPrecautoriaForm.DBListEx1KeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #32 then begin
        if cdsPatentes.RecordCount > 0 then DBListEx1LinkClick(Nil, Nil);
    end;
end;

procedure TRefinanciacionSeleccionarPatentesPrecautoriaForm.DBListEx1LinkClick(Sender: TCustomDBListEx; Column: TDBListExColumn);
begin
    try
        cdsPatentes.Edit;
        cdsPatentesSeleccionar.AsBoolean := not cdsPatentesSeleccionar.AsBoolean;
        if cdsPatentesSeleccionar.AsBoolean then
            Inc(FTotalPatentesSeleccionadas)
        else Dec(FTotalPatentesSeleccionadas);
    finally
        if cdsPatentes.State in dsEditModes then cdsPatentes.Post;
    end;
end;

procedure TRefinanciacionSeleccionarPatentesPrecautoriaForm.edtPatenteChange(Sender: TObject);
begin
    lblDescVehiculo.Caption   := '';
    btnAgregarPatente.Enabled := False;
end;

procedure TRefinanciacionSeleccionarPatentesPrecautoriaForm.edtPatenteKeyPress(Sender: TObject; var Key: Char);
    resourcestring
        rsTituloValidaciones   = 'Validaci�n Patente';
        rsErrorControl_Patente = 'La Patente Introducida NO es V�lida.';

        rsErrorObtenerDatosTitulo = 'Error datos del Veh�culo';
        rsErrorObtenerDatos       = 'Se produjo un Error al intentar obtener los Datos del Veh�culo.' + CRLF + CRLF + 'Error: %s.';

        rsPatenteNoExiste = '< La Patente NO existe. >';

    function ValidarCondiciones: boolean;
        var
            vControles  : Array [1..1] of TControl;
            vCondiciones: Array [1..1] of Boolean;
            vMensajes   : Array [1..1] of String;
    begin

        vControles[1]   := edtPatente;
        vCondiciones[1] := (Length(Trim(edtPatente.Text)) > 6) or VerificarFormatoPatenteChilena(DMConnections.BaseCAC, Trim(edtPatente.Text));
        vMensajes[1]    := rsErrorControl_Patente;

        Result := ValidateControlsCN(vControles, vCondiciones, rsTituloValidaciones, vMensajes, Self);
    end;
begin
    if Key = #13 then try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        if ValidarCondiciones then try
            with spObtenerDatosVehiculo do begin
                if Active then Close;
                Parameters.ParamByName('@Patente').Value := Trim(edtPatente.Text);
                Parameters.ParamByName('@CodigoTipoPatente').Value := PATENTE_CHILE;
                Open;

                if RecordCount = 1 then begin
                    lblDescVehiculo.Caption :=
                        Trim(
                            Trim(FieldByName('DescripcionMarca').AsString) +
                            ' ' +
                            iif(Trim(FieldByName('Modelo').AsString) <> '', Trim(FieldByName('Modelo').AsString),'')
                            );
                end
                else lblDescVehiculo.Caption := rsPatenteNoExiste;

                btnAgregarPatente.Enabled := (spObtenerDatosVehiculo.RecordCount = 1);
                if btnAgregarPatente.Enabled then btnAgregarPatente.SetFocus;
            end;
        except
            on e:Exception do begin
                ShowMsgBoxCN(rsErrorObtenerDatosTitulo, Format(rsErrorObtenerDatos, [e.Message]), MB_ICONERROR, Self);
            end;
        end;
    finally
        Screen.Cursor := crDefault;
        Application.ProcessMessages;
    end;
end;

function TRefinanciacionSeleccionarPatentesPrecautoriaForm.Inicializar(CodigoPersona: Integer; cdsPatentesPrecautoria: TClientDataSet): Boolean;
    var
        cdsPatentesPrecautoriaActual: TClientDataSet;
begin
    try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        Result := False;
        FTotalPatentesSeleccionadas := 0;
        lblDescVehiculo.Caption     := '';
        btnAgregarPatente.Enabled   := False;

        with spObtenerCuentasPersona do begin
            Parameters.ParamByName('@CodigoPersona').Value := CodigoPersona;
            Parameters.ParamByName('@TraerBajas').Value    := 1;
        end;

        cdsPatentes.Open;

        if cdsPatentesPrecautoria.RecordCount > 0 then try
            dsPatentes.DataSet := Nil;

            cdsPatentesPrecautoriaActual      := TClientDataSet.Create(Nil);
            cdsPatentesPrecautoriaActual.Data := cdsPatentesPrecautoria.Data;

            with cdsPatentesPrecautoriaActual do begin
                First;
                while not Eof do begin
                    if cdsPatentes.Locate('Patente', cdsPatentesPrecautoriaActual.FieldByName('Patente').AsString,[]) then begin
                        cdsPatentes.Edit;
                        cdsPatentesSeleccionar.AsBoolean := True;
                        cdsPatentes.Post;
                    end
                    else begin
                        cdsPatentes.AppendRecord(
                            [cdsPatentesPrecautoriaActual.FieldByName('Patente').AsString,
                             cdsPatentesPrecautoriaActual.FieldByName('MarcaModeloColor').AsString,
                             cdsPatentesPrecautoriaActual.FieldByName('Convenio').AsString,
                             True,
                             cdsPatentesPrecautoriaActual.FieldByName('CodigoEstadoCuenta').AsInteger]);
                    end;

                    Next;
                end;
            end;

            FTotalPatentesSeleccionadas := cdsPatentesPrecautoriaActual.RecordCount;
        finally
            dsPatentes.DataSet := cdsPatentes;
            cdsPatentes.First;
            if Assigned(cdsPatentesPrecautoriaActual) then FreeAndNil(cdsPatentesPrecautoriaActual);
        end;

        Result := True;
    finally
        Screen.Cursor := crDefault;
        Application.ProcessMessages;
    end;
end;

procedure TRefinanciacionSeleccionarPatentesPrecautoriaForm.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
    case Key of
        VK_ESCAPE: ModalResult := mrCancel;
    end;
end;

end.
