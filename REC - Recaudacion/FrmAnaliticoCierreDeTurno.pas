{-----------------------------------------------------------------------------
 Unit Name: FrmAnaliticoCierreDeTurno
 Author:    ggomez
 Purpose:   Form para ingresar un n�mero de turno y obtener el informe del
 	Anal�tico de Cierre de Turno.
 History:

 Revision 1:
 Author: ggomez
 Date: 23/05/2005
 Description:
    - Agregu� un par�metro para saber si se permite ver los informes de
    terceros, es decir, de otros usuarios, diferentes al logueado.
    - Cambi� el edit para ingresar el n�mero por un BuscaTabla.

 Revision 2:
 Author: ggomez
 Date: 24/05/2005
 Description:
    - Agregu� un componente para poder seleccionar un usario y poder seleccionar
    entre los turnos del usuario seleccionado. Este componente s�lo se muestra
    si el usuario logueado tiene permiso para ver informes de terceros.

 Revision 3:
 Author: ggomez
 Date: 01/06/2005
 Description:
    - Modifiqu� para que al listar los turnos filtre por el usuario logueado, si
    es que �ste no tiene acceo para ver los turnos de terceros.

-----------------------------------------------------------------------------}
unit FrmAnaliticoCierreDeTurno;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, DPSControls, DmiCtrls, DB, ADODB,
  DMConnection, PeaProcs, UtilProc, Util, FrmReporteAnaliticoCierreDeTurno,
  BuscaTab, FrmReporteChequesCierreDeTurno;

type
  TTipoReporte = (trAnaliticoCierreTurno, trChequesCierreTurno);

  TFormAnaliticoCierreDeTurno = class(TForm)
    pnl_Botones: TPanel;
    lbl_NumeroTurno: TLabel;
    bv_Datos: TBevel;
    sp_EsTurnoCerrado: TADOStoredProc;
    txt_NumeroTurno: TBuscaTabEdit;
    bt_Turnos: TBuscaTabla;
    sp_ObtenerListaTurnos: TADOStoredProc;
    lbl_Usuario: TLabel;
    cb_Usuario: TComboBox;
    btn_Aceptar: TButton;
    btn_Cancelar: TButton;
    procedure btn_CancelarClick(Sender: TObject);
    procedure btn_AceptarClick(Sender: TObject);
    procedure txt_NumeroTurnoButtonClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    function bt_TurnosProcess(Tabla: TDataSet; var Texto: String): Boolean;
    procedure bt_TurnosSelect(Sender: TObject; Tabla: TDataSet);
  private
    { Private declarations }
    FNumeroTurno: Integer;
    FPermitirVerInformesDeTerceros: Boolean;
    FTipoReporte: TTipoReporte;
  public
    { Public declarations }
    function Inicializar(txt_Caption: AnsiString; PermitirVerInformesDeTerceros: Boolean;
        TipoReporte: TTipoReporte): Boolean;
  end;

var
  FormAnaliticoCierreDeTurno: TFormAnaliticoCierreDeTurno;

implementation

{$R *.dfm}

procedure TFormAnaliticoCierreDeTurno.btn_CancelarClick(Sender: TObject);
begin
	Close;
end;

function TFormAnaliticoCierreDeTurno.Inicializar(txt_Caption: AnsiString;
    PermitirVerInformesDeTerceros: Boolean; TipoReporte: TTipoReporte): Boolean;
begin
    try
        Self.Caption := Caption;
        FPermitirVerInformesDeTerceros := PermitirVerInformesDeTerceros;
        FTipoReporte := TipoReporte;
        if FPermitirVerInformesDeTerceros then begin
            (* Cargar y mostrar el combo de usuarios. *)
            CargarCodigosUsuariosSistemas(DMConnections.BaseCAC, cb_Usuario);
            lbl_Usuario.Show;
            cb_Usuario.Show;
        end else begin
            lbl_Usuario.Hide;
            cb_Usuario.Hide;
        end; // else if
        Result := True;
    except
        Result := False;
    end;
end;

procedure TFormAnaliticoCierreDeTurno.btn_AceptarClick(Sender: TObject);
resourcestring
	MSG_ERROR_NUMERO_VACIO = 'Debe ingresar un N�mero de Turno';
	MSG_ERROR_NUMERO_NEGATIVO = 'El N�mero de Turno debe ser un n�mero positivo';
    MSG_TURNO_CERRADO = 'El Turno ingresado no est� cerrado.' + CRLF + 'No se permite emitir el informe.';
    MSG_TURNO_DE_OTRO_USUARIO = 'El Turno ingresado le pertenece a otro usuario.' + CRLF + 'No se permite emitir el informe.';
    MSG_NO_EXISTE_TURNO = 'El Turno ingresado no existe.';
var
    ExisteTurno,
	EsTurnoCerrado,
    MostrarInforme: Boolean;
    UsuarioTurno,
    PuntoVenta: AnsiString;
    r1: TFormReporteAnaliticoCierreDeTurno;
    r2: TFormReporteChequesCierreDeTurno;
begin
    UsuarioTurno := EmptyStr;
    PuntoVenta := EmptyStr;

    if (txt_NumeroTurno.Text <> '-') and (txt_NumeroTurno.Text <> EmptyStr) then begin
        FNumeroTurno := StrToInt(txt_NumeroTurno.Text);
    end;

	(* Controlar que el turno ingresado no sea vac�o, ni negativo ni cero. *)
    if not ValidateControls([txt_NumeroTurno,
    	txt_NumeroTurno],
    	[txt_NumeroTurno.Text <> EmptyStr,
        FNumeroTurno > 0],
        Self.Caption,
        [MSG_ERROR_NUMERO_VACIO,
        MSG_ERROR_NUMERO_NEGATIVO]) then Exit;

	(* Controlar que el turno ingresado est� cerrado. *)
    with sp_EsTurnoCerrado, Parameters do begin
    	ParamByName('@NumeroTurno').Value := FNumeroTurno;
        ExecProc;
        ExisteTurno := ParamByName('@ExisteTurno').Value;
        EsTurnoCerrado := ParamByName('@EsCerrado').Value;
        if ParamByName('@UsuarioTurno').Value = Null then begin
            UsuarioTurno := EmptyStr;
        end else begin
            UsuarioTurno := Trim(ParamByName('@UsuarioTurno').Value);
        end;

        if ParamByName('@PuntoVenta').Value = Null then begin
            PuntoVenta := EmptyStr;
        end else begin
            PuntoVenta := Trim(ParamByName('@PuntoVenta').Value);
        end;
    end; // with

    if not ExisteTurno then begin
    	MsgBox(MSG_NO_EXISTE_TURNO, Self.Caption, MB_ICONSTOP);
        Exit;
    end; // if

    if not EsTurnoCerrado then begin
    	(* Turno NO Cerrado, NO emitir el informe. *)

    	MsgBox(MSG_TURNO_CERRADO, Self.Caption, MB_ICONSTOP);
    end else begin
    	(* Turno Cerrado. *)

        (* Si el usuario logueado NO tiene permisos para ver informes de
        terceros, Controlar que el usuario del turno sea el usuario logueado. *)
        if FPermitirVerInformesDeTerceros then begin
            (* Turno Cerrado. Mostrar el informe. *)
            MostrarInforme := True;
        end else begin
            if UpperCase(Trim(UsuarioTurno)) <> UpperCase(Trim(UsuarioSistema)) then begin
                MostrarInforme := False;
               	MsgBox(MSG_TURNO_DE_OTRO_USUARIO, Self.Caption, MB_ICONSTOP);
            end else begin
                MostrarInforme := True;
            end;

        end; // else if FPermitirVerInformesDeTerceros

        case FTipoReporte of
            trAnaliticoCierreTurno: begin
                if MostrarInforme then begin
                    (* Cierro el form porque si se hace un preview, queda este form sobre el
                    preview. *)
                    Close;
                    Application.CreateForm(TFormReporteAnaliticoCierreDeTurno, r1);
                    try
                        r1.Inicializar(txt_NumeroTurno.ValueInt, UsuarioTurno, PuntoVenta);
                    finally
                        r1.Release;
                    end;
                end; // if MostrarInforme
            end;
            trChequesCierreTurno: begin
                Application.CreateForm(TFormReporteChequesCierreDeTurno, r2);
                try
                    if r2.Inicializar(txt_NumeroTurno.ValueInt, UsuarioTurno, PuntoVenta) then
                        Close;
                finally;
                    r2.Release;
                end;
            end;
        end;
    end; // else if
end;

procedure TFormAnaliticoCierreDeTurno.txt_NumeroTurnoButtonClick(
  Sender: TObject);
begin
    (* Obtener la lista con los turnos. *)
    sp_ObtenerListaTurnos.Close;
    (* Si el combo de usuario est� habilitado y hay un item v�lido seleccionado,
    pasar el usuario como par�metro al sp.*)
    if (cb_Usuario.Visible) then begin
        if (cb_Usuario.ItemIndex > 0) then begin
            sp_ObtenerListaTurnos.Parameters.ParamByName('@CodigoUsuario').Value := Trim(cb_Usuario.Text);
        end else begin
            sp_ObtenerListaTurnos.Parameters.ParamByName('@CodigoUsuario').Value := Null;
        end;
    end else begin
        (* Filtrar por el usuario logueado. *)
        sp_ObtenerListaTurnos.Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;
    end; // else if

    sp_ObtenerListaTurnos.Open;
end;

procedure TFormAnaliticoCierreDeTurno.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    sp_ObtenerListaTurnos.Close;
end;

function TFormAnaliticoCierreDeTurno.bt_TurnosProcess(Tabla: TDataSet;
  var Texto: String): Boolean;
begin
    Texto := PadR(Tabla.FieldByName('NumeroTurno').AsString, 10, ' ')
                + ' - ' + PadR(Tabla.FieldByName('CodigoUsuario').AsString, 20, ' ')
                + ' - ' + PadR(Tabla.FieldByName('FechaHoraCierre').AsString, 10, ' ');
    Result := True;
end;

procedure TFormAnaliticoCierreDeTurno.bt_TurnosSelect(Sender: TObject;
  Tabla: TDataSet);
begin
    txt_NumeroTurno.Text := Tabla.FieldByName('NumeroTurno').AsString;
end;


end.
