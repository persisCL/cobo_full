{********************************** File Header ********************************
File Name   : frmGeneracionRespuestas.pas
Author      : rcastro
Date Created: 28/03/2005
Language    : ES-AR
Description : M�dulo de generaci�n de respuestas a datos solicitados por otras
				concesionarias

  Revision : 1
    Date: 13/03/2009
    Author: mpiazza
    Description:  Ref (ss-716) Se modificaron los commandtimeout de las
                  componenentes de base de datos en el dfm

  Revision : 2
    Date: 01/05/2009
    Author: mpiazza
    Description:  Ref (ss-716) Se modificaron los commandtimeout (again)

  Firma: SS_1147_MCA_20140408
    Descripcion: se reemplaza constante COD_CONCESIONARIA por Codigo Concesionaria Nativa
*******************************************************************************}
unit frmGeneracionRespuestas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UtilProc, StrUtils, StdCtrls, DPSControls, DB, ADODB, Validate,
  DateEdit, DMConnection, ExtCtrls, Buttons, PeaProcs, Util, DmiCtrls,
  BuscaTab, ConstParametrosGenerales;

type
  TformGeneracionRespuestas = class(TForm)
	btnSalir: TDPSButton;
	btnCancelar: TDPSButton;
	btnProcesar: TDPSButton;
	Label2: TLabel;
	txtUbicacion: TEdit;
	SpeedButton1: TSpeedButton;
	Bevel1: TBevel;
	Label3: TLabel;
	ArchivoInfracciones: TBuscaTabEdit;
	Label4: TLabel;
	ArchivoFraudes: TBuscaTabEdit;
	Open: TOpenDialog;
	spBorrarPatentesPosiblesInfractoresFraudes: TADOStoredProc;
	spProcesarPatentesPosiblesInfractoresFraudes: TADOStoredProc;
	spActualizarPatentesPosiblesInfractoresFraudes: TADOStoredProc;
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure btnCancelarClick(Sender: TObject);
	procedure btnProcesarClick(Sender: TObject);
	procedure btnSalirClick(Sender: TObject);
	procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
	procedure SpeedButton1Click(Sender: TObject);
	function ProcesarArchivoConsulta(ArchivoConsulta: String; TipoTransito: Char) : Boolean;
	function Generar(ArchivoConsulta: String; TipoTransito: String) : Boolean;
	procedure ArchivoInfraccionesButtonClick(Sender: TObject);
	procedure ArchivoFraudesButtonClick(Sender: TObject);
  private
	{ Private declarations }
	FCancel : Boolean;
	CodigoOperacion: Integer;
    FConcesionariaNativa: Integer;                                              //SS_1147_MCA_20140408
	procedure RegistrarOperacion;
	procedure RegistrarOperacionErronea;
  public
	{ Public declarations }
	function Inicializar(txtCaption: String; MDIChild : Boolean):Boolean;
  end;

var
  formGeneracionRespuestas: TformGeneracionRespuestas;

implementation

uses PeaTypes;

{$R *.dfm}

resourcestring
	MSG_USER_CANCELLED			= 'Proceso cancelado por el usuario';
	MSG_PROCESS_FINISHED_OK		= 'El proceso ha teminado correctamente';
	MSG_ERROR_DELETING_DATA  	= 'Error al borrar el contenido de la tabla de patentes de posibles infractores/fraudes';
	MSG_ERROR_FILE_DIRECTORY	= 'La ubicaci�n ingresada es incorrecta o no existe';
	MSG_ERROR_FILE_GENERATION	= 'Error al generar el archivo de respuesta por posibles %s';
	MSG_NO_DATA					= 'No hay datos a informar';
	MSG_ERROR_GETTING_DATA  	= 'Error al obtener datos de patentes';
	REQUEST_PREFIX 				= 'paraverif_';
	INFRACTOR_PREFIX			= 'infrac_';
	FRAUD_PREFIX				= 'fraud_';
	MSG_FILE_SELECTION			= 'Seleccionar Archivo a Procesar...';

function TformGeneracionRespuestas.Inicializar(txtCaption: String; MDIChild : Boolean):Boolean;
resourcestring
	MSG_ERROR_DIRECTORIO_NO_EXISTE	= 'El directorio %s especificado en par�metros generales ' +
	'para la %s de respuestas de solicitudes no existe.';
Var
	S: TSize;
	FDirSalida,
	FDirEntrada: String;
begin
	result := false;

	if MDIChild then begin
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;

	CenterForm(Self);
	Caption := AnsiReplaceStr(txtCaption, '&', '');
	btnCancelar.Enabled := False;

	ObtenerParametroGeneral(DMCOnnections.BaseCAC, 'Dir_Salida_RNUTP', FDirSalida);
	txtUbicacion.Text := GoodDir(FDirSalida);
	if not DirectoryExists(FDirSalida) then begin
		MsgBox(Format(MSG_ERROR_DIRECTORIO_NO_EXISTE, [FDirSalida, 'salida']), Caption, MB_OK + MB_ICONERROR);
		Exit;
	end;

	ObtenerParametroGeneral(DMCOnnections.BaseCAC, 'Dir_Entrada_RNUTP', FDirEntrada);
	if not DirectoryExists(FDirEntrada) then begin
		MsgBox(Format(MSG_ERROR_DIRECTORIO_NO_EXISTE, [FDirEntrada, 'entrada']), Caption, MB_OK + MB_ICONERROR);
		Exit;
	end;

    FConcesionariaNativa := ObtenerCodigoConcesionariaNativa;                   //SS_1147_MCA_20140408

	Open.InitialDir := GoodDir(FDirEntrada);

	result := true
end;

procedure TformGeneracionRespuestas.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree
end;

procedure TformGeneracionRespuestas.btnCancelarClick(Sender: TObject);
begin
	FCancel := True;
end;

{******************************** Function Header ******************************
Function Name: ProcesarArchivoConsulta
Author       : rcastro
Date Created : 30/03/2005
Description  : Procesa el archivo de consulta de otras concesionarias
Parameters   : ArchivoConsulta: String; TipoTransito: Char
Return Value : Boolean

Revision : 1
  Date: 01/05/2009
  Author: mpiazza
  Description:  Ref (ss-716) Se modificaron los commandtimeout (again)

*******************************************************************************}
function TformGeneracionRespuestas.ProcesarArchivoConsulta(ArchivoConsulta: String; TipoTransito: Char) : Boolean;
var
	F: TextFile;
	Linea,
	FPatente,
	FContractSerialNumber: String;
	HayErrores: Boolean;
resourcestring
	MSG_ERROR_READING_INPUT_DATA = 'Error al leer archivo de consulta %s';
	MSG_ERROR_WRITING_PLATE_DATA = 'Error al guardar la Patente consultada';
begin
	result := true;

	if not FileExists (ArchivoConsulta) then Exit;
	if FCancel then Exit;

	HayErrores := False;
	Screen.Cursor := crHourGlass;

	try
		try
			AssignFile (F, ArchivoConsulta);
			Reset(F);

			while (not EoF(F)) and (not FCancel) and (not HayErrores) do begin
				Application.ProcessMessages;
				ReadLn(F, Linea);

				FPatente 				:= Trim(ParseParamByNumber(Linea, 1, ';'));
				FContractSerialNumber	:= Trim(ParseParamByNumber(Linea, 2, ';'));

				try
					// Guardo la patente
					with spActualizarPatentesPosiblesInfractoresFraudes, Parameters do begin
            CommandTimeout := 1260;
						ParamByName ('@Patente').Value 				:= iif (FPatente = '', null, FPatente);
						ParamByName ('@ContractSerialNumber').Value := iif (FContractSerialNumber = '', null, FContractSerialNumber);
						ParamByName ('@TipoEstado').Value 			:= TipoTransito;

						ExecProc
					end
				except
					// Error al guardar Patente
					on e: Exception do begin
						Screen.Cursor := crDefault;
						HayErrores := True;
						MsgBoxErr(MSG_ERROR_WRITING_PLATE_DATA, e.message, self.caption, MB_ICONSTOP)
					end;
				end
			end
		except
			// Error al leer archivo de texto
			on e: Exception do begin
				Screen.Cursor := crDefault;
				HayErrores := True;
				MsgBoxErr(Format(MSG_ERROR_READING_INPUT_DATA, [ArchivoConsulta]), e.message, self.caption, MB_ICONSTOP)
			end;
		end
	finally
		Screen.Cursor := crDefault;
		CloseFile(F);

		result := not HayErrores
	end;
end;

{******************************** Function Header ******************************
Function Name: Generar
Author       : rcastro
Date Created : 30/03/2005
Description  : Genera el archivo de respuesta para las otras concesionarias
Parameters   : ArchivoConsulta: String; TipoTransito: String
Return Value : Boolean


Revision 1
    Autor: dAllegretti
    Fecha: 30/09/2008
    Descripcion: Se modifica la extensi�n del archivo, pasa de .Dat a .Txt.

*******************************************************************************}
function TformGeneracionRespuestas.Generar(ArchivoConsulta: String; TipoTransito: String) : Boolean;
resourcestring
	FILE_EXISTS 	= 'El archivo de respuesta %s ya fue generado previamente. Lo genera de nuevo?';
	ANSWER_PREFIX 	= 'respverif_';
var
	F: TextFile;
	NombreArchivo: String;

begin
	result := true;

	if not FileExists (ArchivoConsulta) then Exit;
	if FCancel then Exit;

	spProcesarPatentesPosiblesInfractoresFraudes.Filter := 'TipoEstado = ' + QuotedStr(TipoTransito[1]);
	if spProcesarPatentesPosiblesInfractoresFraudes.IsEmpty then Exit;

	Screen.Cursor := crHourGlass;
	try
		try
			if TipoTransito[1] = 'I' then begin
				NombreArchivo := ANSWER_PREFIX + INFRACTOR_PREFIX +
								 Copy (ArchivoInfracciones.Text, Pos (INFRACTOR_PREFIX, ArchivoInfracciones.Text)+ 7, 2) + // concesionaria que solicita datos
								 //IntToStr (COD_CONCESIONARIA) + '_' +         //SS_1147_MCA_20140408                     // concesionaria que responde
                                 IntToStr(FConcesionariaNativa) + '_' +			//SS_1147_MCA_20140408
								 Copy (ArchivoInfracciones.Text, Pos (INFRACTOR_PREFIX, ArchivoInfracciones.Text)+ 9, 5) + // secuencia del archivo de consulta
								 FormatDateTime('yymmdd', Date);
			end
			else begin
				NombreArchivo := ANSWER_PREFIX + FRAUD_PREFIX +
								 Copy (ArchivoFraudes.Text, Pos (FRAUD_PREFIX, ArchivoFraudes.Text)+ 6, 2) + // concesionaria que solicita datos
								 //IntToStr (COD_CONCESIONARIA) + '_' +         //SS_1147_MCA_20140408       // concesionaria que responde
                                 IntToStr(FConcesionariaNativa) + '_' +			//SS_1147_MCA_20140408
								 Copy (ArchivoFraudes.Text, Pos (FRAUD_PREFIX, ArchivoFraudes.Text)+ 8, 5) + // secuencia del archivo de consulta
								 FormatDateTime('yymmdd', Date);
			end;

			if FileExists (GoodDir (Trim(txtUbicacion.Text)) + NombreArchivo + '.txt') and
				(MsgBox(Format (FILE_EXISTS, [NombreArchivo]), self.Caption, MB_YESNO + MB_ICONQUESTION) <> mrYes) //Revision 1
			then Exit;

			AssignFile (F, GoodDir (Trim(txtUbicacion.Text)) + NombreArchivo + '.txt'); //Revision 1
			ReWrite(F);

			with spProcesarPatentesPosiblesInfractoresFraudes do begin
				First;

				while (not EoF) and (not FCancel) do begin
					Application.ProcessMessages;

					Write(F, Trim(FieldByName ('Patente').AsString), ';');
					Write(F, PadL(Trim(FieldByName('ContractSerialNumber').AsString), 11, '0'), ';');
					Write(F, PadL(Trim(FieldByName('CodigoCategoria').AsString), 2, '0'), ';');
					Write(F, Trim(FieldByName('Vigente').AsString), ';');
					Write(F, Trim(FieldByName('NumeroConvenio').AsString), ';');
					Write(F, FormatDateTime('ddmmyyyy', FieldByName('FechaAltaCuenta').AsDateTime), ';');
					if not FieldByName('FechaBajaCuenta').IsNull then begin
						Write(F, FormatDateTime('ddmmyyyy', FieldByName('FechaBajaCuenta').AsDateTime))
					end;
					WriteLn (F);

					Next
				end;
			end;

			CloseFile (F);
		except
			// Error al generar archivo de respuesta
			on e: Exception do begin
				CloseFile (F);
				DeleteFile (NombreArchivo);
				result := false;

				MsgBox(Format(MSG_ERROR_FILE_GENERATION, [TipoTransito]), self.Caption, MB_OK + MB_ICONSTOP);
			end
		end
	finally
		Screen.Cursor := crDefault;
	end
end;

{******************************** Function Header ******************************

  Revision : 2
    Date: 01/05/2009
    Author: mpiazza
    Description:  Ref (ss-716) Se modificaron los commandtimeout (again)

*******************************************************************************}
procedure TformGeneracionRespuestas.btnProcesarClick(Sender: TObject);
var
	TodoOk: boolean;
begin
	if not ValidateControls([txtUbicacion],
		[(txtUbicacion.text <> '') And DirectoryExists(txtUbicacion.text)],
		Self.Caption,
		[MSG_ERROR_FILE_DIRECTORY]) then Exit;

	ArchivoInfracciones.Enabled := false;
	ArchivoFraudes.Enabled := false;
	btnCancelar.Enabled := true;
	btnSalir.Enabled := False;
	btnProcesar.Enabled := false;
	FCancel := false;
	TodoOk := false;

	try
		try
			RegistrarOperacion;

			// Ejecuto el sp de borrado de patentes consultadas previamente
			Screen.Cursor := crHourGlass;
      spBorrarPatentesPosiblesInfractoresFraudes.CommandTimeout := 1260;
			spBorrarPatentesPosiblesInfractoresFraudes.ExecProc;
			Screen.Cursor := crDefault;

			// Proceso los archivos de consulta por Infracciones y Fraudes
			if not ProcesarArchivoConsulta (ArchivoInfracciones.Text, 'I') then Exit;
			if not ProcesarArchivoConsulta (ArchivoFraudes.Text, 'F') then Exit;

			try
				with spProcesarPatentesPosiblesInfractoresFraudes do begin
          CommandTimeout := 1260;
					Filtered := true;
					Close;
					Open
				end;

				if spProcesarPatentesPosiblesInfractoresFraudes.IsEmpty then
					MsgBox(MSG_NO_DATA, self.Caption, MB_OK + MB_ICONINFORMATION)
				else begin
					if not Generar (ArchivoInfracciones.Text, 'Infractores') then Exit;
					if not Generar (ArchivoFraudes.Text, 'Fraudes') then Exit;

					MsgBox(MSG_PROCESS_FINISHED_OK, self.Caption, MB_OK + MB_ICONINFORMATION)
				end;

				TodoOk := true
			except
				// Error al obtener datos de las cuentas para las patentes consultadas
				on e: Exception do begin
					MsgBoxErr(MSG_ERROR_GETTING_DATA, e.message, self.caption, MB_ICONSTOP)
				end;
			end
		except
			// Error al vaciar la tabla
			on e: Exception do begin
				Screen.Cursor := crDefault;
				MsgBoxErr(MSG_ERROR_DELETING_DATA, e.message, self.caption, MB_ICONSTOP)
			end;
		end
	finally
		Screen.Cursor := crDefault;

		if FCancel then
			MsgBox(MSG_USER_CANCELLED, self.Caption, MB_OK + MB_ICONINFORMATION)
		else begin
			if not TodoOk then RegistrarOperacionErronea
		end;

		ArchivoInfracciones.Enabled := true;
		ArchivoFraudes.Enabled := true;
		btnCancelar.Enabled := false;
		btnSalir.Enabled := true;
		btnProcesar.Enabled := true;
	end
end;

procedure TformGeneracionRespuestas.btnSalirClick(Sender: TObject);
begin
	Close
end;

procedure TformGeneracionRespuestas.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
	CanClose := not btnCancelar.Enabled
end;

procedure TformGeneracionRespuestas.SpeedButton1Click(Sender: TObject);
resourcestring
	MSG_BROWSE_TITLE = 'Seleccione la ubicaci�n d�nde se guardar� el archivo de respuesta a las solicitudes de datos ';
var
	FDirSalida: String;
begin
	txtUbicacion.Text := GoodDir(Trim(BrowseForFolder(MSG_BROWSE_TITLE)));
	if Trim(txtUbicacion.Text) = '' then begin
		ObtenerParametroGeneral(DMCOnnections.BaseCAC, 'Dir_Salida_RNUTP', FDirSalida);
		txtUbicacion.Text := GoodDir(FDirSalida);
	end
end;

{*******************************************************************************
Revision 1
    Autor: dAllegretti
    Fecha: 30/09/2008
    Descripcion: Se modifica la extensi�n del archivo, pasa de .Dat a .Txt.
*******************************************************************************}
procedure TformGeneracionRespuestas.ArchivoInfraccionesButtonClick(
  Sender: TObject);
resourcestring
	STR_INFRACTORS = 'Infractores';
begin
	Open.Filter := STR_INFRACTORS + '|' + REQUEST_PREFIX + INFRACTOR_PREFIX + '*.txt'; //Revision 1
	Open.FileName := '';
	if not Open.Execute then ArchivoInfracciones.Text := MSG_FILE_SELECTION;

	ArchivoInfracciones.Text := Open.FileName
end;

{*******************************************************************************
Revision 1
    Autor: dAllegretti
    Fecha: 30/09/2008
    Descripcion: Se modifica la extensi�n del archivo, pasa de .Dat a .Txt.
*******************************************************************************}
procedure TformGeneracionRespuestas.ArchivoFraudesButtonClick(
  Sender: TObject);
resourcestring
	STR_FRAUDS = 'Fraudes';
begin
	Open.Filter := STR_FRAUDS + '|' + REQUEST_PREFIX + FRAUD_PREFIX + '*.txt'; //Revision 1
	Open.FileName := '';
	if not Open.Execute then ArchivoFraudes.Text := MSG_FILE_SELECTION;

	ArchivoFraudes.Text := Open.FileName;
end;

procedure TformGeneracionRespuestas.RegistrarOperacion;
var
	DescriError: String;
resourcestring
	STR_PROCESS_OK = 'Procesamiento de generaci�n de respuestas exitoso.';
	STR_FILES = 'Archivos: ';
begin
	// Genero el registro de operaci�n
	DMConnections.BaseCAC.BeginTrans;

	CodigoOperacion := CrearRegistroOperaciones(
		DMConnections.BaseCAC,
		SYS_RNUT_PARALELO,
		RO_MOD_PROCESO_RNUT_PARALELO,
		RO_AC_PROCESAR_TRANSITOS_RNUT_PARALELO,
		SEV_INFO,
		GetMachineName,
		UsuarioSistema,
		STR_PROCESS_OK,
		0,
		0,
		0,
		STR_FILES + Trim(ExtractFileName(ArchivoInfracciones.Text) + ' ' + ExtractFileName(ArchivoFraudes.Text)),
		DescriError);

	DMConnections.BaseCAC.CommitTrans;
end;

procedure TformGeneracionRespuestas.RegistrarOperacionErronea;
resourcestring
	MSG_ERROR_FOUND = 'Errores encontrados';
	MSG_EVENT_ERROR	= 'El procesamiento de generaci�n de respuestas se ha cancelado por errores.';
	STR_FILES = 'Archivos: ';
var
	Error: ANSIString;
begin
	Error := MSG_ERROR_FOUND;

	ActualizarRegistroOperacion(
		DMCOnnections.BaseCAC,
		SYS_RNUT_PARALELO,
		CodigoOperacion,
		SEV_ALTA,
		False,
		MSG_EVENT_ERROR,
		0,
		0,
		STR_FILES + Trim(ExtractFileName(ArchivoInfracciones.Text) + ' ' + ExtractFileName(ArchivoFraudes.Text)),
		Error);
end;

end.
