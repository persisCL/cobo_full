object formGeneracionRespuestas: TformGeneracionRespuestas
  Left = 283
  Top = 290
  Anchors = []
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Generaci'#243'n de Respuestas'
  ClientHeight = 196
  ClientWidth = 592
  Color = clBtnFace
  Constraints.MaxHeight = 230
  Constraints.MaxWidth = 600
  Constraints.MinHeight = 228
  Constraints.MinWidth = 598
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 7
    Top = 6
    Width = 577
    Height = 145
  end
  object Label2: TLabel
    Left = 18
    Top = 104
    Width = 229
    Height = 13
    Caption = 'Ubicaci'#243'n de los archivos de respuesta:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object SpeedButton1: TSpeedButton
    Left = 468
    Top = 118
    Width = 23
    Height = 23
    Caption = '...'
    OnClick = SpeedButton1Click
  end
  object Label3: TLabel
    Left = 18
    Top = 12
    Width = 255
    Height = 13
    Caption = 'Archivo de consulta por posibles Infractores:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 18
    Top = 57
    Width = 239
    Height = 13
    Caption = 'Archivo de consulta por posibles Fraudes:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object btnSalir: TDPSButton
    Left = 507
    Top = 159
    Height = 26
    Caption = '&Salir'
    TabOrder = 0
    OnClick = btnSalirClick
  end
  object btnCancelar: TDPSButton
    Left = 426
    Top = 159
    Height = 26
    Caption = '&Cancelar'
    TabOrder = 1
    OnClick = btnCancelarClick
  end
  object btnProcesar: TDPSButton
    Left = 345
    Top = 159
    Height = 26
    Caption = '&Procesar'
    TabOrder = 2
    OnClick = btnProcesarClick
  end
  object txtUbicacion: TEdit
    Left = 18
    Top = 119
    Width = 442
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
  end
  object ArchivoInfracciones: TBuscaTabEdit
    Left = 18
    Top = 27
    Width = 556
    Height = 21
    Hint = 'El archivo debe ser de texto sin formato'
    Enabled = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    ReadOnly = True
    TabOrder = 4
    Decimals = 0
    EditorStyle = bteTextEdit
    OnButtonClick = ArchivoInfraccionesButtonClick
    Text = 'Seleccionar Archivo a Procesar...'
  end
  object ArchivoFraudes: TBuscaTabEdit
    Left = 18
    Top = 72
    Width = 556
    Height = 21
    Hint = 'El archivo debe ser de texto sin formato'
    Enabled = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    ReadOnly = True
    TabOrder = 5
    Decimals = 0
    EditorStyle = bteTextEdit
    OnButtonClick = ArchivoFraudesButtonClick
    Text = 'Seleccionar Archivo a Procesar...'
  end
  object Open: TOpenDialog
    Filter = 'infractores|paraverif_infrac_*.dat|fraudes|paraverif_fraud_*.dat'
    Options = [ofReadOnly, ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 204
    Top = 155
  end
  object spBorrarPatentesPosiblesInfractoresFraudes: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CommandTimeout = 300
    ProcedureName = 'BorrarPatentesPosiblesInfractoresFraudes'
    Parameters = <>
    Left = 54
    Top = 156
  end
  object spProcesarPatentesPosiblesInfractoresFraudes: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CommandTimeout = 300
    ProcedureName = 'ProcesarPatentesPosiblesInfractoresFraudes'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end>
    Left = 114
    Top = 156
  end
  object spActualizarPatentesPosiblesInfractoresFraudes: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CommandTimeout = 300
    ProcedureName = 'ActualizarPatentesPosiblesInfractoresFraudes'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@ContractSerialNumber'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@TipoEstado'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end>
    Left = 84
    Top = 156
  end
end
