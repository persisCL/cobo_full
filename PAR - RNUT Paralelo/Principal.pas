{********************************** File Header ********************************
File Name   : Main.pas
Author      : Castro, Ra�l
Date Created: 28/03/2005
Language    : ES-AR
Description : Principal RNUT paralelo

Firma       :   SS_925_NDR_20131122
Description :   Genera nuevos menu segun campo GenerarMenues de tabla SistemasVersiones (no del .ini)

Firma       :   SS_925_MBE_20131223
Description :   Se cambia la ventana de Acerca de.

*******************************************************************************}

unit Principal;

interface

uses
	Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
	Dialogs, Menus, ExtCtrls, IconBars, ImgList, Buttons, UtilDB,
	StdCtrls, UtilProc, ComCtrls, ToolWin, ActnList, AppEvnts, PeaTypes,
	DmConnection, util, peaprocs, login, DB, ADODB, jpeg, CambioPassword,
	DpsAbout, RStrings, ListBoxEx, DBListEx,
	frmSolicitudDatos, frmGeneracionRespuestas, frmProcesamientoRespuestas, XPMan;
	
type
  TMainForm = class(TForm)
    PageImagenes: TImageList;
	MainMenu: TMainMenu;
    mnu_sistema: TMenuItem;
    mnu_salir: TMenuItem;
    StatusBar: TStatusBar;
	HotLinkImagenes: TImageList;
    MenuImagenes: TImageList;
	ImagenFondo: TImage;
    Ayuda1: TMenuItem;
	Acercade1: TMenuItem;
    mnu_Ventana: TMenuItem;
    mnu_Cascada: TMenuItem;
    mnu_Mosaico: TMenuItem;
    N2: TMenuItem;
    mnu_Siguiente: TMenuItem;
    mnu_Anterior: TMenuItem;
    mnu_Mantenimiento: TMenuItem;
    mnu_CambiodeContrasena: TMenuItem;
    mnuRequerimientos: TMenuItem;
	mnuSolicitudDatos: TMenuItem;
    mnuProcesamientoRespuestas: TMenuItem;
    mnuGenerarRespuestas: TMenuItem;
    mnuPosiblesFraudesTAGCNsinConvenio: TMenuItem;
    XPManifest1: TXPManifest;
	procedure mnu_salirClick(Sender: TObject);
    procedure Cerrar1Click(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure Acercade1Click(Sender: TObject);
	procedure mnu_CascadaClick(Sender: TObject);
    procedure mnu_CambiodeContrasenaClick(Sender: TObject);
	procedure mnu_VentanaClick(Sender: TObject);
    procedure mnuSolicitudDatosClick(Sender: TObject);
    procedure mnuProcesamientoRespuestasClick(Sender: TObject);
    procedure mnuGenerarRespuestasClick(Sender: TObject);
    procedure mnuPosiblesFraudesTAGCNsinConvenioClick(
      Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
	procedure RefrescarBarraDeEstado(StatusBar: TStatusBar);
//	Navegador : TNavigator;
  public
	{ Public declarations }
	function Inicializar: Boolean;
  end;

var
  MainForm: TMainForm;


implementation

uses frmAcercaDe;

var
	Salir: Boolean;

{$R *.dfm}
{$I ..\comunes\Ambiente.inc}  

function TMainForm.Inicializar: Boolean;
var
	f: TLoginForm;
	ok: boolean;
begin
	// Navegador
    Result := False;
	Application.Title := SYS_RNUT_PARALELO_TITLE;
	SistemaActual := SYS_RNUT_PARALELO;

    try
		Application.CreateForm(TLoginForm, f);
		if f.Inicializar(DMConnections.BaseCAC, SYS_RNUT_PARALELO) and (f.ShowModal = mrOk) then begin
			UsuarioSistema := f.CodigoUsuario;

            //BEGIN : SS_925_NDR_20131122-----------------------------------------------------------------------
            //if not ValidarVersion(DMConnections.BaseCAC, SistemaActual) then begin
            //    MsgBoxErr(MSG_ERROR_VERSION, MSG_ERROR_VERSION_APLICACION, Self.Caption, MB_ICONSTOP);
            //    Exit;
            //end;

            //	Navegador := TNavigator.Create(self);
            //	Navegador.IconBarVisible := false;
            // Deberiamos obtener todas las paginas a agregar aca... pero bueno.. es lo que hay

            //Ok := true;
            // Men�es
            //if (InstallIni.ReadInteger('General', 'GenerarMenues', 1) = 1) then begin
            //    ok := GenerateMenuFile(MainMenu, SYS_RNUT_PARALELO, DMConnections.BaseCAC, [] );
            //end;


            if not ValidarVersion(DMConnections.BaseCAC, SistemaActual, MainMenu, [] ) then begin
                MsgBoxErr(MSG_ERROR_VERSION, MSG_ERROR_VERSION_APLICACION, self.Caption, MB_ICONSTOP);
                Exit;
            end;
            ok := True;
            //END : SS_925_NDR_20131122-----------------------------------------------------------------------

            Salir := False;
            if Ok then begin
                RefrescarBarraDeEstado(StatusBar);

                CargarAccesos(DMConnections.BaseCAC, UsuarioSistema, SYS_RNUT_PARALELO);
                HabilitarPermisosMenu(Menu);

                Result := True;
            end else begin
                Result := False;
            end;
        end
        else result := False;

    	if not Result then Exit;
        //	Navegador.ConnectionCOP := nil;
        //	Navegador.ConnectionCAC := DMConnections.BaseCAC;
    finally
        f.Release;
    end;
end;

procedure TMainForm.RefrescarBarraDeEstado(StatusBar: TStatusBar);
begin
	StatusBar.Panels.Items[0].Text  := Format(MSG_USER, [UsuarioSistema]);
	StatusBar.Panels[0].Width:= StatusBar.Font.Size * (Length(StatusBar.Panels[0].text));
	StatusBar.Panels[1].Text := Trim(Format(MSG_CONECTADO_A, [InstallIni.ReadString('Database CAC', 'Server', ''), InstallIni.ReadString('Database CAC', 'DatabaseName', '')]));
	StatusBar.Panels[1].Width:= StatusBar.Font.Size * (Length(StatusBar.Panels[1].text));
end;

procedure TMainForm.mnu_salirClick(Sender: TObject);
begin
	Close;
end;

procedure TMainForm.Cerrar1Click(Sender: TObject);
begin
	Close;
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
	{$IFNDEF PRODUCCION}
        Caption := Caption + Format(' - Versi�n %s: %s', [cAmbiente, GetFileVersionString(GetExeName, True)]);
    {$ELSE}
        Caption := Caption + Format(' - Versi�n: %s', [GetFileVersionString(GetExeName, True)]);
    {$ENDIF}
end;

procedure TMainForm.FormPaint(Sender: TObject);
begin
	DrawMDIBackground(ImagenFondo.Picture.Graphic);
end;

procedure TMainForm.FormResize(Sender: TObject);
begin
	DrawMDIBackground(ImagenFondo.Picture.Graphic);
end;

procedure TMainForm.Acercade1Click(Sender: TObject);
{                                                       SS_925_MBE_20131223
resourcestring
    CAPTION_ACERCADE = 'Atenci�n al Usuario';
var
    f : TAboutFrm;
    a: TfrmLocalAssistance;
}
begin
{                                                       SS_925_MBE_20131223
    Application.CreateForm(TfrmLocalAssistance, a);
    try
        if a.Inicializar then begin
            Application.CreateForm(TAboutFrm, f);
            if f.Inicializa(CAPTION_ACERCADE, '', '', a) then f.ShowModal;
            f.Release;
        end;
    finally
        a.Release;
    end; // finally
}
    Application.CreateForm(TAcercaDeForm, AcercaDeForm);
    AcercaDeForm.ShowModal;
    AcercaDeForm.Release;
end;

procedure TMainForm.mnu_CascadaClick(Sender: TObject);
begin
	if sender = mnu_cascada then cascade
	  else if sender = mnu_mosaico then tile
	  else if sender = mnu_siguiente then next
	  else if sender = mnu_anterior then previous;
end;

procedure TMainForm.mnu_CambiodeContrasenaClick(Sender: TObject);
Var
	f: TFormCambioPassword;
begin
	if not mnu_cambiodecontrasena.Enabled then exit;
	Application.CreateForm(TFormCambioPassword, f);
	if f.Inicializa(DMConnections.BaseCac,UsuarioSistema) then f.ShowModal;
	f.Release;
end;

procedure TMainForm.mnu_VentanaClick(Sender: TObject);
begin
	mnu_cascada.Enabled := GetMenuItemCount(mnu_ventana.handle) > 5;
	mnu_mosaico.Enabled := mnu_cascada.Enabled;
	mnu_anterior.Enabled := GetMenuItemCount(mnu_ventana.handle) > 7;
	mnu_siguiente.Enabled := mnu_anterior.Enabled;
end;

procedure TMainForm.mnuSolicitudDatosClick(Sender: TObject);
var
	f: TformSolicitudDatos;
begin
	if FindFormOrCreate(TformSolicitudDatos, f) then
		f.Show
	else begin
		if not f.Inicializar(mnuSolicitudDatos.Caption, True, False) then f.Release;
	end
end;

procedure TMainForm.mnuProcesamientoRespuestasClick(Sender: TObject);
var
	f: TformProcesamientoRespuestas;
begin
	if FindFormOrCreate(TformProcesamientoRespuestas, f) then
		f.Show
	else begin
		if not f.Inicializar(mnuProcesamientoRespuestas.Caption, True) then f.Release;
	end
end;

procedure TMainForm.mnuGenerarRespuestasClick(Sender: TObject);
var
	f: TformGeneracionRespuestas;
begin
	if FindFormOrCreate(TformGeneracionRespuestas, f) then
		f.Show
	else begin
		if not f.Inicializar(mnuGenerarRespuestas.Caption, True) then f.Release;
	end
end;

procedure TMainForm.mnuPosiblesFraudesTAGCNsinConvenioClick(
  Sender: TObject);
var
	f: TformSolicitudDatos;
begin
	if FindFormOrCreate(TformSolicitudDatos, f) then
		f.ShowModal
	else begin
		if not f.Inicializar(mnuPosiblesFraudesTAGCNsinConvenio.Caption, True, True) then f.Release;
	end
end;

end.

