{********************************** File Header ********************************
File Name   : RNUTParalelo.dpr
Author      : Castro, Ra�l
Date Created: 28/03/05
Language    : ES-AR
Description : Procesamiento de datos paralelos al RNUT
*******************************************************************************}

program RNUTParalelo;

uses
  Util,
  UtilProc,
  OpenOnce,
  Controls,
  Forms,
  SysUtils,
  Windows,
  PeaProcs in '..\Comunes\PeaProcs.pas',
  PeaTypes in '..\Comunes\PeaTypes.pas',
  RStrings in '..\Comunes\RStrings.pas',
  Principal in 'Principal.pas' {MainForm},
  ConstParametrosGenerales in '..\Comunes\ConstParametrosGenerales.pas',
  DMConnection in '..\Comunes\DMConnection.pas' {DMConnections: TDataModule},
  DMOperacionesComunicacion in '..\Comunes\DMOperacionesComunicacion.pas' {DMOperacionesComunicaciones: TDataModule},
  DMComunicacion in '..\Comunes\DMComunicacion.pas' {DMComunicaciones: TDataModule},
  CambioPassword in '..\Comunes\CambioPassword.pas' {FormCambioPassword},
  OPScripts in '..\Comunes\OPScripts.pas',
  MaskCombo in '..\Componentes\MaskCombo\MaskCombo.pas',
  UtilReportes in '..\Comunes\UtilReportes.pas',
  Startup in 'Startup.pas' {FormStartup},
  FrmTerminarTarea in '..\Comunes\FrmTerminarTarea.pas' {FormTerminarTarea},
  FrmCancelarTarea in '..\Comunes\FrmCancelarTarea.pas' {FormCancelarTarea},
  FrmRptInformeUsuario in '..\Comunes\FrmRptInformeUsuario.pas' {FormRptInformeUsuario},
  FrmRptInformeUsuarioConfig in '..\Comunes\FrmRptInformeUsuarioConfig.pas' {FormRptInformeUsuarioConfig},
  frmSolicitudDatos in 'frmSolicitudDatos.pas' {formSolicitudDatos},
  frmGeneracionRespuestas in 'frmGeneracionRespuestas.pas' {formGeneracionRespuestas},
  frmProcesamientoRespuestas in 'frmProcesamientoRespuestas.pas' {formProcesamientoRespuestas},
  RVMBind in '..\Comunes\RVMBind.pas',
  RVMClient in '..\Comunes\RVMClient.pas',
  RVMTypes in '..\Comunes\RVMTypes.pas',
  LoginSetup in '..\Comunes\LoginSetup.pas' {frmLoginSetup},
  Login in '..\Comunes\Login.pas' {LoginForm},
  LoginValuesEditor in '..\Comunes\LoginValuesEditor.pas' {frmLoginValuesEditor},
  EncriptaRijandel in '..\Comunes\EncriptaRijandel.pas',
  AES in '..\Comunes\AES.pas',
  base64 in '..\Comunes\base64.pas',
  Aviso in '..\Comunes\Avisos\Aviso.pas',
  AvisoTagVencidos in '..\Comunes\Avisos\AvisoTagVencidos.pas',
  FactoryINotificacion in '..\Comunes\Avisos\FactoryINotificacion.pas',
  frmVentanaAviso in '..\Comunes\Avisos\frmVentanaAviso.pas' {VentanaAvisoForm},
  Notificacion in '..\Comunes\Avisos\Notificacion.pas',
  NotificacionImpresionDoc in '..\Comunes\Avisos\NotificacionImpresionDoc.pas',
  NotificacionImpresionReporte in '..\Comunes\Avisos\NotificacionImpresionReporte.pas' {FormNotificacionImpresionReporte},
  TipoFormaAtencion in '..\Comunes\Avisos\TipoFormaAtencion.pas',
  ClaveValor in '..\Comunes\ClaveValor.pas',
  Diccionario in '..\Comunes\Diccionario.pas',
  frmMuestraMensaje in '..\Comunes\frmMuestraMensaje.pas' {PanelMensajesForm},
  frmMuestraPDF in '..\Comunes\frmMuestraPDF.pas' {MuestraPDFForm},
  SysUtilsCN in '..\Comunes\SysUtilsCN.pas',
  frmBloqueosSistema in '..\Comunes\frmBloqueosSistema.pas' {BloqueosSistemaForm},
  PeaProcsCN in '..\Comunes\PeaProcsCN.pas',
  MsgBoxCN in '..\Comunes\MsgBoxCN.pas' {frmMsgBoxCN},
  frmAcercaDe in '..\Comunes\frmAcercaDe.pas' {AcercaDeForm},
  ImgTypes in '..\Comunes\ImgTypes.pas',
  frmMensajeACliente in '..\Comunes\frmMensajeACliente.pas' {FormMensajeACliente},
  Crypto in '..\Comunes\Crypto.pas';

{$R *.res}

begin
	Application.Initialize;
	Application.Title := 'Gesti�n de Interoperabilidad - Procedimientos Paralelos RNUT';
	if not AlreadyOpen then begin
		Screen.Cursor	:= crAppStart;
		FormStartup		:= TFormStartup.Create(nil);
		FormStartup.Show;
		FormStartup.Update;
		Delay(1000);
		FormStartUp.Free;
		Application.CreateForm(TDMConnections, DMConnections);
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TFormMensajeACliente, FormMensajeACliente);
  MainForm.Width := Screen.DesktopWidth;
		MainForm.Height := Screen.DesktopHeight;
		MainForm.Top := Screen.DesktopTop;
		MainForm.Left := Screen.DesktopLeft;
		if MainForm.Inicializar then
            MainForm.Show
        else MainForm.Close;
	end;
	Application.Run;
end.
