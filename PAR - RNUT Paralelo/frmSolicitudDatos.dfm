object formSolicitudDatos: TformSolicitudDatos
  Left = 496
  Top = 156
  Anchors = []
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Solicitud Datos'
  ClientHeight = 192
  ClientWidth = 424
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 6
    Top = 6
    Width = 409
    Height = 147
  end
  object Label1: TLabel
    Left = 24
    Top = 24
    Width = 110
    Height = 13
    Caption = 'Fecha de los Tr'#225'nsitos:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 24
    Top = 55
    Width = 216
    Height = 13
    Caption = 'Ubicaci'#243'n de los archivos de datos a solicitar:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object SpeedButton1: TSpeedButton
    Left = 375
    Top = 69
    Width = 23
    Height = 23
    Caption = '...'
    OnClick = SpeedButton1Click
  end
  object lbl_Progreso: TLabel
    Left = 24
    Top = 104
    Width = 42
    Height = 13
    Caption = 'Progreso'
  end
  object FechaTransitos: TDateEdit
    Left = 162
    Top = 21
    Width = 103
    Height = 21
    AutoSelect = False
    TabOrder = 0
    Date = -693594.000000000000000000
  end
  object txtUbicacion: TEdit
    Left = 24
    Top = 70
    Width = 345
    Height = 21
    TabOrder = 1
  end
  object btn_Procesar: TButton
    Left = 166
    Top = 160
    Width = 75
    Height = 25
    Caption = '&Procesar'
    TabOrder = 2
    OnClick = btn_ProcesarClick
  end
  object btn_Cancelar: TButton
    Left = 254
    Top = 160
    Width = 75
    Height = 25
    Caption = '&Cancelar'
    TabOrder = 3
    OnClick = btn_CancelarClick
  end
  object btn_Salir: TButton
    Left = 342
    Top = 160
    Width = 75
    Height = 25
    Caption = '&Salir'
    TabOrder = 4
    OnClick = btn_SalirClick
  end
  object pb_Progreso: TProgressBar
    Left = 24
    Top = 120
    Width = 377
    Height = 19
    TabOrder = 5
  end
  object sp_ObtenerPosiblesInfractoresFraudes: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    CommandTimeout = 300
    ProcedureName = 'ObtenerPosiblesInfractoresFraudes;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 352
    Top = 10
  end
  object spRegistrarSeguimientoConsultaRNUTParalelo: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    CommandTimeout = 300
    ProcedureName = 'RegistrarSeguimientoConsultaRNUTParalelo'
    Parameters = <>
    Left = 381
    Top = 10
  end
  object sp_ObtenerTransitosLimboRNUTP: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    CommandTimeout = 300
    ProcedureName = 'ObtenerTransitosLimboRNUTP;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaTransitos'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CantidadTransitosLimbo'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 238
    Top = 10
  end
  object sp_ObtenerTransitosAReportarRNUTP: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    CommandTimeout = 300
    ProcedureName = 'ObtenerTransitosAReportarRNUTP;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaTransitos'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@NumCorrCA'
        Attributes = [paNullable]
        DataType = ftLargeint
        Direction = pdInputOutput
        Precision = 19
        Value = Null
      end>
    Left = 267
    Top = 10
  end
  object sp_ObtenerPosiblesInfractoresFraudesCN: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    CommandTimeout = 300
    ProcedureName = 'ObtenerPosiblesInfractoresFraudesCN;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaTransitos'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 352
    Top = 40
  end
  object sp_ObtenerPatentesAReportarRNUTP: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    CommandTimeout = 300
    ProcedureName = 'ObtenerPatentesAReportarRNUTP;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 295
    Top = 10
  end
  object sp_RegistrarTransitosConsultadosRNUTP: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    CommandTimeout = 300
    ProcedureName = 'RegistrarTransitosConsultadosRNUTP;1'
    Parameters = <>
    Left = 323
    Top = 10
  end
end
