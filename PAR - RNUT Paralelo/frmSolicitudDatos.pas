{********************************** File Header ********************************
File Name   : frmSolicitudDatos.pas
Author      : rcastro
Date Created: 28/03/2005
Language    : ES-AR
Description : M�dulo de solicitud de datos por posibles tr�nsitos infractores
				o fraudulentos


Firma: SS_1147_MCA_20140408
Descripcion: se reemplaza constante COD_CONCESIONARIA por Codigo Concesionaria Nativa
*******************************************************************************}
unit frmSolicitudDatos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UtilProc, StrUtils, StdCtrls, DPSControls, DB, ADODB, Validate,
  DateEdit, DMConnection, ExtCtrls, Buttons, PeaProcs, Util, ConstParametrosGenerales,
  ComCtrls;

type
  TformSolicitudDatos = class(TForm)
	FechaTransitos: TDateEdit;
    sp_ObtenerPosiblesInfractoresFraudes: TADOStoredProc;
	Label1: TLabel;
	Label2: TLabel;
	txtUbicacion: TEdit;
	SpeedButton1: TSpeedButton;
	Bevel1: TBevel;
    spRegistrarSeguimientoConsultaRNUTParalelo: TADOStoredProc;
    sp_ObtenerTransitosLimboRNUTP: TADOStoredProc;
    sp_ObtenerTransitosAReportarRNUTP: TADOStoredProc;
    btn_Procesar: TButton;
    btn_Cancelar: TButton;
    btn_Salir: TButton;
    pb_Progreso: TProgressBar;
    lbl_Progreso: TLabel;
    sp_ObtenerPosiblesInfractoresFraudesCN: TADOStoredProc;
    sp_ObtenerPatentesAReportarRNUTP: TADOStoredProc;
    sp_RegistrarTransitosConsultadosRNUTP: TADOStoredProc;
    procedure btn_SalirClick(Sender: TObject);
    procedure btn_CancelarClick(Sender: TObject);
    procedure btn_ProcesarClick(Sender: TObject);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure btnCancelarClick(Sender: TObject);
	procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
	procedure SpeedButton1Click(Sender: TObject);
	function ObtenerSecuencia(Directorio, TipoArchivo : string): Integer;
	function Generar(TipoTransito: String) : Boolean;
  private
	{ Private declarations }
	FArchivos: ANSIString;
	CodigoOperacion: Integer;
    FTAGsPropios,
	FCancel : Boolean;
    FConcesionariaNativa: Integer;                                               //SS_1147_MCA_20140408
	procedure RegistrarOperacion;
	procedure RegistrarOperacionErronea;
    procedure GenerarArchivoPosiblesInfractoresOFraudes;
    procedure GenerarArchivoPosiblesFraudesTAGsSinConvenio;
  public
	{ Public declarations }
	function Inicializar(txtCaption: String; MDIChild : Boolean; TAGsPropios: Boolean = false):Boolean;
  end;

var
  formSolicitudDatos: TformSolicitudDatos;

implementation

uses PeaTypes;

{$R *.dfm}

resourcestring
	MSG_USER_CANCELLED		= 'Proceso cancelado por el usuario';
	MSG_PROCESS_FINISHED_OK	= 'El proceso ha teminado correctamente';
	MSG_ERROR_GETTING_DATA  = 'Error al obtener datos de posibles infractores/fraudes';
	MSG_NO_DATA_REQUIRED	= 'No hay requerimientos de datos para la fecha indicada';
	MSG_ERROR_FILE_DIRECTORY= 'La ubicaci�n ingresada es incorrecta o no existe';
	MSG_ERROR_DATE			= 'La fecha ingresada es inv�lida';
	MSG_ERROR_FILE_GENERATION= 'Error al generar el archivo de solicitud de datos por posibles %s';

function TformSolicitudDatos.Inicializar(txtCaption: String; MDIChild : Boolean; TAGsPropios: Boolean = false):Boolean;
resourcestring
	MSG_ERROR_DIRECTORIO_NO_EXISTE	= 'El directorio %s  especificado en par�metros generales ' +
	'para almacenar las solicitudes de datos a enviar no existe.';
Var
	//S: TSize;                                                                 //SS_1147_MCA_20140408
	FDirSalida: String;
begin
	Result := False;
	Caption := AnsiReplaceStr(txtCaption, '&', '');
	btn_Cancelar.Enabled := False;
    FechaTransitos.Date := Date;
	FTAGsPropios := TAGsPropios;
	ObtenerParametroGeneral(DMCOnnections.BaseCAC, 'Dir_Salida_RNUTP', FDirSalida);
	txtUbicacion.Text := GoodDir(FDirSalida);
    FConcesionariaNativa := ObtenerCodigoConcesionariaNativa;                   //SS_1147_MCA_20140408
	if not DirectoryExists(FDirSalida) then begin
		MsgBox(Format(MSG_ERROR_DIRECTORIO_NO_EXISTE, [FDirSalida]), Caption, MB_OK + MB_ICONERROR);
		Exit;
	end;
	FArchivos := '';
	Result := True
end;

procedure TformSolicitudDatos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree
end;

procedure TformSolicitudDatos.btnCancelarClick(Sender: TObject);
begin

end;

{******************************** Function Header ******************************
Function Name: ObtenerSecuencia
Author       : rcastro
Date Created : 06/04/2005
Description  : Obtiene el nro. de secuencia de generaci�n de un archivos de
				solicitud de datos
Parameters   : Directorio, TipoArchivo : string
Return Value : Integer
*******************************************************************************}
function TformSolicitudDatos.ObtenerSecuencia(Directorio, TipoArchivo : string): Integer;
var
  DirInfo: TSearchRec;
  Existe,
  SecEnArchivo,
  Secuencia: Integer;
begin
	Secuencia := 0;

	try
		try
			Existe := FindFirst(Directorio + TipoArchivo, FaAnyfile, DirInfo);

			while Existe = 0 do  begin
				if (DirInfo.Attr and faDirectory <> faDirectory) and (DirInfo.Attr and faVolumeId <> faVolumeID) then begin
					SecEnArchivo :=	StrToInt (ParseParamByNumber(DirInfo.Name, 4, '_'));
					Secuencia := iif (Secuencia < SecEnArchivo, SecEnArchivo, Secuencia)
				end;

				Existe := FindNext(DirInfo);
			end;
		except
			on e: Exception do // error al buscar archivos
		end;
	finally
		// Libero los recursos de FindFirst
		SysUtils.FindClose(DirInfo);
		Result := Secuencia + 1
	end
end;

{******************************** Function Header ******************************
Function Name: Generar
Author       : rcastro
Date Created : 30/03/2005
Description  : Genera el archivo de consulta por posibles infractores/fraudes
				para las otras concesionarias
Parameters   : TipoTransito: String
Return Value : Boolean


Revision 1
    Autor: dAllegretti
    Fecha: 30/09/2008
    Descripcion: Se modifica la extensi�n del archivo, pasa de .Dat a .Txt.
*******************************************************************************}
function TformSolicitudDatos.Generar (TipoTransito: String) : Boolean;
resourcestring
	REQUEST_PREFIX 	= 'paraverif_';
	INFRACTOR_PREFIX= 'infrac_';
	FRAUD_PREFIX 	= 'fraud_';
var
	F: TextFile;
	NombreArchivo: String;
    Linea: String;

begin

	Result := true;
	if FCancel then Exit;

	sp_ObtenerPosiblesInfractoresFraudes.Filter := 'TipoEstado = ' + QuotedStr(TipoTransito[1]);
	if sp_ObtenerPosiblesInfractoresFraudes.IsEmpty then Exit;

	Screen.Cursor := crHourGlass;
	try
		try
			if TipoTransito[1] = 'I' then begin
                if FTAGsPropios then
                    NombreArchivo := 'TELEV_CN_SIN_CONVENIO_INFR_' + FormatDateTime('yymmdd', Date)
                else
                    //NombreArchivo := REQUEST_PREFIX + INFRACTOR_PREFIX + IntToStr (COD_CONCESIONARIA) + '_' +				//SS_1147_MCA_20140408
                      NombreArchivo := REQUEST_PREFIX + INFRACTOR_PREFIX + IntToStr (FConcesionariaNativa) + '_' +			//SS_1147_MCA_20140408
                                     PadL (IntToStr(ObtenerSecuencia(GoodDir (Trim(txtUbicacion.Text)), REQUEST_PREFIX + 'infrac_*.txt')), 4, '0') + '_' +
                                     FormatDateTime('yymmdd', Date) //Revision 1
			end
			else begin
                if FTAGsPropios then
                    NombreArchivo := 'TELEV_CN_SIN_CONVENIO_FRAUD_' + FormatDateTime('yymmdd', Date)
                else
                    //NombreArchivo := REQUEST_PREFIX + FRAUD_PREFIX + IntToStr (COD_CONCESIONARIA) + '_' +					//SS_1147_MCA_20140408
                      NombreArchivo := REQUEST_PREFIX + FRAUD_PREFIX + IntToStr (FConcesionariaNativa) + '_' +				//SS_1147_MCA_20140408
                                     PadL (IntToStr(ObtenerSecuencia(GoodDir (Trim(txtUbicacion.Text)), REQUEST_PREFIX + 'fraud_*.txt')), 4, '0') + '_' +
                                     FormatDateTime('yymmdd', Date) //Revision 1
			end;

			FArchivos := Trim(FArchivos + ' ' + NombreArchivo);

			AssignFile (F, GoodDir (Trim(txtUbicacion.Text)) + NombreArchivo + '.txt'); //Revision 1
			ReWrite(F);

			with sp_ObtenerPosiblesInfractoresFraudes do begin
				First;
				while (not Eof) and (not FCancel) do begin
{                    if not FieldByName('Patente').IsNull then
                        if Trim(FieldByName('Patente').AsString) <> '' then
        					Write(F, );
					if TipoTransito[1] = 'F' then
						Write(F, ';', PadL(Trim(FieldByName('ContractSerialNumber').AsString), 11, '0'));}

                    Linea := '';
                    if not FieldByName('Patente').IsNull then
                        if Trim(FieldByName('Patente').AsString) <> '' then
                           Linea := Trim(FieldByName('Patente').AsString);

    				if (TipoTransito[1] = 'F') then
                        if not FieldByName('ContractSerialNumber').IsNull then
    						Linea := Linea + ';' + PadL(Trim(FieldByName('ContractSerialNumber').AsString), 11, '0')
                        else Linea := Linea + ';';

                    if Linea <> '' then
    					WriteLn(F, Linea);

					Application.ProcessMessages;
					Next;
				end;
			end;

            if not FTAGsPropios then begin
    			with spRegistrarSeguimientoConsultaRNUTParalelo, Parameters do begin
                    Parameters.Refresh;
                    ParamByName ('@TipoConsulta').Value := TipoTransito[1];
                    ParamByName ('@NombreArchivo').Value := NombreArchivo;
                    ExecProc
                end
            end;

			CloseFile (F);
		except
			// Error al generar el archivo de solicitud de datos
			on E: Exception do begin
				CloseFile (F);
				DeleteFile (NombreArchivo);
				Result := False;
				MsgBox(Format(MSG_ERROR_FILE_GENERATION, [TipoTransito]), self.Caption, MB_OK + MB_ICONSTOP);
			end
		end
	finally
		Screen.Cursor := crDefault
	end
end;

procedure TformSolicitudDatos.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
	CanClose := not btn_Cancelar.Enabled
end;

procedure TformSolicitudDatos.SpeedButton1Click(Sender: TObject);
resourcestring
	MSG_BROWSE_TITLE = 'Seleccione la ubicaci�n d�nde se guardar�n los archivos de solicitudes de datos ';
var
	FDirSalida: String;
begin
	txtUbicacion.Text := GoodDir(Trim(BrowseForFolder(MSG_BROWSE_TITLE)));
	if Trim(txtUbicacion.Text) = '' then begin
		ObtenerParametroGeneral(DMCOnnections.BaseCAC, 'Dir_Salida_RNUTP', FDirSalida);
		txtUbicacion.Text := GoodDir(FDirSalida);
	end
end;

procedure TformSolicitudDatos.RegistrarOperacion;
var
	DescriError: String;
resourcestring
	STR_PROCESS_OK = 'Solicitud de datos externos exitoso.';
	STR_FILES = 'Archivos: ';
begin
	// Genero el registro de operaci�n
	DMConnections.BaseCAC.BeginTrans;

	CodigoOperacion := CrearRegistroOperaciones(
		DMConnections.BaseCAC,
		SYS_RNUT_PARALELO,
		RO_MOD_PROCESO_RNUT_PARALELO,
		RO_AC_PROCESAR_TRANSITOS_RNUT_PARALELO,
		SEV_INFO,
		GetMachineName,
		UsuarioSistema,
		STR_PROCESS_OK,
		0,
		0,
		0,
		iif (FArchivos = '', '', STR_FILES + FArchivos),
		DescriError);

	DMConnections.BaseCAC.CommitTrans;
end;

procedure TformSolicitudDatos.RegistrarOperacionErronea;
resourcestring
	MSG_ERROR_FOUND = 'Errores encontrados';
	MSG_EVENT_ERROR	= 'La solicitud de datos externos se ha cancelado por errores.';
	STR_FILES = 'Archivos: ';
var
	Error: ANSIString;
begin
	Error := MSG_ERROR_FOUND;

	ActualizarRegistroOperacion(
		DMCOnnections.BaseCAC,
		SYS_RNUT_PARALELO,
		CodigoOperacion,
		SEV_ALTA,
		False,
		MSG_EVENT_ERROR,
		0,
		0,
		iif (FArchivos = '', '', STR_FILES + FArchivos),
		Error);
end;

procedure TformSolicitudDatos.btn_ProcesarClick(Sender: TObject);
begin
    if FTAGsPropios then begin
        GenerarArchivoPosiblesFraudesTAGsSinConvenio;
    end else begin
        GenerarArchivoPosiblesInfractoresOFraudes;
    end;
end;

procedure TformSolicitudDatos.btn_CancelarClick(Sender: TObject);
begin
	FCancel := True;
end;

procedure TformSolicitudDatos.btn_SalirClick(Sender: TObject);
begin
	Close;
end;

procedure TformSolicitudDatos.GenerarArchivoPosiblesInfractoresOFraudes;
resourcestring
    MSG_PROGRESO = 'Progreso';
    MSG_OBTENIENDO_DATOS = 'Obteniendo datos...';
    MSG_FILTRANDO_DATOS = 'Filtrando datos...';
    MSG_REGISTRANDO_ENVIOS = 'Registrando env�os...';
    MSG_ENVIANDO_DATOS = 'Enviando datos...';
    ERROR_ARCHIVO_INFRACTORES = 'Ha ocurrido un problema al generar el archivo de Posibles Infratores';
    ERROR_ARCHIVO_FRAUDES = 'Ha ocurrido un problema al generar el archivo de Posibles Fraudes';
var
	TodoOk: Boolean;
    NumCorrCA: Int64;
    CantidadTransitosLimbo: Integer;
begin
	if not ValidateControls([FechaTransitos, txtUbicacion],
	    [(FechaTransitos.Date <> NULLDATE), (txtUbicacion.text <> '') And DirectoryExists(txtUbicacion.text)],
		Self.Caption,
		[MSG_ERROR_DATE, MSG_ERROR_FILE_DIRECTORY]) then Exit;

    btn_Cancelar.Enabled := True;
	btn_Salir.Enabled := False;
	btn_Procesar.Enabled := False;
	FCancel := false;
	TodoOk := false;
	try
		try
    		Screen.Cursor := crHourGlass;
			RegistrarOperacion;

            lbl_Progreso.Caption := MSG_OBTENIENDO_DATOS;
			with sp_ObtenerTransitosLimboRNUTP, Parameters do begin
				ParamByName ('@FechaTransitos').Value := FechaTransitos.Date;
                ExecProc;
                CantidadTransitosLimbo := ParamByName('@CantidadTransitosLimbo').Value;
			end;

            with sp_ObtenerTransitosAReportarRNUTP, Parameters do begin
                NumCorrCA := 0;
                pb_Progreso.Max := CantidadTransitosLimbo;
                repeat
                    ParamByName('@NumCorrCA').Value := NumCorrCA;
                    ParamByName('@FechaTransitos').Value := FechaTransitos.Date;
                    ExecProc;
                    NumCorrCA := ParamByName('@NumCorrCA').Value;
                    pb_Progreso.Position := pb_Progreso.Position + 100;
        			Application.ProcessMessages;
                until NumCorrCA = 0;
                pb_Progreso.Position := pb_Progreso.Max;
                Close;
            end;

            lbl_Progreso.Caption := MSG_FILTRANDO_DATOS;
			with sp_ObtenerPatentesAReportarRNUTP do begin
				Close;
                ExecProc;
			end;

    		if FCancel then Exit;

   			with sp_ObtenerPosiblesInfractoresFraudes do begin
				Close;
				Open;
			end;

			if sp_ObtenerPosiblesInfractoresFraudes.IsEmpty then begin
				MsgBox(MSG_NO_DATA_REQUIRED, Self.Caption, MB_OK + MB_ICONINFORMATION)
			end else begin
				if not Generar('Infractores') then MsgBox(ERROR_ARCHIVO_INFRACTORES, Self.Caption, MB_OK + MB_ICONERROR);
				if not Generar('Fraudes') then MsgBox(ERROR_ARCHIVO_FRAUDES, Self.Caption, MB_OK + MB_ICONERROR);
                lbl_Progreso.Caption := MSG_REGISTRANDO_ENVIOS;
    			with sp_RegistrarTransitosConsultadosRNUTP do begin
    				Close;
    				ExecProc;
    			end;
               	btn_Procesar.Enabled := True;
                Screen.Cursor := crDefault;
				MsgBox(MSG_PROCESS_FINISHED_OK, self.Caption, MB_OK + MB_ICONINFORMATION)
			end;

			TodoOk := true;
		except
			on e: Exception do begin
				Screen.Cursor := crDefault;
				MsgBoxErr(MSG_ERROR_GETTING_DATA, e.message, self.caption, MB_ICONSTOP)
			end;
		end
	finally
		if FCancel then
			MsgBox(MSG_USER_CANCELLED, self.Caption, MB_OK + MB_ICONINFORMATION)
		else begin
			if not TodoOk then RegistrarOperacionErronea
		end;
        sp_ObtenerTransitosLimboRNUTP.Close;
        sp_ObtenerTransitosAReportarRNUTP.Close;
        sp_ObtenerPatentesAReportarRNUTP.Close;
        sp_ObtenerPosiblesInfractoresFraudes.Close;
        sp_RegistrarTransitosConsultadosRNUTP.Close;

		btn_Cancelar.Enabled := False;
		btn_Salir.Enabled := True;
        lbl_Progreso.Caption := MSG_PROGRESO;
        pb_Progreso.Position := 0;
  		Screen.Cursor := crDefault;
	end
end;

procedure TformSolicitudDatos.GenerarArchivoPosiblesFraudesTAGsSinConvenio;
var
	TodoOk: Boolean;
begin
	if not ValidateControls([FechaTransitos, txtUbicacion],
		[(FechaTransitos.Date <> NULLDATE), (txtUbicacion.text <> '') And DirectoryExists(txtUbicacion.text)],
		Self.Caption,
		[MSG_ERROR_DATE, MSG_ERROR_FILE_DIRECTORY]) then Exit;

	btn_Cancelar.Enabled := true;
	btn_Salir.Enabled := False;
	btn_Procesar.Enabled := false;
	FCancel := false;
	TodoOk := false;

	try
		try
			RegistrarOperacion;

			// Ejecutar el sp para obtener las patententes de tr�nsitos en
			// posible infracci�n o fraude
			Screen.Cursor := crHourGlass;
			with sp_ObtenerPosiblesInfractoresFraudesCN, Parameters do begin
                Refresh;
				Filtered := true;
				Close;
				ParamByName ('@FechaTransitos').Value := FechaTransitos.Date;
				Open
			end;
			Screen.Cursor := crDefault;

			Application.ProcessMessages;

			if FCancel then Exit;

			if sp_ObtenerPosiblesInfractoresFraudesCN.IsEmpty then
				MsgBox(MSG_NO_DATA_REQUIRED, self.Caption, MB_OK + MB_ICONINFORMATION)
			else begin
				if not Generar ('Infractores') then Exit;
				if not Generar ('Fraudes') then Exit;
				MsgBox(MSG_PROCESS_FINISHED_OK, self.Caption, MB_OK + MB_ICONINFORMATION)
			end;

			TodoOk := true;
		except
			// Error al obtener datos de los posibles infractores/fraudes
			on e: Exception do begin
				Screen.Cursor := crDefault;
				MsgBoxErr(MSG_ERROR_GETTING_DATA, e.message, self.caption, MB_ICONSTOP)
			end;
		end
	finally
		if FCancel then
			MsgBox(MSG_USER_CANCELLED, self.Caption, MB_OK + MB_ICONINFORMATION)
		else begin
			if not TodoOk then RegistrarOperacionErronea
		end;

		sp_ObtenerPosiblesInfractoresFraudesCN.Close;
		Screen.Cursor := crDefault;

		btn_Procesar.Enabled := true;
		btn_Cancelar.Enabled := false;
		btn_Salir.Enabled := true;
	end
end;

end.

