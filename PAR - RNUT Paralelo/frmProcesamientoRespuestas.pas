{********************************** File Header ********************************
File Name   : frmProcesamientoRespuestas.pas
Author      : rcastro
Date Created: 28/03/2005
Language    : ES-AR
Description : M�dulo de generaci�n de respuestas a datos solicitados por otras
				concesionarias

Revision : 1
    Date: 13/03/2009
    Author: mpiazza
    Description:  Ref (ss-716) Se modificaron los commandtimeout de las
                  componenentes de base de datos en el dfm
  Revision : 2
    Date: 01/05/2009
    Author: mpiazza
    Description:  Ref (ss-716) Se modificaron los commandtimeout por codigo

  Revision : 3
    Date: 06/07/2009
    Author: mpiazza
    Description:  Ref (ss-661) se modifico el mensaje de error de proceso

  Revision : 4
    Date: 03/12/2009
    Author: mpiazza
    Description:  Ref (ss-850) se modifico el procedimiento LeerArchivoRespuesta()


    Firma: SS_1147_MCA_20140408
    Descripcion: se reemplaza constante COD_CONCESIONARIA por Codigo Concesionaria Nativa
*******************************************************************************}
unit frmProcesamientoRespuestas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UtilProc, StrUtils, StdCtrls, DPSControls, DB, ADODB, Validate,
  DateEdit, DMConnection, ExtCtrls, Buttons, PeaProcs, Util, DmiCtrls,
  BuscaTab, ConstParametrosGenerales;

type
  TformProcesamientoRespuestas = class(TForm)
	btnSalir: TDPSButton;
	btnCancelar: TDPSButton;
	btnProcesar: TDPSButton;
	Bevel1: TBevel;
	Label3: TLabel;
	ArchivoInfracciones: TBuscaTabEdit;
	Label4: TLabel;
	ArchivoFraudes: TBuscaTabEdit;
	Open: TOpenDialog;
	spActualizarRNUTParalelo: TADOStoredProc;
    btnLimpiarInfractores: TSpeedButton;
    btnLimpiarFraudes: TSpeedButton;
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure btnCancelarClick(Sender: TObject);
	procedure btnProcesarClick(Sender: TObject);
	procedure btnSalirClick(Sender: TObject);
	procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
	function LeerArchivoRespuesta(ArchivoRespuesta: String) : Boolean;
	function ProcesarRespuestas: Boolean;
	procedure ArchivoInfraccionesButtonClick(Sender: TObject);
	procedure ArchivoFraudesButtonClick(Sender: TObject);
    procedure btnLimpiarInfractoresClick(Sender: TObject);
    procedure btnLimpiarFraudesClick(Sender: TObject);
  private
	{ Private declarations }
	FPatente,
	FContractSerialNumber,
	FCategoria,
	FEstadoContrato,
	FNumeroContrato,
	FFechaLeida,
	FConcesionariaRespuesta,
	FTipoRespuesta: String;
	FFechaRespuesta,
	FFechaAltaCuenta,
	FFechaBajaCuenta: TDateTime;
	FCancel : Boolean;
	CodigoOperacion: Integer;
	FListaRespuesta: TStringList;
    FConcesionariaNativa: Integer;                                               //SS_1147_MCA_20140408
	procedure RegistrarOperacion;
	procedure RegistrarOperacionErronea;
  public
	{ Public declarations }
	function Inicializar(txtCaption: String; MDIChild : Boolean):Boolean;
  end;

var
  formProcesamientoRespuestas: TformProcesamientoRespuestas;

implementation

uses PeaTypes;

{$R *.dfm}

resourcestring
	MSG_USER_CANCELLED			= 'Proceso cancelado por el usuario';
	MSG_PROCESS_FINISHED_OK		= 'El proceso ha teminado correctamente';
	MSG_NO_DATA					= 'No hay datos a procesar';
	ANSWER_PREFIX 				= 'respverif_';
	MSG_FILE_SELECTION			= 'Seleccionar Archivo a Procesar...';

function TformProcesamientoRespuestas.Inicializar(txtCaption: String; MDIChild : Boolean):Boolean;
resourcestring
	MSG_ERROR_DIRECTORIO_NO_EXISTE	= 'El directorio %s especificado en par�metros generales ' +
	'para la entrada de respuestas a solicitudes no existe.';
Var
	S: TSize;
	FDirEntrada: String;
begin
	result := false;

	if MDIChild then begin
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;

	CenterForm(Self);
	Caption := AnsiReplaceStr(txtCaption, '&', '');
	btnCancelar.Enabled := False;

    FConcesionariaNativa := ObtenerCodigoConcesionariaNativa;                    //SS_1147_MCA_20140408

	ObtenerParametroGeneral(DMCOnnections.BaseCAC, 'Dir_Entrada_RNUTP', FDirEntrada);
	if not DirectoryExists(FDirEntrada) then begin
		MsgBox(Format(MSG_ERROR_DIRECTORIO_NO_EXISTE, [FDirEntrada]), Caption, MB_OK + MB_ICONERROR);
		Exit;
	end;
	Open.InitialDir := GoodDir(FDirEntrada);

	result := true
end;

procedure TformProcesamientoRespuestas.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree
end;

procedure TformProcesamientoRespuestas.btnCancelarClick(Sender: TObject);
begin
	FCancel := True;
end;

procedure TformProcesamientoRespuestas.btnLimpiarInfractoresClick(
  Sender: TObject);
begin
    ArchivoInfracciones.Text := 'Seleccionar Archivo a Procesar...';
end;

{******************************** Function Header ******************************
Function Name: ProcesarRespuestas
Author       : rcastro
Date Created : 30/03/2005
Description  : Lectura del archivo respuesta de otras concesionarias
Parameters   : None
Return Value : Boolean

Revision : 1
  Date: 06/07/2009
  Author: mpiazza
  Description:  Ref (ss-661) se modifico el mensaje de error de proceso, 

Revision : 2
  Date: 03/12/2009
  Author: mpiazza
  Description:  Ref (ss-850) Se agrega mensaje de error para formato de nombre de archivo incorrecto 

*******************************************************************************}
function TformProcesamientoRespuestas.LeerArchivoRespuesta(ArchivoRespuesta: String) : Boolean;
resourcestring
	MSG_ERROR_READING_INPUT_DATA= 'No se pudo leer archivo de respuesta %s';
	MSG_ERROR_WRONG_FILE_NAME = 'El formato del nombre del archivo "%s" es invalido';
	MSG_ERROR_WRONG_DATE 		= 'La fecha de respuesta en el archivo %s es inv�lida';
var
	F: TextFile;
	Linea: String;
  iReg : integer; //numero de registro para en caso de error ser informado el numero de linea donde se produce
  NombreArchivo : string;
begin
	Result := False; // Habia un True DCalani
  iReg := 1;
	if not FileExists (ArchivoRespuesta) then Exit;
	if FCancel then Exit;
  try
    Linea := Copy (ArchivoRespuesta, Pos (ExtractFileExt (ArchivoRespuesta), ArchivoRespuesta) - 6, 6);
    if not TryEncodeDate(2000+StrToInt(Copy(Linea, 1, 2)), StrToInt(Copy(Linea, 3, 2)), StrToInt(Copy(Linea, 5, 2)), FFechaRespuesta) then begin
      MsgBox(Format (MSG_ERROR_WRONG_DATE, [ArchivoRespuesta]), self.Caption, MB_OK + MB_ICONINFORMATION);
  //		result := true
      Exit;
    end;
  	except
			// Error al leer el nombre del archivo,
			on e: Exception do begin
				Screen.Cursor := crDefault;
        		ShowMessage(Format(MSG_ERROR_WRONG_FILE_NAME, [ExtractFileName(ArchivoRespuesta)]));
        		exit;
			end;
  	end;
	FTipoRespuesta := UpCase(ParseParamByNumber(ExtractFileName (ArchivoRespuesta), 2, '_')[1]);
	FConcesionariaRespuesta := ParseParamByNumber(ExtractFileName (ArchivoRespuesta), 4, '_');;
  	NombreArchivo := ExtractFileName (ArchivoRespuesta);
	Screen.Cursor := crHourGlass;
	try
		try
			AssignFile (F, ArchivoRespuesta);
			Reset(F);

			while (not EoF(F)) and (not FCancel) do begin
				Application.ProcessMessages;
				ReadLn(F, Linea);

				FListaRespuesta.Add(DateToStr(FFechaRespuesta) + ';' +
									FConcesionariaRespuesta + ';' +
									FTipoRespuesta + ';' +
                  inttostr(iReg)  + ';' +
                  NombreArchivo  + ';' +
									Linea );
        inc(iReg);
			end;

			Result := True;
		except
			// Error al leer archivo de respuesta
			on e: Exception do begin
				Screen.Cursor := crDefault;
				MsgBoxErr(Format(MSG_ERROR_READING_INPUT_DATA, [ArchivoRespuesta]), e.message, self.caption, MB_ICONSTOP)
			end;
		end
	finally
		Screen.Cursor := crDefault;
		CloseFile(F);
	end;
end;

{******************************** Function Header ******************************
Function Name: ProcesarRespuestas
Author       : rcastro
Date Created : 30/03/2005
Description  : Parsea las respuestas y actualiza la tabla RNUTParalelo
Parameters   : None
Return Value : Boolean

  Revision : 1
    Date: 01/05/2009
    Author: mpiazza
    Description:  Ref (ss-716) Se modificaron los commandtimeout por codigo

  Revision : 2
    Date: 06/07/2009
    Author: mpiazza
    Description:  Ref (ss-661) se modifico el mensaje de error de proceso,
    se le integra numero de linea y archivo para ser informado en caso de error
*******************************************************************************}
function TformProcesamientoRespuestas.ProcesarRespuestas: Boolean;
var
	NroConver, i: integer;
	HayError: Boolean;
  NroReg        : string;
  NombreArchivo : string;
  CampoError    : string;
  msgErr        : string;
resourcestring
	MSG_ERROR_WRITING_DATA = 'Error de sintaxis, no se pudo actualizar RNUT Paralelo';
	MSG_ERROR_WRITING_DETAIL = 'Error de sintaxis en el archivo : %s ' + CRLF +
          'Campo : %s ' + CRLF +
          'L�nea : %s ' + CRLF +
          'Error : %s ' + CRLF ;
begin
	HayError:= false;

	Screen.Cursor := crHourGlass;
	DMConnections.BaseCAC.BeginTrans;
	try
		i := 0;
		while (i < FListaRespuesta.Count) and (not FCancel) and (not HayError) do begin
			Application.ProcessMessages;
      NroReg := ParseParamByNumber(FListaRespuesta.Strings[i], 4, ';'); // Esta variable informa el numero de linea en caso de error
      NombreArchivo := ParseParamByNumber(FListaRespuesta.Strings[i], 5, ';'); // Esta variable informa el nombre del archivo fuente
      CampoError := 'FechaRespuesta';
			FFechaRespuesta			:= StrToDate (ParseParamByNumber(FListaRespuesta.Strings[i], 1, ';'));
      CampoError := 'ConcesionariaRespuesta';
			FConcesionariaRespuesta := ParseParamByNumber(FListaRespuesta.Strings[i], 2, ';');
      CampoError := 'TipoRespuesta';
			FTipoRespuesta 			:= ParseParamByNumber(FListaRespuesta.Strings[i], 3, ';');
      CampoError := 'Patente';
			FPatente 				:= ParseParamByNumber(FListaRespuesta.Strings[i], 6, ';'); //4
      CampoError := 'ContractSerialNumber';
			FContractSerialNumber	:= ParseParamByNumber(FListaRespuesta.Strings[i], 7, ';');//5
      CampoError := 'Categoria';
			FCategoria				:= ParseParamByNumber(FListaRespuesta.Strings[i], 8, ';');//6
      CampoError := 'EstadoContrato';
			FEstadoContrato			:= ParseParamByNumber(FListaRespuesta.Strings[i], 9, ';');//7
      CampoError := 'NumeroContrato';
			FNumeroContrato			:= ParseParamByNumber(FListaRespuesta.Strings[i], 10, ';');//8
      CampoError := 'FechaLeida';

			FFechaLeida := Trim(ParseParamByNumber(FListaRespuesta.Strings[i], 12, ';'));//10
            if not(TryStrToInt(FFechaLeida, NroConver)) then FFechaBajaCuenta := NullDate
            else FFechaBajaCuenta := EncodeDate(StrToInt(StrRight(FFechaLeida, 4)), StrToInt(Copy(FFechaLeida, 3, 2)), StrToInt(StrLeft(FFechaLeida, 2)));

			FFechaLeida := Trim(ParseParamByNumber(FListaRespuesta.Strings[i], 11, ';'));//9

			try
                if (Trim(FFechaLeida) = '') or not(TryStrToInt(FFechaLeida, NroConver)) then raise Exception.Create('Error en la Fecha de Alta');

                FFechaAltaCuenta := EncodeDate(StrToInt(StrRight(FFechaLeida, 4)), StrToInt(Copy(FFechaLeida, 3, 2)), StrToInt(StrLeft(FFechaLeida, 2)));

				// actualizo RNUTParalelo
				with spActualizarRNUTParalelo, Parameters do begin
          CampoError := 'Patente';
					ParamByName ('@Patente').Value 			   	:= FPatente;
          CampoError := 'ContractSerialNumber';
					ParamByName ('@ContractSerialNumber').Value := FContractSerialNumber;
          CampoError := 'Categoria';
					ParamByName ('@CodigoCategoria').Value 		:= FCategoria;
          CampoError := 'EstadoContrato';
					ParamByName ('@EstadoContrato').Value 		:= FEstadoContrato;
          CampoError := 'NumeroContrato';
					ParamByName ('@NumeroContrato').Value 		:= FNumeroContrato;
          CampoError := 'FechaLeida'; //las dos fechas siguientes salen del mismo campo en el archivo
					ParamByName ('@FechaAlta').Value 			:= FFechaAltaCuenta;
					ParamByName ('@FechaBaja').Value 			:= iif (FFechaBajaCuenta = NullDate, Null, FFechaBajaCuenta);
					ParamByName ('@FechaRespuesta').Value 		:= FFechaRespuesta;
          CampoError := 'TipoRespuesta';
					ParamByName ('@TipoRespuesta').Value 		:= FTipoRespuesta;
          CampoError := 'ConcesionariaRespuesta';
					ParamByName ('@ConcesionariaRespuesta').Value:= FConcesionariaRespuesta;
          CommandTimeout := 1260;
					ExecProc
				end
			except
				// Error al actualizar RNUTParalelo
				on e: Exception do begin
					HayError := true;
					Screen.Cursor := crDefault;

					DMConnections.BaseCAC.RollbackTrans;
          msgErr := Format(MSG_ERROR_WRITING_DETAIL , [NombreArchivo, CampoError, NroReg, e.message]);
					MsgBoxErr(MSG_ERROR_WRITING_DATA, msgErr , self.caption, MB_ICONSTOP)

				end
			end;
			inc (i)
		end;
	finally
		if FCancel and DMConnections.BaseCAC.InTransaction then
			DMConnections.BaseCAC.RollbackTrans
		else
			if not HayError then DMConnections.BaseCAC.CommitTrans;

		Screen.Cursor := crDefault;
		result := not HayError
	end
end;

{*******************************************************************************
Revision 1
    Autor: dAllegretti
    Fecha: 05/08/2008
    Descripcion: Se modifica el procesamiento para poder emitir procesar un solo
                archivo, sin necesitad del otro (cualquiera sea el archivo).
*******************************************************************************}
procedure TformProcesamientoRespuestas.btnProcesarClick(Sender: TObject);
var
	TodoOk : boolean;
begin
	ArchivoInfracciones.Enabled := false;
	ArchivoFraudes.Enabled := false;
	btnCancelar.Enabled := true;
	btnProcesar.Enabled := false;
	btnSalir.Enabled := False;
	FCancel := false;
	TodoOk := false;

	FListaRespuesta := TStringList.Create;

	try
		RegistrarOperacion;

		// Proceso los archivos de consulta por Infracciones y Fraudes

		//if not LeerArchivoRespuesta (ArchivoInfracciones.Text) then Exit; //Comentado en revision 1
		//if not LeerArchivoRespuesta (ArchivoFraudes.Text) then Exit;      //Comentado en revision 1
        LeerArchivoRespuesta (ArchivoInfracciones.Text);                    //Revision 1
        LeerArchivoRespuesta (ArchivoFraudes.Text);                         //Revision 1

		if FListaRespuesta.Count = 0 then begin
			MsgBox(MSG_NO_DATA, self.Caption, MB_OK + MB_ICONINFORMATION)
		end
		else begin
			if not ProcesarRespuestas then Exit;
			if not FCancel then
				MsgBox(MSG_PROCESS_FINISHED_OK, self.Caption, MB_OK + MB_ICONINFORMATION)
		end;

		TodoOk := True;
	finally
		if FCancel then
			MsgBox(MSG_USER_CANCELLED, self.Caption, MB_OK + MB_ICONINFORMATION)
		else begin
			if not TodoOk then RegistrarOperacionErronea
		end;

		Screen.Cursor := crDefault;

		ArchivoInfracciones.Enabled := true;
		ArchivoFraudes.Enabled := true;
		btnCancelar.Enabled := false;
		btnProcesar.Enabled := true;
		btnSalir.Enabled := true;

		FreeAndNil(FListaRespuesta);
	end
end;

procedure TformProcesamientoRespuestas.btnSalirClick(Sender: TObject);
begin
	Close
end;

procedure TformProcesamientoRespuestas.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
	CanClose := not btnCancelar.Enabled
end;

{*******************************************************************************
Revision 1
    Autor: dAllegretti
    Fecha: 30/09/2008
    Descripcion: Se modifica la extensi�n del archivo, pasa de .Dat a .Txt.
*******************************************************************************}
procedure TformProcesamientoRespuestas.ArchivoInfraccionesButtonClick(
  Sender: TObject);
resourcestring
	STR_INFRACTORS = 'Infractores';
begin
 	//Open.Filter := STR_INFRACTORS + '|' + ANSWER_PREFIX + 'infrac_' + IntToStr (COD_CONCESIONARIA) + '_*.txt';  //SS_1147_MCA_20140408 //Revision 1
    Open.Filter := STR_INFRACTORS + '|' + ANSWER_PREFIX + 'infrac_' + IntToStr (FConcesionariaNativa) + '_*.txt';  //SS_1147_MCA_20140408		
	if not Open.Execute then ArchivoInfracciones.Text := MSG_FILE_SELECTION;

	ArchivoInfracciones.Text := Open.FileName
end;

{*******************************************************************************
Revision 1
    Autor: dAllegretti
    Fecha: 30/09/2008
    Descripcion: Se modifica la extensi�n del archivo, pasa de .Dat a .Txt.
*******************************************************************************}
procedure TformProcesamientoRespuestas.ArchivoFraudesButtonClick(
  Sender: TObject);
resourcestring
	STR_FRAUDS = 'Fraudes';
begin
	//Open.Filter := STR_FRAUDS + '|'+ ANSWER_PREFIX + 'fraud_' + IntToStr (COD_CONCESIONARIA) + '_*.txt';  //SS_1147_MCA_20140408 //Revision 1
    Open.Filter := STR_FRAUDS + '|'+ ANSWER_PREFIX + 'fraud_' + IntToStr (FConcesionariaNativa) + '_*.txt';  //SS_1147_MCA_20140408
	if not Open.Execute then ArchivoFraudes.Text := MSG_FILE_SELECTION;

	ArchivoFraudes.Text := Open.FileName;
end;

procedure TformProcesamientoRespuestas.RegistrarOperacion;
var
	DescriError: String;
resourcestring
	STR_PROCESS_OK = 'Procesamiento de respuestas externas exitoso.';
	STR_FILES = 'Archivos: ';
begin
	// Genero el registro de operaci�n
	DMConnections.BaseCAC.BeginTrans;

	CodigoOperacion := CrearRegistroOperaciones(
		DMConnections.BaseCAC,
		SYS_RNUT_PARALELO,
		RO_MOD_PROCESO_RNUT_PARALELO,
		RO_AC_PROCESAR_TRANSITOS_RNUT_PARALELO,
		SEV_INFO,
		GetMachineName,
		UsuarioSistema,
		STR_PROCESS_OK,
		0,
		0,
		0,
		STR_FILES + Trim(ExtractFileName(ArchivoInfracciones.Text) + ' ' + ExtractFileName(ArchivoFraudes.Text)),
		DescriError);

	DMConnections.BaseCAC.CommitTrans;
end;

procedure TformProcesamientoRespuestas.RegistrarOperacionErronea;
resourcestring
	MSG_ERROR_FOUND = 'Errores encontrados';
	MSG_EVENT_ERROR	= 'El procesamiento de respuestas externas se ha cancelado por errores.';
	STR_FILES = 'Archivos: ';
var
	Error: ANSIString;
begin
	Error := MSG_ERROR_FOUND;

	ActualizarRegistroOperacion(
		DMCOnnections.BaseCAC,
		SYS_RNUT_PARALELO,
		CodigoOperacion,
		SEV_ALTA,
		False,
		MSG_EVENT_ERROR,
		0,
		0,
		STR_FILES + Trim(ExtractFileName(ArchivoInfracciones.Text) + ' ' + ExtractFileName(ArchivoFraudes.Text)),
		Error);
end;

procedure TformProcesamientoRespuestas.btnLimpiarFraudesClick(Sender: TObject);
begin
    ArchivoFraudes.Text := 'Seleccionar Archivo a Procesar...';
end;

end.

