{

Etiqueta    : 20160315 MGO
Descripción : Se utiliza InstallIni en vez de ApplicationIni

}
unit FrmActualizarDatosInfractor;

interface

uses
  SysUtils, Classes, DBListEx, RVMTypes, PeaProcs, DMConnection, ConstParametrosGenerales,
  Util, Dialogs, UtilProc, Windows, DB, ADODB, Variants;

type
  TDMActualizarDatosInfractor = class(TDataModule)
    VerificarInfractoresBD: TADOStoredProc;
    ActualizarConsultaRNVM: TADOStoredProc;
  private
    Pregunta: TRNVMQuery;
    { Private declarations }
  public
    function Actualiazar(Patente: String; CodigoInfractor: Integer; Usuario, Password: String):Boolean;
    function ObtenerDatos(Patente: String; Usuario, Password: String; var RespuestaRNVM: TRVMInformation; var ExistePatente: Boolean): Boolean;
    { Public declarations }
  end;

var
  DMActualizarDatosInfractor: TDMActualizarDatosInfractor;

implementation

const
    FILE_EXTENSION = '.xml';

resourcestring
	MSG_INIT_ERROR     = 'Error al Inicializar';
	MSG_ERROR          = 'Error';
    MSG_ERROR_CONSULTA = 'No se pudo Obtener los datos del RNVM';
    MSG_ERROR_DIR      = 'No existe el directorio %s indicado en Parámetros Generales (RVM_Directorio_Respuestas)';

{$R *.dfm}

{ TDMActualizarDatosInfractor }

function TDMActualizarDatosInfractor.Actualiazar(Patente: String;
  CodigoInfractor: Integer; Usuario, Password: String): Boolean;
var
    ServiceURL: String;
    ServicePort: Integer;
    ServiceID: String;
    Dir: String;
    Respuesta: TRVMInformation;
    NombreViejo, NombreNuevo: String;
    ExistePatente: Boolean;
begin
    Result := False;
    try
        // Controlamos la info que tenenos en la bd es lo suficiente mente vigente como para reutilizarla
        VerificarInfractoresBD.Close;
        VerificarInfractoresBD.Parameters.ParamByName('@CodigoInfractor').Value := CodigoInfractor;
        VerificarInfractoresBD.Parameters.ParamByName('@Existe').Value := Null;
        VerificarInfractoresBD.ExecProc;


        if not VerificarInfractoresBD.Parameters.ParamByName('@Existe').Value then begin

            // Inicializamos las varibles para conectarnos al RNVM
            ObtenerParametroGeneral(DMConnections.BaseCAC,'RVM_SERVICE_URL', ServiceURL);
            ObtenerParametroGeneral(DMConnections.BaseCAC,'RVM_SERVICE_PORT', ServicePort);
            ObtenerParametroGeneral(DMConnections.BaseCAC,'RVM_SERVICE_ID', ServiceID);
            ObtenerParametroGeneral(DMConnections.BaseCAC,'RVM_Directorio_Respuestas', Dir);
            if not DirectoryExists(Dir) then
                Raise Exception.Create(Format(MSG_ERROR_DIR, [Dir]));

            Pregunta.Parameters.ServiceURL := ServiceURL;
            Pregunta.Parameters.ServicePort:= ServicePort;
            Pregunta.Parameters.ServiceID  := ServiceID;
            Pregunta.Parameters.SaveDirectory := Dir;
            Pregunta.Parameters.RequestContentType := 'application/x-www-form-urlencoded';

            Pregunta.Parameters.UserName := Usuario;
            Pregunta.Parameters.PassWord := Password;

            Pregunta.Parameters.FileName := IntToStr(CodigoInfractor) + FILE_EXTENSION;;

            { INICIO : 20160315 MGO
            Pregunta.Parameters.RequestUseProxy := ApplicationIni.ReadBool('RNVM', 'USEPROXY', False);
            if Pregunta.Parameters.RequestUseProxy then begin
                Pregunta.Parameters.RequestProxyServer := Trim(ApplicationIni.ReadString('RNVM', 'PROXYSERVER', ''));
                Pregunta.Parameters.RequestProxyServerPort := ApplicationIni.ReadInteger('RNVM', 'SERVERPORT', 0);
            end;
            }
            Pregunta.Parameters.RequestUseProxy := InstallIni.ReadBool('RNVM', 'USEPROXY', False);
            if Pregunta.Parameters.RequestUseProxy then begin
                Pregunta.Parameters.RequestProxyServer := Trim(InstallIni.ReadString('RNVM', 'PROXYSERVER', ''));
                Pregunta.Parameters.RequestProxyServerPort := InstallIni.ReadInteger('RNVM', 'SERVERPORT', 0);
            end;
            // FIN : 20160315 MGO
            Pregunta.LicensePlate := Patente;

            if ConsultarPatenteRNVM(Pregunta, Respuesta, ExistePatente) then begin
                // Registro la Consulta al RNVM
                ActualizarConsultaRNVM.Close;
                ActualizarConsultaRNVM.Parameters.ParamByName('@CodigoConsultaRNVM').Value := NULL;
                ActualizarConsultaRNVM.Parameters.ParamByName('@Patente').Value := Patente;

                if not ExistePatente then
                    ActualizarConsultaRNVM.Parameters.ParamByName('@Resultado').Value := 'NE'
                else
                    if ((Trim(Respuesta.AddressInformation.Street) = '') and (Trim(Respuesta.AddressInformation.DoorNumber) = '')) or
                       (Trim(Respuesta.AddressInformation.Commune) = '') or
                       (Trim(Respuesta.OwnerInformation.OwnerID1) = '') or
                       (Trim(Respuesta.OwnerInformation.OwnerName1) = '') or
                       (Trim(Respuesta.VehicleInformation.Category) = '') or
                       (Trim(Respuesta.VehicleInformation.Manufacturer) = '')
                   then
                        ActualizarConsultaRNVM.Parameters.ParamByName('@Resultado').Value := 'SD'
                    else
                        ActualizarConsultaRNVM.Parameters.ParamByName('@Resultado').Value := 'OK';

                ActualizarConsultaRNVM.Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;
                ActualizarConsultaRNVM.Parameters.ParamByName('@DescripMarca').Value := Respuesta.VehicleInformation.Manufacturer;
                ActualizarConsultaRNVM.Parameters.ParamByName('@Modelo').Value := Respuesta.VehicleInformation.Model;
                if trim(Respuesta.VehicleInformation.Year) = '' then
                    ActualizarConsultaRNVM.Parameters.ParamByName('@AnioVehiculo').Value := null
                else
                    ActualizarConsultaRNVM.Parameters.ParamByName('@AnioVehiculo').Value := StrToInt(Respuesta.VehicleInformation.Year);

                ActualizarConsultaRNVM.Parameters.ParamByName('@DescripCodigoTipoVehiculo').Value := Respuesta.VehicleInformation.Category;
                ActualizarConsultaRNVM.Parameters.ParamByName('@Color').Value := Respuesta.VehicleInformation.Color;
                ActualizarConsultaRNVM.Parameters.ParamByName('@IdMotor').Value := Respuesta.VehicleInformation.EngineID;
                ActualizarConsultaRNVM.Parameters.ParamByName('@IdChasis').Value := Respuesta.VehicleInformation.ChassisID;
                ActualizarConsultaRNVM.Parameters.ParamByName('@RUT').Value := Respuesta.OwnerInformation.OwnerID1;
                ActualizarConsultaRNVM.Parameters.ParamByName('@Nombre').Value := Respuesta.OwnerInformation.OwnerName1;
                ActualizarConsultaRNVM.Parameters.ParamByName('@DescripCodigoComuna').Value := Respuesta.AddressInformation.Commune;
                ActualizarConsultaRNVM.Parameters.ParamByName('@CalleDesnormalizada').Value := Respuesta.AddressInformation.Street;
                ActualizarConsultaRNVM.Parameters.ParamByName('@Numero').Value := Respuesta.AddressInformation.DoorNumber;
                ActualizarConsultaRNVM.Parameters.ParamByName('@Detalle').Value := Respuesta.AddressInformation.OtherInfo;
                ActualizarConsultaRNVM.ExecProc;

                NombreViejo := GoodDir(Dir) + IntToStr(CodigoInfractor) + FILE_EXTENSION;
                NombreNuevo := GoodDir(Dir) + IntToStr(ActualizarConsultaRNVM.Parameters.ParamByName('@CodigoConsultaRNVM').Value) + '_' + Patente  + FILE_EXTENSION;
                ActualizarConsultaRNVM.close;
                while FileExists(NombreNuevo) do  NombreNuevo := StrLeft(NombreNuevo, Length(NombreNuevo) - Length(FILE_EXTENSION)) + 'x' + FILE_EXTENSION;
                RenameFile(NombreViejo, NombreNuevo);
            end;
        end;
        Result := True;
    except
        on e: Exception do MsgBoxErr(MSG_ERROR_CONSULTA, e.Message, 'ERROR', MB_ICONSTOP);
    end;

end;

function TDMActualizarDatosInfractor.ObtenerDatos(Patente: String;
  Usuario, Password: String; var RespuestaRNVM: TRVMInformation; var ExistePatente: boolean): Boolean;
var
    ServiceURL: String;
    ServicePort: Integer;
    ServiceID: String;
    Dir: String;
    Respuesta: TRVMInformation;
    NombreViejo, NombreNuevo: String;
begin
    Result := False;
    ExistePatente := False;
    try
        // Inicializamos las varibles para conectarnos al RNVM
        ObtenerParametroGeneral(DMConnections.BaseCAC,'RVM_SERVICE_URL', ServiceURL);
        ObtenerParametroGeneral(DMConnections.BaseCAC,'RVM_SERVICE_PORT', ServicePort);
        ObtenerParametroGeneral(DMConnections.BaseCAC,'RVM_SERVICE_ID', ServiceID);
        ObtenerParametroGeneral(DMConnections.BaseCAC,'RVM_Directorio_Respuestas ', Dir);
        if not DirectoryExists(Dir) then
            Raise Exception.Create(Format(MSG_ERROR_DIR, [Dir]));

        Pregunta.Parameters.ServiceURL := ServiceURL;
        Pregunta.Parameters.ServicePort:= ServicePort;
        Pregunta.Parameters.ServiceID  := ServiceID;
        Pregunta.Parameters.SaveDirectory := Dir;
        Pregunta.Parameters.RequestContentType := 'application/x-www-form-urlencoded';

        Pregunta.Parameters.UserName := Usuario;
        Pregunta.Parameters.PassWord := Password;

        Pregunta.Parameters.FileName := Patente + FormatDateTime('ddmmyyhhnn', Now) + FILE_EXTENSION;;
        { INICIO : 20160315  MGO
        Pregunta.Parameters.RequestUseProxy := ApplicationIni.ReadBool('RNVM', 'USEPROXY', False);
        if Pregunta.Parameters.RequestUseProxy then begin
            Pregunta.Parameters.RequestProxyServer := Trim(ApplicationIni.ReadString('RNVM', 'PROXYSERVER', ''));
            Pregunta.Parameters.RequestProxyServerPort := ApplicationIni.ReadInteger('RNVM', 'SERVERPORT', 0);
        end;
        }
        Pregunta.Parameters.RequestUseProxy := InstallIni.ReadBool('RNVM', 'USEPROXY', False);
        if Pregunta.Parameters.RequestUseProxy then begin
            Pregunta.Parameters.RequestProxyServer := Trim(InstallIni.ReadString('RNVM', 'PROXYSERVER', ''));
            Pregunta.Parameters.RequestProxyServerPort := InstallIni.ReadInteger('RNVM', 'SERVERPORT', 0);
        end;
        // FIN : 20160315 MGO

        Pregunta.LicensePlate := Patente;

        if ConsultarPatenteRNVM(Pregunta, Respuesta, ExistePatente) then begin

            // Registro la Cinsulta al RNVM
            ActualizarConsultaRNVM.Close;

            ActualizarConsultaRNVM.Parameters.ParamByName('@CodigoConsultaRNVM').Value := NULL;
            ActualizarConsultaRNVM.Parameters.ParamByName('@Patente').Value := Patente;

            if not ExistePatente then
                ActualizarConsultaRNVM.Parameters.ParamByName('@Resultado').Value := 'NE'
            else
                if ((Trim(Respuesta.AddressInformation.Street) = '') and (Trim(Respuesta.AddressInformation.DoorNumber) = '')) or
                   (Trim(Respuesta.AddressInformation.Commune) = '') or
                   (Trim(Respuesta.OwnerInformation.OwnerID1) = '') or
                   (Trim(Respuesta.OwnerInformation.OwnerName1) = '') or
                   (Trim(Respuesta.VehicleInformation.Category) = '') or
                   (Trim(Respuesta.VehicleInformation.Manufacturer) = '')
                then
                    ActualizarConsultaRNVM.Parameters.ParamByName('@Resultado').Value := 'SD'
                else
                    ActualizarConsultaRNVM.Parameters.ParamByName('@Resultado').Value := 'OK';

            ActualizarConsultaRNVM.Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;
            ActualizarConsultaRNVM.Parameters.ParamByName('@DescripMarca').Value := Respuesta.VehicleInformation.Manufacturer;
            ActualizarConsultaRNVM.Parameters.ParamByName('@Modelo').Value := Respuesta.VehicleInformation.Model;

            if trim(Respuesta.VehicleInformation.Year) = '' then
                ActualizarConsultaRNVM.Parameters.ParamByName('@AnioVehiculo').Value := null
            else
                ActualizarConsultaRNVM.Parameters.ParamByName('@AnioVehiculo').Value := StrToInt(Respuesta.VehicleInformation.Year);

            ActualizarConsultaRNVM.Parameters.ParamByName('@DescripCodigoTipoVehiculo').Value := Respuesta.VehicleInformation.Category;
            ActualizarConsultaRNVM.Parameters.ParamByName('@Color').Value := Respuesta.VehicleInformation.Color;
            ActualizarConsultaRNVM.Parameters.ParamByName('@IdMotor').Value := Respuesta.VehicleInformation.EngineID;
            ActualizarConsultaRNVM.Parameters.ParamByName('@IdChasis').Value := Respuesta.VehicleInformation.ChassisID;
            ActualizarConsultaRNVM.Parameters.ParamByName('@RUT').Value := Respuesta.OwnerInformation.OwnerID1;
            ActualizarConsultaRNVM.Parameters.ParamByName('@Nombre').Value := Respuesta.OwnerInformation.OwnerName1;
            ActualizarConsultaRNVM.Parameters.ParamByName('@DescripCodigoComuna').Value := Respuesta.AddressInformation.Commune;
            ActualizarConsultaRNVM.Parameters.ParamByName('@CalleDesnormalizada').Value := Respuesta.AddressInformation.Street;
            ActualizarConsultaRNVM.Parameters.ParamByName('@Numero').Value := Respuesta.AddressInformation.DoorNumber;
            ActualizarConsultaRNVM.Parameters.ParamByName('@Detalle').Value := Respuesta.AddressInformation.OtherInfo;
            ActualizarConsultaRNVM.ExecProc;

            NombreViejo := GoodDir(Dir) + Patente  + FormatDateTime('ddmmyyhhnn', Nowbase(DMConnections.BaseCAC) ) + FILE_EXTENSION;
            NombreNuevo := GoodDir(Dir) + IntToStr(ActualizarConsultaRNVM.Parameters.ParamByName('@CodigoConsultaRNVM').Value) + '_' + Patente + FILE_EXTENSION;
            while FileExists(NombreNuevo) do NombreNuevo := StrLeft(NombreNuevo, Length(NombreNuevo) - Length(FILE_EXTENSION)) + 'x' + FILE_EXTENSION;

            ActualizarConsultaRNVM.close;
            RenameFile(NombreViejo, NombreNuevo);

            RespuestaRNVM := Respuesta;
            Result := True;
        end;
    except
        on e: Exception do MsgBoxErr(MSG_ERROR_CONSULTA, e.Message, 'ERROR', MB_ICONSTOP);
    end;

end;

end.




