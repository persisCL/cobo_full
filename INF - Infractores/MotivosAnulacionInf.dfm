object frmMotivosAnulacionInf: TfrmMotivosAnulacionInf
  Left = 328
  Top = 189
  Caption = 'Motivos de devoluci'#243'n de cartas'
  ClientHeight = 602
  ClientWidth = 825
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 9
    Top = 20
    Width = 43
    Height = 13
    Caption = 'N'#250'mero :'
  end
  object Label3: TLabel
    Left = 9
    Top = 49
    Width = 36
    Height = 13
    Caption = 'Fecha :'
  end
  object GroupB: TPanel
    Left = 0
    Top = 496
    Width = 825
    Height = 67
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 0
    ExplicitWidth = 801
    object Label10: TLabel
      Left = 11
      Top = 39
      Width = 45
      Height = 13
      Caption = '&Detalle:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbl1: TLabel
      Left = 11
      Top = 12
      Width = 60
      Height = 13
      Caption = 'ID Motivo:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object txtDetalle: TEdit
      Left = 88
      Top = 36
      Width = 529
      Height = 21
      MaxLength = 100
      TabOrder = 1
    end
    object edtCodigoMotivoAnulacionInfraccion: TEdit
      Left = 88
      Top = 9
      Width = 73
      Height = 21
      Color = clInactiveCaption
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clInactiveCaptionText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 563
    Width = 825
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitTop = 528
    ExplicitWidth = 801
    object Notebook: TNotebook
      Left = 628
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      ExplicitLeft = 604
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TButton
          Left = 110
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object Panel1: TPanel
    Left = 176
    Top = 2
    Width = 104
    Height = 27
    BevelOuter = bvNone
    TabOrder = 2
  end
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 825
    Height = 33
    Habilitados = [btAlta, btBaja, btModi, btSalir]
    OnClose = AbmToolbar1Close
    ExplicitWidth = 801
  end
  object ListaMotivos: TAbmList
    Left = 0
    Top = 33
    Width = 825
    Height = 463
    TabStop = True
    TabOrder = 4
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -12
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'78'#0'ID de Motivo   '
      
        #0'293'#0'Detalle                                                    ' +
        '                                '
      #0'90'#0'Usuario Creador  '
      #0'110'#0'Fecha Creaci'#243'n         '
      #0'108'#0'Usuario Modificador  '
      #0'107'#0'Fecha Modificaci'#243'n  '
      #0'14'#0'  ')
    HScrollBar = True
    RefreshTime = 100
    Table = tblMotivosAnulacionInfraccion
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = ListaMotivosClick
    OnProcess = ListaMotivosProcess
    OnDrawItem = ListaMotivosDrawItem
    OnRefresh = ListaMotivosRefresh
    OnInsert = ListaMotivosInsert
    OnDelete = ListaMotivosDelete
    OnEdit = ListaMotivosEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
    ExplicitWidth = 801
    ExplicitHeight = 451
  end
  object tblMotivosAnulacionInfraccion: TADOTable
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    TableName = 'MotivosAnulacionInfraccion'
    Left = 49
    Top = 65
  end
  object spActualizarMotivosAnulacionInfraccion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarMotivosAnulacionInfraccion'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Accion'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Codigo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Detalle'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end>
    Left = 84
    Top = 64
  end
end
