unit ConsultarRNVM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, ComCtrls, ToolWin, ImgList, StdCtrls, ExtCtrls,
  Validate, DateEdit, VariantComboBox, DMConnection, DB, ADODB, UtilProc, Util;

type
  TfrmConsultarRNVM = class(TForm)
    pnlSuperior: TPanel;
    lbl: TLabel;
    edtPatente: TEdit;
    btnBuscar: TButton;
    imgImagenes: TImageList;
    tlbrBotonera: TToolBar;
    btnSalir: TToolButton;
    dbgrdConsultas: TDBGrid;
    pnlBotones: TPanel;
    btnSalir1: TButton;
    lbl1: TLabel;
    vcbEstadoConsulta: TVariantComboBox;
    lbl2: TLabel;
    edtDesde: TDateEdit;
    lbl3: TLabel;
    edtHasta: TDateEdit;
    spConsultaRNVM_SELECT_Corto: TADOStoredProc;
    dsConsultasRNVM: TDataSource;
    procedure btnSalir1Click(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnBuscarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function Inicializar: Boolean;
  end;

var
  frmConsultarRNVM: TfrmConsultarRNVM;

implementation

{$R *.dfm}

function TfrmConsultarRNVM.Inicializar: Boolean;
begin
    Result := True;
    try
        vcbEstadoConsulta.Clear;
        vcbEstadoConsulta.Items.Add('Todos','');
        vcbEstadoConsulta.Items.Add('OK','OK');
        vcbEstadoConsulta.Items.Add('Pendiente','MA');
        vcbEstadoConsulta.Items.Add('Enviada','TO');
        vcbEstadoConsulta.Items.Add('No Existe','NE');
        vcbEstadoConsulta.ItemIndex := 0;
    except
        Result := False;
    end;
end;

procedure TfrmConsultarRNVM.btnBuscarClick(Sender: TObject);
resourcestring
    MSG_ERROR_DATOS = 'Ha ocurrido un error al obtener los datos';
begin
    Screen.Cursor := crHourGlass;
    try
        try
            spConsultaRNVM_SELECT_Corto.Close;
            spConsultaRNVM_SELECT_Corto.Parameters.Refresh;            
            spConsultaRNVM_SELECT_Corto.Parameters.ParamByName('@Patente').Value := edtPatente.Text;
            if vcbEstadoConsulta.Value <> '' then
                spConsultaRNVM_SELECT_Corto.Parameters.ParamByName('@Resultado').Value := vcbEstadoConsulta.Value;
            if edtDesde.Date <> NullDate then
                spConsultaRNVM_SELECT_Corto.Parameters.ParamByName('@FechaDesde').Value := edtDesde.Date;   
            if edtHasta.Date <> NullDate then
                spConsultaRNVM_SELECT_Corto.Parameters.ParamByName('@FechaHasta').Value := edtHasta.Date;
            spConsultaRNVM_SELECT_Corto.Open;

            if spConsultaRNVM_SELECT_Corto.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
                raise Exception.Create(spConsultaRNVM_SELECT_Corto.Parameters.ParamByName('@ErrorDescription').Value);
        except
            on e: Exception do
                MsgBoxErr(MSG_ERROR_DATOS, e.Message, 'Error', MB_ICONERROR);
        end;
    finally
        Screen.Cursor := crDefault;
    end;
end;

procedure TfrmConsultarRNVM.btnSalir1Click(Sender: TObject);
begin
    Close;
end;

procedure TfrmConsultarRNVM.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TfrmConsultarRNVM.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

end.
