object FormListadoInfracciones: TFormListadoInfracciones
  Left = 66
  Top = 137
  Caption = 'Listado de Infracciones'
  ClientHeight = 502
  ClientWidth = 876
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 876
    Height = 79
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 876
      Height = 79
      Align = alClient
      Caption = 'Filtros'
      TabOrder = 0
      DesignSize = (
        876
        79)
      object Label8: TLabel
        Left = 40
        Top = 52
        Width = 40
        Height = 13
        Caption = '&Patente:'
        FocusControl = txt_Patente
      end
      object Label2: TLabel
        Left = 177
        Top = 51
        Width = 121
        Height = 13
        Caption = 'Fecha de Pasaje:  &desde:'
        FocusControl = txt_FechaDesde
      end
      object Label3: TLabel
        Left = 426
        Top = 52
        Width = 29
        Height = 13
        Caption = '&hasta:'
        FocusControl = txt_FechaHasta
      end
      object Label4: TLabel
        Left = 591
        Top = 52
        Width = 85
        Height = 13
        Caption = '&Infracciones <= a:'
        FocusControl = txt_CantInfracciones
      end
      object lblConcesionaria: TLabel
        Left = 11
        Top = 24
        Width = 70
        Height = 13
        Caption = '&Concesionaria:'
        FocusControl = txt_Patente
      end
      object btn_Filtrar: TButton
        Left = 708
        Top = 15
        Width = 75
        Height = 25
        Hint = 'Filtrar Solicitud'
        Anchors = [akTop, akRight]
        Caption = '&Filtrar'
        Default = True
        TabOrder = 5
        OnClick = btn_FiltrarClick
      end
      object btn_Limpiar: TButton
        Left = 788
        Top = 15
        Width = 75
        Height = 25
        Hint = 'Filtrar Solicitud'
        Anchors = [akTop, akRight]
        Caption = '&Limpiar'
        TabOrder = 6
        OnClick = btn_LimpiarClick
      end
      object txt_Patente: TEdit
        Left = 85
        Top = 48
        Width = 65
        Height = 21
        CharCase = ecUpperCase
        MaxLength = 10
        TabOrder = 1
      end
      object txt_FechaDesde: TDateEdit
        Left = 304
        Top = 48
        Width = 114
        Height = 21
        AutoSelect = False
        TabOrder = 2
        OnExit = txt_FechaDesdeExit
        Date = -693594.000000000000000000
      end
      object txt_FechaHasta: TDateEdit
        Left = 460
        Top = 48
        Width = 114
        Height = 21
        AutoSelect = False
        TabOrder = 3
        Date = -693594.000000000000000000
      end
      object txt_CantInfracciones: TNumericEdit
        Left = 681
        Top = 48
        Width = 33
        Height = 21
        MaxLength = 3
        TabOrder = 4
        Value = 100.000000000000000000
      end
      object cbConcesionarias: TVariantComboBox
        Left = 85
        Top = 21
        Width = 190
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 13
        TabOrder = 0
        OnChange = cbConcesionariasChange
        Items = <>
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 79
    Width = 876
    Height = 382
    Align = alClient
    BevelOuter = bvNone
    Caption = 'Panel2'
    TabOrder = 1
    object dblInfracciones: TDBListEx
      Left = 0
      Top = 0
      Width = 876
      Height = 382
      Align = alClient
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 120
          Header.Caption = 'Patente'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          OnHeaderClick = dblInfraccionesColumnHeaderClick
          FieldName = 'Patente'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 100
          Header.Caption = 'Concesionaria'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'NombreConcesionaria'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 200
          Header.Caption = 'Fecha de Pasaje'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Sorting = csAscending
          IsLink = False
          OnHeaderClick = dblInfraccionesColumnHeaderClick
          FieldName = 'FechaHora'
        end
        item
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 150
          Header.Caption = 'Cantidad Tr'#225'nsitos'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          OnHeaderClick = dblInfraccionesColumnHeaderClick
          FieldName = 'CantidadTransitos'
        end>
      DataSource = dsObtenerInfracciones
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
      OnDblClick = btnValidarInfraccionClick
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 461
    Width = 876
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object Panel4: TPanel
      Left = 0
      Top = 0
      Width = 876
      Height = 41
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      DesignSize = (
        876
        41)
      object lbl_Mostar: TLabel
        Left = 8
        Top = 16
        Width = 308
        Height = 13
        Anchors = [akLeft, akTop, akRight]
        AutoSize = False
        Caption = 'lbl_Mostar'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object BtnSalir: TButton
        Left = 788
        Top = 7
        Width = 75
        Height = 26
        Hint = 'Salir del visualizador de Solicitudes'
        Anchors = [akRight, akBottom]
        Cancel = True
        Caption = '&Salir'
        TabOrder = 0
        OnClick = BtnSalirClick
      end
      object btnValidarInfraccion: TButton
        Left = 652
        Top = 7
        Width = 130
        Height = 26
        Anchors = [akRight, akBottom]
        Caption = '&Validar Infracci'#243'n'
        TabOrder = 1
        OnClick = btnValidarInfraccionClick
      end
    end
  end
  object spObtenerInfracciones: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    AfterOpen = spObtenerInfraccionesAfterOpen
    AfterClose = spObtenerInfraccionesAfterOpen
    CommandTimeout = 120
    ProcedureName = 'ObtenerInfracciones'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@FechaDesde'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaHasta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@CantidadInfracciones'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConcesionaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 304
    Top = 216
  end
  object dsObtenerInfracciones: TDataSource
    DataSet = spObtenerInfracciones
    Left = 440
    Top = 208
  end
  object spObtenerFechaMinimaTransitoPosibleInfractorConcesionaria: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerFechaMinimaTransitoPosibleInfractorConcesionaria'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConcesionaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@FechaHora'
        Attributes = [paNullable]
        DataType = ftDateTime
        Direction = pdInputOutput
        Value = Null
      end>
    Left = 360
    Top = 272
  end
end
