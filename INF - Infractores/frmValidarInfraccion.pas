{******************************** Function Header ******************************
Function Name: btn_SalirClick
Author :
Date Created :
Description :
Parameters : Sender: TObject
Return Value : None

Revision 1:
Author:	ggomez
Date:	01/11/2005
Description:
    - Modifiqu� para mostrar si el tr�nsito tiene o no tiene Telev�a. Para esto
    modifiqu� los SP ObtenerTransitoInfractoresPatente.

Revision 2:
Author:	jconcheyro
Date:	11/06/2007
Description: Se cambia el m�ximo de categoria a 4 motos

Revision 3:
    Author : nefernandez
    Date : 30/01/2009
    Description : Se quita la funcionalidad de Consulta al RNVM (chk_ConsultarRNVM)
    
Revision 5:
Author: mbecerra
Date: 01-Septiembre-2009
Description:	(Ref SS 822)
        	Se solicita agregar la siguiente validaci�n en la patente ingresada:
            1.-		Que el largo no sea inferior a 5 caracteres
            2.-		Que si el largo es 5 caracteres, la patente comience por 'PR'

Revision 6:
    Author : pdominguez
    Date   : 15/09/2009
    Description : SS 826
        - Se modificaron/a�adieron los siguientes objetos/constantes/variables:
        - Se modificaron/a�adieron los siguientes procedimientos/funciones:
            btn_AceptarVehiculoClick

Revision 7:
    Author : pdominguez
    Date   : 17/05/2010
    Description : Infractores Fase 2
        - Se a�aden/modifican los siguientes objetos/variables/constantes:
            Se a�ade el campo CodigoConcesionaria en el objeto cdsObtenerTransitoInfractoresPatente: TClientDataSet.
            Se a�ade el Par�metro @CodigoConcesionaria en el objeto ObtenerTransitoInfractoresPatente: TADOStoredProc.
            Se a�aden las variables FProcesandoConcesionaria, FProcesandoenBatch
            se a�ade el objeto spObtenerInfracciones: TADOStoredProc
            se modifica el objeto ReservarTransitosInfractores: TADOStoredProc.
            Se a�ade el par�metro @CodigoConcesionaria al objeto ActualizarInfractores: TADOStoredProc.

        - Se modificaron/a�adieron los siguientes procedimientos/funciones:
            Inicializar,
            CargarInfractor,
            btn_CancelarClick,
            btn_AceptarVehiculoClick

Revision 7:
    Author : Javier De Barbieri
    Date   : 03/11/2010
    Description : SS 931
    - Se modifica el procedimiento actAceptarVehiculoClick para que
        verifique si la patente Validada o la propuesta se encuentra en el
        listado de Patentes en Seguimiento. 

Revision 8:
    Author      : Nelson Droguett Sierra
    Date        : 05-Mayo-2011
    Description : SS 231
                  Al validar una infracci�n al momento de grabar, si el estado posterior del transito es <> LIMBO, grabar la misma imagen
                  que se eligio para el JPL en una carpeta para imagenes de infractores.
                  El directorio se indicar� a trav�s de Par�metros generales.
                  Las im�genes se almacenar�n no en archivo padre, sino en una carpeta con la fecha y nombre de imagen modulo 1000 numcorrca
                  y el sufijo del tipo de imagen. Las vistas a grabar seran definidas por parametros generales.
                  Se cambian las instrucciones de transacciones Delphi por instrucciones explicitas de SQL
    Firma       : SS-231-NDR-20110505

Revision    : 8
Author      : Nelson Droguett Sierra
Date        : 07-Junio-2011
Description : (Ref.Fase2)SS-377
              Ya no se trabajara mas con imagenes padre, solo imagenes individuales
              Se agrega variable FImagePathNFI
              Function Inicializar
                Se obtiene el nuevo parametro general.

              Function cdsObtenerTransitoInfractoresPatenteAfterScroll
                Se asigna la variable NombreCorto
                En todas las llamadas a ObtenerImagenTransito se envia la ruta nueva y el nombreCorto
Firma       : SS-377-NDR-20110607

Author      : Claudio Quezada Ib��ez
Date        : 16-Octubre-2012
Firma       : SS_660_CQU_20121010
Description : Se modifica el procedimiento Inicializar, agreg�ndole dos nuevas variables que permitiran saber el tipo de infracci�n
              y el c�digo de cliente.

Author      : Claudio Quezada Ib��ez
Date        : 16-Mayo-2013
Firma       : SS_1091_CQU_20130516
Description : Se modifica la visualizaci�n de im�genes para poder desplegar im�genes de p�rticos Italianos.

Firma		: 	SS_1111_MBE_20130613
Description	:   Se fuerza a que se env�e la categor�a del tr�nsito,
				cuando se confirma la infracci�n, si la categor�a_nueva es '' (Blanco)
                
Firma       :   SS_660_CQU_20130822
Desription  :   Se agrega Label que indica que la infracci�n es por morosidad

Firma       :   SS_1138_CQU_20131009
Descripcion :   Se quita la m�scara al cargar las im�genes.
                Se redimensionan las im�genes italianas
                Cuando se elige la imagen a JPL, se pirde la que se eligi�,
                se modific� para que no ocurra.
                Se agregan las funcionalidades de para los botones F1, F2, F3 y F4
                DURANTE LA NORMALIZACION DE PORTICO ITALIANO,
                SE RENOMBRARAN LAS IMAGENES VDC1 por VRF1 y VDC2 POR VRR2
                ESTO ES PORQUE LOS ITALIANOS ENTREGAN MAL LAS IMAGENES.
                Se agrega par�metro en el ini para indicar si se hace el renombre de archivo.

Firma       :   SS_1135_CQU_20131030
Descripcion :   Se debe grabar la imagen en el Directorio de Infractores, pero se debe COPIAR el archivo existente
                y no del objeto TImage ya que se pierden los datos de Kapsch dentro de la imagen.

Firma       :   SS_1138B_CQU_20140122
Descripcion :   A ra�z de la SS_1138B se detectaron algunos problemas en la SS 1135 que fueron corregidos.
                Las im�genes infractoras deben ser tomadas (en esta aplicaci�n) siempre desde la carpeta de tr�nsitos.
                Al grabar la imagen se debe validar contra el ParametroGeneral solamente
				ya que esta validaci�n "and Not(ImagenAGrabarGeneral2.Picture.Graphic=nil)" no funciona al pasar
				al pr�ximo transito de la infracci�n.

Firma       :   SS_1222_MBE_20141024
Description :   No permitir el cambio de categor�a si el tr�nsito NO es de CN

Author      :   Claudio Quezada Ib��ez
Date        :   17-Diciembre-2014
Firma		:   SS_1147X_CQU_20141217
Descripcion	:   Se agregan los bit  FEsKapschImagenActual, FEsKapschFrontal, FEsKapschFrontal2,
                FEsKapschPosterior, FEsKapschPosterior2 para indicar si la imagen es TJPEGPlusImage
                con ello se pretende mantener el comportamiento independiente de la imagen.
                Se hizo as� ya que el valor "EsItaliano" �no sirve en el caso de VS porque ellos
                usan otro proveedor de p�rticos el cual tambi�n trabaja con im�genes normales.

Firma       :   SS_1147_MCA_20150325
Descripcion :   se corrige error del mensaje, faltaba parametro concesionaria
Firma       :   SS_1222_CQU_20150317
Description :   Se realiza cambio para que no se habilite el cuadro de texto de las categor�as
                cuando la concesionaria sea distinta de CN.
                La obtenci�n del C�digoCN debe ser cambiado luego a Concesionaria Nativa

Firma       :   SS_1222_NDR_20150406
Description :   Se elimina la referencia a ConcesionariaNativa

Firma       :   SS_1222_MCA_20150406
Descripcion :   Se comenta que desactive la imagen JPL independiente si la imagen es KTC o Italiana


Firma		:	SS_1147ZZZ_NDR_20150415
Descripcion	:	Se reemplaza la funcion CONST_CODIGO_CONCESIONARIA_COSTANERA_NORTE() por dbo.ObtenerConcesionariaNativa()
              Se cambia todo lo relativo a CN por Nativa

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

Firma       :   SS_1374_CQU_20150901
Descripcion :   Se debe validar que la patente ingresada por el validador NO sea diferente a la de Validacion Manual

Firma       :   SS_1374A_CQU_20150911
Descripcion :   Se corrige que cuando se ingresa una patente y s�lo exista un transito, vuelva a la patente incial.
                Se corrige que cuando se ingresa una categr�a y s�lo exista un transito, vuelva a la categor�a incial.
                Se modifica el mensaje MSG_PATENTE_DIFERENTE, ahora se entiende que si presiona "Si" deber� ingresar la patente nuevamente.
                Se modifica para que cuando presione "No" (en el mensaje MSG_PATENTE_DIFERENTE) marque "Sin Imagen"
                    y se habilite el boton btn_AceptarVehiculo.
                Se modifican los botones btnNoLegible, btnPatenteExtrajera y btnPatenteEspecial para que al presionarlos se marque "Sin Imagen"
                    y se habilite el boton btn_AceptarVehiculo.

Firma       :   SS_1374A_CQU_20150924
Descripcion :   Se corrige la funcionalidad anterior.
                Se agregan
                    FReIngresarPatenteConfirmado    : Boolean; Indicar� si el operador confirm� el cambio de patente
                    function ReIngresarPatente      : Boolean; Verificar� el cambio de patente del operador
                    function PuedeConfirmar         : Boolean; Verificar� si la infracci�n se puede confirmar
                En el m�todo btn_SiguienteClick se agerga una validaci�n ya que no es necesario recargar todos los datos
                e imagenes de la infracci�n cuando s�lo existe una!
                En el m�todo btn_AceptarPatenteClick se quitan las validaciones y se hacen en PuedeConfirmar
                Se implementa AjustarTama�oTImagePlus(ImagenAmpliar) para redimensionar la imagen al igual que Validacion Manual
                Se agrega el metodo MarcarSinImagen para eliminar la funcionalidad del bot�n.

Firma       :   SS_1374A_CQU_20151002
Descripcion :   Se agerga AjustarTama�oTImagePlus en las acciones correspondientes a F1, F2, etc
                Se modifica PuedeConfirmar, si se cambio patente y no se seleccion� imagen JPL, no aparecer� el mensaje recordandolo.
                Se agrega la funcionalidad de PosiblePatente
                    - gbPatentesPosibles
                    - dblPatentesPosibles
                    - ds_Patentes
                    - spObtenerPatentesPosibles
                    - dblPatentesPosiblesDblClick
                    - PatenteSinComodin(Patente: AnsiString): Boolean;
                Se agrega el NumCorrCA en el caption del gbDatosValidar.
                Se agrega que se realice la Validaci�n SemiAutom�tica cuando est� el chkValidacionSemiAutomatica activo.
                Se agregan los campos NombreCorto, EstadoTransito y EnSeguimiento para optimizar la grabaci�n
                ya que se evalua con ellos al momento de grabar y as� no se consultar� m�s a la BD por cada registro a validar.

Firma       :   SS_1374A_CQU_20151013
Descripcion :   Se corrige la validacion de patentes en ReIngresarPatente
                Se corrige la validacion de SinImagen para que aparezca SOLAMENTE cuando NO SE CAMBIE patente
                Se corrgie que FCodigoPersona est� en 0 o -1, ahora se recupera desde el sp ObtenerTransitoInfractoresPatente
				Se agrega que la �ltima patente validada se muestr en la pr�xima validaci�n

Firma       :   SS_1374A_CQU_20151019
Descripcion :   Se modifica el mensaje a desplegar cuando no selecciona imagen JPL, ya no seleccionar� auto�ticamente.

Firma       :   SS_1374A_CQU_20151020
Descripcion :   Se corrige una parte de SS_1374A_CQU_20151002 ya que es necesario obtener el EstadoTransito luego de haber actualizado la infracci�n.
*******************************************************************************}
unit frmValidarInfraccion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Grids, DBGrids, ComCtrls, DMConnection, DB, ADODB,
  UtilProc, RStrings, ImgList, OPImageOverview, DPSControls, DBCtrls, ImgProcs,
  ImagePlus, ToolWin, Buttons, ActnList, ActnMan, ConstParametrosGenerales, ImgTypes,
  JPEGPlus, Filtros, Math, formImagenOverview, UtilDB, Util, PeaProcs, RVMLogin,
  Provider, DBClient, ListBoxEx, DBListEx, DmiCtrls, ActualizarDatosInfractor,
  XPStyleActnCtrls, RVMTypes, JPeg, SysUtilsCN, PeaProcsCN; // SS_1135_CQU_20131030 // SS_660_CQU_20121010

type
  TformValidarInfraccion = class(TForm)
    gbDatosValidar: TGroupBox;
    gbTransitos: TGroupBox;
    btn_AceptarPatente: TButton;
    ObtenerTransitoInfractoresPatente: TADOStoredProc;
    DSObtenerTransitoInfractoresPatente: TDataSource;
    ImageList1: TImageList;
    ActionManager: TActionManager;
    actZoomIn: TAction;
    actZoomOut: TAction;
    actZoomReset: TAction;
    actZoomPlate: TAction;
    actBrightPlus: TAction;
    actBrigthLess: TAction;
    actBrigthReset: TAction;
    actCenterToPlate: TAction;
    actResetImage: TAction;
    actRemarcarBordes: TAction;
    actSuavizarBordes: TAction;
    pnlImgPatente: TPanel;
    imgPatente: TImage;
    Panel1: TPanel;
    btn_Anterior: TButton;
    btn_Siguiente: TButton;
    actImageOverview: TAction;
    btnNoLegible: TButton;
    gbImagenAmpliar: TGroupBox;
    ImagenAmpliar: TImagePlus;
    CoolBar2: TCoolBar;
    ToolBar2: TToolBar;
    ToolButton6: TToolButton;
    ToolButton8: TToolButton;
    ToolButton13: TToolButton;
    ToolButton14: TToolButton;
    ToolButton15: TToolButton;
    ToolButton16: TToolButton;
    ToolButton17: TToolButton;
    ToolButton18: TToolButton;
    ToolButton19: TToolButton;
    ToolButton20: TToolButton;
    ToolButton1: TToolButton;
    gbImagenes: TGroupBox;
    pnlThumb: TPanel;
    sbThumb0: TSpeedButton;
    sbThumb1: TSpeedButton;
    sbThumb2: TSpeedButton;
    sbThumb5: TSpeedButton;
    sbThumb4: TSpeedButton;
    sbThumb3: TSpeedButton;
    sbThumb6: TSpeedButton;
    dbTransitos: TDBListEx;
    cdsObtenerTransitoInfractoresPatente: TClientDataSet;
    ActualizarInfractores: TADOStoredProc;
    pnl_Botones: TPanel;
    pnlBotones: TPanel;
    btn_AceptarVehiculo: TButton;
    btn_Cancelar: TButton;
    btn_Salir: TButton;
    btnImgRef: TButton;
    btnSinImgRef: TButton;
    lblSetImagen: TLabel;
    btnPatenteExtranjera: TButton;
    btnPatenteEspecial: TButton;
    ReservarTransitosInfractores: TADOStoredProc;
    LiberarTransitosInfractores: TADOStoredProc;
    chk_VerMascara: TCheckBox;
    Label3: TLabel;
    Label2: TLabel;
    Label1: TLabel;
    Label15: TLabel;
    txt_Patente: TEdit;
    lblPatentePropuesta: TLabel;
    lblCategoria: TLabel;
    txt_Categoria: TNumericEdit;
    chk_ConsultarRNVM: TCheckBox;
    txtNoHayImagen: TEdit;
    chkValidacionSemiAutomatica: TCheckBox;
    spObtenerInfracciones: TADOStoredProc;
    txtMoroso: TEdit;                                       // SS_660_CQU_20121010
    lblListaAmarilla: TLabel;                               // SS_660_CQU_20130822
    actImagenFrontal1: TAction;                             //  SS_1138_CQU_20131009
    actImagenFrontal2: TAction;                             //  SS_1138_CQU_20131009
    actImagenPosterior1: TAction;                           //  SS_1138_CQU_20131009
    actImagenPosterior2: TAction;                           //  SS_1138_CQU_20131009
    gbPatentesPosibles: TGroupBox;                          // SS_1374A_CQU_20151002
    dblPatentesPosibles: TDBListEx;                         // SS_1374A_CQU_20151002
    ds_Patentes: TDataSource;                               // SS_1374A_CQU_20151002
    spObtenerPatentesPosibles: TADOStoredProc;              // SS_1374A_CQU_20151002
    grpUltimaValidada: TGroupBox;							// SS_1374A_CQU_20151013
    lblUltimaValidada: TLabel;								// SS_1374A_CQU_20151013
    procedure btn_SalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure sbThumb0Click(Sender: TObject);
    procedure sbThumb1Click(Sender: TObject);
    procedure sbThumb2Click(Sender: TObject);
    procedure sbThumb3Click(Sender: TObject);
    procedure sbThumb4Click(Sender: TObject);
    procedure sbThumb5Click(Sender: TObject);
    procedure sbThumb6Click(Sender: TObject);
    procedure actZoomResetExecute(Sender: TObject);
    procedure actZoomInExecute(Sender: TObject);
    procedure actZoomOutExecute(Sender: TObject);
    procedure actZoomPlateExecute(Sender: TObject);
    procedure actBrightPlusExecute(Sender: TObject);
    procedure actBrigthResetExecute(Sender: TObject);
    procedure actCenterToPlateExecute(Sender: TObject);
    procedure actResetImageExecute(Sender: TObject);
    procedure actRemarcarBordesExecute(Sender: TObject);
    procedure actSuavizarBordesExecute(Sender: TObject);
    procedure Redibujar(VerMascara: Boolean);
    procedure actBrigthLessExecute(Sender: TObject);
    procedure actMaskExecute(Sender: TObject);
    procedure btn_AceptarVehiculoClick(Sender: TObject);
    procedure btn_CancelarClick(Sender: TObject);
    procedure btn_SiguienteClick(Sender: TObject);
    procedure btn_AnteriorClick(Sender: TObject);
    procedure btn_AceptarPatenteClick(Sender: TObject);
    procedure actImageOverviewExecute(Sender: TObject);
    procedure dbTransitosDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: String; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure cdsObtenerTransitoInfractoresPatenteAfterScroll(
      DataSet: TDataSet);
    procedure btnNoLegibleClick(Sender: TObject);
    procedure cdsObtenerTransitoInfractoresPatenteAfterPost(
      DataSet: TDataSet);
    procedure chk_VerMascaraClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btnImgRefClick(Sender: TObject);
    procedure btnSinImgRefClick(Sender: TObject);
    procedure btnPatenteExtranjeraClick(Sender: TObject);
    procedure btnPatenteEspecialClick(Sender: TObject);
    procedure cdsObtenerTransitoInfractoresPatenteAfterEdit(
      DataSet: TDataSet);
    procedure FormDestroy(Sender: TObject);
    procedure actImagenFrontal1Execute(Sender: TObject);	// SS_1138_CQU_20131009
    procedure actImagenFrontal2Execute(Sender: TObject);	// SS_1138_CQU_20131009
    procedure actImagenPosterior1Execute(Sender: TObject);	// SS_1138_CQU_20131009
    procedure actImagenPosterior2Execute(Sender: TObject);	// SS_1138_CQU_20131009
    procedure txt_PatenteChange(Sender: TObject);           // SS_1374A_CQU_20150924
    procedure dblPatentesPosiblesDblClick(Sender: TObject); // SS_1374A_CQU_20151002
  private
    { Private declarations }
    FValidacionSemiAutomatica: Boolean;
    //FValidarxFecha: Boolean; // Rev. 7 (Fase 2)
    FImagenRef: String;
    FRegistrationAccessibilityRef : Integer;                // SS_1138_CQU_20131009
    Act: TDMActualizarDatosInfractor; 
    FImagePath, FImagePathNFI: AnsiString;                                      //SS-377-NDR-20110607

    FImagePathInfractores: AnsiString;    //SS-231-NDR-20110505
    FImagePathNoLegible: AnsiString;      //SS-231-NDR-20110505
    FAlmacenaImagenFrontal:Integer;       //SS-231-NDR-20110505
    FAlmacenaImagenPosterior:Integer;     //SS-231-NDR-20110505
    FAlmacenaImagenGeneral1:Integer;      //SS-231-NDR-20110505
    FAlmacenaImagenGeneral2:Integer;      //SS-231-NDR-20110505
    FAlmacenaImagenGeneral3:Integer;      //SS-231-NDR-20110505
    FAlmacenaImagenFrontal2,			  // SS_1135_CQU_20131030
    FAlmacenaImagenPosterior2:Integer;    // SS_1135_CQU_20131030

    FIniciando: Boolean;
    FUsername: String;
    FPassWord: String;
    CantidadProcesada: Integer;

	FJPG12Frontal: TJPEGPlusImage;
	FJPG12Frontal2: TJPEGPlusImage;
	FJPG12Posterior: TJPEGPlusImage;
	FJPG12Posterior2: TJPEGPlusImage;
	FImgOverview1: TBitMap;
	FImgOverview2: TBitMap;
	FImgOverview3: TBitMap;

	FImagenActual: TJPEGPlusImage;
    FTipoImagenActual: TTipoImagen;
    FShiftOriginal: Integer;

    // Imagenes Italianas               // SS_1091_CQU_20130516
    FImagenItalianaActual,              // SS_1091_CQU_20130516
    FJPGItalianoFrontal,                // SS_1091_CQU_20130516
	FJPGItalianoFrontal2,               // SS_1091_CQU_20130516
	FJPGItalianoPosterior,              // SS_1091_CQU_20130516
	FJPGItalianoPosterior2: TBitmap;    // SS_1091_CQU_20130516

    FPositionPlate: Array[tiFrontal..tiPosterior2] of TRect;
    FProcesandoFecha: TDateTime;
    FFechaATomar: TDateTime;
    FProcesandoPatente: String;
    FProcesandoConcesionaria: Smallint; // Rev. 7 (Fase 2)
    FProcesandoEnBatch: Boolean; // Rev. 7 (Fase 2)
    FFrmImagenOverview: TFrmImageOverview;
    FFormOverviewCargado: Boolean;
    FCodigoTipoInfraccion : SmallInt;   // SS_660_CQU_20121010
    FCodigoPersona : Integer;           // SS_660_CQU_20121010

    FEsKapschImagenActual,              // SS_1147X_CQU_20141217
    FEsKapschFrontal,                   // SS_1147X_CQU_20141217
	FEsKapschFrontal2,                  // SS_1147X_CQU_20141217
	FEsKapschPosterior,                 // SS_1147X_CQU_20141217
	FEsKapschPosterior2 : Boolean;      // SS_1147X_CQU_20141217
    FCodigoConcesionariaNativa : Integer;  //SS_1147ZZZ_NDR_20150415 SS_1222_NDR_20150406 SS_1222_CQU_20150317
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
	FCambioPatente              : Boolean;  // SS_1374A_CQU_20150924
	FCambioPatenteConfirmado    : Boolean;  // SS_1374A_CQU_20150924
    FUltimaPatenteValidada      : string;   // SS_1374A_CQU_20151013
    function ReIngresarPatente  : Boolean;  // SS_1374A_CQU_20150924
	function PuedeConfirmar     : Boolean;  // SS_1374A_CQU_20150924
    procedure MarcarSinImagen;              // SS_1374A_CQU_20150924
    function TieneComodin(Patente: AnsiString): Boolean;   // SS_1374A_CQU_20151002
    procedure SeleccionarImagen(aGraphic: TGraphic; aTipoImagen: TTipoImagen);
    procedure GetImagePlate(aGraphic: TGraphic; TipoImagen: TTipoImagen);
    procedure CentrarImagen;
    procedure CargarInfractor;
    procedure InicializarVariables;
    procedure Siguiente;
    procedure HabilitarBoton;
    procedure ValidacionSemiAutomatica;
    procedure GrabarImagenReferencia(RegistrationAccessibilityRef : Integer);   // SS_1135_CQU_20131030
  public
    { Public declarations }
    //function Inicializar(Patente: String; FechaTransito: TDateTime; CodigoConcesionaria: SmallInt; ProcesarEnBatch: Boolean = False):Boolean; // Rev. 7 (Fase 2)                                                                      // SS_660_CQU_20121010
    function Inicializar(Patente: String; FechaTransito: TDateTime; CodigoConcesionaria: SmallInt; ProcesarEnBatch: Boolean = False; CodigoTipoInfraccion : Integer = 1; CodigoPersona : Integer = -1):Boolean; // Rev. 7 (Fase 2)      // SS_660_CQU_20121010
  end;

var
  formValidarInfraccion: TformValidarInfraccion;
  ImagenAGrabarFrontal : TImage;                    //SS-231-NDR-20110505
  ImagenAGrabarPosterior : TImage;                  //SS-231-NDR-20110505
  ImagenAGrabarGeneral1 : TImage;                   //SS-231-NDR-20110505
  ImagenAGrabarGeneral2 : TImage;                   //SS-231-NDR-20110505
  ImagenAGrabarGeneral3 : TImage;                   //SS-231-NDR-20110505
  ImagenAGrabarFrontal2,                                // SS_1135_CQU_20131030
  ImagenAGrabarPosterior2 : TImage;                     // SS_1135_CQU_20131030
  CONST_CODIGO_TIPO_INFRACCION_NORMAL,                  // SS_660_CQU_20121010
  CONST_CODIGO_TIPO_INFRACCION_POR_MOROSIDAD : Integer; // SS_660_CQU_20121010


implementation

uses frmListadoInfracciones, Peatypes;

ResourceString
    SIN_IMG = 'Sin Imagen JPL';
    IMG = 'Imagen JPL Seleccionada';
    DEBE_IMG = 'Debe especificar una Imagen JPL';
    CAPTION_POS_PATENTE = 'Posici�n de Patente Desconocida';
    //MSG_SQL_CN      = 'SELECT dbo.CONST_CODIGO_CONCESIONARIA_COSTANERA_NORTE()';            //SS_1147ZZZ_NDR_20150415 SS_1222_CQU_20150317
    MSG_SQL_Nativa      = 'SELECT dbo.ObtenerConcesionariaNativa()';                          //SS_1147ZZZ_NDR_20150415 SS_1222_CQU_20150317


const
    NO_LEGIBLE = 'NO LEGIBLE';
    EXTRANJERA = 'EXTRANJERA';
    ESPECIAL =   'ESPECIAL';


{$R *.dfm}

procedure TformValidarInfraccion.btn_SalirClick(Sender: TObject);
begin
    Close;
end;

procedure TformValidarInfraccion.FormClose(Sender: TObject;
  var Action: TCloseAction);
resourcestring
  MGS_ERROR_STORE = ' Error ejecutando LiberarTransitosInfractores';
begin
    if Assigned(FImagenActual) then FImagenActual.Free;
	if Assigned(FJPG12Frontal) then FJPG12Frontal.Free;
	if Assigned(FJPG12Frontal2) then FJPG12Frontal2.Free;
	if Assigned(FJPG12Posterior) then FJPG12Posterior.Free;
	if Assigned(FJPG12Posterior2) then FJPG12Posterior2.Free;
	if Assigned(FImgOverview1) then FImgOverview1.Free;
	if Assigned(FImgOverview2) then FImgOverview2.Free;
	if Assigned(FImgOverview3) then FImgOverview3.Free;
    if Assigned(FFrmImagenOverview) then FFrmImagenOverview.Free;
    ImagenAGrabarFrontal.Free;          //SS-231-NDR-20110505
    ImagenAGrabarPosterior.Free;        //SS-231-NDR-20110505
    ImagenAGrabarGeneral1.Free;         //SS-231-NDR-20110505
    ImagenAGrabarGeneral2.Free;         //SS-231-NDR-20110505
    ImagenAGrabarGeneral3.Free;         //SS-231-NDR-20110505
    ImagenAGrabarFrontal2.Free;         // SS_1135_CQU_20131030
    ImagenAGrabarPosterior2.Free;       // SS_1135_CQU_20131030

    // Imagenes Italianas                                                           // SS_1091_CQU_20130516
    if Assigned(FImagenItalianaActual)      then FImagenItalianaActual.Free;        // SS_1091_CQU_20130516
    if Assigned(FJPGItalianoFrontal)        then FJPGItalianoFrontal.Free;          // SS_1091_CQU_20130516
	if Assigned(FJPGItalianoFrontal2)       then FJPGItalianoFrontal2.Free;         // SS_1091_CQU_20130516
	if Assigned(FJPGItalianoPosterior)      then FJPGItalianoPosterior.Free;        // SS_1091_CQU_20130516
	if Assigned(FJPGItalianoPosterior2)     then FJPGItalianoPosterior2.Free;       // SS_1091_CQU_20130516

	try
	    LiberarTransitosInfractores.Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
    	LiberarTransitosInfractores.ExecProc;
    except
        on e: Exception do begin
            screen.Cursor := crDefault;
            MsgBoxErr(MGS_ERROR_STORE, e.Message, 'Infracciones', MB_ICONSTOP);
        end;
    end;

    Action := caFree;
end;

{
    Function Name: Inicializar
    Author:
    Date Created:
    Parameters: Patente: String; FechaTransito: TDateTime; CodigoConcesionaria: Smallint
    Result: Boolean

    Revision: 7
        Author: pdominguez
        Date: 17/05/2010
        Description: Infractores Fase 2
            - Se a�ade el par�metro CodigoConcesionaria.
}
//function TformValidarInfraccion.Inicializar(Patente: String; FechaTransito: TDateTime; CodigoConcesionaria: Smallint; ProcesarEnBatch: Boolean = False): Boolean;                                                                 // SS_660_CQU_20121010
function TformValidarInfraccion.Inicializar(Patente: String; FechaTransito: TDateTime; CodigoConcesionaria: Smallint; ProcesarEnBatch: Boolean = False; CodigoTipoInfraccion : Integer = 1; CodigoPersona : Integer = -1): Boolean; // SS_660_CQU_20121010
var
    S: TSize;
begin
    Result := True;
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216
    try
        ImagenAGrabarFrontal    := TImage.Create(Self);    //SS-231-NDR-20110505
        ImagenAGrabarPosterior  := TImage.Create(Self);    //SS-231-NDR-20110505
        ImagenAGrabarGeneral1   := TImage.Create(Self);    //SS-231-NDR-20110505
        ImagenAGrabarGeneral2   := TImage.Create(Self);    //SS-231-NDR-20110505
        ImagenAGrabarGeneral3   := TImage.Create(Self);    //SS-231-NDR-20110505
        ImagenAGrabarFrontal2   := TImage.Create(Self);    // SS_1135_CQU_20131030
        ImagenAGrabarPosterior2 := TImage.Create(Self);    // SS_1135_CQU_20131030

        FEsKapschImagenActual   := True;    // SS_1147X_CQU_20141217
        FEsKapschFrontal        := True;    // SS_1147X_CQU_20141217
        FEsKapschFrontal2       := True;    // SS_1147X_CQU_20141217
        FEsKapschPosterior      := True;    // SS_1147X_CQU_20141217
        FEsKapschPosterior2     := True;    // SS_1147X_CQU_20141217

        //ObtenerParametroGeneral(DMConnections.BaseCAC, DIR_IMAGENES_TRANSITOS, FImagePath);                                                              //SS-377-NDR-20110607
        ObtenerParametroGeneral(DMConnections.BaseCAC, DIR_IMAGENES_TRANSITOS_NUEVO_FORMATO_IMAGEN, FImagePathNFI);                                        //SS-377-NDR-20110607
        ObtenerParametroGeneral(DMConnections.BaseCAC, DIR_IMAGENES_TRANSITOS_INFRACTORES, FImagePathInfractores);         //SS-231-NDR-20110505
        ObtenerParametroGeneral(DMConnections.BaseCAC, ALMACENA_IMAGENES_INFRAC_FRONTAL, FAlmacenaImagenFrontal);          //SS-231-NDR-20110505
        ObtenerParametroGeneral(DMConnections.BaseCAC, ALMACENA_IMAGENES_INFRAC_POSTERIOR, FAlmacenaImagenPosterior);      //SS-231-NDR-20110505
        ObtenerParametroGeneral(DMConnections.BaseCAC, ALMACENA_IMAGENES_INFRAC_GENERAL1, FAlmacenaImagenGeneral1);        //SS-231-NDR-20110505
        ObtenerParametroGeneral(DMConnections.BaseCAC, ALMACENA_IMAGENES_INFRAC_GENERAL2, FAlmacenaImagenGeneral2);        //SS-231-NDR-20110505
        ObtenerParametroGeneral(DMConnections.BaseCAC, ALMACENA_IMAGENES_INFRAC_GENERAL3, FAlmacenaImagenGeneral3);        //SS-231-NDR-20110505
        ObtenerParametroGeneral(DMConnections.BaseCAC, ALMACENA_IMAGENES_INFRAC_FRONTAL2, FAlmacenaImagenFrontal2);        // SS_1135_CQU_20131030
        ObtenerParametroGeneral(DMConnections.BaseCAC, ALMACENA_IMAGENES_INFRAC_POSTERIOR2, FAlmacenaImagenPosterior2);    // SS_1135_CQU_20131030
        //if Trim(FImagePath) = '' then raise Exception.Create('No se encontro el Parametro ' + DIR_IMAGENES_TRANSITOS);                                   //SS-377-NDR-20110607
        if Trim(FImagePathInfractores) = '' then raise Exception.Create('No se encontro el Parametro ' + DIR_IMAGENES_TRANSITOS_INFRACTORES);  //SS-231-NDR-20110505
        if Trim(FImagePathNFI) = '' then raise Exception.Create('No se encontro el Parametro ' + DIR_IMAGENES_TRANSITOS_NUEVO_FORMATO_IMAGEN);             //SS-377-NDR-20110607

        screen.Cursor := crHourGlass;
        // Inicializamos los controles para la imagen Overview
        FFrmImagenOverview := TfrmImageOverview.create(self);
        FFrmImagenOverview.Parent := self;
        FFrmImagenOverview.Left := self.ScreenToClient(ImagenAmpliar.ClientToScreen(Point(0, ImagenAmpliar.Left))).X + 2 ;
        FFrmImagenOverview.top  := self.ScreenToClient(ImagenAmpliar.ClientToScreen(Point(ImagenAmpliar.top, 0))).Y + 2;

        //Cargo el peromiso para VerMascara
        chk_VerMascara.Checked := True;
        chk_VerMascara.Visible := ExisteAcceso('ver_mascara');
        //Revision 3: Se quita el uso de este check 
        //chk_ConsultarRNVM.Checked := True;
        chkValidacionSemiAutomatica.Checked := True;

        S := GetFormClientSize(Application.MainForm);
        SetBounds(0, 0, S.cx, S.cy);

        txt_Patente.clear;
        lblPatentePropuesta.Caption := '';
        cdsObtenerTransitoInfractoresPatente.CreateDataSet;
        Update;

        // Rev. 7 (Fase 2)
        //FValidarxFecha := Patente = '';
        FProcesandoEnBatch := ProcesarEnBatch;

        if Not FProcesandoEnBatch then begin
            FProcesandoFecha         := FechaTransito;
            FProcesandoPatente       := Patente;
            FProcesandoConcesionaria := CodigoConcesionaria;
        end;
        // Fin Rev. 7 (Fase 2)
        FCodigoTipoInfraccion := CodigoTipoInfraccion;  // SS_660_CQU_20121010
        FCodigoPersona        := CodigoPersona;         // SS_660_CQU_20121010

        CONST_CODIGO_TIPO_INFRACCION_NORMAL         := SysUtilsCN.QueryGetIntegerValue(DMConnections.BaseCAC, 'SELECT dbo.CONST_CODIGO_TIPO_INFRACCION_NORMAL()');           // SS_660_CQU_20121010
        CONST_CODIGO_TIPO_INFRACCION_POR_MOROSIDAD  := SysUtilsCN.QueryGetIntegerValue(DMConnections.BaseCAC, 'SELECT dbo.CONST_CODIGO_TIPO_INFRACCION_POR_MOROSIDAD()');    // SS_660_CQU_20121010

        FCodigoConcesionariaNativa                  := SysUtilsCN.QueryGetIntegerValue(DMConnections.BaseCAC, MSG_SQL_Nativa);   //SS_1147ZZZ_NDR_20150415 SS_1222_NDR_20150406 SS_1222_CQU_20150317

        CargarInfractor;

        if FCodigoTipoInfraccion = CONST_CODIGO_TIPO_INFRACCION_POR_MOROSIDAD then begin    // SS_660_CQU_20121010
            btnPatenteExtranjera.Enabled := False;                                          // SS_660_CQU_20121010
            btnPatenteEspecial.Enabled := False;                                            // SS_660_CQU_20121010
            lblListaAmarilla.Visible := True;                                               // SS_660_CQU_20130822
        end;                                                                                // SS_660_CQU_20121010
        FCambioPatenteConfirmado := False;                                                  // SS_1374A_CQU_20150924
        Result := True;
        screen.Cursor := crDefault;
    except
        on e: Exception do begin
            screen.Cursor := crDefault;
            MsgBoxErr(format(MSG_ERROR_INICIALIZACION,['Infracciones']), e.Message, 'Infracciones', MB_ICONSTOP);
        end;
    end;

end;

procedure TformValidarInfraccion.SeleccionarImagen(aGraphic: TGraphic; aTipoImagen: TTipoImagen);
begin
    if FTipoImagenActual = aTipoImagen then exit;

    if assigned(aGraphic) then begin
        if aGraphic.Empty then Exit;                                                                    // SS_1138_CQU_20131009
		Screen.Cursor := crHourGlass;
        try
		  	if aTipoImagen in [tiFrontal, tiPosterior, tiFrontal2, tiPosterior2] then begin
                if      ((aTipoImagen = tiFrontal) AND FEsKapschFrontal) then FEsKapschImagenActual := True         // SS_1147X_CQU_20141217
                else if ((aTipoImagen = tiFrontal2) AND FEsKapschFrontal2) then FEsKapschImagenActual := True       // SS_1147X_CQU_20141217
                else if ((aTipoImagen = tiPosterior) AND FEsKapschPosterior) then FEsKapschImagenActual := True     // SS_1147X_CQU_20141217
                else if ((aTipoImagen = tiPosterior2) AND FEsKapschPosterior2) then FEsKapschImagenActual := True   // SS_1147X_CQU_20141217
                else FEsKapschImagenActual := false;                                                                // SS_1147X_CQU_20141217
                //FImagenActual.Assign(aGraphic);                                                       // SS_1091_CQU_20130516
                //if not cdsObtenerTransitoInfractoresPatente.FieldByName('EsItaliano').AsBoolean then  // SS_1147X_CQU_20141217 // SS_1091_CQU_20130516
                if FEsKapschImagenActual then                                                           // SS_1147X_CQU_20141217
				    FImagenActual.Assign(aGraphic)                                                      // SS_1091_CQU_20130516
                else                                                                                    // SS_1091_CQU_20130516
                    FImagenItalianaActual.Assign(aGraphic);                                             // SS_1091_CQU_20130516
				FShiftOriginal := -1;
                FTipoImagenActual := aTipoImagen;
                Redibujar(chk_VerMascara.Checked);
                actResetImageExecute(nil);
                //if cdsObtenerTransitoInfractoresPatente.FieldByName('EsItaliano').AsBoolean then begin// SS_1147X_CQU_20141217  // SS_1138_CQU_20131009
                if not FEsKapschImagenActual then begin                                                 // SS_1147X_CQU_20141217
                    if aTipoImagen in [tiFrontal, tiPosterior] then                                     // SS_1138_CQU_20131009
                        ImagenAmpliar.Scale := 0.4                                                      // SS_1138_CQU_20131009
                    else if aTipoImagen in [tiFrontal2, tiPosterior2] then                              // SS_1138_CQU_20131009
                        ImagenAmpliar.Scale := 0.1;                                                     // SS_1138_CQU_20131009
                end;                                                                                    // SS_1138_CQU_20131009
            end else begin
                FFormOverviewCargado := true;
                FFrmImagenOverview.ShowImageOverview(aGraphic, aTipoImagen);
                FFrmImagenOverview.Show;
            end;

        finally
            AjustarTama�oTImagePlus(ImagenAmpliar);                                        // SS_1374A_CQU_20151002
            Screen.Cursor := crDefault;
		end;
	end;
end;

procedure TformValidarInfraccion.sbThumb0Click(Sender: TObject);
begin
    //SeleccionarImagen(FJPG12Frontal, tiFrontal);                                          // SS_1091_CQU_20120516
    //if not cdsObtenerTransitoInfractoresPatente.FieldByName('EsItaliano').AsBoolean then  // SS_1147X_CQU_20141217  // SS_1091_CQU_20120516
    if FEsKapschFrontal then                                                                // SS_1147X_CQU_20141217
        SeleccionarImagen(FJPG12Frontal, tiFrontal)                                         // SS_1091_CQU_20120516
    else                                                                                    // SS_1091_CQU_20120516
        SeleccionarImagen(FJPGItalianoFrontal, tiFrontal);                                  // SS_1091_CQU_20120516
end;

procedure TformValidarInfraccion.sbThumb1Click(Sender: TObject);
begin
    //SeleccionarImagen(FJPG12Posterior, tiPosterior);                                      // SS_1091_CQU_20120516
    //if not cdsObtenerTransitoInfractoresPatente.FieldByName('EsItaliano').AsBoolean then  // SS_1147X_CQU_20141217  // SS_1091_CQU_20120516
    if FEsKapschPosterior then                                                              // SS_1147X_CQU_20141217
        SeleccionarImagen(FJPG12Posterior, tiPosterior)                                     // SS_1091_CQU_20120516
    else                                                                                    // SS_1091_CQU_20120516
        SeleccionarImagen(FJPGItalianoPosterior, tiPosterior);                              // SS_1091_CQU_20120516
end;

procedure TformValidarInfraccion.sbThumb2Click(Sender: TObject);
begin
    SeleccionarImagen(FImgOverview1, tiOverview1);
end;

procedure TformValidarInfraccion.sbThumb3Click(Sender: TObject);
begin
    SeleccionarImagen(FImgOverview2, tiOverview2);
end;

procedure TformValidarInfraccion.sbThumb4Click(Sender: TObject);
begin
    SeleccionarImagen(FImgOverview3, tiOverview3);
end;

procedure TformValidarInfraccion.sbThumb5Click(Sender: TObject);
begin
    //SeleccionarImagen(FJPG12Frontal2, tiFrontal2);                                        // SS_1091_CQU_20120516
    //if not cdsObtenerTransitoInfractoresPatente.FieldByName('EsItaliano').AsBoolean then  // SS_1147X_CQU_20141217  // SS_1091_CQU_20120516
    if FEsKapschFrontal2 then                                                               // SS_1147X_CQU_20141217
        SeleccionarImagen(FJPG12Frontal2, tiFrontal2)                                       // SS_1091_CQU_20120516
    else                                                                                    // SS_1091_CQU_20120516
        SeleccionarImagen(FJPGItalianoFrontal2, tiFrontal2);                                // SS_1091_CQU_20120516
end;

procedure TformValidarInfraccion.sbThumb6Click(Sender: TObject);
begin
    //SeleccionarImagen(FJPG12Posterior2, tiPosterior2);                                    // SS_1091_CQU_20120516
    //if not cdsObtenerTransitoInfractoresPatente.FieldByName('EsItaliano').AsBoolean then  // SS_1147X_CQU_20141217  // SS_1091_CQU_20120516
    if FEsKapschPosterior2 then                                                             // SS_1147X_CQU_20141217
        SeleccionarImagen(FJPG12Posterior2, tiPosterior2)                                   // SS_1091_CQU_20120516
    else                                                                                    // SS_1091_CQU_20120516
        SeleccionarImagen(FJPGItalianoPosterior2, tiPosterior2);                            // SS_1091_CQU_20120516
end;

procedure TformValidarInfraccion.actZoomInExecute(Sender: TObject);
begin
   	ImagenAmpliar.Scale := ImagenAmpliar.Scale + 0.2;
end;

procedure TformValidarInfraccion.actZoomOutExecute(Sender: TObject);
begin
    //if ImagenAmpliar.Scale < 0.5 then exit;                                               //  SS_1138_CQU_20131009
    //ImagenAmpliar.Scale := ImagenAmpliar.Scale - 0.2;                                     //  SS_1138_CQU_20131009
    //if not cdsObtenerTransitoInfractoresPatente.FieldByName('EsItaliano').AsBoolean then  // SS_1147X_CQU_20141217  //  SS_1138_CQU_20131009
    if FEsKapschImagenActual then                                                           // SS_1147X_CQU_20141217
    begin                                                                                   //  SS_1138_CQU_20131009
        if ImagenAmpliar.Scale < 0.5 then exit;                                             //  SS_1138_CQU_20131009
        ImagenAmpliar.Scale := ImagenAmpliar.Scale - 0.2;                                   //  SS_1138_CQU_20131009
    end else begin                                                                          //  SS_1138_CQU_20131009
        if FTipoImagenActual in [tiFrontal, tiPosterior] then begin                         //  SS_1138_CQU_20131009
            if ImagenAmpliar.Scale < 0.3 then exit                                          //  SS_1138_CQU_20131009
            else ImagenAmpliar.Scale := ImagenAmpliar.Scale - 0.2;                          //  SS_1138_CQU_20131009
        end else if FTipoImagenActual in [tiFrontal2, tiPosterior2] then begin              //  SS_1138_CQU_20131009
            if ImagenAmpliar.Scale > 0.2 then                                               //  SS_1138_CQU_20131009
                ImagenAmpliar.Scale := ImagenAmpliar.Scale - 0.1;                           //  SS_1138_CQU_20131009
        end;                                                                                //  SS_1138_CQU_20131009
    end;                                                                                    //  SS_1138_CQU_20131009
end;

procedure TformValidarInfraccion.actZoomResetExecute(Sender: TObject);
begin
    //if ImagenAmpliar.Scale > 4.0 then exit;   // SS_1374A_CQU_20150924
    //ImagenAmpliar.Scale := 1;                 // SS_1374A_CQU_20150924
    AjustarTama�oTImagePlus(ImagenAmpliar);     // SS_1374A_CQU_20150924
end;

procedure TformValidarInfraccion.actZoomPlateExecute(Sender: TObject);
begin
    // Calcular la escala de la im�gen en funcion del marco de la patente
    ImagenAmpliar.Scale := 1;
end;

procedure TformValidarInfraccion.actBrightPlusExecute(Sender: TObject);
begin
    //if not cdsObtenerTransitoInfractoresPatente.FieldByName('EsItaliano').AsBoolean then begin  // SS_1147X_CQU_20141217 // SS_1091_CQU_20130516
    if FEsKapschImagenActual then begin                                                           // SS_1147X_CQU_20141217
        // Si el Shift supero el limite nos vamos
        if FImagenActual.Shift = 0 then exit;
        // Si el FShift no esta inicializado lo asignamos
        if FShiftOriginal = -1 then begin
            FShiftOriginal := FImagenActual.Shift;
        end;
        FImagenActual.Shift := FImagenActual.Shift - 1;
    end;                                                                                        // SS_1091_CQU_20130516
	Redibujar(chk_VerMascara.Checked);
end;

procedure TformValidarInfraccion.actBrigthResetExecute(Sender: TObject);
begin
    //if not cdsObtenerTransitoInfractoresPatente.FieldByName('EsItaliano').AsBoolean then begin    // SS_1147X_CQU_20141217 // SS_1091_CQU_20130516
    if FEsKapschImagenActual then begin                                                             // SS_1147X_CQU_20141217
        if FShiftOriginal = -1 then begin
            FShiftOriginal := FImagenActual.Shift;
        end;
        if FImagenActual.Shift = FShiftOriginal then exit;
        FImagenActual.Shift := FShiftOriginal;
    end;                                                                                       // SS_1091_CQU_20130516
    Redibujar(chk_VerMascara.Checked);
end;

procedure TformValidarInfraccion.actBrigthLessExecute(Sender: TObject);
begin
    //if not cdsObtenerTransitoInfractoresPatente.FieldByName('EsItaliano').AsBoolean then begin    // SS_1147X_CQU_20141217 // SS_1091_CQU_20130516
    if FEsKapschImagenActual then begin                                                             // SS_1147X_CQU_20141217
        if FImagenActual.Shift > 4 then exit;
        // Si el FShift no esta inicializado lo asignamos
        if FShiftOriginal = -1 then begin
            FShiftOriginal := FImagenActual.Shift;
        end;
        FImagenActual.Shift := FImagenActual.Shift + 1;
    end;                                                                                       // SS_1091_CQU_20130516
	Redibujar(chk_VerMascara.Checked);
end;

procedure TformValidarInfraccion.actRemarcarBordesExecute(Sender: TObject);
begin
	Redibujar(chk_VerMascara.Checked);
end;

procedure TformValidarInfraccion.actSuavizarBordesExecute(Sender: TObject);
begin
	Redibujar(chk_VerMascara.Checked);
end;

procedure TformValidarInfraccion.actMaskExecute(Sender: TObject);
begin
	Redibujar(chk_VerMascara.Checked);
end;

procedure TformValidarInfraccion.Redibujar(VerMascara: Boolean);
var
    bmp: TBitMap;
    EsItaliano : Boolean;   // SS_1091_CQU_20130516
begin
	Screen.Cursor := crHourGlass;

    bmp := TBitMap.create;
    bmp.PixelFormat := pf24bit;

	try
        // Se coloca el try ya que pasa por aqu� antes de incializar el cds                         // SS_1091_CQU_20130516
        try                                                                                         // SS_1091_CQU_20130516
            EsItaliano := cdsObtenerTransitoInfractoresPatente.FieldByName('EsItaliano').AsBoolean; // SS_1091_CQU_20130516
        except                                                                                      // SS_1091_CQU_20130516
            EsItaliano := False;                                                                    // SS_1091_CQU_20130516
        end;                                                                                        // SS_1091_CQU_20130516
        // Reseteamos los filtros aplicados
        //bmp.Assign(FImagenActual);            // SS_1091_CQU_20130516
        //if EsItaliano then                    // SS_1147X_CQU_20141217  // SS_1091_CQU_20130516
        if not FEsKapschImagenActual then       // SS_1147X_CQU_20141217
            bmp.Assign(FImagenItalianaActual)   // SS_1091_CQU_20130516
        else                                    // SS_1091_CQU_20130516
            bmp.Assign(FImagenActual);          // SS_1091_CQU_20130516

    	// Aplicamos el Sharp
	    if actRemarcarBordes.Checked then begin
 		    Sharp(bmp);
    	end;

		// Aplicamos el Blurr
		if actSuavizarBordes.checked then begin
		    Blurr(bmp);
    	end;

        // Dibujamos la Mascara
        //if VerMascara then                                        //  SS_1138_CQU_20131009
        if not EsItaliano and VerMascara then                       //  SS_1138_CQU_20131009
            DrawMask(bmp, FPositionPlate[FTipoImagenActual].top - 4);

        // Cargamos la imagen de la patente
        //GetImagePlate(FImagenActual, FTipoImagenActual);          // SS_1091_CQU_20130516
        //if EsItaliano then                                        // SS_1147X_CQU_20141217  // SS_1091_CQU_20130516
        if not FEsKapschImagenActual then                           // SS_1147X_CQU_20141217
            GetImagePlate(FImagenItalianaActual, FTipoImagenActual) // SS_1091_CQU_20130516
        else                                                        // SS_1091_CQU_20130516
            GetImagePlate(FImagenActual, FTipoImagenActual);        // SS_1091_CQU_20130516

        // Asignamos la nueva imagen
        ImagenAmpliar.Picture.Assign(bmp);
    finally
        Screen.Cursor := crDefault;
        bmp.Free;
    end;
end;


procedure TformValidarInfraccion.GetImagePlate(aGraphic: TGraphic; TipoImagen: TTipoImagen);
var
    bmpSource, bmpDest: TBitMap;
    EsItaliano : Boolean;                                                                               //  SS_1138_CQU_20131009
begin
    bmpSource := TBitMap.create;
	bmpDest := TBitMap.create;
    try
        if cdsObtenerTransitoInfractoresPatente.Active then                                             //  SS_1138_CQU_20131009
            EsItaliano := cdsObtenerTransitoInfractoresPatente.FieldByName('EsItaliano').AsBoolean      //  SS_1138_CQU_20131009
        else EsItaliano := False;                                                                       //  SS_1138_CQU_20131009
        
        if ((FPositionPlate[TipoImagen].Left = 0) and (FPositionPlate[TipoImagen].Right = 0)) then begin
            pnlImgPatente.Caption := CAPTION_POS_PATENTE;
            imgPatente.Picture.Bitmap := Nil;
            //btnImgRef.Enabled := False;                                                                   //  SS_1138_CQU_20131009
            //if not EsItaliano then btnImgRef.Enabled := False                                             //  SS_1222_MCA_20150406 //  SS_1138_CQU_20131009
            //else btnImgRef.Enabled := True;                                                               //  SS_1222_MCA_20150406 //  SS_1138_CQU_20131009
            btnImgRef.Enabled := True;                                                                      //  SS_1222_MCA_20150406
        end else begin
            bmpSource.PixelFormat := pf24bit;
            bmpSource.Assign(aGraphic);
            bmpDest.PixelFormat := pf24bit;
            bmpDest.Width := imgPatente.Width;
            bmpDest.Height := imgPatente.Height;
			bmpDest.Canvas.CopyRect(Rect(0, 0, imgPatente.Width, imgPatente.Height), bmpSource.Canvas,
				Rect(FPositionPlate[TipoImagen].Left - 4, FPositionPlate[TipoImagen].top - 4,
                FPositionPlate[TipoImagen].right + 4, FPositionPlate[TipoImagen].Bottom + 4));
            imgPatente.Picture.Assign(bmpDest);
            btnImgRef.Enabled := True;
        end;
    finally
        bmpSource.free;
        bmpDest.free;
    end;
end;


procedure TformValidarInfraccion.actCenterToPlateExecute(Sender: TObject);
begin
	CentrarImagen;
end;

procedure TformValidarInfraccion.actResetImageExecute(Sender: TObject);
begin
    // Volvemos a False todos los filtros seleccionados
	actRemarcarBordes.Checked := False;
	actSuavizarBordes.Checked := False;
    //if not cdsObtenerTransitoInfractoresPatente.FieldByName('EsItaliano').AsBoolean then begin    // SS_1147X_CQU_20141217 // SS_1091_CQU_20130516
    if FEsKapschImagenActual then begin                                                             // SS_1147X_CQU_20141217
        // Volvelos el brillo al original
        if FShiftOriginal = -1 then begin
            FShiftOriginal := FImagenActual.Shift;
        end;
        FImagenActual.Shift    := FShiftOriginal;
    end;                                                                                       // SS_1091_CQU_20130516
    // Volvemos el Zoom al original
    ImagenAmpliar.Scale     := 1;
    // Redibujamos
    Redibujar(chk_VerMascara.Checked);
	// Centramos la imagen
    CentrarImagen;
    AjustarTama�oTImagePlus(ImagenAmpliar);    // SS_1374A_CQU_20150924
end;

procedure TformValidarInfraccion.CentrarImagen;
begin
    if FPositionPlate[FTipoImagenActual].Left = 0 then begin
            ImagenAmpliar.Center
    end else
        ImagenAmpliar.CenterToPoint(Point(
            FPositionPlate[FTipoImagenActual].Left +
            ((FPositionPlate[FTipoImagenActual].right - FPositionPlate[FTipoImagenActual].Left) div 2),
            FPositionPlate[FTipoImagenActual].top +
            ((FPositionPlate[FTipoImagenActual].bottom - FPositionPlate[FTipoImagenActual].top) div 2)));
end;

{
    Procedure Name: CargarInfractor
    Author:
    Date Created:
    Parameters: None

    Revision: 7
        Author: pdominguez
        Date: 17/05/2010
        Description: Infractores Fase 2
            - Se modifica el proceso para trabajar en modo Batch por d�a y Concesionaria.
}
procedure TformValidarInfraccion.CargarInfractor;
resourcestring
    CAPTION_A_MOSTRAR = 'Validaci�n de Infracciones - Patente: %s del %s - Concesionaria: %s';
    NO_TRANSITO = 'No hay transitos para Validar.';
    FIN_PATENTE = 'Ya no hay m�s tr�nsitos para validar para esta patente.';
    MSG_CON_TELEVIA = 'Si';
    MSG_SIN_TELEVIA = 'No';
var
	EjecutandoStore: string;
begin
    if (not FProcesandoEnBatch) and (ReservarTransitosInfractores.Active) and (ReservarTransitosInfractores.Eof) then begin
        MsgBox(FIN_PATENTE, Caption, MB_ICONINFORMATION);
        if Assigned(FormListadoInfracciones) then
            FormListadoInfracciones.btn_Filtrar.Click;
        Close;
        Exit;
    end;
    EjecutandoStore := '';
    try
        FIniciando := True;

        gbTransitos.Enabled := True;
        gbImagenAmpliar.Enabled := True;
        gbImagenes.Enabled := True;
        FImagenRef := '';
        FRegistrationAccessibilityRef := RegistrationAccessibilityImagen[tiDesconocida]; // SS_1138_CQU_20131009
        lblSetImagen.Caption := DEBE_IMG;
        EnableControlsInContainer(gbDatosValidar, True);
        EnableControlsInContainer(pnl_Botones, True);
        HabilitarBoton;
        lblCategoria.Caption := '';
        lblPatentePropuesta.Caption := '';
        txt_Categoria.ValueInt := 0;
        txt_Patente.Text := '';
        FValidacionSemiAutomatica := False;

        cdsObtenerTransitoInfractoresPatente.DisableControls;
        cdsObtenerTransitoInfractoresPatente.EmptyDataSet;
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        Update;

        with ReservarTransitosInfractores do begin

            if (not Active) or Eof then begin
                if Active then Close;

			    EjecutandoStore := ' LiberarTransitosInfractores';
                LiberarTransitosInfractores.Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
                LiberarTransitosInfractores.ExecProc;

                EjecutandoStore := ' ReservarTransitosInfractores';

                Parameters.ParamByName('@FechaDesde').Value           := IIf(FProcesandoEnBatch, Null, FProcesandoFecha);
                Parameters.ParamByName('@FechaHasta').Value           := IIf(FProcesandoEnBatch, Null, FProcesandoFecha);
                Parameters.ParamByName('@Patente').Value              := IIf(FProcesandoEnBatch, Null, FProcesandoPatente);
                Parameters.ParamByName('@CantidadInfracciones').Value := Null;
                Parameters.ParamByName('@CodigoConcesionaria').Value  := IIf(FProcesandoEnBatch, Null, FProcesandoConcesionaria);
                Parameters.ParamByName('@Usuario').Value              := UsuarioSistema;

                Open;

                if IsEmpty then begin
                    ShowMessage('Fin Validaci�n');
                    Self.Close;
                end;
            end;
            //else Next;

            FProcesandoFecha         := FieldByName('FechaInfraccion').AsDateTime;
            FProcesandoPatente       := FieldByName('Patente').Asstring;
            FProcesandoConcesionaria := FieldByName('CodigoConcesionaria').AsInteger;

            Caption :=
                Format(
                        CAPTION_A_MOSTRAR,
                        [
                            Trim(FProcesandoPatente),
                            FormatDateTime('dd/mm/yyyy',FProcesandoFecha),
                            FieldByName('Concesionaria').AsString
                        ]);

            if FUltimaPatenteValidada <> ''                             // SS_1374A_CQU_20151013
            then lblUltimaValidada.Caption := FUltimaPatenteValidada    // SS_1374A_CQU_20151013
            else lblUltimaValidada.Caption := '';                       // SS_1374A_CQU_20151013
        end;

        // Cargo el DataModulo
        EjecutandoStore := ' ObtenerTransitoInfractoresPatente';

        with ObtenerTransitoInfractoresPatente do begin
            if Active then Close;

            Parameters.ParamByName('@Patente').Value             := FProcesandoPatente;
            Parameters.ParamByName('@FechaTransito').Value       := FProcesandoFecha;
            Parameters.ParamByName('@CodigoConcesionaria').Value := FProcesandoConcesionaria;
            Open;
        end;


        while not ObtenerTransitoInfractoresPatente.Eof do begin
            with cdsObtenerTransitoInfractoresPatente do begin

                Append;
                FieldByName('DescripPuntoCobro').Value   := ObtenerTransitoInfractoresPatente.FieldByName('DescripPuntoCobro').Value;
                FieldByName('PatenteNueva').Clear;
                FieldByName('Patente').Value             := ObtenerTransitoInfractoresPatente.FieldByName('Patente').Value;
                FieldByName('FechaHora').Value           := ObtenerTransitoInfractoresPatente.FieldByName('FechaHora').Value;
                FieldByName('NumCorrCA').Value           := IntToStr(ObtenerTransitoInfractoresPatente.FieldByName('NumCorrCA').Value);
                FieldByName('Categoria').Value           := ObtenerTransitoInfractoresPatente.FieldByName('Categoria').AsString;
                FieldByName('TieneTelevia').AsString     := iif(ObtenerTransitoInfractoresPatente.FieldByName('ContractSerialNumber').Value = NULL, MSG_SIN_TELEVIA, MSG_CON_TELEVIA);
                FieldByName('CodigoCOncesionaria').Value := FProcesandoConcesionaria;
                FieldByName('Moroso').Value              := ObtenerTransitoInfractoresPatente.FieldByName('Moroso').Value;  // SS_660_CQU_20121010
                FieldByName('EsItaliano').Value          := ObtenerTransitoInfractoresPatente.FieldByName('EsItaliano').Value;  // SS_1091_CQU_20130516
                FieldByName('PatenteDetectada').Value    := ObtenerTransitoInfractoresPatente.FieldByName('PatenteDetectada').Value;    // SS_1374_CQU_20150901
                FieldByName('PatenteValidada').Value     := ObtenerTransitoInfractoresPatente.FieldByName('PatenteValidada').Value;     // SS_1374_CQU_20150901
                FieldByName('NombreCorto').Value         := ObtenerTransitoInfractoresPatente.FieldByName('NombreCorto').AsString;      // SS_1374A_CQU_20151002
                FieldByName('EstadoTransito').Value      := ObtenerTransitoInfractoresPatente.FieldByName('EstadoTransito').AsInteger;  // SS_1374A_CQU_20151002
                FieldByName('EnSeguimiento').Value       := ObtenerTransitoInfractoresPatente.FieldByName('EnSeguimiento').AsBoolean;   // SS_1374A_CQU_20151002
                FieldByName('CodigoPersona').Value       := ObtenerTransitoInfractoresPatente.FieldByName('CodigoPersona').AsInteger;   // SS_1374A_CQU_20151013
                Post;
            end;

            ObtenerTransitoInfractoresPatente.Next;
        end;

        ObtenerTransitoInfractoresPatente.close;

        EjecutandoStore := '';

        CantidadProcesada := 0;
        cdsObtenerTransitoInfractoresPatente.First;
        cdsObtenerTransitoInfractoresPatente.EnableControls;
        FCambioPatenteConfirmado := False;  // SS_1374A_CQU_20150924
        FCambioPatente := False;            // SS_1374A_CQU_20150924
        FIniciando := False;

        if not cdsObtenerTransitoInfractoresPatente.IsEmpty then begin

            cdsObtenerTransitoInfractoresPatente.AfterScroll(cdsObtenerTransitoInfractoresPatente) // Para que cargue la primera imagen
        end else begin

            Caption := format(CAPTION_A_MOSTRAR,['Sin Determinar','Sin Determinar', 'Sin Determinar']);     // SS_1222_MCA_20150406
            gbTransitos.Enabled := False;
            gbImagenAmpliar.Enabled := False;
            gbImagenes.Enabled := False;
            ImagenAmpliar.Picture.Bitmap := nil;
            imgPatente.Picture.Bitmap := nil;
            EnableControlsInContainer(gbDatosValidar, False);
            btn_AceptarVehiculo.Enabled := False;
            MsgBox(NO_TRANSITO, Caption, MB_ICONINFORMATION);
        end;
        Screen.Cursor := crDefault;
    except
        on e: Exception do begin
                Screen.Cursor := crDefault;
                gbTransitos.Enabled := False;
                gbImagenAmpliar.Enabled := False;
                gbImagenes.Enabled := False;
                EnableControlsInContainer(gbDatosValidar, False);
                btn_AceptarVehiculo.Enabled := False;
                FProcesandoFecha := NullDate;
                MsgBoxErr('Error al inicializar, ejecutando' + EjecutandoStore, e.Message, Caption, MB_ICONSTOP);
            end;
    end;
end;

{******************************** Function Header ******************************
Function Name: btn_AceptarVehiculoClick
Author :
Date Created :
Description :
Parameters : Sender: TObject
Return Value : None
Revision :
    Author : vpaszkowicz
    Date : 19/10/2007
    Description : Seteo en Null el par�metro Categoria si en la tabla de memoria
    est� como blanco.
Revision: 2
    Author : pdominguez
    Date   : 15/09/2009
    Description : SS 826
        - Se verifica que el estado del Tr�nsito validado siga siendo 3. Si el
        estado fu� cambiado por otro proceso, generamos un evento de alarma en
        la tabla AlarmasEventos y al final del proceso mostramos un mensaje
        al operador con las incidencias encontradas.

Revision: 7
    Author: pdominguez
    Date: 17/05/2010
    Description: Infractores Fase 2
        - Se a�ade el par�metro @CodigoConcesionaria en la llamada al SP ActualizarInfractores.
*******************************************************************************}
procedure TformValidarInfraccion.btn_AceptarVehiculoClick(Sender: TObject);
resourcestring
    ERROR_GUARDAR = 'Error al guardar el transito validado';
    ERROR_STORENAME = ' ActualizarInfracciones';
    ERROR_ESTADO = 'El Estado del Tr�nsito ha cambiado. NumCorrCA: %s, Estado Actual: %d, Usuario: %s. No se guard� la Validaci�n del Tr�nsito'; // Rev. 1 (SS 627)
    ERROR_GUARDAR_ALARMA = 'Se produjo un Error al almacenar la Alarma.';
    ERROR_PROCESO = 'Se produjeron Errores durante el Proceso. Se gener� una alarma por cada uno de ellos.';
    MSG_CONFIRMACION      = 'La patente %s se encuentra en estado de seguimiento. �Desea continuar?'; // Rev. 7 SS 931
    //MSG_SQL_CN      = 'SELECT dbo.CONST_CODIGO_CONCESIONARIA_COSTANERA_NORTE()';          // SS_1222_CQU_20150317  //SS_1222_MBE_20141024
    MSG_NO_PUEDE    = 'No puede cambiar la categoria a un tr�nsito de otra Concesionaria';  //SS_1222_MBE_20141024
var
    //CodigoConcesionariaCN,                                                                // SS_1222_CQU_20150317  //SS_1222_MBE_20141024
    CodigoInfractor: Integer;
    TienePersonaRNVM: Boolean;
    f: TfrmLogin;
    PasarReg: Boolean;
    RVMInformation: TRVMInformation;
    Error: AnsiString;
    // Rev. 1 (SS 826)
    EstadoActual: Integer;
    ErroresEnProceso: TStringList;
    ErrorActual: String;
    // Fin Rev. 1 (SS 826)
    FechaPath:AnsiString;           //SS-231-NDR-20110505
begin

    if (txt_Categoria.ValueInt <> StrToInt(cdsObtenerTransitoInfractoresPatente.FieldByName('Categoria').AsString) ) then begin        //SS_1222_MBE_20141024
        //CodigoConcesionariaCN := QueryGetValueInt(DMConnections.BaseCAC, MSG_SQL_CN);                                                // SS_1222_CQU_20150317  //SS_1222_MBE_20141024
        //if cdsObtenerTransitoInfractoresPatente.FieldByName('CodigoConcesionaria').AsInteger <> CodigoConcesionariaCN then begin     // SS_1222_CQU_20150317  //SS_1222_MBE_20141024
        if cdsObtenerTransitoInfractoresPatente.FieldByName('CodigoConcesionaria').AsInteger <> FCodigoConcesionariaNativa then begin  //SS_1147ZZZ_NDR_20150415 SS_1222_NDR_20150406 SS_1222_CQU_20150317  //SS_1222_MBE_20141024
            MsgBox(MSG_NO_PUEDE, Caption, MB_ICONWARNING);                                                                             //SS_1222_MBE_20141024
            Exit;                                                                                                                      //SS_1222_MBE_20141024
        end;                                                                                                                           //SS_1222_MBE_20141024
    end;                                                                                                                               //SS_1222_MBE_20141024

    //if (EsPatenteEnSeguimiento(DMConnection.DMConnections.BaseCAC, txt_Patente.Text) = True) then     // SS_1374A_CQU_20151002 // Rev. 7 SS 931    
    if (cdsObtenerTransitoInfractoresPatente.FieldByName('EnSeguimiento').AsBoolean) then               // SS_1374A_CQU_20151002
        if  MsgBox(Format(MSG_CONFIRMACION,[txt_Patente.Text]), Caption, MB_YESNO) = IDNO then Exit; // Rev. 7 SS 931

    //if FImagenRef = '' then Exit;                                                                     // SS_660_CQU_20121010
    if (FImagenRef = '') and (FCodigoTipoInfraccion = CONST_CODIGO_TIPO_INFRACCION_NORMAL) then Exit;   // SS_660_CQU_20121010
    FIniciando := True;
    PasarReg := True;
    TienePersonaRNVM := False;
    CodigoInfractor := -1;

    ErroresEnProceso := TStringList.Create;

    Screen.Cursor := crHourGlass;
    try
        cdsObtenerTransitoInfractoresPatente.DisableControls;
        cdsObtenerTransitoInfractoresPatente.First;
        while not cdsObtenerTransitoInfractoresPatente.Eof do begin
            if Trim(cdsObtenerTransitoInfractoresPatente.FieldByName('PatenteNueva').AsString) <> '' then begin
                // Rev. 1 (SS 826)
                EstadoActual := cdsObtenerTransitoInfractoresPatente.FieldByName('EstadoTransito').AsInteger;   // SS_1374A_CQU_20151002
                //    ObtenerEstadoTransito(                                                                    // SS_1374A_CQU_20151002
                //        DMConnections.BaseCAC,                                                                // SS_1374A_CQU_20151002
                //        cdsObtenerTransitoInfractoresPatente.FieldByName('NumCorrCA').AsInteger);             // SS_1374A_CQU_20151002
                if EstadoActual = CONST_ESTADO_TRANSITO_POSIBLE_INFRACTOR then begin
                // Fin Rev. 1 (SS 826)
                    ActualizarInfractores.Close;
                    ActualizarInfractores.Parameters.ParamByName('@CodigoInfraccion').Value := -1;
                    ActualizarInfractores.Parameters.ParamByName('@Patente').Value := cdsObtenerTransitoInfractoresPatente.FieldByName('PatenteNueva').AsString;
                    ActualizarInfractores.Parameters.ParamByName('@NumCorrCA').Value := cdsObtenerTransitoInfractoresPatente.FieldByName('NumCorrCA').AsString;
                    ActualizarInfractores.Parameters.ParamByName('@FechaInfraccion').Value := cdsObtenerTransitoInfractoresPatente.FieldByName('FechaHora').Value;
                    ActualizarInfractores.Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;
                    if cdsObtenerTransitoInfractoresPatente.FieldByName('CategoriaNueva').AsString <> '' then
                        ActualizarInfractores.Parameters.ParamByName('@Categoria').Value := StrToInt(cdsObtenerTransitoInfractoresPatente.FieldByName('CategoriaNueva').AsString)
                    else  begin
                    	// el SP devuelve un 0 si la categoria es NULL                                                                                                     // SS_1111_MBE_20130613
                        if cdsObtenerTransitoInfractoresPatente.FieldByName('Categoria').AsString = '0' then  // para evitar error por FK, no existe categoria = 0                  // SS_1111_MBE_20130613
                        	ActualizarInfractores.Parameters.ParamByName('@Categoria').Value := NULL                                                                                // SS_1111_MBE_20130613
                    	else                                                                                                                                                        // SS_1111_MBE_20130613
                    		ActualizarInfractores.Parameters.ParamByName('@Categoria').Value := StrToInt(cdsObtenerTransitoInfractoresPatente.FieldByName('Categoria').AsString);	// SS_1111_MBE_20130613
                	end;                                                                                                                                                            // SS_1111_MBE_20130613

                    if FImagenRef = '-1' then begin
                        ActualizarInfractores.Parameters.ParamByName('@RefNumCorrCA').Value := null;
                        ActualizarInfractores.Parameters.ParamByName('@RefRegistrationAccessibility').Value := null;
                    end else begin
                        ActualizarInfractores.Parameters.ParamByName('@RefNumCorrCA').Value := FImagenRef;
                        //ActualizarInfractores.Parameters.ParamByName('@RefRegistrationAccessibility').Value := 1;                             // SS_1138_CQU_20131009
                        ActualizarInfractores.Parameters.ParamByName('@RefRegistrationAccessibility').Value := FRegistrationAccessibilityRef;   // SS_1138_CQU_20131009
                    end;

                    ActualizarInfractores.Parameters.ParamByName('@TienePersonaRNVM').Value := 0;
                    ActualizarInfractores.Parameters.ParamByName('@ValidacionAutomatica').Value := cdsObtenerTransitoInfractoresPatente.FieldByName('ValidacionAutomatica').Value;
                    ActualizarInfractores.Parameters.ParamByName('@CodigoConcesionaria').Value := FProcesandoConcesionaria; // Rev. (Fase 2)

                    if cdsObtenerTransitoInfractoresPatente.FieldByName('Moroso').AsBoolean then                                                    // SS_660_CQU_20121010
                        ActualizarInfractores.Parameters.ParamByName('@CodigoTipoInfraccion').Value := CONST_CODIGO_TIPO_INFRACCION_POR_MOROSIDAD   // SS_660_CQU_20121010
                    else                                                                                                                            // SS_660_CQU_20121010
                        ActualizarInfractores.Parameters.ParamByName('@CodigoTipoInfraccion').Value := CONST_CODIGO_TIPO_INFRACCION_NORMAL;         // SS_660_CQU_20121010
                    //ActualizarInfractores.Parameters.ParamByName('@CodigoPersona').Value := IIf(FCodigoPersona = 0, null, FCodigoPersona);                                    // SS_1374A_CQU_20151013  // SS_660_CQU_20121010
                    if FCodigoPersona <= 0 then begin                                                                                                                           // SS_1374A_CQU_20151013
                        if cdsObtenerTransitoInfractoresPatente.FieldByName('CodigoPersona').AsInteger > 0 then                                                                 // SS_1374A_CQU_20151013
                            ActualizarInfractores.Parameters.ParamByName('@CodigoPersona').Value := cdsObtenerTransitoInfractoresPatente.FieldByName('CodigoPersona').AsInteger // SS_1374A_CQU_20151013
                        else                                                                                                                                                    // SS_1374A_CQU_20151013
                            ActualizarInfractores.Parameters.ParamByName('@CodigoPersona').Value := null;                                                                       // SS_1374A_CQU_20151013
                    end else                                                                                                                                                    // SS_1374A_CQU_20151013
                        ActualizarInfractores.Parameters.ParamByName('@CodigoPersona').Value := FCodigoPersona;                                                                 // SS_1374A_CQU_20151013

                    try
                        DMConnections.BaseCAC.BeginTrans;
                        ActualizarInfractores.ExecProc;
                        DMConnections.BaseCAC.CommitTrans;

                        if ActualizarInfractores.Parameters.ParamByName('@CodigoInfraccion').Value > CodigoInfractor then
                            CodigoInfractor := ActualizarInfractores.Parameters.ParamByName('@CodigoInfraccion').Value;

                        if ActualizarInfractores.Parameters.ParamByName('@TienePersonaRNVM').Value then
                            TienePersonaRNVM := True;

                    except
                        on e: Exception do begin
                            DMConnections.BaseCAC.RollbackTrans;

                            MsgBoxErr(ERROR_GUARDAR + ERROR_STORENAME, e.Message, Caption, MB_ICONSTOP);
                            PasarReg := False;
                        end;
                    end; // except
                end
                // Rev. 1 (SS 826)
                else begin
                    // Insertamos un evento el la tabla AlarmasEventos y almacenamos el error para mostrar el conjunto de
                    // Errores al operador.
                    ErrorActual :=
                        Format(
                            ERROR_ESTADO,
                            [cdsObtenerTransitoInfractoresPatente.FieldByName('NumCorrCA').AsString,
                             EstadoActual,
                             UsuarioSistema]);

                    ErroresEnProceso.Add(ErrorActual);

                    if not InsertarAlarmaEvento(DmConnections.BaseCAC,
                        CONST_ALARMA_MODULO_GESTION_INFRACCIONES,
                        CONST_ALARMA_SEVERIDAD_3,
                        ErrorActual) then
                            PasarReg := False;
                // Fin Rev. 1 (SS 826)
                end;

                if ObtenerEstadoTransito(   DMConnections.BaseCAC,                                                                                                                    // SS_1374A_CQU_20151020 // SS_1374A_CQU_20151002    //SS-231-NDR-20110505
                                            cdsObtenerTransitoInfractoresPatente.FieldByName('NumCorrCA').AsInteger                                                                   // SS_1374A_CQU_20151020 // SS_1374A_CQU_20151002    //SS-231-NDR-20110505
                                        ) <> QueryGetValueInt(dmconnections.BaseCAC, 'SELECT dbo.CONST_ESTADO_TRANSITO_LIMBO()') then                                                 // SS_1374A_CQU_20151020 // SS_1374A_CQU_20151002    //SS-231-NDR-20110505
                //if EstadoActual <> CONST_ESTADO_TRANSITO_LIMBO then                                                                                                                 // SS_1374A_CQU_20151020 // SS_1374A_CQU_20151002
                begin                                                                                                                                                                   //SS-231-NDR-20110505
                    DateTimeToString(FechaPath, 'yyyy-mm-dd', cdsObtenerTransitoInfractoresPatente.FieldByName('FechaHora').AsDateTime);                                                //SS-231-NDR-20110505
                    ForceDirectories(GoodDir(                                                                                                                                           //SS-231-NDR-20110505
                                                GoodDir(                                                                                                                                //SS-231-NDR-20110505
                                                            GoodDir(                                                                                                                    //SS-231-NDR-20110505
                                                                        FImagePathInfractores                                                                                           //SS-231-NDR-20110505
                                                                   )+FechaPath                                                                                                          //SS-231-NDR-20110505
                                                        )+ IntToStr(cdsObtenerTransitoInfractoresPatente.FieldByName('NumCorrCA').AsInteger mod 1000 )                                  //SS-231-NDR-20110505
                                             )                                                                                                                                          //SS-231-NDR-20110505
                                     );                                                                                                                                                 //SS-231-NDR-20110505
                    //if (FAlmacenaImagenFrontal=1) and Not(ImagenAGrabarFrontal.Picture.Graphic=nil) then                                                                              // SS_1138B_CQU_20140122 //SS-231-NDR-20110505
                    if (FAlmacenaImagenFrontal=1) then                                                                                													// SS_1138B_CQU_20140122
                        //ImagenAGrabarFrontal.Picture.SaveToFile(    GoodDir(                                                                                                          // SS_1135_CQU_20131030 //SS-231-NDR-20110505
                        //                                                    GoodDir(                                                                                                  // SS_1135_CQU_20131030 //SS-231-NDR-20110505
                        //                                                            GoodDir(                                                                                          // SS_1135_CQU_20131030 //SS-231-NDR-20110505
                        //                                                                        FImagePathInfractores                                                                 // SS_1135_CQU_20131030 //SS-231-NDR-20110505
                        //                                                                    )+FechaPath                                                                               // SS_1135_CQU_20131030 //SS-231-NDR-20110505
                        //                                                            ) +                                                                                               // SS_1135_CQU_20131030 //SS-231-NDR-20110505
                        //                                                    IntToStr(cdsObtenerTransitoInfractoresPatente.FieldByName('NumCorrCA').AsInteger mod 1000 )               // SS_1135_CQU_20131030 //SS-231-NDR-20110505
                        //                                                   )+                                                                                                         // SS_1135_CQU_20131030 //SS-231-NDR-20110505
                        //                                            IntToStr(cdsObtenerTransitoInfractoresPatente.FieldByName('NumCorrCA').AsInteger) +                               // SS_1135_CQU_20131030 //SS-231-NDR-20110505
                        //                                            '-' +                                                                                                             // SS_1135_CQU_20131030 //SS-231-NDR-20110505
                        //                                            SufijoImagen[tiFrontal] + '.jpg'                                                                                  // SS_1135_CQU_20131030 //SS-231-NDR-20110505
                        //                                        );                                                                                                                    // SS_1135_CQU_20131030 //SS-231-NDR-20110505
                        GrabarImagenReferencia(1);                                                                                                                                      // SS_1135_CQU_20131030
                    //if (FAlmacenaImagenPosterior=1) and Not(ImagenAGrabarPosterior.Picture.Graphic=nil) then                                                                          // SS_1138B_CQU_20140122  //SS-231-NDR-20110505
                    if (FAlmacenaImagenPosterior=1) then																																// SS_1138B_CQU_20140122
                        //ImagenAGrabarPosterior.Picture.SaveToFile(    GoodDir(                                                                                                        // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        //                                                    GoodDir(                                                                                                  // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        //                                                            GoodDir(                                                                                          // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        //                                                                        FImagePathInfractores                                                                 // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        //                                                                    )+FechaPath                                                                               // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        //                                                            ) +                                                                                               // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        //                                                    IntToStr(cdsObtenerTransitoInfractoresPatente.FieldByName('NumCorrCA').AsInteger mod 1000 )               // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        //                                                   )+                                                                                                         // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        //                                            IntToStr(cdsObtenerTransitoInfractoresPatente.FieldByName('NumCorrCA').AsInteger) +                               // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        //                                            '-' +                                                                                                             // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        //                                            SufijoImagen[tiPosterior] + '.jpg'                                                                                // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        //                                        );                                                                                                                    // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        GrabarImagenReferencia(2);                                                                                                                                      // SS_1135_CQU_20131030
                    //if (FAlmacenaImagenGeneral1=1) and Not(ImagenAGrabarGeneral1.Picture.Graphic=nil) then					                                                        // SS_1138B_CQU_20140122 //SS-231-NDR-20110505
                    if (FAlmacenaImagenGeneral1=1) then																																	// SS_1138B_CQU_20140122
                        //ImagenAGrabarGeneral1.Picture.SaveToFile(    GoodDir(                                                                                                         // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        //                                                    GoodDir(                                                                                                  // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        //                                                            GoodDir(                                                                                          // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        //                                                                        FImagePathInfractores                                                                 // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        //                                                                    )+FechaPath                                                                               // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        //                                                            ) +                                                                                               // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        //                                                    IntToStr(cdsObtenerTransitoInfractoresPatente.FieldByName('NumCorrCA').AsInteger mod 1000 )               // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        //                                                   )+                                                                                                         // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        //                                            IntToStr(cdsObtenerTransitoInfractoresPatente.FieldByName('NumCorrCA').AsInteger) +                               // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        //                                            '-' +                                                                                                             // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        //                                            SufijoImagen[tiOverview1] + '.jpg'                                                                                // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        //                                        );                                                                                                                    // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        GrabarImagenReferencia(4);                                                                                                                                      // SS_1135_CQU_20131030
                    //if (FAlmacenaImagenGeneral2=1) and Not(ImagenAGrabarGeneral2.Picture.Graphic=nil) then                                                           					// SS_1138B_CQU_20140122 //SS-231-NDR-20110505
                    if (FAlmacenaImagenGeneral2=1) then                                                           																		// SS_1138B_CQU_20140122
                        //ImagenAGrabarGeneral2.Picture.SaveToFile(    GoodDir(                                                                                                         // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        //                                                    GoodDir(                                                                                                  // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        //                                                            GoodDir(                                                                                          // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        //                                                                        FImagePathInfractores                                                                 // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        //                                                                    )+FechaPath                                                                               // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        //                                                            ) +                                                                                               // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        //                                                    IntToStr(cdsObtenerTransitoInfractoresPatente.FieldByName('NumCorrCA').AsInteger mod 1000 )               // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        //                                                   )+                                                                                                         // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        //                                            IntToStr(cdsObtenerTransitoInfractoresPatente.FieldByName('NumCorrCA').AsInteger) +                               // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        //                                            '-' +                                                                                                             // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        //                                            SufijoImagen[tiOverview2] + '.jpg'                                                                                // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        //                                        );                                                                                                                    // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        GrabarImagenReferencia(8);                                                                                                                                      // SS_1135_CQU_20131030
                    //if (FAlmacenaImagenGeneral3=1) and Not(ImagenAGrabarGeneral3.Picture.Graphic=nil) then																			// SS_1138B_CQU_20140122 //SS-231-NDR-20110505
                    if (FAlmacenaImagenGeneral3=1) then                                                           																		// SS_1138B_CQU_20140122
                        //ImagenAGrabarGeneral3.Picture.SaveToFile(    GoodDir(                                                                                                         // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        //                                                    GoodDir(                                                                                                  // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        //                                                            GoodDir(                                                                                          // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        //                                                                        FImagePathInfractores                                                                 // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        //                                                                    )+FechaPath                                                                               // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        //                                                            ) +                                                                                               // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        //                                                    IntToStr(cdsObtenerTransitoInfractoresPatente.FieldByName('NumCorrCA').AsInteger mod 1000 )               // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        //                                                   )+                                                                                                         // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        //                                            IntToStr(cdsObtenerTransitoInfractoresPatente.FieldByName('NumCorrCA').AsInteger) +                               // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        //                                            '-' +                                                                                                             // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        //                                            SufijoImagen[tiOverview3] + '.jpg'                                                                                // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        //                                        );                                                                                                                    // SS_1135_CQU_20131030  //SS-231-NDR-20110505
                        GrabarImagenReferencia(16);                                                                                                                                     // SS_1135_CQU_20131030
                    //if (FAlmacenaImagenFrontal2=1) and not (ImagenAGrabarFrontal2.Picture.Graphic=nil) then                                                              				// SS_1138B_CQU_20140122 // SS_1135_CQU_20131030
                    if (FAlmacenaImagenFrontal2=1) then																																	// SS_1138B_CQU_20140122
                        GrabarImagenReferencia(32);                                                                                                                                     // SS_1135_CQU_20131030
                    //if (FAlmacenaImagenPosterior2=1) and not (ImagenAGrabarPosterior2.Picture.Graphic=nil) then																		// SS_1138B_CQU_20140122 // SS_1135_CQU_20131030
                    if (FAlmacenaImagenPosterior2=1) then																																// SS_1138B_CQU_20140122
                        GrabarImagenReferencia(64);                                                                                                                                     // SS_1135_CQU_20131030
                end;                                                                                                                                                                    //SS-231-NDR-20110505

            end; // if

            cdsObtenerTransitoInfractoresPatente.Next;
            end; // while
    finally
        if Assigned(F) then F.Free;
        FIniciando := False;
        cdsObtenerTransitoInfractoresPatente.EnableControls;
        Screen.Cursor := crDefault;
        // Rev. 1 (SS 826)
        if PasarReg then begin
            FUltimaPatenteValidada  := Trim(txt_Patente.Text);   // SS_1374A_CQU_20151013
            FCodigoPersona          := 0;                        // SS_1374A_CQU_20151013
            if ErroresEnProceso.Count > 0 then
                MsgBoxErr(ERROR_PROCESO, ErroresEnProceso.Text, Caption, MB_ICONSTOP);
            FreeAndNil(ErroresEnProceso);
            ReservarTransitosInfractores.Next; // Rev. 7 (Fase 2)
            CargarInfractor;
        end
        else FreeAndNil(ErroresEnProceso);
        // Fin Rev. 1 (SS 826)
    end;
end;

{
    Function Name : btn_CancelarClick
    Parameters : Sender: TObject
    Author:
    Date:

    Revision : 1
        Author: pdominguez
        Date: 17/05/2010
        Description: Infractores Fase 2
            - Se a�ade el Next para pasar de Tr�nsitos a validar
}
procedure TformValidarInfraccion.btn_CancelarClick(Sender: TObject);
begin
    ReservarTransitosInfractores.Next; // Rev. 7 (Fase 2)
	spObtenerPatentesPosibles.Close;   // SS_1374A_CQU_20151002
    CargarInfractor;
end;

procedure TformValidarInfraccion.btn_SiguienteClick(Sender: TObject);
begin
    if cdsObtenerTransitoInfractoresPatente.RecNo <> cdsObtenerTransitoInfractoresPatente.RecordCount then begin
        cdsObtenerTransitoInfractoresPatente.Next
    end else begin
        cdsObtenerTransitoInfractoresPatente.First;
    end;
end;

procedure TformValidarInfraccion.btn_AnteriorClick(Sender: TObject);
begin
    if cdsObtenerTransitoInfractoresPatente.RecNo > 1 then cdsObtenerTransitoInfractoresPatente.Prior
    else if cdsObtenerTransitoInfractoresPatente.RecordCount <> 1 then cdsObtenerTransitoInfractoresPatente.Last;

end;

procedure TformValidarInfraccion.InicializarVariables;
begin
    if Assigned(FImagenActual) then FreeAndNil(FImagenActual);
	if Assigned(FJPG12Frontal) then FreeAndNil(FJPG12Frontal);
	if Assigned(FJPG12Frontal2) then FreeAndNil(FJPG12Frontal2);
	if Assigned(FJPG12Posterior) then FreeAndNil(FJPG12Posterior);
	if Assigned(FJPG12Posterior2) then FreeAndNil(FJPG12Posterior2);
	if Assigned(FImgOverview1) then FreeAndNil(FImgOverview1);
	if Assigned(FImgOverview2) then FreeAndNil(FImgOverview2);
	if Assigned(FImgOverview3) then FreeAndNil(FImgOverview3);
    if Assigned(FImagenActual) then FreeAndNil(FImagenActual);
    // Imagenes Italianas                                                               // SS_1091_CQU_20130516
    if Assigned(FImagenItalianaActual)      then FreeAndNil(FImagenItalianaActual);     // SS_1091_CQU_20130516
    if Assigned(FJPGItalianoFrontal)        then FreeAndNil(FJPGItalianoFrontal);       // SS_1091_CQU_20130516
	if Assigned(FJPGItalianoFrontal2)       then FreeAndNil(FJPGItalianoFrontal2);      // SS_1091_CQU_20130516
	if Assigned(FJPGItalianoPosterior)      then FreeAndNil(FJPGItalianoPosterior);     // SS_1091_CQU_20130516
	if Assigned(FJPGItalianoPosterior2)     then FreeAndNil(FJPGItalianoPosterior2);    // SS_1091_CQU_20130516

    // Cargar la Tabla de valores de confiabilidad para calcular ConfiabilidadTotal.
    FImagenActual       := TJPEGPlusImage.Create;
    FJPG12Frontal       := TJPEGPlusImage.Create;
    FJPG12Frontal2      := TJPEGPlusImage.Create;
    FJPG12Posterior     := TJPEGPlusImage.Create;
    FJPG12Posterior2          := TJPEGPlusImage.Create;
    FImgOverview1             := TBitMap.Create;
    FImgOverview1.PixelFormat := pf24bit;
    FImgOverview2             := TBitMap.Create;
    FImgOverview2.PixelFormat := pf24bit;
    FImgOverview3             := TBitMap.Create;
    FImgOverview3.PixelFormat := pf24bit;

    FTipoImagenActual := tiDesconocida;

    FImagenItalianaActual   := TBitMap.Create;   // SS_1091_CQU_20130516
    FJPGItalianoFrontal     := TBitMap.Create;   // SS_1091_CQU_20130516
    FJPGItalianoFrontal2    := TBitMap.Create;   // SS_1091_CQU_20130516
    FJPGItalianoPosterior   := TBitMap.Create;   // SS_1091_CQU_20130516
    FJPGItalianoPosterior2  := TBitMap.Create;   // SS_1091_CQU_20130516

    FFrmImagenOverview.ClearImagesOverview();
    FFormOverviewCargado := False;
    FFrmImagenOverview.Hide;

    // Inicializamos controles de imagenes
    FShiftOriginal      := -1;
    ImagenAmpliar.Scale	:= 1;
	actRemarcarBordes.Checked := False;
	actSuavizarBordes.Checked := False;
    AjustarTama�oTImagePlus(ImagenAmpliar);    // SS_1374A_CQU_20150924
end;

procedure TformValidarInfraccion.btn_AceptarPatenteClick(Sender: TObject);
//REV.5
resourcestring                                                                                                                             // SS_1374A_CQU_20151002 // SS_1374A_CQU_20150924
    //MSG_PATENTE_NO_VALIDA		= 'La patente ingresada no es v�lida';                                                                     // SS_1374A_CQU_20150924
    //MSG_PATENTE_INCORRECTA		= 'La patente ingresada es incorrecta';                                                                // SS_1374A_CQU_20150924
    //MSG_PATENTE_DIFERENTE       = 'La patente ingresada difiere a la Validada Manualmente y la Detectada, �Reintentar?';                 // SS_1374A_CQU_20150911 // SS_1374_CQU_20150901
    //MSG_PATENTE_DIFERENTE       = 'La patente ingresada difiere a la Validada Manualmente y la Detectada, �Volver a Ingresar Patente?';  // SS_1374A_CQU_20150924 // SS_1374A_CQU_20150911
	MSG_COMODIN                 = 'En la patente existen caracteres comod�n. ' + CRLF                                                      // SS_1374A_CQU_20151002
                                + '�Desea asignar la patente propuesta por el sistema?';                                                   // SS_1374A_CQU_20151002
var
	Patente : string;
    //PatenteDetectada,                                                                                                 // SS_1374A_CQU_20150924  // SS_1374_CQU_20150901
    //PatenteValidada : string;                                                                                         // SS_1374A_CQU_20150924  // SS_1374_CQU_20150901
begin
    //--------------------------REV.5
    //if Trim(txt_Patente.Text) = '' then Exit;
    // Verifico que la patente no contenga allg�n comod�n                                                               // SS_1374A_CQU_20151002
    if TieneComodin(txt_Patente.Text) then begin                                                                        // SS_1374A_CQU_20151002
        if MsgBox(MSG_COMODIN + CRLF, caption, MB_ICONQUESTION + MB_YESNO) <> IDYES then begin                          // SS_1374A_CQU_20151002
            txt_Patente.SetFocus;                                                                                       // SS_1374A_CQU_20151002
            Exit;                                                                                                       // SS_1374A_CQU_20151002
        end else begin                                                                                                  // SS_1374A_CQU_20151002
            txt_Patente.Text    := Trim(FProcesandoPatente);;                                                           // SS_1374A_CQU_20151002
            txt_Patente.Refresh;                                                                                        // SS_1374A_CQU_20151002
        end;                                                                                                            // SS_1374A_CQU_20151002
    end;                                                                                                                // SS_1374A_CQU_20151002
    Patente := Trim(txt_Patente.Text);
    if not PuedeConfirmar then Exit;

    //if Length(Patente) < 5 then begin                                                                                 // SS_1374A_CQU_20150924
    //    MsgBoxBalloon(MSG_PATENTE_NO_VALIDA, Caption, MB_ICONERROR, txt_Patente);                                     // SS_1374A_CQU_20150924
    //    Exit;                                                                                                         // SS_1374A_CQU_20150924
    //end;                                                                                                              // SS_1374A_CQU_20150924

    //if (Length(Patente) = 5) and (Copy(Patente,1,2) <> 'PR') then begin                                               // SS_1374A_CQU_20150924
    //	MsgBoxBalloon(MSG_PATENTE_INCORRECTA, Caption, MB_ICONERROR, txt_Patente);                                      // SS_1374A_CQU_20150924
    //    Exit;                                                                                                         // SS_1374A_CQU_20150924
    //end;                                                                                                              // SS_1374A_CQU_20150924

    //CUIDADO Maxima categoria usada ac�
    //Revision 2
    //if (txt_Categoria.ValueInt < 1) or (txt_Categoria.ValueInt > 4) then Exit;                                        // SS_1374A_CQU_20150924

    //PatenteDetectada := cdsObtenerTransitoInfractoresPatente.FieldByName('PatenteDetectada').AsString;                // SS_1374A_CQU_20150924  // SS_1374_CQU_20150901
    //PatenteValidada := cdsObtenerTransitoInfractoresPatente.FieldByName('PatenteValidada').AsString;                  // SS_1374A_CQU_20150924  // SS_1374_CQU_20150901
    //if (Patente <> PatenteValidada) and (PatenteDetectada = PatenteValidada) then                                     // SS_1374A_CQU_20150924  // SS_1374_CQU_20150901
    //begin                                                                                                             // SS_1374A_CQU_20150924  // SS_1374_CQU_20150901
    //	//if (MsgBox(MSG_PATENTE_DIFERENTE, Caption, MB_RETRYCANCEL + MB_ICONQUESTION) = IDRETRY) then Exit;            // SS_1374A_CQU_20150924// SS_1374A_CQU_20150911  // SS_1374_CQU_20150901
    //	if (MsgBox(MSG_PATENTE_DIFERENTE, Caption, MB_YESNO + MB_ICONQUESTION) = IDYES) then begin                      // SS_1374A_CQU_20150924// SS_1374A_CQU_20150911
    //        txt_Patente.SetFocus;                                                                                     // SS_1374A_CQU_20150924  // SS_1374A_CQU_20150911
    //        Exit;                                                                                                     // SS_1374A_CQU_20150924  // SS_1374A_CQU_20150911
    //    end else begin                                                                                                // SS_1374A_CQU_20150924  // SS_1374A_CQU_20150911
    //        btnSinImgRefClick(Sender);                                                                                // SS_1374A_CQU_20150924  // SS_1374A_CQU_20150911
    //    end;                                                                                                          // SS_1374A_CQU_20150924  // SS_1374A_CQU_20150911
    //end;                                                                                                              // SS_1374A_CQU_20150924  // SS_1374_CQU_20150901

    cdsObtenerTransitoInfractoresPatente.Edit;
    cdsObtenerTransitoInfractoresPatente.FieldByName('PatenteNueva').Value := Patente; //REV.5 txt_Patente.Text;
    cdsObtenerTransitoInfractoresPatente.FieldByName('CategoriaNueva').Value := txt_Categoria.Text;
    cdsObtenerTransitoInfractoresPatente.FieldByName('ValidacionAutomatica').Value := False;
    cdsObtenerTransitoInfractoresPatente.Post;

    Siguiente;
end;

procedure TformValidarInfraccion.actImageOverviewExecute(Sender: TObject);
begin
    if FFrmImagenOverview.Showing then FFrmImagenOverview.Hide
    else if FFormOverviewCargado then FFrmImagenOverview.Show;
end;

procedure TformValidarInfraccion.dbTransitosDrawText(Sender: TCustomDBListEx;
  Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
  var Text: String; var TxtRect: TRect; var ItemWidth: Integer;
  var DefaultDraw: Boolean);
begin
 (*   if (cdsObtenerTransitoInfractoresPatente.FieldByName('PatenteNueva').IsNull) or (Trim(cdsObtenerTransitoInfractoresPatente.FieldByName('PatenteNueva').Value) = '') then begin
        if odSelected in State then
            Sender.Canvas.Font.Color := clWindow
        else
            Sender.Canvas.Font.Color := clWindowText;
    end;
 *)
    if cdsObtenerTransitoInfractoresPatente.fieldByName('NumCorrCA').AsString = FImagenRef then
        if odSelected in State then
            Sender.Canvas.Font.Color := clWindow
        else
            Sender.Canvas.Font.Color := clBlue;

    if Column.FieldName = 'FechaHora' then Text := FormatDateTime('dd/mm/yyy hh:nn', cdsObtenerTransitoInfractoresPatente.fieldByName('FechaHora').AsDateTime);
end;


procedure TformValidarInfraccion.cdsObtenerTransitoInfractoresPatenteAfterScroll(
  DataSet: TDataSet);
ResourceString
    SIN_IMAGEN = '(Sin Imagen)';
    MENSAJE_ERROR = 'A ocurrido un error ejecutando la funci�n %s el detalle del error es %s';
var
    bmp: TBitMap;

  Procedure MostrarImagenBoton(btn: TSpeedButton; aGraphic: TGraphic);
  var
    aRect: TRect;
  begin
      bmp.FreeImage;
      bmp.Assign(aGraphic);
      bmp.Width := btn.Width;
      bmp.Height := btn.Height;
      bmp.TransparentColor := clRed;

      aRect := Rect(0, 0, btn.ClientRect.Right, btn.ClientRect.Bottom);
      bmp.Canvas.StretchDraw(aRect, aGraphic);
      btn.caption := EmptyStr;
      btn.Glyph.Assign(bmp);
      btn.Enabled := true;
  end;

  Procedure NoMostrarImagenBoton(btn: TSpeedButton);
  begin
      btn.Enabled := False;
      btn.Glyph.FreeImage;
      btn.Glyph.ReleaseHandle;
      btn.Glyph := nil;
      btn.Caption := SIN_IMAGEN;
  end;

  procedure RenombrarItalianas(NombreCorto : String; NumCorrCA : Int64; FechaHora : TDateTime; ImagenSeleccionada: TTipoImagen);    // SS_1138_CQU_20131009
  resourcestring                                                                                                                    // SS_1138_CQU_20131009
    MSG_ERROR_RENOMBRAR =   'Se encontraron im�genes overview para este tr�nsito italiano y no se pudieron renombrar';              // SS_1138_CQU_20131009
  var                                                                                                                               // SS_1138_CQU_20131009
    ArchivoPadre : AnsiString;                                                                                                      // SS_1138_CQU_20131009
    Superior, Nueva : String;                                                                                                       // SS_1138_CQU_20131009
  begin                                                                                                                             // SS_1138_CQU_20131009
        ArmarPathPadreTransitosNuevoFormato(FImagePathNFI, NombreCorto, NumCorrCA, FechaHora, ArchivoPadre);                        // SS_1138_CQU_20131009
        // Crear Nombre de Archivos OverView                                                                                        // SS_1138_CQU_20131009
        Superior := GoodDir(ArchivoPadre) + IntToStr(NumCorrCA) + '-' + SufijoImagen[ImagenSeleccionada] + '.jpg';                  // SS_1138_CQU_20131009
        // Crear Nombre de Archivos Frontales 2                                                                                     // SS_1138_CQU_20131009
        if ImagenSeleccionada = tiOverview1 then                                                                                    // SS_1138_CQU_20131009
            Nueva := GoodDir(ArchivoPadre) + IntToStr(NumCorrCA) + '-' + SufijoImagen[tiFrontal2] + '.jpg'                          // SS_1138_CQU_20131009
        else if ImagenSeleccionada = tiOverview2 then                                                                               // SS_1138_CQU_20131009
            Nueva := GoodDir(ArchivoPadre) + IntToStr(NumCorrCA) + '-' + SufijoImagen[tiPosterior2] + '.jpg';                       // SS_1138_CQU_20131009
        try                                                                                                                         // SS_1138_CQU_20131009
            // Renombro si existe OverView1 y no existe Frontal1                                                                    // SS_1138_CQU_20131009
            if FileExists(Superior) and (not FileExists(Nueva)) then RenameFile(Superior, Nueva);                                   // SS_1138_CQU_20131009
        except                                                                                                                      // SS_1138_CQU_20131009
            // No hago nada porque debe ser transparente este cambio.                                                               // SS_1138_CQU_20131009
            on e: exception do begin                                                                                                // SS_1138_CQU_20131009
                MsgBoxErr(MSG_ERROR_RENOMBRAR, e.Message, Caption, MB_ICONERROR);                                                   // SS_1138_CQU_20131009
            end;                                                                                                                    // SS_1138_CQU_20131009
        end;                                                                                                                        // SS_1138_CQU_20131009
  end;                                                                                                                              // SS_1138_CQU_20131009

var
    NumCorrCA: Int64;
    FechaHora: TDateTime;
	Error: TTipoErrorImg;
	DescriError, NombreCorto: AnsiString;                                       //SS-377-NDR-20110607
    DataImage: TDataImage;
    EsItaliano : Boolean;   // SS_1091_CQU_20130516
    ImagenTMP: TObject;		// SS_1147W_CQU_20141215
begin
    if not FIniciando then begin
        txtNoHayImagen.Visible := False;
        bmp := TBitMap.Create;
        try
            try
                Update;
                txt_Patente.Clear;
                Screen.Cursor := crHourGlass;
                //Cargo las imagenes
                NumCorrCA := StrToInt64(cdsObtenerTransitoInfractoresPatente.fieldByName('NumCorrCA').AsString);
                FechaHora := cdsObtenerTransitoInfractoresPatente.fieldByName('FechaHora').Value;
                EsItaliano:= cdsObtenerTransitoInfractoresPatente.FieldByName('EsItaliano').AsBoolean;              // SS_1091_CQU_20130516
                InicializarVariables;
                //NombreCorto := QueryGetValue(DMConnections.BaseCAC,Format('SELECT Concesionarias.NombreCorto FROM Transitos (NOLOCK) INNER JOIN Concesionarias (NOLOCK) ON Concesionarias.CodigoConcesionaria=Transitos.CodigoConcesionaria WHERE NumCorrCA=%d', [NumCorrCA]));   // SS_1374A_CQU_20151002    //SS-377-NDR-20110607
                NombreCorto := cdsObtenerTransitoInfractoresPatente.FieldByName('NombreCorto').AsString;            // SS_1374A_CQU_20151002
                gbDatosValidar.Caption := ' Datos a Validar del Tr�nsito  - NumCorrCA: ' + IntToStr(NumCorrCA);     // SS_1374A_CQU_20151002

                // Renombro las imagenes Italianas, esto se deber�a borrar a futuro									// SS_1138_CQU_20131009
                if EsItaliano then begin																			// SS_1138_CQU_20131009
                    RenombrarItalianas(NombreCorto, NumCorrCA, FechaHora, tiOverview1);                             // SS_1138_CQU_20131009
                    RenombrarItalianas(NombreCorto, NumCorrCA, FechaHora, tiOverview2);                             // SS_1138_CQU_20131009
                end;                                                                                                // SS_1138_CQU_20131009

                ImagenTMP := TObject.Create;                                                                                                                                // SS_1147X_CQU_20141217
                if ObtenerImagenTransito(FImagePathNFI, NombreCorto, NumCorrCA, FechaHora, tiFrontal, ImagenTMP, DataImage, DescriError, Error, FEsKapschFrontal) then      // SS_1147X_CQU_20141217
                begin                                                                                                                                                       // SS_1147X_CQU_20141217
                    if FEsKapschFrontal then begin                                                                                                                          // SS_1147X_CQU_20141217
                        FJPG12Frontal := (ImagenTMP as TJPEGPlusImage);                                                                                                     // SS_1147X_CQU_20141217
                        MostrarImagenBoton(sbThumb0, FJPG12Frontal);                                                                                                        // SS_1147X_CQU_20141217
                    end else begin                                                                                                                                          // SS_1147X_CQU_20141217
                        FJPGItalianoFrontal := (ImagenTMP as TBitmap);                                                                                                      // SS_1147X_CQU_20141217
                        MostrarImagenBoton(sbThumb0, FJPGItalianoFrontal);                                                                                                  // SS_1147X_CQU_20141217
                    end;                                                                                                                                                    // SS_1147X_CQU_20141217
                    FPositionPlate[tifrontal] := Rect(                                                                                                                      // SS_1147X_CQU_20141217
                                                        Min(DataImage.DataImageVR.UpperLeftLPN.X, DataImage.DataImageVR.LowerLeftLPN.X),                                    // SS_1147X_CQU_20141217
                                                        Min(DataImage.DataImageVR.UpperLeftLPN.Y, DataImage.DataImageVR.UpperRigthLPN.Y),                                   // SS_1147X_CQU_20141217
                                                        Max(DataImage.DataImageVR.LowerRigthLPN.X, DataImage.DataImageVR.UpperRigthLPN.X),                                  // SS_1147X_CQU_20141217
                                                        Max(DataImage.DataImageVR.LowerRigthLPN.Y, DataImage.DataImageVR.LowerLeftLPN.Y));                                  // SS_1147X_CQU_20141217
                    sbThumb0.Click;                                                                                                                                         // SS_1147X_CQU_20141217
                end else NoMostrarImagenBoton(sbThumb0);                                                                                                                    // SS_1147X_CQU_20141217

                if ObtenerImagenTransito(FImagePathNFI, NombreCorto, NumCorrCA, FechaHora, tiPosterior, ImagenTMP, DataImage, DescriError, Error, FEsKapschPosterior) then  // SS_1147X_CQU_20141217
                begin                                                                                                                                                       // SS_1147X_CQU_20141217
                    if FEsKapschPosterior then begin                                                                                                                        // SS_1147X_CQU_20141217
                        FJPG12Posterior := (ImagenTMP as TJPEGPlusImage);                                                                                                   // SS_1147X_CQU_20141217
                        MostrarImagenBoton(sbThumb1, FJPG12Posterior);                                                                                                      // SS_1147X_CQU_20141217
                    end else begin                                                                                                                                          // SS_1147X_CQU_20141217
                        FJPGItalianoPosterior := (ImagenTMP as TBitmap);                                                                                                    // SS_1147X_CQU_20141217
                        MostrarImagenBoton(sbThumb1, FJPGItalianoPosterior);                                                                                                // SS_1147X_CQU_20141217
                    end;                                                                                                                                                    // SS_1147X_CQU_20141217
                    FPositionPlate[tiPosterior] := Rect(                                                                                                                    // SS_1147X_CQU_20141217
                                                        Min(DataImage.DataImageVR.UpperLeftLPN.X, DataImage.DataImageVR.LowerLeftLPN.X),                                    // SS_1147X_CQU_20141217
                                                        Min(DataImage.DataImageVR.UpperLeftLPN.Y, DataImage.DataImageVR.UpperRigthLPN.Y),                                   // SS_1147X_CQU_20141217
                                                        Max(DataImage.DataImageVR.LowerRigthLPN.X, DataImage.DataImageVR.UpperRigthLPN.X),                                  // SS_1147X_CQU_20141217
                                                        Max(DataImage.DataImageVR.LowerRigthLPN.Y, DataImage.DataImageVR.LowerLeftLPN.Y));                                  // SS_1147X_CQU_20141217
                    if FTipoImagenActual = tiDesconocida then sbThumb1.Click;                                                                                               // SS_1147X_CQU_20141217
                end else NoMostrarImagenBoton(sbThumb1);                                                                                                                    // SS_1147X_CQU_20141217

    //              para hardcodeo de imagen...
    //            if ObtenerImagenTransito(FImagePath, 730420, EncodeDate(2005,04,18), tiFrontal2, FJPG12Frontal2,
                //if EsItaliano and ObtenerImagenTransito(FImagePathNFI, NombreCorto, NumCorrCA, FechaHora, tiFrontal                                                       // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
                //    , FJPGItalianoFrontal, DataImage, DescriError, Error) then begin                                                                                      // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
                //        MostrarImagenBoton(sbThumb0, FJPGItalianoFrontal);                                                                                                // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
                //        FPositionPlate[tifrontal] := Rect(                                                                                                                // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
                //            Min(DataImage.DataImageVR.UpperLeftLPN.X, DataImage.DataImageVR.LowerLeftLPN.X),                                                              // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
                //            Min(DataImage.DataImageVR.UpperLeftLPN.Y, DataImage.DataImageVR.UpperRigthLPN.Y),                                                             // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
                //            Max(DataImage.DataImageVR.LowerRigthLPN.X, DataImage.DataImageVR.UpperRigthLPN.X),                                                            // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
                //            Max(DataImage.DataImageVR.LowerRigthLPN.Y, DataImage.DataImageVR.LowerLeftLPN.Y));                                                            // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
                //        sbThumb0.Click;                                                                                                                                   // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
                //end else                                                                                                                                                  // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
                //if ObtenerImagenTransito(FImagePathNFI, NombreCorto, NumCorrCA, FechaHora, tiFrontal, FJPG12Frontal,                                                      // SS_1147X_CQU_20141217    //SS-377-NDR-20110607
                //    DataImage, DescriError, Error) then begin                                                                                                             // SS_1147X_CQU_20141217
                //        MostrarImagenBoton(sbThumb0, FJPG12Frontal);                                                                                                      // SS_1147X_CQU_20141217
                //        FPositionPlate[tifrontal] := Rect(                                                                                                                // SS_1147X_CQU_20141217
                //            Min(DataImage.DataImageVR.UpperLeftLPN.X, DataImage.DataImageVR.LowerLeftLPN.X),                                                              // SS_1147X_CQU_20141217
                //            Min(DataImage.DataImageVR.UpperLeftLPN.Y, DataImage.DataImageVR.UpperRigthLPN.Y),                                                             // SS_1147X_CQU_20141217
                //            Max(DataImage.DataImageVR.LowerRigthLPN.X, DataImage.DataImageVR.UpperRigthLPN.X),                                                            // SS_1147X_CQU_20141217
                //            Max(DataImage.DataImageVR.LowerRigthLPN.Y, DataImage.DataImageVR.LowerLeftLPN.Y));                                                            // SS_1147X_CQU_20141217
                //        sbThumb0.Click;                                                                                                                                   // SS_1147X_CQU_20141217
                //end else begin                                                                                                                                            // SS_1147X_CQU_20141217
                //    //MsgBox(Format(MENSAJE_ERROR, ['Obtener Imagen Tr�nsito', Descrierror]), Caption, MB_ICONERROR);                                                     // SS_1147X_CQU_20141217
                //    NoMostrarImagenBoton(sbThumb0);                                                                                                                       // SS_1147X_CQU_20141217
                //end;                                                                                                                                                      // SS_1147X_CQU_20141217

                //if EsItaliano and ObtenerImagenTransito(FImagePathNFI, NombreCorto, NumCorrCA, FechaHora, tiPosterior,                                                    // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
                //    FJPGItalianoPosterior, DataImage, DescriError, Error) then begin                                                                                      // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
                //        MostrarImagenBoton(sbThumb1, FJPGItalianoPosterior);                                                                                              // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
                //        FPositionPlate[tiPosterior] := Rect(                                                                                                              // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
                //            Min(DataImage.DataImageVR.UpperLeftLPN.X, DataImage.DataImageVR.LowerLeftLPN.X),                                                              // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
                //            Min(DataImage.DataImageVR.UpperLeftLPN.Y, DataImage.DataImageVR.UpperRigthLPN.Y),                                                             // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
                //            Max(DataImage.DataImageVR.LowerRigthLPN.X, DataImage.DataImageVR.UpperRigthLPN.X),                                                            // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
                //            Max(DataImage.DataImageVR.LowerRigthLPN.Y, DataImage.DataImageVR.LowerLeftLPN.Y));                                                            // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
                //        if FTipoImagenActual = tiDesconocida then sbThumb1.Click;                                                                                         // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
                //end else                                                                                                                                                  // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
                //if ObtenerImagenTransito(FImagePathNFI, NombreCorto, NumCorrCA, FechaHora, tiPosterior, FJPG12Posterior,                                                  // SS_1147X_CQU_20141217    //SS-377-NDR-20110607
                //    DataImage, DescriError, Error) then begin                                                                                                             // SS_1147X_CQU_20141217
                //        MostrarImagenBoton(sbThumb1, FJPG12Posterior);                                                                                                    // SS_1147X_CQU_20141217
                //        FPositionPlate[tiPosterior] := Rect(                                                                                                              // SS_1147X_CQU_20141217
                //            Min(DataImage.DataImageVR.UpperLeftLPN.X, DataImage.DataImageVR.LowerLeftLPN.X),                                                              // SS_1147X_CQU_20141217
                //            Min(DataImage.DataImageVR.UpperLeftLPN.Y, DataImage.DataImageVR.UpperRigthLPN.Y),                                                             // SS_1147X_CQU_20141217
                //            Max(DataImage.DataImageVR.LowerRigthLPN.X, DataImage.DataImageVR.UpperRigthLPN.X),                                                            // SS_1147X_CQU_20141217
                //            Max(DataImage.DataImageVR.LowerRigthLPN.Y, DataImage.DataImageVR.LowerLeftLPN.Y));                                                            // SS_1147X_CQU_20141217
                //        if FTipoImagenActual = tiDesconocida then sbThumb1.Click;                                                                                         // SS_1147X_CQU_20141217
                //end else begin                                                                                                                                            // SS_1147X_CQU_20141217
                //    //MsgBox(Format(MENSAJE_ERROR, ['Obtener Imagen Tr�nsito', Descrierror]), Caption, MB_ICONERROR);                                                     // SS_1147X_CQU_20141217
                //    NoMostrarImagenBoton(sbThumb1);                                                                                                                       // SS_1147X_CQU_20141217
                //end;                                                                                                                                                      // SS_1147X_CQU_20141217

                if ObtenerImagenTransito(FImagePathNFI, NombreCorto, NumCorrCA, FechaHora, tiOverview1, FImgOverview1,                  //SS-377-NDR-20110607
                    DataImage, DescriError, Error) then begin
                        MostrarImagenBoton(sbThumb2, FImgOverview1);
                        FPositionPlate[tiOverview1] := Rect(
                            Min(DataImage.DataImageVR.UpperLeftLPN.X, DataImage.DataImageVR.LowerLeftLPN.X),
                            Min(DataImage.DataImageVR.UpperLeftLPN.Y, DataImage.DataImageVR.UpperRigthLPN.Y),
                            Max(DataImage.DataImageVR.LowerRigthLPN.X, DataImage.DataImageVR.UpperRigthLPN.X),
                            Max(DataImage.DataImageVR.LowerRigthLPN.Y, DataImage.DataImageVR.LowerLeftLPN.Y));
                        if FTipoImagenActual = tiDesconocida then sbThumb2.Click;
                end else begin
                    //MsgBox(Format(MENSAJE_ERROR, ['Obtener Imagen Tr�nsito', Descrierror]), Caption, MB_ICONERROR);
                    NoMostrarImagenBoton(sbThumb2);
                end;

                if ObtenerImagenTransito(FImagePathNFI, NombreCorto, NumCorrCA, FechaHora, tiOverview2, FImgOverview2,                  //SS-377-NDR-20110607
                    DataImage, DescriError, Error) then begin
                        MostrarImagenBoton(sbThumb3, FImgOverview2);
                        FPositionPlate[tiOverview2] := Rect(
                            Min(DataImage.DataImageVR.UpperLeftLPN.X, DataImage.DataImageVR.LowerLeftLPN.X),
                            Min(DataImage.DataImageVR.UpperLeftLPN.Y, DataImage.DataImageVR.UpperRigthLPN.Y),
                            Max(DataImage.DataImageVR.LowerRigthLPN.X, DataImage.DataImageVR.UpperRigthLPN.X),
                            Max(DataImage.DataImageVR.LowerRigthLPN.Y, DataImage.DataImageVR.LowerLeftLPN.Y));
                        if FTipoImagenActual = tiDesconocida then sbThumb3.Click;
                end else begin
                    //MsgBox(Format(MENSAJE_ERROR, ['Obtener Imagen Tr�nsito', Descrierror]), Caption, MB_ICONERROR);
                    NoMostrarImagenBoton(sbThumb3);
                end;

                if ObtenerImagenTransito(FImagePathNFI, NombreCorto, NumCorrCA, FechaHora, tiOverview3, FImgOverview3,                  //SS-377-NDR-20110607
                    DataImage, DescriError, Error) then begin
                        MostrarImagenBoton(sbThumb4, FImgOverview3);
                        FPositionPlate[tiOverview3] := Rect(
                            Min(DataImage.DataImageVR.UpperLeftLPN.X, DataImage.DataImageVR.LowerLeftLPN.X),
                            Min(DataImage.DataImageVR.UpperLeftLPN.Y, DataImage.DataImageVR.UpperRigthLPN.Y),
                            Max(DataImage.DataImageVR.LowerRigthLPN.X, DataImage.DataImageVR.UpperRigthLPN.X),
                            Max(DataImage.DataImageVR.LowerRigthLPN.Y, DataImage.DataImageVR.LowerLeftLPN.Y));
                        if FTipoImagenActual = tiDesconocida then sbThumb4.Click;
                end else begin
                    //MsgBox(Format(MENSAJE_ERROR, ['Obtener Imagen Tr�nsito', Descrierror]), Caption, MB_ICONERROR);
                    NoMostrarImagenBoton(sbThumb4);
                end;

                if ObtenerImagenTransito(FImagePathNFI, NombreCorto, NumCorrCA, FechaHora,  tiFrontal2, ImagenTMP, DataImage, DescriError, Error, FEsKapschFrontal2) then   // SS_1147X_CQU_20141217
                begin                                                                                                                                                       // SS_1147X_CQU_20141217
                    if FEsKapschFrontal2 then begin                                                                                                                         // SS_1147X_CQU_20141217
                        FJPG12Frontal2 := (ImagenTMP as TJPEGPlusImage);                                                                                                    // SS_1147X_CQU_20141217
                        MostrarImagenBoton(sbThumb5, FJPG12Frontal2);                                                                                                       // SS_1147X_CQU_20141217
                    end else begin                                                                                                                                          // SS_1147X_CQU_20141217
                        FJPGItalianoFrontal2 := (ImagenTMP as TBitmap);                                                                                                     // SS_1147X_CQU_20141217
                        MostrarImagenBoton(sbThumb5, FJPGItalianoFrontal2);                                                                                                 // SS_1147X_CQU_20141217
                    end;                                                                                                                                                    // SS_1147X_CQU_20141217
                                                                                                                                                                            // SS_1147X_CQU_20141217
                    FPositionPlate[tiFrontal2] := Rect(                                                                                                                     // SS_1147X_CQU_20141217
                                                        Min(DataImage.DataImageVR.UpperLeftLPN.X, DataImage.DataImageVR.LowerLeftLPN.X),                                    // SS_1147X_CQU_20141217
                                                        Min(DataImage.DataImageVR.UpperLeftLPN.Y, DataImage.DataImageVR.UpperRigthLPN.Y),                                   // SS_1147X_CQU_20141217
                                                        Max(DataImage.DataImageVR.LowerRigthLPN.X, DataImage.DataImageVR.UpperRigthLPN.X),                                  // SS_1147X_CQU_20141217
                                                        Max(DataImage.DataImageVR.LowerRigthLPN.Y, DataImage.DataImageVR.LowerLeftLPN.Y));                                  // SS_1147X_CQU_20141217
                    if FTipoImagenActual = tiDesconocida then sbThumb5.Click;                                                                                               // SS_1147X_CQU_20141217
                end else NoMostrarImagenBoton(sbThumb5);                                                                                                                    // SS_1147X_CQU_20141217

                if ObtenerImagenTransito(FImagePathNFI, NombreCorto, NumCorrCA, FechaHora, tiPosterior2, ImagenTMP, DataImage, DescriError, Error, FEsKapschPosterior2)     // SS_1147X_CQU_20141217
                then begin                                                                                                                                                  // SS_1147X_CQU_20141217
                    if FEsKapschPosterior2 then begin                                                                                                                       // SS_1147X_CQU_20141217
                        FJPG12Posterior2 := (ImagenTMP as TJPEGPlusImage);                                                                                                  // SS_1147X_CQU_20141217
                        MostrarImagenBoton(sbThumb6, FJPG12Posterior2);                                                                                                     // SS_1147X_CQU_20141217
                    end else begin                                                                                                                                          // SS_1147X_CQU_20141217
                        FJPGItalianoPosterior2 := (ImagenTMP as TBitmap);                                                                                                   // SS_1147X_CQU_20141217
                        MostrarImagenBoton(sbThumb6, FJPGItalianoPosterior2);                                                                                               // SS_1147X_CQU_20141217
                    end;                                                                                                                                                    // SS_1147X_CQU_20141217
                                                                                                                                                                            // SS_1147X_CQU_20141217
                    FPositionPlate[tiPosterior2] := Rect(                                                                                                                   // SS_1147X_CQU_20141217
                                                        Min(DataImage.DataImageVR.UpperLeftLPN.X, DataImage.DataImageVR.LowerLeftLPN.X),                                    // SS_1147X_CQU_20141217
                                                        Min(DataImage.DataImageVR.UpperLeftLPN.Y, DataImage.DataImageVR.UpperRigthLPN.Y),                                   // SS_1147X_CQU_20141217
                                                        Max(DataImage.DataImageVR.LowerRigthLPN.X, DataImage.DataImageVR.UpperRigthLPN.X),                                  // SS_1147X_CQU_20141217
                                                        Max(DataImage.DataImageVR.LowerRigthLPN.Y, DataImage.DataImageVR.LowerLeftLPN.Y));                                  // SS_1147X_CQU_20141217
                    if FTipoImagenActual = tiDesconocida then sbThumb6.Click;                                                                                               // SS_1147X_CQU_20141217
                end else NoMostrarImagenBoton(sbThumb6);                                                                                                                    // SS_1147X_CQU_20141217

                //if EsItaliano and ObtenerImagenTransito(FImagePathNFI, NombreCorto, NumCorrCA, FechaHora,  tiFrontal2,                                                    // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
                //    FJPGItalianoFrontal2, DataImage, DescriError, Error) then begin                                                                                       // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
                //        MostrarImagenBoton(sbThumb5, FJPGItalianoFrontal2);                                                                                               // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
                //        FPositionPlate[tiFrontal2] := Rect(                                                                                                               // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
                //            Min(DataImage.DataImageVR.UpperLeftLPN.X, DataImage.DataImageVR.LowerLeftLPN.X),                                                              // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
                //            Min(DataImage.DataImageVR.UpperLeftLPN.Y, DataImage.DataImageVR.UpperRigthLPN.Y),                                                             // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
                //            Max(DataImage.DataImageVR.LowerRigthLPN.X, DataImage.DataImageVR.UpperRigthLPN.X),                                                            // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
                //             Max(DataImage.DataImageVR.LowerRigthLPN.Y, DataImage.DataImageVR.LowerLeftLPN.Y));                                                           // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
                //        if FTipoImagenActual = tiDesconocida then sbThumb5.Click;                                                                                         // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
                //end else                                                                                                                                                  // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
                //if ObtenerImagenTransito(FImagePathNFI, NombreCorto, NumCorrCA, FechaHora,  tiFrontal2, FJPG12Frontal2,                                                   // SS_1147X_CQU_20141217    //SS-377-NDR-20110607
                //    DataImage, DescriError, Error) then begin                                                                                                             // SS_1147X_CQU_20141217
                //        MostrarImagenBoton(sbThumb5, FJPG12Frontal2);                                                                                                     // SS_1147X_CQU_20141217
                //        FPositionPlate[tiFrontal2] := Rect(                                                                                                               // SS_1147X_CQU_20141217
                //            Min(DataImage.DataImageVR.UpperLeftLPN.X, DataImage.DataImageVR.LowerLeftLPN.X),                                                              // SS_1147X_CQU_20141217
                //             Min(DataImage.DataImageVR.UpperLeftLPN.Y, DataImage.DataImageVR.UpperRigthLPN.Y),                                                            // SS_1147X_CQU_20141217
                //            Max(DataImage.DataImageVR.LowerRigthLPN.X, DataImage.DataImageVR.UpperRigthLPN.X),                                                            // SS_1147X_CQU_20141217
                //             Max(DataImage.DataImageVR.LowerRigthLPN.Y, DataImage.DataImageVR.LowerLeftLPN.Y));                                                           // SS_1147X_CQU_20141217
                //        if FTipoImagenActual = tiDesconocida then sbThumb5.Click;                                                                                         // SS_1147X_CQU_20141217
                //end else begin                                                                                                                                            // SS_1147X_CQU_20141217
                //    //MsgBox(Format(MENSAJE_ERROR, ['Obtener Imagen Tr�nsito', Descrierror]), Caption, MB_ICONERROR);                                                     // SS_1147X_CQU_20141217
                //    NoMostrarImagenBoton(sbThumb5);                                                                                                                       // SS_1147X_CQU_20141217
                //end;                                                                                                                                                      // SS_1147X_CQU_20141217

                //if EsItaliano and ObtenerImagenTransito(FImagePathNFI, NombreCorto, NumCorrCA, FechaHora, tiPosterior2,                                                   // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
                //    FJPGItalianoPosterior2, DataImage, DescriError, Error) then begin                                                                                     // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
                //        MostrarImagenBoton(sbThumb6, FJPGItalianoPosterior2);                                                                                             // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
                //        FPositionPlate[tiPosterior2] := Rect(                                                                                                             // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
                //            Min(DataImage.DataImageVR.UpperLeftLPN.X, DataImage.DataImageVR.LowerLeftLPN.X),                                                              // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
                //            Min(DataImage.DataImageVR.UpperLeftLPN.Y, DataImage.DataImageVR.UpperRigthLPN.Y),                                                             // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
                //            Max(DataImage.DataImageVR.LowerRigthLPN.X, DataImage.DataImageVR.UpperRigthLPN.X),                                                            // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
                //            Max(DataImage.DataImageVR.LowerRigthLPN.Y, DataImage.DataImageVR.LowerLeftLPN.Y));                                                            // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
                //        if FTipoImagenActual = tiDesconocida then sbThumb6.Click;                                                                                         // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
                //end else                                                                                                                                                  // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
                //if ObtenerImagenTransito(FImagePathNFI, NombreCorto, NumCorrCA, FechaHora, tiPosterior2, FJPG12Posterior2,                                                // SS_1147X_CQU_20141217    //SS-377-NDR-20110607
                //    DataImage, DescriError, Error) then begin                                                                                                             // SS_1147X_CQU_20141217
                //        MostrarImagenBoton(sbThumb6, FJPG12Posterior2);                                                                                                   // SS_1147X_CQU_20141217
                //        FPositionPlate[tiPosterior2] := Rect(                                                                                                             // SS_1147X_CQU_20141217
                //            Min(DataImage.DataImageVR.UpperLeftLPN.X, DataImage.DataImageVR.LowerLeftLPN.X),                                                              // SS_1147X_CQU_20141217
                //            Min(DataImage.DataImageVR.UpperLeftLPN.Y, DataImage.DataImageVR.UpperRigthLPN.Y),                                                             // SS_1147X_CQU_20141217
                //            Max(DataImage.DataImageVR.LowerRigthLPN.X, DataImage.DataImageVR.UpperRigthLPN.X),                                                            // SS_1147X_CQU_20141217
                //            Max(DataImage.DataImageVR.LowerRigthLPN.Y, DataImage.DataImageVR.LowerLeftLPN.Y));                                                            // SS_1147X_CQU_20141217
                //        if FTipoImagenActual = tiDesconocida then sbThumb6.Click;                                                                                         // SS_1147X_CQU_20141217
                //end else begin                                                                                                                                            // SS_1147X_CQU_20141217
                //    //MsgBox(Format(MENSAJE_ERROR, ['Obtener Imagen Tr�nsito', Descrierror]), Caption, MB_ICONERROR);                                                     // SS_1147X_CQU_20141217
                //    NoMostrarImagenBoton(sbThumb6);                                                                                                                       // SS_1147X_CQU_20141217
                //end;                                                                                                                                                      // SS_1147X_CQU_20141217

                //Cargo los datos del vehiculo
                lblCategoria.Caption := cdsObtenerTransitoInfractoresPatente.fieldByName('Categoria').AsString;
                lblPatentePropuesta.Caption := FProcesandoPatente;
                //txt_Categoria.ValueInt := cdsObtenerTransitoInfractoresPatente.fieldByName('Categoria').Value;        // SS_1374A_CQU_20150911
                if ((cdsObtenerTransitoInfractoresPatente.fieldByName('CategoriaNueva').Value <> null) and              // SS_1374A_CQU_20150911
                    (cdsObtenerTransitoInfractoresPatente.fieldByName('CategoriaNueva').Value <> ''))                   // SS_1374A_CQU_20150911
                then                                                                                                    // SS_1374A_CQU_20150911
                    txt_Categoria.ValueInt := cdsObtenerTransitoInfractoresPatente.fieldByName('CategoriaNueva').Value  // SS_1374A_CQU_20150911
                else                                                                                                    // SS_1374A_CQU_20150911
                    txt_Categoria.ValueInt := cdsObtenerTransitoInfractoresPatente.fieldByName('Categoria').Value;      // SS_1374A_CQU_20150911

                //txt_Patente.Text := Trim(FProcesandoPatente);                                                         // SS_1374A_CQU_20150911
                FIniciando := True;                                                                                     // SS_1374A_CQU_20150924
                FCambioPatente := False;                                                                                // SS_1374A_CQU_20150924
                if  ((cdsObtenerTransitoInfractoresPatente.fieldByName('PatenteNueva').Value <> null) AND               // SS_1374A_CQU_20150911
                    (cdsObtenerTransitoInfractoresPatente.fieldByName('PatenteNueva').Value <> ''))                     // SS_1374A_CQU_20150911
                then                                                                                                    // SS_1374A_CQU_20150911
                    txt_Patente.Text := Trim(cdsObtenerTransitoInfractoresPatente.fieldByName('PatenteNueva').Value)    // SS_1374A_CQU_20150911
                else                                                                                                    // SS_1374A_CQU_20150911
                    txt_Patente.Text := Trim(FProcesandoPatente);                                                       // SS_1374A_CQU_20150911
                FIniciando := False;                                                                                    // SS_1374A_CQU_20150924

                //Habilito o no los componenetes, deacuerdo a si tiene imagenes
                EnableControlsInContainer(gbImagenAmpliar, FTipoImagenActual <> tiDesconocida);
                chk_VerMascara.Enabled := (FTipoImagenActual <> tiDesconocida);
                btn_AceptarPatente.Enabled := (FTipoImagenActual <> tiDesconocida);
                btnImgRef.Enabled := (FTipoImagenActual <> tiDesconocida) and
                  ((cdsObtenerTransitoInfractoresPatente.FieldByName('PatenteNueva').AsString = '') or
                  (cdsObtenerTransitoInfractoresPatente.FieldByName('PatenteNueva').AsString = cdsObtenerTransitoInfractoresPatente.FieldByName('Patente').AsString));
                txt_Patente.Enabled := (FTipoImagenActual <> tiDesconocida);
                //txt_Categoria.Enabled := (FTipoImagenActual <> tiDesconocida);                                                        // SS_1222_CQU_20150317
                if  (FTipoImagenActual <> tiDesconocida) and                                                                            // SS_1222_CQU_20150317
                    (cdsObtenerTransitoInfractoresPatente.FieldByName('CodigoConcesionaria').AsInteger = FCodigoConcesionariaNativa)    //SS_1147ZZZ_NDR_20150415 SS_1222_NDR_20150406 SS_1222_CQU_20150317
                then txt_Categoria.Enabled := True                                                                                      // SS_1222_CQU_20150317
                else txt_Categoria.Enabled := False;                                                                                    // SS_1222_CQU_20150317
                if not (cdsObtenerTransitoInfractoresPatente.FieldByName('Moroso').AsBoolean) then begin    // SS_660_CQU_20121010
                    btnPatenteExtranjera.Enabled := (FTipoImagenActual <> tiDesconocida);
                    btnPatenteEspecial.Enabled := (FTipoImagenActual <> tiDesconocida);
                end else begin                                                                              // SS_660_CQU_20121010
                    btnPatenteExtranjera.Enabled := False;                                                  // SS_660_CQU_20121010
                    btnPatenteEspecial.Enabled := False;                                                    // SS_660_CQU_20121010
                end;                                                                                        // SS_660_CQU_20121010
                if (FTipoImagenActual = tiDesconocida) then begin
                    ImagenAmpliar.Visible := False;
                    pnlImgPatente.Caption := CAPTION_POS_PATENTE;
                    imgPatente.Picture.Bitmap := Nil;
                end else begin
                    ImagenAmpliar.Visible := True;
                end;
                HabilitarBoton;
                if txt_Patente.Enabled then begin
                    txt_Patente.SetFocus
                end else begin
                    btnNoLegible.SetFocus;
                end;
                AjustarTama�oTImagePlus(ImagenAmpliar);    // SS_1374A_CQU_20150924
            except
                on e: exception do begin
                    MsgBoxErr('Error al pasar al tr�nsito siguiente', e.Message, Caption, MB_ICONERROR);
                end;
            end;
        finally
            bmp.free;
            Screen.Cursor := crDefault;
        end;
        txtNoHayImagen.Visible := FTipoImagenActual = tiDesconocida;
        if txtNoHayImagen.Visible then txtNoHayImagen.Left := ((ImagenAmpliar.Width - txtNoHayImagen.Width) div 2) + ImagenAmpliar.Left;

        if (txtNoHayImagen.Visible)                                                                          // SS_660_CQU_20121010
         and (cdsObtenerTransitoInfractoresPatente.FieldByName('Moroso').AsBoolean)                          // SS_660_CQU_20121010
         and (cdsObtenerTransitoInfractoresPatente.FieldByName('TieneTelevia').AsString = 'Si') then begin   // SS_660_CQU_20121010
            txtMoroso.Left := txtNoHayImagen.Left + 3;                                                       // SS_660_CQU_20121010
            txtMoroso.Visible := True;                                                                       // SS_660_CQU_20121010
        end else                                                                                             // SS_660_CQU_20121010
            txtMoroso.Visible := False;                                                                      // SS_660_CQU_20121010

    end;
end;

procedure TformValidarInfraccion.btnNoLegibleClick(Sender: TObject);
begin
    cdsObtenerTransitoInfractoresPatente.Edit;
    cdsObtenerTransitoInfractoresPatente.FieldByName('PatenteNueva').Value      := NO_LEGIBLE;
    cdsObtenerTransitoInfractoresPatente.FieldByName('CategoriaNueva').Value    := '';
    cdsObtenerTransitoInfractoresPatente.Post;
    //btnSinImgRefClick(Sender);  // SS_1374A_CQU_20151002 // SS_1374A_CQU_20150911
    MarcarSinImagen;              // SS_1374A_CQU_20151002
    ValidacionSemiAutomatica;     // SS_1374A_CQU_20151002
    Siguiente;
end;


procedure TformValidarInfraccion.Siguiente;
begin
    txt_Patente.Clear;
    txt_Categoria.Clear;
    btn_Siguiente.Click;
end;

procedure TformValidarInfraccion.cdsObtenerTransitoInfractoresPatenteAfterPost(
  DataSet: TDataSet);
begin
    // Si cambio la patente, desmarco el transito como imagen de ref.

    if (cdsObtenerTransitoInfractoresPatente.FieldByName('PatenteNueva').AsString <> cdsObtenerTransitoInfractoresPatente.FieldByName('Patente').AsString) and
        (cdsObtenerTransitoInfractoresPatente.FieldByName('PatenteNueva').AsString <> '') and
        (cdsObtenerTransitoInfractoresPatente.FieldByName('NumCorrCA').AsString = FImagenRef) then begin
        FImagenRef := '';
        FRegistrationAccessibilityRef := RegistrationAccessibilityImagen[tiDesconocida]; // SS_1138_CQU_20131009
        lblSetImagen.Caption := DEBE_IMG;
        HabilitarBoton;
    end;

    ValidacionSemiAutomatica;
    HabilitarBoton;
end;

procedure TformValidarInfraccion.chk_VerMascaraClick(Sender: TObject);
begin
    Redibujar(chk_VerMascara.Checked);
end;

procedure TformValidarInfraccion.FormCreate(Sender: TObject);
begin
    Act := TDMActualizarDatosInfractor.Create(Self); // se destruye cuendo se destruye el Owner
end;

procedure TformValidarInfraccion.FormMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    if Cursor = crHourGlass then exit; // Asi no hace nada
end;

procedure TformValidarInfraccion.btnImgRefClick(Sender: TObject);
var                                                                 //SS-231-NDR-20110505
    bmp: TBitmap;                                                   //SS-231-NDR-20110505
    jpg: TJPEGImage;                                                //SS-231-NDR-20110505
    EsItaliano : Boolean;                                           // SS_1091_CQU_20130516
    TipoImagenSeleccionada : TTipoImagen;                           // SS_1138_CQU_20131009
begin
    if FCambioPatente and not FCambioPatenteConfirmado then begin               // SS_1374A_CQU_20150924
        if ReIngresarPatente then Exit;                                         // SS_1374A_CQU_20150924
    end;                                                                        // SS_1374A_CQU_20150924
    FImagenRef :=  cdsObtenerTransitoInfractoresPatente.fieldByName('NumCorrCA').AsString;
    EsItaliano :=  cdsObtenerTransitoInfractoresPatente.FieldByName('EsItaliano').AsBoolean;    // SS_1091_CQU_20130516
    lblSetImagen.Caption := IMG;
    TipoImagenSeleccionada := FTipoImagenActual;                                            //  SS_1138_CQU_20131009
    FRegistrationAccessibilityRef := RegistrationAccessibilityImagen[FTipoImagenActual];    //  SS_1138_CQU_20131009
    ValidacionSemiAutomatica;
    HabilitarBoton;
    dbTransitos.Repaint;

    //Captura la imagen seleccionada, la que se almacenara (segun el estado) al grabar.
    bmp := TBitmap.create;                                          //SS-231-NDR-20110505
    bmp.PixelFormat:=pf24bit;                                       //SS-231-NDR-20110505
    jpg := TJPEGImage.Create;                                       //SS-231-NDR-20110505

    //if EsItaliano and not(FJPGItalianoFrontal.Empty) then begin   // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
    //    bmp.Assign(FJPGItalianoFrontal);                          // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
    //    jpg.Assign(bmp);                                          // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
    //    ImagenAGrabarFrontal.Picture.Assign(jpg);                 // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
    //end else                                                      // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
    //if Not(FJPG12Frontal.Empty) then begin                        // SS_1147X_CQU_20141217    //SS-231-NDR-20110505
    //  bmp.Assign(FJPG12Frontal);                                  // SS_1147X_CQU_20141217    //SS-231-NDR-20110505
    //  jpg.Assign(bmp);                                            // SS_1147X_CQU_20141217    //SS-231-NDR-20110505
    //  ImagenAGrabarFrontal.Picture.Assign(jpg);                   // SS_1147X_CQU_20141217    //SS-231-NDR-20110505
    //end;                                                          // SS_1147X_CQU_20141217    //SS-231-NDR-20110505

    if FEsKapschFrontal then begin                                  // SS_1147X_CQU_20141217
        if Not(FJPG12Frontal.Empty) then begin                      // SS_1147X_CQU_20141217
          bmp.Assign(FJPG12Frontal);                                // SS_1147X_CQU_20141217
          jpg.Assign(bmp);                                          // SS_1147X_CQU_20141217
          ImagenAGrabarFrontal.Picture.Assign(jpg);                 // SS_1147X_CQU_20141217
        end;                                                        // SS_1147X_CQU_20141217
    end else begin                                                  // SS_1147X_CQU_20141217
        if not(FJPGItalianoFrontal.Empty) then begin                // SS_1147X_CQU_20141217
            bmp.Assign(FJPGItalianoFrontal);                        // SS_1147X_CQU_20141217
            jpg.Assign(bmp);                                        // SS_1147X_CQU_20141217
            ImagenAGrabarFrontal.Picture.Assign(jpg);               // SS_1147X_CQU_20141217
        end;                                                        // SS_1147X_CQU_20141217
    end;                                                            // SS_1147X_CQU_20141217

    if FEsKapschPosterior then begin                                // SS_1147X_CQU_20141217
        if Not(FJPG12Posterior.Empty) then begin                    // SS_1147X_CQU_20141217
            bmp.Assign(FJPG12Posterior);                            // SS_1147X_CQU_20141217
            jpg.Assign(bmp);                                        // SS_1147X_CQU_20141217
            ImagenAGrabarPosterior.Picture.Assign(jpg);             // SS_1147X_CQU_20141217
        end;                                                        // SS_1147X_CQU_20141217
    end else begin                                                  // SS_1147X_CQU_20141217
        if not(FJPGItalianoPosterior.Empty) then begin              // SS_1147X_CQU_20141217
            bmp.Assign(FJPGItalianoPosterior);                      // SS_1147X_CQU_20141217
            jpg.Assign(bmp);                                        // SS_1147X_CQU_20141217
            ImagenAGrabarPosterior.Picture.Assign(jpg);             // SS_1147X_CQU_20141217
        end;                                                        // SS_1147X_CQU_20141217
    end;                                                            // SS_1147X_CQU_20141217

    //if EsItaliano and not(FJPGItalianoPosterior.Empty) then begin // SS_1147X_CQU_20141217      // SS_1091_CQU_20130516
    //    bmp.Assign(FJPGItalianoPosterior);                        // SS_1147X_CQU_20141217      // SS_1091_CQU_20130516
    //    jpg.Assign(bmp);                                          // SS_1147X_CQU_20141217      // SS_1091_CQU_20130516
    //    ImagenAGrabarPosterior.Picture.Assign(jpg);               // SS_1147X_CQU_20141217      // SS_1091_CQU_20130516
    //end else                                                      // SS_1147X_CQU_20141217      // SS_1091_CQU_20130516
    //if Not(FJPG12Posterior.Empty) then begin                      // SS_1147X_CQU_20141217      //SS-231-NDR-20110505
    //bmp.Assign(FJPG12Posterior);                                  // SS_1147X_CQU_20141217      //SS-231-NDR-20110505
    //jpg.Assign(bmp);                                              // SS_1147X_CQU_20141217      //SS-231-NDR-20110505
    //ImagenAGrabarPosterior.Picture.Assign(jpg);                   // SS_1147X_CQU_20141217      //SS-231-NDR-20110505
    //end;                                                          // SS_1147X_CQU_20141217      //SS-231-NDR-20110505
                                                                    //SS-231-NDR-20110505
    if Not(FImgOverview1.Empty) then begin                          //SS-231-NDR-20110505
      bmp.Assign(FImgOverview1);                                    //SS-231-NDR-20110505
      jpg.Assign(bmp);                                              //SS-231-NDR-20110505
      ImagenAGrabarGeneral1.Picture.Assign(jpg);                    //SS-231-NDR-20110505
    end;                                                            //SS-231-NDR-20110505
                                                                    //SS-231-NDR-20110505
    if Not(FImgOverview2.Empty) then begin                          //SS-231-NDR-20110505
      bmp.Assign(FImgOverview2);                                    //SS-231-NDR-20110505
      jpg.Assign(bmp);                                              //SS-231-NDR-20110505
      ImagenAGrabarGeneral2.Picture.Assign(jpg);                    //SS-231-NDR-20110505
    end;                                                            //SS-231-NDR-20110505
                                                                    //SS-231-NDR-20110505
    if Not (FImgOverview3.Empty) then begin                         //SS-231-NDR-20110505
      bmp.Assign(FImgOverview3);                                    //SS-231-NDR-20110505
      jpg.Assign(bmp);                                              //SS-231-NDR-20110505
      ImagenAGrabarGeneral3.Picture.Assign(jpg);                    //SS-231-NDR-20110505
    end;                                                            //SS-231-NDR-20110505
                                                                    //SS-231-NDR-20110505
    //if EsItaliano and not(FJPGItalianoFrontal2.Empty) then begin  // SS_1147X_CQU_20141217  // SS_1135_CQU_20131030
    //    bmp.Assign(FJPGItalianoFrontal2);                         // SS_1147X_CQU_20141217  // SS_1135_CQU_20131030
    //    jpg.Assign(bmp);                                          // SS_1147X_CQU_20141217  // SS_1135_CQU_20131030
    //    ImagenAGrabarFrontal2.Picture.Assign(jpg);                // SS_1147X_CQU_20141217  // SS_1135_CQU_20131030
    //end else if Not(FJPG12Frontal2.Empty) then begin              // SS_1147X_CQU_20141217  // SS_1135_CQU_20131030
    //    bmp.Assign(FJPG12Frontal2);                               // SS_1147X_CQU_20141217  // SS_1135_CQU_20131030
    //    jpg.Assign(bmp);                                          // SS_1147X_CQU_20141217  // SS_1135_CQU_20131030
    //    ImagenAGrabarFrontal2.Picture.Assign(jpg);                // SS_1147X_CQU_20141217  // SS_1135_CQU_20131030
    //end;                                                          // SS_1147X_CQU_20141217  // SS_1135_CQU_20131030
	//																// SS_1147X_CQU_20141217  // SS_1135_CQU_20131030
    //if EsItaliano and not(FJPGItalianoPosterior2.Empty) then begin// SS_1147X_CQU_20141217  // SS_1135_CQU_20131030
    //    bmp.Assign(FJPGItalianoPosterior2);                       // SS_1147X_CQU_20141217  // SS_1135_CQU_20131030
    //    jpg.Assign(bmp);                                          // SS_1147X_CQU_20141217  // SS_1135_CQU_20131030
    //    ImagenAGrabarPosterior2.Picture.Assign(jpg);              // SS_1147X_CQU_20141217  // SS_1135_CQU_20131030
    //end else if Not(FJPG12Posterior2.Empty) then begin            // SS_1147X_CQU_20141217  // SS_1135_CQU_20131030
    //    bmp.Assign(FJPG12Posterior2);                             // SS_1147X_CQU_20141217  // SS_1135_CQU_20131030
    //    jpg.Assign(bmp);                                          // SS_1147X_CQU_20141217  // SS_1135_CQU_20131030
    //    ImagenAGrabarPosterior2.Picture.Assign(jpg);              // SS_1147X_CQU_20141217  // SS_1135_CQU_20131030
    //end;                                                          // SS_1147X_CQU_20141217  // SS_1135_CQU_20131030

    if FEsKapschFrontal2 then begin                                 // SS_1147X_CQU_20141217
        if Not(FJPG12Frontal2.Empty) then begin                     // SS_1147X_CQU_20141217
            bmp.Assign(FJPG12Frontal2);                             // SS_1147X_CQU_20141217
            jpg.Assign(bmp);                                        // SS_1147X_CQU_20141217
            ImagenAGrabarFrontal2.Picture.Assign(jpg);              // SS_1147X_CQU_20141217
        end;                                                        // SS_1147X_CQU_20141217
    end else begin                                                  // SS_1147X_CQU_20141217
        if not(FJPGItalianoFrontal2.Empty) then begin               // SS_1147X_CQU_20141217
            bmp.Assign(FJPGItalianoFrontal2);                       // SS_1147X_CQU_20141217
            jpg.Assign(bmp);                                        // SS_1147X_CQU_20141217
            ImagenAGrabarFrontal2.Picture.Assign(jpg);              // SS_1147X_CQU_20141217
        end;                                                        // SS_1147X_CQU_20141217
    end;                                                            // SS_1147X_CQU_20141217

    if FEsKapschPosterior2 then begin                               // SS_1147X_CQU_20141217
        if Not(FJPG12Posterior2.Empty) then begin                   // SS_1147X_CQU_20141217
            bmp.Assign(FJPG12Posterior2);                           // SS_1147X_CQU_20141217
            jpg.Assign(bmp);                                        // SS_1147X_CQU_20141217
            ImagenAGrabarPosterior2.Picture.Assign(jpg);            // SS_1147X_CQU_20141217
        end;                                                        // SS_1147X_CQU_20141217
    end else begin                                                  // SS_1147X_CQU_20141217
        if not(FJPGItalianoPosterior2.Empty) then begin             // SS_1147X_CQU_20141217
            bmp.Assign(FJPGItalianoPosterior2);                     // SS_1147X_CQU_20141217
            jpg.Assign(bmp);                                        // SS_1147X_CQU_20141217
            ImagenAGrabarPosterior2.Picture.Assign(jpg);            // SS_1147X_CQU_20141217
        end                                                         // SS_1147X_CQU_20141217
    end;                                                            // SS_1147X_CQU_20141217

    bmp.Free;                                                       //SS-231-NDR-20110505
    jpg.Free;                                                       //SS-231-NDR-20110505

    // Imagen Frontal                                                           // SS_1138_CQU_20131009
    if TipoImagenSeleccionada = tiFrontal then begin                            // SS_1138_CQU_20131009
        //if EsItaliano then                                                    // SS_1147X_CQU_20141217  // SS_1138_CQU_20131009
        if not FEsKapschFrontal then                                            // SS_1147X_CQU_20141217
            SeleccionarImagen(FJPGItalianoFrontal, TipoImagenSeleccionada)      // SS_1138_CQU_20131009
        else                                                                    // SS_1138_CQU_20131009
            SeleccionarImagen(FJPG12Frontal, TipoImagenSeleccionada);           // SS_1138_CQU_20131009
    // Imagen Frontal 2                                                         // SS_1138_CQU_20131009
    end else if TipoImagenSeleccionada = tiFrontal2 then begin                  // SS_1138_CQU_20131009
        //if EsItaliano then                                                    // SS_1147X_CQU_20141217  // SS_1138_CQU_20131009
        if not FEsKapschFrontal2 then                                           // SS_1147X_CQU_20141217
            SeleccionarImagen(FJPGItalianoFrontal2, TipoImagenSeleccionada)     // SS_1138_CQU_20131009
        else                                                                    // SS_1138_CQU_20131009
            SeleccionarImagen(FJPG12Frontal2, TipoImagenSeleccionada);          // SS_1138_CQU_20131009
    // Imagen Posterior                                                         // SS_1138_CQU_20131009
    end else if TipoImagenSeleccionada = tiPosterior then begin                 // SS_1138_CQU_20131009
        //if EsItaliano then                                                    // SS_1147X_CQU_20141217  // SS_1138_CQU_20131009
        if not FEsKapschPosterior then                                          // SS_1147X_CQU_20141217
            SeleccionarImagen(FJPGItalianoPosterior, TipoImagenSeleccionada)    // SS_1138_CQU_20131009
        else                                                                    // SS_1138_CQU_20131009
            SeleccionarImagen(FJPG12Posterior, TipoImagenSeleccionada);         // SS_1138_CQU_20131009
    // Imagen Posterior 2                                                       // SS_1138_CQU_20131009
    end else if TipoImagenSeleccionada = tiPosterior2 then begin                // SS_1138_CQU_20131009
        //if EsItaliano then                                                    // SS_1147X_CQU_20141217  // SS_1138_CQU_20131009
        if not FEsKapschPosterior2 then                                         // SS_1147X_CQU_20141217
            SeleccionarImagen(FJPGItalianoPosterior2, TipoImagenSeleccionada)   // SS_1138_CQU_20131009
        else                                                                    // SS_1138_CQU_20131009
            SeleccionarImagen(FJPG12Posterior2, TipoImagenSeleccionada);        // SS_1138_CQU_20131009
    end;                                                                        // SS_1138_CQU_20131009
    if (not FCambioPatente or FCambioPatenteConfirmado)                         // SS_1374A_CQU_20150924
    then begin                                                                  // SS_1374A_CQU_20150924
        btn_AceptarPatente.Click;                                               // SS_1374A_CQU_20150924
        if btn_AceptarVehiculo.Enabled then btn_AceptarVehiculo.SetFocus;       // SS_1374A_CQU_20150924
    end;                                                                        // SS_1374A_CQU_20150924
end;

procedure TformValidarInfraccion.btnSinImgRefClick(Sender: TObject);
begin
    //FImagenRef := '-1';                                                               // SS_1374A_CQU_20150924
    //FRegistrationAccessibilityRef := RegistrationAccessibilityImagen[tiDesconocida];  // SS_1374A_CQU_20150924        // SS_1138_CQU_20131009
    //lblSetImagen.Caption := SIN_IMG;                                                  // SS_1374A_CQU_20150924
    MarcarSinImagen;                                                                    // SS_1374A_CQU_20150924
    HabilitarBoton;
    ValidacionSemiAutomatica;                                                           // SS_1374A_CQU_20151002
    if FCambioPatente and not FCambioPatenteConfirmado then begin                       // SS_1374A_CQU_20150924
        if ReIngresarPatente                                                            // SS_1374A_CQU_20150924
        then Exit                                                                       // SS_1374A_CQU_20150924
        else begin                                                                      // SS_1374A_CQU_20150924
            btn_AceptarPatente.Click;                                                   // SS_1374A_CQU_20150924
            if btn_AceptarVehiculo.Enabled then btn_AceptarVehiculo.SetFocus;           // SS_1374A_CQU_20150924
        end;                                                                            // SS_1374A_CQU_20150924
    end;                                                                                // SS_1374A_CQU_20150924
    if (not FCambioPatente or FCambioPatenteConfirmado)                                 // SS_1374A_CQU_20150924
    then begin                                                                          // SS_1374A_CQU_20150924
        btn_AceptarPatente.Click;                                                       // SS_1374A_CQU_20150924
        if btn_AceptarVehiculo.Enabled then btn_AceptarVehiculo.SetFocus;               // SS_1374A_CQU_20150924
    end;                                                                                // SS_1374A_CQU_20150924
    dbTransitos.Repaint;
end;

procedure TformValidarInfraccion.HabilitarBoton;
begin
    btn_AceptarVehiculo.Enabled := (CantidadProcesada >= cdsObtenerTransitoInfractoresPatente.RecordCount)
                                    and (FImagenRef <> '');

    if (cdsObtenerTransitoInfractoresPatente.FieldByName('TieneTelevia').AsString = 'Si')               // SS_660_CQU_20121010
    and (cdsObtenerTransitoInfractoresPatente.FieldByName('Moroso').AsBoolean = True) then begin        // SS_660_CQU_20121010
            btn_AceptarPatente.Enabled := True;                                                         // SS_660_CQU_20121010

            if (cdsObtenerTransitoInfractoresPatente.FieldByName('PatenteNueva').Value <> Null) and     // SS_660_CQU_20121010
                (cdsObtenerTransitoInfractoresPatente.FieldByName('CategoriaNueva').Value <> Null) then // SS_660_CQU_20121010
                    btn_AceptarVehiculo.Enabled := True;                                                // SS_660_CQU_20121010
    end;                                                                                                // SS_660_CQU_20121010

end;

procedure TformValidarInfraccion.btnPatenteExtranjeraClick(
  Sender: TObject);
begin
    cdsObtenerTransitoInfractoresPatente.Edit;
    cdsObtenerTransitoInfractoresPatente.FieldByName('PatenteNueva').Value      := EXTRANJERA;
    cdsObtenerTransitoInfractoresPatente.FieldByName('CategoriaNueva').Value    := '';
    cdsObtenerTransitoInfractoresPatente.Post;
    //btnSinImgRefClick(Sender);  // SS_1374A_CQU_20151002 // SS_1374A_CQU_20150911
    MarcarSinImagen;              // SS_1374A_CQU_20151002
    ValidacionSemiAutomatica;     // SS_1374A_CQU_20151002
    Siguiente;
end;

procedure TformValidarInfraccion.btnPatenteEspecialClick(Sender: TObject);
begin
    cdsObtenerTransitoInfractoresPatente.Edit;
    cdsObtenerTransitoInfractoresPatente.FieldByName('PatenteNueva').Value      := ESPECIAL;
    cdsObtenerTransitoInfractoresPatente.FieldByName('CategoriaNueva').Value    := '';
    cdsObtenerTransitoInfractoresPatente.Post;
    //btnSinImgRefClick(Sender);  // SS_1374A_CQU_20151002 // SS_1374A_CQU_20150911
    MarcarSinImagen;              // SS_1374A_CQU_20151002
    ValidacionSemiAutomatica;     // SS_1374A_CQU_20151002
    Siguiente;
end;

procedure TformValidarInfraccion.cdsObtenerTransitoInfractoresPatenteAfterEdit(
  DataSet: TDataSet);
begin
    if cdsObtenerTransitoInfractoresPatente.FieldByName('PatenteNueva').Text = '' then inc(CantidadProcesada);
end;

procedure TformValidarInfraccion.FormDestroy(Sender: TObject);
begin
    LiberarTransitosInfractores.Parameters.ParamByName('@Usuario').Value := UsuarioSistema;
    LiberarTransitosInfractores.ExecProc;
end;

procedure TformValidarInfraccion.ValidacionSemiAutomatica;
var
    Marca: TBookmark;
    PatenteNueva,CategoriaNueva: String;

    procedure ValidarInfraccionesRestantes;
    begin
        PatenteNueva := cdsObtenerTransitoInfractoresPatente.FieldByName('PatenteNueva').AsString;
        CategoriaNueva := cdsObtenerTransitoInfractoresPatente.FieldByName('CategoriaNueva').AsString;

        FIniciando := True;
        FValidacionSemiAutomatica := True;
        Marca := cdsObtenerTransitoInfractoresPatente.GetBookmark;
        cdsObtenerTransitoInfractoresPatente.DisableControls;
        cdsObtenerTransitoInfractoresPatente.First;
        while not cdsObtenerTransitoInfractoresPatente.Eof do begin
            if cdsObtenerTransitoInfractoresPatente.FieldByName('PatenteNueva').AsString = '' then begin
                cdsObtenerTransitoInfractoresPatente.Edit;
                cdsObtenerTransitoInfractoresPatente.FieldByName('PatenteNueva').Value := PatenteNueva;
                cdsObtenerTransitoInfractoresPatente.FieldByName('CategoriaNueva').Value := CategoriaNueva;
                cdsObtenerTransitoInfractoresPatente.FieldByName('ValidacionAutomatica').Value := True;
                cdsObtenerTransitoInfractoresPatente.Post;
            end;

            cdsObtenerTransitoInfractoresPatente.Next;
        end;
        FIniciando := False;
        cdsObtenerTransitoInfractoresPatente.GotoBookmark(Marca);
        cdsObtenerTransitoInfractoresPatente.EnableControls;
        cdsObtenerTransitoInfractoresPatente.FreeBookmark(Marca);
        FValidacionSemiAutomatica := False;
    end;
begin
    // Si confirmo la patente y selecciono el transico con la imagen de ref, marco los demas transitos con la validacion semi-automatica
    if (chkValidacionSemiAutomatica.Checked) and
       (not FValidacionSemiAutomatica) and
       (cdsObtenerTransitoInfractoresPatente.FieldByName('NumCorrCA').AsString = FImagenRef) and
       (cdsObtenerTransitoInfractoresPatente.FieldByName('PatenteNueva').AsString = cdsObtenerTransitoInfractoresPatente.FieldByName('Patente').AsString) then begin
        ValidarInfraccionesRestantes;
    end;
    if (chkValidacionSemiAutomatica.Checked) and (not FValidacionSemiAutomatica) and
        ((cdsObtenerTransitoInfractoresPatente.FieldByName('PatenteNueva').AsString = EXTRANJERA) or
        (cdsObtenerTransitoInfractoresPatente.FieldByName('PatenteNueva').AsString = ESPECIAL)) then
        ValidarInfraccionesRestantes;
    // Si no selcciona imagen JPL y est� chequeado se debe validar las otras infracciones.  // SS_1374A_CQU_20151002
    if (chkValidacionSemiAutomatica.Checked) and                                            // SS_1374A_CQU_20151002
       (not FValidacionSemiAutomatica) and          										// SS_1374A_CQU_20151002
       (FImagenRef = '-1')                          										// SS_1374A_CQU_20151002
    then                                            										// SS_1374A_CQU_20151002
        ValidarInfraccionesRestantes;               										// SS_1374A_CQU_20151002
end;

{-----------------------------------------------------------------------------
Procedure Name  :   actImagenFrontal1Execute
Date Created    :   10-Octubre-2013
Description     :   Habilita los Botones F1
Firma           :   SS_1138_CQU_20131009
-----------------------------------------------------------------------------}
procedure TformValidarInfraccion.actImagenFrontal1Execute(Sender: TObject);
begin
    sbThumb0Click(sbThumb0);
	sbThumb0.Down := true;
    //Redibujar(False);							// SS_1374A_CQU_20151002
    Redibujar(chk_VerMascara.Checked);			// SS_1374A_CQU_20151002
    CentrarImagen;
    //AjustarTama�oTImagePlus(ImagenAmpliar);	// SS_1374A_CQU_20151002
end;

{-----------------------------------------------------------------------------
Procedure Name  :   actImagenFrontal1Execute
Date Created    :   10-Octubre-2013
Description     :   Habilita los Botones F3
Firma           :   SS_1138_CQU_20131009
-----------------------------------------------------------------------------}
procedure TformValidarInfraccion.actImagenFrontal2Execute(Sender: TObject);
begin
    sbThumb5Click(sbThumb5);
    sbThumb5.down := true;
    //Redibujar(False);                        // SS_1374A_CQU_20151002
    Redibujar(chk_VerMascara.Checked);         // SS_1374A_CQU_20151002
    CentrarImagen;
    AjustarTama�oTImagePlus(ImagenAmpliar);    // SS_1374A_CQU_20151002
end;

{-----------------------------------------------------------------------------
Procedure Name  :   actImagenFrontal1Execute
Date Created    :   10-Octubre-2013
Description     :   Habilita los Botones F2
Firma           :   SS_1138_CQU_20131009
-----------------------------------------------------------------------------}
procedure TformValidarInfraccion.actImagenPosterior1Execute(Sender: TObject);
begin
    sbThumb1Click(sbThumb1);
    sbThumb1.Down := true;
	//Redibujar(False);                        // SS_1374A_CQU_20151002
    Redibujar(chk_VerMascara.Checked);         // SS_1374A_CQU_20151002
    CentrarImagen;
    AjustarTama�oTImagePlus(ImagenAmpliar);    // SS_1374A_CQU_20151002
end;

{-----------------------------------------------------------------------------
Procedure Name  :   actImagenFrontal1Execute
Date Created    :   10-Octubre-2013
Description     :   Habilita los Botones F4
Firma           :   SS_1138_CQU_20131009
-----------------------------------------------------------------------------}
procedure TformValidarInfraccion.actImagenPosterior2Execute(Sender: TObject);
begin
    sbThumb6Click(sbThumb6);
    sbThumb6.down := true;
    //Redibujar(False);                        // SS_1374A_CQU_20151002
    Redibujar(chk_VerMascara.Checked);         // SS_1374A_CQU_20151002
    CentrarImagen;
    AjustarTama�oTImagePlus(ImagenAmpliar);    // SS_1374A_CQU_20151002
end;

{-----------------------------------------------------------------------------
Procedure Name  :   GrabarImagenReferencia
Date Created    :   15-Octubre-2013
Description     :   Graba la imagen de referencia en el directorio
Firma           :   SS_1135_CQU_20131030
-----------------------------------------------------------------------------}
procedure TformValidarInfraccion.GrabarImagenReferencia(RegistrationAccessibilityRef : Integer);
resourcestring
    MSG_ERROR_NO_EXISTE =   'El Archivo %s no existe';
    MSG_ERROR_COPIANDO  =   'No se pudo copiar el archivo %s en el directorio %s';
    MSG_ERROR_ACCESO    =   'No se pudo acceder a la ruta %s';
var
    FechaPath, Directorio,
    NombreCorto, Archivo,
    ArchivoOrigen           : AnsiString;
    TipoImagenSeleccionada  : TTipoImagen;
    //SR                      : TSearchRec; // SS_1138B_CQU_20140122
    NumCorrCA               : Int64;
    FechaHora               : TDateTime;
begin
    // Obtengo los valores a utilizar
    NumCorrCA   := StrToInt64(cdsObtenerTransitoInfractoresPatente.fieldByName('NumCorrCA').AsString);
    FechaHora   := cdsObtenerTransitoInfractoresPatente.fieldByName('FechaHora').Value;
    //NombreCorto := SysUtilsCN.QueryGetStringValue(DMConnections.BaseCAC,Format('SELECT Concesionarias.NombreCorto FROM Transitos (NOLOCK) INNER JOIN Concesionarias (NOLOCK) ON Concesionarias.CodigoConcesionaria=Transitos.CodigoConcesionaria WHERE NumCorrCA=%d', [NumCorrCA]));  // SS_1374A_CQU_20151002
    NombreCorto := cdsObtenerTransitoInfractoresPatente.FieldByName('NombreCorto').AsString;    // SS_1374A_CQU_20151002
    TipoImagenSeleccionada := ObtenerTipoImagenPorRegistratioAccesibility(RegistrationAccessibilityRef);
    DateTimeToString(FechaPath, 'yyyy-mm-dd', cdsObtenerTransitoInfractoresPatente.FieldByName('FechaHora').AsDateTime);
    // Armo el directorio de origen
    ArmarPathPadreTransitosNuevoFormato(FImagePathNFI, NombreCorto, NumCorrCA, FechaHora, ArchivoOrigen);
    // Armo el directorio destino
    //Directorio := GoodDir(GoodDir(GoodDir(FImagePathInfractores) + FechaPath) + IntToStr(cdsObtenerTransitoInfractoresPatente.FieldByName('NumCorrCA').AsInteger mod 1000));		// SS_1138B_CQU_20140122
	Directorio := GoodDir(GoodDir(GoodDir(FImagePathInfractores) + FechaPath) + IntToStr(NumCorrCA mod 1000));																		// SS_1138B_CQU_20140122
    // Intento crear el directorio si es que no existe
    if ForceDirectories(Directorio) then begin
        // Armo el nombre del archivo destino
        //Archivo := IntToStr(cdsObtenerTransitoInfractoresPatente.FieldByName('NumCorrCA').AsInteger) + '-' + SufijoImagen[TipoImagenSeleccionada] + '.jpg';						// SS_1138B_CQU_20140122
		Archivo := IntToStr(NumCorrCA) + '-' + SufijoImagen[TipoImagenSeleccionada] + '.jpg';																						// SS_1138B_CQU_20140122
        // Armo el archivo de origen
        //ArchivoOrigen := ArchivoOrigen + IntToStr(cdsObtenerTransitoInfractoresPatente.FieldByName('NumCorrCA').AsInteger) + '-' + SufijoImagen[TipoImagenSeleccionada] + '.jpg';	// SS_1138B_CQU_20140122
		ArchivoOrigen := ArchivoOrigen + IntToStr(NumCorrCA) + '-' + SufijoImagen[TipoImagenSeleccionada] + '.jpg';																	// SS_1138B_CQU_20140122
        // Verifico que exista previamente el archivo origen
        //if not FileExists (ArchivoOrigen) then raise Exception.Create(Format(MSG_ERROR_NO_EXISTE, [ArchivoOrigen]));																// SS_1138B_CQU_20140122
        if not FileExists (ArchivoOrigen) then Exit;																																// SS_1138B_CQU_20140122
        // Verifico que no exista previamente el archivo destino
        //if FileExists(Directorio + Archivo) then Exit;																															// SS_1138B_CQU_20140122
        // Intento grabar la im�gen
        if Not CopyFile(PChar(ArchivoOrigen), PChar(Directorio + Archivo), False) then raise Exception.Create(Format(MSG_ERROR_COPIANDO, [ArchivoOrigen, Directorio]));
    end else
        raise Exception.Create(Format(MSG_ERROR_ACCESO, [Directorio]));
end;

{-----------------------------------------------------------------------------
Procedure Name  :   MarcarSinImagen
Date Created    :   24-Septiembre-2015
Description     :   Marca el registro "SinImagen"
Firma           :   SS_1374A_CQU_20150924
-----------------------------------------------------------------------------}
procedure TformValidarInfraccion.MarcarSinImagen;
begin
    FImagenRef := '-1';
    FRegistrationAccessibilityRef := RegistrationAccessibilityImagen[tiDesconocida];
    lblSetImagen.Caption := SIN_IMG;
end;

{-----------------------------------------------------------------------------
Procedure Name  :   ReIngresarPatente
Date Created    :   24-Septiembre-2015
Description     :   Verifica que el usuario est� de acuerdo con el cambio de patente que hizo.
Firma           :   SS_1374A_CQU_20150924
-----------------------------------------------------------------------------}
function TformValidarInfraccion.ReIngresarPatente : Boolean;
resourcestring
    MSG_PATENTE_DIFERENTE       = 'La patente ingresada difiere a la Validada Manualmente y la Detectada, �Volver a Ingresar Patente?';
var
    PatenteDetectada,
    PatenteValidada,
    Patente : string;
begin
    Result := False;
    PatenteDetectada    := cdsObtenerTransitoInfractoresPatente.FieldByName('PatenteDetectada').AsString; // Por el P�rtico
    PatenteValidada     := cdsObtenerTransitoInfractoresPatente.FieldByName('PatenteValidada').AsString;  // Por ValidacionManual
    Patente             := txt_Patente.Text;                                                              // Ingresada por Operador
    //if  ((Patente <> PatenteValidada) and (PatenteDetectada <> PatenteValidada)) or       // SS_1374A_CQU_20151013
    //      ((Patente <> PatenteValidada) and (PatenteDetectada = PatenteValidada)) then    // SS_1374A_CQU_20151013
	//begin																					// SS_1374A_CQU_20151013
    if (Patente <> PatenteValidada)                                                         // SS_1374A_CQU_20151013
        and (PatenteDetectada = PatenteValidada)                                            // SS_1374A_CQU_20151013
        and (PatenteDetectada <> '')                                                        // SS_1374A_CQU_20151013
        and (PatenteValidada <> '')                                                         // SS_1374A_CQU_20151013
    then begin																				// SS_1374A_CQU_20151013
    	if (MsgBox(MSG_PATENTE_DIFERENTE, Caption, MB_YESNO + MB_ICONQUESTION) = IDYES) then begin
            FCambioPatenteConfirmado := False;
            Result := True;
        end else begin
            FCambioPatenteConfirmado := True;
            Result := False;
        end;
    end else begin
        FCambioPatenteConfirmado := True;
        Result := False;
    end;
end;

{-----------------------------------------------------------------------------
Procedure Name  :   PuedeConfirmar
Date Created    :   24-Septiembre-2015
Description     :   Verifica que el usuario pueda confirmar la infraccion
Firma           :   SS_1374A_CQU_20150924
-----------------------------------------------------------------------------}
function TformValidarInfraccion.PuedeConfirmar : Boolean;
resourcestring
    MSG_PATENTE_NO_VALIDA		= 'La patente ingresada no es v�lida';
    MSG_PATENTE_INCORRECTA		= 'La patente ingresada es incorrecta';
    MSG_CATEGORIA_MALA          = 'La Categoria no existe';
    MSG_SIN_IMAGEN              = 'No ha indicado la imagen para Juzgado de Polic�a Local,' + CRLF
    //                            + ' se seleccionar� autom�ticamente "Sin Imagen JPL".' + CRLF                        // SS_1374A_CQU_20151019
    //                            + ' �Desea continuar?';                                                              // SS_1374A_CQU_20151019
                                  + ' debe elegir imagen o presionar el bot�n "Sin Imagen JPL"';                       // SS_1374A_CQU_20151019
	MSG_COMODIN                 = 'En la patente existen caracteres comod�n. ';										// SS_1374A_CQU_20151002
var
	Patente : string;
begin
    Patente := Trim(txt_Patente.Text);
    Result := True;
    // Verifico que la patente tenga el largo correcto
    if Length(Patente) < 5 then begin
        MsgBoxBalloon(MSG_PATENTE_NO_VALIDA, Caption, MB_ICONERROR, txt_Patente);
        Result := False;
        Exit;
    end;

    // Verifico que la patente tenga el largo correcto y que no sea provisoria
    if (Length(Patente) = 5) and (Copy(Patente,1,2) <> 'PR') then begin
        MsgBoxBalloon(MSG_PATENTE_INCORRECTA, Caption, MB_ICONERROR, txt_Patente);
        Result := False;
        Exit;
    end;

    // Verifico que la patente no contenga allg�n comod�n                                                           // SS_1374A_CQU_20151002
    if TieneComodin(txt_Patente.Text) then begin                                                                    // SS_1374A_CQU_20151002
        MsgBoxBalloon(MSG_COMODIN, Caption, MB_ICONERROR, txt_Patente);                                             // SS_1374A_CQU_20151002
    end;                                                                                                            // SS_1374A_CQU_20151002

    // Verifico que la patente haya sido cambiada y el operador confirme el cambio
    if FCambioPatente and not FCambioPatenteConfirmado then begin
        if ReIngresarPatente then begin
            Result := False;
            Exit;
        end;
    end;

    // Verifico que la Categoria corresponda a alguna categoria valida
    if txt_Categoria.Enabled and ((txt_Categoria.ValueInt < 1) or (txt_Categoria.ValueInt > 4)) then begin
        MsgBoxBalloon(MSG_CATEGORIA_MALA, Caption, MB_ICONERROR, txt_Categoria);
        Result := False;
        Exit;
    end;

    // Verifico que haya pinchado alguno de los botones de imagenes (Con o Sin)
    //if FImagenRef = '' then begin                                                             // SS_1374A_CQU_20151002
    if (not FCambioPatente) and (FImagenRef = '') then begin                                    // SS_1374A_CQU_20151002
        //if (MsgBox(MSG_SIN_IMAGEN, Caption, MB_YESNO + MB_ICONQUESTION) = IDYES) then begin   // SS_1374A_CQU_20151019
        //    MarcarSinImagen;                                                                  // SS_1374A_CQU_20151019
        //end else begin                                                                        // SS_1374A_CQU_20151019
            MsgBox(MSG_SIN_IMAGEN, Caption, MB_OK + MB_ICONINFORMATION);                        // SS_1374A_CQU_20151019
            Result := False;
            Exit;
        //end;                                                                                  // SS_1374A_CQU_20151019
    //end;																						 // SS_1374A_CQU_20151002
    //end else if (FCambioPatente and FCambioPatenteConfirmado) and (FImagenRef = '') then begin // SS_1374A_CQU_20151013  // SS_1374A_CQU_20151002
    end else if (FCambioPatente) and (FImagenRef = '') then begin    							 // SS_1374A_CQU_20151013
        MarcarSinImagen                                                                          // SS_1374A_CQU_20151002
    end;                                                                                         // SS_1374A_CQU_20151002

    // Mando a habilitar el bot�n de aceptar infraccion
    HabilitarBoton;
end;

{-----------------------------------------------------------------------------
Procedure Name  :   txt_PatenteChange
Date Created    :   24-Septiembre-2015
Description     :   Se indica que se realiz� un cambio de Patente
Firma           :   SS_1374A_CQU_20150924
-----------------------------------------------------------------------------}
procedure TformValidarInfraccion.txt_PatenteChange(Sender: TObject);
begin
    if Length(txt_Patente.Text) > 4 then begin															    
        try
            spObtenerPatentesPosibles.Close;
            spObtenerPatentesPosibles.Connection := DMConnections.BaseCOP;                                  // TASK_166_MGO_20170404
            spObtenerPatentesPosibles.ProcedureName := 'ObtenerPatentesPosibles';                           // TASK_166_MGO_20170404
            spObtenerPatentesPosibles.Parameters.Refresh;                                                   // TASK_166_MGO_20170404
            spObtenerPatentesPosibles.Parameters.ParamByName('@Patente').Value := Trim(txt_Patente.text);
            spObtenerPatentesPosibles.Parameters.ParamByName('@Categoria').Value := Null;                   // TASK_166_MGO_20170404
            spObtenerPatentesPosibles.Open;                                                                 
        except                                                                                              
            on e: exception do begin
                MsgBoxBalloon(  FORMAT(MSG_ERROR_OBTENER_DATOS, ['Posibles Patentes']),
                                Caption,                                                                    
                                MB_ICONERROR,                                                               
                                dblPatentesPosibles);                                                       
            end;                                                                                            
        end;                                                                                                
    end;																								    
    if Length(txt_Patente.Text) > 5 then begin
        if not FIniciando then begin
            FCambioPatente := True;
            if FCambioPatenteConfirmado then FCambioPatenteConfirmado := False;
        end;
    end;
end;

{-----------------------------------------------------------------------------
Procedure Name  :   dblPatentesPosiblesDblClick
Date Created    :   07-Octubre-2015
Description     :   Se cambia la patente por la seleccionada
Firma           :   SS_1374A_CQU_20151002
-----------------------------------------------------------------------------}
procedure TformValidarInfraccion.dblPatentesPosiblesDblClick(Sender: TObject);
var
    PatentePosible      :   string;
    CategoriaPosible    : Int64;
begin
	if spObtenerPatentesPosibles.IsEmpty then exit;

    try
        PatentePosible      :=  Trim(spObtenerPatentesPosibles.FieldByName('Patente').AsString);
        CategoriaPosible    :=  spObtenerPatentesPosibles.FieldByName('CodigoCategoria').AsInteger;
    except
        on e: exception do begin
            PatentePosible      := Trim(txt_Patente.Text);
            CategoriaPosible    := txt_Categoria.ValueInt;
        end;
    end;
	// Actualizamos los campos patente y categoria s�lo si corresponde
    if (PatentePosible <> '') and (PatentePosible <> Trim(txt_Patente.Text)) then begin
        txt_Patente.Text := PatentePosible;
        FCambioPatenteConfirmado := False;
    end;
    if (CategoriaPosible > 0) and (CategoriaPosible <> txt_Categoria.ValueInt) then
        txt_Categoria.ValueInt  := CategoriaPosible;
end;

{-----------------------------------------------------------------------------
Procedure Name  :   PatenteSinComodin
Date Created    :   07-Octubre-2015
Description     :   Controla que en la Patente ingresada no figuren caracteres comod�n
                    Devuelve true si encuentra alguno de los comodines
                        COMODIN_CHAR = '&'
                        COMODIN_INT	 = '#'
                        COMODIN_GRAL = '*'
Firma           :   SS_1374A_CQU_20151002
-----------------------------------------------------------------------------}
function TformValidarInfraccion.TieneComodin(Patente: AnsiString): Boolean;
begin
    if Pos(COMODIN_CHAR, Patente) > 0       then Result := True
    else if Pos(COMODIN_INT, Patente) > 0   then Result := True
    else if Pos(COMODIN_GRAL, Patente)> 0   then Result := True
    else Result := False;
end;

end.
