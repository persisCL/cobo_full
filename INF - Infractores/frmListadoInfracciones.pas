{
    Unit Name: frmListadoInfracciones.pas
        Author:  mtraversa
        Date  : 07/04/2005
        Language: ES-AR
        Description:

    Revision : 1
        Author : pdominguez
        Date   : 13/05/2010
        Description : Infractore Fase 2
            - Se modificaron / a�adieron los siguientes objetos / Varibales / Constantes
                cbConcesionaria: TVariantComboBox.
                Se a�ade el par�metro @CodigoConcesionaria al objeto spObtenerInfracciones: TADOStoredProc.
                Se crea el objeto spObtenerFechaMinimaTransitoPosibleInfractorConcesionaria: TADOStoredProc.
            - Se modificaron / a�adieron los siguientes procediminentos/funciones
                btn_LimpiarClick,
                btn_FiltrarClick,
                cbConcesionariasChange,
                Inicializar,
                btnValidarInfraccionClick

    Revision : 2
        Author : pdominguez
        Date   : 03/06/2010
        Description : Infractore Fase 2
            - Se modificaron / a�adieron los siguientes procediminentos/funciones
                btn_LimpiarClick,
                cbConcesionariasChange,

    Revision : 3
        Author   : pdominguez
        Date     : 06/06/2010
        Descrption: Infractores Fase 2
            - se modificaron los siguientes objetos:
                Panel1: TPanel - Se borra el valor de la propiedad Caption.

            - se modificaron los siguientes Procedimientos / Funciones:
                btn_FiltrarClick

    Author      :   CQuezadaI
    Date        :   10/10/2012
    Firma       :   SS_660_CQU_20121010
    Description :   Modifica la l�gica para considerar los nuevos campos de CodigoTipoInfraccion y CodigoPersona

    Author      :   CQuezadaI
    Date        :   30/09/2013
    Firma       :   SS_660_CQU_20130822
    Descripcion :   Se modifica un "Active" en btn_FiltrarClick ya que se ca�a al volver a filtrar.

    Firma       : SS_1147_MCA_20150325
    Descripcion : se agrega validacion si concesionaria es mayor q 0, entonces ejecuta el SP

    Firma       : SS_1147_NDR_20141216
    Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

    }
unit frmListadoInfracciones;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, ListBoxEx, DBListEx, StdCtrls, DPSControls, Validate,
  DateEdit, ExtCtrls, DmiCtrls, RStrings, PeaProcs, PeaTypes, Util, UtilProc,
  UtilDB, DateUtils, ImgList, frmValidarInfraccion, Dmconnection,
  VariantComboBox;

type
  TFormListadoInfracciones = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    Label8: TLabel;
    btn_Filtrar: TButton;
    btn_Limpiar: TButton;
    txt_Patente: TEdit;
    Panel2: TPanel;
    dblInfracciones: TDBListEx;
    Panel3: TPanel;
    Panel4: TPanel;
    lbl_Mostar: TLabel;
    BtnSalir: TButton;
    spObtenerInfracciones: TADOStoredProc;
    dsObtenerInfracciones: TDataSource;
    Label2: TLabel;
    txt_FechaDesde: TDateEdit;
    txt_FechaHasta: TDateEdit;
    Label3: TLabel;
    btnValidarInfraccion: TButton;
    Label4: TLabel;
    txt_CantInfracciones: TNumericEdit;
    cbConcesionarias: TVariantComboBox;
    lblConcesionaria: TLabel;
    spObtenerFechaMinimaTransitoPosibleInfractorConcesionaria: TADOStoredProc;
    procedure btn_LimpiarClick(Sender: TObject);
    procedure btn_FiltrarClick(Sender: TObject);
    procedure spObtenerInfraccionesAfterOpen(DataSet: TDataSet);
    procedure txt_FechaDesdeExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    procedure dblInfraccionesColumnHeaderClick(Sender: TObject);
    procedure btnValidarInfraccionClick(Sender: TObject);
    procedure cbConcesionariasChange(Sender: TObject);
  private
    { Private declarations }
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
  public
    function Inicializar(MDIChild: Boolean): Boolean;
    { Public declarations }
  end;

var
  FormListadoInfracciones: TFormListadoInfracciones;

implementation

{$R *.dfm}

resourcestring
    MSG_ERROR_INICIALIZACION = 'Error al inicializar Infracciones';

{
    Procedure Name : btn_LimpiarClick
        Parameters : Sender: TObject
        Author:
        Date:

    Revision : 2
        Author: pdominguez
        Date: 03/06/2010
        Description: Infractores Fase 2
            - Se limpia la patente, la rejilla de datos y se colocan las fechas
            iniciales sin concesionaria seleccionada.
}
procedure TFormListadoInfracciones.btn_LimpiarClick(Sender: TObject);
begin
    txt_Patente.Clear;
    spObtenerInfracciones.Close;
    cbConcesionarias.ItemIndex := 0;
    cbConcesionariasChange(Nil);
end;

{
    Procedure Name : btn_FiltrarClick
        Parameters : Sender: TObject
        Author:
        Date:

    Revision : 1
        Author: pdominguez
        Date: 13/05/2010
        Description: Infractores Fase 2
            - Se coloca la apertura del sp spObtenerInfracciones dentro de un bloque
            protegido de excepciones.
            - Se a�ade el par�metro @CodigoConcesionaria al sp spObtenerInfracciones.
            - Se elimina la funci�n AcomodarFecha, pues las fechas ya se setan en
            los SP para Principio y Final de Dia.

    Revision : 3
        Author: pdominguez
        Date: 06/06/2010
        Description: Infractores Fase 2
            - Se atienden las sugerencias de Test KTC CL:
                - Se a�ade un mensaje de aviso en el caso de que la consulta no devuelva resultados.
}
procedure TFormListadoInfracciones.btn_FiltrarClick(Sender: TObject);
    Resourcestring
        MSG_FALTA_FECHA_DESDE = 'Ingrese una fecha desde para el filtro';
        MSG_FALTA_FECHA_HASTA = 'Ingrese una fecha hasta para el filtro';
        MSG_SIN_RESULTADOS = 'La Consulta con los filtros especificados, no retorn� datos.';
        MSG_SIN_RESULTADOS_CAPTION = ' Aviso resultado Consulta';
begin

    if (txt_CantInfracciones.ValueInt < 0) then begin
        MsgBoxBalloon(MSG_VALIDAR_TOP_LISTA_CONVENIO_MIN, Caption, MB_ICONSTOP, txt_CantInfracciones);
        txt_CantInfracciones.SetFocus;
        Exit;
    end;
	//Revision 1:Se verifican que las fechas no sean nulas.
    if (txt_FechaDesde.Date = NullDate) then begin
        MsgBoxBalloon(MSG_FALTA_FECHA_DESDE, Caption, MB_ICONSTOP, txt_FechaDesde);
        txt_FechaDesde.SetFocus;
        Exit;
    end;

    if (txt_FechaHasta.Date = NullDate) then begin
        MsgBoxBalloon(MSG_FALTA_FECHA_HASTA, Caption, MB_ICONSTOP, txt_FechaHasta);
        txt_FechaHasta.SetFocus;
        Exit;
    end;

    if (txt_FechaDesde.Date <> NullDate) and (txt_FechaHasta.Date <> NullDate) and (txt_FechaDesde.Date > txt_FechaHasta.Date) then begin
        MsgBoxBalloon(MSG_VALIDAR_ORDEN_FECHA, Caption, MB_ICONSTOP, txt_FechaDesde);
        txt_FechaDesde.SetFocus;
        Exit;
    end;
    // Rev. 1 (Infractores Fase 2)
    try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        dsObtenerInfracciones.DataSet := Nil;

        with spObtenerInfracciones do try
            if Active then Close;   // SS_660_CQU_20130822
            Parameters.ParamByName('@FechaDesde').Value           := txt_FechaDesde.Date;
            Parameters.ParamByName('@FechaHasta').Value           := txt_FechaHasta.Date;
            Parameters.ParamByName('@Patente').Value              := iif(Trim(txt_Patente.Text) = '', Null, Trim(txt_Patente.Text));
            Parameters.ParamByName('@CantidadInfracciones').Value := iif(trim(txt_CantInfracciones.Text) = '', Null, txt_CantInfracciones.ValueInt);
            MaxRecords := iif(trim(txt_CantInfracciones.Text) = '', 0, txt_CantInfracciones.ValueInt);
            Parameters.ParamByName('@CodigoConcesionaria').Value  := AsignarValorComboEstado(cbConcesionarias);
            //if Active then Close; // SS_660_CQU_20130822
            Open;
        Except
            on e:Exception do begin
                if Active then Close;
                MsgBoxErr(MSG_ERROR_INICIALIZACION, e.Message, Caption, MB_ICONERROR);
            end;
        end;
    finally
        dsObtenerInfracciones.DataSet := spObtenerInfracciones;
        Screen.Cursor := crDefault;
        Application.ProcessMessages;

        if spObtenerInfracciones.IsEmpty then MsgBox(MSG_SIN_RESULTADOS, MSG_SIN_RESULTADOS_CAPTION, MB_ICONINFORMATION);
    end;
    // Fin Rev. 1 (Infractores Fase 2)
    dblInfracciones.SetFocus;
end;

procedure TFormListadoInfracciones.spObtenerInfraccionesAfterOpen(
  DataSet: TDataSet);
resourcestring
    STR_CANT_INFRACCIONES = 'Cantidad de Infracciones: %d.';
var
    CantRegistros: Integer;
begin
    CantRegistros := 0;
    if spObtenerInfracciones.Active then begin
        CantRegistros := spObtenerInfracciones.RecordCount;
        if spObtenerInfracciones.Sort = EmptyStr then
            spObtenerInfracciones.Sort := 'FechaHora';
    end;
    lbl_Mostar.Caption := Format(STR_CANT_INFRACCIONES, [CantRegistros]);
    btnValidarInfraccion.Enabled := (CantRegistros > 0);
end;

procedure TFormListadoInfracciones.txt_FechaDesdeExit(Sender: TObject);
begin
    if (txt_FechaHasta.Date = NullDate) then
        txt_FechaHasta.Date := txt_FechaDesde.Date;
end;

{
    Procedure Name: cbConcesionariasChange
    Parameters: Sender: TObject
    Author: pdominguez
    Date Created: 08/05/2010

    Description: Infractores Fase 2
        - Busca la fecha m�s antigua con Tr�nsitos Posibles Infractores,
        seg�n la concesionaria seleccionada.

    Revision : 2
        Author: pdominguez
        Date: 03/06/2010
        Description: Infractores Fase 2
            - Se muestra un aviso en el caso de que la concesionaria seleccionada
            no tenga tr�nsitos posibles infractores.
}
procedure TFormListadoInfracciones.cbConcesionariasChange(Sender: TObject);
    ResourceString
        MSG_SIN_TRANSITOS_POSIBLE_INFRACTOR = 'La Concesionaria seleccionada NO tiene Tr�nsitos en estado Posible Infractor.';
        MSG_SIN_TRANSITOS_AVISO = ' Aviso Concesionaria Seleccionada';
begin
    try
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        if spObtenerInfracciones.Active then spObtenerInfracciones.Close;

        with spObtenerFechaMinimaTransitoPosibleInfractorConcesionaria do begin
            if Active then Close;

            if AsignarValorComboEstado(cbConcesionarias)>0 then                 //SS_1147_MCA_20150325
            begin                                                               //SS_1147_MCA_20150325
                Parameters.ParamByName('@CodigoConcesionaria').Value := AsignarValorComboEstado(cbConcesionarias);
                Parameters.ParamByName('@FechaHora').Value := NULL;
                ExecProc;

                if Parameters.ParamByName('@FechaHora').Value = NULL then begin
                    txt_FechaDesde.Clear;
                    txt_FechaHasta.Date := Date;
                    MsgBox(MSG_SIN_TRANSITOS_POSIBLE_INFRACTOR, MSG_SIN_TRANSITOS_AVISO, MB_ICONINFORMATION);
                end
                else begin
                    txt_FechaDesde.Date := Parameters.ParamByName('@FechaHora').Value;
                    if txt_FechaDesde.Date + 30 < Date then
                        txt_FechaHasta.Date := txt_FechaDesde.Date + 30
                    else txt_FechaHasta.Date := Date;
                end;
            end;                                                                //SS_1147_MCA_20150325
        end;
    finally
        Screen.Cursor := crDefault;
        Application.ProcessMessages;
    end;
end;


procedure TFormListadoInfracciones.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    spObtenerInfracciones.Close;
    Action := caFree;
    FormListadoInfracciones := nil;
end;

procedure TFormListadoInfracciones.BtnSalirClick(Sender: TObject);
begin
    Close;
end;

{
    Function Name : Inicializar
    Parameters : Sender: TObject
    Author:
    Date:

    Revision : 1
        Author: pdominguez
        Date: 16/05/2010
        Description: Infractores Fase 2
            - Inicializamos el combo de concesionarias.
            - Buscamos la minima fecha con posibles infractores confirmados, para
            colocarla como fecha desde.
}
function TFormListadoInfracciones.Inicializar(MDIChild: Boolean): Boolean;
var
    S: TSize;
begin
    try
        FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
        FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
        FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
        FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
        Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216
        Update;
        if MDIChild then begin
            S := GetFormClientSize(Application.MainForm);
            SetBounds(0, 0, S.cx, S.cy);
        end else begin
            FormStyle := fsNormal;
            Visible := False;
        end;

        //btn_Limpiar.Click;

        // INICIO : TASK_157_MGO_20170321
        if ObtenerParametroGeneralMulticoncesionHabilitada() then
            CargarComboConcesionarias(DMConnections.BaseCAC, cbConcesionarias, True)
        else begin
            lblConcesionaria.Visible := False;
            cbConcesionarias.Visible := False;      
            dblInfracciones.Columns[1].Width := 0;
        end;
        // FIN : TASK_157_MGO_20170321

        with spObtenerFechaMinimaTransitoPosibleInfractorConcesionaria do begin
            Parameters.ParamByName('@CodigoConcesionaria').Value := NULL;
            Parameters.ParamByName('@FechaHora').Value := NULL;
            ExecProc;

            if Parameters.ParamByName('@FechaHora').Value = NULL then begin
                txt_FechaDesde.Clear;
                txt_FechaHasta.Date := Date;
            end
            else begin
                txt_FechaDesde.Date := Parameters.ParamByName('@FechaHora').Value;
                if txt_FechaDesde.Date + 30 < Date then
                    txt_FechaHasta.Date := txt_FechaDesde.Date + 30
                else txt_FechaHasta.Date := Date;
            end;
        end;
        // Fin Rev. 1 (Fase 2)

        spObtenerInfraccionesAfterOpen(spObtenerInfracciones);
        Result := True;
    except
        on e: Exception do begin
            MsgBoxErr(MSG_ERROR_INICIALIZACION, E.Message, Caption, MB_ICONSTOP);
    		Result := false;
        end;
    end;
end;

procedure TFormListadoInfracciones.dblInfraccionesColumnHeaderClick(
  Sender: TObject);
var
    i: Integer;
begin
    if not spObtenerInfracciones.Active then
        Exit;

    for i := 0 to dblInfracciones.Columns.Count-1 do
        if dblInfracciones.Columns[i] <> Sender then
            dblInfracciones.Columns[i].Sorting := csNone;

    case TDBListExColumn(Sender).Sorting of
        csNone, csDescending :
            begin
                TDBListExColumn(Sender).Sorting := csAscending;
                spObtenerInfracciones.Sort := TDBListExColumn(Sender).FieldName;
            end;
        csAscending :
            begin
                TDBListExColumn(Sender).Sorting := csDescending;
                spObtenerInfracciones.Sort := TDBListExColumn(Sender).FieldName + ' DESC';
            end;
    end;

end;
{
    Function Name : btnValidarInfraccionClick
    Parameters : Sender: TObject
    Author:
    Date:

    Revision : 1
        Author: pdominguez
        Date: 17/05/2010
        Description: Infractores Fase 2
            - Se a�aden el parametro CodigoConcesionaria a la llamada
            al m�todo inicializar del form de validaci�n.
}
procedure TFormListadoInfracciones.btnValidarInfraccionClick(Sender: TObject);
var
    F: TformValidarInfraccion;
begin
	if FindFormOrCreate(TformValidarInfraccion, f) then begin
		f.Show
    end
	else begin
		if not f.Inicializar(
                                Trim(spObtenerInfracciones.FieldByName('Patente').AsString),
                                spObtenerInfracciones.FieldByName('FechaHora').AsDateTime,
                                spObtenerInfracciones.FieldByName('CodigoConcesionaria').AsInteger,     // SS_660_CQU_20121010
                                False,                                                                  // SS_660_CQU_20121010
                                spObtenerInfracciones.FieldByName('CodigoTipoInfraccion').AsInteger,    // SS_660_CQU_20121010
                                spObtenerInfracciones.FieldByName('CodigoPersona').AsInteger            // SS_660_CQU_20121010
                            ) then
            f.Release
	end;
end;

end.
