{
    File Name   : frmEdicionInfraccion
    Author      :
    Date Created:
    Language    : ES-AR
    Description : Permite la Edici�n de una Infracci�n. En esta versi�n solo permite la
            edicion del estado y el cambio de la img de Reg.

    Revision : 1
        Author: pdominguez
        Date: 09/05/2010
        Description: Infracciones Fase 2
            - Se modificaron / a�adieron los siguientes objetos / Varibales / Constantes
                Se a�ade el par�metro @CodigoUsuarioAnulacion al objeto ActualizarDatoInfraccion: TADOStoredProc.
                Se a�aden los objetos lblEstadoIF y lblEstadoCN: TLabel.

            - Se modificaron/a�adieron las siguientes Funciones/Procedimientos
                CargarInfractor,
                lblAnularInfraccionClick,
                btnGuardarClick,
                Inicializar

revision    : 4
Author      : jdebarbieri
Date        : 03/11/2010
Description : SS 931 - Validacion - Mensaje de patentes en seguimiento
        - Se modifica el procedimiento btnGuardarClick para que
        verifique si la patente de la infraccion se encuentra en el
        listado de Patentes en Seguimiento.
Revision    : 2
Author      : Nelson Droguett Sierra
Date        : 07-Junio-2011
Description : (Ref.Fase2)SS-377
              Ya no se trabajara mas con imagenes padre, solo imagenes individuales
              Se agrega variable FImagePathNFI
              Function Inicializar
                Se obtiene el nuevo parametro general.

              Function cdsObtenerTransitoInfractoresPatenteAfterScroll
                Se asigna la variable NombreCorto
                En todas las llamadas a ObtenerImagenTransito se envia la ruta nueva y el nombreCorto
Firma       : SS-377-NDR-20110607

Author      : Claudio Quezada Ib��ez
Date        : 16-Mayo-2013
Firma       : SS_1091_CQU_20130516
Description : Se modifica la visualizaci�n de im�genes para poder desplegar im�genes de p�rticos Italianos.

                
Firma       :   SS_660_CQU_20130822
Desription  :   Se agrega Label que indica que la infracci�n es por morosidad

Firma       :   SS_1138_CQU_20131009
Descripcion :   Se quita la m�scara al cargar las im�genes.
                Se redimensionan las im�genes italianas
                Se agregan las funcionalidades de para los botones F1, F2, F3 y F4

Firma       :   SS_1135_CQU_20131030
Descripcion :   Se debe grabar la imagen en el Directorio de Infractores, pero se debe COPIAR el archivo existente
                y no del objeto ya que se pierden los datos de Kapsch dentro de la imagen.
                Al cargar las imagenes se deben obtener de las imagenes infractoras y no del tr�nsito

Firma       :   SS_1138_CQU_20140121
Descripcion :   Los botones F1, F2, F3 y F4 s�lo funcionan cuando se ha puesto el foco sobre la grilla por lo tanto
                luego de cargar el formulario, se obliga el foco sobre �sta con un timer y luego se deshabilita dicho timer.

Firma       :   SS_1138B_CQU_20140122
Descripcion :   A ra�z de la SS_1138B se detectaron algunos problemas en la SS 1135 que fueron corregidos.
                Las im�genes infractoras deben ser tomadas (en esta aplicaci�n) siempre desde la carpeta de tr�nsitos.

Firma       :   SS_660_CQU_20140310
Descripcion :   Se agrega c�digo faltante para desplegar el lblListaAmarilla

Author      :   Claudio Quezada Ib��ez
Date        :   17-Diciembre-2014
Firma		:   SS_1147X_CQU_20141217
Descripcion	:   Se agregan los bit  FEsKapschImagenActual, FEsKapschFrontal, FEsKapschFrontal2,
                FEsKapschPosterior, FEsKapschPosterior2 para indicar si la imagen es TJPEGPlusImage
                con ello se pretende mantener el comportamiento independiente de la imagen.
                Se hizo as� ya que el valor "EsItaliano" �no sirve en el caso de VS porque ellos
                usan otro proveedor de p�rticos el cual tambi�n trabaja con im�genes normales.

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

Firma       :   SS_1374A_CQU_20151002
Descripcion :   Se agerga AjustarTama�oTImagePlus en las acciones correspondientes a F1, F2, etc
                Se agregan los campos NombreCorto y EnSeguimiento para optimizar la grabaci�n
                ya que se evalua con ellos al momento de grabar y as� no se consultar� m�s a la BD por cada registro a validar.
}
unit frmEdicionInfraccion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Grids, DBGrids, ComCtrls, DMConnection, DB, ADODB,
  UtilProc, RStrings, ImgList, OPImageOverview, DPSControls, DBCtrls, ImgProcs,
  ImagePlus, ToolWin, Buttons, ActnList, ActnMan, ConstParametrosGenerales, ImgTypes,
  JPEGPlus, Filtros, Math, formImagenOverview, UtilDB, Util, PeaProcs, RVMLogin,
  Provider, DBClient, ListBoxEx, DBListEx, DmiCtrls, ActualizarDatosInfractor,
  VariantComboBox, PeaTypes, XPStyleActnCtrls, JPeg, PeaProcsCN, SysUtilsCN;    // SS_1135_CQU_20131030

type
  TformEdicionInfraccion = class(TForm)
    gbTransitos: TGroupBox;
    ObtenerTransitosxInfraccion: TADOStoredProc;
    DSObtenerTransitoInfractoresPatente: TDataSource;
    ImageList1: TImageList;
    ActionManager: TActionManager;
    actZoomIn: TAction;
    actZoomOut: TAction;
    actZoomReset: TAction;
    actZoomPlate: TAction;
    actBrightPlus: TAction;
    actBrigthLess: TAction;
    actBrigthReset: TAction;
    actCenterToPlate: TAction;
    actResetImage: TAction;
    actRemarcarBordes: TAction;
    actSuavizarBordes: TAction;
    Panel1: TPanel;
    btn_Anterior: TButton;
    btn_Siguiente: TButton;
    actImageOverview: TAction;
    gbImagenAmpliar: TGroupBox;
    ImagenAmpliar: TImagePlus;
    CoolBar2: TCoolBar;
    ToolBar2: TToolBar;
    ToolButton6: TToolButton;
    ToolButton8: TToolButton;
    ToolButton13: TToolButton;
    ToolButton14: TToolButton;
    ToolButton15: TToolButton;
    ToolButton16: TToolButton;
    ToolButton17: TToolButton;
    ToolButton18: TToolButton;
    ToolButton19: TToolButton;
    ToolButton20: TToolButton;
    ToolButton1: TToolButton;
    gbImagenes: TGroupBox;
    pnlThumb: TPanel;
    sbThumb0: TSpeedButton;
    sbThumb1: TSpeedButton;
    sbThumb2: TSpeedButton;
    sbThumb5: TSpeedButton;
    sbThumb4: TSpeedButton;
    sbThumb3: TSpeedButton;
    sbThumb6: TSpeedButton;
    dbTransitos: TDBListEx;
    cdsObtenerTransitoInfractoresPatente: TClientDataSet;
    pnl_Botones: TPanel;
    pnlBotones: TPanel;
    btnGuardar: TButton;
    btn_Salir: TButton;
    lblSetImagen: TLabel;
    txtNoHayImagen: TEdit;
    ObtenerDatosInfraccion: TADOStoredProc;
    pnlTransitoInfraccion: TPanel;
    pnlInfraccion: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    lblConsulta: TLabel;
    lblNombreTitular: TLabel;
    lblDireccion: TLabel;
    lblVehiculo: TLabel;
    lblEstadoIF: TLabel;
    lblLabelMotivoAnulacion: TLabel;
    gbDatosValidar: TGroupBox;
    pnlImgPatente: TPanel;
    imgPatente: TImage;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    lblLabelMotivoRechazo: TLabel;
    lblMotivoRechazo: TLabel;
    lblPatente: TLabel;
    lblFecha: TLabel;
    lblUsuario: TLabel;
    Label10: TLabel;
    lblCarta: TLabel;
    pnlAcciones: TPanel;
    chk_VerMascara: TCheckBox;
    btnImgRef: TButton;
    btnSinImgRef: TButton;
    ActualizarDatoInfraccion: TADOStoredProc;
    cbMotivoAnulacion: TVariantComboBox;
    lblAnularInfraccion: TLabel;
    Bevel1: TBevel;
    Label9: TLabel;
    lblEstadoCN: TLabel;
    lblListaAmarilla: TLabel;		// SS_660_CQU_20130822
    actImagenFrontal1: TAction;     // SS_1138_CQU_20131009
    actImagenFrontal2: TAction;     // SS_1138_CQU_20131009
    actImagenPosterior1: TAction;   // SS_1138_CQU_20131009
    actImagenPosterior2: TAction;   // SS_1138_CQU_20131009
    tmrActivaGrilla: TTimer;        // SS_1138_CQU_20140121
    procedure btn_SalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure sbThumb0Click(Sender: TObject);
    procedure sbThumb1Click(Sender: TObject);
    procedure sbThumb2Click(Sender: TObject);
    procedure sbThumb3Click(Sender: TObject);
    procedure sbThumb4Click(Sender: TObject);
    procedure sbThumb5Click(Sender: TObject);
    procedure sbThumb6Click(Sender: TObject);
    procedure actZoomResetExecute(Sender: TObject);
    procedure actZoomInExecute(Sender: TObject);
    procedure actZoomOutExecute(Sender: TObject);
    procedure actZoomPlateExecute(Sender: TObject);
    procedure actBrightPlusExecute(Sender: TObject);
    procedure actBrigthResetExecute(Sender: TObject);
    procedure actCenterToPlateExecute(Sender: TObject);
    procedure actResetImageExecute(Sender: TObject);
    procedure actRemarcarBordesExecute(Sender: TObject);
    procedure actSuavizarBordesExecute(Sender: TObject);
    procedure Redibujar(VerMascara: Boolean);
    procedure actBrigthLessExecute(Sender: TObject);
    procedure actMaskExecute(Sender: TObject);
    procedure btn_SiguienteClick(Sender: TObject);
    procedure btn_AnteriorClick(Sender: TObject);
    procedure actImageOverviewExecute(Sender: TObject);
    procedure dbTransitosDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: String; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure cdsObtenerTransitoInfractoresPatenteAfterScroll(
      DataSet: TDataSet);
    procedure chk_VerMascaraClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btnImgRefClick(Sender: TObject);
    procedure btnSinImgRefClick(Sender: TObject);
    procedure btnGuardarClick(Sender: TObject);
    procedure lblAnularInfraccionClick(Sender: TObject);
    procedure actImagenFrontal1Execute(Sender: TObject);	// SS_1138_CQU_20131009
    procedure actImagenFrontal2Execute(Sender: TObject);	// SS_1138_CQU_20131009
    procedure actImagenPosterior1Execute(Sender: TObject);	// SS_1138_CQU_20131009
    procedure actImagenPosterior2Execute(Sender: TObject);  // SS_1138_CQU_20131009
    procedure tmrActivaGrillaTimer(Sender: TObject);	    // SS_1138_CQU_20140121
  private
    { Private declarations }
    FImagenRef: String;
    FRegistrationAccessibilityRef : Integer;                // SS_1138_CQU_20131009
    FImagePathInfractores : AnsiString;                     // SS_1138_CQU_20131009
    Act: TDMActualizarDatosInfractor;
    FImagePath, FImagePathNFI: AnsiString;                                      //SS-377-NDR-20110607
    FIniciando: Boolean;

	FJPG12Frontal: TJPEGPlusImage;
	FJPG12Frontal2: TJPEGPlusImage;
	FJPG12Posterior: TJPEGPlusImage;
	FJPG12Posterior2: TJPEGPlusImage;
	FImgOverview1: TBitMap;
	FImgOverview2: TBitMap;
	FImgOverview3: TBitMap;

	FImagenActual: TJPEGPlusImage;
    FTipoImagenActual: TTipoImagen;
    FShiftOriginal: Integer;

    // Imagenes Italianas               // SS_1091_CQU_20130516
    FImagenItalianaActual,              // SS_1091_CQU_20130516
    FJPGItalianoFrontal,                // SS_1091_CQU_20130516
	FJPGItalianoFrontal2,               // SS_1091_CQU_20130516
	FJPGItalianoPosterior,              // SS_1091_CQU_20130516
	FJPGItalianoPosterior2: TBitmap;    // SS_1091_CQU_20130516

    FPositionPlate: Array[tiFrontal..tiPosterior2] of TRect;
    FFrmImagenOverview: TFrmImageOverview;
    FFormOverviewCargado: Boolean;
    FCodigoInfraccion: Integer;
    FPuedeSetImgRef: Boolean;

    FEventoImgRef: Boolean;
    FEventoSinImgRef: Boolean;
    FCodigoTipoInfraccion : SmallInt;                                           // SS_660_CQU_20140310

    FEsKapschImagenActual,              // SS_1147X_CQU_20141217
    FEsKapschFrontal,                   // SS_1147X_CQU_20141217
	FEsKapschFrontal2,                  // SS_1147X_CQU_20141217
	FEsKapschPosterior,                 // SS_1147X_CQU_20141217
	FEsKapschPosterior2 : Boolean;      // SS_1147X_CQU_20141217
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216

    FEstaEnSeguimiento : Boolean;                                               		// SS_1374A_CQU_20151002

    procedure SeleccionarImagen(aGraphic: TGraphic; aTipoImagen: TTipoImagen);
    procedure GetImagePlate(aGraphic: TGraphic; TipoImagen: TTipoImagen);
    procedure CentrarImagen;
    procedure CargarInfractor(CodigoInfraccion: Integer);
    procedure InicializarVariables;
    procedure RepaintBarState;
    procedure PosCombo(CodigoInfraccion: Integer);
    procedure GrabarImagenReferencia(RegistrationAccessibilityRef : Integer);   // SS_1135_CQU_20131030
  public
    { Public declarations }
    //function Inicializar(CodigoInfraccion: Integer):Boolean;                                      // SS_660_CQU_20140310
    function Inicializar(CodigoInfraccion: Integer; CodigoTipoInfraccion : Integer = 1):Boolean;    // SS_660_CQU_20140310
  end;

var
  formEdicionInfraccion: TformEdicionInfraccion;
  CONST_CODIGO_TIPO_INFRACCION_NORMAL,                  // SS_660_CQU_20130822
  CONST_CODIGO_TIPO_INFRACCION_POR_MOROSIDAD : Integer; // SS_660_CQU_20130822
  ImagenAGrabar : TImage;                               // SS_1138_CQU_20131009

implementation

uses frmListadoInfracciones, ExplorarInfracciones;


ResourceString
    SIN_IMG = 'Sin Imagen JPL';
    IMG = 'Imagen JPL Seleccionada';
    DEBE_IMG = 'Debe especificar una Imagen JPL';
    CAPTION_POS_PATENTE = 'Posici�n de Patente Desconocida';
    ERROR_GUARDAR = 'Error al Guardar los Cambios.';

{$R *.dfm}

procedure TformEdicionInfraccion.btn_SalirClick(Sender: TObject);
begin
    Close;
end;

procedure TformEdicionInfraccion.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    if Assigned(FImagenActual) then FImagenActual.Free;
	if Assigned(FJPG12Frontal) then FJPG12Frontal.Free;
	if Assigned(FJPG12Frontal2) then FJPG12Frontal2.Free;
	if Assigned(FJPG12Posterior) then FJPG12Posterior.Free;
	if Assigned(FJPG12Posterior2) then FJPG12Posterior2.Free;
	if Assigned(FImgOverview1) then FImgOverview1.Free;
	if Assigned(FImgOverview2) then FImgOverview2.Free;
	if Assigned(FImgOverview3) then FImgOverview3.Free;
    if Assigned(FFrmImagenOverview) then FFrmImagenOverview.Free;

    // Imagenes Italianas                                                           // SS_1091_CQU_20130516
    if Assigned(FImagenItalianaActual)      then FImagenItalianaActual.Free;        // SS_1091_CQU_20130516
    if Assigned(FJPGItalianoFrontal)        then FJPGItalianoFrontal.Free;          // SS_1091_CQU_20130516
	if Assigned(FJPGItalianoFrontal2)       then FJPGItalianoFrontal2.Free;         // SS_1091_CQU_20130516
	if Assigned(FJPGItalianoPosterior)      then FJPGItalianoPosterior.Free;        // SS_1091_CQU_20130516
	if Assigned(FJPGItalianoPosterior2)     then FJPGItalianoPosterior2.Free;       // SS_1091_CQU_20130516
    ImagenAGrabar.Free;                                                             // SS_1138_CQU_20131009
    Action := caFree;
end;

{
    Function Name : Inicializar
    Parameters : Sender: TObject
    Author:
    Date:

    Revision : 1
        Author: pdominguez
        Date: 09/05/2010
        Description: Infractores Fase 2
            - se a�ade el valor del par�metro soloPermitidosAnulacionManual en la llamada a CargarComboMotivoAnulacion.
}
//function TformEdicionInfraccion.Inicializar(CodigoInfraccion: Integer): Boolean;                                      // SS_660_CQU_20140310
function TformEdicionInfraccion.Inicializar(CodigoInfraccion: Integer; CodigoTipoInfraccion : Integer = 1): Boolean;    // SS_660_CQU_20140310
var
    S: TSize;
begin
    Result := True;
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216
    try
        FEsKapschImagenActual   := True;    // SS_1147X_CQU_20141217
        FEsKapschFrontal        := True;    // SS_1147X_CQU_20141217
        FEsKapschFrontal2       := True;    // SS_1147X_CQU_20141217
        FEsKapschPosterior      := True;    // SS_1147X_CQU_20141217
        FEsKapschPosterior2     := True;    // SS_1147X_CQU_20141217

        //ObtenerParametroGeneral(DMConnections.BaseCAC, DIR_IMAGENES_TRANSITOS, FImagePath);                                                      //SS-377-NDR-20110607
        ObtenerParametroGeneral(DMConnections.BaseCAC, DIR_IMAGENES_TRANSITOS_NUEVO_FORMATO_IMAGEN, FImagePathNFI);                                //SS-377-NDR-20110607

        //if Trim(FImagePath) = '' then raise Exception.Create('No se encontro el Parametro ' + DIR_IMAGENES_TRANSITOS);                           //SS-377-NDR-20110607
        if Trim(FImagePathNFI) = '' then raise Exception.Create('No se encontro el Parametro ' + DIR_IMAGENES_TRANSITOS_NUEVO_FORMATO_IMAGEN);     //SS-377-NDR-20110607

        ObtenerParametroGeneral(DMConnections.BaseCAC, DIR_IMAGENES_TRANSITOS_INFRACTORES, FImagePathInfractores);                                 // SS_1138_CQU_20131009
        if Trim(FImagePathInfractores) = '' then raise Exception.Create('No se encontro el Parametro ' + DIR_IMAGENES_TRANSITOS_INFRACTORES);      // SS_1138_CQU_20131009

        screen.Cursor := crHourGlass;
        // Inicializamos los controles para la imagen Overview
        FFrmImagenOverview := TfrmImageOverview.create(self);
        FFrmImagenOverview.Parent := self;
        FFrmImagenOverview.Left := self.ScreenToClient(ImagenAmpliar.ClientToScreen(Point(0, ImagenAmpliar.Left))).X + 2 ;
        FFrmImagenOverview.top  := self.ScreenToClient(ImagenAmpliar.ClientToScreen(Point(ImagenAmpliar.top, 0))).Y + 2;

        ImagenAGrabar           := TImage.Create(Self); // SS_1138_CQU_20131009    
        //Cargo el peromiso para VerMascara
        chk_VerMascara.Checked := True;
        chk_VerMascara.Visible := ExisteAcceso('ver_mascara');

        S := GetFormClientSize(Application.MainForm);
        SetBounds(0, 0, S.cx, S.cy);
        FEstaEnSeguimiento := False;            // SS_1374A_CQU_20151002
        cdsObtenerTransitoInfractoresPatente.CreateDataSet;
        Update;

        FCodigoInfraccion := CodigoInfraccion; // Infraccion Actual
        FPuedeSetImgRef   := False;             // Por defaul NO puede editar la Imagen de Ref
        CargarComboMotivoAnulacion(DMConnections.BaseCAC, cbMotivoAnulacion, False, True); // Cargo el Combo de Motivos de Anulaci�n // Rev. 1 (Fase 2)

        CargarInfractor(CodigoInfraccion); // Cargo los datos y la imagen de la Imfracci�n

        //Revision 1
        btnImgRef.Visible := ExisteAcceso('boton_elegir_imagen_JPL');
        btnSinImgRef.Visible := ExisteAcceso('boton_sin_imagen_JPL');

        FCodigoTipoInfraccion := CodigoTipoInfraccion;                                                                                                                      // SS_660_CQU_20140310
        CONST_CODIGO_TIPO_INFRACCION_POR_MOROSIDAD  := SysUtilsCN.QueryGetIntegerValue(DMConnections.BaseCAC, 'SELECT dbo.CONST_CODIGO_TIPO_INFRACCION_POR_MOROSIDAD()');   // SS_660_CQU_20140310
        if FCodigoTipoInfraccion = CONST_CODIGO_TIPO_INFRACCION_POR_MOROSIDAD then begin                                                                                    // SS_660_CQU_20140310
            lblListaAmarilla.Visible := True;                                                                                                                               // SS_660_CQU_20140310
        end else                                                                                                                                                            // SS_660_CQU_20140310
            lblListaAmarilla.Visible := False;                                                                                                                              // SS_660_CQU_20140310

        //Revision 2
        lblAnularInfraccion.Visible := ExisteAcceso('anular_infraccion');
        FEventoImgRef := False;
        FEventoSinImgRef := False;

        Result := True;
        screen.Cursor := crDefault;
    except
        on e: Exception do begin
            screen.Cursor := crDefault;
            MsgBoxErr(format(MSG_ERROR_INICIALIZACION,['Infracciones']), e.Message, 'Infracciones', MB_ICONSTOP);
        end;
    end;

end;

procedure TformEdicionInfraccion.SeleccionarImagen(aGraphic: TGraphic; aTipoImagen: TTipoImagen);
begin
    if FTipoImagenActual = aTipoImagen then exit;

    if assigned(aGraphic) then begin
        if aGraphic.Empty then Exit;                                                                    // SS_1138_CQU_20131009    
		Screen.Cursor := crHourGlass;
        try
		  	if aTipoImagen in [tiFrontal, tiPosterior, tiFrontal2, tiPosterior2] then begin
                if      ((aTipoImagen = tiFrontal) AND FEsKapschFrontal) then FEsKapschImagenActual := True         // SS_1147X_CQU_20141217
                else if ((aTipoImagen = tiFrontal2) AND FEsKapschFrontal2) then FEsKapschImagenActual := True       // SS_1147X_CQU_20141217
                else if ((aTipoImagen = tiPosterior) AND FEsKapschPosterior) then FEsKapschImagenActual := True     // SS_1147X_CQU_20141217
                else if ((aTipoImagen = tiPosterior2) AND FEsKapschPosterior2) then FEsKapschImagenActual := True   // SS_1147X_CQU_20141217
                else FEsKapschImagenActual := false;                                                                // SS_1147X_CQU_20141217
                //FImagenActual.Assign(aGraphic);                                                       // SS_1091_CQU_20130516
                //if not cdsObtenerTransitoInfractoresPatente.FieldByName('EsItaliano').AsBoolean then  // SS_1147X_CQU_20141217 // SS_1091_CQU_20130516
                if FEsKapschImagenActual then                                                           // SS_1147X_CQU_20141217
				    FImagenActual.Assign(aGraphic)                                                      // SS_1091_CQU_20130516
                else                                                                                    // SS_1091_CQU_20130516
                    FImagenItalianaActual.Assign(aGraphic);                                             // SS_1091_CQU_20130516
				FShiftOriginal := -1;
                FTipoImagenActual := aTipoImagen;
                Redibujar(chk_VerMascara.Checked);
                actResetImageExecute(nil);
                //if cdsObtenerTransitoInfractoresPatente.FieldByName('EsItaliano').AsBoolean then      // SS_1147X_CQU_20141217  // SS_1138_CQU_20131009
                if not FEsKapschImagenActual then                                                       // SS_1147X_CQU_20141217
                begin                                                                                   // SS_1138_CQU_20131009
                    if aTipoImagen in [tiFrontal, tiPosterior] then                                     // SS_1138_CQU_20131009
                        ImagenAmpliar.Scale := 0.4                                                      // SS_1138_CQU_20131009
                    else if aTipoImagen in [tiFrontal2, tiPosterior2] then                              // SS_1138_CQU_20131009
                        ImagenAmpliar.Scale := 0.1;                                                     // SS_1138_CQU_20131009
                end;                                                                                    // SS_1138_CQU_20131009
            end else begin
                FFormOverviewCargado := true;
                FFrmImagenOverview.ShowImageOverview(aGraphic, aTipoImagen);
                FFrmImagenOverview.Show;
            end;

        finally
            AjustarTama�oTImagePlus(ImagenAmpliar);    // SS_1374A_CQU_20151002
            Screen.Cursor := crDefault;
		end;
	end;
end;

procedure TformEdicionInfraccion.sbThumb0Click(Sender: TObject);
begin
    //SeleccionarImagen(FJPG12Frontal, tiFrontal);                                          // SS_1091_CQU_20130516
    //if not cdsObtenerTransitoInfractoresPatente.FieldByName('EsItaliano').AsBoolean then  // SS_1147X_CQU_20141217  // SS_1091_CQU_20120516
    if FEsKapschFrontal then                                                                // SS_1147X_CQU_20141217
        SeleccionarImagen(FJPG12Frontal, tiFrontal)                                         // SS_1091_CQU_20130516
    else                                                                                    // SS_1091_CQU_20130516
        SeleccionarImagen(FJPGItalianoFrontal, tiFrontal);                                  // SS_1091_CQU_20130516
end;

procedure TformEdicionInfraccion.sbThumb1Click(Sender: TObject);
begin
    //SeleccionarImagen(FJPG12Posterior, tiPosterior);                                      // SS_1091_CQU_20130516
    //if not cdsObtenerTransitoInfractoresPatente.FieldByName('EsItaliano').AsBoolean then  // SS_1147X_CQU_20141217  // SS_1091_CQU_20120516
    if FEsKapschPosterior then                                                              // SS_1147X_CQU_20141217
        SeleccionarImagen(FJPG12Posterior, tiPosterior)                                     // SS_1091_CQU_20130516
    else                                                                                    // SS_1091_CQU_20130516
        SeleccionarImagen(FJPGItalianoPosterior, tiPosterior);                              // SS_1091_CQU_20130516
end;

procedure TformEdicionInfraccion.sbThumb2Click(Sender: TObject);
begin
    SeleccionarImagen(FImgOverview1, tiOverview1);
end;

procedure TformEdicionInfraccion.sbThumb3Click(Sender: TObject);
begin
    SeleccionarImagen(FImgOverview2, tiOverview2);
end;

procedure TformEdicionInfraccion.sbThumb4Click(Sender: TObject);
begin
    SeleccionarImagen(FImgOverview3, tiOverview3);
end;

procedure TformEdicionInfraccion.sbThumb5Click(Sender: TObject);
begin
    //SeleccionarImagen(FJPG12Frontal2, tiFrontal2);                                        // SS_1091_CQU_20130516
    //if not cdsObtenerTransitoInfractoresPatente.FieldByName('EsItaliano').AsBoolean then  // SS_1147X_CQU_20141217  // SS_1091_CQU_20120516
    if FEsKapschFrontal2 then                                                               // SS_1147X_CQU_20141217
        SeleccionarImagen(FJPG12Frontal2, tiFrontal2)                                       // SS_1091_CQU_20130516
    else                                                                                    // SS_1091_CQU_20130516
        SeleccionarImagen(FJPGItalianoFrontal2, tiFrontal2);                                // SS_1091_CQU_20130516
end;

procedure TformEdicionInfraccion.sbThumb6Click(Sender: TObject);
begin
    //SeleccionarImagen(FJPG12Posterior2, tiPosterior2);                                    // SS_1091_CQU_20130516
    //if not cdsObtenerTransitoInfractoresPatente.FieldByName('EsItaliano').AsBoolean then  // SS_1147X_CQU_20141217  // SS_1091_CQU_20120516
    if FEsKapschPosterior2 then                                                             // SS_1147X_CQU_20141217
        SeleccionarImagen(FJPG12Posterior2, tiPosterior2)                                   // SS_1091_CQU_20130516
    else                                                                                    // SS_1091_CQU_20130516
        SeleccionarImagen(FJPGItalianoPosterior2, tiPosterior2);                            // SS_1091_CQU_20130516
end;

procedure TformEdicionInfraccion.actZoomInExecute(Sender: TObject);
begin
   	ImagenAmpliar.Scale := ImagenAmpliar.Scale + 0.2;
end;

procedure TformEdicionInfraccion.actZoomOutExecute(Sender: TObject);
begin
    //if ImagenAmpliar.Scale < 0.5 then exit;                                               // SS_1138_CQU_20131009
    //ImagenAmpliar.Scale := ImagenAmpliar.Scale - 0.2;                                     // SS_1138_CQU_20131009
    //if not cdsObtenerTransitoInfractoresPatente.FieldByName('EsItaliano').AsBoolean then  // SS_1147X_CQU_20141217  //  SS_1138_CQU_20131009
    if FEsKapschImagenActual then                                                           // SS_1147X_CQU_20141217
    begin                                                                                   // SS_1138_CQU_20131009
        if ImagenAmpliar.Scale < 0.5 then exit;                                             // SS_1138_CQU_20131009
        ImagenAmpliar.Scale := ImagenAmpliar.Scale - 0.2;                                   // SS_1138_CQU_20131009
    end else begin                                                                          // SS_1138_CQU_20131009
        if FTipoImagenActual in [tiFrontal, tiPosterior] then begin                         // SS_1138_CQU_20131009
            if ImagenAmpliar.Scale < 0.3 then exit                                          // SS_1138_CQU_20131009
            else ImagenAmpliar.Scale := ImagenAmpliar.Scale - 0.2;                          // SS_1138_CQU_20131009
        end else if FTipoImagenActual in [tiFrontal2, tiPosterior2] then begin              // SS_1138_CQU_20131009
            if ImagenAmpliar.Scale > 0.2 then                                               // SS_1138_CQU_20131009
                ImagenAmpliar.Scale := ImagenAmpliar.Scale - 0.1;                           // SS_1138_CQU_20131009
        end;                                                                                // SS_1138_CQU_20131009
    end;                                                                                    // SS_1138_CQU_20131009
end;

procedure TformEdicionInfraccion.actZoomResetExecute(Sender: TObject);
begin
    //if ImagenAmpliar.Scale > 4.0 then exit;   // SS_1374A_CQU_20151002
    //ImagenAmpliar.Scale := 1;                 // SS_1374A_CQU_20151002
    AjustarTama�oTImagePlus(ImagenAmpliar);     // SS_1374A_CQU_20151002
end;

procedure TformEdicionInfraccion.actZoomPlateExecute(Sender: TObject);
begin
    // Calcular la escala de la imagen en funcion del marco de la patente
    ImagenAmpliar.Scale := 1;
end;

procedure TformEdicionInfraccion.actBrightPlusExecute(Sender: TObject);
begin
    //if not cdsObtenerTransitoInfractoresPatente.FieldByName('EsItaliano').AsBoolean then begin  // SS_1147X_CQU_20141217 // SS_1091_CQU_20130516
    if FEsKapschImagenActual then begin                                                           // SS_1147X_CQU_20141217
        // Si el Shift supero el limite nos vamos
        if FImagenActual.Shift = 0 then exit;
        // Si el FShift no esta inicializado lo asignamos
        if FShiftOriginal = -1 then begin
            FShiftOriginal := FImagenActual.Shift;
        end;
        FImagenActual.Shift := FImagenActual.Shift - 1;
    end;                                                                                        // SS_1091_CQU_20130516
	Redibujar(chk_VerMascara.Checked);
end;

procedure TformEdicionInfraccion.actBrigthResetExecute(Sender: TObject);
begin
    //if not cdsObtenerTransitoInfractoresPatente.FieldByName('EsItaliano').AsBoolean then begin    // SS_1147X_CQU_20141217 // SS_1091_CQU_20130516
    if FEsKapschImagenActual then begin                                                             // SS_1147X_CQU_20141217
        if FShiftOriginal = -1 then begin
            FShiftOriginal := FImagenActual.Shift;
        end;
        if FImagenActual.Shift = FShiftOriginal then exit;
        FImagenActual.Shift := FShiftOriginal;
    end;                                                                                        // SS_1091_CQU_20130516
	Redibujar(chk_VerMascara.Checked);
end;

procedure TformEdicionInfraccion.actBrigthLessExecute(Sender: TObject);
begin
    //if not cdsObtenerTransitoInfractoresPatente.FieldByName('EsItaliano').AsBoolean then begin    // SS_1147X_CQU_20141217 // SS_1091_CQU_20130516
    if FEsKapschImagenActual then begin                                                             // SS_1147X_CQU_20141217
        if FImagenActual.Shift > 4 then exit;
        // Si el FShift no esta inicializado lo asignamos
        if FShiftOriginal = -1 then begin
            FShiftOriginal := FImagenActual.Shift;
        end;
        FImagenActual.Shift := FImagenActual.Shift + 1;
    end;                                                                                        // SS_1091_CQU_20130516
	Redibujar(chk_VerMascara.Checked);
end;

procedure TformEdicionInfraccion.actRemarcarBordesExecute(Sender: TObject);
begin
	Redibujar(chk_VerMascara.Checked);
end;

procedure TformEdicionInfraccion.actSuavizarBordesExecute(Sender: TObject);
begin
	Redibujar(chk_VerMascara.Checked);
end;

procedure TformEdicionInfraccion.actMaskExecute(Sender: TObject);
begin
	Redibujar(chk_VerMascara.Checked);
end;

procedure TformEdicionInfraccion.Redibujar(VerMascara: Boolean);
var
    bmp: TBitMap;
    EsItaliano : Boolean;   // SS_1091_CQU_20130516
begin
	Screen.Cursor := crHourGlass;

    bmp := TBitMap.create;
    bmp.PixelFormat := pf24bit;

	try
        // Se coloca el try ya que pasa por aqu� antes de incializar el cds                         // SS_1091_CQU_20130516
        try                                                                                         // SS_1091_CQU_20130516
            EsItaliano := cdsObtenerTransitoInfractoresPatente.FieldByName('EsItaliano').AsBoolean; // SS_1091_CQU_20130516
        except                                                                                      // SS_1091_CQU_20130516
            EsItaliano := False;                                                                    // SS_1091_CQU_20130516
        end;                                                                                        // SS_1091_CQU_20130516

        // Reseteamos los filtros aplicados
        //bmp.Assign(FImagenActual);            // SS_1091_CQU_20130516
        //if EsItaliano then                    // SS_1147X_CQU_20141217  // SS_1091_CQU_20130516
        if FEsKapschImagenActual then           // SS_1147X_CQU_20141217
            bmp.Assign(FImagenActual)           // SS_1091_CQU_20130516
        else                                    // SS_1091_CQU_20130516
            bmp.Assign(FImagenItalianaActual);  // SS_1091_CQU_20130516

    	// Aplicamos el Sharp
	    if actRemarcarBordes.Checked then begin
 		    Sharp(bmp);
    	end;

		// Aplicamos el Blurr
		if actSuavizarBordes.checked then begin
		    Blurr(bmp);
    	end;

        // Dibujamos la Mascara
        //if VerMascara then                                            // SS_1138_CQU_20131009
        if not EsItaliano and VerMascara then                           // SS_1138_CQU_20131009
            DrawMask(bmp, FPositionPlate[FTipoImagenActual].top - 4);

        // Cargamos la imagen de la patente
        //GetImagePlate(FImagenActual, FTipoImagenActual);              // SS_1091_CQU_20130516
        //if not EsItaliano then                                        // SS_1147X_CQU_20141217    // SS_1091_CQU_20130516
        if FEsKapschImagenActual then                                   // SS_1147X_CQU_20141217
            GetImagePlate(FImagenActual, FTipoImagenActual)             // SS_1091_CQU_20130516
        else                                                            // SS_1091_CQU_20130516
            GetImagePlate(FImagenItalianaActual, FTipoImagenActual);    // SS_1091_CQU_20130516

        // Asignamos la nueva imagen
        ImagenAmpliar.Picture.Assign(bmp);
    finally
        Screen.Cursor := crDefault;
        bmp.Free;
    end;
end;


procedure TformEdicionInfraccion.GetImagePlate(aGraphic: TGraphic; TipoImagen: TTipoImagen);
var
    bmpSource, bmpDest: TBitMap;
    EsItaliano : Boolean;                                                                               // SS_1138_CQU_20131009
begin
    bmpSource := TBitMap.create;
	bmpDest := TBitMap.create;
    try
        if cdsObtenerTransitoInfractoresPatente.Active then                                             // SS_1138_CQU_20131009
            EsItaliano := cdsObtenerTransitoInfractoresPatente.FieldByName('EsItaliano').AsBoolean      // SS_1138_CQU_20131009
        else EsItaliano := False;                                                                       // SS_1138_CQU_20131009

        if ((FPositionPlate[TipoImagen].Left = 0) and (FPositionPlate[TipoImagen].Right = 0)) then begin
            pnlImgPatente.Caption := CAPTION_POS_PATENTE;
            imgPatente.Picture.Bitmap := Nil;
            //btnImgRef.Enabled := False;                                                               // SS_1138_CQU_20131009
            if not EsItaliano then btnImgRef.Enabled := False                                           // SS_1138_CQU_20131009
            else btnImgRef.Enabled := True;                                                             // SS_1138_CQU_20131009
        end else begin
            bmpSource.PixelFormat := pf24bit;
            bmpSource.Assign(aGraphic);
            bmpDest.PixelFormat := pf24bit;
            bmpDest.Width := imgPatente.Width;
            bmpDest.Height := imgPatente.Height;
			bmpDest.Canvas.CopyRect(Rect(0, 0, imgPatente.Width, imgPatente.Height), bmpSource.Canvas,
				Rect(FPositionPlate[TipoImagen].Left - 4, FPositionPlate[TipoImagen].top - 4,
                FPositionPlate[TipoImagen].right + 4, FPositionPlate[TipoImagen].Bottom + 4));
            imgPatente.Picture.Assign(bmpDest);
            btnImgRef.Enabled := FPuedeSetImgRef;
        end;
    finally
        bmpSource.free;
        bmpDest.free;
    end;
end;


procedure TformEdicionInfraccion.actCenterToPlateExecute(Sender: TObject);
begin
	CentrarImagen;
end;

procedure TformEdicionInfraccion.actResetImageExecute(Sender: TObject);
begin
    // Volvemos a False todos los filtros seleccionados
	actRemarcarBordes.Checked := False;
	actSuavizarBordes.Checked := False;
    //if not cdsObtenerTransitoInfractoresPatente.FieldByName('EsItaliano').AsBoolean then begin    // SS_1147X_CQU_20141217 // SS_1091_CQU_20130516
    if FEsKapschImagenActual then begin                                                             // SS_1147X_CQU_20141217
        // Volvelos el brillo al original
        if FShiftOriginal = -1 then begin
            FShiftOriginal := FImagenActual.Shift;
        end;
        FImagenActual.Shift    := FShiftOriginal;
    end;                                                                                        // SS_1091_CQU_20130516
    // Volvemos el Zoom al original
    ImagenAmpliar.Scale     := 1;
    // Redibujamos
    Redibujar(chk_VerMascara.Checked);
	// Centramos la imagen
    CentrarImagen;
    AjustarTama�oTImagePlus(ImagenAmpliar);    // SS_1374A_CQU_20151002
end;

procedure TformEdicionInfraccion.CentrarImagen;
begin
    if FPositionPlate[FTipoImagenActual].Left = 0 then begin
            ImagenAmpliar.Center
    end else
        ImagenAmpliar.CenterToPoint(Point(
            FPositionPlate[FTipoImagenActual].Left +
            ((FPositionPlate[FTipoImagenActual].right - FPositionPlate[FTipoImagenActual].Left) div 2),
            FPositionPlate[FTipoImagenActual].top +
            ((FPositionPlate[FTipoImagenActual].bottom - FPositionPlate[FTipoImagenActual].top) div 2)));
end;

procedure TformEdicionInfraccion.CargarInfractor(CodigoInfraccion: Integer);
resourcestring
    CAPTION_A_MOSTRAR = 'Edici�n de Infracciones - Patente: %s Fecha de Infracci�n %s';
    NO_TRANSITO = 'No hay transitos para Validar.';
    FIN_PATENTE = 'Ya no hay m�s tr�nsitos para validar para esta patente.';
begin
    try
        FIniciando := True;

        gbTransitos.Enabled := True;
        gbImagenAmpliar.Enabled := True;
        gbImagenes.Enabled := True;
        EnableControlsInContainer(gbDatosValidar, True);
        EnableControlsInContainer(pnl_Botones, True);

        cdsObtenerTransitoInfractoresPatente.DisableControls;
        cdsObtenerTransitoInfractoresPatente.EmptyDataSet;
        Screen.Cursor := crHourGlass;

        Update;

        // Obtener Infraccion
        ObtenerDatosInfraccion.Close;
        ObtenerDatosInfraccion.Parameters.ParamByName('@CodigoInfraccion').Value := CodigoInfraccion;
        ObtenerDatosInfraccion.Open;

        // Cargo los campos
        lblConsulta.Caption := ObtenerDatosInfraccion.FieldByName('TieneConsultaRNVM').AsString;
        lblNombreTitular.Caption := ObtenerDatosInfraccion.FieldByName('Nombre').AsString;
        lblDireccion.Caption := ObtenerDatosInfraccion.FieldByName('Domicilio').AsString;
        lblVehiculo.Caption := ObtenerDatosInfraccion.FieldByName('DatoVehiculo').AsString;
        lblMotivoRechazo.Caption := ObtenerDatosInfraccion.FieldByName('MotivoRechazo').AsString;
        // Rev. 1 (Fase 2)
        lblEstadoIF.Caption := ObtenerDatosInfraccion.FieldByName('DescEstadoExterno').AsString;
        lblEstadoCN.Caption := ObtenerDatosInfraccion.FieldByName('DescEstadoInterno').AsString;
        // Fin Rev. 1 (Fase 2)
        PosCombo(ObtenerDatosInfraccion.FieldByName('CodigoMotivoAnulacion').AsInteger);
        lblPatente.Caption := ObtenerDatosInfraccion.FieldByName('Patente').AsString;
        lblFecha.Caption := FormatDateTime('dd/mm/yyyy', ObtenerDatosInfraccion.FieldByName('FechaInfraccion').AsDateTime);
        lblCarta.Caption := ObtenerDatosInfraccion.FieldByName('EnvioCodigoCartaAInfractor').AsString
                            + iif(ObtenerDatosInfraccion.FieldByName('FechaEnvioCarta').IsNull, '', ' - ' + ObtenerDatosInfraccion.FieldByName('FechaEnvioCarta').AsString);
        lblUsuario.Caption := ObtenerDatosInfraccion.FieldByName('CodigoUsuario').AsString;
        FImagenRef := ObtenerDatosInfraccion.FieldByName('RefNumCorrCA').AsString;
        FRegistrationAccessibilityRef := ObtenerDatosInfraccion.FieldByName('RefRegistrationAccessibility').AsInteger;  // SS_1135_CQU_20131030
        RepaintBarState;

        // Habilito o dehabilito deacuerdo si ese campo tiene algun valor segun el estado de la infracci�n
        // Rev. 1 (Fase 2)
        {
        lblAnularInfraccion.Enabled := (ObtenerDatosInfraccion.FieldByName('CodigoEstado').AsInteger = PENDIENTE_ENVIO) or // Lo habilito, siempre que el estado de la Infraccion lo permita
                            (ObtenerDatosInfraccion.FieldByName('CodigoEstado').AsInteger = RESPUESTA_ERROR) or
                            (ObtenerDatosInfraccion.FieldByName('CodigoEstado').AsInteger = IMAGEN_ANULADA);
        }

        lblAnularInfraccion.Enabled :=
            (ObtenerDatosInfraccion.FieldByName('CodigoEstadoExterno').AsInteger = CONST_ESTADO_INFRACCION_EXTERNO_PENDIENTE_ENVIO) or
            (ObtenerDatosInfraccion.FieldByName('CodigoEstadoInterno').AsInteger = CONST_ESTADO_INFRACCION_INTERNO_PENDIENTE);
        // fin rev. 1 (Fase 2)
        cbMotivoAnulacion.Enabled := False;
        // Rev. 1 (Fase 2)
        lblLabelMotivoAnulacion.Enabled :=
            (ObtenerDatosInfraccion.FieldByName('CodigoEstadoExterno').AsInteger = CONST_ESTADO_INFRACCION_EXTERNO_ANULADA) or
            (ObtenerDatosInfraccion.FieldByName('CodigoEstadoInterno').AsInteger = CONST_ESTADO_INFRACCION_INTERNO_ANULADA);

        FPuedeSetImgRef :=
            (ObtenerDatosInfraccion.FieldByName('CodigoEstadoExterno').AsInteger = CONST_ESTADO_INFRACCION_EXTERNO_PENDIENTE_ENVIO) or // Lo habilito, siempre que el estado de la Infraccion lo permita
            (
                (ObtenerDatosInfraccion.FieldByName('CodigoEstadoExterno').AsInteger = CONST_ESTADO_INFRACCION_EXTERNO_ANULADA) and
                (ObtenerDatosInfraccion.FieldByName('CodigoMotivoAnulacion').AsInteger = CONST_MOTIV_ANUL_INFRAC_JPL_INFRACCION_CON_ERROR)
            ) or
            (
                (ObtenerDatosInfraccion.FieldByName('CodigoEstadoExterno').AsInteger = CONST_ESTADO_INFRACCION_EXTERNO_ANULADA) and
                (ObtenerDatosInfraccion.FieldByName('CodigoMotivoAnulacion').AsInteger = CONST_MOTIV_ANUL_INFRAC_ERROR_IMAGEN_NO_ENCONTRADA));
        // Fin Rev. 1 (Fase 2)
        btnImgRef.Enabled := FPuedeSetImgRef;
        btnSinImgRef.Enabled := FPuedeSetImgRef;

        btnGuardar.Enabled := lblAnularInfraccion.Enabled or FPuedeSetImgRef; // Se habilita, si se puede editar algun campo de la infracci�n.

        lblNombreTitular.Enabled := lblConsulta.Caption = 'SI';
        lblDireccion.Enabled := lblNombreTitular.Enabled;
        lblVehiculo.Enabled := lblNombreTitular.Enabled;
        lblLabelMotivoRechazo.Enabled := Trim(lblMotivoRechazo.Caption) <> '';

        // Cargo el DataModulo
        ObtenerTransitosxInfraccion.Close;
        ObtenerTransitosxInfraccion.Parameters.ParamByName('@CodigoInfraccion').Value := CodigoInfraccion;
        ObtenerTransitosxInfraccion.Open;

        while not ObtenerTransitosxInfraccion.Eof do begin
            cdsObtenerTransitoInfractoresPatente.Append;
            cdsObtenerTransitoInfractoresPatente.FieldByName('DescripPuntoCobro').Value := ObtenerTransitosxInfraccion.FieldByName('DescripPuntoCobro').Value;
            cdsObtenerTransitoInfractoresPatente.FieldByName('FechaHora').Value := ObtenerTransitosxInfraccion.FieldByName('FechaHora').Value;
            cdsObtenerTransitoInfractoresPatente.FieldByName('NumCorrCA').Value := IntToStr(ObtenerTransitosxInfraccion.FieldByName('NumCorrCA').Value);
            cdsObtenerTransitoInfractoresPatente.FieldByName('Anulada').Value   := ObtenerTransitosxInfraccion.FieldByName('Anulada').Value;
            cdsObtenerTransitoInfractoresPatente.FieldByName('EsItaliano').Value := ObtenerTransitosxInfraccion.FieldByName('EsItaliano').AsBoolean;   // SS_1091_CQU_20130516
            cdsObtenerTransitoInfractoresPatente.FieldByName('NombreCorto').Value	:= ObtenerTransitosxInfraccion.FieldByName('NombreCorto').AsString;     // SS_1374A_CQU_20151002
            cdsObtenerTransitoInfractoresPatente.FieldByName('EnSeguimiento').Value	:= ObtenerTransitosxInfraccion.FieldByName('EnSeguimiento').AsBoolean;	// SS_1374A_CQU_20151002
            if not FEstaEnSeguimiento then FEstaEnSeguimiento := ObtenerTransitosxInfraccion.FieldByName('EnSeguimiento').AsBoolean;                        // SS_1374A_CQU_20151002
            cdsObtenerTransitoInfractoresPatente.Post;
            ObtenerTransitosxInfraccion.Next;
        end;
        ObtenerTransitosxInfraccion.close;

        cdsObtenerTransitoInfractoresPatente.First;
        cdsObtenerTransitoInfractoresPatente.EnableControls;

        FIniciando := False;

        if not cdsObtenerTransitoInfractoresPatente.IsEmpty then begin
            Caption := format(CAPTION_A_MOSTRAR,[
                                    Trim(ObtenerDatosInfraccion.FieldByName('Patente').AsString),
                                    FormatDateTime('dd/mm/yyyy',ObtenerDatosInfraccion.FieldByName('FechaInfraccion').AsDateTime)
                            ]);
            cdsObtenerTransitoInfractoresPatente.AfterScroll(cdsObtenerTransitoInfractoresPatente) // Para que cargue la primera imagen
        end else begin

            Caption := format(CAPTION_A_MOSTRAR,['Sin Determinar','Sin Determinar']);
            gbTransitos.Enabled := False;
            gbImagenAmpliar.Enabled := False;
            gbImagenes.Enabled := False;
            ImagenAmpliar.Picture.Bitmap := nil;
            imgPatente.Picture.Bitmap := nil;
            EnableControlsInContainer(gbDatosValidar, False);
            btnGuardar.Enabled := False;
            MsgBox(NO_TRANSITO, Caption, MB_ICONINFORMATION);
        end;
        Screen.Cursor := crDefault;
    except
        on e: Exception do begin
                Screen.Cursor := crDefault;
                gbTransitos.Enabled := False;
                gbImagenAmpliar.Enabled := False;
                gbImagenes.Enabled := False;
                EnableControlsInContainer(gbDatosValidar, False);
                btnGuardar.Enabled := False;
                raise;
            end;
    end;
end;

procedure TformEdicionInfraccion.btn_SiguienteClick(Sender: TObject);
begin
    if cdsObtenerTransitoInfractoresPatente.RecNo <> cdsObtenerTransitoInfractoresPatente.RecordCount then cdsObtenerTransitoInfractoresPatente.Next
    else cdsObtenerTransitoInfractoresPatente.First;
end;

procedure TformEdicionInfraccion.btn_AnteriorClick(Sender: TObject);
begin
    if cdsObtenerTransitoInfractoresPatente.RecNo > 1 then cdsObtenerTransitoInfractoresPatente.Prior
    else if cdsObtenerTransitoInfractoresPatente.RecordCount <> 1 then cdsObtenerTransitoInfractoresPatente.Last;

end;

procedure TformEdicionInfraccion.InicializarVariables;
begin
    if Assigned(FImagenActual) then FreeAndNil(FImagenActual);
	if Assigned(FJPG12Frontal) then FreeAndNil(FJPG12Frontal);
	if Assigned(FJPG12Frontal2) then FreeAndNil(FJPG12Frontal2);
	if Assigned(FJPG12Posterior) then FreeAndNil(FJPG12Posterior);
	if Assigned(FJPG12Posterior2) then FreeAndNil(FJPG12Posterior2);
	if Assigned(FImgOverview1) then FreeAndNil(FImgOverview1);
	if Assigned(FImgOverview2) then FreeAndNil(FImgOverview2);
	if Assigned(FImgOverview3) then FreeAndNil(FImgOverview3);
    if Assigned(FImagenActual) then FreeAndNil(FImagenActual);
    // Imagenes Italianas                                                               // SS_1091_CQU_20130516
    if Assigned(FImagenItalianaActual)      then FreeAndNil(FImagenItalianaActual);     // SS_1091_CQU_20130516
    if Assigned(FJPGItalianoFrontal)        then FreeAndNil(FJPGItalianoFrontal);       // SS_1091_CQU_20130516
	if Assigned(FJPGItalianoFrontal2)       then FreeAndNil(FJPGItalianoFrontal2);      // SS_1091_CQU_20130516
	if Assigned(FJPGItalianoPosterior)      then FreeAndNil(FJPGItalianoPosterior);     // SS_1091_CQU_20130516
	if Assigned(FJPGItalianoPosterior2)     then FreeAndNil(FJPGItalianoPosterior2);    // SS_1091_CQU_20130516
    // Cargar la Tabla de valores de confiabilidad para calcular ConfiabilidadTotal.
    FImagenActual       := TJPEGPlusImage.Create;
    FJPG12Frontal       := TJPEGPlusImage.Create;
    FJPG12Frontal2      := TJPEGPlusImage.Create;
    FJPG12Posterior     := TJPEGPlusImage.Create;
    FJPG12Posterior2          := TJPEGPlusImage.Create;
    FImgOverview1             := TBitMap.Create;
    FImgOverview1.PixelFormat := pf24bit;
    FImgOverview2             := TBitMap.Create;
    FImgOverview2.PixelFormat := pf24bit;
    FImgOverview3             := TBitMap.Create;
    FImgOverview3.PixelFormat := pf24bit;

    FTipoImagenActual := tiDesconocida;

    FFrmImagenOverview.ClearImagesOverview();
    FFormOverviewCargado := False;
    FFrmImagenOverview.Hide;

    FImagenItalianaActual   := TBitMap.Create;   // SS_1091_CQU_20130516
    FJPGItalianoFrontal     := TBitMap.Create;   // SS_1091_CQU_20130516
    FJPGItalianoFrontal2    := TBitMap.Create;   // SS_1091_CQU_20130516
    FJPGItalianoPosterior   := TBitMap.Create;   // SS_1091_CQU_20130516
    FJPGItalianoPosterior2  := TBitMap.Create;   // SS_1091_CQU_20130516
    // Inicializamos controles de imagenes
    FShiftOriginal      := -1;
    ImagenAmpliar.Scale	:= 1;
	actRemarcarBordes.Checked := False;
	actSuavizarBordes.Checked := False;
    AjustarTama�oTImagePlus(ImagenAmpliar);    // SS_1374A_CQU_20151002
end;

procedure TformEdicionInfraccion.actImageOverviewExecute(Sender: TObject);
begin
    if FFrmImagenOverview.Showing then FFrmImagenOverview.Hide
    else if FFormOverviewCargado then FFrmImagenOverview.Show;
end;

procedure TformEdicionInfraccion.dbTransitosDrawText(Sender: TCustomDBListEx;
  Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
  var Text: String; var TxtRect: TRect; var ItemWidth: Integer;
  var DefaultDraw: Boolean);
begin

    if cdsObtenerTransitoInfractoresPatente.fieldByName('NumCorrCA').AsString = FImagenRef then
        if odSelected in State then
            Sender.Canvas.Font.Color := clWindow
        else
            Sender.Canvas.Font.Color := clBlue;

    if cdsObtenerTransitoInfractoresPatente.fieldByName('Anulada').AsBoolean then
        if odSelected in State then
            Sender.Canvas.Font.Color := clWindow
        else
            Sender.Canvas.Font.Color := clRed;

    if Column.FieldName = 'FechaHora' then Text := FormatDateTime('dd/mm/yyy hh:nn', cdsObtenerTransitoInfractoresPatente.fieldByName('FechaHora').AsDateTime);

    if Column.Header.Caption = 'Anulada' then Text := iif(cdsObtenerTransitoInfractoresPatente.fieldByName('Anulada').AsBoolean, 'SI', 'NO');

    if Column.Header.Caption = 'Img. Ref' then Text := iif(cdsObtenerTransitoInfractoresPatente.fieldByName('NumCorrCA').AsString = FImagenRef, 'SI', 'NO');

end;


procedure TformEdicionInfraccion.cdsObtenerTransitoInfractoresPatenteAfterScroll(
  DataSet: TDataSet);
ResourceString
    SIN_IMAGEN = '(Sin Imagen)';
var
    bmp: TBitMap;

  Procedure MostrarImagenBoton(btn: TSpeedButton; aGraphic: TGraphic);
  var
    aRect: TRect;
  begin
      bmp.FreeImage;
      bmp.Assign(aGraphic);
      bmp.Width := btn.Width;
      bmp.Height := btn.Height;
      bmp.TransparentColor := clRed;

      aRect := Rect(0, 0, btn.ClientRect.Right, btn.ClientRect.Bottom);
      bmp.Canvas.StretchDraw(aRect, aGraphic);
      btn.caption := EmptyStr;
      btn.Glyph.Assign(bmp);
      btn.Enabled := true;
  end;

  Procedure NoMostrarImagenBoton(btn: TSpeedButton);
  begin
      btn.Enabled := False;
      btn.Glyph.FreeImage;
      btn.Glyph.ReleaseHandle;
      btn.Glyph := nil;
      btn.Caption := SIN_IMAGEN;
  end;

var
    NumCorrCA: Int64;
    FechaHora: TDateTime;
	Error: TTipoErrorImg;
	DescriError, NombreCorto: AnsiString;
    DataImage: TDataImage;
    EsItaliano : Boolean;   // SS_1091_CQU_20130516
    ImagenTMP: TObject;		// SS_1147W_CQU_20141215
begin

    if not FIniciando then begin
        txtNoHayImagen.Visible := False;
        bmp := TBitMap.Create;
        try
            self.Update;
            Screen.Cursor := crHourGlass;
            //Cargo las imagenes

            NumCorrCA := StrToInt64(cdsObtenerTransitoInfractoresPatente.fieldByName('NumCorrCA').AsString);
            FechaHora := cdsObtenerTransitoInfractoresPatente.fieldByName('FechaHora').Value;
            EsItaliano:= cdsObtenerTransitoInfractoresPatente.FieldByName('EsItaliano').AsBoolean;                      // SS_1091_CQU_20130516

            InicializarVariables;
            //NombreCorto := QueryGetValue(DMConnections.BaseCAC,Format('SELECT Concesionarias.NombreCorto FROM Transitos (NOLOCK) INNER JOIN Concesionarias (NOLOCK) ON Concesionarias.CodigoConcesionaria=Transitos.CodigoConcesionaria WHERE NumCorrCA=%d', [NumCorrCA]));   // SS_1374A_CQU_20151002    //SS-377-NDR-20110607
            NombreCorto := cdsObtenerTransitoInfractoresPatente.FieldByName('NombreCorto').AsString;					// SS_1374A_CQU_20151002

            ImagenTMP := TObject.Create;
            if ObtenerImagenTransito(FImagePathNFI, NombreCorto, NumCorrCA, FechaHora, tiFrontal, ImagenTMP, DataImage, DescriError, Error, FEsKapschFrontal) then begin    // SS_1147X_CQU_20141217
                if FEsKapschFrontal then begin                                                                                                                              // SS_1147X_CQU_20141217
                    FJPG12Frontal := (ImagenTMP as TJPEGPlusImage);                                                                                                         // SS_1147X_CQU_20141217
                    MostrarImagenBoton(sbThumb0, FJPG12Frontal);                                                                                                            // SS_1147X_CQU_20141217
                end else begin                                                                                                                                              // SS_1147X_CQU_20141217
                    FJPGItalianoFrontal := (ImagenTMP as TBitmap);                                                                                                          // SS_1147X_CQU_20141217
                    MostrarImagenBoton(sbThumb0, FJPGItalianoFrontal);                                                                                                      // SS_1147X_CQU_20141217
                end;                                                                                                                                                        // SS_1147X_CQU_20141217
                FPositionPlate[tifrontal] := Rect(                                                                                                                          // SS_1147X_CQU_20141217
                                                    Min(DataImage.DataImageVR.UpperLeftLPN.X, DataImage.DataImageVR.LowerLeftLPN.X),                                        // SS_1147X_CQU_20141217
                                                    Min(DataImage.DataImageVR.UpperLeftLPN.Y, DataImage.DataImageVR.UpperRigthLPN.Y),                                       // SS_1147X_CQU_20141217
                                                    Max(DataImage.DataImageVR.LowerRigthLPN.X, DataImage.DataImageVR.UpperRigthLPN.X),                                      // SS_1147X_CQU_20141217
                                                    Max(DataImage.DataImageVR.LowerRigthLPN.Y, DataImage.DataImageVR.LowerLeftLPN.Y));                                      // SS_1147X_CQU_20141217
                sbThumb0.Click;                                                                                                                                             // SS_1147X_CQU_20141217
            end else NoMostrarImagenBoton(sbThumb0);                                                                                                                        // SS_1147X_CQU_20141217

            //if EsItaliano and ObtenerImagenTransito(FImagePathNFI, NombreCorto, NumCorrCA, FechaHora, tiFrontal,                                                          // SS_1147X_CQU_20141217  // SS_1091_CQU_20130516
            //    FJPGItalianoFrontal, DataImage, DescriError, Error) then begin                                                                                            // SS_1147X_CQU_20141217  // SS_1091_CQU_20130516
            //        MostrarImagenBoton(sbThumb0, FJPGItalianoFrontal);                                                                                                    // SS_1147X_CQU_20141217  // SS_1091_CQU_20130516
            //        FPositionPlate[tifrontal] := Rect(                                                                                                                    // SS_1147X_CQU_20141217  // SS_1091_CQU_20130516
            //            Min(DataImage.DataImageVR.UpperLeftLPN.X, DataImage.DataImageVR.LowerLeftLPN.X),                                                                  // SS_1147X_CQU_20141217  // SS_1091_CQU_20130516
            //            Min(DataImage.DataImageVR.UpperLeftLPN.Y, DataImage.DataImageVR.UpperRigthLPN.Y),                                                                 // SS_1147X_CQU_20141217  // SS_1091_CQU_20130516
            //            Max(DataImage.DataImageVR.LowerRigthLPN.X, DataImage.DataImageVR.UpperRigthLPN.X),                                                                // SS_1147X_CQU_20141217  // SS_1091_CQU_20130516
            //            Max(DataImage.DataImageVR.LowerRigthLPN.Y, DataImage.DataImageVR.LowerLeftLPN.Y));                                                                // SS_1147X_CQU_20141217  // SS_1091_CQU_20130516
            //        sbThumb0.Click;                                                                                                                                       // SS_1147X_CQU_20141217  // SS_1091_CQU_20130516
            //end else                                                                                                                                                      // SS_1147X_CQU_20141217  // SS_1091_CQU_20130516
            //if ObtenerImagenTransito(FImagePathNFI, NombreCorto, NumCorrCA, FechaHora, tiFrontal, FJPG12Frontal,                                                          // SS_1147X_CQU_20141217  //SS-377-NDR-20110607
            //    DataImage, DescriError, Error) then begin                                                                                                                 // SS_1147X_CQU_20141217
            //        MostrarImagenBoton(sbThumb0, FJPG12Frontal);                                                                                                          // SS_1147X_CQU_20141217
            //        FPositionPlate[tifrontal] := Rect(                                                                                                                    // SS_1147X_CQU_20141217
            //            Min(DataImage.DataImageVR.UpperLeftLPN.X, DataImage.DataImageVR.LowerLeftLPN.X),                                                                  // SS_1147X_CQU_20141217
            //            Min(DataImage.DataImageVR.UpperLeftLPN.Y, DataImage.DataImageVR.UpperRigthLPN.Y),                                                                 // SS_1147X_CQU_20141217
            //            Max(DataImage.DataImageVR.LowerRigthLPN.X, DataImage.DataImageVR.UpperRigthLPN.X),                                                                // SS_1147X_CQU_20141217
            //            Max(DataImage.DataImageVR.LowerRigthLPN.Y, DataImage.DataImageVR.LowerLeftLPN.Y));                                                                // SS_1147X_CQU_20141217
            //        sbThumb0.Click;                                                                                                                                       // SS_1147X_CQU_20141217
            //end else NoMostrarImagenBoton(sbThumb0);                                                                                                                      // SS_1147X_CQU_20141217

            if ObtenerImagenTransito(FImagePathNFI, NombreCorto, NumCorrCA, FechaHora, tiPosterior, ImagenTMP, DataImage, DescriError, Error, FEsKapschPosterior) then      // SS_1147X_CQU_20141217
            begin                                                                                                                                                           // SS_1147X_CQU_20141217
                if FEsKapschPosterior then begin                                                                                                                            // SS_1147X_CQU_20141217
                    FJPG12Posterior := (ImagenTMP as TJPEGPlusImage);                                                                                                       // SS_1147X_CQU_20141217
                    MostrarImagenBoton(sbThumb1, FJPG12Posterior);                                                                                                          // SS_1147X_CQU_20141217
                end else begin                                                                                                                                              // SS_1147X_CQU_20141217
                    FJPGItalianoPosterior := (ImagenTMP as TBitmap);                                                                                                        // SS_1147X_CQU_20141217
                    MostrarImagenBoton(sbThumb1, FJPGItalianoPosterior);                                                                                                    // SS_1147X_CQU_20141217
                end;                                                                                                                                                        // SS_1147X_CQU_20141217
                FPositionPlate[tiPosterior] := Rect(                                                                                                                        // SS_1147X_CQU_20141217
                                                        Min(DataImage.DataImageVR.UpperLeftLPN.X, DataImage.DataImageVR.LowerLeftLPN.X),                                    // SS_1147X_CQU_20141217
                                                        Min(DataImage.DataImageVR.UpperLeftLPN.Y, DataImage.DataImageVR.UpperRigthLPN.Y),                                   // SS_1147X_CQU_20141217
                                                        Max(DataImage.DataImageVR.LowerRigthLPN.X, DataImage.DataImageVR.UpperRigthLPN.X),                                  // SS_1147X_CQU_20141217
                                                        Max(DataImage.DataImageVR.LowerRigthLPN.Y, DataImage.DataImageVR.LowerLeftLPN.Y));                                  // SS_1147X_CQU_20141217
                if FTipoImagenActual = tiDesconocida then sbThumb1.Click;                                                                                                   // SS_1147X_CQU_20141217
            end else NoMostrarImagenBoton(sbThumb1);                                                                                                                        // SS_1147X_CQU_20141217

            //if EsItaliano and ObtenerImagenTransito(FImagePathNFI, NombreCorto, NumCorrCA, FechaHora, tiPosterior,                                                        // SS_1147X_CQU_20141217 // SS_1091_CQU_20130516
            //    FJPGItalianoPosterior, DataImage, DescriError, Error) then begin                                                                                          // SS_1147X_CQU_20141217 // SS_1091_CQU_20130516
            //        MostrarImagenBoton(sbThumb1, FJPGItalianoPosterior);                                                                                                  // SS_1147X_CQU_20141217 // SS_1091_CQU_20130516
            //        FPositionPlate[tiPosterior] := Rect(                                                                                                                  // SS_1147X_CQU_20141217 // SS_1091_CQU_20130516
            //            Min(DataImage.DataImageVR.UpperLeftLPN.X, DataImage.DataImageVR.LowerLeftLPN.X),                                                                  // SS_1147X_CQU_20141217 // SS_1091_CQU_20130516
            //            Min(DataImage.DataImageVR.UpperLeftLPN.Y, DataImage.DataImageVR.UpperRigthLPN.Y),                                                                 // SS_1147X_CQU_20141217 // SS_1091_CQU_20130516
            //            Max(DataImage.DataImageVR.LowerRigthLPN.X, DataImage.DataImageVR.UpperRigthLPN.X),                                                                // SS_1147X_CQU_20141217 // SS_1091_CQU_20130516
            //            Max(DataImage.DataImageVR.LowerRigthLPN.Y, DataImage.DataImageVR.LowerLeftLPN.Y));                                                                // SS_1147X_CQU_20141217 // SS_1091_CQU_20130516
            //        if FTipoImagenActual = tiDesconocida then sbThumb1.Click;                                                                                             // SS_1147X_CQU_20141217 // SS_1091_CQU_20130516
            //end else                                                                                                                                                      // SS_1147X_CQU_20141217 // SS_1091_CQU_20130516
            //if ObtenerImagenTransito(FImagePathNFI, NombreCorto, NumCorrCA, FechaHora, tiPosterior, FJPG12Posterior,                                                      // SS_1147X_CQU_20141217 //SS-377-NDR-20110607
            //    DataImage, DescriError, Error) then begin                                                                                                                 // SS_1147X_CQU_20141217
            //        MostrarImagenBoton(sbThumb1, FJPG12Posterior);                                                                                                        // SS_1147X_CQU_20141217
            //        FPositionPlate[tiPosterior] := Rect(                                                                                                                  // SS_1147X_CQU_20141217
            //            Min(DataImage.DataImageVR.UpperLeftLPN.X, DataImage.DataImageVR.LowerLeftLPN.X),                                                                  // SS_1147X_CQU_20141217
            //            Min(DataImage.DataImageVR.UpperLeftLPN.Y, DataImage.DataImageVR.UpperRigthLPN.Y),                                                                 // SS_1147X_CQU_20141217
            //            Max(DataImage.DataImageVR.LowerRigthLPN.X, DataImage.DataImageVR.UpperRigthLPN.X),                                                                // SS_1147X_CQU_20141217
            //            Max(DataImage.DataImageVR.LowerRigthLPN.Y, DataImage.DataImageVR.LowerLeftLPN.Y));                                                                // SS_1147X_CQU_20141217
            //        if FTipoImagenActual = tiDesconocida then sbThumb1.Click;                                                                                             // SS_1147X_CQU_20141217
            //end else NoMostrarImagenBoton(sbThumb1);                                                                                                                      // SS_1147X_CQU_20141217

            if ObtenerImagenTransito(FImagePathNFI, NombreCorto, NumCorrCA, FechaHora, tiOverview1, FImgOverview1,                  //SS-377-NDR-20110607
                DataImage, DescriError, Error) then begin
                    MostrarImagenBoton(sbThumb2, FImgOverview1);
                    FPositionPlate[tiOverview1] := Rect(
                        Min(DataImage.DataImageVR.UpperLeftLPN.X, DataImage.DataImageVR.LowerLeftLPN.X),
                        Min(DataImage.DataImageVR.UpperLeftLPN.Y, DataImage.DataImageVR.UpperRigthLPN.Y),
                        Max(DataImage.DataImageVR.LowerRigthLPN.X, DataImage.DataImageVR.UpperRigthLPN.X),
                        Max(DataImage.DataImageVR.LowerRigthLPN.Y, DataImage.DataImageVR.LowerLeftLPN.Y));
                    if FTipoImagenActual = tiDesconocida then sbThumb2.Click;
            end else NoMostrarImagenBoton(sbThumb2);

            if ObtenerImagenTransito(FImagePathNFI, NombreCorto, NumCorrCA, FechaHora, tiOverview2, FImgOverview2,                  //SS-377-NDR-20110607
                DataImage, DescriError, Error) then begin
                    MostrarImagenBoton(sbThumb3, FImgOverview2);
                    FPositionPlate[tiOverview2] := Rect(
                        Min(DataImage.DataImageVR.UpperLeftLPN.X, DataImage.DataImageVR.LowerLeftLPN.X),
                        Min(DataImage.DataImageVR.UpperLeftLPN.Y, DataImage.DataImageVR.UpperRigthLPN.Y),
                        Max(DataImage.DataImageVR.LowerRigthLPN.X, DataImage.DataImageVR.UpperRigthLPN.X),
                        Max(DataImage.DataImageVR.LowerRigthLPN.Y, DataImage.DataImageVR.LowerLeftLPN.Y));
                    if FTipoImagenActual = tiDesconocida then sbThumb3.Click;
            end else NoMostrarImagenBoton(sbThumb3);

            if ObtenerImagenTransito(FImagePathNFI, NombreCorto, NumCorrCA, FechaHora, tiOverview3, FImgOverview3,                  //SS-377-NDR-20110607
                DataImage, DescriError, Error) then begin
                    MostrarImagenBoton(sbThumb4, FImgOverview3);
                    FPositionPlate[tiOverview3] := Rect(
                        Min(DataImage.DataImageVR.UpperLeftLPN.X, DataImage.DataImageVR.LowerLeftLPN.X),
                        Min(DataImage.DataImageVR.UpperLeftLPN.Y, DataImage.DataImageVR.UpperRigthLPN.Y),
                        Max(DataImage.DataImageVR.LowerRigthLPN.X, DataImage.DataImageVR.UpperRigthLPN.X),
                        Max(DataImage.DataImageVR.LowerRigthLPN.Y, DataImage.DataImageVR.LowerLeftLPN.Y));
                    if FTipoImagenActual = tiDesconocida then sbThumb4.Click;
            end else NoMostrarImagenBoton(sbThumb4);

            if ObtenerImagenTransito(FImagePathNFI, NombreCorto, NumCorrCA, FechaHora,  tiFrontal2, ImagenTMP, DataImage, DescriError, Error, FEsKapschFrontal2) then   // SS_1147X_CQU_20141217
            begin                                                                                                                                                       // SS_1147X_CQU_20141217
                if FEsKapschFrontal2 then begin                                                                                                                         // SS_1147X_CQU_20141217
                    FJPG12Frontal2 := (ImagenTMP as TJPEGPlusImage);                                                                                                    // SS_1147X_CQU_20141217
                    MostrarImagenBoton(sbThumb5, FJPG12Frontal2);                                                                                                       // SS_1147X_CQU_20141217
                end else begin                                                                                                                                          // SS_1147X_CQU_20141217
                    FJPGItalianoFrontal2 := (ImagenTMP as TBitmap);                                                                                                     // SS_1147X_CQU_20141217
                    MostrarImagenBoton(sbThumb5, FJPGItalianoFrontal2);                                                                                                 // SS_1147X_CQU_20141217
                end;                                                                                                                                                    // SS_1147X_CQU_20141217
                                                                                                                                                                        // SS_1147X_CQU_20141217
                FPositionPlate[tiFrontal2] := Rect(                                                                                                                     // SS_1147X_CQU_20141217
                                                    Min(DataImage.DataImageVR.UpperLeftLPN.X, DataImage.DataImageVR.LowerLeftLPN.X),                                    // SS_1147X_CQU_20141217
                                                    Min(DataImage.DataImageVR.UpperLeftLPN.Y, DataImage.DataImageVR.UpperRigthLPN.Y),                                   // SS_1147X_CQU_20141217
                                                    Max(DataImage.DataImageVR.LowerRigthLPN.X, DataImage.DataImageVR.UpperRigthLPN.X),                                  // SS_1147X_CQU_20141217
                                                    Max(DataImage.DataImageVR.LowerRigthLPN.Y, DataImage.DataImageVR.LowerLeftLPN.Y));                                  // SS_1147X_CQU_20141217
                if FTipoImagenActual = tiDesconocida then sbThumb5.Click;                                                                                               // SS_1147X_CQU_20141217
            end else NoMostrarImagenBoton(sbThumb5);                                                                                                                    // SS_1147X_CQU_20141217

            //if EsItaliano and ObtenerImagenTransito(FImagePathNFI, NombreCorto, NumCorrCA, FechaHora, tiFrontal2,                                                     // SS_1147X_CQU_20141217 // SS_1091_CQU_20130516
            //    FJPGItalianoFrontal2, DataImage, DescriError, Error) then begin                                                                                       // SS_1147X_CQU_20141217 // SS_1091_CQU_20130516
            //        MostrarImagenBoton(sbThumb5, FJPGItalianoFrontal2);                                                                                               // SS_1147X_CQU_20141217 // SS_1091_CQU_20130516
            //        FPositionPlate[tiFrontal2] := Rect(                                                                                                               // SS_1147X_CQU_20141217 // SS_1091_CQU_20130516
            //            Min(DataImage.DataImageVR.UpperLeftLPN.X, DataImage.DataImageVR.LowerLeftLPN.X),                                                              // SS_1147X_CQU_20141217 // SS_1091_CQU_20130516
            //            Min(DataImage.DataImageVR.UpperLeftLPN.Y, DataImage.DataImageVR.UpperRigthLPN.Y),                                                             // SS_1147X_CQU_20141217 // SS_1091_CQU_20130516
            //            Max(DataImage.DataImageVR.LowerRigthLPN.X, DataImage.DataImageVR.UpperRigthLPN.X),                                                            // SS_1147X_CQU_20141217 // SS_1091_CQU_20130516
            //             Max(DataImage.DataImageVR.LowerRigthLPN.Y, DataImage.DataImageVR.LowerLeftLPN.Y));                                                           // SS_1147X_CQU_20141217 // SS_1091_CQU_20130516
            //        if FTipoImagenActual = tiDesconocida then sbThumb5.Click;                                                                                         // SS_1147X_CQU_20141217 // SS_1091_CQU_20130516
            //end else                                                                                                                                                  // SS_1147X_CQU_20141217 // SS_1091_CQU_20130516
            //if ObtenerImagenTransito(FImagePathNFI, NombreCorto, NumCorrCA, FechaHora, tiFrontal2, FJPG12Frontal2,                                                    // SS_1147X_CQU_20141217 //SS-377-NDR-20110607
            //    DataImage, DescriError, Error) then begin                                                                                                             // SS_1147X_CQU_20141217
            //        MostrarImagenBoton(sbThumb5, FJPG12Frontal2);                                                                                                     // SS_1147X_CQU_20141217
            //        FPositionPlate[tiFrontal2] := Rect(                                                                                                               // SS_1147X_CQU_20141217
            //            Min(DataImage.DataImageVR.UpperLeftLPN.X, DataImage.DataImageVR.LowerLeftLPN.X),                                                              // SS_1147X_CQU_20141217
            //            Min(DataImage.DataImageVR.UpperLeftLPN.Y, DataImage.DataImageVR.UpperRigthLPN.Y),                                                             // SS_1147X_CQU_20141217
            //            Max(DataImage.DataImageVR.LowerRigthLPN.X, DataImage.DataImageVR.UpperRigthLPN.X),                                                            // SS_1147X_CQU_20141217
            //             Max(DataImage.DataImageVR.LowerRigthLPN.Y, DataImage.DataImageVR.LowerLeftLPN.Y));                                                           // SS_1147X_CQU_20141217
            //        if FTipoImagenActual = tiDesconocida then sbThumb5.Click;                                                                                         // SS_1147X_CQU_20141217
            //end else NoMostrarImagenBoton(sbThumb5);                                                                                                                  // SS_1147X_CQU_20141217

            if ObtenerImagenTransito(FImagePathNFI, NombreCorto, NumCorrCA, FechaHora, tiPosterior2, ImagenTMP, DataImage, DescriError, Error, FEsKapschPosterior2)     // SS_1147X_CQU_20141217
            then begin                                                                                                                                                  // SS_1147X_CQU_20141217
                if FEsKapschPosterior2 then begin                                                                                                                       // SS_1147X_CQU_20141217
                    FJPG12Posterior2 := (ImagenTMP as TJPEGPlusImage);                                                                                                  // SS_1147X_CQU_20141217
                    MostrarImagenBoton(sbThumb6, FJPG12Posterior2);                                                                                                     // SS_1147X_CQU_20141217
                end else begin                                                                                                                                          // SS_1147X_CQU_20141217
                    FJPGItalianoPosterior2 := (ImagenTMP as TBitmap);                                                                                                   // SS_1147X_CQU_20141217
                    MostrarImagenBoton(sbThumb6, FJPGItalianoPosterior2);                                                                                               // SS_1147X_CQU_20141217
                end;                                                                                                                                                    // SS_1147X_CQU_20141217
                                                                                                                                                                        // SS_1147X_CQU_20141217
                FPositionPlate[tiPosterior2] := Rect(                                                                                                                   // SS_1147X_CQU_20141217
                                                    Min(DataImage.DataImageVR.UpperLeftLPN.X, DataImage.DataImageVR.LowerLeftLPN.X),                                    // SS_1147X_CQU_20141217
                                                    Min(DataImage.DataImageVR.UpperLeftLPN.Y, DataImage.DataImageVR.UpperRigthLPN.Y),                                   // SS_1147X_CQU_20141217
                                                    Max(DataImage.DataImageVR.LowerRigthLPN.X, DataImage.DataImageVR.UpperRigthLPN.X),                                  // SS_1147X_CQU_20141217
                                                    Max(DataImage.DataImageVR.LowerRigthLPN.Y, DataImage.DataImageVR.LowerLeftLPN.Y));                                  // SS_1147X_CQU_20141217
                if FTipoImagenActual = tiDesconocida then sbThumb6.Click;                                                                                               // SS_1147X_CQU_20141217
            end else NoMostrarImagenBoton(sbThumb6);                                                                                                                    // SS_1147X_CQU_20141217

            //if EsItaliano and ObtenerImagenTransito(FImagePathNFI, NombreCorto, NumCorrCA, FechaHora, tiPosterior2,                                                   // SS_1147X_CQU_20141217 // SS_1091_CQU_20130516
            //    FJPGItalianoPosterior2, DataImage, DescriError, Error) then begin                                                                                     // SS_1147X_CQU_20141217 // SS_1091_CQU_20130516
            //        MostrarImagenBoton(sbThumb6, FJPGItalianoPosterior2);                                                                                             // SS_1147X_CQU_20141217 // SS_1091_CQU_20130516
            //        FPositionPlate[tiPosterior2] := Rect(                                                                                                             // SS_1147X_CQU_20141217 // SS_1091_CQU_20130516
            //            Min(DataImage.DataImageVR.UpperLeftLPN.X, DataImage.DataImageVR.LowerLeftLPN.X),                                                              // SS_1147X_CQU_20141217 // SS_1091_CQU_20130516
            //            Min(DataImage.DataImageVR.UpperLeftLPN.Y, DataImage.DataImageVR.UpperRigthLPN.Y),                                                             // SS_1147X_CQU_20141217 // SS_1091_CQU_20130516
            //            Max(DataImage.DataImageVR.LowerRigthLPN.X, DataImage.DataImageVR.UpperRigthLPN.X),                                                            // SS_1147X_CQU_20141217 // SS_1091_CQU_20130516
            //            Max(DataImage.DataImageVR.LowerRigthLPN.Y, DataImage.DataImageVR.LowerLeftLPN.Y));                                                            // SS_1147X_CQU_20141217 // SS_1091_CQU_20130516
            //        if FTipoImagenActual = tiDesconocida then sbThumb6.Click;                                                                                         // SS_1147X_CQU_20141217 // SS_1091_CQU_20130516
            //end else                                                                                                                                                  // SS_1147X_CQU_20141217 // SS_1091_CQU_20130516
            //if ObtenerImagenTransito(FImagePathNFI, NombreCorto, NumCorrCA, FechaHora, tiPosterior2, FJPG12Posterior2,                                                // SS_1147X_CQU_20141217 //SS-377-NDR-20110607
            //    DataImage, DescriError, Error) then begin                                                                                                             // SS_1147X_CQU_20141217
            //        MostrarImagenBoton(sbThumb6, FJPG12Posterior2);                                                                                                   // SS_1147X_CQU_20141217
            //        FPositionPlate[tiPosterior2] := Rect(                                                                                                             // SS_1147X_CQU_20141217
            //            Min(DataImage.DataImageVR.UpperLeftLPN.X, DataImage.DataImageVR.LowerLeftLPN.X),                                                              // SS_1147X_CQU_20141217
            //            Min(DataImage.DataImageVR.UpperLeftLPN.Y, DataImage.DataImageVR.UpperRigthLPN.Y),                                                             // SS_1147X_CQU_20141217
            //            Max(DataImage.DataImageVR.LowerRigthLPN.X, DataImage.DataImageVR.UpperRigthLPN.X),                                                            // SS_1147X_CQU_20141217
            //            Max(DataImage.DataImageVR.LowerRigthLPN.Y, DataImage.DataImageVR.LowerLeftLPN.Y));                                                            // SS_1147X_CQU_20141217
            //        if FTipoImagenActual = tiDesconocida then sbThumb6.Click;                                                                                         // SS_1147X_CQU_20141217
            //end else NoMostrarImagenBoton(sbThumb6);                                                                                                                  // SS_1147X_CQU_20141217

            //Habilito o no los componenetes, deacuerdo a si tiene imagenes
            EnableControlsInContainer(gbImagenAmpliar, FTipoImagenActual <> tiDesconocida);
            chk_VerMascara.Enabled := (FTipoImagenActual <> tiDesconocida);
            btnImgRef.Enabled := (FTipoImagenActual <> tiDesconocida)
                                    and FPuedeSetImgRef
                                    and (not cdsObtenerTransitoInfractoresPatente.fieldByName('Anulada').AsBoolean);


            if (FTipoImagenActual = tiDesconocida) then begin
                ImagenAmpliar.Visible := False;
                pnlImgPatente.Caption := CAPTION_POS_PATENTE;
                imgPatente.Picture.Bitmap := Nil;
            end else ImagenAmpliar.Visible := True;

            if btnGuardar.Enabled then btnGuardar.SetFocus
            else btn_Salir.SetFocus;
        finally
            bmp.free;
            Screen.Cursor := crDefault;
        end;
        txtNoHayImagen.Visible := FTipoImagenActual = tiDesconocida;
        if txtNoHayImagen.Visible then txtNoHayImagen.Left := ((ImagenAmpliar.Width - txtNoHayImagen.Width) div 2) + ImagenAmpliar.Left;
    end;
end;


procedure TformEdicionInfraccion.chk_VerMascaraClick(Sender: TObject);
begin
    Redibujar(chk_VerMascara.Checked);
end;

procedure TformEdicionInfraccion.FormCreate(Sender: TObject);
begin
    Act := TDMActualizarDatosInfractor.Create(Self); // se destruye cuendo se destruye el Owner
end;

procedure TformEdicionInfraccion.FormMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    if Cursor = crHourGlass then exit; // Asi no hace nada
end;

{-------------------------------------------------------------------------------
 Revision 1:
    Author : nefernandez
    Date : 01/11/2007
    Description : Cuando se selecciona el boton se agrega un log de esta accion

 Revision 2:
    Author : nefernandez
    Date : 03/12/2008
    Description : Cuando se selecciona el boton se "activa" este evento para luego
    registrarlo en el log solo si se selecciona "Guardar Cambios"
-------------------------------------------------------------------------------}
procedure TformEdicionInfraccion.btnImgRefClick(Sender: TObject);
var                                                                                         // SS_1138_CQU_20131009
    bmp: TBitmap;                                                                           // SS_1138_CQU_20131009
    jpg: TJPEGImage;                                                                        // SS_1138_CQU_20131009
    EsItaliano : Boolean;                                                                   // SS_1138_CQU_20131009
begin
    FEventoImgRef := True; //Revision 2

    FImagenRef :=  cdsObtenerTransitoInfractoresPatente.fieldByName('NumCorrCA').AsString;
    EsItaliano :=  cdsObtenerTransitoInfractoresPatente.fieldByName('EsItaliano').AsBoolean;// SS_1138_CQU_20131009
    FRegistrationAccessibilityRef := RegistrationAccessibilityImagen[FTipoImagenActual];    // SS_1138_CQU_20131009
    RepaintBarState;
    dbTransitos.Repaint;

    //Captura la imagen seleccionada, la que se almacenara (segun el estado) al grabar.     // SS_1138_CQU_20131009
    bmp             :=  TBitmap.create;                                                     // SS_1138_CQU_20131009
    bmp.PixelFormat :=  pf24bit;                                                            // SS_1138_CQU_20131009
    jpg             :=  TJPEGImage.Create;                                                  // SS_1138_CQU_20131009
    // Imagen Frontal                                                                       // SS_1138_CQU_20131009
    if FTipoImagenActual = tiFrontal then begin                                             // SS_1138_CQU_20131009
        //if EsItaliano then begin                                                          // SS_1147X_CQU_20141217  // SS_1138_CQU_20131009
        if not FEsKapschFrontal then begin                                                  // SS_1147X_CQU_20141217
            bmp.Assign(FJPGItalianoFrontal);                                                // SS_1138_CQU_20131009
            SeleccionarImagen(FJPGItalianoFrontal, FTipoImagenActual);                      // SS_1138_CQU_20131009
        end else begin                                                                      // SS_1138_CQU_20131009
            bmp.Assign(FJPG12Frontal);                                                      // SS_1138_CQU_20131009
            SeleccionarImagen(FJPG12Frontal, FTipoImagenActual);                            // SS_1138_CQU_20131009
        end;                                                                                // SS_1138_CQU_20131009
    // Imagen Frontal 2                                                                     // SS_1138_CQU_20131009
    end else if FTipoImagenActual = tiFrontal2 then begin                                   // SS_1138_CQU_20131009
        //if EsItaliano then begin                                                          // SS_1147X_CQU_20141217  // SS_1138_CQU_20131009
        if not FEsKapschFrontal2 then begin                                                 // SS_1147X_CQU_20141217
            bmp.Assign(FJPGItalianoFrontal2);                                               // SS_1138_CQU_20131009
            SeleccionarImagen(FJPGItalianoFrontal2, FTipoImagenActual)                      // SS_1138_CQU_20131009
        end else begin                                                                      // SS_1138_CQU_20131009
            bmp.Assign(FJPG12Frontal2);                                                     // SS_1138_CQU_20131009
            SeleccionarImagen(FJPG12Frontal2, FTipoImagenActual);                           // SS_1138_CQU_20131009
        end;                                                                                // SS_1138_CQU_20131009
    // Imagen Posterior                                                                     // SS_1138_CQU_20131009
    end else if FTipoImagenActual = tiPosterior then begin                                  // SS_1138_CQU_20131009
        //if EsItaliano then begin                                                          // SS_1147X_CQU_20141217  // SS_1138_CQU_20131009
        if not FEsKapschPosterior then begin                                                // SS_1147X_CQU_20141217
            bmp.Assign(FJPGItalianoPosterior);                                              // SS_1138_CQU_20131009
            SeleccionarImagen(FJPGItalianoPosterior, FTipoImagenActual)                     // SS_1138_CQU_20131009
        end else begin                                                                      // SS_1138_CQU_20131009
            bmp.Assign(FJPG12Posterior);                                                    // SS_1138_CQU_20131009
            SeleccionarImagen(FJPG12Posterior, FTipoImagenActual);                          // SS_1138_CQU_20131009
        end;                                                                                // SS_1138_CQU_20131009
    // Imagen Posterior 2                                                                   // SS_1138_CQU_20131009
    end else if FTipoImagenActual = tiPosterior2 then begin                                 // SS_1138_CQU_20131009
        //if EsItaliano then begin                                                          // SS_1147X_CQU_20141217  // SS_1138_CQU_20131009
        if not FEsKapschPosterior2 then begin                                               // SS_1147X_CQU_20141217
            bmp.Assign(FJPGItalianoPosterior2);                                             // SS_1138_CQU_20131009
            SeleccionarImagen(FJPGItalianoPosterior2, FTipoImagenActual)                    // SS_1138_CQU_20131009
        end else begin                                                                      // SS_1138_CQU_20131009
            bmp.Assign(FJPG12Posterior2);                                                   // SS_1138_CQU_20131009
            SeleccionarImagen(FJPG12Posterior2, FTipoImagenActual);                         // SS_1138_CQU_20131009
        end;                                                                                // SS_1138_CQU_20131009
    end;                                                                                    // SS_1138_CQU_20131009

    jpg.Assign(bmp);                                                                        // SS_1138_CQU_20131009
    ImagenAGrabar.Picture.Assign(jpg);                                                      // SS_1138_CQU_20131009
    bmp.Free;                                                                               // SS_1138_CQU_20131009
    jpg.Free;                                                                               // SS_1138_CQU_20131009

end;

{-------------------------------------------------------------------------------
 Revision 1:
    Author : nefernandez
    Date : 01/11/2007
    Description : Cuando se selecciona el boton se agrega un log de esta accion

 Revision 2:
    Author : nefernandez
    Date : 03/12/2008
    Description : Cuando se selecciona el boton se "activa" este evento para luego
    registrarlo en el log solo si se selecciona "Guardar Cambios"
-------------------------------------------------------------------------------}
procedure TformEdicionInfraccion.btnSinImgRefClick(Sender: TObject);
begin
    FEventoSinImgRef := True; //Revision 2

    FImagenRef := '-1';
    FRegistrationAccessibilityRef := RegistrationAccessibilityImagen[tiDesconocida];        // SS_1138_CQU_20131009
    RepaintBarState;
    dbTransitos.Repaint;
end;

procedure TformEdicionInfraccion.RepaintBarState;
begin
    if FImagenRef = '-1' then lblSetImagen.Caption := SIN_IMG
    else lblSetImagen.Caption := IMG;
end;

{
    Revision: 1
    Author: pdominguez
    Date: 12/5/2010
    Description: Infractores Fase 2
        - Se a�ade el par�metro CodigoUsuarioAnulacion al SP ActualizarDatoInfraccion
}
procedure TformEdicionInfraccion.btnGuardarClick(Sender: TObject);
    resourcestring
        BTN_ELEGIR_IMAGEN_JPL = 'Boton Elegir Imagen JPL';
        BTN_SIN_IMAGEN_JPL = 'Boton Sin Imagen JPL';
        LBL_ANULAR_INFRACCION = 'Anular Infraccion';
        MSG_ERROR_REGISTRACION_AUDITORIA = 'Error registrando el log de auditor�a del sistema';
	    MSG_CONFIRMACION      = 'La patente %s se encuentra en estado de seguimiento. �Desea continuar?'; // Rev. 4 SS 931
	var
        f: TfrmExplorarInfraciones;
        Puntero: TBookmark;
begin
    try
    //if (EsPatenteEnSeguimiento(DMConnection.DMConnections.BaseCAC, Trim(lblpatente.Caption)) = True) then begin // SS_1374A_CQU_20151002  // Rev. 4 SS 931
    if FEstaEnSeguimiento then begin                                                                              // SS_1374A_CQU_20151002
       if  MsgBox(Format(MSG_CONFIRMACION,[Trim(lblpatente.Caption)]), Caption, MB_YESNO) = IDNO then Exit;
    end;

  	DMConnections.BaseCAC.BeginTrans;


        if FEventoImgRef then begin
            if not RegistrarLogAuditoriaAcciones(DMConnections.BaseCAC, SYS_GESTION_INFRACCIONES, Self.Caption, BTN_ELEGIR_IMAGEN_JPL, UsuarioSistema, GetMachineName, '', FCodigoInfraccion)  then
                raise Exception.create(MSG_ERROR_REGISTRACION_AUDITORIA);
        end;

        if FEventoSinImgRef then begin
            if not RegistrarLogAuditoriaAcciones(DMConnections.BaseCAC, SYS_GESTION_INFRACCIONES, Self.Caption, BTN_SIN_IMAGEN_JPL, UsuarioSistema, GetMachineName, '', FCodigoInfraccion)  then
                raise Exception.create(MSG_ERROR_REGISTRACION_AUDITORIA);
        end;

        // Rev. 1 (Fase 2)
        with ActualizarDatoInfraccion do begin
            Close;
            Parameters.ParamByName('@CodigoInfraccion').Value := FCodigoInfraccion;
            Parameters.ParamByName('@MotivoAnulacion').Value := iif(cbMotivoAnulacion.Enabled, cbMotivoAnulacion.Value, null);
            //Parameters.ParamByName('@RefNumCorrCA').Value := FImagenRef;                                      // SS_1138_CQU_20131009
            //Parameters.ParamByName('@RefRegistrationAccessibility').Value := 1;                               // SS_1138_CQU_20131009
            if FImagenRef = '-1' then begin                                                                     // SS_1138_CQU_20131009
                Parameters.ParamByName('@RefNumCorrCA').Value := null;                                          // SS_1138_CQU_20131009
                Parameters.ParamByName('@RefRegistrationAccessibility').Value := null;                          // SS_1138_CQU_20131009
            end else begin                                                                                      // SS_1138_CQU_20131009
                Parameters.ParamByName('@RefNumCorrCA').Value := FImagenRef;                                    // SS_1138_CQU_20131009
                Parameters.ParamByName('@RefRegistrationAccessibility').Value := FRegistrationAccessibilityRef; // SS_1138_CQU_20131009
            end;                                                                                                // SS_1138_CQU_20131009
            Parameters.ParamByName('@CodigoUsuarioAnulacion').Value := UsuarioSistema;
            ExecProc;
        end;
        // Fin Rev. 1 (Fase 2)

        if cbMotivoAnulacion.Enabled then begin
            if not RegistrarLogAuditoriaAcciones(DMConnections.BaseCAC, SYS_GESTION_INFRACCIONES, Self.Caption, LBL_ANULAR_INFRACCION, UsuarioSistema, GetMachineName, '', FCodigoInfraccion)  then
                raise Exception.create(MSG_ERROR_REGISTRACION_AUDITORIA);

        end;

        // GrabarImagen                                                                                         // SS_1135_CQU_20131030
        if FPuedeSetImgRef and (FRegistrationAccessibilityRef > 0) then                                         // SS_1135_CQU_20131030
            GrabarImagenReferencia(FRegistrationAccessibilityRef);                                              // SS_1135_CQU_20131030

        DMConnections.BaseCAC.CommitTrans;

        if FindFormOrCreate(TfrmExplorarInfraciones, f) then
            with f.spExplorarInfracciones do try
                Puntero := GetBookmark;
                Close;
                Open;
                GotoBookmark(Puntero);
            finally
                FreeBookmark(Puntero);
            end;
    except
        on e: Exception do begin
            DMConnections.BaseCAC.RollbackTrans;
            MsgBoxErr(ERROR_GUARDAR, e.Message, Caption, MB_ICONSTOP);
        end;
    end;
    Close;
end;

{*****************************************************************************
  Function Name:
  Author: dcalani
  Date Created: 18/07/2005
  Description: Posiciona el ItemIndex del combo de Motivo de Infracciones en la pos
                del codigo que le pasan.
  Parameters: Not available
  Return Value: Not available
*****************************************************************************}
procedure TformEdicionInfraccion.PosCombo(CodigoInfraccion: Integer);
var
    i, j: Integer;
begin
    i := 0;

    for j := 0 to cbMotivoAnulacion.Items.Count - 1 do begin
        cbMotivoAnulacion.ItemIndex := j;
        if cbMotivoAnulacion.Value = CodigoInfraccion then i := j;
    end;

    cbMotivoAnulacion.ItemIndex := i;
end;

{
    Revision: 1
    Author: pdominguez
    Date: 12/5/2010
    Description: Infractores Fase 2
        - Se cambia la descripci�n de los labels si se decide anular la infracci�n.
}
procedure TformEdicionInfraccion.lblAnularInfraccionClick(Sender: TObject);
ResourceString
    MOTIVO_ANULACION = 'Anulada (Sin Guardar)';
begin
    if not lblAnularInfraccion.Enabled then Exit;

    CargarComboMotivoAnulacion(DMConnections.BaseCAC, cbMotivoAnulacion, False, True); // Cargo el Combo de Motivos de Anulaci�n
    cbMotivoAnulacion.ItemIndex     := 0;
    cbMotivoAnulacion.Enabled       := True;
    // Rev. 1 (Fase 2)
    if ObtenerDatosInfraccion.FieldByName('CodigoEstadoInterno').AsInteger = CONST_ESTADO_INFRACCION_INTERNO_PENDIENTE then lblEstadoCN.Caption := MOTIVO_ANULACION;
    if ObtenerDatosInfraccion.FieldByName('CodigoEstadoExterno').AsInteger = CONST_ESTADO_INFRACCION_EXTERNO_PENDIENTE_ENVIO then lblEstadoIF.Caption := MOTIVO_ANULACION;
    //lblEstado.Caption               := MOTIVO_ANULACION;
    // Fin Rev. 1 (Fase 2)
    lblLabelMotivoAnulacion.Enabled := True;
    lblLabelMotivoRechazo.Visible   := False;
    lblMotivoRechazo.Visible        := False;
end;


{-----------------------------------------------------------------------------
Procedure Name  :   actImagenFrontal1Execute
Date Created    :   10-Octubre-2013
Description     :   Habilita los Botones F1
Firma           :   SS_1138_CQU_20131009
-----------------------------------------------------------------------------}
procedure TformEdicionInfraccion.actImagenFrontal1Execute(Sender: TObject);
begin
    sbThumb0Click(sbThumb0);
	sbThumb0.Down := true;
    //Redibujar(False);                        // SS_1374A_CQU_20151002
    Redibujar(chk_VerMascara.Checked);         // SS_1374A_CQU_20151002
    CentrarImagen;
    AjustarTama�oTImagePlus(ImagenAmpliar);    // SS_1374A_CQU_20151002
end;

{-----------------------------------------------------------------------------
Procedure Name  :   actImagenFrontal1Execute
Date Created    :   10-Octubre-2013
Description     :   Habilita los Botones F3
Firma           :   SS_1138_CQU_20131009
-----------------------------------------------------------------------------}
procedure TformEdicionInfraccion.actImagenFrontal2Execute(Sender: TObject);
begin
    sbThumb5Click(sbThumb5);
    sbThumb5.down := true;
    //Redibujar(False);                        // SS_1374A_CQU_20151002
    Redibujar(chk_VerMascara.Checked);         // SS_1374A_CQU_20151002
    CentrarImagen;
    AjustarTama�oTImagePlus(ImagenAmpliar);    // SS_1374A_CQU_20151002
end;

{-----------------------------------------------------------------------------
Procedure Name  :   actImagenFrontal1Execute
Date Created    :   10-Octubre-2013
Description     :   Habilita los Botones F2
Firma           :   SS_1138_CQU_20131009
-----------------------------------------------------------------------------}
procedure TformEdicionInfraccion.actImagenPosterior1Execute(Sender: TObject);
begin
    sbThumb1Click(sbThumb1);
    sbThumb1.Down := true;
	//Redibujar(False);                        // SS_1374A_CQU_20151002
    Redibujar(chk_VerMascara.Checked);         // SS_1374A_CQU_20151002
    CentrarImagen;
    AjustarTama�oTImagePlus(ImagenAmpliar);    // SS_1374A_CQU_20151002
end;

{-----------------------------------------------------------------------------
Procedure Name  :   actImagenFrontal1Execute
Date Created    :   10-Octubre-2013
Description     :   Habilita los Botones F4
Firma           :   SS_1138_CQU_20131009
-----------------------------------------------------------------------------}
procedure TformEdicionInfraccion.actImagenPosterior2Execute(Sender: TObject);
begin
    sbThumb6Click(sbThumb6);
    sbThumb6.down := true;
    //Redibujar(False);                        // SS_1374A_CQU_20151002
    Redibujar(chk_VerMascara.Checked);         // SS_1374A_CQU_20151002
    CentrarImagen;
    AjustarTama�oTImagePlus(ImagenAmpliar);    // SS_1374A_CQU_20151002
end;

{-----------------------------------------------------------------------------
Procedure Name  :   GrabarImagenReferencia
Date Created    :   15-Octubre-2013
Description     :   Graba la imagen de referencia en el directorio
Firma           :   SS_1135_CQU_20131030
-----------------------------------------------------------------------------}
procedure TformEdicionInfraccion.GrabarImagenReferencia(RegistrationAccessibilityRef : Integer);
resourcestring
    MSG_ERROR_NO_EXISTE =   'El Archivo %s no existe';
    MSG_ERROR_COPIANDO  =   'No se pudo copiar el archivo %s en el directorio %s';
    MSG_ERROR_ACCESO    =   'No se pudo acceder a la ruta %s';
var
    FechaPath, Directorio,
    NombreCorto, Archivo,
    ArchivoOrigen           : AnsiString;
    TipoImagenSeleccionada  : TTipoImagen;
    //SR                      : TSearchRec; // SS_1138B_CQU_20140122
    NumCorrCA               : Int64;
    FechaHora               : TDateTime;
begin
    // Obtengo los valores a utilizar
    NumCorrCA   := StrToInt64(cdsObtenerTransitoInfractoresPatente.fieldByName('NumCorrCA').AsString);
    FechaHora   := cdsObtenerTransitoInfractoresPatente.fieldByName('FechaHora').Value;
    //NombreCorto := SysUtilsCN.QueryGetStringValue(DMConnections.BaseCAC,Format('SELECT Concesionarias.NombreCorto FROM Transitos (NOLOCK) INNER JOIN Concesionarias (NOLOCK) ON Concesionarias.CodigoConcesionaria=Transitos.CodigoConcesionaria WHERE NumCorrCA=%d', [NumCorrCA]));  // SS_1374A_CQU_20151002
    NombreCorto := cdsObtenerTransitoInfractoresPatente.FieldByName('NombreCorto').AsString;    // SS_1374A_CQU_20151002
    TipoImagenSeleccionada := ObtenerTipoImagenPorRegistratioAccesibility(RegistrationAccessibilityRef);
    DateTimeToString(FechaPath, 'yyyy-mm-dd', cdsObtenerTransitoInfractoresPatente.FieldByName('FechaHora').AsDateTime);
    // Armo el directorio de origen
    ArmarPathPadreTransitosNuevoFormato(FImagePathNFI, NombreCorto, NumCorrCA, FechaHora, ArchivoOrigen);    
    // Armo el directorio destino
    //Directorio := GoodDir(GoodDir(GoodDir(FImagePathInfractores) + FechaPath) + IntToStr(cdsObtenerTransitoInfractoresPatente.FieldByName('NumCorrCA').AsInteger mod 1000));      // SS_1138B_CQU_20140122
    Directorio := GoodDir(GoodDir(GoodDir(FImagePathInfractores) + FechaPath) + IntToStr(NumCorrCA mod 1000));                                                                      // SS_1138B_CQU_20140122
    // Intento crear el directorio si es que no existe
    if ForceDirectories(Directorio) then begin
        // Armo el nombre del archivo destino
        //Archivo := IntToStr(cdsObtenerTransitoInfractoresPatente.FieldByName('NumCorrCA').AsInteger) + '-' + SufijoImagen[TipoImagenSeleccionada] + '.jpg';						// SS_1138B_CQU_20140122
		Archivo := IntToStr(NumCorrCA) + '-' + SufijoImagen[TipoImagenSeleccionada] + '.jpg';																						// SS_1138B_CQU_20140122
        // Armo el archivo de origen
        //ArchivoOrigen := ArchivoOrigen + IntToStr(cdsObtenerTransitoInfractoresPatente.FieldByName('NumCorrCA').AsInteger) + '-' + SufijoImagen[TipoImagenSeleccionada] + '.jpg';	// SS_1138B_CQU_20140122
		ArchivoOrigen := ArchivoOrigen + IntToStr(NumCorrCA) + '-' + SufijoImagen[TipoImagenSeleccionada] + '.jpg';																	// SS_1138B_CQU_20140122
        // Verifico que exista previamente el archivo origen
        //if not FileExists (ArchivoOrigen) then raise Exception.Create(Format(MSG_ERROR_NO_EXISTE, [ArchivoOrigen]));                                                              // SS_1138B_CQU_20140122
        if not FileExists (ArchivoOrigen) then Exit;                                                                                                                                // SS_1138B_CQU_20140122
        // Verifico que no exista previamente el archivo destino
        //if FileExists(Directorio + Archivo) then Exit;																															// SS_1138B_CQU_20140122
        // Intento grabar la im�gen
        if Not CopyFile(PChar(ArchivoOrigen), PChar(Directorio + Archivo), False) then raise Exception.Create(Format(MSG_ERROR_COPIANDO, [ArchivoOrigen, Directorio]));
    end else
        raise Exception.Create(Format(MSG_ERROR_ACCESO, [Directorio]));
end;

{-----------------------------------------------------------------------------
Procedure Name  :   tmrActivaGrillaTimer
Param           :   Sender: TObject
Date Created    :   21-Enero-2014
Description     :   Coloca el foco en la grilla al cargar la infraccion,
                    se hizo as� para que funcionen las teclas F1... F4
Firma           :   SS_1138_CQU_20140121
-----------------------------------------------------------------------------}
procedure TformEdicionInfraccion.tmrActivaGrillaTimer(Sender: TObject);         // SS_1138_CQU_20140121
begin                                                                           // SS_1138_CQU_20140121
	tmrActivaGrilla.Enabled := False;                                           // SS_1138_CQU_20140121
    dbTransitos.SetFocus;                                                       // SS_1138_CQU_20140121
end;                                                                            // SS_1138_CQU_20140121

end.
