{-----------------------------------------------------------------------------
 Unit Name: MotivosAnulacionInf.pas
 Author:    ndonadio
 Date Created: 25/01/2005
 Language: ES-AR
 Description: Formulario para la carga de los motivos de devolucion de las
              cartas (comprobantes) por parte del correo
Revision : 1
    Date: 26/02/2009
    Author: pdominguez
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

-----------------------------------------------------------------------------}

unit MotivosAnulacionInf;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, utildb, PeaTypes,
  UtilProc, OleCtrls,  DmiCtrls, Mask,  ComCtrls, PeaProcs, validate, Dateedit,
  Util, ADODB, DMConnection, DPSControls, ListBoxEx, DBListEx, Variants,
  RStrings;

type
  TfrmMotivosAnulacionInf = class(TForm)
	GroupB: TPanel;
	Panel2: TPanel;
	Notebook: TNotebook;
    tblMotivosAnulacionInfraccion: TADOTable;
	Panel1: TPanel;
    AbmToolbar1: TAbmToolbar;
	Label2: TLabel;
    Label3: TLabel;
    spActualizarMotivosAnulacionInfraccion: TADOStoredProc;
	Label10: TLabel;
    ListaMotivos: TAbmList;
    txtDetalle: TEdit;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    lbl1: TLabel;
    edtCodigoMotivoAnulacionInfraccion: TEdit;
	procedure BtnCancelarClick(Sender: TObject);
	procedure ListaMotivosClick(Sender: TObject);
	procedure ListaMotivosDrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
	procedure ListaMotivosEdit(Sender: TObject);
	procedure ListaMotivosRefresh(Sender: TObject);
	procedure AbmToolbar1Close(Sender: TObject);
	procedure ListaMotivosDelete(Sender: TObject);
	procedure ListaMotivosInsert(Sender: TObject);
	procedure BtnAceptarClick(Sender: TObject);
	procedure FormShow(Sender: TObject);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure BtnSalirClick(Sender: TObject);
	function ListaMotivosProcess(Tabla: TDataSet; var Texto: String): Boolean;
  private
	{ Private declarations }
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
	procedure LimpiarCampos;
	Procedure Volver_Campos;
  public
	{ Public declarations }
	function Inicializar(Titulo: AnsiString): Boolean;
  end;

var
  frmMotivosAnulacionInf: TfrmMotivosAnulacionInf;

implementation

{$R *.DFM}

function TfrmMotivosAnulacionInf.Inicializar(Titulo: AnsiString):Boolean;
Var
	S: TSize;
begin
	Result := False;
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

	FormStyle := fsMDIChild;
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
    Caption := Titulo;


	if not OpenTables([tblMotivosAnulacionInfraccion]) then exit;
	Notebook.PageIndex := 0;
	ListaMotivos.Reload;

	Result := True;
end;

procedure TfrmMotivosAnulacionInf.BtnCancelarClick(Sender: TObject);
begin
	Volver_Campos;
end;

procedure TfrmMotivosAnulacionInf.ListaMotivosClick(Sender: TObject);
begin
    with (Sender AS TDbList).Table do begin                                                                     // TASK_148_MGO_20170309
        edtCodigoMotivoAnulacionInfraccion.Text := FieldByName('CodigoMotivoAnulacionInfraccion').AsString;     // TASK_148_MGO_20170309
        txtDetalle.Text := FieldByName('Descripcion').AsString;
    end;                                                                                                        // TASK_148_MGO_20170309
end;

procedure TfrmMotivosAnulacionInf.ListaMotivosDrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
        // INICIO : TASK_148_MGO_20170309
		TextOut(Cols[0], Rect.Top, Tabla.FieldbyName('CodigoMotivoAnulacionInfraccion').AsString);
		TextOut(Cols[1], Rect.Top, Trim(Tabla.FieldbyName('Descripcion').AsString));
		TextOut(Cols[2], Rect.Top, Tabla.FieldbyName('UsuarioCreacion').AsString);
		TextOut(Cols[3], Rect.Top, Tabla.FieldbyName('FechaHoraCreacion').AsString);
		TextOut(Cols[4], Rect.Top, Tabla.FieldbyName('UsuarioModificacion').AsString);
		TextOut(Cols[5], Rect.Top, Tabla.FieldbyName('FechaHoraModificacion').AsString);
        // FIN : TASK_148_MGO_20170309
	end;
end;

procedure TfrmMotivosAnulacionInf.ListaMotivosEdit(Sender: TObject);
begin
	ListaMotivos.Enabled := False;
	ListaMotivos.Estado  := modi;
    Notebook.PageIndex   := 1;
	groupb.Enabled       := True;
    ActiveControl        := txtDetalle;
end;

procedure TfrmMotivosAnulacionInf.ListaMotivosRefresh(Sender: TObject);
begin
	 if ListaMotivos.Empty then LimpiarCampos;
end;

procedure TfrmMotivosAnulacionInf.LimpiarCampos;
begin
    edtCodigoMotivoAnulacionInfraccion.Clear;   // TASK_148_MGO_20170309
	txtDetalle.Clear;
end;

procedure TfrmMotivosAnulacionInf.AbmToolbar1Close(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name: ListaMotivosDelete
  Author:
  Date Created:  /  /
  Description:
  Parameters: Sender: TObject
  Return Value: N/A

  Revision : 1
    Date: 26/02/2009
    Author: pdominguez
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure TfrmMotivosAnulacionInf.ListaMotivosDelete(Sender: TObject);

    function MotivoUtilizado: Boolean;
    var
        Codigo: ShortString;
    begin
        //Se fija si el registro actual tiene registros relacionados en Infracciones
        Codigo := tblMotivosAnulacionInfraccion.FieldByName('CodigoMotivoAnulacionInfraccion').AsString;
        Result := (QueryGetValueInt(DMConnections.BaseCAC,
                                    'SELECT TOP 1 CodigoMotivoAnulacion FROM Infracciones WITH (NOLOCK) ' +
                                    'WHERE CodigoMotivoAnulacion = ' + Codigo)) > 0;
    end;

resourcestring
    MSG_COMNFIRM_DELETE = 'Desea Eliminar El Motivo de Anulaci�n de Infracci�n?';
    MSG_DELETE          = 'Eliminar';
    MSG_ERROR           = 'Error';
    MSG_CAN_NOT_DELETE  = 'No se puede eliminar el Motivo de Anulaci�n de Infracci�n ya que posee Infracciones Anuladas relacionadas';
begin
    //Si el registro posee registros relacionandos en la tabla Infracciones no dejo eliminarlo
    if MotivoUtilizado then begin
        MsgBox(MSG_CAN_NOT_DELETE);
        ListaMotivos.Estado := Normal;
        Exit;
    end;

	Screen.Cursor := crHourGlass;

	if (MsgBox(MSG_COMNFIRM_DELETE, MSG_DELETE, MB_YESNO + MB_ICONQUESTION) = IDYES) then begin
		try
            QueryExecute(DMConnections.BaseCAC,
                         'DELETE FROM MotivosAnulacionInfraccion ' +
                         'WHERE CodigoMotivoAnulacionInfraccion = ' + IntToStr(tblMotivosAnulacionInfraccion.FieldByName('CodigoMotivoAnulacionInfraccion').AsInteger));
		except
			On E: Exception do
				MsgBoxErr(MSG_ERROR, E.message, MSG_ERROR, MB_ICONSTOP);
		end
	end;
	ListaMotivos.Reload;
	ListaMotivos.Estado  := Normal;
	ListaMotivos.Enabled := True;
	Notebook.PageIndex   := 0;
	Screen.Cursor        := crDefault;
end;

procedure TfrmMotivosAnulacionInf.ListaMotivosInsert(Sender: TObject);
begin
	LimpiarCampos;
    groupb.Enabled       := True;
	ListaMotivos.Enabled := False;
	Notebook.PageIndex   := 1;
	ActiveControl        := txtDetalle;
end;

procedure TfrmMotivosAnulacionInf.BtnAceptarClick(Sender: TObject);
begin
    with ListaMotivos do begin
        Screen.Cursor := crHourGlass;
        try
            try
                with spActualizarMotivosAnulacionInfraccion, Parameters do begin
                    Refresh;                                                    // TASK_148_MGO_20170309
                    if ListaMotivos.Estado = Alta then
                        ParamByName('@Accion').Value := 0
                    else
                        ParamByName('@Accion').Value := 1;
                    ParamByName('@Codigo').Value := tblMotivosAnulacionInfraccion.FieldByName('CodigoMotivoAnulacionInfraccion').AsInteger;
                    ParamByName('@Detalle').Value := Trim(txtDetalle.Text);
                    ParamByName('@CodigoUsuario').Value := UsuarioSistema;      // TASK_148_MGO_20170309
                    ExecProc;
                end;
            except
                On E: EDataBaseError do begin
                    MsgBoxErr('Error', e.message, 'Error', MB_ICONERROR);
                end;
            end;
        finally
            Volver_Campos;
            Screen.Cursor:= crDefault;
            Reload;
        end;
    end;
end;

procedure TfrmMotivosAnulacionInf.FormShow(Sender: TObject);
begin
	ListaMotivos.Reload;
end;

procedure TfrmMotivosAnulacionInf.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TfrmMotivosAnulacionInf.BtnSalirClick(Sender: TObject);
begin
	Close;
end;

procedure TfrmMotivosAnulacionInf.Volver_Campos;
begin
	ListaMotivos.Estado  := Normal;
	ListaMotivos.Enabled := True;
	ActiveControl        := ListaMotivos;
	Notebook.PageIndex   := 0;
	groupb.Enabled       := False;
	ListaMotivos.Reload;
end;

function TfrmMotivosAnulacionInf.ListaMotivosProcess(Tabla: TDataSet;
  var Texto: String): Boolean;
begin
	Result := True;
end;


end.

