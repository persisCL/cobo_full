{-----------------------------------------------------------------------------
 Unit Name: frmABMTeleviaInfractor
 Author:    lcanteros
 Date:      27-Feb-2008
 Purpose:  Se encarga de cargar y modificar las infracciones asociadas a televia
 y modificar el estado de las mismas
 History:

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

-----------------------------------------------------------------------------}

unit frmABMTeleviaInfractor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DbList, Abm_obj, Db, DBTables, Util, utildb,
  UtilProc, OleCtrls,  DmiCtrls, Mask,  ComCtrls, PeaProcs, ADODB, BuscaTab,
  Variants, DMConnection, DPSControls, Peatypes, VariantComboBox, ListBoxEx,
  DBListEx;

type
  TABMTeleviaInfractoresForm = class(TForm)
    AbmToolbar1: TAbmToolbar;
    pnlAbm: TPanel;
    Panel2: TPanel;
    Notebook: TNotebook;
    Label8: TLabel;
    Label9: TLabel;
    spListaInfracciones: TADOStoredProc;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    btnDesactivar: TBitBtn;
    DBList1: TAbmList;
    txtPatente: TMaskEdit;
    txtEtiqueta: TEdit;
    procedure BtnCancelarClick(Sender: TObject);

    procedure DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure DBList1Edit(Sender: TObject);
    procedure DBList1Refresh(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);

    procedure DBList1Insert(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    procedure btnDesactivarClick(Sender: TObject);
    procedure txtEtiquetaKeyPress(Sender: TObject; var Key: Char);
    procedure txtPatenteChange(Sender: TObject);
    procedure txtEtiquetaChange(Sender: TObject);

  private
	{ Private declarations }
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
	procedure LimpiarCampos;
    function DesactivarInfractor(): boolean;
    function ModificarInfractor(Estado: TAbmState): boolean;
    procedure ActualizarLista();
    procedure EditarDatos(TipoAbm: TAbmState);
    procedure FinEditarDatos(Actualizar: boolean);
    function PatenteValida(Pat: string): boolean;

  public
    { Public declarations }
    function Inicializar(MDIChild: Boolean): Boolean;
  end;

var
  ABMTeleviaInfractoresForm: TABMTeleviaInfractoresForm;

implementation

Const
    SP_AGREGAR_INFRACCION = 'TeleviaInfractorAgregar';
    SP_MODIF_INFRACCION = 'TeleviaInfractorActualizarDatos';
    SP_DESACTIVAR_INFRACCION = 'TeleviaInfractorDesactivarInfraccion';
    SP_VALIDA_PATENTE = 'VerificarFormatoPatente';
    DIGITOS_PATENTE = 6;


resourcestring
    MSG_ACTUALIZAR_ERROR 	= 'No se pudieron actualizar los datos de la Infracci�n seleccionada.';
    MSG_ACTUALIZAR_CAPTION	= 'Actualizar Infracci�n';
    MSG_ERROR_CAMBIO_ESTADO = 'Error al cambiar el estado';
    MSG_ERROR_PATENTE = 'Por favor complete el campo PATENTE';
    MSG_ERROR_DIGITOS_PATENTE = 'La patente debe contener seis digitos';
    MSG_ERROR_ETIQUETA = 'Por favor complete el campo TELEVIA';
    MSG_ERROR_AL_VALIDAR_PATENTE = 'Se a producido un errror al validar la patente'+ crlf+ 'Por favor intente nuevamente';
    MSG_ERROR_AL_DESACTIVAR_INFRACCION = 'Error al desactivar infrac��n';
    MSG_CONFIRMA_DESACTIVAR = 'Confirma desactivar infracci�n ?';
    MSG_PATENTE_INVALIDA = 'El formato de la patente %s es inv�lido';
    CAPTION_INFRACCION = 'Infracci�n';
    MSG_INFRACCION_ACTIVA = 'El par Patente-Televia ya existe activo';
    CAPTION_INFO = 'Informaci�n';
    TXT_ACTIVADO = 'Activado';
    TXT_DESACTIVADO = 'Desactivado';

{$R *.DFM}

{-----------------------------------------------------------------------------
  Procedure: Inicializar
  Author:    lcanteros
  Date:      29-Feb-2008
  Arguments: MDIChild: Boolean
  Result:    Boolean
  Purpose:   Inicializa el formulario
-----------------------------------------------------------------------------}
function TABMTeleviaInfractoresForm.Inicializar(MDIChild: Boolean): Boolean;
Var
	S: TSize;
begin
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

	if MDIChild then begin
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;
	Result := False;
	if not OpenTables([spListaInfracciones]) then Exit;
	Result := True;
	DbList1.Reload;
  	Notebook.PageIndex := 0;
end;
{-----------------------------------------------------------------------------
  Procedure: BtnCancelarClick
  Author:    lcanteros
  Date:      29-Feb-2008
  Arguments: Sender: TObject
  Result:    None
  Purpose:   cancela la edicion de datos
-----------------------------------------------------------------------------}
procedure TABMTeleviaInfractoresForm.BtnCancelarClick(Sender: TObject);
begin
    FinEditarDatos(False);
end;
{-----------------------------------------------------------------------------
  Procedure: btnDesactivarClick
  Author:    lcanteros
  Date:      28-Feb-2008
  Arguments: Sender: TObject
  Result:    Desactiva la infraccion pasandola a estado = 2
-----------------------------------------------------------------------------}
procedure TABMTeleviaInfractoresForm.btnDesactivarClick(Sender: TObject);
begin
    if Application.MessageBox(PChar(MSG_CONFIRMA_DESACTIVAR), Pchar(CAPTION_INFRACCION), MB_ICONQUESTION or MB_YESNO) = IDNO then exit;

    if not DesactivarInfractor then MsgBoxErr(MSG_ERROR_AL_DESACTIVAR_INFRACCION, '', CAPTION_INFRACCION, 0);
    FinEditarDatos(true);
end;
{-----------------------------------------------------------------------------
  Procedure: DBList1DrawItem
  Author:    lcanteros
  Date:      29-Feb-2008
  Arguments: Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions
  Result:    None
  Purpose:   Asigna el valor de dato a cada campo de la lista seteados en subtitulos
-----------------------------------------------------------------------------}
procedure TABMTeleviaInfractoresForm.DBList1DrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, Trim(FieldByName('Patente').AsString ));
		TextOut(Cols[1], Rect.Top, Trim(FieldByName('Etiqueta').AsString));
		TextOut(Cols[2], Rect.Top, Trim(FieldByName('ContractSerialNumber').AsString));
        TextOut(Cols[3], Rect.Top, Trim(FieldByName('FechaActivacion').AsString));
        TextOut(Cols[4], Rect.Top, Trim(FieldByName('CodigoUsuarioIngreso').AsString));
        //TextOut(Cols[5], Rect.Top, Trim(FieldByName('FechaDesactivacion').AsString));
        //TextOut(Cols[6], Rect.Top, Trim(FieldByName('CodigoUsuarioDesactivacion').AsString));
        TextOut(Cols[5], Rect.Top, IIf(Trim(FieldByName('Estado').AsString) = '1', TXT_ACTIVADO, TXT_DESACTIVADO)+' ');
	end;
end;

procedure TABMTeleviaInfractoresForm.DBList1Edit(Sender: TObject);
resourcestring
    MSG_ERROR_EDITAR = 'Error al editar datos';
    CAPTION_ERROR = 'Error';
begin
    try
        EditarDatos(Modi);
    except
        on ex: Exception do begin
            MsgBoxErr(MSG_ERROR_EDITAR, '', CAPTION_ERROR, 0);
        end;
    end;
end;

procedure TABMTeleviaInfractoresForm.DBList1Refresh(Sender: TObject);
begin
	 if DBList1.Empty then begin
        LimpiarCampos
	 end;
end;
{-----------------------------------------------------------------------------
  Procedure: DesactivarInfractor
  Author:    lcanteros
  Date:      28-Feb-2008
  Arguments: None
  Result:    boolean
  Purpose:  Desactiva la infraccion seleccionada
-----------------------------------------------------------------------------}
function TABMTeleviaInfractoresForm.DesactivarInfractor: boolean;
var spDesactivar: TADOStoredProc;
begin
    Result := false;
    spDesactivar := TADOStoredProc.Create(Self);
    try
      with spDesactivar, Parameters do begin
        Connection := DMConnections.BaseCAC;
        ProcedureName := SP_DESACTIVAR_INFRACCION;
        Refresh;
        ParamByName('@CodigoTeleviaInfractor').Value := spListaInfracciones.FieldByName('CodigoTeleviaInfractor').AsInteger;
        ParamByName('@Usuario').Value := UsuarioSistema;
        ExecProc;
      end;
      Result := true;
    finally
        spDesactivar.Close;
        spDesactivar.Free;
    end;
    if not DBList1.Table.Bof then
        DBList1.Table.Prior
    else
        DBList1.Table.First;

end;
{-----------------------------------------------------------------------------
  Procedure: EditarDatos
  Author:    lcanteros
  Date:      29-Feb-2008
  Arguments: TipoAbm: TAbmState
  Result:    None
  Purpose:   Ingresa o modifica infracciones
-----------------------------------------------------------------------------}
procedure TABMTeleviaInfractoresForm.EditarDatos(TipoAbm: TAbmState);
begin
    DbList1.Enabled     := False;
    case TipoAbm of
      Alta:
        begin
            DBList1.Estado      := Alta;
            LimpiarCampos;
            btnDesactivar.Visible := false;
        end;
      Modi:
        begin
            DBList1.Estado      := modi;
            txtPatente.Text := spListaInfracciones.FieldByName('Patente').AsWideString;
            txtEtiqueta.Text := spListaInfracciones.FieldByName('Etiqueta').AsWideString;
            btnDesactivar.Visible := true;
        end;
    end;
    BtnAceptar.Enabled := false;
    Notebook.PageIndex  := 1;
    pnlAbm.Visible      := True;
    txtPatente.SetFocus;
end;
{-----------------------------------------------------------------------------
  Procedure: LimpiarCampos
  Author:    lcanteros
  Date:      29-Feb-2008
  Arguments: 
  Result:    None
  Purpose:  Vacia los controles
-----------------------------------------------------------------------------}
procedure TABMTeleviaInfractoresForm.LimpiarCampos();
begin
	txtPatente.Clear;
    txtEtiqueta.Clear;
end;
{-----------------------------------------------------------------------------
  Procedure: ModificarInfractor
  Author:    lcanteros
  Date:      28-Feb-2008
  Arguments: Estado: TAbmState
  Result:    Modifica o agrega infraccion
-----------------------------------------------------------------------------}
function TABMTeleviaInfractoresForm.ModificarInfractor(Estado: TAbmState) : boolean;
var spAux: TADOStoredProc;
    Retorno: integer;
begin
    Result := False;
    spAux := TADOStoredProc.Create(Self);
    try
        try
        spAux.Connection := DMConnections.BaseCAC;
        if Estado = Alta then begin
            with spAux , Parameters do begin
                ProcedureName := SP_AGREGAR_INFRACCION;
                Refresh;
                ParamByName('@CodigoUsuarioIngreso').Value := UsuarioSistema;
                ParamByName('@Etiqueta').Value := Trim(txtEtiqueta.Text);
                ParamByName('@Patente').Value := Trim(txtPatente.Text);
            end;
        end else begin
            with spAux , Parameters do begin
                ProcedureName := SP_MODIF_INFRACCION;
                Refresh;
                ParamByName('@CodigoTeleviaInfractor').Value := spListaInfracciones.FieldByName('CodigoTeleviaInfractor').AsInteger;
                ParamByName('@Etiqueta').Value := Trim(txtEtiqueta.Text);
                ParamByName('@Patente').Value := Trim(txtPatente.Text);
                ParamByName('@CodigoUsuarioIngreso').Value := UsuarioSistema;
            end;
        end;
        spAux.ExecProc;
        Retorno := spAux.Parameters.ParamByName('@RETURN_VALUE').Value;
        if Retorno = 1 then begin
            MsgBox(MSG_INFRACCION_ACTIVA, CAPTION_INFO);
        end else
            Result := True;
        except
            On E: EDataBaseError do begin
				MsgBoxErr(MSG_ACTUALIZAR_ERROR, E.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
			end;
        end;
    finally
        spAux.Close;
        spAux.Free;
    end;
end;
{-----------------------------------------------------------------------------
  Procedure: PatenteValida
  Author:    lcanteros
  Date:      29-Feb-2008
  Arguments: Pat: string
  Result:    boolean
  Purpose: Valida el formato de la patente ingresada
-----------------------------------------------------------------------------}
function TABMTeleviaInfractoresForm.PatenteValida(Pat: string): boolean;
var spValidaPatente: TADOStoredProc;
begin
    Result := false;
    spValidaPatente := TADOStoredProc.Create(Self);
    try
      try
        with spValidaPatente, Parameters do begin
            Connection := DMConnections.BaseCAC;
            ProcedureName := SP_VALIDA_PATENTE;
            Refresh;
            ParamByName('@Patente').Value := Pat;
            ExecProc;
            Result := (ParamByName('@RETURN_VALUE').Value = 1);
        end;
      except
        MsgBoxErr(MSG_ERROR_AL_VALIDAR_PATENTE, '', CAPTION_INFRACCION, 0);
      end;
    finally
        spValidaPatente.Close;
        spValidaPatente.Free;
    end;
end;

procedure TABMTeleviaInfractoresForm.txtEtiquetaChange(Sender: TObject);
begin
    if not BtnAceptar.Enabled  then BtnAceptar.Enabled := true;
end;
{-----------------------------------------------------------------------------
  Procedure: txtEtiquetaKeyPress
  Author:    lcanteros
  Date:      28-Feb-2008
  Arguments: Sender: TObject; var Key: Char
  Result:    acepta solo numeros
-----------------------------------------------------------------------------}
procedure TABMTeleviaInfractoresForm.txtEtiquetaKeyPress(Sender: TObject;
  var Key: Char);
begin
    if not (Key in  ['0'..'9', #8]) then
        Key := #0;
end;

procedure TABMTeleviaInfractoresForm.txtPatenteChange(Sender: TObject);
begin
    if not BtnAceptar.Enabled  then BtnAceptar.Enabled := true;
end;

procedure TABMTeleviaInfractoresForm.AbmToolbar1Close(Sender: TObject);
begin
     close;
end;

procedure TABMTeleviaInfractoresForm.DBList1Insert(Sender: TObject);
begin
    EditarDatos(Alta);
end;

{-----------------------------------------------------------------------------
  Procedure: ActualizarLista
  Author:    lcanteros
  Date:      28-Feb-2008
  Arguments: None
  Result:    refresca los datos de la lista
-----------------------------------------------------------------------------}
procedure TABMTeleviaInfractoresForm.ActualizarLista;
begin
    spListaInfracciones.Close;
    spListaInfracciones.Open;
end;
{-----------------------------------------------------------------------------
  Procedure: BtnAceptarClick
  Author:    lcanteros
  Date:      28-Feb-2008
  Arguments: Sender: TObject
  Result:    aplica los cambios seleccionados
-----------------------------------------------------------------------------}
procedure TABMTeleviaInfractoresForm.BtnAceptarClick(Sender: TObject);
resourcestring
    MSG_CONFIRM_CHANGE = 'Confirma aceptar datos ?';
 begin
    if not ValidateControls(
        [txtPatente ,
        txtPatente,
        txtPatente,
        txtEtiqueta],
        [Trim(txtPatente.Text) <> '',
        Length(txtPatente.Text) = DIGITOS_PATENTE,
        PatenteValida(Trim(txtPatente.Text)),
		Trim(txtEtiqueta.text) <> ''],
        MSG_ACTUALIZAR_CAPTION,
        [MSG_ERROR_PATENTE,
        MSG_ERROR_DIGITOS_PATENTE,
        Format(MSG_PATENTE_INVALIDA, [txtPatente.Text]),
        MSG_ERROR_ETIQUETA]) then exit;

    if ModificarInfractor(DBList1.Estado) then begin
        Screen.Cursor := crHourGlass;
        FinEditarDatos(True);
    end;

end;

procedure TABMTeleviaInfractoresForm.FormShow(Sender: TObject);
begin
   	DBList1.Reload;
end;
{-----------------------------------------------------------------------------
  Procedure: FinEditarDatos
  Author:    lcanteros
  Date:      28-Feb-2008
  Arguments: Actualizar: boolean
  Result:    finaliza proceso de edicion
-----------------------------------------------------------------------------}
procedure TABMTeleviaInfractoresForm.FinEditarDatos(Actualizar: boolean);
var RegistroActual: Pointer;

begin
    pnlAbm.Visible     := False;
    DbList1.Estado     := Normal;

	Notebook.PageIndex := 0;
	Screen.Cursor 	   := crDefault;
    if Actualizar then begin
        RegistroActual := DBList1.Table.GetBookmark;
        ActualizarLista;
        DBList1.Reload;
        if not DBList1.Table.IsEmpty then
            if DBList1.Table.BookmarkValid(RegistroActual) then DBList1.Table.GotoBookmark(RegistroActual);
    end;
    DbList1.Enabled    := True;
    DbList1.SetFocus;
end;

procedure TABMTeleviaInfractoresForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TABMTeleviaInfractoresForm.BtnSalirClick(Sender: TObject);
begin
     Close;
end;

end.
