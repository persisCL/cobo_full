program Infractores;

uses
  Forms,
  OpenOnce,
  Controls,
  Util,
  Main in 'Main.pas' {MainForm},
  MaskCombo in '..\Componentes\MaskCombo\MaskCombo.pas',
  PeaTypes in '..\Comunes\PeaTypes.pas',
  PeaProcs in '..\Comunes\PeaProcs.pas',
  Login in '..\Comunes\Login.pas' {LoginForm},
  CambioPassword in '..\Comunes\CambioPassword.pas' {FormCambioPassword},
  RStrings in '..\Comunes\RStrings.pas',
  ConstParametrosGenerales in '..\Comunes\ConstParametrosGenerales.pas',
  FrmRptInformeUsuario in '..\Comunes\FrmRptInformeUsuario.pas' {FormRptInformeUsuario},
  FrmRptInformeUsuarioConfig in '..\Comunes\FrmRptInformeUsuarioConfig.pas' {FormRptInformeUsuarioConfig},
  FrmStartup in 'FrmStartup.pas' {FormStartup},
  UtilProc,
  DPSGrid in '..\Componentes\DPSGrid\DPSGrid.pas',
  DpsAbout,
  frmValidarInfraccion in 'frmValidarInfraccion.pas' {formValidarInfraccion},
  ImgTypes in '..\Comunes\ImgTypes.pas',
  ImgProcs in '..\Comunes\ImgProcs.pas',
  adminImg in '..\Comunes\adminImg.pas',
  utilImg in '..\Comunes\utilImg.pas',
  formImagenOverview in '..\Comunes\formImagenOverview.pas' {frmImageOverview},
  frmListadoInfracciones in 'frmListadoInfracciones.pas' {FormListadoInfracciones},
  OPImageOverview in '..\Componentes\Imagenes\OPImageOverview.pas',
  ProcsOverview in '..\Componentes\Imagenes\ProcsOverview.pas',
  ImagePlus in '..\Componentes\Imagenes\ImagePlus.pas',
  Filtros in '..\Componentes\Imagenes\Filtros.pas',
  UtilDB in 'C:\Utiles\Delphi\VCL\UtilDB.pas',
  RVMClient in '..\Comunes\RVMClient.pas',
  RVMTypes in '..\Comunes\RVMTypes.pas',
  RVMBind in '..\Comunes\RVMBind.pas',
  RVMLogin in '..\Comunes\RVMLogin.pas' {frmLogin},
  RNVMConfig in '..\Comunes\RNVMConfig.pas' {frmConfig},
  ActualizarDatosInfractor in '..\Comunes\ActualizarDatosInfractor.pas' {DMActualizarDatosInfractor: TDataModule},
  frmEdicionInfraccion in 'frmEdicionInfraccion.pas' {formEdicionInfraccion},
  UtilReportes in '..\Comunes\UtilReportes.pas',
  MotivosAnulacionInf in 'MotivosAnulacionInf.pas' {frmMotivosAnulacionInf},
  CustomAssistance in '..\Comunes\CustomAssistance.pas' {frmLocalAssistance},
  ExplorarInfracciones in '..\Comunes\ExplorarInfracciones.pas' {frmExplorarInfraciones},
  frmABMTeleviaInfractor in 'frmABMTeleviaInfractor.pas' {ABMTeleviaInfractoresForm},
  frmVentanaAviso in '..\Comunes\Avisos\frmVentanaAviso.pas' {VentanaAvisoForm},
  Aviso in '..\Comunes\Avisos\Aviso.pas',
  TipoFormaAtencion in '..\Comunes\Avisos\TipoFormaAtencion.pas',
  Notificacion in '..\Comunes\Avisos\Notificacion.pas',
  AvisoTagVencidos in '..\Comunes\Avisos\AvisoTagVencidos.pas',
  NotificacionImpresionDoc in '..\Comunes\Avisos\NotificacionImpresionDoc.pas',
  NotificacionImpresionReporte in '..\Comunes\Avisos\NotificacionImpresionReporte.pas' {FormNotificacionImpresionReporte},
  FactoryINotificacion in '..\Comunes\Avisos\FactoryINotificacion.pas',
  SysUtils,
  frmAcercaDe in '..\Comunes\frmAcercaDe.pas' {AcercaDeForm},
  DMConnection in '..\Comunes\DMConnection.pas' {DMConnections: TDataModule},
  DMComunicacion in '..\Comunes\DMComunicacion.pas' {DMComunicaciones: TDataModule},
  EncriptaRijandel in '..\Comunes\EncriptaRijandel.pas',
  AES in '..\Comunes\AES.pas',
  base64 in '..\Comunes\base64.pas',
  Diccionario in '..\Comunes\Diccionario.pas',
  ClaveValor in '..\Comunes\ClaveValor.pas',
  frmMuestraMensaje in '..\Comunes\frmMuestraMensaje.pas' {PanelMensajesForm},
  frmMuestraPDF in '..\Comunes\frmMuestraPDF.pas' {MuestraPDFForm},
  SysUtilsCN in '..\Comunes\SysUtilsCN.pas',
  frmBloqueosSistema in '..\Comunes\frmBloqueosSistema.pas' {BloqueosSistemaForm},
  PeaProcsCN in '..\Comunes\PeaProcsCN.pas',
  MsgBoxCN in '..\Comunes\MsgBoxCN.pas' {frmMsgBoxCN},
  frmMensajeACliente in '..\Comunes\frmMensajeACliente.pas' {FormMensajeACliente},
  LoginSetup in '..\Comunes\LoginSetup.pas' {frmLoginSetup},
  LoginValuesEditor in '..\Comunes\LoginValuesEditor.pas' {frmLoginValuesEditor},
  Crypto in '..\Comunes\Crypto.pas',
  CSVUtils in '..\Comunes\CSVUtils.pas',
  AdministrarRIVM in 'AdministrarRIVM.pas' {frmAdministrarRIVM},
  ConsultarRNVM in 'ConsultarRNVM.pas' {frmConsultarRNVM};

{$R *.res}

Var
	Startup: TFormStartup;
begin
	try
		MoveToMyOwnDir;
		Application.Initialize;
		Application.Title := 'Gesti�n de Infracciones';
        if not AlreadyOpen then begin
			Screen.Cursor := crAppStart;
			Startup := TFormStartup.Create(nil);
			Startup.Show;
			Startup.Update;
			Delay(1500);
            Startup.Release;
			Screen.Cursor := crDefault;
            Application.CreateForm(TMainForm, MainForm);
            Application.CreateForm(TDMConnections, DMConnections);
            Application.CreateForm(TDMComunicaciones, DMComunicaciones);
  if MainForm.Inicializar then begin
                          MainForm.Show;
                          MainForm.Update;
                      end else MainForm.Close;
		end;

        ShortDateFormat := 'dd/mm/yyyy';
        LongDateFormat  := 'dd/mm/yyyy';
		Application.Run
	except
	end
end.
