unit AdministrarRIVM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DMConnection, UtilProc, Util, UtilDB, Grids, DBGrids, StdCtrls,
  ExtCtrls, ComCtrls, ToolWin, ImgList, DmiCtrls, DB, ADODB, DBClient, Provider;

type
  TfrmAdministrarRIVM = class(TForm)
    ilImagenes: TImageList;
    pnlSuperior: TPanel;
    tlbBotonera: TToolBar;
    btnSalir: TToolButton;
    pnlBotones: TPanel;
    pnlk: TPanel;
    dbgrdConsultas: TDBGrid;
    edtRUT_Filtro: TEdit;
    lbl1: TLabel;
    lbl: TLabel;
    edtPatente_Filtro: TEdit;
    btnBuscar: TButton;
    lbl2: TLabel;
    txtMaxRegistros: TNumericEdit;
    btnSalir1: TButton;
    spConsultaRNVM_SELECT: TADOStoredProc;
    grp1: TGroupBox;
    lbl3: TLabel;
    lbl4: TLabel;
    lbl5: TLabel;
    lbl6: TLabel;
    lbl7: TLabel;
    grp2: TGroupBox;
    lbl8: TLabel;
    lbl9: TLabel;
    lbl10: TLabel;
    lbl11: TLabel;
    lbl12: TLabel;
    lbl13: TLabel;
    lbl14: TLabel;
    lbl15: TLabel;
    lbl16: TLabel;
    lbl17: TLabel;
    grp3: TGroupBox;
    lbl18: TLabel;
    lbl19: TLabel;
    lbl20: TLabel;
    lbl21: TLabel;
    lbl22: TLabel;
    lbl23: TLabel;
    lbl24: TLabel;
    lbl25: TLabel;
    lbl26: TLabel;
    dtstprConsultas: TDataSetProvider;
    cdsConsultas: TClientDataSet;
    dsConsultas: TDataSource;
    lblCodigoConsultaRNVM: TLabel;
    lblFecha: TLabel;
    lblCodigoUsuario: TLabel;
    lblPatente: TLabel;
    lblResultado: TLabel;
    lblNumeroDocumento: TLabel;
    lblNombre: TLabel;
    lblApellido: TLabel;
    lblApellidoMaterno: TLabel;
    lblPatente1: TLabel;
    lblMarca: TLabel;
    lblModelo: TLabel;
    lblCategoriaDesc: TLabel;
    lblTipoVehiculo: TLabel;
    lblColor: TLabel;
    lblIdMotor: TLabel;
    lblIdChasis: TLabel;
    lblAnio: TLabel;
    lblFechaAdquisicion: TLabel;
    lblRegionDesc: TLabel;
    lblComunaDesc: TLabel;
    lblCalleDesnormalizada: TLabel;
    lblNumero: TLabel;
    lblDetalle: TLabel;
    procedure btnSalirClick(Sender: TObject);
    procedure btnSalir1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnBuscarClick(Sender: TObject);
    procedure cdsConsultasAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
    function CargarRegistros: Boolean;
    procedure LimpiarCampos;
  public
    { Public declarations }
    function Inicializar: Boolean;
  end;

var
  frmAdministrarRIVM: TfrmAdministrarRIVM;

implementation

{$R *.dfm}

function TfrmAdministrarRIVM.Inicializar: Boolean;
resourcestring
    MSG_ERROR_DATOS = 'Ha ocurrido un error al obtener los registros';
begin
    LimpiarCampos;
    Result := CargarRegistros;
end;

function TfrmAdministrarRIVM.CargarRegistros: Boolean;
resourcestring
    MSG_ERROR_DATOS = 'Ha ocurrido un error al obtener los registros';
begin
    Result := True;
    Screen.Cursor := crHourGlass;

    try
        try
            spConsultaRNVM_SELECT.Parameters.Refresh;
            spConsultaRNVM_SELECT.Parameters.ParamByName('@MaxRegistros').Value := txtMaxRegistros.ValueInt;
            spConsultaRNVM_SELECT.Parameters.ParamByName('@NumeroDocumento').Value := IIf(edtRUT_Filtro.Text <> EmptyStr, edtRUT_Filtro.Text, Null);
            spConsultaRNVM_SELECT.Parameters.ParamByName('@Patente').Value := IIf(edtPatente_Filtro.Text <> EmptyStr, edtRUT_Filtro.Text, Null);
            spConsultaRNVM_SELECT.Open;

            if spConsultaRNVM_SELECT.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
                raise Exception.Create(spConsultaRNVM_SELECT.Parameters.ParamByName('@ErrorDescription').Value);

            cdsConsultas.Data := dtstprConsultas.Data;
        except
            on e: Exception do begin
                Result := False;
                MsgBoxErr(MSG_ERROR_DATOS, e.Message, 'Error', MB_ICONERROR);
            end;
        end;
    finally
        spConsultaRNVM_SELECT.Close;
        Screen.Cursor := crDefault;
    end;
end;

procedure TfrmAdministrarRIVM.btnBuscarClick(Sender: TObject);
begin
    CargarRegistros;
end;

procedure TfrmAdministrarRIVM.btnSalir1Click(Sender: TObject);
begin
    Close;
end;

procedure TfrmAdministrarRIVM.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TfrmAdministrarRIVM.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TfrmAdministrarRIVM.LimpiarCampos;
begin
    lblCodigoConsultaRNVM.Caption   := EmptyStr;
    lblFecha.Caption                := EmptyStr;
    lblPatente.Caption              := EmptyStr;
    lblCodigoUsuario.Caption        := EmptyStr;
    lblResultado.Caption            := EmptyStr;
    lblPatente1.Caption             := EmptyStr;
    lblMarca.Caption                := EmptyStr;
    lblModelo.Caption               := EmptyStr;
    lblAnio.Caption                 := EmptyStr;
    lblColor.Caption                := EmptyStr;
    lblTipoVehiculo.Caption         := EmptyStr;
    lblFechaAdquisicion.Caption     := EmptyStr;
    lblCategoriaDesc.Caption        := EmptyStr;
    lblIdMotor.Caption              := EmptyStr;
    lblIdChasis.Caption             := EmptyStr;
    lblNumeroDocumento.Caption      := EmptyStr;
    lblNombre.Caption               := EmptyStr;
    lblApellido.Caption             := EmptyStr;
    lblApellidoMaterno.Caption      := EmptyStr;
    lblCalleDesnormalizada.Caption  := EmptyStr;
    lblNumero.Caption               := EmptyStr;
    lblRegionDesc.Caption           := EmptyStr;
    lblComunaDesc.Caption           := EmptyStr;
    lblDetalle.Caption              := EmptyStr;
end;

procedure TfrmAdministrarRIVM.cdsConsultasAfterScroll(DataSet: TDataSet);
begin
    LimpiarCampos;

    lblCodigoConsultaRNVM.Caption   := cdsConsultas.FieldByName('CodigoConsultaRNVM').AsString;
    lblFecha.Caption                := cdsConsultas.FieldByName('Fecha').AsString;
    lblPatente.Caption              := cdsConsultas.FieldByName('Patente').AsString;
    lblCodigoUsuario.Caption        := cdsConsultas.FieldByName('CodigoUsuario').AsString;
    lblResultado.Caption            := cdsConsultas.FieldByName('Resultado').AsString;
    lblPatente1.Caption             := cdsConsultas.FieldByName('Patente').AsString;
    lblMarca.Caption                := cdsConsultas.FieldByName('Marca').AsString;
    lblModelo.Caption               := cdsConsultas.FieldByName('Modelo').AsString;
    lblAnio.Caption                 := cdsConsultas.FieldByName('Anio').AsString;
    lblColor.Caption                := cdsConsultas.FieldByName('Color').AsString;
    lblTipoVehiculo.Caption         := cdsConsultas.FieldByName('TipoVehiculo').AsString;
    lblFechaAdquisicion.Caption     := cdsConsultas.FieldByName('FechaAdquisicion').AsString;
    lblCategoriaDesc.Caption        := cdsConsultas.FieldByName('CategoriaDesc').AsString;
    lblIdMotor.Caption              := cdsConsultas.FieldByName('IdMotor').AsString;
    lblIdChasis.Caption             := cdsConsultas.FieldByName('IdChasis').AsString;
    lblNumeroDocumento.Caption      := cdsConsultas.FieldByName('NumeroDocumento').AsString;
    lblNombre.Caption               := cdsConsultas.FieldByName('Nombre').AsString;
    lblApellido.Caption             := cdsConsultas.FieldByName('Apellido').AsString;
    lblApellidoMaterno.Caption      := cdsConsultas.FieldByName('ApellidoMaterno').AsString;
    lblCalleDesnormalizada.Caption  := cdsConsultas.FieldByName('CalleDesnormalizada').AsString;
    lblNumero.Caption               := cdsConsultas.FieldByName('Numero').AsString;
    lblRegionDesc.Caption           := cdsConsultas.FieldByName('RegionDesc').AsString;
    lblComunaDesc.Caption           := cdsConsultas.FieldByName('ComunaDesc').AsString;
    lblDetalle.Caption              := cdsConsultas.FieldByName('Detalle').AsString;
end;

end.
