{-------------------------------------------------------------------------------
 Revision 1:
    Author : nefernandez
    Date : 30/10/2007
    Description : Se agregan permisos para los botones "&Elegir Imagen JPL" y
    "Sin &Imagen JPL" del formulario "Edici�n de Infracci�n"

 Revision 3:
    Author : nefernandez
    Date : 03/12/2008
    Description : SS 624: Se agrega el permiso para la operaci�n de "Anular Infracci�n". Como esto se
    debe migrar a producci�n antes de los cambios de la revisi�n 2 (SS 218), se agrega un permiso
    con nombre "generico" al unico link para anular la infracci�n.

Firma       :   SS_925_NDR_20131122
Description :   Genera nuevos menu segun campo GenerarMenues de tabla SistemasVersiones (no del .ini)


Firma       :   SS_925_MBE_20131223
Description :   Se cambia la ventana de Acerca de.

Firma       : SS_1147_NDR_20141216
Descripcion : Color de menues segun concesionaria nativa

Etiqueta    : 20160315 MGO
Descripci�n : Se utiliza InstallIni en vez de ApplicationIni

-------------------------------------------------------------------------------}
unit Main;

interface

uses
	Forms, jpeg,Controls, Classes, Menus, ComCtrls, ExtCtrls, PeaTypes, Windows,
    UtilProc, Dialogs, ExtDlgs, sysUtils, Graphics, DMConnection, util, RStrings,
    ImgList, CambioPassword, DB, ADODB, Login, PeaProcs, DPSAbout,
    frmValidarInfraccion, frmListadoInfracciones, RNVMConfig, ExplorarInfracciones,
    MotivosAnulacionInf, XPMan, CustomAssistance, AdministrarRIVM, ConsultarRNVM;  // TASK_159_MGO_20170323  // TASK_149_MGO_20170310

type
  TMainForm = class(TForm)
    StatusBar: TStatusBar;
    Imagenes: TImageList;
    ImagenFondo: TImage;
    MenuImagenes: TImageList;
    MainMenu1: TMainMenu;
	mnu_Archivo: TMenuItem;
    mnu_salir: TMenuItem;
    mnu_Mantenimiento: TMenuItem;
    mnu_CambiarPassword: TMenuItem;
    mnu_ventana: TMenuItem;
    mnu_Cascada: TMenuItem;
    mnu_Mosaico: TMenuItem;
    MenuItem5: TMenuItem;
    mnu_Siguiente: TMenuItem;
    mnu_Anterior: TMenuItem;
    mnu_Ayuda: TMenuItem;
    mnu_AcercaDe: TMenuItem;
    mnu_informes: TMenuItem;
    mnu_Validacion: TMenuItem;
    mnuValidarInfraccion: TMenuItem;
    mnuListadoInfracciones: TMenuItem;
    mnuConfigurarProxy: TMenuItem;
    N1: TMenuItem;
    ExplorarInfracciones1: TMenuItem;
    XPManifest1: TXPManifest;
    ABMMOtivosAnulacion: TMenuItem;
    N2: TMenuItem;
    InfractoresTelevia1: TMenuItem;
    mnu_ConsultarRIVM: TMenuItem;
    mnu_ConsultarRNVM: TMenuItem;
    procedure ABMMOtivosAnulacionClick(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure mnu_siguienteClick(Sender: TObject);
    procedure mnu_anteriorClick(Sender: TObject);
    procedure mnu_cascadaClick(Sender: TObject);
    procedure mnu_mosaicoClick(Sender: TObject);
    procedure mnu_AcercaDeClick(Sender: TObject);
    procedure mnu_salirClick(Sender: TObject);
    procedure mnu_CambiarPasswordClick(Sender: TObject);
    procedure mnu_ventanaClick(Sender: TObject);
    procedure mnuValidarInfraccionClick(Sender: TObject);
    procedure mnuListadoInfraccionesClick(Sender: TObject);
    procedure mnuConfigurarProxyClick(Sender: TObject);
    procedure ExplorarInfracciones1Click(Sender: TObject);
    procedure InfractoresTelevia1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    function CambiarEventoMenu(Menu: TMenu): Boolean;
    procedure mnu_ConsultarRIVMClick(Sender: TObject);
    procedure mnu_ConsultarRNVMClick(Sender: TObject);                           //SS_1147_NDR_20141216
  private
    { Private declarations }
    FUsarProxy: Boolean;
    FProxyServer: String;
    FProxyPort: Integer;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                //SS_1147_NDR_20141216
    procedure RefrescarBarra;
  public
	{ Public declarations }
	function Inicializar: Boolean;
    procedure DrawMenuItem(Sender: TObject; ACanvas: TCanvas;ARect: TRect; Selected: Boolean);             //SS_1147_NDR_20141216
  end;

var
  MainForm: TMainForm;

implementation

uses frmABMTeleviaInfractor, frmAcercaDe;

ResourceString
    MSG_SEGURIDAD_SEGURIDAD = 'Seguridad - Validaci�n Imagenes';
    MSG_VER_MASCARA = 'Ver Mascara';
    MSG_ELEGIR_IMAGEN_JPL = 'Elegir Imagen JPL';
    MSG_SIN_IMAGEN_JPL = 'Sin Imagen JPL';
    MSG_ANULAR_INFRACCION = 'Anular Infracci�n';

const
    FuncionesAdicionales: Array[1..5] of TPermisoAdicional = (
      (Funcion: 'seguridad_validacion_infractores'; Descripcion: MSG_SEGURIDAD_SEGURIDAD; Nivel: 1),
      (Funcion: 'ver_mascara'; Descripcion: MSG_VER_MASCARA; Nivel: 2),
    // Revision 1
      (Funcion: 'boton_elegir_imagen_JPL'; Descripcion: MSG_ELEGIR_IMAGEN_JPL; Nivel: 2),
      (Funcion: 'boton_sin_imagen_JPL'; Descripcion: MSG_SIN_IMAGEN_JPL; Nivel: 2),
    // Revision 3
      (Funcion: 'anular_infraccion'; Descripcion: MSG_ANULAR_INFRACCION; Nivel: 2)
    );

{$R *.dfm}
{$I ..\comunes\Ambiente.inc}   

procedure TMainForm.InfractoresTelevia1Click(Sender: TObject);
var Frm: TABMTeleviaInfractoresForm;
begin
    if FindFormOrCreate(TABMTeleviaInfractoresForm, Frm) then  begin
        Frm.WindowState := wsNormal;
        Frm.Show;
    end else begin
        if not (Frm.Inicializar(true)) then Frm.Release;
    end;
end;

function TMainForm.Inicializar: Boolean;
resourcestring
    CAPTION_USER = 'Usuario: %s';
Var
	f: TLoginForm;
begin
    SetBounds(Screen.WorkAreaLeft, Screen.WorkAreaTop, Screen.WorkAreaWidth, Screen.WorkAreaHeight);
	Result := False;
    SistemaActual := SYS_GESTION_INFRACCIONES;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    CambiarEventoMenu(Menu);                                                                                //SS_1147_NDR_20141216

    try
        Application.CreateForm(TLoginForm, f);
        if f.Inicializar(DMConnections.BaseCAC, SYS_GESTION_INFRACCIONES) and (f.ShowModal = mrOk) then begin
            UsuarioSistema := f.CodigoUsuario;

            //BEGIN : SS_925_NDR_20131122-----------------------------------------------------------------------
            //if not ValidarVersion(DMConnections.BaseCAC, SistemaActual) then begin
            //    MsgBoxErr(MSG_ERROR_VERSION, MSG_ERROR_VERSION_APLICACION, self.Caption, MB_ICONSTOP);
            //    Exit;
            //end;

            // Generamos el men� en la base de datos
            //if (InstallIni.ReadInteger('General', 'GenerarMenues', 1) = 1) then
            //    GenerateMenuFile(Menu, SYS_GESTION_INFRACCIONES, DMConnections.BaseCAC, FuncionesAdicionales);

            if not ValidarVersion(DMConnections.BaseCAC, SistemaActual, Menu, FuncionesAdicionales) then begin
                MsgBoxErr(MSG_ERROR_VERSION, MSG_ERROR_VERSION_APLICACION, self.Caption, MB_ICONSTOP);
                Exit;
            end;
            //END : SS_925_NDR_20131122-----------------------------------------------------------------------

            { INICIO : 20160315 MGO
            FUsarProxy := ApplicationIni.ReadBool('RNVM', 'USEPROXY', False);
            if FUsarProxy then begin
                FProxyServer := ApplicationIni.ReadString('RNVM', 'PROXYSERVER', '');
                FProxyPort := ApplicationIni.ReadInteger('RNVM', 'SERVERPORT', 0);
            end;
            }
            FUsarProxy := InstallIni.ReadBool('RNVM', 'USEPROXY', False);
            if FUsarProxy then begin
                FProxyServer := InstallIni.ReadString('RNVM', 'PROXYSERVER', '');
                FProxyPort := InstallIni.ReadInteger('RNVM', 'SERVERPORT', 0);
            end;
            // FIN : 20160315 MGO

            StatusBar.Panels[0].text := Format(CAPTION_USER, [UsuarioSistema]);
            StatusBar.Panels[1].Text := Trim(Format(MSG_CONECTADO_A, [InstallIni.ReadString('Database CAC', 'Server', ''), InstallIni.ReadString('Database CAC', 'DatabaseName', '')]));
            StatusBar.Panels[1].Width:= StatusBar.Font.Size * (Length(StatusBar.Panels[1].text));
            RefrescarBarra;

            CargarAccesos(DMConnections.BaseCAC, UsuarioSistema, SYS_GESTION_INFRACCIONES);
            HabilitarPermisosMenu(Menu);
            //CargarInformes(DMConnections.BaseCAC, SYS_GESTION_INFRACCIONES, MainMenu1, DMConnections.cnInformes);
            CambiarEventoMenu(Menu);                                        //SS_1147_NDR_20141216

            Result := True;
        end else begin
            Result := False;
        end;
    finally
        f.Release;
    end;
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
	{$IFNDEF PRODUCCION}
        Caption := Caption + Format(' - Versi�n %s: %s', [cAmbiente, GetFileVersionString(GetExeName, True)]);
    {$ELSE}
        Caption := Caption + Format(' - Versi�n: %s', [GetFileVersionString(GetExeName, True)]);
    {$ENDIF}
end;

procedure TMainForm.FormPaint(Sender: TObject);
begin
     DrawMDIBackground(imagenFondo.Picture.Graphic);
end;


procedure TMainForm.mnu_SiguienteClick(Sender: TObject);
begin
 	if not ExisteAcceso('mnu_Siguiente') then begin
    	MsgBox(MSG_NO_TIENE_PERMISOS, Self.Caption, MB_ICONSTOP);
        Exit;
    end;
    Next;
end;

procedure TMainForm.mnu_AnteriorClick(Sender: TObject);
begin
 	if not ExisteAcceso('mnu_Anterior') then begin
    	MsgBox(MSG_NO_TIENE_PERMISOS, Self.Caption, MB_ICONSTOP);
        Exit;
    end;
    Previous;
end;

procedure TMainForm.mnu_CascadaClick(Sender: TObject);
begin
	if not ExisteAcceso('mnu_Cascada') then begin
    	MsgBox(MSG_NO_TIENE_PERMISOS, Self.Caption, MB_ICONSTOP);
        Exit;
    end;
    Cascade;
end;

// INICIO : TASK_149_MGO_20170310
procedure TMainForm.mnu_ConsultarRIVMClick(Sender: TObject);
var
    f: TfrmAdministrarRIVM;
begin
    if FindFormOrCreate(TfrmAdministrarRIVM, f) then
        f.Show
    else begin
        if f.Inicializar then f.Show else f.Release;
    end;
end;
// FIN : TASK_149_MGO_20170310

// INICIO : TASK_159_MGO_20170323
procedure TMainForm.mnu_ConsultarRNVMClick(Sender: TObject);
var
    f: TfrmConsultarRNVM;
begin
    if FindFormOrCreate(TfrmConsultarRNVM, f) then
        f.Show
    else begin
        if f.Inicializar then f.Show else f.Release;
    end;
end;
// FIN : TASK_159_MGO_20170323

procedure TMainForm.mnu_MosaicoClick(Sender: TObject);
begin
 	if not ExisteAcceso('mnu_Mosaico') then begin
    	MsgBox(MSG_NO_TIENE_PERMISOS, Self.Caption, MB_ICONSTOP);
        Exit;
    end;
    Tile;
end;

{******************************** Function Header ******************************
Function Name: mnu_AcercaDeClick
Author :
Date Created :
Description :
Parameters : Sender: TObject
Return Value : None

Revision : 1
    Author : ggomez
    Date : 29/01/2007
    Description : En la llamada f.Inicializa, agregu� el segundo par�metro ''
        debido a que cambi� el perfil de la funci�n en las Utiles.
*******************************************************************************}
procedure TMainForm.mnu_AcercaDeClick(Sender: TObject);
{                                                       SS_925_MBE_20131223
resourcestring
    CAPTION_ACERCADE = 'Atenci�n al Usuario';
var
    f : TAboutFrm;
    a: TfrmLocalAssistance;
}
begin
{                                                       SS_925_MBE_20131223
    Application.CreateForm(TfrmLocalAssistance, a);
    try
        if a.Inicializar then begin
            Application.CreateForm(TAboutFrm, f);
            if f.Inicializa(CAPTION_ACERCADE, '', '', a) then f.ShowModal;
            f.Release;
        end;
    finally
        a.Release;
    end; // finally
}
    Application.CreateForm(TAcercaDeForm, AcercaDeForm);
    AcercaDeForm.ShowModal;
    AcercaDeForm.Release;
end;


procedure TMainForm.mnu_SalirClick(Sender: TObject);
begin
 	if not ExisteAcceso('mnu_Salir') then begin
    	MsgBox(MSG_NO_TIENE_PERMISOS, Self.Caption, MB_ICONSTOP);
        Exit;
    end;
    Close;
end;

procedure TMainForm.mnu_CambiarPasswordClick(Sender: TObject);
Var
	f: TFormCambioPassword;
begin
 	if not ExisteAcceso('mnu_CambiarPassword') then begin
    	MsgBox(MSG_NO_TIENE_PERMISOS, Self.Caption, MB_ICONSTOP);
        Exit;
    end;
	Application.CreateForm(TFormCambioPassword, f);
	if f.Inicializa(DMConnections.BaseCAC, UsuarioSistema) then f.ShowModal;
	f.Release;
end;

procedure TMainForm.mnu_VentanaClick(Sender: TObject);
begin
 	if not ExisteAcceso('mnu_Ventana') then begin
    	MsgBox(MSG_NO_TIENE_PERMISOS, Self.Caption, MB_ICONSTOP);
        Exit;
    end;
	mnu_cascada.Enabled := GetMenuItemCount(mnu_ventana.handle) > 5;
	mnu_mosaico.Enabled := mnu_cascada.Enabled;
	mnu_anterior.Enabled := GetMenuItemCount(mnu_ventana.handle) > 7;
	mnu_siguiente.Enabled := mnu_anterior.Enabled;
end;

procedure TMainForm.mnuValidarInfraccionClick(Sender: TObject);
var
    F: TformValidarInfraccion;
begin
	if FindFormOrCreate(TformValidarInfraccion, f) then
		f.Show
	else begin
		if not f.Inicializar('', NullDate, 0, True)then
            f.Release;
	end;
end;

procedure TMainForm.mnuListadoInfraccionesClick(Sender: TObject);
begin
	if FindFormOrCreate(TFormListadoInfracciones, FormListadoInfracciones) then begin
		FormListadoInfracciones.Show;
	end else begin
		if not FormListadoInfracciones.Inicializar(True )then FormListadoInfracciones.Release;
	end;
end;

procedure TMainForm.mnuConfigurarProxyClick(Sender: TObject);
var
    f: TfrmConfig;
begin
    Application.CreateForm(Tfrmconfig, f);
    if f.inicializar then begin
        f.ShowModal;
        if f.ModalResult = mrOk then begin
            { INICIO : 20160315 MGO
            FUsarProxy := ApplicationIni.ReadBool('RNVM', 'USEPROXY', False);
            if FUsarProxy then begin
                FProxyServer := ApplicationIni.ReadString('RNVM', 'PROXYSERVER', '');
                FProxyPort := ApplicationIni.ReadInteger('RNVM', 'SERVERPORT', 0);
            end else begin
                FProxyServer := '';
                FProxyPort := 0;
            end;
            }
            FUsarProxy := InstallIni.ReadBool('RNVM', 'USEPROXY', False);
            if FUsarProxy then begin
                FProxyServer := InstallIni.ReadString('RNVM', 'PROXYSERVER', '');
                FProxyPort := InstallIni.ReadInteger('RNVM', 'SERVERPORT', 0);
            end else begin
                FProxyServer := '';
                FProxyPort := 0;
            end;
            // FIN : 20160315 MGO
            f.Release;
            RefrescarBarra;
        end;
    end else begin
        f.Release;
    end;
end;

procedure TMainForm.RefrescarBarra;
begin
    if FUsarProxy then
        StatusBar.Panels[2].Text := 'PROXY: ' + FProxyServer + ' Port: ' + IntToStr(FProxyPort)
    else 
        StatusBar.Panels[2].Text := 'NO USA PROXY';
end;

procedure TMainForm.ExplorarInfracciones1Click(Sender: TObject);
var
    f: TfrmExplorarInfraciones;
begin
    if FindFormOrCreate(TfrmExplorarInfraciones, f) then begin
        f.Show;
    end else begin
        if not f.Inicializar(SYS_GESTION_INFRACCIONES) then f.Release;
    end;
end;

procedure TMainForm.ABMMOtivosAnulacionClick(Sender: TObject);
var
    f: TfrmMotivosAnulacionInf;
begin
    if FindFormOrCreate(TfrmMotivosAnulacionInf,f) then begin
        f.Show
    end else begin
        if not f.Inicializar(ABMMOtivosAnulacion.Caption) then f.Release;
    end
end;

//BEGIN : SS_1147_NDR_20141216 -------------------------------------------------
procedure TMainForm.DrawMenuItem(Sender: TObject; ACanvas: TCanvas;
  ARect: TRect; Selected: Boolean);
var
  S: String;
begin
  with ACanvas do
  begin
    S := TMenuItem(Sender).Caption;
    if Selected then
    begin
        Brush.Color := FColorMenuSel;
        Font.Color := FColorFontSel;
    end
    else
    begin
        if TMenuItem(Sender).Enabled then
        begin
            Brush.Color := FColorMenu;
            Font.Color := FColorFont;
            Font.Style:=[fsBold];
        end
        else
        begin
            Brush.Color := FColorMenu;
            Font.Color := clGrayText;
            Font.Style:=[];
        end;
    end;


    if (Parent = nil) and (TMenuItem(Sender).MenuIndex = 5 ) and not Selected then
        ARect.Right := Width;

    FillRect(ARect);
    DrawText(ACanvas.Handle, PChar(S), Length(S), ARect, DT_SINGLELINE or DT_VCENTER);

    if S='-' then
    begin
      with ACanvas do
      begin
        ACanvas.MoveTo(ARect.Left,ARect.top + 4);
        ACanvas.Pen.Color := FColorFont;
        ACanvas.LineTo(ARect.Right, ARect.top + 4 );
      end;
    end;

  end;
end;
//END : SS_1147_NDR_20141216 -------------------------------------------------

//BEGIN : SS_1147_NDR_20141216 -------------------------------------------------
function TMainForm.CambiarEventoMenu(Menu: TMenu): Boolean;

	Procedure CambiarEventoItem(Nivel: Integer; Item: TmenuItem);
	Var
		i: Integer;
	begin
    if (Nivel <> 0) then
       Item.OnDrawItem := DrawMenuItem;

		for i := 0 to Item.Count - 1 do begin
			CambiarEventoItem(Nivel + 1, Item.Items[i]);
		end;
	end;

begin
	CambiarEventoItem(0, Menu.Items);
	Result := True;
end;
//END : SS_1147_NDR_20141216 -------------------------------------------------

end.


