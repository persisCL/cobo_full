object ABMTeleviaInfractoresForm: TABMTeleviaInfractoresForm
  Left = 191
  Top = 177
  Caption = 'Televias a infraccionar'
  ClientHeight = 400
  ClientWidth = 606
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 606
    Height = 33
    Habilitados = [btAlta, btModi, btSalir, btBuscar]
    OnClose = AbmToolbar1Close
  end
  object pnlAbm: TPanel
    Left = 0
    Top = 296
    Width = 606
    Height = 65
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    TabOrder = 1
    Visible = False
    object Label8: TLabel
      Left = 5
      Top = 9
      Width = 49
      Height = 13
      Caption = 'Patente:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label9: TLabel
      Left = 5
      Top = 36
      Width = 47
      Height = 13
      Caption = 'Televia:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object btnDesactivar: TBitBtn
      Left = 159
      Top = 6
      Width = 118
      Height = 26
      Caption = 'Desactivar Infracci'#243'n'
      TabOrder = 2
      Visible = False
      OnClick = btnDesactivarClick
    end
    object txtPatente: TMaskEdit
      Left = 63
      Top = 6
      Width = 90
      Height = 21
      Hint = 'Patente'
      CharCase = ecUpperCase
      Color = 16444382
      MaxLength = 6
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnChange = txtPatenteChange
    end
    object txtEtiqueta: TEdit
      Left = 63
      Top = 33
      Width = 90
      Height = 21
      Hint = 'Etiqueta o Tag'
      Color = 16444382
      MaxLength = 11
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnChange = txtEtiquetaChange
      OnKeyPress = txtEtiquetaKeyPress
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 361
    Width = 606
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object Notebook: TNotebook
      Left = 409
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        ExplicitWidth = 0
        ExplicitHeight = 0
        object BtnSalir: TButton
          Left = 116
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 26
          Top = 6
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object DBList1: TAbmList
    Left = 0
    Top = 33
    Width = 606
    Height = 263
    TabStop = True
    TabOrder = 0
    Align = alClient
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      #0'51'#0'Patente  '
      #0'70'#0'Televia         '
      #0'138'#0'Contract Serial Number       '
      #0'142'#0'Activacion                            '
      #0'97'#0'Usuario Ingreso     '
      #0'44'#0'Estado ')
    Table = spListaInfracciones
    Style = lbOwnerDrawFixed
    OnDrawItem = DBList1DrawItem
    OnRefresh = DBList1Refresh
    OnInsert = DBList1Insert
    OnEdit = DBList1Edit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object spListaInfracciones: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'TeleviaInfractorObtenerLista;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 472
    Top = 72
  end
end
