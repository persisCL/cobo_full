unit WritingToLog;

interface

uses Classes, SysUtils, EventLog, Util, ApplicationGlobalVariables;

Type
    TWriteToLog = procedure(Mensaje: AnsiString; Error: Boolean = False);

var
    gWriteToLog: TWriteToLog;

procedure WriteToLog(Mensaje: AnsiString; Error: Boolean = False);

implementation

procedure WriteToLog(Mensaje: AnsiString; Error: Boolean = False);
    resourceString
        MSG_PROCESAR_FILE_OPEN_ERROR    = 'Ha ocurrido el error "%s" al intentar la apertura del archivo Log %s del M�dulo %s';
        MSG_PROCESAR_FILE_WRITE_ERROR   = 'Ha ocurrido el error "%s" al intentar escribir el mensaje "%s" en el archivo Log %s del M�dulo %s';

    var
        ArchivoLog          : TextFile;
        NombreArchivoLog    : string;
        Ahora               : TDateTime;

    const
        cLineaLog           = '%s - %s - %s';
        cNomenclaturaLog    = 'Log_%s_%s.txt';
        cError              = 'ERROR';
        cInfo               = 'INFO';
begin
    try
        Ahora := Now;

        ForceDirectories(gPathLog);

        NombreArchivoLog :=
            GoodDir(gPathLog) +
            Format(cNomenclaturaLog,[gApplicationName, FormatDateTime('yyyymmdd', Ahora)]);

        AssignFile(ArchivoLog, NombreArchivoLog);

        try
            if FileExists(NombreArchivoLog) then begin
                Append(ArchivoLog);
            end
            else begin
                Rewrite(ArchivoLog);
            end;
        except
            on e:Exception do begin
                EventLogReportEvent(elError, Format(MSG_PROCESAR_FILE_OPEN_ERROR, [e.Message, NombreArchivoLog, gApplicationName]), '');
            end;
        end;

        try
            Writeln(ArchivoLog, Format(cLineaLog, [FormatDateTime('dd/mm/yyyy hh:nn:ss.zzz', Ahora), iif(Error, cError, cInfo), Mensaje]));
        except
            on e:Exception do begin
                EventLogReportEvent(elError, Format(MSG_PROCESAR_FILE_WRITE_ERROR, [e.Message, Mensaje, NombreArchivoLog, gApplicationName]), '');
            end;
        end;
    finally
        CloseFile(ArchivoLog);
    end;
end;

initialization
    gWriteToLog := WriteToLog;

finalization
    gWriteToLog := nil;

end.
