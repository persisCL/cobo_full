unit ApplicationControl;

interface

uses
    SysUtils,
    Windows;

function ApplicationLocalExecution: Boolean;

implementation

{
    Drives Type

    DRIVE_FIXED, DRIVE_REMOVABLE, DRIVE_CDROM, DRIVE_RAMDISK, DRIVE_REMOTE
}


function ApplicationLocalExecution: Boolean;
    var
        DriveType: Integer;
begin

    DriveType := GetDriveType(PChar(ExtractFilePath(ParamStr(0))));

    Result := (DriveType = DRIVE_FIXED);
end;

end.
