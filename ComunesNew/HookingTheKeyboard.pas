unit HookingTheKeyboard;

interface

    uses
        // User
        ApplicationGlobalVariables,
        WritingToLog,
        // Delphi
        Windows, Forms;

    type
        TKeyHooked = record
            KeyPressed,
            KeyReleased: Boolean;
        end;

    var
        KBHook: HHook; {this intercepts keyboard input}

        FKey_VK_RETURN,
        FKey_VK_F1,
        FKey_VK_F2,
        FKey_VK_F3,
        FKey_VK_F4,
        FKey_VK_F5,
        FKey_VK_F6,
        FKey_VK_F7,
        FKey_VK_F8,
        FKey_VK_F9,
        FKey_VK_F10,
        FKey_VK_F11,
        FKey_VK_F12 : TKeyHooked;

    procedure Key_Pressed(var pKey: TKeyHooked);
    procedure Key_Released(var pKey: TKeyHooked);
    function Key_HookIt(var pKey: TKeyHooked; pKeyReleased: Boolean): Integer;

    function KeyboardHookProc(Code: integer; WordParam: WPARAM; LongParam: LPARAM): LResult stdcall;

implementation

procedure Key_Pressed(var pKey: TKeyHooked);
begin
    pKey.KeyPressed     := True;
    pKey.KeyReleased    := False;
end;

procedure Key_Released(var pKey: TKeyHooked);
begin
    pKey.KeyPressed     := False;
    pKey.KeyReleased    := True;
end;

function Key_HookIt(var pKey: TKeyHooked; pKeyReleased: Boolean): Integer;
begin
    Result := 0;

    if pKeyReleased then begin
        Key_Released(pKey);
    end
    else begin
        if pKey.KeyReleased then begin
            Key_Pressed(pKey);
        end
        else Result := 1;
    end;
end;

function KeyboardHookProc(Code: integer; WordParam: WPARAM; LongParam: LPARAM): LResult stdcall;
    var
        KeyReleased : Boolean;

    const
        KeysHooked = [VK_RETURN, VK_F1, VK_F2, VK_F3, VK_F4, VK_F5, VK_F6, VK_F7, VK_F8, VK_F9, VK_F10, VK_F11, VK_F12];

    procedure HookToLog;
        var
            InfoToLog: AnsiString;
    begin
        case WordParam of
            VK_RETURN   : InfoToLog := 'VK_RETURN';
            VK_F1       : InfoToLog := 'VK_F1';
            VK_F2       : InfoToLog := 'VK_F2';
            VK_F3       : InfoToLog := 'VK_F3';
            VK_F4       : InfoToLog := 'VK_F4';
            VK_F5       : InfoToLog := 'VK_F5';
            VK_F6       : InfoToLog := 'VK_F6';
            VK_F7       : InfoToLog := 'VK_F7';
            VK_F8       : InfoToLog := 'VK_F8';
            VK_F9       : InfoToLog := 'VK_F9';
            VK_F10      : InfoToLog := 'VK_F10';
            VK_F11      : InfoToLog := 'VK_F11';
            VK_F12      : InfoToLog := 'VK_F12';
        end;

        if KeyReleased then begin
            InfoToLog := InfoToLog + ' Released';
        end
        else begin
            InfoToLog := InfoToLog + ' Pressed';

            if Result = 0 then begin
                InfoToLog := InfoToLog + ' Accepted';
            end
            else InfoToLog := InfoToLog + ' Ignored';
        end;

        gWriteToLog(InfoToLog);
    end;
begin
    Result := 0;

    case Code of
        3: begin
            if WordParam in KeysHooked then begin

                KeyReleased := (LongParam and (1 shl 31)) <> 0;

                case WordParam of
                    VK_RETURN   : Result := Key_HookIt(FKey_VK_RETURN, KeyReleased);
                    VK_F1       : Result := Key_HookIt(FKey_VK_F1, KeyReleased);
                    VK_F2       : Result := Key_HookIt(FKey_VK_F2, KeyReleased);
                    VK_F3       : Result := Key_HookIt(FKey_VK_F3, KeyReleased);
                    VK_F4       : Result := Key_HookIt(FKey_VK_F4, KeyReleased);
                    VK_F5       : Result := Key_HookIt(FKey_VK_F5, KeyReleased);
                    VK_F6       : Result := Key_HookIt(FKey_VK_F6, KeyReleased);
                    VK_F7       : Result := Key_HookIt(FKey_VK_F7, KeyReleased);
                    VK_F8       : Result := Key_HookIt(FKey_VK_F8, KeyReleased);
                    VK_F9       : Result := Key_HookIt(FKey_VK_F9, KeyReleased);
                    VK_F10      : Result := Key_HookIt(FKey_VK_F10, KeyReleased);
                    VK_F11      : Result := Key_HookIt(FKey_VK_F11, KeyReleased);
                    VK_F12      : Result := Key_HookIt(FKey_VK_F12, KeyReleased);
                end;

                if gLogInfo and Assigned(gWriteToLog) then HookToLog;
            end;
        end;
    end;
end;

initialization
    KBHook := SetWindowsHookEx(WH_KEYBOARD, KeyboardHookProc, hInstance,GetWindowTask(Application.Handle));

    Key_Released(FKey_VK_RETURN);
    Key_Released(FKey_VK_F1);
    Key_Released(FKey_VK_F2);
    Key_Released(FKey_VK_F3);
    Key_Released(FKey_VK_F4);
    Key_Released(FKey_VK_F5);
    Key_Released(FKey_VK_F6);
    Key_Released(FKey_VK_F7);
    Key_Released(FKey_VK_F8);
    Key_Released(FKey_VK_F9);
    Key_Released(FKey_VK_F10);
    Key_Released(FKey_VK_F11);
    Key_Released(FKey_VK_F12);

finalization
  UnhookWindowsHookEx(KBHook);

end.
