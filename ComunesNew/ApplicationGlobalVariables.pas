unit ApplicationGlobalVariables;

interface

uses
    // Delphi
    SysUtils,
    // User
    Util;

var
    gApplicationPath,
    gApplicationName,
    gApplicationEXE,
    gPathLog,
    gPathActualizacion: AnsiString;

    gLogInfo: Boolean;

procedure ApplicationGlobalVariables_Init;

implementation

procedure ApplicationGlobalVariables_Init;
begin
    gApplicationPath    := GoodDir(ExtractFilePath(ParamStr(0)));
    gApplicationName    := ExtractFileName(ParamStr(0));
    gApplicationEXE     := ParamStr(0);
    gPathLog            := GoodDir(InstallIni.ReadString('Paths', 'Log', '.\Log'));
    gPathActualizacion  := GoodDir(InstallIni.ReadString('Paths', 'Actualizacion', ''));
    gLogInfo            := InstallIni.ReadBool('General', 'LogInfo', False);
end;

initialization
    ApplicationGlobalVariables_Init;

end.
