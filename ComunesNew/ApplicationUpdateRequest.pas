unit ApplicationUpdateRequest;


interface

    uses
        // User
        ApplicationGlobalVariables,
        WritingToLog,
        // Delphi
        Windows,
        ShellAPI,
        SysUtils,
        IniFiles,
        Classes;

    type
        THayQueActualizar = function (pModuloActual,pModuloActualizado: String): Boolean; Stdcall;

    function ApplicationUpdateNeeded: Boolean;
    function ApplicationINIUPdateNeeded: Boolean;
    function ActualizadorUpdateNeeded: Boolean;
    function MidasUpdateNeeded: Boolean;

    function OtherFilesUpdateNeeded(pFile: string): Boolean;

    procedure ApplicationUpdateExecute;
    procedure ApplicationINIUpdateExecute;
    procedure ActualizadorUpdateExecute;
    procedure MidasUpdateExecute;


    procedure GetOtherFilesCheckUpdate;
    procedure OtherFilesUpdateExecute(pFile: string);


implementation

    function ApplicationUpdateNeeded: Boolean;
        var
            dllHandle           : Cardinal;
            HayQueActualizar    : THayQueActualizar;
    begin
        Result := False;
        dllHandle := LoadLibrary('Actualizador.dll') ;

        if dllHandle <> 0 then try

            @HayQueActualizar := GetProcAddress(dllHandle, 'HayQueActualizar') ;

            if Assigned (HayQueActualizar) then begin

                Result := HayQueActualizar(PChar(gApplicationEXE), PChar(gPathActualizacion + gApplicationName));
            end
            else gWriteToLog('Funci�n "HayQueActualizar" en Actualizador.dll NO encontrada.', True);
        finally
            FreeLibrary(dllHandle) ;
        end
        else gWriteToLog('Librer�a Actualizador.dll NO encontrada.', True);
    end;

    function ApplicationINIUPdateNeeded: Boolean;
        var
            dllHandle           : Cardinal;
            HayQueActualizar    : THayQueActualizar;
    begin
        Result := False;
        dllHandle := LoadLibrary('Actualizador.dll') ;

        if dllHandle <> 0 then try

            @HayQueActualizar := GetProcAddress(dllHandle, 'HayQueActualizar') ;

            if Assigned (HayQueActualizar) then begin

                Result := HayQueActualizar(PChar(gApplicationPath + 'install.ini'), PChar(gPathActualizacion + 'install.ini'));
            end
            else gWriteToLog('Funci�n "HayQueActualizar" en Actualizador.dll NO encontrada.', True);
        finally
            FreeLibrary(dllHandle) ;
        end
        else gWriteToLog('Librer�a Actualizador.dll NO encontrada.', True);
    end;

    function ActualizadorUpdateNeeded: Boolean;
        var
            dllHandle           : Cardinal;
            HayQueActualizar    : THayQueActualizar;
    begin
        Result := False;
        dllHandle := LoadLibrary('Actualizador.dll') ;

        if dllHandle <> 0 then try

            @HayQueActualizar := GetProcAddress(dllHandle, 'HayQueActualizar') ;

            if Assigned (HayQueActualizar) then begin

                Result := HayQueActualizar(PChar(gApplicationPath + 'Actualizador.dll'), PChar(gPathActualizacion + 'Actualizador.dll'));
            end
            else gWriteToLog('Funci�n "HayQueActualizar" en Actualizador.dll NO encontrada.', True);
        finally
            FreeLibrary(dllHandle) ;
        end
        else begin
            gWriteToLog('Librer�a Actualizador.dll NO encontrada. Se procede a descargar ...', True);
            Result := True;
        end;
    end;

    function MidasUpdateNeeded: Boolean;
        var
            dllHandle           : Cardinal;
            HayQueActualizar    : THayQueActualizar;
    begin
        Result := False;
        dllHandle := LoadLibrary('Actualizador.dll') ;

        if dllHandle <> 0 then try

            @HayQueActualizar := GetProcAddress(dllHandle, 'HayQueActualizar') ;

            if Assigned (HayQueActualizar) then begin

                Result := HayQueActualizar(PChar(gApplicationPath + 'Midas.dll'), PChar(gPathActualizacion + 'Midas.dll'));
            end
            else gWriteToLog('Funci�n "HayQueActualizar" en Actualizador.dll NO encontrada.', True);
        finally
            FreeLibrary(dllHandle) ;
        end
        else gWriteToLog('Librer�a Actualizador.dll NO encontrada.', True);
    end;

    function OtherFilesUpdateNeeded(pFile: string): Boolean;
        var
            dllHandle           : Cardinal;
            HayQueActualizar    : THayQueActualizar;
    begin
        Result := False;
        dllHandle := LoadLibrary('Actualizador.dll') ;

        if dllHandle <> 0 then try

            @HayQueActualizar := GetProcAddress(dllHandle, 'HayQueActualizar') ;

            if Assigned (HayQueActualizar) then begin

                Result := HayQueActualizar(PChar(gApplicationPath + pFile), PChar(gPathActualizacion + pFile));
            end
            else gWriteToLog('Funci�n "HayQueActualizar" en Actualizador.dll NO encontrada.', True);
        finally
            FreeLibrary(dllHandle) ;
        end
        else gWriteToLog('Librer�a Actualizador.dll NO encontrada.', True);
    end;


    procedure ApplicationUpdateExecute;
        const
            cParametrosRundll32 = 'Actualizador.dll ActualizaModulo2 "%s" "%s"';
    begin
        ShellExecute(
          GetDesktopWindow,
          'OPEN',
          'Rundll32.exe',
          PChar(Format(cParametrosRundll32, [gApplicationEXE, gPathActualizacion + gApplicationName])),
          nil,
          SW_SHOWNORMAL);
    end;

    procedure ApplicationINIUpdateExecute;
        const
            cParametrosRundll32 = 'Actualizador.dll ActualizaModulo3 "%s" "%s" "%s"';
    begin
        ShellExecute(
          GetDesktopWindow,
          'OPEN',
          'Rundll32.exe',
          PChar(Format(cParametrosRundll32, [gApplicationPath + 'install.ini', gPathActualizacion + 'install.ini', gApplicationEXE])),
          nil,
          SW_SHOWNORMAL);
    end;

    procedure MidasUpdateExecute;
        const
            cParametrosRundll32 = 'Actualizador.dll ActualizaDLLorOCX "%s" "%s" "%s" "True"';
    begin
        ShellExecute(
          GetDesktopWindow,
          'OPEN',
          'Rundll32.exe',
          PChar(Format(cParametrosRundll32, [gApplicationPath + 'Midas.dll', gPathActualizacion + 'Midas.dll', gApplicationEXE])),
          nil,
          SW_SHOWNORMAL);
    end;

    procedure ActualizadorUpdateExecute;


        const
            cParametrosRundll32 = '%sActualizador.dll ActualizaModulo3 "%s" "%s" "%s"';
    begin
        ShellExecute(
          GetDesktopWindow,
          'OPEN',
          'Rundll32.exe',
          PChar(Format(cParametrosRundll32, [gPathActualizacion, gApplicationPath + 'Actualizador.dll', gPathActualizacion + 'Actualizador.dll', gApplicationEXE])),
          nil,
          SW_SHOWNORMAL);
    end;


    procedure OtherFilesUpdateExecute(pFile: string);
        const
            cParametrosRundll32 = 'Actualizador.dll ActualizaModulo3 "%s" "%s" "%s"';
    begin
        ShellExecute(
          GetDesktopWindow,
          'OPEN',
          'Rundll32.exe',
          PChar(Format(cParametrosRundll32, [gApplicationPath + pFile, gPathActualizacion + pFile, gApplicationEXE])),
          nil,
          SW_SHOWNORMAL);
    end;



    procedure GetOtherFilesCheckUpdate;
        var
            lIniFile    : TIniFile;
            lOtherFiles : TStringList;
            lCounter    : Integer;
            lFile       : string;
    begin
        try
            lOtherFiles := TStringList.Create;
            lIniFile    := TIniFile.Create('.\install.ini');

            lIniFile.ReadSectionValues('OtherFilesNeeded', lOtherFiles);

            if lOtherFiles.Count > 0 then begin
                for lCounter := 0 to lOtherFiles.Count - 1 do begin

                    lFile := Copy(lOtherFiles.Strings[lCounter], Pos('=', lOtherFiles.Strings[lCounter]) + 1, Length(lOtherFiles.Strings[lCounter]) - Pos('=', lOtherFiles.Strings[lCounter]));
                    if OtherFilesUpdateNeeded(lFile) then OtherFilesUpdateExecute(lFile);
                end;
            end;

        finally
            if Assigned(lOtherFiles) then FreeAndNil(lOtherFiles);
            if Assigned(lIniFile) then FreeAndNil(lIniFile);
        end;
    end;

end.
