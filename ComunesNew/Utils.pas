unit Utils;

interface

uses Windows, Forms, Registry, SysUtils;

function _GetTempPath: AnsiString;
function _GetExcelExecutable(var pExcelExecutable: string): Boolean;

function _GetExcelExtension(var pExtension: string): Boolean;

function _FileVersion(const pFileName: TFileName; var pMajor, pMinor, pRelease, pBuild: Word): Boolean;
function _FileVersionSTR(const pFileName: TFileName; var pVersion: AnsiString): Boolean;


implementation

function _GetTempPath: AnsiString;
    var
        lBuffer: Array[0..1024] of Char;
begin
    GetTempPath(SizeOf(lBuffer), lBuffer);
    Result := lBuffer
end;

function _GetExcelExecutable(var pExcelExecutable: string): Boolean;
begin
    Result := False;

    with TRegistry.Create(KEY_READ) do try

        RootKey := HKEY_LOCAL_MACHINE;
        if OpenKey('SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\excel.exe', false) then begin

            pExcelExecutable := ReadString('Path') + 'excel.exe';
            Result := True;
        end;
    finally
        Free;
    end;
end;

function _GetExcelExtension(var pExtension: string): Boolean;
    const
        cExcelExtensionXLS  = '.xls';
        cExcelExtensionXLSX = '.xlsx';
    var
        lMajor,
        lMinor,
        lRelease,
        lBuild           : Word;
        lExcelExecutable : string;
begin
    Result := False;

    if _GetExcelExecutable(lExcelExecutable) then begin
        if _FileVersion(lExcelExecutable, lMajor, lMinor, lRelease, lBuild) then begin
            if lMajor >= 12 then begin
                pExtension := cExcelExtensionXLSX
            end
            else pExtension := cExcelExtensionXLS;

            Result := True;
        end;
    end;
end;


function _FileVersion(const pFileName: TFileName; var pMajor, pMinor, pRelease, pBuild: Word): Boolean;
    var
        lVerInfoSize,
        lVerValueSize,
        lDummy       : Cardinal;
        lPVerInfo    : Pointer;
        lPVerValue   : PVSFixedFileInfo;
begin
    Result := False;

    lVerInfoSize := GetFileVersionInfoSize(PChar(pFileName), lDummy);
    GetMem(lPVerInfo, lVerInfoSize);

    try
        if GetFileVersionInfo(PChar(pFileName), 0, lVerInfoSize, lPVerInfo) then begin
            if VerQueryValue(lPVerInfo, '\', Pointer(lPVerValue), lVerValueSize) then begin
                with lPVerValue^ do begin

                    pMajor      := HiWord(dwFileVersionMS);
                    pMinor      := LoWord(dwFileVersionMS);
                    pRelease    := HiWord(dwFileVersionLS);
                    pBuild      := LoWord(dwFileVersionLS);

                    Result      := True;
//                    Result := Format('v%d.%d.%d build %d', [
//                        HiWord(dwFileVersionMS), //Major
//                        LoWord(dwFileVersionMS), //Minor
//                        HiWord(dwFileVersionLS), //Release
//                        LoWord(dwFileVersionLS)]); //Build
                end;
            end;
        end;
    finally
        FreeMem(lPVerInfo, lVerInfoSize);
    end;
end;

function _FileVersionSTR(const pFileName: TFileName; var pVersion: AnsiString): Boolean;
    const
        cVersion = '%d.%d.%d build %d';
    var
        lMajor,
        lMinor,
        lRelease,
        lBuild  : Word;
begin
    Result := False;

    if _FileVersion(pFileName, lMajor, lMinor, lRelease, lBuild) then begin

        pVersion := Format(cVersion, [lMajor, lMinor, lRelease, lBuild]);
        Result := True;
    end;
end;


end.
