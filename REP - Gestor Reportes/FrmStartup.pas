unit FrmStartup;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  jpeg, ExtCtrls, StdCtrls;

type
  TFormStartup = class(TForm)
    Bevel1: TBevel;
    Bevel2: TBevel;
    Bevel3: TBevel;
    Bevel4: TBevel;
    Image1: TImage;
    lblTitulo: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
    function Inicializar(Titulo: AnsiString): Boolean;
  end;

var
  FormStartup: TFormStartup;

implementation

{$R *.DFM}

function TFormStartup.Inicializar(Titulo: AnsiString): Boolean;
begin
    lblTitulo.Caption := Titulo;
    Result := True;
end;

end.
