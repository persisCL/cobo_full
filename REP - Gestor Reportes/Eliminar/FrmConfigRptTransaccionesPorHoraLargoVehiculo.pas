unit FrmConfigRptTransaccionesPorHoraLargoVehiculo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  TimeEdit, StdCtrls, validate, Dateedit, ExtCtrls, Db, DBTables, UtilProc,
  Util, UtilDB, TeEngine, Series, TeeProcs, Chart, OPButtons, ADODB, PeaProcs,
  DmiCtrls;

type
  TFormConfigRptTransaccionesPorHoraLargoVehiculo = class(TForm)
	Bevel1: TBevel;
	Label1: TLabel;
	deFechaInicial: TDateEdit;
	Label2: TLabel;
	deFechaFinal: TDateEdit;
	cbPuntosCobro: TComboBox;
	Label3: TLabel;
	Bevel2: TBevel;
    neClase1_Limite1: TNumericEdit;
    neClase1_Limite2: TNumericEdit;
    neClase2_Limite2: TNumericEdit;
    neClase3_Limite2: TNumericEdit;
    neClase2_Limite1: TNumericEdit;
    neClase4_Limite2: TNumericEdit;
    neClase3_Limite1: TNumericEdit;
    neClase4_Limite1: TNumericEdit;
    Label4: TLabel;
    Bevel3: TBevel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    neClase5_Limite1: TNumericEdit;
    neClase5_Limite2: TNumericEdit;
    neClase6_Limite2: TNumericEdit;
    neClase7_Limite2: TNumericEdit;
    neClase6_Limite1: TNumericEdit;
    neClase8_Limite2: TNumericEdit;
    neClase7_Limite1: TNumericEdit;
    neClase8_Limite1: TNumericEdit;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    btnAceptar: TOPButton;
    btnCancelar: TOPButton;
  private
    function GetFechaFinal: TDateTime;
    function GetFechaInicial: TDateTime;
    function GetPuntoCobro: Integer;
    procedure SetFechaFinal(const Value: TDateTime);
    procedure SetFechaInicial(const Value: TDateTime);
    procedure SetPuntoCobro(const Value: Integer);
    function GetClase1_Limite1: Integer;
    procedure SetClase1_Limite1(const Value: Integer);
    function GetClase1_Limite2: Integer;
    function GetClase2_Limite1: Integer;
    function GetClase2_Limite2: Integer;
    function GetClase3_Limite1: Integer;
    function GetClase3_Limite2: Integer;
    function GetClase4_Limite1: Integer;
    function GetClase4_Limite2: Integer;
    function GetClase5_Limite1: Integer;
    function GetClase5_Limite2: Integer;
    function GetClase6_Limite1: Integer;
    function GetClase6_Limite2: Integer;
    function GetClase7_Limite1: Integer;
    function GetClase7_Limite2: Integer;
    function GetClase8_Limite1: Integer;
    function GetClase8_Limite2: Integer;
    procedure SetClase1_Limite2(const Value: Integer);
    procedure SetClase2_Limite1(const Value: Integer);
    procedure SetClase2_Limite2(const Value: Integer);
    procedure SetClase3_Limite1(const Value: Integer);
    procedure SetClase3_Limite2(const Value: Integer);
    procedure SetClase4_Limite1(const Value: Integer);
    procedure SetClase4_Limite2(const Value: Integer);
    procedure SetClase5_Limite1(const Value: Integer);
    procedure SetClase5_Limite2(const Value: Integer);
    procedure SetClase6_Limite1(const Value: Integer);
    procedure SetClase6_Limite2(const Value: Integer);
    procedure SetClase7_Limite1(const Value: Integer);
    procedure SetClase7_Limite2(const Value: Integer);
    procedure SetClase8_Limite1(const Value: Integer);
    procedure SetClase8_Limite2(const Value: Integer);
	{ Private declarations }
  public
	{ Public declarations }
	Function Inicializar: Boolean;
	property FechaInicial: TDateTime read GetFechaInicial Write SetFechaInicial;
	property FechaFinal: TDateTime read GetFechaFinal Write SetFechaFinal;
	property PuntoCobro: Integer read GetPuntoCobro Write SetPuntoCobro;
    property Clase1_Limite1: Integer read GetClase1_Limite1 Write SetClase1_Limite1;
    property Clase1_Limite2: Integer read GetClase1_Limite2 Write SetClase1_Limite2;
    property Clase2_Limite1: Integer read GetClase2_Limite1 Write SetClase2_Limite1;
    property Clase2_Limite2: Integer read GetClase2_Limite2 Write SetClase2_Limite2;
    property Clase3_Limite1: Integer read GetClase3_Limite1 Write SetClase3_Limite1;
    property Clase3_Limite2: Integer read GetClase3_Limite2 Write SetClase3_Limite2;
    property Clase4_Limite1: Integer read GetClase4_Limite1 Write SetClase4_Limite1;
    property Clase4_Limite2: Integer read GetClase4_Limite2 Write SetClase4_Limite2;
    property Clase5_Limite1: Integer read GetClase5_Limite1 Write SetClase5_Limite1;
    property Clase5_Limite2: Integer read GetClase5_Limite2 Write SetClase5_Limite2;
    property Clase6_Limite1: Integer read GetClase6_Limite1 Write SetClase6_Limite1;
    property Clase6_Limite2: Integer read GetClase6_Limite2 Write SetClase6_Limite2;
    property Clase7_Limite1: Integer read GetClase7_Limite1 Write SetClase7_Limite1;
    property Clase7_Limite2: Integer read GetClase7_Limite2 Write SetClase7_Limite2;
    property Clase8_Limite1: Integer read GetClase8_Limite1 Write SetClase8_Limite1;
    property Clase8_Limite2: Integer read GetClase8_Limite2 Write SetClase8_Limite2;
  end;

var
  FormConfigRptTransaccionesPorHoraLargoVehiculo: TFormConfigRptTransaccionesPorHoraLargoVehiculo;

implementation

uses DMConnection;

{$R *.DFM}

function TFormConfigRptTransaccionesPorHoraLargoVehiculo.GetClase1_Limite1: Integer;
begin
    Result := neClase1_Limite1.ValueInt;
end;

function TFormConfigRptTransaccionesPorHoraLargoVehiculo.GetClase1_Limite2: Integer;
begin
    Result := neClase1_Limite2.ValueInt;
end;

function TFormConfigRptTransaccionesPorHoraLargoVehiculo.GetClase2_Limite1: Integer;
begin
    Result := neClase2_Limite1.ValueInt;
end;

function TFormConfigRptTransaccionesPorHoraLargoVehiculo.GetClase2_Limite2: Integer;
begin
    Result := neClase2_Limite2.ValueInt;
end;

function TFormConfigRptTransaccionesPorHoraLargoVehiculo.GetClase3_Limite1: Integer;
begin
    Result := neClase3_Limite1.ValueInt;
end;

function TFormConfigRptTransaccionesPorHoraLargoVehiculo.GetClase3_Limite2: Integer;
begin
    Result := neClase3_Limite2.ValueInt;
end;

function TFormConfigRptTransaccionesPorHoraLargoVehiculo.GetClase4_Limite1: Integer;
begin
    Result := neClase4_Limite1.ValueInt;
end;

function TFormConfigRptTransaccionesPorHoraLargoVehiculo.GetClase4_Limite2: Integer;
begin
    Result := neClase4_Limite2.ValueInt;
end;

function TFormConfigRptTransaccionesPorHoraLargoVehiculo.GetClase5_Limite1: Integer;
begin
    Result := neClase5_Limite1.ValueInt;
end;

function TFormConfigRptTransaccionesPorHoraLargoVehiculo.GetClase5_Limite2: Integer;
begin
    Result := neClase5_Limite2.ValueInt;
end;

function TFormConfigRptTransaccionesPorHoraLargoVehiculo.GetClase6_Limite1: Integer;
begin
    Result := neClase6_Limite1.ValueInt;
end;

function TFormConfigRptTransaccionesPorHoraLargoVehiculo.GetClase6_Limite2: Integer;
begin
    Result := neClase6_Limite2.ValueInt;
end;

function TFormConfigRptTransaccionesPorHoraLargoVehiculo.GetClase7_Limite1: Integer;
begin
    Result := neClase7_Limite1.ValueInt;
end;

function TFormConfigRptTransaccionesPorHoraLargoVehiculo.GetClase7_Limite2: Integer;
begin
    Result := neClase7_Limite2.ValueInt;
end;

function TFormConfigRptTransaccionesPorHoraLargoVehiculo.GetClase8_Limite1: Integer;
begin
    Result := neClase8_Limite1.ValueInt;
end;

function TFormConfigRptTransaccionesPorHoraLargoVehiculo.GetClase8_Limite2: Integer;
begin
    Result := neClase8_Limite2.ValueInt;
end;

function TFormConfigRptTransaccionesPorHoraLargoVehiculo.GetFechaFinal: TDateTime;
begin
	Result := deFechaFinal.Date;
end;

function TFormConfigRptTransaccionesPorHoraLargoVehiculo.GetFechaInicial: TDateTime;
begin
	Result := deFechaInicial.Date;
end;

function TFormConfigRptTransaccionesPorHoraLargoVehiculo.GetPuntoCobro: Integer;
begin
	Result := IVal(StrRight(cbPuntosCobro.Text, 20));
end;

Function TFormConfigRptTransaccionesPorHoraLargoVehiculo.Inicializar: Boolean;
resourcestring
    MSG_TODOS = 'Todos los Puntos de Cobro';
begin
    CargarPuntosCobro(DMConnections.BaseCAC, cbPuntosCobro);
    cbPuntosCobro.Items.Insert(0, MSG_TODOS);
    cbPuntosCobro.ItemIndex := 0;
  	FechaInicial := Date - 1;
	FechaFinal := Date - 1;
    Result := True;
end;

procedure TFormConfigRptTransaccionesPorHoraLargoVehiculo.SetClase1_Limite1(const Value: Integer);
begin
    neClase1_Limite1.ValueInt := Value;
end;

procedure TFormConfigRptTransaccionesPorHoraLargoVehiculo.SetClase1_Limite2(const Value: Integer);
begin
    neClase1_Limite2.ValueInt := Value;
end;

procedure TFormConfigRptTransaccionesPorHoraLargoVehiculo.SetClase2_Limite1(const Value: Integer);
begin
    neClase2_Limite1.ValueInt := Value;
end;

procedure TFormConfigRptTransaccionesPorHoraLargoVehiculo.SetClase2_Limite2(const Value: Integer);
begin
    neClase2_Limite2.ValueInt := Value;
end;

procedure TFormConfigRptTransaccionesPorHoraLargoVehiculo.SetClase3_Limite1(const Value: Integer);
begin
    neClase3_Limite1.ValueInt := Value;
end;

procedure TFormConfigRptTransaccionesPorHoraLargoVehiculo.SetClase3_Limite2(const Value: Integer);
begin
    neClase3_Limite2.ValueInt := Value;
end;

procedure TFormConfigRptTransaccionesPorHoraLargoVehiculo.SetClase4_Limite1(const Value: Integer);
begin
    neClase4_Limite1.ValueInt := Value;
end;

procedure TFormConfigRptTransaccionesPorHoraLargoVehiculo.SetClase4_Limite2(const Value: Integer);
begin
    neClase4_Limite2.ValueInt := Value;
end;

procedure TFormConfigRptTransaccionesPorHoraLargoVehiculo.SetClase5_Limite1(const Value: Integer);
begin
    neClase5_Limite1.ValueInt := Value;
end;

procedure TFormConfigRptTransaccionesPorHoraLargoVehiculo.SetClase5_Limite2(const Value: Integer);
begin
    neClase5_Limite2.ValueInt := Value;
end;

procedure TFormConfigRptTransaccionesPorHoraLargoVehiculo.SetClase6_Limite1(const Value: Integer);
begin
    neClase6_Limite1.ValueInt := Value;
end;

procedure TFormConfigRptTransaccionesPorHoraLargoVehiculo.SetClase6_Limite2(const Value: Integer);
begin
    neClase6_Limite2.ValueInt := Value;
end;

procedure TFormConfigRptTransaccionesPorHoraLargoVehiculo.SetClase7_Limite1(const Value: Integer);
begin
    neClase7_Limite1.ValueInt := Value;
end;

procedure TFormConfigRptTransaccionesPorHoraLargoVehiculo.SetClase7_Limite2(const Value: Integer);
begin
    neClase7_Limite2.ValueInt := Value;
end;

procedure TFormConfigRptTransaccionesPorHoraLargoVehiculo.SetClase8_Limite1(const Value: Integer);
begin
    neClase8_Limite1.ValueInt := Value;
end;

procedure TFormConfigRptTransaccionesPorHoraLargoVehiculo.SetClase8_Limite2(const Value: Integer);
begin
    neClase8_Limite2.ValueInt := Value;
end;

procedure TFormConfigRptTransaccionesPorHoraLargoVehiculo.SetFechaFinal(const Value: TDateTime);
begin
	deFechaFinal.Date := Value;
end;

procedure TFormConfigRptTransaccionesPorHoraLargoVehiculo.SetFechaInicial(const Value: TDateTime);
begin
	deFechaInicial.Date := Value;
end;

procedure TFormConfigRptTransaccionesPorHoraLargoVehiculo.SetPuntoCobro(const Value: Integer);
Var
	i: Integer;
begin
	for i := 0 to cbPuntosCobro.Items.Count - 1 do begin
		if IVal(StrRight(cbPuntosCobro.Items[i], 20)) = Value then begin
			cbPuntosCobro.ItemIndex := i;
			break;
		end;
	end;
end;

end.
