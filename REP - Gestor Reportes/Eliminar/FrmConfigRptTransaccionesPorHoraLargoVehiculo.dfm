object FormConfigRptTransaccionesPorHoraLargoVehiculo: TFormConfigRptTransaccionesPorHoraLargoVehiculo
  Left = 115
  Top = 378
  BorderStyle = bsDialog
  Caption = 'Configuraci'#243'n del Informe'
  ClientHeight = 288
  ClientWidth = 561
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  DesignSize = (
    561
    288)
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 7
    Top = 4
    Width = 545
    Height = 245
  end
  object Label1: TLabel
    Left = 14
    Top = 15
    Width = 63
    Height = 13
    Caption = 'Fecha Inicial:'
  end
  object Label2: TLabel
    Left = 14
    Top = 43
    Width = 58
    Height = 13
    Caption = 'Fecha Final:'
  end
  object Label3: TLabel
    Left = 216
    Top = 23
    Width = 74
    Height = 13
    Caption = 'Punto de Cobro'
  end
  object Bevel2: TBevel
    Left = 9
    Top = 68
    Width = 542
    Height = 2
  end
  object Label4: TLabel
    Left = 97
    Top = 95
    Width = 35
    Height = 26
    Alignment = taCenter
    Caption = 'Limite Inferior '
    Transparent = True
    WordWrap = True
  end
  object Bevel3: TBevel
    Left = 7
    Top = 122
    Width = 544
    Height = 2
  end
  object Label5: TLabel
    Left = 15
    Top = 131
    Width = 41
    Height = 13
    Caption = 'Rango 1'
  end
  object Label6: TLabel
    Left = 188
    Top = 95
    Width = 39
    Height = 26
    Alignment = taCenter
    Caption = 'Limite Superior'
    Transparent = True
    WordWrap = True
  end
  object Label7: TLabel
    Left = 15
    Top = 160
    Width = 41
    Height = 13
    Caption = 'Rango 2'
  end
  object Label8: TLabel
    Left = 15
    Top = 189
    Width = 41
    Height = 13
    Caption = 'Rango 3'
  end
  object Label9: TLabel
    Left = 15
    Top = 219
    Width = 41
    Height = 13
    Caption = 'Rango 4'
  end
  object Label10: TLabel
    Left = 222
    Top = 72
    Width = 140
    Height = 16
    Alignment = taCenter
    Caption = 'Rangos de Longitud'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
    WordWrap = True
  end
  object Label11: TLabel
    Left = 295
    Top = 131
    Width = 41
    Height = 13
    Caption = 'Rango 5'
  end
  object Label12: TLabel
    Left = 295
    Top = 160
    Width = 41
    Height = 13
    Caption = 'Rango 6'
  end
  object Label13: TLabel
    Left = 295
    Top = 189
    Width = 41
    Height = 13
    Caption = 'Rango 7'
  end
  object Label14: TLabel
    Left = 295
    Top = 219
    Width = 41
    Height = 13
    Caption = 'Rango 8'
  end
  object Label15: TLabel
    Left = 468
    Top = 95
    Width = 39
    Height = 26
    Alignment = taCenter
    Caption = 'Limite Superior'
    Transparent = True
    WordWrap = True
  end
  object Label16: TLabel
    Left = 377
    Top = 95
    Width = 35
    Height = 26
    Alignment = taCenter
    Caption = 'Limite Inferior '
    Transparent = True
    WordWrap = True
  end
  object deFechaInicial: TDateEdit
    Left = 103
    Top = 11
    Width = 90
    Height = 21
    AutoSelect = False
    TabOrder = 0
    AllowEmpty = False
    Date = -693594.000000000000000000
  end
  object deFechaFinal: TDateEdit
    Left = 103
    Top = 39
    Width = 90
    Height = 21
    AutoSelect = False
    TabOrder = 1
    AllowEmpty = False
    Date = -693594.000000000000000000
  end
  object cbPuntosCobro: TComboBox
    Left = 215
    Top = 38
    Width = 209
    Height = 22
    Style = csOwnerDrawFixed
    BiDiMode = bdRightToLeft
    ItemHeight = 16
    ParentBiDiMode = False
    TabOrder = 2
  end
  object neClase1_Limite1: TNumericEdit
    Left = 71
    Top = 127
    Width = 89
    Height = 21
    TabOrder = 3
    Decimals = 0
  end
  object neClase1_Limite2: TNumericEdit
    Left = 167
    Top = 127
    Width = 89
    Height = 21
    TabOrder = 4
    Decimals = 0
  end
  object neClase2_Limite2: TNumericEdit
    Left = 167
    Top = 156
    Width = 89
    Height = 21
    TabOrder = 6
    Decimals = 0
  end
  object neClase3_Limite2: TNumericEdit
    Left = 167
    Top = 185
    Width = 89
    Height = 21
    TabOrder = 8
    Decimals = 0
  end
  object neClase2_Limite1: TNumericEdit
    Left = 71
    Top = 156
    Width = 89
    Height = 21
    TabOrder = 5
    Decimals = 0
  end
  object neClase4_Limite2: TNumericEdit
    Left = 167
    Top = 215
    Width = 89
    Height = 21
    TabOrder = 10
    Decimals = 0
  end
  object neClase3_Limite1: TNumericEdit
    Left = 71
    Top = 185
    Width = 89
    Height = 21
    TabOrder = 7
    Decimals = 0
  end
  object neClase4_Limite1: TNumericEdit
    Left = 71
    Top = 215
    Width = 89
    Height = 21
    TabOrder = 9
    Decimals = 0
  end
  object neClase5_Limite1: TNumericEdit
    Left = 351
    Top = 127
    Width = 89
    Height = 21
    TabOrder = 11
    Decimals = 0
  end
  object neClase5_Limite2: TNumericEdit
    Left = 447
    Top = 127
    Width = 89
    Height = 21
    TabOrder = 12
    Decimals = 0
  end
  object neClase6_Limite2: TNumericEdit
    Left = 447
    Top = 156
    Width = 89
    Height = 21
    TabOrder = 14
    Decimals = 0
  end
  object neClase7_Limite2: TNumericEdit
    Left = 447
    Top = 185
    Width = 89
    Height = 21
    TabOrder = 16
    Decimals = 0
  end
  object neClase6_Limite1: TNumericEdit
    Left = 351
    Top = 156
    Width = 89
    Height = 21
    TabOrder = 13
    Decimals = 0
  end
  object neClase8_Limite2: TNumericEdit
    Left = 447
    Top = 215
    Width = 89
    Height = 21
    TabOrder = 18
    Decimals = 0
  end
  object neClase7_Limite1: TNumericEdit
    Left = 351
    Top = 185
    Width = 89
    Height = 21
    TabOrder = 15
    Decimals = 0
  end
  object neClase8_Limite1: TNumericEdit
    Left = 351
    Top = 215
    Width = 89
    Height = 21
    TabOrder = 17
    Decimals = 0
  end
  object btnAceptar: TOPButton
    Left = 393
    Top = 256
    Width = 76
    Anchors = [akRight, akBottom]
    Caption = '&Aceptar'
    Default = True
    ModalResult = 1
    TabOrder = 19
  end
  object btnCancelar: TOPButton
    Left = 476
    Top = 256
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 20
  end
end
