unit FrmRptTransaccionesPorHoraLargo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables, PeaProcs, Util, UtilProc, FrmConfigRptTransaccionesPorHoraLargoVehiculo,
  Series, ExtCtrls, TeeProcs, Chart, ppDB, ppDBPipe, ppComm, ppRelatv, ppProd, ppClass,
  ppReport, ppCtrls, ppBands, ppVar, ppPrnabl, ppCache, ADODB, TeEngine, UtilRB,
  DMConnection;

type
  TFormRptTransaccionesPorHoraLargo = class(TForm)
    spReporte: TADOStoredProc;
    rptReporte: TppReport;
    dsReporte: TDataSource;
	ppHeaderBand1: TppHeaderBand;
     DetailBand: TppDetailBand;
	ppFooterBand1: TppFooterBand;
	lbl_pagina: TppSystemVariable;
	ppLabel1: TppLabel;
    rbiReporte: TRBInterface;
     Grupo: TppGroup;
     ppGroupHeaderBand1: TppGroupHeaderBand;
     ppGroupFooterBand1: TppGroupFooterBand;
     ppLabel2: TppLabel;
     ppDBText1: TppDBText;
     ppDBText2: TppDBText;
     ppLabel4: TppLabel;
	ppDBText4: TppDBText;
     ppLine3: TppLine;
    ppDBText3: TppDBText;
    ppDBText8: TppDBText;
    ppLabel9: TppLabel;
    ppGroup1: TppGroup;
    ppGroupHeaderBand2: TppGroupHeaderBand;
    ppGroupFooterBand2: TppGroupFooterBand;
    ppGroup3: TppGroup;
    ppGroupHeaderBand4: TppGroupHeaderBand;
    ppGroupFooterBand4: TppGroupFooterBand;
    ppLabel5: TppLabel;
    ppDBCalc1: TppDBCalc;
    ppLabel6: TppLabel;
    ppLabel10: TppLabel;
    ppDBCalc2: TppDBCalc;
    ppLine2: TppLine;
    ppLine4: TppLine;
    ppLine5: TppLine;
    ppLine6: TppLine;
    ppPageStyle1: TppPageStyle;
    dbpReporte: TppDBPipeline;
    ppImage1: TppImage;
    ppLabel3: TppLabel;
    ppLabel7: TppLabel;
    ppLine1: TppLine;
    pplblUsuario: TppLabel;
    ppLabel14: TppLabel;
    ppLRangoFecha: TppLabel;
	procedure rbiReporteSelect(Sender: TObject);
    procedure rptReporteBeforePrint(Sender: TObject);
  private
	{ Private declarations }
	FPuntoCobro: Integer;
	FFechaInicial, FFechaFinal: TDateTime;
    FClase1_Limite1, FClase1_Limite2, FClase2_Limite1, FClase2_Limite2,
    FClase3_Limite1, FClase3_Limite2, FClase4_Limite1, FClase4_Limite2,
    FClase5_Limite1, FClase5_Limite2, FClase6_Limite1, FClase6_Limite2,
    FClase7_Limite1, FClase7_Limite2, FClase8_Limite1, FClase8_Limite2: Integer;
  public
	{ Public declarations }
	function Inicializar: Boolean;
	function Ejecutar: Boolean;
  end;

var
  FormRptTransaccionesPorHoraLargo: TFormRptTransaccionesPorHoraLargo;

implementation

{$R *.DFM}

function TFormRptTransaccionesPorHoraLargo.Inicializar: Boolean;
begin
	FFechaInicial := Date - 1;
	FFechaFinal := Date - 1;
    FPuntoCobro := 0;
    rbiReporte.Caption := Self.Caption;
	Result := True;
end;

function TFormRptTransaccionesPorHoraLargo.Ejecutar: Boolean;
begin
    rbiReporte.Execute;
	Result := True;
end;

procedure TFormRptTransaccionesPorHoraLargo.rbiReporteSelect(Sender: TObject);
var
	f: TFormConfigRptTransaccionesPorHoraLargoVehiculo;
begin
	Application.CreateForm(TFormConfigRptTransaccionesPorHoraLargoVehiculo, f);
	if f.Inicializar then begin
		f.FechaInicial := FFechaInicial;
		f.FechaFinal := FFechaFinal;
		f.PuntoCobro := FPuntoCobro;
        f.Clase1_Limite1 := FClase1_Limite1;
        f.Clase1_Limite2 := FClase1_Limite2;
        f.Clase2_Limite1 := FClase2_Limite1;
        f.Clase2_Limite2 := FClase2_Limite2;
        f.Clase3_Limite1 := FClase3_Limite1;
        f.Clase3_Limite2 := FClase3_Limite1;
        f.Clase4_Limite1 := FClase4_Limite1;
        f.Clase4_Limite2 := FClase4_Limite2;
        f.Clase5_Limite1 := FClase5_Limite1;
        f.Clase5_Limite2 := FClase5_Limite2;
        f.Clase6_Limite1 := FClase6_Limite1;
        f.Clase6_Limite2 := FClase6_Limite2;
        f.Clase7_Limite1 := FClase7_Limite1;
        f.Clase7_Limite2 := FClase7_Limite2;
        f.Clase8_Limite1 := FClase8_Limite1;
        f.Clase8_Limite2 := FClase8_Limite2;
		if (f.ShowModal = mrOk) then begin
			FFechaInicial := f.FechaInicial;
			FFechaFinal := f.FechaFinal;
			FPuntoCobro := f.PuntoCobro;
            FClase1_Limite1 := f.Clase1_Limite1;
            FClase1_Limite2 := f.Clase1_Limite2;
            FClase2_Limite1 := f.Clase2_Limite1;
            FClase2_Limite2 := f.Clase2_Limite2;
            FClase3_Limite1 := f.Clase3_Limite1;
            FClase3_Limite2 := f.Clase3_Limite2;
            FClase4_Limite1 := f.Clase4_Limite1;
            FClase4_Limite2 := f.Clase4_Limite2;
            FClase5_Limite1 := f.Clase5_Limite1;
            FClase5_Limite2 := f.Clase5_Limite2;
            FClase6_Limite1 := f.Clase6_Limite1;
            FClase6_Limite2 := f.Clase6_Limite2;
            FClase7_Limite1 := f.Clase7_Limite1;
            FClase7_Limite2 := f.Clase7_Limite2;
            FClase8_Limite1 := f.Clase8_Limite1;
            FClase8_Limite2 := f.Clase8_Limite2;
		end;
	end;
	f.Release;
end;

procedure TFormRptTransaccionesPorHoraLargo.rptReporteBeforePrint(Sender: TObject);
begin
    ppLRangoFecha.Text := format('%s - %s', [DateTimeToStr(FFechaInicial), DateTimeToStr(FFechaFinal)]);
    spReporte.close;
	With spReporte.Parameters do begin
		ParamByName('@NumeroPuntoCobro').Value := FPuntoCobro;
		ParamByName('@FechaDesde').Value := FFechaInicial;
		ParamByName('@FechaHasta').Value := FFechaFinal;
   		ParamByName('@Clase1_Limite1').Value := FClase1_Limite1;
		ParamByName('@Clase1_Limite2').Value := FClase1_Limite2;
   		ParamByName('@Clase2_Limite1').Value := FClase2_Limite1;
		ParamByName('@Clase2_Limite2').Value := FClase2_Limite2;
   		ParamByName('@Clase3_Limite1').Value := FClase3_Limite1;
		ParamByName('@Clase3_Limite2').Value := FClase3_Limite2;
   		ParamByName('@Clase4_Limite1').Value := FClase4_Limite1;
		ParamByName('@Clase4_Limite2').Value := FClase4_Limite2;
   		ParamByName('@Clase5_Limite1').Value := FClase5_Limite1;
		ParamByName('@Clase5_Limite2').Value := FClase5_Limite2;
   		ParamByName('@Clase6_Limite1').Value := FClase6_Limite1;
		ParamByName('@Clase6_Limite2').Value := FClase6_Limite2;
   		ParamByName('@Clase7_Limite1').Value := FClase7_Limite1;
		ParamByName('@Clase7_Limite2').Value := FClase7_Limite2;
   		ParamByName('@Clase8_Limite1').Value := FClase8_Limite1;
		ParamByName('@Clase8_Limite2').Value := FClase8_Limite2;
	end;
    spReporte.Open;    
end;

end.


