{

Firma       :   SS_925_NDR_20131122
Description :   Genera nuevos menu segun campo GenerarMenues de tabla SistemasVersiones (no del .ini)


Firma       :   SS_925_MBE_20131223
Description :   Se cambia la ventana de Acerca de.

Firma       : SS_1147_NDR_20141216
Descripcion : Color de menues segun concesionaria nativa

}

unit FrmMain;

interface

uses
    Forms, jpeg, ImgList, Controls, Classes, Navigator, ActnList, ActnMan, Menus, ComCtrls, ExtCtrls,
    PeaTypes, PeaProcs, Windows, UtilProc, Dialogs, ExtDlgs, sysUtils, ABMInformes,
    Login, Graphics, DMConnection, util, CambioPassword, DB, ADODB, RStrings,
    ABMCategoriasInformes, FrmDiccionarioDatos, ConstParametrosGenerales, XPMan,
    CustomAssistance, DPSAbout, StdCtrls, Categoria, ClaseBase, FrameDatosVehiculos;       //TASK_108_JMA_20170214

type
  TMainForm = class(TForm)
    StatusBar: TStatusBar;
    PageImagenes: TImageList;
    ImagenFondo: TImage;
    MenuImagenes: TImageList;
    MainMenu: TMainMenu;
    mnu_Archivo: TMenuItem;
    mnu_salir: TMenuItem;                                                                                                           
    mnu_ver: TMenuItem;
    mnu_IconBar: TMenuItem;
    mnu_Procesos: TMenuItem;
    mnu_MantenimientoInformes: TMenuItem;
//    mnu_Informes: TMenuItem;
    mnu_Mantenimiento: TMenuItem;
    mnu_CambiarPassword: TMenuItem;
    mnu_ventana: TMenuItem;
    mnu_Cascada: TMenuItem;
    mnu_Mosaico: TMenuItem;
    MenuItem5: TMenuItem;
    mnu_Siguiente: TMenuItem;
    mnu_Anterior: TMenuItem;
    mnu_Ayuda: TMenuItem;
    mnu_AcercaDe: TMenuItem;
    mnu_DiccionarioDatos: TMenuItem;
    mnu_BarraDeNavegacion: TMenuItem;
    mnu_BarradeHotLinks: TMenuItem;
    XPManifest1: TXPManifest;
    mnu_categoriaInformes: TMenuItem;
    procedure FormPaint(Sender: TObject);
    procedure mnu_siguienteClick(Sender: TObject);
    procedure mnu_anteriorClick(Sender: TObject);
    procedure mnu_cascadaClick(Sender: TObject);
    procedure mnu_mosaicoClick(Sender: TObject);
    procedure mnu_AcercaDeClick(Sender: TObject);
    procedure mnu_MantenimientoInformesClick(Sender: TObject);
    procedure mnu_salirClick(Sender: TObject);
    procedure mnu_IconBarClick(Sender: TObject);
    procedure mnu_CambiarPasswordClick(Sender: TObject);
    procedure mnu_ventanaClick(Sender: TObject);
    procedure mnu_CategoriasInformesClick(Sender: TObject);
    procedure mnu_DiccionarioDatosClick(Sender: TObject);
    procedure mnu_BarraDeNavegacionClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure mnu_BarradeHotLinksClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    function CambiarEventoMenu(Menu: TMenu): Boolean;                           //SS_1147_NDR_20141216
  private
    { Private declarations }
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                //SS_1147_NDR_20141216
    procedure RefrescarEstadosIconBar;
    procedure InicializarIconBar;
    procedure GuardarUserSettings;
  public
	{ Public declarations }
	function Inicializar: Boolean;
    procedure DrawMenuItem(Sender: TObject; ACanvas: TCanvas;ARect: TRect; Selected: Boolean);             //SS_1147_NDR_20141216
  end;

var
  MainForm: TMainForm;

implementation

uses frmAcercaDe, FrmStartup;

resourcestring
    PAGINA_PROCESOS = 'Page_Procesos';
    CAPTION_PROCESOS = 'Procesos';
    PAGINA_MANTENIMIENTO = 'Page_Matenimiento';
    CAPTION_MANTENIMIENTO = 'Matenimiento';
    CAPTION_DICCIONARIO_DATOS = 'Diccionario de Datos';
    CAPTION_MANTENIMIENTO_INFORMES = 'Mantenimiento de Informes';
    CAPTION_PARAMETROS = 'Par�metros del Sistema';
	CAPTION_CAMBIO_CONTRASENIA	= 'Cambio de Contrase�a';

{$R *.dfm}
{$I ..\comunes\Ambiente.inc}    

function TMainForm.Inicializar: Boolean;
resourcestring
    CAPTION_PROCESOS = 'Procesos';
    CAPTION_USUARIO = 'Usuario: %s';
    CAPTION_MANTENIMIENTO = 'Matenimiento';
    CAPTION_PARAMETROS = 'Par�metros del Sistema';
    CAPTION_DICCIONARIO_DATOS = 'Diccionario de Datos';
	CAPTION_CAMBIO_CONTRASENIA	= 'Cambio de Contrase�a';
    CAPTION_CATEGORIAS_INFORMES = 'Categor�as de Informes';
    CAPTION_MANTENIMIENTO_INFORMES = 'Mantenimiento de Informes';
var
	f: TLoginForm;
begin
	Result := False;
    Application.Title := SYS_GESTION_INFORMES_TITLE;
    SistemaActual := SYS_GESTION_INFORMES;
    SetBounds(Screen.WorkAreaLeft, Screen.WorkAreaTop, Screen.WorkAreaWidth, Screen.WorkAreaHeight);

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    CambiarEventoMenu(MainMenu);                                                                            //SS_1147_NDR_20141216


    try
        Application.CreateForm(TLoginForm, f);
        if f.Inicializar(DMConnections.BaseCAC, SYS_GESTION_INFORMES) and (f.ShowModal = mrOk) then begin
            UsuarioSistema := f.CodigoUsuario;


            //BEGIN : SS_925_NDR_20131122-----------------------------------------------------------------------
            //if not ValidarVersion(DMConnections.BaseCAC, SistemaActual) then begin
            //    MsgBoxErr(MSG_ERROR_VERSION, MSG_ERROR_VERSION_APLICACION, self.Caption, MB_ICONSTOP);
            //    Exit;
            //end;
            //
            //// Generamos el men� en la base de datos
            //if (InstallIni.ReadInteger('General', 'GenerarMenues', 1) = 1) then begin
            //    if not GenerateMenuFile(Menu, SYS_GESTION_INFORMES, DMConnections.BaseCAC, []) then begin
            //        Result := False;
            //        Exit;
            //    end;
            //end;

            if not ValidarVersion(DMConnections.BaseCAC, SistemaActual, Menu, []) then begin
                MsgBoxErr(MSG_ERROR_VERSION, MSG_ERROR_VERSION_APLICACION, self.Caption, MB_ICONSTOP);
                Exit;
            end;
            //END : SS_925_NDR_20131122-----------------------------------------------------------------------



            StatusBar.Panels[0].text := Format(CAPTION_USUARIO, [UsuarioSistema]);
            CargarAccesos(DMConnections.BaseCAC, UsuarioSistema, SYS_GESTION_INFORMES);
            HabilitarPermisosMenu(Menu);
{INICIO:TASK_027_JMA_20160622
	    	CargarInformes(DMConnections.BaseCAC, SYS_GESTION_INFORMES, MainMenu, DMConnections.cnInformes);
TERMINO:TASK_027_JMA_20160622}
            CargarInformes(DMConnections.BaseCAC, SYS_GESTION_INFORMES, MainMenu);
            CambiarEventoMenu(MainMenu);                                                                            //SS_1147_NDR_20141216
            Result := True;
        end else begin
            Result := False;
        end;
        if Result then InicializarIconBar;
    finally
        f.Release;
        if Assigned(FormStartup) then FreeAndNil(FormStartup);
    end;
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
	{$IFNDEF PRODUCCION}
        Caption := Caption + Format(' - Versi�n %s: %s', [cAmbiente, GetFileVersionString(GetExeName, True)]);
    {$ELSE}
        Caption := Caption + Format(' - Versi�n: %s', [GetFileVersionString(GetExeName, True)]);
    {$ENDIF}
end;

procedure TMainForm.FormPaint(Sender: TObject);
begin
     DrawMDIBackground(imagenFondo.Picture.Graphic);
end;

procedure TMainForm.mnu_SiguienteClick(Sender: TObject);
begin
 	if not ExisteAcceso('mnu_Siguiente') then begin
    	MsgBox(MSG_NO_TIENE_PERMISOS, Self.Caption, MB_ICONSTOP);
        Exit;
    end;
    Next;
end;

procedure TMainForm.mnu_AnteriorClick(Sender: TObject);
begin
 	if not ExisteAcceso('mnu_Anterior') then begin
    	MsgBox(MSG_NO_TIENE_PERMISOS, Self.Caption, MB_ICONSTOP);
        Exit;
    end;
    Previous;
end;

procedure TMainForm.mnu_CascadaClick(Sender: TObject);
begin
 	if not ExisteAcceso('mnu_Cascada') then begin
    	MsgBox(MSG_NO_TIENE_PERMISOS, Self.Caption, MB_ICONSTOP);
        Exit;
    end;
    Cascade;
end;

procedure TMainForm.mnu_MosaicoClick(Sender: TObject);
begin
 	if not ExisteAcceso('mnu_Mosaico') then begin
    	MsgBox(MSG_NO_TIENE_PERMISOS, Self.Caption, MB_ICONSTOP);
        Exit;
    end;
    Tile;
end;

{******************************** Function Header ******************************
Function Name: mnu_AcercaDeClick
Author :
Date Created :
Description :
Parameters : Sender: TObject
Return Value : None

Revision : 1
    Author : ggomez
    Date : 29/01/2007
    Description : En la llamada f.Inicializa, agregu� el segundo par�metro ''
        debido a que cambi� el perfil de la funci�n en las Utiles.
*******************************************************************************}
procedure TMainForm.mnu_AcercaDeClick(Sender: TObject);
{                                                       SS_925_MBE_20131223
resourcestring
    CAPTION_ACERCADE = 'Atenci�n al Usuario';
var
    f : TAboutFrm;
    a: TfrmLocalAssistance;
}
begin
{                                                       SS_925_MBE_20131223
    Application.CreateForm(TfrmLocalAssistance, a);
    try
        if a.Inicializar then begin
            Application.CreateForm(TAboutFrm, f);
            if f.Inicializa(CAPTION_ACERCADE, '', '', a) then f.ShowModal;
            f.Release;
        end;
    finally
        a.Release;
    end; // finally
}
    Application.CreateForm(TAcercaDeForm, AcercaDeForm);
    AcercaDeForm.ShowModal;
    AcercaDeForm.Release;
end;

procedure TMainForm.mnu_MantenimientoInformesClick(Sender: TObject);
Var
	f: TFormABMInformes;
begin
	if not ExisteAcceso('mnu_MantenimientoInformes') then begin
    	MsgBox(MSG_NO_TIENE_PERMISOS, Self.Caption, MB_ICONSTOP);
        Exit;
    end;
	if FindFormOrCreate(TFormABMInformes, f) then
	    f.Show
	else begin
        if not f.inicializar(MainMenu) then f.Release;
	end;
end;

procedure TMainForm.mnu_SalirClick(Sender: TObject);
begin
 	if not ExisteAcceso('mnu_Salir') then begin
    	MsgBox(MSG_NO_TIENE_PERMISOS, Self.Caption, MB_ICONSTOP);
        Exit;
    end;
    Close;
end;

procedure TMainForm.mnu_IconBarClick(Sender: TObject);
begin
 	if not ExisteAcceso('mnu_IconBar') then begin
    	MsgBox(MSG_NO_TIENE_PERMISOS, Self.Caption, MB_ICONSTOP);
        Exit;
    end;
	GNavigator.iconBarVisible := not GNavigator.iconBarVisible;
    mnu_IconBar.Checked	:= GNavigator.iconBarVisible;
    InstallIni.WriteBool('General', 'IconBarVisible', GNavigator.iconBarVisible);
end;

procedure TMainForm.mnu_CambiarPasswordClick(Sender: TObject);
Var
	f: TFormCambioPassword;
begin
 	if not ExisteAcceso('mnu_CambiarPassword') then begin
    	MsgBox(MSG_NO_TIENE_PERMISOS, Self.Caption, MB_ICONSTOP);
        Exit;
    end;
	Application.CreateForm(TFormCambioPassword, f);
	if f.Inicializa(DMConnections.BaseCAC, UsuarioSistema) then f.ShowModal;
	f.Release;
end;

procedure TMainForm.mnu_VentanaClick(Sender: TObject);
begin
 	if not ExisteAcceso('mnu_Ventana') then begin
    	MsgBox(MSG_NO_TIENE_PERMISOS, Self.Caption, MB_ICONSTOP);
        Exit;
    end;
	mnu_cascada.Enabled := GetMenuItemCount(mnu_ventana.handle) > 5;
	mnu_mosaico.Enabled := mnu_cascada.Enabled;
	mnu_anterior.Enabled := GetMenuItemCount(mnu_ventana.handle) > 7;
	mnu_siguiente.Enabled := mnu_anterior.Enabled;
end;

procedure TMainForm.mnu_CategoriasInformesClick(Sender: TObject);
Var
	f: TFormCategoriasInformes;
begin
// 	if not ExisteAcceso('mnu_CategoriasInformes') then begin
//    	MsgBox(MSG_NO_TIENE_PERMISOS, Self.Caption, MB_ICONSTOP);
//        Exit;
//    end;
	if FindFormOrCreate(TFormCategoriasInformes, f) then
	    f.Show
	else begin
        if not f.inicializar then f.Release;
	end;
end;
{******************************** Function Header ******************************
Function Name: mnu_DiccionarioDatosClick
Author : llazarte
Date Created : 25/01/2006
Description : Permite elegir el servidor/base de datos del cual se mostrar� el diccionario
de datos
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TMainForm.mnu_DiccionarioDatosClick(Sender: TObject);
Var
	f: TFormDiccionarioDatos;
begin
 	if not ExisteAcceso('mnu_DiccionarioDatos') then begin
    	MsgBox(MSG_NO_TIENE_PERMISOS, Self.Caption, MB_ICONSTOP);
        Exit;
	end;
    if FindFormOrCreate(TFormDiccionarioDatos, f) then f.Show
    else begin
        if not f.inicializar then f.Release;
    end;
end;

{-----------------------------------------------------------------------------
  Procedure: TMainForm.InicializarIconBar
  Author:    ggomez
  Date:      21-Jun-2005
  Arguments: None
  Result:    None
  Description: Inicializa la barra de �conos.
-----------------------------------------------------------------------------}
procedure TMainForm.InicializarIconBar;
var
    Bmp: TBitmap;
    NavigatorHasPages : Boolean;
begin
    //------------------------------------------------------------------------
	// Navegador
    //------------------------------------------------------------------------
	Bmp := TBitmap.Create;

	GNavigator := TNavigator.Create(Self);

    GNavigator.ShowBrowserBar       := GetUserSettingShowBrowserBar;
    mnu_BarraDeNavegacion.Checked   := GNavigator.ShowBrowserBar;

    GNavigator.ShowHotLinkBar   := GetUserSettingShowHotLinksBar;
    mnu_BarraDeHotLinks.Checked := GNavigator.ShowHotLinkBar;

    GNavigator.IconBarVisible   := False;
    mnu_IconBar.Checked         := GNavigator.IconBarVisible;
    NavigatorHasPages           := False;

	//----------------------------------------------------------
	// Agregamos las p�ginas e �conos HABILITADAS al toolbar
	//----------------------------------------------------------

    // PROCESOS
    if ExisteAcceso('mnu_DiccionarioDatos') or ExisteAcceso('mnu_MantenimientoInformes') then begin
        GNavigator.AddPage(PAGINA_PROCESOS, CAPTION_PROCESOS, EmptyStr);
    	NavigatorHasPages := True;

        if ExisteAcceso('mnu_DiccionarioDatos') then begin
            PageImagenes.GetBitmap(0, Bmp);
            GNavigator.AddIcon(PAGINA_PROCESOS, CAPTION_DICCIONARIO_DATOS, EmptyStr, Bmp, mnu_DiccionarioDatos.OnClick);
            Bmp.Assign(nil);
        end;

        if ExisteAcceso('mnu_MantenimientoInformes') then begin
            PageImagenes.GetBitmap(1, Bmp);
            GNavigator.AddIcon(PAGINA_PROCESOS, CAPTION_MANTENIMIENTO_INFORMES, EmptyStr, Bmp, mnu_MantenimientoInformes.OnClick);
            Bmp.Assign(nil);
        end;
    end;

    // MANTENIMIENTO
    if ExisteAcceso('mnu_CambiarPassword') then begin
        GNavigator.AddPage(PAGINA_MANTENIMIENTO, CAPTION_MANTENIMIENTO, EmptyStr);
    	NavigatorHasPages := True;

        if ExisteAcceso('mnu_CambiarPassword') then begin
            PageImagenes.GetBitmap(3, Bmp);
            GNavigator.AddIcon(PAGINA_MANTENIMIENTO, CAPTION_CAMBIO_CONTRASENIA, EmptyStr, Bmp, mnu_CambiarPassword.OnClick);
            Bmp.Assign(nil);
        end;
    end;

	if NavigatorHasPages then GNavigator.IconBarActivePage := 0;

	Bmp.Free;
	Update;

    RefrescarEstadosIconBar;

    GNavigator.IconBarVisible   := GetUserSettingShowIconBar;
    mnu_IconBar.Checked		    := GNavigator.IconBarVisible;
    GNavigator.ConnectionCOP    := Nil;
    GNavigator.ConnectionCAC    := DMConnections.BaseCAC;

end;

{-----------------------------------------------------------------------------
  Function Name: RefrescarEstadosIconBar
  Author:    ggomez
  Date Created: 21/06/2005
  Description: Refresca los Estados de la IconBar
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMainForm.RefrescarEstadosIconBar;
begin
    if (GNavigator = Nil) then Exit;

    GNavigator.SetIconEnabled(PAGINA_PROCESOS, CAPTION_DICCIONARIO_DATOS, mnu_DiccionarioDatos.Enabled);
    GNavigator.SetIconEnabled(PAGINA_PROCESOS, CAPTION_MANTENIMIENTO_INFORMES, mnu_MantenimientoInformes.Enabled);

    GNavigator.SetIconEnabled(PAGINA_MANTENIMIENTO, CAPTION_CAMBIO_CONTRASENIA, mnu_CambiarPassword.Enabled);

end;

procedure TMainForm.mnu_BarraDeNavegacionClick(Sender: TObject);
begin
	GNavigator.ShowBrowserBar       := not GNavigator.ShowBrowserBar;
    mnu_BarraDeNavegacion.Checked   := GNavigator.ShowBrowserBar;
end;

procedure TMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    (* Guardar las opciones de configuraci�n del usuario. *)
    GuardarUserSettings;
end;

procedure TMainForm.mnu_BarradeHotLinksClick(Sender: TObject);
begin
	GNavigator.ShowHotLinkBar   := not GNavigator.ShowHotLinkBar;
    mnu_BarraDeHotLinks.Checked := GNavigator.ShowHotLinkBar;
end;

{-----------------------------------------------------------------------------
  Procedure: TMainForm.GuardarUserSettings
  Author:    ggomez
  Date:      30-Jun-2005
  Arguments: None
  Result:    None
  Description: Guarda las opciones de configuraci�n para el usuario logueado
    en el Sistema Operativo.
-----------------------------------------------------------------------------}
procedure TMainForm.GuardarUserSettings;
begin
    SetUserSettingShowIconBar(mnu_IconBar.Checked);
    SetUserSettingShowBrowserBar(mnu_BarraDeNavegacion.Checked);
    SetUserSettingShowHotLinksBar(mnu_BarraDeHotLinks.Checked);
end;

//BEGIN : SS_1147_NDR_20141216 -------------------------------------------------
procedure TMainForm.DrawMenuItem(Sender: TObject; ACanvas: TCanvas;
  ARect: TRect; Selected: Boolean);
var
  S: String;
begin
  with ACanvas do
  begin
    S := TMenuItem(Sender).Caption;
    if Selected then
    begin
        Brush.Color := FColorMenuSel;
        Font.Color := FColorFontSel;
    end
    else
    begin
        if TMenuItem(Sender).Enabled then
        begin
            Brush.Color := FColorMenu;
            Font.Color := FColorFont;
            Font.Style:=[fsBold];
        end
        else
        begin
            Brush.Color := FColorMenu;
            Font.Color := clGrayText;
            Font.Style:=[];
        end;
    end;


    if (Parent = nil) and (TMenuItem(Sender).MenuIndex = 6 ) and not Selected then
        ARect.Right := Width;

    FillRect(ARect);
    DrawText(ACanvas.Handle, PChar(S), Length(S), ARect, DT_SINGLELINE or DT_VCENTER);

    if S='-' then
    begin
      with ACanvas do
      begin
        ACanvas.MoveTo(ARect.Left,ARect.top + 4);
        ACanvas.Pen.Color := FColorFont;
        ACanvas.LineTo(ARect.Right, ARect.top + 4 );
      end;
    end;

  end;
end;
//END : SS_1147_NDR_20141216 -------------------------------------------------

//BEGIN : SS_1147_NDR_20141216 -------------------------------------------------
function TMainForm.CambiarEventoMenu(Menu: TMenu): Boolean;

	Procedure CambiarEventoItem(Nivel: Integer; Item: TmenuItem);
	Var
		i: Integer;
	begin
    if (Nivel <> 0) then
       Item.OnDrawItem := DrawMenuItem;

		for i := 0 to Item.Count - 1 do begin
			CambiarEventoItem(Nivel + 1, Item.Items[i]);
		end;
	end;

begin
	CambiarEventoItem(0, Menu.Items);
	Result := True;
end;
//END : SS_1147_NDR_20141216 -------------------------------------------------


end.

