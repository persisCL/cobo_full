object FormABMInformes: TFormABMInformes
  Left = 123
  Top = 155
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  Caption = 'Mantenimiento de Informes'
  ClientHeight = 554
  ClientWidth = 818
  Color = clBtnFace
  Constraints.MinHeight = 31
  Constraints.MinWidth = 160
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clBlack
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  KeyPreview = True
  OldCreateOrder = True
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 515
    Width = 818
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object Notebook: TNotebook
      Left = 488
      Top = 0
      Width = 330
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TButton
          Left = 239
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 166
          Top = 6
          Width = 75
          Height = 27
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 247
          Top = 7
          Width = 75
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
        object btn_Disenio: TButton
          Left = 85
          Top = 6
          Width = 75
          Height = 27
          Caption = 'Dise'#241'ar'
          TabOrder = 2
          OnClick = btn_DisenioClick
        end
      end
    end
  end
  object PageControl: TPageControl
    Left = 0
    Top = 267
    Width = 818
    Height = 248
    ActivePage = tab_Sistemas
    Align = alBottom
    TabOrder = 1
    object tab_General: TTabSheet
      Caption = 'General'
      Enabled = False
      DesignSize = (
        810
        220)
      object Label1: TLabel
        Left = 31
        Top = 15
        Width = 72
        Height = 13
        Caption = 'Descripci'#243'n:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label5: TLabel
        Left = 58
        Top = 42
        Width = 45
        Height = 13
        Caption = 'Detalle:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblTiempoEstimado: TLabel
        Left = 3
        Top = 186
        Width = 100
        Height = 13
        Caption = 'Tiempo estimado:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblsegundos: TLabel
        Left = 161
        Top = 186
        Width = 148
        Height = 13
        Caption = 'segundos (0 = no especificado)'
      end
      object txt_descripcion: TEdit
        Left = 111
        Top = 12
        Width = 683
        Height = 21
        Color = 16444382
        MaxLength = 60
        TabOrder = 0
      end
      object txt_Detalle: TMemo
        Left = 111
        Top = 39
        Width = 683
        Height = 137
        Anchors = [akLeft, akTop, akRight, akBottom]
        Color = clWhite
        MaxLength = 2000
        ScrollBars = ssVertical
        TabOrder = 1
      end
      object txtTiempoEstimado: TNumericEdit
        Left = 110
        Top = 183
        Width = 46
        Height = 21
        TabOrder = 2
        BlankWhenZero = False
      end
    end
    object tab_Consulta: TTabSheet
      Caption = 'Consulta'
      Enabled = False
      ImageIndex = 2
      OnExit = tab_ConsultaExit
      DesignSize = (
        810
        220)
      object Label7: TLabel
        Left = 461
        Top = 1
        Width = 56
        Height = 13
        Anchors = [akTop, akRight]
        Caption = 'Servidor :'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitLeft = 474
      end
      object Label11: TLabel
        Left = 628
        Top = 2
        Width = 88
        Height = 13
        Anchors = [akTop, akRight]
        Caption = 'Base de Datos:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitLeft = 641
      end
      object lblCategoria: TLabel
        Left = 461
        Top = 41
        Width = 57
        Height = 13
        Anchors = [akTop, akRight]
        Caption = 'Categor'#237'a'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitLeft = 474
      end
      object txt_consulta: TMemo
        Left = 0
        Top = 0
        Width = 446
        Height = 217
        Anchors = [akLeft, akTop, akRight, akBottom]
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        MaxLength = 8000
        ParentFont = False
        ScrollBars = ssVertical
        TabOrder = 0
        OnChange = txt_consultaChange
      end
      object txtServidor: THistoryEdit
        Left = 461
        Top = 17
        Width = 161
        Height = 21
        Anchors = [akTop, akRight]
        MaxLength = 60
        TabOrder = 1
      end
      object txtBasedeDatos: THistoryEdit
        Left = 627
        Top = 16
        Width = 161
        Height = 21
        Anchors = [akTop, akRight]
        MaxLength = 30
        TabOrder = 2
      end
      inline FrameCategInformes: TFrameCategInformes
        Left = 461
        Top = 61
        Width = 324
        Height = 160
        Align = alCustom
        Anchors = [akTop, akRight]
        TabOrder = 3
        TabStop = True
        ExplicitLeft = 461
        ExplicitTop = 61
        ExplicitWidth = 324
        ExplicitHeight = 160
        inherited ArbolCat: TTreeView
          Left = 0
          Top = 0
          Width = 324
          Height = 160
          Align = alClient
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 324
          ExplicitHeight = 160
        end
      end
    end
    object tab_Variables: TTabSheet
      Caption = 'Variables / Par'#225'metros'
      Enabled = False
      ImageIndex = 1
      OnEnter = tab_VariablesEnter
      DesignSize = (
        810
        220)
      object btn_Bajar: TBitBtn
        Left = 320
        Top = 64
        Width = 22
        Height = 22
        TabOrder = 0
        OnClick = btn_BajarClick
        Glyph.Data = {
          36030000424D3603000000000000360000002800000010000000100000000100
          18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
          0000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000
          0000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000FFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000
          0000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFF000000000000000000000000000000000000FFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000
          0000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF000000000000000000000000000000000000000000000000FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000
          0000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF0000000000000000000000000000000000000000000000000000000000
          00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000
          0000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF
          0000000000000000000000000000000000000000000000000000000000000000
          00000000FFFFFFFFFFFFFFFFFF00000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000FFFFFFFFFFFF000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      end
      object btn_Subir: TBitBtn
        Left = 320
        Top = 32
        Width = 22
        Height = 22
        TabOrder = 1
        OnClick = btn_SubirClick
        Glyph.Data = {
          36030000424D3603000000000000360000002800000010000000100000000100
          18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000FFFFFFFFFFFF000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000FFFFFFFFFFFFFFFFFF00000000000000000000000000000000
          0000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF
          0000000000000000000000000000000000000000000000000000000000000000
          00000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000
          0000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF0000000000000000000000000000000000000000000000000000000000
          00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000
          0000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF000000000000000000000000000000000000000000000000FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000
          0000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFF000000000000000000000000000000000000FFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000
          0000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000FFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
          0000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      end
      object VarPanel: TPanel
        Left = 352
        Top = 29
        Width = 457
        Height = 161
        Anchors = [akLeft, akRight]
        BevelOuter = bvNone
        TabOrder = 2
        DesignSize = (
          457
          161)
        object Label2: TLabel
          Left = 19
          Top = 14
          Width = 59
          Height = 13
          Anchors = [akLeft]
          Caption = 'Descripci'#243'n:'
        end
        object Label3: TLabel
          Left = 19
          Top = 47
          Width = 24
          Height = 13
          Anchors = [akLeft]
          Caption = 'Tipo:'
        end
        object Label4: TLabel
          Left = 19
          Top = 76
          Width = 44
          Height = 13
          Anchors = [akLeft]
          Caption = 'Consulta:'
        end
        object txt_consultavar: TMemo
          Left = 89
          Top = 76
          Width = 365
          Height = 77
          Anchors = [akLeft, akRight]
          Color = 16444382
          MaxLength = 8000
          ScrollBars = ssVertical
          TabOrder = 0
          OnExit = ControlChange
        end
        object cb_tipovar: TComboBox
          Left = 89
          Top = 43
          Width = 157
          Height = 21
          Style = csDropDownList
          Anchors = [akLeft]
          Color = 16444382
          ItemHeight = 13
          TabOrder = 1
          OnChange = ControlChange
          Items.Strings = (
            'AlfaNum'#233'rico'
            'Fecha'
            'Hora'
            'Lista'
            'Num'#233'rico')
        end
        object txt_descrivar: TEdit
          Left = 89
          Top = 10
          Width = 157
          Height = 21
          Anchors = [akLeft]
          Color = 16444382
          MaxLength = 60
          TabOrder = 2
          OnExit = ControlChange
        end
      end
      object DBListEx1: TDBListEx
        Left = 0
        Top = 0
        Width = 313
        Height = 161
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 130
            Header.Caption = 'Par'#225'metro'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'CodigoVariable'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 179
            Header.Caption = 'Descripci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Descripcion'
          end>
        DataSource = dsParametrosReporte
        DragReorder = True
        ParentColor = False
        TabOrder = 3
        TabStop = True
      end
    end
    object tab_Graficos: TTabSheet
      Caption = 'Gr'#225'ficos'
      ImageIndex = 4
      DesignSize = (
        810
        220)
      object btn_SinGrafico: TSpeedButton
        Left = 110
        Top = 10
        Width = 22
        Height = 22
        GroupIndex = 1
        Down = True
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00000094FF00009CFFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF0000009CEF00009CFFFFFFFF00FFFFFF00FFFFFF00FFFF
          FF0000009CF71010EFFF00009CF7FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF0000009CEF1010EFFF00009CEFFFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF000000B5DE1010EFFF00009CF7FFFFFF00FFFFFF00FFFFFF00FFFF
          FF0000009CF71010EFFF0000B5B5FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF000000B5E71010EFFF00009CEFFFFFFF00FFFFFF000000
          9CF71010EFFF0000B5BDFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF0000009CFF1010EFFF00009CEF00009CF71010
          EFFF0000B5BDFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000009CFF00009CFF1014FFFF0000
          9CF7FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000009CF71014FFFF00009CFF0000
          B5C6FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF0000009CF71014FFFF00009CF70000B5B51010
          EFFF0000B5C6FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF0000009CF71014FFFF0000B5BDFFFFFF00FFFFFF00080C
          DEEF1010EFFF0000B5C6FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF0000009CF71014FFFF0000B5D6FFFFFF00FFFFFF00FFFFFF00FFFF
          FF000000B5D61010EFFF0000B5C6FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF0000009CF71010EFFF0000B5D6FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF000000B5D60808D6FF0000B5C6FFFFFF00FFFFFF00FFFFFF00FFFF
          FF000000B5E70000B5BDFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF000000B5AD0000B5C6FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
        OnClick = btn_GraficoClick
      end
      object btn_GraficoBarras: TSpeedButton
        Left = 134
        Top = 10
        Width = 22
        Height = 22
        GroupIndex = 1
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF002124
          21F7212421FF212421FF212421FF212421FF212421FF212421FF212421FF2124
          21FF212421FF212421FF212421FF212421FF212421FFFFFFFF00FFFFFF002124
          21FF7B79FFFF4A59FFFF212421FF39C3FFFF39C7FFFF212421FF4ACF8CFF2124
          21FFB5EBFFF7212421FFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF002124
          21FF848AFFFF4A59FFFF212421FF42C7FFFF42C7FFFF212421FF4ACF8CFF2124
          21FFB5EBFFF7212421FFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF002124
          21FF848AFFFF4A59FFFF212421FF42C7FFFF42C7FFFF212421FF4ACF8CFF2124
          21FFB5EBFFF7212421FFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF002124
          21FF848AFFFF4A59FFFF212421FF42C7FFFF42C7FFFF212421FF4ACF8CFF2124
          21FFB5EBFFF7212421FFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF002124
          21FF6369FFFF4A4DFFF7212421FF42C7FFFF42C7FFFF212421FF4ACF8CFF2124
          21FFB5EBFFF7212421FFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF002124
          21FF212421FF212421FF212421F742C7FFFF42C7FFFF212421FF4ACF8CFF2124
          21FFB5EBFFF7212421FFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF002124
          21FFFFFFFF00FFFFFF00212421FF42C7FFFF42C7FFFF212421FF4ACF8CFF2124
          21FF212421FF212421F7FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF002124
          21FFFFFFFF00FFFFFF00212421FF29C3FFFF31C7FFFF212421FF4ACF8CFF2124
          21FFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF002124
          21FFFFFFFF00FFFFFF00212421FF212421FF212421FF212421FF4ACF8CFF2124
          21FFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF002124
          21FFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00212421FF4ACF8CFF2124
          21FFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF002124
          21FFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00212421FF4ACF8CFF2124
          21FFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF002124
          21FFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00212421FF4ACF8CFF2124
          21FFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF002124
          21FFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00212421F7212421FF2124
          21F7FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
        OnClick = btn_GraficoClick
      end
      object btn_GraficoTorta: TSpeedButton
        Left = 158
        Top = 10
        Width = 22
        Height = 22
        GroupIndex = 1
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00212421FF212421FF212421FF212421FF2124
          21FF212421FFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00212421FF5AC339FF5AC339FF5AC339FF212421FF29CF
          FFFF29CFFFFF212421FFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00212421FF5AC339FF5AC339FF5AC339FF39BA18FF212421FF18CF
          FFFF29CFFFFF29CFFFFF212421FFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00212421FF6369FFFF212421FF39BA18FF5AC339FF39BA18FF212421FF29CF
          FFFF29CFFFFF18CFFFFF29CFFFFF212421FFFFFFFF00FFFFFF00FFFFFF002124
          21FF7B82FFFF8C8EFFFF6369FFFF212421FF39BA18FF39BA18FF212421FF29CF
          FFFF29CFFFFF18C7FFFF212421FFFF9E7BFF212421FFFFFFFF00FFFFFF002124
          21FF8C8EFFFF848AFFFF8C92FFFF6369FFFF212421FF29BA08FF212421FF29CF
          FFFF18CFFFFF212421FFFF9E7BFFFFA67BFF212421FFFFFFFF00FFFFFF002124
          21FF8C8AFFFF848AFFFF848AFFFF848EFFFF6369FFFF212421FF212421FF18C7
          FFFF212421FFFF9E7BFFFF9E7BFFFF9E7BFF212421FFFFFFFF00FFFFFF002124
          21FF8486FFFF848AFFFF848AFFFF7B86FFFF848AFFFF4A59FFFF212421FF2124
          21FF212421FF212421FF212421FF212421FF212421FFFFFFFF00FFFFFF002124
          21FF8C8AFFFF848AFFFF848AFFFF7B86FFFF848AFFFF848AFFFF6369FFFF6369
          FFFF4A59FFFF4A59FFFF4A4DFFFF4A4DFFFF212421FFFFFFFF00FFFFFF002124
          21FF7B79FFFF848AFFFF848AFFFF7B86FFFF848AFFFF848AFFFF7B79FFFF7B79
          FFFF6369FFFF6369FFFF4A59FFFF4A4DFFFF212421FFFFFFFF00FFFFFF00FFFF
          FF00212421FF6B71FFFF848AFFFF7B86FFFF848AFFFF848AFFFF7B79FFFF7B79
          FFFF6369FFFF6369FFFF4251FFFF212421FFFFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00212421FF7B7DFFFF848AFFFF848AFFFF848AFFFF7B79FFFF7B79
          FFFF6369FFFF4251FFFF212421FFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00212421FF7371FFFF848AFFFF848AFFFF7B79FFFF7B79
          FFFF5259FFFF212421FFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00212421FF212421FF212421FF212421FF2124
          21FF212421FFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
        OnClick = btn_GraficoClick
      end
      object btn_GraficoVectorial: TSpeedButton
        Left = 182
        Top = 10
        Width = 22
        Height = 22
        GroupIndex = 1
        Glyph.Data = {
          36030000424D3603000000000000360000002800000010000000100000000100
          1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFF21242121242121242121242121242121242121
          2421212421212421212421212421212421212421212421FFFFFFFFFFFF212421
          212421FFFFFFFFFFFF212421FFFFFF212421FFFFFF212421FFFFFF212421FFFF
          FF212421FFFFFFFFFFFFFFFFFF212421FFFFFF212421FFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFF8C8A94FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF212421
          FFFFFFFFFFFF212421FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFF212421212421FFFFFFFFFFFF212421FFFFFFFF
          FFFFFFFFFF8C8A94FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF212421
          FFFFFFFFFFFFFFFFFFFFFFFF212421FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFF212421212421FFFFFFFFFFFFFFFFFFFFFFFF21
          2421FFFFFF8C8A94FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF212421
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF212421FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFF212421212421FFFFFF8C8A94FFFFFF8C8A94FF
          FFFFFFFFFF212421FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF212421
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF212421FFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFF212421212421FFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFF212421FFFFFF212421FFFFFFFFFFFFFFFFFF212421
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2124
          21212421FFFFFFFFFFFFFFFFFF212421737573FFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFF212421212421212421FFFFFFFFFFFFFFFFFF212421
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
        OnClick = btn_GraficoClick
      end
      object Label6: TLabel
        Left = 10
        Top = 10
        Width = 24
        Height = 13
        Caption = 'Tipo:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Panel3: TPanel
        Left = 0
        Top = 89
        Width = 809
        Height = 91
        Anchors = [akLeft, akRight]
        BevelOuter = bvNone
        TabOrder = 0
        object Label8: TLabel
          Left = 10
          Top = 3
          Width = 88
          Height = 13
          Caption = 'Campo con Datos:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label9: TLabel
          Left = 10
          Top = 27
          Width = 95
          Height = 13
          Caption = 'Campo con Valores:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object txt_CampoDatos: TEdit
          Left = 111
          Top = 0
          Width = 252
          Height = 21
          Color = 16444382
          MaxLength = 255
          TabOrder = 0
        end
        object txt_CampoValores: TEdit
          Left = 110
          Top = 27
          Width = 252
          Height = 21
          Color = 16444382
          MaxLength = 255
          TabOrder = 1
        end
      end
    end
    object tab_Sistemas: TTabSheet
      Caption = 'Sistemas'
      Enabled = False
      ImageIndex = 4
      object Label10: TLabel
        Left = 10
        Top = 12
        Width = 49
        Height = 13
        Caption = 'Sistema:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object ckFunciones: TVariantCheckListBox
        Left = 0
        Top = 99
        Width = 810
        Height = 121
        OnClickCheck = ckFuncionesClickCheck
        Align = alBottom
        BevelWidth = 0
        ItemHeight = 13
        Items = <>
        TabOrder = 0
      end
      object cbSistemas: TComboBox
        Left = 67
        Top = 9
        Width = 196
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 1
        OnChange = cbSistemasChange
      end
    end
    object Tab_Usuario: TTabSheet
      Caption = 'Usuario y Contrase'#241'a'
      Enabled = False
      ImageIndex = 5
      object lblUsuario: TLabel
        Left = 28
        Top = 67
        Width = 48
        Height = 13
        Caption = 'Usuario:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblContrasenia: TLabel
        Left = 20
        Top = 94
        Width = 69
        Height = 13
        Caption = 'Contrase'#241'a:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object txt_usuario: TEdit
        Left = 103
        Top = 67
        Width = 252
        Height = 21
        Color = 16444382
        MaxLength = 30
        ParentShowHint = False
        ShowHint = False
        TabOrder = 0
      end
      object txtcontrasenia: TEdit
        Left = 103
        Top = 94
        Width = 252
        Height = 21
        Color = 16444382
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 15
        ParentFont = False
        PasswordChar = '*'
        TabOrder = 1
      end
    end
  end
  object pnlSuperior: TPanel
    Left = 0
    Top = 0
    Width = 818
    Height = 41
    Align = alTop
    TabOrder = 2
    object Botonera: TToolBar
      Left = 9
      Top = 6
      Width = 176
      Height = 29
      Align = alNone
      ButtonHeight = 25
      Caption = 'Botonera'
      DisabledImages = Imagenes
      Images = ImageList1
      TabOrder = 0
      object ToolButton1: TToolButton
        Left = 0
        Top = 0
        Hint = 'Salir'
        Caption = 'btnSalir'
        ImageIndex = 0
        ParentShowHint = False
        ShowHint = True
        OnClick = BtnSalirClick
      end
      object ToolButton2: TToolButton
        Left = 23
        Top = 0
        Width = 8
        Caption = 'ToolButton2'
        ImageIndex = 1
        Style = tbsSeparator
        Visible = False
      end
      object btnAgregar: TToolButton
        Left = 31
        Top = 0
        Hint = 'Agregar informe'
        Caption = 'btnAgregar'
        ImageIndex = 1
        ParentShowHint = False
        ShowHint = True
        OnClick = btnAgregarClick
      end
      object btnEliminar: TToolButton
        Left = 54
        Top = 0
        Hint = 'Eliminar informe'
        Caption = 'btnEliminar'
        ImageIndex = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = btnEliminarClick
      end
      object btnEditar: TToolButton
        Left = 77
        Top = 0
        Hint = 'Modificar informe'
        Caption = 'btnEditar'
        ImageIndex = 3
        ParentShowHint = False
        ShowHint = True
        OnClick = btnEditarClick
      end
      object ToolButton6: TToolButton
        Left = 100
        Top = 0
        Width = 8
        Caption = 'ToolButton6'
        ImageIndex = 4
        Style = tbsSeparator
        Visible = False
      end
      object btnImprimir: TToolButton
        Left = 108
        Top = 0
        Caption = 'btnImprimir'
        ImageIndex = 4
        OnClick = btnImprimirClick
      end
    end
  end
  object dbgrdConsultas: TDBGrid
    Left = 0
    Top = 41
    Width = 818
    Height = 226
    Align = alClient
    DataSource = dsConsultas
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clBlack
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnCellClick = dbgrdConsultasCellClick
    OnDblClick = dbgrdConsultasDblClick
    OnKeyPress = dbgrdConsultasKeyPress
    Columns = <
      item
        Expanded = False
        FieldName = 'CodigoConsulta'
        Title.Caption = 'C'#243'digo'
        Width = 40
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Descripcion'
        Title.Caption = 'Informe'
        Width = 754
        Visible = True
      end>
  end
  object ReporteDisenio: TppReport
    AutoStop = False
    DataPipeline = DBPipelineDiseniar
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = 'Report'
    PrinterSetup.PaperName = 'A4'
    PrinterSetup.PrinterName = 'Default'
    PrinterSetup.SaveDeviceSettings = False
    PrinterSetup.mmMarginBottom = 6350
    PrinterSetup.mmMarginLeft = 6350
    PrinterSetup.mmMarginRight = 6350
    PrinterSetup.mmMarginTop = 6350
    PrinterSetup.mmPaperHeight = 297000
    PrinterSetup.mmPaperWidth = 210000
    PrinterSetup.PaperSize = 9
    Template.DatabaseSettings.NameField = 'CodigoConsulta'
    Template.DatabaseSettings.TemplateField = 'Template'
    DeviceType = 'Screen'
    EmailSettings.ReportFormat = 'PDF'
    LanguageID = 'Default'
    OutlineSettings.CreateNode = True
    OutlineSettings.CreatePageNodes = True
    OutlineSettings.Enabled = True
    OutlineSettings.Visible = True
    PDFSettings.EmbedFontOptions = [efUseSubset]
    PDFSettings.EncryptSettings.AllowCopy = True
    PDFSettings.EncryptSettings.AllowInteract = True
    PDFSettings.EncryptSettings.AllowModify = True
    PDFSettings.EncryptSettings.AllowPrint = True
    PDFSettings.EncryptSettings.Enabled = False
    PDFSettings.FontEncoding = feAnsi
    RTFSettings.DefaultFont.Charset = DEFAULT_CHARSET
    RTFSettings.DefaultFont.Color = clWindowText
    RTFSettings.DefaultFont.Height = -13
    RTFSettings.DefaultFont.Name = 'Arial'
    RTFSettings.DefaultFont.Style = []
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = True
    XLSSettings.ExportComponents = []
    XLSSettings.MergeAdjacentCells = False
    XLSSettings.OpenXLSFile = True
    Left = 456
    Top = 206
    Version = '12.04'
    mmColumnWidth = 0
    DataPipelineName = 'DBPipelineDiseniar'
    object ppHeaderBand1: TppHeaderBand
      mmBottomOffset = 0
      mmHeight = 13229
      mmPrintPosition = 0
    end
    object ppDetailBand1: TppDetailBand
      Background1.Brush.Style = bsClear
      Background1.Gradient.EndColor = clWhite
      Background1.Gradient.StartColor = clWhite
      Background1.Gradient.Style = gsNone
      Background2.Brush.Style = bsClear
      Background2.Gradient.EndColor = clWhite
      Background2.Gradient.StartColor = clWhite
      Background2.Gradient.Style = gsNone
      mmBottomOffset = 0
      mmHeight = 13229
      mmPrintPosition = 0
    end
    object ppFooterBand1: TppFooterBand
      mmBottomOffset = 0
      mmHeight = 13229
      mmPrintPosition = 0
    end
    object ppParameterList1: TppParameterList
    end
  end
  object DSDiseniar: TDataSource
    DataSet = qryReporte
    Left = 376
    Top = 109
  end
  object DBPipelineDiseniar: TppDBPipeline
    DataSource = DSDiseniar
    CloseDataSource = True
    UserName = 'DBPipelineDiseniar'
    Left = 376
    Top = 157
  end
  object RBInterface1: TRBInterface
    Report = ReporteDisenio
    Caption = 'Reporte'
    OrderIndex = 0
    Left = 376
    Top = 205
  end
  object Diseniador: TppDesigner
    AllowSaveToFile = False
    Caption = 'ReportBuilder'
    DataSettings.SessionType = 'BDESession'
    DataSettings.AllowEditSQL = False
    DataSettings.DatabaseType = dtParadox
    DataSettings.GuidCollationType = gcString
    DataSettings.IsCaseSensitive = True
    DataSettings.SQLType = sqBDELocal
    Position = poScreenCenter
    Report = ReporteDisenio
    IniStorageType = 'IniFile'
    IniStorageName = '($LocalAppData)\RBuilder\RBuilder.ini'
    WindowHeight = 400
    WindowLeft = 100
    WindowTop = 50
    WindowWidth = 600
    OnTabChange = DiseniadorTabChange
    Left = 456
    Top = 157
  end
  object cdsParametrosReporte: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoVariable'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'Descripcion'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'Orden'
        DataType = ftInteger
      end
      item
        Name = 'Tipo'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'ConsultaLista'
        DataType = ftString
        Size = 8000
      end>
    IndexDefs = <
      item
        Name = 'cdsParametrosReporteIndex3'
        Fields = 'Orden'
        Options = [ixUnique]
      end>
    IndexFieldNames = 'Orden'
    Params = <>
    StoreDefs = True
    Left = 640
    Top = 64
  end
  object dsParametrosReporte: TDataSource
    AutoEdit = False
    DataSet = cdsParametrosReporte
    OnDataChange = dsParametrosReporteDataChange
    Left = 640
    Top = 112
  end
  object qryReporte: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    CommandTimeout = 60
    ParamCheck = False
    Parameters = <>
    Left = 456
    Top = 112
  end
  object cdsConsultas: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoConsulta'
        DataType = ftInteger
      end
      item
        Name = 'Descripcion'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'Detalle'
        DataType = ftString
        Size = 2000
      end
      item
        Name = 'TipoGrafico'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CampoDatosGrafico'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'CampoValoresGrafico'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'ErrorSintaxis'
        DataType = ftBoolean
      end
      item
        Name = 'Servidor'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'BasedeDatos'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'CodigoCategoria'
        DataType = ftInteger
      end
      item
        Name = 'UsuarioBaseDatos'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'Password'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'TiempoEstimado'
        DataType = ftSmallint
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    AfterScroll = cdsConsultasAfterScroll
    Left = 176
    Top = 112
  end
  object dsConsultas: TDataSource
    DataSet = cdsConsultas
    Left = 176
    Top = 168
  end
  object Imagenes: TImageList
    ShareImages = True
    Left = 232
    Bitmap = {
      494C010106001800540010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000002000000001002000000000000020
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008080800080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000808080008080800080808000FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008080800080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000808080008080800080808000FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008080800080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000808080008080800080808000FFFFFF000000000000000000000000000000
      0000808080000000000000000000000000000000000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008080800080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000808080008080800080808000FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008080800080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      800080808000FFFFFF0080808000FFFFFF000000000000000000000000008080
      800000000000000000000000000000000000FFFF0000FFFF0000808080000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008080800080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000808080008080800080808000FFFFFF000000000080808000000000000000
      00000000000000000000000000000000000000000000FFFF0000000000000000
      0000808080000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      8000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF0080808000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFF00000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      8000FFFFFF008080800080808000808080008080800000000000808080000000
      000080808000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      8000FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF0080808000FFFFFF000000000000000000000000000000000000000000FFFF
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      8000FFFFFF008080800080808000000000008080800080808000808080008080
      800080808000000000000000000000000000000000000000000000000000FFFF
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      8000FFFFFF0000000000FFFFFF00FFFFFF0080808000FFFFFF00000000008080
      800000000000000000000000000000000000000000008080800000000000FFFF
      0000FFFF00000000000000000000000000000000000000000000000000000000
      0000808080000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      8000FFFFFF0080808000808080000000000080808000FFFFFF00808080000000
      0000000000000000000000000000000000000000000000000000000000008080
      8000FFFF0000FFFF0000FFFF0000000000000000000000000000808080000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      8000FFFFFF00FFFFFF00FFFFFF00FFFFFF008080800080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      8000808080008080800080808000808080008080800000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000808080000000000000000000000000000000000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      800080808000FFFFFF0000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000080808000808080008080800080808000808080008080
      800080808000808080008080800080808000000000000000000080808000FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      000080808000FFFFFF0000000000000000000000000000000000000000008080
      800080808000FFFFFF0000000000000000000000000000000000000000000000
      0000000000008080800080808000808080000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000080808000808080008080800000000000FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF0080808000FFFFFF00FFFFFF000000000000000000FFFF
      FF0000000000FFFFFF00FFFFFF0080808000000000000000000080808000FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      000080808000FFFFFF0000000000000000000000000000000000FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000008080800080808000808080000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000008080800080808000808080008080800080808000FFFFFF008080
      80008080800080808000808080008080800000000000FFFFFF00808080000000
      000080808000808080000000000080808000000000000000000080808000FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      000080808000FFFFFF0000000000000000000000000080808000808080008080
      8000808080008080800080808000FFFFFF000000000000000000000000000000
      0000000000000000000000000000FFFFFF000000000080808000808080008080
      8000808080008080800080808000FFFFFF000000000000000000000000000000
      0000000000000000000000000000FFFFFF008080800080808000808080000000
      0000000000000000000000000000FFFFFF0080808000FFFFFF00000000000000
      00000000000000000000FFFFFF0080808000000000000000000080808000FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      000080808000FFFFFF0000000000000000000000000080808000808080008080
      8000808080008080800080808000000000000000000000000000000000000000
      0000000000000000000080808000808080000000000080808000808080008080
      8000808080008080800080808000000000000000000000000000000000000000
      0000000000000000000080808000808080008080800080808000FFFFFF000000
      0000000000000000000080808000808080008080800000000000FFFFFF000000
      0000FFFFFF00808080000000000080808000000000000000000080808000FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      000080808000FFFFFF0000000000000000000000000000000000000000008080
      800080808000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000080808000808080000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000080808000808080008080800080808000FFFFFF000000
      0000000000000000000000000000FFFFFF00FFFFFF0080808000FFFFFF008080
      800080808000FFFFFF000000000080808000000000000000000080808000FFFF
      FF0000000000000000000000000000000000000000000000000000000000FFFF
      FF0080808000FFFFFF0000000000000000000000000000000000000000008080
      8000808080000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF008080800080808000FFFFFF000000
      0000000000000000000080808000808080008080800080808000808080000000
      000080808000000000000000000080808000000000000000000080808000FFFF
      FF00000000000000000000000000000000000000000000000000808080000000
      000080808000FFFFFF0000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000000000000000
      0000000000000000000080808000808080000000000000000000000000000000
      000000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000080808000808080008080800080808000FFFFFF000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800000000000000000000000000080808000000000000000000080808000FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      000080808000FFFFFF0000000000000000000000000000000000000000000000
      000000000000000000000000000080808000FFFFFF00FFFFFF00000000000000
      0000000000000000000080808000808080000000000000000000000000000000
      000080808000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000080808000808080008080800080808000FFFFFF00FFFF
      FF00808080008080800080808000808080008080800080808000808080000000
      000000000000FFFFFF00FFFFFF0080808000000000000000000080808000FFFF
      FF000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      000080808000FFFFFF0000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF008080800080808000FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00000000000000000000000000FFFFFF008080
      800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00000000008080800080808000808080000000
      0000FFFFFF00FFFFFF0080808000808080000000000080808000000000000000
      000080808000808080000000000080808000000000000000000080808000FFFF
      FF00000000008080800080808000808080008080800080808000FFFFFF000000
      000080808000FFFFFF0000000000000000000000000080808000808080008080
      800080808000808080008080800080808000808080008080800080808000FFFF
      FF008080800080808000FFFFFF00000000000000000080808000808080008080
      800080808000808080008080800080808000808080008080800080808000FFFF
      FF008080800080808000FFFFFF00000000000000000000000000000000008080
      8000808080008080800080808000000000008080800000000000000000000000
      000000000000FFFFFF00FFFFFF0080808000000000000000000080808000FFFF
      FF000000000080808000FFFFFF00FFFFFF00FFFFFF0080808000FFFFFF000000
      000080808000FFFFFF0000000000000000000000000080808000808080008080
      8000808080008080800080808000808080008080800080808000808080000000
      0000808080008080800000000000000000000000000080808000808080008080
      8000808080008080800080808000808080008080800080808000808080000000
      0000808080008080800000000000000000000000000000000000000000000000
      0000000000008080800000000000808080000000000000000000FFFFFF00FFFF
      FF0080808000808080008080800080808000000000000000000080808000FFFF
      FF00000000008080800080808000808080008080800080808000000000000000
      000080808000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000808080008080800000000000000000000000
      00000000000000000000FFFFFF00FFFFFF000000000000000000000000008080
      800080808000FFFFFF0000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF000000000000000000000000000000
      0000808080000000000080808000FFFFFF000000000080808000808080000000
      000080808000FFFFFF000000000080808000000000000000000080808000FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      000080808000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000808080000000000000000000000000000000
      0000000000008080800080808000808080000000000000000000000000000000
      0000808080000000000000000000000000000000000000000000000000000000
      0000000000008080800080808000808080000000000000000000000000008080
      8000FFFFFF008080800080808000FFFFFF000000000000000000000000000000
      000080808000FFFFFF008080800000000000000000000000000080808000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0080808000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008080800080808000808080000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008080800080808000808080000000000000000000808080008080
      8000808080000000000080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00808080008080800000000000000000000000000000000000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000808080000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      8000000000000000000080808000808080008080800080808000808080008080
      800080808000000000000000000000000000424D3E000000000000003E000000
      2800000040000000200000000100010000000000000100000000000000000000
      000000000000000000000000FFFFFF008000FFFF000000000000FFFD00000000
      0000FFF8000000000000FFF1000000000000F023000000000000E78700000000
      0000CF0F0000000000009FA700000000E423BFD700000000E053BFF700000000
      E483AFF700000000E107AFF700000000E42F87E700000000E11FC1CF00000000
      E03FE79F00000000E07FF03F00000000C003F3FCFFFCFC00CFF3E3F8FFF89068
      CFF3C0F8C0F80092CFF380FE80FE1E3CCFF381FC81FC1C52CFF3E3FCFFFC1E02
      CFE3E7FEFFFE1C16CFD3FF7CFBFC180ECFF3FE3CF3FC0018CC13C009C00910B2
      C81380018001E178C81380138013FAC0C833FE7CE3FCF492CFF3FEF8F7F8E0F1
      C003FFF8FFF8C403C007FFFFFFFFEC0700000000000000000000000000000000
      000000000000}
  end
  object ImageList1: TImageList
    Left = 272
    Bitmap = {
      494C010106000800740010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000002000000001002000000000000020
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00C0C0C000FFFF
      FF00C0C0C000FFFFFF00C0C0C000FFFFFF00C0C0C000FFFFFF00C0C0C000FFFF
      FF00C0C0C000FFFFFF00C0C0C000000000000000000000000000000000000000
      0000808080000000000000000000000000000000000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C0C0C000FFFFFF00C0C0
      C000FFFFFF00C0C0C000FFFFFF00C0C0C000FFFFFF00C0C0C000FFFFFF00C0C0
      C000FFFFFF000000FF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00C0C0C000FFFF
      FF00C0C0C000FFFFFF00C0C0C000FFFFFF00C0C0C000FFFFFF00C0C0C000FFFF
      FF00C0C0C000FFFFFF00C0C0C000000000000000000000000000000000008080
      800000000000000000000000000000000000FFFF0000FFFF0000808080000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000080808000000000000000
      00000000000000000000000000000000000000000000FFFF0000000000000000
      0000808080000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFF00000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000000000000000000000000000FFFFFF0000000000FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000000000000000000000000000000000000000000000000000FFFF
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000FFFFFF000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      000000000000000000000000000000000000000000008080800000000000FFFF
      0000FFFF00000000000000000000000000000000000000000000000000000000
      0000808080000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000C0C0C000FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      8000FFFF0000FFFF0000FFFF0000000000000000000000000000808080000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000808080000000000000000000000000000000000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      8000000080000000800000008000000080000000800000008000000080000000
      8000000000000000000000000000000000000000000000000000000000000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      8000000080000000800000008000000080000000800000008000000080000000
      8000000000000000000000000000000000000000000000000000000000000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF0000000000FFFF
      FF000000000000000000FFFFFF00000000000000000000000000000000000000
      8000000080000000800000008000000080000000800000008000000080000000
      800000000000000000000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000000000000000000000000000
      000000000000000000000000000000000000FFFF0000000000000000000000FF
      FF00FFFFFF0000FFFF00FFFFFF0000FFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      8000000080000000800000008000000080000000800000008000000080000000
      800000000000000000000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000000000000000000000000000
      000000000000000000000000000000000000FFFF00000000000000FFFF00FFFF
      FF0000FFFF00FFFFFF00000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00000000000000000000000000000000000000
      8000000080000000800000008000000080000000800000008000000080000000
      8000000000000000000000000000000000000000000000000000000000000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFF000000000000FFFFFF0000FF
      FF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000000000FFFFFF000000
      000000000000FFFFFF00FFFFFF00000000000000000000000000000000000000
      8000000080000000800000008000000080000000800000008000000080000000
      8000000000000000000000000000000000000000000000000000000000000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFF00000000000000FFFF00FFFF
      FF0000FFFF00FFFFFF00000000000000000000000000000000000000000000FF
      FF0000000000FFFFFF00FFFFFF00000000000000000000000000000000000000
      800000008000000080000000800000008000000080000000800000FFFF000000
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFF000000000000FFFFFF0000FF
      FF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      8000000080000000800000008000000080000000800000008000000080000000
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF0000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFF00000000000000FFFF00FFFF
      FF0000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      8000000080000000800000008000000080000000800000008000000080000000
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF000000FF00000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FF00
      0000FF0000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF00FFFFFF0000FFFF00000000000000000000FFFF0000000000FFFFFF00FFFF
      FF000000000000000000FFFFFF00000000000000000000000000000000000000
      800000008000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000000080000000
      80000000000000000000000000000000000000000000FF000000FF000000FF00
      0000FF000000FF000000FF000000FF000000FF000000FF000000FF0000000000
      00000000FF000000FF00000000000000000000000000FF000000FF000000FF00
      0000FF000000FF000000FF000000FF000000FF000000FF000000FF0000000000
      00000000FF000000FF0000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      800000008000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000000080000000
      80000000000000000000000000000000000000000000FF000000FF000000FF00
      0000FF000000FF000000FF000000FF000000FF000000FF000000FF0000000000
      00000000FF000000FF00000000000000000000000000FF000000FF000000FF00
      0000FF000000FF000000FF000000FF000000FF000000FF000000FF0000000000
      00000000FF000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000FFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      800000008000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000000080000000
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF000000FF00000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FF00
      0000FF0000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000000000FFFFFF00FFFFFF000000000000000000FFFF
      FF0000000000FFFFFF00FFFFFF00000000000000000000000000000000000000
      8000000080000000800000008000000080000000800000008000000080000000
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF0000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FFFF000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      8000000080000000800000008000000080000000800000008000000080000000
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF00000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000200000000100010000000000000100000000000000000000
      000000000000000000000000FFFFFF008001FFFF000000000000FFFD00000000
      0000FFF8000000000000FFF1000000000000F023000000000000E78700000000
      0000CF0F0000000000009FA700000000E007BFD700000000E007BFF700000000
      E007AFF700000000E007AFF700000000E00F87E700000000E01FC1CF00000000
      E03FE79F00000000E07FF03F00000000C007FFFFFFFFFC00C007E7F8FFF8FC00
      C007E7F8FFF82000C00781FF81FF0000C00781FC81FC0000C007E7FCFFFC0000
      C007E7FFFFFF0000C007FFFCFFFC0000C007FEFCF7FC0000C007FE7FE7FF0000
      C00780138013E000C00780138013F800C007FE7FE7FFF000C007FEF8F7F8E001
      C007FFF8FFF8C403C007FFFFFFFFEC0700000000000000000000000000000000
      000000000000}
  end
  object tmrInicializarBusqueda: TTimer
    Enabled = False
    Interval = 5000000
    OnTimer = tmrInicializarBusquedaTimer
    Left = 656
    Top = 3
  end
end
