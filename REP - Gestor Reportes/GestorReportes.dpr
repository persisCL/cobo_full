program GestorReportes;

uses
  Forms,
  OpenOnce,
  Controls,
  Util,
  MidasLib,
  FrmMain in 'FrmMain.pas' {MainForm},
  FrmStartup in 'FrmStartup.pas' {FormStartup},
  DMConnection in '..\Comunes\DMConnection.pas' {DMConnections: TDataModule},
  DPSGrid in '..\Componentes\DPSGrid\DPSGrid.pas',
  MaskCombo in '..\Componentes\MaskCombo\MaskCombo.pas',
  WorkflowComp in '..\Componentes\Workflow\WorkflowComp.pas',
  DPSPageControl in '..\Componentes\PageControl\DPSPageControl.pas',
  PeaTypes in '..\Comunes\PeaTypes.pas',
  PeaProcs in '..\Comunes\PeaProcs.pas',
  Login in '..\Comunes\Login.pas' {LoginForm},
  Navigator in '..\Comunes\Navigator.pas' {NavWindowFrm},
  CambioPassword in '..\Comunes\CambioPassword.pas' {FormCambioPassword},
  FrmRptInformeUsuarioConfig in '..\Comunes\FrmRptInformeUsuarioConfig.pas' {FormRptInformeUsuarioConfig},
  FrmRptInformeUsuario in '..\Comunes\FrmRptInformeUsuario.pas' {FormRptInformeUsuario},
  ABMCategoriasInformes in 'ABMCategoriasInformes.pas' {FormCategoriasInformes},
  ABMInformes in 'ABMInformes.pas' {FormABMInformes},
  FrmDiccionarioDatos in '..\Comunes\FrmDiccionarioDatos.pas' {FormDiccionarioDatos},
  Mensajes in '..\Comunes\Mensajes.pas' {FormMensajes},
  RStrings in '..\Comunes\RStrings.pas',
  ConstParametrosGenerales in '..\Comunes\ConstParametrosGenerales.pas',
  FrmWrkDisplay in '..\Comunes\FrmWrkDisplay.pas' {FormWorkflowDisplay},
  FrmCancelarTarea in '..\Comunes\FrmCancelarTarea.pas' {FormCancelarTarea},
  FrmTerminarTarea in '..\Comunes\FrmTerminarTarea.pas' {FormTerminarTarea},
  UtilReportes in '..\Comunes\UtilReportes.pas',
  OrdenesServicio in '..\Comunes\OrdenesServicio.pas',
  FreContactoReclamo in '..\Comunes\FreContactoReclamo.pas' {FrameContactoReclamo: TFrame},
  FreCompromisoOrdenServicio in '..\Comunes\FreCompromisoOrdenServicio.pas' {FrameCompromisoOrdenServicio: TFrame},
  FreSolucionOrdenServicio in '..\Comunes\FreSolucionOrdenServicio.pas' {FrameSolucionOrdenServicio: TFrame},
  FrmReclamoGeneral in '..\Comunes\FrmReclamoGeneral.pas' {FormReclamoGeneral},
  FrmReclamoFactura in '..\Comunes\FrmReclamoFactura.pas' {FormReclamoFactura},
  FrmReclamoCuenta in '..\Comunes\FrmReclamoCuenta.pas' {FormReclamoCuenta},
  MenuesContextuales in '..\Comunes\MenuesContextuales.pas',
  ImagenesTransitos in '..\Comunes\ImagenesTransitos.pas' {frmImagenesTransitos},
  utilImg in '..\Comunes\utilImg.pas',
  JCAPIMin in '..\Componentes\Jpeg 12bits\JCAPIMin.PAS',
  JCAPISTD in '..\Componentes\Jpeg 12bits\JCAPISTD.PAS',
  JCCoefCt in '..\Componentes\Jpeg 12bits\JCCoefCt.pas',
  JCColor in '..\Componentes\Jpeg 12bits\JCColor.PAS',
  JCDCTMgr in '..\Componentes\Jpeg 12bits\JCDCTMgr.PAS',
  JCHuff in '..\Componentes\Jpeg 12bits\JCHuff.PAS',
  JCInit in '..\Componentes\Jpeg 12bits\JCInit.PAS',
  JCMainCt in '..\Componentes\Jpeg 12bits\JCMainCt.PAS',
  JCMarker in '..\Componentes\Jpeg 12bits\JCMarker.PAS',
  JCMaster in '..\Componentes\Jpeg 12bits\JCMaster.PAS',
  JCOMapi in '..\Componentes\Jpeg 12bits\JCOMapi.PAS',
  JConst in '..\Componentes\Jpeg 12bits\JConst.PAS',
  JCParam in '..\Componentes\Jpeg 12bits\JCParam.pas',
  JCPHuff in '..\Componentes\Jpeg 12bits\JCPHuff.pas',
  JCPrepCt in '..\Componentes\Jpeg 12bits\JCPrepCt.pas',
  JCSample in '..\Componentes\Jpeg 12bits\JCSample.pas',
  JDAPIMin in '..\Componentes\Jpeg 12bits\JDAPIMin.PAS',
  JDAPIStd in '..\Componentes\Jpeg 12bits\JDAPIStd.PAS',
  JDataDst in '..\Componentes\Jpeg 12bits\JDataDst.PAS',
  JDataSRC in '..\Componentes\Jpeg 12bits\JDataSRC.PAS',
  JDCoefCt in '..\Componentes\Jpeg 12bits\JDCoefCt.PAS',
  JDColor in '..\Componentes\Jpeg 12bits\JDColor.PAS',
  JDCT in '..\Componentes\Jpeg 12bits\JDCT.PAS',
  jddctmgr in '..\Componentes\Jpeg 12bits\jddctmgr.pas',
  JDefErr in '..\Componentes\Jpeg 12bits\JDefErr.PAS',
  JDHuff in '..\Componentes\Jpeg 12bits\JDHuff.PAS',
  JDInput in '..\Componentes\Jpeg 12bits\JDInput.PAS',
  JDMainCt in '..\Componentes\Jpeg 12bits\JDMainCt.pas',
  JDMarker in '..\Componentes\Jpeg 12bits\JDMarker.PAS',
  JDMaster in '..\Componentes\Jpeg 12bits\JDMaster.PAS',
  JDMerge in '..\Componentes\Jpeg 12bits\JDMerge.PAS',
  JDPHuff in '..\Componentes\Jpeg 12bits\JDPHuff.pas',
  JDPostCT in '..\Componentes\Jpeg 12bits\JDPostCT.PAS',
  JDSample in '..\Componentes\Jpeg 12bits\JDSample.PAS',
  JError in '..\Componentes\Jpeg 12bits\JError.PAS',
  JFDCTFlt in '..\Componentes\Jpeg 12bits\JFDCTFlt.PAS',
  JFDCTFst in '..\Componentes\Jpeg 12bits\JFDCTFst.PAS',
  JFDCTInt in '..\Componentes\Jpeg 12bits\JFDCTInt.PAS',
  JIDCTFlt in '..\Componentes\Jpeg 12bits\JIDCTFlt.PAS',
  JIDCTFst in '..\Componentes\Jpeg 12bits\JIDCTFst.PAS',
  JIDCTInt in '..\Componentes\Jpeg 12bits\JIDCTInt.PAS',
  JIDCTRed in '..\Componentes\Jpeg 12bits\JIDCTRed.PAS',
  JInclude in '..\Componentes\Jpeg 12bits\JInclude.PAS',
  JMemMgr in '..\Componentes\Jpeg 12bits\JMemMgr.pas',
  JMemNOBS in '..\Componentes\Jpeg 12bits\JMemNOBS.PAS',
  JMoreCfg in '..\Componentes\Jpeg 12bits\JMoreCfg.PAS',
  JPEGLib in '..\Componentes\Jpeg 12bits\JPEGLib.pas',
  JPEGPlus in '..\Componentes\Jpeg 12bits\JPEGPlus.pas',
  jquant1 in '..\Componentes\Jpeg 12bits\jquant1.pas',
  JQuant2 in '..\Componentes\Jpeg 12bits\JQuant2.pas',
  JUtils in '..\Componentes\Jpeg 12bits\JUtils.pas',
  ImgTypes in '..\Comunes\ImgTypes.pas',
  ImgProcs in '..\Comunes\ImgProcs.pas',
  adminImg in '..\Comunes\adminImg.pas',
  FrmInicioConsultaConvenio in '..\Comunes\FrmInicioConsultaConvenio.pas',
  BuscaClientes in '..\Comunes\BuscaClientes.pas' {FormBuscaClientes},
  FrmChecklist in '..\Comunes\FrmChecklist.pas' {FormChecklist},
  Filtros in '..\Componentes\Imagenes\Filtros.pas',
  FreHistoriaOrdenServicio in '..\Comunes\FreHistoriaOrdenServicio.pas' {FrameHistoriaOrdenServicio: TFrame},
  FrmNumeroReclamoAsignado in '..\Comunes\FrmNumeroReclamoAsignado.pas' {FNumeroReclamoAsignado},
  FrmUltimosConsumos in '..\FAC - Facturacion\FrmUltimosConsumos.pas' {NavWindowUltimosConsumos},
  UtilFacturacion in '..\Comunes\UtilFacturacion.pas',
  ComprobantesEmitidos in '..\FAC - Facturacion\ComprobantesEmitidos.pas' {NavWindowComprobantesEmitidos},
  ReporteFactura in '..\Comunes\ReporteFactura.pas' {frmReporteFactura},
  frmReporteFacturacionDetallada in '..\FAC - Facturacion\frmReporteFacturacionDetallada.pas' {FormReporteFacturacionDetallada},
  frmSeleccFactDetallada in '..\FAC - Facturacion\frmSeleccFactDetallada.pas' {fSeleccFactDetallada},
  FrmModificacionConvenio in '..\CRM - Gestion Comercial\FrmModificacionConvenio.pas',
  FreTelefono in '..\Comunes\FreTelefono.pas' {FrameTelefono: TFrame},
  FrmCheckListConvenio in '..\CRM - Gestion Comercial\FrmCheckListConvenio.pas' {FormCheckListConvenio},
  FrmObservacionesGeneral in '..\Comunes\FrmObservacionesGeneral.pas' {FormObservacionesGeneral},
  Convenios in '..\CRM - Gestion Comercial\Convenios.pas',
  CacProcs in '..\Comunes\CacProcs.pas',
  DMComunicacion in '..\Comunes\DMComunicacion.pas' {DMComunicaciones: TDataModule},
  DMOperacionesComunicacion in '..\Comunes\DMOperacionesComunicacion.pas' {DMOperacionesComunicaciones: TDataModule},
  frmImprimirConvenio in '..\CRM - Gestion Comercial\frmImprimirConvenio.pas' {FormImprimirConvenio},
  frmDatoPesona in '..\Comunes\frmDatoPesona.pas' {FreDatoPresona: TFrame},
  frmDocPresentada in '..\CRM - Gestion Comercial\frmDocPresentada.pas' {formDocPresentada},
  FechaVencimientoCheckList in '..\CRM - Gestion Comercial\FechaVencimientoCheckList.pas' {FrmFechaVencimientoCheckList},
  frmEliminarTag in '..\CRM - Gestion Comercial\frmEliminarTag.pas' {FormEliminarTag},
  frmDocPresentadaModificacionConvenio in '..\CRM - Gestion Comercial\frmDocPresentadaModificacionConvenio.pas' {formDocPresentadaModificacionConvenio},
  FreDomicilio in '..\Comunes\FreDomicilio.pas' {FrameDomicilio: TFrame},
  ComboBuscarCalle in '..\Componentes\ComboBuscarCalle\ComboBuscarCalle.pas',
  formReacuperacionTag in '..\CRM - Gestion Comercial\formReacuperacionTag.pas' {frmReacuperacionTag},
  FrmEditarDomicilio in '..\Comunes\FrmEditarDomicilio.pas' {formEditarDomicilio},
  FrmSeleccionarReimpresionPlantillas in '..\CRM - Gestion Comercial\FrmSeleccionarReimpresionPlantillas.pas' {FrmSeleccionarReimpresion},
  UtilMails in '..\Comunes\UtilMails.pas',
  frmMediosPagosConvenio in '..\Comunes\frmMediosPagosConvenio.pas' {FormMediosPagosConvenio},
  frmFirmarConvenio in '..\CRM - Gestion Comercial\frmFirmarConvenio.pas' {formFirmarConvenio},
  frmMedioEnvioDocumentosCobro in '..\CRM - Gestion Comercial\frmMedioEnvioDocumentosCobro.pas' {formMedioEnvioDocumentosCobro},
  FrmRepresentantes in '..\CRM - Gestion Comercial\FrmRepresentantes.pas' {FormRepresentantes},
  formEliminarCuenta in '..\CRM - Gestion Comercial\formEliminarCuenta.pas' {frmEliminarCuenta},
  formSuspensionCuenta in '..\CRM - Gestion Comercial\formSuspensionCuenta.pas' {frmSuspensionCuenta},
  FormConfirmarBajaModifConvenio in '..\CRM - Gestion Comercial\FormConfirmarBajaModifConvenio.pas' {frmConfirmarBajaModifConvenio},
  frmPerdidaTelevia in '..\CRM - Gestion Comercial\frmPerdidaTelevia.pas' {frm_PerdidaTelevia},
  frmCambiarTag in '..\CRM - Gestion Comercial\frmCambiarTag.pas' {formCambiarTag},
  FormActivarCuenta in '..\CRM - Gestion Comercial\FormActivarCuenta.pas',
  FrameSCDatosVehiculo in '..\CRM - Gestion Comercial\FrameSCDatosVehiculo.pas' {FmeSCDatosVehiculo: TFrame},
  FrmSCDatosVehiculo in '..\CRM - Gestion Comercial\FrmSCDatosVehiculo.pas' {FormSCDatosVehiculo},
  frmSeleccionSolicitudDuplicada in '..\CRM - Gestion Comercial\frmSeleccionSolicitudDuplicada.pas' {formSeleccionSolicitudDuplicada},
  frmRegistrarLlamada in '..\CRM - Gestion Comercial\frmRegistrarLlamada.pas' {FormRegistrarLlamada},
  FrmReclamoTransito in '..\Comunes\FrmReclamoTransito.pas' {FormReclamoTransito},
  ImprimirWO in '..\Comunes\ImprimirWO.pas' {frmImprimirWO},
  RVMTypes in '..\Comunes\RVMTypes.pas',
  RVMBind in '..\Comunes\RVMBind.pas',
  RVMClient in '..\Comunes\RVMClient.pas',
  FrmReclamoContactarCliente in '..\Comunes\FrmReclamoContactarCliente.pas' {FormReclamoContactarCliente},
  frmVisorDocumentos in '..\Comunes\frmVisorDocumentos.pas' {formVisorDocumentos},
  ImagePlus in '..\Componentes\Imagenes\ImagePlus.pas',
  FrmReclamoFaltaVehiculo in '..\Comunes\FrmReclamoFaltaVehiculo.pas' {FormReclamoFaltaVehiculo},
  SeleccReimpresionComprobante in '..\FAC - Facturacion\SeleccReimpresionComprobante.pas' {frmSeleccReimpresion},
  FrmBonificacionFacturacionDeCuenta in '..\CRM - Gestion Comercial\FrmBonificacionFacturacionDeCuenta.pas' {FormBonificacionFacturacionDeCuenta},
  FrmReclamoTransitoDesconoceInfraccion in '..\Comunes\FrmReclamoTransitoDesconoceInfraccion.pas' {FormReclamoTransitoDesconoceInfraccion},
  FrmNoFacturarTransitosDeCuenta in '..\Comunes\FrmNoFacturarTransitosDeCuenta.pas' {FormNoFacturarTransitosDeCuenta},
  FrmTransitosReclamados in '..\Comunes\FrmTransitosReclamados.pas' {FormTransitosReclamados},
  FreFuenteReclamoOrdenServicio in '..\Comunes\FreFuenteReclamoOrdenServicio.pas' {FrameFuenteReclamoOrdenServicio: TFrame},
  FreNumeroOrdenServicio in '..\Comunes\FreNumeroOrdenServicio.pas' {FrameNumeroOrdenServicio: TFrame},
  FrmIncluirTransitosInfractoresAlReclamo in '..\Comunes\FrmIncluirTransitosInfractoresAlReclamo.pas' {FormIncluirTransitosInfractoresAlReclamo},
  FreRespuestaOrdenServicio in '..\Comunes\FreRespuestaOrdenServicio.pas' {FrameRespuestaOrdenServicio: TFrame},
  FrmTransitosReclamadosAcciones in '..\Comunes\FrmTransitosReclamadosAcciones.pas' {FormTransitosReclamadosAcciones},
  FrmAnulacionRecibos in '..\Comunes\FrmAnulacionRecibos.pas' {frm_AnulacionRecibos},
  PagoVentanilla in '..\Comunes\PagoVentanilla.pas' {frmPagoVentanilla},
  FrmMsgNormalizar in '..\CRM - Gestion Comercial\FrmMsgNormalizar.pas' {FormMsgNormalizar},
  CSVUtils in '..\Comunes\CSVUtils.pas',
  dmMensaje in '..\Comunes\dmMensaje.pas' {dmMensajes: TDataModule},
  MensajesEmail in '..\Comunes\MensajesEmail.pas',
  ComponerEMail in '..\Comunes\ComponerEMail.pas' {frmComponerMail},
  LoginValuesEditor in '..\Comunes\LoginValuesEditor.pas' {frmLoginValuesEditor},
  LoginSetup in '..\Comunes\LoginSetup.pas' {frmLoginSetup},
  CustomAssistance in '..\Comunes\CustomAssistance.pas' {frmLocalAssistance},
  DMConnectionInformes in '..\Comunes\DMConnectionInformes.pas' {DMConnInformes: TDataModule},
  FrmRptReclamo in '..\Comunes\FrmRptReclamo.pas' {FRptReclamo},
  ReporteCK in '..\FAC - Facturacion\ReporteCK.pas' {frmReporteCK},
  FrmHistoricoListTag in '..\Comunes\FrmHistoricoListTag.pas' {frmHistoricoListTagForm},
  frmCambiarEstadoTag in '..\CRM - Gestion Comercial\frmCambiarEstadoTag.pas' {FormCambiarEstadoTag},
  FrmAgregarCheque in '..\Comunes\FrmAgregarCheque.pas' {FrmAgregarCheque},
  ReporteNC in '..\Comunes\ReporteNC.pas' {frmReporteNC},
  XMLCAC in '..\Comunes\XMLCAC.pas',
  ConvenioToXML in '..\Comunes\ConvenioToXML.pas',
  ModeloEntrada in '..\Comunes\ModeloEntrada.pas',
  ReporteRecibo in '..\Comunes\ReporteRecibo.pas' {formRecibo},
  frmSeleccionarMovimientosTelevia in '..\CRM - Gestion Comercial\frmSeleccionarMovimientosTelevia.pas' {frmSeleccionarMovimientosTeleviaFORM},
  FormConfirmarImpresion in '..\Comunes\FormConfirmarImpresion.pas' {frmConfirmarImpresion},
  ModificarFechaAlta in '..\Comunes\ModificarFechaAlta.pas' {frmModificarFechaAlta},
  frmRptDetInfraccionesAnuladas in '..\Comunes\frmRptDetInfraccionesAnuladas.pas' {RptDetInfraccionesAnuladas},
  frmReporteNotaDebitoElectronica in '..\Comunes\frmReporteNotaDebitoElectronica.pas' {ReporteNotaDebitoElectronicaForm},
  DTEControlDLL in '..\FAC - Facturacion\DBNet\DTEControlDLL.pas',
  frmReporteNotaCreditoElectronica in '..\Comunes\frmReporteNotaCreditoElectronica.pas' {ReporteNotaCreditoElectronicaForm},
  frmReporteBoletaFacturaElectronica in '..\Comunes\frmReporteBoletaFacturaElectronica.pas' {ReporteBoletaFacturaElectronicaForm},
  EncriptaRijandel in '..\Comunes\EncriptaRijandel.pas',
  AES in '..\Comunes\AES.pas',
  base64 in '..\Comunes\base64.pas',
  FreCategInformes in 'FreCategInformes.pas' {FrameCategInformes: TFrame},
  FreSubTipoReclamoOrdenServicio in '..\Comunes\FreSubTipoReclamoOrdenServicio.pas' {FrameSubTipoReclamoOrdenServicio: TFrame},
  FreConcesionariaReclamoOrdenServicio in '..\Comunes\FreConcesionariaReclamoOrdenServicio.pas' {FrameConcesionariaReclamoOrdenServicio: TFrame},
  SysUtilsCN in '..\Comunes\SysUtilsCN.pas',
  frmBloqueosSistema in '..\Comunes\frmBloqueosSistema.pas' {BloqueosSistemaForm},
  frmMuestraMensaje in '..\Comunes\frmMuestraMensaje.pas' {PanelMensajesForm},
  PeaProcsCN in '..\Comunes\PeaProcsCN.pas',
  MsgBoxCN in '..\Comunes\MsgBoxCN.pas' {frmMsgBoxCN},
  FrmReclamoEstacionamiento in '..\Comunes\FrmReclamoEstacionamiento.pas' {FormReclamoEstacionamiento},
  FrmEstacionamientosReclamadosAcciones in '..\Comunes\FrmEstacionamientosReclamadosAcciones.pas' {FormEstacionamientosReclamadosAcciones},
  CobranzasRoutines in '..\Comunes\CobranzasRoutines.pas',
  CobranzasResources in '..\Comunes\CobranzasResources.pas',
  CobranzasClasses in '..\Comunes\CobranzasClasses.pas',
  FrmEstacionamientosReclamados in '..\Comunes\FrmEstacionamientosReclamados.pas' {FormEstacionamientosReclamados},
  frmFacturacionManual in '..\Comunes\frmFacturacionManual.pas' {FacturacionManualForm},
  Aviso in '..\Comunes\Avisos\Aviso.pas',
  AvisoTagVencidos in '..\Comunes\Avisos\AvisoTagVencidos.pas',
  FactoryINotificacion in '..\Comunes\Avisos\FactoryINotificacion.pas',
  frmVentanaAviso in '..\Comunes\Avisos\frmVentanaAviso.pas' {VentanaAvisoForm},
  Notificacion in '..\Comunes\Avisos\Notificacion.pas',
  NotificacionImpresionDoc in '..\Comunes\Avisos\NotificacionImpresionDoc.pas',
  NotificacionImpresionReporte in '..\Comunes\Avisos\NotificacionImpresionReporte.pas' {FormNotificacionImpresionReporte},
  TipoFormaAtencion in '..\Comunes\Avisos\TipoFormaAtencion.pas',
  ClaveValor in '..\Comunes\ClaveValor.pas',
  Diccionario in '..\Comunes\Diccionario.pas',
  frmMuestraPDF in '..\Comunes\frmMuestraPDF.pas' {MuestraPDFForm},
  SysUtils,
  frmDMGuardarConvenios in '..\Comunes\frmDMGuardarConvenios.pas' {DMGuardarConvenios: TDataModule},
  frmReporteCompDevDinero in '..\Comunes\frmReporteCompDevDinero.pas' {ReporteCompDevDineroForm},
  frmAcercaDe in '..\Comunes\frmAcercaDe.pas' {AcercaDeForm},
  frmReporteContratoAdhesionPA in '..\Comunes\frmReporteContratoAdhesionPA.pas' {ReporteContratoAdhesionPAForm},
  FrmHabilitarCuentasListasAcceso in '..\CRM - Gestion Comercial\FrmHabilitarCuentasListasAcceso.pas' {FormHabilitarCuentasListasAcceso},
  frmConsultaAltaPArauco in '..\CRM - Gestion Comercial\frmConsultaAltaPArauco.pas' {FormConsultaAltaPArauco},
  frmMedioEnvioMail in '..\Comunes\frmMedioEnvioMail.pas' {FormMedioEnvioMail},
  FrmHabilitarCuentasVentanaEnrolamiento in '..\CRM - Gestion Comercial\FrmHabilitarCuentasVentanaEnrolamiento.pas' {FormHabilitarCuentasVentanaEnrolamiento},
  frmMensajeACliente in '..\Comunes\frmMensajeACliente.pas' {FormMensajeACliente},
  Crypto in '..\Comunes\Crypto.pas',
  frmConsultaAltaOtraConsecionaria in '..\CRM - Gestion Comercial\frmConsultaAltaOtraConsecionaria.pas' {FormConsultaAltaOtraConsecionaria},
  frmConvenioCuentasRUT in '..\CRM - Gestion Comercial\frmConvenioCuentasRUT.pas' {formConvenioCuentasRUT},
  frmCodigoConvenioFacturacion in '..\CRM - Gestion Comercial\frmCodigoConvenioFacturacion.pas' {formCodigoConvenioFacturacion},
  frmResultadoSuscripcionCuentas in '..\CRM - Gestion Comercial\frmResultadoSuscripcionCuentas.pas' {frmResultadoSuscripcionCuentas},
  frmImagenesSPC in '..\CRM - Gestion Comercial\frmImagenesSPC.pas' {ImagenesSPCForm},
  ApplicationGlobalVariables in '..\ComunesNew\ApplicationGlobalVariables.pas',
  ApplicationUpdateRequest in '..\ComunesNew\ApplicationUpdateRequest.pas',
  WritingToLog in '..\ComunesNew\WritingToLog.pas',
  frmAltaForzada in '..\CRM - Gestion Comercial\frmAltaForzada.pas' {FormAltaForzada},
  frmSeleccionarGrupoFacturacion in '..\CRM - Gestion Comercial\frmSeleccionarGrupoFacturacion.pas' {frmSeleccionarGrupoFacturacionForm},
  Categoria in '..\Comunes\Clases\Categoria.pas',
  ClaseBase in '..\Comunes\Clases\ClaseBase.pas',
  FrameDatosVehiculos in '..\Comunes\Clases\FrameDatosVehiculos.pas',
  frmAdministrarDescuentos in '..\Comunes\frmAdministrarDescuentos.pas' {FormAdministrarDescuentos},
  frmPatentesConvenio in '..\Comunes\frmPatentesConvenio.pas' {FormPatentesConvenio};

//TASK_106_JMA_20160206

{$R *.res}


begin
    MoveToMyOwnDir;
    Application.Initialize;
    Application.Title := 'Gesti�n de Informes';

    if not ActualizadorUpdateNeeded then begin
        if not MidasUpdateNeeded then begin
            if not ApplicationINIUPdateNeeded then begin
                if not ApplicationUpdateNeeded then begin

                    GetOtherFilesCheckUpdate;

                    if not AlreadyOpen then begin

                        Screen.Cursor := crAppStart;
                        Application.ProcessMessages;
                        FormStartup := TFormStartup.Create(nil);

                        if FormStartup.Inicializar('Gesti�n de Informes') then begin

                            FormStartup.Show;
                            FormStartup.Update;
                            Application.ProcessMessages;
                            Sleep(1500);
                        end;

                        Application.CreateForm(TDMConnections, DMConnections);
  Application.CreateForm(TDMConnInformes, DMConnInformes);
  Application.CreateForm(TMainForm, MainForm);
  Mainform.Show;

                        if not MainForm.Inicializar then begin

                            Application.Terminate;
                        end;

                        Screen.Cursor := crDefault;
                        Application.ProcessMessages;

                        ShortDateFormat := 'dd/mm/yyyy';
                        LongDateFormat  := 'dd/mm/yyyy';
                        Application.Run;
                    end
                    else Application.Terminate;
                end
                else begin
                    ApplicationUpdateExecute;
                    Application.Terminate;
                end;
            end
            else begin
                ApplicationINIUpdateExecute;
                Application.Terminate;
            end;
        end
        else begin
            MidasUpdateExecute;
            Application.Terminate;
        end;
    end
    else begin
        ActualizadorUpdateExecute;
        Application.Terminate;
    end;
end.
