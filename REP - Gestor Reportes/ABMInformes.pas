{-----------------------------------------------------------------------------
 Unit Name: ABMInformes
 Author:
 Date:
 Description:
 History:
    Revision 1:
        Author : lcanteros
        Date : 15/07/2008
        Description :  Modificacion SS (718) se agrega un time out configurable
        desde un parametro general para la consulta del reporte

Revision : 2
    Date: 26/02/2009
    Author: pdominguez
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura

Revision : 3
	Date:
	Author:
	Description: SS 744

Etiqueta    : TASK_054_MGO_20160728
Descripci�n : Se agrega tiempo estimado
-----------------------------------------------------------------------------}
unit ABMInformes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, DB, DBTables, ExtCtrls, DbList, Util, Menus,
  UtilProc, UtilDb, Abm_obj, OleCtrls,  Mask, ADODB, PeaTypes,
  ComCtrls, DMConnection, FrmRptInformeUsuario, Variants, PeaProcs,
  DPSControls, CheckLst, VariantComboBox, Grids, DBGrids, DBCtrls,
  ListBoxEx, DBListEx, DmiCtrls, DBClient, UtilRB, ppDB, ppDBPipe,
  ppParameter, ppBands, ppCtrls, ppPrnabl, ppClass, ppCache, ppComm,
  ppRelatv, ppProd, ppReport, ppEndUsr, ppTypes, ppModule, raCodMod,
  constParametrosGenerales, UtilReportes, FrmRptInformeUsuarioConfig, ppVar,
  daDataModule, DMConnectionInformes, FreCategInformes,EncriptaRijandel, ImgList, ToolWin;


resourcestring
    MSG_ERROR_GENERANDO_REPORTE = 'Error generando reporte, se cancela el proceso';
    TXT_ATENCION                = 'Atenci�n';
    MSG_BLOQMAY_CONTRASENIA ='Si tiene Activada la tecla Bloq May�s es posible que escriba incorrectamente su contrase�a.' +	// Rev. 3 (SS 744)
    Char(32)+CHAR(13)+ 'Presione la tecla Bloq May�s para desactivarla antes de escribir su contrase�a';	// Rev. 3 (SS 744)

type

  PConsVar = ^TConsVar;
  TConsVar = record
	VarName: String[40];
	VarDescri: String[60];
	VarType: String[1];
	VarQuery: String;
  end;

  PConsCol = ^TConsCol;
  TConsCol = record
	ColNombre: String[60];
	ColAncho: Byte;
	ColEstilo: String[2];
	ColAlineacion: String[1];
  end;

  TFuncionesSistemas = record
    CodigoSistema: integer;
    FuncionSistema: string;
  end;

  TFormABMInformes = class(TForm)
	Panel2: TPanel;
    PageControl: TPageControl;
	tab_General: TTabSheet;
	Label1: TLabel;
    txt_descripcion: TEdit;
	tab_Variables: TTabSheet;
	tab_Consulta: TTabSheet;
    Notebook: TNotebook;
	txt_Detalle: TMemo;
	Label5: TLabel;
	tab_Graficos: TTabSheet;
	txt_consulta: TMemo;
	btn_Bajar: TBitBtn;
    btn_Subir: TBitBtn;
    VarPanel: TPanel;
    Label2: TLabel;
	Label3: TLabel;
    Label4: TLabel;
	txt_consultavar: TMemo;
	cb_tipovar: TComboBox;
	txt_descrivar: TEdit;
	tab_Sistemas: TTabSheet;
    ckFunciones: TVariantCheckListBox;
    btn_SinGrafico: TSpeedButton;
    btn_GraficoBarras: TSpeedButton;
	btn_GraficoTorta: TSpeedButton;
    btn_GraficoVectorial: TSpeedButton;
    Label6: TLabel;
	Panel3: TPanel;
	Label8: TLabel;
	txt_CampoDatos: TEdit;
	Label9: TLabel;
    txt_CampoValores: TEdit;
    dsConsultas: TDataSource;
    ReporteDisenio: TppReport;
    DSDiseniar: TDataSource;
    DBPipelineDiseniar: TppDBPipeline;
    RBInterface1: TRBInterface;
    Diseniador: TppDesigner;
    cdsParametrosReporte: TClientDataSet;
    DBListEx1: TDBListEx;
    dsParametrosReporte: TDataSource;
    ppHeaderBand1: TppHeaderBand;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    Label10: TLabel;
    cbSistemas: TComboBox;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    btn_Disenio: TButton;
    Label7: TLabel;
    Label11: TLabel;
    txtServidor: THistoryEdit;
    txtBasedeDatos: THistoryEdit;
    lblCategoria: TLabel;
    FrameCategInformes: TFrameCategInformes;
    Tab_Usuario: TTabSheet;
    txt_usuario: TEdit;
    txtcontrasenia: TEdit;
    lblUsuario: TLabel;
    lblContrasenia: TLabel;
    qryReporte: TADOQuery;
    cdsConsultas: TClientDataSet;
    pnlSuperior: TPanel;
    Botonera: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    btnAgregar: TToolButton;
    btnEliminar: TToolButton;
    btnEditar: TToolButton;
    ToolButton6: TToolButton;
    btnImprimir: TToolButton;
    Imagenes: TImageList;
    ImageList1: TImageList;
    dbgrdConsultas: TDBGrid;
    tmrInicializarBusqueda: TTimer;
    lblTiempoEstimado: TLabel;          // TASK_054_MGO_20160728
    txtTiempoEstimado: TNumericEdit;    // TASK_054_MGO_20160728
    lblsegundos: TLabel;                // TASK_054_MGO_20160728
	procedure FormShow(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
	procedure BtnCancelarClick(Sender: TObject);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure BtnSalirClick(Sender: TObject);
	procedure txt_consultaChange(Sender: TObject);
	procedure AbmToolbar1Print(Sender: TObject);
	procedure ControlChange(Sender: TObject);
	procedure btn_SubirClick(Sender: TObject);
	procedure btn_BajarClick(Sender: TObject);
	procedure btn_GraficoClick(Sender: TObject);
    procedure btn_DisenioClick(Sender: TObject);
    procedure dsParametrosReporteDataChange(Sender: TObject; Field: TField);
    procedure DiseniadorTabChange(Sender: TObject; NewTab: String; var AllowChange: Boolean);
    procedure cbSistemasChange(Sender: TObject);
    procedure ckFuncionesClickCheck(Sender: TObject);
    procedure tab_VariablesEnter(Sender: TObject);
    procedure tab_ConsultaExit(Sender: TObject);
  
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
	procedure btnAgregarClick(Sender: TObject);
    procedure btnEliminarClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
    procedure dbgrdConsultasDblClick(Sender: TObject);
    procedure dbgrdConsultasCellClick(Column: TColumn);
    procedure tmrInicializarBusquedaTimer(Sender: TObject);
    procedure dbgrdConsultasKeyPress(Sender: TObject; var Key: Char);
    procedure CargarDatosConsulta;
    procedure cdsConsultasAfterScroll(DataSet: TDataSet);
    procedure CargarConsultas;
  private
	{ Private declarations }
    FCargando: Boolean;
    FRootReportes: AnsiString;
	FMenuInformes: TMenu;
    FParams: TParamsReporte;
    FTimeOutConsultas: integer;
    FuncionesSistemas: array of TFuncionesSistemas;
    Accion            : Integer; // 1=agregar   2=eliminar  3=editar
    FContenidoBuscar:   string;
    FCargandoDatos: Boolean;
	function  FindVar(VarName: AnsiString): Boolean;
	procedure ParseVars;
	procedure CargarVars(CodigoConsulta: Integer);
    procedure CargarArrayFunciones;

	procedure Limpiar_Campos;
    procedure CargarFuncionesSistemas(ckFunciones: TVariantCheckListBox; CodigoSistema: integer);
  public
	{ Public declarations }
	Function Inicializar(MenuInformes: TMenu): boolean;
  end;

var
  FormABMInformes  : TFormABMInformes;


implementation

uses RStrings;

{$R *.DFM}

{******************************** Function Header ******************************
Function Name: Inicializar
Author :
Date Created : 07/09/2006
Description :
Parameters : MenuInformes: TMenu
Return Value : boolean
Revision :
Author : vpaszkowicz
    Date : 07/09/2006
    Description : Reemplazo el combo por un �rbol.
Author : lcanteros
    Date : 15/07/2008
    Description : se agrego la asignaci�n de time out de la consulta desde el parametro general
    (TIME_OUT_CONSULTAS_INFORMES)
*******************************************************************************}
function TFormABMInformes.Inicializar(MenuInformes: TMenu): boolean;
var
    S: TSize;
    i: Integer;

begin
    FCargando := false;
    Result := False;
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
	Notebook.PageIndex := 0;
	PageControl.ActivePageIndex := 0;
	FMenuInformes := MenuInformes;

    ObtenerTiempoConsultaReporte(DMConnections.BaseCAC, FTimeOutConsultas);
    qryReporte.CommandTimeout := FTimeOutConsultas;

    ObtenerParametroGeneral(DMConnections.BaseCAC, ROOT_TEMPLATE_REPORTES, FRootReportes);
    if (FRootReportes = Null) or not DirectoryExists(GoodDir(FRootReportes)) then begin
        MsgBox('No se encuentra el directorio de las plantillas.', self.Caption, MB_ICONERROR);
        Exit;
    end;

    CargarConsultas;

    cdsParametrosReporte.CreateDataSet;
    cdsParametrosReporte.Open;
    CargarSistemas(DMConnections.BaseBO_Master, cbSistemas, 1, True);
    CargarFuncionesSistemas(ckFunciones, 1);
    CargarArrayFunciones;
    FrameCategInformes.Inicializar(DMConnections.BaseCAC);

    Result := True;
    for i := 1 to 255 do FParams[i] := null;
    tab_Graficos.TabVisible := False;
    FContenidoBuscar := EmptyStr;
end;

procedure TFormABMInformes.CargarConsultas;
var
    spREP_Consultas_SELECT: TADOStoredProc;
    I: Integer;
begin
    FCargandoDatos := True;

    spREP_Consultas_SELECT := TADOStoredProc.Create(Self);
    try
        spREP_Consultas_SELECT.Connection := DMConnections.BaseCAC;
        spREP_Consultas_SELECT.CommandTimeout := 60;
        spREP_Consultas_SELECT.ProcedureName := 'REP_Consultas_SELECT';
        spREP_Consultas_SELECT.Open;

        cdsConsultas.Active := False;
        for I := 0 to cdsConsultas.Fields.Count - 1 do
            cdsConsultas.Fields[i].ReadOnly := False;

        cdsConsultas.CreateDataSet;
        cdsConsultas.Active   := True;
        cdsConsultas.ReadOnly := False;

        while not spREP_Consultas_SELECT.Eof do begin
            cdsConsultas.Append;
            cdsConsultas.FieldByName('CodigoConsulta').Value := spREP_Consultas_SELECT.FieldByName('CodigoConsulta').Value; 
            cdsConsultas.FieldByName('Descripcion').Value := spREP_Consultas_SELECT.FieldByName('Descripcion').Value;   
            cdsConsultas.FieldByName('TiempoEstimado').Value := spREP_Consultas_SELECT.FieldByName('TiempoEstimado').Value;             // TASK_054_MGO_20160728
            cdsConsultas.FieldByName('Detalle').Value := spREP_Consultas_SELECT.FieldByName('Detalle').Value;
            cdsConsultas.FieldByName('TipoGrafico').Value := spREP_Consultas_SELECT.FieldByName('TipoGrafico').Value;
            cdsConsultas.FieldByName('CampoDatosGrafico').Value := spREP_Consultas_SELECT.FieldByName('CampoDatosGrafico').Value;
            cdsConsultas.FieldByName('CampoValoresGrafico').Value := spREP_Consultas_SELECT.FieldByName('CampoValoresGrafico').Value;
            cdsConsultas.FieldByName('ErrorSintaxis').Value := spREP_Consultas_SELECT.FieldByName('ErrorSintaxis').Value;
            cdsConsultas.FieldByName('Servidor').Value := spREP_Consultas_SELECT.FieldByName('Servidor').Value;
            cdsConsultas.FieldByName('BasedeDatos').Value := spREP_Consultas_SELECT.FieldByName('BasedeDatos').Value;
            cdsConsultas.FieldByName('CodigoCategoria').Value := spREP_Consultas_SELECT.FieldByName('CodigoCategoria').Value;
            cdsConsultas.FieldByName('UsuarioBaseDatos').Value := spREP_Consultas_SELECT.FieldByName('UsuarioBaseDatos').Value;
            cdsConsultas.FieldByName('Password').Value := spREP_Consultas_SELECT.FieldByName('Password').Value;
            cdsConsultas.Post;

            spREP_Consultas_SELECT.Next;
        end;
        cdsConsultas.First;
    finally
        spREP_Consultas_SELECT.Free;
        FCargandoDatos := False;
    end;
end;

procedure TFormABMInformes.Limpiar_Campos;
begin
	txt_Descripcion.Clear;
    txtTiempoEstimado.Clear;
    txt_Detalle.Clear;
	txt_Consulta.Clear;
    txt_descrivar.Clear;
    cb_tipovar.ItemIndex := -1;
    txt_consultavar.Clear;
    SetLength(FuncionesSistemas,0);
    txtServidor.Text    := '';
    txtBasedeDatos.Text := '';
    txt_usuario.Clear;
    txtcontrasenia.Clear;

	cdsParametrosReporte.EmptyDataSet;
end;

procedure TFormABMInformes.FormShow(Sender: TObject);
begin
//	DBList1.Reload;
end;

{******************************** Function Header ******************************
Function Name: BtnAceptarClick
Author :
Date Created : 07/09/2006
Description :
Parameters : Sender: TObject
Return Value : None
Revision :
    Author : vpaszkowicz
    Date : 07/09/2006
    Description : Reemplazo el combo de Categor�as por un �rbol.
Revision :
    Date: 26/02/2009
    Author: pdominguez
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura
*******************************************************************************}

procedure TFormABMInformes.BtnAceptarClick(Sender: TObject);
resourcestring
	MSG_ACTUALIZAR_INFORME = 'No se pudieron actualizar los datos del informe.';
    MSG_FALTA_SERVIDOR     = 'Falta especificar el nombre del servidor';
    MSG_FALTA_BASEDEDATOS  = 'Falta especificar el nombre de la base de datos';
	MSG_FALTA_CONSULTA     = 'Falta ingresar la Consulta del Reporte ' ;
    MSG_FALTA_USUARIO      = 'Falta especificar el Usuario de configuracion del Reporte';
    MSG_FALTA_CONTRASENIA  = 'Falta especificar la Contrase�a de Configuracion del Reporte ' ;
    MSG_ERROR_EN_CONSULTA  = 'Se produjo un Error al Validar la Consulta.';
    MSG_ERROR_EN_CONSULTA_CAPTION = 'Validar Consulta';
Var
	i, CodigoConsulta: Integer;
    TodoOk: boolean;
    Usuario, Password, Parametros, Funciones: string;
    spREP_Consultas: TADOStoredProc;
begin

    if Trim(txt_descripcion.Text) = '' then begin
        PageControl.ActivePage := tab_General;
        MsgBoxBalloon('Se debe especificar el nombre de la consulta', self.Caption, MB_ICONSTOP, txt_descripcion);
        Exit;
    end;

    if  FrameCategInformes.RetornarNodoActual = 0 then begin
        //MsgBoxBalloon('Se debe especificar la categor�a de la consulta en el �rbol de categor�as o ha seleccionado un Nodo Ra�z', self.Caption, MB_ICONSTOP, txt_descripcion);	//20160629_MGO
        MsgBoxBalloon('Se debe especificar la categor�a de la consulta en el �rbol de categor�as o ha seleccionado un Nodo Ra�z', self.Caption, MB_ICONSTOP, lblCategoria);			//20160629_MGO
        Exit;
    end;

    // 1- Verifico si eligi� el servidor y la base de datos, desde ahora, obligatorios
    if Trim(txtServidor.Text) = '' then begin
        PageControl.ActivePage := tab_Consulta;
        MsgBoxBalloon(MSG_FALTA_SERVIDOR, Self.Caption, MB_ICONSTOP, txtServidor );
        Exit;
    end;

    if Trim(txtBasedeDatos.Text) = '' then begin
        PageControl.ActivePage := tab_Consulta;
        MsgBoxBalloon(MSG_FALTA_BASEDEDATOS, Self.Caption, MB_ICONSTOP, txtBasedeDatos);
        Exit;
    end;

    if Length((txt_consulta.Text)) = 0 then begin
        PageControl.ActivePage := tab_Consulta;
       MsgBoxBalloon(MSG_FALTA_CONSULTA, Self.Caption, MB_ICONSTOP, txt_consulta);
       Exit;
    end;

    if ( trim(txt_usuario.Text ) = '') then begin
        PageControl.ActivePage := Tab_Usuario;
        MsgBoxBalloon(MSG_FALTA_USUARIO, Self.Caption, MB_ICONSTOP, txt_usuario);
        Exit;
    end;


    if (Trim(txtcontrasenia.Text) = '')  then begin
        PageControl.ActivePage := Tab_Usuario;
        MsgBoxBalloon(MSG_FALTA_CONTRASENIA, Self.Caption, MB_ICONSTOP, txtcontrasenia);
        Exit;
    end;


    //Verifico las consultas de los parametros
    cdsParametrosReporte.First;
    while not cdsParametrosReporte.eof do begin
        if Trim(cdsParametrosReporte.fieldByName('Descripcion').AsString) = ''  then begin
            PageControl.ActivePage := tab_Variables;
            MsgBoxBalloon('Se debe especificar la descripci�n del par�metro', 'Error en la consulta', MB_ICONSTOP, txt_descrivar);
            Exit;
        end;

        if Trim(cdsParametrosReporte.fieldByName('Tipo').AsString)[1] = 'L' then begin
            if trim(cdsParametrosReporte.fieldByName('ConsultaLista').AsString) = '' then begin
                PageControl.ActivePage := tab_Variables;
                MsgBoxBalloon('Se debe especificar una consulta', 'Error en la consulta', MB_ICONSTOP, txt_consultavar);
                Exit;
            end;

            qryReporte.Close;
            qryReporte.SQL.Clear;
            qryReporte.SQL.Add(cdsParametrosReporte.fieldByName('ConsultaLista').AsString);
            try
                qryReporte.Open
            except
                on e: exception do begin
                    PageControl.ActivePage := tab_Variables;
                    MsgBoxErr(MSG_ERROR_EN_CONSULTA, e.Message, MSG_ERROR_EN_CONSULTA_CAPTION, MB_ICONERROR);
                    Exit;
                end;
            end;
            if (qryReporte.FieldCount < 0) or (qryReporte.FieldCount > 2) then begin
                PageControl.ActivePage := tab_Variables;
                MsgBoxBalloon('La consulta debe tener como m�ximo dos campos en el Select', 'Error en la consulta', MB_ICONSTOP, txt_consultavar);
                Exit;
            end;
        end;
        cdsParametrosReporte.Next;
    end;

    //Verifico la Consulta
    for i:=0 to MAX_CANT_PARAMETROS_REPORTE do
        FParams[i] := Null;

    qryReporte.Close;
    qryReporte.SQL.Clear;
    qryReporte.SQL.Add(Txt_Consulta.Lines.Text);

    ArmarConsultaSQLParametrosBlanco(FParams, cdsParametrosReporte, QryReporte);
    Usuario := iif((Trim(txt_Usuario.Text) = ''), CONST_USUARIO_INFORMES_STANDARD, Trim(txt_Usuario.Text));
    Password := iif((Trim(txtcontrasenia.Text) = ''), '', Trim(txtContrasenia.Text));
    if DMConnInformes.ConectarServer(TrimRight(txtServidor.Text), TrimRight(txtBasedeDatos.Text), Usuario, Password) then qryReporte.Connection := DMConnInformes.BaseInformes
    else begin
        MsgBox(MSG_ERROR_GENERANDO_REPORTE, TXT_ATENCION, MB_ICONINFORMATION);
        Exit;
    end;
    try
        try					
            qryReporte.Open
        except
            on e: exception do begin
                PageControl.ActivePage := tab_Consulta;
                MsgBoxErr(MSG_ERROR_EN_CONSULTA, e.Message, MSG_ERROR_EN_CONSULTA_CAPTION, MB_ICONERROR);
                Exit;
            end;
        end;
    finally					
        qryReporte.Close;	
	end;

    Screen.Cursor := crHourGlass;
    TodoOk := False;

    cdsParametrosReporte.disableControls;
    cdsParametrosReporte.first;
    while not cdsParametrosReporte.eof do begin
        if Parametros <> EmptyStr then
            Parametros := Parametros + '-|-';

        {INICIO:	20160823 CFU
        Parametros := Parametros +
                      cdsParametrosReporte.fieldByName('CodigoVariable').AsString + '-;-' +
                      IntToStr(cdsParametrosReporte.fieldByName('Orden').AsInteger) + '-;-' +
                      cdsParametrosReporte.fieldByName('Descripcion').AsString + '-;-' +
                      cdsParametrosReporte.fieldByName('Tipo').AsString + '-;-' +
                      StringReplace(cdsParametrosReporte.fieldByName('ConsultaLista').AsString, '''', '''''', [rfReplaceAll]);
        }
        Parametros := Parametros +
                      cdsParametrosReporte.fieldByName('CodigoVariable').AsString + '-;-' +
                      IntToStr(cdsParametrosReporte.fieldByName('Orden').AsInteger) + '-;-' +
                      cdsParametrosReporte.fieldByName('Descripcion').AsString + '-;-' +
                      cdsParametrosReporte.fieldByName('Tipo').AsString + '-;-' +
                      cdsParametrosReporte.fieldByName('ConsultaLista').AsString;
        //TERMINO:	20160823 CFU
    
        cdsParametrosReporte.Next;
    end;
    cdsParametrosReporte.enableControls;

    for i := 0 to Length(FuncionesSistemas) - 1 do begin
        if Funciones <> EmptyStr then
            Funciones := Funciones + '-|-';

        Funciones := Funciones +
                     IntToStr(FuncionesSistemas[i].CodigoSistema) + '-;-' +
                     Trim(FuncionesSistemas[i].FuncionSistema);
    end;

    spREP_Consultas := TADOStoredProc.Create(Self);
    try
        try

            spREP_Consultas.CommandTimeout := 60;
            spREP_Consultas.Connection := DMConnections.BaseCAC;

            if Accion = 1 then begin
                spREP_Consultas.ProcedureName := 'REP_Consultas_INSERT';
                spREP_Consultas.Parameters.Refresh;
                spREP_Consultas.Parameters.ParamByName('@CodigoConsulta').Value := Null;
            end else begin
                spREP_Consultas.ProcedureName := 'REP_Consultas_UPDATE';
                spREP_Consultas.Parameters.Refresh;
                spREP_Consultas.Parameters.ParamByName('@CodigoConsulta').Value := cdsConsultas.FieldByName('CodigoConsulta').AsInteger;
                CodigoConsulta := cdsConsultas.FieldByName('CodigoConsulta').AsInteger;
            end;

            spREP_Consultas.Parameters.ParamByName('@Descripcion').Value := txt_Descripcion.text;
            spREP_Consultas.Parameters.ParamByName('@TiempoEstimado').Value := txtTiempoEstimado.ValueInt;            // TASK_054_MGO_20160728
            spREP_Consultas.Parameters.ParamByName('@CodigoCategoria').Value := FrameCategInformes.RetornarNodoActual;
            spREP_Consultas.Parameters.ParamByName('@Detalle').Value := iif((txt_Detalle.text = ''), NULL, txt_Detalle.text);
            spREP_Consultas.Parameters.ParamByName('@CampoDatosGrafico').Value := IIf((txt_CampoDatos.text = ''), NULL, txt_CampoDatos.text);
            spREP_Consultas.Parameters.ParamByName('@CampoValoresGrafico').Value := IIf((txt_CampoValores.text = ''), NULL, txt_CampoValores.text);
            spREP_Consultas.Parameters.ParamByName('@ErrorSintaxis').Value := NULL;
            spREP_Consultas.Parameters.ParamByName('@Servidor').Value := TrimRight(txtServidor.Text);
            spREP_Consultas.Parameters.ParamByName('@BasedeDatos').Value := TrimRight(txtBaseDeDatos.Text);
            spREP_Consultas.Parameters.ParamByName('@UsuarioBaseDatos').Value := IIf((txt_usuario.text = ''), NULL, TrimRight(txt_usuario.Text));
            spREP_Consultas.Parameters.ParamByName('@Password').Value := IIf((txtcontrasenia.text = ''), NULL,Cifrar(Trim(txtcontrasenia.text),CONTRASENIA_REPORTE));
            spREP_Consultas.Parameters.ParamByName('@Parametros').Value := Parametros;
            spREP_Consultas.Parameters.ParamByName('@TextoSQL').Value := Trim(txt_Consulta.Text);
            spREP_Consultas.Parameters.ParamByName('@Funciones').Value := Funciones;

            if (btn_GraficoBarras.Down = True) then
                spREP_Consultas.Parameters.ParamByName('@TipoGrafico').Value := GRAFICO_BARRAS
            else if (btn_GraficoTorta.Down = True) then
                spREP_Consultas.Parameters.ParamByName('@TipoGrafico').Value := GRAFICO_TORTA
            else if (btn_GraficoVectorial.Down = True) then
                spREP_Consultas.Parameters.ParamByName('@TipoGrafico').Value := GRAFICO_VECTORIAL
            else
                spREP_Consultas.Parameters.ParamByName('@TipoGrafico').Value := NULL;

            spREP_Consultas.ExecProc;

            if spREP_Consultas.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
                raise Exception.Create(spREP_Consultas.Parameters.ParamByName('@ErrorDescription').Value);

            if Accion = 1 then
                CodigoConsulta := spREP_Consultas.Parameters.ParamByName('@CodigoConsulta').Value;

        except
            On E: exception do begin
            	Screen.Cursor := crDefault;
				MsgBoxErr(MSG_ACTUALIZAR_INFORME, E.message, Self.Caption, MB_ICONSTOP);
                Exit;
			end;
        end;
    finally
        spREP_Consultas.Free;
    end;

    ReporteDisenio.Template.FileName := GoodDir(FRootReportes) + intToStr(CodigoConsulta) + '.rtm';
    ReporteDisenio.Template.SaveToFile;

    Accion := 0;
    dbgrdConsultas.Enabled := True;
	Tab_General.Enabled := False;
	Tab_Consulta.Enabled := False;
	Tab_Variables.Enabled := False;
	Tab_Sistemas.Enabled := False;
    Tab_Usuario.Enabled  := False;
	Notebook.PageIndex := 0;
    CargarConsultas;
    cdsConsultas.Locate('CodigoConsulta', CodigoConsulta, []);
	Screen.Cursor := crDefault;
end;

procedure TFormABMInformes.BtnCancelarClick(Sender: TObject);
begin
    Limpiar_campos;
	Accion := 0;
    dbgrdConsultas.Enabled := True;
    dbgrdConsultas.SetFocus;
	Tab_General.Enabled := False;
	Tab_Consulta.Enabled := False;
	Tab_Variables.Enabled := False;
	Tab_Sistemas.Enabled := False;
	Notebook.PageIndex := 0;

    CargarDatosConsulta;
end;

procedure TFormABMInformes.btnEditarClick(Sender: TObject);
var
    i: Integer;
begin
    Accion := 3;
    dbgrdConsultas.Enabled := False;

    if FileExists(GoodDir(FRootReportes) + IntToStr(cdsConsultas.fieldByName('CodigoConsulta').asInteger) + '.rtm') then begin
        ReporteDisenio.Template.FileName := GoodDir(FRootReportes) + IntToStr(cdsConsultas.fieldByName('CodigoConsulta').asInteger) + '.rtm';
        ReporteDisenio.Template.loadFromFile;
    end
    else
        if MsgBox('No se encuentra una plantilla para este informe. �Desea redise�ar el informe desde la plantilla est�ndar?', self.Caption, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
            if FileExists(GoodDir(FRootReportes) + 'Template Tipo' + '.rtm') then begin
                ReporteDisenio.Template.FileName := GoodDir(FRootReportes) + 'Template Tipo' + '.rtm';
                ReporteDisenio.Template.loadFromFile;
            end
            else begin
                if MsgBox('No se encontr� la plantilla est�ndar. �Desea continuar?', self.Caption, MB_YESNO + MB_ICONQUESTION) <> IDYES then begin
                    Accion := 0;
                    dbgrdConsultas.Enabled := True;
                    Exit;
                end
                else begin
                    ReporteDisenio.Template.New;
                end;
            end;
        end else ReporteDisenio.Template.New;

    for i:=0 to MAX_CANT_PARAMETROS_REPORTE do FParams[i] := Null;

	Tab_General.Enabled := True;
	Tab_Consulta.Enabled := True;
	Tab_Variables.Enabled := True;
	Tab_Sistemas.Enabled := True;
    Tab_Usuario.Enabled := true;
	Notebook.PageIndex := 1;
 end;

procedure TFormABMInformes.btnEliminarClick(Sender: TObject);
resourcestring
	MSG_ELIMINAR_CONSULTA     = '�Est� seguro de querer eliminar esta consulta?';
    MSG_ERROR_ELIMINAR        = 'No se pudo eliminar la consulta.';
	CAPTION_ELIMINAR_CONSULTA = 'Eliminar Consulta Personalizada';
var
	CodigoConsulta: LongInt;
    spREP_Consultas_DELETE: TADOStoredProc;
begin
    Accion := 2;
    dbgrdConsultas.Enabled := False;
    
	Screen.Cursor := crHourGlass;
	If MsgBox(MSG_ELIMINAR_CONSULTA, CAPTION_ELIMINAR_CONSULTA, MB_YESNO + MB_ICONQUESTION) = IDYES then begin

        spREP_Consultas_DELETE := TADOStoredProc.Create(Self);
        try
            try
            spREP_Consultas_DELETE.Connection := DMConnections.BaseCAC;
            spREP_Consultas_DELETE.CommandTimeout := 60;
            spREP_Consultas_DELETE.ProcedureName := 'REP_Consultas_DELETE';
            spREP_Consultas_DELETE.Parameters.Refresh;
            spREP_Consultas_DELETE.Parameters.ParamByName('@Codigoconsulta').Value := cdsConsultas.FieldByName('CodigoConsulta').AsInteger;
            spREP_Consultas_DELETE.ExecProc;

            if spREP_Consultas_DELETE.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
                raise Exception.Create(spREP_Consultas_DELETE.Parameters.ParamByName('@ErrorDescription').Value);

            except
                On E: Exception do begin
                    MsgBoxErr(MSG_ERROR_ELIMINAR, e.message, CAPTION_ELIMINAR_CONSULTA, MB_ICONSTOP);
                end;
            end;
        finally
            spREP_Consultas_DELETE.Free;
        end;

        CargarConsultas;
	end;
	Accion := 0;
    dbgrdConsultas.Enabled := True;

	Tab_General.Enabled := False;
	Tab_Consulta.Enabled := False;
	Tab_Variables.Enabled := False;
    Tab_Sistemas.Enabled := False;
	Notebook.PageIndex := 0;
	Screen.Cursor := crDefault;
 end;

procedure TFormABMInformes.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Action := caFree;
    DMConnInformes.BaseInformes.Close;
end;

procedure TFormABMInformes.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if GetKeyState(VK_CAPITAL) and $01 <> 0 then
      begin
          MsgBoxBalloon(MSG_BLOQMAY_CONTRASENIA, Self.Caption, MB_ICONSTOP, txtcontrasenia);
          key :=0;
          Exit;
     end;

end;

procedure TFormABMInformes.BtnSalirClick(Sender: TObject);
begin
	Close;
end;

procedure TFormABMInformes.txt_ConsultaChange(Sender: TObject);
begin
(*
	if tab_Consulta.Enabled then ParseVars;
    if (cdsParametrosReporte.RecordCount > 0) then cdsParametrosReporte.First;
*)
end;



function TFormABMInformes.FindVar(VarName: AnsiString): Boolean;
begin
	VarName := UpperCase(VarName);
    cdsParametrosReporte.disableControls;
    cdsParametrosReporte.first;
    Result := cdsParametrosReporte.FieldByName('CodigoVariable').asString = VarName;
	While (not cdsParametrosReporte.eof) and (not result) do begin
        cdsParametrosReporte.next;
		Result := cdsParametrosReporte.FieldByName('CodigoVariable').asString = VarName;
	end;
    cdsParametrosReporte.enableControls;
end;



procedure TFormABMInformes.ParseVars;
Var
	i, orden: Integer;
	VarList: TStringList;
	SQL, VarName: AnsiString;
begin
 	SQL := txt_Consulta.Text;
	SQL := StringReplace(SQL, CRLF, ' '  ,[rfReplaceAll]);
	SQL := StringReplace(SQL, '(' , ' ( ', [rfReplaceAll]);
	SQL := StringReplace(SQL, ')' , ' ) ', [rfReplaceAll]);
	SQL := StringReplace(SQL, ',' , ' , ', [rfReplaceAll]);
	SQL := StringReplace(SQL, '*' , ' * ', [rfReplaceAll]);
	SQL := StringReplace(SQL, '+' , ' + ', [rfReplaceAll]);
	SQL := StringReplace(SQL, '-' , ' - ', [rfReplaceAll]);
	SQL := StringReplace(SQL, '/' , ' / ', [rfReplaceAll]);
	SQL := StringReplace(SQL, '%' , ' % ', [rfReplaceAll]);
	VarList := TStringList.Create;
	// Parseamos el texto para encontrar la lista de variables
	for i := 1 to 10000 do begin
		VarName := ParseParamByNumber(SQL, i, ' ');
		if VarName = '' then Break;
		if (Copy(VarName, 1, 1) = ':') and (Length(VarName) > 2) then begin
			VarName := UpperCase(Copy(VarName, 2, 255));
			if VarList.IndexOf(VarName) = -1 then begin
                VarList.Add(VarName);
            end;
		end;
	end;

//    cdsParametrosReporte.EmptyDataSet;

	// Agregamos las variables Nuevas
    FCargando := True;
    Orden := cdsParametrosReporte.recordCount;
	for i := 0 to VarList.Count - 1 do begin
		if not FindVar(VarList[i]) then begin
          	cdsParametrosReporte.AppendRecord([UpperCase(VarList[i]),
			    '', Orden, 'S', '']);
            inc(orden);
        end;
	end;

    //Elimino aquellas que no van mas
    cdsParametrosReporte.First;
    while not cdsParametrosReporte.Eof do begin
	    for i := 0 to VarList.Count - 1 do
            if (VarList[i] = Trim(cdsParametrosReporte.FieldByName('CodigoVariable').AsString)) then break;
        if (i > VarList.Count - 1) then cdsParametrosReporte.Delete;
        cdsParametrosReporte.Next;
    end;

    FCargando := False;
	txt_DescriVar.Enabled := cdsParametrosReporte.RecordCount <> 0;
	cb_TipoVar.Enabled := cdsParametrosReporte.RecordCount <> 0;
	txt_ConsultaVar.Enabled := cdsParametrosReporte.RecordCount <> 0;
	btn_Subir.Enabled := cdsParametrosReporte.RecordCount <> 0;
	btn_Bajar.Enabled := cdsParametrosReporte.RecordCount <> 0;
	VarPanel.Visible := cdsParametrosReporte.RecordCount <> 0;
	VarList.Free;
end;


procedure TFormABMInformes.CargarVars(CodigoConsulta: Integer);
var
    spVariablesConsultas: TADOStoredProc;
begin
	cdsParametrosReporte.EmptyDataSet;
    FCargando := true;
    spVariablesConsultas := TADOStoredProc.Create(nil);
    try
        spVariablesConsultas.Connection := DMConnections.BaseCAC;
        spVariablesConsultas.ProcedureName := 'ObtenerVariablesConsulta';
        spVariablesConsultas.Parameters.Refresh;
        spVariablesConsultas.Parameters.ParamByName('@CodigoConsulta').value := CodigoConsulta;
        spVariablesConsultas.Open;
        While not spVariablesConsultas.Eof do begin
                cdsParametrosReporte.AppendRecord([
                  trim(spVariablesConsultas.FieldByName('CodigoVariable').asString),
                  Trim(spVariablesConsultas.FieldByName('Descripcion').AsString),
                  spVariablesConsultas.FieldByName('Orden').AsInteger,
                  Trim(spVariablesConsultas.FieldByName('Tipo').AsString),
                  Trim(spVariablesConsultas.FieldByName('ConsultaLista').AsString)]);
                spVariablesConsultas.Next;
        end;
        spVariablesConsultas.Close;
    finally
        spVariablesConsultas.Free;
    end;
    FCargando := false;
    cdsParametrosReporte.first;
	btn_Subir.Enabled := cdsParametrosReporte.RecordCount <> 0;
	btn_Bajar.Enabled := cdsParametrosReporte.RecordCount <> 0;
	VarPanel.Visible := cdsParametrosReporte.RecordCount <> 0;

end;

procedure TFormABMInformes.AbmToolbar1Print(Sender: TObject);
Var
	TipoGrafico: AnsiString;
	f: TFormRptInformeUsuario;
begin
	if (btn_GraficoBarras.Down = True) then TipoGrafico := GRAFICO_BARRAS
	else if (btn_GraficoTorta.Down = True) then TipoGrafico := GRAFICO_TORTA
	else if (btn_GraficoVectorial.Down = True) then TipoGrafico := GRAFICO_VECTORIAL
	else TipoGrafico := SIN_GRAFICO;
	Application.CreateForm(TFormRptInformeUsuario, f);
	if f.Inicializar(cdsConsultas.FieldByName('CodigoConsulta').AsInteger,
	   txt_Descripcion.Text, txt_Consulta.Text, cdsParametrosReporte.RecordCount, TipoGrafico,
	   txt_CampoDatos.Text, txt_CampoValores.Text) then f.rbiReporte.Execute;
	f.Release;
end;

procedure TFormABMInformes.ControlChange(Sender: TObject);
Var
	VarType: Char;
begin
    FCargando := true;
	if (Sender = txt_DescriVar) then begin
        cdsParametrosReporte.Edit;
        cdsParametrosReporte.FieldByName('Descripcion').AsString := trim(txt_DescriVar.Text);
        cdsParametrosReporte.post;
	end else if (Sender = cb_tipovar) then begin
		case cb_tipovar.ItemIndex of
			1: VarType := 'D';
			2: VarType := 'H';
			3: VarType := 'L';
			4: VarType := 'N';
			else VarType := 'S';
		end;
        cdsParametrosReporte.Edit;
        cdsParametrosReporte.FieldByName('tipo').AsString := VarType;
        cdsParametrosReporte.post;
	end else begin
        cdsParametrosReporte.Edit;
        cdsParametrosReporte.FieldByName('ConsultaLista').AsString := trim(txt_ConsultaVar.Text);
        cdsParametrosReporte.post;
    end;
    FCargando := False;
end;

procedure TFormABMInformes.dbgrdConsultasCellClick(Column: TColumn);
begin
    CargarDatosConsulta;
end;

procedure TFormABMInformes.CargarDatosConsulta;
begin
    if FCargandoDatos then Exit;

    with cdsConsultas do begin
		txt_Descripcion.Text := Trim(FieldByName('Descripcion').AsString);
        txtTiempoEstimado.Value := FieldByName('TiempoEstimado').AsInteger;    // TASK_054_MGO_20160728
        FrameCategInformes.PararseEnNodo(FieldByName('CodigoCategoria').AsInteger);
		txt_Detalle.Text := Trim(FieldByName('Detalle').AsString);
		txt_Consulta.Text := QueryGetValue(DMConnections.BaseCAC, Format('select dbo.ObtenerTextoSQL(%d)', [FieldByName('CodigoConsulta').AsInteger]));
		CargarVars(FieldByName('CodigoConsulta').AsInteger);

		if (FieldByName('TipoGrafico').AsString = GRAFICO_BARRAS) then btn_GraficoBarras.Down := True
        else if (FieldByName('TipoGrafico').AsString = GRAFICO_TORTA) then btn_GraficoTorta.Down := True
        else if (FieldByName('TipoGrafico').AsString = GRAFICO_VECTORIAL) then btn_GraficoVectorial.Down := True
		else btn_SinGrafico.Down := True;
		txt_CampoDatos.Text := Trim(FieldByName('CampoDatosGrafico').AsString);
		txt_CampoValores.Text := Trim(FieldByName('CampoValoresGrafico').AsString);
        btn_GraficoClick(nil);
        CargarArrayFunciones;
        // cargo datos del servidor y base de datos del reporte
        txtServidor.Text    := TrimRight(FieldByName('Servidor').AsString);
        txtBasedeDatos.Text := TrimRight(FieldByName('BasedeDatos').AsString);
		txt_usuario.Text    := IIf((FieldByName('UsuarioBaseDatos').AsString = NULL), '',trimRight(FieldByName('UsuarioBaseDatos').AsString));

        if (FieldByName('password').Value = NULL) then
        txtcontrasenia.Text := ''
        else
        txtcontrasenia.Text := Descifrar(trim(FieldByName('Password').asstring),CONTRASENIA_REPORTE);
		FrameCategInformes.PararseEnNodo(FieldByName('CodigoCategoria').AsInteger);
	end;
end;

procedure TFormABMInformes.dbgrdConsultasDblClick(Sender: TObject);
begin
    btnEditar.Click;
end;

procedure TFormABMInformes.dbgrdConsultasKeyPress(Sender: TObject;
  var Key: Char);
begin
    tmrInicializarBusqueda.Enabled := False;
    FContenidoBuscar := FContenidoBuscar + Key;
    if cdsConsultas.Locate('Descripcion', FContenidoBuscar, [loPartialKey, loCaseInsensitive]) then begin
        tmrInicializarBusqueda.Enabled := True;
    end
    else
        FContenidoBuscar := EmptyStr;
end;

procedure TFormABMInformes.btn_SubirClick(Sender: TObject);
var
	i: integer;
    CV1, CV2: AnsiString;
begin
    FCargando := True;
    cdsParametrosReporte.disableControls;
    if not cdsParametrosReporte.Bof then begin
        // Alamcenamos el valor actual
        i := cdsParametrosReporte.fieldByName('Orden').asInteger;
		CV1 := cdsParametrosReporte.fieldByName('CodigoVariable').asString;
        cdsParametrosReporte.prior;
        // Asignamos un valor cualquiera al orden
        CV2 := cdsParametrosReporte.fieldByName('CodigoVariable').asString;
        cdsParametrosReporte.edit;
        cdsParametrosReporte.fieldByName('Orden').asInteger := -1;
        cdsParametrosReporte.post;
        // Buscamos la Variable 1 y le asignamos el valor de i - 1
        FindVar(CV1);
        cdsParametrosReporte.edit;
        cdsParametrosReporte.fieldByName('Orden').asInteger := i - 1;
        cdsParametrosReporte.post;
        // Buscamos la Variable 2 y le asignamos el valor de i
        cdsParametrosReporte.first;
        cdsParametrosReporte.edit;
        cdsParametrosReporte.fieldByName('Orden').asInteger := i;
        cdsParametrosReporte.post;
        FindVar(CV1);
	end;
    cdsParametrosReporte.EnableControls;
    FCargando := False;
end;

procedure TFormABMInformes.btn_BajarClick(Sender: TObject);
var
	i: integer;
    CV1, CV2: AnsiString;
begin
    FCargando := True;
    cdsParametrosReporte.disableControls;
    if (cdsParametrosReporte.RecNo < cdsParametrosReporte.RecordCount) then begin
        // Alamcenamos el valor actual
        i := cdsParametrosReporte.fieldByName('Orden').asInteger;
		CV1 := cdsParametrosReporte.fieldByName('CodigoVariable').asString;
        cdsParametrosReporte.next;
        // Asignamos un valor cualquiera al orden
        CV2 := cdsParametrosReporte.fieldByName('CodigoVariable').asString;
        cdsParametrosReporte.edit;
        cdsParametrosReporte.fieldByName('Orden').asInteger := -1;
        cdsParametrosReporte.post;
        // Buscamos la Variable 1 y le asignamos el valor de i - 1
        FindVar(CV1);
        cdsParametrosReporte.edit;
        cdsParametrosReporte.fieldByName('Orden').asInteger := i + 1;
        cdsParametrosReporte.post;
        // Buscamos la Variable 2 y le asignamos el valor de i
        cdsParametrosReporte.first;
        cdsParametrosReporte.edit;
        cdsParametrosReporte.fieldByName('Orden').asInteger := i;
        cdsParametrosReporte.post;
        FindVar(CV1);
	end;
    cdsParametrosReporte.EnableControls;
    FCargando := False;
end;

procedure TFormABMInformes.btn_GraficoClick(Sender: TObject);
begin
	Panel3.Visible := (btn_SinGrafico.Down = True);
end;


procedure TFormABMInformes.btn_DisenioClick(Sender: TObject);
resourcestring
    MSG_FALTA_SERVIDOR    = 'No seleccion� el servidor de la base de datos';
    MSG_FALTA_BASEDEDATOS = 'No seleccion� la base de datos del servidor';
    MSG_FALTA_CONSULTA     = 'Falta ingresar la Consulta del Reporte ' ;
    MSG_FALTA_USUARIO      = 'Falta especificar el Usuario de configuracion del Reporte';
    MSG_FALTA_CONTRASENIA  = 'Falta especificar la Contrase�a de Configuracion del Reporte ' ;
var
    Usuario, Password: string;
begin
    // 1- Verifico si eligi� el servidor y la base de datos, desde ahora, obligatorios
    if Trim(txtServidor.Text) = '' then begin
        PageControl.ActivePage := tab_Consulta;
        MsgBoxBalloon(MSG_FALTA_SERVIDOR, Self.Caption, MB_ICONSTOP, txtServidor );
        Exit;
    end;

    if Trim(txtBasedeDatos.Text) = '' then begin
        PageControl.ActivePage := tab_Consulta;
        MsgBoxBalloon(MSG_FALTA_BASEDEDATOS, Self.Caption, MB_ICONSTOP, txtBasedeDatos);
        Exit;
    end;
    // INICIO : 20160629_MGO
    if Length((txt_consulta.Text)) = 0 then begin
        PageControl.ActivePage := tab_Consulta;
       MsgBoxBalloon(MSG_FALTA_CONSULTA, Self.Caption, MB_ICONSTOP, txt_consulta);
       Exit;
    end;

    if ( trim(txt_usuario.Text ) = '') then begin
        PageControl.ActivePage := Tab_Usuario;
        MsgBoxBalloon(MSG_FALTA_USUARIO, Self.Caption, MB_ICONSTOP, txt_usuario);
        Exit;
    end;

    if (Trim(txtcontrasenia.Text) = '')  then begin
        PageControl.ActivePage := Tab_Usuario;
        MsgBoxBalloon(MSG_FALTA_CONTRASENIA, Self.Caption, MB_ICONSTOP, txtcontrasenia);
        Exit;
    end;
    qryReporte.Close;
    qryReporte.SQL.Clear;
    qryReporte.SQL.Add(Txt_Consulta.Lines.Text);

    Usuario := iif((Trim(txt_Usuario.Text) = ''), CONST_USUARIO_INFORMES_STANDARD, Trim(txt_Usuario.Text));
    Password := iif((Trim(txtcontrasenia.Text) = ''), '', Trim(txtContrasenia.Text));

    ArmarConsultaSQLParametrosBlanco(FParams, cdsParametrosReporte, QryReporte);
    if DMConnInformes.ConectarServer(TrimRight(txtServidor.Text), TrimRight(txtBasedeDatos.Text), Usuario, Password) then
        qryReporte.Connection := DMConnInformes.BaseInformes
    else begin
        MsgBox(MSG_ERROR_GENERANDO_REPORTE, TXT_ATENCION, MB_ICONINFORMATION);
        Exit;
    end;

    try
        qryReporte.Open
	except
		on e: exception do begin
            PageControl.ActivePage := tab_Consulta;
            MsgBoxBalloon(e.Message, 'Error en la consulta', MB_ICONSTOP, txt_consulta);
			Exit;

		end;
	end;
    Diseniador.ShowModal;
end;


procedure TFormABMInformes.dsParametrosReporteDataChange(Sender: TObject;
  Field: TField);
begin
	// Mostramos los datos de la variable seleccionada
    if FCargando or (not cdsParametrosReporte.Active) or (cdsParametrosReporte.recordcount = 0) then exit;

	txt_DescriVar.Text := trim(cdsParametrosReporte.fieldbyName('Descripcion').asString);
	case cdsParametrosReporte.fieldbyName('Tipo').asString[1] of
		'D':
			begin
				cb_tipovar.ItemIndex := 1;
				Label4.Visible := False;
				txt_consultavar.Visible := False;
				txt_consultavar.Text := '';
			end;
		'H':
			begin
				cb_tipovar.ItemIndex := 2;
				Label4.Visible := False;
				txt_consultavar.Visible := False;
				txt_consultavar.Text := '';
			end;
		'L':
			begin
				cb_tipovar.ItemIndex := 3;
				Label4.Visible := True;
				txt_consultavar.Visible := True;
				txt_consultavar.Text := cdsParametrosReporte.fieldbyName('ConsultaLista').asString;
			end;
		'N':
			begin
				cb_tipovar.ItemIndex := 4;
				Label4.Visible := False;
				txt_consultavar.Visible := False;
				txt_consultavar.Text := '';
			end;
		else
			begin
				cb_tipovar.ItemIndex := 0;
				Label4.Visible := False;
				txt_consultavar.Visible := False;
				txt_consultavar.Text := '';
			end;
	end;
end;

procedure TFormABMInformes.DiseniadorTabChange(Sender: TObject;
  NewTab: String; var AllowChange: Boolean);
var
    f: TFormRptInformeUsuarioConfig;
begin
    if NewTab = 'Preview' then begin
        qryReporte.Close;
        AllowChange := False;
        if cdsParametrosReporte.RecordCount > 0 then begin
            Application.CreateForm(TFormRptInformeUsuarioConfig, f);
    	    if f.Inicializar(cdsParametrosReporte, FParams, SIN_GRAFICO) then begin
                if f.ShowModal <> MrOk then begin
                    AllowChange := False;
                    f.Release;
                    Exit;
                end;
                f.Release;
            end;
        end;
        AllowChange := True;
        qryReporte.SQL.Clear;
        qryReporte.SQL.Add(Txt_Consulta.Lines.Text);
        ArmarConsultaSQLParametrosBlanco(FParams, cdsParametrosReporte, QryReporte);
        qryReporte.open;
    end
    else begin
        qryReporte.Close;
        qryReporte.SQL.Clear;
        qryReporte.SQL.Add(Txt_Consulta.Lines.Text);
        ArmarConsultaSQLParametrosBlanco(FParams, cdsParametrosReporte, QryReporte);
        qryReporte.open;
    end;
end;

procedure TFormABMInformes.CargarFuncionesSistemas(ckFunciones: TVariantCheckListBox; CodigoSistema: integer);
var
    spFunciones: TADOStoredProc;
begin

    spFunciones := TADOStoredProc.Create(nil);
    try
        spFunciones.Connection := DMConnections.BaseBO_Master;
        spFunciones.ProcedureName := 'ADM_FuncionesSistemas_SELECT';
        spFunciones.Parameters.Refresh;
        spFunciones.Parameters.ParamByName('@CodSistema').Value := CodigoSistema; 
        spFunciones.Parameters.ParamByName('@Level').Value := 1;
        spFunciones.Open;
        ckFunciones.Items.Clear;
        while not spFunciones.Eof do begin
            ckFunciones.Items.Add(EliminarCaracterDeCadena(spFunciones.FieldbyName('Descripcion').AsString, COMODIN_CHAR), spFunciones.FieldbyName('Funcion').AsString);
            spFunciones.Next;
        end;
    finally
        spFunciones.Close;
    end;

end;

procedure TFormABMInformes.cbSistemasChange(Sender: TObject);
var
    i, j: integer;
begin
    CargarFuncionesSistemas(ckFunciones, strtoint(Trim(StrRight(cbSistemas.Text, 50))));
    for j := 0 to ckFunciones.Count-1 do begin
        i:= 0;
        while (i < Length(FuncionesSistemas)) and not ((Trim(FuncionesSistemas[i].FuncionSistema) = Trim(ckFunciones.Items[j].Value)) and
              (FuncionesSistemas[i].CodigoSistema = strtoint(Trim(StrRight(cbSistemas.Text, 50))))) do begin
            i := i + 1;
        end;
        if not (i >= Length(FuncionesSistemas)) then
            ckFunciones.Checked[j] := True
    end;
end;

procedure TFormABMInformes.cdsConsultasAfterScroll(DataSet: TDataSet);
begin
    CargarDatosConsulta;
end;

procedure TFormABMInformes.ckFuncionesClickCheck(Sender: TObject);
var
    i, j: integer;
begin
    for j := 0 to ckFunciones.Count-1 do begin
        if ckFunciones.Checked[j] then begin
            i:= 0;
            while (i < Length(FuncionesSistemas)) and not ((Trim(FuncionesSistemas[i].FuncionSistema) = Trim(ckFunciones.Items[j].Value)) and
                  (FuncionesSistemas[i].CodigoSistema = strtoint(Trim(StrRight(cbSistemas.Text, 50)))))  do begin
                i := i + 1;
            end;
            if (i >= Length(FuncionesSistemas)) then begin
                SetLength(FuncionesSistemas, Length(FuncionesSistemas) + 1);
                FuncionesSistemas[Length(FuncionesSistemas)-1].CodigoSistema := strtoint(Trim(StrRight(cbSistemas.Text, 50)));
                FuncionesSistemas[Length(FuncionesSistemas)-1].FuncionSistema :=  Trim(ckFunciones.Items[j].Value);
            end;
        end
        else begin
            i:= 0;
            while (i < Length(FuncionesSistemas)) and not ((Trim(FuncionesSistemas[i].FuncionSistema) = Trim(ckFunciones.Items[j].Value)) and
                  (FuncionesSistemas[i].CodigoSistema = strtoint(Trim(StrRight(cbSistemas.Text, 50))))) do begin
                i := i + 1;
            end;
            if not (i >= Length(FuncionesSistemas)) then begin
                while ((i < Length(FuncionesSistemas)-1)) do begin
                    FuncionesSistemas[i].CodigoSistema := FuncionesSistemas[i+1].CodigoSistema;
                    FuncionesSistemas[i].FuncionSistema := FuncionesSistemas[i+1].FuncionSistema;
                    i := i + 1;
                end;
                SetLength(FuncionesSistemas,i);
            end;
       end;
    end;
end;

procedure TFormABMInformes.CargarArrayFunciones;
var
    spConsultasFuncionesSistemas:TADOStoredProc;
begin

    spConsultasFuncionesSistemas := TADOStoredProc.Create(nil);
    try
        spConsultasFuncionesSistemas.Connection := DMConnections.BaseCAC;
        spConsultasFuncionesSistemas.ProcedureName := 'ObtenerConsultasFuncionesSistemas';
        spConsultasFuncionesSistemas.Parameters.Refresh;
        spConsultasFuncionesSistemas.Parameters.ParamByName('@CodigoConsulta').Value := cdsConsultas.FieldByName('CodigoConsulta').AsInteger;
        spConsultasFuncionesSistemas.Open;
        if spConsultasFuncionesSistemas.RecordCount > 0 then
        begin
            SetLength(FuncionesSistemas,0);
            spConsultasFuncionesSistemas.First;
            while not spConsultasFuncionesSistemas.Eof do begin
                SetLength(FuncionesSistemas, Length(FuncionesSistemas) + 1);
                FuncionesSistemas[Length(FuncionesSistemas)-1].CodigoSistema := spConsultasFuncionesSistemas.FieldByName('CodigoSistema').AsInteger;
                FuncionesSistemas[Length(FuncionesSistemas)-1].FuncionSistema :=  Trim(spConsultasFuncionesSistemas.FieldByName('Funcion').AsString);
                spConsultasFuncionesSistemas.Next;
            end;
            cbSistemasChange(cbSistemas);
        end;
    finally
        spConsultasFuncionesSistemas.Free;
    end;
end;

procedure TFormABMInformes.tab_VariablesEnter(Sender: TObject);
begin
(*
    if cdsParametrosReporte.RecordCount = 0 then
        EnableControlsInContainer(VarPanel, False);
*)
end;

procedure TFormABMInformes.tab_ConsultaExit(Sender: TObject);
begin
    txt_consulta.Text := Trim(txt_consulta.Text);
    
	if tab_Consulta.Enabled then ParseVars;
    if (cdsParametrosReporte.RecordCount > 0) then cdsParametrosReporte.First;
end;

procedure TFormABMInformes.tmrInicializarBusquedaTimer(Sender: TObject);
begin
    tmrInicializarBusqueda.Enabled := False;
    FContenidoBuscar := EmptyStr;
end;

procedure TFormABMInformes.btnAgregarClick(Sender: TObject);
var
    i: integer;
begin
    Accion := 1;
    dbgrdConsultas.Enabled := False;
    ReporteDisenio.Template.New;

    if FileExists(GoodDir(FRootReportes) + 'Template Tipo' + '.rtm') then begin
        ReporteDisenio.Template.FileName := GoodDir(FRootReportes) + 'Template Tipo' + '.rtm';
        ReporteDisenio.Template.loadFromFile;
    end
    else
        if MsgBox('No se encontr� la plantilla est�ndar. �Desea continuar?', self.Caption, MB_YESNO + MB_ICONQUESTION) <> IDYES then begin
            Accion := 0;
            dbgrdConsultas.Enabled := True;
            Exit;
        end;

    for i:=0 to MAX_CANT_PARAMETROS_REPORTE do
        FParams[i] := Null;

	Tab_General.Enabled := True;
	Tab_Consulta.Enabled := True;
	Tab_Variables.Enabled := True;
	Tab_Sistemas.Enabled := True;
    Tab_usuario.Enabled := True;
	Limpiar_Campos;
	Notebook.PageIndex := 1;
	PageControl.ActivePage := Tab_General;
	txt_Descripcion.SetFocus;
end;

procedure TFormABMInformes.btnImprimirClick(Sender: TObject);
var
	TipoGrafico: AnsiString;
	f: TFormRptInformeUsuario;
begin
	if (btn_GraficoBarras.Down = True) then
        TipoGrafico := GRAFICO_BARRAS
	else if (btn_GraficoTorta.Down = True) then
        TipoGrafico := GRAFICO_TORTA
	else if (btn_GraficoVectorial.Down = True) then
        TipoGrafico := GRAFICO_VECTORIAL
	else
        TipoGrafico := SIN_GRAFICO;
        
	Application.CreateForm(TFormRptInformeUsuario, f);
	if f.Inicializar(cdsConsultas.FieldByName('CodigoConsulta').AsInteger,
	   txt_Descripcion.Text, txt_Consulta.Text, cdsParametrosReporte.RecordCount, TipoGrafico,
	   txt_CampoDatos.Text, txt_CampoValores.Text) then
        f.rbiReporte.Execute;
       
	f.Release;
end;

end.
