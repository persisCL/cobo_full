unit FreCategInformes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ADODB;

type

    TCategoriaReporte = class
        private
            // C�digo de la Categor�a.
            FCodigo: Integer;
        protected
            procedure SetCodigo(const Value: Integer);
        public
            constructor Create(CodigoCategoria: Integer); reintroduce;
            destructor Destroy; override;
            property Codigo: Integer read FCodigo Write SetCodigo;
    end; // class

  TFrameCategInformes = class(TFrame)
    ArbolCat: TTreeView;
    procedure ArbolCatClick(Sender: TObject);
  private
    { Private declarations }
    procedure CargarArbolCategorias(Conn: TADOConnection; Arbol: TTreeView);
    function NodoDeClave(unaClave: integer):TCategoriaReporte;
  public
    { Public declarations }
    function RetornarNodoActual: integer;
    procedure PararseEnNodo(unaClave: integer);
    procedure Inicializar(Conn: TADOConnection);
    procedure AgregarNodo(unCodigo: integer; unaDescripcion: string);
    procedure BorrarNodo(Clave: integer);
    procedure ModificarNodo(ClaveNueva, ClaveVieja: integer);
  end;

implementation

{$R *.dfm}

{ TCategoriaReporte }

{******************************** Function Header ******************************
Function Name: Create
Author : ggomez
Date Created : 07/09/2006
Description :
Parameters : CodigoCategoria: Integer
Return Value : None
Revision :
    Author : vpaszkowicz
    Date : 07/09/2006
    Description : Nodo del �rbol - Creaci�n
*******************************************************************************}

constructor TCategoriaReporte.Create(CodigoCategoria: Integer);
begin
    inherited Create;
    SetCodigo(CodigoCategoria);
end;

{******************************** Function Header ******************************
Function Name: Create
Author : ggomez
Date Created : 07/09/2006
Description :
Parameters : CodigoCategoria: Integer
Return Value : None
Revision :
    Author : vpaszkowicz
    Date : 07/09/2006
    Description : Setter del c�digo
*******************************************************************************}

procedure TCategoriaReporte.SetCodigo(const Value: Integer);
begin
	if Value = FCodigo then Exit;
    FCodigo := Value;
end;

{******************************** Function Header ******************************
Function Name: Create
Author : ggomez
Date Created : 07/09/2006
Description :
Parameters : CodigoCategoria: Integer
Return Value : None
Revision :
    Author : vpaszkowicz
    Date : 07/09/2006
    Description : Nodo del �rbol - Destructor
*******************************************************************************}

destructor TCategoriaReporte.Destroy;
begin
	inherited Destroy;
end;

{TFrameCategInformes}

{******************************** Function Header ******************************
Function Name: Inicializar
Author : vpaszkowicz
Date Created : 07/09/2006
Description : Inicializa el �rbol y lo expande
Parameters : Conn: TADOConnection
Return Value : None
*******************************************************************************}

procedure TFrameCategInformes.Inicializar(Conn: TADOConnection);
begin
    CargarArbolCategorias(Conn, ArbolCat);
    ArbolCat.FullExpand;
end;

{******************************** Function Header ******************************
Function Name: PararseEnNodo
Author : vpaszkowicz
Date Created : 07/09/2006
Description : Se posiciona en unNodo buscando por una clave
Parameters : unaClave: integer
Return Value : None
*******************************************************************************}

procedure TFrameCategInformes.PararseEnNodo(unaClave: integer);
var
    unIndice: integer;
    Encontre: Boolean;
    unNodo: TCategoriaReporte;
begin
    Encontre := False;
    unIndice := 0;
    while (unIndice <= ArbolCat.Items.Count - 1) and not Encontre do begin
        unNodo := (ArbolCat.Items[unIndice]).Data;
        if unNodo.Codigo = unaClave then Encontre := True
        else inc(unIndice);
    end;
    if Encontre then begin
        ArbolCat.Items[unIndice].Selected := True;
        ArbolCat.Items[unIndice].Focused := True;
        ArbolCat.Repaint;
    end;

end;

{******************************** Function Header ******************************
Function Name: AgregarNodo
Author : vpaszkowicz
Date Created : 07/09/2006
Description : Agrega un nodo al �rbol
Parameters : unCodigo: integer; unaDescripcion: string
Return Value : None
*******************************************************************************}

procedure TFrameCategInformes.AgregarNodo(unCodigo: integer; unaDescripcion: string);
var
    unaCategoria: TCategoriaReporte;
begin
    unaCategoria := TCategoriaReporte.Create(unCodigo);
    ArbolCat.Items.AddChildObject(ArbolCat.Selected, unaDescripcion, unaCategoria);
end;

{******************************** Function Header ******************************
Function Name: RetornarNodoActual
Author : vpaszkowicz
Date Created : 07/09/2006
Description : Retorna el nodo en el que est� actualmente posicionado.
Parameters : None
Return Value : integer
*******************************************************************************}

function TFrameCategInformes.RetornarNodoActual: integer;
var
    unaCategoria: TCategoriaReporte;
begin
    Result := 0;                                    //20160629_MGO
    if Assigned(ArbolCat.Selected) then begin       //20160629_MGO
        unaCategoria := ArbolCat.Selected.Data;
        Result := unaCategoria.Codigo;
    end;                                            //20160629_MGO
end;

{******************************** Function Header ******************************
Function Name: CargarArbolCategorias
Author : ggomez
Date Created : 07/09/2006
Description :
Parameters : Conn: TADOConnection; Arbol: TTreeView
Return Value : None
Revision : 1
    Author : vpaszkowicz
    Date : 07/09/2006
    Description : Basado en un m�todo de Guille para cargar las categir�as de
    informes desde la base de datos. Adem�s le agrego una ra�z.
*******************************************************************************}

procedure TFrameCategInformes.CargarArbolCategorias(Conn: TADOConnection; Arbol: TTreeView);
    procedure CargarSubArbolCategorias(CodigoCategoria: Integer; Arbol: TTreeView; Nodo: TTreeNode; Qry: TADOQuery);
    var
        RecNro: Integer;
        NuevoNodo: TTreeNode;
        Categoria: TCategoriaReporte;
    begin
        // Almacenar el registro en el que est� el query.
        RecNro := Qry.RecNo;

        // Recorrer las categor�as para encontrar las "Hijas" de la categor�a pasada como par�metro.
        Qry.First;
        while not Qry.Eof do begin
            // Si la categor�a a tratar es "hija" de la que se pas� como par�metro, entonces
            // agregar el menu.
            if CodigoCategoria = Qry.FieldByName('CodigoCategoriaPadre').AsInteger then begin

                Categoria := TCategoriaReporte.Create(Qry.FieldByName('CodigoCategoria').AsInteger);
                NuevoNodo := Arbol.Items.AddChildObject(Nodo, Qry.FieldByName('Descripcion').AsString, Categoria);

                // Llamada recursiva para agregar los submenues
                CargarSubArbolCategorias(Qry.FieldByName('CodigoCategoria').AsInteger, Arbol, NuevoNodo, Qry);
            end;
            Qry.Next;
        end;
        // Restaurar el registro en el que estaba el query.
        Qry.RecNo := RecNro;
    end;

var
	QryCategorias: TADOQuery;
    Categoria: TCategoriaReporte;
    NuevoNodo, NodoRaiz: TTreeNode;
begin
    //Creo todos items de categor�as de reportes
	QryCategorias := TADOQuery.Create(nil);
	try
		QryCategorias.Connection := Conn;

        QryCategorias.Close;
        QryCategorias.SQL.Text := 'SELECT CodigoCategoria, Descripcion, CodigoCategoriaPadre'
            + ' FROM CategoriasConsultas WITH (NOLOCK)'
            + ' ORDER BY Descripcion';

        QryCategorias.Open;
        QryCategorias.First;
        Categoria := TCategoriaReporte.Create(0);
        NodoRaiz := Arbol.Items.AddChildObject(Nil, 'Ra�z', Categoria);
        while not QryCategorias.Eof do begin

            // Si la Categor�a es Padre de otra, entonces agregarla como ra�z.
            if QryCategorias.FieldByName('CodigoCategoriaPadre').AsInteger = 0 then begin

                Categoria := TCategoriaReporte.Create(QryCategorias.FieldByName('CodigoCategoria').AsInteger);
                //NuevoNodo := Arbol.Items.AddChildObject(Nil, QryCategorias.FieldByName('Descripcion').AsString, Categoria);
                NuevoNodo := Arbol.Items.AddChildObject(NodoRaiz, QryCategorias.FieldByName('Descripcion').AsString, Categoria);
                // Llamada recursiva para agregar las subcategorias.
                CargarSubArbolCategorias(QryCategorias.FieldByName('CodigoCategoria').AsInteger, Arbol, NuevoNodo, QryCategorias);
            end; // if
            QryCategorias.Next;
        end;
    finally
        QryCategorias.Free;
    end;
end;

{******************************** Function Header ******************************
Function Name: NodoDeClave
Author : vpaszkowicz
Date Created : 07/09/2006
Description : Retorna la instancia nodo que coincide con una clave
Parameters : unaClave: integer
Return Value : TCategoriaReporte
*******************************************************************************}

function TFrameCategInformes.NodoDeClave(unaClave: integer):TCategoriaReporte;
begin
    PararseEnNodo(unaClave);
    Result := ArbolCat.Selected.Data;
end;

{******************************** Function Header ******************************
Function Name: ModificarNodo
Author : vpaszkowicz
Date Created : 07/09/2006
Description : modifica el nodo que encuentra con clave vieja por el valor de
clave nueva
Parameters : ClaveNueva, ClaveVieja: integer
Return Value : None
*******************************************************************************}

procedure TFrameCategInformes.ModificarNodo(ClaveNueva, ClaveVieja: integer);
var
    unaCategoria: TCategoriaReporte;
begin
    unaCategoria := NodoDeClave(ClaveVieja);
    if unaCategoria.Codigo = ClaveVieja then
        unaCategoria.Codigo := ClaveNueva
end;

{******************************** Function Header ******************************
Function Name: BorrarNodo
Author : vpaszkowicz
Date Created : 07/09/2006
Description : Borra el nodo que encuentra utilizando una clave.
Parameters : Clave: integer
Return Value : None
*******************************************************************************}

procedure TFrameCategInformes.BorrarNodo(Clave: integer);
var
    unaCategoria: TCategoriaReporte;
begin
    unaCategoria := NodoDeClave(Clave);
    if unaCategoria.Codigo = Clave then
        ArbolCat.Items.Delete(ArbolCat.Selected);
end;

procedure TFrameCategInformes.ArbolCatClick(Sender: TObject);
begin
    //nada
end;

end.
