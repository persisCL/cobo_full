unit FrmParametros;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Db, DBTables, UtilProc, UtilDB, ExtCtrls,
  CheckLst, ComCtrls, ADODB, Buttons, DPSControls, PeaProcs;

type
  TFormParametros = class(TForm)
    Parametros: TADOTable;
    PageControl: TPageControl;
    tab_Informes: TTabSheet;
    txtDirArchivosImagenes: TEdit;
    Label1: TLabel;
    btnDirArchivosImagenes: TSpeedButton;
    btnCancelar: TDPSButton;
    btnAceptar: TDPSButton;
    btnAplicar: TDPSButton;
    txtLogo: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    txtTextoLogo: TEdit;
    procedure btnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnAceptarClick(Sender: TObject);
	procedure btnAplicarClick(Sender: TObject);
	procedure btnDirArchivosImagenesClick(Sender: TObject);
	procedure ActualizarBotones(Sender: TObject);
  private
	{ Private declarations }
  public
	{ Public declarations }
	Function Inicializar: boolean;
  end;

var
  FormParametros: TFormParametros;

implementation
{$R *.DFM}

function TFormParametros.Inicializar: boolean;
begin
	CenterForm(Self);
	PageControl.TabIndex := 0;
	if not OpenTables([Parametros]) then
		Result := False
	else begin
		txtDirArchivosImagenes.Text := Trim(Parametros.FieldByName('DirImagenesVarias').AsString);
		txtLogo.Text := Trim(Parametros.FieldByName('LogoReportes').AsString);
		txtTextoLogo.Text := Trim(Parametros.FieldByName('TextoLogoReportes').AsString);        
        btnAplicar.Enabled := False;
		Result := True;
	end;
end;

procedure TFormParametros.btnCancelarClick(Sender: TObject);
begin
	Close;
end;

procedure TFormParametros.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Action := caFree;
end;

procedure TFormParametros.btnAceptarClick(Sender: TObject);
begin
	btnAplicarClick(btnAplicar);
	Close;
end;

procedure TFormParametros.btnAplicarClick(Sender: TObject);
begin
	with Parametros do begin
		if Eof then Append else Edit;
		FieldByName('Id').AsInteger := 1;
		FieldByName('DirImagenesVarias').AsString := Trim(txtDirArchivosImagenes.text);
		FieldByName('LogoReportes').AsString := Trim(txtLogo.text);
		FieldByName('TextoLogoReportes').AsString := Trim(txtTextoLogo.text);
		Post;
	end;
    btnAplicar.Enabled := False;
end;

procedure TFormParametros.btnDirArchivosImagenesClick(Sender: TObject);
var
    DirArchivosImagenes: AnsiString;
begin
    DirArchivosImagenes := Trim(BrowseForFolder(Self));
    if not (DirArchivosImagenes = '') then txtDirArchivosImagenes.Text := DirArchivosImagenes;
end;

procedure TFormParametros.ActualizarBotones(Sender: TObject);
begin
	BtnAplicar.Enabled := True;
end;

end.
