unit UtilReportes;

interface
uses
    DB, ADODB, SysUtils, variants, util;

type
    TParamsReporte = array[0..255] of Variant;



function ArmarConsultaSQL(Params: TParamsReporte; QryParametrosReporte: TDataSet; QryReporte: TADOQuery): Boolean;
function ArmarCondicionWhereMultipleFiltro (lista: ANSIString; nombreCampo: AnsiString): AnsiString;

implementation


function ArmarCondicionWhereMultipleFiltro (lista: ANSIString; nombreCampo: AnsiString): AnsiString;
var i: integer;
	s, OrConcatenados: AnsiString;
begin
	if Lista = '*' Then Begin
		s := '( (1=1) ';
		Lista := '';
	end else
		if Lista = '' then s := '( (1<>1) '
		else s := '( ';

	OrConcatenados := '';
	while Length (Lista) <> 0 do begin
		i := Pos('*', Lista);

		OrConcatenados := OrConcatenados + iif (OrConcatenados = '', '', ' OR ') + '(' + nombreCampo + ' = ' + Copy (Lista, 1, i-1) + ')';
		Delete (Lista, 1, i)
	end;

	result := s + OrConcatenados + ' )';
end;


function ArmarConsultaSQL(Params: TParamsReporte; QryParametrosReporte: TDataSet; QryReporte: TADOQuery): Boolean;
Var
	nParam: integer;
	ParamName,
	Token,
	ListaFiltro: AnsiString;

	procedure GenerarFiltro (SQL: ANSIString; Parametro: AnsiString; Lista: ANSIString; var Condicion, Filtro: ANSIString);
	var
		PosParam,
		PosInic,
		PosFin: Integer;
		Campo: ANSIString;
	begin
		PosParam := Pos (UpperCase(Parametro), UpperCase(SQL));

		// Busco hacia atr�s el =
		PosFin := PosParam-1;
		While (PosFin > 0) And (SQL[PosFin] <> '=') And (SQL[PosFin] = ' ') Do Dec (PosFin);

		If (PosFin > 0) And (SQL[PosFin] = '=') then begin
			// Busco el nombre del campo a la izquierda del par�metro

			Dec(PosFin);
			While (PosFin > 0) And (SQL[PosFin] = ' ') Do Dec (PosFin);

			PosInic := PosFin;
			While (PosInic > 0) And (SQL[PosInic] <> ' ') And (SQL[PosInic] <> '(') Do Dec (PosInic);
			Inc(PosInic);

			Condicion := Copy(SQL, PosInic, PosParam - PosInic + Length(Parametro));
		end else begin
			// Busco hacia adelante el =
			PosInic := PosParam + Length(Parametro);
			While (PosInic <= Length (SQL)) And (SQL[PosInic] <> '=') And (SQL[PosInic] = ' ') Do Inc(PosInic);

			if (PosInic <= Length(SQL)) And (SQL[PosInic] = '=') then begin
				// Busco el nombre del campo a la derecha del parametro
				Inc(PosInic);
				While (PosInic <= Length (SQL)) And (SQL[PosInic] = ' ') Do Inc (PosInic);

				PosFin := PosInic;
				While (PosFin <= Length (SQL)) And (SQL[PosFin] <> ' ') And (SQL[PosInic] <> ')') Do
                    Inc(PosFin);
				Dec(PosFin);

				Condicion := Trim(Copy(SQL, PosParam, PosFin - PosParam + 1));
			end
		end;

		Campo := Trim(Copy (SQL, PosInic, PosFin-PosInic+1));
		Filtro:= ArmarCondicionWhereMultipleFiltro(Lista, Campo);
	end;


begin
    result := true;
	qryParametrosReporte.First;
	nParam := 0;
	While not qryParametrosReporte.Eof do begin
		ParamName := Trim(qryParametrosReporte.FieldByName('CodigoVariable').AsString);
		case qryParametrosReporte.FieldByName('Tipo').AsString[1] of
			'D': begin
                    if  (Params[nParam] = NULL) then  Params[nParam] := '';//FormatDateTime('yyyy-mm-dd' , Date);
                    qryReporte.SQL.Text := StringReplace(qryReporte.SQL.Text, ':' + ParamName, '''' + Params[nParam] + '''', [rfReplaceAll, rfIgnoreCase]);
                 end;
			'H': begin
                    if  (Params[nParam] = NULL) then  Params[nParam] := '';//FormatDateTime('yyyy-mm-dd hh:nn', Now);
                    qryReporte.SQL.Text := StringReplace(qryReporte.SQL.Text, ':' + ParamName, '''' + Params[nParam] + '''', [rfReplaceAll, rfIgnoreCase]);
                 end;
			'N': begin
                    if  (Params[nParam] = NULL) then  Params[nParam] := 1;
                    qryReporte.SQL.Text := StringReplace(qryReporte.SQL.Text, ':' + ParamName, Params[nParam], [rfReplaceAll, rfIgnoreCase]);
                 end;
			'L': begin
                    if  (Params[nParam] = NULL) then  Params[nParam] := '';
    		        GenerarFiltro(qryReporte.SQL.Text, ':' + ParamName, iif(Params[nParam]= null, '',Params[nParam]) , Token, ListaFiltro);
			        qryReporte.SQL.Text := StringReplace(qryReporte.SQL.Text, Token, ListaFiltro, [rfReplaceAll, rfIgnoreCase]);
    			 end
			else begin
                    if  (Params[nParam] = NULL) then  Params[nParam] := '';
                    qryReporte.SQL.Text := StringReplace(qryReporte.SQL.Text, ':' + ParamName, '''' + Params[nParam] + '''', [rfReplaceAll, rfIgnoreCase]);
                 end;
            end;
		Inc(nParam);
		qryParametrosReporte.Next;
	end;
end;


end.
 