{-----------------------------------------------------------------------------
 File Name: 
 Author:    
 Date Created: 
 Language: ES-AR
 Description: 

Revision : 1
Date: 26/02/2009
Author: pdominguez
Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para 
	los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
unit ABMCategoriasInformes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, DB, DBTables, ExtCtrls, DbList, UtilProc,
  UtilDb, Abm_obj, OleCtrls,  DmiCtrls, Mask, ADODB, DPSControls,
  FreCategInformes;

type
  TFormCategoriasInformes = class(TForm)
	Panel2: TPanel;
    Lista: TAbmList;
	AbmToolbar1: TAbmToolbar;
    CategoriasConsultas: TADOTable;
    Label1: TLabel;
    GroupB: TPanel;
    Label15: TLabel;
    txt_Descripcion: TEdit;
    Panel1: TPanel;
    Notebook: TNotebook;
	txt_codigo: TNumericEdit;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    FrameCategInformes: TFrameCategInformes;
    procedure FrameCategInformesArbolCatClick(Sender: TObject);
	function  ListaProcess(Tabla: TDataSet; var Texto: string): Boolean;
	procedure BtSalirClick(Sender: TObject);
	procedure ListaInsert(Sender: TObject);
	procedure ListaEdit(Sender: TObject);
	procedure ListaDelete(Sender: TObject);
	procedure ListaClick(Sender: TObject);
	procedure ListaRefresh(Sender: TObject);
	procedure AbmToolbar1Close(Sender: TObject);
    procedure ListaDrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure FormShow(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
  private
	{ Private declarations }
    FNodoActual: integer;
    procedure Limpiar_Campos;
    function ProblemaEnCodigo: Boolean;
  public
	{ Public declarations }
	Function Inicializar: boolean;
  end;

var
  FormCategoriasInformes  : TFormCategoriasInformes;

implementation

uses DMConnection;

resourcestring
    MSG_ACTUALIZAR_ERROR	= 'No se puedo actualizar la Categor�a';
    MSG_ACTUALIZAR_CAPTION	= 'Actualizar Categor�a';
	MSG_DELETE_QUESTION		= '�Est� seguro de querer eliminar la Categor�a?';
    MSG_DELETE_ERROR		= 'No se puede eliminar la Categor�a porque hay datos que dependen de ella.';
    MSG_DELETE_CAPTION 		= 'Eliminar Categor�a';
    MSG_EXISTE_CODIGO       = 'El c�digo ingresado ya fue utilizado, por favor ingrese otro';
    MSG_TIENE_SUBCATEGORIAS = 'El c�digo ingresado ya contiene categor�as o informes asociados y no se puede ser modificado o eliminado';
    MSG_CAPTION_CODIGO      = 'C�digo err�neo';

{$R *.DFM}

{******************************** Function Header ******************************
Function Name: Inicializar
Author :
Date Created : 07/09/2006
Description :
Parameters : None
Return Value : boolean
Revision :
    Author : vpaszkowicz
    Date : 07/09/2006
    Description : Cargo el �rbol con los datos de la conexion actual.
*******************************************************************************}

function TFormCategoriasInformes.Inicializar: boolean;
Var
	S: TSize;
begin
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
	Notebook.PageIndex := 0;
	if not OpenTables([CategoriasConsultas]) then
    	Result := False
    else begin
    	Result := True;
       	Lista.Reload;
        //Cargo arbol con las categorias
        FrameCategInformes.Inicializar(DMConnections.BaseCAC);
        FNodoActual := 0;
        FrameCategInformes.PararseEnNodo(0);
        FrameCategInformesArbolCatClick(Self);
	end;
end;

procedure TFormCategoriasInformes.Limpiar_Campos();
begin
	txt_Codigo.Clear;
	txt_Descripcion.Clear;
end;

function TFormCategoriasInformes.ListaProcess(Tabla: TDataSet; var Texto: string): Boolean;
begin
	Texto := Tabla.FieldByName('CodigoCategoria').AsString + ' ' +
	  Tabla.FieldByName('Descripcion').AsString;
	Result := True;
end;

procedure TFormCategoriasInformes.BtSalirClick(Sender: TObject);
begin
	 Close;
end;

procedure TFormCategoriasInformes.ListaInsert(Sender: TObject);
begin
	Lista.Estado       := Alta;
	Limpiar_Campos;
	GroupB.Enabled     := True;
	Lista.Enabled      := False;
	Notebook.PageIndex := 1;
	txt_Codigo.SetFocus;
end;

procedure TFormCategoriasInformes.ListaEdit(Sender: TObject);
begin
	Lista.Estado       := Modi;
	GroupB.Enabled     := True;
	Lista.Enabled      := False;
	Notebook.PageIndex := 1;
	txt_Codigo.SetFocus;
end;
{-----------------------------------------------------------------------------
  Function Name: ListaDelete 
  Author:
  Date Created:  /  /
  Description:
  Parameters: Sender: TObject
  Return Value: N/A

  Revision : 1
    Date: 26/02/2009
    Author: pdominguez
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure TFormCategoriasInformes.ListaDelete(Sender: TObject);
resourcestring
	MSG_DELETE_CATEGORIA = 'Esta Categor�a tiene asignado el reporte: %s. Debe eliminar el Reporte antes de la Categor�a.';
Var
    ReporteDescrip: string;
    CodigoBorrar: integer;
begin
	Screen.Cursor := crHourGlass;
    try
        ReporteDescrip := UpperCase(Trim(QueryGetValue(DMConnections.BaseCAC,
          'select Descripcion from Consultas WITH (NOLOCK) where CodigoCategoria = ' +
          IntToStr(CategoriasConsultas.FieldByName('CodigoCategoria').Value), 10)));
        if ReporteDescrip <> '' then begin
            MsgBox(Format(MSG_DELETE_CATEGORIA, [ReporteDescrip]), Self.Caption, MB_OK);
            Screen.Cursor := crDefault;
            Exit;
        end;
        //Virginia - Le agrego la misma validaci�n que la modificaci�n,
        if not ProblemaEnCodigo then begin
            If MsgBox( MSG_DELETE_QUESTION, MSG_DELETE_CAPTION,  MB_YESNO or MB_ICONWARNING) = IDYES then begin
                try
                    CodigoBorrar := CategoriasConsultas.FieldByName('CodigoCategoria').asinteger;
                    CategoriasConsultas.Delete;
                    FrameCategInformes.BorrarNodo(CodigoBorrar);
                except
                    On E: EDataBaseError do begin
                        CategoriasConsultas.Cancel;
                        MsgBoxErr( MSG_DELETE_ERROR, e.message, MSG_DELETE_CAPTION, MB_ICONSTOP);
                    end;
                end;
                Lista.Reload;
            end;
        end;
    finally
        Lista.Estado       := Normal;
        Lista.Enabled      := True;
        GroupB.Enabled     := False;
        Notebook.PageIndex := 0;
        Screen.Cursor      := crDefault;
    end;
end;

procedure TFormCategoriasInformes.ListaClick(Sender: TObject);
begin
	 with CategoriasConsultas do begin
		  txt_Codigo.Value     := FieldByName('CodigoCategoria').AsInteger;
		  txt_Descripcion.text := Trim(FieldByName('Descripcion').AsString);
	 end;
end;

procedure TFormCategoriasInformes.ListaRefresh(Sender: TObject);
begin
	 if Lista.Empty then Limpiar_Campos;
end;

procedure TFormCategoriasInformes.AbmToolbar1Close(Sender: TObject);
begin
	 Close;
end;

procedure TFormCategoriasInformes.ListaDrawItem(Sender: TDBList; Tabla: TDataSet;
  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, FieldByName('CodigoCategoria').AsString);
		TextOut(Cols[1], Rect.Top, FieldByName('Descripcion').AsString);
	end;
end;

procedure TFormCategoriasInformes.FormShow(Sender: TObject);
begin
	Lista.Reload;
end;

{******************************** Function Header ******************************
Function Name: ProblemaEnCodigo
Author : vpaszkowicz
Date Created : 07/09/2006
Description : Retorna True si existe un problema con la clave de la tabla de
CategoriasConsultas, ya sea para el alta porque est� repetida, o por la baja o
modificaci�n en caso de tener Informes o categor�as asociadas.
Parameters : None
Return Value : Boolean
*******************************************************************************}

function TFormCategoriasInformes.ProblemaEnCodigo: Boolean;
    function ExisteCodigo: Boolean;
    begin
        Result := QueryGetValue(DMConnections.BaseCAC,
                Format('select dbo.ExisteCodigoCategoria(%d)',
                [txt_Codigo.ValueInt])) = 'True';
        if Result then MsgBox(MSG_EXISTE_CODIGO, MSG_CAPTION_CODIGO, 0);
    end;
begin
    Result := False;
    if Lista.Estado = Alta then begin
        //Valido que el codigo no este usado.

        Result := ExisteCodigo;
    end
    else begin
        Result := QueryGetValue(DMConnections.BaseCAC,
                Format('select dbo.ExisteInformeCategoriaAsociada(%d)',
                [CategoriasConsultas.FieldByName('CodigoCategoria').asinteger])) = 'True';
        if Result then MsgBox(MSG_TIENE_SUBCATEGORIAS, MSG_CAPTION_CODIGO, 0);
        //Si puedo modificar porque no contiene asociados, tambien tengo que validar que no haya
        //usado el c�digo nuevo
        if Lista.Estado = Modi then Result := Result or ExisteCodigo;
    end;
end;

{******************************** Function Header ******************************
Function Name: BtnAceptarClick
Author :
Date Created : 07/09/2006
Description :
Parameters : Sender: TObject
Return Value : None
Revision :
    Author : vpaszkowicz
    Date : 07/09/2006
    Description : Antes de agregar o modificar valido que no haya problemas con
    el c�digo, ya que ser�a foreing key de otras tablas.
*******************************************************************************}

procedure TFormCategoriasInformes.BtnAceptarClick(Sender: TObject);
var
    CodigoViejo: integer;
begin
	Screen.Cursor := crHourGlass;
	With CategoriasConsultas do begin
        if not ProblemaEnCodigo then begin
            Try
                {if Lista.Estado = Alta then Append else Edit;
                FieldByName('CodigoCategoria').AsFloat  := txt_Codigo.Value;
                FieldByName('Descripcion').AsString := Trim(txt_Descripcion.Text);
                FieldByName('CodigoCategoriaPadre').AsFloat := FNodoActual;
                Post;}
                if Lista.Estado = Alta then Append
                else begin
                    CodigoViejo := FieldByName('CodigoCategoria').AsInteger;
                    Edit;
                end;
                FieldByName('CodigoCategoria').AsFloat  := txt_Codigo.Value;
                FieldByName('Descripcion').AsString := Trim(txt_Descripcion.Text);
                FieldByName('CodigoCategoriaPadre').AsFloat := FNodoActual;
                Post;
                if Lista.Estado = Alta then FrameCategInformes.AgregarNodo(FieldByName('CodigoCategoria').asinteger, Trim(txt_Descripcion.Text))
                else FrameCategInformes.ModificarNodo(FieldByName('CodigoCategoria').asinteger, CodigoViejo);
            except
                On E: EDataBaseError do begin
                    Cancel;
                    MsgBoxErr( MSG_ACTUALIZAR_ERROR, E.message, MSG_ACTUALIZAR_CAPTION, MB_ICONSTOP);
                end;
            end
        end;
	end;
	Lista.Estado       := Normal;
	Lista.Enabled      := True;
	GroupB.Enabled     := False;
	Notebook.PageIndex := 0;
	Lista.Reload;
	Lista.SetFocus;
	Screen.Cursor 	   := crDefault;
end;

procedure TFormCategoriasInformes.BtnCancelarClick(Sender: TObject);
begin
	GroupB.Enabled     := False;
	Lista.Estado       := Normal;
	Lista.Enabled      := True;
	Lista.SetFocus;
	Notebook.PageIndex := 0;
end;

procedure TFormCategoriasInformes.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Action := caFree;
end;

procedure TFormCategoriasInformes.BtnSalirClick(Sender: TObject);
begin
	Close;
end;

{******************************** Function Header ******************************
Function Name: FrameCategInformesArbolCatClick
Author : vpaszkowicz
Date Created : 07/09/2006
Description : Cada vez que me posiciono en el �rbol filtro la grilla de la dere-
cha con todos los nodos que posee el nodo actual. Uso FNodoActual para tener la
variable con la posicion actual en el �rbol para grabarlo.
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}

procedure TFormCategoriasInformes.FrameCategInformesArbolCatClick(
  Sender: TObject);
begin
    if not (CategoriasConsultas.State in [dsInsert, dsEdit]) then begin
        CategoriasConsultas.Filtered := False;
        FNodoActual := FrameCategInformes.RetornarNodoActual;
        CategoriasConsultas.Filter := 'CodigoCategoriaPadre = ' + inttostr(FNodoActual);
        CategoriasConsultas.Filtered := True;
        Lista.Reload;
    end;
end;

end.
