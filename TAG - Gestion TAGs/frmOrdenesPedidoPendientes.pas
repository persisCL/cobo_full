
{-----------------------------------------------------------------------------
 File Name: 
 Author:    
 Date Created: 
 Language: ES-AR
 Description: 

Revision : 1
Date: 26/02/2009
Author: pdominguez
Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para 
	los bloqueos de tablas en la lectura

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

-----------------------------------------------------------------------------}
unit frmOrdenesPedidoPendientes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, StdCtrls, ListBoxEx, DBListEx, DPSControls, UtilProc,
  StrUtils, DMConnection, RStrings, Util;

type
  TFormOrdenesPedidoPendientes = class(TForm)
    qryDetalleListaOrdenesPedidoPendientes: TADOQuery;
    dsObtenerListaOrdenesPedidoPendientes: TDataSource;
    dsDetalleListaOrdenesPedidoPendientes: TDataSource;
    DBListEx1: TDBListEx;
    DBListEx2: TDBListEx;
    Label1: TLabel;
    Label2: TLabel;
    spObtenerListaOrdenesPedidoPendientes: TADOStoredProc;
    spObtenerListaOrdenesPedidoPendientesCodigoOrdenPedido: TIntegerField;
    spObtenerListaOrdenesPedidoPendientesCodigoPuntoEntrega: TIntegerField;
    spObtenerListaOrdenesPedidoPendientesFechaOrdenPedido: TDateTimeField;
    spObtenerListaOrdenesPedidoPendientesEstado: TStringField;
    spObtenerListaOrdenesPedidoPendientesUsuario: TStringField;
    spObtenerListaOrdenesPedidoPendientesPuntoEntrega: TStringField;
    spCancelarOrdenPedidoTAGs: TADOStoredProc;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    DateTimeField1: TDateTimeField;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    spObtenerListaOrdenesPedidoPendientesCodigoAlmacenRecepcion: TIntegerField;
    txtObservaciones: TEdit;
    Label3: TLabel;
    btnSalir: TButton;
    btnCancelar: TButton;
    btnAtender: TButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSalirClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnAtenderClick(Sender: TObject);
  private
    { Private declarations }
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
  public
    { Public declarations }
	function Inicializar(txtCaption: String; MDIChild: boolean): Boolean;
  end;

var
  FormOrdenesPedidoPendientes: TFormOrdenesPedidoPendientes;

implementation

uses EntregarATransportesConPedidos;

{$R *.dfm}

function TFormOrdenesPedidoPendientes.Inicializar(txtCaption: String; MDIChild: boolean): Boolean;
Var
	S: TSize;
begin
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

	if MDIChild then begin
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;
	CenterForm(Self);
	Caption := AnsiReplaceStr(txtCaption, '&', '');

    txtObservaciones.Clear;
	Result := false;
	try
		spObtenerListaOrdenesPedidoPendientes.Open;
		qryDetalleListaOrdenesPedidoPendientes.Open;

        btnCancelar.Enabled := not spObtenerListaOrdenesPedidoPendientes.IsEmpty;
        btnAtender.Enabled := btnCancelar.Enabled;

		Result := true;
	except
	    on e: Exception do MsgBoxErr(MSG_ERROR_OBTENER_OP_PENDIENTES, e.message, self.caption, MB_ICONSTOP);
	end
end;

procedure TFormOrdenesPedidoPendientes.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree
end;

procedure TFormOrdenesPedidoPendientes.btnSalirClick(Sender: TObject);
begin
	spObtenerListaOrdenesPedidoPendientes.Close;
	qryDetalleListaOrdenesPedidoPendientes.Close;

	Close
end;

procedure TFormOrdenesPedidoPendientes.btnCancelarClick(Sender: TObject);
begin
	if spObtenerListaOrdenesPedidoPendientes.IsEmpty then Exit;

    if not(MsgBox('�Confirma la Cancelaci�n de la Orden de Pedido?', self.Caption,
          MB_YESNO + MB_ICONQUESTION) = IDYES) then Exit;

	try
		with spCancelarOrdenPedidoTAGs, Parameters do begin
			ParamByName ('@CodigoOrdenPedido').Value := spObtenerListaOrdenesPedidoPendientes.FieldByName('CodigoOrdenPedido').Value;
			ParamByName ('@Usuario').Value := UsuarioSistema;
			ParamByName ('@Observaciones').Value := txtObservaciones.Text;

            ExecProc
        end;

		spObtenerListaOrdenesPedidoPendientes.Close;
		qryDetalleListaOrdenesPedidoPendientes.Close;
		spObtenerListaOrdenesPedidoPendientes.Open;
		qryDetalleListaOrdenesPedidoPendientes.Open;

        txtObservaciones.Clear
    except
	    on e: Exception do MsgBoxErr(MSG_ERROR_CANCELAR_ORDEN_PEDIDOS, e.message, self.caption, MB_ICONSTOP);
    end
end;

procedure TFormOrdenesPedidoPendientes.btnAtenderClick(Sender: TObject);
var
    f:TfrmEntregarATransportesCP;
begin
	if spObtenerListaOrdenesPedidoPendientes.IsEmpty then Exit;
    
	if FindFormOrCreate(TfrmEntregarATransportesCP, f) then
		f.Show
	else begin
        if not f.Inicializar(True, 'Entregar a Transporte', -1, spObtenerListaOrdenesPedidoPendientes.FieldByName('CodigoAlmacenRecepcion').Value) then f.Release;
	end;
end;

end.
