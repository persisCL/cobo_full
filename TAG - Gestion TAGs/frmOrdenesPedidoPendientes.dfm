object FormOrdenesPedidoPendientes: TFormOrdenesPedidoPendientes
  Left = 275
  Top = 120
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Ordenes de Pedido Pendientes'
  ClientHeight = 566
  ClientWidth = 537
  Color = clBtnFace
  Constraints.MaxHeight = 600
  Constraints.MaxWidth = 545
  Constraints.MinHeight = 594
  Constraints.MinWidth = 543
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 229
    Height = 13
    Caption = 'Lista de Ordenes de Pedido Pendientes:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 8
    Top = 328
    Width = 45
    Height = 13
    Caption = 'Detalle:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 9
    Top = 504
    Width = 181
    Height = 13
    Caption = 'Observaciones de Cancelaci'#243'n:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object DBListEx1: TDBListEx
    Left = 8
    Top = 24
    Width = 521
    Height = 297
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 90
        Header.Caption = 'Orden Pedido'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taRightJustify
        IsLink = False
        FieldName = 'CodigoOrdenPedido'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 300
        Header.Caption = 'Punto de Entrega'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'PuntoEntrega'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 110
        Header.Caption = 'Fecha Solicitud'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'FechaOrdenPedido'
      end>
    DataSource = dsObtenerListaOrdenesPedidoPendientes
    DragReorder = True
    ParentColor = False
    TabOrder = 0
    TabStop = True
  end
  object DBListEx2: TDBListEx
    Left = 8
    Top = 341
    Width = 521
    Height = 152
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 255
        Header.Caption = 'Categoria'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'DescripcionCategoria'
      end
      item
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 120
        Header.Caption = 'Cantidad Solicitada'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taRightJustify
        IsLink = False
        FieldName = 'Cantidad'
      end
      item
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 120
        Header.Caption = 'Pendientes'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taRightJustify
        IsLink = False
        FieldName = 'Pendiente'
      end>
    DataSource = dsDetalleListaOrdenesPedidoPendientes
    DragReorder = True
    ParentColor = False
    TabOrder = 1
    TabStop = True
  end
  object txtObservaciones: TEdit
    Left = 192
    Top = 501
    Width = 337
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 80
    TabOrder = 2
  end
  object btnSalir: TButton
    Left = 450
    Top = 532
    Width = 75
    Height = 25
    Caption = '&Salir'
    TabOrder = 5
    OnClick = btnSalirClick
  end
  object btnCancelar: TButton
    Left = 369
    Top = 533
    Width = 75
    Height = 25
    Caption = '&Cancelar'
    TabOrder = 4
    OnClick = btnCancelarClick
  end
  object btnAtender: TButton
    Left = 285
    Top = 533
    Width = 75
    Height = 25
    Caption = '&Atender'
    TabOrder = 3
    OnClick = btnAtenderClick
  end
  object qryDetalleListaOrdenesPedidoPendientes: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    DataSource = dsObtenerListaOrdenesPedidoPendientes
    Parameters = <
      item
        Name = 'CodigoOrdenPedido'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'
      
        #9'DOPT.*, CAT.Descripcion DescripcionCategoria, dbo.CalcularPedid' +
        'osPendientes(OP.CodigoPuntoEntrega, DOPT.Categoria, OP.CodigoOrd' +
        'enPedido) Pendiente'
      'FROM'
      
        #9'DetalleOrdenPedidoTAGs DOPT WITH (NOLOCK), Categorias CAT WITH ' +
        '(NOLOCK), OrdenesPedidoTAGS OP  WITH (NOLOCK) '
      'WHERE'
      #9'CAT.Categoria = DOPT.Categoria'
      #9'AND DOPT.CodigoOrdenPedido = :CodigoOrdenPedido'
      '    AND OP.CodigoOrdenPedido = DOPT.CodigoOrdenPedido')
    Left = 264
    Top = 96
  end
  object dsObtenerListaOrdenesPedidoPendientes: TDataSource
    DataSet = spObtenerListaOrdenesPedidoPendientes
    Left = 232
    Top = 64
  end
  object dsDetalleListaOrdenesPedidoPendientes: TDataSource
    DataSet = qryDetalleListaOrdenesPedidoPendientes
    Left = 232
    Top = 96
  end
  object spObtenerListaOrdenesPedidoPendientes: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerListaOrdenesPedidoPendientes'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end>
    Left = 264
    Top = 64
    object spObtenerListaOrdenesPedidoPendientesCodigoOrdenPedido: TIntegerField
      FieldName = 'CodigoOrdenPedido'
    end
    object spObtenerListaOrdenesPedidoPendientesCodigoPuntoEntrega: TIntegerField
      FieldName = 'CodigoPuntoEntrega'
    end
    object spObtenerListaOrdenesPedidoPendientesFechaOrdenPedido: TDateTimeField
      FieldName = 'FechaOrdenPedido'
    end
    object spObtenerListaOrdenesPedidoPendientesEstado: TStringField
      FieldName = 'Estado'
      FixedChar = True
      Size = 1
    end
    object spObtenerListaOrdenesPedidoPendientesUsuario: TStringField
      FieldName = 'Usuario'
      FixedChar = True
      Size = 10
    end
    object spObtenerListaOrdenesPedidoPendientesPuntoEntrega: TStringField
      FieldName = 'PuntoEntrega'
      Size = 60
    end
    object spObtenerListaOrdenesPedidoPendientesCodigoAlmacenRecepcion: TIntegerField
      FieldName = 'CodigoAlmacenRecepcion'
    end
  end
  object spCancelarOrdenPedidoTAGs: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'CancelarOrdenPedidoTAGs'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoOrdenPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@Observaciones'
        Attributes = [paNullable]
        DataType = ftString
        Size = 80
        Value = Null
      end>
    Left = 264
    Top = 127
    object IntegerField1: TIntegerField
      FieldName = 'CodigoOrdenPedido'
    end
    object IntegerField2: TIntegerField
      FieldName = 'CodigoPuntoEntrega'
    end
    object DateTimeField1: TDateTimeField
      FieldName = 'FechaOrdenPedido'
    end
    object StringField1: TStringField
      FieldName = 'Estado'
      FixedChar = True
      Size = 1
    end
    object StringField2: TStringField
      FieldName = 'Usuario'
      FixedChar = True
      Size = 10
    end
    object StringField3: TStringField
      FieldName = 'PuntoEntrega'
      Size = 60
    end
  end
end
