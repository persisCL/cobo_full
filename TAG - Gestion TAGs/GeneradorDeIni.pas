unit GeneradorDeIni;
{
Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

Etiqueta    : 20160315 MGO
Descripci�n : Se utiliza InstallIni en vez de ApplicationIni

}
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  StdCtrls, ExtCtrls, UtilProc, Variants, Util,
  DPSControls, DmiCtrls, PeaProcs;

type
  TFormGeneradorIni = class(TForm)
	Label1: TLabel;
	Bevel1: TBevel;
    edtAlmacenOrigen: TNumericEdit;
    btnAceptar: TButton;
    btnCancelar: TButton;
	procedure btnAceptarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
  private
	{ Private declarations }
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    function GetAlmacenOrigen: integer;
  public
	{ Public declarations }
	Function Inicializa: Boolean;
    property AlmacenOrigen: integer read GetAlmacenOrigen;
  end;

var
  FormGeneradorIni: TFormGeneradorIni;

implementation

{$R *.DFM}

Function TFormGeneradorIni.Inicializa: Boolean;
resourcestring
	MSG_YACONFIGURADO = 'Archivo de Inicio ya se encuentra configurado.';
var
	AAlmacenOrigen: integer;
begin
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216
    { INICIO : 20160315 MGO
    AAlmacenOrigen:= ApplicationIni.ReadInteger('General', 'AlmacenOrigen', -1);
    }
    AAlmacenOrigen:= InstallIni.ReadInteger('General', 'AlmacenOrigen', -1);
    // FIN : 20160315 MGO
    if AAlmacenOrigen > -1 then edtAlmacenOrigen.ValueInt := AAlmacenOrigen;
    Result:= True;
end;

procedure TFormGeneradorIni.btnAceptarClick(Sender: TObject);
resourcestring
	MSG_DEBE_COMPLETAR_AO =  'Debe completar Almac�n de Origen';
	MSG_DEBE_COMPLETAR_BO =  'Debe completar Bodega de Origen';
begin

	if not ValidateControls([edtAlmacenOrigen],
	  [(Trim(edtAlmacenOrigen.Text) <> '')], self.Caption,
	  [MSG_DEBE_COMPLETAR_AO]) then begin
		Exit;
	end;

    { INICIO : 20160315 MGO
    ApplicationIni.WriteInteger('General', 'AlmacenOrigen', edtAlmacenOrigen.ValueInt);
    }
    InstallIni.WriteInteger('General', 'AlmacenOrigen', edtAlmacenOrigen.ValueInt);
    // FIN : 20160315 MGO
    ModalResult := MrOk;
end;

procedure TFormGeneradorIni.btnCancelarClick(Sender: TObject);
begin
    ModalResult := MrCancel;
end;

function TFormGeneradorIni.GetAlmacenOrigen: integer;
begin
    result := edtAlmacenOrigen.ValueInt;
end;

end.
