{********************************** File Header ********************************
File Name   : frmIdentificarTAG.pas
Author      : rcastro
Date Created:
Language    : ES-AR
Description :

Revision : 1
    Date: 26/02/2009
    Author: pdominguez
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura


Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

*******************************************************************************}

unit frmIdentificarTAG;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, PeaProcs, Util, RStrings,
  PeaTypes, DB, ADODB, UtilProc, Comm, ExtCtrls, StrUtils, DmiCtrls,
  VariantComboBox;

type
  TFormIdentificarTAG = class(TForm)
    txtEtiqueta: TEdit;
    Label1: TLabel;
    Label2: TLabel;
	Label3: TLabel;
    txtContextMark: TEdit;
    Label4: TLabel;
    txtCategoria: TEdit;
    GBListas: TGroupBox;
    spLeerDatosTAG: TADOStoredProc;
    lblNoExiste: TLabel;
    txtContractSerialNumber: TEdit;
	txtDescripcionCategoria: TEdit;
    ledVerde: TLed;
    ledGris: TLed;
    ledAmarilla: TLed;
	ledNegra: TLed;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
	Label8: TLabel;
    Bevel1: TBevel;
    Label9: TLabel;
    txtDescripcionUbicacion: TEdit;
    lblNoPuedeCambiarEstadoSituacion: TLabel;
    spActualizarSituacionTag: TADOStoredProc;
    Label10: TLabel;
    txtEstadoActual: TEdit;
    lblNuevoEstado: TLabel;
    cbEstadosPosibles: TVariantComboBox;
    qryEstadosPosibles: TADOQuery;
    spLeerDatosTAGContextMark: TWordField;
    spLeerDatosTAGContractSerialNumber: TLargeintField;
    spLeerDatosTAGEtiqueta: TStringField;
    spLeerDatosTAGCodigoCategoria: TWordField;
    spLeerDatosTAGCodigoEstadoSituacionTAG: TWordField;
    spLeerDatosTAGIndicadorListaVerde: TBooleanField;
    spLeerDatosTAGIndicadorListaAmarilla: TBooleanField;
    spLeerDatosTAGIndicadorListaGris: TBooleanField;
    spLeerDatosTAGIndicadorListaNegra: TBooleanField;
    spLeerDatosTAGDescripcionCategoria: TStringField;
    spLeerDatosTAGDescripcionUbicacion: TStringField;
    spLeerDatosTAGEstadoSituacion: TStringField;
    spLeerDatosTAGOtraConcesionaria: TIntegerField;
    spLeerDatosTAGCodigoCategoriaUrbana: TWordField;
    spLeerDatosTAGPatente: TStringField;
	lblOtraConcesionaria: TLabel;
    btnSalir: TButton;
    btnAceptar: TButton;
    btnCancelar: TButton;
    procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure btnAceptarClick(Sender: TObject);
	procedure btnCancelarClick(Sender: TObject);
    procedure txtEtiquetaChange(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
	{ Private declarations }
	FCambiarEstadoSituacion: Boolean;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    //INICIO:	20160812 CFU
    strPatente: string;
    //TERMINO:	20160812 CFU
	procedure LimpiarDatos;
	procedure GetTagData(TagNumber:DWORD);
	procedure CargarEstadosPosibles(CodigoEstadoActual: Integer);
  public
	{ Public declarations }
	function Inicializar (txtCaption: String; MDIChild, CambiarEstadoSituacion: boolean):Boolean;
  end;

var
  FormIdentificarTAG: TFormIdentificarTAG;

implementation

uses DMConnection;

{$R *.dfm}

function TFormIdentificarTAG.Inicializar (txtCaption: String; MDIChild, CambiarEstadoSituacion: boolean):Boolean;
Var
	S: TSize;
begin
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216
	if MDIChild then begin
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;
	CenterForm(Self);
	Caption := AnsiReplaceStr(txtCaption, '&', '');

	FCambiarEstadoSituacion := CambiarEstadoSituacion;

	txtEtiqueta.Clear;
	LimpiarDatos;

	KeyPreview := True;
	Result := True;
end;

procedure TFormIdentificarTAG.LimpiarDatos;
begin
//	txtEtiqueta.Clear;
	txtContractSerialNumber.Clear;
	txtContextMark.Clear;
	txtCategoria.Clear;
	txtDescripcionCategoria.Clear;
	txtDescripcionUbicacion.Clear;
	txtEstadoActual.Clear;

	ledGris.Enabled 	:= False;
	ledVerde.Enabled	:= False;
	ledNegra.Enabled 	:= False;
	ledAmarilla.Enabled := False;

	lblNuevoEstado.Visible		:= False;
	cbEstadosPosibles.Visible 	:= False;
	btnAceptar.Visible 			:= False;
	btnCancelar.Visible 		:= False;

	lblNoExiste.Visible 		:= False;
	lblNoPuedeCambiarEstadoSituacion.Visible := False;
	lblOtraConcesionaria.Visible := False;
end;

procedure TFormIdentificarTAG.btnSalirClick(Sender: TObject);
begin
	Close;
end;

procedure TFormIdentificarTAG.CargarEstadosPosibles(CodigoEstadoActual: Integer);
begin
	cbEstadosPosibles.Clear;
	qryEstadosPosibles.Close;
	qryEstadosPosibles.Parameters.ParamByName('CodigoEstadoSituacionTAG').Value := CodigoEstadoActual;

 	qryEstadosPosibles.Open;
	While not qryEstadosPosibles.Eof do begin
        //INICIO:	20160812 CFU
    	if (strPatente = '') and (UpperCase(TrimRight(qryEstadosPosibles.FieldByName('Descripcion').AsString)) = 'ROBADO') then begin
        	qryEstadosPosibles.Next;
            Continue;
        end;
        //TERMINO:	20160812 CFU
        
    	cbEstadosPosibles.Items.Add(qryEstadosPosibles.FieldByName('Descripcion').AsString, qryEstadosPosibles.FieldByName('CodigoEstadoSituacionTAG').AsInteger);
		qryEstadosPosibles.Next;
	end;
    qryEstadosPosibles.Close;

	cbEstadosPosibles.ItemIndex := 0;
    cbEstadosPosibles.Refresh
end;

procedure TFormIdentificarTAG.GetTagData(TagNumber: DWORD);
begin
	try
        with spLeerDatosTAG, Parameters do begin
			Close;
            //INICIO:	20160812 CFU 
            Parameters.Refresh;
            //TERMINO:	20160812 CFU 
            ParamByName ('@ContractSerialNumber').Value := TagNumber;
            Open;
            if RecordCount > 0 then begin
                txtContractSerialNumber.Text:= FieldByName ('ContractSerialNumber').AsString;
				txtContextMark.Text			:= FieldByName ('ContextMark').AsString;
                txtCategoria.Text			:= FieldByName ('CodigoCategoria').AsString;
                txtDescripcionCategoria.Text:= FieldByName ('DescripcionCategoria').AsString;
                txtEstadoActual.Text		:= FieldByName ('EstadoSituacion').AsString;
                txtDescripcionUbicacion.Text:= FieldByName ('DescripcionUbicacion').AsString;

                ledVerde.Enabled 			:= FieldByName ('IndicadorListaVerde').AsBoolean;
                ledAmarilla.Enabled 		:= FieldByName ('IndicadorListaAmarilla').AsBoolean;
                ledGris.Enabled 			:= FieldByName ('IndicadorListaGris').AsBoolean;
				ledNegra.Enabled 			:= FieldByName ('IndicadorListaNegra').AsBoolean;
                                                             
                //INICIO:	20160812 CFU 
                strPatente					:= FieldByName ('Patente').AsString;
                //TERMINO:	20160812 CFU 

				if FCambiarEstadoSituacion then begin
					btnAceptar.Visible 			:= (FieldByName ('CodigoEstadoSituacionTAG').AsInteger
													in [CONST_TAG_DISPONIBLE, CONST_TAG_DEVUELTO,
														CONST_TAG_ROBADO, CONST_TAG_BAJA])
													and (FieldByName ('OtraConcesionaria').AsInteger = 0);
					btnCancelar.Visible 		:= btnAceptar.Visible;
					lblNuevoEstado.Visible 		:= btnAceptar.Visible;
					cbEstadosPosibles.Visible 	:= btnAceptar.Visible;

					lblNoPuedeCambiarEstadoSituacion.Visible := not btnAceptar.Visible;
					lblOtraConcesionaria.Visible := FieldByName ('OtraConcesionaria').AsInteger = 1;

					if btnAceptar.Visible then CargarEstadosPosibles(FieldByName ('CodigoEstadoSituacionTAG').AsInteger);
				end else begin
					lblOtraConcesionaria.Visible := FieldByName ('OtraConcesionaria').AsInteger = 1;
				end
			end else begin
				LimpiarDatos;
                lblNoExiste.Visible := True;
			end;
            Close;
        end;
	except
        on e: Exception do begin
			MsgBoxErr(MSG_ERROR_LECTURA_DATOS_TAG, e.message, Caption, MB_ICONSTOP);
            LimpiarDatos;
		end;
    end;
end;

procedure TFormIdentificarTAG.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree
end;

procedure TFormIdentificarTAG.btnAceptarClick(Sender: TObject);
begin
	try
        if MsgBox('�Est� seguro de cambiar el estado del Telev�a?', self.Caption, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
			try
				with spActualizarSituacionTag, Parameters do begin
    				Close;
                    //INICIO:	20160816 CFU
                    Parameters.Refresh;
                    //TERMINO:	20160816 CFU
					ParamByName ('@ContextMark').Value 				:= txtContextMark.Text;
					ParamByName ('@ContractSerialNumber').Value 	:= txtContractSerialNumber.Text;
					ParamByName ('@CodigoEstadoSituacionTAG').Value := cbEstadosPosibles.Value;

					ExecProc
				end
			except
				on e: Exception do MsgBoxErr(MSG_ERROR_GRABACION_DATOS_TAG, e.message, Caption, MB_ICONSTOP);
			end
		end
	finally
		LimpiarDatos;
	end;
end;

procedure TFormIdentificarTAG.btnCancelarClick(Sender: TObject);
begin
	LimpiarDatos;
	txtEtiqueta.SetFocus;
end;

procedure TFormIdentificarTAG.txtEtiquetaChange(Sender: TObject);
begin
	if (Length (txtEtiqueta.Text) = 11) and ValidarEtiqueta(self.Caption, 'Telev�a Ingresado', PadL(txtEtiqueta.Text, 11, '0'), true) then begin
		GetTagData(EtiquetaToSerialNumber(PadL(txtEtiqueta.Text, 11, '0')))
	end
	else begin
		LimpiarDatos
	end
end;

procedure TFormIdentificarTAG.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if key = #27 then Close
end;

end.

