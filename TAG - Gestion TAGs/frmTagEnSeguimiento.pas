{********************************** File Header ********************************
File Name   : frmTagEnSeguimiento.pas
Author      : Mcabello
Date Created: 14/10/2013
Language    : ES-CL
Description : Permite agregar televia en seguimiento
SS          : 660 C

Author      : Mcabello
Date Created: 13/11/2013
Language    : ES-CL
Description : Agregan botones Nuevo y Limpiar, y se modifica funcionalidad del editar
        	se agregan filtros de busqueda por fecha desde y hasta por EH y/o MMICO
Firma		: SS_660C_MCA_20131113

Author      : Mcabello
Date Created: 08/01/2014
Language    : ES-CL
Description : se agrega variable FEditar que indica si el tag es editado o nuevo.
Firma		: SS_660C_MCA_20140108

 Autor          :   CQuezada
 Fecha          :   07/03/2014
 Firma          :   SS_660_CQU_20140307
 Descripcion    :   Se agrega la variable MSG_SIN_REGISTROS con el mensaje que indica que no se encuentran registros al filtrar.

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

}

unit frmTagEnSeguimiento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, Grids, DBGrids, PeaProcs, StdCtrls, DPSControls, Util,
  ListBoxEx, DBListEx, ExtCtrls, UtilProc, DPSGrid, UtilDB, RStrings,
  StrUtils, DMConnection, EventLog, Abm_obj, DbList, ImgList, BuscaTab,
  PeaTypes,
  Buttons, DBTables, OleCtrls,
  DmiCtrls, Mask,  ComCtrls,  validate, Dateedit ;

type
  TTagEnSeguimientoForm = class(TForm)
    dblTagEnSeguimiento: TDBListEx;
    spObtenerTagEnSeguimiento: TADOStoredProc;
    dsObtenerTagEnSeguimiento: TDataSource;
    Imagenes: TImageList;
    grpFiltro: TGroupBox;
    lblEtiquetaFiltro: TLabel;
    txtEtiquetaFiltro: TEdit;
    lblPatente: TLabel;
    txtPatente: TEdit;
    lblFechaAltaEH: TLabel;
    deFechaAltaEHDesde: TDateEdit;                                              //SS_660C_MCA_20131113
    lblFechaBajaEH: TLabel;
    deFechaBajaEHDesde: TDateEdit;                                              //SS_660C_MCA_20131113
    lblFechaAltaMMICO: TLabel;
    lblFechaBajaMMICO: TLabel;
    deFechaBajaMMICODesde: TDateEdit;                                           //SS_660C_MCA_20131113
    btnBuscar: TButton;
    spAgregarTagEnSeguimiento: TADOStoredProc;
    spActualizarTagEnSeguimiento: TADOStoredProc;
    spEliminarTagEnSeguimiento: TADOStoredProc;
    deFechaAltaMMICODesde: TDateEdit;                                           //SS_660C_MCA_20131113
    btnSalir: TButton;
    grpopciones: TGroupBox;
    pnlOpciones: TPanel;
    btnGuardar: TButton;
    btnEditar: TButton;
    btnCancelar: TButton;
    grpNuevoTag: TGroupBox;
    lblMMIContactOperator: TLabel;
    lblEtiqueta: TLabel;
    lblExceptionHandling: TLabel;
    TxtEtiqueta: TEdit;
    chkSetExceptionHandling: TCheckBox;
    chkSetMMIContactOperator: TCheckBox;
    TxtId: TEdit;
    btnNuevo: TButton;                                                          //SS_660C_MCA_20131113
    btnLimpiar: TButton;                                                        //SS_660C_MCA_20131113
    deFechaAltaEHHasta: TDateEdit;                                              //SS_660C_MCA_20131113
    deFechaBajaEHHasta: TDateEdit;                                              //SS_660C_MCA_20131113
    deFechaAltaMMICOHasta: TDateEdit;                                           //SS_660C_MCA_20131113
    deFechaBajaMMICOHasta: TDateEdit;                                           //SS_660C_MCA_20131113
    lbldesdeEH: TLabel;                                                         //SS_660C_MCA_20131113
    lblHastaEH: TLabel;                                                         //SS_660C_MCA_20131113
    lblDesdeMMICO: TLabel;                                                      //SS_660C_MCA_20131113
    lblHastaMMICO: TLabel;                                                  	//SS_660C_MCA_20131113
    procedure dblTagEnSeguimientoDrawText(
      Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
      State: TOwnerDrawState; var Text: string; var TxtRect: TRect;
      var ItemWidth: Integer; var DefaultDraw: Boolean);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dblTagEnSeguimientoClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnGuardarClick(Sender: TObject);
    procedure dblTagsEnSeguimientoColumnsHeaderClick(Sender: TObject);
    procedure btnBuscarClick(Sender: TObject);
    procedure btnLimpiarClick(Sender: TObject);									//SS_660C_MCA_20131113
    procedure btnNuevoClick(Sender: TObject);                                   //SS_660C_MCA_20131113
  private
    FEditar: Boolean;                                                           //SS_660C_MCA_20140108
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;              //SS_1147_NDR_20141216
    { Private declarations }
    procedure ObtenerTagEnSeguimiento(Etiqueta: String; Patente: string; FechaAltaEHDesde, FechaAltaEHHasta, FechaBajaEHDesde, FechaBajaEHHasta, FechaAltaMMICODesde, FechaAltaMMICOHasta, FechaBajaMMICODesde, FechaBajaMMICOHasta: TDateTime);		//SS_660C_MCA_20131113
    procedure LimpiarDatos;
  public
    { Public declarations }
    function Inicializar(txtCaption: String; MDIChild: boolean): Boolean;
  end;

var
  TagEnSeguimientoForm: TTagEnSeguimientoForm;

implementation

{$R *.dfm}

procedure TTagEnSeguimientoForm.btnBuscarClick(Sender: TObject);
resourcestring
	MSG_SIN_REGISTROS = 'No se encontraron registros para los criterios de b�squeda indicados';
    MSG_ERROR_OBTENER = 'Se ha producido un error al obtener los Telev�as en seguimiento';
begin

    if not ValidarEtiqueta(self.Caption, 'Telev�a Ingresado', PadL(txtEtiquetaFiltro.Text, 11, '0'), True) then begin
        exit;
    end;

    if not ValidateControls([deFechaAltaEHDesde, deFechaBajaEHDesde, deFechaAltaMMICODesde, deFechaBajaMMICODesde],                                                                //SS_660C_MCA_20131113
        [not ((deFechaAltaEHDesde.Date <> Util.NullDate) and (deFechaAltaEHHasta.Date <> Util.NullDate) and (deFechaAltaEHDesde.Date > deFechaAltaEHHasta.Date)),                  //SS_660C_MCA_20131113
        not ((deFechaBajaEHDesde.Date <> Util.NullDate) and (deFechaBajaEHHasta.Date <> Util.NullDate) and (deFechaBajaEHDesde.Date > deFechaBajaEHHasta.Date)),                   //SS_660C_MCA_20131113
        not ((deFechaAltaMMICODesde.Date <> Util.NullDate) and (deFechaAltaMMICOHasta.Date <> Util.NullDate) and (deFechaAltaMMICODesde.Date > deFechaAltaMMICOHasta.Date)),       //SS_660C_MCA_20131113
        not ((deFechaBajaMMICODesde.Date <> Util.NullDate) and (deFechaBajaMMICODesde.Date <> Util.NullDate) and (deFechaBajaMMICODesde.Date > deFechaBajaMMICOHasta.Date))]       //SS_660C_MCA_20131113
        ,Caption                                                                                                                                                                   //SS_660C_MCA_20131113
        ,[MSG_VALIDAR_ORDEN_FECHA, MSG_VALIDAR_ORDEN_FECHA, MSG_VALIDAR_ORDEN_FECHA, MSG_VALIDAR_ORDEN_FECHA]) then Exit;                                                          //SS_660C_MCA_20131113

    //    if txtEtiquetaFiltro.Text<>EmptyStr then                                                      //SS_660C_MCA_20131113
    //            ParamByName('@Etiqueta').Value := txtEtiquetaFiltro.Text;                             //SS_660C_MCA_20131113
    //
    //        if txtPatente.Text <> EmptyStr then                                                       //SS_660C_MCA_20131113
    //            ParamByName('@Patente').Value := txtPatente.Text;                                     //SS_660C_MCA_20131113
    //
    //        if deFechaAltaEH.Date <> NullDate then                                                    //SS_660C_MCA_20131113
    //            ParamByName('@FechaAltaEH').Value := deFechaAltaEH.Date;                              //SS_660C_MCA_20131113
    //
    //        if deFechaBajaEH.Date <> NullDate then                                                    //SS_660C_MCA_20131113
    //            ParamByName('@FechaBajaEH').Value := deFechaBajaEH.Date;                              //SS_660C_MCA_20131113
    //
    //        if deFechaAltaMMICO.Date <> NullDate then                                                 //SS_660C_MCA_20131113
    //            ParamByName('@FechaAltaMMICO').Value := deFechaAltaMMICO.Date;                        //SS_660C_MCA_20131113
    //
    //        if deFechaBajaMMICO.Date <> NullDate then                                                 //SS_660C_MCA_20131113
    //            ParamByName('@FechaBajaMMICO').Value := deFechaBajaMMICO.Date;                        //SS_660C_MCA_20131113
    //
    //        Open;                                                                                     //SS_660C_MCA_20131113
    //
    //        if spObtenerTagEnSeguimiento.RecordCount = 0 then                                         //SS_660C_MCA_20131113
    //        	MsgBox(MSG_SIN_REGISTROS, Self.Caption, MB_OK + MB_ICONWARNING);                        //SS_660C_MCA_20131113
    //
    //
    //    except                                                                                        //SS_660C_MCA_20131113
    //         on e:Exception do begin                                                                  //SS_660C_MCA_20131113
    //            MsgBoxErr(MSG_ERROR_OBTENER, e.Message, 'Obtener Tag en Seguimiento', 1);             //SS_660C_MCA_20131113
    //        end;                                                                                      //SS_660C_MCA_20131113
    //
    //    end;                                                                                          //SS_660C_MCA_20131113

    ObtenerTagEnSeguimiento(txtEtiquetaFiltro.Text, txtPatente.Text, deFechaAltaEHDesde.Date, deFechaAltaEHHasta.Date, deFechaBajaEHDesde.Date, deFechaBajaEHHasta.Date, deFechaAltaMMICODesde.Date, deFechaAltaMMICOHasta.Date, deFechaBajaMMICODesde.Date, deFechaBajaMMICOHasta.Date);		//SS_660C_MCA_20131113
end;

procedure TTagEnSeguimientoForm.btnCancelarClick(Sender: TObject);
begin
     //Objetos del Filtro}
    txtId.Clear;
    txtEtiquetaFiltro.Clear;
    txtPatente.Clear;
    deFechaAltaEHDesde.Date := NullDate;                                        //SS_660C_MCA_20131113
    deFechaAltaMMICODesde.Date := NullDate;                                     //SS_660C_MCA_20131113
    deFechaBajaEHDesde.Date := NullDate;                                        //SS_660C_MCA_20131113
    deFechaBajaMMICODesde.Date := NullDate;                                     //SS_660C_MCA_20131113
    deFechaAltaEHHasta.Date := NullDate;                                        //SS_660C_MCA_20131113
    deFechaAltaMMICOHasta.Date := NullDate;                                     //SS_660C_MCA_20131113
    deFechaBajaEHHasta.Date := NullDate;                                        //SS_660C_MCA_20131113
    deFechaBajaMMICOHasta.Date := NullDate;                                     //SS_660C_MCA_20131113

    //Objetos Nuevo Tag
    txtEtiqueta.Clear;
    chkSetExceptionHandling.Checked  := False;
    chkSetMMIContactOperator.Checked    := False;
	btnNuevo.Enabled := True;                                                   //SS_660C_MCA_20131113
    btnEditar.Enabled := False;													//SS_660C_MCA_20131113
	//btnEditar.Caption := '&Nuevo';											//SS_660C_MCA_20131113
    //btnEditar.Enabled := True;												//SS_660C_MCA_20131113
    btnGuardar.Enabled := False;
    grpNuevoTag.Enabled := False;
    grpNuevoTag.Visible := False;
    btnLimpiar.Enabled	 := True;												//SS_660C_MCA_20131113
    If spObtenerTagEnSeguimiento.Active then spObtenerTagEnSeguimiento.Close;
    //ObtenerTagEnSeguimiento(EmptyStr, EmptyStr, NullDate, NullDate, NullDate, NullDate);
    txtEtiquetaFiltro.SetFocus;;
end;

procedure TTagEnSeguimientoForm.btnEditarClick(Sender: TObject);
begin
	//if btnEditar.Caption = '&Editar' then begin								//SS_660C_MCA_20131113

    	if txtEtiqueta.Text <> EmptyStr then  begin
        	grpNuevoTag.Enabled := True;
        	grpNuevoTag.Visible := True;
        	btnGuardar.Enabled := True;
        	btnEditar.Enabled := False;
        	txtEtiqueta.Enabled := False;
        	grpNuevoTag.Caption := 'Edici�n Telev�a En Seguimiento';
        	btnLimpiar.Enabled	:= False;										//SS_660C_MCA_20131113
            FEditar := True;                                                    //SS_660C_MCA_20140108
    	end
    	else begin
        	grpNuevoTag.Enabled := False;
        	btnGuardar.Enabled := False;
        	btnLimpiar.Enabled	:= True;										//SS_660C_MCA_20131113
    	end;
	//else begin																//SS_660C_MCA_20131113
    //     grpNuevoTag.Caption := 'Nuevo Telev�a En Seguimiento';				//SS_660C_MCA_20131113
    //     grpNuevoTag.Visible := True;											//SS_660C_MCA_20131113
    //     grpNuevoTag.Enabled := True;											//SS_660C_MCA_20131113
    //     btnGuardar.Enabled := True;											//SS_660C_MCA_20131113
    //     btnEditar.Enabled := False;											//SS_660C_MCA_20131113
    //     txtEtiqueta.Text := EmptyStr;										//SS_660C_MCA_20131113
    //     chkSetMMIContactOperator.Checked := False;							//SS_660C_MCA_20131113
    //     chkSetExceptionHandling.Checked := False;							//SS_660C_MCA_20131113
    //     txtEtiqueta.Enabled := True;											//SS_660C_MCA_20131113
    //     txtEtiqueta.SetFocus;												//SS_660C_MCA_20131113
    //end;																		//SS_660C_MCA_20131113
end;

procedure TTagEnSeguimientoForm.btnGuardarClick(Sender: TObject);
resourcestring
    MSG_CONFIRMAR_ELIMINAR_TAG_EN_SEGUIMIENTO = '�Est� seguro que desea eliminar el telev�a de Seguimiento?';
    MSG_AGREGAR_TAG_EN_SEGUIMIENTO = 'El telev�a %s ha sido ingresado a Seguimiento';
    MSG_ACTUALIZAR_TAG_EN_SEGUIMIENTO = 'Se ha actualizado el telev�a: %s';
    MSG_ELIMINAR_TAG_EN_SEGUIMIENTO = 'Se ha eliminado el Televia %s de seguimiento';
    MSG_VALIDA_BIT_ACTIVADOS = 'Debe activar a lo menos un bit para poder ingresar el televia a seguimiento';
    MSG_VALIDA_TAG_VACIO = 'Debe ingresar el N� de Telev�a';
    MSG_ERROR_REGISTRANDO_TAG_EN_SEGUIMIENTO = 'Error registrando el telev�a en seguimiento';
    MSG_TELEVIA_YA_INGRESADO_SEGUIMIENTO = 'El telev�a se encuentra activo en seguimiento';
    MSG_ERROR_ELIMINANDO_TAG_EN_SEGUIMIENTO = 'Error al intentar eliminar un telev�a en seguimiento';
const
  Sql_EstaTagEnSeguimiento = 'SELECT dbo.EstaTagEnSeguimiento(''%s'')';
var
    EstaTagEnSeguimiento, CodigoRetorno, Respuesta: integer;
    MensajeError : string;
begin

    //Antes de guardar valido si el televia ingresado es correcto.
    if not ValidarEtiqueta(self.Caption, 'Telev�a Ingresado', PadL(txtEtiquetaFiltro.Text, 11, '0'), True) then begin
        exit;
    end;

    //pregunto si el tag esta en seguimiento
    EstaTagEnSeguimiento := QueryGetValueInt(DMConnections.BaseCAC, Format(Sql_EstaTagEnSeguimiento, [txtEtiqueta.Text]));
    //si presiono el boton Nuevo
	//if btnEditar.Caption = '&Nuevo' then begin    							//SS_660C_MCA_20131113
	//if btnEditar.Enabled = False then begin                                   //SS_660C_MCA_20140108 //SS_660C_MCA_20131113
    if not FEditar then begin                                                   //SS_660C_MCA_20140108
        //valida que haya ingresado la etiqueta del televia
        if txtEtiqueta.Text = EmptyStr then begin
            MsgBoxBalloon(MSG_VALIDA_TAG_VACIO, Caption, MB_ICONSTOP, txtEtiqueta);
            exit;
        end;
        //Valida que haya activado algun bit
        if (not chkSetMMIContactOperator.Checked) and not chkSetExceptionHandling.Checked then begin
            MsgBoxBalloon(MSG_VALIDA_BIT_ACTIVADOS, Caption, MB_ICONSTOP, chkSetMMIContactOperator);
            Exit;
        end;
        //si el tag no esta en seguimiento, se registra en la tabla TagEnSeguimiento
        if EstaTagEnSeguimiento = 0 then begin

            try
        
                with spAgregarTagEnSeguimiento, Parameters do begin
                      Close;
                      Refresh;
                      ParamByName('@Etiqueta').Value := txtEtiqueta.Text;
                      ParamByName('@ExceptionHandling').Value := chkSetExceptionHandling.Checked;
                      ParamByName('@MMIContactOperator').Value := chkSetMMIContactOperator.Checked;
                      ParamByName('@Usuario').Value := UsuarioSistema;
                      ParamByName('@MensajeError').Value := null;
                      ExecProc;

                      CodigoRetorno := Parameters.ParamByName('@RETURN_VALUE').Value;
                      MensajeError := Parameters.ParamByName('@MensajeError').Value;
                       case CodigoRetorno of
                            0 :
                              begin
                                 MsgBox(Format(MSG_AGREGAR_TAG_EN_SEGUIMIENTO, [txtEtiqueta.Text]), Caption, MB_OK + MB_ICONINFORMATION);
                                 grpNuevoTag.Visible := False;
                              end;
                       else
							//MsgBoxErr(MensajeError, EmptyStr ,Self.Caption, MB_ICONERROR);									//SS_660C_MCA_20131113
                            MsgBoxErr(MSG_ERROR_REGISTRANDO_TAG_EN_SEGUIMIENTO, MensajeError, Self.Caption, MB_ICONERROR);		//SS_660C_MCA_20131113
                            LimpiarDatos;                                       //SS_660C_MCA_20131113
                            exit;                                               //SS_660C_MCA_20131113
                       end;
                       Close;
                end;
            except
                on e:exception do begin
                    MsgBoxErr(MSG_ERROR_REGISTRANDO_TAG_EN_SEGUIMIENTO, e.Message, self.Caption, MB_ICONWARNING);
                    LimpiarDatos;                                               //SS_660C_MCA_20131113
                    exit;                                                       //SS_660C_MCA_20131113
                end;
            end;
        end
        else begin
            MsgBoxErr(MSG_TELEVIA_YA_INGRESADO_SEGUIMIENTO, Format('Televia N� %s', [txtEtiqueta.Text]) ,Self.Caption, MB_ICONERROR);
            LimpiarDatos;                                                       //SS_660C_MCA_20131113
            Exit;                                                               //SS_660C_MCA_20131113
        end;
    end
    else begin
        //Si el tag esta en seguimiento
        if EstaTagEnSeguimiento = 1 then begin
            //valido si hay algun bit activado
            if chkSetExceptionHandling.Checked or chkSetMMIContactOperator.Checked then begin
                //si es asi, actualizo la informacion del televia
                with spActualizarTagEnSeguimiento, Parameters do begin
                    Close;
                    Refresh;
                    ParamByName('@IdTagEnSeguimiento').Value := txtId.Text;
                    ParamByName('@Etiqueta').Value := txtEtiqueta.Text;
                    ParamByName('@ExceptionHandling').Value := chkSetExceptionHandling.Checked;
                    ParamByName('@MMIContactOperator').Value := chkSetMMIContactOperator.Checked;
                    ParamByName('@Usuario').Value := UsuarioSistema;
                    ParamByName('@MensajeError').Value := null;
                    ExecProc;

                    CodigoRetorno := Parameters.ParamByName('@RETURN_VALUE').Value;
                    MensajeError := Parameters.ParamByName('@MensajeError').Value;
                     case CodigoRetorno of
                          0 :
                            begin
                               MsgBox(Format(MSG_ACTUALIZAR_TAG_EN_SEGUIMIENTO, [txtEtiqueta.Text]), self.Caption, MB_ICONINFORMATION);
                            end;
                     else
                     	MsgBox(MensajeError, Self.Caption, MB_ICONERROR);
                        LimpiarDatos;                                           //SS_660C_MCA_20131113
                        exit;                                                   //SS_660C_MCA_20131113
                     end;
                     Close;
                end;
            end
            else
            begin
                //si no hay ningun bit activado, pregunta si esta seguro que desea eliminar el televia de seguimiento
                Respuesta := MsgBox(MSG_CONFIRMAR_ELIMINAR_TAG_EN_SEGUIMIENTO, 'Aviso', MB_ICONINFORMATION or MB_YESNO);
                if Respuesta = IDYES then begin
                    // la respuesta fue un si, por lo que proseguimos a eliminar el televia de seguimiento
                    try
                        with spEliminarTagEnSeguimiento, Parameters do begin
                            Close;
                            Refresh;
                            ParamByName('@IdTagEnSeguimiento').Value := spObtenerTagEnSeguimiento.FieldByName('IdTagEnSeguimiento').Value;;
                            ParamByName('@Usuario').Value := UsuarioSistema;
                            ParamByName('@MensajeError').Value := Null;
                            ExecProc;

                            CodigoRetorno := ParamByName('@RETURN_VALUE').Value;
                            MensajeError  := ParamByName('@MensajeError').Value;

                             case CodigoRetorno of
                                  0 :
                                  begin
                                     MsgBox(Format(MSG_ELIMINAR_TAG_EN_SEGUIMIENTO, [txtEtiqueta.Text]), self.Caption, MB_ICONINFORMATION);
                                  end;
                             else
                                MsgBox(MensajeError, Self.Caption, MB_ICONERROR);
                                LimpiarDatos;                                   //SS_660C_MCA_20131113
                                Exit;                                           //SS_660C_MCA_20131113
                             end;
                             Close;

                        end;
                    except
                        on e:exception do begin
                            MsgBoxErr(MSG_ERROR_ELIMINANDO_TAG_EN_SEGUIMIENTO, e.Message, Self.Caption, MB_ICONERROR);
                            LimpiarDatos;                                       //SS_660C_MCA_20131113
                            Exit;                                               //SS_660C_MCA_20131113
                        end;
                    end;
                end;
            end;
        end;
    end;
    //Limpia la informacion en pantalla
    LimpiarDatos;
    //y vuelve a cargar el listado de televias en seguimiento

    ObtenerTagEnSeguimiento(txtEtiquetaFiltro.Text, txtPatente.Text, deFechaAltaEHDesde.Date, deFechaAltaEHHasta.Date, deFechaBajaEHDesde.Date, deFechaBajaEHHasta.Date, deFechaAltaMMICODesde.Date, deFechaAltaMMICOHasta.Date, deFechaBajaMMICODesde.Date, deFechaBajaMMICOHasta.Date); 	//SS_660C_MCA_20131113
end;

procedure TTagEnSeguimientoForm.btnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TTagEnSeguimientoForm.dblTagEnSeguimientoClick(
  Sender: TObject);
begin
    if not spObtenerTagEnSeguimiento.EOF then begin
        if (not spObtenerTagEnSeguimiento.FieldByName('EH').Value) and not spObtenerTagEnSeguimiento.FieldByName('MMICO').Value then begin
            btnGuardar.Enabled := False;
            grpNuevoTag.Visible := False;
			//btnEditar.Caption := '&Nuevo';									//SS_660C_MCA_20131113
            //btnEditar.Enabled := True;										//SS_660C_MCA_20131113
            btnEditar.Enabled := False;											//SS_660C_MCA_20131113
            btnNuevo.Enabled := True;                                           //SS_660C_MCA_20131113
            txtEtiqueta.Enabled := True;
            Exit;
        end;

        txtId.Text := spObtenerTagEnSeguimiento.FieldByName('IdTagEnSeguimiento').Value;
        txtEtiqueta.Text := spObtenerTagEnSeguimiento.FieldByName('Etiqueta').Value;
    	txtEtiqueta.Enabled := False;
        chkSetExceptionHandling.Checked := IIf(spObtenerTagEnSeguimiento.FieldByName('EH').Value, True, False);
        chkSetMMIContactOperator.Checked := IIf(spObtenerTagEnSeguimiento.FieldByName('MMICO').Value, True, False);
		//btnEditar.Caption := '&Editar';										//SS_660C_MCA_20131113
        btnEditar.Enabled := True;
        btnNuevo.Enabled := False;                                              //SS_660C_MCA_20131113
        btnGuardar.Enabled := False;
        grpNuevoTag.Enabled := False;
        grpNuevoTag.Visible := False;
    end;
end;

procedure TTagEnSeguimientoForm.dblTagEnSeguimientoDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
var
    bmp, Blanco: TBitMap;

begin
    with Sender do begin
        if (Column.FieldName = 'EH') then begin
            DefaultDraw := False;
            Text        := '';
            canvas.FillRect(rect);
            bmp         := TBitMap.Create;
            Blanco      := TBitMap.Create;
            if (spObtenerTagEnSeguimiento.FieldByName('EH').AsBoolean ) then begin
                Imagenes.GetBitmap(10, Bmp);
            end else begin
                Imagenes.GetBitmap(0, Bmp);
            end;

            Imagenes.GetBitmap(5, Blanco);
            Blanco.Width := Rect.Right - Rect.Left;
            Canvas.Draw(Rect.Left, rect.Top, Blanco);
            Canvas.Draw((Blanco.Width div 2) - 8 + Rect.Left, Rect.Top, bmp); // Agrego un fondo balnco y centro el led en la grilla
            Bmp.Free;
            Blanco.Free;
        end;

        if (Column.FieldName = 'MMICO') then begin
            DefaultDraw := False;
            Text        := '';
            canvas.FillRect(rect);
            bmp         := TBitMap.Create;
            Blanco      := TBitMap.Create;

            if (spObtenerTagEnSeguimiento.FieldByName('MMICO').AsBoolean ) then begin
                Imagenes.GetBitmap(10, Bmp);
            end else begin
                Imagenes.GetBitmap(0, Bmp);
            end;

             Imagenes.GetBitmap(5, Blanco);
             Blanco.Width := Rect.Right - Rect.Left;
             Canvas.Draw(Rect.Left, rect.Top, Blanco);
             Canvas.Draw((Blanco.Width div 2) - 8 + Rect.Left, Rect.Top, bmp);
             Bmp.Free;
             Blanco.Free;
        end;
    end;
end;

procedure TTagEnSeguimientoForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree
end;

procedure TTagEnSeguimientoForm.FormKeyPress(Sender: TObject; var Key: Char);
begin
    if key = #27 then Close
end;

function TTagEnSeguimientoForm.Inicializar(txtCaption: String; MDIChild: boolean): Boolean;
Var
	S: TSize;
begin
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216
	if MDIChild then begin
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;
	CenterForm(Self);
	Caption := AnsiReplaceStr(txtCaption, '&', '');

    txtEtiquetaFiltro.SetFocus;
    KeyPreview := True;
	Result := true;
end;

procedure TTagEnSeguimientoForm.ObtenerTagEnSeguimiento(Etiqueta: String; Patente: string; FechaAltaEHDesde, FechaAltaEHHasta, FechaBajaEHDesde, FechaBajaEHHasta, FechaAltaMMICODesde, FechaAltaMMICOHasta, FechaBajaMMICODesde, FechaBajaMMICOHasta: TDateTime);	//SS_660C_MCA_20131113
resourcestring
    MSG_ERROR_LECTURA_TAG_EN_SEGUIMIENTO = 'No se pudo obtener el listado de Telev�as en Seguimiento';
	MSG_SIN_REGISTROS = 'No se encontraron registros para los criterios de b�squeda indicados';                                 // SS_660_CQU_20140307
begin
     	try
        with spObtenerTagEnSeguimiento, Parameters do begin
      			Close;
                refresh;
                ParamByName ('@Etiqueta').Value := IIF(Etiqueta=EmptyStr , Null, Etiqueta);
                ParamByName ('@Patente').Value := IIF(Patente=EmptyStr , Null, Patente);
                ParamByName ('@FechaAltaEHDesde').Value := IIF(FechaAltaEHDesde=NullDate , Null, FechaAltaEHDesde);				//SS_660C_MCA_20131113
                ParamByName ('@FechaAltaEHHasta').Value := IIF(FechaAltaEHHasta=NullDate , Null, FechaAltaEHHasta);             //SS_660C_MCA_20131113
                ParamByName ('@FechaBajaEHDesde').Value := IIF(FechaBajaEHDesde=NullDate , Null, FechaBajaEHDesde);             //SS_660C_MCA_20131113
                ParamByName ('@FechaBajaEHHasta').Value := IIF(FechaBajaEHHasta=NullDate , Null, FechaBajaEHHasta);             //SS_660C_MCA_20131113
                ParamByName ('@FechaAltaMMICODesde').Value := IIF(FechaAltaMMICODesde=NullDate , Null, FechaAltaMMICODesde);    //SS_660C_MCA_20131113
                ParamByName ('@FechaAltaMMICOHasta').Value := IIF(FechaAltaMMICOHasta=NullDate , Null, FechaAltaMMICOHasta);    //SS_660C_MCA_20131113
                ParamByName ('@FechaBajaMMICODesde').Value := IIF(FechaBajaMMICODesde=NullDate , Null, FechaBajaMMICODesde);    //SS_660C_MCA_20131113
                ParamByName ('@FechaBajaMMICOHasta').Value := IIF(FechaBajaMMICOHasta=NullDate , Null, FechaBajaMMICOHasta);    //SS_660C_MCA_20131113

            Open;
            if RecordCount = 0 then MsgBox(MSG_SIN_REGISTROS, Self.Caption, MB_OK + MB_ICONWARNING);                            // SS_660_CQU_20140307
        end;
	except
    on e: Exception do begin
			  MsgBoxErr(MSG_ERROR_LECTURA_TAG_EN_SEGUIMIENTO, e.message, Caption, MB_ICONSTOP);
              LimpiarDatos;
		end;
    end;
end;

procedure TTagEnSeguimientoForm.LimpiarDatos;
begin
    txtEtiqueta.Clear;
    chkSetExceptionHandling.Checked  := False;
    chkSetMMIContactOperator.Checked    := False;
	//btnEditar.Caption := '&Nuevo';											//SS_660C_MCA_20131113
    //btnEditar.Enabled := True;												//SS_660C_MCA_20131113
	btnNuevo.Enabled := True;                                                   //SS_660C_MCA_20131113
    btnEditar.Enabled := False;
    btnGuardar.Enabled := False;
    grpNuevoTag.Enabled := False;
    grpNuevoTag.Visible := False;

    deFechaAltaEHDesde.Date := NullDate;                                        //SS_660C_MCA_20131113
    deFechaAltaEHHasta.Date := NullDate;                                        //SS_660C_MCA_20131113
    deFechaBajaEHDesde.Date := NullDate;                                        //SS_660C_MCA_20131113
    deFechaBajaEHHasta.Date := NullDate;                                        //SS_660C_MCA_20131113
    deFechaAltaMMICODesde.Date := NullDate;                                     //SS_660C_MCA_20131113
    deFechaAltaMMICOHasta.Date := NullDate;                                     //SS_660C_MCA_20131113
    deFechaBajaMMICODesde.Date := NullDate;                                     //SS_660C_MCA_20131113
    deFechaBajaMMICOHasta.Date := NullDate;                                     //SS_660C_MCA_20131113
    FEditar := False;                                                           //SS_660C_MCA_20140108

    If spObtenerTagEnSeguimiento.Active then spObtenerTagEnSeguimiento.Close;
    txtEtiquetaFiltro.SetFocus;
end;

procedure TTagEnSeguimientoForm.dblTagsEnSeguimientoColumnsHeaderClick(Sender: TObject);
var order : string;
begin
	  if spObtenerTagEnSeguimiento.IsEmpty then Exit;

	  if TDBListExColumn(sender).Sorting = csAscending then begin
        TDBListExColumn(sender).Sorting := csDescending;
        order := ' ASC';
    end
    else begin
        TDBListExColumn(sender).Sorting := csAscending;
        order := ' DESC';
    end;
	  spObtenerTagEnSeguimiento.Sort := TDBListExColumn(Sender).FieldName + order;
end;

procedure TTagEnSeguimientoForm.btnLimpiarClick(Sender: TObject);
begin
	btnGuardar.Enabled := False;                                                //SS_660C_MCA_20131113
    btnNuevo.Enabled := True;                                                   //SS_660C_MCA_20131113
    btnEditar.Enabled := False;                                                 //SS_660C_MCA_20131113
    txtEtiqueta.Clear;                                                          //SS_660C_MCA_20131113
    chkSetExceptionHandling.Checked  := False;                                  //SS_660C_MCA_20131113
    chkSetMMIContactOperator.Checked    := False;                               //SS_660C_MCA_20131113
    txtId.Clear;                                                                //SS_660C_MCA_20131113
    txtEtiquetaFiltro.Clear;                                                    //SS_660C_MCA_20131113
    txtPatente.Clear;                                                           //SS_660C_MCA_20131113
    deFechaAltaEHDesde.Date := NullDate;                                        //SS_660C_MCA_20131113
    deFechaAltaEHHasta.Date := NullDate;                                        //SS_660C_MCA_20131113
    deFechaBajaEHDesde.Date := NullDate;                                        //SS_660C_MCA_20131113
    deFechaBajaEHHasta.Date := NullDate;                                        //SS_660C_MCA_20131113
    deFechaAltaMMICODesde.Date := NullDate;                                     //SS_660C_MCA_20131113
    deFechaAltaMMICOHasta.Date := NullDate;                                     //SS_660C_MCA_20131113
    deFechaBajaMMICODesde.Date := NullDate;                                     //SS_660C_MCA_20131113
    deFechaBajaMMICOHasta.Date := NullDate;                                     //SS_660C_MCA_20131113
    txtEtiquetaFiltro.SetFocus;                                                 //SS_660C_MCA_20131113
	FEditar := False;                                                           //SS_660C_MCA_20140108
end;

procedure TTagEnSeguimientoForm.btnNuevoClick(Sender: TObject);
begin
	grpNuevoTag.Caption := 'Nuevo Telev�a En Seguimiento';                      //SS_660C_MCA_20131113
    grpNuevoTag.Visible := True;                                                //SS_660C_MCA_20131113
    grpNuevoTag.Enabled := True;                                                //SS_660C_MCA_20131113
    btnGuardar.Enabled := True;                                                 //SS_660C_MCA_20131113
    btnEditar.Enabled := False;                                                 //SS_660C_MCA_20131113
    txtEtiqueta.Text := EmptyStr;                                               //SS_660C_MCA_20131113
    chkSetMMIContactOperator.Checked := False;                                  //SS_660C_MCA_20131113
    chkSetExceptionHandling.Checked := False;                                   //SS_660C_MCA_20131113
    txtEtiqueta.Enabled := True;                                                //SS_660C_MCA_20131113
    txtEtiqueta.SetFocus;                                                       //SS_660C_MCA_20131113
    FEditar := False;                                                           //SS_660C_MCA_20140108
end;

end.
