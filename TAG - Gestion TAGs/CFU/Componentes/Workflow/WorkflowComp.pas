unit WorkflowComp;

interface

Uses
	Classes, Windows, SysUtils, Messages, Controls, Scada, Math, Graphics,
	Util, ExtCtrls, DB, UtilDB, ADODB;

Type
	TWorkflowTask = class;

	TWorkflowItem = class(TCustomScadaShape)
	protected
		function  GetDesignMode: Boolean; virtual; abstract;
		procedure SetDesignMode(const Value: Boolean); virtual; abstract;
		function  GetSelected: Boolean; virtual; abstract;
		procedure SetSelected(const Value: Boolean); virtual; abstract;
		//
	public
		property DesignMode: Boolean Read GetDesignMode Write SetDesignMode;
		property Selected: Boolean Read GetSelected Write SetSelected;
	end;

	TWorkflowLink = class(TWorkflowItem)
	private
		FEndPoint: TPoint;
		FArranging: Boolean;
		FStartPoint: TPoint;
		FDesignMode: Boolean;
		FCondition: AnsiString;
		FEndTask: TWorkflowTask;
		FArrow1, FArrow2: TPoint;
		FStartTask: TWorkflowTask;
		FSizePanelW, FSizePanelE: TPanel;
		procedure SetCondition(const Value: AnsiString);
	protected
		procedure Paint; override;
		procedure CMHitTest(var Message: TCMHitTest); message CM_HITTEST;
		procedure Resize; override;
		procedure CreatePanels;
		procedure DestroyPanels;
		procedure ArrangePanels;
		function  GetDesignMode: Boolean; override;
		procedure SetDesignMode(const Value: Boolean); override;
		function  GetSelected: Boolean; override;
		procedure SetSelected(const Value: Boolean); override;
		procedure Arrange;
	public
		destructor Destroy; override;
		property   StartTask: TWorkflowTask Read FStartTask;
		property   EndTask: TWorkflowTask Read FEndTask;
		property   Condition: AnsiString Read FCondition Write SetCondition;
	end;

	TTaskType = (ttNormal, ttOptional, ttResult);
	TTaskStatus = (tsPending, tsWorking, tsDone);

	TWorkflowTask = class(TWorkflowItem)
	private
		FArea: Integer;
        FProceso: Integer;
		FFwdLinks: TList;
		FBackLinks: TList;
		FCaption: TCaption;
		FDuration: Integer;
		FEllapsed: Integer;
		FDragPoint: TPoint;
		FArranging: Boolean;
		FStatus: TTaskStatus;
		FDesignMode: Boolean;
		FTaskType: TTaskType;
		FOnMoved: TNotifyEvent;
		FPanelDragPoint: TPoint;
		FEndDateTime: TDateTime;
		FOnPredecessorsChanged: TNotifyEvent;
		FSizePanelNW, FSizePanelN, FSizePanelNE, FSizePanelW,
		  FSizePanelE, FSizePanelSW, FSizePanelS, FSizePanelSE: TPanel;
		procedure SetCaption(const Value: TCaption);
		function  GetPredCount: Integer;
		procedure SetArea(const Value: Integer);
		procedure SetProceso(const Value: Integer);
		procedure SetTaskType(const Value: TTaskType);
		function  GetPredecessor(Index: Integer): TWorkflowTask;
		procedure SetDuration(const Value: Integer);
		procedure SetEllapsed(const Value: Integer);
	    procedure SetStatus(const Value: TTaskStatus);
    	procedure SetEndDateTime(const Value: TDateTime);
	protected
		procedure Paint; override;
		Function  GetDesignMode: Boolean; override;
		procedure SetDesignMode(const Value: Boolean); override;
		procedure SetSelected(const Value: Boolean); override;
		function  GetSelected: Boolean; override;
		procedure Resize; override;
		procedure CreatePanels;
		procedure DestroyPanels;
		procedure ArrangePanels;
		procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
		procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
		procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
		procedure DragOver(Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean); override;
		procedure PanelMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
		procedure PanelMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
	public
		constructor Create(AOwner: TComponent); override;
		destructor Destroy; override;
		function  AddPredecessor(Task: TWorkflowTask): TWorkflowLink;
		function  RemovePredecessor(Task: TWorkflowTask): Boolean;
		function  IsPredecessor(Task: TWorkflowTask): Boolean;
		function  IsDirectPredecessor(Task: TWorkflowTask): Boolean;
		procedure DragDrop(Source: TObject; X, Y: Integer); override;
		//
		property Selected: Boolean Read GetSelected Write SetSelected;
		property PredecessorCount: Integer Read GetPredCount;
		Property Predecessors[Index: Integer]: TWorkflowTask Read GetPredecessor;
		Property DesignMode;
	published
		property DragKind;
		property Caption: TCaption Read FCaption Write SetCaption;
		property Area: Integer read FArea Write SetArea default 0;
		property Proceso: Integer read FProceso Write SetProceso default 0;
		property TaskType: TTaskType Read FTaskType Write SetTaskType default ttNormal;
		property MaxDuration: Integer Read FDuration Write SetDuration;
		property OnMoved: TNotifyEvent Read FOnMoved Write FOnMoved;
		property OnPredecessorsChanged: TNotifyEvent Read FOnPredecessorsChanged Write FOnPredecessorsChanged;
		property Ellapsed: Integer  Read FEllapsed Write SetEllapsed;
		property Status: TTaskStatus Read FStatus Write SetStatus;
		property EndDateTime: TDateTime Read FEndDateTime Write SetEndDateTime;
	end;

	TWorkflowViewer = class(TScadaViewer)
	public
		procedure InsertItem(AItem: TScadaItem; Index: Integer = -1); override;
	end;

	TWorkflowDisplay = class(TCustomControl)
	private
		FWorkflowID: Integer;
		FServiceOrder: Integer;
		FViewer: TWorkflowViewer;
		FWorkflowVersion: Integer;
		FConnection: TADOConnection;
		procedure SetWorkflowID(const Value: Integer);
		procedure SetServiceOrder(const Value: Integer);
		procedure SetWorkflowVersion(const Value: Integer);
		procedure SetConnection(const Value: TADOConnection);
	protected
		procedure Loaded; override;
		procedure Notification(AComponent: TComponent; Operation: TOperation); override;
	public
		procedure   Reload; virtual;
		constructor Create(AOwner: TComponent); override;
	published
		property Align;
		property Anchors;
		property Cursor;
		property PopupMenu;
		property WorkflowID: Integer Read FWorkflowID Write SetWorkflowID;
		property ServiceOrder: Integer Read FServiceOrder Write SetServiceOrder;
		property Connection: TADOConnection read FConnection write SetConnection;
		property WorkflowVersion: Integer Read FWorkflowVersion Write SetWorkflowVersion;
	end;

function  TimeAsText(Time: TDateTime): AnsiString;
procedure LoadWorkflow(WorkflowID, Version: Integer; Connection: TADOConnection; Viewer: TWorkflowViewer);
procedure LoadServiceOrder(ServiceOrder, WorkflowID: Integer; Connection: TADOConnection; Viewer: TWorkflowViewer);

implementation

Function TimeAsText(Time: TDateTime): AnsiString;
resourcestring
	MSG_DIA = 'd�a';
	MSG_DIAS = 'd�as';
	MSG_HORA = 'hora';
	MSG_HORAS = 'horas';
	MSG_MINUTO = 'minuto';
	MSG_MINUTOS = 'minutos';
Var
	Days, Hours, Minutes: Integer;
begin
	Days    := Trunc(Time);
	Hours   := Hour(Time);
	Minutes := Minute(Time);
	Result := '';
	if Days > 0 then begin
		Result := IntToStr(Days) + ' ';
		if Days = 1 then Result := Result + MSG_DIA else Result := Result + MSG_DIAS;
	end;
	if Hours > 0 then begin
		if Result <> '' then Result := Result + ', ';
		Result := Result + IntToStr(Hours) + ' ';
		if Hours = 1 then Result := Result + MSG_HORA else Result := Result + MSG_HORAS;
	end;
	if Minutes > 0 then begin
		if Result <> '' then Result := Result + ', ';
		Result := Result + IntToStr(Minutes) + ' ';
		if Minutes = 1 then Result := Result + MSG_MINUTO else Result := Result + MSG_MINUTOS;
	end;
	if Result = '' then Result := '0 ' + MSG_MINUTOS;
end;

procedure LoadWorkflow(WorkflowID, Version: Integer; Connection: TADOConnection; Viewer: TWorkflowViewer);

	procedure AddTask(D: TDataset);
	Var
		Task: TWorkflowTask;
		StrStream:TStringStream;
		BinStream: TMemoryStream;
	begin
		Task := TWorkflowTask.Create(nil);
		Task.Viewer := Viewer;
		StrStream := TStringStream.Create(D.FieldByName('Layout').AsString);
		BinStream := TMemoryStream.Create;
		try
			ObjectTextToBinary(StrStream, BinStream);
			BinStream.Seek(0, soFromBeginning);
			BinStream.ReadComponent(Task);
		except
		end;
		Task.Tag := D.FieldByName('CodigoTarea').Asinteger;
		Task.Caption := TrimRight(D.FieldByName('Descripcion').AsString);
		Task.MaxDuration := D.FieldByName('DuracionMaxima').AsInteger;
		Task.Area := D.FieldByName('CodigoArea').AsInteger;
        Task.Proceso := D.FieldByName('CodigoProceso').AsInteger;
		if D.FieldByName('TipoTarea').AsString = 'R' then Task.TaskType := ttResult
		  else if D.FieldByName('TipoTarea').AsString = 'O' then Task.TaskType := ttOptional
		  else Task.TaskType := ttNormal;
		BinStream.Free;
		StrStream.Free;
	end;

	Function FindTask(TaskID: Integer): TWorkflowTask;
	Var
		i: Integer;
	begin
		for i := 0 to Viewer.ItemCount - 1 do begin
			if (Viewer.Items[i] is TWorkflowTask) and
			  (TWorkflowTask(Viewer.Items[i]).Tag = TaskID) then begin
				Result := TWorkflowTask(Viewer.Items[i]);
				Exit;
			end;
		end;
		Result := nil;
	end;

	procedure AddLink(D: TDataset);
	Var
		L: TWorkflowLink;
	begin
		L := FindTask(D.FieldByName('CodigoTarea').AsInteger).AddPredecessor(
		  FindTask(D.FieldByName('CodigoTareaAnterior').AsInteger));
		L.Condition := Trim(D.FieldByName('Condicion').AsString);
	end;

Var
	Qry: TADOQuery;
begin
	// Quitamos los elementos actuales
	While Viewer.ItemCount > 0 do Viewer.Items[0].Free;
	// Calculamos la versi�n, si corresponde
	if Version <= 0 then Version := QueryGetValueInt(Connection,
	  'SELECT VersionActual FROM Workflows WHERE CodigoWorkflow = ' + IntToStr(WorkflowID));
	// Armamos las tareas
	Qry := TAdoQuery.Create(nil);
	try
		// Cargamos las tareas
		Qry.Connection := Connection;
		Qry.SQL.Text :=
          'SELECT ' +
          ' Tareasworkflow.CodigoWorkFlow, Tareasworkflow.Version, Tareasworkflow.CodigoTarea, ' +
          ' Tareasworkflow.TipoTarea, Tareasworkflow.CodigoArea, Tareasworkflow.Layout, ' +
          ' Tareasworkflow.DuracionMaxima, TareasWorkflow.Descripcion, Tareasworkflow.CodigoProceso ' +
          'FROM TareasWorkflow WHERE ' +
          ' CodigoWorkflow = ' + IntToStr(WorkflowID) +
		  'AND Version = ' + IntToStr(Version);
		Qry.Open;
		While not Qry.Eof do begin
			AddTask(Qry);
			Qry.Next;
		end;
		Qry.Close;
		// Cargamos los v�nculos
		Qry.SQL.Text := 'SELECT * FROM SecuenciaTareasWorkflow (NOLOCK) WHERE CodigoWorkflow = ' + IntToStr(WorkflowID) +
		  'AND Version = ' + IntToStr(Version);
		Qry.Open;
		While not Qry.Eof do begin
			AddLink(Qry);
			Qry.Next;
		end;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

procedure LoadServiceOrder(ServiceOrder, WorkflowID: Integer; Connection: TADOConnection; Viewer: TWorkflowViewer);
Var
	i: Integer;
	Qry: TADOQuery;
	Version: Integer;
	Tarea: TWorkflowTask;
	Pasado, Total: Integer;
begin
	// Existe lo que nos piden?
	if QueryGetValueInt(Connection, 'SELECT 1 FROM OrdenesServicio (NOLOCK) WHERE' +
	  ' CodigoOrdenServicio = ' + IntToStr(ServiceOrder)) <> 1 then begin
		// No, no existe. Vaciamos todo y nos vamos
		While Viewer.ItemCount > 0 do Viewer.Items[0].Free;
		Exit;
	end;
	// Calculamos la versi�n
	Version := QueryGetValueInt(Connection, 'SELECT Version FROM OrdenesServicio (NOLOCK) WHERE' +
	  ' CodigoOrdenServicio = ' + IntToStr(ServiceOrder));

	// Armamos las tareas de acuerdo al workflow
	LoadWorkflow(WorkflowID, Version, Connection, Viewer);
	// Buscamos las tareas de la orden de servicio
	Qry := TAdoQuery.Create(nil);
	Qry.Connection := Connection;
	Qry.SQL.Text :=
      'SELECT OrdenesServicio.CodigoWorkflow, OrdenesServicio.Version, OrdenesServicioTareas.*, TareasWorkflow.DuracionMaxima, TareasWorkflow.CodigoProceso' +
	  ' FROM ' +
      ' OrdenesServicio (NOLOCK), OrdenesServicioTareas (NOLOCK), TareasWorkflow (NOLOCK) ' +
	  ' WHERE ' + 
      ' OrdenesServicio.CodigoOrdenServicio = ' + IntToStr(ServiceOrder) +
      ' AND OrdenesServicio.CodigoOrdenServicio = OrdenesServicioTareas.CodigoOrdenServicio' +
//	  ' AND OrdenesServicioTareas.CodigoWorkflow = ' + IntToStr(WorkflowID) +
//	  ' AND OrdenesServicioTareas.Version = ' + IntToStr(Version) +
	  ' AND TareasWorkflow.CodigoWorkflow = OrdenesServicio.CodigoWorkflow ' +
	  ' AND TareasWorkflow.Version = OrdenesServicio.Version ' +
	  ' AND TareasWorkflow.CodigoTarea = OrdenesServicioTareas.CodigoTarea';

	// Quitamos las tareas que est�n "de m�s" (porque al workflow le hayan
	// agregado posteriormente otras tareas)
	try
		Qry.Open;
		i := 0;
		While i < Viewer.ItemCount do begin
			if (Viewer.Items[i] is TWorkflowTask) then begin
				Tarea := TWorkflowTask(Viewer.Items[i]);
				if Qry.Locate('CodigoTarea', Tarea.Tag, []) then begin
					if Qry.FieldByName('Estado').AsString = 'T' then begin
						Tarea.Brush.Color := $00CCFFCD;
					end else if not Qry.FieldByName('FechaHoraInicio').IsNull then begin
						// Calculamos el tiempo transcurrido
						Pasado := Round((Now - Qry.FieldByName('FechaHoraInicio').AsDateTime) * 24 * 60);
						Total := (Qry.FieldByName('DuracionMaxima').AsInteger);
						if (Pasado > Total) then Pasado := Total;
						if (Total > 0) then Tarea.Ellapsed := Round(Pasado / Total * 100);
						Tarea.MaxDuration := (Total - Pasado);
					end;
					Tarea.EndDateTime := Qry.FieldByName('FechaHoraFin').AsDateTime;
                    Tarea.Proceso	  := Qry.FieldByName('CodigoProceso').AsInteger;
					if Qry.FieldByName('FechaHoraInicio').IsNull then
						Tarea.Status := tsPending
					else if Qry.FieldByName('FechaHoraFin').IsNull then
						Tarea.Status := tsWorking
					else begin
						Tarea.Status := tsDone;
					end;
					Inc(i);
				end else begin
					Tarea.Free;
					i := 0;
				end;
			end else begin
				Inc(i);
			end;
		end;
	finally
		Qry.Close;
		Qry.Free;
	end;
	// Les quitamos las condiciones a los links, para que aparezcan dibujados
	// normalmente.
	for i := 0 to Viewer.ItemCount - 1 do begin
		if Viewer.Items[i] is TWorkflowLink then
		  TWorkflowLink(Viewer.Items[i]).Condition := '';
	end;
end;

{ TWorkflowViewer }

procedure TWorkflowViewer.InsertItem(AItem: TScadaItem; Index: Integer);
begin
	if not (AItem is TWorkflowItem) then begin
		raise Exception.Create('Cannot Insert ' +
		  AItem.ClassName + ' into ' + ClassName);
	end;
	inherited;
end;


{ TWorkflowTask }

function TWorkflowTask.AddPredecessor(Task: TWorkflowTask): TWorkflowLink;
Var
	Link: TWorkflowLink;
begin
	Result := nil;
	if (Task = Self) or IsDirectPredecessor(Task) then Exit;
	Link := TWorkflowLink.Create(Owner);
	Link.Viewer := Viewer;
	Link.FStartTask := Task;
	Link.FEndTask := Self;
	FBackLinks.Add(Link);
	Task.FFwdLinks.Add(Link);
	Link.Arrange;
	if not (csDestroying in ComponentState) and Assigned(FOnPredecessorsChanged) then
	  FOnPredecessorsChanged(Self);
	Result := Link;
end;

procedure TWorkflowTask.ArrangePanels;
begin
	FSizePanelNW.SetBounds(Left - 2, Top - 2, 5, 5);
	FSizePanelN.SetBounds(Left - 2 + Width div 2, Top - 2, 5, 5);
	FSizePanelNE.SetBounds(Left - 2 + Width, Top - 2, 5, 5);
	FSizePanelW.SetBounds(Left - 2, Top - 2 + Height div 2, 5, 5);
	FSizePanelE.SetBounds(Left - 2 + Width, Top - 2 + Height div 2, 5, 5);
	FSizePanelSW.SetBounds(Left - 2, Top - 2 + Height, 5, 5);
	FSizePanelS.SetBounds(Left - 2 + Width div 2, Top - 2 + Height, 5, 5);
	FSizePanelSE.SetBounds(Left - 2 + Width, Top - 2 + Height, 5, 5);
end;

constructor TWorkflowTask.Create(AOwner: TComponent);
begin
	inherited;
	Shape := sstRoundRect;
	Brush.Color := $D9FFFF;
	FFwdLinks := TList.Create;
	FBackLinks := TList.Create;
	FArranging := False;
end;

procedure TWorkflowTask.CreatePanels;

	function CreatePanel: TPanel;
	begin
		Result := TPanel.Create(Self);
		Result.BevelInner := bvNone;
		Result.BevelOuter := bvNone;
		Result.Color := clBlack;
		Result.SetBounds(-10, -10, 6, 6);
		Result.Parent := Viewer;
		Result.OnMouseMove := PanelMouseMove;
	end;

begin
	FSizePanelNW := CreatePanel; FSizePanelNW.Cursor := crSizeNWSE;
	FSizePanelN  := CreatePanel; FSizePanelN.Cursor := crSizeNS;
	FSizePanelNE := CreatePanel; FSizePanelNE.Cursor := crSizeNESW;
	FSizePanelW  := CreatePanel; FSizePanelW.Cursor := crSizeWE;
	FSizePanelE  := CreatePanel; FSizePanelE.Cursor := crSizeWE;
	FSizePanelSW := CreatePanel; FSizePanelSW.Cursor := crSizeNESW;
	FSizePanelS  := CreatePanel; FSizePanelS.Cursor := crSizeNS;
	FSizePanelSE := CreatePanel; FSizePanelSE.Cursor := crSizeNWSE;
	ArrangePanels;
end;

destructor TWorkflowTask.Destroy;
Var
	Link: TWorkflowLink;
begin
	While FBackLinks.Count > 0 do RemovePredecessor(Predecessors[0]);
	While FFwdLinks.Count > 0 do begin
		Link := TWorkflowLink(FFwdLinks[0]);
		Link.FStartTask := nil;
		if Assigned(Link.FEndTask) then begin
			Link.FEndTask.FBackLinks.Remove(Link);
			Link.FEndTask := nil;
		end;
		FFwdLinks.Remove(Link); 
		Link.Free;
	end;
	Selected := False;
	FFwdLinks.Free;
	FBackLinks.Free;
	inherited;
end;

procedure TWorkflowTask.DestroyPanels;
begin
	FSizePanelNW.Free;  FSizePanelNW := nil;
	FSizePanelN.Free;   FSizePanelN := nil;
	FSizePanelNE.Free;  FSizePanelNE := nil;
	FSizePanelW.Free;   FSizePanelW := nil;
	FSizePanelE.Free;   FSizePanelE := nil;
	FSizePanelSW.Free;  FSizePanelSW := nil;
	FSizePanelS.Free;   FSizePanelS := nil;
	FSizePanelSE.Free;  FSizePanelSE := nil;
end;

procedure TWorkflowTask.DragDrop(Source: TObject; X, Y: Integer);
begin
	inherited;
	AddPredecessor(TWorkflowTask(Source)); 
end;

procedure TWorkflowTask.DragOver(Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
begin
	Accept :=
	  (Source <> Self)
	  and (Source is TWorkflowTask)
	  and not TWorkflowTask(Source).IsPredecessor(Self);
end;

function TWorkflowTask.GetDesignMode: Boolean;
begin
	Result := FDesignMode;
end;

function TWorkflowTask.GetPredCount: Integer;
begin
	Result := FBackLinks.Count;
end;

function TWorkflowTask.GetPredecessor(Index: Integer): TWorkflowTask;
begin
	Result := TWorkflowLink(FBackLinks[Index]).StartTask;
end;

function TWorkflowTask.GetSelected: Boolean;
begin
	Result := Assigned(FSizePanelNW);
end;

function TWorkflowTask.IsDirectPredecessor(Task: TWorkflowTask): Boolean;
Var
	i: Integer;
	T: TWorkflowTask;
begin
	for i := 0 to PredecessorCount - 1 do begin
		T := Predecessors[i];
		if (T = Task) then begin
			Result := True;
			Exit;
		end;
	end;
	Result := False;
end;

function TWorkflowTask.IsPredecessor(Task: TWorkflowTask): Boolean;
Var
	i: Integer;
	T: TWorkflowTask;
begin
	for i := 0 to PredecessorCount - 1 do begin
		T := Predecessors[i];
		if (T = Task) or (T.IsPredecessor(Task)) then begin
			Result := True;
			Exit;
		end;
	end;
	Result := False;
end;

procedure TWorkflowTask.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
	inherited;
	if not FDesignMode or (Button <> mbLeft) then Exit;
	// Comenzamos manualmente el Drag o la creaci�n de un nuevo link
	if (Shift = [ssLeft]) then begin
		FDragPoint := Point(X, Y);
		MouseCapture := True;
	end else if (Shift = [ssLeft, ssCtrl]) then begin
		BeginDrag(True, 0);
	end;
end;

procedure TWorkflowTask.MouseMove(Shift: TShiftState; X, Y: Integer);
Var
	dx, dy, i: Integer;
	Item: TWorkflowItem;
begin
	inherited;
	if FDesignMode and MouseCapture and Selected and (Shift = [ssLeft]) then begin
		dx := X - FDragPoint.X;
		dy := Y - FDragPoint.Y;
		// Chequeamos que ning�n componente se pase para la izquierda o para arriba
		for i := 0 to Viewer.ItemCount - 1 do begin
			Item := TWorkflowItem(Viewer.Items[i]);
			if Item.Selected then begin
				if Viewer.Items[i].Left + dx < 0 then dx := - Viewer.Items[i].Left;
				if Viewer.Items[i].Top  + dy < 0 then dy := - Viewer.Items[i].Top;
			end;
		end;
		// Movemos los componentes
		if (dx = 0) and (dy = 0) then Exit;
		for i := 0 to Viewer.ItemCount - 1 do begin
			Item := TWorkflowItem(Viewer.Items[i]);
			if Item.Selected then begin
				Item.SetBounds(Item.Left + dx, Item.Top + dy, Item.Width, Item.Height);
				Item.Update;
			end;
		end;
		if not (csDestroying in ComponentState) and Assigned(OnMoved) then OnMoved(Self);
	end;
end;

procedure TWorkflowTask.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
	inherited;
	if FDesignMode and MouseCapture and (Button = mbLeft) then begin
		MouseCapture := False;
	end;
end;

procedure TWorkflowTask.Paint;

	Function GetRect: TRect;
	begin
		Result := ClientRect;
		Inc(Result.Left, Width div 10);
		Dec(Result.Right, Width div 10);
		Inc(Result.Top, Height div 10);
		Dec(Result.Bottom, Height div 10);
	end;

Var
	R: TRect;
	Rgn: HRgn;
	x, h: Integer;
	FText: AnsiString;
begin
	// Dibujamos el fondo
	if (FEllapsed > 0) and (FEllapsed < 100) then begin
		Rgn := CreateRoundRectRgn(Left, Top, Left + Width + 1, Top + Height + 1, Width div 4, Height div 4);
		SelectClipRgn(Canvas.Handle, Rgn);
		x := Round(Width * FEllapsed / 100);
		Canvas.Brush.Color := RGB(255, 180, 180);
		Canvas.FillRect(Rect(0, 0, x, Height));
		Canvas.Brush.Color := Brush.Color;
		Canvas.FillRect(Rect(x, 0, Width, Height));
		Canvas.Brush.Style := bsClear;
		SelectClipRgn(Canvas.Handle, 0);
		DeleteObject(Rgn);
	end else begin
		Canvas.Brush.Style := bsSolid;
		if FEllapsed >= 100 then Canvas.Brush.Color := RGB(255, 180, 180)
		  else Canvas.Brush.Color := Brush.Color;
	end;
	if TaskType = ttOptional then Canvas.Pen.Style := psDot
	  else Canvas.Pen.Style := psSolid;
	Canvas.RoundRect(0, 0, Width, Height, Width div 4, Height div 4);
	if TaskType = ttResult then Canvas.RoundRect(2, 2, Width - 2, Height - 2,
	  Width div 4, Height div 4);
	// Dibujamos el texto. El texto depende del estado de la tarea
	case Status of
		tsPending, tsWorking:
			FText := FCaption + CRLF + '(' + TimeAsText(MaxDuration / 24 / 60) + ')';
		tsDone:
			FText := FCaption + CRLF + '(' + FormatShortDate(EndDateTime) + ' ' +
			  FormatTime(EndDateTime) + ')';
	end;
	// El color del font depende del estado de la tarea
	if FDesignMode then begin
		Canvas.Font.Color := clBlack
	end else begin
		if Status = tsPending then Canvas.Font.Color := clSilver
		  else Canvas.Font.Color := clBlack;
	end;
	// Dibujamos el texto
	R := GetRect;
	Canvas.Font.Name  := 'Arial';
	Canvas.Font.Style := [fsBold];
	Canvas.Font.Size  := Round(9 * Viewer.ScaleFactor);
	SetBkMode(Canvas.Handle, TRANSPARENT);
	h := DrawText(Canvas.Handle, PChar(FText), Length(FText), R,
	  DT_CENTER or DT_WORDBREAK or DT_CALCRECT);
	R := GetRect;
	OffsetRect(R, 0, (Height - h) div 2 - R.Top);
	DrawText(Canvas.Handle, PChar(FText), Length(FText), R,
	  DT_CENTER or DT_WORDBREAK);
end;

procedure TWorkflowTask.PanelMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
	if Shift <> [ssLeft] then Exit; 
	FPanelDragPoint := Point(X, Y);
end;

procedure TWorkflowTask.PanelMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
Var
	Pnl: TPanel;
	dx, dy: Integer;
begin
	Pnl := TPanel(Sender);
	if Shift <> [ssLeft] then Exit;
	dx := X - FPanelDragPoint.X;
	dy := Y - FPanelDragPoint.Y;
	// Cambiamos el tama�o
	if Pnl = FSizePanelNW then begin
		if Width - dx < 10 then dx := Width - 10;
		if Height - dy < 10 then dy := Height - 10;
		SetBounds(Left + dx, Top + dy, Width - dx, Height - dy);
	end else if Pnl = FSizePanelNE then begin
		if Width + dx < 10 then dx := 10 - Width;
		if Height - dy < 10 then dy := Height - 10;
		SetBounds(Left, Top + dy, Width + dx, Height - dy);
	end else if Pnl = FSizePanelSW then begin
		if Width - dx < 10 then dx := Width - 10;
		if Height + dy < 10 then dy := 10 - Height;
		SetBounds(Left + dx, Top, Width - dx, Height + dy);
	end else if Pnl = FSizePanelSE then begin
		if Width + dx < 10 then dx := 10 - Width;
		if Height + dy < 10 then dy := 10 - Height;
		SetBounds(Left, Top, Width + dx, Height + dy);
	end else if Pnl = FSizePanelN then begin
		if Height - dy < 10 then dy := Height - 10;
		SetBounds(Left, Top + dy, Width, Height - dy);
	end else if Pnl = FSizePanelW then begin
		if Width - dx < 10 then dx := Width - 10;
		SetBounds(Left + dx, Top, Width - dx, Height);
	end else if Pnl = FSizePanelE then begin
		if Width + dx < 10 then dx := 10 - Width;
		SetBounds(Left, Top, Width + dx, Height);
	end else if Pnl = FSizePanelS then begin
		if Height + dy < 10 then dy := 10 - Height;
		SetBounds(Left, Top, Width, Height + dy);
	end;
	if Assigned(FOnMoved) and not (csDestroying in ComponentState) then FOnMoved(Self);
end;

function TWorkflowTask.RemovePredecessor(Task: TWorkflowTask): Boolean;
Var
	i: Integer;
	Link: TWorkflowLink;
begin
	for i := 0 to FBackLinks.Count - 1 do begin
		Link := TWorkflowLink(FBackLinks[i]);
		if Link.StartTask = Task then begin
			Link.FEndTask := nil;
			if Assigned(Link.FStartTask) then begin
				Link.FStartTask.FFwdLinks.Remove(Link);
				Link.FStartTask := nil;
			end;
			FBackLinks.Remove(Link);
			Link.Free;
			if not (csDestroying in ComponentState) and Assigned(FOnPredecessorsChanged) then
			  FOnPredecessorsChanged(Self);
			Result := True;
			Exit;
		end;
	end;
	Result := False;
end;

procedure TWorkflowTask.Resize;
Var
	i: Integer;
begin
	inherited;
	// Arreglamos los links para atr�s
	for i := 0 to FBackLinks.Count - 1 do TWorkflowLink(FBackLinks[i]).Arrange;
	// Arreglamos los links para adelante
	for i := 0 to FFwdLinks.Count - 1 do TWorkflowLink(FFwdLinks[i]).Arrange;
	// Arreglamos los paneles de selecci�n
	if Selected then ArrangePanels;
end;

procedure TWorkflowTask.SetArea(const Value: Integer);
begin
	if FArea = Value then Exit;
	FArea := Value;
	Invalidate;
end;

procedure TWorkflowTask.SetProceso(const Value: Integer);
begin
	if FProceso = Value then Exit;
	FProceso := Value;
	Invalidate;
end;

procedure TWorkflowTask.SetCaption(const Value: TCaption);
begin
	if FCaption = Value then Exit;
	FCaption := Value;
	Invalidate;
end;

procedure TWorkflowTask.SetDesignMode(const Value: Boolean);
begin
	if FDesignMode = Value then Exit;
	FDesignMode := Value;
end;

procedure TWorkflowTask.SetDuration(const Value: Integer);
begin
	if FDuration = Value then Exit;
	FDuration := Value;
	Invalidate;
end;

procedure TWorkflowTask.SetEllapsed(const Value: Integer);
begin
	if FEllapsed = Value then Exit;
	FEllapsed := Value;
	Invalidate;
end;

procedure TWorkflowTask.SetEndDateTime(const Value: TDateTime);
begin
	if FEndDateTime = Value then Exit;
	FEndDateTime := Value;
	Invalidate;
end;

procedure TWorkflowTask.SetSelected(const Value: Boolean);
begin
	if (Selected = Value) then Exit;
	if Value then CreatePanels else DestroyPanels;
end;

procedure TWorkflowTask.SetStatus(const Value: TTaskStatus);
begin
	if FStatus = Value then Exit;
	FStatus := Value;
	Invalidate;
end;

procedure TWorkflowTask.SetTaskType(const Value: TTaskType);
begin
	if FTaskType = Value then Exit;
	FTaskType := Value;
	Invalidate;
end;


{ TWorkflowLink }
procedure TWorkflowLink.Arrange;

	procedure CalcArrows(P1, P2: TPoint; Var A1, A2: TPoint);
	Var
		Hip, Angle, Angle1, Angle2: Double;
	begin
		Hip := SQrt(Power(P2.X - P1.X, 2) + Power(P2.Y - P1.Y, 2));
		if P2.Y >= P1.Y then Angle  := DegToRad(90) + ArcSin((P2.X - P1.X) / Hip)
		  else Angle  := DegToRad(270) - ArcSin((P2.X - P1.X) / Hip);
		Angle1 := Angle + DegToRad(20);
		Angle2 := Angle - DegToRad(20);
		A1 := Point(Round(P2.X + 10 * Cos(Angle1)), Round(P2.Y - 10 * Sin(Angle1)));
		A2 := Point(Round(P2.X + 10 * Cos(Angle2)), Round(P2.Y - 10 * Sin(Angle2)));
	end;

Var
	SP, EP: TPoint;
begin
	if not Assigned(FStartTask) or not Assigned(FEndTask) then Exit;
	if FArranging then Exit;
	FArranging := True;
	if EndTask.WorldLeft > (StartTask.WorldLeft + StartTask.WorldWidth) then begin
		// El predecesor est� a la izquierda
		SP.X 	:= StartTask.WorldLeft + StartTask.WorldWidth;
		SP.Y 	:= StartTask.WorldTop + (StartTask.WorldHeight div 2);
		EP.X 	:= EndTask.WorldLeft;
		EP.Y 	:= EndTask.WorldTop + (EndTask.WorldHeight div 2);
	end else if StartTask.WorldLeft > (EndTask.WorldLeft + EndTask.WorldWidth) then begin
		// El predecesor est� a la derecha
		SP.X 	:= StartTask.WorldLeft;
		SP.Y 	:= StartTask.WorldTop + (StartTask.WorldHeight div 2);
		EP.X 	:= EndTask.WorldLeft + EndTask.WorldWidth;
		EP.Y 	:= EndTask.WorldTop + (EndTask.WorldHeight div 2);
	end else if StartTask.WorldTop > (EndTask.WorldTop + EndTask.WorldHeight) then begin
		// El predecesor est� abajo
		SP.X 	:= StartTask.WorldLeft + StartTask.WorldWidth div 2;
		SP.Y 	:= StartTask.WorldTop;
		EP.X 	:= EndTask.WorldLeft + EndTask.WorldWidth div 2;
		EP.Y 	:= EndTask.WorldTop + EndTask.WorldHeight;
	end else if EndTask.WorldTop > (StartTask.WorldTop + StartTask.WorldHeight) then begin
		// El predecesor est� arriba
		SP.X 	:= StartTask.WorldLeft + StartTask.WorldWidth div 2;
		SP.Y 	:= StartTask.WorldTop + StartTask.WorldHeight;
		EP.X 	:= EndTask.WorldLeft + EndTask.WorldWidth div 2;
		EP.Y 	:= EndTask.WorldTop;
	end else begin
		Visible := False;
		FArranging := False;
		Exit;
	end;
	// Actualizamos el Link
	WorldTop    := Min(SP.Y, EP.Y) - 5;
	WorldLeft   := Min(SP.X, EP.X) - 5;
	WorldWidth  := Round(Abs(SP.X - EP.X)) + 10;
	WorldHeight := Round(Abs(SP.Y - EP.Y)) + 10;
	// Calculamos las coordenadas de las puntas
	FStartPoint.X 	:= Round(SP.X * Viewer.ScaleFactor) - Left;
	FStartPoint.Y 	:= Round(SP.Y * Viewer.ScaleFactor) - Top;
	FEndPoint.X 	:= Round(EP.X * Viewer.ScaleFactor) - Left;
	FEndPoint.Y 	:= Round(EP.Y * Viewer.ScaleFactor) - Top;
	CalcArrows(SP, EP, FArrow1, FArrow2);
	FArrow1.X       := Round(FArrow1.X * Viewer.ScaleFactor) - Left;
	FArrow1.Y       := Round(FArrow1.Y * Viewer.ScaleFactor) - Top;
	FArrow2.X       := Round(FArrow2.X * Viewer.ScaleFactor) - Left;
	FArrow2.Y       := Round(FArrow2.Y * Viewer.ScaleFactor) - Top;
	// Listo
	SendToBack;
	Visible  := True;
	FArranging := False;
end;

procedure TWorkflowLink.ArrangePanels;
begin
	FSizePanelW.SetBounds(Left + FStartPoint.X - 2, Top + FStartPoint.Y - 2, 5, 5);
	FSizePanelE.SetBounds(Left + FEndPoint.X - 2, Top + FEndPoint.Y - 2, 5, 5);
end;

procedure TWorkflowLink.CMHitTest(var Message: TCMHitTest);
Var
	P: TPoint;
	Rgn: HRgn;
	PP: Array[1..4] of TPoint;
begin
	P := SmallPointToPoint(Message.Pos);
	PP[1] := Point(FStartPoint.X - 5, FStartPoint.Y - 5);
	PP[2] := Point(FStartPoint.X + 5, FStartPoint.Y + 5);
	PP[3] := Point(FEndPoint.X - 5, FEndPoint.Y + 5);
	PP[4] := Point(FEndPoint.X + 5, FEndPoint.Y - 5);
	Rgn := CreatePolygonRgn(PP, 4, WINDING);
	if PtInRegion(Rgn, P.X, P.Y) then Message.Result := HTCLIENT
	  else Message.Result := HTNOWHERE;
	DeleteObject(Rgn);
end;

procedure TWorkflowLink.CreatePanels;

	function CreatePanel: TPanel;
	begin
		Result := TPanel.Create(Self);
		Result.BevelInner := bvNone;
		Result.BevelOuter := bvNone;
		Result.Color := clBlack;
		Result.SetBounds(-10, -10, 6, 6);
		Result.Parent := Viewer;
	end;

begin
	FSizePanelW := CreatePanel;
	FSizePanelE  := CreatePanel;
	ArrangePanels;
end;

destructor TWorkflowLink.Destroy;
begin
	Selected := False;
	if Assigned(FStartTask) then FStartTask.FFwdLinks.Remove(Self);
	if Assigned(FEndTask) then FEndTask.FBackLinks.Remove(Self);
	inherited;
end;

procedure TWorkflowLink.DestroyPanels;
begin
	FSizePanelW.Free; FSizePanelW := nil;
	FSizePanelE.Free; FSizePanelE := nil;
end;

function TWorkflowLink.GetDesignMode: Boolean;
begin
	Result := FDesignMode;
end;

function TWorkflowLink.GetSelected: Boolean;
begin
	Result := Assigned(FSizePanelW);
end;

procedure TWorkflowLink.Paint;
begin
	Canvas.Pen.Style := psSolid;
	if Trim(FCondition) = '' then Canvas.Pen.Color := clBlack
	  else Canvas.Pen.Color := clRed;
	Canvas.MoveTo(FStartPoint.X, FStartPoint.Y);
	Canvas.LineTo(FEndPoint.X, FEndPoint.Y);
	Canvas.MoveTo(FArrow1.X, FArrow1.Y);
	Canvas.Brush.Color := clBlack;
	Canvas.Pen.Style := psSolid;
	Canvas.Polygon([FEndPoint, FArrow1, FArrow2]);
end;

procedure TWorkflowLink.Resize;
begin
	inherited;
	if FArranging then Exit;
	Arrange;
	if Selected then ArrangePanels;
end;

procedure TWorkflowLink.SetCondition(const Value: AnsiString);
begin
	if FCondition = Value then Exit;
	FCondition := Value;
	Invalidate;
end;

procedure TWorkflowLink.SetDesignMode(const Value: Boolean);
begin
	if FDesignMode = Value then Exit;
	FDesignMode := Value;
end;

procedure TWorkflowLink.SetSelected(const Value: Boolean);
begin
	if (Selected = Value) then Exit;
	if Value then CreatePanels else DestroyPanels;
end;

{ TWorkflowDisplay }

constructor TWorkflowDisplay.Create(AOwner: TComponent);
begin
	inherited;
	FViewer := TWorkflowViewer.Create(Self);
	FViewer.Parent := Self;
	FViewer.Align := alClient;
	FViewer.HorzScrollBar.Margin := 15;
	FViewer.Color := $00FAF8F2;
end;

procedure TWorkflowDisplay.Loaded;
begin
	inherited;
	Reload;
end;

procedure TWorkflowDisplay.Notification(AComponent: TComponent; Operation: TOperation);
begin
	if (Operation = opRemove) and (AComponent = Connection) then Connection := nil;
end;

procedure TWorkflowDisplay.Reload;
begin
	if not Assigned(FConnection) or (FWorkflowID = 0) then begin
		While FViewer.ItemCount > 0 do FViewer.Items[0].Free;
		Exit;
	end;
	if FServiceOrder = 0 then begin
		LoadWorkflow(FWorkflowID, FWorkflowVersion, Connection, FViewer);
	end else begin
		LoadServiceOrder(FServiceOrder, FWorkflowID, Connection, FViewer);
	end;
	FViewer.ZoomType := ztPercentage;
	FViewer.ZoomType := ztWidth;
end;

procedure TWorkflowDisplay.SetConnection(const Value: TADOConnection);
begin
	if (FConnection = Value) then Exit;
	FConnection := Value;
	if not (csLoading in ComponentState) then Reload;
end;

procedure TWorkflowDisplay.SetServiceOrder(const Value: Integer);
begin
	if FServiceOrder = Value then Exit;
	FServiceOrder := Value;
	FWorkflowVersion := 0;
	if not (csLoading in ComponentState) then Reload;
end;

procedure TWorkflowDisplay.SetWorkflowID(const Value: Integer);
begin
	if FWorkflowID = Value then Exit;
	FWorkflowID := Value;
	if not (csLoading in ComponentState) then Reload;
end;

procedure TWorkflowDisplay.SetWorkflowVersion(const Value: Integer);
begin
	if FWorkflowVersion = Value then Exit;
	FWorkflowVersion := Value;
	FServiceOrder := 0;
	if not (csLoading in ComponentState) then Reload;
end;


end.
