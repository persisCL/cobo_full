unit WNVVIDEOLib_TLB;

// ************************************************************************ //
// WARNING                                                                  //
// -------                                                                  //
// The types declared in this file were generated from data read from a     //
// Type Library. If this type library is explicitly or indirectly (via      //
// another type library referring to this type library) re-imported, or the //
// 'Refresh' command of the Type Library Editor activated while editing the //
// Type Library, the contents of this file will be regenerated and all      //
// manual modifications will be lost.                                       //
// ************************************************************************ //

// PASTLWTR : $Revision: 1.1 $
// File generated on 1/10/99 9.45.50 from Type Library described below.

// ************************************************************************ //
// Type Lib: C:\Program Files\Winnov Videum NT\WnvVideo.ocx
// IID\LCID: {772D8C60-6294-11D0-B9B5-00A024FF8660}\0
// Helpfile: 
// HelpString: Winnov Video Capture 1.0
// Version:    1.0
// ************************************************************************ //

interface

uses Windows, ActiveX, Classes, Graphics, OleCtrls, StdVCL;

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:      //
//   Type Libraries     : LIBID_xxxx                                    //
//   CoClasses          : CLASS_xxxx                                    //
//   DISPInterfaces     : DIID_xxxx                                     //
//   Non-DISP interfaces: IID_xxxx                                      //
// *********************************************************************//
const
  LIBID_WNVVIDEOLib: TGUID = '{772D8C60-6294-11D0-B9B5-00A024FF8660}';
  DIID__DWnvVideo: TGUID = '{772D8C61-6294-11D0-B9B5-00A024FF8660}';
  DIID__DWnvVideoEvents: TGUID = '{772D8C62-6294-11D0-B9B5-00A024FF8660}';
  CLASS_WnvVideo: TGUID = '{772D8C63-6294-11D0-B9B5-00A024FF8660}';
type

// *********************************************************************//
// Forward declaration of interfaces defined in Type Library            //
// *********************************************************************//
  _DWnvVideo = dispinterface;
  _DWnvVideoEvents = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                     //
// (NOTE: Here we map each CoClass to its Default Interface)            //
// *********************************************************************//
  {$WARNINGS OFF}
  WnvVideo = _DWnvVideo;
  {$WARNINGS ON}

// *********************************************************************//
// Declaration of structures, unions and aliases.                       //
// *********************************************************************//
  PInteger1 = ^Integer; {*}


// *********************************************************************//
// DispIntf:  _DWnvVideo
// Flags:     (4112) Hidden Dispatchable
// GUID:      {772D8C61-6294-11D0-B9B5-00A024FF8660}
// *********************************************************************//
  _DWnvVideo = dispinterface
    ['{772D8C61-6294-11D0-B9B5-00A024FF8660}']
    property Version: WideString dispid 1;
    property ErrorCode: Integer dispid 2;
    property PreviewPriority: Integer dispid 3;
    property FrameRate: Integer dispid 4;
    property AudioCaptureOn: WordBool dispid 5;
    property AudioCaptureStereo: WordBool dispid 6;
    property AudioCaptureSampleRate: Integer dispid 7;
    property TimeLimit: Integer dispid 8;
    property FrameLimit: Integer dispid 9;
    property VideoFileName: WideString dispid 10;
    property VideoCodec: WideString dispid 11;
    property VideoQuality: Integer dispid 12;
    property VideoHeight: Integer dispid 13;
    property VideoWidth: Integer dispid 14;
    property MaintainAspectRatio: WordBool dispid 15;
    property BackColor: OLE_COLOR dispid -501;
    property StillWidth: Integer dispid 16;
    property StillHeight: Integer dispid 17;
    property StillImageType: Integer dispid 18;
    property StillOrientation: Integer dispid 19;
    property Enabled: WordBool dispid -514;
    property DynamicVideoSize: WordBool dispid 20;
    property VideoBorder: Integer dispid 21;
    property StillMode: Integer dispid 22;
    property AudioCapture8Bit: WordBool dispid 23;
    property DeviceIndex: Integer dispid 24;
    property EnableStatusEvents: WordBool dispid 25;
    property EnableFrameEvents: WordBool dispid 26;
    property EnableVideoStreamEvents: WordBool dispid 27;
    property EnableAudioStreamEvents: WordBool dispid 28;
    property CanStretchPreview: WordBool dispid 29;
    property StretchPreview: WordBool dispid 30;
    property PreviewFrameRate: Integer dispid 31;
    property EnableErrorEvents: WordBool dispid 32;
    property PerformanceX10: Integer dispid 33;
    function Startup: Integer; dispid 1000;
    function Shutdown: Integer; dispid 1001;
    function StartContinuousCapture: Integer; dispid 1002;
    function StartContinuousCaptureNoFile: Integer; dispid 1003;
    function SuspendVideoCapture: Integer; dispid 1004;
    function ResumeVideoCapture: Integer; dispid 1005;
    function StartNonContinuousCapture: Integer; dispid 1006;
    function GrabFrame: Integer; dispid 1007;
    function StopCapture: Integer; dispid 1008;
    function CaptureFrameToFile(const Filename: WideString): Integer; dispid 1009;
    function CaptureFrameToClipboard: Integer; dispid 1010;
    function CaptureStillToFile(const Filename: WideString): Integer; dispid 1011;
    function CaptureStillToClipboard: Integer; dispid 1012;
    function SaveCaptureAs(const Filename: WideString): Integer; dispid 1013;
    function PlayVideo(const Filename: WideString): Integer; dispid 1014;
    function ShowFormatDialog: Integer; dispid 1015;
    function ShowSourceDialog: Integer; dispid 1016;
    function AllocateCaptureFile(SizeInMB: Integer): Integer; dispid 1017;
    function CloseStillCapture: Integer; dispid 1018;
    function InitializeStillCapture: Integer; dispid 1019;
    function SetVideoWidthAndHeight(Width: Integer; Height: Integer): Integer; dispid 1020;
    function CopyData(var Array_: Integer; var Data: Integer; Get: WordBool; Bytes: Integer): Integer; dispid 1021;
  end;

// *********************************************************************//
// DispIntf:  _DWnvVideoEvents
// Flags:     (4096) Dispatchable
// GUID:      {772D8C62-6294-11D0-B9B5-00A024FF8660}
// *********************************************************************//
  _DWnvVideoEvents = dispinterface
    ['{772D8C62-6294-11D0-B9B5-00A024FF8660}']
    procedure VideoFrameCaptured(var Data: Integer; BufferLength: Integer; BytesUsed: Integer; 
                                 TimeCaptured: Integer; Flags: Integer); dispid 1;
    procedure CaptureStatus(Id: Integer; const Message: WideString); dispid 2;
    procedure AudioStreamData(var Data: Integer; BufferLength: Integer; BytesRecorded: Integer; 
                              Flags: Integer); dispid 3;
    procedure VideoStreamData(var Data: Integer; BufferLength: Integer; BytesUsed: Integer; 
                              TimeCaptured: Integer; Flags: Integer); dispid 4;
    procedure OnError(const ErrorSource: WideString; ErrorCode: Integer); dispid 5;
    procedure CaptureStopped; dispid 6;
    procedure CaptureError(Id: Integer; const Message: WideString); dispid 7;
  end;


// *********************************************************************//
// OLE Control Proxy class declaration
// Control Name     : TWnvVideo
// Help String      : WnvVideo Control
// Default Interface: _DWnvVideo
// Def. Intf. DISP? : Yes
// Event   Interface: _DWnvVideoEvents
// TypeFlags        : (38) CanCreate Licensed Control
// *********************************************************************//
  TWnvVideoVideoFrameCaptured = procedure(Sender: TObject; var Data: Integer; 
                                                           BufferLength: Integer; 
                                                           BytesUsed: Integer; 
                                                           TimeCaptured: Integer; Flags: Integer) of object;
  TWnvVideoCaptureStatus = procedure(Sender: TObject; Id: Integer; const Message: WideString) of object;
  TWnvVideoAudioStreamData = procedure(Sender: TObject; var Data: Integer; BufferLength: Integer; 
                                                        BytesRecorded: Integer; Flags: Integer) of object;
  TWnvVideoVideoStreamData = procedure(Sender: TObject; var Data: Integer; BufferLength: Integer; 
                                                        BytesUsed: Integer; TimeCaptured: Integer; 
                                                        Flags: Integer) of object;
  TWnvVideoOnError = procedure(Sender: TObject; const ErrorSource: WideString; ErrorCode: Integer) of object;
  TWnvVideoCaptureError = procedure(Sender: TObject; Id: Integer; const Message: WideString) of object;

  TWnvVideo = class(TOleControl)
  private
    FOnVideoFrameCaptured: TWnvVideoVideoFrameCaptured;
    FOnCaptureStatus: TWnvVideoCaptureStatus;
    FOnAudioStreamData: TWnvVideoAudioStreamData;
    FOnVideoStreamData: TWnvVideoVideoStreamData;
    FOnError: TWnvVideoOnError;
    FOnCaptureStopped: TNotifyEvent;
    FOnCaptureError: TWnvVideoCaptureError;
	{$WARNINGS OFF}
	FIntf: _DWnvVideo;
	function  GetControlInterface: _DWnvVideo;
	{$WARNINGS ON}
  protected
    procedure CreateControl;
    procedure InitControlData; override;
  public
    function Startup: Integer;
    function Shutdown: Integer;
    function StartContinuousCapture: Integer;
    function StartContinuousCaptureNoFile: Integer;
    function SuspendVideoCapture: Integer;
    function ResumeVideoCapture: Integer;
    function StartNonContinuousCapture: Integer;
    function GrabFrame: Integer;
    function StopCapture: Integer;
    function CaptureFrameToFile(const Filename: WideString): Integer;
    function CaptureFrameToClipboard: Integer;
    function CaptureStillToFile(const Filename: WideString): Integer;
    function CaptureStillToClipboard: Integer;
    function SaveCaptureAs(const Filename: WideString): Integer;
    function PlayVideo(const Filename: WideString): Integer;
    function ShowFormatDialog: Integer;
    function ShowSourceDialog: Integer;
    function AllocateCaptureFile(SizeInMB: Integer): Integer;
    function CloseStillCapture: Integer;
    function InitializeStillCapture: Integer;
    function SetVideoWidthAndHeight(Width: Integer; Height: Integer): Integer;
    function CopyData(var Array_: Integer; var Data: Integer; Get: WordBool; Bytes: Integer): Integer;
	{$WARNINGS OFF}
	property  ControlInterface: _DWnvVideo read GetControlInterface;
	{$WARNINGS ON}
  published
    property  ParentColor;
    property  TabStop;
    property  Align;
    property  DragCursor;
    property  DragMode;
    property  ParentShowHint;
    property  PopupMenu;
    property  ShowHint;
    property  TabOrder;
    property  Visible;
    property  OnDragDrop;
    property  OnDragOver;
    property  OnEndDrag;
    property  OnEnter;
    property  OnExit;
    property  OnStartDrag;
    property Version: WideString index 1 read GetWideStringProp write SetWideStringProp stored False;
    property ErrorCode: Integer index 2 read GetIntegerProp write SetIntegerProp stored False;
    property PreviewPriority: Integer index 3 read GetIntegerProp write SetIntegerProp stored False;
    property FrameRate: Integer index 4 read GetIntegerProp write SetIntegerProp stored False;
    property AudioCaptureOn: WordBool index 5 read GetWordBoolProp write SetWordBoolProp stored False;
    property AudioCaptureStereo: WordBool index 6 read GetWordBoolProp write SetWordBoolProp stored False;
    property AudioCaptureSampleRate: Integer index 7 read GetIntegerProp write SetIntegerProp stored False;
    property TimeLimit: Integer index 8 read GetIntegerProp write SetIntegerProp stored False;
    property FrameLimit: Integer index 9 read GetIntegerProp write SetIntegerProp stored False;
    property VideoFileName: WideString index 10 read GetWideStringProp write SetWideStringProp stored False;
    property VideoCodec: WideString index 11 read GetWideStringProp write SetWideStringProp stored False;
    property VideoQuality: Integer index 12 read GetIntegerProp write SetIntegerProp stored False;
    property VideoHeight: Integer index 13 read GetIntegerProp write SetIntegerProp stored False;
    property VideoWidth: Integer index 14 read GetIntegerProp write SetIntegerProp stored False;
    property MaintainAspectRatio: WordBool index 15 read GetWordBoolProp write SetWordBoolProp stored False;
    property BackColor: TColor index -501 read GetTColorProp write SetTColorProp stored False;
    property StillWidth: Integer index 16 read GetIntegerProp write SetIntegerProp stored False;
    property StillHeight: Integer index 17 read GetIntegerProp write SetIntegerProp stored False;
    property StillImageType: Integer index 18 read GetIntegerProp write SetIntegerProp stored False;
    property StillOrientation: Integer index 19 read GetIntegerProp write SetIntegerProp stored False;
    property Enabled: WordBool index -514 read GetWordBoolProp write SetWordBoolProp stored False;
    property DynamicVideoSize: WordBool index 20 read GetWordBoolProp write SetWordBoolProp stored False;
    property VideoBorder: Integer index 21 read GetIntegerProp write SetIntegerProp stored False;
    property StillMode: Integer index 22 read GetIntegerProp write SetIntegerProp stored False;
    property AudioCapture8Bit: WordBool index 23 read GetWordBoolProp write SetWordBoolProp stored False;
    property DeviceIndex: Integer index 24 read GetIntegerProp write SetIntegerProp stored False;
    property EnableStatusEvents: WordBool index 25 read GetWordBoolProp write SetWordBoolProp stored False;
    property EnableFrameEvents: WordBool index 26 read GetWordBoolProp write SetWordBoolProp stored False;
    property EnableVideoStreamEvents: WordBool index 27 read GetWordBoolProp write SetWordBoolProp stored False;
    property EnableAudioStreamEvents: WordBool index 28 read GetWordBoolProp write SetWordBoolProp stored False;
    property CanStretchPreview: WordBool index 29 read GetWordBoolProp write SetWordBoolProp stored False;
    property StretchPreview: WordBool index 30 read GetWordBoolProp write SetWordBoolProp stored False;
    property PreviewFrameRate: Integer index 31 read GetIntegerProp write SetIntegerProp stored False;
    property EnableErrorEvents: WordBool index 32 read GetWordBoolProp write SetWordBoolProp stored False;
    property PerformanceX10: Integer index 33 read GetIntegerProp write SetIntegerProp stored False;
    property OnVideoFrameCaptured: TWnvVideoVideoFrameCaptured read FOnVideoFrameCaptured write FOnVideoFrameCaptured;
    property OnCaptureStatus: TWnvVideoCaptureStatus read FOnCaptureStatus write FOnCaptureStatus;
    property OnAudioStreamData: TWnvVideoAudioStreamData read FOnAudioStreamData write FOnAudioStreamData;
    property OnVideoStreamData: TWnvVideoVideoStreamData read FOnVideoStreamData write FOnVideoStreamData;
    property OnError: TWnvVideoOnError read FOnError write FOnError;
    property OnCaptureStopped: TNotifyEvent read FOnCaptureStopped write FOnCaptureStopped;
    property OnCaptureError: TWnvVideoCaptureError read FOnCaptureError write FOnCaptureError;
  end;

procedure Register;

implementation

uses ComObj;

procedure TWnvVideo.InitControlData;
const
  CEventDispIDs: array [0..6] of DWORD = (
    $00000001, $00000002, $00000003, $00000004, $00000005, $00000006,
    $00000007);
  CLicenseKey: array[0..19] of Word = ( $0043, $006F, $0070, $0079, $0072, $0069, $0067, $0068, $0074, $0020, $0028
    , $0063, $0029, $0020, $0031, $0039, $0039, $0036, $0020, $0000);
  CControlData: TControlData2 = (
    ClassID: '{772D8C63-6294-11D0-B9B5-00A024FF8660}';
    EventIID: '{772D8C62-6294-11D0-B9B5-00A024FF8660}';
    EventCount: 7;
    EventDispIDs: @CEventDispIDs;
    LicenseKey: @CLicenseKey;
    Flags: $00000009;
    Version: 401);
begin
  ControlData := @CControlData;
  TControlData2(CControlData).FirstEventOfs := Cardinal(@@FOnVideoFrameCaptured) - Cardinal(Self);
end;

procedure TWnvVideo.CreateControl;

  procedure DoCreate;
  begin
	{$WARNINGS OFF}
	FIntf := IUnknown(OleObject) as _DWnvVideo;
	{$WARNINGS ON}
  end;

begin
  if FIntf = nil then DoCreate;
end;

{$WARNINGS OFF}
function TWnvVideo.GetControlInterface: _DWnvVideo;
{$WARNINGS ON}
begin
  CreateControl;
  Result := FIntf;
end;

function TWnvVideo.Startup: Integer;
begin
  Result := ControlInterface.Startup;
end;

function TWnvVideo.Shutdown: Integer;
begin
  Result := ControlInterface.Shutdown;
end;

function TWnvVideo.StartContinuousCapture: Integer;
begin
  Result := ControlInterface.StartContinuousCapture;
end;

function TWnvVideo.StartContinuousCaptureNoFile: Integer;
begin
  Result := ControlInterface.StartContinuousCaptureNoFile;
end;

function TWnvVideo.SuspendVideoCapture: Integer;
begin
  Result := ControlInterface.SuspendVideoCapture;
end;

function TWnvVideo.ResumeVideoCapture: Integer;
begin
  Result := ControlInterface.ResumeVideoCapture;
end;

function TWnvVideo.StartNonContinuousCapture: Integer;
begin
  Result := ControlInterface.StartNonContinuousCapture;
end;

function TWnvVideo.GrabFrame: Integer;
begin
  Result := ControlInterface.GrabFrame;
end;

function TWnvVideo.StopCapture: Integer;
begin
  Result := ControlInterface.StopCapture;
end;

function TWnvVideo.CaptureFrameToFile(const Filename: WideString): Integer;
begin
  Result := ControlInterface.CaptureFrameToFile(Filename);
end;

function TWnvVideo.CaptureFrameToClipboard: Integer;
begin
  Result := ControlInterface.CaptureFrameToClipboard;
end;

function TWnvVideo.CaptureStillToFile(const Filename: WideString): Integer;
begin
  Result := ControlInterface.CaptureStillToFile(Filename);
end;

function TWnvVideo.CaptureStillToClipboard: Integer;
begin
  Result := ControlInterface.CaptureStillToClipboard;
end;

function TWnvVideo.SaveCaptureAs(const Filename: WideString): Integer;
begin
  Result := ControlInterface.SaveCaptureAs(Filename);
end;

function TWnvVideo.PlayVideo(const Filename: WideString): Integer;
begin
  Result := ControlInterface.PlayVideo(Filename);
end;

function TWnvVideo.ShowFormatDialog: Integer;
begin
  Result := ControlInterface.ShowFormatDialog;
end;

function TWnvVideo.ShowSourceDialog: Integer;
begin
  Result := ControlInterface.ShowSourceDialog;
end;

function TWnvVideo.AllocateCaptureFile(SizeInMB: Integer): Integer;
begin
  Result := ControlInterface.AllocateCaptureFile(SizeInMB);
end;

function TWnvVideo.CloseStillCapture: Integer;
begin
  Result := ControlInterface.CloseStillCapture;
end;

function TWnvVideo.InitializeStillCapture: Integer;
begin
  Result := ControlInterface.InitializeStillCapture;
end;

function TWnvVideo.SetVideoWidthAndHeight(Width: Integer; Height: Integer): Integer;
begin
  Result := ControlInterface.SetVideoWidthAndHeight(Width, Height);
end;

function TWnvVideo.CopyData(var Array_: Integer; var Data: Integer; Get: WordBool; Bytes: Integer): Integer;
begin
  Result := ControlInterface.CopyData(Array_, Data, Get, Bytes);
end;

procedure Register;
begin
  RegisterComponents('ActiveX',[TWnvVideo]);
end;

end.


