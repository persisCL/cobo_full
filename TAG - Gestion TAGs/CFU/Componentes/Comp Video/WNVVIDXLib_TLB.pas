unit WNVVIDXLib_TLB;

// ************************************************************************ //
// WARNING                                                                  //
// -------                                                                  //
// The types declared in this file were generated from data read from a     //
// Type Library. If this type library is explicitly or indirectly (via      //
// another type library referring to this type library) re-imported, or the //
// 'Refresh' command of the Type Library Editor activated while editing the //
// Type Library, the contents of this file will be regenerated and all      //
// manual modifications will be lost.                                       //
// ************************************************************************ //

// PASTLWTR : $Revision: 1.1 $
// File generated on 17/10/99 21.33.47 from Type Library described below.

// ************************************************************************ //
// Type Lib: C:\Program Files\Winnov Videum NT\WnvVidX.ocx
// IID\LCID: {5B5E2861-6554-11D0-B9B5-00A024FF8660}\0
// Helpfile: 
// HelpString: Winnov Video Device Controls 1.0
// Version:    1.0
// ************************************************************************ //

interface

uses Windows, ActiveX, Classes, Graphics, OleCtrls, StdVCL;

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:      //
//   Type Libraries     : LIBID_xxxx                                    //
//   CoClasses          : CLASS_xxxx                                    //
//   DISPInterfaces     : DIID_xxxx                                     //
//   Non-DISP interfaces: IID_xxxx                                      //
// *********************************************************************//
const
  LIBID_WNVVIDXLib: TGUID = '{5B5E2861-6554-11D0-B9B5-00A024FF8660}';
  DIID__DWnvVidX: TGUID = '{5B5E2862-6554-11D0-B9B5-00A024FF8660}';
  DIID__DWnvVidXEvents: TGUID = '{5B5E2863-6554-11D0-B9B5-00A024FF8660}';
  CLASS_WnvVidX: TGUID = '{5B5E2864-6554-11D0-B9B5-00A024FF8660}';
type

// *********************************************************************//
// Forward declaration of interfaces defined in Type Library            //
// *********************************************************************//
  _DWnvVidX = dispinterface;
  _DWnvVidXEvents = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                     //
// (NOTE: Here we map each CoClass to its Default Interface)            //
// *********************************************************************//
  {$WARNINGS OFF}
  WnvVidX = _DWnvVidX;
  {$WARNINGS ON}

// *********************************************************************//
// DispIntf:  _DWnvVidX
// Flags:     (4112) Hidden Dispatchable
// GUID:      {5B5E2862-6554-11D0-B9B5-00A024FF8660}
// *********************************************************************//
  _DWnvVidX = dispinterface
    ['{5B5E2862-6554-11D0-B9B5-00A024FF8660}']
    property ErrorCode: Integer dispid 1;
    property Version: WideString dispid 2;
    property BackColor: OLE_COLOR dispid -501;
    property ForeColor: OLE_COLOR dispid -513;
    function Startup(hCapWnd: Integer): Integer; dispid 1000;
    function GetBrightness: Integer; dispid 1001;
    function SetBrightness(Brightness: Integer): Integer; dispid 1002;
    function GetContrast: Integer; dispid 1003;
    function SetContrast(Contrast: Integer): Integer; dispid 1004;
    function GetHue: Integer; dispid 1005;
    function SetHue(Hue: Integer): Integer; dispid 1006;
    function GetSaturation: Integer; dispid 1007;
    function SetSaturation(Saturation: Integer): Integer; dispid 1008;
    function ReturnToDefaults: Integer; dispid 1009;
    function SetStandard(Standard: Integer): Integer; dispid 1010;
    function SetEquipment(Equipment: Integer): Integer; dispid 1011;
    function GetEquipment: Integer; dispid 1012;
    function GetStandard: Integer; dispid 1013;
    function GetSource: Integer; dispid 1014;
    function SetSource(Source: Integer): Integer; dispid 1015;
    function GetFirstSource: Integer; dispid 1016;
    function GetNextSource: Integer; dispid 1017;
    function GetSourceName(Source: Integer): WideString; dispid 1018;
    function IsVideoActive: WordBool; dispid 1019;
    function SetZoom(Zoom: Integer): Integer; dispid 1020;
    function GetVerticalCentering: Integer; dispid 1021;
    function SetVerticalCentering(Value: Integer): Integer; dispid 1022;
    function GetHorizontalCentering: Integer; dispid 1023;
    function SetHorizontalCentering(Value: Integer): Integer; dispid 1024;
    function Shutdown: Integer; dispid 1025;
    function GetFlipHorizontal: WordBool; dispid 1026;
    function SetFlipHorizontal(Flip: WordBool): Integer; dispid 1027;
    function GetFlipVertical: WordBool; dispid 1028;
    function SetFlipVertical(Flip: WordBool): Integer; dispid 1029;
    function GetZoom: Integer; dispid 1030;
    function GetPan: Integer; dispid 1031;
    function GetTilt: Integer; dispid 1032;
    function SetTilt(Tilt: Integer): Integer; dispid 1033;
    function SetPan(Pan: Integer): Integer; dispid 1034;
    function GetUnit: Integer; dispid 1035;
    function SetUnit(Unit_: Integer): Integer; dispid 1036;
    function GetShutterSpeed: Integer; dispid 1037;
    function SetShutterSpeed(Speed: Integer): Integer; dispid 1038;
    function GetGain: Integer; dispid 1039;
    function SetGain(Gain: Integer): Integer; dispid 1040;
    function GetAutoGain: WordBool; dispid 1041;
    function SetAutoGain(AutoGain: WordBool): Integer; dispid 1042;
    function GetAutoWhiteBalance: Integer; dispid 1043;
    function SetAutoWhiteBalance(AutoWhiteBalance: WordBool): Integer; dispid 1044;
    function DoWhiteBalance: Integer; dispid 1045;
    function SetPanAndTilt(Pan: Integer; Tilt: Integer): Integer; dispid 1046;
  end;

// *********************************************************************//
// DispIntf:  _DWnvVidXEvents
// Flags:     (4096) Dispatchable
// GUID:      {5B5E2863-6554-11D0-B9B5-00A024FF8660}
// *********************************************************************//
  _DWnvVidXEvents = dispinterface
    ['{5B5E2863-6554-11D0-B9B5-00A024FF8660}']
    procedure OnError(const ErrorSource: WideString; ErrorCode: Integer); dispid 1;
    procedure BrightnessChanged; dispid 2;
    procedure ContrastChanged; dispid 3;
    procedure HueChanged; dispid 4;
    procedure SaturationChanged; dispid 5;
    procedure StandardChanged; dispid 6;
    procedure EquipmentChanged; dispid 7;
  end;


// *********************************************************************//
// OLE Control Proxy class declaration
// Control Name     : TWnvVidX
// Help String      : WnvVidX Control
// Default Interface: _DWnvVidX
// Def. Intf. DISP? : Yes
// Event   Interface: _DWnvVidXEvents
// TypeFlags        : (38) CanCreate Licensed Control
// *********************************************************************//
  TWnvVidXOnError = procedure(Sender: TObject; const ErrorSource: WideString; ErrorCode: Integer) of object;

  TWnvVidX = class(TOleControl)
  private
    FOnError: TWnvVidXOnError;
    FOnBrightnessChanged: TNotifyEvent;
    FOnContrastChanged: TNotifyEvent;
    FOnHueChanged: TNotifyEvent;
    FOnSaturationChanged: TNotifyEvent;
    FOnStandardChanged: TNotifyEvent;
    FOnEquipmentChanged: TNotifyEvent;
	{$WARNINGS OFF}
	FIntf: _DWnvVidX;
	function  GetControlInterface: _DWnvVidX;
	{$WARNINGS ON}
  protected
    procedure CreateControl;
    procedure InitControlData; override;
  public
    function Startup(hCapWnd: Integer): Integer;
    function GetBrightness: Integer;
    function SetBrightness(Brightness: Integer): Integer;
    function GetContrast: Integer;
    function SetContrast(Contrast: Integer): Integer;
    function GetHue: Integer;
    function SetHue(Hue: Integer): Integer;
    function GetSaturation: Integer;
    function SetSaturation(Saturation: Integer): Integer;
    function ReturnToDefaults: Integer;
    function SetStandard(Standard: Integer): Integer;
    function SetEquipment(Equipment: Integer): Integer;
    function GetEquipment: Integer;
    function GetStandard: Integer;
    function GetSource: Integer;
    function SetSource(Source: Integer): Integer;
    function GetFirstSource: Integer;
    function GetNextSource: Integer;
    function GetSourceName(Source: Integer): WideString;
    function IsVideoActive: WordBool;
    function SetZoom(Zoom: Integer): Integer;
    function GetVerticalCentering: Integer;
    function SetVerticalCentering(Value: Integer): Integer;
    function GetHorizontalCentering: Integer;
    function SetHorizontalCentering(Value: Integer): Integer;
    function Shutdown: Integer;
    function GetFlipHorizontal: WordBool;
    function SetFlipHorizontal(Flip: WordBool): Integer;
    function GetFlipVertical: WordBool;
    function SetFlipVertical(Flip: WordBool): Integer;
    function GetZoom: Integer;
    function GetPan: Integer;
    function GetTilt: Integer;
    function SetTilt(Tilt: Integer): Integer;
    function SetPan(Pan: Integer): Integer;
    function GetUnit: Integer;
    function SetUnit(Unit_: Integer): Integer;
    function GetShutterSpeed: Integer;
    function SetShutterSpeed(Speed: Integer): Integer;
    function GetGain: Integer;
    function SetGain(Gain: Integer): Integer;
    function GetAutoGain: WordBool;
    function SetAutoGain(AutoGain: WordBool): Integer;
    function GetAutoWhiteBalance: Integer;
    function SetAutoWhiteBalance(AutoWhiteBalance: WordBool): Integer;
    function DoWhiteBalance: Integer;
    function SetPanAndTilt(Pan: Integer; Tilt: Integer): Integer;
	{$WARNINGS OFF}
	property  ControlInterface: _DWnvVidX read GetControlInterface;
	{$WARNINGS ON}
  published
    property  ParentColor;
    property  ParentFont;
    property  TabStop;
    property  Align;
    property  DragCursor;
    property  DragMode;
    property  ParentShowHint;
    property  PopupMenu;
    property  ShowHint;
    property  TabOrder;
    property  Visible;
    property  OnDragDrop;
    property  OnDragOver;
    property  OnEndDrag;
    property  OnEnter;
    property  OnExit;
    property  OnStartDrag;
    property ErrorCode: Integer index 1 read GetIntegerProp write SetIntegerProp stored False;
    property Version: WideString index 2 read GetWideStringProp write SetWideStringProp stored False;
    property BackColor: TColor index -501 read GetTColorProp write SetTColorProp stored False;
    property ForeColor: TColor index -513 read GetTColorProp write SetTColorProp stored False;
    property OnError: TWnvVidXOnError read FOnError write FOnError;
    property OnBrightnessChanged: TNotifyEvent read FOnBrightnessChanged write FOnBrightnessChanged;
    property OnContrastChanged: TNotifyEvent read FOnContrastChanged write FOnContrastChanged;
    property OnHueChanged: TNotifyEvent read FOnHueChanged write FOnHueChanged;
    property OnSaturationChanged: TNotifyEvent read FOnSaturationChanged write FOnSaturationChanged;
    property OnStandardChanged: TNotifyEvent read FOnStandardChanged write FOnStandardChanged;
    property OnEquipmentChanged: TNotifyEvent read FOnEquipmentChanged write FOnEquipmentChanged;
  end;

procedure Register;

implementation

uses ComObj;

procedure TWnvVidX.InitControlData;
const
  CEventDispIDs: array [0..6] of DWORD = (
    $00000001, $00000002, $00000003, $00000004, $00000005, $00000006,
    $00000007);
  CLicenseKey: array[0..19] of Word = ( $0043, $006F, $0070, $0079, $0072, $0069, $0067, $0068, $0074, $0020, $0028
    , $0063, $0029, $0020, $0031, $0039, $0039, $0037, $0020, $0000);
  CControlData: TControlData2 = (
    ClassID: '{5B5E2864-6554-11D0-B9B5-00A024FF8660}';
    EventIID: '{5B5E2863-6554-11D0-B9B5-00A024FF8660}';
    EventCount: 7;
    EventDispIDs: @CEventDispIDs;
    LicenseKey: @CLicenseKey;
    Flags: $00000003;
    Version: 401);
begin
  ControlData := @CControlData;
  TControlData2(CControlData).FirstEventOfs := Cardinal(@@FOnError) - Cardinal(Self);
end;

procedure TWnvVidX.CreateControl;

  procedure DoCreate;
  begin
	{$WARNINGS OFF}
	FIntf := IUnknown(OleObject) as _DWnvVidX;
	{$WARNINGS ON}
  end;

begin
  if FIntf = nil then DoCreate;
end;

{$WARNINGS OFF}
function TWnvVidX.GetControlInterface: _DWnvVidX;
{$WARNINGS ON}
begin
  CreateControl;
  Result := FIntf;
end;

function TWnvVidX.Startup(hCapWnd: Integer): Integer;
begin
  Result := ControlInterface.Startup(hCapWnd);
end;

function TWnvVidX.GetBrightness: Integer;
begin
  Result := ControlInterface.GetBrightness;
end;

function TWnvVidX.SetBrightness(Brightness: Integer): Integer;
begin
  Result := ControlInterface.SetBrightness(Brightness);
end;

function TWnvVidX.GetContrast: Integer;
begin
  Result := ControlInterface.GetContrast;
end;

function TWnvVidX.SetContrast(Contrast: Integer): Integer;
begin
  Result := ControlInterface.SetContrast(Contrast);
end;

function TWnvVidX.GetHue: Integer;
begin
  Result := ControlInterface.GetHue;
end;

function TWnvVidX.SetHue(Hue: Integer): Integer;
begin
  Result := ControlInterface.SetHue(Hue);
end;

function TWnvVidX.GetSaturation: Integer;
begin
  Result := ControlInterface.GetSaturation;
end;

function TWnvVidX.SetSaturation(Saturation: Integer): Integer;
begin
  Result := ControlInterface.SetSaturation(Saturation);
end;

function TWnvVidX.ReturnToDefaults: Integer;
begin
  Result := ControlInterface.ReturnToDefaults;
end;

function TWnvVidX.SetStandard(Standard: Integer): Integer;
begin
  Result := ControlInterface.SetStandard(Standard);
end;

function TWnvVidX.SetEquipment(Equipment: Integer): Integer;
begin
  Result := ControlInterface.SetEquipment(Equipment);
end;

function TWnvVidX.GetEquipment: Integer;
begin
  Result := ControlInterface.GetEquipment;
end;

function TWnvVidX.GetStandard: Integer;
begin
  Result := ControlInterface.GetStandard;
end;

function TWnvVidX.GetSource: Integer;
begin
  Result := ControlInterface.GetSource;
end;

function TWnvVidX.SetSource(Source: Integer): Integer;
begin
  Result := ControlInterface.SetSource(Source);
end;

function TWnvVidX.GetFirstSource: Integer;
begin
  Result := ControlInterface.GetFirstSource;
end;

function TWnvVidX.GetNextSource: Integer;
begin
  Result := ControlInterface.GetNextSource;
end;

function TWnvVidX.GetSourceName(Source: Integer): WideString;
begin
  Result := ControlInterface.GetSourceName(Source);
end;

function TWnvVidX.IsVideoActive: WordBool;
begin
  Result := ControlInterface.IsVideoActive;
end;

function TWnvVidX.SetZoom(Zoom: Integer): Integer;
begin
  Result := ControlInterface.SetZoom(Zoom);
end;

function TWnvVidX.GetVerticalCentering: Integer;
begin
  Result := ControlInterface.GetVerticalCentering;
end;

function TWnvVidX.SetVerticalCentering(Value: Integer): Integer;
begin
  Result := ControlInterface.SetVerticalCentering(Value);
end;

function TWnvVidX.GetHorizontalCentering: Integer;
begin
  Result := ControlInterface.GetHorizontalCentering;
end;

function TWnvVidX.SetHorizontalCentering(Value: Integer): Integer;
begin
  Result := ControlInterface.SetHorizontalCentering(Value);
end;

function TWnvVidX.Shutdown: Integer;
begin
  Result := ControlInterface.Shutdown;
end;

function TWnvVidX.GetFlipHorizontal: WordBool;
begin
  Result := ControlInterface.GetFlipHorizontal;
end;

function TWnvVidX.SetFlipHorizontal(Flip: WordBool): Integer;
begin
  Result := ControlInterface.SetFlipHorizontal(Flip);
end;

function TWnvVidX.GetFlipVertical: WordBool;
begin
  Result := ControlInterface.GetFlipVertical;
end;

function TWnvVidX.SetFlipVertical(Flip: WordBool): Integer;
begin
  Result := ControlInterface.SetFlipVertical(Flip);
end;

function TWnvVidX.GetZoom: Integer;
begin
  Result := ControlInterface.GetZoom;
end;

function TWnvVidX.GetPan: Integer;
begin
  Result := ControlInterface.GetPan;
end;

function TWnvVidX.GetTilt: Integer;
begin
  Result := ControlInterface.GetTilt;
end;

function TWnvVidX.SetTilt(Tilt: Integer): Integer;
begin
  Result := ControlInterface.SetTilt(Tilt);
end;

function TWnvVidX.SetPan(Pan: Integer): Integer;
begin
  Result := ControlInterface.SetPan(Pan);
end;

function TWnvVidX.GetUnit: Integer;
begin
  Result := ControlInterface.GetUnit;
end;

function TWnvVidX.SetUnit(Unit_: Integer): Integer;
begin
  Result := ControlInterface.SetUnit(Unit_);
end;

function TWnvVidX.GetShutterSpeed: Integer;
begin
  Result := ControlInterface.GetShutterSpeed;
end;

function TWnvVidX.SetShutterSpeed(Speed: Integer): Integer;
begin
  Result := ControlInterface.SetShutterSpeed(Speed);
end;

function TWnvVidX.GetGain: Integer;
begin
  Result := ControlInterface.GetGain;
end;

function TWnvVidX.SetGain(Gain: Integer): Integer;
begin
  Result := ControlInterface.SetGain(Gain);
end;

function TWnvVidX.GetAutoGain: WordBool;
begin
  Result := ControlInterface.GetAutoGain;
end;

function TWnvVidX.SetAutoGain(AutoGain: WordBool): Integer;
begin
  Result := ControlInterface.SetAutoGain(AutoGain);
end;

function TWnvVidX.GetAutoWhiteBalance: Integer;
begin
  Result := ControlInterface.GetAutoWhiteBalance;
end;

function TWnvVidX.SetAutoWhiteBalance(AutoWhiteBalance: WordBool): Integer;
begin
  Result := ControlInterface.SetAutoWhiteBalance(AutoWhiteBalance);
end;

function TWnvVidX.DoWhiteBalance: Integer;
begin
  Result := ControlInterface.DoWhiteBalance;
end;

function TWnvVidX.SetPanAndTilt(Pan: Integer; Tilt: Integer): Integer;
begin
  Result := ControlInterface.SetPanAndTilt(Pan, Tilt);
end;

procedure Register;
begin
  RegisterComponents('ActiveX',[TWnvVidX]);
end;

end.
