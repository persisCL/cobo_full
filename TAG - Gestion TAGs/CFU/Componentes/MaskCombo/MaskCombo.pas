unit MaskCombo;

interface

uses Classes, ExtCtrls, StdCtrls, Mask, MaskUtils, Controls, dialogs, SysUtils,
 	 Graphics, variants, forms, windows;


type
    TMessageError = type String;
    TMaskEditExt = Class(TCustomMaskEdit)
    private
        FOriginalMask: AnsiString;
        FMessageError: TMessageError;
        FValidateRUT: Boolean;

        Function ValidRut(RUT: AnsiString): AnsiString;
    protected
        function Validate(const Value: string; var Pos: Integer): Boolean; Override;
        procedure ValidateError; Override;
        procedure CMExit(var Message: TCMExit);   message CM_EXIT;
    public
        property MessageError: TMessageError read FMessageError Write FMessageError;
        property ValidateRUT: Boolean read FValidateRUT write FValidateRUT;
    published
    end;

	TMaskComboItem = class(TCollectionItem)
    private
    	FMask: TMaskedText;
        FCaption: TCaption;
        FMessageError: TMessageError;
        FValidateRUT: Boolean;

        procedure SetCaption(const Value: TCaption);
        procedure SetMask(const Value: TMaskedText);
        procedure setValidateRut(const Value: Boolean);
    published
        property Caption: TCaption read FCaption Write SetCaption;
        property Mask: TMaskedText read FMask Write SetMask;
        property ValidateRut: Boolean read FValidateRUT write setValidateRut;
        property MessageError: TMessageError read FMessageError Write FMessageError;
    end;

    TMaskComboItems = class(TOwnedCollection)
    protected
    	function GetItem(index: integer): TMaskComboItem;
    public
    	constructor Create(AOwner: TPersistent);
        procedure Notify(Item: TCollectionItem;  Action: TCollectionNotification); override;
        procedure Update(Item: TCollectionItem); override;
        function  Add: TMaskComboItem;
        property  Items[index: integer]: TMaskComboItem read GetItem; default;
    end;

    TMaskComboCombo = class(TCustomComboBox)
    protected
    	procedure Resize; override;
//  published
//    	property Width;
//      property Font;
    end;

	TMaskCombo = class(TCustomControl)
    private
    	FItems: TMaskComboItems;
        FOmitCharacterRequired: Boolean;
        procedure SetOmitCharacterRequired(const Value: Boolean);
	protected
	    FAction: TCollectionNotification;
    	FFont: TFont;
    	FCombo: TMaskComboCombo;
        FMaskNumero: TMaskEditExt;
        FOnChangeMaskCombo: TNotifyEvent;
        procedure MyComboChange(Sender: TObject);
        procedure MyMaskChange(Sender: TObject);
        procedure MyMaskNumeroExit(Sender: TObject);
        procedure MyComboResize;
        procedure SetItems(const Value: TMaskComboItems);
        function CanResize(var NewWidth, NewHeight: Integer): Boolean; override;
        procedure setFont(Const Value: TFont);
        function GetComboWidth: integer;
        procedure SetComboWidth(Value: integer);
        function GetComboIndex: integer;
		procedure SetComboIndex(Value: integer);
		function GetItemsCount: integer;
		function GetMaskComboEnabled: Boolean;
		procedure SetMaskComboEnabled(Value: boolean);
		procedure DoReloadItems;
		function GetColorCombo: TColor;
		procedure SetColorCombo(const Value: TColor);
		function GetComboText: AnsiString;
		function GetMaskText: TMaskedText;
		procedure SetMaskText(const Value: TMaskedText);
	public
		constructor Create(AOwner: TComponent); override;
		destructor Destroy; override;
		procedure SetFocus; Override;
		function AddItem(const ComboText: ansiString; const ComboMask: TMaskedText): integer;
		function ValidateMask(): Boolean;
		function Focused: Boolean; override;
        Property ItemsCount: integer read getItemsCount;
        Property ComboText: AnsiString read GetComboText;
        Property MaskText: TMaskedText read GetMaskText write SetMaskText;
	published
        property OnChange: TNotifyEvent read FOnChangeMaskCombo write FOnChangeMaskCombo;
        property Items: TMaskComboItems read FItems write SetItems;
        property Font: TFont read FFont write SetFont;
        property Color: TColor read GetColorCombo write SetColorCombo;
        property ComboWidth: integer read getComboWidth write SetComboWidth;
        property ItemIndex: integer read getComboIndex write SetComboIndex;
        property Enabled;
        property OmitCharacterRequired: Boolean read FOmitCharacterRequired write SetOmitCharacterRequired;
        property TabOrder;
	end;

implementation

constructor TMaskCombo.Create(AOwner: TComponent);
begin
	inherited;
    //lo que quiera

    FCombo 				:= TMaskComboCombo.Create(Self);
    FMaskNumero 		:= TMaskEditExt.Create(Self);
    FItems				:= TMaskComboItems.Create(Self);

	//Propiedades del Combo
    FCombo.Parent		:= Self;
    FCombo.Align		:= alLeft;
    FCombo.Style		:= csDropDownList;

    //Propiedades del MaskNumero
    FMaskNumero.Parent	:= Self;
    FMaskNumero.Height	:= Self.Height;
	FMaskNumero.Left	:= FCombo.Left + FCombo.Width;
    FMaskNumero.Top		:= FCombo.Top;
    FMaskNumero.Align	:= AlClient;

    Width 				:= FCombo.Width + 50;

    //lista de mascaras
    //FMaskList			:= TStringList.Create;
    FFont				:= TFont.Create;
    FCombo.OnChange		:= MyComboChange;
    FMaskNumero.OnChange := MyMaskChange;
    FMaskNumero.OnExit  := MyMaskNumeroExit;
end;

function TMaskCombo.GetComboWidth: integer;
begin
	Result := FCombo.Width;
end;

procedure TMaskCombo.SetComboWidth(Value: integer);
begin
	if Value = FCombo.Width then Exit;
    FCombo.Width := Value;
end;

function TMaskCombo.GetComboIndex: integer;
begin
	Result := FCombo.ItemIndex;
end;

procedure TMaskCombo.SetComboIndex(Value: integer);
begin
	if Value = FCombo.ItemIndex then Exit;
    FCombo.ItemIndex := Value;

    FCombo.OnChange(self);
end;

function TMaskCombo.AddItem(const ComboText: ansiString; const ComboMask: TMaskedText): integer;
begin
    with FItems.Add do begin
    	Caption	:= ComboText;
        Mask	:= ComboMask;
	end;
    Result := FItems.Count - 1;
end;

function TMaskCombo.CanResize(var NewWidth, NewHeight: Integer): Boolean;
begin
	Result :=
      inherited CanResize(NewWidth, NewHeight)
      and (NewHeight = FCombo.Height);
end;

procedure TMaskCombo.MyComboResize;
begin
	Height := FCombo.Height;
end;

procedure TMaskCombo.MyComboChange(Sender: TObject);
begin
    //Aca hago el cambio de mascara.
    if FCombo.itemindex <> -1 then begin
        FMaskNumero.EditMask := FItems.Items[FCombo.ItemIndex].Mask;
        FMaskNumero.MessageError := FItems.Items[FCombo.ItemIndex].MessageError;
        FMaskNumero.ValidateRUT := FItems.Items[FCombo.ItemIndex].ValidateRUT;
    end;

	if Assigned(FOnChangeMaskCombo) then FOnChangeMaskCombo(self);
end;

procedure TMaskCombo.SetItems(const Value: TMaskComboItems);
begin
	FItems.Assign(Value);
end;

procedure TMaskCombo.SetFont(Const Value: TFont);
begin
	FCombo.Font 		:= Value;
    FMaskNumero.Font	:= Value;
end;

function TMaskCombo.GetItemsCount: integer;
begin
	Result := FItems.Count;
end;

destructor TMaskCombo.destroy;
begin
    FCombo.Free;
    FMaskNumero.Free;
    FItems.free;
	inherited Destroy;
end;

procedure TMaskCombo.DoReloadItems;
var
	i: integer;
begin
	FCombo.OnChange := nil;
    FCombo.Items.Clear;
	for i := 0 to Items.Count -1 do begin
    	FCombo.Items.Add(
	       	(FItems.Items[i] As TMaskComboItem).FCaption );
    end;
	FCombo.OnChange := MyComboChange;
    //FCombo.OnChange(self);
end;

function TMaskCombo.GetMaskComboEnabled: Boolean;
begin
	Result := FCombo.Enabled;
end;

procedure TMaskCombo.SetMaskComboEnabled(Value: boolean);
begin
	if FCombo.Enabled = Value then Exit;
    FCombo.Enabled		:= Value;
    FMaskNumero.Enabled	:= Value;
end;

function TMaskCombo.GetColorCombo: TColor;
begin
	Result := FCombo.Color;
end;

procedure TMaskCombo.SetColorCombo(const Value: TColor);
begin
    if FCombo.Color = Value then Exit;
    FCombo.Color		:= Value;
    FMaskNUmero.Color	:= Value;
end;


function TMaskCombo.GetComboText: AnsiString;
begin
	Result := FCombo.Text;
end;


function TMaskCombo.GetMaskText: TMaskedText;
begin
	Result := FMaskNumero.Text;
end;


procedure TMaskCombo.SetMaskText(const Value: TMaskedText);
begin
	if Value = FMaskNumero.Text then Exit;
    FMaskNumero.Text := Value;
end;


{ TMaskComboCombo }
procedure TMaskComboCombo.Resize;
begin
	inherited;
    (Owner as TMaskCombo).MyComboResize;
end;


{ TMaskComboItem }
procedure TMaskComboItem.SetCaption(const Value: TCaption);
begin
	if Value = FCaption then Exit;
    FCaption := Value;
    (Collection as TMaskComboItems).Update(self);
end;

procedure TMaskComboItem.SetMask(const Value: TMaskedText);
begin
	if Value = FMask then Exit;
    FMask := Value;
    (Collection as TMaskComboItems).Update(self);
end;


procedure TMaskComboItem.setValidateRut(const Value: Boolean);
begin
    if FValidateRUT = Value then exit;
    FValidateRUT := value;
end;


{ TMaskComboItems }
constructor TMaskComboItems.Create(AOwner: TPersistent);
begin
	inherited Create(AOwner, TMaskComboItem);
end;

procedure TMaskComboItems.Notify(Item: TCollectionItem;  Action: TCollectionNotification);
begin
	(*
    case Action of
    	cnAdded: ShowMessage('Agregando');
	    cnDeleting: ShowMessage('Eliminando');
  	end;
    *)
    inherited Notify(Item, Action);
end;

procedure TMaskComboItems.Update(Item: TCollectionItem);
begin
	inherited Update(Item);
    (Self.Owner As TMaskCombo).DoReloadItems;
end;

function  TMaskComboItems.Add: TMaskComboItem;
begin
	Result := (inherited Add) As TMaskComboItem;
end;

function TMaskComboItems.GetItem(index: integer): TMaskComboItem;
begin
	Result := (inherited Items[Index]) As TMaskComboItem;
end;

procedure TMaskCombo.setFocus;
begin
  //  inherited;
    FCombo.SetFocus;
end;

procedure TMaskCombo.MyMaskChange(Sender: TObject);
begin
  	if Assigned(FOnChangeMaskCombo) then FOnChangeMaskCombo(self);
end;

procedure TMaskCombo.MyMaskNumeroExit(Sender: TObject);
begin

end;

{ TMaskEditExt }

procedure TMaskEditExt.CMExit(var Message: TCMExit);
begin
    if IsMasked and not (csDesigning in ComponentState) then begin
        CheckCursor;
    end;
end;

function TMaskEditExt.Validate(const Value: string;
  var Pos: Integer): Boolean;
var DigitCheck: AnsiString;
begin
    try
        FOriginalMask := copy(EditMask, 0, length(EditMask));
        if (Parent as TMaskCombo).OmitCharacterRequired then begin
            EditMask := StringReplace(EditMask, 'L', 'l', [rfReplaceAll]);
            EditMask := StringReplace(EditMask, 'A', 'a', [rfReplaceAll]);
            EditMask := StringReplace(EditMask, 'C', 'c', [rfReplaceAll]);
            EditMask := StringReplace(EditMask, '0', '9', [rfReplaceAll]);
        end;

        if not (Parent as TMaskCombo).OmitCharacterRequired and ValidateRUT then begin
            DigitCheck := Copy(trim(text), Length(trim(Text)), 1);
            result := (DigitCheck = ValidRut(Copy(trim(text), 0, Length(trim(Text)) - 1))) and (inherited Validate(Value, Pos));
        end else
            result := inherited Validate(Value, Pos);

    finally
        EditMask := copy(FOriginalMask, 0, Length(FOriginalMask));
    end;
end;


procedure TMaskEditExt.ValidateError;
begin

end;

procedure TMaskCombo.SetOmitCharacterRequired(const Value: Boolean);
begin
    FOmitCharacterRequired := Value;
end;

function TMaskEditExt.ValidRut(RUT: AnsiString): AnsiString;
var
   Cuenta, Suma, totalrut, Revisa : Integer;
begin
    Suma:=2;
    TotalRut:=0;
    for Cuenta := Length(Trim(rut)) downto 1 do begin
         if Suma > 7 then Suma := 2;
         try
             Totalrut := Totalrut + ((StrToInt(copy(rut, cuenta, 1))) * suma);
             Suma:=Suma + 1;
         except
             result := '';
             exit;
         end;
    end;
    Revisa:=Round((frac(Totalrut/11)*10)+0.5);
    Revisa:=11-revisa;
    if Revisa=10 then Result:='K'
    else begin
        If Revisa=11 then Result:='0'
        else Result:=IntToStr(Revisa);
    end;
end;


function TMaskCombo.ValidateMask: Boolean;
var pos: Integer;
begin
    pos:= 0;
	result := FMaskNumero.Validate(FMaskNumero.EditText, pos);
end;

function TMaskCombo.Focused: Boolean;
begin
	Result := (inherited Focused) or FCombo.Focused or FMaskNumero.Focused;
end;

end.
