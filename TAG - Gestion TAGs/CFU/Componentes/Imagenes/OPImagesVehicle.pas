{ *****************************************************************************}
{ Componente para la carga y visualizaci�n de las im�genes de un tr�nsito,     }
{  m�s el combo que permite seleccionar c/tipo.                                }
{																			   }
{ *****************************************************************************}

unit OPImagesVehicle;

interface

uses
  Windows, Messages, SysUtils, Classes, Controls, Forms, ExtCtrls, ADODB, StdCtrls,
  Graphics, ImgTypes, ImgProcs, ProcsOverview, Util, OPImageOverview, Math, ImagePlus,
  UtilProc, dialogs, JPEGPLus;

Type
	tcPos = (cpLeft, cpRight);

Type
  TImagesVehicle = class;

  TConfigurationOverview = class(TPersistent)
  private
	{ Private declarations }
	FEscalaVisual: TEscala;
	FVisualMode: TModoVisual;
	FImagenVehiculo: TImagesVehicle;
	FInteractiveMode: Boolean;
	procedure SetEscala(const Value: TEscala);
	procedure SetVisualMode(const Value: TModoVisual);
	procedure SetInteractiveMode(const Value: Boolean);
  protected
	{ Protected declarations }
  public
	{ Public declarations}
	destructor Destroy; override;
  published
	{ Published declarations }
	constructor Create;
	property VisualMode: TModoVisual read FVisualMode write SetVisualMode default MVDefault;
	property EscalaImage: TEscala read FEscalaVisual write SetEscala default 100;
	property InteractiveMode: Boolean read FInteractiveMode write SetInteractiveMode;
  end;

  TConfigurationJpeg = class(TPersistent)
  private
	{ Private declarations }
    FPositionPlate: TRect;
	FCargarBmp: Boolean;
	FAcumular: Double;
	FImagenVehiculo: TImagesVehicle;
    FCenter: Boolean;
    FStretch: Boolean;
    FUmbralMask: Integer;
    FShift: Byte;
    FShowPositionPlate: Boolean;
    FShowMask: Boolean;
    procedure SetUmbralMask(const Value: Integer);
    procedure SetCenter(const Value: Boolean);
    procedure SetStretch(const Value: Boolean);
    procedure SetShift(const Value: Byte);
    procedure SetShowPositionPlate(const Value: Boolean);
    procedure setPositionPlate(const Value: TRect);
    procedure SetShowMask(const Value: Boolean);
  protected
	{ Protected declarations }
  public
	{ Public declarations}
	destructor Destroy; override;
  published
	{ Published declarations }
	constructor Create;
	property LoadBmp: Boolean read FCargarBmp write FCargarBmp default True;
	property Shift: Byte read FShift write SetShift;
    Property ShowPositionPlate: Boolean read FShowPositionPlate write SetShowPositionPlate;
    property Center: Boolean read FCenter write SetCenter;
    property ShowMask: Boolean read FShowMask write SetShowMask;
    Property PositionPlate: TRect read FPositionPlate write setPositionPlate;
  end;

  TImagesVehicle = class(TCustomControl)
  private
	{ Private declarations }
	FVentana: TPanel;
	FCombo: TComboBox;
	FLastError: AnsiString;
	FADOConnection: TADOConnection;
	FImagenAlmacenable: Boolean;
	FConfigOverview: TConfigurationOverview;
	FTipoImagen: TTipoImagen;
	FOnClick: TNotifyEvent;
	FConfigJpeg: TConfigurationJpeg;
	FExisteImage: Boolean;
	FComboPosition: tcPos;
	FColor: TCOlor;
    FOnDblClick: TNotifyEvent;
	procedure SetADOConnection(const Value: TADOConnection);
	procedure SetConfigOverview(const Value: TConfigurationOverview);
	procedure ComboOnClick(Sender: TObject);
	procedure SetConfigJpeg(const Value: TConfigurationJpeg);
    procedure SetComboPosition(const Value: tcPos);
    procedure SetColor(const Value: TCOlor);
    procedure SetOnDblClick(const Value: TNotifyEvent);
  protected
	{ Protected declarations }
	FFechaHora: TDateTime;
	FImagenComun: TImagePlus;
	FPatenteEsperada: AnsiString;
	FEstadoTransito: TEstadoTransito;
	FImagenOverview: TImageOverview;
	FConcesionaria, FPuntoCobro, FNumeroTransito: Integer;
	function  MostrarImagen(var DescriError: AnsiString; var Error: TTipoErrorImg): Boolean;
	procedure LimpiarCampos;
  public
	{ Public declarations }
	constructor Create(AOwner: TComponent); override;
	destructor  Destroy; override;
	procedure   Resize; override;
	//
	property  LastError: AnsiString Read FLastError;
	property  ImagenAlmacenable: Boolean Read FImagenAlmacenable;
	property  ExistImage: Boolean Read FExisteImage;
	property  TypeImage: TTipoImagen Read FTipoImagen;
	function  LoadImages(Concesionaria, PuntoCobro, NumeroTransito: Integer; EstadoTransito: TEstadoTransito; FechaHora: TDateTime; PatenteEsperada: AnsiString = ''; TipoImagen: TTipoImagen = tiFrontal): Boolean; virtual;
    procedure GetImagePlate(bmp: TBitMap);
	function  ReadBitmap: TBitmap;
    procedure GetImage(Bmp: TBitMap);
    procedure GetImageJPG12(JPG12: TJPEGPlusImage);
	procedure IntensityChange(Value: Integer; SobreImgOriginal: Boolean);
	procedure ContrastChange(Value: Integer; SobreImgOriginal: Boolean);
	procedure RealceChange(Value: Integer; SobreImgOriginal: Boolean);
	procedure Sharp(SobreImgOriginal: Boolean);
	procedure Edges(SobreImgOriginal: Boolean);
	procedure Reset;
  published
	{ Published declarations }
	property Width default 200;
	property ADOConnection: TADOConnection read FADOConnection write SetADOConnection;
	property ConfigOverview: TConfigurationOverview read FConfigOverview write SetConfigOverview;
	property ConfigJpeg: TConfigurationJpeg read FConfigJpeg write SetConfigJpeg;
	property TipoImagen: TTipoImagen read FTipoImagen;
	property ComboPosition: tcPos read FComboPosition write SetComboPosition default cpLeft;
	property Color: TColor read FColor write SetColor default clBtnFace;
	property OnClick: TNotifyEvent read FOnClick write FOnClick;
	property OnDblClick: TNotifyEvent read FOnDblClick write SetOnDblClick;
	property OnResize;
	//
	property Align;
	property Anchors;
	property AutoSize;
	property Constraints;
	property DockSite;
	property DragCursor;
	property DragKind;
	property DragMode;
	property Enabled;
	property Ctl3D;
	property Font;
	property ParentBiDiMode;
	property ParentColor;
	property ParentCtl3D;
	property ParentFont;
	property ParentShowHint;
	property PopupMenu;
	property ShowHint;
	property TabOrder;
	property TabStop;
	property Visible;
	//
	property OnCanResize;
	property OnConstrainedResize;
	property OnContextPopup;
	property OnDockDrop;
	property OnDockOver;
	property OnDragDrop;
	property OnDragOver;
	property OnEndDock;
	property OnEndDrag;
	property OnEnter;
	property OnExit;
	property OnGetSiteInfo;
	property OnMouseDown;
	property OnMouseMove;
	property OnMouseUp;
	property OnStartDock;
	property OnStartDrag;
	property OnUnDock;  
  end;

	  
implementation

{ ImageVehicle }

constructor TImagesVehicle.Create(AOwner: TComponent);
begin
	inherited Create(AOwner);
	ControlStyle  := ControlStyle - [csAcceptsControls];
	Font.Size     := 8;
	ComboPosition := cpLeft;
	FColor 		  := clBtnFace;
	//
	FCombo := TComboBox.Create(Self);
	FCombo.Parent  := Self;
	FCombo.Top 	   := 1;
	FCombo.Left	   := 1; 
	FCombo.Width   := 170;
	FCombo.Style   := csDropDownList;
	FCombo.Font    := Font;
	FCombo.OnClick := ComboOnClick;
	//
	FVentana := TPanel.Create(Self);
	FVentana.Parent 	  := Self;
	FVentana.Top    	  := FCombo.Top + FCombo.Height + 5;
	FVentana.Left   	  := FCombo.Left;
	FVentana.Width  	  := Width - 1; 						
	FVentana.Height		  := Height - FVentana.Top - 1; 	
	FVentana.BevelOuter   := bvNone; //bvLowered;
	FVentana.BorderStyle  := bsNone; 
	FVentana.Visible	  := True;
	FVentana.ControlStyle := ControlStyle - [csAcceptsControls];
	//
	// Crear las dos im�genes posibles. Una visible por vez.
	FImagenComun := TImagePlus.Create(Self);                 
	FImagenComun.Parent  := FVentana;
	FImagenComun.Visible := True;
	FImagenComun.Top 	 := 0; 
	FImagenComun.Left	 := 0; 
	FImagenComun.Width	 := FVentana.Width; 
	FImagenComun.Height  := FVentana.Height; 
	FImagenComun.Transparent := True;
	FImagenComun.Color 	 := FColor;
	//
	FImagenOverview := TImageOverview.Create(Self);
	FImagenOverview.Parent  := FVentana;
	FImagenOverview.Visible := False;
	FImagenOverview.Top 	:= 0;
	FImagenOverview.Left	:= 0;
	FImagenOverview.Width	:= FVentana.Width;
	FImagenOverview.Height  := FVentana.Height;
	//
	FConfigOverview := TConfigurationOverview.Create;
	FConfigOverview.FImagenVehiculo := Self;
	//
	FConfigJpeg := TConfigurationJpeg.Create;
	FConfigJpeg.FImagenVehiculo := Self;
	//
	FExisteImage := False;
	Invalidate;
end;

destructor TImagesVehicle.Destroy;
begin
	FConfigJpeg.Free;
	FConfigOverview.Free;
	FImagenOverview.Free;
	FImagenComun.Free;
	FCombo.Free;
	FVentana.Free; 
	//
	inherited Destroy;
end;

function TImagesVehicle.LoadImages(Concesionaria, PuntoCobro, NumeroTransito: Integer; EstadoTransito: TEstadoTransito; 
  FechaHora: TDateTime; PatenteEsperada: AnsiString = ''; TipoImagen: TTipoImagen = tiFrontal): Boolean;
begin
	try
		if not Assigned(FADOConnection) then
			raise exception.Create('Falta el par�metro ADOConnection');
		//
		FConcesionaria   := Concesionaria;
		FPuntoCobro      := PuntoCobro;
		FNumeroTransito  := NumeroTransito;
		FPatenteEsperada := PatenteEsperada;
		FEstadoTransito	 := EstadoTransito;
		FFechaHora		 := FechaHora;
		if FEstadoTransito = etPendiente then begin
			CargarComboImgPendientes(FADOConnection, FCombo, FConcesionaria, FPuntoCobro, FNumeroTransito, TipoImagen);
		end else begin
			CargarComboImgViajes(FADOConnection, FCombo, FConcesionaria, FPuntoCobro, FNumeroTransito, FechaHora, TipoImagen);
		end;
		FExisteImage 	 := FCombo.Items.Count > 0;
		FCombo.Enabled	 := FCombo.Items.Count > 0;
		ComboOnClick(Self);
		Result := True;
	except
		on e:exception do begin
			FLastError := e.message;
			Result := False;
		end;
	end;
end;

procedure TImagesVehicle.SetADOConnection(const Value: TADOConnection);
begin
	FADOConnection := Value;
end;

procedure TImagesVehicle.ComboOnClick(Sender: TObject);
var
	Error: TTipoErrorImg;
	DescriError, Extension: AnsiString; 
begin
	Extension   := Copy(FCombo.Text, 201, 4);
	FTipoImagen := TTipoImagen(IVal(Extension));
	if not MostrarImagen(DescriError, Error) then FlastError := DescriError;
	//
	if Assigned(FOnClick) then FOnClick(Self);
end;

function TImagesVehicle.MostrarImagen(var DescriError: AnsiString; var Error: TTipoErrorImg): Boolean;
var
	TipoImg: TTipoImagen;
	ImagenActual: TBitmap;
    JPG12: TJpegPlusImage;
    DataImage: TDataImage;
begin
	ImagenActual := TBitmap.Create;
	try
		// Buscar y mostrar la imagen de la posici�n seleccionada.
		TipoImg := FTipoImagen;
		if TipoImg in [tiOverview1, tiOverview2, tiOverview3] then begin
			FImagenComun.Visible	:= False;
  			if not FImagenOverview.LoadImage(FADOConnection, FConcesionaria, FPuntoCobro,
              FNumeroTransito, FFechaHora, FPatenteEsperada, TipoImg) then begin
 	    		FLastError := FImagenOverview.LastError;
  		    end;
           	FImagenOverview.Visible  := True;
		end else begin
			FImagenOverview.Visible  := False;
			FImagenComun.Visible	 := True;
			FImagenComun.Stretch     := True;
			FImagenComun.Transparent := True;
			FImagenComun.Picture.Assign(nil);
			FImagenAlmacenable := False;
			if FEstadoTransito = etPendiente then begin
                if TipoImg in [tiOverview1, tiOverview2, tiOverview3] then begin
    				if not ObtenerOverviewTransitoPendiente(FADOConnection, FConcesionaria, FPuntoCobro, FNumeroTransito,
	    			  TipoImagen, ImagenActual, DataImage, DescriError, Error) then raise exception.Create(DescriError);
                end else begin
                    try
                        JPG12 := TJPEGPlusImage.Create;
                        if not ObtenerImagenTransitoPendiente(FADOConnection, FConcesionaria, FPuntoCobro, FNumeroTransito, TipoImagen,
                          JPG12, DataImage, DescriError, Error) then raise exception.Create(DescriError);
            			FImagenComun.Assign(JPG12);
                        ConfigJPeg.PositionPlate := Rect(DataImage.DataImageVR.UpperLeftLPN.X,
                           DataImage.DataImageVR.UpperLeftLPN.Y, DataImage.DataImageVR.UpperRigthLPN.X,
                           dataImage.DataImageVR.LowerRigthLPN.Y);
                        ConfigJPeg.Shift := FImagenComun.Shift;
                    finally
                        JPG12.free;
                    end;
                end;
			end else begin
				if not ObtenerImagenTransito(FADOConnection, FConcesionaria, FPuntoCobro, FNumeroTransito, FFechaHora,
				  TipoImg, ImagenActual, DescriError, Error, FConfigJpeg.FAcumular) then raise exception.Create(DescriError);
	    		FImagenComun.Picture.Assign(ImagenActual);
			end;
		   	FImagenComun.Transparent := False;
		end;
		// Ver si esa imagen es almacenable. // Cambiar
		if (TipoImg = tiFrontal) or (TipoImg = tiPosterior) then FImagenAlmacenable := True;
		//
		Result := True;
	except
		on e:exception do begin
			FLastError := e.message;
			Result := False;
		end;
	end;
	ImagenActual.Free;
end;

procedure TImagesVehicle.SetConfigOverview(const Value: TConfigurationOverview);
begin
	FConfigOverview := Value;
end;

procedure TImagesVehicle.Resize;
begin
	inherited;
	FVentana.Width  		:= Width - 1; // - 2; 						
	FVentana.Height			:= Height - FVentana.Top - 1; // - 2;
	FImagenComun.Width	 	:= FVentana.Width; // - 2;
	FImagenComun.Height  	:= FVentana.Height; // - 2;
	FImagenOverview.Width	:= FVentana.Width;
	FImagenOverview.Height  := FVentana.Height; 
	FVentana.BevelOuter 	:= bvNone; //bvLowered;
	if Width <= FCombo.Width then FCombo.Width := Width - 2;
	if Width > (FCombo.Width + FCombo.Width * 0.5) then FCombo.Width := Min((Width - 40), 200);//- 20), 220)
	if FComboPosition = cpLeft then begin
		FCombo.Left := 1; 
	end;
	if FComboPosition = cpRight then begin
		FCombo.Left := Width - FCombo.Width - 1;
	end; 
end;

procedure TImagesVehicle.SetConfigJpeg(const Value: TConfigurationJpeg);
begin
	FConfigJpeg := Value;
end;

procedure TImagesVehicle.IntensityChange(Value: Integer; SobreImgOriginal: Boolean);
begin
	if (Value < 0) or (Value > 100) then Exit;
	FImagenComun.AplicarBrillo(Value, SobreImgOriginal);	
end;

procedure TImagesVehicle.ContrastChange(Value: Integer; SobreImgOriginal: Boolean);
begin
	if (Value < 0) or (Value > 100) then Exit;
	FImagenComun.AplicarContraste(Value, SobreImgOriginal);	
end;

procedure TImagesVehicle.RealceChange(Value: Integer; SobreImgOriginal: Boolean);
begin
	if (Value < 0) or (Value > 100) then Exit;
	FImagenComun.AplicarRealce(Value, SobreImgOriginal);	
end;

procedure TImagesVehicle.Edges(SobreImgOriginal: Boolean);
begin
	FImagenComun.AplicarBordes(SobreImgOriginal);	
end;

procedure TImagesVehicle.Sharp(SobreImgOriginal: Boolean);
begin
	FImagenComun.AplicarSharp(SobreImgOriginal);	
end;

procedure TImagesVehicle.Reset;
{var
	Error: TTipoErrorImg;
	DescriError: AnsiString;}
begin
	LimpiarCampos;
	//MostrarImagen(DescriError, Error);
end;

procedure TImagesVehicle.LimpiarCampos;
begin
	FConcesionaria   := 0;
	FPuntoCobro      := 0;
	FNumeroTransito  := 0;
	FPatenteEsperada := '';
	FExisteImage 	 := False;
	FCombo.Clear; 
	FImagenComun.Transparent := True;
	FImagenComun.Picture.Assign(nil);
	FImagenOverview.Reset;
end;

function TImagesVehicle.ReadBitmap: TBitmap;
resourceString
	MSG_ERRORBMP = 'Error al retornar Bmp.';
begin
	try
		Result := FImagenComun.Picture.Bitmap;
	except
		Result := nil;
		FLastError := MSG_ERRORBMP;
	end;
end;


{ TConfigurationJpeg }

constructor TConfigurationJpeg.Create;
begin
	inherited Create;
	FCargarBmp := True;
	FAcumular  := 100;
end;

destructor TConfigurationJpeg.Destroy;
begin
  inherited Destroy;
end;

procedure TConfigurationJpeg.SetCenter(const Value: Boolean);
begin
    FCenter := Value;
    FImagenVehiculo.FImagenComun.Center := FCenter;
end;

procedure TConfigurationJpeg.SetUmbralMask(const Value: Integer);
begin
end;

procedure TConfigurationJpeg.SetShift(const Value: Byte);
begin
    FShift := Value;
    FImagenVehiculo.FImagenComun.Shift := FShift;
end;

procedure TConfigurationJpeg.SetStretch(const Value: Boolean);
begin
    FStretch := Value;
    FImagenVehiculo.FImagenComun.Stretch := FStretch;
end;

procedure TConfigurationJpeg.SetShowPositionPlate(const Value: Boolean);
begin
    FShowPositionPlate := Value;
    FImagenVehiculo.FImagenComun.ShowPositionPlate := FShowPositionPlate;
end;

procedure TConfigurationJpeg.setPositionPlate(const Value: TRect);
begin
    FPositionPlate := Value;
    FImagenVehiculo.FImagenComun.PositionPlate := Value;
end;

procedure TConfigurationJpeg.SetShowMask(const Value: Boolean);
begin
    FShowMask := Value;
    FImagenVehiculo.FImagenComun.ShowMask := FShowMask;
end;

{ TConfigurationOverview }

constructor TConfigurationOverview.Create;
begin
	inherited Create;
	FEscalaVisual := 100;
	FVisualMode   := MVDefault;
end;

destructor TConfigurationOverview.Destroy;
begin
	inherited Destroy;
end;

procedure TConfigurationOverview.SetEscala(const Value: TEscala);
begin
	FEscalaVisual := Value;
	FImagenVehiculo.FImagenOverview.EscalaImage := FEscalaVisual;
end;

procedure TConfigurationOverview.SetInteractiveMode(const Value: Boolean);
begin
	FInteractiveMode := Value;
	FImagenVehiculo.FImagenOverview.InteractiveMode := FInteractiveMode;
end;

procedure TConfigurationOverview.SetVisualMode(const Value: TModoVisual);
begin
	FVisualMode := Value;
	FImagenVehiculo.FImagenOverview.VisualMode := FVisualMode;
end;
  
procedure TImagesVehicle.SetComboPosition(const Value: tcPos);
begin
	if Value = FComboPosition then Exit;
	FComboPosition := Value;
	if FComboPosition = cpLeft then begin
		FCombo.Left := 1;
	end;
	if FComboPosition = cpRight then begin 
		FCombo.Left := Width - FCombo.Width - 1;
	end; 
end;

procedure TImagesVehicle.SetColor(const Value: TColor);
begin
	if FColor = Value then Exit;
	FColor := Value;
	FImagenComun.Color := Value;
end;

procedure TImagesVehicle.SetOnDblClick(const Value: TNotifyEvent);
begin
	FOnDblClick := Value;
end;

procedure TImagesVehicle.GetImagePlate(bmp: TBitMap);
begin
    FImagenComun.GetImagePlate(bmp);
end;

procedure TImagesVehicle.GetImage(Bmp: TBitMap);
begin
    FIMagenComun.GetImage(Bmp);
end;

procedure TImagesVehicle.GetImageJPG12(JPG12: TJPEGPlusImage);
begin
    FIMagenComun.GetImageJPG12(JPG12);
end;

end.
