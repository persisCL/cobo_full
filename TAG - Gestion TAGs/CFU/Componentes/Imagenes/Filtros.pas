unit Filtros;

interface

uses
	Windows, Messages, SysUtils, Graphics, Dialogs, Math, Classes, DMIGDI;

Type
	TPixel = record
	  B, G, R: BYTE;
	end;
	
	TMatriz = Record
	  Width, Height: Integer;
	  Data: Array of Array of TPixel; //Array [0..1000, 0..1000] of TPixel;
	end;
	
	TKernel = array [0..2,0..2] of Single;
  	TK_1 = array [0..4,0..4] of Single;

	tFiltro = (tfBrillo, tfContraste, tfRealce, tfSharp, tfBordes);

const
	BRILLO_PEND_OSC		= 0.064;
	BRILLO_DEZPL_OSC 	= -0.7;
	//
	BRILLO_PEND	 		= 0.016;	//0.01;		//0.005;
	BRILLO_DEZPL		= 0.2;		//0.5; 		//0.75;
	CONTRASTE_PEND 		= 0.012;
	CONTRASTE_DEZPL		= 0.4;
//
	KEdge : TKernel = ((-1.5,-1.5,-1.5),(-1.5,12,-1.5),(-1.5,-1.5,-1.5));
   	KSharp : TKernel = ((0,-1,0),(-1, 5,-1),(0,-1,0));
    K1 : TK_1 = ((-2, -2, -1, -2, -2),
                 (-2, -1, -1, -1, -2),
                 (-1, -1, 25, -1, -1),
                 (-2, -1, -1, -1, -2),
                 (-2, -2, -1, -2, -2));
  	KBlurr : TKernel = ((0.02,0.4,0.02),(0.4,0.,0.4),(0.02,0.04,0.02));

procedure LoadMatrix(Bitmap: TBitmap; var Matrix: TMatriz);
procedure SaveMatrix(Bitmap: TBitmap; const Matrix: TMatriz);
procedure AplicarEfecto(const Source:TMatriz; var Dest: TMatriz; Kernel: TKernel; N: Integer);
procedure AplicarEfecto1(const Source:TMatriz; var Dest: TMatriz; Kernel: TK_1; N: Integer);
//
procedure Brillo(Bmp: TBitmap; Valor: Double);
procedure Contraste(Bmp: TBitmap; Valor: Double);
procedure Realce(Bmp: TBitmap; Valor: Double);
procedure Sharp(Bmp: TBitmap);
procedure Sharp1(Bmp: TBitmap);
procedure Blurr(Bmp: TBitmap);
procedure Bordes(Bmp: TBitmap);
procedure ExpandirHistograma(Bmp: TBitmap);
procedure HistogramaAcumulado(Bmp: TBitmap; Valor: Double);
procedure HistogramaOscuro(Bmp: TBitmap; Valor: Double);
procedure RaizCuadrada(Bmp: TBitmap; Valor: Double);
procedure BlackWhite(Bmp: TBitmap; Valor: Double);
procedure DrawMask(bmp: TBitMap; limite: Integer);
procedure DrawRectPlate(bmp: TBitMap; aRect: TRect; aColor: TColor);

implementation


procedure DrawMask(bmp: TBitMap; Limite: INteger);
var
    bmpSource: TBitMap;
    LimiteMascara, Ancho, Alto: Integer;
begin
    // Si la imagen esta vacia nos vamos
    if bmp.Height = 0 then exit;

    if Limite <= 0 then // no hay coord. de patente, enmascara la mitad de la imagen
	    LimiteMascara := (bmp.Height div 2)
    else
	    LimiteMascara := Min(Max(Limite, 0), bmp.Height);
    // Creamos los bmp auxiliares
    bmpSource := TBitMap.Create;
    try
        Ancho := Bmp.Width;
        Alto := LimiteMascara;
        bmpSource.Assign(bmp);
        AlphaBitmap(bmpSource, clBlack, bmp, Rect(0, 0, Ancho, Alto), 150);
   finally
        bmpSource.Free;
   end;
end;

procedure DrawRectPlate(bmp: TBitMap; aRect: TRect; aColor: TColor);
begin
    bmp.Canvas.Pen.color := aColor;
    bmp.Canvas.Pen.Width := 2;
    bmp.Canvas.PolyLine([Point(aRect.Left - 6, aRect.Top - 6),
      Point(aRect.Left - 6, aRect.Bottom + 6),
      Point(aRect.Right + 6, aRect.Bottom + 6),
      Point(aRect.Right + 6, aRect.Top - 6),
      Point(aRect.Left - 6, aRect.Top - 6)]);
end;

procedure ObtenerConfigImagen(Bmp: TBitmap; var Ancho: Integer);
begin
	if Bmp.PixelFormat = pf8bit then ancho := Bmp.Width-1
	  else ancho := (Bmp.Width * 3)-1;
end;

procedure Brillo(Bmp: TBitmap; Valor: Double);
Var
	P: PByteArray;
	Factor: Double;
	i, j, val, ancho: Integer;
begin
	try
		Factor := Valor * BRILLO_PEND + BRILLO_DEZPL;
		ObtenerConfigImagen(Bmp, Ancho);
		//
		for i := 0 to (Bmp.Height-1) do begin
			P := Bmp.ScanLine[i];
			j := 0;
			while j < ancho do begin
				val := Round(P[j] * Factor);
				if val < 0 then val := 0;
				if val > 255 then val := 255;
				P[j]   := val;
				Inc(j);
			end;
		end;
	except
	end;
end;

procedure Contraste(Bmp: TBitmap; Valor: Double);
var
	P: PByteArray;
	Factor: Double;
	i, j, val, Dev, ancho:Integer;
begin
	try
		Factor := Valor * CONTRASTE_PEND + CONTRASTE_DEZPL;
		ObtenerConfigImagen(Bmp, Ancho);
		//
		for i := 0 to (Bmp.Height-1) do begin
			P := Bmp.ScanLine[i];
			j := 0;
			while j < ancho do begin   
				Dev := P[j] - 128;
				val := 128 + Round(Dev * Factor);
				if val < 0 then val := 0;
				if val > 255 then val := 255;
				P[j]   := val;
				Inc(j);
			end;
		end;
	except
	end;
end;

procedure Realce(Bmp: TBitmap; Valor: Double);
Var
	P: PByteArray;
	Factor: Double;
	i, j, val, ancho: Integer;
begin
	try
		Factor := Valor * BRILLO_PEND_OSC + BRILLO_DEZPL_OSC;
		//Factor := 0.27; //Valor * 0.064 + (-0.7);
		ObtenerConfigImagen(Bmp, Ancho);
		//
		for i := 0 to (Bmp.Height-1) do begin
			P := Bmp.ScanLine[i];
			j := 0;
			while j < ancho do begin         
				val := Round(P[j] * Factor);
				if val < 0 then val := 0;
				if val > 255 then val := 255;
				P[j]   := val;
				Inc(j);
			end;
		end;
	except
	end;
end;

procedure Sharp(Bmp: TBitmap);
var
	MatrizO, MatrizR: TMatriz;
begin
	try
		LoadMatrix(Bmp, MatrizO);
		AplicarEfecto(MatrizO, MatrizR, KSharp, 3);
		SaveMatrix(Bmp, MatrizR);
	except
	end;
end;

procedure Sharp1(Bmp: TBitmap);
var
	MatrizO, MatrizR: TMatriz;
begin
	try
		LoadMatrix(Bmp, MatrizO);
		AplicarEfecto1(MatrizO, MatrizR, K1, 3);
		SaveMatrix(Bmp, MatrizR);
	except
	end;
end;


procedure Bordes(Bmp: TBitmap);
var
	MatrizO, MatrizR: TMatriz;
begin
	try
		LoadMatrix(Bmp, MatrizO);
		AplicarEfecto(MatrizO, MatrizR, KEdge, 3);
		SaveMatrix(Bmp, MatrizR);
	except
	end;
end;

procedure Blurr(Bmp: TBitmap);
var
	MatrizO, MatrizR: TMatriz;
begin
	try
		LoadMatrix(Bmp, MatrizO);
		AplicarEfecto(MatrizO, MatrizR, KBLurr, 3);
		SaveMatrix(Bmp, MatrizR);
	except
	end;
end;


procedure LoadMatrix(Bitmap: TBitmap; var Matrix: TMatriz);
Var
	P: Pointer;
	y: Integer;
begin
	SetLength(Matrix.Data, Bitmap.Height);
	// 
	Matrix.Width  := Bitmap.Width;
	Matrix.Height := Bitmap.Height;
	for y := 0 to Bitmap.Height - 1 do begin
		SetLength(Matrix.Data[y], Bitmap.Width);
		P := Bitmap.ScanLine[y];
		Move(P^, Matrix.Data[y, 0], Bitmap.Width * 3);
	end;
end;

procedure SaveMatrix(Bitmap: TBitmap; const Matrix: TMatriz);
Var
	y: Integer;
	P: Pointer;
begin
	Bitmap.Width  := Matrix.Width;
	Bitmap.Height := Matrix.Height;
	for y := 0 to Bitmap.Height - 1 do begin
		P := Bitmap.ScanLine[y];
		Move(Matrix.Data[y, 0], P^, Bitmap.Width * 3);
	end;
	If Assigned(Bitmap.OnChange) then Bitmap.OnChange(Bitmap);
end;

procedure AplicarEfecto(const Source:TMatriz; var Dest: TMatriz; Kernel: TKernel; N: Integer);
Var
	d, i, j, x, y: Integer;
	RR, GG, BB: Single;

begin
	d := trunc((N - 1) / 2);
	Dest.Width := Source.Width;
	Dest.Height := Source.Height;
	SetLength(Dest.Data, Source.Height);
	//
	SetLength(Dest.Data[0], Source.Width);
	//
	for y := 1 to (Source.Height - 1 - d) do begin     
		SetLength(Dest.Data[y], Source.Width);
		for x := 1 to (Source.Width - 1 - d) do begin
			RR := 0 ; 
			GG := 0 ; 
			BB := 0 ; 
			//
			for i := 0 to 2 do
				for j := 0 to 2 do begin
					RR := RR + Kernel[i,j] * Source.data[y + i - d, x + j - d].R;
					GG := GG + Kernel[i,j] * Source.data[y + i - d, x + j - d].G;
					BB := BB + Kernel[i,j] * Source.data[y + i - d, x + j - d].B;
				end;
			//
			if RR < 0 then RR := 0; if RR > 255 then RR := 255;
			if GG < 0 then GG := 0; if GG > 255 then GG := 255;
			if BB < 0 then BB := 0; if BB > 255 then BB := 255;
			//
			Dest.data[y, x].R := Round(RR);
			Dest.data[y, x].G := Round(GG);
			Dest.data[y, x].B := Round(BB);
		end;
	end;
	SetLength(Dest.Data[Source.Height-1], Source.Width);
end;


procedure AplicarEfecto1(const Source:TMatriz; var Dest: TMatriz; Kernel: TK_1; N: Integer);
Var
	d, i, j, x, y: Integer;
	RR, GG, BB: Single;

begin
	d := trunc((N - 1) / 2);
	Dest.Width := Source.Width;
	Dest.Height := Source.Height;
	SetLength(Dest.Data, Source.Height);
	//
	SetLength(Dest.Data[0], Source.Width);
	//
	for y := 1 to (Source.Height - 1 - d) do begin     
		SetLength(Dest.Data[y], Source.Width);
		for x := 1 to (Source.Width - 1 - d) do begin
			RR := 0 ; 
			GG := 0 ; 
			BB := 0 ; 
			//
			for i := 0 to 2 do
				for j := 0 to 2 do begin
					RR := RR + Kernel[i,j] * Source.data[y + i - d, x + j - d].R;
					GG := GG + Kernel[i,j] * Source.data[y + i - d, x + j - d].G;
					BB := BB + Kernel[i,j] * Source.data[y + i - d, x + j - d].B;
				end;
			//
			if RR < 0 then RR := 0; if RR > 255 then RR := 255;
			if GG < 0 then GG := 0; if GG > 255 then GG := 255;
			if BB < 0 then BB := 0; if BB > 255 then BB := 255;
			//
			Dest.data[y, x].R := Round(RR);
			Dest.data[y, x].G := Round(GG);
			Dest.data[y, x].B := Round(BB);
		end;
	end;
	SetLength(Dest.Data[Source.Height-1], Source.Width);
end;


procedure ExpandirHistograma(Bmp: TBitmap);
var
	P: PByteArray;	
	factor: Double;
	min, max, i, j, val, ancho: Integer;
begin
	try  
		min := 255;
		max := 0;
		// Calcular Histograma
		ObtenerConfigImagen(Bmp, Ancho);
		//
		for i := 0 to (Bmp.Height-1) do begin
			P := Bmp.ScanLine[i];
			j := 0;
			while j < ancho do begin         
				if P[j] < min then min := P[j];
				if P[j] > max then max := P[j];
				Inc(j);
			end;
		end;
		// Aplicar a la imagen
		factor := ((255 / (max - min)) - min);
		for i := 0 to (Bmp.Height-1) do begin
			P := Bmp.ScanLine[i];
			j := 0;
			while j < ancho do begin         
				 val := Round(P[j] * factor);
				 if val < 0 then val := 0;
				 if val > 255 then val := 255;   
				 P[j] := val;
				//
				Inc(j);
			end;
		end;
	except
	end;
end;


procedure HistogramaAcumulado(Bmp: TBitmap; Valor: Double);
var
	Histo: Array[0..255] of Integer;
	P: PByteArray;	
	factor, ExpMax, hAcum: Double;
	min, max, i, j, val, ancho: Integer;
begin
	try
		min := 255;
		for i := 0 to 255 do Histo[i] := 0;
		// Calcular Histograma
		ObtenerConfigImagen(Bmp, Ancho);
		for i := 0 to (Bmp.Height-1) do begin
			P := Bmp.ScanLine[i];
			// Armar el Histograma
			j := 0;
			while j < ancho do begin         
				Histo[P[j]] := Histo[P[j]] + 1;
				// Calcular el m�nimo valor
				if P[j] < Min then Min := P[j];
				j := j+3;
			end;
		end;
		// Buscar en el Histograma el valor de Intensidad que acumula el valor parametro
		ExpMax := (Int(Bmp.Height * Bmp.Width) * Valor) / 100;
		i      := 0;
		hAcum  := 0;
		while hAcum < ExpMax do begin
			hAcum := hAcum + Histo[i];
			Inc(i);
		end;
		Max := (i - 1);	//por si es 0
		// Calcular el factor para pasar a 8 bits
		if Max = 0 then max := 1;
		Factor := 255 / (Max - Min);
		// Aplicar a la imagen
		for i := 0 to (Bmp.Height-1) do begin
			P := Bmp.ScanLine[i];
			j := 0;
			while j < ancho do begin
				 val := Round(P[j] * factor);
				 if val < 0 then val := 0;
				 if val > 255 then val := 255;
				 P[j] := val;
				 P[j+1] := val;
				 P[j+2] := val;
				//
				j := j+3;
			end;
		end;
	except
	end;
end;


procedure HistogramaOscuro(Bmp: TBitmap; Valor: Double);
var
	Histo: Array[0..255] of Integer;
	P: PByteArray;
	factor, ExpMax, hAcum: Double;
	min, i, j, val, ancho: Integer;
begin
	try
		min := 255;
		for i := 0 to 255 do Histo[i] := 0;
		// Calcular Histograma
		ObtenerConfigImagen(Bmp, Ancho);
		for i := 0 to (Bmp.Height-1) do begin
			P := Bmp.ScanLine[i];
			// Armar el Histograma
			j := 0;
			while j < ancho do begin
				Histo[P[j]] := Histo[P[j]] + 1;
				// Calcular el m�nimo valor
				if P[j] < Min then Min := P[j];
				j := j+3;
			end;
		end; 
		// Buscar en el Histograma el valor de Intensidad que acumula el valor parametro
		ExpMax := (Int(Bmp.Height * Bmp.Width) * Valor) / 100;
		i      := 255;
		hAcum  := 0;
		while hAcum < ExpMax do begin
			hAcum := hAcum + Histo[i];
			i := i - 1;
		end;
		Min := (i + 1);	
		// Calcular el factor para pasar a 8 bits 
		try
            Factor := 255 / (Min);
        except
            Factor := 255;
        end;
		// Aplicar a la imagen
		for i := 0 to (Bmp.Height-1) do begin
			P := Bmp.ScanLine[i];
			j := 0;
			while j < ancho do begin         
				 val := Round(P[j] * factor);
				 if val < 0 then val := 0;
				 if val > 255 then val := 255;   
				 P[j] := val;
				 P[j+1] := val;
				 P[j+2] := val;
				//
				j := j+3;
			end;
		end;
	except
	end;
end;

procedure RaizCuadrada(Bmp: TBitmap; Valor: Double);
Var
	P: PByteArray;
	i, j, val, ancho: Integer;
begin
	try
		ObtenerConfigImagen(Bmp, Ancho);
		//
		for i := 0 to (Bmp.Height-1) do begin
			P := Bmp.ScanLine[i];
			j := 0;
			while j < ancho do begin
				val := Round(SQRT(P[j]));
				if val < 0 then val := 0;
				if val > 255 then val := 255;
				P[j]   := val;
				Inc(j);
			end;
		end;
	except
	end;
end;


procedure BlackWhite(Bmp: TBitmap; Valor: Double);
Var
	P: PByteArray;
	i, j, ancho: Integer;
begin
	try
		ObtenerConfigImagen(Bmp, Ancho);
		//
		for i := 0 to (Bmp.Height-1) do begin
			P := Bmp.ScanLine[i];
			j := 0;
			while j < ancho do begin
				if P[j] < valor then P[j] := 0
                else P[j] := 255;
				Inc(j);
			end;
		end;
	except
	end;
end;


end.
