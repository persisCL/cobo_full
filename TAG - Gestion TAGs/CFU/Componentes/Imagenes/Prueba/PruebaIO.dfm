object Form1: TForm1
  Left = 207
  Top = 198
  Width = 725
  Height = 485
  AutoSize = True
  Caption = 'Prueba - Componentes Imagenes'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 417
    Width = 717
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    BiDiMode = bdLeftToRight
    ParentBiDiMode = False
    TabOrder = 0
    object Button3: TButton
      Left = 629
      Top = 10
      Width = 75
      Height = 25
      Caption = 'Salir'
      TabOrder = 0
      OnClick = Button3Click
    end
  end
  object Button4: TButton
    Left = 445
    Top = 0
    Width = 99
    Height = 25
    Caption = 'Cargar Imagenes'
    TabOrder = 2
    OnClick = Button4Click
  end
  object btn_I12: TButton
    Left = 445
    Top = 40
    Width = 99
    Height = 25
    Caption = 'Solo Imagen'
    Enabled = False
    TabOrder = 4
    OnClick = btn_I12Click
  end
  object btn_G12: TButton
    Left = 445
    Top = 80
    Width = 99
    Height = 25
    Caption = 'Solo Gr'#225'fico'
    Enabled = False
    TabOrder = 1
    OnClick = btn_G12Click
  end
  object btn_IG12: TButton
    Left = 445
    Top = 120
    Width = 99
    Height = 25
    Caption = 'Imagen + Gr'#225'fico'
    Enabled = False
    TabOrder = 3
    OnClick = btn_IG12Click
  end
  object btn_gamma: TButton
    Left = 493
    Top = 200
    Width = 71
    Height = 25
    Caption = 'Realce'
    TabOrder = 6
    OnClick = btn_gammaClick
  end
  object btn_inten: TButton
    Left = 491
    Top = 240
    Width = 71
    Height = 25
    Caption = 'Brillo'
    TabOrder = 8
    OnClick = btn_intenClick
  end
  object btn_con: TButton
    Left = 491
    Top = 280
    Width = 71
    Height = 25
    Caption = 'Contraste'
    TabOrder = 10
    OnClick = btn_conClick
  end
  object int: TNumericEdit
    Left = 440
    Top = 240
    Width = 33
    Height = 21
    MaxLength = 3
    TabOrder = 7
    Decimals = 0
  end
  object con: TNumericEdit
    Left = 440
    Top = 280
    Width = 34
    Height = 21
    MaxLength = 3
    TabOrder = 9
    Decimals = 0
  end
  object gam: TNumericEdit
    Left = 440
    Top = 200
    Width = 33
    Height = 21
    MaxLength = 3
    TabOrder = 5
    Decimals = 0
  end
  object Button1: TButton
    Left = 491
    Top = 320
    Width = 71
    Height = 25
    Caption = 'Reset'
    TabOrder = 11
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 584
    Top = 200
    Width = 113
    Height = 25
    Caption = 'Realce sobre modif.'
    TabOrder = 12
    OnClick = Button2Click
  end
  object Button5: TButton
    Left = 584
    Top = 240
    Width = 113
    Height = 25
    Caption = 'Brillo sobre modif.'
    TabOrder = 13
    OnClick = Button5Click
  end
  object Button6: TButton
    Left = 584
    Top = 280
    Width = 113
    Height = 25
    Caption = 'Contraste sobre modif.'
    TabOrder = 14
    OnClick = Button6Click
  end
  object IV12: TImagesVehicle
    Left = 56
    Top = 56
    Width = 225
    Height = 217
    ADOConnection = DMConnections.BaseCOP
    ConfigOverview.InteractiveMode = False
    ConfigJpeg.PorcentajeAcumulado = 100
    Color = clGray
    TabOrder = 15
  end
end
