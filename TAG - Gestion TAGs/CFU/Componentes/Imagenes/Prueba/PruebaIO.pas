unit PruebaIO;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, jpeg, OPImageOverview, StdCtrls, DMConnection, DB,
  ADODB, Login, ImgTypes, UtilProc,
  DmiCtrls;

type
  TForm1 = class(TForm)
    Panel1: TPanel;
	Button3: TButton;
    Button4: TButton;
    btn_I12: TButton;
    btn_G12: TButton;
    btn_IG12: TButton;
    btn_gamma: TButton;
    btn_inten: TButton;
    btn_con: TButton;
    int: TNumericEdit;
	con: TNumericEdit;
	gam: TNumericEdit;
    Button1: TButton;
    Button2: TButton;
    Button5: TButton;
    Button6: TButton;
    IV12: TImagesVehicle;
    procedure Button3Click(Sender: TObject);
	procedure btn_IGClick(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure btn_I12Click(Sender: TObject);
    procedure btn_G12Click(Sender: TObject);
    procedure btn_IG12Click(Sender: TObject);
    procedure IV12Click(Sender: TObject);
    procedure btn_gammaClick(Sender: TObject);
    procedure btn_intenClick(Sender: TObject);
    procedure btn_conClick(Sender: TObject);
	procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
  private
	{ Private declarations }
  public
	{ Public declarations }
	function Inicializa(): Boolean;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

function TForm1.Inicializa: Boolean;
var
	f: TLoginForm;
begin
	Application.CreateForm(TLoginForm, f);
	if f.Inicializa(DMConnections.BaseCOP) and (f.ShowModal = mrOk) then Result := True
	  else Result := False;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
	Close;
end;

procedure TForm1.btn_IGClick(Sender: TObject);
begin

end;

//
procedure TForm1.Button4Click(Sender: TObject);
begin
	IV12.ConfigOverview.EscalaImage := 200;
	if not IV12.LoadImages(1, 1, 1, etPendiente, Now, '', tiFrontal) then MsgBox(IV12.LastError);
	IV12.ConfigOverview.InteractiveMode := True;
end;

procedure TForm1.btn_I12Click(Sender: TObject);
begin
	IV12.ConfigOverview.VisualMode := vmSoloImage;
end;

procedure TForm1.btn_G12Click(Sender: TObject);
begin
	IV12.ConfigOverview.VisualMode := vmSoloGrafico;
end;

procedure TForm1.btn_IG12Click(Sender: TObject);
begin
	IV12.ConfigOverview.VisualMode := vmAmbos;
end;

procedure TForm1.IV12Click(Sender: TObject);
begin
	if IV12.TipoImagen in [tiOverview1, tiOverview2, tiOverview3] then begin
		btn_I12.Enabled  := True;
		btn_G12.Enabled  := True;
		btn_IG12.Enabled := True;
	end else begin
		btn_I12.Enabled  := False;	
		btn_G12.Enabled  := False;
		btn_IG12.Enabled := False;
	end;
end;

procedure TForm1.btn_gammaClick(Sender: TObject);
begin
	IV12.RealceChange(gam.ValueInt, true);
end;

procedure TForm1.btn_intenClick(Sender: TObject);
begin
	IV12.IntensityChange(int.ValueInt, true);
end;

procedure TForm1.btn_conClick(Sender: TObject);
begin
   IV12.ContrastChange(con.ValueInt, true);
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
	IV12.Reset;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
	IV12.RealceChange(gam.ValueInt, false);
end;

procedure TForm1.Button5Click(Sender: TObject);
begin
	IV12.IntensityChange(int.ValueInt, false);
end;

procedure TForm1.Button6Click(Sender: TObject);
begin
   IV12.ContrastChange(con.ValueInt, False);
end;

end.
