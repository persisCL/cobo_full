{

 Carmen V 5.0.7 Delphi component of CARMEN(TM) Automatic Number Plate Recognition.

 Copyright (C) 1992-2000, by Adaptive Recognition Hungary.
 CARMEN is the Trade Mark of Adaptive Recognition Hungary.

 This code and information is only intended as a supplement to the CARMEN Automatic Number
 Plate Recognition Product.  For conditions of distribution  and use, see the accompanying
 README file.  This code and information is provided "AS IS" without warranty of any kind,
 either  expressed or implied, including  but not limited  to  the  implied  warranties of
 merchantability and/or fitness for a particular purpose.

}

unit uConstantsV507;

interface

uses Windows,classes;

const MAX_PLATES=10;
        FXCAP_NOOFCHANNELS=4;

     { video signal format }
        FXCAP_SIGNAL_PAL=$0000;

     { color formats }
        FXCAP_FORMAT_GRAYSCALE=$0011;	// Gray scale
        FXCAP_FORMAT_HICOLOR15=$0012;	// 5-5-5 Hi color format
        FXCAP_FORMAT_HICOLOR16=$0032;	// 5-6-5 Hi color format

        FXCAP_FORMAT_YUV422=$0022;	// YUV422 color format
        FXCAP_FORMAT_RGB24=$0013;       // 24 bit RGB color format
        FXCAP_FORMAT_RGB32=$0014;       // 32 bit RGB color format

     { grabbing formats }
        FXCAP_HALF_FIELD_EVEN=$0100;	  // Half even field
        FXCAP_HALF_FIELD_ODD=$0200;	     // Half odd field
        FXCAP_HALF_FIELD=$0300;	        // Half any field
        FXCAP_FIELD_EVEN=$0400;	        // Even field
        FXCAP_FIELD_ODD=$0800;	        // Odd field
        FXCAP_FIELD=$0C00;	              // Any field
        FXCAP_FRAME_EVEN=$1000;	        // Even frame
        FXCAP_FRAME_ODD=$2000;	        // Odd frame
        FXCAP_FRAME=$3000;	              // Any frame
        FXCAP_CFMT_MASK=$00FF;	        // Color format mask
        FXCAP_VFMT_MASK=$FF00;	        // Video format mask
        FXCAP_FMT_DEFAULT=(FXCAP_FORMAT_GRAYSCALE or FXCAP_FRAME);	//Default format

     { Warning and error codes. }
        ERROR_NOERROR				=Integer($00000000);
        ERROR_UNKNOWN				=Integer($20000000);
        ERROR_FULL_QUEUE			=Integer($10010000);
        ERROR_MEMORY				=Integer($10020000);
        ERROR_NOVIDEOSIGNAL			=Integer($10030000);
        ERROR_PARAMS				=Integer($20010000);
        ERROR_NODEVICE				=Integer($20020000);
        ERROR_CAPTURE				=Integer($20035000);
        ERROR_BUFFER_SIZE			=Integer($20040000);
        ERROR_INVALIDCHANNEL    		=Integer($20050000);
        ERROR_WATCHDOG				=Integer($20060000);
        ERROR_NODRIVER				=Integer($20070000);
        ERROR_FILE				=Integer($20080000);
        ERROR_ENGINE				=Integer($20090000);
        ERROR_INUSE				=Integer($200a0000);
        ERROR_VERSION				=Integer($200b0000);
        ERROR_FORMAT				=Integer($200c0000);

type
    TVideoFormat=(vfPal);
    TColorFormat=(cfGray,cfHiColor,cfYuv422,cfRGB24,cfRGB32);
    TResolution=(resHFE,resHFO,resHF,resFE,resFO,resF,resFRE,resFRO,resFR);
    TErrorHandling=(ehManual,ehAutomatic);
    TErrorKind=(ekDLL,ekComponent);
    TErrorLevel=(elWarning,elError);
    TRaiseError=(reErrors,reErrorsAndWarnings,reNone);

type
    byteptr=^byte;
    intptr=^integer;
    ovrptr=^OVERLAPPED;
    charptr=^char;

    PImage_Head=^TImage_Head;
    TImage_Head = packed record
      image : pointer;
      simage : integer;
      xsize : integer;
      ysize : integer;
      sline : integer;
      format : integer;
    end;

    PCapture_Info = ^TCapture_Info;
    TCapture_Info = packed record
      image_header : TImage_Head;
      time_odd : longword;
      time_even : longword;
    end;

    PRead_Plate_Params = ^TRead_Plate_Params;
    TRead_Plate_Params = packed record
      contrast_min, contrast_max: Integer;
      char_height, char_height_min, char_height_max: Integer;
      char_width, char_width_min, char_width_max: Integer;
      char_orientation, char_orientation_min, char_orientation_max: Integer;
      plate_slope, plate_slope_min, plate_slope_max: Integer;
      plate_gap_min, plate_gap_max: Integer;
      plate_width_min, plate_width_max: Integer;
    end;

    TCarmenPlate = packed record
      x1,y1,x2,y2,x3,y3,x4,y4: Integer;
    end;

    PCarmenTip = ^TCarmenTip;
    TCarmenTip = packed record
      Code:Word;
      Quality:Word;
    end;

    TCarmenTipCol = packed record
       x,y,xs,ys:integer;
       tip:Array [1..5] of TCarmenTip
    end;

    TCarmenTipList = record
      Cols:Array[1..32] of TCarmenTipCol;
    end;

    PRead_Plate_Result = ^TRead_Plate_Result;
    TRead_Plate_Result = packed record
      lptext: array[1..32] of Char;
      plate: TCarmenPlate;
      lpcolor: Integer;
      tiplist: TCarmenTipList;
    end;

    TCarmenResultSet = packed record
       Result:Array [0..MAX_PLATES] of TRead_Plate_Result;
    end;

    TPicEvent=procedure(RawImage:Pointer;Sender:TComponent) of Object;

    CString=^PChar;

    PChannel_Param = ^TChannel_Param;
    TChannel_Param = packed record
      signal : integer;
      contrast : integer;
      bright : integer;
      saturation : integer;
      hue : integer;
    end;

    PCarmen_Info = ^TCarmen_Info;
    TCarmen_Info = packed record
      Size:Integer;
      MinorVersion:Smallint;
      MajorVersion:Smallint;
    end;

    TOpenVideo      = function(var handle:integer;overlapped_:ovrptr=nil):boolean;stdcall;
    TCloseVideo     = function(handle:integer):boolean;stdcall;
    TCapture        = function(handle:integer;channel:cardinal;img:Pointer;
                               simg:integer;format:integer=FXCAP_FMT_DEFAULT;
                               sline:integer=0;
                               pinfo:PCapture_Info=nil):boolean;stdcall;
    TSetupChannel   = procedure(handle:integer;channel:cardinal;const padj:PChannel_Param);stdcall;
    TGetImageSize   = function(signal,format:integer;sline:integer):integer;stdcall;
    TGetVideoError  = function(handle:integer;error:charptr=nil;serror:integer=0):integer;stdcall;
    TGetDriverInfo  = function(CARMEN_INFO:PCarmen_Info):integer;stdCall;
    TDisplayImage   = procedure(hdc_:HDC;x,y,xs,ys:integer;pinfo:PImage_head);stdcall;
    TSetupReadPlate = function(handle: Integer; Parameters: PRead_Plate_Params) : Boolean;stdcall;
    TReadPlate      = function(handle: Integer; Imagehead: PCapture_Info; PlateResult: PRead_Plate_Result;maxresult: Integer) : Boolean;stdcall;
    TReadPlateGetParams = function(handle:integer; Parameters: PRead_Plate_Params):Boolean;stdCall;
    TGetError       = function(handle: Integer; error: CString; serror: Integer) : Integer;stdcall;

implementation

end.
