{

 Carmen V 5.0.7 Delphi component of CARMEN(TM) Automatic Number Plate Recognition.

 Copyright (C) 1992-2000, by Adaptive Recognition Hungary.
 CARMEN is the Trade Mark of Adaptive Recognition Hungary.

 This code and information is only intended as a supplement to the CARMEN Automatic Number
 Plate Recognition Product.  For conditions of distribution  and use, see the accompanying
 README file.  This code and information is provided "AS IS" without warranty of any kind,
 either  expressed or implied, including  but not limited  to  the  implied  warranties of
 merchantability and/or fitness for a particular purpose.

}

unit CarmenV507;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls,uConstantsV507,uChannelV507;

type
   ECarmenError = class (Exception);
   ECarmenDllError = class (ECarmenError);
   ECarmenComponentError = class (ECarmenError);

type
  TCarmenV507 = class(TComponent)
  private
    { DLL function declarations }
    OpenVideo:TOpenVideo;
    CloseVideo_:TCloseVideo;
    Capture:TCapture;
    SetupChannel:TSetupChannel;
    GetImageSize:TGetImageSize;
    DisplayImage:TDisplayImage;
    SetupReadPlate:TSetupReadPlate;
    ReadPlate:TReadPlate;
    ReadPlateGetParams:TReadPlateGetParams;
    GetError:TGetError;
    GetCarmenInfo:TGetDriverInfo;
    { Handle declarations }
    Hinst:THandle;
    HVideo:THandle;
    { Internal property }
    FDllLoaded:Boolean;
    FFramePerSec:integer;
    FGrabInterPolation:boolean;
    FLiveInterPolation:boolean;
    FFxCapDLLPath:string;
    FActive:Boolean;
    FPlateCounter:integer;
    FSlope, FSlopeMin, FSlopeMax: Integer;
    FContrastMin: Integer;
    FContrastMax: Integer;
    FChHeight: Integer;
    FChHeightMin, FChHeightMax: Integer;
    FChOrient, FChOrientMin, FChOrientMax: Integer;
    FChWidth, FChWidthMin, FChWidthMax: Integer;
    FPlateWidthMin, FPlateWidthMax: Integer;
    FCharDistanceMax: Integer;
    FCharDistanceMin: Integer;
    { Result variables }
    CParams:TRead_Plate_Params;
    CResult:TCarmenResultSet;
    FPlates:TStringList;
    FTipList:Array [0..MAX_PLATES] of TCarmenTipList;
    FPlateTopLeft:Array [0..MAX_PLATES] of TPoint;
    FPlateBottomLeft:Array [0..MAX_PLATES] of TPoint;
    FPlateTopRight:Array [0..MAX_PLATES] of TPoint;
    FPlateBottomRight:Array [0..MAX_PLATES] of TPoint;
    FVersion:integer;
    FSpecVersion:integer;
    { Error handling }
    FErrorHandling: TErrorHandling;
    FRaiseError:TRaiseError;
    FLastErrorCode: integer;
    FLastErrorMessage: String;
    FLastErrorKind: TErrorKind;
    { Events }
    FOnGrab:TPicEvent;
    { Protected declarations }
    Channels:array [0..FXCAP_NOOFCHANNELS-1] of TChannel;
    function GrabFrameToBitmapFast(Channel:integer;Bitmap:TBitmap):Boolean;
    procedure InterPolate(Channel:integer;Memory:Pointer);
    procedure SetParams;
    procedure GetParams;
    procedure SetupCarmenParams;
    procedure OnTimer(Sender: TObject);virtual;
    function ProcessResult:integer;
    { Carmen params }
    procedure SetContrastMin(value:integer);
    procedure SetContrastMax(value:integer);
    procedure SetCharHeight(value:integer);
    procedure SetCharHeightMin(value:integer);
    procedure SetCharHeightMax(value:integer);
    procedure SetCharWidth(value:integer);
    procedure SetCharWidthMin(value:integer);
    procedure SetCharWidthMax(value:integer);
    procedure SetCharOrientation(value:integer);
    procedure SetCharOrientationMin(value:integer);
    procedure SetCharOrientationMax(value:integer);
    procedure SetSlope(value:integer);
    procedure SetSlopeMin(value:integer);
    procedure SetSlopeMax(value:integer);
    procedure SetCharDistanceMax(value:integer);
    procedure SetCharDistanceMin(value:integer);
    procedure SetPlateWidthMin(value:integer);
    procedure SetPlateWidthMax(value:integer);

    function GetContrastMin:integer;
    function GetContrastMax:integer;
    function GetCharHeight:integer;
    function GetCharHeightMin:integer;
    function GetCharHeightMax:integer;
    function GetCharWidth:integer;
    function GetCharWidthMin:integer;
    function GetCharWidthMax:integer;
    function GetCharOrientation:integer;
    function GetCharOrientationMin:integer;
    function GetCharOrientationMax:integer;
    function GetSlope:integer;
    function GetSlopeMin:integer;
    function GetSlopeMax:integer;
    function GetCharDistanceMax:integer;
    function GetCharDistanceMin:integer;
    function GetPlateWidthMin:integer;
    function GetPlateWidthMax:integer;

    function GetInitialized:Boolean;
    { Video format }
    function GetVideoFormat(Channel:integer):TVideoFormat;
    procedure SetVideoFormat(Channel:integer;Value:TVideoFormat);
    function GetHue(Channel:integer):integer;
    procedure SetHue(Channel:integer;value:integer);
    function GetSaturation(Channel:integer):integer;
    procedure SetSaturation(Channel:integer;value:integer);
    function GetBrightness(Channel:integer):integer;
    procedure SetBrightness(Channel:integer;value:integer);
    function GetContrast(Channel:integer):integer;
    procedure SetContrast(Channel:integer;value:integer);
    function GetRes(Channel:integer):TResolution;
    procedure SetRes(Channel:integer;Value:TResolution);
    function GetColorFormat(Channel:integer):TColorFormat;
    procedure SetColorFormat(Channel:integer;Value:TColorFormat);
    function LoadDLL:Boolean;
    procedure ShowError(Msg:String);
    procedure SetLastError(ErrorCode:integer;Msg:String;ErrorKind:TErrorKind;Level:TErrorLevel);
  public
    { Public declarations }
    constructor Create(AOwner:TComponent);override;
    destructor Destroy;override;
    procedure BeforeDestruction;override;
    function InitVideo:boolean;
    procedure CloseVideo;
    procedure ChangeVideoSource(Channel,VideoSource:integer);
    function GrabFrameToBitmap(Channel:integer;bitmap:TBitmap):boolean;
    function GrabFrameToRawImage(Channel:integer;var RawImage:TImage_Head):boolean;
    procedure CreateGrayPalette(bmpPicture:TBitmap);
    procedure SetBitmapObject(Channel:integer;Image:TImage);
    procedure RawImageToBitmap(RawImage:TImage_Head;Bitmap:TBitmap);
    procedure StartLiveVideo(Channel:integer);
    procedure StopLiveVideo(Channel:integer);
    procedure GetLastErrorCode(var ErrorCode:integer;var ErrorMsg:String;var ErrorKind:TErrorKind);

    function SizeOfFrame(Channel:integer):integer;
    function GetPlatesFromChannel(Channel: Integer) : boolean;
    function GetPlatesFromBitmap(bmp: TBitmap) : boolean;
    function GetPlatesFromRawImage( RawImage:TImage_Head ):boolean;
    property VideoFormat[i:integer]:TVideoFormat read GetVideoFormat write SetVideoFormat;

    property Hue[i:integer]:integer read GetHue write SetHue;
    property Saturation[i:integer]:integer read GetSaturation write SetSaturation;
    property Brightness[i:integer]:integer read GetBrightness write SetBrightness;
    property Contrast[i:integer]:integer read GetContrast write SetContrast;
    property Resolution[Channel:integer]:TResolution read GetRes write SetRes;
    property ColorFormat[i:integer]:TColorFormat read GetColorFormat write SetColorFormat;

    function GetTipList(index:integer):TCarmenTipList;
    function GetPlateTopLeft(index:integer):TPoint;
    function GetPlateTopRight(index:integer):TPoint;
    function GetPlateBottomLeft(index:integer):TPoint;
    function GetPlateBottomRight(index:integer):TPoint;

    { Runtime propertys }
    property Plates:TStringList read FPlates;
    property PlatesCount:integer read FPlateCounter;

    property ContrastMin: Integer read GetContrastMin write SetContrastMin;                   //FContrastMin;
    property ContrastMax: Integer read GetContrastMax write SetContrastMax;                   //FContrastMax;
    property CharHeight: Integer read GetCharHeight write SetCharHeight;                      //FChHeight;
    property CharHeightMin: Integer read GetCharHeightMin write SetCharHeightMin;             //FChHeightMin;
    property CharHeightMax: Integer read GetCharHeightMax write SetCharHeightMax;             //FChHeightMax;
    property CharWidth: Integer read GetCharWidth write SetCharWidth;                         //FChWidth;
    property CharWidthMin: Integer read GetCharWidthMin write SetCharWidthMin;                //FChWidthMin;
    property CharWidthMax: Integer read GetCharWidthMax write SetCharWidthMax;                //FChWidthMax;
    property CharOrientation: Integer read GetCharOrientation write SetCharOrientation;       //FChOrient;
    property CharOrientMin: Integer read GetCharOrientationMin write SetCharOrientationMin;   //FChOrientMin;
    property CharOrientMax: Integer read GetCharOrientationMax write SetCharOrientationMax;   //FChOrientMax;
    property Slope: Integer read GetSlope write SetSlope;                                     //FSlope;
    property SlopeMin: Integer read GetSlopeMin write SetSlopeMin;                            //FSlopeMin;
    property SlopeMax: Integer read GetSlopeMax write SetSlopeMax;                            //FSlopeMax;
    property CharDistanceMax: Integer read GetCharDistanceMax write SetCharDistanceMax;       //FCharDistanceMax;
    property CharDistanceMin: Integer read GetCharDistanceMin write SetCharDistanceMin;       //FCharDistanceMin;
    property PlateWidthMin: Integer read GetPlateWidthMin write SetPlateWidthMin;             //FPlateWidthMin;
    property PlateWidthMax: Integer read GetPlateWidthMax write SetPlateWidthMax;             //FPlateWidthMax;
    property Initialized: Boolean read GetInitialized;
    property Version:integer read FVersion;
    property SpecVersion:integer read FSpecVersion;

  published
    { Published declarations }
    property CarmenDLLPath:string read FFxCapDLLPath write FFXCapDLLPath nodefault;
    property FramePerSec:integer read FFramePerSec write FFramePerSec nodefault;
    property GrabInterpolation:Boolean read FGrabInterpolation write FGrabInterpolation default false;
    property LiveInterpolation:Boolean read FLiveInterpolation write FLiveInterpolation default false;
    property AfterGrab:TPicEvent read FOnGrab write FOnGrab default nil;
    property ErrorHandling: TErrorHandling read FErrorHandling write FErrorHandling;
    //property RaiseError:TRaiseError read FRaiseError write FRaiseError;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Carmen', [TCarmenV507]);
end;

constructor TCarmenV507.Create(AOwner:TComponent);
var
   i:integer;
begin
    { Constructor of the component }
    { Initialize property values }
    inherited Create(AOwner);
    FDllLoaded:=False;
    FFramePerSec:=10;
    FFxCapDLLPath:='.\';
    FActive:=False;
    FPlates:=TStringList.Create;
    FPlateCounter:=0;
    FErrorHandling:=ehAutomatic;
    FRaiseError:=reErrorsAndWarnings;
    FLastErrorCode:=0;
    FLastErrorMessage:='';
    HVideo:=0;
    { Creat channels }
    for i:=0 to FXCAP_NOOFCHANNELS-1 do
       begin
          Channels[i]:=TChannel.Create;
          Channels[i].VideoSource:=i;
      end;
end;

procedure TCarmenV507.BeforeDestruction;
begin
   { If not in design mode, free library (Carmen.DLL) before destrutruction }
   if (csDesigning in ComponentState) then exit;
   FreeLibrary(Hinst);
end;

destructor TCarmenV507.Destroy;
var
   i:integer;
begin
   { On destroy, free all of the channels, and the plates stringlist }
   for i:=0 to FXCAP_NOOFCHANNELS-1 do
       Channels[i].Free;
   FPlates.Free;
   inherited;
end;

function TCarmenV507.LoadDLL:Boolean;
var tmp:string;
begin
   Result:=False;
   { Load carmen.DLL, and get address of all the functions }
   if not(csDesigning in ComponentState)
      then
         begin
         tmp:=FFxCapDLLPath+'Carmen.Dll';
         Hinst:=LoadLibrary(PChar(tmp));
         if Hinst=0
            then
               begin
                  SetLastError(100,'Carmen.dll not found!',ekComponent,elError);
                  exit;
               end;
         @OpenVideo:=GetProcAddress(Hinst,'CarmenOpen');
         @CloseVideo_:=GetProcAddress(Hinst,PChar('CarmenClose'));
         @Capture:=GetProcAddress(Hinst,PChar('CarmenCaptureImage'));
         @SetupChannel:=GetProcAddress(Hinst,PChar('CarmenSetupChannel'));
         @GetImageSize:=GetProcAddress(Hinst,PChar('CarmenGetImageSize'));
         @DisplayImage:=GetProcAddress(Hinst,PChar('CarmenDisplayImage'));
         @SetupReadPlate:=GetProcAddress(Hinst, PChar('CarmenSetupReadPlate'));
         @ReadPlate:=GetProcAddress(Hinst, PChar('CarmenReadPlate'));
         @GetError:=GetProcAddress(Hinst, PChar('CarmenGetError'));
         @GetCarmenInfo:=GetProcAddress(Hinst, PChar('CarmenGetSystemInfo'));
         @ReadPlateGetParams:=GetProcAddress(Hinst, PChar('CarmenReadPlateGetParams'));
         if(@OpenVideo=nil)      then SetLastError(201,'Not supported DLL function: CarmenOpen',ekComponent,elError);
         if(@Closevideo_=nil)    then SetLastError(202,'Not supported DLL function: CarmenClose',ekComponent,elError);
         if(@Capture=nil)        then SetLastError(203,'Not supported DLL function: CarmenCaptureImage',ekComponent,elError);
         if(@SetupChannel=nil)   then SetLastError(204,'Not supported DLL function: CarmenSetupChannel',ekComponent,elError);
         if(@GetImageSize=nil)   then SetLastError(205,'Not supported DLL function: CarmenGetImageSize',ekComponent,elError);
         if(@DisplayImage=nil)   then SetLastError(206,'Not supported DLL function: CarmenDisplayImage',ekComponent,elError);
         if(@SetupReadPlate=nil) then SetLastError(207,'Not supported DLL function: CarmenSetupReadPlate',ekComponent,elError);
         if(@ReadPlate=nil)      then SetLastError(208,'Not supported DLL function: CarmenReadPlate',ekComponent,elError);
         if(@GetError=nil)       then SetLastError(209,'Not supported DLL function: CarmenGetError',ekComponent,elError);
         if(@GetCarmenInfo=nil)  then SetLastError(210,'Not supported DLL function: CarmenGetSystemInfo',ekComponent,elError);
         if(@ReadPlateGetParams=nil) then SetLastError(211,'Not supported DLL function: CarmenReadPlateGetParams',ekComponent,elError);
      end;
   Result:=True;
   FDllLoaded:=True;
end;

function TCarmenV507.InitVideo:boolean;
var i:integer;
    versionInfo:TCarmen_Info;
begin
   Result:=False;
   { Load DLL and get function addresses }
   if not FDllLoaded then LoadDLL;
   if not FDllLoaded then Exit;
   { Get Carmen's version info   }
   VersionInfo.Size:=8;
   GetCarmenInfo(@VersionInfo);
   FVersion:=VersionInfo.MajorVersion;
   FSpecVersion:=versionInfo.MinorVersion;
   { Open Carmen }
   if HVideo=0 then OpenVideo(Integer(HVideo));
   if HVideo=0
      then
         begin
            SetLastError(101,'Cannot initialize Carmen device.'+#10+#13+#13+'1. Goto the control panel and double-click the devices icon.'+#10+#13+'2. Make sure the Carmen device is in the device list and it''s started.',ekComponent,elWarning);
            result:=false;
            exit;
         end;
   { Set up each channel of carmen }
   for i:=0 to FXCAP_NOOFCHANNELS-1 do
      if not Assigned(Channels[i].RawImage)
         then
            begin
               GetMem(Channels[i].RawImage,SizeofFrame(i));
               SetupChannel(HVideo,i,@(Channels[i].Adjust));
            end;
   Result:=true;
end;

function TCarmenV507.GetInitialized:Boolean;
begin
   Result:=HVideo>0;
end;

procedure TCarmenV507.CloseVideo;
begin
   { Close Carmen }
   if not FDllLoaded then exit;
   if HVideo>0 then CloseVideo_(HVideo);
end;

procedure TCarmenV507.SetContrastMin(value:integer);
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   GetParams;
   FContrastMin:=Value;
   SetupCarmenParams;
end;

procedure TCarmenV507.SetContrastMax(value:integer);
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   GetParams;
   FContrastMax:=Value;
   SetupCarmenParams;
end;

procedure TCarmenV507.SetCharHeight(value:integer);
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   GetParams;
   FChHeight:=Value;
   SetupCarmenParams;
end;

procedure TCarmenV507.SetCharHeightMin(value:integer);
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   GetParams;
   FChHeightMin:=Value;
   SetupCarmenParams;
end;

procedure TCarmenV507.SetCharHeightMax(value:integer);
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   GetParams;
   FChHeightMax:=Value;
   SetupCarmenParams;
end;

procedure TCarmenV507.SetCharWidth(value:integer);
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   GetParams;
   FChWidth:=Value;
   SetupCarmenParams;
end;

procedure TCarmenV507.SetCharWidthMin(value:integer);
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   GetParams;
   FChWidthMin:=Value;
   SetupCarmenParams;
end;

procedure TCarmenV507.SetCharWidthMax(value:integer);
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   GetParams;
   FChWidthMax:=Value;
   SetupCarmenParams;
end;

procedure TCarmenV507.SetCharOrientation(value:integer);
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   GetParams;
   FChOrient:=Value;
   SetupCarmenParams;
end;

procedure TCarmenV507.SetCharOrientationMin(value:integer);
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   GetParams;
   FChOrientMin:=Value;
   SetupCarmenParams;
end;

procedure TCarmenV507.SetCharOrientationMax(value:integer);
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   GetParams;
   FChOrientMax:=Value;
   SetupCarmenParams;
end;

procedure TCarmenV507.SetSlope(value:integer);
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   GetParams;
   FSlope:=Value;
   SetupCarmenParams;
end;

procedure TCarmenV507.SetSlopeMin(value:integer);
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   GetParams;
   FSlopeMin:=Value;
   SetupCarmenParams;
end;

procedure TCarmenV507.SetSlopeMax(value:integer);
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   GetParams;
   FSlopeMax:=Value;
   SetupCarmenParams;
end;

procedure TCarmenV507.SetCharDistanceMax(value:integer);
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   GetParams;
   FCharDistanceMax:=Value;
   SetupCarmenParams;
end;

procedure TCarmenV507.SetCharDistanceMin(value:integer);
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   GetParams;
   FCharDistanceMin:=Value;
   SetupCarmenParams;
end;

procedure TCarmenV507.SetPlateWidthMin(value:integer);
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   GetParams;
   FPlateWidthMin:=Value;
   SetupCarmenParams;
end;

procedure TCarmenV507.SetPlateWidthMax(value:integer);
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   GetParams;
   FPlateWidthMax:=Value;
   SetupCarmenParams;
end;

procedure TCarmenV507.SetParams;
begin
   { Set the carmen's recognition properties }
   with CParams do
      begin
         plate_slope :=FSlope ;
         plate_slope_min :=FSlopeMin;
         plate_slope_max :=FSlopeMax;
         char_height :=FChHeight;
         char_height_min:=FChHeightMin;
         char_height_max:=FChHeightMax;
         char_width:=FChWidth;
         char_width_min:=FChWidthMin;
         char_width_max:=FChWidthMax;
         char_orientation:=FChOrient;
         char_orientation_min:=FChOrientMin;
         char_orientation_max:=FChOrientMax;
         contrast_min:=FContrastMin;
         contrast_max:=FContrastMax;
         plate_width_min:=FPlateWidthMin;
         plate_width_max:=FPlateWidthMax;
         plate_gap_min:=FCharDistanceMin;
         plate_gap_max:=FCharDistanceMax;
      end;
end;

function TCarmenV507.GetContrastMin:integer;
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   GetParams;
   Result:=FContrastMin;
end;

function TCarmenV507.GetContrastMax:integer;
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   GetParams;
   Result:=FContrastMax;
end;

function TCarmenV507.GetCharHeight:integer;
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   GetParams;
   Result:=FChHeight;
end;

function TCarmenV507.GetCharHeightMin:integer;
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   GetParams;
   Result:=FChHeightMin;
end;

function TCarmenV507.GetCharHeightMax:integer;
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   GetParams;
   Result:=FChHeightMax;
end;

function TCarmenV507.GetCharWidth:integer;
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   GetParams;
   Result:=FChWidth;
end;

function TCarmenV507.GetCharWidthMin:integer;
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   GetParams;
   Result:=FChWidthMin;
end;

function TCarmenV507.GetCharWidthMax:integer;
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   GetParams;
   Result:=FChWidthMax;
end;

function TCarmenV507.GetCharOrientation:integer;
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   GetParams;
   Result:=FChOrient;
end;

function TCarmenV507.GetCharOrientationMin:integer;
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   GetParams;
   Result:=FChOrientMin;
end;

function TCarmenV507.GetCharOrientationMax:integer;
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   GetParams;
   Result:=FChOrientMax;
end;

function TCarmenV507.GetSlope:integer;
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   GetParams;
   Result:=FSlope;
end;

function TCarmenV507.GetSlopeMin:integer;
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   GetParams;
   Result:=FSlopeMin;
end;

function TCarmenV507.GetSlopeMax:integer;
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   GetParams;
   Result:=FSlopeMax;
end;

function TCarmenV507.GetCharDistanceMax:integer;
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   GetParams;
   Result:=FCharDistanceMax;
end;

function TCarmenV507.GetCharDistanceMin:integer;
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   GetParams;
   Result:=FCharDistanceMin;
end;

function TCarmenV507.GetPlateWidthMin:integer;
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   GetParams;
   Result:=FPlateWidthMin;
end;

function TCarmenV507.GetPlateWidthMax:integer;
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   GetParams;
   Result:=FPlateWidthMax;
end;

procedure TCarmenV507.GetParams;
begin
   { Get default values for parameters }
   if not ReadPlateGetParams(HVideo,@CParams)
      then raise ECarmenDllError.Create('Can not read parameters from Carmen engine');
   with CParams do
      begin
         FSlope := plate_slope;
         FSlopeMin := plate_slope_min ;
         FSlopeMax := plate_slope_max;
         FChHeight := char_height;
         FChHeightMin := char_height_min;
         FChHeightMax := char_height_max;
         FChWidth := char_width;
         FChWidthMin := char_width_min;
         FChWidthMax := char_width_max;
         FChOrient := char_orientation;
         FChOrientMin := char_orientation_min;
         FChOrientMax := char_orientation_max;
         FContrastMin := contrast_min;
         FContrastMax := contrast_max;
         FPlateWidthMin := plate_width_min;
         FPlateWidthMax := plate_width_max;
         FCharDistanceMin := plate_gap_min;
         FCharDistanceMax := plate_gap_max;
      end;
end;

function TCarmenV507.GetVideoFormat(Channel:integer):TVideoFormat;
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   { Return VideoFormat for the given channel }
   if not((Channel>=0) and (Channel<FXCAP_NOOFCHANNELS))
      then raise ECarmenComponentError.Create('Channel does not exist');
   result:=Channels[Channel].VideoFormat;
end;

procedure TCarmenV507.SetVideoFormat(Channel:integer;Value:TVideoFormat);
var startagain:boolean;
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   if not((Channel>=0)and(Channel<FXCAP_NOOFCHANNELS))
      then raise ECarmenComponentError.Create('Channel does not exist');
   with Channels[Channel] do
      begin
         { Set Start again to True if the video is live at the given channel }
         startagain:=Live;
         { Stop video, if it's live }
         if startagain then StopLiveVideo(Channel);
         if Assigned(RawImage)
            then
               begin
                  { Free the image memory block assigned to the channel }
                  FreeMem(RawImage);
                  RawImage:=nil;
               end;
         VideoFormat:=Value;
         SetupChannel(HVideo,Channel,@Adjust);
         try
            { Allocate new image memory block for the new format (wich can be different in size }
            GetMem(RawImage,SizeOfFrame(Channel));
         except
            on E:Exception do raise ECarmenComponentError.Create('Not enough memory');
         end;
         { If the video was live, before the format is changed, then start again }
         if startagain then StartLiveVideo(Channel);
      end;
end;

function TCarmenV507.GetRes(Channel:integer):TResolution;
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   { Return resolution for the given channel }
   if not((Channel>=0)and(Channel<FXCAP_NOOFCHANNELS))
      then raise ECarmenComponentError.Create('Channel does not exist');
   result:=Channels[Channel].Resolution;
end;

procedure TCarmenV507.SetRes(Channel:integer;Value:TResolution);
var startagain:boolean;
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   if not((Channel>=0)and(Channel<FXCAP_NOOFCHANNELS))
      then raise ECarmenComponentError.Create('Channel does not exist');
   with Channels[Channel] do
      begin
         { Set Start again to True if the video is live at the given channel }
         startagain:=Live;
         { Stop video, if it's live }
         if startagain then StopLiveVideo(Channel);
         { Free the image memory block assigned to the channel }
         if Assigned(RawImage)
            then
               begin
                  FreeMem(RawImage);
                  RawImage:=nil;
               end;
         Resolution:=Value;
         SetupChannel(HVideo,Channel,@Adjust);
         try
            { Allocate new image memory block for the new resulution (wich can be different in size }
            GetMem(RawImage,SizeOfFrame(Channel));
         except on E:Exception do raise ECarmenComponentError.Create('Not enough memory');
         end;
         { If the video was live, before the format is changed, then start again }
         if startagain then StartLiveVideo(Channel);
      end;
end;

function TCarmenV507.GetColorFormat(Channel:integer):TColorFormat;
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   { Return ColorFormat for the given channel }
   if not((Channel>=0)and(Channel<FXCAP_NOOFCHANNELS))
      then raise ECarmenComponentError.Create('Channel does not exist');
   result:=Channels[Channel].ColorFormat;
end;

procedure TCarmenV507.SetColorFormat(Channel:integer;Value:TColorFormat);
var startagain:boolean;
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   if not((Channel>=0)and(Channel<FXCAP_NOOFCHANNELS))
      then raise ECarmenComponentError.Create('Channel does not exist');
   with Channels[Channel] do
      begin
         { Set Start again to True if the video is live at the given channel }
         startagain:=Live;
         { Stop video, if it's live }
         StopLiveVideo(Channel);
         { Free the image memory block assigned to the channel }
         if Assigned(RawImage)
            then
               begin
                  FreeMem(RawImage);
                  RawImage:=nil;
               end;
         ColorFormat:=Value;
         SetupChannel(HVideo,Channel,@Adjust);
         try
            { Allocate new image memory block for the new resulution (wich can be different in size }
            GetMem(RawImage,SizeOfFrame(Channel));
         except on E:Exception do raise ECarmenComponentError.Create('Not enough memory');
         end;
         { If the video was live, before the format is changed, then start again }
         if startagain then StartLiveVideo(Channel);
     end;
end;

procedure TCarmenV507.ChangeVideoSource(Channel,VideoSource:integer);
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   { Change VideoSource for the given carmen cahannel }
   if not((VideoSource>=0)and(VideoSource<FXCAP_NOOFCHANNELS)and
         (Channel>=0)and(Channel<FXCAP_NOOFCHANNELS))
      then raise ECarmenComponentError.Create('Channel does not exist');
   Channels[Channel].VideoSource:=VideoSource;
end;

procedure TCarmenV507.SetBitmapObject(channel:integer;image:TImage);
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   { Assign bitmap object with a given channel }
   if Assigned(image)
      then
         begin
            with image.Picture.Bitmap do
               begin
                  { Set Images's bitmap size to be the same size with the image's size. }
                  Width:=Image.Width;
                  Height:=Image.Height;
                  { Create palette for the color format }
                  case Channels[channel].ColorFormat of
                     cfGray:begin
                              PixelFormat:=pf8bit;
                              CreateGrayPalette(image.Picture.Bitmap);
                            end;
                     cfHiColor:PixelFormat:=pf15bit;
                     cfYuv422:PixelFormat:=pf24bit;
                     cfRGB24:PixelFormat:=pf24bit;
                     cfRGB32:PixelFormat:=pf32bit;
                  end;
                  HandleType:=bmDIB;
               end;
            { Assign }
            Channels[channel].Bitmap:=Image.Picture.Bitmap;
     end;
end;

function TCarmenV507.GrabFrameToBitmap(Channel:integer;bitmap:TBitmap):boolean;
var row,imgsource:PByteArray;
    x,y:integer;
    CurrChannel:TChannel;
    tmp:TImage_head;
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   if not assigned(bitmap) then raise ECarmenComponentError.Create('GrabFrameToBitmap: nil pointer passed as bitmap.');
   { Grab a frame from the given channel, to a TBitmap object }
   CurrChannel:=Channels[Channel];
   if (Capture(HVideo,CurrChannel.VideoSource,CurrChannel.RawImage,SizeOfFrame(Channel),CurrChannel.Format,0,@(CurrChannel.ImageInfo)))
      then
         begin
            { Build temporary image header }
            tmp.image:=CurrChannel.Rawimage;
            tmp.xsize:=CurrChannel.ImageInfo.image_header.xsize;
            tmp.ysize:=CurrChannel.ImageInfo.image_header.ysize;
            tmp.sline:=768;
            tmp.format:=CurrChannel.Format;
            if GrabInterpolation then Interpolate(Channel,CurrChannel.RawImage);
            imgsource:=CurrChannel.RawImage;
            with Bitmap do
               begin
                  { Set bitmap size }
                  Width:=CurrChannel.ImageInfo.image_header.xsize;
                  Height:=CurrChannel.ImageInfo.image_header.ysize;
                  { Set bitmap format }
                  HandleType:=bmDIB;
                  case Channels[channel].ColorFormat of
                     cfGray:begin
                               PixelFormat:=pf8bit;
                               CreateGrayPalette(Bitmap);
                            end;
                     cfHiColor:begin
                                    PixelFormat:=pf15bit;
                               end;
                     cfYuv422:begin
                                   PixelFormat:=pf24bit;
                              end;
                     cfRGB24:begin
                                  PixelFormat:=pf24bit;
                             end;
                     cfRGB32:begin
                                  PixelFormat:=pf32bit;
                             end;
                  end;
               end;
            { Copy raw image to the bitmap image }
            for y:=0 to (Bitmap.Height-1)  do
               begin
                  row:=Bitmap.ScanLine[y];
                  for x:=0 to Bitmap.Width-1 do
                     begin
                          row[x]:=imgsource[y*Bitmap.Width+x];
                     end;
               end;
            result:=true;
            if Assigned(FOnGrab) then FOnGrab(CurrChannel.RawImage,self);
         end
      else
         begin
            if HVideo=0
               then SetLastError(400,'Carmen device initialization was failed.',ekComponent,elWarning)
               else SetLastError(401,'No video signal on the selected source.',ekComponent,elWarning);
            result:=false;
         end;
end;

procedure TCarmenV507.CreateGrayPalette(bmpPicture:TBitmap);
var pal: PLogPalette;
    hpal: HPALETTE;
    i: Integer;
begin
   { Create a 256 color gray palette for the given bitmap object }
   pal := nil;
   try
      GetMem(pal, sizeof(TLogPalette) + sizeof(TPaletteEntry) * 255);
      pal.palVersion := $300;
      pal.palNumEntries := 256;
      for i := 0 to 255 do
         begin
           pal.palPalEntry[i].peRed :=i;
           pal.palPalEntry[i].peGreen :=i;
           pal.palPalEntry[i].peBlue :=i;
         end;
      hpal := CreatePalette(pal^);
      if hpal <> 0 then bmpPicture.Palette := hpal;
   finally
      FreeMem(pal);
   end;
end;

procedure TCarmenV507.InterPolate(Channel:integer;Memory:Pointer);
var x,y:integer;
    imgSource:PByteArray;
begin
   { Interpolate the image on the given channel }
   imgSource:=Memory;
   for y:=0 to (Channels[Channel].ImageInfo.image_header.ysize-1) div 2 do
      for x:=0 to Channels[Channel].ImageInfo.image_header.xsize-1 do
          imgSource[(Y*2+1)*(Channels[Channel].ImageInfo.image_header.xsize)+X]:=imgSource[Y*2*(Channels[Channel].ImageInfo.image_header.xsize)+X];
end;

procedure TCarmenV507.StartLiveVideo(Channel:integer);
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   { Start video on the given channel }
   if not Channels[Channel].Live
      then
         begin
            with Channels[Channel] do
               begin
                  { Set frame rate }
                  Timer.Interval:=1000 div FramePerSec;
                  Timer.tag:=channel;
                  Timer.OnTimer:=OnTimer;
                  Timer.Enabled:=True;
                  Live:=true;
               end;
         end;
end;

procedure TCarmenV507.OnTimer(Sender: TObject);
var Channel:integer;
begin
   { Get the caller channel number }
   Channel:=TTimer(Sender).tag;
   if (not Channels[Channel].Grabbing) and (Channels[Channel].Live)
       and (Assigned(Channels[Channel].Bitmap))
      then
         begin
            Channels[Channel].Grabbing:=True;
            { Grab frame from the channel }
            GrabFrameToBitmapfast(Channel,Channels[Channel].Bitmap);
         end;
   Channels[Channel].Grabbing:=False;
end;

procedure TCarmenV507.StopLiveVideo(Channel:integer);
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   { Stop video on the given channel }
   if Channels[Channel].Live
      then
         begin
            Channels[Channel].Timer.Enabled:=False;
            Channels[Channel].Live:=false;
         end;
end;

function TCarmenV507.GrabFrameToBitmapFast(Channel:integer;Bitmap:TBitmap):Boolean;
var x,y:integer;
    xPich,yPich:double;
    ax,ay:double;
    row,imgsource:PByteArray;
    CurrChannel:TChannel;
    StrWidth,StrHeight:integer;
    NoSignalStr:String;
begin
   { A speed optimalized grabbing method for the live image display }
   CurrChannel:=Channels[Channel];
   { Grab a frame, from the given carmen channel }
   if(Capture(HVideo,CurrChannel.VideoSource,CurrChannel.RawImage,SizeOfFrame(Channel),CurrChannel.Format,0,@(CurrChannel.ImageInfo)))
      then
         begin
            if LiveInterPolation then InterPolate(Channel,CurrChannel.RawImage);
            imgsource:=CurrChannel.RawImage;
            xPich:=CurrChannel.ImageInfo.image_header.XSize/Bitmap.Width;
            yPich:=CurrChannel.ImageInfo.image_header.ysize/(Bitmap.Height);
            ay:=0;
            { Copy image to the bitmap object (Only the visible part) }
            for y:=0 to bitmap.height-1 do
               begin
                  ax:=0;
                  row:=Bitmap.ScanLine[y];
                  for x:=0 to bitmap.width-1 do
                     begin
                        row[x]:=imgsource[round(ay)*(CurrChannel.ImageInfo.image_header.XSize)+round(ax)];
                        ax:=ax+xPich;
                     end;
                  ay:=ay+ypich;
               end;
            Bitmap.Modified:=True;
            Result:=True;
            if Assigned(FOnGrab) then FOnGrab(CurrChannel.RawImage,self);
         end
      else
          begin
             Result:=False;
             { If there is an error, display a black screen }
             For y:=0 to Bitmap.Height-1 do
                begin
                   row:=Bitmap.ScanLine[y];
                   For x:=0 to bitmap.Width-1 do
                      row[x]:=0;
                end;
             with Bitmap.Canvas do
                begin
                   Font.Name:='Arial';
                   Font.Height:=22;
                   Font.Style :=[fsBold];
                   Font.Color:=$ffffff;
                   Brush.Color:=0;
                   NoSignalStr:='No video signal';
                   StrWidth:=TextWidth(NoSignalStr);
                   StrHeight:=TextHeight(NoSignalStr);
                   TextOut(ClipRect.Right div 2-(StrWidth div 2),ClipRect.Bottom div 2-(StrHeight div 2),NoSignalStr);
                end;
             Bitmap.Modified:=True;
          end;
end;

function TCarmenV507.GrabFrameToRawImage(Channel:integer;var RawImage:TImage_Head):Boolean;
var image:Pointer;
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   { Grabb a frame to the memory from, the given channel }
   Image:=RawImage.Image;
   with Channels[Channel] do
      begin
         if (Capture(HVideo,VideoSource,image,SizeOfFrame(Channel),Format,0,@ImageInfo))
            then
               begin
                  if GrabInterpolation then Interpolate(Channel,Image);
                  result:=true
               end
            else
               begin
                  if HVideo=0
                     then SetLastError(400,'Carmen device initialization was failed.',ekComponent,elWarning)
                     else SetLastError(401,'No video signal on the selected source.',ekComponent,elWarning);
                  result:=false;
               end;
      end;
   RawImage.simage:=Channels[channel].ImageInfo.image_header.simage;
   RawImage.xsize:=Channels[channel].ImageInfo.image_header.xsize;
   RawImage.ysize:=Channels[channel].ImageInfo.image_header.ysize;
   RawImage.simage:=Channels[channel].ImageInfo.image_header.simage;
   RawImage.sline:=Channels[channel].ImageInfo.image_header.sline;
   RawImage.format:=Channels[channel].ImageInfo.image_header.format;
end;

procedure TCarmenV507.RawImageToBitmap(RawImage:TImage_Head;Bitmap:TBitmap);
var x,y:integer;
    Row:PByteArray;
    SrcImage:PByteArray;
begin
   if Assigned(Bitmap)
      then
         begin
            SrcImage:=RawImage.Image;
            with bitmap do
               begin
                  Width:=RawImage.xsize;
                  Height:=RawImage.ysize;
                  HandleType:=bmDIB;
                  PixelFormat:=pf8bit;
                  CreateGrayPalette(bitmap);
                  for y:=0 to RawImage.ysize-1 do
                     begin
                        row:=Bitmap.ScanLine[y];
                        for x:=0 to RawImage.xsize-1 do
                           row[x]:=SrcImage[y*(RawImage.xsize)+x];
                     end;
               end;
         end;
end;

function TCarmenV507.SizeOfFrame(Channel:integer):integer;
begin
    if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
    if not((Channel>=0)and(Channel<FXCAP_NOOFCHANNELS)) then
       raise ECarmenComponentError.Create('Channel does not exist');
    result:=GetImageSize(Channels[Channel].CtrlVideo,Channels[Channel].CtrlFormat,-1);
end;

function TCarmenV507.GetHue(Channel:integer):integer;
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   if not((Channel>=0)and(Channel<FXCAP_NOOFCHANNELS)) then
      raise ECarmenComponentError.Create('Channel does not exist');
   result:=Channels[Channel].Hue;
end;

procedure TCarmenV507.SetHue(Channel:integer;value:integer);
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   if not((Channel>=0)and(Channel<FXCAP_NOOFCHANNELS)) then
      raise ECarmenComponentError.Create('Channel does not exist');
   Channels[Channel].Hue:=Value;
   SetupChannel(HVideo,Channels[Channel].VideoSource,@(Channels[Channel].Adjust));
end;

function TCarmenV507.GetSaturation(Channel:integer):integer;
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   if not((Channel>=0)and(Channel<FXCAP_NOOFCHANNELS)) then
      raise ECarmenComponentError.Create('Channel does not exist');
   result:=Channels[Channel].Saturation;
end;

procedure TCarmenV507.SetSaturation(Channel:integer;value:integer);
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   if not((Channel>=0)and(Channel<FXCAP_NOOFCHANNELS)) then
      raise ECarmenComponentError.Create('Channel does not exist');
   Channels[Channel].Saturation:=Value;
   SetupChannel(HVideo,Channels[Channel].VideoSource,@(Channels[Channel].Adjust));
end;

function TCarmenV507.GetBrightness(Channel:integer):integer;
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   if not((Channel>=0)and(Channel<FXCAP_NOOFCHANNELS)) then
      raise ECarmenComponentError.Create('Channel does not exist');
   result:=Channels[Channel].Brightness;
end;

procedure TCarmenV507.SetBrightness(Channel:integer;value:integer);
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   if not((Channel>=0)and(Channel<FXCAP_NOOFCHANNELS)) then
      raise ECarmenComponentError.Create('Channel does not exist');
   Channels[Channel].Brightness:=Value;
   SetupChannel(HVideo,Channels[Channel].VideoSource,@(Channels[Channel].Adjust));
end;

function TCarmenV507.GetContrast(Channel:integer):integer;
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   if not((Channel>=0)and(Channel<FXCAP_NOOFCHANNELS)) then
      raise ECarmenComponentError.Create('Channel does not exist');
   result:=Channels[Channel].Contrast;
end;

procedure TCarmenV507.SetContrast(Channel:integer;value:integer);
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   if not((Channel>=0)and(Channel<FXCAP_NOOFCHANNELS)) then
      raise ECarmenComponentError.Create('Channel does not exist');
   Channels[Channel].Contrast:=Value;
   SetupChannel(HVideo,Channels[Channel].VideoSource,@(Channels[Channel].Adjust));
end;

procedure TCarmenV507.SetupCarmenParams;
var
  errortext: PChar;
  ErrorMsg:String;
begin
  SetParams;
  if not SetupReadPlate(HVideo, @CParams)
     then
        begin
          GetMem(errortext, 255);
          GetError(HVideo, @errortext^, 255);
          ErrorMsg:=errortext;
          FreeMem(errortext);
          SetLastError(300,ErrorMsg,ekDLL,elWarning);
        end;
end;

function TCarmenV507.GetPlatesFromChannel(Channel: Integer) : boolean;
var errortext: PChar;
    ErrorMsg:String;
begin
  if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
  if Channels[Channel].live
     then
        begin
           if not ReadPlate(HVideo,@Channels[Channel].ImageInfo.image_header,@CResult,MAX_PLATES)
              then
                 begin
                    GetMem(errortext, 255);
                    GetError(HVideo, @errortext^, 255);
                    ErrorMsg:=errortext;
                    FreeMem(errortext);
                    SetLastError(301,ErrorMsg,ekDLL,elWarning);
                    Result:=False;
                 end
              else Result:=ProcessResult>0;
        end
     else Result:=False;
end;

function TCarmenV507.GetPlatesFromBitmap(bmp:TBitmap) : Boolean;
var Xsize,ysize,X,Y,SLine,Format:integer;
    Row:pByteArray;
    picture:PByteArray;
    image:TCapture_Info;
    errortext: PChar;
    ErrorMsg:String;
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   if not assigned(bmp) then raise ECarmenComponentError.Create('GetPlatesFromBitmap: nil pointer passed as bitmap.');
   { Get raw image part of the bitmap in the right pixel format}
   with bmp do
      begin
         HandleType:=bmDIB;
         if Height>576 then Height:=576;
         if Width>768 then Width:=768;
         Xsize:=Width;
         Ysize:=Height;
         case PixelFormat of
            pf1bit:begin
                     PixelFormat:=pf15bit;
                     SLine:=XSize*2;
                     Format:=FXCAP_FORMAT_HICOLOR15;
                   end;
            pf4bit:begin
                     PixelFormat:=pf15bit;
                     SLine:=XSize*2;
                     Format:=FXCAP_FORMAT_HICOLOR15;
                   end;
            pf8bit:begin
                     CreateGrayPalette(bmp);
                     SLine:=XSize;
                     Format:=FXCAP_FMT_DEFAULT;
                   end;
            pf15bit:begin
                      SLine:=XSize*2;
                      Format:=FXCAP_FORMAT_HICOLOR15 ;
                    end;
            pf16bit:begin
                      SLine:=XSize*2;
                      Format:=FXCAP_FORMAT_HICOLOR16 ;
                    end;
            pf24bit:begin
                      SLine:=XSize*3;
                      Format:=FXCAP_FORMAT_RGB24 ;
                    end;
            pf32bit:begin
                      SLine:=XSize*4;
                      Format:=FXCAP_FORMAT_RGB32 ;
                    end;
            else
               begin
                   PixelFormat:=pf32bit;
                   SLine:=XSize*4;
                   Format:=FXCAP_FORMAT_RGB32 ;
               end;
         end;
         { Copy bitmap to raw image }
         GetMem(picture,YSize*SLine);
         for y:=0 to Ysize-1 do
            begin
               Row:=ScanLine[y];
               For x:=0 to SLine-1 do
                  begin
                    picture^[y*sline+x]:=Row^[x];
                  end;
            end;
      end;
   { Build image heade }
   image.image_header.image:=@picture^;
   image.image_header.xsize:=Xsize;
   image.image_header.ysize:=Ysize;
   image.image_header.sline:=SLine;
   image.image_header.simage:=SLine*YSize;
   image.image_header.format:=Format;
   { Get plates }
   if not ReadPlate(HVideo,@Image.image_header,@CResult,MAX_PLATES)
     then
        begin
          GetMem(errortext, 255);
          GetError(HVideo, @errortext^, 255);
          ErrorMsg:=errortext;
          FreeMem(errortext);
          SetLastError(301,ErrorMsg,ekDLL,elWarning);
          Result:=False;
        end
     else Result:=ProcessResult>0;
   { Free raw image memory }
   FreeMem(Picture);
end;

function TCarmenV507.GetPlatesFromRawImage( RawImage:TImage_Head ):boolean;
var errortext: PChar;
    image:TCapture_Info;
    ErrorMsg:String;
begin
   if not FDllLoaded then raise ECarmenComponentError.Create('Carmen.Dll not loaded!');
   { Build image heade }
   image.image_header.image:=@RawImage.image^;
   image.image_header.xsize:=RawImage.Xsize;
   image.image_header.ysize:=RawImage.Ysize;
   image.image_header.sline:=RawImage.XSize;
   image.image_header.simage:=RawImage.XSize*RawImage.YSize;
   image.image_header.format:=FXCAP_FMT_DEFAULT;
   { Get plates }
   if not ReadPlate(HVideo,@Image.image_header,@CResult,MAX_PLATES)
     then
        begin
          GetMem(errortext, 255);
          GetError(HVideo, @errortext^, 255);
          ErrorMsg:=errortext;
          FreeMem(errortext);
          SetLastError(301,ErrorMsg,ekDLL,elWarning);
          Result:=False;
        end
     else Result:=ProcessResult>0;
end;

function TCarmenV507.ProcessResult:integer;
var PlatesCount:integer;
    Plate:String;
    i,j:integer;
begin
   FPlates.Clear;
   PlatesCount:=0;
   j:=0;
   For i:=0 to MAX_PLATES-1 do
      begin
         Plate:=CResult.Result[i].lptext;
         if Trim(Plate)<>''
            then
               begin
                  FPlates.Add(CResult.Result[i].lptext);
                  FTipList[j]:=CResult.Result[i].tiplist;
                  FPlateTopLeft[j].x:=CResult.Result[i].plate.x1;
                  FPlateTopLeft[j].y:=CResult.Result[i].plate.y1;
                  FPlateTopRight[j].x:=CResult.Result[i].plate.x2;
                  FPlateTopRight[j].y:=CResult.Result[i].plate.y2;
                  FPlateBottomRight[j].x:=CResult.Result[i].plate.x3;
                  FPlateBottomRight[j].y:=CResult.Result[i].plate.y3;
                  FPlateBottomLeft[j].x:=CResult.Result[i].plate.x4;
                  FPlateBottomLeft[j].y:=CResult.Result[i].plate.y4;
                  inc(PlatesCount);
                  inc(j);
               end;
      end;
  FPlateCounter:=PlatesCount;
  Result:=PlatesCount;
end;

function TCarmenV507.GetTipList(index:integer):TCarmenTipList;
begin
   if FPlateCounter-1<index
      then raise ECarmenComponentError.Create('PlateIndex out of bound.');
   Result:=FTipList[index];
end;

function TCarmenV507.GetPlateTopLeft(index:integer):TPoint;
begin
   if FPlateCounter-1<index
      then raise ECarmenComponentError.Create('PlateIndex out of bound.');
   Result:=FPlateTopLeft[index];
end;

function TCarmenV507.GetPlateTopRight(index:integer):TPoint;
begin
   if FPlateCounter-1<index
      then raise ECarmenComponentError.Create('PlateIndex out of bound.');
   Result:=FPlateTopRight[index];
end;

function TCarmenV507.GetPlateBottomLeft(index:integer):TPoint;
begin
   if FPlateCounter-1<index
      then raise ECarmenComponentError.Create('PlateIndex out of bound.');
   Result:=FPlateBottomLeft[index];
end;

function TCarmenV507.GetPlateBottomRight(index:integer):TPoint;
begin
   if FPlateCounter-1<index
      then raise ECarmenComponentError.Create('PlateIndex out of bound.');
   Result:=FPlateBottomRight[index];
end;

procedure TCarmenV507.ShowError(Msg:String);
begin
   MessageDlg(Msg, mtError,[mbOk], 0);
end;

procedure TCarmenV507.SetLastError(ErrorCode:integer;Msg:String;ErrorKind:TErrorKind;Level:TErrorLevel);
begin
   FLastErrorCode:=ErrorCode;
   FLastErrorMessage:=Msg;
   FLastErrorKind:=ErrorKind;
   if (FRaiseError=reNone) then Exit;
   if (FRaiseError=reErrors) and (Level=elWarning) then exit;
   case FErrorHandling of
      ehManual:
         begin
            if ErrorKind=ekDLL       then raise ECarmenDLLError.Create(Msg);
            if ErrorKind=ekComponent then raise ECarmenComponentError.Create(Msg);
         end;
      ehAutomatic: ShowError(Msg);
   end;
end;

procedure TCarmenV507.GetLastErrorCode(var ErrorCode:integer;var ErrorMsg:String;var ErrorKind:TErrorKind);
begin
   ErrorCode:=FLastErrorCode;
   ErrorKind:=FLastErrorKind;
   ErrorMsg:=FLastErrorMessage;
end;

end.
