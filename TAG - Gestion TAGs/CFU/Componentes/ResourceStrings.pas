unit ResourceStrings;

interface

ResourceString
	WRD_ERROR				= 'Error';
	WRD_SALIR				= 'Salir';
	WRD_AGREGAR				= 'Agregar';
	WRD_ELIMINAR			= 'Eliminar';
	WRD_MODIFICAR			= 'Modificar';
	WRD_IMPRIMIR			= 'Imprimir';
	WRD_BUSCAR				= 'Buscar';
	WRD_SI					= 'Si';
	WRD_NO					= 'No';
	WRD_CERRAR				= 'Cerrar';

	MSG_ERROR_FECHA 		= 'Formato de fecha Incorrecto';
	MSG_ERROR_HORA			= 'Formato de Hora Incorrecto';
	MSG_ANCHO_PAGINA		= 'Ancho de P�gina';
	MSG_PAGINA_ENTERA		= 'P�gina Completa';
	MSG_NOHAYIMPRESORA		= 'No hay ninguna impresora instalada';
	MSG_PREVIEW				= 'Presentaci�n Preliminar';
	MSG_CUSTOMSIZE			= 'Tama�o personal';

	MSG_ERROR_OPENING_FILES = 'Error al Abrir Archivo(s)';
	MSG_UNKNOWN_CONN_CLASS	= 'Clase de conexi�n desconocida';

	MSG_SYNTAX_ERROR		= 'Error de Syntaxis';

implementation

end.









