{-----------------------------------------------------------------------------
 File Name: FrmChecklist
 Author:
 Date Created:
 Language: ES-AR
 Description:

 Revision : 1
  Date: 25/02/2009
  Author: mpiazza
  Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
    los bloqueos de tablas en la lectura
-----------------------------------------------------------------------------}
unit FrmChecklist;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Validate, DateEdit, VariantComboBox, DB, ADODB, UtilDB;

type
  TFormChecklist = class(TForm)
    Label1: TLabel;
    cbChecklist: TVariantComboBox;
    Label2: TLabel;
    txtVencimiento: TDateEdit;
    Label3: TLabel;
    txtObservaciones: TMemo;
    btnAceptar: TButton;
    Button2: TButton;
    Query: TADOQuery;
  private
    { Private declarations }
  public
    { Public declarations }
    function Inicializar(CodigoConvenio: Integer = 0): boolean;
  end;

var
  FormChecklist: TFormChecklist;

implementation

{$R *.dfm}

{ TFormChecklist }
{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:
  Date Created:  /  /
  Description:
  Parameters: CodigoConvenio: Integer = 0
  Return Value: boolean

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function TFormChecklist.Inicializar(CodigoConvenio: Integer = 0): boolean;
begin
    if CodigoConvenio = 0 then begin
        Query.SQL.Text := 'SELECT * FROM MaestroCheckList  WITH (NOLOCK) ORDER BY Descripcion';
    end else begin
        Query.SQL.Text := Format('SELECT * FROM MaestroCheckList  WITH (NOLOCK) WHERE NOT EXISTS(' +
          'SELECT 1 FROM ConvenioCheckList  WITH (NOLOCK) WHERE CodigoConvenio = %d '+
          'AND CodigoCheckList = MaestroChecklist.CodigoChecklist) ORDER BY Descripcion',
          [CodigoConvenio]);
    end;
    Result := OpenTables([Query]);
    if not Result then Exit;
    while not Query.Eof do begin
        cbChecklist.Items.Add(Query['Descripcion'], Query['CodigoChecklist']);
        Query.Next;
    end;
    btnAceptar.Enabled := not Query.IsEmpty;
    Query.Close;
    cbChecklist.ItemIndex := 0;
end;

end.
