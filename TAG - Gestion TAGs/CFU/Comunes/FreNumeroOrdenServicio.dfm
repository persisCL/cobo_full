object FrameNumeroOrdenServicio: TFrameNumeroOrdenServicio
  Left = 0
  Top = 0
  Width = 239
  Height = 68
  TabOrder = 0
  DesignSize = (
    239
    68)
  object PReclamo: TGroupBox
    Left = 3
    Top = 2
    Width = 233
    Height = 63
    Anchors = [akTop, akRight]
    Caption = 'Orden de Servicio'
    TabOrder = 0
    object LOrden: TLabel
      Left = 12
      Top = 19
      Width = 72
      Height = 13
      Caption = 'N'#250'mero Orden:'
    end
    object LContactarCliente: TLabel
      Left = 117
      Top = 40
      Width = 105
      Height = 13
      Cursor = crHandPoint
      Caption = 'Ir a Contactar Cliente..'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      Visible = False
      OnClick = LContactarClienteClick
    end
    object EReclamo: TEdit
      Left = 117
      Top = 15
      Width = 104
      Height = 21
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 0
    end
  end
end
