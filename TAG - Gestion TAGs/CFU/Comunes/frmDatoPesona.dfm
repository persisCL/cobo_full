object FreDatoPresona: TFreDatoPresona
  Left = 0
  Top = 0
  Width = 1060
  Height = 99
  TabOrder = 0
  TabStop = True
  DesignSize = (
    1060
    99)
  object ledTipoCliente: TLed
    Left = 1036
    Top = 55
    Width = 16
    Height = 16
    Anchors = [akRight, akBottom]
    Blinking = False
    Diameter = 15
    Enabled = True
    ColorOn = clWindow
    ColorOff = clBtnFace
    ExplicitLeft = 1015
  end
  object lblTipo: TLabel
    Left = 828
    Top = 56
    Width = 69
    Height = 13
    Alignment = taRightJustify
    Anchors = [akRight, akBottom]
    Caption = 'Tipo Cliente:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblSemaforo1: TLabel
    Left = 832
    Top = 34
    Width = 65
    Height = 13
    Alignment = taRightJustify
    Anchors = [akRight, akBottom]
    Caption = 'Semaforo1:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object LedSemaforo1: TLed
    Left = 1036
    Top = 34
    Width = 16
    Height = 16
    Anchors = [akRight, akBottom]
    Blinking = False
    Diameter = 15
    Enabled = True
    ColorOn = clWindow
    ColorOff = clBtnFace
    ExplicitLeft = 1015
  end
  object lblSemaforo3: TLabel
    Left = 832
    Top = 78
    Width = 65
    Height = 13
    Alignment = taRightJustify
    Anchors = [akRight, akBottom]
    Caption = 'Semaforo3:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object LedSemaforo3: TLed
    Left = 1036
    Top = 76
    Width = 16
    Height = 16
    Anchors = [akRight, akBottom]
    Blinking = False
    Diameter = 15
    Enabled = True
    ColorOn = clWindow
    ColorOff = clBtnFace
    ExplicitLeft = 1015
  end
  object Pc_Datos: TPageControl
    Left = 0
    Top = 25
    Width = 775
    Height = 55
    ActivePage = Tab_Fisica
    Style = tsFlatButtons
    TabOrder = 1
    TabStop = False
    object Tab_Juridica: TTabSheet
      Caption = 'Tab_Juridica'
      TabVisible = False
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object lblSexo_Contacto: TLabel
        Left = 2
        Top = 30
        Width = 87
        Height = 13
        Caption = 'Sexo contacto:'
        Color = clBtnFace
        FocusControl = cbSexo_Contacto
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object lblApellidoMaterno: TLabel
        Left = 498
        Top = 5
        Width = 100
        Height = 13
        Caption = 'Apellido Materno:'
        FocusControl = txtApellidoMaterno_Contacto
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblApellido: TLabel
        Left = 259
        Top = 5
        Width = 50
        Height = 13
        Hint = 'Apellido paterno del contacto'
        Caption = 'Apellido:'
        FocusControl = txtApellido_Contacto
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblNombre: TLabel
        Left = 2
        Top = 5
        Width = 103
        Height = 13
        Caption = 'Nombre Contacto:'
        FocusControl = txtNombre_Contacto
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object txtApellidoMaterno_Contacto: TEdit
        Left = 600
        Top = 0
        Width = 166
        Height = 21
        Hint = 'Apellido Materno'
        CharCase = ecUpperCase
        MaxLength = 30
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnKeyPress = txt_NombreKeyPress
      end
      object cbSexo_Contacto: TComboBox
        Left = 111
        Top = 24
        Width = 114
        Height = 21
        Hint = 'Sexo'
        Style = csDropDownList
        Color = 16444382
        ItemHeight = 0
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
      end
      object txtApellido_Contacto: TEdit
        Left = 333
        Top = 0
        Width = 155
        Height = 21
        Hint = 'Apellido paterno'
        CharCase = ecUpperCase
        Color = 16444382
        MaxLength = 60
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnKeyPress = txt_NombreKeyPress
      end
      object txtNombre_Contacto: TEdit
        Left = 108
        Top = 0
        Width = 145
        Height = 21
        Hint = 'Nombre'
        CharCase = ecUpperCase
        Color = 16444382
        MaxLength = 60
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnKeyPress = txt_NombreKeyPress
      end
    end
    object Tab_Fisica: TTabSheet
      Caption = 'Tab_Fisica'
      ImageIndex = 1
      TabVisible = False
      object Label4: TLabel
        Left = 498
        Top = 5
        Width = 100
        Height = 13
        Caption = 'Apellido Materno:'
        FocusControl = txt_ApellidoMaterno
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 258
        Top = 5
        Width = 50
        Height = 13
        Caption = 'Apellido:'
        FocusControl = txt_Apellido
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbl_Sexo: TLabel
        Left = 3
        Top = 31
        Width = 33
        Height = 13
        Caption = 'Sexo:'
        FocusControl = cbSexo
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbl_FechaNacimiento: TLabel
        Left = 259
        Top = 30
        Width = 66
        Height = 13
        Caption = 'Nacimiento:'
        FocusControl = txtFechaNacimiento
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 3
        Top = 5
        Width = 48
        Height = 13
        Caption = 'Nombre:'
        FocusControl = txt_Nombre
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object txt_ApellidoMaterno: TEdit
        Left = 600
        Top = 0
        Width = 166
        Height = 21
        Hint = 'Apellido materno'
        CharCase = ecUpperCase
        MaxLength = 30
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnKeyPress = txt_NombreKeyPress
      end
      object txt_Apellido: TEdit
        Left = 333
        Top = 0
        Width = 155
        Height = 21
        Hint = 'Apellido paterno'
        CharCase = ecUpperCase
        Color = 16444382
        MaxLength = 60
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnKeyPress = txt_NombreKeyPress
      end
      object cbSexo: TComboBox
        Left = 108
        Top = 24
        Width = 114
        Height = 21
        Style = csDropDownList
        Color = 16444382
        ItemHeight = 13
        TabOrder = 3
      end
      object txtFechaNacimiento: TDateEdit
        Left = 332
        Top = 24
        Width = 98
        Height = 21
        AutoSelect = False
        TabOrder = 4
        OnExit = txtFechaNacimientoExit
        Date = -693594.000000000000000000
      end
      object txt_Nombre: TEdit
        Left = 108
        Top = 0
        Width = 145
        Height = 21
        Hint = 'Nombre'
        CharCase = ecUpperCase
        Color = 16444382
        MaxLength = 60
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnKeyPress = txt_NombreKeyPress
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1060
    Height = 25
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      1060
      25)
    object lbl_Personeria: TLabel
      Left = 7
      Top = 7
      Width = 67
      Height = 13
      Caption = 'Personer'#237'a:'
      FocusControl = cbPersoneria
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblRutRun: TLabel
      Left = 262
      Top = 7
      Width = 31
      Height = 13
      Caption = 'RUT:'
      FocusControl = txtDocumento
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblRazonSocial: TLabel
      Left = 457
      Top = 7
      Width = 80
      Height = 13
      Caption = 'Raz'#243'n Social:'
      FocusControl = txtRazonSocial
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblGiro: TLabel
      Left = 783
      Top = 6
      Width = 28
      Height = 13
      Caption = 'Giro:'
      FocusControl = txtGiro
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object cbPersoneria: TComboBox
      Left = 112
      Top = 3
      Width = 114
      Height = 21
      Hint = 'Tipo de Personer'#237'a'
      Style = csDropDownList
      Color = 16444382
      ItemHeight = 13
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnChange = cbPersoneriaChange
      Items.Strings = (
        'F'#237'sica                          F'
        'Jur'#237'dica                       J')
    end
    object txtDocumento: TEdit
      Left = 338
      Top = 3
      Width = 105
      Height = 21
      Hint = 'N'#250'mero de documento'
      CharCase = ecUpperCase
      Color = 16444382
      MaxLength = 9
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnKeyPress = txtDocumentoKeyPress
    end
    object txtRazonSocial: TEdit
      Left = 544
      Top = 3
      Width = 225
      Height = 21
      Hint = 'Raz'#243'n Social'
      CharCase = ecUpperCase
      Color = 16444382
      MaxLength = 60
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnKeyPress = txt_NombreKeyPress
    end
    object txtGiro: TEdit
      Left = 817
      Top = 3
      Width = 200
      Height = 21
      Hint = 'Raz'#243'n Social'
      Anchors = [akLeft, akTop, akRight]
      CharCase = ecUpperCase
      Color = 16444382
      MaxLength = 60
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnKeyPress = txt_NombreKeyPress
    end
  end
  object cboTipoCliente: TDBLookupComboBox
    Left = 903
    Top = 52
    Width = 131
    Height = 21
    Hint = 'Seleccionar tipo de cliente'
    Anchors = [akRight, akBottom]
    KeyField = 'CodigoTipoCliente'
    ListField = 'Descripcion'
    ListSource = srcTipoCliente
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
    OnCloseUp = cboTipoClienteCloseUp
    OnKeyPress = cboTipoClienteKeyPress
  end
  object cboSemaforo1: TDBLookupComboBox
    Left = 903
    Top = 30
    Width = 131
    Height = 21
    Hint = 'Seleccionar Valor para Semaforo 1'
    Anchors = [akRight, akBottom]
    KeyField = 'CodigoTipoCliente'
    ListField = 'Descripcion'
    ListSource = srcSemaforo1
    ParentShowHint = False
    ShowHint = True
    TabOrder = 3
    OnCloseUp = cboSemaforo1CloseUp
    OnKeyPress = cboSemaforo1KeyPress
  end
  object cboSemaforo3: TDBLookupComboBox
    Left = 903
    Top = 74
    Width = 131
    Height = 21
    Hint = 'Seleccionar Valor para Semaforo 3'
    Anchors = [akRight, akBottom]
    KeyField = 'CodigoTipoCliente'
    ListField = 'Descripcion'
    ListSource = srcSemaforo3
    ParentShowHint = False
    ShowHint = True
    TabOrder = 4
    OnCloseUp = cboSemaforo3CloseUp
    OnKeyPress = cboSemaforo3KeyPress
  end
  object spTiposClientes: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'TiposClienteObtenerLista;1'
    Parameters = <>
    Left = 592
    Top = 64
  end
  object srcTipoCliente: TDataSource
    DataSet = spTiposClientes
    Left = 560
    Top = 64
  end
  object spSemaforo1ObtenerLista: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'Semaforo1ObtenerLista;1'
    Parameters = <>
    Left = 520
    Top = 64
  end
  object srcSemaforo1: TDataSource
    DataSet = spSemaforo1ObtenerLista
    Left = 488
    Top = 64
  end
  object srcSemaforo3: TDataSource
    DataSet = spSemaforo3ObtenerLista
    Left = 632
    Top = 64
  end
  object spSemaforo3ObtenerLista: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'Semaforo3ObtenerLista;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 664
    Top = 64
  end
end
