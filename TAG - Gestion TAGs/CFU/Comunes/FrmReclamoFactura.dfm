object FormReclamoFactura: TFormReclamoFactura
  Left = 98
  Top = 152
  BorderStyle = bsDialog
  Caption = 'Reclamo sobre comprobantes'
  ClientHeight = 511
  ClientWidth = 862
  Color = clBtnFace
  Constraints.MinHeight = 458
  Constraints.MinWidth = 870
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  Visible = True
  OnClose = FormClose
  OnDestroy = FormDestroy
  OnKeyPress = FormKeyPress
  DesignSize = (
    862
    511)
  PixelsPerInch = 96
  TextHeight = 13
  inline FrameContactoReclamo1: TFrameContactoReclamo
    Left = 1
    Top = 0
    Width = 627
    Height = 172
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 3
    TabStop = True
    ExplicitLeft = 1
    inherited gbUsuario: TGroupBox
      inherited LNumeroConvenio: TLabel
        Width = 103
        ExplicitWidth = 103
      end
    end
  end
  object PAbajo: TPanel
    Left = 0
    Top = 472
    Width = 862
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitTop = 385
    object PDerecha: TPanel
      Left = 616
      Top = 0
      Width = 246
      Height = 39
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object CKRetenerOrden: TCheckBox
        Left = 3
        Top = 11
        Width = 89
        Height = 17
        Caption = 'Retener Orden'
        TabOrder = 0
      end
      object AceptarBTN: TButton
        Left = 96
        Top = 6
        Width = 70
        Height = 25
        Caption = '&Aceptar'
        Default = True
        TabOrder = 1
        OnClick = AceptarBTNClick
      end
      object btnCancelar: TButton
        Left = 171
        Top = 6
        Width = 70
        Height = 25
        Cancel = True
        Caption = '&Cancelar'
        ModalResult = 2
        TabOrder = 2
        OnClick = btnCancelarClick
      end
    end
  end
  object PageControl: TPageControl
    Left = 5
    Top = 173
    Width = 610
    Height = 332
    ActivePage = TabSheetDatos
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    ExplicitHeight = 245
    object TabSheetDatos: TTabSheet
      Caption = 'Datos del Reclamo'
      ExplicitHeight = 217
      DesignSize = (
        602
        304)
      object Lcomprobante: TLabel
        Left = 7
        Top = 13
        Width = 66
        Height = 13
        Caption = 'Comprobante:'
      end
      object txtComprobante: TEdit
        Left = 108
        Top = 10
        Width = 321
        Height = 21
        TabStop = False
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 0
      end
      object Panel1: TPanel
        Left = 3
        Top = 38
        Width = 590
        Height = 391
        Anchors = [akLeft, akTop, akRight, akBottom]
        BevelOuter = bvNone
        TabOrder = 1
        ExplicitHeight = 304
        object txtDetalle: TMemo
          Left = 0
          Top = 132
          Width = 590
          Height = 259
          Align = alClient
          TabOrder = 3
          ExplicitHeight = 172
        end
        object Panel3: TPanel
          Left = 0
          Top = 113
          Width = 590
          Height = 19
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 2
          object Label3: TLabel
            Left = 3
            Top = 2
            Width = 97
            Height = 13
            Caption = 'Otras observaciones'
          end
        end
        object pnlPago: TPanel
          Left = 0
          Top = 0
          Width = 590
          Height = 54
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          Visible = False
          object Label4: TLabel
            Left = 4
            Top = 3
            Width = 76
            Height = 13
            Caption = 'Fecha de Pago:'
          end
          object Label5: TLabel
            Left = 4
            Top = 30
            Width = 73
            Height = 13
            Caption = 'Lugar de Pago:'
          end
          object txtFechaPago: TDateEdit
            Left = 105
            Top = 0
            Width = 90
            Height = 21
            AutoSelect = False
            TabOrder = 0
            Date = -693594.000000000000000000
          end
          object txtLugarPago: TEdit
            Left = 105
            Top = 27
            Width = 321
            Height = 21
            TabOrder = 1
          end
        end
        object pnlReclamoContenido: TPanel
          Left = 0
          Top = 88
          Width = 590
          Height = 25
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          Visible = False
          object Label1: TLabel
            Left = 4
            Top = 3
            Width = 79
            Height = 13
            Caption = 'Tipo de reclamo:'
          end
          object cbSubtipoReclamo: TVariantComboBox
            Left = 105
            Top = 0
            Width = 209
            Height = 21
            Style = vcsDropDownList
            DroppedWidth = 0
            ItemHeight = 13
            TabOrder = 0
            Items = <
              item
                Caption = 'Peajes del per'#237'odo en general'
                Value = 1
              end
              item
                Caption = 'Peajes anteriores en general'
                Value = 2
              end
              item
                Caption = 'Intereses'
                Value = 3
              end
              item
                Caption = 'Ajustes'
                Value = 4
              end
              item
                Caption = 'Saldo anterior'
                Value = 5
              end
              item
                Caption = 'Otros'
                Value = 6
              end>
          end
        end
        object PnlNumeroRecibo: TPanel
          Left = 0
          Top = 54
          Width = 590
          Height = 34
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 4
          Visible = False
          object LNumeroRecibo: TLabel
            Left = 4
            Top = 8
            Width = 95
            Height = 13
            Caption = 'Numero de  Recibo:'
          end
          object ENumeroRecibo: TNumericEdit
            Left = 105
            Top = 4
            Width = 134
            Height = 21
            TabOrder = 0
          end
        end
      end
    end
    object TabSheetProgreso: TTabSheet
      Caption = 'Progreso / soluci'#243'n'
      ImageIndex = 3
      object Ldetalledelprogreso: TLabel
        Left = 0
        Top = 88
        Width = 602
        Height = 13
        Align = alTop
        Caption = '  Detalles del progreso / soluci'#243'n'
        ExplicitWidth = 155
      end
      object PResolucion: TPanel
        Left = 0
        Top = 0
        Width = 602
        Height = 82
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        Visible = False
        object LresoluciondelCaso: TLabel
          Left = 13
          Top = 10
          Width = 100
          Height = 13
          Caption = 'Resoluci'#243'n del Caso:'
        end
        object ckDireccionIncorrecta: TCheckBox
          Left = 12
          Top = 58
          Width = 249
          Height = 17
          Caption = 'La direcci'#243'n estaba mal y hubo que corregirla'
          TabOrder = 0
        end
        object Eresolucion: TVariantComboBox
          Left = 125
          Top = 6
          Width = 240
          Height = 21
          Style = vcsDropDownList
          DroppedWidth = 0
          ItemHeight = 13
          TabOrder = 1
          Items = <
            item
              Caption = 'Ninguna'
              Value = '0'
            end
            item
              Caption = 'No se habia Emitido Nota de Cobro'
              Value = '1'
            end
            item
              Caption = 'Se Remiti'#243' Nota de Cobro por Email'
              Value = '2'
            end
            item
              Caption = 'Se Remiti'#243' Nota de Cobro por Correo'
              Value = '3'
            end
            item
              Caption = 'Cliente Retira Copia en COPEC'
              Value = '4'
            end
            item
              Caption = 'Cliente Imprime desde la Web'
              Value = '5'
            end>
        end
        object CkRecibioNC: TCheckBox
          Left = 12
          Top = 36
          Width = 249
          Height = 17
          Caption = 'Recibio alguna vez Nota de Cobro'
          TabOrder = 2
        end
      end
      object txtDetalleSolucion: TMemo
        Left = 0
        Top = 107
        Width = 602
        Height = 197
        Align = alClient
        TabOrder = 1
      end
      object Pdivisor: TPanel
        Left = 0
        Top = 101
        Width = 602
        Height = 6
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 2
      end
      object Pdivisor2: TPanel
        Left = 0
        Top = 82
        Width = 602
        Height = 6
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 3
      end
    end
    object TabSheetRespuesta: TTabSheet
      Caption = 'Respuesta'
      ImageIndex = 3
      inline FrameRespuestaOrdenServicio1: TFrameRespuestaOrdenServicio
        Left = 0
        Top = 0
        Width = 602
        Height = 304
        Align = alClient
        TabOrder = 0
        TabStop = True
        ExplicitWidth = 602
        ExplicitHeight = 304
        inherited LRespuestadelaConcesionaria: TLabel
          Width = 602
        end
        inherited LComentariosDelCliente: TLabel
          Width = 602
          ExplicitWidth = 116
        end
        inherited PContactarCliente: TPanel
          Width = 602
          ExplicitWidth = 602
        end
        inherited txtDetalleRespuesta: TMemo
          Width = 602
          ExplicitWidth = 602
        end
        inherited TxtComentariosdelCliente: TMemo
          Width = 602
          Height = 130
          ExplicitWidth = 602
          ExplicitHeight = 130
        end
      end
    end
    object TabSheetHistoria: TTabSheet
      Caption = 'Historia'
      ImageIndex = 4
      ExplicitHeight = 217
      inline FrameHistoriaOrdenServicio: TFrameHistoriaOrdenServicio
        Left = 0
        Top = 0
        Width = 602
        Height = 304
        Align = alClient
        TabOrder = 0
        TabStop = True
        ExplicitWidth = 602
        ExplicitHeight = 217
        inherited DBLHistoria: TDBListEx
          Width = 602
          Height = 228
          Columns = <
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 120
              Header.Caption = 'F. Modificaci'#243'n'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'FechaHoraModificacion'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 100
              Header.Caption = 'F. Compromiso'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'FechaCompromiso'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 60
              Header.Caption = 'Usuario'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'Usuario'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 60
              Header.Caption = 'Prioridad'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'Prioridad'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 60
              Header.Caption = 'Estado'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescEstado'
            end>
          ExplicitWidth = 602
          ExplicitHeight = 228
        end
        inherited Pencabezado: TPanel
          Width = 602
          ExplicitWidth = 602
          inherited Pder: TPanel
            Left = 594
            ExplicitLeft = 594
          end
          inherited Parriva: TPanel
            Width = 602
            ExplicitWidth = 602
          end
        end
        inherited Pabajo: TPanel
          Top = 263
          Width = 602
          ExplicitTop = 263
          ExplicitWidth = 602
        end
      end
    end
  end
  inline FrameCompromisoOrdenServicio1: TFrameCompromisoOrdenServicio
    Left = 623
    Top = 69
    Width = 239
    Height = 102
    Anchors = [akTop, akRight]
    TabOrder = 2
    TabStop = True
    ExplicitLeft = 623
    ExplicitTop = 69
    inherited GBCompromisoCliente: TGroupBox
      inherited Label1: TLabel
        Width = 44
        ExplicitWidth = 44
      end
      inherited Label2: TLabel
        Width = 103
        ExplicitWidth = 103
      end
    end
  end
  inline FrameSolucionOrdenServicio1: TFrameSolucionOrdenServicio
    Left = 622
    Top = 171
    Width = 240
    Height = 116
    Anchors = [akTop, akRight]
    TabOrder = 4
    TabStop = True
    ExplicitLeft = 622
    ExplicitTop = 171
    ExplicitHeight = 116
    inherited GBEstadoDelReclamo: TGroupBox
      inherited Label1: TLabel
        Width = 36
        ExplicitWidth = 36
      end
      inherited PAdicional: TPanel
        inherited Label2: TLabel
          Width = 72
          ExplicitWidth = 72
        end
        inherited Label3: TLabel
          Width = 39
          ExplicitWidth = 39
        end
      end
    end
  end
  inline FrameFuenteReclamoOrdenServicio1: TFrameFuenteReclamoOrdenServicio
    Left = 622
    Top = 6
    Width = 239
    Height = 65
    Anchors = [akTop, akRight]
    TabOrder = 5
    TabStop = True
    ExplicitLeft = 622
    ExplicitTop = 6
  end
  inline FrameNumeroOrdenServicio1: TFrameNumeroOrdenServicio
    Left = 622
    Top = 285
    Width = 239
    Height = 68
    Anchors = [akTop, akRight]
    TabOrder = 6
    TabStop = True
    ExplicitLeft = 622
    ExplicitTop = 285
    inherited PReclamo: TGroupBox
      inherited LOrden: TLabel
        Width = 72
        ExplicitWidth = 72
      end
    end
  end
  inline FrameConcesionariaReclamoOrdenServicio1: TFrameConcesionariaReclamoOrdenServicio
    Left = 625
    Top = 349
    Width = 235
    Height = 56
    Anchors = [akTop, akRight]
    TabOrder = 7
    ExplicitLeft = 625
    ExplicitTop = 349
    inherited GBConcesionaria: TGroupBox
      Left = -6
      Top = 5
      ExplicitLeft = -6
      ExplicitTop = 5
      inherited lblConcesionaria: TLabel
        Width = 70
        ExplicitWidth = 70
      end
    end
  end
  inline FrameSubTipoReclamoOrdenServicio1: TFrameSubTipoReclamoOrdenServicio
    Left = 623
    Top = 402
    Width = 240
    Height = 55
    Anchors = [akTop, akRight]
    TabOrder = 8
    ExplicitLeft = 623
    ExplicitTop = 402
    inherited GBSubTipo: TGroupBox
      inherited lblSubTipo: TLabel
        Width = 88
        ExplicitWidth = 88
      end
    end
  end
  object spObtenerOrdenServicioFacturacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerOrdenServicioFacturacion;1'
    Parameters = <>
    Left = 624
    Top = 360
  end
end
