object FormMedioEnvioMail: TFormMedioEnvioMail
  Left = 0
  Top = 0
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = ' Medio de env'#237'o por E-Mail'
  ClientHeight = 211
  ClientWidth = 498
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object btnAceptar: TButton
    Left = 334
    Top = 178
    Width = 75
    Height = 25
    Caption = 'Aceptar'
    TabOrder = 0
    OnClick = btnAceptarClick
  end
  object btnCancelar: TButton
    Left = 414
    Top = 178
    Width = 75
    Height = 25
    Caption = 'Cancelar'
    TabOrder = 1
    OnClick = btnCancelarClick
  end
  object tpMedioEnvio: TPanel
    Left = 8
    Top = 116
    Width = 481
    Height = 54
    TabOrder = 2
    object Label1: TLabel
      Left = 7
      Top = 7
      Width = 74
      Height = 13
      Caption = 'Direcci'#243'n E-Mail'
    end
    object txtEmail: TEdit
      Left = 7
      Top = 25
      Width = 465
      Height = 21
      TabOrder = 0
    end
  end
  object pnl_ConvenioNoTieneEmail: TPanel
    Left = 8
    Top = 8
    Width = 481
    Height = 51
    BevelInner = bvRaised
    BevelOuter = bvLowered
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
    object lblEstadoClienteEMail: TLabel
      Left = 13
      Top = 7
      Width = 454
      Height = 16
      Alignment = taCenter
      AutoSize = False
      Caption = 'El Cliente NO tiene E-Mail'
    end
    object lblEstadoConvenioEnvioPorEMail: TLabel
      Left = 13
      Top = 27
      Width = 454
      Height = 16
      Alignment = taCenter
      AutoSize = False
      Caption = 'El Cliente no est'#225' adherido al env'#237'o de Documentos v'#237'a E-Mail'
      Transparent = True
    end
  end
  object grp1: TGroupBox
    Left = 8
    Top = 64
    Width = 481
    Height = 45
    TabOrder = 4
    object chkNotaCobroPorEMail: TCheckBox
      Left = 26
      Top = 18
      Width = 327
      Height = 17
      Caption = #191' Desea recibir Gratuitamente su Nota de Cobro v'#237'a E-Mail ?'
      TabOrder = 0
    end
  end
end
