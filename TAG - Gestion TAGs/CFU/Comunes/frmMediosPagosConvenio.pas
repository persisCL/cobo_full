{-----------------------------------------------------------------------------
 Unit Name: frmMediosPagosConvenio
 Author:
 Purpose:  Gestion de Datos de Medios de Pago de un Convenio
 History:
------------------------------------------------------------------------------
 Revision 1:
 Author      : ggomez
 Date        : 23/06/2005
 Description :
    - Controlar la obligatoriedad de ciertos datos seg�n el Origen del Mandato.
    - Si se ingresa RUT del Mandante se exigen los datos Personer�a, Nombre,
    Apellido, Raz�n Social.
------------------------------------------------------------------------------
 Revision 2:
 Author      : ggomez
 Date        : 24/06/2005
 Description :
    - Si no se ingresa el RUT del mandante, deshabilitar los campos de los datos
    del mandante.
    - M�todo ArmarMedioPago: Si el Tipo de Tarjeta es Transbank no
    mostrar "TRANSBANK".
 ------------------------------------------------------------------------------
 Revisi�n 3:
 Author      : ddiaz
 Date        : 08/09/2006
 Description : Se modifica devoluci�n de la variable MedioPago para que devuelva el n�mero de tarjeta de cr�dito
    formateado con X menos los �ltimos 4 d�gitos. SS 0234
------------------------------------------------------------------------------
 Revisi�n 4:
 Author      : lgisuk
 Date        : 08/03/2007
 Description : btnAceptarClick : Ahora el a�o del fecha de vencimiento ingresada, se considerara
 mayor al a�o 2000, por ejemplo si se ingresa la fecha 12/49, el sistema
 considerara que es el a�o 2049 y no 1949.
-----------------------------------------------------------------------------
 Revisi�n 5:
 Author      : FSandi
 Date        : 16/07/2007
 Description : Nrocuenta y NroTarjeta, evitamos que se ingrese el caracter ' para evitar problemas con las
                funciones QueryGetValue
-----------------------------------------------------------------------------
 Revision 6:
 Date        : 25/02/2009
 Author      : mpiazza
 Description :  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura

 Revision 7:
 Date        : 03 de Junio de 2010
 Author      : jjofre
 Description :  Ref (SS-884) Se modifican las siguientes funciones
                -btnAceptarClick
                -ValidarMandante
                -ColorearCamposObligatorios


Description : Se agrega validaci�n en m�todo btnAceptarClick para que la validaci�n
              de los medios de pago excluya a los ConveniosSinCuenta.
Firma       : SS_628_ALA_20110720

Description : Se quitan los par�metros del m�todo ObtenerConvenioMedioPago.
Firma       : SS_628_ALA_20110818

Firma       : SS-1068_NDR_20120910
Description : Enmascarar el Numero de Cuenta Bancaria para los PAC (similar a la mascara de las tarjetas de credito)

Firma       : SS_1068_NDR_20120910_CQU
Description : Se agrega Enmascarar el Numero de Cuenta Bancaria para lo PAC cuando se edita el medio de pago, valida que exista Permiso.
              esto en el m�todo Inicializar.

Firma       :   SS_1325_CQU_20150625
Descripcion :   La validacion de la tarjeta de credito se hace sobre el objeto txt_NroTarjetaCredito,
                cuando un usuario no tiene permiso ('Ver_Numero_PAT_PAC') para ver el numero de tarjeta
                �sta se enmascara por lo tanto al validar se hace con la mascara lo que esta incorrecto.
                Se modifica la validaci�n para que evalue si tiene o no el permiso y coloque el valor correspondiente.
-----------------------------------------------------------------------------}


unit frmMediosPagosConvenio;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,PeaTypes,RStrings, validate, UtilDB, Util, UtilProc, PeaProcs,
  StdCtrls, ExtCtrls, DPSControls, DmConnection, DmiCtrls, Mask,
  FreTelefono, Buttons, frmDatoPesona, DB, ADODB, ComCtrls, StrUtils,PeaProcsCN;           //SS_1068_NDR_20120910

type
  TFormMediosPagosConvenio = class(TForm)
    Notebook: TNotebook;
    gb_PAT: TGroupBox;
    gb_DatosPersonales: TGroupBox;
    gb_PAC: TGroupBox;
    cb_TipoCuenta: TComboBox;
    lbl_TipoCuenta: TLabel;
    lbl_Banco: TLabel;
    cb_Banco: TComboBox;
    lbl_NumeroCuenta: TLabel;
    txt_NumeroCuenta: TEdit;
    txt_Sucursal: TEdit;
    lbl_Sucursal: TLabel;
    Panel1: TPanel;
    pnlBotton: TPanel;
    lbl_FechaVencimiento: TLabel;
    txt_FechaVencimiento: TMaskEdit;
    Me_Mensaje: TLabel;
    Pc_Datos: TPageControl;
    Tab_Juridica: TTabSheet;
    lbl_RazonSocial: TLabel;
    txt_RazonSocial: TEdit;
    Tab_Fisica: TTabSheet;
    lbl_ApellidoMaternoMandante: TLabel;
    lbl_ApellidoMandante: TLabel;
    lbl_NombreMandante: TLabel;
    txtApellidoMaterno: TEdit;
    txtApellido: TEdit;
    txtNombre: TEdit;
    lbl_PersoneriaMandante: TLabel;
    cbPersoneria: TComboBox;
    lblRutRun: TLabel;
    txtDocumento: TEdit;
    FrameTelefono: TFrameTelefono;
    ObtenerDatosPersona: TADOStoredProc;
    ObtenerMedioComunicacionPricipalPersona: TADOStoredProc;
    cb_EmisoresTarjetasCreditos: TComboBox;
    lbl_EmisorTarjeta: TLabel;
    btnDatosConvenio: TSpeedButton;
    VerificarMedioPagoAutomatico: TADOStoredProc;
    pnl_Confirmacion: TPanel;
    lbl_EstadoTipoMedioPago: TLabel;
    chk_ConfirmarTipoMedioPago: TCheckBox;
    btnAceptar: TButton;
    btnSalir: TButton;
    lbl_TipoTarjeta: TLabel;
    cb_TipoTarjeta: TComboBox;
    lbl_NumeroTarjetaCredito: TLabel;
    txt_NroTarjetaCredito: TEdit;
    qry_prefijos: TADOQuery;
    procedure cb_TipoCuentaClick(Sender: TObject);
    procedure cb_TipoTarjetaClick(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
    procedure cb_BancoClick(Sender: TObject);
    procedure btnDatosConvenioClick(Sender: TObject);
    procedure FreDatoPresonatxtDocumentoChange(Sender: TObject);
    procedure cbPersoneriaChange(Sender: TObject);
    procedure Pc_DatosEnter(Sender: TObject);
    procedure txtDocumentoChange(Sender: TObject);
    procedure LimpiarControles;
    procedure txtDocumentoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cb_EmisoresTarjetasCreditosClick(Sender: TObject);
    procedure txt_NroTarjetaCreditoKeyPress(Sender: TObject; var Key: Char);
    procedure chk_ConfirmarTipoMedioPagoClick(Sender: TObject);
    procedure txt_NroTarjetaCreditoChange(Sender: TObject);
    procedure txt_NumeroCuentaChange(Sender: TObject);
    procedure txt_NumeroCuentaKeyPress(Sender: TObject; var Key: Char);
    procedure txt_FechaVencimientoExit(Sender: TObject);
    procedure cb_TipoTarjetaChange(Sender: TObject);
    procedure txt_NroTarjetaCreditoExit(Sender: TObject);
    procedure cb_TipoTarjetaExit(Sender: TObject);
  private
    { Private declarations }
    FRegistro:TDatosMediosPago;
    FCodigoAreaConvenio: integer;
    FTelefonoConvenio: string;
    FRegistroPersonaConvenio: TDatosPersonales;
    FRegistroPersonaConvenioActual: TDatosPersonales;
    FRegistroPersonaConvenioFinal: TDatosPersonales;
    FConvenioPuro : Boolean;
    FIndiceTelLevantado: Integer;
    FIndiceTelefono: Integer;
    FCodigoCovenio: Integer;
    FOrigenMedioPago: String;
    FTipoCuentaOriginal: Integer;
    FTipoTarjetaOriginal: Integer;
    FEmisorTarjetaOriginal: Integer;
    FBancoOriginal: Integer;
    FNroCuentaOTarjeta: String[30];
    FEstadoInicialForzarConfirmadoEnabled: Boolean;
    FEstadoInicialForzarConfirmadoChecked: Boolean;
    FForzarConfirmado: Boolean;
    FPuedeForzarConfirmado: Boolean;
    function DarTipoCuenta:Integer;
    function DarCodigoTarjetaCredito: Integer;
    function DarCodigoEmisorTarjetaCredito:Integer;
    function DarBanco:Integer;
    function LuhnChecksumValid(const Value: string): Boolean;
    procedure VerificarCliente;
    procedure Limpiar(var Persona: TDatosPersonales);
    function GetTelefonoHabilitado: Boolean;
    function GetTTipoMedioComunicacion: TTipoMedioComunicacion;
    procedure ColorearCamposObligatorios;
    procedure VerificarValoresIniciales(NroCuentaOTarjeta: String);
    procedure HabilitarDatosMandante(Valor: Boolean);
  public
    { Public declarations }
    property ForzarConfirmado: Boolean read FForzarConfirmado;
    property PuedeForzarConfirmado: Boolean read FPuedeForzarConfirmado;
    property RegistroMedioPago:TDatosMediosPago read FRegistro;
    property RegistroPersonaMandante: TDatosPersonales read FRegistroPersonaConvenioFinal;
    property TelefonoHabilitado: Boolean read GetTelefonoHabilitado;
    property Telefono: TTipoMedioComunicacion read GetTTipoMedioComunicacion;
    function Inicializar(Caption: TCaption;
        Registro: TDatosMediosPago;  //Datos de la Solicitud
        RegistroMandante: TDatosPersonales; //Datos del mandante si esta en Edicion
        RegistroPersonaConvenio: TDatosPersonales; //Datos Cargados en el Convenio
        OrigenMedioPago: String;
        Modo: TStateConvenio = scAlta;
        CodigoAreaConvenio: integer = 0;
        TelefonoConvenio: String = '';
        SoloLectura: boolean = False;
        ConvenioPuro:Boolean = False;
        IndiceMedioComunicacion: Integer = -1;
        CodigoConvenio: Integer = -1;
        ForzarConfirmado: Boolean = False): Boolean;
    class procedure LimpiarMedioPago(var Registro: TDatosMediosPago);
    class procedure LimpiarMandante(var Registro: TDatosPersonales);
    class function ValidarMedioPago(Registro: TDatosMediosPago; OrigenMedioPago: AnsiString;
        NumeroDocumento: String; var mensaje: AnsiString; ConvenioPuro: Boolean = False): Boolean;
    class function ValidarMandante(OrigenMedioPago: AnsiString;
        Registro: TDatosPersonales; NumeroDocumentoConvenio: String;
        var Mensaje: AnsiString): Boolean;
    class function ArmarMedioPago(Registro: TDatosMediosPago; Confirmado: Boolean;
        CodigoConvenio: Integer; var MedioPago: AnsiString): Boolean;
  end;

var
	bYaValidado: Boolean;

resourcestring
	MSG_ERROR_VALIDACION_TC	= 'No se pudo validar el N� de Tarjeta de Cr�dito';
	MSG_ERROR_CANTIDAD_DIGITOS = 'La cantidad de d�gitos ingresados no corresponde al tipo de tarjeta seleccionado';
    MSG_ERROR_NUM_TARJETA_NO_CORRESPONDE_A_TIPO = 'N�mero de Tarjeta de Cr�dito no corresponde a Tipo de Tarjeta seleccionado';
    MSG_ERROR_NUM_TARJETA_INCORRECTO = 'N�mero de Tarjeta de Cr�dito incorrecto';
    STR_MEDIOS_PAGO_CONVENIO = 'Medios de Pago';

implementation

{$R *.dfm}

const
    CONFIRMADO = 'Medio de Pago Confirmado';
    NO_CONFIRMADO = 'Medio de Pago NO Confirmado';

{******************************** Function Header ******************************
Function Name: Inicializar
Author :
Date Created :
Description :
Parameters : Caption: TCaption; Registro: TDatosMediosPago; RegistroMandante: TDatosPersonales; RegistroPersonaConvenio: TDatosPersonales; OrigenMedioPago: String; Modo: TStateConvenio = scAlta; CodigoAreaConvenio: integer = 0; TelefonoConvenio: String = ''; SoloLectura: boolean = False; ConvenioPuro:Boolean = False; IndiceMedioComunicacion: Integer = -1; CodigoConvenio: Integer = -1; ForzarConfirmado: Boolean = False
Return Value : Boolean

Revision 1:
    Author : ggomez
    Date : 07/11/2006
    Description : En el caso de PAT:
        - En el select que llama a la FN FormatNumeroTarjeta, agregu� la
        la sentencia QuotedStr, para que la cadena del select se arme con un
        string. Esto se hizo pues al pasarle un n� de tarjeta vac�o fallaba.
        Adem�s, la FN FormatNumeroTarjeta debe recibir como par�metro un string.
        - Agregu� un Trim a lo que retorna la FN FormatNumeroTarjeta, asi en el
        control para el n� de tarjeta no se incorporan los espacios iniciales ni
        finales.

Revision 3:
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura
*******************************************************************************}
function TFormMediosPagosConvenio.Inicializar(Caption: TCaption;
                                  Registro: TDatosMediosPago;
                                  RegistroMandante: TDatosPersonales;
                                  RegistroPersonaConvenio: TDatosPersonales;
                                  OrigenMedioPago: String;
                                  Modo: TStateConvenio = scAlta;
                                  CodigoAreaConvenio: integer = 0;
                                  TelefonoConvenio: String = '';
                                  SoloLectura: boolean = False;
                                  ConvenioPuro:Boolean = False;
                                  IndiceMedioComunicacion: Integer = -1;
                                  CodigoConvenio: Integer = -1;
                                  ForzarConfirmado: Boolean = False): Boolean;

        procedure Habilitar(Control: TWinControl);
        begin
            if Control <> nil then Begin
                Control.Enabled := True;
                Habilitar(Control.Parent);
            end;
        end;

var
    Alta: Boolean;
begin
    try

        Result := False;

        Self.Caption        := Caption;
        FCodigoCovenio      := CodigoConvenio;
        FOrigenMedioPago    := UpperCase(OrigenMedioPago);

        //Los Tel cargados en el convenio
        FCodigoAreaConvenio := CodigoAreaConvenio;
        FTelefonoConvenio   := TelefonoConvenio;

        //El medio de Pago de la Solicitud
        FRegistro := Registro;

        //Los datos cargados en el convenio
        FRegistroPersonaConvenioActual := RegistroPersonaConvenio;

        //Hago esto porque me viene en el CodigoPersona el Indice persona de la solicitud
        FConvenioPuro := ConvenioPuro;

        GestionarComponentesyHints(Self, DMConnections.BaseCAC,
            [cb_TipoTarjeta, cb_EmisoresTarjetasCreditos, cb_TipoCuenta,
                cb_Banco, txt_NumeroCuenta, txt_Sucursal, txt_NroTarjetaCredito,
                txt_FechaVencimiento]);

        FrameTelefono.Inicializa(STR_TELEFONO, False, False, False);
        FIndiceTelLevantado := IndiceMedioComunicacion;
        FIndiceTelefono     := IndiceMedioComunicacion;

        // Cargo el mensaje
        Me_Mensaje.Caption := QueryGetValue(DMConnections.BaseCAC,
                                'SELECT Descripcion FROM Mensajes where CodigoMensaje = 143');

        CargarPersoneria(cbPersoneria, iif(trim(RegistroMandante.Personeria) = EmptyStr, PERSONERIA_FISICA, Trim(RegistroMandante.Personeria)), False);

        if not ConvenioPuro then FRegistroPersonaConvenioActual.CodigoPersona := -1;

        FTipoCuentaOriginal     := FRegistro.PAC.TipoCuenta;
        FBancoOriginal          := FRegistro.PAC.CodigoBanco;
        FTipoTarjetaOriginal    := FRegistro.PAT.TipoTarjeta;
        FEmisorTarjetaOriginal  := FRegistro.PAT.EmisorTarjetaCredito;
        Alta := (FRegistro.PAC.TipoCuenta = -1) and (FRegistro.PAT.TipoTarjeta = -1);

        HabilitarDatosMandante(not (Trim(RegistroMandante.NumeroDocumento) = EmptyStr));

        if (Trim(RegistroMandante.NumeroDocumento) = Trim(RegistroPersonaConvenio.NumeroDocumento))
                or (Trim(RegistroPersonaConvenio.NumeroDocumento) =  Trim(Registro.RUT)) then begin
            btnDatosConvenio.Click;
        end else begin
            //Tomo los datos de la Solicitud
            if not (Alta) and ((Trim(RegistroMandante.RazonSocial) = EmptyStr) and (Trim(RegistroMandante.Apellido) = EmptyStr)) then begin
                LimpiarControles;
                txt_RazonSocial.Text    := iif(Trim(RegistroMandante.RazonSocial) <> EmptyStr, Trim(Registro.Nombre), EmptyStr);
                txtApellido.Text        := iif(Trim(RegistroMandante.Apellido) = EmptyStr, Trim(Registro.Nombre), EmptyStr);
                FrameTelefono.CargarTelefono(Registro.CodigoArea, Registro.Telefono, 0);
                txtDocumento.Text       := Trim(Registro.RUT);
            end
            else//Tomo los datos del Mandato (Est� Editando)
                if not (Alta) then begin
                        FRegistroPersonaConvenio := RegistroMandante;
                        CargarPersoneria(cbPersoneria, RegistroMandante.Personeria, False);
                        cbPersoneriaChange(Self);
                        txtDocumento.Text       := RegistroMandante.NumeroDocumento;
                        txtNombre.Text          := RegistroMandante.Nombre;
                        txtApellido.Text        := RegistroMandante.Apellido;
                        txtApellidoMaterno.Text := RegistroMandante.ApellidoMaterno;
                        txt_RazonSocial.Text    := RegistroMandante.RazonSocial;
                        if FConvenioPuro then
                            //FrameTelefono.CargarTelefono(CodigoAreaConvenio,TelefonoConvenio, 0)
                            FrameTelefono.CargarTelefono(Registro.CodigoArea,Registro.Telefono, 0)
                        else
                            FrameTelefono.CargarTelefono(Registro.CodigoArea, Registro.Telefono, 0);
                        //Si la persona es distinta y no tiene conveniola puedo editar. En caso contrario no.
                        HabilitarDatosMandante(not NumeroDocumentoTieneConvenio(RegistroMandante.NumeroDocumento));
                end else LimpiarMandante(FRegistroPersonaConvenio);
        end;

        txt_FechaVencimiento.Text := '00' + DateSeparator + '00';

        case FRegistro.TipoMedioPago of
            PAT: begin
                    Notebook.PageIndex := 0;
					Caption := Caption + CAPTION_TITULAR_TARJETA_CREDITO;
                    if not (Alta) then begin
                        with FRegistro.PAT do begin
                            CargarTiposTarjetasCredito(DMConnections.BaseCAC, cb_TipoTarjeta, TipoTarjeta, True);
                            CargarEmisoresTarjetasCredito(DMConnections.BaseCAC, cb_EmisoresTarjetasCreditos, EmisorTarjetaCredito, True);
                            if ExisteAcceso('Ver_Numero_PAT_PAC') then begin
                                txt_NroTarjetaCredito.Text := Trim(NroTarjeta)
                            end else begin
                                txt_NroTarjetaCredito.Text := Trim(QueryGetValue(DMConnections.BaseCAC,
                                  'SELECT dbo.FormatNumeroTarjeta(' + QuotedStr(Trim(NroTarjeta)) + ')', 10));
                            end;
                            FNroCuentaOTarjeta := Trim(NroTarjeta);
                            if Length(Trim(FechaVencimiento)) = 5 then FechaVencimiento[3] := DateSeparator;
                            txt_FechaVencimiento.Text := iif(Length(FechaVencimiento) < 5, '00' + DateSeparator + '00', FechaVencimiento); // Siempre se guarda con una barra
                        end;
                    end else begin
                        CargarTiposTarjetasCredito(DMConnections.BaseCAC, cb_TipoTarjeta, 0, True);
                        CargarEmisoresTarjetasCredito(DMConnections.BaseCAC, cb_EmisoresTarjetasCreditos, -1, True);
                    end;
                    //INICIO:	20160712 CFU
                    bYaValidado						:= False;
                    txt_NroTarjetaCredito.MaxLength	:= QueryGetValueInt(DMConnections.BaseCAC, 'SELECT max(LongitudBanda) FROM TarjetasCreditoTipos  WITH (NOLOCK)');
                    //FIN:	20160712 CFU
                end;
            PAC: begin
                    Notebook.PageIndex := 1;
					Caption := Caption + CAPTION_TITULAR_CUENTA_BANCARIA;
                    if not (Alta) then begin
                        with FRegistro.PAC do begin
                            CargarTiposCuentas(DMConnections.BaseCAC, cb_TipoCuenta, TipoCuenta, True);
                            cb_TipoCuenta.OnClick(cb_TipoCuenta);
                            CargarBancosxCuentas(DMConnections.BaseCAC, cb_Banco, TipoCuenta, CodigoBanco, True);
                            //txt_NumeroCuenta.Text   := Trim(NroCuentaBancaria);                                       //  SS_1068_NDR_20120910_CQU
                            if ExisteAcceso('Ver_Numero_PAT_PAC') then begin                                            //  SS_1068_NDR_20120910_CQU
                                txt_NumeroCuenta.Text := Trim(NroCuentaBancaria)                                        //  SS_1068_NDR_20120910_CQU
                            end else begin                                                                              //  SS_1068_NDR_20120910_CQU
                                txt_NumeroCuenta.Text := Trim(QueryGetValue(DMConnections.BaseCAC,                      //  SS_1068_NDR_20120910_CQU
                                  'SELECT dbo.FormatNumeroTarjeta(' + QuotedStr(Trim(NroCuentaBancaria)) + ')', 10));   //  SS_1068_NDR_20120910_CQU
                            end;                                                                                        //  SS_1068_NDR_20120910_CQU
                            FNroCuentaOTarjeta      := Trim(NroCuentaBancaria);
                            txt_Sucursal.Text       := Trim(Sucursal);
                        end;
                    end else begin
                        CargarTiposCuentas(DMConnections.BaseCAC, cb_TipoCuenta, -1, True);
                        cb_TipoCuenta.OnClick(cb_TipoCuenta);
                    end;
                end;
            else Exit;
        end;

        ColorearCamposObligatorios;

        //Habilitar el ingreso de la tarjeta
        lbl_NumeroTarjetaCredito.Enabled      := True;
        txt_NroTarjetaCredito.Enabled         := True;
        lbl_FechaVencimiento.Enabled          := True;
        txt_FechaVencimiento.Enabled          := True;
        FEstadoInicialForzarConfirmadoEnabled := False;
        FEstadoInicialForzarConfirmadoChecked := False;
        FForzarConfirmado                     := False;
        FPuedeForzarConfirmado                := False;


        if ExisteAcceso('CONFIRMAR_TIPO_MEDIO_PAGO_AUTOMATICO') then begin
            FPuedeForzarConfirmado := True;
            FForzarConfirmado := ForzarConfirmado;
            chk_ConfirmarTipoMedioPago.Visible := True;
            chk_ConfirmarTipoMedioPago.Checked := ForzarConfirmado;
            FEstadoInicialForzarConfirmadoChecked := ForzarConfirmado;
            FEstadoInicialForzarConfirmadoEnabled := not (((Registro.TipoMedioPago = PAT) and (Registro.PAT.Confirmado)) or ((Registro.TipoMedioPago = PAC) and (Registro.PAC.Confirmado))) or ForzarConfirmado;
            chk_ConfirmarTipoMedioPago.Enabled := FEstadoInicialForzarConfirmadoEnabled;
        end;

        // Seteo el Caption de Confirmado
        if ((Registro.TipoMedioPago = PAC) and (Registro.PAC.Confirmado)) or ((Registro.TipoMedioPago = PAT) and (Registro.PAT.Confirmado)) or ForzarConfirmado then
            lbl_EstadoTipoMedioPago.Caption := CONFIRMADO
        else lbl_EstadoTipoMedioPago.Caption := NO_CONFIRMADO;

        if  SoloLectura then begin
            EnableControlsInContainer(Self, not SoloLectura);
            btnSalir.Enabled    := SoloLectura;
            pnlBotton.Enabled   := SoloLectura;
        end;

        if Modo = scNormal then begin
            txtDocumento.Text   := RegistroMandante.NumeroDocumento;
            EnableControlsInContainer(Self, False);
            btnSalir.Enabled    := True;
            pnlBotton.Enabled   := True;
        end;

        btnDatosConvenio.Visible := Trim(RegistroPersonaConvenio.NumeroDocumento) <> EmptyStr;

        Result := True;
    except
        Result := False;
        Exit;
    end;
end;

procedure TFormMediosPagosConvenio.VerificarValoresIniciales(NroCuentaOTarjeta: String);
begin
    if NroCuentaOTarjeta = '' then
        chk_ConfirmarTipoMedioPago.Enabled := False
   else begin
        if FNroCuentaOTarjeta =  NroCuentaOTarjeta then begin
            chk_ConfirmarTipoMedioPago.Enabled := FEstadoInicialForzarConfirmadoEnabled;
            chk_ConfirmarTipoMedioPago.Checked := FEstadoInicialForzarConfirmadoChecked;
            if FRegistro.PAC.Confirmado or FRegistro.PAT.Confirmado or FForzarConfirmado then
                lbl_EstadoTipoMedioPago.Caption := CONFIRMADO
            else lbl_EstadoTipoMedioPago.Caption := NO_CONFIRMADO;
        end else begin
            chk_ConfirmarTipoMedioPago.Enabled := True;
            chk_ConfirmarTipoMedioPago.Checked := False;
            lbl_EstadoTipoMedioPago.Caption := NO_CONFIRMADO;            
        end;
    end;
end;

procedure TFormMediosPagosConvenio.cb_TipoCuentaClick(Sender: TObject);
begin
    CargarBancosxCuentas(DMConnections.BaseCAC,cb_Banco,DarTipoCuenta,-1,true);

    if self.Visible then cb_Banco.SetFocus;
end;

function TFormMediosPagosConvenio.DarTipoCuenta: Integer;
begin
    if (trim(cb_TipoCuenta.Text)='') or (trim(cb_TipoCuenta.Text)=SELECCIONAR) then Result := FTipoCuentaOriginal
    else Result := StrToInt(Trim(StrRight(cb_TipoCuenta.Text, 30)));
end;

//INICIO:	20100712 CFU
procedure TFormMediosPagosConvenio.cb_TipoTarjetaChange(Sender: TObject);
var
	CodigoTC: Integer;
	LargoBandaTC: Int64;
begin
    if cb_TipoTarjeta.ItemIndex > 0 then begin
    	try
			CodigoTC		:= StrToInt(Trim(StrRight(cb_TipoTarjeta.Text, 30)));
			LargoBandaTC 	:= QueryGetValueInt(DMConnections.BaseCAC, 'SELECT LongitudBanda FROM TarjetasCreditoTipos  WITH (NOLOCK) WHERE CodigoTipoTarjetaCredito = ' + IntToStr(CodigoTC));
        except
        	LargoBandaTC 	:= QueryGetValueInt(DMConnections.BaseCAC, 'SELECT max(LongitudBanda) FROM TarjetasCreditoTipos  WITH (NOLOCK)');
        end;
        
        if Length(TrimRight(TrimLeft(Trim(txt_NroTarjetaCredito.Text)))) > LargoBandaTC then
        	txt_NroTarjetaCredito.Text := Copy(txt_NroTarjetaCredito.Text, 1, LargoBandaTC);
        
        txt_NroTarjetaCredito.MaxLength := LargoBandaTC;
    end;
    bYaValidado := False;
end;


function TFormMediosPagosConvenio.LuhnChecksumValid(const Value: string): Boolean;
var
	C: Char;
  	Digit: Integer;
  	Sum: Integer;
  	OddChar: Boolean;
begin
  	Sum := 0;
  	OddChar := odd(Length(Value));
  	for C in Value do
  	begin
    	Digit := ord(C) - ord('0');
    	if OddChar then
      		inc(Sum, Digit)
    	else if Digit < 5 then
      		inc(Sum, 2*Digit)
    	else
      		inc(Sum, 2*Digit - 9);
    	OddChar := not OddChar;
  	end;
  	Result := ((9*Sum) mod 10 = 0);
end;

procedure TFormMediosPagosConvenio.cb_TipoTarjetaExit(Sender: TObject);
var
	CodigoTC: Integer;
    //INICIO:	20160715 CFU
    LongCaracterIni: Integer;
    bPrefijoOK: Boolean;
    //FIN:	 	20160715 CFU
begin
	if (btnSalir.Focused) or (Length(txt_NroTarjetaCredito.Text) = 0) then
    	Exit;
        
	if (Length(TrimRight(TrimLeft(Trim(txt_NroTarjetaCredito.Text)))) = 0) or (txt_NroTarjetaCredito.Focused) then begin
    	bYaValidado := False;
    	txt_NroTarjetaCredito.SetFocus;
    	Exit;
    end;

    if (Length(txt_NroTarjetaCredito.Text) <> txt_NroTarjetaCredito.MaxLength) then begin
    	{INICIO:	20160715 CFU
        MessageDlg('La cantidad de d�gitos ingresados no corresponde al tipo de tarjeta seleccionado', mtError, [mbOK], 0);
        }
        MsgBoxErr(MSG_ERROR_VALIDACION_TC, MSG_ERROR_CANTIDAD_DIGITOS, STR_MEDIOS_PAGO_CONVENIO, MB_ICONSTOP);
        //FIN:		20160715 CFU
        txt_NroTarjetaCredito.SetFocus;
        Exit;
    end;

	//Se permite este c�digo de TC es para pruebas KTC
    if (Copy(txt_NroTarjetaCredito.Text, 1, 4) = '9876') then       //20160712 CFU
    	Exit;

    CodigoTC		:= StrToInt(Trim(StrRight(cb_TipoTarjeta.Text, 30)));
    {INICIO:	20160715 CFU
    if (not bYaValidado) and 
    (StrToInt(Copy(txt_NroTarjetaCredito.Text, 1, 1)) <> QueryGetValueInt(DMConnections.BaseCAC, 'SELECT CaracterInicialBanda FROM TarjetasCreditoTipos  WITH (NOLOCK) WHERE CodigoTipoTarjetaCredito = ' + IntToStr(CodigoTC))) then begin
    	MessageDlg('N�mero de Tarjeta de Cr�dito no corresponde a Tipo de Tarjeta seleccionado', mtError, [mbOk], 0);
        if bYaValidado then
        	txt_NroTarjetaCredito.Text := '';
        txt_NroTarjetaCredito.SetFocus;
        Exit;
    end;
    }
    LongCaracterIni	:= QueryGetValueInt(DMConnections.BaseCAC, 'SELECT CaracterInicialBanda FROM TarjetasCreditoTipos  WITH (NOLOCK) WHERE CodigoTipoTarjetaCredito = ' + IntToStr(CodigoTC));

    //poner u n ciclo while buscando los prefijos de la tabla TarjetasCreditoPrefijos asociados al c�digo de tarjerta
    bPrefijoOK	:= False;
    with qry_prefijos do begin
        if Active then
        	Close;
        Parameters.ParamByName('CodigoTipoTarjetaCredito').Value := CodigoTC;
        Open;
        if FieldByName('ValidarPrefijo').AsBoolean then begin

            while not Eof do begin
                if (Copy(txt_NroTarjetaCredito.Text, 1, LongCaracterIni) = Trim(FieldByName('Prefijo').AsString)) then begin
                    bPrefijoOK	:= True;
                    Break;
                end;
                Next;
            end;
            Close;

            if (not bYaValidado) and (not bPrefijoOK) then begin
            	MsgBoxErr(MSG_ERROR_VALIDACION_TC, MSG_ERROR_NUM_TARJETA_NO_CORRESPONDE_A_TIPO, STR_MEDIOS_PAGO_CONVENIO, MB_ICONSTOP);
                if bYaValidado then
                    txt_NroTarjetaCredito.Text := '';
                cb_TipoTarjeta.SetFocus;
                Exit;
            end;
        end;
    end;
	//FIN    : 20160715 CFU
    
    if (not bYaValidado) and (not LuhnChecksumValid(txt_NroTarjetaCredito.Text)) then begin
    	{INICIO:	20160715 CFU
        MessageDlg('N�mero de Tarjeta de Cr�dito incorrecto', mtError, [mbOk], 0);
        }
        MsgBoxErr(MSG_ERROR_VALIDACION_TC, MSG_ERROR_NUM_TARJETA_INCORRECTO, STR_MEDIOS_PAGO_CONVENIO, MB_ICONSTOP);
        //FIN	:	20160715 CFU
        if bYaValidado then
        	txt_NroTarjetaCredito.Text := '';
        txt_NroTarjetaCredito.SetFocus;
        Exit;
    end;

    txt_NroTarjetaCredito.SetFocus;
	bYaValidado := False;
end;
//FIN:		20160713 CFU

procedure TFormMediosPagosConvenio.cb_TipoTarjetaClick(Sender: TObject);
begin
    if self.Visible then begin
        if txt_NroTarjetaCredito.Enabled then cb_EmisoresTarjetasCreditos.SetFocus
        else txtDocumento.SetFocus;
    end;
end;


{*******************************************************************************
    Revision :2
    Author : vpaszkowicz
    Date : 18/01/2008
    Description : Se valida que se completen todos los campos para poder dar de
    alta o modificar el PAT.
*******************************************************************************}
procedure TFormMediosPagosConvenio.btnAceptarClick(Sender: TObject);
resourcestring
    MSG_ERROR_NUMERO_CUENTA_VACIO   = 'Debe insertar un Nro de Cuenta';
    MSG_ERROR_SUCURSAL_VACIO        = 'ATENCI�N, No Ingres� el nombre de la SUCURSAL';
    MSG_CAPTION_ALERTA              = 'Alerta';
var
    ErrorNumeroTarjetaCredito: String;
    ErrorNumero: Boolean;
    AnioTarjeta: Integer;
    Personeria: Char;
    AuxCodigoConvenio: Integer;
    AuxTarjetaCredito : string; // SS_1325_CQU_20150625
begin

    if (FRegistro.TipoMedioPago = PAT) then begin //Credito
        (* Validar los datos de PAT. *)

        //Comentado en rev.2
      {*  if (FOrigenMedioPago = CONST_ORIGEN_MEDIO_PAGO_PRESTO)
                or (FOrigenMedioPago = CONST_ORIGEN_MEDIO_PAGO_SANTANDER)
                or (FOrigenMedioPago = CONST_ORIGEN_MEDIO_PAGO_TRANSBANK) then begin
            (* Controlar la obligatoriedad y la validez del Nro de Tarjeta. *)
            ErrorNumero := True;
            if (Trim(cb_EmisoresTarjetasCreditos.Text) <> EmptyStr) and
                    (Trim(cb_EmisoresTarjetasCreditos.Text) <> SELECCIONAR) then begin
                ErrorNumero := ValidarTarjetaCredito(DMConnections.BaseCAC,
                                DarCodigoTarjetaCredito,
                                txt_NroTarjetaCredito.Text,
                                ErrorNumeroTarjetaCredito);
            end;

            if not (ValidateControls([txt_NroTarjetaCredito,
                    txt_NroTarjetaCredito],
                    [(Trim(txt_NroTarjetaCredito.Text) <> EmptyStr),
                    ErrorNumero],
                    Format(MSG_CAPTION_GESTION, [STR_MEDIOS_PAGOS]),
                    [Format(MSG_VALIDAR_DEBE_EL, [FLD_NRO_TARJETA]),
                    Format(MSG_ERROR_NUMERO_TARJETA, [ErrorNumeroTarjetaCredito])])) then Exit;

        end else begin }
            (* Controlar la obligatoriedad de todos los campos. *)
            if not (ValidateControls([cb_TipoTarjeta,
                    cb_EmisoresTarjetasCreditos,
                    txt_NroTarjetaCredito,
                    txt_FechaVencimiento],
                    [(cb_TipoTarjeta.ItemIndex > 0),
                    (cb_EmisoresTarjetasCreditos.ItemIndex > 0),
                    ( (not txt_NroTarjetaCredito.Enabled) or (trim(txt_NroTarjetaCredito.text) <> EmptyStr) ),
                    ( (not txt_FechaVencimiento.Enabled) or
                        ( (Length((trim(txt_FechaVencimiento.Text))) = 5) and
                            (Pos(' ', Trim(txt_FechaVencimiento.Text)) = 0) and
                            (Trim(txt_FechaVencimiento.Text) <> DateSeparator) ) )],
                    Format(MSG_CAPTION_GESTION, [STR_MEDIOS_PAGOS]),
                    [Format(MSG_VALIDAR_DEBE_EL, [FLD_TIPO_TARJETA]),
                    Format(MSG_VALIDAR_DEBE_EL, [FLD_TIPO_EMISOR]),
                    Format(MSG_VALIDAR_DEBE_EL, [FLD_NRO_TARJETA]),
                    MSG_ERROR_FECHA_VENCIMIENTO])) then Exit;

            (* Controlar la validez del n�mero de Tarjeta de Cr�dito.
            Controlar la Fecha de Vencimiento. *)
            AnioTarjeta := StrToInt(StrRight(txt_FechaVencimiento.Text, 2));

            AnioTarjeta := 2000 + AnioTarjeta; //modificado en revision 4

            // No puede ver la tarjeta por lo tanto se enmascara con X                      // SS_1325_CQU_20150625
            if not ExisteAcceso('Ver_Numero_PAT_PAC') then begin                            // SS_1325_CQU_20150625
                // Modific� la tarjeta por lo tanto no vienen X (POS = 0)                   // SS_1325_CQU_20150625
                if Pos('X', txt_NroTarjetaCredito.Text) = 0 then begin                      // SS_1325_CQU_20150625
                    if Trim(FRegistro.PAT.NroTarjeta) = Trim(txt_NroTarjetaCredito.Text)    // SS_1325_CQU_20150625
                    then AuxTarjetaCredito := FRegistro.PAT.NroTarjeta                      // SS_1325_CQU_20150625
                    else AuxTarjetaCredito := Trim(txt_NroTarjetaCredito.Text);             // SS_1325_CQU_20150625
                end else begin                                                              // SS_1325_CQU_20150625
                    // No Modific� la tarjeta por lo tanto vienen X                         // SS_1325_CQU_20150625
                    AuxTarjetaCredito := FRegistro.PAT.NroTarjeta;                          // SS_1325_CQU_20150625
                end;                                                                        // SS_1325_CQU_20150625
            // Puede ver la tarjeta por lo tanto NO se enmascara con X                      // SS_1325_CQU_20150625
            end else AuxTarjetaCredito := txt_NroTarjetaCredito.Text;                       // SS_1325_CQU_20150625

            ErrorNumero := ValidarTarjetaCredito(DMConnections.BaseCAC,
                            DarCodigoTarjetaCredito,
                            //txt_NroTarjetaCredito.Text,                                   // SS_1325_CQU_20150625
                            AuxTarjetaCredito,                                              // SS_1325_CQU_20150625
                            ErrorNumeroTarjetaCredito);
            if not ( ValidateControls([txt_NroTarjetaCredito,
                    txt_FechaVencimiento],
                    [ErrorNumero,
                    (((not txt_FechaVencimiento.Enabled) or
                        (StrToInt(StrLeft(txt_FechaVencimiento.Text, 2)) <= 12) and (StrToInt(StrLeft(txt_FechaVencimiento.Text, 2)) > 0) and
                        (StrToInt(IntToStr(AnioTarjeta) + StrLeft(txt_FechaVencimiento.Text, 2)) >= StrToInt(FormatDateTime('yyyymm', NowBase(DMConnections.BaseCAC))))))],
                    Format(MSG_CAPTION_GESTION, [STR_MEDIOS_PAGOS]),
                    [Format(MSG_ERROR_NUMERO_TARJETA, [ErrorNumeroTarjetaCredito]),
                    MSG_ERROR_FECHA_VENCIMIENTO]) ) then Exit;

   // comentado en rev.2
   //     end; // else if if FOrigenMedioPago = CONST_ORIGEN_MEDIO_PAGO_PRESTO

    end else begin //Debito
        (* Validar los datos de PAC. *)
        if (FOrigenMedioPago = CONST_ORIGEN_MEDIO_PAGO_PRESTO)
                or (FOrigenMedioPago = CONST_ORIGEN_MEDIO_PAGO_SANTANDER)
                or (FOrigenMedioPago = CONST_ORIGEN_MEDIO_PAGO_BANCOCHILE)
                or (FOrigenMedioPago = CONST_ORIGEN_MEDIO_PAGO_TRANSBANK) then begin
            (* Controlar el n�mero de la cuenta. *)

            if not ( ValidateControls([txt_NumeroCuenta],
                    [(Trim(txt_NumeroCuenta.Text) <> EmptyStr) and
                        ValidarNumeroCuentaBancaria(DMConnections.BaseCAC, DarTipoCuenta, txt_NumeroCuenta.Text)],
                    Format(MSG_CAPTION_GESTION, [STR_MEDIOS_PAGOS]),
                    [MSG_ERROR_NUMERO_CUENTA]) ) then Exit;

            if (Trim(txt_Sucursal.Text) = EmptyStr) then begin
                MsgBox(MSG_ERROR_SUCURSAL_VACIO,MSG_CAPTION_ALERTA,MB_ICONWARNING)
            end;

        end else begin
            (* Controlar la obligatoriedad de todos los campos. *)

            if not ( ValidateControls([cb_TipoCuenta,
                    cb_Banco,
                    txt_NumeroCuenta,
                    txt_NumeroCuenta,
                    txt_Sucursal],
                    [(cb_TipoCuenta.itemindex > 0),
                    (cb_Banco.itemindex > 0),
                    (Trim(txt_NumeroCuenta.Text)<>EmptyStr),
                    (ValidarNumeroCuentaBancaria(DMConnections.BaseCAC,DarTipoCuenta,txt_NumeroCuenta.Text)),
                     Trim(txt_Sucursal.Text) <> EmptyStr],
                    Format(MSG_CAPTION_GESTION, [STR_MEDIOS_PAGOS]),
                    [MSG_FALTA_CUENTA,
                    Format(MSG_VALIDAR_DEBE_EL, [FLD_BANCO]),
                    MSG_ERROR_NUMERO_CUENTA_VACIO,
                    MSG_ERROR_NUMERO_CUENTA,
                    Format(MSG_VALIDAR_DEBE_LA, [FLD_SUCURSAL])]) ) then Exit;
        end; // else if (FOrigenMedioPago = CONST_ORIGEN_MEDIO_PAGO_PRESTO)

    end;

    Personeria := StrRight(Trim(cbPersoneria.Items[cbPersoneria.ItemIndex]), 1)[1];
    // Valido datos de la persona, siempre y cuando no se la misma que la del convenio
    if not ((not (cbPersoneria.Enabled)) and (Trim(txtDocumento.Text) <> '')) then begin
        if (FOrigenMedioPago = CONST_ORIGEN_MEDIO_PAGO_PRESTO)
                or (FOrigenMedioPago = CONST_ORIGEN_MEDIO_PAGO_SANTANDER)
                or (FOrigenMedioPago = CONST_ORIGEN_MEDIO_PAGO_BANCOCHILE)
                or (FOrigenMedioPago = CONST_ORIGEN_MEDIO_PAGO_TRANSBANK) then begin

            if  not ( ValidateControls([txtDocumento,
                    cbPersoneria,
                    txtNombre,
                    txtApellido,
                    txt_RazonSocial],
                    [(Trim(txtDocumento.Text) = EmptyStr) or (ValidarRUT(DMConnections.BaseCAC, Trim(txtDocumento.Text)) ),
                    (Trim(txtDocumento.Text) = EmptyStr) or
                        ( (Personeria = PERSONERIA_JURIDICA) or (Personeria = PERSONERIA_FISICA) or (Personeria = PERSONERIA_AMBAS)),
                    (Trim(txtDocumento.Text) = EmptyStr) or
                        ((Personeria = PERSONERIA_JURIDICA) or (Trim(txtNombre.Text) <> EmptyStr)),
                    (Trim(txtDocumento.Text) = EmptyStr) or
                        ((Personeria = PERSONERIA_JURIDICA) or (Trim(txtApellido.Text) <> EmptyStr)),
                    (Trim(txtDocumento.Text) = EmptyStr) or
                        ((Personeria = PERSONERIA_FISICA) or (Trim(txt_RazonSocial.Text) <> EmptyStr))],
                    Format(MSG_CAPTION_GESTION, [STR_MEDIOS_PAGOS]),
                    [MSG_ERROR_DOCUMENTO,
                    Format(MSG_VALIDAR_DEBE_EL, [FLD_PERSONERIA]),
                    Format(MSG_VALIDAR_DEBE_EL, [FLD_NOMBRE]),
                    Format(MSG_VALIDAR_DEBE_EL, [FLD_APELLIDO]),
                    Format(MSG_VALIDAR_DEBE_EL, [FLD_RAZON_SOCIAL])]) ) then Exit;
        end else begin
            (* Controlar la obligatoriedad de los campos. *)
            if  not ( ValidateControls([txtDocumento,
                    txtDocumento,
                    txtNombre,
                    txtApellido,
                    txt_RazonSocial],
                    [Trim(txtDocumento.text) <> EmptyStr,
                    ValidarRUT(DMConnections.BaseCAC, Trim(txtDocumento.Text)),
                    ((Personeria = PERSONERIA_JURIDICA) or (Trim(txtNombre.Text) <> EmptyStr)),
                    ((Personeria = PERSONERIA_JURIDICA) or (Trim(txtApellido.Text) <> EmptyStr)),
                    ((Personeria = PERSONERIA_FISICA) or (Trim(txt_RazonSocial.Text) <> EmptyStr))],
                    Format(MSG_CAPTION_GESTION, [STR_MEDIOS_PAGOS]),
                    [Format(MSG_VALIDAR_DEBE_EL, [STR_RUT]),
                    MSG_ERROR_DOCUMENTO,
                    Format(MSG_VALIDAR_DEBE_EL, [FLD_NOMBRE]),
                    Format(MSG_VALIDAR_DEBE_EL, [FLD_APELLIDO]),
                    Format(MSG_VALIDAR_DEBE_EL, [FLD_RAZON_SOCIAL])]) ) then Exit;

            if (FrameTelefono.NumeroTelefono <> EmptyStr) and not FrameTelefono.TelefonoValido then Exit;

            if not FrameTelefono.ValidarTelefono then Exit;
        end; // if FOrigenMedioPago = CONST_ORIGEN_MEDIO_PAGO_PRESTO
    end; // if (cbPersoneria.Enabled)


    // Valido que la persona no tenga cargado este medio de pago en alg�n otro convenio
    // esto es para que no tenga mas de un convenio con el mismo medio de pago
    if NoPermitirMedioPagoDuplicado(DMConnections.BaseCAC, FRegistroPersonaConvenioActual.NumeroDocumento) then begin
        if FRegistro.TipoMedioPago = PAT then begin
            AuxCodigoConvenio := ObtenerConvenioMedioPago(DMConnections.BaseCAC,
                                    FRegistroPersonaConvenioActual.NumeroDocumento,
                                    FCodigoCovenio,
                                    CONST_CANAL_PAGO_PAT,
                                    //DarCodigoTarjetaCredito,                                                                     //SS_628_ALA_20110818
                                    //Trim(txt_NroTarjetaCredito.Text),                                                            //SS_628_ALA_20110818
                                    //EmptyStr,                                                                                    //SS_628_ALA_20110818
                                    //-1,                                                                                          //SS_628_ALA_20110818
                                    //-1,                                                                                          //SS_628_ALA_20110818
                                    1);                                                                                            //SS_628_ALA_20110720
        end else begin
            AuxCodigoConvenio := ObtenerConvenioMedioPago(DMConnections.BaseCAC,
                                    FRegistroPersonaConvenioActual.NumeroDocumento,
                                    FCodigoCovenio,
                                    CONST_CANAL_PAGO_PAC,
                                    //-1,                                                                                          //SS_628_ALA_20110818
                                    //EmptyStr,                                                                                    //SS_628_ALA_20110818
                                    //Trim(txt_NumeroCuenta.Text),                                                                 //SS_628_ALA_20110818
                                    //DarBanco,                                                                                    //SS_628_ALA_20110818
                                    //DarTipoCuenta,                                                                               //SS_628_ALA_20110818
                                    1);                                                                                           //SS_628_ALA_20110720
        end;
        if AuxCodigoConvenio > -1 then begin
            MsgBox(MSG_ERROR_MEDIO_PAGO_DUPLICADO, MSG_CAPTION_ERROR_MEDIO_PAGO , MB_ICONSTOP);
            Exit;
        end;
    end;

    // Grabo el registro
    if FRegistro.TipoMedioPago = PAT then begin
        if Pos('X', txt_NroTarjetaCredito.Text) = 0 then begin                      // SS_1325_CQU_20150625
            if Trim(FRegistro.PAT.NroTarjeta) = Trim(txt_NroTarjetaCredito.Text)    // SS_1325_CQU_20150625
            then AuxTarjetaCredito := FRegistro.PAT.NroTarjeta                      // SS_1325_CQU_20150625
            else AuxTarjetaCredito := Trim(txt_NroTarjetaCredito.Text);             // SS_1325_CQU_20150625
        end else begin                                                              // SS_1325_CQU_20150625
            // No Modific� la tarjeta por lo tanto vienen X                         // SS_1325_CQU_20150625
            AuxTarjetaCredito := FRegistro.PAT.NroTarjeta;                          // SS_1325_CQU_20150625
        end;                                                                        // SS_1325_CQU_20150625
        FRegistro.PAT.TipoTarjeta           := DarCodigoTarjetaCredito;
        //FRegistro.PAT.NroTarjeta            := trim(txt_NroTarjetaCredito.Text);  // SS_1325_CQU_20150625
        FRegistro.PAT.NroTarjeta            := trim(AuxTarjetaCredito);             // SS_1325_CQU_20150625
        FRegistro.PAT.FechaVencimiento      := txt_FechaVencimiento.Text;
        FRegistro.PAT.FechaVencimiento[3]   := '/';
        FRegistro.PAT.EmisorTarjetaCredito  := DarCodigoEmisorTarjetaCredito;
        {if ( ((FRegistro.PAT.TipoTarjeta = 8) or (FRegistro.PAT.TipoTarjeta = 9)) and
             (FRegistro.PAT.EmisorTarjetaCredito = 47) ) then
            FRegistro.PAT.OrigenMandato := '00'
        else
            FRegistro.PAT.OrigenMandato := '';}

        FRegistro.PAC.TipoCuenta            := -1;
        FRegistro.PAC.CodigoBanco           := -1;
        FRegistro.PAC.NroCuentaBancaria     := EmptyStr;
        FRegistro.PAC.Sucursal              := EmptyStr;
    end else begin
        FRegistro.PAT.TipoTarjeta           := -1;
        FRegistro.PAT.NroTarjeta            := EmptyStr;
        FRegistro.PAT.FechaVencimiento      := EmptyStr;
        FRegistro.PAT.EmisorTarjetaCredito  := -1;

        if Pos('X', txt_NumeroCuenta.Text) = 0 then begin                           // SS_1325_CQU_20150625
            if Trim(FRegistro.PAC.NroCuentaBancaria) = Trim(txt_NumeroCuenta.Text)  // SS_1325_CQU_20150625
            then AuxTarjetaCredito := FRegistro.PAC.NroCuentaBancaria               // SS_1325_CQU_20150625
            else AuxTarjetaCredito := Trim(txt_NumeroCuenta.Text);                  // SS_1325_CQU_20150625
        end else begin                                                              // SS_1325_CQU_20150625
            // No Modific� la tarjeta por lo tanto vienen X                         // SS_1325_CQU_20150625
            AuxTarjetaCredito := FRegistro.PAC.NroCuentaBancaria;                   // SS_1325_CQU_20150625
        end;                                                                        // SS_1325_CQU_20150625

        FRegistro.PAC.TipoCuenta            := DarTipoCuenta;
        FRegistro.PAC.CodigoBanco           := DarBanco;
        //FRegistro.PAC.NroCuentaBancaria     := Trim(txt_NumeroCuenta.Text);       // SS_1325_CQU_20150625
        FRegistro.PAC.NroCuentaBancaria     := Trim(AuxTarjetaCredito);             // SS_1325_CQU_20150625
        FRegistro.PAC.Sucursal              := Trim(txt_Sucursal.Text);

    end;

    FRegistroPersonaConvenioFinal := FRegistroPersonaConvenio;

    FRegistroPersonaConvenioFinal.Personeria      := Personeria;
    FRegistroPersonaConvenioFinal.RazonSocial     := txt_RazonSocial.Text;
    FRegistroPersonaConvenioFinal.Nombre          := txtNombre.Text;
    FRegistroPersonaConvenioFinal.Apellido        := txtApellido.Text;
    FRegistroPersonaConvenioFinal.ApellidoMaterno := txtApellidoMaterno.Text;
    FRegistroPersonaConvenioFinal.TipoDocumento   := TIPO_DOCUMENTO_RUT;
    FRegistroPersonaConvenioFinal.NumeroDocumento := txtDocumento.Text;

    //Por si se termina grabando como solicitud
    FRegistro.RUT           := txtDocumento.Text;
    FRegistro.Nombre        := iif(Personeria = PERSONERIA_JURIDICA, Trim(txt_RazonSocial.Text), Trim(txtApellido.Text + ' ' + txtApellidoMaterno.Text + ' ' + txtNombre.Text));
    FRegistro.Telefono      := trim(FrameTelefono.NumeroTelefono);
    FRegistro.CodigoArea    := FrameTelefono.CodigoArea;
    AuxTarjetaCredito       := '';  // SS_1325_CQU_20150625
    ModalResult := mrOk;
end;


function TFormMediosPagosConvenio.DarCodigoTarjetaCredito: Integer;
begin
    if (Trim(cb_TipoTarjeta.Text) = EmptyStr) or (Trim(cb_TipoTarjeta.Text) = SELECCIONAR) then Result := FTipoTarjetaOriginal
    else Result := StrToInt(Trim(StrRight(cb_TipoTarjeta.Text, 30)));
end;


procedure TFormMediosPagosConvenio.cb_BancoClick(Sender: TObject);
begin
    if self.Visible then txt_NumeroCuenta.SetFocus;
end;

function TFormMediosPagosConvenio.DarBanco: Integer;
begin
    if (Trim(cb_Banco.Text) = EmptyStr) or (Trim(cb_Banco.Text) = SELECCIONAR) then Result := FBancoOriginal
    else Result := StrToInt(Trim(StrRight(cb_Banco.Text, 30)));

end;

class procedure TFormMediosPagosConvenio.LimpiarMedioPago(
  var Registro: TDatosMediosPago);
begin
    with Registro do begin
        TipoMedioPago           := Peatypes.PAC;
        PAC.TipoCuenta          := -1;
        PAC.CodigoBanco         := -1;
        PAC.NroCuentaBancaria   := EmptyStr;
        PAC.Sucursal            := EmptyStr;
        PAT.TipoTarjeta         := -1;
        PAT.NroTarjeta          := EmptyStr;
        PAT.FechaVencimiento    := EmptyStr;
        Telefono                := EmptyStr;
    end;
end;

class function TFormMediosPagosConvenio.ValidarMedioPago(Registro: TDatosMediosPago;
    OrigenMedioPago: AnsiString; NumeroDocumento: String;
    var Mensaje: AnsiString; ConvenioPuro: Boolean = False): Boolean;
var
    ErrorNumeroTarjetaCredito: AnsiString;
    EsTarjetaValida: Boolean;
begin
    OrigenMedioPago := UpperCase(OrigenMedioPago);
    
    ErrorNumeroTarjetaCredito := EmptyStr;
    Mensaje := EmptyStr;
    //Valida que los datos de un registro sean validos, y si
    //son incorrectos, devolver falso con el Mensaje en la variable recibida

    if Registro.TipoMedioPago = Peatypes.PAT then begin //Credito
        with Registro.PAT do begin
            if (OrigenMedioPago = CONST_ORIGEN_MEDIO_PAGO_PRESTO)
                    or (OrigenMedioPago = CONST_ORIGEN_MEDIO_PAGO_SANTANDER)
                    or (OrigenMedioPago = CONST_ORIGEN_MEDIO_PAGO_TRANSBANK) then begin
                (* Controlar la obligatoriedad y la validez del Nro de Tarjeta. *)

                if (TipoTarjeta > 1) and (TipoTarjeta <> 7) then begin
                    EsTarjetaValida := ValidarTarjetaCredito(DMConnections.BaseCAC,
                                        TipoTarjeta,
                                        NroTarjeta,
                                        ErrorNumeroTarjetaCredito);
                    if (Trim(NroTarjeta) = EmptyStr) or (not EsTarjetaValida) then begin
                        Mensaje := iif(ErrorNumeroTarjetaCredito = EmptyStr, MSG_ERROR_FALTA_NUMERO_TARJETA, ErrorNumeroTarjetaCredito)
                    end;
                end;

            end else begin
                if (TipoTarjeta < 0) then
                    Mensaje := Format(MSG_VALIDAR_DEBE_EL,[FLD_TIPO_TARJETA])
                else
                    (* ver cuando se deshabilite la tarjeta. *)
                    if (Trim(NroTarjeta) = EmptyStr) or
                        not (ValidarTarjetaCredito(DMConnections.BaseCAC, TipoTarjeta,
                            NroTarjeta, ErrorNumeroTarjetaCredito)) then
                        Mensaje := iif(ErrorNumeroTarjetaCredito = EmptyStr, MSG_ERROR_FALTA_NUMERO_TARJETA, ErrorNumeroTarjetaCredito)
                    else
                        if (Trim(FechaVencimiento) = '/') or
                                (StrToInt(strleft(FechaVencimiento, 2)) <= 0) or
                                (StrToInt(strleft(FechaVencimiento, 2)) > 12) or
                                (StrToInt(StrRight(FechaVencimiento, 2)+
                                StrLeft(FechaVencimiento, 2)) <= StrToInt(FormatDateTime('yymm', NowBase(DMConnections.BaseCAC)))) then begin
                            Mensaje := MSG_ERROR_FECHA_VENCIMIENTO;
                        end;
            end; // else if (FOrigenMedioPago = CONST_ORIGEN_MEDIO_PAGO_PRESTO)

        end; // with
    end else begin
        if Registro.TipoMedioPago = Peatypes.PAC then begin //Credito
            with Registro.PAC do begin
                if (OrigenMedioPago = CONST_ORIGEN_MEDIO_PAGO_PRESTO)
                        or (OrigenMedioPago = CONST_ORIGEN_MEDIO_PAGO_SANTANDER)
                        or (OrigenMedioPago = CONST_ORIGEN_MEDIO_PAGO_BANCOCHILE)
                        or (OrigenMedioPago = CONST_ORIGEN_MEDIO_PAGO_TRANSBANK) then begin
                    (* Controlar el n�mero de la cuenta. *)

                    if (Trim(NroCuentaBancaria) = EmptyStr) then begin
                        Mensaje := MSG_ERROR_NUMERO_CUENTA;
                    end;

                end else begin
                    if (TipoCuenta < 0) then
                        Mensaje := MSG_FALTA_CUENTA
                    else
                        if (CodigoBanco < 0) then
                            Mensaje := Format(MSG_VALIDAR_DEBE_EL, [FLD_BANCO])
                        else
                            if (Trim(NroCuentaBancaria) = EmptyStr) then
                                Mensaje := MSG_ERROR_NUMERO_CUENTA;
                end; // else if (FOrigenMedioPago = CONST_ORIGEN_MEDIO_PAGO_PRESTO)
            end; // with
        end else Mensaje := MSG_ERROR_TIPO_MEDIO_PAGO_AUTOMATICO;
    end; // else if Registro.TipoMedioPago = Peatypes.PAT

    if not ConvenioPuro then begin
        if (Mensaje = EmptyStr) then begin
            if (OrigenMedioPago <> CONST_ORIGEN_MEDIO_PAGO_PRESTO)
                    and (OrigenMedioPago <> CONST_ORIGEN_MEDIO_PAGO_SANTANDER)
                    and (OrigenMedioPago <> CONST_ORIGEN_MEDIO_PAGO_TRANSBANK) then begin

                // Si llego hasta aca significa que los datos particulares estan Ok
                // ahora valido los datos que son compartidos. Nombre, RUT
                if Trim(NumeroDocumento) <> Trim(Registro.RUT) then begin
                    if Trim(Registro.Nombre) = EmptyStr then begin
                        Mensaje := Format(MSG_VALIDAR_DEBE_EL, [FLD_NOMBRE])
                    end else begin
                        if Trim(Registro.RUT) = EmptyStr then Mensaje := Format(MSG_VALIDAR_DEBE_EL, [STR_RUT]);
                    end;
                end;
            end; // if (OrigenMedioPago <> CONST_ORIGEN_MEDIO_PAGO_PRESTO)
        end;
    end;
    Result := (Mensaje = EmptyStr);
end;
{-----------------------------------------------------------------------------
  Function Name: ArmarMedioPago
  Author:
  Date Created:  /  /
  Description:
  Parameters: Registro: TDatosMediosPago;
              Confirmado: Boolean; CodigoConvenio: Integer; var MedioPago: AnsiString
  Return Value: boolean

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
class function TFormMediosPagosConvenio.ArmarMedioPago(Registro: TDatosMediosPago;
   Confirmado: Boolean; CodigoConvenio: Integer; var MedioPago: AnsiString): boolean;
resourcestring
		STS_CONFIRMADO      = ' - (Confirmado)';
        STS_NO_CONFIRMADO   = ' - (No Confirmado)';
        STS_RECHAZADO       = ' - (Rechazado: %s)';

    function TieneRechazados(CodigoConvenio: Integer; Var MotivoRechazo: String): Boolean;
    begin
        MotivoRechazo := Trim(QueryGetValue(DMConnections.BaseCAC,
            'EXEC ObtenerMotivoRechazoImportacionMandato ' + IntToStr(CodigoConvenio)));
        Result := (MotivoRechazo <> EmptyStr);
    end;

var
    DescripcionTarjeta, DescripcionCuenta, DescripcionBanco, MotivoRechazo, NroTarjetaCuentaFormateadoX : string;
begin
    //BEGIN:SS_1068_NDR_20120910--------------------------------------------------------------------------------------------------
    Result := False;
        MedioPago := EmptyStr;
    try
        if Registro.TipoMedioPago = Peatypes.PAT then MedioPago := EmptyStr; //ninguno

        if Registro.TipoMedioPago = Peatypes.PAT then begin //Credito
            if Registro.PAT.TipoTarjeta > 0 then begin
                if Registro.PAT.TipoTarjeta = TARJETA_TRANSBANK then begin
                    DescripcionTarjeta := EmptyStr;
                end else begin
                    DescripcionTarjeta := Trim(QueryGetValue(DMConnections.BaseCAC,
                        'SELECT Descripcion FROM TarjetasCreditoTipos  WITH (NOLOCK) WHERE CodigoTipoTarjetaCredito = ' + IntToStr(Registro.PAT.TipoTarjeta)));
                    DescripcionTarjeta := DescripcionTarjeta + Space(1);
                end; // else if Registro.PAT.TipoTarjeta = TARJETA_TRANSBANK

                NroTarjetaCuentaFormateadoX := Trim(QueryGetValue(DMConnections.BaseCAC,
                  'SELECT dbo.FormatNumeroTarjeta(' + QuotedStr(Registro.PAT.NroTarjeta) + ')' ));
                MedioPago := UpperCase(DescripcionTarjeta + NroTarjetaCuentaFormateadoX);
                if Confirmado then MedioPago := MedioPago + STS_CONFIRMADO
                else begin
                    if (Registro.PAT.TipoTarjeta = TARJETA_PRESTO) and
                        TieneRechazados(CodigoConvenio, MotivoRechazo) then
                            MedioPago := MedioPago + Format(STS_RECHAZADO, [MotivoRechazo])
                    else MedioPago := MedioPago + STS_NO_CONFIRMADO;
                end;
            end;
        end;

        if Registro.TipoMedioPago = Peatypes.PAC then begin //Cuenta
            if Registro.PAC.TipoCuenta > 0  then begin
                DescripcionCuenta := Trim(QueryGetValue(DMConnections.BaseCAC,
                    'SELECT Descripcion FROM TiposCuentasBancarias  WITH (NOLOCK) WHERE CodigoTipoCuentaBancaria = ' + IntToStr(Registro.PAC.TipoCuenta)));
                DescripcionBanco := Trim(QueryGetValue(DMConnections.BaseCAC,
                    'SELECT Descripcion FROM Bancos  WITH (NOLOCK) WHERE CodigoBanco = ' + IntToStr(Registro.PAC.CodigoBanco)));

                NroTarjetaCuentaFormateadoX := Trim(QueryGetValue(DMConnections.BaseCAC,                                           
                  'SELECT dbo.FormatNumeroTarjeta(' + QuotedStr( ReplaceText(Registro.PAC.NroCuentaBancaria,'-','')) + ')' ));     

                //MedioPago := DescripcionBanco + ' ' + DescripcionCuenta + ' ' + Registro.PAC.NroCuentaBancaria;                  
                MedioPago := DescripcionBanco + ' ' + DescripcionCuenta + ' ' + NroTarjetaCuentaFormateadoX;                       

                if Confirmado then MedioPago := MedioPago + STS_CONFIRMADO
                else MedioPago := MedioPago + STS_NO_CONFIRMADO;
            end;
        end;
        Result := True;
    except
        on e: Exception do begin
            ShowMsgBoxCN(e, Application.MainForm);
    end;
    end;
    //END:SS_1068_NDR_20120910--------------------------------------------------------------------------------------------------
end;

procedure TFormMediosPagosConvenio.btnDatosConvenioClick(Sender: TObject);
begin
    CargarPersoneria(cbPersoneria, FRegistroPersonaConvenioActual.Personeria, False);
    cbPersoneriaChange(Sender);
    txtDocumento.Text       := FRegistroPersonaConvenioActual.NumeroDocumento;
    txtNombre.Text          := FRegistroPersonaConvenioActual.Nombre;
    txtApellido.Text        := FRegistroPersonaConvenioActual.Apellido;
    txtApellidoMaterno.Text := FRegistroPersonaConvenioActual.ApellidoMaterno;
    txt_RazonSocial.Text    := FRegistroPersonaConvenioActual.RazonSocial;
    FrameTelefono.CargarTelefono(FCodigoAreaConvenio, FTelefonoConvenio, 0);
    Limpiar(FRegistroPersonaConvenio);
    FRegistroPersonaConvenio := FRegistroPersonaConvenioActual;
    EnableControlsInContainer(Pc_Datos, False);
    EnableControlsInContainer(FrameTelefono, False);
    cbPersoneria.Enabled    := False;
    FIndiceTelLevantado     := FIndiceTelefono;
 end;

procedure TFormMediosPagosConvenio.FreDatoPresonatxtDocumentoChange(
  Sender: TObject);
begin
    if (Trim(txtDocumento.Text) = EmptyStr) then begin
        FRegistroPersonaConvenio.CodigoPersona := -1;
    end;

    VerificarCliente;
end;

procedure TFormMediosPagosConvenio.VerificarCliente;
begin
    //Verifico si ya es una persona

    if (Length(txtDocumento.Text) < 8) then begin
        (* La persona est� en la BD? *)
        if FRegistroPersonaConvenio.CodigoPersona <> -1 then begin
            EnableControlsInContainer(Pc_Datos, True);
            EnableControlsInContainer(FrameTelefono, True);
//            lblPersoneria.Enabled := True;
            cbPersoneria.Enabled := True;
            Limpiar(FRegistroPersonaConvenio);
            LimpiarControles;
        end else begin
            HabilitarDatosMandante(not (Trim(txtDocumento.Text) = EmptyStr));
            LimpiarControles;
        end;

        FRegistroPersonaConvenio.CodigoPersona := -1;

        Exit;
    end;

    //Es el mismo RUT que en el convenio llamo al click que toma los datos del convenio
    if (Trim(txtDocumento.Text) = FRegistroPersonaConvenioActual.NumeroDocumento) then begin
        btnDatosConvenio.Click;
        Exit;
    end;

    //Busco en Personas si Existe
    ObtenerDatosPersona.Close;
    ObtenerDatosPersona.Parameters.Refresh;
    ObtenerDatosPersona.Parameters.ParamByName('@CodigoPersona').Value:=Null;
    ObtenerDatosPersona.Parameters.ParamByName('@CodigoDocumento').Value := TIPO_DOCUMENTO_RUT;
    ObtenerDatosPersona.Parameters.ParamByName('@NumeroDocumento').Value := Trim(txtDocumento.Text);
    ObtenerDatosPersona.Open;

    if ObtenerDatosPersona.RecordCount > 0 then begin
       if Trim(ObtenerDatosPersona.FieldByName('Personeria').AsString) = PERSONERIA_FISICA then begin
            txtNombre.Text                          := Trim(ObtenerDatosPersona.FieldByName('Nombre').AsString);
            txtApellido.Text                        := Trim(ObtenerDatosPersona.FieldByName('Apellido').AsString);
            txtApellidoMaterno.Text                 := Trim(ObtenerDatosPersona.FieldByName('ApellidoMaterno').AsString);
            FRegistroPersonaConvenio.Nombre         := Trim(ObtenerDatosPersona.FieldByName('Nombre').AsString);
            FRegistroPersonaConvenio.Apellido       := Trim(ObtenerDatosPersona.FieldByName('Apellido').AsString);
            FRegistroPersonaConvenio.ApellidoMaterno := Trim(ObtenerDatosPersona.FieldByName('ApellidoMaterno').AsString);
            if ObtenerDatosPersona.FieldByName('FechaNacimiento').IsNull then
            	FRegistroPersonaConvenio.FechaNacimiento := NullDate
            else
	            FRegistroPersonaConvenio.FechaNacimiento := ObtenerDatosPersona.FieldByName('FechaNacimiento').AsDateTime;

            FRegistroPersonaConvenio.Sexo               := (Trim(ObtenerDatosPersona.FieldByName('Sexo').AsString) + SEXO_MASCULINO)[1];
            FRegistroPersonaConvenio.LugarNacimiento    := Trim(ObtenerDatosPersona.FieldByName('LugarNacimiento').AsString);
            FRegistroPersonaConvenio.Password           := Trim(ObtenerDatosPersona.FieldByName('Password').AsString);
            FRegistroPersonaConvenio.Pregunta           := Trim(ObtenerDatosPersona.FieldByName('Pregunta').AsString);
            FRegistroPersonaConvenio.Respuesta          := Trim(ObtenerDatosPersona.FieldByName('Respuesta').AsString);
            FRegistroPersonaConvenio.TipoDocumento      := Trim(ObtenerDatosPersona.FieldByName('CodigoDocumento').AsString);
       end
       else begin
            txt_RazonSocial.Text                        := Trim(ObtenerDatosPersona.FieldByName('Apellido').AsString);
            FRegistroPersonaConvenio.Apellido           := Trim(ObtenerDatosPersona.FieldByName('ApellidoContactoComercial').AsString);
            FRegistroPersonaConvenio.ApellidoMaterno    := Trim(ObtenerDatosPersona.FieldByName('ApellidoMaternoContactoComercial').AsString);
            FRegistroPersonaConvenio.Nombre             := Trim(ObtenerDatosPersona.FieldByName('NombreContactoComercial').AsString);
            FRegistroPersonaConvenio.Sexo               := (Trim(ObtenerDatosPersona.FieldByName('SexoContactoComercial').AsString)+SEXO_MASCULINO)[1];
            FRegistroPersonaConvenio.FechaNacimiento    := NullDate
       end;
       HabilitarDatosMandante(not NumeroDocumentoTieneConvenio(txtDocumento.Text));
       FRegistroPersonaConvenio.Personeria      := Trim(ObtenerDatosPersona.FieldByName('Personeria').AsString)[1];
       FRegistroPersonaConvenio.CodigoActividad := ObtenerDatosPersona.FieldByName('CodigoActividad').AsInteger;
       FRegistroPersonaConvenio.CodigoPersona   := ObtenerDatosPersona.FieldByName('CodigoPersona').AsInteger;

       CargarPersoneria(cbPersoneria, FRegistroPersonaConvenio.Personeria);
       cbPersoneriaChange(Self);

       ObtenerMedioComunicacionPricipalPersona.Close;
       ObtenerMedioComunicacionPricipalPersona.Parameters.Refresh;
       ObtenerMedioComunicacionPricipalPersona.Parameters.ParamByName('@CodigoPersona').Value := FRegistroPersonaConvenio.CodigoPersona;
       ObtenerMedioComunicacionPricipalPersona.Open;

       if ObtenerMedioComunicacionPricipalPersona.RecordCount > 0 then begin
            FrameTelefono.CargarTelefono(iif(ObtenerMedioComunicacionPricipalPersona.FieldByName('CodigoArea').IsNull, -1, ObtenerMedioComunicacionPricipalPersona.FieldByName('CodigoArea').AsInteger),
                                    Trim(ObtenerMedioComunicacionPricipalPersona.FieldByName('Valor').AsString), -1);
            FIndiceTelLevantado := ObtenerMedioComunicacionPricipalPersona.FieldByName('CodigoMedioComunicacion').AsInteger;
       end else begin
            FrameTelefono.Limpiar;
            FIndiceTelLevantado := -1;
       end;

       if not FConvenioPuro then begin
           EnableControlsInContainer(Pc_Datos, False);
           EnableControlsInContainer(FrameTelefono, False);
       end;

       cbPersoneria.Enabled := False;
    end else begin
         FIndiceTelLevantado := -1;
         FrameTelefono.Limpiar;
         Limpiar(FRegistroPersonaConvenio);
         LimpiarControles;
         if FRegistroPersonaConvenio.CodigoPersona <> -1 then begin // son los datos del titular del convenio
             FRegistroPersonaConvenio.CodigoPersona := -1;
         end else begin

            if (not cbPersoneria.Enabled) and (Trim(txtDocumento.Text) <> FRegistroPersonaConvenioActual.NumeroDocumento) then begin
                EnableControlsInContainer(Pc_Datos, True);
                EnableControlsInContainer(FrameTelefono, True);
                cbPersoneria.Enabled := True;
            end;
         end;
    end;

end;

procedure TFormMediosPagosConvenio.cbPersoneriaChange(Sender: TObject);
begin
    if StrRight(' '+trim(cbPersoneria.items[cbPersoneria.itemindex]),1)=PERSONERIA_FISICA then Pc_Datos.ActivePage:=Tab_Fisica;
    if StrRight(' '+trim(cbPersoneria.items[cbPersoneria.itemindex]),1)=PERSONERIA_JURIDICA then Pc_Datos.ActivePage:=Tab_Juridica;
end;

procedure TFormMediosPagosConvenio.Pc_DatosEnter(Sender: TObject);
begin
    if cbPersoneria.Enabled then begin
        if Pc_Datos.ActivePage=Tab_Fisica then begin
            if not txtApellido.Focused and not txtApellidoMaterno.Focused then txtNombre.SetFocus;
        end
        else
            txt_RazonSocial.SetFocus;
    end;
end;

procedure TFormMediosPagosConvenio.txtDocumentoChange(Sender: TObject);
begin
    if (Trim(txtDocumento.Text) = EmptyStr) then FRegistroPersonaConvenio.CodigoPersona := -1;

    VerificarCliente;
end;

procedure TFormMediosPagosConvenio.LimpiarControles;
begin
    txtNombre.Clear;
    txtApellido.Clear;
    txtApellidoMaterno.Clear;
    txt_RazonSocial.Clear;
    FrameTelefono.Limpiar;
end;

procedure TFormMediosPagosConvenio.txtDocumentoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    if ( ((Key = VK_DELETE) or (Key = VK_CLEAR) or (Key = VK_BACK))
            and (FRegistroPersonaConvenio.CodigoPersona > 0) ) then begin
        LimpiarControles;
    end;

end;

class procedure TFormMediosPagosConvenio.LimpiarMandante(
  var Registro: TDatosPersonales);
begin
    with Registro do begin
        CodigoPersona   := -1;
        Nombre          := EmptyStr;
        Apellido        := EmptyStr;
        ApellidoMaterno := EmptyStr;
        RazonSocial     := EmptyStr;
        TipoDocumento   := TIPO_DOCUMENTO_RUT;
        NumeroDocumento := EmptyStr;
        Personeria      := PERSONERIA_FISICA;
        FechaNacimiento := NullDate;
    end;
end;

class function TFormMediosPagosConvenio.ValidarMandante(OrigenMedioPago: AnsiString;
  Registro: TDatosPersonales; NumeroDocumentoConvenio: String; var Mensaje: AnsiString): Boolean;
begin
    OrigenMedioPago := UpperCase(OrigenMedioPago);
    
    Mensaje := EmptyStr;
    if (OrigenMedioPago <> CONST_ORIGEN_MEDIO_PAGO_PRESTO)
            and (OrigenMedioPago <> CONST_ORIGEN_MEDIO_PAGO_SANTANDER)
            and (OrigenMedioPago <> CONST_ORIGEN_MEDIO_PAGO_BANCOCHILE)
            and (OrigenMedioPago <> CONST_ORIGEN_MEDIO_PAGO_TRANSBANK) then begin
        //Valida que los datos del mandante sean corerctos
        //son incorrectos, devolver falso con el Mensaje en la variable recibida

        if Trim(NumeroDocumentoConvenio) <> Trim(Registro.NumeroDocumento) then begin

            if Registro.Personeria = PERSONERIA_FISICA then begin
                if Trim(Registro.Nombre) = EmptyStr then Mensaje := Format(MSG_VALIDAR_DEBE_EL, [FLD_NOMBRE])
                else if Trim(Registro.Apellido) = EmptyStr then Mensaje := Format(MSG_VALIDAR_DEBE_EL, [FLD_APELLIDO]);
            end
            else begin
                if Registro.Personeria = PERSONERIA_JURIDICA then begin
                    if Trim(Registro.RazonSocial) = EmptyStr then Mensaje := Format(MSG_VALIDAR_DEBE_LA, [FLD_RAZON_SOCIAL]);
                end else
                    Mensaje := Format(MSG_VALIDAR_DEBE_LA, [FLD_PERSONERIA]);
            end;
        end;
    end;
    Result := (Mensaje = EmptyStr);
end;

procedure TFormMediosPagosConvenio.Limpiar(var Persona: TDatosPersonales);
begin
    Persona.CodigoPersona := -1;
    Persona.RazonSocial := '';
    Persona.Apellido := '';
    Persona.ApellidoMaterno := '';
    Persona.Nombre := '';
    Persona.Sexo := #0;
    Persona.LugarNacimiento := '';
    Persona.FechaNacimiento := NullDate;
    Persona.CodigoActividad := -1;
    Persona.Password := '';
    Persona.Pregunta := '';
    Persona.Respuesta := '';
    Persona.Pregunta := '';
end;


function TFormMediosPagosConvenio.DarCodigoEmisorTarjetaCredito: Integer;
begin
    if (Trim(cb_EmisoresTarjetasCreditos.Text) = EmptyStr) or
        (Trim(cb_EmisoresTarjetasCreditos.Text) = SELECCIONAR) or
        (cb_EmisoresTarjetasCreditos.ItemIndex < 1) then Result := FEmisorTarjetaOriginal
    else Result := StrToInt(Trim(StrRight(cb_EmisoresTarjetasCreditos.Text, 30)));

end;

procedure TFormMediosPagosConvenio.cb_EmisoresTarjetasCreditosClick(
  Sender: TObject);
begin
    if self.Visible then begin
        if txt_NroTarjetaCredito.Enabled then txt_NroTarjetaCredito.SetFocus
        else txtDocumento.SetFocus;
    end;
end;

function TFormMediosPagosConvenio.GetTelefonoHabilitado: Boolean;
begin
	result:=FrameTelefono.txtTelefono.Enabled;
end;

function TFormMediosPagosConvenio.GetTTipoMedioComunicacion: TTipoMedioComunicacion;
begin
    Result.CodigoTipoMedioContacto:=FrameTelefono.CodigoTipoTelefono;
    Result.CodigoArea:=FrameTelefono.CodigoArea;
    Result.Valor:=FrameTelefono.NumeroTelefono;
    Result.Anexo:=FrameTelefono.Anexo;
    Result.HoraDesde:=FrameTelefono.HorarioDesde;
    Result.HoraHasta:=FrameTelefono.HorarioHasta;
    Result.Principal := True;
    result.Indice := FIndiceTelLevantado;
end;

//Revision 5
procedure TFormMediosPagosConvenio.txt_NroTarjetaCreditoKeyPress(
  Sender: TObject; var Key: Char);
begin
	//INICIO:	20160718 CFU
    //if Key  in ['''', '}'] then Key := #0;
    //S�lo se permite el ingreso de n�meros
    if not (Key  in ['0'..'9',#8]) then
		Key := #0;
    //FIN:		20160718 CFU
end;
//Fin de Revision 5

procedure TFormMediosPagosConvenio.chk_ConfirmarTipoMedioPagoClick(
  Sender: TObject);
begin
    if not chk_ConfirmarTipoMedioPago.Enabled then Exit;

    FForzarConfirmado := chk_ConfirmarTipoMedioPago.Checked;
    if FForzarConfirmado then
        lbl_EstadoTipoMedioPago.Caption := CONFIRMADO
    else lbl_EstadoTipoMedioPago.Caption := NO_CONFIRMADO;
end;

{-----------------------------------------------------------------------------
  Procedure: TFormMediosPagosConvenio.ColorearCamposObligatorios
  Author:    ggomez
  Date:      24-Jun-2005
  Arguments: None
  Result:    None
  Description: Colorea los campos obligatorios seg�n el Origen del Mandato del
    Tipo de Medio de Pago.
-----------------------------------------------------------------------------}
procedure TFormMediosPagosConvenio.ColorearCamposObligatorios;
const
    ColorEditsObligatorios          = $00FAEBDE;
    ColorFuenteLabelsObligatorios   = clNavy;
    ColorEditsNoObligatorios        = clWindow;
    ColorFuenteLabelsNoObligatorios = clBlack;
begin
    if ( (FRegistro.TipoMedioPago = PAT) or (FRegistro.TipoMedioPago = PAC) )
            and ( (FOrigenMedioPago = CONST_ORIGEN_MEDIO_PAGO_PRESTO)
                or (FOrigenMedioPago = CONST_ORIGEN_MEDIO_PAGO_SANTANDER)
                or (FOrigenMedioPago = CONST_ORIGEN_MEDIO_PAGO_BANCOCHILE)
                or (FOrigenMedioPago = CONST_ORIGEN_MEDIO_PAGO_TRANSBANK) ) then begin

        if FRegistro.TipoMedioPago = PAT then begin
            (* Dejar como obligatorio s�lo a Nro de Tarjeta. *)
            lbl_NumeroCuenta.Font.Color         := ColorFuenteLabelsObligatorios;
            lbl_NumeroCuenta.Font.Style         := lbl_NumeroCuenta.Font.Style + [fsBold];
            txt_NroTarjetaCredito.Color         := ColorEditsObligatorios;

            lbl_TipoTarjeta.Font.Color          := ColorFuenteLabelsNoObligatorios;
            lbl_TipoTarjeta.Font.Style          := lbl_TipoTarjeta.Font.Style - [fsBold];
            cb_TipoTarjeta.Color                := ColorEditsNoObligatorios;

            lbl_EmisorTarjeta.Font.Color        := ColorFuenteLabelsNoObligatorios;
            lbl_EmisorTarjeta.Font.Style        := lbl_EmisorTarjeta.Font.Style - [fsBold];
            cb_EmisoresTarjetasCreditos.Color   := ColorEditsNoObligatorios;

            lbl_FechaVencimiento.Font.Color     := ColorFuenteLabelsNoObligatorios;
            lbl_FechaVencimiento.Font.Style     := lbl_FechaVencimiento.Font.Style - [fsBold];
            txt_FechaVencimiento.Color          := ColorEditsNoObligatorios;
        end else begin
            lbl_NumeroCuenta.Font.Color         := ColorFuenteLabelsObligatorios;
            lbl_NumeroCuenta.Font.Style         := lbl_NumeroCuenta.Font.Style + [fsBold];
            txt_NumeroCuenta.Color              := ColorEditsObligatorios;

            lbl_TipoCuenta.Font.Color           := ColorFuenteLabelsNoObligatorios;
            lbl_TipoCuenta.Font.Style           := lbl_TipoCuenta.Font.Style - [fsBold];
            cb_TipoCuenta.Color                 := ColorEditsNoObligatorios;

            lbl_Banco.Font.Color                := ColorFuenteLabelsNoObligatorios;
            lbl_Banco.Font.Style                := lbl_Banco.Font.Style - [fsBold];
            cb_Banco.Color                      := ColorEditsNoObligatorios;

            lbl_Sucursal.Font.Color             := ColorFuenteLabelsNoObligatorios;
            lbl_Sucursal.Font.Style             := lbl_Sucursal.Font.Style - [fsBold];
            txt_Sucursal.Color                  := ColorEditsNoObligatorios;
        end;

        lblRutRun.Font.Color    := ColorFuenteLabelsNoObligatorios;
        lblRutRun.Font.Style    := lblRutRun.Font.Style - [fsBold];
        txtDocumento.Color      := ColorEditsNoObligatorios;

    end else begin
        if FRegistro.TipoMedioPago = PAT then begin
            (* Colocar como obligatorios a todos los campos. *)
            lbl_NumeroCuenta.Font.Color         := ColorFuenteLabelsObligatorios;
            lbl_NumeroCuenta.Font.Style         := lbl_NumeroCuenta.Font.Style + [fsBold];
            txt_NroTarjetaCredito.Color         := ColorEditsObligatorios;

            lbl_TipoTarjeta.Font.Color          := ColorFuenteLabelsObligatorios;
            lbl_TipoTarjeta.Font.Style          := lbl_TipoTarjeta.Font.Style + [fsBold];
            cb_TipoTarjeta.Color                := ColorEditsObligatorios;

            lbl_EmisorTarjeta.Font.Color        := ColorFuenteLabelsObligatorios;
            lbl_EmisorTarjeta.Font.Style        := lbl_EmisorTarjeta.Font.Style + [fsBold];
            cb_EmisoresTarjetasCreditos.Color   := ColorEditsObligatorios;

            lbl_FechaVencimiento.Font.Color     := ColorFuenteLabelsObligatorios;
            lbl_FechaVencimiento.Font.Style     := lbl_FechaVencimiento.Font.Style + [fsBold];
            txt_FechaVencimiento.Color          := ColorEditsObligatorios;
        end else begin
            lbl_NumeroCuenta.Font.Color         := ColorFuenteLabelsObligatorios;
            lbl_NumeroCuenta.Font.Style         := lbl_NumeroCuenta.Font.Style + [fsBold];
            txt_NumeroCuenta.Color              := ColorEditsObligatorios;

            lbl_TipoCuenta.Font.Color           := ColorFuenteLabelsObligatorios;
            lbl_TipoCuenta.Font.Style           := lbl_TipoCuenta.Font.Style + [fsBold];
            cb_TipoCuenta.Color                 := ColorEditsObligatorios;

            lbl_Banco.Font.Color                := ColorFuenteLabelsObligatorios;
            lbl_Banco.Font.Style                := lbl_Banco.Font.Style + [fsBold];
            cb_Banco.Color                      := ColorEditsObligatorios;

            lbl_Sucursal.Font.Color             := ColorFuenteLabelsObligatorios;
            lbl_Sucursal.Font.Style             := lbl_Sucursal.Font.Style + [fsBold];
            txt_Sucursal.Color                  := ColorEditsObligatorios;

        end;

        lblRutRun.Font.Color    := ColorFuenteLabelsObligatorios;
        lblRutRun.Font.Style    := lblRutRun.Font.Style + [fsBold];
        txtDocumento.Color      := ColorEditsObligatorios;
    end; // else if (FOrigenMedioPago = CONST_ORIGEN_MEDIO_PAGO_PRESTO)
end;

{******************************** Function Header ******************************
Function Name: txt_FechaVencimientoExit
Author :
Date Created :
Description :
Parameters : Sender: TObject
Return Value : None
Revision :1
    Author : vpaszkowicz
    Date : 18/01/2008
    Description : Cambio la condici�n para que si por alg�n motivo me qued� un
    espacio metido, que arroje un mensaje.
*******************************************************************************}
procedure TFormMediosPagosConvenio.txt_FechaVencimientoExit(Sender: TObject);
begin
    if ((Length((trim(txt_FechaVencimiento.Text))) < 5) or
       (Pos(' ', Trim(txt_FechaVencimiento.Text)) <> 0)) then
    begin
        MsgBoxBalloon(MSG_ERROR_FECHA_VENCIMIENTO, FLD_FECHA, MB_ICONSTOP, txt_FechaVencimiento);
        if txt_FechaVencimiento.enabled then txt_FechaVencimiento.SetFocus;
    end;
end;

procedure TFormMediosPagosConvenio.txt_NroTarjetaCreditoChange(
  Sender: TObject);
begin
    VerificarValoresIniciales(txt_NroTarjetaCredito.Text);
    //INICIO:	20160713 CFU
    bYaValidado := False;
    //FIN:		20160713 CFU
end;

//INICIO	: 20160712 CFU
procedure TFormMediosPagosConvenio.txt_NroTarjetaCreditoExit(Sender: TObject);
var
	CodigoTC: Integer;
//INICIO	: 20160715 CFU
	LongCaracterIni: Integer;
    bPrefijoOK: Boolean;
//FIN		: 20160715 CFU
begin
	if (btnSalir.Focused) or (Length(txt_NroTarjetaCredito.Text) = 0) then
    	Exit;

    if (Length(txt_NroTarjetaCredito.Text) > 0) and (Length(txt_NroTarjetaCredito.Text) <> txt_NroTarjetaCredito.MaxLength) then begin
    	MsgBoxErr(MSG_ERROR_VALIDACION_TC, MSG_ERROR_CANTIDAD_DIGITOS, STR_MEDIOS_PAGO_CONVENIO, MB_ICONSTOP);
        txt_NroTarjetaCredito.SetFocus;
        Exit;
    end;

	//Se permite este c�digo de TC es para pruebas KTC
    if (Copy(txt_NroTarjetaCredito.Text, 1, 4) = '9876') then       //20160712 CFU
    	Exit;

    CodigoTC		:= StrToInt(Trim(StrRight(cb_TipoTarjeta.Text, 30)));
    //INICIO	: 20160715 CFU
    LongCaracterIni	:= QueryGetValueInt(DMConnections.BaseCAC, 'SELECT CaracterInicialBanda FROM TarjetasCreditoTipos  WITH (NOLOCK) WHERE CodigoTipoTarjetaCredito = ' + IntToStr(CodigoTC));

    //poner u n ciclo while buscando los prefijos de la tabla TarjetasCreditoPrefijos asociados al c�digo de tarjerta
    bPrefijoOK	:= False;
    with qry_prefijos do begin
        if Active then
        	Close;
        Parameters.ParamByName('CodigoTipoTarjetaCredito').Value := CodigoTC;
        Open;
        if FieldByName('ValidarPrefijo').AsBoolean then begin

            while not Eof do begin
                if (Copy(txt_NroTarjetaCredito.Text, 1, LongCaracterIni) = Trim(FieldByName('Prefijo').AsString)) then begin
                    bPrefijoOK	:= True;
                    Break;
                end;
                Next;
            end;
            Close;

            if (not bYaValidado) and (Length(txt_NroTarjetaCredito.Text) > 0) and (not bPrefijoOK) then begin
            	MsgBoxErr(MSG_ERROR_VALIDACION_TC, MSG_ERROR_NUM_TARJETA_NO_CORRESPONDE_A_TIPO, STR_MEDIOS_PAGO_CONVENIO, MB_ICONSTOP);
                if bYaValidado then
                    txt_NroTarjetaCredito.Text := '';
                cb_TipoTarjeta.SetFocus;
                Exit;
            end;
        end;
    end;
    
{    
	if (not bYaValidado) and (Length(txt_NroTarjetaCredito.Text) > 0) and (not bPrefijoOK) then begin
    	MessageDlg('N�mero de Tarjeta de Cr�dito no corresponde a Tipo de Tarjeta seleccionado', mtError, [mbOk], 0);
        MsgBoxErr(MSG_ERROR_VALIDACION_TC, MSG_ERROR_NUM_TARJETA_NO_CORRESPONDE_A_TIPO, STR_MEDIOS_PAGO_CONVENIO, MB_ICONSTOP);
        if bYaValidado then
        	txt_NroTarjetaCredito.Text := '';
        cb_TipoTarjeta.SetFocus;
        Exit;
    end;}
    //FIN	: 20160715 CFU
    
    if (not bYaValidado) and (Length(txt_NroTarjetaCredito.Text) > 0) and (not LuhnChecksumValid(txt_NroTarjetaCredito.Text)) then begin
    	MsgBoxErr(MSG_ERROR_VALIDACION_TC, MSG_ERROR_NUM_TARJETA_INCORRECTO, STR_MEDIOS_PAGO_CONVENIO, MB_ICONSTOP);
        if bYaValidado then
        	txt_NroTarjetaCredito.Text := '';
        cb_TipoTarjeta.SetFocus;
        Exit;
    end;

	bYaValidado := True;
 
end;
//FIN	: 20160712 CFU

procedure TFormMediosPagosConvenio.txt_NumeroCuentaChange(Sender: TObject);
begin
    VerificarValoresIniciales(txt_NumeroCuenta.Text);
end;

//Revision 5
procedure TFormMediosPagosConvenio.txt_NumeroCuentaKeyPress(Sender: TObject;
  var Key: Char);
begin
    //INICIO JLO 20160927
    //if Key  in [''''] then Key := #0;
    if not (Key  in ['0'..'9',#8]) then
		Key := #0;
    //TERMINO JLO 20160927
end;
//Fin de Revision 5

procedure TFormMediosPagosConvenio.HabilitarDatosMandante(Valor: Boolean);
begin
    cbPersoneria.Enabled := Valor;
    EnableControlsInContainer(Pc_Datos, Valor);
    EnableControlsInContainer(FrameTelefono, Valor);
end;

end.
