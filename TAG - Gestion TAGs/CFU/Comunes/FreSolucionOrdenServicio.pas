{-------------------------------------------------------------------------------
 File Name: FreSolucionOrdenServicio.pas
 Author: DdMarco
 Date Created:
 Language: ES-AR
 Description: Seccion con el estado del reclamo
-------------------------------------------------------------------------------}
unit FreSolucionOrdenServicio;

interface

uses
  //SolucionOrdenServicio
  DMConnection,                   //Coneccion a base de datos OP_CAC
  util,                           //NullDate
  utilproc,                       //MsgboxBalloon
  rstrings,                       //STR_ERROR
  //General
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, VariantComboBox, ExtCtrls, DB, Validate, DateEdit,
  ADODB;

type
  TFrameSolucionOrdenServicio = class(TFrame)
    SpObtenerEntidades: TADOStoredProc;
    GBEstadoDelReclamo: TGroupBox;
    Label1: TLabel;
    cbEstado: TVariantComboBox;
    PAdicional: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    EFechaCompromiso: TDateEdit;
    EEntidad: TVariantComboBox;
    procedure cbEstadoSelect(Sender: TObject);
    procedure EEntidadDropDown(Sender: TObject);
  private
    function  GetEstado: AnsiString;
    procedure SetEstado(const Value: AnsiString);


    function  GetFechaCompromiso: TDateTime;
    procedure SetFechaCompromiso(const Value: TDateTime);
    function  GetEntidad: integer;
    procedure SetEntidad(const Value: integer);
    Function  CargarEntidades:boolean;
    { Private declarations }
  public
    { Public declarations }
    function  Inicializar: Boolean;
    procedure LoadFromDataset(DS: TDataset);
    function  Validar: Boolean;
    //
    property  Estado: AnsiString Read GetEstado write SetEstado;
    property  FechaCompromiso: TDateTime read GetFechaCompromiso write SetFechaCompromiso;
    property  Entidad: integer read GetEntidad write SetEntidad;
  end;

implementation

{$R *.dfm}

{ TFrameSolucionOrdenServicio }


{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:
  Date Created: 19/04/2005
  Description:
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFrameSolucionOrdenServicio.Inicializar: Boolean;
begin
    cbEstado.Value := 'P';
    //Deshabilito
    Padicional.visible:=false;
    Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarEntidades
  Author:    lgisuk
  Date Created: 19/04/2005
  Description: Cargo las Entidades
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFrameSolucionOrdenServicio.CargarEntidades:boolean;
const
    CONST_NINGUNO = 'Ninguno';
begin
    result:=false;
    eentidad.Items.Clear;
    eentidad.Items.Add(CONST_NINGUNO, 0);
    spobtenerentidades.Parameters.Refresh;
    spobtenerentidades.Parameters.ParamByName('@Tipo').value:= Estado;
    try
        spObtenerEntidades.Open;
        while not spObtenerEntidades.Eof do begin
            EEntidad.Items.Add(spObtenerEntidades.fieldbyname('descripcion').asstring,spObtenerEntidades.fieldbyname('CodigoEntidad').asinteger);
            spObtenerEntidades.Next;
        end;
        spObtenerEntidades.Close;
        eentidad.ItemIndex:=0;
        result:=true;
    except
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: LoadFromDataset
  Author:
  Date Created: 19/04/2005
  Description:
  Parameters: DS: TDataset
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrameSolucionOrdenServicio.LoadFromDataset(DS: TDataset);
begin
    //Obtengo el estado
    cbEstado.Value := DS['Estado'];
    //Cargo la informacion adicional
    if ((Estado = 'I') or (Estado = 'E')) then begin
        CargarEntidades;
        efechacompromiso.Date:=DS.FieldByName('FechaCompromisoInterna').Asdatetime;
        eentidad.Value:=DS.FieldByName('CodigoEntidad').Asinteger;
        //Habilito
        Padicional.visible:=true;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: Validar
  Author:
  Date Created: 19/04/2005
  Description:
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFrameSolucionOrdenServicio.Validar: Boolean;
Resourcestring
    STR_FECHA_COMPROMISO = 'Debe indicar Fecha de Compromiso';
begin

    //estado es pendiente de respuesta interna o externa
    if ((Estado = 'I') or (Estado = 'E')) then begin

        //Obligo a que carguen una fecha comprometida
        if EFechaCompromiso.Date = NULLDATE then begin
            result:=false;
            MsgBoxBalloon(STR_FECHA_COMPROMISO , STR_ERROR, MB_ICONSTOP, EFechaCompromiso);
            EFechaCompromiso.SetFocus;
            exit;
        end;

        {//Obligo a que carguen la entidad
        if  EEntidad.Value = 0 then begin
            result:=false;
            ActivatePage;
            MsgBoxBalloon('Debe indicar la Entidad', STR_ERROR, MB_ICONSTOP, EEntidad);
            EEntidad.SetFocus;
            exit;
        end;}

    end;

    Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: cbEstadoChange
  Author:    lgisuk
  Date Created: 19/04/2005
  Description: permito cargar informacion adicional si es pendiente interna
               o externa
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrameSolucionOrdenServicio.cbEstadoSelect(Sender: TObject);
var valor:string;
begin
    valor:=cbestado.Value;
    if ((valor = 'I') or (Valor = 'E')) then begin
        SetEntidad(0);
        //Habilito
        Padicional.visible:=true;
    end else begin
       //Deshabilito
        Padicional.visible:=false;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: EEntidadDropDown
  Author:    lgisuk
  Date Created: 19/04/2005
  Description: Cargo Entidades Externas o Departamentos
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrameSolucionOrdenServicio.EEntidadDropDown(Sender: TObject);
begin
    CargarEntidades;
end;


{-----------------------------------------------------------------------------
  Function Name: SetEstado
  Author:
  Date Created: 19/04/2005
  Description:
  Parameters: const Value: AnsiString
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrameSolucionOrdenServicio.SetEstado(const Value: AnsiString);
begin
    cbEstado.Value := Value;
end;

{-----------------------------------------------------------------------------
  Function Name: Setfechacompromiso
  Author:
  Date Created: 19/04/2005
  Description:
  Parameters: const Value: AnsiString
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrameSolucionOrdenServicio.SetFechaCompromiso(const Value: Tdatetime);
begin
    efechacompromiso.Date := Value;
end;

{-----------------------------------------------------------------------------
  Function Name: SetEntidad
  Author:
  Date Created: 19/04/2005
  Description:
  Parameters: const Value: AnsiString
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrameSolucionOrdenServicio.SetEntidad(const Value: integer);
begin
    eentidad.Value := Value;
end;


{-----------------------------------------------------------------------------
  Function Name: GetEstado
  Author:
  Date Created: 19/04/2005
  Description:
  Parameters: None
  Return Value: AnsiString
-----------------------------------------------------------------------------}
function TFrameSolucionOrdenServicio.GetEstado: AnsiString;
begin
    Result := cbEstado.Value;
end;


{-----------------------------------------------------------------------------
  Function Name: GetFechaCompromiso
  Author:
  Date Created: 19/04/2005
  Description:
  Parameters: None
  Return Value: AnsiString
-----------------------------------------------------------------------------}
function TFrameSolucionOrdenServicio.GetFechacompromiso: tdatetime;
begin
    Result := efechacompromiso.Date;
end;

{-----------------------------------------------------------------------------
  Function Name: GetEntidad
  Author:
  Date Created: 19/04/2005
  Description:
  Parameters: None
  Return Value: AnsiString
-----------------------------------------------------------------------------}
function TFrameSolucionOrdenServicio.GetEntidad: integer;
begin
    Result := eentidad.value;
end;




end.
