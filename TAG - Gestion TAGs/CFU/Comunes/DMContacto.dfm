object DMContactos: TDMContactos
  OldCreateOrder = False
  Left = 329
  Top = 210
  Height = 200
  Width = 249
  object SPActualizarContacto: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarContacto'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@Apellido'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
      end
      item
        Name = '@ApellidoMaterno'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
      end
      item
        Name = '@Nombre'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
      end
      item
        Name = '@Sexo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
      end
      item
        Name = '@Pais'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
      end
      item
        Name = '@CallePersonal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
      end
      item
        Name = '@NumeroPersonal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
      end
      item
        Name = '@DetallePersonal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
      end
      item
        Name = '@RegionPersonal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
      end
      item
        Name = '@ComunaPersonal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
      end
      item
        Name = '@CodigoPostalPersonal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
      end
      item
        Name = '@TelefonoPersonal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
      end
      item
        Name = '@TelefonoComercialPersonal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
      end
      item
        Name = '@TelefonoMovilPersonal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
      end
      item
        Name = '@CalleDocumentos'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
      end
      item
        Name = '@NumeroDocumentos'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
      end
      item
        Name = '@DetalleDocumentos'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
      end
      item
        Name = '@RegionDocumentos'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
      end
      item
        Name = '@ComunaDocumentos'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
      end
      item
        Name = '@CodigoPostalDocumentos'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
      end
      item
        Name = '@email'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
      end
      item
        Name = '@email2'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
      end
      item
        Name = '@FechaNacimiento'
        Attributes = [paNullable]
        DataType = ftDateTime
      end
      item
        Name = '@Estado'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
      end
      item
        Name = '@PIN'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 4
      end
      item
        Name = '@CodigoContacto'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
      end>
    Left = 64
    Top = 32
  end
end
