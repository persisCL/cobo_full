object frameMovimientoStock: TframeMovimientoStock
  Left = 0
  Top = 0
  Width = 675
  Height = 415
  TabOrder = 0
  object vleIngresados: TValueListEditor
    Left = 166
    Top = 211
    Width = 34
    Height = 40
    TabOrder = 0
    Visible = False
    ColWidths = (
      103
      -75)
  end
  object vleRecibidos: TValueListEditor
    Left = 212
    Top = 211
    Width = 37
    Height = 40
    TabOrder = 1
    Visible = False
    ColWidths = (
      53
      -22)
  end
  object pnlEntradaDatos: TPanel
    Left = 0
    Top = 319
    Width = 675
    Height = 96
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    Visible = False
    DesignSize = (
      675
      96)
    object gbAgregaTelevias: TGroupBox
      Left = 1
      Top = 6
      Width = 671
      Height = 87
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = ' A'#241'adir Telev'#237'as '
      TabOrder = 0
      DesignSize = (
        671
        87)
      object Label1: TLabel
        Left = 76
        Top = 14
        Width = 64
        Height = 13
        Anchors = [akTop, akRight]
        Caption = 'Telev'#237'a Inicial'
      end
      object Label2: TLabel
        Left = 201
        Top = 14
        Width = 59
        Height = 13
        Anchors = [akTop, akRight]
        Caption = 'Telev'#237'a Final'
      end
      object Label3: TLabel
        Left = 339
        Top = 14
        Width = 43
        Height = 13
        Anchors = [akTop, akRight]
        Caption = 'Cantidad'
      end
      object Label4: TLabel
        Left = 412
        Top = 13
        Width = 108
        Height = 13
        Anchors = [akTop, akRight]
        Caption = 'Categor'#237'a Interurbana'
      end
      object lbl1: TLabel
        Left = 569
        Top = 13
        Width = 85
        Height = 13
        Anchors = [akTop, akRight]
        Caption = 'Categor'#237'a Urbana'
      end
      object dbeTeleviaInicial: TDBEdit
        Left = 54
        Top = 29
        Width = 117
        Height = 21
        Anchors = [akTop, akRight]
        DataField = 'NumeroInicialTAG'
        DataSource = dsTAGs
        TabOrder = 0
      end
      object dbeTeleviaFinal: TDBEdit
        Left = 172
        Top = 29
        Width = 117
        Height = 21
        Anchors = [akTop, akRight]
        DataField = 'NumeroFinalTAG'
        DataSource = dsTAGs
        TabOrder = 1
      end
      object dbeCantidad: TDBEdit
        Left = 290
        Top = 29
        Width = 97
        Height = 21
        Anchors = [akTop, akRight]
        DataField = 'CantidadTAGs'
        DataSource = dsTAGs
        TabOrder = 2
      end
      object dbeCategoria: TDBEdit
        Left = 388
        Top = 29
        Width = 132
        Height = 21
        Anchors = [akTop, akRight]
        DataField = 'CategoriaTAGs'
        DataSource = dsTAGs
        TabOrder = 3
      end
      object btnGrabar: TButton
        Left = 497
        Top = 56
        Width = 75
        Height = 25
        Anchors = [akTop, akRight]
        Caption = '&Grabar'
        TabOrder = 4
        OnClick = btnGrabarClick
      end
      object btnCancelar: TButton
        Left = 576
        Top = 56
        Width = 75
        Height = 25
        Anchors = [akTop, akRight]
        Caption = 'Cancelar'
        TabOrder = 5
        OnClick = btnCancelarClick
      end
      object dbeCategoriaUrbana: TDBEdit
        Left = 522
        Top = 29
        Width = 132
        Height = 21
        Anchors = [akTop, akRight]
        DataField = 'CategoriaUrbanaTAGs'
        DataSource = dsTAGs
        TabOrder = 6
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 675
    Height = 319
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    DesignSize = (
      675
      319)
    object lblDetalle: TLabel
      Left = 4
      Top = 114
      Width = 93
      Height = 13
      Caption = 'Detalle de Telev'#237'as'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object dbgRecibirTags: TDBGrid
      Left = 0
      Top = 0
      Width = 675
      Height = 108
      TabStop = False
      Align = alTop
      DataSource = dsEntrega
      Options = [dgTitles, dgColLines, dgRowLines, dgTabs, dgRowSelect]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      OnKeyDown = dbgRecibirTagsKeyDown
      Columns = <
        item
          Expanded = False
          FieldName = 'Descripcion'
          ReadOnly = True
          Title.Caption = 'Categor'#237'a Interurbana'
          Width = 215
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DscCategoriaUrbana'
          Title.Caption = 'Categor'#237'a Urbana'
          Width = 215
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'CantidadTAGsDisponibles'
          ReadOnly = True
          Title.Alignment = taCenter
          Title.Caption = 'Telev'#237'as Disponibles'
          Width = 110
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CantidadTags'
          Title.Caption = 'Cantidad de Telev'#237'as'
          Width = 108
          Visible = True
        end>
    end
    object dbgTags: TDBGrid
      Left = 0
      Top = 133
      Width = 675
      Height = 187
      TabStop = False
      Anchors = [akLeft, akTop, akRight, akBottom]
      DataSource = dsTAGs
      Options = [dgTitles, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection]
      ReadOnly = True
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'NumeroInicialTAG'
          Title.Alignment = taCenter
          Title.Caption = 'Telev'#237'a Inicial'
          Width = 120
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'NumeroFinalTAG'
          Title.Alignment = taCenter
          Title.Caption = 'Telev'#237'a Final'
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CantidadTAGs'
          Title.Alignment = taRightJustify
          Title.Caption = 'Cantidad'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CategoriaTAGs'
          Title.Alignment = taRightJustify
          Title.Caption = 'Categor'#237'a Interurbana'
          Width = 122
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DscCategoriaUrbana'
          Title.Caption = 'Categor'#237'a Urbana'
          Width = 125
          Visible = True
        end>
    end
  end
  object spObtenerDatosPuntoEntrega: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDatosPuntoEntrega'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPuntoEntrega'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NombreAlmacenDestino'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 50
        Value = Null
      end
      item
        Name = '@CodigoAlmacenDestino'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@SoloNominado'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 258
    Top = 66
  end
  object spVerificarTAGEnArchivos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'VerificarTAGEnArchivos'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@ContractSerialNumber'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end>
    Left = 288
    Top = 66
  end
  object spObtenerDatosRangoTAGsAlmacen: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerDatosRangoTAGsAlmacen'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TAGInicial'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@TAGFinal'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CategoriaTAG'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoAlmacen'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 318
    Top = 66
  end
  object dsTAGs: TDataSource
    DataSet = cdsTAGs
    Left = 136
    Top = 160
  end
  object cdsTAGs: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'NumeroInicialTAG'
        DataType = ftLargeint
      end
      item
        Name = 'NumeroFinalTAG'
        DataType = ftLargeint
      end
      item
        Name = 'CantidadTAGs'
        DataType = ftLargeint
      end
      item
        Name = 'CategoriaTAGs'
        DataType = ftInteger
      end
      item
        Name = 'CategoriaUrbanaTAGs'
        DataType = ftInteger
      end>
    IndexDefs = <
      item
        Name = 'DEFAULT_ORDER'
      end
      item
        Name = 'CHANGEINDEX'
      end>
    Params = <>
    StoreDefs = True
    BeforeEdit = cdsTAGsBeforeEdit
    BeforePost = cdsTAGsBeforePost
    AfterPost = cdsTAGsAfterPost
    AfterCancel = cdsTAGsAfterPost
    AfterDelete = cdsTAGsAfterDelete
    OnNewRecord = cdsTAGsNewRecord
    Left = 262
    Top = 136
  end
  object spRegistrarMovimientoStock: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'RegistrarMovimientoStock'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoOperacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoMovimiento'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoEmbalaje'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroInicialTAG'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@NumeroFinalTAG'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CategoriaTAG'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CategoriaUrbanaTAG'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Cantidad'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@EstadoSituacion'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@EstadoControl'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@CodigoAlmacenOrigen'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoAlmacenDestino'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@GuiaDespacho'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@FechaHoraEvento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@MotivoEntrega'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@ConstanciaCarabinero'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@FechaHoraConstancia'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@DescripcionMotivoRegistro'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@CodigoComunicacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 198
    Top = 148
  end
  object ObtenerSaldosTAGsAlmacen: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerSaldosTAGsAlmacen'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoAlmacen'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 168
    Top = 69
  end
  object cdsEntrega: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Descripcion'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CantidadTAGsDisponibles'
        DataType = ftLargeint
      end
      item
        Name = 'CantidadTAGs'
        DataType = ftLargeint
      end
      item
        Name = 'Categoria'
        DataType = ftInteger
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 136
    Top = 69
  end
  object dsEntrega: TDataSource
    DataSet = cdsEntrega
    Left = 104
    Top = 69
  end
  object qryCategorias: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <>
    SQL.Strings = (
      'SELECT Categoria, Descripcion FROM Categorias')
    Left = 368
    Top = 65
  end
end
