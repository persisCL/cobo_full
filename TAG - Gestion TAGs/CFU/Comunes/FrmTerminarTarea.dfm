object FormTerminarTarea: TFormTerminarTarea
  Left = 248
  Top = 155
  BorderStyle = bsDialog
  Caption = 'Terminar Tarea'
  ClientHeight = 224
  ClientWidth = 457
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 9
    Top = 8
    Width = 443
    Height = 45
    Alignment = taCenter
    AutoSize = False
    Caption = 
      'Si contin'#250'a, la tarea se'#225' marcada como terminada, con lo cual ot' +
      'ros usuarios podr'#225'n realizar las tareas que dependen de '#233'sta. De' +
      ' ser necesario, por favor complete un comentario de lo que ha re' +
      'alizado que pueda servir de ayuda a los dem'#225's usuarios.'
    WordWrap = True
  end
  object Memo1: TMemo
    Left = 5
    Top = 54
    Width = 447
    Height = 133
    TabOrder = 0
  end
  object OPButton1: TButton
    Left = 256
    Top = 195
    Width = 107
    Height = 25
    Caption = 'Terminar &Tarea'
    Default = True
    TabOrder = 1
    OnClick = OPButton1Click
  end
  object OPButton2: TButton
    Left = 376
    Top = 195
    Width = 75
    Height = 25
    Cancel = True
    Caption = '&Salir'
    ModalResult = 2
    TabOrder = 2
  end
  object TerminarTarea: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'TerminarTarea;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoOrdenServicio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTarea'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Observaciones'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end>
    Left = 8
    Top = 195
  end
end
