unit frmEditarMedioComunicacion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, Util, Peaprocs, UtilProc, UtilDB,
  DmiCtrls, BuscaTab, Validate, TimeEdit, Buttons, peatypes, DB, DBClient,
  Provider, ADODB, VariantComboBox,RStrings, FreTelefono;

type
  TformEditarMedioComunicacion = class(TForm)
    pnlInferior: TPanel;
    Label1: TLabel;
    cbTipoMedioContacto: TVariantComboBox;
    btDomicilios: TBuscaTabla;
    Bevel1: TBevel;
    Label4: TLabel;
    ObtenerDomiciliosPersona: TADOStoredProc;
    dsDomicilios: TDataSource;
    cdsDomicilios: TClientDataSet;
    dspDomicilios: TDataSetProvider;
    Notebook: TNotebook;
    txtObservaciones: TEdit;
    bteDetalle: TBuscaTabEdit;
    Label6: TLabel;
    eMail: TEdit;
    Label5: TLabel;
    sbtnClear: TSpeedButton;
    cdsDomiciliosCodigoDomicilio: TIntegerField;
    cdsDomiciliosCodigoPersona: TIntegerField;
    cdsDomiciliosTipoDomicilio: TSmallintField;
    cdsDomiciliosDescriTipoDomicilio: TStringField;
    cdsDomiciliosCodigoPais: TStringField;
    cdsDomiciliosDescriPais: TStringField;
    cdsDomiciliosCodigoRegion: TStringField;
    cdsDomiciliosDescriRegion: TStringField;
    cdsDomiciliosCodigoComuna: TStringField;
    cdsDomiciliosDescriComuna: TStringField;
    cdsDomiciliosDescripcionCiudad: TStringField;
    cdsDomiciliosCodigoCalle: TIntegerField;
    cdsDomiciliosNumero: TStringField;
    cdsDomiciliosPiso: TSmallintField;
    cdsDomiciliosDpto: TStringField;
    cdsDomiciliosCalleDesnormalizada: TStringField;
    cdsDomiciliosDetalle: TStringField;
    cdsDomiciliosCodigoPostal: TStringField;
    cdsDomiciliosDomicilioEntrega: TBooleanField;
    cdsDomiciliosCalleRelacionadaUno: TIntegerField;
    cdsDomiciliosNumeroCalleRelacionadaUno: TIntegerField;
    cdsDomiciliosCalleRelacionadaDos: TIntegerField;
    cdsDomiciliosNumeroCalleRelacionadaDos: TIntegerField;
    cdsDomiciliosNormalizado: TIntegerField;
    cdsDomiciliosDireccionCompleta: TStringField;
    cdsDomiciliosUbicacionGeografica: TStringField;
    FreTelefono: TFrameTelefono;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    procedure BtnAceptarClick(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
    procedure sbtnClearClick(Sender: TObject);
    procedure IrAlInicio(Sender: TObject);
    procedure btDomiciliosSelect(Sender: TObject; Tabla: TDataSet);
    function btDomiciliosProcess(Tabla: TDataSet;
      var Texto: String): Boolean;
    procedure FormCreate(Sender: TObject);
    procedure cbTipoMedioContactoChange(Sender: TObject);
  private
    { Private declarations }
    FCodigoDomicilio: integer;
    FCodigoDomicilioRecNo: integer;
    FTipoMedio: CHAR;

    function GetObservaciones: AnsiString;
    function GetTipoMedioContacto: Integer;
    function GetCodigoArea: Integer;
    function GetValor: AnsiString;
    function getHoraDesde: TDateTime;
    function getHoraHasta: TDateTime;
    function getDescriDomicilio: AnsiString;
    procedure HabilitarMediosContacto(TipoMedio: AnsiString);
    function GetAnexo: AnsiString;
    function getDetalle: AnsiString;
    function GetDescripcionMedioContacto:String;
    function GetCodigoMedioContacto:Integer;
  public
    { Public declarations }
    Property CodigoDomicilio: integer read FCodigoDomicilio;
    Property CodigoDomicilioRecNo: integer read FCodigoDomicilioRecNo;
    property CodigoMedioContacto: integer read GetCodigoMedioContacto;
    property DescripcionMedioContacto: String read GetDescripcionMedioContacto;
    property TipoMedioContacto: integer read GetTipoMedioContacto;
    property Detalle: AnsiString read getDetalle;
    property CodigoArea: Integer read GetCodigoArea;
    property Valor: AnsiString read GetValor;
    property Anexo: AnsiString read GetAnexo;
    property Observaciones: AnsiString read GetObservaciones;
    property HoraDesde: TDatetime read getHoraDesde;
    property HoraHasta: TDatetime read getHoraHasta;
    property DescriDomicilio: ANsiString read GetDescriDomicilio;
    Function Inicializar(CodigoTipoMedioContacto: integer=TIPO_MEDIO_CONTACTO_E_MAIL; CodigoArea: Integer=0; Valor: AnsiString=''; HoraDesde: TDateTime = NullDate;  HoraHasta: TDateTime = NullDate; Anexo: AnsiString=''; Observaciones: AnsiString=''; CodigoDomicilio: integer=0; TablaDireccions:TClientDataSet=nil): Boolean;

  end;

var
  formEditarMedioComunicacion: TformEditarMedioComunicacion;

implementation

uses DMConnection;

{$R *.dfm}


Function TformEditarMedioComunicacion.Inicializar(
    CodigoTipoMedioContacto: integer=TIPO_MEDIO_CONTACTO_E_MAIL;
    CodigoArea: Integer=0;
    Valor: AnsiString='';
    HoraDesde: TDateTime = NullDate;
     HoraHasta: TDateTime = NullDate;
    Anexo: AnsiString='';
    Observaciones: AnsiString='';
    CodigoDomicilio: integer=0;
    TablaDireccions:TClientDataSet=nil): Boolean;
begin
    FreTelefono.Inicializa('Telefono',true);
    if TablaDireccions<>nil then begin
        cdsDomicilios:=TablaDireccions;
        CargarTiposMedioContacto(DMConnections.BaseCAC, cbTipoMedioContacto, CodigoTipoMedioContacto);
    end else
        CargarTiposMedioContacto(DMConnections.BaseCAC, cbTipoMedioContacto, CodigoTipoMedioContacto,'',false,false,TMC_DIRECCION);


    cbTipoMedioContacto.OnChange(cbTipoMedioContacto);
    txtObservaciones.Text   := Trim(Observaciones);
    if FTipoMedio = TMC_TELEFONO then begin
        FreTelefono.CargarTelefono(CodigoArea,trim(Valor),CodigoTipoMedioContacto,HoraDesde,HoraHasta);
    end else if FTipoMedio = TMC_EMAIL then begin
        eMail.text := valor;
    end else if FTipoMedio = TMC_DIRECCION then begin
        FCodigoDomicilioRecNo   := CodigoDomicilio;
        if CodigoDomicilio > 0 then begin
            cdsDomicilios.First;
            bteDetalle.Text := '';
            while not cdsDomicilios.Eof do begin
                if cdsDomicilios.FieldByName('CodigoDomicilio').AsInteger = CodigoDomicilio then begin
                    bteDetalle.Text     := Trim(cdsDomicilios.fieldByName('DireccionCompleta').AsString);
                    break;
                end;
                cdsDomicilios.Next;
            end;
        end;
    end;
    Result := True;
end;

procedure TformEditarMedioComunicacion.HabilitarMediosContacto(TipoMedio: AnsiString);
begin
    if (trim(TipoMedio)<>'') then begin
        FTipoMedio := TipoMedio[1];
        if TipoMedio = TMC_TELEFONO then begin
            Notebook.ActivePage := 'Telefono';
            FreTelefono.CargarTelefono(CODIGO_AREA_SANTIAGO,'',self.CodigoMedioContacto);
        end else if TipoMedio = TMC_EMAIL then begin
            Notebook.ActivePage := 'EMail';
        end else if TipoMedio = TMC_DIRECCION then begin
            Notebook.ActivePage := 'Domicilio';
        end;
    end;
end;

function TformEditarMedioComunicacion.getHoraDesde: TDateTime;
begin
    if FTipoMedio = TMC_TELEFONO then begin
            result:=FreTelefono.HorarioDesde;
    end else begin
        Result := 0;
    end;
end;

function TformEditarMedioComunicacion.getHoraHasta: TDateTime;
begin
    if FTipoMedio = TMC_TELEFONO then begin
        result:=FreTelefono.HorarioHasta;
    end else begin
        Result := 0;
    end;
end;

function TformEditarMedioComunicacion.GetValor: AnsiString;
begin
    if FTipoMedio = TMC_TELEFONO then begin
        result:=FreTelefono.NumeroTelefono;
    end else if FTipoMedio = TMC_EMAIL then begin
        Result := trim(eMail.Text);
    end else if FTipoMedio = TMC_DIRECCION then begin
        Result := '';
    end;
end;

function TformEditarMedioComunicacion.GetTipoMedioCOntacto: integer;
begin
    if cbTipoMedioContacto.ItemIndex < 0 then Result := -1
    else Result := cbTipoMedioCOntacto.Value;
end;


function TformEditarMedioComunicacion.GetObservaciones: AnsiString;
begin
    Result := Trim(txtObservaciones.Text);
end;


procedure TformEditarMedioComunicacion.BtnAceptarClick(Sender: TObject);
begin
    //Valido los datos del formato TE
    if (FTipoMedio = TMC_TELEFONO) and not(FreTelefono.ValidarTelefono) then
            exit
    else if FTipoMedio = TMC_EMAIL then begin
            if not(ValidateControls([eMail],
                                [(trim(eMail.Text)<>'') and IsValidEMailList(eMail.Text)],
                                format(MSG_CAPTION_VALIDAR,[FLD_MEDIO_CONTACTO]),
                                [MSG_VALIDAR_DIRECCION_EMAIL])
            ) then Exit;

    end else if FTipoMedio = TMC_DIRECCION then begin
        if not(ValidateControls([bteDetalle],
                                [trim(bteDetalle.text)=''],
                                format(MSG_CAPTION_VALIDAR,[FLD_MEDIO_CONTACTO]),
                                [format(MSG_VALIDAR_DEBE_EL,[FLD_MEDIO_CONTACTO])])
        ) then exit;

    end;
    ModalResult := mrOK;
end;

procedure TformEditarMedioComunicacion.BtnCancelarClick(Sender: TObject);
begin
    Close;
end;

procedure TformEditarMedioComunicacion.sbtnClearClick(Sender: TObject);
begin
	bteDetalle.Text := '';
end;

procedure TformEditarMedioComunicacion.IrAlInicio(Sender: TObject);
begin
	bteDetalle.SelStart := 0;
end;

function TformEditarMedioComunicacion.getDescriDomicilio: AnsiString;
begin
    if FTipoMedio = TMC_DIRECCION then begin
        Result := Trim(cdsDomicilios.FieldByName('DireccionCompleta').AsString);
    end else begin
        Result := '';
    end;
end;

procedure TformEditarMedioComunicacion.btDomiciliosSelect(Sender: TObject;
  Tabla: TDataSet);
begin
    bteDetalle.Text         := Trim(Tabla.FieldByName('DireccionCompleta').AsString);
    FCodigoDomicilio        := Tabla.FieldByName('CodigoDomicilio').AsInteger;
    FCodigoDomicilioRecNo   := Tabla.RecNo;
end;

function TformEditarMedioComunicacion.btDomiciliosProcess(Tabla: TDataSet;
  var Texto: String): Boolean;
begin
    Texto   := Trim(Tabla.FieldByName('DireccionCompleta').AsString);
    result  := True;
end;

procedure TformEditarMedioComunicacion.FormCreate(Sender: TObject);
begin
    if cdsDomicilios<>nil then  cdsDomicilios.Open;
    ObtenerDomiciliosPersona.Close;
end;

procedure TformEditarMedioComunicacion.cbTipoMedioContactoChange(
  Sender: TObject);
var
    aTipoMedioCOntacto: INteger;
begin
    aTipoMedioContacto := iif(TVariantComboBox(Sender).Items.Count<=0, -1, TVariantComboBox(Sender).Value);
    HabilitarMediosContacto(QueryGetValue(DMCOnnections.BaseCAC,
      format('SELECT Formato FROM TiposMedioContacto WHERE CodigoTipoMedioContacto = %d', [aTipoMedioContacto])));
end;


function TformEditarMedioComunicacion.GetAnexo: AnsiString;
begin
    if FTipoMedio = TMC_TELEFONO then
//       result := trim(eAnexo.Text)
        result:=''
    else
       result := '';
end;

function TformEditarMedioComunicacion.GetCodigoArea: Integer;
begin
    if FTipoMedio = TMC_TELEFONO then begin
        try
            result:=FreTelefono.CodigoArea
        except
            result := 0;
        end;
    end else begin
        result := 0;
    end;
end;

function TformEditarMedioComunicacion.getDetalle: AnsiString;
begin


    if FTipoMedio = TMC_TELEFONO then begin
        Result := format('%s-%s', [Trim(inttostr(self.CodigoArea)), self.Valor]);
    end else if FTipoMedio = TMC_EMAIL then begin
        Result := Self.Valor;
    end else if FTipoMedio = TMC_DIRECCION then begin
        Result := Trim(cdsDomicilios.fieldByName('DireccionCompleta').AsString);
    end;
end;

function TformEditarMedioComunicacion.GetDescripcionMedioContacto: String;
begin
    result:=trim(cbTipoMedioContacto.Items[cbTipoMedioContacto.ItemIndex].Caption);
end;

function TformEditarMedioComunicacion.GetCodigoMedioContacto: Integer;
begin
    if cbTipoMedioContacto.Items.Count>0 then result:=Integer(cbTipoMedioContacto.Value)
    else result:=-1;
end;

end.
