object FrameHistoriaOrdenServicio: TFrameHistoriaOrdenServicio
  Left = 0
  Top = 0
  Width = 419
  Height = 380
  TabOrder = 0
  object DBLHistoria: TDBListEx
    Left = 0
    Top = 35
    Width = 419
    Height = 304
    Align = alClient
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 120
        Header.Caption = 'F. Modificaci'#243'n'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = DBLHistoriaColumns0HeaderClick
        FieldName = 'FechaHoraModificacion'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 100
        Header.Caption = 'F. Compromiso'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = DBLHistoriaColumns0HeaderClick
        FieldName = 'FechaCompromiso'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 60
        Header.Caption = 'Usuario'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = DBLHistoriaColumns0HeaderClick
        FieldName = 'Usuario'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 60
        Header.Caption = 'Prioridad'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = DBLHistoriaColumns0HeaderClick
        FieldName = 'Prioridad'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 60
        Header.Caption = 'Estado'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        OnHeaderClick = DBLHistoriaColumns0HeaderClick
        FieldName = 'DescEstado'
      end>
    DataSource = DataSource
    DragReorder = True
    ParentColor = False
    TabOrder = 0
    TabStop = True
    OnDrawText = DBLHistoriaDrawText
  end
  object Pencabezado: TPanel
    Left = 0
    Top = 0
    Width = 419
    Height = 35
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object Lmensaje: TLabel
      Left = 31
      Top = 5
      Width = 418
      Height = 47
      AutoSize = False
      Caption = 'Historia de los cambios que se produjeron'
      WordWrap = True
    end
    object Pizq: TPanel
      Left = 0
      Top = 6
      Width = 7
      Height = 29
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
    end
    object Pder: TPanel
      Left = 411
      Top = 6
      Width = 8
      Height = 29
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
    end
    object Parriva: TPanel
      Left = 0
      Top = 0
      Width = 419
      Height = 6
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
    end
  end
  object Pabajo: TPanel
    Left = 0
    Top = 339
    Width = 419
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
  end
  object SpObtenerHistoriaOrdenServicio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerHistoriaOrdenServicio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoOrdenServicio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 1
      end>
    Left = 48
    Top = 120
    object SpObtenerHistoriaOrdenServicioCodigoOrdenServicio: TIntegerField
      FieldName = 'CodigoOrdenServicio'
    end
    object SpObtenerHistoriaOrdenServicioFechaHoraModificacion: TDateTimeField
      FieldName = 'FechaHoraModificacion'
    end
    object SpObtenerHistoriaOrdenServicioUsuario: TStringField
      FieldName = 'Usuario'
      FixedChar = True
    end
    object SpObtenerHistoriaOrdenServicioEstado: TStringField
      FieldName = 'Estado'
      FixedChar = True
      Size = 1
    end
    object SpObtenerHistoriaOrdenServicioPrioridad: TWordField
      FieldName = 'Prioridad'
    end
    object SpObtenerHistoriaOrdenServicioFechaCompromiso: TDateTimeField
      FieldName = 'FechaCompromiso'
    end
    object SpObtenerHistoriaOrdenServicioDescEstado: TStringField
      FieldName = 'DescEstado'
      ReadOnly = True
      Size = 9
    end
  end
  object DataSource: TDataSource
    DataSet = SpObtenerHistoriaOrdenServicio
    Left = 144
    Top = 160
  end
end
