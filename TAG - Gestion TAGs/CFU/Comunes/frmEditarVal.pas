unit frmEditarVal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ADODB, ExtCtrls, StdCtrls, Util, UtilDB, UtilProc, Buttons,
  PeaProcs, constParametrosGenerales;

type
  TfrmValueEdit = class(TForm)
    btnAceptar: TButton;
    btnCancelar: TButton;
    Bevel1: TBevel;
    txtValor: TEdit;
    Label1: TLabel;
    lblDescri: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    procedure btnAceptarClick(Sender: TObject);
    procedure Label3Click(Sender: TObject);
  private
    FClass: String[1];
    FName: String;
    FType: String[1];
    FTabla: TADOTable;
    FRecNum: Integer;
  public
    function Inicializar(Tabla: TADOTable; RecNum: Integer): Boolean;
  end;

var
  frmValueEdit: TfrmValueEdit;

implementation

{$R *.dfm}

{ TfrmValueEdit }

function TfrmValueEdit.Inicializar(Tabla: TADOTable; RecNum: Integer): Boolean;
begin
    lblDescri.Caption := Tabla.FieldByName('Descripcion').AsString;
    FClass := Tabla.FieldByName('ClaseParametro').AsString;
    FName := Trim(Tabla.FieldByName('Nombre').AsString);
    FType := Trim(Tabla.FieldByName('TipoParametro').AsString);
    txtValor.Text := Trim(Tabla.FieldByName('Valor').AsString);
    FTabla := Tabla;
    FRecNum:= RecNum;
    Result := True;
end;

procedure TfrmValueEdit.btnAceptarClick(Sender: TObject);
resourcestring
    MSG_INVALID_DATE = 'El valor ingresado no es una fecha valida';
    MSG_VALUE = 'Valor No V�lido';
    MSG_UPDATE = 'Error al actualizar';
    MSG_ERROR = 'Error';
begin
    try
        if FType = TP_FECHA then begin
            if  not ValidateControls([txtValor], [(IsValidDate(txtValor.text))],
              MSG_VALUE,[MSG_INVALID_DATE]) then begin
                Exit;
            end else begin
                txtValor.Text := FormatDateTime('dd/mm/yyyy', StrToDateTime(txtValor.text));
            end;
        end;
        FTabla.Close;
        QueryExecute(FTabla.Connection,
        'UPDATE PARAMETROSGENERALES SET VALOR =' + QuotedStr(Trim(txtValor.Text)) +
        'WHERE CLASEPARAMETRO = ' + QuotedStr(FClass) + ' AND ' +
        'NOMBRE = ' + QuotedStr(FName));
        FTabla.Open;
        FTabla.MoveBy(FRecNum - 1);
        ModalResult := mrOK;
    except
        on e: Exception do begin
            MsgBoxErr(MSG_ERROR, e.Message, MSG_UPDATE, 0);
            ModalResult := mrOK;
        end;
    end;
end;

procedure TfrmValueEdit.Label3Click(Sender: TObject);
var
    Ubica: String;
begin
    Ubica := BrowseForFolder('Elija una carpeta');
    if Ubica <> '' then txtValor.Text := Ubica;
end;


end.
