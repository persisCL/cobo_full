{-------------------------------------------------------------------------------
 File Name: FreConcesionariaReclamoOrdenServicio.pas
 Author: Lgisuk
 Date Created: 17/06/05
 Language: ES-AR
 Description: Seccion con la información de la Concesionaria del reclamo
-------------------------------------------------------------------------------}
unit FreConcesionariaReclamoOrdenServicio;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, VariantComboBox, DB, ADODB, UtilProc;

type
  TFrameConcesionariaReclamoOrdenServicio = class(TFrame)
    GBConcesionaria: TGroupBox;
    lblConcesionaria: TLabel;
    vcbConcesionarias: TVariantComboBox;
  private
    { Private declarations }
    Function  GetConcesionariaReclamo: Integer;
    Procedure SetConcesionariaReclamo(const Value: Integer);
  public
    { Public declarations }
    Function  Inicializar: Boolean; overload;
    Function  Inicializar(ConcesionariaReclamo: Integer): Boolean; overload;
    Function  Deshabilitar:boolean;
    Function  Validar: Boolean;
    Procedure LoadFromDataset(DS: TDataset);
    //
    Property ConcesionariaReclamo: Integer read GetConcesionariaReclamo write SetConcesionariaReclamo;
  end;

implementation

uses RStrings, DMConnection, PeaProcs;

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name	: Inicializar
  Author		: Nelson Droguett Sierra
  Date Created	: 24/06/2010
  Description	: inicializacion del frame
  Parameters	: None
  Return Value	: Boolean
-----------------------------------------------------------------------------}
function TFrameConcesionariaReclamoOrdenServicio.Inicializar: Boolean;
var SQLQuery:string;
    CodigoConcesionariaReclamo:integer;
begin
    CargarConcesionariasReclamoHistorico(vcbConcesionarias);                    //SS_1006_1015_MCO_20120925
    // Aca poner la Conce. por defecto.
    Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author		: Nelson Droguett Sierra
  Date Created	: 24/06/2010
  Description: inicializo el frame eligiendo una concesionaria de reclamo
  Parameters: ConcesionariaReclamo: Integer
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFrameConcesionariaReclamoOrdenServicio.Inicializar(ConcesionariaReclamo: Integer): Boolean;
begin
    vcbConcesionarias.Value := ConcesionariaReclamo;
    Result := Inicializar;
end;

{-----------------------------------------------------------------------------
  Function Name: GetConcesionariaReclamo
  Author		: Nelson Droguett Sierra
  Date Created	: 24/06/2010
  Description:  Obtengo la Concesionaria de reclamo
  Parameters: None
  Return Value: Integer
-----------------------------------------------------------------------------}
function TFrameConcesionariaReclamoOrdenServicio.GetConcesionariaReclamo: Integer;
begin
    Result := vcbConcesionarias.Value;
end;

{-----------------------------------------------------------------------------
  Function Name: SetConcesionariaReclamo
  Author		: Nelson Droguett Sierra
  Date Created	: 24/06/2010
  Description: Asigno la concesionaria de Reclamo
  Parameters: const Value: Integer
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrameConcesionariaReclamoOrdenServicio.SetConcesionariaReclamo(const Value: Integer);
begin
    vcbConcesionarias.Value := Value;
end;

{-----------------------------------------------------------------------------
  Function Name: LoadFromDataset
  Author		: Nelson Droguett Sierra
  Date Created	: 24/06/2010
  Description: cargo la Concesionaria de reclamo de un dataset
  Parameters: DS: TDataset
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrameConcesionariaReclamoOrdenServicio.LoadFromDataset(DS: TDataset);
begin
    vcbConcesionarias.Value := DS['CodigoConcesionaria'];
end;

{-----------------------------------------------------------------------------
  Function Name: Deshabilitar
  Author		: Nelson Droguett Sierra
  Date Created	: 24/06/2010
  Description:  deshabilito los campos
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
function TFrameConcesionariaReclamoOrdenServicio.Deshabilitar:boolean;
begin
    vcbConcesionarias.Enabled:= False;
    vcbConcesionarias.Color:= clBtnFace;
    result:= true;
end;

{-----------------------------------------------------------------------------
  Function Name: Validar
  Author		: Nelson Droguett Sierra
  Date Created	: 24/06/2010
  Description: valido la concesionaria de reclamo
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFrameConcesionariaReclamoOrdenServicio.Validar: Boolean;
resourcestring
    MSG_ERROR_CONCESIONARIA = 'Debe indicar la concesionaria del reclamo';
begin
    Result := False;
    //obligo a que carguen una concesionaria de reclamo
    if vcbConcesionarias.value < 1 then begin
        MsgBoxBalloon(MSG_ERROR_CONCESIONARIA, STR_ERROR, MB_ICONSTOP, vcbConcesionarias);
        vcbConcesionarias.SetFocus;
        Exit;
    end;
    Result := True;
end;

end.
