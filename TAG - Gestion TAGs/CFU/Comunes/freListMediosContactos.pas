unit freListMediosContactos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, DPSControls, ListBoxEx, DBListEx, Provider, DB, DBClient, ADODB,
  frmEditarMedioComunicacion,DMConnection,Peatypes,Variants,UtilDb,UtilProc,RStrings,Util;

type
  TFrameListMediosContactos = class(TFrame)
    ObtenerMediosComunicacionPersona: TADOStoredProc;
    dsMediosComunicacion: TDataSource;
    cdsMediosComunicacion: TClientDataSet;
    dspMediosComunicacion: TDataSetProvider;
    dblMedios: TDBListEx;
    cdsMediosComunicacionBorrados: TClientDataSet;
    cdsMediosComunicacionBorradosIndiceMedioComunicacion: TIntegerField;
    cdsMediosComunicacionBorradosCodigoTipoMedioContacto: TSmallintField;
    cdsMediosComunicacionBorradosFormato: TStringField;
    cdsMediosComunicacionBorradosDescriTipoMedioContacto: TStringField;
    cdsMediosComunicacionBorradosCodigoArea: TSmallintField;
    cdsMediosComunicacionBorradosValor: TStringField;
    cdsMediosComunicacionBorradosANexo: TStringField;
    cdsMediosComunicacionBorradosCodigoDomicilio: TIntegerField;
    cdsMediosComunicacionBorradosDescriDomicilio: TStringField;
    cdsMediosComunicacionBorradosDetalle: TStringField;
    cdsMediosComunicacionBorradosHorarioDesde: TDateTimeField;
    cdsMediosComunicacionBorradosHorarioHasta: TDateTimeField;
    cdsMediosComunicacionBorradosObservaciones: TStringField;
    cdsMediosComunicacionBorradosCodigoDomicilioRecNo: TIntegerField;
    cdsMediosComunicacionIndiceMedioComunicacion: TIntegerField;
    cdsMediosComunicacionCodigoTipoMedioContacto: TSmallintField;
    cdsMediosComunicacionFormato: TStringField;
    cdsMediosComunicacionDescriTipoMedioContacto: TStringField;
    cdsMediosComunicacionCodigoArea: TSmallintField;
    cdsMediosComunicacionValor: TStringField;
    cdsMediosComunicacionANexo: TStringField;
    cdsMediosComunicacionCodigoDomicilio: TIntegerField;
    cdsMediosComunicacionDescriDomicilio: TStringField;
    cdsMediosComunicacionDetalle: TStringField;
    cdsMediosComunicacionHorarioDesde: TDateTimeField;
    cdsMediosComunicacionHorarioHasta: TDateTimeField;
    cdsMediosComunicacionObservaciones: TStringField;
    cdsMediosComunicacionCodigoDomicilioRecNo: TIntegerField;
    btnAgregarMedioComunicacion: TButton;
    btnEditarMedioComunicacion: TButton;
    btnEliminarMedioComunicacion: TButton;
    procedure dblMediosDblClick(Sender: TObject);
    procedure cdsMediosComunicacionAfterDelete(DataSet: TDataSet);
    procedure btnAgregarMedioComunicacionClick(Sender: TObject);
    procedure dblMediosDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: String; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure btnEditarMedioComunicacionClick(Sender: TObject);
    procedure btnEliminarMedioComunicacionClick(Sender: TObject);
  private
    { Private declarations }
    FCodigoTipoDomicilio:Integer;
    FDomicilios:TClientDataSet;
    function GetMediosCargados:TClientDataSet;
    function GetTieneMediosCargados:Boolean;
    function GetMediosBorrados:TClientDataSet;
    function GetTieneMediosBorrados:Boolean;
    procedure SetFDomicilio(Sender:TClientDataSet);
  public
    { Public declarations }
    property MediosCargados: TClientDataSet read GetMediosCargados;
    property TieneMediosCargados:Boolean read GetTieneMediosCargados;
    property MediosBorrados:TClientDataSet read GetMediosBorrados;
    property TieneMediosBorrados:Boolean read GetTieneMediosBorrados;
    property Domicilio:TClientDataSet read  FDomicilios write SetFDomicilio;
    function inicializar(CodigoTipoDomicilio:Integer=1):boolean;
    function CargarMediosContactosPersona(CodigoPersona: integer):Boolean;
  end;

implementation



{$R *.dfm}

procedure TFrameListMediosContactos.dblMediosDblClick(
  Sender: TObject);
begin
    if btnEditarMedioComunicacion.Enabled then btnEditarMedioComunicacion.Click;
end;

procedure TFrameListMediosContactos.cdsMediosComunicacionAfterDelete(
  DataSet: TDataSet);
begin
    btnEditarMedioComunicacion.Enabled := ((cdsMediosComunicacion.Active) and (cdsMediosComunicacion.RecordCount > 0));
    btnEliminarMedioComunicacion.Enabled    := btnEditarMedioComunicacion.Enabled;
end;

function TFrameListMediosContactos.GetMediosCargados: TClientDataSet;
begin
    result:=cdsMediosComunicacion;
end;

function TFrameListMediosContactos.GetTieneMediosCargados:Boolean;
begin
    result:=((cdsMediosComunicacion.Active) and (cdsMediosComunicacion.RecordCount > 0));
end;

function TFrameListMediosContactos.GetMediosBorrados: TClientDataSet;
begin
    result:=cdsMediosComunicacionBorrados;
end;

function TFrameListMediosContactos.GetTieneMediosBorrados: Boolean;
begin
    result:=((cdsMediosComunicacionBorrados.Active) and (cdsMediosComunicacionBorrados.RecordCount > 0));
end;


procedure TFrameListMediosContactos.btnAgregarMedioComunicacionClick(
  Sender: TObject);
var
	f: TFormEditarMedioComunicacion;
begin
    Application.CreateForm(TFormEditarMedioComunicacion, f);
    try
        if f.Inicializar(1,0,'',nulldate,nulldate,'','',0,FDomicilios) and (f.ShowModal = mrOK) then begin
            try
                with dsMediosComunicacion.DataSet do begin
                    Append;

                    FieldByName('IndiceMedioComunicacion').AsInteger:=-1;
                    FieldByName('CodigoTipoMedioContacto').AsInteger:=f.CodigoMedioContacto;
                    FieldByName('Formato').AsString:='';
                    FieldByName('DescriTipoMedioContacto').AsString:=f.DescripcionMedioContacto;
                    FieldByName('CodigoArea').AsInteger:=f.CodigoArea;
                    FieldByName('Valor').AsString:=f.Valor;
                    FieldByName('Anexo').AsString:=f.Anexo;
                    FieldByName('CodigoDomicilio').AsInteger:=f.CodigoDomicilio;
                    FieldByName('DescriDomicilio').AsString:=f.DescriDomicilio;
                    FieldByName('Detalle').AsString:=f.Detalle;
                    FieldByName('HorarioDesde').AsDateTime:=f.HoraDesde;
                    FieldByName('HorarioHasta').AsDateTime:=f.HoraHasta;
                    FieldByName('Observaciones').AsString:=f.Observaciones;
                    FieldByName('CodigoDomicilioRecNo').AsInteger:=f.CodigoDomicilioRecNo;

                    Post;
            end;
            except
                On E: Exception do begin
                    MsgBoxErr( Format(MSG_ERROR_AGREGAR,[FLD_MEDIO_CONTACTO]), E.message, Format(MSG_CAPTION_AGREGAR,[FLD_MEDIO_CONTACTO]), MB_ICONSTOP);
                    dsMediosComunicacion.DataSet.Cancel;
                end;
            end;
        end;
    finally
        f.Release;
        dblMedios.SetFocus;
    end;

end;


function TFrameListMediosContactos.inicializar(CodigoTipoDomicilio:Integer=1):boolean;
begin
    FCodigoTipoDomicilio:=CodigoTipoDomicilio;
    cdsMediosComunicacion.Close;
    cdsMediosComunicacion.CreateDataSet;
    cdsMediosComunicacionBorrados.close;
    cdsMediosComunicacionBorrados.CreateDataSet;
    cdsMediosComunicacion.AfterOpen(cdsMediosComunicacion);
    result:=True;
end;

procedure TFrameListMediosContactos.dblMediosDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: String; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
    with dblMedios.DataSource.DataSet do begin
        if Column.Index = 3 then Text := Formatdatetime('hh:nn', FieldByName('HorarioDesde').AsDateTime);
        if Column.Index = 4 then Text := iif(Formatdatetime('hh:nn', FieldByName('HorarioHasta').AsDateTime)='23:59','24:00',Formatdatetime('hh:nn', FieldByName('HorarioHasta').AsDateTime));
    end;
end;

procedure TFrameListMediosContactos.btnEditarMedioComunicacionClick(
  Sender: TObject);
var
	f: TFormEditarMedioComunicacion;
begin
    Application.CreateForm(TFormEditarMedioComunicacion, f);
    try
        with dsMediosComunicacion.DataSet do begin
            if f.Inicializar(FieldByName('CodigoTipoMedioContacto').AsInteger,
                              FieldByName('CodigoArea').AsInteger,
                              FieldByName('Valor').AsString,
                              FieldByName('HorarioDesde').AsDateTime,
                              FieldByName('HorarioHasta').AsDateTime,
                              FieldByName('Anexo').AsString,
                              FieldByName('Observaciones').AsString,
                              FieldByName('CodigoDomicilioRecNo').AsInteger,
                              FDomicilios)
             and (f.ShowModal = mrOK) then begin
                try
                    edit;

                    //FieldByName('IndiceMedioComunicacion').AsInteger:=-1
                    FieldByName('CodigoTipoMedioContacto').AsInteger:=f.CodigoMedioContacto;
                    FieldByName('Formato').AsString:='';
                    FieldByName('DescriTipoMedioContacto').AsString:=f.DescripcionMedioContacto;
                    FieldByName('CodigoArea').AsInteger:=f.CodigoArea;
                    FieldByName('Valor').AsString:=f.Valor;
                    FieldByName('Anexo').AsString:=f.Anexo;
                    FieldByName('CodigoDomicilio').AsInteger:=f.CodigoDomicilio;
                    FieldByName('DescriDomicilio').AsString:=f.DescriDomicilio;
                    FieldByName('Detalle').AsString:=f.Detalle;
                    FieldByName('HorarioDesde').AsDateTime:=f.HoraDesde;
                    FieldByName('HorarioHasta').AsDateTime:=f.HoraHasta;
                    FieldByName('Observaciones').AsString:=f.Observaciones;
                    FieldByName('CodigoDomicilioRecNo').AsInteger:=f.CodigoDomicilioRecNo;

                    Post;
                except
                    On E: Exception do begin
                        MsgBoxErr( Format(MSG_ERROR_ACTUALIZAR,[FLD_MEDIO_CONTACTO]), E.message, Format(MSG_CAPTION_AGREGAR,[FLD_MEDIO_CONTACTO]), MB_ICONSTOP);
                        dsMediosComunicacion.DataSet.Cancel;
                    end;
                end;
            end;
        end;
    finally
        f.Release;
        dblMedios.SetFocus;
    end;

end;

procedure TFrameListMediosContactos.btnEliminarMedioComunicacionClick(Sender: TObject);
begin
	If MsgBox(Format(MSG_QUESTION_ELIMINAR,[FLD_MEDIO_CONTACTO]), Format(MSG_CAPTION_ELIMINAR,[FLD_MEDIO_CONTACTO]), MB_YESNO + MB_ICONQUESTION) = IDYES then begin
		try
            with dsMediosComunicacion.DataSet do begin
                cdsMediosComunicacionBorrados.Append;

                cdsMediosComunicacionBorrados.FieldByName('IndiceMedioComunicacion').AsInteger:=FieldByName('IndiceMedioComunicacion').AsInteger;
                cdsMediosComunicacionBorrados.FieldByName('CodigoTipoMedioContacto').AsInteger:=FieldByName('CodigoTipoMedioContacto').AsInteger;
                cdsMediosComunicacionBorrados.FieldByName('Formato').AsString:=FieldByName('Formato').AsString;
                cdsMediosComunicacionBorrados.FieldByName('DescriTipoMedioContacto').AsString:=FieldByName('DescriTipoMedioContacto').AsString;
                cdsMediosComunicacionBorrados.FieldByName('CodigoArea').AsInteger:=FieldByName('CodigoArea').AsInteger;
                cdsMediosComunicacionBorrados.FieldByName('Valor').AsString:=FieldByName('Valor').AsString;
                cdsMediosComunicacionBorrados.FieldByName('Anexo').AsString:=FieldByName('Anexo').AsString;
                cdsMediosComunicacionBorrados.FieldByName('CodigoDomicilio').AsInteger:=FieldByName('CodigoDomicilio').AsInteger;
                cdsMediosComunicacionBorrados.FieldByName('DescriDomicilio').AsString:=FieldByName('DescriDomicilio').AsString;
                cdsMediosComunicacionBorrados.FieldByName('Detalle').AsString:=FieldByName('Detalle').AsString;
                cdsMediosComunicacionBorrados.FieldByName('HorarioDesde').AsDateTime:=FieldByName('HorarioDesde').AsDateTime;
                cdsMediosComunicacionBorrados.FieldByName('HorarioHasta').AsDateTime:=FieldByName('HorarioHasta').AsDateTime;
                cdsMediosComunicacionBorrados.FieldByName('Observaciones').AsString:=FieldByName('Observaciones').AsString;
                cdsMediosComunicacionBorrados.FieldByName('CodigoDomicilioRecNo').AsInteger:=FieldByName('CodigoDomicilioRecNo').AsInteger;


                cdsMediosComunicacionBorrados.Post;
                Delete;
                dblMedios.SetFocus;
            end;
		Except
			On E: Exception do begin
				dsMediosComunicacion.DataSet.Cancel;
                cdsMediosComunicacionBorrados.Cancel;
				MsgBoxErr( Format(MSG_ERROR_ELIMINAR,[FLD_MEDIO_CONTACTO]), e.message, Format(MSG_CAPTION_ELIMINAR,[FLD_MEDIO_CONTACTO]), MB_ICONSTOP);
                dblMedios.SetFocus;
			end;
		end;
	end;

end;

function TFrameListMediosContactos.CargarMediosContactosPersona(CodigoPersona: integer):Boolean;
begin
    try
        ObtenerMediosComunicacionPersona.close;
        ObtenerMediosComunicacionPersona.Parameters.ParamByName('@CodigoPersona').Value:=CodigoPersona;
        ObtenerMediosComunicacionPersona.Open;

        cdsMediosComunicacion.Close;
        cdsMediosComunicacion.CreateDataSet;
        cdsMediosComunicacionBorrados.Close;
        cdsMediosComunicacionBorrados.CreateDataSet;

        cdsMediosComunicacion.Data := dspMediosComunicacion.Data;
        ObtenerMediosComunicacionPersona.Close;
        cdsMediosComunicacion.AfterOpen(cdsMediosComunicacion);

        Result:=True;
    except
    	on E: Exception do begin
            result:=false;
        	MsgBoxErr(format(MSG_ERROR_INICIALIZACION,[FLD_MEDIO_CONTACTO]), e.Message, format(MSG_CAPTION_LIST,[FLD_MEDIO_CONTACTO]), MB_ICONSTOP);
        end;
    end;

end;

procedure TFrameListMediosContactos.SetFDomicilio(Sender: TClientDataSet);
begin
    FDomicilios:=Sender;
end;

end.
