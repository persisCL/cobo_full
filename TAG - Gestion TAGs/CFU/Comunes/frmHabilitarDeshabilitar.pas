unit frmHabilitarDeshabilitar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Util, Utildb,Utilproc, DB, ADODB,
  PeaProcs, DMConnection, DmiCtrls, BuscaClientes, DPSControls, peatypes;

type
  TfrmHabilitarDeshabilitarTag = class(TForm)
    Habilitar: TADOStoredProc;
    Inhabilitar: TADOStoredProc;
    lbl_tag_cliente: TLabel;
    txt_tag_cliente: TPickEdit;
    Label31: TLabel;
    Label32: TLabel;
    cb_tag_cuenta: TComboBox;
    lbl_tag: TLabel;
    cb_motivos: TComboBox;
    Label2: TLabel;
    Label3: TLabel;
    Bevel1: TBevel;
    btn_aceptar: TDPSButton;
    btn_cancelar: TDPSButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btn_cancelarClick(Sender: TObject);
    procedure btn_aceptarClick(Sender: TObject);
    procedure txt_tag_clienteButtonClick(Sender: TObject);
    procedure txt_tag_clienteChange(Sender: TObject);
    procedure cb_tag_cuentaChange(Sender: TObject);
  private
    Actividad: Boolean;
    CodigoCuenta : Integer;
    procedure OcultarControlesInhabilitar;
    function ObtenerNumeroTag(Cuenta: Integer):AnsiString;
    function IngresoOk: boolean;
  public
    function inicializa(OrdenServicio: integer; Habilitar: Boolean):Boolean;
  end;

var
  frmHabilitarDeshabilitarTag: TfrmHabilitarDeshabilitarTag;

implementation

{$R *.dfm}

{ TfrmHabiltarDeshabilitarTag }

function TfrmHabilitarDeshabilitarTag.inicializa(
  OrdenServicio: integer; Habilitar: Boolean): Boolean;
resourcestring
    MSG_ORDEN_SERVICIO      = 'El tipo de Orden de Servicio no puede ser resuelto mediante esta pantalla.';
    CAPTION_HABILITACION    = 'Habilitación de TAGs';
    CAPTION_INHABILITACION  = 'Inhabilitación de TAGs';
var
	CodigoWorkFlow: integer;
    FCodigoCliente: integer;
begin
	Result := False;
	//Obtengo los Datos que necesito
    if OrdenServicio > -1 then begin
    	CodigoWorkFlow := QueryGetValueInt(DMConnections.BaseCAC,
        	'SELECT CodigoWorkFlow FROM OrdenesServicio WHERE CodigoOrdenServicio = ' + IntToStr(OrdenServicio));
    	if (CodigoWorkFlow <> 1) and (CodigoWorkFlow <> 6) then begin
	    	MsgBox(MSG_ORDEN_SERVICIO, iif(Habilitar, CAPTION_HABILITACION, CAPTION_INHABILITACION), MB_ICONSTOP);
    	    Exit;
        end else begin
    	    Habilitar := iif(CodigoWorkFLow = 1, False, True);
        end;
    end;

	FCodigoCliente	:= QueryGetValueInt(DMConnections.BaseCAC,
    	'SELECT CodigoPersona As CodigoCliente FROM OrdenesServicio, Comunicaciones ' +
        ' WHERE CodigoOrdenServicio = ' + IntToStr(OrdenServicio) +
        ' AND Comunicaciones.CodigoComunicacion = OrdenesServicio.CodigoComunicacion') ;
	//FCodigoCuenta	= QueryGetValue(DMConnections.BaseCAC, 'SELECT CodigoCliente WHERE CodigoOrdenesServicio = ' + IntToStr(CodigoOrdenServicio));

    if Habilitar then begin
        OcultarControlesInhabilitar;
        Self.Caption := CAPTION_HABILITACION;
        Actividad := true; // lo que estoy haciendo es una habilitacion
    end else begin
        Self.Caption := CAPTION_INHABILITACION;
//        CargarMotivosBaja(DMCOnnections.BaseCac,cb_motivos,1);
        Actividad := false; // lo que estoy haciendo es una inhabilitacion
    end;
    if FCodigoCliente > 0 then begin
    	txt_Tag_Cliente.Value := FCodigoCliente;
	    txt_Tag_Cliente.OnChange(txt_Tag_Cliente);
        ActiveControl := cb_tag_Cuenta;
    end else begin
	    ActiveControl := txt_tag_cliente;
    end;
    Result := true;
end;

procedure TfrmHabilitarDeshabilitarTag.OcultarControlesInhabilitar;
begin
    cb_motivos.Enabled := false;
    label2.Enabled := false;
    label3.Enabled := false;
end;

procedure TfrmHabilitarDeshabilitarTag.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TfrmHabilitarDeshabilitarTag.btn_cancelarClick(Sender: TObject);
begin
    close;
end;

procedure TfrmHabilitarDeshabilitarTag.btn_aceptarClick(Sender: TObject);
resourcestring
    MSG_TAG_YA_HABILITADO  = 'El número de TAG indicado ya esta habilitado.';
    MSG_TAG_HABILITADO     = 'El TAG Indicado fue Exitosamente Habilitado.';
    MSG_ERROR_HABILITACION = 'No se ha podido Habilitar el TAG Indicado.';
    MSG_TAG_INHABILITADO      = 'El TAG Indicado fue Exitosamente Inhabilitado.';
    MSG_ERROR_INHABILITACION  = 'No se ha podido Inhabilitar el TAG Indicado.';
    MSG_SELECCIONE_MOTIVO  = 'Seleccione el motivo de baja.';
    CAPTION_GRABAR = 'Grabar';
    MSG_FALTA_CLIENTE = 'Debe ingresar el cliente.';
    MSG_FALTA_CUENTA  = 'Debe elegir la cuenta.';
begin
	if not ValidateControls([txt_tag_cliente, cb_tag_cuenta],
      [(txt_tag_cliente.text <> ''),
      (cb_tag_cuenta.itemindex > -1)],
      CAPTION_GRABAR,
      [MSG_FALTA_CLIENTE, MSG_FALTA_CUENTA]) then begin
		Exit;
	end;

    screen.Cursor := crHourGlass;
	if actividad then begin //HABILITACION
		if QueryGetValueInt(DMConnections.BaseCac,'SELECT 1 FROM TAGSASIGNADOS WHERE '+
                'CONTEXTMARK = ' + QueryGetvalue(DMConnections.BaseCAC,
                    'select dbo.ObtenerContextMark(' + IntToStr(CodigoCuenta) + ')' ) + ' AND ' +
                'CONTRACTSERIALNUMBER = '+  QueryGetvalue(DMConnections.BaseCAC,
                    'select dbo.ObtenerContractSerialNumber(' + IntToStr(CodigoCuenta) + ')' ) + ' AND ' +
				'FECHAHORABAJA IS  NULL') = 1 then begin
                Screen.Cursor := crDefault;
				msgBox(MSG_TAG_YA_HABILITADO, Self.Caption, MB_ICONSTOP);
             end else begin
                 try
                      with habilitar do begin
                          Parameters.ParamByName('@CONTEXTMARK').Value := QueryGetValueInt(DMConnections.BaseCAC,
                            'select dbo.ObtenerContextmark(' + IntToStr(CodigoCuenta) + ')' );
                          Parameters.ParamByName('@CONTRACTSERIALNUMBER').Value := StrToFloat(QueryGetValue(DMConnections.BaseCAC,
                            'select dbo.ObtenerContractSerialNumber(' + IntToStr(CodigoCuenta) + ')' ));
                          ExecProc;
                      end;
                      MsgBox(MSG_TAG_HABILITADO, self.Caption, MB_ICONINFORMATION);
                      txt_tag_cliente.SetFocus;
                 except
                    on e: Exception do begin
                        Screen.Cursor := crDefault;
                        MsgBoxErr(MSG_ERROR_HABILITACION, e.Message, self.caption, MB_ICONSTOP);
                        Exit;
                    end;
                 end;
             end;
    end else begin //INHABILITACION
        if cb_motivos.ItemIndex < 0 then begin
            Screen.Cursor := crDefault;
            msgBox(MSG_SELECCIONE_MOTIVO, self.Caption, MB_ICONSTOP);
        end;
        if QueryGetValueInt(DMConnections.BaseCac,'SELECT 1 FROM TAGSASIGNADOS WHERE '+
                'CONTEXTMARK = ' + QueryGetvalue(DMConnections.BaseCAC,
                    'select dbo.ObtenerContextMark(' + IntToStr(CodigoCuenta) + ')' ) + ' AND ' +
                'CONTRACTSERIALNUMBER = '+  QueryGetvalue(DMConnections.BaseCAC,
                    'select dbo.ObtenerContractSerialNumber(' + IntToStr(CodigoCuenta) + ')' ) + ' AND ' +
                'FECHAHORABAJA IS NOT NULL') = 1 then begin
                Screen.Cursor := crDefault;
                msgBox(MSG_ERROR_INHABILITACION, self.Caption, MB_ICONSTOP);
        end else begin
                try
                    with Inhabilitar do begin
                        Parameters.ParamByName('@CONTEXTMARK').Value := QueryGetValueInt(DMConnections.BaseCAC,
                            'select dbo.ObtenerContextmark(' + IntToStr(CodigoCuenta) + ')' );
                        Parameters.ParamByName('@CONTRACTSERIALNUMBER').Value := StrToFloat(QueryGetValue(DMConnections.BaseCAC,
                            'select dbo.ObtenerContractSerialNumber(' + IntToStr(CodigoCuenta) + ')' ));
                        Parameters.ParamByName('@Motivo').Value      := Ival(Trim(Copy(cb_motivos.Text,200,100)));
                        ExecProc;
                    end;
                    MsgBox(MSG_TAG_INHABILITADO, self.caption, MB_ICONINFORMATION);
                    cb_motivos.ItemIndex := 0;
                    txt_tag_cliente.SetFocus;
                except
                    on e: Exception do begin
                        Screen.Cursor := crDefault;
                        MsgBoxErr(MSG_ERROR_INHABILITACION, e.Message, self.caption, MB_ICONSTOP);
                        Exit;
                    end;
                end;
             end;
    end;
    txt_tag_cliente.Clear;
    cb_tag_cuenta.Clear;
    Screen.Cursor := crDefault;
    btn_Aceptar.Enabled := false;
    ModalResult := mrOK;
end;

procedure TfrmHabilitarDeshabilitarTag.txt_tag_clienteButtonClick(
  Sender: TObject);
Var
	f: TFormBuscaClientes;
begin
	Application.CreateForm(TFormBuscaClientes, f);
(*
	if f.Inicializa('', '', '', '', '', TBP_CLIENTES) and (f.ShowModal = mrOk) then begin
		txt_tag_cliente.Value := f.Persona.CodigoPersona;
	end;
*)
	f.Release;
end;

procedure TfrmHabilitarDeshabilitarTag.txt_tag_clienteChange(
  Sender: TObject);
begin
    // Mostramos el nombre del cliente
	lbl_tag_cliente.caption := QueryGetValue(DMConnections.BaseCac,
	  'select dbo.ObtenerApellidoNombreCliente(' + FloatToStr(txt_tag_cliente.Value) + ')' );
	// Cargamos las cuentas
	CargarCuentas(DMConnections.BaseCaC, cb_tag_cuenta,Round(txt_tag_cliente.Value));
	cb_tag_cuenta.ItemIndex := 0;
    cb_tag_cuenta.OnChange(self);
    btn_aceptar.Enabled := IngresoOk;
end;

function TfrmHabilitarDeshabilitarTag.ObtenerNumeroTag(
  Cuenta: Integer): AnsiString;
begin
    Result := QueryGetValue(DMConnections.BaseCac,'SELECT dbo.ObtenerNumeroTagAsString(' + IntToStr(Cuenta)+')' );
end;

procedure TfrmHabilitarDeshabilitarTag.cb_tag_cuentaChange(
  Sender: TObject);
begin
    CodigoCuenta := Ival(Trim(Copy(cb_tag_cuenta.Text,201,100)));
    lbl_tag.caption := ObtenerNumeroTag(CodigoCuenta);
end;

function TfrmHabilitarDeshabilitarTag.IngresoOk: boolean;
begin
    result := (lbl_tag_cliente.caption <> '') and (cb_tag_cuenta.ItemIndex >= 0);
end;

end.
