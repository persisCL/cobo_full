unit FrmCancelarTarea;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, UtilProc, DB, ADODB, DPSControls;

type
  TFormCancelarTarea = class(TForm)
    Label1: TLabel;
    Memo1: TMemo;
    CancelarTarea: TADOStoredProc;
    OPButton1: TButton;
    OPButton2: TButton;
    procedure OPButton1Click(Sender: TObject);
  private
	{ Private declarations }
	FOrdenServicio, FWorkflow, FVersion, FTarea: Integer;
  public
	{ Public declarations }
	function Inicializa(OrdenServicio, Workflow, Version, Tarea: Integer): Boolean;
  end;

var
  FormCancelarTarea: TFormCancelarTarea;

implementation

Uses
	DMConnection;

{$R *.dfm}

function TFormCancelarTarea.Inicializa(OrdenServicio, Workflow, Version, Tarea: Integer): Boolean;
begin
	FOrdenServicio := OrdenServicio;
	FWorkflow := Workflow;
	FVersion := Version;
	FTarea := Tarea;
	Result := True;
end;

procedure TFormCancelarTarea.OPButton1Click(Sender: TObject);
resourcestring
    MSG_COMENTARIO = 'Debe indicar alg�n comentario explicando el motivo por el cual cancela esta tarea.';
    CAPTION_CANCELAR_TAREA = 'Cancelar Tarea';

begin
	if Trim(Memo1.Lines.Text) = '' then begin
		MsgBox(MSG_COMENTARIO, CAPTION_CANCELAR_TAREA, MB_ICONSTOP);
		ActiveControl := Memo1;
		Exit;
	end;
	With CancelarTarea do begin
		Parameters.ParamByName('@CodigoOrdenServicio').Value := FOrdenServicio;
		Parameters.ParamByName('@CodigoWorkflow').Value := FWorkflow;
		Parameters.ParamByName('@Version').Value := FVersion;
		Parameters.ParamByName('@CodigoTarea').Value := FTarea;
		Parameters.ParamByName('@Observaciones').Value := Memo1.Lines.Text;
		try
			Screen.Cursor := crHourGlass;
			ExecProc;
		finally
			Screen.Cursor := crDefault;
		end;
	end;
	ModalResult := mrOk;
end;

end.
