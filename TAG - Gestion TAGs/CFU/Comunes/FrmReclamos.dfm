object FReclamos: TFReclamos
  Left = 39
  Top = 104
  Caption = 'Ver / Editar Reclamos'
  ClientHeight = 508
  ClientWidth = 862
  Color = clBtnFace
  Constraints.MinHeight = 458
  Constraints.MinWidth = 870
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDesktopCenter
  Visible = True
  OnClose = FormClose
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object PBotones: TPanel
    Left = 0
    Top = 467
    Width = 862
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object LCantidadTotal: TLabel
      Left = 645
      Top = 16
      Width = 3
      Height = 13
      Alignment = taRightJustify
    end
    object PBotonesDerecha: TPanel
      Left = 677
      Top = 0
      Width = 185
      Height = 41
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object EditarBTN: TButton
        Left = 22
        Top = 8
        Width = 75
        Height = 25
        Caption = 'Editar'
        TabOrder = 1
        OnClick = EditarBTNClick
      end
      object SalirBTN: TButton
        Left = 102
        Top = 8
        Width = 75
        Height = 25
        Caption = 'Salir'
        TabOrder = 0
        OnClick = SalirBTNClick
      end
    end
  end
  object DBLReclamos: TDBListEx
    Left = 0
    Top = 170
    Width = 862
    Height = 297
    Align = alClient
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 20
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
      end
      item
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 50
        Header.Caption = 'Orden'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taRightJustify
        IsLink = False
        OnHeaderClick = DBLReclamosColumns0HeaderClick
        FieldName = 'CodigoOrdenServicio'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 160
        Header.Caption = 'Reclamo'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        OnHeaderClick = DBLReclamosColumns0HeaderClick
        FieldName = 'Descripcion'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 100
        Header.Caption = 'F. Creaci'#243'n'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = DBLReclamosColumns0HeaderClick
        FieldName = 'FechaHoraCreacion'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 95
        Header.Caption = 'F. Compromiso'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = DBLReclamosColumns0HeaderClick
        FieldName = 'FechaCompromiso'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 100
        Header.Caption = 'F. Cierre'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = DBLReclamosColumns0HeaderClick
        FieldName = 'FechaHoraFin'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 80
        Header.Caption = 'RUT Cliente'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = DBLReclamosColumns0HeaderClick
        FieldName = 'Rut'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 120
        Header.Caption = 'N'#250'mero Convenio'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = DBLReclamosColumns0HeaderClick
        FieldName = 'NumeroConvenio'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 60
        Header.Caption = 'Prioridad'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = DBLReclamosColumns0HeaderClick
        FieldName = 'Prioridad'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 60
        Header.Caption = 'Estado'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = DBLReclamosColumns0HeaderClick
        FieldName = 'DescEstado'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Concesionaria'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'NombreCorto'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Sub Tipo Reclamo'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'DescripcionSubTipoOrdenServicio'
      end>
    DataSource = DataSource
    DragReorder = True
    ParentColor = False
    TabOrder = 1
    TabStop = True
    OnDblClick = DBLReclamosDblClick
    OnDrawText = DBLReclamosDrawText
    OnMouseMove = DBLReclamosMouseMove
  end
  object GBCriterios: TGroupBox
    Left = 0
    Top = 0
    Width = 862
    Height = 170
    Align = alTop
    Caption = 'Criterios de B'#250'squeda  '
    TabOrder = 0
    object LFecha: TLabel
      Left = 251
      Top = 32
      Width = 33
      Height = 13
      Caption = 'Fecha:'
    end
    object LRut: TLabel
      Left = 15
      Top = 60
      Width = 78
      Height = 13
      Caption = 'RUT del Cliente:'
    end
    object LNumeroConvenio: TLabel
      Left = 252
      Top = 59
      Width = 48
      Height = 13
      Caption = 'Convenio:'
    end
    object LNumeroReclamo: TLabel
      Left = 15
      Top = 32
      Width = 75
      Height = 13
      Caption = 'N'#176' de Reclamo:'
    end
    object Ldesde: TLabel
      Left = 554
      Top = 13
      Width = 67
      Height = 13
      Caption = 'Desde Fecha '
    end
    object LHasta: TLabel
      Left = 651
      Top = 13
      Width = 61
      Height = 13
      Caption = 'Hasta Fecha'
    end
    object LTipoReclamo: TLabel
      Left = 15
      Top = 89
      Width = 69
      Height = 13
      Caption = 'Tipo Reclamo:'
    end
    object LPrioridad: TLabel
      Left = 554
      Top = 62
      Width = 44
      Height = 13
      Caption = 'Prioridad:'
    end
    object LLimitar: TLabel
      Left = 554
      Top = 90
      Width = 42
      Height = 13
      Caption = 'Limitar a:'
    end
    object LEstado: TLabel
      Left = 16
      Top = 118
      Width = 36
      Height = 13
      Caption = 'Estado:'
    end
    object LFuenteDelReclamo: TLabel
      Left = 252
      Top = 92
      Width = 98
      Height = 13
      Caption = 'Fuente del Reclamo:'
    end
    object lblConcesionaria: TLabel
      Left = 17
      Top = 144
      Width = 70
      Height = 13
      Caption = 'Concesionaria:'
    end
    object lblSubTipo: TLabel
      Left = 251
      Top = 143
      Width = 88
      Height = 13
      Caption = 'SubTipo Reclamo:'
    end
    object EFechaDesde: TDateEdit
      Left = 554
      Top = 29
      Width = 90
      Height = 21
      AutoSelect = False
      TabOrder = 4
      Date = -693594.000000000000000000
    end
    object EFechaHasta: TDateEdit
      Left = 651
      Top = 29
      Width = 90
      Height = 21
      AutoSelect = False
      TabOrder = 5
      Date = -693594.000000000000000000
    end
    object PCriterios: TPanel
      Left = 778
      Top = 15
      Width = 82
      Height = 153
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 11
      object BuscarBTN: TButton
        Left = 3
        Top = 13
        Width = 69
        Height = 25
        Caption = 'Buscar'
        Default = True
        TabOrder = 0
        OnClick = BuscarBTNClick
      end
      object LimpiarBTN: TButton
        Left = 3
        Top = 40
        Width = 69
        Height = 25
        Caption = 'Limpiar'
        TabOrder = 1
        OnClick = LimpiarBTNClick
      end
    end
    object EPrioridad: TVariantComboBox
      Left = 620
      Top = 59
      Width = 121
      Height = 21
      Style = vcsDropDownList
      DropDownCount = 10
      ItemHeight = 13
      TabOrder = 7
      OnKeyDown = EPrioridadKeyDown
      Items = <
        item
          Caption = '(Seleccionar)'
          Value = '0'
        end
        item
          Caption = 'Muy Alta'
          Value = 1
        end
        item
          Caption = 'Alta'
          Value = 2
        end
        item
          Caption = 'Media'
          Value = 3
        end
        item
          Caption = 'Baja'
          Value = 4
        end>
    end
    object CKcerrado: TCheckBox
      Left = 690
      Top = 90
      Width = 143
      Height = 17
      Caption = 'Incluir Reclamos Cerrados'
      TabOrder = 10
    end
    object peNumeroDocumento: TPickEdit
      Left = 97
      Top = 56
      Width = 140
      Height = 21
      CharCase = ecUpperCase
      Enabled = True
      TabOrder = 1
      OnChange = ERutChange
      EditorStyle = bteTextEdit
      OnButtonClick = peNumeroDocumentoButtonClick
    end
    object txt_Top: TNumericEdit
      Left = 620
      Top = 88
      Width = 57
      Height = 21
      TabOrder = 9
    end
    object EnumeroReclamo: TNumericEdit
      Left = 97
      Top = 28
      Width = 138
      Height = 21
      TabOrder = 0
    end
    object EEstado: TVariantComboBox
      Left = 96
      Top = 113
      Width = 140
      Height = 21
      Style = vcsDropDownList
      DropDownCount = 10
      ItemHeight = 13
      TabOrder = 8
      OnKeyDown = EEstadoKeyDown
      OnSelect = EEstadoSelect
      Items = <
        item
          Caption = '(Seleccionar)'
          Value = '0'
        end
        item
          Caption = 'Pendiente'
          Value = 1
        end
        item
          Caption = 'P.Resp.Int'
          Value = '2'
        end
        item
          Caption = 'P.Resp.Ext'
          Value = '3'
        end
        item
          Caption = 'Resuelta'
          Value = 4
        end
        item
          Caption = 'Cancelada'
          Value = 5
        end>
    end
    object EtipoFecha: TVariantComboBox
      Left = 352
      Top = 28
      Width = 194
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 2
      OnKeyDown = EtipoFechaKeyDown
      Items = <
        item
          Caption = '(Seleccionar)'
          Value = '(Seleccionar)'
        end
        item
          Caption = 'Creaci'#243'n'
          Value = 'Creaci'#243'n'
        end
        item
          Caption = 'Compromiso'
          Value = 'Compromiso'
        end
        item
          Caption = 'Cierre'
          Value = 'Cierre'
        end>
    end
    object ETipoReclamo: TVariantComboBox
      Left = 97
      Top = 86
      Width = 140
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 3
      OnKeyDown = ETipoReclamoKeyDown
      Items = <>
    end
    object EFuenteReclamo: TVariantComboBox
      Left = 352
      Top = 88
      Width = 194
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 12
      OnKeyDown = EFuenteReclamoKeyDown
      Items = <>
    end
    object PAdicional: TPanel
      Left = 251
      Top = 113
      Width = 497
      Height = 21
      BevelOuter = bvNone
      TabOrder = 13
      object Label2: TLabel
        Left = 2
        Top = 5
        Width = 72
        Height = 13
        Caption = 'F. Compromiso:'
      end
      object Label3: TLabel
        Left = 303
        Top = 5
        Width = 39
        Height = 13
        Caption = 'Entidad:'
      end
      object EFechaCompromiso: TDateEdit
        Left = 101
        Top = 0
        Width = 194
        Height = 21
        AutoSelect = False
        TabOrder = 0
        Date = -693594.000000000000000000
      end
      object EEntidad: TVariantComboBox
        Left = 369
        Top = 0
        Width = 107
        Height = 21
        Style = vcsDropDownList
        DropDownCount = 10
        ItemHeight = 13
        TabOrder = 1
        OnDropDown = EEntidadDropDown
        Items = <
          item
            Caption = 'Ninguno'
            Value = '0'
          end>
      end
    end
    object vcbConcesionarias: TVariantComboBox
      Left = 96
      Top = 140
      Width = 141
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 14
      Items = <>
    end
    object vcbSubTipoReclamo: TVariantComboBox
      Left = 352
      Top = 140
      Width = 194
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 15
      Items = <>
    end
    object enumeroconvenio: TVariantComboBox
      Left = 352
      Top = 56
      Width = 194
      Height = 19
      Style = vcsOwnerDrawFixed
      ItemHeight = 13
      TabOrder = 6
      OnDrawItem = enumeroconvenioDrawItem
      Items = <>
    end
  end
  object SpObtenerReclamos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerReclamos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@DesdeFecha'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@HastaFecha'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Limite'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Creacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Compromiso'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Cierre'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Pendiente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Descripcion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@Prioridad'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoFuenteReclamo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaCompromisoInterna'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoEntidad'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Estado'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@CodigoOrdenServicio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@NumeroConvenio'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@CantidadTotal'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoConcesionaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@SubTipoOrdenServicio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 776
    Top = 133
    object SpObtenerReclamosCodigoOrdenServicio: TAutoIncField
      FieldName = 'CodigoOrdenServicio'
      ReadOnly = True
    end
    object SpObtenerReclamosDescripcion: TStringField
      FieldName = 'Descripcion'
      Size = 255
    end
    object SpObtenerReclamosFechaHoraCreacion: TDateTimeField
      FieldName = 'FechaHoraCreacion'
    end
    object SpObtenerReclamosFechaCompromiso: TDateTimeField
      FieldName = 'FechaCompromiso'
    end
    object SpObtenerReclamosFechaHoraFin: TDateTimeField
      FieldName = 'FechaHoraFin'
    end
    object SpObtenerReclamosRut: TStringField
      FieldName = 'Rut'
      FixedChar = True
      Size = 11
    end
    object SpObtenerReclamosNumeroConvenio: TStringField
      FieldName = 'NumeroConvenio'
      ReadOnly = True
      FixedChar = True
      Size = 19
    end
    object SpObtenerReclamosPatente: TStringField
      FieldName = 'Patente'
      FixedChar = True
      Size = 10
    end
    object SpObtenerReclamosPrioridad: TWordField
      FieldName = 'Prioridad'
    end
    object SpObtenerReclamosDescEstado: TStringField
      FieldName = 'DescEstado'
      ReadOnly = True
      Size = 15
    end
    object SpObtenerReclamosUsuario: TStringField
      FieldName = 'Usuario'
      FixedChar = True
    end
    object SpObtenerReclamosEstado: TStringField
      FieldName = 'Estado'
      FixedChar = True
      Size = 1
    end
    object SpObtenerReclamosCodigoConcesionaria: TWordField
      FieldName = 'CodigoConcesionaria'
    end
    object SpObtenerReclamosSubTipoOrdenServicio: TIntegerField
      FieldName = 'SubTipoOrdenServicio'
    end
    object SpObtenerReclamosNombreCorto: TStringField
      FieldName = 'NombreCorto'
      FixedChar = True
      Size = 3
    end
    object SpObtenerReclamosDescripcionSubTipoOrdenServicio: TStringField
      FieldName = 'DescripcionSubTipoOrdenServicio'
      Size = 50
    end
  end
  object DataSource: TDataSource
    DataSet = SpObtenerReclamos
    Left = 808
    Top = 133
  end
  object SPObtenerConveniosxNumeroDocumento: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerConveniosxNumeroDocumento;1'
    Parameters = <>
    Left = 712
    Top = 133
  end
  object Timer: TTimer
    Interval = 100
    OnTimer = TimerTimer
    Left = 648
    Top = 135
  end
  object SpObtenerTiposOrdenServicioReclamo: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerTiposOrdenServicioReclamo;1'
    Parameters = <>
    Left = 744
    Top = 133
  end
  object SpObtenerEntidades: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerEntidades;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Tipo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end>
    Left = 680
    Top = 135
  end
end
