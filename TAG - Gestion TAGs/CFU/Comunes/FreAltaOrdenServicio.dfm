object FrameAltaOrdenServicio: TFrameAltaOrdenServicio
  Left = 0
  Top = 0
  Width = 1028
  Height = 746
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  Anchors = [akLeft, akTop, akRight, akBottom]
  AutoScroll = False
  Constraints.MinHeight = 325
  Constraints.MinWidth = 890
  TabOrder = 0
  object PageControl: TPageControl
    Left = 177
    Top = 0
    Width = 851
    Height = 746
    ActivePage = tab_cuentas
    Align = alClient
    Constraints.MinHeight = 325
    Constraints.MinWidth = 710
    TabOrder = 0
    object tab_cuentas: TTabSheet
      Caption = '&Cuentas'
      ImageIndex = 1
      TabVisible = False
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 843
        Height = 736
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object pc_Adhesion: TPageControl
          Left = 0
          Top = 0
          Width = 843
          Height = 736
          ActivePage = ts_DatosCliente
          Align = alClient
          TabIndex = 0
          TabOrder = 0
          object ts_DatosCliente: TTabSheet
            Caption = 'Datos del C&liente'
            DesignSize = (
              835
              708)
            object Label8: TLabel
              Left = 10
              Top = 103
              Width = 88
              Height = 13
              Caption = 'Pa'#237's de origen:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object lblNombre: TLabel
              Left = 10
              Top = 79
              Width = 117
              Height = 13
              Caption = 'Nombre de fantas'#237'a:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object lblApellido: TLabel
              Left = 10
              Top = 54
              Width = 80
              Height = 13
              Caption = 'Raz'#243'n Social:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label4: TLabel
              Left = 10
              Top = 31
              Width = 69
              Height = 13
              Caption = 'Documento:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label17: TLabel
              Left = 10
              Top = 127
              Width = 47
              Height = 13
              Caption = 'Actividad:'
            end
            object lblFechaNacCreacion: TLabel
              Left = 351
              Top = 104
              Width = 89
              Height = 13
              Caption = 'Fecha Nacimiento:'
            end
            object lblSexo: TLabel
              Left = 351
              Top = 32
              Width = 33
              Height = 13
              Caption = 'Sexo:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object lblApellidoMaterno: TLabel
              Left = 351
              Top = 55
              Width = 81
              Height = 13
              Caption = 'Apellido materno:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label53: TLabel
              Left = 10
              Top = 8
              Width = 67
              Height = 13
              Caption = 'Personer'#237'a:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label12: TLabel
              Left = 351
              Top = 79
              Width = 82
              Height = 13
              Caption = 'Situaci'#243'n de IVA:'
            end
            object txt_nombre: TEdit
              Left = 128
              Top = 75
              Width = 212
              Height = 21
              Color = 16444382
              TabOrder = 4
            end
            object txt_apellido: TEdit
              Left = 128
              Top = 51
              Width = 212
              Height = 21
              Color = 16444382
              TabOrder = 2
            end
            object txt_fechanacimiento: TDateEdit
              Left = 451
              Top = 99
              Width = 103
              Height = 21
              AutoSelect = False
              TabOrder = 9
              Date = -693594
            end
            object cb_sexo: TComboBox
              Left = 451
              Top = 27
              Width = 103
              Height = 21
              Style = csDropDownList
              Color = 16444382
              ItemHeight = 13
              ItemIndex = 0
              TabOrder = 8
              Text = 'Masculino'
              Items.Strings = (
                'Masculino'
                'Femenino')
            end
            object cb_pais: TComboBox
              Left = 128
              Top = 99
              Width = 212
              Height = 21
              Style = csDropDownList
              Color = 16444382
              ItemHeight = 13
              MaxLength = 4
              TabOrder = 6
            end
            object cbTipoNumeroDocumento: TMaskCombo
              Left = 128
              Top = 27
              Width = 212
              Height = 21
              Items = <>
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Color = 16444382
              ComboWidth = 103
              ItemIndex = -1
              OmitCharacterRequired = False
              TabOrder = 1
            end
            object txt_apellidoMaterno: TEdit
              Left = 451
              Top = 51
              Width = 219
              Height = 21
              TabOrder = 3
            end
            object cbPersoneria: TComboBox
              Left = 128
              Top = 3
              Width = 103
              Height = 21
              Style = csDropDownList
              Color = 16444382
              ItemHeight = 13
              TabOrder = 0
              OnChange = cbPersoneriaChange
              Items.Strings = (
                'F'#237'sica                          F'
                'Jur'#237'dica                       J')
            end
            object cbSituacionIVA: TComboBox
              Left = 451
              Top = 75
              Width = 219
              Height = 21
              Style = csDropDownList
              ItemHeight = 13
              TabOrder = 5
            end
            object cb_actividad: TComboBox
              Left = 128
              Top = 123
              Width = 212
              Height = 21
              Style = csDropDownList
              ItemHeight = 13
              MaxLength = 4
              TabOrder = 7
            end
            object GBMediosComunicacion: TGroupBox
              Left = 5
              Top = 247
              Width = 824
              Height = 122
              Anchors = [akLeft, akTop, akRight]
              Caption = 'Medios de comunicaci'#243'n'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 10
              object Label35: TLabel
                Left = 14
                Top = 80
                Width = 28
                Height = 13
                Caption = 'Email:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object txtEmailParticular: TEdit
                Left = 122
                Top = 72
                Width = 231
                Height = 21
                Hint = 'direcci'#243'n de e-mail'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
              end
              inline FrameTelefono1: TFrameTelefono
                Left = 8
                Top = 16
                Width = 701
                Height = 27
                TabOrder = 1
              end
              inline FrameTelefono2: TFrameTelefono
                Left = 8
                Top = 42
                Width = 701
                Height = 30
                TabOrder = 2
                inherited lblTelefono: TLabel
                  Width = 97
                  Caption = 'Tel'#233'fono alternativo:'
                  Font.Color = clWindowText
                end
                inherited txtTelefono: TEdit
                  Color = clWindow
                end
                inherited cbCodigosArea: TComboBox
                  Color = clWindow
                end
              end
            end
            object GBDomicilios: TGroupBox
              Left = 4
              Top = 144
              Width = 826
              Height = 101
              Anchors = [akLeft, akTop, akRight]
              Caption = 'Domicilios '
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 11
              object lblnada: TLabel
                Left = 15
                Top = 18
                Width = 63
                Height = 13
                Cursor = crHandPoint
                Hint = 'Domicilio particular'
                Caption = 'Particular: '
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold, fsUnderline]
                ParentFont = False
                ParentShowHint = False
                ShowHint = True
              end
              object Label2: TLabel
                Left = 15
                Top = 43
                Width = 60
                Height = 13
                Cursor = crHandPoint
                Hint = 'Domicilio particular'
                Caption = 'Comercial:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold, fsUnderline]
                ParentFont = False
                ParentShowHint = False
                ShowHint = True
              end
              object LblDomicilioComercial: TLabel
                Left = 77
                Top = 43
                Width = 95
                Height = 13
                Caption = 'Ingrese el domicilio'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsItalic]
                ParentFont = False
              end
              object LblDomicilioParticular: TLabel
                Left = 78
                Top = 18
                Width = 95
                Height = 13
                Caption = 'Ingrese el domicilio'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsItalic]
                ParentFont = False
              end
              object Label1: TLabel
                Left = 16
                Top = 76
                Width = 96
                Height = 13
                Caption = 'Domicilio de entrega'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object BtnDomicilioComercialAccion: TDPSButton
                Left = 542
                Top = 47
                Width = 61
                Hint = 'Domicilio comercial'
                Caption = 'Alta'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 1
                OnClick = BtnDomicilioComercialAccionClick
              end
              object BtnDomicilioParticularAccion: TDPSButton
                Left = 542
                Top = 16
                Width = 61
                Hint = 'Domicilio particular'
                Caption = 'Alta'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 0
                OnClick = BtnDomicilioParticularAccionClick
              end
              object RGDomicilioEntrega: TRadioGroup
                Left = 127
                Top = 65
                Width = 235
                Height = 32
                Columns = 2
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                Items.Strings = (
                  '&Particular'
                  '&Comercial')
                ParentFont = False
                TabOrder = 2
              end
              object ChkEliminarDomicilioParticular: TCheckBox
                Left = 607
                Top = 22
                Width = 67
                Height = 17
                Caption = 'Eliminar'
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 3
                OnClick = ChkEliminarDomicilioParticularClick
              end
              object ChkEliminarDomicilioComercial: TCheckBox
                Left = 608
                Top = 51
                Width = 67
                Height = 17
                Caption = 'Eliminar'
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 4
                OnClick = ChkEliminarDomicilioComercialClick
              end
            end
          end
          object ts_DatosCuenta: TTabSheet
            Caption = 'Datos de la C&uenta'
            ImageIndex = 1
            object GroupBox2: TGroupBox
              Left = 3
              Top = 8
              Width = 358
              Height = 254
              Caption = ' Datos del Veh'#237'culo '
              TabOrder = 0
              object Label13: TLabel
                Left = 21
                Top = 25
                Width = 49
                Height = 13
                Caption = 'Patente:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clNavy
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Label14: TLabel
                Left = 21
                Top = 50
                Width = 40
                Height = 13
                Caption = 'Marca:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clNavy
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Label20: TLabel
                Left = 21
                Top = 75
                Width = 46
                Height = 13
                Caption = 'Modelo:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clNavy
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Label21: TLabel
                Left = 175
                Top = 125
                Width = 27
                Height = 13
                Caption = 'Color:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object Label38: TLabel
                Left = 20
                Top = 99
                Width = 61
                Height = 13
                Caption = 'Categor'#237'a:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clNavy
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Label47: TLabel
                Left = 21
                Top = 126
                Width = 27
                Height = 13
                Caption = '&A'#241'o:'
                FocusControl = txt_anio
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clNavy
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object cb_color: TComboBox
                Left = 211
                Top = 119
                Width = 126
                Height = 21
                Style = csDropDownList
                ItemHeight = 13
                TabOrder = 4
              end
              object cb_marca: TComboBox
                Left = 101
                Top = 46
                Width = 236
                Height = 21
                Style = csDropDownList
                Color = 16444382
                ItemHeight = 13
                TabOrder = 1
                OnChange = cb_marcaChange
              end
              object cb_modelo: TComboBox
                Left = 101
                Top = 71
                Width = 236
                Height = 21
                Style = csDropDownList
                Color = 16444382
                ItemHeight = 13
                TabOrder = 2
                OnChange = cb_modeloChange
              end
              object mcPatente: TMaskCombo
                Left = 101
                Top = 21
                Width = 235
                Height = 21
                Items = <>
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                Color = 16444382
                ComboWidth = 130
                ItemIndex = -1
                OmitCharacterRequired = False
                TabOrder = 0
              end
              object txt_anio: TNumericEdit
                Left = 101
                Top = 119
                Width = 47
                Height = 21
                Color = 16444382
                MaxLength = 4
                TabOrder = 3
                OnChange = txt_anioChange
                Decimals = 0
              end
              object GroupDimensiones: TGroupBox
                Left = 13
                Top = 166
                Width = 333
                Height = 76
                Caption = 'Dimensiones (en cent'#237'metros)'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 5
                object Label48: TLabel
                  Left = 117
                  Top = 49
                  Width = 92
                  Height = 13
                  Caption = '&Largo del veh'#237'culo:'
                  FocusControl = txt_largoVehiculo
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                end
                object Label49: TLabel
                  Left = 134
                  Top = 22
                  Width = 75
                  Height = 13
                  Caption = '&Largo adicional:'
                  FocusControl = txt_LargoAdicionalVehiculo
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                end
                object txt_largoVehiculo: TNumericEdit
                  Left = 212
                  Top = 44
                  Width = 76
                  Height = 21
                  Color = clBtnFace
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 2
                  Decimals = 0
                end
                object txt_LargoAdicionalVehiculo: TNumericEdit
                  Left = 212
                  Top = 18
                  Width = 76
                  Height = 21
                  Color = clBtnFace
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 1
                  Decimals = 0
                end
                object chk_acoplado: TCheckBox
                  Left = 8
                  Top = 23
                  Width = 101
                  Height = 17
                  Caption = 'Tiene Acoplado'
                  Color = clBtnFace
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentColor = False
                  ParentFont = False
                  TabOrder = 0
                  OnClick = chk_acopladoClick
                end
              end
              object txt_DescCategoria: TEdit
                Left = 101
                Top = 95
                Width = 236
                Height = 21
                TabStop = False
                Color = clBtnFace
                MaxLength = 16
                ReadOnly = True
                TabOrder = 6
              end
              object txt_CodigoCategoria: TNumericEdit
                Left = 341
                Top = 95
                Width = 8
                Height = 21
                TabOrder = 7
                Visible = False
                Decimals = 0
              end
            end
            object Panel2: TPanel
              Left = 364
              Top = 8
              Width = 313
              Height = 253
              BevelOuter = bvNone
              TabOrder = 1
              object GroupBox3: TGroupBox
                Left = 0
                Top = 0
                Width = 313
                Height = 164
                Align = alClient
                Caption = ' Forma de Pago '
                TabOrder = 0
                object Label22: TLabel
                  Left = 45
                  Top = 43
                  Width = 60
                  Height = 13
                  Caption = 'Debitar de'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object lbl_entidad: TLabel
                  Left = 45
                  Top = 68
                  Width = 45
                  Height = 13
                  Caption = 'Tarjeta:'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object Label23: TLabel
                  Left = 45
                  Top = 93
                  Width = 48
                  Height = 13
                  Caption = 'N'#250'mero:'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object rb_prepago: TRadioButton
                  Left = 32
                  Top = 134
                  Width = 113
                  Height = 17
                  Caption = '&Tarjeta Prepaga'
                  TabOrder = 0
                  Visible = False
                  OnClick = chk_contagClick
                end
                object rb_pospagomanual: TRadioButton
                  Left = 32
                  Top = 116
                  Width = 161
                  Height = 17
                  Caption = 'Po&spago con pago manual'
                  TabOrder = 1
                  OnClick = chk_contagClick
                end
                object rb_pospagoautomatico: TRadioButton
                  Left = 32
                  Top = 17
                  Width = 181
                  Height = 17
                  Caption = 'Po&spago con d'#233'bito autom'#225'tico'
                  Checked = True
                  TabOrder = 2
                  TabStop = True
                  OnClick = chk_contagClick
                end
                object cb_tipodebito: TComboBox
                  Left = 121
                  Top = 39
                  Width = 181
                  Height = 21
                  Style = csDropDownList
                  Color = 16444382
                  ItemHeight = 13
                  ItemIndex = 0
                  TabOrder = 3
                  Text = 'Tarjeta de Cr'#233'dito'
                  OnChange = cb_tipodebitoChange
                  Items.Strings = (
                    'Tarjeta de Cr'#233'dito'
                    'Cuenta Bancaria')
                end
                object cb_entidaddebito: TComboBox
                  Left = 121
                  Top = 64
                  Width = 181
                  Height = 21
                  Style = csDropDownList
                  Color = 16444382
                  ItemHeight = 13
                  TabOrder = 4
                end
                object txt_cuentadebito: TEdit
                  Left = 121
                  Top = 89
                  Width = 139
                  Height = 21
                  Color = 16444382
                  MaxLength = 16
                  TabOrder = 5
                end
              end
              object GroupBox4: TGroupBox
                Left = 0
                Top = 164
                Width = 313
                Height = 89
                Align = alBottom
                Caption = 'Otros datos'
                TabOrder = 1
                object chk_contag: TCheckBox
                  Left = 33
                  Top = 18
                  Width = 180
                  Height = 17
                  Caption = 'Equipar el veh'#237'culo con un TAG'
                  Checked = True
                  State = cbChecked
                  TabOrder = 0
                  OnClick = chk_contagClick
                end
                object chk_detallepasadas: TCheckBox
                  Left = 33
                  Top = 57
                  Width = 260
                  Height = 17
                  Caption = 'Imprimir un detalle de pasadas para este veh'#237'culo'
                  TabOrder = 1
                  OnClick = chk_detallepasadasClick
                end
                object chk_enviofacturas: TCheckBox
                  Left = 33
                  Top = 38
                  Width = 149
                  Height = 17
                  Caption = 'Enviar facturas a domicilio'
                  TabOrder = 2
                end
              end
            end
          end
          object ts_Observaciones: TTabSheet
            Caption = 'O&bservaciones'
            ImageIndex = 2
            object txt_adhesion_observaciones: TMemo
              Left = 5
              Top = 5
              Width = 820
              Height = 484
              TabOrder = 0
            end
          end
        end
      end
    end
    object tab_tags: TTabSheet
      Caption = '&Tags'
      ImageIndex = 2
      TabVisible = False
      object GroupBox9: TGroupBox
        Left = 5
        Top = 8
        Width = 695
        Height = 374
        Caption = ' Datos de la Cuenta '
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        DesignSize = (
          695
          374)
        object Label31: TLabel
          Left = 13
          Top = 29
          Width = 44
          Height = 13
          Caption = 'Cliente:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label32: TLabel
          Left = 13
          Top = 54
          Width = 45
          Height = 13
          Caption = 'Cuenta:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label36: TLabel
          Left = 13
          Top = 83
          Width = 71
          Height = 13
          Caption = 'Observaciones'
        end
        object lbl_tag_cliente: TLabel
          Left = 216
          Top = 30
          Width = 449
          Height = 13
          AutoSize = False
        end
        object cb_tag_cuenta: TComboBox
          Left = 87
          Top = 51
          Width = 370
          Height = 21
          Style = csDropDownList
          Color = 16444382
          ItemHeight = 13
          MaxLength = 4
          TabOrder = 1
        end
        object txt_tag_cliente: TPickEdit
          Left = 87
          Top = 26
          Width = 122
          Height = 21
          Color = 16444382
          Enabled = True
          MaxLength = 10
          TabOrder = 0
          OnChange = txt_tag_clienteChange
          Decimals = 0
          EditorStyle = bteNumericEdit
          OnButtonClick = txt_tag_clienteButtonClick
        end
        object txt_tag_observaciones: TMemo
          Left = 8
          Top = 100
          Width = 677
          Height = 249
          Anchors = [akLeft, akTop, akRight, akBottom]
          TabOrder = 2
        end
      end
    end
    object tab_facturacion: TTabSheet
      Caption = '&Facturaci'#243'n'
      TabVisible = False
      DesignSize = (
        843
        736)
      object GroupBox12: TGroupBox
        Left = 5
        Top = 5
        Width = 832
        Height = 726
        Anchors = [akLeft, akTop, akRight, akBottom]
        Caption = ' Datos del error de Facturaci'#243'n '
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        DesignSize = (
          832
          726)
        object Label50: TLabel
          Left = 13
          Top = 29
          Width = 44
          Height = 13
          Caption = 'Cliente:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label15: TLabel
          Left = 13
          Top = 83
          Width = 71
          Height = 13
          Caption = 'Observaciones'
        end
        object lbl_fact_cliente: TLabel
          Left = 230
          Top = 29
          Width = 3
          Height = 13
        end
        object pnl_ultimafactura: TPanel
          Left = 8
          Top = 52
          Width = 613
          Height = 29
          BevelOuter = bvNone
          TabOrder = 2
          object Label29: TLabel
            Left = 5
            Top = 6
            Width = 87
            Height = 13
            Caption = #218'ltima Factura:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label30: TLabel
            Left = 221
            Top = 6
            Width = 15
            Height = 13
            Caption = 'de'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object cb_fact_mes: TComboBox
            Left = 95
            Top = 3
            Width = 122
            Height = 21
            Style = csDropDownList
            Color = 16444382
            ItemHeight = 13
            MaxLength = 4
            TabOrder = 0
          end
          object cb_fact_anio: TComboBox
            Left = 247
            Top = 3
            Width = 83
            Height = 21
            Style = csDropDownList
            Color = 16444382
            ItemHeight = 13
            MaxLength = 4
            TabOrder = 1
          end
        end
        object pnl_Factura: TPanel
          Left = 7
          Top = 54
          Width = 497
          Height = 29
          BevelOuter = bvNone
          TabOrder = 3
          object Label16: TLabel
            Left = 5
            Top = 6
            Width = 48
            Height = 13
            Caption = 'Factura:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object cb_fact_factura: TComboBox
            Left = 95
            Top = 3
            Width = 370
            Height = 21
            Style = csDropDownList
            Color = 16444382
            ItemHeight = 13
            MaxLength = 4
            TabOrder = 0
          end
        end
        object txt_fact_cliente: TPickEdit
          Left = 103
          Top = 26
          Width = 119
          Height = 21
          Color = 16444382
          Enabled = True
          MaxLength = 10
          TabOrder = 0
          OnChange = txt_fact_clienteChange
          Decimals = 0
          EditorStyle = bteNumericEdit
          OnButtonClick = txt_fact_clienteButtonClick
        end
        object txt_fact_observaciones: TMemo
          Left = 8
          Top = 100
          Width = 813
          Height = 615
          Anchors = [akLeft, akTop, akRight, akBottom]
          TabOrder = 1
        end
      end
    end
    object tab_Otros: TTabSheet
      Caption = '&Seguridad Vial'
      ImageIndex = 4
      TabVisible = False
      DesignSize = (
        843
        736)
      object GroupBox6: TGroupBox
        Left = 6
        Top = 4
        Width = 832
        Height = 726
        Anchors = [akLeft, akTop, akRight, akBottom]
        Caption = ' Comentarios '
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        DesignSize = (
          832
          726)
        object txt_Otros_observaciones: TMemo
          Left = 8
          Top = 19
          Width = 813
          Height = 698
          Anchors = [akLeft, akTop, akRight, akBottom]
          TabOrder = 0
        end
      end
    end
    object tsOrdenTrabajo: TTabSheet
      Caption = #211'rden de Trabajo'
      ImageIndex = 4
      TabVisible = False
      object GroupBox1: TGroupBox
        Left = 14
        Top = 9
        Width = 680
        Height = 365
        Caption = ' '#211'rden de Trabajo '
        TabOrder = 0
        object Label44: TLabel
          Left = 13
          Top = 77
          Width = 64
          Height = 13
          Caption = 'Tipo de Falla:'
        end
        object Label45: TLabel
          Left = 13
          Top = 102
          Width = 51
          Height = 13
          Caption = 'Ubicaci'#243'n:'
        end
        object Label46: TLabel
          Left = 13
          Top = 130
          Width = 74
          Height = 13
          Caption = 'Observaciones:'
        end
        object Label51: TLabel
          Left = 13
          Top = 25
          Width = 111
          Height = 13
          Caption = 'Tipo de Mantenimiento:'
        end
        object Label52: TLabel
          Left = 13
          Top = 51
          Width = 72
          Height = 13
          Caption = 'Tipo de Causa:'
        end
        object cbTiposFallas: TComboBox
          Left = 128
          Top = 74
          Width = 245
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 2
        end
        object mmDescripcion: TMemo
          Left = 128
          Top = 126
          Width = 333
          Height = 76
          TabOrder = 3
        end
        object cbUbicaciones: TComboBox
          Left = 128
          Top = 99
          Width = 333
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 4
        end
        object gbComunicacion: TGroupBox
          Left = 9
          Top = 216
          Width = 464
          Height = 85
          Caption = 'Comunicaci'#243'n  '
          TabOrder = 5
          Visible = False
          object Label26: TLabel
            Left = 8
            Top = 30
            Width = 93
            Height = 13
            Caption = 'Medio de Contacto:'
          end
          object Label41: TLabel
            Left = 10
            Top = 55
            Width = 46
            Height = 13
            Caption = 'Contacto:'
          end
          object btnBorrar: TSpeedButton
            Left = 500
            Top = 3
            Width = 23
            Height = 22
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              04000000000000010000120B0000120B00001000000000000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333000000000
              3333333777777777F3333330F777777033333337F3F3F3F7F3333330F0808070
              33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
              33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
              333333F7F7F7F7F7F3F33030F080707030333737F7F7F7F7F7333300F0808070
              03333377F7F7F7F773333330F080707033333337F7F7F7F7F333333070707070
              33333337F7F7F7F7FF3333000000000003333377777777777F33330F88877777
              0333337FFFFFFFFF7F3333000000000003333377777777777333333330777033
              3333333337FFF7F3333333333000003333333333377777333333}
            NumGlyphs = 2
            OnClick = btnBorrarClick
          end
          object cbMediosContacto: TComboBox
            Left = 114
            Top = 24
            Width = 325
            Height = 21
            Style = csDropDownList
            ItemHeight = 13
            TabOrder = 0
          end
          object bteContacto: TBuscaTabEdit
            Left = 114
            Top = 49
            Width = 325
            Height = 21
            Enabled = True
            TabOrder = 1
            OnEnter = IrAlInicio
            OnExit = IrAlInicio
            Decimals = 0
            EditorStyle = bteTextEdit
            OnButtonClick = bteContactoButtonClick
          end
        end
        object cbTiposMantenimiento: TComboBox
          Left = 128
          Top = 22
          Width = 245
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 0
        end
        object cbTiposCausa: TComboBox
          Left = 128
          Top = 48
          Width = 245
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 1
        end
      end
    end
  end
  object tvOrdenesServicio: TTreeView
    Left = 0
    Top = 0
    Width = 177
    Height = 746
    Align = alLeft
    Color = 16315631
    HotTrack = True
    Images = ImageList1
    Indent = 19
    ParentShowHint = False
    ReadOnly = True
    RowSelect = True
    ShowHint = True
    TabOrder = 1
    OnClick = tvOrdenesServicioClick
    OnDeletion = tvOrdenesServicioDeletion
  end
  object ObtenerDatosContacto: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDatosContacto'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@TipoContacto'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end>
    Left = 664
    Top = 188
  end
  object ImageList1: TImageList
    Left = 72
    Top = 152
    Bitmap = {
      494C010116001800040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000006000000001002000000000000060
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000DEDEDE007B7B84004A4A520031313900B5B5
      BD00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000B5B5B50029313100292931005A5A630084949400ADC6C6003139
      4200737B7B000000000000000000000000000000000000000000000000000000
      00000000000000000000EFEFEF00CECECE00BDBDC600CECED600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00004A52520039394200848C8C00F7FFFF00F7FFFF00EFFFFF00EFFFFF00E7FF
      FF0052636300424A4A0000000000000000000000000000000000000000000000
      0000F7F7F7006B6B73004A4A4A005A5A4A0063634A005A5A4A0039423900ADAD
      AD00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000004A4A
      4A004A525A00E7EFF700000000000000000000000000F7FFFF00EFFFFF005A63
      6B004A525A003131390029313100000000000000000000000000000000000000
      0000636363008C8C5A00AD9C5A00B5A563008C845200B5A56300AD9C5A006B6B
      4A007B847B000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000007B8484002929
      3100A5B5B5005A5A5A00E7E7E70000000000000000006B6B7300292931004A52
      52008C8C8C00B5B5B500CECECE00000000000000000000000000000000004A4A
      52009C945A00AD9C5A00B5A56300423931006B5A4200C6AD6300B5A56300A594
      5A0084845A007B7B7B0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000031313900A5BD
      BD00D6E7E700737B73006B6B6B008C949400636B6B006B6B6B00B5B5BD000000
      0000000000000000000000000000000000000000000000000000D6D6DE005252
      42009C9C5A00B5A56300947B5200DEB56B00E7BD6B00524A3100CEAD6300B5A5
      63009C9C5A004A4A4200EFEFEF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000848C8C0029313900D6EF
      EF008C949C006B7B7B006B6B6B006B6B6B002929290094949400000000000000
      0000000000000000000000000000000000000000000000000000A5A5A5006363
      42009C9C5A00B5A56300CEAD6300E7BD6B00F7C67300C6A56300CEAD6300B5A5
      63009C9C5A005A5A4200CECECE00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000004A5252007B9494009CB5
      B50094ADAD00A5B5B500737B7B00A5A5A50063636300E7E7E700000000000000
      00000000000000000000000000000000000000000000000000008C8C94006B6B
      42009C9C5A00B5A5630031312900DEB56B00E7BD6B0042423100736B4200B5A5
      63009C9C5A005A634A00BDBDBD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DEDEDE0018212900C6EFF700B5D6
      D600B5CED600B5CECE00A5B5B500737B7B00B5B5B5006B6B6B008C848400F7F7
      F700000000000000000000000000000000000000000000000000ADB5B500636B
      52009C945A006B634200181821006B634200BDA563002121210029292900AD9C
      5A009C945A0042423900F7F7F700000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CECECE0031313900292931002129
      290021212900212129002129290031393900737373009C9C9C00737373009494
      9400D6D6D6000000000000000000000000000000000000000000000000004242
      42008C8C52009C9C5A00524A3900B5A56300B5A56300736B42007B734A009C9C
      5A006B6B4A008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F7F7F700EFEFEF00D6D6D600E7E7
      E700F7F7F70000000000000000000000000094949400CECECE00B5B5B5007B7B
      7B00C6BDBD00BDBDBD0000000000000000000000000000000000000000000000
      00003939390084844A009C945A009C9C5A009C9C5A009C9C5A009C945A007B7B
      4A00525A52000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F7F7F70094949400DEDEDE00A5A5
      A500848484009C9C9C0000000000000000000000000000000000000000000000
      0000000000005A5A5A005A5A4A0063634200636342006B6B4A004A4A42007373
      7300000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFEFEF0094949400A5A5
      A500636B6300C6C6C60000000000000000000000000000000000000000000000
      00000000000000000000CECECE00ADADAD009C9C9C00A5A5AD00E7E7E7000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00949C
      9400637B8C00DEDEE70000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00E7E7F7000000000000000000D6D6
      E700FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00EFEFEF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000948CBD004242AD0031318C006B73
      6B00636B630073737300FFFFFF00FFFFFF00EFEFF7003131940042399400BDBD
      D600FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000737B840029315A002931
      5A0029315A0029315A0029315A0029315A0029315A0029315A0029315A002931
      5A0029315A0039395200E7E7E70000000000000000000000000094949C007373
      730073737B0073737B0073737B0073737B0073737B0073737B0073737B007373
      7B0073737B0073737300E7E7E70000000000000000000000000094949C007373
      730073737B0073737B0073737B0073737B0073737B0073737B0073737B007373
      7B0073737B0073737300E7E7E70000000000FFFFFF00736BB5002929BD004A4A
      94004A4A390084846300ADADAD00E7E7E700423994003131B500A5A5CE00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000BDBDBD00212984001821
      DE001821DE001821DE001821DE001821DE001821DE001821DE001821DE001821
      DE001821DE0031314A00000000000000000000000000000000005A6363009C9C
      9C00B5BDBD00B5BDBD00B5BDBD00B5BDBD00B5BDBD00B5BDBD00B5BDBD00B5BD
      BD00B5BDBD0063636300DEDEDE000000000000000000000000005A6363009C9C
      9C00B5BDBD00B5BDBD00B5BDBD00B5BDBD00B5BDBD00B5BDBD00B5BDBD00B5BD
      BD00B5BDBD0063636300DEDEDE0000000000FFFFFF00FFFFFF00424294003131
      B50073739C004A4A4200313931007373AD002929BD005A5AA500FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000EFEFEF0029315A001821
      DE001821DE001821DE001821D60000000000000000001821DE001821DE001821
      DE001821BD00525A6B00000000000000000000000000000000005A636300CECE
      CE009C9C9C007B7B84007B7B84007B7B84007B7B84007B7B84007B7B84007B7B
      8400C6C6C60063636B00DEDEDE000000000000000000000000005A636300CECE
      CE009C9C9C007B7B84007B7B84007B7B84007B7B84007B7B84007B7B84007B7B
      8400C6C6C60063636B00DEDEDE0000000000FFFFFF00FFFFFF00525263002929
      940029298400424242003131420029299C0031318C00F7F7FF00FFFFFF004A52
      4A0052525200FFFFFF00FFFFFF00FFFFFF000000000000000000313952001821
      D6001821DE001821DE001821D60000000000000000001821DE001821DE001821
      DE001821940094949C00000000000000000000000000000000005A636300CECE
      CE0052525A001818210021212900212129002121290021212900212129001821
      21009494940063636B00DEDEDE000000000000000000000000005A636300CECE
      CE0052525A001818210021212900212129002121290021212900212129001821
      21009494940063636B00DEDEDE0000000000FFFFFF00FFFFFF00424A42005252
      73002929B5004A428400393184003131A50039395200E7E7E7004A4A4200BDA5
      6300BDA5630052524A00FFFFFF00FFFFFF000000000000000000636373001821
      B5001821DE001821DE001821D60000000000000000001821DE001821DE001821
      DE0029297300CECECE00000000000000000000000000000000005A636300CECE
      CE00BDBDBD00B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5
      B500CECED60063636B00DEDEDE000000000000000000000000005A636300CECE
      CE00BDBDBD00B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5
      B500CECED60063636B00DEDEDE0000000000FFFFFF00FFFFFF004A4A4200B5A5
      6300423984002929A5002929AD005A527B006B634A0063636B00636B3900CEAD
      6300F7C673007B6B5200B5B5BD00FFFFFF000000000000000000A5A5AD001821
      8C001821DE001821DE001821D60000000000000000001821DE001821DE001821
      DE0029315200F7F7F700000000000000000000000000000000005A636300CECE
      CE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDE
      DE00DEDEDE0063636B00DEDEDE000000000000000000000000005A636300CECE
      CE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDE
      DE00DEDEDE0063636B00DEDEDE0000000000FFFFFF00FFFFFF004A4A4200B5A5
      6300B59473002929A50031319400B5A5630063634A0073737B0063634200CEAD
      6300CEAD63005A524200E7E7E700FFFFFF000000000000000000DEDEDE002931
      63001821DE001821DE001821D60021296B0018219C001821DE001821DE001821
      D6003942520000000000000000000000000000000000000000005A636300CECE
      CE004A4A52003139390039393900393939003939390039393900393939004242
      4200D6D6D60063636B00DEDEDE000000000000000000000000005A636300CECE
      CE004A4A52003139390039393900393939003939390039393900393939004242
      4200D6D6D60063636B00DEDEDE0000000000FFFFFF00FFFFFF004A4A42009C8C
      5A0039318C003131A5002929B500423984004A4A4200F7F7F700525A52006363
      42005A6342008C8C8C00FFFFFF00FFFFFF000000000000000000000000003131
      4A001821DE001821DE001821D600182194001821BD001821DE001821DE001821
      AD006B73840000000000000000000000000000000000000000005A636300CECE
      CE004A4A52001821210029313100293131002931310029313100293131002121
      2900CECECE0063636B00DEDEDE000000000000000000000000005A636300CECE
      CE004A4A52001821210029313100293131002931310029313100293131002121
      2900CECECE0063636B00DEDEDE0000000000FFFFFF00FFFFFF006B6B6B003131
      7B002129B5003931730029297B002929B5007373A500FFFFFF00FFFFFF009CA5
      A500ADB5B500FFFFFF00FFFFFF00FFFFFF000000000000000000000000005252
      6B001821C6001821DE001821D60000000000000000001821DE001821DE002121
      8400ADB5B50000000000000000000000000000000000000000005A636300CECE
      CE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDE
      DE00DEDEDE0063636B00DEDEDE000000000000000000000000005A636300CECE
      CE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDE
      DE00DEDEDE0063636B00DEDEDE0000000000FFFFFF00FFFFFF00A5A5CE003131
      B50039318C00525242004A4A420029298C003939A500DEDEEF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000009494
      9C0018219C001821DE001821D60000000000000000001821DE001821DE002931
      6300E7E7E70000000000000000000000000000000000000000005A636300C6C6
      C600848484008484840084848400848484008484840084848400848484007B84
      8400B5B5B50063636B00DEDEDE000000000000000000000000005A636300C6C6
      C600848484008484840084848400848484008484840084848400848484007B84
      8400B5B5B50063636B00DEDEDE0000000000FFFFFF00EFEFF70039399C003131
      A500525252006B735A00DEDEDE009C9CCE003131B50042429400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000CECE
      CE00293173001821DE001821D60000000000000000001821DE001821DE003131
      4A000000000000000000000000000000000000000000000000005A636300C6C6
      C600182121002121290021212900212129002121290021212900212129001821
      210084848C0063636B00DEDEDE000000000000000000000000005A636300C6C6
      C600182121002121290021212900212129002121290021212900212129001821
      210084848C0063636B00DEDEDE0000000000FFFFFF005252A5002929BD002929
      630042424200ADADAD00FFFFFF00FFFFFF005252A5002929BD007B7BB500EFEF
      F700FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000F7F7
      FF00293152001821DE001821D60000000000000000001821DE001821C6004A52
      63000000000000000000000000000000000000000000000000005A636300C6CE
      CE00BDBDC600BDBDC600BDBDC600BDBDC600BDBDC600BDBDC600BDBDC600BDBD
      C600CECECE0063636B00DEDEDE000000000000000000000000005A636300C6CE
      CE00BDBDC600BDBDC600BDBDC600BDBDC600BDBDC600BDBDC600BDBDC600BDBD
      C600CECECE0063636B00DEDEDE0000000000CECEE700424294004A4A9C00424A
      4A00EFEFEF00FFFFFF00FFFFFF00FFFFFF00EFEFF700424294004A4A9400D6D6
      E700FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      000042425A001821CE001821DE001821CE001821D6001821DE0018219C008C8C
      94000000000000000000000000000000000000000000000000005A6363007B84
      84008C8C94008C8C94008C8C94008C8C94008C8C94008C8C94008C8C94008C8C
      94008C8C9400525A5A00DEDEDE000000000000000000000000005A6363007B84
      84008C8C94008C8C94008C8C94008C8C94008C8C94008C8C94008C8C94008C8C
      94008C8C9400525A5A00DEDEDE0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      00007B848C0029314A0029314A0029314A0029314A0029314A0031314A00CECE
      CE00000000000000000000000000000000000000000000000000BDBDBD00ADAD
      AD00ADADAD00ADADAD00ADADAD00ADADAD00ADADAD00ADADAD00ADADAD00ADAD
      AD00ADADAD00ADADAD00EFEFEF00000000000000000000000000BDBDBD00ADAD
      AD00ADADAD00ADADAD00ADADAD00ADADAD00ADADAD00ADADAD00ADADAD00ADAD
      AD00ADADAD00ADADAD00EFEFEF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00EFEFEF00FFFFFF00000000000000000000000000000000001018
      1800101818001018180010181800101818001018180010181800101818001018
      1800101818000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000DEE7
      F7007394B5009CBDD60000000000000000000000000000000000000000000000
      0000000000007B7B7B005A5A5A005A5A5A005A5A5A005A5A5A005A5A5A006B73
      7B007394B5009CBDD600000000000000000000000000737B7B00313139003131
      3900313139003131390031313900313139003131390031313900313139003131
      39003131390039394200E7E7E700000000000000000000000000000000001018
      18001821DE001821DE001821DE001821DE001821DE001821DE001821DE001821
      DE00101818000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DEE7EF00315A
      9400218CD60029639C007BADCE00000000000000000000000000000000000000
      0000000000005A5A5A009C9C9C00ADADB500ADADB500ADADB500BDBDBD003139
      4A00218CD60029639C007BADCE000000000000000000BDBDBD00293131003131
      3900313139003131390031313900313139003131390031313900313139001821
      DE00313139003131390000000000000000000000000000000000000000001018
      18001821DE001821DE001821DE00101818001821DE001821DE001821DE001821
      DE00101818000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000042638C0042A5
      EF0029C6F7002963A5003984A500000000000000000000000000000000000000
      0000000000005A5A5A0042424A00292929002929290039393900A5A5AD00314A
      5A0029C6F7002963A5003984A5000000000000000000EFEFEF00313139003131
      390031313900313139003131310000000000000000003131390029315A001821
      DE0029295A005A5A5A0000000000000000000000000000000000000000001018
      18001821DE001821DE001018180010181800101818001821DE001821DE001821
      DE00101818000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000094A5C6003173CE0073D6
      FF00398CCE002984BD008CADBD00000000000000000000000000000000000000
      0000000000005A5A5A00BDBDC600CECECE00CECECE00CECECE00F7F7F7003952
      5A00398CCE002984BD008CADBD00000000000000000000000000393939003131
      39003131390031313900313139000000000000000000313139001821DE002129
      94001821DE00949C9C0000000000000000000000000000000000000000001018
      18001821DE00101818001821DE00101818001821DE00101818001821DE001821
      DE00101818000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F7F7FF0039639C007BBDF70063AD
      E7002973AD005A94B50000000000000000000000000000000000000000000000
      0000000000005A5A5A0042424A00212929002129290039393900A5A5AD00394A
      5A002973AD005A94B5000000000000000000000000000000000063636B002931
      31003131390031313900313139000000000000000000313139001821DE003131
      42001821DE00CECECE0000000000000000000000000000000000000000001018
      18001821DE00101818001821DE00101818001821DE00101818001821DE001821
      DE00101818000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000007394BD00428CDE00A5CEF7002963
      9C00428CB500F7FFFF0000000000000000000000000000000000000000000000
      0000000000005A5A5A009C9CA500A5A5A500A5A5A500A5ADAD00BDBDBD003139
      4A00428CB500F7FFFF0000000000000000000000000000000000A5ADAD002929
      310031313900313139003131390000000000000000001821DE001821DE003131
      39001821DE00B5B5EF0000000000000000000000000000000000000000001018
      18001821DE001821DE001018180010181800101818001821DE001821DE001821
      DE00101818000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000D6DEEF00316BB5009CCEF700316BA5003184
      B500DEEFF7000000000000000000000000000000000000000000000000000000
      0000000000005A5A5A004A4A5200293131002931310039393900A5A5AD003142
      4A00DEEFF7000000000000000000000000000000000000000000DEDEDE003131
      3900313139003131390031313100000000000000000031313900313139003131
      3100313973001821DE0000000000000000000000000000000000000000001018
      18001821DE00101818001821DE00101818001821DE001821DE001821DE001821
      DE00101818000000000000000000000000000000000000000000000000000000
      00000000000000000000000000005A84B5006BBDF7004A8CC6002973A500BDD6
      E700000000000000000000000000000000000000000000000000000000000000
      0000000000005A5A5A007B7B7B006B7373006B737B006B737B00636B73004A52
      5200000000000000000000000000000000000000000000000000000000003131
      3900313139003131390031313900292929002931310031313900313139002929
      31006B6B7B001821DE00F7F7FF00000000000000000000000000000000001018
      18001821DE00101818001821DE00101818001821DE00101818001821DE001821
      DE001018180000000000000000000000000000000000EFEFF7007B7394008484
      9C00DEDEE70000000000A5B5CE00399CE70052ADE70021638C009CC6D6000000
      00000000000000000000000000000000000000000000EFEFF7007B7394008484
      9C00DEDEE700D6D6D6008494AD00317BBD00428CBD0021527B00849CAD00CED6
      D600000000000000000000000000000000000000000000000000000000005252
      5A00293131003131390031313900000000000000000031313900313139002931
      3100ADB5B5001821DE00ADADF700000000000000000000000000000000001018
      18001821DE001821DE00101818001018180010181800101818001821DE001821
      DE0010181800000000000000000000000000000000006B6384005A5273008C84
      9C00524A6B00736B8C003173B50018B5F700215A8C00739CB500EFEFEF000000
      000000000000000000000000000000000000000000006B6384005A5273008C84
      9C00524A6B00736B8C003173B50018B5F700215A8C00739CB500EFEFEF000000
      0000000000000000000000000000000000000000000000000000000000009494
      9400292931003131390031313900000000000000000031313900313139003131
      3900E7E7E700ADB5F70000000000000000000000000000000000000000001018
      18001821DE001821DE001821DE00101818001821DE001821DE001821DE001821
      DE0010181800000000000000000000000000000000006B637B009C94A5008C7B
      94007B6B84008C7B94006B8CC6003173BD005284A500847B8C005A5A6B00F7F7
      F70000000000000000000000000000000000000000006B637B009C94A5008C7B
      94007B6B84008C7B94006B8CC6003173BD005284A500847B8C005A5A6B00F7F7
      F70000000000000000000000000000000000000000000000000000000000CECE
      CE00313139003131390031313900000000000000000031313900313139003131
      390000000000FFFFFF00F7F7FF00000000000000000000000000000000001018
      18001821DE001821DE001821DE001821DE001821DE001821DE001821DE001821
      DE001018180000000000000000000000000000000000DEDEE700A59CA5008C84
      9400948C9C00B5A5B500AD9CAD006B5A7300635A73006B637B00736B84000000
      00000000000000000000000000000000000000000000DEDEE700A59CA5008C84
      9400948C9C00B5A5B500AD9CAD006B5A7300635A73006B637B00736B84000000
      000000000000000000000000000000000000000000000000000000000000F7F7
      FF00313139003131390031313900000000000000000031313900293131005252
      5200000000000000000000000000000000000000000000000000000000001018
      18001821DE001821DE001821DE001821DE001821DE001821DE001821DE001821
      DE0010181800000000000000000000000000000000000000000000000000F7F7
      F70084738C00B5A5B500A594A500AD9CAD00B5ADB500948C9C00E7E7EF000000
      000000000000000000000000000000000000000000000000000000000000F7F7
      F70084738C00B5A5B500A594A500AD9CAD00B5ADB500948C9C00E7E7EF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000042424A003131310031313900313131003131310031313900292931008C8C
      8C00000000000000000000000000000000000000000000000000000000001018
      1800101818001018180010181800101818001018180010181800101818001018
      1800101818000000000000000000000000000000000000000000000000000000
      0000FFFFFF00B5ADBD00ADA5AD00ADA5B500BDBDC600FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00B5ADBD00ADA5AD00ADA5B500BDBDC600FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000084848400313139003131390031313900313139003131390031313900CECE
      CE00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000BDBDBD00B5B5B500B5B5BD00B5B5BD00B5B5BD00B5B5BD00B5B5BD00B5B5
      BD00B5B5B500F7F7FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000C6C6F7008484000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001018
      1800101818001018180010181800101818001018180010181800101818001018
      1800101818000000000000000000000000000000000000000000000000001018
      1800101818001018180010181800101818001018180010181800101818001018
      1800101818000000000000000000000000000000000000000000000000000000
      000052525A008C94940094949400949494009494940094949400949494009494
      940052525200F7F7F70000000000000000000000000000000000B5BDBD004242
      420052525A00BDBDC600000000008484000000840000EFEFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000001018
      1800000000000000000000000000000000000000000000000000000000000000
      0000101818000000000000000000000000000000000000000000000000001018
      180000F7FF0000F7FF0000F7FF0000F7FF0000F7FF0000F7FF0000F7FF0000F7
      FF00101818000000000000000000000000000000000000000000000000000000
      000052525A00F7F7F70000000000000000009C9C9C0000000000000000000000
      000052525A00F7F7F70000000000000000000000000000000000182121001818
      2100101818001018180018182900008400000084000084840000000000000000
      0000000000000000000000000000000000000000000000000000000000001018
      1800000000000000000000000000101818000000000000000000000000000000
      0000101818000000000000000000000000000000000000000000000000001018
      180000F7FF0000F7FF0000F7FF001010100000F7FF0000F7FF0000F7FF0000F7
      FF0010181800000000000000000000000000E7E7EF00B5B5BD00B5BDBD00B5BD
      BD0039424200B5B5B5006B6B73006B6B6B004A4A4A007B7B7B00BDBDBD000000
      000052525A00F7F7F7000000000000000000000000008C8C8C006B6B6B005252
      5A00292929001821210018216300008400000084000084840000293131008C8C
      9400F7F7F7000000000000000000000000000000000000000000000000001018
      1800000000000000000010181800101818001018180000000000000000000000
      0000101818000000000000000000000000000000000000000000000000001018
      180000F7FF0000F7FF0010101000101010001010100000F7FF0000F7FF0000F7
      FF0010181800000000000000000000000000CECECE0021636300108C9400108C
      940008848C00108C940008848C00108C9400185A5A00CED6D600424242000000
      000052525A00F7F7F70000000000000000000000000042424A00A5A5A5005A5A
      5A00394242005252520000840000008400001821310000840000101839001018
      1800181821001821210000000000000000000000000000000000000000001018
      1800000000001018180000000000101818000000000010181800000000000000
      0000101818000000000000000000000000000000000000000000000000001018
      180000F7FF001010100000F7FF001010100000F7FF001010100000F7FF0000F7
      FF0010181800000000000000000000000000CECECE00217B7B0000F7FF0000F7
      FF0000F7FF0000F7FF0000F7FF0000F7FF00106B7300636363006B6B6B000000
      000052525A00F7F7F7000000000000000000E7E7E700525252009C9C9C004A4A
      5200DEDEDE00A5ADBD000084000039395A004A4A520000840000848400001818
      2100101818001018180018182100E7E7EF000000000000000000000000001018
      1800000000001018180000000000101818000000000010181800000000000000
      0000101818000000000000000000000000000000000000000000000000001018
      180000F7FF001010100000F7FF001010100000F7FF001010100000F7FF0000F7
      FF0010181800000000000000000000000000CECECE00217B7B0000F7FF0000F7
      FF0000DEE70000DEEF0000F7FF0000F7FF00106B6B009C9C9C00000000000000
      000052525A00F7F7F7000000000000000000C6C6C600636363004A4A4A009494
      9400DEDEDE00CECEDE00A5ADDE00CECECE004242420084840000008400001818
      21001018180010181800181821007B7B7B000000000000000000000000001018
      1800000000000000000010181800101818001018180000000000000000000000
      0000101818000000000000000000000000000000000000000000000000001018
      180000F7FF0000F7FF0010101000101010001010100000F7FF0000F7FF0000F7
      FF0010181800000000000000000000000000CECECE00217B7B0000F7FF00089C
      A5000894940008949C0000B5BD0000F7FF00186B7300CECECE00949494000000
      000052525A00F7F7F7000000000000000000E7E7E70052525A006B6B6B003939
      420063636B00A5ADAD00DEDEDE0084848C00737B7B009C9CAD00008400005A5A
      8400182121001018180010181800B5B5BD000000000000000000000000001018
      1800000000001018180000000000101818000000000000000000000000000000
      0000101818000000000000000000000000000000000000000000000000001018
      180000F7FF001010100000F7FF001010100000F7FF0000F7FF0000F7FF0000F7
      FF0010181800000000000000000000000000CECECE00217B7B0000F7FF0000C6
      CE0000D6DE0008949C00088C940000F7FF00106B7300636363009C9C9C000000
      000052525A00F7F7F7000000000000000000000000000000000042424A005A5A
      5A007B7B7B00636363003942420039394200ADADAD00ADADAD00008400008484
      0000A5A5A5002929310031393900000000000000000000000000000000001018
      1800000000001018180000000000101818000000000010181800000000000000
      0000101818000000000000000000000000000000000000000000000000001018
      180000F7FF001010100000F7FF001010100000F7FF001010100000F7FF0000F7
      FF0010181800000000000000000000000000CECECE00217B7B0000F7FF0000C6
      CE0008636B0000A5AD0000E7F70000F7FF00186B7300CECED600000000000000
      000052525A00F7F7F7000000000000000000000000000000000000000000CECE
      CE0063636B0042424A00525A5A0073737300ADADAD00ADADAD00848400000084
      0000A5A5AD006B6B6B00B5B5B500000000000000000000000000000000001018
      1800000000000000000010181800101818001018180010181800000000000000
      0000101818000000000000000000000000000000000000000000000000001018
      180000F7FF0000F7FF001010100010101000101010001010100000F7FF0000F7
      FF0010181800000000000000000000000000CECECE00217B7B0000F7FF0000A5
      AD0008A5AD000894940000ADB50000F7FF0021737B00CECED600000000000000
      000052525A00F7F7F70000000000000000000000000000000000000000000000
      000000000000FFFFFF00BDBDC6007373730039424200848484009494B5000084
      0000848400005252520000000000000000000000000000000000000000001018
      1800000000000000000000000000101818000000000000000000000000000000
      0000101818000000000000000000000000000000000000000000000000001018
      180000F7FF0000F7FF0000F7FF001010100000F7FF0000F7FF0000F7FF0000F7
      FF0010181800000000000000000000000000CECECE00217B7B0000F7FF0000F7
      FF0000CED60000D6DE0000F7FF0000F7FF0018737B00B5BDBD00E7E7E700E7E7
      E70052525A00F7F7F70000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000007B7B8400525252008484
      0000848400000000000000000000000000000000000000000000000000001018
      1800000000000000000000000000000000000000000000000000000000000000
      0000101818000000000000000000000000000000000000000000000000001018
      180000F7FF0000F7FF0000F7FF0000F7FF0000F7FF0000F7FF0000F7FF0000F7
      FF0010181800000000000000000000000000CECECE00217B7B0000F7FF0000F7
      FF0000F7FF0000F7FF0000F7FF0000F7FF00106B730052525A00636B6B00636B
      6B00636B6B00F7F7F70000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E7E7E700A5A5
      A500CECECE000000000000000000000000000000000000000000000000001018
      1800000000000000000000000000000000000000000000000000000000000000
      0000101818000000000000000000000000000000000000000000000000001018
      180000F7FF0000F7FF0000F7FF0000F7FF0000F7FF0000F7FF0000F7FF0000F7
      FF0010181800000000000000000000000000CECECE00217B7B0000F7FF0000F7
      FF0000F7FF0000F7FF0000F7FF0000F7FF0021737B00CECED600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001018
      1800101818001018180010181800101818001018180010181800101818001018
      1800101818000000000000000000000000000000000000000000000000001018
      1800101818001018180010181800101818001018180010181800101818001018
      180010181800000000000000000000000000CECECE00295A5A00216B7300216B
      7300216B7300216B7300216B7300216B730029525A00CED6D600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F7F7F700DEDEDE00DEDEDE00DEDE
      DE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00F7F7F700000000000000
      0000000000000000000000000000000000000000000000000000B5B5B5007B7B
      84007B7B84007B7B84007B7B84007B7B84007B8484007B8484007B8484007B84
      84007B848400B5B5B50000000000000000000000000000000000B5B5B5007B7B
      84007B7B84007B7B84007B7B84007B7B84007B8484007B8484007B8484007B84
      84007B848400B5B5B50000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000084848C00848C
      9400B5BDC600B5BDC600BDBDC600BDC6C600BDC6C600BDB5AD00A5845A00AD8C
      6B0094949C008C8C8C000000000000000000000000000000000084848C006B6B
      7300181821008C8C9400BDBDC600BDC6C600BDC6C600C6C6CE00C6C6CE00A5A5
      A500181821005A63630000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000ADADAD001818
      2100C6C6CE000000000000000000000000000000000000000000F7F7F7003139
      3900525A5A000000000000000000000000000000000000000000848C8C00ADAD
      BD00B5BDC600ADB5BD00B5B5BD00E7E7EF00F7F7FF00BD9C7B009C6B3100AD7B
      4A00C6C6C6008C8C8C0000000000000000000000000000000000848C8C00ADAD
      BD005A636B0021292900A5ADB500B5B5BD00B5BDBD00B5BDBD00B5B5BD003139
      39004A5252008C8C8C0000000000000000000000000000000000B5BDBD004242
      420052525A00BDBDC60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000B5BDBD002121
      290018212100B5B5BD00000000000000000000000000000000007B7B84002129
      2900E7E7EF000000000000000000000000000000000000000000848C8C00ADAD
      BD0094949C007B848C0084848C00BDAD9C00AD8C6300A56B3900A5734200A573
      4200BDC6C6008C8C8C0000000000000000000000000000000000848C8C00ADAD
      BD008C949C00292931003942420084848C0084848C0084848C004A4A52002129
      2900B5B5B5008C8C8C0000000000000000000000000000000000182121001818
      210010181800101818001818210039394200ADADAD0000000000000000000000
      0000000000000000000000000000000000000000000000000000182121001818
      210010181800101818001818210039394200ADADAD00CECECE0018182100ADB5
      B500000000000000000000000000000000000000000000000000848C8C00ADAD
      BD007B848C006B6B73006B6B7300846B52009C6B39009C7B5A00BD9C7300A573
      4200BDBDB5008C8C8C0000000000000000000000000000000000848C8C00ADAD
      BD007B848C005A5A63001818210052525A006B6B73005A5A6300181821009C9C
      A500C6C6C6008C8C8C000000000000000000000000008C8C8C00636363005252
      5200292929001821210018182100101818001018180018181800292931008C8C
      9400F7F7F700000000000000000000000000000000008C8C8C006B6B6B005252
      5A00212129001018180018182100101818001018180010181800182121008C8C
      9400F7F7F7000000000000000000000000000000000000000000848C8C00ADAD
      BD00C6CED600C6CED600C6CED600CECED600BDB5AD00DEDEE700D6BDA500A56B
      3900B5A594008C8C8C0000000000000000000000000000000000848C8C00ADAD
      BD00C6CED600C6CED60073737B0021293100BDBDC6003139390063636B00EFF7
      F700C6C6C6008C8C8C0000000000000000000000000042424A009C9C94005252
      5200393942004A4A52004A4A4A00293131002121210018182100101818001018
      1800181821001821210000000000000000000000000042424A00A5A5A5005A5A
      5A00394242002931310018212100293131001818210010181800101818001018
      1800181821001821210000000000000000000000000000000000848C8C00ADAD
      BD00737B84005A6363005A636B0063636B0063636B0063636B00A59C9C00A57B
      4A00AD8463008C8C8C0000000000000000000000000000000000848C8C00ADAD
      BD00737B84005A6363005A636300292931001821210021212900636363006363
      6B00ADADAD008C8C8C000000000000000000000000004A4A520094948C004A4A
      5200DEDEDE00B5BDBD0073737B00394242004A4A4A0063636300181821001818
      210010181800101818001818210000000000E7E7E700525252009C9C9C004A4A
      5200DEDEDE00B5B5B50021292900212129001018180052525200181821001818
      2100101818001018180018182100E7E7EF000000000000000000848C8C00ADAD
      BD00CED6DE00CECED600CECED600CED6DE00D6D6DE00D6D6DE00E7E7E700B58C
      6300A57342008C8C8C0000000000000000000000000000000000848C8C00ADAD
      BD00CED6DE00CECED600CECED6008C949C00101818007B7B8400D6DEDE00DEDE
      DE00BDBDBD008C8C8C000000000000000000000000005A5A630042424A009494
      9400DEDEDE00DEDEDE00DEDEDE00CECECE00394242009C9494008C8484001818
      210010181800101818001818210000000000C6C6C600636363004A4A4A009494
      9400DEDEDE00DEDEDE00ADADAD001018180021292900A5A5A500949494001818
      21001018180010181800181821007B7B7B000000000000000000848C8C00ADAD
      BD00636B73004A4A52004A4A52004A4A52004A4A52004A5252004A5252008C73
      63009C7339008C84730000000000000000000000000000000000848C8C00ADAD
      BD00636B73004A4A52004A4A5200182121002121290018182100424A4A007B84
      8400C6C6C6008C8C8C0000000000000000000000000052525200636363003939
      420063636B00A5ADAD00DEDEDE0084848C00736B6B009C9C9C009C9C9C006B63
      630018212100101818001018180000000000E7E7E70052525A006B6B6B003939
      420063636B00A5ADAD005A5A630018182100182121009C9C9C00ADADAD006B6B
      7300182121001018180010181800B5B5BD000000000000000000848C8C00ADAD
      BD00CED6DE00CECED600CED6D600CED6DE00D6D6DE00D6D6DE00DEDEDE00E7E7
      E700A57B4A009C6B420000000000000000000000000000000000848C8C00ADAD
      BD00CED6DE00CECED6005A5A630039394200CED6D60052525A004A4A4A00E7E7
      E700C6C6C6008C8C8C000000000000000000000000000000000042424A005252
      5200737373005A5A5A0039394200393942009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9400292929003131390000000000000000000000000042424A005A5A
      5A007B7B7B004A4A4A0010181800313139003139390042424A00ADADAD00ADAD
      AD00A5A5A5002929310031393900000000000000000000000000848C8C00ADAD
      BD007B7B840063636B0063636B0063636B00636B6B00636B6B00636B6B009494
      9C00BDBDB5009C6B3900E7D6CE00000000000000000000000000848C8C00ADAD
      BD007B7B84004A4A520018182100525A5A00636B6B005A636300181821005A5A
      6300C6C6C6008C8C8C000000000000000000000000000000000000000000CECE
      CE0063636B0042424200525252006B6B6B009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C0063636300B5B5B50000000000000000000000000000000000CECE
      CE005A5A630018182100313139007373730094949400181821006B737300ADAD
      AD00ADADAD006B6B6B00B5B5B500000000000000000000000000848C8C00ADAD
      BD00949CA500848C9400848C94008C8C94008C8C94008C8C94008C949400ADAD
      B500C6C6C60094734A00AD7B4A00EFE7D6000000000000000000848C8C00ADAD
      BD008C949400212129004A5252008C8C94008C8C94008C8C940063636B001818
      2100A5A5AD008C8C8C0000000000000000000000000000000000000000000000
      000000000000FFFFFF00BDBDC6006B737300393942007B7B7B009C9C9C009C9C
      9C00949494004A52520000000000000000000000000000000000000000000000
      00005A5A630039394200BDBDBD00737373003942420052525200182121009494
      9C009CA5A5005252520000000000000000000000000000000000848C8C00ADAD
      BD00B5B5BD00A5ADB500ADADB500ADADB500ADB5B500ADB5B500B5B5B500CECE
      CE00C6C6C6008C8C8C00CEB59400E7D6C6000000000000000000848C8C00ADAD
      BD00424A520031313900A5ADAD00ADADB500ADB5B500ADB5B500B5B5B5004A4A
      52003939420084848C0000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000007B7B84004A5252006B6B
      6B0042424A00000000000000000000000000000000000000000000000000A5A5
      A50018182100D6D6D6000000000000000000000000007B7B8400212129002929
      310042424A000000000000000000000000000000000000000000848C8C00ADAD
      BD00E7EFF700E7EFF700EFEFF700EFF7FF00F7F7FF00F7F7FF00FFFFFF000000
      0000C6C6C6008C8C8C0000000000000000000000000000000000848C8C009494
      9C005A636B00C6C6CE00EFEFF700EFF7FF00F7F7FF00F7F7FF00FFFFFF00E7E7
      E700525252006B6B730000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E7E7E700A5A5
      A500CECECE000000000000000000000000000000000000000000DEDEE7002121
      2900848C8C000000000000000000000000000000000000000000C6C6C6001818
      2100737B7B00000000000000000000000000000000000000000084848C00848C
      9400B5BDC600B5BDC600BDBDC600BDC6C600BDC6C600C6C6CE00C6C6CE00C6C6
      CE0094949C008C8C8C000000000000000000000000000000000084848C00848C
      9400B5BDC600B5BDC600BDBDC600BDC6C600BDC6C600C6C6CE00C6C6CE00C6C6
      CE0094949C008C8C8C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E7E7E700CECE
      CE00FFFFFF00000000000000000000000000000000000000000000000000DEDE
      DE00CED6D6000000000000000000000000000000000000000000B5B5B5007B7B
      84007B7B84007B7B84007B7B84007B7B84007B8484007B8484007B8484007B84
      84007B848400B5B5B50000000000000000000000000000000000B5B5B5007B7B
      84007B7B84007B7B84007B7B84007B7B84007B8484007B8484007B8484007B84
      84007B848400B5B5B50000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000B5B5B5007B7B
      84007B7B84007B7B84007B7B84007B7B84007B8484007B8484007B8484007B84
      84007B848400B5B5B50000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000084848C00848C
      9400B5BDC600B5BDC600BDBDC600BDC6C600BDC6C600C6C6CE00C6C6CE00C6C6
      CE0094949C008C8C8C0000000000000000000000000000000000E7EFF700E7EF
      F700E7EFF700E7EFF700EFEFF70073737B0063636B0073737B00737B7B005A5A
      6300E7EFF700E7EFF70000000000000000000000000000000000A5ADB500DEE7
      EF00E7EFF700E7EFF700EFEFF700EFEFF700EFEFF700F7F7FF00D6DEDE004A52
      5200FFFFFF00B5B5B50000000000000000000000000000000000A5ADB500DEE7
      EF00E7EFF700E7EFF700EFEFF700EFEFF700EFEFF700F7F7FF00D6DEDE004A52
      5200FFFFFF00B5B5B50000000000000000000000000000000000848C8C00ADAD
      BD00B5BDC600ADB5BD00B5B5BD00B5B5BD00B5BDBD00B5BDBD00BDBDBD00D6D6
      DE00C6C6C6008C8C8C0000000000000000000000000000000000E7EFF700E7EF
      F700E7EFF700E7EFF700EFEFF70073737B00A5A5A500CECECE00D6D6D6007B7B
      7B00E7EFF700E7EFF70000000000000000000000000000000000A5ADB500DEE7
      EF00E7EFF700E7EFF700EFEFF700EFEFF700EFEFF700BDBDC60063636B00F7F7
      FF00FFFFFF00B5B5B50000000000000000000000000000000000A5ADB500DEE7
      EF00E7EFF700E7EFF700EFEFF700EFEFF700EFEFF700BDBDC60063636B00F7F7
      FF00FFFFFF00B5B5B50000000000000000000000000000000000848C8C00ADAD
      BD0094949C007B848C0084848C0084848C0084848C0084848C00848C8C00B5BD
      BD00C6C6C6008C8C8C0000000000000000000000000000000000E7EFF700E7EF
      F700E7EFF700E7EFF700E7EFF70031393900A5A5A500CECECE00D6D6D6007B7B
      7B00E7EFF700E7EFF70000000000000000000000000000000000A5ADB500BDC6
      CE005A6373005A6373005A6373005A63730039525A00314A5200636B7300EFEF
      EF00FFFFFF00B5B5B50000000000000000000000000000000000A5ADB500BDC6
      CE0063636B006363630063636300636363004A524A00424242006B6B6B00EFEF
      EF00FFFFFF00B5B5B50000000000000000000000000000000000848C8C00ADAD
      BD007B848C006B6B73006B6B73006B6B73006B6B73006B7373006B737300D6D6
      D600C6C6C6008C8C8C00000000000000000000000000000000008C8C94009C9C
      A5009C9CA5007B84840042424A006B6B7300636363006B737300737373005A5A
      6300E7EFF700E7EFF70000000000000000000000000000000000A5ADB500BDBD
      C60021396B001052BD001863C600187BC600008CCE0000A5D600294A5A00EFEF
      EF00FFFFFF00B5B5B50000000000000000000000000000000000A5ADB500BDBD
      C6004A4239007B634200846B42008C7B42009C8C3900A59431004A4A3900EFEF
      EF00FFFFFF00B5B5B50000000000000000000000000000000000848C8C00ADAD
      BD00C6CED600C6CED600C6CED600CECED600CECED600CED6D600D6D6D600EFF7
      F700C6C6C6008C8C8C0000000000000000000000000000000000636B73001039
      7300185A9400087B9C0021526300E7EFF700E7EFF700E7EFF700E7EFF700E7EF
      F700E7EFF700E7EFF70000000000000000000000000000000000A5ADB500BDBD
      C600293973001063E700187BE700188CEF0000ADF70000C6FF00294A5A00EFEF
      EF00FFFFFF00B5B5B50000000000000000000000000000000000A5ADB500BDBD
      C6004A4239008C734A009C7B5200AD8C4A00BDA54200C6B539004A4A3900EFEF
      EF00FFFFFF00B5B5B50000000000000000000000000000000000848C8C00ADAD
      BD00737B84005A6363005A636B0063636B0063636B0063636B0063636B006363
      6B00ADADAD008C8C8C0000000000000000000000000000000000636B7300104A
      9C00188CEF0000C6FF00215A6B00E7EFF700EFEFF700E7EFF700E7EFF700E7EF
      F700E7EFF700E7EFF70000000000000000000000000000000000A5ADB500BDBD
      C600293973001063E700187BE700188CEF0000ADF70000C6FF00294A5A00EFEF
      EF00FFFFFF00B5B5B50000000000000000000000000000000000A5ADB500BDBD
      C6004A4239008C734A009C7B5200AD8C4A00BDA54200C6B539004A4A3900EFEF
      EF00FFFFFF00B5B5B50000000000000000000000000000000000848C8C00ADAD
      BD00CED6DE00CECED600CECED600CED6DE00D6D6DE00D6D6DE00D6DEDE00DEDE
      DE00BDBDBD008C8C8C0000000000000000000000000000000000636B7300104A
      9C00188CEF0000C6FF00215A6B00E7EFF700E7EFF700E7EFF700E7EFF700E7EF
      F700E7EFF700E7EFF70000000000000000000000000000000000A5ADB500BDBD
      C600293973001063E700187BE700188CEF0000C6FF0000C6FF00294A5A00EFEF
      EF00FFFFFF00B5B5B50000000000000000000000000000000000A5ADB500BDBD
      C6004A4239008C734A009C7B5200AD8C4A00C6B53900C6B539004A4A3900EFEF
      EF00FFFFFF00B5B5B50000000000000000000000000000000000848C8C00ADAD
      BD00636B73004A4A52004A4A52004A4A52004A4A52004A5252004A5252007B84
      8400C6C6C6008C8C8C00000000000000000000000000000000006B6B73002939
      4A0029394A001831390029424A00737B7B0052525A005A5A5A005A5A5A005A5A
      630073737B00E7EFF70000000000000000000000000000000000A5ADB500BDBD
      C600293973001063E700187BE700188CEF0000ADF70000C6FF00294A5A00EFEF
      EF00FFFFFF00B5B5B50000000000000000000000000000000000A5ADB500BDBD
      C6004A4239008C734A009C7B5200AD8C4A00BDA54200C6B539004A4A3900EFEF
      EF00FFFFFF00B5B5B50000000000000000000000000000000000848C8C00ADAD
      BD00CED6DE00CECED600CED6D600CED6DE00D6D6DE00D6D6DE00DEDEDE00E7E7
      E700C6C6C6008C8C8C0000000000000000000000000000000000E7EFF700E7EF
      F700E7EFF700E7EFF7005A5A63006B73730094949400CECECE00D6D6D600BDBD
      BD0073737300E7EFF70000000000000000000000000000000000A5ADB500BDBD
      C600293952002942630029426300294A63001039520021526300314A5200EFEF
      EF00FFFFFF00B5B5B50000000000000000000000000000000000A5ADB500BDBD
      C60042424200424242004A4A39004A4A390042392900525239004A4A3900EFEF
      EF00FFFFFF00B5B5B50000000000000000000000000000000000848C8C00ADAD
      BD007B7B840063636B0063636B0063636B00636B6B00636B6B00636B6B009494
      9C00C6C6C6008C8C8C0000000000000000000000000000000000E7EFF700E7EF
      F700E7EFF700E7EFF700EFEFF7004A4A520094949400CECECE00D6D6D600BDBD
      BD0073737300E7EFF70000000000000000000000000000000000A5ADB500D6DE
      E700CED6DE00CED6DE00D6D6DE00D6DEDE00D6D6DE0042424A00CECECE00F7F7
      FF00FFFFFF00B5B5B50000000000000000000000000000000000A5ADB500D6DE
      E700CED6DE00CED6DE00D6D6DE00D6DEDE00D6D6DE0042424A00CECECE00F7F7
      FF00FFFFFF00B5B5B50000000000000000000000000000000000848C8C00ADAD
      BD00949CA500848C9400848C94008C8C94008C8C94008C8C94008C949400ADAD
      B500C6C6C6008C8C8C0000000000000000000000000000000000E7EFF700E7EF
      F700E7EFF700E7EFF700EFEFF700848C8C007B7B7B00B5B5B500BDBDBD00949C
      9C0073737300E7EFF70000000000000000000000000000000000A5ADB500DEE7
      EF00E7EFF700E7EFF700EFEFF700EFEFF700EFEFF700F7F7FF005A636300C6CE
      CE00FFFFFF00B5B5B50000000000000000000000000000000000A5ADB500DEE7
      EF00E7EFF700E7EFF700EFEFF700EFEFF700EFEFF700F7F7FF005A636300C6CE
      CE00FFFFFF00B5B5B50000000000000000000000000000000000848C8C00ADAD
      BD00B5B5BD00A5ADB500ADADB500ADADB500ADB5B500ADB5B500B5B5B500CECE
      CE00C6C6C6008C8C8C0000000000000000000000000000000000E7EFF700E7EF
      F700E7EFF700E7EFF700E7EFF700E7EFF70063636B0063636B0063636B006363
      6B009C9C9C00E7EFF70000000000000000000000000000000000A5ADB500DEE7
      EF00E7EFF700E7EFF700EFEFF700EFEFF700EFEFF700F7F7FF00F7F7FF007B7B
      8400ADADAD00B5B5B50000000000000000000000000000000000A5ADB500DEE7
      EF00E7EFF700E7EFF700EFEFF700EFEFF700EFEFF700F7F7FF00F7F7FF007B7B
      8400ADADAD00B5B5B50000000000000000000000000000000000848C8C00ADAD
      BD00E7EFF700E7EFF700EFEFF700EFF7FF00F7F7FF00F7F7FF00FFFFFF000000
      0000C6C6C6008C8C8C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000084848C00848C
      9400B5BDC600B5BDC600BDBDC600BDC6C600BDC6C600C6C6CE00C6C6CE00C6C6
      CE0094949C008C8C8C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000B5B5B5007B7B
      84007B7B84007B7B84007B7B84007B7B84007B8484007B8484007B8484007B84
      84007B848400B5B5B5000000000000000000424D3E000000000000003E000000
      2800000040000000600000000100010000000000000300000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF00000000FE0FFFFF00000000
      F807FC3F00000000F003F00F00000000E381F00700000000C181E00300000000
      C01FC00100000000803FC00100000000803FC00100000000000FC00100000000
      0007E003000000000703F00700000000FF03F80F00000000FF83FC1F00000000
      FFC3FFFF00000000FFFFFFFF00000000E0608001FFFFFFFF00008001C001C001
      00008003C001C00100008183C001C0010000C183C001C0010000C183C001C001
      0000C183C001C0010000C007C001C0010000E007C001C0010000E187C001C001
      0000E187C001C0010000E18FC001C0010000E18FC001C0010000F00FC001C001
      0000F00FC001C0010000F01FFFFFFFFFFFFFFFFFFFFF8001E007FFE3F8038001
      E007FFC1F8018003E007FFC1F8018183E007FF81F801C183E007FF03F803C183
      E007FF03F803C183E007FE07F807C183E007FE0FF80FE001E007841F800FE181
      E007801F801FE183E007800F800FE189E007801F801FE18FE007E01FE01FF00F
      E007F03FF03FF00FFFFFFFFFFFFFF01FFFFFFFFFFFFFF003FE7FE007E007F003
      C23FEFF7E007F373C03FEEF7E00700138007EC77E00700138003EAB7E0070013
      0000EAB7E00700330000EC77E00700130000EAF7E0070013C001EAB7E0070033
      E001EC37E0070033F803EEF7E0070003FF87EFF7E0070003FFC7EFF7E007003F
      FFFFE007E007003FFFFFFFFFFFFF003FC003C003FFFFFFFFC003C003FFFFC7C7
      C003C003C3FFC3C7C003C003C07FC00FC003C00380078007C003C00380038003
      C003C00380010000C003C00380010000C003C00380010000C003C003C001C001
      C001C003E001E001C000C003F803F003C000C003FF87E387C013C003FFC7C7C7
      C003C003FFFFC7E7C003C003FFFFFFFFFFFFFFFFFFFFC003800180018001C003
      800180018001C003800180018001C003800180018001C003800180018001C003
      800180018001C003800180018001C003800180018001C003800180018001C003
      800180018001C003800180018001C003800180018001C003800180018001C013
      800180018001C003FFFFFFFFFFFFC00300000000000000000000000000000000
      000000000000}
  end
  object ObtenerOrdenesServicio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerOrdenesServicio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Categoria'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoOrdenServicio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@RetornarSoloContactos'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 72
    Top = 120
  end
  object QryObtenerOSCustomizadasPorCategoria: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'CategoriaOrdenServicio'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT '
      '  CodigoWorkFlow '
      'FROM '
      '  WorkFlows '
      'WHERE '
      '  CategoriaOrdenServicio = :CategoriaOrdenServicio '
      '  AND CodigoWorkFLow > 200')
    Left = 740
    Top = 430
  end
  object btContacto: TBuscaTabla
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HelpContext = 0
    OnProcess = btContactoProcess
    OnSelect = btContactoSelect
    Left = 724
    Top = 399
  end
  object qryClientes: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <>
    SQL.Strings = (
      'SELECT'
      'CodigoPersona AS Codigo,'
      
        'Apellido + IsNull('#39' '#39'+ ApellidoMaterno,'#39#39') + IsNull('#39', '#39'+ Nombre' +
        ','#39#39') as ApellidoNombre'
      'FROM '
      'Personas')
    Left = 696
    Top = 460
  end
  object qryContactos: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <>
    SQL.Strings = (
      'SELECT'
      'CodigoContacto AS Codigo,'
      'Apellido + '#39', '#39' + Nombre As ApellidoNombre'
      'FROM'
      'Contactos')
    Left = 726
    Top = 460
  end
  object qryLegajos: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <>
    SQL.Strings = (
      'SELECT'
      'Legajo AS Codigo,'
      'Apellido + '#39', '#39' + Nombre As ApellidoNombre'
      'FROM'
      'MaestroPersonal')
    Left = 756
    Top = 460
  end
  object QryCuentaCliente: TADOQuery
    Connection = DMConnections.BaseCOP
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'ContextMark'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Size = 1
        Value = Null
      end
      item
        Name = 'ContractSerialNumber'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        Precision = 11
        Size = 19
        Value = Null
      end>
    SQL.Strings = (
      'SELECT '
      '  CodigoCliente,'
      '  CodigoCuenta'
      'FROM'
      '  CuentasTAGs'
      'WHERE'
      '  ContextMark = :ContextMark '
      '  AND ContractSerialNumber = :ContractSerialNumber'
      ''
      '')
    Left = 709
    Top = 430
  end
  object ObtenerPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerPersona;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCuenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end>
    Left = 632
    Top = 188
  end
  object ObtenerDatosDomicilio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDatosDomicilio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDomicilio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 696
    Top = 188
  end
end
