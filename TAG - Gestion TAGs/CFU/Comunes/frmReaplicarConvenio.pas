{-----------------------------------------------------------------------------
 File Name: frmReaplicarConvenio.pas
 Author:    jconcheyro
 Date Created: 18/08/2006
 Language: ES-AR
 Description: Sirve para lanzar el store ReaplicarConvenio, el cual busca pagos
 con saldo y los intenta aplicar contra comprobantes pendientes.

 Revision: 1
 Author: nefernandez
 Date: 16/03/2007
 Description : Se pasa el parametro CodigoUsuario al SP "ReaplicarConvenio"
 (para Auditoria).

  Revision : 3
     Author : FSandi
     Date : 04-10-2007
     Description : Se realiza una asignacin de los parametros de salida del SP
                spObtenerDatosReemisionConvenio para asegurarnos que llegan de tipo
                BIGINT (esto estaba generando un error de conversion variant).

Firma       :   SS_1120_MVI_20130930
Descripcion :   Se crea llamado a DrawItem para verificar si el combo del convenio
                posee alg�n convenio con el texto "(Baja)" y en caso de encontrarlo
                llamar a la funci�n que lo colorear� en rojo.
                Adem�s se modifica el estilo del combo a vcsOwnerDrawFixed.

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

 -----------------------------------------------------------------------------}
unit frmReaplicarConvenio;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ListBoxEx, DBListEx, StdCtrls, DPSControls, VariantComboBox,
  DmiCtrls, DB, ADODB, DMCOnnection, utilDB, utilProc, buscaClientes,
  PeaTypes, RStrings, DBCtrls, peaProcs, Util, ExtCtrls, DBClient;

const
	KEY_F9 	= VK_F9;

type
  TformReaplicarConvenio = class(TForm)
  	gbPorCliente: TGroupBox;
    lb_CodigoCliente: TLabel;
    lbl_NumeroConvenio: TLabel;
    pe_NumeroDocumento: TPickEdit;
    cb_Convenios: TVariantComboBox;
    gb_DatosCliente: TGroupBox;
    Label6: TLabel;
    lblNombre: TLabel;
    lblDomicilio: TLabel;
    Label10: TLabel;
    sp_ObtenerConvenio: TADOStoredProc;
    Label8: TLabel;
    lblRUT: TLabel;
    Timer1: TTimer;
    btnReaplicar: TButton;
    btn_Salir: TButton;
    spReaplicarConvenio: TADOStoredProc;
    lblSaldoDebitos: TLabel;
    lblSaldoDebitosImporte: TLabel;
    lblSaldoCreditos: TLabel;
    lblSaldoCreditosImporte: TLabel;
    spObtenerDatosReemisionConvenio: TADOStoredProc;
    procedure pe_NumeroDocumentoChange(Sender: TObject);
	procedure cb_ConveniosChange(Sender: TObject);
	procedure FormCreate(Sender: TObject);
    procedure pe_NumeroDocumentoButtonClick(Sender: TObject);
	procedure btn_SalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnReaplicarClick(Sender: TObject);
	procedure FormKeyDown(Sender: TObject; var Key: Word;
	  Shift: TShiftState);
	procedure Timer1Timer(Sender: TObject);
    procedure cb_ConveniosDrawItem(Control: TWinControl; Index: Integer;             //SS_1120_MVI_20130930
      Rect: TRect; State: TOwnerDrawState);                                          //SS_1120_MVI_20130930
  private
	{ Private declarations }
	FUltimaBusqueda: TBusquedaCliente;
    FCodigoCliente: integer;
    FCodigoConvenio: integer;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    procedure MostrarDatosCliente(CodigoCLiente: Integer);
	procedure LimpiarCampos;
    procedure LimpiarConvenios;
    procedure LimpiarDetalles;
    procedure LimpiarDatosCliente;
	procedure Reaplicar;
    procedure RefrescarDatosSaldosConvenio;
  public
    { Public declarations }
	function Inicializar: Boolean;
  end;

var
  formReaplicarConvenio: TformReaplicarConvenio;

implementation


{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    jconcheyro
  Date Created: 10/08/2005
  Description: Inicializa el Form - Carga los tipos de Comprobantes
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TformReaplicarConvenio.Inicializar: Boolean;
resourcestring
	MSG_CAPTION             = 'Reaplicar Convenio';
	MSG_ERROR_INICIALIZAR   = 'Error inicializando el formulario.';
begin
    try
        FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
        FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
        FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
        FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
        Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

        FCodigoCliente := -1;
        // Por ahora lo cargamos a mano
        LimpiarDatosCliente;
        LimpiarCampos;
        FormStyle := fsMDIChild;
        CenterForm(Self);

        pe_NumeroDocumento.SetFocus;
        Result := True;
    except
	   	on e: exception do begin
			MsgBoxErr(MSG_ERROR_INICIALIZAR, E.Message, MSG_CAPTION, MB_ICONERROR);
			Result := False;
        end;
    end; // except
end;

procedure TformReaplicarConvenio.FormCreate(Sender: TObject);
begin
    CenterForm(Self);
end;

{-----------------------------------------------------------------------------
  Function Name: peNumeroDocumentoChange
  Author:    jconcheyro
  Date Created: 20/12/2004
  Description: Actualiza los Convenios y Comprobantes Impagos de un cliente determinado
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformReaplicarConvenio.pe_NumeroDocumentoChange(Sender: TObject);
begin
    if ActiveControl <> Sender then Exit;
    LimpiarDetalles;
    LimpiarConvenios;
    FCodigoCliente := QueryGetValueInt(DMConnections.BaseCAC, Format(
      'SELECT dbo.ObtenerCodigoPersonaRUT(''%s'')', [Trim(PadL(pe_NumeroDocumento.Text, 9, '0'))]));

	if FCodigoCliente > 0 then begin
		CargarConveniosRUT(DMConnections.BaseCAC, cb_Convenios, 'RUT', pe_NumeroDocumento.Text, False, 0, True);
    	MostrarDatosCliente(FCodigoCliente);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: cb_ConveniosChange
  Author:    jconcheyro
  Date Created: 10/08/2005
  Description: Procesa un Cambio de Convenio - Trae los comprobantes impagos
				del dicho Convenio
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformReaplicarConvenio.cb_ConveniosChange(Sender: TObject);
begin
	if ActiveControl <> Sender then Exit;
    btnReaplicar.Enabled := (cb_Convenios.ItemIndex >= 1);
    FCodigoConvenio := cb_Convenios.Value;
    RefrescarDatosSaldosConvenio;
end;


//BEGIN:SS_1120_MVI_20130930 -------------------------------------------------
{-----------------------------------------------------------------------------
  Procedure: TformReaplicarConvenio.cb_ConveniosDrawItem
  Date:      30-September-2013
  Description: Verifica si existe alg�n convenio de baja para llamar a la funci�n
                que colorea de rojo el item.
-----------------------------------------------------------------------------}
procedure TformReaplicarConvenio.cb_ConveniosDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin

  if Pos( TXT_CONVENIO_DE_BAJA, cb_Convenios.Items[Index].Caption ) > 0 then  begin

     ItemComboBoxColor(Control, Index, Rect, State, clred, true);

  end
  else begin

     ItemComboBoxColor(Control, Index, Rect, State);

  end;

end;
// END: SS_1120_MVI_20130930 ----------------------------------------------------------------------------------

{-----------------------------------------------------------------------------
  Function Name: MostrarDatosCliente
  Author:    jconcheyro
  Date Created: 18/08/2006
  Description:  Muestra los datos del Cliente
  Parameters: CodigoCliente: Integer
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformReaplicarConvenio.MostrarDatosCliente(CodigoCliente: Integer);
begin
	// Cargamos la informaci�n del cliente.
    lblRUT.Caption := QueryGetValue(DMConnections.BaseCAC, Format(
      'SELECT dbo.ObtenerRUTPersona(%d)', [CodigoCliente]));
    lblNombre.Caption := QueryGetValue(DMConnections.BaseCAC, Format(
	  'SELECT dbo.ObtenerNombrePersona(%d)', [CodigoCliente]));
	lblDomicilio.Caption := QueryGetValue(DMConnections.BaseCAC, Format(
      'SELECT dbo.ObtenerDescripcionDomicilioPrincipalPersona(%d)', [CodigoCliente]));
end;

{-----------------------------------------------------------------------------
  Function Name: LimpiarDetalles;
  Author:    jconcheyro
  Date Created: 18/08/2006
  Description: Borra los detalles del Comprobante
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformReaplicarConvenio.LimpiarDetalles;
begin
	btnReaplicar.Enabled := False;
    lblSaldoCreditosImporte.Caption := '0';
    lblSaldoDebitosImporte.Caption := '0';    
end;

{-----------------------------------------------------------------------------
  Function Name: pe_NumeroDocumentoButtonClick
  Author:    jconcheyro
  Date Created: 18/08/2006
  Description: Procesa un cambio en el RUT
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformReaplicarConvenio.pe_NumeroDocumentoButtonClick(Sender: TObject);
var
    f: TFormBuscaClientes;
begin
    Application.CreateForm(TFormBuscaClientes, F);
	if F.Inicializa(FUltimaBusqueda) then begin
		F.ShowModal;
        if F.ModalResult = idOk then begin
            FUltimaBusqueda := F.UltimaBusqueda;
            pe_NumeroDocumento.Text := f.Persona.NumeroDocumento;
		end;
	end;
    F.free;
end;


{-----------------------------------------------------------------------------
  Function Name: btnReaplicarClick
  Author:    jconcheyro
  Date Created: 10/08/2005
  Description: Llama a Reaplicar.
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformReaplicarConvenio.btnReaplicarClick(Sender: TObject);
begin
    Reaplicar;
end;

{-----------------------------------------------------------------------------
  Function Name: Reaplicar
  Author:    jconcheyro
  Date Created: 18/08/2006
  Description: Lanza el store de Reaplicar un  convenio
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformReaplicarConvenio.Reaplicar;
resourcestring
	MSG_REGISTRO_CORRECTO = 'Proceso ejecutado correctamente.';
	MSG_CAPTION = 'Reaplicar Convenio';
begin
    spReaplicarConvenio.Parameters.Refresh;
    spReaplicarConvenio.Parameters.ParamByName('@CodigoConvenio').Value := FCodigoConvenio;
    // Revision 1
    spReaplicarConvenio.Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;
    spReaplicarConvenio.ExecProc;
    RefrescarDatosSaldosConvenio;
    MsgBox(MSG_REGISTRO_CORRECTO, MSG_CAPTION, MB_ICONINFORMATION + MB_OK);

end;

procedure TformReaplicarConvenio.btn_SalirClick(Sender: TObject);
begin
	Close;
end;

procedure TformReaplicarConvenio.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree;
end;

{-----------------------------------------------------------------------------
  Procedure: TformReaplicarConvenio.LimpiarCampos
  Author:    jconcheyro
  Date:      18/08/2006
  Arguments: None
  Result:    None
-----------------------------------------------------------------------------}
procedure TformReaplicarConvenio.LimpiarCampos;
begin
	pe_NumeroDocumento.Clear;
    FCodigoCliente := -1;
	LimpiarConvenios;
	LimpiarDetalles;
end;

procedure TformReaplicarConvenio.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if (Key = KEY_F9) and btnReaplicar.Enabled then btnReaplicar.Click;
end;

procedure TformReaplicarConvenio.Timer1Timer(Sender: TObject);
begin
    Timer1.Enabled := False;
end;

procedure TformReaplicarConvenio.LimpiarConvenios;
begin
    cb_Convenios.Clear;
    cb_Convenios.Items.Add(SELECCIONAR, 0);
    cb_Convenios.ItemIndex := 0;
end;




procedure TformReaplicarConvenio.LimpiarDatosCliente;
begin
    lblRUT.Caption := EmptyStr;
    lblNombre.Caption := EmptyStr;
    lblDomicilio.Caption := EmptyStr;
end;

procedure TformReaplicarConvenio.RefrescarDatosSaldosConvenio;
var
    SaldoDebitosRetorno, SaldoCreditosRetorno: int64;     //Revision 3
begin

  //recalcular los datos de saldos de comprobantes y saldos de pagos
  spObtenerDatosReemisionConvenio.Parameters.Refresh;
  spObtenerDatosReemisionConvenio.Parameters.ParamByName('@CodigoConvenio').Value := FCodigoConvenio;
  spObtenerDatosReemisionConvenio.Parameters.ParamByName('@SaldoDebitos').Value := 0;
  spObtenerDatosReemisionConvenio.Parameters.ParamByName('@SaldoCreditos').Value := 0;
  spObtenerDatosReemisionConvenio.ExecProc;
  SaldoDebitosRetorno := spObtenerDatosReemisionConvenio.Parameters.ParamByName('@SaldoDebitos').Value;   //Revision 3
  SaldoCreditosRetorno := spObtenerDatosReemisionConvenio.Parameters.ParamByName('@SaldoCreditos').Value; //Revision 3
  lblSaldoDebitosImporte.Caption := FormatearImporteConDecimales(spObtenerDatosReemisionConvenio.Connection, SaldoDebitosRetorno /100, True, False, True);
  lblSaldoCreditosImporte.Caption := FormatearImporteConDecimales(spObtenerDatosReemisionConvenio.Connection, SaldoCreditosRetorno/100, True, False, True);




end;

end.
