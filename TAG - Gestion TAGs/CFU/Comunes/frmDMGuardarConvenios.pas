unit frmDMGuardarConvenios;

interface

uses
  SysUtils, Classes, ADODB, DMConnection, DB;

type
  TDMGuardarConvenios = class(TDataModule)
    spActualizarCuentaAPerdido: TADOStoredProc;
    spBajaCuentaxCambioVehiculo: TADOStoredProc;
    spActivarCuenta: TADOStoredProc;
    spVehiculos: TADOStoredProc;
    spCuentas: TADOStoredProc;
    spBajaCuenta: TADOStoredProc;
    spSuspenderCuenta: TADOStoredProc;
    spActualizarTurnoMovimientoEntrega: TADOStoredProc;
    spActualizarTurnoMovimientoDevolucion: TADOStoredProc;
    spGenerarMovimientoCuentaConTelevia: TADOStoredProc;
    spRegistrarTAGVendido: TADOStoredProc;
    spHabilitarListaBlanca: TADOStoredProc;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DMGuardarConvenios: TDMGuardarConvenios;

implementation

{$R *.dfm}

end.
