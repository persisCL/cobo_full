{********************************** Unit Header ********************************
File Name : FrmAperturaCierrePuntoVenta.pas
Author :
Date Created:
Language : ES-AR
Description :

Revision 1:
Date : 15/09/2005
Description : Al cerrar el turno, se deben ingresar los importes de Cheques,
    Tarjetas de D�bito y de Cr�dito, y Vales Vista.

Revision 2:
Date : 18/04/2007
Description : Al cerrar el turno, se agregaron cuadros para el ingreso de
    cantidad e importe de Pagar�s.

Author: nefernandez
Revision 3:
Date : 08/08/2007
Description : En la funci�n LiquidarTurnoEfectivo, se cambio el tipo de dato
de la variable ImporteEfectivo de Int a Int64.

Author: nefernandez
Revision 4:
Date : 13/08/2007
Description : Le quit� la multiplicaci�n * 100 en la asignaci�n del parametro @ImporteEfectivo
de spAgregarLiquidacionCupones, porque la variable ImporteEfectivo ya estaba multiplicada
por * 100 al llegar a dicha asignaci�n

Author: nefernandez
Revision 5:
Date : 29/08/2007
Description : SS 548: Se cambia el valor de retorno de la funcion
ObtenerSaldoTurnosAbiertosCategorias de Integer a Int64

Author: FSandi
Revision 6:
Date : 24-09-2007
Description : SS 548: Validamos las cantidades maximas a ingresar

Author: mpiazza
Revision 7:
Date : 28/01/2010
Description : SS-510
            - se modifica ActualizarAperturaTurno : Boolean;

*******************************************************************************}
unit FrmAperturaCierrePuntoVenta;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, ADODB, Provider, DB, DBClient,
  Grids, DBGrids, DMConnection, UtilProc, PeaTypes, UtilDB, Util, RStrings,
  PeaProcs, FrmRptCierreTurno, ComCtrls, DmiCtrls, frmConfirmaContrasena, Menus;

type
//Revision 6
   TNewStringGrid = class(TDbGrid)
  function GetCellInEditMode: Boolean;
  function GetEditor : TInplaceEdit;
  public
      property CellInEditMode: Boolean read GetCellInEditMode;
      property Editor : TInplaceEdit read GetEditor;
  end;

  TNewInplaceEditor = class (Tinplaceedit)
  function GetMaxLength: Integer;
  public
     procedure SetMaxLength(Value: Integer);
     property MaxLength: Integer read GetMaxLength write SetMaxLength;
  end;
//Fin de Revision 6

  TTurnoAccion = (Abrir, Cerrar, AdicionarTelevia, AdicionarDinero);

  TFormAperturaCierrePuntoVenta = class(TForm)
    dsAperturaCierre: TDataSource;
    spObtenerCategoriasTAGs: TADOStoredProc;
	cdsTeleviasCategoria: TClientDataSet;
    pnlBottom: TPanel;
    ActualizarTurno: TADOStoredProc;
    ActualizarTurnoMovimiento: TADOStoredProc;
    spVerificarPuntoEntregaVenta: TADOStoredProc;
	pcTurnos: TPageControl;
    tsTelevias: TTabSheet;
    dbgAperturaCierre: TDBGrid;
    tsEfectivo: TTabSheet;
    Label2: TLabel;
    lblMensaje: TLabel;
    txt_Monto: TNumericEdit;
    Label1: TLabel;
    tsLiquidacionEfectivo: TTabSheet;
    spCerrarTurnoLiquidacionCaja: TADOStoredProc;
    spObtenerLiquidacionTurnoEfectivo: TADOStoredProc;
    spAgregarLiquidacionEfectivo: TADOStoredProc;
    spAgregarLiquidacionCupones: TADOStoredProc;
    Label3: TLabel;
	Label4: TLabel;
    edCuponesCredito: TEdit;
    edCheques: TEdit;
    edCuponesDebito: TEdit;
    Label5: TLabel;
    GrillaBilletes: TStringGrid;
    lblValeTelevias: TLabel;
	txtValeTelevias: TEdit;
    spActualizarTurno: TADOStoredProc;
    Bevel1: TBevel;
    lblDisponeTelevias: TLabel;
    lblAca: TLabel;
    Label6: TLabel;
    edValesVista: TEdit;
    btnAceptar: TButton;
    btnCancelar: TButton;
    ne_ImporteCheques: TNumericEdit;
    lbl_Cantidad: TLabel;
    lbl_Importe: TLabel;
    ne_importeTarjetasCredito: TNumericEdit;
    ne_ImporteTarjetasDebito: TNumericEdit;
    ne_ImporteValesVista: TNumericEdit;
    edPagares: TEdit;
    Label7: TLabel;
    ne_ImportePagares: TNumericEdit;
    PopDefecto: TPopupMenu;
    edDepositoAnticipado: TEdit;
    ne_ImporteDepositoAnticipado: TNumericEdit;
    Label8: TLabel;
    spObtenerCuadraturaCierre: TADOStoredProc;
    txtValeEfectivo: TEdit;
    procedure btnAceptarClick(Sender: TObject);
    procedure dbgAperturaCierreKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cdsTeleviasCategoriaAfterInsert(DataSet: TDataSet);
    procedure dbgAperturaCierreKeyPress(Sender: TObject; var Key: Char);
    procedure GrillaBilletesSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
    procedure GrillaBilletesKeyPress(Sender: TObject; var Key: Char);
    procedure pcTurnosChange(Sender: TObject);
    procedure lblAcaClick(Sender: TObject);
    procedure txt_MontoKeyPress(Sender: TObject; var Key: Char);
    procedure edChequesKeyPress(Sender: TObject; var Key: Char);
    procedure btnCancelarClick(Sender: TObject);
  private
    FUsuario : string;
	FCodigoPuntoVenta: integer;
    FCodigoPuntoEntrega: integer;
    FSistemaActual: integer;
    FLastCol: integer;
    FLastRow: integer;
    FEstado: TTurnoAccion;
    function  	ObtenerSaldoTurnosAbiertosCategorias(Conn: TADOConnection; CodigoPuntoEntrega: integer; Categoria: integer): Int64;
	function	ConfirmarAperturaCierre : boolean;
    function	AbrirAdicionarTurno : boolean;
	function 	CerrarTurno : boolean;
	function 	ValidarCantidadesTagsStock : boolean;
    function	HayCategoriasCargadas : boolean;
    function	ActualizarTags : boolean;
    function	ActualizarDinero : boolean;
	function 	LiquidarTurnoEfectivo : boolean;
    function	VerificarPuntoDeEntregaVenta(nCodigoPuntoVenta, nCodigoPuntoEntrega : integer) : boolean;
    function	ValidarDatosTurno : Boolean;
	procedure 	CargarCategoriasTAGs;
	function 	CargarDenominacionesMoneda : boolean;
	function 	ActualizarAperturaTurno: boolean;
	function 	ActualizarCierreTurno: Boolean;
  public
	FNumeroTurno: longint;

    function Inicializar(Caption: TCaption; Usuario: AnsiString; CodigoPuntoVenta: integer;
    					Estado: TTurnoAccion; CodigoPuntoEntrega:integer; NumeroTurno: Integer;
                        bDerechoTelevia: boolean = True; bDerechoDinero :boolean = True ): boolean;
  end;


var
  FormAperturaCierrePuntoVenta: TFormAperturaCierrePuntoVenta;
var
  InfoValidada: Byte;
var
  DifEfectivo: LongInt;
var
  Supervisor: string;

implementation

uses DetalleTurnoCierre;

{$R *.dfm}
//Revision 6
function TNewStringGrid.GetCellInEditMode: Boolean;
begin
  if not Assigned(InplaceEditor) then
    Result := False
  else
    if InplaceEditor.Visible then
      Result := True
    else
      Result := False;
end;

function TNewInplaceEditor.GetMaxLength: Integer;
begin
    Result := inherited maxlength;
end;

procedure TNewInplaceEditor.SetMaxLength(Value: Integer);
begin
    inherited MaxLength := Value
end;
//Fin de Revision 6


{ TFormAperturaCierrePuntoVenta }

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    flamas
  Date Created: 08/02/2005
  Description: Inicializa el Form de Apertura y Cierre de Turno
  Parameters:
  Return Value:
-----------------------------------------------------------------------------}
function TFormAperturaCierrePuntoVenta.Inicializar(Caption: TCaption;
			Usuario: AnsiString; CodigoPuntoVenta: integer; Estado: TTurnoAccion;
            CodigoPuntoEntrega : integer; NumeroTurno : integer;
            bDerechoTelevia: boolean = True; bDerechoDinero :boolean = True): boolean;
begin
    Result := False;

    Self.Caption 		:= Caption;
    FUsuario 			:= Usuario;
    FCodigoPuntoVenta 	:= CodigoPuntoVenta;
    FEstado 			:= Estado;
    FSistemaActual		:= SistemaActual;
    FCodigoPuntoEntrega := CodigoPuntoEntrega;
    FNumeroTurno 		:= NumeroTurno;

    if not ConfirmarAperturaCierre then Exit;

	// Selecciona el Tab de acuerdo al Sistema
	if 	( FSistemaActual = SYS_CAC ) or
    	( FSistemaActual = SYS_CAC_AC )then pcTurnos.ActivePage := tsTelevias
	else if ( FEstado <> Cerrar ) then pcTurnos.ActivePage := tsEfectivo
	else if ( FEstado = Cerrar ) then pcTurnos.ActivePage := tsLiquidacionEfectivo;

	// Verifica los Puntos de Entrega y Venta
	if not VerificarPuntoDeEntregaVenta(FCodigoPuntoVenta, FCodigoPuntoEntrega) then Exit;
    if not ValidarDatosTurno then Exit;

    CargarCategoriasTAGs;
	if (FEstado = Cerrar) then
    	if not CargarDenominacionesMoneda then Exit;

	// Activa las solapas que corresponden
    tsLiquidacionEfectivo.TabVisible 	:= (FEstado = Cerrar) and bDerechoDinero;
    tsEfectivo.TabVisible 				:= not tsLiquidacionEfectivo.TabVisible and (FEstado <> AdicionarTelevia) and bDerechoDinero;
    tsTelevias.TabVisible 				:= (FEstado <> AdicionarDinero) and bDerechoTelevia;
    lblDisponeTelevias.Visible 			:= tsTelevias.TabVisible;
    lblAca.Visible              		:= tsTelevias.TabVisible;
    if tsEfectivo.Enabled and tsEfectivo.Visible then ActiveControl := txtValeEfectivo;
    Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: ConfirmarApertura
  Author:    flamas
  Date Created: 09/02/2005
  Description: Confirma la apertura o cierre de un turno / Pide la contrase�a
  Parameters:
  Return Value:
-----------------------------------------------------------------------------}
function TFormAperturaCierrePuntoVenta.ConfirmarAperturaCierre : boolean;
resourcestring
    CONFIRMATION_OPEN_SHIFT 	= 'Esta seguro de confirmar la apertura del turno ? ' + CRLF +
                       				'Si confirma a continuaci�n se solicita las cantidades iniciales.';
    CAPTION_OPEN_SHIFT 			= 'Apertura de turno';
    CONFIRMATION_CLOSE_SHIFT	= '�Est� seguro de que desea cerrar su turno n�mero %d?';
    CAPTION_CLOSE_SHIFT   		= 'Cerrar Turno';
    CONFIRMATION_ADD_TAGS		= '�Est� seguro de que desea adicionar telev�as?';
    CAPTION_CLOSE_ADD_TAGS		= 'Adicionar telev�as';
    CONFIRMATION_ADD_MONEY		= '�Est� seguro de que desea adicionar dinero?';
    CAPTION_CLOSE_ADD_MONEY		= 'Adicionar dinero';
var
	sMessage : string;
    sCaption : string;
    fContrasenia: TFormConfirmaContrasena;
begin
	result := False;

	// Selecciona el Mensaje seg�n el tipo de operaci�n
    if (FEstado = Abrir) then begin
		sMessage := CONFIRMATION_OPEN_SHIFT;
    	sCaption := CAPTION_OPEN_SHIFT;
    end else if (FEstado = Cerrar) then begin
		sMessage := Format(CONFIRMATION_CLOSE_SHIFT, [FNumeroTurno]);
    	sCaption := CAPTION_CLOSE_SHIFT;
    end else if (FEstado = AdicionarTelevia) then begin
		sMessage := Format(CONFIRMATION_ADD_TAGS, [FNumeroTurno]);
    	sCaption := CAPTION_CLOSE_ADD_TAGS;
    end else if (FEstado = AdicionarDinero) then begin
		sMessage := Format(CONFIRMATION_ADD_MONEY, [FNumeroTurno]);
    	sCaption := CAPTION_CLOSE_ADD_MONEY;
    end;

    if MsgBox(sMessage, sCaption, MB_ICONQUESTION or MB_YESNO) <> IDYES then Exit;

    // Si confirm� la operaci�n, solicita la Contrase�a
	try
    	Application.CreateForm(TFormConfirmaContrasena, fContrasenia);
		fContrasenia.Inicializar(UsuarioSistema);
    	if (fContrasenia.ShowModal <> mrOk) then begin
        	fContrasenia.Release;
        	Exit;
    	end;

        Result := True;
	finally
			fContrasenia.Release;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: VerificarPuntoDeEntregaVenta
  Author:    flamas
  Date Created: 08/02/2005
  Description: Verifica los Puntos de Entrega y de Venta
  Parameters:
  Return Value:
-----------------------------------------------------------------------------}
function TFormAperturaCierrePuntoVenta.VerificarPuntoDeEntregaVenta(nCodigoPuntoVenta, nCodigoPuntoEntrega : integer): boolean;
resourcestring
   MSG_ERROR_SELL_POINT = 'Los valores configurados para el punto de entrega o venta son inv�lidos';
begin
	Result := False;

	// Verifica el Punto de Entrega
    if (FEstado <> Cerrar) and (nCodigoPuntoEntrega < 1) then begin
        Msgbox(MSG_ERROR_PUNTO_ENTREGA, self.Caption, MB_ICONSTOP);
		Exit;
    end;

	// Verifica el Punto de Venta
    if (nCodigoPuntoVenta < 1) then begin
        Msgbox(MSG_ERROR_PUNTO_VENTA, self.Caption, MB_ICONSTOP);
        Exit;
    end;

	// Verifica el Punto de Entrega
    with spVerificarPuntoEntregaVenta do begin
        Parameters.Refresh;
        Parameters.ParamByName('@CodigoPuntoEntrega').Value := nCodigoPuntoEntrega;
        Parameters.ParamByName('@CodigoPuntoVenta').Value 	:= nCodigoPuntoVenta;
        ExecProc;

        if Parameters.ParamByName('@Valido').Value = 0 then begin
			MsgBoxErr(MSG_ERROR_SELL_POINT,'',self.Caption,MB_ICONERROR);
            Exit;
        end;
    end;

	Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: ValidarDatosTurno
  Author:    flamas
  Date Created: 08/02/2005
  Description: Valida los datos del Turno
  Parameters:
  Return Value:
-----------------------------------------------------------------------------}
function TFormAperturaCierrePuntoVenta.ValidarDatosTurno : boolean;
var
    DatosTurno: TDatosTurno;
    cEstadoAbierto : string;
begin
	Result := False;
    (* Obtener los datos del turno para el Punto de Entrega y de Venta. *)
	if not ObtenerDatosTurno(DMConnections.BaseCAC, FCodigoPuntoEntrega,
    							FCodigoPuntoVenta, DatosTurno) then Exit;

    (* Obtener el valor que indica Turno Abierto. *)
    cEstadoAbierto := Trim(QueryGetValue(DMConnections.BaseCAC, 'SELECT DBO.CONST_TURNO_ESTADO_ABIERTO()'));

	// Hago las validaciones
    if (FEstado = Abrir) and (Trim(DatosTurno.Estado) = cEstadoAbierto ) then begin
        (* Si se quiere Abrir un Turno y ya existe un Turno Abierto, NO permitir la apertura. *)
        if Uppercase(Trim(DatosTurno.CodigoUsuario)) <> UpperCase(Trim(FUsuario)) then begin
        	MsgBox(format(MSG_ERROR_TURNO_ABIERTO_OTRO_USUARIO, [UpperCase(Trim(DatosTurno.CodigoUsuario))]), Self.Caption, MB_ICONSTOP);
            Exit;
        end else begin
        	MsgBox(Format(MSG_ERROR_TURNO_ABIERTO_MISMO_USUARIO, [UpperCase(Trim(DatosTurno.CodigoUsuario))]), Self.Caption, MB_ICONSTOP);
            Exit;
        end;
    end else begin
        if ((FEstado In [Cerrar, AdicionarTelevia, AdicionarDinero]) and (Trim(DatosTurno.Estado) <> cEstadoAbierto)) then begin
            (* Si se intenta Cerrar, Adicionar Telev�as o Dinero, y el turno
            NO est� Abierto, NO permitir la operaci�n. *)
            MsgBox(MSG_ERROR_TURNO_CERRADO, Self.Caption, MB_ICONSTOP);
            Exit;
        end else begin
        	if ((FEstado = Cerrar) and ((UpperCase(Trim(DatosTurno.CodigoUsuario)) <> UpperCase(FUsuario)) and
                	not ExisteAcceso('mnu_CerrarTurnosTerceros'))) then begin
                (* Si se intenta Cerrar el turno y el usuario del turno NO es el
                usuario que intenta cerrarlo (FUsuario) y FUsuario NO tiene
                permisos para cerrar turnos de terceros, NO permitir cerrarlo. *)
            	MsgBox(MSG_ERROR_TURNO_CERRADO_USUARIO, Self.Caption, MB_ICONSTOP);
                Exit;
            end else begin
            	if ((FEstado = AdicionarTelevia) and (upperCase(Trim(DatosTurno.CodigoUsuario)) <> UpperCase(FUsuario))) then begin
                    (* Si se intenta Adicionar Telev�as y el usuario del turno
                    NO es el que intenta adicionar, NO permitir la adici�n. *)
                	MsgBox(MSG_ERROR_ADICIONAR_TELEVIA_USUARIO, Self.Caption, MB_ICONSTOP);
                    Exit;
                end else if ((FEstado = AdicionarDinero) and (upperCase(Trim(DatosTurno.CodigoUsuario)) <> UpperCase(FUsuario))) then begin
                    (* Si se intenta Adicionar Dinero y el usuario del turno
                    NO es el que intenta adicionar, NO permitir la adici�n. *)
                	MsgBox(MSG_ERROR_ADICIONAR_DINERO_USUARIO, Self.Caption, MB_ICONSTOP);
					Exit;
                end;
            end;
        end;
    end;
    Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarCategoriasTAGs;
  Author:    flamas
  Date Created: 08/02/2005
  Description: Carga las Categor�as de TAGS
  Parameters:
  Return Value:
-----------------------------------------------------------------------------}
procedure TFormAperturaCierrePuntoVenta.CargarCategoriasTAGs;
begin
	spObtenerCategoriasTAGs.Close;
    spObtenerCategoriasTAGs.Open;

    cdsTeleviasCategoria.CreateDataSet;
	cdsTeleviasCategoria.Open;

    spObtenerCategoriasTAGs.First;

    while not spObtenerCategoriasTAGs.Eof do begin
    	cdsTeleviasCategoria.Append;
        cdsTeleviasCategoria.FieldByName('Categoria').AsInteger := spObtenerCategoriasTAGs.FieldByName('Categoria').AsInteger;
        cdsTeleviasCategoria.FieldByName('Descripcion').AsString := spObtenerCategoriasTAGs.FieldByName('Descripcion').AsString;
        cdsTeleviasCategoria.FieldByName('Cantidad').AsInteger := 0;
        cdsTeleviasCategoria.Post;
        spObtenerCategoriasTAGs.Next;
    end;

    cdsTeleviasCategoria.First;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarDenominacionesMoneda;
  Author:    flamas
  Date Created: 08/02/2005
  Description: Carga las denominaciones de Monedas
  Parameters:
  Return Value:
-----------------------------------------------------------------------------}
function TFormAperturaCierrePuntoVenta.CargarDenominacionesMoneda : boolean;
resourcestring
	CAPTION_MONEY          = 'Moneda';
	CAPTION_VALUE           = 'Valor';
	CAPTION_QUANTITY        = 'Cantidad';
var
	i: integer;
begin
      spObtenerLiquidacionTurnoEfectivo.Parameters.ParamByName( '@NumeroTurno' ).Value := FNumeroTurno;
      result := (OpenTables([spObtenerLiquidacionTurnoEfectivo]));

      {-----------------------------------------------------------------}
      { Carga las denominaciones de los Billetes y la cantidad que hay
      {-----------------------------------------------------------------}
	  GrillaBilletes.Cells[0,0] := CAPTION_MONEY;
	  GrillaBilletes.Cells[1,0] := CAPTION_VALUE;
	  GrillaBilletes.Cells[2,0] := CAPTION_QUANTITY;

      with spObtenerLiquidacionTurnoEfectivo, GrillaBilletes do begin
          i:= 0;
		  RowCount :=  RecordCount + 1;

          while not Eof do begin
              inc(i);
              Cells[0,i] := PadR(Trim(FieldByName('Descripcion').AsString), 300, ' ') + Istr(FieldByName('CodigoDenominacionMoneda').AsInteger);
              Cells[1,i] := '$' + PadL(FloatToStrF(FieldByName('ValorMonedaLocal').AsFloat, ffFixed,8, 0), 9, ' ');
              Cells[2,i] := FieldByName('Cantidad').AsString;
              Next;
          end;

          Close;
          Row := 1;
          Col := 2;
      end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnAceptarClick
  Author:    flamas
  Date Created: 08/02/2005
  Description: Abre o Cierra el Turno
  Parameters:
  Return Value: Sender: TObject
-----------------------------------------------------------------------------}
procedure TFormAperturaCierrePuntoVenta.btnAceptarClick(Sender: TObject);
begin
	if (FEstado In [Abrir, AdicionarTelevia, AdicionarDinero]) then begin
    	if AbrirAdicionarTurno then ModalResult := mrOk;
    end else if FEstado = Cerrar then begin
    	if CerrarTurno then ModalResult := mrOk;
    end;
end;

procedure TFormAperturaCierrePuntoVenta.btnCancelarClick(Sender: TObject);
begin

end;

{-----------------------------------------------------------------------------
  Function Name: AbrirAdicionarTurno
  Author:    flamas
  Date Created: 08/02/2005
  Description: Abre un Turno de Telev�as, Dinero o Ambos o adiciona los mismos
  Parameters:
  Return Value:
-----------------------------------------------------------------------------}
function TFormAperturaCierrePuntoVenta.AbrirAdicionarTurno : boolean;
resourcestring
	MSG_QUESTION_NO_INPUTS_MADE = 'No ingresaron dinero ni telev�as.'#10#13 +
									'� Desea continuar con la apertura de turno ?';
    MSG_MONTO_INVALIDO = 'Monto invalido';
    MSG_VALE_REQUERIDO = 'Ingrese n�mero de vale';
    MSG_MONTO_REQUERIDO = 'Ingrese Monto';
var
    valor:Integer;
begin
	Result := False;
{INICIO: TASK_017_JMA_20160606_Validaci�n de ingreso de numero de vale en caja en Recaudaci�n}
if tsEfectivo.Visible then
begin

    if ((txtValeEfectivo.Text <> '') and (txt_Monto.Text = ''))  then
    begin
        MsgBox(MSG_MONTO_REQUERIDO, Caption, MB_OK);
        Exit;
    end
    else if ((txtValeEfectivo.Text = '') and (txt_Monto.Text <> '')) then
    begin
        MsgBox(MSG_VALE_REQUERIDO, Caption, MB_OK);
        Exit;
    end;
{TERMINO: TASK_017_JMA_20160606_Validaci�n de ingreso de numero de vale en caja en Recaudaci�n}

{INICIO: TASK_016_JMA_20160606_Validaci�n de ingreso de monto en caja en Recaudaci�n}
    if (txt_Monto.Text <> '') and (StrToInt(txt_Monto.Text)<=0) then
    begin
       MsgBox(MSG_MONTO_INVALIDO, Caption, MB_OK);
       Exit;
    end;

end;
{TERMINO: TASK_016_JMA_20160606_Validaci�n de ingreso de monto en caja en Recaudaci�n}

	// Verifica si hay Categor�as Cargadas
    if not (HayCategoriasCargadas) and (txt_Monto.Text = '') and
    	(MsgBox(MSG_QUESTION_NO_INPUTS_MADE, Caption, MB_YESNO + MB_ICONQUESTION) <> IDYES) then Exit;

    if not ValidarCantidadesTagsStock then Exit;

    if not ValidarDatosTurno then Exit;

	try
    	DMConnections.BaseCAC.BeginTrans;
		// Actualiza los Tags y el Dinero
        if  ActualizarAperturaTurno and
        	ActualizarTags and
        	ActualizarDinero then begin
    		DMConnections.BaseCAC.CommitTrans;
            Result := True;
        end else
        	DMConnections.BaseCAC.RollbackTrans;
    except
    	on e: Exception do begin
        	MsgBoxErr(MSG_ERROR_TURNO_GUARDAR, e.message, self.caption, MB_ICONSTOP);
		end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: CerrarTurno
  Author:    flamas
  Date Created: 08/02/2005
  Description: Cierra un Turno de Telev�as, Dinero o Ambos
  Parameters:
  Return Value:
-----------------------------------------------------------------------------}
function TFormAperturaCierrePuntoVenta.CerrarTurno : boolean;
resourcestring
    MSG_IMPORTE_DEBE_SER_MAYOR_O_IGUAL_A_CERO = 'El importe debe ser mayor o igual a cero.';
var
    f: TFormRptCierreTurno;
    fct: TDetalleCierreTurno;
begin
	Result := False;
	if MsgBox(MSG_QUESTION_CERRAR_TURNO,
    			MSG_QUESTION_ELIMINAR_CAPTION, MB_YESNO + MB_ICONQUESTION) <> IDYES then Exit;

    if not ValidarDatosTurno then Exit;

    if not ValidateControls([
            ne_ImporteCheques,
            ne_importeTarjetasCredito,
            ne_ImporteTarjetasDebito,
            ne_ImporteValesVista,
            ne_ImportePagares,  // Revision 2
            ne_ImporteDepositoAnticipado],  // SS_637_PDO_20110503
            [ne_ImporteCheques.Value >= 0,
            ne_importeTarjetasCredito.Value >= 0,
            ne_ImporteTarjetasDebito.Value >= 0,
            ne_ImporteValesVista.Value >= 0,
            ne_ImportePagares.Value >= 0, // Revision 2
            ne_ImporteDepositoAnticipado.Value >= 0], // SS_637_PDO_20110503
            Self.Caption,
            [MSG_IMPORTE_DEBE_SER_MAYOR_O_IGUAL_A_CERO,
            MSG_IMPORTE_DEBE_SER_MAYOR_O_IGUAL_A_CERO,
            MSG_IMPORTE_DEBE_SER_MAYOR_O_IGUAL_A_CERO,
            MSG_IMPORTE_DEBE_SER_MAYOR_O_IGUAL_A_CERO,
            MSG_IMPORTE_DEBE_SER_MAYOR_O_IGUAL_A_CERO, // SS_637_PDO_20110503
            MSG_IMPORTE_DEBE_SER_MAYOR_O_IGUAL_A_CERO]) then Exit; // Revision 2
    
	try
    	DMConnections.BaseCAC.BeginTrans;
		// Actualiza los Tags y el Dinero
        { // JLO INICIO 20160421 CAC-REC-029
    	if ActualizarTags and
        	LiquidarTurnoEfectivo and
        		ActualizarCierreTurno then begin }  //JLO TERMINO 20160421 CAC-REC-029
        if ActualizarTags and
        	LiquidarTurnoEfectivo then begin

    		DMConnections.BaseCAC.CommitTrans;
            Result := True;
    	end else begin
    		DMConnections.BaseCAC.RollbackTrans;
        	MsgBox(MSG_ERROR_TURNO_GUARDAR, self.caption, MB_ICONSTOP);
        end;
    except
    	on e: Exception do begin
    		DMConnections.BaseCAC.RollbackTrans;
        	MsgBoxErr(MSG_ERROR_TURNO_GUARDAR, e.message, self.caption, MB_ICONSTOP);
    	end;
    end;
     //JLO INICIO 20160425
    try
        DifEfectivo := 0;  //JLO 20160526
        Supervisor  := ''; //JLO 20160526

        with spObtenerCuadraturaCierre, Parameters  do begin
            ParamByName('@NumeroTurno').Value := FNumeroTurno;
            ParamByName('@Detalle').Value 	:= 0;

            ExecProc;

            if Parameters.ParamByName('@RETURN_VALUE').Value = 1 then begin
                spObtenerCuadraturaCierre.Close;
                InfoValidada := 1;
                //DifEfectivo := 0;  //JLO 20160526
                //Supervisor  := ''; //JLO 20160526
                Application.CreateForm(TDetalleCierreTurno, fct);
    	        if fct.Inicializar(FNumeroTurno) then
			    fct.ShowModal;

                fct.release;
            end

        end;

        if InfoValidada = 1 then    begin
          	MsgBox(MSG_ERROR_TURNO_GUARDAR, self.caption, MB_ICONSTOP);
            Exit;
        end;

    finally

    end;

     if InfoValidada = 0 then begin  //JLO 20160425
        try
    	    DMConnections.BaseCAC.BeginTrans;
		    // Finaliza cerrando el Turno
    	    if ActualizarCierreTurno then begin

               try
                    // Imprime el Reporte de Liquidaci�n
                    Application.CreateForm(TFormRptCierreTurno, f);
    	            if f.Inicializar(
                                        FCodigoPuntoEntrega,
                                        FCodigoPuntoVenta,
                                        FUsuario,
                                        FNumeroTurno,
    					                tsTelevias.TabVisible,
                                        tsLiquidacionEfectivo.TabVisible) then begin
			        f.Ejecutar;
                    end;
               finally
                    f.Release;
               end;
               
               DMConnections.BaseCAC.CommitTrans;
               Result := True;

    	    end else begin
    		DMConnections.BaseCAC.RollbackTrans;
        	MsgBox(MSG_ERROR_TURNO_GUARDAR, self.caption, MB_ICONSTOP);
            end;

    	except
            on e: Exception do begin
    		DMConnections.BaseCAC.RollbackTrans;
        	MsgBoxErr(MSG_ERROR_TURNO_GUARDAR, e.message, self.caption, MB_ICONSTOP);
    	    end;
         //JLO TERMINO 20160425
        end;
      end;
 end;

{-----------------------------------------------------------------------------
  Function Name: ActualizarTags;
  Author:    flamas
  Date Created: 08/02/2005
  Description: Actualiza la Cantidad de Tags
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFormAperturaCierrePuntoVenta.ActualizarTags : boolean;
var
	TipoMovimiento : integer;
begin
	result := True;
    // Si no hay Categor�as Cargadas, SALE
	if not HayCategoriasCargadas then Exit;

	Screen.Cursor := crHourGlass;

    try
    	// Setea el Tipo de Movimiento
    	case FEstado of
        	Abrir				: TipoMovimiento := TURNO_APERTURA;
            AdicionarTelevia 	: TipoMovimiento := TURNO_ADICIONAR_TAG;
        else
            TipoMovimiento := TURNO_CIERRE;
        end;

        try
        	cdsTeleviasCategoria.DisableControls;
            cdsTeleviasCategoria.First;

            while not cdsTeleviasCategoria.Eof do begin
				if ( cdsTeleviasCategoria.FieldByName('Cantidad').AsInteger > 0 ) then begin
                	ActualizarTurnoMovimiento.Close;
                	ActualizarTurnoMovimiento.Parameters.Refresh;
                	ActualizarTurnoMovimiento.Parameters.ParamByName('@NumeroTurno').Value := FNumeroTurno;
                	ActualizarTurnoMovimiento.Parameters.ParamByName('@CodigoTipoMovimiento').Value := TipoMovimiento;
                	ActualizarTurnoMovimiento.Parameters.ParamByName('@CodigoCategoria').Value := cdsTeleviasCategoria.FieldByName('Categoria').AsInteger;
					ActualizarTurnoMovimiento.Parameters.ParamByName('@Cantidad').Value := cdsTeleviasCategoria.FieldByName('Cantidad').AsInteger;
                	ActualizarTurnoMovimiento.Parameters.ParamByName('@NumeroVale').Value := Trim(txtValeTelevias.Text);
                	ActualizarTurnoMovimiento.ExecProc;
                end;

                cdsTeleviasCategoria.Next;
            end;
            cdsTeleviasCategoria.First;
        except
		    on e: Exception do begin
			    Screen.Cursor := crDefault;
                result := False;
                MsgBoxErr(MSG_ERROR_TURNO_GUARDAR, e.message, self.caption, MB_ICONSTOP);
    		end;
        end;
    finally
        Screen.Cursor := crDefault;
        cdsTeleviasCategoria.EnableControls;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ActualizarDinero;
  Author:    flamas
  Date Created: 08/02/2005
  Description: Actualiza un Movimiento de Dinero
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFormAperturaCierrePuntoVenta.ActualizarDinero : boolean;
var
	TipoMovimiento : integer;
begin
	result := True;
    // Si no hay Dinero Cargado, SALE
	if (txt_Monto.Text = '') then Exit;

	Screen.Cursor := crHourGlass;

    try
    	// Setea el Tipo de Movimiento
    	case FEstado of
        	Abrir			: TipoMovimiento := TURNO_APERTURA;
            AdicionarDinero	: TipoMovimiento := TURNO_ADICIONAR_TAG;
		else
            TipoMovimiento := TURNO_CIERRE;
        end;

        try
        	ActualizarTurnoMovimiento.Close;
            ActualizarTurnoMovimiento.Parameters.Refresh;
            ActualizarTurnoMovimiento.Parameters.ParamByName('@NumeroTurno').Value := FNumeroTurno;
            ActualizarTurnoMovimiento.Parameters.ParamByName('@CodigoTipoMovimiento').Value := TipoMovimiento;
            ActualizarTurnoMovimiento.Parameters.ParamByName('@CodigoCategoria').Value := null;
            ActualizarTurnoMovimiento.Parameters.ParamByName('@Cantidad').Value := txt_Monto.ValueInt * 100;
            ActualizarTurnoMovimiento.Parameters.ParamByName('@NumeroVale').Value := Trim(txtValeEfectivo.Text);
            ActualizarTurnoMovimiento.ExecProc;
        except
		    on e: Exception do begin
			    Screen.Cursor := crDefault;
                result := False;
                MsgBoxErr(MSG_ERROR_TURNO_GUARDAR, e.message, self.caption, MB_ICONSTOP);
    		end;
        end;
    finally
        Screen.Cursor := crDefault;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: LiquidarTurnoEfectivo
  Author:    flamas
  Date Created: 08/02/2005
  Description: Hace la liquidaci�n del Turno en Efectivo
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFormAperturaCierrePuntoVenta.LiquidarTurnoEfectivo : boolean;
resourcestring
	// Mensajes de confirmaci�n
	MSG_GUARDAR_LIQUIDACION     = '�Desea guardar la liquidaci�n con la informaci�n que ve en pantalla?';
	CAPTION_CIERRE_TURNO        = 'Cierre de Turno';
	MSG_IMPRIMIR_DETALLE        = '�Desea imprimir el detalle de la liquidaci�n ahora?';
	CAPTION_IMPRIMIR_DETALLE    = 'Imprimir Detalle Liquidaci�n';

    // Mensajes de error
    MSG_FECHA_CIERRE     	    = 'No se pudo actualizar la fecha de cierre de turno.';
    MSG_DETALLE_LIQUIDACION     = 'No se pudo agregar el detalle de liquidaci�n.';
var
    i : integer;
    ImporteEfectivo: Int64;
begin
	Screen.Cursor := crHourGlass;
	result := False;
    try
        ImporteEfectivo := 0;
        // Actualizo la Cantidad de Efectivo
        for i := 1 to GrillaBilletes.RowCount - 1 do begin
            try
            	if Ival(GrillaBilletes.Cells[2, i]) > 0 then
                   	with spAgregarLiquidacionEfectivo, Parameters, GrillaBilletes do begin
                    	ParamByName('@NumeroTurno').Value 				:= FNumeroTurno;
                		ParamByName('@CodigoDenominacionMoneda').Value 	:= Ival(Trim(strRight(Cells[0, i], 7)));
                		ParamByName('@ValorMonedaLocal').Value 		  	:= StrToFloat(Trim(StrRight(Cells[1, i], 7))) * 100;
                		ParamByName('@Cantidad').Value 					:= Trim(Cells[2, i]);

                        ImporteEfectivo := ImporteEfectivo
                                            + (ParamByName('@ValorMonedaLocal').Value * ParamByName('@Cantidad').Value);

                      	ExecProc;
                   end;
            except
                 on E: Exception do begin
                    MsgBoxErr(MSG_DETALLE_LIQUIDACION, e.message, CAPTION_CIERRE_TURNO, MB_ICONSTOP);
                    Exit;
                 end;
			end;
        end;

        // Actualizo la Cantidad e Importe de Cupones
        try
        	with spAgregarLiquidacionCupones, Parameters do begin
                Parameters.Refresh;
            	ParamByName('@NumeroTurno').Value:= FNumeroTurno;
                ParamByName('@CantidadCheques').Value:= StrToIntDef(edCheques.Text, 0);
                ParamByName('@CantidadCuponesTarjetaDebito').Value := StrToIntDef(edCuponesDebito.Text, 0);
                ParamByName('@CantidadCuponesTarjetaCredito').Value := StrToIntDef(edCuponesCredito.Text, 0);
                ParamByName('@CantidadValesVista').Value := StrToIntDef(edValesVista.Text, 0);
                ParamByName('@CantidadPagares').Value := StrToIntDef(edPagares.Text, 0); // Revision 2
                ParamByName('@CantidadDepositosAnticipados').Value := StrToIntDef(edDepositoAnticipado.Text, 0); // SS_637_PDO_20110503

                ParamByName('@ImporteCheques').Value              := ne_ImporteCheques.ValueInt * 100;
                ParamByName('@ImporteTarjetasDebito').Value       := ne_ImporteTarjetasDebito.ValueInt * 100;
                ParamByName('@ImporteTarjetasCredito').Value      := ne_importeTarjetasCredito.ValueInt * 100;
                ParamByName('@ImporteValesVista').Value           := ne_ImporteValesVista.ValueInt * 100;
                // Revision 4: La variable ImporteEfectivo ya est� multiplicada * 100
                ParamByName('@ImporteEfectivo').Value             := ImporteEfectivo;
                ParamByName('@ImportePagares').Value              := ne_ImportePagares.ValueInt * 100; // Revision 2
                ParamByName('@ImporteDepositosAnticipados').Value := ne_ImporteDepositoAnticipado.ValueInt * 100; // SS_637_PDO_20110503


                ExecProc;
            end;
        except
        	on E: Exception do begin
            	MsgBoxErr(MSG_DETALLE_LIQUIDACION, e.message, CAPTION_CIERRE_TURNO, MB_ICONSTOP);
                Exit;
            end;
        end;
    finally
        Screen.Cursor := crDefault;
    end;

	result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: ValidarCantidadesTagsStock
  Author:    flamas
  Date Created: 08/02/2005
  Description: Valida si la Cantidad de Telev�as ingresada estaba en el Stock
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFormAperturaCierrePuntoVenta.ValidarCantidadesTagsStock : boolean;
var
    AlmacenPuntoEntrega: integer;
begin
	result := False;
	cdsTeleviasCategoria.DisableControls;
	AlmacenPuntoEntrega := ObtenerAlmacenPuntoEntrega(DMConnections.BaseCAC, FCodigoPuntoEntrega);
    cdsTeleviasCategoria.First;

    // Valida si hay Stock de Telev�as en el Almac�n
    while not cdsTeleviasCategoria.Eof do begin
		if (ObtenerStockAlmacen(DMConnections.BaseCAC,AlmacenPuntoEntrega, cdsTeleviasCategoria.FieldByName('Categoria').AsInteger) < cdsTeleviasCategoria.FieldByName('Cantidad').AsInteger) then begin
          pcTurnos.ActivePage := tsTelevias;
          MsgBoxBalloon(MSG_ERROR_STOCK_TAG_TURNO, self.Caption, MB_ICONSTOP, dbgAperturaCierre);
          cdsTeleviasCategoria.EnableControls;
          Exit;
        end;

        if (ObtenerStockAlmacen(DMConnections.BaseCAC,AlmacenPuntoEntrega, cdsTeleviasCategoria.FieldByName('Categoria').AsInteger) <
        	(ObtenerSaldoTurnosAbiertosCategorias(DMConnections.BaseCAC,FCodigoPuntoEntrega, cdsTeleviasCategoria.FieldByName('Categoria').AsInteger) + cdsTeleviasCategoria.FieldByName('Cantidad').AsInteger)) then begin
          pcTurnos.ActivePage := tsTelevias;
          MsgBoxBalloon(MSG_ERROR_STOCK_TAG_TURNO_ASIGNADOS, self.Caption, MB_ICONSTOP, dbgAperturaCierre);
          cdsTeleviasCategoria.EnableControls;
          Exit;
        end;
        cdsTeleviasCategoria.Next;

    end;
    cdsTeleviasCategoria.First;
    cdsTeleviasCategoria.EnableControls;

	result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: HayCategoriasCargadas
  Author:    flamas
  Date Created: 08/02/2005
  Description: Verifica si hay alguna categor�a cargadas
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFormAperturaCierrePuntoVenta.HayCategoriasCargadas : boolean;
begin
	result := False;
	cdsTeleviasCategoria.DisableControls;
    cdsTeleviasCategoria.First;

    // Valida si hay Stock de Telev�as en el Almac�n
    while not cdsTeleviasCategoria.Eof do begin
    	if ( cdsTeleviasCategoria.FieldByName( 'Cantidad' ).AsInteger > 0 ) then begin
        	result := True;
			break;
        end;

		cdsTeleviasCategoria.Next;
    end;

    cdsTeleviasCategoria.First;
    cdsTeleviasCategoria.EnableControls;
end;

{-----------------------------------------------------------------------------
  Function Name: ActualizarAperturaTurno
  Author:    flamas
  Date Created: 08/02/2005
  Description: Ejecuta el Store Procedure que Abre un turno
  Parameters:
  Return Value: Boolean

Revision 1:
Date : 28/01/2010
Description : SS-510 se agrega el parametro @IdEstacion

-----------------------------------------------------------------------------}
function TFormAperturaCierrePuntoVenta.ActualizarAperturaTurno : Boolean;
resourcestring
    MSG_ERROR_OPENING_SHIFT = 'Error actualizando el turno.';
    CAPTION_ERROR =  'Abrir turno';
begin
    // Si adiciona telev�as o dinero, no abre el turno
	if ( FEstado <> Abrir ) then begin
		Result := True;
    	Exit;
    end;

	Result := False;
    try
        spActualizarTurno.Parameters.Refresh;
        spActualizarTurno.Parameters.ParamByName('@NumeroTurno').Value        := null;
        spActualizarTurno.Parameters.ParamByName('@Usuario').Value            := FUsuario;
        spActualizarTurno.Parameters.ParamByName('@CodigoPuntoVenta').Value   := FCodigoPuntoVenta;
        spActualizarTurno.Parameters.ParamByName('@CodigoPuntoEntrega').Value := FCodigoPuntoEntrega;
		    spActualizarTurno.Parameters.ParamByName('@UsuarioCierre').Value      := Null;
		    spActualizarTurno.Parameters.ParamByName('@IdEstacion').Value         := ResolveHostNameStr(GetMachineName);
        spActualizarTurno.ExecProc;
        FNumeroTurno := spActualizarTurno.Parameters.ParamByName('@NumeroTurno').Value;
        Result := True;
    except
    	On e: Exception do begin
        	MsgBoxErr(MSG_ERROR_OPENING_SHIFT, e.Message, CAPTION_ERROR, MB_ICONSTOP);
        end;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: ActualizarCierreTurno
  Author:    flamas
  Date Created: 08/02/2005
  Description: Ejecuta el Store Procedure que Cierra un turno
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFormAperturaCierrePuntoVenta.ActualizarCierreTurno : Boolean;
resourcestring
    MSG_ERROR_INICIALIZACION = 'Error cerrando el turno.';
    CAPTION_ERROR =  'Cerrar turno';
var
    SP: TAdoStoredProc;
begin
    SP := TAdoStoredProc.Create(nil);
    SP.ProcedureName := 'ActualizarTurno';
    try
        spActualizarTurno.Parameters.Refresh;
        spActualizarTurno.Parameters.ParamByName('@NumeroTurno').Value        := FNumeroTurno;
        spActualizarTurno.Parameters.ParamByName('@Usuario').Value            := FUsuario;
        spActualizarTurno.Parameters.ParamByName('@CodigoPuntoVenta').Value   := Null;
		spActualizarTurno.Parameters.ParamByName('@CodigoPuntoEntrega').Value := Null;
		spActualizarTurno.Parameters.ParamByName('@UsuarioCierre').Value      := FUsuario;
        spActualizarTurno.Parameters.ParamByName('@DiferenciaEfectivo').Value := DifEfectivo;
        spActualizarTurno.Parameters.ParamByName('@CodigoSupervisor').Value   := Supervisor;
        spActualizarTurno.ExecProc;
        Result := True;
    except
    	On E: Exception do begin
            result := false;
        	MsgBoxErr(MSG_ERROR_INICIALIZACION, e.Message, CAPTION_ERROR, MB_ICONSTOP);
        end;
	end;
end;

function TFormAperturaCierrePuntoVenta.ObtenerSaldoTurnosAbiertosCategorias(Conn: TADOConnection; CodigoPuntoEntrega, Categoria: integer): Int64;
begin
    Result := QueryGetValueInt(Conn, 'SELECT dbo.ObtenerSaldoTurnosAbiertosCategorias(' + inttostr(FCodigoPuntoEntrega) + ',' + inttostr(Categoria) + ')');
end;

procedure TFormAperturaCierrePuntoVenta.cdsTeleviasCategoriaAfterInsert(DataSet: TDataSet);
begin
    if visible then begin
        if Trim(DataSet.FieldByName('Descripcion').AsString) = '' then DataSet.Delete;
        btnAceptar.SetFocus;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name:
  Author:    flamas
  Date Created: 08/02/2005
  Description: Manejo de Teclas en el Grid
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure TFormAperturaCierrePuntoVenta.dbgAperturaCierreKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
    if (ssCtrl in Shift) and (key = VK_DELETE) then key := 0;
    if (key = VK_INSERT) then key := 0;
end;

{-----------------------------------------------------------------------------
  Function Name:
  Author:    flamas
  Date Created: 08/02/2005
  Description: Manejo de Teclas en el Grid
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure TFormAperturaCierrePuntoVenta.dbgAperturaCierreKeyPress(Sender: TObject; var Key: Char);
var
    xDbGrid: TNewStringGrid;                //Revision 6
    xInplaceEditor: TNewInplaceEditor;      //Revision 6
begin
    if ORD(key) = 22 then begin
        Key := #0;
        Exit;
    end; //Evita el comando ctrl+v para pegar texto en los campos
    if not (Key  in ['0'..'9', #8]) then Key := #0;
    //Revision 6
    xdbgrid := TNewStringGrid(DbgAperturaCierre);
    xInplaceEditor := TNewInplaceEditor(xdbgrid.InplaceEditor);
    xInplaceEditor.SetMaxLength(7);
    //Fin de Revision 6
end;

procedure TFormAperturaCierrePuntoVenta.edChequesKeyPress(Sender: TObject;
  var Key: Char);
begin
    if ORD(key) = 22 then begin
        Key := #0;
        Exit;
    end; //Evita el comando ctrl+v para pegar texto en los campos
	if  not (Key in [#8, #9, #13, #27, '0'..'9']) then Key := #0;
end;

{-----------------------------------------------------------------------------
  Function Name: GrillaBilletesSelectCell
  Author:    flamas
  Date Created: 08/02/2005
  Description: Formatea las Cantidades de Grid
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure TFormAperturaCierrePuntoVenta.GrillaBilletesSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
begin
	CanSelect := ( ACol = 2 );
	// Hace un trim de las cantidades para que queden alineadas
	GrillaBilletes.Cells[FLastCol, FLastRow] := trim ( GrillaBilletes.Cells[FLastCol, FLastRow] );
    FLastCol := ACol;
    FLastRow := ARow;
end;

{-----------------------------------------------------------------------------
  Function Name: GrillaBilletesKeyPress
  Author:    flamas
  Date Created: 08/02/2005
  Description: Manejo de Teclas en el Grid
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
//Revision 6
procedure TFormAperturaCierrePuntoVenta.GrillaBilletesKeyPress(Sender: TObject; var Key: Char);
var
    xDbGrid: TNewStringGrid;
    xInplaceEditor: TNewInplaceEditor;
begin
    if ORD(key) = 22 then begin
        Key := #0;
        Exit;
    end; //Evita el comando ctrl+v para pegar texto en los campos
	if  not (Key in [#8, #9, #13, #27, '0'..'9']) then Key := #0;
    xdbgrid := TNewStringGrid(GrillaBilletes);
    xInplaceEditor := TNewInplaceEditor(xdbgrid.InplaceEditor);
    xInplaceEditor.SetMaxLength(7);
end;

function TNewStringGrid.GetEditor: TInplaceEdit;
begin
    Result := InPlaceEditor;
end;
//Fin de Revision 6

procedure TFormAperturaCierrePuntoVenta.pcTurnosChange(Sender: TObject);
begin
	if pcTurnos.ActivePage = tsTelevias then ActiveControl := txtValeTelevias
    else if pcTurnos.ActivePage = tsEfectivo then ActiveControl := txtValeEfectivo
    else if pcTurnos.ActivePage = tsLiquidacionEfectivo then ActiveControl := GrillaBilletes;
end;



procedure TFormAperturaCierrePuntoVenta.txt_MontoKeyPress(Sender: TObject;
  var Key: Char);
begin
    if ORD(key) = 22 then begin
        Key := #0;
        Exit;
    end; //Evita el comando ctrl+v para pegar texto en los campos
end;

procedure TFormAperturaCierrePuntoVenta.lblAcaClick(Sender: TObject);
begin
    pcTurnos.ActivePageIndex := 0;
    if tsTelevias.Enabled and tsTelevias.Visible then ActiveControl := txtValeTelevias;
end;

end.
