object FormRptInformeUsuarioConfig: TFormRptInformeUsuarioConfig
  Left = 296
  Top = 326
  BorderStyle = bsDialog
  Caption = 'Configuraci'#243'n del Informe'
  ClientHeight = 71
  ClientWidth = 403
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  DesignSize = (
    403
    71)
  PixelsPerInch = 96
  TextHeight = 13
  object bvlParametros: TBevel
    Left = 6
    Top = 8
    Width = 389
    Height = 25
  end
  object btnCancelar: TButton
    Left = 312
    Top = 40
    Width = 79
    Height = 26
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 0
  end
  object btnAceptar: TButton
    Left = 229
    Top = 40
    Width = 79
    Height = 26
    Anchors = [akRight, akBottom]
    Caption = '&Aceptar'
    Default = True
    ModalResult = 1
    TabOrder = 1
    OnClick = btnAceptarClick
  end
  object spVariables: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerVariablesConsulta'
    Parameters = <>
    Left = 32
    Top = 16
  end
end
