unit DPSDLLRutinas;

interface


const
	DPSDLL = 'Dpsdll.dll';


//********************************************************************//
//                              RUTINAS                               //
//********************************************************************//
Function DPSInit(
	Servidor: Pchar;
	BaseDeDatos: Pchar;
    Usuario: Pchar;
    Password: Pchar;
	Msg: Pchar): integer; stdcall; external DPSDLL;

Function DPSExit(Msg: Pchar): integer; stdcall; external DPSDLL;

Function DPSObtenerPuesto(CodigoSistema: Pchar; var NumeroPuesto: integer; Msg: Pchar): integer; stdcall; external DPSDLL;

Function DPSTarjetaPago
 (NumeroPuesto: integer;
  TipoDebito: Integer;
  CodigoTarjeta: Integer;
  NumeroTarjeta: PChar;
  PIN, Vencimiento: PChar;
  Cuotas: integer;
  Importe: integer;
  CodigoComercio: PChar;
  NumeroTerminal: PChar;
  var Cupon : integer;
  Autorizacion: Pchar;
  var FechaOriginal: TDateTime;  
  Titular: Pchar; Domicilio: PChar; Telefono: PChar;
  Msg: Pchar): Integer; stdcall; external DPSDLL;

Function DPSTarjetaDevolucion
 (NumeroPuesto: integer;
  TipoDebito: Integer;
  CodigoTarjeta: integer;
  NumeroTarjeta: PChar;
  PIN, Vencimiento: PChar;
  Cuotas: integer;
  Importe: integer;
  CodigoComercio: PChar;
  NumeroTerminal: PChar;
  Cupon : integer;
  Autorizacion: Pchar;
  FechaOriginal: TDateTime;
  Titular: PChar; Domicilio: PChar; Telefono: PChar;
  Msg: PChar): Integer; stdcall; external DPSDLL;

implementation
end.
