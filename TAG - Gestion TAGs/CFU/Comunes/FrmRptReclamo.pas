{-------------------------------------------------------------------------------
 File Name: FrmRptReclamo.pas
 Author:    Lgisuk
 Date Created: 20/02/2006
 Language: ES-AR
 Description: Gestion de Reclamos - Reporte con la información de un reclamo

 Revision		:1
 Author			: Nelson Droguett Sierra
 Date			: 30-Junio-2010
 Description	: (Ref. Fase 2) Se agrega concesionaria y sub tipo orden de servicio


Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.

Etiqueta    : 20160610 MGO
Descripción : Se actualiza SpObtenerReporteReclamo

-------------------------------------------------------------------------------}
Unit FrmRptReclamo;

interface

uses
  //reporte
  DMConnection,              //Coneccion a base de datos OP_CAC
  Util,                      //Stringtofile,padl..
  Utildb,                    //querygetvalue
  UtilProc,                  //Mensajes
  //general
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ppDB, ppDBPipe, ADODB, UtilRB, ppParameter, ppBands,
  ppCtrls, ppPrnabl, ppClass, ppCache, ppComm, ppRelatv, ppProd, ppReport,
  RBSetup, ppStrtch, ppMemo, Printers, ConstParametrosGenerales;                //SS_1147_NDR_20140710

type
  TFRptReclamo = class(TForm)
    rptReclamo: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppImage2: TppImage;
    ppLtitulo: TppLabel;
    ppLine1: TppLine;
    ppDetailBand1: TppDetailBand;
    ppSummaryBand1: TppSummaryBand;
    pptImporteTotal: TppLabel;
    ppParameterList1: TppParameterList;
    ppDBPipeline: TppDBPipeline;
    DataSource: TDataSource;
    SpObtenerReporteReclamo: TADOStoredProc;
    ppLine4: TppLine;
    ppLine5: TppLine;
    Ltusuario: TppLabel;
    LtFechaHora: TppLabel;
    ppusuario: TppLabel;
    ppfechahora: TppLabel;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppLabel7: TppLabel;
    ppLabel8: TppLabel;
    ppLabel9: TppLabel;
    ppLabel10: TppLabel;
    ppTipoContacto: TppLabel;
    ppContacto: TppLabel;
    ppLabel13: TppLabel;
    ppLabel14: TppLabel;
    ppLabel15: TppLabel;
    ppFechaCreacion: TppLabel;
    ppNumeroOrden: TppLabel;
    ppRut: TppLabel;
    ppNombreyApellido: TppLabel;
    ppNumeroConvenio: TppLabel;
    ppPrioridad: TppLabel;
    ppFecha: TppLabel;
    ppLine2: TppLine;
    ppLine3: TppLine;
    ppLabel23: TppLabel;
    RBI_Listado: TRBInterface;
    ppLabel1: TppLabel;
    ppConcesionaria: TppLabel;
    ppLabel12: TppLabel;
    ppSubTipoOrdenServicio: TppLabel;
    ppLabel6: TppLabel;
    ppMemo: TppMemo;
    procedure RBI_ListadoExecute(Sender: TObject; var Cancelled: Boolean);
  private
    FCodigoOrdenServicio : Integer;
    FNombreReporte : string;
    { Private declarations }
  public
    function Inicializar(NombreReporte: AnsiString; CodigoOrdenServicio: Integer): Boolean;
    { Public declarations }
  end;

var
  FRptReclamo: TFRptReclamo;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    lgisuk
  Date Created: 20/02/2006
  Description: Inicializo este formulario
  Parameters: NombreReporte: AnsiString; CodigoOrdenServicio: Integer
  Return Value: Boolean

  Revision 1:
      Author : ggomez
      Date : 23/02/2006
      Description : Agregué el parámetro CodigoOrdenServicio.

-----------------------------------------------------------------------------}
function TFRptReclamo.Inicializar(NombreReporte: AnsiString; CodigoOrdenServicio: Integer): Boolean;
var                                                                             //SS_1147_NDR_20140710
  RutaLogo : AnsiString;                                                        //SS_1147_NDR_20140710
begin
    ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
    try                                                                                                 //SS_1147_NDR_20140710
      ppImage2.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
    except                                                                                              //SS_1147_NDR_20140710
      On E: Exception do begin                                                                          //SS_1147_NDR_20140710
        Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
        MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
        Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
      end;                                                                                              //SS_1147_NDR_20140710
    end;                                                                                                //SS_1147_NDR_20140710

    FCodigoOrdenServicio    := CodigoOrdenServicio;
    FNombreReporte          := NombreReporte;
    Result                  := RBI_Listado.Execute;
end;

{-----------------------------------------------------------------------------
  Function Name: RBIListadoExecute
  Author:    lgisuk
  Date Created: 20/02/2006
  Description: Ejecuto el informe
  Parameters: Sender: TObject;var Cancelled: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFRptReclamo.RBI_ListadoExecute(Sender: TObject;var Cancelled: Boolean);
Resourcestring
    MSG_ERROR_TO_GENERATE_REPORT = 'Error al generar el reporte';
    MSG_ERROR_EMPTY_QUERY = 'No se encontraron datos para la consulta solicitada';
    MSG_ERROR = 'Error';
var
    Config : TrbConfig;
begin

    try
        //Obtengo los datos del operador que imprime el reclamo
        ppUsuario.Caption:= QueryGetValue(DMConnections.BaseCAC,'SELECT dbo.ObtenerNombreUsuario( ''' + UsuarioSistema + ''' ) ');//usuariosistema;
        ppFechaHora.Caption:= DateTimeToStr(Now);
        //Obtengo los datos del reclamo
        with SpObtenerReporteReclamo, Parameters do begin
            Close;
            refresh;
            ParamByName('@CodigoOrdenServicio').value := FCodigoOrdenServicio;
            ParamByName('@ErrorDescription').Value := Null;                     // 20160610 MGO
            CommandTimeout := 500;
            Open;
            // INICIO : 20160610 MGO
            if ParamByName('@RETURN_VALUE').Value <> 0 then begin
                MsgBoxErr(MSG_ERROR_TO_GENERATE_REPORT, ParamByName('@ErrorDescription').Value, MSG_ERROR, MB_ICONERROR);
                Close;
                Cancelled := True;
                Exit;
            end;
            // FIN : 20160610 MGO

            //verifico si no esta vacio
            if IsEmpty then begin
                Close;
                MsgBox(MSG_ERROR_EMPTY_QUERY);
                Cancelled := True;
                Exit;
            end;
            //Titulo
            ppLtitulo.Caption :=  FieldByName('Titulo').AsString;
            //Datos Generales
            ppFechaCreacion.Caption := FieldByName('FechaCreacion').AsString;
            ppNumeroOrden.Caption := FieldByName('NumeroOrden').AsString;

            //Rev.1 / 30-Junio-2010 / Nelson Droguett Sierra--------------------------------------------------------
            ppConcesionaria.Caption := FieldByName('Nombrecorto').AsString;
            ppSubTipoOrdenServicio.Caption := FieldByName('DescripcionSubTipo').AsString;
            //FinRev.1----------------------------------------------------------------------------------------------

            //Identificación del usuario
            ppRut.Caption := FieldByName('Rut').AsString;
            ppNombreyApellido.Caption := FieldByName('NombreyApellido').AsString;
            ppNumeroConvenio.Caption := FieldByName('NumeroConvenio').AsString;
            //Compromiso con el cliente
            ppPrioridad.Caption := FieldByName('Prioridad').AsString;
            ppfecha.Caption := FieldByName('Fecha').AsString;
            //Información de Contacto
            ppTipoContacto.Caption :=  FieldByName('TipoContacto').AsString;
            ppContacto.Caption := FieldByName('InfoContacto').AsString;
            //Detalle del Reclamo
            ppMemo.lines.add(FieldByName('DetalleDelReclamo').AsString);
            //Cierro la consulta
            Close;
        end;

        //Configuro el Report Builder y Ejecuto el Reporte
        rbi_Listado.Caption := FnombreReporte;
        Config := rbi_Listado.GetConfig;
        //Establezco la cantidad de copias
        Config.Copies := 2;
        //Mostrar Vista Previa
        Config.DeviceType := 'Screen';
        //Seteo la configuración
        rbi_Listado.SetConfig(Config);

    except
        on E : Exception do begin
            MsgBoxErr(MSG_ERROR_TO_GENERATE_REPORT, E.message, MSG_ERROR, MB_ICONERROR);
        end;
    end;
end;


end.
