object FrameRespuestaOrdenServicio: TFrameRespuestaOrdenServicio
  Left = 0
  Top = 0
  Width = 393
  Height = 216
  TabOrder = 0
  object LRespuestadelaConcesionaria: TLabel
    Left = 0
    Top = 60
    Width = 393
    Height = 13
    Align = alTop
    Caption = '  Respuesta de la Concesionaria'
  end
  object LComentariosDelCliente: TLabel
    Left = 0
    Top = 161
    Width = 393
    Height = 13
    Align = alTop
    Caption = '  Comentarios del Cliente'
  end
  object PContactarCliente: TPanel
    Left = 0
    Top = 0
    Width = 393
    Height = 60
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object cknotificado: TCheckBox
      Left = 7
      Top = 8
      Width = 177
      Height = 17
      Caption = 'Qued'#243' Notificado'
      TabOrder = 0
    end
    object ckconforme: TCheckBox
      Left = 7
      Top = 32
      Width = 233
      Height = 17
      Caption = 'Estuvo de acuerdo con la Resoluci'#243'n'
      TabOrder = 1
    end
  end
  object txtDetalleRespuesta: TMemo
    Left = 0
    Top = 73
    Width = 393
    Height = 88
    Align = alTop
    TabOrder = 1
  end
  object TxtComentariosdelCliente: TMemo
    Left = 0
    Top = 174
    Width = 393
    Height = 42
    Align = alClient
    TabOrder = 2
  end
end
