object dmMensajes: TdmMensajes
  OldCreateOrder = False
  Height = 240
  Width = 529
  object spObtenerMensaje: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EMAIL_ObtenerMensaje;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoMensaje'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 80
    Top = 8
  end
  object spObtenerCorrespondencia: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EMAIL_Correspondencia_Obtener;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoMensaje'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ErrorDescription'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdOutput
        Size = 4000
        Value = Null
      end>
    Left = 80
    Top = 8
  end
  object spMensajesSinEnviar: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EMAIL_ObtenerMensajesNoEnviados;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 192
    Top = 8
  end
  object spAgregarReintento: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EMAIL_AgregarReintento;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoMensaje'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 440
    Top = 8
  end
  object spMarcarComoEnviado: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EMAIL_MarcarComoEnviado;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoMensaje'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 312
    Top = 8
  end
  object spAgregarMensaje: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EMAIL_AgregarMensaje;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Email'
        Attributes = [paNullable]
        DataType = ftString
        Size = 200
        Value = Null
      end
      item
        Name = '@CodigoPlantilla'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoFirma'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Parametros'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4096
        Value = Null
      end>
    Left = 32
    Top = 72
  end
  object spObtenerDatosPersona: TADOStoredProc
    AutoCalcFields = False
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerDatosPersona'
    Parameters = <>
    Left = 256
    Top = 32
  end
  object spObtenerDomicilioConvenio: TADOStoredProc
    AutoCalcFields = False
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerDomicilioConvenio'
    Parameters = <>
    Left = 288
    Top = 32
  end
  object spObtenerParametroGeneral: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerParametroGeneral;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Nombre'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@Valor'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 255
        Value = Null
      end
      item
        Name = '@TipoParametro'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 1
        Value = Null
      end>
    Left = 176
    Top = 72
  end
end
