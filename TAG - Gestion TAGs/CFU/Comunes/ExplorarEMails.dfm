object frmExplorarMails: TfrmExplorarMails
  Left = 105
  Top = 137
  Caption = 'Explorar e-Mails'
  ClientHeight = 488
  ClientWidth = 845
  Color = clBtnFace
  Constraints.MinHeight = 522
  Constraints.MinWidth = 845
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 845
    Height = 82
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object gbBusqueda: TGroupBox
      Left = 0
      Top = 0
      Width = 804
      Height = 82
      Align = alLeft
      Caption = 'Par'#225'metros de b'#250'squeda de e-Mails'
      TabOrder = 0
      DesignSize = (
        804
        82)
      object Label1: TLabel
        Left = 224
        Top = 33
        Width = 96
        Height = 13
        Caption = 'Mostrar los primeros '
      end
      object Label2: TLabel
        Left = 374
        Top = 33
        Width = 79
        Height = 13
        Caption = 'creados entre el '
      end
      object Label3: TLabel
        Left = 555
        Top = 33
        Width = 16
        Height = 13
        Caption = 'y el'
      end
      object neTop: TNumericEdit
        Left = 328
        Top = 31
        Width = 40
        Height = 21
        MaxLength = 4
        TabOrder = 0
      end
      object deDesde: TDateEdit
        Left = 459
        Top = 30
        Width = 90
        Height = 21
        AutoSelect = False
        TabOrder = 1
        Date = -693594.000000000000000000
      end
      object deHasta: TDateEdit
        Left = 584
        Top = 30
        Width = 90
        Height = 21
        AutoSelect = False
        TabOrder = 2
        Date = -693594.000000000000000000
      end
      object RgTipos: TRadioGroup
        Left = 2
        Top = 15
        Width = 210
        Height = 65
        Align = alLeft
        Caption = 'Tipos de Email'
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'Todos'
          'Notas de Cobro'
          'Facturaci'#243'n'
          'Env'#237'o de Clave'
          'Correspondencia')
        TabOrder = 3
      end
      object btnBuscar: TButton
        Left = 687
        Top = 28
        Width = 75
        Height = 25
        Anchors = [akRight, akBottom]
        Caption = '&Buscar'
        Default = True
        TabOrder = 4
        OnClick = btnBuscarClick
        ExplicitLeft = 728
      end
    end
  end
  object cpOpciones: TCollapsablePanel
    Left = 0
    Top = 82
    Width = 845
    Height = 145
    Align = alTop
    Animated = False
    Caption = 'M'#225's Opciones'
    BarColorStart = 14071199
    BarColorEnd = 10646097
    RightMargin = 0
    LeftMargin = 0
    ArrowLocation = cpaRightTop
    Style = cpsWinXP
    InternalSize = 122
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWhite
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    DesignSize = (
      845
      145)
    object Label4: TLabel
      Left = 547
      Top = 89
      Width = 256
      Height = 13
      Hint = 
        'Un archivo de mailing es un archivo de texto creado de formato e' +
        'special normalmente creado por un dba que incluye una lista de e' +
        '-mails, convenios y par'#225'metros especiales'
      Anchors = [akRight, akBottom]
      Caption = 'Si Desea Importar un Archivo de Mailing Haga "click" '
      ParentShowHint = False
      ShowHint = True
      ExplicitLeft = 539
      ExplicitTop = -33
    end
    object lblImportar: TLabel
      Left = 814
      Top = 88
      Width = 18
      Height = 13
      Cursor = crHandPoint
      Anchors = [akRight, akBottom]
      Caption = 'ac'#225
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      OnClick = lblImportarClick
      ExplicitLeft = 806
      ExplicitTop = -34
    end
    object Label6: TLabel
      Left = 359
      Top = 108
      Width = 442
      Height = 13
      Anchors = [akRight, akBottom]
      Caption = 
        'Si Desea Ver o Modificar los Par'#225'metros Generales Relativos al E' +
        'nv'#237'o de e-mails haga "click"'
      ExplicitLeft = 351
      ExplicitTop = -14
    end
    object lblParametros: TLabel
      Left = 814
      Top = 107
      Width = 18
      Height = 13
      Cursor = crHandPoint
      Anchors = [akRight, akBottom]
      Caption = 'ac'#225
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      OnClick = lblParametrosClick
      ExplicitLeft = 806
      ExplicitTop = -15
    end
    object Label8: TLabel
      Left = 253
      Top = 127
      Width = 551
      Height = 13
      Anchors = [akRight, akBottom]
      Caption = 
        'Si Desea que se Intente Enviar Nuevamente Todos los e-mails que ' +
        'Alcanzaron el L'#237'mite de Reintentos Haga "click" '
      ExplicitLeft = 245
      ExplicitTop = 5
    end
    object lblUpdate: TLabel
      Left = 814
      Top = 127
      Width = 18
      Height = 13
      Cursor = crHandPoint
      Anchors = [akRight, akBottom]
      Caption = 'ac'#225
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      OnClick = lblUpdateClick
      ExplicitLeft = 806
      ExplicitTop = 5
    end
    object rgOtros: TRadioGroup
      Left = 2
      Top = 25
      Width = 215
      Height = 118
      Caption = 'Filtros Adicionales'
      ItemIndex = 0
      Items.Strings = (
        'Ninguno'
        'S'#243'lo los que se enviaron'
        'S'#243'lo los que no se enviaron'
        'S'#243'lo los que excedieron los reintentos')
      TabOrder = 0
    end
  end
  object pnlFooter: TPanel
    Left = 0
    Top = 447
    Width = 845
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    DesignSize = (
      845
      41)
    object btnCerrar: TButton
      Left = 760
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = '&Cerrar'
      TabOrder = 0
      OnClick = btnCerrarClick
    end
  end
  object DBListEx2: TDBListEx
    Left = 0
    Top = 227
    Width = 845
    Height = 220
    Align = alClient
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 150
        Header.Caption = 'C'#243'digo'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'CodigoMensaje'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 150
        Header.Caption = 'Fecha Creaci'#243'n'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'FechaCreacion'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Proceso Facturaci'#243'n'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'NumeroProcesoFacturacion'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 150
        Header.Caption = 'E-Mail Destino'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'email'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 150
        Header.Caption = 'Asunto'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Asunto'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 150
        Header.Caption = 'Plantilla Original'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Plantilla'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 150
        Header.Caption = 'N'#250'mero Convenio'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'NumeroConvenio'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 150
        Header.Caption = 'Par'#225'metros Originales'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Parametros'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 150
        Header.Caption = 'Reintentos'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Reintentos'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 150
        Header.Caption = 'Enviado'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Enviado'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 150
        Header.Caption = 'Comprobante'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'NumeroComprobante'
      end>
    DataSource = dsEMails
    DragReorder = True
    ParentColor = False
    PopupMenu = mnuComponer
    TabOrder = 2
    TabStop = True
    OnDblClick = mnuComponerEsteMailClick
    ExplicitTop = 228
  end
  object dsEMails: TDataSource
    DataSet = spExplorarEMails
    Left = 8
    Top = 456
  end
  object spExplorarEMails: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ExplorarEMails'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@TopFilter'
        Attributes = [paNullable]
        DataType = ftString
        Size = 6
        Value = Null
      end
      item
        Name = '@FechaDesde'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@FechaHasta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftString
        Size = 13
        Value = Null
      end>
    Left = 48
    Top = 456
  end
  object dlgAbrir: TOpenDialog
    DefaultExt = '*.txt'
    Filter = 'Archivos de texto|*.txt|Todos los archivos|*.*'
    Options = [ofReadOnly, ofNoChangeDir, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 80
    Top = 456
  end
  object mnuComponer: TPopupMenu
    OnPopup = mnuComponerPopup
    Left = 768
    Top = 232
    object mnuComponerEsteMail: TMenuItem
      Caption = 'Componer este e-Mail'
      OnClick = mnuComponerEsteMailClick
    end
  end
end
