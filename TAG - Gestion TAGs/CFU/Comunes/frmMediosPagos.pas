unit frmMediosPagos;
{-----------------------------------------------------------------------------
 Revision  2:
 Date         : 25/02/2009
 Author       : mpiazza
 Description  : Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,PeaTypes,RStrings, validate, UtilDB, Util, UtilProc, PeaProcs,
  StdCtrls, ExtCtrls, DPSControls,DmConnection, DmiCtrls, Mask,
  FreTelefono, Buttons;

type
  TFormMediosPagos = class(TForm)
    Notebook: TNotebook;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    lblNumeroTarjetaCredito: TLabel;
	cb_TipoTarjeta: TComboBox;
    gb_DatosPersonales: TGroupBox;
    txtDocumento: TEdit;
    lblRutRun: TLabel;
    txt_Nombre: TEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    cb_TipoCuenta: TComboBox;
    Label7: TLabel;
    Label8: TLabel;
    cb_Banco: TComboBox;
    Label9: TLabel;
    txt_NumeroCuenta: TEdit;
    txt_Sucursal: TEdit;
    Label10: TLabel;
    Panel1: TPanel;
    pnlBotton: TPanel;
	txt_NroTarjetaCredito: TEdit;
    lblFechaVencimiento: TLabel;
    txt_FechaVencimiento: TMaskEdit;
    Me_Mensaje: TLabel;
    FrameTelefono: TFrameTelefono;
    btnSolicitudContacto: TSpeedButton;
    cb_EmisoresTarjetasCreditos: TComboBox;
    Label2: TLabel;
    btnAceptar: TButton;
    btnSalir: TButton;
    procedure cb_TipoCuentaClick(Sender: TObject);
    procedure cb_TipoTarjetaClick(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
    procedure cb_BancoClick(Sender: TObject);
    procedure cbTomarDatosClick(Sender: TObject);
    procedure btnSolicitudContactoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cb_EmisoresTarjetasCreditosClick(Sender: TObject);
    function DarCodigoEmisorTarjetaCredito: Integer;
    procedure txt_NumeroCuentaKeyPress(Sender: TObject; var Key: Char);
  private
	{ Private declarations }
    FRegistro:TDatosMediosPago;
    FDocumentoSolicitud: string;
    FNombreSolicitud: string;
    FCodigoAreaSolicitud: integer;
    FTelefonoSolicitud: string;
    function DarTipoCuenta:Integer;
    function DarCodigoTarjetaCredito:Integer;
    function DarBanco:Integer;

  public
    { Public declarations }
    property Registro:TDatosMediosPago read FRegistro;
    function inicializar(Registro:TDatosMediosPago;
                         Modo: TStateConvenio = scAlta;
                         DocumentoSolicitud: string = '';
                         NombreSolicitud: string = '';
                         CodigoAreaSolicitud: integer = 0;
                         TelefonoSolicitud: string = '';
						 SoloLectura: boolean = False): Boolean;
    Class procedure LimpiarRegistro(var Registro:TDatosMediosPago);
    Class function ValidarRegistro(var Registro:TDatosMediosPago; var mensaje: AnsiString; RegistroPersonaConvenio: TDatosPersonales): boolean;
    Class function ArmarMedioPago(Registro:TDatosMediosPago; var MedioPago: AnsiString): boolean;
  end;

implementation

{$R *.dfm}

const
    CONST_VENCIMIENTO_MAXIMO = 10;

{-----------------------------------------------------------------------------
  Function Name: inicializar
  Author:
  Date Created:  /  /
  Description:
  Parameters: Registro:TDatosMediosPago;
  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function TFormMediosPagos.inicializar(Registro:TDatosMediosPago;
									  Modo: TStateConvenio = scAlta;
									  DocumentoSolicitud: string = '';
									  NombreSolicitud: string = '';
									  CodigoAreaSolicitud: integer = 0;
									  TelefonoSolicitud: string = '';
									  SoloLectura: boolean = False): Boolean;
var
	alta:Boolean;
begin
	try
		Result := False;

		GestionarComponentesyHints(self,DMConnections.BaseCAC,[cb_TipoTarjeta,cb_EmisoresTarjetasCreditos,cb_TipoCuenta,cb_Banco,txtDocumento,txt_Nombre,txt_NumeroCuenta,txt_Sucursal,txt_NroTarjetaCredito,txt_FechaVencimiento]);

//        cbTomarDatos.Checked := (Registro.RUT <> '') and (Trim(Registro.RUT) = Trim(DocumentoSolicitud));

		FrameTelefono.Inicializa(STR_TELEFONO, False, False, False);

		//cargo el mensaje
		Me_Mensaje.Caption:=QueryGetValue(DMConnections.BaseCAC,'SELECT Descripcion FROM Mensajes  WITH (NOLOCK) where codigomensaje = 143');

		alta := (Registro.PAC.TipoCuenta=-1) and (Registro.PAT.TipoTarjeta=-1);


		if not(alta) then begin
			txtDocumento.Text := Trim(Registro.RUT);
			txt_Nombre.Text := Trim(Registro.Nombre);
            FrameTelefono.CargarTelefono(Registro.CodigoArea, Registro.Telefono, 0);
        end;
        FRegistro:= Registro;
        FDocumentoSolicitud:= DocumentoSolicitud;
        FNombreSolicitud:= NombreSolicitud;
        FCodigoAreaSolicitud:= CodigoAreaSolicitud;
        FTelefonoSolicitud:= TelefonoSolicitud;

        txt_FechaVencimiento.Text := '00' + dateseparator + '00';

		case FRegistro.TipoMedioPago of
			PAT: begin
					Notebook.PageIndex:=0;
					Caption:=Caption+CAPTION_TITULAR_TARJETA_CREDITO;
					if not(Alta) then begin
						with Registro.PAT do begin
							CargarTiposTarjetasCredito(DMConnections.BaseCAC,cb_TipoTarjeta,TipoTarjeta, true);
							CargarEmisoresTarjetasCredito(DMConnections.BaseCAC,cb_EmisoresTarjetasCreditos,EmisorTarjetaCredito, true);
							txt_NroTarjetaCredito.Text:=Trim(NroTarjeta);
							if Length(trim(FechaVencimiento)) = 5 then FechaVencimiento[3] := dateseparator;
							txt_FechaVencimiento.Text:= iif(Length(FechaVencimiento)< 5, '00' + dateseparator + '00',FechaVencimiento); // Siempre se guarda con una barra
						end;
					end else begin
						CargarTiposTarjetasCredito(DMConnections.BaseCAC,cb_TipoTarjeta,0,true);
						CargarEmisoresTarjetasCredito(DMConnections.BaseCAC,cb_EmisoresTarjetasCreditos,-1, true);
					end;
				end;
			PAC: begin
					Notebook.PageIndex:=1;
					Caption:=Caption+CAPTION_TITULAR_CUENTA_BANCARIA;
					if not(Alta) then begin
						with Registro.PAC do begin
							CargarTiposCuentas(DMConnections.BaseCAC,cb_TipoCuenta,TipoCuenta,true);
							cb_TipoCuenta.OnClick(cb_TipoCuenta);
							CargarBancosxCuentas(DMConnections.BaseCAC,cb_Banco,TipoCuenta,CodigoBanco,true);
							txt_NumeroCuenta.Text:=Trim(NroCuentaBancaria);
							txt_Sucursal.Text:=Trim(Sucursal);
						end;
					end else begin
						CargarTiposCuentas(DMConnections.BaseCAC,cb_TipoCuenta,-1,true);
						cb_TipoCuenta.OnClick(cb_TipoCuenta);
					end;
                end;
            else exit;
        end;

        //Si es consulta (modo Normal, no se habilita nada).
        EnableControlsInContainer(Notebook, Modo <> scNormal);
        EnableControlsInContainer(gb_DatosPersonales, Modo <> scNormal);
        btnAceptar.Enabled := Modo <> scNormal;

        //Habilitar el ingreso de la tarjeta
        lblNumeroTarjetaCredito.Enabled := true;
        txt_NroTarjetaCredito.Enabled := true;
		lblFechaVencimiento.Enabled := true;
        txt_FechaVencimiento.Enabled := true;

        if SoloLectura then begin
            EnableControlsInContainer(self, not SoloLectura);
            btnSalir.Enabled := SoloLectura;
            pnlBotton.Enabled := SoloLectura;
        end;

        Result:=True;
    except
        result:=False;
        exit;
    end;
end;

procedure TFormMediosPagos.cb_TipoCuentaClick(Sender: TObject);
begin
	CargarBancosxCuentas(DMConnections.BaseCAC,cb_Banco,DarTipoCuenta,-1,true);
	if self.Visible then cb_Banco.SetFocus;
end;

function TFormMediosPagos.DarTipoCuenta: Integer;
begin
    if (trim(cb_TipoCuenta.Text)='') or (trim(cb_TipoCuenta.Text)=SELECCIONAR) then result:=-1
    else result:=StrToInt(Trim(StrRight(cb_TipoCuenta.Text,30)));
end;

procedure TFormMediosPagos.cb_TipoTarjetaClick(Sender: TObject);
begin
    if self.Visible then begin
        if cb_EmisoresTarjetasCreditos.Enabled then
            cb_EmisoresTarjetasCreditos.SetFocus
        else
            txtDocumento.SetFocus;
        //txt_NroTarjetaCredito.SetFocus;//cbTomarDatos.setfocus;
    end;
end;

procedure TFormMediosPagos.btnAceptarClick(Sender: TObject);
var
    ErrorNumeroTarjetaCredito:String;
    ErrorNumero:Boolean;
    anioTarjeta: INteger;
begin

    if Registro.TipoMedioPago = PAT then begin //Credito

        if not (ValidateControls([cb_TipoTarjeta,
								cb_EmisoresTarjetasCreditos,
                                txt_NroTarjetaCredito,
                                txt_FechaVencimiento],
                                [(cb_TipoTarjeta.itemindex > 0),
                                cb_EmisoresTarjetasCreditos.itemindex>0,
                                 ((not txt_NroTarjetaCredito.Enabled) or (trim(txt_NroTarjetaCredito.text) <> '')),
                                 ((not txt_FechaVencimiento.Enabled) or ((Length((trim(txt_FechaVencimiento.Text))) = 5) and (Pos(' ',Trim(txt_FechaVencimiento.Text)) = 0) and (trim(txt_FechaVencimiento.Text) <> dateSeparator)))],
                                 format(MSG_CAPTION_GESTION,[STR_MEDIOS_PAGOS]),
								 [Format(MSG_VALIDAR_DEBE_EL,[FLD_TIPO_TARJETA]),
                                 Format(MSG_VALIDAR_DEBE_EL,[FLD_TIPO_EMISOR]),
                                  Format(MSG_VALIDAR_DEBE_EL,[FLD_NRO_TARJETA]),
                                  MSG_ERROR_FECHA_VENCIMIENTO])) then exit;


        //Sacar esto si se habilita la tarjeta
//        ErrorNumero := true;
        if txt_NroTarjetaCredito.Enabled then begin
            ErrorNumero:=ValidarTarjetaCredito(DMConnections.BaseCAC,DarCodigoTarjetaCredito,txt_NroTarjetaCredito.Text,ErrorNumeroTarjetaCredito);
        anioTarjeta := strtoint(StrRight(txt_FechaVencimiento.Text,2));
        if AnioTarjeta > ((year(now) mod 100) + CONST_VENCIMIENTO_MAXIMO) then
            anioTarjeta := 1900 + AnioTarjeta
        else
            anioTarjeta := 2000 + AnioTarjeta;

        if not(ValidateControls([txt_NroTarjetaCredito,
                                txt_FechaVencimiento],
                                [ErrorNumero,
								(((not txt_FechaVencimiento.Enabled) or
                                 (strtoint(strleft(txt_FechaVencimiento.Text,2)) <= 12) and
                                  (StrToInt(intToStr(AnioTarjeta) + StrLeft(txt_FechaVencimiento.Text, 2)) >= strtoint(FormatDateTime('yyyymm', NowBase(DMConnections.BaseCAC))))))],
                                format(MSG_CAPTION_GESTION,[STR_MEDIOS_PAGOS]),
								[Format(MSG_ERROR_NUMERO_TARJETA,[ErrorNumeroTarjetaCredito]),
                                MSG_ERROR_FECHA_VENCIMIENTO]
        )) then exit;
        end;//hasta aca
    end else begin //Debito
        if not(ValidateControls([cb_TipoCuenta,
                                cb_Banco,
                                txt_NumeroCuenta,
                                txt_Sucursal],
                                [(cb_TipoCuenta.itemindex>0),
                                (cb_Banco.itemindex>0),
                                (Trim(txt_NumeroCuenta.Text)<>'') and ValidarNumeroCuentaBancaria(DMConnections.BaseCAC,DarTipoCuenta,txt_NumeroCuenta.Text),
                                trim(txt_Sucursal.Text)<>''],
                                format(MSG_CAPTION_GESTION,[STR_MEDIOS_PAGOS]),
                                [MSG_FALTA_CUENTA,
								Format(MSG_VALIDAR_DEBE_EL,[FLD_BANCO]),
                                MSG_ERROR_NUMERO_CUENTA,
                                Format(MSG_VALIDAR_DEBE_LA,[FLD_SUCURSAL])]
        )) then exit;
    end;



    // Valido datos de la persona
    if not(ValidateControls([txtDocumento,
                            txtDocumento,
                            txt_Nombre],
                            [trim(txtDocumento.Text) <> '',
                             ValidarRUT(DMConnections.BaseCAC,trim(txtDocumento.Text)),
                             trim(txt_Nombre.Text)<>''],
//                             (EsComponenteObligatorio(txt_Telefono) and (trim(txt_Telefono.Text)<>'')) or not(EsComponenteObligatorio(txt_Telefono))],
                             format(MSG_CAPTION_GESTION,[STR_MEDIOS_PAGOS]),
                             [Format(MSG_VALIDAR_DEBE_EL,[STR_RUT]),
                             MSG_ERROR_DOCUMENTO,
							 Format(MSG_VALIDAR_DEBE_EL,[FLD_NOMBRE]),
                             MSG_NUMERO_TELEFONO
                             ]
    )) then exit;


    if (FrameTelefono.NumeroTelefono <> '') and not FrameTelefono.TelefonoValido then Exit;

    // grabo el registro
    if Registro.TipoMedioPago= PAT then begin
        FRegistro.PAT.TipoTarjeta:=DarCodigoTarjetaCredito;
        FRegistro.PAT.NroTarjeta:=trim(txt_NroTarjetaCredito.Text);
        FRegistro.PAT.FechaVencimiento:=txt_FechaVencimiento.Text;
        FRegistro.PAT.FechaVencimiento[3]:= '/';
        FRegistro.PAT.EmisorTarjetaCredito:=DarCodigoEmisorTarjetaCredito;
        FRegistro.PAC.TipoCuenta:=-1;
        FRegistro.PAC.CodigoBanco:=-1;
        FRegistro.PAC.NroCuentaBancaria:='';
        FRegistro.PAC.Sucursal:='';
	end else begin
        FRegistro.PAT.TipoTarjeta:=-1;
        FRegistro.PAT.NroTarjeta:='';
        FRegistro.PAT.FechaVencimiento:='';
        FRegistro.PAT.EmisorTarjetaCredito:=-1;        
        FRegistro.PAC.TipoCuenta:=DarTipoCuenta;
        FRegistro.PAC.CodigoBanco:=DarBanco;
        FRegistro.PAC.NroCuentaBancaria:=trim(txt_NumeroCuenta.Text);
        FRegistro.PAC.Sucursal:=trim(txt_Sucursal.Text);
    end;

    FRegistro.RUT:=trim(txtDocumento.Text);
    FRegistro.Nombre:=Trim(txt_Nombre.Text);
    FRegistro.Telefono:=trim(FrameTelefono.NumeroTelefono);
    FRegistro.CodigoArea:= FrameTelefono.CodigoArea;

    ModalResult:=mrOk;
end;


function TFormMediosPagos.DarCodigoTarjetaCredito: Integer;
begin
    if trim(cb_TipoTarjeta.Text)='' then result:=-1
    else result:=StrToInt(Trim(StrRight(cb_TipoTarjeta.Text,30)));
end;


procedure TFormMediosPagos.cb_BancoClick(Sender: TObject);
begin
    if self.Visible then txt_NumeroCuenta.SetFocus;
end;

function TFormMediosPagos.DarBanco: Integer;
begin
    if trim(cb_Banco.Text)='' then result:=-1
    else result:=StrToInt(Trim(StrRight(cb_Banco.Text,30)));

end;

class procedure TFormMediosPagos.LimpiarRegistro(
  var Registro: TDatosMediosPago);
begin
    with Registro do begin
        TipoMedioPago:=Peatypes.PAC;
        PAC.TipoCuenta:=-1;
        PAC.CodigoBanco:=-1;
        PAC.NroCuentaBancaria:='';
        PAC.Sucursal:='';
        PAT.TipoTarjeta:=-1;
        PAT.NroTarjeta:='';
        PAT.FechaVencimiento:='';
        RUT:='';
        Nombre:='';
        Telefono:='';
    end;
end;

procedure TFormMediosPagos.txt_NumeroCuentaKeyPress(Sender: TObject;
  var Key: Char);
begin
    //INICIO JLO 20160927
    //if Key  in [''''] then Key := #0;
    if not (Key  in ['0'..'9',#8]) then
		Key := #0;
    //TERMINO JLO 20160927
end;

class function TFormMediosPagos.ValidarRegistro(var Registro: TDatosMediosPago;
  var mensaje: AnsiString; RegistroPersonaConvenio:TDatosPersonales): boolean;
var
    ErrorNumeroTarjetaCredito: AnsiString;
begin
    ErrorNumeroTarjetaCredito := '';
    mensaje := '';
    //Valida que los datos de un registro sean validos, y si
    //son incorrectos, devolver falso con el Mensaje en la variable recibida

    if Registro.TipoMedioPago = Peatypes.PAT then begin //Credito
        with Registro.PAT do
        begin
            if (TipoTarjeta < 0) then
                mensaje := Format(MSG_VALIDAR_DEBE_EL,[FLD_TIPO_TARJETA])
            else
                (*ver cuando se deshabilite la tarjeta*)
                if (trim(NroTarjeta) = '') or
                    not (ValidarTarjetaCredito(DMConnections.BaseCAC,TipoTarjeta ,
						NroTarjeta, ErrorNumeroTarjetaCredito)) then
                    mensaje := iif(ErrorNumeroTarjetaCredito = '', MSG_ERROR_FALTA_NUMERO_TARJETA, ErrorNumeroTarjetaCredito)
                else
                    if (trim(FechaVencimiento) = '/') or
                       (strtoint(strleft(FechaVencimiento,2))>12) or
                       (strtoint(StrRight(FechaVencimiento,2)+
                       StrLeft(FechaVencimiento,2))<= strtoint(FormatDateTime('yymm',NowBase(DMConnections.BaseCAC)))) then
                        mensaje := MSG_ERROR_FECHA_VENCIMIENTO;
        end;
    end
    else begin
        if Registro.TipoMedioPago = Peatypes.PAC then begin //Credito
            with Registro.PAC do
            begin
                if (TipoCuenta < 0) then
                    mensaje := MSG_FALTA_CUENTA
                else
                    if (CodigoBanco < 0) then
                        mensaje := Format(MSG_VALIDAR_DEBE_EL,[FLD_BANCO])
					else
                        if (trim(NroCuentaBancaria) = '') then
                            mensaje := MSG_ERROR_NUMERO_CUENTA;
            end;
        end
        else mensaje := MSG_ERROR_TIPO_MEDIO_PAGO_AUTOMATICO;
    end;
    if trim(RegistroPersonaConvenio.NumeroDocumento) = (Registro.RUT) then
        Registro.Nombre := iif(RegistroPersonaConvenio.Personeria = PERSONERIA_JURIDICA, Trim(RegistroPersonaConvenio.RazonSocial), Trim(RegistroPersonaConvenio.Apellido) + Trim(RegistroPersonaConvenio.ApellidoMaterno) + Trim(RegistroPersonaConvenio.Nombre));
    if (mensaje = '') then begin
        //si llego hasta aca significa que los datos particulares estan Ok
        //ahora valido los datos que son compartidos. Nombre, RUT
        if trim(Registro.Nombre) = '' then
            mensaje := Format(MSG_VALIDAR_DEBE_EL,[FLD_NOMBRE])
        else
            if trim(Registro.RUT) = '' then mensaje := Format(MSG_VALIDAR_DEBE_EL,[STR_RUT]);
    end;
    result := mensaje = '';
end;

procedure TFormMediosPagos.cbTomarDatosClick(Sender: TObject);
begin
(*
    if cbTomarDatos.Checked then begin
        txtDocumento.Text  := FDocumentoSolicitud;
        txt_Nombre.Text := FNombreSolicitud;
        FrameTelefono.CargarTelefono(FCodigoAreaSolicitud, FTelefonoSolicitud, 0);
    end
    else begin
        txtDocumento.Text := FRegistro.RUT;
        txt_Nombre.Text := FRegistro.Nombre;
        FrameTelefono.CargarTelefono(iif(Registro.CodigoArea=0,2,Registro.CodigoArea), Registro.Telefono, 0);
    end;
*)
end;


{-----------------------------------------------------------------------------
  Function Name: ArmarMedioPago
  Author:
  Date Created:  /  /
  Description:
  Parameters: Registro: TDatosMediosPago;
  var MedioPago: AnsiString
  Return Value: boolean

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
class function TFormMediosPagos.ArmarMedioPago(Registro: TDatosMediosPago;
  var MedioPago: AnsiString): boolean;

var
    DescripcionTarjeta, DescripcionCuenta, DescripcionBanco: string;
begin
    result := false;

    try
        if Registro.TipoMedioPago = Peatypes.PAT then begin //Credito

                if Registro.PAT.TipoTarjeta > 0 then begin
                    DescripcionTarjeta := Trim(QueryGetValue(DMConnections.BaseCAC, 'SELECT Descripcion FROM TarjetasCreditoTipos  WITH (NOLOCK) WHERE CodigoTipoTarjetaCredito = ' + inttostr(Registro.PAT.TipoTarjeta)));
                    MedioPago := uppercase(DescripcionTarjeta + ' ' +
                      Trim(QueryGetValue(DMConnections.BaseCAC,
                        'SELECT dbo.FormatNumeroTarjeta(' + QuotedStr(Registro.PAT.NroTarjeta) + ')' )));
                end;
        end else begin
            if Registro.TipoMedioPago = Peatypes.PAC then begin //Cuenta
                if Registro.PAC.TipoCuenta > 0  then begin
                    DescripcionCuenta := Trim(QueryGetValue(DMConnections.BaseCAC, 'SELECT Descripcion FROM TiposCuentasBancarias WHERE CodigoTipoCuentaBancaria = ' + inttostr(Registro.PAC.TipoCuenta)));
                    DescripcionBanco := Trim(QueryGetValue(DMConnections.BaseCAC, 'SELECT Descripcion FROM Bancos WHERE CodigoBanco = ' + inttostr(Registro.PAC.CodigoBanco)));
                    MedioPago := Trim(Registro.PAC.NroCuentaBancaria);
                end;
            end;
        end;
    except
        exit;
    end;

    result := true;
end;

procedure TFormMediosPagos.btnSolicitudContactoClick(Sender: TObject);
begin
    txtDocumento.Text  := FDocumentoSolicitud;
    txt_Nombre.Text := Trim(FNombreSolicitud);
    FrameTelefono.CargarTelefono(FCodigoAreaSolicitud, FTelefonoSolicitud, 0);
end;

procedure TFormMediosPagos.FormShow(Sender: TObject);
begin
    //DateSeparatorOriginal := dateSeparator;
    //dateSeparator := '/';
end;

procedure TFormMediosPagos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    //dateSeparator := DateSeparatorOriginal;
end;

procedure TFormMediosPagos.cb_EmisoresTarjetasCreditosClick(
  Sender: TObject);
begin
    if self.Visible then begin
        if txt_NroTarjetaCredito.Enabled then
            txt_NroTarjetaCredito.SetFocus
        else
            txtDocumento.SetFocus;
    end;
end;

function TFormMediosPagos.DarCodigoEmisorTarjetaCredito: Integer;
begin
    if (trim(cb_EmisoresTarjetasCreditos.Text)='') or (cb_EmisoresTarjetasCreditos.itemindex<1) then result:=-1
    else result:=StrToInt(Trim(StrRight(cb_EmisoresTarjetasCreditos.Text,30)));

end;

end.
