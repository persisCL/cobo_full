object FrameSolucionOrdenServicio: TFrameSolucionOrdenServicio
  Left = 0
  Top = 0
  Width = 240
  Height = 118
  TabOrder = 0
  object GBEstadoDelReclamo: TGroupBox
    Left = 3
    Top = 1
    Width = 233
    Height = 112
    Caption = 'Estado del Reclamo'
    TabOrder = 0
    object Label1: TLabel
      Left = 9
      Top = 23
      Width = 36
      Height = 13
      Caption = 'Estado:'
    end
    object cbEstado: TVariantComboBox
      Left = 118
      Top = 20
      Width = 108
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 0
      OnSelect = cbEstadoSelect
      Items = <
        item
          Caption = 'Pendiente'
          Value = 'P'
        end
        item
          Caption = 'P. Resp. Int'
          Value = 'I'
        end
        item
          Caption = 'P. Resp. Ext'
          Value = 'E'
        end
        item
          Caption = 'Resuelta'
          Value = 'T'
        end
        item
          Caption = 'Cancelada'
          Value = 'C'
        end>
    end
    object PAdicional: TPanel
      Left = 5
      Top = 42
      Width = 224
      Height = 62
      BevelOuter = bvNone
      TabOrder = 1
      object Label2: TLabel
        Left = 4
        Top = 11
        Width = 72
        Height = 13
        Caption = 'F. Compromiso:'
      end
      object Label3: TLabel
        Left = 5
        Top = 40
        Width = 39
        Height = 13
        Caption = 'Entidad:'
      end
      object EFechaCompromiso: TDateEdit
        Left = 114
        Top = 8
        Width = 107
        Height = 21
        AutoSelect = False
        TabOrder = 0
        Date = -693594.000000000000000000
      end
      object EEntidad: TVariantComboBox
        Left = 113
        Top = 36
        Width = 108
        Height = 21
        Style = vcsDropDownList
        DropDownCount = 10
        ItemHeight = 13
        TabOrder = 1
        OnDropDown = EEntidadDropDown
        Items = <
          item
            Caption = 'Ninguno'
            Value = '0'
          end>
      end
    end
  end
  object SpObtenerEntidades: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerEntidades;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Tipo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end>
    Left = 192
    Top = 120
  end
end
