object FormGestionComponentesObligatorios: TFormGestionComponentesObligatorios
  Left = 229
  Top = 141
  Caption = 'Gestion de Obligatoriedad de Datos y Hints'
  ClientHeight = 472
  ClientWidth = 680
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 433
    Width = 680
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object Notebook: TNotebook
      Left = 483
      Top = 0
      Width = 197
      Height = 39
      Align = alRight
      PageIndex = 1
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'PageSalir'
        object BtnSalir: TButton
          Left = 116
          Top = 6
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Salir'
          TabOrder = 0
          OnClick = BtnSalirClick
        end
      end
      object TPage
        Left = 0
        Top = 0
        HelpContext = 1
        Caption = 'PageModi'
        object BtnAceptar: TButton
          Left = 24
          Top = 7
          Width = 79
          Height = 26
          Caption = '&Aceptar'
          Default = True
          TabOrder = 0
          OnClick = BtnAceptarClick
        end
        object BtnCancelar: TButton
          Left = 111
          Top = 7
          Width = 79
          Height = 26
          Cancel = True
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = BtnCancelarClick
        end
      end
    end
  end
  object AbmToolbar1: TAbmToolbar
    Left = 0
    Top = 0
    Width = 680
    Height = 33
    Habilitados = [btModi, btSalir, btBuscar]
    OnClose = AbmToolbar1Close
  end
  object GroupB: TPanel
    Left = 0
    Top = 349
    Width = 680
    Height = 84
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 2
    object Label1: TLabel
      Left = 18
      Top = 17
      Width = 43
      Height = 13
      Caption = 'Campo:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 18
      Top = 41
      Width = 27
      Height = 13
      Caption = 'Hints:'
      FocusControl = txt_Hint
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object txt_Campo: TEdit
      Left = 102
      Top = 8
      Width = 572
      Height = 21
      Hint = 'Se muestran s'#243'lo aquellos paises que tienen provinicas'
      Color = 16444382
      Enabled = False
      MaxLength = 100
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
    end
    object cb_Obligatorio: TCheckBox
      Left = 16
      Top = 61
      Width = 101
      Height = 17
      Alignment = taLeftJustify
      Caption = '&Obligatorio:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
    end
    object txt_Hint: TEdit
      Left = 102
      Top = 32
      Width = 572
      Height = 21
      Hint = 'Se muestran s'#243'lo aquellos paises que tienen provinicas'
      MaxLength = 100
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
    end
  end
  object DBList: TAbmList
    Left = 0
    Top = 81
    Width = 680
    Height = 268
    TabStop = True
    TabOrder = 3
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'MS Sans Serif'
    HeaderFont.Style = []
    SubTitulos.Sections = (
      
        #0'368'#0'Campo                                                      ' +
        '                                                       '
      #0'67'#0'Obligatorio   '
      #0'32'#0'Hints')
    HScrollBar = True
    RefreshTime = 100
    Table = ComponentesSistemas
    Style = lbOwnerDrawFixed
    ItemHeight = 14
    OnClick = DBListClick
    OnDrawItem = DBListDrawItem
    OnEdit = DBListEdit
    Access = [accAlta, accBaja, accModi]
    Estado = Normal
    ToolBar = AbmToolbar1
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 33
    Width = 680
    Height = 48
    Align = alTop
    Caption = 'Formulario'
    TabOrder = 4
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 676
      Height = 31
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      DesignSize = (
        676
        31)
      object cb_Formulario: TComboBox
        Left = 8
        Top = 1
        Width = 665
        Height = 21
        Style = csDropDownList
        Anchors = [akLeft, akRight]
        ItemHeight = 13
        TabOrder = 0
        OnClick = cb_FormularioClick
      end
    end
  end
  object ComponentesSistemas: TADOTable
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Filtered = True
    IndexFieldNames = 'CodigoSistema,DescripForm,DescripComponente'
    TableName = 'ComponentesSistemas'
    Left = 300
    Top = 80
  end
end
