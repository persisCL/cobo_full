{-----------------------------------------------------------------------------
 File Name: frm_ReIngresarComprobantes.pas
 Author:    FSandi
 Date Created: 02/01/2007
 Language: ES-AR
 Description:   Permite ingresar manualmente las boletas que fallaron en la impresora
 fiscal

Revision: 1
Author: nefernandez
Date: 16/03/2007
Description: Se pasa el parametro Usuario al SP CrearComprobante.

Revision : 2
    Author : FSandi
    Date : 06/08/2007
    Description : Se cambian todos los importes integer a Float, para evitar desbordamientos.

Revision : 3
    Author : vpaszkowicz
    Date : 12/12/2007
    Description : Agrego datos del arriendo.

Revision 4:
    Author: dAllegretti
    Date: 22/05/2008
    Description:    Se le agreg� el parametro @UsuarioCreacion del Stored spAgregarMovimientoCuentaIVA
                    y la correspondiente asignaci�n en el llamado al stored.
                    Se agregaron los par�metros @UsuarioModificacion y @FechaHoraModificacion
                    para el stored procedure spActualizarLoteFacturacion y la correspondiente asignaci�n
                    en el llamado al stored.

    Revision : 5
                        Author : vpaszkowicz
                        Date : 11/02/2009
                        Description :Agrego la funcionalidad de Arriendo cuota
                        Para poder reingresar comprobantes de este tipo.


Firma : SS_1147_MCA_20140408
Descripcion: se elimina sigla CN en las fn o sp

Etiqueta    : 20160315 MGO
Descripci�n : Se utiliza InstallIni en vez de ApplicationIni

-----------------------------------------------------------------------------}
unit frm_REIngresarComprobantes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Dateedit, ExtCtrls, utilProc, DmiCtrls, util, DBCtrls,
  ImgList, ADODB, DMConnection, ComCtrls, BuscaClientes, Utildb, DB,
  DPSControls, VariantComboBox, constParametrosGenerales, Validate,
  peaTypes, PeaProcs, dateUtils, ListBoxEx, DBListEx, DBClient,
  Frm_CargoNotaCredito, TimeEdit, DeclHard, DeclPrn;

type


    ExceptionFiscal = class(Exception);

    TArrString = Array Of String;

  TfrmReIngresarComprobantes = class(TForm)
    gbDatosConvenio: TGroupBox;
    Label1: TLabel;
    Label3: TLabel;
    peRUTCliente: TPickEdit;
    cbConveniosCliente: TVariantComboBox;
    Label7: TLabel;
    spObtenerMovimientosAFacturar: TADOStoredProc;
    dsObtenerPendienteFacturacion: TDataSource;
    gbDatosCliente: TGroupBox;
    lNombreCompleto: TLabel;
    lDomicilio: TLabel;
    lComuna: TLabel;
    lRegion: TLabel;
    Label31: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    gbItemsPendientes: TGroupBox;
    dbItemsPendientes: TDBListEx;
    cdsObtenerPendienteFacturacion: TClientDataSet;
    Imagenes: TImageList;
    Label2: TLabel;
    gbDatosComprobante: TGroupBox;
    lbl_NroComprobante: TLabel;
    lbl_FechaEmision: TLabel;
    edFechaEmision: TDateEdit;
    edNumeroComprobante: TNumericEdit;
    ObtenerCLiente: TADOStoredProc;
    spAgregarMovimientoCuentaIVA: TADOStoredProc;
    spCrearLoteFacturacion: TADOStoredProc;
    spCrearComprobante: TADOStoredProc;
    spActualizarLoteFacturacion: TADOStoredProc;
    Label5: TLabel;
    cbTiposComprobantes: TVariantComboBox;
    Label6: TLabel;
    lbl_Importe: TLabel;
    spObtenerConveniosCliente: TADOStoredProc;
    spObtenerDomicilioFacturacion: TADOStoredProc;
    ImpresoraFiscal: TFiscalPrinter;
    btnSalir: TButton;
    btnAgregarItem: TButton;
    btnModificarItem: TButton;
    btnEliminarItem: TButton;
    nb_Botones: TNotebook;
    btnRegistrar: TButton;
    spObtenerDetalleConceptoMovimiento: TADOStoredProc;
    lblRUT: TLabel;
    lblRUTRUT: TLabel;
    btnBuscarRUT: TButton;
    cbImpresorasFiscales: TVariantComboBox;
    lblImpresoraFiscal: TLabel;
    spReaplicarConvenio: TADOStoredProc;
    lblNumeroTelevia: TLabel;
    edNumeroTelevia: TEdit;
    edCantidadCuotas: TNumericEdit;
    lblCuotas: TLabel;
    CbSoporte: TCheckBox;
    lbl_EstadoEmisionBoleta: TLabel;
    spCrearComprobanteCuotas: TADOStoredProc;
    procedure btnSalirClick(Sender: TObject);
    procedure cbConveniosClienteChange(Sender: TObject);
    procedure peRUTClienteButtonClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure dbItemsPendientesDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: String; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure dbItemsPendientesDblClick(Sender: TObject);
    procedure edNumeroComprobanteChange(Sender: TObject);
    procedure cdsObtenerPendienteFacturacionAfterScroll(DataSet: TDataSet);
    procedure btnAgregarItemClick(Sender: TObject);
    procedure btnModificarItemClick(Sender: TObject);
    procedure btnEliminarItemClick(Sender: TObject);
    procedure btnRegistrarClick(Sender: TObject);
    procedure cbTiposComprobantesChange(Sender: TObject);
    procedure cdsObtenerPendienteFacturacionCalcFields(DataSet: TDataSet);
    procedure btnBuscarRUTClick(Sender: TObject);
    procedure peRUTClienteChange(Sender: TObject);
    procedure edNumeroComprobanteExit(Sender: TObject);
    procedure edFechaEmisionExit(Sender: TObject);
    procedure cbImpresorasFiscalesChange(Sender: TObject);
    procedure CbSoporteClick(Sender: TObject);
    procedure edNumeroTeleviaExit(Sender: TObject);
  private
    { Private declarations }
	FUltimaBusqueda : TBusquedaCliente;
    FLoteFacturacion : Integer;
	FCodigoCliente : Integer;
	FFechaCorteMaximaPosible: TDateTime;
    FTotalComprobante : Double;
    FTipoComprobante: AnsiString;
    // Almacena el n� de boleta que retorna la impresora fiscal.
    FNroBoletaFiscal: Int64;
    // Indica si se est� registrando una boleta fiscal.
    FRegistrandoBoletaFiscal: Boolean;
    // Almacena el C�digo de Convenio a quien se le emite el Comprobante.
    FCodigoConvenio: Integer;
    // Solo para las boletas, porque si se pierde uno no importa para el campo numeroComprobante de Comprobantes
    FNumeroComprobanteBoleta: Int64;
    // Modo de recuperaci�n de error con el cual se realiza la impresi�n de boletas
    FModoRecuperacionImpresion: Byte;
    FImprimirBoletaEnImpresoraFiscal: boolean;
    FCodigoImpresora: integer;
    FNumeroSerie: String;
    //Agrego todas las variables para que funcione arriendo como en el ingresar comprobantes
    FCodigoConceptoArriendo: integer;
    FCodigoConceptoSoporteTelevia : Integer;
    FIncluirSoporte : Boolean;
    FMinimaCantidadCuotasArriendo: integer;
    FMaximaCantidadCuotasArriendo: integer;
    FCotizacion: double;
    FCodigoConceptoArriendoCuotasMail: integer;
    FCodigoConceptoArriendoCuotasPostal: integer;
    FTipoEnvioDocumento: integer;
    FCodigoConcesionariaNativa: Integer;										//SS_1147_MCA_20140408
    procedure ClearCaptions;
	procedure CargarTiposComprobantes;
    procedure MostrarDomicilioFacturacion;
	procedure ActualizarBotonRegistrar;
	procedure AgregarNuevosMovimientosCuenta;
    procedure AgregarLoteFacturacion(NumeroComprobante: integer);
    function AgregarComprobante: integer;
	procedure ActualizarLotesFacturacion;
    function  ObtenerSignoMovimientoCuenta : integer;
    function GrabarBoleta: Boolean;
    function ObtenerCodigoImpresora(NumeroDeSerie: String): Integer;
    procedure AsentarComprobante;
    procedure GuardarComprobante;
    procedure ResetearControles;
    function ObtenerIVA: Double;
    function SetearDatosArriendo: Boolean;
    procedure AgregarSoporteInterfaz(Mostrar:Boolean);
    function ObtenerCotizacionMoneda(const Fecha: TDateTime; const Moneda:String): boolean;
    function InicializarDatosArriendo: boolean;
    function ValidarTelevia: boolean;
    function ValidarCuotasArriendo:boolean;
    procedure CargarComprobantesPendientes;
    procedure AgregarCuotasComprobanteArriendo;
    function SetearDatosArriendoCuotas:Boolean;
  public
    { Public declarations }
    function Inicializar(ImprimirBoletaEnImpresoraFiscal: boolean): Boolean;
    Function ValidarNumeroComprobante (NumeroComprobante : Integer) :Boolean;
    Function ValidarFechaComprobante (NumeroComprobante : Integer; Fecha:AnsiString; VAR Error:AnsiString):Boolean;
  end;


implementation

resourcestring
    MSG_ERROR_GENERAL = 'Ocurri� un error cuando se intent� generar una Boleta.';
    MSG_DRIVER_IMPRESORA = 'Driver Impresora: ';
    MSG_BASE_DATOS = 'Base de Datos: ';
    MSG_ERROR_AL_OBTENER_NRO_SERIE = 'Ocurri� un error al obtener el N�mero de Serie de la Impresora Fiscal';
    MSG_NUMERO_FISCAL_ERRONEO = 'El N�mero fiscal no puede ser negativo'  + CRLF + 'Archivo: %s';
    MSG_FECHA_COMPROBANTE_INCORRECTA = 'La Fecha del comprobante ingresado es incorrecta.';
    MSG_COMPROBANTE_NO_ENTERO = 'El Valor del Comprobante no es un n�mero valido';
    MSG_CAPTION_FECHA_INCORRECTA = 'Fecha Inv�lida';
    MSG_NUMERO_COMPRANTE_INCORRECTO = 'El N�mero de comprobante ingresado no puede ser ultilizado.';
    MSG_CAPTION_COMPROBANTE_INCORRECTO = 'N�mero Invalido';

const
    // Modos de recuperaci�n de error al imprimir una boleta fiscal.
    // Modo que indica que NO se desea continuar con la impresi�n de la boleta.
    MODO_RECUPERACION_CANCELAR  = 0;
    // Modo que indica que SI se desea continuar con la impresi�n de la boleta.
    MODO_RECUPERACION_CONTINUAR = 1;

    BOTON_CONTINUAR = 0;
    BOTON_REGISTRAR = 1;
    TC_ARRIENDO = 'Arriendo Telev�a';
    TC_ARRIENDO_CUOTAS = 'Arriendo Telev�a Cuotas';
{$R *.dfm}

function TfrmReIngresarComprobantes.InicializarDatosArriendo: boolean;
resourcestring
    MSG_CONCEPTO_ARRIENDO_INVALIDO       = 'El valor del concepto para generar arriendos de telev�as es inv�lido';
    MSG_CONCEPTO_SOPORTE_INVALIDO        = 'El valor del concepto para soporte de telev�as para motos es inv�lido';
    MSG_ERROR_CARGANDO_CONCEPTO_ARRIENDO = 'Error obteniendo el concepto para generar arriendos de telev�as';
    MSG_ERROR_CARGANDO_CONCEPTO_SOPORTE = 'Error obteniendo el concepto para soportes de telev�as de motos';
    MSG_ERROR_CARGANDO_CUOTAS_ARRIENDO   = 'Error obteniendo cantidades m�xima y m�nima de cuotas para arriendo de telev�as ';

begin
    Result := True;
    lblNumeroTelevia.Visible := False ;
    edNumeroTelevia.Visible  := False ;
    lblCuotas.Visible        := False ;
    edCantidadCuotas.Visible := False ;

    try
        if not (ObtenerParametroGeneral(DMConnections.BaseCAC, 'MINIMA_CANTIDAD_CUOTAS_ARRIENDO', FMinimaCantidadCuotasArriendo) and
                ObtenerParametroGeneral(DMConnections.BaseCAC, 'MAXIMA_CANTIDAD_CUOTAS_ARRIENDO', FMaximaCantidadCuotasArriendo)) then begin
                MsgBoxErr( MSG_ERROR_CARGANDO_CUOTAS_ARRIENDO, Caption, Caption, MB_ICONSTOP);
                Result := False;
        end;
    except
        on e : Exception do begin
                MsgBoxErr( MSG_ERROR_CARGANDO_CUOTAS_ARRIENDO, Caption, Caption, MB_ICONSTOP);
                Result := False;
        end;
    end;

    try
{INICIO: TASK_005_JMA_20160418
        FCodigoConceptoArriendo := QueryGetValueInt(DMConnections.BaseCAC, 'select dbo.CONST_CODIGO_CONCEPTO_ARRIENDO_TELEVIA()');
}		
		FCodigoConceptoArriendo := QueryGetValueInt(DMConnections.BaseCAC, 'select dbo.CONST_CODIGO_CONCEPTO_ARRIENDO_TELEVIA(dbo.ObtenerConcesionariaNativa())');
{TERMINO: TASK_005_JMA_20160418}		
        if FCodigoConceptoArriendo <= 0 then begin
            MsgBoxErr( MSG_CONCEPTO_ARRIENDO_INVALIDO, Caption, Caption, MB_ICONSTOP);
            Result := False;
        end;
    except
        on e : Exception do begin
            MsgBoxErr( MSG_ERROR_CARGANDO_CONCEPTO_ARRIENDO, e.Message, Caption, MB_ICONSTOP);
            Result := False;
        end;
    end;

//Revision 14
    try
{INICIO: TASK_005_JMA_20160418	
        FCodigoConceptoSoporteTelevia := QueryGetValueInt(DMConnections.BaseCAC, 'select dbo.CONST_CODIGO_CONCEPTO_SOPORTE_TELEVIA()');
}
		FCodigoConceptoSoporteTelevia := QueryGetValueInt(DMConnections.BaseCAC, 'select dbo.CONST_CODIGO_CONCEPTO_SOPORTE_TELEVIA(dbo.ObtenerConcesionariaNativa())');
{TERMINO: TASK_005_JMA_20160418}		
        if FCodigoConceptoSoporteTelevia <= 0 then begin
            MsgBoxErr( MSG_CONCEPTO_SOPORTE_INVALIDO, Caption, Caption, MB_ICONSTOP);
            Result := False;
        end;
    except
        on e : Exception do begin
            MsgBoxErr( MSG_ERROR_CARGANDO_CONCEPTO_SOPORTE, e.Message, Caption, MB_ICONSTOP);
            Result := False;
        end;
    end;
    //arriendo en cuotas por mail
    try
{INICIO: TASK_005_JMA_20160418		
        FCodigoConceptoArriendoCuotasMail := QueryGetValueInt(DMConnections.BaseCAC, 'select dbo.CONST_CODIGO_CONCEPTO_ARRIENDO_TELEVIA_CUOTAS_MAIL()');
}		
		FCodigoConceptoArriendoCuotasMail := QueryGetValueInt(DMConnections.BaseCAC, 'select dbo.CONST_CODIGO_CONCEPTO_ARRIENDO_TELEVIA_CUOTAS_MAIL(dbo.ObtenerConcesionariaNativa())');
{TERMINO: TASK_005_JMA_20160418}		
        if FCodigoConceptoArriendoCuotasMail <= 0 then begin
            MsgBoxErr( MSG_CONCEPTO_ARRIENDO_INVALIDO, Caption, Caption, MB_ICONSTOP);
            Result := False;
        end;
    except
        on e : Exception do begin
            MsgBoxErr( MSG_ERROR_CARGANDO_CONCEPTO_ARRIENDO, e.Message, Caption, MB_ICONSTOP);
            Result := False;
        end;
    end;

    //arriendo en cuotas por correo postal
    try
{INICIO: TASK_005_JMA_20160418
        FCodigoConceptoArriendoCuotasPostal := QueryGetValueInt(DMConnections.BaseCAC, 'select dbo.CONST_CODIGO_CONCEPTO_ARRIENDO_TELEVIA_CUOTAS_POSTAL()');
}
		FCodigoConceptoArriendoCuotasPostal := QueryGetValueInt(DMConnections.BaseCAC, 'select dbo.CONST_CODIGO_CONCEPTO_ARRIENDO_TELEVIA_CUOTAS_POSTAL(dbo.ObtenerConcesionariaNativa())');
{TERMINO: TASK_005_JMA_20160418}
        if FCodigoConceptoArriendoCuotasPostal <= 0 then begin
            MsgBoxErr( MSG_CONCEPTO_ARRIENDO_INVALIDO, Caption, Caption, MB_ICONSTOP);
            Result := False;
        end;
    except
        on e : Exception do begin
            MsgBoxErr( MSG_ERROR_CARGANDO_CONCEPTO_ARRIENDO, e.Message, Caption, MB_ICONSTOP);
            Result := False;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: inicializar
Author       : flamas
Date Created : 20-12-2004
Description  : Iniciliza el form.
Parameters   : None
Return Value : Boolean
*******************************************************************************}
function TfrmReIngresarComprobantes.Inicializar(ImprimirBoletaEnImpresoraFiscal: boolean): Boolean;
resourcestring
	MSG_CANNOT_LOAD_INVOICE_GENERATION  = 'No se pudo cargar el Ingreso de Comprobantes';
  	MSG_ERROR_IMPRESORA_NO_ENCONTRADA 	= 'No se encontro la impresora en la n�mina de impresoras registadas';
    MSG_ERROR_IMPRESORA_POR_DEFECTO     = 'No se detecto Impresora conectada';
var
    NumeroSerie: PChar;
    Index: integer;

begin
    cbTiposComprobantes.OnChange := nil;
    try
        FImprimirBoletaEnImpresoraFiscal := ImprimirBoletaEnImpresoraFiscal;

        // Si el dato que indica el Modo de Recuperaci�n no existe en el archivo,
        // crearlo con el valor por defecto de Continuar.
        //{ INICIO : 20160315 MGO
        if not ApplicationIni.ValueExists('FiscalPrinter', 'ModoRecuperacionError') then begin
            ApplicationIni.WriteInteger('FiscalPrinter', 'ModoRecuperacionError', MODO_RECUPERACION_CONTINUAR);
        end;
        FModoRecuperacionImpresion  := ApplicationIni.ReadInteger('FiscalPrinter', 'ModoRecuperacionError', MODO_RECUPERACION_CONTINUAR);
        {
        if not InstallIni.ValueExists('FiscalPrinter', 'ModoRecuperacionError') then begin
            InstallIni.WriteInteger('FiscalPrinter', 'ModoRecuperacionError', MODO_RECUPERACION_CONTINUAR);
        end;
        FModoRecuperacionImpresion  := InstallIni.ReadInteger('FiscalPrinter', 'ModoRecuperacionError', MODO_RECUPERACION_CONTINUAR);
        }// FIN : 20160315 MGO
        FCodigoCliente              := -1;
        FCodigoConvenio             := -1;
        FNroBoletaFiscal            := -1;
        FRegistrandoBoletaFiscal    := False;
        FCodigoConcesionariaNativa	:= ObtenerCodigoConcesionariaNativa;		//SS_1147_MCA_20140408

        //siempre llenamos el combo de impresoras
        CargarImpresorasFiscales(DMConnections.BaseCAC, cbImpresorasFiscales, 0);
        cbImpresorasFiscales.ItemIndex := 0 ;

        //Obtiene el codigo de la impresora , si hay una conectada, reubica el combo de impresoras
        try
        if not ImpresoraFiscal.Active then ImpresoraFiscal.Active := True;
       
        if ImpresoraFiscal.ObtenerNumeroSerie(NumeroSerie) then begin
            FNumeroSerie        := NumeroSerie;
            FCodigoImpresora    := ObtenerCodigoImpresora(NumeroSerie);
            for Index := 0 to cbImpresorasFiscales.Items.Count - 1 do
                if cbImpresorasFiscales.Items[Index].Value = FCodigoImpresora then begin
                    cbImpresorasFiscales.ItemIndex := Index;
                    Break;
                end;
            end;
        except
            //si no hay impresora conectada, entonces pone la primera de la lista por defecto
            on e : Exception do begin
                MsgBoxErr(MSG_ERROR_IMPRESORA_POR_DEFECTO, Caption, Caption, MB_ICONWARNING);
                cbImpresorasFiscales.ItemIndex := 0;
            end;
        end;

        ClearCaptions;

		FFechaCorteMaximaPosible := StrToDate(DateToStr(NowBase(DMConnections.BaseCAC)));
        edFechaEmision.Date := StrToDate(DateToStr(NowBase(DMConnections.BaseCAC)));
        EdFechaEmision.Enabled := False;
        EdFechaEmision.Visible := False;
        Lbl_FechaEmision.Enabled := False;
        Lbl_FechaEmision.Visible := False;

		CargarTiposComprobantes;
        if FImprimirBoletaEnImpresoraFiscal then begin
            if not InicializarDatosArriendo then begin
                Result := False;
                Exit;
            end;

            if FModoRecuperacionImpresion = MODO_RECUPERACION_CANCELAR then begin
                Result := True;
            end else begin
                //El ProccessMessage es para que termine de dibujar cuando no encuentre la impresora
                Application.ProcessMessages;
                Result := True;
                end;
            end else begin
            Result := True;
        end;
        cbTiposComprobantes.OnChange := cbTiposComprobantesChange;
    except
        on e : Exception do begin
            MsgBoxErr( MSG_CANNOT_LOAD_INVOICE_GENERATION, e.Message, Caption, MB_ICONSTOP);
            Result := False;
        end;
    end;
end;


procedure TfrmReIngresarComprobantes.btnSalirClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarTiposComprobantes;
  Author:    flamas
  Date Created: 22/01/2005
  Description: Carga los tipos de Comprobantes
  Parameters: TipoComprobante: AnsiString;
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure TfrmReIngresarComprobantes.CargarTiposComprobantes;
begin
    cbTiposComprobantes.Items.Clear;
    if FImprimirBoletaEnImpresoraFiscal then
        cbTiposComprobantes.Items.Add(DescriTipoComprobante(TC_BOLETA), TC_BOLETA);

    cbTiposComprobantes.Value := Null;
    FTipoComprobante := EmptyStr;
end;

{******************************** Function Header ******************************
Function Name: ObtenerSignoMovimientoCuenta
Author       : FLamas
Date Created : 24-01-2005
Description  : Devuelve el signo del Movimiento de cuenta
Parameters   : Sender:
Return Value : None
*******************************************************************************}
function TfrmReIngresarComprobantes.ObtenerSignoMovimientoCuenta : integer;
begin
	if FTipoComprobante = TC_NOTA_CREDITO then Result := -1
    else Result := 1;
end;


{-----------------------------------------------------------------------------
  Function Name: MostrarDomicilioFacturacion
  Author:    flamas
  Date Created: 23/03/2005
  Description:
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TfrmReIngresarComprobantes.MostrarDomicilioFacturacion;
resourcestring
	MSG_ERROR = 'Error';
	MSG_ERROR_GETTING_ADDRESS = 'Error obteniendo el domicilio del cliente';
begin
	with spObtenerDomicilioFacturacion, Parameters do begin
    	// Mostramos el domicilio de Facturaci�n
        Close;
    	ParamByName('@CodigoConvenio').Value := FCodigoConvenio;
        try
        	ExecProc;
    		lDomicilio.Caption	:= ParamByName('@Domicilio').Value;
 			lComuna.Caption		:= ParamByName('@Comuna').Value;
            lRegion.Caption 	:= ParamByName('@Region').Value;
        except
        	on E: Exception do begin
            	MsgBoxErr( MSG_ERROR_GETTING_ADDRESS, e.Message, MSG_ERROR, MB_ICONSTOP);
            end;
        end;
    end;
end;


{******************************** Function Header ******************************
Function Name: cbConveniosClienteChange
Author       : fmalisia
Date Created : 24-11-2004
Description  : Cuando cambia la lista de convenios se refrescan
               la lista de items de la factura a realizar.
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmReIngresarComprobantes.cbConveniosClienteChange(Sender: TObject);
var
    indice: integer;
begin
    if cbConveniosCliente.ItemIndex = -1 then Exit;
    FCodigoConvenio := cbConveniosCliente.Value;
	MostrarDomicilioFacturacion;

    for Indice := cbTiposComprobantes.Items.Count - 1 downto  0 do begin
        if (cbTiposComprobantes.Items[Indice].Caption = TC_ARRIENDO) or (cbTiposComprobantes.Items[Indice].Caption = TC_ARRIENDO_CUOTAS) then begin
            cbTiposComprobantes.Items.Delete(Indice);
        end;
    end;

    //if QueryGetValueInt(DMConnections.BaseCAC, format('select dbo.EsConvenioCN(%d)', [FCodigoConvenio])) = 1 then   									//SS_1147_MCA_20140408
    if QueryGetValueInt(DMConnections.BaseCAC, format('select dbo.EsConvenioNativo(%d)', [FCodigoConvenio])) = 1 then			//SS_1147_MCA_20140408
        if FImprimirBoletaEnImpresoraFiscal then begin
            cbTiposComprobantes.Items.Add(TC_ARRIENDO, TC_BOLETA);
            cbTiposComprobantes.Items.Add(TC_ARRIENDO_CUOTAS, TC_BOLETA);
        end;
    cbTiposComprobantes.ItemIndex := 0 ;
    cbTiposComprobantesChange(Self);
end;

procedure TfrmReIngresarComprobantes.cbImpresorasFiscalesChange(
  Sender: TObject);
begin
    FCodigoImpresora := cbImpresorasFiscales.Value;
    if Trim(edNumeroComprobante.Text) <> EmptyStr then
        edNumeroComprobanteExit(self);
end;

{******************************** Function Header ******************************
Function Name: CbSoporteClick
Author : vpaszkowicz
Date Created : 12/12/2007
Description : Agrego la funcionalidad de arriendo
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmReIngresarComprobantes.CbSoporteClick(Sender: TObject);
begin
    FIncluirSoporte := CbSoporte.Checked;
    if not SetearDatosArriendo then begin
        btnRegistrar.Enabled := False;
        Exit;
    end;
end;

{******************************** Function Header ******************************
Function Name: peRUTClienteButtonClick
Author       : fmalisia
Date Created : 24-11-2004
Description  : Permite buscar un cliente por medio del form FormBuscaClientes.
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmReIngresarComprobantes.peRUTClienteButtonClick(Sender: TObject);
var
    f: TFormBuscaClientes;
begin
	FillChar (FUltimaBusqueda, SizeOf (FUltimaBusqueda), 0);
    Application.CreateForm(TFormBuscaClientes, F);
	if F.Inicializa(FUltimaBusqueda) then begin
        F.ShowModal;
        if F.ModalResult = idOk then begin
            FUltimaBusqueda := F.UltimaBusqueda;
            peRUTCliente.Text :=  F.Persona.NumeroDocumento;
            peRUTCliente.setFocus;
        end;
    end;
    btnBuscarRUTClick(Self);
    F.Free;
end;


procedure TfrmReIngresarComprobantes.peRUTClienteChange(Sender: TObject);
begin
    cbConveniosCliente.Clear;
    cbConveniosClienteChange(Self);
end;

procedure TfrmReIngresarComprobantes.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    // Desactivar la Impresora Fiscal
    if FImprimirBoletaEnImpresoraFiscal and ImpresoraFiscal.Active then begin
        ImpresoraFiscal.Active := False;
    end;

	Action := caFree;
end;

procedure TfrmReIngresarComprobantes.FormCreate(Sender: TObject);
begin
	CenterForm(Self);
end;

procedure TfrmReIngresarComprobantes.ClearCaptions;
begin
    lNombreCompleto.Caption	:= '';
    lDomicilio.Caption      := '';
    lComuna.Caption         := '';
    lRegion.Caption 		:= '';
    lblRUTRUT.CAption       := '';
end;


{******************************** Function Header ******************************
Function Name: dbItemsPendientesDrawText
Author       : flamas
Date Created : 24-01-2005
Description  : Dibuja el s�mbolo de Checked
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmReIngresarComprobantes.dbItemsPendientesDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: String; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
var
	bmp: TBitMap;
begin
    with Sender do begin
        if (Column.FieldName = 'Marcado') then begin
            DefaultDraw := False;
            Text := '';
            canvas.FillRect(rect);
            bmp := TBitMap.Create;
            if (cdsObtenerPendienteFacturacion.FieldByName('Marcado').AsBoolean ) then
          	    Imagenes.GetBitmap(0, Bmp)
            else
                Imagenes.GetBitmap(1, Bmp);
            canvas.Draw(rect.Left, rect.Top,bmp);
            bmp.Free;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: dbItemsPendientesDblClick
Author       : flamas
Date Created : 24-01-2005
Description  : Marca y Desmarca items
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmReIngresarComprobantes.dbItemsPendientesDblClick(Sender: TObject);
begin
	if cdsObtenerPendienteFacturacion.IsEmpty then Exit;

	with cdsObtenerPendienteFacturacion do begin
    	Edit;
        FieldByName('Marcado').AsBoolean := not FieldByName('Marcado').AsBoolean;
        Post;
        ActualizarBotonRegistrar;
    end;
end;



{******************************** Function Header ******************************
Function Name: ActualizarBotonRegistrar
Author       : flamas
Date Created : 24-01-2005
Description  : Habilita y deshabilita el bot�n de registrar
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmReIngresarComprobantes.ActualizarBotonRegistrar;
var
	bHayMarcados : boolean;
    FBookmark  	 : TBookmark;
begin
    FTotalComprobante := 0;
    lbl_Importe.Caption := FormatearImporteconDecimales(DMConnections.BaseCAC, FTotalComprobante, true, true, true);
    with cdsObtenerPendienteFacturacion do begin
    	bHayMarcados := False;
        DisableControls;
		FBookmark := GetBookmark;
        First;
        // Verifica si hay items marcados
        while not EOF do begin
        	if ( FieldByName('Marcado').AsBoolean ) then begin
            	bHayMarcados := True;
                FTotalComprobante := FTotalComprobante + FieldByName('ImporteCtvs').AsFloat ;
            end;
            Next;
        end;
        GotoBookmark( FBookmark );
        FreeBookmark( FBookmark );
        lbl_Importe.Caption := FormatearImporteconDecimales(DMConnections.BaseCAC, FTotalComprobante, true, true, true);
        EnableControls;
    end;
    btnRegistrar.Enabled := ((not cdsObtenerPendienteFacturacion.IsEmpty) and
    						(bHayMarcados) and
                            (edFechaEmision.Visible = True) and
                            (((cbTiposComprobantes.Value = TC_BOLETA) or (cbTiposComprobantes.Value = TC_ARRIENDO)) and (FTotalComprobante > 0)) and
                            (edFechaEmision.Date <> Nulldate)and
                            //((cbTiposComprobantes.Value = TC_BOLETA) or (edNumeroComprobante.Text <> EmptyStr)) and
    						(edFechaEmision.Date <> Nulldate));




end;

{******************************** Function Header ******************************
Function Name: ValidarFechaComprobante
Author       : FSandi
Date Created : 03-01-2007
Description  : Verifica que la fecha ingresada sea menor a la fecha de la boleta superior y
                mayor a la fecha de la boleta inferior
Parameters   : NumeroComprobante : Integer; Fecha:AnsiString;
                VAR Error:AnsiString
Return Value : Boolean
*******************************************************************************}
Function TfrmReIngresarComprobantes.ValidarFechaComprobante (NumeroComprobante : Integer; Fecha:AnsiString; VAR Error:AnsiString):Boolean;
begin
    ValidarFechaComprobante:=False;
    if  ((Fecha <> EmptyStr) and (NumeroComprobante > 0)) then begin
        Error :=Trim(QueryGetValue(DMConnections.BaseCAC,
            'select  dbo.VerificarFechaTicketManual(dbo.CONST_TIPO_COMPROBANTE_BOLETA(),'+
            IntToStr(NumeroComprobante)+','+
            IntToStr(FCodigoImpresora)+ ','''+ Fecha + ''')'));
        If Trim(Error) = 'CORRECTO' Then  ValidarFechaComprobante := True;
    end;
end;

{******************************** Function Header ******************************
Function Name: edFechaEmisionExit
Author : vpaszkowicz
Date Created : 20/12/2007
Description : Corrijo que setee los datos de arriendo solo si el combo tiene se-
teado el valor de boleta de arriendo.
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmReIngresarComprobantes.edFechaEmisionExit(Sender: TObject);
var
    Fecha, Error : AnsiString;
    NroConvert : Integer;

Begin
    BtnRegistrar.Enabled := False;
    If Not TryStrtoInt(edNumeroComprobante.Text, NroConvert) then Begin
        MsgBoxBalloon(MSG_COMPROBANTE_NO_ENTERO, MSG_CAPTION_COMPROBANTE_INCORRECTO, MB_ICONSTOP, edNumeroComprobante);
        Exit;
    End;
    Fecha := FormatDateTime ('yyyymmdd', EdFechaEmision.Date);
    If ValidarFechaComprobante (strtoint(Trim(edNumeroComprobante.Text)), Fecha, Error) then Begin
        //If cdsObtenerPendienteFacturacion.RecordCount>0 then BtnRegistrar.Enabled := True;
        if (cbTiposComprobantes.Items[cbTiposComprobantes.ItemIndex].Caption = TC_ARRIENDO) and (not SetearDatosArriendo) then begin
            btnRegistrar.Enabled := False;
            Exit;
        end;
        if (cbTiposComprobantes.Items[cbTiposComprobantes.ItemIndex].Caption = TC_ARRIENDO_CUOTAS) and (not SetearDatosArriendoCuotas) then begin
            btnRegistrar.Enabled := False;
            Exit;
        end;        
    End Else Begin
        MsgBoxBalloon(MSG_FECHA_COMPROBANTE_INCORRECTA + CRLF + Error, MSG_CAPTION_FECHA_INCORRECTA, MB_ICONSTOP, EdFechaEmision);
        Exit;
    End;
End;


procedure TfrmReIngresarComprobantes.edNumeroComprobanteChange(Sender: TObject);
begin
    ActualizarBotonRegistrar;
end;

{******************************** Function Header ******************************
Function Name: ValidarNumeroComprobante
Author       : FSandi
Date Created : 03-01-2007
Description  : Verifica que el Numero de Boleta ingresado este disponible en la base
                de datos y no sea ni el primero ni el ultimo.
Parameters   : NumeroComprobante : Integer;
Return Value : Boolean
*******************************************************************************}
Function TfrmReIngresarComprobantes.ValidarNumeroComprobante (NumeroComprobante : Integer):Boolean;
begin
    Result := False;
    if NumeroComprobante > 0 then begin
            If (QueryGetValue(DMConnections.BaseCAC,
                'select  dbo.VerificarNoExisteTicket(dbo.CONST_TIPO_COMPROBANTE_BOLETA(),'+
                IntToStr(NumeroComprobante) + ',' +
                IntToStr(FCodigoImpresora) + ')')) = 'True' then  Result := True;
    end;
end;

procedure TfrmReIngresarComprobantes.edNumeroComprobanteExit(Sender: TObject);
Var
    NroConvert : Integer;
Begin
    If Not TryStrtoInt(edNumeroComprobante.Text,NroConvert) Then Begin
        MsgBoxBalloon(MSG_COMPROBANTE_NO_ENTERO, MSG_CAPTION_COMPROBANTE_INCORRECTO, MB_ICONSTOP,edNumeroComprobante);
        EdFechaEmision.Enabled := False;
        EdFechaEmision.Visible := False;
        Lbl_FechaEmision.Enabled := False;
        Lbl_FechaEmision.Visible := False;
        BtnRegistrar.Enabled := False;
        Exit;
    End;

    If ValidarNumeroComprobante (strtoint(Trim(edNumeroComprobante.Text))) Then Begin
            EdFechaEmision.Enabled := True;
            EdFechaEmision.Visible := True;
            Lbl_FechaEmision.Enabled := True;
            Lbl_FechaEmision.Visible := True;
            EdFechaEmision.SetFocus;
    End
        Else Begin
            MsgBoxBalloon(MSG_NUMERO_COMPRANTE_INCORRECTO, MSG_CAPTION_COMPROBANTE_INCORRECTO, MB_ICONSTOP, edNumeroComprobante);
            EdFechaEmision.Enabled := False;
            EdFechaEmision.Visible := False;
            Lbl_FechaEmision.Enabled := False;
            Lbl_FechaEmision.Visible := False;
            BtnRegistrar.Enabled := False;
            Exit;
        End;
End;

{******************************** Function Header ******************************
Function Name: ValidarTelevia
Author : vpaszkowicz
Date Created : 12/12/2007
Description : Agrego la funcionalidad para arriendo como en el ingresarComprobante
Parameters : None
Return Value : boolean
*******************************************************************************}
function TfrmReIngresarComprobantes.ValidarTelevia: boolean;
resourcestring
    MSG_ERROR_TELEVIA0 = 'El telev�a ingresado no existe';
    MSG_ERROR_TELEVIA1 = 'El telev�a pertence al convenio pero est� mal ingresado';
    MSG_ERROR_TELEVIA2 = 'El telev�a est� bien ingresado pero no pertenece al convenio';

    MSG_ERROR_TELEVIA_1 = 'Atenci�n: El telev�a que se indic�, est� actualmente en una boleta de arriendo no anulada para este convenio';
    MSG_ERROR_TELEVIA_2 = 'El telev�a no est� en arriendo o entregado al cliente. No se puede facturar el arriendo';
    MSG_ERROR_TELEVIA3 = 'El telev�a ingresado no fue entregado en arriendo en cuotas para este convenio';
var
    SerialNumber: DWORD;
    ValorDevuelto: integer;
    ResultadoFase1: boolean ;
    ResultadoFase2: boolean ;
    ResultadoFase3: boolean;
    StringMensajeError: string;
begin
    // no puedo lanzar dos MsgBoxBalloon juntos...
    if Trim(edNumeroTelevia.Text) = EmptyStr then begin
        Result := False;
    end;

    ValorDevuelto := QueryGetValueInt(DMConnections.BaseCAC,
               //format('select dbo.ExisteTeleviaEnCuentaCN(%d,''%s'')',		//SS_1147_MCA_20140408
               Format('select dbo.ExisteTeleviaEnCuentaNativa(%d, ''%s'')',     //SS_1147_MCA_20140408
                [FCodigoConvenio , edNumeroTelevia.Text ])) ;


    ResultadoFase1 := False;
    ResultadoFase2 := False;
    StringMensajeError := '';
    ResultadoFase3 := True;

    // entonces: si la respuesta es 1, la cuenta est� bien pero el TAG Mal
    // si la respuesta es cero ninguno de los dos est� bien
    // si la respuesta es 3, los dos est�n bien


    if ValorDevuelto = 0  then StringMensajeError := MSG_ERROR_TELEVIA0;
    if ValorDevuelto = 1  then StringMensajeError := MSG_ERROR_TELEVIA1;
    if ValorDevuelto = 2  then StringMensajeError := MSG_ERROR_TELEVIA2;
    if ValorDevuelto = 3  then ResultadoFase1 := True;

    SerialNumber := EtiquetaToSerialNumber(edNumeroTelevia.Text);

    // si el resultado es 0, no existe en arriendo y est� en situacion de ser arrendado
    // si el resultado es 1, entonces est� en arriendo y ya sali� por arriba
    // si el resultado es 2, no est� en arriendo y no puede ser arrendado.

    ValorDevuelto := QueryGetValueInt(DMConnections.BaseCAC,
                    format('select dbo.ExisteTeleviaEnArriendoConvenio(%d,%d)',
                    [FCodigoConvenio,SerialNumber])) ;

    if ValorDevuelto = 0  then ResultadoFase2 := True;
    if ValorDevuelto = 1  then begin  //esto ahora solo genera un warning rev 16
        ResultadoFase2 := True;
        StringMensajeError := StringMensajeError + CRLF + MSG_ERROR_TELEVIA_1;
    end;
    if ValorDevuelto = 2  then StringMensajeError := StringMensajeError + CRLF + MSG_ERROR_TELEVIA_2;

    if (StringMensajeError <> '')  then
        if (ValorDevuelto <> 1) then MsgBoxBalloon(StringMensajeError ,Caption, MB_ICONSTOP, edNumeroTelevia)
            else MsgBoxBalloon(StringMensajeError ,Caption, MB_ICONWARNING, edNumeroTelevia);

    // valido lo correspondiente a cuotas
    if cbTiposComprobantes.Text = TC_ARRIENDO_CUOTAS then begin
        ValorDevuelto := QueryGetValueInt(DMConnections.BaseCAC,
                            format('SELECT dbo.TagEntregadoEnArriendoEnCuotas(%d,''%s'')',
                            [FCodigoConvenio , edNumeroTelevia.Text ]));

        if ValorDevuelto = 1 then
            ResultadoFase3 := True
        else begin
            StringMensajeError :=  StringMensajeError + CRLF + MSG_ERROR_TELEVIA3;
            MsgBoxBalloon(StringMensajeError ,Caption, MB_ICONWARNING, edNumeroTelevia);
            ResultadoFase3 := False;
        end;
    end;

    Result := ResultadoFase1 and ResultadoFase2 and ResultadoFase3;
end;

{******************************** Function Header ******************************
Function Name: edNumeroTeleviaExit
Author : vpaszkowicz
Date Created : 12/12/2007
Description : Agrego la funcionalidad para arriendo como en el ingresar comprobante
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmReIngresarComprobantes.edNumeroTeleviaExit(Sender: TObject);
var
    EsMoto : Boolean;
begin
    if Trim(edNumeroTelevia.Text) = EmptyStr then
        Exit;
    if not ValidarTelevia then Exit;
    if cbTiposComprobantes.Text = TC_ARRIENDO then begin
        if QueryGetValue(DMConnections.BaseCAC,
            format('select dbo.EsTagMoto(''%s'')',
            [edNumeroTelevia.Text]))= 'True' then begin
                EsMoto := True
            end else begin
                EsMoto := False;
            end;
        if EsMoto then begin
            AgregarSoporteInterfaz (True);
            CbSoporte.Checked := True;
            SetearDatosArriendo;
        end else begin
            AgregarSoporteInterfaz (False);
            CbSoporte.Checked := False;
        end;
    end else begin
        AgregarSoporteInterfaz (False);
        CbSoporte.Checked := False;
    end;
end;

{******************************** Function Header ******************************
Function Name: cdsObtenerPendienteFacturacionAfterScroll
Author       : flamas
Date Created : 24-01-2005
Description  : Habilita y deshabilita los botones de modificar y eliminar
Parameters   : DataSet: TDataSet
Return Value : None
*******************************************************************************}
procedure TfrmReIngresarComprobantes.cdsObtenerPendienteFacturacionAfterScroll(DataSet: TDataSet);
begin
    // Habilito el bot�n de Modificar si es un item Nuevo y no estoy
    // registrando un comprobante.
	btnModificarItem.Enabled := (cdsObtenerPendienteFacturacion.FieldByName('Nuevo').AsBoolean)
                                    and (nb_Botones.PageIndex = BOTON_REGISTRAR)
                                    and (not FRegistrandoBoletaFiscal);
    btnEliminarItem.Enabled	:= btnModificarItem.Enabled;
    if (cbTiposComprobantes.Text = TC_ARRIENDO) or (cbTiposComprobantes.Text = TC_ARRIENDO_CUOTAS) then
        btnEliminarItem.Enabled	:= False ;
end;

{******************************** Function Header ******************************
Function Name: btnAgregarItemClick
Author       : flamas
Date Created : 24-01-2005
Description  : Agrega un item a facturar
Parameters   : Sender: TObject
Return Value : None

Revision: 1
Author: jconcheyro
Date: 08/09/2006
Description: Se utiliza ahora el form frmCargoNotaCredito que tiene el IVA discriminado

*******************************************************************************}
procedure TfrmReIngresarComprobantes.btnAgregarItemClick(Sender: TObject);
var
	f : TfrmCargoNotaCredito;
    EditorFechasHabilitado : boolean;
begin
    Application.CreateForm(TfrmCargoNotaCredito, f);
    f.Visible := False;
    if cbTiposComprobantes.Value = TC_BOLETA  then EditorFechasHabilitado := False else
        EditorFechasHabilitado := True;

    with cdsObtenerPendienteFacturacion do begin
    	if f.Inicializar(EditorFechasHabilitado, false, false, cbTiposComprobantes.Value, ObtenerIVA, edFechaEmision.Date, False) then begin
    		if f.ShowModal = mrOK then begin

            	Append;
                FieldByName('Marcado').AsBoolean        := True;
                FieldByName('CodigoConvenio').AsInteger := FCodigoConvenio;
                FieldByName('FechaHora').AsDateTime     := f.edFecha.Date;
                FieldByName('CodigoConcepto').AsInteger := f.cbConcepto.Value;
                FieldByName('Concepto').AsString        := f.cbConcepto.Items[f.cbConcepto.ItemIndex].Caption;
                FieldByName('Observaciones').AsString   := f.edDetalle.Text;
                FieldByName('DescImporte').AsString     := FormatearImporteConDecimales(DMConnections.BaseCAC,f.edImporte.Value + f.edImporteIVA.Value, True, True, True);
                FieldByName('ImporteCtvs').AsFloat      := f.edImporte.Value + f.edImporteIVA.Value;
                FieldByName('Nuevo').AsBoolean          := True;
                FieldByName('Cantidad').AsFloat         := 1;
                FieldByName('PorcentajeIVA').AsFloat   := f.edPorcentajeIVA.Value; //19
                FieldByName('ValorNeto').Value          := f.edImporte.Value ;
                FieldByName('ImporteIVA').Value         := f.edImporteIVA.Value;
                FieldByName('EstaImpreso').AsBoolean    := False;
                Post;
                ActualizarBotonRegistrar;
                btnModificarItem.Enabled    := not cdsObtenerPendienteFacturacion.IsEmpty;
                btnEliminarItem.Enabled     := not cdsObtenerPendienteFacturacion.IsEmpty;

                cdsObtenerPendienteFacturacionAfterScroll( cdsObtenerPendienteFacturacion );
            end;
        end;
    end;
    f.Free;
end;


{******************************** Function Header ******************************
Function Name: btnModificarItemClick
Author       : flamas
Date Created : 24-01-2005
Description  : Modifica un item a facturar
Parameters   : Sender: TObject
Return Value : None

Revision: 1
Author: jconcheyro
Date: 08/09/2006
Description: Se utiliza ahora el form frmCargoNotaCredito que tiene el IVA discriminado

*******************************************************************************}
procedure TfrmReIngresarComprobantes.btnModificarItemClick(Sender: TObject);
var
	f : TfrmCargoNotaCredito;
    EditorFechasHabilitado : boolean;
begin
    // Si no hay items no permitir modificar.
    if cdsObtenerPendienteFacturacion.IsEmpty then Exit;

    // no se puede modificar la fecha del movimiento para boletas, de arriendo o no
    if cbTiposComprobantes.Value = TC_BOLETA  then EditorFechasHabilitado := False else
    EditorFechasHabilitado := True;

    Application.CreateForm(TfrmCargoNotaCredito, f);
    f.Visible := False;
    if f.Inicializar(EditorFechasHabilitado, false, false, cbTiposComprobantes.Text <> EmptyStr, cdsObtenerPendienteFacturacion.FieldByName('CodigoConcepto').Value,
        cdsObtenerPendienteFacturacion.FieldByName('ImporteCtvs').AsFloat,
        99999999,
        cdsObtenerPendienteFacturacion.FieldByName('Observaciones').Value,
        '',
        cbTiposComprobantes.Value,
        ObtenerIVA,
        cdsObtenerPendienteFacturacion.FieldByName('FechaHora').AsDateTime,
        False) then begin
        if f.ShowModal = mrOK then begin
            cdsObtenerPendienteFacturacion.Edit;
            cdsObtenerPendienteFacturacion.FieldByName('FechaHora').AsDateTime       := f.edFecha.Date;
            cdsObtenerPendienteFacturacion.FieldByName('CodigoConcepto').AsInteger   := f.cbConcepto.Value;
            cdsObtenerPendienteFacturacion.FieldByName('Concepto').AsString          := f.cbConcepto.Items[f.cbConcepto.ItemIndex].Caption;
            cdsObtenerPendienteFacturacion.FieldByName('Observaciones').AsString     := f.edDetalle.Text;
            cdsObtenerPendienteFacturacion.FieldByName('DescImporte').AsString       := FormatearImporteConDecimales(DMConnections.BaseCAC, f.edImporte.Value + f.edImporteIVA.Value, True, True, True);
            cdsObtenerPendienteFacturacion.FieldByName('ImporteCtvs').AsFloat        := f.edImporte.Value + f.edImporteIVA.Value;
            cdsObtenerPendienteFacturacion.FieldByName('Cantidad').AsFloat           := 1;
            cdsObtenerPendienteFacturacion.FieldByName('PorcentajeIVA').AsFloat     := f.edPorcentajeIVA.Value; //19
            cdsObtenerPendienteFacturacion.FieldByName('ValorNeto').Value            := f.edImporte.Value ;
            cdsObtenerPendienteFacturacion.FieldByName('ImporteIVA').Value           := f.edImporteIVA.Value;
            cdsObtenerPendienteFacturacion.Post;
            ActualizarBotonRegistrar;
        end;
    end;
    f.Free;
end;

{******************************** Function Header ******************************
Function Name: btnEliminarItemClick
Author       : flamas
Date Created : 24-01-2005
Description  : Elimina un item a facturar
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmReIngresarComprobantes.btnEliminarItemClick(Sender: TObject);
resourcestring
	MSG_ITEM_DELETE_CONFIRMATION = 'Desea eliminar el item ?';
begin
    // Si no hay items no permitir eliminar.
    if cdsObtenerPendienteFacturacion.IsEmpty then Exit;

	if MsgBox(	MSG_ITEM_DELETE_CONFIRMATION, Caption,
    			MB_ICONQUESTION or MB_DEFBUTTON2 or MB_YESNO) = IDYES then begin
		cdsObtenerPendienteFacturacion.Delete;
		ActualizarBotonRegistrar;
    end;
end;

function TfrmReIngresarComprobantes.ValidarCuotasArriendo:boolean;
begin
    Result := True;
    if not ((edCantidadCuotas.ValueInt >= FMinimaCantidadCuotasArriendo) and
        (edCantidadCuotas.ValueInt <= FMaximaCantidadCuotasArriendo)) then
        Result := False;
end;

{******************************** Function Header ******************************
Function Name: btnRegistrarClick
Author       : flamas
Date Created : 24-01-2005
Description  : Registra los movimientos de cuenta, el lote de facturacion y
				genera el comprobante
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TfrmReIngresarComprobantes.btnRegistrarClick(Sender: TObject);
resourcestring
	MSG_INVOICE_NUMBER_ALREADY_EXISTS 	   	= 'El comprobante ingresado ya existe.';
    MSG_INVOICE_GENERATED_OK             	= 'La %s se gener� exitosamente.';
    MSG_INVOICE_NUMBER_MUST_BE_POSITIVE     = 'El N� de la boleta debe ser mayor a cero.';
    MSG_ERROR_GENERAR_COMPROBANTE           = 'Ocurri� un error al generar el comprobante.';
    MSG_ERROR_OBTENER_NUMERO_COMPROBANTE    = 'Ocurri� un error al obtener el identificador para la Boleta.';
	MSG_NUMERO_COMPROBANTE_FUERA_TALONARIO  = 'No se encuentra Talonario asignado para el comprobante ingresado';
    MSG_ERROR_CANTIDAD_CUOTAS = 'La cantidad de cuotas debe estar entre %d y %d';
    MSG_ERROR_CONCEPTO_ARRIENDO_NO_PERMITIDO = 'Solo se puede ingresar un concepto de arriendo en una boleta de arriendo';

var
    NroConvert : Integer;
    Error, Fecha : AnsiString;
begin
    // Si no hay items no permitir registrar.
    if cdsObtenerPendienteFacturacion.IsEmpty then Exit;
	Screen.Cursor := crHourglass;
    try
        //Verificamos si el Numero de boleta es Correcto

        If not TryStrtoInt(edNumeroComprobante.Text,NroConvert) Then Begin
            MsgBoxBalloon(MSG_COMPROBANTE_NO_ENTERO, MSG_CAPTION_COMPROBANTE_INCORRECTO, MB_ICONSTOP,edNumeroComprobante);
            EdFechaEmision.Enabled := False;
            EdFechaEmision.Visible := False;
            Lbl_FechaEmision.Enabled := False;
            Lbl_FechaEmision.Visible := False;
            BtnRegistrar.Enabled := False;
            Exit;
        End;

        If Not ValidarNumeroComprobante (strtoint(Trim(edNumeroComprobante.Text))) Then Begin
                MsgBoxBalloon(MSG_NUMERO_COMPRANTE_INCORRECTO, MSG_CAPTION_COMPROBANTE_INCORRECTO, MB_ICONSTOP, edNumeroComprobante);
                EdFechaEmision.Enabled := False;
                EdFechaEmision.Visible := False;
                Lbl_FechaEmision.Enabled := False;
                Lbl_FechaEmision.Visible := False;
                BtnRegistrar.Enabled := False;
                Exit;
        End;


        //Verificamos si la Fecha de la boleta es Correcta
        Fecha := FormatDateTime ('yyyymmdd', EdFechaEmision.Date);
        If Not ValidarFechaComprobante (strtoint(Trim(edNumeroComprobante.Text)), Fecha, Error) then Begin
            MsgBoxBalloon(MSG_FECHA_COMPROBANTE_INCORRECTA + CRLF + Error, MSG_CAPTION_FECHA_INCORRECTA, MB_ICONSTOP, EdFechaEmision);
            BtnRegistrar.Enabled := False;
            Exit;
        End;

        // Si el comprobante es Boleta, Obtener un NumeroComprobante para la Boleta a generar.
        if ( cbTiposComprobantes.Value = TC_BOLETA ) then begin
            try
                FNumeroComprobanteBoleta := ObtenerNumeroSiguienteComprobante(spCrearComprobante.Connection , FTipoComprobante);
                //con esto nos aseguramos que tenemos base de datos
            except
                on E: Exception do begin
                    MsgBoxErr(MSG_ERROR_OBTENER_NUMERO_COMPROBANTE, E.Message, Caption, MB_ICONERROR);
                    Exit;
                end;
            end;
        end;

        if (cbTiposComprobantes.Text = TC_ARRIENDO) or (cbTiposComprobantes.Text = TC_ARRIENDO_CUOTAS) then begin
            // si es arriendo normal valido la cantidad de cuotas
            if (cbTiposComprobantes.Text = TC_ARRIENDO) then
                if not ValidarCuotasArriendo then begin
                    MsgBoxBalloon(format(MSG_ERROR_CANTIDAD_CUOTAS, 
                        [FMinimaCantidadCuotasArriendo,FMaximaCantidadCuotasArriendo]),Caption, MB_ICONSTOP, edCantidadCuotas);
                    Exit;
                end;
            if not ValidarTelevia then Exit;
        end;

        // Deshabilitar los componentes para que si ocurre un error no
        // permitir modificar la boleta que se intent� emitir.
        FRegistrandoBoletaFiscal := True;
        EnableControlsInContainer(gbDatosConvenio, not FRegistrandoBoletaFiscal);
        EnableControlsInContainer(gbDatosComprobante, not FRegistrandoBoletaFiscal);
        EnableControlsInContainer(gbItemsPendientes, not FRegistrandoBoletaFiscal);

        if not GrabarBoleta then Exit;

        // Habilitar los componentes.
        FRegistrandoBoletaFiscal := False;
        EnableControlsInContainer(gbDatosConvenio, not FRegistrandoBoletaFiscal);
        EnableControlsInContainer(gbDatosComprobante, not FRegistrandoBoletaFiscal);
        EnableControlsInContainer(gbItemsPendientes, not FRegistrandoBoletaFiscal);
        btnAgregarItem.Enabled      := False;
        btnModificarItem.Enabled    := False;
        btnEliminarItem.Enabled     := False;
        edFechaEmision.Visible := False;
        edFechaEmision.Enabled := False;
        lbl_FechaEmision.Enabled := False;
        lbl_FechaEmision.Visible := False;
        ResetearControles;
    finally
    	Screen.Cursor := crDefault;
    end;
end;

{******************************** Function Header ******************************
Procedure Name: AgregarNuevosMovimientosCuenta
Author       : flamas
Date Created : 24-01-2005
Description  : Agrega los Movimientos de cuenta correspondientes a los
				nuevos items cargados
Parameters   :
Return Value : boolean
*******************************************************************************}
procedure TfrmReIngresarComprobantes.AgregarNuevosMovimientosCuenta;
resourcestring
	MSG_ERROR_UPDATING_ITEM = 'Error actualizando Movimientos de Cuenta.';
begin
	with cdsObtenerPendienteFacturacion do begin
    	DisableControls;
        cdsObtenerPendienteFacturacion.Filter := 'Marcado = 1';
        cdsObtenerPendienteFacturacion.Filtered := True;
        try
            First;
            while not EOF do begin
                if ( FieldByName('Nuevo').AsBoolean ) then
                    with spAgregarMovimientoCuentaIVA, Parameters do begin
                        try
                            ParamByName('@CodigoConvenio').Value        := cdsObtenerPendienteFacturacion.FieldByName('CodigoConvenio').AsInteger;
                            ParamByName('@IndiceVehiculo').Value        := NULL;
                            ParamByName('@FechaHora').Value	            := cdsObtenerPendienteFacturacion.FieldByName('FechaHora').AsDateTime;
                            ParamByName('@CodigoConcepto').Value        := cdsObtenerPendienteFacturacion.FieldByName('CodigoConcepto').AsInteger;
                            ParamByName('@Importe').Value               := Abs(cdsObtenerPendienteFacturacion.FieldByName('ImporteCtvs').AsFloat ) * ObtenerSignoMovimientoCuenta * 100;
                            ParamByName('@LoteFacturacion').Value       := NULL;
                            ParamByName('@EsPago').Value                := False;
                            ParamByName('@Observaciones').Value	        := cdsObtenerPendienteFacturacion.FieldByName('Observaciones').AsString;
                            ParamByName('@NumeroPromocion').Value		:= NULL;
                            ParamByName('@NumeroFinanciamiento').Value	:= NULL;
                            ParamByName('@ImporteIVA').Value            := Abs(cdsObtenerPendienteFacturacion.FieldByName('ImporteIVA').AsFloat ) * ObtenerSignoMovimientoCuenta * 100;
                            ParamByName('@PorcentajeIVA').Value         := Abs(cdsObtenerPendienteFacturacion.FieldByName('PorcentajeIVA').AsFloat * 100 ) * ObtenerSignoMovimientoCuenta;
                            ParamByName('@NumeroMovimiento').Value      := NULL;
                            ParamByName('@UsuarioCreacion').Value      := UsuarioSistema;
                            ExecProc;
                            cdsObtenerPendienteFacturacion.Edit;
                            cdsObtenerPendienteFacturacion.FieldByName('NumeroMovimiento').Value := ParamByName('@NumeroMovimiento').Value;
                            cdsObtenerPendienteFacturacion.Post;
                        except
                            on E : Exception do begin
                                raise Exception.Create(MSG_ERROR_UPDATING_ITEM + CRLF + E.Message);
                            end;
                        end; // except
                end; // with
                Next;
            end; // while
        finally
            cdsObtenerPendienteFacturacion.Filtered := False;
            cdsObtenerPendienteFacturacion.Filter := '';
            EnableControls;
        end; // finally
    end; // with
end;

{******************************** Function Header ******************************
Function Name: AgregarLoteFacturacion
Author       : flamas
Date Created : 24-01-2005
Description  : Agrega el lote de facturaci�n
Parameters   :
Return Value : boolean
*******************************************************************************}
procedure TfrmReIngresarComprobantes.AgregarLoteFacturacion(NumeroComprobante: integer);
resourcestring
	MSG_ERROR_GENERATING_INVOICE_BATCH = 'Error creando Lote de Facturaci�n.';
begin
	with spCrearLoteFacturacion, Parameters do begin
    	try
            ParamByName('@CodigoConvenio').Value 			:= FCodigoConvenio;
            ParamByName('@Total').Value 				   	:= Abs(FTotalComprobante * 100) * ObtenerSignoMovimientoCuenta;
            ParamByName('@FechaHora').Value				 	:= StrToDate(DateToStr(NowBase(DMConnections.BaseCAC)));
            ParamByName('@NumeroProcesoFacturacion').Value 	:= NULL;
            ParamByName('@TipoComprobante').Value		   	:= FTipoComprobante;
            ParamByName('@NumeroComprobante').Value		 	:= NumeroComprobante;
            ExecProc;
            FLoteFacturacion := ParamByName('@LoteFacturacion').Value;
        except
        	on E : Exception do begin
                raise Exception.Create(MSG_ERROR_GENERATING_INVOICE_BATCH + CRLF + E.Message);
            end;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: AgregarComprobante
Author       : flamas
Date Created : 24-01-2005
Description  : Agrega la factura
Parameters   :
Return Value : boolean

*******************************************************************************}
function TfrmReIngresarComprobantes.AgregarComprobante: integer;
resourcestring
	MSG_ERROR_GENERATING_INVOICE = 'Error generando el comprobante.';
begin
    try
        spCrearComprobante.Parameters.ParamByName('@TipoComprobante').Value 		:= FTipoComprobante;

        if (FTipoComprobante = TC_BOLETA) then  begin
            spCrearComprobante.Parameters.ParamByName('@NumeroComprobante').Value := FNumeroComprobanteBoleta
            //solo las boletas por ahora usan numeracion no basada en MAX
        end else begin
            spCrearComprobante.Parameters.ParamByName('@NumeroComprobante').Value		:= NULL;
        end;
        spCrearComprobante.Parameters.ParamByName('@NumeroComprobanteFiscal').Value	:= edNumeroComprobante.Text;
        spCrearComprobante.Parameters.ParamByName('@CodigoImpresoraFiscal').Value := NULL;
        if (FTipoComprobante = TC_BOLETA) and FImprimirBoletaEnImpresoraFiscal then begin
            spCrearComprobante.Parameters.ParamByName('@CodigoImpresoraFiscal').Value := FCodigoImpresora;
        end;

        spCrearComprobante.Parameters.ParamByName('@FechaEmision').Value := edFechaEmision.Date;
        spCrearComprobante.Parameters.ParamByName('@FechaVencimiento').Value := NULL;
        spCrearComprobante.Parameters.ParamByName('@CodigoPersona').Value := FCodigoCliente;
        spCrearComprobante.Parameters.ParamByName('@CodigoConvenio').Value := FCodigoConvenio;
        spCrearComprobante.Parameters.ParamByName('@CodigoTipoMedioPago').Value := TPA_NINGUNO;
        spCrearComprobante.Parameters.ParamByName('@TotalComprobante').Value := Abs( FTotalComprobante * 100 );
        spCrearComprobante.Parameters.ParamByName('@TotalAPagar').Value := Abs( FTotalComprobante * 100 );
        spCrearComprobante.Parameters.ParamByName('@AjusteSencilloAnterior').Value := 0;
        spCrearComprobante.Parameters.ParamByName('@AjusteSencilloActual').Value := 0;
        spCrearComprobante.Parameters.ParamByName('@Inserto1').Value := 0;
        spCrearComprobante.Parameters.ParamByName('@Inserto2').Value := 0;
        spCrearComprobante.Parameters.ParamByName('@Inserto3').Value := 0;
        spCrearComprobante.Parameters.ParamByName('@Inserto4').Value := 0;
        // Revision 1 
        spCrearComprobante.Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;
        spCrearComprobante.ExecProc;
        Result := spCrearComprobante.Parameters.ParamByName('@NumeroComprobante').Value;

    except
        on E: Exception do begin
            raise Exception.Create(MSG_ERROR_GENERATING_INVOICE + CRLF + E.Message);
        end;
    end;
end;



{******************************** Function Header ******************************
Function Name: ActualizarLotesFacturacion
Author       : flamas
Date Created : 24-01-2005
Description  : Actualiza los lotes de Facturaci�n en los movimientos de cuentas
Parameters   :
Return Value : boolean

*******************************************************************************}
procedure TfrmReIngresarComprobantes.ActualizarLotesFacturacion;
resourcestring
	MSG_ERROR_UPDATING_INVOICE_BATCH = 'Error actualizando Lote de Facturaci�n';
begin
	with cdsObtenerPendienteFacturacion do begin
    	DisableControls;
        try
            First;
            while not EOF do begin
                if ( FieldByName('Marcado').AsBoolean ) then
                    with spActualizarLoteFacturacion, Parameters do begin
                        try
                        ParamByName('@CodigoConvenio').Value := cdsObtenerPendienteFacturacion.FieldByName('CodigoConvenio').AsInteger;
                        ParamByName('@NumeroMovimiento').Value := cdsObtenerPendienteFacturacion.FieldByName('NumeroMovimiento').AsInteger;
                        ParamByName('@LoteFacturacion').Value := FLoteFacturacion;
                        ParamByName('@UsuarioModificacion').Value := UsuarioSistema;
                        ExecProc;
                        except
                            on E : Exception do begin
                                raise Exception.Create(MSG_ERROR_UPDATING_INVOICE_BATCH + CRLF + E.Message);
                            end;
                        end;
                    end;
                Next;
            end;
        finally
            EnableControls;
        end;
    end;
end;

function TfrmReIngresarComprobantes.ObtenerCotizacionMoneda(const Fecha: TDateTime; const Moneda:String): boolean;
resourcestring
    MSG_ERROR_COTIZACION_INVALIDA = 'La cotizaci�n de %s a la fecha %s est� en cero o no existe. Debe ingresarse desde el m�dulo de Facturaci�n';
var
    CotizacionInt: integer;
begin
    //de la base viene como INT a 4 decimales
    Result := True;
    CotizacionInt := QueryGetValueInt(DMConnections.BaseCAC, format('select dbo.ObtenerCotizacionAFecha(''%s'',''%s'')',[Moneda , FormatDateTime('yyyymmdd',Fecha)]));
    FCotizacion := CotizacionInt / 100 ;
    if FCotizacion = 0  then begin
        MsgBoxErr(format(MSG_ERROR_COTIZACION_INVALIDA, [Moneda, DateToStr(Fecha)]),Caption, Caption, MB_ICONSTOP);
        Result := False ;
    end;
end;

procedure TfrmReIngresarComprobantes.AgregarSoporteInterfaz(Mostrar: Boolean);
begin
    CbSoporte.Visible := Mostrar;
    if Mostrar then begin
        gbDatosComprobante.Height:= 107;
    end else begin
        gbDatosComprobante.Height:= 90;
    end;
    gbDatosCliente.Top := gbDatosComprobante.Height + gbDatosComprobante.Top + 4;
    gbItemsPendientes.Top := gbDatosCliente.Height + gbDatosCliente.Top + 4;
    Label2.Top := gbItemsPendientes.Height + gbItemsPendientes.Top + 2;
    Label2.Top := gbItemsPendientes.Height + gbItemsPendientes.Top + 2;
    lbl_EstadoEmisionBoleta.Top := Label2.Height + Label2.Top + 2;
    NB_Botones.Top := gbItemsPendientes.Height + gbItemsPendientes.Top + 2;
    BtnSalir.Top := gbItemsPendientes.Height + gbItemsPendientes.Top + 3;
end;

{*******************************************************************************
Revision 1
Author: nefernandez
Date: 09/04/2008
Description: Se redondean los valores de importes.
*******************************************************************************}
procedure TfrmReIngresarComprobantes.CargarComprobantesPendientes;
begin
    FTotalComprobante   := 0;

    lbl_Importe.Caption := FormatearImporteConDecimales(DMConnections.BaseCAC,FTotalComprobante, True, True, True);
	cdsObtenerPendienteFacturacion.DisableControls;
    try
        cdsObtenerPendienteFacturacion.EmptyDataSet;
        with spObtenerMovimientosAFacturar do begin
            Close;
            Parameters.ParamByName('@CodigoConvenio').Value := FCodigoConvenio;
            Parameters.ParamByName('@TipoComprobante').Value := cbTiposComprobantes.Value;
            Open;
            while not EOF do begin
                cdsObtenerPendienteFacturacion.Insert;
                cdsObtenerPendienteFacturacion.FieldByName('Marcado').AsBoolean := False;
                cdsObtenerPendienteFacturacion.FieldByName('CodigoConvenio').AsInteger := FieldByName('CodigoConvenio').AsInteger;
                cdsObtenerPendienteFacturacion.FieldByName('NumeroMovimiento').AsInteger := FieldByName('NumeroMovimiento').AsInteger;
                cdsObtenerPendienteFacturacion.FieldByName('FechaHora').AsDateTime := FieldByName('FechaHora').AsDateTime;
                cdsObtenerPendienteFacturacion.FieldByName('CodigoConcepto').AsInteger := FieldByName('CodigoConcepto').AsInteger;
                cdsObtenerPendienteFacturacion.FieldByName('Concepto').AsString := FieldByName('Concepto').AsString;
                cdsObtenerPendienteFacturacion.FieldByName('Observaciones').AsString := FieldByName('Observaciones').AsString;
                cdsObtenerPendienteFacturacion.FieldByName('DescImporte').AsString := FormatearImporteConDecimales(DMConnections.BaseCAC, Round(FieldByName('ImporteCtvs').AsFloat / 100), True, True, True);
                cdsObtenerPendienteFacturacion.FieldByName('ImporteCtvs').AsFloat := Round(Abs( FieldByName('ImporteCtvs').AsFloat / 100 ));
                cdsObtenerPendienteFacturacion.FieldByName('Cantidad').AsFloat := 1;
                cdsObtenerPendienteFacturacion.FieldByName('PorcentajeIVA').AsFloat := FieldByName('PorcentajeIVA').AsInteger / 100; //19
                cdsObtenerPendienteFacturacion.FieldByName('ImporteIVA').AsFloat := Round(Abs(FieldByName('ImporteIVA').AsFloat / 100)) ;
                cdsObtenerPendienteFacturacion.FieldByName('EstaImpreso').AsBoolean := False;
                cdsObtenerPendienteFacturacion.Post;
                Next;
            end;
            Close;
        end;
        cdsObtenerPendienteFacturacionAfterScroll( cdsObtenerPendienteFacturacion);
        //de esta manera definimos que solo se pueden modificar solamente los movimientos que se ingresen como nuevos en este form
//        btnModificarItem.Enabled := not cdsObtenerPendienteFacturacion.IsEmpty;
//        btnEliminarItem.Enabled := not cdsObtenerPendienteFacturacion.IsEmpty;
    finally
        cdsObtenerPendienteFacturacion.EnableControls;
    end; // finally
end;

{******************************** Function Header ******************************
Revision 1
Author: nefernandez
Date: 09/04/2008
Description: Se redondean los valores de importes, pues en ConceptosMovimiento
el Precio puede contener centavos
*******************************************************************************}
Function TfrmReIngresarComprobantes.SetearDatosArriendo: Boolean;
begin
    Result := False;
    cdsObtenerPendienteFacturacion.DisableControls;
    cdsObtenerPendienteFacturacion.EmptyDataSet;
    lblNumeroTelevia.Visible := True ;
    edNumeroTelevia.Visible  := True ;
    lblCuotas.Visible        := True ;
    edCantidadCuotas.Visible := True ;
    if not (CbSoporte.Visible) then edNumeroTelevia.Clear;
    if not (CbSoporte.Visible) then edCantidadCuotas.Clear;

    try
        spObtenerDetalleConceptoMovimiento.Parameters.ParamByName('@CodigoConcepto').Value := FCodigoConceptoArriendo;
        spObtenerDetalleConceptoMovimiento.Open ;
        //if Not ObtenerCotizacionMoneda(NowBase(DMConnections.BaseCAC),
        if Not ObtenerCotizacionMoneda(edFechaEmision.Date,
                spObtenerDetalleConceptoMovimiento.FieldByName('Moneda').AsString ) then Exit;
        cdsObtenerPendienteFacturacion.Append;
        cdsObtenerPendienteFacturacion.FieldByName('CodigoConvenio').AsInteger := FCodigoConvenio;
        cdsObtenerPendienteFacturacion.FieldByName('Nuevo').AsBoolean := True;
        cdsObtenerPendienteFacturacion.FieldByName('FechaHora').AsDateTime := edFechaEmision.Date;
        cdsObtenerPendienteFacturacion.FieldByName('CodigoConcepto').AsInteger := spObtenerDetalleConceptoMovimiento.FieldByName('CodigoConcepto').AsInteger;
        cdsObtenerPendienteFacturacion.FieldByName('Concepto').AsString := spObtenerDetalleConceptoMovimiento.FieldByName('Descripcion').AsString;
        cdsObtenerPendienteFacturacion.FieldByName('Observaciones').AsString := '';
        if (spObtenerDetalleConceptoMovimiento.FieldByName('AfectoIVA').AsBoolean = False) then
            cdsObtenerPendienteFacturacion.FieldByName('ImporteCtvs').AsFloat := Round ( Abs( spObtenerDetalleConceptoMovimiento.FieldByName('Precio').AsFloat / 100 * FCotizacion  ) )
        else begin
            cdsObtenerPendienteFacturacion.FieldByName('ImporteCtvs').AsFloat := Round ( Abs( spObtenerDetalleConceptoMovimiento.FieldByName('Precio').AsFloat / 100 * FCotizacion * ( 1 + ObtenerIVA / 100) ) );
            cdsObtenerPendienteFacturacion.FieldByName('ValorNeto').AsFloat :=   Round ( spObtenerDetalleConceptoMovimiento.FieldByName('Precio').AsFloat / 100 * FCotizacion );
            cdsObtenerPendienteFacturacion.FieldByName('PorcentajeIVA').AsFloat := ObtenerIVA ;
            cdsObtenerPendienteFacturacion.FieldByName('ImporteIVA').AsFloat := Round ( cdsObtenerPendienteFacturacion.FieldByName('ImporteCtvs').AsFloat - cdsObtenerPendienteFacturacion.FieldByName('ValorNeto').AsFloat );
        end;
        cdsObtenerPendienteFacturacion.FieldByName('DescImporte').AsString := FormatearImporteConDecimales(DMConnections.BaseCAC, cdsObtenerPendienteFacturacion.FieldByName('ImporteCtvs').AsFloat, True, True, True);
        cdsObtenerPendienteFacturacion.FieldByName('Cantidad').AsFloat := 1;
        cdsObtenerPendienteFacturacion.FieldByName('EstaImpreso').AsBoolean := False;
        cdsObtenerPendienteFacturacion.FieldByName('Nuevo').AsBoolean := True;
        cdsObtenerPendienteFacturacion.FieldByName('Marcado').AsBoolean := False ; //True;
        spObtenerDetalleConceptoMovimiento.Close ;

        If FIncluirSoporte then begin
            spObtenerDetalleConceptoMovimiento.Parameters.ParamByName('@CodigoConcepto').Value := FCodigoConceptoSoporteTelevia;
            spObtenerDetalleConceptoMovimiento.Open ;
            //if not ObtenerCotizacionMoneda(NowBase(DMConnections.BaseCAC),
            if Not ObtenerCotizacionMoneda(edFechaEmision.Date,
                    spObtenerDetalleConceptoMovimiento.FieldByName('Moneda').AsString ) then Exit;

            cdsObtenerPendienteFacturacion.Append;
            cdsObtenerPendienteFacturacion.FieldByName('CodigoConvenio').AsInteger := FCodigoConvenio;
            cdsObtenerPendienteFacturacion.FieldByName('Nuevo').AsBoolean := True;
            cdsObtenerPendienteFacturacion.FieldByName('FechaHora').AsDateTime := edFechaEmision.Date;
            cdsObtenerPendienteFacturacion.FieldByName('CodigoConcepto').AsInteger := spObtenerDetalleConceptoMovimiento.FieldByName('CodigoConcepto').AsInteger;
            cdsObtenerPendienteFacturacion.FieldByName('Concepto').AsString := spObtenerDetalleConceptoMovimiento.FieldByName('Descripcion').AsString;
            cdsObtenerPendienteFacturacion.FieldByName('Observaciones').AsString := '';

            if (spObtenerDetalleConceptoMovimiento.FieldByName('AfectoIVA').AsBoolean = False) then
                cdsObtenerPendienteFacturacion.FieldByName('ImporteCtvs').AsFloat := Round ( Abs( spObtenerDetalleConceptoMovimiento.FieldByName('Precio').AsFloat / 100 * FCotizacion  ) )
            else begin
                cdsObtenerPendienteFacturacion.FieldByName('ImporteCtvs').AsFloat := Round ( Abs( spObtenerDetalleConceptoMovimiento.FieldByName('Precio').AsFloat / 100 * FCotizacion * ( 1 + ObtenerIVA / 100) ) );
                cdsObtenerPendienteFacturacion.FieldByName('ValorNeto').AsFloat :=   Round ( spObtenerDetalleConceptoMovimiento.FieldByName('Precio').AsFloat / 100 * FCotizacion );
                cdsObtenerPendienteFacturacion.FieldByName('PorcentajeIVA').AsFloat := ObtenerIVA ;
                cdsObtenerPendienteFacturacion.FieldByName('ImporteIVA').AsFloat := Round ( cdsObtenerPendienteFacturacion.FieldByName('ImporteCtvs').AsFloat - cdsObtenerPendienteFacturacion.FieldByName('ValorNeto').AsFloat );
            end;
            cdsObtenerPendienteFacturacion.FieldByName('DescImporte').AsString := FormatearImporteConDecimales(DMConnections.BaseCAC, cdsObtenerPendienteFacturacion.FieldByName('ImporteCtvs').AsFloat, True, True, True);
            cdsObtenerPendienteFacturacion.FieldByName('Cantidad').AsFloat := 1;
            cdsObtenerPendienteFacturacion.FieldByName('EstaImpreso').AsBoolean := False;
            cdsObtenerPendienteFacturacion.FieldByName('Nuevo').AsBoolean := True;
            cdsObtenerPendienteFacturacion.FieldByName('Marcado').AsBoolean := False ;
            spObtenerDetalleConceptoMovimiento.Close ;
        end;
        cdsObtenerPendienteFacturacion.Post;
        //cdsObtenerPendienteFacturacion.EnableControls;
        btnEliminarItem.Enabled := False ;
        btnAgregarItem.Enabled  := False ;
        //cdsObtenerPendienteFacturacionAfterScroll( cdsObtenerPendienteFacturacion);
        ActualizarBotonRegistrar;
        Result := True;
    finally
        cdsObtenerPendienteFacturacion.EnableControls;
        cdsObtenerPendienteFacturacionAfterScroll(cdsObtenerPendienteFacturacion);
    end;
end;

procedure TfrmReIngresarComprobantes.cbTiposComprobantesChange(Sender: TObject);
begin
	btnAgregarItem.Enabled := (cbConveniosCliente.Items.Count > 0) and
    							(cbTiposComprobantes.ItemIndex >= 0);

    edNumeroComprobante.Enabled := True;

    FTipoComprobante := cbTiposComprobantes.Value;
    if FImprimirBoletaEnImpresoraFiscal and (FTipoComprobante = TC_BOLETA ) then begin
        edNumeroComprobante.Clear;
        lbl_NroComprobante.Enabled  := True;
        edNumeroComprobante.Enabled := True;
        lbl_FechaEmision.Enabled    := True;
        edFechaEmision.Enabled      := True;
        edFechaEmision.Date         := NowBase(DMConnections.BaseCAC);
    end
    else begin
        lbl_NroComprobante.Enabled  := True;
        edNumeroComprobante.Enabled := True;
        lbl_FechaEmision.Enabled    := True;
        edFechaEmision.Enabled      := True;
    end;
    //Agrego para que se pueda ingresar una boleta de arriendo
    if cbTiposComprobantes.Text = TC_ARRIENDO then begin
        if not SetearDatosArriendo then begin
            btnRegistrar.Enabled := False;  //Revision 19
            Exit;
        end;
        edCantidadCuotas.Enabled := true;
    end
    else if cbTiposComprobantes.Text = TC_ARRIENDO_CUOTAS then begin
                if not SetearDatosArriendoCuotas then begin
                        btnRegistrar.Enabled := False;  //Revision 19
                        Exit;
                end;
             end else begin
                AgregarSoporteInterfaz (False);
                CbSoporte.Checked := False;
                lblNumeroTelevia.Visible := False ;
                edNumeroTelevia.Visible  := False ;
                lblCuotas.Visible        := False ;
                edCantidadCuotas.Visible := False ;
                CargarComprobantesPendientes;
          end;
end;


procedure TfrmReIngresarComprobantes.cdsObtenerPendienteFacturacionCalcFields(DataSet: TDataSet);
begin
    cdsObtenerPendienteFacturacion.FieldByName('FechaDescrip').AsString := DateToStr(cdsObtenerPendienteFacturacion.FieldByName('FechaHora').AsDateTime)
end;

procedure TfrmReIngresarComprobantes.btnBuscarRUTClick(Sender: TObject);
resourcestring
    MSG_RUT_INVALIDO = 'No se han encontrado datos para ese RUT';
    MSG_DEBE_INGRESAR_RUT =  'Debe ingresar un n�mero de RUT';
var
    CodigoConvenio : Variant;
    indice: integer;
begin
	Screen.Cursor := crHourglass;
    try
        ClearCaptions;
        cbConveniosCliente.Items.Clear;
        for Indice := cbTiposComprobantes.Items.Count - 1 downto 0 do begin
            if cbTiposComprobantes.Items[Indice].Caption = TC_ARRIENDO then
                cbTiposComprobantes.Items.Delete(Indice);
        end;
        cbTiposComprobantesChange(Self);
        if edNumeroTelevia.Visible  then edNumeroTelevia.Text:= '';
        if edCantidadCuotas.Visible then edCantidadCuotas.Clear;
        cbTiposComprobantes.ItemIndex := 0;

        if not ValidateControls([peRUTCliente], [(Trim(peRUTCliente.Text) <> EmptyStr)], caption, [MSG_DEBE_INGRESAR_RUT]) then exit;

        // Obtenemos el cliente seleccionado
        with obtenerCliente do begin
            Close;
            Parameters.ParamByName('@CodigoCliente').Value := NULL;
            Parameters.ParamByName('@CodigoDocumento').Value := TIPO_DOCUMENTO_RUT;
            Parameters.ParamByName('@NumeroDocumento').Value := iif(peRUTCLiente.Text <> EmptyStr,
                                                                PadL(Trim(peRUTCLiente.Text), 9, '0'), EmptyStr);
            Open;
        end;
        // Obtenemos la lista de convenios para el cliente seleccionado
        CodigoConvenio := Null;

        if ObtenerCliente.Eof then begin
            MsgBoxErr(MSG_RUT_INVALIDO,Caption, Caption, MB_ICONSTOP);
            Exit;
        end;

        With spObtenerConveniosCliente do begin
            Parameters.ParamByName('@CodigoCliente').Value := obtenerCliente.fieldbyname('CodigoCliente').AsInteger;
			Parameters.ParamByName('@IncluirConveniosDeBaja').Value := True;
            Open;
            First;
            while not spObtenerConveniosCliente.Eof do begin
                cbConveniosCliente.Items.Add(spObtenerConveniosCliente.fieldbyname('NumeroConvenioFormateado').AsString,
                  trim(spObtenerConveniosCliente.fieldbyname('CodigoConvenio').AsString));
                spObtenerConveniosCliente.Next;
            end;
            Close;
        end;

        // Si no se recuperan convenios es porque ocurri� un inconveniente al
        // retornarlos, ya que un cliente siempre tiene convenios.
        if (CodigoConvenio <> null ) then cbConveniosCliente.Value := CodigoConvenio
        else cbConveniosCliente.ItemIndex := 0;

        cbConveniosClienteChange(cbConveniosCliente);

        with ObtenerCliente do begin
            FCodigoCliente := FieldByName('CodigoCliente').AsInteger;
            lblRUTRUT.Caption := FieldByName('NumeroDocumento').AsString;
            // Mostramos el nombre completo del cliente
            if (FieldByName('Personeria').AsString = PERSONERIA_FISICA) then
                lNombreCompleto.Caption :=  ArmaNombreCompleto(Trim(FieldByName('Apellido').asString),
                  Trim(FieldByName('ApellidoMaterno').asString), Trim(FieldByName('Nombre').asString))
            else
                lNombreCompleto.Caption :=  ArmaRazonSocial(Trim(FieldByName('Apellido').asString),
                  Trim(FieldByName('ApellidoMaterno').asString));

            btnAgregarItem.Enabled := ( cbConveniosCliente.Items.Count > 0 ) and
                                        (cbTiposComprobantes.ItemIndex >= 0 ) and
                                        (cbTiposComprobantes.Text <> TC_ARRIENDO)
                                        and (not FRegistrandoBoletaFiscal);
        end;
    finally
		Screen.Cursor := crDefault;
    end;
end;

{-----------------------------------------------------------------------------
  Procedure: TfrmReIngresarComprobantes.GrabarBoleta
  Author:    FSandi
  Date:      04-Jul-2005
  Arguments: None
  Result:    Boolean
  Description: Graba la Boleta a la Base de Datos
-----------------------------------------------------------------------------}
function TfrmReIngresarComprobantes.GrabarBoleta: Boolean;
begin
    Result := False;
    if not cdsObtenerPendienteFacturacion.IsEmpty then begin
        try
            // Asentar la Boleta en la BD.
            AsentarComprobante;

            // Si se pudo Guardar
            Result := True;

        except
            on E: Exception do begin
                Result := False;
                MsgBoxErr(MSG_ERROR_GENERAL, E.Message, Caption, MB_ICONERROR);
            end;
        end; // except
    end; // if not cdsObtenerPendienteFacturacion.IsEmpty
end; // function TfrmReIngresarComprobantes.GrabarBoleta


{-----------------------------------------------------------------------------
  Procedure: TfrmReIngresarComprobantes.AsentarComprobante
  Author:    ggomez
  Date:      06-Jul-2005
  Arguments: None
  Result:    None
  Description: Realiza el asiento del comprobante en la BD.

  Revision 1:
    Author: ggomez
    Date: 12/08/2006
    Description: Cambi� para que ahora sea un procedure.
-----------------------------------------------------------------------------}
procedure TfrmReIngresarComprobantes.AsentarComprobante;
resourcestring
    MSG_INVOICE_GENERATED_OK    = 'La %s se gener� exitosamente.';
    MSG_INVOICE_NOT_GENERATED   = 'Ha ocurrido un error al generar la %s ';
var
	sDescCompr : AnsiString;
begin
    sDescCompr := DescriTipoComprobante(FTipoComprobante) + Space(1) + edNumeroComprobante.Text;
    try
        GuardarComprobante;
        Screen.Cursor := crDefault;
        MsgBox(Format(MSG_INVOICE_GENERATED_OK, [sDescCompr]), Caption, MB_ICONINFORMATION);
    except
        on E: Exception do begin
            // Crear una excepci�n para que la resuelva quien llama a este m�todo.
            raise Exception.Create(Format(MSG_INVOICE_NOT_GENERATED, [sDescCompr]) + CRLF + E.Message);
        end;
    end;
end;


{******************************** Function Header ******************************
Function Name: ObtenerCodigoImpresora
Author : mvitali
Date Created : 22/09/2005
Description : Dado un numero de serie de impresora la busca en la nomina de
    impresoras y devuelve el codigo de la misma, en caso de no encontrarla
    devuelve -1
Parameters : NumeroDeSerie: String
Return Value : integer
*******************************************************************************}
function TfrmReIngresarComprobantes.ObtenerCodigoImpresora(
  NumeroDeSerie: String): Integer;
resourcestring
    MSG_ERROR_OBTENIENDO_CODIGO_IMPRESORA = 'Error al obtener el Codigo de la Impresora.';
var
    sp: TADOStoredProc;
    Codigo: Integer;
    Descripcion: AnsiString;
begin
    Codigo := -1;
    Descripcion := EmptyStr;
    sp := TADOStoredProc.Create(nil);
    try
        try
            sp.Connection := DMConnections.BaseCAC;
            sp.ProcedureName := 'ObtenerCodigoImpresora';
            sp.Parameters.Refresh;
            sp.Parameters.ParamByName('@NumeroSerie').Value := NumeroDeSerie;
            sp.Parameters.ParamByName('@Codigo').Value := Codigo;
            sp.Parameters.ParamByName('@Descripcion').Value := Descripcion;
            sp.ExecProc;
            if (sp.Parameters.ParamByName('@Codigo').Value <> Null) then begin
                Result := sp.Parameters.ParamByName('@Codigo').Value
            end else begin
                Result := -1;
            end;
        except
            on E: Exception do begin
                MsgBoxErr(MSG_ERROR_AL_OBTENER_NRO_SERIE, e.Message, Caption, MB_ICONERROR);
                Result := -1;
            end;
        end;
    finally
        sp.Free;
    end;
end;

procedure TfrmReIngresarComprobantes.AgregarCuotasComprobanteArriendo;
begin
        if (cbTiposComprobantes.Text = TC_ARRIENDO) or (cbTiposComprobantes.Text = TC_ARRIENDO_CUOTAS) then begin
            //creamos el comprobante de deuda y la primera cuota
            spCrearComprobanteCuotas.Parameters.ParamByName('@TipoComprobante'      ).Value := TC_BOLETA;
            spCrearComprobanteCuotas.Parameters.ParamByName('@NumeroComprobante'    ).Value := spCrearComprobante.Parameters.ParamByName('@NumeroComprobante').Value;
            spCrearComprobanteCuotas.Parameters.ParamByName('@CantidadCuotasTotales').Value := edCantidadCuotas.ValueInt;
            spCrearComprobanteCuotas.Parameters.ParamByName('@ContractSerialNumber' ).Value := EtiquetaToSerialNumber(edNumeroTelevia.Text);
            spCrearComprobanteCuotas.Parameters.ParamByName('@UsuarioCreacion'      ).Value := UsuarioSistema;
            spCrearComprobanteCuotas.Parameters.ParamByName('@FechaEmision'         ).Value := edFechaEmision.Date;
            spCrearComprobanteCuotas.ExecProc;
        end;
end;

{******************************** Function Header ******************************
Function Name: GuardarComprobante
Author : mlopez
Date Created : 11/08/2006
Description : Genera Movimientos Cuenta, Lote Facturacion, Guarda el comprobante
              y por ultimo actualiza el Lote de Facturacion recien creado

Revision: 1
Author: nefernandez
Date: 01/10/2007
Description: SS 611: Se agrega la llamada al store ReaplicarConvenio luego de
generar todos los comprobantes
*******************************************************************************}
procedure TfrmReIngresarComprobantes.GuardarComprobante;
begin
    DMConnections.BaseCAC.BeginTrans;
    try
        AgregarNuevosMovimientosCuenta;
        AgregarLoteFacturacion(AgregarComprobante);
        ActualizarLotesFacturacion;
        AgregarCuotasComprobanteArriendo;
        //Revision 1
        spReaplicarConvenio.Parameters.ParamByName('@CodigoConvenio').Value := FCodigoConvenio;
        spReaplicarConvenio.Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;
        spReaplicarConvenio.ExecProc;

        DMConnections.BaseCAC.CommitTrans;
    except
        on E: Exception do begin
            DMConnections.BaseCAC.RollbackTrans;
            // Subir la excepci�n para que la resuelva quien llama a este m�todo.
            raise;
        end;
    end;
end;


{******************************** Function Header ******************************
Function Name: ResetearControles
Author : mlopez
Date Created : 14/08/2006
Description : Reinicia los componentes y variables en la ventana
*******************************************************************************}
procedure TfrmReIngresarComprobantes.ResetearControles;
begin
    // Se resetean variables
    FCodigoCliente := -1;
    FCodigoConvenio := -1;
    FNroBoletaFiscal := -1;
    FNumeroComprobanteBoleta := -1;
    FRegistrandoBoletaFiscal := False;
    ClearCaptions;
    // Se inicializan los controles
    peRUTCliente.Clear;
    cbConveniosCliente.Clear;
    cbTiposComprobantes.Clear;
	CargarTiposComprobantes;
    edFechaEmision.Date := StrToDate(DateToStr(NowBase( DMConnections.BaseCAC )));
    edFechaEmision.Visible := False;
    edFechaEmision.Enabled := False;
    lbl_FechaEmision.Enabled := False;
    lbl_FechaEmision.Visible := False;
    edNumeroComprobante.Clear;
    cdsObtenerPendienteFacturacion.EmptyDataSet;
    peRUTCliente.SetFocus;
    lbl_Importe.Caption := '0';
    btnAgregarItem.Enabled      := False;
    btnModificarItem.Enabled    := False;
    btnEliminarItem.Enabled     := False;
    //Borro todo lo que tenga que ver con arriendo
    edNumeroTelevia.Visible := False;
    lblNumeroTelevia.Visible := False;
    lblCuotas.Visible := False;
    edCantidadCuotas.Visible := False;
    cbSoporte.Visible := False;
end;


{******************************** Function Header ******************************
Function Name: ObtenerIVA
Author : jconcheyro
Date Created : 11/09/2006
Description :
Parameters : None
Return Value : devuelve el porcentaje actual de IVA
*******************************************************************************}
function TfrmReIngresarComprobantes.ObtenerIVA: Double;
begin
    //se usa now porque la emision de los creditos en la base es getdate
    Result := QueryGetValueInt(DMConnections.BaseCAC,
                                'SELECT dbo.ObtenerIVA(''' + FormatDateTime('yyyymmdd',
                                NowBase(spCrearComprobante.Connection)) + ''')');
    Result := Result / 100;       // esto devuelve un 19 porque en la base dice 1900

end;

{******************************** Function Header ******************************
Author: rharris
Date: 26/12/2008
Description: Se agrega funcion para el arriendo de televias en cuotas hasta abril 2008
*******************************************************************************}
function TfrmREIngresarComprobantes.SetearDatosArriendoCuotas;
begin
    Result := False;
    cdsObtenerPendienteFacturacion.DisableControls;
    cdsObtenerPendienteFacturacion.EmptyDataSet;
    lblNumeroTelevia.Visible := True ;
    edNumeroTelevia.Visible  := True ;
    lblCuotas.Visible        := True ;
    edCantidadCuotas.Visible := True ;
    if not (CbSoporte.Visible) then edNumeroTelevia.Clear;
    if not (CbSoporte.Visible) then edCantidadCuotas.Clear;
    // determino la cantidad de cuotas a generar al 01-05-2009
    edCantidadCuotas.ValueInt := QueryGetValueInt( DMConnections.BaseCAC, 'SELECT DATEDIFF(Month, ' + QuotedStr(ConvertirEnFechaParaScript(edFechaEmision.Date, False))+ ' ,' + QuotedStr('20090701') +')');
    edCantidadCuotas.Enabled := False;
    FTipoEnvioDocumento := QueryGetValueInt(DMConnections.BaseCAC,format('SELECT dbo.ObtenerMedioEnvioDocumento(%d)',[FCodigoConvenio]));
    // Determino el codigoconcepto a utilizar dependiendo del tipo medio envio del convenio
    if FTipoEnvioDocumento = 1 then
        spObtenerDetalleConceptoMovimiento.Parameters.ParamByName('@CodigoConcepto').Value := FCodigoConceptoArriendoCuotasPostal
    else
        spObtenerDetalleConceptoMovimiento.Parameters.ParamByName('@CodigoConcepto').Value := FCodigoConceptoArriendoCuotasMail;
    spObtenerDetalleConceptoMovimiento.Open;
    if not ObtenerCotizacionMoneda(NowBase(DMConnections.BaseCAC),
            spObtenerDetalleConceptoMovimiento.FieldByName('Moneda').AsString ) then Exit;
    cdsObtenerPendienteFacturacion.Append;
    cdsObtenerPendienteFacturacion.FieldByName('CodigoConvenio').AsInteger := FCodigoConvenio;
    cdsObtenerPendienteFacturacion.FieldByName('Nuevo').AsBoolean := True;
    cdsObtenerPendienteFacturacion.FieldByName('FechaHora').AsDateTime := edFechaEmision.Date;
    cdsObtenerPendienteFacturacion.FieldByName('CodigoConcepto').AsInteger := 
    spObtenerDetalleConceptoMovimiento.FieldByName('CodigoConcepto').AsInteger;
    cdsObtenerPendienteFacturacion.FieldByName('Concepto').AsString := 
    spObtenerDetalleConceptoMovimiento.FieldByName('Descripcion').AsString;
    cdsObtenerPendienteFacturacion.FieldByName('Observaciones').AsString := '';

    if (spObtenerDetalleConceptoMovimiento.FieldByName('AfectoIVA').AsBoolean = False) then
        cdsObtenerPendienteFacturacion.FieldByName('ImporteCtvs').AsFloat := edCantidadCuotas.ValueInt * (Round( Abs( spObtenerDetalleConceptoMovimiento.FieldByName('Precio').AsFloat / 100 * FCotizacion)))
    else begin
        cdsObtenerPendienteFacturacion.FieldByName('ImporteCtvs').AsFloat := edCantidadCuotas.ValueInt * (Round ( Abs( spObtenerDetalleConceptoMovimiento.FieldByName('Precio').AsFloat / 100 * FCotizacion * ( 1 + ObtenerIVA / 100))));
        cdsObtenerPendienteFacturacion.FieldByName('ValorNeto').AsFloat :=   edCantidadCuotas.ValueInt * (Round ( spObtenerDetalleConceptoMovimiento.FieldByName('Precio').AsFloat / 100 * FCotizacion));
        cdsObtenerPendienteFacturacion.FieldByName('PorcentajeIVA').AsFloat := ObtenerIVA ;
        cdsObtenerPendienteFacturacion.FieldByName('ImporteIVA').AsFloat := Round ( 
        cdsObtenerPendienteFacturacion.FieldByName('ImporteCtvs').AsFloat - 
        cdsObtenerPendienteFacturacion.FieldByName('ValorNeto').AsFloat  );
    end;
    cdsObtenerPendienteFacturacion.FieldByName('DescImporte').AsString := FormatearImporteConDecimales(DMConnections.BaseCAC, 
    cdsObtenerPendienteFacturacion.FieldByName('ImporteCtvs').AsFloat, True, True, True);
    cdsObtenerPendienteFacturacion.FieldByName('Cantidad').AsFloat := 1;
    cdsObtenerPendienteFacturacion.FieldByName('EstaImpreso').AsBoolean := False;
    cdsObtenerPendienteFacturacion.FieldByName('Nuevo').AsBoolean := True;
    cdsObtenerPendienteFacturacion.FieldByName('Marcado').AsBoolean := False ; //True;
    spObtenerDetalleConceptoMovimiento.Close ;
    cdsObtenerPendienteFacturacion.Post;
    cdsObtenerPendienteFacturacion.EnableControls;
    btnEliminarItem.Enabled := False ;
    btnAgregarItem.Enabled  := False ;
    cdsObtenerPendienteFacturacionAfterScroll( cdsObtenerPendienteFacturacion);
    ActualizarBotonRegistrar;
    Result := True;
end;

end.

