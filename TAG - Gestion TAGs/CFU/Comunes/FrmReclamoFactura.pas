{-------------------------------------------------------------------------------
 File Name: FrmReclamoFactura.pas
 Author: DDeMarco
 Date Created:
 Language: ES-AR
 Description: Ingreso y edicion de reclamos
 
 Revision		: 1
 Author			: Nelson Droguett Sierra
 Date			: 29-Junio-2010
 Description	: (Ref. Fase 2) Se agrega concesionaria y SubTipoReclamo

 Firma        : SS-1006-NDR-20120802
 Description  : Validar concesionaria en reclamos
-------------------------------------------------------------------------------}
unit FrmReclamoFactura;

interface

uses
  //Reclamo
  DMConnection,                 //Coneccion a base de datos OP_CAC
  util,                         //Nulldate
  UtilDB,                       //Rutinas para base de datos
  UtilProc,                     //Mensajes
  RStrings,                     //Resource Strings
  FreContactoReclamo,           //Frame contacto
  FreCompromisoOrdenServicio,   //Frame compromiso
  FreSolucionOrdenServicio,     //Frame solucion
  FreHistoriaOrdenServicio,     //Frame Historia
  FreFuenteReclamoOrdenServicio,//Frame Fuente Reclamo
  FreNumeroOrdenServicio,       //Frame Numero Orden Servicio
  FreRespuestaOrdenServicio,    //Frame Respuesta Orden Servicio  
  OrdenesServicio,              //Registra la orden de  servicio
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, DB, ADODB, ExtCtrls, VariantComboBox, Validate,
  DateEdit, Grids, DBGrids, DmiCtrls, FreSubTipoReclamoOrdenServicio,
  FreConcesionariaReclamoOrdenServicio;

type
  TFormReclamoFactura = class(TForm)
    PageControl: TPageControl;
    TabSheetDatos: TTabSheet;
    Lcomprobante: TLabel;
    txtComprobante: TEdit;
    Panel1: TPanel;
    txtDetalle: TMemo;
    Panel3: TPanel;
    Label3: TLabel;
    pnlPago: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    txtFechaPago: TDateEdit;
    txtLugarPago: TEdit;
    pnlReclamoContenido: TPanel;
    Label1: TLabel;
    cbSubtipoReclamo: TVariantComboBox;
    TabSheetProgreso: TTabSheet;
    TabSheetHistoria: TTabSheet;
    FrameHistoriaOrdenServicio: TFrameHistoriaOrdenServicio;
    PnlNumeroRecibo: TPanel;
    LNumeroRecibo: TLabel;
    ENumeroRecibo: TNumericEdit;
    PResolucion: TPanel;
    ckDireccionIncorrecta: TCheckBox;
    Eresolucion: TVariantComboBox;
    LresoluciondelCaso: TLabel;
    CkRecibioNC: TCheckBox;
    spObtenerOrdenServicioFacturacion: TADOStoredProc;
    PAbajo: TPanel;
    PDerecha: TPanel;
    CKRetenerOrden: TCheckBox;
    AceptarBTN: TButton;
    btnCancelar: TButton;
    FrameCompromisoOrdenServicio1: TFrameCompromisoOrdenServicio;
    FrameContactoReclamo1: TFrameContactoReclamo;
    FrameSolucionOrdenServicio1: TFrameSolucionOrdenServicio;
    Ldetalledelprogreso: TLabel;
    txtDetalleSolucion: TMemo;
    Pdivisor: TPanel;
    Pdivisor2: TPanel;
    FrameFuenteReclamoOrdenServicio1: TFrameFuenteReclamoOrdenServicio;
    FrameNumeroOrdenServicio1: TFrameNumeroOrdenServicio;
    TabSheetRespuesta: TTabSheet;
    FrameRespuestaOrdenServicio1: TFrameRespuestaOrdenServicio;
    FrameConcesionariaReclamoOrdenServicio1: TFrameConcesionariaReclamoOrdenServicio;
    FrameSubTipoReclamoOrdenServicio1: TFrameSubTipoReclamoOrdenServicio;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure AceptarBTNClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
  private
    { Private declarations }
    FNumeroComprobante : Int64;
    FTipoOrdenServicio : Integer;
    FTipoComprobante : AnsiString;
    FCodigoOrdenServicio : Integer;
    FEditando:boolean; //Indica si estoy editando o no
    Function GetCodigoOrdenServicio: Integer;
  public
    { Public declarations }
    Function Inicializar(TipoOrdenServicio : Integer ; TipoComprobante : AnsiString ; NumeroComprobante : Int64) : Boolean;
    Function InicializarEditando(CodigoOrdenServicio : Integer) : Boolean;
    Function InicializarSolucionando(CodigoOrdenServicio : Integer) : Boolean;
    Property CodigoOrdenServicio: Integer Read GetCodigoOrdenServicio;
  end;

var
  FormReclamoFactura: TFormReclamoFactura;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:   DDeMarco
  Date Created:
  Description:  Inicializaci�n de este formulario
  Parameters: TipoOrdenServicio: Integer; TipoComprobante: AnsiString; NumeroComprobante: Int64
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFormReclamoFactura.Inicializar(TipoOrdenServicio: Integer; TipoComprobante: AnsiString; NumeroComprobante: Int64) : Boolean;
Resourcestring
    MSG_ERROR_INIT =  'Error al inicializar Reclamo sobre Comprobante / Pagos';
    MSG_ERROR = 'Error';
Var
    Sz : TSize;
begin
    FTipoOrdenServicio := TipoOrdenServicio;
    FTipoComprobante := TipoComprobante;
    FNumeroComprobante := NumeroComprobante;
    PageControl.ActivePageIndex := 0;
    try
        //Centro el form
        CenterForm(Self);
        //ajusto el form al tama�o disponible
        SZ := GetFormClientSize(Application.MainForm);
  	    SetBounds(0, 0, SZ.cx, sz.cy);
        //Titulo
        Caption := QueryGetValue(DMConnections.BaseCAC, Format('select dbo.ObtenerDescripcionTipoOrdenServicio (%d)',[TipoOrdenServicio]));
        //Tipo y numero de comprobante
        TxtComprobante.Text := TipoComprobante + ' ' + IntToStr(NumeroComprobante) + ' - $' +QueryGetValue(DMConnections.BaseCAC, Format('SELECT TotalAPagar / 100 FROM Comprobantes WHERE TipoComprobante = ''%s'' AND NumeroComprobante = %d',[TipoComprobante, NumeroComprobante]));
        //Codigo de convenio
        FrameContactoReclamo1.CodigoConvenio := QueryGetValueInt(DMConnections.BaseCAC, Format('select dbo.ObtenerCodigoConvenioComprobantes (''%s'',%d)',[TipoComprobante, NumeroComprobante]));
        case TipoOrdenServicio of
            204: //Cobros efectuados (reposici�n de Telev�a, intereses, pago incorrecto, etc.)
                begin
                    pnlReclamoContenido.Visible := True;
                    pnlReclamoContenido.Top := 0;
                    cbSubtipoReclamo.ItemIndex := 0;
                    ActiveControl := cbSubtipoReclamo;
                end;
            206, 214, 215, 216:
                begin
                    ActiveControl := txtDetalle;
                end;
            205: //Comprobantes no recibidos
                begin
                    PResolucion.Visible := True;
                    ActiveControl := TxtDetalle;
                end;
            208:   //Pagos (no aplicado, doble pago, pago incorrecto de su cuenta, etc.)//
                begin
                    PnlPago.Visible := True;
                    PnlPago.Top := 0;
                    TxtFechaPago.Date := Date - 5;
                    ActiveControl := txtFechaPago;
                    PnlNumeroRecibo.Visible := True;
                end;
        end;

        Result := FrameContactoReclamo1.Inicializar and
                        FrameCompromisoOrdenServicio1.Inicializar(5) and
                                    FrameSolucionOrdenServicio1.Inicializar and
                                        FrameRespuestaOrdenServicio1.Inicializar and
                                        //Rev.1 / 29-Junio-2010 / Nelson Droguett Sierra-----------------
                                        	FrameConcesionariaReclamoOrdenServicio1.Inicializar and
                                            	FrameSubTipoReclamoOrdenServicio1.Inicializar;
                                        //---------------------------------------------------------------


        if Result then FrameContactoReclamo1.CodigoConvenio := QueryGetValueInt(DMConnections.BaseCAC, Format('SELECT dbo.ObtenerCodigoConvenioComprobantes (''%s'',%d)',[TipoComprobante, NumeroComprobante]));
    except
        on E: Exception do begin
            MsgBoxErr(MSG_ERROR_INIT, E.Message, MSG_ERROR, MB_ICONSTOP);
            Result := False;
        end;
    end;

    //termino de inicializar
    FrameFuenteReclamoOrdenServicio1.Inicializar;
    FrameNumeroOrdenServicio1.Visible := False;
    FEditando := False; //No estoy Editando
    EResolucion.Value := 0; //Ninguna
    //Pongo una fecha comprometida por defecto en funcion del reclamo
    if FrameCompromisoOrdenServicio1.FechaCompromiso = NULLDATE then FrameCompromisoOrdenServicio1.FechaCompromiso:=ObtenerFechaComprometidaOS(now, FTipoOrdenServicio, FrameFuenteReclamoOrdenServicio1.FuenteReclamo, FrameCompromisoOrdenServicio1.Prioridad);
    AddOSForm(Self);
end;

{-----------------------------------------------------------------------------
  Function Name: InicializarEditando
  Author:   DDeMarco
  Date Created:
  Description:  Modifico Reclamo comprobante que ya existe
  Parameters: CodigoOrdenServicio: Integer
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFormReclamoFactura.InicializarEditando(CodigoOrdenServicio: Integer): Boolean;

    //form read Only
    Procedure ReadOnly;
    begin
        PnlPago.Enabled := False;
        PnlReclamoContenido.Enabled := False;
        //comunes
        FrameContactoReclamo1.Deshabilitar;
        FrameCompromisoOrdenServicio1.Enabled := False;
        FrameSolucionordenServicio1.Enabled := False;
        FrameRespuestaOrdenServicio1.Deshabilitar;
        TxtDetalle.ReadOnly := True;
        TxtDetalleSolucion.ReadOnly := True;
        CKRetenerOrden.Visible := False;
        BtnCancelar.Caption := 'Salir';
        AceptarBtn.Visible := False;
    end;
var
    TipoOrdenServicio : Integer;
    TipoComprobante : String;
    NumeroComprobante : Integer;
    //
    Estado: String;
begin

    FCodigoOrdenServicio := CodigoOrdenServicio;
    spObtenerOrdenServicioFacturacion.Parameters.Refresh;
    spObtenerOrdenServicioFacturacion.Parameters.ParamByName('@CodigoOrdenServicio').Value := CodigoOrdenServicio;
    Result := OpenTables([spObtenerOrdenServicioFacturacion]);
    if not Result then Exit;

    //asigno los valores
    TipoOrdenServicio := spObtenerOrdenServicioFacturacion.FieldByName('TipoOrdenServicio').AsInteger;
    TipoComprobante := spObtenerOrdenServicioFacturacion.FieldByName('TipoComprobante').AsString;
    NumeroComprobante := spObtenerOrdenServicioFacturacion.FieldByName('NumeroComprobante').AsInteger;

    // Cargamos los datos
    Result := Inicializar(  TipoOrdenServicio,
                            TipoComprobante,
                            NumeroComprobante
                         );

    FrameCompromisoOrdenServicio1.LoadFromDataset(spObtenerOrdenServicioFacturacion);
    FrameContactoReclamo1.LoadFromDataset(spObtenerOrdenServicioFacturacion);
    FrameSolucionOrdenServicio1.LoadFromDataset(spObtenerOrdenServicioFacturacion);
    FrameRespuestaOrdenServicio1.LoadFromDataset(spObtenerOrdenServicioFacturacion);

    TxtDetalle.Text := SpObtenerOrdenServicioFacturacion.FieldByName('ObservacionesSolicitante').AsString;
    TxtDetalleSolucion.Text := SpObtenerOrdenServicioFacturacion.FieldByName('ObservacionesEjecutante').AsString;

    if not spObtenerOrdenServicioFacturacion.FieldByName('FechaPago').IsNull then
    TxtFechaPago.Date := SpObtenerOrdenServicioFacturacion.FieldByName('FechaPago').AsDateTime;
    TxtLugarPago.Text := SpObtenerOrdenServicioFacturacion.FieldByName('LugarPago').AsString;

    //obtengo el numero de recibo
    ENumeroRecibo.ValueInt := SpObtenerOrdenServicioFacturacion.FieldByName('Numerorecibo').Asinteger;

    //obtengo la historia para esa orden de servicio
    FrameHistoriaOrdenServicio.Inicializar(CodigoOrdenServicio);

    //Obtengo CodigoResolucion, RecibioComprobante, DireccionIncorrecta
    EResolucion.Value := SpObtenerOrdenServicioFacturacion.FieldByName('CodigoResolucion').AsString;
    CkRecibioNc.Checked := SpObtenerOrdenServicioFacturacion.FieldByName('RecibioComprobante').AsBoolean;
    CkDireccionIncorrecta.Checked := SpObtenerOrdenServicioFacturacion.FieldByName('direccionincorrecta').Asboolean;

    //obtengo la fuente del reclamo
    FrameFuenteReclamoOrdenServicio1.LoadFromDataset(spObtenerOrdenServicioFacturacion);

    //Obtengo el Estado de la OS para saber si permito editar o read only
    Estado := SpObtenerOrdenServicioFacturacion.FieldByName('Estado').AsString;
    //si esta finalizada solo se puede leer
    if ((Estado = 'C') or (Estado = 'T')) then begin
        ReadOnly;
    end;

    // Listo
    spObtenerOrdenServicioFacturacion.Close;

    //termino de inicializar
    FrameNumeroOrdenServicio1.Visible := True;
    FrameNumeroOrdenServicio1.Inicializar(IntToStr(CodigoOrdenServicio));
    CKRetenerOrden.Checked := False; //por Default no retengo la orden de servicio
    //la fuente del reclamo no puede ser cambiada despues de creada la OS
    FrameFuenteReclamoOrdenServicio1.Deshabilitar;
    FEditando := True; //Estoy Editando
end;

{-----------------------------------------------------------------------------
  Function Name: InicializarSolucionando
  Author:   DDeMarco
  Date Created:
  Description:  Soluciono un Reclamo Comprobante
  Parameters: CodigoOrdenServicio: Integer
  Return Value: Boolean
-----------------------------------------------------------------------------}
Function TFormReclamoFactura.InicializarSolucionando(CodigoOrdenServicio: Integer): Boolean;
begin
    Result := InicializarEditando(CodigoOrdenServicio);
    if Result then begin
        PageControl.ActivePageIndex := 1;
        FrameSolucionOrdenServicio1.Estado := 'T';
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: GetCodigoOrdenServicio
  Author:    lgisuk
  Date Created: 11/04/2005
  Description: obtengo el codigoordenservicio
  Parameters: None
  Return Value: integer
-----------------------------------------------------------------------------}
Function TFormReclamoFactura.GetCodigoOrdenServicio: integer;
begin
    try
        Result := StrToInt(FrameNumeroOrdenServicio1.CodigoOrdenServicio);
    except
        Result := 0;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: FormKeyPress
  Author:    DDeMarco
  Date Created:
  Description: si tocan escape permito cancelar
  Parameters: Sender: TObject; var Key: Char
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoFactura.FormKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #27 then btnCancelar.Click;
end;

{-----------------------------------------------------------------------------
  Function Name: AceptarBTNClick
  Author:    DDeMarco
  Date Created:
  Description:  acepta el reclamo
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoFactura.AceptarBTNClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: Validar
      Author:    lgisuk
      Date Created: 16/06/2005
      Description:  valido el contenido del reclamo antes de guardarlo
      Parameters: None
      Return Value: Boolean
    -----------------------------------------------------------------------------}
    Function Validar: Boolean;
    Resourcestring
        MSG_ERROR_DET    = 'Debe indicar alg�n detalle para el reclamo';
        MSG_ERROR_FUENTE = 'Debe indicar la fuente del reclamo';
        MSG_ERROR_FECHA  = 'La Fecha de Compromiso Interna o Externa '+ CRLF +
                           'debe ser menor a la Fecha Comprometida con el Cliente';
    Begin
        Result := False;

        //Validamos fuente de reclamo
        if FEditando = False then begin
            if not FrameFuenteReclamoOrdenServicio1.Validar then Exit;
        end;

        //si es un reclamo que va a ser cancelado, no valida el resto de los
        //campos para no forzar al usuario a que los complete y asi pueda
        //salir rapidamente.
        if FrameSolucionOrdenServicio1.Estado = 'C' then begin
            Result := True;
            Exit;
        end;

        //Validamos identificacion de usuario y contacto
        if not FrameContactoReclamo1.Validar then Exit;

        //Validamos compromiso con el cliente
        if FEditando = False then begin
            if not FrameCompromisoOrdenServicio1.Validar then Exit;
        end;

        //Validamos detalle del reclamo
        if Trim(txtDetalle.Text) = '' then begin
            PageControl.ActivePageIndex := 0;
            MsgBoxBalloon(MSG_ERROR_DET, STR_ERROR, MB_ICONSTOP, txtDetalle);
            TxtDetalle.SetFocus;
            Exit;
        end;

        //Validamos progreso / solucion
        if not FrameSolucionOrdenServicio1.Validar then Exit;
        //Si es un reclamo pendiente de respuesta Interna o Externa
        if ((FrameSolucionOrdenServicio1.Estado = 'I') or (FrameSolucionOrdenServicio1.Estado = 'E')) then begin
            //Valido que la fecha comprometida interna/externa
            //sea menor a la fecha comprometida con el cliente
            if (FrameCompromisoOrdenServicio1.txtFechaCompromiso.Date < FrameSolucionOrdenServicio1.EFechaCompromiso.Date) then begin
                MsgBoxBalloon(MSG_ERROR_FECHA, STR_ERROR, MB_ICONSTOP, FrameSolucionOrdenServicio1.EFechaCompromiso);
                FrameSolucionOrdenServicio1.EFechaCompromiso.SetFocus;
                Exit;
            end;
        end;

        if not FrameConcesionariaReclamoOrdenServicio1.Validar then Exit ;      //SS-1006-NDR-20120802

        Result := True;
    end;

Var
    OS: Integer;
begin

    //Valido el reclamo antes de guardarlo
    if not Validar then exit;

    // Guardamos
    //Rev.1 / 29-Junio-2010 / Nelson Droguett Sierra -------------------------------------------------------
    DMConnections.BaseCAC.BeginTrans;
    OS := GuardarOrdenServicioBase(FCodigoOrdenServicio,
                                   FTipoOrdenServicio,
                                   FrameFuenteReclamoOrdenServicio1.FuenteReclamo,
                                   FrameContactoReclamo1,
                                   FrameCompromisoOrdenServicio1,
                                   FrameSolucionOrdenServicio1,
                                   txtDetalle.Text,
                                   txtDetalleSolucion.text,
                                   FrameRespuestaOrdenServicio1.ObservacionesRespuesta,
                                   FrameRespuestaOrdenServicio1.ObservacionesComentariosCliente,
                                   FrameRespuestaOrdenServicio1.Notificado,
                                   FrameRespuestaOrdenServicio1.Conforme,
                                   FrameConcesionariaReclamoOrdenServicio1,
                                   FrameSubTipoReclamoOrdenServicio1
                                   );
    //FinRev.1-------------------------------------------------------------------------

    if OS <= 0 then begin
        DMConnections.BaseCAC.RollbackTrans;
        Exit;
    end;

    // Especifico de facturacion
    if not GuardarOrdenServicioFacturacion(OS, FTipoComprobante,FNumeroComprobante, txtFechaPago.Date, txtLugarPago.Text, cbSubtipoReclamo.Value, enumerorecibo.ValueInt,eresolucion.Value, ckrecibionc.Checked, ckdireccionincorrecta.Checked) then begin
        DMConnections.BaseCAC.RollbackTrans;
        Exit;
    end;

    // Listo
    DMConnections.BaseCAC.CommitTrans;

    //si es un reclamo nuevo
    if FEditando = False then begin
        //informo al operador el numero de Reclamo que se genero
        InformarNumeroReclamo(inttostr(OS));
        DesbloquearOrdenServicio(OS);
    end;

     close;
end;

{-----------------------------------------------------------------------------
  Function Name: btnCancelarClick
  Author:    lgisuk
  Date Created: 11/04/2005
  Description:     cierro el formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoFactura.btnCancelarClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormDestroy
  Author:    lgisuk
  Date Created: 11/04/2005
  Description:  remuevo al form de la lista de creados
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoFactura.FormDestroy(Sender: TObject);
begin
    RemoveOSform(Self);
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 05/04/2005
  Description:  desbloqueo la orden de servicio antes de salir
                y lo libero de memoria
  Parameters: Sender: TObject;var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoFactura.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    //si se esta modificando un reclamo
    if FEditando = True then begin
        //verifico si desea desbloquear el reclamo
        if CKRetenerOrden.Checked = False then begin
            //Desbloqueo la orden de servicio
            DesbloquearOrdenServicio(FCodigoOrdenServicio);
        end;
    end;
     //lo libero de memoria
    Action := caFree;
end;

end.
