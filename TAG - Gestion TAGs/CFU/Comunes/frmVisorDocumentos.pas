{********************************** File Header ********************************
File Name   : frmVisorDocumentos.pas
Author      : rcastro
Date Created: 24/11/2004
Language    : ES-AR
Description : Visor de im�genes escaneadas de documentos

Revision 1:
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura
*******************************************************************************}

unit frmVisorDocumentos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Util,
  Dialogs, DB, ADODB, Grids, DBGrids, UtilProc, StrUtils, UtilDB, StdCtrls,
  ExtCtrls, ImagePlus, DBClient, ComCtrls, ToolWin, ImgList, DPSControls,
  VariantComboBox, BuscaClientes, DmiCtrls, BuscaTab, DBCtrls,
  ConstParametrosGenerales;

type
  TformVisorDocumentos = class(TForm)
	dsDocumentos: TDataSource;
	spObtenerIndiceScan: TADOStoredProc;
	Panel2: TPanel;
	imgDocumento: TImagePlus;
	cdsDocumentos: TClientDataSet;
	cdsDocumentosIdentificacionImagen: TStringField;
	cdsDocumentosFechaProceso: TDateTimeField;
	cdsDocumentosNumeroArchivador: TIntegerField;
	cdsDocumentosDescripcion: TStringField;
    pnlBuscarCliente: TPanel;
	lblConvenio: TLabel;
    ImageList1: TImageList;
    ToolBar1: TToolBar;
	ToolButton1: TToolButton;
	ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    Panel3: TPanel;
    Panel4: TPanel;
    cdsDocumentosNumeroCaja: TIntegerField;
	cbConvenios: TVariantComboBox;
    spObtenerConveniosIndice: TADOStoredProc;
    lb_CodigoCliente: TLabel;
	lblApellidoNombre: TLabel;
    peCodigoCliente: TPickEdit;
    Label1: TLabel;
    dblcDocEscaneada: TDBLookupComboBox;
	Panel1: TPanel;
    gbImagen: TGroupBox;
	lblUbicacion: TLabel;
    lblFechaProcesamiento: TLabel;
    lblArchivo: TLabel;
    lbl_NoDisponible: TLabel;
    btnSalir: TButton;
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure cdsDocumentosAfterScroll(DataSet: TDataSet);
    procedure btnSalirClick(Sender: TObject);
	procedure ToolButton1Click(Sender: TObject);
	procedure ToolButton3Click(Sender: TObject);
	procedure ToolButton4Click(Sender: TObject);
	procedure cbConveniosChange(Sender: TObject);
	procedure peCodigoClienteButtonClick(Sender: TObject);
	procedure peCodigoClienteChange(Sender: TObject);
  private
	{ Private declarations }
	FDirAlmacenamiento: ANSIString;
	FCodigoConvenio: LongInt;
	FCargando: Boolean;
	function CargarDocumentosConvenio (CodigoConvenio: LongInt): Boolean;
	procedure InicializarImagen;
  public
	{ Public declarations }
	function Inicializar(txtCaption: String; MDIChild: Boolean = False):Boolean; overload;
	function Inicializar(txtCaption: String; CodigoConvenio: LongInt = 0; MDIChild: Boolean = False):Boolean; overload;
  end;

var
  formVisorDocumentos: TformVisorDocumentos;

implementation

uses DMConnection, PeaTypes, PeaProcs;

{$R *.dfm}

resourcestring
	MSG_ERROR_DIRECTORIO_NO_EXISTE	= 'El directorio de im�genes no existe.' + CRLF +
										'Debe crearse o definirse correctamente dentro de los Par�metros Generales';
	MSG_ERROR_LECTURA_CONVENIO		= 'No puede leerse datos de Documentos Escaneados del convenio';

function TformVisorDocumentos.Inicializar(txtCaption: String; MDIChild: Boolean = False):Boolean;
Var
	S: TSize;
begin
	Result := false;

	if MDIChild then begin
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;
	CenterForm(Self);
	Caption := AnsiReplaceStr(txtCaption, '&', '');

	// Leo directorio destino desde parametros generales
    ObtenerParametroGeneral(DMConnections.BaseCAC, 'Dir_Imagenes_SCAN', FDirAlmacenamiento);
    FDirAlmacenamiento := GoodDir(FDirAlmacenamiento);
    Update;
	if not DirectoryExists(FDirAlmacenamiento) then begin
		MsgBox(Format(MSG_ERROR_DIRECTORIO_NO_EXISTE, [FDirAlmacenamiento]), self.Caption, MB_OK + MB_ICONERROR);
		Exit
	end;

	cdsDocumentos.CreateDataSet;
	cdsDocumentos.Open;

	Result := True;
end;
{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:
  Date Created:  /  /
  Description:
  Parameters: txtCaption: String; CodigoConvenio: LongInt = 0; MDIChild: Boolean = False
  Return Value: Boolean

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function TformVisorDocumentos.Inicializar(txtCaption: String; CodigoConvenio: LongInt = 0; MDIChild: Boolean = False):Boolean;
Var
	NumeroConvenio : String[30];
begin
	Result := Inicializar(txtCaption, MDIChild);
    if Result then begin
        lb_CodigoCliente.Visible := False;
        peCodigoCliente.Visible := False;
        imgDocumento.Visible := True;
        cbConvenios.Items.Clear;
        NumeroConvenio := Trim(QueryGetValue(DMConnections.BaseCAC, 'SELECT NumeroConvenio FROM Convenio  WITH (NOLOCK) WHERE CodigoConvenio = ' + IntToStr(CodigoConvenio)));
        if NumeroConvenio <> '' then cbConvenios.Items.Add(FormatearNumeroConvenio(NumeroConvenio), CodigoConvenio);
        Update;
        InicializarImagen;

	    Result := True;
    end;
end;


function TformVisorDocumentos.CargarDocumentosConvenio (CodigoConvenio: LongInt): Boolean;
begin
	result := false;

	try
		FCargando := True;
		with spObtenerIndiceScan, Parameters do begin
			Close;
			ParamByName('@CodigoConvenio').Value := FCodigoConvenio;
			Open;

			cdsDocumentos.EmptyDataSet;
			while not EoF do begin
				cdsDocumentos.AppendRecord([
					FieldByName('IdentificacionImagen').Value,
					FieldByName('FechaProceso').Value,
					FieldByName('NumeroCaja').Value,
					FieldByName('NumeroArchivador').Value,
					FieldByName('Descripcion').Value]);

				Next
			end;

			Close
		end;
		FCargando := False;

		cdsDocumentos.First;
		result := true
	except
		on e: Exception do MsgBoxErr(MSG_ERROR_LECTURA_CONVENIO, e.message, self.caption, MB_ICONSTOP)
	end;
end;


procedure TformVisorDocumentos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	cdsDocumentos.Close;
	Action := caFree
end;

procedure TformVisorDocumentos.cdsDocumentosAfterScroll(DataSet: TDataSet);
var
	NombreImagen: ANSIString;
	Archivador: Integer;
resourcestring
	STR_UBICACION_CAJA = 'Ubicaci�n: Caja N� ';
	STR_UBICACION_ARCHIVADOR = ' - Archivador N� ';
	STR_FECHA_PRECESAMIENTO = 'Fecha de Procesamiento: ';
	STR_ARCHIVO_ASOCIADO = 'Archivo asociado: ';
begin
	if FCargando or DataSet.IsEmpty then Exit;

	Archivador := cdsDocumentos.FieldByName('NumeroArchivador').AsInteger;
	NombreImagen := GoodDir(FDirAlmacenamiento + 'A_' + IntToStr(Archivador)) +
					cdsDocumentos.FieldByName('IdentificacionImagen').AsString + '.JPG';
	try
		imgDocumento.Visible := True;
		imgDocumento.Picture.LoadFromFile(NombreImagen);
		lblUbicacion.Caption := STR_UBICACION_CAJA + cdsDocumentos.FieldByName('NumeroCaja').AsString +
							STR_UBICACION_ARCHIVADOR+ IntToStr(Archivador);
		lblFechaProcesamiento.Caption := STR_FECHA_PRECESAMIENTO + cdsDocumentos.FieldByName('FechaProceso').AsString;
		lblArchivo.Caption := STR_ARCHIVO_ASOCIADO + cdsDocumentos.FieldByName('IdentificacionImagen').AsString + '.JPG';
	except
		imgDocumento.Visible := False;
		lblUbicacion.Caption := '';
		lblFechaProcesamiento.Caption := '';
		lblArchivo.Caption := ''
	end
end;

procedure TformVisorDocumentos.btnSalirClick(Sender: TObject);
begin
	Close
end;

procedure TformVisorDocumentos.ToolButton1Click(Sender: TObject);
begin
	imgDocumento.Scale := imgDocumento.Scale + 0.2;
end;

procedure TformVisorDocumentos.ToolButton3Click(Sender: TObject);
begin
	if imgDocumento.Scale < 0.5 then exit;
   	imgDocumento.Scale := imgDocumento.Scale - 0.2;
end;

procedure TformVisorDocumentos.ToolButton4Click(Sender: TObject);
begin
	if imgDocumento.Scale > 4.0 then exit;
    imgDocumento.Scale := 1;
end;

procedure TformVisorDocumentos.cbConveniosChange(Sender: TObject);
begin
	FCodigoConvenio := cbConvenios.Items[cbConvenios.ItemIndex].Value;
	CargarDocumentosConvenio(FCodigoConvenio);

   	if cdsDocumentos.IsEmpty then begin
		gbImagen.Visible := False;
		imgDocumento.Picture.Bitmap := Nil;
		dblcDocEscaneada.KeyValue := Null;
		dblcDocEscaneada.DropDownRows := 0;
	end
	else begin
		gbImagen.Visible := True;
		dblcDocEscaneada.DropDownRows := 8;
		dblcDocEscaneada.KeyValue := cdsDocumentos.FieldByName(dblcDocEscaneada.KeyField).Value;
    end;
end;

procedure TformVisorDocumentos.peCodigoClienteButtonClick(Sender: TObject);
var
	f: TFormBuscaClientes;
	DatosBusc: TBusquedaCliente;
begin
	imgDocumento.Visible := True;

	Application.CreateForm(TFormBuscaClientes, f);
	if f.Inicializa(DatosBusc) and (f.ShowModal = mrOk) then
	begin
		peCodigoCliente.Cursor    := crHourGlass;
		peCodigoCliente.Value     := f.Persona.CodigoPersona;
		lblApellidoNombre.Caption := f.Persona.Apellido + ' ' + f.Persona.ApellidoMaterno + ', ' + f.Persona.Nombre;
		peCodigoCliente.Cursor    := crDefault;
	end;
	f.Release;
end;

procedure TformVisorDocumentos.peCodigoClienteChange(Sender: TObject);
begin
	cdsDocumentos.EmptyDataSet;
	cbConvenios.Items.Clear;

	with spObtenerConveniosIndice do begin
		Parameters.ParamByName('@CodigoCliente').Value := peCodigoCliente.Value;
		Open;
		while not EoF do begin
			cbConvenios.Items.Add(FormatearNumeroConvenio(Trim(FieldByName('NumeroConvenio').AsString)),
								  FieldByName('CodigoConvenio').AsInteger);
			Next;
		end;
		Close;
	end;

	cbConvenios.Enabled := cbConvenios.Items.Count > 1;

	InicializarImagen;
end;

procedure TformVisorDocumentos.InicializarImagen;
begin
	if cbConvenios.Items.Count = 0 then begin
		gbImagen.Visible := False;
		imgDocumento.Picture.Bitmap := Nil;
		dblcDocEscaneada.KeyValue := Null;
		dblcDocEscaneada.DropDownRows := 0;
		Exit;
	end;

	cbConvenios.ItemIndex := 0;
	FCodigoConvenio := cbConvenios.Items[cbConvenios.ItemIndex].Value;
	CargarDocumentosConvenio(FCodigoConvenio);

	gbImagen.Visible := True;
	dblcDocEscaneada.DropDownRows := 8;

	dblcDocEscaneada.KeyValue := cdsDocumentos.FieldByName(dblcDocEscaneada.KeyField).Value;
end;


end.
