object FrameFuenteReclamoOrdenServicio: TFrameFuenteReclamoOrdenServicio
  Left = 0
  Top = 0
  Width = 239
  Height = 65
  TabOrder = 0
  object PFuenteReclamo: TGroupBox
    Left = 3
    Top = 2
    Width = 235
    Height = 60
    Caption = 'Fuente del Reclamo'
    TabOrder = 0
    object LFuenteReclamo: TLabel
      Left = 7
      Top = 25
      Width = 98
      Height = 13
      Caption = 'Fuente del Reclamo:'
    end
    object cbFuenteReclamo: TVariantComboBox
      Left = 119
      Top = 21
      Width = 105
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 0
      Items = <>
    end
  end
end
