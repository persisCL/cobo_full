{
    File Name   : SysUtilsCN
    Author      : pdominguez
    Date Created: 14/06/2010
    Language    : ES-AR
    Description : Utilidaddes del Sistema propietarias Proyecto CN.

    Autor       : CQuezadaI
    Fecha       : 19/07/2013
    Firma       : SS_660_CQU_20130711
    Descripcion : Se agrega funci�n DBListExObtenerIDColumna para retornar el ID
                  de una columna en la grilla seg�n su nombre de campo.
                  Devolver� -1 si no la encuentra.

    Autor       : CQuezadaI
    Fecha       : 10/11/2013
    Firma       : SS_660_CQU_20130822
    Descripcion : Se agrega funci�n DBListExMostrarMensajeBalloon
                  para desplegar un mensaje MsgBoxBalloon en la Celda que se indique
                  para la Grilla que se indique.
}
unit SysUtilsCN;

interface

Uses
    // Delphi
    SysUtils, Classes, Windows, Messages, Forms, Controls, Contnrs, Graphics, ADODB, DB, DBClient, Variants, WinSock, DateUtils,

    // Proyecto CN
    frmMuestraMensaje, frmBloqueosSistema,

    // Ole Integration
    Word2000, OleServer, Bullzip_TLB,

    // Componentes Kapsch AR
    DBListEx, ListBoxEx,

    UtilProc,   // SS_660_CQU_20130822
    // A Revisar
    Util, UtilDB;

type
    EGeneralExceptionCN = class(Exception)
        private
            FTitle,
            FMessage: String;
            FStyle: Integer;
        public
            constructor Create(const Title, Msg: string); virtual;
            property Title: string read FTitle write FTitle;
            property Message: string read FMessage write FMessage;
            property Style: Integer read FStyle write FStyle;
        end;

    EInfoExceptionCN = class(EGeneralExceptionCN)
        private
        public
            constructor Create(const Title, Msg: string); override;
        end;

    EErrorExceptionCN = class(EGeneralExceptionCN)
        private
        public
            constructor Create(const Title, Msg: string); override;
        end;

    EWarningExceptionCN = class(EGeneralExceptionCN)
        private
        public
            constructor Create(const Title, Msg: string); override;
        end;

    EQuestionExceptionCN = class(EGeneralExceptionCN)
        private
        public
            constructor Create(const Title, Msg: string); override;
        end;

const
    CURSOR_RELOJ   = crHourGlass;
    CURSOR_DEFECTO = crDefault;

procedure EmptyKeyQueue;

procedure CambiarEstadoCursor(pEstado: Smallint); overload;
procedure CambiarEstadoCursor(pEstado: Smallint; pFormulario: TForm); overload;
procedure CambiarEstadoCursor(pEstado: Smallint; pFormulario: TForm; var pComponentes: TObjectList); overload;

procedure ActualizaListaObjetos(var pLista: TObjectList; var pObjeto: TObject; pAccion: Boolean);
function ExisteEnListaObjetos(pLista: TObjectList; pObjeto: TObject): Boolean;

procedure DBListExSortGenerico(Grilla: TCustomDBListEx; Columna: TDBListExColumn; NombreCampo: string; NoCambiarOrden: boolean=False);
procedure DBListExColumnHeaderClickGenerico(Sender: TObject);
procedure DBListExResaltaCamposIndex(Grilla: TDBListEx);
function  DBListExObtenerIDColumna(Grilla: TDBListEx; Nombre: String; IniciarDesde: Integer) : Integer;  // SS_660_CQU_20130711
procedure DBListExMostrarMensajeBalloon(dblGrilla: TDBListEx; NombreCeldaDespliegue, Titulo, Mensaje: string);  // SS_660_CQU_20130822

procedure Word2PDF(pFicheroWord, pFicheroSalidaPDF, pficherosMergePDF, pImpresoraBullZip, pTextoMarcaAgua: string; pTextoSizeMarcaAgua: Integer = 108);

function ObtenerBloqueo(pConexionADO: TADOConnection; pObjeto, pClave, pUsuario: String; var pIDBloqueo: Integer): Integer;
function EliminarBloqueo(pConexionADO: TADOConnection; pIDBloqueo: Integer): Integer;

Function IsValidEMailCN(email: String): Boolean;

function QueryGetDateTimeValue(pConexionADO: TADOConnection; pSQL: string; pTimeout: Integer = 0): TDateTime;
function QueryGetSmallDateTimeValue(pConexionADO: TADOConnection; pSQL: string; pTimeout: Integer = 0): TDateTime;
function QueryGetDateValue(pConexionADO: TADOConnection; pSQL: string; pTimeout: Integer = 0): TDate;

function QueryGetBooleanValue(pConexionADO: TADOConnection; pSQL: string; pTimeout: Integer = 0): Boolean;

function QueryGetTinyintValue(pConexionADO: TADOConnection; pSQL: string; pTimeout: Integer = 0): Byte;
function QueryGetSmallIntValue(pConexionADO: TADOConnection; pSQL: string; pTimeout: Integer = 0): Smallint;
function QueryGetIntegerValue(pConexionADO: TADOConnection; pSQL: string; pTimeout: Integer = 0): Integer;
function QueryGetBigintValue(pConexionADO: TADOConnection; pSQL: string; pTimeout: Integer = 0): Int64;

function QueryGetStringValue(pConexionADO: TADOConnection; pSQL: string; pTimeout: Integer = 0): string;

function NowBaseCN(pDatabase: TADOConnection): TDateTime;
function SmallNowBaseCN(pDatabase: TADOConnection): TDateTime;

function HandleMessageException(e: Exception): string;

function GetIPFromHost (var HostName, IPaddr, WSAErr: string): Boolean;
function GETIPAddress(): string;

implementation


constructor EGeneralExceptionCN.Create(const Title, Msg: string);
begin
    FTitle := Title;
    FMessage := Msg;
end;

constructor EInfoExceptionCN.Create(const Title, Msg: string);
begin
    inherited;
    FStyle := MB_ICONINFORMATION;
end;

constructor EErrorExceptionCN.Create(const Title, Msg: string);
begin
    inherited;
    FStyle := MB_ICONERROR;
end;

constructor EWarningExceptionCN.Create(const Title, Msg: string);
begin
    inherited;
    FStyle := MB_ICONWARNING;
end;

constructor EQuestionExceptionCN.Create(const Title, Msg: string);
begin
    inherited;
    FStyle := MB_ICONQUESTION;
end;

{
    Date Created: 15/09/2010
    Description: Borra las pulsaciones pendientes del buffer del teclado.
}
procedure EmptyKeyQueue;
    Var
      Msg: TMsg;
Begin
    While PeekMessage( Msg, 0, WM_KEYFIRST, WM_KEYLAST, PM_REMOVE or PM_NOYIELD ) Do;
End;

function NowBaseCN(pDatabase: TADOConnection): TDateTime;
	const
    	cSQL = 'SELECT GETDATE()';
begin
    Result := QueryGetDateTimeValue(pDatabase, cSQL);
end;

function SmallNowBaseCN(pDatabase: TADOConnection): TDateTime;
	const
    	cSQL = 'SELECT GETDATE()';
begin
    Result := QueryGetSmallDateTimeValue(pDatabase, cSQL);
end;

{
    Date Created: 17/01/2011
    Description: Cambia el Cursor en Pantalla.
}
procedure CambiarEstadoCursor(pEstado: Smallint); overload;
begin
    case pEstado of
        crDefault: begin
            Screen.Cursor := crDefault;
        end;
    else
        Screen.Cursor := pEstado;
        EmptyKeyQueue;
        Application.ProcessMessages;
    end;
end;

procedure CambiarEstadoCursor(pEstado: Smallint; pFormulario: TForm); overload;
begin
    case pEstado of
        crDefault: begin
            pFormulario.Enabled := True;
            Screen.Cursor := crDefault;
        end;
    else
        Screen.Cursor := pEstado;
        pFormulario.Enabled := False;
        EmptyKeyQueue;
        Application.ProcessMessages;
    end;
end;

procedure CambiarEstadoCursor(pEstado: Smallint; pFormulario: TForm; var pComponentes: TObjectList); overload;
    var
        Indice: Integer;
begin
    case pEstado of
        crDefault: begin

            if Assigned(pFormulario) then begin
                if Assigned(pComponentes) then try
                    for Indice := 0 to pComponentes.Count - 1 do begin
                        TControl(pComponentes.Items[0]).Enabled := True;
                        pComponentes.Extract(pComponentes.Items[0]);
                    end;
                finally
                    FreeAndNil(pComponentes);
                    if pFormulario.Visible and pFormulario.Enabled then begin
                        BringWindowToTop(Application.Handle);
                    end;
                end;
            end;

            Screen.Cursor := crDefault;
        end;
    else
        
        if Assigned(pFormulario) then begin
            if not Assigned(pComponentes) then try
                pComponentes := TObjectList.Create;

                for Indice := 0 to pFormulario.ComponentCount - 1 do begin
                    if (pFormulario.Components[Indice] is TWinControl) or (pFormulario.Components[Indice] is TGraphicControl) then begin
                        if TControl(pFormulario.Components[Indice]).Enabled and TControl(pFormulario.Components[Indice]).Visible then begin
                            if not ExisteEnListaObjetos(pComponentes, TObject(pFormulario.Components[Indice])) then begin
                                pComponentes.Add(pFormulario.Components[Indice]);
                                TControl(pFormulario.Components[Indice]).Enabled := False;
                            end;
                        end;
                    end;
                end;
            finally
                if pComponentes.Count = 0 then FreeAndNil(pComponentes);
            end;
        end;

        Screen.Cursor := pEstado;
        EmptyKeyQueue;
        Application.ProcessMessages;
    end;
end;

procedure ActualizaListaObjetos(var pLista: TObjectList; var pObjeto: TObject; pAccion: Boolean);
begin
    if (pObjeto is TWinControl) or (pObjeto is TGraphicControl) then begin
        case pAccion of
            True: begin
                if pLista.IndexOf(pObjeto) = -1 then begin
                    pLista.Add(pObjeto);
                    TControl(pObjeto).Enabled := False;
                end;
            end;
            False: begin
                if pLista.IndexOf(pObjeto) <> -1 then begin
                    pLista.Extract(pObjeto);
                    TControl(pObjeto).Enabled := False;
                end;
            end;
        end;
    end;
end;

function ExisteEnListaObjetos(pLista: TObjectList; pObjeto: TObject): Boolean;
begin
    Result := pLista.IndexOf(pObjeto) <> -1;
end;

procedure DBListExSortGenerico(Grilla: TCustomDBListEx; Columna: TDBListExColumn; NombreCampo: string; NoCambiarOrden: boolean=False);
begin
    if not NoCambiarOrden then begin
        if Columna.Sorting = csAscending then begin
            Columna.Sorting := csDescending
        end
        else Columna.Sorting := csAscending;

        if Grilla.DataSource.DataSet is TCustomADODataSet then begin
            TCustomADODataSet(Grilla.DataSource.DataSet).Sort := NombreCampo + iif(Columna.Sorting = csAscending, ' ASC' ,' DESC');
        end
        else if Grilla.DataSource.DataSet is TClientDataSet then begin
            TClientDataSet(Grilla.DataSource.DataSet).IndexName := NombreCampo + iif(Columna.Sorting = csAscending, '' ,'DESC');
        end;
    end
    else begin
        Columna.Sorting := csAscending;
        if Grilla.DataSource.DataSet is TCustomADODataSet then begin
            TCustomADODataSet(Grilla.DataSource.DataSet).Sort := NombreCampo;
        end
        else if Grilla.DataSource.DataSet is TClientDataSet then begin
            TClientDataSet(Grilla.DataSource.DataSet).IndexName := NombreCampo;
        end;
    end;
end;

procedure DBListExColumnHeaderClickGenerico(Sender: TObject);
begin
    if TCustomDBListEx(TDBListExColumn(sender).Columns.List).DataSource.DataSet.Active then begin
        DBListExSortGenerico(TDBListExColumn(sender).Columns.List,
                             TDBListExColumn(sender),
                             TDBListExColumn(sender).FieldName);
    end;
end;

procedure DBListExResaltaCamposIndex(Grilla: TDBListEx);
    var
        Columnas: Integer;
begin
    for Columnas := 0 to Grilla.Columns.Count - 1 do begin
        if Assigned(Grilla.Columns.Items[Columnas].OnHeaderClick) then begin
            Grilla.Columns.Items[Columnas].Header.Font.Color := clBlue;
        end;
    end;
end;

{---------------------------------------------------------------------------------
Procedure Name  : DBListExObtenerIDColumna
Author          : CQuezadaI
Date Created    : 19/07/2013
Description     : Obtiene el ID de una Columna por su Nombre
Parameters      :   Grilla: TDBListEx       -- TDBListEx en la que se debe evaluar
                    Nombre: String;         -- Nombre de Columna a Buscar
                    IniciarDesde: Integer   -- Indice del cual iniciar la b�squeda
Return Value    : Integer
Firma           : SS_660_CQU_20130711
----------------------------------------------------------------------------------}
function DBListExObtenerIDColumna(Grilla: TDBListEx; Nombre: String; IniciarDesde: Integer) : Integer;
begin
    if IniciarDesde > Grilla.Columns.Count - 1 then begin Result := -1; Exit; end;
    if Grilla.Columns[IniciarDesde].FieldName = Nombre then Result := IniciarDesde
    else Result := DBListExObtenerIDColumna(Grilla, Nombre, IniciarDesde + 1);
end;

{---------------------------------------------------------------------------------
Procedure Name  : DBListExMostrarMensajeBalloon
Author          : CQuezadaI
Date Created    : 10/11/2013
Description     : Muestra un MsgBoxBalloon en una celda indicada.
Parameters      :   dblGrilla               : TDBListEx -- TDBListEx en la que se debe evaluar
                    NombreCeldaDespliegue   : String    -- Nombre de la celda en la que se desplegar� el mensaje
                    Titulo                  : String    -- T�tulo que se desplegar� en el mensaje
                    Mensaje                 : String    -- Mensaje a desplegar

Firma           : SS_660_CQU_20130822
----------------------------------------------------------------------------------}
procedure DBListExMostrarMensajeBalloon(dblGrilla: TDBListEx; NombreCeldaDespliegue, Titulo, Mensaje: string);
var
    PuntoGrilla : TPoint;
    CeldaGrilla : TRect;
    IndexCelda : Integer;
begin
    // Obtengo el Indice de la Celda
    IndexCelda := DBListExObtenerIDColumna(dblGrilla, NombreCeldaDespliegue, 0);
    // Obtengo la posici�n de la Grilla
    PuntoGrilla := dblGrilla.ClientToScreen(Point(0,0));
    // Obtengo la posici�n de la Celda
    CeldaGrilla := dblGrilla.Columns[IndexCelda].CurrentRect;
    // Muestro el mensaje
    MsgBoxBalloon(Mensaje
                , Titulo
                , MB_ICONEXCLAMATION
                , (PuntoGrilla.X + ((CeldaGrilla.TopLeft.X + CeldaGrilla.BottomRight.X) div 2))
                , (PuntoGrilla.Y + ((CeldaGrilla.TopLeft.Y + CeldaGrilla.BottomRight.Y) div 2)));
end;

{
    Date Created: 10/02/2011
    Description: Imprime un documento de Word en PDF usando la impresora BullZip.
}
procedure Word2PDF(pFicheroWord, pFicheroSalidaPDF, pficherosMergePDF, pImpresoraBullZip, pTextoMarcaAgua: string; pTextoSizeMarcaAgua: Integer = 108);
	const
		cUnitProcedureFunction = 'SysUtilsCN.pas.Word2PDF';
        cMSG_ERROR_EXCEPT_NN   = 'Error en Bloque %s. Error: %s';

    var
        lWordApp           : TWordApplication;
        lWordDoc           : TWordDocument;
        lPDFPrinterSettings: TPDFPrinterSettings;
        lFicheroWord,
        lActivePrinter,
        lSaveChanges       : OleVariant;
begin
    try
        try
            TPanelMensajesForm.MuestraMensaje('Creando Objetos OLE ...');
            lFicheroWord         := pFicheroWord;

            lWordApp             := TWordApplication.Create(Application);
            lWordApp.ConnectKind := ckNewInstance;
            lWordDoc             := TWordDocument.Create(Application);
            lWordDoc.ConnectKind := ckNewInstance;

            lPDFPrinterSettings := TPDFPrinterSettings.Create(Application);
            with lPDFPrinterSettings do begin

                ConnectKind := ckNewInstance;

                SetPrinterName(pImpresoraBullZip);

                SetValue('output',    pFicheroSalidaPDF);

                if pficherosMergePDF <> EmptyStr then begin
                    SetValue('mergefile', pficherosMergePDF);
                end;

                SetValue('Res',       '300');
                SetValue('ResX',      '300');
                SetValue('ResY',      '300');

                if pTextoMarcaAgua <> EmptyStr then begin
                    SetValue('WatermarkText',         pTextoMarcaAgua);
//                    SetValue('WatermarkTransparency', '80');
                    SetValue('WatermarkLayer',        'bottom');
                    SetValue('WatermarkOutlineWidth', '0');
                    SetValue('WatermarkFontSize',     IntToStr(pTextoSizeMarcaAgua));
                    SetValue('WatermarkColor',        '#00B1B1B1');
                end;

                SetValue('ShowSettings',         'never');
                SetValue('ShowPDF',              'no');
                SetValue('ShowProgress',         'no');
                SetValue('ShowProgressFinished', 'no');
                SetValue('ConfirmOverwrite',     'no');

                WriteSettings(True);
            end;

            TPanelMensajesForm.MuestraMensaje('Seteando Impresora PDF ...');
            if lWordApp.ActivePrinter <> pImpresoraBullZip then begin
                lActivePrinter         := lWordApp.ActivePrinter;
                lWordApp.ActivePrinter := pImpresoraBullZip;
            end
            else lActivePrinter := EmptyStr;

            TPanelMensajesForm.MuestraMensaje('Imprimiendo Documento PDF ...');
            lWordDoc.ConnectTo(lWordApp.Documents.Add(lFicheroWord, EmptyParam,EmptyParam,EmptyParam));

            lWordApp.PrintOut;

            while lWordApp.BackgroundPrintingStatus = 1 do begin
                Sleep(250);
                Application.ProcessMessages;
            end;

            Sleep(250);
            Application.ProcessMessages;
        except  // e:01
            on e:Exception do begin
                raise EErrorExceptionCN.Create(cUnitProcedureFunction, Format(cMSG_ERROR_EXCEPT_NN, ['01', e.Message]));
            end;
        end;
    finally
        try
            TPanelMensajesForm.MuestraMensaje('Eliminando Objetos OLE ...');        
            if Assigned(lPDFPrinterSettings) then lPDFPrinterSettings.Disconnect;

            If Assigned(lWordApp) Then begin
                TPanelMensajesForm.MuestraMensaje('Restaurando Impresora ...');
                if lActivePrinter <> '' then begin
                    lWordApp.ActivePrinter := lActivePrinter;
                end;

                lSaveChanges := wdDoNotSaveChanges;
                lWordApp.Quit(lSaveChanges);

                if Assigned(lWordDoc) then begin
                    lWordDoc.Disconnect;
                    FreeAndNil(lWordDoc);
                end;

                lWordApp.Disconnect;
                FreeAndNil(lWordApp);
            end;
        except  // e:02
            on e:Exception do begin
                raise EErrorExceptionCN.Create(cUnitProcedureFunction, Format(cMSG_ERROR_EXCEPT_NN, ['02', e.Message]));
            end;
        end;
    end;
end;

function ObtenerBloqueo(pConexionADO: TADOConnection; pObjeto, pClave, pUsuario: String; var pIDBloqueo: Integer): Integer;
begin
    try
        try
            QueryExecute(pConexionADO, 'BEGIN TRANSACTION ObtenerBloqueoObjeto');

            Result :=
                TBloqueosSistemaForm.Inicializar
                    (
                        pObjeto,
                        pClave,
                        pUsuario,
                        pIDBloqueo
                    );

            QueryExecute(pConexionADO, 'IF @@TRANCOUNT > 0 BEGIN COMMIT TRANSACTION ObtenerBloqueoObjeto END');
        except
            on e: Exception do begin
                QueryExecute(pConexionADO, 'IF @@TRANCOUNT > 0 BEGIN ROLLBACK TRANSACTION ObtenerBloqueoObjeto END');
                raise;
            end;
        end;
    finally

    end;
end;

function EliminarBloqueo(pConexionADO: TADOConnection; pIDBloqueo: Integer): Integer;
begin
    try
        try
            QueryExecute(pConexionADO, 'BEGIN TRANSACTION ObtenerBloqueoObjeto');

            Result :=
                TBloqueosSistemaForm.Inicializar
                    (
                        EmptyStr,
                        EmptyStr,
                        EmptyStr,
                        pIDBloqueo
                    );

            QueryExecute(pConexionADO, 'IF @@TRANCOUNT > 0 BEGIN COMMIT TRANSACTION ObtenerBloqueoObjeto END');
        except
            on e: Exception do begin
                QueryExecute(pConexionADO, 'IF @@TRANCOUNT > 0 BEGIN ROLLBACK TRANSACTION ObtenerBloqueoObjeto END');
                raise;
            end;
        end;
    finally

    end;
end;

Function IsValidEMailCN(email: String): Boolean;

    procedure Split (const Delimiter: Char; Input: string; const Strings: TStrings);
    begin
       Assert(Assigned(Strings));
       Strings.Clear;
       Strings.Delimiter := Delimiter;
       Strings.DelimitedText := Input;
    end;

    function HasNumbers(const S: string): Boolean;
    var
      i: integer;
    begin
      Result := True;
      i := 1;
      while i <= Length(S) do begin
        if(S[i] in ['0'..'9']) then exit;
        i := i+1;
      end;
      Result := False;
    end;

    Function IsAccountValid(S:String):Boolean;
    begin
      Result:=False;
      if S = '' then Exit;
      if Pos('..', S ) <> 0 then Exit;
      if (Copy(S, 1, 1) = '.') or
         (Copy(S, 1, 1) = '_') or
         (Copy(S, 1, 1) = '-') then Exit;
      if (Copy(S, Length(S), 1) = '.') then exit;
      Result:=True;
    end;

    Function IsDomainValid(S:String):Boolean;
    var
      ls: TStringList;
      I: Integer;
    begin
      Result := False;
      ls:= TStringList.Create;
      try
          Split('.',S, ls);
          if (ls.Count <= 1) then exit;
          for I := 0 to ls.Count - 1 do begin
            if ls[i] = '' then Exit;
            if Pos('_', ls[i] ) <> 0 then Exit;
            if (Pos('-', ls[i] ) = Length(ls[i])) or (Pos('-', ls[i])=1) then Exit;
            if(i=ls.Count-1) and (HasNumbers(ls[i])) then Exit;
          end;
          Result := True;
      finally
        ls.Free;
      end;
    end;

Var
	i, AtCount: Integer;
	Account, Domain: String;
  ls: TStringList;
begin
	Result := False;
	email := Trim(email);
	AtCount := 0;
	// Buscamos caracteres raros y arrobas
	for i := 1 to Length(email) do begin
		case email[i] of
			'@': Inc(AtCount);
			'a'..'z', 'A'..'Z', '0'..'9', '_', '-', '.':;
			else Exit;
		end;
	end;
	// Tiene una arroba?
	if AtCount <> 1 then Exit;
	// Separamos la cuenta y el dominio
  ls := TStringList.Create;
  try
      Split('@',email,ls);
      Account := ls[0];
      Domain := ls[1];
      // La cuenta es v�lida?
      if(not IsAccountValid(Account)) then Exit;
      // El dominio es v�lido?
      if(not IsDomainValid(Domain)) then Exit;
      // Todo Ok
      Result := True;
  finally
    ls.Free;
  end;
end;


function QueryGetValue(pConexionADO: TADOConnection; pSQL: string; ClaseCampo: TFieldClass; pTimeout: Integer = 0): Variant;
    resourcestring
        rsErrorTitulo            = 'Error en funci�n SysUtils.QueryGetValue';
        rsErrorMensajeClaseCampo = 'Error en funci�n SysUtils.QueryGetValue. Valor para par�metro ClaseCampo NO Definido. Valor: %s';
        rsErrorMensajeInesperado = 'Error en funci�n SysUtils.QueryGetValue. Error Inesperado: %s';
	var
        Query: TADOQuery;
begin
	try
    	try
            Query := TADOQuery.Create(nil);

            with Query do begin
                Connection     := pConexionADO;
                CommandTimeout := pTimeout;
                SQL.Text       := pSQL;
                Open;

                if Query.Fields[0].IsNull then begin
                	if (ClaseCampo = TStringField) then begin
                    	Result := EmptyStr;
                    end
                    else if (ClaseCampo = TWordField) then begin
                    	Result := 0;
                    end
                    else if (ClaseCampo = TSmallintField) then begin
                    	Result := 0;
                    end
                    else if (ClaseCampo = TIntegerField) then begin
                    	Result := 0;
                    end
                    else if (ClaseCampo = TLargeintField) then begin
                    	Result := 0;
                    end
                    else if (ClaseCampo = TDateTimeField) then begin
                    	Result := NullDate;
                    end
                    else if (ClaseCampo = TBooleanField) then begin
                    	Result := False;
                    end
                    else begin
                    	raise EErrorExceptionCN.Create(rsErrorTitulo, Format(rsErrorMensajeClaseCampo, [ClaseCampo.ClassName]));
                    end;
                end
                else begin
	                Result := (Query.Fields[0] as ClaseCampo).AsVariant;
                end;
            end;
        except
        	on e: Exception do begin
	            raise EErrorExceptionCN.Create(rsErrorTitulo, Format(rsErrorMensajeInesperado, [e.Message]));
            end;
        end;
    finally
    	if Assigned(Query) then FreeAndNil(Query);
    end;
end;

function QueryGetDateTimeValue(pConexionADO: TADOConnection; pSQL: string; pTimeout: Integer = 0): TDateTime;
begin
    try
    	Result := QueryGetValue(pConexionADO, pSQL, TDateTimeField, pTimeout);
    except
    	raise
    end;
end;

function QueryGetSmallDateTimeValue(pConexionADO: TADOConnection; pSQL: string; pTimeout: Integer = 0): TDateTime;
    var
        SmallDateTime: TDateTime;
        SmallYear,
        SmallMonth,
        SmallDay,
        SmallHour,
        SmallMinutes,
        SmallSeconds,
        SmallMiliseconds: Word;

begin
    try
    	SmallDateTime := QueryGetValue(pConexionADO, pSQL, TDateTimeField, pTimeout);
        DecodeDateTime(SmallDateTime, SmallYear, SmallMonth, SmallDay, SmallHour, SmallMinutes, SmallSeconds, SmallMiliseconds);

        if SmallSeconds >= 29 then begin
            if SmallSeconds > 29 then begin
                Inc(SmallMinutes);
            end
            else begin
            	if SmallMiliseconds = 999 then begin
	                Inc(SmallMinutes);
                end;
            end;
        end;

        SmallSeconds     := 0;
        SmallMiliseconds := 0;

        Result := EncodeDateTime(SmallYear, SmallMonth, SmallDay, SmallHour, SmallMinutes, SmallSeconds, SmallMiliseconds);
    except
    	raise
    end;
end;

function QueryGetDateValue(pConexionADO: TADOConnection; pSQL: string; pTimeout: Integer = 0): TDate;
	var
        _Result: TDate;
begin
    try
    	_Result := QueryGetValue(pConexionADO, pSQL, TDateTimeField, pTimeout);

        Result    := _Result;
    except
    	raise
    end;
end;

function QueryGetBooleanValue(pConexionADO: TADOConnection; pSQL: string; pTimeout: Integer = 0): Boolean;
begin
    try
    	Result := QueryGetValue(pConexionADO, pSQL, TBooleanField, pTimeout);
    except
    	raise
    end;
end;

function QueryGetTinyintValue(pConexionADO: TADOConnection; pSQL: string; pTimeout: Integer = 0): Byte;
	var
        _Result: Byte;
begin
    try
    	_Result :=  QueryGetValue(pConexionADO, pSQL, TWordField, pTimeout);

        Result    := _Result;
    except
    	raise
    end;
end;

function QueryGetSmallIntValue(pConexionADO: TADOConnection; pSQL: string; pTimeout: Integer = 0): Smallint;
	var
        _Result  : SmallInt;
begin
    try
    	_Result :=  QueryGetValue(pConexionADO, pSQL, TSmallintField, pTimeout);

        Result    := _Result
    except
    	raise
    end;
end;

function QueryGetIntegerValue(pConexionADO: TADOConnection; pSQL: string; pTimeout: Integer = 0): Integer;
begin
    try
    	Result :=  QueryGetValue(pConexionADO, pSQL, TIntegerField, pTimeout);
    except
    	raise
    end;
end;

function QueryGetBigintValue(pConexionADO: TADOConnection; pSQL: string; pTimeout: Integer = 0): Int64;
begin
    try
    	Result :=  QueryGetValue(pConexionADO, pSQL, TLargeintField, pTimeout);
    except
    	raise
    end;
end;

function QueryGetStringValue(pConexionADO: TADOConnection; pSQL: string; pTimeout: Integer = 0): string;
begin
    try
    	Result :=  QueryGetValue(pConexionADO, pSQL, TStringField, pTimeout);
    except
    	raise
    end;
end;

function HandleMessageException(e: Exception): string;
begin
    if e is EGeneralExceptionCN then begin
        Result := EGeneralExceptionCN(e).Message;
    end
    else if e is Exception then begin
    	Result := Exception(e).Message;
    end;
end;

function GetIPFromHost (var HostName, IPaddr, WSAErr: string): Boolean;
    type
        Name = array[0..100] of Char;
        PName = ^Name;
    var
        HEnt: pHostEnt;
        HName: PName;
        WSAData: TWSAData;
        i: Integer;
begin
    Result := False;

    if WSAStartup($0101, WSAData) <> 0 then begin
        WSAErr := 'Winsock is not responding."';
        Exit;
    end;

    IPaddr := '';
    New(HName);
    if GetHostName(HName^, SizeOf(Name)) = 0 then
    begin
        HostName := StrPas(HName^);
        HEnt := GetHostByName(HName^);
        for i := 0 to HEnt^.h_length - 1 do
            IPaddr :=
                Concat(IPaddr,
                IntToStr(Ord(HEnt^.h_addr_list^[i])) + '.');
                SetLength(IPaddr, Length(IPaddr) - 1);

        Result := True;
    end
    else begin
        case WSAGetLastError of
            WSANOTINITIALISED:WSAErr:='WSANotInitialised';
            WSAENETDOWN      :WSAErr:='WSAENetDown';
            WSAEINPROGRESS   :WSAErr:='WSAEInProgress';
        end;
    end;
    
    Dispose(HName);
    WSACleanup;
end;


function GETIPAddress(): string;
    type
    	pu_long = ^u_long;
	var
    	varTWSAData : TWSAData;
		varPHostEnt : PHostEnt;
		varTInAddr : TInAddr;
		namebuf : Array[0..255] of ansichar;
begin
    try
        try
            If WSAStartup($101,varTWSAData) <> 0 Then
                Result := ''
            Else Begin
                gethostname(namebuf,sizeof(namebuf));
                varPHostEnt := gethostbyname(namebuf);
                varTInAddr.S_addr := u_long(pu_long(varPHostEnt^.h_addr_list^)^);
                Result := inet_ntoa(varTInAddr);
            End;
        except
        	on e: Exception do begin
	            raise Exception.Create(e.Message);
            end;
        end;
    finally
        WSACleanup;
    end;
end;

end.
