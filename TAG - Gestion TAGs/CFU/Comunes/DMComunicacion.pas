{********************************** File Header ********************************
File Name   : DMComunicacion.pas
Author      :
Date Created:
Language    : ES-AR
Description :
--------------------------------------------------------------------------------
Historial de Revisiones
--------------------------------------------------------------------------------
Author      :
Date        :
Description :
*******************************************************************************}
unit DMComunicacion;

interface

uses
  SysUtils, Classes, DmConnection, DB, ADODB, DBTables;

type
  TDMComunicaciones = class(TDataModule)
    spCierraLogComunicaciones: TADOStoredProc;
    spAgregarLogComunicacioes: TADOStoredProc;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DMComunicaciones: TDMComunicaciones;

implementation

{$R *.dfm}

end.
