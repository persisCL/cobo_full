object DMEvalWorkflow: TDMEvalWorkflow
  OldCreateOrder = False
  OnDestroy = DataModuleDestroy
  Left = 391
  Top = 229
  Height = 203
  Width = 222
  object qry_Tareas: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'CodigoWorkflow'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'
      '  TareasWorkflow.*'
      'FROM'
      '  TareasWorkflow,'
      '  Workflows'
      'WHERE'
      '  Workflows.CodigoWorkflow = :CodigoWorkflow'
      '  AND TareasWorkflow.CodigoWorkflow = Workflows.CodigoWorkflow'
      '  AND TareasWorkflow.Version = Workflows.VersionActual')
    Left = 40
    Top = 48
  end
  object qry_Secuencias: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'CodigoWorkflow'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'
      '  SecuenciaTareasWorkflow.*'
      'FROM'
      '  SecuenciaTareasWorkflow,'
      '  Workflows'
      'WHERE'
      '  Workflows.CodigoWorkflow = :CodigoWorkflow'
      
        '  AND SecuenciaTareasWorkflow.CodigoWorkflow = Workflows.CodigoW' +
        'orkflow'
      '  AND SecuenciaTareasWorkflow.Version = Workflows.VersionActual')
    Left = 128
    Top = 48
  end
  object qry_GrabarTarea: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'CodigoOrdenServicio'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'CodigoTarea'
        DataType = ftWord
        Precision = 3
        Size = 1
        Value = Null
      end
      item
        Name = 'Prioridad'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Size = 1
        Value = Null
      end>
    SQL.Strings = (
      'INSERT INTO'
      '  OrdenesServicioTareas'
      '  (CodigoOrdenServicio,  CodigoTarea, Prioridad, Estado)'
      'VALUES '
      '  (:CodigoOrdenServicio, :CodigoTarea, :Prioridad, '#39'P'#39')'
      '')
    Left = 136
    Top = 104
  end
end
