{-----------------------------------------------------------------------------
 Unit Name: frmReemisionNotaCobro
 Author:    mlopez
 Date:      29-Jul-2005
 Purpose:   Reemtir una Nota de Cobro, recalculando el total a Pagar
 History:

 Revision 1
 Author: nefernandez
 Date: 09/03/2007
 Description: Se pasa el parametro CodigoUsuario al SP ReemisionNotaCobro
              (para Auditoria)

 Revision 2
 Author: nefernandez
 Date: 16/03/2007
 Description: Se reemplazo el uso de un TADOQuery por la
            llamada a un SP, para actualizar la FechaHora de
            impresion del Comprobante

 Revision :  4
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura

    Firma       :   SS_1006_1015_CQU_20120907
    Descripcion :   Se modifica la funci�n EsFechaHabil quitando la validaci�n en ella,
                    agregando llamada a la funci�n EsDiaHabil desde PeaProcs.pas para realizarla
                    ya que en �sta �ltima se valida que el d�a sea laboral y considera los feriados.
                    Se agrega "Exit" en la validaci�n de d�a h�bil ya que no estaba contemplado
                    en la funci�n ValidarFechaVencimiento.

Firma       :   SS_1120_MVI_20130930
Descripcion :   Se crea llamado a DrawItem para verificar si el combo del convenio
                posee alg�n convenio con el texto "(Baja)" y en caso de encontrarlo
                llamar a la funci�n que lo colorear� en rojo.
                Adem�s se modifica el estilo del combo a vcsOwnerDrawFixed.

Firma       : SS_1147_NDR_20141216
descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

-----------------------------------------------------------------------------}

unit frmReemisionNotaCobro;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ListBoxEx, DBListEx, StdCtrls, DPSControls, VariantComboBox,
  DmiCtrls, DB, ADODB, DMCOnnection, utilDB, UtilFacturacion, utilProc, buscaClientes,
  PeaTypes, RStrings, DBCtrls, peaProcs, Util, PagoVentanilla, ExtCtrls, ReporteRecibo,
  FrmImprimir, DBClient, ImgList, ReporteFactura, Validate, DateEdit, DateUtils;

const
	KEY_F9 	= VK_F9;
type
  TformReemisionNotaCobro = class(TForm)
  	gbPorCliente: TGroupBox;
    lb_CodigoCliente: TLabel;
    lbl_NumeroConvenio: TLabel;
    peNumeroDocumento: TPickEdit;
    cbConvenios: TVariantComboBox;
    gb_DatosCliente: TGroupBox;
    Label6: TLabel;
    lblNombre: TLabel;
    lblDomicilio: TLabel;
  	Label10: TLabel;
    gbDatosComprobante: TGroupBox;
    Label2: TLabel;
    lblFecha: TLabel;
    lblFechaVencimiento: TLabel;
    Label7: TLabel;
    Label12: TLabel;
    sp_ObtenerComprobantesReemitir: TADOStoredProc;
    lbl_NumeroComprobante: TLabel;
    lbl_CodigoBarra: TLabel;
    edNumeroComprobante: TNumericEdit;
  	edCodigoBarras: TEdit;
    Label11: TLabel;
    Label8: TLabel;
    lblRUT: TLabel;
    lblMensaje: TLabel;
    Img_Tilde: TImageList;
    btn_Reemitir: TButton;
    btnSalir: TButton;
    Timer1: TTimer;
    lbl_TotalComprobantes: TLabel;
    sp_ReemisionNotaCobro: TADOStoredProc;
    lbl_SaldoAFavor: TLabel;
    Label4: TLabel;
    lbl_Estado: TLabel;
    Label5: TLabel;
    Label9: TLabel;
    lbl_NotaCobro: TLabel;
    Label1: TLabel;
    deFechaVencimiento: TDateEdit;
    spFechasProximaFacturacion: TADOStoredProc;
    spActualizarFechaImpresionComprobantes: TADOStoredProc;
    procedure peNumeroDocumentoChange(Sender: TObject);
	  procedure cbConveniosChange(Sender: TObject);
    procedure peNumeroDocumentoButtonClick(Sender: TObject);
	  procedure btnSalirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btn_ReemitirClick(Sender: TObject);
  	procedure cbComprobantesChange(Sender: TObject);
    procedure edCodigoBarrasChange(Sender: TObject);
    procedure edCodigoBarrasKeyPress(Sender: TObject; var Key: Char);
  	procedure edNumeroComprobanteChange(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  	procedure Timer1Timer(Sender: TObject);
    procedure cbConveniosDrawItem(Control: TWinControl; Index: Integer;             //SS_1120_MVI_20130930
      Rect: TRect; State: TOwnerDrawState);                                         //SS_1120_MVI_20130930
  private
	{ Private declarations }
    FTipoComprobante: AnsiString;
  	FNumeroComprobante: Int64;
  	FUltimaBusqueda: TBusquedaCliente;
	FFechaEmision : TDateTime;
    FVencimientoAnterior : TDateTime;
    FVencimientoProgramado : TDateTime;
    FNumeroPOS, FPuntoEntrega, FPuntoVenta: Integer;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    procedure MostrarDatosCliente(CodigoCLiente: Integer);
  	function  CargarComprobantes : boolean;
    procedure LimpiarConvenios;
    procedure LimpiarDetalles;
    procedure LimpiarDatosCliente;
    procedure MostrarDatosComprobante;
    procedure HabilitarFiltroRUT(Valor: Boolean);
    function ImprimirComprobante(TipoComprobante:string;NroComprobante: int64): Boolean;
    function EsFechaHabil(aFecha: TDateTime): Boolean;
    function ValidarFechaVencimiento: boolean;
  public
    { Public declarations }
    function Inicializar(NumeroPOS, PuntoEntrega, PuntoVenta: Integer): Boolean; overload;
  end;

implementation

{$R *.dfm}
{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    flamas
  Date Created: 21/12/2004
  Description: Inicializa el Form - Carga los tipos de Comprobantes
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
{-----------------------------------------------------------------------------
  Revision: 1
  Author: ndonadio
  Solucion tema LOGO no levantado.
------------------------------------------------------------------------------}
function TformReemisionNotaCobro.Inicializar(NumeroPOS, PuntoEntrega, PuntoVenta: Integer): Boolean;
resourcestring
	MSG_LOGO_NO_ENCONTRADO  = 'No se ha encontrado el archivo con la imagen del logo para el recibo.';
	MSG_CAPTION             = 'Cobro de Comprobantes';
	MSG_ERROR_INICIALIZAR   = 'Error inicializando el formulario.';
begin
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216
    
    try
        LimpiarDatosCliente;
        LimpiarDetalles;
        btn_Reemitir.Enabled := False;
        deFechaVencimiento.Enabled := False;

        FNumeroPOS := NumeroPOS;
        FPuntoEntrega := PuntoEntrega;
        FPuntoVenta := PuntoVenta;

        FormStyle := fsNormal;
        CenterForm(Self);
        Result := True;
    except
	   	on e: exception do begin
			MsgBoxErr(MSG_ERROR_INICIALIZAR, E.Message, MSG_CAPTION, MB_ICONERROR);
			Result := False;
        end;
    end; // except


end;

{-----------------------------------------------------------------------------
  Function Name: peNumeroDocumentoChange
  Author:    flamas
  Date Created: 20/12/2004
  Description: Actualiza los Convenios y Comprobantes Impagos de un cliente determinado
  Parameters: Sender: TObject
  Return Value: None

  Revision 1:
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure TformReemisionNotaCobro.peNumeroDocumentoChange(Sender: TObject);
var
	FCodigoCliente : integer;
begin
    if ActiveControl <> Sender then Exit;

    edNumeroComprobante.Clear;

    LimpiarDatosCliente;
    LimpiarDetalles;
    LimpiarConvenios;

    FCodigoCliente := QueryGetValueInt(DMConnections.BaseCAC, Format(
      'SELECT CodigoPersona FROM Personas  WITH (NOLOCK) WHERE CodigoDocumento = ''RUT'' ' +
      'AND NumeroDocumento = ''%s''', [iif(peNumeroDocumento.Text <> '',
      									trim(PadL(peNumeroDocumento.Text, 9, '0')), '')]));

	if FCodigoCliente > 0 then begin
		CargarConveniosRUT(DMConnections.BaseCAC, cbConvenios, 'RUT', peNumeroDocumento.Text, False, 0, False);

    	MostrarDatosCliente(FCodigoCliente);

        CargarComprobantes;
	end;

end;

{-----------------------------------------------------------------------------
  Function Name: cbConveniosChange
  Author:    flamas
  Date Created: 21/12/2004
  Description: Procesa un Cambio de Convenio - Trae los comprobantes impagos
				del dicho Convenio
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformReemisionNotaCobro.cbConveniosChange(Sender: TObject);
begin
	if ActiveControl <> Sender then Exit;
    Cargarcomprobantes;
end;

//BEGIN:SS_1120_MVI_20130930 -------------------------------------------------
{-----------------------------------------------------------------------------
  Procedure: TformReemisionNotaCobro.cbConveniosDrawItem
  Date:      30-September-2013
  Description: Verifica si existe alg�n convenio de baja para llamar a la funci�n
                que colorea de rojo el item.
-----------------------------------------------------------------------------}
procedure TformReemisionNotaCobro.cbConveniosDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin

  if Pos( TXT_CONVENIO_DE_BAJA, cbConvenios.Items[Index].Caption ) > 0 then  begin

     ItemComboBoxColor(Control, Index, Rect, State, clred, true);

  end
  else begin

     ItemComboBoxColor(Control, Index, Rect, State);

  end;

end;
// END: SS_1120_MVI_20130930 ----------------------------------------------------------------------------------


{-----------------------------------------------------------------------------
  Function Name: CargarComprobantes
  Author:    flamas
  Date Created: 20/12/2004
  Description: Carga los comprobantes impagos de un Convenio determinado
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
function TformReemisionNotaCobro.CargarComprobantes : boolean;
begin
    sp_ObtenerComprobantesReemitir.Close;
    sp_ObtenerComprobantesReemitir.Parameters.ParamByName('@CodigoConvenio').Value := iif(cbConvenios.Value = 0, Null, cbConvenios.Value);
    sp_ObtenerComprobantesReemitir.Parameters.ParamByName('@TipoComprobante').Value := 'NK';
    sp_ObtenerComprobantesReemitir.Parameters.ParamByName('@NumeroComprobante').Value :=
        											iif( edNumeroComprobante.Text <> '', edNumeroComprobante.Value, null);
 	sp_ObtenerComprobantesReemitir.Open;
    // Cargamos la lista de comprobantes.
    MostrarDatosCliente(sp_ObtenerComprobantesReemitir.FieldByName('CodigoPersona').AsInteger);

    Result := True;
                    (* Incrementar el monto total de comprobantes. *)
    lbl_TotalComprobantes.Caption   := FormatFloat(FORMATO_IMPORTE, sp_ObtenerComprobantesReemitir.FieldByName('TotalAPagar').AsFloat);
    lbl_SaldoAFavor.Caption   := FormatFloat(FORMATO_IMPORTE, sp_ObtenerComprobantesReemitir.FieldByName('SaldoAFavor').AsFloat);

    (* Si hay comprobantes por cobrar, habilitar el bot�n de Cobrar. *)
    btn_Reemitir.Enabled := (sp_ObtenerComprobantesReemitir.FieldByName('EsUltimoComprobante').AsBoolean
                            and (sp_ObtenerComprobantesReemitir.FieldByName('EstadoPago').AsString = 'I'));
                          //  and (sp_ObtenerComprobantesReemitir.FieldByName('SaldoAFavor').AsInteger <> 0));
    deFechaVencimiento.Enabled := ExisteAcceso('Modif_Fecha_Vencimiento_Reemision') and btn_Reemitir.Enabled;
end;

{-----------------------------------------------------------------------------
  Function Name: MostrarDatosCliente
  Author:    flamas
  Date Created: 20/12/2004
  Description:  Muestra los datos del Cliente
  Parameters: CodigoCliente: Integer
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformReemisionNotaCobro.MostrarDatosCliente(CodigoCliente: Integer);
begin
	// Cargamos la informaci�n del cliente.
    lblRUT.Caption := QueryGetValue(DMConnections.BaseCAC, Format(
      'SELECT dbo.ObtenerRUTPersona(%d)', [CodigoCliente]));
    lblNombre.Caption := QueryGetValue(DMConnections.BaseCAC, Format(
	  'SELECT dbo.ObtenerNombrePersona(%d)', [CodigoCliente]));
	lblDomicilio.Caption := QueryGetValue(DMConnections.BaseCAC, Format(
      'SELECT dbo.ObtenerDescripcionDomicilioPrincipalPersona(%d)', [CodigoCliente]));
    MostrarDatosComprobante;
end;

{-----------------------------------------------------------------------------
  Function Name: MostrarDatosComprobante
  Author:    flamas
  Date Created: 20/12/2004
  Description: Muestra los datos del Comprobante
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformReemisionNotaCobro.MostrarDatosComprobante;
resourcestring
    MSG_NOT_LAST_INVOICE = 'No es la �ltima Nota de Cobro emitida';
    MSG_ERROR			 = 'Error';
    MSG_ERROR_GETTING_DATES = 'Error obteniendo la pr�xima fecha de vencimiento programada';
begin
    if sp_ObtenerComprobantesReemitir.Active and not sp_ObtenerComprobantesReemitir.IsEmpty then begin

		lblFecha.Caption            	:= FormatDateTime('dd/mm/yyyy', sp_ObtenerComprobantesReemitir.FieldByName('FechaEmision').AsDateTime);
		lblFechaVencimiento.Caption 	:= FormatDateTime('dd/mm/yyyy', sp_ObtenerComprobantesReemitir.FieldByName('FechaVencimiento').AsDateTime);
		FVencimientoAnterior			:= sp_ObtenerComprobantesReemitir.FieldByName('FechaVencimiento').AsDateTime;
        FFechaEmision					:= sp_ObtenerComprobantesReemitir.FieldByName('FechaEmision').AsDateTime;
        deFechaVencimiento.Date			:= sp_ObtenerComprobantesReemitir.FieldByName('FechaVencimiento').AsDateTime;
        lbl_Estado.Caption := sp_ObtenerComprobantesReemitir.FieldByName('DescriEstado').AsString;

        FTipoComprobante    := sp_ObtenerComprobantesReemitir.FieldByName('TipoComprobante').AsString;
        FNumeroComprobante  := sp_ObtenerComprobantesReemitir.FieldByName('NumeroComprobante').AsInteger;
        lbl_NotaCobro.Caption := sp_ObtenerComprobantesReemitir.FieldByName('NumeroComprobante').AsString;
        if not sp_ObtenerComprobantesReemitir.FieldByName('EsUltimoComprobante').AsBoolean then
            lblMensaje.Caption := MSG_NOT_LAST_INVOICE
        else lblMensaje.Caption := '';

        try
			spFechasProximaFacturacion.Close;
            spFechasProximaFacturacion.Parameters.ParamByName('@CodigoConvenio').Value := sp_ObtenerComprobantesReemitir.FieldByName('CodigoConvenio').AsInteger;
			spFechasProximaFacturacion.Open;
            FVencimientoProgramado := spFechasProximaFacturacion.FieldByName('Vencimiento').AsDateTime;
        except
        	on e : exception do
				MsgBoxErr(MSG_ERROR_GETTING_DATES, e.Message, MSG_ERROR, MB_ICONERROR);
        end;
        
    end else begin
        LimpiarDetalles;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: LimpiarDetalles;
  Author:    flamas
  Date Created: 26/01/2005
  Description: Borra los detalles del Comprobante
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformReemisionNotaCobro.LimpiarDetalles;
begin
	lblFecha.Caption				:= EmptyStr;
	lblFechaVencimiento.Caption		:= EmptyStr;
	lbl_Estado.Caption				:= EmptyStr;
	lbl_NotaCobro.Caption			:= EmptyStr;
    lbl_TotalComprobantes.Caption	:= EmptyStr;
    lbl_SaldoAFavor.Caption			:= EmptyStr;
	btn_Reemitir.Enabled			:= False;
    deFechaVencimiento.Enabled 		:= False;
end;

{-----------------------------------------------------------------------------
  Function Name: peNumeroDocumentoButtonClick
  Author:    flamas
  Date Created: 21/12/2004
  Description: Procesa un cambio en el RUT - Muestra los comprobantes pendientes
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformReemisionNotaCobro.peNumeroDocumentoButtonClick(Sender: TObject);
var
    f: TFormBuscaClientes;
begin
    Application.CreateForm(TFormBuscaClientes,F);
	if F.Inicializa(FUltimaBusqueda) then begin
		F.ShowModal;
        if F.ModalResult = idOk then begin
            FUltimaBusqueda := F.UltimaBusqueda;
            peNumeroDocumento.Text := f.Persona.NumeroDocumento;
		end;
	end;
    F.free;
end;

{-----------------------------------------------------------------------------
  Function Name: cbComprobantesChange
  Author:    flamas
  Date Created: 21/12/2004
  Description: Procesa un cambio de comprobante y muestra los datos
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformReemisionNotaCobro.cbComprobantesChange(Sender: TObject);
begin
    if ActiveControl <> Sender then Exit;
    MostrarDatosComprobante;
end;

{-----------------------------------------------------------------------------
  Function Name: edNumeroComprobanteChange
  Author:    flamas
  Date Created: 21/12/2004
  Description: Procesa un cambio de comprobante y muestra los datos del comprobante
  Parameters: Sender: TObject
  Return Value: None
  -----------------------------------------------------------------------------}
procedure TformReemisionNotaCobro.edNumeroComprobanteChange(Sender: TObject);
begin
    if ActiveControl <> Sender then Exit;

    peNumeroDocumento.Clear;
    LimpiarConvenios;
    HabilitarFiltroRUT(edNumeroComprobante.Text = EmptyStr);

	if Timer1.Enabled then begin
        Timer1.Enabled := False;
        Timer1.Enabled := True;
    end else Timer1.Enabled := True;
end;

{----------------------------------------------------------------------------
  Function Name: edCodigoBarrasChange
  Author:    flamas
  Date Created: 21/12/2004
  Description: Procesa un cambio del c�digo de barras y muestra los datos
				del comprobante
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformReemisionNotaCobro.edCodigoBarrasChange(Sender: TObject);
resourcestring
	MSG_ALREADY_PAID = 'Este comprobante est� pago';
begin
    if ActiveControl <> Sender then Exit;
	if Length(edCodigoBarras.Text) = 29 then begin
    	edNumeroComprobante.Value := StrToInt( Copy( edCodigoBarras.Text, 7, 9)); //StrToInt( Copy( edCodigoBarras.Text, 21, Length(edCodigoBarras.Text)));
        CargarComprobantes;
    end else begin
        edNumeroComprobante.Clear;
        cbConvenios.Value           := 0;
		peNumeroDocumento.Clear;
		lblFecha.Caption            := '';
        lblFechaVencimiento.Caption	:= '';
        lbl_Estado.Caption          := EmptyStr;
        LimpiarDatosCliente;
        btn_Reemitir.Enabled		:= False;
        deFechaVencimiento.Enabled 	:= False;
    end;
end;

procedure TformReemisionNotaCobro.edCodigoBarrasKeyPress(Sender: TObject; var Key: Char);
begin
	if  not (Key in [#8, #9, #13, '0'..'9']) then Key := #0;
    if (Key = #13) AND (Length(edCodigoBarras.Text) < 29) then begin
        if edCodigobarras.Text <> '' then begin
			edCodigoBarras.Text :=  stringreplace( format('%29.0d',[ival(edCodigoBarras.Text)]), ' ' , '0',[rfReplaceAll]);
            if btn_Reemitir.Enabled then btn_Reemitir.SetFocus;
        end;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: btn_ReemitirClick
  Author:    MLopez
  Date Created: 29/07/2005
  Description: Cobra un Comprobante
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformReemisionNotaCobro.btn_ReemitirClick(Sender: TObject);
resourcestring
	MSG_CONFIRMAR_REEMISION = '�Desea Reemitir la Nota de Cobro %s?';
	MSG_CAPTION = 'Reemitir Nota de Cobro';
	MSG_IMPRESION_EXITOSA = '�La impresi�n se ha realizado con �xito?';           //SS-1006-1015-MCO-20120904
	MSG_DESEA_IMPRIMIR = 'La Nota de Cobro %s '#10#13 +
                         'Se reemiti� correctamente'#10#13#10#13  +
                         'Total a pagar anterior: %s'#10#13 +
                         'Total a pagar actual: %s'#10#13#10#13  +
                         '�Desea imprimir la Nota de Cobro?';
  MSG_ERROR = 'Error reemitiento la Nota de Cobro';
	MSG_DESEA_REMITIR_OTRA = '�Desea reemitir otra Nota de Cobro?';

begin
	// Valida la Fecha de Vencimiento
	if not ValidarFechaVencimiento then Exit;

    with sp_ReemisionNotaCobro, Parameters do begin
        if MsgBox(Format(MSG_CONFIRMAR_REEMISION, [sp_ObtenerComprobantesReemitir.FieldByName('NumeroComprobante').Value]),
                  MSG_CAPTION,
                  MB_ICONINFORMATION or MB_YESNO) <> ID_YES then Exit;

        // Comienza el proceso de Reemision
        btn_Reemitir.Enabled 	   := False;
        deFechaVencimiento.Enabled := False;
        Parameters.ParamByName('@TipoComprobante').Value := 'NK';
        ParamByName('@NumeroComprobante').Value := sp_ObtenerComprobantesReemitir.FieldByName('NumeroComprobante').Value;
        ParamByName('@FechaVencimiento').Value := deFechaVencimiento.Date;
        // Revision 1
        ParamByName('@CodigoUsuario').Value := UsuarioSistema;
        ExecProc;
        if ParamByName('@DescripcionError').Value = '' then begin
            if MsgBox(Format(MSG_DESEA_IMPRIMIR, [ParamByName('@NumeroComprobante').Value,
                                                 ParamByName('@TotalAnterior').Value,
                                                 ParamByName('@TotalRecalculado').Value]),
                      MSG_CAPTION,
                      MB_ICONINFORMATION or MB_YESNO) = ID_YES then
                ImprimirComprobante('NK', sp_ObtenerComprobantesReemitir.FieldByName('NumeroComprobante').Value);
            if MsgBox(MSG_DESEA_REMITIR_OTRA, MSG_CAPTION, MB_ICONINFORMATION or MB_YESNO) <> ID_YES then Self.Close
            else begin
            	edNumeroComprobante.Clear;
                peNumeroDocumento.Clear;
                LimpiarDatosCliente;
                LimpiarDetalles;
                LimpiarConvenios;
                edNumeroComprobante.SetFocus;
            end;

        end else begin
            MsgBoxErr(MSG_ERROR, ParamByName('@DescripcionError').Value, MSG_CAPTION, MB_ICONERROR);
        end;
    end;
end;

procedure TformReemisionNotaCobro.btnSalirClick(Sender: TObject);
begin
	Close;
end;

procedure TformReemisionNotaCobro.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree;
end;

procedure TformReemisionNotaCobro.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if (Key = KEY_F9) and btn_Reemitir.Enabled then btn_Reemitir.Click;
end;

procedure TformReemisionNotaCobro.Timer1Timer(Sender: TObject);
begin
    Timer1.Enabled := False;
    CargarComprobantes;
    edCodigoBarras.Text := EmptyStr;
end;

{-----------------------------------------------------------------------------
  Function Name: LimpiarConvenios
  Author:    mlopez
  Date Created: 29/07/2005
  Description: Limpia los Convenios
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformReemisionNotaCobro.LimpiarConvenios;
begin
    cbConvenios.Clear;
    cbConvenios.Items.Add(SELECCIONAR, 0);
    cbConvenios.ItemIndex := 0;
end;

procedure TformReemisionNotaCobro.HabilitarFiltroRUT(Valor: Boolean);
begin
    peNumeroDocumento.Enabled := Valor;
    lb_CodigoCliente.Enabled := Valor;
    cbConvenios.Enabled := Valor;
    lbl_NumeroConvenio.Enabled := Valor;
end;

{-----------------------------------------------------------------------------
  Function Name: LimpiarDatosCliente;
  Author:    mlopez
  Date Created: 29/07/2005
  Description: Limpia los datos del Cliente
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TformReemisionNotaCobro.LimpiarDatosCliente;
begin
    lblRUT.Caption := EmptyStr;
    lblNombre.Caption := EmptyStr;
    lblDomicilio.Caption := EmptyStr;
end;

{-----------------------------------------------------------------------------
  Function Name: ImprimirComprobante
  Author:    mlopez
  Date Created: 29/07/2005
  Description: Imprime los Comprobantes
  Parameters: TipoComprobante:string;NroComprobante: int64
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TformReemisionNotaCobro.ImprimirComprobante(TipoComprobante:string;NroComprobante: int64): Boolean;
var
    Qry:    TADOQuery;
    f:      TFrmReporteFactura;
    sError: string;
begin
    Result := False;
    Application.CreateForm(TFrmReporteFactura,f);
    try
        if not f.Inicializar(DMConnections.BaseCAC, TipoComprobante, NroComprobante, sError, True) then begin
            ShowMessage( sError );
        	Exit;
        end;
        Result := f.Ejecutar;
    	if Result then begin
            // Hago que se marque como Impreso el Comprobante...
            // Revision 2
            spActualizarFechaImpresionComprobantes.Parameters.ParamByName('@TipoComprobante').Value := TipoComprobante;
            spActualizarFechaImpresionComprobantes.Parameters.ParamByName('@NumeroComprobante').Value := NroComprobante;
            spActualizarFechaImpresionComprobantes.Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;
            spActualizarFechaImpresionComprobantes.ExecProc;
        end;
	finally
        f.Release;
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: ValidarFechaVencimiento
  Author:    flamas
  Date Created: 01/08/2005
  Description: Valida la Fecha de Vencimiento
  Parameters:
  Return Value: boolean
-----------------------------------------------------------------------------}
function TformReemisionNotaCobro.ValidarFechaVencimiento : boolean;
resourcestring
    MSG_FECHA_VENCIMIENTO_NO_EXISTE     = 'Debe especificarse una fecha de vencimiento.';
    MSG_FECHA_VENCIMIENTO_INCORRECTA    = 'La fecha de vencimiento no puede ser menor o igual a la fecha de emisi�n.';
    MSG_FECHA_VENCIMIENTO_NO_HABIL      = 'La fecha de vencimiento debe corresponder a un d�a laborable.';
    MSG_FECHA_VENCIMIENTO_POSTERIOR_A_PROGRAMADA  = 'La fecha de vencimiento debe ser menor o igual'#10#13 +
    												' a la pr�xima fecha de vencimiento programada';
begin
 	result := False;
	if deFechaVencimiento.Date = nulldate then begin
		MsgBoxBalloon(MSG_FECHA_VENCIMIENTO_NO_EXISTE, caption, MB_ICONSTOP, deFechaVencimiento);
        exit;
	end;
	if FFechaEmision > deFechaVencimiento.Date then begin
		MsgBoxBalloon(MSG_FECHA_VENCIMIENTO_INCORRECTA, caption, MB_ICONSTOP, deFechaVencimiento);
        exit;
	end;
	if not EsFechaHabil(deFechaVencimiento.date) then begin
		MsgBoxBalloon(MSG_FECHA_VENCIMIENTO_NO_HABIL, caption, MB_ICONSTOP, deFechaVencimiento);
        Exit;   // SS_1006_1015_CQU_20120907
	end;

	if FVencimientoProgramado < deFechaVencimiento.Date then begin
		MsgBoxBalloon(MSG_FECHA_VENCIMIENTO_POSTERIOR_A_PROGRAMADA + FormatDateTime('(dd/mm/yyyy)', FVencimientoProgramado), caption, MB_ICONSTOP, deFechaVencimiento);
        exit;
	end;
   	Result := True;
end;

function TformReemisionNotaCobro.EsFechaHabil(aFecha: TDateTime): Boolean;
begin
    //result := not ((DayOfTheWeek(aFecha) = daySaturday) or (DayOfTheWeek(aFecha) = daySunday));   // SS_1006_1015_CQU_20120907
    Result := EsDiaHabil(DMConnections.BaseCAC, aFecha);                                            // SS_1006_1015_CQU_20120907
end;

end.
