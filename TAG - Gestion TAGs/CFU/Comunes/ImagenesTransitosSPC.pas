unit ImagenesTransitosSPC;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, DMConnection, ImgTypes, SysUtilsCN, UtilDB,
  JPEGPlus, UtilProc, StdCtrls;

type
  TfrmImagenesTransitosSPC = class(TForm)
    pgcImages: TPageControl;
    tsFrontal1: TTabSheet;
    imgFrontal1: TImage;
    tsFrontal2: TTabSheet;
    imgFrontal2: TImage;
    tsContexto1: TTabSheet;
    imgContexto1: TImage;
    tsContexto2: TTabSheet;
    imgContexto2: TImage;
    tsContexto3: TTabSheet;
    imgContexto3: TImage;
    tsContexto4: TTabSheet;
    imgContexto4: TImage;
    tsContexto5: TTabSheet;
    imgContexto5: TImage;
    tsContexto6: TTabSheet;
    imgContexto6: TImage;
    lblSinImgFrontal1: TLabel;
    lblSinImgFrontal2: TLabel;
    lblSinImgContexto1: TLabel;
    lblSinImgContexto2: TLabel;
    lblSinImgContexto3: TLabel;
    lblSinImgContexto4: TLabel;
    lblSinImgContexto5: TLabel;
    lblSinImgContexto6: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
	function MostrarVentanaImagen(AOwner: TObject; NumCorrCA: Int64; RegistrationAccessibility: Int64): Boolean;
  end;

var
  frmImagenesTransitosSPC: TfrmImagenesTransitosSPC;

implementation

{$R *.dfm}

Var
    GVentana: TfrmImagenesTransitosSPC = nil;
    GOwner: TObject = nil;

function TfrmImagenesTransitosSPC.MostrarVentanaImagen(AOwner: TObject; NumCorrCA: Int64; RegistrationAccessibility: Int64): Boolean;
resourcestring
	MSG_NO_IMAGES = 'Im�genes del tr�nsito no disponibles';
var
    lImagen: string;
begin

	GOwner := AOwner;
	if not Assigned(GVentana) then 
    	Application.CreateForm(TfrmImagenesTransitosSPC, GVentana);

    pgcImages.ActivePage := tsFrontal1;
    try
    
        if (RegistrationAccessibility and RegistrationAccessibilityImagen[tiFrontalSPC1]) = RegistrationAccessibilityImagen[tiFrontalSPC1] then begin

            lImagen := QueryGetValue(
                                DMConnections.BaseCAC,
                                Format('SELECT dbo.BOCore_TransitosCAC_RutaCompletaImagenArmar(%d, %d)', [NumCorrCA, RegistrationAccessibilityImagen[tiFrontalSPC1]]));

            imgFrontal1.Picture.LoadFromFile(lImagen);
            if lImagen <> '' then
            	lblSinImgFrontal1.Visible := False;
        end;
        if (RegistrationAccessibility and RegistrationAccessibilityImagen[tiFrontalSPC2]) = RegistrationAccessibilityImagen[tiFrontalSPC2] then begin

            lImagen := QueryGetValue(
                                DMConnections.BaseCAC,
                                Format('SELECT dbo.BOCore_TransitosCAC_RutaCompletaImagenArmar(%d, %d)', [NumCorrCA, RegistrationAccessibilityImagen[tiFrontalSPC2]]));

            imgFrontal2.Picture.LoadFromFile(lImagen);
            if lImagen <> '' then
            	lblSinImgFrontal2.Visible := False;
        end;
        if (RegistrationAccessibility and RegistrationAccessibilityImagen[tiContextoSPC1]) = RegistrationAccessibilityImagen[tiContextoSPC1] then begin

            lImagen := QueryGetValue(
                                DMConnections.BaseCAC,
                                Format('SELECT dbo.BOCore_TransitosCAC_RutaCompletaImagenArmar(%d, %d)', [NumCorrCA, RegistrationAccessibilityImagen[tiContextoSPC1]]));

            imgContexto1.Picture.LoadFromFile(lImagen);
            if lImagen <> '' then
            	lblSinImgContexto1.Visible := False;
        end;
        if (RegistrationAccessibility and RegistrationAccessibilityImagen[tiContextoSPC2]) = RegistrationAccessibilityImagen[tiContextoSPC2] then begin

            lImagen := QueryGetValue(
                                DMConnections.BaseCAC,
                                Format('SELECT dbo.BOCore_TransitosCAC_RutaCompletaImagenArmar(%d, %d)', [NumCorrCA, RegistrationAccessibilityImagen[tiContextoSPC2]]));

            imgContexto2.Picture.LoadFromFile(lImagen);
            if lImagen <> '' then
            	lblSinImgContexto2.Visible := False;
        end;
        if (RegistrationAccessibility and RegistrationAccessibilityImagen[tiContextoSPC3]) = RegistrationAccessibilityImagen[tiContextoSPC3] then begin

            lImagen := QueryGetValue(
                                DMConnections.BaseCAC,
                                Format('SELECT dbo.BOCore_TransitosCAC_RutaCompletaImagenArmar(%d, %d)', [NumCorrCA, RegistrationAccessibilityImagen[tiContextoSPC3]]));

            imgContexto3.Picture.LoadFromFile(lImagen);
            if lImagen <> '' then
            	lblSinImgContexto3.Visible := False;
        end;
        if (RegistrationAccessibility and RegistrationAccessibilityImagen[tiContextoSPC4]) = RegistrationAccessibilityImagen[tiContextoSPC4] then begin

            lImagen := QueryGetValue(
                                DMConnections.BaseCAC,
                                Format('SELECT dbo.BOCore_TransitosCAC_RutaCompletaImagenArmar(%d, %d)', [NumCorrCA, RegistrationAccessibilityImagen[tiContextoSPC4]]));

            imgContexto4.Picture.LoadFromFile(lImagen);
            if lImagen <> '' then
            	lblSinImgContexto4.Visible := False;
        end;
        if (RegistrationAccessibility and RegistrationAccessibilityImagen[tiContextoSPC5]) = RegistrationAccessibilityImagen[tiContextoSPC5] then begin

            lImagen := QueryGetValue(
                                DMConnections.BaseCAC,
                                Format('SELECT dbo.BOCore_TransitosCAC_RutaCompletaImagenArmar(%d, %d)', [NumCorrCA, RegistrationAccessibilityImagen[tiContextoSPC5]]));

            imgContexto5.Picture.LoadFromFile(lImagen);
            if lImagen <> '' then
            	lblSinImgContexto5.Visible := False;
        end;
        if (RegistrationAccessibility and RegistrationAccessibilityImagen[tiContextoSPC6]) = RegistrationAccessibilityImagen[tiContextoSPC6] then begin

            lImagen := QueryGetValue(
                                DMConnections.BaseCAC,
                                Format('SELECT dbo.BOCore_TransitosCAC_RutaCompletaImagenArmar(%d, %d)', [NumCorrCA, RegistrationAccessibilityImagen[tiContextoSPC6]]));

            imgContexto6.Picture.LoadFromFile(lImagen);
            if lImagen <> '' then
            	lblSinImgContexto6.Visible := False;
        end;
        Result := True;
    except
    	Result := False;
		MsgBox(MSG_NO_IMAGES, 'Im�genes Tr�nsito', MB_ICONINFORMATION);
    end;
    
end;

end.
