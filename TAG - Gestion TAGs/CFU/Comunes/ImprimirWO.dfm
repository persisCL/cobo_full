object frmImprimirWO: TfrmImprimirWO
  Left = 387
  Top = 323
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'Opciones de Impresi'#243'n'
  ClientHeight = 148
  ClientWidth = 366
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  DesignSize = (
    366
    148)
  PixelsPerInch = 96
  TextHeight = 13
  object lbl_Mensaje: TLabel
    Left = 45
    Top = 8
    Width = 313
    Height = 17
    Anchors = [akLeft, akTop, akRight]
    AutoSize = False
    Caption = 'Mensaje'
    Constraints.MaxHeight = 73
    Constraints.MaxWidth = 313
    Constraints.MinHeight = 17
    Constraints.MinWidth = 313
    WordWrap = True
  end
  object img: TImage
    Left = 8
    Top = 8
    Width = 32
    Height = 32
    AutoSize = True
    Center = True
    Picture.Data = {
      055449636F6E0000010001002020100001000400E80200001600000028000000
      2000000040000000010004000000000080020000000000000000000000000000
      0000000000000000000080000080000000808000800000008000800080800000
      C0C0C000808080000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000
      FFFFFF0000000000000000000008800000000000000000000000000000888000
      000000000000000000000000000880000000000000000000000000000F088000
      000000000000000000000000FF08800000000000000000000000080FFF088000
      00000000000000000088880FFF08888800000000000000008880007FFF088888
      88000000000000080007FFFFFF70008888800000000000007FFFFFFFFFFFF700
      888800000000007FFFFFFFFFFFFFFFF70888800000000FFFFFFFFFFFFFFFFFFF
      F08888000000FFFFFFFFF7CC7FFFFFFFFF088880008FFFFFFFFFFCCCCFFFFFFF
      FFF08880087FFFFFFFFFFCCCCFFFFFFFFFF7088808FFFFFFFFFFF7CC7FFFFFFF
      FFFF088887FFFFFFFFFFFFFFFFFFFFFFFFFF70888FFFFFFFFFFFFFCCFFFFFFFF
      FFFFF0888FFFFFFFFFFFFFCC7FFFFFFFFFFFF0888FFFFFFFFFFFFFCCCFFFFFFF
      FFFFF0888FFFFFFFFFFFFF7CCCFFFFFFFFFFF0888FFFFFFFFF7CC7FCCCCFFFFF
      FFFFF08087FFFFFFFFCCCCF7CCCCFFFFFFFF708008FFFFFFFFCCCCFFCCCCFFFF
      FFFF0800087FFFFFFFCCFFFFCCCCFFFFFFF70000008FFFFFFF7C7FFCCCC7FFFF
      FFF000000008FFFFFFF7CCCCCC7FFFFFFF00000000008FFFFFFFFFFFFFFFFFFF
      F00000000000087FFFFFFFFFFFFFFFF780000000000000887FFFFFFFFFFFF788
      00000000000000008887FFFFFF78880000000000000000000008888888800000
      00000000FFFFE7FFFFFFC7FFFFFF87FFFFFF07FFFFFE07FFFFF807FFFFC000FF
      FF00003FFE00001FFC00000FF8000007F0000003E0000001C000000180000000
      8000000000000000000000000000000000000000000000000000000100000001
      8000000380000007C000000FE000001FF000003FF800007FFC0000FFFF0003FF
      FFE01FFF}
  end
  object rg1: TRadioGroup
    Left = 40
    Top = 80
    Width = 321
    Height = 33
    Anchors = [akLeft, akTop, akRight, akBottom]
    Ctl3D = True
    ParentCtl3D = False
    TabOrder = 2
  end
  object pn1: TPanel
    Left = 40
    Top = 90
    Width = 321
    Height = 19
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelOuter = bvNone
    TabOrder = 3
    object cbl: TVariantCheckListBox
      Left = 0
      Top = 0
      Width = 321
      Height = 15
      Align = alClient
      BevelInner = bvNone
      BevelOuter = bvNone
      BorderStyle = bsNone
      Color = clBtnFace
      IntegralHeight = True
      ItemHeight = 15
      Items = <
        item
          Caption = 'aaaaa'
        end>
      Style = lbOwnerDrawFixed
      TabOrder = 0
    end
  end
  object btn_Aceptar: TButton
    Left = 204
    Top = 120
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Aceptar'
    TabOrder = 0
    OnClick = btn_AceptarClick
  end
  object btn_Cancelar: TButton
    Left = 287
    Top = 120
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Cancelar'
    TabOrder = 1
    OnClick = btn_CancelarClick
  end
end
