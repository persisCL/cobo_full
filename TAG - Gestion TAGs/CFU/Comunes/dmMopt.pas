{********************************** Unit Header ********************************
File Name : dmMopt.pas
Author : no se quien la hizo pero el 25/09/2006 yo le agregue el header para
         escribir mis cambios (mlopez)
Date Created:
Language : ES-AR
Description :

Revision 2:
    Author : mlopez
    Date Created : 25/09/2006
    Description : Se cambio la unit para que no genere mas los XMLs de entrada
                  de Action Lists. En esta unit comente lo

Revision 3:
    Author : jconcheyro
    Date Created : 08/03/2007
    Description : comento la importacio de listas del RNT porque no se usa mas
        redireccion� la salida del XML a otro par�mtro por ahora, hay que dejarlo
        como estaba. buscar 'PATH_ARCHIVO_SALIDA_XML_SYNCMOPT'

Revision 4:
    Author: nefernandez
    Date: 25/04/2007
    Description : Cambi� el tipo de dato de ContractSerialNumber de Integer a Int64, y
    la conversi�n de AsInteger a AsFloat/AsVariant

Revision 5:
    Author: dAllegretti
    Date: 31/07/2008
    Description : Agregu� el par�metro @Usuario en el stored ActualizarCuentasEnviosRNUT.


Revision 6:
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura

Revision 7:
    Date: 23/04/2009
    Author: nefernandez
    Description:  SS 801: Mediante la llamada al procedimiento "RegistrarLineaLog" se registran
    en un archivo de logs las acciones del servicio. Tambi�n se agreg� al m�todo "VerificarSemaforo" el
    par�metro "ProduceExcepcion" para indicar si se produjo un exception dentro de dicho m�todo.

Revision 8:
    Author : pdominguez
    Date   : 10/09/2009
    Description : SS 801
        - Se modificaron/a�adieron los siguientes objetos/constantes/variables:
            Se a�adieron los par�metros @HostName y @IsService al SP SincroEntrar.

        - Se modificaron/a�adieron los siguientes procedimientos/funciones:
            VerificarSemaforo,
            ObtenerDirectorio

Firma           : SS_1147_MCA_20150519
Descripcion     : el nombre de archivo depende de la concesionaria nativa Entrada_?_ ?= concesionarianativa
*******************************************************************************}
unit dmMopt;
{$IFDEF MSWINDOWS}
  {$WARN SYMBOL_PLATFORM OFF}
{$ENDIF}

interface

uses
  SysUtils, Classes, DB, ADODB, IdFTP, IdBaseComponent, IdComponent, Variants, DMConnection,
  UtilProc, IdTCPConnection, IdTCPClient, IdMessageClient, IdSMTP, RStrings,
  xmldom, msxmldom, XMLDoc, xmlintf, ContenedorXML, Util, UtilDB, PeaTypes, PeaProcs,
  EventLog, ConstParametrosGenerales, dialogs, IdExplicitTLSClientServerBase, LogProcs;

type

  TdmSyncMOPT = class(TDataModule)
    ftp: TIdFTP;
    ContenedorXML: TXMLDocument;
    VerificarContratadoExiste: TADOStoredProc;
    ActualizarVehiculosContratados: TADOStoredProc;
    ObtenerDatosPersona: TADOStoredProc;
    ObtenerVehiculosTAGConvenioRNUT: TADOStoredProc;
    ActualizarConvenioRolesEnviosRNUT: TADOStoredProc;
    ActualizarDireccionesContratados: TADOStoredProc;
    ObtenerNovedadesActionListRNUT: TADOStoredProc;
    SetearReenvio: TADOStoredProc;
    ResetearVariablesReenvioRNUT: TADOStoredProc;
    ActualizarEstadoConvenioEnviosRNUT: TADOStoredProc;
    ActualizarListActionRNUT: TADOStoredProc;
    ObtenerNovedadesTags: TADOStoredProc;
    ResetearAccionRNUTTags: TADOStoredProc;
    ActualizarContratados: TADOStoredProc;
    VerificarVehiculoContratadoExiste: TADOStoredProc;
    VerificarDireccionContratadoExiste: TADOStoredProc;
    VerificarEntidadContradatoExiste: TADOStoredProc;
    ActualizarContactosContratados: TADOStoredProc;
    ActualizarEntidadesContratados: TADOStoredProc;
    ActualizarRespuestaConvenioEnviosRNUT: TADOStoredProc;
    ObtenerNovedadesConveniosRNUT: TADOStoredProc;
    ActualizarCuentasEnviosRNUT: TADOStoredProc;
    AgregarEnvioConvenioRNUT: TADOStoredProc;
    ActualizarEnvioConvenioRNUT: TADOStoredProc;
    ActualizarConvenioEnviosRNUT: TADOStoredProc;
    ObtenerRepresentateLegalRNUT: TADOStoredProc;
    ObtenerDomicilioConvenio: TADOStoredProc;
    ActualizarRecepcionEnviosRNUT: TADOStoredProc;
    ActualizarActionListDesdeNovedadRNUT: TADOStoredProc;
    ActualizarTAGContratados: TADOStoredProc;
    BajasConvenioRNUT: TADOStoredProc;
    AltasConvenioRNUT: TADOStoredProc;
    ModificadosConvenioRNUT: TADOStoredProc;
    ModificadosCuentasRNUT: TADOStoredProc;
    ModificadosRepresentantesRNUT: TADOStoredProc;
    ActualizarConveniosSincronizados: TADOStoredProc;
    ActualizarConvenioSincronizado: TADOStoredProc;
    ObtenerTagPendientes: TADOStoredProc;
    SincroEntrar: TADOStoredProc;
    VerificarRespuestasCargadas: TADOStoredProc;
    ActualizarRespuestaListaEnviosRNUT: TADOStoredProc;
    spActualizarEnvioTelevia: TADOStoredProc;
    ImportarListasRNUT: TADOStoredProc;
    ModificadosDomiciliosRNUT: TADOStoredProc;
    spConvenio_NumeracionXML: TADOStoredProc;
  private
    { Private declarations }
    procedure ActualizarEstadoConvenio(NumeroConvenioRNUT: String);
    procedure ActualizarRespuesta(NumeroConvenioRNUT: String; CodigoEnvioRNUT,
      CodigoRespuesta: integer; Complemento: string);
    procedure ActualizarRespuestaLista(Etiqueta: String; CodigoEnvioRNUT,
      CodigoRespuesta: integer; Complemento: string);
    procedure ActualizarConvenioEnvio(NumeroConvenioRNUT: string; CodigoEnvioConvenio: Integer; Estado: string);
    procedure ActualizarEnvioTelevia(CodigoEnvioRnut: integer; ContractSerialNumber: int64; Color: integer);

//    procedure ProcesarNovedadAutomatica;
    procedure AgregarEnvioConvenio(usuario : string; CodigoEnvio: integer);
    procedure ProcesarEnvioManual;
    //procedure ProcesarEnvioAutomatico;
    function ObtenerDirectorio(parametro: string): string;
    procedure ProcesarRespuestaManual;
    procedure AgregarRepresentantesLegales(CodigoConvenio : longint; EntidadXML,ContratoXML : IXMLNode; CodigoEnvioConvenio : integer; Accion: String); overload;
    procedure AgregarRepresentantesLegales(EntidadXML,ContratoXML : IXMLNode; Accion, NumeroDocumento, dv, Nombre, Apellido, ApellidoMaterno: String); overload;
//    procedure EnviarFTP(Directorio: String);
//    procedure EnviarMail(Directorio: String);
//    function EnviarArchivoMail(Archivo: String) : boolean;
//    function EnviarArchivoFTP(Archivo: String) : boolean;
//    function VerificarArchivoNovedadesAProcesar(Archivo : string) : Boolean;
    procedure CargarParametros;
    function ProcesarRespuestaSinEnvio(Archivo : string) : Boolean;
    function RespuestaYaPrecesadas(CodigoEnvioConvenioRNUT: Integer): Boolean;
  public
    { Public declarations }
    FOnLine : Boolean;
    Fdesde : TDateTime;
    Fhasta : TDateTime;
    FCodigoConvenioDesde, FCodigoConvenioAltas, FcodigoConvenioBajas, FCodigoConvenioModif: Integer;
    //Procesa XML Respuesta a Archivo enviado RNUT.
    function ProcesarRespuesta(Archivo : string) : Boolean;
    //Procesa XML Novedad RNUT.
(*    function Procesar(Archivo : string;
                      const TipoNovedad : string) : Boolean;*)
    //Genera archivo XML de exportaci�n de novedades locales.
    function VerificarSemaforo(Semaforo: string; Entrar: boolean; var ProduceExcepcion: boolean): boolean;
    function ExportarNovedadesRNUT : string;
// Modificado en Revision 2 (las 3 funciones que siguen)
//    function ExportarBajasRNUT: string;
//    function ExportarModifRNUT: string;
//    function ExportarAltasRNUT: string;
    function ExportarAccionListRNUT: string;
    function CambiarEstadoTag: string;
    //function ExportarNovedadesTags: string;

    //Procesa autom�ticamente todas las novedades depositadas en el directorio .
    procedure ProcesarNovedad;
    procedure ProcesarEnvio;
    procedure ProcesarImportacionLista;
    procedure ProcesarRespuestaManualSinEnvio;
  end;

var
  dmSyncMOPT: TdmSyncMOPT;

implementation

uses wsMOPT, MainForm ;

{$R *.dfm}

function TdmSyncMOPT.ProcesarRespuesta(Archivo : string) : boolean;
var
    mensajeTotalRegistros, msjLog: AnsiString;
    Etiqueta, mensajeAux : AnsiString;

    NombreArchivoLog: AnsiString;

    ContenedorXML: IXMLDocument;
    Cantidad,
    CantidadError, CantidadCorrectos: integer;
    AuxNodoConvenio: IXMLNode;


    Convenio : Integer;
    CodigoEnvioRNUT : integer;
    NumeroConvenio : string;
    CodigoRespuesta : integer;
    Complemento : string;

    NombreArchivoOriginal : string;
    ConveniosDistintos : TStringList;
    Log : TStringList;
begin
    Result := False;
    mensajeTotalRegistros := '';
    msjLog := ''; mensajeAux := '';
    NombreArchivoLog := '';

    Convenio := 0; Cantidad := 0; CantidadError := 0; CantidadCorrectos := 0;

    ConveniosDistintos := TStringList.Create;
    Log := TStringList.Create;

    // Archivo de entrada de datos
    ContenedorXML:=CrearXML;
    ContenedorXML.LoadFromFile(Archivo);

    NombreArchivoLog := ExtractFilePath(Archivo) +
                        copy(ExtractFileName(Archivo)
                        , 1, pos('.',ExtractFileName(Archivo)) - 1) + '.log';
{   BLOQUE ORIGINAL
    //Reconstruyo el nombre del archivo original en funcion del nombre del
    //archivo de respuesta.
    NombreArchivoOriginal := 'entrada_' +
      ParseParamByNumber(Archivo,2,'_') + '_' +
      ParseParamByNumber(Archivo,3,'_') + '_' +
      ParseParamByNumber(Archivo,4,'_');
}

//  BLOQUE NUEVO
    NombreArchivoOriginal := 'entrada_' +
      ParseParamByNumber(ExtractFileName(Archivo),2,'_') + '_' +
      ParseParamByNumber(ExtractFileName(Archivo),3,'_') + '_' +
      ParseParamByNumber(ExtractFileName(Archivo),4,'_');
    NombreArchivoOriginal := '%'+NombreArchivoOriginal+'%'; 
//

    CodigoEnvioRNUT := QueryGetValueInt(DMConnections.BaseCAC, format('select dbo.ObtenerCodigoEnvioRNUT (''%s'')',[NombreArchivoOriginal]));

    try
        try
              if CodigoEnvioRNUT = 0 then
                  raise Exception.Create('No se encontr� el envio correspondiente a la respuesta.');

              if RespuestaYaPrecesadas(CodigoEnvioRNUT) then
                raise Exception.Create('Ya se cargo la respuesta de este archivo.');

              //Proceso los contratos

              AuxNodoConvenio := ContenedorXML.DocumentElement.ChildNodes.FindNode(XML_CONTRATO);
              //Verifico si es una respuesta de un XML de convenio o de Listas
              if (AuxNodoConvenio <> nil) then begin
                  while not (AuxNodoConvenio = nil) do begin
                      try
                        NumeroConvenio  := AuxNodoConvenio.ChildNodes[XML_CODIGO].Text;
                        CodigoRespuesta := AuxNodoConvenio.ChildNodes[XML_RESPUESTA].NodeValue;
                        Complemento := AuxNodoConvenio.ChildNodes[XML_COMPLEMENTO].NodeValue;

                        if ConveniosDistintos.IndexOf(NumeroConvenio) < 0 then
                          ConveniosDistintos.Add(NumeroConvenio);

                        ActualizarRespuesta(NumeroConvenio,CodigoEnvioRNUT,CodigoRespuesta,Complemento);

                        //Actualizar estado
                        ActualizarEstadoConvenio(NumeroConvenio);
                        Inc(CantidadCorrectos);
                        if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.CommitTrans;
                      except
                          on E: Exception do begin
                                  if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.RollbackTrans;
                                  msjLog := E.Message;
                                  Inc(CantidadError);
                                  Log.Add('Convenio: ' + Inttostr(Convenio) + ' - Complemento: ' + Complemento + ' - ' + msjLog);
                          end;
                      end;
                      msjLog := '';
                      AuxNodoConvenio := AuxNodoConvenio.NextSibling;
                  end;
                end
                else begin //Es una respuesta de listas
                    AuxNodoConvenio := ContenedorXML.DocumentElement.ChildNodes.FindNode(XML_LISTA);
                    while not (AuxNodoConvenio = nil) do begin
                        try
                          Etiqueta  := AuxNodoConvenio.ChildNodes[XML_SERIE].Text;

                          CodigoRespuesta := AuxNodoConvenio.ChildNodes[XML_RESPUESTA].NodeValue;

                          Complemento := AuxNodoConvenio.ChildNodes[XML_COMPLEMENTO].NodeValue;

                          if ConveniosDistintos.IndexOf(Etiqueta) < 0 then
                            ConveniosDistintos.Add(Etiqueta);

                          //Actualizo la repuesta y el Estado
                          ActualizarRespuestaLista(Etiqueta,CodigoEnvioRNUT,CodigoRespuesta,Complemento);

                          Inc(CantidadCorrectos);
                          if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.CommitTrans;
                        except
                            on E: Exception do begin
                                    if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.RollbackTrans;
                                    msjLog := E.Message;
                                    Inc(CantidadError);
                                    Log.Add('Etiqueta: ' + Trim(Etiqueta) + ' - Complemento: ' + Complemento + ' - ' + msjLog);
                            end;
                        end;
                        msjLog := '';
                        AuxNodoConvenio := AuxNodoConvenio.NextSibling;
                    end;
                end;

              mensajeTotalRegistros := 'Resultados: ' + CRLF +
                '   - Total Rechazos en Archivo :' + #9 + #9+ IntToStr(Cantidad) +  CRLF +
                '   - Total Rechazos Importados Correctamente:' + #9 + #9+ IntToStr(CantidadCorrectos) + CRLF +
                '   - Total Rechazos Importados Con Error:' + #9 + #9 + IntToStr(CantidadError) + CRLF +
                '   - Total Telev�as Rechazados Diferentes :' + #9 + #9 + IntToStr(ConveniosDistintos.Count) + CRLF +
                ' Mensajes Adicionales: ' + mensajeAux;

              Log.Add(mensajeTotalRegistros);
              Log.SaveToFile(NombreArchivoLog);
              FreeAndNil(Log);

              if CantidadError = 0 then begin
                    result := True;
                    EventLogReportEvent(elInformation, 'El archivo se proces� correctamente. ' + CRLF + mensajeTotalRegistros , '');
              end else begin
                    result := False;
                    EventLogReportEvent(elError, 'El proceso se proceso con errores. ' + CRLF + mensajeTotalRegistros +
                                                  CRLF +  CRLF + 'Se gener� el archivo de log ' + NombreArchivoLog, '');
              end;

        except
                on E: Exception do begin
                      if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.RollbackTrans;
                      EventLogReportEvent(elError, 'El proceso grave al procesar el archivo. ' + CRLF + e.Message, 'El proceso grave al procesar el archivo. ' + CRLF + e.Message);
                end;
        end;
    finally
        ConveniosDistintos.Free;
    end;
end;



procedure TDmSyncMopt.ActualizarConvenioEnvio(NumeroConvenioRNUT: string; CodigoEnvioConvenio: Integer; Estado: string);
begin
      with ActualizarConvenioEnviosRNUT do begin
            Parameters.ParamByName('@NumeroConvenioRNUT').Value := NumeroConvenioRNUT;
            Parameters.ParamByName('@CodigoEnvioConvenioRNUT').Value := CodigoEnvioConvenio;
            Parameters.ParamByName('@Estado').Value := Estado[1];
            ExecProc;
      end;

end;

procedure TdmSyncMOPT.ActualizarEstadoConvenio(NumeroConvenioRNUT: string);
begin
        with ActualizarEstadoConvenioEnviosRNUT, ActualizarEstadoConvenioEnviosRNUT.Parameters do begin
            ParamByName('@NumeroConvenioRNUT').Value := NumeroConvenioRNUT;
            ExecProc;
        end;
end;

procedure TdmSyncMOPT.AgregarEnvioConvenio(usuario: string; CodigoEnvio: integer) ;
begin
      RegistrarLineaLog('TdmSyncMOPT.AgregarEnvioConvenio: Inicio ejecucion de SP ' + AgregarEnvioConvenioRNUT.ProcedureName + ' @CodigoEnvioConvenioRNUT=' + IntToStr(CodigoEnvio) + ', @Usuario=' + usuario);
      AgregarEnvioConvenioRNUT.parameters.ParamByName('@CodigoEnvioConvenioRNUT').Value := CodigoEnvio;
      AgregarEnvioConvenioRNUT.parameters.ParamByName('@Usuario').Value := usuario;
      AgregarEnvioConvenioRNUT.ExecProc;
      RegistrarLineaLog('TdmSyncMOPT.AgregarEnvioConvenio: Fin ejecucion de SP ' + AgregarEnvioConvenioRNUT.ProcedureName);
//  antes esto era funcion y devolvia el numero
// ahora le pasamos el numerador del CAC
//      result := AgregarEnvioConvenioRNUT.parameters.ParamByName('@CodigoEnvioConvenioRNUT').Value;
//      if Result < 0 then raise Exception.Create('Error al Obtener Codigo de Envio');
end;


procedure TDmSyncMOPT.ActualizarRespuesta(NumeroConvenioRNUT: String; CodigoEnvioRNUT,CodigoRespuesta: integer; Complemento : string);
begin
    with ActualizarRespuestaConvenioEnviosRNUT, ActualizarRespuestaConvenioEnviosRNUT.Parameters do begin
        ParamByName('@NumeroConvenioRNUT').Value := NumeroConvenioRNUT;
        ParamByName('@CodigoEnvioConvenioRNUT').Value := CodigoEnvioRNUT;
        ParamByName('@CodigoRespuesta').Value := CodigoRespuesta;
        ParamByName('@Complemento').Value := Complemento;
        ExecProc;
    end;
end;


function TDMSyncMOPT.ExportarNovedadesRNUT : string;
var
    XML: IXMLDocument;
    ContenedorXML, ContratoXML, EntidadXML: IXMLNode;
    FileNameRNUT, NumeroConvenio, Personeria, PathXML,
    DigitoVerificadorPatente, ContractSerialNumber: AnsiString;
    CodigoEnvioConvenio, CantTotal, CantidadError,
    CantidadCorrectos, CodigoConvenio: longint;
    FechaEnvio: TDateTime;
    accion, ContextMark, IssuerIdentifier,
    TypeOfContract, ContextVersion,
    PatenteRNUT, DVRNUT: AnsiString;
    ContextMarkRNUT, ContractSerialNumberRNUT : Int64;

    ArchivoXML : string;
    MensajeFinal, MensajesError : string;
    PathXMLEnviado : string;
    MensajeProgreso : string;
begin
    CargarParametros;

    CantTotal:=0; CantidadError := 0; CantidadCorrectos := 0;

    MensajeFinal := ''; MensajesError := '';


    if (Fdesde > 0) and (Fhasta > 0) then begin
        with SetearReenvio, SetearReenvio.Parameters do begin
            ParamByName('@FechaDesde').Value := Fdesde;
            ParamByName('@FechaHasta').Value := Fhasta;
            ExecProc;
        end;
        ResetearVariablesReenvioRNUT.ExecProc;
    end;

    try
        try
            PathXML := ObtenerDirectorio(XML_PARAMETRO_ARCHIVO_XML);
            PathXMLEnviado := ObtenerDirectorio(XML_PATH_ARCHIVO_SALIDA_XML_ENVIADOS);

            with ObtenerNovedadesConveniosRNUT do begin
                repeat
                    MensajeProgreso := 'Obteniendo Novedades Convenios';
                    close;
                    open;

                    if RecordCount = 0 then Exit;
                    if not DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.BeginTrans;

                    FechaEnvio := NowBase(Connection);
                    // Registro de Envio
                    CodigoEnvioConvenio := 0 ; //AgregarEnvioConvenio(usuarioSistema);
                    //no se usa mas. JRC

                    XML := CrearXML;
                    ContenedorXML := CrearContenedor(XML);


                    while (not(EOF)) do begin
                        ContratoXML     := AgregarXMLContrato(ContenedorXML, ObtenerNovedadesConveniosRNUT.fieldbyName('AccionRNUT').AsString, fieldByname('NumeroConvenioRNUT').AsString, '1', NullDate);
                        CodigoConvenio  := ObtenerNovedadesConveniosRNUT.fieldbyName('CodigoConvenio').AsInteger;
                        Inc(CantTotal);

                        MensajeProgreso := 'Actualizando Convenios Envios';

                        if ObtenerNovedadesConveniosRNUT.fieldbyName('AccionRNUT').AsString <> XML_BAJA then begin
                            try
                                //Datos del titular
                                ObtenerDatosPersona.Close;
                                MensajeProgreso := 'Obteniendo Datos Persona';
                                with ObtenerDatosPersona.Parameters do begin
                                    ParamByName('@CodigoPersona').Value:=FieldByName('CodigoCliente').AsInteger;
                                    ParamByName('@CodigoDocumento').Value:=null;
                                    ParamByName('@NumeroDocumento').Value:=null;
                                end;
                                ObtenerDatosPersona.open;
                                if ObtenerDatosPersona.RecordCount<1 then begin
                                    ObtenerDatosPersona.Close;
                                    raise Exception.Create(format(MSG_ERROR_OPENTABLE,['Personas - Convenio Numero: '+NumeroConvenio]));
                                end;
                                Personeria:=ObtenerDatosPersona.FieldByName('Personeria').AsString;
                                NumeroConvenio:=trim(FieldByName('NumeroConvenio').AsString);

                                if ObtenerDatosPersona.FieldByName('Personeria').AsString=PERSONERIA_JURIDICA then begin
                                    EntidadXML:=AgregarXMLEntidad(ContratoXML,
                                      ObtenerNovedadesConveniosRNUT.fieldbyName('AccionRNUT').AsString,
                                      StrLeft(trim(ObtenerDatosPersona.FieldByName('NumeroDocumento').AsString), length(trim(ObtenerDatosPersona.FieldByName('NumeroDocumento').AsString))-1),
                                      StrRight(trim(ObtenerDatosPersona.FieldByName('NumeroDocumento').AsString),1),
                                      XML_PERSONERIA_JURIDICA,
                                      ObtenerDatosPersona.FieldByName('Apellido').AsString,
                                      '',
                                      '',
                                      '',//iif(ObtenerDatosPersona.FieldByName('Personeria').AsString=PERSONERIA_JURIDICA,ObtenerDatosPersona.FieldByName('Apellido').AsString, ''), //Nombre Fantasia
                                      XML_ROL_CLIENTE,
                                      ObtenerDatosPersona.FieldByName('Giro').AsString);

                                     //Representates Legales
                                     MensajeProgreso := 'Obteniendo Representante';
//Pablo                                     AgregarRepresentantesLegales(CodigoConvenio, EntidadXML, ContratoXML, CodigoEnvioConvenio);
                                end else begin
                                    EntidadXML:=AgregarXMLEntidad(ContratoXML,
                                      ObtenerNovedadesConveniosRNUT.fieldbyName('AccionRNUT').AsString,
                                      StrLeft(trim(ObtenerDatosPersona.FieldByName('NumeroDocumento').AsString), length(trim(ObtenerDatosPersona.FieldByName('NumeroDocumento').AsString))-1),
                                      StrRight(trim(ObtenerDatosPersona.FieldByName('NumeroDocumento').AsString),1),
                                      XML_PERSONERIA_NATURAL,
                                      ObtenerDatosPersona.FieldByName('Nombre').AsString,
                                      ObtenerDatosPersona.FieldByName('Apellido').AsString,
                                      ObtenerDatosPersona.FieldByName('ApellidoMaterno').AsString,
                                      '',//iif(ObtenerDatosPersona.FieldByName('Personeria').AsString=PERSONERIA_JURIDICA,ObtenerDatosPersona.FieldByName('Apellido').AsString, ''), //Nombre Fantasia
                                      XML_ROL_CLIENTE,
                                      ObtenerDatosPersona.FieldByName('Giro').AsString);
                                end;

                                ObtenerDatosPersona.close;

                                MensajeProgreso := 'Obteniendo Domicilios Convenios';
                                //Domicilio
                                ObtenerDomicilioConvenio.Close;
                                with ObtenerDomicilioConvenio.Parameters do begin
                                    ParamByName('@CodigoConvenio').Value:= CodigoConvenio;
                                end;
                                ObtenerDomicilioConvenio.open;

                                if ObtenerDomicilioConvenio.RecordCount<1 then begin
                                    ObtenerDomicilioConvenio.Close;
                                    raise Exception.Create(format(MSG_ERROR_OPENTABLE,['No se encontr� el domicilio del convenio - Convenio Numero: '+NumeroConvenio]));
                                end;

                                if not(ObtenerDomicilioConvenio.Eof) then begin
                                    AgregarXMLDomicilio(ContratoXML,
                                      ObtenerNovedadesConveniosRNUT.fieldbyName('AccionRNUT').AsString,
                                      trim(ObtenerDomicilioConvenio.fieldByName('CodigoDomicilio').AsString),
                                      XML_DIRECCION_FACTURACION,
                                      iif(not ObtenerDomicilioConvenio.fieldByName('Normalizado').AsBoolean,
                                              ObtenerDomicilioConvenio.fieldByName('DescriCalle').AsString,
                                              ObtenerDomicilioConvenio.fieldByName('CalleDesnormalizada').AsString),
                                      ObtenerDomicilioConvenio.fieldByName('DescriComuna').AsString,
                                      ObtenerDomicilioConvenio.fieldByName('Numero').AsString,
                                      '',
                                      ObtenerDomicilioConvenio.fieldByName('CodigoPostal').AsString,
                                      ObtenerDomicilioConvenio.fieldByName('Detalle').AsString);
                                end;
                                ObtenerDomicilioConvenio.close;

                                MensajeProgreso := 'Obteniendo Vehiculos Tags';

                                // Vehiculo
                                ObtenerVehiculosTAGConvenioRNUT.Close;
                                with ObtenerVehiculosTAGConvenioRNUT.Parameters do begin
                                     ParamByName('@CodigoConvenio').Value:= CodigoConvenio;
                                end;
                                ObtenerVehiculosTAGConvenioRNUT.Open;
                                while not(ObtenerVehiculosTAGConvenioRNUT.Eof) do begin
                                    DigitoVerificadorPatente := ''; ContractSerialNumber := ''; ContextMark := ''; PatenteRNUT := '';
                                    DVRNUT := ''; IssuerIdentifier := ''; TypeOfContract := ''; ContextVersion := '';

                                    if (ObtenerVehiculosTAGConvenioRNUT.FieldByName('DigitoVerificadorPatente').IsNull) or
                                       (ObtenerVehiculosTAGConvenioRNUT.FieldByName('DigitoVerificadorPatente').AsString = '') then begin
                                        DigitoVerificadorPatente := QueryGetValue(DMConnections.BaseCAC, format('Exec ObtenerDigitoVerificadorPatente ''%s''',[ trim(ObtenerVehiculosTAGConvenioRNUT.FieldByName('Patente').AsString)]));
                                    end
                                    else DigitoVerificadorPatente := trim(ObtenerVehiculosTAGConvenioRNUT.FieldByName('DigitoVerificadorPatente').AsString);
                                    ContractSerialNumber := IIf(ObtenerVehiculosTAGConvenioRNUT.FieldByName('ContractSerialNumber').IsNull,'', ObtenerVehiculosTAGConvenioRNUT.FieldByName('ContractSerialNumber').AsString);

                                    ContextMark := IIf(ObtenerVehiculosTAGConvenioRNUT.FieldByName('ContextMark').IsNull,'', ObtenerVehiculosTAGConvenioRNUT.FieldByName('ContextMark').AsString);
                                    IssuerIdentifier := IIf(ObtenerVehiculosTAGConvenioRNUT.FieldByName('IssuerIdentifier').IsNull,'', ObtenerVehiculosTAGConvenioRNUT.FieldByName('IssuerIdentifier').AsString);

                                    //Forzado a 1. 15-09-2004--
                                    TypeOfContract := '1'; // iif (ObtenerVehiculosTAGConvenioRNUT.FieldByName('TypeOfContract').IsNull,'', ObtenerVehiculosTAGConvenioRNUT.FieldByName('TypeOfContract').AsString);
                                    ContextVersion := '1'; //iif (ObtenerVehiculosTAGConvenioRNUT.FieldByName('ContextVersion').IsNull,'', ObtenerVehiculosTAGConvenioRNUT.FieldByName('ContextVersion').AsString);


                                    PatenteRNUT := IIf(ObtenerVehiculosTAGConvenioRNUT.FieldByName('Patente').IsNull,'', trim(ObtenerVehiculosTAGConvenioRNUT.FieldByName('Patente').AsString));
                                    DVRNUT      := IIf(ObtenerVehiculosTAGConvenioRNUT.FieldByName('DigitoVerificadorPatente').IsNull,'', trim(ObtenerVehiculosTAGConvenioRNUT.FieldByName('DigitoVerificadorPatente').AsString));

(*Pablo
                                    PatenteRNUT := IIf(ObtenerVehiculosTAGConvenioRNUT.FieldByName('PatenteRNUT').IsNull,'', trim(ObtenerVehiculosTAGConvenioRNUT.FieldByName('PatenteRNUT').AsString));
                                    DVRNUT      := IIf(ObtenerVehiculosTAGConvenioRNUT.FieldByName('DigitoVerificadorPatenteRNUT').IsNull,'', trim(ObtenerVehiculosTAGConvenioRNUT.FieldByName('DigitoVerificadorPatenteRNUT').AsString));
*)

                                    Accion := ObtenerVehiculosTAGConvenioRNUT.fieldbyName('AccionRNUT').AsString;

                                    MensajeProgreso := 'Actualizando Cuentas Envios';
                                    with ActualizarCuentasEnviosRNUT do begin
                                        Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;//ObtenerNovedadesConveniosRNUT.FieldByName('CodigoConvenio').AsInteger;
                                        Parameters.ParamByName('@IndiceVehiculo').Value := ObtenerVehiculosTAGConvenioRNUT.FieldByName('IndiceVehiculo').AsInteger;
                                        Parameters.ParamByName('@CodigoEnvioConvenioRNUT').Value := CodigoEnvioConvenio;
                                        Parameters.ParamByName('@AccionRNUT').Value := ObtenerVehiculosTAGConvenioRNUT.FieldByName('AccionRNUT').AsInteger;
                                        Parameters.ParamByName('@ContextMark').Value := iif(ContextMark='', null, ContextMark);
                                        Parameters.ParamByName('@ContractSerialNumber').Value := iif(ContractSerialNumber = '', null, ContractSerialNumber);
                                        Parameters.ParamByName('@Patente').Value := ObtenerVehiculosTAGConvenioRNUT.FieldByName('Patente').AsString;
                                        Parameters.ParamByName('@DigitoVerificadorPatente').Value := ObtenerVehiculosTAGConvenioRNUT.FieldByName('DigitoVerificadorPatente').AsString;
                                        Parameters.ParamByName('@Usuario').Value := UsuarioSistema;    //Revision 5
                                        ExecProc;
                                    end;
                                    ObtenerVehiculosTAGConvenioRNUT.Next;
                                end;
                                ObtenerVehiculosTAGConvenioRNUT.close;

                                Inc(CantidadCorrectos);
                            except
                                on E: Exception do begin
                                    MensajesError := MensajesError + 'Error procesando Convenio: ' + IntToStr(CodigoConvenio) + ' ' +
                                                                     E.Message + CRLF;
                                    Inc(CantidadError);
                                end;
                            end;
                        end
                        else begin

                            //como es baja de convenio le cambio la accionRNUT a todas sus cuentas
                            try
                                ObtenerVehiculosTAGConvenioRNUT.Close;
                                with ObtenerVehiculosTAGConvenioRNUT.Parameters do begin
                                     ParamByName('@CodigoConvenio').Value:= CodigoConvenio;
                                end;
                                ObtenerVehiculosTAGConvenioRNUT.Open;
                                while not(ObtenerVehiculosTAGConvenioRNUT.Eof) do begin
                                    with ActualizarCuentasEnviosRNUT do begin
                                        Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;//ObtenerNovedadesConveniosRNUT.FieldByName('CodigoConvenio').AsInteger;
                                        Parameters.ParamByName('@IndiceVehiculo').Value := ObtenerVehiculosTAGConvenioRNUT.FieldByName('IndiceVehiculo').AsInteger;
                                        Parameters.ParamByName('@CodigoEnvioConvenioRNUT').Value := CodigoEnvioConvenio;
                                        Parameters.ParamByName('@AccionRNUT').Value := ObtenerVehiculosTAGConvenioRNUT.FieldByName('AccionRNUT').AsInteger;
                                        Parameters.ParamByName('@ContextMark').Value := ObtenerVehiculosTAGConvenioRNUT.FieldByName('ContextMark').AsInteger;
                                        Parameters.ParamByName('@ContractSerialNumber').Value := ObtenerVehiculosTAGConvenioRNUT.FieldByName('ContractSerialNumber').AsFloat; // Revision 4
                                        Parameters.ParamByName('@Patente').Value := ObtenerVehiculosTAGConvenioRNUT.FieldByName('Patente').AsString;
                                        Parameters.ParamByName('@DigitoVerificadorPatente').Value := ObtenerVehiculosTAGConvenioRNUT.FieldByName('DigitoVerificadorPatente').AsString;
                                        Parameters.ParamByName('@Usuario').Value := 'MOPT Service';    //Revision 5
                                        ExecProc;
                                    end;
                                    ObtenerVehiculosTAGConvenioRNUT.Next;
                                end;
                            except
                                on E: Exception do begin
                                    MensajesError := MensajesError + 'Error procesando Convenio: ' + IntToStr(CodigoConvenio) + ' ' +
                                                                     E.Message + CRLF;
                                    Inc(CantidadError);
                                end;
                            end;
                        end;
                        // fin
                        Next;
                    end;


                    MensajeFinal := 'Exportaci�n Terminada. ' + CRLF + 'Resultados: ' + CRLF +
                        ' - Registros exportados correctamente: ' + IntToStr(CantidadCorrectos) + CRLF +
                        ' - Registros con error (sin exportar): ' + IntToStr(CantidadError) + CRLF +
                        ' - Total de registros: ' + IntToStr(CantTotal);



                    if CantidadError = 0 then begin
                        // Grabar
                        FileNameRNUT := 'entrada_' + IntToStr(ObtenerCodigoConcesionariaNativa()) + '_' + IntToStr(CodigoEnvioConvenio) + '_' + FormatDateTime('yyyymmddhhnnss', FechaEnvio);       //SS_1147_MCA_20150519
                        ArchivoXML :=  GoodFileName(PathXML + FileNameRNUT, '.xml');
                        GrabarXML(XML, ContenedorXML, ArchivoXML);
                        // Actualizamos la fecha de envio

                        MensajeProgreso := 'Actualizando Envio Convenio';
                        ActualizarEnvioConvenioRNUT.Parameters.ParamByName('@CodigoEnvioConvenioRNUT').Value := CodigoEnvioConvenio;
                        ActualizarEnvioConvenioRNUT.Parameters.ParamByName('@NombreArchivo').Value := FileNameRNUT;
                        ActualizarEnvioConvenioRNUT.execProc;
                        DMConnections.BaseCAC.CommitTrans;


                        result := ArchivoXML;

                        (*
                            if EnviarArchivoMail(ArchivoXML) or
                            EnviarArchivoFTP(ArchivoXML) then
                            RenameFile(ArchivoXML, PathXMLEnviado + ExtractFileName(ArchivoXML));
                        *)

                        EventLogReportEvent(elInformation, MensajeFinal, '');

                    end else begin
                        result := '';
                        DMConnections.BaseCAC.RollbackTrans;
                        EventLogReportEvent(elError, MensajeFinal + CRLF + MensajesError, '');
                    end;
                until ObtenerNovedadesConveniosRNUT.RecordCount = 0;
            end; //with ObtenerNovedadesConveniosRNUT
        except
            on E: Exception do begin
                if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.RollbackTrans;
                EventLogReportEvent(elError, E.message + CRLF + MensajeProgreso, '');
            end;
        end;
    finally
            ObtenerNovedadesConveniosRNUT.Close;
    end;
end;


function TDMSyncMOPT.ExportarAccionListRNUT : string;
var
    XML: IXMLDocument;
    ContenedorXML, ColorXML, TAGXML : IXMLNode;
    FileNameRNUT, PathXML : AnsiString;
    CodigoEnvioConvenio, CantTotal, CantidadError,
    CantidadCorrectos : Integer;
    FechaEnvio: TDateTime;

    ArchivoXML : string;
    MensajeFinal, MensajesError : string;
    PathXMLEnviado : string;
    MensajeProgreso : string;

    AccionActual, ColorActual : Integer;
    FechaActual : string;
begin
    RegistrarLineaLog('TdmSyncMOPT.ExportarAccionListRNUT: Inicio');
    CantTotal:=0; CantidadError := 0; CantidadCorrectos := 0;

    MensajeFinal := ''; MensajesError := '';

    try
        try
            RegistrarLineaLog('TdmSyncMOPT.ExportarAccionListRNUT: Obtener Directorio PATH_ARCHIVO_SALIDA_XML_SYNCMOPT');
            PathXML := ObtenerDirectorio('PATH_ARCHIVO_SALIDA_XML_SYNCMOPT');
            RegistrarLineaLog('TdmSyncMOPT.ExportarAccionListRNUT: PATH_ARCHIVO_SALIDA_XML_SYNCMOPT=' + PathXML);
            // deberia quedar asi PathXML := ObtenerDirectorio(XML_PARAMETRO_ARCHIVO_XML);
            // para que queden unificadas las salidas
            // rev 3


            RegistrarLineaLog('TdmSyncMOPT.ExportarAccionListRNUT: Obtener Directorio ' + XML_PATH_ARCHIVO_SALIDA_XML_ENVIADOS);
            PathXMLEnviado := ObtenerDirectorio(XML_PATH_ARCHIVO_SALIDA_XML_ENVIADOS);
            RegistrarLineaLog('TdmSyncMOPT.ExportarAccionListRNUT: ' + XML_PATH_ARCHIVO_SALIDA_XML_ENVIADOS + '=' + PathXMLEnviado);

//            with ObtenerNovedadesActionListRNUT do begin
                //Este repeat es por si se quiere usar un top en el ObtenerNovedadesActionListRNUT
//                repeat
                    MensajeProgreso := 'Obteniendo Novedades de Accion List';
                    RegistrarLineaLog('TdmSyncMOPT.ExportarAccionListRNUT: Inicio ejecucion de SP ' + ObtenerNovedadesActionListRNUT.ProcedureName);
                    ObtenerNovedadesActionListRNUT.Close;
                    ObtenerNovedadesActionListRNUT.Open;
                    RegistrarLineaLog('TdmSyncMOPT.ExportarAccionListRNUT: FIN ejecucion de SP ' + ObtenerNovedadesActionListRNUT.ProcedureName);

                    if ObtenerNovedadesActionListRNUT.Eof then begin
                        RegistrarLineaLog('TdmSyncMOPT.ExportarAccionListRNUT: SP ' + ObtenerNovedadesActionListRNUT.ProcedureName + ' no retorna registros');
                        Exit;
                    end;
                    //Porque al sacar el repeat, la creacion del XML se hacia aun cuando la query venia vacia.


                    //if RecordCount = 0 then Exit;
                    RegistrarLineaLog('TdmSyncMOPT.ExportarAccionListRNUT: Inicio de Transaccion');
                    if not DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.BeginTrans;

                    FechaEnvio := NowBase(ObtenerNovedadesActionListRNUT.Connection);

                    // Registro de Envio, ahora con el numerador del CAC
                    RegistrarLineaLog('TdmSyncMOPT.ExportarAccionListRNUT: Inicio ejecucion de SP ' + spConvenio_NumeracionXML.ProcedureName);
                    spConvenio_NumeracionXML.Parameters.ParamByName('@Numeracion').Value := Null;
                    spConvenio_NumeracionXML.ExecProc;
                    CodigoEnvioConvenio := spConvenio_NumeracionXML.Parameters.ParamByName('@Numeracion').Value;
                    RegistrarLineaLog('TdmSyncMOPT.ExportarAccionListRNUT: Fin ejecucion de SP, resultado @Numeracion=' + IntToStr(CodigoEnvioConvenio));

                    //CodigoEnvioConvenio := AgregarEnvioConvenio(usuarioSistema);
                    // rev 3 , lo unificamos con el store del CAC
                    // ahora queda asi
                    AgregarEnvioConvenio(usuarioSistema, CodigoEnvioConvenio);


                    RegistrarLineaLog('TdmSyncMOPT.ExportarAccionListRNUT: Se crea el objeto XML ');
                    XML := CrearXML;
                    RegistrarLineaLog('TdmSyncMOPT.ExportarAccionListRNUT: Se crea el objeto del Contenedor XML ');
                    ContenedorXML := CrearContenedor(XML);

                    RegistrarLineaLog('TdmSyncMOPT.ExportarAccionListRNUT: Inicio construccion XML ');
                    while (not ObtenerNovedadesActionListRNUT.EOF) and (CantidadError = 0) do begin
                        AccionActual := -1;
                        ColorActual := -1;
                        try
                                Inc(CantTotal);
                                ColorActual  := ObtenerNovedadesActionListRNUT.FieldByName('color').AsInteger;
                                AccionActual := ObtenerNovedadesActionListRNUT.FieldByName('accionRNUT').AsInteger;
                                FechaActual	 := FormatDateTime('ddmmyyyy hhnnss', ObtenerNovedadesActionListRNUT.FieldByName('FechaLista').AsDateTime);
                                ColorXML := AgregarXMLColorAccion(ContenedorXML,ColorActual,AccionActual,FechaActual);

                                RegistrarLineaLog('TdmSyncMOPT.ExportarAccionListRNUT: Inicio llamada a AgregarXMLTagLista');
                                TAGXML := AgregarXMLTagLista(ColorXML,
                                                             SerialNumberToEtiqueta(ObtenerNovedadesActionListRNUT.FieldByName('ContractSerialNumber').AsString),
                                                             ObtenerNovedadesActionListRNUT.FieldByName('fabricante').AsInteger);
                                RegistrarLineaLog('TdmSyncMOPT.ExportarAccionListRNUT: Fin llamada a AgregarXMLTagLista');

                                ActualizarEnvioTelevia(CodigoEnvioConvenio, ObtenerNovedadesActionListRNUT.FieldByName('ContractSerialNumber').AsVariant, ObtenerNovedadesActionListRNUT.FieldByName('color').AsInteger); // Revision 4
                                Inc(CantidadCorrectos);

                        except
                                on E: Exception do begin
                                    MensajesError := MensajesError + 'Error procesando Lista: Color - ' + IntToStr(ColorActual) +
                                                                     '  Accion - ' + IntToStr(AccionActual) +
                                                                     '  Tag - ' + SerialNumberToEtiqueta(ObtenerNovedadesActionListRNUT.FieldByName('ContractSerialNumber').AsString) + CRLF +
                                                                     E.Message;
                                    Inc(CantidadError);
                                end;
                        end;

                        ObtenerNovedadesActionListRNUT.Next;
                    end;
                    RegistrarLineaLog('TdmSyncMOPT.ExportarAccionListRNUT: Fin construccion XML ');

                    MensajeFinal := 'Exportaci�n Lista Terminada. ' + CRLF + 'Resultados: ' + CRLF +
                        ' - Registros exportados correctamente: ' + IntToStr(CantidadCorrectos) + CRLF +
                        ' - Registros con error (sin exportar): ' + IntToStr(CantidadError) + CRLF +
                        ' - Total de registros: ' + IntToStr(CantTotal);

                    if CantidadError = 0 then begin
                        // Grabar
                        RegistrarLineaLog('TdmSyncMOPT.ExportarAccionListRNUT: Inicio generacion de archivo XML');
                        FileNameRNUT := 'entrada_' + IntToStr(ObtenerCodigoConcesionariaNativa()) + '_' + IntToStr(CodigoEnvioConvenio) + '_' + FormatDateTime('yyyymmddhhnnss', FechaEnvio);           //SS_1147_MCA_20150519
                        ArchivoXML :=  GoodFileName(PathXML + FileNameRNUT, '.xml');
                        GrabarXML(XML, ContenedorXML, ArchivoXML);
                        RegistrarLineaLog('TdmSyncMOPT.ExportarAccionListRNUT: Se graba el archivo: ' + ArchivoXML);
                        // Actualizamos la fecha de envio

                        MensajeProgreso := 'Actualizando Envio Action List';
                        RegistrarLineaLog('TdmSyncMOPT.ExportarAccionListRNUT: Inicio ejecucion de SP ' + ActualizarEnvioConvenioRNUT.ProcedureName + ' @CodigoEnvioConvenioRNUT=' + IntToStr(CodigoEnvioConvenio) + ', @NombreArchivo=' + FileNameRNUT);
                        ActualizarEnvioConvenioRNUT.Parameters.ParamByName('@CodigoEnvioConvenioRNUT').Value := CodigoEnvioConvenio;
                        ActualizarEnvioConvenioRNUT.Parameters.ParamByName('@NombreArchivo').Value := FileNameRNUT;
                        ActualizarEnvioConvenioRNUT.execProc;
                        RegistrarLineaLog('TdmSyncMOPT.ExportarAccionListRNUT: Fin ejecucion de SP ' + ActualizarEnvioConvenioRNUT.ProcedureName);

                        //Actualiza los ya enviados.
                        //ActualizarListActionRNUT.ExecProc;

                        DMConnections.BaseCAC.CommitTrans;
                        RegistrarLineaLog('TdmSyncMOPT.ExportarAccionListRNUT: COMMIT de Transaccion');

                        result := ArchivoXML;

                        (*
                        if EnviarArchivoMail(ArchivoXML) or
                            EnviarArchivoFTP(ArchivoXML) then
                            RenameFile(ArchivoXML, PathXMLEnviado + ExtractFileName(ArchivoXML));
                        *)

                        EventLogReportEvent(elInformation, MensajeFinal, '');
                        RegistrarLineaLog('TdmSyncMOPT.ExportarAccionListRNUT: Fin generacion de archivo XML');

                    end else begin
                        result := '';
                        DMConnections.BaseCAC.RollbackTrans;
                        RegistrarLineaLog('TdmSyncMOPT.ExportarAccionListRNUT: ROLLBACK de Transaccion');
                        EventLogReportEvent(elError, MensajeFinal + CRLF + MensajesError, '');
                    end;
//                until ObtenerNovedadesActionListRNUT.RecordCount = 0;
//            end;
        except
            on E: Exception do begin
                if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.RollbackTrans;
                EventLogReportEvent(elError, E.message + CRLF + MensajeProgreso, '');
            end;
        end;
    finally
            ObtenerNovedadesConveniosRNUT.Close;
            RegistrarLineaLog('TdmSyncMOPT.ExportarAccionListRNUT: Fin');
    end;

end;


{******************************** Function Header ******************************
Function Name: TDMSyncMOPT.ExportarNovedadesTags
Author       : cchiappero
Date Created : 20/09/2004
Description  : Genera novedades de tags independientemente de cuentas.
Parameters   : None
Return Value : string
*******************************************************************************}
(*function TDMSyncMOPT.ExportarNovedadesTags : string;
var
    XML: IXMLDocument;
    ContenedorXML, TAGXML : IXMLNode;
    FileNameRNUT, PathXML : AnsiString;
    CodigoEnvioConvenio, CantTotal, CantidadError,
    CantidadCorrectos : Integer;
    FechaEnvio: TDateTime;

    ArchivoXML : string;
    MensajeFinal, MensajesError : string;
    PathXMLEnviado : string;
    MensajeProgreso : string;
begin
    CantTotal:=0; CantidadError := 0; CantidadCorrectos := 0;

    MensajeFinal := ''; MensajesError := '';

    try
        try
            PathXML := ObtenerDirectorio(XML_PARAMETRO_ARCHIVO_XML);
            PathXMLEnviado := ObtenerDirectorio(XML_PATH_ARCHIVO_SALIDA_XML_ENVIADOS);

            with ObtenerNovedadesTags do begin
                //Este repeat es por si se quiere limitar la cantidad de tags en cada archivo
                // utilizando top n.
                repeat
                    MensajeProgreso := 'Obteniendo Novedades de Tags';
                    close;
                    open;

                    if RecordCount = 0 then Exit;
                    if not DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.BeginTrans;

                    FechaEnvio := NowBase(Connection);

                    // Registro de Envio
                    CodigoEnvioConvenio := AgregarEnvioConvenio(usuarioSistema);

                    XML := CrearXML;
                    ContenedorXML := CrearContenedor(XML);


                    while (not(EOF)) do begin
                        try

                                TAGXML := AgregarXMLTagEstado(  ContenedorXML,
                                                                FieldByName('Accion').AsInteger,
                                                                SerialNumberToEtiqueta(FieldByName('ContractSerialNumber').AsString),
                                                                FieldByName('fabricante').AsInteger,
                                                                FieldByName('Estado').AsInteger);

                                ResetearAccionRNUTTags.Parameters.ParamByName('@ContextMark').Value := FieldByName('ContextMark').AsInteger;
                                ResetearAccionRNUTTags.Parameters.ParamByName('@ContractSerialNumber').Value := FieldByName('ContractSerialNumber').AsInteger;
                                ResetearAccionRNUTTags.ExecProc;

                                Inc(CantidadCorrectos);
                        except
                                on E: Exception do begin
                                    MensajesError := MensajesError + 'Error procesando novedades de tags: ' + CRLF +
                                                                     '  Accion - ' + FieldByName('Accion').AsString +
                                                                     '  Tag - ' + SerialNumberToEtiqueta(FieldByName('ContractSerialNumber').AsString) + CRLF +
                                                                     E.Message;
                                    Inc(CantidadError);
                                end;
                        end;

                        Next;
                    end;



                    MensajeFinal := 'Exportaci�n Novedades Estado Tags Terminada. ' + CRLF + 'Resultados: ' + CRLF +
                        ' - Registros exportados correctamente: ' + IntToStr(CantidadCorrectos) + CRLF +
                        ' - Registros con error (sin exportar): ' + IntToStr(CantidadError) + CRLF +
                        ' - Total de registros: ' + IntToStr(CantTotal);

                    if CantidadError = 0 then begin
                        // Grabar
                        FileNameRNUT := 'entrada_1_' + IntToStr(CodigoEnvioConvenio) + '_' + FormatDateTime('yyyymmddhhnnss', FechaEnvio);
                        ArchivoXML :=  GoodFileName(PathXML + FileNameRNUT, '.xml');
                        GrabarXML(XML, ContenedorXML, ArchivoXML);
                        // Actualizamos la fecha de envio

                        MensajeProgreso := 'Actualizando Envio Estados Tags';
                        ActualizarEnvioConvenioRNUT.Parameters.ParamByName('@CodigoEnvioConvenioRNUT').Value := CodigoEnvioConvenio;
                        ActualizarEnvioConvenioRNUT.Parameters.ParamByName('@NombreArchivo').Value := FileNameRNUT;
                        ActualizarEnvioConvenioRNUT.execProc;

                        //Actualiza los ya enviados.
                        ActualizarListActionRNUT.ExecProc;

                        DMConnections.BaseCAC.CommitTrans;

                        result := ArchivoXML;

                        if EnviarArchivoMail(ArchivoXML) or
                            EnviarArchivoFTP(ArchivoXML) then
                            RenameFile(ArchivoXML, PathXMLEnviado + ExtractFileName(ArchivoXML));

                        EventLogReportEvent(elInformation, MensajeFinal, '');

                    end else begin
                        result := '';
                        DMConnections.BaseCAC.RollbackTrans;
                        EventLogReportEvent(elError, MensajeFinal + CRLF + MensajesError, '');
                    end;
                until ObtenerNovedadesTags.RecordCount = 0;
            end;
        except
            on E: Exception do begin
                if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.RollbackTrans;
                EventLogReportEvent(elError, E.message + CRLF + MensajeProgreso, '');
            end;
        end;
    finally
            ObtenerNovedadesTags.Close;
    end;

end;
*)


{******************************** Function Header ******************************
Function Name: TdmSyncMOPT.ProcesarNovedadAutomatica
Author       : cchiappero
Date Created : 18/08/2004
Description  : Procesa Novedad via SOAP.
Parameters   : None
Return Value : None
*******************************************************************************}
(*
procedure TdmSyncMOPT.ProcesarNovedadAutomatica;
var
 UltimaFechaEnvio : TDateTime;
 respuesta : string;
 FechaEnvio : TDateTime;
 ArchivoSalida : string;
 PathXML : string;
begin
  try

        PathXML := ObtenerDirectorio(XML_PATH_ARCHIVO_ENTRADA_XML);

        //Obtengo la ultima fecha de una novedad autom�tica
        UltimaFechaEnvio := QueryGetValueDateTime(DMConnections.BaseCAC, 'select max(FechaRecepcionRNUT) from RecepcionConvenioRNUT where TipoRecepcion=''' + XML_NOVEDAD_AUTOMATICA + '''');
        //Fecha actual
        FechaEnvio := NowBase(DMConnections.BaseCAC);
        //Consulto el ws por el intervalo de fechas
        respuesta := GetConsultaContratos(UltimaFechaEnvio,FechaEnvio);

        //La novedad que genera no tiene numero de concesionaria ni secuencia solo fechas de envio.
        ArchivoSalida := PathXML + '\novedad_X_1_' + FormatDateTime('yyyymmddhhnnss', FechaEnvio)
                      + '_' + FormatDateTime('yyyymmddhhnnss', FechaEnvio) + '.xml';

        //Genero el archivo emulando el proceso manual.
        StringToFile(respuesta, ArchivoSalida);

        ProcesarRespuesta(ArchivoSalida);
  except
         //FIXME --- controlar la excepcion del envio recepcion de la novedad.
      on E: Exception do  EventLogReportEvent(elError, E.Message, '');
  end;

end;
  *)
procedure TdmSyncMOPT.ProcesarNovedad;
begin
    //Proceso de novedad de los archivos ya generados y depositados en
    // XML_PATH_....
//    ProcesarNovedadManual;
    //Peticion de novedades via SOAP;
    //if FOnLine then ProcesarNovedadAutomatica;
end;


procedure TdmSyncMOPT.ProcesarEnvio;
begin
    //Proceso todas las respuestas que devueltas por el RNUT
    //en respuesta de procesos en envio manual ProcesarNovedadManual
    //Lo hago antes del envio para enterarme primero de los posibles rechazos.


    //ProcesarRespuestaManual; comentado en rev 3
    RegistrarLineaLog('TdmSyncMOPT.ProcesarEnvio: Llamando a ProcesarEnvioManual');
    ProcesarEnvioManual;
end;


{******************************** Function Header ******************************
Function Name: TdmSyncMOPT.ProcesarEnvioAutomatico
Author       : cchiappero
Date Created : 18/08/2004
Description  : Envia actualizacion via SOAP.
Parameters   : None
Return Value : None
Todo         : TERMINARLO.... control de errores.. mover archivos...etc
*******************************************************************************}
(*
procedure TdmSyncMOPT.ProcesarEnvioAutomatico;
var
 ArchivoSalida : string;
 ArchivoEntrada : string;
 Respuesta : string;
begin


  try
      //Fixme.. controlar los errores y la finalizaci�n de esto. y todo

      //Archivo de salida con formato entrada_
      //Si el nombre de entrada es = '' es que contiene errores.
      ArchivoEntrada := dmSyncMOPT.ExportarNovedadesRNUT;

      //Envia las novedades locales y recibe respuesta de resultado de proceso.
      Respuesta := SetProcesarContrato(FileToString(ArchivoEntrada));

      ArchivoSalida := 'salida_1_' + ParseParamByNumber(ArchivoEntrada,3,'_')
                             + '_' + ParseParamByNumber(ArchivoEntrada,4,'_') + '.xml';

      StringToFile(Respuesta, ArchivoSalida);
      ProcesarRespuesta(ArchivoSalida);
  except
      on E : Exception do EventLogReportEvent(elError, E.Message, '');
  end;
end;
  *)
procedure TdmSyncMOPT.ProcesarEnvioManual;
var
 ArchivoEntrada : string;
 Entro: boolean;
 SemaforoExcepcion: boolean;
begin
  Entro := False;
  RegistrarLineaLog('TdmSyncMOPT.ProcesarEnvioManual: Inicio');
  SemaforoExcepcion := False;
  try
     //' el CambiarEstadoTag correlo una vez y luego de que genere el xml comentarlo'
     //ArchivoEntrada := CambiarEstadoTag;
     RegistrarLineaLog('TdmSyncMOPT.ProcesarEnvioManual: Llamando a VerificarSemaforo');
     Entro := VerificarSemaforo(CONST_RNUT, True, SemaforoExcepcion);
     RegistrarLineaLog('TdmSyncMOPT.ProcesarEnvioManual: Resultado de llamada VerificarSemaforo = ' + BoolToStr(Entro, True));
     if Entro then begin
        RegistrarLineaLog('TdmSyncMOPT.ProcesarEnvioManual: Semaforo NO levantado');
      //Establezco un minimo de media hora
//      if ((Principal.FFechaHoraUltimaEjecucion = NullDate) or
//          (strtoint(FormatDatetime('nn',NowBase(DMConnections.BaseCAC) - Principal.FFechaHoraUltimaEjecucion)) >= 30)) then begin

             RegistrarLineaLog('TdmSyncMOPT.ProcesarEnvioManual: Obteniendo FechaHora de la BD');
             Principal.FFechaHoraUltimaEjecucion := NowBase(DMConnections.BaseCAC);
             RegistrarLineaLog('TdmSyncMOPT.ProcesarEnvioManual: Llamando a ExportarAccionListRNUT');
             ArchivoEntrada := ExportarAccionListRNUT; // Primero generamos los AL
//             ArchivoEntrada := ExportarBajasRNUT;
//             ArchivoEntrada := ExportarModifRNUT;
//             ArchivoEntrada := ExportarAltasRNUT;
//       end;
     end else begin
        if not SemaforoExcepcion then begin
            RegistrarLineaLog('TdmSyncMOPT.ProcesarEnvioManual: Semaforo levantado');
            EventLogReportEvent(elError, 'NO se genero XML por estar el semaforo levantado', '');
        end;
     end;
  except
      on E : Exception do EventLogReportEvent(elError, E.Message, '');
  end;
  if Entro then VerificarSemaforo(CONST_RNUT, False, SemaforoExcepcion);
  RegistrarLineaLog('TdmSyncMOPT.ProcesarEnvioManual: Fin');
end;


{******************************** Function Header ******************************
Function Name: TDMSyncMOPT.ProcesarRespuestaManual
Author       : cchiappero
Date Created : 17/11/2004
Description  : Procedimiento procesar las respuestas a las novedades locales
               Mandadas por el RNUT cuando el proceso de envio fue autom�tico a
               trav�s de soap. En ese caso la respuesta se proceso en respuesta
               al mensaje de envio de novedad local. En el caso del envio manual
               el usuario debe colocar el archivo correspondiente en el directorio
               apropiado.
Parameters   : None
Return Value : None
*******************************************************************************}
procedure TDMSyncMOPT.ProcesarRespuestaManual;
var
    sr: TSearchRec;
    PathXML : string;
    PathXMLFinal : string;
    ListaArchivos : TStringList;
    i : Integer;
begin
    ListaArchivos := TStringList.Create;
    try
            ListaArchivos.Sorted := True;

            PathXML := ObtenerDirectorio(XML_PATH_ARCHIVO_ENTRADA_XML);
            PathXMLFinal := ObtenerDirectorio(XML_PATH_ARCHIVO_ENTRADA_XML_RECIBIDOS);

            if FindFirst(PathXML + 'salida_*.xml', faArchive, sr) = 0 then
            begin
              repeat
                    ListaArchivos.Add(sr.Name);
              until FindNext(sr) <> 0;
              FindClose(sr);
            end;

            for i := 0 to ListaArchivos.Count - 1 do begin
                 if dmSyncMOPT.ProcesarRespuesta(PathXML + '\' + ListaArchivos.Strings[i]) then begin
                    RenameFile(PathXML + '\' + ListaArchivos.Strings[i], PathXMLFinal + '\' + ListaArchivos.Strings[i])
                 end else begin
                    RenameFile(PathXML + '\' + ListaArchivos.Strings[i], PathXML + '\' + ListaArchivos.Strings[i] + '.err');
                end;
            end;
    finally
        ListaArchivos.Free;
    end;
end;

procedure TDMSyncMOPT.ProcesarRespuestaManualSinEnvio;
var
    sr: TSearchRec;
    PathXML : string;
    PathXMLFinal : string;
    ListaArchivos : TStringList;
    i : Integer;
begin
    ListaArchivos := TStringList.Create;
    try
            ListaArchivos.Sorted := True;

            PathXML := ObtenerDirectorio(XML_PATH_ARCHIVO_ENTRADA_XML);
            PathXMLFinal := ObtenerDirectorio(XML_PATH_ARCHIVO_ENTRADA_XML_RECIBIDOS);

            if FindFirst(PathXML + 'salida_*.xml', faArchive, sr) = 0 then
            begin
              repeat
                    ListaArchivos.Add(sr.Name);
              until FindNext(sr) <> 0;
              FindClose(sr);
            end;

            for i := 0 to ListaArchivos.Count - 1 do begin
                 if dmSyncMOPT.ProcesarRespuestaSinEnvio(PathXML + '\' + ListaArchivos.Strings[i]) then
                      RenameFile(PathXML + '\' + ListaArchivos.Strings[i], PathXMLFinal + '\' + ListaArchivos.Strings[i])
                 else RenameFile(PathXML + '\' + ListaArchivos.Strings[i], PathXML + '\' + ListaArchivos.Strings[i] + '.err');
            end;
    finally
        ListaArchivos.Free;
    end;
end;

{******************************** Function Header ******************************
Procedure Name: ObtenerDirectorio
Author :
Date Created :
Parameters : parametro : string
Return Value : string
Description :

Revision : 1
    Author : pdominguez
    Date   : 10/09/2009
    Description : SS 801
        - Se cambia la llamada a CreateDir por ForceDirectories, para la creaci�n
        la ruta completa para la salida del fichero en caso de que esta no exista
        de forma parcial o total.
*******************************************************************************}
function TDMSyncMOPT.ObtenerDirectorio(parametro : string): string;
begin
    if not(ObtenerParametroGeneral(DMConnections.BaseCAC, Parametro, result)) then
        raise Exception.Create('Error al obtener ubicaci�n archivo XML (Param. ' + parametro +' )');

    Result := Trim(Result);

    Result := GoodDir(result);

        if not DirectoryExists(result) then
            if not ForceDirectories(result) then raise Exception.Create('Error al crear directorio de archivo XML: ' + result); // Rev. 1 (SS 801)
end;

procedure TdmSyncMOPT.AgregarRepresentantesLegales(CodigoConvenio : longint; EntidadXML,ContratoXML : IXMLNode; CodigoEnvioConvenio : integer; Accion: String);
begin
    with ObtenerRepresentateLegalRNUT do
    try
            Parameters.ParamByName('@CodigoConvenio').Value:= CodigoConvenio;
            Open;
            while not ObtenerRepresentateLegalRNUT.Eof do begin
                EntidadXML:=AgregarXMLEntidad(ContratoXML, Accion,
                    StrLeft(trim(FieldByName('NumeroDocumento').AsString),length(trim(FieldByName('NumeroDocumento').AsString))-1),
                    StrRight(trim(FieldByName('NumeroDocumento').AsString),1),
                    XML_PERSONERIA_NATURAL,
                    FieldByName('Nombre').AsString,
                    FieldByName('Apellido').AsString,
                    FieldByName('ApellidoMaterno').AsString,
                    FieldByName('Apellido').AsString,
                    XML_ROL_REPRESENTATE_LEGAL,
                    ObtenerDatosPersona.FieldByName('Giro').AsString);

                Next;
            end;
    finally
            Close;
    end;

    //Resetea las acciones de convenioroles a ningunaaccion.-
(*    with ResetearAccionRnutConvenioRoles do begin
        Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
        ExecProc;
    end;
*)
end;


(*
procedure TdmSyncMOPT.EnviarMail(Directorio : String);
var
 direcciones : string;
 enviador : string;
 servidor : string;
 usuario : string;
 sr: TSearchRec;
begin

    //Obtengo las cuentad de mail para enviar.
    direcciones := InstallIni.ReadString('Mail', 'DireccionesDestino', '');
    enviador := InstallIni.ReadString('Mail', 'DireccionEnviador', '');
    servidor := InstallIni.ReadString('Mail', 'servidor', '');
    usuario :=  InstallIni.ReadString('Mail', 'usuario', '');


    if (enviador <> '') and (servidor <> '') and (direcciones <> '') then begin
        try
            dmSyncMOPT.smtp.Host := servidor;
            dmSyncMOPT.smtp.UserID := usuario;
            dmSyncMOPT.smtp.Connect;


            if FindFirst(Directorio + '*.xml', faArchive, sr) = 0 then
            begin
              repeat
                if (sr.Attr and faArchive) = sr.Attr then begin

                    dmSyncMOPT.smtp.PostMessage.FromAddress := Enviador;
                    dmSyncMOPT.smtp.PostMessage.ToAddress.CommaText := direcciones;
                    dmSyncMOPT.smtp.PostMessage.Subject := 'Envio autom�tico de RNUT - Archivo: ' + sr.Name;
                    dmSyncMOPT.smtp.PostMessage.Attachments.Clear;
                    dmSyncMOPT.smtp.PostMessage.Attachments.Add(Directorio + '\' + sr.Name);
                    dmSyncMOPT.smtp.SendMail;
                end;
              until FindNext(sr) <> 0;
              FindClose(sr);
            end;
        finally
            dmSyncMOPT.smtp.Disconnect;
        end;
    end;
end;


procedure TdmSyncMOPT.EnviarFTP(Directorio : String);
var
 servidor : string;
 usuario : string;
 contrasenia : string;
 DirectorioDestino : string;
 sr: TSearchRec;
begin

    //Obtengo las cuentad de mail para enviar.

    servidor := InstallIni.ReadString('FTP', 'servidor', '');
    usuario :=  InstallIni.ReadString('FTP', 'usuario', '');
    contrasenia :=  InstallIni.ReadString('FTP', 'contrasenia', 'cc@xx.com');
    DirectorioDestino :=  InstallIni.ReadString('FTP', 'directorio', '/');



    if (servidor <> '') and (usuario <> '')  then begin
        try
            ftp.Host := servidor;
            ftp.User := usuario;
            ftp.Password := contrasenia;
            ftp.Connect(true);


            if FindFirst(Directorio + '*.xml', faArchive, sr) = 0 then
            begin
              repeat
                if (sr.Attr and faArchive) = sr.Attr then begin
                  ftp.Put(Directorio + sr.Name, DirectorioDestino + sr.Name, false);
                end;
              until FindNext(sr) <> 0;
              FindClose(sr);
            end;
        finally
            dmSyncMOPT.ftp.Disconnect;
        end;
    end;
end;
*)

(*function TdmSyncMOPT.EnviarArchivoFTP(Archivo: String) : boolean;
var
 servidor : string;
 usuario : string;
 contrasenia : string;
 DirectorioDestino : string;
begin

    //Obtengo las cuentad de mail para enviar.

    servidor := InstallIni.ReadString('FTP', 'servidor', '');
    usuario :=  InstallIni.ReadString('FTP', 'usuario', '');
    contrasenia :=  InstallIni.ReadString('FTP', 'contrasenia', 'cc@xx.com');
    DirectorioDestino :=  InstallIni.ReadString('FTP', 'directorio', '/');

    Result := False;
    if (servidor <> '') and (usuario <> '')  then begin
        try
          try
            ftp.Host := servidor;
            ftp.User := usuario;
            ftp.Password := contrasenia;
            ftp.Connect(true);
            ftp.Put(Archivo, directorioDestino + '\' + ExtractFileName(Archivo), false);
            Result := True;
          except
            EventLogReportEvent(elError, 'Error al enviar el archivo:' + Archivo, '');
            RenameFile(Archivo, Archivo + '.err');
          end;
        finally
            dmSyncMOPT.ftp.Disconnect;
        end;

    end;

end;
*)

(*
function TdmSyncMOPT.EnviarArchivoMail(Archivo: String) : boolean;
var
 direcciones : string;
 enviador : string;
 servidor : string;
 usuario : string;
begin

    //Obtengo las cuentad de mail para enviar.
    direcciones := InstallIni.ReadString('Mail', 'DireccionesDestino', '');
    enviador := InstallIni.ReadString('Mail', 'DireccionEnviador', '');
    servidor := InstallIni.ReadString('Mail', 'servidor', '');
    usuario :=  InstallIni.ReadString('Mail', 'usuario', '');

    Result := False;
    if (enviador <> '') and (servidor <> '') and (direcciones <> '') then begin
        try
          try
            dmSyncMOPT.smtp.Host := servidor;
            dmSyncMOPT.smtp.UserID := usuario;
            dmSyncMOPT.smtp.Connect;


            dmSyncMOPT.smtp.PostMessage.FromAddress := Enviador;
            dmSyncMOPT.smtp.PostMessage.ToAddress.CommaText := direcciones;
            dmSyncMOPT.smtp.PostMessage.Subject := 'Envio autom�tico de RNUT - Archivo: ' + ExtractFileName(Archivo);
            dmSyncMOPT.smtp.PostMessage.Attachments.Clear;
            dmSyncMOPT.smtp.PostMessage.Attachments.Add(Archivo);
            dmSyncMOPT.smtp.SendMail;

            Result := True;
          except
            EventLogReportEvent(elError, 'Error al enviar el archivo:' + Archivo, '');
            RenameFile(Archivo, Archivo + '.err');
          end;



        finally
            dmSyncMOPT.smtp.Disconnect;
        end;
    end;
end;
*)

procedure TdmSyncMOPT.CargarParametros;
var
  ValorDate : TDateTime;

begin
    ObtenerParametroServicio(DMConnections.BaseCAC,
                             sYncMOPT,
                             SYNCMOPT_RESINCRONIZARDESDE, ValorDate);
    FDesde := ValorDate;

    ObtenerParametroServicio(DMConnections.BaseCAC,
                             sYncMOPT,
                             SYNCMOPT_RESINCRONIZARHASTA, ValorDate);
    FHasta := ValorDate;
end;

(*function TdmSyncMOPT.VerificarArchivoNovedadesAProcesar(
  Archivo: string): Boolean;
begin
    result := QueryGetValueInt(DMConnections.BaseCAC,format('select count* from RecepcionConvenioRNUT where NombreArchivo = ''%s''', [Archivo])) = 0;
end;*)

{-----------------------------------------------------------------------------
  Function Name: ProcesarRespuestaSinEnvio
  Author:
  Date Created:  /  /
  Description:
  Parameters: Archivo : string
  Return Value: boolean

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function TdmSyncMOPT.ProcesarRespuestaSinEnvio(Archivo : string) : boolean;
var
    mensajeTotalRegistros, msjLog: AnsiString;
    mensajeAux : AnsiString;

    NombreArchivoLog: AnsiString;

    ContenedorXML: IXMLDocument;
    Cantidad,
    CantidadError, CantidadCorrectos, i, x: integer;
    AuxNodoConvenio: IXMLNode;

    Convenio : Integer;
    NumeroConvenio : string;
    CodigoRespuesta : integer;
    Complemento : string;

    NombreArchivoReporte : string;
    ConveniosDistintos : TStringList;
    Log : TStringList;
    LogErroresProceso : TStringList;
begin
    Result := False;
    mensajeTotalRegistros := '';
    msjLog := ''; mensajeAux := '';
    NombreArchivoLog := '';

    Convenio := 0; Cantidad := 0; CantidadError := 0; CantidadCorrectos := 0;

    ConveniosDistintos := TStringList.Create;
    Log := TStringList.Create;
    LogErroresProceso := TStringList.Create;

    // Archivo de entrada de datos
    ContenedorXML:=CrearXML;
    ContenedorXML.LoadFromFile(Archivo);

    NombreArchivoLog := ExtractFilePath(Archivo) +
                        copy(ExtractFileName(Archivo)
                        , 1, pos('.',ExtractFileName(Archivo)) - 1) + '.log';

    NombreArchivoReporte := ExtractFilePath(Archivo) +
                        copy(ExtractFileName(Archivo)
                        , 1, pos('.',ExtractFileName(Archivo)) - 1) + '.rep';
    try
        try
              with ContenedorXML.DocumentElement.ChildNodes do begin
                  for i := 0 to Count - 1  do begin //Contratos
                       try
                          Inc(cantidad);
                          x := i;
                          AuxNodoConvenio := ObtenerGrupo(ContenedorXML.DocumentElement, x, XML_CONTRATO);
                          NumeroConvenio  := AuxNodoConvenio.ChildNodes[XML_CODIGO].Text;
                          CodigoRespuesta := AuxNodoConvenio.ChildNodes[XML_RESPUESTA].NodeValue;
                          Convenio := QueryGetValueInt(DMConnections.BaseCAC,format('select CodigoConvenio from convenio  WITH (NOLOCK) where NumeroConvenio = ''%s''',[NumeroConvenio]));
                          Complemento := AuxNodoConvenio.ChildNodes[XML_COMPLEMENTO].NodeValue;

                          if ConveniosDistintos.IndexOf(NumeroConvenio) < 0 then ConveniosDistintos.Add(NumeroConvenio);

                          if Convenio = 0 then
                            LogErroresProceso.Add('No se encontr� el C�digo de Convenio para el Nro.Convenio: ' + NumeroConvenio)
                          else if CodigoRespuesta > 1 then LogErroresProceso.Add('Nro Convenio: ' + NumeroConvenio + '. Error: ' + Complemento);

                          Inc(CantidadCorrectos);
                        except
                            on E: Exception do begin
                                    if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.RollbackTrans;
                                    msjLog := E.Message;
                                    Inc(CantidadError);
                                    Log.Add('Convenio: ' + Inttostr(Convenio) + ' - Complemento: ' + Complemento + ' - ' + msjLog);
                            end;
                        end;

                        msjLog := '';

                  end; //for i;
              end; //with


              mensajeTotalRegistros := 'Resultados: ' + CRLF +
                '   - Total Rechazos en Archivo :' + #9 + #9+ IntToStr(Cantidad) +  CRLF +
                '   - Total Rechazos Importados Correctamente:' + #9 + #9+ IntToStr(CantidadCorrectos) + CRLF +
                '   - Total Rechazos Importados Con Error:' + #9 + #9 + IntToStr(CantidadError) + CRLF +
                '   - Total Convenios Rechazados Diferentes :' + #9 + #9 + IntToStr(ConveniosDistintos.Count) + CRLF +
                ' Mensajes Adicionales: ' + mensajeAux;

              Log.Add(mensajeTotalRegistros);
              Log.SaveToFile(NombreArchivoLog);
              FreeAndNil(Log);

              LogErroresProceso.SaveToFile(NombreArchivoReporte);
              FreeAndNil(LogErroresProceso);

              if CantidadError = 0 then begin
                    result := True;
                    EventLogReportEvent(elInformation, 'El archivo se proces� correctamente. ' + CRLF + mensajeTotalRegistros , '');
              end else begin
                    result := False;
                    EventLogReportEvent(elError, 'El proceso se proceso con errores. ' + CRLF + mensajeTotalRegistros +
                                                  CRLF +  CRLF + 'Se gener� el archivo de log ' + NombreArchivoLog, '');
              end;


        except
                on E: Exception do begin
                      mensajeAux := E.Message;
                end;
        end;
    finally
        ConveniosDistintos.Free;

    end;
end;

{*******************************************************************************
Revision 1:
    Author : mlopez
    Date : 25/09/2006
    Description : Este procedimiento quedo obsoleto
*******************************************************************************}
{function TdmSyncMOPT.ExportarAltasRNUT: string;
var
    XML: IXMLDocument;
    ContenedorXML, ContratoXML, EntidadXML, VehiculoXML: IXMLNode;
    FileNameRNUT, NumeroConvenio, Personeria, PathXML,
    DigitoVerificadorPatente, ContractSerialNumber: AnsiString;
    CodigoEnvioConvenio, CantTotal, CantidadError,
    CantidadCorrectos, CodigoConvenio: LongInt;
    FechaEnvio: TDateTime;
    ContextMark, IssuerIdentifier,
    TypeOfContract, ContextVersion,
    PatenteRNUT, DVRNUT: AnsiString;

    ArchivoXML : string;
    MensajeFinal, MensajesError : string;
    PathXMLEnviado : string;
    MensajeProgreso : string;
begin
    CargarParametros;

    try
        try
            PathXML := ObtenerDirectorio(XML_PARAMETRO_ARCHIVO_XML);
            PathXMLEnviado := ObtenerDirectorio(XML_PATH_ARCHIVO_SALIDA_XML_ENVIADOS);

            with AltasConvenioRNUT do begin
                repeat

                    CantTotal:=0; CantidadError := 0; CantidadCorrectos := 0;

                    MensajeFinal := ''; MensajesError := '';

                    MensajeProgreso := 'Obteniendo Novedades Convenios';
                    close;
                    Parameters.ParamByName('@CodigoConvenio').Value := Principal.FCodigoConvenioAltas;
                    open;

                    Principal.FCodigoConvenioAltas := Parameters.ParamByName('@CodigoConvenio').Value;

                    if RecordCount <> 0 then begin
                        if not DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.BeginTrans;

                        FechaEnvio := NowBase(Connection);
                        // Registro de Envio
                        CodigoEnvioConvenio := AgregarEnvioConvenio(usuarioSistema);

                         XML := CrearXML;
                        ContenedorXML := CrearContenedor(XML);
                         FileNameRNUT := 'entrada_1_' + IntToStr(CodigoEnvioConvenio) + '_' + FormatDateTime('yyyymmddhhnnss', FechaEnvio);
                         ArchivoXML :=  GoodFileName(PathXML + FileNameRNUT, '.xml');

                        while not EOF and (CantidadError = 0) do begin
                            ContratoXML     := AgregarXMLContrato(ContenedorXML, XML_ALTA, fieldByname('NumeroConvenioRNUT').AsString, '1', NullDate);
                            CodigoConvenio  := AltasConvenioRNUT.fieldbyName('CodigoConvenio').AsInteger;
                            Inc(CantTotal);

                            MensajeProgreso := 'Actualizando Alta Convenios Envios';
                            try
                            	ActualizarConvenioEnvio(fieldByname('NumeroConvenioRNUT').AsString, CodigoEnvioConvenio, XML_PROCESADO_PENDIENTE);

                            	ActualizarConvenioSincronizado.Close;
                            	ActualizarConvenioSincronizado.Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
                            	ActualizarConvenioSincronizado.ExecProc;

                                //Datos del titular
                                ObtenerDatosPersona.Close;
                                MensajeProgreso := 'Obteniendo Datos Persona';
                                with ObtenerDatosPersona.Parameters do begin
                                    ParamByName('@CodigoPersona').Value:=FieldByName('CodigoCliente').AsInteger;
                                    ParamByName('@CodigoDocumento').Value:=null;
                                    ParamByName('@NumeroDocumento').Value:=null;
                                end;
                                ObtenerDatosPersona.open;
                                if ObtenerDatosPersona.RecordCount<1 then begin
                                    ObtenerDatosPersona.Close;
                                    raise Exception.Create(format(MSG_ERROR_OPENTABLE,['Personas - Convenio Numero: '+NumeroConvenio]));
                                end;
                                Personeria:=ObtenerDatosPersona.FieldByName('Personeria').AsString;
                                NumeroConvenio:=trim(FieldByName('NumeroConvenioRNUT').AsString);

                                if ObtenerDatosPersona.FieldByName('Personeria').AsString=PERSONERIA_JURIDICA then begin
                                    EntidadXML:=AgregarXMLEntidad(ContratoXML,
                                      XML_ALTA,
                                      StrLeft(trim(ObtenerDatosPersona.FieldByName('NumeroDocumento').AsString),
                                      length(trim(ObtenerDatosPersona.FieldByName('NumeroDocumento').AsString))-1),
                                      StrRight(trim(ObtenerDatosPersona.FieldByName('NumeroDocumento').AsString),1),
                                      XML_PERSONERIA_JURIDICA,
                                      ObtenerDatosPersona.FieldByName('Apellido').AsString,
                                      '',
                                      '',
                                      '',//iif(ObtenerDatosPersona.FieldByName('Personeria').AsString=PERSONERIA_JURIDICA,ObtenerDatosPersona.FieldByName('Apellido').AsString, ''), //Nombre Fantasia
                                      XML_ROL_CLIENTE,
                                      ObtenerDatosPersona.FieldByName('Giro').AsString);

                                    //Representates Legales
                                    MensajeProgreso := 'Obteniendo Representante';
                                    AgregarRepresentantesLegales(CodigoConvenio, EntidadXML, ContratoXML, CodigoEnvioConvenio, XML_ALTA);
                                end else begin
                                    EntidadXML:=AgregarXMLEntidad(ContratoXML,
                                      XML_ALTA,
                                      StrLeft(trim(ObtenerDatosPersona.FieldByName('NumeroDocumento').AsString), length(trim(ObtenerDatosPersona.FieldByName('NumeroDocumento').AsString))-1),
                                      StrRight(trim(ObtenerDatosPersona.FieldByName('NumeroDocumento').AsString),1),
                                      XML_PERSONERIA_NATURAL,
                                      ObtenerDatosPersona.FieldByName('Nombre').AsString,
                                      ObtenerDatosPersona.FieldByName('Apellido').AsString,
                                      ObtenerDatosPersona.FieldByName('ApellidoMaterno').AsString,
                                      '',//iif(ObtenerDatosPersona.FieldByName('Personeria').AsString=PERSONERIA_JURIDICA,ObtenerDatosPersona.FieldByName('Apellido').AsString, ''), //Nombre Fantasia
                                      XML_ROL_CLIENTE,
                                      ObtenerDatosPersona.FieldByName('Giro').AsString);
                                end;

                                ObtenerDatosPersona.close;

                                MensajeProgreso := 'Obteniendo Domicilios Convenios';
                                //Domicilio
                                ObtenerDomicilioConvenio.Close;
                                with ObtenerDomicilioConvenio.Parameters do begin
                                    ParamByName('@CodigoConvenio').Value:= CodigoConvenio;
                                end;
                                ObtenerDomicilioConvenio.open;

                                if ObtenerDomicilioConvenio.RecordCount<1 then begin
                                    ObtenerDomicilioConvenio.Close;
                                    raise Exception.Create(format(MSG_ERROR_OPENTABLE,['No se encontr� el domicilio del convenio - Convenio Numero: '+NumeroConvenio]));
                                end;


                                if not(ObtenerDomicilioConvenio.Eof) then begin
                                    AgregarXMLDomicilio(ContratoXML,
                                      XML_ALTA,
                                      trim(ObtenerDomicilioConvenio.fieldByName('CodigoDomicilio').AsString),
                                      XML_DIRECCION_FACTURACION,
                                      iif(not ObtenerDomicilioConvenio.fieldByName('Normalizado').AsBoolean,
                                              ObtenerDomicilioConvenio.fieldByName('DescriCalle').AsString,
                                              ObtenerDomicilioConvenio.fieldByName('CalleDesnormalizada').AsString),
                                      ObtenerDomicilioConvenio.fieldByName('DescriComuna').AsString,
                                      ObtenerDomicilioConvenio.fieldByName('Numero').AsString,
                                      '',
                                      ObtenerDomicilioConvenio.fieldByName('CodigoPostal').AsString,
                                      ObtenerDomicilioConvenio.fieldByName('Detalle').AsString);
                                end;
                                ObtenerDomicilioConvenio.close;

                                MensajeProgreso := 'Obteniendo Vehiculos Tags';
                                // Vehiculo
                                ObtenerVehiculosTAGConvenioRNUT.Close;
                                with ObtenerVehiculosTAGConvenioRNUT.Parameters do begin
                                     ParamByName('@CodigoConvenio').Value:= CodigoConvenio;
                                end;
                                ObtenerVehiculosTAGConvenioRNUT.Open;
                                while not(ObtenerVehiculosTAGConvenioRNUT.Eof) do begin
                                    DigitoVerificadorPatente := ''; ContractSerialNumber := ''; ContextMark := ''; PatenteRNUT := '';
                                    DVRNUT := ''; IssuerIdentifier := ''; TypeOfContract := ''; ContextVersion := '';

                                    if (ObtenerVehiculosTAGConvenioRNUT.FieldByName('DigitoVerificadorPatente').IsNull) or
                                       (ObtenerVehiculosTAGConvenioRNUT.FieldByName('DigitoVerificadorPatente').AsString = '') then begin
                                        DigitoVerificadorPatente := QueryGetValue(DMConnections.BaseCAC, format('Exec ObtenerDigitoVerificadorPatente ''%s''',[ trim(ObtenerVehiculosTAGConvenioRNUT.FieldByName('Patente').AsString)]));
                                    end
                                    else DigitoVerificadorPatente := trim(ObtenerVehiculosTAGConvenioRNUT.FieldByName('DigitoVerificadorPatente').AsString);

                                    IssuerIdentifier := IIf(ObtenerVehiculosTAGConvenioRNUT.FieldByName('IssuerIdentifier').IsNull,'', ObtenerVehiculosTAGConvenioRNUT.FieldByName('IssuerIdentifier').AsString);

                                    //Forzado a 1. 15-09-2004--
                                    TypeOfContract := '1'; // iif (ObtenerVehiculosTAGConvenioRNUT.FieldByName('TypeOfContract').IsNull,'', ObtenerVehiculosTAGConvenioRNUT.FieldByName('TypeOfContract').AsString);
                                    ContextVersion := '1'; //iif (ObtenerVehiculosTAGConvenioRNUT.FieldByName('ContextVersion').IsNull,'', ObtenerVehiculosTAGConvenioRNUT.FieldByName('ContextVersion').AsString);

                                    VehiculoXML := AgregarXMLVehiculo(ContratoXML, XML_ALTA,
                                                trim(ObtenerVehiculosTAGConvenioRNUT.FieldByName('Patente').AsString),
                                                DigitoVerificadorPatente,
                                                ObtenerCategoriaVehiculoXML(ObtenerVehiculosTAGConvenioRNUT.FieldByName('CodigoTipoVehiculo').AsInteger,ObtenerVehiculosTAGConvenioRNUT.FieldByName('TieneAcoplado').AsBoolean),
                                                ObtenerVehiculosTAGConvenioRNUT.FieldByName('DescripMarca').AsString,
                                                ObtenerVehiculosTAGConvenioRNUT.FieldByName('Modelo').AsString,
                                                ObtenerVehiculosTAGConvenioRNUT.FieldByName('AnioVehiculo').AsString,
                                                NullDate);

                                    AgregarXMLTag(VehiculoXML, XML_ALTA,
                                                ObtenerVehiculosTAGConvenioRNUT.FieldByName('ContextMark').AsString,
                                                ObtenerVehiculosTAGConvenioRNUT.FieldByName('IssuerIdentifier').AsString,
                                                ObtenerVehiculosTAGConvenioRNUT.FieldByName('TypeOfContract').AsString,
                                                ObtenerVehiculosTAGConvenioRNUT.FieldByName('ContextVersion').AsString,
                                                ObtenerVehiculosTAGConvenioRNUT.FieldByName('ContractSerialNumber').AsString,
                                                ObtenerVehiculosTAGConvenioRNUT.FieldByName('CodigoProveedorTAG').AsString,
                                                ObtenerVehiculosTAGConvenioRNUT.FieldByName('CodigoCategoria').AsString,
                                                XML_ESTADO_TAG_INSTALADO,
                                                FormatDateTime('ddmmyyyy hhnnss', ObtenerVehiculosTAGConvenioRNUT.FieldByName('FechaAltaCuenta').AsDateTime)
                                                );

                                    ObtenerVehiculosTAGConvenioRNUT.Next;
                                end;
                                ObtenerVehiculosTAGConvenioRNUT.close;

                                Inc(CantidadCorrectos);
                            except
                                on E: Exception do begin
                                    MensajesError := MensajesError + 'Error procesando Convenio: ' + IntToStr(CodigoConvenio) + ' ' +
                                                                     E.Message + CRLF;
                                    Inc(CantidadError);
                                end;
                            end;
                            // fin
                            Next;
                        end;

                          MensajeFinal := 'Exportaci�n Altas Terminada. ' + CRLF + 'Resultados: ' + CRLF +
                              ' - Registros exportados correctamente: ' + IntToStr(CantidadCorrectos) + CRLF +
                              ' - Registros con error (sin exportar): ' + IntToStr(CantidadError) + CRLF +
                              ' - Total de registros: ' + IntToStr(CantTotal);



                          if CantidadError = 0 then begin
                              // Grabar
//                              FileNameRNUT := 'entrada_1_' + IntToStr(CodigoEnvioConvenio) + '_' + FormatDateTime('yyyymmddhhnnss', FechaEnvio);
//                              ArchivoXML :=  GoodFileName(PathXML + FileNameRNUT, '.xml');
                              GrabarXML(XML, ContenedorXML, ArchivoXML);
                              // Actualizamos la fecha de envio

                              MensajeProgreso := 'Actualizando Envio Convenio';
                              ActualizarEnvioConvenioRNUT.Parameters.ParamByName('@CodigoEnvioConvenioRNUT').Value := CodigoEnvioConvenio;
                              ActualizarEnvioConvenioRNUT.Parameters.ParamByName('@NombreArchivo').Value := FileNameRNUT;
                              ActualizarEnvioConvenioRNUT.execProc;
                              DMConnections.BaseCAC.CommitTrans;
                              result := ArchivoXML;
                              (*
                              if EnviarArchivoMail(ArchivoXML) or
                                  EnviarArchivoFTP(ArchivoXML) then
                                  RenameFile(ArchivoXML, PathXMLEnviado + ExtractFileName(ArchivoXML));
                              *)
                              EventLogReportEvent(elInformation, MensajeFinal, '');
                          end else begin
                              result := '';
                              DMConnections.BaseCAC.RollbackTrans;
                              EventLogReportEvent(elError, MensajeFinal + CRLF + MensajesError, '');
                          end;
                    end;
                until (not AltasConvenioRNUT.Active) or (Principal.FCodigoConvenioAltas = 0);
            end; //with ObtenerNovedadesConveniosRNUT
        except
            on E: Exception do begin
                if FileExists(ArchivoXML) then RenameFile(ArchivoXML, ArchivoXML + '.old');
                if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.RollbackTrans;
                EventLogReportEvent(elError, E.message + CRLF + MensajeProgreso, '');
            end;
        end;
    finally
        AltasConvenioRNUT.Close;
    end;
end; }

{*******************************************************************************
Revision 1:
    Author : mlopez
    Date : 25/09/2006
    Description : Este procedimiento quedo obsoleto
*******************************************************************************}
{function TdmSyncMOPT.ExportarBajasRNUT: string;
var
    XML: IXMLDocument;
    ContenedorXML, ContratoXML: IXMLNode;
    CodigoEnvioConvenio, CantTotal, CantidadError,
    CantidadCorrectos, CodigoConvenio: LongInt;
    FechaEnvio: TDateTime;
    FileNameRNUT, PathXML, ArchivoXML : string;
    MensajeFinal, MensajesError : string;
    PathXMLEnviado : string;
    MensajeProgreso : string;
begin
    CargarParametros;
    try
        try
            PathXML := ObtenerDirectorio (XML_PARAMETRO_ARCHIVO_XML);
            PathXMLEnviado := ObtenerDirectorio(XML_PATH_ARCHIVO_SALIDA_XML_ENVIADOS);

            with BajasConvenioRNUT do begin
                repeat

                    CantTotal:=0; CantidadError := 0; CantidadCorrectos := 0;

                    MensajeFinal := ''; MensajesError := '';

                    MensajeProgreso := 'Obteniendo Novedades Bajas Convenios';
                    close;
                    Parameters.ParamByName('@CodigoConvenio').Value := Principal.FCodigoConvenioBajas;
                    open;

                    Principal.FCodigoConvenioBajas := Parameters.ParamByName('@CodigoConvenio').Value;

                    if (RecordCount <> 0) then begin
                        if not DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.BeginTrans;

                        FechaEnvio := NowBase(Connection);
                        // Registro de Envio
                        CodigoEnvioConvenio := AgregarEnvioConvenio(usuarioSistema);
                        XML := CrearXML;
                        ContenedorXML := CrearContenedor(XML);


                        while (not(EOF)) do begin
                            ContratoXML     := AgregarXMLContrato(ContenedorXML, XML_BAJA, fieldByname('NumeroConvenioRNUT').AsString, '', fieldByname('FechaBajaRNUT').AsDateTime );
                            CodigoConvenio  := BajasConvenioRNUT.fieldbyName('CodigoConvenio').AsInteger;
                            Inc(CantTotal);

                            ActualizarConvenioSincronizado.Close;
                            ActualizarConvenioSincronizado.Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
                            ActualizarConvenioSincronizado.ExecProc;

                            MensajeProgreso := 'Actualizando Convenios Envios';
                            ActualizarConvenioEnvio(fieldByname('NumeroConvenioRNUT').AsString, CodigoEnvioConvenio, XML_PROCESADO_PENDIENTE);

                            // fin
                            Next;
                        end;

                        MensajeFinal := 'Exportaci�n Bajas Convenios Terminada. ' + CRLF + 'Resultados: ' + CRLF +
                            ' - Registros exportados correctamente: ' + IntToStr(CantidadCorrectos) + CRLF +
                            ' - Registros con error (sin exportar): ' + IntToStr(CantidadError) + CRLF +
                            ' - Total de registros: ' + IntToStr(CantTotal);



                        if CantidadError = 0 then begin
                            // Grabar
                            FileNameRNUT := 'entrada_1_' + IntToStr(CodigoEnvioConvenio) + '_' + FormatDateTime('yyyymmddhhnnss', FechaEnvio);
                            ArchivoXML :=  GoodFileName(PathXML + FileNameRNUT, '.xml');
                            GrabarXML(XML, ContenedorXML, ArchivoXML);
                            // Actualizamos la fecha de envio

                            MensajeProgreso := 'Actualizando Envio Convenio';
                            ActualizarEnvioConvenioRNUT.Parameters.ParamByName('@CodigoEnvioConvenioRNUT').Value := CodigoEnvioConvenio;
                            ActualizarEnvioConvenioRNUT.Parameters.ParamByName('@NombreArchivo').Value := FileNameRNUT;
                            ActualizarEnvioConvenioRNUT.execProc;
                            DMConnections.BaseCAC.CommitTrans;


                            result := ArchivoXML;

                            (*
                            if EnviarArchivoMail(ArchivoXML) or
                                EnviarArchivoFTP(ArchivoXML) then
                                RenameFile(ArchivoXML, PathXMLEnviado + ExtractFileName(ArchivoXML));
                            *)
                            
                            EventLogReportEvent(elInformation, MensajeFinal, '');

                        end else begin
                            result := '';
                            DMConnections.BaseCAC.RollbackTrans;
                            EventLogReportEvent(elError, MensajeFinal + CRLF + MensajesError, '');
                        end;
                    end;
                until (not BajasConvenioRNUT.Active) or (Principal.FCodigoConvenioBajas = 0);
            end; //with ObtenerNovedadesConveniosRNUT

        except
            on E: Exception do begin
                if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.RollbackTrans;
                EventLogReportEvent(elError, E.message + CRLF + MensajeProgreso, '');
            end;
        end;
    finally
            BajasConvenioRNUT.Close;
    end;
end;}

{******************************** Function Header ******************************
Function Name: ExportarModifRNUT
Author :
Date Created :
Description :
Parameters : None
Return Value : string

Revision 1:
    Author : ggomez
    Date : 23/02/2006
    Description : En el proceso de los domicilios, si no se encuentra el
        domicilio para el convenio (esto puede ser pues hace un inner con una
        tabla de la BD RNUT, y en esa tabla puede no estar el dato), se hace un
        alta del domicilio con los datos que hay en el CAC.

Revision 1:
    Author : mlopez
    Date : 25/09/2006
    Description : Este procedimiento quedo obsoleto
*******************************************************************************}
{function TdmSyncMOPT.ExportarModifRNUT: string;
var
    XML: IXMLDocument;
    ContenedorXML, ContratoXML, EntidadXML, VehiculoXML: IXMLNode;
    FileNameRNUT, NumeroConvenio, Personeria : String;
    CodigoEnvioConvenio, CantTotal, CantidadError,
    CantidadCorrectos, CodigoConvenio: LongInt;
    FechaEnvio: TDateTime;

    PathXML, ArchivoXML : string;
    MensajeFinal, MensajesError : string;
    PathXMLEnviado : string;
    MensajeProgreso : string;
begin
    CargarParametros;
    try
        try
            PathXML := ObtenerDirectorio(XML_PARAMETRO_ARCHIVO_XML);
            PathXMLEnviado := ObtenerDirectorio(XML_PATH_ARCHIVO_SALIDA_XML_ENVIADOS);
            with ModificadosConvenioRNUT do begin
                repeat
                    CantTotal := 0; CantidadError := 0; CantidadCorrectos := 0;
                    MensajeFinal := ''; MensajesError := '';
                    MensajeProgreso := 'Obteniendo Modificaciones de Convenios';
                    Close;
                    Parameters.ParamByName('@CodigoConvenio').Value := Principal.FCodigoConvenioModif;
                    Open;
                    Principal.FCodigoConvenioModif := Parameters.ParamByName('@CodigoConvenio').Value;
                    if (RecordCount <> 0) then begin
						try
                        	ModificadosCuentasRNUT.Close;
                        	ModificadosCuentasRNUT.Open;
                        	ModificadosRepresentantesRNUT.Close;
                        	ModificadosRepresentantesRNUT.Open;
                            ModificadosDomiciliosRNUT.Close;
                            ModificadosDomiciliosRNUT.Open;
                        except
                        	on E: Exception do begin
                            	MensajesError := MensajesError + 'Error al obtener Cuentas, Domicilios o Rep Legales. Iniciando en Convenio: ' +
                                					IntToStr(ModificadosConvenioRNUT.fieldbyName('CodigoConvenio').AsInteger) + ' ' +
                                					E.Message + CRLF;
                                Inc(CantidadError);
                            end;
                        end;

                        if not DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.BeginTrans;

                        FechaEnvio := NowBase(Connection);
                        // Registro de Envio
                        CodigoEnvioConvenio := AgregarEnvioConvenio(usuarioSistema);
                        XML := CrearXML;
                        ContenedorXML := CrearContenedor(XML);
                        //Calculamos el nombre que va a tener este archivo
                        FileNameRNUT := 'entrada_1_' + IntToStr(CodigoEnvioConvenio) + '_' + FormatDateTime('yyyymmddhhnnss', FechaEnvio);
                        ArchivoXML :=  GoodFileName(PathXML + FileNameRNUT, '.xml');
                        while (not EOF) and (CantidadError = 0) do begin
                            ContratoXML     := AgregarXMLContrato(ContenedorXML, XML_MODIFICACION, fieldByname('NumeroConvenioRNUT').AsString, '1', NullDate);
                            CodigoConvenio  := ModificadosConvenioRNUT.fieldbyName('CodigoConvenio').AsInteger;
                            Inc(CantTotal);

                            try
                            	MensajeProgreso := 'Actualizando Modificaciones Convenios Envios';
                               	ActualizarConvenioEnvio(fieldByname('NumeroConvenioRNUT').AsString, CodigoEnvioConvenio, XML_PROCESADO_PENDIENTE);

                                //Datos del titular
                                ObtenerDatosPersona.Close;
                                MensajeProgreso := 'Obteniendo Datos Persona';
                                with ObtenerDatosPersona.Parameters do begin
                                    ParamByName('@CodigoPersona').Value:=FieldByName('CodigoCliente').AsInteger;
                                    ParamByName('@CodigoDocumento').Value:=null;
                                    ParamByName('@NumeroDocumento').Value:=null;
                                end;
                                ObtenerDatosPersona.Open;
                                if ObtenerDatosPersona.RecordCount < 1 then begin
                                    ObtenerDatosPersona.Close;
                                    raise Exception.Create(format(MSG_ERROR_OPENTABLE,['Personas - Convenio Numero: '+NumeroConvenio]));
                                end;
                                Personeria := ObtenerDatosPersona.FieldByName('Personeria').AsString;
                                NumeroConvenio := Trim(FieldByName('NumeroConvenioRNUT').AsString);

                                if ObtenerDatosPersona.FieldByName('Personeria').AsString = PERSONERIA_JURIDICA then begin
                                    EntidadXML := AgregarXMLEntidad(ContratoXML,
                                      XML_MODIFICACION,
                                      StrLeft(trim(ObtenerDatosPersona.FieldByName('NumeroDocumento').AsString), length(trim(ObtenerDatosPersona.FieldByName('NumeroDocumento').AsString))-1),
                                      StrRight(trim(ObtenerDatosPersona.FieldByName('NumeroDocumento').AsString),1),
                                      XML_PERSONERIA_JURIDICA,
                                      ObtenerDatosPersona.FieldByName('Apellido').AsString,
                                      '',
                                      '',
                                      '',//iif(ObtenerDatosPersona.FieldByName('Personeria').AsString=PERSONERIA_JURIDICA,ObtenerDatosPersona.FieldByName('Apellido').AsString, ''), //Nombre Fantasia
                                      XML_ROL_CLIENTE,
                                      ObtenerDatosPersona.FieldByName('Giro').AsString);

                                     //Representates Legales
                                     MensajeProgreso := 'Obteniendo Representante';

                                     if ModificadosRepresentantesRNUT.Locate('CodigoConvenio', CodigoConvenio, [loCaseInsensitive]) then begin
                                         While not ModificadosRepresentantesRNUT.Eof and (ModificadosRepresentantesRNUT.FieldByName('CodigoConvenio').AsInteger = CodigoConvenio) do begin
                                             AgregarRepresentantesLegales(EntidadXML, ContratoXML, ModificadosRepresentantesRNUT.fieldByName('Accion').AsString,
                                                ModificadosRepresentantesRNUT.fieldByName('NumeroDocumento').AsString,
                                                ModificadosRepresentantesRNUT.fieldByName('DV').AsString,
                                                ModificadosRepresentantesRNUT.fieldByName('Nombre').AsString,
                                                ModificadosRepresentantesRNUT.fieldByName('Apellido').AsString,
                                                ModificadosRepresentantesRNUT.fieldByName('ApellidoMaterno').AsString);
                                             ModificadosRepresentantesRNUT.Next;
                                         end;
                                     end;

                                end else begin
                                    EntidadXML:=AgregarXMLEntidad(ContratoXML,
                                      XML_MODIFICACION,
                                      StrLeft(trim(ObtenerDatosPersona.FieldByName('NumeroDocumento').AsString), length(trim(ObtenerDatosPersona.FieldByName('NumeroDocumento').AsString))-1),
                                      StrRight(trim(ObtenerDatosPersona.FieldByName('NumeroDocumento').AsString),1),
                                      XML_PERSONERIA_NATURAL,
                                      ObtenerDatosPersona.FieldByName('Nombre').AsString,
                                      ObtenerDatosPersona.FieldByName('Apellido').AsString,
                                      ObtenerDatosPersona.FieldByName('ApellidoMaterno').AsString,
                                      '',//iif(ObtenerDatosPersona.FieldByName('Personeria').AsString=PERSONERIA_JURIDICA,ObtenerDatosPersona.FieldByName('Apellido').AsString, ''), //Nombre Fantasia
                                      XML_ROL_CLIENTE,
                                      ObtenerDatosPersona.FieldByName('Giro').AsString);
                                end;
                                ObtenerDatosPersona.close;

                                //Domicilio
                                MensajeProgreso := 'Obteniendo Domicilios Convenios';
                                if ModificadosDomiciliosRNUT.Locate('CodigoConvenio', CodigoConvenio, [loCaseInsensitive]) then begin
                                    if not ModificadosDomiciliosRNUT.FieldByName('CodigoDomicilioBaja').IsNull then begin
                                        // Doy de baja el domicilio Anterior
                                        AgregarXMLDomicilio(ContratoXML,
                                            XML_BAJA,
                                            Trim(ModificadosDomiciliosRNUT.fieldByName('CodigoDomicilioBaja').AsString),
                                            '', '', '', '', '', '', '');
                                    end;
                                    AgregarXMLDomicilio(ContratoXML,
                                      ModificadosDomiciliosRNUT.FieldByName('AccionDomicilioFacturacion').AsString,
                                      ModificadosDomiciliosRNUT.FieldByName('CodigoDomicilioFacturacion').AsString,
                                      XML_DIRECCION_FACTURACION,
                                      ModificadosDomiciliosRNUT.FieldByName('DescriCalle').AsString,
                                      ModificadosDomiciliosRNUT.fieldByName('DescriComuna').AsString,
                                      ModificadosDomiciliosRNUT.fieldByName('Numero').AsString,
                                      '',
                                      ModificadosDomiciliosRNUT.fieldByName('CodigoPostal').AsString,
                                      ModificadosDomiciliosRNUT.fieldByName('Detalle').AsString
                                    );
                                end else begin
//                                    raise Exception.Create(format(MSG_ERROR_OPENTABLE,['No se encontr� el domicilio del convenio - Convenio Numero: ' + NumeroConvenio]));

                                    // Colocar un Alta de Domicilio en el XML.
                                    // Obtener los datos, que existen en el CAC, del Domicilio del Convenio.
                                    ObtenerDomicilioConvenio.Close;
                                    ObtenerDomicilioConvenio.Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
                                    ObtenerDomicilioConvenio.Open;
                                    try

                                        if ObtenerDomicilioConvenio.RecordCount < 1 then begin
                                            ObtenerDomicilioConvenio.Close;
                                            raise Exception.Create(Format(MSG_ERROR_OPENTABLE,
                                                ['No se encontr�, en el CAC, el domicilio del convenio - Convenio Numero: '+NumeroConvenio]));
                                        end;

                                        AgregarXMLDomicilio(ContratoXML,
                                            XML_ALTA,
                                            Trim(ObtenerDomicilioConvenio.fieldByName('CodigoDomicilio').AsString),
                                            XML_DIRECCION_FACTURACION,
                                            iif(not ObtenerDomicilioConvenio.fieldByName('Normalizado').AsBoolean,
                                                  ObtenerDomicilioConvenio.fieldByName('DescriCalle').AsString,
                                                  ObtenerDomicilioConvenio.fieldByName('CalleDesnormalizada').AsString),
                                            ObtenerDomicilioConvenio.fieldByName('DescriComuna').AsString,
                                            ObtenerDomicilioConvenio.fieldByName('Numero').AsString,
                                            '',
                                            ObtenerDomicilioConvenio.fieldByName('CodigoPostal').AsString,
                                            ObtenerDomicilioConvenio.fieldByName('Detalle').AsString);
                                    finally
                                        ObtenerDomicilioConvenio.Close;
                                    end; // finally
                                end;

                                //Domicilio
                               (*
                                MensajeProgreso := 'Obteniendo Domicilios Convenios';
                                ObtenerDomicilioConvenio.Close;
                                with ObtenerDomicilioConvenio.Parameters do begin
                                    ParamByName('@CodigoConvenio').Value:= CodigoConvenio;
                                end;
                                ObtenerDomicilioConvenio.open;

                                if ObtenerDomicilioConvenio.RecordCount < 1 then begin
                                    ObtenerDomicilioConvenio.Close;
                                    raise Exception.Create(format(MSG_ERROR_OPENTABLE,['No se encontr� el domicilio del convenio - Convenio Numero: '+NumeroConvenio]));
                                end;


                                if not(ObtenerDomicilioConvenio.Eof) then begin
                                    AgregarXMLDomicilio(ContratoXML,
                                      XML_MODIFICACION,
                                      Trim(ObtenerDomicilioConvenio.fieldByName('CodigoDomicilio').AsString),
                                      XML_DIRECCION_FACTURACION,
                                      iif(not ObtenerDomicilioConvenio.fieldByName('Normalizado').AsBoolean,
                                              ObtenerDomicilioConvenio.fieldByName('DescriCalle').AsString,
                                              ObtenerDomicilioConvenio.fieldByName('CalleDesnormalizada').AsString),
                                      ObtenerDomicilioConvenio.fieldByName('DescriComuna').AsString,
                                      ObtenerDomicilioConvenio.fieldByName('Numero').AsString,
                                      '',
                                      ObtenerDomicilioConvenio.fieldByName('CodigoPostal').AsString,
                                      ObtenerDomicilioConvenio.fieldByName('Detalle').AsString);
                                end;
                                ObtenerDomicilioConvenio.close;
                                *)

                                MensajeProgreso := 'Obteniendo Vehiculos Tags';
                                // Vehiculo

                                if (ModificadosCuentasRNUT.Locate('CodigoConvenio', CodigoConvenio, [loCaseInsensitive])) then begin
                                    while not(ModificadosCuentasRNUT.Eof) and (ModificadosCuentasRNUT.FieldByName('CodigoConvenio').AsInteger = CodigoConvenio) do begin

                                        VehiculoXML := AgregarXMLVehiculo(ContratoXML, ModificadosCuentasRNUT.fieldByname('AccionVehiculo').AsString,
                                                    ModificadosCuentasRNUT.fieldByname('Patente').AsString,
                                                    ModificadosCuentasRNUT.fieldByname('dv').AsString,
                                                    ModificadosCuentasRNUT.fieldByname('Categoria').AsString,
                                                    ModificadosCuentasRNUT.FieldByName('Marca').AsString,
                                                    ModificadosCuentasRNUT.FieldByName('Modelo').AsString,
                                                    ModificadosCuentasRNUT.FieldByName('Anio').AsString,
                                                    iif(ModificadosCuentasRNUT.FieldByName('FechaBajaCuenta').AsDateTime = Null, NullDate, ModificadosCuentasRNUT.FieldByName('FechaBajaCuenta').AsDateTime));

                                        AgregarXMLTag(VehiculoXML, ModificadosCuentasRNUT.fieldByname('AccionTag').AsString,
                                                    ModificadosCuentasRNUT.FieldByName('ContextMark').AsString,
                                                    '',
                                                    ModificadosCuentasRNUT.FieldByName('Contract').AsString,
                                                    ModificadosCuentasRNUT.FieldByName('ContextVersion').AsString,
                                                    ModificadosCuentasRNUT.FieldByName('ContractSerialNumber').AsString,
                                                    ModificadosCuentasRNUT.FieldByName('Fabricante').AsString,
                                                    ModificadosCuentasRNUT.FieldByName('Categoria').AsString,
                                                    ModificadosCuentasRNUT.FieldByName('Estado').AsString,
                                                    FormatDateTime('ddmmyyyy hhnnss', ModificadosCuentasRNUT.FieldByName('FechaInstalacion').AsDateTime)
                                                    );

                                        ModificadosCuentasRNUT.Next;
                                    end;
                                end;
                                Inc(CantidadCorrectos);
                            except
                                on E: Exception do begin
                                    MensajesError := MensajesError + 'Error procesando Convenio: ' + IntToStr(CodigoConvenio) + ' ' +
                                                                     E.Message + CRLF + '(' + MensajeProgreso + ')';
                                    Inc(CantidadError);
                                end;
                            end;
                            // fin
                            Next;
                        end;

                        MensajeFinal := 'Exportaci�n Modificaciones Terminada. ' + CRLF + 'Resultados: ' + CRLF +
                            ' - Registros exportados correctamente: ' + IntToStr(CantidadCorrectos) + CRLF +
                            ' - Registros con error (sin exportar): ' + IntToStr(CantidadError) + CRLF +
                            ' - Total de registros: ' + IntToStr(CantTotal);


                        if CantidadError = 0 then begin
                            // Grabar
//                            FileNameRNUT := 'entrada_1_' + IntToStr(CodigoEnvioConvenio) + '_' + FormatDateTime('yyyymmddhhnnss', FechaEnvio);
//                            ArchivoXML :=  GoodFileName(PathXML + FileNameRNUT, '.xml');
                            GrabarXML(XML, ContenedorXML, ArchivoXML);
                            // Actualizamos la fecha de envio

                            ActualizarConveniosSincronizados.Close;
                            ActualizarConveniosSincronizados.ExecProc;

                            MensajeProgreso := 'Actualizando Envio Convenio';
                            ActualizarEnvioConvenioRNUT.Parameters.ParamByName('@CodigoEnvioConvenioRNUT').Value := CodigoEnvioConvenio;
                            ActualizarEnvioConvenioRNUT.Parameters.ParamByName('@NombreArchivo').Value := FileNameRNUT;
                            ActualizarEnvioConvenioRNUT.execProc;
                            DMConnections.BaseCAC.CommitTrans;
                            result := ArchivoXML;
                            (*
                            if EnviarArchivoMail(ArchivoXML) or
                                EnviarArchivoFTP(ArchivoXML) then
                                RenameFile(ArchivoXML, PathXMLEnviado + ExtractFileName(ArchivoXML));
                            *)

                            EventLogReportEvent(elInformation, MensajeFinal, '');

                        end else begin
                            Result := '';
                            DMConnections.BaseCAC.RollbackTrans;
                            EventLogReportEvent(elError, MensajeFinal + CRLF + MensajesError, '');
                        end;
                    end;

                try
                    QueryExecute(DMConnections.BaseCAC, 'DROP TABLE ##ConvenioCN');
                except
                end;
                until (not ModificadosConvenioRNUT.Active) or (Principal.FCodigoConvenioModif = 0)
            end; //with ObtenerNovedadesConveniosRNUT
        except
            on E: Exception do begin
                //Si por alguna raz�n ca� ac� es que algo paso y como voy a hacer un rollback
                //por lo tanto el archivo que
                if FileExists(ArchivoXML) then RenameFile(ArchivoXML, ArchivoXML + '.old');
                if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.RollbackTrans;
                EventLogReportEvent(elError, E.message + CRLF + MensajeProgreso, '');
            end;
        end;
    finally
            try
                QueryExecute(DMConnections.BaseCAC, 'DROP TABLE ##ConvenioCN');
            except
            end;
            ModificadosConvenioRNUT.Close;
    end;
end;}

procedure TdmSyncMOPT.AgregarRepresentantesLegales(EntidadXML,ContratoXML : IXMLNode; Accion, NumeroDocumento, dv, Nombre, Apellido, ApellidoMaterno: String);
begin

    EntidadXML:=AgregarXMLEntidad(ContratoXML, Accion,
                    NumeroDocumento,
                    dv,
                    XML_PERSONERIA_NATURAL,
                    Nombre,
                    Apellido,
                    ApellidoMaterno,
                    '',
                    XML_ROL_REPRESENTATE_LEGAL,
                    ObtenerDatosPersona.FieldByName('Giro').AsString);

end;

function TdmSyncMOPT.CambiarEstadoTag: string;
var
    XML: IXMLDocument;
    ContenedorXML : IXMLNode;
    FileNameRNUT, PathXML : AnsiString;
    CodigoEnvioConvenio, CantTotal, CantidadError,
    CantidadCorrectos : Integer;
    FechaEnvio: TDateTime;

    ArchivoXML : string;
    MensajeFinal, MensajesError : string;
    PathXMLEnviado : string;
    MensajeProgreso : string;

    AccionActual, ColorActual : Integer;


begin
    CantTotal:=0; CantidadError := 0; CantidadCorrectos := 0;

    MensajeFinal := ''; MensajesError := '';

    try
        try
            PathXML := ObtenerDirectorio(XML_PARAMETRO_ARCHIVO_XML);
            PathXMLEnviado := ObtenerDirectorio(XML_PATH_ARCHIVO_SALIDA_XML_ENVIADOS);

            with ObtenerTagPendientes do begin
                //Este repeat es por si se quiere usar un top en el ObtenerNovedadesActionListRNUT
//                repeat
                    MensajeProgreso := 'Obteniendo Telev�as en Pendientes';
                    close;
                    open;

                    if RecordCount = 0 then Exit;
                    if not DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.BeginTrans;

                    FechaEnvio := NowBase(Connection);

                    // Registro de Envio
                    CodigoEnvioConvenio := 0 ; //AgregarEnvioConvenio(usuarioSistema);
                    //no se usa mas JRC

                    XML := CrearXML;
                    ContenedorXML := CrearContenedor(XML);

                    ColorActual  := 0;
                    AccionActual := 0;

                    while (not(EOF)) do begin
                        try
                                Inc(CantTotal);

                                AgregarXMLTag(ContenedorXML, XML_MODIFICACION,
                                            '', //ObtenerTagPendientes.FieldByName('context').AsString,
                                            '',
                                            '', //ObtenerTagPendientes.FieldByName('Contract').AsString,
                                            '', //ObtenerTagPendientes.FieldByName('ContextVersion').AsString,
                                            inttostr(EtiquetaToSerialNumber(ObtenerTagPendientes.FieldByName('Serie').AsString)),
                                            ObtenerTagPendientes.FieldByName('id_fabricante').AsString,
                                            '', //ObtenerTagPendientes.FieldByName('id_categoria_urbana').AsString,
                                            XML_ESTADO_TAG_DISPONIBLE,
                                            '' //FormatDateTime('ddmmyyyy hhnnss', ObtenerTagPendientes.FieldByName('FechaInstalacion').AsDateTime)
                                            );

                                Inc(CantidadCorrectos);

                        except
                                on E: Exception do begin
                                    MensajesError := MensajesError + 'Error procesando cambio estado - ' + IntToStr(ColorActual) +
                                                                     '  Accion - ' + IntToStr(AccionActual) +
                                                                     '  Tag - ' + SerialNumberToEtiqueta(FieldByName('ContractSerialNumber').AsString) + CRLF +
                                                                     E.Message;
                                    Inc(CantidadError);
                                end;
                        end;

                        Next;
                    end;



                    MensajeFinal := 'Exportaci�n Cambio Estado Terminada. ' + CRLF + 'Resultados: ' + CRLF +
                        ' - Registros exportados correctamente: ' + IntToStr(CantidadCorrectos) + CRLF +
                        ' - Registros con error (sin exportar): ' + IntToStr(CantidadError) + CRLF +
                        ' - Total de registros: ' + IntToStr(CantTotal);

                    if CantidadError = 0 then begin
                        // Grabar
                        FileNameRNUT := 'entrada_' + IntToStr(ObtenerCodigoConcesionariaNativa()) + '_' + IntToStr(CodigoEnvioConvenio) + '_' + FormatDateTime('yyyymmddhhnnss', FechaEnvio);           //SS_1147_MCA_20150519
                        ArchivoXML :=  GoodFileName(PathXML + FileNameRNUT, '.xml');
                        GrabarXML(XML, ContenedorXML, ArchivoXML);
                        // Actualizamos la fecha de envio

                        MensajeProgreso := 'Actualizando Envio Televias Pendientes';
                        ActualizarEnvioConvenioRNUT.Parameters.ParamByName('@CodigoEnvioConvenioRNUT').Value := CodigoEnvioConvenio;
                        ActualizarEnvioConvenioRNUT.Parameters.ParamByName('@NombreArchivo').Value := FileNameRNUT;
                        ActualizarEnvioConvenioRNUT.execProc;

                        //Actualiza los ya enviados.
//                        ActualizarListActionRNUT.ExecProc;

                        DMConnections.BaseCAC.CommitTrans;

                        result := ArchivoXML;

                        (*
                        if EnviarArchivoMail(ArchivoXML) or
                            EnviarArchivoFTP(ArchivoXML) then
                            RenameFile(ArchivoXML, PathXMLEnviado + ExtractFileName(ArchivoXML));
                        *)

                        EventLogReportEvent(elInformation, MensajeFinal, '');

                    end else begin
                        result := '';
                        DMConnections.BaseCAC.RollbackTrans;
                        EventLogReportEvent(elError, MensajeFinal + CRLF + MensajesError, '');
                    end;
            end;
        except
            on E: Exception do begin
                if DMConnections.BaseCAC.InTransaction then DMConnections.BaseCAC.RollbackTrans;
                EventLogReportEvent(elError, E.message + CRLF + MensajeProgreso, '');
            end;
        end;
    finally
            ObtenerTagPendientes.Close;
    end;

end;

{******************************** Function Header ******************************
Procedure Name: VerificarSemaforo
Author :
Date Created :
Parameters : Semaforo: string; Entrar: boolean; var ProduceExcepcion: boolean
Return Value : boolean
Description :

Revision : 1
    Author : pdominguez
    Date   : 10/09/2009
    Description : SS 801
        - Se a�aden las variables NombreEquipo y EsServicio.
        - Se a�ade la asignaci�n de los parametros @HostName y @IsService al
        SP SincroEntrar.
*******************************************************************************}
function TdmSyncMOPT.VerificarSemaforo(Semaforo: string; Entrar: boolean; var ProduceExcepcion: boolean): boolean;
    var
        NombreEquipo: String; // Rev. 1 (SS 801)
        EsServicio: Boolean;  // Rev. 1 (SS 801)
begin
    result := false;
    RegistrarLineaLog('TdmSyncMOPT.VerificarSemaforo: Inicio');
    NombreEquipo := GetMachineName;     // Rev. 1 (SS 801)
    EsServicio := IsServiceApplication; // Rev. 1 (SS 801)
    ProduceExcepcion := false;
    try
        SincroEntrar.Close;
        SincroEntrar.Parameters.ParamByName('@Semaforo').Value := Semaforo;
        SincroEntrar.Parameters.ParamByName('@HostName').Value := NombreEquipo; // Rev. 1 (SS 801)
        SincroEntrar.Parameters.ParamByName('@IsService').Value := EsServicio;  // Rev. 1 (SS 801)
        SincroEntrar.Parameters.ParamByName('@Entrar').Value := Entrar;
        SincroEntrar.Parameters.ParamByName('@Entro').Value := NULL;
        RegistrarLineaLog('TdmSyncMOPT.VerificarSemaforo: Llamando al SP SincroEntrar @Semaforo=' + Semaforo + ', @Entrar=' + BoolToStr(Entrar, True) + ', @Entro=NULL');
        SincroEntrar.ExecProc;
        RegistrarLineaLog('TdmSyncMOPT.VerificarSemaforo: Fin llamada SP SincroEntrar');
        result := SincroEntrar.Parameters.ParamByName('@Entro').Value;
        RegistrarLineaLog('TdmSyncMOPT.VerificarSemaforo: Resultado de SP SincroEntrar @Entro=' + BoolToStr(result, True));
    except
        on E: Exception do begin
            ProduceExcepcion :=  true;
            EventLogReportEvent(elError, 'Error al Verificar Semaforo' + CRLF + E.Message, '');
        end;
    end;
    RegistrarLineaLog('TdmSyncMOPT.VerificarSemaforo: Fin');
end;

// Indica si ya se cargaron las respuestas para este Codigo de Envio
function TdmSyncMOPT.RespuestaYaPrecesadas(
  CodigoEnvioConvenioRNUT: Integer): Boolean;
begin

    with VerificarRespuestasCargadas do begin
        Parameters.ParamByName('@CodigoEnvioConvenioRNUT').Value := CodigoEnvioConvenioRNUT;
        Open;
	    Result :=  iif(FieldByName('Respuesta').AsInteger = 1,true , false);
        close;
    end;
end;

procedure TdmSyncMOPT.ActualizarRespuestaLista(Etiqueta: String;
  CodigoEnvioRNUT, CodigoRespuesta: integer; Complemento: string);
begin
    with ActualizarRespuestaListaEnviosRNUT, ActualizarRespuestaListaEnviosRNUT.Parameters do begin
        ParamByName('@ContractSerialNumber').Value := EtiquetaToSerialNumber(Trim(Etiqueta));
        ParamByName('@CodigoEnvioConvenioRNUT').Value := CodigoEnvioRNUT;
        ParamByName('@CodigoRespuesta').Value := CodigoRespuesta;
        ParamByName('@Complemento').Value := Complemento;
        ExecProc;
    end;
end;

procedure TdmSyncMOPT.ActualizarEnvioTelevia(CodigoEnvioRnut: integer;
  ContractSerialNumber: Int64; Color: integer);
begin
    RegistrarLineaLog('TdmSyncMOPT.AgregarEnvioConvenio: Inicio ejecucion de SP ' + spActualizarEnvioTelevia.ProcedureName + ' @CodigoEnvioConvenioRNUT=' + IntToStr(CodigoEnvioRnut) + ', @ContractSerialNumber=' + IntToStr(ContractSerialNumber) + ', @Color=' + IntToStr(Color));
    with spActualizarEnvioTelevia.Parameters do begin
        ParamByName('@CodigoEnvioConvenioRNUT').Value := CodigoEnvioRnut;
        ParamByName('@ContractSerialNumber').Value := ContractSerialNumber;
        ParamByName('@Color').Value := Color;
        spActualizarEnvioTelevia.ExecProc;
    end;
    RegistrarLineaLog('TdmSyncMOPT.AgregarEnvioConvenio: Fin ejecucion de SP ' + spActualizarEnvioTelevia.ProcedureName);
end;

procedure TdmSyncMOPT.ProcesarImportacionLista;
{
var
    Entro: Boolean;
}
begin
{
    Entro := VerificarSemaforo(CONST_RNUT, True);
    if Entro then begin
        //Establezco un minimo de media hora
        if ((Principal.FFechaHoraUltimaEjecucionLista = nulldate) or
            (strtoint(FormatDatetime('nn',NowBase(DMConnections.BaseCAC) - Principal.FFechaHoraUltimaEjecucionLista)) >= 30)) then begin

            Principal.FFechaHoraUltimaEjecucionLista := NowBase(DMConnections.BaseCAC);
            try
                ImportarListasRNUT.ExecProc;
            except
                on e: Exception do begin
                    EventLogReportEvent(elError, E.message + CRLF + 'Importando Lista del RNUT', '');
                end;
            end;
        end;
        VerificarSemaforo(CONST_RNUT, False);
    end;
}
end;


end.




