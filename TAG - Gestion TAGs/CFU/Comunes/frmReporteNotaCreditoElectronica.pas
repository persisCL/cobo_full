{-----------------------------------------------------------------------------
  Revision : 1
 	Author: Nelson Droguett Sierra
    Date: 24-04-2009
    Description:En el Ejecutar es opcional el dialogo de configuracion
                (aunque por defecto es TRUE), asi se puede llamar desde
                el mailer sin que salga el dialogo de configuracion.

  Revision 2
  Author: mbecerra
  Date: 12-Junio-2009
  Description:	Se cambia la inicializaci�n de la Nota de Cr�dito Electr�nica
            	para que sea llamada desde la WEB.

  Revision 3
  Author: Nelson Droguett Sierra
  Date: 29-Enero-2010
  Description: Se hacen publicos los siguientes elementos
                FDirImagenFondo: string;
                FNombreImagenFondo: string;
                function Inicializar(Conexion: TADOConnection; TipoComprobante: string; NumeroComprobante: int64; var Error: string;esWEB : boolean = False ): Boolean;
                function ObtenerTimbreElectronico:Boolean;

                Para ser llamados desde la nueva GeneracionPDF para el Mailer.
                Esto porque al ejecutar el reporte levantaba una ventana de preview, lo que para la ejecucion
                como servicio era incorrecto. Al dejarlas publicas, no se cambio la programacion de lo que
                estaba, para no alterar el funcionamiento de otros modulos que usan el reporte, pero si
                se puede llamar a su ejecucion de otra manera sin tener que ver la previsualizacion.

                Se quito la funcion Preparar, ya que no hacia nada :)


 Revision 4
 Author: mbecerra
 Date: 03-Febrero-2010
 Description:	Se agrega una funci�n que borre el archivo JPG generado para el
            	Timbre electr�nico.

                Se agrega la llamada a esta funci�n en la funci�n Ejecutar, para que
                borre el archivo autom�ticamente cuando imprime el Reporte.

                Se crea esta funci�n ya que los servicios de Mailer no usan la
                funci�n Ejecutar. De esta manera se puede invocar despu�s de
                generar el archivo PDF.


Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.

Firma		:	SS_1147_MBE_20150429
Description	:	Se colocan las direcciones de VS.

-----------------------------------------------------------------------------}
unit frmReporteNotaCreditoElectronica;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ppDB, ppDBPipe, ppParameter, ppBands, ppModule, raCodMod,
  ppReport, ppSubRpt, ppCtrls, ppPrnabl, ppClass, ppStrtch, ppRichTx, ppCache,
  ppComm, ppRelatv, ppProd,utildb, ADODB,UtilProc, UtilRB, jpeg, Util,ConstParametrosGenerales,
  DTEControlDLL,PeaTypes,StrUtils, DBClient;

type
  TReporteNotaCreditoElectronicaForm = class(TForm)
    rbiCK: TRBInterface;
    ppCK: TppDBPipeline;
    spComprobantes: TADOStoredProc;
    sdCK: TDataSource;
    ppLineasCKDBPipeline: TppDBPipeline;
    dsLineasImpresionCK: TDataSource;
    spObtenerLineasImpresionCK: TADOStoredProc;
    spObtenerLineasImpresionCKDescripcion: TStringField;
    spObtenerLineasImpresionCKImporte: TLargeintField;
    spObtenerLineasImpresionCKDescImporte: TStringField;
    spObtenerLineasImpresionCKCodigoConcepto: TWordField;
    spObtenerDatosComprobanteParaTimbreElectronico: TADOStoredProc;
    StringField1: TStringField;
    LargeintField1: TLargeintField;
    StringField2: TStringField;
    WordField1: TWordField;
    rptCK: TppReport;
    ppParameterList2: TppParameterList;
    spObtenerMaestroConcesionaria: TADOStoredProc;
    ppDetailBand3: TppDetailBand;
    ppImageFondo: TppImage;
    ppShape1: TppShape;
    ppRutConcesionaria: TppLabel;
    ppTipoDocumentoElectronico: TppLabel;
    ppLabel1: TppLabel;
    ppDBText3: TppDBText;
    ppdbtNombre: TppDBText;
    ppdbtDir1: TppDBText;
    ppdbtDir2: TppDBText;
    ppDBText4: TppDBText;
    ppDBText1: TppDBText;
    ppDBText11: TppDBText;
    ppDBText12: TppDBText;
    ppLabel5: TppLabel;
    ppDBText5: TppDBText;
    ppDBText7: TppDBText;
    ppLabel3: TppLabel;
    ppDBText6: TppDBText;
    ppLabel8: TppLabel;
    ppDBText10: TppDBText;
    ppImage2: TppImage;
    ppResolucionSII: TppLabel;
    pplblUltimoAjuste: TppLabel;
    ppSubReport1: TppSubReport;
    ppChildReport1: TppChildReport;
    ppDetailBand2: TppDetailBand;
    ppDBText8: TppDBText;
    ppDBText9: TppDBText;
    raCodeModule1: TraCodeModule;
    ppDBText2: TppDBText;
    ppLabel10: TppLabel;
    ppLabel2: TppLabel;
    ppLabel4: TppLabel;
    ppLabel9: TppLabel;
    ppLabel6: TppLabel;
    raCodeModule3: TraCodeModule;
    plbl1: TppLabel;
    ppDBTotalPipeline: TppDBPipeline;
    cdsDetalle: TClientDataSet;
    cdsTotal: TClientDataSet;
    dsTotal: TDataSource;
    ppSubReport2: TppSubReport;
    ppChildReport2: TppChildReport;
    ppDetailBand1: TppDetailBand;
    ppDBText13: TppDBText;
    ppDBText14: TppDBText;
    raCodeModule2: TraCodeModule;
  private
	{ Private declarations }
    FGlosaResolucionSII,
    FTipoComprobante: String;
    FNumeroComprobante: Int64;
    FRutaImagenTimbreElectronico : string;		//REV.4
  public
	{ Public declarations }
    // Rev.3 / 29-Enero-2010 / Nelson Droguett Sierra. -----------------------------------------------------
	FDirImagenFondo: string;
	FNombreImagenFondo: string;
	FImprimioComprobante: Boolean;
	function Inicializar(Conexion: TADOConnection; TipoComprobante: string; NumeroComprobante: int64; var Error: string;esWEB : boolean = False ): Boolean;
  function ObtenerTimbreElectronico:Boolean;
    //procedure Preparar;

  // Rev.1 / 24-04-2009 / Nelson Droguett Sierra
	//function Ejecutar:boolean; //(PRN_NK, BIN_NK, PRN_DETCON, BIN_DETCON: String): Boolean;
	function Ejecutar(bMostrarDialog:Boolean=True):boolean; //(PRN_NK, BIN_NK, PRN_DETCON, BIN_DETCON: String): Boolean;

    //REV.4
    function EliminarImagenTimbreElectronico : boolean;

  end;

var
  ReporteNotaCreditoElectronicaForm: TReporteNotaCreditoElectronicaForm;

implementation

{$R *.dfm}

{ TfrmReporteCK }

function TReporteNotaCreditoElectronicaForm.Inicializar;  //REV.2 ( Conexion: TADOConnection; TipoComprobante: string; NumeroComprobante: int64; var Error: string): Boolean;
resourcestring
	  MSG_MISSED_IMAGES_FILES = 'No existen todas las im�genes requeridas para '
		+ CRLF + 'poder imprimir una Nota de Cr�dito a Nota de Cobro en el sistema';
	  MSG_ERROR = 'Error';
    MSG_ERROR_OBTENCION_DATOS_CONFIG_REPORTE = 'Error al obtener datos de configuraci�n del reporte.'
                                                + CRLF + 'Consulte al Administrador de Sistemas';
    MSG_ERROR_OBTENER_FECHA_CORTE_RUT = 'Ha ocurrido un error al obtener la fecha desde la cual la concesionaria cambio el RUT.';
    MSG_ERROR_OBTENER_RUT_ANTERIOR  = 'Ha ocurrido un error al obtener el RUT Anterior de la concesionaria.';
    MSG_ERROR_OBTENER_RUT           = 'Ha ocurrido un error al obtener el RUT de la concesionaria.';
    MSG_RUT                         = 'RUT: ';
    MSG_ERROR_CONFIG_REPORTE = 'Error al configurar las componentes utilizadas en el reporte';
    MSG_ERROR_ConfigurarVariable_EGATE_HOME = 'Ha ocurrido un error al intentar configurar la variable ambiental para timbraje electr�nico';
    MSG_ERROR_AL_CARGAR_DLL = 'Ha ocurrido un error al cargar la DLL para timbraje electr�nico';
var
    FechaCreacion , FechaDeCorte : TDateTime;
    Mensaje : string;
    Cod : integer;
    sRazonSocial,sRut,sDireccion,sLocalidad: AnsiString;
begin
	try

		spComprobantes.Connection									:= Conexion;
		spObtenerLineasImpresionCK.Connection						:= Conexion;
        spObtenerDatosComprobanteParaTimbreElectronico.Connection	:= Conexion;
        spObtenerMaestroConcesionaria.Connection					:= Conexion;


        spObtenerMaestroConcesionaria.Close;
        spObtenerMaestroConcesionaria.Parameters.Refresh;
        spObtenerMaestroConcesionaria.Parameters.ParamByName('@CodigoConcesionaria').Value := QueryGetValueInt(Conexion, 'SELECT dbo.ObtenerConcesionariaNativa()');         
        spObtenerMaestroConcesionaria.Open;
        {INICIO: TASK_058_JMA_20161014      - Los datos estan directamente en el reporte
            sRazonSocial  := FieldByName('RazonSocialConcesionaria').AsString;

            sRut          := 'RUT : ' + FormatFloat('#.#',FieldByName('RutConcesionaria').AsInteger) + '-' +
                           FieldByName('DigitoVerificadorConcesionaria').AsString + ' ' +
                           'Giro :' + Trim(FieldByName('GiroConcesionaria').AsString);

            sDireccion    := Trim(FieldByName('DireccionConcesionaria').AsString) + ' - ' +
                           Trim(FieldByName('Comuna').AsString) + ' - ' +
                           Trim(FieldByName('Region').AsString)+ ' - ' +
                           Trim(FieldByName('Pais').AsString);


            ppRazonSocial.Caption := sRazonSocial;
            ppRutGiro.Caption := sRut;
            ppDireccionCasaMatriz.Caption := sDireccion;


            if Parameters.ParamByName('@CodigoConcesionaria').Value = CODIGO_CN then
            begin
            ppTelefonosCasaMatriz.Caption:='Tel�fono : (56-2) 2 490 0900 / Fax : (56-2) 490 0748';
            ppSucursal1.Caption := 'Costanera Norte - Sta. Mar�a 5621 A - 5621 B - Vitacura - Santiago';
            ppSucursal2.Caption := 'Avda. Kennedy 5413 Local S 7276 - Las Condes - Santiago';
            ppSucursal3.Caption := 'Av. Bicentenario 3800 - Centro C�vico Municipalidad - Vitacura';
            ppSucursal4.Caption := 'Estaci�n Metro U. de Chile l�nea 1, local 3 - Santiago ';
            ppSucursal5.Caption := 'Av. Holanda 1998, Providencia - Santiago (Sucursal no realiza atenci�n de p�blico)';
            ppSucursal6.Caption := 'Acceso Vial AMB - Calzada Poniente - Pudahuel';
            ppSucursal7.Caption := 'Bodega : Las Alpacas 903 - Renca';
            end
            else if Parameters.ParamByName('@CodigoConcesionaria').Value = CODIGO_VS then
            begin
            ppTelefonosCasaMatriz.Caption:='(56 2) 2 694 3500';                               								                // SS_1147_MBE_20150429
            ppSucursal1.Caption := '';                                                                 						            //SS_1371_NDR_20150827 SS_1147_MBE_20150429
            ppSucursal2.Caption := 'Av. Am�rico Vespucio 4665 - Macul - Santiago';                                            //SS_1371_NDR_20150827 SS_1147_MBE_20150429
            ppSucursal3.Caption := 'Av. Am�rico Vespucio 1501, Mall Plaza Oeste, Local 133 - Cerrillos - Santiago';           //SS_1371_NDR_20150827 SS_1147_MBE_20150429
            ppSucursal4.Caption := 'Mar Tirreno 3349, Mall Paseo Quil�n, Local 1123 - Pe�alol�n - Santiago';		               //SS_1371_NDR_20150827 SS_1147_MBE_20150429
            ppSucursal5.Caption := 'Metro Estaci�n Universidad de Chile - L�nea 1, Local 3 - Santiago';                       //SS_1371_NDR_20150827 SS_1147_MBE_20150429
            ppSucursal6.Caption := '';                                                                 						            // SS_1147_MBE_20150429
            ppSucursal7.Caption := '';                                                                 						            // SS_1147_MBE_20150429
            end;
        TERMINO: TASK_058_JMA_20161014}




		result := True;
		FTipoComprobante := TipoComprobante;
		FNumeroComprobante := NumeroComprobante;
        if not ObtenerParametroGeneral(Conexion, 'RESOLUCION_SII_FACTURA_ELECTRONICA', FGlosaResolucionSII) then
            FGlosaResolucionSII := '<Sin Glosa>';
        ppResolucionSII.Caption := FGlosaResolucionSII;

         //revision :1
        try
            ObtenerParametroGeneral(Conexion, 'FECHA_DE_CORTE_RUT', FechaDeCorte);
         except
            on E: Exception do begin
                //mensaje de error
                // se supone q se puede consultar el recibo sino no se
                // podria imprimir , VER si salimos o seguimos sin el rut
                MsgBoxErr(MSG_ERROR_OBTENER_FECHA_CORTE_RUT, E.Message, Self.Caption, MB_ICONSTOP);
                Result := False;
                Exit;
            end;
        end;
        try
            FechaCreacion := QueryGetValueDateTime(spComprobantes.Connection,
                ' select FechaCreacion '+
                ' from Comprobantes with (nolock) '+
                ' where(NumeroComprobante='''+ IntToStr(FNumeroComprobante) +''')'+
                       ' and(TipoComprobante = '''+ FTipoComprobante +''')');
        except
            on E: Exception do begin
                //mensaje de error
                // se supone q se puede consultar el recibo sino no se
                // podria imprimir , VER si salimos o seguimos sin el rut
                MsgBoxErr(MSG_ERROR_OBTENCION_DATOS_CONFIG_REPORTE, E.Message, Self.Caption, MB_ICONSTOP);
                Result := False;
                Exit;
            end;
        end;

        if ( FechaCreacion < FechaDeCorte )then begin
            try
                ObtenerParametroGeneral(Conexion, 'IMG_FONDO_NOTA_CREDITO_CK_ANTERIOR', FNombreImagenFondo);
            except
                on E: Exception do begin
                    //mensaje de error
                    // se supone q se puede consultar el recibo sino no se
                    // podria imprimir , VER si salimos o seguimos sin el rut
                    MsgBoxErr(MSG_ERROR_OBTENCION_DATOS_CONFIG_REPORTE, E.Message, Self.Caption, MB_ICONSTOP);
                    Result := False;
                    Exit;
                end;
            end;

        end else begin
             try
                //ObtenerParametroGeneral(Conexion, 'IMAGEN_FONDO_NOTA_CREDITO_CK', FNombreImagenFondo);
                ObtenerParametroGeneral(Conexion, 'IMAGEN_FONDO_NOTA_CREDITO_DEBITO',FNombreImagenFondo);
             except
                on E: Exception do begin
                    //mensaje de error
                    // se supone q se puede consultar el recibo sino no se
                    // podria imprimir , VER si salimos o seguimos sin el rut
                    MsgBoxErr(MSG_ERROR_OBTENCION_DATOS_CONFIG_REPORTE, E.Message, Self.Caption, MB_ICONSTOP);
                    Result := False;
                    Exit;
                end;
             end;
        end;

        try
            ObtenerParametroGeneral(Conexion, 'DIR_IMAGEN_FONDO_NOTA_CREDITO_CK', FDirImagenFondo);
        except
            on E: Exception do begin
                //mensaje de error
                // se supone q se puede consultar el recibo sino no se
                // podria imprimir , VER si salimos o seguimos sin el rut
                MsgBoxErr(MSG_ERROR_OBTENCION_DATOS_CONFIG_REPORTE, E.Message, Self.Caption, MB_ICONSTOP);
                Result := False;
                Exit;
            end;
        end;
//		ObtenerParametroGeneral(Conexion, 'NOMBRE_IMAGEN_FONDO_NOTA_CREDITO_CK', FNombreImagenFondo);
        //fin revision :1

		FDirImagenFondo := GoodDir(FDirImagenFondo);
		FNombreImagenFondo := FDirImagenFondo + FNombreImagenFondo;
		// Fondo
		if (not FileExists(FNombreImagenFondo)) then begin
			raise Exception.Create(MSG_MISSED_IMAGES_FILES);
		end;
        // seteo la variable EGATE_HOME utilizada por la DLL DBNet para generar timbre electronico
        try
            if not ConfigurarVariable_EGATE_HOME(Conexion, esWEB, Mensaje) then
                MsgBoxErr(MSG_ERROR_ConfigurarVariable_EGATE_HOME, Mensaje, Self.Caption, MB_ICONSTOP);
        except
            on E: Exception do begin
                //mensaje de error
                // se supone q se puede consultar el recibo sino no se
                // podria imprimir , VER si salimos o seguimos sin el rut
                MsgBoxErr(MSG_ERROR_CONFIG_REPORTE, E.Message, Self.Caption, MB_ICONSTOP);
                Result := False;
                Exit;
            end;
        end;
        // cargo la DLL DBNet para generar timbre electronico
        try
	        Cod := CargarDLL();
            if Cod <> 0 then MsgBoxErr(MSG_ERROR_AL_CARGAR_DLL, ObtieneErrorAlCargarDLL(Cod), Self.Caption, MB_ICONSTOP);
        except
            on E: Exception do begin
                //mensaje de error
                MsgBoxErr(MSG_ERROR_CONFIG_REPORTE, E.Message, Self.Caption, MB_ICONSTOP);
                Result := False;
                Exit;
            end;
        end;
	except
		on e:Exception do begin
			result := false;
			Error := e.message;
		end;
	end;

end;

// Rev 1. / 24-04-2009 / Nelson Droguett Sierra
// function TReporteNotaCreditoElectronicaForm.Ejecutar: boolean;
function TReporteNotaCreditoElectronicaForm.Ejecutar(bMostrarDialog:Boolean=True): boolean;
resourcestring
    MSG_ERROR_AL_TIMBRAR = 'Se produjo un error al timbrar el documento';
    var
{INICIO: TASK_060_JMA_20161020}
    descripcion: string;
    orden : SmallInt;
{TERMINO: TASK_060_JMA_20161020}
begin
	ppImageFondo.Picture.LoadFromFile(FNombreImagenFondo);


	spComprobantes.Close;
	spComprobantes.Parameters.ParamByName('@TipoComprobante').Value:= FTipoComprobante;
	spComprobantes.Parameters.ParamByName('@NumeroComprobante').Value:= FNumeroComprobante;
	spComprobantes.Open;

	spObtenerLineasImpresionCK.Close;
	spObtenerLineasImpresionCK.Parameters.ParamByName('@TipoComprobante').Value:= FTipoComprobante;
	spObtenerLineasImpresionCK.Parameters.ParamByName('@NumeroComprobante').Value:= FNumeroComprobante;
	spObtenerLineasImpresionCK.Open;
	
{INICIO: TASK_058_JMA_20161014}
    spObtenerLineasImpresionCK.First;
    cdsTotal.Close;
    cdsTotal.Open;
    cdsDetalle.close;
    cdsDetalle.Open;
    cdsTotal.IndexFieldNames := 'Orden'; //TASK_060_JMA_20161020
    while Not spObtenerLineasImpresionCK.Eof do
    begin
{INICIO: TASK_060_JMA_20161020}
        descripcion := spObtenerLineasImpresionCK.Fields.FieldByName('Descripcion').Value;
        if ContainsStr(descripcion, 'TOTAL') OR ContainsStr(descripcion, 'IVA') then
        begin
            if descripcion = 'TOTAL EXENTO' then
                orden := 1
            else if descripcion = 'TOTAL NETO' then
                orden := 2
            else if ContainsStr(descripcion, 'IVA') then
                orden := 3
            else
                orden := 4;
            cdsTotal.InsertRecord([descripcion,spObtenerLineasImpresionCK.Fields.FieldByName('DescImporte').Value, orden])
        end
        else
            cdsDetalle.InsertRecord([descripcion,spObtenerLineasImpresionCK.Fields.FieldByName('DescImporte').Value]);
{TERMINO: TASK_060_JMA_20161020}
        spObtenerLineasImpresionCK.Next;
    end;

    spObtenerLineasImpresionCK.Close;
{TERMINO: TASK_058_JMA_20161014 }

    // obtengo el timbre electronico para la CK
    if ObtenerTimbreElectronico then begin
        // ejecuto el reporte
	    rbiCK.Execute(bMostrarDialog);

        //REV.4
        EliminarImagenTimbreElectronico();
        
        result := True;
    end
        else result := false;
end;


{--------------------------------------------------------------------------
                    EliminarImagenTimbreElectronico

Author: mbecerra
Date: 03-Febrero-2010
Description:	Borra la imagen JPG del Timbre Electr�nico

            	Retorna verdadero si la imagen no existe despu�s de borrarla.
                Falso en caso contrario
----------------------------------------------------------------------------}
function TReporteNotaCreditoElectronicaForm.EliminarImagenTimbreElectronico;
begin
    DeleteFile(FRutaImagenTimbreElectronico);
    Result := not FileExists(FRutaImagenTimbreElectronico);

end;

function TReporteNotaCreditoElectronicaForm.ObtenerTimbreElectronico;
resourcestring
    MSG_ERROR_SP = 'Se produjo un error al ejecutar el procedimiento almacenado';
    MSG_ERROR_AL_OBTENER_TIMBRE = 'Se produjo un error al obtener el timbre';
    MSG_ERROR_AL_LIBERAR_DLL = 'Se produjo un error al intentar liberar la DLL';
var
	RespuestaDLL    : TTimbreArchivo;
    ArchivoImg      : string;
    RutEmisor       : Integer;

begin

    try
	    spObtenerDatosComprobanteParaTimbreElectronico.Close;
	    spObtenerDatosComprobanteParaTimbreElectronico.Parameters.ParamByName('@TipoComprobante').Value:= FTipoComprobante;
	    spObtenerDatosComprobanteParaTimbreElectronico.Parameters.ParamByName('@NumeroComprobante').Value:= FNumeroComprobante;
	    spObtenerDatosComprobanteParaTimbreElectronico.ExecProc;
        Result := true;
        // Completo datos de la cabecera
        RutEmisor := spObtenerDatosComprobanteParaTimbreElectronico.Parameters.ParamByName('@RutEmisor').Value;
        ppRutConcesionaria.Caption :=	'R.U.T. ' +
                                        Format('%.0n', [1.0 * RutEmisor]) +
                                        '-' +
                                        spObtenerDatosComprobanteParaTimbreElectronico.Parameters.ParamByName('@DVRutEmisor').Value;
        ppRutConcesionaria.Caption := ReplaceText(ppRutConcesionaria.Caption,',','.');
        ppTipoDocumentoElectronico.Text := 'NOTA DE CR�DITO ELECTR�NICA';

    except
            on E: Exception do begin
                //mensaje de error
                MsgBoxErr(MSG_ERROR_SP, E.Message, Self.Caption, MB_ICONSTOP);
                Result := False;
                Exit;
            end
    end;

    try
        //obtengo el timbre con los datos del SP
    	if ObtenerTimbreDBNet(	spObtenerDatosComprobanteParaTimbreElectronico.Parameters.ParamByName('@RutEmisor').Value,
                                spObtenerDatosComprobanteParaTimbreElectronico.Parameters.ParamByName('@DVRutEmisor').Value,
                                spObtenerDatosComprobanteParaTimbreElectronico.Parameters.ParamByName('@CodigoTipoDocumentoElectronico').Value,
                                spObtenerDatosComprobanteParaTimbreElectronico.Parameters.ParamByName('@NumeroComprobanteFiscal').Value,
                                spObtenerDatosComprobanteParaTimbreElectronico.Parameters.ParamByName('@FechaEmision').Value,
                                spObtenerDatosComprobanteParaTimbreElectronico.Parameters.ParamByName('@TotalComprobante').Value,
                                spObtenerDatosComprobanteParaTimbreElectronico.Parameters.ParamByName('@DescripcionPrimerItem').Value,
                                spObtenerDatosComprobanteParaTimbreElectronico.Parameters.ParamByName('@RutReceptor').Value,
                                spObtenerDatosComprobanteParaTimbreElectronico.Parameters.ParamByName('@DVRutReceptor').Value,
                                spObtenerDatosComprobanteParaTimbreElectronico.Parameters.ParamByName('@RazonSocialONombrePersona').Value,
                                spObtenerDatosComprobanteParaTimbreElectronico.Parameters.ParamByName('@FechaTimbre').Value,
                                true,
                                RespuestaDLL
                            ) then begin

        	    ArchivoImg := PARAM_DIR_EGATE_HOME + 'out\html\' + RespuestaDLL.Archivo + '.jpg';
                FRutaImagenTimbreElectronico := ArchivoImg;    //REV.4
                if FileExists(ArchivoImg) then  ppImage2.Picture.LoadFromFile(ArchivoImg);
                Result := true;
        end;
    except
            on E: Exception do begin
                //mensaje de error
                MsgBoxErr(MSG_ERROR_AL_OBTENER_TIMBRE, E.Message, Self.Caption, MB_ICONSTOP);
                Result := False;
                Exit;
            end
    end;

    // libero la DLL luego de utilizarla
    try
        if LiberarDLL() then Result := true
    except
            on E: Exception do begin
                //mensaje de error
                MsgBoxErr(MSG_ERROR_AL_LIBERAR_DLL, E.Message, Self.Caption, MB_ICONSTOP);
                Result := False;
                Exit;
            end
    end;

end;


// Rev.3 / 29-Enero-2010 / Nelson Droguett Sierra --------------------------------------------------------------
//procedure TReporteNotaCreditoElectronicaForm.Preparar;
//resourcestring
//    TIPO_DOCUMENTO_FACTURA_AFECTA	= 'FACTURA ELECTRONICA';
//    TIPO_DOCUMENTO_FACTURA_EXENTA	= 'FACTURA NO AFECTA O EXENTA ELECTRONICA';
//    TIPO_DOCUMENTO_BOLETA_AFECTA	= 'BOLETA ELECTRONICA';
//    TIPO_DOCUMENTO_BOLETA_EXENTA	= 'BOLETA NO AFECTA O EXENTA ELECTRONICA';
//    TIPO_DOCUMENTO_NOTA_COBRO		= 'NOTA DE COBRO';
//
//    MSG_ERROR_TIMBRE	= 'No se pudo timbrar el documento electr�nico';
//    MSG_ERROR_IMAGEN	= 'No se encontr� el archivo %s';
//var
//    TmpFile: String;
//    CodigoTipoMedioPago: Byte;
//	OldDecimalSeparator : char;
//    OldThousandSeparator : char;
//    Desplazamiento: single;
//    oldHeight: single;
//    oldWidth: single;
//    CantVehiculos, RutEmisor: integer;
//    TipoDocumentoElectronico, DvEmisor : string;
//    Respuesta : TTimbreArchivo;
//begin
//
//end;
//
//procedure TReporteNotaCreditoElectronicaForm.rptCKBeforePrint(Sender: TObject);
//begin
//
//end;

end.
