{-------------------------------------------------------------------------------
 File Name: FreHistoriaOrdenServicio.pas
 Author: Lgisuk
 Date Created:  31/03/05
 Language: ES-AR
 Description: Seccion con la información de la Historia de la OS
-------------------------------------------------------------------------------}
unit FreHistoriaOrdenServicio;

interface

uses 
  //FreHistoriaordenServicio
  DMConnection,
  Utilproc,
  Util,                      //IIF
  //General
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, ListBoxEx, DBListEx, DB, ADODB;

type
  TFrameHistoriaOrdenServicio = class(TFrame)
    DBLHistoria: TDBListEx;
    Pencabezado: TPanel;
    Lmensaje: TLabel;
    Pizq: TPanel;
    Pder: TPanel;
    Parriva: TPanel;
    Pabajo: TPanel;
    SpObtenerHistoriaOrdenServicio: TADOStoredProc;
    DataSource: TDataSource;
    SpObtenerHistoriaOrdenServicioCodigoOrdenServicio: TIntegerField;
    SpObtenerHistoriaOrdenServicioFechaHoraModificacion: TDateTimeField;
    SpObtenerHistoriaOrdenServicioUsuario: TStringField;
    SpObtenerHistoriaOrdenServicioEstado: TStringField;
    SpObtenerHistoriaOrdenServicioPrioridad: TWordField;
    SpObtenerHistoriaOrdenServicioFechaCompromiso: TDateTimeField;
    SpObtenerHistoriaOrdenServicioDescEstado: TStringField;
    procedure DBLHistoriaDrawText(Sender: TCustomDBListEx;Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;var Text: String; var TxtRect: TRect; var ItemWidth: Integer;var DefaultDraw: Boolean);
    procedure DBLHistoriaColumns0HeaderClick(Sender: TObject);
  private
    { Private declarations }
  public
    function Inicializar(CodigoOrdenServicio:integer):boolean;
    { Public declarations }
  end;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    lgisuk
  Date Created: 31/03/2005
  Description: obtengo la historia de la orden de servicio
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFrameHistoriaOrdenServicio.Inicializar(CodigoOrdenServicio:integer): Boolean;
begin
    Result:=false;
    with SPObtenerHistoriaOrdenServicio.Parameters do begin
            refresh;
            parambyname('@CodigoOrdenServicio').value:= CodigoOrdenServicio;
    end;
    //ejecuto la consulta
    try
        with SPObtenerHistoriaOrdenServicio do begin
            Close;
            Open;
        end;
        Result := True;
    except
        on e: Exception do begin
            //el stored procedure podria fallar
            MsgBoxErr('Error', e.message, 'Error Al', MB_ICONERROR);
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: DBLHistoriaDrawText
  Author:    lgisuk
  Date Created: 31/03/2005
  Description:
  Parameters: Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;State: TOwnerDrawState; var Text: String; var TxtRect: TRect;var ItemWidth: Integer; var DefaultDraw: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrameHistoriaOrdenServicio.DBLHistoriaDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;State: TOwnerDrawState; var Text: String; var TxtRect: TRect;var ItemWidth: Integer; var DefaultDraw: Boolean);

    //descripcion de la prioridad
    Function Prioridad(codigo:integer):string;
    begin
        case codigo of
            1: result:='Muy Alta';
            2: result:='Alta';
            3: result:='Media';
            4: result:='Baja';
        end;
    end;

begin
    //FechaHoraModificación
    if Column = dblhistoria.Columns[0] then
       if text <> '' then Text := FormatDateTime('dd-mm-yyyy hh:nn', spObtenerHistoriaOrdenServicio.FieldByName('FechaHoraModificacion').AsDateTime);

    //prioridad
    if Column = dblhistoria.Columns[3] then
       Text := Prioridad(spObtenerHistoriaOrdenServicio.FieldByName('prioridad').Asinteger);


end;
{-----------------------------------------------------------------------------
  Function Name: DBLHistoriaColumns0HeaderClick
  Author:    lgisuk
  Date Created: 31/03/2005
  Description: Ordena por Columna
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrameHistoriaOrdenServicio.DBLHistoriaColumns0HeaderClick(Sender: TObject);
begin
  inherited;
    if spObtenerHistoriaOrdenServicio.Active = false then exit;
    if TDBListExColumn(sender).Sorting = csAscending then TDBListExColumn(sender).Sorting := csDescending
    else TDBListExColumn(sender).Sorting := csAscending;

    if spObtenerHistoriaOrdenServicio.RecordCount > 0 then
        spObtenerHistoriaOrdenServicio.Sort := TDBListExColumn(Sender).FieldName + iif(TDBListExColumn(sender).Sorting = csAscending, ' ASC' ,' DESC');
end;

end.
