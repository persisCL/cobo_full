{-----------------------------------------------------------------------------
 File Name      : Diccionario.pas
 Author         : Alejandro Labra
 Date Created   : 11-04-2011
 Description    : Unit que contiene la definición de la clase TDiccionario
                  La cual es una lista de TClaveValor.

 Description    : Se incluye metodo ObtenerValor.
 Firma          : PAR00133_COPAMB_ALA_20110801
-----------------------------------------------------------------------------}
unit Diccionario;

interface
uses
    ClaveValor,     // Unit que contiene la definición de la clase TClaveValor
    Classes, SysUtils;

type
    TDiccionario = class
        private
            FItems: TList;
            function SetDiccionario(index: integer): TClaveValor;
            function Setcount: integer;
        public
            property Items[index:integer]: TClaveValor read SetDiccionario;
            property Count: integer read Setcount;
            function Add(const ClaveValor: TClaveValor): integer; overload;
            function Add(Clave, Valor : String) : integer; overload;
            procedure Clear;
            function ObtenerValor(Clave : String) : String;                     //PAR00133_COPAMB_ALA_20110801
            constructor Create(); overload;
            destructor Destroy; override;
            function ObtenerItem(Clave :string): TClaveValor;
    end;

implementation

{ TDiccionario }

{-----------------------------------------------------------------------------
Function Name  : Add
Author         : Alejandro Labra   
Date Created   : 08-04-2011
Description    : Agrega un nuevo objeto TClaveValor a la lista del diccionario
-----------------------------------------------------------------------------}
function TDiccionario.Add(const ClaveValor: TClaveValor): integer;
begin
    Result := FItems.Add(ClaveValor);
end;

{-----------------------------------------------------------------------------
Function Name  : Add
Author         : Alejandro Labra   
Date Created   : 08-04-2011
Description    : Crea un nuevo objeto TClaveValor y lo agrega a la lista del
                 diccionario.
-----------------------------------------------------------------------------}
function TDiccionario.Add(Clave, Valor: String): integer;
begin
    Result := FItems.Add(TClaveValor.Create(Clave, Valor));
end;

{-----------------------------------------------------------------------------
Function Name  : Clear
Author         : Alejandro Labra   
Date Created   : 08-04-2011
Description    : Borra la lista de objetos TClaveValor
-----------------------------------------------------------------------------}
procedure TDiccionario.Clear;
begin
    FItems.Clear;
end;

{-----------------------------------------------------------------------------
Function Name  : Create
Author         : Alejandro Labra   
Date Created   : 08-04-2011
Description    : Sobrecarga del constructor de la clase para crear la lista que
                 almacenará los objetos TClaveValor.
-----------------------------------------------------------------------------}
constructor TDiccionario.Create;
begin
    FItems := TList.Create;
end;

{-----------------------------------------------------------------------------
Function Name  : Destroy
Author         : Alejandro Labra   
Date Created   : 08-04-2011
Description    : Sobrecarga del destructor de la clase para liberar de memoria
                 la lista de TClaveValor.
-----------------------------------------------------------------------------}
destructor TDiccionario.Destroy;
begin
    FItems.Clear;
    FreeAndNil(FItems);
    inherited;
end;

{-----------------------------------------------------------------------------
Function Name  : ObtenerValor
Author         : Alejandro Labra
Date Created   : 11-05-2011
Description    : Retorna el valor de acuerdo a la clave, si no existe .
Firma          : PAR00133_COPAMB_ALA_20110801
-----------------------------------------------------------------------------}
function TDiccionario.ObtenerValor(Clave: String): String;
var
    i   : Integer;
begin
    for i := 0 to FItems.Count - 1 do begin
        if TClaveValor(FItems.Items[i]).Clave = Clave then begin
            Result := TClaveValor(FItems.Items[i]).Valor;
            Exit;
        end;
    end;
end;

{-----------------------------------------------------------------------------
Function Name  : Setcount
Author         : Alejandro Labra
Date Created   : 08-04-2011
Description    : Retorna la cantidad de elementos que existen en la lista de
                 objetos TClaveValor.
-----------------------------------------------------------------------------}
function TDiccionario.Setcount: integer;
begin
    Result := FItems.Count;
end;

{-----------------------------------------------------------------------------
Function Name  : SetDiccionario
Author         : Alejandro Labra   
Date Created   : 08-04-2011
Description    : Obtiene un objeto TClaveValor de acuerdo a una posición dada.
-----------------------------------------------------------------------------}
function TDiccionario.SetDiccionario(index: integer): TClaveValor;
resourcestring
    MSG_INVALID_INDEX = 'Indice de Diccionario fuera de Rango.';
begin
    if index > FItems.Count then
        raise Exception.Create(MSG_INVALID_INDEX);

    result := TClaveValor(FItems[index]);
end;

function TDiccionario.ObtenerItem(Clave :string): TClaveValor;
var
  I: Integer;
begin
    Result := nil;
    for I := 0 to FItems.Count - 1 do
    begin
        Result := FItems[I];
        if Result.Clave = Clave then
            break;
    end;
end;

end.
