object DMConnections: TDMConnections
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 177
  Width = 458
  object BaseCOP: TADOConnection
    ConnectionTimeout = 30
    CursorLocation = clUseServer
    KeepConnection = False
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    OnWillConnect = BaseCOPWillConnect
    Left = 40
    Top = 16
  end
  object BaseCAC: TADOConnection
    ConnectionTimeout = 30
    KeepConnection = False
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    OnWillConnect = BaseCACWillConnect
    Left = 112
    Top = 16
  end
  object cnInformes: TADOConnection
    CommandTimeout = 300
    ConnectionTimeout = 30
    CursorLocation = clUseServer
    KeepConnection = False
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 184
    Top = 16
  end
  object BasePMMOPTT: TADOConnection
    ConnectionTimeout = 30
    CursorLocation = clUseServer
    KeepConnection = False
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    OnWillConnect = BasePMMOPTTWillConnect
    Left = 264
    Top = 16
  end
  object BaseMOPCAC: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=usr_op;Persist Security Info=True;U' +
      'ser ID=usr_op;Initial Catalog=OP_CAC;Data Source=SRV-DATABASE\DB' +
      'COSTANERA'
    ConnectionTimeout = 30
    CursorLocation = clUseServer
    KeepConnection = False
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    OnWillConnect = BaseMOPCACWillConnect
    Left = 40
    Top = 72
  end
  object BaseSOR: TADOConnection
    ConnectionTimeout = 30
    CursorLocation = clUseServer
    KeepConnection = False
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    OnWillConnect = BaseSORWillConnect
    Left = 120
    Top = 72
  end
  object ReportesAdmin: TADOConnection
    ConnectionTimeout = 30
    CursorLocation = clUseServer
    KeepConnection = False
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 264
    Top = 72
  end
  object BaseCOPAMB: TADOConnection
    ConnectionTimeout = 30
    CursorLocation = clUseServer
    KeepConnection = False
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    OnWillConnect = BaseCOPAMBWillConnect
    Left = 184
    Top = 72
  end
  object BasePMMOPTTAMB: TADOConnection
    ConnectionTimeout = 30
    CursorLocation = clUseServer
    KeepConnection = False
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    OnWillConnect = BasePMMOPTTAMBWillConnect
    Left = 352
    Top = 16
  end
  object BaseBO_Master: TADOConnection
    ConnectionTimeout = 30
    CursorLocation = clUseServer
    KeepConnection = False
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    OnWillConnect = BaseBO_MasterWillConnect
    Left = 40
    Top = 120
  end
  object BaseBO_Rating: TADOConnection
    ConnectionTimeout = 30
    CursorLocation = clUseServer
    KeepConnection = False
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    OnWillConnect = BaseBO_RatingWillConnect
    Left = 128
    Top = 120
  end
  object BaseBO_Billing: TADOConnection
    ConnectionTimeout = 30
    CursorLocation = clUseServer
    KeepConnection = False
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    OnWillConnect = BaseBO_MasterWillConnect
    Left = 216
    Top = 120
  end
end
