unit FreAltaOrdenServicio;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, ComCtrls, validate, Dateedit, DmiCtrls,
  BuscaTab, UtilDB, DB, ADODB, Util, PeaProcs, PeaTypes, CacProcs,
  UtilProc, BuscaClientes, ImgList, MaskCombo, Buttons, Mensajes, Grids,
  DPSControls, FrmEditarDomicilio, FreTelefono, UtilFacturacion;

type
  TFrameAltaOrdenServicio = class(TFrame)
    ObtenerDatosContacto: TADOStoredProc;
    PageControl: TPageControl;
    tab_cuentas: TTabSheet;
    tab_tags: TTabSheet;
    tab_facturacion: TTabSheet;
    GroupBox12: TGroupBox;
    Label50: TLabel;
    Label15: TLabel;
    lbl_fact_cliente: TLabel;
    txt_fact_cliente: TPickEdit;
    txt_fact_observaciones: TMemo;
    pnl_ultimafactura: TPanel;
    Label29: TLabel;
    Label30: TLabel;
    cb_fact_mes: TComboBox;
    cb_fact_anio: TComboBox;
    pnl_Factura: TPanel;
    Label16: TLabel;
    cb_fact_factura: TComboBox;
    tab_Otros: TTabSheet;
    GroupBox6: TGroupBox;
    txt_Otros_observaciones: TMemo;
    tvOrdenesServicio: TTreeView;
    ImageList1: TImageList;
    tsOrdenTrabajo: TTabSheet;
    Panel1: TPanel;
    pc_Adhesion: TPageControl;
    ts_DatosCliente: TTabSheet;
    Label8: TLabel;
    lblNombre: TLabel;
    lblApellido: TLabel;
    Label4: TLabel;
    Label17: TLabel;
    lblFechaNacCreacion: TLabel;
    lblSexo: TLabel;
    lblApellidoMaterno: TLabel;
    txt_nombre: TEdit;
    txt_apellido: TEdit;
    txt_fechanacimiento: TDateEdit;
    cb_sexo: TComboBox;
    cb_pais: TComboBox;
    cbTipoNumeroDocumento: TMaskCombo;
    txt_apellidoMaterno: TEdit;
    ts_DatosCuenta: TTabSheet;
    GroupBox2: TGroupBox;
    Label13: TLabel;
    Label14: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label38: TLabel;
    cb_color: TComboBox;
    cb_marca: TComboBox;
    cb_modelo: TComboBox;
    mcPatente: TMaskCombo;
    ts_Observaciones: TTabSheet;
    txt_adhesion_observaciones: TMemo;
    GroupBox9: TGroupBox;
    Label31: TLabel;
    Label32: TLabel;
    Label36: TLabel;
    lbl_tag_cliente: TLabel;
    cb_tag_cuenta: TComboBox;
    txt_tag_cliente: TPickEdit;
    txt_tag_observaciones: TMemo;
    GroupBox1: TGroupBox;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    cbTiposFallas: TComboBox;
    mmDescripcion: TMemo;
    cbUbicaciones: TComboBox;
    ObtenerOrdenesServicio: TADOStoredProc;
    QryObtenerOSCustomizadasPorCategoria: TADOQuery;
    Label47: TLabel;
    txt_anio: TNumericEdit;
    GroupDimensiones: TGroupBox;
    Label48: TLabel;
    Label49: TLabel;
    txt_largoVehiculo: TNumericEdit;
    txt_LargoAdicionalVehiculo: TNumericEdit;
    chk_acoplado: TCheckBox;
    txt_DescCategoria: TEdit;
    Panel2: TPanel;
    GroupBox3: TGroupBox;
    Label22: TLabel;
    lbl_entidad: TLabel;
    Label23: TLabel;
    rb_prepago: TRadioButton;
    rb_pospagomanual: TRadioButton;
    rb_pospagoautomatico: TRadioButton;
    cb_tipodebito: TComboBox;
    cb_entidaddebito: TComboBox;
    txt_cuentadebito: TEdit;
    GroupBox4: TGroupBox;
    chk_contag: TCheckBox;
    chk_detallepasadas: TCheckBox;
    chk_enviofacturas: TCheckBox;
    gbComunicacion: TGroupBox;
    cbMediosContacto: TComboBox;
    Label26: TLabel;
    bteContacto: TBuscaTabEdit;
    Label41: TLabel;
    btContacto: TBuscaTabla;
    btnBorrar: TSpeedButton;
    qryClientes: TADOQuery;
    qryContactos: TADOQuery;
    qryLegajos: TADOQuery;
    txt_CodigoCategoria: TNumericEdit;
    Label51: TLabel;
    cbTiposMantenimiento: TComboBox;
    Label52: TLabel;
    cbTiposCausa: TComboBox;
    QryCuentaCliente: TADOQuery;
    Label53: TLabel;
    cbPersoneria: TComboBox;
    Label12: TLabel;
    cbSituacionIVA: TComboBox;
    ObtenerPersona: TADOStoredProc;
    cb_actividad: TComboBox;
    GBMediosComunicacion: TGroupBox;
    Label35: TLabel;
    txtEmailParticular: TEdit;
    GBDomicilios: TGroupBox;
    lblnada: TLabel;
    Label2: TLabel;
    LblDomicilioComercial: TLabel;
    LblDomicilioParticular: TLabel;
    Label1: TLabel;
    BtnDomicilioComercialAccion: TDPSButton;
    BtnDomicilioParticularAccion: TDPSButton;
    RGDomicilioEntrega: TRadioGroup;
    ChkEliminarDomicilioParticular: TCheckBox;
    ChkEliminarDomicilioComercial: TCheckBox;
    ObtenerDatosDomicilio: TADOStoredProc;
    FrameTelefono1: TFrameTelefono;
    FrameTelefono2: TFrameTelefono;
	procedure chk_contagClick(Sender: TObject);
	procedure cb_cierre_cuentaClick(Sender: TObject);
	procedure txt_fact_clienteButtonClick(Sender: TObject);
	procedure txt_fact_clienteChange(Sender: TObject);
	procedure txt_tag_clienteButtonClick(Sender: TObject);
	procedure txt_tag_clienteChange(Sender: TObject);
	procedure chk_detallepasadasClick(Sender: TObject);
	procedure cb_marcaChange(Sender: TObject);
    procedure cb_tipodebitoChange(Sender: TObject);
    procedure tvOrdenesServicioClick(Sender: TObject);
    procedure tvOrdenesServicioDeletion(Sender: TObject; Node: TTreeNode);
    procedure cb_modeloChange(Sender: TObject);
    procedure txt_anioChange(Sender: TObject);
    procedure chk_acopladoClick(Sender: TObject);
    procedure cbTiposContactoChange(Sender: TObject);
    function btContactoProcess(Tabla: TDataSet;
      var Texto: String): Boolean;
    procedure btContactoSelect(Sender: TObject; Tabla: TDataSet);
    procedure btnBorrarClick(Sender: TObject);
    procedure IrAlInicio(Sender: TObject);
    procedure cbPersoneriaChange(Sender: TObject);
    procedure bteContactoButtonClick(Sender: TObject);
    procedure BtnDomicilioParticularAccionClick(Sender: TObject);
    procedure BtnDomicilioComercialAccionClick(Sender: TObject);
    procedure ChkEliminarDomicilioComercialClick(Sender: TObject);
    procedure ChkEliminarDomicilioParticularClick(Sender: TObject);
  private
	{ Private declarations }
    FTipoOrdenServicio: Integer;
    FCodigoPersona: integer;
	FCodigoCliente: Integer;
	FCodigoContacto: Integer;
    FTipoContacto: AnsiString;
	FCodigoComunicacion: Integer;
    FEsContacto: boolean;
	FResult: TModalResult;
	FOrdenServicio: integer;
    FMedioContacto: Integer;
    CodigoDomicilioParticular, CodigoDomicilioComercial, CodigoDomicilioEntrega: integer;
    DomicilioParticular: TDatosDomicilio;
    DomicilioComercial: TDatosDomicilio;
    GuardarTelefonoParticular, GuardarTelefonoComercial,
    GuardarTelefonoMovil, GuardarEmail: boolean;
    procedure HabilitarCamposPersoneria(Personeria: Char);
    procedure CargarArbolOrdenesServicio(Categoria, TipoOrdenServicio: Integer);

    procedure CargarCliente(CodigoPersona: Integer; TipoDocumento, NumeroDocumento: AnsiString);
    procedure CargarDatosReclamoFactura(aCodigoCliente, aNumeroFactura: Integer);

    Procedure SeleccionarTreeNode(Codigo: Integer);
    Procedure EliminarTreeNode(Codigo: Integer);
    procedure GuardarComunicacion;
	procedure GuardarAdhesion;
	procedure GuardarCierreCuenta;
	procedure GuardarReclamoInstalaciones;
	procedure GuardarAccidente;
	procedure GuardarReclamoSeguridad;
	procedure GuardarReclamoMantenimiento;
	procedure GuardarOrdenTrabajoReparacionPostes;
	procedure GuardarOrdenTrabajoReparacionPorticos;
	procedure GuardarOrdenTrabajoReparacionPavimento;
	procedure GuardarReclamoGeneral;
	procedure GuardarSugerencia;
	procedure GuardarAgradecimiento;
	procedure GuardarReclamoFactura;
	procedure GuardarReclamoFaltaFactura;
	procedure GuardarInhabilitacionTag;
	procedure GuardarRehabilitacionTag;
    procedure GuardarCambioTAG;
    procedure GuardarMalFuncionamiento;
    procedure GuardarOrdenServicioGenerica(TipoOrdenServicio: integer);
    Procedure ObtenerClienteContacto(CodigoComunicacion: Integer; var FCodigoContacto: integer);
    procedure TraerDimensionesVehiculo;
    procedure TraerDescripcionCategoria;
    procedure MarcarComoDomicilioEntrega(tipoOrigenDatos: integer);
    procedure PrepararMostrarDomicilio (descripcion: AnsiString; tipoOrigen: integer);
    procedure MostrarDomicilio (var L: TLabel; descripcion: AnsiString);
    procedure AltaDomicilio(tipoOrigenDatos: integer; var datosDomicilio: TDatosDomicilio);
    procedure ModificarDomicilio(tipoOrigenDatos: integer; var datosDomicilio: TDatosDomicilio);
    function  EstaElegidoComoDomicilioEntrega(tipoOrigenDatos: integer): boolean;
  public
	{ Public declarations }
 	function Inicializa(CodigoComunicacion, CodigoPersona: Integer;
             TipoDocumento, NumeroDocumento: AnsiString): Boolean;
	function InicializarAdhesion(CodigoComunicacion: Integer; CategoriaOS: Integer): Boolean;
  	function InicializarAdhesionContacto(CodigoComunicacion: Integer; CategoriaOS, TipoOrdenServicio: Integer): Boolean;
	function InicializarReclamoGeneral(CodigoComunicacion, CategoriaOrdenServicio: Integer): Boolean;
	function InicializarSolicitudTrabajo(CodigoComunicacion, CategoriaOrdenServicio, OrdenServicio: Integer): Boolean;
	function InicializaReclamoFactura(CodigoComunicacion, NumeroFactura: Integer): Boolean;
	function InicializarOrdenTrabajo(CodigoComunicacion: Integer): Boolean;
    function InicializarValidacion(CodigoComunicacion, CategoriaOrdenServicio,
      TipoOrdenServicio, ContextMark: Integer; ContractSerialNumber: Double): Boolean;
	procedure Guardar;

    Property Resultado: TModalResult read FResult;
    Property OrdenServicio : integer Read FOrdenServicio;
    Property CodigoCliente: Integer Read FCodigoCliente;
    Property TipoContacto: AnsiString Read FTipoContacto;
    Property MedioContacto: Integer read FMedioContacto write FMedioContacto;


  end;

var
  FrameAltaOrdenServicio: TFrameAltaOrdenServicio;

implementation

uses DMConnection;

{ TFormAltaOrdenServicio }


{$R *.dfm}



resourcestring
    CAPTION_BOTON_DOMICILIO_ALTA = 'Alta';
    CAPTION_BOTON_DOMICILIO_MODIFICAR = 'Modificar';
    CAPTION_RECLAMO_FACTURACION   = 'Validar datos del Reclamo de Facturaci�n';
    MSG_OBSERVACIONES_FACTURACION = 'Debe indicar alguna observaci�n como explicaci�n del Reclamo por Facturaci�n.';
    MSG_CLIENTE                   = 'Debe indicar un cliente para este reclamo.';
    MSG_ERROR_CODIGO_INEXISTENTE  = 'El c�digo de orden de servicio no existe.';
    MSG_SELECCIONAR_FACTURA	  	  = 'Debe indicar el Comprobante que posee la facturaci�n incorrecta';
    MSG_ACTUALIZAR_CONTACTO       = 'Actualizar Contacto';
    MSG_ACTUALIZAR_COMUNICACION   = 'Actualizar Comunicaci�n';
    MSG_FINALIZAR_COMUNICACION    = 'Finalizar Comunicaci�n';

function TFrameAltaOrdenServicio.Inicializa(CodigoComunicacion, CodigoPersona: Integer;
  TipoDocumento, NumeroDocumento: AnsiString): Boolean;
begin
    Result := False;
    TipoDocumento := Trim(TipoDocumento);
    NumeroDocumento := Trim(NumeroDocumento);
    FCodigoComunicacion := CodigoComunicacion;

    GuardarTelefonoParticular := false;
    GuardarTelefonoComercial  := false;
    GuardarTelefonoMovil      := false;
    GuardarEmail              := false;

    CargarPersoneria(cbPersoneria);
    CargarActividadesPersona(DMConnections.BaseCAC, cb_actividad);
    cbPersoneria.ItemIndex := 0;
    CodigoDomicilioParticular := -1;
    CodigoDomicilioComercial := -1;
    CodigoDomicilioEntrega := -1;
    try
        // Cargamos el cliente

        // Si se indica un Cliente Especifico no lo buscamos por medio de la comunicacion.
        if (CodigoPersona = 0) and ((TipoDocumento = '') or (NumeroDocumento = '')) then begin
            ObtenerClienteContacto(CodigoComunicacion, FCodigoContacto);

            // Cargamos los datos del cliente
            CargarCliente(FCodigoPersona, '', '');
        end else
            // Cargamos los datos del cliente
            CargarCliente(FCodigoPersona, TipoDocumento, NumeroDocumento);

        // Cargamos el arbol de ordenes de servicio para todas las categor�as y
        // todos los tipos ordenes de servicio
        FEsContacto := not ExisteCliente(DMConnections.BaseCAC, FCodigoPersona);
        CargarArbolOrdenesServicio(0, 0);

        // Elimino las OS fijas de mantenimiento y automaticos que no se deben cargar
        EliminarTreeNode(OS_REPARAR_POSTE_SOS);
        EliminarTreeNode(OS_REPARAR_PORTICO);
        EliminarTreeNode(OS_REPARAR_PAVIMENTO);
        EliminarTreeNode(OS_ENVIAR_MAIL_CLIENTE);
        EliminarTreeNode(OS_ENVIAR_CARTA_CLIENTE);
        EliminarTreeNode(OS_ENVIAR_FAX_CLIENTE);
        EliminarTreeNode(OS_ENVIAR_VOICE_MAIL_CLIENTE);
        EliminarTreeNode(OS_MAL_FUNCIONAMIENTO_TAG);
        EliminarTreeNode(OS_LLAMAR_POR_TELEFONO_CLIENTE);
        EliminarTreeNode(CAT_OS_MOROSOS);
        EliminarTreeNode(CAT_OS_VALIDACION);

        // Elimino las OS custmizables de mantenimiento
        QryObtenerOSCustomizadasPorCategoria.parameters.paramByName('CategoriaOrdenServicio').value := CAT_OS_MANTENIMIENTO;
        QryObtenerOSCustomizadasPorCategoria.open;
        while not QryObtenerOSCustomizadasPorCategoria.eof do begin
            EliminarTreeNode(QryObtenerOSCustomizadasPorCategoria.FieldByName('CodigoWorkFlow').asInteger);
            QryObtenerOSCustomizadasPorCategoria.next;
        end;
        QryObtenerOSCustomizadasPorCategoria.close;

        // Cargamos los datos de facturaci�n (Ultima factura y �ltimo ciclo de facturaci�n)
        //**viejo txt_fact_cliente.Value := FCodigoCliente + FCOdigoContacto;
        txt_fact_cliente.Value := FCodigoPersona;
        //**viejo CargarDatosReclamoFactura(FCodigoCliente + FCOdigoContacto, 0);
        CargarDatosReclamoFactura(FCodigoPersona, 0);

        // Cargamos los datos de TAGs
        //**viejo txt_tag_cliente.ValueInt := FCodigoCliente + FCodigoCOntacto;
        txt_tag_cliente.ValueInt := FCodigoPersona;
        CargarCuentas(DMConnections.BaseCAC, cb_tag_cuenta, FCodigoPersona);
        cb_tag_cuenta.itemIndex := 0;

        // Cargamos los datos de la falla
//Pablo        CargarTiposMantenimiento(DMConnections.BaseCAC, cbTiposMantenimiento, 0);
//Pablo        CargarTiposCausa(DMConnections.BaseCAC, cbTiposCausa, 0);
//Pablo        CargarTiposFallas(DMConnections.BaseCAC, cbTiposFallas, 0, True, False);
//Pablo        CargarUbicacionesMantenimiento(DMConnections.BaseCAC, cbUbicaciones, 0, True, False);

        // Por defecto utilizamos la orden de servicio de adhesi�n
        PageControl.ActivePage := tab_Cuentas;
        SeleccionarTreeNode(OS_ADHESION);
        result := true;
    except
    end;
end;


procedure TFrameAltaOrdenServicio.CargarCliente(CodigoPersona: Integer;
                                TipoDocumento, NumeroDocumento: AnsiString);
var
    PaisAux: AnsiString;
begin
    try
		// Traemos datos de la base
        FCodigoPersona := CodigoPersona;
        // Si la comunicacion no tiene un cliente asociado, y no fueron asignados los campos de busqueda de un cliente
        // asignamos los campos en vacio
        if (CodigoPersona <> 0) or (TipoDocumento <> '') and (NumeroDocumento <> '') then begin
            ObtenerPersona.Parameters.ParamByName('@CodigoPersona').Value := iif(CodigoPersona = 0, null, CodigoPersona);
            ObtenerPersona.Parameters.ParamByName('@CodigoDocumento').Value := iif(((TipoDocumento = '') or (NumeroDocumento = '')), null, TipoDocumento);
            ObtenerPersona.Parameters.ParamByName('@NumeroDocumento').Value := iif(((TipoDocumento = '') or (NumeroDocumento = '')), null, NumeroDocumento);
            ObtenerPersona.Open;
            // Los cargamos en los campos
            With ObtenerPersona do begin
                //FCodigoPersona := FieldByName('CodigoPersona').AsInteger;
                CargarPersoneria(cbPersoneria, FieldByName('Personeria').AsString);
                HabilitarCamposPersoneria(FieldByName('Personeria').AsString[1]); //es char, por eso el [1] 
                CargarTiposDocumento(DMConnections.BaseCAC, cbTipoNumeroDocumento,
                                     FieldByName('CodigoDocumento').AsString);
                cbTipoNumeroDocumento.MaskText := FieldByName('NumeroDocumento').AsString;
                if FieldByName('NumeroDocumento').IsNull then
                    cbTipoNumeroDocumento.Enabled := true;
                txt_nombre.Text := TrimRight(FieldByName('Nombre').AsString);
                txt_Apellido.Text := TrimRight(FieldByName('Apellido').AsString);
                //completo Apellido Materno o Contacto comercial seg�n la Personeria
                if FieldByName('Personeria').AsString = PERSONERIA_FISICA then
                    txt_ApellidoMaterno.Text := TrimRight(FieldByName('ApellidoMaterno').AsString)
                else
                    //para las Juridicas mostramos el Contacto comercial
                    txt_ApellidoMaterno.Text := TrimRight(FieldByName('ContactoComercial').AsString);
                CargarSexos(cb_Sexo, FieldByName('Sexo').AsString);
                CargarTipoSituacionIVA(DMConnections.BaseCAC, cbSituacionIVA,
                                       FieldByName('CodigoSituacionIVA').AsString);
                PaisAux := iif(FieldByName('LugarNacimiento').IsNull, PAIS_CHILE,
                           Trim(FieldByName('LugarNacimiento').AsString));
                CargarPaises(DMConnections.BaseCAC, cb_Pais, PaisAux);
                txt_fechanacimiento.Date := FieldByName('FechaNacimiento').AsDateTime;
                if FieldByName('CodigoActividad').IsNull then
                    CargarActividadesPersona(DMConnections.BaseCAC, cb_actividad)
                else
                    CargarActividadesPersona(DMConnections.BaseCAC, cb_actividad, FieldByName('CodigoActividad').AsInteger);

                CodigoDomicilioEntrega := iif(FieldByName('CodigoDomicilioEntrega').IsNull, -1,
                                              FieldByName('CodigoDomicilioEntrega').AsInteger);
                ObtenerPersona.Close;
            end;

            //CARGO LOS DOMICILIOS
            //1- primero el Particular
            //2- segundo el Comercial

            //***** DOMICILIO PARTICULAR ******//
            codigodomicilioparticular := ObtenerCodigoDomicilioPersona(FCodigoPersona, TIPO_ORIGEN_DATO_PARTICULAR);
            if (codigodomicilioparticular <> -1) then begin
                with ObtenerDatosDomicilio do
                begin
                    Close;
                    Parameters.ParamByName('@CodigoDomicilio').Value := codigodomicilioparticular;
                    Open;
                    if IsEmpty then begin
                        LimpiarRegistroDomicilio(DomicilioParticular, TIPO_ORIGEN_DATO_PARTICULAR);
                    end
                    else begin
                        DomicilioParticular.CodigoDomicilio := codigodomicilioparticular;
                        //IndiceDomicilio Lo utilizo para indicar que esta ingresado. Si tiene -1 es que no hay datos!!!
                        {
                        DomicilioParticular.IndiceDomicilio := 1;
                        DomicilioParticular.Descripcion := '';
                        DomicilioParticular.CodigoTipoDomicilio := TIPO_ORIGEN_DATO_PARTICULAR;
                        DomicilioParticular.CodigoCalle := iif(FieldByName('CodigoCalle').IsNull, -1,FieldByName('CodigoCalle').AsInteger);
                        DomicilioParticular.CodigoTipoCalle := iif(FieldByName('CodigoTipoCalle').IsNull, -1,FieldByName('CodigoTipoCalle').AsInteger);
                        DomicilioParticular.NumeroCalle := FieldByName('Numero').AsInteger;
                        DomicilioParticular.Piso := FieldByName('Piso').AsInteger;
                        DomicilioParticular.Dpto := FieldByName('Dpto').AsString;
                        DomicilioParticular.Detalle := FieldByName('Detalle').AsString;
                        DomicilioParticular.CodigoPostal := FieldByName('CodigoPostal').AsString;
                        DomicilioParticular.CodigoPais := FieldByName('CodigoPais').AsString;
                        DomicilioParticular.CodigoRegion := FieldByName('CodigoRegion').AsString;
                        DomicilioParticular.CodigoComuna := FieldByName('CodigoComuna').AsString;
                        DomicilioParticular.CodigoCiudad := FieldByName('CodigoCiudad').AsString;
                        DomicilioParticular.CalleDesnormalizada := FieldByName('CalleDesnormalizada').AsString;
                        DomicilioParticular.CodigoTipoEdificacion := FieldByName('CodigoTipoEdificacion').AsInteger;
                        DomicilioParticular.DomicilioEntrega := false;
                        DomicilioParticular.CodigoCalleRelacionadaUno := iif(FieldByName('CodigoCalleRelacionadaUno').IsNull, -1,FieldByName('CodigoCalleRelacionadaUno').AsInteger);
                        DomicilioParticular.NumeroCalleRelacionadaUno := iif(FieldByName('NumeroCalleRelacionadaUno').IsNull, -1,FieldByName('NumeroCalleRelacionadaUno').AsInteger);
                        DomicilioParticular.CodigoCalleRelacionadaDos := iif(FieldByName('CodigoCalleRelacionadaDos').IsNull, -1,FieldByName('CodigoCalleRelacionadaDos').AsInteger);
                        DomicilioParticular.NumeroCalleRelacionadaDos := iif(FieldByName('NumeroCalleRelacionadaDos').IsNull, -1,FieldByName('NumeroCalleRelacionadaDos').AsInteger);
                        DomicilioParticular.Activo := FieldByName('Activo').AsBoolean;
                        }
                    end;
                end;
                if (codigodomicilioparticular = codigodomicilioentrega) then
                    MarcarComoDomicilioEntrega(TIPO_ORIGEN_DATO_PARTICULAR);
            end
            else LimpiarRegistroDomicilio(DomicilioParticular, TIPO_ORIGEN_DATO_PARTICULAR);

            //***** DOMICILIO COMERCIAL ******//
            codigodomiciliocomercial := ObtenerCodigoDomicilioPersona(FCodigoPersona, TIPO_ORIGEN_DATO_COMERCIAL);
            if (codigodomiciliocomercial <> -1) then begin
                with ObtenerDatosDomicilio do
                begin
                    Close;
                    Parameters.ParamByName('@CodigoDomicilio').Value := codigodomiciliocomercial;
                    Open;
                    if IsEmpty then begin
                        LimpiarRegistroDomicilio(DomicilioComercial, TIPO_ORIGEN_DATO_COMERCIAL);
                    end
                    else begin
                        Domiciliocomercial.CodigoDomicilio := codigodomiciliocomercial;
                        {
                        Domiciliocomercial.IndiceDomicilio := 1;
                        Domiciliocomercial.Descripcion := '';
                        Domiciliocomercial.CodigoTipoDomicilio := TIPO_ORIGEN_DATO_COMERCIAL;
                        Domiciliocomercial.CodigoCalle := iif(FieldByName('CodigoCalle').IsNull, -1,FieldByName('CodigoCalle').AsInteger);
                        DomicilioComercial.CodigoTipoCalle := iif(FieldByName('CodigoTipoCalle').IsNull, -1,FieldByName('CodigoTipoCalle').AsInteger);
                        DomicilioComercial.NumeroCalle := FieldByName('Numero').AsInteger;
                        DomicilioComercial.Piso := FieldByName('Piso').AsInteger;
                        DomicilioComercial.Dpto := FieldByName('Dpto').AsString;
                        DomicilioComercial.Detalle := FieldByName('Detalle').AsString;
                        DomicilioComercial.CodigoPostal := FieldByName('CodigoPostal').AsString;
                        DomicilioComercial.CodigoPais := FieldByName('CodigoPais').AsString;
                        DomicilioComercial.CodigoRegion := FieldByName('CodigoRegion').AsString;
                        DomicilioComercial.CodigoComuna := FieldByName('CodigoComuna').AsString;
                        DomicilioComercial.CodigoCiudad := FieldByName('CodigoCiudad').AsString;
                        DomicilioComercial.CalleDesnormalizada := FieldByName('CalleDesnormalizada').AsString;
                        DomicilioComercial.CodigoTipoEdificacion := FieldByName('CodigoTipoEdificacion').AsInteger;
                        DomicilioComercial.DomicilioEntrega := false;
                        DomicilioComercial.CodigoCalleRelacionadaUno := iif(FieldByName('CodigoCalleRelacionadaUno').IsNull, -1,FieldByName('CodigoCalleRelacionadaUno').AsInteger);
                        DomicilioComercial.NumeroCalleRelacionadaUno := iif(FieldByName('NumeroCalleRelacionadaUno').IsNull, -1,FieldByName('NumeroCalleRelacionadaUno').AsInteger);
                        DomicilioComercial.CodigoCalleRelacionadaDos := iif(FieldByName('CodigoCalleRelacionadaDos').IsNull, -1,FieldByName('CodigoCalleRelacionadaDos').AsInteger);
                        DomicilioComercial.NumeroCalleRelacionadaDos := iif(FieldByName('NumeroCalleRelacionadaDos').IsNull, -1,FieldByName('NumeroCalleRelacionadaDos').AsInteger);
                        DomicilioComercial.Activo := FieldByName('Activo').AsBoolean;
                        }
                    end;
                end;
                if (codigodomiciliocomercial = codigodomicilioentrega) then
                    MarcarComoDomicilioEntrega(TIPO_ORIGEN_DATO_COMERCIAL);
            end
            else LimpiarRegistroDomicilio(DomicilioComercial, TIPO_ORIGEN_DATO_COMERCIAL);

            //Muestro los domicilios en el Label
            PrepararMostrarDomicilio (iif(codigodomicilioparticular < 1, '',
                                    ArmarDomicilioCompletoPorCodigo(DMConnections.BaseCAC, CodigoPersona,codigodomicilioparticular)),
                                        TIPO_ORIGEN_DATO_PARTICULAR);
            PrepararMostrarDomicilio (iif(codigodomiciliocomercial < 1, '',
                                    ArmarDomicilioCompletoPorCodigo(DMConnections.BaseCAC, CodigoPersona ,codigodomiciliocomercial)),
                                        TIPO_ORIGEN_DATO_COMERCIAL);
                                        
            //FMTelefonoParticular.txtTelefonoParticular.Text := '';
            //txtTelefonoComercial.Text := '';
            //txtTelefonoMovil.Text := '';
            //txtEmailParticular.Text := '';
            //txtTelefonoParticular.Text := ObtenerValorMediosComunicacion(CodigoPersona, MC_TELEPHONE, TIPO_ORIGEN_DATO_PARTICULAR);
            //txtTelefonoComercial.Text := ObtenerValorMediosComunicacion(CodigoPersona, MC_TELEPHONE, TIPO_ORIGEN_DATO_COMERCIAL);
            //txtTelefonoMovil.Text := ObtenerValorMediosComunicacion(CodigoPersona, MC_MOVIL_PHONE, TIPO_ORIGEN_DATO_PARTICULAR);
            //txtEMailParticular.Text := ObtenerValorMediosComunicacion(CodigoPersona, MC_EMAIL, TIPO_ORIGEN_DATO_PARTICULAR);

            //GuardarTelefonoParticular := txtTelefonoParticular.Text <> '';
            //GuardarTelefonoComercial  := txtTelefonoComercial.Text <> '';
            //GuardarTelefonoMovil      := txtTelefonoMovil.Text <> '';
            //GuardarEmail              := txtEmailParticular.Text <> '';
            
            CargarTiposPatente(DMConnections.BaseCAC, mcPatente, '');
            CargarMarcasVehiculos(DMConnections.BaseCAC, cb_marca);
            CargarColoresVehiculos(DMConnections.BaseCAC, cb_color);
            CargarTarjetasCredito(DMConnections.BaseCAC, cb_entidaddebito);

            // Otros
        end;
        // Listo
	finally
		Screen.Cursor := crDefault;
	end;
end;

function TFrameAltaOrdenServicio.InicializarOrdenTrabajo(CodigoComunicacion: Integer): Boolean;
begin
    result := False;
    FCodigoComunicacion := CodigoComunicacion;
    try
        if CodigoComunicacion <= 0 then begin
            if not OpenTables([qryClientes, qryContactos, qryLegajos]) then Exit;
            gbComunicacion.Visible := True;
            gbComunicacion.Enabled := True;
            {***
            CargarTiposContactos(cbTiposContacto);
            cbTiposContacto.ItemIndex := 0;
            cbTiposContacto.OnChange(cbTiposContacto); }
            CargarMediosContacto(DMConnections.BaseCAC, cbMediosContacto);
        end else begin
            gbComunicacion.Visible := False;
            gbComunicacion.Enabled := False;
        end;

//Pablo        CargarTiposMantenimiento(DMConnections.BaseCAC, cbTiposMantenimiento, 0);
//Pablo        CargarTiposCausa(DMConnections.BaseCAC, cbTiposCausa, 0);
//Pablo    	CargarTiposFallas(DMConnections.BaseCAC, cbTiposFallas, 0, True, False);
//Pablo	    CargarUbicacionesMantenimiento(DMConnections.BaseCAC, cbUbicaciones, 0, True, False);

        FEsContacto := not ExisteCliente(DMConnections.BaseCAC, FCodigoPersona);
	    CargarArbolOrdenesServicio(CAT_OS_MANTENIMIENTO, 0);

        //Debo sacar del arbol la Orden de Servicio de Mantenimiento
        EliminarTreeNode(OS_GENERAR_ORDEN_DE_TRABAJO);
    	PageControl.ActivePage := tsOrdenTrabajo;
        SeleccionarTreeNode(CAT_OS_MANTENIMIENTO);
        Result := True;
    except
    end;
end;

function TFrameAltaOrdenServicio.InicializarAdhesion(CodigoComunicacion: Integer;
  CategoriaOS: Integer): Boolean;
begin
	Result := False;
    FCodigoComunicacion := CodigoComunicacion;
    try
        // Cargamos el cliente o contacto
        ObtenerClienteContacto(CodigoComunicacion, FCodigoContacto);

        // Cargamos el arbol de ordenes de servicio
        //Se guarda en FCodigoContacto el codigo persona si es que no es cliente
        FEsContacto := not ExisteCliente(DMConnections.BaseCAC, FCodigoPersona);
        CargarArbolOrdenesServicio(CategoriaOS, 0);

        // Cargamos la informaci�n del cliente
        CargarCliente(FCodigoPersona, '', '');

        // Cargamos los datos de TAGs
        txt_tag_cliente.ValueInt := FCodigoPersona;
        CargarCuentas(DMConnections.BaseCAC, cb_tag_cuenta, FCodigoPersona);
        cb_tag_cuenta.itemIndex := 0;

        // Seleccionamos el node del arbol por defecto
        PageControl.ActivePage := tab_Cuentas;
        SeleccionarTreeNode(OS_ADHESION);

        Result := True;
	except
    	On E: Exception do begin
	    	Exit;
		end;
    end;
end;


function TFrameAltaOrdenServicio.InicializarReclamoGeneral(CodigoComunicacion,
  CategoriaOrdenServicio: Integer): Boolean;
begin
	Result := False;
    FCodigoComunicacion := CodigoComunicacion;
    try
        // Cargamos los datos del cliente o contacto
        ObtenerClienteContacto(CodigoComunicacion, FCodigoContacto);

        // Cargamos el arbol ordenes de servicio
        FEsContacto := not ExisteCliente(DMConnections.BaseCAC, FCodigoPersona);
        CargarArbolOrdenesServicio(CategoriaOrdenServicio, 0);

        // Seleccionamos el Tab correspondiente
        PageControl.ActivePage := tab_otros;
        SeleccionarTreeNode(OS_RECLAMO_GENERAL);
        result := True;
	except
    	On E: Exception do begin
	    	Exit;
		end;
    end;
end;


function TFrameAltaOrdenServicio.InicializarSolicitudTrabajo(CodigoComunicacion,
  CategoriaOrdenServicio, OrdenServicio: Integer): Boolean;
begin
    result := False;
    FCodigoComunicacion := CodigoComunicacion;
    try
        if CodigoComunicacion <= 0 then begin
            if not OpenTables([qryClientes, qryContactos, qryLegajos]) then Exit;
            gbComunicacion.Visible := True;
            gbComunicacion.Enabled := True;
            //CargarTiposContactos(cbTiposContacto);
            //cbTiposContacto.ItemIndex := 0;
            //cbTiposContacto.OnChange(cbTiposContacto);
            CargarMediosContacto(DMConnections.BaseCAC, cbMediosContacto);
        end else begin
            gbComunicacion.Visible := False;
            gbComunicacion.Enabled := False;
        end;

        // Cargamos los datos del cliente o contacto
        ObtenerClienteContacto(CodigoComunicacion, FCodigoPersona);

        // Cargamos el arbol ordenes de servicio
        FEsContacto := not ExisteCliente(DMConnections.BaseCAC, FCodigoPersona);
        CargarArbolOrdenesServicio(CategoriaOrdenServicio, OrdenServicio);
        
        // Cargamos los datos de la falla
//Pablo        CargarTiposMantenimiento(DMConnections.BaseCAC, cbTiposMantenimiento, 0);
//Pablo        CargarTiposCausa(DMConnections.BaseCAC, cbTiposCausa, 0);
//Pablo        CargarTiposFallas(DMConnections.BaseCAC, cbTiposFallas, 0, True, False);
//Pablo        CargarUbicacionesMantenimiento(DMConnections.BaseCAC, cbUbicaciones, 0, True, False);

        PageControl.ActivePage := tsOrdenTRabajo;
        SeleccionarTreeNode(OS_GENERAR_ORDEN_DE_TRABAJO);

        result := true;
	except
    	On E: Exception do begin
	    	Exit;
		end;
    end;
end;

function TFrameAltaOrdenServicio.InicializaReclamoFactura(CodigoComunicacion,
  NumeroFactura: Integer): Boolean;
Var
	i: Integer;
begin
	Result := False;
    FCodigoComunicacion := CodigoComunicacion;
    try
        // Obtenemos la informacion del cliente o del contacto en funci�n del codigo de comunicaci�n.
        ObtenerClienteContacto(CodigoComunicacion, FCodigoContacto);

        // Cargamos el arbol de ordenes de servcicio.
        FEsContacto := not ExisteCliente(DMConnections.BaseCAC, FCodigoPersona);
        CargarArbolOrdenesServicio(CAT_OS_FACTURACION, 0);


        // Cargamos la informaci�n necesaria para el reclamo de facturaci�n
        //txt_fact_cliente.Value := FCodigoPersona;
        //txt_fact_cliente.ValueInt := FCodigoPersona;
        CargarDatosReclamoFactura(FCodigoPersona, 0);

        // Seleccionamos la factura si esta indicada
        cb_fact_factura.ItemIndex := 0;
        for i := 0 to cb_fact_factura.Items.Count - 1 do begin
            if IVal(StrRight(cb_fact_factura.Items[i], 20)) = NumeroFactura then begin
                cb_fact_factura.ItemIndex := i;
                Break;
            end;
        end;

        // Cargamos el arbol ordenes de servicio
        //CargarArbolOrdenesServicio(OS_FACTURACION_INCORRECTA, 0);

        PageControl.ActivePage := tab_Facturacion;
        SeleccionarTreeNode(OS_FACTURACION_INCORRECTA);
        Result := True;
	except
    	On E: Exception do begin
	    	Exit;
		end;
    end;
    Result := True;
end;


procedure TFrameAltaOrdenServicio.chk_contagClick(Sender: TObject);
begin
	rb_pospagomanual.Enabled := chk_contag.Checked;
	rb_pospagoautomatico.Enabled := chk_contag.Checked;
	cb_tipodebito.Enabled := chk_contag.Checked and rb_pospagoautomatico.Checked;
	cb_entidaddebito.Enabled := cb_tipodebito.Enabled;
	txt_cuentadebito.Enabled := cb_tipodebito.Enabled;
end;

procedure TFrameAltaOrdenServicio.GuardarAdhesion;

	Function ValidarPatente: Boolean;
	begin
		Result := QueryGetValueInt(DMConnections.BaseCAC, Format(
		  'SELECT 1 FROM Cuentas (INDEX = IX_Patente) WHERE Patente = ''%s'' ' +
		  'AND FechaBaja IS NULL', [Trim(mcPatente.MaskText)])) <> 1;
	end;

resourcestring
    MSG_CAPTION_ORDEN_SERVICIO_ADHESION = 'Orden de servicio de Adhesi�n';
    CAPTION_VALIDAR_ADHESION = 'Validar Datos de la Adhesi�n';
    CAPTION_GRABADOS_CORRECTAMENTE = 'Datos grabados correctamente.';
    GRABADOS_CORRECTAMENTE = 'Datos grabados correctamente.';

    MSG_NOMBRE       = 'Debe indicar un nombre.';
    MSG_APELLIDO     = 'Debe indicar un apellido.';
    MSG_DOCUMENTO    = 'Debe indicar un documento.';
    MSG_NUMERODOCUMENTO = 'El documento especificado es incorrecto.';

    MSG_CALLE_PERS      = 'Debe indicar la calle (domicilio personal)';
    MSG_NUMERO_PERS     = 'Debe indicar un n�mero (domicilio personal)';
    MSG_CPOSTAL_PERS    = 'Debe indicar el c�digo postal (domicilio personal)';
    MSG_REGION_PERS     = 'Debe indicar la regi�n o estado de residencia (domicilio personal)';
    MSG_COMUNA_PERS     = 'Debe indicar la comuna (domicilio personal)';
    MSG_CALLE_DOCU      = 'Debe indicar la calle (domicilio de la documentaci�n)';
    MSG_NUMERO_DOCU     = 'Debe indicar un n�mero (domicilio de la documentaci�n)';
    MSG_CPOSTAL_DOCU    = 'Debe indicar el c�digo postal (domicilio de la documentaci�n)';
    MSG_REGION_DOCU     = 'Debe indicar la regi�n o estado de residencia (domicilio de la documentaci�n)';
    MSG_COMUNA_DOCU     = 'Debe indicar la comuna (domicilio de la documentaci�n)';

    MSG_PAIS         = 'Debe indicar el pa�s de residencia.';

    MSG_FECHANACIMIENTO = 'La fecha es incorrecta';
    MSG_MAIL         = 'La direcci�n de mail es inv�lida.';

    MSG_PATENTE      = 'Debe indicar patente.';
    MSG_FORMATOPATENTE = 'La patente especificada es incorrecta.';
    MSG_CUENTA_ACTIVA = 'Ya existen una cuenta activa con la patente %s';
    MSG_CATEGORIA    = 'Debe indicar una categor�a.';
    MSG_MARCA        = 'Debe indicar una marca.';
    MSG_MODELO       = 'Debe indicar un modelo.';
    MSG_ANIO         = 'El a�o es incorrecto. ' + #13#10 +
                   'Debe ser mayor a 1900 y menor al a�o siguiente al actual. ';
    MSG_COLOR        = 'Debe indicar un color.';
    MSG_TARJETA_BANCO = 'Debe indicar el tipo de tarjeta o banco.';
    MSG_CUENTA_BANCARIA = 'Debe indicar n�mero de cuenta bancaria.';
    MSG_DOMICILIO_DOCUMENTOS_INCOMPLETO = 'Ingres� el domicilio de documentos en forma incompleta. '+ CRLF +
                                          'Por favor revise el n�mero, la comuna y la regi�n.';
    MSG_DOMICILIO_NINGUNO = 'No ingres� ninguno de los domicilios. '+ CRLF +
                            'Por favor complete los datos.';
    MSG_DOMICILIO_NINGUNO_ENTREGA = 'No seleccion� ninguno de los domicilios para que sea de entrega. ';
    MSG_DOMICILIO_ENTREGA_LIMPIO = 'Seleccion� un domicilio de entrega que tiene los datos incompletos. ';
    MSG_ERROR_DOMICILIO_ENTREGA_BORRAR = 'Eligi� un domicilio de entrega que est� marcado para ser borrado.';
Var
	PatenteOk, TarjOk: Boolean;
    DescriError: AnsiString;
    DocCorrecto: Boolean;
    DatosContacto : TDatosAdhesion;
    TipoOrigenDatoDomicilioEntrega: smallint;
begin
    FResult := mrNone;

    // Validamos el documento del cliente
    DocCorrecto := cbTipoNumeroDocumento.ValidateMask;

    (*Si ya viene ingresado de la base, se encuentra deshabilitado. Pero puede
    ocurrir que est� mal cargado en la base, por eso lo habilito en este momento
    para que pueda corregirlo (y no de error!!!) si esta mal*)

    cbTipoNumeroDocumento.Enabled := true;
    if not ValidateControls(
	  [cbTipoNumeroDocumento,
  	  cbTipoNumeroDocumento],
	  [Trim(cbTipoNumeroDocumento.MaskText) <> '',
      (Length(cbTipoNumeroDocumento.MaskText) <= 11) and DocCorrecto],
      CAPTION_VALIDAR_ADHESION,
      [MSG_DOCUMENTO,
       MSG_NUMERODOCUMENTO]
    ) then exit;
    cbTipoNumeroDocumento.Enabled := false;

	// Validamos los datos del cliente

	if not ValidateControls(
	  [txt_nombre,
	  txt_apellido,
	  cb_pais,
	  txt_fechanacimiento],
	  [Trim(txt_nombre.text) <> '',
	  Trim(txt_apellido.text) <> '',
	  cb_pais.itemindex >= 0,
	  (txt_fechanacimiento.Text = '  /  /    ') or FechaNacimientoValida(txt_fechanacimiento.Date)],
	  CAPTION_VALIDAR_ADHESION,
	  [MSG_NOMBRE,
        MSG_APELLIDO,
	    MSG_PAIS,
        MSG_FECHANACIMIENTO]
	  ) then Exit;

	// Validamos los datos del veh�culo
    PatenteOk := mcPatente.ValidateMask;

	if not ValidateControls(
	  [mcPatente,
	  mcPatente,
      mcPatente,
	  cb_marca,
	  cb_modelo,
      txt_anio],
	  [Trim(mcPatente.MaskText) <> '',
	  ValidarPatente,
      PatenteOk,
	  cb_marca.itemindex >= 0,
	  cb_modelo.itemindex >= 0,
      EsAnioCorrecto(txt_anio.ValueInt)],
	  CAPTION_VALIDAR_ADHESION,
	  [MSG_PATENTE,
	  Format(MSG_CUENTA_ACTIVA, [Trim(mcPatente.MaskText)]),
      MSG_FORMATOPATENTE,
	  MSG_MARCA,
	  MSG_MODELO,
      MSG_ANIO]

	  ) then Exit;



    //Si no carg� ninguno de los dos domicilios, reboto!
    if (DomicilioParticular.IndiceDomicilio <> 1) and
       (DomicilioParticular.IndiceDomicilio <> 1) then begin
        MsgBox(MSG_DOMICILIO_NINGUNO, CAPTION_VALIDAR_ADHESION, MB_ICONSTOP);
        Exit;
    end;

    //Si marc� el domicilio particular para entrega, pero no lo carg�, le aviso
    if (EstaElegidoComoDomicilioEntrega(TIPO_ORIGEN_DATO_PARTICULAR)) then begin
        if (DomicilioParticular.IndiceDomicilio <> 1) then begin
            MsgBox(MSG_DOMICILIO_ENTREGA_LIMPIO, CAPTION_VALIDAR_ADHESION, MB_ICONSTOP);
            exit;
        end
        else begin
            if (ChkEliminarDomicilioParticular.Checked) then begin
                MsgBox(MSG_ERROR_DOMICILIO_ENTREGA_BORRAR, CAPTION_VALIDAR_ADHESION, MB_ICONSTOP);
                exit;
            end;
        end;
    end;

    //Si marc� el domicilio comercial para entrega, pero no lo carg�, le aviso
    if (EstaElegidoComoDomicilioEntrega(TIPO_ORIGEN_DATO_COMERCIAL)) then begin
        if (DomicilioComercial.IndiceDomicilio <> 1) then begin
            MsgBox(MSG_DOMICILIO_ENTREGA_LIMPIO, CAPTION_VALIDAR_ADHESION, MB_ICONSTOP);
            exit;
        end
        else begin
            if (ChkEliminarDomicilioComercial.Checked) then begin
                MsgBox(MSG_ERROR_DOMICILIO_ENTREGA_BORRAR, CAPTION_VALIDAR_ADHESION, MB_ICONSTOP);
                exit;
            end;
        end;
    end;

    //Si no marc� ninguno, me fijo cual ingres� y lo marco.
    if (not EstaElegidoComoDomicilioEntrega(TIPO_ORIGEN_DATO_PARTICULAR)) and
       (not EstaElegidoComoDomicilioEntrega(TIPO_ORIGEN_DATO_COMERCIAL)) then begin
        if StrRight(Trim(cbPersoneria.Text), 1) = PERSONERIA_FISICA then begin
            //Si es persona f�sica y cargo el domicilio particular lo marco...
            if (DomicilioParticular.IndiceDomicilio = 1) then
                MarcarComoDomicilioEntrega(TIPO_ORIGEN_DATO_PARTICULAR)
            else
                MarcarComoDomicilioEntrega(TIPO_ORIGEN_DATO_COMERCIAL);
        end
        else begin //Es Persona Jur�dica
            //Si es persona f�sica y cargo el domicilio comercial lo marco...
            if (DomicilioComercial.IndiceDomicilio = 1) then
                MarcarComoDomicilioEntrega(TIPO_ORIGEN_DATO_COMERCIAL)
            else
                MarcarComoDomicilioEntrega(TIPO_ORIGEN_DATO_PARTICULAR);
        end;
    end;

	// Validamos las formas de pago
	if rb_pospagoautomatico.checked then begin
		if not ValidateControls([cb_entidaddebito], [cb_entidaddebito.ItemIndex >= 0],
		  CAPTION_VALIDAR_ADHESION, [MSG_TARJETA_BANCO]) then Exit;
		if cb_tipodebito.ItemIndex = 0 then begin
			// Con tarjeta
            TarjOk := ValidarTarjetaCredito(DMConnections.BaseCAC,
			  IVal(StrRight(cb_entidaddebito.Text, 20)), txt_cuentadebito.text, DescriError);
			if not ValidateControls([txt_cuentadebito], [TarjOk], CAPTION_VALIDAR_ADHESION,
              [DescriError]) then Exit;
		end else begin
			// Con cuenta bancaria
			if not ValidateControls([txt_cuentadebito], [Trim(txt_cuentadebito.Text) <> ''],
			  CAPTION_VALIDAR_ADHESION, [MSG_CUENTA_BANCARIA]) then Exit;
		end;
	end;

    //Cargo el registro
    With DatosContacto do begin
        Persona.Personeria := Trim(StrRight(cbPersoneria.Text, 1))[1];
        Persona.CodigoPersona := FCodigoPersona;
        Persona.Apellido            := Trim(txt_apellido.text);
        if StrRight(Trim(cbPersoneria.Text), 1) = PERSONERIA_FISICA then begin
            Persona.ApellidoMaterno     := Trim(txt_apellidoMaterno.text);
            ContactoComercial   := '';
            Persona.Sexo                := trim(cb_Sexo.Text)[1];
        end else begin //JURIDICAS
            Persona.ApellidoMaterno     := '';
            ContactoComercial   := Trim(txt_apellidoMaterno.text);
            ///Sexo                := '';
        end;
        Persona.Nombre              := Trim(txt_nombre.text);
        Persona.TipoDocumento       := Trim(StrRight(cbTipoNumeroDocumento.ComboText, 10));
        Persona.NumeroDocumento     := Trim(cbTipoNumeroDocumento.MaskText);
        Persona.LugarNacimiento     := Trim(StrRight(cb_pais.Text, 20));
        Persona.CodigoActividad     := iif(cb_actividad.ItemIndex < 0, 0,  Ival(StrRight( Trim(cb_actividad.Text), 10)));

        Persona.FechaNacimiento     := txt_fechanacimiento.Date;

        TipoOrigenDatoDomicilioEntrega := 0;
        //Domicilio Particular
        if (DomicilioParticular.IndiceDomicilio = 1) then begin
            CodigoDomicilioParticular := DomicilioParticular.CodigoDomicilio;
            {
            CodigoCalleParticular := DomicilioParticular.CodigoCalle;
            CodigoTipoCalleParticular := DomicilioParticular.CodigoTipoCalle;
            NumeroCalleParticular := DomicilioParticular.NumeroCalle;
            PisoParticular := DomicilioParticular.Piso;
            DptoParticular := DomicilioParticular.Dpto;
            DetalleParticular := DomicilioParticular.Detalle;
            CodigoPostalParticular := DomicilioParticular.CodigoPostal;
            CalleDesnormalizadaParticular := DomicilioParticular.CalleDesnormalizada;
            CodigoTipoEdificacionParticular := DomicilioParticular.CodigoTipoEdificacion;
            CodigoCalleRelacionadaUnoParticular := DomicilioParticular.CodigoCalleRelacionadaUno;
            NumeroCalleRelacionadaUnoParticular := DomicilioParticular.NumeroCalleRelacionadaUno;
            CodigoCalleRelacionadaDosParticular := DomicilioParticular.CodigoCalleRelacionadaDos;
            NumeroCalleRelacionadaDosParticular := DomicilioParticular.NumeroCalleRelacionadaDos;
            NormalizadoParticular := DomicilioParticular.Normalizado;
            if EstaElegidoComoDomicilioEntrega(TIPO_ORIGEN_DATO_PARTICULAR) then
                TipoOrigenDatoDomicilioEntrega := TIPO_ORIGEN_DATO_PARTICULAR;
            }
        end else begin
            CodigoDomicilioParticular := -1;
            {
            CodigoCalleParticular := -1;
            CodigoTipoCalleParticular := -1;
            NumeroCalleParticular := -1;
            PisoParticular := -1;
            DptoParticular := '';
            DetalleParticular := '';
            CodigoPostalParticular := '';
            CalleDesnormalizadaParticular := '';
            CodigoTipoEdificacionParticular := -1;
            CodigoCalleRelacionadaUnoParticular := -1;
            NumeroCalleRelacionadaUnoParticular := -1;
            CodigoCalleRelacionadaDosParticular := -1;
            NumeroCalleRelacionadaDosParticular := -1;
            NormalizadoParticular := false;
            }
        end;

        //Domicilio Comercial
        if (DomicilioComercial.IndiceDomicilio = 1) then begin
            CodigoDomicilioComercial := DomicilioComercial.CodigoDomicilio;
            {
            CodigoCalleComercial := DomicilioComercial.CodigoCalle;
            CodigoTipoCalleComercial := DomicilioComercial.CodigoTipoCalle;
            NumeroCalleComercial := DomicilioComercial.NumeroCalle;
            PisoComercial := DomicilioComercial.Piso;
            DptoComercial := DomicilioComercial.Dpto;
            DetalleComercial := DomicilioComercial.Detalle;
            CodigoPostalComercial := DomicilioComercial.CodigoPostal;
            CalleDesnormalizadaComercial := DomicilioComercial.CalleDesnormalizada;
            CodigoTipoEdificacionComercial := DomicilioComercial.CodigoTipoEdificacion;
            CodigoCalleRelacionadaUnoComercial := DomicilioComercial.CodigoCalleRelacionadaUno;
            NumeroCalleRelacionadaUnoComercial := DomicilioComercial.NumeroCalleRelacionadaUno;
            CodigoCalleRelacionadaDosComercial := DomicilioComercial.CodigoCalleRelacionadaDos;
            NumeroCalleRelacionadaDosComercial := DomicilioComercial.NumeroCalleRelacionadaDos;
            NormalizadoComercial := DomicilioComercial.Normalizado;
            if EstaElegidoComoDomicilioEntrega(TIPO_ORIGEN_DATO_COMERCIAL) then
                TipoOrigenDatoDomicilioEntrega := TIPO_ORIGEN_DATO_COMERCIAL;
            }
        end else begin
            //Limpiar domicilio
            CodigoDomicilioComercial := -1;
            {
            CodigoCalleComercial := -1;
            CodigoTipoCalleComercial := -1;
            NumeroCalleComercial := -1;
            PisoComercial := -1;
            DptoComercial := '';
            DetalleComercial := '';
            CodigoPostalComercial := '';
            CalleDesnormalizadaComercial := '';
            CodigoTipoEdificacionComercial := -1;
            CodigoCalleRelacionadaUnoComercial := -1;
            NumeroCalleRelacionadaUnoComercial := -1;
            CodigoCalleRelacionadaDosComercial := -1;
            NumeroCalleRelacionadaDosComercial := -1;
            NormalizadoComercial := false;
            }
        end;

		// Datos del veh�culo
        TipoPatente             := trim(StrRight(mcPatente.ComboText, 20));
        Patente                 := mcPatente.Masktext;
        CodigoMarca             := IVal(StrRight(cb_marca.Text, 20));
        CodigoModelo            := IVal(StrRight(cb_modelo.Text, 20));
        Categoria               := txt_CodigoCategoria.ValueInt;
        AnioVehiculo            := txt_Anio.ValueInt;
        CodigoColor             := IIf(cb_color.itemindex >= 0,IVal(StrRight(cb_color.Text, 20)),0);
        TieneAcoplado           := chk_acoplado.Checked;
        LargoAdicionalVehiculo  := iif( chk_acoplado.Checked, txt_LargoAdicionalVehiculo.ValueInt, 0);
        DetallePasadas          := chk_detallepasadas.Checked;
        TipoAdhesion            := iif(chk_contag.checked, TA_CON_TAG, TA_SIN_TAG);
        EnvioFacturas           := chk_enviofacturas.checked;

		// Datos del Pago
        TipoPago        := iif(rb_pospagomanual.Checked, TP_POSPAGO_MANUAL, TP_DEBITO_AUTOMATICO);
        TipoDebito      := iif( not rb_pospagoautomatico.Checked, '', iif(cb_tipodebito.ItemIndex = 0, DA_TARJETA_CREDITO, DA_CUENTA_BANCARIA));
        CodigoEntidad   := iif( not rb_pospagoautomatico.Checked, 0, IVal(StrRight(cb_entidaddebito.Text, 20)));
        CuentaDebito    := iif( not rb_pospagoautomatico.Checked, '', Trim(txt_cuentadebito.Text));

        //Medios de comunicacion
        {
        TelefonoParticular.Valor := trim(txtTelefonoParticular.Text);
        TelefonoParticular.Guardar := GuardarTelefonoParticular;
        TelefonoComercial.Valor := trim(txtTelefonoComercial.Text);
        TelefonoComercial.Guardar := GuardarTelefonoComercial;
        TelefonoMovilParticular.Valor := trim(txtTelefonoMovil.Text);
        TelefonoMovilParticular.Guardar := GuardarTelefonoMovil;
        EmailParticular.Valor := trim(txtEmailParticular.Text);
        EmailParticular.Guardar := GuardarEmail;
        }
	end;

	// Guardamos la orden de servicio
    FOrdenServicio := GenerarOrdenAdhesion(
        DMConnections.BaseCAC,
        FCodigoComunicacion,
        PRI_NORMAL,
        Trim(txt_adhesion_observaciones.Text),
        DatosContacto,
        TipoOrigenDatoDomicilioEntrega,
        DescriError);

	if FOrdenServicio <= 0 then begin
        MsgBox( DescriError, MSG_CAPTION_ORDEN_SERVICIO_ADHESION, MB_ICONSTOP);
        Exit;
    end;
	// Listo
	FResult := mrOk;
end;


procedure TFrameAltaOrdenServicio.GuardarComunicacion;
resourcestring
    CAPTION_COMUNICACION    = 'Validar datos de la Comunicaci�n';

    MSG_MEDIO_CONTACTO      = 'Debe seleccionar un Medio de Contacto para Crear la Comunicaci�n';
    MSG_TIPO_CONTACTO       = 'Debe seleccionar un Tipo de Contacto para Crear la Comunicaci�n';
    MSG_CONTACTO            = 'Debe indicar un Contacto para Crear la Comunicaci�n';
var
    ComunicacionTemp: integer;
    FechaHoraInicioTmp: TDateTime;
    MsgError: TMsgError;
    ContactoComunicacion: TDatosContactoComunicacion;
begin
    FResult := mrNone;

	// Validamos los datos del cliente
	if not ValidateControls(
	  [cbMediosContacto,
       bteContacto],

	  [cbMediosContacto.ItemIndex >= 0,
      Trim(bteContacto.Text) <> ''],

	  CAPTION_COMUNICACION,

	  [MSG_MEDIO_CONTACTO,
      MSG_TIPO_CONTACTO,
      MSG_CONTACTO]) then Exit;

    ComunicacionTemp := 0;

    if not ActualizarComunicacion(
        ContactoComunicacion,
        UsuarioSistema,
        FechaHoraInicioTmp,
        nulldate,
        null,
        Ival(StrRight(cbMediosContacto.text, 10)),
        '',//Estado
        nil, //Observaciones	: TStringList;
        ComunicacionTemp, MsgError) then begin
        MsgBox(StrPas(MsgError), MSG_ACTUALIZAR_COMUNICACION, MB_ICONSTOP);
        Exit;
    end;

    if not FinalizarComunicacion(ComunicacionTemp, MsgError) then begin
        MsgBox(StrPas(MsgError), MSG_FINALIZAR_COMUNICACION, MB_ICONSTOP);
        Exit;
    end;

    FCodigoComunicacion  := ComunicacionTemp;
end;

procedure TFrameAltaOrdenServicio.GuardarCierreCuenta;
resourcestring
    CAPTION_CIERRE_CUENTA = 'Validar datos del Cierre de Cuenta';
    MSG_CIERRE_CUENTA     = 'Debe seleccionar la cuenta a cerrar.';
    MSG_MOTIVO_CIERRE_CUENTA = 'Debe escribir alguna observaci�n indicando el motivo del cierre de cuenta.';
Var
    DescriError: AnsiString;
begin
    FResult := mrNone;
	// Validamos los datos del cliente
	if not ValidateControls(
	  [cb_tag_cuenta, txt_tag_observaciones],
	  [cb_tag_cuenta.ItemIndex >= 0, Trim(txt_tag_observaciones.Text) <> ''],
	  CAPTION_CIERRE_CUENTA,
	  [MSG_CIERRE_CUENTA,
      MSG_MOTIVO_CIERRE_CUENTA]) then Exit;


	Screen.Cursor := crHourGlass;

	// Guardamos la orden de servicio
    fOrdenServicio := GenerarOrdenCierreCuenta(
		DMConnections.BaseCAC,
        FCodigoComunicacion,
        PRI_NORMAL,
        Trim(txt_otros_Observaciones.Text),
        txt_tag_cliente.ValueInt, IVal(StrRight(cb_tag_cuenta.Text, 20)),
        DescriError);

	Screen.Cursor := crDefault;

	if OrdenServicio < 1 then begin
    	MsgBox( DescriError, CAPTION_CIERRE_CUENTA, MB_ICONSTOP);
    	Exit;
	end;

	// Listo
	FResult := mrOk;
end;



procedure TFrameAltaOrdenServicio.GuardarReclamoInstalaciones;
resourcestring
    CAPTION_MANTENIMINETO           = 'Validar datos del Mantenimiento';
    MSG_OBSERVACION_MANTENIMIENTO   = 'Debe indicar alguna observaci�n sobre el Reclamo por Mantenimiento.';
Var
    DescriError: AnsiString;
begin
    FResult := mrNone;
	// Validamos que hayan tipeado alg�n comentario
	if not ValidateControls([txt_Otros_observaciones],
	  [Trim(txt_otros_observaciones.Text) <> ''], CAPTION_MANTENIMINETO,
	  [MSG_OBSERVACION_MANTENIMIENTO]) then Exit;


	Screen.Cursor := crHourGlass;

	// Guardamos la orden de servicio
    FOrdenServicio := GenerarOrdenServicioGenerica(
		DMConnections.BaseCAC,
        FCodigoComunicacion,
    	OS_RECLAMO_MALAS_INSTALACIONES,
        PRI_NORMAL,
        Trim(txt_otros_Observaciones.Text),
        DescriError);

	Screen.Cursor := crDefault;

	if OrdenServicio < 1 then begin
    	MsgBox( DescriError, CAPTION_MANTENIMINETO, MB_ICONSTOP);
    	Exit;
	end;

	// Listo
	FResult := mrOk;
end;

procedure TFrameAltaOrdenServicio.GuardarAccidente;
resourcestring
    CAPTION_VALIDAR_ACCIDENTE = 'Validar datos del Reclamo por Accidente';
    MSG_OBSERVACION_ACCIDENTE = 'Debe indicar alguna observaci�n como contenido del Reclamo por Accidente.';
Var
    DescriError: AnsiString;
begin
    FResult := mrNone;
	// Validamos que hayan tipeado alg�n comentario
	if not ValidateControls([txt_Otros_observaciones],
	  [Trim(txt_Otros_observaciones.Text) <> ''], CAPTION_VALIDAR_ACCIDENTE,
	  [MSG_OBSERVACION_ACCIDENTE]) then Exit;

	// Guardamos la orden de servicio
    FOrdenServicio := GenerarOrdenServicioGenerica(
		DMConnections.BaseCAC,
        FCodigoComunicacion,
    	OS_ACCIDENTE,
        PRI_URGENTE,
        Trim(txt_otros_Observaciones.Text),
        DescriError);

	if OrdenServicio < 1 then begin
    	MsgBox( DescriError, CAPTION_VALIDAR_ACCIDENTE, MB_ICONSTOP);
    	Exit;
	end;

	// Listo
	FResult := mrOk;
end;

procedure TFrameAltaOrdenServicio.GuardarOrdenServicioGenerica(TipoOrdenServicio: integer);
resourcestring
    CAPTION_VALIDAR_ORDEN_GENERICA = 'Validar datos de la �rden de Servicio';
    MSG_OBSERVACION_ORDEN_GENERICA = 'Debe indicar alguna observaci�n como contenido de la �rden de Servicio.';
Var
    DescriError: AnsiString;
begin
    FResult := mrNone;
	// Validamos que hayan tipeado alg�n comentario
	if not ValidateControls([txt_Otros_observaciones],
	  [Trim(txt_Otros_observaciones.Text) <> ''], CAPTION_VALIDAR_ORDEN_GENERICA,
	  [MSG_OBSERVACION_ORDEN_GENERICA]) then Exit;

	// Guardamos la orden de servicio
    FOrdenServicio := GenerarOrdenServicioGenerica(
		DMConnections.BaseCAC,
        FCodigoComunicacion,
    	TipoOrdenServicio,
        PRI_BAJA,
        Trim(txt_otros_Observaciones.Text),
        DescriError);

	if OrdenServicio < 1 then begin
    	MsgBox( DescriError, CAPTION_VALIDAR_ORDEN_GENERICA, MB_ICONSTOP);
    	Exit;
	end;

	// Listo
	FResult := mrOk;
end;
procedure TFrameAltaOrdenServicio.GuardarReclamoSeguridad;
resourcestring
    CAPTION_VALIDAR_SEGURIDAD = 'Validar datos del Reclamo por Seguridad';
    MSG_OBSERVACION_SEGURIDAD = 'Debe indicar alguna observaci�n como contenido del Reclamo por Seguridad.';
Var
    DescriError: AnsiString;
begin
    FResult := mrNone;
	// Validamos que hayan tipeado alg�n comentario
	if not ValidateControls([txt_Otros_observaciones],
	  [Trim(txt_Otros_observaciones.Text) <> ''], CAPTION_VALIDAR_SEGURIDAD,
	  [MSG_OBSERVACION_SEGURIDAD]) then Exit;

	// Guardamos la orden de servicio
    FOrdenServicio := GenerarOrdenServicioGenerica(
		DMConnections.BaseCAC,
        FCodigoComunicacion,
    	OS_FALTA_SEGURIDAD,
        PRI_ALTA,
        Trim(txt_otros_Observaciones.Text),
        DescriError);

	if OrdenServicio < 1 then begin
    	MsgBox( DescriError, CAPTION_VALIDAR_SEGURIDAD, MB_ICONSTOP);
    	Exit;
	end;

	// Listo
	FResult := mrOk;
end;

procedure TFrameAltaOrdenServicio.GuardarAgradecimiento;
resourcestring
    CAPTION_AGRADECIMIENTO = 'Validar datos del Agradecimiento';
    MSG_AGRADECIMIENTO = 'Debe indicar alg�n texto para el Agradecimiento.';
Var
    DescriError: AnsiString;
begin
    FResult := mrNone;
	// Validamos que hayan tipeado alg�n comentario
	if not ValidateControls([txt_otros_Observaciones],
	  [Trim(txt_otros_Observaciones.Text) <> ''], CAPTION_AGRADECIMIENTO,
	  [MSG_AGRADECIMIENTO]) then Exit;

	// Guardamos la orden de servicio
    FOrdenServicio := GenerarOrdenServicioGenerica(
		DMConnections.BaseCAC,
        FCodigoComunicacion,
    	OS_AGRADECIMIENTO,
        PRI_NORMAL,
        Trim(txt_otros_Observaciones.Text),
        DescriError);

	if OrdenServicio < 1 then begin
    	MsgBox( DescriError, CAPTION_AGRADECIMIENTO, MB_ICONSTOP);
    	Exit;
	end;

	// Listo
	FResult := mrOk;
end;

procedure TFrameAltaOrdenServicio.GuardarReclamoGeneral;
resourcestring
    CAPTION_RECLAMO_GENERAL = 'Validar datos del Reclamo General';
    MSG_ObSERVACIONES_RECLAMO_GENERAL = 'Debe indicar alguna observaci�n sobre el Reclamo General.';
var
    DescriError: AnsiString;
begin
    FResult := mrNone;
	// Validamos que hayan tipeado alg�n comentario
	if not ValidateControls([txt_Otros_Observaciones],
	  [Trim(txt_Otros_Observaciones.Text) <> ''], CAPTION_RECLAMO_GENERAL,
	  [MSG_OBSERVACIONES_RECLAMO_GENERAL]) then Exit;

	// Guardamos la orden de servicio
    FOrdenServicio := GenerarOrdenServicioGenerica(
		DMConnections.BaseCAC,
        FCodigoComunicacion,
        OS_RECLAMO_GENERAL,
    	PRI_NORMAL,
        Trim(txt_otros_Observaciones.Text),
        DescriError);

	if OrdenServicio < 1 then begin
    	MsgBox( DescriError, CAPTION_RECLAMO_GENERAL, MB_ICONSTOP);
    	Exit;
	end;

	// Listo
	FResult := mrOk;
end;

procedure TFrameAltaOrdenServicio.GuardarSugerencia;
resourcestring
    CAPTION_SUGERENCIA = 'Validar datos de la Sugerencia';
    MSG_OBSERVACIONES_SUGERENCIA = 'Debe indicar alg�n texto para la Sugerencia.';
Var
    DescriError: AnsiString;
begin
    FResult := mrNone;
	// Validamos que hayan tipeado alg�n comentario
	if not ValidateControls([txt_otros_Observaciones],
	  [Trim(txt_otros_Observaciones.Text) <> ''], CAPTION_SUGERENCIA,
	  [MSG_OBSERVACIONES_SUGERENCIA]) then Exit;

	// Guardamos la orden de servicio
    FOrdenServicio := GenerarOrdenServicioGenerica(
		DMConnections.BaseCAC,
        FCodigoComunicacion,
    	OS_RECOMENDACION,
        PRI_NORMAL,
        Trim(txt_otros_Observaciones.Text),
        DescriError);

	if OrdenServicio < 1 then begin
    	MsgBox( DescriError, CAPTION_SUGERENCIA, MB_ICONSTOP);
    	Exit;
	end;

	// Listo
	FResult := mrOk;
end;

procedure TFrameAltaOrdenServicio.GuardarReclamoMantenimiento;
resourcestring
    CAPTION_SOLICITUD_TRABAJO = 'Validar datos del Reclamo de Mantenimiento';
    MSG_OBSERVACIONES_RECLAMO  = 'Debe indicar alg�n texto de observaciones para definir el reclamo.';
    MSG_TIPO_FALLA  = 'Debe indicar el Tipo de Falla que origin� el reclamo.';
    MSG_UBICACION  = 'Debe indicar alguna ubicaci�n para detallar el reclamo.';
Var
    DescriError: AnsiString;
begin
    FResult := mrNone;
	// Validamos que hayan Cargado Algun Tipo de Falla
	if not ValidateControls([cbTiposFallas],
	  [Trim(StrRight(cbTiposFallas.Text, 10)) <> '0'], CAPTION_SOLICITUD_TRABAJO,
	  [MSG_TIPO_FALLA]) then Exit;

	// Validamos que hayan Cargado alguna Ubicacion
	if not ValidateControls([cbUbicaciones],
	  [Trim(StrRight(cbUbicaciones.text, 10)) <> '0'], CAPTION_SOLICITUD_TRABAJO,
	  [MSG_UBICACION]) then Exit;

	// Validamos que hayan tipeado alg�n comentario
	if not ValidateControls([mmDescripcion],
	  [Trim(mmDescripcion.Text) <> ''], CAPTION_SOLICITUD_TRABAJO,
	  [MSG_OBSERVACIONES_RECLAMO]) then Exit;


	// Guardamos la orden de servicio
    FOrdenServicio := GenerarOrdenServicioSolicitudTrabajo(
		DMConnections.BaseCAC,
        FCodigoComunicacion,
        PRI_NORMAL,
        Trim(mmDescripcion.Text),
    	Ival(StrRight( cbTiposFallas.Text, 10)),
        Ival(StrRight( cbUbicaciones.Text, 10)),
        Ival(StrRight( cbTiposMantenimiento.Text, 10)),
        Ival(StrRight( cbTiposCausa.Text, 10)),
        DescriError);

	if OrdenServicio < 1 then begin
    	MsgBox( DescriError,CAPTION_SOLICITUD_TRABAJO, MB_ICONSTOP);
    	Exit;
	end;

	// Listo
	FResult := mrOk;
end;

procedure TFrameAltaOrdenServicio.GuardarOrdenTrabajoReparacionPostes;
resourcestring
    CAPTION_REPARACION_POSTES = 'Validar datos de la �rden de Trabajo';
    MSG_OBSERVACIONES_RECLAMO = 'Debe indicar alg�n texto de observaciones para definir la �rden de Trabajo';
    MSG_TIPO_FALLA = 'Debe indicar el Tipo de Falla que origin� la �rden de Trabajo';
    MSG_UBICACION = 'Debe indicar alguna ubicaci�n para detallar la �rden de Trabajo';
    MSG_CAPTION_ERROR = 'Generar Orden de Servicio de Reparaci�n de POSTES';
Var
	DescriError: AnsiString;
begin
    FResult 	:= mrNone;
    DescriError	:= '';
	// Validamos que hayan Cargado Algun Tipo de Falla
	if not ValidateControls([cbTiposFallas],
	  [Trim(StrRight(cbTiposFallas.Text, 10)) <> '0'], CAPTION_REPARACION_POSTES,
	  [MSG_TIPO_FALLA]) then Exit;

	// Validamos que hayan Cargado alguna Ubicacion
	if not ValidateControls([cbUbicaciones],
	  [Trim(StrRight(cbUbicaciones.text, 10)) <> '0'], CAPTION_REPARACION_POSTES,
	  [MSG_UBICACION]) then Exit;

	// Validamos que hayan tipeado alg�n comentario
	if not ValidateControls([mmDescripcion],
	  [Trim(mmDescripcion.Text) <> ''], CAPTION_REPARACION_POSTES,
	  [MSG_OBSERVACIONES_RECLAMO]) then Exit;

    Screen.Cursor := crHourGlass;

	// Guardamos la orden de servicio
	FOrdenServicio := GenerarOrdenServicioReparacionPostes(
		DMConnections.BaseCAC,
        FCodigoComunicacion,
        PRI_NORMAL,
        mmDescripcion.text,
    	Ival(StrRight( cbTiposFallas.Text, 10)),
        Ival(StrRight( cbUbicaciones.Text, 10)),
        Ival(StrRight( cbTiposMantenimiento.Text, 10)),
        Ival(StrRight( cbTiposCausa.Text, 10)),
        DescriError);

	if OrdenServicio < 1 then begin
    	MsgBox(DescriError, MSG_CAPTION_ERROR, MB_ICONSTOP);
    	Exit;
	end;


    Screen.Cursor := crDefault;
	// Listo
	FResult := mrOk;
end;

procedure TFrameAltaOrdenServicio.GuardarOrdenTrabajoReparacionPorticos;
resourcestring
    CAPTION_REPARACION_PORTICOS = 'Validar datos de la �rden de Trabajo';
    MSG_OBSERVACIONES_RECLAMO = 'Debe indicar alg�n texto de observaciones para definir la �rden de Trabajo';
    MSG_TIPO_FALLA = 'Debe indicar el Tipo de Falla que origin� la �rden de Trabajo';
    MSG_UBICACION = 'Debe indicar alguna ubicaci�n para detallar la �rden de Trabajo';
Var
    DescriError: AnsiString;
begin
    FResult := mrNone;

	// Validamos que hayan Cargado Algun Tipo de Falla
	if not ValidateControls([cbTiposFallas],
	  [Trim(StrRight(cbTiposFallas.Text, 10)) <> '0'], CAPTION_REPARACION_PORTICOS,
	  [MSG_TIPO_FALLA]) then Exit;

	// Validamos que hayan Cargado alguna Ubicacion
	if not ValidateControls([cbUbicaciones],
	  [Trim(StrRight(cbUbicaciones.text, 10)) <> '0'], CAPTION_REPARACION_PORTICOS,
	  [MSG_UBICACION]) then Exit;

	// Validamos que hayan tipeado alg�n comentario
	if not ValidateControls([mmDescripcion],
	  [Trim(mmDescripcion.Text) <> ''], CAPTION_REPARACION_PORTICOS,
	  [MSG_OBSERVACIONES_RECLAMO]) then Exit;



	// Guardamos la orden de servicio
	FOrdenServicio := GenerarOrdenServicioReparacionPortico(
		DMConnections.BaseCAC,
        FCodigoComunicacion,
        PRI_NORMAL,
        mmDescripcion.text,
    	Ival(StrRight( cbTiposFallas.Text, 10)),
        Ival(StrRight( cbUbicaciones.Text, 10)),
        Ival(StrRight( cbTiposMantenimiento.Text, 10)),
        Ival(StrRight( cbTiposCausa.Text, 10)),
        DescriError);

	if OrdenServicio < 1 then begin
    	MsgBox(DescriError, CAPTION_REPARACION_PORTICOS, MB_ICONSTOP);
    	Exit;
	end;

	// Listo
	FResult := mrOk;
end;

procedure TFrameAltaOrdenServicio.GuardarOrdenTrabajoReparacionPavimento;
resourcestring
    CAPTION_REPARACION_PAVIMENTO = 'Validar datos de la �rden de Trabajo';
    MSG_OBSERVACIONES_RECLAMO = 'Debe indicar alg�n texto de observaciones para definir la �rden de Trabajo';
    MSG_TIPO_FALLA = 'Debe indicar el Tipo de Falla que origin� la �rden de Trabajo';
    MSG_UBICACION = 'Debe indicar alguna ubicaci�n para detallar la �rden de Trabajo';
Var
    DescriError: AnsiString;
begin
    FResult := mrNone;

	// Validamos que hayan Cargado Algun Tipo de Falla
	if not ValidateControls([cbTiposFallas],
	  [Trim(StrRight(cbTiposFallas.Text, 10)) <> '0'], CAPTION_REPARACION_PAVIMENTO,
	  [MSG_TIPO_FALLA]) then Exit;

	// Validamos que hayan Cargado alguna Ubicacion
	if not ValidateControls([cbUbicaciones],
	  [Trim(StrRight(cbUbicaciones.text, 10)) <> '0'], CAPTION_REPARACION_PAVIMENTO,
	  [MSG_UBICACION]) then Exit;

	// Validamos que hayan tipeado alg�n comentario
	if not ValidateControls([mmDescripcion],
	  [Trim(mmDescripcion.Text) <> ''], CAPTION_REPARACION_PAVIMENTO,
	  [MSG_OBSERVACIONES_RECLAMO]) then Exit;

	// Guardamos la orden de servicio
	FOrdenServicio := GenerarOrdenServicioReparacionPortico(
		DMConnections.BaseCAC,
        FCodigoComunicacion,
        PRI_NORMAL,
        mmDescripcion.text,
    	Ival(StrRight( cbTiposFallas.Text, 10)),
        Ival(StrRight( cbUBicaciones.Text, 10)),
        Ival(StrRight( cbTiposMantenimiento.Text, 10)),
        Ival(StrRight( cbTiposCausa.Text, 10)),
        DescriError);

	if OrdenServicio < 1 then begin
    	MsgBox(DescriError, CAPTION_REPARACION_PAVIMENTO, MB_ICONSTOP);
    	Exit;
	end;

	FResult := mrOk;
end;

procedure TFrameAltaOrdenServicio.cb_cierre_cuentaClick(Sender: TObject);
begin
    { TODO : Aprovechar ac� para mostrar un resumen del estado de la cuenta }
end;

procedure TFrameAltaOrdenServicio.txt_fact_clienteButtonClick(Sender: TObject);
Var
	f: TFormBuscaClientes;
begin
	Application.CreateForm(TFormBuscaClientes, f);
	if f.Inicializa('', '', '', '', '') and (f.ShowModal = mrOk) then begin
		txt_fact_cliente.Value := f.Persona.CodigoPersona;
	end;
	f.Release;
end;

procedure TFrameAltaOrdenServicio.txt_fact_clienteChange(Sender: TObject);
begin
	// Mostramos el nombre del cliente
	lbl_fact_cliente.caption := QueryGetValue(DMConnections.BaseCAC,
	  'SELECT dbo.ObtenerApellidoNombreCliente('+ FloatToStr(txt_fact_cliente.Value) + ')');

	// Cargamos las cuentas
	CargarFacturas(DMConnections.BaseCAC, cb_fact_factura,
	  Round(txt_fact_cliente.Value));
	cb_fact_factura.ItemIndex := 0;
end;


procedure TFrameAltaOrdenServicio.GuardarReclamoFactura;
Var
    DescriError: AnsiString;
begin
    FResult := mrNone;
	// Validamos que hayan tipeado alg�n comentario
	if not ValidateControls(
	  [txt_fact_cliente,
      cb_Fact_Factura,
      txt_fact_observaciones],
	  [txt_fact_cliente.value > 0,
      (cb_Fact_Factura.Items.count > 0) and (cb_Fact_Factura.itemindex > -1),
      Trim(txt_fact_observaciones.Text) <> ''],
	  CAPTION_RECLAMO_FACTURACION,
	  [MSG_CLIENTE,
      MSG_SELECCIONAR_FACTURA,
	  MSG_OBSERVACIONES_FACTURACION]) then Exit;

	// Guardamos la orden de servicio
	FOrdenServicio := GenerarOrdenServicioReclamoFactura(
        	DMConnections.BaseCAC, FCodigoComunicacion, PRI_NORMAL, Trim(txt_fact_observaciones.Text),
            txt_fact_cliente.Valueint, IVal(StrRight(cb_fact_factura.Text, 20)), DescriError);

	if OrdenServicio < 1 then begin
		MsgBox(DescriError,CAPTION_RECLAMO_FACTURACION, MB_ICONSTOP);
    	Exit;
	end;

	// Listo
	FResult := mrOk;
end;

procedure TFrameAltaOrdenServicio.GuardarReclamoFaltaFactura;
Var
    DescriError: AnsiString;
begin
    FResult := mrNone;
	// Validamos que hayan tipeado alg�n comentario
	if not ValidateControls(
	  [txt_fact_cliente, txt_fact_observaciones],
	  [txt_fact_cliente.value > 0, Trim(txt_fact_observaciones.Text) <> ''],
	  CAPTION_RECLAMO_FACTURACION,
	  [MSG_CLIENTE,
	  MSG_OBSERVACIONES_FACTURACION]) then Exit;

	// Guardamos la orden de servicio
	FOrdenServicio := GenerarOrdenServicioReclamoFacturaFaltante(
        	DMConnections.BaseCAC, FCodigoComunicacion, PRI_NORMAL, Trim(txt_fact_observaciones.Text),
            txt_fact_cliente.Valueint, EncodeDate(IVal(cb_fact_anio.text), cb_fact_mes.ItemIndex + 1, 1),
            DescriError);


	if OrdenServicio < 1 then begin
		MsgBox(DescriError,CAPTION_RECLAMO_FACTURACION, MB_ICONSTOP);
    	Exit;
	end;

	// Listo
	FResult := mrOk;
end;

procedure TFrameAltaOrdenServicio.txt_tag_clienteButtonClick(Sender: TObject);
Var
	f: TFormBuscaClientes;
begin
	Application.CreateForm(TFormBuscaClientes, f);
	if f.Inicializa('', '', '', '', '') and (f.ShowModal = mrOk) then begin
		txt_tag_cliente.Value := f.Persona.CodigoPersona;
	end;
	f.Release;
end;

procedure TFrameAltaOrdenServicio.txt_tag_clienteChange(Sender: TObject);
begin
	// Mostramos el nombre del cliente
	//lbl_tag_cliente.caption := QueryGetValue(DMConnections.BaseCAC,
	//  'SELECT RTRIM(Apellido) + '', '' + RTRIM(Nombre) FROM Clientes ' +
	//  'WHERE CodigoCliente = ' + FloatToStr(txt_tag_cliente.Value));

	lbl_tag_cliente.caption := QueryGetValue(DMConnections.BaseCAC,
	  'SELECT dbo.ObtenerApellidoNombreCliente('+ FloatToStr(txt_tag_cliente.Value) + ')');

	// Cargamos las cuentas
	PeaProcs.CargarCuentas(DMConnections.BaseCAC, cb_tag_cuenta,
	  Round(txt_tag_cliente.Value));
	cb_tag_cuenta.ItemIndex := 0;
end;

procedure TFrameAltaOrdenServicio.GuardarInhabilitacionTag;
resourcestring
    CAPTION_INHABILITAR_TAG         = 'Inhabilitar TAG';
    MSG_CUENTA_INHABILITAR_TAG      = 'Debe indicar una cuenta para la cual se inhabilitar� el TAG.';
    MSG_OBSERVACION_INHABILITAR_TAG = 'Debe indicar alguna observaci�n para Inhabilitar el TAG.';
Var
    DescriError: AnsiString;
begin
    FResult := mrNone;
	// Validamos que hayan tipeado alg�n comentario
	if not ValidateControls(
	  [txt_tag_cliente, cb_tag_cuenta, txt_tag_observaciones],
	  [txt_tag_cliente.value > 0, cb_tag_cuenta.itemIndex >= 0,
	  Trim(txt_tag_observaciones.Text) <> ''],
	  CAPTION_INHABILITAR_TAG,
	  [MSG_CLIENTE,
	  MSG_CUENTA_INHABILITAR_TAG,
      MSG_OBSERVACION_INHABILITAR_TAG]) then Exit;

	// Guardamos la orden de servicio
	FOrdenservicio := GenerarOrdenServicioInhabilitacionTag(
    		DMConnections.BaseCAC, FCodigoComunicacion, PRI_NORMAL, Trim(txt_tag_observaciones.Text),
           	txt_tag_cliente.ValueInt, IVal(StrRight(cb_tag_cuenta.Text, 20)), DescriError);

	if OrdenServicio < 1 then begin
    	MsgBox( DescriError, CAPTION_INHABILITAR_TAG, MB_ICONSTOP);
    	Exit;
    end;

	// Listo
	FResult := mrOk;
end;

procedure TFrameAltaOrdenServicio.GuardarRehabilitacionTag;
resourcestring
    CAPTION_REHABILITAR_TAG         = 'Rehabilitar TAG';
    MSG_CUENTA_REHABILITAR_TAG      = 'Debe indicar una cuenta para la cual se rehabilitar� el TAG.';
    MSG_OBSERVACION_REHABILITAR_TAG = 'Debe indicar alguna observaci�n para Rehabilitar el TAG.';
Var
    DescriError: ansiString;
begin
    FResult := mrNone;
	// Validamos que hayan tipeado alg�n comentario
	if not ValidateControls(
	  [txt_tag_cliente, cb_tag_cuenta, txt_tag_observaciones],
	  [txt_tag_cliente.value > 0, cb_tag_cuenta.itemIndex >= 0,
	  Trim(txt_tag_observaciones.Text) <> ''],
	  CAPTION_REHABILITAR_TAG,
	  [MSG_CLIENTE,
	  MSG_CUENTA_REHABILITAR_TAG,
	  MSG_OBSERVACION_REHABILITAR_TAG]) then Exit;

	// Guardamos la orden de servicio
	FOrdenservicio := GenerarOrdenServicioRehabilitacionTag(
    		DMConnections.BaseCAC, FCodigoComunicacion, PRI_NORMAL, Trim(txt_tag_observaciones.Text),
           	txt_tag_cliente.ValueInt, IVal(StrRight(cb_tag_cuenta.Text, 20)), DescriError);

	if OrdenServicio < 1 then begin
    	MsgBox( DescriError, CAPTION_REHABILITAR_TAG, MB_ICONSTOP);
    	Exit;
    end;

	// Listo
	FResult := mrOk;
end;

procedure TFrameAltaOrdenServicio.chk_detallepasadasClick(Sender: TObject);
begin
	// Si piden detalle de pasadas, corresponde tambi�n env�o de facturas
    if chk_detallepasadas.checked then begin
    	chk_enviofacturas.checked := true;
        chk_enviofacturas.enabled := false;
    end else begin
        chk_enviofacturas.enabled := true;
    end;
end;


procedure TFrameAltaOrdenServicio.cb_marcaChange(Sender: TObject);
begin
	CargarModelosVehiculos(DMConnections.BaseCAC, cb_modelo, IVal(StrRight(cb_marca.text, 20)));
    TraerDimensionesVehiculo;
    TraerDescripcionCategoria;
end;

procedure TFrameAltaOrdenServicio.cb_tipodebitoChange(Sender: TObject);
resourcestring
    CAPTION_TARJETA = 'Tarjeta:';
    CAPTION_BANCO   = 'Banco:';
begin
	if cb_tipodebito.ItemIndex = 0 then begin
		CargarTarjetasCredito(DMConnections.BaseCAC, cb_entidaddebito);
		lbl_entidad.Caption := CAPTION_TARJETA;
	end else begin
		CargarBancos(DMConnections.BaseCAC, cb_entidaddebito);
		lbl_entidad.Caption := CAPTION_BANCO;
	end;
end;

procedure TFrameAltaOrdenServicio.Guardar;
ResourceString
	MSG_CAPTION = 'Guardar Orden de Servicio';
    MSG_DETALLE = 'Debe Seleccionar un Tipo de Orden de Servicio';
var
    ComunicacionTemp: integer;
    FechaHoraInicioTmp: TDateTime;
    MsgError: TMsgError;
    ContactoComunicacion: TDatosContactoComunicacion;
begin
    if tvOrdenesServicio.selected.Level = 0 then begin
        MsgBox( MSG_DETALLE,MSG_CAPTION,MB_ICONINFORMATION);
        Exit;
    end else begin
        // Primero me fijo si tengo una Comunicacion creada
        if (FCodigoComunicacion <= 0) then begin
            if (FTipoOrdenServicio in [OS_GENERAR_ORDEN_DE_TRABAJO, OS_REPARAR_POSTE_SOS, OS_REPARAR_PORTICO, OS_REPARAR_PAVIMENTO])
                and (not FMedioContacto in [MC_EMAIL, MC_FAX, MC_VOICE_MAIL]) then
                GuardarComunicacion
            else begin
                ComunicacionTemp := 0;
                FechaHoraInicioTmp := nulldate;
                if not ActualizarComunicacion(
                    ContactoComunicacion,
                    UsuarioSistema,
                    FechaHoraInicioTmp,
                    nulldate,
                    null,
                    FMedioContacto,
                    '',
                    nil,
                    ComunicacionTemp, MsgError) then begin
                        MsgBox(StrPas(MsgError), MSG_ACTUALIZAR_COMUNICACION, MB_ICONSTOP);
                        Exit;
                end;

                if not FinalizarComunicacion(ComunicacionTemp, MsgError) then begin
                    MsgBox(StrPas(MsgError), MSG_FINALIZAR_COMUNICACION, MB_ICONSTOP);
                    Exit;
                end;

                FCodigoComunicacion  := ComunicacionTemp;
            end;
        end;

        case FTipoOrdenServicio of
            OS_ADHESION: GuardarAdhesion;
            OS_CIERRE_CUENTA: GuardarCierreCuenta;
            OS_MODIFICACION_DATOS_CUENTA_CLIENTE :;
            OS_CAMBIO_MODALIDAD_PAGO: ;
            OS_REHABILITACION_TAG: GuardarRehabilitacionTag;
            OS_INHABILITACION_TAG: GuardarInhabilitacionTag;
            OS_FACTURACION_INCORRECTA: GuardarReclamoFactura;
            OS_FACTURA_NO_RECIBIDA: GuardarReclamoFaltaFactura;
            OS_ACCIDENTE: GuardarAccidente;
            OS_FALTA_SEGURIDAD: guardarReclamoSeguridad;
            OS_RECLAMO_GENERAL: GuardarReclamoGeneral;
            OS_RECOMENDACION: GuardarSugerencia;
            OS_AGRADECIMIENTO: GuardarAgradecimiento;
			OS_GENERAR_ORDEN_DE_TRABAJO: GuardarReclamoMantenimiento;
			OS_REPARAR_POSTE_SOS: GuardarOrdenTrabajoReparacionPostes;
			OS_REPARAR_PORTICO: GuardarOrdenTrabajoReparacionPorticos;
            OS_REPARAR_PAVIMENTO: GuardarOrdenTrabajoReparacionPavimento;
			OS_RECLAMO_MALAS_INSTALACIONES: GuardarReclamoInstalaciones;
            OS_CAMBIO_TAG: GuardarCambioTAG;
            OS_MAL_FUNCIONAMIENTO_TAG: GuardarMalFuncionamiento;
			else GuardarOrdenServicioGenerica(FTipoOrdenServicio);
        end;
    end;
end;

procedure TFrameAltaOrdenServicio.CargarArbolOrdenesServicio(Categoria, TipoOrdenServicio: Integer);
var
    Node, RaizTree: TTreeNode;
    Codigo: ^Integer;
    DescripcionCategoria: AnsiString;
begin
    with ObtenerOrdenesServicio do begin
        Parameters.ParamByName('@Categoria').Value := Categoria;
        Parameters.ParamByName('@TipoOrdenServicio').Value := TipoOrdenServicio;
        Parameters.ParamByName('@RetornarSoloContactos').Value := FEsContacto;
        open;
    end;
    try
        with tvOrdenesServicio.Items do begin
            Clear;
            while not ObtenerOrdenesServicio.eof do begin
                new(Codigo);
                Codigo^ := ObtenerOrdenesServicio.FieldByName('CodigoCategoriaOrdenServicio').AsInteger;
                RaizTree := Add(nil, Trim(ObtenerOrdenesServicio.FieldByName('CategoriaOrdenServicio').AsString));
                RaizTree.ImageIndex	:= 0;
                RaizTree.SelectedIndex	:= 2;
                RaizTree.Data := Codigo;
                DescripcionCategoria := Trim(ObtenerOrdenesServicio.FieldByName('CategoriaOrdenServicio').AsString);
                while (Trim(ObtenerOrdenesServicio.FieldByName('CategoriaOrdenServicio').AsString) = DescripcionCategoria) and
                  (not ObtenerOrdenesServicio.eof) do begin
                    new(Codigo);
                    Codigo^ := ObtenerOrdenesServicio.FieldByName('TipoOrdenServicio').AsInteger;
                    Node := AddChild( RaizTree, Trim(ObtenerOrdenesServicio.FieldByName('DescripcionOrdenServicio').AsString));
                    Node.ImageIndex := 1;
                    Node.SelectedIndex	:= 2;
                    Node.Data := Codigo;
                    ObtenerOrdenesServicio.next;
                end;
            end;
        end;
    finally
        ObtenerOrdenesServicio.Close;
    end;
end;

procedure TFrameAltaOrdenServicio.tvOrdenesServicioClick(Sender: TObject);
begin
	if tvOrdenesServicio.Items.Count = 0 then exit;
	FTipoOrdenServicio := Integer(tvOrdenesServicio.Selected.Data^);
    if tvOrdenesServicio.selected.Level = 0 then begin
        case FTipoOrdenServicio of
			CAT_OS_RECLAMO:
                PageControl.ActivePage		:= tab_otros;
		    CAT_OS_CONSULTA:
                PageControl.ActivePage		:= tab_otros;
		    CAT_OS_INFORMACION:
                PageControl.ActivePage		:= tab_otros;
            CAT_OS_FACTURACION:
                PageControl.ActivePage		:= tab_Facturacion;
		    CAT_OS_ACTUALIZACION_CUENTA:
                begin
                    PageControl.ActivePage 		:= tab_cuentas;
                    ts_DatosCliente.TabVisible	:= True;
                    ts_DatosCuenta.TabVisible	:= True;
                    ts_Observaciones.TabVisible	:= True;
                    pc_Adhesion.ActivePageIndex := 0;
                end;
		    CAT_OS_MANTENIMIENTO: PageControl.ActivePage	:= tsOrdenTrabajo;
        end;
    end else begin
        case FTipoOrdenServicio of
            OS_ADHESION:
                begin
                    PageControl.ActivePage 		:= tab_cuentas;
                    ts_DatosCliente.TabVisible	:= True;
                    ts_DatosCuenta.TabVisible	:= True;
                    ts_Observaciones.TabVisible	:= True;
                    pc_Adhesion.ActivePageIndex := 0;
                end;
            OS_CIERRE_CUENTA:
            	begin
                	PageControl.ActivePage	:= tab_tags;
            	end;
            OS_REHABILITACION_TAG, OS_INHABILITACION_TAG, OS_CAMBIO_TAG:
                PageControl.ActivePage := tab_tags;
            OS_FACTURA_NO_RECIBIDA:
                begin
                    PageControl.ActivePage		:= tab_facturacion;
                    pnl_Factura.Visible 		:= False;
                    pnl_UltimaFactura.Visible	:= True;
                end;
            OS_FACTURACION_INCORRECTA:
                begin
                    PageControl.ActivePage		:= tab_facturacion;
                    pnl_Factura.Visible 		:= true;
                    pnl_UltimaFactura.Visible	:= False;
                end;
            OS_RECLAMO_GENERAL, OS_RECOMENDACION, OS_AGRADECIMIENTO, OS_ACCIDENTE,
            OS_FALTA_SEGURIDAD, OS_RECLAMO_MALAS_INSTALACIONES:
                PageControl.ActivePage		:= tab_otros;
			OS_GENERAR_ORDEN_DE_TRABAJO: PageControl.ActivePage	:= tsOrdenTrabajo;
			OS_REPARAR_POSTE_SOS, OS_REPARAR_PORTICO,
            	OS_REPARAR_PAVIMENTO: PageControl.ActivePage	:= tsOrdenTrabajo;
			else begin
                PageControl.ActivePage		:= tab_otros;
			end;
        end;
    end;
end;

procedure TFrameAltaOrdenServicio.SeleccionarTreeNode(Codigo: Integer);
var i: Integer;
begin
    i := 0;
    while (i < tvOrdenesServicio.Items.Count - 1) do begin
        if tvOrdenesServicio.Items[i].Level = 0  then
            inc(i)
        else
            if Integer(tvOrdenesServicio.Items[i].Data^) <> Codigo then
                inc(i)
            else
                break;
    end;
    if i = tvOrdenesServicio.Items.Count then
        raise Exception.Create(MSG_ERROR_CODIGO_INEXISTENTE);
    tvOrdenesServicio.Select(tvOrdenesServicio.Items[i]);
    FtipoOrdenServicio := Integer(tvOrdenesServicio.Selected.Data^);
end;

procedure TFrameAltaOrdenServicio.EliminarTreeNode(Codigo: Integer);
var i: Integer;
begin
    i := 0;
    while (i < tvOrdenesServicio.Items.Count) do begin
        if tvOrdenesServicio.Items[i].Level = 0  then begin
            if tvOrdenesServicio.Items[i].Count > 0 then
                inc(i)
            else
                tvOrdenesServicio.items[i].Free;
        end else
            if Integer(tvOrdenesServicio.Items[i].Data^) <> Codigo then
                inc(i)
            else begin
            	//Dispose(tvOrdenesServicio.Items[i].Data);
                tvOrdenesServicio.items[i].Free;
			end;
    end;
end;


procedure TFrameAltaOrdenServicio.tvOrdenesServicioDeletion(
  Sender: TObject; Node: TTreeNode);
begin
    Dispose(Node.Data);
end;

procedure TFrameAltaOrdenServicio.ObtenerClienteContacto(
  CodigoComunicacion: Integer; var FCodigoContacto: integer);
var
    CodCont: Integer;
begin
    CodCont := QueryGetValueInt(DMConnections.BaseCAC,
      'SELECT CodigoPersona FROM Comunicaciones WHERE CodigoComunicacion = ' +
      IntToStr(CodigoComunicacion));

    if QueryGetValueInt(DMConnections.BaseCAC,
      ' SELECT 1 FROM Clientes WHERE CodigoPersona = ' + IntToStr(CodCont)) = 1 then begin
        // Es Cliente
        FCodigoContacto := 0;
    end else begin
        // No es Cliente
        FCodigoContacto := CodCont;
    end;
    FCodigoPersona := CodCont;
end;

procedure TFrameAltaOrdenServicio.CargarDatosReclamoFactura(
  aCodigoCliente, aNumeroFactura: Integer);
var
    i: Integer;
begin
    txt_fact_cliente.Value := FCodigoPersona;
    // Cargamos el combo de facturas (Cuando reclama una factura)
    //CargarFacturas(DMConnections.BaseCAC, cb_fact_Factura, FCodigoPersona, aNumeroFactura);
    // Cargamos el combo de ciclo de facturaci�n (Reclama envio de factura)
    for i := Year(Date) - 4 to Year(Date) do cb_fact_anio.Items.Add(IntToStr(i));
    cb_fact_anio.ItemIndex := 4;
    for i := 1 to 12 do cb_fact_mes.Items.Add(LongMonthNames[i]);
    cb_fact_mes.ItemIndex := Month(Date) - 1;
    cb_fact_factura.ItemIndex := 0;
end;

function TFrameAltaOrdenServicio.InicializarAdhesionContacto(
  CodigoComunicacion, CategoriaOS, TipoOrdenServicio: Integer): Boolean;
begin
	Result := False;
    FCodigoComunicacion := CodigoCOmunicacion;
    try
        // Obtener Informaci�n Cliente o Contacto
        ObtenerClienteContacto(CodigoComunicacion, FCodigoContacto);

        FEsContacto := not ExisteCliente(DMConnections.BaseCAC, FCodigoPersona);
        CargarArbolOrdenesServicio(CategoriaOS, TipoOrdenServicio);
        
        // Cuestiones visuales
(*      PageControl.ActivePageIndex := 0;
        ts_DatosCliente.TabVisible	:= True;
        ts_DatosCuenta.TabVisible	:= True;
        ts_Observaciones.TabVisible	:= True;*)
        // Cargamos informaci�n del cliente
        CargarCliente(FCodigoPersona, '', '');

        PageControl.ActivePage := tab_Cuentas;
        SeleccionarTreeNode(OS_ADHESION);
	except
    	On E: Exception do begin
	    	Exit;
		end;
    end;
    result := True;
end;

procedure TFrameAltaOrdenServicio.GuardarCambioTAG;
resourcestring
    CAPTION_CAMBIAR_TAG = 'Cambio de TAG';
    MSG_CUENTA_CAMBIAR_TAG  = 'Debe indicar una cuenta para la cual se cambiar� el TAG.';
    MSG_OBSERVACION_CAMBIAR_TAG = 'Debe indicar alguna observaci�n para cambiar el TAG.';
    MSG_CAMBIO_TAG_CAPTION = 'Generar la Orden de Servicio de Cambio de TAG';
Var
    DescriError: AnsiString;
begin
    FResult := mrNone;
	// Validamos que hayan tipeado alg�n comentario
	if not ValidateControls(
	  [txt_tag_cliente, cb_tag_cuenta, txt_tag_observaciones],
	  [txt_tag_cliente.value > 0, cb_tag_cuenta.itemIndex >= 0,
	  Trim(txt_tag_observaciones.Text) <> ''],
	  CAPTION_CAMBIAR_TAG,
	  [MSG_CLIENTE,
	  MSG_CUENTA_CAMBIAR_TAG,
      MSG_OBSERVACION_CAMBIAR_TAG]) then Exit;

	// Guardamos la orden de servicio
	FOrdenservicio := GenerarOrdenServicioCambioTag(
    		DMConnections.BaseCAC, FCodigoComunicacion, PRI_NORMAL, Trim(txt_tag_observaciones.Text),
           	txt_tag_cliente.ValueInt, IVal(StrRight(cb_tag_cuenta.Text, 20)), DescriError);

	if OrdenServicio < 1 then begin
    	MsgBox( DescriError, MSG_CAMBIO_TAG_CAPTION, MB_ICONSTOP);
    	Exit;
    end;
	// Listo
	FResult := mrOk;
end;

procedure TFrameAltaOrdenServicio.TraerDimensionesVehiculo;
var LargoAux: integer;
begin
    //Para traer las dimensiones se necesita la marca, el modelo y el a�o
    //y con eso se va a buscar a la tabla Vehiculos
    if (cb_marca.itemindex >= 0) and
       (cb_modelo.itemindex >= 0) and
       (EsAnioCorrecto(txt_anio.ValueInt)) then begin
        LargoAux := ObtenerLargoVehiculo(DMConnections.BaseCAC, IVal(StrRight(cb_marca.Text, 20)),
                            IVal(StrRight(cb_modelo.Text, 20)), txt_anio.ValueInt);
        if LargoAux = 0 then txt_largoVehiculo.Clear
        else txt_largoVehiculo.ValueInt := LargoAux;
    end
    else txt_largoVehiculo.Clear;
end;

procedure TFrameAltaOrdenServicio.cb_modeloChange(Sender: TObject);
begin
    TraerDimensionesVehiculo;
    TraerDescripcionCategoria;    
end;

procedure TFrameAltaOrdenServicio.txt_anioChange(Sender: TObject);
begin
    TraerDimensionesVehiculo;
end;

procedure TFrameAltaOrdenServicio.chk_acopladoClick(Sender: TObject);
begin
    if chk_acoplado.Checked then begin
        txt_LargoAdicionalVehiculo.Enabled  := true;
        txt_LargoAdicionalVehiculo.Color    := clWindow;
    end else begin
        txt_LargoAdicionalVehiculo.Enabled  := false;
        txt_LargoAdicionalVehiculo.Color    := txt_largoVehiculo.Color;
    end;
end;

procedure TFrameAltaOrdenServicio.TraerDescripcionCategoria;
var
    CategoriaAux: integer;
begin
    if (cb_marca.itemindex >= 0) and
       (cb_modelo.itemindex >= 0) then begin
            txt_DescCategoria.text := BuscarDescripcionCategoriaVehiculo(DMConnections.BaseCAC, IVal(StrRight(cb_marca.text, 20)),
                                          IVal(StrRight(cb_modelo.text, 20)), CategoriaAux);
            txt_CodigoCategoria.valueint := CategoriaAux;

    end else  begin

        txt_DescCategoria.Clear;
        txt_CodigoCategoria.Clear;
        
    end;
end;

procedure TFrameAltaOrdenServicio.cbTiposContactoChange(Sender: TObject);
begin
    {
    FTipoContacto := StrRight(cbTiposContacto.Text, 1)[1];

    case FTipoContacto  of
        TIPO_CONTACTO : btContacto.Dataset := qryContactos;
        TIPO_CLIENTE : btContacto.Dataset := qryClientes;
        TIPO_LEGAJO : btContacto.Dataset := qryLegajos;
    end;}
end;

function TFrameAltaOrdenServicio.btContactoProcess(Tabla: TDataSet;
  var Texto: String): Boolean;
begin
	Texto := (Tabla.FieldByName('ApellidoNombre').AsString) + Space(200) +  Tabla.FieldByName('Codigo').AsString;
	Result := True;
end;

procedure TFrameAltaOrdenServicio.btContactoSelect(Sender: TObject;
  Tabla: TDataSet);
begin
	bteContacto.Text := Trim(Tabla.FieldByName('ApellidoNombre').AsString) + Space(200) + Tabla.FieldByName('Codigo').AsString;
end;

procedure TFrameAltaOrdenServicio.btnBorrarClick(Sender: TObject);
begin
	bteContacto.Text        := '';
end;

procedure TFrameAltaOrdenServicio.IrAlInicio(Sender: TObject);
begin
	bteContacto.SelStart    := 0;
end;

function TFrameAltaOrdenServicio.InicializarValidacion(CodigoComunicacion,
  CategoriaOrdenServicio, TipoOrdenServicio, ContextMark: Integer; ContractSerialNumber: Double): Boolean;
var
    i: Integer;
begin
    Result := False;
    FCodigoComunicacion := CodigoComunicacion;
    try
        // Cargamos los datos del cliente o contacto
        QryCuentaCliente.Close;
        QryCuentaCliente.Parameters.ParamByName('ContextMark').Value := ContextMark;
        QryCuentaCliente.Parameters.ParamByName('ContractSerialNumber').Value := ContractSerialNumber;
        QryCuentaCliente.Open;

        FCodigoPersona := QryCuentaCliente.FieldByName('CodigoPersona').asInteger;
        txt_tag_cliente.Value := QryCuentaCliente.FieldByName('CodigoPersona').asInteger;
        CargarCuentas(DMConnections.BaseCAC, cb_tag_cuenta, FCodigoPersona);

        i := 0;
        while i <  cb_tag_cuenta.Items.Count do begin
            if IVal(StrRight(cb_tag_cuenta.Text, 20)) =
              QryCuentaCliente.FieldByName('CodigoCuenta').asInteger then
                break;
            inc(i);
        end;
        if i = cb_tag_cuenta.Items.Count then
            cb_tag_cuenta.itemIndex := 0
        else
            cb_tag_cuenta.itemIndex := i;

        // Cargamos el arbol ordenes de servicio
        CargarArbolOrdenesServicio(CategoriaOrdenServicio, 0);

        // Seleccionamos el Tab correspondiente
        PageControl.ActivePage := tab_tags;
        SeleccionarTreeNode(TipoOrdenServicio);
        result := True;
	except
    	On E: Exception do begin
	    	Exit;
		end;
    end;
end;

procedure TFrameAltaOrdenServicio.GuardarMalFuncionamiento;
resourcestring
    MSG_TAG_MAL_FUNCIONAMIENTO = 'Error generando la orden de servicio de mal funcionamiento.';
    CAPTION_TAG_MAL_FUNCIONAMIENTO = 'Mal funcionamiento del TAG';
    MSG_CUENTA_TAG_MAL_FUNCIONAMIENTO = 'Debe especificarse una cuenta.';
    MSG_OBSERVACION_TAG_MAL_FUNCIONAMIENTO = 'Debe especificarse una observaci�n.';
Var
    DescriError: AnsiString;
begin
    FResult := mrNone;

	// Validamos que hayan tipeado alg�n comentario
	if not ValidateControls(
	  [txt_tag_cliente, cb_tag_cuenta, txt_tag_observaciones],
	  [txt_tag_cliente.value > 0, cb_tag_cuenta.itemIndex >= 0,
	  Trim(txt_tag_observaciones.Text) <> ''],
	  CAPTION_TAG_MAL_FUNCIONAMIENTO,
	  [MSG_CLIENTE,
	  MSG_CUENTA_TAG_MAL_FUNCIONAMIENTO,
      MSG_OBSERVACION_TAG_MAL_FUNCIONAMIENTO]) then Exit;

	// Guardamos la orden de servicio
    FOrdenServicio := GenerarOrdenServicioValidacion(DMConnections.BaseCAC, 0, OS_MAL_FUNCIONAMIENTO_TAG, PRI_BAJA,
        Trim(txt_tag_observaciones.Text), trunc(txt_Tag_Cliente.value), IVal(StrRight(cb_tag_cuenta.Text, 20)),
         DescriError);
	if OrdenServicio < 1 then begin
    	MsgBox( DescriError, MSG_TAG_MAL_FUNCIONAMIENTO, MB_ICONSTOP);
    	Exit;
    end;

	// Listo
	FResult := mrOk;
end;

procedure TFrameAltaOrdenServicio.HabilitarCamposPersoneria(
  Personeria: Char);
ResourceString
    LBL_PERSONA_RAZON_SOCIAL        = 'Raz�n Social';
    LBL_PERSONA_NOMBRE_FANTASIA     = 'Nombre de Fantas�a:';
    LBL_PERSONA_SITUACION_IVA       = 'Situaci�n de IVA:';
    LBL_PERSONA_FECHA_CREACION      = 'Fecha de Creaci�n:';
    LBL_PERSONA_APELLIDO            = 'Apellido:';
    LBL_PERSONA_NOMBRE              = 'Nombre:';
    LBL_PERSONA_APELLIDO_MATERNO    = 'Apellido Materno:';
    LBL_PERSONA_CONTACTO_COMERCIAL  = 'Contacto Comercial:';
    LBL_PERSONA_SEXO                = 'Sexo:';
    LBL_PERSONA_FECHA_NACIMIENTO    = 'Fecha de Nacimiento:';

begin
    Case Personeria of
        PERSONERIA_JURIDICA:
            begin
                lblApellido.Caption         := LBL_PERSONA_RAZON_SOCIAL;
                lblNombre.Caption           := LBL_PERSONA_NOMBRE_FANTASIA;
                lblApellidoMaterno.Caption  := LBL_PERSONA_CONTACTO_COMERCIAL;
                lblSexo.Visible             := False;
                cb_Sexo.Visible              := False;
                lblFechaNacCreacion.Caption := LBL_PERSONA_FECHA_CREACION;

            end;
        PERSONERIA_FISICA:
            begin
                lblApellido.Caption         := LBL_PERSONA_APELLIDO;
                lblNombre.Caption           := LBL_PERSONA_NOMBRE;
                lblApellidoMaterno.Caption  := LBL_PERSONA_APELLIDO_MATERNO;
                lblSexo.Caption             := LBL_PERSONA_SEXO;
                lblSexo.Visible             := True;
                cb_Sexo.Visible              := True;
                lblFechaNacCreacion.Caption := LBL_PERSONA_FECHA_NACIMIENTO;
            end;
    end;
end;
procedure TFrameAltaOrdenServicio.cbPersoneriaChange(Sender: TObject);
begin
    HabilitarCamposPersoneria( StrRight(Trim(cbPersoneria.Text), 1)[1]);
end;

procedure TFrameAltaOrdenServicio.bteContactoButtonClick(Sender: TObject);
var
    f: TFormBuscaClientes;
begin
    Application.CreateForm(TFormBuscaClientes, f);
    if f.Inicializa('', '', '', '', '') and (f.ShowModal = mrOk) then begin
        if f.Persona.Personeria = PERSONERIA_FISICA then
            bteContacto.Text := ArmaNombreCompleto(f.Persona.Apellido, f.Persona.ApellidoMaterno,
                    f.Persona.Nombre) + Space(200) + IntToStr(f.Persona.CodigoPersona)
        else
            bteContacto.Text := ArmaRazonSocial(f.Persona.Apellido, f.Persona.Nombre) + Space(200) + IntToStr(f.Persona.CodigoPersona)

    end;
    f.Release;
end;

procedure TFrameAltaOrdenServicio.MarcarComoDomicilioEntrega(
  tipoOrigenDatos: integer);
begin
    if tipoOrigenDatos = TIPO_ORIGEN_DATO_PARTICULAR then
        RGDomicilioEntrega.ItemIndex := 0
    else
        if tipoOrigenDatos = TIPO_ORIGEN_DATO_COMERCIAL then
            RGDomicilioEntrega.ItemIndex := 1
        else
            RGDomicilioEntrega.ItemIndex := -1;
end;

procedure TFrameAltaOrdenServicio.PrepararMostrarDomicilio(
  descripcion: AnsiString; tipoOrigen: integer);
begin
    if tipoOrigen = TIPO_ORIGEN_DATO_PARTICULAR then begin
        if (descripcion = '') then
            BtnDomicilioParticularAccion.Caption := CAPTION_BOTON_DOMICILIO_ALTA
        else
            BtnDomicilioParticularAccion.Caption := CAPTION_BOTON_DOMICILIO_MODIFICAR;
        ChkEliminarDomicilioParticular.Enabled := (descripcion <> '') and (codigodomicilioparticular > 0);
        MostrarDomicilio (lblDomicilioParticular, descripcion);
    end
    else begin
        if (descripcion = '') then
            BtnDomicilioComercialAccion.Caption := CAPTION_BOTON_DOMICILIO_ALTA
        else
            BtnDomicilioComercialAccion.Caption := CAPTION_BOTON_DOMICILIO_MODIFICAR;
        ChkEliminarDomicilioComercial.Enabled := (descripcion <> '') and (codigodomiciliocomercial > 0);
        MostrarDomicilio (lblDomicilioComercial, descripcion);
    end;
end;

procedure TFrameAltaOrdenServicio.MostrarDomicilio(var L: TLabel;
  descripcion: AnsiString);
begin
    if descripcion = '' then begin
        l.Font.Style := [fsItalic];
        l.Caption := FALTA_INGRESAR_DOMICILIO;
    end
    else begin
        l.Font.Style := [];
        l.Caption := descripcion;
    end;
end;

procedure TFrameAltaOrdenServicio.AltaDomicilio(tipoOrigenDatos: integer;
  var datosDomicilio: TDatosDomicilio);
ResourceString
    MSG_AGREGAR_CAPTION = 'Agregar Domicilio';
    MSG_ERROR_AGREGAR   = 'No se ha podido agregar el domicilio';
var
    f: TFormEditarDomicilio;
begin
 {   LimpiarRegistroDomicilio(datosDomicilio, tipoOrigenDatos);
    Application.CreateForm(TFormEditarDomicilio, f);
    if f.Inicializar(tipoOrigenDatos, true) and (f.ShowModal = mrOK) then begin
        try
            with datosDomicilio do begin
                CodigoTipoDomicilio := f.TipoDomicilio;
                //lo utilizo para indicar que hay datos. Si tiene -1 es porque esta vac�o
                IndiceDomicilio := 1;
                CodigoCalle := f.CodigoCalle;
                CodigoTipoCalle := f.TipoCalle;
                NumeroCalle := f.Numero;
                Piso := f.Piso;
                Dpto := f.Depto;
                Detalle := f.Detalle;
                CodigoPostal := f.CodigoPostal;
                CodigoPais := f.Pais;
                CodigoRegion := f.Region;
                CodigoComuna := f.Comuna;
                CodigoCiudad := f.Ciudad;
                CalleDesnormalizada := f.DescripcionCalle;
                CodigoTipoEdificacion := f.TipoEdificacion;
                DomicilioEntrega := f.DomicilioEntrega;
                Normalizado := f.CodigoCalle > 0;
                CodigoCalleRelacionadaUno := f.CalleRelacionadaUno;
                NumeroCalleRelacionadaUno := f.NumeroCalleRelacionadaUno;
                CodigoCalleRelacionadaDos := f.CalleRelacionadaDos;
                NumeroCalleRelacionadaDos := f.NumeroCalleRelacionadaDos;
                Descripcion := f.DomicilioCompleto;
                if f.DomicilioEntrega then MarcarComoDomicilioEntrega(tipoOrigenDatos);
                PrepararMostrarDomicilio(Descripcion,tipoOrigenDatos);
            end;
        except
            On E: Exception do begin
                MsgBoxErr( MSG_ERROR_AGREGAR, E.message, MSG_AGREGAR_CAPTION, MB_ICONSTOP);
            end;
        end;
    end;
    f.Release;}
end;

procedure TFrameAltaOrdenServicio.ModificarDomicilio(
  tipoOrigenDatos: integer; var datosDomicilio: TDatosDomicilio);
ResourceString
    MSG_EDITAR_CAPTION = 'Modificar Domicilio';
    MSG_ERROR_EDITAR   = 'No se ha podido modificar el domicilio';
var
	f: TFormEditarDomicilio;
begin
    Application.CreateForm(TFormEditarDomicilio, f);
    {
    if f.Inicializar(
        datosDomicilio.CodigoDomicilio,
        tipoOrigenDatos,
        datosDomicilio.CodigoPais,
        datosDomicilio.CodigoRegion,
        datosDomicilio.CodigoComuna,
        datosDomicilio.CodigoCiudad,
        datosDomicilio.CodigoTipoCalle,
        datosDomicilio.CodigoCalle,
        datosDomicilio.CalleDesnormalizada,
        datosDomicilio.NumeroCalle,
        datosDomicilio.Piso,
        datosDomicilio.Dpto,
        datosDomicilio.Detalle,
        datosDomicilio.CodigoPostal,
        datosDomicilio.CodigoTipoEdificacion,
        EstaElegidoComoDomicilioEntrega(tipoOrigenDatos),
        datosDomicilio.CodigoCalleRelacionadaUno,
        datosDomicilio.NumeroCalleRelacionadaUno,
        datosDomicilio.CodigoCalleRelacionadaDos,
        datosDomicilio.NumeroCalleRelacionadaDos
        ) and (f.ShowModal = mrOK) then begin
        try
            with datosDomicilio do begin
                CodigoTipoDomicilio := f.TipoDomicilio;
                IndiceDomicilio := 1; //significa que hay datos
                CodigoCalle := f.CodigoCalle;
                CodigoTipoCalle := f.TipoCalle;
                NumeroCalle := f.Numero;
                Piso := f.Piso;
                Dpto := f.Depto;
                Detalle := f.Detalle;
                CodigoPostal := f.CodigoPostal;
                CodigoPais := f.Pais;
                CodigoRegion := f.Region;
                CodigoComuna := f.Comuna;
                CodigoCiudad := f.Ciudad;
                CalleDesnormalizada := f.DescripcionCalle;
                CodigoTipoEdificacion := f.TipoEdificacion;
                DomicilioEntrega := f.DomicilioEntrega;
                Normalizado := f.CodigoCalle > 0;
                CodigoCalleRelacionadaUno := f.CalleRelacionadaUno;
                NumeroCalleRelacionadaUno := f.NumeroCalleRelacionadaUno;
                CodigoCalleRelacionadaDos := f.CalleRelacionadaDos;
                NumeroCalleRelacionadaDos := f.NumeroCalleRelacionadaDos;
                Descripcion := f.DomicilioCompleto;
                if f.DomicilioEntrega then MarcarComoDomicilioEntrega(tipoOrigenDatos);
                PrepararMostrarDomicilio(Descripcion, tipoOrigenDatos);
            end;
        except
            On E: Exception do begin
                MsgBoxErr( MSG_ERROR_EDITAR, E.message, MSG_EDITAR_CAPTION, MB_ICONSTOP);
            end;
        end;
    end;
    }
    f.Release;
end;

function TFrameAltaOrdenServicio.EstaElegidoComoDomicilioEntrega(
  tipoOrigenDatos: integer): boolean;
begin
    if tipoOrigenDatos = TIPO_ORIGEN_DATO_PARTICULAR then
        result := RGDomicilioEntrega.ItemIndex = 0
    else
        if tipoOrigenDatos = TIPO_ORIGEN_DATO_COMERCIAL then
            result := RGDomicilioEntrega.ItemIndex = 1
        else
            result := false;
end;

procedure TFrameAltaOrdenServicio.BtnDomicilioParticularAccionClick(
  Sender: TObject);
begin
    if (BtnDomicilioParticularAccion.Caption = CAPTION_BOTON_DOMICILIO_ALTA) then begin
        //Alta
        AltaDomicilio(TIPO_ORIGEN_DATO_PARTICULAR, DomicilioParticular);
        //Si todav�a no eligi� ning�n domicilio para entrega, lo marco
        if (not EstaElegidoComoDomicilioEntrega(TIPO_ORIGEN_DATO_PARTICULAR)) and
           (not EstaElegidoComoDomicilioEntrega(TIPO_ORIGEN_DATO_COMERCIAL)) then
            MarcarComoDomicilioEntrega(TIPO_ORIGEN_DATO_PARTICULAR);
    end
    else begin
        //Modificacion
        ModificarDomicilio(TIPO_ORIGEN_DATO_PARTICULAR, DomicilioParticular);
    end;
end;

procedure TFrameAltaOrdenServicio.BtnDomicilioComercialAccionClick(
  Sender: TObject);
begin
    if (BtnDomicilioComercialAccion.Caption = CAPTION_BOTON_DOMICILIO_ALTA) then begin
        //Alta
        AltaDomicilio(TIPO_ORIGEN_DATO_COMERCIAL, DomicilioComercial);
        //Si todav�a no eligi� ning�n domicilio para entrega, lo marco
        if (not EstaElegidoComoDomicilioEntrega(TIPO_ORIGEN_DATO_PARTICULAR)) and
           (not EstaElegidoComoDomicilioEntrega(TIPO_ORIGEN_DATO_COMERCIAL)) then
            MarcarComoDomicilioEntrega(TIPO_ORIGEN_DATO_COMERCIAL);
    end
    else begin
        //Modificacion
        ModificarDomicilio(TIPO_ORIGEN_DATO_COMERCIAL, DomicilioComercial);
    end;
end;

procedure TFrameAltaOrdenServicio.ChkEliminarDomicilioComercialClick(
  Sender: TObject);
begin
    ClickCheckEliminar(chkEliminarDomicilioComercial,
                    EstaElegidoComoDomicilioEntrega(TIPO_ORIGEN_DATO_COMERCIAL));
end;

procedure TFrameAltaOrdenServicio.ChkEliminarDomicilioParticularClick(
  Sender: TObject);
begin
    ClickCheckEliminar(chkEliminarDomicilioParticular,
                    EstaElegidoComoDomicilioEntrega(TIPO_ORIGEN_DATO_PARTICULAR));
end;

end.
