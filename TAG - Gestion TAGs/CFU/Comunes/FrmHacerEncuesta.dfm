object FormHacerEncuesta: TFormHacerEncuesta
  Left = 214
  Top = 152
  BorderStyle = bsDialog
  Caption = 'Por favor responda la encuesta'
  ClientHeight = 453
  ClientWidth = 688
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object pnl_BotonesGeneral: TPanel
    Left = 0
    Top = 414
    Width = 688
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object btn_Terminar: TButton
      Left = 592
      Top = 7
      Width = 79
      Height = 26
      Caption = '&Terminar'
      Default = True
      TabOrder = 0
      OnClick = btn_TerminarClick
    end
  end
  object pnl_Preguntas: TScrollBox
    Left = 0
    Top = 0
    Width = 688
    Height = 414
    Align = alClient
    BevelInner = bvNone
    BevelOuter = bvNone
    TabOrder = 1
  end
  object ObtenerPreguntasEncuesta: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerPreguntasEncuesta;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoFuenteSolicitud'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 8
    Top = 384
  end
  object ObtenerOpcionesPreguntas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerOpcionesPreguntas;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPregunta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 40
    Top = 384
  end
  object GrabarResultadoEncuesta: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'GrabarResultadoEncuesta;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPregunta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Respuesta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@CodigoOpcionesPregunta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoSolicitudContacto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCuestionario'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 544
    Top = 416
  end
end
