{-----------------------------------------------------------------------------
 File Name: FrmAgregarCheque.pas
 Author:    lgisuk
 Date Created: 07/09/2006
 Language: ES-AR
 Description: Agrega un cheque a ser cobrado
-----------------------------------------------------------------------------}
unit FrmAgregarCheque;

interface

uses
  //Agregar Cheque
  DMConnection,
  UtilDB,
  PeaTypes,
  PeaProcs,
  Util,
  UtilProc,
  //Otros
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Validate, DateEdit, VariantComboBox, DmiCtrls, DB, ADODB,
  ExtCtrls, TimeEdit, Buttons;

type
  TfrmAgregarCheque = class(TForm)
    lblConcepto: TLabel;
    cbBanco: TVariantComboBox;
    btnCancelar: TButton;
    btnAceptar: TButton;
    lblObservaciones: TLabel;
    lblImporte: TLabel;
    ednumerocheque: TEdit;
    Bevel1: TBevel;
    lblImporteIVA: TLabel;
    lblTipoComprobante: TLabel;
    lblFecha: TLabel;
    edFecha: TDateEdit;
    edMonto: TNumericEdit;
    lblPagareTitulo: TLabel;
    EdTitular: TEdit;
    btnDatosClientePagare: TSpeedButton;
    EdRut: TEdit;
    procedure btnDatosClientePagareClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  	procedure btnAceptarClick(Sender: TObject);
  private
	{ Private declarations }
    FTitular : String;
    FRut : String;
  	FFechaMinimaCheque,
  	FFechamaximaCheque: TDate;
    function CargarBancos(Combo: TVariantComboBox): Boolean;
  public
	{ Public declarations }
  	function Inicializar (Titular, Rut: String; FechaMinimaCheque, FechamaximaCheque: TDate; ChequeTitular, ChequeRUT : string; ChequeCodigoBanco : Integer) : Boolean;
  end;

var
  FfrmAgregarCheque: TfrmAgregarCheque;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    lgisuk
  Date Created: 07/09/2006
  Description: Inicializacion de este formulario
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------
  Author:    lgisuk
  Date Created: 17/10/2006
  Description: Ahora al ingresar un nuevo cheque se carga con los datos del
               cheque anterior
-----------------------------------------------------------------------------}
function TfrmAgregarCheque.Inicializar (Titular, Rut : String; 	FechaMinimaCheque, FechaMaximaCheque: TDate; ChequeTitular, ChequeRUT : string; ChequeCodigoBanco : Integer) : Boolean;
Resourcestring
    MSG_INIT_ERROR = 'Error al inicializar el formulario';
    MSG_ERROR = 'Error';
begin
    Result := False;
    try
        FTitular := Titular;
        FRut := Rut;
        FFechaMinimaCheque := FechaMinimaCheque;
        FFechaMaximaCheque :=  FechamaximaCheque;
        CargarBancos(CBBanco);
        EdTitular.Text := ChequeTitular;
        EdRUT.Text := ChequeRUT;
        if ChequeCodigoBanco > 0 then CbBanco.ItemIndex := CbBanco.Items.IndexOfValue(ChequeCodigoBanco);
        Result := True;
    except
        on E : Exception do begin
            MsgBoxErr(MSG_INIT_ERROR, E.Message, MSG_ERROR, MB_ICONERROR);
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnDatosClientePagareClick
  Author:    lgisuk
  Date Created: 07/09/2006
  Description: Copia los valores de titular y rut
  Parameters: Sender: TObject
  Return Value:
-----------------------------------------------------------------------------}
procedure TfrmAgregarCheque.btnDatosClientePagareClick(Sender: TObject);
begin
  edTitular.Text := FTitular;
	edRut.Text     := FRut;
end;

{-----------------------------------------------------------------------------
  Procedure: CargarBancos
  Author:    lgisuk
  Date:      07/09/2006
  Description: Carga la lista de bancos
  Parameters: Sender: TObject
  Result:    Boolean
-----------------------------------------------------------------------------}
function TfrmAgregarCheque.CargarBancos(Combo: TVariantComboBox): Boolean;
var
    Qry:TADOQuery;
begin
    Result := True;

    Qry := TADOQuery.Create(nil);
    Qry.Connection := DMConnections.BaseCAC;
    (* Obtener los bancos que est�n habilitados para el Pago en Ventanilla. *)
    Qry.SQL.Add('SELECT CodigoBanco As Codigo, Descripcion As Descripcion'
        + ' From Bancos (nolock)'
		+ ' Where (Activo = 1) And (HabilitadoPagoVentanilla = 1)'
		+ ' Order By Descripcion');
    try
        try
            Qry.Open;
            if Qry.IsEmpty then begin
                Qry.Close;
                Exit;
            end;
            while not Qry.Eof do begin
                Combo.Items.Add(Qry.FieldByName('Descripcion').AsString, Qry.FieldByName('Codigo').AsInteger);
                Qry.Next;
            end;
            Combo.ItemIndex := 0;
            Combo.OnChange(Self);
        except
            Result := False;
        end;
    finally
        FreeAndNil(Qry);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnAceptarClick
  Author:    lgisuk
  Date Created: 07/09/2006
  Description:  Valido los datos ingresados
  Parameters: Sender: TObject
  Return Value:
-----------------------------------------------------------------------------}
procedure TfrmAgregarCheque.btnAceptarClick(Sender: TObject);
resourcestring
    MSG_ERROR_NOMBRE_TITULAR = 'Ingrese el nombre del titular';
    MSG_ERROR_DOCUMENTO = 'Debe ingresar el RUT';
    MSG_ERROR_RUT_INVALIDO = 'El RUT ingresado es inv�lido';
    MSG_ERROR_NUMERO_CHEQUE = 'Ingrese el n�mero de cheque';
    MSG_ERROR_NUMERO_CHEQUE_INVALIDO = 'El Numero de cheque ingresado es inv�lido';
    MSG_ERROR_FECHA_CHEQUE_INVALIDA = 'La fecha del cheque no es v�lida';
    MSG_ERROR_FECHA_CHEQUE_RANGO = 'La fecha del cheque debe estar entre el %s y el %s ';
    MSG_ERROR_MONTO = 'Ingrese el monto';
const
    STR_ERROR = 'Error';
begin

    //valido nombre titular
    if edtitular.text = '' then begin
        MsgBoxBalloon(MSG_ERROR_NOMBRE_TITULAR, STR_ERROR, MB_ICONSTOP, edtitular);
        edtitular.SetFocus;
        exit;
    end;

    //valido si rut completo
    if edrut.Text = '' then begin
        MsgBoxBalloon(MSG_ERROR_DOCUMENTO, STR_ERROR, MB_ICONSTOP, edrut);
        edrut.SetFocus;
        exit;
    end;

    //valido el rut 
    if not ValidarRUT(DMConnections.BaseCAC, trim(edrut.Text)) then begin
        MsgBoxBalloon(MSG_ERROR_RUT_INVALIDO, STR_ERROR, MB_ICONSTOP, edrut);
        edrut.SetFocus;
        exit;
    end;

    //valido que sea numerico
    try
       strtoint(ednumerocheque.text);
    except
        MsgBoxBalloon(MSG_ERROR_NUMERO_CHEQUE_INVALIDO, STR_ERROR, MB_ICONSTOP, ednumerocheque);
        ednumerocheque.SetFocus;
        exit;
    end;

    //valido el numero de cheque
    if ednumerocheque.text = '' then begin
        MsgBoxBalloon(MSG_ERROR_NUMERO_CHEQUE, STR_ERROR, MB_ICONSTOP, ednumerocheque);
        ednumerocheque.SetFocus;
        exit;
    end;

    //valido la fecha
    if not IsValidDate(DateTimeToStr(edfecha.Date)) then begin
        MsgBoxBalloon(MSG_ERROR_FECHA_CHEQUE_INVALIDA, STR_ERROR, MB_ICONSTOP, edfecha);
        edfecha.SetFocus;
        exit;
    end;

    //valido rango fecha
    if not ((edfecha.Date >= FFechaMinimaCheque) AND (edfecha.Date < FFechaMaximaCheque)) then begin
        MsgBoxBalloon(Format(MSG_ERROR_FECHA_CHEQUE_RANGO, [FormatDateTime('dd/mm/yyyy',FFechaMinimaCheque), FormatDateTime('dd/mm/yyyy',FFechaMaximaCheque-1)]), STR_ERROR, MB_ICONSTOP, edfecha);
        edfecha.SetFocus;
        exit;
    end;

    //valido el monto
    if edmonto.text = '' then begin
        MsgBoxBalloon(MSG_ERROR_MONTO, STR_ERROR, MB_ICONSTOP, edmonto);
        edmonto.SetFocus;
        exit;
    end;



		ModalResult := mrOK;
end;


{-----------------------------------------------------------------------------
  Function Name: btnCancelarClick
  Author:    lgisuk
  Date Created: 07/09/2006
  Description:
  Parameters: Sender: TObject
  Return Value:
-----------------------------------------------------------------------------}
procedure TfrmAgregarCheque.btnCancelarClick(Sender: TObject);
begin
	Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 07/09/2006
  Description:
  Parameters: Sender: TObject
  Return Value:
-----------------------------------------------------------------------------}
procedure TfrmAgregarCheque.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Action := caFree;
end;



end.
