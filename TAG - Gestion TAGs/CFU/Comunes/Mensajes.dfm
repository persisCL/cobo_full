object FormMensajes: TFormMensajes
  Left = 198
  Top = 309
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  ClientHeight = 188
  ClientWidth = 479
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lblComentario: TLabel
    Left = 5
    Top = 6
    Width = 59
    Height = 13
    Caption = 'Comentario'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsItalic]
    ParentFont = False
    Visible = False
  end
  object Mensaje: TRichEdit
    Left = 1
    Top = 19
    Width = 472
    Height = 138
    Cursor = crArrow
    TabStop = False
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 0
  end
  object btnAceptar: TButton
    Left = 392
    Top = 161
    Width = 75
    Height = 25
    Caption = '&Salir'
    Default = True
    TabOrder = 1
    OnClick = btnAceptarClick
  end
end
