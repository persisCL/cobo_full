unit FrmMontoApertura;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DmiCtrls, ExtCtrls, DPSControls, PeaProcs;

type
  TTipoOperacionCaja = (tocApertura, tocAdicion);
  TFormMontoApertura = class(TForm)
    btn_Aceptar: TDPSButton;
    btn_Omitir: TDPSButton;
    Bevel1: TBevel;
    txt_Monto: TNumericEdit;
    Label2: TLabel;
    lblMensaje: TLabel;
    procedure btn_AceptarClick(Sender: TObject);
    procedure btn_OmitirClick(Sender: TObject);
  private
    FNumeroTurno: Integer;

    { Private declarations }
  public
    { Public declarations }
    function inicializar(NumeroTurno: Integer; TipoOperacion: TTipoOperacionCaja): Boolean;
  end;

var
  FormMontoApertura: TFormMontoApertura;

implementation

uses DMConnection;

{$R *.dfm}

procedure TFormMontoApertura.btn_AceptarClick(Sender: TObject);
begin
    if not ActualizarMontoTurno(DMConnections.BaseCAC, FNumeroTurno, txt_Monto.Value) then
        Exit;

	ModalResult := mrOk;
end;

procedure TFormMontoApertura.btn_OmitirClick(Sender: TObject);
begin
	Close;
end;

function TFormMontoApertura.inicializar(NumeroTurno: Integer; TipoOperacion: TTipoOperacionCaja): Boolean;
resourcestring
    MSG_CAPTION_MONTO_INICIAL   = 'Monto Apertura de Caja';
    MSG_CAPTION_MONTO_ADICIONAR = 'Monto a adicionar a la Caja';
    MSG_LABEL_MONTO_INICIAL     = 'Ingrese el importe con el que efect�a la apertura de  Caja.';
    MSG_LABEL_MONTO_ADICIONAR   = 'Ingrese el importe que se adiciona a  Caja.';
begin
    FNumeroTurno := NumeroTurno;

    if (TipoOperacion = tocApertura) then begin
        Caption := MSG_CAPTION_MONTO_INICIAL;
        lblMensaje.Caption := MSG_LABEL_MONTO_INICIAL;
    end
    else begin  //tocAdicion
        Caption := MSG_CAPTION_MONTO_ADICIONAR;
        lblMensaje.Caption := MSG_LABEL_MONTO_ADICIONAR;
    end;

	Result := True;
end;

end.
