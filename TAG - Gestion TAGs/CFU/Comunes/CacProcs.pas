
unit CacProcs;
{-------------------------------------------------------------------------------
Firma       : SS_1419_NDR_20151130
Descripcion : El SP ActualizarDatosPersona tiene el nuevo parametro @CodigoUsuario
              para grabarlo en los campos de auditoria de la tabla Personas y en
              HistoricoPersonas
--------------------------------------------------------------------------------}
interface

Uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, Rstrings,
  StdCtrls, Db, DBTables, Util,  UtilProc, PeaTypes, DMComunicacion, ComCtrls, UtilDB,
  Variants, AdoDB, DMOperacionesComunicacion, PeaProcs;


type
    TDatosMedioComunicacion = Record
        Valor: AnsiString;
        Guardar: boolean;
    end;

    TDatosEmail = Record
        Sender: AnsiString;
        Subject: AnsiString;
        ReplyTo: AnsiString;
        Header: AnsiString;
        BodyHTML: AnsiString;
        BodyPLAIN: AnsiString;
        Fecha: TDateTime;
    end;

	TDatosAdhesion = Record
        //*** A guardar en OrdenesServicioAdhesion y Personas ***//
	    CodigoOrdenServicio	: integer;
        Persona             : TDatosPersonales;
		ContactoComercial   : AnsiString;

		// Datos del vehículo
   		TipoPatente     : AnsiString;
		Patente         : AnsiString;
		CodigoMarca     : integer;
        CodigoModelo    : integer;
        Categoria       : integer;
        AnioVehiculo    : integer;
        CodigoColor     : integer;
        TieneAcoplado   : Boolean;
        LargoAdicionalVehiculo: integer;
        DetallePasadas  : Boolean;
        TipoAdhesion    : integer;
        EnvioFacturas   : Boolean;

		// Datos del Pago
        TipoPago        : AnsiString;
		TipoDebito      : AnsiString;
        CodigoEntidad   : Integer;
        CuentaDebito    : AnsiString;

        //*** A guardar en Domicilios ***//
        //Domicilio Particular
        CodigoDomicilioParticular: integer;
        CodigoCalleParticular: integer;
        CodigoTipoCalleParticular: integer;
        NumeroCalleParticular: integer;
        PisoParticular: integer;
        DptoParticular: AnsiString;
        DetalleParticular: AnsiString;
        CodigoPostalParticular: AnsiString;
        CalleDesnormalizadaParticular: AnsiString;
        CodigoTipoEdificacionParticular: integer;
        CodigoCalleRelacionadaUnoParticular: integer;
        NumeroCalleRelacionadaUnoParticular: AnsiString;
        CodigoCalleRelacionadaDosParticular: integer;
        NumeroCalleRelacionadaDosParticular: AnsiString;
        NormalizadoParticular: boolean;

        //Domicilio Comercial
        CodigoDomicilioComercial: integer;
        CodigoCalleComercial: integer;
        CodigoTipoCalleComercial: integer;
        NumeroCalleComercial: integer;
        PisoComercial: integer;
        DptoComercial: AnsiString;
        DetalleComercial: AnsiString;
        CodigoPostalComercial: AnsiString;
        CalleDesnormalizadaComercial: AnsiString;
        CodigoTipoEdificacionComercial: integer;
        CodigoCalleRelacionadaUnoComercial: integer;
        NumeroCalleRelacionadaUnoComercial: integer;
        CodigoCalleRelacionadaDosComercial: integer;
        NumeroCalleRelacionadaDosComercial: integer;
        NormalizadoComercial: boolean;

        //*** A guardar en MediosComunicacion ***//
        TelefonoParticular: TDatosMedioComunicacion;
        TelefonoComercial: TDatosMedioComunicacion;
        TelefonoMovilParticular: TDatosMedioComunicacion;
        EmailParticular: TDatosMedioComunicacion;
    end;

function GenerarOrdenServicioAltaConvenio(Connection: TADOConnection; CodigoComunicacion, CodigoWorkflow,
  CodigoTipoOrdenServicio: integer; Prioridad: Integer; Estado: Char;
  Observaciones: AnsiString; Compromiso: AnsiString; FechaCompromiso: TDateTime;
  CodigoConvenio: integer; var DescriError: AnsiString): Integer;

function GenerarOrdenServicioModificacionConvenio(Connection: TADOConnection; CodigoComunicacion, CodigoWorkflow,
  CodigoTipoOrdenServicio: integer; Prioridad: Integer; Estado: Char;
  Observaciones: AnsiString; Compromiso: AnsiString; FechaCompromiso: TDateTime;
  CodigoConvenio: integer; var DescriError: AnsiString): Integer;

function ActualizarOperacionComunicacion(CodigoComunicacion: LongInt; MotivoContacto: integer; Comentarios: TStringList): boolean;

function ActualizarDatosPersona(
    Conn: TADOConnection;
    Personeria: AnsiString;
	Apellido,
    ApellidoMaterno,
    Nombre,
    CodigoDocumento,
    NumeroDocumento: AnsiString;
    Sexo: AnsiString;
    LugarNacimiento: AnsiString;
    FechaNacimiento: tDateTime;
    Actividad: integer;
    ContactoComercial: AnsiString;
    CodigoSituacionIVA: AnsiString;
    CodigoActividad: integer;
    Estado: AnsiString;
    DomicilioEntrega: integer;
    var PIN: AnsiString;
	var CodigoPersona: integer;
    MsgError: Pchar): boolean;

function ActualizarDatosLugarDeVenta(
    Conn: TADOConnection;
	CodigoPersona: integer;
    Activo: Boolean;
    CodigoOperadorLogistico: integer;
    MsgError: Pchar): boolean;

function ActualizarDatosProveedor(
    Conn: TADOConnection;
	CodigoPersona: integer;
	CUIT: AnsiString;
    ProveedorTAG: Boolean;
    Activo: Boolean;
    MsgError: Pchar): boolean;

function ActualizarDatosCliente(
    Conn: TADOConnection;
    Estado: AnsiString;
	CodigoPersona: integer;
    MsgError: Pchar): boolean;

function ActualizarDatosMaestroPersonal(
    Conn: TADOConnection;
    CodigoCategoriaEmpleado: integer;
    ListaNegra: Boolean;
    TarjetasEmitidas,
    TarjetasGrabadas,
    TarjetasDestruidas,
    CodigoTipoSueldo: integer;
    SueldoBruto, SueldoNeto: Double;
    CodigoUsuario: AnsiString;
    Activo: Boolean;
    CodigoPersona: integer;
    MsgError: Pchar): boolean;

function ActualizarDatosDomicilio(
    Conn: TADOConnection;
    CodigoPersona: integer;
    CodigoTipoDomicilio: integer;
    CodigoPais, CodigoRegion, CodigoComuna: AnsiString;
    Calle, Numero, Detalle: AnsiString;
    CodigoPostal: AnsiString;
	var CodigoDomicilio: integer;
    MsgError: Pchar): boolean;

function ActualizarMedioComunicacion(
    Conn: TADOConnection;
    CodigoPersona: Integer;
    CodigoMedioContacto: Integer;
    CodigoTipoOrigenDato: Integer;
    Valor: AnsiString;
    HorarioDesde, HorarioHasta: TDateTime;
    Observaciones: AnsiString;
    MsgError: Pchar): boolean;


implementation

Uses
	DMConnection;

Function CrearOrdenServicio(Connection: TADOConnection; CodigoComunicacion, CodigoWorkflow,
  CodigoTipoOrdenServicio: smallint;
  Prioridad: Integer; Estado: Char;
  Observaciones: AnsiString;
  Compromiso: AnsiString;
  FechaCompromiso: TDateTime; var DescriError: AnsiString): Integer;
resourcestring
    MSG_ORDEN_SERVICIO      = 'No se pudo crear la Orden de Servicio.';
    CAPTION_ORDEN_SERVICIO  = 'Crear Orden de Servicio';
Var
	SP: TADOStoredProc;
begin
	SP := TADOStoredProc.Create(nil);
    try
        try
            SP.ProcedureName := 'CrearOrdenServicio';
            SP.Connection := Connection;
            //*** Creo los parametros
            SP.Parameters.CreateParameter('@CodigoComunicacion', ftInteger, pdInput, 0, NULL);
            SP.Parameters.CreateParameter('@CodigoWorkflow', ftInteger, pdInput, 0, NULL);
            SP.Parameters.CreateParameter('@Prioridad', ftInteger, pdInput, 0, NULL);
            SP.Parameters.CreateParameter('@Estado', ftString, pdInput, 0, NULL);
            SP.Parameters.CreateParameter('@Compromiso', ftMemo, pdInput, 0, NULL);
            SP.Parameters.CreateParameter('@FechaCompromiso', ftDateTime, pdInput, 0, NULL);
            SP.Parameters.CreateParameter('@Observaciones', ftMemo, pdInput, 0, NULL);
            SP.Parameters.CreateParameter('@CodigoTipoOrdenServicio', ftInteger, pdInput, 0, NULL);
            SP.Parameters.CreateParameter('@CodigoOrdenServicio', ftInteger, pdInputOutput, 0, NULL);
            //*** Asigno los valores
            SP.Parameters.ParamByName('@CodigoComunicacion').Value := iif(CodigoComunicacion <= 0, null, CodigoComunicacion);
            SP.Parameters.ParamByName('@CodigoWorkflow').Value := iif(CodigoWorkflow <= 0, null, CodigoWorkflow);
            SP.Parameters.ParamByName('@Prioridad').Value := Prioridad;
            SP.Parameters.ParamByName('@Estado').Value := iif(Trim(Estado) = '', NULL, Estado);
            SP.Parameters.ParamByName('@Compromiso').Value := Compromiso;
            if FechaCompromiso <= 0 then SP.Parameters.ParamByName('@FechaCompromiso').Value := NULL
              else SP.Parameters.ParamByName('@FechaCompromiso').Value := FechaCompromiso;
            if Trim(Observaciones) = '' then SP.Parameters.ParamByName('@Observaciones').Value := NULL
              else SP.Parameters.ParamByName('@Observaciones').Value := Observaciones;
            SP.Parameters.ParamByName('@CodigoTipoOrdenServicio').Value := iif(CodigoTipoOrdenServicio <= 0, null, CodigoTipoOrdenServicio);
            SP.ExecProc;
            Result := SP.Parameters.ParamByName('@CodigoOrdenServicio').Value;
        except
            on e: exception do begin
                DescriError := e.Message;
                Result := 0;
            end;
        end;
    finally
        SP.Free;
    end;
end;

function PasarCodigoAEstadoSolicitud(Codigo: AnsiString): TEstadoSolicitud;
begin
	for Result := Low(TEstadoSolicitud) to High(TEstadoSolicitud) do begin
		if DescripcionEstado[Result].CodigoEstado = Codigo then Exit;
	end;
	Result := esIncorrecto;
end;

function PasarCodigoADescripcionEstado(Codigo: AnsiString): AnsiString;
begin
	Result := DescripcionEstado[PasarCodigoAEstadoSolicitud(Codigo)].Descripcion;
end;

function ActualizarDatosPersona(
    Conn: TADOConnection;
    Personeria: AnsiString;
	Apellido,
    ApellidoMaterno,
    Nombre,
    CodigoDocumento,
    NumeroDocumento: AnsiString;
    Sexo: AnsiString;
    LugarNacimiento: AnsiString;
    FechaNacimiento: tDateTime;
    Actividad: integer;
    ContactoComercial: AnsiString;
    CodigoSituacionIVA: AnsiString;
    CodigoActividad: integer;
    Estado: AnsiString;
    DomicilioEntrega: integer;
    var PIN: AnsiString;
	var CodigoPersona: integer;
    MsgError: Pchar): boolean;

resourcestring
    MSG_ACTUALIZAR_PERSONA     = 'No se pudieron actualizar los datos de la Persona.';
    CAPTION_ACTUALIZAR_PERSONA = 'Actualizar Datos de la Persona';

var
    SP: TADOStoredProc;
begin
	Result := False;
    SP := TADOStoredProc.Create(nil);
    try
        SP.Connection       := Conn;
        SP.ProcedureName    := 'ActualizarDatosPersona';
        with SP.Parameters do begin
            CreateParameter('@Personeria', ftString, pdInput, 1, iif( Trim(Personeria) = '', null, Trim(Personeria)));
            CreateParameter('@Apellido', ftString, pdInput, 30, iif( Trim(Apellido) = '', null, Trim(Apellido)));
            CreateParameter('@ApellidoMaterno', ftString, pdInput, 30, iif( Trim(ApellidoMaterno) = '', null, Trim(ApellidoMaterno)));
            CreateParameter('@Nombre', ftString, pdInput, 30, iif( Trim(Nombre) = '', null, Trim(Nombre)));
            CreateParameter('@CodigoDocumento', ftString, pdInput, 4, iif( Trim(CodigoDocumento) = '', null, Trim(CodigoDocumento)));
            CreateParameter('@NumeroDocumento', ftString, pdInput, 11, iif( Trim(NumeroDocumento) = '', null, Trim(NumeroDocumento)));
            CreateParameter('@Sexo', ftString, pdInput, 1, iif( Trim(Sexo) = '', null, Trim(Sexo)));
            CreateParameter('@LugarNacimiento', ftString, pdInput, 3, iif( Trim(LugarNacimiento) = '', null, Trim(LugarNacimiento)));
            CreateParameter('@FechaNacimiento', ftDateTime, pdInput, 0, iif(FechaNacimiento < 0, null, FechaNacimiento));
            CreateParameter('@CodigoActividad', ftInteger, pdInput, 0, iif(CodigoActividad < 0, null, CodigoActividad));
            CreateParameter('@ContactoComercial', ftString, pdInput, 50, iif( Trim(ContactoComercial) = '', null, Trim(ContactoComercial)));
            CreateParameter('@CodigoSituacionIVA', ftString, pdInput, 2, iif( Trim(CodigoSituacionIVA) = '', null, Trim(CodigoSituacionIVA)));
            CreateParameter('@Estado', ftString, pdInput, 1, iif( Trim(Estado) = '', null, Trim(Estado)));
            CreateParameter('@CodigoDomicilioEntrega', ftInteger, pdInput, 0, iif(DomicilioEntrega < 0, null, DomicilioEntrega));
            CreateParameter('@PIN', ftString, pdInputOutput, 4, iif( Trim(PIN) = '', null, Trim(PIN)));
            CreateParameter('@CodigoPersona', ftInteger, pdInputOutput, 0, iif(CodigoPersona < 0, null, CodigoPersona));
            CreateParameter('@TipoCliente', ftInteger, pdInput, 0, Null);
            CreateParameter('@CodigoUsuario',ftString,pdInput,20,UsuarioSistema);                              //SS_1419_NDR_20151130
        end;
        try
            SP.ExecProc;
            PIN := Trim(SP.Parameters.ParamByName('@PIN').Value);
            CodigoPersona := SP.Parameters.ParamByName('@CodigoPersona').Value;
            SP.Close;
            Result := True;
        except
            On E: Exception do begin
                StrPCopy(MsgError, e.Message);
            end;
        end;
    finally
        SP.Free;
    end;
end;

function ActualizarDatosCliente(
    Conn: TADOConnection;
    Estado: AnsiString;
	CodigoPersona: integer;
    MsgError: Pchar): boolean;

resourcestring
    MSG_ACTUALIZAR_PERSONA     = 'No se pudieron actualizar los datos del Cliente.';
    CAPTION_ACTUALIZAR_PERSONA = 'Actualizar Datos del Cliente';

var
    SP: TADOStoredProc;
begin
	Result := False;
    SP := TADOStoredProc.Create(nil);
    try
        SP.Connection       := Conn;
        SP.ProcedureName    := 'ActualizarDatosCliente';
        with SP.Parameters do begin
            CreateParameter('@CodigoPersona', ftInteger, pdInput, 0, iif(CodigoPersona < 0, null, CodigoPersona));
            CreateParameter('@Estado', ftString, pdInput, 1, iif( Trim(Estado) = '', null, Trim(Estado)));
        end;
        try
            SP.ExecProc;
            SP.Close;
            Result := True;
        except
            On E: Exception do begin
                StrPCopy(MsgError, e.Message);
            end;
        end;
    finally
        SP.Free;
    end;
end;

function ActualizarDatosProveedor(
    Conn: TADOConnection;
	CodigoPersona: integer;
	CUIT: AnsiString;
    ProveedorTAG: Boolean;
    Activo: Boolean;
    MsgError: Pchar): boolean;

resourcestring
    MSG_ACTUALIZAR_PERSONA     = 'No se pudieron actualizar los datos del Proveedor.';
    CAPTION_ACTUALIZAR_PERSONA = 'Actualizar Datos del Proveedor';

var
    SP: TADOStoredProc;
begin
	Result := False;
    SP := TADOStoredProc.Create(nil);
    try
        SP.Connection       := Conn;
        SP.ProcedureName    := 'ActualizarDatosProveedor';
        with SP.Parameters do begin
            CreateParameter('@CodigoPersona', ftInteger, pdInput, 0, iif(CodigoPersona < 0, null, CodigoPersona));
            CreateParameter('@CUIT', ftString, pdInput, 12, iif( Trim(CUIT) = '', null, Trim(CUIT)));
            CreateParameter('@Activo', ftBoolean, pdInput, 1, Activo);
            CreateParameter('@ProveedorTAG', ftBoolean, pdInput, 1, ProveedorTAG);
        end;
        try
            SP.ExecProc;
            SP.Close;
            Result := True;
        except
            On E: Exception do begin
                StrPCopy(MsgError, e.Message);
            end;
        end;
    finally
        SP.Free;
    end;
end;

function ActualizarDatosLugarDeVenta(
    Conn: TADOConnection;
	CodigoPersona: integer;
    Activo: Boolean;
    CodigoOperadorLogistico: integer;
    MsgError: Pchar): boolean;

resourcestring
    MSG_ACTUALIZAR_PERSONA     = 'No se pudieron actualizar los datos del Lugar de Venta.';
    CAPTION_ACTUALIZAR_PERSONA = 'Actualizar Datos del Lugar de Venta';

var
    SP: TADOStoredProc;
begin
	Result := False;
    SP := TADOStoredProc.Create(nil);
    try
        SP.Connection       := Conn;
        SP.ProcedureName    := 'ActualizarDatosLugarDeVenta';
        with SP.Parameters do begin
            CreateParameter('@CodigoOperadorLogistico', ftInteger, pdInput, 0, iif(CodigoOperadorLogistico < 0, null, CodigoOperadorLogistico));
            CreateParameter('@CodigoPersona', ftInteger, pdInput, 0, iif(CodigoPersona < 0, null, CodigoPersona));
            CreateParameter('@Activo', ftBoolean, pdInput, 1, Activo);
        end;
        try
            SP.ExecProc;
            SP.Close;
            Result := True;
        except
            On E: Exception do begin
                StrPCopy(MsgError, e.Message);
            end;
        end;
    finally
        SP.Free;
    end;
end;


function ActualizarDatosMaestroPersonal(
    Conn: TADOConnection;
    CodigoCategoriaEmpleado: integer;
    ListaNegra: Boolean;
    TarjetasEmitidas,
    TarjetasGrabadas,
    TarjetasDestruidas,
    CodigoTipoSueldo: integer;
    SueldoBruto, SueldoNeto: Double;
    CodigoUsuario: AnsiString;
    Activo: Boolean;
    CodigoPersona: integer;
    MsgError: Pchar): boolean;

resourcestring
    MSG_ACTUALIZAR_PERSONA     = 'No se pudieron actualizar los datos del Personal.';
    CAPTION_ACTUALIZAR_PERSONA = 'Actualizar Datos del Personal';

var
    SP: TADOStoredProc;
begin
	Result := False;
    SP := TADOStoredProc.Create(nil);
    try
        SP.Connection       := Conn;
        SP.ProcedureName    := 'ActualizarDatosMaestroPersonal';
        with SP.Parameters do begin
            CreateParameter('@CodigoCategoriaEmpleado', ftSmallint	, pdInput, 0, CodigoCategoriaEmpleado);
            CreateParameter('@ListaNegra', ftBoolean, pdInput, 1, Activo);
            CreateParameter('@TarjetasEmitidas', ftInteger, pdInput, 0, TarjetasEmitidas);
            CreateParameter('@TarjetasGrabadas', ftInteger, pdInput, 0, TarjetasGrabadas);
            CreateParameter('@TarjetasDestruidas', ftInteger, pdInput, 0, TarjetasDestruidas);
            CreateParameter('@CodigoTipoSueldo', ftSmallint	, pdInput, 0, CodigoTipoSueldo);
            CreateParameter('@SueldoBruto', ftInteger, pdInput, 0, SueldoBruto * 100);
            CreateParameter('@SueldoNeto', ftInteger, pdInput, 0, SueldoNeto * 100);
            CreateParameter('@CodigoUsuario', ftString, pdInput, 20, iif( Trim(CodigoUsuario) = '', null, CodigoUsuario));
            CreateParameter('@Activo', ftBoolean, pdInput, 1, Activo);
            CreateParameter('@CodigoPersona', ftInteger, pdInput, 0, iif(CodigoPersona < 0, null, CodigoPersona));
        end;

        try
            SP.ExecProc;
            SP.Close;
            Result := True;
        except
            On E: Exception do begin
                StrPCopy(MsgError, e.Message);
            end;
        end;
    finally
        SP.Free;
    end;
end;

function ActualizarDatosDomicilio(
    Conn: TADOConnection;
    CodigoPersona: integer;
    CodigoTipoDomicilio: integer;
    CodigoPais, CodigoRegion, CodigoComuna: AnsiString;
    Calle, Numero, Detalle: AnsiString;
    CodigoPostal: AnsiString;
	var CodigoDomicilio: integer;
    MsgError: Pchar): boolean;

resourcestring
    MSG_ACTUALIZAR_PERSONA     = 'No se pudieron actualizar los datos del Domicilio.';
    CAPTION_ACTUALIZAR_PERSONA = 'Actualizar Datos del Domicilio';

var
    SP: TADOStoredProc;
begin
	Result := False;
    SP := TADOStoredProc.Create(nil);
    try
        SP.Connection       := Conn;
        SP.ProcedureName    := 'ActualizarDomicilio';
        with SP.Parameters do begin
            CreateParameter('@CodigoPersona', ftInteger, pdInput, 0, iif(CodigoPersona < 0, null, CodigoPersona));
            CreateParameter('@CodigoTipoDomicilio', ftSmallint, pdInput, 0, iif(CodigoTipoDomicilio < 0, null, CodigoTipoDomicilio));
            CreateParameter('@CodigoPais', ftString, pdInput, 3, iif( Trim(CodigoPais) = '', null, CodigoPais));
            CreateParameter('@CodigoRegion', ftString, pdInput, 3, iif( Trim(CodigoRegion) = '', null, CodigoRegion));
            CreateParameter('@CodigoComuna', ftString, pdInput, 3, iif( Trim(CodigoComuna) = '', null, CodigoComuna));
            CreateParameter('@Calle', ftString, pdInput, 50, iif( Trim(Calle) = '', null, Calle));
            CreateParameter('@Numero', ftString, pdInput, 10, iif( Trim(Numero) = '', null, Numero));
            CreateParameter('@Detalle', ftString, pdInput, 50, iif( Trim(Detalle) = '', null, Detalle));
            CreateParameter('@CodigoPostal', ftString, pdInput, 10, iif( Trim(CodigoPostal) = '', null, CodigoPostal));
            CreateParameter('@CodigoDomicilio', ftInteger, pdInputOutput, 0, iif(CodigoDomicilio < 0, null, CodigoDomicilio));
        end;

        try
            SP.ExecProc;
            CodigoDomicilio := SP.Parameters.ParamByName('@CodigoDomicilio').Value;
            SP.Close;
            Result := True;
        except
            On E: Exception do begin
                StrPCopy(MsgError, e.Message);
            end;
        end;
    finally
        SP.Free;
    end;
end;

function ActualizarMedioComunicacion(
    Conn: TADOConnection;
    CodigoPersona: Integer;
    CodigoMedioContacto: Integer;
    CodigoTipoOrigenDato: Integer;
    Valor: AnsiString;
    HorarioDesde, HorarioHasta: TDateTime;
    Observaciones: AnsiString;
    MsgError: Pchar): boolean;

resourcestring
    MSG_ACTUALIZAR_PERSONA     = 'No se pudieron actualizar los datos del Domicilio.';
    CAPTION_ACTUALIZAR_PERSONA = 'Actualizar Datos del Domicilio';

var
    SP: TADOStoredProc;
begin
	Result := False;
    SP := TADOStoredProc.Create(nil);
    try
        SP.Connection       := Conn;
        SP.ProcedureName    := 'ActualizarDomicilio';
        with SP.Parameters do begin
            CreateParameter('@CodigoPersona', ftInteger, pdInput, 0, iif(CodigoPersona < 0, null, CodigoPersona));
            CreateParameter('@CodigoMedioContacto', ftSmallint, pdInput, 0, iif(CodigoMedioContacto < 0, null, CodigoMedioContacto));
            CreateParameter('@CodigoTipoOrigenDato', ftSmallint, pdInput, 0, iif(CodigoTipoOrigenDato < 0, null, CodigoTipoOrigenDato));
            CreateParameter('@Valor', ftString, pdInput, 255, iif( Trim(Valor) = '', null, Trim(Valor)));
            CreateParameter('@HorarioDesde', ftDatetime, pdInput, 0, Frac(HorarioDesde));
            CreateParameter('@HorarioHasta', ftDatetime, pdInput, 0, Frac(HorarioHasta));
            CreateParameter('@Observaciones', ftString, pdInput, 100, iif( Trim(Observaciones) = '', null, Trim(Observaciones)));
        end;

        try
            SP.ExecProc;
            SP.Close;
            Result := True;
        except
            On E: Exception do begin
                StrPCopy(MsgError, e.Message);
            end;
        end;
    finally
        SP.Free;
    end;
end;



function ActualizarOperacionComunicacion(CodigoComunicacion: LongInt;
	MotivoContacto: integer; Comentarios: TStringList): boolean;
resourcestring
    MSG_MOTIVO_COMUNICACION     = 'No se pudo crear un nuevo Motivo de Comunicación.';
    CAPTION_MOTIVO_COMUNICACION = 'Crear Motivo de Comunicación';
var
	DM: TDMOperacionesComunicaciones;
begin
	Result := False;
   	DM := TDMOperacionesComunicaciones.Create(nil);
    try
        try
            //Veo Como laburo las Observaciones.
            if Assigned(Comentarios)then begin
                DM.OperacionesComunicacion.Active := True;
                DM.OperacionesComunicacion.Append;
                DM.OperacionesComunicacion.FieldByName('CodigoComunicacion').AsInteger	:=  CodigoComunicacion;
                DM.OperacionesComunicacion.FieldByName('CodigoMotivo').AsInteger 		:=  MotivoContacto;
                TMemo(DM.OperacionesComunicacion.FieldByName('Comentarios')).Assign(Comentarios);
                DM.OperacionesComunicacion.Post;
                DM.OperacionesComunicacion.Close;
            end;
            Result := True;
        except
            on E: Exception do begin
                MsgBoxErr(MSG_MOTIVO_COMUNICACION, e.Message, CAPTION_MOTIVO_COMUNICACION, MB_ICONSTOP);
                Dm.OperacionesComunicacion.Close;
            end;
        end;
    finally
        DM.Free;
    end;
end;

function GenerarOrdenServicioAltaConvenio(Connection: TADOConnection; CodigoComunicacion, CodigoWorkflow,
  CodigoTipoOrdenServicio: integer; Prioridad: Integer; Estado: Char;
  Observaciones: AnsiString; Compromiso: AnsiString; FechaCompromiso: TDateTime;
  CodigoConvenio: integer; var DescriError: AnsiString): Integer;
var
	SP: TADOStoredProc;
	FOrdenServicio: integer;
begin
	Result := -1;
	// Guardamos la orden de servicio
	FOrdenServicio := CrearOrdenServicio(Connection, CodigoComunicacion, CodigoWorkflow, 1,
    Prioridad, ESTADO_TAREA_TERMINADA, Observaciones, '', nulldate, DescriError);

	if FOrdenServicio = 0 then Exit;

	SP := TADOStoredProc.Create(nil);
    try
        try
            SP.Connection := Connection;
            SP.ProcedureName := 'CrearOrdenServicioAltaConvenio';
            SP.Parameters.CreateParameter('@CodigoOrdenServicio', ftInteger, pdInput, 0, FOrdenServicio);
            SP.Parameters.CreateParameter('@CodigoConvenio', ftInteger, pdInput, 0, CodigoConvenio);
            SP.ExecProc;
            Result := FOrdenServicio;
        except
            on e: exception do begin
            	DescriError := e.Message;
                Result := 0;
            end;
        end;
    finally
    	SP.Close;
        SP.Free;
    end;
	// No tiene Workflow
end;

function GenerarOrdenServicioModificacionConvenio(Connection: TADOConnection; CodigoComunicacion, CodigoWorkflow,
  CodigoTipoOrdenServicio: integer; Prioridad: Integer; Estado: Char;
  Observaciones: AnsiString; Compromiso: AnsiString; FechaCompromiso: TDateTime;
  CodigoConvenio: integer; var DescriError: AnsiString): Integer;
var
	SP: TADOStoredProc;
	FOrdenServicio: integer;
begin
	Result := -1;
	// Guardamos la orden de servicio
	FOrdenServicio := CrearOrdenServicio(Connection, CodigoComunicacion, CodigoWorkflow, CodigoTipoOrdenServicio,
                                Prioridad, Estado, Observaciones, '', nulldate, DescriError);
	if FOrdenServicio = 0 then Exit;

	SP := TADOStoredProc.Create(nil);
    try
        try
            SP.Connection := Connection;
            SP.ProcedureName := 'CrearOrdenServicioModificacionConvenio';
            SP.Parameters.CreateParameter('@CodigoOrdenServicio', ftInteger, pdInput, 0, FOrdenServicio);
            SP.Parameters.CreateParameter('@CodigoConvenio', ftInteger, pdInput, 0, CodigoConvenio);
            SP.ExecProc;
            Result := FOrdenServicio;
        except
            on e: exception do begin
            	DescriError := e.Message;
                Result := 0;
            end;
        end;
    finally
    	SP.Close;
        SP.Free;
    end;
	// No tiene Workflow
end;


end.

