object FormMantenimientoCuenta: TFormMantenimientoCuenta
  Left = 128
  Top = 156
  BorderStyle = bsDialog
  Caption = 'Alta de Cuenta'
  ClientHeight = 505
  ClientWidth = 690
  Color = clBtnFace
  Constraints.MinHeight = 505
  Constraints.MinWidth = 690
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl: TPageControl
    Left = 2
    Top = 3
    Width = 688
    Height = 461
    ActivePage = tab_cliente
    TabOrder = 0
    object tab_cliente: TTabSheet
      Caption = 'Datos del &Cliente'
      DesignSize = (
        680
        433)
      object lblNombre: TLabel
        Left = 4
        Top = 90
        Width = 117
        Height = 13
        Caption = 'Nombre de fantas'#237'a:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblApellido: TLabel
        Left = 4
        Top = 64
        Width = 80
        Height = 13
        Caption = 'Raz'#243'n Social:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label17: TLabel
        Left = 4
        Top = 139
        Width = 47
        Height = 13
        Caption = 'Actividad:'
      end
      object lblFechaNacCreacion: TLabel
        Left = 361
        Top = 115
        Width = 89
        Height = 13
        Caption = 'Fecha Nacimiento:'
      end
      object lblSexo: TLabel
        Left = 361
        Top = 40
        Width = 33
        Height = 13
        Caption = 'Sexo:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = 4
        Top = 39
        Width = 69
        Height = 13
        Caption = 'Documento:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label8: TLabel
        Left = 4
        Top = 117
        Width = 31
        Height = 13
        Caption = 'Pa'#237's:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblApellidoMaterno: TLabel
        Left = 361
        Top = 64
        Width = 82
        Height = 13
        Caption = 'Apellido Materno:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
      end
      object Label53: TLabel
        Left = 4
        Top = 15
        Width = 67
        Height = 13
        Caption = 'Personer'#237'a:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label12: TLabel
        Left = 361
        Top = 90
        Width = 82
        Height = 13
        Caption = 'Situaci'#243'n de IVA:'
      end
      object txt_nombre: TEdit
        Left = 123
        Top = 84
        Width = 229
        Height = 21
        Color = 16444382
        TabOrder = 3
      end
      object txt_apellido: TEdit
        Left = 123
        Top = 60
        Width = 229
        Height = 21
        Color = 16444382
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
      end
      object txt_fechanacimiento: TDateEdit
        Left = 470
        Top = 109
        Width = 103
        Height = 21
        AutoSelect = False
        TabOrder = 9
        Date = -693594
      end
      object cb_sexo: TComboBox
        Left = 470
        Top = 35
        Width = 103
        Height = 21
        Style = csDropDownList
        Color = 16444382
        ItemHeight = 13
        ItemIndex = 0
        TabOrder = 6
        Text = 'Masculino'
        Items.Strings = (
          'Masculino'
          'Femenino')
      end
      object cbTipoNumeroDocumento: TMaskCombo
        Left = 123
        Top = 35
        Width = 229
        Height = 21
        OnChange = cbTipoNumeroDocumentoChange
        Items = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Color = 16444382
        ComboWidth = 105
        ItemIndex = -1
        OmitCharacterRequired = False
        TabOrder = 1
      end
      object cb_pais: TComboBox
        Left = 123
        Top = 109
        Width = 229
        Height = 21
        Style = csDropDownList
        Color = 16444382
        ItemHeight = 13
        MaxLength = 4
        TabOrder = 4
      end
      object txt_ApellidoMaterno: TEdit
        Left = 470
        Top = 60
        Width = 203
        Height = 21
        ParentShowHint = False
        ShowHint = True
        TabOrder = 7
      end
      object cbPersoneria: TComboBox
        Left = 123
        Top = 10
        Width = 105
        Height = 21
        Style = csDropDownList
        Color = 16444382
        ItemHeight = 13
        TabOrder = 0
        OnChange = cbPersoneriaChange
        Items.Strings = (
          'F'#237'sica                          F'
          'Jur'#237'dica                       J')
      end
      object cb_actividad: TComboBox
        Left = 123
        Top = 133
        Width = 229
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        MaxLength = 4
        TabOrder = 5
      end
      object cbSituacionIVA: TComboBox
        Left = 470
        Top = 84
        Width = 203
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 8
      end
      object GBDomicilios: TGroupBox
        Left = 2
        Top = 158
        Width = 677
        Height = 109
        Anchors = [akLeft, akTop, akRight]
        Caption = 'Domicilios '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 10
        object lblnada: TLabel
          Left = 15
          Top = 18
          Width = 63
          Height = 13
          Cursor = crHandPoint
          Hint = 'Domicilio particular'
          Caption = 'Particular: '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold, fsUnderline]
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
        end
        object Label5: TLabel
          Left = 15
          Top = 46
          Width = 60
          Height = 13
          Cursor = crHandPoint
          Hint = 'Domicilio particular'
          Caption = 'Comercial:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold, fsUnderline]
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
        end
        object LblDomicilioComercial: TLabel
          Left = 77
          Top = 46
          Width = 451
          Height = 26
          AutoSize = False
          Caption = 'Ingrese el domicilio'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsItalic]
          ParentFont = False
          WordWrap = True
        end
        object LblDomicilioParticular: TLabel
          Left = 78
          Top = 18
          Width = 451
          Height = 26
          AutoSize = False
          Caption = 'Ingrese el domicilio'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsItalic]
          ParentFont = False
          WordWrap = True
        end
        object Label6: TLabel
          Left = 16
          Top = 85
          Width = 96
          Height = 13
          Caption = 'Domicilio de entrega'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object BtnDomicilioComercialAccion: TDPSButton
          Left = 539
          Top = 47
          Width = 61
          Hint = 'Domicilio comercial'
          Caption = 'Alta'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          OnClick = BtnDomicilioComercialAccionClick
        end
        object BtnDomicilioParticularAccion: TDPSButton
          Left = 539
          Top = 16
          Width = 61
          Hint = 'Domicilio particular'
          Caption = 'Alta'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          OnClick = BtnDomicilioParticularAccionClick
        end
        object RGDomicilioEntrega: TRadioGroup
          Left = 127
          Top = 74
          Width = 235
          Height = 32
          Columns = 2
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Items.Strings = (
            '&Particular'
            '&Comercial')
          ParentFont = False
          TabOrder = 2
        end
        object ChkEliminarDomicilioParticular: TCheckBox
          Left = 604
          Top = 22
          Width = 67
          Height = 17
          Caption = 'Eliminar'
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
          OnClick = ChkEliminarDomicilioParticularClick
        end
        object ChkEliminarDomicilioComercial: TCheckBox
          Left = 605
          Top = 51
          Width = 67
          Height = 17
          Caption = 'Eliminar'
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 4
          OnClick = ChkEliminarDomicilioComercialClick
        end
      end
      object GBMediosComunicacion: TGroupBox
        Left = 2
        Top = 269
        Width = 676
        Height = 74
        Anchors = [akLeft, akTop, akRight]
        Caption = 'Medios de comunicaci'#243'n'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 11
        object Label7: TLabel
          Left = 11
          Top = 25
          Width = 91
          Height = 13
          Caption = 'Tel'#233'fono particular:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label18: TLabel
          Left = 253
          Top = 25
          Width = 93
          Height = 13
          Caption = 'Tel'#233'fono comercial:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label9: TLabel
          Left = 11
          Top = 49
          Width = 72
          Height = 13
          Caption = 'Tel'#233'fono movil:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label10: TLabel
          Left = 254
          Top = 51
          Width = 28
          Height = 13
          Caption = 'Email:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object txtAreaTelefonoParticular: TEdit
          Left = 111
          Top = 19
          Width = 46
          Height = 21
          Hint = 'C'#243'digo de '#225'rea'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnKeyPress = txtTelefonoParticularKeyPress
        end
        object txtTelefonoParticular: TEdit
          Left = 160
          Top = 19
          Width = 86
          Height = 21
          Hint = 'N'#250'mero de tel'#233'fono'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnKeyPress = txtTelefonoParticularKeyPress
        end
        object txtTelefonoComercial: TEdit
          Left = 403
          Top = 19
          Width = 86
          Height = 21
          Hint = 'N'#250'mero de tel'#233'fono'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnKeyPress = txtTelefonoComercialKeyPress
        end
        object txtTelefonoMovil: TEdit
          Left = 160
          Top = 43
          Width = 86
          Height = 21
          Hint = 'N'#250'mero de tel'#233'fono'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          OnKeyPress = txtTelefonoMovilKeyPress
        end
        object txtEmailParticular: TEdit
          Left = 353
          Top = 43
          Width = 228
          Height = 21
          Hint = 'direcci'#243'n de e-mail'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 6
        end
        object txtAreaTelefonoMovil: TEdit
          Left = 111
          Top = 43
          Width = 46
          Height = 21
          Hint = 'C'#243'digo de '#225'rea'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnKeyPress = txtTelefonoParticularKeyPress
        end
        object txtAreaTelefonoComercial: TEdit
          Left = 353
          Top = 19
          Width = 46
          Height = 21
          Hint = 'C'#243'digo de '#225'rea'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnKeyPress = txtTelefonoParticularKeyPress
        end
      end
    end
    object tab_DebitosAutomaticos: TTabSheet
      Caption = 'D'#233'bito &autom'#225'tico'
      ImageIndex = 2
      object Label1: TLabel
        Left = 12
        Top = 11
        Width = 344
        Height = 13
        Caption = 
          'A continuaci'#243'n se muestran las cuentas de d'#233'bito autom'#225'tico del ' +
          'cliente:'
      end
      object ListaDebitos: TDBList
        Left = 8
        Top = 30
        Width = 649
        Height = 145
        TabStop = True
        TabOrder = 0
        HeaderFont.Charset = DEFAULT_CHARSET
        HeaderFont.Color = clWindowText
        HeaderFont.Height = -11
        HeaderFont.Name = 'MS Sans Serif'
        HeaderFont.Style = []
        Table = DebitosAutomaticos
        Style = lbOwnerDrawFixed
        OnDrawItem = ListaDebitosDrawItem
        OnRefresh = ListaDebitosRefresh
      end
      object btn_nueva: TDPSButton
        Left = 406
        Top = 180
        Caption = '&Nueva'
        TabOrder = 1
        OnClick = btn_nuevaClick
      end
      object btn_modificar: TDPSButton
        Left = 486
        Top = 180
        Caption = '&Modificar'
        TabOrder = 2
        OnClick = btn_modificarClick
      end
      object btn_quitar: TDPSButton
        Left = 566
        Top = 180
        Caption = '&Quitar'
        TabOrder = 3
        OnClick = btn_quitarClick
      end
    end
    object tab_Cuenta: TTabSheet
      Caption = 'Datos de &la Cuenta'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object GroupBoxVehiculo: TGroupBox
        Left = 3
        Top = 4
        Width = 664
        Height = 257
        Caption = ' Datos del Veh'#237'culo '
        TabOrder = 0
        object Label13: TLabel
          Left = 37
          Top = 55
          Width = 49
          Height = 13
          Caption = 'Patente:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label14: TLabel
          Left = 37
          Top = 81
          Width = 40
          Height = 13
          Caption = 'Marca:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label20: TLabel
          Left = 37
          Top = 106
          Width = 46
          Height = 13
          Caption = 'Modelo:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label21: TLabel
          Left = 195
          Top = 155
          Width = 27
          Height = 13
          Caption = 'Color:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label23: TLabel
          Left = 37
          Top = 30
          Width = 64
          Height = 13
          Caption = 'Utilizaci'#243'n:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label38: TLabel
          Left = 37
          Top = 130
          Width = 61
          Height = 13
          Caption = 'Categor'#237'a:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label47: TLabel
          Left = 37
          Top = 155
          Width = 27
          Height = 13
          Caption = '&A'#241'o:'
          FocusControl = anio
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object chk_contag: TCheckBox
          Left = 367
          Top = 188
          Width = 177
          Height = 17
          Caption = 'Equipar el veh'#237'culo con un TAG'
          Checked = True
          State = cbChecked
          TabOrder = 5
          OnClick = ActualizarPago
        end
        object chk_detallepasadas: TCheckBox
          Left = 367
          Top = 207
          Width = 258
          Height = 17
          Caption = 'Imprimir un detalle de pasadas para este veh'#237'culo'
          TabOrder = 6
          OnClick = chk_detallepasadasClick
        end
        object cb_color: TComboBox
          Left = 233
          Top = 149
          Width = 121
          Height = 21
          Style = csDropDownList
          ItemHeight = 0
          TabOrder = 4
        end
        object cb_marca: TComboBox
          Left = 111
          Top = 76
          Width = 243
          Height = 21
          Style = csDropDownList
          Color = 16444382
          ItemHeight = 0
          TabOrder = 2
          OnChange = cb_marcaChange
        end
        object cb_modelo: TComboBox
          Left = 111
          Top = 101
          Width = 243
          Height = 21
          Style = csDropDownList
          Color = 16444382
          ItemHeight = 0
          TabOrder = 3
          OnChange = cb_modeloChange
        end
        object cb_TipoCliente: TComboBox
          Left = 111
          Top = 27
          Width = 243
          Height = 21
          Style = csDropDownList
          Color = 16444382
          ItemHeight = 0
          TabOrder = 0
          OnClick = ActualizarPago
        end
        object chk_enviofacturas: TCheckBox
          Left = 367
          Top = 225
          Width = 185
          Height = 17
          Caption = 'Enviar facturas a domicilio'
          TabOrder = 7
        end
        object mcPatente: TMaskCombo
          Left = 111
          Top = 52
          Width = 241
          Height = 21
          Items = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Color = 16444382
          ComboWidth = 140
          ItemIndex = -1
          OmitCharacterRequired = False
          TabOrder = 1
        end
        object anio: TNumericEdit
          Left = 111
          Top = 149
          Width = 47
          Height = 21
          Color = 16444382
          MaxLength = 4
          TabOrder = 8
          OnChange = anioChange
        end
        object GroupDimensiones: TGroupBox
          Left = 36
          Top = 175
          Width = 321
          Height = 74
          Caption = 'Dimensiones (en cent'#237'metros)'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 9
          object Label48: TLabel
            Left = 102
            Top = 49
            Width = 92
            Height = 13
            Caption = '&Largo del veh'#237'culo:'
            FocusControl = largoVehiculo
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label49: TLabel
            Left = 119
            Top = 25
            Width = 75
            Height = 13
            Caption = '&Largo adicional:'
            FocusControl = LargoAdicionalVehiculo
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object largoVehiculo: TNumericEdit
            Left = 197
            Top = 43
            Width = 69
            Height = 21
            Color = clBtnFace
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            TabOrder = 0
          end
          object LargoAdicionalVehiculo: TNumericEdit
            Left = 197
            Top = 19
            Width = 69
            Height = 21
            Color = clBtnFace
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
          end
          object chk_acoplado: TCheckBox
            Left = 9
            Top = 25
            Width = 78
            Height = 17
            Caption = 'Acoplado'
            Color = clBtnFace
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentColor = False
            ParentFont = False
            TabOrder = 2
            OnClick = chk_acopladoClick
          end
        end
        object txt_DescCategoria: TEdit
          Left = 111
          Top = 125
          Width = 243
          Height = 21
          TabStop = False
          Color = clBtnFace
          MaxLength = 16
          ReadOnly = True
          TabOrder = 10
        end
        object txt_codigoCategoria: TNumericEdit
          Left = 359
          Top = 125
          Width = 47
          Height = 21
          Color = 16444382
          MaxLength = 4
          TabOrder = 11
          Visible = False
          OnChange = anioChange
        end
      end
      object GroupBoxCondiciones: TGroupBox
        Left = 3
        Top = 263
        Width = 664
        Height = 138
        Caption = ' Condiciones Comerciales '
        TabOrder = 1
        object Label22: TLabel
          Left = 41
          Top = 89
          Width = 89
          Height = 13
          Caption = 'Plan Comercial:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label34: TLabel
          Left = 464
          Top = 90
          Width = 64
          Height = 13
          Cursor = crHandPoint
          Hint = 'Alternativas sobre planes comerciales'
          Caption = 'Informaci'#243'n...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsUnderline]
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          OnClick = Label34Click
        end
        object rb_prepago: TRadioButton
          Left = 40
          Top = 67
          Width = 73
          Height = 17
          Caption = '&Prepago'
          TabOrder = 3
          Visible = False
          OnClick = ActualizarPago
        end
        object rb_pospagomanual: TRadioButton
          Left = 40
          Top = 20
          Width = 158
          Height = 17
          Caption = 'Po&spago con pago manual'
          Checked = True
          TabOrder = 0
          TabStop = True
          OnClick = ActualizarPago
        end
        object rb_pospagoautomatico: TRadioButton
          Left = 40
          Top = 44
          Width = 178
          Height = 17
          Caption = 'Po&spago con d'#233'bito autom'#225'tico'
          TabOrder = 1
          OnClick = ActualizarPago
        end
        object cb_debitoautomatico: TComboBox
          Left = 229
          Top = 42
          Width = 372
          Height = 21
          Style = csDropDownList
          Color = 16444382
          ItemHeight = 0
          TabOrder = 2
          OnClick = ActualizarPago
        end
        object cb_PlanComercial: TComboBox
          Left = 229
          Top = 85
          Width = 226
          Height = 21
          Style = csDropDownList
          Color = 16444382
          ItemHeight = 0
          TabOrder = 4
          OnClick = cb_PlanComercialClick
        end
      end
    end
    object tab_Contratos: TTabSheet
      Caption = 'C&ontratos'
      ImageIndex = 3
      object Label16: TLabel
        Left = 12
        Top = 11
        Width = 349
        Height = 13
        Caption = 
          'A continuaci'#243'n se muestran los contratos correspondientes a esta' +
          ' cuenta:'
      end
      object ListaContratos: TDBList
        Left = 8
        Top = 30
        Width = 633
        Height = 145
        TabStop = True
        TabOrder = 0
        HeaderFont.Charset = DEFAULT_CHARSET
        HeaderFont.Color = clWindowText
        HeaderFont.Height = -11
        HeaderFont.Name = 'MS Sans Serif'
        HeaderFont.Style = []
        SubTitulos.Sections = (
          #0'85'#0'N'#250'mero'
          #0'247'#0'Plan Comercial'
          #0'138'#0'Fecha de Impresi'#243'n'
          #0'81'#0'Fecha de Firma')
        Table = Contratos
        Style = lbOwnerDrawVariable
        OnDrawItem = ListaContratosDrawItem
        OnRefresh = ListaContratosRefresh
      end
      object btn_imprimir: TDPSButton
        Left = 476
        Top = 179
        Width = 78
        Caption = '&Imprimir'
        TabOrder = 2
      end
      object btn_reimprimir: TDPSButton
        Left = 562
        Top = 179
        Width = 78
        Caption = '&Reimprimir'
        TabOrder = 3
      end
      object btn_firmar: TDPSButton
        Left = 391
        Top = 179
        Width = 78
        Caption = '&Firmar'
        TabOrder = 1
      end
    end
    object tab_Facturacion: TTabSheet
      Caption = '&Facturaci'#243'n'
      ImageIndex = 4
      object GroupBox4: TGroupBox
        Left = 4
        Top = 7
        Width = 662
        Height = 378
        Caption = ' Agrupaci'#243'n de Facturas '
        TabOrder = 0
        object Label15: TLabel
          Left = 42
          Top = 55
          Width = 106
          Height = 13
          Caption = 'Grupo de Facturaci'#243'n:'
        end
        object Label2: TLabel
          Left = 27
          Top = 281
          Width = 116
          Height = 13
          Caption = 'Domicilio de facturaci'#243'n:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label3: TLabel
          Left = 27
          Top = 306
          Width = 165
          Height = 13
          Caption = 'Domicilio de informaci'#243'n comercial:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object rb_separada: TRadioButton
          Left = 24
          Top = 30
          Width = 252
          Height = 17
          Caption = 'Imprimir una factura separada para esta cuenta'
          Checked = True
          TabOrder = 0
          TabStop = True
          OnClick = CambioAgrupacion
        end
        object rb_agrupar: TRadioButton
          Left = 24
          Top = 94
          Width = 345
          Height = 17
          Caption = 
            'Incluir esta cuenta con la factura del siguiente grupo de cuenta' +
            's'
          TabOrder = 1
          OnClick = CambioAgrupacion
        end
        object ListaGrupoCuentas: TDBList
          Left = 24
          Top = 118
          Width = 553
          Height = 145
          Color = clBtnFace
          TabStop = True
          TabOrder = 2
          HeaderFont.Charset = DEFAULT_CHARSET
          HeaderFont.Color = clWindowText
          HeaderFont.Height = -11
          HeaderFont.Name = 'MS Sans Serif'
          HeaderFont.Style = []
          Table = qry_GruposCuentas
          Style = lbOwnerDrawFixed
          OnDrawItem = ListaGrupoCuentasDrawItem
        end
        object cb_GrupoFacturacion: TComboBox
          Left = 158
          Top = 52
          Width = 419
          Height = 21
          Style = csDropDownList
          ItemHeight = 0
          TabOrder = 3
        end
        object cbDomicilioFacturacion: TEdit
          Left = 203
          Top = 274
          Width = 374
          Height = 21
          Enabled = False
          MaxLength = 50
          TabOrder = 4
        end
        object cbDomicilioInfoComercial: TEdit
          Left = 203
          Top = 299
          Width = 374
          Height = 21
          Enabled = False
          MaxLength = 50
          TabOrder = 5
        end
      end
    end
    object Roles: TTabSheet
      Caption = 'Roles'
      ImageIndex = 5
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Label35: TLabel
        Left = 12
        Top = 11
        Width = 367
        Height = 13
        Caption = 
          'A continuaci'#243'n se muestran las personas relacionadas a la cuenta' +
          ' y sus roles:'
      end
      object btn_AgregarRol: TDPSButton
        Left = 481
        Top = 276
        Caption = '&Agregar'
        TabOrder = 0
        OnClick = btn_AgregarRolClick
      end
      object btn_QuitarRol: TDPSButton
        Left = 563
        Top = 276
        Caption = '&Quitar'
        TabOrder = 1
        OnClick = btn_QuitarRolClick
      end
      object ListaRoles: TDBListEx
        Left = 8
        Top = 30
        Width = 646
        Height = 241
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 300
            Header.Caption = 'Nombre'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Nombre'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 300
            Header.Caption = 'Rol'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Descripcion'
          end>
        DataSource = DataSource1
        DragReorder = True
        ParentColor = False
        TabOrder = 2
        TabStop = True
      end
    end
  end
  object OPButton1: TDPSButton
    Left = 521
    Top = 472
    Height = 26
    Caption = '&Guardar'
    Default = True
    TabOrder = 1
    OnClick = OPButton1Click
  end
  object OPButton2: TDPSButton
    Left = 601
    Top = 472
    Height = 26
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 2
  end
  object DebitosAutomaticos: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoDebitoAutomatico'
        DataType = ftInteger
      end
      item
        Name = 'TipoDebito'
        Attributes = [faRequired]
        DataType = ftString
        Size = 2
      end
      item
        Name = 'CodigoEntidad'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'CuentaDebito'
        Attributes = [faRequired]
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Descripcion'
        Attributes = [faRequired]
        DataType = ftString
        Size = 255
      end
      item
        Name = 'Vencimiento'
        DataType = ftDateTime
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 6
    Top = 431
  end
  object BuscarCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'BuscarClienteContacto;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@Apellido'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@ApellidoMaterno'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@Nombre'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@CodigoCuenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Personeria'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end>
    Left = 291
    Top = 431
  end
  object qry_Debitos: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'CodigoPersona'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'
      '  CodigoDebitoAutomatico,'
      '  TipoDebito,'
      '  CodigoEntidad,'
      '  CuentaDebito,'
      
        '  dbo.ObtenerDescripcionDebitoAutomatico(CodigoPersona, CodigoDe' +
        'bitoAutomatico) AS Descripcion'
      'FROM'
      '  DebitoAutomatico'
      'WHERE'
      '  CodigoPersona = :CodigoPersona')
    Left = 100
    Top = 431
  end
  object Contratos: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'NumeroContrato'
        DataType = ftInteger
      end
      item
        Name = 'PlanComercial'
        DataType = ftInteger
      end
      item
        Name = 'Version'
        DataType = ftInteger
      end
      item
        Name = 'Descripcion'
        DataType = ftString
        Size = 80
      end
      item
        Name = 'FechaImpresion'
        DataType = ftDateTime
      end
      item
        Name = 'FechaFirma'
        DataType = ftDateTime
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 66
    Top = 431
  end
  object qry_Contratos: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'CodigoCuenta'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'
      '  Contratos.*,'
      '  PlanesComerciales.Descripcion AS DescriPlan,'
      'FROM'
      '  Contratos (INDEX = UK_CuentaPlan),'
      '  PlanesComerciales'
      'WHERE'
      '  Contratos.CodigoCuenta = :CodigoCuenta'
      '  AND PlanesComerciales.PlanComercial = Contratos.PlanComercial'
      '--  AND PlanesComerciales.Version = Contratos.Version')
    Left = 132
    Top = 431
  end
  object ObtenerContratoPlan: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerContratoPlan'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@PlanComercial'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 323
    Top = 431
  end
  object qry_GruposCuentas: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'CodigoPersona'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'CodigoCuenta'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'TipoPago'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'CodigoDebitoAutomatico'
        DataType = ftWord
        Precision = 3
        Size = 1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT DISTINCT'
      '  AgrupacionCuentas,'
      '  TipoPago,'
      
        '  dbo.ObtenerCuentasAgrupacion(CodigoPersona, TipoPago, Agrupaci' +
        'onCuentas) AS Cuentas'
      'FROM'
      '  Cuentas'
      'WHERE'
      '  CodigoPersona = :CodigoPersona'
      '  AND CodigoCuenta <> :CodigoCuenta'
      '  AND TipoPago = :TipoPago'
      
        '  AND ISNULL(CodigoDebitoAutomatico, 0) = :CodigoDebitoAutomatic' +
        'o'
      '')
    Left = 163
    Top = 431
  end
  object ActualizarDatosCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDatosCliente;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Estado'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end>
    Left = 355
    Top = 471
  end
  object ActualizarDebitoAutomatico: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDebitoAutomatico;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDebitoAutomatico'
        Attributes = [paNullable]
        DataType = ftWord
        Direction = pdInputOutput
        Precision = 3
        Value = Null
      end
      item
        Name = '@TipoDebito'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@CodigoEntidad'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CuentaDebito'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = Null
      end
      item
        Name = '@Vencimiento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaCierre'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 419
    Top = 471
  end
  object ActualizarCuentaCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarCuentaCliente;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDebitoAutomatico'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@TipoPatente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@Patente'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@CodigoMarca'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@CodigoModelo'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@Categoria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@AnioVehiculo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@LargoAdicionalVehiculo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TieneAcoplado'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@CodigoColor'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@TipoAdhesion'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@TipoCliente'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@PlanComercial'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@DetallePasadas'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@CodigoGrupoFacturacion'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@FechaBaja'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@TipoPago'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@AgrupacionCuentas'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@EstadoCuenta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@EnvioFacturas'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@CodigoDomicilioFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDomicilioInfoComercial'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ResponsableLegal'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCuenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 451
    Top = 471
  end
  object CrearContrato: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CrearContrato;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroContrato'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCuenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PlanComercial'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 483
    Top = 471
  end
  object qry_DatosCuenta: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'CodigoCuenta'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'
      '  Cuentas.*,'
      '  Personas.CodigoDocumento,'
      '  Personas.NumeroDocumento'
      'FROM'
      '  Cuentas,'
      '  Personas'
      'WHERE'
      '  Cuentas.CodigoCuenta = :CodigoCuenta'
      '  AND Personas.CodigoPersona = Cuentas.CodigoPersona')
    Left = 195
    Top = 431
  end
  object qry_ContratosHechos: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'CodigoCuenta'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'
      '  Contratos.*,'
      '  PlanesComerciales.Descripcion'
      'FROM'
      '  Contratos,'
      '  PlanesComerciales'
      'WHERE'
      '  Contratos.CodigoCuenta = :CodigoCuenta'
      '  AND PlanesComerciales.PlanComercial = Contratos.PlanComercial')
    Left = 227
    Top = 431
  end
  object qry_OS: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = 'CodigoOrdenServicio'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'
      '  *'
      'FROM'
      '  OrdenesServicioAdhesion'
      'WHERE'
      '  CodigoOrdenServicio = :CodigoOrdenServicio')
    Left = 259
    Top = 431
  end
  object DebitosAutomaticosBorrados: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoDebitoAutomatico'
        DataType = ftInteger
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 36
    Top = 431
  end
  object qry_DebitosAutomaticosBorrar: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <
      item
        Name = '@CodigoPersona'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = '@CodigoDebitoAutomatico'
        DataType = ftWord
        Precision = 3
        Size = 1
        Value = Null
      end>
    SQL.Strings = (
      'DELETE FROM DebitoAutomatico'
      'WHERE CodigoPersona = :@CodigoPersona'
      'AND CodigoDebitoAutomatico = :@CodigoDebitoAutomatico')
    Left = 355
    Top = 438
  end
  object ObtenerDomiciliosPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDomiciliosPersona;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoOrigenDato'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end>
    Left = 376
    Top = 172
  end
  object ObtenerOrdenServicioAdhesionDomicilios: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerOrdenServicioAdhesionDomicilios;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoOrdenServicio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoOrigenDato'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Size = -1
        Value = Null
      end>
    Left = 408
    Top = 172
  end
  object ActualizarDatosPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDatosPersona;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Personeria'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@Apellido'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@ApellidoMaterno'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@Nombre'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@Sexo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@LugarNacimiento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@FechaNacimiento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoActividad'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ContactoComercial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@CodigoSituacionIVA'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@Estado'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@CodigoDomicilioEntrega'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PIN'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 4
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 387
    Top = 471
  end
  object ActualizarDomicilio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDomicilio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoOrigenDato'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoTipoEdificacion'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoCalle'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CalleDesnormalizada'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@Numero'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Piso'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Depto'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@CodigoPostal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@Detalle'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@Activo'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@CodigoDomicilio'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 406
    Top = 398
  end
  object ActualizarDomicilioEntregaPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDomicilioEntregaPersona;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDomicilioEntrega'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 415
    Top = 438
  end
  object EliminarDomicilioPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'EliminarDomicilioPersona;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDomicilio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 446
    Top = 438
  end
  object ActualizarMedioComunicacionPersona: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarMedioComunicacionPersona;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoMedioContacto'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoTipoOrigenDato'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Valor'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@CodigoDomicilio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@HorarioDesde'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@HorarioHasta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Observaciones'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@Activo'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 477
    Top = 438
  end
  object qry_RolesCuenta: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'CodigoCuenta'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'select * from VW_Roles'
      'where CodigoCuenta = :CodigoCuenta')
    Left = 171
    Top = 345
  end
  object DataSource1: TDataSource
    DataSet = qry_RolesCuenta
    Left = 141
    Top = 345
  end
  object ActualizarRolesCuentas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarRolesCuentas;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRol'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCuenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 516
    Top = 438
  end
  object BorrarRolesCuentas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'BorrarRolesCuentas;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoRol'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoCuenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end>
    Left = 546
    Top = 438
  end
  object ADOQuery1: TADOQuery
    Parameters = <>
    Left = 371
    Top = 24
  end
  object ObtenerDatosDomicilio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDatosDomicilio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDomicilio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 338
    Top = 176
  end
  object ActualizarDomicilioRelacionado: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarDomicilioRelacionado;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDomicilio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCalle'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Numero'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 374
    Top = 398
  end
end
