{********************************** File Header ********************************
File Name : ComponerEMail.pas
Author : gcasais
Date Created: 19/07/2005
Language : ES-AR
Description :

Etiqueta	:	20160530 MGO
Descripción	:	Se llama a ObtenerCorrespondencia cuando el tipo es "C"
*******************************************************************************}

unit ComponerEMail;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, MensajesEMail, Util, UtilProc, DMMensaje,
  OleCtrls, SHDocVw, ComCtrls;

type
  TfrmComponerMail = class(TForm)
    btnCerrar: TButton;
    Memo: TMemo;
    Bevel1: TBevel;
    Label1: TLabel;
    Label2: TLabel;
    lblPara: TLabel;
    lblAsunto: TLabel;
    wbEMail: TWebBrowser;
    btnActualizar: TButton;
    procedure btnCerrarClick(Sender: TObject);
    procedure btnActualizarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    strDir: string;
    procedure MostrarMail(Mensaje: TMensaje);
  public
    { INICIO : 20160530 MGO
    function Inicializar(DM: TDMMensajes; CodigoMensaje: Integer; var Mensaje: TMensaje; var Error: string): Boolean;
    }
    function Inicializar(DM: TDMMensajes; CodigoMensaje: Integer; TipoLetra: String; var Mensaje: TMensaje; var Error: string): Boolean;
    // FIN : 20160530 MGO
  end;

var
  frmComponerMail: TfrmComponerMail;

implementation

resourcestring
	//INICIO:	20160706 CFU
	NOMBREARCHIVO1 	= 'paso.html';
	NOMBREARCHIVO2 	= 'paso2.html';
	//TERMINO:	20160706 CFU

{$R *.dfm}

{******************************** Function Header ******************************
Function Name: TfrmComponerMail.Inicializar
Author : gcasais
Date Created : 19/07/2005
Description :
Parameters : None
Return Value : Boolean
*******************************************************************************}
{ INICIO : 20160530 MGO
function TfrmComponerMail.Inicializar(DM: TDMMensajes; CodigoMensaje: Integer; var Mensaje: TMensaje; var Error: string): Boolean;
}
function TfrmComponerMail.Inicializar(DM: TDMMensajes; CodigoMensaje: Integer; TipoLetra: String; var Mensaje: TMensaje; var Error: string): Boolean;
// FIN : 20160530 MGO
resourcestring
    MSG_ERROR = 'Error';
    MSG_TITLE = 'Componer e-Mail';
var
    Email: TMensaje;
    ErrorMsg: String;
begin
    { INICIO : 20160530 MGO
    Result := ObtenerMensaje(DM, CodigoMensaje, EMail, ErrorMsg);
    }
    if TipoLetra = 'C' then begin
        Result := ObtenerCorrespondencia(DM, CodigoMensaje, Email, ErrorMsg);
    end else begin
        Result := ObtenerMensaje(DM, CodigoMensaje, EMail, ErrorMsg);
    end;
    // FIN : 20160530 MGO
    if Result then begin
        MostrarMail(Email);
    end else begin
        MsgBoxErr(MSG_ERROR, Error, MSG_TITLE, MB_ICONERROR);
    end;
end;

{******************************** Function Header ******************************
Function Name: TfrmComponerMail.MostrarMail
Author : gcasais
Date Created : 19/07/2005
Description :
Parameters : Mensaje: TMensaje
Return Value : None
*******************************************************************************}
procedure TfrmComponerMail.MostrarMail(Mensaje: TMensaje);
//INICIO:	20160706 CFU
var
	mensajeHTML: string;
    tf: TextFile;
begin
	mensajeHTML			:= '<html><body>' + Mensaje.Mensaje + '</body></html>';
    try
//    	if not DirectoryExists(NOMBREDIR) then
//        	MkDir(NOMBREDIR);
    	AssignFile(tf, strDir + NOMBREARCHIVO1);
    	Rewrite(tf);
    	Writeln(tf, mensajeHTML);
    	CloseFile(tf);
        wbEMail.Navigate(strDir + NOMBREARCHIVO1);
    except
    	
    	wbEMail.Navigate('about:blank');
    end;
//TERMINO:	20160706 CFU

    lblPara.Caption		:= Mensaje.Para;
    lblAsunto.Caption	:= Mensaje.Asunto;
    Memo.Text 			:= Mensaje.Mensaje;
end;

//INICIO:	20160706 CFU
procedure TfrmComponerMail.btnActualizarClick(Sender: TObject);
var
	mensajeHTML: string;
    tf: TextFile;
begin
	mensajeHTML			:= '<html><body>' + Memo.Text + '</body></html>';

    try
//    	if not DirectoryExists(NOMBREDIR) then
//        	MkDir(NOMBREDIR);
    	AssignFile(tf, strDir + NOMBREARCHIVO2);
    	Rewrite(tf);
    	Writeln(tf, mensajeHTML);
    	CloseFile(tf);
        wbEMail.Navigate(strDir + NOMBREARCHIVO2);
    except
    	wbEMail.Navigate('about:blank');
    end;
    
end;

procedure TfrmComponerMail.btnCerrarClick(Sender: TObject);
begin
    try
        DeleteFile(strDir + NOMBREARCHIVO1);
        DeleteFile(strDir + NOMBREARCHIVO2);
//        RmDir(NOMBREDIR);
    except
    end;
    Close;
end;
procedure TfrmComponerMail.FormCreate(Sender: TObject);
begin
	strDir	:= ExtractFilePath(Application.ExeName);
end;

end.
