{-------------------------------------------------------------------------------
 File Name: FreFuenteReclamoOrdenServicio.pas
 Author: Lgisuk
 Date Created: 17/06/05
 Language: ES-AR
 Description: Seccion con la información de la fuente de origen del reclamo
 
Etiqueta    : 20160315 MGO
Descripción : Se utiliza InstallIni en vez de ApplicationIni

-------------------------------------------------------------------------------}
unit FreFuenteReclamoOrdenServicio;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, Util, UtilDB, VariantComboBox, Validate, DateEdit, UtilProc, DB,ADODB;

type
  TFrameFuenteReclamoOrdenServicio = class(TFrame)
    PFuenteReclamo: TGroupBox;
    LFuenteReclamo: TLabel;
    cbFuenteReclamo: TVariantComboBox;
  private
    Function  GetFuenteReclamo: Integer;
    Procedure SetFuenteReclamo(const Value: Integer);
    { Private declarations }
  public
    { Public declarations }
    Function  Inicializar: Boolean; overload;
    Function  Inicializar(FuenteReclamo: Integer): Boolean; overload;
    Function  Deshabilitar:boolean;
    Function  Validar: Boolean;
    Procedure LoadFromDataset(DS: TDataset);
    //
    Property FuenteReclamo: Integer read GetFuenteReclamo write SetFuenteReclamo;
  end;

implementation

uses RStrings, DMConnection;

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    lgisuk
  Date Created: 17/06/2005
  Description: inicializacion del frame
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFrameFuenteReclamoOrdenServicio.Inicializar: Boolean;

    {-----------------------------------------------------------------------------
      Function Name: CargarFuentesReclamo
      Author:    lgisuk
      Date Created: 20/04/2005
      Description: cargo las fuentes del reclamo
      Parameters: Combo:TVariantComboBox
      Return Value: None
    -----------------------------------------------------------------------------}
    procedure CargarFuentesReclamo(Combo:TVariantComboBox);
    const
        CONST_NINGUNO = '(Seleccionar)';
    var
        SP: TAdoStoredProc;
    begin
        try
            Combo.Items.Add(CONST_NINGUNO, 0);
            try
                SP := TADOStoredProc.Create(nil);
                SP.Connection := DMConnections.BaseCAC;
                SP.ProcedureName := 'ObtenerFuentesReclamo';
                SP.Open;
                while not sp.Eof do begin
                    Combo.Items.Add(SP.fieldbyname('descripcion').asstring,SP.fieldbyname('CodigoFuenteReclamo').asinteger);
                    SP.Next;
                end;
                SP.Close;
                Combo.ItemIndex:=0;
            except
            end;
        finally
            FreeAndNil(SP);
        end;
    end;

var SQLQuery:string;
    CodigoFuenteReclamo:integer;
    PuntoEntrega:Integer;
begin
    //Cargo las fuentes de reclamo
    CargarFuentesReclamo(cbFuenteReclamo);
    //Verifico si hay una fuente de reclamo predeterminada para ese punto de Entrega
    // INICIO : 20160315 MGO
    PuntoEntrega:=ApplicationIni.ReadInteger('General', 'PuntoEntrega', -1);
    {
    PuntoEntrega:=InstallIni.ReadInteger('General', 'PuntoEntrega', -1);
    }
    // FIN : 20160315 MGO
    SQLQuery:='SELECT dbo.ObtenerFuenteReclamoPredeterminadaOS ('+IntToStr(PuntoEntrega)+')';
    CodigoFuenteReclamo:=QueryGetValueInt(dmconnections.BaseCAC,SQLQuery);
    cbfuentereclamo.Value:=CodigoFuenteReclamo;
    Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    lgisuk
  Date Created: 17/06/2005
  Description: inicializo el frame eligiendo una fuente de reclamo
  Parameters: FuenteReclamo: Integer
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFrameFuenteReclamoOrdenServicio.Inicializar(FuenteReclamo: Integer): Boolean;
begin
    cbfuentereclamo.Value := FuenteReclamo;
    Result := Inicializar;
end;

{-----------------------------------------------------------------------------
  Function Name: GetFuenteReclamo
  Author:    lgisuk
  Date Created: 17/06/2005
  Description:  Obtengo la fuente de reclamo
  Parameters: None
  Return Value: Integer
-----------------------------------------------------------------------------}
function TFrameFuenteReclamoOrdenServicio.GetFuenteReclamo: Integer;
begin
    Result := cbFuenteReclamo.Value;
end;

{-----------------------------------------------------------------------------
  Function Name: SetFuenteReclamo
  Author:    lgisuk
  Date Created: 17/06/2005
  Description: Asigno la fuente de Reclamo
  Parameters: const Value: Integer
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrameFuenteReclamoOrdenServicio.SetFuenteReclamo(const Value: Integer);
begin
    cbFuenteReclamo.Value := Value;
end;

{-----------------------------------------------------------------------------
  Function Name: LoadFromDataset
  Author:    lgisuk
  Date Created: 17/06/2005
  Description: cargo la fuente de reclamo de un dataset
  Parameters: DS: TDataset
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrameFuenteReclamoOrdenServicio.LoadFromDataset(DS: TDataset);
begin
    cbFuenteReclamo.Value := DS['CodigoFuenteReclamo'];
end;

{-----------------------------------------------------------------------------
  Function Name: Deshabilitar
  Author:    lgisuk
  Date Created: 17/06/2005
  Description:  deshabilito los campos
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
function TFrameFuenteReclamoOrdenServicio.Deshabilitar:boolean;
begin
    cbfuentereclamo.Enabled:= False;
    cbfuentereclamo.Color:= clBtnFace;
    result:= true;
end;

{-----------------------------------------------------------------------------
  Function Name: Validar
  Author:    lgisuk
  Date Created: 17/06/2005
  Description: valido la fuente de reclamo
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFrameFuenteReclamoOrdenServicio.Validar: Boolean;
resourcestring
    MSG_ERROR_FUENTE = 'Debe indicar la fuente del reclamo';
begin
    Result := False;
    //obligo a que carguen una fuente de reclamo
    if cbfuentereclamo.value = 0 then begin
        MsgBoxBalloon(MSG_ERROR_FUENTE, STR_ERROR, MB_ICONSTOP, cbfuentereclamo);
        cbfuentereclamo.SetFocus;
        Exit;
    end;
    Result := True;
end;



end.
