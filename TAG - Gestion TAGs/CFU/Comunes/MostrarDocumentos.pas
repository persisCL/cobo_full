unit MostrarDocumentos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, ComCtrls, OleServer, Word2000,
  UtilProc;

type
  TFormMostrarDocumentos = class(TForm)
    Panel1: TPanel;
    Texto: TRichEdit;
    Panel2: TPanel;
    Panel3: TPanel;
    btnAceptar: TButton;
    procedure btnAceptarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
    function Inicializa(MDIChild: Boolean; CaptionForm: AnsiString; DocumentoAMostrar: AnsiString): boolean;
  end;

var
  FormMostrarDocumentos: TFormMostrarDocumentos;

implementation

{$R *.dfm}

{ TForm1 }

function TFormMostrarDocumentos.Inicializa(MDIChild: Boolean; CaptionForm,
  DocumentoAMostrar: AnsiString): boolean;
resourcestring
    NO_SE_PUEDE = 'No se puede mostrar la información deseada.';
Var
	S: TSize;
begin
    result := true;
    if MDIChild then begin
        S := GetFormClientSize(Application.MainForm);
        SetBounds(0, 0, S.cx, S.cy);
    end else begin
        FormStyle := fsNormal;
        Visible := False;
    end;
        self.Caption := CaptionForm;
    try
        Texto.lines.LoadFromFile(DocumentoAMostrar);
    except
        on E: exception do begin
            MsgBox(NO_SE_PUEDE + #13#10 +
                    E.Message, self.Caption, MB_ICONSTOP);
            result := false;
        end;
    end;

end;

procedure TFormMostrarDocumentos.btnAceptarClick(Sender: TObject);
begin
    close;
end;

procedure TFormMostrarDocumentos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

end.
