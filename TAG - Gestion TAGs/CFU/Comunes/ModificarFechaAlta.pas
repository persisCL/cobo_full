{----------------------------------------------------------------------------------------
                ModificarFechaAlta

    Author:
    Date:
    Description:

    Revision 1
        Author      : mbecerra
        Date        : 14-Septiembre-2010
        Description :       (Ref SS 921 )
                        Nueva Inicializaci�n que permite el ingreso de la
                        fecha de vencimiento de la garant�a del TAG)
-----------------------------------------------------------------------------------------}
unit ModificarFechaAlta;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, TimeEdit, Validate, DateEdit, ExtCtrls, PeaProcs, UtilDB, utilproc, Variants, Util;

type
  TfrmModificarFechaAlta = class(TForm)
    GridPanel1: TGridPanel;
    Panel1: TPanel;
    GridPanel2: TGridPanel;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    DtFechaAlta: TDateEdit;
    DtHoraAlta: TTimeEdit;
    lblFechaAlta: TLabel;
    lblHoraAlta: TLabel;
    procedure BtnAceptarClick(Sender: TObject);
  private
    { Private declarations }
    FEsModificacionFechaAlta : Boolean;         //REV.1
    FFechaAltaCuenta : TDateTime;               //REV.1
    FAniosGarantia : Integer;                   //REV.1
    FUltimaBaja: TDateTime;         // SS_916_PDO_20120109
  public
    Function Inicializa (patente:String; ContractSerialNumber: Int64; ContextMark: Integer; FechaHoraAlta, FechaHoraUltimaBaja: TDateTime) : Boolean; overload;     // SS_916_PDO_20120109
    Function ValidarFechaHoraAlta(Fecha: TdateTime; patente:String; ContractSerialNumber: Int64; ContextMark: Integer): Boolean;
    { Public declarations }
    function Inicializa(FechaVencimiento, FechaAlta : TDateTime; AniosGarantia : integer) : Boolean; overload;      //REV.1
  end;

var
  frmModificarFechaAlta: TfrmModificarFechaAlta;
  Fpatente:String;
  FContractSerialNumber: Int64;
  FContextMark: Integer;


implementation

uses DMConnection, FrmSCDatosVehiculo;

{$R *.dfm}

{ TfrmModificarFechaAlta }

procedure TfrmModificarFechaAlta.BtnAceptarClick(Sender: TObject);
resourcestring
    MSG_FECHA_ALTA_ERROR  = 'La fecha indicada como fecha de alta no es v�lida. La Fecha NO puede ser superior a Hoy.';                                // SS_916_PDO_20120109
    MSG_FECHA_ALTA_ERROR2 = '"La fecha indicada como fecha de alta no es v�lida. La Fecha NO puede ser inferior a el "dd"-"mm"-"yyy" a las "hh:mm';    // SS_916_PDO_20120109
    MSG_FECHA_VCTO_ERROR  = 'La fecha de vencimiento de la garant�a del TAG est� fuera del rango permitido';       //REV.1

var
fechahoraalta,
Ahora : Tdatetime;		// SS_916_PDO_20120109
ContractSerial: Int64;
patente: string;
ContextMark : integer;

begin
    if FEsModificacionFechaAlta then begin                      //REV.1
        if BtnAceptar.ModalResult <> mrOK then begin
            fechahoraalta := DtFechaAlta.Date + DtHoraAlta.Time;
            Ahora := NowBase(DMConnections.BaseCAC);	// SS_916_PDO_20120109
            ContextMark := FContextMark;
            patente := FPatente;
            ContractSerial := FContractSerialNumber;
            if ((fechahoraalta < Ahora) and ((FUltimaBaja = NullDate) or (fechahoraalta > FUltimaBaja))) then begin        // SS_916_PDO_20120109
            //if ValidarFechaHoraAlta (fechahoraalta, patente, ContractSerial, ContextMark) then begin                     // SS_916_PDO_20120109
                BtnAceptar.ModalResult:=mrOK;
                BtnAceptar.Click;
            end else begin
                if fechahoraalta > Ahora then begin                                                                       // SS_916_PDO_20120109
                    MsgBoxBalloon(MSG_FECHA_ALTA_ERROR,'Error',MB_ICONSTOP, BtnAceptar);                                  // SS_916_PDO_20120109
                end                                                                                                       // SS_916_PDO_20120109  
                else begin                                                                                                // SS_916_PDO_20120109         
                    MsgBoxBalloon(FormatDateTime(MSG_FECHA_ALTA_ERROR2, FUltimaBaja),'Error',MB_ICONSTOP, BtnAceptar);    // SS_916_PDO_20120109
                end;
                BtnAceptar.ModalResult := mrNone;
            end;
        end;
    end
    else begin                                  //REV.1
        fechahoraalta := DtFechaAlta.Date + DtHoraAlta.Time;
        if (fechahoraalta < FFechaAltaCuenta) or (fechahoraalta > IncMonth(FFechaAltaCuenta, 12 * fAniosGarantia)) then begin
            MsgBoxBalloon(MSG_FECHA_VCTO_ERROR, 'Error', MB_ICONERROR, DtFechaAlta);
        end
        else ModalResult := mrOk;
    end;                                        //REV.1
end;


function TfrmModificarFechaAlta.Inicializa(patente:String; ContractSerialNumber: Int64; ContextMark: Integer; FechaHoraAlta, FechaHoraUltimaBaja: TDateTime) : Boolean;		// SS_916_PDO_20120109
begin
	//DtFechaAlta.Date :=  NowBase(DMConnections.BaseCAC);		// SS_916_PDO_20120109
	//DtHoraAlta.Time := NowBase(DMConnections.BaseCAC);        // SS_916_PDO_20120109
    DtFechaAlta.Date :=  FechaHoraAlta;							// SS_916_PDO_20120109
    DtHoraAlta.Time := FechaHoraAlta;							// SS_916_PDO_20120109
    FPatente:= trim(patente) ;
    FContractSerialNumber:= ContractSerialNumber;
    FContextMark:= ContextMark;
    FUltimaBaja := FechaHoraUltimaBaja;							// SS_916_PDO_20120109
    //validamos que las variables contengan datos
    if ((FPatente = EmptyStr) or (FContractSerialNumber = 0)) then begin
        Result := False;
    end else begin
        Result := True;
    end;

    FEsModificacionFechaAlta := True;
end;

{-----------------------------------------------------------------
        Inicializa

    Author      : mbecerra
    Date        : 14-Septiembre-2010
    Description :   (Ref SS 921)
                    Indica que la edici�n es de la fecha de vencimiento del Tag
-------------------------------------------------------------------------}
function TfrmModificarFechaAlta.Inicializa(FechaVencimiento: TDateTime; FechaAlta: TDateTime; AniosGarantia: Integer) : boolean;
resourcestring
    MSG_CAPTION     = 'Modificaci�n fecha de vencimiento TAG';
    MSG_LABEL_FECHA = 'Fecha Vto. TAG';
    MSG_LABEL_HORA  = 'Hora Vto. TAG';

begin
    Result                      := True;
    DtFechaAlta.Date            := Trunc(FechaVencimiento);
    FFechaAltaCuenta            := FechaAlta;
    FAniosGarantia              := AniosGarantia;
    DtHoraAlta.Time             := FechaVencimiento;
    Caption                     := MSG_CAPTION;
    lblFechaAlta.Caption        := MSG_LABEL_FECHA;
    lblHoraAlta.Caption         := MSG_LABEL_HORA;
    FEsModificacionFechaAlta    := False;
end;

//Esta Funcion valida que la fecha y hora que se esta escogiendo, no se superponga con una fecha en que
//esa patente o ese televia pudieran estar asignados a otra cuenta
function TfrmModificarFechaAlta.ValidarFechaHoraAlta(Fecha: TdateTime; patente:String; ContractSerialNumber: Int64; ContextMark: Integer): Boolean;
var
    FechaFormateada, FechaHoraActual : string;
    ValorValidado: boolean;
begin
    FechaFormateada := FormatDateTime('yyyymmdd hh:nn',Fecha);
    FechaHoraActual := FormatDateTime('yyyymmdd hh:nn',NowBase(DMConnections.BaseCAC));
    ValorValidado := False;
    TryStrToBool(QueryGetValue(DMConnections.BaseCAC, FORMAT('SELECT DBO.ValidarFechaAltaCuentaPatenteTag (''%s'', %d, %d, ''%s'', ''%s'') ', [trim(patente),ContractSerialNumber,ContextMark, fechaformateada, FechaHoraActual])),ValorValidado);
    if ValorValidado then begin
        Result := True;
    end else begin
        Result := False;
    end;
end;

end.
