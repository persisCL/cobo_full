{-----------------------------------------------------------------------------
  Revision : 1
 	Author: Nelson Droguett Sierra
    Date: 24-04-2009
    Description:En el Ejecutar es opcional el dialogo de configuracion
                (aunque por defecto es TRUE), asi se puede llamar desde
                el mailer sin que salga el dialogo de configuracion.
-----------------------------------------------------------------------------}
unit ReporteNC;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ppDB, ppDBPipe, ppParameter, ppBands, ppModule, raCodMod,
  ppReport, ppSubRpt, ppCtrls, ppPrnabl, ppClass, ppStrtch, ppRichTx, ppCache,
  ppComm, ppRelatv, ppProd,utildb, ADODB,UtilProc, UtilRB, jpeg, Util,ConstParametrosGenerales;

type
  TfrmReporteNC = class(TForm)
    rbiNC: TRBInterface;
    rptNC: TppReport;
    ppParameterList1: TppParameterList;
    ppNC: TppDBPipeline;
    spComprobantes: TADOStoredProc;
    spComprobantesNumeroConvenioFormateado: TStringField;
    spComprobantesNumeroConvenio: TStringField;
    spComprobantesCodigoConvenio: TIntegerField;
    spComprobantesTipoComprobante: TStringField;
    spComprobantesNumeroComprobante: TLargeintField;
    spComprobantesPeriodoInicial: TDateTimeField;
    spComprobantesPeriodoFinal: TDateTimeField;
    spComprobantesFechaEmision: TDateTimeField;
    spComprobantesFechaVencimiento: TDateTimeField;
    spComprobantesNombreCliente: TStringField;
    spComprobantesDomicilio: TStringField;
    spComprobantesComuna: TStringField;
    spComprobantesComentarios: TStringField;
    spComprobantesCodigoBarras: TStringField;
	spComprobantesCodigoPostal: TStringField;
    spComprobantesSaldoPendiente: TStringField;
    spComprobantesAjusteSencilloAnterior: TStringField;
    spComprobantesAjusteSencilloActual: TStringField;
    spComprobantesTotalAPagar: TStringField;
    spComprobantesCodigoTipoMedioPago: TWordField;
    spComprobantesEstadoPago: TStringField;
    sd_NC: TDataSource;
    ppLineasNCDBPipeline: TppDBPipeline;
    dsLineasImpresionNC: TDataSource;
    spObtenerLineasImpresionNC: TADOStoredProc;
    spObtenerLineasImpresionNCDescripcion: TStringField;
    spObtenerLineasImpresionNCImporte: TLargeintField;
    spObtenerLineasImpresionNCDescImporte: TStringField;
    spObtenerLineasImpresionNCCodigoConcepto: TWordField;
    spComprobantesComprobanteQueAnula: TStringField;
    spComprobantesNumeroComprobanteFiscal: TLargeintField;
    spComprobantesNumeroDocumento: TStringField;
    ppHeaderBand1: TppHeaderBand;
    ppImage1: TppImage;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppDetailBand1: TppDetailBand;
    ppLabel1: TppLabel;
    ppdbtNombre: TppDBText;
    ppdbtDir1: TppDBText;
    ppdbtDir2: TppDBText;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppDBText7: TppDBText;
    ppSubReport1: TppSubReport;
    ppChildReport1: TppChildReport;
    ppTitleBand1: TppTitleBand;
    ppDetailBand2: TppDetailBand;
    ppDBText8: TppDBText;
    ppDBText9: TppDBText;
    ppSummaryBand1: TppSummaryBand;
    raCodeModule1: TraCodeModule;
    ppDBText5: TppDBText;
    ppLabel2: TppLabel;
    ppFooterBand1: TppFooterBand;
    raCodeModule2: TraCodeModule;
  private
	{ Private declarations }
    FTipoComprobante: String;
    FNumeroComprobante: Int64;
	FDirImagenFondo: string;
	FNombreImagenFondo: string;
	FTextoVariable: string;

  public
	{ Public declarations }
	FImprimioComprobante: Boolean;
	function Inicializar(Conexion: TADOConnection; TipoComprobante: string; NumeroComprobante: int64; var Error: string): Boolean;
  // Rev.1 / 24-04-2009 / Nelson Droguett Sierra
	//function Ejecutar:boolean; //(PRN_NK, BIN_NK, PRN_DETCON, BIN_DETCON: String): Boolean;
	function Ejecutar(bMostrarDialog:Boolean=True):boolean; //(PRN_NK, BIN_NK, PRN_DETCON, BIN_DETCON: String): Boolean;

  end;

var
  frmReporteNC: TfrmReporteNC;

implementation

{$R *.dfm}

{ TfrmReporteNC }

function TfrmReporteNC.Inicializar( Conexion: TADOConnection; TipoComprobante: string; NumeroComprobante: int64; var Error: string): Boolean;
resourcestring
	MSG_MISSED_IMAGES_FILES = 'No existen todas las im�genes requeridas para '
		+ CRLF + 'poder imprimir una Nota de Cr�dito en el sistema';
	MSG_ERROR = 'Error';
    MSG_ERROR_OBTENCION_DATOS_CONFIG_REPORTE = 'Error al obtener datos de configuraci�n del reporte.'
                                                + CRLF + 'Consulte al Administrador de Sistemas';
    MSG_ERROR_OBTENER_FECHA_CORTE_RUT = 'Ha ocurrido un error al obtener la fecha desde la cual la concesionaria cambio el RUT.';
    MSG_ERROR_OBTENER_RUT_ANTERIOR  = 'Ha ocurrido un error al obtener el RUT Anterior de la concesionaria.';
    MSG_ERROR_OBTENER_RUT           = 'Ha ocurrido un error al obtener el RUT de la concesionaria.';
    MSG_ERROR_OBTENER_TEXTO_VARIABLE_NC = 'Ha ocurrido un error al obtener el Texto Variable de las NC.';
    MSG_RUT                         = 'RUT: ';
var
    FechaCreacion , FechaDeCorte : TDateTime;
begin
	try
		spComprobantes.Connection := Conexion;
		spObtenerLineasImpresionNC.Connection := Conexion;
		result := True;
		FTipoComprobante := TipoComprobante;
		FNumeroComprobante := NumeroComprobante;
         //revision :1
        try
            ObtenerParametroGeneral(Conexion, 'FECHA_DE_CORTE_RUT', FechaDeCorte);
         except
            on E: Exception do begin
                //mensaje de error
                // se supone q se puede consultar el recibo sino no se
                // podria imprimir , VER si salimos o seguimos sin el rut
                MsgBoxErr(MSG_ERROR_OBTENER_FECHA_CORTE_RUT, E.Message, Self.Caption, MB_ICONSTOP);
                Result := False;
                Exit;
            end;
        end;
        try
            FechaCreacion := QueryGetValueDateTime(spComprobantes.Connection,
                ' select FechaCreacion '+
                ' from Comprobantes with (nolock) '+
                ' where(NumeroComprobante='''+ IntToStr(FNumeroComprobante) +''')'+
                       ' and(TipoComprobante = '''+ FTipoComprobante +''')');
        except
            on E: Exception do begin
                //mensaje de error
                // se supone q se puede consultar el recibo sino no se
                // podria imprimir , VER si salimos o seguimos sin el rut
                MsgBoxErr(MSG_ERROR_OBTENCION_DATOS_CONFIG_REPORTE, E.Message, Self.Caption, MB_ICONSTOP);
                Result := False;
                Exit;
            end;
        end;

        if ( FechaCreacion < FechaDeCorte )then begin
            try
                ObtenerParametroGeneral(Conexion, 'IMG_FONDO_NOTA_CREDITO_NC_ANTERIOR', FNombreImagenFondo);
            except
                on E: Exception do begin
                    //mensaje de error
                    // se supone q se puede consultar el recibo sino no se
                    // podria imprimir , VER si salimos o seguimos sin el rut
                    MsgBoxErr(MSG_ERROR_OBTENCION_DATOS_CONFIG_REPORTE, E.Message, Self.Caption, MB_ICONSTOP);
                    Result := False;
                    Exit;
                end;
            end;

        end else begin
             try
                ObtenerParametroGeneral(Conexion, 'IMAGEN_FONDO_NOTA_CREDITO_NC', FNombreImagenFondo);
             except
                on E: Exception do begin
                    //mensaje de error
                    // se supone q se puede consultar el recibo sino no se
                    // podria imprimir , VER si salimos o seguimos sin el rut
                    MsgBoxErr(MSG_ERROR_OBTENCION_DATOS_CONFIG_REPORTE, E.Message, Self.Caption, MB_ICONSTOP);
                    Result := False;
                    Exit;
                end;
             end;
        end;

        try
            ObtenerParametroGeneral(Conexion, 'DIR_IMAGEN_FONDO_NOTA_CREDITO_NC', FDirImagenFondo);
        except
            on E: Exception do begin
                //mensaje de error
                // se supone q se puede consultar el recibo sino no se
                // podria imprimir , VER si salimos o seguimos sin el rut
                MsgBoxErr(MSG_ERROR_OBTENCION_DATOS_CONFIG_REPORTE, E.Message, Self.Caption, MB_ICONSTOP);
                Result := False;
                Exit;
            end;
        end;

        try
            ObtenerParametroGeneral(Conexion, 'TEXTO_VARIABLE_NC', FTextoVariable);
         except
            on E: Exception do begin
                MsgBoxErr(MSG_ERROR_OBTENER_TEXTO_VARIABLE_NC, E.Message, Self.Caption, MB_ICONSTOP);
                Result := False;
                Exit;
            end;
        end;






//		ObtenerParametroGeneral(Conexion, 'NOMBRE_IMAGEN_FONDO_NOTA_CREDITO_NC', FNombreImagenFondo);
        //fin revision :1

		FDirImagenFondo := GoodDir(FDirImagenFondo);
		FNombreImagenFondo := FDirImagenFondo + FNombreImagenFondo;
		// Fondo
		if (not FileExists(FNombreImagenFondo)) then begin
			raise Exception.Create(MSG_MISSED_IMAGES_FILES);
		end;
	except
		on e:Exception do begin
			result := false;
			Error := e.message;
		end;
	end;

end;

// Rev 1. / 24-04-2009 / Nelson Droguett Sierra
// function TReporteNotaCreditoElectronicaForm.Ejecutar: boolean;
function TfrmReporteNC.Ejecutar(bMostrarDialog:Boolean=True): boolean;
begin
	ppimage1.Picture.LoadFromFile(FNombreImagenFondo);

    ppLabel1.Caption := FTextoVariable;
	spComprobantes.Close;
	spComprobantes.Parameters.ParamByName('@TipoComprobante').Value:= FTipoComprobante;
	spComprobantes.Parameters.ParamByName('@NumeroComprobante').Value:= FNumeroComprobante;
	spComprobantes.Open;

	spObtenerLineasImpresionNC.Close;
	spObtenerLineasImpresionNC.Parameters.ParamByName('@TipoComprobante').Value:= FTipoComprobante;
	spObtenerLineasImpresionNC.Parameters.ParamByName('@NumeroComprobante').Value:= FNumeroComprobante;
	spObtenerLineasImpresionNC.Open;

	rbiNC.Execute(bMostrarDialog);
    result := True;
end;

end.
