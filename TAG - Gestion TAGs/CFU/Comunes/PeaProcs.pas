{********************************** File Header ********************************
File Name   : PeaProcs
Author      :
Date Created:
Language    : ES-AR
Description :

Revision 1
Author      :
Date Created:
Language    : ES-AR
Description : En ObtenerNumeroSiguienteComprobante se hace un raise si dio error
as� el m�todo llamante puede dar una excepcion

Revision 2
Author      : jconcheyro
Date Created: 14/12/2006
Language    : ES-AR
Description : creo CargarMotivosMovimientosCuentasTelevias , ObtenerUbicacionActualTagPorNumero

Revision 3
Author      : jconcheyro
Date Created: 18/12/2006
Language    : ES-AR
Description : Paso VerificarCotizacionesDelDia a este modulo

Revision 4
Author      : jconcheyro
Date Created: 26/01/2007
Language    : ES-AR
Description : elimino CargarMotivosMovimientosCuentasTelevias porque se reemplazo con un form

Revision 5
Author      :Fsandi
Date Created: 06/02/2007
Languaje    : ES-AR
Description :   Se modific� la funci�n CaracteresIngresadosSonDePatenteChilena para permitir el control del
                nuevo formato de patentes.

Revision : 7
    Author : nefernandez
    Date: 20/06/2007
    Description : Le agregu� un par�metro a la funci�n ConsultarPatenteRNVM, para
    retornar el string del XML de respuesta

Revision : 8
    Author : jconcheyro
    Date: 27/06/2007
    Description : en la funcion ArmarNombrePersona ahora uso QuotedStr porque apellidos con ' generaban error

Revision : 9
    Author : nefernandez
    Date: 01/11/2007
    Description : Se agrega la funci�n para agregar el log de acciones por sistema
Revision : 10
    Author : lcanteros
    Date: 12/06/2008
    Description : Se agrega la funci�n para eliminar los caracteres que no son soportados
    por el sistema en las consultas al RNVM (SS 704)
Revision : 11
    Author : lcanteros
    Date: 15/07/2008
    Description : Se agrega la funci�n para obtener el timepo de time out de las
    consultas realzadas desde el modulo de gesti�n de informes
    (ObtenerTiempoConsultaReporte)

Revision : 12
    Author : dAllegretti
    Date: 15/10/2008
    Description : Se agrega la funcionalidad de convenios sin cuentas


Resvision : 13
   Author : Svigay
   Date: 21/10/2008
   Description: SS 744
		-Se agrego un funcion decifrar para encriptar contrase�a en
   el conexion String de informes

Revision : 14
   Author : nefernandez
   Date: 03/12/2008
   Description: Se cambia el nombre de la funci�n "RegistrarLogSistemaAcciones" por
   "RegistrarLogAuditoriaAcciones" para unificar el cambio del nombre del store y
   la tabla involucrados

Revision : 15
   Author : mpiazza
   Date: 03/12/2008
   Description: Se agrega funcion MensajeTipoCliente, que busca mensaje segun
   RUT y sistema busca segun tipo de cliente

Revision : 16
   Author : mpiazza
   Date: 03/03/2009
   Description: se agrega procedimiento de carga de empresas de recaudacion y
   tipo deuda en un variantcombobox

Revision : 17
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura

Revision : 17
   Author : rharris
   Date: 06/04/2009
   Description: se agrega en la funcion DescriTipoComprobante los comprobantes electr�nicos
Revision : 19
  Author: mpiazza
  Date: 04/06/2009
  Description: SS-803- se le a�ade un parametro de filtro opcional por campo judicial

Revision : 20
    Author : pdominguez
    Date   : 08/06/2009
    Description : SS 809
            - Se crearon los procedimientos CargarTiposTarjetasCreditoVentanilla y
            CargarBancosPagoVentanilla.
Revision : 20
  Author: mpiazza
  Date: 22/06/2009
  Description: SS-803- se agregan dos funciones CargarEmpresaRecaudacionJudicial y
               CargarEmpresaRecaudacionPreJudicial despues de que se agrego el flag prejudicial

Revision : 21
  Author: pdominguez
  Date: 08/07/2009
  Description: SS 784
        - Se implementa la funci�n ObtenerDatosPersonales.
Revision : 24
    Author : pdominguez
    Date   : 17/09/2009
    Description : SS 809
        - Se modificaron/a�adieron los siguientes procedimientos/funciones:
            CargarBancosPAC
Revision : 26
    Author : pdominguez
    Date   : 27/11/2009
    Description : SS 719
        - Se modificaron/a�adieron los siguientes procedimientos/funciones:
            ExistePatente
            VerificarFormatoPatenteChilena
Revision : 27
    Author : mpiazza
    Date   : 28/01/2010
    Description : SS-510
        - se modifico ObtenerDatosTurno agregando el campo IdEstacion

Revision : 28
    Date: 29/01/2010
    Author: mpiazza
    Description:  Ref SS-510 Se agrega una validacion por IP en
                  procedimiento VerTurnoAbierto(...

Revision : 29
    Date: 09/05/2010
    Author: pdominguez
    Description: Infractores Fase 2
        - se modifica el procedimiento CargarComboMotivoAnulacion.
        - Se crea el procedimiento CargarComboConcesionarias.
        - Se crea la funci�n AsignarValorComboEstado.

Revision : 30
    Date: 12-Mayo-2010
    Author: Nelson Droguett Sierra
    Description: Infractores Fase 2
        - Se crea el procedimiento CargarComboTiposConceptosMovimientos.

Revision : 31
    Date: 25/03/2010
    Author: jjofre
    Description:  (Ref SS 800-676)
                Se agrega el objeto TADOstoredProc spValidaConvenioInfraccionesImpagas
                a la funcion MensajeRUTConvenioCuentas, este stored retorna true
                si el convenio no posee cuentas con infracciones pendientes de regularizar.

Revision : 32
Date: 22/04/2010
Author: jjofre
Description:  (Ref SS 800-676)
                 Se modifica la funcion MensajeRUTConvenioCuentas
                 para que muestre la patente en caso de tener 1 cuenta con infracciones
                 si hay mas de 1 cuenta con infracciones, se muestra un mensaje generalizado

Revision 	: 33
Date		: 23-Junio-2010
Author		: Nelson Droguett Sierra
Description	: (Ref. Fase 2)
        	  - Se crea el procedimiento CargarComboTiposOrdenServicio.
              - Se crea el procedimiento CargarConcesionariasReclamo
              - Se crea el procedimiento CargarSubTiposReclamo

Revision	: 31
Auhor		: Nelson Droguett Sierra
Date		: 27-Julio-2010
Description	: En la funcion ValidateControls, luego de fallar la condicion muestra un mensaje Balloon
              advirtiendo el error, y luego le da el foco al control que fall� la condicion, pero si
              el control esta inactivo se cae el programa. Ahora solo cambia el foco cuando el control
              esta disponible, en caso contrario no har� el cambio de foco.

Revision	: 32
Auhor		: Jose Jofre
Date		: 12-Agosto-2010
Description	: (ref. SS 690 Semaforos)
                Se Modifica la funcion MensajeTipoCliente:
                    - Para que retorne todos los mensajes.
                    - Se identa de forma adecuada.

Revision	: 32
Auhor		: Nelson Droguett Sierra
Date		: 20-Abril-2011
Description	: Ordenar los combos alfabeticamente
Firma       : PAR00133-NDR-20110420

Revison		: 33
Author		: Nelson Droguett Sierra
Date		: 17-Agosto-2010
Description	: (Ref. SS 601-715)
              Se genera combo para elegir Pie de Firma en el ABM de Plantillas.

Revision    : 34
Author      : pdominguez
Date        : 06/10/2010
Description : SS 927 - CAC - Mensaje de patentes infractoras no corresponde
        - Se modific� la funci�n MensajeRUTConvenioCuentas

Revision    : 35
Author      : jdebarbieri
Date        : 03/11/2010
Description : SS 931 - Validacion - Mensaje de patentes en seguimiento
        - Se crea la funci�n EsPatenteEnSeguimiento
Revision    : 38
Author      : mbecerra
Date        : 13-Enero-2011
Description :   (Ref SS 948)
                Se crea la nueva funci�n NumeroAPalabras, la cual recibe
                un n�mero entero y lo convierte a palabras

Revision    : 33
Author      : Nelson Droguett Sierra
Date        : 24-Junio-2011
Description : Se corrige que los combos de concesionarias filtren como se espera y que segun la solapa
            en que esten desplieguen la concesionaria segun el tipoconcesionaria
Firma       : PAR00133-FASE2-NDR-20110624


Revision    : 41
Author      : Alejandro Labra
Date        : 11-abril-2011
Firma       : Rev.960 SS Ventana de mensajes de TAGs vencidos
Description : Se agrego llamada a ventana TVentanaAvisoForm, la cual se muestra en caso
              de encontrar TAGs con fecha de garant�a vencida.
Firma       : SS-960-ALA-20110411

Revision    : 42
Author      : Alejandro Labra
Date        : 23-04-2011
Firma       : Rev.960 SS Ventana de mensajes de TAGs vencidos
Description : Se arregla problema cuando se consulta un rut que no tiene
              tags vencidos y la ventana igual se mostraba.
              En este sentido se edita procedimiento "MostrarTagsVencidos",
              la cual pregunta si se puedo inicializar TVentanaAvisoForm, si es
              as�, se muestra la ventana, en caso contrario finaliza.
Firma       : SS-960-ALA-20110423

Revision	: 43
Author		: mbecerra
Date		: 17-Junio-2011
Description	:		(Ref SS 959)
            	Se agrega el comprobante de devoluci�n de dinero
Firma		: SS959-20110617-MBE

Firma		: SS_740_MBE_20110720
Description	:   Se modifica la funci�n DescriTipoComprobante para que retorne la
                descripci�n de los Tipos TC y TD (Traspaso)

Description : Se agrega nueva funci�n "VerificarExisteConvenioSinCuenta"
              la cual verifica si ya existe un ConvenioSinCuenta para este
              Cliente.
              Adem�s se modifica funci�n "ObtenerConvenioMedioPago", para que
              agregue si el convenio que se est� ingresando corresponde a un
              ConvenioSinCuenta, as� el store tomar� en cueta o no los
              ConveniosSinCuenta
Firma       : SS_628_ALA_20110720

Descripcion : Se agrega propiedad ConeccionDatosBase a la clase TReportItem, para
              obtener los datos del formulario TFormRptInformeUsuario.
              Estos datos se pasar�n en el m�todo CargarInformes con el primer
              par�metro
Firma       : PAR00133_COPAMB_ALA_20110801

Description : Se modifica funci�n NoPermitirMedioPAgoDuplicado, por cambio en
              store VerificarMedioPagoAutomatico, el cual ahora recive menos
              par�metros debido a que se debe validar que no exista un convenio
			 con el mismo medio de pago.
Firma       : SS_628_ALA_20110818

Firma       : SS_1012_ALA_20111128
Description : Se crean las funciones ObtenerPorcentajeIVA, CalcularIVADeImporte,
              CalcularImporteNetoDeImporte, RoundCS.

Firma       : SS_628_ALA_20111219
Description : Se actualiza funcion VerificarExisteConvenioSinCuenta, para incluir
              la posibilidad que se excluya de la verificaci�n alg�n convenio,
              para verificar si existe alg�n otro Sin Cuenta.

Firma       : SS_1015_HUR_20120409
Descripcion : Se agrega procedimiento CargarComboOtrasConcesionarias, para incluir
              la posibilidad que solo incluya otras concesionarias.

Firma       : SS_1015_HUR_20120413
Descripcion : Se agregan los tipos de comprobante Saldo Inicial OC (SI) y Anula Saldo Inicial (AS)

Firma       : SS_1015_HUR_20120417
Descripcion : Se modifican los tipo de comprobante de Saldo Inicial

Firma       : SS-1015-NDR-20120510
Descripcion : Se agregan los tipos de comprobante Cuotas Otras Concesionarias (OC)
            y Anula Cuotas Otras Concesionarias (AC)

Firma       : SS_1047_CQU_20120528
Descripcion : Se agrega funcionalidad para cargar los combos de Gestion Judicial

Firma       :   SS_1120_MVI_20130820
Descripcion :   Se modifica el procedimeinto CargarConveniosRUT para identificar
                los convenios que est�n de baja y sin cuenta. Para esto se cambia el SP
                ObtenerConvenios por el SP ObtenerConveniosCliente, ya que trae la informaci�n
                necesaria para el llenado de los combos.
                Adem�s se crea el procedimeinto , encargado de pintar los convenios
                de baja. Este debe ser llamado desde los eventos DrawItem.

Firma       :   SS_1118_MVI_20131007
Descripcion :   Se agrega TRIM en el procediemiento CargarConveniosRUT, cuando va
                a buscar los datos del cliente.

Firma       :   SS_925_NDR_20131122
Description :   Genera nuevos menu segun campo GenerarMenues de tabla SistemasVersiones (no del .ini)

Firma       :   SS_1189_MCA_20140929
Descripcion :   se agrega validacion a la funcion CaracteresIngresadosSonDePatenteChilena
                para que valide el nuevo formato de patentes de motos


Firma		: 	SS_1147_MCA_20140303
Descripcion	:	se agrega nueva funcion ObtenerCodigoConcesionariaNativa.
              Se obtiene el nombre corto de la concesionaria nativa

Firma       : SS_1147_NDR_20140603
Descripcion : Se corrige el llenado del combo del punto de venta para solucionar un problema en el filtro
              de los cierres de turno (Observaciones Q&A 1147)

Firma       : SS_1147_CQU_20140731
Descripcion : Se agrega funci�n que cambia el color de los botones

Firma       : SS_925_NDR_20141202
Desription  : Se modifica para que el procedure GrabarComponentesObligatoriosyHints obtenga el dato GenerarMenues
              desde la base de datos y no desde el .INI de la aplicacion

Firma       : SS_1147Q_NDR_20141202
Description : Funcion LargoNumeroConvenioConcesionariaNativa
              CN=17
              VS=12

Firma       :   SS_1147_CQU_20150316
Descripcion :   Se agrega la funcion ObtenerDocumentoInterno para obtener el NumeroComprobante y TipoComprobante
                Seg�n el NumeroComprobanteFiscal y TipoComprobanteFiscal

Firma       :   SS_1147_MBE_20150413
Description :   Se agrega el tipo de comprobante, ya que la deuda puede originarse por NK, SD o TD impago.
                Adem�s, se corrigen errores de programaci�n

Firma       :   SS_1255_MCA_20150505
Descripcion :   se deja predeterminada la concesionaria nativa para los reclamos

Firma       :   SS_1147_CQU_20150511
Descripcion :   Se agrega una validaci�n de si en numerico el valor de TipoComprobanteFiscal
                en la funcion ObtenerDocumentoInterno ya que Transbank es un proceso ESPECIAL

Firma       : SS_1397_MGO_20151027
Description : Se agrega funci�n ObtenerCodigoConcesionariaVS para obtener el c�digo de VS

Firma		: SS_1397_MGO_20151028	
Description	: Se agrega par�metro opcional de Conexi�n a ObtenerCodigoConcesionariaVS y ObtenerCodigoConcesionariaNativa

Firma       : SS_1408_MCA_20151027
Descripcion : se agrega funcion MensajeACliente el cual valida si debe mostrar el mensaje al cliente ingresado
            ademas obtiene el mensaje desde parametros generales.


Firma       : SS_1437_NDR_20151230
Descripcion : Que los menus de los informes se carguen segun los permisos del usuario, y que dependan del GenerarMenues del Sistema

Etiqueta	: 20160307 MGO
Descripci�n	: Se cambia conexi�n a BaseBO_Master para SP relacionados a usuarios y permisos.
				Se usa conexi�n a BaseBO_Master para comprobar la versi�n de los aplicativos.

Etiqueta    : 20160414 MGO
Descripci�n : No se registra "logout" cuando se cancel� el inicio de sesi�n, evitando error de conexi�n
--------------------------------------------------------------------------------}
unit PeaProcs;

interface

Uses
	Classes, Windows, Messages, SysUtils, PeaTypes, Menus, Util, UtilProc, DB,
	DBTables, UtilDB, Graphics, WinSock, Forms, Controls, JPeg, ADODB,
	StdCtrls, Variants, ComCtrls, StrUtils, dialogs, DMConnection, Math,
    VariantComboBox, ShlObj, DateUtils, RStrings,
	MaskCombo, ShellAPI, RVMClient, RVMTypes, ConstParametrosGenerales,
    Registry, DMComunicacion, DBListEx, ListBoxEx,EncriptaRijandel,
    httpApp, DBClient,
    frmVentanaAviso, AvisoTagVencidos,     //Mensaje de tags Vencidos SS-960-ALA-20110411
    frmMensajeACliente,				//SS_1408_MCA_20151027
    PeaProcsCN,
    SysUtilsCN,
    xmldom, XMLIntf, msxmldom, XMLDoc, Diccionario, ClaveValor;

const
    APOSTROFE = '�';
    SP_VALIDA_PATENTE = 'VerificarFormatoPatente';


resourcestring
    SIN_ESPECIFICAR = '(Sin especificar)';
    SELECCIONAR = '(Seleccionar)';
    TODOS = '<Todos>';                                                          //SS_1120_MVI_20130820

type
    // Declaro un tipo de item especial para el manejo de los reportes.
	TReportItem = class(TMenuItem)
    private
		FCodigo                 : Integer;
        //FConnection             : TADOConnection;                             //SS_979_ALA_20110929
        FConnectionDatosBase    : TADOConnection;                               //PAR00133_COPAMB_ALA_20110801
    protected
        procedure MyItemClick(Sender: TObject);
        procedure SetCodigo(const Value: Integer);
        //procedure SetConnection(const Value: TADOConnection);                 //SS_979_ALA_20110929
        procedure SetConnectionDatosBase(const Value: TADOConnection);          //PAR00133_COPAMB_ALA_20110801
    public
	    constructor Create(AOwner: TComponent); override;
		destructor Destroy; override;
		property Codigo: Integer read FCodigo Write SetCodigo;
		//property Connection: TADOConnection read FConnection Write SetConnection;                               //SS_979_ALA_20110929
        property ConnectionDatosBase : TADOConnection read FConnectionDatosBase write SetConnectionDatosBase;    //PAR00133_COPAMB_ALA_20110801
	end;

{******************************** Function Header ******************************
Function Name: SetCodigo
Author :
Date Created : 07/09/2006
Description :
Parameters : const Value: Integer
Return Value : None
Revision :
    Author : vpaszkowicz
    Date : 07/09/2006
    Description : Se agreg� la variable FCodigo a la clase TCategoriaReportItem
    con setter y getter
*******************************************************************************}

	TCategoriaReportItem = class(TMenuItem)
        private
            // C�digo de la Categor�a.
            FCodigo: Integer;
        protected
            procedure SetCodigo(const Value: Integer);
        public
            constructor Create(AOwner: TComponent; CodigoCategoria: Integer); reintroduce;
            destructor Destroy; override;
            property Codigo: Integer read FCodigo Write SetCodigo;
	end; // TCategoriaReportItem


    // Declaro un tipo de item especial para el manejo de las intefaces.
	TInterfaceItem = class(TMenuItem)
	private
		FCodigo: Integer;
		FConnection: TADOConnection;
	protected
		procedure MyItemClick(Sender: TObject);
		procedure SetCodigo(const Value: Integer);
		procedure SetConnection(const Value: TADOConnection);
	public
		constructor Create(AOwner: TComponent); override;
		destructor Destroy; override;
		property Codigo: Integer read FCodigo Write SetCodigo;
		property Connection: TADOConnection read FConnection Write SetConnection;
	end;

    // Este Tipo de datos es para representar los permisos "adicionales" que no
    // corresponden a �tems del men�
    TPermisoAdicional = record
        Funcion: AnsiString;
        Descripcion: AnsiString;
        Nivel: Integer;
    end;

// Rutinas Generales
function  ArmarCondicionWhereMultipleFiltro (lista: ANSIString; nombreCampo: AnsiString): AnsiString;
function  ExisteCadenaEnStringList(STL: TStringList; Cadena: AnsiString): boolean;
function  PrimeraLetraMayuscula(Texto: AnsiString; elRestoMinuscula: boolean = false): AnsiString;
function  FechaNacimientoValida(Fecha: TDateTime; edadMinima: Integer = 0): boolean;
function  DateTimeToDWord(FechaDate: TDateTime): DWORD;
function  EsDiaFeriado(Conn: TADOConnection; Fecha: TDateTime; var Descripcion: AnsiString): Boolean;
function  EsDiaHabil(Conn: TADOConnection; Fecha: TDateTime): Boolean;
function EsDomingoOSabado(Fecha: TDateTime): Boolean;
function  DWordToDateTime(FechaWord: DWORD): TDateTime;
function  DescriIncidencia(Codigo: Integer; Detalle: AnsiString): AnsiString;
function  DescriPais(Conn :TADOConnection; CodigoPais: AnsiString): String;
function  DescriTipoComprobante(TipoComprobante: AnsiString):AnsiString;
procedure CargarTipoComprobante(Combo: TComboBox; TipoComprobante: AnsiString = TC_FACTURA);
procedure CargarMediosPagoAutomatico(Combo: TVariantComboBox; TipoPagoAutomatico: Integer = -1; TieneSeleccione : Boolean = True; TieneSinEspecificar : Boolean = False);
function  DescriEstadoComprobante(TipoEstado: AnsiString):AnsiString;
function  TextoDiferenciaFecha(Fecha: TDateTime): AnsiString;
function  BuscarDescripcionTipoPago(Connection :TADOConnection; TipoPago: AnsiString):AnsiString;
function  BuscarDescripcionTipoDebito(Connection :TADOConnection; TipoDebito: AnsiString):AnsiString;
function  ObtenerNombreDiaSemana(const aFecha: TDateTime): AnsiString;
function  ValorDanio(Conn: TADOConnection; EstadoConservacionTAG: integer): Integer;
function  ValorSoporte(Conn: TADOConnection): Integer;
procedure CargarCanalesDePago(Conn: TADOConnection; Combo: TVariantComboBox; CodigoItemSeleccionar: Integer; CanalesRefinanciacion: Integer = -1);
procedure CargarFormasPago(Conn: TADOConnection; Combo: TVariantComboBox; CodigoCanalDePago: Byte);
procedure CargarImpresorasFiscales(Conn: TADOConnection; Combo: TVariantComboBox; CodigoImpresoraFiscal: Integer);
function  MensajeRUTConvenioCuentas(Conn: TADOConnection; NumeroDocumento: AnsiString; SoloActivos: Boolean = False): AnsiString;
procedure ReemplazarCaracteresNoSoportados(var Texto: string);
procedure ObtenerTiempoConsultaReporte(Conexion: TADOConnection ; var Tiempo: integer);
function ObtenerNumeroConvenioSinConcesionaria(Numero: string): string;
function LargoNumeroConvenioConcesionariaNativa(): Integer;                                       //SS_1147Q_NDR_20141202

procedure EstaClienteConMensajeEspecial(Conn: TADOConnection; NumeroDocumento: AnsiString);               //SS_1408_MCA_20151027
//function  BuscarDescripcionCuenta(Connection:TADOConnection; Cuenta: Integer): AnsiString;
procedure CargarDocumentacion(Conn: TADOConnection; var ListaOrdenServicio:ATOrdenServicio;OrdenServicio:Integer;Personeria:String;PatenteVehiculo:String='';IndiceVehiculo:Integer=-1);
//function BuscarIndexOrdenServicio(var ListaOrdenServicio:ATOrdenServicio;CodigoOrdenServicio:Integer; Patente: String = ''):integer;
procedure QuitarDocumentacion(var ListaOrdenServicio:ATOrdenServicio;OrdenServicio:Integer; Patente: String = '');
function  BuscarDatosVehiculo(Connection:TADOConnection; Cuenta: Integer): AnsiString;
function ArmarNombrePersona(Personeria,Nombre,Apellido,ApellidoMaterno:String):String;
function ArmarNombreGenerico(Personeria,Nombre,Apellido,ApellidoMaterno,NombreContactoComercial,ApellidoContactoComercial,ApellidoMaternoContactoComercial:String):String;
function  BuscarApellidoNombreCliente(Conection :TADOConnection; CodigoCliente: Integer; NumeroDocumento : String = ''): AnsiString;
function  BuscarCategoriaVehiculo (Connection :TADOConnection; CodigoCuenta:LongInt; Var Factortarifa: Double): AnsiString;
function  ObtenerDescripcionDebito(Connection: TADOConnection; TipoDebito: AnsiString; CodigoEntidad: Integer; CuentaDebito: AnsiString): AnsiString;
function  ObtenerDescripTipoVehiculo(Connection: TADOConnection; CodigoTipoVehiculo:Integer; var DescripTipo:String;var DescripCategoria:String ;var Categoria:String):boolean;
function  ExisteCliente(Connection :TADOConnection; CodigoCliente: Integer): Boolean;
function  ConectarRecursoImagenes(Password, Usuario: PChar; Path: AnsiString; var DescriError: AnsiString): Boolean;
function  ObtenerEstadoOC(Estado:Byte): AnsiString;
//procedure RefrescarBarraDeEstado(StatusBar: TStatusBar; Sistema: Byte);
function  ObtenerDescripcionAptitudCalidad(Estado: string):AnsiString;
function  ObtenerNombreArchivoFactura(Conn: TADOConnection; TipoCOmprobante: AnsiString; NumeroComprobante: Double): AnsiString;
function  ObtenerFormatoFechaSQL(Conn: TADOConnection): AnsiString;
function  DescripcionTolerancia(medida: integer; unidad: AnsiString): AnsiString;
function  DescripcionDescuento (medida: double; unidad: AnsiString): AnsiString;
function  DescripcionParametroPromocion(TipoParametro: AnsiString): AnsiString;
function  EsAnioCorrecto(anio: integer): boolean; //para los vehiculos
function  ObtenerTipoContactoComunicacion(Conn:TADOConnection; Comunicacion: integer): AnsiString;
function  ObtenerTipoFuenteDeFuenteSolicitud(Conn:TADOConnection; CodigoFuenteSolicitud: integer): AnsiString;
function  BuscarDescripcionRegion(Connection:TADOConnection; pais, region: AnsiString): AnsiString;
function  BuscarDescripcionComuna(Connection:TADOConnection; pais, region, comuna: AnsiString): AnsiString;
function  BuscarDescripcionCiudad(Connection:TADOConnection; pais, region, ciudad: AnsiString): AnsiString;
function  ArmaNombreCompleto(Apellido, ApellidoMaterno, Nombre: AnsiString): AnsiString;
function  ArmaRazonSocial(Apellido, Nombre: AnsiString): AnsiString;
function ObtenerDescripCalle(Connection :TADOConnection;
  CodigoPais, CodigoRegion, CodigoComuna: AnsiString; CodigoCalle: Integer = 0; CalleDesnormalizada: AnsiString = ''):String;
function  ArmarDomicilioSimple(Connection :TADOConnection; calle: AnsiString; numero, piso: integer; dpto, detalle: AnsiString): AnsiString; overload;
function  ArmarDomicilioSimple(Connection :TADOConnection; calle: AnsiString; numero:AnsiString; piso: integer; dpto, detalle: AnsiString): AnsiString; overload;
function  ArmarDomicilioCompleto(Connection :TADOConnection; calle: AnsiString; numero, piso: integer; dpto, detalle: AnsiString; ciudad, comuna, region: AnsiString): AnsiString; overload;
function  ArmarDomicilioCompleto(Connection :TADOConnection; calle: AnsiString; numero:AnsiString; piso: integer; dpto, detalle: AnsiString; ciudad, comuna, region: AnsiString): AnsiString; overload;
function  ArmarDomicilioCompletoPorCodigo(Conn :TADOConnection; CodigoPersona, CodigoDomicilio: integer): AnsiString;
function  ArmarUbicacionGeografica(Connection :TADOConnection; CodigoPais, CodigoRegion, CodigoComuna:AnsiString;CodigoCiudad:AnsiString='';DescripcionCiudad:AnsiString=''): AnsiString;
function  FormatearNumeroConvenio(Numeroconvenio:String):String;
function  FormatearNumeroRut(RUT:String):String;
function ValidarUbicacionTelevia(Conn :TADOConnection; ContextMark: integer; SerialNumber: DWORD; CodigoPuntoEntrega: integer): boolean;
function  FormatearImporte(Conn: TADOConnection; Importe:Int64):String;
function  FormatearImporteConDecimales(Conn: TADOConnection; Importe:Double; ConSimboloMoneda:boolean; Con2Decimales:boolean; ConSeparadorDeMiles:boolean):String;

function RedondearINTConCentavosHaciaArribaBase( Conn: TADOConnection; Importe:Int64): Int64;
function RedondearINTConCentavosHaciaAbajoBase( Conn: TADOConnection; Importe:Int64): Int64;

function NumeroAPalabras(Numero : LongInt) : string;        //SS 948 20110113

function  ImporteStrToInt(Conn: TADOConnection; Importe: AnsiString): Int64;
function  NoCargoDomicilio(calle, numero, comuna: AnsiString): boolean;
function  ObtenerLargoVehiculo(Conn: TADOConnection; marca, modelo, anio: integer): integer;
//function  BuscarDescripcionCategoriaVehiculo(Connection:TADOConnection; Marca, Modelo: integer; var categoria: integer): AnsiString;
procedure QuitarDebitoAutomaticoDecuenta(Connection:TADOConnection; cuenta: integer);
procedure MarcarItemComboVariant(var Combo: TVariantComboBox; Codigo: integer; TieneItemNinguno: boolean = true); overload;
procedure MarcarItemComboVariant(var Combo: TVariantComboBox; Codigo: AnsiString; TieneItemNinguno: boolean = true); overload;
function  ObtenerCantidadPasesPatenteAnioActual(Connection:TADOConnection;
            patente: AnsiString; anio: smallint): smallint;
function ObtenerDigitoVerificadorPatente(Connection:TADOConnection; Patente: AnsiString): AnsiString;
function  ObtenerCodigoDomicilioEntrega(codigoPersona: integer): integer;
function  ObtenerCodigoDomicilioPersona(codigoPersona, codigotipodomicilio: integer): integer;
function  ObtenerValorMediosComunicacion(codigopersona, codigomediocontacto, codigotipodomicilio: integer): string;
function  EsDomicilioEntrega (codigopersona, codigodomicilio: integer): boolean;
function  ExisteCiudadComuna (Pais, Region, Comuna, Ciudad: AnsiString): boolean;
procedure LimpiarRegistroDomicilio(var datos: TDatosDomicilio; tipoDomicilio: integer);
function ArmarComuna(Connection :TADOConnection; CodigoDomicilio: AnsiString): AnsiString;
function ArmarRegion(Connection :TADOConnection; CodigoDomicilio: AnsiString): AnsiString;
function ArmarCiudad(Connection :TADOConnection; CodigoDomicilio: AnsiString): AnsiString;
procedure ObtenerMensaje(Conn :TADOConnection; CodigoMensaje: integer; var tituloMensaje, descripcion, comentario: AnsiString);
function FormatearNumeroCalle(NumeroCalle:String):Integer;
function ArmarDescripcionDatosVehiculo(CodigoMarca,CodigoModelo:Integer;CodigoColor:Integer=-1):String; overload;
function ArmarDescripcionDatosVehiculo(Marca,Modelo:String;Color:String=''):String; overload;
function NumeroCalleNormalizado(Conn:TADOConnection ;CodigoCalle:integer;CodigoRegion,CodigoComuna:String;Numero:Integer):Boolean;
function ObtenerConvenioVehiculo(Coon:TADOConnection;TipoPatente,Patente:String):String;
function ObtenerCodigoPersona(Conn: TADOConnection; NumeroDocumento:String;TipoDocumento:String='RUT'):Integer;
function ObtenerDatosTurno(Conn: TADOConnection; CodigoPuntoEntrega: integer; CodigoPuntoVenta: integer; var DatosTurno: TDatosTurno):boolean;
function VerificarPuntoEntregaVenta(Conn: TADOConnection;PuntoEntrega: integer; PuntoVenta: integer):boolean;
function VerTurnoAbierto(Conn: TADOConnection; CodigoPuntoEntrega: integer; CodigoPuntoVenta: integer; CodigoUsuario: string; var DatosTurno: TDatosTurno):boolean;
function ActualizarMontoTurno(conn: TADOCOnnection; CodigoTurno: Integer; Monto: Double): Boolean;
function ObtenerDescripcionBanco(Conn: TADOConnection; CodigoBanco:Integer):String;
function ObtenerDescripcionActividad(Conn: TADOConnection; CodigoActividad:Integer):String;
function ObtenerCuentasBancarias(Conn: TADOConnection; Codigocuenta:Integer):String;
function ObtenerTiposTarjetasCreditos(Conn: TADOConnection; CodigoTipoTarjetaCredito:Integer):String;
function ObtenerStockAlmacen(Conn: TADOConnection; CodigoAlmacen: integer; Categoria: integer): integer;
function ValidarStockAlmacen(Conn: TADOConnection; CodigoAlmacen: integer; Categoria: integer): boolean;
function ObtenerCategoriaVehiculo(Conn: TADOConnection; CodigoTipoVehiculo:Integer):Integer;
function ObtenerDuenioTagVendido(Conn: TADOConnection; ContextMarks: Integer; ContractSerialNumber: DWord): Integer;
function ObtenerAlmacenPuntoEntrega(Conn: TADOConnection; CodigoPuntoEntrega: integer): integer;
function ValidarVersion(Conn: TADOConnection; CodigoSistema:Integer): Boolean; overload; //SS_925_NDR_20131122
function ObtenerEmisoresTarjetasCredito(Conn: TADOConnection; CodigoEmisorTarjetaCredito:Integer):String;
function ObtenerDescripcionPuntoEntrega(conn: TADOConnection; CodigoPuntoEntrega : Integer) : String;
function TagEnCliente(conn: TADOConnection; CodigoPersona: integer; SerialNumber:DWORD; ConvenioaExcluir: integer = -1): boolean;
function ObtenerCodigoConceptoPago(Conn: TADOConnection; CodigoCanalPago: Integer): Integer;
// Revision 14
function RegistrarLogAuditoriaAcciones(Conn: TADOConnection; CodigoSistema: Integer; Pantalla, Accion, Usuario, EstacionTrabajo, Observacion: String; CodigoInfraccion: Integer): Boolean;

// Carga de Combos (documentos, sexos, colores, etc.)
function IndexCombo(Combo: TComboBox; Referencia: String) : Integer;
procedure CargarTiposSueldo(Conn: TADOConnection; Combo: TComboBox; CodigoTipoSueldo: integer = 0);
{INICIO: 	20160720 CFU
procedure CargarAlmacenes(Conn: TADOConnection; Combo: TVariantComboBox; CodigoAlmacen: Integer = 0; todos : Boolean = true);
}
procedure CargarAlmacenes(Conn: TADOConnection; Combo: TVariantComboBox; Condicion: string; CodigoAlmacen: Integer = 0; todos : Boolean = true);
//TERMINO: 20160720
procedure CargarMaestroAlmacenes(Conn: TADOConnection; Combo: TComboBox; CodigoAlmacen: integer = 0; TieneItemNinguno: boolean = false);
procedure CargarBodegas(Conn: TADOConnection; Combo: TVariantComboBox; CodigoBodega: Integer = 0; Todos: Boolean = true);overload;
procedure CargarBodegas(Conn: TADOConnection; Combo: TComboBox; CodigoBodega: integer = 0; TieneItemNinguno: boolean = false);overload;
procedure CargarTiposProcesos(Conn: TADOConnection; Combo: TComboBox; CodigoProceso: integer = 0);
procedure CargarUnidadesToleranciaCategoria(Combo: TComboBox; Unidad: AnsiString = '');
procedure CargarConfiguracionNotificaciones(Conn: TADOConnection; Combo: TComboBox; CodigoConfiguracion: integer = 0);
procedure CargarCategoriasConsultas(Conn: TADOConnection; Combo: TComboBox; CodigoCategoria: integer = 0);
procedure CargarConveniosRUT(Conn: TADOConnection; Combo: TVariantComboBox; CodigoDocumento,
  NumeroDocumento: AnsiString; SoloActivos: Boolean = False; CodigoConvenio: Integer = 0; TieneItemSeleccionar: Boolean = False; NumeroFormateado: Boolean = True; SeleccionTodos: Boolean = False; CodigoTipoConvenio: Integer = 0); //SS_1120_MVI_20130820   // SS_637_20100927
procedure CargarSistemas(Conn: TADOConnection; Combo: TComboBox; EsVenta: boolean = false; CodigoSistema: integer = 0); overload;
procedure CargarSistemas(Conn: TADOConnection; Combo: TComboBox; CodigoSistema: integer = 0; Mostrar: boolean = False); overload;
procedure CargarUsuariosSistemas(Conn: TADOConnection; Combo: TVariantComboBox; CodigoUsuario: AnsiString = ''; TieneItemNinguno: boolean = false);
procedure CargarCodigosUsuariosSistemas(Conn: TADOConnection; Combo: TComboBox; TieneItemNinguno: Boolean = True);
procedure CargarPersonalSinUsuariosSistemas(Conn: TADOConnection; Combo: TVariantComboBox; CodigoPersona: Integer = 0);
procedure CargarMotivosContacto(Conn: TADOConnection; Combo: TVariantComboBox; Ninguno: Boolean = True; CodigoMotivoContacto: integer = 0);
procedure CargarCategoriasModulos(Conn: TADOConnection; Combo: TComboBox; CodigoCategoria: integer = 0);
procedure CargarTiposDocumento(Conn: TADOConnection; Combo: TMaskCombo; CodigoDocumento: AnsiString = '');
procedure CargarTiposCalle(Conn: TADOConnection; Combo: TComboBox; CodigoTipoCalle: integer = 0; TieneItemNinguno: boolean = false);
procedure CargarTiposEdificacion(Conn: TADOConnection; Combo: TComboBox; CodigoTipoEdificacion: integer = 0);
procedure CargarTiposPatente(Conn: TADOConnection; Combo: TMaskCombo; TipoPatente: AnsiString = '');
procedure CargarMediosContacto(Conn: TADOConnection; Combo: TComboBox; CodigoMedio: integer = 0; Todos: boolean = False); overload;
procedure CargarMediosContacto(Conn: TADOConnection; Combo: TVariantComboBox; CodigoMedio: integer = 0; Todos: boolean = False); overload;
procedure CargarTiposMedioContacto(Conn: TADOConnection; Combo: TVariantComboBox;
  CodigoTipoMedioContacto: integer = 0; Formato: AnsiString = ''; Todos: boolean = False; TieneItemNinguno: boolean = False;FormatoDistintoA:AnsiString='');
procedure CargarCodigosAreaTelefono(Conn: TADOConnection; Combo: TComboBox; CodigoAreaTelefono: Integer = 0);
procedure CargarTiposSeveridad(Conn: TADOConnection; Combo: TComboBox; CodigoSeveridad:integer = 0);
procedure CargarSexos(Combo: TComboBox; Sexo: AnsiString = ''; TieneItemNinguno: boolean = false);
procedure CargarPersoneria(Combo: TComboBox; Personeria: AnsiString = ''; PermiteAmbas: Boolean = False; TieneItemNinguno: boolean = false);
procedure CargarTipoSituacionIVA(Conn: TADOConnection; Combo: TComboBox; SituacionIVA: AnsiString = '');
procedure CargarColoresVehiculos(Conn: TADOConnection; Combo: TComboBox; CodigoColor: Integer = 0);
procedure CargarMarcasVehiculos(Conn: TADOConnection; Combo: TComboBox; CodigoMarca: Integer = 0);
procedure CargarMarcasComboVariant(Conn: TADOConnection; Combo: TVariantComboBox; marcaVehiculo: Integer = 0);
//procedure CargarModelosVehiculos(Conn: TADOConnection; Combo: TComboBox; CodigoMarca: Integer; CodigoModelo: Integer = 0);
procedure CargarTarjetasCredito(Conn: TADOConnection; Combo: TComboBox; CodigoTarjeta: Integer = 0);
procedure CargarTiposTarjetasCreditoDebito(Conn: TADOConnection; Combo: TComboBox; TipoTarjeta: AnsiString = DA_TARJETA_CREDITO; CodigoTipoTarjeta: Integer = 0);
procedure CargarTipoTarjeta(Combo: TComboBox; TipoTarjeta: AnsiString = DA_TARJETA_CREDITO);
procedure CargarBancos(Conn: TADOConnection; Combo: TComboBox; CodigoBanco: Integer = 0);
procedure CargarBancosPagoVentanilla(Conn: TADOConnection; Combo: TVariantComboBox; CodigoBanco: Integer = -1; TipoMedioPago: Integer = 0; TieneItemSeleccionar: Boolean = False); // SS 809
procedure CargarBancosPAC(Conn: TADOConnection; Combo: TVariantComboBox; CodigoBanco: Integer = 0; TieneItemSeleccionar: Boolean = False); // Rev. 24 (SS 809)
procedure CargarCuentas(Conn: TADOConnection; Combo: TComboBox; CodigoCliente: LongInt; TipoPago: String = ''; CodigoDebitoAutomatico: Integer = 0; CodigoCuenta: LongInt = 0);
procedure CargarPaises(Conn: TADOConnection; Combo: TComboBox; CodigoPais: AnsiString = '');
procedure CargarRegiones(Conn: TADOConnection; Combo: TComboBox; CodigoPais: AnsiString; CodigoRegion: AnsiString = ''; TieneItemNinguno: boolean = false); overload;
procedure CargarRegiones(Conn: TADOConnection; Combo: TVariantComboBox; CodigoPais: AnsiString; CodigoRegion: AnsiString = ''; TieneItemSeleccionar: boolean = false); overload;
procedure CargarComunas(Conn: TADOConnection; Combo: TComboBox; CodigoPais, CodigoRegion: AnsiString; CodigoComuna: AnsiString = ''; TieneItemNinguno: boolean = false;TieneItemSeleccionar:Boolean=False); overload;
procedure CargarComunas(Conn: TADOConnection; Combo: TVariantComboBox; CodigoPais, CodigoRegion: AnsiString; CodigoComuna: AnsiString = ''; TieneItemSeleccionar:Boolean=False); overload;
procedure CargarComunasConRegion(Conn: TADOConnection; Combo: TComboBox; CodigoPais: AnsiString; CodigoRegion: AnsiString = ''; CodigoComuna: AnsiString = ''; TieneItemNinguno: boolean = false);
procedure CargarCallesComuna(Conn: TADOConnection; Combo: TComboBox; CodigoPais,
            CodigoRegion: AnsiString; CodigoComuna: AnsiString = '';
            CodigoCalle: integer = -1; CodigoSegmento:Integer = -1);
procedure CargarCategoriasVehiculos(Conn: TADOConnection; Combo: TComboBox; CodigoCategoria: Integer = 0); overload;
procedure CargarCategoriasVehiculos(Conn: TADOConnection; Combo: TVariantComboBox; CodigoCategoria: Integer = 0; todos: boolean = false); overload;
procedure CargarVehiculosTipos(Conn: TADOConnection; Combo: TVariantComboBox; Ninguno: Boolean = True; tipoVehiculo: Integer = 0);
procedure CargarPuntosEntregaDisponibles(Conn: TADOConnection; Combo: TVariantComboBox; Ninguno: Boolean = True; CodigoPuntoEntrega: Integer = 0);
procedure CargarPuntosEntrega(Conn: TADOConnection; Combo: TVariantComboBox; Ninguno: Boolean = True; CodigoPuntoEntrega: Integer = 0);
procedure CargarPuntosVenta(Conn: TADOConnection; Combo: TVariantComboBox; Ninguno: Boolean = True; CodigoPuntoEntrega: Integer = 0; PuntoVentaSeleccionar: Integer = 0);
procedure CargarTransportistas(Conn: TADOConnection; Combo: TVariantComboBox; CodigoTransportista: Integer = 0);
procedure CargarCategoriasVehiculosVariant(Conn: TADOConnection; Combo: TVariantComboBox; CategoriaAdicional: AnsiString = '');
procedure CargarCuentasBancarias(Conn: TADOConnection; CheckList: TVariantCheckListBox; CodigoBanco: Integer = 0);
procedure CargarTiposCuentasBancarias(Conn: TADOConnection; Combo: TVariantComboBox);
procedure CargarFechasParaReportes(Combo: TComboBox);
procedure CargarCategoriasPersonal(Conn: TADOConnection; Combo: TComboBox; CodigoCategoria: integer = 0);
procedure CargarContextMarks(Conn: TADOConnection; Combo: TComboBox; ContextMark: integer = 0);
procedure CargarActividadesPersona(Conn: TADOConnection; Combo: TComboBox; Actividad: integer = 0);
procedure CargarTipoDomicilio(Conn: TADOConnection; Combo: TComboBox; TipoDomicilio: integer = 0);
procedure CargarTitulosFAQ(Conn: TADOConnection; Combo: TComboBox; CodigoTitulo: integer = 0);
procedure CargarSubTitulosFAQ(Conn: TADOConnection; Combo: TComboBox; CodigoSubTitulo: integer = 0);
procedure CargarPreguntasFAQ(Conn: TADOConnection; Combo: TComboBox; Condicion: String; CodigoPregunta: integer = 0);
procedure CargarTiposDatoPregunta(Conn: TADOConnection; Combo: TComboBox; CodigoPregunta: Integer; CodigoDatoPregunta: integer = 0);
procedure CargarTiposPregunta(Conn: TADOConnection; Combo: TComboBox; CodigoTipoPregunta: integer = 0);
procedure CargarFuentesSolicitud(Conn: TADOConnection; Combo: TComboBox; CodigoFuenteSolicitud:Integer = 0; TieneItemNinguno: boolean = false; Default: boolean = false; DescartarTipoFuente: AnsiString = '');
procedure CargarEstadosSolicitud(Conn: TADOConnection; Combo: TComboBox; CodigoEstadoSolicitud:Integer = 0; TieneItemNinguno: boolean = false);
procedure CargarEstadosTurno(Conn: TADOConnection; Combo: TComboBox; CodigoEstadoTurno:char = #0; TieneItemNinguno: boolean = false);
procedure CargarResultadosEstados(Conn: TADOConnection; Combo: TVariantComboBox; CodigoEstadoSolicitud:Integer = 0; CodigoEstadoResultado: integer = 0; TieneItemNinguno: boolean = false);
procedure CargarEstadosConvenio(Conn: TADOConnection; Combo: TComboBox; CodigoEstadoConvenio:Integer = 0; TieneItemNinguno: boolean = false);
procedure CargarTiposFuenteSolicitud(Combo: TVariantComboBox; TieneItemNinguno: Boolean = False; Default: Char = 'W');
procedure CargarFuenteComunicacion(Conn: TADOConnection; Combo: TComboBox; CodigoFuenteComunicacion: integer = 0; TieneItemNinguno: boolean = False);
procedure CargarFuentesOrigenContacto(Conn: TADOConnection; Combo: TComboBox; CodigoFuenteOrigenContacto: integer = 0);
procedure CargarFormularios(Conn: TADOConnection; Combo: TComboBox);
procedure CargarTiposCuentas(Conn: TADOConnection; Combo: TComboBox; TipoCuenta:Integer=-1; TieneItemSeleccionar: boolean = false);
procedure CargarBancosxCuentas(Conn: TADOConnection; Combo: TComboBox; TipoCuenta:Integer=-1; CodigoBanco:Integer=-1; TieneItemSeleccionar: boolean = false);
procedure CargarTiposTarjetasCredito(Conn: TADOConnection; Combo: TComboBox; CodigoTipoTarjetaCredito:Integer=-1; TieneItemSeleccionar: boolean = false); overload;
procedure CargarTiposTarjetasCredito(Conn: TADOConnection; Combo: TVariantComboBox; CodigoTipoTarjetaCredito: Integer = -1; TieneItemSeleccionar: Boolean = False); overload;
procedure CargarTiposTarjetasCreditoVentanilla(Conn: TADOConnection; Combo: TVariantComboBox; CodigoTipoTarjetaCredito: Integer = -1; TieneItemSeleccionar: Boolean = False; SoloTarjetasPagoVentanilla: Boolean = False); // SS 809
procedure CargarEmisoresTarjetasCredito(Conn: TADOConnection; Combo: TComboBox; CodigoEmisorTarjetaCredito:Integer=-1; TieneItemSeleccionar: boolean = false); overload;
procedure CargarEmisoresTarjetasCredito(Conn: TADOConnection; Combo: TVariantComboBox; CodigoEmisorTarjetaCredito: Integer = -1; TieneItemSeleccionar: Boolean = False); overload;
procedure CargarMotivosCancelacion(Conn: TADOConnection; Combo: TVariantComboBox; Ninguno: Boolean = True; CodigoMotivoCancelacion: integer = 0);
procedure CargarGuiaDespacho(Conn: TADOConnection; Combo: TComboBox; CodigoAlmacenDestino: integer = -1; Ninguno: Boolean = True; CodigoTransportista: integer = -1; CodigoAlmacenOrigen: integer = -1);
procedure CargarGuiaDespachoOrigen(Conn: TADOConnection; Combo: TComboBox; CodigoAlmacenDestino: integer = -1; Ninguno: Boolean = True; CodigoTransportista: integer = -1; CodigoAlmacenOrigen: integer = -1);
procedure CargarCombioSituacionTag(Conn: TADOConnection; Combo: TComboBox; SituacionesCAC : Boolean = True);
procedure CargarMotivosTransitoNoFacturable(Conn: TADOConnection; Combo: TVariantComboBox);

//Revision : 16
procedure CargarEmpresaRecaudacion(Conn: TADOConnection; Combo: TVariantComboBox; Ninguno: Boolean = True; EmpresaRecaudacion: Integer = 0; FiltroJudicial: boolean = false);
procedure CargarTipoDeuda(Conn: TADOConnection; Combo: TVariantComboBox; Ninguno: Boolean = True; TipoDeuda: Integer = 0);


// Revisi�n 20
procedure CargarEmpresaRecaudacionJudicial(Conn: TADOConnection; Combo: TVariantComboBox; Ninguno: Boolean = True; EmpresaRecaudacion: Integer = 0);
procedure CargarEmpresaRecaudacionPreJudicial(Conn: TADOConnection; Combo: TVariantComboBox; Ninguno: Boolean = True; EmpresaRecaudacion: Integer = 0);


//Domicilios en General
procedure ClickCheckEliminar(var checkEliminar: TCheckBox; ElegidoParaEntrega: boolean);

//Programacion
procedure GrabarComponentesObligatoriosyHints(Form:TComponent;Database:TADOConnection;ComponentesExcluidos: array of TControl;GrabarOcultos:Boolean=False;ComponentesExcluidosdelHint:TControl=nil);
procedure ObtenerComponentesObligatoriosyHints(Form:TComponent;Database:TADOConnection);
function ObtenerLabelComponente(Form,Componente:TComponent):TLabel;
procedure GestionarComponentesyHints(Form:TComponent;Database:TADOConnection;ComponentesExcluidos: array of TControl;GrabarOcultos:Boolean=False;ComponentesExcluidosdelHint:TControl=nil);
function EsComponenteObligatorio(Componente:TControl):Boolean;
function LimpiarCaracteresLabel(Caption:String):String;
function DarNombredeClase(Componente:TComponent):String;
function DarCaption(Componente:TComponent):String;
function NowBase(Database:TADOConnection):TDateTime;
function NowBaseSmall(Database:TADOConnection):TDateTime;
function MesNombre(Fecha: TDateTime): String;
function InvocarCorreoPredeterminado(Componente: TWinControl; Direccion:string; Asunto: string; Cuerpo: AnsiString): boolean;
procedure GrabarMensajeEnArchivoLog(const Path, FileName, FileExtension, Msg, MachineName, UserName: String);
function ConvertirEnFechaParaScript(unaFecha: TDateTime; IncluirHora: Boolean): String;


// Est�tica
procedure DrawMDIBackground(Graphic: TGraphic);

function RegistryKeyExists(RootKey: HKey; Key: AnsiString): Boolean;
function RegistryValueExists(RootKey: HKey; Key, ValueName: AnsiString): Boolean;

function GetRegistryBooleanValue(RootKey: HKey; Key, ValueName: AnsiString): Boolean;
procedure SetRegistryBooleanValue(RootKey: HKey; Key, ValueName: AnsiString; Value: Boolean);

function GetUserSettingShowBrowserBar: Boolean;
function SetUserSettingShowBrowserBar(Valor: Boolean): Boolean;

function GetUserSettingShowIconBar: Boolean;
function SetUserSettingShowIconBar(Valor: Boolean): Boolean;

function GetUserSettingShowHotLinksBar: Boolean;
function SetUserSettingShowHotLinksBar(Valor: Boolean): Boolean;

// Funciones de validaci�n
function  ValidateControls(Controls: Array of TControl; Conditions: Array of Boolean; Caption: AnsiString; Messages: Array of String): Boolean;
function  ValidarPais(Connection: TADOConnection; CodigoPais: AnsiString): Boolean;
function  ValidarRegion(Connection: TADOConnection; CodigoPais, CodigoRegion: AnsiString): Boolean;
function  ValidarComuna(Connection: TADOConnection; CodigoPais, CodigoRegion, CodigoComuna: AnsiString): Boolean;
function  ValidarMarca(Connection: TADOConnection; CodigoMarca: Integer): Boolean;
function  ValidarTarjetaCredito(Connection: TADOConnection; CodigoTarjeta: Integer; NumeroTarjeta: AnsiString; var DescriError: Ansistring): Boolean;
function  ValidarCuentaBancaria(Connection: TADOConnection; CodigoBanco: Integer; NumeroCuenta: AnsiString;  var DescriError: Ansistring): Boolean;
function  ValidarEmail(Conn: TADOConnection; email: ansistring): boolean;
function PermitirSoloLetrasNumeros(Caracter:Char):char;
function PermitirSoloNumeros(Caracter:Char):Char;
function EsFormatoPatenteValido(Pat: string): boolean;
function ExistePatenteCuenta(Base:TADOConnection; Patente : TEdit; CodigoConvenio: integer; var PatAsociada, ConvenioAsociado: string): string;
//function ValidarSintaxisEmail(email: ansistring): boolean;
function ValidarNumeroCalle(CodigoPais,CodigoRegion,CodigoComuna:String;CodigoCalle,Numero:Integer):Boolean;
function ValidarNumeroCuentaBancaria(Conn: TADOConnection;CodigoTipoCuenta:Integer;Numero:String):Boolean;
function ValidarRUT(Conn: TADOConnection;Numero:String):Boolean;
function ValidarVehiculoOtraConcesionaria(Conn: TADOConnection; Patente: string): Boolean;
function CaracteresIngresadosSonDePatenteChilena(Patente: String): boolean;
function PermitirSoloDigitos(Texto: string): boolean;
function ObtenerModulo10(Conn: TADOConnection; Numero: String): String; // Rev. 24 (SS 719)
function ExistePatente(Conn: TADOConnection; Patente: string): Boolean; // Rev. 26 (SS 719 - Defect 13741)
function VerificarFormatoPatenteChilena(Conn: TADOConnection; Patente: string): Boolean; // Rev. 26 (SS 719 - Defect 13741)

// Manejo de Menues
function GenerateMenuFile(Menu: TMenu; CodigoSistema: Integer;
  Database: TADOConnection; const PermisosAdicionales: Array of TPermisoAdicional): Boolean;
function CargarAccesos(BaseDeDatos: TADOConnection; CodigoUsuario: AnsiString;
  CodigoSistema: Integer): Boolean; overload;
function CargarAccesosEnLista(BaseDeDatos: TADOConnection; CodigoUsuario: AnsiString;
  CodigoSistema: Integer; Lista: TStringList): Boolean; overload;
function EliminarCaracterDeCadena(Cadena, caracter: AnsiString): AnsiString;
function HabilitarPermisosMenu(Menu: TMenu): Boolean;
function ExisteAcceso(Funcion: AnsiString): Boolean;
procedure EjecutarInterfaz(Conn: TADOConnection; Codigo: integer);
procedure EjecutarInterfazSalidaXML(Conn: TADOConnection; CodigoInterfaz: integer; TextoSQL: AnsiString; NombreInterfaz: AnsiString; NombreArchivo: AnsiString; Directorio: AnsiString);
procedure EjecutarInterfazSalidaTXT(Conn: TADOConnection; CodigoInterfaz: integer; TextoSQL: AnsiString; NombreInterfaz: AnsiString; NombreArchivo: AnsiString; Directorio: AnsiString; Separador: AnsiString);
procedure EjecutarInterfazEntradaXML(Conn: TADOConnection; CodigoInterfaz: integer; TextoSQL: AnsiString; NombreInterfaz: AnsiString; NombreArchivo: AnsiString; Directorio: AnsiString);
procedure EjecutarInterfazEntradaTXT(Conn: TADOConnection; CodigoInterfaz: integer; TextoSQL: AnsiString; NombreInterfaz: AnsiString; NombreArchivo: AnsiString; Directorio: AnsiString; Separador: AnsiString);
procedure GuardarRegistroInterface(Conn: TADOConnection; CodigoInterfaz: integer; IdInterfaz: AnsiString; NombreInterfaz: AnsiString);
procedure QuitarCategoriasSinInformes(Menu: TMenu);overload;
procedure QuitarCategoriasSinInformes(ItemMenu: TMenuItem);overload;
{INICIO:TASK_027_JMA_20160622
procedure CargarInformes(Conn: TADOConnection; CodigoAplicacion: Integer; Menu: TMenu; ConnInformes: TADOConnection); overload;
TERMINO:TASK_027_JMA_20160622}
procedure CargarInformes(Conn: TADOConnection; CodigoAplicacion: Integer; Menu: TMenu); overload;
procedure CargarCategoriasInformes(Conn: TADOConnection; ItemMenu: TMenuItem);

//function TieneConvenioDeCN(Conn: TADOConnection; NumeroDocumento : String) :Boolean;		//SS_1147_MCA_20140408
function TieneConvenioNativo(Conn: TADOConnection; NumeroDocumento : String) :Boolean;		//SS_1147_MCA_20140408
//function ObtenerContextMarkTAG(Conn: TADOConnection; ContractSerialNumber: DWord): Integer;   //SS_1147_MCA_20140408
function ObtenerContextMarkTAG(Conn: TADOConnection; ContractSerialNumber: Int64): Integer; //SS_1147_MCA_20140408

// Manejo de registro de operaciones
function CrearRegistroOperaciones(Conn: TADOConnection; CodigoSistema, CodigoModulo,
  CodigoAccion, CodigoSeveridad: Integer; PuestoDeTrabajo, CodigoUsuario, Evento: AnsiString;
  IDRegistroOperacionPadre: Integer; Reservado1, Reservado2, Reservado3: Variant;
  DescriError: String): Integer;

function ActualizarRegistroOperacion(Conn: TADOConnection; CodigoSistema,
  IDRegistroOperacion, CodigoSeveridad: Integer; OperacionExitosa: Boolean;
  evento: AnsiString; Reservado1, Reservado2, Reservado3: Variant;
  var DescriError: AnsiString): Boolean;

// Funciones para mimicos de viaje
//procedure DibujarViaje(ImagenViaje: TBitMap; PuntosCobro: array of Boolean);

function EliminarCeroIzquierda(S: string): String;

// Funciones TAGs
function SerialNumberToEtiqueta(SerialNumber: AnsiString): AnsiString; overload;
function SerialNumberToEtiqueta(SerialNumber: DWORD): AnsiString; overload;
function EtiquetaToSerialNumber(Etiqueta: AnsiString): DWORD;
function TAGSLibres(Categoria: Integer): LongInt;
function ValidarEtiqueta(TextoCaption: ANSIString; Referencia, Etiqueta: ANSIString; ConMsg: Boolean=False): Boolean;
function ValidarCategoria(TextoCaption: ANSIString; Categoria: Integer; ConMsg: Boolean=False): Boolean;
//INICIO	: CFU 20161026 TASK_057_CFU_20161017-Gesti�n_TAGs_Doble_Categoria
function ValidarCategoriaUrbana(TextoCaption: ANSIString; CategoriaUrbana: Integer; ConMsg: Boolean=False): Boolean;
function ValidarCategoriaUrbanaTAG(TextoCaption: ANSIString; Referencia: ANSIString; TAG: DWORD; Categoria: integer; ConMsg: Boolean=False): Boolean;
//TERMINO	: CFU 20161026 TASK_057_CFU_20161017-Gesti�n_TAGs_Doble_Categoria
function ValidarCategoriaTAG(TextoCaption: ANSIString; Referencia: ANSIString; TAG: DWORD; Categoria: integer; ConMsg: Boolean=False): Boolean;
function ValidarEditable(TextoCaption: ANSIString; Editable: Boolean; ConMsg: Boolean=False): Boolean;
function ValidarContenedor(TextoCaption: ANSIString; Contenedor: integer; ConMsg: Boolean=False): Boolean;
function ValidarCantidadTAGs(TextoCaption: ANSIString; Categoria: Integer; CantidadTAGS: LongInt; CompararDisponibles: Boolean; ConMsg: Boolean=False): Boolean;
function ValidarRangosTAGs(TextoCaption: ANSIString; TAGInicial, TAGFinal: DWORD; Nominal: Boolean; ConMsg: Boolean=False): Boolean;
function ObtenerUbicacionActualTagPorNumero( const ContextMark: Smallint; const ContactSerialNumber: DWORD): integer;


// CAC
{ INICIO BLOQUE SS_628_ALA_20110818
function ObtenerConvenioMedioPago(Conn: TADOConnection; NumeroDocumento: String; CodigoConvenioEditado: Integer;
 TipoMedioPagoAutomatico: Integer; CodigoTipoTarjetaCredito: Integer; NumeroTarjetaCredito: String;                                 NroCuentaBancaria: String;
 CodigoBanco: Integer; CodigoTipoCuentaBancaria: Integer; ExcluirConvenioSinCuenta : Integer): Integer;     //SS_628_ALA_20110720
 TERMINO BLOQUE SS_628_ALA_20110818}
function ObtenerConvenioMedioPago(Conn: TADOConnection; NumeroDocumento: String; CodigoConvenioEditado: Integer; TipoMedioPagoAutomatico: Integer; ExcluirConvenioSinCuenta : Integer): Integer; //SS_628_ALA_20110818
function NoPermitirMedioPAgoDuplicado(Coneccion: TADOConnection; NumeroDocumento: String): Boolean;
procedure OrdenarGridPorColumna(Grilla: TCustomDBListEx; Columna: TDBListExColumn; NombreCampo: string; NoCambiarOrden: boolean=False);
function ChequearConvenioEnCarpetaLegal(CodigoConvenio: integer): Boolean;
function ObtenerEmpresaConvenioEnCarpetaLegal(CodigoConvenio: integer): string;
procedure CargarConveniosEnCombo(Conn: TADOConnection; Combo: TVariantComboBox; FCodigoPersona: integer; CodigoConvenio: Integer = 0);
function ValidarVoucher( NumeroVoucher: integer; ListaVouchers:TStringList; Control:TControl ): boolean;
function ObtenerDatosPersonales(Conn: TADOConnection; CodigoDocumento, NumeroDocumento: String; CodigoPersona: Integer = 0): TDatosPersonales; // SS 784
function VerificarExisteConvenioSinCuenta(Conn: TADOConnection; NumeroDocumento: String; ExcluirEsteConvenio : Int64) : Boolean;         //SS_628_ALA_20110720   //SS_628_ALA_20111219

//RNVM
function ConsultarPatenteRNVM(Pregunta: TRNVMQuery; var Respuesta: TRVMInformation; var ExistePatente:Boolean; var DescriError:String; var RespuestaRNVM: WideString): Boolean;
procedure ValidarResultadoConsultaRNMVM (Respuesta: TRVMInformation; ExistePatente: Boolean; var Resultado: TRVMQueryResult; var DatosInfraccion: TEnforcementData);

// Imagen Logo
function LevantarLogo (var Logo: TBitmap): Boolean;

// Infracciones
procedure CargarComboMotivoAnulacion(Conn: TADOConnection; Combo: TVariantComboBox; TieneSinEspecificar : Boolean = False; SoloPermitidosAnulacionManual: Boolean = False); // Rev. 29 (Fase 2)
procedure CargarComboConcesionarias(Conn: TADOConnection; Combo: TVariantComboBox; TieneSinEspecificar : Boolean = False);overload; // Rev. 29 (Fase 2)                                                //PAR00133-FASE2-NDR-20110624
procedure CargarComboTiposConceptosMovimientos(Conn: TADOConnection; Combo: TVariantComboBox; TieneSinEspecificar : Boolean = False); // Rev. 30 (Fase 2)

procedure CargarComboConcesionarias(Conn: TADOConnection; Combo: TVariantComboBox; TipoConcesionaria:Integer; TieneSinEspecificar : Boolean = False );overload; // Rev. 29 (Fase 2)                    //PAR00133-FASE2-NDR-20110624

procedure CargarComboOtrasConcesionarias(Conn: TADOConnection; Combo: TVariantComboBox; TieneSinEspecificar : Boolean = False; SoloFacturaCN: Boolean = False); // SS_1015_HUR_20120409

//Ordenes Servicio
//Rev.33 / 29-Junio-2010 / Nelson Droguett Sierra ----------------------------------------------------------------
procedure CargarComboTiposOrdenServicio(Conn: TADOConnection; Combo: TVariantComboBox; TieneSinEspecificar : Boolean = False); // Rev. 33 (Fase 2)
procedure CargarSubTiposReclamo(Combo:TVariantComboBox);
procedure CargarConcesionariasReclamo(Combo:TVariantComboBox);
procedure CargarConcesionariasReclamoHistorico(Combo:TVariantComboBox); //SS_1006_1015_MCO_20120906
//FinRev.33-------------------------------------------------------------------------------------------------------


function AsignarValorComboEstado(Combo: TVariantComboBox): Variant;

//Funciones para Comunicaciones en integraci�n con CTI
function EnComunicacion: Boolean;
function IniciarComunicacion(NumeroDocumento: AnsiString; var CodigoComunicacion: Integer; var MsgError: String): Boolean;
function TerminarComunicacion(CodigoComunicacion: Integer; var MsgError: String):Boolean;

//Funcion para Facturacion
function ObtenerNumeroSiguienteComprobante( Conn: TADOConnection; TipoComprobante: string): integer ;
function VerificarExistenCotizacionesDelDia:boolean;

//Funci�n para ver si un rut tiene convenio con alguien.
function NumeroDocumentoTieneConvenio(NumeroDocumento: string; codigoConcesionaria: integer = 0): Boolean;
//Verifica si hay un turno abierto por un usuario dado
function ExisteTurnoAbiertoPorUsuario(Conn: TADOConnection; AUsuarioSistema: string): Boolean;
//mensaje de acuerdo al documento y sistema se trae tipo cliente y el mensaje
function  MensajeTipoCliente(Conn: TADOConnection; NumeroDocumento: AnsiString; CodigoSistema: Integer): AnsiString;
//funcion para sacar el estado actual de un transito
function ObtenerEstadoTransito(Conn: TADOConnection; NumCorrCA: Integer ): Integer;

//procedimiento para insertar eventos de alarma
function InsertarAlarmaEvento(Conn: TADOConnection;Modulo : string; Categoria :integer; Detalle: string): Boolean;

function EsPatenteEnSeguimiento(Conn: TADOConnection; Patente: string): Boolean; // Rev. 35

//Rev.33 / 17-Agosto-2010 / Nelson Droguet Sierra
procedure CargarFirmasEmail(Conn: TADOConnection; Combo: TVariantComboBox; CodigoFirma: Integer = 0; TieneItemSeleccionar: Boolean = False);

procedure MostrarTagsVencidos(Rut : String; pDataBase : TADOConnection; var pNotificado: Boolean; SinAviso: Boolean = False); //SS-960-ALA-20110411    // SS_960_PDO_20120104

function ObtenerPorcentajeIVA(Conn: TADOConnection): Extended;                                              //SS_1012_ALA_20111128
function CalcularIVADeImporte(Conn: TADOConnection; Importe : Extended) : Int64; overload;                  //SS_1012_ALA_20111128
function CalcularIVADeImporte(PorcentajeIva : Extended; Importe : Extended) : Int64; overload;              //SS_1012_ALA_20111128
function CalcularImporteNetoDeImporte(Conn: TADOConnection; Importe : Extended) : Int64; overload;            //SS_1012_ALA_20111128
function CalcularImporteNetoDeImporte(PorcentajeIva : Extended; Importe : Extended) : Int64; overload;        //SS_1012_ALA_20111128

function RoundCS(Val : Extended) : Int64;                                                                   //SS_1012_ALA_20111128

procedure CargarComboGestionJudicialCategorias(Conn: TADOConnection; Combo: TVariantComboBox; TieneSinEspecificar : Boolean = False );                                  // SS_1047_CQU_20120528
procedure CargarComboGestionJudicialSubCategorias(Conn: TADOConnection; Combo: TVariantComboBox; CodigoCategoria : Integer; TieneSinEspecificar : Boolean = False );    // SS_1047_CQU_20120528
procedure CargarComboGestionJudicialGestion(Conn: TADOConnection; Combo: TVariantComboBox; CodigoSubCategoria : Integer; TieneSinEspecificar : Boolean = False );       // SS_1047_CQU_20120528
procedure CargarComboTribunales(Conn: TADOConnection; Combo: TVariantComboBox; CodigoTribunal:Integer = 0; TieneSinEspecificar : Boolean = False );                     // SS_1047_CQU_20120528
Procedure RefinanciacionRenumerarCuotas(Conn: TADOConnection; CodigoRefinanciacion: Integer);       // SS_1050_PDO_20120605
Procedure ItemComboBoxColor(Control: TWinControl; position: Integer; R: TRect; State: TOwnerDrawState; FontColor: TColor = clBlack; FontBold: Boolean = False);  //SS_1120_MVI_20130820

function ValidarVersion( Conn: TADOConnection;                                           //SS_925_NDR_20131122
                         CodigoSistema:Integer;                                          //SS_925_NDR_20131122
                         Menu: TMenu;                                                    //SS_925_NDR_20131122
                         const PermisosAdicionales: Array of TPermisoAdicional           //SS_925_NDR_20131122
                       ):Boolean; overload;                                             //SS_925_NDR_20131122
function EsEntero(TextoEvaluar : string; EsBigInt : Boolean = False) : Boolean; // SS_1151_CQU_20140103
function ObtenerCodigoConcesionariaNativa(Conn: TADOConnection = nil): integer;	//SS_1397_MGO_20151028						//SS_1147_MCA_20140408
function ObtenerNombreConcesionariaNativa(): string;							//SS_1147_MCA_20140408
function ObtenerNombreCortoConcesionariaNativa(): string;						//SS_1147_MCA_20140408
function ObtenerCodigoConcesionariaVS(Conn: TADOConnection = nil): Integer;		//SS_1397_MGO_20151028					                  //SS_1397_MGO_20151027

procedure CambiaColorMenu(Sender: TObject; ACanvas: TCanvas; ARect: TRect;      // SS_1147_CQU_20140731
            Selected: Boolean; ColorMenu, ColorFont, ColorMenuSel, ColorFontSel : TColor);    // SS_1147_CQU_20140731

function ObtenerDocumentoInterno(TipoComprobanteFiscal: string; NumeroComprobanteFiscal: Int64;         // SS_1147_CQU_20150316
                                var TipoComprobante: string; var NumeroComprobante: Int64) : Boolean;   // SS_1147_CQU_20150316
procedure CrearNodosXML(var NodoRaiz: IXMLNode; Valores: TDiccionario);
function BooleanToString(Valor: Boolean): string;

var
    SistemaActual: Integer;

implementation

uses
      FrmRptInformeUsuario;


function EliminarCeroIzquierda(S: string): String;
begin
    S := trim(S);
    while ((Length(S) > 0) and (S[1]='0')) do begin
        Delete(S, 1, 1)
    end;
    result := S;
end;

{TReportItem}
constructor TReportItem.Create(AOwner: TComponent);
begin
	inherited;
    OnClick := MyItemClick;
end;

destructor TReportItem.Destroy;
begin
	inherited Destroy;
end;

{******************************** Function Header ******************************
Function Name: Create
Author : ggomez
Date Created : 05/09/2006
Description :
Parameters : AOwner: TComponent; CodigoCategoria: Integer
Return Value : None
Revision : 1
    Author : vpaszkowicz
    Date : 05/09/2006
    Description :Agrego esta clase de Guille para manejar los itemss de men�
    en forma de �rbol
*******************************************************************************}

{ TCategoriaReportItem }

constructor TCategoriaReportItem.Create(AOwner: TComponent; CodigoCategoria: Integer);
begin
    inherited Create(AOwner);
    SetCodigo(CodigoCategoria);
end;

procedure TCategoriaReportItem.SetCodigo(const Value: Integer);
begin
	if Value = FCodigo then Exit;
    FCodigo := Value;
end;

destructor TCategoriaReportItem.Destroy;
begin
	inherited Destroy;
end;

constructor TInterfaceItem.Create(AOwner: TComponent);
begin
	inherited;
    OnClick := MyItemClick;
end;

destructor TInterfaceItem.Destroy;
begin
	inherited Destroy;
end;

Var
	Accesos: TStringList;
	TransitoAnterior: DWORD;

// Rutinas Generales
{--------------------------------------------------------
            NumeroAPalabras

Author  : mbecerra
Date    : 13-Enero-2011
Description :   (Ref SS 948)
                Funci�n que recibe un n�mero entero y
                devuelve las palabras que lo describen
------------------------------------------------------------}
function NumeroAPalabras(Numero : Longint) : string;
var
    lstLista : TStringList;
    NumeroStr, vNumero, vTexto, vGlosa : string;
    i, vTres : integer;
begin
    lstLista := TStringList.Create;
    NumeroStr := IntToStr(Numero);
    Result := '';
    vTres := 0;
    while NumeroStr <> '' do begin    {antes de truncar el numero, ver si es millon o millones, billon o billones}
        vGlosa := '';
        vNumero := Copy(NumeroStr, Length(NumeroStr) - 2,3);
        if vTres = 2 then begin
            if StrToInt(vNumero) > 1 then vGlosa := ' millones '
            else vGlosa := ' mill�n ';
        end
        else if vTres = 3 then vGlosa := 'mil '
        else if vTres = 4 then begin
            if StrToInt(vNumero) > 1 then vGlosa := ' billones '
            else vGlosa := ' bill�n ';
        end;

        NumeroStr := Copy(NumeroStr,1,Length(NumeroStr) - 3);
        {analizar este n�mero de m�ximo 3 digitos}
        while vNumero <> '' do begin
            case Length(vNumero) of
                3 : begin
                    if vNumero = '100' then begin
                        lstLista.Add('cien');
                        vNumero := '';
                    end
                    else begin
                        if vNumero[1] = '1' then lstLista.Add('ciento')
                        else if vNumero[1] = '2' then lstLista.Add('docientos')
                        else if vNumero[1] = '3' then lstLista.Add('trecientos')
                        else if vNumero[1] = '4' then lstLista.Add('cuatrocientos')
                        else if vNumero[1] = '5' then lstLista.Add('quinientos')
                        else if vNumero[1] = '6' then lstLista.Add('seiscientos')
                        else if vNumero[1] = '7' then lstLista.Add('setecientos')
                        else if vNumero[1] = '8' then lstLista.Add('ochocientos')
                        else if vNumero[1] = '9' then lstLista.Add('novecientos');

                        vNumero := Copy(vNumero,2,2);
                    end;
                end;

                2 : begin
                    if vNumero[1] = '1' then begin
                        if vNumero = '10' then begin
                            lstLista.Add('diez');
                            vNumero := '';
                        end
                        else if vNumero = '11' then begin
                            lstLista.Add('once');
                            vNumero := '';
                        end
                        else if vNumero = '12' then begin
                            lstLista.Add('doce');
                            vNumero := '';
                        end
                        else if vNumero = '13' then begin
                            lstLista.Add('trece');
                            vNumero := '';
                        end
                        else if vNumero = '14' then begin
                            lstLista.Add('catorce');
                            vNumero := '';
                        end
                        else if vNumero = '15' then begin
                            lstLista.Add('quince');
                            vNumero := '';
                        end
                        else begin
                            lstLista.Add('diez y ');
                            vNumero := Copy(vNumero,2,1);
                        end;
                    end
                    else begin
                        if vNumero[1] = '2' then lstLista.Add('veinte')
                        else if vNumero[1] = '3' then lstLista.Add('treinta')
                        else if vNumero[1] = '4' then lstLista.Add('cuarenta')
                        else if vNumero[1] = '5' then lstLista.Add('cincuenta')
                        else if vNumero[1] = '6' then lstLista.Add('sesenta')
                        else if vNumero[1] = '7' then lstLista.Add('setenta')
                        else if vNumero[1] = '8' then lstLista.Add('ochenta')
                        else if vNumero[1] = '9' then lstLista.Add('noventa');

                        vNumero := Copy(vNumero,2,1);
                        if vNumero <> '0' then begin
                            if lstLista.Count > 0 then lstLista[lstLista.Count - 1] := lstLista[lstLista.Count - 1] + ' y';
                        end;
                    end;
                end;

                1 : begin
                    if vNumero = '1' then begin
                        if lstLista.Count > 0 then lstLista.Add( 'uno')
                        else lstLista.Add('un');
                    end
                    else if vNumero = '2' then lstLista.Add( 'dos')
                    else if vNumero = '3' then lstLista.Add( 'tres')
                    else if vNumero = '4' then lstLista.Add( 'cuatro')
                    else if vNumero = '5' then lstLista.Add( 'cinco')
                    else if vNumero = '6' then lstLista.Add( 'seis')
                    else if vNumero = '7' then lstLista.Add( 'siete')
                    else if vNumero = '8' then lstLista.Add( 'ocho')
                    else if vNumero = '9' then lstLista.Add( 'nueve');

                    vNumero := '';
                end;
      end; {case}
    end; {while vNumero <> ''}

    vTexto := '';
    for i := 0 to lstLista.Count - 1 do
      vTexto:= vTexto + lstLista[i] + ' ';

    lstLista.Clear;
    Inc(vTres);
    case vTres of
      1 : Result := vTexto;
      2 : begin
            if vTexto<> '' then
              Result := vTexto + ' mil ' + Result;
          end;
      3,4,5 : begin
            if vTexto <> '' then
              Result := vTexto + vGlosa + Result;
          end;
    end;

  end; {while NumeroStr <> ''}
  
  lstLista.Free;

end;


function ArmarCondicionWhereMultipleFiltro (lista: ANSIString; nombreCampo: AnsiString): AnsiString;
var i: integer;
	s, OrConcatenados: AnsiString;
begin
	if Lista = '*' Then Begin
		s := '( (1=1) ';
		Lista := '';
	end else
		if Lista = '' then s := '( (1<>1) '
		else s := '( ';

	OrConcatenados := '';
	while Length (Lista) <> 0 do begin
		i := Pos('*', Lista);

		OrConcatenados := OrConcatenados + iif (OrConcatenados = '', '', ' OR ') + '(' + nombreCampo + ' = ' + Copy (Lista, 1, i-1) + ')';
		Delete (Lista, 1, i)
	end;

	result := s + OrConcatenados + ' )';
end;

function  ExisteCadenaEnStringList(STL: TStringList; Cadena: AnsiString): boolean;
var i: integer;
begin
    if STL.Count = 0 then begin
        result := false
    end
    else begin
        i := 0;
        while (i < STL.Count) do
        begin
            if trim(STL.Strings[i]) = trim(Cadena) then Break;
            Inc(i);
        end;
        result := (i <> STL.Count); //si es menor es porque la encontr�, sino es igual
    end;
end;

function  PrimeraLetraMayuscula(Texto: AnsiString; elRestoMinuscula: boolean = false): AnsiString;
var aux: AnsiString;
begin
    aux := '';
    {el Par�metro elRestoMinuscula significa que adem�s de poner la primer letra
    en may�scula se ponen (como indica el nombre!) el resto en min�scula. EJ: PEaje queda Peaje.
    Sino lo devuelve tal cual...}
    if trim(texto) <> '' then begin
        aux := UpperCase(texto[1]) +
               iif(elRestoMinuscula, LowerCase(Copy(texto,2,length(texto))),Copy(texto,2,length(texto)));
    end;
    result := aux;
end;

function  FechaNacimientoValida(Fecha: TDateTime; edadMinima: Integer = 0): boolean;
var
    anio: WORD;
begin
    try
        anio := YearOf(Fecha);
        result := (Fecha < Now) and (anio >= 1900) and
          (trunc(YearSpan(now, Fecha)) >= EdadMinima);
    except
        result := false;
    end;
end;

function DateTimeToDWord(FechaDate: TDateTime): DWORD;
begin
	Result := Trunc( (FechaDate - EncodeDate(1990, 1, 1)) * SecsPerDay );
end;

function DWordToDateTime(FechaWord: DWORD): TDateTime;
begin
	Result := EncodeDate(1990, 1, 1) + (FechaWord / SecsPerDay);
end;

function DescriIncidencia(Codigo: Integer; Detalle: AnsiString): AnsiString;
resourcestring
    STR_MSG_DESDE_VIA        = 'Mensaje desde la V�a: ';
    STR_INTENTO_ROBO         = 'Intento de Robo';
    STR_CIERRE_FORZADO       = 'Cierre Forzado';
    STR_ERROR_LECTURA        = 'Error de Lectura ';
    STR_MEDIO_PAGO_RECHAZADO = 'Medio de Pago Rechazado: ';
    STR_VIA_ONLINE           = 'V�a Online';
    STR_VIA_OFFLINE          = 'V�a Offline';
    STR_SOFT_INICIADO        = 'Software de v�a Iniciado. ';
    STR_POCO_ESPACIO         = 'Queda poco espacio libre en Disco: ';
    STR_INC_DESCONOCIDA      = 'Incidencia desconocida: ';
begin
	Case Codigo of
		// Incidencias de v�a
		INCID_MENSAJEPEAJISTA:
			Result := STR_MSG_DESDE_VIA + Detalle;
		INCID_ROBO:
			Result := STR_INTENTO_ROBO;
		INCID_CIERREFORZADO:
			Result := STR_CIERRE_FORZADO;
		INCID_ERRORLECTURAMEDIO:
			Result :=  STR_ERROR_LECTURA + Detalle;
		INCID_MEDIORECHAZADO:
			Result := STR_MEDIO_PAGO_RECHAZADO + Detalle;
		INCID_VIAONLINE:
			Result := STR_VIA_ONLINE;
		INCID_VIAOFFLINE:
			Result := STR_VIA_OFFLINE;
		INCID_ERRORHARDWARE:
			Result := Detalle;
		INCID_INICIOSOFTWARE:
			Result := STR_SOFT_INICIADO + Detalle;
		// Incidencias de Estaci�n / Generales
		INCID_POCOESPACIOENDISCO:
			Result := STR_POCO_ESPACIO + Detalle;
		else
			Result := STR_INC_DESCONOCIDA + Detalle;
	end;
end;
function DescriPais(Conn :TADOConnection; CodigoPais: AnsiString): String;
begin
    result:=Trim(QueryGetValue(Conn,format('select dbo.ObtenerDescripcionPais (''%s'')',[CodigoPais])));
end;

function EliminarCaracterDeCadena(Cadena, caracter: AnsiString): AnsiString;
var i: integer;
    s: AnsiString;
begin
    s := '';
    if Length(Cadena) > 0 then begin
        for i := 1 to Length(Cadena) do begin
            if cadena[i] <> caracter then s := s + cadena[i];
        end;
    end;
    result := s;
end;

function HabilitarPermisosMenu(Menu: TMenu): Boolean;

	Procedure HabilitarItem(Nivel: Integer; Item: TmenuItem);
	Var
		i: Integer;
	begin
        if (Nivel <> 0) and (Item.Caption <> '-') then
          //Item.Enabled := ExisteAcceso(Item.Name);
            Item.Visible := ExisteAcceso(Item.Name) or (Item.Caption = '&Salir');

		for i := 0 to Item.Count - 1 do begin
			Habilitaritem(Nivel + 1, Item.Items[i]);
		end;
	end;

begin
	HabilitarItem(0, Menu.Items);
	Result := True;
end;

function ExisteAcceso(Funcion: AnsiString): Boolean;
begin
    if (Funcion = '*') then begin
        result := Accesos.Count > 0;
    end else begin
        result := (Accesos.IndexOf(UpperCase(Funcion)) <> -1);
    end;
    Sleep(1);
end;

// Manejo de Men�es - Versi�n con ADO
function GenerateMenuFile(Menu: TMenu; CodigoSistema: Integer;
  Database: TADOConnection; const PermisosAdicionales: Array of TPermisoAdicional): Boolean;

Var
	OrdenMenu: Integer;

	// Genera men� recursivamente
	Procedure ExplodeMenuItem(Level: Integer; Item: TMenuItem; SP: TADOStoredProc);
	Var
		i: Integer;
	begin
        if (Level > 0) and (Item.Caption <> '-') then begin //and (item.Parent.Visible) and (Item.Visible) then begin
            Inc(OrdenMenu);
            With SP.Parameters Do Begin
                ParamByName('@CodigoSistema').Value := CodigoSistema;
                ParamByName('@Funcion').Value 		:= Item.Name;
                ParamByName('@Orden').Value 		:= OrdenMenu;
                ParamByName('@Descripcion').Value 	:= Trim(Item.Caption);
                ParamByName('@Nivel').Value 		:= Level;
            end;
            SP.ExecProc;
        end;
        for i := 0 to Item.Count - 1 do begin
            ExplodeMenuItem(Level + 1, Item.Items[i], SP);
        end;
	end;

    Procedure ActualizarPermisosAdicionales(SP: TADOStoredProc);
    Var
        i: Integer;
    begin
        for i := Low(PermisosAdicionales) to High(PermisosAdicionales) do begin
            Inc(OrdenMenu);
            With SP.Parameters Do Begin
                ParamByName('@CodigoSistema').Value := CodigoSistema;
                ParamByName('@Funcion').Value 		:= PermisosAdicionales[i].Funcion;
                ParamByName('@Orden').Value 		:= OrdenMenu;
                ParamByName('@Descripcion').Value 	:= PermisosAdicionales[i].Descripcion;
                ParamByName('@Nivel').Value 		:= PermisosAdicionales[i].Nivel;
            end;
            SP.ExecProc;
        end;
    end;

resourcestring
    MSG_ERROR_GENERATE_MENUS = 'No se pudieron generar los men�es';
    MSG_GENERATE_MENUS       = 'Generar Men�es';
Var
	ok: boolean;
	SP: TADOStoredProc;
begin
	Screen.Cursor := crHourGlass;
	ok := true;
    SP := TADOStoredProc.Create(nil);
    OrdenMenu := 0;
	// INICIO : 20160307 MGO
    Database := DMConnections.BaseBO_Master;
	// FIN : 20160307 MGO
    if not Database.Connected then Database.Open;
    Database.BeginTrans;
    try
        try
            // Defino estructura de stored procedure para actualizar FuncionesSistema
            SP.Connection := TADOConnection(Database);
            with SP, Parameters do begin
                ProcedureName := 'ActualizarSistemasFunciones';
                CreateParameter('@CodigoSistema', ftInteger, pdInput, 0, NULL);
                CreateParameter('@Funcion',  ftString, pdInput, 40, NULL);
                CreateParameter('@Orden', ftInteger, pdInput, 0, NULL);
                CreateParameter('@Descripcion', ftString, pdInput, 80, NULL);
                CreateParameter('@Nivel', ftInteger, pdInput, 0, NULL);
            end;

            // Anulamos las Funciones existentes para el sistema en curso, poniendo su nivel en 0
            try
                QueryExecute(Database, 'UPDATE FuncionesSistemas SET Level = 0 WHERE (CodigoSistema) = ''' + intToStr(CodigoSistema) + '''');
                ok := True;
            except
                ok := False;
            end;

            //pregunto x ok porque si no se conectaba aparecian sucesivos mensajes
            //que decian lo mismo.
            if ok then begin
                // Actualiza las funciones en la base, tomando los items del
                // men� recibido como par�metro.
                ExplodemenuItem(0, Menu.Items, SP);
                // Actualiza las funciones de la base, tomando los items
                // adicionales recibidos como par�metro
                ActualizarPermisosAdicionales(SP);
                // ahora elimino los items de menues con Nivel = 0 y los permisos asociados a ellos
                QueryExecute(Database, 'EXECUTE EliminarMenuesInexistentes');
            end
        except
	    	On e: exception do begin
    		    MsgBoxErr(MSG_ERROR_GENERATE_MENUS, E.message, MSG_GENERATE_MENUS, MB_ICONSTOP);
                ok := false;
		    end;
        end;
    finally
    	// Listo
	    SP.Free;
    	Screen.Cursor := crDefault;
        if Ok then Database.CommitTrans
        else Database.RollbackTrans;
	    Result := ok;
    end;
end;

function CargarAccesos(BaseDeDatos: TADOConnection; CodigoUsuario: AnsiString; CodigoSistema: Integer): Boolean;
begin
	Result := CargarAccesosEnLista(BaseDeDatos, CodigoUsuario, CodigoSistema, Accesos);
end;

function CargarAccesosEnLista(BaseDeDatos: TADOConnection; CodigoUsuario: AnsiString;
  CodigoSistema: Integer; Lista: TStringList): Boolean;
Var
	ObtenerPermisosUsuario: TADOStoredProc;
begin
	// INICIO : 20160307 MGO
    BaseDeDatos := DMConnections.BaseBO_Master;
	// FIN : 20160307 MGO
	Lista.Clear;
	Lista.Sorted := False;
	//
	ObtenerPermisosUsuario 					:= TADOStoredProc.Create(nil);
	ObtenerPermisosUsuario.Connection		:= BaseDeDatos;
	ObtenerPermisosUsuario.Name				:= 'ObtenerPermisosUsuario';
	ObtenerPermisosUsuario.ProcedureName	:= 'dbo.ObtenerPermisosUsuario';
	ObtenerPermisosUsuario.Parameters.CreateParameter('@CodigoUsuario', ftString, pdInput, 60, CodigoUsuario);
	ObtenerPermisosUsuario.Parameters.CreateParameter('@CodigoSistema', ftInteger, pdInput, 0, CodigoSistema);
	ObtenerPermisosUsuario.Parameters.ParamByName('@CodigoUsuario').Value := CodigoUsuario;
	ObtenerPermisosUsuario.Parameters.ParamByName('@CodigoSistema').Value := CodigoSistema;
	Result := OpenTables([ObtenerPermisosUsuario]);
	if not Result then Exit;
	// Cargamos
	While not ObtenerPermisosUsuario.Eof do begin
		Lista.Add(UpperCase(Trim(ObtenerPermisosUsuario.FieldByname('Funcion').AsString)));
		ObtenerPermisosUsuario.Next;
	end;
	Lista.Sorted := True;
	ObtenerPermisosUsuario.Close;
	ObtenerPermisosUsuario.Free;
end;

// Rutinas para manejo de Tr�nsitos
function ObtenerNumeroTransito(nVia: Integer): DWORD;
Var
	s: AnsiString;
begin
	// Esta funci�n devuelve un n�mero de tr�nsito de la forma vvdnnnnnn,
	// donde vv es el n�mero de v�a, d es el n�mero de d�a m�dulo 10,
	// y nnnnnn es la hora actual, expresada en d�cimas de segundo.
	s :=
	  IStr0(nVia, 2) +
	  IStr0(Day(Date) mod 10, 1) +
	  IStr0(Round(Time * 24 * 60 * 60 * 10), 6);
	Result := IVal(s);
	if (Result <= TransitoAnterior) then begin
		// Se nos repiti� el �ltimo valor. Lo cambiamos.
		Inc(Result);
	end;
	TransitoAnterior := Result;
end;


//
function TextoDiferenciaFecha(Fecha: TDateTime): AnsiString;
resourcestring
    STR_NUNCA      = 'nunca';
    STR_AHORA      = 'ahora';
    STR_HOY        = 'hoy';
    STR_MANANA     = 'ma�ana';
    STR_PAS_MANANA = 'pasado ma�ana';
    STR_EL_PROXIMO = 'el pr�ximo ';
    STR_DENTRO_DE  = 'dentro de ';
    STR_DIAS       = ' d�as';
    STR_AYER       = 'ayer';
    STR_ANTEAYER   = 'anteayer';
    STR_EL         = 'el ';
    STR_PASADO     = ' pasado';
    STR_HACE       = 'hace ';
begin
	if Fecha = NULLDATE then begin
		Result := STR_NUNCA;
	end else if Fecha = Now then begin
		Result := STR_AHORA
	end else if Fecha > Now then begin
		if (Fecha < Date + 1) then begin
			Result := STR_HOY;
		end else if (Fecha < Date + 2) then begin
			Result := STR_MANANA;
		end else if (Fecha < Date + 3) then begin
			Result := STR_PAS_MANANA;
		end else if (Fecha < Date + 8) then begin
			Result := STR_EL_PROXIMO + LongDayNames[DayOfWeek(Fecha)];
		end else begin
			Result := STR_DENTRO_DE + IntToStr(Round(Fecha) - Round(Date)) + STR_DIAS;
		end;
	end else begin
		if (Fecha >= Date) then begin
			Result := STR_HOY;
		end else if (Fecha >= Date - 1) then begin
			Result := STR_AYER;
		end else if (Fecha >= Date - 2) then begin
			Result := STR_ANTEAYER;
		end else if (Fecha >= Date - 7) then begin
			Result := STR_EL + LongDayNames[DayOfWeek(Fecha)] + STR_PASADO;
		end else begin
			Result := STR_HACE + IntToStr(Round(Date) - Round(Fecha)) + STR_DIAS;
		end;
	end;
end;
{-----------------------------------------------------------------------------
  Function Name: CargarSistemas
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo: TComboBox; EsVenta: boolean = false; CodigoSistema: integer = 0
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarSistemas(Conn: TADOConnection; Combo: TComboBox; EsVenta: boolean = false; CodigoSistema: integer = 0);
Var
	I: Integer;
	Qry: TADOQuery;
    texto: AnsiString;
begin
    texto := '';
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
        //Texto := ' SELECT * FROM Sistemas  WITH (NOLOCK) ';   //TASK_004_ECA_20160319
        Texto := ' SELECT * FROM Sistemas  WITH (NOLOCK) ';     //TASK_004_ECA_20160319
        //if EsVenta then Texto := Texto + ' WHERE EsVenta = 1 ';  //TASK_004_ECA_20160319
		Qry.SQL.Text := texto;
		Qry.Open;
		Combo.Clear;
		I := -1;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString +
			  Space(200) + Qry.FieldByName('CodigoSistema').AsString);
			if Qry.FieldByName('CodigoSistema').AsInteger = CodigoSistema then begin
            	I := Combo.Items.Count - 1;
            end;
			Qry.Next;
		end;
		Combo.ItemIndex := I;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: MensajeRUTConvenioCuentas
  Author: nefernandez
  Date Created: 08/02/2008
  Description:  Se genera un mensaje para indicar Cuentas suspendidas y Tags
  en lista negra, correspondientes a un Cliente (RUT)
  Parameters: Conn: TADOConnection, NumeroDocumento: AnsiString, SoloActivos: Boolean
  Return Value: AnsiString

  Revision : 34
  Author   : pdominguez
  Date     : 06/10/2010
  Description : SS 927 - CAC - Mensaje de patentes infractoras no corresponde
        - Se modifican los mensajes de convenio con patente(s) infractora(s)
-----------------------------------------------------------------------------}
function  MensajeRUTConvenioCuentas(Conn: TADOConnection; NumeroDocumento: AnsiString; SoloActivos: Boolean = False): AnsiString;
resourcestring
    STR_MSG_UNA_CUENTA_SUSPENDIDA = 'Convenio %s posee cuenta suspendida Patente %s';
    STR_MSG_UN_TAG_LISTA_NEGRA = 'Convenio %s Tag N� %s Patente %s se encuentra en Lista Negra';
    STR_MSG_CUENTAS_SUSPENDIDAS = 'Convenio %s posee m�s de una cuenta suspendida';
    STR_MSG_TAGS_LISTA_NEGRA = 'Convenio %s posee m�s de un Tag en Lista Negra';
    STR_MSG_CONVENIO_UNA_CUENTA_INFRACTORA = 'La Patente %s del convenio %s posee Infracciones Pendientes de Regularizar o en estado Impagas';  //Rev. 30  // SS_927_20101006
    STR_MSG_CONVENIO_CUENTAS_INFRACTORAS = 'Patentes del convenio %s poseen Infracciones Pendientes de Regularizar o en estado Impagas';     //Rev .30  // SS_927_20101006
const
    CUENTA_SUSPENDIDA = 'CS';
    LISTA_NEGRA = 'LN';
    CUENTA_INFRACTORA = 'CI'; //Rev. 29
var
	spObtenerConvenios, spObtenerCuentas: TADOStoredProc;
    spValidaConvenioInfraccionesImpagas : TADOStoredProc; // Rev. 29
    Texto, NumeroConvenio, Patente, Etiqueta: AnsiString;
    cdsConvenioMensaje: TClientDataSet;
begin
    Screen.Cursor   := crHourGlass;    //TASK_033_ECA_20160610
    texto := '';
	spObtenerConvenios := TADOStoredProc.Create(nil);
	spObtenerCuentas := TADOStoredProc.Create(nil);
    spValidaConvenioInfraccionesImpagas := TADOStoredProc.Create(nil); //Rev. 29
    cdsConvenioMensaje := TClientDataSet.Create(Conn);
    try
		spObtenerConvenios.Connection := Conn;
        spObtenerConvenios.ProcedureName:='ObtenerConvenios';
		spObtenerCuentas.Connection := Conn;
        spObtenerCuentas.ProcedureName:='ObtenerCuentas';
        spValidaConvenioInfraccionesImpagas.Connection:= Conn;  //Rev 29
        spValidaConvenioInfraccionesImpagas.ProcedureName:='ValidaConvenioInfraccionesImpagas';    //Rev 29


        cdsConvenioMensaje.FieldDefs.Clear;
        cdsConvenioMensaje.FieldDefs.Add('CodigoConvenio', ftInteger, 0, True);
        cdsConvenioMensaje.FieldDefs.Add('TipoMensaje', ftString, 2, True);
        cdsConvenioMensaje.FieldDefs.Add('Mensaje', ftString, 100);
        cdsConvenioMensaje.IndexFieldNames := 'CodigoConvenio;TipoMensaje';
        cdsConvenioMensaje.CreateDataSet;
        cdsConvenioMensaje.EmptyDataSet;
        cdsConvenioMensaje.Open;

        spObtenerConvenios.Parameters.Refresh;
        spObtenerConvenios.Parameters.ParamByName('@CodigoDocumento').Value := TIPO_DOCUMENTO_RUT;
        spObtenerConvenios.Parameters.ParamByName('@NumeroDocumento').Value := NumeroDocumento;
        spObtenerConvenios.Parameters.ParamByName('@SoloActivos').Value := SoloActivos;
        spObtenerConvenios.Open;
        while not spObtenerConvenios.Eof do begin
            NumeroConvenio := Trim(spObtenerConvenios.FieldByName('NumeroConvenioFormateado').AsString);
                //Rev. 29
                spValidaConvenioInfraccionesImpagas.Parameters.Refresh;
                with spValidaConvenioInfraccionesImpagas, Parameters do begin
                    ParamByName('@CodigoConvenio').Value := spObtenerConvenios.FieldByName('CodigoConvenio').AsInteger;
                    ExecProc;
                    if not ParamByName('@NoTieneInfraccionesPendientesPago').Value then begin
                        //Rev. 30
                        if ParamByName('@cantidadCuentasInfractoras').Value = 1 then begin
                            cdsConvenioMensaje.Append;
                            cdsConvenioMensaje.FieldByName('CodigoConvenio').AsInteger:= spObtenerConvenios.FieldByName('CodigoConvenio').AsInteger;
                            cdsConvenioMensaje.FieldByName('TipoMensaje').AsString := CUENTA_INFRACTORA;
                            cdsConvenioMensaje.FieldByName('Mensaje').AsString := format(STR_MSG_CONVENIO_UNA_CUENTA_INFRACTORA, [TrimRight(ParamByName('@PatenteInFractora').Value),RightStr(NumeroConvenio,3)]);
                            cdsConvenioMensaje.Post;
                        end else begin
                            cdsConvenioMensaje.Append;
                            cdsConvenioMensaje.FieldByName('CodigoConvenio').AsInteger:= spObtenerConvenios.FieldByName('CodigoConvenio').AsInteger;
                            cdsConvenioMensaje.FieldByName('TipoMensaje').AsString := CUENTA_INFRACTORA;
                            cdsConvenioMensaje.FieldByName('Mensaje').AsString := format(STR_MSG_CONVENIO_CUENTAS_INFRACTORAS, [RightStr(NumeroConvenio,3)]);
                            cdsConvenioMensaje.Post;
                        end;   //End. rev 30
                    end;
                end;
                //end Rev. 29

            spObtenerCuentas.Parameters.Refresh;
            spObtenerCuentas.Parameters.ParamByName('@CodigoConvenio').Value := spObtenerConvenios.FieldByName('CodigoConvenio').Value;
            spObtenerCuentas.Parameters.ParamByName('@TraerBaja').Value := 1;
            spObtenerCuentas.Open;
            while not spObtenerCuentas.Eof do begin
                Patente := Trim(spObtenerCuentas.FieldByName('Patente').AsString);
                Etiqueta := Trim(spObtenerCuentas.FieldByName('Etiqueta').AsString);

                if (spObtenerCuentas.FieldByName('CodigoEstadoCuenta').AsInteger = ESTADO_CUENTA_VEHICULO_SUSPENDIDA) then begin // Cuenta Suspendida
                    cdsConvenioMensaje.First;
                    if cdsConvenioMensaje.FindKey([spObtenerConvenios.FieldByName('CodigoConvenio').AsInteger,CUENTA_SUSPENDIDA]) then begin
                        cdsConvenioMensaje.Edit;
                        cdsConvenioMensaje.FieldByName('Mensaje').AsString := format(STR_MSG_CUENTAS_SUSPENDIDAS,[NumeroConvenio]);
                        cdsConvenioMensaje.Post;
                    end
                    else begin
                        cdsConvenioMensaje.Append;
                        cdsConvenioMensaje.FieldByName('CodigoConvenio').AsInteger:= spObtenerConvenios.FieldByName('CodigoConvenio').AsInteger;
                        cdsConvenioMensaje.FieldByName('TipoMensaje').AsString := CUENTA_SUSPENDIDA;
                        cdsConvenioMensaje.FieldByName('Mensaje').AsString := format(STR_MSG_UNA_CUENTA_SUSPENDIDA, [NumeroConvenio, Patente]);
                        cdsConvenioMensaje.Post;
                    end;
                end;
                if (spObtenerCuentas.FieldByName('IndicadorListaNegra').AsBoolean) then begin // TAG en Lista Negra
                    cdsConvenioMensaje.First;
                    if cdsConvenioMensaje.FindKey([spObtenerConvenios.FieldByName('CodigoConvenio').AsInteger,LISTA_NEGRA]) then begin
                        cdsConvenioMensaje.Edit;
                        cdsConvenioMensaje.FieldByName('Mensaje').AsString := format(STR_MSG_TAGS_LISTA_NEGRA,[NumeroConvenio]);
                        cdsConvenioMensaje.Post;
                    end
                    else begin
                        cdsConvenioMensaje.Append;
                        cdsConvenioMensaje.FieldByName('CodigoConvenio').AsInteger:= spObtenerConvenios.FieldByName('CodigoConvenio').AsInteger;
                        cdsConvenioMensaje.FieldByName('TipoMensaje').AsString := LISTA_NEGRA;
                        cdsConvenioMensaje.FieldByName('Mensaje').AsString := format(STR_MSG_UN_TAG_LISTA_NEGRA, [NumeroConvenio, Etiqueta, Patente]);
                        cdsConvenioMensaje.Post;
                    end;
                end;


                spObtenerCuentas.Next;
            end;
            spObtenerCuentas.Close;
            spValidaConvenioInfraccionesImpagas.Close;     //Rev. 29

            spObtenerConvenios.Next;
        end;
        cdsConvenioMensaje.First;
        while not cdsConvenioMensaje.Eof do begin
            Texto := Texto + cdsConvenioMensaje.FieldByName('Mensaje').AsString + CRLF;
            cdsConvenioMensaje.Next;
        end;
    finally
    	spObtenerConvenios.Close;
    	spObtenerConvenios.Free;
    	spObtenerCuentas.Close;
    	spObtenerCuentas.Free;
        cdsConvenioMensaje.Close;
        cdsConvenioMensaje.Free;
        //Rev. 29
        spValidaConvenioInfraccionesImpagas.Close;
        spValidaConvenioInfraccionesImpagas.Free;
        //End. Rev. 29
    end;
    result := texto;
    Screen.Cursor   := crDefault; //TASK_033_ECA_20160610
end;

{-----------------------------------------------------------------------------
  Function Name: CargarConveniosRUT
  Author:
  Date Created:
  Description:
  Parameters:
  Return Value:

  Revision : 1
  Author: dAllegretti
  Date Created: 15/10/2008
  Description: Agrego la funcionalidad de diferenciar los convenios sin cuenta de los
                que lo tienen.

  Revision : 2
  Author: pDominguez
  Date  : 27/09/2010
  Description: SS 637 - Refinanciaci�n
    - Se agrega el par�metro NumeroFormateado, para poder devolver los n�meros sin
    el formateo actual
-----------------------------------------------------------------------------}
procedure CargarConveniosRUT(Conn: TADOConnection; Combo: TVariantComboBox; CodigoDocumento,
  NumeroDocumento: AnsiString; SoloActivos: Boolean = False; CodigoConvenio: Integer = 0;
  TieneItemSeleccionar: Boolean = False; NumeroFormateado: Boolean = True; SeleccionTodos: Boolean = False; CodigoTipoConvenio: Integer = 0);    //SS_1120_MVI_20130820 //TASK_007_ECA_20160428
Var
	i, CodigoCliente, AnchoConvenio: Integer;                                                                    //SS_1120_MVI_20130820
	SP: TADOStoredProc;
    texto: AnsiString;
    EsConvenioSinCuenta: Boolean;
    Predeterminado : string;    //TASK_012_ECA_20160514
begin
//BEGIN : SS_1120_MVI_20130820-------------------------------------------------------------------------------------
//    texto := '';
//	SP := TADOStoredProc.Create(nil);
//	try
//		SP.Connection := Conn;
//        sp.ProcedureName:='ObtenerConvenios';
//        with sp.Parameters do begin
//            Refresh;
//            ParamByName('@CodigoDocumento').Value := CodigoDocumento;
//            ParamByName('@NumeroDocumento').Value := NumeroDocumento;
//            ParamByName('@SoloActivos').Value := SoloActivos;
//        end;
//        sp.Open;
//		Combo.Clear;
//		i := -1;
//
//        if TieneItemSeleccionar then begin
//			Combo.Items.Add(SELECCIONAR, 0);
//            i := 0;
//        end;
//
//		While not SP.Eof do begin
//            //Revision 1
//            if NumeroFormateado then begin // SS_637_20100927
//                EsConvenioSinCuenta := StrToBool(QueryGetValue(DMConnections.BaseCAC, Format('SELECT dbo.EsConvenioSinCuenta(%d)', [SP.FieldByName('CodigoConvenio').AsInteger])));
//                if EsConvenioSinCuenta then texto := SP.FieldByName('NumeroConvenioFormateado').AsString + TXT_CONVENIO_SIN_CUENTA
//                else texto := SP.FieldByName('NumeroConvenioFormateado').AsString;
//            end // SS_637_20100927
//            else texto := Trim(SP.FieldByName('NumeroConvenio').AsString); // SS_637_20100927
//            //FIN Revision 1
//			Combo.Items.Add(texto, SP.FieldByName('CodigoConvenio').AsString);
//			if SP.FieldByName('CodigoConvenio').AsInteger = CodigoConvenio then begin
//            	i := Combo.Items.Count - 1;
//            end;
//			SP.Next;
//		end;
//		if (i <> -1) then Combo.ItemIndex := i
//        else Combo.ItemIndex := 0;
//	finally
//		SP.Close;
//		SP.Free;
//	end;
//END : SS_1120_MVI_20130820-------------------------------------------------------------------------------------


//BEGIN : SS_1120_MVI_20130820-------------------------------------------------------------------------------------
  try
    SP := TADOStoredProc.Create(nil);
    SP.Connection := Conn;
    sp.ProcedureName:='ObtenerCliente';
    with sp.Parameters do
    begin
      Refresh;
      ParamByName('@NumeroDocumento').value := PadL(Trim(NumeroDocumento), 9, '0' );          //SS_1118_MVI_20131007
      ParamByName('@CodigoDocumento').value := TIPO_DOCUMENTO_RUT;

      SP.Open;
      if not SP.eof then
      begin
        CodigoCliente := SP.FieldByName('CodigoCliente').AsInteger;
      end;
    end;
  finally
    SP.Close;
    SP.Free;
  end;

  if(CodigoCliente <> 0) then
  begin
    SP := TADOStoredProc.Create(nil);
    SP.Connection := Conn;

    try
      SP.Connection := Conn;
      sp.ProcedureName:='ObtenerConveniosCliente';
      with sp.Parameters do
      begin
          Refresh;
          ParamByName('@CodigoCliente').Value := CodigoCliente;
          ParamByName('@IncluirConveniosDeBaja').Value := not SoloActivos;
          //INICIO: TASK_007_ECA_20160428
          if CodigoTipoConvenio > 0 then
          begin
             ParamByName('@CodigoTipoConvenio').value := CodigoTipoConvenio;
          end;
          //FIN: TASK_007_ECA_20160428
      end;
      sp.Open;
      Combo.Clear;
      i := -1;

      if TieneItemSeleccionar then
      begin
        Combo.Items.Add(SELECCIONAR, 0);
        i := 0;
      end;

      if SeleccionTodos then
      begin
        Combo.Items.Add(TODOS, 0);
        i := -1;
      end;

      AnchoConvenio := 150;

      While not SP.Eof do
      begin
      //INICIO: TASK_007_ECA_20160428
//         if NumeroFormateado then
//         begin
//           texto :=   SP.Fieldbyname('NumeroConvenioFormateado').AsString +
//                      IfThen(SP.Fieldbyname('SinCuenta').AsBoolean,TXT_CONVENIO_SIN_CUENTA,'')+
//                      IfThen(SP.Fieldbyname('CodigoEstadoConvenio').AsInteger=2,TXT_CONVENIO_DE_BAJA,'');
//
//            if SP.Fieldbyname('SinCuenta').AsBoolean then AnchoConvenio := 190;
//         end
//         else
//           texto := Trim(SP.FieldByName('NumeroConvenio').AsString);

        //INICIO: TASK_012_ECA_20160514
        Predeterminado:='';
        if SP.Fieldbyname('Predeterminado').AsInteger=1 then
           Predeterminado:='Predeterminado';

        if NumeroFormateado then
         begin
//           texto :=   Trim(SP.Fieldbyname('NumeroConvenioFormateado').AsString) + ' Convenio ' + //TASK_046_ECA_20160702
           texto :=   Trim(SP.Fieldbyname('NumeroConvenio').AsString) + ' Convenio ' +
                      SP.Fieldbyname('TipoConvenio').AsString + ' ' +
                      IfThen(SP.Fieldbyname('CodigoEstadoConvenio').AsInteger=2,TXT_CONVENIO_DE_BAJA,'') +
                      Predeterminado;


         end
         else
           texto := Trim(SP.FieldByName('NumeroConvenio').AsString) + ' Convenio ' +
                    SP.Fieldbyname('TipoConvenio').AsString + Predeterminado;
        //FIN: TASK_012_ECA_20160514
        AnchoConvenio := 350;
        //FIN: TASK_007_ECA_20160428

         Combo.Items.Add(texto, SP.FieldByName('CodigoConvenio').AsString);

         if SP.FieldByName('CodigoConvenio').AsInteger = CodigoConvenio then
         begin
            i := Combo.Items.Count - 1;
         end;
         SP.Next;
      end;

      if (i <> -1) then
        Combo.ItemIndex := i
      else
        Combo.ItemIndex := 0;

      Combo.DroppedWidth := AnchoConvenio;

    finally
      SP.Close;
      SP.Free;
    end;
  end;
//END : SS_1120_MVI_20130820-------------------------------------------------------------------------------------

end;

{-----------------------------------------------------------------------------
  Function Name: CargarSistemas(
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo: TComboBox; CodigoSistema: integer = 0; Mostrar: boolean = False
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarSistemas(Conn: TADOConnection; Combo: TComboBox; CodigoSistema: integer = 0; Mostrar: boolean = False); overload;
Var
	i: Integer;
    texto: AnsiString;
    sp: TADOStoredProc;
begin
    texto := '';
	sp := TADOStoredProc.Create(nil);
	try
		sp.Connection := Conn;
        sp.ProcedureName := 'ADM_SistemasModulos_Aplicaciones_SELECT';
		sp.Open;
		Combo.Clear;
		i := -1;
		While not sp.Eof do begin
			Combo.Items.Add(sp.FieldByName('Descripcion').AsString +
			  Space(200) + sp.FieldByName('CodigoSistema').AsString);
			if sp.FieldByName('CodigoSistema').AsInteger = CodigoSistema then begin
            	i := Combo.Items.Count - 1;
            end;
			sp.Next;
		end;
		Combo.ItemIndex := i;
	finally
		sp.Close;
		sp.Free;
	end;
end;

// Busca una referencia dentro de un combo y devuelve su posici�n
function IndexCombo(Combo: TComboBox; Referencia: String) : Integer;
var
  Index : Integer;
begin
	Index := Combo.Items.Count - 1;
	While (Index <> -1) And (Copy(Combo.Items[Index], 1, 200) <> Referencia)
	  Do Dec (Index);

	if (Index = -1) then Index := 0;

    result := Index;
end;


function ValidarRUT(Conn: TADOConnection;Numero:String):Boolean;
begin
    if (Length(trim(Numero))>10) or (Length(trim(Numero))<8)then begin result:=False; exit; end;
    numero:=strright('000000000000000'+trim(numero),10);
    result:=QueryGetValueInt(Conn,format('EXEC VerificarRUT ''%s'',''%s''',[strleft(Numero,9),strright(numero,1)]))=1;
end;

function ValidarVehiculoOtraConcesionaria(Conn: TADOConnection; Patente: string): Boolean;
begin
    result := QueryGetValueInt(Conn,format('EXEC ValidarVehiculoOtraConcesionaria ''%s''',[Trim(Patente)])) = 1;
end;

//Revision 5 Se permite la verificaci�n del nuevo formato de patentes AAAA00
function CaracteresIngresadosSonDePatenteChilena(Patente: String): boolean;
begin
    if Patente[3] in ['0'..'9'] then begin
        result := (Length(Patente) = 6) and
          (UpperCase(Patente)[1] in ['A'..'Z']) and
          (UpperCase(Patente)[2] in ['A'..'Z']) and
          (Patente[3] in ['0'..'9']) and
          (Patente[4] in ['0'..'9']) and
          (Patente[5] in ['0'..'9']) and
          (Patente[6] in ['0'..'9']);
    end
    else if Patente[4] = '0' then begin                                         //SS_1189_MCA_20140929 //moto   AAA099
        result := (Length(Patente) = 6) and                                     //SS_1189_MCA_20140929
          (UpperCase(Patente)[1] in ['A'..'Z']) and                             //SS_1189_MCA_20140929
          (UpperCase(Patente)[2] in ['A'..'Z']) and                             //SS_1189_MCA_20140929
          (UpperCase(Patente)[3] in ['A'..'Z']) and                             //SS_1189_MCA_20140929
          (Patente[4] = '0') and                                                //SS_1189_MCA_20140929
          (Patente[5] in ['0'..'9']) and                                        //SS_1189_MCA_20140929
          (Patente[6] in ['0'..'9']);                                           //SS_1189_MCA_20140929
    end                                                                         //SS_1189_MCA_20140929
    else begin
        result := (Length(Patente) = 6) and
          (UpperCase(Patente)[1] in ['A'..'Z']) and
          (UpperCase(Patente)[2] in ['A'..'Z']) and
          (UpperCase(Patente)[3] in ['A'..'Z']) and
          (UpperCase(Patente)[4] in ['A'..'Z']) and
          (Patente[5] in ['0'..'9']) and
          (Patente[6] in ['0'..'9']);
    end;
end;

function ValidarNumeroCuentaBancaria(Conn: TADOConnection;CodigoTipoCuenta:Integer;Numero:String):Boolean;
begin
    result:=QueryGetValueInt(Conn,format('SELECT CONVERT(INT,DBO.ValidarNumeroCuentaBancaria(%d,''%s''))',[CodigoTipoCuenta,Numero]))=1;
end;
{-----------------------------------------------------------------------------
  Function Name: CargarTiposSueldo
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo: TComboBox; CodigoTipoSueldo: integer = 0
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarTiposSueldo(Conn: TADOConnection; Combo: TComboBox; CodigoTipoSueldo: integer = 0);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'SELECT * FROM TiposSueldo WITH (NOLOCK) ';
		Qry.Open;
		Combo.Clear;
		i := -1;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString +
			  Space(200) + Qry.FieldByName('CodigoTipoSueldo').AsString);
			if (Qry.FieldByName('CodigoTipoSueldo').AsInteger = CodigoTipoSueldo) then
			  i := Combo.Items.Count - 1;
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

{INICIO: 	20160720 CFU
procedure CargarAlmacenes(Conn: TADOConnection; Combo: TVariantComboBox; CodigoAlmacen: Integer = 0; todos : Boolean = true);
}
procedure CargarAlmacenes(Conn: TADOConnection; Combo: TVariantComboBox; Condicion: string; CodigoAlmacen: Integer = 0; todos : Boolean = true);
//TERMINO: 20160720
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
        {INICIO: 	20160720 CFU
		Qry.SQL.Text := 'SELECT CodigoAlmacen, Descripcion FROM MaestroAlmacenes WITH (NOLOCK) ORDER BY Descripcion';
        }
        Qry.SQL.Text := 'SELECT CodigoAlmacen, Descripcion FROM MaestroAlmacenes WITH (NOLOCK) ';
        if Condicion <> '' then
        Qry.SQL.Text := Qry.SQL.Text + 'WHERE	' + Condicion;
        Qry.SQL.Text := Qry.SQL.Text + 'ORDER BY Descripcion';
        //TERMINO: 20160720
		Qry.Open;
		Combo.Clear;
        i := -1;
        if Todos then begin
			Combo.Items.Add(TPA_SELECCIONE, 0);
            i := 0;
		end;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString,Qry.FieldByName('CodigoAlmacen').AsInteger);
			if Qry.FieldByName('CodigoAlmacen').AsInteger = CodigoAlmacen then
				i := Combo.Items.Count - 1;
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;
{-----------------------------------------------------------------------------
  Function Name: CargarMaestroAlmacenes
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo: TComboBox; CodigoAlmacen: integer = 0; TieneItemNinguno: boolean = false
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarMaestroAlmacenes(Conn: TADOConnection; Combo: TComboBox; CodigoAlmacen: integer = 0; TieneItemNinguno: boolean = false);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
        //Rev.32 / 20-Abril-2011 / Nelson Droguett Sierra
		Qry.SQL.Text := 'SELECT * FROM MaestroAlmacenes WITH (NOLOCK) ORDER BY Descripcion ASC';  //PAR00133-NDR-20110420
		Qry.Open;
		Combo.Clear;
		i := -1;

        if TieneItemNinguno then begin
			Combo.Items.Add(SIN_ESPECIFICAR + Space(200) + '');
            i := 0;
        end;

		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString +
			  Space(200) + Qry.FieldByName('CodigoAlmacen').AsString);
			if (Qry.FieldByName('CodigoAlmacen').AsInteger = CodigoAlmacen) then
			  i := Combo.Items.Count - 1;
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;
{-----------------------------------------------------------------------------
  Function Name: CargarBodegas
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo: TVariantComboBox; CodigoBodega: Integer = 0; Todos: Boolean = true
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarBodegas(Conn: TADOConnection; Combo: TVariantComboBox; CodigoBodega: Integer = 0; Todos: Boolean = true);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
        //Rev.32 / 20-Abril-2011 / Nelson Droguett Sierra
		Qry.SQL.Text := 'SELECT * FROM Bodegas WITH (NOLOCK) ORDER BY Descripcion ASC';   //PAR00133-NDR-20110420
		Qry.Open;
		Combo.Clear;
		i := -1;
        if Todos then begin
			Combo.Items.Add(TPA_SELECCIONE, 0);
            i := 0;
		end;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString,Qry.FieldByName('CodigoBodega').AsInteger);
			if Qry.FieldByName('CodigoBodega').AsInteger = CodigoBodega then
				i := Combo.Items.Count - 1;
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarBodegas
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo: TComboBox; CodigoBodega: integer = 0; TieneItemNinguno: boolean = false
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarBodegas(Conn: TADOConnection; Combo: TComboBox; CodigoBodega: integer = 0; TieneItemNinguno: boolean = false);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
       //Rev.32 / 20-Abril-2011 / Nelson Droguett Sierra
		Qry.SQL.Text := 'SELECT * FROM Bodegas WITH (NOLOCK) ORDER BY Descripcion ASC';     //PAR00133-NDR-20110420
		Qry.Open;
		Combo.Clear;
		i := -1;

        if TieneItemNinguno then begin
			Combo.Items.Add(TPA_SELECCIONE + Space(200) + '');
            i := 0;
        end;

		While not Qry.Eof do begin
			Combo.Items.Add(Trim(Qry.FieldByName('Descripcion').AsString) +
			  Space(200) + Qry.FieldByName('CodigoBodega').AsString);
			if (Qry.FieldByName('CodigoBodega').AsInteger = CodigoBodega) then
			  i := Combo.Items.Count - 1;
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;
{-----------------------------------------------------------------------------
  Function Name: CargarTiposProcesos
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo: TComboBox; CodigoProceso: integer = 0
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarTiposProcesos(Conn: TADOConnection; Combo: TComboBox; CodigoProceso: integer = 0);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'SELECT * FROM TiposProcesos WITH (NOLOCK) ';
		Qry.Open;
		Combo.Clear;
		i := -1;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString +
			  Space(200) + Istr(Qry.FieldByName('CodigoProceso').AsInteger));
			if (Qry.FieldByName('CodigoProceso').AsInteger = CodigoProceso) then begin
            	i := Combo.Items.Count - 1;
            end;
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

procedure CargarUnidadesToleranciaCategoria(Combo: TComboBox; Unidad: AnsiString = '');
resourcestring
    MSG_NINGUNA = '<Ninguna>';
    MSG_CENTIMETROS = 'Cent�metros';
    MSG_PORCENTAJE  = 'Por ciento';
begin
	Combo.Clear;
    //Combo.Items.Add(MSG_NINGUNA + Space(200) +  'N');
	Combo.Items.Add(MSG_CENTIMETROS + Space(200) +  'C');
	Combo.Items.Add(MSG_PORCENTAJE + Space(200) + 'P');
    //Tiene que estar uno seleccionado, si no indica porcentaje, toma Centimetros
    if Trim(UpperCase(Unidad)) = 'P' then Combo.Itemindex := 1
            else Combo.Itemindex := 0;
	(* Esto que viene es porque antes se pod�a tener como opci�n "Ninguna".
    if Trim(UpperCase(Unidad)) = 'C' then Combo.Itemindex := 1
	  else  if Trim(UpperCase(Unidad)) = 'P' then Combo.Itemindex := 2
            else Combo.Itemindex := 0;
    *)
end;

{-----------------------------------------------------------------------------
  Function Name: CargarConfiguracionNotificaciones
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo: TComboBox; CodigoConfiguracion: integer = 0
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarConfiguracionNotificaciones(Conn: TADOConnection; Combo: TComboBox; CodigoConfiguracion: integer = 0);
Var
	i: Integer;
	Qry: TADOQuery;
begin
    //
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'SELECT * FROM ConfiguracionNotificaciones  WITH (NOLOCK) WHERE Activado = 1';
		Qry.Open;
		Combo.Clear;
		i := -1;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('DescripcionAccion').AsString +
			  Space(200) + Qry.FieldByName('ID').AsString);
			if Qry.FieldByName('ID').AsInteger = CodigoConfiguracion then begin
            	i := Combo.Items.Count - 1;
            end;
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarCategoriasConsultas
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo: TComboBox; CodigoCategoria: integer = 0
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarCategoriasConsultas(Conn: TADOConnection; Combo: TComboBox; CodigoCategoria: integer = 0);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection	:= Conn;
		Qry.SQL.Text	:= 'SELECT * FROM CategoriasConsultas  WITH (NOLOCK) ORDER BY CodigoCategoria';
		Qry.Open;
		Combo.Clear;
		i := -1;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString +
			  Space(200) + Qry.FieldByName('CodigoCategoria').AsString);
			if Qry.FieldByName('CodigoCategoria').AsInteger = CodigoCategoria then begin
            	i := Combo.Items.Count - 1;
            end;
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;


procedure ClickCheckEliminar(var checkEliminar: TCheckBox; ElegidoParaEntrega: boolean);
resourcestring
    ES_DOMICILIO_ENTREGA = 'Atenci�n: El domicilio que selecciona para eliminar es el Domicilio de entrega ' + #13#10 +
						   'Tendr� que indicar otro domicilio para entrega. �Esta seguro?';
    CAPTION_ELIMINAR_DOMICILIO = 'Eliminar domicilio';
begin
    //Posibilidades, 3:
	//1- Que sea el domicilio de entrega el que quiere eliminar (negrita!)
    //2- Que sea el domicilio de entrega y lo desmarque
    //3- que no sea el domicilio de entrega, y lo marque (negrita)
    //if (not checkEliminar.Checked) then exit;
    if  (not checkEliminar.Checked) then begin
        checkEliminar.Font.Style := [];
    end
    else begin
        if  (ElegidoParaEntrega) then begin
            //if (CodigoDomicilioParticular = CodigoDomicilioEntrega) then begin
                if MsgBox(ES_DOMICILIO_ENTREGA, CAPTION_ELIMINAR_DOMICILIO,
                    MB_ICONWARNING or MB_YESNO) = IDYES then begin
                        checkEliminar.Font.Style := [fsBold]
                end
                else begin
                    checkEliminar.Font.Style := [];
                    //checkEliminar.Checked := false;
                    checkEliminar.State := cbUnchecked;
                end;
            //end else checkEliminar.Font.Style := [fsBold];
        end
        else
            checkEliminar.Font.Style := [fsBold];
    end;
end;

procedure TReportItem.SetCodigo(const Value: Integer);
begin
	if Value = FCodigo then Exit;
    FCodigo := Value;
end;

{
INICIO BLOQUE SS_979_ALA_20110929
procedure TReportItem.SetConnection(const Value: TADOConnection);
begin
	if Value = FConnection then Exit;
    FConnection := Value;
end;
TERMINO BLOQUE SS_979_ALA_20110929
}
//INCIO BLOQUE PAR00133_COPAMB_ALA_20110801
procedure TReportItem.SetConnectionDatosBase(const Value: TADOConnection);
begin
    if Value = FConnectionDatosBase then Exit;
    FConnectionDatosBase := Value;
end;
//TERMINO BLOQUE PAR00133_COPAMB_ALA_20110801

procedure TInterfaceItem.SetCodigo(const Value: Integer);
begin
	if Value = FCodigo then Exit;
    FCodigo := Value;
end;

procedure TInterfaceItem.SetConnection(const Value: TADOConnection);
begin
	if Value = FConnection then Exit;
    FConnection := Value;
end;

{-----------------------------------------------------------------------------
  Function Name: MyItemClick
  Author:
  Date Created:  /  /
  Description:
  Parameters: Sender: TObject
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

  Revision :2
      Author : vpaszkowicz
      Date : 27/04/2009
      Description : SS 744
			-Rehago todo el tema de las contrasenias en reportes.
-----------------------------------------------------------------------------}
procedure TReportItem.MyItemClick(Sender: TObject);
resourcestring
    CAPTION_INFORMES            = 'Informes';
    MSG_ERROR_CARGANDO_CONSULTA = 'Error cargando la consulta.';
    MSG_CONSULTA_NO_ENCONTRADA  = 'La consulta no fue encontrada.';
    MSG_TIEMPO_ESTIMADO         = 'El reporte tiene un tiempo de demora estimado en %d segundos. �Desea continuar?';   // TASK_054_MGO_20160728
Var
	f: TFormRptInformeUsuario;
    spObtenerConsultaImprimir: TADOStoredProc;
begin
    spObtenerConsultaImprimir := TADOStoredProc.Create(Self);
    try
        try
            spObtenerConsultaImprimir.Connection := DMConnections.BaseCAC;
            spObtenerConsultaImprimir.CommandTimeout := 300;
            spObtenerConsultaImprimir.ProcedureName := 'ObtenerConsultaImprimir';
            spObtenerConsultaImprimir.Parameters.Refresh;
            spObtenerConsultaImprimir.Parameters.ParamByName('@CodigoConsulta').Value := TReportItem(Sender).Codigo;
            spObtenerConsultaImprimir.Open;

            if spObtenerConsultaImprimir.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
                raise Exception.Create(spObtenerConsultaImprimir.Parameters.ParamByName('@ErrorDescription').Value);

            // INICIO : TASK_054_MGO_20160728
            // Si el reporte tiene un tiempo estimado definido, se muestra confirmaci�n
            if spObtenerConsultaImprimir.FieldByName('TiempoEstimado').AsInteger > 0 then begin
                if MsgBox(Format(MSG_TIEMPO_ESTIMADO, [spObtenerConsultaImprimir.FieldByName('TiempoEstimado').AsInteger]),
                                    TXT_ATENCION, MB_ICONQUESTION+MB_YESNO) = IDNO then
                    Exit;
            end;
            // FIN : TASK_054_MGO_20160728

            if not spObtenerConsultaImprimir.Eof then begin
                Application.CreateForm(TFormRptInformeUsuario, f);
                if f.Inicializar(TReportItem(Sender).Codigo,
                   spObtenerConsultaImprimir.FieldByName('Descripcion').AsString,
                   spObtenerConsultaImprimir.FieldByName('TextoSQL').AsString,
                   spObtenerConsultaImprimir.FieldByName('CantParametros').AsInteger,
                   spObtenerConsultaImprimir.FieldByName('TipoGrafico').AsString,
                   spObtenerConsultaImprimir.FieldByName('CampoDatosGrafico').AsString,
                   spObtenerConsultaImprimir.FieldByName('CampoValoresGrafico').AsString,
                   TReportItem(Sender).ConnectionDatosBase) then
                    f.rbiReporte.Execute;
                f.Release;
            end else begin
                MsgBox(MSG_CONSULTA_NO_ENCONTRADA, CAPTION_INFORMES, MB_ICONSTOP);
            end;
        except
            On e: exception do
    		    MsgBoxErr(MSG_ERROR_CARGANDO_CONSULTA, E.message, CAPTION_INFORMES, MB_ICONSTOP);
        end;
    finally
        spObtenerConsultaImprimir.Free;
    end;
end;

procedure TInterfaceItem.MyItemClick(Sender: TObject);
begin
    EjecutarInterfaz(TInterfaceItem(Sender).Connection, TInterfaceItem(Sender).Codigo);
end;

{-----------------------------------------------------------------------------
  Function Name: EjecutarInterfaz
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Codigo: integer
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure EjecutarInterfaz(Conn: TADOConnection; Codigo: integer);
resourcestring
    CAPTION_INTERFACES                     = 'Interfaces';
    MSG_ERROR_CARGANDO_INTERFAZ            = 'Error cargando la interfaz.';
    MSG_INTERFAZ_NO_ENCONTRADA             = 'La interfaz no fue encontrada.';
    MSG_CONFIRMACION                       = '�Est� seguro que desea continuar con la operaci�n?';
Var
	Qry: TADOQuery;
    NombreArchivo, NombreInterfaz: AnsiString;
begin
    if (MsgBox(MSG_CONFIRMACION, CAPTION_INTERFACES, MB_YESNO + MB_ICONQUESTION) = IDYES) then
    begin
        Screen.Cursor := crHourGlass;
	    Qry := TAdoQuery.Create(nil);
    	try
	    	Qry.Connection := Conn;
		    Qry.SQL.Text := 'SELECT TextoSQL, Directorio, Tipo, Interfaz, Separador FROM Interfaces  WITH (NOLOCK) WHERE CodigoInterfaz = ' + IntToStr(Codigo);
    		Qry.Open;
	        if not Qry.Eof then begin
                if (Qry.FieldByName('Interfaz').AsString = 'E') then
                begin
                    NombreInterfaz := 'IE'+ RightStr(('0000' + IntToStr(Codigo)), 4);
                    NombreArchivo := NombreInterfaz + '.' + Qry.FieldByName('Tipo').AsString;
                    if (Qry.FieldByName('Tipo').AsString = 'XML') then EjecutarInterfazEntradaXML(Conn, Codigo, Qry.FieldByName('TextoSQL').AsString, NombreInterfaz, NombreArchivo, Qry.FieldByName('Directorio').AsString) else EjecutarInterfazEntradaTXT(Conn, Codigo, Qry.FieldByName('TextoSQL').AsString, NombreInterfaz, NombreArchivo, Qry.FieldByName('Directorio').AsString, Qry.FieldByName('Separador').AsString);
                end
                else
                begin
                    NombreInterfaz := 'IS' + RightStr(('0000' + IntToStr(Codigo)), 4) + FormatDateTime('yyyymmddhhnnss', Now);
                    NombreArchivo := NombreInterfaz + '.' + Qry.FieldByName('Tipo').AsString;
                    if (Qry.FieldByName('Tipo').AsString = 'XML') then EjecutarInterfazSalidaXML(Conn, Codigo, Qry.FieldByName('TextoSQL').AsString, NombreInterfaz, NombreArchivo, Qry.FieldByName('Directorio').AsString) else EjecutarInterfazSalidaTXT(Conn, Codigo, Qry.FieldByName('TextoSQL').AsString, NombreInterfaz, NombreArchivo, Qry.FieldByName('Directorio').AsString, Qry.FieldByName('Separador').AsString);
                end;
                Screen.Cursor := crHourGlass;
            end
            else begin
                Screen.Cursor := crDefault;
                MsgBox(MSG_INTERFAZ_NO_ENCONTRADA, CAPTION_INTERFACES, MB_ICONSTOP);
                Screen.Cursor := crHourGlass;
            end;
        except
		    On e: exception do begin
                Screen.Cursor := crDefault;
        		MsgBoxErr(MSG_ERROR_CARGANDO_INTERFAZ, E.message, CAPTION_INTERFACES, MB_ICONSTOP);
                Screen.Cursor := crHourGlass;
	    	end;
        end;
	    Qry.Close;
    	Qry.Free;
        Screen.Cursor := crDefault;
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: CargarActividadesPersona
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo: TComboBox; Actividad: integer = 0
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarActividadesPersona(Conn: TADOConnection; Combo: TComboBox; Actividad: integer = 0);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection  := Conn;
        //Rev.32 / 20-Abril-2011 / Nelson Droguett Sierra
		Qry.SQL.Text    := 'SELECT * from Actividades WITH (NOLOCK) ORDER BY Descripcion ASC';        //PAR00133-NDR-20110420
		Qry.Open;
		Combo.Clear;
		i := -1;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString + Space(200) + Trim(Qry.FieldByName('CodigoActividad').AsString));
			if Qry.FieldByName('CodigoActividad').AsInteger = Actividad then
			  i := Combo.Items.Count - 1;
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;
{-----------------------------------------------------------------------------
  Function Name: CargarTipoDomicilio
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo: TComboBox; TipoDomicilio: integer = 0
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarTipoDomicilio(Conn: TADOConnection; Combo: TComboBox; TipoDomicilio: integer = 0);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection  := Conn;
		Qry.SQL.Text    := 'SELECT * from TiposDomicilio WITH (NOLOCK) ';
		Qry.Open;
		Combo.Clear;
		i := -1;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString + Space(200) + Trim(Qry.FieldByName('CodigoTipoDomicilio').AsString));
			if Qry.FieldByName('CodigoTipoDomicilio').AsInteger = TipoDomicilio then
			  i := Combo.Items.Count - 1;
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

procedure EjecutarInterfazSalidaXML(Conn: TADOConnection; CodigoInterfaz: integer; TextoSQL: AnsiString; NombreInterfaz: AnsiString; NombreArchivo: AnsiString; Directorio: AnsiString);
resourcestring
    CAPTION_INTERFACES                     = 'Interfaces';
    MSG_OPERACION_FINALIZADA_CORRECTAMENTE = 'La operaci�n finaliz� correctamente.';
    MSG_ARCHIVO_EXISTENTE                  = 'El archivo ya existe. �Desea continuar?';
var
    i: integer;
	Qry: TADOQuery;
    Linea: AnsiString;
    Archivo: TextFile;
begin
    Screen.Cursor := crHourGlass;
    Qry := TAdoQuery.Create(nil);
    try
        if (FileExists(Directorio + NombreArchivo) = True) then
        begin
           	Screen.Cursor := crDefault;
            if not(MsgBox(MSG_ARCHIVO_EXISTENTE, CAPTION_INTERFACES, MB_YESNO + MB_ICONQUESTION) = IDYES) then Exit;
            Screen.Cursor := crHourGlass;
            DeleteFile(Directorio + NombreArchivo);
        end;
        Qry.Connection := Conn;
        Qry.SQL.Text := TextoSQL;
        Qry.Open;
        if not Qry.Eof then
        begin
            AssignFile(Archivo, (Directorio + NombreArchivo));
            Rewrite(Archivo);
            WriteLn(Archivo, '<?xml version="1.0"?>');
            WriteLn(Archivo, '<ROWSET>');
            While not Qry.Eof do begin
                Linea := '<ROW ';
                for i := 0 to Qry.Fields.Count - 1 do begin
                    if not (Qry.Fields[i].FieldName = 'IdInterfaz') then
                    begin
                        if ((i > 0) and (i < Qry.Fields.Count)) then Linea := Linea + ' ';
                        Linea := Linea + Qry.Fields[i].FieldName + '="' + Qry.Fields[i].AsString + '"';
                    end;
                end;
                Linea := Linea + ' />';
                WriteLn(Archivo, Linea);
                for i := 0 to Qry.Fields.Count - 1 do begin
                    if (Qry.Fields[i].FieldName = 'IdInterfaz') then
                    begin
                        GuardarRegistroInterface(Conn, CodigoInterfaz, Qry.Fields[i].AsString, NombreInterfaz);
                        break;
                    end;
                end;
                Qry.Next;
            end;
            WriteLn(Archivo, '</ROWSET>');
            CloseFile(Archivo);
        end;
        Qry.Close;
    finally
        Qry.Free;
    end;
    Screen.Cursor := crDefault;
    MsgBox(MSG_OPERACION_FINALIZADA_CORRECTAMENTE, CAPTION_INTERFACES, MB_ICONINFORMATION);
end;

procedure EjecutarInterfazSalidaTXT(Conn: TADOConnection; CodigoInterfaz: integer; TextoSQL: AnsiString; NombreInterfaz: AnsiString; NombreArchivo: AnsiString; Directorio: AnsiString; Separador: AnsiString);
resourcestring
    CAPTION_INTERFACES                     = 'Interfaces';
    MSG_OPERACION_FINALIZADA_CORRECTAMENTE = 'La operaci�n finaliz� correctamente.';
    MSG_ARCHIVO_EXISTENTE                  = 'El archivo ya existe. �Desea continuar?';
var
    i: integer;
	Qry: TADOQuery;
    Linea: AnsiString;
    Archivo: TextFile;
begin
    Screen.Cursor := crHourGlass;
    Qry := TAdoQuery.Create(nil);
    try
        if (FileExists(Directorio + NombreArchivo) = True) then
        begin
           	Screen.Cursor := crDefault;
            if not(MsgBox(MSG_ARCHIVO_EXISTENTE, CAPTION_INTERFACES, MB_YESNO + MB_ICONQUESTION) = IDYES) then Exit;
            Screen.Cursor := crHourGlass;
            DeleteFile(Directorio + NombreArchivo);
        end;
        Qry.Connection := Conn;
        Qry.SQL.Text := TextoSQL;
        Qry.Open;
        if not Qry.Eof then
        begin
            AssignFile(Archivo, (Directorio + NombreArchivo));
            Rewrite(Archivo);
            for i := 0 to Qry.Fields.Count - 1 do begin
                if not (Qry.Fields[i].FieldName = 'IdInterfaz') then
                begin
                    if ((i > 0) and (i < Qry.Fields.Count)) then Linea := Linea + Separador;
                    Linea := Linea + Qry.Fields[i].FieldName;
                end;
            end;
            WriteLn(Archivo, Linea);
            While not Qry.Eof do begin
                Linea := '';
                for i := 0 to Qry.Fields.Count - 1 do begin
                    if not (Qry.Fields[i].FieldName = 'IdInterfaz') then
                    begin
                        if ((i > 0) and (i < Qry.Fields.Count)) then Linea := Linea + Separador;
                        Linea := Linea + Qry.Fields[i].AsString;
                    end;
                end;
                WriteLn(Archivo, Linea);
                for i := 0 to Qry.Fields.Count - 1 do begin
                    if (Qry.Fields[i].FieldName = 'IdInterfaz') then
                    begin
                        GuardarRegistroInterface(Conn, CodigoInterfaz, Qry.Fields[i].AsString, NombreInterfaz);
                        break;
                    end;
                end;
                Qry.Next;
            end;
            CloseFile(Archivo);
        end;
        Qry.Close;
    finally
        Qry.Free;
    end;
    Screen.Cursor := crDefault;
    MsgBox(MSG_OPERACION_FINALIZADA_CORRECTAMENTE, CAPTION_INTERFACES, MB_ICONINFORMATION);
end;

procedure EjecutarInterfazEntradaTXT(Conn: TADOConnection; CodigoInterfaz: integer; TextoSQL: AnsiString; NombreInterfaz: AnsiString; NombreArchivo: AnsiString; Directorio: AnsiString; Separador: AnsiString);
resourcestring
    CAPTION_INTERFACES                       = 'Interfaces';
    MSG_ERROR_CARGANDO_ARCHIVO               = 'Error cargando el archivo.';
    MSG_ARCHIVO_INEXISTENTE                  = 'El archivo de interface no existe.';
    MSG_OPERACION_FINALIZADA_INCORRECTAMENTE = 'La operaci�n finaliz� con errores.';
    MSG_OPERACION_FINALIZADA_CORRECTAMENTE   = 'La operaci�n finaliz� correctamente.';
var
    i: integer;
    sr: TSearchRec;
    Archivo: TextFile;
    ErrorAlProcesar: boolean;
    Cabecera: array of AnsiString;
    Linea, TextoError, miTextoSQL, Valor, NombreArchivoError: AnsiString;
begin
    Screen.Cursor := crHourGlass;
    if (FindFirst(Directorio + NombreInterfaz + '*.TXT', faAnyFile, sr) = 0) then
    begin
        TextoError := '';
        ErrorAlProcesar := false;
        repeat
            AssignFile(Archivo, (Directorio + sr.Name));
            Reset(Archivo);
            if not eof(Archivo) then
            begin
                ReadLn(Archivo, Linea);
                i := 1;
                Valor := ParseParamByNumber(Linea, i, Separador);
                while not (Valor = '') do
                begin
                    SetLength(Cabecera, i);
                    Cabecera[i - 1] := UpperCase(Valor);
                    i := i + 1;
                    Valor := ParseParamByNumber(Linea, i, Separador);
                end;
                while not eof(Archivo) do
                begin
                    ReadLn(Archivo, Linea);
                    miTextoSQL := UpperCase(TextoSQL);
                    for i := 0 to High(Cabecera) do begin
                        miTextoSQL := StringReplace(miTextoSQL, ':' + Cabecera[i], ParseParamByNumber(Linea, (i + 1), Separador), [rfReplaceAll]);
                    end;
                    try
                        Conn.Execute(miTextoSQL);
                    except
                        On e: exception do begin
                            TextoError := TextoError + miTextoSQL + ' -> ' + E.Message +  CRLF + CRLF;
                            ErrorAlProcesar := true;
                        end;
                    end;
                end;
            end;
            CloseFile(Archivo);
            DeleteFile(Directorio + sr.Name);
            if not (TextoError = '') then
            begin
                NombreArchivoError := (Directorio + Copy(sr.Name, 1, (Length(sr.Name) - 4)) + '.ERR');
                if (FileExists(NombreArchivoError) = True) then DeleteFile(NombreArchivoError);
                AssignFile(Archivo, NombreArchivoError);
                Rewrite(Archivo);
                WriteLn(Archivo, TextoError);
                CloseFile(Archivo);
            end;
        until (FindNext(sr) <> 0);
        FindClose(sr);
        Screen.Cursor := crDefault;
        if (ErrorAlProcesar = false) then
        begin
            MsgBox(MSG_OPERACION_FINALIZADA_CORRECTAMENTE, CAPTION_INTERFACES, MB_ICONINFORMATION);
        end
        else
        begin
            MsgBox(MSG_OPERACION_FINALIZADA_INCORRECTAMENTE, CAPTION_INTERFACES, MB_ICONERROR);
            Run('Notepad ' + NombreArchivoError);
        end;
    end
    else
    begin
        Screen.Cursor := crDefault;
        MsgBox(MSG_ARCHIVO_INEXISTENTE, CAPTION_INTERFACES, MB_ICONINFORMATION);
    end;
end;

procedure EjecutarInterfazEntradaXML(Conn: TADOConnection; CodigoInterfaz: integer; TextoSQL: AnsiString; NombreInterfaz: AnsiString; NombreArchivo: AnsiString; Directorio: AnsiString);
resourcestring
    CAPTION_INTERFACES                       = 'Interfaces';
    MSG_ERROR_CARGANDO_ARCHIVO               = 'Error cargando el archivo.';
    MSG_ARCHIVO_INEXISTENTE                  = 'El archivo de interface no existe.';
    MSG_OPERACION_FINALIZADA_INCORRECTAMENTE = 'La operaci�n finaliz� con errores.';
    MSG_OPERACION_FINALIZADA_CORRECTAMENTE   = 'La operaci�n finaliz� correctamente.';
var
    i: integer;
    sr: TSearchRec;
    Nodo: IXMLNode;
    Archivo: TextFile;
    ErrorAlProcesar: boolean;
    XMLDocument: IXMLDocument;
    TextoError, miTextoSQL, NombreArchivoError: AnsiString;
begin
    Screen.Cursor := crHourGlass;
    if (FindFirst(Directorio + NombreInterfaz + '*.XML', faAnyFile, sr) = 0) then
    begin
        TextoError := '';
        ErrorAlProcesar := false;
        XMLDocument := TXMLDocument.Create(nil);
        repeat
            XMLDocument.LoadFromFile(Directorio + sr.Name);
            Nodo := XMLDocument.DocumentElement.ChildNodes.FindNode('ROW');
            while not (Nodo = nil) do
            begin
                miTextoSQL := UpperCase(TextoSQL);
                for i := 0 to (Nodo.AttributeNodes.Count - 1) do
                begin
                    miTextoSQL := StringReplace(miTextoSQL, ':' + UpperCase(Nodo.AttributeNodes[i].NodeName), Nodo.AttributeNodes[i].NodeValue, [rfReplaceAll]);
                end;
                try
                    Conn.Execute(miTextoSQL);
                except
                    On e: exception do begin
                        TextoError := TextoError + miTextoSQL + ' -> ' + E.Message + CRLF + CRLF;
                        ErrorAlProcesar := true;
                    end;
                end;
                Nodo := Nodo.NextSibling;
            end;
            DeleteFile(Directorio + sr.Name);
            if not (TextoError = '') then
            begin
                NombreArchivoError := (Directorio + Copy(sr.Name, 1, (Length(sr.Name) - 4)) + '.ERR');
                if (FileExists(NombreArchivoError) = True) then DeleteFile(NombreArchivoError);
                AssignFile(Archivo, NombreArchivoError);
                Rewrite(Archivo);
                WriteLn(Archivo, TextoError);
                CloseFile(Archivo);
            end;
        until (FindNext(sr) <> 0);
        FindClose(sr);
        FreeAndNil(XMLDocument);
        Screen.Cursor := crDefault;
        if (ErrorAlProcesar = false) then
        begin
            MsgBox(MSG_OPERACION_FINALIZADA_CORRECTAMENTE, CAPTION_INTERFACES, MB_ICONINFORMATION);
        end
        else
        begin
            MsgBox(MSG_OPERACION_FINALIZADA_INCORRECTAMENTE, CAPTION_INTERFACES, MB_ICONERROR);
            Run('Notepad ' + NombreArchivoError);
        end;
    end
    else
    begin
        Screen.Cursor := crDefault;
        MsgBox(MSG_ARCHIVO_INEXISTENTE, CAPTION_INTERFACES, MB_ICONINFORMATION);
    end;
end;

procedure GuardarRegistroInterface(Conn: TADOConnection; CodigoInterfaz: integer; IdInterfaz: AnsiString; NombreInterfaz: AnsiString);
resourcestring
    CAPTION_INTERFACES            = 'Interfaces';
    MSG_ERROR_GUARDANDO_INTERFACE = 'Error guardando el registro de la interfaz.';
var
    SP: TAdoStoredProc;
begin
    SP := TAdoStoredProc.Create(nil);
    try
        SP.ProcedureName := 'GuardarRegistroInterfaz';
        SP.Connection := Conn;
        SP.Parameters.CreateParameter('@CodigoInterfaz', ftInteger, pdInput, 0, CodigoInterfaz);
        SP.Parameters.CreateParameter('@IdInterfaz', ftString, pdInput, 255, IdInterfaz);
        SP.Parameters.CreateParameter('@NombreInterfaz', ftString, pdInput, 12, NombreInterfaz);
        SP.ExecProc;
        SP.Close;
    finally
        SP.Free;
    end;
end;
//function ObtenerContextMarkTAG(Conn: TADOConnection; ContractSerialNumber: DWord): Integer; //SS_1147_MCA_20140408
function ObtenerContextMarkTAG(Conn: TADOConnection; ContractSerialNumber: Int64): Integer;   //SS_1147_MCA_20140408
begin
    result := QueryGetValueInt(Conn, format('SELECT DBO.ObtenerContextMarkTAG(%d)',[ContractSerialNumber]));
end;

//function TieneConvenioDeCN(Conn: TADOConnection; NumeroDocumento : String) :Boolean;				//SS_1147_MCA_20140408
function TieneConvenioNativo(Conn: TADOConnection; NumeroDocumento : String) :Boolean;        		//SS_1147_MCA_20140408
begin
    //Result := QueryGetValueInt(Conn, format('EXEC ConvenioCN ''%s''',[NumeroDocumento])) = 1;		//SS_1147_MCA_20140408
    Result := QueryGetValueInt(Conn, format('EXEC ConvenioNativo ''%s''',[NumeroDocumento])) = 1;	//SS_1147_MCA_20140408
end;
{-----------------------------------------------------------------------------
  Function Name: CargarInterfaces
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; CodigoSistema: Integer; MenuInterfaces: TMenuItem
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarInterfaces(Conn: TADOConnection; CodigoSistema: Integer; MenuInterfaces: TMenuItem);
resourcestring
    CAPTION_INTERFAZ_SALIDA  = 'Salida';
    CAPTION_INTERFAZ_ENTRADA = 'Entrada';
Var
    ItemInterfaz: TInterfaceItem;
	QryInterfaces, QryCategorias: TADOQuery;
    ItemPadre, ItemInterfazEntrada, ItemInterfazSalida: TMenuItem;
begin
	QryInterfaces := TAdoQuery.Create(nil);
	QryCategorias := TAdoQuery.Create(nil);
	try
        QryCategorias.Connection := Conn;
		QryInterfaces.Connection := Conn;
        ItemInterfazEntrada := TMenuItem.Create(MenuInterfaces);
        ItemInterfazEntrada.Caption := CAPTION_INTERFAZ_ENTRADA;
        ItemInterfazEntrada.Hint := CAPTION_INTERFAZ_ENTRADA;
        MenuInterfaces.Add(ItemInterfazEntrada);
        QryCategorias.SQL.Text := 'SELECT CodigoCategoria, Descripcion FROM CategoriasInterfaces  WITH (NOLOCK) ' +
            'WHERE CodigoCategoria IN (SELECT DISTINCT CodigoCategoria FROM Interfaces  WITH (NOLOCK) ' +
            'WHERE Interfaz = ''E'' AND CodigoInterfaz IN (SELECT CodigoInterfaz FROM InterfacesSistemas  WITH (NOLOCK) WHERE CodigoSistema = ' +
            IntToStr(CodigoSistema) + ')) ORDER BY Descripcion';
        QryCategorias.Open;
        While not QryCategorias.Eof do begin
   			ItemPadre := TMenuItem.Create(MenuInterfaces);
       		ItemPadre.Caption := Trim(QryCategorias.FieldByName('Descripcion').AsString);
            ItemPadre.Hint := Trim(QryCategorias.FieldByName('Descripcion').AsString);
            ItemInterfazEntrada.Add(ItemPadre);
    		QryInterfaces.SQL.Text := 'SELECT CodigoInterfaz, Descripcion, Detalle ' +
                'FROM Interfaces  WITH (NOLOCK) WHERE Interfaz = ''E'' AND CodigoCategoria = ' +  QryCategorias.FieldByName('CodigoCategoria').AsString +
                'AND CodigoInterfaz IN (SELECT CodigoInterfaz FROM InterfacesSistemas  WITH (NOLOCK) WHERE CodigoSistema = ' +
                IntToStr(CodigoSistema) + ') ORDER BY Descripcion';
        	QryInterfaces.Open;
		    While not QryInterfaces.Eof do begin
       			ItemInterfaz := TInterfaceItem.Create(MenuInterfaces);
           		ItemInterfaz.Caption := Trim(QryInterfaces.FieldByName('Descripcion').AsString);
                ItemInterfaz.Codigo := QryInterfaces.FieldByName('CodigoInterfaz').AsInteger;
                ItemInterfaz.Hint := Trim(QryInterfaces.FieldByName('Detalle').AsString);
                ItemInterfaz.Connection := Conn;
                ItemPadre.Add(ItemInterfaz);
		    	QryInterfaces.Next;
    		end;
            QryInterfaces.Close;
			QryCategorias.Next;
        end;
		QryCategorias.Close;
        ItemInterfazEntrada.Visible := (ItemInterfazEntrada.Count > 0);
        ItemInterfazSalida := TMenuItem.Create(MenuInterfaces);
        ItemInterfazSalida.Caption := CAPTION_INTERFAZ_SALIDA;
        ItemInterfazSalida.Hint := CAPTION_INTERFAZ_SALIDA;
        MenuInterfaces.Add(ItemInterfazSalida);
        QryCategorias.SQL.Text := 'SELECT CodigoCategoria, Descripcion FROM CategoriasInterfaces  WITH (NOLOCK) ' +
            'WHERE CodigoCategoria IN (SELECT DISTINCT CodigoCategoria FROM Interfaces  WITH (NOLOCK) ' +
            'WHERE Interfaz = ''S'' AND CodigoInterfaz IN (SELECT CodigoInterfaz FROM InterfacesSistemas  WITH (NOLOCK) WHERE CodigoSistema = ' +
            IntToStr(CodigoSistema) + ')) ORDER BY Descripcion';
        QryCategorias.Open;
        While not QryCategorias.Eof do begin
   			ItemPadre := TMenuItem.Create(MenuInterfaces);
       		ItemPadre.Caption := Trim(QryCategorias.FieldByName('Descripcion').AsString);
            ItemPadre.Hint := Trim(QryCategorias.FieldByName('Descripcion').AsString);
            ItemInterfazSalida.Add(ItemPadre);
    		QryInterfaces.SQL.Text := 'SELECT CodigoInterfaz, Descripcion, Detalle ' +
                'FROM Interfaces  WITH (NOLOCK) WHERE Interfaz = ''S'' AND CodigoCategoria = ' +  QryCategorias.FieldByName('CodigoCategoria').AsString +
                'AND CodigoInterfaz IN (SELECT CodigoInterfaz FROM InterfacesSistemas  WITH (NOLOCK) WHERE CodigoSistema = ' +
                IntToStr(CodigoSistema) + ') ORDER BY Descripcion';
        	QryInterfaces.Open;
		    While not QryInterfaces.Eof do begin
       			ItemInterfaz := TInterfaceItem.Create(MenuInterfaces);
           		ItemInterfaz.Caption := Trim(QryInterfaces.FieldByName('Descripcion').AsString);
                ItemInterfaz.Codigo := QryInterfaces.FieldByName('CodigoInterfaz').AsInteger;
                ItemInterfaz.Hint := Trim(QryInterfaces.FieldByName('Detalle').AsString);
                ItemInterfaz.Connection := Conn;
                ItemPadre.Add(ItemInterfaz);
		    	QryInterfaces.Next;
    		end;
            QryInterfaces.Close;
			QryCategorias.Next;
        end;
		QryCategorias.Close;
        ItemInterfazSalida.Visible := (ItemInterfazSalida.Count > 0);
	finally
		QryCategorias.Free;
		QryInterfaces.Free;
	end;
    MenuInterfaces.Visible := (MenuInterfaces.Count > 0);
end;

function ObtenerFormatoFechaSQL(Conn: TADOConnection): AnsiString;
Var
	Qry: TADOQuery;
begin
    Result := 'mdy';
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text   := 'DBCC UserOptions';
		Qry.Open;
		While not Qry.Eof do begin
            if (Qry.FieldByName('Set Option').AsString = 'dateformat') then
            begin
                Result := Qry.FieldByName('Value').AsString;
                break;
            end;
			Qry.Next;
		end;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

function  DescripcionTolerancia(medida: integer; unidad: AnsiString): AnsiString;
resourcestring
    MSG_CENTIMETROS = 'Cent�metros';
    MSG_PORCENTAJE  = 'Por ciento';
begin
    if (unidad = 'C') then
        result := IntToStr(medida) + ' ' + MSG_CENTIMETROS
    else
        if (unidad = 'P') then
            result := IntToStr(medida) + ' ' + MSG_PORCENTAJE
        else
            result := '';
end;

function  DescripcionDescuento(medida: double; unidad: AnsiString): AnsiString;
resourcestring
    MSG_IMPORTE = 'por un monto de';
    MSG_PORCENTAJE  = 'por ciento';
var
    desc: double;
    descInt: integer;
begin
    if (unidad = 'I') then
    begin
        descInt := trunc(medida / 100);
        result := MSG_IMPORTE + ' ' + formatFloat(FORMATO_IMPORTE, descInt);
    end
    else
        if (unidad = 'P') then
        begin
            desc   := medida / 100;
            result := formatFloat(FORMATO_PORCENTAJE, desc) + ' ' + MSG_PORCENTAJE;
        end
        else
            result := '';
end;

function DescripcionParametroPromocion(TipoParametro: AnsiString): AnsiString;
resourcestring
    MSG_CT  = 'Cantidad de tr�nsitos ';
    MSG_IT  = 'Monto de tr�nsitos ';
    MSG_CV  = 'Cantidad de viajes ';
    MSG_IV  = 'Monto de viajes ';
begin
    if (TipoParametro = 'CT') then result := MSG_CT
    else
        if (TipoParametro = 'IT') then result := MSG_IT
        else
            if (TipoParametro = 'CV') then  result := MSG_CV
            else
                if (TipoParametro = 'IV') then  result := MSG_IV
                else
                    result := '';
end;

function  EsAnioCorrecto(anio: integer): boolean;
var
    anioActual: word;
    mesActual: word;
    fecha: TDateTime;
    // DecodeDate(Date: TDateTime; var Year, Month, Day: Word);
begin
    ///Esta validacion es para el A�o de los vehiculos
    // Validacion: Tiene que ser mayor a 1900 y menor o igual
    // al a�o siguiente del actual. Ejemplo en el 2004, pueden cargar
    // anios hasta el 2005, mas no!!! Dependiendo de la epoca del a�o. en la segunda mitad si!!!
{INICIO: TASK_021_JMA_20160616
    anioActual := year(now);
    mesActual := month(now);
}
    fecha := NowBaseCN(DMConnections.BaseCAC);
    anioActual := year(fecha);
    mesActual := month(fecha);
{TERMINO: TASK_021_JMA_20160616}
    if mesActual >= 7 then
        result := ((anio >= 1900) and (anio <= anioActual + 1))
    else
        result := ((anio >= 1900) and (anio <= anioActual));
end;

{-----------------------------------------------------------------------------
  Function Name: ObtenerTipoContactoComunicacion
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn:TADOConnection; Comunicacion: integer
  Return Value: AnsiString

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function ObtenerTipoContactoComunicacion(Conn:TADOConnection; Comunicacion: integer): AnsiString;
Var
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
        with Qry do
        begin
            Connection := Conn;
            SQL.Text := 'SELECT TipoContacto FROM Comunicaciones  WITH (NOLOCK) WHERE CodigoComunicacion = '
                        + IntToStr(Comunicacion);
            Open;
            if not EOF then result := FieldByName('TipoContacto').AsString
            else result := '';
        end;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: ObtenerTipoFuenteDeFuenteSolicitud
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn:TADOConnection; CodigoFuenteSolicitud: integer
  Return Value: AnsiString

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function  ObtenerTipoFuenteDeFuenteSolicitud(Conn:TADOConnection; CodigoFuenteSolicitud: integer): AnsiString;
Var
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
        with Qry do
        begin
            Connection := Conn;
            SQL.Text := 'SELECT TipoFuente FROM FuentesSolicitud  WITH (NOLOCK) WHERE CodigoFuenteSolicitud = '
                        + IntToStr(CodigoFuenteSolicitud);
            Open;
            if not EOF then result := FieldByName('TipoFuente').AsString
            else result := '';
        end;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;


procedure CargarUsuariosSistemas(Conn: TADOConnection; Combo: TVariantComboBox; CodigoUsuario: AnsiString = ''; TieneItemNinguno: boolean = false);
Var
	i: Integer;
	Qry: TADOQuery;
    NombreCompleto: AnsiString;
begin
    NombreCompleto := '';
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'SELECT BO_COBO.dbo.CONST_PERSONERIA_NATURAL() AS Personeria, * FROM vw_UsuariosSistemas (NOLOCK)';   //JLO 20160527
		Qry.Open;
		Combo.Items.Clear;
		i := -1;
        if (TieneItemNinguno) then begin
            Combo.Items.Add(SIN_ESPECIFICAR, '');
            i := 0;
        end;

		While not Qry.Eof do begin
            NombreCompleto := ArmarNombrePersona(Qry.FieldByName('Personeria').AsString,
                                                Qry.FieldByName('Nombre').AsString,
                                                Qry.FieldByName('Apellido').AsString,
                                                Qry.FieldByName('ApellidoMaterno').AsString);
			Combo.Items.Add(NombreCompleto, Qry.FieldByName('CodigoUsuario').AsString);
			if (Qry.FieldByName('CodigoUsuario').AsString = CodigoUsuario) then begin
            	i := Combo.Items.Count - 1;
            end;
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

{-----------------------------------------------------------------------------
  Procedure: CargarCodigosUsuariosSistemas
  Author:    ggomez
  Date:      01-Jun-2005
  Arguments: Conn: TADOConnection; Combo: TComboBox; TieneItemNinguno: Boolean = True
  Result:    None
  Description: Carga en el combo pasado como par�metro los cc�digos de los
    usuarios de sistemas.
-----------------------------------------------------------------------------}
procedure CargarCodigosUsuariosSistemas(Conn: TADOConnection; Combo: TComboBox;
    TieneItemNinguno: Boolean = True);
var
	sp: TADOStoredProc;
begin
    Combo.Items.Clear;

	sp := TADOStoredProc.Create(nil);
	try
		{ INICIO : 20160307 MGO
		sp.Connection := Conn;
		}
		sp.Connection := DMConnections.BaseBO_Master;
		// FIN : 20160307 MGO
		sp.ProcedureName := 'ObtenerUsuariosSistemas';
		sp.Open;
        sp.First;

        if TieneItemNinguno then Combo.Items.Add(SIN_ESPECIFICAR);

		while not sp.Eof do begin
			Combo.Items.Add(Trim(sp.FieldByName('CodigoUsuario').AsString));

			sp.Next;
		end; // while

		Combo.ItemIndex := 0;
	finally
		sp.Close;
		sp.Free;
	end; // finally
end;

{-----------------------------------------------------------------------------
  Function Name: CargarPersonalSinUsuariosSistemas
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo: TVariantComboBox; CodigoPersona: Integer = 0
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarPersonalSinUsuariosSistemas(Conn: TADOConnection; Combo: TVariantComboBox; CodigoPersona: Integer = 0);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'SELECT RTRIM(dbo.ArmarNombrePersona(dbo.CONST_PERSONERIA_NATURAL(),Apellido,ApellidoMaterno,Nombre)) AS NombreCompleto, CodigoPersona ' +
                        '  FROM VW_MaestroPersonal (NOLOCK)' +
                        ' WHERE NOT EXISTS(SELECT 1 FROM UsuariosSistemas  WITH (NOLOCK) ' +
                        '                   WHERE UsuariosSistemas.CodigoPersona = VW_MaestroPersonal.CodigoPersona) ' +
                        ' ORDER BY NombreCompleto';
		Qry.Open;
		Combo.Items.Clear;
		i := -1;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('NombreCompleto').AsString,
              Qry.FieldByName('CodigoPersona').AsInteger);
			if (Qry.FieldByName('CodigoPersona').AsInteger = CodigoPersona) then begin
            	i := Combo.Items.Count - 1;
            end;
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;
{-----------------------------------------------------------------------------
  Function Name: CargarMotivosContacto
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo: TVariantComboBox; Ninguno: Boolean = True; CodigoMotivoContacto: integer = 0
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarMotivosContacto(Conn: TADOConnection; Combo: TVariantComboBox; Ninguno: Boolean = True; CodigoMotivoContacto: integer = 0);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'SELECT * FROM MotivosContacto  WITH (NOLOCK) Order By Descripcion';
		Qry.Open;
		Combo.Clear;
		i := -1;
        if Ninguno then begin
        	Combo.Items.Add('(Sin especificar)', 0);
            i := 0;
		end;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString,
              Istr(Qry.FieldByName('CodigoMotivo').AsInteger));
			if (Qry.FieldByName('CodigoMotivo').AsInteger = CodigoMotivoContacto) then begin
            	i := Combo.Items.Count - 1;
            end;
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarMotivos
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo: TVariantComboBox; Tipo: String; Ninguno: Boolean = True
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarMotivos(Conn: TADOConnection; Combo: TVariantComboBox; Tipo: String; Ninguno: Boolean = True);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'SELECT * FROM MotivosObservaciones  WITH (NOLOCK) WHERE Tipo = ''' + Trim(Tipo) +  ''' Order By Detalle';
		Qry.Open;
		Combo.Clear;
		i := -1;
        if Ninguno then begin
        	Combo.Items.Add('(Sin especificar)', 0);
            i := 0;
		end;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Detalle').AsString,
              Istr(Qry.FieldByName('CodigoMotivo').AsInteger));
(*
			if (Qry.FieldByName('CodigoMotivo').AsInteger = CodigoMotivoCancelacion) then begin
            	i := Combo.Items.Count - 1;
            end;
*)
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;


{-----------------------------------------------------------------------------
  Function Name: CargarCategoriasModulos
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo: TComboBox; CodigoCategoria: integer = 0
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarCategoriasModulos(Conn: TADOConnection; Combo: TComboBox; CodigoCategoria: integer = 0);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'SELECT * FROM CategoriasModulo WITH (NOLOCK) ';
		Qry.Open;
		Combo.Clear;
		i := -1;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString +
			  Space(200) + Istr(Qry.FieldByName('CodigoCategoria').AsInteger));
			if Qry.FieldByName('CodigoCategoria').AsInteger = CodigoCategoria then begin
            	i := Combo.Items.Count - 1;
            end;
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

function ValidarNumeroCalle(CodigoPais,CodigoRegion,CodigoComuna:String;CodigoCalle,Numero:Integer):Boolean;
begin
    if QueryGetValueInt(DMConnections.BaseCAC,format('SELECT DBO.NumeroCalleValido(''%s'',''%s'',''%s'',%d,%d)',[CodigoPais,CodigoRegion,CodigoComuna,CodigoCalle,Numero]))=1 then begin
        Result := true;
    end else begin
        Result := False;
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: CargarTiposDocumento
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo: TMaskCombo; CodigoDocumento: AnsiString = ''
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarTiposDocumento(Conn: TADOConnection; Combo: TMaskCombo; CodigoDocumento: AnsiString = '');
resourcestring
    MSG_DOCUMENTO_INCORRECTO = 'N�mero de documento especificado incorrecto';
Var
	i, ix: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'SELECT * FROM TiposDocumento WITH (NOLOCK) ';
		Qry.Open;
		Combo.Items.Clear;
		i	:= -1;
        ix	:= -1;
		While not Qry.Eof do begin
        	inc(i);
        	Combo.Items.Add;
            with Combo.Items[i] do begin
                ValidateRut := Qry.FieldByName('ValidarNroDoc').AsBoolean;
				Caption := Trim(Qry.FieldByName('Descripcion').AsString) + Space(200) + Trim(Qry.FieldByName('CodigoDocumento').AsString);
                Mask	:= Trim(Qry.FieldByName('Mascara').AsString);
                MessageError := MSG_DOCUMENTO_INCORRECTO;
			end;
			if Trim(Qry.FieldByName('CodigoDocumento').AsString) = Trim(CodigoDocumento) then begin
            	ix := Combo.Items.Count - 1;
            end else if Qry.FieldByName('DocumentoDefault').AsBoolean then begin
            	if ix = -1 then ix := Combo.Items.Count - 1;
            end;
			Qry.Next;
		end;

		(*While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString +
			  Space(200) + Qry.FieldByName('CodigoDocumento').AsString);
			if Trim(Qry.FieldByName('CodigoDocumento').AsString) = Trim(CodigoDocumento) then begin
            	i := Combo.Items.Count - 1;
            end else if Qry.FieldByName('DocumentoDefault').AsBoolean then begin
            	if i = -1 then i := Combo.Items.Count - 1;
            end;
			Qry.Next;
		end; *)
		Combo.ItemIndex := ix;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;
{-----------------------------------------------------------------------------
  Function Name: CargarTiposCalle
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo: TComboBox; CodigoTipoCalle: integer = 0; TieneItemNinguno: boolean = false
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarTiposCalle(Conn: TADOConnection; Combo: TComboBox; CodigoTipoCalle: integer = 0; TieneItemNinguno: boolean = false);
resourcestring
    MSG_TIPO_CALLE_INCORRECTO = 'El Tipo de Calle especificado es incorrecto';
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'SELECT * FROM TiposCalle  WITH (NOLOCK) order by Descripcion';
		Qry.Open;
		Combo.Items.Clear;
		i	:= -1;

        if TieneItemNinguno then begin
			Combo.Items.Add(SIN_ESPECIFICAR + Space(200) + '');
            i := 0;
        end;

		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString +  Space(200) + Qry.FieldByName('CodigoTipoCalle').AsString);
			if Qry.FieldByName('CodigoTipoCalle').AsInteger = CodigoTipoCalle then begin
            	i := Combo.Items.Count - 1;
            end else if Qry.FieldByName('TipoCalleDefault').AsBoolean then begin
            	i := Combo.Items.Count - 1;
            end;
			Qry.Next;
		end;

        if i > -1 then Combo.ItemIndex := i
            else if (i = -1) and (combo.Items.Count > 0) then Combo.ItemIndex := 0;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;
{-----------------------------------------------------------------------------
  Function Name: CargarTiposEdificacion
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo: TComboBox; CodigoTipoEdificacion: integer = 0
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarTiposEdificacion(Conn: TADOConnection; Combo: TComboBox; CodigoTipoEdificacion: integer = 0);
resourcestring
    MSG_TIPO_EDIFICACION_INCORRECTO = 'El Tipo de Edificaci�n especificado es incorrecto';
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'SELECT * FROM TiposEdificacion WITH (NOLOCK) ';
		Qry.Open;
		Combo.Items.Clear;
		i	:= -1;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString +  Space(200) + Qry.FieldByName('CodigoTipoEdificacion').AsString);
			if Qry.FieldByName('CodigoTipoEdificacion').AsInteger = CodigoTipoEdificacion then begin
            	i := Combo.Items.Count - 1;
            end else if Qry.FieldByName('TipoEdificacionDefault').AsBoolean then begin
            	i := Combo.Items.Count - 1;
            end;
			Qry.Next;
		end;
        if i > -1 then Combo.ItemIndex := i
            else if (i = -1) and (combo.Items.Count > 0) then Combo.ItemIndex := 0;
	finally
		Qry.Close;
		Qry.Free;
	end;

end;

{-----------------------------------------------------------------------------
  Function Name: CargarTiposPatente
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo: TMaskCombo; TipoPatente: AnsiString = ''
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarTiposPatente(Conn: TADOConnection; Combo: TMaskCombo; TipoPatente: AnsiString = '');
resourcestring
    MSG_PATENTE_INCORRECTA = 'Patente especificada incorrecta';
Var
	i, ix: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'SELECT * FROM TiposPatente WITH (NOLOCK) ';
		Qry.Open;
		Combo.Items.Clear;
		i	:= -1;
        ix	:= -1;
		While not Qry.Eof do begin
        	inc(i);
        	Combo.Items.Add;
            with Combo.Items[i] do begin
                ValidateRut := False;
				Caption := Trim(Qry.FieldByName('Descripcion').AsString) + Space(200) + Trim(Qry.FieldByName('CodigoTipoPatente').AsString);
                Mask	:= Trim(Qry.FieldByName('Mascara').AsString);
                MessageError := MSG_PATENTE_INCORRECTA;
			end;
            if Trim(Qry.FieldByName('CodigoTipoPatente').AsString) = Trim(TipoPatente) then begin
               	ix := Combo.Items.Count - 1;
            end else if Qry.FieldByName('TipoPatenteDefault').AsBoolean then begin
            	if ix = -1 then ix := Combo.Items.Count - 1;
            end;
			Qry.Next;
		end;
		Combo.ItemIndex := ix;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarMediosContacto
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo: TVariantComboBox; CodigoMedio: integer = 0; Todos: boolean = False
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarMediosContacto(Conn: TADOConnection; Combo: TVariantComboBox; CodigoMedio: integer = 0; Todos: boolean = False);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'SELECT * FROM MediosContacto  WITH (NOLOCK) Order By Descripcion';
		Qry.Open;
		Combo.Items.Clear;
		i := -1;
        if Todos then begin
        	Combo.Items.Add('', 0);
            i := 0;
		end;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString,
              Istr(Qry.FieldByName('CodigoMedioContacto').AsInteger));
			if (Qry.FieldByName('CodigoMedioContacto').AsInteger = CodigoMedio) then begin
            	i := Combo.Items.Count - 1;
            end;
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;
{-----------------------------------------------------------------------------
  Function Name: CargarMediosContacto
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo: TComboBox; CodigoMedio: integer = 0; Todos: boolean = False
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarMediosContacto(Conn: TADOConnection; Combo: TComboBox; CodigoMedio: integer = 0; Todos: boolean = False);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'SELECT * FROM MediosContacto  WITH (NOLOCK) order by Descripcion';
		Qry.Open;
		Combo.Clear;
		i := -1;
        if Todos then begin
			Combo.Items.Add('' + Space(200) + TMC_EMAIL + PadL('0', 10));
            i := 0;
		end;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString +
			  Space(200) + qry.FieldByName('TipoMedio').AsString + PadL(Istr(Qry.FieldByName('CodigoMedioContacto').AsInteger), 10));
			if Qry.FieldByName('CodigoMedioContacto').AsInteger = CodigoMedio then begin
            	i := Combo.Items.Count - 1;
            end;
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarTiposMedioContacto
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo: TVariantComboBox;
              CodigoTipoMedioContacto: integer = 0; Formato: AnsiString = '';
              Todos: boolean = False; TieneItemNinguno: boolean = False;
              FormatoDistintoA:AnsiString=''
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarTiposMedioContacto(Conn: TADOConnection; Combo: TVariantComboBox;
  CodigoTipoMedioContacto: integer = 0; Formato: AnsiString = ''; Todos: boolean = False; TieneItemNinguno: boolean = False;FormatoDistintoA:AnsiString='');
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := Format('SELECT * FROM TiposMedioContacto  WITH (NOLOCK) WHERE ((Formato = ''%s'') OR (''%s'' = '''')) AND ((''%s''='''') OR (Formato<>''%s''))', [Formato,Formato,FormatoDistintoA,FormatoDistintoA]);
		Qry.Open;
		Combo.Items.Clear;
		i := -1;
        if Todos then begin
        	Combo.Items.Add('(Todos)', 0);
            i := 0;
		end;

        if (TieneItemNinguno) and (not Todos) then begin
            Combo.Items.Add(SIN_ESPECIFICAR + Space(200) + '',0);
            i := 0;
        end;

		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString,
              Istr(Qry.FieldByName('CodigoTipoMedioContacto').AsInteger));
			if (Qry.FieldByName('CodigoTipoMedioContacto').AsInteger = CodigoTipoMedioContacto) then begin
            	i := Combo.Items.Count - 1;
            end;
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;
{-----------------------------------------------------------------------------
  Function Name: CargarCodigosAreaTelefono
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo: TComboBox;
              CodigoAreaTelefono: Integer = 0
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarCodigosAreaTelefono(Conn: TADOConnection; Combo: TComboBox; CodigoAreaTelefono: Integer = 0);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	if combo.Items.Count=0 then begin
            Qry := TAdoQuery.Create(nil);
            try

                Qry.Connection := Conn;
                Qry.SQL.Text := 'SELECT CodigoArea, TipoLinea FROM CodigosAreaTelefono  WITH (NOLOCK) ';
                Qry.Open;
                Combo.Items.Clear;
                i := -1;
                While not Qry.Eof do begin
                  Combo.Items.Add(Qry.FieldByName('CodigoArea').AsString+space(200)+Qry.FieldByName('TipoLinea').AsString[1]);
                  if (Qry.FieldByName('CodigoArea').AsInteger = CodigoAreaTelefono) then begin
                      i := Combo.Items.Count - 1;
                  end;
                    Qry.Next;
                end;
                Combo.ItemIndex := i;
            finally
                Qry.Close;
                Qry.Free;
            end;
    end else begin
    	i:=0;
    	while (i<combo.Items.Count) and (strtoint(trim(strleft(combo.Items[i],5)))<>CodigoAreaTelefono) do
        	inc(i);

        if i=combo.Items.Count then combo.ItemIndex:=0
        else combo.ItemIndex:=i;
    end;
end;





{-----------------------------------------------------------------------------
  Function Name: CargarTiposSeveridad
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo: TComboBox; CodigoSeveridad:integer = 0
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarTiposSeveridad(Conn: TADOConnection; Combo: TComboBox; CodigoSeveridad:integer = 0);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'SELECT * FROM TiposSeveridad WITH (NOLOCK) ';
		Qry.Open;
		Combo.Clear;
		i := -1;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString +
			  Space(200) + Istr(Qry.FieldByName('CodigoSeveridad').AsInteger));
			if Qry.FieldByName('CodigoSeveridad').AsInteger = CodigoSeveridad then begin
            	i := Combo.Items.Count - 1;
            end;
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

procedure CargarSexos(Combo: TComboBox; Sexo: AnsiString = ''; TieneItemNinguno: boolean = false);
resourcestring
    MSG_FEMENINO    = 'Femenino';
    MSG_MASCULINO   = 'Masculino';
var
    auxTieneItemNinguno: integer;
begin
	Combo.Clear;
    auxTieneItemNinguno := 0;
    if TieneItemNinguno then begin
        Combo.Items.Add(SIN_ESPECIFICAR + Space(200) + PadL('', 10, ' ') + PadL('', 10, ' '));
        Inc(auxTieneItemNinguno);
        Combo.Itemindex := 0;
    end;
	Combo.Items.Add(MSG_MASCULINO + Space(200) + SEXO_MASCULINO);
	Combo.Items.Add(MSG_FEMENINO + Space(200) +  SEXO_FEMENINO);
	if Trim(UpperCase(Sexo)) = SEXO_MASCULINO then Combo.Itemindex := auxTieneItemNinguno
	  else if Trim(UpperCase(Sexo)) = SEXO_FEMENINO then Combo.Itemindex := auxTieneItemNinguno + 1;
    //if TieneItemNinguno then  Combo.Itemindex := Combo.Itemindex + 1;
end;

procedure CargarPersoneria(Combo: TComboBox; Personeria: AnsiString = ''; PermiteAmbas: Boolean = False; TieneItemNinguno: boolean = false);
begin
	Combo.Clear;
    if TieneItemNinguno then Combo.Items.Add(SIN_ESPECIFICAR + Space(200) + PadL('0', 10, ' '));

    if PermiteAmbas then Combo.Items.Add( PERSONERIA_AMBAS_DESC + Space(200) +  PERSONERIA_AMBAS);
	Combo.Items.Add( PERSONERIA_FISICA_DESC + Space(200) +  PERSONERIA_FISICA);
	Combo.Items.Add( PERSONERIA_JURIDICA_DESC + Space(200) + PERSONERIA_JURIDICA);
    if PermiteAmbas then begin
        if Trim(UpperCase(Personeria)) = PERSONERIA_AMBAS then Combo.Itemindex := 0+iif(TieneItemNinguno,1,0)
          else if Trim(UpperCase(Personeria)) = PERSONERIA_FISICA then Combo.Itemindex := 1+iif(TieneItemNinguno,1,0)
	      else if Trim(UpperCase(Personeria)) = PERSONERIA_JURIDICA then Combo.Itemindex := 2+iif(TieneItemNinguno,1,0)
          else if Trim(Personeria) = SIN_ESPECIFICAR then Combo.Itemindex := 0;

    end else begin
    	if Trim(UpperCase(Personeria)) = PERSONERIA_FISICA then Combo.Itemindex := 0+iif(TieneItemNinguno,1,0)
	      else if Trim(UpperCase(Personeria)) = PERSONERIA_JURIDICA then Combo.Itemindex := 1+iif(TieneItemNinguno,1,0)
          else if Trim(Personeria) = SIN_ESPECIFICAR then Combo.Itemindex := 0;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarTipoSituacionIVA
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo: TComboBox; SituacionIVA: AnsiString = ''
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarTipoSituacionIVA(Conn: TADOConnection; Combo: TComboBox; SituacionIVA: AnsiString = '');
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection  := Conn;
		Qry.SQL.Text    := 'SELECT CodigoSituacionIVA, Descripcion FROM SituacionesIVA WITH (NOLOCK) ';
		Qry.Open;
		Combo.Clear;
		i := -1;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString + Space(200) + Trim(Qry.FieldByName('CodigoSituacionIVA').AsString));
			if Trim(Qry.FieldByName('CodigoSituacionIVA').AsString) = Trim(SituacionIVA) then
			  i := Combo.Items.Count - 1;
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;


{-----------------------------------------------------------------------------
  Function Name: CargarColoresVehiculos
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo: TComboBox; CodigoColor: Integer = 0
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarColoresVehiculos(Conn: TADOConnection; Combo: TComboBox; CodigoColor: Integer = 0);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'SELECT * FROM VehiculosColores  WITH (NOLOCK) ORDER BY Descripcion';
		Qry.Open;
		Combo.Clear;
		i := -1;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString +
			  Space(200) + Qry.FieldByName('CodigoColor').AsString);
			if Qry.FieldByName('CodigoColor').AsInteger = CodigoColor then
			  i := Combo.Items.Count - 1;
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;


procedure CargarMarcasVehiculos(Conn: TADOConnection; Combo: TComboBox; CodigoMarca: Integer = 0);
Var
	i: Integer;
	Qry: TADOStoredProc;
begin
	Qry := TADOStoredProc.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.ProcedureName := 'ObtenerVehiculosMarcas';
		Qry.Parameters.Refresh;
        Qry.Open;
		Combo.Clear;
		i := -1;
		While not Qry.Eof do begin
			Combo.Items.Add(UpperCase(Qry.FieldByName('Descripcion').AsString) +
			  Space(200) + Qry.FieldByName('CodigoMarca').AsString);
			if Qry.FieldByName('CodigoMarca').AsInteger = CodigoMarca then
			  i := Combo.Items.Count - 1;
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarMarcasComboVariant
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo: TVariantComboBox;
              marcaVehiculo: Integer = 0
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarMarcasComboVariant(Conn: TADOConnection; Combo: TVariantComboBox;
                                   marcaVehiculo: Integer = 0);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
        //Rev.32 / 20-Abril-2011 / Nelson Droguett Sierra
		Qry.SQL.Text := ' SELECT * FROM VehiculosMarcas  WITH (NOLOCK) ORDER BY Descripcion ASC';        //PAR00133-NDR-20110420
		Qry.Open;
		combo.Clear;
        combo.Items.Add(SIN_ESPECIFICAR, 0);
        i := 0;
        While not Qry.Eof do begin
			combo.Items.Add(UpperCase(Qry.FieldByName('Descripcion').AsString),
              Istr(Qry.FieldByName('CodigoMarca').AsInteger));
			if (Qry.FieldByName('CodigoMarca').AsInteger = marcaVehiculo) then begin
            	i := combo.Items.Count - 1;
            end;
			Qry.Next;
		end;
		combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

//procedure CargarModelosVehiculos(Conn: TADOConnection; Combo: TComboBox; CodigoMarca: Integer; CodigoModelo: Integer = 0);
//Var
//	i: Integer;
//	Qry: TADOQuery;
//begin
//	Qry := TAdoQuery.Create(nil);
//	try
//		Qry.Connection := Conn;
//		Qry.SQL.Text := 'SELECT * FROM VehiculosModelos WHERE CodigoMarca = ' +
//		  IntToStr(CodigoMarca) + ' ORDER BY Descripcion';
//		Qry.Open;
//		Combo.Clear;
//		i := -1;
//		While not Qry.Eof do begin
//			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString +
//			  Space(200) + Qry.FieldByName('CodigoModelo').AsString);
//			if Qry.FieldByName('CodigoModelo').AsInteger = CodigoModelo then
//			  i := Combo.Items.Count - 1;
//			Qry.Next;
//		end;
//		Combo.ItemIndex := i;
//	finally
//		Qry.Close;
//		Qry.Free;
//	end;
//end;

procedure CargarTipoTarjeta(Combo: TComboBox; TipoTarjeta: AnsiString = DA_TARJETA_CREDITO);
ResourceString
    MSG_DETALLE_TARJETA_CREDITO = 'Tarjeta de Cr�dito';
    MSG_DETALLE_TARJETA_DEBITO = 'Tarjeta de D�bito';
begin
    Combo.Items.Clear;
    Combo.Items.Add( MSG_DETALLE_TARJETA_CREDITO + Space(200) + DA_TARJETA_CREDITO);
    Combo.Items.Add( MSG_DETALLE_TARJETA_DEBITO + Space(200) + DA_TARJETA_DEBITO);

    if TipoTarjeta = DA_TARJETA_CREDITO then
        Combo.ItemIndex := 0
    else if TipoTarjeta = DA_TARJETA_DEBITO then
        Combo.ItemIndex := 1
    else
        Combo.ItemIndex := -1;

end;

procedure CargarTarjetasCredito(Conn: TADOConnection; Combo: TComboBox; CodigoTarjeta: Integer = 0);
Var
	i: Integer;
	Qry: TADOQuery;
    NombreTarjeta: AnsiString;
begin
    NombreTarjeta := '';
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
        Qry.SQL.Text := 'SELECT * FROM VW_TarjetasCredito (nolock) ORDER BY Apellido ';
		Qry.Open;
		Combo.Clear;
		i := -1;
		While not Qry.Eof do begin
            NombreTarjeta := iif(Qry.FieldByName('Personeria').AsString = PERSONERIA_JURIDICA,
                                    ArmaRazonSocial(Qry.FieldByName('Apellido').AsString,
                                        Qry.FieldByName('Nombre').AsString),
                                    ArmaNombreCompleto(Qry.FieldByName('Apellido').AsString,
                                        Qry.FieldByName('ApellidoMaterno').AsString,
                                        Qry.FieldByName('Nombre').AsString));
			Combo.Items.Add(NombreTarjeta + Space(200) + Qry.FieldByName('CodigoPersona').AsString);
			if Qry.FieldByName('CodigoPersona').AsInteger = CodigoTarjeta then i := Combo.Items.Count - 1;
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;


procedure CargarTiposTarjetasCreditoDebito(Conn: TADOConnection; Combo: TComboBox; TipoTarjeta: AnsiString = DA_TARJETA_CREDITO; CodigoTipoTarjeta: Integer = 0);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
        if TipoTarjeta = DA_TARJETA_CREDITO Then
            Qry.SQL.Text := 'SELECT * FROM VW_TarjetasCreditoTipos (nolock)'
        else if TipoTarjeta = DA_TARJETA_DEBITO Then
            Qry.SQL.Text := 'SELECT * FROM VW_TarjetasDebito (nolock)'
        else begin
    		Qry.SQL.Text := 'SELECT * FROM VW_Tarjetas (nolock) ORDER BY Descripcion';
        end;

		Qry.Open;
		Combo.Clear;
		i := -1;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString + Space(200) + Qry.FieldByName('CodigoTipoTarjeta').AsString);
			if Qry.FieldByName('CodigoTipoTarjeta').AsInteger = CodigoTipoTarjeta then i := Combo.Items.Count - 1;
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;


procedure CargarBancos(Conn: TADOConnection; Combo: TComboBox; CodigoBanco: Integer = 0);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'SELECT * FROM VW_Bancos (nolock)';
		Qry.Open;
		Combo.Clear;
		i := -1;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Apellido').AsString +
			  Space(200) + Qry.FieldByName('CodigoPersona').AsString);
			if Qry.FieldByName('CodigoPersona').AsInteger = CodigoBanco then
			  i := Combo.Items.Count - 1;
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

{******************************* Procedure Header ******************************
Procedure Name: CargarBancosPAC
Author :
Date Created :
Parameters : Conn: TADOConnection; Combo: TVariantComboBox; CodigoBanco: Integer = 0; TieneItemSeleccionar: Boolean = False
Description :

Revision : 1
    Author : pdominguez
    Date   : 17/09/2009
    Description : SS 809
        - Se a�ade el par�metro TieneItemSeleccionar.
*******************************************************************************}
procedure CargarBancosPAC(Conn: TADOConnection; Combo: TVariantComboBox; CodigoBanco: Integer = 0; TieneItemSeleccionar: Boolean = False);
var
    sp: TADOStoredProc;
begin
    sp := TADOStoredProc.Create(Nil);
    try
        sp.Connection := Conn;
        sp.ProcedureName := 'ObtenerBancosPAC';
        sp.Open;

        if TieneItemSeleccionar then Combo.Items.Add(SELECCIONAR, 0);

        while not sp.Eof do begin
            Combo.Items.Add(sp.FieldByName('Descripcion').AsString, sp.FieldByName('CodigoBanco').AsInteger);
            sp.Next;
        end; // while

        if (Combo.Items.Count > 0) then Combo.ItemIndex := Combo.Items.IndexOfValue(CodigoBanco);

    finally
        sp.Close;
        sp.Free;
    end; // finally

end;
{-----------------------------------------------------------------------------
  Function Name: 
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo: TComboBox; CodigoCliente: LongInt;
    TipoPago: String = ''; CodigoDebitoAutomatico: Integer = 0; CodigoCuenta: LongInt = 0
  Return Value: N/A

  Revision : 1
    Date: 04/03/2009
    Author: pdominguez
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarCuentas(Conn: TADOConnection; Combo: TComboBox; CodigoCliente: LongInt; TipoPago: String = ''; CodigoDebitoAutomatico: Integer = 0; CodigoCuenta: LongInt = 0);
Var
	i: Integer;
	Qry: TADOQuery;
	ConsultaTipoPago, ConsultaDebitoAutomatico: AnsiString;
begin
	if TipoPago <> '' then begin
		ConsultaTipoPago := format(' and TipoPago = ''%s''', [Trim(TipoPago)]);
		if TipoPago = 'DA' then ConsultaDebitoAutomatico :=
		  format(' and CodigoDebitoAutomatico = %d',[CodigoDebitoAutomatico]);
	end;
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := format(
		  'Select' +
		  '  CodigoCuenta, Patente, VehiculosColores.Descripcion as Color,' +
		  '  VehiculosMarcas.Descripcion as Marca,' +
//		  '  VehiculosModelos.Descripcion as Modelo ' +
          'from ' +
          'VehiculosMarcas WITH (NOLOCK) ' +
//, VehiculosModelos, ' +
          ' Cuentas left outer join VehiculosColores (nolock) on Cuentas.CodigoColor = VehiculosColores.CodigoColor ' +
		  'where Cuentas.CodigoMarca = VehiculosMarcas.CodigoMarca' +
//		  '  and Cuentas.CodigoMarca = VehiculosModelos.CodigoMarca and Cuentas.CodigoModelo = VehiculosModelos.CodigoModelo' +
		  '  and Cuentas.CodigoPersona = %d %s %s',
		  [CodigoCliente, ConsultaTipoPago, ConsultaDebitoAutomatico]);

		Qry.Open;
		Combo.Clear;
		i := -1;
		While not Qry.Eof do begin
			Combo.Items.Add(trim(Qry.FieldByName('CodigoCuenta').AsString) + ' - ' +
			  trim(Qry.FieldByName('Patente').AsString) + ' - ' +
			  trim(Qry.FieldByName('Marca').AsString) + ' ' +
			  trim(Qry.FieldByName('Modelo').AsString) + ' ' +
			  trim(Qry.FieldByName('Color').AsString) +
			  Space(200) + Qry.FieldByName('CodigoCuenta').AsString);
			if Qry.FieldByName('CodigoCuenta').AsInteger = CodigoCuenta then
			  i := Combo.Items.Count - 1;
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarPaises
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo: TComboBox; CodigoPais: AnsiString = ''
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarPaises(Conn: TADOConnection; Combo: TComboBox; CodigoPais: AnsiString = '');
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'SELECT * FROM Paises  WITH (NOLOCK) ORDER BY DESCRIPCION ';
		Qry.Open;
		Combo.Clear;
		i := -1;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString +
			  Space(200) + Qry.FieldByName('CodigoPais').AsString);
			if Trim(Qry.FieldByName('CodigoPais').AsString) = Trim(CodigoPais) then
			  i := Combo.Items.Count - 1;
			Qry.Next;
		end;
        if (Combo.Items.Count > 0) and (i = -1) then i := 0;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarRegiones
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo: TComboBox; CodigoPais: AnsiString;
              CodigoRegion: AnsiString = ''; TieneItemNinguno: boolean = false
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarRegiones(Conn: TADOConnection; Combo: TComboBox; CodigoPais: AnsiString; CodigoRegion: AnsiString = ''; TieneItemNinguno: boolean = false); overload;
Var
	i: Integer;
	Qry: TADOQuery;
    S: AnsiString;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
        S := 'SELECT * FROM Regiones  WITH (NOLOCK) WHERE CodigoPais = ''' + CodigoPais + '''';
        //S := S + ' ORDER BY Descripcion ';
		Qry.SQL.Text := S;
		Qry.Open;
		Combo.Clear;
		i := -1;
        if TieneItemNinguno then begin
			Combo.Items.Add(SIN_ESPECIFICAR + Space(200) + '');
            i := 0;
        end;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString +
			  Space(200) + Qry.FieldByName('CodigoRegion').AsString);

			if Trim(Qry.FieldByName('CodigoRegion').AsString) = Trim(CodigoRegion) then begin
			  i := Combo.Items.Count - 1;
            end;
			Qry.Next;
		end;

        if (Combo.Items.Count > 0) and (i = -1) then i := 0;
		Combo.ItemIndex := i;

	finally
		Qry.Close;
		Qry.Free;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarComunas
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo: TComboBox;
              CodigoPais, CodigoRegion: AnsiString;
              CodigoComuna: AnsiString = '';
              TieneItemNinguno: boolean = false;
              TieneItemSeleccionar: Boolean=False
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarComunas(Conn: TADOConnection; Combo: TComboBox; CodigoPais, CodigoRegion: AnsiString; CodigoComuna: AnsiString = ''; TieneItemNinguno: boolean = false;TieneItemSeleccionar: Boolean=False);
Var
	i: Integer;
	Qry: TADOQuery;
    s: AnsiString;
begin
	Qry := TAdoQuery.Create(nil);
	try
        s := '';
		Qry.Connection := Conn;
        s := 'SELECT * FROM Comunas  WITH (NOLOCK) WHERE CodigoPais = ''' +
		  CodigoPais + ''' AND CodigoRegion = ''' + CodigoRegion + ''' AND CodigoComuna <> 999';
        s := s + ' ORDER BY Descripcion ';
        Qry.SQL.Text := s;
		Qry.Open;
		Combo.Clear;
		i := -1;
        if TieneItemSeleccionar=true then begin
			Combo.Items.Add(SELECCIONAR + Space(200) + '');
            i := i+1;
        end;

        if TieneItemNinguno then begin
			Combo.Items.Add(SIN_ESPECIFICAR + Space(200) + '');
            i := i+1;
        end;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString +
			  Space(200) + Qry.FieldByName('CodigoComuna').AsString);
			if Trim(Qry.FieldByName('CodigoComuna').AsString) = Trim(CodigoComuna) then
			  i := Combo.Items.Count - 1;
			Qry.Next;
		end;
        if (Combo.Items.Count > 0) and (i = -1) then i := 0;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

procedure CargarComunasConRegion(Conn: TADOConnection; Combo: TComboBox; CodigoPais: AnsiString; CodigoRegion: AnsiString = ''; CodigoComuna: AnsiString = ''; TieneItemNinguno: boolean = false);
Var
	Qry: TADOQuery;
    codigoFull, s: AnsiString;

begin
	Qry := TAdoQuery.Create(nil);
	try
        s := '';
        codigoFull := CodigoRegion + CodigoComuna;
		Qry.Connection := Conn;
        {
        s := 'SELECT * FROM VW_ComunasRegiones (nolock) WHERE CodigoPais = ''' +
		  CodigoPais + ''';
        s := s + ' ORDER BY DescripcionComuna ';
        Qry.SQL.Text := s;
		Qry.Open;
		Combo.Clear;
		i := -1;
        if TieneItemNinguno then begin
			Combo.Items.Add(SIN_ESPECIFICAR + Space(200) + '');
            i := 0;
        end;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('DescripcionComuna').AsString +
			  Space(200) + Qry.FieldByName('CodigoRegion').AsString) + Qry.FieldByName('CodigoComuna').AsString);
			if Trim(Qry.FieldByName('CodigoComuna').AsString) = Trim(CodigoComuna) then
			  i := Combo.Items.Count - 1;
			Qry.Next;
		end;
        if (Combo.Items.Count > 0) and (i = -1) then i := 0;
		Combo.ItemIndex := i;
        }
	finally
		Qry.Close;
		Qry.Free;
	end;
end;


procedure CargarCallesComuna(Conn: TADOConnection; Combo: TComboBox; CodigoPais,
                        CodigoRegion: AnsiString; CodigoComuna: AnsiString = '';
                        CodigoCalle: integer = -1; CodigoSegmento:Integer = -1);
Var
	i: Integer;
    SP: TADOStoredProc;

begin
	SP := TADOStoredProc.Create(nil);
	try
        SP.Connection := Conn;
        with SP, Parameters do begin
            ProcedureName := 'ObtenerCalles';
            CreateParameter('@CodigoPais', ftString, pdInput, 3, CodigoPais);
            CreateParameter('@CodigoRegion',  ftString, pdInput, 3, CodigoRegion);
            CreateParameter('@CodigoComuna',  ftString, pdInput, 3, NULL);
            CreateParameter('@NumeroCalle', ftInteger, pdInput, 0, NULL);
            CreateParameter('@Descripcion', ftString, pdInput, 80, NULL);
            CreateParameter('@SoloCallesPropias', ftBoolean, pdInput, 0, false);
            CreateParameter('@CodigoCalle', ftInteger, pdInput, 0, CodigoCalle);
            CreateParameter('@CodigoSegmento', ftInteger, pdInput, 0, CodigoSegmento);

            if CodigoComuna <> '' then ParamByName('@CodigoComuna').Value := CodigoComuna;
            open;
            SP.First;
        end;

        //Parametros del SP: Pais, Region, Comuna, Numero Calle, Descripcion...
		Combo.Clear;
		i := -1;
		While not SP.Eof do begin
            //Esta soluci�n es extra�a porque se necesita poder escribir en el combo
            //en el que hay que setear: Autocomplete = true, CharCase mayus., Style = DropDown
            //*Ojo: No es un puntero sino un integer!!!
            //if (cb_calle.ItemIndex < 0) then  result := -1 (Si no esta esto da Error Out of index!)
            //else result:=Integer(cb_calle.Items.Objects[cb_Calle.ItemIndex]);
			Combo.Additem(SP.FieldByName('Descripcion').AsString,TObject(SP.FieldByName('CodigoCalle').AsInteger*1000+(SP.FieldByName('CodigoComuna').AsInteger)) ); // guardo el codigo de calle *1000 + codigo comuna OJO que codigo de comuna debe ser numerico
			if SP.FieldByName('CodigoCalle').AsInteger = CodigoCalle then
			  i := Combo.Items.Count - 1;
			SP.Next;
		end;
//        if (Combo.Items.Count > 0) and (i = -1) then i := 0;
		Combo.ItemIndex := i;
	finally
		SP.Close;
		SP.Free;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarCategoriasVehiculos
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection;
              Combo: TComboBox;
              CodigoCategoria: Integer = 0
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarCategoriasVehiculos(Conn: TADOConnection; Combo: TComboBox; CodigoCategoria: Integer = 0);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'SELECT Categoria, ''Cat. '' + CAST(Categoria AS VARCHAR) + '' - '' + RTRIM(LTRIM(Descripcion)) AS Descripcion FROM Categorias ORDER BY Categoria';
		Qry.Open;
		Combo.Clear;
		i := -1;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString +
			Space(200) + Qry.FieldByName('Categoria').AsString);
			if Qry.FieldByName('Categoria').AsInteger = CodigoCategoria then
				i := Combo.Items.Count - 1;
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

procedure CargarPuntosEntregaDisponibles(Conn: TADOConnection; Combo: TVariantComboBox; Ninguno: Boolean = True; CodigoPuntoEntrega: Integer = 0);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'EXEC ObtenerPuntosDeEntrega';
		Qry.Open;
		Combo.Clear;
		i := -1;
        if Ninguno then begin
        	Combo.Items.Add(SELECCIONAR, 0);
            i := 0;
		end;
       While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString,
              Istr(Qry.FieldByName('CodigoPuntoEntrega').AsInteger));
			if (Qry.FieldByName('CodigoPuntoEntrega').AsInteger = CodigoPuntoEntrega) then begin
            	i := Combo.Items.Count - 1;
            end;
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarPuntosEntrega
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection;
              Combo: TVariantComboBox;
              Ninguno: Boolean = True;
              CodigoPuntoEntrega: Integer = 0
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarPuntosEntrega(Conn: TADOConnection; Combo: TVariantComboBox; Ninguno: Boolean = True; CodigoPuntoEntrega: Integer = 0);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'SELECT * FROM PUNTOSENTREGA  WITH (NOLOCK) WHERE ACTIVO = 1 ORDER BY DESCRIPCION ';
		Qry.Open;
		Combo.Clear;
		i := -1;
        if Ninguno then begin
        	Combo.Items.Add(SELECCIONAR, 0);
            i := 0;
		end;
       While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString
                + ' (' + Qry.FieldByName('CodigoPuntoEntrega').AsString + ')',
              Istr(Qry.FieldByName('CodigoPuntoEntrega').AsInteger));
			if (Qry.FieldByName('CodigoPuntoEntrega').AsInteger = CodigoPuntoEntrega) then begin
            	i := Combo.Items.Count - 1;
            end;
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

{-----------------------------------------------------------------------------
  Procedure: CargarPuntosVenta
  Author:    ggomez
  Date:      01-Jun-2005
  Arguments:
    - Conn: TADOConnection. Conexi�n a utilizar para obtener los datos.
    - Combo: TVariantComboBox. Combo en el cual cargar los datos.
    - Ninguno: Boolean = True. Indica si se debe cargar el item Seleccionar.
    - CodigoPuntoEntrega: Integer = 0. Punto de Entrega del cual obtener los
    - PuntoVentaSeleccionar: Integer = 0. Indica el punto de vena a seleccionar.
    Puntos de Venta.
  Result:    None
-----------------------------------------------------------------------------}
procedure CargarPuntosVenta(Conn: TADOConnection; Combo: TVariantComboBox;
    Ninguno: Boolean = True; CodigoPuntoEntrega: Integer = 0;
    PuntoVentaSeleccionar: Integer = 0);
Var
	i: Integer;
	sp: TADOStoredProc;
begin
	sp := TADOStoredProc.Create(nil);
	try
		sp.Connection := Conn;
		sp.ProcedureName := 'ObtenerPuntosVenta';
        sp.Parameters.Refresh;
		sp.Parameters.ParamByName('@CodigoPuntoEntrega').Value := CodigoPuntoEntrega;
		sp.Open;
		Combo.Clear;
		i := -1;
    if Ninguno then begin
        	Combo.Items.Add(SELECCIONAR, 0);
            i := 0;
		end;
    while not sp.Eof do
    begin
		  //Combo.Items.Add(sp.FieldByName('Descripcion').AsString,sp.FieldByName('CodigoPuntoEntrega').AsInteger);    //SS_1147_NDR_20140603
		  Combo.Items.Add(sp.FieldByName('Descripcion').AsString,sp.FieldByName('CodigoPuntoVenta').AsInteger);        //SS_1147_NDR_20140603
		  //if (sp.FieldByName('CodigoPuntoEntrega').AsInteger = PuntoVentaSeleccionar) then begin                       //SS_1147_NDR_20140603
		  if (sp.FieldByName('CodigoPuntoVenta').AsInteger = PuntoVentaSeleccionar) then begin                       //SS_1147_NDR_20140603
          	i := Combo.Items.Count - 1;
      end;
		  sp.Next;
		end;
		Combo.ItemIndex := i;
	finally
		sp.Close;
		sp.Free;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarTransportistas
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection;
              Combo: TVariantComboBox;
              CodigoTransportista: Integer = 0
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarTransportistas(Conn: TADOConnection; Combo: TVariantComboBox; CodigoTransportista: Integer = 0);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
        //Rev.32 / 20-Abril-2011 / Nelson Droguett Sierra
		Qry.SQL.Text := 'SELECT * FROM MaestroTransportistas WITH (NOLOCK) ORDER BY Descripcion ASC';       //PAR00133-NDR-20110420
		Qry.Open;
		Combo.Clear;
    	i := 0;
       	Combo.Items.Add(SELECCIONAR, 0);
       While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString,
              Istr(Qry.FieldByName('CodigoTransportista').AsInteger));
			if (Qry.FieldByName('CodigoTransportista').AsInteger = CodigoTransportista) then begin
            	i := Combo.Items.Count - 1;
            end;
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;



{-----------------------------------------------------------------------------
  Function Name: CargarVehiculosTipos
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection;
              Combo: TVariantComboBox;
              Ninguno: Boolean = True;
              tipoVehiculo: Integer = 0
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarVehiculosTipos(Conn: TADOConnection; Combo: TVariantComboBox;
                               Ninguno: Boolean = True; tipoVehiculo: Integer = 0);
Var
	i: Integer;
	sp: TADOStoredProc;
begin
	sp := TADOStoredProc.Create(nil);
	try
		sp.Connection := Conn;
		sp.ProcedureName := 'CRM_VehiculosTipos_SELECT';
		sp.Open;
		Combo.Clear;
		i := -1;
    if Ninguno then begin
        	Combo.Items.Add(SIN_ESPECIFICAR, 0);
          i := 0;
		end;
       While not sp.Eof do
       begin
			        Combo.Items.Add(sp.FieldByName('Descripcion').AsString,Istr(sp.FieldByName('CodigoTipoVehiculo').AsInteger));
			        if (sp.FieldByName('CodigoTipoVehiculo').AsInteger = tipoVehiculo) then begin
            	  i := Combo.Items.Count - 1;
              end;
			        sp.Next;
       end;
		Combo.ItemIndex := i;
	finally
		sp.Close;
		sp.Free;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarCategoriasPersonal
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection;
              Combo: TComboBox;
              CodigoCategoria: integer = 0
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarCategoriasPersonal(Conn: TADOConnection; Combo: TComboBox; CodigoCategoria: integer = 0);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text :=
        //Rev.32 / 20-Abril-2011 / Nelson Droguett Sierra
		' select * from CategoriasEmpleados  WITH (NOLOCK) ORDER BY Descripcion ASC';          //PAR00133-NDR-20110420
		Qry.Open;
		Combo.Clear;
		i := -1;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString +
			Space(200) + Qry.FieldByName('CodigoCategoriaEmpleado').AsString);
			if Qry.FieldByName('CodigoCategoriaEmpleado').AsInteger = CodigoCategoria then
				i := Combo.Items.Count - 1;
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarContextMarks
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection;
              Combo: TComboBox;
              ContextMark: integer = 0
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarContextMarks(Conn: TADOConnection; Combo: TComboBox; ContextMark: integer = 0);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text :=
		' select * from ContextMarks WITH (NOLOCK) ';
		Qry.Open;
		Combo.Clear;
		i := -1;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('ContextMark').AsString +
			Space(200) + Qry.FieldByName('ContextMark').AsString);
			if Qry.FieldByName('ContextMark').AsInteger = ContextMark then
				i := Combo.Items.Count - 1;
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;


{-----------------------------------------------------------------------------
  Function Name: CargarCategoriasVehiculosVariant
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection;
              Combo: TVariantComboBox;
              CategoriaAdicional: AnsiString = ''
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarCategoriasVehiculosVariant(Conn: TADOConnection;
                    Combo: TVariantComboBox; CategoriaAdicional: AnsiString = '');
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := ' select * from Categorias  WITH (NOLOCK) ';
		Qry.Open;
		Combo.Clear;
		i := -1;
        if CategoriaAdicional <> '' then begin
        	Combo.Items.Add(CategoriaAdicional, 0);
			i := 0;
		end;
        While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString,
                       Istr(Qry.FieldByName('Categoria').AsInteger));
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarCuentasBancarias
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; CheckList:
              TVariantCheckListBox; CodigoBanco: Integer = 0
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarCuentasBancarias(Conn: TADOConnection; CheckList: TVariantCheckListBox; CodigoBanco: Integer = 0);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'SELECT	CodigoTipoCuentaBancaria, ' +
	                    'Descripcion, ' +
	                    'ISNULL((SELECT TB.Activo ' +
                        'FROM TiposCuentasBancariasxBancos TB  WITH (NOLOCK) ' +
                        'WHERE TCB.CodigoTipoCuentaBancaria = TB.CodigoTipoCuentaBancaria AND TB.CodigoBanco = ' + inttostr(CodigoBanco) +
                        '), 0) AS Checked ' +
                        'FROM TiposCuentasBancarias TCB WITH (NOLOCK)';
		Qry.Open;
		CheckList.Items.Clear;
		While not Qry.Eof do begin
			  CheckList.Items.Add(Qry.FieldByName('Descripcion').AsString,
              Qry.FieldByName('CodigoTipoCuentaBancaria').AsInteger);
              Qry.Next;
		end;

        Qry.First;
        i:= 0;
		While not Qry.Eof do begin
			CheckList.Checked[i]:=Qry.FieldByName('Checked').AsBoolean;
            i:= i + 1;
			Qry.Next;
		end;

	finally
		Qry.Close;
		Qry.Free;
	end;
end;

procedure CargarTiposCuentasBancarias(Conn: TADOConnection; Combo: TVariantComboBox);
var
    sp: TADOStoredProc;
begin
    sp := TADOStoredProc.Create(Nil);
    try
        sp.Connection := Conn;
        sp.ProcedureName := 'ObtenerTiposCuentasBancarias';
        sp.Open;
        while not sp.Eof do begin
            Combo.Items.Add(Trim(sp.FieldByName('Descripcion').AsString), sp.FieldByName('CodigoTipoCuentaBancaria').AsInteger);
            sp.Next;
        end; // while

    finally
        sp.Close;
        sp.Free;
    end; // finally

end;

procedure CargarFechasParaReportes(Combo: TComboBox);
resourcestring
    MSG_HOY = 'Hoy';
    MSG_AYER = 'Ayer';
    MSG_MES_EN_CURSO = 'Mes en Curso';
    MSG_MES_ANTERIOR = 'Mes Anterior';
var
	Anio, Mes, Dia: Word;
	FechaMesAnterior: TDateTime;
begin
	Combo.Items.Clear;
	Combo.Items.Add(PadR(MSG_HOY, 200, ' ') + FormatDateTime('yyyymmdd', date) + FormatDateTime('yyyymmdd', date));
	Combo.Items.Add(PadR(MSG_AYER, 200, ' ') + FormatDateTime('yyyymmdd', (date-1)) + FormatDateTime('yyyymmdd', (date-1)));
	DecodeDate(Date, Anio, Mes, Dia);
	Combo.Items.Add(PadR(MSG_MES_EN_CURSO, 200, ' ') + FormatDateTime('yyyymmdd', EncodeDate(Anio, Mes, 1)) + FormatDateTime('yyyymmdd', (date)));
	//Mes anterior
	FechaMesAnterior := EncodeDate(Anio, Mes, 1) - 1;
	DecodeDate(FechaMesAnterior, Anio, Mes, Dia);
	Combo.Items.Add(PadR(MSG_MES_ANTERIOR, 200, ' ') + FormatDateTime('yyyymmdd', EncodeDate(Anio, Mes, 1)) + FormatDateTime('yyyymmdd', EncodeDate(Anio, Mes, Dia)));
	Combo.ItemIndex := 0;
end;


function  ObtenerEstadoOC(Estado:Byte): AnsiString;
resourcestring
	MSG_ABIERTA = 'Abierta';
	MSG_CERRADA = 'Cerrada';
	MSG_CANCELADA = 'Cancelada';
begin
	case Estado of
		0: result := MSG_CERRADA;
		1: result := MSG_ABIERTA;
		2: result := MSG_CANCELADA;
	end;
end;

function  ObtenerDescripcionAptitudCalidad(Estado: String):AnsiString;
begin
    if Estado = 'C' then Result := 'Lote Conforme';
    if Estado = 'N' then Result := 'Lote No Conforme';
    if Estado = 'P' then  Result := 'Lote Pendiente de Control';
end;

function ValidateControls(Controls: Array of TControl; Conditions: Array of Boolean;
  Caption: AnsiString; Messages: Array of String): Boolean;

	procedure ActivarTab(Ctrl: TControl);
	Var
		P: TControl;
	begin
		P := Ctrl.Parent;
		While P <> nil do begin
			if P is TTabSheet then begin
				TTabSheet(P).PageControl.ActivePage := TTabSheet(P);
			end;
			P := P.Parent;
		end;
	end;

Var
	i: Integer;
begin
	for i := Low(Controls) to High(Controls) do begin
		if not Conditions[i] then begin
			ActivarTab(Controls[i]);
			MsgBoxBalloon(Messages[i], Caption, MB_ICONSTOP, Controls[i]);
            //Rev.31 / 27-Julio-2010 / Nelson Droguett Sierra
			if ((Controls[i] is TWinControl) and (TWinControl(Controls[i]).Enabled)) then
            	TWinControl(Controls[i]).SetFocus;
			Result := False;
			Exit;
		end;
	end;
	Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: ValidarPais
  Author:
  Date Created:  /  /
  Description:
  Parameters: Connection: TADOConnection; CodigoPais: AnsiString
  Return Value: Boolean

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function ValidarPais(Connection: TADOConnection; CodigoPais: AnsiString): Boolean;
begin
	Result := QueryGetValueInt(Connection, 'SELECT 1 FROM Paises  WITH (NOLOCK) WHERE CodigoPais = ''' +
	  CodigoPais + '''') = 1;
end;

{-----------------------------------------------------------------------------
  Function Name: ValidarRegion
  Author:
  Date Created:  /  /
  Description:
  Parameters: Connection: TADOConnection; CodigoPais, CodigoRegion: AnsiString
  Return Value: Boolean

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function ValidarRegion(Connection: TADOConnection; CodigoPais, CodigoRegion: AnsiString): Boolean;
begin
	Result := QueryGetValueInt(Connection, 'SELECT 1 FROM Regiones  WITH (NOLOCK) WHERE CodigoPais = ''' +
	  CodigoPais + ''' AND CodigoRegion = ''' + CodigoRegion + '''') = 1;
end;

{-----------------------------------------------------------------------------
  Function Name: ValidarComuna
  Author:
  Date Created:  /  /
  Description:
  Parameters: Connection: TADOConnection; CodigoPais, CodigoRegion, CodigoComuna: AnsiString
  Return Value: Boolean

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function ValidarComuna(Connection: TADOConnection; CodigoPais, CodigoRegion, CodigoComuna: AnsiString): Boolean;
begin
	Result := QueryGetValueInt(Connection, 'SELECT 1 FROM Comunas  WITH (NOLOCK) WHERE CodigoPais = ''' +
	  CodigoPais + ''' AND CodigoRegion = ''' + CodigoRegion +
	  ''' AND CodigoComuna = ''' + CodigoComuna + '''') = 1;
end;

{-----------------------------------------------------------------------------
  Function Name:
  Author:
  Date Created:  /  /
  Description:
  Parameters: Connection: TADOConnection; CodigoMarca: Integer
  Return Value: Boolean

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function ValidarMarca(Connection: TADOConnection; CodigoMarca: Integer): Boolean;
begin
	Result := QueryGetValueInt(Connection,
	  'SELECT 1 FROM VehiculosMarcas  WITH (NOLOCK) WHERE CodigoMarca = ' +
	  IntToStr(CodigoMarca)) = 1;
end;

function  ValidarTarjetaCredito(Connection: TADOConnection; CodigoTarjeta: Integer; NumeroTarjeta: AnsiString;  var DescriError: Ansistring): Boolean;
Var
	SP: TAdoStoredProc;
begin
	SP := TAdoStoredProc.Create(nil);
	try
		SP.ProcedureName := 'ValidarTarjetaCredito';
		SP.Connection := Connection;
		SP.Parameters.CreateParameter('@CodigoTarjeta', ftInteger, pdInput, 0, NULL);
		SP.Parameters.CreateParameter('@NumeroTarjeta', ftString, pdInput, 20, NULL);
		SP.Parameters.CreateParameter('@Resultado', ftBoolean, pdInputOutput, 0, NULL);
		SP.Parameters.CreateParameter('@DescriError', ftString, pdInputOutput, 60, NULL);
		SP.Parameters.ParamByName('@CodigoTarjeta').Value := CodigoTarjeta;
		SP.Parameters.ParamByName('@NumeroTarjeta').Value := NumeroTarjeta;
		SP.ExecProc;
		Result := SP.Parameters.ParamByName('@Resultado').Value;
        if not Result then DescriError := SP.Parameters.ParamByName('@DescriError').Value;
		SP.Close;
	finally
		SP.Free;
	end;
end;

function  ValidarCuentaBancaria(Connection: TADOConnection; CodigoBanco: Integer; NumeroCuenta: AnsiString;  var DescriError: Ansistring): Boolean;
begin
    Result := True;
end;

//function BuscarDescripcionCuenta(Connection:TADOConnection; Cuenta: Integer): AnsiString;
//begin
//	Result := QueryGetValue(Connection,
//	  'SELECT dbo.ObtenerDescripcionPago(' + IntToStr(Cuenta) + ')');
//end;

function BuscarDatosVehiculo(Connection:TADOConnection; Cuenta: Integer): AnsiString;
begin
	Result := QueryGetValue(Connection,
	  'SELECT dbo.ObtenerDatosVehiculo(' + IntToStr(Cuenta) + ')');
end;

function ArmarNombreGenerico(Personeria,Nombre,Apellido,ApellidoMaterno,NombreContactoComercial,ApellidoContactoComercial,ApellidoMaternoContactoComercial:String):String;
begin
        if Personeria = PERSONERIA_JURIDICA then
                Result := ArmarNombrePersona(Personeria, ArmarNombrePersona(PERSONERIA_FISICA, NombreContactoComercial, ApellidoContactoComercial, ApellidoMaternoContactoComercial), Apellido, '')
        else
                Result := ArmarNombrePersona(Personeria, Nombre, Apellido, ApellidoMaterno);
end;

function ArmarNombrePersona(Personeria,Nombre,Apellido,ApellidoMaterno:String):String;
begin
//    result:=trim(QueryGetValue(DMConnections.BaseCAC, Format(
//                      'SELECT dbo.ArmarNombrePersona(''%s'', ''%s'', ''%s'',''%s'')',
//                      [Trim(Personeria), Trim(Apellido), trim(ApellidoMaterno), trim(Nombre)])));
// Revision 8

    result:=trim(QueryGetValue(DMConnections.BaseCAC, Format(
                      'SELECT dbo.ArmarNombrePersona(%s, %s, %s,%s)',
            [QuotedStr(Trim(Personeria)), QuotedStr(Trim(Apellido)), QuotedStr(trim(ApellidoMaterno)), QuotedStr(trim(Nombre))])));



end;

function  BuscarApellidoNombreCliente(Conection :TADOConnection; CodigoCliente: Integer; NumeroDocumento : String = ''): AnsiString;
resourcestring
    MSG_CONTACTO_CLIENTE = 'No se pudo obtener la descripcion de la persona';
var
    SP : TADOStoredProc;
begin
    SP := TADOStoredProc.Create(nil);
    try
        with SP do begin
            Connection := Conection;
            ProcedureName := 'ObtenerCliente';
            Parameters.Refresh;
            if CodigoCliente > 0 then
                Parameters.ParamByName('@CodigoCliente').Value := CodigoCliente
            else
                Parameters.ParamByName('@CodigoCliente').Value := NULL;
            if Trim(NumeroDocumento) <> '' then begin
                Parameters.ParamByName('@NumeroDocumento').Value := Trim(NumeroDocumento);
                Parameters.ParamByName('@CodigoDocumento').Value := TIPO_DOCUMENTO_RUT;
            end else begin
                Parameters.ParamByName('@NumeroDocumento').Value := NULL;
                Parameters.ParamByName('@CodigoDocumento').Value := NULL;
            end;

            Open;

            //Armo nombre o razon social seg�n al personer�a.
            if FieldByName('PERSONERIA').AsString = PERSONERIA_FISICA then
                Result := ArmaNombreCompleto(trim(Fieldbyname('Apellido').AsString),
                                             Trim(Fieldbyname('ApellidoMaterno').AsString),
                                             Trim(Fieldbyname('Nombre').AsString))
            else
                Result := ArmaRazonSocial(trim(Fieldbyname('Apellido').AsString),
                                        Trim(Fieldbyname('Nombre').AsString));
            Close;

         end;

    except
		result := MSG_CONTACTO_CLIENTE;
    end;
    SP.Free;
end;

{******************************** function Header ******************************
function Name: BuscarIndexOrdenServicio
Author       : dcalani
Date Created : 28/07/2004
Description  : Busca dentro de la estructura de OS, la OS pedida.
Parameters   : var ListaOrdenServicio:ATOrdenServicio;CodigoOrdenServicio:Integer; Patente: String = ''
Return Value : integer
*******************************************************************************}
//function BuscarIndexOrdenServicio(var ListaOrdenServicio:ATOrdenServicio;CodigoOrdenServicio:Integer; Patente: String = ''):integer;
//var
//    i:Integer;
//begin
//    result:=-1;
//    for i:=0 to length(ListaOrdenServicio)-1 do
//        if (ListaOrdenServicio[i].CodigoTipoOrdenServicio = CodigoOrdenServicio) and
//            ((Trim(Patente) = '') or ( Trim(ListaOrdenServicio[i].Patente) = Trim(Patente))) then result:=i;
//end;

{******************************** function Header ******************************
function Name: QuitarDocumentacion
Author       : dcalani
Date Created : 28/07/2004
Description  : Quita una OS a la estructura de OS
Parameters   : var ListaOrdenServicio:ATOrdenServicio;OrdenServicio:Integer; Patente: String = ''
Return Value : None
*******************************************************************************}
procedure QuitarDocumentacion(var ListaOrdenServicio:ATOrdenServicio;OrdenServicio:Integer; Patente: String = '');
var
    i:Integer;
begin
    i := 0;
    while i < length(ListaOrdenServicio) do begin
        if (ListaOrdenServicio[i].CodigoTipoOrdenServicio = OrdenServicio) and
            ((Trim(Patente) = '') or ( Trim(ListaOrdenServicio[i].Patente) = Trim(Patente)))
            then begin
                if i < length(ListaOrdenServicio) - 1 then ListaOrdenServicio[i] := ListaOrdenServicio[i+1];
                SetLength(ListaOrdenServicio,length(ListaOrdenServicio)-1);
            end else inc(i);
    end;
end;

{******************************** function Header ******************************
function Name: CargarDocumentacion
Author       : dcalani
Date Created : 28/07/2004
Description  : agrega una OS a la estructura de OS
Parameters   : Conn: TADOConnection; var ListaOrdenServicio:ATOrdenServicio;OrdenServicio:Integer;Personeria:String;PatenteVehiculo:String='';IndiceVehiculo:Integer=-1
Return Value : None
*******************************************************************************}
procedure CargarDocumentacion(Conn: TADOConnection; var ListaOrdenServicio:ATOrdenServicio;OrdenServicio:Integer;Personeria:String;PatenteVehiculo:String='';IndiceVehiculo:Integer=-1);

    function ObtenerAccionRNUT(Conn : TADOConnection; OrdenServicio : Integer):Integer;
    begin
        result := QueryGetValueInt(Conn, format('EXEC ObtenerAccionRNUT %d',[OrdenServicio]));
    end;

var
    ObtenerDocRequeridosConvenioTipoOrdenServicio:TADOStoredProc;
begin
    ObtenerDocRequeridosConvenioTipoOrdenServicio:=TADOStoredProc.create(nil);

    try
          with  ObtenerDocRequeridosConvenioTipoOrdenServicio do begin

            Connection := Conn;
            ProcedureName := 'ObtenerDocRequeridosConvenioTipoOrdenServicio';
            Parameters.Refresh;

            with Parameters do begin
                ParamByName('@SoloImpresion').Value:=NULL;
                ParamByName('@Personeria').Value:=Personeria;
                ParamByName('@Tipo').Value:=null;
                ParamByName('@CodigoTipoOrdenServicio').Value:=OrdenServicio;
            end;

            Open;

            SetLength(ListaOrdenServicio,length(ListaOrdenServicio)+1);
            ListaOrdenServicio[high(ListaOrdenServicio)].CodigoTipoOrdenServicio := OrdenServicio;
            ListaOrdenServicio[high(ListaOrdenServicio)].Patente := PatenteVehiculo;
            ListaOrdenServicio[high(ListaOrdenServicio)].AccionRNUT := ObtenerAccionRNUT(Conn, OrdenServicio);

            while not eof do begin
                SetLength(ListaOrdenServicio[high(ListaOrdenServicio)].Documentacion,length(ListaOrdenServicio[high(ListaOrdenServicio)].Documentacion)+1);
                with ListaOrdenServicio[high(ListaOrdenServicio)].Documentacion[high(ListaOrdenServicio[high(ListaOrdenServicio)].Documentacion)] do begin
                    CodigoDocumentacionRespaldo := FieldByName('CodigoDocumentacionRespaldo').AsInteger;
                    IndiceVehiculoRelativo := IndiceVehiculo;
                    TipoPatente := PATENTE_CHILE;
                    Patente := PatenteVehiculo;
                    NoCorresponde := False;
                    InfoAdicional := '';
                    Marcado := False;
                    Obligatorio := FieldByName('Obligatorio').AsBoolean;
                    CodigoOrdenServicio := OrdenServicio;
                    Orden := 1;
                    SoloImpresion := FieldByName('SoloImpresion').AsInteger;
                end;
                next;
            end;
        end;

    finally
        ObtenerDocRequeridosConvenioTipoOrdenServicio.close;
        ObtenerDocRequeridosConvenioTipoOrdenServicio.free;
    end;

end;



{-----------------------------------------------------------------------------
  Function Name: BuscarCategoriaVehiculo
  Author:
  Date Created:  /  /
  Description:
  Parameters: Connection :TADOConnection; CodigoCuenta:LongInt; Var FactorTarifa: Double
  Return Value: AnsiString

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function  BuscarCategoriaVehiculo (Connection :TADOConnection; CodigoCuenta:LongInt; Var FactorTarifa: Double): AnsiString;
resourcestring
    MSG_CATEGORIA = ' No se pudo obtener la categoria';
var
    BuscaCategoria :TADOQuery;
begin
    BuscaCategoria := TADOQuery.Create(nil);
    try
         BuscaCategoria.Connection := Connection;
         BuscaCategoria.CursorLocation :=clUseServer;
         BuscaCategoria.CursorType := ctDynamic;
		 BuscaCategoria.SQL.Text :=
         ' SELECT CATEGORIAS.DESCRIPCION, CATEGORIAS.FACTORTARIFA AS TARIFA '+
         ' FROM CUENTAS WITH (NOLOCK) , CATEGORIAS  WITH (NOLOCK) '+
         ' WHERE CUENTAS.CODIGOCUENTA = :CUENTA AND CUENTAS.CATEGORIA = CATEGORIAS.CATEGORIA';
		 with BuscaCategoria do begin
            parameters.ParamByName('Cuenta').Value := CodigoCuenta;
            active := true;
            Factortarifa := FieldByname('Tarifa').AsFloat;
            Result := Trim(Fieldbyname('Descripcion').AsString);
        end;
    except
        BuscaCategoria.active := false;
        result := MSG_CATEGORIA;
    end;
    BuscaCategoria.Free;
end;

function DescriTipoComprobante(TipoComprobante: AnsiString):AnsiString;
resourcestring
    STR_FACTURA              	= 'Factura';
    STR_NOTA_CREDITO         	= 'Nota de Cr�dito';
    STR_NOTA_CREDITO_A_COBRO	= 'Nota de Cr�dito a Nota de Cobro';
    STR_NOTA_DEBITO          	= 'Nota de D�bito';
    STR_NOTA_COBRO           	= 'Nota de Cobro';
    STR_BOLETA               	= 'Boleta';
    STR_BOLETA_MANUAL        	= 'Boleta Manual';
    STR_TICKET               	= 'Ticket';
    STR_NOTA_COBRO_INFRACTOR    = 'Nota de Cobro Infractor';
    STR_COMPROBANTE_INVALIDO 	= 'Comprobante inv�lido';
    STR_COMPROBANTE_CUOTA       = 'Comprobante Cuota';

    STR_FACTURA_EXENTA          = 'Factura Exenta (Electr�nica)';
    STR_FACTURA_AFECTA          = 'Factura Afecta (Electr�nica)';
    STR_BOLETA_EXENTA           = 'Boleta Exenta (Electr�nica)';
    STR_BOLETA_AFECTA           = 'Boleta Afecta (Electr�nica)';
    STR_NOTA_CREDITO_ELECTRONICA = 'Nota de Cr�dito (Electr�nica)';

    STR_TRASPASO_CREDITO		= 'Traspaso Cr�dito';			//SS_740_MBE_20110720
    STR_TRASPASO_DEBITO			= 'Traspaso D�bito';			//SS_740_MBE_20110720

    STR_DEVOLUCION_DINERO		= 'Comprobante Devoluci�n de Dinero'; 		//SS959-20110617-MBE

   //STR_SALDO_INICIAL_OC        = 'Saldo Inicial Otras Concesionarias';        // SS_1015_HUR_20120413 // SS_1015_HUR_20120417
   //STR_ANULA_SALDO_INICIAL_OC   = 'Anula Saldo Inicial Otras Concesionarias'; // SS_1015_HUR_20120413 // SS_1015_HUR_20120417
   STR_SALDO_INICIAL_DEUDOR         = 'Saldo Inicial Deudor';                   // SS_1015_HUR_20120417
   STR_ANULA_SALDO_INICIAL_DEUDOR   = 'Anula Saldo Inicial Deudor';             // SS_1015_HUR_20120417
   STR_SALDO_INICIAL_ACREEDOR       = 'Saldo Inicial Acreedor';                 // SS_1015_HUR_20120417
   STR_ANULA_SALDO_INICIAL_ACREEDOR = 'Anula Saldo Inicial Acreedor';           // SS_1015_HUR_20120417
   STR_CUOTA_OTRAS_CONCESIONARIAS       = 'Cuota Otras Concesionarias';         // SS-1015-NDR-20120510
   STR_ANULA_CUOTA_OTRAS_CONCESIONARIAS = 'Anula Cuota Otras Concesionarias';   // SS-1015-NDR-20120510
begin
    	if 	TipoComprobante = TC_FACTURA then Result := STR_FACTURA
        else if TipoComprobante = TC_NOTA_CREDITO then  Result := STR_NOTA_CREDITO
        else if TipoComprobante = TC_NOTA_CREDITO_A_COBRO then  Result := STR_NOTA_CREDITO_A_COBRO
        else if TipoComprobante = TC_NOTA_DEBITO then  Result := STR_NOTA_DEBITO
        else if TipoComprobante = TC_NOTA_COBRO	then  Result := STR_NOTA_COBRO
        else if TipoComprobante = TC_BOLETA	then  Result := STR_BOLETA
        else if TipoComprobante = TC_TICKET	then  Result := STR_TICKET
        else if TipoComprobante = TC_NOTA_COBRO_INFRACCIONES then Result := STR_NOTA_COBRO_INFRACTOR
        else if TipoComprobante = TC_COMPROBANTE_CUOTA then Result := STR_COMPROBANTE_CUOTA
        // agrego comprobantes electronicos
        else if TipoComprobante = TC_FACTURA_AFECTA then Result := STR_FACTURA_AFECTA
        else if TipoComprobante = TC_FACTURA_EXENTA then Result := STR_FACTURA_EXENTA
        else if TipoComprobante = TC_BOLETA_AFECTA then Result := STR_BOLETA_AFECTA
        else if TipoComprobante = TC_BOLETA_EXENTA then Result := STR_BOLETA_EXENTA
        else if TipoComprobante = TC_NOTA_CREDITO_ELECTRONICA then Result := STR_NOTA_CREDITO_ELECTRONICA
        else if TipoComprobante = TC_TRASPASO_CREDITO then Result := STR_TRASPASO_CREDITO		//SS_740_MBE_20110720
        else if TipoComprobante = TC_TRASPASO_DEBITO then Result := STR_TRASPASO_DEBITO			//SS_740_MBE_20110720
        else if TipoComprobante = TC_DEVOLUCION_DINERO then Result := STR_DEVOLUCION_DINERO		//SS959-20110617-MBE
        //else if TipoComprobante = TC_SALDO_INICIA_OC then Result := STR_SALDO_INICIAL_OC                         // SS_1015_HUR_20120413 // SS_1015_HUR_20120417
        //else if TipoComprobante = TC_ANULA_SALDO_INICIAL_OC then Result := STR_ANULA_SALDO_INICIAL_OC            // SS_1015_HUR_20120413 // SS_1015_HUR_20120417
        else if TipoComprobante = TC_SALDO_INICIAL_DEUDOR then Result := STR_SALDO_INICIAL_DEUDOR                  // SS_1015_HUR_20120417
        else if TipoComprobante = TC_ANULA_SALDO_INICIAL_DEUDOR then Result := STR_ANULA_SALDO_INICIAL_DEUDOR      // SS_1015_HUR_20120417
        else if TipoComprobante = TC_SALDO_INICIAL_ACREEDOR then Result := STR_SALDO_INICIAL_ACREEDOR              // SS_1015_HUR_20120417
        else if TipoComprobante = TC_ANULA_SALDO_INICIAL_ACREEDOR then Result := STR_ANULA_SALDO_INICIAL_ACREEDOR  // SS_1015_HUR_20120417
        else if TipoComprobante = TC_CUOTAS_OTRAS_CONCESIONARIAS then Result := STR_CUOTA_OTRAS_CONCESIONARIAS                // SS-1015-NDR-20120510
        else if TipoComprobante = TC_ANULA_CUOTAS_OTRAS_CONCESIONARIAS then Result := STR_ANULA_CUOTA_OTRAS_CONCESIONARIAS    // SS-1015-NDR-20120510
        else Result := STR_COMPROBANTE_INVALIDO;
end;

procedure CargarTipoComprobante(Combo: TComboBox; TipoComprobante: AnsiString = TC_FACTURA);
begin
    Combo.Items.Clear;
    combo.Items.Add( DescriTipoComprobante(TC_FACTURA) + Space(200) + TC_FACTURA);
    combo.Items.Add( DescriTipoComprobante(TC_NOTA_CREDITO) + Space(200) + TC_NOTA_CREDITO);
    combo.Items.Add( DescriTipoComprobante(TC_NOTA_DEBITO) + Space(200) + TC_NOTA_DEBITO);

    if TipoComprobante = TC_FACTURA then
        Combo.ItemIndex := 0
    else if TipoComprobante = TC_NOTA_CREDITO then
        Combo.ItemIndex := 1
    else if TipoComprobante = TC_NOTA_DEBITO then
        Combo.ItemIndex := 2;

end;

procedure CargarMediosPagoAutomatico(Combo: TVariantComboBox; TipoPagoAutomatico: Integer = -1; TieneSeleccione : Boolean = True; TieneSinEspecificar : Boolean = False);
begin
    Combo.Items.Clear;
    if TieneSeleccione then combo.Items.Add( TPA_SELECCIONE, null)     //Space(200)+ IntToStr(TPA_PAC));
    else if TieneSinEspecificar then combo.Items.Add(SIN_ESPECIFICAR, null);
    combo.Items.Add( TPA_NINGUNO_DESC, 0);      //Space(200)+ IntToStr(TPA_NINGUNO));
    combo.Items.Add( TPA_PAT_DESC, 1);          //Space(200)+ IntToStr(TPA_PAT));
    combo.Items.Add( TPA_PAC_DESC, 2);          //Space(200)+ IntToStr(TPA_PAC));
    
    //if TipoPagoAutomatico = TPA_NINGUNO then    TPA_SELECCIONE
    
    if TipoPagoAutomatico >= 0 then
        Combo.Value := TipoPagoAutomatico
    else
        Combo.ItemIndex:=0; //Combo.Value := -1;
end;

function DescriEstadoComprobante(TipoEstado: AnsiString):AnsiString;
resourcestring
    STR_IMPAGO          = 'Impago';
    STR_PAGO            = 'Pagado';
    STR_CANCELADO       = 'Cancelado';
    STR_ESTADO_INVALIDO = 'Estado inv�lido';
begin
	Case TipoEstado[1] of
		COMPROBANTE_IMPAGO    : Result := STR_IMPAGO;
	    COMPROBANTE_PAGO      : Result := STR_PAGO;
    	COMPROBANTE_CANCLEADO : Result := STR_CANCELADO;
        else Result := STR_ESTADO_INVALIDO;
    end;
end;
{-----------------------------------------------------------------------------
  Function Name: BuscarDescripcionRegion
  Author:
  Date Created:  /  /
  Description:
  Parameters: Connection:TADOConnection; pais, region: AnsiString
  Return Value: AnsiString

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function BuscarDescripcionRegion(Connection:TADOConnection; pais, region: AnsiString): AnsiString;
var select: AnsiString;
begin
    if (pais <> Null) and (region <> null) and
       (trim(pais) <> '')   and (trim(region) <> '') then
    begin
        select := 'SELECT Descripcion FROM Regiones  WITH (NOLOCK) WHERE CodigoPais = ''' +
                  pais + '''' + ' AND CodigoRegion = ''' +  region + '''';
        Result := QueryGetValue(Connection, select);
    end
	else Result := '';
end;

{-----------------------------------------------------------------------------
  Function Name: BuscarDescripcionComuna
  Author:
  Date Created:  /  /
  Description:
  Parameters: Connection:TADOConnection; pais, region, comuna: AnsiString
  Return Value: AnsiString

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function BuscarDescripcionComuna(Connection:TADOConnection; pais, region, comuna: AnsiString): AnsiString;
var select: AnsiString;
begin
    if (pais <> Null) and (region <> null) and (comuna <> null) and
       (trim(pais) <> '')   and (trim(region) <> '')   and (trim(comuna) <> '') then
    begin
        select := 'SELECT Descripcion FROM Comunas  WITH (NOLOCK) WHERE CodigoPais = ''' +
                pais + ''' AND CodigoRegion = '''  + region +
                ''' AND CodigoComuna = '''    + comuna + '''';
        Result := trim(QueryGetValue(Connection, select));
    end
    else Result := '';
end;

{-----------------------------------------------------------------------------
  Function Name: BuscarDescripcionCiudad
  Author:
  Date Created:  /  /
  Description:
  Parameters: Connection:TADOConnection; pais, region, ciudad: AnsiString
  Return Value: AnsiString

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function BuscarDescripcionCiudad(Connection:TADOConnection; pais, region, ciudad: AnsiString): AnsiString;
var select: AnsiString;
begin
    if (pais <> Null) and (region <> null) and (ciudad <> null) and
	   (trim(pais) <> '')   and (trim(region) <> '')   and (trim(ciudad) <> '') then
    begin
        select := 'SELECT Descripcion FROM Ciudades  WITH (NOLOCK) WHERE CodigoPais = ''' +
                pais + ''' AND CodigoRegion = '''  + region +
				''' AND CodigoCiudad = '''    + ciudad + '''';
        Result := QueryGetValue(Connection, select);
    end
    else Result := '';
end;

function ArmaNombreCompleto(Apellido, ApellidoMaterno, Nombre: AnsiString): AnsiString;
begin
    if (ApellidoMaterno <> '') then result := trim(Apellido) + ' ' + trim(ApellidoMaterno) + ', ' + trim(Nombre)
    else result := trim(Apellido) + ', ' + trim(Nombre);
end;

function ArmaRazonSocial(Apellido, Nombre: AnsiString): AnsiString;
begin
    //En las personas de tipo juridica el Apellido es la Razon Social
    //y el Nombre ser�a el Nombre de Fantas�a. Si son iguales retorno solo uno
    if (trim(Apellido) <> trim(Nombre)) and (Trim(Nombre) <> '') then
		result := Apellido + ' (' + Nombre + ')'
    else
        result := Apellido;
end;


//INICIO: TASK_009_ECA_20160506
//BEGIN : SS_1147Q_NDR_20141202 --------------------------------------------------------------------------------
function FormatearNumeroConvenio(Numeroconvenio:String):String;
var
    largo:Integer;
begin
    largo:=Length(Trim(Numeroconvenio));
    Result := StrLeft(NumeroConvenio,3) + '-' + copy(NumeroConvenio,4,largo)
//    if ObtenerCodigoConcesionariaNativa=CODIGO_VS then
//    begin
//      Result := copy(NumeroConvenio,1,9) + '-' + StrRight(NumeroConvenio,3);
//    end
//    else
//    begin
//      Result := StrLeft(NumeroConvenio,3) + '-' + copy(NumeroConvenio,4,11) + '-' + StrRight(NumeroConvenio,3);
//    end;
end;
//END : SS_1147Q_NDR_20141202 --------------------------------------------------------------------------------
//FIN: TASK_009_ECA_20160506


function FormatearNumeroRut(RUT:String):String;
begin
    Result := StrLeft(RUT, Length(RUT) - 1)+ '-' + StrRight(RUT,1);
end;

function  ArmarUbicacionGeografica(Connection :TADOConnection; CodigoPais, CodigoRegion, CodigoComuna:AnsiString;CodigoCiudad:AnsiString='';DescripcionCiudad:AnsiString=''): AnsiString;
begin
    Result := trim(QueryGetValue(Connection, Format(
	  'SELECT dbo.ArmarUbicacionGeografica(''%s'',''%s'', ''%s'', ''%s'',''%s'')',
	  [CodigoPais,CodigoRegion,CodigoComuna,CodigoCiudad,DescripcionCiudad])));
end;

{-----------------------------------------------------------------------------
  Function Name: ObtenerDescripCalle
  Author:
  Date Created:  /  /
  Description:
  Parameters: Connection :TADOConnection;
              CodigoPais, CodigoRegion, CodigoComuna: AnsiString;
              CodigoCalle: Integer = 0;
              CalleDesnormalizada: AnsiString = ''
  Return Value: String

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function ObtenerDescripCalle(Connection :TADOConnection;
  CodigoPais, CodigoRegion, CodigoComuna: AnsiString; CodigoCalle: Integer = 0; CalleDesnormalizada: AnsiString = ''):String;
begin
    result:= trim(iif(CodigoCalle = 0, CalleDesnormalizada,
      QueryGetValue(Connection,format('SELECT Descripcion FROM MaestroCalles  WITH (NOLOCK) WHERE CodigoPais = ''%s'' AND CodigoRegion= ''%s'' AND CodigoComuna = ''%s'' AND CodigoCalle = %d',[CodigoPais,CodigoRegion,CodigoComuna,CodigoCalle]))));
end;

function  ArmarDomicilioSimple(Connection :TADOConnection; calle: AnsiString;
  numero, piso: integer; dpto, detalle: AnsiString): AnsiString;
var
    p:Integer;
begin
    p := Pos((''''), Calle);
    if p > 0 then Calle[p] := APOSTROFE;

    p := Pos((''''), detalle);
    if p > 0 then detalle[p] := APOSTROFE;

    Result := trim(QueryGetValue(Connection, Format(
	  'SELECT dbo.ArmarDomicilioSimple(''%s'', %d, %d, ''%s'',''%s'')',
	  [Calle, numero, piso, dpto, detalle])));
end;

function  ArmarDomicilioSimple(Connection :TADOConnection; calle: AnsiString; numero: AnsiString; piso: integer; dpto, detalle: AnsiString): AnsiString;
var
    p:Integer;
begin
    p := Pos((''''), Calle);
    if p > 0 then Calle[p] := APOSTROFE;

    p := Pos((''''), detalle);
    if p > 0 then detalle[p] := APOSTROFE;

    Result := trim(QueryGetValue(Connection, Format(
	  'SELECT dbo.ArmarDomicilioSimpleChar(''%s'', ''%s'', %d, ''%s'',''%s'')',
	  [Calle, numero, piso, dpto, detalle])));
end;

function ArmarComuna(Connection :TADOConnection; CodigoDomicilio: AnsiString): AnsiString;
begin
    Result := trim(QueryGetValue(Connection, format('SELECT dbo.ObtenerDescripcionComunaDomicilio(''%s'')',
                            	  [CodigoDomicilio])));
end;

function ArmarRegion(Connection :TADOConnection; CodigoDomicilio: AnsiString): AnsiString;
begin
	Result := trim(QueryGetValue(Connection, format('SELECT dbo.ObtenerDescripcionRegionDomicilio(''%s'')',
                            	  [CodigoDomicilio])));
end;

function ArmarCiudad(Connection :TADOConnection; CodigoDomicilio: AnsiString): AnsiString;
begin
    Result := trim(QueryGetValue(Connection, format('SELECT dbo.ObtenerDescripcionCiudadDomicilio(''%s'')',
                            	  [CodigoDomicilio])));
end;
{-----------------------------------------------------------------------------
  Function Name: ObtenerMensaje
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn :TADOConnection; CodigoMensaje: integer;
              var tituloMensaje, descripcion, comentario: AnsiString
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure ObtenerMensaje(Conn :TADOConnection; CodigoMensaje: integer; var tituloMensaje, descripcion, comentario: AnsiString);
Var
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection	:= Conn;
		Qry.SQL.Text	:= 'SELECT * FROM Mensajes  WITH (NOLOCK) Where CodigoMensaje = ' + IntToStr(CodigoMensaje);
		Qry.Open;
        tituloMensaje   := Qry.FieldByName('tituloMensaje').AsString;
        descripcion     := Qry.FieldByName('descripcion').AsString;
        comentario      := Qry.FieldByName('comentario').AsString;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

function  ArmarDomicilioCompleto(Connection :TADOConnection; calle: AnsiString; numero, piso: integer; dpto, detalle: AnsiString; ciudad, comuna, region: AnsiString): AnsiString;
var
    p:Integer;
begin
    p := Pos((''''), Calle);
    if p > 0 then Calle[p] := APOSTROFE;

    p := Pos((''''), detalle);
    if p > 0 then detalle[p] := APOSTROFE;

    Result := trim(QueryGetValue(Connection, Format(
	  'SELECT dbo.ArmarDomicilioCompleto(''%s'', %d, %d, ''%s'',''%s'',''%s'',''%s'',''%s'' )',
	  [Calle, numero, piso, dpto, detalle, ciudad, comuna, region ])));
end;

function  ArmarDomicilioCompleto(Connection :TADOConnection; calle: AnsiString; numero:AnsiString; piso: integer; dpto, detalle: AnsiString; ciudad, comuna, region: AnsiString): AnsiString;
var
    p:Integer;
begin
    p := Pos((''''), Calle);
    if p > 0 then Calle[p] := APOSTROFE;

    p := Pos((''''), detalle);
    if p > 0 then detalle[p] := APOSTROFE;

    Result := trim(QueryGetValue(Connection, Format(
	  'SELECT dbo.ArmarDomicilioCompletoChar(''%s'', ''%s'', %d, ''%s'',''%s'',''%s'',''%s'',''%s'' )',
	  [Calle, numero, piso, dpto, detalle, ciudad, comuna, region ])));
end;



function  ArmarDomicilioSimplePorCodigo(Connection :TADOConnection; codigoDomicilio: integer): AnsiString;
Var
	ObtenerDatosDomicilio: TADOStoredProc;
begin
	result := '';
	ObtenerDatosDomicilio 					:= TADOStoredProc.Create(nil);
	ObtenerDatosDomicilio.Connection		:= Connection;
	ObtenerDatosDomicilio.Name				:= 'ObtenerDatosDomicilio';
	ObtenerDatosDomicilio.ProcedureName	:= 'dbo.ObtenerDatosDomicilio';
	ObtenerDatosDomicilio.Parameters.CreateParameter('@CodigoDomicilio', ftInteger, pdInput, 0, CodigoDomicilio);
	ObtenerDatosDomicilio.Parameters.ParamByName('@CodigoDomicilio').Value := CodigoDomicilio;
	OpenTables([ObtenerDatosDomicilio]);
    result := ArmarDomicilioSimple(Connection,
                                  ObtenerDatosDomicilio.FieldByname('Calle').AsString,
                                  ObtenerDatosDomicilio.FieldbyName('numero').AsInteger,
                                  ObtenerDatosDomicilio.FieldbyName('piso').AsInteger,
                                  ObtenerDatosDomicilio.FieldbyName('dpto').AsString,
                                  ObtenerDatosDomicilio.FieldbyName('detalle').AsString);
	ObtenerDatosDomicilio.Close;
	ObtenerDatosDomicilio.Free;
end;

function  ArmarDomicilioCompletoPorCodigo(Conn :TADOConnection; CodigoPersona, CodigoDomicilio: integer): AnsiString;
begin
	result := '';
    with TADOStoredProc.Create(nil) do begin
        try
            Connection		:= Conn;
            ProcedureName	:= 'dbo.ObtenerDomiciliosPersona';
            Parameters.Refresh;
            Parameters.ParamByName('@CodigoPersona').Value := CodigoPersona;
            Parameters.ParamByName('@CodigoDomicilio').Value := CodigoDomicilio;

            open;
            result := FieldByName('DireccionCompleta').AsString;
        except
            on e:Exception do MsgBoxErr('Error al Obtener Domicilio Completo',e.Message,'Error',MB_ICONSTOP);
        end;
        Close;
        Free;
    end;
end;

function NoCargoDomicilio(calle, numero, comuna: AnsiString): boolean;
begin
    result := (trim(calle) = '') or (trim(numero) = '') or (trim(comuna) = '');
end;


{-----------------------------------------------------------------------------
  Function Name: ObtenerLargoVehiculo
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; marca, modelo, anio: integer
  Return Value: integer

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function ObtenerLargoVehiculo(Conn: TADOConnection; marca, modelo, anio: integer): integer;
var
   	Qry: TADOQuery;
begin
    Qry := TAdoQuery.Create(nil);
    try
        with Qry do
        begin
            Connection := Conn;
            SQL.Add(' SELECT Largo FROM Vehiculos  WITH (NOLOCK) ');
            SQL.Add(' WHERE Marca  = :xMarca ' );
            SQL.Add('   AND Modelo = :xModelo ' );
            SQL.Add('   AND Anio   = :xAnio ' );
            Parameters.ParamByName('xMarca').Value := marca;
            Parameters.ParamByName('xModelo').Value := Modelo;
            Parameters.ParamByName('xAnio').Value := Anio;
            Open;
            if not Eof then
                 result := FieldByName('Largo').AsInteger
            else result := 0;
		end;
    finally
        Qry.Close;
        Qry.Free;
    end;
end;

{
    Deja de tener sentido porque la tabla Modelos no existe mas, entonces no hay forma de saber
        segun la marca que categoria es. DCalani


function  BuscarDescripcionCategoriaVehiculo(Connection:TADOConnection;Marca, Modelo: integer  var Categoria: integer): AnsiString;
var
   	Qry: TADOStoredProc;
begin
    result := '';
    Qry := TAdoQuery.Create(nil);
    try
        with Qry do
        begin
            Connection := DMConnections.BaseCAC;
            SQL.Add(' SELECT CAT.Descripcion as Descripcion, CAT.Categoria as Categoria ' );
            SQL.Add(' FROM VehiculosModelos VM (nolock), ' );
            SQL.Add(' VehiculosTipos VT (nolock), ' );
            SQL.Add(' Categorias CAT (Nolock)' );
			SQL.Add(' WHERE VM.CodigoMarca = :xMarca ' );
            SQL.Add(' AND VM.CodigoModelo = :xModelo ' );
            SQL.Add(' AND VT.CodigoTipoVehiculo = VM.TipoVehiculo ' );
            SQL.Add(' AND CAT.Categoria = VT.Categoria ' );
            Parameters.ParamByName('xMarca').Value := marca;
            Parameters.ParamByName('xModelo').Value := Modelo;
            Open;
            if not Eof then begin
                 result := FieldByName('Descripcion').AsString;
                 categoria := FieldByName('Categoria').AsInteger;
            end
            else begin
                result := '';
                categoria := 0;
            end;
        end;
    finally
        Qry.Close;
        Qry.Free;
    end;
end;
}
procedure QuitarDebitoAutomaticoDecuenta(Connection:TADOConnection; cuenta: integer);
var
   	Qry: TADOQuery;
begin
    Qry := TAdoQuery.Create(nil);
    try
        with Qry do
        begin
            Connection := DMConnections.BaseCAC;
            SQL.Add(' UPDATE Cuentas  ' );
            SQL.Add(' SET CodigoDebitoAutomatico = :xCodigoDebitoAutomatico, ' );
            SQL.Add(' TipoPago = :xTipoPago ' );
            SQL.Add(' WHERE CodigoCuenta = :xCuenta ' );
            Parameters.ParamByName('xCodigoDebitoAutomatico').Value := NULL;
            Parameters.ParamByName('xTipoPago').Value := TP_POSPAGO_MANUAL;
            Parameters.ParamByName('xCuenta').Value := cuenta;
            ExecSQL;
        end;
    finally
        Qry.Close;
		Qry.Free;
    end;
end;

procedure MarcarItemComboVariant(var Combo: TVariantComboBox; Codigo: integer; TieneItemNinguno: boolean = true);
Var
    i: Integer; sigo: boolean;
begin
{******************************** function Header ******************************
function Name: MarcarItemComboVariant
Author       : dgonzalez
Date Created : 01/10/2003
Description  : Es para dejar seleccionado item del combo en base a un codigo
               recibido por parametro. Es util por ejemplo al moverse por
               una grilla, para mostrar el detalle del registro actual.
Parameters   : Connection :TADOConnection; TipoPago: AnsiString
               TieneItemNinguno es util para indicar que el combo tiene el primer
               item con una descripcion como "<ninguno>" o "(Sin especificar)".
               En ese caso si no encuentra el item o es null, devuelve ese.
Return Value : AnsiString
*******************************************************************************}
	i := 0;
    sigo := true;
    if (Combo.items.Count = 0) then exit;

    if TieneItemNinguno then begin
        //lo dejo asi para que quede en "ninguno" por si no encuentra
        Combo.ItemIndex := i;
    end;

    while (i < Combo.Items.Count) and (sigo) do
    begin
        if Codigo = Combo.Items[i].Value then begin
            Combo.ItemIndex := i;
            sigo := false;
        end
        else inc(i);
    end;
    if (sigo) and (not TieneItemNinguno) then begin
        //Si Sigo es verdadero es porque no encontro ninguno
        Combo.ItemIndex := -1;
    end;
end;

procedure MarcarItemComboVariant(var Combo: TVariantComboBox; Codigo: AnsiString; TieneItemNinguno: boolean = true); overload;
Var
    i: Integer; sigo: boolean;
begin
{******************************** function Header ******************************
function Name: MarcarItemComboVariant
Author       : dgonzalez
Date Created : 01/10/2003
Description  : Es para dejar seleccionado item del combo en base a un codigo
               recibido por parametro. Es util por ejemplo al moverse por
               una grilla, para mostrar el detalle del registro actual.
Parameters   : Connection :TADOConnection; TipoPago: AnsiString
               TieneItemNinguno es util para indicar que el combo tiene el primer
               item con una descripcion como "<ninguno>" o "(Sin especificar)".
               En ese caso si no encuentra el item o es null, devuelve ese.
*******************************************************************************}
	i := 0;
    sigo := true;
    if (Combo.items.Count = 0) then exit;

    if TieneItemNinguno then begin
        //lo dejo asi para que quede en "ninguno" por si no encuentra
        Combo.ItemIndex := i;
    end;

    while (i < Combo.Items.Count) and (sigo) do
    begin
        if Codigo = Combo.Items[i].Value then begin
            Combo.ItemIndex := i;
            sigo := false;
        end
        else inc(i);
    end;
    if (sigo) and (not TieneItemNinguno) then begin
        //Si Sigo es verdadero es porque no encontro ninguno
        Combo.ItemIndex := -1;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ObtenerCantidadPasesPatenteAnioActual
  Author:
  Date Created:  /  /
  Description:
  Parameters: Connection:TADOConnection;
              patente: AnsiString;
              anio: smallint
  Return Value: smallint

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function  ObtenerCantidadPasesPatenteAnioActual(Connection:TADOConnection; patente: AnsiString; anio: smallint): smallint;
resourcestring
    MSG_ERROR_CANTIDAD_PASES = 'No se pudo obtener la cantidad de pases en el a�o para la patente.';
    CAPTION = 'Error';
var
    Qry :TADOQuery;
begin
    Qry := TADOQuery.Create(nil);
    try
		Qry.Connection := Connection;
        Qry.CursorLocation :=clUseServer;
		Qry.CursorType := ctDynamic;
		Qry.SQL.Clear;
		Qry.SQL.Text := 'SELECT COUNT(*) as Cantidad ' +
                        '  FROM maestroDayPass  WITH (NOLOCK) ' +
                        ' WHERE Patente = :xPatente ' +
                        '   AND Year(FechaDesde) = :xAnio';
    	With Qry do begin
         	Parameters.ParamByName('xPatente').Value := patente;
			Parameters.ParamByName('xAnio').Value := anio;
            Active := true;
            Result := Fieldbyname('Cantidad').AsInteger;
            Active := false;
		end;
    except
    	On E: Exception do begin
            MsgBoxErr(MSG_ERROR_CANTIDAD_PASES, E.Message,CAPTION ,MB_ICONSTOP );
            result := 0;
        end;
    end;
    Qry.Free;
end;

function ObtenerDigitoVerificadorPatente(Connection:TADOConnection; Patente: AnsiString): AnsiString;
begin
    result := QueryGetValue(Connection,
        ' EXEC ObtenerDigitoVerificadorPatente''' + Trim(Patente) + '''');
end;

{-----------------------------------------------------------------------------
  Function Name: ObtenerCodigoDomicilioEntrega
  Author:
  Date Created:  /  /
  Description:
  Parameters: codigoPersona: integer
  Return Value: integer

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function  ObtenerCodigoDomicilioEntrega(codigoPersona: integer): integer;
begin
    result := QueryGetValueInt(DMConnections.BaseCAC,
                ' SELECT IsNull(CodigoDomicilioEntrega, 0) FROM Personas  WITH (NOLOCK) ' +
                ' WHERE CodigoPersona = ' + IntToStr(codigopersona));
end;

{-----------------------------------------------------------------------------
  Function Name: ObtenerCodigoDomicilioPersona
  Author:
  Date Created:  /  /
  Description:
  Parameters: codigoPersona, codigotipodomicilio: integer
  Return Value: integer

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function  ObtenerCodigoDomicilioPersona(codigoPersona, codigotipodomicilio: integer): integer;
begin
    Result :=  QueryGetValueInt(DMConnections.BaseCAC,
                ' SELECT TOP 1 CodigoDomicilio FROM Domicilios  WITH (NOLOCK) ' +
                '  WHERE CodigoPersona = ' + IntToStr(codigopersona) +
                '    AND CodigoTipoDomicilio = ' + IntToStr(codigotipodomicilio));
end;

{-----------------------------------------------------------------------------
  Function Name:
  Author:
  Date Created:  /  /
  Description:
  Parameters: codigopersona, codigomediocontacto, codigotipodomicilio: integer
  Return Value: string

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function  ObtenerValorMediosComunicacion(codigopersona, codigomediocontacto, codigotipodomicilio: integer): string;
begin
    result := QueryGetValue(DMConnections.BaseCAC,
                ' SELECT IsNull(Valor, '''') FROM MediosComunicacion  WITH (NOLOCK) ' +
                '  WHERE CodigoPersona = ' + IntToStr(codigopersona) +
                '    AND CodigoMedioContacto = ' + IntToStr(codigomediocontacto) +
                '    AND CodigoTipoDomicilio= ' + IntToStr(codigotipodomicilio));
end;
{-----------------------------------------------------------------------------
  Function Name: EsDomicilioEntrega
  Author:
  Date Created:  /  /
  Description:
  Parameters: codigopersona, codigodomicilio: integer
  Return Value: boolean

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function  EsDomicilioEntrega (codigopersona, codigodomicilio: integer): boolean;
var
    codigoDomicilioEntregaPersona,
    codigoDomicilioFacturacionCuenta,
    codigoDomicilioInfoComercialCuenta: integer;
begin
    codigoDomicilioEntregaPersona := QueryGetValueInt(DMConnections.BaseCAC,
                ' SELECT IsNull(CodigoDomicilioEntrega, 0) FROM Personas  WITH (NOLOCK) ' +
                '  WHERE CodigoPersona = ' + IntToStr(codigopersona));

    codigoDomicilioFacturacionCuenta := QueryGetValueInt(DMConnections.BaseCAC,
                ' SELECT ISNULL((IsNull(CodigoDomicilioFacturacion, 0) FROM Cuentas  WITH (NOLOCK) ' +
                '  WHERE CodigoPersona = ' + IntToStr(codigopersona) +
                '    AND CodigoDomicilioFacturacion = ' + IntToStr(codigodomicilio) + ')');
    codigoDomicilioInfoComercialCuenta := QueryGetValueInt(DMConnections.BaseCAC,
                ' SELECT ISNULL((IsNull(CodigoDomicilioInfoComercial, 0) FROM Personas  WITH (NOLOCK) ' +
                '  WHERE CodigoPersona = ' + IntToStr(codigopersona) + ')');
    result := (codigodomicilio = codigoDomicilioEntregaPersona)
           or (codigodomicilio = codigoDomicilioFacturacionCuenta)
           or (codigodomicilio = codigoDomicilioInfoComercialCuenta);
end;

{-----------------------------------------------------------------------------
  Function Name: ExisteCiudadComuna
  Author:
  Date Created:  /  /
  Description:
  Parameters: Pais, Region, Comuna, Ciudad: AnsiString
  Return Value: boolean

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function  ExisteCiudadComuna (Pais, Region, Comuna, Ciudad: AnsiString): boolean;
begin
    Result :=  QueryGetValueInt(DMConnections.BaseCAC,
                'SELECT 1 FROM CiudadesComunas  WITH (NOLOCK) ' +
                ' WHERE CodigoPais = ''' + Pais + '''' +
                '   AND CodigoRegion = ''' + Region + '''' +
				'   AND CodigoComuna = ''' + Comuna + '''' +
                '   AND CodigoCiudad = ''' + Ciudad + '''') = 1;
end;

procedure LimpiarRegistroDomicilio(var datos: TDatosDomicilio; tipoDomicilio: integer);
begin
    with datos do
    begin
        CodigoDomicilio := -1;
        IndiceDomicilio := -1;
        Descripcion := '';
        CodigoTipoDomicilio := tipoDomicilio;
        CodigoCalle := -1;
        CodigoTipoCalle := -1;
        NumeroCalle := -1;
        NumeroCalleSTR := '';
        Piso := -1;
        Dpto := '';
        Detalle := '';
        CodigoPostal := '';
        CodigoPais := '';
        CodigoRegion := '';
		CodigoComuna := '';
        CalleDesnormalizada := '';
        DomicilioEntrega := false;
        CodigoPaisRelacionadoUno := '';
        CodigoRegionRelacionadoUno := '';
        CodigoComunaRelacionadoUno := '';
        CodigoCalleRelacionadaUno := -1;
        NumeroCalleRelacionadaUno := '';
        CodigoPaisRelacionadoDos := '';
        CodigoRegionRelacionadoDos := '';
        CodigoComunaRelacionadoDos := '';
        CodigoCalleRelacionadaDos := -1;
        NumeroCalleRelacionadaDos := '';
        Activo := false;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: BuscarDescripcionTipoPago
  Author:
  Date Created:  /  /
  Description:
  Parameters: Connection :TADOConnection; TipoPago: AnsiString
  Return Value: AnsiString

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function BuscarDescripcionTipoPago(Connection :TADOConnection; TipoPago: AnsiString):AnsiString;
resourcestring
    MSG_DESCRIPCION_TIPO_PAGO = 'No se pudo obtener la descripcion del Tipo de Pago.';
var
    Qry_TipoPago :TADOQuery;
begin
    Qry_TipoPago := TADOQuery.Create(nil);
    try
		Qry_TipoPago.Connection := Connection;
		Qry_TipoPago.CursorLocation :=clUseServer;
		Qry_TipoPago.CursorType := ctDynamic;
		Qry_TipoPago.SQL.Clear;
		Qry_TipoPago.SQL.Text :=
	         ' SELECT TIPOPAGO, DESCRIPCION '+
	         ' FROM TIPOPAGO  WITH (NOLOCK) '+
	         ' WHERE TIPOPAGO.TIPOPAGO = :PAGO';
    	With Qry_TipoPago do begin
         	Parameters.ParamByName('PAGO').Value := TipoPago;
            Active := true;
            Result := Trim(Fieldbyname('Descripcion').AsString);
            Active := false;
		end;
    except
    	On E: Exception do begin
    	    Result := MSG_DESCRIPCION_TIPO_PAGO;
        end;
    end;
    Qry_TipoPago.Free;
end;

{-----------------------------------------------------------------------------
  Function Name: BuscarDescripcionTipoDebito
  Author:
  Date Created:  /  /
  Description:
  Parameters: Connection :TADOConnection; TipoDebito: AnsiString
  Return Value: AnsiString

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function BuscarDescripcionTipoDebito(Connection :TADOConnection; TipoDebito: AnsiString):AnsiString;
resourcestring
    MSG_DESCRIPCION_TIPO_DEBITO = 'No se pudo obtener la descripcion del Tipo de D�bito.';
var
    Qry_TipoDebito :TADOQuery;
begin
    Qry_TipoDebito := TADOQuery.Create(nil);
    try
        try
             Qry_TipoDebito.Connection := Connection;
             Qry_TipoDebito.CursorLocation :=clUseServer;
             Qry_TipoDebito.CursorType := ctDynamic;
             Qry_TipoDebito.SQL.Clear;
             Qry_TipoDebito.SQL.Text :=
             ' SELECT TIPODEBITO, DESCRIPCION '+
             ' FROM TIPODEBITO  WITH (NOLOCK) '+
             ' WHERE TIPODEBITO.TIPODEBITO = :DEBITO';
             with Qry_TipoDebito do begin
                parameters.ParamByName('DEBITO').Value := TipoDebito;
                Active := true;
                Result := Trim(Fieldbyname('Descripcion').AsString);
                Qry_TipoDebito.active := false;
            end;
        except
            On E: Exception do begin
                Result := MSG_DESCRIPCION_TIPO_DEBITO;
            end;
        end;
    finally
	    Qry_TipoDebito.Free;
    end;
end;


function ValorDanio(Conn: TADOConnection; EstadoConservacionTAG: integer): Integer;
begin
	Result := QueryGetValueInt(Conn, 'SELECT DBO.ValorDanio(' + Trim(inttostr(EstadoConservacionTAG)) + ')');
end;

function ValorSoporte(Conn: TADOConnection): Integer;
begin
	Result := QueryGetValueInt(Conn, 'SELECT DBO.ValorSoporte()');
end;

{-----------------------------------------------------------------------------
  Procedure: TfrmPagoVentanilla.CargarCanalesDePago
  Author:    ggomez
  Date:      07-Jun-2005
  Arguments: Combo: TVariantComboBox
  Result:    Boolean
  Description: Carga en el combo pasado como par�metro los Canales de Pago para
    los cuales tiene permiso el usuario logueado.
-----------------------------------------------------------------------------}
procedure CargarCanalesDePago(Conn: TADOConnection; Combo: TVariantComboBox; CodigoItemSeleccionar: Integer; CanalesRefinanciacion: Integer = -1);
var
    sp: TADOStoredProc;
    Indice: Integer;
begin
    Indice := -1;
    Combo.Items.Clear;
    sp := TADOStoredProc.Create(Nil);
    try
        sp.Connection       := Conn;
        sp.ProcedureName    := 'ObtenerCanalesPago';
        sp.Parameters.Refresh;
        sp.Parameters.ParamByName('@EsCanalRefinanciacion').Value := iif(CanalesRefinanciacion in [0,1], CanalesRefinanciacion, Null);
        sp.Open;
        while not sp.Eof do begin
            if (not sp.FieldByName('Funcion').IsNull) and (ExisteAcceso(Trim(sp.FieldByName('Funcion').Value))) then begin
                Combo.Items.Add(sp.FieldByName('Descripcion').Value, sp.FieldByName('CodigoTipoMedioPago').Value);

                if sp.FieldByName('CodigoTipoMedioPago').Value = CodigoItemSeleccionar then begin
                	Indice := Combo.Items.Count - 1;
                end;
            end;
            sp.Next;
        end; // while

        (* Si hay items en el combo y existe el que se indica como par�metro,
        seleccionarlo. *)
        if (Combo.Items.Count > 0) and (Indice <> -1) then begin
            Combo.ItemIndex := Indice;
        end else begin
            (* NO existe el que se indica en el par�metro, seleccionar el primero. *)
            if (Combo.Items.Count > 0) then Combo.ItemIndex := 0;
        end;

    finally
        sp.Close;
        sp.Free;
    end; // finally
end;

{-----------------------------------------------------------------------------
  Procedure: CargarFormasPago
  Author:    ggomez
  Date:      07-Jun-2005
  Arguments: Combo: TVariantComboBox; CodigoCanalDePago: Byte
  Result:    None
  Description: Carga en el combo pasado como par�metro las Formas de Pago
    asociadas al Canal de Pago par�metro.
-----------------------------------------------------------------------------}
procedure CargarFormasPago(Conn: TADOConnection; Combo: TVariantComboBox; CodigoCanalDePago: Byte);
var
    sp: TADOStoredProc;
begin
    Combo.Items.Clear;
    sp := TADOStoredProc.Create(Nil);
    try
        sp.Connection       := Conn;
        sp.ProcedureName    := 'ObtenerFormasPagoPorCanal';
        sp.Parameters.Refresh;
        sp.Parameters.ParamByName('@CodigoCanalDePago').Value := CodigoCanalDePago;
        sp.Open;
        while not sp.Eof do begin
            Combo.Items.Add(sp.FieldByName('Descripcion').Value, sp.FieldByName('CodigoFormaPago').Value);
            sp.Next;
        end; // while

        (* Si hay items en el combo, seleccionar el primero. *)
        if Combo.Items.Count > 0 then Combo.Value := Combo.Items[0].Value;

    finally
        sp.Close;
        sp.Free;
    end; // finally
end;

{******************************** function Header ******************************
function Name: CargarImpresorasFiscales
Author : mlopez
Date Created : 20/07/2006
Description : Carga en un combo todas las impresoras fiscales dadas de alta en
              el sistema
Parameters : Conn: TADOConnection; Combo: TVariantComboBox; CodigoCanalDePago: Byte
Return Value : None
*******************************************************************************}
procedure CargarImpresorasFiscales(Conn: TADOConnection; Combo: TVariantComboBox; CodigoImpresoraFiscal: Integer);
var
    sp: TADOStoredProc;
begin
    Combo.Items.Clear;
    sp := TADOStoredProc.Create(Nil);
    try
        sp.Connection := Conn;
        sp.ProcedureName := 'ObtenerImpresorasFiscales';
        sp.Parameters.Refresh;
        sp.Open;
        while not sp.Eof do begin
            Combo.Items.Add(sp.FieldByName('Descripcion').Value, sp.FieldByName('CodigoImpresoraFiscal').Value);
            sp.Next;
        end;
        if (Combo.Items.Count > 0) and (CodigoImpresoraFiscal <> 0) then
            Combo.Value := CodigoImpresoraFiscal;
    finally
        sp.Close;
        sp.Free;
    end;
end;

function  ObtenerNombreDiaSemana(const aFecha: TDateTime): AnsiString;
const Dia: array[1..7] of AnsiString = ('Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'S�bado');
begin
	result := Dia[DayOfWeek(aFecha)];
end;

function ObtenerDescripTipoVehiculo(Connection: TADOConnection; CodigoTipoVehiculo:Integer; var DescripTipo:String;var DescripCategoria:String;var Categoria:String):boolean;
var
	sp:TADOStoredProc;
begin
    sp:=TADOStoredProc.Create(nil);
    try
        try
            DescripTipo:='';
            DescripCategoria:='';
            Categoria:='';

            sp.Connection:=Connection;
            sp.ProcedureName:='ObtenerDescripTipoVehiculo';
            with sp.Parameters do begin
            	Refresh;
                ParamByName('@CodigoTipoVehiculo').Value:=CodigoTipoVehiculo;
            end;
            sp.open;

            if sp.RecordCount>0 then begin
                DescripTipo:=sp.FieldByName('TIPO').AsString;
                DescripCategoria:=sp.FieldByName('Desrip_Catergoria').AsString;
                Categoria:=sp.FieldByName('Categoria').AsString;
            end;
            sp.close;

            result:=True
        except
            result:=False;
        end;
    finally
        sp.Destroy;
    end;
end;

function ObtenerDescripcionDebito(Connection: TADOConnection;
  TipoDebito: AnsiString; CodigoEntidad: Integer; CuentaDebito: AnsiString): AnsiString;
begin
	Result := QueryGetValue(Connection, Format(
	  'SELECT dbo.ObtenerDescripcionDebito(''%s'', %d, ''%s'')',
	  [TipoDebito, CodigoEntidad, CuentaDebito]));
end;

function  ExisteCliente(Connection :TADOConnection; CodigoCliente: Integer): Boolean;
begin
    Result := (UpperCase(QueryGetValue(Connection,'SELECT dbo.ExisteCliente(' + IntToStr(CodigoCliente) + ')')) = 'TRUE');
end;


Var
	AntWndProc: function(Wnd: HWnd; Msg: UInt; wParam: WParam; lParam: LParam): LResult; stdcall;

function ClientProc(Wnd: HWnd; Msg: UInt; wParam: WParam; lParam: LParam): LResult; stdcall;
begin
	case Msg of
		WM_ERASEBKGND:
			begin
				InvalidateRect(Application.MainForm.Handle, nil, False);
				Result := 1;
			end;
		else
			Result := AntWndProc(Wnd, Msg, wParam, lParam);
	end;
end;

procedure DrawMDIBackground(Graphic: TGraphic);
const
	MARGEN_DERECHO = 20;
	MARGEN_INFERIOR = 20;
Var
	DC: HDC;
	S: TSize;
	Proc: Pointer;
	ImgRect: TRect;
	Canvas: TCanvas;
	Rgn1, Rgn2: HRgn;
begin
	if not Assigned(Application.MainForm) or not Assigned(Graphic) then Exit;
	S := GetFormClientSize(Application.MainForm);
	DC := GetDC(Application.MainForm.ClientHandle);
	Proc := Pointer(GetWindowLong(Application.MainForm.ClientHandle, GWL_WNDPROC));
	if Proc <> @ClientProc then begin
       	AntWndProc := Proc;
		SetWindowLong(Application.MainForm.ClientHandle, GWL_WNDPROC, LongInt(@ClientProc));
	end;
	Canvas := TCanvas.Create;
	try
		ImgRect := Rect(S.cx - Graphic.Width - MARGEN_DERECHO, S.cy - Graphic.Height - MARGEN_INFERIOR, S.cx - MARGEN_INFERIOR, S.cy - MARGEN_DERECHO);
		Rgn1 := CreateRectRgn(0, 0, S.cx, S.cy);
		Rgn2 := CreateRectRgn(ImgRect.Left, ImgRect.Top, ImgRect.Right, ImgRect.Bottom);
		CombineRgn(Rgn1, Rgn1, Rgn2, RGN_DIFF);
		Canvas.Handle := DC;
		SelectClipRgn(DC, Rgn1);
		Canvas.Brush.Color := RGB(255, 255, 255);
		Canvas.FillRect(Rect(0, 0, S.cx, S.cy));
		SelectClipRgn(DC, 0);
		Canvas.StretchDraw(ImgRect, Graphic);
		DeleteObject(Rgn1);
		DeleteObject(Rgn2);
	finally
		Canvas.Handle := 0;
		Canvas.Free;
	end;
	ReleaseDC(Application.MainForm.ClientHandle, DC);
end;

{-----------------------------------------------------------------------------
  Procedure: RegistryKeyExists
  Author:    ggomez
  Date:      30-Jun-2005
  Arguments: RootKey: HKey; Key: AnsiString
  Result:    Boolean
  Description: Retorna True si existe una entrada en el registro de Windows.
-----------------------------------------------------------------------------}
function RegistryKeyExists(RootKey: HKey; Key: AnsiString): Boolean;
var
    Reg: TRegistry;
begin
    Reg := TRegistry.Create;
    try
        Reg.RootKey := RootKey;
        (* Si la entrada no existe, retornar False. *)
        Result := Reg.KeyExists(Key);
    finally
        Reg.Free;
    end;
end;

{-----------------------------------------------------------------------------
  Procedure: RegistryValueExists
  Author:    ggomez
  Date:      30-Jun-2005
  Arguments: RootKey: HKey; Key, ValueName: AnsiString
  Result:    Boolean
  Description: Retorna True si existe un Valor en una entrada en el registro
    de Windows.
-----------------------------------------------------------------------------}
function RegistryValueExists(RootKey: HKey; Key, ValueName: AnsiString): Boolean;
var
    Reg: TRegistry;
begin
    Reg := TRegistry.Create;
    try
        Reg.RootKey := RootKey;
        (* Si la entrada no existe, retornar False. *)
        Result := Reg.KeyExists(Key);

        if Result then begin
            if Reg.OpenKey(Key, False) then begin
                Result := Reg.ValueExists(ValueName);
            end;
        end;
    finally
        Reg.Free;
    end;
end;

{-----------------------------------------------------------------------------
  Procedure: GetRegistryBooleanValue
  Author:    ggomez
  Date:      30-Jun-2005
  Arguments: RootKey: HKey; Key, ValueName: AnsiString
  Result:    Boolean
  Description: Retorna el Valor de una entrada en el registro de Windows que es
    de tipo boolean.
    Si la entrada o el valor no existen, retorna False.
-----------------------------------------------------------------------------}
function GetRegistryBooleanValue(RootKey: HKey; Key, ValueName: AnsiString): Boolean;
var
    Reg: TRegistry;
begin
    Reg := TRegistry.Create;
    try
        Reg.RootKey := RootKey;
        (* Si la entrada no existe, retornar False. *)
        if not Reg.KeyExists(Key) then begin
            Result := False;
            Exit;
        end;

        if Reg.OpenKey(Key, False) then begin
            (* Si el value no existe, retornar False. *)
            if not Reg.ValueExists(ValueName) then begin
                Result := False;
                Exit;
            end;
        end;

        Result := Reg.ReadBool(ValueName);

    finally
        Reg.Free;
    end;
end;

{-----------------------------------------------------------------------------
  Procedure: SetRegistryBooleanValue
  Author:    ggomez
  Date:      30-Jun-2005
  Arguments: RootKey: HKey; Key, ValueName: AnsiString; Value: Boolean
  Result:    Boolean
  Description: Guarda en el registro de Windows un valor boolean.
    Lanza una excepci�n si no se pudo guardar el dato.
-----------------------------------------------------------------------------}
procedure SetRegistryBooleanValue(RootKey: HKey; Key, ValueName: AnsiString; Value: Boolean);
var
    Reg: TRegistry;
begin
    Reg := TRegistry.Create;
    try
        Reg.RootKey := RootKey;

        (* Si la entrada no existe, crearla. *)
        if not Reg.KeyExists(Key) then begin
            Reg.CreateKey(Key);
        end;

        (* Escribir el valor de la entrada. *)
        if Reg.OpenKey(Key, False) then begin
            Reg.WriteBool(ValueName, Value);
            Reg.CloseKey;
        end;

    finally
        Reg.Free;
    end;
end;

{-----------------------------------------------------------------------------
  Procedure: GetUserSettingShowBrowserBar
  Author:    ggomez
  Date:      28-Jun-2005
  Arguments: None
  Result:    Booolean
  Description: Retorna True, si entre las opciones seteadas por el usuario, para
    la aplicaci�n en ejecuci�n, est� que se debe mostrar la barra de navegaci�n.
    En otro caso, retorna False.
-----------------------------------------------------------------------------}
function GetUserSettingShowBrowserBar: Boolean;
var
    Key: AnsiString;
begin
    Key := '\Software\DMI\' +
            ChangeFileExt(ExtractFileName(Application.ExeName), EmptyStr) +
            '\UserSettings';

    if (not RegistryKeyExists(HKEY_CURRENT_USER, Key))
            or (not RegistryValueExists(HKEY_CURRENT_USER, Key, 'ShowBrowserBar')) then begin
        (* Crear la entrada. *)
        if not SetUserSettingShowBrowserBar(False) then begin
            (* No se pudo crear la entrada, retornar False. *)
            Result := False;
            Exit;
        end;
    end;

    Result := GetRegistryBooleanValue(HKEY_CURRENT_USER, Key, 'ShowBrowserBar');
end;

{-----------------------------------------------------------------------------
  Procedure: SetUserSettingShowBrowserBar
  Author:    ggomez
  Date:      28-Jun-2005
  Arguments: Valor: Boolean
  Result:    Booolean
  Description: Setea el valor de la opci�n de mostrar la barra de navegaci�n.
-----------------------------------------------------------------------------}
function SetUserSettingShowBrowserBar(Valor: Boolean): Boolean;
resourcestring
    MSG_ERROR_CREAR_ENTRADA = 'Ha ocurrido un error al guardar las opciones de configuraci�n del usuario.';
    MSG_CAPTION             = 'Opciones de configuraci�n.';
var
    Key: AnsiString;
begin
    Key := '\Software\DMI\' +
            ChangeFileExt(ExtractFileName(Application.ExeName), EmptyStr) +
            '\UserSettings';

    (* Crear la entrada. *)
    try
        SetRegistryBooleanValue(HKEY_CURRENT_USER, Key, 'ShowBrowserBar', Valor);
        Result := True;
    except
        on E: Exception do begin
            Result := False;
            MsgBoxErr(MSG_ERROR_CREAR_ENTRADA, E.Message, MSG_CAPTION, MB_ICONWARNING);
            Exit;
        end;
    end; // except
end;

{-----------------------------------------------------------------------------
  Procedure: GetUserSettingShowIconBar
  Author:    ggomez
  Date:      28-Jun-2005
  Arguments: None
  Result:    Booolean
  Description: Retorna True, si entre las opciones seteadas por el usuario, para
    la aplicaci�n en ejecuci�n, est� que se debe mostrar la barra de �conos.
    En otro caso, retorna False.
-----------------------------------------------------------------------------}
function GetUserSettingShowIconBar: Boolean;
var
    Key: AnsiString;
begin
    Key := '\Software\DMI\' +
            ChangeFileExt(ExtractFileName(Application.ExeName), EmptyStr) +
            '\UserSettings';

    if (not RegistryKeyExists(HKEY_CURRENT_USER, Key))
            or (not RegistryValueExists(HKEY_CURRENT_USER, Key, 'ShowIconBar')) then begin
        (* Crear la entrada. *)
        if not SetUserSettingShowIconBar(False) then begin
            (* No se pudo crear la entrada, retornar False. *)
            Result := False;
            Exit;
        end;
    end;

    Result := GetRegistryBooleanValue(HKEY_CURRENT_USER, Key, 'ShowIconBar');
end;

{-----------------------------------------------------------------------------
  Procedure: SetUserSettingShowIconBar
  Author:    ggomez
  Date:      28-Jun-2005
  Arguments: Valor: Boolean
  Result:    Booolean
  Description: Setea el valor de la opci�n de mostrar la barra de �conos.
-----------------------------------------------------------------------------}
function SetUserSettingShowIconBar(Valor: Boolean): Boolean;
resourcestring
    MSG_ERROR_CREAR_ENTRADA = 'Ha ocurrido un error al guardar las opciones de configuraci�n del usuario.';
    MSG_CAPTION             = 'Opciones de configuraci�n.';
var
    Key: AnsiString;
begin
    Key := '\Software\DMI\' +
            ChangeFileExt(ExtractFileName(Application.ExeName), EmptyStr) +
            '\UserSettings';

    (* Crear la entrada. *)
    try
        SetRegistryBooleanValue(HKEY_CURRENT_USER, Key, 'ShowIconBar', Valor);
        Result := True;
    except
        on E: Exception do begin
            Result := False;
            MsgBoxErr(MSG_ERROR_CREAR_ENTRADA, E.Message, MSG_CAPTION, MB_ICONWARNING);
            Exit;
        end;
    end; // except
end;

{-----------------------------------------------------------------------------
  Procedure: GetUserSettingShowIconBar
  Author:    ggomez
  Date:      30-Jun-2005
  Arguments: None
  Result:    Booolean
  Description: Retorna True, si entre las opciones seteadas por el usuario, para
    la aplicaci�n en ejecuci�n, est� que se debe mostrar la barra de ot Links.
    En otro caso, retorna False.
-----------------------------------------------------------------------------}
function GetUserSettingShowHotLinksBar: Boolean;
var
    Key: AnsiString;
begin
    Key := '\Software\DMI\' +
            ChangeFileExt(ExtractFileName(Application.ExeName), EmptyStr) +
            '\UserSettings';

    if (not RegistryKeyExists(HKEY_CURRENT_USER, Key))
            or (not RegistryValueExists(HKEY_CURRENT_USER, Key, 'ShowHotLinksBar')) then begin
        (* Crear la entrada. *)
        if not SetUserSettingShowHotLinksBar(False) then begin
            (* No se pudo crear la entrada, retornar False. *)
            Result := False;
            Exit;
        end;
    end;

    Result := GetRegistryBooleanValue(HKEY_CURRENT_USER, Key, 'ShowHotLinksBar');
end;

{-----------------------------------------------------------------------------
  Procedure: SetUserSettingShowIconBar
  Author:    ggomez
  Date:      28-Jun-2005
  Arguments: Valor: Boolean
  Result:    Booolean
  Description: Setea el valor de la opci�n de mostrar la barra de Hot Links.
-----------------------------------------------------------------------------}
function SetUserSettingShowHotLinksBar(Valor: Boolean): Boolean;
resourcestring
    MSG_ERROR_CREAR_ENTRADA = 'Ha ocurrido un error al guardar las opciones de configuraci�n del usuario.';
    MSG_CAPTION             = 'Opciones de configuraci�n.';
var
    Key: AnsiString;
begin
    Key := '\Software\DMI\' +
            ChangeFileExt(ExtractFileName(Application.ExeName), EmptyStr) +
            '\UserSettings';

    (* Crear la entrada. *)
    try
        SetRegistryBooleanValue(HKEY_CURRENT_USER, Key, 'ShowHotLinksBar', Valor);
        Result := True;
    except
        on E: Exception do begin
            Result := False;
            MsgBoxErr(MSG_ERROR_CREAR_ENTRADA, E.Message, MSG_CAPTION, MB_ICONWARNING);
            Exit;
        end;
    end; // except
end;

function ConectarRecursoImagenes(Password,
  Usuario: PChar; Path: AnsiString; var DescriError: AnsiString): Boolean;
resourcestring
    MSG_ERROR_CONECCION = 'Error al conectar a %s : %s';
Var
	NetResource: TNetResource;
begin
	// S�lo tomamos hasta el nombre del recurso
	Path := '\\' + ParseParamByNumber(Path, 3, '\') + '\' +
	  ParseParamByNumber(Path, 4, '\');
	// Ahora nos conectamos
	FillChar(NetResource, SizeOf(NetResource), 0);
	NetResource.dwType       := RESOURCETYPE_DISK;
	NetResource.lpLocalName  := nil;
	NetResource.lpRemoteName := PChar(Path);
	NetResource.lpProvider   := nil;
	if WNetAddConnection2(NetResource, Password, Usuario, 0) = NO_ERROR then begin
		Result := True;
	end else begin
		// No pudimos conectar al recurso remoto
		Result := False;
		DescriError := Format(MSG_ERROR_CONECCION, [trim(Path), trim(SysErrorMessage(GetLastError))]);
	end;
end;

function CrearRegistroOperaciones(Conn: TADOConnection; CodigoSistema, CodigoModulo,
  CodigoAccion, CodigoSeveridad: Integer; PuestoDeTrabajo, CodigoUsuario, Evento: AnsiString;
  IDRegistroOperacionPadre: Integer; Reservado1, Reservado2, Reservado3: Variant;
  DescriError: String): Integer;
Var
	StoredProc: TADOStoredProc;
begin
    // INICIO : 20160414 MGO
    if Trim(CodigoUsuario) = EmptyStr then Exit;
    // FIN : 20160414 MGO
    // INICIO : 20160307 MGO
	Conn := DMConnections.BaseBO_Master;
	// FIN : 20160307 MGO
    StoredProc := TADOStoredProc.Create(nil);
    try
        try
            with StoredProc do begin
                Connection := Conn;
                ProcedureName := 'CrearRegistroOperaciones';
                Parameters.Refresh;
                if (CodigoSistema <> 0) then Parameters.ParamByName('@CodigoSistema').Value := CodigoSistema
                else Parameters.ParamByName('@CodigoSistema').Value := NULL;
                if (CodigoModulo >= 0) then Parameters.ParamByName('@CodigoModulo').Value := CodigoModulo
                else Parameters.ParamByName('@CodigoModulo').Value := NULL;
                if (CodigoAccion >= 0) then Parameters.ParamByName('@CodigoAccion').Value := CodigoAccion
                else Parameters.ParamByName('@CodigoAccion').Value := NULL;
                if (CodigoSeveridad >= 0) then Parameters.ParamByName('@CodigoSeveridad').Value := CodigoSeveridad
                else Parameters.ParamByName('@CodigoSeveridad').Value := NULL;
                if (Trim(PuestoDeTrabajo) <> '') then Parameters.ParamByName('@PuestoDeTrabajo').Value := Trim(PuestoDeTrabajo)
                else Parameters.ParamByName('@PuestoDeTrabajo').Value := NULL;
                if (Trim(CodigoUsuario) <> '') then Parameters.ParamByName('@CodigoUsuario').Value := Trim(CodigoUsuario)
                else Parameters.ParamByName('@CodigoUsuario').Value := NULL;
                if (Trim(Evento) <> '') then Parameters.ParamByName('@Evento').Value := Trim(Evento)
                else Parameters.ParamByName('@Evento').Value := NULL;
                if (IDRegistroOperacionPadre >= 0) then Parameters.ParamByName('@IDRegistroOperacionPadre').Value := IDRegistroOperacionPadre
                else Parameters.ParamByName('@IDRegistroOperacionPadre').Value := NULL;
                Parameters.ParamByName('@Reservado1').Value := Reservado1;
                Parameters.ParamByName('@Reservado2').Value := Reservado2;
                Parameters.ParamByName('@Reservado3').Value := Reservado3;
                Parameters.ParamByName('@IDRegistroOperacion').Value := NULL;
                ExecProc;
                Result := iif(Parameters.ParamByName('@IDRegistroOperacion').Value = NULL, 0, Parameters.ParamByName('@IDRegistroOperacion').Value);
            end;
        except
            on E: Exception do begin
                Result := -1;
                DescriError := e.Message;
            end;
        end;
    finally
	    FreeAndNil(StoredProc);
    end;
end;

function ActualizarRegistroOperacion(Conn: TADOConnection; CodigoSistema,
  IDRegistroOperacion, CodigoSeveridad: Integer; OperacionExitosa: Boolean;
  evento: AnsiString; Reservado1, Reservado2, Reservado3: Variant;
  var DescriError: AnsiString): Boolean;
var updateRegistroOperacion: TADOStoredProc;
begin
	result := true;
	try
		updateRegistroOperacion := TADOStoredProc.Create(nil);
		with updateRegistroOperacion do begin
			Connection := Conn;
			ProcedureName := 'ActualizarRegistroOperacion';
			Parameters.CreateParameter('@CodigoSistema', ftInteger, pdInput, 0, CodigoSistema);
			Parameters.CreateParameter('@IDRegistroOperacion', ftInteger, pdInput, 0, IDRegistroOperacion);
			Parameters.CreateParameter('@CodigoSeveridad', ftInteger, pdInput, 0, CodigoSeveridad);
			Parameters.CreateParameter('@OperacionExitosa', ftBoolean, pdInput, 0, OperacionExitosa);
			Parameters.CreateParameter('@Event', ftString, pdInput, 2147483647, evento);
			Parameters.CreateParameter('@Reservado1', ftInteger, pdInput, 0, Reservado1);
			Parameters.CreateParameter('@Reservado2', ftInteger, pdInput, 0, Reservado2);
			Parameters.CreateParameter('@Reservado3', ftString, pdInput, 255, Reservado3);
			ExecProc;
		end;
	except
		on E: Exception do begin
			Result := False;
            DescriError := E.message;
		end;
	end;
end;


//procedure DibujarViaje(ImagenViaje: TBitMap; PuntosCobro: array of Boolean);
//var PuntoCobroAnterior , i: Integer;
//    colorAReemplazar: TColor;
//begin
//
//    ImagenViaje.Canvas.Brush.Color := RGB(0, 147, 221);
//    colorAReemplazar := ImagenViaje.Canvas.Pixels[79, 107];
//    PuntoCobroAnterior := -1;
//    for i:= 0 to 14 do begin
//        with ImagenViaje.canvas do begin
//            if PuntosCobro[i] then begin
//                case i+1  of
//                    1 : begin
//                            // Pintamos desde el 1 hacia atras
//							FloodFill(54, 102, ColorAReemplazar, fsSurface);
//                            // Pintamos desde el 1 hacia delante
//                            FloodFill(79, 107, ColorAReemplazar, fsSurface);
//                            FloodFill(95, 111, ColorAReemplazar, fsSurface);
//                        end;
//                    4 : begin
//                            // Pintamos union del 1 al 4
//                            if PuntoCobroAnterior = 1 then
//                                FloodFill(111, 112, Pixels[111, 112], fsSurface);
//                            // Pintamos hacia el 4
//                            FloodFill(119, 114, ColorAReemplazar, fsSurface);
//                            FloodFill(146, 118, ColorAReemplazar, fsSurface);
//                            // Pintamos desde el 4
//                            FloodFill(173, 122, ColorAReemplazar, fsSurface);
//                        end;
//                    5 : begin
//                            // Pintamos union del 4 al 5
//                            if PuntoCobroAnterior = 4 then
//                                FloodFill(180, 123, Pixels[180, 123], fsSurface);
//                            FloodFill(188, 124, ColorAReemplazar, fsSurface);
//                            FloodFill(198, 125, ColorAReemplazar, fsSurface);
//							FloodFill(231, 131, ColorAReemplazar, fsSurface);
//                        end;
//                    7 : begin    // Pintamos la union del 5 al 7
//                            if PuntoCobroAnterior = 5 then
//                                FloodFill(247, 132, Pixels[247, 132], fsSurface);
//                            FloodFill(255, 134, ColorAReemplazar, fsSurface);
//                            FloodFill(313, 143, ColorAReemplazar, fsSurface);
//                        end;
//                    9 : begin
//                            // Pintamos la union del 7 al 9
//                            if PuntoCobroAnterior = 7 then begin
//                                // Cruce de calle
//                                FloodFill(336, 145, Pixels[336, 145], fsSurface);
//                                FloodFill(343, 146, Pixels[343, 146], fsSurface);
//                                FloodFill(350, 144, Pixels[350, 144], fsSurface);
//                            end;
//                            FloodFill(364, 142, ColorAReemplazar, fsSurface);
//                            FloodFill(398, 137, ColorAReemplazar, fsSurface);
//                            FloodFill(398, 137, ColorAReemplazar, fsSurface);
//                            FloodFill(435, 143, ColorAReemplazar, fsSurface);
//                        end;
//				   11 : Begin
//                            if PuntoCobroAnterior = 9 then
//                                FloodFill(450, 145, Pixels[450, 145], fsSurface);
//                            // Pintamos la pasada por el 11
//                            FloodFill(458, 146, ColorAReemplazar, fsSurface);
//                            FloodFill(473, 150, ColorAReemplazar, fsSurface);
//                            FloodFill(481, 149, ColorAReemplazar, fsSurface);
//                            FloodFill(512, 155, ColorAReemplazar, fsSurface);
//                        end;
//                    13 : begin
//                            // Pintamos la union del 7 al 13
//                            if PuntoCobroAnterior = 7 then begin
//                                FloodFill(335, 144, Pixels[335, 144], fsSurface);
//                            end;
//                            FloodFill(336, 145, ColorAReemplazar, fsSurface);
//                        end;
//                   15 : Begin
//                            // Pintamos la union del 13 al 15
//                            if PuntoCobroAnterior = 13 then
//                                FloodFill(396, 175, Pixels[396, 175], fsSurface);
//                            // Pintamos la pasada por 15
//							FloodFill(410, 178, ColorAReemplazar, fsSurface);
//                            FloodFill(430, 180, ColorAReemplazar, fsSurface);
//                            FloodFill(462, 186, ColorAReemplazar, fsSurface);
//                        end;
//                   14 : begin
//                            if PuntoCobroAnterior = 12 then
//                                FloodFill(412, 163, Pixels[412, 163], fsSurface);
//                            // Pintamos del 14 hacia afuera
//                            FloodFill(484, 174, ColorAReemplazar, fsSurface);
//                            // Pimtamos del 14 hacia el 12
//                            FloodFill(443, 168, ColorAReemplazar, fsSurface);
//                            FloodFill(420, 165, ColorAReemplazar, fsSurface);
//                        end;
//                   12 : begin
//                            if PuntoCObroAnterior = 6 then begin
//                                // Cruce de calle
//                                FloodFill(350, 134, Pixels[350, 134], fsSurface);
//                                // Cruce de Mano
//                                FloodFill(350, 138, Pixels[350, 138], fsSurface);
//                                // Empalme
//                                FloodFill(350, 144, Pixels[348, 144], fsSurface);
//							end;
//                            // Pintamos del 14 al 12
//                            FloodFill(398, 161, ColorAReemplazar, fsSurface);
//                            FloodFill(350, 150, ColorAReemplazar, fsSurface);
//                            FloodFill(365, 156, ColorAReemplazar, fsSurface);
//                        end;
//                   10 : begin
//                            if PuntoCobroAnterior = 8 then
//                                FloodFill(459, 135, Pixels[459, 135], fsSurface);
//                            // Pintamos la entrada al 10
//
//                            FloodFill(530, 144, ColorAReemplazar, fsSurface);
//                            // Pintamos la salida al 10
//                            FloodFill(498, 139, ColorAReemplazar, fsSurface);
//                            FloodFill(470, 136, ColorAReemplazar, fsSurface);
//                        end;
//                    8 : begin
//                            // Pintamos del 10 al 8
//                            if PuntoCobroAnterior = 6 then
//                                FloodFill(350, 134, Pixels[350, 134], fsSurface);
//                            FloodFill(448, 133, ColorAReemplazar, fsSurface);
//							FloodFill(368, 132, ColorAReemplazar, fsSurface);
//                            FloodFill(419, 129, ColorAReemplazar, fsSurface);
//                        end;
//                    6 : begin
//                            // Pintamos la union del 6 al 3
//                            if PuntoCobroAnterior = 3 then
//                                FloodFill(242, 118, Pixels[242, 118], fsSurface);
//                            // Pintamos la entrada al 6
//                            FloodFill(328, 130, ColorAReemplazar, fsSurface);
//                            // Pintamos el resto del camino hasta el 6 segun de donde se inicia el viaje
//                            if (PuntoCobroAnterior = 12) or (PuntoCobroAnterior = 14) then begin
//                            end else if (PuntoCobroAnterior = 10) or (PuntoCobroAnterior = 8) then begin
//                                // Cruce de calle
//                                ;
//                            end;
//                            // Pintamos la salida del 6
//                            FloodFill(290, 125, ColorAReemplazar, fsSurface);
//                            FloodFill(259, 120, ColorAReemplazar, fsSurface);
//                        end;
//                    3 : begin
//                            // Pintamos la pasada por el 3
//							FloodFill(220, 115, ColorAReemplazar, fsSurface);
//                            FloodFill(188, 109, ColorAReemplazar, fsSurface);
//                            FloodFill(161, 106, ColorAReemplazar, fsSurface);
//                        end;
//                    2 : begin
//                            // Pintamos la passda por el 2 (La union no hace falta por las caracteristicas del dibujo)
//                            FloodFill(135, 101, ColorAReemplazar, fsSurface);
//                            FloodFill(104, 99, ColorAReemplazar, fsSurface);
//                        end;
//                end;
//                PuntoCobroAnterior := i+1;
//            end;
//        end;
//    end;
//end;

{-----------------------------------------------------------------------------
  Function Name: ObtenerNombreArchivoFactura
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; TipoCOmprobante: AnsiString; NumeroComprobante: Double
  Return Value: AnsiString

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function ObtenerNombreArchivoFactura(Conn: TADOConnection; TipoCOmprobante: AnsiString; NumeroComprobante: Double): AnsiString;
var
	DirectorioFacturas: ansiString;
begin
    DirectorioFacturas	:= GoodDir(Trim(QueryGetValue(Conn, 'SELECT DirFacturas FROM Parametros WITH (NOLOCK) ')));
	Result := DirectorioFacturas + Trim(TipoCOmprobante) + PadL(FloatToStr(NumeroComprobante), 18, '0') + '.rpt';
end;

{-----------------------------------------------------------------------------
  Function Name: CargarTitulosFAQ
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo: TComboBox; CodigoTitulo: Integer = 0
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarTitulosFAQ(Conn: TADOConnection; Combo: TComboBox; CodigoTitulo: Integer = 0);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'SELECT * FROM FAQTitulos WITH (NOLOCK) ';
		Qry.Open;
		Combo.Clear;
		i := -1;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString +
			  Space(200) + Qry.FieldByName('CodigoTitulo').AsString);
			if Qry.FieldByName('CodigoTitulo').AsInteger = CodigoTitulo then
			  i := Combo.Items.Count - 1;
			Qry.Next;
		end;
		if (Combo.Items.Count > 0) and (i = -1) then i := 0;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name:CargarTiposPregunta
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection;
              Combo: TComboBox;
              CodigoTipoPregunta: integer = 0
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarTiposPregunta(Conn: TADOConnection; Combo: TComboBox; CodigoTipoPregunta: integer = 0);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'SELECT * FROM TiposPregunta WITH (NOLOCK) ';
		Qry.Open;
		Combo.Clear;
		i := -1;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString +
			  Space(200) + Qry.FieldByName('CodigoTipoPregunta').AsString);
			if Qry.FieldByName('CodigoTipoPregunta').AsInteger = CodigoTipoPregunta then
			  i := Combo.Items.Count - 1;
			Qry.Next;
		end;
		if (Combo.Items.Count > 0) and (i = -1) then i := 0;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarTiposDatoPregunta
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo: TComboBox;
              CodigoPregunta: Integer;
              CodigoDatoPregunta: integer = 0
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarTiposDatoPregunta(Conn: TADOConnection; Combo: TComboBox; CodigoPregunta: Integer; CodigoDatoPregunta: integer = 0);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text :=
          format('SELECT DISTINCT TiposDatoPregunta.* ' +
                 '  FROM TiposDatoPregunta  WITH (NOLOCK) INNER JOIN TiposDatoPreguntaXTiposPregunta  WITH (NOLOCK) ON ' +
                 '       TiposDatoPreguntaXTiposPregunta.codigoTipoDato = TiposDatoPregunta.CodigoTipoDato ' +
                 '       AND TiposDatoPreguntaXTiposPregunta.CodigoTipoPregunta = %d', [CodigoPregunta]) ;
		Qry.Open;
		Combo.Clear;
		i := -1;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString +
			  Space(200) + Qry.FieldByName('CodigoTipoDato').AsString);
			if Qry.FieldByName('CodigoTipoDato').AsInteger = CodigoDatoPregunta then
			  i := Combo.Items.Count - 1;
			Qry.Next;
		end;
		if (Combo.Items.Count > 0) and (i = -1) then i := 0;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarFuenteComunicacion
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo: TComboBox;
              CodigoFuenteComunicacion: integer = 0;
              TieneItemNinguno: boolean = False
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarFuenteComunicacion(Conn: TADOConnection; Combo: TComboBox; CodigoFuenteComunicacion: integer = 0; TieneItemNinguno: boolean = False);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'SELECT * FROM FuentesComunicacion  WITH (NOLOCK) Order by Descripcion';
		Qry.Open;
		Combo.Clear;
		i := -1;
        if TieneItemNinguno then begin
			Combo.Items.Add(SIN_ESPECIFICAR + Space(200) + PadL('0', 10)+PadL('0', 10));
            i := 0;
		end;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString +
			  Space(200) + PadL(qry.FieldByName('CodigoFuenteComunicacion').AsString,10) + PadL(Qry.FieldByName('CodigoCanalComunicacion').AsString, 10));
			if Qry.FieldByName('CodigoFuenteComunicacion').AsInteger = CodigoFuenteComunicacion then begin
            	i := Combo.Items.Count - 1;
            end;
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

procedure CargarBancosxCuentas(Conn: TADOConnection; Combo: TComboBox; TipoCuenta:Integer=-1; CodigoBanco:Integer=-1; TieneItemSeleccionar: boolean = false);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'EXEC ObtenerBancosxCuentas '+inttostr(TipoCuenta);
		Qry.Open;
		Combo.Clear;
		i := -1;

        if TieneItemSeleccionar then begin
			Combo.Items.Add(SELECCIONAR + Space(200) + '');
            i:= i + 1;
        end;


		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString +
			  Space(200) + PadL(qry.FieldByName('CodigoBanco').AsString,30));

			if Qry.FieldByName('CodigoBanco').AsInteger = CodigoBanco then begin
            	i := Combo.Items.Count - 1;
            end;

			Qry.Next;
		end;
		Combo.ItemIndex := i;
        if Combo.ItemIndex < 0 then Combo.ItemIndex := 0;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarGuiaDespachoOrigen
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection;
              Combo: TComboBox;
              CodigoAlmacenDestino: integer = -1;
              Ninguno: Boolean = True;
              CodigoTransportista: integer = -1;
              CodigoAlmacenOrigen: integer = -1
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarGuiaDespachoOrigen(Conn: TADOConnection; Combo: TComboBox; CodigoAlmacenDestino: integer = -1; Ninguno: Boolean = True; CodigoTransportista: integer = -1; CodigoAlmacenOrigen: integer = -1);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;

		Qry.SQL.Text := 'SELECT * FROM GuiasDespacho  WITH (NOLOCK) WHERE CodigoTransportista = ' + inttostr(CodigoTransportista);
        if (CodigoAlmacenDestino <> -1) and (CodigoAlmacenOrigen <> -1) then
            Qry.SQL.Text := Qry.SQL.Text +  'AND ((Estado = ''E'') AND (CodigoAlmacenDestino = ' + inttostr(CodigoAlmacenDestino) + ')  OR (CodigoAlmacenOrigen = ' + inttostr(CodigoAlmacenOrigen) + ') AND (Estado = ''O'') OR (Estado = ''E'') AND (CodigoAlmacenOrigen = ' + inttostr(CodigoAlmacenOrigen) + '))'
        else
            if CodigoAlmacenDestino <> -1 then Qry.SQL.Text := Qry.SQL.Text + ' AND (CodigoAlmacenDestino = ' + inttostr(CodigoAlmacenDestino)
            else Qry.SQL.Text := Qry.SQL.Text + ' AND CodigoAlmacenOrigen = ' + inttostr(CodigoAlmacenOrigen);


(*
		Qry.SQL.Text := 'SELECT * FROM GuiasDespacho (nolock) WHERE (Estado = ''E''' + ' OR Estado = ''O'')' + ' AND CodigoTransportista = ' + inttostr(CodigoTransportista);
        if (CodigoAlmacenDestino <> -1) and (CodigoAlmacenOrigen <> -1) then
            Qry.SQL.Text := Qry.SQL.Text + ' AND (CodigoAlmacenDestino = ' + inttostr(CodigoAlmacenDestino) + ' OR CodigoAlmacenOrigen = ' + inttostr(CodigoAlmacenOrigen) + ')'
        else
            if CodigoAlmacenDestino <> -1 then Qry.SQL.Text := Qry.SQL.Text + ' AND (CodigoAlmacenDestino = ' + inttostr(CodigoAlmacenDestino)
            else Qry.SQL.Text := Qry.SQL.Text + ' AND CodigoAlmacenOrigen = ' + inttostr(CodigoAlmacenOrigen);
*)
		Qry.Open;
		Combo.Clear;
		i := -1;
        if Ninguno then begin
        	Combo.Items.Add(SELECCIONAR +
			  Space(200) + '0');
            i := 0;
		end;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('NumeroGuiaDespacho').AsString +
			  Space(200) + Qry.FieldByName('CodigoGuiaDespacho').AsString);
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

procedure CargarCombioSituacionTag(Conn: TADOConnection; Combo: TComboBox; SituacionesCAC : Boolean = True);
begin
    with TADOStoredProc.Create(nil) do begin
        try
            Combo.Items.Clear;
            Combo.Items.Add(SELECCIONAR + Space(100) + '-1');

            Connection := Conn;
            ProcedureName := 'ObtenerSituacionTag';
            Parameters.Refresh;
            Parameters.ParamByName('SoloCAC').Value := 1;
            Open;
            while not eof do begin
                Combo.Items.Add(FieldByName('Descripcion').AsString + Space(100) + FieldByName('CodigoEstadoSituacionTAG').AsString);
                Next;
            end;
            Close;
        except
            on e: Exception do MsgBoxErr(MSG_ERROR_EJECUTAR_SP,e.Message,'Error',MB_ICONSTOP );
        end;
        Combo.ItemIndex := 0;
        Free;
    end;
end;

{-----------------------------------------------------------------------------
  Procedure: CargarMotivosTransitoNoFacturable
  Author:    ggomez
  Date:      04-May-2005
  Arguments: Conn: TADOConnection;
    Combo: TComboBox;
    SoloExternos: Boolean = True. Para indicar si s�lo cargar los motivos No
    Internos.
  Result:    None
  Description: Carga en Combo los Motivos de Tr�nsitos No Facturables.
-----------------------------------------------------------------------------}
procedure CargarMotivosTransitoNoFacturable(Conn: TADOConnection;
    Combo: TVariantComboBox);
begin
    with TADOStoredProc.Create(nil) do begin
        try
            Combo.Items.Clear;
            Combo.Items.Add(SELECCIONAR, -1);
            try
                Connection := Conn;
                ProcedureName := 'ObtenerMotivosTransitoNoFacturable';
                Open;
                while not Eof do begin
                    Combo.Items.Add(FieldByName('Descripcion').AsString,
                        FieldByName('CodigoMotivoTransitoNoFacturable').AsInteger);
                    Next;
                end;
                Close;
            except
                on e: Exception do MsgBoxErr(MSG_ERROR_EJECUTAR_SP, E.Message, 'Error', MB_ICONSTOP);
            end;
            Combo.ItemIndex := 0;
        finally
            Free;
        end; // finally
    end; //with
end;


{-----------------------------------------------------------------------------
  Function Name: CargarGuiaDespacho
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo: TComboBox;
              CodigoAlmacenDestino: integer = -1;
              Ninguno: Boolean = True;
              CodigoTransportista: integer = -1;
              CodigoAlmacenOrigen: integer = -1
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarGuiaDespacho(Conn: TADOConnection; Combo: TComboBox; CodigoAlmacenDestino: integer = -1; Ninguno: Boolean = True; CodigoTransportista: integer = -1; CodigoAlmacenOrigen: integer = -1);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'SELECT * FROM GuiasDespacho  WITH (NOLOCK) WHERE Estado = ''E''' + ' AND CodigoTransportista = ' + inttostr(CodigoTransportista);
        if (CodigoAlmacenDestino <> -1) and (CodigoAlmacenOrigen <> -1) then
            Qry.SQL.Text := Qry.SQL.Text + ' AND (CodigoAlmacenDestino = ' + inttostr(CodigoAlmacenDestino) + ' OR CodigoAlmacenOrigen = ' + inttostr(CodigoAlmacenOrigen) + ')'
        else
            if CodigoAlmacenDestino <> -1 then Qry.SQL.Text := Qry.SQL.Text + ' AND (CodigoAlmacenDestino = ' + inttostr(CodigoAlmacenDestino)
            else Qry.SQL.Text := Qry.SQL.Text + ' AND CodigoAlmacenOrigen = ' + inttostr(CodigoAlmacenOrigen);

		Qry.Open;
		Combo.Clear;
		i := -1;
        if Ninguno then begin
        	Combo.Items.Add(SELECCIONAR +
			  Space(200) + '0');
            i := 0;
		end;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('NumeroGuiaDespacho').AsString +
			  Space(200) + Qry.FieldByName('CodigoGuiaDespacho').AsString);
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;


{-----------------------------------------------------------------------------
  Function Name: CargarMotivosCancelacion
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection;
              Combo: TVariantComboBox;
              Ninguno: Boolean = True;
              CodigoMotivoCancelacion: integer = 0
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarMotivosCancelacion(Conn: TADOConnection; Combo: TVariantComboBox; Ninguno: Boolean = True; CodigoMotivoCancelacion: integer = 0);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'SELECT * FROM ResultadosImportacion  WITH (NOLOCK) WHERE Tipo = ''C'' Order By Detalle';
		Qry.Open;
		Combo.Clear;
		i := -1;
        if Ninguno then begin
        	Combo.Items.Add('(Sin especificar)', 0);
            i := 0;
		end;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Detalle').AsString,
              Istr(Qry.FieldByName('CodigoResultado').AsInteger));
			if (Qry.FieldByName('CodigoResultado').AsInteger = CodigoMotivoCancelacion) then begin
            	i := Combo.Items.Count - 1;
            end;
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

procedure CargarTiposTarjetasCredito(Conn: TADOConnection; Combo: TComboBox;
    CodigoTipoTarjetaCredito:Integer=-1; TieneItemSeleccionar: boolean = false);
Var
	i: Integer;
	Qry: TADOStoredProc;
begin
	Qry := TADOStoredProc.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.ProcedureName:='ObtenerTiposTarjetaCredito';
        qry.parameters.Refresh;
        Qry.Parameters.ParamByName('@CodigoTipoTarjetaCredito').Value:=NULL;
        qry.Open;
		Combo.Clear;
		i := -1;

        if TieneItemSeleccionar then begin
			Combo.Items.Add(SELECCIONAR + Space(200) + '');
            i:= i + 1;
        end;

		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString +
			  Space(200) + PadL(qry.FieldByName('CodigoTipoTarjetaCredito').AsString,30));

			if Qry.FieldByName('CodigoTipoTarjetaCredito').AsInteger = CodigoTipoTarjetaCredito then begin
            	i := Combo.Items.Count - 1;
            end;

			Qry.Next;
		end;

		Combo.ItemIndex := i;
        if Combo.ItemIndex < 0 then Combo.ItemIndex := 0;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

procedure CargarTiposTarjetasCredito(Conn: TADOConnection; Combo: TVariantComboBox;
    CodigoTipoTarjetaCredito: Integer = -1; TieneItemSeleccionar: Boolean = False); overload;
var
	i: Integer;
    sp: TADOStoredProc;
begin
    sp := TADOStoredProc.Create(Nil);
    try
        sp.Connection := Conn;
        sp.ProcedureName := 'ObtenerTiposTarjetaCredito';
        sp.parameters.Refresh;
        sp.Parameters.ParamByName('@CodigoTipoTarjetaCredito').Value := NULL;
        sp.Open;
		i := -1;

        if TieneItemSeleccionar then begin
			Combo.Items.Add(SELECCIONAR, 0);
            i:= i + 1;
        end;

        while not sp.Eof do begin
            Combo.Items.Add(sp.FieldByName('Descripcion').AsString, sp.FieldByName('CodigoTipoTarjetaCredito').AsInteger);

			if sp.FieldByName('CodigoTipoTarjetaCredito').AsInteger = CodigoTipoTarjetaCredito then begin
            	i := Combo.Items.Count - 1;
            end;

            sp.Next;
        end; // while

		Combo.ItemIndex := i;
        if Combo.ItemIndex < 0 then Combo.ItemIndex := 0;
    finally
        sp.Close;
        sp.Free;
    end; // finally

end;

{******************************** Procedure Header *****************************
Procedure Name: CargarTiposTarjetasCreditoVentanilla
Author : pdominguez
Date Created : 11/06/2009
Parameters : Conn: TADOConnection; Combo: TVariantComboBox;
    CodigoTipoTarjetaCredito: Integer = -1; TieneItemSeleccionar: Boolean = False;
    SoloTarjetasPagoVentanilla: Boolean = False
Description : SS 809
        - Se copia del procedimiento ya existente para devolver solo las tarjetas
        admitidas en el canal de pago "Ventanilla".
*******************************************************************************}
procedure CargarTiposTarjetasCreditoVentanilla(Conn: TADOConnection; Combo: TVariantComboBox;
    CodigoTipoTarjetaCredito: Integer = -1; TieneItemSeleccionar: Boolean = False; SoloTarjetasPagoVentanilla: Boolean = False);
var
	i: Integer;
    sp: TADOStoredProc;
begin
    sp := TADOStoredProc.Create(Nil);
    try
        sp.Connection := Conn;
        sp.ProcedureName := 'ObtenerTiposTarjetaCredito';
        sp.parameters.Refresh;
        if SoloTarjetasPagoVentanilla then
            sp.Parameters.ParamByName('@SoloTarjetasPagoVentanilla').Value := 1;
        sp.Open;
		i := -1;

        if TieneItemSeleccionar then begin
			Combo.Items.Add(SELECCIONAR, 0);
            i:= i + 1;
        end;

        while not sp.Eof do begin
            Combo.Items.Add(sp.FieldByName('Descripcion').AsString, sp.FieldByName('CodigoTipoTarjetaCredito').AsInteger);

			if sp.FieldByName('CodigoTipoTarjetaCredito').AsInteger = CodigoTipoTarjetaCredito then begin
            	i := Combo.Items.Count - 1;
            end;

            sp.Next;
        end; // while

		Combo.ItemIndex := i;
        if Combo.ItemIndex < 0 then Combo.ItemIndex := 0;
    finally
        sp.Close;
        sp.Free;
    end; // finally

end;

{******************************** Procedure Header *****************************
Procedure Name: CargarBancosPagoVentanilla
Author : pdominguez
Date Created : 10/06/2009
Parameters : Conn: TADOConnection; Combo: TVariantComboBox; CodigoBanco: Integer = -1; TieneItemSeleccionar: Boolean = False
Description : SS 809
        - Carga en el combo pasado como par�metro los c�digos y descripciones de
        los Bancos habilitados para el pago en ventanilla.
*******************************************************************************}
procedure CargarBancosPagoVentanilla(Conn: TADOConnection; Combo: TVariantComboBox;
    CodigoBanco: Integer = -1; TipoMedioPago: Integer = 0; TieneItemSeleccionar: Boolean = False);
var
	i: Integer;
    sp: TADOStoredProc;
begin
    sp := TADOStoredProc.Create(Nil);
    try
        sp.Connection := Conn;
        sp.ProcedureName := 'ObtenerBancos';
        sp.parameters.Refresh;
        sp.Parameters.ParamByName('@CodigoBanco').Value := NULL;
        if TipoMedioPago = 0 then
            sp.Parameters.ParamByName('@TipoMedioPago').Value := NULL
        else sp.Parameters.ParamByName('@TipoMedioPago').Value := TipoMedioPago;
        sp.Parameters.ParamByName('@FormaPago').Value := NULL;
        sp.Open;
		i := -1;

        if TieneItemSeleccionar then begin
			Combo.Items.Add(SELECCIONAR, 0);
            i:= i + 1;
        end;

        while not sp.Eof do begin
            Combo.Items.Add(sp.FieldByName('Descripcion').AsString, sp.FieldByName('CodigoBanco').AsInteger);

			if sp.FieldByName('CodigoBanco').AsInteger = CodigoBanco then begin
            	i := Combo.Items.Count - 1;
            end;

            sp.Next;
        end; // while

		Combo.ItemIndex := i;
        if Combo.ItemIndex < 0 then Combo.ItemIndex := 0;
    finally
        sp.Close;
        sp.Free;
    end; // finally

end;

{-----------------------------------------------------------------------------
  Function Name: ObtenerDescripcionPuntoEntrega
  Author:
  Date Created:  /  /
  Description:
  Parameters: conn: TADOConnection; CodigoPuntoEntrega : Integer
  Return Value: String

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function ObtenerDescripcionPuntoEntrega(conn: TADOConnection; CodigoPuntoEntrega : Integer) : String;
begin
    Result := Trim(QueryGetValue(conn, format('SELECT Descripcion FROM PuntosEntrega  WITH (NOLOCK) WHERE CodigoPuntoEntrega = %d',[CodigoPuntoEntrega])));
end;

function TagEnCliente(conn: TADOConnection; CodigoPersona: integer; SerialNumber:DWord; ConvenioaExcluir: integer = -1): boolean;
var
    sp:TADOStoredProc;
begin
    sp:=TADOStoredProc.create(nil);
    try
		sp.Connection := Conn;
		sp.ProcedureName := 'TagEnCliente';
        with sp.Parameters do begin
        	Refresh;
            ParamByName('@CodigoCliente').Value:=CodigoPersona;
            ParamByName('@ContractSerialNumber').Value:=SerialNumber;
            ParamByName('@CodigoConvenioExcluir').Value:=iif(ConvenioaExcluir=-1, NULL, ConvenioaExcluir);
        end;
		sp.Open;
        result:= (sp.fieldByName('Resultado').AsInteger = 1)
    except
        result := False;
        sp.Close;
        sp.Destroy;
    end;
end;

function ObtenerCodigoConceptoPago(Conn: TADOConnection; CodigoCanalPago: Integer): Integer;
var
    sp:TADOStoredProc;
begin
    Result := -1;
    sp := TADOStoredProc.Create(Nil);
    try
		sp.Connection := Conn;
		sp.ProcedureName := 'ObtenerCanalPago';
      	sp.Parameters.Refresh;
        sp.Parameters.ParamByName('@CodigoCanalPago').Value := CodigoCanalPago;
		sp.Open;
        if not sp.FieldByName('CodigoConceptoPago').IsNull then begin
            Result:= sp.FieldByName('CodigoConceptoPago').AsInteger;
        end;
    finally
        sp.Close;
        sp.Free;
    end;
end;

function RegistrarLogAuditoriaAcciones(Conn: TADOConnection; CodigoSistema: Integer; Pantalla, Accion, Usuario, EstacionTrabajo, Observacion: String; CodigoInfraccion: Integer): Boolean;
var
    sp:TADOStoredProc;
begin
    Result := False;
    sp := TADOStoredProc.Create(Nil);
    try
        try
            sp.Connection := Conn;
            sp.ProcedureName := 'AgregarLogAuditoriaAcciones';
            sp.Parameters.Refresh;
            sp.Parameters.ParamByName('@CodigoSistema').Value := CodigoSistema;
            sp.Parameters.ParamByName('@Pantalla').Value := Pantalla;
            sp.Parameters.ParamByName('@Accion').Value := Accion;
            sp.Parameters.ParamByName('@Usuario').Value := Usuario;
            sp.Parameters.ParamByName('@EstacionTrabajo').Value := EstacionTrabajo;
            sp.Parameters.ParamByName('@Observacion').Value := Observacion;
            sp.Parameters.ParamByName('@CodigoInfraccion').Value := CodigoInfraccion;
            sp.ExecProc;

            if sp.Parameters.ParamByName('@RETURN_VALUE').Value = 0 then Result := True;
        except
            on e: exception do begin
                Result := False;
            end;
        end;
    finally
        sp.Free;
    end;
end;

function ObtenerEmisoresTarjetasCredito(Conn: TADOConnection; CodigoEmisorTarjetaCredito:Integer):String;
var
    sp:TADOStoredProc;
begin
    sp:=TADOStoredProc.create(nil);
    try
		sp.Connection := Conn;
		sp.ProcedureName := 'ObtenerEmisoresTarjetasCredito';
        with sp.Parameters do begin
        	Refresh;
            ParamByName('@CodigoEmisorTarjetaCredito').Value:=CodigoEmisorTarjetaCredito;
        end;
		sp.Open;
        result:='';
        if sp.RecordCount>0 then result:=trim(sp.fieldByName('Descripcion').AsString);
    except
        sp.Close;
        sp.Destroy;
    end;
end;

procedure CargarEmisoresTarjetasCredito(Conn: TADOConnection; Combo: TComboBox; CodigoEmisorTarjetaCredito:Integer=-1; TieneItemSeleccionar: boolean = false);
Var
	i: Integer;
	Qry: TADOStoredProc;
begin
	Qry := TADOStoredProc.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.ProcedureName := 'ObtenerEmisoresTarjetasCredito';
        qry.Parameters.Refresh;
        with qry.Parameters do begin
            ParamByName('@CodigoEmisorTarjetaCredito').Value:=null;
        end;
		Qry.Open;
		Combo.Clear;
		i := -1;

        if TieneItemSeleccionar then begin
			Combo.Items.Add(SELECCIONAR + Space(200) + '');
            i:= i + 1;
        end;

		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString +
			  Space(200) + PadL(qry.FieldByName('CodigoEmisorTarjetaCredito').AsString,30));

			if Qry.FieldByName('CodigoEmisorTarjetaCredito').AsInteger = CodigoEmisorTarjetaCredito then begin
            	i := Combo.Items.Count - 1;
            end;

			Qry.Next;
		end;

		Combo.ItemIndex := i;
        if Combo.ItemIndex < 0 then Combo.ItemIndex := 0;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

procedure CargarEmisoresTarjetasCredito(Conn: TADOConnection; Combo: TVariantComboBox; CodigoEmisorTarjetaCredito: Integer = -1; TieneItemSeleccionar: Boolean = False); overload;
Var
	i: Integer;
	sp: TADOStoredProc;
begin
	sp := TADOStoredProc.Create(Nil);
	try
		sp.Connection := Conn;
		sp.ProcedureName := 'ObtenerEmisoresTarjetasCredito';
        sp.Parameters.Refresh;
        sp.Parameters.ParamByName('@CodigoEmisorTarjetaCredito').Value := Null;
		sp.Open;
		Combo.Clear;
		i := -1;

        if TieneItemSeleccionar then begin
			Combo.Items.Add(SELECCIONAR, 0);
            Inc(i);
        end;

		while not sp.Eof do begin
			Combo.Items.Add(sp.FieldByName('Descripcion').AsString, sp.FieldByName('CodigoEmisorTarjetaCredito').AsInteger);

			if sp.FieldByName('CodigoEmisorTarjetaCredito').AsInteger = CodigoEmisorTarjetaCredito then begin
            	i := Combo.Items.Count - 1;
            end;

			sp.Next;
		end;

		Combo.ItemIndex := i;
        if Combo.ItemIndex < 0 then Combo.ItemIndex := 0;
	finally
		sp.Close;
		sp.Free;
	end;
end;


{-----------------------------------------------------------------------------
  Function Name: CargarTiposCuentas
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection;
              Combo: TComboBox;
              TipoCuenta:Integer=-1;
              TieneItemSeleccionar: boolean = false
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarTiposCuentas(Conn: TADOConnection; Combo: TComboBox; TipoCuenta:Integer=-1; TieneItemSeleccionar: boolean = false);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'SELECT * FROM TiposCuentasBancarias  WITH (NOLOCK) order by Descripcion';
		Qry.Open;
		Combo.Clear;
		i := -1;

        if TieneItemSeleccionar then begin
			Combo.Items.Add(SELECCIONAR + Space(200) + '');
            i:= i + 1;
        end;


		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString +
			  Space(200) + PadL(qry.FieldByName('CodigoTipoCuentaBancaria').AsString,30));

			if Qry.FieldByName('CodigoTipoCuentaBancaria').AsInteger = TipoCuenta then begin
            	i := Combo.Items.Count - 1;
            end;

			Qry.Next;
		end;
		Combo.ItemIndex := i;
        if Combo.ItemIndex < 0 then Combo.ItemIndex := 0;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarFormularios
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Combo: TComboBox
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarFormularios(Conn: TADOConnection; Combo: TComboBox);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'Select  distinct Formulario,Indice, DescripForm  from componentessistemas  WITH (NOLOCK) order by DescripForm';
		Qry.Open;
		Combo.Clear;
		i := -1;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('DescripForm').AsString +
			  Space(600) + PadL(qry.FieldByName('Formulario').AsString,30)+PadL(qry.FieldByName('Indice').AsString,3));
			Qry.Next;
		end;
		Combo.ItemIndex := i;
        if Combo.ItemIndex < 0 then Combo.ItemIndex := 0;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;


{-----------------------------------------------------------------------------
  Function Name: CargarFuentesOrigenContacto
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection;
              Combo: TComboBox;
              CodigoFuenteOrigenContacto: integer = 0
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarFuentesOrigenContacto(Conn: TADOConnection; Combo: TComboBox; CodigoFuenteOrigenContacto: integer = 0);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'SELECT * FROM FuentesOrigenContacto  WITH (NOLOCK) Order by OrdenMostrar, Descripcion';
		Qry.Open;
		Combo.Clear;
		i := -1;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString +
			  Space(200) + PadL(qry.FieldByName('CodigoFuenteOrigenContacto').AsString,10) + PadL(Qry.FieldByName('CodigoFuenteOrigenContacto').AsString, 10));
			if Qry.FieldByName('CodigoFuenteOrigenContacto').AsInteger = CodigoFuenteOrigenContacto then begin
            	i := Combo.Items.Count - 1;
            end;
			Qry.Next;
		end;
		Combo.ItemIndex := i;
        if Combo.ItemIndex < 0 then Combo.ItemIndex := 0;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;




procedure CargarTiposFuenteSolicitud(Combo: TVariantComboBox; TieneItemNinguno: Boolean = False; Default: Char = 'W');
resourcestring
    TF_DESC_WEB           = 'Web';
    TF_DESC_CALL_CENTER   = 'Tel�fono';
    TF_DESC_CUPON         = 'Cupon';
begin
	Combo.Clear;
    if TieneItemNinguno then begin
        Combo.Items.Add(SIN_ESPECIFICAR, '0') ;
    end;
	Combo.Items.Add(TF_DESC_WEB, TF_WEB);
	Combo.Items.Add(TF_DESC_CALL_CENTER, TF_CALL_CENTER);
    Combo.Items.Add(TF_DESC_CUPON, TF_CUPON);
    case Default of
	    TF_WEB: Combo.Itemindex := 0;
	    TF_CALL_CENTER: Combo.Itemindex := 1;
        TF_CUPON: Combo.Itemindex := 2;
        else if TieneItemNinguno then Combo.Itemindex := Combo.Itemindex + 1;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarFuentesSolicitud
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection;
              Combo: TComboBox;
              CodigoFuenteSolicitud:Integer = 0;
              TieneItemNinguno: boolean = false;
              Default: boolean = false;
              DescartarTipoFuente: AnsiString = ''
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarFuentesSolicitud(Conn: TADOConnection; Combo: TComboBox; CodigoFuenteSolicitud:Integer = 0; TieneItemNinguno: boolean = false; Default: boolean = false; DescartarTipoFuente: AnsiString = '');
Var
	i: Integer;
	Qry: TADOQuery;
    s: AnsiString;
begin
	Qry := TAdoQuery.Create(nil);
	try
        s := '';
		Qry.Connection := Conn;
        s := 'SELECT * FROM FuentesSolicitud  WITH (NOLOCK) WHERE Activo = 1';
        //esto se puede agregar en el futuro para mostrar todas las
        //fuentes de solicitud excepto las que estan relacionadas con al WEB.
        if DescartarTipoFuente <> '' then
            s := s + Format(' AND TipoFuente <> ''%s' + '''',[DescartarTipoFuente]);
        s := s + ' order by Descripcion';
        Qry.SQL.Text := s;
		Qry.Open;
		Combo.Clear;
		i := -1;
        if TieneItemNinguno then begin
			Combo.Items.Add(Copy(SIN_ESPECIFICAR + Space(200), 1, 200) + PadL('0', 10, ' '));
            i := 0;
        end;
		While not Qry.Eof do begin
			Combo.Items.Add(Copy(Qry.FieldByName('Descripcion').AsString +
			  Space(200), 1, 200) + PadL(Trim(Qry.FieldByName('CodigoFuenteSolicitud').AsString), 10, ' '));
			if Qry.FieldByName('CodigoFuenteSolicitud').AsInteger = CodigoFuenteSolicitud then
			  i := Combo.Items.Count - 1;
			if Qry.FieldByName('TipoFuenteDefault').AsBoolean and Default then
			  i := Combo.Items.Count - 1;
			Qry.Next;
		end;
        if (Combo.Items.Count > 0) and (i = -1) then i := 0;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name:CargarEstadosConvenio
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection;
              Combo: TComboBox;
              CodigoEstadoConvenio:Integer = 0;
              TieneItemNinguno: boolean = false
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarEstadosConvenio(Conn: TADOConnection; Combo: TComboBox; CodigoEstadoConvenio:Integer = 0; TieneItemNinguno: boolean = false);
Var
	i: Integer;
	Qry: TADOQuery;
    s: AnsiString;
begin
	Qry := TAdoQuery.Create(nil);
	try
        s := '';
		Qry.Connection := Conn;
        s := 'SELECT * FROM EstadosConvenios WITH (NOLOCK) ';
        Qry.SQL.Text := s;
		Qry.Open;
		Combo.Clear;
		i := -1;
        if TieneItemNinguno then begin
			Combo.Items.Add(SIN_ESPECIFICAR + Space(200) + PadL('0', 10, ' '));
            i := 0;
        end;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString +
			  Space(200) + PadL(Trim(Qry.FieldByName('CodigoEstadoConvenio').AsString), 10, ' '));
			if Qry.FieldByName('CodigoEstadoConvenio').AsInteger = CodigoEstadoConvenio then
			  i := Combo.Items.Count - 1;
			Qry.Next;
		end;
        if (Combo.Items.Count > 0) and (i = -1) then i := 0;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

procedure CargarEstadosTurno(Conn: TADOConnection; Combo: TComboBox; CodigoEstadoTurno:char = #0; TieneItemNinguno: boolean = false);
Var
	i: Integer;
	Qry: TADOQuery;
    s: AnsiString;
begin
	Qry := TAdoQuery.Create(nil);
	try
        s := '';
		Qry.Connection := Conn;
        s := 'SELECT DBO.ObtenerDescripcionEstadoTurno(DBO.CONST_TURNO_ESTADO_CERRADO()) AS Descripcion, DBO.CONST_TURNO_ESTADO_CERRADO() AS CodigoEstadoTurno' +
             ' UNION ' +
             ' SELECT DBO.ObtenerDescripcionEstadoTurno(DBO.CONST_TURNO_ESTADO_ABIERTO()) AS Descripcion, DBO.CONST_TURNO_ESTADO_ABIERTO() AS CodigoEstadoTurno ';
        Qry.SQL.Text := s;
		Qry.Open;
		Combo.Clear;
		i := -1;

        if TieneItemNinguno then begin
			Combo.Items.Add(TPA_SELECCIONE +  Space(200) + '0');
            i := 0;
		end;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString +  Space(200) + Qry.FieldByName('CodigoEstadoTurno').AsString[1]);
			if Qry.FieldByName('CodigoEstadoTurno').AsString[1] = CodigoEstadoTurno then
				i := Combo.Items.Count - 1;
			Qry.Next;
		end;
		Combo.ItemIndex := i;

	finally
		Qry.Close;
		Qry.Free;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarResultadosEstado
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection;
              Combo: TVariantComboBox;
              CodigoEstadoSolicitud:Integer = 0;
              CodigoEstadoResultado: integer = 0;
              TieneItemNinguno: boolean = false
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarResultadosEstados(Conn: TADOConnection; Combo: TVariantComboBox; CodigoEstadoSolicitud:Integer = 0; CodigoEstadoResultado: integer = 0; TieneItemNinguno: boolean = false);
Var
	i: Integer;
	Qry: TADOQuery;
    s: AnsiString;
begin
	Qry := TAdoQuery.Create(nil);
	try
        s := '';
		Qry.Connection := Conn;
        s := 'SELECT * FROM EstadosResultados  WITH (NOLOCK) Where CodigoEstadoSolicitud = ' + inttostr(CodigoEstadoSolicitud);
        Qry.SQL.Text := s;
		Qry.Open;
		Combo.Clear;
		i := -1;

        if TieneItemNinguno then begin
			Combo.Items.Add(TPA_SELECCIONE, 0);
            i := 0;
		end;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString,Qry.FieldByName('CodigoEstadoResultado').AsInteger);
			if Qry.FieldByName('CodigoEstadoResultado').AsInteger = CodigoEstadoResultado then
				i := Combo.Items.Count - 1;
			Qry.Next;
		end;
		Combo.ItemIndex := i;

	finally
		Qry.Close;
		Qry.Free;
	end;
end;


{-----------------------------------------------------------------------------
  Function Name: CargarEstadosSolicitud
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection;
              Combo: TComboBox;
              CodigoEstadoSolicitud:Integer = 0;
              TieneItemNinguno: boolean = false
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarEstadosSolicitud(Conn: TADOConnection; Combo: TComboBox; CodigoEstadoSolicitud:Integer = 0; TieneItemNinguno: boolean = false);
Var
	i: Integer;
	Qry: TADOQuery;
    s: AnsiString;
begin
	Qry := TAdoQuery.Create(nil);
	try
        s := '';
		Qry.Connection := Conn;
        s := 'SELECT * FROM EstadosSolicitudContacto WITH (NOLOCK) ';
        Qry.SQL.Text := s;
		Qry.Open;
		Combo.Clear;
		i := -1;
        if TieneItemNinguno then begin
			Combo.Items.Add(SIN_ESPECIFICAR + Space(200) + PadL('0', 10, ' '));
            i := 0;
        end;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString +
			  Space(200) + PadL(Trim(Qry.FieldByName('CodigoEstadoSolicitud').AsString), 10, ' '));
			if Qry.FieldByName('CodigoEstadoSolicitud').AsInteger = CodigoEstadoSolicitud then
			  i := Combo.Items.Count - 1;
			Qry.Next;
		end;
        if (Combo.Items.Count > 0) and (i = -1) then i := 0;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarSubTitulosFAQ
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection;
              Combo: TComboBox;
              CodigoSubTitulo: Integer = 0
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarSubTitulosFAQ(Conn: TADOConnection; Combo: TComboBox; CodigoSubTitulo: Integer = 0);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'SELECT * FROM FAQSubTitulos WITH (NOLOCK) ';
		Qry.Open;
		Combo.Clear;
		i := -1;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString +
			  Space(200) + Qry.FieldByName('CodigoSubTitulo').AsString);
			if Qry.FieldByName('CodigoSubTitulo').AsInteger = CodigoSubTitulo then
			  i := Combo.Items.Count - 1;
			Qry.Next;
		end;
		if (Combo.Items.Count > 0) and (i = -1) then i := 0;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarPreguntasFAQ
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection;
              Combo: TComboBox;
              Condicion: String;
              CodigoPregunta: Integer = 0
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarPreguntasFAQ(Conn: TADOConnection; Combo: TComboBox;  Condicion: String; CodigoPregunta: Integer = 0);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'SELECT * FROM FAQPreguntas  WITH (NOLOCK) ' + Condicion;
		Qry.Open;
		Combo.Clear;
		i := -1;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString +
			  Space(200) + Qry.FieldByName('CodigoPregunta').AsString);
			if Qry.FieldByName('CodigoPregunta').AsInteger = CodigoPregunta then
			  i := Combo.Items.Count - 1;
			Qry.Next;
		end;
		if (Combo.Items.Count > 0) and (i = -1) then i := 0;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

function  ValidarEmail(Conn: TADOConnection; email: ansistring): boolean;
var
    SP: TAdoStoredProc;
begin
    SP := TAdoStoredProc.Create(nil);
    eMail := copy(email, pos('@', email)+1, length(email));
    SP.ProcedureName := 'VerificarDominioMail';
    SP.Connection := Conn;
    SP.Parameters.CreateParameter('@Return_Value', ftInteger, pdReturnValue, 0, NULL);
    SP.Parameters.CreateParameter('@mail', ftString, pdInput, 120, eMail);
    SP.ExecProc;
    Result := SP.Parameters.ParamByName('@Return_Value').value = 0;
    SP.Close;
    SP.Free;
end;


function ObtenerCodigoPersona(Conn: TADOConnection; NumeroDocumento:String;TipoDocumento:String='RUT'):Integer;
var
    SP: TAdoStoredProc;
begin
    SP := TAdoStoredProc.Create(nil);
    try
        result:=-1;
        SP.ProcedureName := 'ObtenerDatosPersona';
        SP.Connection := Conn;
        SP.Parameters.CreateParameter('@CodigoPersona', ftInteger, pdInput, 0, NULL);
        SP.Parameters.CreateParameter('@CodigoDocumento', ftString, pdInput, 4, TipoDocumento);
        SP.Parameters.CreateParameter('@NumeroDocumento', ftString, pdInput, 11, NumeroDocumento);
        SP.open;
        Result := iif(trim(SP.FieldByName('CodigoPersona').AsString)='',-1,SP.FieldByName('CodigoPersona').AsInteger);
        SP.Close;
    finally
        SP.Free;
    end;

end;
{-----------------------------------------------------------------------------
  Function Name: ObtenerDatosTurno
  Author:
  Date Created:  /  /
  Description:
  Parameters:
  Return Value: N/A

  Revision : 1
    Date: 28/02/2009
    Author: mpiazza
    Description:  ss-510 Se agrega IdEstacion como parametro
-----------------------------------------------------------------------------}
function ObtenerDatosTurno(Conn: TADOConnection; CodigoPuntoEntrega: integer; CodigoPuntoVenta: integer; var DatosTurno: TDatosTurno):boolean;
resourcestring
    MSG_ERROR_TURNO         = 'Se produjo un error al obtener los datos del turno';
    MSG_CAPTION_ERROR_TURNO = 'Obtener datos de turno';
var
    SP: TAdoStoredProc;
begin
    SP := TAdoStoredProc.Create(nil);
    try
        try
            SP.ProcedureName := 'ObtenerDatosTurno';
            SP.Connection := Conn;
            SP.Parameters.CreateParameter('@CodigoPuntoVenta', ftInteger, pdInput, 0, CodigoPuntoVenta);
            SP.Parameters.CreateParameter('@CodigoPuntoEntrega', ftInteger, pdInput, 0, CodigoPuntoEntrega);
            SP.open;
            DatosTurno.NumeroTurno := SP.FieldByName('NumeroTurno').AsInteger;
            DatosTurno.CodigoUsuario := UpperCase(Trim(SP.FieldByName('CodigoUsuario').AsString));

            if SP.FieldByName('FechaHoraApertura').IsNull then
                DatosTurno.FechaHoraApertura := NullDate
            else
                DatosTurno.FechaHoraApertura := SP.FieldByName('FechaHoraApertura').AsDateTime;

            if SP.FieldByName('FechaHoraCierre').IsNull then
                DatosTurno.FechaHoraCierre := NullDate
            else
                DatosTurno.FechaHoraCierre := SP.FieldByName('FechaHoraCierre').AsDateTime;

            DatosTurno.CodigoPuntoVenta := SP.FieldByName('CodigoPuntoVenta').AsInteger;

            if (Trim(SP.FieldByName('Estado').AsString) <> '') then
                DatosTurno.Estado := Trim(SP.FieldByName('Estado').AsString)[1]
            else
                DatosTurno.Estado := #0;

            if (Trim(SP.FieldByName('IdEstacion').AsString) <> '') then
                DatosTurno.IdEstacion := Trim(SP.FieldByName('IdEstacion').AsString)
            else
                DatosTurno.IdEstacion := #0;

            SP.Close;
            result := true;
        except
	    	On e: exception do begin
    		    MsgBoxErr(MSG_ERROR_TURNO, E.message, MSG_CAPTION_ERROR_TURNO, MB_ICONSTOP);
                Result := False;
		    end;
        end;
    finally
        SP.Free;
    end;

end;

function VerificarPuntoEntregaVenta(Conn: TADOConnection;PuntoEntrega: integer; PuntoVenta: integer):boolean;
var
    SP: TAdoStoredProc;
begin
    SP := TAdoStoredProc.Create(nil);
    try
        SP.ProcedureName := 'VerificarPuntoEntregaVenta';
        SP.Connection := Conn;
        SP.Parameters.CreateParameter('@CodigoPuntoEntrega', ftInteger, pdInput, 0, PuntoEntrega);
        SP.Parameters.CreateParameter('@CodigoPuntoVenta', ftInteger, pdInput, 0, PuntoVenta);
        SP.Parameters.CreateParameter('@Valido', ftBoolean , pdInputOutput, 0, NULL);
        SP.ExecProc;

        Result := SP.Parameters.ParamByName('@Valido').Value;
        SP.Close;
    finally
        SP.Free;
    end;

end;

function ObtenerTiposTarjetasCreditos(Conn: TADOConnection; CodigoTipoTarjetaCredito:Integer):String;
var
    sp:TADOStoredProc;
begin
    sp:=TADOStoredProc.create(nil);
    try
        sp.Connection:=conn;
        sp.ProcedureName:='ObtenerTiposTarjetaCredito';
        sp.Parameters.refresh;
        sp.Parameters.ParamByName('@CodigoTipoTarjetaCredito').Value:=CodigoTipoTarjetaCredito;
        sp.Open;
        if sp.RecordCount=1 then result:=trim(sp.FieldByName('Descripcion').AsString)
        else result:=SIN_ESPECIFICAR;
    finally
        sp.Close;
        sp.Destroy;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ObtenerCuentasBancarias
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; Codigocuenta:Integer
  Return Value: String

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function ObtenerCuentasBancarias(Conn: TADOConnection; Codigocuenta:Integer):String;
begin
    result:=trim(QueryGetValue(Conn,'select Descripcion from TiposCuentasBancarias WITH (NOLOCK) where CodigoTipoCuentaBancaria ='+inttostr(Codigocuenta)));
    if Result='' then result:=SIN_ESPECIFICAR;
end;

function ObtenerStockAlmacen(Conn: TADOConnection; CodigoAlmacen: integer; Categoria: integer): integer;
begin
    Result := QueryGetValueInt(Conn, 'SELECT DBO.ObtenerSaldoAlmacen(' + inttostr(CodigoAlmacen) + ',' + inttostr(Categoria) + ')');
end;

function ValidarStockAlmacen(Conn: TADOConnection; CodigoAlmacen: integer; Categoria: integer): boolean;
begin
    Result := ObtenerStockAlmacen(Conn, CodigoAlmacen, Categoria) > 0;
end;

function ObtenerDuenioTagVendido(Conn: TADOConnection; ContextMarks: Integer; ContractSerialNumber: DWord): Integer;
begin
    Result := QueryGetValueInt(Conn, 'select dbo.ObtenerDuenioTagVendido( ' + IntToStr(ContextMarks) + ' , ' + FloatToStr(ContractSerialNumber) + ' )');
end;

{-----------------------------------------------------------------------------
  Function Name: ObtenerCategoriaVehiculo
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; CodigoTipoVehiculo:Integer
  Return Value: Integer;

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function ObtenerCategoriaVehiculo(Conn: TADOConnection; CodigoTipoVehiculo:Integer):Integer;
begin
    result := QueryGetValueInt(Conn,'Select CategoriaTag From vehiculostipos WITH (NOLOCK) WHERE CodigoTipoVehiculo='+inttostr(CodigoTipoVehiculo));
end;

{-----------------------------------------------------------------------------
  Function Name: ValidarVersion
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; CodigoSistema:Integer
  Return Value: Boolean

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function ValidarVersion(Conn: TADOConnection; CodigoSistema:Integer): Boolean; overload; //SS_925_NDR_20131122
Var
	Qry: TADOQuery;
    VersionOk: boolean;
    MajorVersion, MinorVersion, Release, Build: Word;
begin
    VersionOk := False;
	Qry := TAdoQuery.Create(nil);
	try
        { INICIO : 20160307 MGO
		Qry.Connection := Conn;
        }
        Qry.Connection := DMConnections.BaseBO_Master;
        // FIN : 20160307 MGO
		Qry.SQL.Text := 'SELECT TOP 1 * FROM SISTEMASVERSIONES  WITH (NOLOCK) WHERE CODIGOSISTEMA = ' + inttostr(CodigoSistema) +  ' ORDER BY FechaHora DESC';
		Qry.Open;
	    if Qry.RecordCount > 0 then
	    begin
	        GetFileVersion(GetExeName, MajorVersion, MinorVersion, Release, Build);
	        VersionOk := (Qry.FieldByName('MajorVersion').AsInteger = MajorVersion) and
	            (Qry.FieldByName('MinorVersion').AsInteger = MinorVersion) and
	            (Qry.FieldByName('Release').AsInteger = Release);
	    end
	    else
	        VersionOk := false;

	finally
		Qry.Close;
		Qry.Free;
        result := VersionOk;
	end;
end;


{-----------------------------------------------------------------------------
  Function Name: ObtenerAlmacenPuntoEntrega
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; CodigoPuntoEntrega: integer
  Return Value: integer

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function ObtenerAlmacenPuntoEntrega(Conn: TADOConnection; CodigoPuntoEntrega: integer): integer;
begin
    Result := QueryGetValueInt(Conn, 'SELECT CodigoAlmacenRecepcion FROM PuntosEntrega WITH (NOLOCK) WHERE CodigoPuntoEntrega = ''' + inttostr(CodigoPuntoEntrega) + '''');
end;


{-----------------------------------------------------------------------------
  Function Name: ObtenerDescripcionActividad(
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; CodigoActividad:Integer
  Return Value: String

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function ObtenerDescripcionActividad(Conn: TADOConnection; CodigoActividad:Integer):String;
begin
    result:=trim(QueryGetValue(Conn,'SELECT Descripcion from Actividades  WITH (NOLOCK) where CodigoActividad ='+inttostr(CodigoActividad)));
    if Result='' then result:=SIN_ESPECIFICAR;
end;

{-----------------------------------------------------------------------------
  Function Name: ObtenerDescripcionBanco
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; CodigoBanco:Integer
  Return Value: String

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function ObtenerDescripcionBanco(Conn: TADOConnection; CodigoBanco:Integer):String;
begin
    result := Trim(QueryGetValue(Conn, 'select descripcion from bancos WITH (NOLOCK) where codigoBanco = ' + inttostr(CodigoBanco)));
end;
{-----------------------------------------------------------------------------
  Function Name: VerTurnoAbierto
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection;
              CodigoPuntoEntrega: integer;
              CodigoPuntoVenta: integer;
              CodigoUsuario: string;
              var DatosTurno: TDatosTurno
  Return Value: boolean

Revision : 1
    Date: 29/01/2010
    Author: mpiazza
    Description:  Ref SS-510 Se agrega una validacion por IP en

-----------------------------------------------------------------------------}
function VerTurnoAbierto(Conn: TADOConnection; CodigoPuntoEntrega: integer; CodigoPuntoVenta: integer; CodigoUsuario: string; var DatosTurno: TDatosTurno):boolean;
begin
    (* Inicializar la estructura para, luego, saber si la ejecuci�n de
    ObtenerDatosTurno fue sin errores. *)
    DatosTurno.NumeroTurno := -1;
    if not ObtenerDatosTurno(Conn, CodigoPuntoEntrega, CodigoPuntoVenta, DatosTurno) then begin
        Result := False;
        Exit;
    end else begin
        (* Si se encontr� un Turno y est� Abierto y pertenece al Usuario pasado
        como par�metro, retornar True. *)
        if (DatosTurno.NumeroTurno <> -1) and
            ((DatosTurno.Estado = Trim(QueryGetValue(Conn, 'SELECT DBO.CONST_TURNO_ESTADO_ABIERTO()'))) and
            (UpperCase(Trim(CodigoUsuario)) = UpperCase(Trim(DatosTurno.CodigoUsuario)))) and
             ( ResolveHostNameStr(GetMachineName) = DatosTurno.IdEstacion ) then Result := True
        else Result := False;
    end;
end;

function ActualizarMontoTurno(conn: TADOConnection; CodigoTurno: Integer; Monto: Double): Boolean;
resourcestring
    MSG_ERROR_INICIALIZACION = 'Error actualizando el monto de apertura del turno.';
    CAPTION_ERROR =  'Actualizar Monto';
var
    SP: TAdoStoredProc;
begin
    SP := TAdoStoredProc.Create(nil);
    SP.ProcedureName := 'ActualizarMontoAperturaTurno';
    try
        with SP do begin
            Connection := Conn;
            Parameters.refresh;
            Parameters.ParamByName('@NumeroTurno').Value        := CodigoTurno;
            Parameters.ParamByName('@MontoApertura').Value      := Monto;
            ExecProc;
            result := True;
        end;
    except
    	On E: Exception do begin
            result := false;
        	MsgBoxErr(MSG_ERROR_INICIALIZACION, e.Message, CAPTION_ERROR, MB_ICONSTOP);
        end;
	end;
end;


function ObtenerConvenioVehiculo(Coon:TADOConnection;TipoPatente,Patente:String):String;
begin
    result:=trim(QueryGetValue(Coon,format('SELECT dbo.ExisteVehiculoConvenio (''%s'',''%s'')',[TipoPatente,Patente])));
end;


function NumeroCalleNormalizado(Conn:TADOConnection; CodigoCalle:integer;CodigoRegion,CodigoComuna:String;Numero:Integer):Boolean;
begin
    result:=QueryGetValueInt(Conn,format('EXEC NumeroCalleNormalizado %d,''%s'',''%s'',%d',[CodigoCalle,CodigoRegion,CodigoComuna,Numero]))=1;
end;

function ArmarDescripcionDatosVehiculo(CodigoMarca,CodigoModelo:Integer;CodigoColor:Integer=-1):String;
begin
    result:=iif(QueryGetValue(DMConnections.BaseCAC,format('SELECT dbo.ArmarDescripcionDatosVehiculo(dbo.ObtenerMarca(%d),dbo.ObtenerVehiculoModelo(%d, %d),dbo.ObtenerColor(%d))',[CodigoMarca,CodigoMarca,CodigoModelo,CodigoColor]))=null,'',QueryGetValue(DMConnections.BaseCAC,format('SELECT dbo.ArmarDescripcionDatosVehiculo(dbo.ObtenerMarca(%d),dbo.ObtenerVehiculoModelo(%d, %d),dbo.ObtenerColor(%d))',[CodigoMarca,CodigoMarca,CodigoModelo,CodigoColor])));
end;

function ArmarDescripcionDatosVehiculo(Marca,Modelo:String;Color:String=''):String; overload;
begin
    result:=iif(QueryGetValue(DMConnections.BaseCAC,format('SELECT dbo.ArmarDescripcionDatosVehiculo(''%s'',''%s'',''%s'')',[Marca,Modelo,Color]))=null,'',QueryGetValue(DMConnections.BaseCAC,format('SELECT dbo.ArmarDescripcionDatosVehiculo(''%s'',''%s'',''%s'')',[Marca,Modelo,Color])));
end;

function FormatearNumeroCalle(NumeroCalle:String):Integer;
begin
    result:=QueryGetValueInt(DMConnections.BaseCAC,format('SELECT dbo.FormatearNumeroCalle (''%s'')',[NumeroCalle]));
end;

procedure GestionarComponentesyHints(Form:TComponent;Database:TADOConnection;ComponentesExcluidos: array of TControl;GrabarOcultos:Boolean=False;ComponentesExcluidosdelHint:TControl=nil);
begin
    GrabarComponentesObligatoriosyHints(form,Database,ComponentesExcluidos,GrabarOcultos,ComponentesExcluidosdelHint);
    ObtenerComponentesObligatoriosyHints(Form,Database);
end;

{******************************** function Header ******************************
function Name: GrabarComponentesObligatoriosyHints
Author       : dcalani
Date Created : 20/04/2004
Description  : Procedimiento que se encarga de guardar en la base el nombre del compoente
                y si es optativo u obligatorio. Y guarda el hint de cada componente
Parameters   : Form:TComponent;Database:TADOConnection;ComponentesExcluidos: array of TControl
Return Value : None
*******************************************************************************}
procedure GrabarComponentesObligatoriosyHints(Form:TComponent;Database:TADOConnection;ComponentesExcluidos: array of TControl;GrabarOcultos:Boolean=False;ComponentesExcluidosdelHint:TControl=nil);

    function EsComponenteDeDato(Componente:TComponent):Boolean;
    var
        ClassRef: TClass;
        i:integer;
    begin
        result:=False;

        ClassRef := Componente.ClassType;
        while ClassRef <> nil do
        begin
          for i:=1 to length(TMPComponentesDato) do if UpperCase(TMPCOMPONENTESDATO[i])=UpperCase(ClassRef.ClassName) then result:=True;
          ClassRef := ClassRef.ClassParent;
        end;
    end;

    function EsComponenteDeHint(Componente:TComponent):Boolean;
    var
        ClassRef: TClass;
        i:integer;
    begin
        result:=False;

        ClassRef := Componente.ClassType;
        while ClassRef <> nil do
        begin
          for i:=1 to length(TMPCOMPONENTESHINT) do if UpperCase(TMPCOMPONENTESHINT[i])=UpperCase(ClassRef.ClassName) then result:=True;
          ClassRef := ClassRef.ClassParent;
        end;
    end;


    function EsComponenteExcluido(Componente:TComponent;ComponentesExcluidos: array of TControl):Boolean;
    var
        i:Integer;
    begin
        result:=False;
        for i:=0 to length(ComponentesExcluidos)-1 do if UpperCase(Componente.Name)=UpperCase(ComponentesExcluidos[i].Name) then result:=True;
    end;

    //BEGIN : SS_925_NDR_20141202 --------------------------------------------------------------------------
    function GenerarMenues(Database:TADOConnection):Boolean;
    var Qry: TADOQuery;
    begin
      Result:=False;
      Qry := TAdoQuery.Create(nil);
      { INICIO : 20160307 MGO
      Qry.Connection := Database;
      }
      Qry.Connection := DMConnections.BaseBO_Master;
      // FIN : 20160307 MGO
      Qry.SQL.Text := 'SELECT TOP 1 * FROM SISTEMASVERSIONES  WITH (NOLOCK) WHERE CODIGOSISTEMA = ' + inttostr(SistemaActual) +  ' ORDER BY FechaHora DESC';
  		Qry.Open;
      if Qry.RecordCount > 0 then
      begin
        Result := Qry.FieldByName('GenerarMenues').AsBoolean;
      end;
    end;
    //END : SS_925_NDR_20141202 --------------------------------------------------------------------------

var
    Obligatorio,i:Integer;
    LabeldelCombo:TLabel;
    Hint,DescripComponente:String;
    Entro:Boolean;
begin
    //if (InstallIni.ReadInteger('General', 'GenerarMenues', 1) = 1) then begin       //SS_925_NDR_20141202
    if GenerarMenues(Database) then                                                   //SS_925_NDR_20141202
    begin
        try
            Database.BeginTrans;
            for i:=0 to Form.ComponentCount-1 do begin
                Entro:=False;
                Hint:='';
                Obligatorio:=-1;

                if (TWinControl(form.Components[i]).Visible) and
                ((ComponentesExcluidosdelHint=nil) or (form.Components[i].Name<>ComponentesExcluidosdelHint.Name)) then begin
                    if (EsComponenteDeDato(form.Components[i])) and  not(EsComponenteExcluido(Form.Components[i],ComponentesExcluidos)) and (TControl(Form.Components[i]).Visible or GrabarOcultos)  then begin
                        entro:=true;
                        Obligatorio:=iif(EsComponenteObligatorio(TControl(Form.Components[i])),1,0);
                    end;

                    if EsComponenteDeHint(Form.Components[i]) and (TControl(Form.Components[i]).Visible or GrabarOcultos)  then begin
                        entro:=true;
                        hint:=TControl(Form.Components[i]).Hint;
                    end;

                    if Entro then begin
                        LabeldelCombo:=ObtenerLabelComponente(Form,Form.Components[i]);
                        DescripComponente:=iif(LabeldelCombo<>nil,LimpiarCaracteresLabel(LabeldelCombo.Caption),iif(hint='',SIN_ESPECIFICAR,hint));
                        QueryExecute(Database,format('EXEC ActualizarComponentesSistemas %d,''%s'',%d,''%s'',%d,''%s'',''%s'', ''%s''',[SistemaActual , DarNombredeClase(Form) , TForm(Form).TabOrder+1, Form.Components[i].Name ,Obligatorio ,DarCaption(Form) ,DescripComponente, hint]));
                    end;
                end;

            end;
            QueryExecute(Database,format('EXEC ActualizarIntegridadComponentesSistemas %d,''%s'',%d',[SistemaActual , DarNombredeClase(Form), TForm(Form).TabOrder+1]));
            Database.CommitTrans;
        except
            On E: Exception do begin
                Database.RollbackTrans;
                MsgBoxErr(MSG_ERROR_GRAVAR_CAMPOS_OBLIGATORIO, e.message, MSG_CAPTION_GRAVAR_CAMPOS_OBLIGATORIO, MB_ICONSTOP);
            end;
        end;
    end;
end;

{******************************** function Header ******************************
function Name: ObtenerLabelComponente
Author       : dcalani
Date Created : 20/04/2004
Description  : Es utilizada por los procedimientos de manejo de campos obligatorios
                / optativos, y devuelve el label asociado a un componente.
Parameters   : Form,Componente:TComponent
Return Value : TLabel
*******************************************************************************}
function ObtenerLabelComponente(Form,Componente:TComponent):TLabel;
var
    i:Integer;
begin
    result:=nil;
    for i:=0 to form.ComponentCount-1 do
        if (form.Components[i].ClassType=TLabel) and (TLabel(form.Components[i]).FocusControl<>Nil) and (TLabel(form.Components[i]).FocusControl.Name=Componente.Name) then
            Result:=TLabel(form.Components[i]);

end;


{******************************** function Header ******************************
function Name: ObtenerComponentesObligatoriosyHints
Author       : dcalani
Date Created : 20/04/2004
Description  : Procedimiento que se encarga de setear en obligatorios u optativos
            los componentes del form pasado como parametro.
Parameters   : Form:TComponent;Database:TADOConnection
Return Value : None

Revision     : 1
Author       : dAllegretti
Date Created : 22/10/2008
Description  : Agrando la longitud del string del par�metro @Formulario
*******************************************************************************}
procedure ObtenerComponentesObligatoriosyHints(Form:TComponent;Database:TADOConnection);

    function BuscarComponente(Form:TComponent;NombreComponente:String):TControl;
    var
        i:Integer;
    begin
        result:=nil;
        for i:=0 to form.ComponentCount-1 do
            if (Trim(UpperCase(form.Components[i].Name))=Trim(UpperCase(NombreComponente))) then
                Result:=TControl(form.Components[i]);
    end;

var
    sp:TADOStoredProc;
    auxComponente:TComponent;
    auxLabel:TLabel;
begin
    SP := TADOStoredProc.Create(nil);
    try
        try
            with SP do begin
                Connection := TADOConnection(Database);
                ProcedureName := 'ObtenerComponentesSistemas';
                Parameters.CreateParameter('@CodigoSistema', ftInteger, pdInput, 0, SistemaActual);
                Parameters.CreateParameter('@Formulario',  ftString, pdInput, 60, DarNombredeClase(Form));
                Parameters.CreateParameter('@Indice',  ftInteger, pdInput, 0, TForm(Form).TabOrder+1);

                Open;
                first;
                while not(eof) do begin
                    //cargo lo aobligatoriedad
                    auxComponente:=BuscarComponente(Form,fieldByName('Componente').AsString);
                    if auxComponente <> nil then begin
                        auxLabel:=ObtenerLabelComponente(form,auxComponente);
                        if auxComponente<>nil then begin
                            if not(fieldByName('Obligatorio').IsNull) then begin
                                TEdit(auxComponente).Color:=iif(fieldByName('Obligatorio').AsBoolean,COLOR_EDITS_OBLIGATORIO,COLOR_EDITS_NO_OBLIGATORIO);
                                if auxLabel<>nil then begin
                                    if fieldByName('Obligatorio').AsBoolean then begin
                                        auxLabel.Font.Color:=COLOR_LABEL_OBLIGATORIO;
                                        auxLabel.Font.Style:=STYLE_LABEL_OBLIGATORIO;
                                    end else begin
                                        auxLabel.Font.Color:=COLOR_LABEL_NO_OBLIGATORIO;
                                        auxLabel.Font.Style:=STYLE_LABEL_NO_OBLIGATORIO;
                                    end;
                                end;
                            end;
                            // cargo el hint
                            TControl(auxComponente).Hint:=fieldByName('Hint').AsString;
                            TControl(auxComponente).ShowHint:=True;
                        end;
                    end;
                    next;
                end;
                close;
            end;

        except
            On E: Exception do begin
                DMConnections.BaseCAC.RollbackTrans;
                MsgBoxErr(MSG_ERROR_CARGA_CAMPOS_OBLIGATORIO, e.message, MSG_CAPTION_CARGA_CAMPOS_OBLIGATORIO, MB_ICONSTOP);
            end;
        end;
    finally
        SP.Free;
    end;
end;

{******************************** function Header ******************************
function Name: EsComponenteObligatorio
Author       : dcalani
Date Created : 20/04/2004
Description  : Funcion que indica si el componente pasado es del tipo obligatorio
Parameters   : Componente:TControl
Return Value : Boolean
*******************************************************************************}
function EsComponenteObligatorio(Componente:TControl):Boolean;
begin
    result:=TEdit(Componente).Color=COLOR_EDITS_OBLIGATORIO;
end;


{******************************** function Header ******************************
function Name: LimpiarCaracteresLabel
Author       : dcalani
Date Created : 20/04/2004
Description  : Es utilizada por los procedimientos de manejo de campos obligatorios
                / optativos, y saca los caracteres '&' ':' de un texto
Parameters   : Caption:String
Return Value : String
*******************************************************************************}
function LimpiarCaracteresLabel(Caption:String):String;
var
    i:Integer;
    a:String;
begin
    a:='';
    for i:=1 to length(Caption) do
        if (copy(caption,i,1)<>'&') and (copy(caption,i,1)<>':') then
            a:=a+copy(caption,i,1);

    result:=a;
end;

{******************************** function Header ******************************
function Name: DarCaption
Author       : dcalani
Date Created : 20/04/2004
Description  : Es utilizada por los procedimientos de manejo de campos obligatorios
                / optativos, y el caption del form o el caption del form / frame si es un frame
Parameters   : Componente:TComponent
Return Value : String
*******************************************************************************}
function DarCaption(Componente:TComponent):String;
var
    AuxPadre:TForm;
begin
    result:='';
    AuxPadre := TForm(componente);
    while AuxPadre.Parent<>nil do
    begin
        AuxPadre:=TFORM(AuxPadre.Parent);
    end;
    if AuxPadre.Caption=TForm(Componente).Caption then result:=AuxPadre.Caption
    else result:=AuxPadre.Caption+' / '+LimpiarCaracteresLabel(TForm(Componente).Caption);
end;

{******************************** function Header ******************************
function Name: DarNombredeClase
Author       : dcalani
Date Created : 20/04/2004
Description  : Es utilizada por los procedimientos de manejo de campos obligatorios
                / optativos, y devuelve el nombre form o el nomrbe del form del parent
                si se trata de un frame.
Parameters   : Componente:TComponent
Return Value : String
*******************************************************************************}
function DarNombredeClase(Componente:TComponent):String;
var
    AuxPadre:TForm;
begin
    result:='';
    AuxPadre := TForm(componente);
    while AuxPadre.Parent<>nil do
    begin
        AuxPadre:=TFORM(AuxPadre.Parent);
    end;
    result:=AuxPadre.ClassName;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarCategoriasVehiculos
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection;
              Combo: TVariantComboBox;
              CodigoCategoria: Integer = 0;
              Todos: Boolean = false
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure CargarCategoriasVehiculos(Conn: TADOConnection; Combo: TVariantComboBox; CodigoCategoria: Integer = 0; Todos: Boolean = false); overload;
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'SELECT Categoria, ''Cat. '' + CAST(Categoria AS VARCHAR) + '' - '' + RTRIM(LTRIM(Descripcion)) AS Descripcion FROM Categorias ORDER BY Categoria';
		Qry.Open;
		Combo.Clear;
   		i := -1;
        if Todos then begin
			Combo.Items.Add(TPA_SELECCIONE, 0);
            i := 0;
		end;
		While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString,Qry.FieldByName('Categoria').AsInteger);
			if Qry.FieldByName('Categoria').AsInteger = CodigoCategoria then
				i := Combo.Items.Count - 1;
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

{******************************** function Header ******************************
function Name: NowBase
Author       : dcalani
Date Created : 21/04/2004
Description  : Devuelve la fecha y hora de la Base de Datos
Parameters   : None
Return Value : TDateTime
*******************************************************************************}
function NowBase(Database:TADOConnection):TDateTime;
begin
//    result := QueryGetValueDateTime(Database, 'SELECT GETDATE()');
    Result := SysUtilsCN.NowBaseCN(Database);
end;

function NowBaseSmall(Database:TADOConnection):TDateTime;
begin
//    result := QueryGetValueDateTime(Database, 'SELECT CAST(GETDATE() as SMALLDATETIME)');
    Result := SysUtilsCN.SmallNowBaseCN(Database);
end;

function MesNombre(Fecha: TDateTime): String;
resourcestring
    STR_ENERO      = 'Enero';
    STR_FEBRERO    = 'Febrero';
    STR_MARZO      = 'Marzo';
    STR_ABRIL      = 'Abril';
    STR_MAYO       = 'Mayo';
    STR_JUNIO      = 'Junio';
    STR_JULIO      = 'Julio';
    STR_AGOSTO     = 'Agosto';
    STR_SEPTIEMBRE = 'Septiembre';
    STR_OCTUBRE    = 'Octubre';
    STR_NOVIEMBRE  = 'Noviembre';
    STR_DICIEMBRE  = 'Diciembre';
begin
    Case MonthOf(Fecha) of
        1: result   := STR_ENERO;
        2: result   := STR_FEBRERO;
        3: result   := STR_MARZO;
        4: result   := STR_ABRIL;
        5: result   := STR_MAYO;
        6: result   := STR_JUNIO;
        7: result   := STR_JULIO;
        8: result   := STR_AGOSTO;
        9: result   := STR_SEPTIEMBRE;
        10: result  := STR_OCTUBRE;
        11: result  := STR_NOVIEMBRE;
        12: result  := STR_DICIEMBRE;
        else result := STR_ERROR;
    end;
end;

function InvocarCorreoPredeterminado(Componente: TWinControl; Direccion:string; Asunto: string; Cuerpo: AnsiString): boolean;
begin
  result := ShellExecute(Componente.Handle, nil, PChar('mailto:' + Trim(Direccion)
      + '?Subject=' + iif(Trim(Asunto)='', ' ',Trim(Asunto)) + '&Body=' + Cuerpo),
      nil, nil, SW_NORMAL) > 32;
end;

{******************************** Function Header ******************************
Function Name: SaveMessageIntoLogFile
Author : ggomez
Date Created : 20/02/2008
Description : Save into a file a message.
Parameters : const Path, FileName, FileExtension, Msg, MachineName, UserName: String
    Path: Path where the file is located.
    Filename: Name of de file.
    FileExtension: Extension of the file.
    Msg: Message to save.
    MachineName: Name of the machine associated to the Msg.
    UserName: Name of the user associated to the Msg.
Return Value : None
*******************************************************************************}
procedure GrabarMensajeEnArchivoLog(const Path, FileName, FileExtension, Msg, MachineName, UserName: String);
const
    MAX_RETRIES = 100;
Var
    Text: AnsiString;
    i: Integer;
    FileNameComplete: String;
    Stream: TFileStream;
begin
    FileNameComplete := IncludeTrailingPathDelimiter(Path) + FileName + FormatDateTime('yyyymmdd', Date) + '.' + FileExtension;
    for i := 1 to MAX_RETRIES do begin
        try
            if FileExists(FileNameComplete) then begin
                Stream := TFileStream.Create(FileNameComplete, fmOpenReadWrite + fmShareExclusive);
            end else begin
                Stream := TFileStream.Create(FileNameComplete, fmCreate);
            end;
            try
                // Cabecera
                Text := FormatDateTime('yyyy"-"mm"-"dd hh":"nn":"ss":"zzz', Now()) + ', ' +
                    MachineName + ', ' + UserName + ', ' + Msg + CRLF;
                Stream.Position := Stream.Size;
                Stream.Write(Text[1], Length(Text));
                Exit;
            finally
                Stream.Free;
            end;
        except
            on E: Exception do begin
                if i = MAX_RETRIES then raise;
            end;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: SerialNumberToEtiqueta
Author :
Date Created :
Description : Devuelve el Serial Number de un TAG en formato de etiqueta,
    Dig.Ver. inclu�do
Parameters : SerialNumber: AnsiString
Return Value : AnsiString

Revision 1:
    Author : ggomez
    Date : 02/08/2006
    Description : Cambi� la funci�n que retorna el valor para almacenarlo en la
        la variable Buffer. El cambio se hizo pues IVal retorna un Longint y hay
        ContractSerialNumber que no entran en un longint. Ahora se usa
        StrToInt64. Un ejemplo de ContractSerialNumber que no entra en Longint
        es 2235583011, para este caso la funci�n IVal retorna 223558301 (le
        falta el �ltimo 1). Debido a esto la funci�n SerialNumberToEtiqueta no
        retorna el valor correcto en estos casos.

*******************************************************************************}
function SerialNumberToEtiqueta(SerialNumber: AnsiString): AnsiString;

	function BufferToSerialNumber(Buff: PCHAR): String;
	Var
		buff2: Array[0..3] of BYTE;
        dig1, dig2: byte;
	begin
		Buff2[0] := BYTE(Buff[3]);
		Buff2[1] := BYTE(Buff[2]);
		Buff2[2] := BYTE(Buff[1]);
		Buff2[3] := BYTE(Buff[0]);

        dig1 := (Buff2[0] and $F0) div 16;
        dig2 := Buff2[0] and $0F;
		Result := IStr0(dig1, 1) + IStr0(dig2, 1) +
	  //  IntToHex(Buff2[0], 2)	+			// A�o de Fabricaci�n
		  IStr0(Buff2[1] shr 2, 2);			// Semana de Fabricaci�n

		Buff2[1] := Buff2[1] and 3;

		Result := Result + IStr0(HexToInt(		// N�mero Secuencial
		  IntToHex(Buff2[1], 2) +
		  IntToHex(Buff2[2], 2) +
		  IntToHex(Buff2[3], 2)), 6);

		Result := Result + Luhn(Result);		// D�gito Verificador
	end;

Var
	Buffer: DWORD;
begin
// Comentado en Revision 1
//	Buffer := IVal(SerialNumber);
// Agregado en Revision 1
	Buffer := StrToInt64(SerialNumber);
	Result := BufferToSerialNumber(@Buffer);
end;

function SerialNumberToEtiqueta(SerialNumber: DWORD): AnsiString;
begin
    Result := SerialNumberToEtiqueta(FloatToStr(SerialNumber));
end;

// Devuelve el Serial Number desde una etiqueta
function EtiquetaToSerialNumber(Etiqueta: AnsiString): DWORD;
Var
    dig1, dig2, AA1, AA2, SS, NN: DWORD;
begin
	result := 0;
    // si la longitud es incorrecta, sale
    if Length(Etiqueta) < 10 then exit;
    dig1 := IVal(Copy(Etiqueta, 1, 1));
    dig2 := IVal(Copy(Etiqueta, 2, 1));

  //	AA := IVal(Copy(Etiqueta, 1, 2)) shl 24;		// A�o

  	AA1 := dig1 shl 28;		// Digito 1 A�o
    AA2 := dig2 shl 24;		// Digito 2 A�o
    SS := IVal(Copy (Etiqueta, 3, 2)) shl 18;		// Semana
    NN := IVal(Copy (Etiqueta, 5, 6));				// Numero de Serie

	Result := AA1 or AA2 or SS or NN
end;



// Devuelve la cantidad de TAGs libres para ingresar, esto es, no reservados por
// recepci�n no nominada
function TAGSLibres(Categoria: Integer): LongInt;
begin
	result := QueryGetValueInt(DMConnections.BaseCAC, 'Select dbo.TAGsSinRecibir('+IntToStr(Categoria)+')');
end;

// Valida el formato de una etiqueta
function ValidarEtiqueta(TextoCaption: ANSIString; Referencia, Etiqueta: ANSIString; ConMsg: Boolean=False): Boolean;
begin
	result := SerialNumberToEtiqueta (FloatToStr(EtiquetaToSerialNumber(PadL(Etiqueta, 11, '0')))) = PadL(Etiqueta, 11, '0');
	if ConMsg and (not result) then MsgBox(Format(MSG_ERROR_ETIQUETA_TAG, [Referencia, Etiqueta]), TextoCaption, MB_OK + MB_ICONSTOP);
end;

// Verifica si existe la categor�a
{-----------------------------------------------------------------------------
  Function Name: ValidarCategoria
  Author:
  Date Created:  /  /
  Description:
  Parameters: TextoCaption: ANSIString; Categoria: Integer; ConMsg: Boolean=False
  Return Value: Boolean

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function ValidarCategoria(TextoCaption: ANSIString; Categoria: Integer; ConMsg: Boolean=False): Boolean;
begin
	result := QueryGetValueInt(DMConnections.BaseCAC,
      'select ISNULL((Select Categoria From Categorias  WITH (NOLOCK) where Categorias.Categoria = ' + IntToStr(Categoria) + '), 0)') <> 0;
	if ConMsg and (not result) then MsgBox(MSG_ERROR_CATEGORIA, TextoCaption, MB_OK + MB_ICONSTOP);
end;

//INICIO	: CFU 20161026 TASK_057_CFU_20161017-Gesti�n_TAGs_Doble_Categoria
function ValidarCategoriaUrbana(TextoCaption: ANSIString; CategoriaUrbana: Integer; ConMsg: Boolean=False): Boolean;
begin
	result := QueryGetValueInt(DMConnections.BaseCAC,
      'select ISNULL((Select CategoriaUrbana From CategoriasUrbanas WITH (NOLOCK) where CategoriasUrbanas.CategoriaUrbana = ' + IntToStr(CategoriaUrbana) + '), 0)') <> 0;
	if ConMsg and (not result) then MsgBox(MSG_ERROR_CATEGORIA, TextoCaption, MB_OK + MB_ICONSTOP);
end;
//TERMINO	: CFU 20161026 TASK_057_CFU_20161017-Gesti�n_TAGs_Doble_Categoria


// Verifica si un TAG pertenece a una categor�a
{-----------------------------------------------------------------------------
  Function Name: ValidarCategoriaTAG
  Author:
  Date Created:  /  /
  Description:
  Parameters: TextoCaption: ANSIString;
              Referencia: ANSIString;
              TAG: DWORD;
              Categoria: integer;
              ConMsg: Boolean=False
  Return Value: Boolean;

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function ValidarCategoriaTAG(TextoCaption: ANSIString; Referencia: ANSIString; TAG: DWORD; Categoria: integer; ConMsg: Boolean=False): Boolean;
var
	aCategoriaI,
	aCategoriaM: Integer;
begin
	aCategoriaM := 0;
	aCategoriaI := (QueryGetValueInt(DMConnections.BaseCAC,
    	            'select ISNULL((Select Categoria From InterfaseTAGs  WITH (NOLOCK) ' +
        	        ' where InterfaseTAGs.ContractSerialNumber = ' + IntToStr(TAG) + '), 0)'));
    if aCategoriaI = 0 then begin
		aCategoriaM := (QueryGetValueInt(DMConnections.BaseCAC,
    	        	    'select ISNULL((Select CodigoCategoria From MaestroTAGs  WITH (NOLOCK) ' +
        	        	' where MaestroTAGs.ContractSerialNumber = ' + IntToStr(TAG) + '), 0)'))
    end;

	result := (aCategoriaI = Categoria) or (aCategoriaM = Categoria);

    if ConMsg and (not result) then begin
    	if (aCategoriaI = 0) and (aCategoriaM = 0) then
	    	MsgBox(Format(MSG_ERROR_NO_EXISTE_TAG, [Referencia]), TextoCaption, MB_OK + MB_ICONSTOP)
    	else
	    	MsgBox(Format(MSG_ERROR_CATEGORIA_PARA_TAG, [Referencia]), TextoCaption, MB_OK + MB_ICONSTOP);
    end
end;

//INICIO	: CFU 20161026 TASK_057_CFU_20161017-Gesti�n_TAGs_Doble_Categoria
function ValidarCategoriaUrbanaTAG(TextoCaption: ANSIString; Referencia: ANSIString; TAG: DWORD; Categoria: integer; ConMsg: Boolean=False): Boolean;
var
	aCategoriaI,
	aCategoriaM: Integer;
begin
	aCategoriaM := 0;
	aCategoriaI := (QueryGetValueInt(DMConnections.BaseCAC,
    	            'select ISNULL((Select CategoriaUrbana From InterfaseTAGs  WITH (NOLOCK) ' +
        	        ' where InterfaseTAGs.ContractSerialNumber = ' + IntToStr(TAG) + '), 0)'));
    if aCategoriaI = 0 then begin
		aCategoriaM := (QueryGetValueInt(DMConnections.BaseCAC,
    	        	    'select ISNULL((Select CodigoCategoriaUrbana From MaestroTAGs  WITH (NOLOCK) ' +
        	        	' where MaestroTAGs.ContractSerialNumber = ' + IntToStr(TAG) + '), 0)'))
    end;

	result := (aCategoriaI = Categoria) or (aCategoriaM = Categoria);

    if ConMsg and (not result) then begin
    	if (aCategoriaI = 0) and (aCategoriaM = 0) then
	    	MsgBox(Format(MSG_ERROR_NO_EXISTE_TAG, [Referencia]), TextoCaption, MB_OK + MB_ICONSTOP)
    	else
	    	MsgBox(Format(MSG_ERROR_CATEGORIA_PARA_TAG, [Referencia]), TextoCaption, MB_OK + MB_ICONSTOP);
    end
end;
//TERMINO	: CFU 20161026 TASK_057_CFU_20161017-Gesti�n_TAGs_Doble_Categoria

// Verifica si un registro de recepci�n/control calidad de TAGs es editable
function ValidarEditable (TextoCaption: ANSIString; Editable: Boolean; ConMsg: Boolean=False): Boolean;
begin
	result :=  Editable;
    if ConMsg and (not result) then MsgBox(MSG_ERROR_DATOS_NO_EDITABLES, TextoCaption, MB_OK + MB_ICONSTOP);
end;

// Verifica si el tipo de contenedor para recepci�n de TAGs es correcto
function ValidarContenedor(TextoCaption: ANSIString; Contenedor: integer; ConMsg: Boolean=False): Boolean;
begin
	result := Contenedor <> -1;
    if ConMsg and (not result) then MsgBox(MSG_ERROR_EMBALAJE_INCORRECTO, TextoCaption, MB_OK + MB_ICONSTOP);
end;

// Verifica que la cantidad de TAGs indicados no sea nula y, eventualmenrte, no
// no exceda la cantidad disponible (TAGsLibres)
function ValidarCantidadTAGs (TextoCaption: ANSIString; Categoria: Integer; CantidadTAGS: LongInt; CompararDisponibles: Boolean; ConMsg: Boolean=False): Boolean;
begin
   	result := CantidadTAGs > 0;
    if not result then begin
    	if ConMsg then MsgBox(MSG_ERROR_CANTIDAD_ES_CERO, TextoCaption, MB_OK + MB_ICONSTOP);
        Exit
    end;

    if not CompararDisponibles then exit;

    result := CantidadTAGs <= TAGSLibres(Categoria);
    if not result then begin
    	if ConMsg then MsgBox(MSG_ERROR_CANTIDAD_DISPONIBLE + IntToStr(TAGSLibres(Categoria)), TextoCaption, MB_OK + MB_ICONSTOP);
        Exit
    end;
end;

// Verifica el rango de TAGs de un movimiento, considerando si �ste es nominal o no nominal
function ValidarRangosTAGs (TextoCaption: ANSIString; TAGInicial, TAGFinal: DWORD; Nominal: Boolean; ConMsg: Boolean=False):Boolean;
begin
	case Nominal of
    	true: begin
			result := TAGInicial > 0;
		    if ConMsg and (not Result) then begin
				MsgBox(Format(MSG_ERROR_NUMERO_TAG_INCORRECTO, ['Telev�a Inicial']), TextoCaption, MB_OK + MB_ICONSTOP);
        		exit
		    end;

			result := TAGFinal > 0;
	    	if ConMsg and (not Result) then begin
				MsgBox(Format(MSG_ERROR_NUMERO_TAG_INCORRECTO, ['Telev�a Final']), TextoCaption, MB_OK + MB_ICONSTOP);
    	    	exit
		    end;

			result := TAGInicial <= TAGFinal;
		    if not result then begin
    			if ConMsg then MsgBox(MSG_ERROR_RANGO_TAGS_MAYOR_MENOR, TextoCaption, MB_OK + MB_ICONSTOP);
		        Exit
		    end
	    end;

    	else begin
			result := (TAGInicial + TAGFinal) = 0;
		    if ConMsg and (not Result) then begin
				MsgBox(MSG_ERROR_RANGO_TAGS_NO_VALIDO, TextoCaption, MB_OK + MB_ICONSTOP);
        		exit
		    end;
    	end
	end
end;

{-----------------------------------------------------------------------------
  Function Name: NoPermitirMedioPAgoDuplicado
  Author:
  Date Created:  /  /
  Description:
  Parameters: Coneccion: TADOConnection; NumeroDocumento: String
  Return Value: Boolean

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function NoPermitirMedioPAgoDuplicado(Coneccion: TADOConnection; NumeroDocumento: String): Boolean;
begin
    //Result := Trim(QueryGetValue(Coneccion, format('SELECT NumeroDocumento FROM PersonasMedioPagoDuplicados  WITH (NOLOCK) WHERE NumeroDocumento = ''%s''',[Trim(NumeroDocumento)]))) = ''; //TASK_022_ECA_20160601
    Result :=False; //TASK_022_ECA_20160601
end;

function ObtenerConvenioMedioPago(Conn: TADOConnection;
                                  NumeroDocumento: String;
                                  CodigoConvenioEditado: Integer;
                                  TipoMedioPagoAutomatico: Integer;
                                  //CodigoTipoTarjetaCredito: Integer;                                  //SS_628_ALA_20110818
                                  //NumeroTarjetaCredito: String;                                       //SS_628_ALA_20110818
                                  //NroCuentaBancaria: String;                                          //SS_628_ALA_20110818
                                  //CodigoBanco: Integer;                                               //SS_628_ALA_20110818
                                  //CodigoTipoCuentaBancaria: Integer;                                  //SS_628_ALA_20110818
                                  ExcluirConvenioSinCuenta : Integer): Integer;                         //SS_628_ALA_20110720
var
    SP: TADOStoredProc;
begin
    Result := -1;
    SP := TADOStoredProc.Create(nil);
    try
        sp.Connection := Conn;
        sp.ProcedureName := 'VerificarMedioPagoAutomatico';
        with sp do begin
            Parameters.Refresh;
            
            Parameters.ParamByName('@NumeroDocumento').Value := NumeroDocumento;
            Parameters.ParamByName('@CodigoCovenio').Value := -1;
            Parameters.ParamByName('@CodigoConvenioEditado').Value := CodigoConvenioEditado;
            Parameters.ParamByName('@TipoMedioPagoAutomatico').Value := TipoMedioPagoAutomatico;
            //Parameters.ParamByName('@CodigoTipoTarjetaCredito').Value := CodigoTipoTarjetaCredito;    //SS_628_ALA_20110818
            //Parameters.ParamByName('@NumeroTarjetaCredito').Value := NumeroTarjetaCredito;            //SS_628_ALA_20110818
            //Parameters.ParamByName('@NroCuentaBancaria').Value := NroCuentaBancaria;                  //SS_628_ALA_20110818
            //Parameters.ParamByName('@CodigoBanco').Value := CodigoBanco;                              //SS_628_ALA_20110818
            //Parameters.ParamByName('@CodigoTipoCuentaBancaria').Value := CodigoTipoCuentaBancaria;    //SS_628_ALA_20110818
            Parameters.ParamByName('@ExcluirConvenioSinCuenta').Value := ExcluirConvenioSinCuenta;   //SS_628_ALA_20110720

            ExecProc;
            Result := Parameters.ParamByName('@CodigoCovenio').Value;
        end;
    except
          on e: Exception do MsgBoxErr('Error al Tratar de Verificar el Medio de Pago', e.Message, 'Error al ejecutar SP', MB_ICONSTOP);
    end;
    sp.Free;
end;

{-----------------------------------------------------------------------------
  Function Name : VerificarExisteConvenioSinCuenta
  Author        : Alejandro Labra (alabra)
  Date Created  : 20/07/2011
  Description   : Verifica si el Cliente ya posee un convenio sin Cuenta.
  Parameters    : Coneccion: TADOConnection; NumeroDocumento: String;
  Return Value  : Boolean
  Firma         : SS_628_ALA_20110720
-----------------------------------------------------------------------------}
function VerificarExisteConvenioSinCuenta(Conn: TADOConnection; NumeroDocumento: String; ExcluirEsteConvenio : Int64) : Boolean;                                //SS_628_ALA_20111219
var
    SP: TADOStoredProc;
begin
    Result := False;
    SP := TADOStoredProc.Create(nil);
    try
        sp.Connection := Conn;
        sp.ProcedureName := 'VerificarConvenioSinCuentaPorPersona';
        with sp do begin
            Parameters.Refresh;
            Parameters.ParamByName('@NumeroDocumento').Value := NumeroDocumento;
            Parameters.ParamByName('@ConvenioExcluir').Value := IIf(ExcluirEsteConvenio>0, ExcluirEsteConvenio, Null);                                          //SS_628_ALA_20111219
            Parameters.ParamByName('@CodigoCovenio').Value := -1;
            ExecProc;
            Result := Parameters.ParamByName('@CodigoCovenio').Value;
        end;
    except
          on e: Exception do MsgBoxErr('Error al Tratar de Verificar Convenio Sin Cuenta', e.Message, 'Error al ejecutar SP', MB_ICONSTOP);
    end;
    sp.Free;
end;

function ValidarUbicacionTelevia(Conn :TADOConnection; ContextMark: integer; SerialNumber: DWORD; CodigoPuntoEntrega: integer): boolean;
begin
    result := QueryGetValueInt(DMConnections.BaseCAC,'EXEC ValidarUbicacionTag' + '''' +
              inttostr(ContextMark) + ''',''' + intToStr(SerialNumber) + ''',''' + intToStr(CodigoPuntoEntrega) + ''''  ) = 1;
end;

function FormatearImporte(Conn: TADOConnection; Importe:Int64): String;
begin
	result := QueryGetValue(Conn, Format('SELECT dbo.FloatToStr(%d)', [Importe]));
end;

{******************************** function Header ******************************
function Name: FormatearImporteConDecimales
Author : jconcheyro
Date Created : 27/07/2006
Description : Devuelve el float de parametro como string con signo de moneda, decimales
y separador de miles si se rerquiere. Utiliza la funcion FloatToStrD de la base
Parameters : Conn: TADOConnection; Importe:Double; ConSimboloMoneda:boolean;
    Con2Decimales:boolean; ConSeparadorDeMiles:boolean
Return Value : String
*******************************************************************************}
function  FormatearImporteConDecimales(Conn: TADOConnection; Importe:Double; ConSimboloMoneda:boolean; Con2Decimales:boolean; ConSeparadorDeMiles:boolean):String;
var
    aConSimboloMoneda, aCon2Decimales, aConSeparadorDeMiles: integer;
    OldDecimalSeparator: char;
begin
    aConSimboloMoneda := 0 ;
    aCon2Decimales := 0 ;
    aConSeparadorDeMiles := 0 ;
    if ConSimboloMoneda     then aConSimboloMoneda := 1 ;
    if Con2Decimales        then aCon2Decimales := 1 ;
    if ConSeparadorDeMiles  then aConSeparadorDeMiles := 1;

    OldDecimalSeparator:= DecimalSeparator;

    DecimalSeparator := '.'; // es la unica forma de asegurarme que a la base le llega con . decimal
    Result := QueryGetValue(Conn, Format('SELECT dbo.FloatToStrD(%f,%d,%d,%d)', [Importe,aConSimboloMoneda,aCon2Decimales,aConSeparadorDeMiles]));
    DecimalSeparator := OldDecimalSeparator;

end;

{******************************** function Header ******************************
function Name: RedondearINTConCentavosHaciaArribaBase
Author : jconcheyro
Date Created : 21/12/2006
Description : Para unificar el redondeo de decimales que no pueden existir en chile
el redondeo se hace con la base de datos.
Parameters : Conn: TADOConnection; Importe: Int64
Return Value : Int64
*******************************************************************************}
function RedondearINTConCentavosHaciaArribaBase( Conn: TADOConnection; Importe:Int64): Int64;
begin
    Result := QueryGetValueINT(Conn, Format('SELECT dbo.RedondearINTConCentavosHaciaArriba(%d)', [Importe]));
end;

{******************************** function Header ******************************
function Name: RedondearINTConCentavosHaciaAbajoBase
Author : jconcheyro
Date Created : 21/12/2006
Description : Para unificar el redondeo de decimales que no pueden existir en chile
el redondeo se hace con la base de datos.
Parameters : Conn: TADOConnection; Importe: Int64
Return Value : Int64
*******************************************************************************}
function RedondearINTConCentavosHaciaAbajoBase( Conn: TADOConnection; Importe:Int64): Int64;
begin
    Result := QueryGetValueINT(Conn, Format('SELECT dbo.RedondearINTConCentavosHaciaAbajo(%d)', [Importe]));
end;








{******************************** function Header ******************************
function Name: ImporteStrToInt
Author : ggomez
Date Created : 10/01/2006
Description : Dado una cadena con un importe en le formato que lo retorna la FN
    dbo.FlostToStr, retorna el importe como n�mero.
Parameters : Conn: TADOConnection; Importe: AnsiString
Return Value : Integer
*******************************************************************************}
function  ImporteStrToInt(Conn: TADOConnection; Importe: AnsiString): Int64;
var
    Imp: Int64;
begin
    Imp := QueryGetValueInt(Conn, Format('SELECT dbo.StrToFloat(%s)', [QuotedStr(Importe)]));
	result := Imp;
end;

{-----------------------------------------------------------------------------
  Procedure: EsDiaFeriado
  Author:    ggomez
  Date:      01-Feb-2005
  Arguments: Conn: TADOConnection; Fecha: TDateTime, Descripcion: AnsiString
  Result:    Boolean
  Description: Retorna True si la fecha pasada como par�metro es un d�a feriado,
    si no es un d�a feriado, retorna False.
    Si la fecha es feriado en Descripcion deja la descripci�n del d�a.
-----------------------------------------------------------------------------}
function EsDiaFeriado(Conn: TADOConnection; Fecha: TDateTime; var Descripcion: AnsiString): Boolean;
resourcestring
    MSG_ERROR = 'Error';
    CAPTION_TITULO = 'Error';
var
	SP: TADOStoredProc;
begin
    SP := TADOStoredProc.Create(nil);
    try
        try
            SP.Connection := Conn;
            SP.ProcedureName := 'FechaEsDiaFeriado';
            SP.Parameters.Refresh;
            Descripcion := EmptyStr;
            SP.Parameters.ParamByName('@Fecha').Value := Fecha;
            SP.Parameters.ParamByName('@EsFeriado').Value := NULL;
            SP.Parameters.ParamByName('@Descripcion').Value := NULL;
            SP.ExecProc;
            Result := (SP.Parameters.ParamByName('@EsFeriado').Value <> 0);
            if Result then
                Descripcion := SP.Parameters.ParamByName('@Descripcion').Value;
        except
            on E: Exception do begin
                Result := False;
                MsgBoxErr(MSG_ERROR, E.Message, CAPTION_TITULO, MB_ICONERROR);
            end;
        end;
    finally
        SP.Free;
    end;
end;

{-----------------------------------------------------------------------------
  Procedure: EsDiaHabil
  Author:    ggomez
  Date:      27-Ene-2005
  Arguments: Conn: TADOConnection; Fecha: TDateTime
  Result:    Boolean
  Description: Retorna True si la fecha pasada como par�metro es un d�a h�bil,
    si no es un d�a h�bil, retorna False.
-----------------------------------------------------------------------------}
function EsDiaHabil(Conn: TADOConnection; Fecha: TDateTime): Boolean;
var
    Descrip: AnsiString;
begin
    // Si NO es Domingo ni S�bado, es d�a h�bil.
    Result := not EsDomingoOSabado(Fecha);

    // Si no es Domingo ni S�bado, verifico si es feriado.
    if Result then
        Result := not EsDiaFeriado(Conn, Fecha, Descrip);
end;

{-----------------------------------------------------------------------------
  Procedure: EsDomingoOSabado
  Author:    ggomez
  Date:      01-Feb-2005
  Arguments: Fecha: TDateTime
  Result:    Boolean
  Description: Retorna True si la fecha pasada como par�metro es un d�a Domingo
    o es un d�a S�bado,
    sino retorna False.
-----------------------------------------------------------------------------}
function EsDomingoOSabado(Fecha: TDateTime): Boolean;
begin
    Result := ((DayOfTheWeek(Fecha) = DaySaturday)
                or (DayOfTheWeek(Fecha) = DaySunday));
end;

{-----------------------------------------------------------------------------
  function Name: ConsultarPatenteRNVM
  Author:    gcasais
  Date Created: 12/04/2005
  Description: Consulta al RNVM
  Parameters: Pregunta: TRNVMQuery; var Respuesta: TRVMInformation
  Return Value: Boolean
-----------------------------------------------------------------------------}
function ConsultarPatenteRNVM(Pregunta: TRNVMQuery; var Respuesta: TRVMInformation; var ExistePatente:Boolean; var DescriError:String; var RespuestaRNVM: WideString): Boolean;
var
    Client: TRVMClient;
    TextoError: String;
    TextoRNVM: WideString;
begin
    ExistePatente := False;
    Client := TRVMClient.Create;
    Client.Username := Pregunta.Parameters.UserName;
    Client.Password := Pregunta.Parameters.PassWord;
    Client.ServiceID := Pregunta.Parameters.ServiceID;
    Client.ServiceURL := Pregunta.Parameters.ServiceURL;
    Client.ServicePort := Pregunta.Parameters.ServicePort;
    Client.SaveDirectory := Pregunta.Parameters.SaveDirectory;
    Client.FileName := Pregunta.Parameters.FileName;
    Client.RequestUseProxy := Pregunta.Parameters.RequestUseProxy;
    if Client.RequestUseProxy then begin
        Client.RequestProxyServer := Pregunta.Parameters.RequestProxyServer;
        Client.RequestProxyServerPort := Pregunta.Parameters.RequestProxyServerPort;
    end;
    try
        Result := Client.RequestLPData(Pregunta.LicensePlate, Respuesta, TextoError, TextoRNVM);
        ExistePatente := Client.LicensePlateExists;
        DescriError := TextoError;
        RespuestaRNVM := TextoRNVM;
    finally
        FreeAndNil(Client);
    end;
end;

{******************************** function Header ******************************
function Name: LevantarLogo
Author       : rcastro
Date Created : 22/04/2005
Description  : Busca el archivo con el logo para el recibo y si lo encuentra
				setea la imagen del logo con el archivo encontrado.
Parameters   : var Logo: TBitmap
Return Value : Boolean
*******************************************************************************}
function LevantarLogo (var Logo: TBitmap): Boolean;
var
	FDirLogoRecibo,
	FNombreLogoRecibo: AnsiString;
    FUsaLogo: Integer;
begin
	Result := false;
	FDirLogoRecibo := EmptyStr;
	FNombreLogoRecibo := EmptyStr;

    FUsaLogo := 0; //por defecto que NO use logo...

    ObtenerParametroGeneral(DMConnections.BaseCAC, 'UTILIZAR_LOGO_RECIBO', FUsaLogo);
    if FUsaLogo <> 1 then begin
        Logo := nil;
        Result := True;
        Exit;
    end;

	ObtenerParametroGeneral(DMConnections.BaseCAC, 'DIR_LOGONEGRO_RECIBO', FDirLogoRecibo);
	ObtenerParametroGeneral(DMConnections.BaseCAC, 'NOMBRE_LOGONEGRO_RECIBO', FNombreLogoRecibo);

    if TRIM(FNombreLogoRecibo) = '' then begin
        Logo := nil;
        Result := True;
        Exit;
    end;
	FNombreLogoRecibo := GoodDir(FDirLogoRecibo) + FNombreLogoRecibo;

	if FileExists(FNombreLogoRecibo) then begin
		Logo.LoadFromFile(FNombreLogoRecibo);

		Result := true
	end else begin
		Logo := nil;
	end;
end;

// Comunicaciones con DDE
function EnComunicacion: Boolean;
begin
    Result := (GCodigoComunicacion > 0);
end;

function IniciarComunicacion(NumeroDocumento: AnsiString; var CodigoComunicacion: Integer; var MsgError: String): Boolean;
begin
    Result := False;
    CodigoComunicacion := 0;
    MsgError := '';
    try
        DMComunicaciones.spAgregarLogComunicacioes.Parameters.ParamByName('@NumeroDocumento').Value := NumeroDocumento;
        DMComunicaciones.spAgregarLogComunicacioes.Parameters.ParamByName('@CodUsuario').Value := UsuarioSistema;
        DMComunicaciones.spAgregarLogComunicacioes.ExecProc;
        CodigoComunicacion := DMComunicaciones.spAgregarLogComunicacioes.Parameters.ParamByName('@CodComunicacion').Value;
        Result := True;
    except
        on e: Exception do begin
            GCodigoComunicacion := 0;
            MsgError:= e.Message;
        end;
    end;
end;

function TerminarComunicacion(CodigoComunicacion: Integer; var MsgError: String):Boolean;
resourcestring
    MSG_ERROR = 'No se puede finalizar una comunicaci�n si no hay una comunicaci�n en curso';
begin
    Result := False;
    if CodigoComunicacion <= 0 then begin
        raise Exception.Create(MSG_ERROR);
        Exit;
    end;
    DMComunicaciones.spCierraLogComunicaciones.Parameters.ParamByName('@CodComunicacion').Value := CodigoComunicacion;
    try
        try
            DMComunicaciones.spCierraLogComunicaciones.ExecProc;
            Result := True;
        except
            on e: Exception do begin
                MsgError := e.Message;
            end;
        end;
    finally
        GCodigoComunicacion := 0;
    end;
end;

procedure ValidarResultadoConsultaRNMVM (Respuesta: TRVMInformation; ExistePatente: Boolean; var Resultado: TRVMQueryResult; var DatosInfraccion: TEnforcementData);
const
    UNKNOWN_DOOR_NUMBER = 'S/N';
begin
    // Validamos una consulta al RNVM y preparamos los datos para completar una infracci�n
    FillChar(DatosInfraccion, SizeOf(TEnforcementData), 0);
    // Primero asumimos que el RNVM no tiene la patente que buscamos
    Resultado := qrRNVMUnknown;
    if not ExistePatente then Exit;
    // A partir de ac� si salimos asumimos que la respuesta es incompleta como para armar una infracci�n
    Resultado := qrRNVMIncomplete;
    //Si no tenemos nada de informacion del titular salimos de una
    if (Trim(Respuesta.OwnerInformation.OwnerID1) = '') and (Trim(Respuesta.OwnerInformation.OwnerName1) = '')
      and (Trim(Respuesta.OwnerInformationCOM.Rut1) = '') and (Trim(Respuesta.OwnerInformationCOM.Nombre1) = '') then exit;
    // Si no tenemos ni calle, ni numero de puerta, ni comuna, ni categoria del veh�culo tambien la damos por incompleta de una
    if (Trim(Respuesta.AddressInformation.Street) = '') and (Trim(Respuesta.AddressInformation.DoorNumber) = '') and
      (Trim(Respuesta.AddressInformation.Commune) = '') and (Trim(Respuesta.VehicleInformation.Category) = '') then exit;
    // Ahora basta con que tengamos la calle y la comuna decimos que est� OK, sino seguimos como incompleta
    if (Trim(Respuesta.AddressInformation.Street) <> '') and (Trim(Respuesta.AddressInformation.Commune) <> '') then  Resultado := qrRNVMOk;
    // Vemos que tenemos que informar como titular.
    // S� la estructura "AMO" que para nosotros se
    // llama OwnerInformation y es la principal est� totalmente vac�a usamos
    // lo que haya venido en el AMO-COM y siempre nos quedamos con el primero
    // aunque vengan m�s de uno
    if (Trim(Respuesta.OwnerInformation.OwnerID1) = '') and
       (Trim(Respuesta.OwnerInformation.OwnerName1) = '') then begin
        DatosInfraccion.Owner.ID        := Respuesta.OwnerInformationCOM.Rut1;
        DatosInfraccion.Owner.FullName  := Respuesta.OwnerInformationCOM.Nombre1;
    end else begin
       DatosInfraccion.Owner.ID         := Respuesta.OwnerInformation.OwnerID1;
       DatosInfraccion.Owner.FullName   := Respuesta.OwnerInformation.OwnerName1;
    end;
    // Completamos los ultimos datos del propietario
    DatosInfraccion.Owner.Street        := Respuesta.AddressInformation.Street;
    // Si el numero de la puerta vino vac�o le ponemos la constante "S/N".
    if Trim(Respuesta.AddressInformation.DoorNumber) = '' then begin
        DatosInfraccion.Owner.DoorNumber := UNKNOWN_DOOR_NUMBER;
    end else begin
        DatosInfraccion.Owner.DoorNumber    := Respuesta.AddressInformation.DoorNumber;
    end;
    DatosInfraccion.Owner.Commune := Respuesta.AddressInformation.Commune;
    DatosInfraccion.Owner.Details := Respuesta.AddressInformation.OtherInfo;
    DatosInfraccion.Registration.AcquisitionDate := Respuesta.OwnerRegistration.AcquisitionDate;
    //Completamos los datos del veh�culo
    DatosInfraccion.Vehicle.Category        := Respuesta.VehicleInformation.Category;
    DatosInfraccion.Vehicle.Manufacturer    := Respuesta.VehicleInformation.Manufacturer;
    DatosInfraccion.Vehicle.Model           := Respuesta.VehicleInformation.Model;
    DatosInfraccion.Vehicle.EngineID        := Respuesta.VehicleInformation.EngineID;
    DatosInfraccion.Vehicle.ChassisID       := Respuesta.VehicleInformation.ChassisID;
    DatosInfraccion.Vehicle.Serial          := Respuesta.VehicleInformation.Serial;
    DatosInfraccion.Vehicle.VINNumber       := Respuesta.VehicleInformation.VINNumber;
    DatosInfraccion.Vehicle.Year            := Respuesta.VehicleInformation.Year;
    // Listo!!!!!!
end;

{-----------------------------------------------------------------------------
  Procedure Name: PermitirSoloDigitos
  Author:    ddiaz
  Date Created: 25/08/2005
  Description: Devuelve false si en el par�metro TEXTO existe algun caracter no num�rico
  Parameters: Texto a analizar
  Return Value: Boolean
-----------------------------------------------------------------------------}

function PermitirSoloDigitos(Texto: string): boolean;
var i: integer;
begin
    Result:=False;
    for i:=1 to Length(Texto) do
        if not (Texto[i] in ['0'..'9']) then exit;

    Result:=True;
end;

{-----------------------------------------------------------------------------
  Procedure Name: OrdenarColumna
  Author:    ddiaz
  Date Created: 02/09/2005
  Description: Establece orden ASC/DESC en cualquier columna de cualquier grilla
  Parameters: Grilla, columna y nombre del campo de la columna
  Return Value: None

      Revisi�n 1:
        Author:    ddiaz
        Date:      17-Mar-2006
        Se agrega par�metro opcional para mantener o no el orden ya establecido en la columna.
-----------------------------------------------------------------------------}
procedure OrdenarGridPorColumna(Grilla: TCustomDBListEx; Columna: TDBListExColumn; NombreCampo: string; NoCambiarOrden: boolean=False);
begin
    if not NoCambiarOrden then begin
        if Columna.Sorting = csAscending then
            Columna.Sorting := csDescending
        else
            Columna.Sorting := csAscending;
	// SS_637_20100929
        if Grilla.DataSource.DataSet is TCustomADODataSet then
            TCustomADODataSet(Grilla.DataSource.DataSet).Sort := NombreCampo + iif(Columna.Sorting = csAscending, ' ASC' ,' DESC');
        if Grilla.DataSource.DataSet is TClientDataSet then
            TClientDataSet(Grilla.DataSource.DataSet).IndexFieldNames := NombreCampo;
    end
    else begin
        Columna.Sorting := csAscending;    
        if Grilla.DataSource.DataSet is TCustomADODataSet then
            TCustomADODataSet(Grilla.DataSource.DataSet).Sort := NombreCampo;
        if Grilla.DataSource.DataSet is TClientDataSet then
            TClientDataSet(Grilla.DataSource.DataSet).IndexFieldNames := NombreCampo;
    end;
	// Fin Rev. SS_637_20100929
end;

function ChequearConvenioEnCarpetaLegal(CodigoConvenio: integer): Boolean;
begin
    Result := False;
    if QueryGetValue(DMConnections.BaseCAC, format('SELECT DBO.ConvenioEstaEnCarpetaLegal(%d)',[CodigoConvenio])) = 'True' then
      Result :=True;
end;

{******************************** Function Header ******************************
Function Name: ObtenerEmpresaConvenioEnCarpetaLegal
Author : vpaszkowicz
Date Created : 27/02/2009
Description : Retorna el nombre de la empresa a la que fue asignado el convenio
en carpeta legal.
Parameters : CodigoConvenio: integer
Return Value : string
*******************************************************************************}
function ObtenerEmpresaConvenioEnCarpetaLegal(CodigoConvenio: integer): string;
begin
    Result := '';
    Result := Trim(QueryGetValue(DMConnections.BaseCAC, format('SELECT DBO.ConvenioEstaEnCarpetaLegalEmpresaNombre(%d)',[CodigoConvenio])));
end;


{********************************** File Header ********************************
Procedure Name  : CargarConveniosEnCombo
    Author      : ddiaz
    Date Created: 20/07/2006
    Description : Carga los convenios en el cmb pasado como par�metro. Se utiliza en Carpetas Morosos - App Facturaci�n
--------------------------------------------------------------------------------}
procedure CargarConveniosEnCombo(Conn: TADOConnection; Combo: TVariantComboBox; FCodigoPersona: integer; CodigoConvenio: Integer = 0);
Var
	i: Integer;
	SP: TADOStoredProc;
begin
	SP := TADOStoredProc.Create(nil);
	try
		SP.Connection := Conn;
        sp.ProcedureName:='ObtenerConveniosCliente';
        with sp.Parameters do begin
            Refresh;
            ParamByName('@CodigoCliente').Value := FCodigoPersona;
        end;
        sp.Open;
		Combo.Clear;

        Combo.Items.Add(SIN_ESPECIFICAR, 0);
        i := 0;

		While not SP.Eof do begin
			Combo.Items.Add(SP.FieldByName('NumeroConvenioFormateado').AsString, SP.FieldByName('CodigoConvenio').AsString);
			if SP.FieldByName('CodigoConvenio').AsInteger = CodigoConvenio then begin
            	i := Combo.Items.Count - 1;
            end;
			SP.Next;
		end;
		if (i <> -1) then Combo.ItemIndex := i
        else Combo.ItemIndex := 0;
	finally
		SP.Close;
		SP.Free;
	end;
end;

{********************************** File Header ********************************
Procedure Name  : ObtenerNumeroSiguienteComprobante
    Author      : jconcheyro
    Date Created: 12/08/2006
    Description : Obtiene el pr�ximo n�mero de comprobante a usar de un tipo especifico
Revision 1:
    Author: mlopez
    Date: 15/08/2006
    Description: Agregue el segmento try...except y el Refresh
--------------------------------------------------------------------------------}
function ObtenerNumeroSiguienteComprobante(Conn: TADOConnection; TipoComprobante: string): integer ;
resourcestring
    MSG_TITLE = 'N�mero del Siguiente Comprobante';
    MSG_ERROR_GETTING_NEXT_NUMBER = 'Ha ocurrido un error obteniendo pr�ximo n�mero de comprobante.' ;
var
    SP: TAdoStoredProc;
    NumeroComprobante: integer;
begin
    Result := 0;
    NumeroComprobante := 0 ;
    SP := TAdoStoredProc.Create(nil);
    try
        try
            SP.Connection := Conn;
            SP.ProcedureName := 'ObtenerNumeroSiguienteComprobante';
            SP.Parameters.Refresh;
            SP.Parameters.ParamByName('@TipoComprobante').Value := TipoComprobante;
            SP.Parameters.ParamByName('@NumeroComprobante').Value := NumeroComprobante;
            SP.ExecProc;
            Result := SP.Parameters.ParamByName('@NumeroComprobante').Value ;
            SP.Close;
        except
            on e: Exception do begin
                raise Exception.Create(MSG_ERROR_GETTING_NEXT_NUMBER + CRLF + E.Message);
            end;
        end;
    finally
        SP.Free;
    end;
end;

{******************************** Function Header ******************************
Function Name: CargarCategoriasInformes
Author : ggomez
Date Created : 24/06/2006
Description : Agrega, en el item de menu que se pasa como par�metro,
    los items de menu de las categor�as de reportes
Parameters : Conn: TADOConnection;
    ItemMenu: TMenuItem: Item de men� desde el cual "colgar" los submenues.
Return Value : None
Revision :
    Author : vpaszkowicz
    Date : 05/09/2006
    Description : Adaptado a la estructura de tablas del CAC
*******************************************************************************}
procedure CargarCategoriasInformes(Conn: TADOConnection; ItemMenu: TMenuItem);

    {INICIO:TASK_028_JMA_20160628
    procedure CargarSubCategoriasInformes(CodigoCategoria: Integer; ItemMenu: TMenuItem; Qry: TADOQuery);
    var
        RecNro: Integer;
        NewItem: TCategoriaReportItem;
    begin
        // Almacenar el registro en el que est� el query.
        RecNro := Qry.RecNo;

        // Recorrer las categor�as para encontrar las "Hijas" de la categor�a pasada como par�metro.
        Qry.First;
        while not Qry.Eof do begin
            // Si la categor�a a tratar es "hija" de la que se pas� como par�metro, entonces
            // agregar el menu.
            if CodigoCategoria = Qry.FieldByName('CodigoCategoriaPadre').AsInteger then begin

                NewItem := TCategoriaReportItem.Create(ItemMenu, Qry.FieldByName('CodigoCategoria').AsInteger);
                NewItem.Caption := Qry.FieldByName('Descripcion').AsString;
                NewItem.Name:='__sub__'+Qry.FieldByName('CodigoCategoria').AsString;                            //SS_1437_NDR_20151230
                ItemMenu.Add(NewItem);

                // Llamada recursiva para agregar los submenues
                CargarSubCategoriasInformes(Qry.FieldByName('CodigoCategoria').AsInteger, NewItem, Qry);
            end;
            Qry.Next;
        end;
        // Restaurar el registro en el que estaba el query.
        Qry.RecNo := RecNro;
    end;
    }
     procedure CargarSubCategoriasInformes(CodigoCategoria: Integer; ItemMenu: TMenuItem; sp: TADOStoredProc);
    var
        RecNro: Integer;
        NewItem: TCategoriaReportItem;
    begin
        // Almacenar el registro en el que est� el query.
        RecNro := sp.RecNo;

        // Recorrer las categor�as para encontrar las "Hijas" de la categor�a pasada como par�metro.
        sp.First;
        while not sp.Eof do begin
            // Si la categor�a a tratar es "hija" de la que se pas� como par�metro, entonces
            // agregar el menu.
            if CodigoCategoria = sp.FieldByName('CodigoCategoriaPadre').AsInteger then begin

                NewItem := TCategoriaReportItem.Create(ItemMenu, sp.FieldByName('CodigoCategoria').AsInteger);
                NewItem.Caption := sp.FieldByName('Descripcion').AsString;
                NewItem.Name:='__sub__' + sp.FieldByName('CodigoCategoria').AsString;
                ItemMenu.Add(NewItem);

                // Llamada recursiva para agregar los submenues
                CargarSubCategoriasInformes(sp.FieldByName('CodigoCategoria').AsInteger, NewItem, sp);
            end;
            sp.Next;
        end;
        // Restaurar el registro en el que estaba el query.
        sp.RecNo := RecNro;
    end;
    {TERMINO:TASK_028_JMA_20160628}
var
{INICIO:TASK_028_JMA_20160628
	QryCategorias: TADOQuery;
TERMINO:TASK_028_JMA_20160628}
    spCategorias: TADOStoredProc;
    NewItem: TCategoriaReportItem;
    begin
    //Creo todos items de categor�as de reportes
{INICIO:TASK_028_JMA_20160628
	QryCategorias := TADOQuery.Create(nil);
	try
		QryCategorias.Connection := Conn;

        QryCategorias.Close;
        //QryCategorias.SQL.Text := 'SELECT CodigoCategoria, Nombre, CodigoCategoriaPadre, Nivel'
        QryCategorias.SQL.Text := 'SELECT CodigoCategoria, Descripcion, CodigoCategoriaPadre'
            + ' FROM CategoriasConsultas WITH (NOLOCK)'
            + ' ORDER BY Descripcion';

        QryCategorias.Open;
        QryCategorias.First;
        while not QryCategorias.Eof do begin

            // Si la Categor�a es Padre de otra, entonces agregarlo como submen�.
            if QryCategorias.FieldByName('CodigoCategoriaPadre').AsInteger = 0 then begin

                NewItem := TCategoriaReportItem.Create(ItemMenu, QryCategorias.FieldByName('CodigoCategoria').AsInteger);
                NewItem.Caption := QryCategorias.FieldByName('Descripcion').AsString;
                NewItem.Name := '__cat__'+QryCategorias.FieldByName('CodigoCategoria').AsString;                        //SS_1437_NDR_20151230
                ItemMenu.Add(NewItem);

                // Llamada recursiva para agregar los submenues de las categor�as.
                CargarSubCategoriasInformes(QryCategorias.FieldByName('CodigoCategoria').AsInteger, NewItem, QryCategorias);
            end; // if
            QryCategorias.Next;
        end;
    finally
        QryCategorias.Free;
    end;
}
    spCategorias := TADOStoredProc.Create(nil);
	try
		spCategorias.Connection := Conn;
        spCategorias.ProcedureName := 'ObtenerCategoriasConsultas';
        spCategorias.Parameters.Refresh;

        spCategorias.Open;
        if spCategorias.RecordCount > 0 then
        begin
            spCategorias.First;
            while not spCategorias.Eof do begin

                // Si la Categor�a es Padre de otra, entonces agregarlo como submen�.
                if spCategorias.FieldByName('CodigoCategoriaPadre').AsInteger = 0 then begin

                    NewItem := TCategoriaReportItem.Create(ItemMenu, spCategorias.FieldByName('CodigoCategoria').AsInteger);
                    NewItem.Caption := spCategorias.FieldByName('Descripcion').AsString;
                    NewItem.Name := '__cat__' + spCategorias.FieldByName('CodigoCategoria').AsString;                        
                    ItemMenu.Add(NewItem);

                    // Llamada recursiva para agregar los submenues de las categor�as.
                    CargarSubCategoriasInformes(spCategorias.FieldByName('CodigoCategoria').AsInteger, NewItem, spCategorias);
                end; // if
                spCategorias.Next;
            end;
        end;
    finally
        if spCategorias.State = dsBrowse then
            spCategorias.Close;
        spCategorias.Free;
    end;
{TERMINO:TASK_028_JMA_20160628}
end;

{******************************** Function Header ******************************
Function Name: QuitarCategoriasSinInformes
Author : ggomez
Date Created : 25/06/2006
Description : Dado un item de menu, borra todos los items de Categor�as de Reportes
    que no tienen items hijos.
    Este m�todo es recursivo.
Parameters : ItemMenu: TMenuItem
Return Value : None
Revision :
    Author : vpaszkowicz
    Date : 07/09/2006
    Description : Adaptado a la estructura de tablas del CAC
*******************************************************************************}
procedure QuitarCategoriasSinInformes(ItemMenu: TMenuItem);
var
    i: Integer;
begin
    i := 0;
    // Mientras existan items, procesar.
    while (i < ItemMenu.Count) do begin
        if ItemMenu.Items[i].ClassType = TCategoriaReportItem then begin
            // Llamada recursiva para quitar los hijos
            QuitarCategoriasSinInformes(ItemMenu.Items[i]);
            // Si despu�s de quitar los hijos, el item qued� sin hijos, entonces borrar el item.
            if ItemMenu.Items[i].Count <= 0 then begin
                // El item no tiene items hijos, entonces borrarlo.
                ItemMenu.Delete(i);
                // Decrementar el cintador pues se acaba de eliminar un item.
                i := i - 1;
            end;
        end;
        i := i + 1;
    end; // while
end;

{******************************** Function Header ******************************
Function Name: QuitarCategoriasSinInformes
Author : ggomez
Date Created : 25/06/2006
Description : Quitar los items de menu de categor�as que no tienen hijos.
Parameters : Menu: TMenu
Return Value : None
Revision :
    Author : vpaszkowicz
    Date : 05/09/2006
    Description : Adaptado a la estructura de tablas del CAC
*******************************************************************************}
procedure QuitarCategoriasSinInformes(Menu: TMenu);
var
    i: Integer;
begin
    i:= 0;
    while (i <= Menu.Items.Count - 1) do begin
        QuitarCategoriasSinInformes(Menu.Items[i]);
        i := i + 1;
    end;

end;
{BLOQUE SS_979_ALA_20110929}

{INICIO:TASK_028_JMA_20160628
procedure CargarInformes(Conn: TADOConnection; CodigoSistema: Integer; Menu: TMenu);
begin
    CargarInformes(Conn, CodigoSistema, Menu, Conn);
end;
TERMINO:TASK_028_JMA_20160628}

{******************************** Function Header ******************************
Function Name: CargarInformes
Author : ggomex
Date Created : 07/09/2006
Description :
Parameters : Conn: TADOConnection; CodigoSistema: Integer; Menu: TMenu; ConnInformes: TADOConnection
Return Value : None
Revision : 1
    Author : vpaszkowicz
    Date : 07/09/2006
    Description : Adaptado a la estructura de tablas del CAC

Revision : 2
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura
*******************************************************************************}

{INICIO:TASK_028_JMA_20160628
procedure CargarInformes(Conn: TADOConnection; CodigoAplicacion: Integer; Menu: TMenu; ConnInformes: TADOConnection);
TERMINO:TASK_028_JMA_20160628}
procedure CargarInformes(Conn: TADOConnection; CodigoAplicacion: Integer; Menu: TMenu);

    function ObtenerItemMenuDeCategoria(CodigoCategoria: Integer; ItemMenu: TMenuItem): TCategoriaReportItem;
    var
        i: Integer;
    begin
        Result := Nil;
        i := 0;
        // Mientras no se terminen los items y no encuentre el item de la categor�a, entonces seguir buscando.
        while (i < ItemMenu.Count) and (Result = Nil) do begin
            // Si el item es uno de los de Categor�as de Reportes.
            if ItemMenu.Items[i].ClassType = TCategoriaReportItem then begin
                if TCategoriaReportItem(ItemMenu.Items[i]).Codigo = CodigoCategoria then begin
                    // Si el c�digo del item es el de la categor�a que se busca, retornar el item.
                    Result := TCategoriaReportItem(ItemMenu.Items[i]);
                end else begin
                    // Sino seguir buscando en los hijos del item
                    Result := ObtenerItemMenuDeCategoria(CodigoCategoria, ItemMenu.Items[i]);
                end;
            end;
            i := i + 1;
        end; // while
    end;

    procedure QuitarItemsMenu(Menu: TMenu);
    var
        i, j: Integer;
        NewItem: TMenuItem;
    begin
        i:= 0;
        while (i <= Menu.Items.Count - 1) do begin
            j := 0;
            NewItem := Menu.Items[i];
            while (i <= Menu.Items.Count - 1) and (j <= NewItem.Count - 1) do begin
                if (Menu.Items[i].Items[j].ClassName = 'TReportItem')
                        or (Menu.Items[i].Items[j].ClassName = 'TCategoriaReportItem') then begin
                    if (j > 1) and (Menu.Items[i].Items[j-1].Caption = '-') then begin
                        Menu.Items[i].Delete(j-1);
                        j := j - 1;
                    end;
                    Menu.Items[i].Delete(j);
                    NewItem:= Menu.Items[i];
                    j := j - 1;
                end;
                j := j + 1;
            end;
            i := i + 1;
        end;
    end;

var
{INICIO:TASK_028_JMA_20160628
	QryInformes: TADOQuery;
}
    spInformes: TADOStoredProc;
{TERMINO:TASK_028_JMA_20160628}
    i: integer;
    ItemInforme: TReportItem;
    ItemCategoria: TCategoriaReportItem;
    FuncionAnterior: String;
begin
    //Primero borro todos los Items de reportes
    QuitarItemsMenu(Menu);
{INICIO:TASK_028_JMA_20160628
	QryInformes := TAdoQuery.Create(nil);

	try
        QryInformes.Close;
        QryInformes.Connection := Conn;
        QryInformes.SQL.Text := 'SELECT Consultas.CodigoConsulta, Funcion, Consultas.CodigoCategoria, Consultas.Descripcion, Detalle'
            + ' FROM Consultas WITH (NOLOCK) '
            + 'INNER JOIN ConsultasFuncionesSistemas  WITH (NOLOCK) ON ConsultasFuncionesSistemas.CodigoConsulta = Consultas.CodigoConsulta '
            + 'INNER JOIN CategoriasConsultas  WITH (NOLOCK) ON CategoriasConsultas.CodigoCategoria = Consultas.CodigoCategoria '
            + ' WHERE CodigoSistema = ' + IntToStr(CodigoSistema)
            + ' AND  Consultas.CodigoCategoria <>25 ' //TASK_009_ECA_20160506
            + ' AND  Funcion <>''Infractores1'' '     //TASK_035_ECA_20160620
            + ' ORDER BY Funcion, Consultas.Descripcion';
//             + ' and ConsultasFuncionesSistemas.CodigoConsulta = Consultas.CodigoConsulta ORDER BY funcion, Descripcion';

        QryInformes.Open;
        QryInformes.First;
        FuncionAnterior := '';
        while not QryInformes.Eof do begin
            i:= 0;
            // Mientras no se terminen los items del menu principal y
            // el nombre del menu sea distinto al nombre de la funci�n, Incrementar el contador
            // hasta encontrar el item de menu desde el cual "colgar" los reportes.
			while (i <= Menu.Items.Count - 1)
                    and (UpperCase(Trim(Menu.Items[i].Name)) <> UpperCase(Trim(QryInformes.FieldByName('Funcion').AsString))) do begin
                i:= i + 1;
            end; // while


            // Si a�n quedan items por tratar
            if (i <= Menu.Items.Count - 1) then begin
                // Si se cambio de Funci�n y hay items de men�, entonces Crear un item de men� de separaci�n.
                if (FuncionAnterior <> UpperCase(Trim(QryInformes.FieldByName('Funcion').AsString))) then begin
                    // Cargar los items de menu de las categor�as
                    CargarCategoriasInformes(Conn, Menu.Items[i]);
                end;

                // Almacenar la Funci�n que se acaba de tratar.
				FuncionAnterior := UpperCase(Trim(QryInformes.FieldByName('Funcion').AsString));

                // Obtener el item de categor�a del que debe colgar el informe.
                ItemCategoria := ObtenerItemMenuDeCategoria(QryInformes.FieldByName('CodigoCategoria').AsInteger, Menu.Items[i]);

                // Crear un item de men� para el reporte.
                ItemInforme := TReportItem.Create(ItemCategoria);
                ItemInforme.Name:='__inf__'+Trim(QryInformes.FieldByName('CodigoConsulta').AsString);                           //SS_1437_NDR_20151230
                ItemInforme.Caption := Trim(QryInformes.FieldByName('Descripcion').AsString);
                ItemInforme.Codigo := QryInformes.FieldByName('CodigoConsulta').AsInteger;
                ItemInforme.Hint := iif(QryInformes.FieldByName('Detalle').IsNull, '', QryInformes.FieldByName('Detalle').Value);
                //ItemInforme.Connection := ConnInformes;                       //SS_979_ALA_20110929
                ItemInforme.ConnectionDatosBase := Conn;                        //PAR00133_COPAMB_ALA_20110801
                ItemCategoria.Add(ItemInforme);
            end;

            QryInformes.Next;
        end;
    finally
		QryInformes.Free;
	end;
}
    spInformes := TADOStoredProc.Create(nil);
	try

        spInformes.Connection := Conn;
        spInformes.ProcedureName := 'ObtenerInformes';
        spInformes.Parameters.Refresh;
        spInformes.Parameters.ParamByName('@CodigoAplicacion').Value := CodigoAplicacion;

        spInformes.Open;
        if spInformes.RecordCount > 0 then
        begin
            spInformes.First;
            FuncionAnterior := '';
            while not spInformes.Eof do
            begin
                i:= 0;
                // Mientras no se terminen los items del menu principal y
                // el nombre del menu sea distinto al nombre de la funci�n, Incrementar el contador
                // hasta encontrar el item de menu desde el cual "colgar" los reportes.
                while (i <= Menu.Items.Count - 1)
                        and (UpperCase(Trim(Menu.Items[i].Name)) <> UpperCase(Trim(spInformes.FieldByName('Funcion').AsString))) do
                begin
                    i:= i + 1;
                end;


                // Si a�n quedan items por tratar
                if (i <= Menu.Items.Count - 1) then
                begin
                    // Si se cambio de Funci�n y hay items de men�, entonces Crear un item de men� de separaci�n.
                    if (FuncionAnterior <> UpperCase(Trim(spInformes.FieldByName('Funcion').AsString))) then
                    begin
                        // Cargar los items de menu de las categor�as
                        CargarCategoriasInformes(Conn, Menu.Items[i]);
                    end;

                    // Almacenar la Funci�n que se acaba de tratar.
                    FuncionAnterior := UpperCase(Trim(spInformes.FieldByName('Funcion').AsString));

                    // Obtener el item de categor�a del que debe colgar el informe.
                    ItemCategoria := ObtenerItemMenuDeCategoria(spInformes.FieldByName('CodigoCategoria').AsInteger, Menu.Items[i]);

                    // Crear un item de men� para el reporte.
                    ItemInforme := TReportItem.Create(ItemCategoria);
                    ItemInforme.Name:='__inf__'+Trim(spInformes.FieldByName('CodigoConsulta').AsString);
                    ItemInforme.Caption := Trim(spInformes.FieldByName('Descripcion').AsString);
                    ItemInforme.Codigo := spInformes.FieldByName('CodigoConsulta').AsInteger;
                    ItemInforme.Hint := iif(spInformes.FieldByName('Detalle').IsNull, '', spInformes.FieldByName('Detalle').Value);
                    ItemInforme.ConnectionDatosBase := Conn;
                    ItemCategoria.Add(ItemInforme);
                end;

                spInformes.Next;
            end;
        end;
    finally
        if spInformes.State = dsBrowse then
           spInformes.Close;
		spInformes.Free;
	end;
{TERMINO:TASK_028_JMA_20160628}


    // Quitar los items de menu que no tienen hijos.
    QuitarCategoriasSinInformes(Menu);
end;



function ObtenerUbicacionActualTagPorNumero( const ContextMark: Smallint; const ContactSerialNumber: DWORD): integer;
resourcestring
    MSG_ERROR_OBTENIENDO_ALMACEN_TAG = 'Error Obteniendo almac�n actual del telev�a';
begin
    try
        Result := QueryGetValueInt(DMConnections.BaseCAC, format('select dbo.ObtenerUbicacionActualTagPorNumero(%d,%d)',[ContextMark , ContactSerialNumber]));
    except
        On e: exception do
            Raise Exception.Create(MSG_ERROR_OBTENIENDO_ALMACEN_TAG);
    end;
end;


function VerificarExistenCotizacionesDelDia:boolean;
resourcestring
    MSG_ERROR_COTIZACION_UF_INVALIDA = 'Las cotizaci�n de %s a la fecha %s est� en cero o no existe. Debe ingresarse desde el m�dulo de Facturaci�n';
    MSG_CAPTION = 'Validaci�n Cotizaciones';
var
    CotizacionInt: integer;
    Fecha: TDateTime;
	sp: TADOStoredProc;
    Moneda: string;
begin
    Result := True;
    Fecha := NowBase(DMConnections.BaseCAC);
    sp := TADOStoredProc.Create(nil);
	try
		sp.Connection := DMConnections.BaseCAC;
        sp.ProcedureName:='ObtenerListaMonedas';
        sp.Open;
		While not SP.Eof do begin
            //de la base viene como INT a 2 decimales
            Moneda:= SP.FieldByName('Moneda').AsString;
            CotizacionInt := QueryGetValueInt(DMConnections.BaseCAC, format('select dbo.ObtenerCotizacionAFecha(''%s'',''%s'')',[Moneda,FormatDateTime('yyyymmdd',Fecha)]));
            if CotizacionInt = 0  then
//                MsgBoxErr(format(MSG_ERROR_COTIZACION_UF_INVALIDA, [Moneda, DateToStr(Fecha)]),MSG_CAPTION, MSG_CAPTION, MB_ICONSTOP);
                ShowMsgBoxCN(MSG_CAPTION, format(MSG_ERROR_COTIZACION_UF_INVALIDA, [Moneda, DateToStr(Fecha)]), MB_ICONERROR, Application.MainForm);
			SP.Next;
		end;
	finally
		SP.Close;
		SP.Free;
	end;
end;


function ValidarVoucher( NumeroVoucher: integer; ListaVouchers:TStringList; Control:TControl ): boolean;
ResourceString
    MSG_ERROR_VOUCHER_YA_ASIGNADO = 'El n�mero de voucher ingresado ya ha sido utilizado';
    MSG_ERROR_VOUCHER_NO_EXISTE = 'El n�mero de voucher ingresado no existe';
var
    Index: integer;
    CodigoRespuesta: integer;
begin
    Result := True;
    for Index := 0 to ListaVouchers.Count - 1 do begin
        if NumeroVoucher = StrToInt(ListaVouchers[Index]) then begin
            MsgBoxBalloon(MSG_ERROR_VOUCHER_YA_ASIGNADO,'Error',MB_ICONSTOP, Control);
            Result := False;
            Exit;
        end;
    end;
    CodigoRespuesta := QueryGetValueInt(DMConnections.BaseCAC, format('select dbo.ValidarVoucherArriendo(%d)',[NumeroVoucher]));
    // 0 1 2
    if CodigoRespuesta = 0 then //disponible
        Result := True;

    if CodigoRespuesta = 1 then begin //no existe
        MsgBoxBalloon(MSG_ERROR_VOUCHER_NO_EXISTE,'Error',MB_ICONSTOP, Control);
        Result := False;
    end;

    if CodigoRespuesta = 2 then begin //ya usado
        MsgBoxBalloon(MSG_ERROR_VOUCHER_YA_ASIGNADO,'Error',MB_ICONSTOP, Control);
        Result := False;
    end;

end;

{******************************** Function Header ******************************
Function Name: NumeroDocumentoTieneConvenio
Author : vpaszkowicz
Date Created : 21/01/2008
Description : Retorna true si la persona tiene un convenio con alguna concesio-
naria si no se especifica la misma en el par�metro o con alguna en particular si
se especifica.
Parameters : NumeroDocumento: string; codigoConcesionaria: integer = 0
Return Value : Boolean
*******************************************************************************}
function NumeroDocumentoTieneConvenio(NumeroDocumento: string; codigoConcesionaria: integer = 0): Boolean;
resourceString
    MSG_ERROR_BUSCANDO_CONVENIO = 'Se ha producido un error al ejecutar la funci�n TieneConvenioConcesionaria';
    MSG_BUSCANDO_CONVENIO = 'Error';
begin
    Result := False;
    try
        if CodigoConcesionaria <> 0 then
            Result := QueryGetValueInt(dmconnections.BaseCAC, 'Select dbo.TieneConvenioConConcesionaria( ' + QuotedStr(NumeroDocumento) + ',' + inttostr(CodigoConcesionaria) + ')') = 1
        else Result := QueryGetValueInt(dmconnections.BaseCAC, 'Select dbo.TieneConvenioConConcesionaria( ' + QuotedStr(NumeroDocumento) + ',NULL)') = 1;
    except
        on e: exception do begin
            MsgBoxErr(MSG_ERROR_BUSCANDO_CONVENIO, E.message, MSG_BUSCANDO_CONVENIO, MB_ICONSTOP);
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Procedure: PermitirSoloLetrasNumeros
  Author:    lcanteros
  Date:      23-Abr-2008
  Arguments: Caracter:Char
  Result:    char
  Description: Permite caracteres letras y numeros solamente en el ingreso de
  texto, con espacio y tecla de backspace
  History:
-----------------------------------------------------------------------------}
function PermitirSoloLetrasNumeros(Caracter:Char):char;
begin
    Result := Caracter;
    if not(Caracter in ['a'..'z','A'..'Z','0'..'9', ' ', '�', '�', #8]) then Result := #0;
end;
{-----------------------------------------------------------------------------
  Procedure: PermitirSoloNumeros
  Author:    lcanteros
  Date:      23-Abr-2008
  Arguments: Caracter:Char
  Result:    Char
  Description: permite el ingreso de numeros solamente con espacio
  y tecla de backspace
  History:
-----------------------------------------------------------------------------}
function PermitirSoloNumeros(Caracter:Char):Char;
begin
    Result := Caracter;
    if not(Caracter in ['0'..'9', ' ', #8]) then Result := #0;
end;
{-----------------------------------------------------------------------------
  Procedure: EsFormatoPatenteValido
  Author:    lcanteros
  Date:      30-Abr-2008
  Arguments: Pat: string
  Result:    boolean
  Description: valida el formato de la patente chilena
  History:

  Revision : 1
      Author : pdominguez
      Date   : 05/11/2009
      Description : SS 719
        - Se corrige un bug encontrado. El valor de Result estaba invertido.
-----------------------------------------------------------------------------}
function EsFormatoPatenteValido(Pat: string): boolean;
var spValidaPatente: TADOStoredProc;
begin
    Result := false;
    spValidaPatente := TADOStoredProc.Create(nil);
    try
      try
        with spValidaPatente, Parameters do begin
            Connection := DMConnections.BaseCAC;
            ProcedureName := SP_VALIDA_PATENTE;
            Refresh;
            ParamByName('@Patente').Value := Pat;
            ExecProc;
            Result := ParamByName('@RETURN_VALUE').Value = 1; // Rev. 1 (SS 719)
        end;
      except
        MsgBoxErr(MSG_ERROR_BD_VALIDAR_FORMATO_PATENTE, '', CAPTION_ERROR, 0);
      end;
    finally
        spValidaPatente.Close;
        spValidaPatente.Free;
    end;
end;

{-----------------------------------------------------------------------------
  Procedure: ExisteTurnoAbiertoPorUsuario
  Author:    mvitali
  Date:      12-May-2008
  Arguments: Pat: string
  Result:    boolean
  Description: Verifica si hay un turno abierto por un usuario dado
  History:
-----------------------------------------------------------------------------}

function ExisteTurnoAbiertoPorUsuario(Conn: TADOConnection; AUsuarioSistema: string): Boolean;
begin
    If (QueryGetValueInt(Conn,'SELECT TOP 1 1 FROM Turnos (NOLOCK) WHERE CodigoUsuario =' +
     QuotedStr(AUsuarioSistema) + ' AND Estado = dbo.CONST_TURNO_ESTADO_ABIERTO()') = 1) then
     Result := True else Result := False;
end;
{-----------------------------------------------------------------------------
  Procedure: BuscarPatenteCuenta
  Author:    lcanteros
  Date:      14-May-2008
  Arguments: Patente : TEdit; CodigoConvenio: integer
  Result:    string 0: si no existe | 1: Si existe por campo patente | 2: si existe por campo PatenteRVM
  Description: verifica si la patente ya existe en otra cuenta ya sea por
  el campo patente o por el campo PatenteRVM
  History:
-----------------------------------------------------------------------------}
function ExistePatenteCuenta(Base:TADOConnection; Patente : TEdit; CodigoConvenio: integer; var PatAsociada, ConvenioAsociado: string): string;
var spExiste: TADOStoredProc;
begin
Result := '';
spExiste := TADOStoredProc.Create(nil);
try
    with spExiste, Parameters do begin
        Connection := Base;
        ProcedureName := 'ExisteCuentaEx';
        Refresh;
        ParamByName('@Patente').Value := Trim(Patente.Text);
        ParamByName('@CodigoTipoPatente').Value := PATENTE_CHILE;
        ParamByName('@ConvenioExcento').Value := CodigoConvenio;
        ExecProc;
        Result := ParamValues['@RETURN_VALUE'];
        PatAsociada := VarToStr(ParamValues['@PatenteAsociada']);
        ConvenioAsociado := VarToStr(ParamValues['@ConvenioAsociado']);
    end;
finally
    spExiste.Close;
    spExiste.Free;
end;
end;
{-----------------------------------------------------------------------------
  Procedure: EliminarCaracteresNoSoportados
  Author:    lcanteros
  Date:      12-Jun-2008
  Arguments: var Texto: string
  Result:    None
  Description: reemplaza los caractreres no soportados por el sistema de inspecci�n fiscal
  (SS 704) El arreglo ReemplazarPor debe tener la misma cantidad de items que el
  arreglo de caracters malos ya que de ahi se toman los caracteres a reemplazar
  History:
  Revision 1 :
     Author:    lcanteros
     Date:      04/Ago/2008
     Description: pruebo con el for pq han dicho en chile que se filtraban algunos
     caracters 
-----------------------------------------------------------------------------}
procedure ReemplazarCaracteresNoSoportados(var Texto: string);
const
    CARACTER_MALO = '''';
    REEMPLAZAR_POR = ' ';
var
    AuxTexto: string;
    i, Long: integer;
begin
    AuxTexto := Texto;
    Long := Length(AuxTexto);
    for i := 1 to Long do begin
        if UpperCase(AuxTexto[i]) = UpperCase(CARACTER_MALO) then
            AuxTexto[i] := REEMPLAZAR_POR;
    end;
    Texto := AuxTexto;
end;
{-----------------------------------------------------------------------------
  Procedure: ObtenerTiempoConsultaReporte
  Author:    lcanteros
  Date:      15-Jul-2008
  Arguments: None
  Result:    None
  Description: Obtengo el timepo de time out de la consulta por el
  parametro general, si es menor que el valor default se asigna el default
  History:
-----------------------------------------------------------------------------}
procedure ObtenerTiempoConsultaReporte(Conexion: TADOConnection ; var Tiempo: integer);
begin
    ObtenerParametroGeneral(Conexion, TIME_OUT_CONSULTAS_INFORMES, Tiempo);
    if Tiempo < TIME_OUT_CONSULTAS_GESTION_INFORMES_MINIMO then
        Tiempo := TIME_OUT_CONSULTAS_GESTION_INFORMES_MINIMO;
end;
{-----------------------------------------------------------------------------
  Procedure: ObtenerNumeroConvenioSinConcesiuonaria
  Author:    lcanteros
  Date:      07-Ago-2008
  Arguments: Numero: string
  Result:    string
  Description: se le quita el codigo de concesionaria a un numero de convenio formateado
  como AAA-AAAAAAAAAAA-AAA o AAAAAAAAAAAAAAAAA
  History:

  Revision 1
  Author: mbecerra
  Date: 16-Dic-2008
  Description: 	Se modifica esta funci�n para cumplir lo de la SS 725 (re-abierta), a saber:
                	* Que se eliminen todos los guiones del n�mero de convenio,
                      en cualquier posici�n (el c�digo original eliminaba s�lo el primer gui�n)
                    * Que se elimine el prefijo 001
-----------------------------------------------------------------------------}
//BEGIN : SS_1147Q_NDR_20141202 ------------------------------------------------
function ObtenerNumeroConvenioSinConcesionaria(Numero: string): string;
var
    PosAux: integer;
begin
  if Numero = EmptyStr then
    Exit;

  //Revision 1
  Result := '';
  for PosAux := 1 to Length(Numero) do
  begin
    if Numero[PosAux] <> '-' then
      Result := Result + Numero[PosAux];
	end;

  if ObtenerCodigoConcesionariaNativa <> CODIGO_VS then
    Delete(Result, 1, 3);

  //PosAux := Pos('-', Numero);
  //if PosAux <=0 then // si no tiene guiones le quito los 3 primeros
  //	Delete(Result, 1, 3)
  //else if PosAux = 4 then // con guion en la posicion que va le quito los 4 primeros
  //	Delete(Result, 1, 4);

  // Fin		Revision 1
end;
//END : SS_1147Q_NDR_20141202 ------------------------------------------------


//BEGIN : SS_1147Q_NDR_20141202----------------------------------------------------------
function LargoNumeroConvenioConcesionariaNativa():Integer;
begin
  if ObtenerCodigoConcesionariaNativa=CODIGO_VS then
  begin
      Result:=12
  end
  else
  begin
      Result:=17
  end;
end;
//END : SS_1147Q_NDR_20141202-------------------------------------------------------------




{-----------------------------------------------------------------------------
  Function Name: MensajeTipoCliente
  Author: mpiazza
  Date Created: 23/01/2009
  Description:  Se genera un mensaje de acuerdo a numero de documento y
  tipo de sistema.
  Parameters: Conn: Conn: TADOConnection; NumeroDocumento: AnsiString; CodigoSistema: Integer
  Return Value: AnsiString
-----------------------------------------------------------------------------}
function  MensajeTipoCliente(Conn: TADOConnection; NumeroDocumento: AnsiString; CodigoSistema: Integer): AnsiString;
var
	spObtenerMensajes: TADOStoredProc;
    Texto, NumeroConvenio, Patente, Etiqueta: AnsiString;
    cdsConvenioMensaje: TClientDataSet;
begin
    texto := '';
	spObtenerMensajes := TADOStoredProc.Create(nil);

    cdsConvenioMensaje := TClientDataSet.Create(Conn);
    try
		spObtenerMensajes.Connection := Conn;
        spObtenerMensajes.ProcedureName:='ObtenerMensajePorClienteSistema';
        spObtenerMensajes.Parameters.Refresh;
        spObtenerMensajes.Parameters.ParamByName('@CodigoSistema').Value := CodigoSistema;
        spObtenerMensajes.Parameters.ParamByName('@NumeroDocumento').Value := NumeroDocumento;
        spObtenerMensajes.Open;

        while not spObtenerMensajes.Eof do begin
            texto := Texto + Trim(spObtenerMensajes.FieldByName('Descripcion').AsString)+ #13#10 + #13#10;
            spObtenerMensajes.Next;
        end;

    finally
    	spObtenerMensajes.Close;
    	spObtenerMensajes.Free;
    end;
    result := texto;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarTipoDeuda
  Author: mpiazza
  Date Created: 03/02/2009
  Description: cargo una cvariantcombobox con los tipos de deuda
  Parameters: Conn: TADOConnection; Combo: TVariantComboBox; Ninguno: Boolean = True; TipoDeuda: Integer = 0
  Return N/A
-----------------------------------------------------------------------------}

procedure CargarTipoDeuda(Conn: TADOConnection; Combo: TVariantComboBox; Ninguno: Boolean = True; TipoDeuda: Integer = 0);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'CarpetasMorosos_Obtenertiposdeudas';
		Qry.Open;
		Combo.Clear;
		i := -1;
        if Ninguno then begin
        	Combo.Items.Add(SIN_ESPECIFICAR, 0);
            i := 0;
		end;
       While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('Descripcion').AsString,
              Istr(Qry.FieldByName('CodigoTipoDeuda').AsInteger));
			if (Qry.FieldByName('CodigoTipoDeuda').AsInteger = TipoDeuda) then begin
            	i := Combo.Items.Count - 1;
            end;
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;


{-----------------------------------------------------------------------------
  Function Name: CargarEmpresaRecaudacion
  Author: mpiazza
  Date Created: 03/02/2009
  Description: cargo variantcombobox con empresas recaudadoras disponibles
  Parameters: Conn: TADOConnection; Combo: TVariantComboBox; Ninguno: Boolean = True; EmpresaRecaudacion: Integer = 0
  Return N/A

  Revision 1
    Author: mpiazza
    Date: 04/06/2009
    Description: SS 803
           - se le a�ade un parametro de filtro opcional por campo judicial,
           True: trae solo empresas judiciales
           False: trae solo empresas NO judiciales
-----------------------------------------------------------------------------}
procedure CargarEmpresaRecaudacion(Conn: TADOConnection; Combo: TVariantComboBox; Ninguno: Boolean = True; EmpresaRecaudacion: Integer = 0; FiltroJudicial: boolean = false);
Var
	i: Integer;
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'CarpetasMorosos_ObtenerEmpresasRecaudadoras ?';
    Qry.Parameters.ParamByName('@filtroJudicial').Value := FiltroJudicial;
		Qry.Open;
		Combo.Clear;
		i := -1;
        if Ninguno then begin
        	Combo.Items.Add(SIN_ESPECIFICAR, 0);
            i := 0;
		end;
       While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('AliasEmpresa').AsString,
              Istr(Qry.FieldByName('CodigoEmpresasRecaudadoras').AsInteger));
			if (Qry.FieldByName('CodigoEmpresasRecaudadoras').AsInteger = EmpresaRecaudacion) then begin
            	i := Combo.Items.Count - 1;
            end;
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarEmpresaRecaudacionJudicial
  Author: mpiazza
  Date Created: 22/06/2009
  Description: cargo variantcombobox con empresas recaudadoras disponibles (agregado en la rev 20
  Parameters: Conn: TADOConnection; Combo: TVariantComboBox; Ninguno: Boolean = True; EmpresaRecaudacion: Integer = 0
  Return N/A

-----------------------------------------------------------------------------}
procedure CargarEmpresaRecaudacionJudicial(Conn: TADOConnection; Combo: TVariantComboBox; Ninguno: Boolean = True; EmpresaRecaudacion: Integer = 0);
Var
	i: Integer;
	Qry: TADOQuery;
  FiltroJudicial: boolean ;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'CarpetasMorosos_ObtenerEmpresasRecaudadorasJudicial';
		Qry.Open;
		Combo.Clear;
		i := -1;
        if Ninguno then begin
        	Combo.Items.Add(SIN_ESPECIFICAR, 0);
            i := 0;
		end;
       While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('AliasEmpresa').AsString,
              Istr(Qry.FieldByName('CodigoEmpresasRecaudadoras').AsInteger));
			if (Qry.FieldByName('CodigoEmpresasRecaudadoras').AsInteger = EmpresaRecaudacion) then begin
            	i := Combo.Items.Count - 1;
            end;
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarEmpresaRecaudacionPreJudicial
  Author: mpiazza
  Date Created: 22/06/2009
  Description: cargo variantcombobox con empresas recaudadoras disponibles
  Parameters: Conn: TADOConnection; Combo: TVariantComboBox; Ninguno: Boolean = True; EmpresaRecaudacion: Integer = 0
  Return N/A

-----------------------------------------------------------------------------}
procedure CargarEmpresaRecaudacionPreJudicial(Conn: TADOConnection; Combo: TVariantComboBox; Ninguno: Boolean = True; EmpresaRecaudacion: Integer = 0);
Var
	i: Integer;
	Qry: TADOQuery;
  FiltroJudicial: boolean ;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := Conn;
		Qry.SQL.Text := 'CarpetasMorosos_ObtenerEmpresasRecaudadorasPreJudicial';
		Qry.Open;
		Combo.Clear;
		i := -1;
        if Ninguno then begin
        	Combo.Items.Add(SIN_ESPECIFICAR, 0);
            i := 0;
		end;
       While not Qry.Eof do begin
			Combo.Items.Add(Qry.FieldByName('AliasEmpresa').AsString,
              Istr(Qry.FieldByName('CodigoEmpresasRecaudadoras').AsInteger));
			if (Qry.FieldByName('CodigoEmpresasRecaudadoras').AsInteger = EmpresaRecaudacion) then begin
            	i := Combo.Items.Count - 1;
            end;
			Qry.Next;
		end;
		Combo.ItemIndex := i;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;


function ConvertirEnFechaParaScript(unaFecha: TDateTime; IncluirHora: boolean): String;
var
    dd, mm, aaaa, hh24, mi, ss, ms: word;
begin
    DecodeDateTime(unaFecha, aaaa, mm, dd, hh24, mi, ss, ms);
    if IncluirHora then
        result :=
        Istr0(aaaa, 4)  +
        Istr0(mm, 2)    +
        Istr0(dd, 2)    +
        Istr0(hh24, 2)  +
        Istr0(mi, 2)    +
        Istr0(ss, 2)
    else
        Result :=
        Istr0(aaaa, 4)  +
        Istr0(mm, 2)    +
        Istr0(dd, 2);
end;

{******************************** Function Header ******************************
    Function Name: ObtenerDatosPersonales
    Author : pdominguez
    Date Created : 07/07/2009
    Parameters : Conn: TADOConnection; CodigoDocumento, NumeroDocumento: String
    Return Value : TDatosPersonales
    Description : SS 784
        - Devuelve los Datos Personales para el numero de RUT pasado, en el
        formato TDatosPersonales.

    09-Julio-2009
    Author: mbecerra
        	Se modifica para que devuelva -1 en el c�digo de persona si el cliente
            no fue encontrado
*******************************************************************************}
function ObtenerDatosPersonales(Conn: TADOConnection; CodigoDocumento, NumeroDocumento: String; CodigoPersona: Integer = 0): TDatosPersonales; // SS 784
var
    SP: TAdoStoredProc;
    DatosPersonales: TDatosPersonales;
begin
	DatosPersonales.CodigoPersona := -1; 		//mbecerra

    SP := TAdoStoredProc.Create(nil);
    try
        SP.ProcedureName := 'ObtenerDatosPersona';
        SP.Connection := Conn;
        SP.Parameters.CreateParameter('@CodigoPersona', ftInteger, pdInput, 0, iif(CodigoPersona = 0, NULL, CodigoPersona));
        SP.Parameters.CreateParameter('@CodigoDocumento', ftString, pdInput, 4, CodigoDocumento);
        SP.Parameters.CreateParameter('@NumeroDocumento', ftString, pdInput, 11, NumeroDocumento);
        SP.open;

        //mbecerra
        //if SP.FieldByName('CodigoPersona').AsString <> '' then
        if (not SP.IsEmpty) and (SP.FieldByName('CodigoPersona').AsString <> '') then begin
            with DatosPersonales, SP do begin
                CodigoPersona := FieldByName('CodigoPersona').AsInteger;
                Personeria    := FieldByName('Personeria').AsString[1];
                TipoDocumento :=  Trim(FieldByName('CodigoDocumento').AsString);
                NumeroDocumento :=  Trim(FieldByName('NumeroDocumento').AsString);
                RazonSocial := iif(Personeria = PERSONERIA_JURIDICA, Trim(FieldByName('Apellido').AsString), '');
                Apellido :=  iif(Personeria = PERSONERIA_FISICA, Trim(FieldByName('Apellido').AsString), Trim(FieldByName('ApellidoContactoComercial').AsString));
                ApellidoMaterno :=  iif(Personeria = PERSONERIA_FISICA, Trim(FieldByName('ApellidoMaterno').AsString), Trim(FieldByName('ApellidoMaternoContactoComercial').AsString));
                Nombre :=  iif(Personeria = PERSONERIA_FISICA, Trim(FieldByName('Nombre').AsString), Trim(FieldByName('NombreContactoComercial').AsString));
                Sexo := (iif(Personeria = PERSONERIA_FISICA, FieldByName('Sexo').AsString, FieldByName('SexoContactoComercial').AsString)+' ')[1];
                FechaNacimiento := FieldByName('FechaNacimiento').AsDateTime;
                Password :=  Trim(FieldByName('Password').AsString);
                Pregunta :=  Trim(FieldByName('Pregunta').AsString);
                Respuesta :=  Trim(FieldByName('Respuesta').AsString);
            end;
        end;

        SP.Close;
    finally
        SP.Free;
    end;

    Result := DatosPersonales;		//mbecerra
end;

{******************************** Function Header ******************************
Procedure Name: ObtenerEstadoTransito
Author : mpiazza
Date Created : 15-09-2009
Parameters : Conn: TADOConnection; NumCorrCA: Integer
Return Value : integer
Description : SS-826 se genera la funcion para obtener el estado de un transito

Revision : 1
    Author :
    Date   :
    Description :
*******************************************************************************}
function ObtenerEstadoTransito(Conn: TADOConnection; NumCorrCA: Integer ): Integer;
var
    SP: TADOStoredProc;
begin
    Result := -1;
    SP := TADOStoredProc.Create(nil);
    try
        sp.Connection := Conn;
        sp.ProcedureName := 'ObtenerEstadoTransito';
        with sp do begin
            Parameters.Refresh;

            Parameters.ParamByName('@NumCorrCA').Value := NumCorrCA;
            ExecProc;
            Result := Parameters.ParamByName('@RETURN_VALUE').Value;
        end;
    except
          on e: Exception do MsgBoxErr('Error al Tratar de Obtener Estado del Transito', e.Message, 'Error al ejecutar SP', MB_ICONSTOP);
    end;
    sp.Free;
end;

{******************************** Function Header ******************************
Procedure Name: InsertarAlarmaEvento
Author : mpiazza
Date Created : 15-09-2009
Parameters : Conn: TADOConnection; Modulo : string; Categoria :integer; Detalle: string
Return Value :
Description : SS-826 se genera el procedimiento Insertar Alarma Evento

Revision : 1
    Author :
    Date   :
    Description :
*******************************************************************************}
function InsertarAlarmaEvento(Conn: TADOConnection;
                Modulo : string; Categoria :integer; Detalle: string): Boolean;
var
    SP: TADOStoredProc;
begin
    Result := False;
    SP := TADOStoredProc.Create(nil);
    try
        sp.Connection := Conn;
        sp.ProcedureName := 'InsertarAlarmaEvento';
        with sp do begin
            Parameters.Refresh;
            Parameters.ParamByName('@Modulo').Value := Modulo;
            Parameters.ParamByName('@Categoria').Value := Categoria;
            Parameters.ParamByName('@Detalle').Value := Detalle;
            ExecProc;
        end;
        Result := True;
    except
          on e: Exception do MsgBoxErr('Error al Insertar Alarma de evento', e.Message, 'Error al ejecutar SP', MB_ICONSTOP);
    end;
    sp.Free;
end;

{******************************** Function Header ******************************
Function Name: ObtenerModulo10
Author : pdominguez
Date Created : 03/11/2009
Parameters : Conn: TADOConnection; Numero: String
Return Value : String
Description : SS 719
    - Devuelve el D�gito de Control calculado por el M�dulo 10.
*******************************************************************************}
function ObtenerModulo10(Conn: TADOConnection; Numero: String): String;
begin
    Result := QueryGetValue(Conn,Format('SELECT dbo.ObtenerModulo10(%s)',[Numero]));
end;

{******************************** Function Header ******************************
Function Name: ExistePatente
Author : pdominguez
Date Created : 27/11/2009
Parameters : Conn: TADOConnection; Patente: String
Return Value : Boolean
Description : SS 719 - Defect 13741
    - Comprueba si existe una Patente en el Maestro de Veh�culos.
*******************************************************************************}
function ExistePatente(Conn: TADOConnection; Patente: string): Boolean;
    resourcestring
        MSG_ERROR_EJECUCION1 = 'Se produjo un Error al ejecutar el SP %s.';
        MSG_ERROR_EJECUCION2 = 'Error al ejecutar el SP %s. Error: %s';
        MSG_ERROR_CAPTION = 'Error Ejecuci�n Store Procedure';
    var
        ADOStoredProc: TADOStoredProc;
begin
    try
        Result := False;
        ADOStoredProc := TADOStoredProc.Create(Nil);
        with ADOStoredProc do try
            Connection := Conn;
            ProcedureName := 'ExistePatente';
            Parameters.Refresh;
            Parameters.ParamByName('@Patente').Value := Patente;
            Open;
            Result := Fields[0].AsInteger = 1;
        except
            on e: Exception do begin
                MsgBoxErr(
                    Format(MSG_ERROR_EJECUCION1, [ProcedureName]),
                    Format(MSG_ERROR_EJECUCION2, [ProcedureName, e.Message]),
                    MSG_ERROR_CAPTION,
                    MB_ICONERROR);
            end;
        end;
    finally
        if Assigned(ADOStoredProc) then begin
            if ADOStoredProc.Active then ADOStoredProc.Close;
            FreeAndNil(ADOStoredProc);
        end;
    end;

end;

{******************************** Function Header ******************************
Function Name: VerificarFormatoPatenteChilena
Author : pdominguez
Date Created : 30/11/2009
Parameters : Conn: TADOConnection; Patente: String
Return Value : Boolean
Description : SS 719 - Defect 13741
    - Comprueba el formato para las Patentes Chilenas.
*******************************************************************************}
function VerificarFormatoPatenteChilena(Conn: TADOConnection; Patente: string): Boolean;
    resourcestring
        MSG_ERROR_EJECUCION1 = 'Se produjo un Error al ejecutar el SP %s.';
        MSG_ERROR_EJECUCION2 = 'Error al ejecutar el SP %s. Error: %s';
        MSG_ERROR_CAPTION = 'Error Ejecuci�n Store Procedure';
    var
        ADOStoredProc: TADOStoredProc;
begin
    try
        Result := False;
        ADOStoredProc := TADOStoredProc.Create(Nil);
        with ADOStoredProc do try
            Connection := Conn;
            ProcedureName := 'VerificarFormatoPatenteChilena';
            Parameters.Refresh;
            Parameters.ParamByName('@Patente').Value := Patente;
            ExecProc;
            Result := Parameters.ParamByName('@RETURN_VALUE').Value = 1;
        except
            on e: Exception do begin
                MsgBoxErr(
                    Format(MSG_ERROR_EJECUCION1, [ProcedureName]),
                    Format(MSG_ERROR_EJECUCION2, [ProcedureName, e.Message]),
                    MSG_ERROR_CAPTION,
                    MB_ICONERROR);
            end;
        end;
    finally
        if Assigned(ADOStoredProc) then begin
            if ADOStoredProc.Active then ADOStoredProc.Close;
            FreeAndNil(ADOStoredProc);
        end;
    end;

end;


{
  Procedure Name: CargarComboConcesionarias
  Parameters: Conn: TADOConnection; Combo: TVariantComboBox; TieneSinEspecificar : Boolean = False

  Author: pdominguez
  Date Created: 09/05/2010
  Description: Infractores - Fase 2
        - Carga en TVarianComboBox las Concesionarias
}
procedure CargarComboConcesionarias(Conn: TADOConnection; Combo: TVariantComboBox; TieneSinEspecificar : Boolean = False);
    resourcestring
        MSG_ERROR_EJECUCION1 = 'Se produjo un Error al ejecutar el SP %s.';
        MSG_ERROR_EJECUCION2 = 'Error al ejecutar el SP %s. Error: %s';
        MSG_ERROR_CAPTION = 'Error Ejecuci�n Store Procedure';
    var
        ADOStoredProc: TADOStoredProc;
begin
    try
        ADOStoredProc := TADOStoredProc.Create(nil);
        with ADOStoredProc, Combo do try
            Connection := Conn;
            ProcedureName := 'ObtenerConcesionarias';
            Open;
            Clear;
            if TieneSinEspecificar then Items.Add(SIN_ESPECIFICAR, -1);
            while not Eof do begin
                Items.Add(FieldByName('Descripcion').AsString, FieldByName('CodigoConcesionaria').AsInteger);
                Next;
            end;
            ItemIndex := 0;
        except
            on e: Exception do begin
                MsgBoxErr(
                    Format(MSG_ERROR_EJECUCION1, [ProcedureName]),
                    Format(MSG_ERROR_EJECUCION2, [ProcedureName, e.Message]),
                    MSG_ERROR_CAPTION,
                    MB_ICONERROR);
            end;
        end;
    finally
        if Assigned(ADOStoredProc) then begin
            if ADOStoredProc.Active then ADOStoredProc.Close;
            FreeAndNil(ADOStoredProc);
        end;
    end;
end;

{
  Procedure Name: CargarComboMotivoAnulacion
  Parameters: Conn: TADOConnection; Combo: TVariantComboBox; TieneSinEspecificar : Boolean = False;
    SoloPermitidosAnulacionManual: Boolean = False

  Author: pdominguez
  Date Created: 09/05/2010
  Description: Infractores - Fase 2
        - Carga un VarianCombo con los Motivos de Anulaci�n de una Infracci�n
}
procedure CargarComboMotivoAnulacion(Conn: TADOConnection; Combo: TVariantComboBox; TieneSinEspecificar : Boolean = False;
    SoloPermitidosAnulacionManual: Boolean = False);
    resourcestring
        MSG_ERROR_EJECUCION1 = 'Se produjo un Error al ejecutar el SP %s.';
        MSG_ERROR_EJECUCION2 = 'Error al ejecutar el SP %s. Error: %s';
        MSG_ERROR_CAPTION = 'Error Ejecuci�n Store Procedure';
var
    ADOStoredProc: TADOStoredProc;
begin
     try
        ADOStoredProc := TADOStoredProc.Create(nil);
        with ADOStoredProc, Combo do try
            Connection := Conn;
            ProcedureName := 'ObtenerMotivosAnulacionInfraccion';
            Parameters.Refresh;
            Parameters.ParamByName('@PermitirAnulacionManual').Value := iif(not SoloPermitidosAnulacionManual, NULL, SoloPermitidosAnulacionManual);
            Open;
            Clear;
            if TieneSinEspecificar then Items.Add(SIN_ESPECIFICAR, -1);
            while not Eof do begin
                Items.Add(FieldByName('Descripcion').AsString, FieldByName('CodigoMotivoAnulacionInfraccion').AsInteger);
                Next;
            end;
            ItemIndex := 0;
        except
            on e: Exception do begin
                MsgBoxErr(
                    Format(MSG_ERROR_EJECUCION1, [ProcedureName]),
                    Format(MSG_ERROR_EJECUCION2, [ProcedureName, e.Message]),
                    MSG_ERROR_CAPTION,
                    MB_ICONERROR);
            end;
        end;
    finally
        if Assigned(ADOStoredProc) then begin
            if ADOStoredProc.Active then ADOStoredProc.Close;
            FreeAndNil(ADOStoredProc);
        end;
    end;
end;

{
  Procedure Name: CargarComboTiposConceptosMovimientos
  Parameters: Conn: TADOConnection; Combo: TVariantComboBox; TieneSinEspecificar : Boolean = False

  Author: Nelson Droguett Sierra
  Date Created: 12-Mayo-2010
  Description: Infractores - Fase 2
        - Carga en TVarianComboBox los TiposConceptosMovimientos
}
procedure CargarComboTiposConceptosMovimientos(Conn: TADOConnection; Combo: TVariantComboBox; TieneSinEspecificar : Boolean = False);
    resourcestring
        MSG_ERROR_EJECUCION1 = 'Se produjo un Error al ejecutar el SP %s.';
        MSG_ERROR_EJECUCION2 = 'Error al ejecutar el SP %s. Error: %s';
        MSG_ERROR_CAPTION = 'Error Ejecuci�n Store Procedure';
    var
        ADOStoredProc: TADOStoredProc;
begin
    try
        ADOStoredProc := TADOStoredProc.Create(nil);
        with ADOStoredProc, Combo do try
            Connection := Conn;
            ProcedureName := 'ObtenerTiposConceptosMovimientos';
            Open;
            Clear;
            if TieneSinEspecificar then Items.Add(SIN_ESPECIFICAR, -1);
            while not Eof do begin
                Items.Add(FieldByName('Descripcion').AsString, FieldByName('CodigoTipoConcepto').AsInteger);
                Next;
            end;
            ItemIndex := 0;
        except
            on e: Exception do begin
                MsgBoxErr(
                    Format(MSG_ERROR_EJECUCION1, [ProcedureName]),
                    Format(MSG_ERROR_EJECUCION2, [ProcedureName, e.Message]),
                    MSG_ERROR_CAPTION,
                    MB_ICONERROR);
            end;
        end;
    finally
        if Assigned(ADOStoredProc) then begin
            if ADOStoredProc.Active then ADOStoredProc.Close;
            FreeAndNil(ADOStoredProc);
        end;
    end;
end;
{
  Function Name: AsignarValorComboEstado
  Parameters: Combo: TVariantComboBox

  Author: pdominguez
  Date Created: 16/05/2010
  Description: Infractores - Fase 2
        - Devuelve el valor del ID del Item seleccionado o NULL si el Item seleccionado
        es el que se utiliza para indicar "Todos" o "Debe Seleccionar"
}
function AsignarValorComboEstado(Combo: TVariantComboBox): Variant;
begin
    if (Combo.ItemIndex = -1) or (Combo.Items[Combo.ItemIndex].Value = -1) then begin
        Result := NULL;
    end
    else begin
        Result := Combo.Items[Combo.ItemIndex].Value;
    end;
end;



{
  Procedure Name: CargarComboSubTiposOrdenServicio
  Parameters: Conn: TADOConnection; Combo: TVariantComboBox; TieneSinEspecificar : Boolean = False

  Author		: Nelson Droguett Sierra
  Date Created	: 23-Junio-2010
  Description	: Ref.Fase 2
        - Carga en TVarianComboBox los TiposOrdenServicio
}
procedure CargarComboTiposOrdenServicio(Conn: TADOConnection; Combo: TVariantComboBox; TieneSinEspecificar : Boolean = False);
    resourcestring
        MSG_ERROR_EJECUCION1 = 'Se produjo un Error al ejecutar el SP %s.';
        MSG_ERROR_EJECUCION2 = 'Error al ejecutar el SP %s. Error: %s';
        MSG_ERROR_CAPTION = 'Error Ejecuci�n Store Procedure';
    var
        ADOStoredProc: TADOStoredProc;
begin
    try
        ADOStoredProc := TADOStoredProc.Create(nil);
        with ADOStoredProc, Combo do try
            Connection := Conn;
            ProcedureName := 'ObtenerTiposOrdenServicio';
            Open;
            Clear;
            if TieneSinEspecificar then Items.Add(SIN_ESPECIFICAR, -1);
            while not Eof do begin
                Items.Add(FieldByName('Descripcion').AsString, FieldByName('TipoOrdenServicio').AsInteger);
                Next;
            end;
            ItemIndex := 0;
        except
            on e: Exception do begin
                MsgBoxErr(
                    Format(MSG_ERROR_EJECUCION1, [ProcedureName]),
                    Format(MSG_ERROR_EJECUCION2, [ProcedureName, e.Message]),
                    MSG_ERROR_CAPTION,
                    MB_ICONERROR);
            end;
        end;
    finally
        if Assigned(ADOStoredProc) then begin
            if ADOStoredProc.Active then ADOStoredProc.Close;
            FreeAndNil(ADOStoredProc);
        end;
    end;
end;

{---------------------------------------------------------------------------------------
			CargarFirmasEmail

Author		: ndroguett
Date		: 17-Agosto-2010
Description	:	(Ref 601)
				Llena un Combobox para elegir Pie de Firma en el ABM de Plantillas.
----------------------------------------------------------------------------------------}
procedure CargarFirmasEmail(Conn: TADOConnection; Combo: TVariantComboBox; CodigoFirma: Integer = 0; TieneItemSeleccionar: Boolean = False);
var
    table: TADOTable;
begin
    table := TADOTable.Create(Nil);
    try
        table.Connection := Conn;
        table.TableName := 'EMAIL_Firmas';
        table.Open;

        if TieneItemSeleccionar then Combo.Items.Add(SELECCIONAR, 0);

        while not table.Eof do begin
            Combo.Items.Add(table.FieldByName('Firma').AsString, table.FieldByName('CodigoFirma').AsInteger);
            table.Next;
        end; // while

        if (Combo.Items.Count > 0) then Combo.ItemIndex := Combo.Items.IndexOfValue(CodigoFirma);

    finally
        table.Close;
        table.Free;
    end; // finally

end;

{*******************************************************************************
Revisi�n 35
    Autor : Javier De Barbieri
    Date  : 10/11/2010
    Description : SS 931: Entrega un boolean informando si la patente se encuentra
    o no en el listado de Patentes en Seguimiento.

*******************************************************************************}

function EsPatenteEnSeguimiento(Conn: TADOConnection; patente: String): Boolean;
begin
  result := StrToBoolDef(QueryGetValue(Conn,format('select dbo.ExistePatenteEnSeguimiento (''%s'', 1)',[patente])), False);
end;

procedure CargarRegiones(Conn: TADOConnection; Combo: TVariantComboBox; CodigoPais: AnsiString; CodigoRegion: AnsiString = ''; TieneItemSeleccionar: boolean = false); overload;
    var
        sp: TADOStoredProc;
begin
    sp := TADOStoredProc.Create(Nil);
    try
        sp.Connection := Conn;
        sp.ProcedureName := 'ObtenerRegionesComunasChile';
        sp.Open;

        if TieneItemSeleccionar then Combo.Items.Add(SELECCIONAR, Null);

        while not sp.Eof do begin
            Combo.Items.Add(sp.FieldByName('Region').AsString, sp.FieldByName('CodigoRegion').AsString);
            sp.Next;
        end; // while

        if (Combo.Items.Count > 0) then begin
            Combo.ItemIndex := Combo.Items.IndexOfValue(CodigoRegion);
            if Combo.ItemIndex = -1 then Combo.ItemIndex := 0;
        end;
    finally
        sp.Close;
        sp.Free;
    end; // finally
end;

procedure CargarComunas(Conn: TADOConnection; Combo: TVariantComboBox; CodigoPais, CodigoRegion: AnsiString; CodigoComuna: AnsiString = ''; TieneItemSeleccionar:Boolean=False); overload;
    var
        sp: TADOStoredProc;
begin
    sp := TADOStoredProc.Create(Nil);
    try
        Combo.Items.Clear;

        sp.Connection := Conn;
        sp.ProcedureName := 'ObtenerComunasChileRegion';
        sp.Parameters.Refresh;
        sp.Parameters.ParamByName('@CodigoRegion').Value := CodigoRegion;
        sp.Open;

        if TieneItemSeleccionar then Combo.Items.Add(SELECCIONAR, Null);

        while not sp.Eof do begin
            Combo.Items.Add(sp.FieldByName('Descripcion').AsString, sp.FieldByName('CodigoComuna').AsString);
            sp.Next;
        end; // while

        if (Combo.Items.Count > 0) then begin
            Combo.ItemIndex := Combo.Items.IndexOfValue(CodigoComuna);
            if Combo.ItemIndex = -1 then Combo.ItemIndex := 0;            
        end;
    finally
        sp.Close;
        sp.Free;
    end; // finally
end;

{-----------------------------------------------------------------------------
  Function Name	: CargarConcesionariasReclamo
  Author		: Nelson Droguett Sierra
  Date Created	: 29/06/2010
  Description	: cargo las concesionarias del reclamo
  Parameters	: Combo:TVariantComboBox
  Return Value	: None
-----------------------------------------------------------------------------}
procedure CargarConcesionariasReclamo(Combo:TVariantComboBox);
const
    CONST_NINGUNO = '(Seleccionar)';
var
    SP: TAdoStoredProc;
begin
    try
        Combo.Items.Add(CONST_NINGUNO, 0);
        try
            SP := TADOStoredProc.Create(nil);
            SP.Connection := DMConnections.BaseCAC;
            SP.ProcedureName := 'ObtenerConcesionariasReclamo';                 //SS_1006_1015_MCO_20120904
            SP.Open;
            while not sp.Eof do begin
                Combo.Items.Add(SP.fieldbyname('descripcion').asstring,SP.fieldbyname('CodigoConcesionaria').asinteger);
                SP.Next;
            end;
            SP.Close;
            Combo.ItemIndex:=0;
        except
        end;
    finally
        FreeAndNil(SP);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name	: CargarConcesionariasReclamoHistorico
  Author		: Manuel Cabello Ocaranza
  Date Created	: 06/09/2012
  Description	: cargo las concesionarias activas mas las que alguna vez tuvieron algun reclamo
  Parameters	: Combo:TVariantComboBox
  Return Value	: None
-----------------------------------------------------------------------------}
procedure CargarConcesionariasReclamoHistorico(Combo:TVariantComboBox);
const
    CONST_NINGUNO = '(Seleccionar)';
var
    SP: TAdoStoredProc;
begin
    try
        Combo.Items.Add(CONST_NINGUNO, 0);
        try
            SP := TADOStoredProc.Create(nil);
            SP.Connection := DMConnections.BaseCAC;
            SP.ProcedureName := 'ObtenerConcesionariasReclamoHistorico';
            SP.Open;
            while not sp.Eof do begin
                Combo.Items.Add(SP.fieldbyname('descripcion').asstring,SP.fieldbyname('CodigoConcesionaria').asinteger);
                SP.Next;
            end;
            SP.Close;
            //Combo.ItemIndex:=0;                                               //SS_1255_MCA_20150505
            Combo.Value:=ObtenerCodigoConcesionariaNativa();                    //SS_1255_MCA_20150505
        except
        end;
    finally
        FreeAndNil(SP);
    end;
end;
{-----------------------------------------------------------------------------
  Function Name: CargarSubTiposReclamo
  Author:    Nelson Droguett Sierra
  Date Created: 29/06/2010
  Description: cargo las SubTipos del reclamo
  Parameters: Combo:TVariantComboBox
  Return Value: None
-----------------------------------------------------------------------------}
procedure CargarSubTiposReclamo(Combo:TVariantComboBox);
const
    CONST_NINGUNO = '(Seleccionar)';
var
    SP: TAdoStoredProc;
begin
    try
        Combo.Items.Add(CONST_NINGUNO, 0);
        try
            SP := TADOStoredProc.Create(nil);
            SP.Connection := DMConnections.BaseCAC;
            SP.ProcedureName := 'ObtenerSubTiposOrdenServicio';
            SP.Open;
            while not sp.Eof do begin
                Combo.Items.Add(SP.fieldbyname('descripcion').asstring,SP.fieldbyname('SubTipoOrdenServicio').asinteger);
                SP.Next;
            end;
            SP.Close;
            Combo.ItemIndex:=0;
        except
        end;
    finally
        FreeAndNil(SP);
    end;
end;

procedure CargarComboConcesionarias(Conn: TADOConnection; Combo: TVariantComboBox; TipoConcesionaria:Integer; TieneSinEspecificar : Boolean = False );overload; //PAR00133-FASE2-NDR-20110624
    resourcestring                                                                                                                                              //PAR00133-FASE2-NDR-20110624
        MSG_ERROR_EJECUCION1 = 'Se produjo un Error al ejecutar el SP %s.';                                                                                     //PAR00133-FASE2-NDR-20110624
        MSG_ERROR_EJECUCION2 = 'Error al ejecutar el SP %s. Error: %s';                                                                                         //PAR00133-FASE2-NDR-20110624
        MSG_ERROR_CAPTION = 'Error Ejecuci�n Store Procedure';                                                                                                  //PAR00133-FASE2-NDR-20110624
    var                                                                                                                                                         //PAR00133-FASE2-NDR-20110624
        ADOStoredProc: TADOStoredProc;                                                                                                                          //PAR00133-FASE2-NDR-20110624
begin                                                                                                                                                           //PAR00133-FASE2-NDR-20110624
    try                                                                                                                                                         //PAR00133-FASE2-NDR-20110624
        ADOStoredProc := TADOStoredProc.Create(nil);                                                                                                            //PAR00133-FASE2-NDR-20110624
        with ADOStoredProc, Combo do try                                                                                                                        //PAR00133-FASE2-NDR-20110624
            Connection := Conn;                                                                                                                                 //PAR00133-FASE2-NDR-20110624
            ProcedureName := 'ObtenerConcesionarias';                                                                                                           //PAR00133-FASE2-NDR-20110624
            Parameters.Refresh;                                                                                                                                 //PAR00133-FASE2-NDR-20110624
            Parameters.ParamByName('@CodigoTipoConcesionaria').Value := TipoConcesionaria;                                                                      //PAR00133-FASE2-NDR-20110624
            Open;                                                                                                                                               //PAR00133-FASE2-NDR-20110624
            Clear;                                                                                                                                              //PAR00133-FASE2-NDR-20110624
            if TieneSinEspecificar then Items.Add(SIN_ESPECIFICAR, -1);                                                                                         //PAR00133-FASE2-NDR-20110624
            while not Eof do begin                                                                                                                              //PAR00133-FASE2-NDR-20110624
                Items.Add(FieldByName('Descripcion').AsString, FieldByName('CodigoConcesionaria').AsInteger);                                                   //PAR00133-FASE2-NDR-20110624
                Next;                                                                                                                                           //PAR00133-FASE2-NDR-20110624
            end;                                                                                                                                                //PAR00133-FASE2-NDR-20110624
            ItemIndex := 0;                                                                                                                                     //PAR00133-FASE2-NDR-20110624
        except                                                                                                                                                  //PAR00133-FASE2-NDR-20110624
            on e: Exception do begin                                                                                                                            //PAR00133-FASE2-NDR-20110624
                MsgBoxErr(                                                                                                                                      //PAR00133-FASE2-NDR-20110624
                    Format(MSG_ERROR_EJECUCION1, [ProcedureName]),                                                                                              //PAR00133-FASE2-NDR-20110624
                    Format(MSG_ERROR_EJECUCION2, [ProcedureName, e.Message]),                                                                                   //PAR00133-FASE2-NDR-20110624
                    MSG_ERROR_CAPTION,                                                                                                                          //PAR00133-FASE2-NDR-20110624
                    MB_ICONERROR);                                                                                                                              //PAR00133-FASE2-NDR-20110624
            end;                                                                                                                                                //PAR00133-FASE2-NDR-20110624
        end;                                                                                                                                                    //PAR00133-FASE2-NDR-20110624
    finally                                                                                                                                                     //PAR00133-FASE2-NDR-20110624
        if Assigned(ADOStoredProc) then begin                                                                                                                   //PAR00133-FASE2-NDR-20110624
            if ADOStoredProc.Active then ADOStoredProc.Close;                                                                                                   //PAR00133-FASE2-NDR-20110624
            FreeAndNil(ADOStoredProc);                                                                                                                          //PAR00133-FASE2-NDR-20110624
        end;                                                                                                                                                    //PAR00133-FASE2-NDR-20110624
    end;                                                                                                                                                        //PAR00133-FASE2-NDR-20110624
end;   

{-----------------------------------------------------------------------------
Function Name   :   MostrarTagsVencidos
Author          :   Alejandro Labra
Date Created    :   11-abril-2011
Description     :   Muestra una ventana si el cliente consultado tiene Tags
                    con fecha de vencimiento posterior a la actual.
                    Para esto se crea una instancia de TAvisoTagVencidos.
Firma           :   SS-960-ALA-20110411
-----------------------------------------------------------------------------}
procedure MostrarTagsVencidos(Rut : String; pDataBase : TADOConnection; var pNotificado: Boolean; SinAviso: Boolean = False);
var
    FormMensajeTagsVencidos : TVentanaAvisoForm;
    AvisoTagVencidos        : TAvisoTagVencidos;
begin
     try
        if Rut<>'' then begin
            FormMensajeTagsVencidos := TVentanaAvisoForm.Create(Nil);
            AvisoTagVencidos := TAvisoTagVencidos.Create;
            //Solo si se pudo inicializar mostramos el formulario.                                      //SS-960-ALA-20110423
            if FormMensajeTagsVencidos.Inicializar(AvisoTagVencidos, [Rut], pDataBase) then begin       //SS-960-ALA-20110423
                if not SinAviso then begin
                    FormMensajeTagsVencidos.ShowModal;
                end
                else FormMensajeTagsVencidos.LanzarAccionSinAviso;

                pNotificado := FormMensajeTagsVencidos._Notificado;                                     // SS_960_PDO_20120104
            end;                                                                                        //SS-960-ALA-20110423
        end;
    finally
        FreeAndNil(FormMensajeTagsVencidos);
    end;
end;

function ObtenerPorcentajeIVA(Conn: TADOConnection): Extended;
begin
     //se usa now porque la emision de los creditos en la base es getdate
    Result := QueryGetValueInt(Conn,
                                'SELECT dbo.ObtenerIVA(''' + FormatDateTime('yyyymmdd',
                                NowBase(Conn)) + ''')');
    Result := Result / 100;       // esto devuelve un 0.19 porque en la base dice 1900
end;
{-----------------------------------------------------------------------------
Function Name   :   CalcularIVADeImporte
Author          :   Alejandro Labra
Date Created    :   30-Noviembre-2011
Description     :   Calcula el Iva de un Importe.
Firma           :   SS-1012-ALA-20111128
-----------------------------------------------------------------------------}
function CalcularIVADeImporte(Conn: TADOConnection; Importe : Extended) : Int64;
begin
    Result := CalcularIVADeImporte(ObtenerPorcentajeIVA(Conn), Importe);
end;

{-----------------------------------------------------------------------------
Function Name   :   CalcularIVADeImporte
Author          :   Alejandro Labra
Date Created    :   30-Noviembre-2011
Description     :   Calcula el Iva de un Importe.
Firma           :   SS-1012-ALA-20111128
-----------------------------------------------------------------------------}
function CalcularIVADeImporte(PorcentajeIva : Extended ; Importe : Extended) : Int64;
begin
    Result := RoundCS((Importe / ( 1 + PorcentajeIva / 100 )) * (PorcentajeIva / 100));
end;

{-----------------------------------------------------------------------------
Function Name   :   CalcularImporteNetoDeImporte
Author          :   Alejandro Labra
Date Created    :   30-Noviembre-2011
Description     :   Calcula el Neto de un Importe.
Firma           :   SS-1012-ALA-20111128
-----------------------------------------------------------------------------}
function CalcularImporteNetoDeImporte(Conn: TADOConnection; Importe : Extended) : Int64;
begin
    Result:= CalcularImporteNetoDeImporte(ObtenerPorcentajeIVA(Conn), Importe);
end;
{-----------------------------------------------------------------------------
Function Name   :   CalcularImporteNetoDeImporte
Author          :   Alejandro Labra
Date Created    :   30-Noviembre-2011
Description     :   Calcula el Neto de un Importe.
Firma           :   SS-1012-ALA-20111128
-----------------------------------------------------------------------------}
function CalcularImporteNetoDeImporte(PorcentajeIva : Extended; Importe : Extended) : Int64;
begin
    Result:= RoundCS(Importe / ( 1 + PorcentajeIva / 100 ));
end;
{-----------------------------------------------------------------------------
Function Name   :   RoundCS
Author          :   Alejandro Labra
Date Created    :   30-Noviembre-2011
Description     :   Realiza el Round de un valor, ya que el Round de Delphi
                    no funciona 100% correcto.
                    Ver: http://www.festra.com/eng/tip-rounding.htm
Firma           :   SS-1012-ALA-20111128
-----------------------------------------------------------------------------}
function RoundCS(Val : Extended) : Int64;
begin
    Result:= Trunc(Val);       // extract the integer part
    if Frac(Val) >= 0.5 then   // if fractional part >= 0.5 then...
        Result:= Result + 1;   // ...add 1
end;

procedure CargarComboOtrasConcesionarias(Conn: TADOConnection; Combo: TVariantComboBox; TieneSinEspecificar : Boolean = False; SoloFacturaCN: Boolean = False);     // SS_1015_HUR_20120409
    resourcestring                                                                                                                                                  // SS_1015_HUR_20120409
        MSG_ERROR_EJECUCION1 = 'Se produjo un Error al ejecutar el SP %s.';                                                                                         // SS_1015_HUR_20120409
        MSG_ERROR_EJECUCION2 = 'Error al ejecutar el SP %s. Error: %s';                                                                                             // SS_1015_HUR_20120409
        MSG_ERROR_CAPTION = 'Error Ejecuci�n Store Procedure';                                                                                                      // SS_1015_HUR_20120409
        //CONST_NOMBRECORTO_CN = 'CN';                                          //SS_1147_MCA_20140303
    var                                                                                                                                                             // SS_1015_HUR_20120409
        ADOStoredProc: TADOStoredProc;                                                                                                                              // SS_1015_HUR_20120409
begin                                                                                                                                                               // SS_1015_HUR_20120409
    try                                                                                                                                                             // SS_1015_HUR_20120409
        ADOStoredProc := TADOStoredProc.Create(nil);                                                                                                                // SS_1015_HUR_20120409
        with ADOStoredProc, Combo do try                                                                                                                            // SS_1015_HUR_20120409
            Connection := Conn;                                                                                                                                     // SS_1015_HUR_20120409
            ProcedureName := 'ObtenerConcesionarias';                                                                                                               // SS_1015_HUR_20120409
            Open;                                                                                                                                                   // SS_1015_HUR_20120409
            Clear;                                                                                                                                                  // SS_1015_HUR_20120409
            if TieneSinEspecificar then Items.Add(SIN_ESPECIFICAR, -1);                                                                                             // SS_1015_HUR_20120409
            while not Eof do begin                                                                                                                                  // SS_1015_HUR_20120409
                //if not(FieldByName('NombreCorto').AsString = CONST_NOMBRECORTO_CN) then                                 //SS_1147_MCA_20140303                                                                        // SS_1015_HUR_20120409
                if not(FieldByName('NombreCorto').AsString = ObtenerNombreCortoConcesionariaNativa) then                 //SS_1147_MCA_20140303                                                                        // SS_1015_HUR_20120409
                begin                                                                                                                                               // SS_1015_HUR_20120409
                    if SoloFacturaCN then                                                                                                                           // SS_1015_HUR_20120409
                    begin                                                                                                                                           // SS_1015_HUR_20120409
                      if FieldByName('TransitosLosFacturaCN').AsBoolean then                                                                                        // SS_1015_HUR_20120409
                        Items.Add(FieldByName('Descripcion').AsString, FieldByName('CodigoConcesionaria').AsInteger);                                               // SS_1015_HUR_20120409
                    end                                                                                                                                             // SS_1015_HUR_20120409
                    else                                                                                                                                            // SS_1015_HUR_20120409
                      Items.Add(FieldByName('Descripcion').AsString, FieldByName('CodigoConcesionaria').AsInteger);                                                 // SS_1015_HUR_20120409
                end;                                                                                                                                                // SS_1015_HUR_20120409

                Next;                                                                                                                                               // SS_1015_HUR_20120409
            end;                                                                                                                                                    // SS_1015_HUR_20120409
            ItemIndex := 0;                                                                                                                                         // SS_1015_HUR_20120409
        except                                                                                                                                                      // SS_1015_HUR_20120409
            on e: Exception do begin                                                                                                                                // SS_1015_HUR_20120409
                MsgBoxErr(                                                                                                                                          // SS_1015_HUR_20120409
                    Format(MSG_ERROR_EJECUCION1, [ProcedureName]),                                                                                                  // SS_1015_HUR_20120409
                    Format(MSG_ERROR_EJECUCION2, [ProcedureName, e.Message]),                                                                                       // SS_1015_HUR_20120409
                    MSG_ERROR_CAPTION,                                                                                                                              // SS_1015_HUR_20120409
                    MB_ICONERROR);                                                                                                                                  // SS_1015_HUR_20120409
            end;                                                                                                                                                    // SS_1015_HUR_20120409
        end;                                                                                                                                                        // SS_1015_HUR_20120409
    finally                                                                                                                                                         // SS_1015_HUR_20120409
        if Assigned(ADOStoredProc) then begin                                                                                                                       // SS_1015_HUR_20120409
            if ADOStoredProc.Active then ADOStoredProc.Close;                                                                                                       // SS_1015_HUR_20120409
            FreeAndNil(ADOStoredProc);                                                                                                                              // SS_1015_HUR_20120409
        end;                                                                                                                                                        // SS_1015_HUR_20120409
    end;                                                                                                                                                            // SS_1015_HUR_20120409
end;                                                                                                                                                                // SS_1015_HUR_20120409

{-----------------------------------------------------------------------------
Function Name   :   CargarComboGestionJudicialSubCategorias
Author          :   Claudio Quezada
Date Created    :   28-Mayo-2012
Description     :   Carga en un Combobox las SubCategorias.
                    Se cambia el nombre del SP y de los campos
Firma           :   SS_1047_CQU_20120528
-----------------------------------------------------------------------------}
procedure CargarComboGestionJudicialSubCategorias(Conn: TADOConnection; Combo: TVariantComboBox; CodigoCategoria : Integer; TieneSinEspecificar : Boolean = False );
    resourcestring
        MSG_ERROR_EJECUCION1 = 'Se produjo un Error al ejecutar el SP %s.';
        MSG_ERROR_EJECUCION2 = 'Error al ejecutar el SP %s. Error: %s';
        MSG_ERROR_CAPTION = 'Error Ejecuci�n Store Procedure';
    var
        ADOStoredProc: TADOStoredProc;
        CodigoDescripcion : string;
begin
    try
        ADOStoredProc := TADOStoredProc.Create(nil);
        with ADOStoredProc, Combo do try
            Connection := Conn;
            ProcedureName := 'GestionJudicial_ObtenerSubCategorias';
            Parameters.Refresh;
            Parameters.ParamByName('@CodigoCategoria').Value := CodigoCategoria;
            Open;
            Clear;
            if TieneSinEspecificar then Items.Add(SIN_ESPECIFICAR, -1);
            while not Eof do begin
                CodigoDescripcion := FieldByName('CodigoSubCategoria').AsString + ' - ' + FieldByName('DescripcionSubCategoria').AsString;
                Items.Add(CodigoDescripcion, FieldByName('CodigoSubCategoria').AsInteger);
                Next;
            end;
            ItemIndex := 0;
        except
            on e: Exception do begin
                MsgBoxErr(
                    Format(MSG_ERROR_EJECUCION1, [ProcedureName]),
                    Format(MSG_ERROR_EJECUCION2, [ProcedureName, e.Message]),
                    MSG_ERROR_CAPTION,
                    MB_ICONERROR);
            end;
        end;
    finally
        if Assigned(ADOStoredProc) then begin
            if ADOStoredProc.Active then ADOStoredProc.Close;
            FreeAndNil(ADOStoredProc);
        end;
    end;
end;

{-----------------------------------------------------------------------------
Function Name   :   CargarComboGestionJudicialGestion
Author          :   Claudio Quezada
Date Created    :   28-Mayo-2012
Description     :   Carga en un Combobox las Gestiones.
                    Se cambia el nombre del SP y de los campos
Firma           :   SS_1047_CQU_20120528
-----------------------------------------------------------------------------}
procedure CargarComboGestionJudicialGestion(Conn: TADOConnection; Combo: TVariantComboBox; CodigoSubCategoria:Integer; TieneSinEspecificar : Boolean = False );
    resourcestring
        MSG_ERROR_EJECUCION1 = 'Se produjo un Error al ejecutar el SP %s.';
        MSG_ERROR_EJECUCION2 = 'Error al ejecutar el SP %s. Error: %s';
        MSG_ERROR_CAPTION = 'Error Ejecuci�n Store Procedure';
    var
        ADOStoredProc: TADOStoredProc;
begin
    try
        ADOStoredProc := TADOStoredProc.Create(nil);
        with ADOStoredProc, Combo do try
            Connection := Conn;
            ProcedureName := 'GestionJudicial_ObtenerGestion';
            Parameters.Refresh;
            Parameters.ParamByName('@CodigoSubCategoria').Value := CodigoSubCategoria;
            Open;
            Clear;
            if TieneSinEspecificar then Items.Add(SIN_ESPECIFICAR, -1);
            while not Eof do begin
                Items.Add(FieldByName('DescripcionGestion').AsString, FieldByName('CodigoGestion').AsInteger);
                Next;
            end;
            ItemIndex := 0;
        except
            on e: Exception do begin
                MsgBoxErr(
                    Format(MSG_ERROR_EJECUCION1, [ProcedureName]),
                    Format(MSG_ERROR_EJECUCION2, [ProcedureName, e.Message]),
                    MSG_ERROR_CAPTION,
                    MB_ICONERROR);
            end;
        end;
    finally
        if Assigned(ADOStoredProc) then begin
            if ADOStoredProc.Active then ADOStoredProc.Close;
            FreeAndNil(ADOStoredProc);
        end;
    end;

end;

{-----------------------------------------------------------------------------
Function Name   :   CargarComboTribunales
Author          :   Claudio Quezada
Date Created    :   28-Mayo-2012
Description     :   Carga en un Combobox los Tribunales.
Firma           :   SS_1047_CQU_20120528
-----------------------------------------------------------------------------}
procedure CargarComboTribunales(Conn: TADOConnection; Combo: TVariantComboBox; CodigoTribunal:Integer = 0; TieneSinEspecificar : Boolean = False );
    resourcestring
        MSG_ERROR_EJECUCION1 = 'Se produjo un Error al ejecutar el SP %s.';
        MSG_ERROR_EJECUCION2 = 'Error al ejecutar el SP %s. Error: %s';
        MSG_ERROR_CAPTION = 'Error Ejecuci�n Store Procedure';
    var
        ADOStoredProc: TADOStoredProc;
begin
    try
        ADOStoredProc := TADOStoredProc.Create(nil);
        with ADOStoredProc, Combo do try
            Connection := Conn;
            ProcedureName := 'ObtenerTribunalesDeJusticia';
            Parameters.Refresh;
            Parameters.ParamByName('@CodigoTribunal').Value := CodigoTribunal;
            Open;
            Clear;
            if TieneSinEspecificar then Items.Add(SIN_ESPECIFICAR, -1);
            while not Eof do begin
                Items.Add(FieldByName('Descripcion').AsString, FieldByName('CodigoTribunal').AsInteger);
                Next;
            end;
            ItemIndex := 0;
        except
            on e: Exception do begin
                MsgBoxErr(
                    Format(MSG_ERROR_EJECUCION1, [ProcedureName]),
                    Format(MSG_ERROR_EJECUCION2, [ProcedureName, e.Message]),
                    MSG_ERROR_CAPTION,
                    MB_ICONERROR);
            end;
        end;
    finally
        if Assigned(ADOStoredProc) then begin
            if ADOStoredProc.Active then ADOStoredProc.Close;
            FreeAndNil(ADOStoredProc);
        end;
    end;

end;

{-----------------------------------------------------------------------------
Function Name   :   CargarComboGestionJudicialCategorias
Author          :   Claudio Quezada
Date Created    :   29-Junio-2012
Description     :   Carga en un Combobox las Categorias.
Firma           :   SS_1047_CQU_20120528
-----------------------------------------------------------------------------}
procedure CargarComboGestionJudicialCategorias(Conn: TADOConnection; Combo: TVariantComboBox; TieneSinEspecificar : Boolean = False );
    resourcestring
        MSG_ERROR_EJECUCION1 = 'Se produjo un Error al ejecutar el SP %s.';
        MSG_ERROR_EJECUCION2 = 'Error al ejecutar el SP %s. Error: %s';
        MSG_ERROR_CAPTION = 'Error Ejecuci�n Store Procedure';
    var
        ADOStoredProc: TADOStoredProc;
        CodigoDescripcion : string;
begin
    try
        ADOStoredProc := TADOStoredProc.Create(nil);
        with ADOStoredProc, Combo do try
            Connection := Conn;
            ProcedureName := 'GestionJudicial_ObtenerCategorias';
            Open;
            Clear;
            if TieneSinEspecificar then Items.Add(SIN_ESPECIFICAR, -1);
            while not Eof do begin
                CodigoDescripcion := FieldByName('CodigoCategoria').AsString + ' - ' + FieldByName('Descripcion').AsString;
                Items.Add(CodigoDescripcion, FieldByName('CodigoCategoria').AsInteger);
                Next;
            end;
            ItemIndex := 0;
        except
            on e: Exception do begin
                MsgBoxErr(
                    Format(MSG_ERROR_EJECUCION1, [ProcedureName]),
                    Format(MSG_ERROR_EJECUCION2, [ProcedureName, e.Message]),
                    MSG_ERROR_CAPTION,
                    MB_ICONERROR);
            end;
        end;
    finally
        if Assigned(ADOStoredProc) then begin
            if ADOStoredProc.Active then ADOStoredProc.Close;
            FreeAndNil(ADOStoredProc);
        end;
    end;
end;

// SS_1050_PDO_20120605 Inicio bloque
Procedure RefinanciacionRenumerarCuotas(Conn: TADOConnection; CodigoRefinanciacion: Integer);
    resourcestring
        MSG_ERROR_EJECUCION = 'Error al ejecutar el SP %s. Error: %s';
    var
        ADOStoredProc: TADOStoredProc;
begin
    try
        ADOStoredProc := TADOStoredProc.Create(nil);

        with ADOStoredProc do try
            Connection    := Conn;
            ProcedureName := 'ActualizarRefinanciacionNumeracionCuotas';
            Parameters.Refresh;
            Parameters.ParamByName('@CodigoRefinanciacion').Value := CodigoRefinanciacion;
            ExecProc;

        except
            on e: Exception do begin
                raise Exception.Create(Format(MSG_ERROR_EJECUCION, [ProcedureName, e.Message]));
            end;
        end;
    finally
        if Assigned(ADOStoredProc) then begin
            if ADOStoredProc.Active then ADOStoredProc.Close;
            FreeAndNil(ADOStoredProc);
        end;
    end;
end;
// SS_1050_PDO_20120605 Fin Bloque

 
//BEGIN:SS_1120_MVI_20130820 -------------------------------------------------
{-----------------------------------------------------------------------------
Procedure Name  :   ItemComboBoxColor
Date Created    :   20-Agosto-2013
Description     :   Recibe por parametro los atributos de la funci�n DrawItem de
                    un ComboBox, m�s el valor por el que se debe colorear el item del combo.
                    Dentro se valida el valor, se asigna color y se escribe.
                    En caso de no venir un valor, se asignan los valores estandar para el
                    color del fondo y el color de la fuente del item del combo.
Firma           :   SS_1120_MVI_20130820
-----------------------------------------------------------------------------}
procedure ItemComboBoxColor(Control: TWinControl; position: Integer; R: TRect; State: TOwnerDrawState; FontColor: TColor = clBlack; FontBold: Boolean = False);
var
        BackGroundColor: TColor;
        Lugar: Integer;
        Opcion: string;
begin
     BackGroundColor     :=    clWhite;

    if (Control as TVariantComboBox).DroppedDown then begin

        if (odselected in State) then begin

          BackGroundColor   :=    clHighlight;
          FontColor         :=    clWhite;

        end;



    end;

   with (Control as TVariantComboBox) do begin

        Canvas.Brush.color    :=    BackGroundColor;
        Canvas.FillRect(R);
        Canvas.Font.Color     :=    FontColor;


       if FontBold then begin

        	Canvas.Font.Style   :=    Canvas.Font.Style + [fsBold]

        end
       else begin

        	Canvas.Font.Style   :=    Canvas.Font.Style - [fsBold]

        end;


        // Se identifica si alguno de los valores a imprimir posee texto en baja
        // para eliminar el texto, ya que se utiliz� para identificar el convenio a pintar en rojo.
        if Pos( TXT_CONVENIO_DE_BAJA,  Items[position].Caption ) > 0  then begin

          Lugar :=  Pos( TXT_CONVENIO_DE_BAJA,  Items[position].Caption);
          Opcion := Copy( Items[position].Caption, 0, Lugar);

        end
        else begin

            Opcion := Items[position].Caption;

        end;

        //INICIO: TASK_012_ECA_20160514
        if Pos('Predeterminado',  Items[position].Caption ) > 0  then begin

          Lugar :=  Pos('Predeterminado',  Items[position].Caption);
          Opcion := Copy( Items[position].Caption, 0, Lugar-1);

        end;
        //FIN: TASK_012_ECA_20160514

        Canvas.TextOut( R.Left, R.Top, Opcion );

   end;
end;
//END:SS_1120_MVI_20130820 ----------------------------------------------------

//BEGIN : SS_925_NDR_20131122--------------------------------------------------
function ValidarVersion( Conn: TADOConnection;
                         CodigoSistema:Integer;
                         Menu: TMenu;
                         const PermisosAdicionales: Array of TPermisoAdicional
                       ): Boolean;  overload;
Var
	Qry: TADOQuery;
    VersionOk: boolean;
    MajorVersion, MinorVersion, Release, Build: Word;
begin
    VersionOk := False;
	Qry := TAdoQuery.Create(nil);
	try
        { INICIO : 20160307 MGO
		Qry.Connection := Conn;
        }
        Qry.Connection := DMConnections.BaseBO_Master;
        // FIN : 20160307 MGO    
		Qry.SQL.Text := 'SELECT TOP 1 * FROM SISTEMASVERSIONES  WITH (NOLOCK) WHERE CODIGOSISTEMA = ' + inttostr(CodigoSistema) +  ' ORDER BY FechaHora DESC';
		Qry.Open;
        if Qry.RecordCount > 0 then begin
            GetFileVersion(GetExeName, MajorVersion, MinorVersion, Release, Build);
            VersionOk :=  (Qry.FieldByName('MajorVersion').AsInteger = MajorVersion) and
                          (Qry.FieldByName('MinorVersion').AsInteger = MinorVersion) and
                          (Qry.FieldByName('Release').AsInteger = Release);
            if VersionOk then
            begin
              if Qry.FieldByName('GenerarMenues').AsBoolean = True then
              begin
                if GenerateMenuFile(Menu, CodigoSistema, DMConnections.BaseCAC, PermisosAdicionales) then
                begin
                  Qry.SQL.Text := 'UPDATE SISTEMASVERSIONES  SET GenerarMenues=0 WHERE CODIGOSISTEMA = ' + inttostr(CodigoSistema) +
                                  ' AND MinorVersion='+inttostr(MinorVersion)+
                                  ' AND MajorVersion='+inttostr(MajorVersion)+
                                  ' AND Release='+inttostr(Release)+
                                  ' AND GenerarMenues=1' ;
                  Qry.ExecSQL;
                end
                else
                begin
                  VersionOk := false;
                end;
              end;
            end;
        end
        else
            VersionOk := false;
	finally
		Qry.Close;
		Qry.Free;
    result := VersionOk;
	end;
end;
//END : SS_925_NDR_20131122-----------------------------------------------------

function ObtenerCodigoConcesionariaNativa(Conn: TADOConnection = nil): integer;	                    //SS_1397_MGO_20151028						//SS_1147_MCA_20140408
begin																			//SS_1147_MCA_20140408
    if not Assigned(Conn) then Conn := DMConnections.BaseCAC;                                       //SS_1397_MGO_20151028 //SS_1397_MGO_20151027
    //Result:= QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.ObtenerConcesionariaNativa()');  //SS_1397_MGO_20151028
    Result:= QueryGetValueInt(Conn, 'SELECT dbo.ObtenerConcesionariaNativa()');	                    //SS_1397_MGO_20151028 //SS_1147_MCA_20140408
end;                                                                            //SS_1147_MCA_20140408

function ObtenerNombreConcesionariaNativa(): string;							//SS_1147_MCA_20140408
begin                                                                           //SS_1147_MCA_20140408
    Result:= QueryGetStringValue(DMConnections.BaseCAC, 'SELECT dbo.ObtenerNombreConcesionariaNativa()');	//SS_1147_MCA_20140408
end;                                                                            //SS_1147_MCA_20140408

function ObtenerNombreCortoConcesionariaNativa(): string;						//SS_1147_MCA_20140408
begin                                                                           //SS_1147_MCA_20140408
    Result:= QueryGetStringValue(DMConnections.BaseCAC, 'SELECT dbo.ObtenerNombreCortoConcesionaria(dbo.ObtenerConcesionariaNativa())');	//SS_1147_MCA_20140408
end;                                                                            //SS_1147_MCA_20140408

function ObtenerCodigoConcesionariaVS(Conn: TADOConnection = nil): Integer;                                     //SS_1397_MGO_20151028 //SS_1397_MGO_20151027
begin
    if not Assigned(Conn) then Conn := DMConnections.BaseCAC;                                                   //SS_1397_MGO_20151028 //SS_1397_MGO_20151027
    //Result:= QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.CONST_CODIGO_CONCESIONARIA_VESPUCIO_SUR()'); //SS_1397_MGO_20151028 //SS_1397_MGO_20151027
    Result:= QueryGetValueInt(Conn, 'SELECT dbo.CONST_CODIGO_CONCESIONARIA_VESPUCIO_SUR()');                    //SS_1397_MGO_20151028 //SS_1397_MGO_20151027
end;                                                                                                            //SS_1397_MGO_20151027

/// <summary>
/// Valida que un texto se pueda Parsear a Integer o Int64
/// </summary>
/// <param name="TextoEvaluar">Texto a evaluar si se puede parsear</param>
/// <param name="EsBigInt">Indica si es un Integer o Int64</param>
/// <returns>True, si puede convertirlo; False si no</returns>
/// <author>Claudio Quezada Ib��ez</author>
/// <seealso>SS_1151_CQU_20140103</seealso>
function EsEntero(TextoEvaluar : string; EsBigInt : Boolean = False) : Boolean;
var
    AuxiliarInt     : Integer;
    AuxiliarBigInt  : Int64;
begin
    Result := False;

    if EsBigInt then Result := TryStrToInt64(TextoEvaluar, AuxiliarBigInt)
    else Result := TryStrToInt(TextoEvaluar, AuxiliarInt);
end;

procedure CambiaColorMenu(Sender: TObject; ACanvas: TCanvas;                        // SS_1147_CQU_20140731
  ARect: TRect; Selected: Boolean; ColorMenu, ColorFont, ColorMenuSel, ColorFontSel : TColor);    // SS_1147_CQU_20140731
var
    TopLevel: Boolean;
begin                                                                               // SS_1147_CQU_20140731
    TopLevel := TMenuItem(Sender).GetParentComponent is TMainMenu;

    with ACanvas do begin                                                           // SS_1147_CQU_20140731
        If not Selected then begin                                                  // SS_1147_CQU_20140731
            Brush.Color :=  ColorMenu;                                              // SS_1147_CQU_20140731
            Font.Color  :=  ColorFont;                                              // SS_1147_CQU_20140731
        end else begin                                                              // SS_1147_CQU_20140731
            Brush.Color :=  ColorMenuSel;                                           // SS_1147_CQU_20140731
            Font.Color  :=  ColorFontSel;                                              // SS_1147_CQU_20140731
        end;                                                                        // SS_1147_CQU_20140731
        FillRect(ARect);                                                            // SS_1147_CQU_20140731
        DrawText(Handle,PChar((Sender as TMenuItem).Caption),-1,ARect,DT_VCENTER);  // SS_1147_CQU_20140731
    end;                                                                            // SS_1147_CQU_20140731
end;                                                                                // SS_1147_CQU_20140731

function ObtenerDocumentoInterno(TipoComprobanteFiscal: string; NumeroComprobanteFiscal: Int64;                     // SS_1147_CQU_20150316
                                var TipoComprobante: string; var NumeroComprobante: Int64) : Boolean;               // SS_1147_CQU_20150316
resourcestring                                                                                                      // SS_1147_CQU_20150316
    SQL_CONSULTA        = 'SELECT TipoComprobante, NumeroComprobante FROM Comprobantes'                             // SS_1147_CQU_20150316
                        + ' WITH(NOLOCK, INDEX = IX_ComprobanteFiscal)'                                             // SS_1147_CQU_20150316
                        + ' WHERE NumeroComprobanteFiscal = %d AND  TipoComprobanteFiscal = ''%s''';                // SS_1147_CQU_20150316
    MSG_ERROR_EJECUCION = 'Error al ejecutar obtener el TipoComprobante y NumeroComprobante interno'                // SS_1147_CQU_20150316
                        + ', mensaje del sistema: %s';                                                              // SS_1147_CQU_20150316
var                                                                                                                 // SS_1147_CQU_20150316
    Resultado : Boolean;                                                                                            // SS_1147_CQU_20150316
	Qry: TADOQuery;                                                                                                 // SS_1147_CQU_20150316
    TipoFiscal : Integer;                                                                                           // SS_1147_CQU_20150316
begin                                                                                                               // SS_1147_CQU_20150316
    Resultado := False;                                                                                             // SS_1147_CQU_20150316
    try                                                                                                             // SS_1147_CQU_20150316
        if EsEntero(TipoComprobanteFiscal, False) then begin                                                        // SS_1147_CQU_20150511
            // Aumenta TABULADO                                                                                     // SS_1147_CQU_20150511
            TipoFiscal := StrToInt(TipoComprobanteFiscal);                                                          // SS_1147_CQU_20150316
            case TipoFiscal of                                                                                      // SS_1147_CQU_20150316
    //          33 : TipoComprobanteFiscal := TC_FACTURA_EXENTA;                                                    // SS_1147_MBE_20150413
              33 : TipoComprobanteFiscal := TC_FACTURA_AFECTA;                                                      // SS_1147_MBE_20150413
              34 : TipoComprobanteFiscal := TC_FACTURA_EXENTA;                                                      // SS_1147_CQU_20150316
              39 : TipoComprobanteFiscal := TC_BOLETA_AFECTA;                                                       // SS_1147_CQU_20150316
              41 : TipoComprobanteFiscal := TC_BOLETA_EXENTA;                                                       // SS_1147_CQU_20150316
            end;                                                                                                    // SS_1147_CQU_20150316
        end;                                                                                                        // SS_1147_CQU_20150511
        Qry := TADOQuery.Create(nil);                                                                               // SS_1147_CQU_20150316
        try                                                                                                         // SS_1147_CQU_20150316
            Qry.Connection  := DMConnections.BaseCAC;                                                               // SS_1147_CQU_20150316
            Qry.SQL.Text    := Format(SQL_CONSULTA, [NumeroComprobanteFiscal, TipoComprobanteFiscal]);              // SS_1147_CQU_20150316
            Qry.Open;                                                                                               // SS_1147_CQU_20150316
            if Qry.RecordCount > 0 then begin                                                                       // SS_1147_CQU_20150316
                TipoComprobante := Qry.FieldByName('TipoComprobante').AsString;                                     // SS_1147_CQU_20150316
                NumeroComprobante := Qry.FieldByName('NumeroComprobante').AsVariant;                                // SS_1147_CQU_20150316
            end;                                                                                                    // SS_1147_CQU_20150316
        except                                                                                                      // SS_1147_CQU_20150316
            on e: Exception do begin                                                                                // SS_1147_CQU_20150316
                raise Exception.Create(Format(MSG_ERROR_EJECUCION, [e.Message]));                                   // SS_1147_CQU_20150316
            end;                                                                                                    // SS_1147_CQU_20150316
        end;                                                                                                        // SS_1147_CQU_20150316
    finally                                                                                                         // SS_1147_CQU_20150316
		Qry.Close;                                                                                                  // SS_1147_CQU_20150316
		Qry.Free;                                                                                                   // SS_1147_CQU_20150316
    end;                                                                                                            // SS_1147_CQU_20150316
end;                                                                                                                // SS_1147_CQU_20150316


procedure EstaClienteConMensajeEspecial(Conn: TADOConnection; NumeroDocumento: AnsiString); //SS_1408_MCA_20151027
resourcestring                                                                                      //SS_1408_MCA_20151027
    MSG_ERROR_OBTENER_MENSAJE_CLIENTE = 'Se ha producido un error al obtener el mensaje';           //SS_1408_MCA_20151027
var                                                                                                 //SS_1408_MCA_20151027
    Mensaje: AnsiString;                                                                            //SS_1408_MCA_20151027
    f: TFormMensajeACliente;                                                                        //SS_1408_MCA_20151027
begin                                                                                               //SS_1408_MCA_20151027
    try
        if QueryGetValueInt(Conn, 'SELECT 1 FROM ClientesAContactar WITH (NOLOCK) WHERE NumeroDocumento = ''' + NumeroDocumento + ''' AND FechaBaja IS NULL') = 1 then  //SS_1408_MCA_20151027
        begin                                                                                           //SS_1408_MCA_20151027
            ObtenerParametroGeneral(Conn, 'MENSAJES_A_CLIENTES_A_CONTACTAR', Mensaje);                  //SS_1408_MCA_20151027
            if Mensaje <> '' then begin                                                                 //SS_1408_MCA_20151027
                f := TFormMensajeACliente.Create(Nil);                                                 //SS_1408_MCA_20151027
                if f.Inicializar(Mensaje) then                                                          //SS_1408_MCA_20151027
                    f.ShowModal;                                                                        //SS_1408_MCA_20151027
                f.Release;                                                                              //SS_1408_MCA_20151027
            end;                                                                                        //SS_1408_MCA_20151027
        end;                                                                                            //SS_1408_MCA_20151027
    except                                                                                              //SS_1408_MCA_20151027
        on e:Exception do begin                                                                         //SS_1408_MCA_20151027
            MsgBoxErr(MSG_ERROR_OBTENER_MENSAJE_CLIENTE, MSG_ERROR_OBTENER_MENSAJE_CLIENTE, 'Cliente a Contactar', MB_ICONEXCLAMATION );    //SS_1408_MCA_20151027
        end;                                                                                            //SS_1408_MCA_20151027
    end;                                                                                                //SS_1408_MCA_20151027
end;                                                                                                    //SS_1408_MCA_20151027


procedure CrearNodosXML(var NodoRaiz: IXMLNode; Valores: TDiccionario);
var
  I: Integer;
  NewNodo: IXMLNode;
  valor: TClaveValor;
begin
    for I := 0 to Valores.Count - 1 do
    begin
        valor:= Valores.Items[i];
        NewNodo := NodoRaiz.AddChild(valor.Clave);
        NewNodo.Text := valor.Valor;
    end;
end;

function BooleanToString(Valor: Boolean): string;
begin
   Result:=iif(Valor,'1','0');
end;

initialization
	TransitoAnterior := 0;
	Accesos := TStringList.Create;
finalization
	Accesos.Free;
end.






