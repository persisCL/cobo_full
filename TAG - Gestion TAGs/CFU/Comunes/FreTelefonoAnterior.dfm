object FrameTelefono: TFrameTelefono
  Left = 0
  Top = 0
  Width = 696
  Height = 28
  TabOrder = 0
  object lblTelefono: TLabel
    Left = 2
    Top = 9
    Width = 87
    Height = 13
    Caption = 'Tel'#233'fono principal:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lblDesde: TLabel
    Left = 483
    Top = 9
    Width = 34
    Height = 13
    Caption = 'Desde:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lblHasta: TLabel
    Left = 589
    Top = 9
    Width = 31
    Height = 13
    Caption = 'Hasta:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object cbCodigosArea: TComboBox
    Left = 115
    Top = 3
    Width = 64
    Height = 21
    Hint = 'C'#243'digo de Area'
    Style = csDropDownList
    Color = 16444382
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ItemHeight = 13
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
  end
  object txtTelefono: TEdit
    Left = 184
    Top = 3
    Width = 159
    Height = 21
    Hint = 'N'#250'mero de tel'#233'fono'
    Color = 16444382
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    OnKeyPress = txtTelefonoKeyPress
  end
  object cbTipoTelefono: TVariantComboBox
    Left = 347
    Top = 3
    Width = 133
    Height = 21
    Style = vcsDropDownList
    Color = 16444382
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ItemHeight = 13
    ParentFont = False
    TabOrder = 2
    Items = <>
  end
  object teDesde: TTimeEdit
    Left = 521
    Top = 3
    Width = 65
    Height = 21
    Hint = 'Hora desde'
    AutoSelect = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    AllowEmpty = False
    Time = 0.375
    ShowSeconds = False
  end
  object teHasta: TTimeEdit
    Left = 624
    Top = 3
    Width = 65
    Height = 21
    Hint = 'Hora hasta'
    AutoSelect = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    AllowEmpty = False
    Time = 0.75
    ShowSeconds = False
  end
end
