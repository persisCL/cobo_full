unit LoginSetup;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Util, StdCtrls, ExtCtrls, CategoryButtons, LoginValuesEditor;

type
  TfrmLoginSetup = class(TForm)
    btnAceptar: TButton;
    btnCancelar: TButton;
    Secciones: TCategoryButtons;
    Bevel1: TBevel;
  private
    procedure DoButtonClick(Sender: TObject);
  public
    function Inicializar(AllowChangeDataBaseNames: Boolean = False): Boolean;
  end;

var
  frmLoginSetup: TfrmLoginSetup;

implementation

{$R *.dfm}

{ TfrmLoginSetup }

procedure TfrmLoginSetup.DoButtonClick(Sender: TObject);
var
    f: TfrmLoginValuesEditor;
begin
    Application.CreateForm(TfrmLoginValuesEditor, f);
    if f.Inicializar((Sender as TCategoryButtons)) then f.ShowModal;
    f.Release; 
end;

function TfrmLoginSetup.Inicializar(AllowChangeDataBaseNames: Boolean = False): Boolean;
Var
    Sections, SectValues: TStringList;
    Cat: TButtonCateGory;
    Butt: TButtonItem;
    i, x: Integer;
    CatName, EntryName: String;
begin
    Sections := TStringList.Create;
    SectValues := TStringList.Create;
    try
        //Leemos todas las secciones que tiene el InstallIni
        InstallIni.ReadSections(Sections);
        //Vamos cargando una a una con sus entradas
        for i:= 0 to Sections.Count -1 do begin
            //Creamos la categoria
            Cat:= Secciones.Categories.Insert(i);
            // Agregamos una Categoria (Section del ini)
            Secciones.Categories.AddItem(Cat, i);
            Secciones.Categories.Items[i].Color := clMenu;
            CatName:= Sections.Strings[i];
            Secciones.Categories.Items[i].Caption := CatName;
            //Ahora leemos las entradas de esta categoria
            SectValues.Clear;
            //Leemos los valores de cada Secci�n
            InstallIni.ReadSectionValues(CatName, SectValues);
            //Creamos los botones y los configuramos
            for x:= 0 to SectValues.Count -1 do begin
                Butt:= Secciones.Categories.Items[i].Items.Add;
                Secciones.Categories.Items[i].Items.AddItem(Butt, x);
                EntryName:= SectValues.Strings[x];
                Secciones.Categories.Items[i].Items[x].Caption := EntryName;
                Secciones.Categories.Items[i].Items[x].OnClick := DoButtonClick;
            end;
        end;
        // Colapsamos las categorias para que se muestren cerradas
        for i := 0 to Secciones.Categories.Count - 1 do begin
            Secciones.Categories.Items[i].Collapsed := True;
        end;
    finally
        FreeAndNil(Sections);
        FreeAndNil(SectValues);
    end;
    Result := True;
end;

end.

