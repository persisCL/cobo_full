object frmHabilitarDeshabilitarTag: TfrmHabilitarDeshabilitarTag
  Left = 301
  Top = 225
  BorderStyle = bsDialog
  ClientHeight = 172
  ClientWidth = 440
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  DesignSize = (
    440
    172)
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 4
    Top = 5
    Width = 433
    Height = 128
  end
  object lbl_tag_cliente: TLabel
    Left = 196
    Top = 26
    Width = 5
    Height = 13
    Anchors = [akLeft, akTop, akRight]
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label31: TLabel
    Left = 16
    Top = 26
    Width = 44
    Height = 13
    Caption = 'Cliente:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label32: TLabel
    Left = 14
    Top = 54
    Width = 45
    Height = 13
    Caption = 'Cuenta:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbl_tag: TLabel
    Left = 107
    Top = 78
    Width = 5
    Height = 13
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 15
    Top = 78
    Width = 88
    Height = 13
    Caption = 'N'#250'mero de tag:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 15
    Top = 106
    Width = 90
    Height = 13
    Caption = 'Motivo de Baja:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object txt_tag_cliente: TPickEdit
    Left = 105
    Top = 22
    Width = 91
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    Color = 16444382
    Enabled = True
    MaxLength = 10
    TabOrder = 0
    OnChange = txt_tag_clienteChange
    Decimals = 0
    EditorStyle = bteNumericEdit
    OnButtonClick = txt_tag_clienteButtonClick
  end
  object cb_tag_cuenta: TComboBox
    Left = 105
    Top = 51
    Width = 316
    Height = 21
    Style = csDropDownList
    Anchors = [akLeft, akTop, akRight]
    Color = 16444382
    ItemHeight = 13
    MaxLength = 4
    TabOrder = 1
    OnChange = cb_tag_cuentaChange
  end
  object cb_motivos: TComboBox
    Left = 105
    Top = 100
    Width = 315
    Height = 21
    Style = csDropDownList
    Anchors = [akLeft, akTop, akRight]
    Color = 16444382
    ItemHeight = 13
    TabOrder = 2
  end
  object btn_aceptar: TDPSButton
    Left = 270
    Top = 140
    Width = 83
    Anchors = [akRight, akBottom]
    Caption = '&Aceptar'
    Default = True
    Enabled = False
    TabOrder = 3
    OnClick = btn_aceptarClick
  end
  object btn_cancelar: TDPSButton
    Left = 358
    Top = 140
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Cerrar'
    TabOrder = 4
    OnClick = btn_cancelarClick
  end
  object Habilitar: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'HabilitarTAG;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@ContextMark'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@ContractSerialNumber'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 11
        Value = Null
      end>
    Left = 232
    Top = 21
  end
  object Inhabilitar: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'InhabilitarTAG;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@ContextMark'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@ContractSerialNumber'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 11
        Value = Null
      end
      item
        Name = '@Motivo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 272
    Top = 21
  end
end
