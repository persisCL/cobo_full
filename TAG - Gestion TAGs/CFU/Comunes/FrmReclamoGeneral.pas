{-------------------------------------------------------------------------------
 File Name: FrmReclamoGeneral.pas
 Author: DDeMarco
 Date Created:
 Language: ES-AR
 Description: Ingreso y edicion de reclamos

 Revision		: 1
 Author			: Nelson Droguett Sierra
 Date			: 29-Junio-2010
 Description	: (Ref. Fase 2) Se agrega concesionaria y SubTipoReclamo

  Firma        : SS-1006-NDR-20120802
 Description  : Validar concesionaria en reclamos
 -------------------------------------------------------------------------------}
unit FrmReclamoGeneral;

interface

uses
  //Reclamo
  DMConnection,                   //Coneccion a base de datos OP_CAC
  Util,                           //Nulldate
  UtilDB,                         //Rutinas para base de datos
  UtilProc,                       //Mensajes
  FreContactoReclamo,             //Frame contacto
  FreCompromisoOrdenServicio,     //Frame compromiso
  FreSolucionOrdenServicio,       //Frame solucion
  FreHistoriaOrdenServicio,       //Frame Historia
  FreFuenteReclamoOrdenServicio,  //Frame Fuente de Reclamo
  FreNumeroOrdenServicio,         //Frame Numero Orden Servicio
  FreRespuestaOrdenServicio,      //Frame Respuesta Orden Servicio
  OrdenesServicio,                //Registra la orden de  servicio
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls,  DB, ADODB,RStrings, ExtCtrls,
  VariantComboBox, FreSubTipoReclamoOrdenServicio,
  FreConcesionariaReclamoOrdenServicio;

type
  TFormReclamoGeneral = class(TForm)
    spObtenerOrdenServicio: TADOStoredProc;
    PAbajo: TPanel;
    PageControl: TPageControl;
    TabSheetDatos: TTabSheet;
    Ldetallesdelreclamo: TLabel;
    txtDetalle: TMemo;
    TabSheetProgreso: TTabSheet;
    TabSheetHistoria: TTabSheet;
    PDerecha: TPanel;
    CKRetenerOrden: TCheckBox;
    AceptarBTN: TButton;
    btnCancelar: TButton;
    FrameHistoriaOrdenServicio1: TFrameHistoriaOrdenServicio;
    Ldetalledelprogreso: TLabel;
    txtDetalleSolucion: TMemo;
    FrameContactoReclamo1: TFrameContactoReclamo;
    FrameCompromisoOrdenServicio1: TFrameCompromisoOrdenServicio;
    FrameSolucionOrdenServicio1: TFrameSolucionOrdenServicio;
    FrameFuenteReclamoOrdenServicio1: TFrameFuenteReclamoOrdenServicio;
    FrameNumeroOrdenServicio1: TFrameNumeroOrdenServicio;
    TabSheetRespuesta: TTabSheet;
    FrameRespuestaOrdenServicio1: TFrameRespuestaOrdenServicio;
    FrameConcesionariaReclamoOrdenServicio1: TFrameConcesionariaReclamoOrdenServicio;
    FrameSubTipoReclamoOrdenServicio1: TFrameSubTipoReclamoOrdenServicio;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure AceptarBTNClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    FTipoOrdenServicio : Integer;
    FCodigoOrdenServicio : Integer;
    FEditando : Boolean; //Indica si estoy editando o no
    Function GetCodigoOrdenServicio : Integer;
  public
    { Public declarations }
    Function Inicializar(TipoOrdenServicio : Integer; CodigoConvenio : Integer = 0) : Boolean;
    Function InicializarEditando(CodigoOrdenServicio : Integer) : Boolean;
    Function InicializarSolucionando(CodigoOrdenServicio : Integer) : Boolean;
    Property CodigoOrdenServicio: Integer Read GetCodigoOrdenServicio;
  end;


implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:  DDeMarco
  Date Created:
  Description:  Inicializaci�n de este formulario
  Parameters: TipoOrdenServicio: Integer; CodigoConvenio: Integer = 0
  Return Value: Boolean
-----------------------------------------------------------------------------}
Function TFormReclamoGeneral.Inicializar(TipoOrdenServicio : Integer; CodigoConvenio : Integer = 0) : Boolean;
Resourcestring
    MSG_ERROR_INIT =  'Error al inicializar Reclamo General';
    MSG_ERROR = 'Error';
Const
    STR_RECLAMO = 'Reclamo general - ';
Var
  	Sz: TSize;
Begin

    FTipoOrdenServicio := TipoOrdenServicio;
    try
        //Centro el form
        CenterForm(Self);

        //Ajusto el form al tama�o disponible
        SZ := GetFormClientSize(Application.MainForm);
  	    SetBounds(0, 0, SZ.cx, sz.cy);

        //Titulo
        Caption := STR_RECLAMO + QueryGetValue(DMConnections.BaseCAC, Format(
                   'select dbo.ObtenerDescripcionTipoOrdenServicio (%d)',
                   [TipoOrdenServicio]));
        PageControl.ActivePageIndex := 0;

        ActiveControl := FrameContactoReclamo1.txtNumeroDocumento; //txtDetalle

        Result := FrameContactoReclamo1.Inicializar and
                        FrameCompromisoOrdenServicio1.Inicializar(10) and
                                FrameSolucionOrdenServicio1.Inicializar and
                                    FrameRespuestaOrdenServicio1.Inicializar and
                                        //Rev.1 / 29-Junio-2010 / Nelson Droguett Sierra-----------------
                                        	FrameConcesionariaReclamoOrdenServicio1.Inicializar and
                                            	FrameSubTipoReclamoOrdenServicio1.Inicializar;
                                        //---------------------------------------------------------------


        if Result then FrameContactoReclamo1.CodigoConvenio := CodigoConvenio;

    except
        on E: Exception do begin
            MsgBoxErr(MSG_ERROR_INIT, E.Message, MSG_ERROR, MB_ICONSTOP);
            Result := False;
        end;
    end;

    //Termino de inicializar
    FrameFuenteReclamoOrdenServicio1.Inicializar;
    FrameNumeroOrdenServicio1.Visible := False;

    //No estoy Editando
    FEditando := False;

    //Pongo una fecha comprometida por defecto en funcion del reclamo
    if FrameCompromisoOrdenServicio1.FechaCompromiso = NULLDATE then FrameCompromisoOrdenServicio1.FechaCompromiso:=ObtenerFechaComprometidaOS(now, FTipoOrdenServicio, FrameFuenteReclamoOrdenServicio1.FuenteReclamo, FrameCompromisoOrdenServicio1.Prioridad);

    AddOSForm(self);
end;

{-----------------------------------------------------------------------------
  Function Name: InicializarEditando
  Author:  DDeMarco
  Date Created:
  Description: Modifico Reclamo que ya existe
  Parameters: CodigoOrdenServicio: Integer
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFormReclamoGeneral.InicializarEditando(CodigoOrdenServicio : Integer) : Boolean;

    //form read Only
    Procedure ReadOnly;
    begin
        FrameContactoReclamo1.Deshabilitar;
        FrameCompromisoOrdenServicio1.Enabled := False;
        FrameSolucionordenServicio1.Enabled := False;
        FrameRespuestaOrdenServicio1.Deshabilitar;
        txtdetalle.ReadOnly := True;
        txtDetalleSolucion.ReadOnly := True;
        CKRetenerOrden.Visible := False;
        btncancelar.Caption := 'Salir';
        aceptarbtn.Visible := False;
    end;

var
    Estado: String;
begin
    //asigo el codigoordenservicio recibido a una varible global
    FCodigoOrdenServicio := CodigoOrdenServicio;
    //Asigno los parametros
    with spObtenerOrdenServicio.Parameters do begin
        ParamByName('@CodigoOrdenServicio').Value := CodigoOrdenServicio;
    end;
    //Abro la consulta
    Result := OpenTables([spObtenerOrdenServicio]);
    //si no puede abrir la consulta sale
    if not Result then Exit;
    //Cargamos los datos
    Result := Inicializar(spObtenerOrdenServicio['TipoOrdenServicio']);
    //Obtengo el detalle, fuente y estado del reclamo
    with spObtenerOrdenServicio do begin
        txtDetalle.Text := FieldByName('ObservacionesSolicitante').AsString;
        txtDetalleSolucion.Text := FieldByName('ObservacionesEjecutante').AsString;
        Estado := FieldByName('Estado').AsString;
    end;
    //cargo los frames
    FrameCompromisoOrdenServicio1.LoadFromDataset(spObtenerOrdenServicio);
    FrameContactoReclamo1.LoadFromDataset(spObtenerOrdenServicio);
    FrameSolucionOrdenServicio1.LoadFromDataset(spObtenerOrdenServicio);
    FrameHistoriaOrdenServicio1.Inicializar(CodigoOrdenServicio);
    FrameFuenteReclamoOrdenServicio1.LoadFromDataset(spObtenerOrdenServicio);
    FrameRespuestaOrdenServicio1.LoadFromDataset(spObtenerOrdenServicio);
    //Rev.1 / 07-Julio-2010 / Nelson Droguett Sierra ----------------------------------
    FrameConcesionariaReclamoOrdenServicio1.LoadFromDataset(spObtenerOrdenServicio);
    FrameSubTipoReclamoOrdenServicio1.LoadFromDataset(spObtenerOrdenServicio);
    //FinRev.1-------------------------------------------------------------------------
    // Listo
    spObtenerOrdenServicio.Close;
    //si esta finalizada solo se puede leer
    if ((Estado = 'C') or (Estado = 'T')) then begin
        ReadOnly;
    end;
    //termino de inicializar
    FrameNumeroOrdenServicio1.Visible := True;
    FrameNumeroOrdenServicio1.Inicializar(inttostr(CodigoOrdenServicio));
    //por Default no retengo la orden de servicio
    CKRetenerOrden.Checked := False;
    //la fuente del reclamo no puede ser cambiada despues de creada la OS
    FrameFuenteReclamoOrdenServicio1.Deshabilitar;
    //Estoy Editando
    FEditando := True;

end;


{-----------------------------------------------------------------------------
  Function Name: InicializarSolucionando
  Author:  DDeMarco
  Date Created:
  Description: Soluciono un Reclamo
  Parameters: CodigoOrdenServicio: Integer
  Return Value: Boolean
-----------------------------------------------------------------------------}
Function TFormReclamoGeneral.InicializarSolucionando(CodigoOrdenServicio : Integer) : Boolean;
begin
    Result := InicializarEditando(CodigoOrdenServicio);
    if Result then begin
        PageControl.TabIndex := 1;
        FrameSolucionOrdenServicio1.Estado := 'T';
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: GetCodigoOrdenServicio
  Author:    lgisuk
  Date Created: 11/04/2005
  Description: obtengo el codigoordenservicio
  Parameters: None
  Return Value: integer
-----------------------------------------------------------------------------}
Function TFormReclamoGeneral.GetCodigoOrdenServicio: Integer;
begin
    try
        Result := StrToInt(FrameNumeroOrdenServicio1.CodigoOrdenServicio);
    except
        Result := 0;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: FormKeyPress
  Author:    DDeMarco
  Date Created:
  Description: permite cancelar
  Parameters: Sender: TObject; var Key: Char
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoGeneral.FormKeyPress(Sender: TObject; var Key: Char);
resourcestring
    MSG_CANCELAR_RECLAMO = '�Desea cancelar el reclamo?';
    MSG_CANCELAR_CAPTION = 'Cancelar reclamo';
begin
    if Key = #27 then
        if 	MsgBox(MSG_CANCELAR_RECLAMO, MSG_CANCELAR_CAPTION, MB_YESNO + MB_ICONQUESTION) = mrYes then  //TASK_020_JMA_20160616
            btnCancelar.Click;
end;

{-----------------------------------------------------------------------------
  Function Name: AceptarBTNClick
  Author: DDeMarco
  Date Created:
  Description: registra el reclamo
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoGeneral.AceptarBTNClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: Validar
      Author:    lgisuk
      Date Created: 16/06/2005
      Description:  valido el contenido del reclamo antes de guardarlo
      Parameters: None
      Return Value: Boolean
    -----------------------------------------------------------------------------}
    Function Validar: Boolean;
    Resourcestring
        MSG_ERROR_DET    = 'Debe indicar alg�n detalle para el reclamo';
        MSG_ERROR_FUENTE = 'Debe indicar la fuente del reclamo';
        MSG_ERROR_FECHA  = 'La Fecha de Compromiso Interna o Externa '+ CRLF +
                           'debe ser menor a la Fecha Comprometida con el Cliente';
    begin
        Result := False;

        //Validamos fuente de reclamo
        if FEditando = False then begin
             if not FrameFuenteReclamoOrdenServicio1.Validar then Exit;
        end;

        //si es un reclamo que va a ser cancelado, no valida el resto de los
        //campos para no forzar al usuario a que los complete y asi pueda
        //salir rapidamente.
        if FrameSolucionOrdenServicio1.Estado = 'C' then begin
            result:=true;
            exit;
        end;

        //Validamos identificacion de usuario y contacto
        if not FrameContactoReclamo1.Validar then Exit;

        //Validamos compromiso con el cliente
        if FEditando = false then begin
            if not FrameCompromisoOrdenServicio1.Validar then Exit;
        end;

        //Validamos detalle del reclamo
        if Trim(txtDetalle.Text) = '' then begin
            PageControl.ActivePageIndex := 0;
            MsgBoxBalloon(MSG_ERROR_DET, STR_ERROR, MB_ICONSTOP, txtDetalle);
            txtDetalle.SetFocus;
            Exit;
        end;

        //Validamos progreso / solucion
        if not FrameSolucionOrdenServicio1.Validar then Exit;
        //Si es un reclamo pendiente de respuesta Interna o Externa
        if ((FrameSolucionOrdenServicio1.Estado = 'I') or (FrameSolucionOrdenServicio1.Estado = 'E')) then begin
            //Valido que la fecha comprometida interna/externa
            //sea menor a la fecha comprometida con el cliente
            if (FrameCompromisoOrdenServicio1.txtFechaCompromiso.Date < FrameSolucionOrdenServicio1.EFechaCompromiso.Date) then begin
                MsgBoxBalloon(MSG_ERROR_FECHA, STR_ERROR, MB_ICONSTOP, FrameSolucionOrdenServicio1.EFechaCompromiso);
                FrameSolucionOrdenServicio1.EFechaCompromiso.SetFocus;
                Exit;
            end;
        end;

        if not FrameConcesionariaReclamoOrdenServicio1.Validar then Exit ;      //SS-1006-NDR-20120802

        Result := True;

    end;

Var
    OS: Integer;
begin

    //Valido el reclamo antes de guardarlo
    if not Validar then exit;

    // Guardamos
    //Rev.1 / 29-Junio-2010 / Nelson Droguet Sierra ----------------------------------
    OS := GuardarOrdenServicioBase(FCodigoOrdenServicio,
                                   FTipoOrdenServicio,
                                   FrameFuenteReclamoOrdenServicio1.FuenteReclamo,
                                   FrameContactoReclamo1,
                                   FrameCompromisoOrdenServicio1,
                                   FrameSolucionOrdenServicio1,
                                   txtDetalle.Text,
                                   txtDetalleSolucion.text,
                                   FrameRespuestaOrdenServicio1.ObservacionesRespuesta,
                                   FrameRespuestaOrdenServicio1.ObservacionesComentariosCliente,
                                   FrameRespuestaOrdenServicio1.Notificado,
                                   FrameRespuestaOrdenServicio1.Conforme,
                                   FrameConcesionariaReclamoOrdenServicio1,
                                   FrameSubTipoReclamoOrdenServicio1
                                   );
    //FinRev.1-------------------------------------------------------------------------
                                   
    if OS > 0 then begin

        //si es un reclamo nuevo
        if FEditando = False then begin

            //informo al operador el numero de Reclamo que se genero
            InformarNumeroReclamo(IntToStr(OS));
            DesbloquearOrdenServicio(OS);
        end;

        close;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnCancelarClick
  Author:    lgisuk
  Date Created: 11/04/2005
  Description: cierro el formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoGeneral.btnCancelarClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormDestroy
  Author:    lgisuk
  Date Created: 11/04/2005
  Description: remuevo al form de la lista de creados
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoGeneral.FormDestroy(Sender: TObject);
begin
    RemoveOSform(Self);
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 05/04/2005
  Description: desbloqueo la orden de servicio antes de salir
               y libero el formulario de memoria
  Parameters: Sender: TObject;var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoGeneral.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    //si se esta modificando un reclamo
    if FEditando = True then begin
        //verifico si desea desbloquear el reclamo
        if CKRetenerOrden.Checked = False then begin

            //Desbloqueo la orden de servicio
            DesbloquearOrdenServicio(FCodigoOrdenServicio);
        end;
    end;
    //lo libero de memoria
    Action := caFree;
end;

end.
