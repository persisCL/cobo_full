object FormListadoTurnos: TFormListadoTurnos
  Left = 79
  Top = 175
  Caption = 'Listado de Turnos'
  ClientHeight = 363
  ClientWidth = 854
  Color = clBtnFace
  Constraints.MinHeight = 300
  Constraints.MinWidth = 799
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 854
    Height = 80
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 854
      Height = 73
      Align = alTop
      Caption = ' Filtros '
      TabOrder = 0
      DesignSize = (
        854
        73)
      object Label1: TLabel
        Left = 11
        Top = 20
        Width = 68
        Height = 13
        Caption = 'Fecha  &desde:'
        FocusControl = txt_FechaDesde
      end
      object Label2: TLabel
        Left = 192
        Top = 20
        Width = 29
        Height = 13
        Caption = '&hasta:'
        FocusControl = txt_FechaHasta
      end
      object Label6: TLabel
        Left = 402
        Top = 20
        Width = 39
        Height = 13
        Caption = '&Usuario:'
      end
      object Label3: TLabel
        Left = 580
        Top = 20
        Width = 36
        Height = 13
        Caption = '&Estado:'
      end
      object lbl_PuntoEntrega: TLabel
        Left = 11
        Top = 46
        Width = 71
        Height = 13
        Caption = 'Punto Entrega:'
      end
      object lbl_PuntoVenta: TLabel
        Left = 381
        Top = 46
        Width = 62
        Height = 13
        Caption = 'Punto Venta:'
      end
      object txt_FechaDesde: TDateEdit
        Left = 87
        Top = 16
        Width = 91
        Height = 21
        AutoSelect = False
        TabOrder = 0
        Date = -693594.000000000000000000
      end
      object txt_FechaHasta: TDateEdit
        Left = 232
        Top = 16
        Width = 91
        Height = 21
        AutoSelect = False
        TabOrder = 1
        Date = 36526.000000000000000000
      end
      object txtUsuario: TEdit
        Left = 449
        Top = 16
        Width = 121
        Height = 21
        TabOrder = 2
      end
      object cbEstadosTurno: TComboBox
        Left = 620
        Top = 15
        Width = 128
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 3
      end
      object cb_PuntosEntrega: TVariantComboBox
        Left = 87
        Top = 42
        Width = 282
        Height = 21
        Style = vcsDropDownList
        DroppedWidth = 0
        ItemHeight = 13
        TabOrder = 5
        OnChange = cb_PuntosEntregaChange
        Items = <>
      end
      object cb_PuntosVenta: TVariantComboBox
        Left = 449
        Top = 42
        Width = 299
        Height = 21
        Style = vcsDropDownList
        DroppedWidth = 0
        ItemHeight = 13
        TabOrder = 6
        Items = <>
      end
      object btn_Filtrar: TButton
        Left = 767
        Top = 10
        Width = 75
        Height = 25
        Hint = 'Filtrar Solicitud'
        Anchors = [akTop, akRight]
        Caption = '&Filtrar'
        Default = True
        TabOrder = 4
        OnClick = btn_FiltrarClick
      end
      object btn_Limpiar: TButton
        Left = 767
        Top = 40
        Width = 75
        Height = 25
        Hint = 'Filtrar Solicitud'
        Anchors = [akTop, akRight]
        Caption = '&Limpiar'
        TabOrder = 7
        OnClick = btn_LimpiarClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 80
    Width = 854
    Height = 242
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object dbl_Turnos: TDBListEx
      Left = 0
      Top = 0
      Width = 854
      Height = 242
      Align = alClient
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 100
          Header.Caption = 'Usuario'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Sorting = csAscending
          IsLink = False
          OnHeaderClick = dbl_SolicitudColumns0HeaderClick
          FieldName = 'CodigoUsuario'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 120
          Header.Caption = 'Fecha Apertura'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          OnHeaderClick = dbl_SolicitudColumns0HeaderClick
          FieldName = 'FechaHoraApertura'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 120
          Header.Caption = 'Fecha Cierre'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          OnHeaderClick = dbl_SolicitudColumns0HeaderClick
          FieldName = 'FechaHoraCierre'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 170
          Header.Caption = 'Nombre Usuario'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          OnHeaderClick = dbl_SolicitudColumns0HeaderClick
          FieldName = 'NombrePersona'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 80
          Header.Caption = 'Estado'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          OnHeaderClick = dbl_SolicitudColumns0HeaderClick
          FieldName = 'DescripcionEstado'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 100
          Header.Caption = 'Usuario de Cierre'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          OnHeaderClick = dbl_SolicitudColumns0HeaderClick
          FieldName = 'CodigoUsuarioCierre'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 110
          Header.Caption = 'Punto de Venta'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          OnHeaderClick = dbl_SolicitudColumns0HeaderClick
          FieldName = 'PuntoVenta'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 100
          Header.Caption = 'Punto Entrega'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'PuntoEntrega'
        end>
      DataSource = dsListadoTurnos
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 322
    Width = 854
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object Panel4: TPanel
      Left = 0
      Top = 0
      Width = 854
      Height = 41
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 0
      DesignSize = (
        854
        41)
      object BtnSalir: TButton
        Left = 751
        Top = 7
        Width = 79
        Height = 26
        Hint = 'Salir del visualizador de Solicitudes'
        Anchors = [akRight, akBottom]
        Cancel = True
        Caption = '&Salir'
        TabOrder = 0
        OnClick = BtnSalirClick
      end
      object btnImprimir: TButton
        Left = 467
        Top = 7
        Width = 79
        Height = 26
        Hint = 'Imprimir los datos de un turno cerrado'
        Anchors = [akRight, akBottom]
        Caption = '&Imprimir Cierre'
        TabOrder = 1
        Visible = False
        OnClick = btnImprimirClick
      end
      object btnSupervisor: TButton
        Left = 629
        Top = 7
        Width = 118
        Height = 26
        Hint = 'Imprimir los datos de un turno cerrado'
        Anchors = [akRight, akBottom]
        Caption = 'Informe del Su&pervisor'
        TabOrder = 2
        OnClick = btnSupervisorClick
      end
      object btnCerrarTurno: TButton
        Left = 548
        Top = 7
        Width = 79
        Height = 26
        Hint = 'Cierra un turno'
        Anchors = [akRight, akBottom]
        Caption = '&Cerrar Turno'
        TabOrder = 3
        Visible = False
        OnClick = btnCerrarTurnoClick
      end
    end
  end
  object ObtenerListadoTurnos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    AfterOpen = ObtenerListadoTurnosAfterOpen
    AfterClose = ObtenerListadoTurnosAfterOpen
    AfterScroll = ObtenerListadoTurnosAfterScroll
    ProcedureName = 'ObtenerListadoTurnos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoPuntoEntrega'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoPuntoVenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@FechaHoraDesde'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaHoraHasta'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@Estado'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@OrderBy'
        Attributes = [paNullable]
        DataType = ftFixedChar
        Size = 100
        Value = Null
      end>
    Left = 168
    Top = 248
  end
  object dsListadoTurnos: TDataSource
    DataSet = ObtenerListadoTurnos
    Left = 200
    Top = 248
  end
end
