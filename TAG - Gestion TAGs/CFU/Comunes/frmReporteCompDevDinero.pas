{------------------------------------------------------------------------
                	frmReporteCompDevDinero

Author		: mbecerra
Date		: 20-Junio-2011
Description	: 		(Ref SS 959 )
            	Imprime el comprobante de devolución de Dinero,
                en una impresora Térmica


Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.

Firma        : SS_1147_MCA_20150226
Descripcion  : se reemplaza EXECPROC por OPEN.

Firma		 :	SS_1147_MBE_20150428
Description	 :	Se cambia el título y pie de página de la reserva y del comprobante
				devolución de dinero para ajustarse a la concesionaria nativa
                 
----------------------------------------------------------------------------}
unit frmReporteCompDevDinero;

interface

uses
  DMConnection,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ppParameter, ppCtrls, ppBands, ppPrnabl, ppClass, ppCache, ppProd,
  ppReport, DB, ADODB, ppDB, ppComm, ppRelatv, ppDBPipe, UtilRB, PeaProcs, ppTypes,
  ConstParametrosGenerales, UtilProc;                                           //SS_1147_NDR_20140710

type
  TReporteCompDevDineroForm = class(TForm)
    dsGenerarDetalleComprobante: TDataSource;
    rbiComprobanteDevDinero: TRBInterface;
    ppdbPipeComprobanteDevolucion: TppDBPipeline;
    spObtenerDetalleComprobanteDevolucionDinero: TADOStoredProc;
    rp_Recibo: TppReport;
    ppHeaderBand1: TppHeaderBand;
    pplbl_Concesionaria: TppLabel;
    pplbl_RUTConcesionaria: TppLabel;
    pplbl_DireccionConcesionaria: TppLabel;
    pplbl_LocalidadConcesionaria: TppLabel;
    pptxt_Nombre: TppDBText;
    pptxt_NumeroRecibo: TppDBText;
    pplbl_Nombre: TppLabel;
    pplbl_DatosCliente: TppLabel;
    pplbl_NumeroRecibo: TppLabel;
    pplbl_Fecha: TppLabel;
    ppLogo: TppImage;
    pplblLineaDatosCliente: TppLabel;
    ppDBText1: TppDBText;
    pplblSeparador: TppLabel;
    ppDetailBand3: TppDetailBand;
    pptxt_ImporteCancelado: TppDBText;
    ppDBText2: TppDBText;
    ppFooterBand1: TppFooterBand;
    ppSummaryBand3: TppSummaryBand;
    pplbl_TotalCancelado: TppLabel;
    pplbl_FirmaConcesionaria: TppLabel;
    pplbl_LineaTotalCancelado: TppLabel;
    pplbl_LineaFirmaConcesionaria: TppLabel;
    ppDBCalc1: TppDBCalc;
    ppParameterList1: TppParameterList;
    pprpReserva: TppReport;
    ppHeaderBand2: TppHeaderBand;
    ppDBText3: TppDBText;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppDBText4: TppDBText;
    ppLabel5: TppLabel;
    ppDBText6: TppDBText;
    ppDBText7: TppDBText;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLabel8: TppLabel;
    ppLabel10: TppLabel;
    ppLabel11: TppLabel;
    ppLabel12: TppLabel;
    ppImage1: TppImage;
    ppLabel13: TppLabel;
    ppDBText8: TppDBText;
    ppLabel15: TppLabel;
    ppDBText9: TppDBText;
    ppDetailBand1: TppDetailBand;
    ppDBText11: TppDBText;
    ppFooterBand2: TppFooterBand;
    ppSummaryBand1: TppSummaryBand;
    ppLabel17: TppLabel;
    ppParameterList2: TppParameterList;
    ppDBPipeReserva: TppDBPipeline;
    ppLabel20: TppLabel;
    ppDBText12: TppDBText;
    rbiReserva: TRBInterface;
    spObtenerReservaComprobantesDV: TADOStoredProc;
    dsReserva: TDataSource;
    ppLabel9: TppLabel;
    ppLabel14: TppLabel;
    ppLabel16: TppLabel;
    ppLabel18: TppLabel;
    ppLabel19: TppLabel;
    ppDBText5: TppDBText;
    ppLabel21: TppLabel;
    lblNroConvenio: TppLabel;
    txtNumeroConvenio: TppDBText;
    lblNotaFinal: TppLabel;
    ppLabel1: TppLabel;
    ppLabel22: TppLabel;
    spObtenerMaestroConcesionaria: TADOStoredProc;                    
  private
    { Private declarations }
  public
    { Public declarations }
    function Inicializar( NumeroComprobante : Int64) : Boolean;
    function InicializarReserva( IDReserva : Int64) : Boolean;
  end;

var
  ReporteCompDevDineroForm: TReporteCompDevDineroForm;

implementation

{$R *.dfm}

{------------------------------------------------------------------------
                	Inicializar

Author		: mbecerra
Date		: 20-Junio-2011
Description	: 		(Ref SS 959 )
            	Inicializa la impresión del comprobante devolución de dinero

----------------------------------------------------------------------------}
function TReporteCompDevDineroForm.Inicializar(NumeroComprobante: Int64) : Boolean;
var                                                                                                     //SS_1147_NDR_20140710
  sNombreCorto,																							//SS_1147_MBE_20150428
  RutaLogo, sRazonSocial,sRut,sDireccion,sLocalidad: AnsiString;                                        //SS_1147_NDR_20140710
begin
    ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
    try                                                                                                 //SS_1147_NDR_20140710
      ppImage1.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
    except                                                                                              //SS_1147_NDR_20140710
      On E: Exception do begin                                                                          //SS_1147_NDR_20140710
        Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
        MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
        Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
      end;                                                                                              //SS_1147_NDR_20140710
    end;                                                                                                //SS_1147_NDR_20140710

    with spObtenerMaestroConcesionaria do                                                               //SS_1147_NDR_20140710
    begin                                                                                               //SS_1147_NDR_20140710
      Close;                                                                                            //SS_1147_NDR_20140710
      Parameters.Refresh;                                                                               //SS_1147_NDR_20140710
      Parameters.ParamByName('@CodigoConcesionaria').Value := ObtenerCodigoConcesionariaNativa;         //SS_1147_NDR_20140710
      //ExecProc;                                                                                       //SS_1147_MCA_20150226  //SS_1147_NDR_20140710
      Open;
      sRazonSocial  := FieldByName('RazonSocialConcesionaria').AsString;                                //SS_1147_NDR_20140710
      sRut          := FormatFloat('#.#',FieldByName('RutConcesionaria').AsInteger) + '-' +             //SS_1147_NDR_20140710
                       FieldByName('DigitoVerificadorConcesionaria').AsString;                          //SS_1147_NDR_20140710
      sDireccion    := FieldByName('DireccionConcesionaria').AsString;                                  //SS_1147_NDR_20140710
      sLocalidad    := FieldByName('Comuna').AsString + '  ' +                                          //SS_1147_NDR_20140710
                       FieldByName('Region').AsString+ '  ' +                                           //SS_1147_NDR_20140710
                       FieldByName('Pais').AsString;                                                    //SS_1147_NDR_20140710
      sNombreCorto	:= FieldByName('NombreCorto').AsString;												//SS_1147_MBE_20150428


//---------------------- Inicio SS_1147_MBE_20150428
//
//  estas variables no son de este reporte    ------
//      ppLabel2.Caption := sRazonSocial;                                                                 //SS_1147_NDR_20140710
//      ppLabel3.Caption := sRut;                                                                         //SS_1147_NDR_20140710
//      ppLabel4.Caption := sDireccion;                                                                   //SS_1147_NDR_20140710
//      ppLabel5.Caption := sLocalidad;                                                                   //SS_1147_NDR_20140710

        pplbl_Concesionaria.Caption				:= sRazonSocial;
        pplbl_RUTConcesionaria.Caption			:= sRut;
        pplbl_DireccionConcesionaria.Caption	:= sDireccion;
        pplbl_LocalidadConcesionaria.Caption	:= sLocalidad;
        pplbl_FirmaConcesionaria.Caption		:= 'p. ' + sNombreCorto;                                 //SS_1147_MBE_20150428

// ------------------------- FIn  SS_1147_MBE_20150428

    end;                                                                                                //SS_1147_NDR_20140710

    with spObtenerDetalleComprobanteDevolucionDinero do begin
    	Parameters.Refresh;
        Parameters.ParamByName('@NumeroComprobante').Value	:= NumeroComprobante;
        Open;
    end;

    rbiComprobanteDevDinero.Execute(True);
    Result := True;
end;

{------------------------------------------------------------------------
                	InicializarReserva

Author		: mbecerra
Date		: 20-Junio-2011
Description	: 		(Ref SS 959 )
            	Inicializa la impresión de la Reserva de devolución de dinero

----------------------------------------------------------------------------}
function TReporteCompDevDineroForm.InicializarReserva( IDReserva : Int64) : Boolean;
var                                                                                                     //SS_1147_NDR_20140710
  sNombreCorto,																							//SS_1147_MBE_20150428
  RutaLogo, sRazonSocial,sRut,sDireccion,sLocalidad: AnsiString;                                        //SS_1147_NDR_20140710
begin

    ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
    try                                                                                                 //SS_1147_NDR_20140710
      ppImage1.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
    except                                                                                              //SS_1147_NDR_20140710
      On E: Exception do begin                                                                          //SS_1147_NDR_20140710
        Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
        MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
        Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
      end;                                                                                              //SS_1147_NDR_20140710
    end;                                                                                                //SS_1147_NDR_20140710

    with spObtenerMaestroConcesionaria do                                                               //SS_1147_NDR_20140710
    begin                                                                                               //SS_1147_NDR_20140710
      Close;                                                                                            //SS_1147_NDR_20140710
      Parameters.Refresh;                                                                               //SS_1147_NDR_20140710
      Parameters.ParamByName('@CodigoConcesionaria').Value := ObtenerCodigoConcesionariaNativa;         //SS_1147_NDR_20140710
      //ExecProc;                                                                                       //SS_1147_MCA_20150226  //SS_1147_NDR_20140710
      Open;                                                                                             //SS_1147_MCA_20150226
      sRazonSocial  := Uppercase(FieldByName('RazonSocialConcesionaria').AsString);                     //SS_1147_NDR_20140710
      sRut          := FormatFloat('#.#',FieldByName('RutConcesionaria').AsInteger) + '-' +             //SS_1147_NDR_20140710
                       FieldByName('DigitoVerificadorConcesionaria').AsString;                          //SS_1147_NDR_20140710
      sDireccion    := FieldByName('DireccionConcesionaria').AsString;                                  //SS_1147_NDR_20140710
      sLocalidad    := FieldByName('Comuna').AsString + '  ' +                                          //SS_1147_NDR_20140710
                       FieldByName('Region').AsString+ '  ' +                                           //SS_1147_NDR_20140710
                       FieldByName('Pais').AsString;                                                    //SS_1147_NDR_20140710
      sNombreCorto	:= FieldByName('NombreCorto').AsString;												//SS_1147_MBE_20150428
                                                                                                        //SS_1147_NDR_20140710
      ppLabel2.Caption := sRazonSocial;                                                                 //SS_1147_NDR_20140710
      ppLabel3.Caption := sRut;                                                                         //SS_1147_NDR_20140710
      ppLabel4.Caption := sDireccion;                                                                   //SS_1147_NDR_20140710
      ppLabel5.Caption := sLocalidad;                                                                   //SS_1147_NDR_20140710
      ppLabel17.Caption	:= 'p. ' + sNombreCorto 														//SS_1147_MBE_20150428
      
    end;                                                                                                //SS_1147_NDR_20140710

    with spObtenerReservaComprobantesDV do begin
    	Parameters.Refresh;
        Parameters.ParamByName('@IdReservaComprobanteDV').Value	        := IDReserva;
        Parameters.ParamByName('@NumeroRecibo').Value                   := Null;
	    Parameters.ParamByName('@NumeroConvenio').Value                 := Null;
	    Parameters.ParamByName('@FechaSolicitud').Value                 := Null;
	    Parameters.ParamByName('@FechaHoraImpresionDV').Value           := Null;
	    Parameters.ParamByName('@RutCliente').Value                     := Null;
	    Parameters.ParamByName('@IncluyeComprobanteInactivo').Value     := 0;
        Parameters.ParamByName('@IncluyeReservasEliminadas').Value		:= 0;
        Open;
    end;

    rbiReserva.Execute(True);
    Result := True;
end;
end.
