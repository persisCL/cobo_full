object FrameConcesionariaReclamoOrdenServicio: TFrameConcesionariaReclamoOrdenServicio
  Left = 0
  Top = 0
  Width = 235
  Height = 56
  Anchors = [akTop, akRight]
  TabOrder = 0
  object GBConcesionaria: TGroupBox
    Left = 2
    Top = 1
    Width = 235
    Height = 48
    Caption = 'Concesionaria Reclamo'
    TabOrder = 0
    object lblConcesionaria: TLabel
      Left = 7
      Top = 23
      Width = 71
      Height = 13
      Caption = 'Concesionaria:'
    end
    object vcbConcesionarias: TVariantComboBox
      Left = 118
      Top = 19
      Width = 108
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 0
      Items = <>
    end
  end
end
