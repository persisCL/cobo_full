object formDatoPersona: TformDatoPersona
  Left = 145
  Top = 206
  BorderStyle = bsDialog
  Caption = 'formDatoPersona'
  ClientHeight = 271
  ClientWidth = 805
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 230
    Width = 805
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object btn_Cancelar: TDPSButton
      Left = 720
      Top = 8
      Cancel = True
      Caption = '&Cancelar'
      TabOrder = 0
      OnClick = btn_CancelarClick
    end
    object btn_Aceptar: TDPSButton
      Left = 640
      Top = 8
      Caption = '&Aceptar'
      Default = True
      TabOrder = 1
      OnClick = btn_AceptarClick
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 805
    Height = 116
    Align = alTop
    Caption = 'Datos Personas'
    TabOrder = 1
    inline FrePersona: TFreDatoPresona
      Left = 2
      Top = 15
      Width = 801
      Height = 95
      Align = alTop
      TabOrder = 0
      inherited Pc_Datos: TPageControl
        Width = 801
      end
    end
  end
  object gb_Direccion: TGroupBox
    Left = 0
    Top = 116
    Width = 805
    Height = 109
    Align = alTop
    Caption = 'Direcci'#243'n'
    TabOrder = 2
    inline FrmDomicilio: TFrameDomicilio
      Left = 2
      Top = 15
      Width = 801
      Height = 89
      HorzScrollBar.Visible = False
      VertScrollBar.Visible = False
      Align = alTop
      TabOrder = 0
      inherited Pnl_Arriba: TPanel
        Width = 801
      end
      inherited Pnl_Abajo: TPanel
        Width = 801
      end
      inherited Pnl_Medio: TPanel
        Left = 784
        Top = 48
        Align = alNone
      end
    end
  end
end
