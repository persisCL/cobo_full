object NavWindowTransitosPorConvenio: TNavWindowTransitosPorConvenio
  Left = 95
  Top = 108
  HorzScrollBar.Color = clBtnFace
  HorzScrollBar.ParentColor = False
  Anchors = [akRight, akBottom]
  AutoScroll = False
  Caption = 'Transitos por Convenio'
  ClientHeight = 470
  ClientWidth = 799
  Color = clBtnFace
  Constraints.MinHeight = 504
  Constraints.MinWidth = 800
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  DesignSize = (
    799
    470)
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox2: TGroupBox
    Left = 7
    Top = 152
    Width = 786
    Height = 281
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = ' Lista de Tr'#225'nsitos '
    TabOrder = 0
    DesignSize = (
      786
      281)
    object dblTRansitos: TDBListEx
      Left = 5
      Top = 15
      Width = 772
      Height = 260
      Anchors = [akLeft, akTop, akRight, akBottom]
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 130
          Header.Caption = 'Fecha/Hora'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'FECHAHORA'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 150
          Header.Caption = 'Tipo de Horario'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'TipoHorario'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 120
          Header.Caption = 'Punto de Cobro'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'DescriPuntosCobro'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 100
          Header.Caption = 'Patente'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Patente'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 100
          Header.Caption = 'Categor'#237'a'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Categoria'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 150
          Header.Caption = 'Importe'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'DescriImporte'
        end>
      DataSource = dsTransitos
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
    end
  end
  object GroupBox1: TGroupBox
    Left = 5
    Top = 3
    Width = 788
    Height = 81
    Anchors = [akLeft, akTop, akRight]
    Caption = ' Par'#225'metros de b'#250'squeda '
    TabOrder = 1
    object lb_CodigoCliente: TLabel
      Left = 11
      Top = 26
      Width = 113
      Height = 13
      Caption = 'N'#250'mero de Documento:'
    end
    object Label2: TLabel
      Left = 289
      Top = 26
      Width = 53
      Height = 13
      Caption = 'Convenios:'
    end
    object Label1: TLabel
      Left = 537
      Top = 26
      Width = 51
      Height = 13
      Caption = 'Veh'#237'culos:'
    end
    object Label3: TLabel
      Left = 11
      Top = 56
      Width = 150
      Height = 13
      Caption = 'Rangos de fechas predefinidos:'
    end
    object Label4: TLabel
      Left = 364
      Top = 56
      Width = 63
      Height = 13
      Caption = 'Fecha Inicial:'
    end
    object Label7: TLabel
      Left = 547
      Top = 56
      Width = 58
      Height = 13
      Caption = 'Fecha Final:'
    end
    object peRUTCliente: TPickEdit
      Left = 129
      Top = 22
      Width = 133
      Height = 21
      CharCase = ecUpperCase
      Enabled = True
      TabOrder = 0
      OnChange = peRUTClienteChange
      Decimals = 0
      EditorStyle = bteTextEdit
      OnButtonClick = peRUTClienteButtonClick
    end
    object cbRangosPredefinidos: TComboBox
      Left = 168
      Top = 52
      Width = 177
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 3
      OnChange = cbRangosPredefinidosChange
      Items.Strings = (
        'Todos los Comprobantes'
        'Rango de Fechas Personalizado'
        'Hoy'
        'Ayer'
        'Ultima Semana'
        'Ultimo Mes')
    end
    object deFechaInicial: TDateEdit
      Left = 432
      Top = 52
      Width = 96
      Height = 21
      AutoSelect = False
      TabOrder = 4
      OnChange = deFechaInicialChange
      Date = -693594
    end
    object deFechaFinal: TDateEdit
      Left = 611
      Top = 52
      Width = 93
      Height = 21
      AutoSelect = False
      TabOrder = 5
      OnChange = deFechaInicialChange
      Date = -693594
    end
    object cbVehiculos: TVariantComboBox
      Left = 592
      Top = 22
      Width = 169
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 2
      OnChange = cbVehiculosChange
      Items = <>
    end
    object cbConveniosCliente: TVariantComboBox
      Left = 348
      Top = 22
      Width = 153
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 1
      OnChange = cbConveniosClienteChange
      Items = <
        item
        end>
    end
  end
  object DPSButton1: TDPSButton
    Left = 704
    Top = 440
    Width = 83
    Anchors = [akRight, akBottom]
    Caption = '&Salir'
    TabOrder = 2
    OnClick = DPSButton1Click
  end
  object GroupBox3: TGroupBox
    Left = 6
    Top = 86
    Width = 787
    Height = 63
    Anchors = [akLeft, akTop, akRight]
    Caption = 'Datos del Cliente'
    TabOrder = 3
    DesignSize = (
      787
      63)
    object lNombreCompleto: TLabel
      Left = 72
      Top = 19
      Width = 673
      Height = 16
      Anchors = [akLeft, akTop, akBottom]
      AutoSize = False
      Caption = 'Apellido Cliente'
    end
    object Label6: TLabel
      Left = 10
      Top = 19
      Width = 48
      Height = 13
      Caption = 'Nombre:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LDomicilioCompleto: TLabel
      Left = 72
      Top = 39
      Width = 689
      Height = 16
      Anchors = [akLeft, akTop, akBottom]
      AutoSize = False
      Caption = 'DomicilioCliente'
    end
    object Label10: TLabel
      Left = 10
      Top = 39
      Width = 56
      Height = 13
      Caption = 'Domicilio:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object ObtenerTransitosConvenio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerTransitosConvenio'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IndiceVehiculo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaHoraInicial'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaHoraFinal'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 424
    Top = 224
  end
  object dsTransitos: TDataSource
    DataSet = ObtenerTransitosConvenio
    Left = 360
    Top = 224
  end
  object ObtenerCLiente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerCliente'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end>
    Left = 392
    Top = 224
  end
  object qryConvenios: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'CodigoCliente'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT CodigoTipoMedioPago, Descripcion, dbo.FormatearNumeroConv' +
        'enio(NumeroConvenio) as NumeroConvenio, CodigoConvenio FROM Conv' +
        'enio WITH (NOLOCK), TipoMedioPago WITH (NOLOCK)'
      
        'WHERE Convenio.TipoMedioPagoAutomatico = TipoMedioPago.CodigoTip' +
        'oMedioPago'
      '      and MedioPagoAutomatico = 1'
      '      and Convenio.CodigoCliente = :CodigoCliente')
    Left = 456
    Top = 224
  end
end
