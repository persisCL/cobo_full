{********************************** File Header ********************************
File Name   : formImagenOverview.pas
Author      :
Date Created:
Language    : ES-AR
Description : 
--------------------------------------------------------------------------------
Historial de Revisiones
--------------------------------------------------------------------------------
Author      :
Date        :
Description :
*******************************************************************************}

unit formImagenOverview;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ImgTypes, ToolWin, ActnMan, ActnCtrls, Menus;

type
  TfrmImageOverview = class(TForm)
    Panel1: TPanel;
    ImageOverview1: TImage;
    ImageOverview2: TImage;
    ImageOverview3: TImage;
  private
    { Private declarations }
  public
    { Public declarations }
    Procedure ShowImageOverview(aGraphic: TGraphic; TipoImagen: TTipoImagen);
    Procedure ClearImagesOverview;

    Function Inicializa(): Boolean;
  end;

implementation

{$R *.dfm}

procedure TfrmImageOverview.ShowImageOverview(aGraphic: TGraphic; TipoImagen: TTipoImagen);
begin
    case TipoImagen of
        tiOverview1:
            begin
                if not assigned(ImageOverview1.Picture.Graphic) then begin
                    ImageOverview1.Picture.Assign(aGraphic);
                end;
                ImageOverview1.BringToFront;
            end;
        tiOverview2:
            begin
                if not assigned(ImageOverview2.Picture.Graphic) then begin
                    ImageOverview2.Picture.Assign(aGraphic);
                end;
                ImageOverview2.BringToFront;
            end;
        tiOverview3:
            begin
                if not assigned(ImageOverview3.Picture.Graphic) then begin
                    ImageOverview3.Picture.Assign(aGraphic);
                end;
                ImageOverview3.BringToFront;
            end;
    end;
end;

procedure TfrmImageOverview.ClearImagesOverview();
begin
    ImageOverview1.Picture.Assign(nil);
    ImageOverview2.Picture.Assign(nil);
    ImageOverview3.Picture.Assign(nil);
end;

function TfrmImageOverview.Inicializa: Boolean;
begin
    result := true;
end;

end.
