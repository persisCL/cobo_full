 {-----------------------------------------------------------------------------
 File Name      : ClaveValor.pas
 Author         : Alejandro Labra
 Date Created   : 11-04-2011
 Description    : Clase que sirve para almacenar un objeto que contiene
                  Clave y Valor, especial para cargar los parametros generales.
 -----------------------------------------------------------------------------}
 
unit ClaveValor;

interface
type
    TClaveValor = class
        private
            Key     : String;
            Value   : String;
        public
            property Clave : String read Key write Key;
            property Valor : String read Value write Value;
            constructor Create(ClaveStr, ValorStr : String);
            function ObtenerClave : String;
            function ObtenerValor : String;
    end;
implementation

{ TClaveValor }
{-----------------------------------------------------------------------------
 Function Name  : Create
 Author         : Alejandro Labra
 Date Created   : 08-04-2011
 Description    : Crea un nuevo objeto ClaveValor.
 Parameters     :
                 1.- ClaveStr: Valor que tendr� la propiedad Clave
                 2.- ValorStr: Valor que tendr� la propiedad Valor
-----------------------------------------------------------------------------}
constructor TClaveValor.Create(ClaveStr, ValorStr: String);
begin
    Clave := ClaveStr;
    Valor := ValorStr;
end;

 {-----------------------------------------------------------------------------
 Function Name  : ObtenerClave
 Author         : Alejandro Labra
 Date Created   : 08-04-2011
 Description    : Obtiene el valor de la propiedad Clave
 -----------------------------------------------------------------------------}
function TClaveValor.ObtenerClave: String;
begin
    Result := Clave;
end;

{-----------------------------------------------------------------------------
 Function Name  : ObtenerValor
 Author         : Alejandro Labra
 Date Created   : 08-04-2011
 Description    : Obtiene el valor de la propiedad Valor
-----------------------------------------------------------------------------}
function TClaveValor.ObtenerValor: String;
begin
    Result := Valor;
end;

end.
