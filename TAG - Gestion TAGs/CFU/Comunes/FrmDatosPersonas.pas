unit FrmDatosPersonas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, frmDatoPesona, ExtCtrls, StdCtrls, DPSControls,Peatypes,
  FreDomicilio;

type
  TformDatoPersona = class(TForm)
    Panel1: TPanel;
    btn_Cancelar: TDPSButton;
    btn_Aceptar: TDPSButton;
    GroupBox1: TGroupBox;
    FrePersona: TFreDatoPresona;
    gb_Direccion: TGroupBox;
    FrmDomicilio: TFrameDomicilio;
    procedure btn_AceptarClick(Sender: TObject);
    procedure btn_CancelarClick(Sender: TObject);
  private
    { Private declarations }
    function GetRegDatoPersona:TDatosPersonales;
    procedure SetRegDatoPersona(Value:TDatosPersonales);
  public
    { Public declarations }
    property RegDatoPersona:TDatosPersonales read GetRegDatoPersona write SetRegDatoPersona;
    procedure inicializar(Caption:TCaption;Personeria:String='');
  end;

var
  formDatoPersona: TformDatoPersona;

implementation

{$R *.dfm}

{ TformDatoPersona }

function TformDatoPersona.GetRegDatoPersona: TDatosPersonales;
begin
    Result:=FrePersona.RegistrodeDatos;
end;

procedure TformDatoPersona.inicializar(Caption: TCaption;
  Personeria: String);
begin
    self.Caption:=Caption;
    FrmDomicilio.Inicializa();
    if trim(Personeria)<>'' then
        FrePersona.Personeria:=Personeria;
end;

procedure TformDatoPersona.SetRegDatoPersona(Value: TDatosPersonales);
begin
    FrePersona.RegistrodeDatos:=Value;
end;

procedure TformDatoPersona.btn_AceptarClick(Sender: TObject);
begin
    ModalResult:=mrOk;
end;

procedure TformDatoPersona.btn_CancelarClick(Sender: TObject);
begin
    ModalResult:=mrCancel;
end;

end.
