unit AltaOrdenServicio;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls, DPSControls, UtilProc, FreAltaOSMultiple;

type
  TFormAltaOrdenServicio = class(TForm)
    FrameAltaOSMultiple1: TFrameAltaOSMultiple;
	procedure btnGuardarClick(Sender: TObject);
	procedure btnCancelarClick(Sender: TObject);
    procedure FrameAltaOSMultiple1btnSiguienteClick(Sender: TObject);
  private
	{ Private declarations }
  public
	{ Public declarations }
  end;

var
  FormAltaOrdenServicio: TFormAltaOrdenServicio;

implementation

{$R *.dfm}

procedure TFormAltaOrdenServicio.btnGuardarClick(Sender: TObject);
begin
	ModalResult := FrameAltaOSMultiple1.Guardar;
    if ModalResult = mrOk then close;
end;

procedure TFormAltaOrdenServicio.btnCancelarClick(Sender: TObject);
begin
	ModalResult := FrameAltaOSMultiple1.Cancelar;
 	close;
end;

procedure TFormAltaOrdenServicio.FrameAltaOSMultiple1btnSiguienteClick(
  Sender: TObject);
begin
  FrameAltaOSMultiple1.btnSiguienteClick(Sender);

end;

end.
