{-----------------------------------------------------------------------------
File Name      : FactoryINotificacion.pas
Author         : Alejandro Labra   
Date Created   : 11-04-2011
Description    : TFactoryINotificacion, implementa el patron de diseño factory
                 el cual permite crear objetos INotificacion.
-----------------------------------------------------------------------------}
unit FactoryINotificacion;

interface
uses
    Forms,
    Notificacion                ,//Interface que define el contrato para las notificaciones
    NotificacionImpresionDoc    ,//Implementación de INotificacion, el cual imprime el documento.
    NotificacionImpresionReporte;//Implementación de INotificacion, el cual imprime el documento en formato Reporte.                       //SS_960_ALA_20111125

type
    TFactoryINotificacion = class
        class function CrearINotificacion(tipoNotificacion : String) : INotificacion; static;
    end;

implementation

{ TFactoryINotificacion }

{-----------------------------------------------------------------------------
Function Name   : CrearINotificacion
Author          : Alejandro Labra   
Date Created    : 08-04-2011
Description     : Método estatico que genera objetos INotificacion de acuerdo
                  al tipo de notificación entregado por el parámetro tipoNotificacion.
Parameters      : tipoNotificacion: Contiene el nombre de la clase que implementa
                                    INotificacion.
-----------------------------------------------------------------------------}
class function TFactoryINotificacion.CrearINotificacion(
  tipoNotificacion: String): INotificacion;
begin
    if tipoNotificacion='TNotificacionImpresionDoc' then
        Result := TNotificacionImpresionDoc.Create
    else if tipoNotificacion = 'TFormNotificacionImpresionReporte' then         //SS_960_ALA_20111125
         Result := TFormNotificacionImpresionReporte.Create(Application);       //SS_960_PDO_20111229
end;

end.
