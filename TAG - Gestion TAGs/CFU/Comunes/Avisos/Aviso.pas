{-----------------------------------------------------------------------------
File Name      : Aviso.pas
Author         : Alejandro Labra
Date Created   : 11-04-2011
Description    : Define la interface IAviso, la cual permite generar avisos al
                 usuario mediante un formulario.

Firma       : SS_960_ALA_20111011
Description : Se agrega metodo que indica si muestra o no el detalle del mensaje.

Firma       : SS-960-NDR-20120605
Description : Se agrega metodo que devuelve TAGsVencidos : TList
-----------------------------------------------------------------------------}
unit Aviso;

interface
uses
    TipoFormaAtencion,          //Enum que contiene la definici�n de las formas de atencion.
    Notificacion,               //Interface que contiene la definici�n de las formas de notificaci�n.
    Classes, Windows, SysUtils, ADODB, UtilDB;

type
   TArrayINotificacion = Array of INotificacion;

type
    IAviso = interface(IInterface)
        {-----------------------------------------------------------------------------
        Author         : Alejandro Labra   
        Date Created   : 08-04-2011
        Description    : Declaraci�n de la propiedad Mensaje, la cual contiene
                         el mensaje ha ser mostrado en el formulario.
        -----------------------------------------------------------------------------}
        function ObtenerMensaje : String;
        property Mensaje : String read ObtenerMensaje;

        {-----------------------------------------------------------------------------
        Author         : Alejandro Labra   
        Date Created   : 08-04-2011
        Description    : Declaraci�n de la propiedad TiempoCierreVentana,
                         la cual contiene el tiempo que demorar� en cerrarse
                         la ventana al usuario.
        -----------------------------------------------------------------------------}
        function ObtenerTiempoCierreVentana : Integer;
        property TiempoCierreVentana : Integer read ObtenerTiempoCierreVentana;

        {-----------------------------------------------------------------------------
        Author         : Alejandro Labra   
        Date Created   : 08-04-2011
        Description    : Declaraci�n de la propiedad DetalleMensaje, la cual
                         contiene el detalle que se mostrar� al usuario.
        -----------------------------------------------------------------------------}
        function ObtenerDetalleMensaje : TStrings;
        property DetalleMensaje : TStrings read ObtenerDetalleMensaje;

        {-----------------------------------------------------------------------------      //SS-960-NDR-20120605
        Author         : Nelson Droguett                                                    //SS-960-NDR-20120605
        Date Created   : 06-06-2012                                                         //SS-960-NDR-20120605
        Description    : Declaraci�n de la propiedad TAGsVencidos, la cual                  //SS-960-NDR-20120605
                         contiene la lista de TAGs vencidos para accederlos desde fuera.    //SS-960-NDR-20120605
        -----------------------------------------------------------------------------}      //SS-960-NDR-20120605
        function ObtenerTAGsVencidos : TList;                                               //SS-960-NDR-20120605
        property TAGsVencidos : TList read ObtenerTAGsVencidos;                             //SS-960-NDR-20120605

        {-----------------------------------------------------------------------------
        Author         : Alejandro Labra
        Date Created   : 08-04-2011
        Description    : Declaraci�n de la propiedad ColorFormulario, la cual
                         contiene el color que tendr� el formulario.
        -----------------------------------------------------------------------------}
        function ObtenerColorFormulario : COLORREF;
        property ColorFormulario : COLORREF read ObtenerColorFormulario;

        {-----------------------------------------------------------------------------
        Author         : Alejandro Labra
        Date Created   : 08-04-2011
        Description    : Declaraci�n de la propiedad ColorMensaje, la cual
                         contiene el color que tendr� el mensaje.
        -----------------------------------------------------------------------------}
        function ObtenerColorMensaje : COLORREF;
        property ColorMensaje : COLORREF read ObtenerColorMensaje;

        {-----------------------------------------------------------------------------
        Author         : Alejandro Labra
        Date Created   : 08-04-2011
        Description    : Declaraci�n de la propiedad Notificaciones, la cual
                         contiene un array de INotificacion, que ser� la forma
                         de notificaci�n al usuario.
        -----------------------------------------------------------------------------}
        function ObtenerNotificaciones : TArrayINotificacion;
        property Notificaciones : TArrayINotificacion read ObtenerNotificaciones;

        {-----------------------------------------------------------------------------
        Author         : Alejandro Labra
        Date Created   : 08-04-2011
        Description    : Declaraci�n de la propiedad TituloVentana, la cual
                         contiene el titulo del formulario.
        -----------------------------------------------------------------------------}
        function ObtenerTituloVentana : String;
        property TituloVentana : String read ObtenerTituloVentana;

        {-----------------------------------------------------------------------------
        Author         : Alejandro Labra
        Date Created   : 08-04-2011
        Description    : Declaraci�n de la m�todo GenerarNotificacion, el cual
                         llamar� al m�todo Notificar de INotificacion.
        Parameters     : Este m�todo recibe un TTipoFormaAtencion que define la
                         forma de atenci�n entre CN y el usuario.
        -----------------------------------------------------------------------------}
        procedure GenerarNotificacion(formaAtencion : TTipoFormaAtencion; var Notificado: Boolean);     // SS_960_PDO_20120104
        {-----------------------------------------------------------------------------
        Author         : Alejandro Labra
        Date Created   : 08-04-2011
        Description    : Declaraci�n de la m�todo ReGenerarAviso, el cual
                         regenerar� la notificaci�n llamando al m�todo Notificar
                         de INotificacion.
        -----------------------------------------------------------------------------}
        procedure ReGenerarAviso;
        {-----------------------------------------------------------------------------
        Author         : Alejandro Labra
        Date Created   : 08-04-2011
        Description    : Declaraci�n de la m�todo Inicializar, el cual inicializa
                         el objeto IAviso, seteando sus parametros base.
        Parameters     :
                         1.- parametros: Este m�todo recibe un array de strings que contiene la lista
                                         de parametros.
                         2.- pDataBase : TADOConnection sobre la cual se realizar�n las consultas.
        -----------------------------------------------------------------------------}
        procedure Inicializar(parametros : Array of String; pDataBase : TADOConnection);
        {-----------------------------------------------------------------------------
        Author         : Alejandro Labra
        Date Created   : 08-04-2011
        Description    : Declaraci�n de la m�todo VerificarMostrarAviso, el cual
                         verifica si se debe o no mostrar el aviso al usuario.
        Return         : Retorna un booleano que indica true si se debe mostrar
                         el aviso o false si no se debe mostrar.
        -----------------------------------------------------------------------------}
        function VerificarMostrarAviso : Boolean;
        {-----------------------------------------------------------------------------
        Author         : Alejandro Labra
        Date Created   : 08-04-2011
        Description    : Declaraci�n de la m�todo VerificarGenerarNotificacion, el cual
                         verifica si se debe generar o no la Notificacion.
        Parameters     : Este m�todo recibe un TTipoFormaAtencion que define la
                         forma de atenci�n entre CN y el usuario.
        Return         : Retorna un booleano que indica true si se debe generar
                         la notificaci�n o false si no se debe generar.
        -----------------------------------------------------------------------------}
        function VerificarGenerarNotificacion(formaAtencion : TTipoFormaAtencion) : Boolean;
        {-----------------------------------------------------------------------------                //SS_960_ALA_20111011
        Author         : Alejandro Labra                                                              //SS_960_ALA_20111011
        Date Created   : 11-1-2011                                                                    //SS_960_ALA_20111011
        Description    : Indica si debe mostrar el detalle de mensaje.                                //SS_960_ALA_20111011
        Return         : Retorna un booleano que indica true si se debe mostrar                       //SS_960_ALA_20111011
                         el detalle o false si no se debe mostrar.                                    //SS_960_ALA_20111011
        -----------------------------------------------------------------------------}                //SS_960_ALA_20111011
        function MuestraDetalleMensaje () : Boolean;                                                  //SS_960_ALA_20111011
    end;

implementation

end.
