unit NotificacionImpresionReporte;

interface

{
Firma       : SS-960-NDR-20120125
Description : Que solo salga una copia y sin la leyenda de copiacliente o copiacostanera.



Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.


Firma			: SS_1185_NDR_20140509
Descripcion		: Se agrega una columna para mostrar el Televia en la lista de TAGs vencidos
				  que se imprimen en la plantilla y en el anexo.
}

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,
  Notificacion,       //Interface que define el contrato para las notificaciones
  Diccionario,        //Contiene la definición de una lista de TClaveValor.
  frmMuestraMensaje,  // Muestra mensajes descriptivos al usuario.
  frmMuestraPDF, ppComm, ppRelatv, ppDB, ppDBPipe, DB, UtilRB, ppProd, ppClass,
  ppReport, ppCtrls, ppRichTx, ppStrtch, ppSubRpt, jpeg, ppPrnabl, ppBands,
  ppCache, ppParameter, Util, utilhttp, UtilProc, DBClient,ConstParametrosGenerales,
  DMConnection, StrUtils;

type
  TFormNotificacionImpresionReporte = class(TForm, INotificacion)
    dsTagsVencidos          : TDataSource;
    ppDBPTagsVencidos       : TppDBPipeline;
    ppReporteTagsVencidos   : TppReport;
    rbInterfaceTagsVencidos : TRBInterface;
    ppParameterList1        : TppParameterList;
    ppHeaderBand1           : TppHeaderBand;
    ppDetailBand1           : TppDetailBand;
    ppFooterBand1           : TppFooterBand;
    ppSummaryBand1          : TppSummaryBand;
    ppImage1                : TppImage;
    ppSubReport1            : TppSubReport;
    ppChildReport1          : TppChildReport;
    ppRichText1             : TppRichText;
    ppDetailBand2           : TppDetailBand;
    ppHeaderBand2           : TppHeaderBand;
    ppFooterBand2           : TppFooterBand;
    ppImage2                : TppImage;
    ppShape1                : TppShape;
    ppShape2                : TppShape;
    ppShape3                : TppShape;
    ppShape4                : TppShape;
    ppLabel1                : TppLabel;
    ppLabel2                : TppLabel;
    ppDBText1               : TppDBText;
    ppDBText2               : TppDBText;
    ppLabel3                : TppLabel;
    ppLblTagCopiaAnexo      : TppLabel;
    ppLblTagCopia: TppLabel;
    procedure ppSummaryBand1BeforePrint(Sender: TObject);
    private
        _plantillas     : TStringList;
        _variables      : TList;
        _copiaCliente   : Boolean;
        _tagCopia       : String;
        _datos          : TClientDataset;
        _cantidadLineas : Integer;
        function GenerarReporte(esCopiaCliente : Boolean) : Boolean;
        procedure CargarDatos();
    public
        function ObtenerPlantillas : TStringList;
        procedure AsignarPlantillas(plantillas : TStringList);
        property Plantillas : TStringList read ObtenerPlantillas write AsignarPlantillas;

        function ObtenerVariables : TList;
        procedure AsignarVariables(variables : TList);
        property Variables : TList read ObtenerVariables write AsignarVariables;

        function ObtenerConCopiaCliente : Boolean;
        procedure AsignarConCopiaCliente(conCopia : Boolean);
        property ConCopiaCliente : Boolean read ObtenerConCopiaCliente write AsignarConCopiaCliente;

        function ObtenerTagCopia : String;
        procedure AsignarTagCopia(tagCopia : String);
        property TagCopia : String read ObtenerTagCopia write AsignarTagCopia;
            
        function Notificar : Boolean;
        Destructor Destroy; override;
  end;

var
  FormNotificacionImpresionReporte: TFormNotificacionImpresionReporte;

implementation

{$R *.dfm}

{ TFormNotificacionImpresionReporte }
 {-----------------------------------------------------------------------------
Function Name   : AsignarConCopiaCliente
Author          : Alejandro Labra
Date Created    : 24-11-2011
Description     : Setea el valor a la propiedad ConCopiaCliente.
Parameter       : conCopia, boolean que indica si la impresión es con copia al
                  cliente o no.
-----------------------------------------------------------------------------}
procedure TFormNotificacionImpresionReporte.AsignarConCopiaCliente(
  conCopia: Boolean);
begin
    _copiaCliente := conCopia;
end;
{-----------------------------------------------------------------------------
Function Name   : AsignarPlantillas
Author          : Alejandro Labra
Date Created    : 24-11-2011
Description     : Setea el valor a la propiedad Plantilla.                      //SS-960-ALA-20110427
Parameter       : plantilla, la cual contiene la ruta de la plantilla a ser     //SS-960-ALA-20110427
                  cargada.
-----------------------------------------------------------------------------}
procedure TFormNotificacionImpresionReporte.AsignarPlantillas(
  plantillas: TStringList);
begin
    _plantillas := plantillas;
end;

{-----------------------------------------------------------------------------
Function Name   : AsignarTagCopia
Author          : Alejandro Labra
Date Created    : 24-11-2011
Description     : Setea el valor a la propiedad TagCopia.
Parameter       : tagCopia, string que contiene el tag a ser reemplazado en el
                  documento.
-----------------------------------------------------------------------------}
procedure TFormNotificacionImpresionReporte.AsignarTagCopia(tagCopia: String);
begin
    _tagCopia := tagCopia;
end;

{-----------------------------------------------------------------------------
Function Name   : AsignarVariables
Author          : Alejandro Labra
Date Created    : 24-11-2011
Description     : Setea el valor a la propiedad Variables.
Parameter       : variales, contiene la lista de objetos ClaveValor, almacenados
                  en un TDiccionario.
-----------------------------------------------------------------------------}
procedure TFormNotificacionImpresionReporte.AsignarVariables(variables: TList);
begin
    _variables := variables;
end;

procedure TFormNotificacionImpresionReporte.CargarDatos;
var
    i                   : Integer;
    k                   : Integer;
begin
    _datos := TClientDataset.Create(nil);
    _datos.FieldDefs.Add('Patente', ftString, 20, false);
    _datos.FieldDefs.Add('FechaVencimiento', ftString, 20, false);
    _datos.FieldDefs.Add('Televia', ftString, 11, false);									//SS_1185_NDR_20140509
    _datos.CreateDataSet;
    _datos.Open;

    for k := 0 to _variables.Count - 1 do begin
        i := 0;
        while i < TDiccionario(_variables.Items[k]).Count - 1 do begin
            if (AnsiContainsStr(TDiccionario(_variables.Items[k]).Items[i].Clave, 'PATENTE_')) Or								//SS_1185_NDR_20140509
               (AnsiContainsStr(TDiccionario(_variables.Items[k]).Items[i].Clave, 'FECHA_VENCIMIENTO_')) OR						//SS_1185_NDR_20140509
               (AnsiContainsStr(TDiccionario(_variables.Items[k]).Items[i].Clave, 'TELEVIA_')) then begin						//SS_1185_NDR_20140509
                if (Trim(TDiccionario(_variables.Items[k]).Items[i].Valor)<>'') and												//SS_1185_NDR_20140509
                   (Trim(TDiccionario(_variables.Items[k]).Items[i + 1].Valor)<>'') and											//SS_1185_NDR_20140509
                   (Trim(TDiccionario(_variables.Items[k]).Items[i + 2].Valor)<>'')	then begin									//SS_1185_NDR_20140509
                    _datos.Append;
                    _datos.FieldByName('Televia').AsString := TDiccionario(_variables.Items[k]).Items[i].Valor;					//SS_1185_NDR_20140509
                    _datos.FieldByName('Patente').AsString := TDiccionario(_variables.Items[k]).Items[i+1].Valor;				//SS_1185_NDR_20140509
                    _datos.FieldByName('FechaVencimiento').AsString := TDiccionario(_variables.Items[k]).Items[i+2].Valor;		//SS_1185_NDR_20140509
                    _datos.Post;
                end;
                i := i + 3;																										//SS_1185_NDR_20140509
            end
            else
                i := i + 1;
        end;
    end;
    dsTagsVencidos.DataSet := _datos;
end;

destructor TFormNotificacionImpresionReporte.Destroy;
begin
    inherited;
    if Assigned(_datos) then FreeAndNil(_datos);
    if Assigned(_variables) then FreeAndNil(_variables);
end;

function TFormNotificacionImpresionReporte.GenerarReporte(
  esCopiaCliente: Boolean) : Boolean;
resourcestring
    TAG_COPIA_CLIENTE               = 'Copia Cliente';
    //TAG_COPIA_CONCESIONARIA         = 'Copia Costanera Norte';				//SS_1147_MCA_20140408
    MSG_ERROR_NO_EXISTE_PLANTILLA   = 'No existe la plantilla, por favor contactese con el administrador.';
var
    i               : Integer;
    TextoPlantilla  : String;
    Resultado       : Boolean;
    RutaLogo        : AnsiString;                                               //SS_1147_NDR_20140710
begin
    Resultado := False;
    try
        ObtenerParametroGeneral(DMConnections.BaseCAC, 'CANT_LINEAS_TAG_VENCIDOS', _cantidadLineas);
        ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
        try                                                                                                 //SS_1147_NDR_20140710
          ppImage1.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
        except                                                                                              //SS_1147_NDR_20140710
          On E: Exception do begin                                                                          //SS_1147_NDR_20140710
            Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
            MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
            Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
          end;                                                                                              //SS_1147_NDR_20140710
        end;                                                                                                //SS_1147_NDR_20140710


        //
        if FileExists(_plantillas[0]) then begin

            TextoPlantilla := FileToString(_plantillas[0] , fmShareDenyNone);
            for i := 0 to TDiccionario(_variables.Items[0]).Count - 1 do begin
                if  (AnsiContainsStr(TDiccionario(_variables.Items[0]).Items[i].Clave, 'TELEVIA_')= false) And						//SS_1185_NDR_20140509
                    (AnsiContainsStr(TDiccionario(_variables.Items[0]).Items[i].Clave, 'PATENTE_')= false) And						//SS_1185_NDR_20140509
                    (AnsiContainsStr(TDiccionario(_variables.Items[0]).Items[i].Clave, 'FECHA_VENCIMIENTO_')=False) then begin		//SS_1185_NDR_20140509

                    TextoPlantilla := ReplaceTag(TextoPlantilla, TDiccionario(_variables.Items[0]).Items[i].Clave, TDiccionario(_variables.Items[0]).Items[i].Valor);
                end;
            end;
            dsTagsVencidos.DataSet.First;
            for i := 1 to _cantidadLineas do begin
                TextoPlantilla := ReplaceTag(TextoPlantilla, Format('TELEVIA_%s', [FormatFloat('0#', i)]), iif(not dsTagsVencidos.DataSet.Eof, dsTagsVencidos.DataSet.FieldByName('Televia').AsString, ''));									//SS_1185_NDR_20140509
                TextoPlantilla := ReplaceTag(TextoPlantilla, Format('PATENTE_%s', [FormatFloat('0#', i)]), iif(not dsTagsVencidos.DataSet.Eof, dsTagsVencidos.DataSet.FieldByName('Patente').AsString, ''));									//SS_1185_NDR_20140509
                TextoPlantilla := ReplaceTag(TextoPlantilla, Format('FECHA_VENCIMIENTO_%s', [FormatFloat('0#', i)]), iif(not dsTagsVencidos.DataSet.Eof, dsTagsVencidos.DataSet.FieldByName('FechaVencimiento').AsString, ''));					//SS_1185_NDR_20140509
                if not dsTagsVencidos.DataSet.Eof then dsTagsVencidos.DataSet.Next;
            end;

            ppRichText1.RichText :=  TextoPlantilla;

            ppLblTagCopia.Visible      := False;
            ppLblTagCopiaAnexo.Visible := False;

            //if esCopiaCliente then                                            //SS-960-NDR-20120125
            //    ppLblTagCopia.Caption := TAG_COPIA_CLIENTE;                   //SS-960-NDR-20120125
            //    ppLblTagCopiaAnexo.Caption := TAG_COPIA_CLIENTE;              //SS-960-NDR-20120125
            //end                                                               //SS-960-NDR-20120125
            //else begin                                                        //SS-960-NDR-20120125
            //    ppLblTagCopia.Caption := TAG_COPIA_CONCESIONARIA;             //SS-960-NDR-20120125
            //    ppLblTagCopiaAnexo.Caption := TAG_COPIA_CONCESIONARIA;        //SS-960-NDR-20120125
            //end;                                                              //SS-960-NDR-20120125
        end
        else begin
            MsgBoxErr(MSG_ERROR_NO_EXISTE_PLANTILLA, 'Ruta plantilla: ' + _plantillas[0] ,'Generando Documento PDF', 0);
            //Result := false;
        end;

        rbInterfaceTagsVencidos.Execute(esCopiaCliente);
        Resultado := True;
    finally
        Result := Resultado;
    end;
end;

function TFormNotificacionImpresionReporte.Notificar: Boolean;
var
    Resultado : Boolean;
begin
    Resultado := True;
    CargarDatos;
    Resultado := GenerarReporte(true);
    //if (_copiaCliente) and (Resultado) then Resultado := GenerarReporte(false);      //SS-960-NDR-20120125
    

    Result := Resultado;
end;

function TFormNotificacionImpresionReporte.ObtenerConCopiaCliente: Boolean;
begin
    Result := _copiaCliente;
end;

function TFormNotificacionImpresionReporte.ObtenerPlantillas: TStringList;
begin
    Result := _plantillas;
end;

{-----------------------------------------------------------------------------
Function Name  : ObtenerTagCopia
Author         : Alejandro Labra
Date Created   : 24-11-2011
Description    : Retorna el valor de la propiedad TagCopia.
-----------------------------------------------------------------------------}
function TFormNotificacionImpresionReporte.ObtenerTagCopia: String;
begin
    Result := _tagCopia;
end;

{-----------------------------------------------------------------------------
Function Name  : ObtenerVariables
Author         : Alejandro Labra
Date Created   : 24-11-2011
Description    : Retorna el valor de la propiedad Variables.
-----------------------------------------------------------------------------}
function TFormNotificacionImpresionReporte.ObtenerVariables: TList;
begin
    Result:= _variables;
end;

procedure TFormNotificacionImpresionReporte.ppSummaryBand1BeforePrint(
  Sender: TObject);
begin
    ppSummaryBand1.Visible := ppDBPTagsVencidos.DataSource.DataSet.RecordCount > _cantidadLineas;
end;

end.
