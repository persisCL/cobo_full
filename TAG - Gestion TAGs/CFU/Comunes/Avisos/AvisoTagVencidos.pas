{-----------------------------------------------------------------------------
File Name      : AvisoTagVencidos.pas
Author         : Alejandro Labra
Date Created   : 11-04-2011
Description    : Implementaci�n de la interface IAviso. Esta implementaci�n
                 resuelve la Rev.960 SS Ventana de mensajes de TAGs vencidos,
                 el cual buscar� los tags con garant�a vencida para un cliente
                 en particular.

Revision       : 1
Author         : Alejandro Labra
Date           : 23-04-2011
Firma          : Rev.960 SS Ventana de mensajes de TAGs vencidos
Description    : Se arregla problema cuando se consulta un rut que no tiene
                 tags vencidos y la ventana igual se mostraba.
                 En este sentido se edita funcion "VerificarMostrarAviso",
                 la cual en el if se encontraba preguntando >= en vez de >.
Firma          : SS-960-ALA-20110423

Revision       : 2
Author         : Alejandro Labra
Date           : 27-04-2011
Firma          : Rev.960 SS Ventana de mensajes de TAGs vencidos
Description    : Se modifica para soportar una �nica impresi�n de pdf
Firma          : SS-960-ALA-20110427

Revision       : 3
Author         : Alejandro Labra
Date           : 12-07-2011
Description    : Se arregla impresi�n del mes para que se obtenga en espa�ol.
Firma          : SS-960-ALA-20110712

Firma          : SS_960_ALA_20110722
Description    : Se modifica m�todo Inicializar, el cual obtendr�
                 el valor del par�metro "TiempoVolverMostrarAviso" y lo
                 almacenara en la variable "_tiempoVolverMostrarAviso" y se lo
                 entregar� como par�metro al Store Procedure SPObtenerTagVencidosCN
                 el cual filtrar� los avisos a ser mostrados.

Firma           : SS_960_ALA_20111011
Description     : Se agrega metodo que indica si muestra o no el detalle del mensaje.
                  Adem�s se modifica el texto del mensaje, si la cantidad de tags vencidos
                  es mayor a 10.

Firma           : SS-960-NDR-20120605
Description     : La ventana mostrar� todos los TAGs activos y vencidos del cliente.
                  A la informaci�n actual se le a�adir�:
                    Fecha �ltima notificaci�n,
                    Tilde con el encabezado A Imprimir.

                  Saldr�n tildados los TAGs que:
                    Nunca fueron notificados,
                    Y de los que ya se notificaron, todos los que desde su �ltima fecha de
                    notificaci�n, ya haya transcurrido los d�as marcados por el par�metro
                    general AvisoTagTiempoVolverMostrarAviso.


Firma			: SS_1185_NDR_20140509
Descripcion		: Se agrega una columna para mostrar el Televia en la lista de TAGs vencidos
				  que se imprimen en la plantilla y en el anexo.
                Cuando es persona juridica imprimir entre parentesis el representante legal. Si es persona natural solo el nombre
-----------------------------------------------------------------------------}
unit AvisoTagVencidos;

interface

uses
    Aviso                       ,//Interface que define el contrato para los Avisos.
    TipoFormaAtencion           ,//Enum que contiene la definici�n de las formas de atencion.
    Diccionario                 ,//Contiene la definici�n de una lista de TClaveValor.
    Notificacion                ,//Interface que define el contrato para las notificaciones
    NotificacionImpresionDoc    ,//Implementaci�n de INotificacion, el cual imprime el documento.
    FactoryINotificacion        ,//Factory que crea objetos que cumplen con el contrato INotificacion.
    DMConnection                ,//coneccion a base de datos OP_CAC
    ConstParametrosGenerales    ,   //obtenci�n de parametros generales
    Classes, Windows, SysUtils, SysUtilsCN, ADODB, UtilDB, Util, UtilProc, Forms, Controls, PeaProcsCN,
    DateUtils, Variants, StrUtils,PeaTypes;                                                                //SS_1185_NDR_20140509 //SS-960-NDR-20120605 //SS-960-ALA-20110712

const
    NOMBRE_TIPO_AVISO       = 'TAvisoTagVencidos';//Nombre del tipo de aviso que se buscar� en base de datos, el cual corresponde al nombre de la clase.

type
    TTagVencidos = class
        Patente                 : String;
        Televia                 : String;
        FechaVencimiento        : String;
        CodigoConvenio          : String;
        IndiceVehiculo          : String;
        ContractSerialNumber    : String;
        CodigoCliente           : String;
        Notificado              : Boolean;
        FechaUltimaNotificacion : String;                                       //SS-960-NDR-20120605
        NumeroConvenio          : String;
    end;
type
    TAvisoTagVencidos = class(TInterfacedObject, IAviso)
        private
            _mensaje                    : String;
            _tiempoCierreVentana        : Integer;
            _detalleMensaje             : TStrings;
            _colorFormulario            : COLORREF;
            _colorMensaje               : COLORREF;
            _notificaciones             : TArrayINotificacion;
            _tituloVentana              : String;
            _rutCliente                 : String;
            _listaTagVencidos           : TList;
            _pDataBase                  : TADOConnection;
            _codigoTipoAviso            : Integer;
            _codigoCliente              : Integer;
            _tiempoVolverMostrarAviso   : Integer;                              //SS_960_ALA_20110722
            _TAGsSinNotificar           : Integer;
            procedure AsignarColores;
        public
            function ObtenerMensaje : String;
            property Mensaje : String read ObtenerMensaje;

            function ObtenerTiempoCierreVentana : Integer;
            property TiempoCierreVentana : Integer read ObtenerTiempoCierreVentana;

            function ObtenerDetalleMensaje : TStrings;
            property DetalleMensaje : TStrings read ObtenerDetalleMensaje;

            function ObtenerTAGsVencidos : TList;                               //SS-960-NDR-20120605
            property TAGsVencidos : TList read ObtenerTAGsVencidos;             //SS-960-NDR-20120605

            function ObtenerColorFormulario : COLORREF;
            property ColorFormulario : COLORREF read ObtenerColorFormulario;

            function ObtenerColorMensaje : COLORREF;
            property ColorMensaje : COLORREF read ObtenerColorMensaje;

            function ObtenerNotificaciones : TArrayINotificacion;
            property Notificaciones : TArrayINotificacion read ObtenerNotificaciones;

            function ObtenerTituloVentana : String;
            property TituloVentana : String read ObtenerTituloVentana;

            procedure GenerarNotificacion(formaAtencion : TTipoFormaAtencion; var Notificado: Boolean);     // SS_960_PDO_20120104
            procedure ReGenerarAviso;
            procedure Inicializar(parametros : Array of String; pDataBase : TADOConnection);
            function VerificarMostrarAviso : Boolean;
            function VerificarGenerarNotificacion(formaAtencion : TTipoFormaAtencion) : Boolean;
            function MuestraDetalleMensaje () : Boolean;                                            //SS_960_ALA_20111011
            Destructor Destroy; override;
    end;

implementation

{ TAvisoTagVencidos }

{-----------------------------------------------------------------------------
Function Name  : AsignarColores
Author         : Alejandro Labra   
Date Created   : 08-04-2011
Description    : M�todo que asigna los colores al formulario y mensaje de acuerdo
                 al d�a de la semana que nos encontremos.
-----------------------------------------------------------------------------}
procedure TAvisoTagVencidos.AsignarColores;
begin
    case DayOfWeek(NowBaseCN(DMConnections.BaseCAC)) of
        1 : _colorFormulario:= RGB(238, 130, 238);          //Domingo   - Color Violeta
        2 : _colorFormulario:= RGB(0, 204, 0);              //Lunes     - Color Verde
        3 : _colorFormulario:= RGB(255, 0, 0);              //Martes    - Color Rojo
        4 : _colorFormulario:= RGB(255, 255, 0);            //Miercoles - Color Amarillo
        5 : begin
                _colorFormulario:= RGB(0, 0, 255);          //Jueves    - Color Azul
                _colorMensaje:= RGB(255, 255, 255);
            end;
        6 : begin
                _colorFormulario:= RGB(0, 0, 0);            //Viernes   - Color Negro
                _colorMensaje:= RGB(255, 255, 255);
            end;
        7 : _colorFormulario:= RGB(173, 216, 230);          //Sabado    - Color Celeste
    end;
end;

{-----------------------------------------------------------------------------
Function Name  : Destroy
Author         : Alejandro Labra
Date Created   : 08-04-2011
Description    : Sobrecarga del m�todo Destroy que libera de memoria el objeto
                 detalleMensaje.
-----------------------------------------------------------------------------}
destructor TAvisoTagVencidos.Destroy;
begin
    FreeAndNil(_detalleMensaje);
    FreeAndNil(_listaTagVencidos);
    inherited;
end;

{-----------------------------------------------------------------------------
Function Name  : GenerarAviso
Author         : Alejandro Labra
Date Created   : 08-04-2011
Description    : M�todo que genera las notificaciones de acuerd a la lista de
                 notificaciones asignadas.
-----------------------------------------------------------------------------}
procedure TAvisoTagVencidos.GenerarNotificacion(formaAtencion : TTipoFormaAtencion; var Notificado: Boolean);   // SS_960_PDO_20120104
resourceString
    MSG_ERROR_GRABAR_NOTIFICACION   = 'No se pudo grabar la notificaci�n de TAGs con garant�a vencida. ' + #13 + 'Consulte al Administrador del Sistema ';
    TEXTO_OBSERVACION               = '[%s - %s] Se ha impreso la Notificaci�n de TAGs Vencidos ID. %d, informando los siguientes TAGs: ';       // SS_960_PDO_20120104
    TEXTO_OBSERVACION_MAS_DIEZ_TAGS = '[%s - %s] Se ha impreso la Notificaci�n de TAGs Vencidos ID. %d, informando un total de %d TAGs';         // SS_960_PDO_20120104
var
    i                                   : Integer;
    SPInsertarNotificacion,
    SPInsertarNotificacionTagVencido,
    SPActualizarConvenioObservacion     : TADOStoredProc;    // SS_960_PDO_20120104
    NotificacionExitosa                 : Boolean;
    CodigoNotificacion,
    CodigoConvenio,
    CantidadTeleviasConvenio            : Integer;           // SS_960_PDO_20120104
    TextoObservacionConvenio            : string;            // SS_960_PDO_20120104
    FechaObservacion                    : TDateTime;         // SS_960_PDO_20120104
    PrimerTAG                           : Boolean;           // SS_960_PDO_20120104

    // Inicio Bloque: SS_960_PDO_20120104
    procedure ActualizaObservacionesPorTAGsNotificados;
    begin
        if CantidadTeleviasConvenio > 10 then begin
            TextoObservacionConvenio :=
                Format(
                    TEXTO_OBSERVACION_MAS_DIEZ_TAGS,
                    [FormatDateTime('dd"-"mm"-"yyyy hh:mm:ss', FechaObservacion),
                     UsuarioSistema,
                     CodigoNotificacion,
                     CantidadTeleviasConvenio]);
        end;

        SPActualizarConvenioObservacion.Parameters.Refresh;
        SPActualizarConvenioObservacion.Parameters.ParamByName('@CodigoConvenio').Value    := CodigoConvenio;
        SPActualizarConvenioObservacion.Parameters.ParamByName('@IndiceObservacion').Value := null;
        SPActualizarConvenioObservacion.Parameters.ParamByName('@Observacion').Value       := TextoObservacionConvenio + '.';
        SPActualizarConvenioObservacion.Parameters.ParamByName('@Activo').Value            := 1;
        SPActualizarConvenioObservacion.Parameters.ParamByName('@FechaObservacion').Value  := FechaObservacion;
        SPActualizarConvenioObservacion.Parameters.ParamByName('@UsuarioAuditoria').Value  := UsuarioSistema;
        SPActualizarConvenioObservacion.Parameters.ParamByName('@FechaAuditoria').Value    := FechaObservacion;
        SPActualizarConvenioObservacion.ExecProc;
    end;

    procedure InicializaObservacionPorTAGsNotificados;
    begin
        CodigoConvenio           := StrToInt(TTagVencidos(_listaTagVencidos.Items[i]).CodigoConvenio);
        CantidadTeleviasConvenio := 0;
        TextoObservacionConvenio :=
            Format(
                TEXTO_OBSERVACION,
                [FormatDateTime('dd"-"mm"-"yyyy hh:mm:ss', FechaObservacion),
                 UsuarioSistema,
                 CodigoNotificacion]);
        PrimerTAG                := True;
    end;
    // Fin Bloque: SS_960_PDO_20120104
begin
    try
        try
            Screen.Cursor := crHourGlass;                                       //SS-960-ALA-20110427
            Application.ProcessMessages;                                        //SS-960-ALA-20110427
            if _notificaciones = nil then begin
                NotificacionExitosa := false;
                ObtenerNotificaciones;
                for i := 0 to Length(_notificaciones) - 1 do begin
                    //Si la notificacion fue exitosa insertamos la notificacion
                    if(_notificaciones[i].Notificar()) then begin
                        NotificacionExitosa := true;
                    end;
                end;
                if NotificacionExitosa then begin
                    //Insertando la notificacion
                    QueryExecute(_pDataBase, 'BEGIN TRANSACTION GrabarNotificacion');

                    SPInsertarNotificacion  := TADOStoredProc.Create(nil);
                    SPInsertarNotificacion.Connection := TADOConnection(_pDataBase);
                    SPInsertarNotificacion.ProcedureName := 'InsertarNotificacion';
                    SPInsertarNotificacion.Parameters.Refresh;
                    SPInsertarNotificacion.Parameters.ParamByName('@CodigoCliente').Value       := _codigoCliente;
                    SPInsertarNotificacion.Parameters.ParamByName('@CodigoTipoAviso').Value     := _codigoTipoAviso;
                    SPInsertarNotificacion.Parameters.ParamByName('@Usuario').Value             := UsuarioSistema;
                    SPInsertarNotificacion.Parameters.ParamByName('@CodigoNotificacion').Value  := CodigoNotificacion;
                    SPInsertarNotificacion.ExecProc;

                    CodigoNotificacion := SPInsertarNotificacion.Parameters.ParamByName('@CodigoNotificacion').Value;
                    SPInsertarNotificacion.Close;

                    //Insertando el detalle de la notificacion.
                    SPInsertarNotificacionTagVencido := TADOStoredProc.Create(nil);
                    SPInsertarNotificacionTagVencido.Connection := TADOConnection(_pDataBase);
                    for i := 0 to _listaTagVencidos.Count - 1 do begin
                        //Insertamos el detalle solamente si no ha sido notificado.
                        if TTagVencidos(_listaTagVencidos.Items[i]).Notificado=false then begin
                            SPInsertarNotificacionTagVencido.ProcedureName := 'InsertarNotificacionTagVencido';
                            SPInsertarNotificacionTagVencido.Parameters.Refresh;
                            with SPInsertarNotificacionTagVencido, Parameters do begin
                                ParamByName('@CodigoNotificacion').Value     := CodigoNotificacion;
                                ParamByName('@IndiceVehiculo').Value         := TTagVencidos(_listaTagVencidos.Items[i]).IndiceVehiculo;
                                ParamByName('@CodigoConvenio').Value         := TTagVencidos(_listaTagVencidos.Items[i]).CodigoConvenio;
                                ParamByName('@ContractSerialNumber').Value   := TTagVencidos(_listaTagVencidos.Items[i]).ContractSerialNumber;
                            end;
                            SPInsertarNotificacionTagVencido.ExecProc;
                        end;
                    end;
                    SPInsertarNotificacionTagVencido.Close;

                    // Inicio Bloque: SS_960_PDO_20120104
                    SPActualizarConvenioObservacion               := TADOStoredProc.Create(nil);
                    SPActualizarConvenioObservacion.Connection    := TADOConnection(_pDataBase);
                    SPActualizarConvenioObservacion.ProcedureName := 'ActualizarConvenioObservacion';

                    FechaObservacion := NowBaseCN(DMConnections.BaseCAC);
                    CodigoConvenio := 0;
                    CantidadTeleviasConvenio := 0;

                    for i := 0 to _listaTagVencidos.Count - 1 do begin
                        if TTagVencidos(_listaTagVencidos.Items[i]).Notificado = false then begin

                            if CodigoConvenio = 0 then begin
                                InicializaObservacionPorTAGsNotificados;
                            end;

                            if CodigoConvenio <>  StrToInt(TTagVencidos(_listaTagVencidos.Items[i]).CodigoConvenio) then begin
                                ActualizaObservacionesPorTAGsNotificados;
                                InicializaObservacionPorTAGsNotificados;
                            end;

                            if CantidadTeleviasConvenio <= 10 then begin
                                if not PrimerTAG then begin
                                    TextoObservacionConvenio := TextoObservacionConvenio + ', ';
                                end
                                else PrimerTAG := False;

                                TextoObservacionConvenio := TextoObservacionConvenio + TTagVencidos(_listaTagVencidos.Items[i]).Televia;
                            end;

                            Inc(CantidadTeleviasConvenio);
                        end;
                    end;

                    ActualizaObservacionesPorTAGsNotificados;
                    // Fin Bloque: SS_960_PDO_20120104

                    QueryExecute(_pDataBase, 'IF @@TRANCOUNT > 0 BEGIN COMMIT TRANSACTION GrabarNotificacion END');

                    Notificado := True;     // SS_960_PDO_20120104
                end;
            end;
        except
            on e : Exception do begin
                QueryExecute(_pDataBase, 'IF @@TRANCOUNT > 0 BEGIN ROLLBACK TRANSACTION GrabarNotificacion END');
                MsgBoxErr(MSG_ERROR_GRABAR_NOTIFICACION, e.Message, _tituloVentana, MB_ICONERROR);
            end;
        end;
    finally
        if Assigned(SPInsertarNotificacion) then FreeAndNil(SPInsertarNotificacion);                      // SS_960_PDO_20120104
        if Assigned(SPInsertarNotificacionTagVencido) then FreeAndNil(SPInsertarNotificacionTagVencido);  // SS_960_PDO_20120104
        if Assigned(SPActualizarConvenioObservacion) then FreeAndNil(SPActualizarConvenioObservacion);    // SS_960_PDO_20120104
        Screen.Cursor := crDefault;                                             //SS-960-ALA-20110427
    end;
end;

{-----------------------------------------------------------------------------
Function Name  : Inicializar
Author         : Alejandro Labra
Date Created   : 08-04-2011
Description    : Inicializa el objeto seteando sus parametros.
-----------------------------------------------------------------------------}
procedure TAvisoTagVencidos.Inicializar(parametros : Array of String; pDataBase : TADOConnection);
var
    //SPObtenerTagVencidosCN  : TADOStoredProc;									//SS_1147_MCA_20140408
    SPObtenerTagVencidos  : TADOStoredProc;										//SS_1147_MCA_20140408
    SPObtenerAviso          : TADOStoredProc;
    TagVencidos             : TTagVencidos;
begin
    try
        if parametros[0]='' then
            raise Exception.Create('Debe enviar el rut del cliente para verificaci�n de Tags Vencidos');

        _rutCliente := parametros[0];
        _pDataBase  := pDataBase;
        _listaTagVencidos := TList.Create;
        //Llamamos a setear los colores que tendr� el formulario y el
        AsignarColores();

        //Llamamos al Store SPObtenerAviso
        SPObtenerAviso := TADOStoredProc.Create(nil);
        SPObtenerAviso.Connection := TADOConnection(_pDataBase);
        SPObtenerAviso.ProcedureName := 'ObtenerAvisos';
        SPObtenerAviso.Parameters.Refresh;
        SPObtenerAviso.Parameters.ParamByName('@NombreAviso').Value :=  NOMBRE_TIPO_AVISO;
        SPObtenerAviso.Open;

        _codigoTipoAviso            := SPObtenerAviso.FieldByName('Codigo').AsInteger;
        _mensaje                    := SPObtenerAviso.FieldByName('Mensaje').AsString;
        _tituloVentana              := SPObtenerAviso.FieldByName('Titulo').AsString;
        _tiempoCierreVentana        := SPObtenerAviso.FieldByName('TiempoCierreVentana').AsInteger;
        _tiempoVolverMostrarAviso   := SPObtenerAviso.FieldByName('TiempoVolverMostrarAviso').AsInteger;                //SS_960_ALA_20110722

        SPObtenerAviso.Close;

        //Llamamos al Store ObtenerTagVencidos                                  //SS_1147_MCA_20140408
        //SPObtenerTagVencidosCN := TADOStoredProc.Create(nil);                 //SS_1147_MCA_20140408
        //SPObtenerTagVencidosCN.Connection := TADOConnection(_pDataBase);      //SS_1147_MCA_20140408
        //SPObtenerTagVencidosCN.ProcedureName := 'ObtenerTagsVencidosCN';		//SS_1147_MCA_20140408
        //SPObtenerTagVencidosCN.Parameters.Refresh;                            //SS_1147_MCA_20140408
        //SPObtenerTagVencidosCN.Parameters.ParamByName('@Rut').Value                     := _rutCliente;                //SS_1147_MCA_20140408
        //SPObtenerTagVencidosCN.Parameters.ParamByName('@NumeroDiasNotificacion').Value  := _tiempoVolverMostrarAviso;  //SS_1147_MCA_20140408 //SS_960_ALA_20110722
        //SPObtenerTagVencidosCN.Open;

        SPObtenerTagVencidos := TADOStoredProc.Create(nil);                     //SS_1147_MCA_20140408
        SPObtenerTagVencidos.Connection := TADOConnection(_pDataBase);          //SS_1147_MCA_20140408
        SPObtenerTagVencidos.ProcedureName := 'ObtenerTagsVencidos';          	//SS_1147_MCA_20140408
        SPObtenerTagVencidos.Parameters.Refresh;                                //SS_1147_MCA_20140408
        SPObtenerTagVencidos.Parameters.ParamByName('@Rut').Value                     := _rutCliente;                //SS_1147_MCA_20140408
        SPObtenerTagVencidos.Parameters.ParamByName('@NumeroDiasNotificacion').Value  := _tiempoVolverMostrarAviso;  //SS_1147_MCA_20140408 //SS_960_ALA_20110722
        SPObtenerTagVencidos.Open;

        //while not SPObtenerTagVencidosCN.Eof do begin                         //SS_1147_MCA_20140408
        while not SPObtenerTagVencidos.Eof do begin                           	//SS_1147_MCA_20140408
            //Creando objetos e ingresandolos a la lista
            //TagVencidos                         := TTagVencidos.Create;                                                                                         	//SS_1147_MCA_20140408
            //TagVencidos.Patente                 := SPObtenerTagVencidosCN.FieldByName('Patente').AsString;                                                      	//SS_1147_MCA_20140408
                //TagVencidos.Televia                 := SPObtenerTagVencidosCN.FieldByName('Televia').AsString;                                                    //SS_1147_MCA_20140408
            //TagVencidos.FechaVencimiento        := FormatDateTime('dd"-"mm"-"yyyy hh:nn', SPObtenerTagVencidosCN.FieldByName('FechaVencimiento').AsDateTime);   	//SS_1147_MCA_20140408 // SS_960_PDO_20120104
            //TagVencidos.CodigoConvenio          := SPObtenerTagVencidosCN.FieldByName('CodigoConvenio').AsString;                                               	//SS_1147_MCA_20140408
            //TagVencidos.IndiceVehiculo          := SPObtenerTagVencidosCN.FieldByName('IndiceVehiculo').AsString;                                               	//SS_1147_MCA_20140408
            //TagVencidos.ContractSerialNumber    := SPObtenerTagVencidosCN.FieldByName('ContractSerialNumber').AsString;                                         	//SS_1147_MCA_20140408
            //TagVencidos.CodigoCliente           := SPObtenerTagVencidosCN.FieldByName('CodigoCliente').AsString;                                                	//SS_1147_MCA_20140408
            //TagVencidos.Notificado              := SPObtenerTagVencidosCN.FieldByName('Notificado').AsBoolean;                                                  	//SS_1147_MCA_20140408
            //TagVencidos.FechaUltimaNotificacion := IfThen(SPObtenerTagVencidosCN.FieldByName('FechaNotificacion').IsNull,                                       	//SS_1147_MCA_20140408 // SS-960-NDR-20120605
            //                                              '',                                                                                           			//SS_1147_MCA_20140408 // SS-960-NDR-20120605
            //                                              FormatDateTime('dd"-"mm"-"yyyy hh:nn', SPObtenerTagVencidosCN.FieldByName('FechaNotificacion').AsDateTime)//SS_1147_MCA_20140408 // SS-960-NDR-20120605
            //                                             );                                                                                                         //SS_1147_MCA_20140408 // SS-960-NDR-20120605
            //TagVencidos.NumeroConvenio          := SPObtenerTagVencidosCN.FieldByName('NumeroConvenio').AsString;

				TagVencidos                         := TTagVencidos.Create;
                TagVencidos.Patente                 := SPObtenerTagVencidos.FieldByName('Patente').AsString;
                TagVencidos.Televia                 := SPObtenerTagVencidos.FieldByName('Televia').AsString;
                TagVencidos.FechaVencimiento        := FormatDateTime('dd"-"mm"-"yyyy hh:nn', SPObtenerTagVencidos.FieldByName('FechaVencimiento').AsDateTime);   	//SS_1147_MCA_20140408 // SS_960_PDO_20120104
                TagVencidos.CodigoConvenio          := SPObtenerTagVencidos.FieldByName('CodigoConvenio').AsString;                                                 //SS_1147_MCA_20140408
                TagVencidos.IndiceVehiculo          := SPObtenerTagVencidos.FieldByName('IndiceVehiculo').AsString;                                                 //SS_1147_MCA_20140408
                TagVencidos.ContractSerialNumber    := SPObtenerTagVencidos.FieldByName('ContractSerialNumber').AsString;                                           //SS_1147_MCA_20140408
                TagVencidos.CodigoCliente           := SPObtenerTagVencidos.FieldByName('CodigoCliente').AsString;                                                  //SS_1147_MCA_20140408
                TagVencidos.Notificado              := SPObtenerTagVencidos.FieldByName('Notificado').AsBoolean;                                                    //SS_1147_MCA_20140408
                TagVencidos.FechaUltimaNotificacion := IfThen(SPObtenerTagVencidos.FieldByName('FechaNotificacion').IsNull,                                     	//SS_1147_MCA_20140408 // SS-960-NDR-20120605
                                                          '',                                                                                           			//SS_1147_MCA_20140408 // SS-960-NDR-20120605
                                                          FormatDateTime('dd"-"mm"-"yyyy hh:nn', SPObtenerTagVencidos.FieldByName('FechaNotificacion').AsDateTime)  //SS_1147_MCA_20140408 // SS-960-NDR-20120605
                                                         );                                                                                                         //SS_1147_MCA_20140408 // SS-960-NDR-20120605
            	TagVencidos.NumeroConvenio          := SPObtenerTagVencidos.FieldByName('NumeroConvenio').AsString;                                                 //SS_1147_MCA_20140408

            _listaTagVencidos.Add(TagVencidos);
            //SPObtenerTagVencidosCN.Next;										//SS_1147_MCA_20140408
            SPObtenerTagVencidos.Next;											//SS_1147_MCA_20140408
        end;
        //SPObtenerTagVencidosCN.Close;                                         //SS_1147_MCA_20140408
        SPObtenerTagVencidos.Close;                                             //SS_1147_MCA_20140408
    finally
        FreeAndNil(SPObtenerAviso);
        //FreeAndNil(SPObtenerTagVencidosCN);                                     //SS_1147_MCA_20140408
        FreeAndNil(SPObtenerTagVencidos);                                     //SS_1147_MCA_20140408
    end;
end;

{-----------------------------------------------------------------------------
Function Name  : MuestraDetalleMensaje
Author         : Alejandro Labra
Date Created   : 11-10-2011
Description    : Solo si la cantidad de Tags Vencidos es menor a 10 se muestra el
                 detalle de mensaje.
-----------------------------------------------------------------------------}
function TAvisoTagVencidos.MuestraDetalleMensaje: Boolean;
    var
        _Contador: Integer;
begin
    _TAGsSinNotificar := 0;

    for _contador := 0 to _listaTagVencidos.Count - 1 do begin
        if not TTagVencidos(_listaTagVencidos.Items[_contador]).Notificado then begin
            Inc(_TAGsSinNotificar);
        end;
    end;

    if _listaTagVencidos.Count > 10 then
        Result := False
    else
        Result := True;
end;

{-----------------------------------------------------------------------------
Function Name  : ObtenerColorFormulario
Author         : Alejandro Labra
Date Created   : 08-04-2011
Description    : Retorna la propiedad ColorFormulario
-----------------------------------------------------------------------------}
function TAvisoTagVencidos.ObtenerColorFormulario: COLORREF;
begin
    if _colorFormulario=0 then
        AsignarColores();
    Result := Self._colorFormulario;
end;

{-----------------------------------------------------------------------------
Function Name  : ObtenerColorMensaje
Author         : Alejandro Labra
Date Created   : 08-04-2011
Description    : Retorna la propiedad ColorMensaje
-----------------------------------------------------------------------------}
function TAvisoTagVencidos.ObtenerColorMensaje: COLORREF;
begin
    if _colorMensaje=0 then
        AsignarColores();
    Result := Self._colorMensaje;
end;

{-----------------------------------------------------------------------------
Function Name  : ObtenerDetalleMensaje
Author         : Alejandro Labra
Date Created   : 08-04-2011
Description    : Retorna la propiedad DetalleMensaje que contiene el detalle
                 del mensaje formateado.
-----------------------------------------------------------------------------}
function TAvisoTagVencidos.ObtenerDetalleMensaje: TStrings;
var
  i: Integer;
begin
    if _detalleMensaje=nil then begin
        //Consulta
        _detalleMensaje := TStringList.Create;
        _detalleMensaje.Add('A Imprimir*Convenio*Patente*Telev�a*Fecha Vto.*Fecha Ult. Notif.');  //SS-960-NDR-20120605

        //Solamente mostraremos los primeros 10 Tags vencidos.
        for i := 0 to 9 do begin
            if _listaTagVencidos.Count > i then begin
                _detalleMensaje.Add
                            ( ' *' +                                                                       //SS-960-NDR-20120605
                              Trim(TTagVencidos(_listaTagVencidos.Items[i]).NumeroConvenio) + '*' +        //SS-960-NDR-20120605
                              Trim(TTagVencidos(_listaTagVencidos.Items[i]).Patente) + '*' +               //SS-960-NDR-20120605
                              Trim(TTagVencidos(_listaTagVencidos.Items[i]).Televia) + '*' +               //SS-960-NDR-20120605
                              Trim(TTagVencidos(_listaTagVencidos.Items[i]).FechaVencimiento) + '*' +      //SS-960-NDR-20120605
                              Trim(TTagVencidos(_listaTagVencidos.Items[i]).FechaUltimaNotificacion)       //SS-960-NDR-20120605
                            );                                                                             //SS-960-NDR-20120605
            end;

        end;
    end;
    Result := Self._detalleMensaje;
end;



function TAvisoTagVencidos.ObtenerTAGsVencidos: TList;                          //SS-960-NDR-20120605
begin                                                                           //SS-960-NDR-20120605
  Result := Self._listaTagVencidos;                                             //SS-960-NDR-20120605
end;                                                                            //SS-960-NDR-20120605


{-----------------------------------------------------------------------------
Function Name  : ObtenerMensaje
Author         : Alejandro Labra
Date Created   : 08-04-2011
Description    : Retorna la propiedad Mensaje. Por defecto vacio.
-----------------------------------------------------------------------------}
function TAvisoTagVencidos.ObtenerMensaje: String;
resourcestring                                                                                                                                          //SS_960_ALA_20111011
    MSG_MAS_DIES_TAGS       = '  El cliente posee %d (cantidad) TAGs con garant�a vencida entregados en Comodato';      //SS_960_ALA_20111011
    MSG_TAGS_POR_NOTIFICAR  = ', de los cuales %d No han sido Notificados o ya venci� su Notificaci�n. Debe Imprimir Notificaci�n TAGs Vencidos.';      //SS_960_ALA_20111011
    MSG_TAGS_YA_NOTIFICADOS = ', ya notificados.';
begin
    _mensaje := Format(MSG_MAS_DIES_TAGS, [_listaTagVencidos.Count]);
    if _TAGsSinNotificar > 0 then begin
        _mensaje := _mensaje + Format(MSG_TAGS_POR_NOTIFICAR, [_TAGsSinNotificar]);
    end
    else _mensaje := _mensaje + MSG_TAGS_YA_NOTIFICADOS;

//    if _listaTagVencidos.Count > 10 then begin                                                                                                          //SS_960_ALA_20111011
//       _mensaje := Format(MSG_MAS_DIES_TAGS, [_listaTagVencidos.Count]);                                                                                 //SS_960_ALA_20111011
//       if True then
//    end;                                                                                                                                                //SS_960_ALA_20111011
//    if _mensaje = '' then begin
//        _mensaje :=' ';
//    end;
    Result := Self._mensaje;
end;

{-----------------------------------------------------------------------------
Function Name  : ObtenerMensaje
Author         : Alejandro Labra
Date Created   : 08-04-2011
Description    : Obtiene las notificaciones asignadas para este aviso.
-----------------------------------------------------------------------------}
function TAvisoTagVencidos.ObtenerNotificaciones: TArrayINotificacion;
resourcestring
    NOMBRE_DOCUMENTO_SIN_ANEXO          = 'PLANTILLA_TAGS_VENCIDOS_SIN_ANEXO';
    NOMBRE_DOCUMENTO_CON_ANEXO          = 'PLANTILLA_TAGS_VENCIDOS_CON_ANEXO';
    NOMBRE_DOCUMENTO_ANEXO              = 'PLANTILLA_TAGS_VENCIDOS_ANEXO';
    TAG_NOMBRE_CLIENTE                  = 'NOM_CLI';
    TAG_DIRECCION_CLIENTE               = 'DIRECCION_CLI';
    TAG_COMUNA_CLIENTE                  = 'COMUNA_CLI';
    TAG_REGION_CLIENTE                  = 'REGION_CLI';
    TAG_FECHA_ACTUAL                    = 'FECHA_ACTUAL';
    TAG_TELEVIA                         = 'TELEVIA_%s';							//SS_1185_NDR_20140509
    TAG_PATENTE                         = 'PATENTE_%s';
    TAG_FECHA_VENCIMIENTO               = 'FECHA_VENCIMIENTO_%s';
    TAG_ID_DOCUMENTO                    = 'ID_DOCUMENTO';
const                                                                                                                                                                       //SS-960-ALA-20110712
    CONST_MESES: Array[1..12] of String = ('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');    //SS-960-ALA-20110712
var
    Notificacion                        : INotificacion;
    NotificacionAnexo                   : INotificacion;
    Documentos                          : TStringList;                          //SS-960-ALA-20110427
    VariablesDocumento                  : TList;                                //SS-960-ALA-20110427
    Variables                           : TDiccionario;
    VariablesAnexo                      : TDiccionario;
    SPObtenerNotificaciones             : TADOStoredProc;
    SPObtenerDatosPersona               : TADOStoredProc;
    SPObtenerDatosCliente               : TADOStoredProc;                       //SS_1185_NDR_20140509
    SPObtenerDomicilioPrincipalPersona  : TADOStoredProc;
    i                                   : Integer;
    j                                   : Integer;
    FechaActual                         : TDateTime;
    NombreCliente                       : String;
    Direccion                           : String;
    Comuna                              : String;
    Region                              : String;
    RutaDocumentoSinAnexo               : String;
    RutaDocumentoConAnexo               : String;
    RutaDocumentoAnexo                  : String;
begin
    try
        if _notificaciones = nil then begin
            FechaActual := NowBaseCN(DMConnections.BaseCAC);
            //Obteniendo los datos de la persona
            SPObtenerDatosPersona := TADOStoredProc.Create(nil);
            SPObtenerDatosPersona.Connection := TADOConnection(_pDataBase);
            SPObtenerDatosPersona.ProcedureName := 'ObtenerDatosPersona';
            SPObtenerDatosPersona.Parameters.Refresh;
            SPObtenerDatosPersona.Parameters.ParamByName('@CodigoDocumento').Value := 'RUT';
            SPObtenerDatosPersona.Parameters.ParamByName('@NumeroDocumento').Value := _rutCliente;
            SPObtenerDatosPersona.Open;

            if SPObtenerDatosPersona.FieldByName('Personeria').AsString = PERSONERIA_JURIDICA then          //SS_1185_NDR_20140509
            begin                                                                                           //SS_1185_NDR_20140509
              SPObtenerDatosCliente := TADOStoredProc.Create(nil);                                          //SS_1185_NDR_20140509
              SPObtenerDatosCliente.Connection := TADOConnection(_pDataBase);                               //SS_1185_NDR_20140509
              SPObtenerDatosCliente.ProcedureName := 'ObtenerDatosCliente';                                 //SS_1185_NDR_20140509
              SPObtenerDatosCliente.Parameters.Refresh;                                                     //SS_1185_NDR_20140509
              SPObtenerDatosCliente.Parameters.ParamByName('@CodigoDocumento').Value := 'RUT';              //SS_1185_NDR_20140509
              SPObtenerDatosCliente.Parameters.ParamByName('@NumeroDocumento').Value := _rutCliente;        //SS_1185_NDR_20140509
              SPObtenerDatosCliente.Open;                                                                   //SS_1185_NDR_20140509
              NombreCliente := SPObtenerDatosCliente.FieldByName('Nombre').AsString;                        //SS_1185_NDR_20140509
            end                                                                                             //SS_1185_NDR_20140509
            else                                                                                            //SS_1185_NDR_20140509
            begin                                                                                           //SS_1185_NDR_20140509
              NombreCliente :=                                                                              //SS_1185_NDR_20140509
                  ArmarNombrePersonaCN(                                                                     //SS_1185_NDR_20140509
                      DMConnections.BaseCAC,                                                                //SS_1185_NDR_20140509
                      SPObtenerDatosPersona.FieldByName('Personeria').AsString,                             //SS_1185_NDR_20140509
                      SPObtenerDatosPersona.FieldByName('Nombre').AsString,                                 //SS_1185_NDR_20140509
                      SPObtenerDatosPersona.FieldByName('Apellido').AsString,                               //SS_1185_NDR_20140509
                      SPObtenerDatosPersona.FieldByName('ApellidoMaterno').AsString);                       //SS_1185_NDR_20140509
            end;                                                                                            //SS_1185_NDR_20140509

            _codigoCliente := SPObtenerDatosPersona.FieldByName('CodigoPersona').AsInteger;
            SPObtenerDatosPersona.Close;  //Cerrando la coneccion

            //Obteniendo la direccion principal del cliente
            SPObtenerDomicilioPrincipalPersona := TADOStoredProc.Create(nil);
            SPObtenerDomicilioPrincipalPersona.Connection := TADOConnection(_pDataBase);
            SPObtenerDomicilioPrincipalPersona.ProcedureName := 'ObtenerDomicilioPrincipalPersona';
            SPObtenerDomicilioPrincipalPersona.Parameters.Refresh;
            SPObtenerDomicilioPrincipalPersona.Parameters.ParamByName('@CodigoPersona').Value := _codigoCliente;
            SPObtenerDomicilioPrincipalPersona.Open;

            Direccion := Trim(SPObtenerDomicilioPrincipalPersona.FieldByName('DescriDireccion').AsString);
            Comuna := Trim(SPObtenerDomicilioPrincipalPersona.FieldByName('DescriComuna').AsString);
            Region := Trim(SPObtenerDomicilioPrincipalPersona.FieldByName('DescriRegion').AsString);
            SPObtenerDomicilioPrincipalPersona.Close;//Cerrando la coneccion

                
            //Llamamos al Store SPObtenerNotificaciones
            SPObtenerNotificaciones := TADOStoredProc.Create(nil);
            SPObtenerNotificaciones.Connection := TADOConnection(_pDataBase);
            SPObtenerNotificaciones.ProcedureName := 'ObtenerNotificaciones';
            SPObtenerNotificaciones.Parameters.Refresh;
            SPObtenerNotificaciones.Parameters.ParamByName('@CodigoAviso').Value := _codigoTipoAviso;
            SPObtenerNotificaciones.Open;

            while not SPObtenerNotificaciones.Eof do begin
                if _listaTagVencidos.Count <= 10 then begin							//SS_1185_NDR_20140509
                    //Agrandamos el array de notificaciones
                    SetLength(_notificaciones, Length(_notificaciones) + 1);

                    //Creamos un nuevo objeto INotificacion de acuerdo al nombre del implementador
                    Notificacion := TFactoryINotificacion.CrearINotificacion(SPObtenerNotificaciones.FieldByName('NombreImplementador').AsString);
                    Notificacion.AsignarConCopiaCliente(true);
                    Notificacion.AsignarTagCopia(TAG_ID_DOCUMENTO);

                    //Asignamos las variables
                    Variables := TDiccionario.Create;
                    Variables.Add(TAG_NOMBRE_CLIENTE, NombreCliente);
                    Variables.Add(TAG_DIRECCION_CLIENTE, Direccion);
                    Variables.Add(TAG_COMUNA_CLIENTE, Comuna);
                    Variables.Add(TAG_REGION_CLIENTE, Region);
                    Variables.Add(TAG_FECHA_ACTUAL, FormatDateTime('dd', FechaActual) + ' de ' + CONST_MESES[MonthOfTheYear(FechaActual)] + ' de ' + FormatDateTime('yyyy', FechaActual));          //SS-960-ALA-20110712

                    Documentos := TStringList.Create;                           //SS-960-ALA-20110427
                    VariablesDocumento := TList.Create;                         //SS-960-ALA-20110427
                    //Si los tags vencidos son menores que 10, entonces ocupamos la carta sin anexo.
                    for i := 1 to 10 do begin
                        //Agregamos solos los tags que no han sido notificados.
                        if _listaTagVencidos.Count >= i then begin
                            if TTagVencidos(_listaTagVencidos.Items[i-1]).Notificado=false then begin
                                Variables.Add(Format(TAG_TELEVIA, [FormatFloat('0#', i)]), TTagVencidos(_listaTagVencidos.Items[i-1]).Televia);											//SS_1185_NDR_20140509
                                Variables.Add(Format(TAG_PATENTE, [FormatFloat('0#', i)]), TTagVencidos(_listaTagVencidos.Items[i-1]).Patente);
                                Variables.Add(Format(TAG_FECHA_VENCIMIENTO, [FormatFloat('0#', i)]), TTagVencidos(_listaTagVencidos.Items[i-1]).FechaVencimiento);
                            end;
                        end
                        else begin //Los demas (<10) le agregamos vacio
                            Variables.Add(Format(TAG_TELEVIA, [FormatFloat('0#', i)]), '');																								//SS_1185_NDR_20140509
                            Variables.Add(Format(TAG_PATENTE, [FormatFloat('0#', i)]), '');
                            Variables.Add(Format(TAG_FECHA_VENCIMIENTO, [FormatFloat('0#', i)]), '');
                        end;
                    end;
                    //Obtenemos la ruta del documento con anexo desde los parametros generales.
                    if ObtenerParametroGeneral(DMConnections.BaseCAC, NOMBRE_DOCUMENTO_SIN_ANEXO, RutaDocumentoSinAnexo) then begin
                        Documentos.Add(RutaDocumentoSinAnexo);                  //SS-960-ALA-20110427
                        Notificacion.Plantillas := Documentos;                  //SS-960-ALA-20110427
                    end;
                    VariablesDocumento.Add(Variables);                          //SS-960-ALA-20110427
                    Notificacion.AsignarVariables(VariablesDocumento);          //SS-960-ALA-20110427

                    _notificaciones[Length(_notificaciones) - 1] := Notificacion;
                end
                else begin
                    //Agrandamos el array de notificaciones
                    SetLength(_notificaciones, Length(_notificaciones) + 1);
                    //Creamos un nuevo objeto INotificacion de acuerdo al nombre del implementador
                    Notificacion := TFactoryINotificacion.CrearINotificacion(SPObtenerNotificaciones.FieldByName('NombreImplementador').AsString);
                    Notificacion.AsignarConCopiaCliente(true);
                    Notificacion.AsignarTagCopia(TAG_ID_DOCUMENTO);

                    //Asignamos las variables
                    Variables := TDiccionario.Create;
                    Variables.Add(TAG_NOMBRE_CLIENTE, NombreCliente);
                    Variables.Add(TAG_DIRECCION_CLIENTE, Direccion);
                    Variables.Add(TAG_COMUNA_CLIENTE, Comuna);
                    Variables.Add(TAG_REGION_CLIENTE, Region);
                    Variables.Add(TAG_FECHA_ACTUAL, FormatDateTime('dd', FechaActual) + ' de ' + CONST_MESES[MonthOfTheYear(FechaActual)] + ' de ' + FormatDateTime('yyyy', FechaActual));      //SS-960-ALA-20110712

                    Documentos := TStringList.Create;                           //SS-960-ALA-20110427
                    VariablesDocumento := TList.Create;                         //SS-960-ALA-20110427

                    //Notificacion.AsignarVariables(Variables);
                    //Obtenemos la ruta del documento con anexo desde los parametros generales.
                    if ObtenerParametroGeneral(DMConnections.BaseCAC, NOMBRE_DOCUMENTO_CON_ANEXO, RutaDocumentoConAnexo) then begin
                        Documentos.Add(RutaDocumentoConAnexo);
                    end;
                    VariablesDocumento.Add(Variables);                          //SS-960-ALA-20110427

                    j := 0;
                    while j < _listaTagVencidos.Count do begin
                        VariablesAnexo := TDiccionario.Create;                  //SS-960-ALA-20110427

                        //Hacemos for hasta 23 ya que tenemos 23 posiciones para insertar en el documento de anexo.
                        for i := 1 to 23 do begin
                            //Preguntamos si j < que la cantidad de elementos de la lista de tags vencidos
                            if j < _listaTagVencidos.Count then begin
                                //Agregamos solos los tags que no han sido notificados.
                                if TTagVencidos(_listaTagVencidos.Items[j]).Notificado=false then begin
                                    VariablesAnexo.Add(Format(TAG_TELEVIA, [FormatFloat('0#', i)]), TTagVencidos(_listaTagVencidos.Items[j]).Televia);							//SS_1185_NDR_20140509
                                    VariablesAnexo.Add(Format(TAG_PATENTE, [FormatFloat('0#', i)]), TTagVencidos(_listaTagVencidos.Items[j]).Patente);
                                    VariablesAnexo.Add(Format(TAG_FECHA_VENCIMIENTO, [FormatFloat('0#', i)]), TTagVencidos(_listaTagVencidos.Items[j]).FechaVencimiento);
                                end;
                            end
                            else begin //De lo contrario agregamos espacios vacios.
                                VariablesAnexo.Add(Format(TAG_TELEVIA, [FormatFloat('0#', i)]), '');																			//SS_1185_NDR_20140509
                                VariablesAnexo.Add(Format(TAG_PATENTE, [FormatFloat('0#', i)]), '');
                                VariablesAnexo.Add(Format(TAG_FECHA_VENCIMIENTO, [FormatFloat('0#', i)]), '');
                            end;
                            j := j + 1;
                        end;
                        //Obtenemos la ruta del anexo de acuerdo a los parametros generales.
                        if ObtenerParametroGeneral(DMConnections.BaseCAC, NOMBRE_DOCUMENTO_ANEXO, RutaDocumentoAnexo) then begin
                            Documentos.Add(RutaDocumentoAnexo);                 //SS-960-ALA-20110427
                            VariablesDocumento.Add(VariablesAnexo);             //SS-960-ALA-20110427
                        end;
                    end;
                    Notificacion.Plantillas := Documentos;                      //SS-960-ALA-20110427
                    Notificacion.AsignarVariables(VariablesDocumento);          //SS-960-ALA-20110427
                    //Agregamos la notificacion del anexo al array.
                    _notificaciones[Length(_notificaciones) - 1] := Notificacion;
                end;
                SPObtenerNotificaciones.Next;
            end;
            SPObtenerNotificaciones.Close;
        end;
        Result := Self._notificaciones;
    finally
        if Assigned(SPObtenerDatosPersona) then FreeAndNil(SPObtenerDatosPersona);                              // SS_960_PDO_20111229
        if Assigned(SPObtenerDatosCliente) then FreeAndNil(SPObtenerDatosCliente);                              // SS_1185_NDR_20140509
        if Assigned(SPObtenerDomicilioPrincipalPersona) then FreeAndNil(SPObtenerDomicilioPrincipalPersona);    // SS_960_PDO_20111229
        if Assigned(SPObtenerNotificaciones) then FreeAndNil(SPObtenerNotificaciones);                          // SS_960_PDO_20111229
    end;
end;

{-----------------------------------------------------------------------------
Function Name  : ObtenerTiempoCierreVentana
Author         : Alejandro Labra
Date Created   : 08-04-2011
Description    : Retorna la propiedad TiempoCierreVentana. Por defecto 10 segundos.
-----------------------------------------------------------------------------}
function TAvisoTagVencidos.ObtenerTiempoCierreVentana: Integer;
begin
    if _tiempoCierreVentana=0 then
        _tiempoCierreVentana:=10;
    Result := Self._tiempoCierreVentana;
end;

{-----------------------------------------------------------------------------
Function Name  : ObtenerTituloVentana
Author         : Alejandro Labra
Date Created   : 08-04-2011
Description    : Retorna la propiedad TituloVentana. Por defecto vacio.
-----------------------------------------------------------------------------}
function TAvisoTagVencidos.ObtenerTituloVentana: String;
begin
    if _tituloVentana = '' then begin
        _tituloVentana := ' ';
    end;
    Result := Self._tituloVentana;
end;

procedure TAvisoTagVencidos.ReGenerarAviso;
begin

end;

{-----------------------------------------------------------------------------
Function Name  : VerificarGenerarNotificacion
Author         : Alejandro Labra
Date Created   : 08-04-2011
Description    : Verifica si se debe realizar el proceso de notificaci�n al
                 usuario. Esto ocurre si existiendo tags vencidos, almenos 1 de
                 ellos no ha sido notificado, siempre y cuando la forma de
                 atenci�n sea Presencial.
-----------------------------------------------------------------------------}
function TAvisoTagVencidos.VerificarGenerarNotificacion(
  formaAtencion: TTipoFormaAtencion): Boolean;
var
  i         : Integer;
begin
    //No se debe notificar si el tipo de atencion es Telefonico.
    if formaAtencion = Telefonico then begin
        Result  := false;
        Exit;
    end;


    //Se debe notificar si al menos existe un tag vencido que no se ha notificado.
    //Y la forma de atencion sea presencial.
    for i := 0 to _listaTagVencidos.Count - 1 do begin
        if  (TTagVencidos(_listaTagVencidos.Items[i]).Notificado=false) and
            (formaAtencion = Presencial) then begin
                Result := true;
                Exit;
            end;
    end;

    //De lo contrario, si todos los tags vencidos fueron notificados,
    //No se debe notificar. 
    Result := false;
end;

{-----------------------------------------------------------------------------
Function Name  : VerificarMostrarAviso
Author         : Alejandro Labra
Date Created   : 08-04-2011
Description    : Verifica si se debe mostrar o no el aviso.
-----------------------------------------------------------------------------}
function TAvisoTagVencidos.VerificarMostrarAviso : Boolean;
begin
    if _listaTagVencidos.Count > 0 then begin                                   //SS-960-ALA-20110423
       Result := true
    end
    else begin
        Result := false;
    end;
end;

end.
