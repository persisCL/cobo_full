{-------------------------------------------------------------------------------
File Name       :   frmVentanaMensaje.pas
Author          :   Alejandro Labra
Date Created    :   11-Abril-2011
Description     :   Ventana que muestra un aviso de acuerdo a la interfaz IAviso.

Revision        : 1
Author          : Alejandro Labra
Date            : 23-04-2011
Firma           : Rev.960 SS Ventana de mensajes de TAGs vencidos
Description     : Se arregla problema cuando se consulta un rut que no tiene
                 tags vencidos y la ventana igual se mostraba.
                 En este sentido se edita procedimiento Inicializar, se modifica
                 para que sea una funci�n y retorne true si se debe mostrar
                 la ventana o false si no se debe mostrar.
Firma           : SS-960-ALA-20110423

Revision        : 2
Author          : Alejandro Labra
Date            : 27-04-2011
Firma           : Rev.960 SS Ventana de mensajes de TAGs vencidos
Description     : Se agrega que el cursor muestre un reloj de espera mientras
                  se procesa informaci�n.
Firma           : SS-960-ALA-20110427


Firma           : SS_960_ALA_20110829
Description     : Se oculta formulario antes de que se imprima el documento.

Firma           : SS_960_ALA_20111011
Description     : Se incluye la consulta si se debe mostrar o no el detalle del mensaje.
-------------------------------------------------------------------------------}
unit frmVentanaAviso;

interface

uses
    Aviso                       ,//Interface que define el contrato para los Avisos.
    TipoFormaAtencion           ,//Enum que contiene la definici�n de las formas de atencion.
    Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
    Dialogs, StdCtrls, ExtCtrls, ADODB, CheckLst, DB, DBClient, ComCtrls,
  ImgList;                               //SS-960-NDR-20120605

type
    TTagVencidos = class                                                        //SS-960-NDR-20120605
        Patente                 : String;                                       //SS-960-NDR-20120605
        Televia                 : String;                                       //SS-960-NDR-20120605
        FechaVencimiento        : String;                                       //SS-960-NDR-20120605
        CodigoConvenio          : String;                                       //SS-960-NDR-20120605
        IndiceVehiculo          : String;                                       //SS-960-NDR-20120605
        ContractSerialNumber    : String;                                       //SS-960-NDR-20120605
        CodigoCliente           : String;                                       //SS-960-NDR-20120605
        Notificado              : Boolean;                                      //SS-960-NDR-20120605
    end;                                                                        //SS-960-NDR-20120605

    TVentanaAvisoForm = class(TForm)
        lblMensaje: TLabel;
        gbFormaAsistencia: TGroupBox;
        lblPreguntaAsistenciaPresencial: TLabel;
        bttAceptar: TButton;
        rbPresencial: TRadioButton;
        rbTelefonico: TRadioButton;
        bttAccion: TButton;
        //meListaMensajes: TMemo;                                               //SS-960-NDR-20120605
        tmrTiempoCierreVentana: TTimer;
        pnlTAGVencidos: TPanel;
    lvMensaje: TListView;
    lnCheck: TImageList;                                               //SS-960-NDR-20120605
        procedure tmrTiempoCierreVentanaTimer(Sender: TObject);
        procedure rbPresencialClick(Sender: TObject);
        procedure rbTelefonicoClick(Sender: TObject);
        procedure bttAccionClick(Sender: TObject);
        procedure bttAceptarClick(Sender: TObject);
        procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    private
        _aviso          : IAviso;
        _tiempoRestante : Integer;
        _formaAtencion  : TTipoFormaAtencion;
        procedure ObtenerFormaAtencion;
        procedure MostrarBotonAccion;
    public
        _Notificado: Boolean;   // SS_960_PDO_20120104
        function Inicializar(Aviso : IAviso; parametros : Array of String; pDataBase : TADOConnection) : Boolean;  //SS-960-ALA-20110423
        procedure LanzarAccionSinAviso;
  end;

var
  VentanaAvisoForm: TVentanaAvisoForm;

implementation

{$R *.dfm}
{-----------------------------------------------------------------------------
Function Name  : bttAccionClick
Author         : Alejandro Labra
Date Created   : 11-04-2011
Description    : Genera la notificaci�n declarada en la interfaz IAviso y
                 luego cierra la ventana
-----------------------------------------------------------------------------}
procedure TVentanaAvisoForm.bttAccionClick(Sender: TObject);
begin
    Self.Visible := false;                                                      //SS_960_ALA_20110829
	Application.ProcessMessages;                                                //SS_960_ALA_20110829
    _aviso.GenerarNotificacion(_formaAtencion, _Notificado);                    // SS_960_PDO_20120104
    Self.Tag := 0;
    //Cerramos la ventana
    Close;
end;

{-----------------------------------------------------------------------------
Function Name  : bttAceptarClick
Author         : Alejandro Labra   
Date Created   : 11-04-2011
Description    : Cierra la ventana sin generar la notificaci�n.
-----------------------------------------------------------------------------}
procedure TVentanaAvisoForm.bttAceptarClick(Sender: TObject);
begin
    Self.Tag := 0;
    //Cerramos la ventana
    Close;
end;
{-----------------------------------------------------------------------------
Function Name  : FormCloseQuery
Author         : Alejandro Labra   
Date Created   : 11-04-2011
Description    : Evento que verifica si es posible cerrar la ventana actual
                 para evitar ALT+F4.
-----------------------------------------------------------------------------}
procedure TVentanaAvisoForm.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
    if Self.Tag=1 then
        CanClose := false;
end;

{-----------------------------------------------------------------------------
Function Name  : Inicializar
Author         : Alejandro Labra   
Date Created   : 11-04-2011
Description    : Inicializa la ventana, primero inicializando el objeto aviso,
                 luego seteando los controles seg�n los valores del aviso y
                 inicalizando el timer.
-----------------------------------------------------------------------------}
function TVentanaAvisoForm.Inicializar(Aviso: IAviso;
  parametros: array of String; pDataBase : TADOConnection) : Boolean;           //SS-960-ALA-20110423
var
 iFor : Integer;                                                                //SS-960-NDR-20120605

 procedure CreaCaptions(Captions: Ansistring);
    var
        _Caption : String;
        _Veces   : Integer;
    const
        cCaptions: array[0..5] of string  = ('A Imprimir','Convenio','Patente','Telev�a','Fecha Vto.','Fecha Ult. Notif.');
        cWidths  : array[0..5] of integer = (65,120,65,80,105,105);

    function DevuelveWidth(Caption: String): Integer;
        var
            _Veces: Integer;
    begin
        for _Veces := Low(cCaptions) to High(cCaptions) do begin
            if cCaptions[_veces] = Caption then begin
                Result := cWidths[_veces];
            end;
        end;
    end;
 begin
    _Caption := '';

    for _Veces := 0 to Length(Captions) do begin

        if Captions[_Veces] = '*' then begin
            lvMensaje.Columns.Add.Caption := _Caption;
            lvMensaje.Columns[lvMensaje.Columns.Count - 1].Width := DevuelveWidth(_Caption);

            _Caption := '';
        end
        else begin
            if Captions[_Veces] <> #0 then _Caption := _Caption + Captions[_Veces];
        end;
    end;

    lvMensaje.Columns.Add.Caption := _Caption;
    lvMensaje.Columns[lvMensaje.Columns.Count - 1].Width := DevuelveWidth(_Caption);

    lvMensaje.Columns[0].Alignment := taCenter;
 end;

 procedure CreaItems(CadenaItems: String; NroItem: Integer);
    var
        _Item: TListItem;
        _Caption : String;
        _Veces   : Integer;
        _Iniciar: Boolean;
 begin
    _Caption := '';
    _Iniciar := True;

    for _Veces := 0 to Length(CadenaItems) do begin

        if CadenaItems[_Veces] = '*' then begin
            if _Iniciar then begin
                _Item := lvMensaje.Items.Add;
                _Item.Caption := _Caption;
                if not(TTagVencidos(_aviso.TAGsVencidos.Items[NroItem]).Notificado) then begin
                    _Item.ImageIndex := 1;
                end;
                _Iniciar := False;
            end
            else begin
                _Item.SubItems.Add(_Caption)
            end;
            _Caption := '';
        end
        else begin
            if CadenaItems[_Veces] <> #0 then _Caption := _Caption + CadenaItems[_Veces];
        end;
    end;

    _Item.SubItems.Add(_Caption);
 end;

begin
    try                                                                         //SS-960-ALA-20110427
        Screen.Cursor := crHourGlass;                                           //SS-960-ALA-20110427
        Application.ProcessMessages;                                            //SS-960-ALA-20110427
        FormStyle                   := fsStayOnTop;
        Position                    := poScreenCenter;
        BorderStyle                 := bsDialog;
        BorderIcons                 := [];
        Caption                     := '';
        Tag                         := 1;
        bttAceptar.Enabled          := false;
        bttAccion.Visible           := false;
        //meListaMensajes.Enabled     := false;                                 //SS-960-NDR-20120605
        _aviso                      := Aviso;

        //Inicializamos el aviso.
        _aviso.Inicializar(parametros, pDataBase);

        //Preguntamos si se debe generar el aviso
        if _aviso.VerificarMostrarAviso=false then begin
            Self.Tag := 0;
            Close;
            Result := false;                                                        //SS-960-ALA-20110423
            Exit;                                                                   //SS-960-ALA-20110423
        end;
    
        //De lo contrario proseguimos
        lblMensaje.Font.Color := _aviso.ColorMensaje;
        Self.Color := _aviso.ColorFormulario;
        Self.Caption := _aviso.TituloVentana;
        if _aviso.MuestraDetalleMensaje then                                        //SS_960_ALA_20111011
        begin
            //meListaMensajes.Lines := _aviso.DetalleMensaje;                       //SS-960-NDR-20120605
            CreaCaptions(_aviso.DetalleMensaje[0]);
            _aviso.DetalleMensaje.Delete(0);
            for iFor := 0 to _aviso.TAGsVencidos.Count - 1 do                       //SS-960-NDR-20120605
            begin
                CreaItems(_aviso.DetalleMensaje[iFor], iFor);                                                                                              //SS-960-NDR-20120605
            end;                                                                    //SS-960-NDR-20120605
        end;                                                                        //SS-960-NDR-20120605
        //else                                                                      //SS-960-NDR-20120605//SS_960_ALA_20111011
            //meListaMensajes.Lines.Clear;                                          //SS-960-NDR-20120605//SS_960_ALA_20111011
        lblMensaje.Caption := _aviso.Mensaje;
        _tiempoRestante := _aviso.TiempoCierreVentana;
        //Iniciamos el timer
        tmrTiempoCierreVentana.Enabled := true;
        Result := true;                                                             //SS-960-ALA-20110423
    finally                                                                         //SS-960-ALA-20110427
        Screen.Cursor := crDefault;                                                 //SS-960-ALA-20110427
    end;                                                                            //SS-960-ALA-20110427
end;

{-----------------------------------------------------------------------------
Function Name  : MostrarBotonAccion
Author         : Alejandro Labra   
Date Created   : 11-04-2011
Description    : De acuerdo a la forma de atenci�n muestra el bot�n de la acci�n,
                 activa el bot�n de cerrado del formulario y desactiva el timer.
                 Esto solo si se ha cumplido el tiempo de espera del formulario
-----------------------------------------------------------------------------}
procedure TVentanaAvisoForm.MostrarBotonAccion;
begin
    if _tiempoRestante<=0 then begin
        if rbPresencial.Checked or rbTelefonico.Checked then begin
            if _aviso.VerificarGenerarNotificacion(_formaAtencion) then begin
                bttAccion.Caption := 'Imprimir';
                bttAccion.Visible := true;
                bttAceptar.Enabled := false;
            end
        else begin
            bttAccion.Visible := false;
            bttAceptar.Enabled := true;
        end;

        tmrTiempoCierreVentana.Enabled := false;
        end;
    end;
end;

{-----------------------------------------------------------------------------
Function Name  : ObtenerFormaAtencion
Author         : Alejandro Labra   
Date Created   : 11-04-2011
Description    : Obtiene la forma de atenci�n de acuerdo a los checkbox
-----------------------------------------------------------------------------}
procedure TVentanaAvisoForm.ObtenerFormaAtencion;
begin
    if rbPresencial.Checked then
        _formaAtencion := Presencial;
    if rbTelefonico.Checked then
        _formaAtencion := Telefonico;
end;

{-----------------------------------------------------------------------------
Function Name  : rbPresencialClick
Author         : Alejandro Labra   
Date Created   : 11-04-2011
Description    : Cuando el usuario haga click en el checkbox se setear� la forma
                 de atenci�n y se llamar� a mostrar el bot�n.
-----------------------------------------------------------------------------}
procedure TVentanaAvisoForm.rbPresencialClick(Sender: TObject);
begin
    ObtenerFormaAtencion;
    MostrarBotonAccion;
end;

{-----------------------------------------------------------------------------
Function Name  : rbTelefonicoClick
Author         : Alejandro Labra   
Date Created   : 11-04-2011
Description    : Cuando el usuario haga click en el checkbox se setear� la forma
                 de atenci�n y se llamar� a mostrar el bot�n.
-----------------------------------------------------------------------------}
procedure TVentanaAvisoForm.rbTelefonicoClick(Sender: TObject);
begin
    ObtenerFormaAtencion;
    MostrarBotonAccion;
end;

{-----------------------------------------------------------------------------
Function Name  : tmrTiempoCierreVentanaTimer
Author         : Alejandro Labra   
Date Created   : 11-04-2011
Description    : Evento que se produce cada segundo el cual resta el tiempo
                 de cierre de la ventana.
-----------------------------------------------------------------------------}
procedure TVentanaAvisoForm.tmrTiempoCierreVentanaTimer(Sender: TObject);
begin
    if _tiempoRestante > 0 then
        begin
            _tiempoRestante := _tiempoRestante - 1;
            bttAceptar.Caption := 'Aceptar ('+ IntToStr(_tiempoRestante)+')';
        end
    else
        MostrarBotonAccion;
end;

procedure TVentanaAvisoForm.LanzarAccionSinAviso;
begin
    bttAccionClick(nil);
end;

end.
