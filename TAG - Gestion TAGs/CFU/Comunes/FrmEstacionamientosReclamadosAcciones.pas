{-------------------------------------------------------------------------------
 File Name: FrmEstacionamientosReclamadosAcciones.pas
 Author: lgisuk
 Date Created: 05/07/05
 Language: ES-AR
 Description: Informo que se realizaron acciones sobre los siguientes Estacionamientos
              porque la empresa acepto el reclamo del cliente.
              Las Acciones son:
                - Se Genero Ajuste.
                - Se Pasaron los Estacionamientos a No Facturable.
                - Se Dio infracci�n por Anulada.

Author      :   Claudio Quezada Ib��ez
Date        :   28/09/2012
Firma       :   SS_1006_1015_CQU_20120928
Descripcion :   Se modifica el dormulario ya que faltaba por adaptar la grilla
                a lo que entrega el procedimiento almacenado.
-------------------------------------------------------------------------------}
unit FrmEstacionamientosReclamadosAcciones;

interface

uses
  //Reclamo
  DMConnection,                   //Coneccion a base de datos OP_CAC
  Util,                           //CRLF
  UtilProc,                       //Mensajes
  //ImagenesEstacionamientos,              //Muestra la Ventana con la imagen
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls,  DB, ADODB,RStrings, ExtCtrls,
  VariantComboBox, Grids, DBGrids, ListBoxEx, DBListEx, ImgList, Menus;

type
  TFormEstacionamientosReclamadosAcciones = class(TForm)
    Ltitulo: TLabel;
    DataSource: TDataSource;
    spObtenerEstacionamientosReclamadosAcciones: TADOStoredProc;
    DBLEstacionamientos: TDBListEx;
    btnCerrar: TButton;
    procedure DBLEstacionamientosDrawText(Sender: TCustomDBListEx;Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;var Text: String; var TxtRect: TRect; var ItemWidth: Integer;var DefaultDraw: Boolean);
    procedure btnCerrarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function Inicializar(ListaEstacionamientos:tstringlist): Boolean;
  end;

var
  FormEstacionamientosReclamadosAcciones: TFormEstacionamientosReclamadosAcciones;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    lgisuk
  Date Created: 05/07/05
  Description: Incializacion de este formulario
  Parameters: ListaEstacionamientos:tstringlist
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFormEstacionamientosReclamadosAcciones.Inicializar(ListaEstacionamientos:tstringlist): Boolean;

    {-----------------------------------------------------------------------------
      Function Name: ObtenerDatosEstacionamientos
      Author:    lgisuk
      Date Created: 05/07/05
      Description: Cargo la grilla de Estacionamientos reclamados
      Parameters: Estacionamientos:string
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function ObtenerDatosEstacionamientos(Estacionamientos : TStringList):boolean;

            {----------------------------------------------------------------
              Function Name: ObtenerEnumeracion
              Author:    lgisuk
              Date Created: 23/09/05
              Description: Convierto la lista a un string separado por comas
              Parameters: Estacionamientos:string
              Return Value: string
            -----------------------------------------------------------------}
            function ObtenerEnumeracion(Lista: TStringList): String;
            var
                Enum: AnsiString;
                i: Integer;
            begin
                Enum := '';
                for i:= 0 to Lista.Count - 1 do begin
                    //Enum := Enum + Lista.Strings[i] + ',';
                    Enum := Enum + ParseParamByNumber(Lista.Strings[i],1,'=') + ',';
                end;
                Delete(Enum, Length(Enum), 1);
                Result := Enum;
            end;

    begin
        with spObtenerEstacionamientosReclamadosAcciones do begin
            close;
            parameters.Refresh;
            parameters.ParamByName('@Enumeracion').value := ObtenerEnumeracion(Estacionamientos);
            open;
        end;
        result:=true;
    end;

resourcestring
    MSG_ERROR   = 'Error';
begin
    Result := False;
    try
        //Si es un reclamo nuevo obtengo los reclamos del stringlist
        if ListaEstacionamientos <> nil then begin
            //Cargo la grilla con los Estacionamientos
            ObtenerDatosEstacionamientos(ListaEstacionamientos);
            //Si la consulta viene vacia no muestro el cartel
            if SPObtenerEstacionamientosReclamadosAcciones.RecordCount = 0 then exit;
        end;
        //Centro el form
        CenterForm(Self);
        //Titulo
        ActiveControl := dblEstacionamientos;
        Result := true;
    except
        on e: exception do begin
            MsgBox(e.message, MSG_ERROR, MB_ICONSTOP);
            Result := False;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: DBLEstacionamientosDrawText
  Author:    lgisuk
  Date Created: 05/07/05
  Description: doy formato a la grilla
  Parameters: Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;State: TOwnerDrawState; var Text: String; var TxtRect: TRect;var ItemWidth: Integer; var DefaultDraw: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormEstacionamientosReclamadosAcciones.DBLEstacionamientosDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;State: TOwnerDrawState; var Text: String; var TxtRect: TRect;var ItemWidth: Integer; var DefaultDraw: Boolean);

    // Inicia Bloque SS_1006_1015_CQU_20120928
    {-----------------------------------------------------------------------------
      Function Name: DibujarBitmap
      Author:    lgisuk
      Date Created: 05/07/05
      Description: Dibujo el bitmap seleccionado en la columna de la grilla
      Parameters: Grilla:TCustomDBlistEx;ImageList:TimageList;numero:integer
      Return Value: boolean
    -----------------------------------------------------------------------------}
    {
    Function DibujarBitmap(Grilla: TCustomDBlistEx; ImageList: TimageList; IndiceImg: integer):boolean;
    var
        bmp: TBitMap;
    begin
        bmp := TBitMap.Create;
        try
            ImageList.GetBitmap(IndiceImg, Bmp);
            Grilla.Canvas.Draw((Rect.Left + Rect.Right - Bmp.Width) div 2, Rect.Top, Bmp);
            Result := true;
        finally
            bmp.Free;
        end;
    end;
    }
    // Fin Bloque SS_1006_1015_CQU_20120928
var
    i:integer;
begin
    //FechaHora
    if Column = dblEstacionamientos.Columns[2] then begin
       //if text <> '' then Text := FormatDateTime('dd/mm/yyyy hh:nn', spObtenerEstacionamientosReclamadosAcciones.FieldByName('FechaHora').AsDateTime);        // SS_1006_1015_CQU_20120928
       if text <> '' then Text := FormatDateTime('dd/mm/yyyy hh:nn', spObtenerEstacionamientosReclamadosAcciones.FieldByName('FechaHoraEntrada').AsDateTime);   // SS_1006_1015_CQU_20120928
    end;

    { // Inicio Bloque SS_1006_1015_CQU_20120928
    //si el Estacionamiento tiene imagen le crea un link para que se pueda acceder a verla
    if Column = dblEstacionamientos.Columns[3] then begin
        i:=iif(spObtenerEstacionamientosReclamadosAcciones.FieldByName('RegistrationAccessibility').AsInteger > 0, 1, 0);
        //Dibujo el icono correspondiente
        DibujarBitmap(dblEstacionamientos,ilCamara,i);
    end;
    } // Fin Bloque SS_1006_1015_CQU_20120928        

    if Column = dblEstacionamientos.Columns[4] then begin                                                                                                       // SS_1006_1015_CQU_20120928
       //if text <> '' then Text := FormatDateTime('dd/mm/yyyy hh:nn', spObtenerEstacionamientosReclamadosAcciones.FieldByName('FechaHora').AsDateTime);        // SS_1006_1015_CQU_20120928
       if text <> '' then Text := FormatDateTime('dd/mm/yyyy hh:nn', spObtenerEstacionamientosReclamadosAcciones.FieldByName('FechaHoraSalida').AsDateTime);    // SS_1006_1015_CQU_20120928
    end;                                                                                                                                                        // SS_1006_1015_CQU_20120928

end;

{-----------------------------------------------------------------------------
  Function Name: btnCerrarClick
  Author:    lgisuk
  Date Created: 05/07/05
  Description: Permito cerrar el formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormEstacionamientosReclamadosAcciones.btnCerrarClick(Sender: TObject);
begin
    ModalResult := mrYes;
end;

end.
