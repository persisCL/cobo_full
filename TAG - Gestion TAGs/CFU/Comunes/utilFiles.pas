{********************************** File Header ********************************
File Name   : utilFiles
Author      :
Date Created:
Language    : ES-AR
Description : Contiene una serie de funciones �tiles para trabajar con archivos.
Caso t�pico de uso: interfaz con otras entidades, importaciones, etc.
Otras librer�as donde hay funciones �tiles: SysUtils (delphi), Util (DPS)

*******************************************************************************}

unit utilFiles;

interface
uses
    Classes, Util;

function GetNextToken(Const S: string; Separator: char; var StartPos: integer): String;
procedure Split(const S: String; Separator: Char; sl: TStringList);
function GetEntryCount(const S: String; Separator: Char): integer;

implementation

function GetNextToken(Const S: string; Separator: char; var StartPos: integer): String;
var Index: integer;
begin
   Result := '';


   if StartPos > length(S) then Exit;

   Index := StartPos;

   While (S[Index] <> Separator)
   and (Index <= length(S))do
    Index := Index + 1;

   Result := Copy(S, StartPos, Index - StartPos) ;
   StartPos := Index + 1;
end;

procedure Split(const S: String; Separator: Char; sl: TStringList);
var Start: integer;
begin
   Start := 1;
   While Start <= Length(S) do
     sl.Add(GetNextToken(S, Separator, Start)) ;
end;

function GetEntryCount(const S: String; Separator: Char): integer;
var index : integer;
begin
   index := 0; result := 0;
   While (Index <= length(S)) do
   begin
      if (S[Index] = Separator) then result := result + 1;
      Index := Index + 1;
   end;
end;

end.
 