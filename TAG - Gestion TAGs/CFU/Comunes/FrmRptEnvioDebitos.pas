{-------------------------------------------------------------------------------
 File Name: FrmRptEnvioDebitos.pas
 Author:    Lgisuk
 Date Created: 07/07/2006
 Language: ES-AR
 Description: M�dulo de Interfaces - Reporte de Envio de Debitos



Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.


 -------------------------------------------------------------------------------}
Unit FrmRptEnvioDebitos;

interface

uses
  //Reporte
  DMConnection,                //Coneccion a Base de Datos OP_CAC
  Util,                        //StringToFile,Padl..
  Utildb,                      //QueryGetValue
  UtilProc,                    //Mensajes
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ppDB, ppDBPipe, ADODB, UtilRB, ppParameter, ppBands,
  ppCtrls, ppPrnabl, ppClass, ppCache, ppComm, ppRelatv, ppProd, ppReport,
  RBSetup, ppStrtch, ppSubRpt, TXComp, TXRB, ConstParametrosGenerales;          //SS_1147_NDR_20140710

type
  TFRptEnvioDebitos = class(TForm)
    rptEnvioDebitos: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppImage2: TppImage;
    ppLtitulo: TppLabel;
    ppMaxFechaEmision: TppLabel;
    ppLabel1: TppLabel;
    ppLine1: TppLine;
    ppDetailBand1: TppDetailBand;
    ppSummaryBand1: TppSummaryBand;
    ppParameterList1: TppParameterList;
   	RBIListado: TRBInterface;
    SpObtenerReporteEnvioDebitos: TADOStoredProc;
    ppLine5: TppLine;
    Ltusuario: TppLabel;
    LtFechaHora: TppLabel;
    ppusuario: TppLabel;
    ppfechahora: TppLabel;
    ppNumeroProceso: TppLabel;
    ppIdentificacionArchivoGenerado: TppLabel;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppImporteTotal: TppLabel;
    ppCantidadComprobantes: TppLabel;
    ppLabel3: TppLabel;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppModulo: TppLabel;
    ppLine2: TppLine;
    procedure rptEnvioDebitosBeforePrint(Sender: TObject);
    procedure RBIListadoExecute(Sender: TObject; var Cancelled: Boolean);
  private
    FCodigoOperacionInterfase : Integer;
    Function ObtenerDatosReporte : Boolean;
    { Private declarations }
  public
    Function  Inicializar(CodigoOperacionInterfase : Integer) : Boolean;
    Procedure GenerarReporteDisco (CodigoOperacionInterfase : Integer; FileName : String);
    { Public declarations }
  end;

var
  FRptEnvioDebitos : TFRptEnvioDebitos;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    lgisuk
  Date Created: 07/07/2006
  Description: Inicializo este formulario
  Parameters: NombreReporte: AnsiString
  Return Value: Boolean
-----------------------------------------------------------------------------}
Function TFRptEnvioDebitos.Inicializar(CodigoOperacionInterfase : Integer): Boolean;
var                                                                             //SS_1147_NDR_20140710
  RutaLogo: AnsiString;                                                         //SS_1147_NDR_20140710
begin
  ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
  try                                                                                                 //SS_1147_NDR_20140710
    ppImage2.Picture.LoadFromFile(Trim(RutaLogo));                                                    //SS_1147_NDR_20140710
  except                                                                                              //SS_1147_NDR_20140710
    On E: Exception do begin                                                                          //SS_1147_NDR_20140710
      Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
      MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
      Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
    end;                                                                                              //SS_1147_NDR_20140710
  end;                                                                                                //SS_1147_NDR_20140710

  FCodigoOperacionInterfase := CodigoOperacionInterfase;
  Result := RBIListado.Execute;
end;

{-----------------------------------------------------------------------------
  Function Name: ObtenerDatosReporte
  Author:    lgisuk
  Date Created: 07/07/2006
  Description: Obtengo los datos a mostrar en el reporte
  Parameters: NombreReporte: AnsiString
  Return Value: Boolean
-----------------------------------------------------------------------------}
Function TFRptEnvioDebitos.ObtenerDatosReporte: Boolean;
Resourcestring
    MSG_ERROR_AL_GENERAR_REPORTE = 'Error al generar el reporte';
    MSG_ERROR = 'Error';
Const
    REPORT = 'Reporte Envio Debitos';
begin
    try
        //Encabezado, Control, Archivo y Validaci�n
        with SpObtenerReporteEnvioDebitos do begin
            Close;
            Parameters.ParamByName('@CodigoOperacionInterfase').value:= FCodigoOperacionInterfase;
            Parameters.ParamByName('@FechaProcesamiento').value:= Now;
            Parameters.ParamByName('@Usuario').value:= '';
            Parameters.ParamByName('@NombreArchivo').value:= '';
            Parameters.ParamByName('@Modulo').value:= '';
            Parameters.ParamByName('@CantidadComprobantes').value:= 0;
            Parameters.ParamByName('@ImporteTotal').value := 0;
            ExecProc;
        end;
    except
        on E : Exception do begin
            Result := False;
            MsgBoxErr(MSG_ERROR, E.Message, REPORT , MB_ICONERROR);
            Exit;
        end;
    end;

    try
        //Asigno los valores a los campos del reporte
        with SpObtenerReporteEnvioDebitos do begin

            //Encabezado
            ppModulo.Caption                           := Parameters.ParamByName('@Modulo').Value;
            ppFechaHora.Caption                        := DateTimeToStr(Parameters.ParamByName('@FechaProcesamiento').Value);
            ppUsuario.Caption                          := Parameters.ParamByName('@Usuario').Value;
            ppNumeroProceso.Caption                    := InttoStr(FCodigoOperacionInterfase);
            ppIdentificacionArchivoGenerado.Caption    := ExtractFileName(Parameters.ParamByName('@NombreArchivo').Value);
            //Datos del Registro de Control
            ppCantidadComprobantes.Caption             := IntToStr(Parameters.ParamByName('@CantidadComprobantes').value);
            ppImporteTotal.Caption                     := Parameters.ParamByName('@ImporteTotal').value;

        end;
        //Configuro el Report Builder y Ejecuto el Reporte
        rbiListado.Caption := REPORT;

    except
        on E : Exception do begin
            Result := False;
            MsgBoxErr(MSG_ERROR_AL_GENERAR_REPORTE, E.Message, MSG_ERROR, MB_ICONERROR );
            Exit;
        end;
    end; 
    Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: RBIListadoExecute
  Author:    lgisuk
  Date Created: 07/07/2006
  Description: Ejecuto el informe
  Parameters: Sender: TObject;var Cancelled: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
Procedure TFRptEnvioDebitos.RBIListadoExecute(Sender: TObject;var Cancelled: Boolean);
begin
    //Obtengo los datos del reporte
    if not ObtenerDatosReporte then begin
        //si no posible, cancelo
        Cancelled := True;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: rptEnvioDebitosBeforePrint
  Author:    lgisuk
  Date Created: 10/07/2006
  Description: Obtiene los datos antes de generar el reporte en disco
  Parameters: NombreReporte: AnsiString
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure TFRptEnvioDebitos.rptEnvioDebitosBeforePrint(Sender: TObject);
begin
     //Obtengo los datos del reporte
    if not ObtenerDatosReporte then begin
        //si no posible, salgo
        Exit;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: GenerarReporteDisco
  Author:    lgisuk
  Date Created: 07/07/2006
  Description: Guarda el Reporte en un Archivo PDF
  Parameters: NombreReporte: AnsiString
  Return Value: Boolean
-----------------------------------------------------------------------------}
procedure TFRptEnvioDebitos.GenerarReporteDisco (CodigoOperacionInterfase : Integer; FileName : String);
var
    Exportador : TPDFDevice;
begin
    FCodigoOperacionInterfase := CodigoOperacionInterfase;

    //Guardo el reporte en PDF
    Exportador := TPDFDevice.Create(Self);
    Exportador.FileName := FileName;
    try
        Exportador.Publisher := rptEnvioDebitos.Publisher;
        Exportador.Reset;
        rptEnvioDebitos.PrintToDevices;
    finally
        FreeAndNil(Exportador);
    end;
end;

end.
