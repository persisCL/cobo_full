object frmSeleccionarRolYPersona: TfrmSeleccionarRolYPersona
  Left = 333
  Top = 291
  BorderStyle = bsToolWindow
  Caption = 'Selecci'#243'n de Persona y su rol en la Cuenta'
  ClientHeight = 95
  ClientWidth = 405
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 12
    Top = 42
    Width = 20
    Height = 13
    Caption = 'Rol'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object BuscaPersona: TBuscaTabEdit
    Left = 12
    Top = 14
    Width = 382
    Height = 24
    Enabled = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 2
    Decimals = 0
    EditorStyle = bteTextEdit
    OnButtonClick = BuscaPersonaButtonClick
  end
  object cb_Roles: TComboBox
    Left = 12
    Top = 57
    Width = 205
    Height = 24
    Style = csDropDownList
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ItemHeight = 16
    ParentFont = False
    TabOrder = 3
  end
  object btnAceptar: TButton
    Left = 238
    Top = 56
    Width = 75
    Height = 25
    Caption = '&Aceptar'
    ModalResult = 1
    TabOrder = 0
  end
  object btnCancelar: TButton
    Left = 319
    Top = 56
    Width = 75
    Height = 25
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 1
  end
  object qry_Roles: TADOQuery
    Connection = DMConnections.BaseCAC
    Parameters = <>
    SQL.Strings = (
      'select * from Roles  WITH (NOLOCK) where CodigoRol <> 1')
    Left = 96
    Top = 48
  end
end
