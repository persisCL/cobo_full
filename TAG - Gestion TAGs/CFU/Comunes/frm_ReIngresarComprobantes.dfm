object frmReIngresarComprobantes: TfrmReIngresarComprobantes
  Left = 205
  Top = 125
  Anchors = []
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'ReIngreso Manual de Comprobantes'
  ClientHeight = 513
  ClientWidth = 664
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  DesignSize = (
    664
    513)
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 7
    Top = 466
    Width = 354
    Height = 13
    Caption = 
      'Si desea incluir un '#237'tem en el comprobante haga doble click sobr' +
      'e el mismo'
  end
  object lbl_EstadoEmisionBoleta: TLabel
    Left = 208
    Top = 484
    Width = 143
    Height = 13
    Alignment = taCenter
    Caption = 'Estado emisi'#243'n de boleta'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object gbDatosConvenio: TGroupBox
    Left = 6
    Top = 1
    Width = 652
    Height = 77
    Caption = ' Datos del Convenio '
    TabOrder = 0
    object Label1: TLabel
      Left = 12
      Top = 22
      Width = 78
      Height = 13
      Caption = 'RUT del Cliente:'
    end
    object Label3: TLabel
      Left = 12
      Top = 49
      Width = 105
      Height = 13
      Caption = 'Convenios del Cliente:'
    end
    object Label7: TLabel
      Left = 9
      Top = 305
      Width = 122
      Height = 13
      Caption = 'Detalle del Comprobante: '
    end
    object peRUTCliente: TPickEdit
      Left = 127
      Top = 18
      Width = 139
      Height = 21
      CharCase = ecUpperCase
      Color = 16444382
      Enabled = True
      TabOrder = 0
      OnChange = peRUTClienteChange
      EditorStyle = bteTextEdit
      OnButtonClick = peRUTClienteButtonClick
    end
    object cbConveniosCliente: TVariantComboBox
      Left = 127
      Top = 45
      Width = 218
      Height = 21
      Style = vcsDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 2
      OnChange = cbConveniosClienteChange
      Items = <>
    end
    object btnBuscarRUT: TButton
      Left = 272
      Top = 16
      Width = 73
      Height = 25
      Caption = 'Buscar'
      TabOrder = 1
      OnClick = btnBuscarRUTClick
    end
  end
  object gbDatosCliente: TGroupBox
    Left = 6
    Top = 185
    Width = 652
    Height = 100
    Caption = ' Datos del Cliente '
    TabOrder = 2
    object lNombreCompleto: TLabel
      Left = 81
      Top = 19
      Width = 83
      Height = 13
      Caption = 'lNombreCompleto'
    end
    object lDomicilio: TLabel
      Left = 81
      Top = 50
      Width = 44
      Height = 13
      Caption = 'lDomicilio'
    end
    object lComuna: TLabel
      Left = 81
      Top = 65
      Width = 41
      Height = 13
      Caption = 'lComuna'
    end
    object lRegion: TLabel
      Left = 81
      Top = 81
      Width = 36
      Height = 13
      Caption = 'lRegion'
    end
    object Label31: TLabel
      Left = 14
      Top = 19
      Width = 43
      Height = 13
      AutoSize = False
      Caption = 'Apellido:'
    end
    object Label33: TLabel
      Left = 14
      Top = 50
      Width = 59
      Height = 13
      AutoSize = False
      Caption = 'Domicilio:'
    end
    object Label34: TLabel
      Left = 14
      Top = 65
      Width = 43
      Height = 13
      AutoSize = False
      Caption = 'Comuna:'
    end
    object Label35: TLabel
      Left = 14
      Top = 81
      Width = 43
      Height = 13
      AutoSize = False
      Caption = 'Regi'#243'n:'
    end
    object lblRUT: TLabel
      Left = 14
      Top = 34
      Width = 59
      Height = 13
      AutoSize = False
      Caption = 'RUT :'
    end
    object lblRUTRUT: TLabel
      Left = 81
      Top = 34
      Width = 59
      Height = 13
      AutoSize = False
      Caption = 'lRUT'
    end
  end
  object gbItemsPendientes: TGroupBox
    Left = 7
    Top = 285
    Width = 651
    Height = 182
    Caption = ' Items Pendientes '
    TabOrder = 3
    DesignSize = (
      651
      182)
    object dbItemsPendientes: TDBListEx
      Left = 8
      Top = 17
      Width = 545
      Height = 157
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 25
          Header.Caption = ' '
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Marcado'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 110
          Header.Caption = 'Fecha'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'FechaHora'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 140
          Header.Caption = 'Concepto'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Concepto'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 160
          Header.Caption = 'Detalle'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Observaciones'
        end
        item
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 80
          Header.Caption = 'Monto'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          Header.Alignment = taCenter
          IsLink = False
          FieldName = 'DescImporte'
        end>
      DataSource = dsObtenerPendienteFacturacion
      DragReorder = True
      ParentColor = False
      TabOrder = 0
      TabStop = True
      OnDblClick = dbItemsPendientesDblClick
      OnDrawText = dbItemsPendientesDrawText
    end
    object btnAgregarItem: TButton
      Left = 556
      Top = 17
      Width = 88
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = '&Agregar'
      Enabled = False
      TabOrder = 1
      OnClick = btnAgregarItemClick
    end
    object btnModificarItem: TButton
      Left = 556
      Top = 42
      Width = 88
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = '&Modificar'
      Enabled = False
      TabOrder = 2
      OnClick = btnModificarItemClick
    end
    object btnEliminarItem: TButton
      Left = 556
      Top = 67
      Width = 88
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = '&Eliminar'
      Enabled = False
      TabOrder = 3
      OnClick = btnEliminarItemClick
    end
  end
  object gbDatosComprobante: TGroupBox
    Left = 6
    Top = 80
    Width = 652
    Height = 106
    Caption = ' Datos del Comprobante '
    TabOrder = 1
    object lbl_NroComprobante: TLabel
      Left = 9
      Top = 42
      Width = 112
      Height = 13
      AutoSize = False
      Caption = 'N'#250'mero Comprobante:'
    end
    object lbl_FechaEmision: TLabel
      Left = 400
      Top = 46
      Width = 87
      Height = 13
      AutoSize = False
      Caption = 'Fecha Emisi'#243'n:'
    end
    object Label5: TLabel
      Left = 396
      Top = 68
      Width = 56
      Height = 13
      AutoSize = False
      Caption = 'Total :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 9
      Top = 18
      Width = 100
      Height = 13
      Caption = 'Tipos Comprobantes:'
    end
    object lbl_Importe: TLabel
      Left = 491
      Top = 68
      Width = 133
      Height = 13
      AutoSize = False
    end
    object lblImpresoraFiscal: TLabel
      Left = 400
      Top = 22
      Width = 87
      Height = 13
      AutoSize = False
      Caption = 'Impresora Fiscal:'
    end
    object lblNumeroTelevia: TLabel
      Left = 9
      Top = 65
      Width = 112
      Height = 13
      AutoSize = False
      Caption = 'N'#250'mero Telev'#237'a:'
      Visible = False
    end
    object lblCuotas: TLabel
      Left = 280
      Top = 65
      Width = 112
      Height = 13
      AutoSize = False
      Caption = 'Cuotas'
      Visible = False
    end
    object CbSoporte: TCheckBox
      Left = 7
      Top = 85
      Width = 226
      Height = 19
      Caption = 'Agregar Concepto de Soporte de Telev'#237'a'
      TabOrder = 6
      Visible = False
      OnClick = CbSoporteClick
    end
    object edFechaEmision: TDateEdit
      Left = 489
      Top = 42
      Width = 145
      Height = 21
      AutoSelect = False
      TabOrder = 3
      OnChange = edNumeroComprobanteChange
      OnExit = edFechaEmisionExit
      Date = -693594.000000000000000000
    end
    object edNumeroComprobante: TNumericEdit
      Left = 121
      Top = 38
      Width = 145
      Height = 21
      TabOrder = 2
      OnChange = edNumeroComprobanteChange
      OnExit = edNumeroComprobanteExit
    end
    object cbTiposComprobantes: TVariantComboBox
      Left = 121
      Top = 15
      Width = 145
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 0
      OnChange = cbTiposComprobantesChange
      Items = <>
    end
    object cbImpresorasFiscales: TVariantComboBox
      Left = 489
      Top = 18
      Width = 145
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 1
      OnChange = cbImpresorasFiscalesChange
      Items = <>
    end
    object edNumeroTelevia: TEdit
      Left = 121
      Top = 61
      Width = 145
      Height = 21
      MaxLength = 11
      TabOrder = 4
      Visible = False
      OnExit = edNumeroTeleviaExit
    end
    object edCantidadCuotas: TNumericEdit
      Left = 328
      Top = 61
      Width = 26
      Height = 21
      MaxLength = 2
      TabOrder = 5
      Visible = False
    end
  end
  object btnSalir: TButton
    Left = 557
    Top = 484
    Width = 96
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Salir'
    TabOrder = 5
    OnClick = btnSalirClick
  end
  object nb_Botones: TNotebook
    Left = 431
    Top = 477
    Width = 118
    Height = 32
    Anchors = [akRight, akBottom]
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    PageIndex = 1
    ParentFont = False
    TabOrder = 4
    object TPage
      Left = 0
      Top = 0
      Caption = 'PAGE_REGISTRAR'
    end
    object TPage
      Left = 0
      Top = 0
      Caption = 'PAGE_CONTINUAR'
      DesignSize = (
        118
        32)
      object btnRegistrar: TButton
        Left = 21
        Top = 6
        Width = 96
        Height = 25
        Anchors = [akRight, akBottom]
        Caption = '&Registrar'
        Default = True
        Enabled = False
        TabOrder = 0
        OnClick = btnRegistrarClick
      end
    end
  end
  object spObtenerMovimientosAFacturar: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerMovimientosAFacturar;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end>
    Left = 79
    Top = 329
  end
  object dsObtenerPendienteFacturacion: TDataSource
    DataSet = cdsObtenerPendienteFacturacion
    Left = 46
    Top = 328
  end
  object cdsObtenerPendienteFacturacion: TClientDataSet
    Active = True
    Aggregates = <>
    AutoCalcFields = False
    FieldDefs = <
      item
        Name = 'Marcado'
        DataType = ftBoolean
      end
      item
        Name = 'CodigoConvenio'
        DataType = ftInteger
      end
      item
        Name = 'NumeroMovimiento'
        DataType = ftInteger
      end
      item
        Name = 'FechaHora'
        DataType = ftDateTime
      end
      item
        Name = 'CodigoConcepto'
        DataType = ftWord
      end
      item
        Name = 'Concepto'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'Observaciones'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'DescImporte'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'ImporteCtvs'
        DataType = ftLargeint
      end
      item
        Name = 'Nuevo'
        DataType = ftBoolean
      end
      item
        Name = 'Cantidad'
        DataType = ftFloat
      end
      item
        Name = 'PorcentajeIVA'
        DataType = ftFloat
      end
      item
        Name = 'EstaImpreso'
        DataType = ftBoolean
      end
      item
        Name = 'ImporteIVA'
        DataType = ftFloat
      end
      item
        Name = 'ValorNeto'
        DataType = ftFloat
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    AfterScroll = cdsObtenerPendienteFacturacionAfterScroll
    OnCalcFields = cdsObtenerPendienteFacturacionCalcFields
    Left = 381
    Top = 346
    Data = {
      640100009619E0BD01000000180000000F0000000000030000006401074D6172
      6361646F02000300000000000E436F6469676F436F6E76656E696F0400010000
      000000104E756D65726F4D6F76696D69656E746F040001000000000009466563
      6861486F726108000800000000000E436F6469676F436F6E636570746F020002
      000000000008436F6E636570746F010049000000010005574944544802000200
      32000D4F62736572766163696F6E657301004900000001000557494454480200
      02003C000B44657363496D706F72746501004900000001000557494454480200
      020014000B496D706F727465437476730800010000000000054E7565766F0200
      0300000000000843616E746964616408000400000000000D506F7263656E7461
      6A6549564108000400000000000B45737461496D707265736F02000300000000
      000A496D706F72746549564108000400000000000956616C6F724E65746F0800
      0400000000000000}
  end
  object Imagenes: TImageList
    Left = 80
    Top = 372
    Bitmap = {
      494C010102000400080010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF008C8C9400B5B5B500F7F7F700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00E7E7
      E7003939420018212100ADADAD00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00C6C6
      C600182121000000000031313900F7F7F700FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF003942
      4200181821003942420018182100ADB5B500FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF0063636B001818
      210031313900E7E7E7003131390029293100FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00C6C6C6003939
      4200FFFFFF00FFFFFF00A5A5A500212129004A525200FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF006B6B7300212929007B848400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF007373730021212900C6C6C600F7F7
      F700FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00F7F7F7005A5A5A0031313900D6DE
      DE00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0042424A001821
      2100949C9C00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF009C9C
      9C002129290094949C00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00E7E7E70042424A00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF000000008001800100000000
      8001800100000000800180010000000084018001000000008001800100000000
      8001800100000000800180010000000080018001000000008001800100000000
      8001800100000000800180010000000080018001000000008001800100000000
      8001800100000000FFFFFFFF0000000000000000000000000000000000000000
      000000000000}
  end
  object ObtenerCLiente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerCliente'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end>
    Left = 334
    Top = 379
  end
  object spAgregarMovimientoCuentaIVA: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'AgregarMovimientoCuentaIVA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IndiceVehiculo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaHora'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoConcepto'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Importe'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 10
        Value = Null
      end
      item
        Name = '@LoteFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@EsPago'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@Observaciones'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@NumeroPromocion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroFinanciamiento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ImporteIVA'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorcentajeIVA'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroMovimiento'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@UsuarioCreacion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 21
        Value = Null
      end>
    Left = 146
    Top = 385
  end
  object spCrearLoteFacturacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CrearLoteFacturacion'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Total'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaHora'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@NumeroProcesoFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@LoteFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 240
    Top = 378
  end
  object spCrearComprobante: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CrearComprobante;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@PeriodoInicial'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@PeriodoFinal'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaEmision'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaVencimiento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Personeria'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@Apellido'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@ApellidoMaterno'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@Nombre'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@TipoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@CodigoTipoMedioPago'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Sexo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@Calle'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@Numero'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@DetalleDomicilio'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@CodigoComuna'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoRegion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@CodigoPostal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@TotalComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 10
        Value = Null
      end
      item
        Name = '@SaldoAnterior'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalAPagar'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@AjusteSencilloAnterior'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@AjusteSencilloActual'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoBanco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoCuenta'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@NumeroCuentaBancaria'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@Sucursal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@CodigoTipoTarjeta'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@NumeroTarjetaCredito'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@FechaVencimientoTarjeta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@CodigoEmisorTarjeta'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Inserto1'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@Inserto2'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@Inserto3'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@Inserto4'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@ImprDetallada'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@EnvioElectronico'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@NumeroProcesoFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Direction = pdInputOutput
        Precision = 19
        Value = Null
      end
      item
        Name = '@NumeroComprobanteFiscal'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CodigoImpresoraFiscal'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 271
    Top = 378
  end
  object spActualizarLoteFacturacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarLoteFacturacion'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroMovimiento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@LoteFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@UsuarioModificacion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 302
    Top = 379
  end
  object spObtenerConveniosCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerConveniosCliente'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IncluirConveniosDeBaja'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 382
    Top = 379
  end
  object spObtenerDomicilioFacturacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDomicilioFacturacion'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Domicilio'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 255
        Value = Null
      end
      item
        Name = '@Comuna'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = Null
      end
      item
        Name = '@Region'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = Null
      end>
    Left = 210
    Top = 381
  end
  object ImpresoraFiscal: TFiscalPrinter
    Active = False
    DLL = 'TMT88IIIFCH.dll'
    ID = 1
    PollingMode = pmSinglethreaded
    Left = 416
    Top = 464
  end
  object spObtenerDetalleConceptoMovimiento: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerDetalleConceptoMovimiento;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConcepto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 382
    Top = 411
  end
  object spReaplicarConvenio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ReaplicarConvenio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 440
    Top = 344
  end
  object spCrearComprobanteCuotas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'CrearComprobanteCuotas;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CantidadCuotasTotales'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ContractSerialNumber'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@FechaEmision'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@UsuarioCreacion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 414
    Top = 411
  end
end
