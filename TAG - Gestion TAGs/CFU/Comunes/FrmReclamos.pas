{-------------------------------------------------------------------------------
 File Name: FrmReclamos.pas
 Author:    lgisuk
 Date Created: 28/03/2005
 Language: ES-AR
 Description: Permite por distintos criterios, listar todos los reclamos,
              seleccionar uno y editarlo.

 Revision 1:
    Author : mpiazza
    Date : 04/02/2009
    Description : SS-689 cuando se aplica un filtro de b�squeda y se tiene
    que liberar los datos consultados

Revision	: 2
Author		: Nelson Droguett Sierra
Date		: 29-Junio-2010
Description	: (Ref. Fase 2) Agregar Concesionaria y SubTipo OrdenServicio

 Firma       :   SS_1120_MVI_20130820
 Descripcion :  Se cambia combo de Convenio del tipo TComboBox a TVariantComboBox.
                Se crea llamado a DrawItem para verificar si el combo del convenio
                posee alg�n convenio con el texto "(Baja)" y en caso de encontrarlo
                llamar a la funci�n que lo colorear� en rojo.
                Se agrega una funcion para buscar el n�mero de convenio.

 Firma       :  SS_1120_MVI_20131017
 Descripcion :  Se agrega validaci�n para saber si el convenio es distinto de vac�o antes
                de llamar a la funci�n ObtenerNumeroConvenio.
-------------------------------------------------------------------------------}
unit FrmReclamos;

interface

uses
  //Reclamos
  DMConnection,              //coneccion a base de datos OP_CAC
  UtilProc,                  //Mensajes
  Util,                      //IIF
  UtilDB,                    //Rutinas para base de datos
  BuscaClientes,             //Busca Clientes
  OrdenesServicio,           //Rutinas para Ordenes de Servicio
  PeaProcs,
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ListBoxEx, DBListEx, StdCtrls, Validate, DateEdit, ExtCtrls, DB,
  ADODB, VariantComboBox, DmiCtrls,
  //Rev.2 / 29-Junio-2010 / Nelson Droguett Siera ------------------------------------------------------------------
  FreConcesionariaReclamoOrdenServicio, FreSubTipoReclamoOrdenServicio,
  PeaTypes;                                                                                 //SS_1120_MVI_20130820

type
  TFReclamos = class(TForm)
    GBCriterios: TGroupBox;
    DBLReclamos: TDBListEx;
    EFechaDesde: TDateEdit;
    EFechaHasta: TDateEdit;
    LFecha: TLabel;
    LRut: TLabel;
    LNumeroConvenio: TLabel;
    PBotones: TPanel;
    PCriterios: TPanel;
    BuscarBTN: TButton;
    PBotonesDerecha: TPanel;
    EditarBTN: TButton;
    SalirBTN: TButton;
    SpObtenerReclamos: TADOStoredProc;
    DataSource: TDataSource;
    LNumeroReclamo: TLabel;
    SPObtenerConveniosxNumeroDocumento: TADOStoredProc;
    Ldesde: TLabel;
    LHasta: TLabel;
    LTipoReclamo: TLabel;
    LPrioridad: TLabel;
    EPrioridad: TVariantComboBox;
    Timer: TTimer;
    CKcerrado: TCheckBox;
    peNumeroDocumento: TPickEdit;
    LLimitar: TLabel;
    txt_Top: TNumericEdit;
    LimpiarBTN: TButton;
    EnumeroReclamo: TNumericEdit;
    LEstado: TLabel;
    EEstado: TVariantComboBox;
    LCantidadTotal: TLabel;
    EtipoFecha: TVariantComboBox;
    ETipoReclamo: TVariantComboBox;
    SpObtenerTiposOrdenServicioReclamo: TADOStoredProc;
    LFuenteDelReclamo: TLabel;
    EFuenteReclamo: TVariantComboBox;
    PAdicional: TPanel;
    Label2: TLabel;
    EFechaCompromiso: TDateEdit;
    Label3: TLabel;
    EEntidad: TVariantComboBox;
    SpObtenerEntidades: TADOStoredProc;
    lblConcesionaria: TLabel;
    vcbConcesionarias: TVariantComboBox;
    lblSubTipo: TLabel;
    vcbSubTipoReclamo: TVariantComboBox;
    SpObtenerReclamosCodigoOrdenServicio: TAutoIncField;
    SpObtenerReclamosDescripcion: TStringField;
    SpObtenerReclamosFechaHoraCreacion: TDateTimeField;
    SpObtenerReclamosFechaCompromiso: TDateTimeField;
    SpObtenerReclamosFechaHoraFin: TDateTimeField;
    SpObtenerReclamosRut: TStringField;
    SpObtenerReclamosNumeroConvenio: TStringField;
    SpObtenerReclamosPatente: TStringField;
    SpObtenerReclamosPrioridad: TWordField;
    SpObtenerReclamosDescEstado: TStringField;
    SpObtenerReclamosUsuario: TStringField;
    SpObtenerReclamosEstado: TStringField;
    SpObtenerReclamosCodigoConcesionaria: TWordField;
    SpObtenerReclamosSubTipoOrdenServicio: TIntegerField;
    SpObtenerReclamosNombreCorto: TStringField;
    SpObtenerReclamosDescripcionSubTipoOrdenServicio: TStringField;
    enumeroconvenio: TVariantComboBox;                                                   //SS_1120_MVI_20130820
    procedure SalirBTNClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BuscarBTNClick(Sender: TObject);
    procedure EditarBTNClick(Sender: TObject);
    procedure ERutChange(Sender: TObject);
    procedure DBLReclamosColumns0HeaderClick(Sender: TObject);
    procedure DBLReclamosDrawText(Sender: TCustomDBListEx;Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;var Text: String; var TxtRect: TRect; var ItemWidth: Integer;var DefaultDraw: Boolean);
    procedure TimerTimer(Sender: TObject);
    procedure peNumeroDocumentoButtonClick(Sender: TObject);
    procedure EPrioridadKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure DBLReclamosDblClick(Sender: TObject);
    procedure LimpiarBTNClick(Sender: TObject);
    procedure EEstadoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure DBLReclamosMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
    procedure EtipoFechaKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure ETipoReclamoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure FormDestroy(Sender: TObject);
    procedure EFuenteReclamoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EEstadoSelect(Sender: TObject);
    procedure EEntidadDropDown(Sender: TObject);
    procedure enumeroconvenioDrawItem(Control: TWinControl; Index: Integer;           //SS_1120_MVI_20130820
      Rect: TRect; State: TOwnerDrawState);                                           //SS_1120_MVI_20130820
  private
    FUltimaBusqueda: TBusquedaCliente; //busca el cliente y devuelve el rut
    Procedure OSChanged;               //Actuliza la grilla si cambio algun reclamo
    function  QuitarSeleccionar(palabra:string):string;
    Function  Editar:boolean;
    Function  ObtenerEstado:Variant;
    Function  CargarEntidades:boolean;
    { Private declarations }
  public
    function  Inicializar : Boolean;
    { Public declarations }
  end;

var
  FReclamos: TFReclamos;
  GBitLock: TBitmap;
  GBitLockOwner: TBitmap;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    lgisuk
  Date Created: 28/03/2005
  Description: M�dulo de inicializaci�n de este formulario
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFReclamos.Inicializar: Boolean;

    Function CargarTiposReclamo:boolean;
    resourcestring
        SELECCIONAR = '(Seleccionar)';
    begin
        result:=false;
        etiporeclamo.Clear;
        etiporeclamo.value:= SELECCIONAR;
        etiporeclamo.items.add(SELECCIONAR,SELECCIONAR);
        //ejecuto la consulta
        try
            with SpObtenerTiposOrdenServicioReclamo do begin
                Close;
                Open;
                //cargo el combo
                while not eof do begin
                    etipoReclamo.Items.Add(trim(fieldbyname('Descripcion').asstring),trim(fieldbyname('Descripcion').asstring));
                    next;
                end;
                close;
            end;
            result:=true;
        except
            on e: Exception do begin
                //el stored procedure podria fallar
                MsgBoxErr('Error', e.message, 'Error Al', MB_ICONERROR);
            end;
        end;
    end;

resourcestring
	MSG_INIT_ERROR = 'Error al Inicializar';
	MSG_ERROR = 'Error';
    SELECCIONAR = '(Seleccionar)';
const
    TOP = 100;
var
    S: Tsize;
begin
	Result := False;
    //lo ajusto al tama�o disponible
    S := GetFormClientSize(Application.MainForm);
    SetBounds(0, 0, S.cx, S.cy);
    //me conecto a la base
	try
        DMConnections.BaseCAC.Connected := True;
        Result := DMConnections.BaseCAC.Connected
                                and cargartiposReclamo;
    except
        on e: Exception do begin
            MsgBoxErr(MSG_INIT_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
            Exit;
        end;
    end;
    // Todo Ok, terminamos de inicializar
    //acepto ser notificado por cambios
    //en las ordenes de servicio
    AddOSNotification(OSChanged);
    etipofecha.Value:= SELECCIONAR;
    efechadesde.Enabled:=False;
    efechahasta.Enabled:=false;
    efechadesde.Date:= now;
    efechahasta.Date:= now;
    enumeroconvenio.text := SELECCIONAR;
    CargarFuentesReclamo(EFuenteReclamo);
    //Rev.2 / 29-Junio-2010 / Nelson Droguett Sierra----------------------------------------------------------------
    CargarConcesionariasReclamoHistorico(vcbConcesionarias);                    //SS_1006_1015_MCO_20120906
    CargarSubTiposReclamo(vcbSubTiporeclamo);
    //FinRev.2------------------------------------------------------------------------------------------------------
    Padicional.visible:=false; 
    //Limitar a 100
    txt_Top.Value:= TOP;
    eprioridad.Value:=0;
    eestado.Value:=0;
end;

{-----------------------------------------------------------------------------
  Function Name: Do_Init
  Author:    lgisuk
  Date Created: 04/04/2005
  Description: Creo las imagenes que iran en la grilla
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
Procedure Do_Init;
begin
    //Imagen Candado Bloqueado
	GBitLock := TBitmap.Create;
	GBitLock.LoadFromResourceName(HInstance, 'TASK_LOCK');
    GBitLock.TransparentColor := clOlive;
    GBitLock.Transparent := True;
    //Imagen Candado Bloquedado
	GBitLockOwner := TBitmap.Create;
	GBitLockOwner.LoadFromResourceName(HInstance, 'TASK_LOCK_OWNER');
    GBitLockOwner.TransparentColor := clOlive;
    GBitLockOwner.Transparent := True;
end;

{-----------------------------------------------------------------------------
  Function Name: OSChanged
  Author:    lgisuk
  Date Created: 11/04/2005
  Description: cuando cambio una orden de servicio refresca los cambio
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFReclamos.OSChanged;
begin
    if spObtenerReclamos.Active then spObtenerReclamos.Requery;
end;

{-----------------------------------------------------------------------------
  Function Name: QuitarSeleccionar
  Author:    lgisuk
  Date Created: 04/04/2005
  Description: Quita la palabra seleccionar para buscar
  Parameters: palabra:string
  Return Value: string
-----------------------------------------------------------------------------}
function TFReclamos.QuitarSeleccionar(palabra:string):string;
resourcestring
    SELECCIONAR = '(Seleccionar)';
begin
    result:= iif(palabra = SELECCIONAR, '', palabra);
end;

{-----------------------------------------------------------------------------
  Function Name: TimerTimer
  Author:    lgisuk
  Date Created: 30/03/2005
  Description:  habilito o deshabilito fechas segun el caso
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFReclamos.TimerTimer(Sender: TObject);
begin
    if QuitarSeleccionar(etipofecha.Value) = '' then begin
        efechadesde.Enabled:=false;
        efechahasta.Enabled:=false;
    end else begin
        efechadesde.Enabled:=true;
        efechahasta.Enabled:=true;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: peNumeroDocumentoButtonClick
  Author:    lgisuk
  Date Created: 01/04/2005
  Description: Buscar al cliente y te devuelve su rut
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFReclamos.peNumeroDocumentoButtonClick(Sender: TObject);
var
	f: TFormBuscaClientes;
begin
	Application.CreateForm(TFormBuscaClientes, f);
    if f.Inicializa(FUltimaBusqueda)then begin
    	if (f.ShowModal = mrok) then begin
			peNumeroDocumento.Text := Trim(f.Persona.NumeroDocumento);
	   	end;
	end;
    f.Release;
end;


{-----------------------------------------------------------------------------
  Function Name: ERutChange
  Author:    lgisuk
  Date Created: 29/03/2005
  Description: obtengo los convenios que corresponden a ese rut
               y los cargo en un combobox
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFReclamos.ERutChange(Sender: TObject);

    Procedure CargarConvenios(NumeroDocumento:string);
    resourcestring
        SELECCIONAR = '(Seleccionar)';
    begin
        NumeroDocumento := PadL(NumeroDocumento, 9, '0');
        enumeroconvenio.text:= SELECCIONAR;
        enumeroconvenio.Clear;
        //si hay parametreos
     //   with SPObtenerConveniosxNumeroDocumento.Parameters do begin                             //SS_1120_MVI_20130820
     //       refresh;                                                                            //SS_1120_MVI_20130820
     //       parambyname('@NumeroDocumento').value:= NumeroDocumento;                            //SS_1120_MVI_20130820
     //   end;                                                                                    //SS_1120_MVI_20130820
        //ejecuto la consulta                                                                     //SS_1120_MVI_20130820
     //   try                                                                                     //SS_1120_MVI_20130820
     //       with SPObtenerConveniosxNumeroDocumento do begin                                    //SS_1120_MVI_20130820
     //           Close;                                                                          //SS_1120_MVI_20130820
     //           Open;                                                                           //SS_1120_MVI_20130820
                //asigo el primero                                                                //SS_1120_MVI_20130820
     //           if not eof then begin                                                           //SS_1120_MVI_20130820
     //               enumeroconvenio.text:= trim(fieldbyname('numeroconvenio').asstring);        //SS_1120_MVI_20130820
     //               enumeroconvenio.Items.Add(SELECCIONAR);                                     //SS_1120_MVI_20130820
     //           end;                                                                            //SS_1120_MVI_20130820
                //cargo el combo                                                                  //SS_1120_MVI_20130820
     //           while not eof do begin                                                          //SS_1120_MVI_20130820
     //               enumeroconvenio.Items.Add(trim(fieldbyname('numeroconvenio').asstring));    //SS_1120_MVI_20130820
     //               next;                                                                       //SS_1120_MVI_20130820
     //           end;                                                                            //SS_1120_MVI_20130820
     //           close;                                                                          //SS_1120_MVI_20130820
     //       end;                                                                                //SS_1120_MVI_20130820
     //   except                                                                                  //SS_1120_MVI_20130820
     //       on e: Exception do begin                                                            //SS_1120_MVI_20130820
                //el stored procedure podria fallar                                               //SS_1120_MVI_20130820
     //           MsgBoxErr('Error', e.message, 'Error Al', MB_ICONERROR);                        //SS_1120_MVI_20130820
     //       end;                                                                                //SS_1120_MVI_20130820
     //   end;                                                                                    //SS_1120_MVI_20130820
                                                                                                  //SS_1120_MVI_20130820
      CargarConveniosRUT(DMCOnnections.BaseCAC,enumeroconvenio, 'RUT', NumeroDocumento,           //SS_1120_MVI_20130820
      False, 0, True, True);                                                                      //SS_1120_MVI_20130820

    end;

begin

    Cargarconvenios(penumerodocumento.text);

end;

{-----------------------------------------------------------------------------
  Function Name: ObtenerEstado
  Author:    lgisuk
  Date Created: 14/06/2005
  Description: Obtengo el estado
  Parameters: None
  Return Value: Variant
-----------------------------------------------------------------------------}
Function TFReclamos.ObtenerEstado:Variant;
const
    PENDIENTE = 'P';
    INTERNA   = 'I';
    EXTERNA   = 'E';
    RESUELTA  = 'T';
    CANCELADA = 'C';
var
    Estado:variant;
begin
    Estado:= eestado.value;
    if Estado = 1 then Result:= PENDIENTE;
    if Estado = 2 then Result:= INTERNA;
    if Estado = 3 then Result:= EXTERNA;
    if Estado = 4 then Result:= RESUELTA;
    if Estado = 5 then Result:= CANCELADA;
end;

{-----------------------------------------------------------------------------
  Function Name: EEstadoSelect
  Author:    lgisuk
  Date Created: 14/06/2005
  Description: Hago visible el resto de los datos en caso que sea una
               busqueda de pendientes de rta interna o externa
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFReclamos.EEstadoSelect(Sender: TObject);
var valor:string;
begin
    valor:=ObtenerEstado;
    if ((valor = 'I') or (Valor = 'E')) then begin
        EEntidad.Value:=0;
        EFechaCompromiso.Date:= NULLDATE;
        //Habilito
        Padicional.visible:=true;
    end else begin
       //Deshabilito
        Padicional.visible:=false;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarEntidades
  Author:    lgisuk
  Date Created: 19/04/2005
  Description: Cargo las Entidades
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFReclamos.CargarEntidades:boolean;
const
    CONST_NINGUNO = '(Seleccionar)';
begin
    result:=false;
    eentidad.Items.Clear;
    eentidad.Items.Add(CONST_NINGUNO, 0);
    spobtenerentidades.Parameters.Refresh;
    spobtenerentidades.Parameters.ParamByName('@Tipo').value:= ObtenerEstado;
    try
        spObtenerEntidades.Open;
        while not spObtenerEntidades.Eof do begin
            EEntidad.Items.Add(spObtenerEntidades.fieldbyname('descripcion').asstring,spObtenerEntidades.fieldbyname('CodigoEntidad').asinteger);
            spObtenerEntidades.Next;
        end;
        spObtenerEntidades.Close;
        eentidad.ItemIndex:=0;
        result:=true;
    except
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: EPrioridadKeyDown
  Author:    lgisuk
  Date Created: 01/04/2005
  Description: Permito borrar la prioridad
  Parameters: Sender: TObject; var Key: Word;Shift: TShiftState
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFReclamos.EPrioridadKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
    if key = vk_delete then eprioridad.Value:=0;
end;

{-----------------------------------------------------------------------------
  Function Name: EEstadoKeyDown
  Author:    lgisuk
  Date Created: 04/04/2005
  Description: Permito borrar el estado
  Parameters: Sender: TObject; var Key: Word;Shift: TShiftState
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFReclamos.EEstadoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
    if key = vk_delete then eEstado.Value:=0;
end;

{-----------------------------------------------------------------------------
  Function Name: EtipoFechaKeyDown
  Author:    lgisuk
  Date Created: 08/04/2005
  Description:  Permito borrar la fecha
  Parameters: Sender: TObject; var Key: Word;Shift: TShiftState
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFReclamos.EtipoFechaKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
resourcestring
    SELECCIONAR = '(Seleccionar)';
begin
    if key = vk_delete then eTipoFecha.Value:= SELECCIONAR;
end;

{-----------------------------------------------------------------------------
  Function Name: ETipoReclamoKeyDown
  Author:    lgisuk
  Date Created: 08/04/2005
  Description: Permito borrar tiporeclamo
  Parameters: Sender: TObject; var Key: Word;Shift: TShiftState
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFReclamos.ETipoReclamoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
resourcestring
    SELECCIONAR = '(Seleccionar)';
begin
    if key = vk_delete then eTipoReclamo.Value:= SELECCIONAR;
end;

{-----------------------------------------------------------------------------
  Function Name: EFuenteReclamoKeyDown
  Author:    lgisuk
  Date Created: 14/06/2005
  Description:
  Parameters: Sender: TObject; var Key: Word;Shift: TShiftState
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFReclamos.EFuenteReclamoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
   if key = vk_delete then EFuenteReclamo.Value:=0;
end;

//BEGIN:SS_1120_MVI_20130820 -------------------------------------------------
{-----------------------------------------------------------------------------
  Procedure: TFReclamos.enumeroconvenioDrawItem
  Date:      20-August-2013
  Description: Verifica si existe alg�n convenio de baja para llamar a la funci�n
                que colorea de rojo el item.
-----------------------------------------------------------------------------}
procedure TFReclamos.enumeroconvenioDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  if Pos( TXT_CONVENIO_DE_BAJA, enumeroconvenio.Items[Index].Caption ) > 0 then  begin

     ItemComboBoxColor(Control, Index, Rect, State, clred, true);

  end
  else begin

     ItemComboBoxColor(Control, Index, Rect, State);

  end;

end;
//END:SS_1120_MVI_20130820 ----------------------------------------------------

{-----------------------------------------------------------------------------
  Function Name: EEntidadDropDown
  Author:    lgisuk
  Date Created: 14/06/2005
  Description: cargo las entidades seg�n su tipo
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFReclamos.EEntidadDropDown(Sender: TObject);
begin
    CargarEntidades;
end;


{-----------------------------------------------------------------------------
  Function Name: LimpiarBTNClick
  Author:    lgisuk
  Date Created: 04/04/2005
  Description: Limpia los filtros de busqueda
  Parameters: Sender: TObject
  Return Value: None

  Revision 1:
      Author : mpiazza
      Date : 04/02/2009
      Description : SS-689 cuando se aplica un filtro de b�squeda y se tiene
      que liberar los datos consultados

-----------------------------------------------------------------------------}
procedure TFReclamos.LimpiarBTNClick(Sender: TObject);
resourcestring
    SELECCIONAR = '(Seleccionar)';
const
    TOP = 100;
begin
    //Numero de Reclamo
    enumeroreclamo.value:=0;
    //Fechas
    etipofecha.Value:= SELECCIONAR;
    efechadesde.Enabled:=False;
    efechahasta.Enabled:=false;
    efechadesde.Date:= now;
    efechahasta.Date:= now;
    //Limitar a
    txt_Top.Value:= TOP; //Limitar a 100
    //Rut
    penumerodocumento.Text:= '';
    enumeroconvenio.Text:= SELECCIONAR;
    //tipo reclamo
    etiporeclamo.value:= SELECCIONAR;
    //Prioridad
    eprioridad.Value:=0;
    //Estado
    eestado.Value:=0;
    efechacompromiso.Date:= NULLDATE;
    eentidad.Value:=0;
    //fuente reclamo
    efuentereclamo.Value:=0;
    //Incluir reclamos cerrados
    ckcerrado.Checked:=false;
    SPObtenerReclamos.Close;
end;

{-----------------------------------------------------------------------------
  Function Name: BuscarBTNClick
  Author:    lgisuk
  Date Created: 28/03/2005
  Description: Busco los Reclamos
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFReclamos.BuscarBTNClick(Sender: TObject);

    //averiguo si son fechas validas
    Function SonFechasValidas:boolean;
    Const
        DATE_FORMAT = '00/00/0000';
    begin
        try
            result:= (DateToStr(EFechaDesde.Date) <> DATE_FORMAT) or
                              (DateToStr(EFechaHasta.Date) <> DATE_FORMAT);
        except

            result:=false;
        end;
    end;

    //quito los guiones del numero de convenio
    Function QuitarGuiones(Palabra:string):string;
    begin
        Result:=stringreplace(Palabra,'-','',[rfReplaceAll]);
    end;

    //Obtengo el estado
    Function ObtenerEstado:Variant;
    const
        PENDIENTE = 'P';
        INTERNA   = 'I';
        EXTERNA   = 'E';
        RESUELTA  = 'T';
        CANCELADA = 'C';
    var
        Estado:variant;
    begin
        Estado:= eestado.value;
        if Estado = 1 then Result:= PENDIENTE;
        if Estado = 2 then Result:= INTERNA;
        if Estado = 3 then Result:= EXTERNA;
        if Estado = 4 then Result:= RESUELTA;
        if Estado = 5 then Result:= CANCELADA;
    end;

resourcestring
    STR_TITLE  = 'Sistema de Reclamos';
    STR_ERROR  = 'Error al buscar los Reclamos';
    VALID_DATE = 'Ingrese Fechas Validas!';
    SQL_OBTENER_CONVENIO = 'SELECT dbo.ObtenerNumeroConvenio(%s)';                        //SS_1120_MVI_20130820
const
    CREACION   = 'Creaci�n';
    COMPROMISO = 'Compromiso';
    CIERRE     = 'Cierre';
    PENDIENTE  = '1';
var
    TipoFecha: string;
    TipoReclamo: string;
    NumeroConvenio: string;
    CantidadTotal:integer;
begin

    //quito las marcas para el usuario
    TipoFecha := QuitarSeleccionar(etipofecha.Value);
    TipoReclamo := QuitarSeleccionar(etiporeclamo.Value);
//  NumeroConvenio := QuitarSeleccionar(enumeroconvenio.text);                                                                 //SS_1120_MVI_20130820
    if  enumeroconvenio.Value > 0 then                                                                                         //SS_1120_MVI_20131017
        NumeroConvenio := QueryGetValue(DMConnections.BaseCAC, Format(SQL_OBTENER_CONVENIO, [enumeroconvenio.Value]));         //SS_1120_MVI_20130820

    //sale si se busca por fecha y estas no son validas
    if (enumeroreclamo.text = '') and (Tipofecha <> '') and (not SonFechasValidas) then begin
        MsgBox(VALID_DATE);
        exit;
    end;

    //si hay parametreos
    with SPObtenerReclamos.Parameters do begin
        refresh;
        parambyname('@DesdeFecha').value             := iif(EFechaDesde.Date = NullDate, NULL, EFechaDesde.Date);
        parambyname('@HastaFecha').value             := iif(EFechaHasta.Date = NullDate, NULL, EFechaHasta.Date);
        parambyname('@Creacion').value               := iif(etipofecha.Value = CREACION, 1, NULL);
        parambyname('@Compromiso').value             := iif(etipofecha.Value = COMPROMISO, 1, NULL);
        parambyname('@Cierre').value                 := iif(tipofecha = CIERRE, 1, NULL);
        parambyname('@Creacion').value               := iif(tipofecha = '', NULL, parambyname('@Creacion').value);
        parambyname('@Compromiso').value             := iif(tipofecha = '', NULL, parambyname('@Compromiso').value);
        parambyname('@Cierre').value                 := iif(tipofecha = '', NULL, parambyname('@Cierre').value);
        parambyname('@Descripcion').value            := iif(TipoReclamo <> '', TipoReclamo , NULL);
        parambyname('@Prioridad').value              := iif(eprioridad.value <> 0, ePrioridad.value , NULL);
        parambyname('@CodigoFuenteReclamo').value    := iif(efuentereclamo.value <> 0, efuentereclamo.value , NULL);
        Parambyname('@FechaCompromisoInterna').value := iif(EFechaCompromiso.Date = NullDate, NULL, EFechaCompromiso.Date);
        parambyname('@CodigoEntidad').value          := iif(eentidad.value <> 0, eentidad.value , NULL);
        parambyname('@Pendiente').value              := iif((ObtenerEstado = 'C') or (ObtenerEstado = 'T') or (ObtenerEstado = 'TO'),NULL,iif(CKCerrado.Checked = False, PENDIENTE , NULL));//iif(CKCerrado.Checked = False, PENDIENTE , NULL);
        parambyname('@Estado').value                 := ObtenerEstado;
        parambyname('@codigoordenservicio').value    := iif(EnumeroReclamo.text <> '', EnumeroReclamo.text , NULL);
        parambyname('@numerodocumento').value        := iif(penumerodocumento.text <> '', PadL(Trim(penumerodocumento.text), 9, '0') , NULL);
        parambyname('@numeroconvenio').value         := iif(numeroconvenio <> '', QuitarGuiones(numeroconvenio) , NULL);
        parambyname('@patente').value                := NULL;
        parambyname('@Limite').value                 := txt_top.Value;
        ParamByName('@CantidadTotal').value          := 0;
        //Rev.2 / 29-Junio-2010 / Nelson Droguett Sierra -----------------------------------------------------------
        if vcbConcesionarias.Value>0 then
        	ParamByName('@CodigoConcesionaria').value  := vcbConcesionarias.Value;
        if vcbSubTipoReclamo.Value>0 then
        	ParamByName('@SubTipoOrdenServicio').value := vcbSubTipoReclamo.Value;
    end;
    //ejecuto la consulta
    try
        SPObtenerReclamos.Close;
        SPObtenerReclamos.Open;

        //muestro la cantidad total
        lcantidadtotal.Caption:='';
        cantidadtotal:= SPObtenerReclamos.Parameters.ParamByName('@CantidadTotal').value;
        if cantidadtotal <> 0 then begin
            if  (cantidadtotal <= txt_top.value) or (txt_top.Value = 0)  then begin
                lcantidadtotal.caption:='Reclamos: '+inttostr(cantidadtotal);
            end else begin
                lcantidadtotal.caption:='Reclamos: '+ iif(txt_top.text = '', '0',txt_top.text)  + '/'+ inttostr(cantidadtotal);
            end;
        end;

    except
        on e: Exception do begin
            //el stored procedure podria fallar
            MsgBoxErr(STR_ERROR, e.message,STR_TITLE , MB_ICONERROR);
        end;
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: DBLReclamosDrawText
  Author:    lgisuk
  Date Created: 30/03/2005
  Description: Quito los segundos a las Fechas
  Parameters: Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState; var Text: String; var TxtRect: TRect; var ItemWidth: Integer; var DefaultDraw: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFReclamos.DBLReclamosDrawText(Sender: TCustomDBListEx;Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;var Text: String; var TxtRect: TRect; var ItemWidth: Integer;var DefaultDraw: Boolean);
Resourcestring
    NO_DETERMINADA =  'No Determinada';
Const
    DATE_FORMAT = 'dd/mm/yyyy hh:nn';
Var
    Estado, Usuario: AnsiString;
begin

    //Bloqueado / Desbloqueado
    Usuario := (spObtenerReclamos.FieldByName('Usuario').AsString);
    Estado := spObtenerReclamos.FieldByName('Estado').AsString;
    if Column = dblReclamos.Columns[0] then begin
        if ((Estado = 'P') or (Estado = 'I') or (Estado = 'E')) and (Usuario <> '') then begin  //inclui pendientes internas y externas
            if Usuario = UsuarioSistema then
                Sender.Canvas.Draw(Rect.Left + 2, Rect.Top, GBitLock)
            else begin
                Sender.Canvas.Draw(Rect.Left + 2, Rect.Top, GBitLockOwner);
            end;
            ItemWidth := GBitLock.Width + 4;
        end;
    end;
    //FechaHoraCreacion
    if Column = dblreclamos.Columns[3] then
       if text <> '' then Text := FormatDateTime( DATE_FORMAT , spObtenerReclamos.FieldByName('FechaHoraCreacion').AsDateTime);
    //FechaCompromiso
    if Column = dblreclamos.Columns[4] then
        if text = '' then Text := NO_DETERMINADA;
    //FechaHoraFin
    if Column = dblreclamos.Columns[5] then
       if text <> '' then Text := FormatDateTime( DATE_FORMAT , spObtenerReclamos.FieldByName('FechaHoraFin').AsDateTime);
    //Prioridad
    if Column = dblreclamos.Columns[8] then
        Text := ObtenerDescripcionPrioridadOS(spObtenerReclamos.FieldByName('Prioridad').Asinteger);//Obtengo la descripci�n de la prioridad

end;


{-----------------------------------------------------------------------------
  Function Name: DBLReclamosColumns0HeaderClick
  Author:    lgisuk
  Date Created: 30/03/2005
  Description: Ordena por Columna
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFReclamos.DBLReclamosColumns0HeaderClick(Sender: TObject);
begin
  inherited;
    if spObtenerreclamos.Active = false then exit;
    if TDBListExColumn(sender).Sorting = csAscending then TDBListExColumn(sender).Sorting := csDescending
    else TDBListExColumn(sender).Sorting := csAscending;


    if spObtenerreclamos.RecordCount > 0 then
        spObtenerreclamos.Sort := TDBListExColumn(Sender).FieldName + iif(TDBListExColumn(sender).Sorting = csAscending, ' ASC' ,' DESC');
end;


{-----------------------------------------------------------------------------
  Function Name: DBLReclamosMouseMove
  Author:    lgisuk
  Date Created: 05/04/2005
  Description: Muestro un Hint con el usuario que Tiene el Reclamo Bloqueado
  Parameters: Sender: TObject;Shift: TShiftState; X, Y: Integer
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFReclamos.DBLReclamosMouseMove(Sender: TObject;Shift: TShiftState; X, Y: Integer);
Var
    Estado, Usuario: AnsiString;
begin
    if spObtenerreclamos.Active = false then exit;
    dblreclamos.ShowHint:=false;
    dblreclamos.Hint:='';
    if x < 20 then begin
        try
            //Bloqueado / Desbloqueado
            Usuario := spObtenerReclamos.FieldByName('Usuario').AsString;
            Estado := spObtenerReclamos.FieldByName('Estado').AsString;
            if ((Estado = 'P') or (Estado = 'I')) and (Usuario <> '') then begin  //inclui pendientes internas //(Estado = 'P')//
                if Usuario <> UsuarioSistema then begin
                     DBLReclamos.Hint:='Bloqueado por Usuario: '+Usuario+inttostr(y);
                     dblreclamos.ShowHint:=true;
                end;
            end;
        except
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: Editar
  Author:    lgisuk
  Date Created: 04/04/2005
  Description: Edito un Reclamo
  Parameters: None
  Return Value: boolean

  Revision 1:
    Auhtor: nefernandez
    Date: 13/11/2008
    Description: SS 631: Se muestra un aviso con las observaciones de las cuentas
    del Cliente (RUT) en cuesti�n.

-----------------------------------------------------------------------------}
Function TFReclamos.Editar:boolean;
var
    CodigoOrdenServicio: Integer;
    Texto: AnsiString;
begin
    result:=false;
    //Salgo si no hay reclamos para seleccionar
    if SpObtenerReclamos.active = false then exit;
    //Obtengo el codigo de orden de servicio
    CodigoOrdenServicio:=SpObtenerReclamos.FieldByName('CodigoOrdenServicio').value;
    //Edito la orden de servicio
    EditarOrdenServicio(CodigoOrdenServicio);

    // Revision 1
    if not ( SpObtenerReclamos.FieldByName('Rut').IsNull ) then begin
        Texto := MensajeRUTConvenioCuentas(DMCOnnections.BaseCAC, Trim(SpObtenerReclamos.FieldByName('Rut').value));
        if Texto <> '' then
            MsgBox(Texto, Self.Caption, MB_ICONWARNING);
    end;
    // Fin Revision 1

    result:=true;
end;

{-----------------------------------------------------------------------------
  Function Name: EditarBTNClick
  Author:    lgisuk
  Date Created: 28/03/2005
  Description: Edito un reclamo
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFReclamos.EditarBTNClick(Sender: TObject);
begin
    Editar;
end;

{-----------------------------------------------------------------------------
  Function Name: DBLReclamosDblClick
  Author:    lgisuk
  Date Created: 04/04/2005
  Description: Hace que el doble click en la grilla sea equivalente
               al boton editar.
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFReclamos.DBLReclamosDblClick(Sender: TObject);
begin
    Editar;
end;

{-----------------------------------------------------------------------------
  Function Name: SalirBTNClick
  Author:    lgisuk
  Date Created: 28/03/2005
  Description: Cierro el formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFReclamos.SalirBTNClick(Sender: TObject);
begin
	close;
end;

{-----------------------------------------------------------------------------
  Function Name: Do_Final
  Author:    lgisuk
  Date Created: 04/04/2005
  Description: Libero los recursos Imagen
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
Procedure Do_Final;
begin
    FreeAndNil(GBitLock);       //Libero el recurso Imagen Lock
    FreeAndNil(GBitLockOwner);  //Libero el recurso Imagen LockOwner
end;

{-----------------------------------------------------------------------------
  Function Name: FormDestroy
  Author:    lgisuk
  Date Created: 11/04/2005
  Description: ya no deseo ser mas notificado, si hubo cambios en las
               ordenes de servicio.
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFReclamos.FormDestroy(Sender: TObject);
begin
    RemoveOSNotification(OSChanged);
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 28/03/2005
  Description:  Lo libero de memoria
  Parameters: Sender: TObject; var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFReclamos.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action := caFree;
end;


initialization
    Do_Init;
finalization
    Do_Final;
end.
