unit UltimaFactura;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Navigator, StdCtrls, ExtCtrls, Buttons, DmiCtrls,
  Grids, Util, Scada;

type
  TNavWindowUltimaFactura = class(TNavWindowFrm)
    Label1: TLabel;
	procedure btn_cerrarClick(Sender: TObject);
	procedure FormCreate(Sender: TObject);
  private
	  FNumeroComprobante: Double;
  public
	{ Public declarations }
	Procedure RefreshData; override;
	Function GetBookmark(Var Bookmark, Description: AnsiString): Boolean; override;
	Function GotoBookmark(Bookmark: AnsiString): Boolean; override;
	class Function CreateBookmark(NumeroComprobante: Double): AnsiString;
	function Inicializa: Boolean; override;
  end;

var
  NavWindowUltimaFactura: TNavWindowUltimaFactura;



implementation

{$R *.dfm}

{ TNavWindowDatosCliente }
class function TNavWindowUltimaFactura.CreateBookmark(NumeroComprobante: Double): AnsiString;
begin
	Result := FloatToStr(NumeroComprobante);
end;

function TNavWindowUltimaFactura.GetBookmark(var Bookmark, Description: AnsiString): Boolean;
resourcestring
    MSG_BOOKMARK = 'Comprobante Factura: %18.0n';
begin
	Bookmark	:= CreateBookMark(FNumeroComprobante);
	Description	:= format(MSG_BOOKMARK, [FNumeroComprobante]);
	Result		:= True;
end;

function TNavWindowUltimaFactura.GotoBookmark(Bookmark: AnsiString): Boolean;
begin
	//Cargar la Factura.
    FNumeroComprobante	:= StrToFloat(BookMark);
    Label1.Caption		:= FloatToStr(FNumeroComprobante);
	result := True;
end;

function TNavWindowUltimaFactura.Inicializa: Boolean;
begin
	Update;
	Result := Inherited Inicializa;
	//Obtengo el ultimo comprobante del Cliente y guardo las Variables
	if not Result then Exit;
end;

procedure TNavWindowUltimaFactura.btn_cerrarClick(Sender: TObject);
begin
	Close;
end;

procedure TNavWindowUltimaFactura.RefreshData;
begin
	inherited;
end;

procedure TNavWindowUltimaFactura.FormCreate(Sender: TObject);
begin
	Update;
end;

end.
