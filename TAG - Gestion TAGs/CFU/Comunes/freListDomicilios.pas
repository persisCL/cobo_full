unit freListDomicilios;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, DPSControls, ListBoxEx, DBListEx, FrmEditarDomicilio, Util, Provider,
  DB, DBClient, ADODB, Peatypes, Variants, UtilDb, UtilProc, RStrings;

type
  TFrameListDomicilios = class(TFrame)
    ObtenerDomiciliosPersona: TADOStoredProc;
    dsDomicilios: TDataSource;
    cdsDomicilios: TClientDataSet;
    dspDomicilios: TDataSetProvider;
    dblDomicilios: TDBListEx;
    cdsDomiciliosBorrados: TClientDataSet;
    cdsDomiciliosCodigoDomicilio: TIntegerField;
    cdsDomiciliosCodigoPersona: TIntegerField;
    cdsDomiciliosCodigoTipoDomicilio: TSmallintField;
    cdsDomiciliosDescriTipoDomicilio: TStringField;
    cdsDomiciliosCodigoPais: TStringField;
    cdsDomiciliosDescriPais: TStringField;
    cdsDomiciliosCodigoRegion: TStringField;
    cdsDomiciliosDescriRegion: TStringField;
    cdsDomiciliosCodigoComuna: TStringField;
    cdsDomiciliosDescriComuna: TStringField;
    cdsDomiciliosCodigoCiudad: TStringField;
    cdsDomiciliosDescriCiudad: TStringField;
    cdsDomiciliosDescripcionCiudad: TStringField;
    cdsDomiciliosCodigoCalle: TIntegerField;
    cdsDomiciliosNumero: TStringField;
    cdsDomiciliosPiso: TSmallintField;
    cdsDomiciliosDpto: TStringField;
    cdsDomiciliosCalleDesnormalizada: TStringField;
    cdsDomiciliosDetalle: TStringField;
    cdsDomiciliosCodigoPostal: TStringField;
    cdsDomiciliosDomicilioEntrega: TBooleanField;
    cdsDomiciliosPaisCalleRelacionadaUno: TStringField;
    cdsDomiciliosRegionCalleRelacionadaUno: TStringField;
    cdsDomiciliosComunaCalleRelacionadaUno: TStringField;
    cdsDomiciliosCalleRelacionadaUno: TIntegerField;
    cdsDomiciliosNumeroCalleRelacionadaUno: TIntegerField;
    cdsDomiciliosPaisCalleRelacionadaDos: TStringField;
    cdsDomiciliosRegionCalleRelacionadaDos: TStringField;
    cdsDomiciliosComunaCalleRelacionadaDos: TStringField;
    cdsDomiciliosCalleRelacionadaDos: TIntegerField;
    cdsDomiciliosNumeroCalleRelacionadaDos: TIntegerField;
    cdsDomiciliosNormalizado: TIntegerField;
    cdsDomiciliosDireccionCompleta: TStringField;
    cdsDomiciliosUbicacionGeografica: TStringField;
    cdsDomiciliosBorradosCodigoDomicilio: TIntegerField;
    cdsDomiciliosBorradosCodigoPersona: TIntegerField;
    cdsDomiciliosBorradosCodigoTipoDomicilio: TSmallintField;
    cdsDomiciliosBorradosDescriTipoDomicilio: TStringField;
    cdsDomiciliosBorradosCodigoPais: TStringField;
    cdsDomiciliosBorradosDescriPais: TStringField;
    cdsDomiciliosBorradosCodigoRegion: TStringField;
    cdsDomiciliosBorradosDescriRegion: TStringField;
    cdsDomiciliosBorradosCodigoComuna: TStringField;
    cdsDomiciliosBorradosDescriComuna: TStringField;
    cdsDomiciliosBorradosCodigoCiudad: TStringField;
    cdsDomiciliosBorradosDescriCiudad: TStringField;
    cdsDomiciliosBorradosDescripcionCiudad: TStringField;
    cdsDomiciliosBorradosCodigoCalle: TIntegerField;
    cdsDomiciliosBorradosNumero: TStringField;
    cdsDomiciliosBorradosPiso: TSmallintField;
    cdsDomiciliosBorradosDpto: TStringField;
    cdsDomiciliosBorradosCalleDesnormalizada: TStringField;
    cdsDomiciliosBorradosDetalle: TStringField;
    cdsDomiciliosBorradosCodigoPostal: TStringField;
    cdsDomiciliosBorradosDomicilioEntrega: TBooleanField;
    cdsDomiciliosBorradosPaisCalleRelacionadaUno: TStringField;
    cdsDomiciliosBorradosRegionCalleRelacionadaUno: TStringField;
    cdsDomiciliosBorradosComunaCalleRelacionadaUno: TStringField;
    cdsDomiciliosBorradosCalleRelacionadaUno: TIntegerField;
    cdsDomiciliosBorradosNumeroCalleRelacionadaUno: TIntegerField;
    cdsDomiciliosBorradosPaisCalleRelacionadaDos: TStringField;
    cdsDomiciliosBorradosRegionCalleRelacionadaDos: TStringField;
    cdsDomiciliosBorradosComunaCalleRelacionadaDos: TStringField;
    cdsDomiciliosBorradosCalleRelacionadaDos: TIntegerField;
    cdsDomiciliosBorradosNumeroCalleRelacionadaDos: TIntegerField;
    cdsDomiciliosBorradosNormalizado: TIntegerField;
    cdsDomiciliosBorradosDireccionCompleta: TStringField;
    cdsDomiciliosBorradosUbicacionGeografica: TStringField;
    btnAgregarDomicilio: TButton;
    btnEditarDomicilio: TButton;
    btnEliminarDomicilio: TButton;
    procedure dblDomiciliosDblClick(Sender: TObject);
    procedure btnEditarDomicilioClick(Sender: TObject);
    procedure dblDomiciliosDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: String; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure cdsDomiciliosAfterOpen(DataSet: TDataSet);
    procedure btnAgregarDomicilioClick(Sender: TObject);
    function GetDomiciliosCargados:TClientDataSet;
    function GetTieneDomiciliosCargados:Boolean;
    function GetDomiciliosBorrados:TClientDataSet;
    function GetTieneDomiciliosBorrados:Boolean;
    procedure btnEliminarDomicilioClick(Sender: TObject);

  private
    { Private declarations }
    FCodigoTipoDomicilio:Integer;
//    function ControlarDomicilioEntrega(Valor:Boolean;DomiciolioActual:Integer=-1):Boolean;
  public
    { Public declarations }
    property DomiciliosCargados: TClientDataSet read GetDomiciliosCargados;
    property TieneDomiciliosCargados:Boolean read GetTieneDomiciliosCargados;
    property DomiciliosBorrados:TClientDataSet read GetDomiciliosBorrados;
    property TieneDomiciliosBorrados:Boolean read GetTieneDomiciliosBorrados;

    function inicializar(CodigoTipoDomicilio:Integer=1):boolean;
    function CargarDomiciliosPersona(CodigoPersona: integer):Boolean;
  end;

implementation

{$R *.dfm}

procedure TFrameListDomicilios.dblDomiciliosDblClick(Sender: TObject);
begin
    if btnEditarDomicilio.Enabled then btnEditarDomicilio.Click;
end;

procedure TFrameListDomicilios.btnEditarDomicilioClick(Sender: TObject);
//var
//	f: TFormEditarDomicilio;
//    i:Integer;
//    DomicilioRelacionado:Boolean;
begin
(*
	Application.CreateForm(TFormEditarDomicilio, f);
    try
        with cdsDomicilios do begin
                if f.Inicializar(FieldByName('CodigoDomicilio').AsInteger,
                            fCodigoTipoDomicilio,
                            FieldByName('CodigoPais').AsString,
                            FieldByName('CodigoRegion').AsString,
                            FieldByName('CodigoComuna').AsString,
                            FieldByName('DescripcionCiudad').AsString,
                            FieldByName('CodigoCalle').AsInteger,
                            FieldByName('CalleDesnormalizada').AsString,
                            FieldByName('Numero').AsString,
                            FieldByName('Piso').AsInteger,
                            FieldByName('Dpto').AsString,
                            FieldByName('Detalle').AsString,
                            FieldByName('CodigoPostal').AsString,
                            FieldByName('TipoDomicilio').AsInteger,
                            FieldByName('DomicilioEntrega').AsBoolean,
                            FieldByName('CalleRelacionadaUno').AsInteger,
                            FieldByName('NumeroCalleRelacionadaUno').AsInteger,
                            FieldByName('CalleRelacionadaDos').AsInteger,
                            FieldByName('NumeroCalleRelacionadaDos').AsInteger,
                            FieldByName('PaisCalleRelacionadaUno').AsString,
                            FieldByName('PaisCalleRelacionadaDos').AsString,
                            FieldByName('ComunaCalleRelacionadaUno').AsString,
                            FieldByName('ComunaCalleRelacionadaDos').AsString,
                            FieldByName('RegionCalleRelacionadaUno').AsString,
                            FieldByName('RegionCalleRelacionadaDos').AsString
                            ) and (f.ShowModal = mrOK) then begin
                try
                    for i := 0 to FieldCount -1 do begin
                        FieldDefs[i].Attributes  := [];
                        Fields[i].ReadOnly:=False;
                    end;
                    DomicilioRelacionado:=ControlarDomicilioEntrega(f.DomicilioEntrega,RecNo);
                    Edit;

                    FieldByName('CodigoDomicilio').AsInteger:=f.CodigoDomicilio;
                    FieldByName('TipoDomicilio').AsInteger:=f.TipoDomicilio;
                    FieldByName('CodigoPais').AsString:=f.Pais;
                    FieldByName('CodigoRegion').AsString:=f.Region;
                    FieldByName('CodigoComuna').AsString:=f.Comuna;
//                    FieldByName('CodigoCiudad').AsString:=f.CodigoCiudad;
                    FieldByName('DescripcionCiudad').AsString:=f.Ciudad;
                    FieldByName('CodigoCalle').AsInteger:=f.CodigoCalle;
                    FieldByName('Numero').AsString:=f.Numero;
                    FieldByName('Piso').AsInteger:=f.Piso;
                    FieldByName('Dpto').AsString:=f.Depto;
                    FieldByName('CalleDesnormalizada').AsString:=f.DescripcionCalle;
                    FieldByName('Detalle').AsString:=f.Detalle;
                    FieldByName('CodigoPostal').AsString:=f.CodigoPostal;
                    FieldByName('PaisCalleRelacionadaUno').AsString:=f.PaisCalleRelacionadaUno;
                    FieldByName('RegionCalleRelacionadaUno').AsString:=f.RegionCalleRelacionadaUno;
                    FieldByName('ComunaCalleRelacionadaUno').AsString:=f.ComunaCalleRelacionadaUno;
                    FieldByName('CalleRelacionadaUno').AsInteger:=f.CalleRelacionadaUno;
                    FieldByName('NumeroCalleRelacionadaUno').AsInteger:=f.NumeroCalleRelacionadaUno;
                    FieldByName('PaisCalleRelacionadaDos').AsString:=f.PaisCalleRelacionadaDos;
                    FieldByName('RegionCalleRelacionadaDos').AsString:=f.RegionCalleRelacionadaDos;
                    FieldByName('ComunaCalleRelacionadaDos').AsString:=f.ComunaCalleRelacionadaDos;
                    FieldByName('CalleRelacionadaDos').AsInteger:=f.CalleRelacionadaDos;
                    FieldByName('NumeroCalleRelacionadaDos').AsInteger:=f.NumeroCalleRelacionadaDos;
                    FieldByName('Normalizado').AsInteger:=iif(f.EsNormalizado,1,0);
                    FieldByName('DomicilioEntrega').AsBoolean:=DomicilioRelacionado;
                    FieldByName('DireccionCompleta').AsString:=f.DomicilioCompleto;
                    FieldByName('UbicacionGeografica').AsString:=f.UbicacionGeografica;

                    Post;
                except
                    On E: Exception do begin
                        MsgBoxErr( Format(MSG_ERROR_ACTUALIZAR,[FLD_DOMICILIO]), E.message, Format(MSG_CAPTION_ACTUALIZAR,[FLD_DOMICILIO]), MB_ICONSTOP);
                        dsDomicilios.DataSet.Cancel;
                        f.Release;
                    end;
                end;
            end;
        end;
    finally
        f.Release;
        dblDomicilios.SetFocus;
    end;
    *)
end;

procedure TFrameListDomicilios.dblDomiciliosDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: String; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
    if (Column.Index = 1) and (dsDomicilios.DataSet.fieldbyname('DomicilioEntrega').AsBoolean) then Text := MSG_SI;
end;

function TFrameListDomicilios.CargarDomiciliosPersona(CodigoPersona: integer):Boolean;
begin
    try
        //Primero intento abrir el Stored Procedure
        ObtenerDomiciliosPersona.Close;
        ObtenerDomiciliosPersona.Parameters.ParamByName('@CodigoPersona').Value:= CodigoPersona;
        ObtenerDomiciliosPersona.Parameters.ParamByName('@CodigoTipoDomicilio').Value  := iif(FCodigoTipoDomicilio < 1, null, FCodigoTipoDomicilio);
        ObtenerDomiciliosPersona.open;

        cdsDomicilios.Close;
        cdsDomicilios.CreateDataSet;
        cdsDomiciliosBorrados.Close;
        cdsDomiciliosBorrados.CreateDataSet;

        cdsDomicilios.Data := dspDomicilios.Data;
        cdsDomicilios.AfterOpen(cdsDomicilios);
        ObtenerDomiciliosPersona.Close;
        result:=true;
    except
    	on E: Exception do begin
            result:=false;
        	MsgBoxErr(format(MSG_ERROR_INICIALIZACION,[FLD_DOMICILIO]), e.Message, format(MSG_CAPTION_LIST,[FLD_DOMICILIO]), MB_ICONSTOP);
        end;
    end;

end;

procedure TFrameListDomicilios.cdsDomiciliosAfterOpen(DataSet: TDataSet);
begin
    btnEditarDomicilio.Enabled      := ((cdsDomicilios.Active) and (cdsDomicilios.RecordCount > 0));
    btnEliminarDomicilio.Enabled    := btnEditarDOmicilio.Enabled;
end;

procedure TFrameListDomicilios.btnAgregarDomicilioClick(Sender: TObject);
//var
//	f: TFormEditarDomicilio;
//    DomicilioRelacionado:Boolean;
begin
(*
	Application.CreateForm(TFormEditarDomicilio, f);
    try
        if f.Inicializar(fCodigoTipoDomicilio) and (f.ShowModal = mrOK) then begin
            DomicilioRelacionado:=ControlarDomicilioEntrega(f.DomicilioEntrega);
            try
                with cdsDomicilios do begin
                    Append;

                    FieldByName('CodigoDomicilio').AsInteger:=f.CodigoDomicilio;
                    FieldByName('TipoDomicilio').AsInteger:=f.TipoDomicilio;
                    FieldByName('CodigoPais').AsString:=f.Pais;
                    FieldByName('CodigoRegion').AsString:=f.Region;
                    FieldByName('CodigoComuna').AsString:=f.Comuna;
//                    FieldByName('CodigoCiudad').AsString:=f.CodigoCiudad;
                    FieldByName('DescripcionCiudad').AsString:=f.Ciudad;
                    FieldByName('CodigoCalle').AsInteger:=f.CodigoCalle;
                    FieldByName('Numero').AsString:=f.Numero;
                    FieldByName('Piso').AsInteger:=f.Piso;
                    FieldByName('Dpto').AsString:=f.Depto;
                    FieldByName('CalleDesnormalizada').AsString:=iif(f.EsNormalizado,'',f.DescripcionCalle);
                    FieldByName('Detalle').AsString:=f.Detalle;
                    FieldByName('CodigoPostal').AsString:=f.CodigoPostal;
                    FieldByName('PaisCalleRelacionadaUno').AsString:=f.PaisCalleRelacionadaUno;
                    FieldByName('RegionCalleRelacionadaUno').AsString:=f.RegionCalleRelacionadaUno;
                    FieldByName('ComunaCalleRelacionadaUno').AsString:=f.ComunaCalleRelacionadaUno;
                    FieldByName('CalleRelacionadaUno').AsInteger:=f.CalleRelacionadaUno;
                    FieldByName('NumeroCalleRelacionadaUno').AsInteger:=f.NumeroCalleRelacionadaUno;
                    FieldByName('CalleRelacionadaDos').AsInteger:=f.CalleRelacionadaDos;
                    FieldByName('PaisCalleRelacionadaDos').AsString:=f.PaisCalleRelacionadaDos;
                    FieldByName('RegionCalleRelacionadaDos').AsString:=f.RegionCalleRelacionadaDos;
                    FieldByName('ComunaCalleRelacionadaDos').AsString:=f.ComunaCalleRelacionadaDos;
                    FieldByName('NumeroCalleRelacionadaDos').AsInteger:=f.NumeroCalleRelacionadaDos;
                    FieldByName('Normalizado').AsInteger:=iif(f.EsNormalizado=true,1,0);
                    FieldByName('DomicilioEntrega').AsBoolean:= DomicilioRelacionado;
                    FieldByName('DireccionCompleta').AsString:=f.DomicilioCompleto;
                    FieldByName('UbicacionGeografica').AsString:=f.UbicacionGeografica;

                    Post;
                end;
            except
                On E: Exception do begin
                    MsgBoxErr( Format(MSG_ERROR_AGREGAR,[FLD_DOMICILIO]), E.message, Format(MSG_CAPTION_AGREGAR,[FLD_DOMICILIO]), MB_ICONSTOP);
                    dsDomicilios.DataSet.Cancel;
                end;
            end;
        end;
    finally
        f.Release;
        dblDomicilios.SetFocus;
    end;
*)
end;

function TFrameListDomicilios.inicializar(CodigoTipoDomicilio:Integer=1):boolean;
begin
    FCodigoTipoDomicilio:=CodigoTipoDomicilio;
    cdsDomicilios.Close;
    cdsDomicilios.CreateDataSet;
    cdsDomicilios.AfterOpen(cdsDomicilios);
    cdsDomiciliosBorrados.Close;
    cdsDomiciliosBorrados.CreateDataSet;
    result:=True;
end;

function TFrameListDomicilios.GetDomiciliosCargados: TClientDataSet;
begin
    result:=cdsDomicilios;
end;

function TFrameListDomicilios.GetTieneDomiciliosCargados: Boolean;
begin
    result:=((cdsDomicilios.Active) and (cdsDomicilios.RecordCount > 0));
end;

function TFrameListDomicilios.GetDomiciliosBorrados: TClientDataSet;
begin
    result:=cdsDomiciliosBorrados;
end;

function TFrameListDomicilios.GetTieneDomiciliosBorrados: Boolean;
begin
    result:=((cdsDomiciliosBorrados.Active) and (cdsDomiciliosBorrados.RecordCount > 0));
end;

procedure TFrameListDomicilios.btnEliminarDomicilioClick(Sender: TObject);
begin
	Screen.Cursor   := crHourGlass;
	If MsgBox(Format(MSG_QUESTION_ELIMINAR,[FLD_DOMICILIO]), Format(MSG_CAPTION_ELIMINAR,[FLD_DOMICILIO]), MB_YESNO + MB_ICONQUESTION) = IDYES then begin
		try

            with cdsDomiciliosBorrados do begin
                Append;
                FieldByName('CodigoDomicilio').AsInteger:=dsDomicilios.DataSet.FieldByName('CodigoDomicilio').AsInteger;
                FieldByName('TipoDomicilio').AsInteger:=dsDomicilios.DataSet.FieldByName('TipoDomicilio').AsInteger;
                FieldByName('CodigoPais').AsString:=dsDomicilios.DataSet.FieldByName('CodigoPais').AsString;
                FieldByName('CodigoRegion').AsString:=dsDomicilios.DataSet.FieldByName('CodigoRegion').AsString;
                FieldByName('CodigoComuna').AsString:=dsDomicilios.DataSet.FieldByName('CodigoComuna').AsString;
//                FieldByName('CodigoCiudad').AsString:=dsDomicilios.DataSet.FieldByName('CodigoCiudad').AsString;
                FieldByName('DescripcionCiudad').AsString:=dsDomicilios.DataSet.FieldByName('DescripcionCiudad').AsString;
                FieldByName('CodigoCalle').AsInteger:=dsDomicilios.DataSet.FieldByName('CodigoCalle').AsInteger;
                FieldByName('Numero').AsString:=dsDomicilios.DataSet.FieldByName('Numero').AsString;
                FieldByName('Piso').AsInteger:=dsDomicilios.DataSet.FieldByName('Piso').AsInteger;
                FieldByName('Dpto').AsString:=dsDomicilios.DataSet.FieldByName('Dpto').AsString;
                FieldByName('CalleDesnormalizada').AsString:=dsDomicilios.DataSet.FieldByName('CalleDesnormalizada').AsString;
                FieldByName('Detalle').AsString:=dsDomicilios.DataSet.FieldByName('Detalle').AsString;
                FieldByName('CodigoPostal').AsString:=dsDomicilios.DataSet.FieldByName('CodigoPostal').AsString;
                FieldByName('CalleRelacionadaUno').AsInteger:=dsDomicilios.DataSet.FieldByName('CalleRelacionadaUno').AsInteger;
                FieldByName('NumeroCalleRelacionadaUno').AsInteger:=dsDomicilios.DataSet.FieldByName('NumeroCalleRelacionadaUno').AsInteger;
                FieldByName('CalleRelacionadaDos').AsInteger:=dsDomicilios.DataSet.FieldByName('CalleRelacionadaDos').AsInteger;
                FieldByName('NumeroCalleRelacionadaDos').AsInteger:=dsDomicilios.DataSet.FieldByName('NumeroCalleRelacionadaDos').AsInteger;
                FieldByName('Normalizado').AsInteger:=dsDomicilios.DataSet.FieldByName('Normalizado').AsInteger;
                FieldByName('DomicilioEntrega').AsBoolean:=dsDomicilios.DataSet.FieldByName('DomicilioEntrega').AsBoolean;
                FieldByName('DireccionCompleta').AsString:=dsDomicilios.DataSet.FieldByName('DireccionCompleta').AsString;
                FieldByName('UbicacionGeografica').AsString:=dsDomicilios.DataSet.FieldByName('UbicacionGeografica').AsString;
                Post;
            end;
			dsDomicilios.DataSet.Delete;
            dblDomicilios.SetFocus;
		Except
			On E: Exception do begin
				dsDomicilios.DataSet.Cancel;
                cdsDomiciliosBorrados.Cancel;
				MsgBoxErr( Format(MSG_ERROR_ELIMINAR,[FLD_DOMICILIO]), e.message, Format(MSG_CAPTION_ELIMINAR,[FLD_DOMICILIO]), MB_ICONSTOP);
                dblDomicilios.SetFocus;
			end;
		end;
	end;
	Screen.Cursor   := crDefault;
end;


{******************************** Function Header ******************************
Function Name: TFrameListDomicilios.ControlarDomicilioEntrega
Author       : dcalani
Date Created : 07/04/2004
Description  : Controla que no exista dos domicilios de entregas.
Parameters   : DomiciolioActual: Integer
Return Value : Boolean
*******************************************************************************}
(*function TFrameListDomicilios.ControlarDomicilioEntrega(
  Valor:Boolean;DomiciolioActual: Integer): Boolean;
var
    pos:TBookmark;
begin
    result:=Valor;
    if not(valor) then exit;

    with cdsDomicilios do begin
        pos:=GetBookmark;
        first;
        while not(eof) do begin
            if (RecNo<>DomiciolioActual) and (FieldByName('DomicilioEntrega').AsBoolean) then begin
                if MessageDlg( MSG_QUESTION_DOMICILIO_RELACIONADO, mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin
                    result:=True;
                    try
                        edit;
                        FieldByName('DomicilioEntrega').Value:=False;
                        post;
                    except
                        result:=False;
                    end;
                end else result := False;
                last;
            end;
            next;
        end;
        GotoBookmark(pos);
    end;
end;*)

end.
