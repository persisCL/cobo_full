{-------------------------------------------------------------------------------
 File Name: MenuesContextuales.pas
 Author:
 Date Created:
 Language: ES-AR
 Description: Menues Contextuales

Revision 1:
    Author : ggomez
    Date : 30/06/2006
    Description : Modifiqu� el m�todo MostrarMenuContextualCuenta para que
        reciba como par�metro el Form desde el cual se llama al men� contextual.

Revision 2:
    Author : ggomez
    Date : 05/07/2006
    Description : En el menu contextual TMenuCuentas, agregu� la opci�n para
        mostrar el hist�rico de pedidos de env�o de action list.

Revision 3:
    Author : mpiazza
    Date : 27/01/2009
    Description : En el menu contextual de ir a convenio, si el formulario
    esta en memoria lo cierro y lo vuelvo a crear con los nuevos datos
    solicitados perdiendo la seleccion anterior

Revision 4:
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura

    Date: 03/10/2013
    Authos: mcabello
    Description: Se agrega order by para el men� de reclamo.
    Firma: SS_1136_MCA_20131003

-------------------------------------------------------------------------------}
unit MenuesContextuales;

interface

Uses
    //menues contextuales
    DMConnection,
    Util,
    UtilDB,
    UtilProc,
    OrdenesServicio,
    FrmReclamoGeneral,
    FrmReclamoFactura,
    FrmReclamoCuenta,
    FrmReclamoFaltaVehiculo,
    FrmReclamoTransito,
    FrmReclamoTransitoDesconoceInfraccion,
    FrmReclamoEstacionamiento,
    ImagenesTransitos,
    Navigator,
    FrmTransitosReclamados,                  //Informo los transitos que ya fueron reclamados
    FrmEstacionamientosReclamados,                  //Informo los estacionamientos que ya fueron reclamados
    //general
    Windows, SysUtils, Forms, Controls, Menus, AdoDB, Classes,
    FrmAnulacionRecibos,
    frmCambiarEstadoTag,
    PeaProcs,
    FrmHistoricoListTag,
    StrUtils;

    function MostrarMenuContextualTransito(Pos: TPoint; NumCorrCA: Int64): Boolean;
    function MostrarMenuContextualMultiplesTransitos(Pos: TPoint; ListaTransitos:tstringList): Boolean;
    function MostrarMenuContextualMultiplesTransitosNoCliente(Pos: TPoint; ListaTransitos:tstringList): Boolean;
    function MostrarMenuContextualComprobante(Pos: TPoint; TipoComprobante: AnsiString; NumeroComprobante: Int64): Boolean;
    function MostrarMenuContextualOrdenServicio(Pos: TPoint; CodigoOrdenServicio, PuntoEntrega, PuntoVenta: Integer): Boolean;
    function MostrarMenuContextualCuenta(Pos: TPoint; CodigoConvenio, IndiceVehiculo: Integer;
        Pantalla: TForm; Etiqueta: AnsiString): Boolean;
    function GenerarMenuReclamosGenerales(MenuPadre: TMenuItem; CodigoConvenio: Integer = 0): Boolean;
    function MostrarMenuContextualPago(Pos: TPoint; NumeroRecibo: Int64; spPagos : TADOStoredProc): Boolean;

    function MostrarMenuContextualEstacionamiento(Pos: TPoint; NumCorrEstacionamiento: Int64): Boolean;
    function MostrarMenuContextualMultiplesEstacionamientos(Pos: TPoint; ListaEstacionamientos:TStringList): Boolean;
    //function MostrarMenuContextualMultiplesEstacionamientosNoCliente(Pos: TPoint; ListaEstacionamientos:TStringList): Boolean;

implementation

uses
	FrmInicioConsultaConvenio, FrmIncluirTransitosInfractoresAlReclamo, DB;

Type
    TMenuHandler = class(TObject)
        FMenu: TPopupMenu;
        procedure DoMenuPopup(Sender: TObject); virtual; abstract;
        procedure CrearSeparador;
        constructor Create; virtual;
        destructor Destroy; override;
    end;

    TMenuTransitos = class(TMenuHandler)
        FNumCorrCA: Int64;
        FsNumCorrCA: String;
        FFechaHora: TDateTime;
        //
        FListaTransitos: TStringList; //Lista de Transitos Seleccionados
        //
        FVerImagen: TMenuItem;
        FReclamos: TMenuItem;
        FReclamoTransito: TMenuItem;
        //
        procedure DoMenuPopup(Sender: TObject); override;
        procedure DoVerImagen(Sender: TObject);
        procedure DoReclamoTransito(Sender: TObject);
        //
        procedure MostrarMenu(Pos: TPoint; NumCorrCA: Int64);
        procedure MostrarMenuMultiplesTransitos(Pos: TPoint; ListaTransitos:TstringList);

        constructor Create; override;
    end;

    TMenuTransitosNoCliente = class(TMenuHandler)
       FNumCorrCA: Int64;
       //
       FReclamos: TMenuItem;
       //
       FListaTransitos: TstringList; //Lista de Transitos Seleccionados
       //
       FReclamoTransito: TMenuItem;
       FReclamoTransitoDesconoceInfraccion: TMenuItem;
       //
       procedure DoReclamoTransitoNoCliente(Sender: TObject);
       procedure DoReclamoTransitoDesconoceInfraccion(Sender: TObject);
       procedure DoMenuPopup(Sender: TObject); override;
       procedure MostrarMenuMultiplesTransitosNoCliente(Pos: TPoint; ListaTransitos:TstringList);
       constructor Create; override; //creo el objeto
    end;

    TMenuEstacionamientos = class(TMenuHandler)
        FNumCorrEstacionamiento: Int64;
        FsNumCorrEstacionamiento: String;
        FFechaHora: TDateTime;
        //
        FListaEstacionamientos: TStringList; //Lista de Estacionamientos Seleccionados
        //
        //FVerImagen: TMenuItem;
        FReclamos: TMenuItem;
        FReclamoEstacionamiento: TMenuItem;
        //
        procedure DoMenuPopup(Sender: TObject); override;
        //procedure DoVerImagen(Sender: TObject);
        procedure DoReclamoEstacionamiento(Sender: TObject);
        //
        procedure MostrarMenu(Pos: TPoint; NumCorrEstacionamiento: Int64);
        procedure MostrarMenuMultiplesEstacionamientos(Pos: TPoint; ListaEstacionamientos:TstringList);

        constructor Create; override;
    end;

    //TMenuEstacionamientosNoCliente = class(TMenuHandler)
    //   FNumCorrEstacionamiento: Int64;
    //   //
    //   FReclamos: TMenuItem;
    //   //
    //   FListaEstacionamientos: TstringList; //Lista de Estacionamientos Seleccionados
    //   //
    //   FReclamoEstacionamiento: TMenuItem;
    //   //FReclamoEstacionamientoDesconoceInfraccion: TMenuItem;
    //   //
    //   procedure DoReclamoEstacionamientoNoCliente(Sender: TObject);
    //   //procedure DoReclamoEstacionamientoDesconoceInfraccion(Sender: TObject);
    //   procedure DoMenuPopup(Sender: TObject); override;
    //   procedure MostrarMenuMultiplesEstacionamientosNoCliente(Pos: TPoint; ListaEstacionamientos:TstringList);
    //   constructor Create; override; //creo el objeto
    //end;


    //Menu Contextual Reclamo Factura
    TMenuComprobantes = class(TMenuHandler)
        FTipoComprobante: String;
        FNumeroComprobante: Int64;
        //
        FReclamos: TMenuItem;
        FReclamoNoRecibido: TMenuItem;
        FReclamoGeneral: TMenuItem;
        FReclamoPagado: TMenuItem;
        FReclamoDomicilioMal: TMenuItem;
        FReclamoContenido: TMenuItem;
        FReclamoNKLlegaTarde: TMenuItem;
        FReclamoProblemaParaPagarNK: TMenuItem;
        //
        procedure DoMenuPopup(Sender: TObject); override;
        procedure DoReclamoGeneral(Sender: TObject);
        procedure DoNoRecibido(Sender: TObject);
        procedure DoDomicilioIncorrecto(Sender: TObject);
        procedure DoReclamoContenido(Sender: TObject);
        procedure DoYaPagado(Sender: TObject);
        procedure DoReclamoNKLlegaTarde(Sender: TObject);
        procedure DoReclamoProblemaParaPagarNK(Sender: TObject);
        //
        procedure MostrarMenu(Pos: TPoint; TipoComprobante: AnsiString; NumeroComprobante: Int64);
        constructor Create; override;
    end;

    TMenuCuentas = class(TMenuHandler)
        FCodigoConvenio: Integer;
        FIndiceVehiculo: Integer;
        // Para mantener la pantalla desde donde se llama al menu.
        FPantalla: TForm;
        // Para almacenar la etiqueta con la que se llamar� al form de Hist�rico
        // de Pedidos de Action List.
        FEtiqueta: AnsiString;
        FReclamos: TMenuItem;
        FFaltaVehiculo: TMenuItem;
        FDesconocePatente: TMenuItem;
        FMalFuncionamientoTelevia: TMenuItem;
        FTeleviaInhabilitado: TMenuItem;
        FCambiarActionList : TMenuItem;
        FHistoricoPedidoActionList: TMenuItem;
        //
        procedure DoMenuPopup(Sender: TObject); override;
        procedure DoDesconocePatente(Sender: TObject);
        procedure DoFaltaVehiculo(Sender: TObject);            //creo un reclamo por falta de vehiculo en un convenio
        procedure DoMalFuncionamientoTelevia(Sender: TObject);
        procedure DoTeleviaInhabilitado(Sender: TObject);
        procedure DoCambiarActionList(Sender: TObject);
        procedure DoMostrarHistoricoPedidosActionList(Sender: TObject);
        //
        procedure MostrarMenu(Pos: TPoint; CodigoConvenio, IndiceVehiculo: Integer;
            Pantalla: TForm = Nil; Etiqueta: AnsiString = '');
        constructor Create; override;
    end;

    TMenuOrdenesServicio = class(TMenuHandler)
        FCodigoConvenio: Integer;
        FCodigoOrdenServicio: Integer;
        FPuntoEntrega: Integer;
        FPuntoVenta: Integer;
        //
        //FReclamos: TMenuItem;
        FEditar: TMenuItem;
        FBloquear: TMenuItem;
        FSolucionar: TMenuItem;
        FDesbloquear: TMenuItem;
        FIrAlConvenio: TMenuItem;
        //
        procedure DoMenuPopup(Sender: TObject); override;
        procedure DoEditar(Sender: TObject);
        procedure DoBloquear(Sender: TObject);
        procedure DoDesbloquear(Sender: TObject);
        procedure DoSolucionar(Sender: TObject);
        procedure DoIrAlConvenio(Sender: TObject);
        //
        procedure MostrarMenu(Pos: TPoint; CodigoOrdenServicio, PuntoEntrega, PuntoVenta: Integer);
        constructor Create; override;
    end;

    TMenuReclamosGenerales = class(TObject)
        procedure DoReclamoGeneral(Sender: TObject);
    end;

    //Menu Contextual Pagos
    TMenuPagos = class(TMenuHandler)
        FNumeroRecibo: Int64;
    	FspPagos : TADOStoredProc;
        FAnularRecibo: TMenuItem;

        procedure DoMenuPopup(Sender: TObject); override;
        procedure DoAnularRecibo(Sender: TObject);
        //
        procedure MostrarMenu(Pos: TPoint; NumeroRecibo: Int64; spPagos : TADOStoredProc);
        constructor Create; override;
    end;

Var
    MenuCuentas             : TMenuCuentas = nil;
    MenuTransitos           : TMenuTransitos = nil;
    MenuTransitosNoCliente  : TMenuTransitosNoCliente = nil;
    MenuComprobantes        : TMenuComprobantes = nil;
    MenuOrdenesServicio     : TMenuOrdenesServicio = nil;
    MenuReclamosGenerales   : TMenuReclamosGenerales = nil;
    MenuPagos               : TMenuPagos = nil;
    MenuEstacionamientos    : TMenuEstacionamientos = nil;
    //MenuEstacionamientosNoCliente  : TMenuEstacionamientosNoCliente = nil;

function MostrarMenuContextualTransito(Pos: TPoint; NumCorrCA: Int64): Boolean;
begin
    MenuTransitos.MostrarMenu(Pos, NumCorrCA);
    Result := True;
end;


{-----------------------------------------------------------------------------
  Function Name: MostrarMenuContextualMultiplesTransitos
  Author:    lgisuk
  Date Created: 14/04/2005
  Description:
  Parameters: Pos: TPoint; ListaTransitos:tstringList
  Return Value: Boolean
-----------------------------------------------------------------------------}
function MostrarMenuContextualMultiplesTransitos(Pos: TPoint; ListaTransitos:tstringList): Boolean;
begin
    //Habilito la opci�n solo si tiene permisos
    MenuTransitos.FReclamoTransito.Enabled := ExisteAcceso('REALIZAR_RECLAMO_TRANSITOS');
    //Muestro el menu
    MenuTransitos.MostrarMenuMultiplesTransitos(Pos, ListaTransitos);
    Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: MostrarMenuContextualMultiplesTransitosNoCliente
  Author:    lgisuk
  Date Created: 14/04/2005
  Description: No Cliente
  Parameters: Pos: TPoint; ListaTransitos:tstringList
  Return Value: Boolean
-----------------------------------------------------------------------------}
function MostrarMenuContextualMultiplesTransitosNoCliente(Pos: TPoint; ListaTransitos:tstringList): Boolean;
begin
    //Habilito la opci�n solo si tiene permisos
    MenuTransitosNoCliente.FReclamoTransito.Enabled := ExisteAcceso('REALIZAR_RECLAMO_TRANSITOS');
    //Muestro el menu
    MenuTransitosNoCliente.MostrarMenuMultiplesTransitosNoCliente(Pos, ListaTransitos);
    Result := True;
end;

function MostrarMenuContextualComprobante(Pos: TPoint; TipoComprobante: AnsiString; NumeroComprobante: Int64): Boolean;
begin
    MenuComprobantes.MostrarMenu(Pos, TipoComprobante, NumeroComprobante);
    Result := True;
end;

function MostrarMenuContextualOrdenServicio(Pos: TPoint; CodigoOrdenServicio, PuntoEntrega, PuntoVenta: Integer): Boolean;
begin
    MenuOrdenesServicio.MostrarMenu(Pos, CodigoOrdenServicio, PuntoEntrega, PuntoVenta);
    Result := True;
end;

function MostrarMenuContextualCuenta(Pos: TPoint; CodigoConvenio,
    IndiceVehiculo: Integer; Pantalla: TForm; Etiqueta: AnsiString): Boolean;
begin
    MenuCuentas.MostrarMenu(Pos, CodigoConvenio, IndiceVehiculo, Pantalla, Etiqueta);
    Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: GenerarMenuReclamosGenerales
  Author:
  Date Created:  /  /
  Description:
  Parameters: MenuPadre: TMenuItem; CodigoConvenio: Integer = 0
  Return Value: Boolean

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function GenerarMenuReclamosGenerales(MenuPadre: TMenuItem; CodigoConvenio: Integer = 0): Boolean;
Var
    Qry: TAdoQuery;
    Item: TMenuItem;
begin
    Qry := TADOQuery.Create(nil);
    Qry.Connection := DMConnections.BaseCAC;
    //Qry.SQL.Text := 'SELECT TipoOrdenServicio, Descripcion FROM TiposOrdenServicio  WITH (NOLOCK) WHERE EsGeneral = 1';                    //SS_1136_MCA_20131003
    Qry.SQL.Text := 'SELECT TipoOrdenServicio, Descripcion FROM TiposOrdenServicio  WITH (NOLOCK) WHERE EsGeneral = 1 ORDER BY Orden'; //SS_1136_MCA_20131003
    while MenuPadre.Count > 0 do MenuPadre.Items[0].Free;
    MenuPadre.Tag := CodigoConvenio;
    try
        Qry.Open;
        while not Qry.Eof do begin
            Item := TMenuItem.Create(MenuPadre);
            Item.Caption := Qry['Descripcion'];
            Item.Tag := Qry['TipoOrdenServicio'];
            Item.OnClick := MenuReclamosGenerales.DoReclamoGeneral;
            MenuPadre.Add(Item);
            Qry.Next;
        end;
    finally
        Qry.Free;
    end;
    Result := True;
end;

{******************************** Function Header ******************************
Function Name: MostrarMenuContextualPago
Author : ggomez
Date Created : 14/07/2005
Description : Muestra el menu contextual para los pagos.
Parameters : Pos: TPoint; NumeroRecibo: Int64
Return Value : Boolean
*******************************************************************************}
function MostrarMenuContextualPago(Pos: TPoint; NumeroRecibo: Int64; spPagos : TADOStoredProc): Boolean;
begin
	// Le pasa el SP desde donde se llama para poder refrescar los datos
    MenuPagos.MostrarMenu(Pos, NumeroRecibo, spPagos);
    Result := True;
end;

{ TMenuTransitos }

{-----------------------------------------------------------------------------
  Function Name: Create
  Author:    lgisuk
  Date Created: 29/03/2005
  Description: Haciendo Click en el item llama al form para realizar un reclamo
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
constructor TMenuTransitos.Create;
begin
    inherited;
    //
    FReclamos := TMenuItem.Create(FMenu);
    FReclamos.Caption := '&Reclamos';
    FMenu.Items.Add(FReclamos);
    //
    FReclamoTransito := TMenuItem.Create(FMenu);
    FReclamoTransito.Caption := '&Reclamo por tr�nsitos no aceptados';
    FReclamoTransito.Tag := 207;
    FReclamoTransito.OnClick := MenuTransitos.doReclamoTransito;   // abro la ventana para editar un reclamo por transito   MenuReclamosGenerales.DoReclamoGeneral; //
    FReclamos.Add(FReclamoTransito);
    //
    CrearSeparador;
    //
    FVerImagen := TMenuItem.Create(FMenu);
    FVerImagen.Caption := '&Ver imagen';
    FVerImagen.OnClick := DoVerImagen;
    FMenu.Items.Add(FVerImagen);
end;


{-----------------------------------------------------------------------------
  Function Name: Create
  Author:    lgisuk
  Date Created: 29/03/2005
  Description: Haciendo Click en el item llama al form para realizar un reclamo
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
constructor TMenuTransitosNoCliente.Create;
begin
    inherited;
    //
    FReclamos := TMenuItem.Create(FMenu);
    FReclamos.Caption := '&Reclamos';
    FMenu.Items.Add(FReclamos);
    //
    FReclamoTransito := TMenuItem.Create(FMenu);
    FReclamoTransito.Caption := '&Reclamo por tr�nsitos no reconocidos'; //no aceptados
    FReclamoTransito.Tag := 207;
    FReclamoTransito.OnClick := MenuTransitosNoCliente.doReclamoTransitoNoCliente;   // abro la ventana para editar un reclamo por transito   MenuReclamosGenerales.DoReclamoGeneral; //
    FReclamos.Add(FReclamoTransito);
    //
    FReclamoTransitoDesconoceInfraccion := TMenuItem.Create(FMenu);
    FReclamoTransitoDesconoceInfraccion.Caption := '&Reclamo por tr�nsitos Desconoce Infraccion'; //con justificacion
    FReclamoTransitoDesconoceInfraccion.Tag := 201;
    FReclamoTransitoDesconoceInfraccion.OnClick := MenuTransitosNoCliente.doReclamoTransitoDesconoceInfraccion;   // abro la ventana para editar un reclamo por transito   MenuReclamosGenerales.DoReclamoGeneral; //
    FReclamos.Add(FReclamoTransitoDesconoceInfraccion);

end;
{-----------------------------------------------------------------------------
  Function Name: DoMenuPopup
  Author:
  Date Created:  /  /
  Description:
  Parameters: Sender: TObject
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure TMenuTransitos.DoMenuPopup(Sender: TObject);
Var
    Qry: TAdoQuery;
begin
    Qry := TADOQuery.Create(nil);
    Qry.Connection := DMConnections.BaseCAC;
    Qry.SQL.Text := Format('SELECT FechaHora, RegistrationAccessibility FROM Transitos WITH (NOLOCK) WHERE NumCorrCA = %d', [FNumCorrCA]);
    try
        Qry.Open;
        FVerImagen.Visible := Qry.FieldByName('RegistrationAccessibility').AsInteger <> 0;
        FFechaHora := Qry.FieldByName('FechaHora').AsDateTime;
    finally
        Qry.Free;
    end;
end;

{-----------------------------------------------------------------------------
  Procedure: TMenuTransitosNoCliente.DoMenuPopup
  Author:    lgisuk
  Date:      05-May-2005
  Arguments: Sender: TObject
  Result:    None

  Revision 1:
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure TMenuTransitosNoCliente.DoMenuPopup(Sender: TObject);
Var
    Qry: TAdoQuery;
begin
    Qry := TADOQuery.Create(nil);
    Qry.Connection := DMConnections.BaseCAC;
    Qry.SQL.Text := Format('SELECT FechaHora, RegistrationAccessibility FROM Transitos WITH (NOLOCK) WHERE NumCorrCA = %d', [FNumCorrCA]);
    try
        Qry.Open;
    finally
        Qry.Free;
    end;
end;

procedure TMenuTransitos.DoVerImagen(Sender: TObject);
begin
    MostrarVentanaImagen(Self, FNumCorrCA, FFechaHora);
end;

procedure TMenuTransitos.MostrarMenu(Pos: TPoint; NumCorrCA: Int64);
begin
    FNumCorrCA := NumCorrCA;
    FsNumCorrCA:= IntToStr(NumCorrCA);
    FMenu.Popup(Pos.X, Pos.Y);
end;

{-----------------------------------------------------------------------------
  Function Name: MostrarMenuMultiplesTransitos
  Author:    lgisuk
  Date Created: 14/04/2005
  Description:
  Parameters: Pos: TPoint; ListaTransitos:TstringList
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMenuTransitos.MostrarMenuMultiplesTransitos(Pos: TPoint; ListaTransitos:TstringList);
begin
    FListatransitos := ListaTransitos;
    FMenu.Popup(Pos.X, Pos.Y);
end;

{-----------------------------------------------------------------------------
  Function Name: MostrarMenuMultiplesTransitos
  Author:    lgisuk
  Date Created: 14/04/2005
  Description:
  Parameters: Pos: TPoint; ListaTransitos:TstringList
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMenuTransitosNoCliente.MostrarMenuMultiplesTransitosNoCliente(Pos: TPoint; ListaTransitos:TstringList);
begin
    FListatransitos := ListaTransitos;
    FMenu.Popup(Pos.X, Pos.Y);
end;

{ TMenuComprobantes }

constructor TMenuComprobantes.Create;
begin
    inherited;
    //
    FReclamos := TMenuItem.Create(FMenu);
    FReclamos.Caption := '&Reclamos';
    FMenu.Items.Add(FReclamos);
    //
    FReclamoNoRecibido := TMenuItem.Create(FMenu);
    FReclamoNoRecibido.Caption := '&No se recibi� este comprobante';
    FReclamoNoRecibido.OnClick := DoNoRecibido;
    FReclamos.Add(FReclamoNoRecibido);
    //
    FReclamoDomicilioMal := TMenuItem.Create(FMenu);
    FReclamoDomicilioMal.Caption := 'Comprobante recibido en &domicilio incorrecto';
    FReclamoDomicilioMal.OnClick := DoDomicilioIncorrecto;
    FReclamos.Add(FReclamoDomicilioMal);
    //
    FReclamoPagado := TMenuItem.Create(FMenu);
    FReclamoPagado.Caption := '&El comprobante se pag�, pero figura impago';
    FReclamoPagado.OnClick := DoYaPagado;
    FReclamos.Add(FReclamoPagado);
    //
    FReclamoContenido := TMenuItem.Create(FMenu);
    FReclamoContenido.Caption := 'Reclamo sobre el &contenido de este comprobante';
    FReclamoContenido.OnClick := DoReclamoContenido;
    FReclamos.Add(FReclamoContenido);
    //
    FReclamoNKLlegaTarde := TMenuItem.Create(FMenu);
    FReclamoNKLlegaTarde.Caption := '&Recepci�n Tard�a de Comprobante';
    FReclamoNKLlegaTarde.OnClick := DoReclamoNKLlegaTarde;
    FReclamos.Add(FReclamoNKLlegaTarde);
    //
    FReclamoProblemaParaPagarNK := TMenuItem.Create(FMenu);
    FReclamoProblemaParaPagarNK.Caption := '&Problemas para pagar el Comprobante';
    FReclamoProblemaParaPagarNK.OnClick := DoReclamoProblemaParaPagarNK;
    FReclamos.Add(FReclamoProblemaParaPagarNK);
    //
    FReclamoGeneral := TMenuItem.Create(FMenu);
    FReclamoGeneral.Caption := '&Otros reclamos &generales sobre este comprobante';
    FReclamoGeneral.OnClick := DoReclamoGeneral;
    FReclamos.Add(FReclamoGeneral);
end;

{-----------------------------------------------------------------------------
  Function Name: DoDomicilioIncorrecto
  Author:    lgisuk
  Date Created: 07/06/2005
  Description: Domicilio de entrega de comprobantes incorrecto
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMenuComprobantes.DoDomicilioIncorrecto(Sender: TObject);
Var
    F: TFormReclamoFactura;
begin
    Application.CreateForm(TFormReclamoFactura, f);
    if f.Inicializar(206, FTipoComprobante, FNumeroComprobante) then f.show;
end;

procedure TMenuComprobantes.DoMenuPopup(Sender: TObject);
begin

end;

{-----------------------------------------------------------------------------
  Function Name: DoNoRecibido
  Author:    lgisuk
  Date Created: 07/06/2005
  Description: Comprobantes no recibidos (facturas, boletas, detalles)
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMenuComprobantes.DoNoRecibido(Sender: TObject);
Var
    F: TFormReclamoFactura;
begin
    Application.CreateForm(TFormReclamoFactura, f);
    if f.Inicializar(205, FTipoComprobante, FNumeroComprobante) then f.show;
end;

{-----------------------------------------------------------------------------
  Function Name: DoReclamoContenido
  Author:    lgisuk
  Date Created: 07/06/2005
  Description: Cobros efectuados (reposici�n de Telev�a, intereses, pago incorrecto, etc.)
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMenuComprobantes.DoReclamoContenido(Sender: TObject);
Var
    F: TFormReclamoFactura;
begin
    Application.CreateForm(TFormReclamoFactura, f);
    if f.Inicializar(204, FTipoComprobante, FNumeroComprobante) then f.show;

end;

{-----------------------------------------------------------------------------
  Function Name: DoReclamoGeneral
  Author:    lgisuk
  Date Created: 07/06/2005
  Description: Reclamo general sobre un comprobante
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMenuComprobantes.DoReclamoGeneral(Sender: TObject);
Var
    F: TFormReclamoFactura;
begin
    Application.CreateForm(TFormReclamoFactura, f);
    if f.Inicializar(214, FTipoComprobante, FNumeroComprobante) then f.show;
end;

{-----------------------------------------------------------------------------
  Function Name: DoReclamoNKLlegaTarde
  Author:    lgisuk
  Date Created: 07/06/2005
  Description:  El comprobante llega tarde
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMenuComprobantes.DoReclamoNKLlegaTarde(Sender: TObject);
Var
    F: TFormReclamoFactura;
begin
    Application.CreateForm(TFormReclamoFactura, f);
    if f.Inicializar(215, FTipoComprobante, FNumeroComprobante) then f.show;
end;

{-----------------------------------------------------------------------------
  Function Name: DoReclamoNKLlegaTarde
  Author:    lgisuk
  Date Created: 07/06/2005
  Description:   Problema para pagar el comprobante
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMenuComprobantes.DoReclamoProblemaParaPagarNK(Sender: TObject);
Var
    F: TFormReclamoFactura;
begin
    Application.CreateForm(TFormReclamoFactura, f);
    if f.Inicializar(216, FTipoComprobante, FNumeroComprobante) then f.show;

end;


{-----------------------------------------------------------------------------
  Function Name: DoYaPagado
  Author:    lgisuk
  Date Created: 07/06/2005
  Description: Pagos (no aplicado, doble pago, pago incorrecto de su cuenta, etc.)
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMenuComprobantes.DoYaPagado(Sender: TObject);
Var
    F: TFormReclamoFactura;
begin
    Application.CreateForm(TFormReclamoFactura, f);
    if f.Inicializar(208, FTipoComprobante, FNumeroComprobante) then f.show;

end;

procedure TMenuComprobantes.MostrarMenu(Pos: TPoint; TipoComprobante: AnsiString; NumeroComprobante: Int64);
begin
    FTipoComprobante := TipoComprobante;
    FNumeroComprobante := NumeroComprobante;
    FMenu.Popup(Pos.X, Pos.Y);
end;

{ TMenuHandler }

procedure TMenuHandler.CrearSeparador;
Var
    Item: TMenuItem;
begin
    Item := TMenuItem.Create(FMenu);
    Item.Caption := '-';
    FMenu.Items.Add(Item);
end;

constructor TMenuHandler.Create;
begin
    inherited;
    FMenu := TPopupMenu.Create(nil);
    FMenu.OnPopup := DoMenuPopup;
end;

destructor TMenuHandler.Destroy;
begin
    FreeAndNil(FMenu);
    inherited;
end;

{ TMenuReclamosGenerales }

procedure TMenuReclamosGenerales.DoReclamoGeneral(Sender: TObject);
Var
    f: TFormReclamoGeneral;
begin

    Application.CreateForm(TFormReclamoGeneral, f);
    if f.Inicializar(TMenuItem(Sender).Tag, TMenuItem(Sender).Parent.Tag) then f.show;

end;

{-----------------------------------------------------------------------------
  Function Name: DoReclamoTransito
  Author:    lgisuk
  Date Created: 06/04/2005
  Description:
  Parameters: Sender: TObject
  Return Value: None

  Revision 1:
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura
-----------------------------------------------------------------------------}
procedure TMenuTransitos.DoReclamoTransito(Sender: TObject);

            //Obtengo la Enumeraci�n
            function ObtenerEnumeracion(Lista: TStringList): String;
            var
                Enum: AnsiString;
                i: Integer;
            begin
                Enum := '';
                for i:= 0 to Lista.Count - 1 do begin
                    //Enum := Enum + Lista.Strings[i] + ',';
                    Enum := Enum + ParseParamByNumber(Lista.Strings[i],1,'=') + ',';
                end;
                Delete(Enum, Length(Enum), 1);
                Result := Enum;
            end;

            //informo los transitos reclamados
            Procedure InformarTransitosReclamados(CodigoConvenio: Integer; ListaTransitos:TStringList);
            Var
                F:  TFormTransitosReclamados;
            begin
              	if FindFormOrCreate( TFormTransitosReclamados, F) then
                		F.ShowModal
               	else begin
               		if not F.Inicializar(CodigoConvenio, ListaTransitos) then f.Release;
              	end;
            end;

resourcestring
    MSG_ERROR = 'Debe seleccionar al menos un transito';
Var
    F: TFormReclamoTransito;
    CodigoConvenio: Integer;
    NumCorrCA: String;
begin
    //controlo que hallan seleccionado al menos un transito
    if MenuTransitos.FListaTransitos.Count = 0 then begin
        Msgbox(MSG_ERROR);
        Exit;
    end;

    //controlo que no hallan seleccionado transitos que ya fueron reclamados
    CodigoConvenio:= QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.ObtenerCodigoConvenioTransito ('+ MenuTransitos.FListaTransitos.Strings[0] + ')' );
    NumCorrCA := QueryGetValue(dmconnections.BaseCAC,'SELECT TOP 1 NumCorrCA FROM OrdenesServicio O WITH (NOLOCK) , OrdenesServicioTransito T  WITH (NOLOCK) WHERE O.CodigoOrdenServicio = T.CodigoOrdenServicio AND O.CodigoConvenio = ' +IntToStr(CodigoConvenio)+' and NumCorrCA in (' + ObtenerEnumeracion(MenuTransitos.FListaTransitos) + ') ORDER BY O.CodigoOrdenServicio');
    if NumCorrCA <> '' then begin
        InformarTransitosReclamados(CodigoConvenio,MenuTransitos.FListaTransitos);
        Exit;
    end;

    //creo el form de reclamo por transitos no aceptados
    Application.CreateForm(TFormReclamoTransito, F);
    if F.Inicializar(TMenuItem(Sender).Tag, TMenuItem(Sender).Parent.Tag, MenuTransitos.FListaTransitos) then F.Show;
end;


{-----------------------------------------------------------------------------
  Function Name: DoReclamoTransitoNoCliente
  Author:    lgisuk
  Date Created: 06/04/2005
  Description:
  Parameters: Sender: TObject
  Return Value: None

  Revision :
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure TMenuTransitosNoCliente.DoReclamoTransitoNoCliente(Sender: TObject);

            //Obtengo la Enumeraci�n
            function ObtenerEnumeracion(Lista: TStringList): String;
            var
                Enum: AnsiString;
                i: Integer;
            begin
                Enum := '';
                for i:= 0 to Lista.Count - 1 do begin
                    //Enum := Enum + Lista.Strings[i] + ',';
                    Enum := Enum + ParseParamByNumber(Lista.Strings[i],1,'=') + ',';
                end;
                Delete(Enum, Length(Enum), 1);
                Result := Enum;
            end;

            //informo los transitos reclamados
            Procedure InformarTransitosReclamados(CodigoConvenio: Integer; ListaTransitos:tstringlist);
            Var
	              F :  TFormTransitosReclamados;
            begin
                if FindFormOrCreate( TFormTransitosReclamados, F) then
                  F.Show
                else begin
                  if not F.Inicializar(CodigoConvenio, ListaTransitos) then F.Release;
                end;
            end;

Resourcestring
    MSG_ERROR = 'Debe seleccionar al menos un transito';
Var
    F: TFormReclamoTransito;
    NumCorrCA: String;
begin
    //controlo que hallan seleccionado al menos un transito
    if MenuTransitosNocliente.FListaTransitos.Text = '' then begin
        MsgBox(MSG_ERROR);
        Exit;
    end;

    //controlo que no hallan seleccionado transitos que ya fueron reclamados
    NumCorrCA := QueryGetValue(dmconnections.BaseCAC,'SELECT TOP 1 NumCorrCA FROM OrdenesServicio O WITH (NOLOCK) , OrdenesServicioTransito T  WITH (NOLOCK) WHERE O.CodigoOrdenServicio = T.CodigoOrdenServicio AND NumCorrCA IN (' + ObtenerEnumeracion(MenuTransitosNoCliente.FListaTransitos) + ')  ORDER BY O.CodigoOrdenServicio');
    if NumCorrCA <> '' then begin
        InformarTransitosReclamados(0, MenuTransitosNoCliente.FListaTransitos);
        Exit;
    end;

    //creo el form de reclamo por transitos no aceptados
    Application.CreateForm(TFormReclamoTransito, F);
    if F.Inicializar(TMenuItem(Sender).Tag, TMenuItem(Sender).Parent.Tag, MenuTransitosNoCliente.FlistaTransitos) then F.show;
end;

{-----------------------------------------------------------------------------
  Function Name: DoReclamoTransitoDesconoceInfraccion
  Author:    lgisuk
  Date Created: 06/04/2005
  Description: Abro el tipo de reclamo Desconoce Infracci�n
  Parameters: Sender: TObject
  Return Value: None

  Revision :
      Author : ggomez
      Date : 10/01/2006
      Description : Quit� la validaci�n de que los tr�nsitos sean Infractor
        Confirmado y la sugerencia de que hay m�s tr�nsitos que son Infractor
        Confirmado, de esta manera, s�lo se procesar�n los tr�nsitos que el
        operador seleccion�. El cambio se hizo por un pedido de CN.

-----------------------------------------------------------------------------}
procedure TMenuTransitosNoCliente.DoReclamoTransitoDesconoceInfraccion(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: ObtenerEnumeracion
      Author:    lgisuk
      Date Created: 27/06/2005
      Description:  Convierto la lista a un string separado por comas
      Parameters: Transitos:string
      Return Value: string
    -----------------------------------------------------------------------------}
     function ObtenerEnumeracion(Lista: TStringList): String;
     var
          Enum: AnsiString;
          i: Integer;
     begin
          Enum := '';
          for i:= 0 to Lista.Count - 1 do begin
              //Enum := Enum + Lista.Strings[i] + ',';
              Enum := Enum + ParseParamByNumber(Lista.Strings[i],1,'=') + ',';
          end;
          Delete(Enum, Length(Enum), 1);
          Result := Enum;
     end;

    {-----------------------------------------------------------------------------
      Function Name: ValidarInfractorConfirmado
      Author:    lgisuk
      Date Created: 24/06/2005
      Description: Verifico que todos los transitos incluidos sean todos del tipo
                   Infractor Confirmado
      Parameters: None
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function ValidarInfractorConfirmado(ListaTransitos:TstringList):boolean;
    Resourcestring
        MSG_FAIL  = 'No pudo validar infractor confirmado';
        STR_ERROR = 'Error';
    Var
        SP: TAdoStoredProc;
    begin
        SP := TADOStoredProc.Create(nil);
        SP.Connection := DMConnections.BaseCAC;
        SP.ProcedureName := 'ValidarInfractorConfirmado';
        try
            Result := False;
            try
                SP.Parameters.Refresh;
                SP.Parameters.ParamByName('@Enumeracion').Value :=ObtenerEnumeracion(ListaTransitos);
                SP.Parameters.ParamByName('@Confirmado').Value  :=0;
                SP.open;
                Result := (SP.Parameters.ParamByName('@Confirmado').Value = 1);
            except
                on e: exception do begin
                    MsgBox(MSG_FAIL + ' ' + e.Message, STR_ERROR, MB_ICONSTOP);
               end;
            end;
        finally
            SP.Free;
        end
    end;

    {-----------------------------------------------------------------------------
      Function Name: HayMasTransitosInfractorConfirmado
      Author:    lgisuk
      Date Created: 27/06/2005
      Description: verifico si hay mas transitos infractores para reclamar
      Parameters: ListaTransitos:TstringList
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function HayMasTransitosInfractorConfirmado(ListaTransitos:TstringList):boolean;
    Resourcestring
        MSG_FAIL  = 'No pudo obtener si hay mas transitos infractores a reclamar';
        STR_ERROR = 'Error';
    Var
        SP: TAdoStoredProc;
    begin
        SP := TADOStoredProc.Create(nil);
        SP.Connection := DMConnections.BaseCAC;
        SP.ProcedureName := 'ObtenerTransitosInfractoresAReclamar';
        try
            Result := False;
            try
                SP.Parameters.Refresh;
                SP.Parameters.ParamByName('@Enumeracion').Value := ObtenerEnumeracion(ListaTransitos);
                SP.open;
                Result := (sp.RecordCount > ListaTransitos.Count);
            except
                on E: Exception do begin
                    MsgBox(MSG_FAIL + ' ' + e.Message, STR_ERROR, MB_ICONSTOP);
               end;
            end;
        finally
            SP.Free;
        end
    end;

resourcestring
    MSG_TITLE = 'Sistema de Reclamos';
    MSG_ERROR = 'Debe seleccionar al menos un transito';
    MSG_ERROR_NO_ES_INFRACTOR_CONFIRMADO = 'Debe seleccionar solo transitos que sean del tipo Infractor Confirmado!';
Var
    ListaTransitosSel : TStringlist;
//    f:  TFormIncluirTransitosInfractoresAlReclamo;
    f1: TFormReclamoTransitoDesconoceInfraccion;
begin

    //Obtengo la lista de transitos seleccionados
    ListaTransitosSel:= MenuTransitosNoCliente.FListaTransitos;

    //Controlo que hallan seleccionado al menos un transito
    if ListaTransitosSel.Text = '' then begin
        MsgBox(MSG_ERROR, MSG_TITLE, 0);
        Exit;
    end;

    //Creo el form de reclamo deconoce infraccion
    Application.CreateForm(TFormReclamoTransitoDesconoceInfraccion, F1);
    if F1.Inicializar(TMenuItem(Sender).Tag, TMenuItem(Sender).Parent.Tag,ListaTransitosSel) then F1.show;


end;

{ TMenuOrdenesServicio }

constructor TMenuOrdenesServicio.Create;
begin
    inherited;
    //
    {FReclamos := TMenuItem.Create(FMenu);
    FReclamos.Caption := '&Reclamos';
    FMenu.Items.Add(FReclamos);
    //
    CrearSeparador;
    //} //no tiene ninguna accion asignada
    FBloquear := TMenuItem.Create(FMenu);
    FBloquear.Caption := '&Bloquear';
    FBloquear.OnClick := DoBloquear;
    FMenu.Items.Add(FBloquear);
    //
    FDesbloquear := TMenuItem.Create(FMenu);
    FDesbloquear.Caption := '&Desbloquear';
    FDesbloquear.OnClick := DoDesbloquear;
    FMenu.Items.Add(FDesbloquear);
    //
    CrearSeparador;
    //
    FIrAlConvenio := TMenuItem.Create(FMenu);
    FIrAlConvenio.Caption := '&Ir al convenio...';
    FIrAlConvenio.OnClick := DoIrAlConvenio;
    FMenu.Items.Add(FIrAlConvenio);
    //
    CrearSeparador;
    //
    FEditar := TMenuItem.Create(FMenu);
    FEditar.Caption := '&Editar...';
    FEditar.OnClick := DoEditar;
    FMenu.Items.Add(FEditar);
    //
    FSolucionar := TMenuItem.Create(FMenu);
    FSolucionar.Caption := '&Solucionar...';
    FSolucionar.OnClick := DoSolucionar;
    FMenu.Items.Add(FSolucionar);
end;

procedure TMenuOrdenesServicio.DoBloquear(Sender: TObject);
begin
    BloquearOrdenServicio(FCodigoOrdenServicio);
end;

procedure TMenuOrdenesServicio.DoDesbloquear(Sender: TObject);
begin
    DesbloquearOrdenServicio(FCodigoOrdenServicio);
end;

procedure TMenuOrdenesServicio.DoEditar(Sender: TObject);
begin
    EditarOrdenServicio(FCodigoOrdenServicio);
end;
{-----------------------------------------------------------------------------
  Function Name: DoIrAlConvenio
  Author:    ?????
  Date Created: ??/??/????
  Description: llama al formulario de convenio
  Parameters: Sender: TObject
  Return Value: N/A

Revision 1:
    Author : mpiazza
    Date : 27/01/2009
    Description : En el menu contextual de ir a convenio, si el formulario
    esta en memoria lo cierro y lo vuelvo a crear con los nuevos datos
    solicitados perdiendo la seleccion anterior

-----------------------------------------------------------------------------}
procedure TMenuOrdenesServicio.DoIrAlConvenio(Sender: TObject);
var
	f: TFormInicioConsultaConvenio;
begin

	if FindFormOrCreate(TFormInicioConsultaConvenio, f) then begin
      f.Close;
      f.Free;
      f := nil;
      FindFormOrCreate(TFormInicioConsultaConvenio, f);
	end;

  if f.Inicializar(FCodigoConvenio, FPuntoEntrega, FPuntoVenta) then begin
    f.Show;
    f.IrAConvenio(FCodigoConvenio);
  end
  else begin
      f.Release;
      Exit;
  end;


{
	if FindFormOrCreate(TFormInicioConsultaConvenio, f) then begin
		f.Show;
        f.IrAConvenio(FCodigoConvenio);
	end else begin
		if not f.Inicializar(FCodigoConvenio, FPuntoEntrega, FPuntoVenta) then begin
            f.Release;
            Exit;
        end;
        f.IrAConvenio(FCodigoConvenio);
	end;
  }
end;
{-----------------------------------------------------------------------------
  Function Name: DoMenuPopup
  Author:
  Date Created:  /  /
  Description:
  Parameters: Sender: TObject
  Return Value: N/A

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
procedure TMenuOrdenesServicio.DoMenuPopup(Sender: TObject);
Var
    Qry: TAdoQuery;
    Usuario: AnsiString;
begin
    Qry := TADOQuery.Create(nil);
    Qry.Connection := DMConnections.BaseCAC;
    Qry.SQL.Text := Format('SELECT TipoOrdenServicio, CodigoConvenio, Estado, Usuario FROM OrdenesServicio  WITH (NOLOCK) WHERE CodigoOrdenServicio = %d',
      [FCodigoOrdenServicio]);
    Qry.Open;
    Usuario := Trim(Qry.FieldByName('Usuario').AsString);
    FCodigoConvenio := Qry.FieldByName('CodigoConvenio').AsInteger;
    FEditar.Visible :=
      TienePermisoOrdenServicio(Qry['TipoOrdenServicio'])
      and ((Usuario = '') or (Usuario = UsuarioSistema));
    FBloquear.Visible := (Usuario = '');
    FIrAlConvenio.Visible := (FCodigoConvenio > 0);
    FDesbloquear.Visible := (Usuario = UsuarioSistema);
    FSolucionar.Visible := (Qry['Estado'] = 'P');
    Qry.Free;
end;

procedure TMenuOrdenesServicio.DoSolucionar(Sender: TObject);
begin
    SolucionarOrdenServicio(FCodigoOrdenServicio);
end;

procedure TMenuOrdenesServicio.MostrarMenu(Pos: TPoint; CodigoOrdenServicio, PuntoEntrega, PuntoVenta: Integer);
begin
    FCodigoOrdenServicio := CodigoOrdenServicio;
    FPuntoEntrega := PuntoEntrega;
    FPuntoVenta := PuntoVenta;
    FMenu.Popup(Pos.X, Pos.Y);
end;

{ TMenuCuentas }

constructor TMenuCuentas.Create;
begin
    inherited;
    //
    FReclamos := TMenuItem.Create(FMenu);
    FReclamos.Caption := '&Reclamos';
    FMenu.Items.Add(FReclamos);
    //
    FFaltaVehiculo := TMenuItem.Create(FMenu);                 //Creo el item del menu
    FFaltaVehiculo.Caption := '&Falta Vehiculo en un Convenio';//Le asigno un titulo
    FFaltaVehiculo.OnClick := DoFaltaVehiculo;                 //Tarea que ejecuta
    FReclamos.Add(FFaltaVehiculo);                             //Lo agrega dentro de reclamos
    //
    //
    FDesconocePatente := TMenuItem.Create(FMenu);
    FDesconocePatente.Caption := '&Se desconoce patente';
    FDesconocePatente.OnClick := DoDesconocePatente;
    FReclamos.Add(FDesconocePatente);
    //
    FMalFuncionamientoTelevia := TMenuItem.Create(FMenu);
    FMalFuncionamientoTelevia.Caption := '&Mal funcionamiento del telev�a';
    FMalFuncionamientoTelevia.OnClick := DoMalFuncionamientoTelevia;
    FReclamos.Add(FMalFuncionamientoTelevia);
    //
    FTeleviaInhabilitado := TMenuItem.Create(FMenu);
    FTeleviaInhabilitado.Caption := 'Reclamo sobre &telev�a inhabilitado';
    FTeleviaInhabilitado.OnClick := DoTeleviaInhabilitado;
    FReclamos.Add(FTeleviaInhabilitado);
    //
    FCambiarActionList := TMenuItem.Create(FMenu);
    FCambiarActionList.Caption := '&Cambiar Action List';
    FCambiarActionList.OnClick := DoCambiarActionList;
    FMenu.Items.Add(FCambiarActionList);

    FHistoricoPedidoActionList := TMenuItem.Create(FMenu);
    FHistoricoPedidoActionList.Caption := '&Hist�rico Pedidos Action List';
    FHistoricoPedidoActionList.OnClick := DoMostrarHistoricoPedidosActionList;
    FMenu.Items.Add(FHistoricoPedidoActionList);

end;
{******************************** Function Header ******************************
Function Name: DoCambiarActionList
Author : gcasais
Date Created : 26/06/2006
Description : Lanza el m�dulo de cambiar solicitudes de Action List al P�rtico
Parameters : Sender: TObject
Return Value : None

Revision 1:
    Author : ggomez
    Date : 30/06/2006
    Description : Si se presion� el bot�n Aceptar en el form de Cambio de Action
        List, se refresca la cartola.

*******************************************************************************}
procedure TMenuCuentas.DoCambiarActionList(Sender: TObject);
var
    f: TFormCambiarEstadoTag;
begin
    Application.CreateForm(TFormCambiarEstadoTag, f);
    if f.Inicializar(FCodigoConvenio, FIndiceVehiculo) then begin
        if f.ShowModal = mrOk then begin
            // Si Acept� los cambios y la pantalla desde la que se llam� el menu
            // es la Cartola, entonces Refrescar los Action Lists en la
            // Cartola.
            if FPantalla is TFormInicioConsultaConvenio then begin
                TFormInicioConsultaConvenio(FPantalla).RefrescarActionListCuenta(FCodigoConvenio, FIndiceVehiculo);
            end;
        end;
        f.Release;
    end else begin
    	f.Release;
    end;
end;

procedure TMenuCuentas.DoDesconocePatente(Sender: TObject);
Var
    f: TFormReclamoCuenta;
begin
    Application.CreateForm(TFormReclamoCuenta, f);
    if f.Inicializar(211, FCodigoConvenio, FIndiceVehiculo) then f.show;

end;

{-----------------------------------------------------------------------------
  Function Name: DoFaltaVehiculo
  Author:    lgisuk
  Date Created: 20/04/2005
  Description: creo un reclamo por falta de vehiculo en el convenio
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TMenuCuentas.DoFaltaVehiculo(Sender: TObject);
Var
    f: TFormReclamoFaltaVehiculo;
begin
    Application.CreateForm(TFormReclamoFaltaVehiculo, f);
    if f.Inicializar(212, FCodigoConvenio) then f.show;
end;

procedure TMenuCuentas.DoMalFuncionamientoTelevia(Sender: TObject);
Var
    f: TFormReclamoCuenta;
begin
    Application.CreateForm(TFormReclamoCuenta, f);
    if f.Inicializar(301, FCodigoConvenio, FIndiceVehiculo) then f.show;
end;

procedure TMenuCuentas.DoMenuPopup(Sender: TObject);
begin
  inherited;
end;

procedure TMenuCuentas.DoMostrarHistoricoPedidosActionList(Sender: TObject);
var
    f: TfrmHistoricoListTagForm;
begin
    Application.CreateForm(TfrmHistoricoListTagForm, f);
    if f.Inicializar(F.Caption, False, FEtiqueta) then begin
        f.ShowModal;
    end else begin
        f.Release;
    end;

end;

procedure TMenuCuentas.DoTeleviaInhabilitado(Sender: TObject);
Var
    f: TFormReclamoCuenta;
begin
    Application.CreateForm(TFormReclamoCuenta, f);
    if f.Inicializar(302, FCodigoConvenio, FIndiceVehiculo) then f.show;
end;

{******************************** Function Header ******************************
Function Name: MostrarMenu
Author :
Date Created :
Description :
Parameters : Pos: TPoint; CodigoConvenio, IndiceVehiculo: Integer
Return Value : None

Revision 1:
    Author : ggomez
    Date : 30/06/2006
    Description : Agregu� el par�metro Pantalla: TForm. Para tener el form que
        llam� al menu contextual.

*******************************************************************************}
procedure TMenuCuentas.MostrarMenu(Pos: TPoint; CodigoConvenio,
    IndiceVehiculo: Integer; Pantalla: TForm; Etiqueta: AnsiString);
begin
    FCodigoConvenio := CodigoConvenio;
    FIndiceVehiculo := IndiceVehiculo;
    FPantalla       := Pantalla;
    FEtiqueta       := Etiqueta;
    FMenu.Popup(Pos.X, Pos.Y);
end;

{-----------------------------------------------------------------------------
  Function Name: CargarListaMultiplesTransitos
  Author:    lgisuk
  Date Created: 16/05/2005
  Description: Obtengo la lista con los transitos seleccionados
  Parameters: ListaTransitos:TstringList
  Return Value: None
-----------------------------------------------------------------------------}


{ TMenuPagos }

constructor TMenuPagos.Create;
begin
    inherited;
    //
    FAnularRecibo           := TMenuItem.Create(FMenu);
    FAnularRecibo.Caption   := '&Anular Recibo';
    FAnularRecibo.OnClick   := DoAnularRecibo;
    FMenu.Items.Add(FAnularRecibo);

end;

procedure TMenuPagos.DoAnularRecibo(Sender: TObject);
var
    F: Tfrm_AnulacionRecibos;
begin
    Application.CreateForm(Tfrm_AnulacionRecibos, F);
    if F.Inicializar(FNumeroRecibo) then begin
    	F.Show;

		// Esto mucho no me gusta (no es muy prolijo)
        // Pero es para refrescar la Grilla
        // desde la cual se llam� la Anulaci�n
        with MenuPagos.FspPagos do begin
            DisableControls;
            Close;
            Open;
            EnableControls;
        end;
    end;
end;

procedure TMenuPagos.DoMenuPopup(Sender: TObject);
begin
  inherited;
end;

{********************************** File Header ********************************
File Name : MenuesContextuales.pas
Author : FLamas
Date Created: 15/07/2005
Language : ES-AR
Description :
*******************************************************************************}
procedure TMenuPagos.MostrarMenu(Pos: TPoint; NumeroRecibo: Int64; spPagos : TADOStoredProc);
begin
    FNumeroRecibo := NumeroRecibo;
    FspPagos := spPagos; // Asigna el SP de Pagos para poder refrescarlo

    // Si no tiene permisos para el item de menu Anular Recibos, deshabilitarlo.
    FAnularRecibo.Enabled := ExisteAcceso('ANULAR_RECIBO_DESDE_CARTOLA');
    FMenu.Popup(Pos.X, Pos.Y);
end;

{-----------------------------------------------------------------------------
  Function Name: MostrarMenuContextualMultiplesEstacionamientos
  Author:    Nelson Droguett Sierra
  Date Created: 03-Abril-2011
  Description:
  Parameters: Pos: TPoint; ListaEstacionamientos:tstringList
  Return Value: Boolean
-----------------------------------------------------------------------------}
function MostrarMenuContextualMultiplesEstacionamientos(Pos: TPoint; ListaEstacionamientos:TStringList): Boolean;
begin
    //Habilito la opci�n solo si tiene permisos
    MenuEstacionamientos.FReclamoEstacionamiento.Enabled := ExisteAcceso('REALIZAR_RECLAMO_ESTACIONAMIENTOS');
    //Muestro el menu
    MenuEstacionamientos.MostrarMenuMultiplesEstacionamientos(Pos, ListaEstacionamientos);
    Result := True;
end;



{ TMenuEstacionamientos }

constructor TMenuEstacionamientos.Create;
begin
    inherited;
    //
    FReclamos := TMenuItem.Create(FMenu);
    FReclamos.Caption := '&Reclamos';
    FMenu.Items.Add(FReclamos);
    //
    FReclamoEstacionamiento := TMenuItem.Create(FMenu);
    FReclamoEstacionamiento.Caption := '&Reclamo por estacionamientos no aceptados';
    FReclamoEstacionamiento.Tag := 217;
    FReclamoEstacionamiento.OnClick := MenuEstacionamientos.DoReclamoEstacionamiento;   // abro la ventana para editar un reclamo por transito   MenuReclamosGenerales.DoReclamoGeneral; //
    FReclamos.Add(FReclamoEstacionamiento);
end;


function MostrarMenuContextualEstacionamiento(Pos: TPoint; NumCorrEstacionamiento: Int64): Boolean;
begin
    MenuEstacionamientos.MostrarMenu(Pos, NumCorrEstacionamiento);
    Result := True;
end;


procedure TMenuEstacionamientos.DoMenuPopup(Sender: TObject);
Var
    Qry: TAdoQuery;
begin
    Qry := TADOQuery.Create(nil);
    Qry.Connection := DMConnections.BaseCAC;
//    Qry.SQL.Text := Format('SELECT FechaHora, RegistrationAccessibility FROM Transitos WITH (NOLOCK) WHERE NumCorrCA = %d', [FNumCorrCA]);
    Qry.SQL.Text := Format('SELECT FechaHoraEntrada FROM Estacionamientos WITH (NOLOCK) WHERE NumCorrEstacionamiento = %d', [FNumCorrEstacionamiento]);
    try
        Qry.Open;
        //FVerImagen.Visible := False ; //Qry.FieldByName('RegistrationAccessibility').AsInteger <> 0;
        FFechaHora := Qry.FieldByName('FechaHoraEntrada').AsDateTime;
    finally
        Qry.Free;
    end;
end;

procedure TMenuEstacionamientos.DoReclamoEstacionamiento(Sender: TObject);

            //Obtengo la Enumeraci�n
            function ObtenerEnumeracion(Lista: TStringList): String;
            var
                Enum: AnsiString;
                i: Integer;
            begin
                Enum := '';
                for i:= 0 to Lista.Count - 1 do begin
                    Enum := Enum + ParseParamByNumber(Lista.Strings[i],1,'=') + ',';
                end;
                Delete(Enum, Length(Enum), 1);
                Result := Enum;
            end;

            //informo los transitos reclamados
            Procedure InformarEstacionamientosReclamados(CodigoConvenio: Integer; ListaEstacionamientos:TStringList);
            Var
                F:  TFormEstacionamientosReclamados;
            begin
              	if FindFormOrCreate( TFormEstacionamientosReclamados, F) then
                		F.ShowModal
               	else begin
               		if not F.Inicializar(CodigoConvenio, ListaEstacionamientos) then f.Release;
              	end;
            end;

resourcestring
    MSG_ERROR = 'Debe seleccionar al menos un Estacionamiento';
Var
    F: TFormReclamoEstacionamiento;
    CodigoConvenio: Integer;
    NumCorrEstacionamiento: String;
begin
    //controlo que hallan seleccionado al menos un transito
    if MenuEstacionamientos.FListaEstacionamientos.Count = 0 then begin
        Msgbox(MSG_ERROR);
        Exit;
    end;

    //controlo que no hallan seleccionado estacionamientos que ya fueron reclamados
    // Inicio Bloque: SS_1006_PDO_20120106
    //CodigoConvenio:= QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.ObtenerCodigoConvenioEstacionamiento ('+ Copy( MenuEstacionamientos.FListaEstacionamientos.Strings[0], 0 , Pos('=',MenuEstacionamientos.FListaEstacionamientos.Strings[0])-1) + ')' );
    CodigoConvenio :=
        QueryGetValueInt(
            DMConnections.BaseCAC,
            'SELECT dbo.ObtenerCodigoConvenioEstacionamiento (' + MenuEstacionamientos.FListaEstacionamientos.Strings[0] + ')' );
    // Fin Bloque: SS_1006_PDO_20120106

    NumCorrEstacionamiento := QueryGetValue(dmconnections.BaseCAC,'SELECT TOP 1 NumCorrEstacionamiento FROM OrdenesServicio O WITH (NOLOCK) , OrdenesServicioEstacionamiento T  WITH (NOLOCK) WHERE O.CodigoOrdenServicio = T.CodigoOrdenServicio AND O.CodigoConvenio = ' +IntToStr(CodigoConvenio)+' and NumCorrEstacionamiento in (' + ObtenerEnumeracion(MenuEstacionamientos.FListaEstacionamientos) + ') ORDER BY O.CodigoOrdenServicio');
    if NumCorrEstacionamiento <> '' then begin
        InformarEstacionamientosReclamados(CodigoConvenio,MenuEstacionamientos.FListaEstacionamientos);
        Exit;
    end;

    //creo el form de reclamo por transitos no aceptados
    Application.CreateForm(TFormReclamoEstacionamiento, F);
    if F.Inicializar(TMenuItem(Sender).Tag, TMenuItem(Sender).Parent.Tag, MenuEstacionamientos.FListaEstacionamientos) then F.ShowModal;
end;

procedure TMenuEstacionamientos.MostrarMenu(Pos: TPoint; NumCorrEstacionamiento: Int64);
begin
    FNumCorrEstacionamiento := NumCorrEstacionamiento;
    FsNumCorrEstacionamiento:= IntToStr(NumCorrEstacionamiento);
    if not Assigned(FListaEstacionamientos) then begin     // SS_1006_PDO_20120106
        FListaEstacionamientos := TStringList.Create;      // SS_1006_PDO_20120106
    end                                                    // SS_1006_PDO_20120106
    else FListaEstacionamientos.Clear;                     // SS_1006_PDO_20120106
    FListaEstacionamientos.Add(FsNumCorrEstacionamiento);  // SS_1006_PDO_20120106
    FMenu.Popup(Pos.X, Pos.Y);
end;

procedure TMenuEstacionamientos.MostrarMenuMultiplesEstacionamientos(
  Pos: TPoint; ListaEstacionamientos: TstringList);
begin
    FListaEstacionamientos := ListaEstacionamientos;
    FMenu.Popup(Pos.X, Pos.Y);
end;


Initialization
    MenuCuentas                     := TMenuCuentas.Create;
    MenuTransitos                   := TMenuTransitos.Create;
    MenuTransitosNoCliente          := TMenuTransitosNoCliente.Create;
    MenuComprobantes                := TMenuComprobantes.Create;
    MenuOrdenesServicio             := TMenuOrdenesServicio.Create;
    MenuReclamosGenerales           := TMenuReclamosGenerales.Create;
    MenuPagos                       := TMenuPagos.Create;
    MenuEstacionamientos            := TMenuEstacionamientos.Create;
    //MenuEstacionamientosNoCliente   := TMenuEstacionamientosNoCliente.Create;
finalization
    FreeAndNil(MenuCuentas);
    FreeAndNil(MenuTransitos);
    FreeAndNil(MenuTransitosNoCliente);
    FreeAndNil(MenuComprobantes);
    FreeAndNil(MenuOrdenesServicio);
    FreeAndNil(MenuReclamosGenerales);
    FreeAndNil(MenuPagos);
    FreeAndNil(MenuEstacionamientos);
    //FreeAndNil(MenuEstacionamientosNoCliente);
end.
