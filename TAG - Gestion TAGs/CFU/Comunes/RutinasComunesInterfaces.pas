{-----------------------------------------------------------------------------
 File Name: RutinasComunesInterfaces.pas
 Author:    lgisuk
 Date Created: 25/11/2004
 Language: ES-AR
 Description:  Procedimientos y Funciones Comunes a todos los formularios
-----------------------------------------------------------------------------}
unit RutinasComunesInterfaces;

interface

uses //Comunes Interfaces
     UtilProc,
     UtilDB,
     Util,
     ConstParametrosGenerales,
     //General
     Classes, ADODB, VariantComboBox, Windows, Variants, Controls, SysUtils;

    //Author lgisuk
    Function RegistrarOperacionEnLogInterface(Conn: TAdoConnection;CodigoModulo:Integer;NombreArchivo,Usuario,Observaciones:string;recibido, Reprocesamiento:boolean;FechaHoraRecepcion:tdatetime;UltimoRegistroEnviado: integer;var CodigoOperacionInterfase:integer;Var DescError:Ansistring):boolean; overload;
    Function ActualizarObservacionesEnLogInterface(Base:TADOConnection;SP:tadostoredproc;CodigoOperacionInterfase:Integer;Observaciones:string;Agrega:boolean):boolean; overload;
    Function ActualizarLogOperacionesInterfaseAlFinal(Conn: TAdoConnection;CodigoOperacionInterfase:integer;Observaciones:string;Var DescError:Ansistring):boolean; overload;
    function AgregarDetalleMandatoRecibido(Conn: TAdoConnection; CodigoOperacionInterfase: Integer;CodigoConvenio: Integer; TipoOperacion: string; CodigoRespuesta: string; GlosaRespuesta: string; var DescError: AnsiString): Boolean;
    function AgregarDetalleMandatoEnviado(Conn: TAdoConnection; CodigoOperacionInterfase: Integer;CodigoConvenio: Integer; TipoOperacion: string; var DescError: AnsiString): Boolean;
    Function ExistePalabra(frase,buscar:string):boolean;
  	function AgregarErrorInterfase(Conn: TAdoConnection; CodigoOperacionInterfase : Integer; DescripcionError : String) : Boolean;
    function AgregarErrorInterfaz(Conn: TAdoConnection; CodigoOperacionInterfase : Integer; CodigoModulo : Integer; CodigoError : Integer; Fecha : TDateTime; DescripcionError : String) : Boolean;
    function AgregarResumenComprobantesEnviados(Conn: TAdoConnection; CodigoOperacionInterfase: Integer; Cantidad: Int64; Monto: Int64; var DescError: AnsiString): Boolean;
    //Author flamas
    Function CargarFechasDeInterfasesEjecutadas( sp : TADOStoredProc; CodigoModulo:Integer; Combo : TVariantComboBox ) : boolean;
    function ValidarFechaAAAAMMDD( sFecha : string ) : boolean;
    function VerificarArchivoProcesado( Base:TADOConnection; sFileName : string ) : boolean;
    function ValidarHeaderFooterPRESTO(var sArchivo : TStringList; sNombreArchivo, sEPS, sGlosa : string) : boolean;
    //Author mtraversa
    function RegistrarComprobanteEnviadoEnLog(Conn: TAdoConnection; TipoComprobante: ShortString;NumeroComprobante: LongInt; CodigoOperacionInterfase: Integer; var DescError: AnsiString): Boolean;
    function MsgProcesoFinalizado(sMensajeOK, sMensajeError, sTitulo: AnsiString; RProcesados, RExito, RError: integer):boolean;
    //Author mlopez
    function RemoveIllegalCharacters(Value: String): String;

implementation

resourcestring
    MSG_ERROR = 'Error';

{-----------------------------------------------------------------------------
  Function Name: RegistrarOperacionEnLogInterface
  Author:    lgisuk
  Date Created: 12/01/2005
  Description: Registra la Operacion en el Log 
  Parameters: Conn: TAdoConnection;CodigoModulo:Integer;Fecha:tdatetime;NombreArchivo,Usuario,Observaciones:string;recibido, Reprocesamiento:boolean;FechaHoraRecepcion:tdatetime;UltimoRegistroEnviado: integer;var CodigoOperacionInterfase:integer;Var DescError:Ansistring
  Return Value: boolean
-----------------------------------------------------------------------------}
Function  RegistrarOperacionEnLogInterface(Conn : TAdoConnection; CodigoModulo : Integer; NombreArchivo, Usuario, Observaciones : String; Recibido, Reprocesamiento : Boolean; FechaHoraRecepcion : TDateTime; UltimoRegistroEnviado : Integer; var CodigoOperacionInterfase : Integer; Var DescError : Ansistring) : Boolean; overload;
var sp : TADOStoredProc;
begin
  	CodigoOperacionInterfase := 0;
    Result := False;
    sp := TADOStoredProc.Create(Nil);
    try
        try
            sp.Connection := Conn;
            sp.ProcedureName := 'AgregarLogOperacionesInterfases';
            with SP.Parameters do begin
                Refresh;
                ParamByName('@CodigoModulo').Value:= CodigoModulo;
                ParamByName('@NombreArchivo').Value:= NombreArchivo;
                ParamByName('@Usuario').Value:= Usuario;
                ParamByName('@Observaciones').Value:= Observaciones;
                ParamByName('@Reprocesamiento').Value:= Reprocesamiento;
                ParamByName('@FechaHoraRecepcion').Value:= FechaHoraRecepcion;
                ParamByName('@UltimoRegistroEnviado').Value:= Iif( UltimoRegistroEnviado = 0, null, UltimoRegistroEnviado );
                ParamByName('@CodigoOperacionInterfase').Value := 0;
            end;
            SP.ExecProc;                                                                             //Ejecuto el procedimiento
            CodigoOperacionInterfase:= SP.Parameters.ParamByName('@CodigoOperacionInterfase').Value; //Obtengo el Id
            result:=true;
		    except
            on E : Exception do begin
                DescError := E.Message;
            end;
        end;
    finally
        SP.Free;
	  end;
end;

{-----------------------------------------------------------------------------
  Function Name: ActualizarLogOperacionesInterfaseAlFinal
  Author:    lgisuk
  Date Created: 04/07/2005
  Description: Asigna la fecha de finalizaci�n al final y las observaciones
  Parameters: Conn: TAdoConnection;CodigoOperacionInterfase:integer;Observaciones:string;Var DescError:Ansistring
  Return Value: boolean
-----------------------------------------------------------------------------}
Function  ActualizarLogOperacionesInterfaseAlFinal(Conn : TAdoConnection; CodigoOperacionInterfase : Integer; Observaciones : String; Var DescError : Ansistring) : Boolean;
var sp : TADOStoredProc;
begin
    Result := False;
    sp := TADOStoredProc.Create(Nil);
    try
        try
            sp.Connection := Conn;
            sp.ProcedureName := 'ActualizarLogOperacionesInterfaseAlFinal';
            with SP.Parameters do begin
                Refresh;
                ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacionInterfase;
                ParamByName('@Observaciones').Value := Observaciones;
            end;
            SP.ExecProc;                                                                             //Ejecuto el procedimiento
            Result := True;
		    except
            on E : Exception do begin
                DescError := E.Message;
            end;
        end;
    finally
        SP.Free;
  	end;
end;

{-----------------------------------------------------------------------------
  Function Name: ActualizarObservacionesEnLogInterface
  Author:    lgisuk
  Date Created: 13/12/2004
  Description: Permite Actualizar la Observacion de una operacion del log
  Parameters: SP:tadostoredproc;CodigoOperacionInterfase:Integer;Observaciones:string
  Return Value: Integer
-----------------------------------------------------------------------------}
Function ActualizarObservacionesEnLogInterface(Base:TADOConnection; SP:tadostoredproc; CodigoOperacionInterfase:Integer; Observaciones:string; Agrega:boolean):boolean; overload;
var SqlQuery, Value : String;
begin
    // Nombre del SP en OP_CAC -> ActualizarObservacionesLogOperacionesInterfase
    Result := False;
    SqlQuery:='SELECT Observaciones FROM LogOperacionesInterfases WHERE CodigoOperacionInterfase = '+IntToStr(CodigoOperacionInterfase);
   	Value := QueryGetValue(Base, SqlQuery);
    if Agrega = True then begin
        Value := Value + Observaciones;
    end else begin
        Value := Observaciones;
    end;
    with SP.Parameters do begin
        ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacionInterfase;
        ParamByName('@Observaciones').Value := Value;
    end;
    try
        SP.ExecProc;
        Result := True;                                                                             //Ejecuto el procedimiento
    except
        on E : Exception do begin
            MsgBoxErr(MSG_ERROR, E.message, 'Error Al', 0);                                        //el stored procedure podria fallar
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: AgregarDetalleMandatoRecibido
  Author:    lgisuk
  Date Created: 22/06/2005
  Description: permite registrar el mandato recibido
  Parameters: Conn: TAdoConnection; CodigoOperacionInterfase: Integer; CodigoConvenio: Integer; TipoOperacion: string; CodigoRespuesta: string; GlosaRespuesta: string; var DescError: AnsiString
  Return Value: Boolean
-----------------------------------------------------------------------------}
function AgregarDetalleMandatoRecibido(Conn: TAdoConnection; CodigoOperacionInterfase: Integer; CodigoConvenio: Integer; TipoOperacion: string; CodigoRespuesta: string; GlosaRespuesta: string; var DescError: AnsiString): Boolean;
begin
    Result := False;
    try
        with TADOStoredProc.Create(Nil) do
            try
                Connection := Conn;
                ProcedureName := 'AgregarDetalleMandatoRecibido';
                with Parameters do begin
                    Refresh;
                    ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacionInterfase;
                    ParamByName('@CodigoConvenio').Value           := CodigoConvenio;
                    ParamByName('@TipoOperacion').Value            := TipoOperacion;
                    ParamByName('@CodigoRespuesta').Value          := CodigoRespuesta;
                    ParamByName('@GlosaRespuesta').Value           := GlosaRespuesta;
                end;
                ExecProc;
                Result := True;
            finally
                Free;
            end;
    except
        on E: Exception do
			DescError := E.Message;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: AgregarDetalleMandatoEnviado
  Author:    lgisuk
  Date Created: 22/06/2005
  Description: Permite registrar el mandato enviado
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
function AgregarDetalleMandatoEnviado(Conn: TAdoConnection; CodigoOperacionInterfase: Integer; CodigoConvenio: Integer; TipoOperacion: string; var DescError: AnsiString): Boolean;
begin
    Result := False;
    try
        with TADOStoredProc.Create(Nil) do
            try
                Connection := Conn;
                ProcedureName := 'AgregarDetalleMandatoEnviado';
                with Parameters do begin
                    Refresh;
                    ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacionInterfase;
                    ParamByName('@CodigoConvenio').Value := CodigoConvenio;
                    ParamByName('@TipoOperacion').Value := TipoOperacion;
                end;
                ExecProc;
                Result := True;
            finally
                Free;
            end;
    except
        on E : Exception do begin
    			  DescError := E.Message;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ExistePalabra
  Author:    lgisuk
  Date Created: 07/12/2004
  Description: verifica si existe una palabra dentro de una frase
  Parameters: frase,buscar:string
  Return Value: boolean
-----------------------------------------------------------------------------}
Function ExistePalabra(Frase, Buscar : String) : Boolean;
var Posi : Integer;
  	Bandera : Boolean;
begin
     Posi := Pos(Buscar, Frase); //Esta funcion delphi hace lo mismo que la substring que cree
     if Posi = 0 then begin
        Bandera := False;
     end
     else begin
        Bandera := True;
     end;
     Result := Bandera;
end;

{-----------------------------------------------------------------------------
  Function Name: AgregarErrorInterfase
  Author:    lgisuk
  Date Created: 17/06/2005
  Description: Agrega un mensaje de Error en la interfase
  Parameters: Conn: TAdoConnection; nCodigoOperacionInterfase, sDescripcionError : string
  Return Value: boolean
-----------------------------------------------------------------------------}
function AgregarErrorInterfase(Conn: TAdoConnection; CodigoOperacionInterfase : Integer; DescripcionError : String) : Boolean;
resourcestring
    MSG_ERROR_GENERATING_ERROR_LOG = 'Error generando log de errores';
  	MSG_ERROR = 'Error';
var
    spAgregarErrorInterfase : TADOStoredProc;
begin
  	Result := False;
    spAgregarErrorInterfase := TADOStoredProc.Create(Nil);
   	try
		    try
            with spAgregarErrorInterfase, Parameters do begin
                Connection := Conn;
                ProcedureName := 'AgregarErrorInterfase';
            	  Refresh;
                ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacionInterfase;
				        ParamByName('@DescripcionError').Value := DescripcionError;
                ExecProc;
                Result := True;
            end;
        except
			      on e: Exception do begin
                MsgBoxErr(MSG_ERROR_GENERATING_ERROR_LOG, e.Message, MSG_ERROR, MB_ICONERROR);
            end;
        end;
    finally
        spAgregarErrorInterfase.Free;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: AgregarErrorInterfaz
  Author:    lgisuk
  Date Created: 19/01/2006
  Description: Agrega un mensaje de Error en la interfase
  Parameters:
  Return Value: boolean
-----------------------------------------------------------------------------}
function AgregarErrorInterfaz(Conn: TAdoConnection; CodigoOperacionInterfase : Integer; CodigoModulo : Integer; CodigoError : Integer; Fecha : TDateTime; DescripcionError : String) : Boolean;
resourcestring
    MSG_ERROR_GENERATING_ERROR_LOG = 'Error generando log de errores';
  	MSG_ERROR = 'Error';
var
    spAgregarErrorInterfaz : TADOStoredProc;
begin
  	Result := False;
    spAgregarErrorInterfaz := TADOStoredProc.Create(Nil);
   	try
		    try
            with spAgregarErrorInterfaz, Parameters do begin
                Connection := Conn;
                ProcedureName := 'AgregarErrorInterfaz';
            	  Refresh;
                ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacionInterfase;
                ParamByName('@CodigoModulo').Value := CodigoModulo;
                ParamByName('@CodigoError').Value := CodigoError;
                ParamByName('@Fecha').Value := Fecha;
				        ParamByName('@DescripcionError').Value := DescripcionError;
                ExecProc;
                Result := True;
            end;
        except
			      on e: Exception do begin
                MsgBoxErr(MSG_ERROR_GENERATING_ERROR_LOG, e.Message, MSG_ERROR, MB_ICONERROR);
            end;
        end;
    finally
        spAgregarErrorInterfaz.Free;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarFechasDeInterfasesEjecutadas
  Author:    flamas
  Date Created: 09/12/2004
  Description: Carga un VariantComoBox con las Fechas en que se ejecutaron las interfaces
  				de un m�dulo determinado y sus correspondientes c�digos de operaci�n
                sirve para reprocesar la interfase de una fecha determinada
  Parameters: sp : TADOStoreProcedure; CodigoModulo:Integer; Combo : TVariantComboBox
  Return Value: boolean
-----------------------------------------------------------------------------}
Function  CargarFechasDeInterfasesEjecutadas( sp : TADOStoredProc; CodigoModulo:Integer; Combo : TVariantComboBox ) : boolean;
resourcestring
	MSG_COULD_NOT_LOAD_INTERFACES = 'No se pudieron cargar las interfaces anteriores';
begin
  	result := False;
    try
    	with SP do begin
    		Parameters.ParamByName( '@CodigoModulo' ).Value := CodigoModulo;
    		try
    			  Open;
        		Combo.Clear;
        		while not Eof do begin
                Combo.Items.Add( FormatDateTime( 'dd-mm-yyyy', FieldByName( 'FECHA' ).AsDateTime), FieldByName( 'CODIGOOPERACIONINTERFASE' ).AsInteger );
                Next;
        		end;
        		result := True;
    		except
    			on e: Exception do
        			MsgBoxErr(MSG_COULD_NOT_LOAD_INTERFACES, e.Message, MSG_ERROR, MB_ICONERROR);
    		end;
    	end;
    finally
    	  SP.Close;
  	end;
end;

{-----------------------------------------------------------------------------
  Function Name: ValidarFecha
  Author:    flamas
  Date Created: 23/12/2004
  Description: Valida una fecha dada en formato AAAMMDD
  Parameters: sFecha : string
  Return Value: boolean
-----------------------------------------------------------------------------}
function ValidarFechaAAAAMMDD( sFecha : string ) : boolean;
begin
	  try
        EncodeDate( StrToInt(Copy(sFecha, 1, 4)), StrToInt(Copy(sFecha, 5, 2)), StrToInt(Copy(sFecha, 7, 2)));
        Result := True;
    except
    	  on exception do begin
            Result := False;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: FechaHoraAAAAMMDDToDateTime
Author : ndonadio
Date Created : 11/08/2005
Description : Valida y convierte una fecha hora en formato
                AAAAMMDDHHMMSS a un DateTime.
                Se basa en la fn YYYYMMDDHHMMSS_TO_DATETIME
                en la DB CAC del SQL.
Parameters : strFecha: Ansistring; var outFecha: TDateTime
Return Value : boolean
*******************************************************************************}
function FechaHoraAAAAMMDDToDateTime(Conn: TAdoConnection;  strFecha: Ansistring; var outFecha: TDateTime; var Error: AnsiString): boolean;
begin
    Result := False;
    try
        outFecha := QueryGetValueDateTime(Conn, Format('SELECT dbo.YYYYMMDDHHMMSS_TO_DATETIME(''%s'','':'')',[TRIM(strFecha)]));
        Result := True;
    except
        on e:exception do begin
            Error := e.Message;
        end;
    end;
end;
{-----------------------------------------------------------------------------
  Function Name: VerificarArchivoProcesado
  Author:    flamas
  Date Created: 23/12/2004
  Description: Verifica si el Archivo ha sido procesado en una interface
  Parameters: sFileName : string
  Return Value: boolean
-----------------------------------------------------------------------------}
function VerificarArchivoProcesado ( Base:TADOConnection; sFileName : string ) : boolean;
resourcestring
    MSG_FILE_ALREADY_PROCESSED = 'El archivo %s ya fue procesado';
begin
    Result := ( QueryGetValue(Base, 'SELECT dbo.VerificarArchivoInterfaseProcesado(''' + ExtractFileName( sFileName ) + ''')') = 'True' );
end;

{-----------------------------------------------------------------------------
  Function Name: RegistrarComprobanteEnviadoEnLog
  Author:    mtraversa
  Date Created: 28/12/2004
  Description: Crea un nuevo registro en la tabla DetalleComprobantesEnviados
  Parsmeters: Conn: TAdoConnection; TipoComprobante: ShortString; NumeroComprobante: LongInt; CodigoOperacionInterfase: Integer
  Return Value:None
-----------------------------------------------------------------------------}
function RegistrarComprobanteEnviadoEnLog(Conn: TAdoConnection; TipoComprobante: ShortString; NumeroComprobante: LongInt; CodigoOperacionInterfase: Integer; var DescError: AnsiString): Boolean;
begin
    Result := False;
    try
        with TADOStoredProc.Create(Nil) do
            try
                Connection := Conn;
                ProcedureName := 'AgregarDetalleComprobantesEnviados';
                with Parameters do begin
                    Refresh;
                    ParamByName('@TipoComprobante').Value          := TipoComprobante;
                    ParamByName('@NumeroComprobante').Value        := NumeroComprobante;
                    ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacionInterfase;
                end;
                ExecProc;
                Result := True;
        finally
            Free;
        end;
    except
        on E: Exception do begin
            DescError := E.Message;
        end;
    end;
end;


{-----------------------------------------------------------------------------
  Procedure: MsgProcesoFinalizado
  Author:    ndonadio
  Date:      24-Abr-2005
  Arguments: sMensaje (AnsiString):   Mensjae Original.
             sTitulo: (AnsiString):  Caption para el dialogo.
             RProcesados, RExito, RError (integer) : Cantidad de Registros
                                        procesados total, exitosos y erroneos.
  Result:    boolean
  Decription:
-----------------------------------------------------------------------------}
function MsgProcesoFinalizado(sMensajeOK, sMensajeError, sTitulo: AnsiString; RProcesados, RExito, RError: integer):boolean;
resourcestring
    MSG_PROC_INFO                       =   CrLf+'Cantidad de Registros Procesados: %d '+
                                            CrLf+'Cantidad de Registros Procesados con �xito: %d ' +
                                            CrLf+'Cantidad de Registros Procesados con Error: %d ';
    MSG_ASK_SHOW_ERROR_REPORT           =   CrLf + 'Desea ver el Reporte de Errores?.';
var
    Mensaje     : AnsiString;
    Respuesta   : Boolean;
begin
    Mensaje :=  Format(MSG_PROC_INFO,[RProcesados, RExito, RError]);

    if RError > 0 then begin
        Mensaje := sMensajeError + Mensaje +  MSG_ASK_SHOW_ERROR_REPORT;
        Respuesta := (MsgBox(Mensaje, sTitulo, MB_ICONWARNING + MB_YESNO) = mrYes);
    end
    else begin
        Mensaje := sMensajeOK + Mensaje;
        Respuesta := False;
        MsgBox(Mensaje, sTitulo, MB_ICONINFORMATION);
    end;
    Result :=  Respuesta;
end;

{-----------------------------------------------------------------------------
  Function Name: MoverArchivoProcesado
  Author:    ndonadio
  Date Created: 02/05/2005
  Description: Mueve el archivo de origen al de destino.
                Se utiliza principalmente para mover los archivos procesados
                exitosamente a un directorio de archivos procesados.

  Parameters: PathNameOrigen, PathNameDestino: AnsiString
  Return Value: boolean
-----------------------------------------------------------------------------}
function MoverArchivoProcesado(MsgTitulo, PathNameOrigen, PathNameDestino: AnsiString; NoInfo: Boolean = False): boolean;
resourcestring
    MSG_ERROR_MOVING_FILE   =   'El archivo %s '+CrLf+
                                'no se ha podido mover a: %s'+CrLf +
                                'Deber� completar la operaci�n manualmente.';
    MSG_MOVING_COMPLETE     =   'El archivo %s '+CrLf+
                                'ha sido movido a: %s';
begin
 try
    if TRIM(PathNameOrigen) = TRIM(PathNameDestino) then
    begin
        Result := True;
        Exit;
    end;
    if MoveFile(PChar(PathNameOrigen), PChar(PathNameDestino)) then begin
        Result := True;
        if NOT NoInfo then MsgBox(Format(MSG_MOVING_COMPLETE,[PathNameOrigen, PathNameDestino]),'INFORMACI�N: '+MsgTitulo,MB_ICONINFORMATION);
    end
    else begin
        Result := False;
        MsgBox(Format(MSG_ERROR_MOVING_FILE, [PathNameOrigen, PathNameDestino]),'ADVERTENCIA: '+MsgTitulo,MB_ICONWARNING);
    end;
 except
    Result := False;
    MsgBox(Format(MSG_ERROR_MOVING_FILE,[PathNameOrigen, PathNameDestino]),'ADVERTENCIA: '+MsgTitulo,MB_ICONWARNING);
 end;
end;


{-----------------------------------------------------------------------------
  Function Name: ValidarHeaderFooterPRESTO
  Author:    flamas
  Date Created: 27/05/2005
  Description: Valida un Header de PRESTO
  Parameters: var sArchivo : TStringList; sNombreArchivo, sEPS, sGlosa : string
  Return Value: boolean
-----------------------------------------------------------------------------}
function  ValidarHeaderFooterPRESTO(var sArchivo : TStringList; sNombreArchivo, sEPS, sGlosa : string) : boolean;
resourcestring
	MSG_INVALID_HEADER = 'El encabezado del archivo es inv�lido';
	MSG_INVALID_DESCRIPTION = 'La glosa es incorrecta';
	MSG_INVALID_DATE = 'La fecha de glosa es incorrecta';
	MSG_INVALID_COMPANY = 'El c�digo de empresa prestadora de servicios es inv�lido';
	MSG_INVALID_FOOTER = 'El pie de p�gina del archivo es inv�lido';
	MSG_INVALID_REGISTER_COUNT 	= 'La cantidad de registros del archivo'#10#13 + 'no coincide con la cantidad reportada';
	MSG_ERROR = 'Error';
var
  	nLineas : integer;
    nRegs	: integer;
begin
  	nLineas := sArchivo.Count - 1;

    result := False;

    // Valida el Header
  	if ( Pos( 'HEADER', sArchivo[0] ) <> 1 ) then begin
    	MsgBox( MSG_INVALID_HEADER, MSG_ERROR, MB_ICONERROR );
    	Exit;
    end;

    // Valida la EPS
    if ( Pos( sEPS , sArchivo[0] ) <> 8 ) then begin
    	MsgBox( MSG_INVALID_COMPANY, MSG_ERROR, MB_ICONERROR );
    	Exit;
    end;

    // Valida la Glosa
	  if  (Pos( UpperCase(Trim( sGlosa )), UpperCase(sArchivo[0])) <> 15)  then begin
    	MsgBox( MSG_INVALID_DESCRIPTION, MSG_ERROR, MB_ICONERROR );
     	Exit;
    end;

    // Valida el Footer
    if ( Pos( 'FOOTER', sArchivo[nLineas] ) <> 1 ) then begin
    	MsgBox( MSG_INVALID_FOOTER, MSG_ERROR, MB_ICONERROR );
    	Exit;
    end;

  	try
      	nRegs := StrToInt( trim( Copy( sArchivo[nLineas], 8, 6 )));
    except
      	on exception do begin
    	    	MsgBox( MSG_INVALID_FOOTER, MSG_ERROR, MB_ICONERROR );
    		    Exit;
		    end;
    end;

    if ( nRegs <> nLineas - 1 ) then begin
    	  MsgBox( MSG_INVALID_REGISTER_COUNT, MSG_ERROR, MB_ICONERROR );
    	  Exit;
    end;

    Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: AgregarResumenComprobantesEnviados
  Author:    lgisuk
  Date Created: 10/07/2006
  Description: Permite registrar el resumen de lo que se envio
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
function AgregarResumenComprobantesEnviados(Conn: TAdoConnection; CodigoOperacionInterfase: Integer; Cantidad: Int64; Monto: Int64; var DescError: AnsiString): Boolean;
begin
    Result := False;
    try
        with TADOStoredProc.Create(Nil) do
            try
                Connection := Conn;
                ProcedureName := 'AgregarResumenComprobantesEnviados';
                with Parameters do begin
                    Refresh;
                    ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacionInterfase;
                    ParamByName('@Cantidad').Value := Cantidad;
                    ParamByName('@Monto').Value := Monto;
                end;
                ExecProc;
                Result := True;
            finally
                Free;
            end;
    except
        on E : Exception do begin
    			  DescError := E.Message;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: RemoveIllegalCharacters
Author : mlopez
Date Created : 21/07/2006
Description : Esta funcion pide un String y evalua que no existan caracteres
              distintos de los definidos en LegalSetOfCharacters. Devuelve
              un string nuevo que no contiene los caracteres ilegales.
              Ojo!!!!
              Elimina los caracteres ilegales, asi que a tener cuidado cuando
              se utilice.
Parameters : Value: String
Return Value : String
*******************************************************************************}
function RemoveIllegalCharacters(Value: String): String;
const
    LegalSetOfCharacters: set of Char = ['A'..'Z', 'a'..'z', ' ', '0'..'9'];
var
    I, Len: Integer;
    LineToValidate: String;
begin
    Result := '';
    LineToValidate := Value;
    Len := Length(LineToValidate);
    for I := 1 to Len do begin
        if not(LineToValidate[I] in LegalSetOfCharacters) then begin
            LineToValidate[I] := ' ';
        end;
    end;
    Result := LineToValidate;
end;

end.
