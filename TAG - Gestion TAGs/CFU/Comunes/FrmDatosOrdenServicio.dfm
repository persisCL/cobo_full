object FormDatosOrdenServicio: TFormDatosOrdenServicio
  Left = 152
  Top = 155
  BorderStyle = bsDialog
  Caption = 'Informaci'#243'n de la Orden de Servicio'
  ClientHeight = 551
  ClientWidth = 795
  Color = clBtnFace
  Constraints.MinHeight = 578
  Constraints.MinWidth = 800
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 384
    Top = 6
    Width = 235
    Height = 13
    Caption = 'Detalle de las tareas realizadas hasta el momento:'
  end
  object Label2: TLabel
    Left = 8
    Top = 6
    Width = 194
    Height = 13
    Caption = 'Datos generales de la Orden de Servicio:'
  end
  object re_Tareas: TRichEdit
    Left = 375
    Top = 24
    Width = 401
    Height = 489
    Color = 16447730
    HideScrollBars = False
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 1
  end
  object re_General: TRichEdit
    Left = 4
    Top = 24
    Width = 362
    Height = 489
    Color = 16447730
    HideScrollBars = False
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 0
  end
  object OPButton1: TDPSButton
    Left = 709
    Top = 520
    Cancel = True
    Caption = '&Cerrar'
    ModalResult = 2
    TabOrder = 2
  end
  object btn_Ejecutar: TDPSButton
    Left = 104
    Top = 520
    Width = 149
    TabOrder = 3
    OnClick = btn_EjecutarClick
  end
  object OrdenServicio: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'CodigoOrdenServicio'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'
      '  OS.*,'
      '  DOS.*,'
      
        '  TelefonoPersonal = dbo.ObtenerValorMedioComunicacion(DOS.Codig' +
        'oPersona, dbo.CONST_MEDIOS_CONTACTO_TELEFONO(), dbo.CONST_TIPOSO' +
        'RIGENDATOS_PARTICULAR()),'
      
        '  Email = dbo.ObtenerValorMedioComunicacion(DOS.CodigoPersona, d' +
        'bo.CONST_MEDIOS_CONTACTO_EMAIL(), dbo.CONST_TIPOSORIGENDATOS_PAR' +
        'TICULAR()),'
      '  Workflows.Descripcion AS Descripcion,'
      
        '  dbo.ObtenerNombreContacto(OS.CodigoComunicacion) as NombreCont' +
        'acto'
      'FROM'
      '  OrdenesServicio OS,'
      '  VW_DatosOrdenServicio DOS,'
      '  Workflows'
      'WHERE'
      '  OS.CodigoOrdenServicio = :CodigoOrdenServicio'
      '  AND Workflows.CodigoWorkflow = OS.CodigoWorkflow'
      '  AND DOS.CodigoOrdenServicio = OS.CodigoOrdenServicio'
      '')
    Left = 414
    Top = 90
  end
  object qry_Facturacion: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'CodigoOrdenServicio'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'
      #9'OSF.*,'
      #9'C.Patente,'
      #9'T.ContextMark, T.ContractSerialNumber,'
      ''
      #9'DescriMarca = dbo.ObtenerMarca (C.CodigoMarca),'
      
        #9'DescriModelo = dbo.ObtenerVehiculoModelo (C.CodigoMarca, C.Codi' +
        'goModelo),'
      #9'DescriColor = dbo.ObtenerColor (C.CodigoColor),'
      
        #9'DescriCategoria = dbo.ObtenerCategoria (C.CodigoMarca, C.Codigo' +
        'Modelo)'
      'FROM'
      #9'OrdenesServicioFacturacion OSF INNER JOIN'
      #9'dbo.Cuentas C ON OSF.CodigoCuenta = C.CodigoCuenta INNER JOIN'
      #9'dbo.TagsAsignados T ON C.CodigoCuenta = T.CodigoCuenta'
      'WHERE'
      #9'OSF.CodigoOrdenServicio = :CodigoOrdenServicio'
      '')
    Left = 483
    Top = 90
  end
  object qry_DatosMoroso: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'CodigoOrdenServicio'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Clientes.*'
      'FROM '
      'OrdenesServicioMorosos, Clientes'
      'WHERE'
      
        '(OrdenesServicioMorosos.CodigoOrdenServicio = :CodigoOrdenServic' +
        'io) AND'
      '(OrdenesServicioMorosos.CodigoPersona = Clientes.CodigoPersona)')
    Left = 516
    Top = 90
  end
  object qry_reparacion: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'CodigoOrdenServicio'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT * FROM OrdenesServicioReparacionPortico WHERE  CodigoOrde' +
        'nServicio = :CodigoOrdenServicio')
    Left = 549
    Top = 90
  end
  object qry_Adhesion: TADOQuery
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'CodigoOrdenServicio'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 299
      end>
    SQL.Strings = (
      'SELECT'
      #9'DOA.*,'
      #9'DescriMarca = dbo.ObtenerMarca (DOA.CodigoMarca),'
      
        #9'DescriModelo = dbo.ObtenerVehiculoModelo (DOA.CodigoMarca, DOA.' +
        'CodigoModelo),'
      #9'DescriColor = dbo.ObtenerColor (DOA.CodigoColor),'
      
        #9'DescriCategoria = dbo.ObtenerCategoria (DOA.CodigoMarca, DOA.Co' +
        'digoModelo)'
      'FROM'
      #9'VW_DatosOrdenAdhesion DOA'
      'WHERE'
      #9'DOA.CodigoOrdenServicio = :CodigoOrdenServicio'
      '')
    Left = 450
    Top = 90
  end
  object ObtenerDomiciliosPersonaParticular: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDomiciliosPersona;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoTipoOrigenDato'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = 0
      end>
    Left = 417
    Top = 120
  end
  object ObtenerDomiciliosPersonaComercial: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDomiciliosPersona;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoTipoOrigenDato'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = 0
      end>
    Left = 450
    Top = 120
  end
  object ObtenerTareasTerminadas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerTareasTerminadas'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoOrdenServicio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 483
    Top = 121
  end
end
