unit FrmTerminarTareaMantenimiento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, StdCtrls, BuscaTab, Grids, ExtCtrls,
  DMConnection, Util, UtilProc, Buttons, UtilDB;

type
  TFormTerminarTareaMantenimiento = class(TForm)
    Panel1: TPanel;
    btnAceptar: TButton;
    btnCancelar: TButton;
    sgLegajosHoras: TStringGrid;
    Label1: TLabel;
    btLegajos: TBuscaTabla;
    qryLegajos: TADOQuery;
    btnEliminarLegajo: TSpeedButton;
    btnAgregarLegajo: TSpeedButton;
    Label2: TLabel;
    sgEquiposConsumo: TStringGrid;
    btnAgregarEquipo: TSpeedButton;
    btnEliminarEquipo: TSpeedButton;
    qryEquipos: TADOQuery;
    btEquipos: TBuscaTabla;
    ActualizarTareaHorasEmpleado: TADOStoredProc;
    ActualizarTareaEquiposConsumo: TADOStoredProc;
    procedure btnAceptarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnAgregarLegajoClick(Sender: TObject);
    function btLegajosProcess(Tabla: TDataSet; var Texto: String): Boolean;
    procedure sgLegajosHorasKeyPress(Sender: TObject; var Key: Char);
    procedure btLegajosSelect(Sender: TObject; Tabla: TDataSet);
    procedure btnEliminarLegajoClick(Sender: TObject);
    procedure sgEquiposConsumoKeyPress(Sender: TObject; var Key: Char);
    procedure btnAgregarEquipoClick(Sender: TObject);
    procedure btnEliminarEquipoClick(Sender: TObject);
    function btEquiposProcess(Tabla: TDataSet; var Texto: String): Boolean;
    procedure btEquiposSelect(Sender: TObject; Tabla: TDataSet);
    procedure sgLegajosHorasExit(Sender: TObject);
    procedure sgLegajosHorasSelectCell(Sender: TObject; ACol,
      ARow: Integer; var CanSelect: Boolean);
    procedure sgEquiposConsumoExit(Sender: TObject);
    procedure sgEquiposConsumoSelectCell(Sender: TObject; ACol,
      ARow: Integer; var CanSelect: Boolean);
  private
    { Private declarations }
    FOrdenServicio: integer;
    FTarea: integer;
  public
    { Public declarations }
    function inicializar(OrdenServicio, CodigoTarea: integer): boolean;
  end;

var
  FormTerminarTareaMantenimiento: TFormTerminarTareaMantenimiento;
  ConjuntoPersonal: set of char;
  ConjuntoEquipos: set of char;
  CantidadLegajos, CantidadEquipos: integer;

implementation

{$R *.dfm}
function DevolverMinutos(Hora: AnsiString): integer;
begin
	Result := Ival(Copy(Trim(Hora), 1, 2)) * 60 + Ival(Copy(Trim(Hora), 4, 2));
end;

function FormatearHora(Hora : AnsiString): AnsiString;
begin
	Hora := Trim(Hora);
	if Length(Hora) <= 2 then begin

	    Result := iif(Hora = '', '', PadL(Hora, 2, '0') + Timeseparator + '00');

	end else if Length(Hora) = 3 then begin
        case Pos(TimeSeparator, Hora) of
        	1 : Result := '00' + Timeseparator +  Copy(Hora, 2, 2);
            2 : Result := PadL(Copy(Hora, 1, 1),  2, '0') +
            	Timeseparator +
                PadR(Copy(Hora, 3, 2), 2, '0');
            else Result := ''; //'00' + Timeseparator +  '00';
        end;
    end else if Length(Trim(Hora)) = 4 then begin
    	case Pos(TimeSeparator, Hora) of
        	1, 4 : Result := '';//'00' + Timeseparator +  '00';
            2, 3 : Result := PadL(Copy(Hora, 1, Pos(TimeSeparator, Hora)-1), 2, '0') +
            	Timeseparator +
                PadR(Copy(Hora, Pos(TimeSeparator, Hora) + 1, 2), 2, '0');
        end;
	end else if Length(Trim(Hora)) = 5 then begin
    	case  Pos(TimeSeparator, Hora) of
			1 : Result := PadL(Copy(Hora, 2, 2), 2, '0') + Timeseparator + PadR(Copy(Hora, 4, 2), 2, '0');
        	2,4 : Result := '';
            3 : Result := Hora;
            5 : Result := Copy(Hora, 1, 2) + Timeseparator + Copy(Hora, 3, 2);
        end;
	end else if Length(Trim(Hora)) > 5 then begin
	    Result := '';
    end;

    Result := iif( (Result = '') or (Ival(copy(Result, 4, 2)) < 60), Result, '');
end;

procedure TFormTerminarTareaMantenimiento.btnAceptarClick(Sender: TObject);
resourcestring
	MSG_ERROR_EMPLEADO		= 'No se pudieron cargar las horas del Empleado';
    MSG_CAPTION_EMPLEADO	= 'Cargar Horas de Empleado';
	MSG_ERROR_EQUIPO		= 'No se pudo cargar el Consumo del Equipo';
    MSG_CAPTION_EQUIPO 		= 'Cargar Consumo de Equipo';
    MSG_VALIDAR_EMPLEADOS	= 'Debe especificarse al menos un empleado';

var
	TodoOk: boolean;
    i: integer;
begin
	TodoOk := False;
    if CantidadLegajos <= 0 then begin
  		MsgBoxBalloon(MSG_VALIDAR_EMPLEADOS, MSG_CAPTION_EMPLEADO, MB_ICONSTOP,
        	sgLegajosHoras);
    	Exit;
    end;
    
    Screen.Cursor := crHourGlass;
    DMConnections.BaseCAC.BeginTrans;
	try
    	for i := 1 to sgLegajosHoras.RowCount  do begin
        	if ( Ival(StrRight(sgLegajosHoras.Cells[0, i], 10)) > 0) and
               ( Trim(sgLegajosHoras.Cells[1,i]) <> '')	  then begin
            	try
                	with ActualizarTareaHorasEmpleado do begin
                    	Parameters.ParamByName('@CodigoOrdenServicio').Value	:= FOrdenServicio;
                    	Parameters.ParamByName('@CodigoTarea').Value 		 	:= FTarea;
                    	Parameters.ParamByName('@Legajo').Value					:= Ival(StrRight(sgLegajosHoras.Cells[0, i], 10));
                    	Parameters.ParamByName('@Horas').Value 					:= DevolverMinutos(Trim(sgLegajosHoras.Cells[1, i]));
                        ExecProc;
                    end;
				except
                	on E: Exception do begin
                    	MsgBoxErr(MSG_ERROR_EMPLEADO, e.Message, MSG_CAPTION_EMPLEADO, MB_ICONSTOP);
                        Exit;
                    end;
                end;
            end;
        end;
    	for i := 1 to sgEquiposConsumo.RowCount  do begin
        	if ( Ival(StrRight(sgEquiposConsumo.Cells[0, i], 10)) > 0) and
               (( Trim(sgEquiposConsumo.Cells[1,i]) <> '') or
               	( Trim(sgEquiposConsumo.Cells[2,i]) <> '') )	  then begin
            	try
                	with ActualizarTareaEquiposConsumo do begin
                    	Parameters.ParamByName('@CodigoOrdenServicio').Value	:= FOrdenServicio;
                    	Parameters.ParamByName('@CodigoTarea').Value 		 	:= FTarea;
                    	Parameters.ParamByName('@CodigoEquipo').Value			:= Ival(StrRight(sgEquiposConsumo.Cells[0, i], 10));
                    	Parameters.ParamByName('@ConsumoKms').Value 			:= Ival(sgEquiposConsumo.Cells[1, i]) * 100;
                    	Parameters.ParamByName('@ConsumoHs').Value 				:= DevolverMinutos(Trim(sgEquiposConsumo.Cells[2, i]));
                        ExecProc;
                    end;
				except
                	on E: Exception do begin
                    	MsgBoxErr(MSG_ERROR_EMPLEADO, e.Message ,MSG_CAPTION_EMPLEADO, MB_ICONSTOP);
                        Exit;
                    end;
                end;
            end;
        end;

        TodoOk := True;

    finally
    	if TodoOK then DMConnections.BaseCAC.CommitTrans
        else DMConnections.BaseCAC.RollbackTrans;
    end;
    Screen.Cursor := crDefault;
	ModalResult := mrOk;
end;

procedure TFormTerminarTareaMantenimiento.btnCancelarClick(
  Sender: TObject);
begin
	Close;
end;

procedure TFormTerminarTareaMantenimiento.btnAgregarLegajoClick(
  Sender: TObject);
begin
	btLegajos.Activate;
end;

function TFormTerminarTareaMantenimiento.btLegajosProcess(Tabla: TDataSet;
  var Texto: String): Boolean;
begin
	Texto :=
    	Trim(Tabla.FieldByName('ApellidoNombre').AsString) + Space(200) +
        Tabla.FieldByName('Legajo').AsString;
	Result := True;
end;

function TFormTerminarTareaMantenimiento.inicializar(OrdenServicio, CodigoTarea: integer): boolean;
begin
	Result := False;
    FOrdenServicio:= OrdenServicio;
    FTarea:= CodigoTarea;
	if not OpenTables([qryLegajos, qryEquipos]) then Exit;

    with sgLegajosHoras do begin
		Cells[0, 0] := 'Personal';
		Cells[1, 0] := 'Horas';
	end;

	with sgEquiposConsumo do begin
	    Cells[0, 0] := 'Equipo';
		Cells[1, 0] := 'Consumo en Km';
		Cells[2, 0] := 'Consumo en Hs';
    end;

    CantidadLegajos	:= 0;
    CantidadEquipos	:= 0;

	ConjuntoPersonal	:=  [ char(VK_BACK), char(VK_TAB), char(VK_CLEAR), char(VK_RETURN), '0'..'9', TimeSeparator, CHar(VK_SPACE), Char(VK_DELETE)];
	ConjuntoEquipos		:=  [ char(VK_BACK), char(VK_TAB), char(VK_CLEAR), char(VK_RETURN), '0'..'9', DecimalSeparator, CHar(VK_SPACE), Char(VK_DELETE)];
    Result		:= True;
end;

procedure TFormTerminarTareaMantenimiento.sgLegajosHorasKeyPress(
  Sender: TObject; var Key: Char);
begin
	if not (Key in ConjuntoPersonal) or
    	((Key = TimeSeparator) and (Pos(sgLegajosHoras.Cells[sgLegajosHoras.Col, sgLegajosHoras.Row], TimeSeparator) > 0)) or
	   ((Length(Trim(sgLegajosHoras.Cells[sgLegajosHoras.Col, sgLegajosHoras.Row])) > 4)
       	and (Key <> char(VK_BACK)))	  then Key := #0;
end;

procedure TFormTerminarTareaMantenimiento.btLegajosSelect(Sender: TObject;
  Tabla: TDataSet);
var
	FLegajo, i: integer;
    NuevoLEgajo: Boolean;
begin
	FLegajo := Tabla.FieldByName('Legajo').AsInteger;

	//Primero me fijo que este empleado ya no este en la lista.
    NuevoLegajo := True;
    for i := 1 to sgLegajosHoras.RowCount - 1  do begin
    	if Ival(StrRight(sgLegajosHoras.Cells[0, i], 10)) = FLegajo then begin
            NuevoLegajo := False;
        	break;
        end;
    end;
    if NuevoLegajo then begin
    	CantidadLegajos := CantidadLegajos + 1;
        if (CantidadLegajos > sgLegajosHoras.RowCount - 1) then begin
        	sgLegajosHoras.RowCount := CantidadLegajos + 1;
        end;
		sgLegajosHoras.Cells[0, CantidadLegajos] :=
            	Tabla.FieldByName('ApellidoNombre').AsString + Space(200)+
                IntToStr(FLegajo);
    end;
    btnEliminarLegajo.Enabled := True;
end;

procedure TFormTerminarTareaMantenimiento.btnEliminarLegajoClick(
  Sender: TObject);
var
	i : integer;
begin
	if sgLegajosHoras.Row = 0 then Exit;

	for i := sgLegajosHoras.Row to sgLegajosHoras.RowCount - 1 do begin
    	sgLegajosHoras.Cells[0, i] := sgLegajosHoras.Cells[0, i + 1];
    	sgLegajosHoras.Cells[1, i] := sgLegajosHoras.Cells[1, i + 1];
    end;

    if sgLegajosHoras.RowCount > 2 then sgLegajosHoras.RowCount := sgLegajosHoras.RowCount -1;
    CantidadLegajos :=  CantidadLegajos - 1;
    if CantidadLegajos = 0 then btnEliminarLegajo.Enabled := False;
end;

procedure TFormTerminarTareaMantenimiento.sgEquiposConsumoKeyPress(
  Sender: TObject; var Key: Char);
begin

	if not (Key in ConjuntoEquipos) then Key := #0;

    if sgEquiposConsumo.Col = 2 then begin
	    if ((Key = TimeSeparator) and (Pos(sgEquiposConsumo.Cells[sgEquiposConsumo.Col, sgEquiposConsumo.Row], TimeSeparator) > 0)) or
		   ((Length(Trim(sgEquiposConsumo.Cells[sgEquiposConsumo.Col, sgEquiposConsumo.Row])) > 4)
	       	and (Key <> char(VK_BACK)))	  then Key := #0;
	end;
    if sgEquiposConsumo.Col = 1 then begin
	    if ((Key = DecimalSeparator) and (Pos(sgEquiposConsumo.Cells[sgEquiposConsumo.Col, sgEquiposConsumo.Row], decimalSeparator) > 0)) then Key := #0;
	end;
end;

procedure TFormTerminarTareaMantenimiento.btnAgregarEquipoClick(
  Sender: TObject);
begin
	btEquipos.Activate;
end;

procedure TFormTerminarTareaMantenimiento.btnEliminarEquipoClick(
  Sender: TObject);
var
	i : integer;
begin
	if sgEquiposConsumo.Row = 0 then Exit;

	for i := sgEquiposConsumo.Row to sgEquiposConsumo.RowCount - 1 do begin
    	sgEquiposConsumo.Cells[0, i] := sgEquiposConsumo.Cells[0, i + 1];
    	sgEquiposConsumo.Cells[1, i] := sgEquiposConsumo.Cells[1, i + 1];
    	sgEquiposConsumo.Cells[2, i] := sgEquiposConsumo.Cells[2, i + 1];
    end;

    if sgEquiposConsumo.RowCount > 2 then sgEquiposConsumo.RowCount := sgEquiposConsumo.RowCount -1;
    CantidadEquipos :=  CantidadEquipos - 1;
    if CantidadEquipos = 0 then btnEliminarEquipo.Enabled := False;
end;

function TFormTerminarTareaMantenimiento.btEquiposProcess(Tabla: TDataSet;
  var Texto: String): Boolean;
begin
	Texto :=
    	Trim(Tabla.FieldByName('Descripcion').AsString) + Space(200) +
        Tabla.FieldByName('CodigoEquipo').AsString;
	Result := True;
end;

procedure TFormTerminarTareaMantenimiento.btEquiposSelect(Sender: TObject;
  Tabla: TDataSet);
var
	FEquipo, i: integer;
    NuevoEquipo: Boolean;
begin
	FEquipo := Tabla.FieldByName('CodigoEquipo').AsInteger;

	//Primero me fijo que este equipo ya no este en la lista.
    NuevoEquipo := True;
    for i := 1 to sgEquiposConsumo.RowCount - 1  do begin
    	if Ival(StrRight(sgEquiposConsumo.Cells[0, i], 10)) = FEquipo then begin
            NuevoEquipo := False;
        	break;
        end;
    end;
    if NuevoEquipo then begin
    	CantidadEquipos := CantidadEquipos + 1;
        if (CantidadEquipos > sgEquiposConsumo.RowCount - 1) then begin
        	sgEquiposConsumo.RowCount := CantidadEquipos + 1;
        end;
		sgEquiposConsumo.Cells[0, CantidadEquipos] :=
            	Tabla.FieldByName('Descripcion').AsString + Space(200)+
                IntToStr(FEquipo);
    end;
    //sgLegajosHoras.Selection := TGridRecT(sgLegajosHoras.CellRect(1, CantidadLegajos));
    btnEliminarEquipo.Enabled := True;
end;

procedure TFormTerminarTareaMantenimiento.sgLegajosHorasExit(
  Sender: TObject);
var
	i: integer;
begin
	if CantidadLegajos = 0 then Exit;
	for i := 1 to sgLegajosHoras.RowCount do begin
    	sgLEgajosHoras.Cells[1, i] := FormatearHora(sgLEgajosHoras.Cells[1, i]);
    end;
end;

procedure TFormTerminarTareaMantenimiento.sgLegajosHorasSelectCell(
  Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
begin
	sgLegajosHoras.Cells[sgLegajosHoras.Col, sgLegajosHoras.Row] := FormatearHora(sgLegajosHoras.Cells[sgLegajosHoras.Col, sgLegajosHoras.Row]);
end;

procedure TFormTerminarTareaMantenimiento.sgEquiposConsumoExit(
  Sender: TObject);
var
	i: integer;
begin
	if CantidadEquipos = 0 then Exit;
	for i := 1 to sgEquiposConsumo.RowCount do begin
    	sgEquiposConsumo.Cells[2, i] := FormatearHora(sgEquiposConsumo.Cells[2, i]);
    end;
end;

procedure TFormTerminarTareaMantenimiento.sgEquiposConsumoSelectCell(
  Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
begin
	if sgEquiposConsumo.Col = 2 then
		 sgEquiposConsumo.Cells[ sgEquiposConsumo.Col,  sgEquiposConsumo.Row] := FormatearHora( sgEquiposConsumo.Cells[ sgEquiposConsumo.Col,  sgEquiposConsumo.Row]);
end;

end.
