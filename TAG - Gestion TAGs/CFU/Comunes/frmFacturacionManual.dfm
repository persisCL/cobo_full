object FacturacionManualForm: TFacturacionManualForm
  Left = 177
  Top = 114
  Anchors = []
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = ' Facturaci'#243'n de un Convenio Especifico '
  ClientHeight = 603
  ClientWidth = 640
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox4: TGroupBox
    Left = 0
    Top = 0
    Width = 640
    Height = 352
    Align = alClient
    Caption = ' Datos del Comprobante '
    TabOrder = 0
    object Label1: TLabel
      Left = 33
      Top = 22
      Width = 78
      Height = 13
      Caption = 'RUT del Cliente:'
    end
    object Label3: TLabel
      Left = 6
      Top = 46
      Width = 105
      Height = 13
      Caption = 'Convenios del Cliente:'
    end
    object Label2: TLabel
      Left = 35
      Top = 75
      Width = 76
      Height = 13
      Caption = 'Fecha de Corte:'
    end
    object Label4: TLabel
      Left = 214
      Top = 78
      Width = 87
      Height = 13
      Caption = 'Fecha de Emisi'#243'n:'
    end
    object Label5: TLabel
      Left = 409
      Top = 78
      Width = 109
      Height = 13
      Caption = 'Fecha de Vencimiento:'
    end
    object lMedioPago: TLabel
      Left = 158
      Top = 176
      Width = 72
      Height = 13
      Caption = 'Medio de Pago'
    end
    object Label6: TLabel
      Left = 7
      Top = 176
      Width = 131
      Height = 13
      Caption = 'Medio de Pago Autom'#225'tico:'
    end
    object Bevel1: TBevel
      Left = 3
      Top = 162
      Width = 617
      Height = 18
      Shape = bsTopLine
    end
    object Label7: TLabel
      Left = 7
      Top = 195
      Width = 122
      Height = 13
      Caption = 'Detalle del Comprobante: '
    end
    object Label8: TLabel
      Left = 347
      Top = 22
      Width = 103
      Height = 13
      Caption = 'N'#250'mero de Convenio:'
    end
    object Label9: TLabel
      Left = 296
      Top = 129
      Width = 144
      Height = 13
      Caption = 'Tipo de Documento deseado: '
    end
    object lblEnListaAmarilla: TLabel
      Left = 472
      Top = 45
      Width = 148
      Height = 14
      Alignment = taCenter
      AutoSize = False
      Color = clYellow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      Transparent = False
      Visible = False
    end
    object peRUTCliente: TPickEdit
      Left = 117
      Top = 18
      Width = 139
      Height = 21
      CharCase = ecUpperCase
      Color = 16444382
      Enabled = True
      TabOrder = 0
      OnChange = peRUTClienteChange
      EditorStyle = bteTextEdit
      OnButtonClick = peRUTClienteButtonClick
    end
    object deFechaCorte: TDateEdit
      Left = 117
      Top = 74
      Width = 92
      Height = 21
      AutoSelect = False
      Color = 16444382
      TabOrder = 3
      OnChange = deFechaCorteChange
      OnExit = deFechaCorteExit
      Date = -693594.000000000000000000
    end
    object deFechaEmision: TDateEdit
      Left = 307
      Top = 74
      Width = 92
      Height = 21
      AutoSelect = False
      Color = 16444382
      TabOrder = 4
      OnChange = deFechaEmisionChange
      OnExit = deFechaEmisionExit
      Date = -693594.000000000000000000
    end
    object deFechaVencimiento: TDateEdit
      Left = 526
      Top = 74
      Width = 92
      Height = 21
      AutoSelect = False
      Color = 16444382
      TabOrder = 5
      OnExit = deFechaVencimientoExit
      Date = -693594.000000000000000000
    end
    object cbConveniosCliente: TVariantComboBox
      Left = 117
      Top = 45
      Width = 282
      Height = 19
      Style = vcsOwnerDrawFixed
      Color = 16444382
      ItemHeight = 13
      TabOrder = 2
      OnChange = cbConveniosClienteChange
      OnDrawItem = cbConveniosClienteDrawItem
      Items = <>
    end
    object DBListEx1: TDBListEx
      Left = 3
      Top = 214
      Width = 609
      Height = 121
      BorderStyle = bsSingle
      Columns = <
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Width = 56
          Header.Caption = 'Facturar'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Seleccionado'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 280
          Header.Caption = 'Descripci'#243'n'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'Descripcion'
        end
        item
          Alignment = taLeftJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 120
          Header.Caption = 'Importe'
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -12
          Header.Font.Name = 'Tahoma'
          Header.Font.Style = []
          IsLink = False
          FieldName = 'IMPORTE'
        end>
      DataSource = dsInformeFactura
      DragReorder = True
      ParentColor = False
      PopupMenu = PopFacturacionManual
      TabOrder = 6
      TabStop = True
      OnDblClick = DBListEx1DblClick
      OnDrawText = DBListEx1DrawText
      OnKeyPress = DBListEx1KeyPress
    end
    object NumeroConvenio: TEdit
      Left = 456
      Top = 18
      Width = 160
      Height = 21
      Color = 16444382
      TabOrder = 7
      OnChange = NumeroConvenioChange
    end
    object chkFacturarCompleto: TCheckBox
      Left = 14
      Top = 114
      Width = 264
      Height = 17
      Caption = 'Facturar el convenio completo, sin ajuste sencillo'
      TabOrder = 8
    end
    object chkFacturarSinIntereses: TCheckBox
      Left = 14
      Top = 137
      Width = 169
      Height = 17
      Caption = 'Facturar sin generar intereses'
      TabOrder = 9
    end
    object vcbTipoDocumentoDeseado: TVariantComboBox
      Left = 448
      Top = 125
      Width = 170
      Height = 21
      Style = vcsDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 10
      Items = <>
    end
    object btnBuscar: TButton
      Left = 262
      Top = 16
      Width = 75
      Height = 25
      Caption = 'Buscar'
      TabOrder = 1
      OnClick = btnBuscarClick
    end
  end
  object GroupBox5: TGroupBox
    Left = 0
    Top = 442
    Width = 640
    Height = 120
    Align = alBottom
    Caption = ' Datos del Cliente '
    TabOrder = 1
    object lNombreCompleto: TLabel
      Left = 81
      Top = 24
      Width = 83
      Height = 13
      Caption = 'lNombreCompleto'
    end
    object lDomicilio: TLabel
      Left = 81
      Top = 48
      Width = 44
      Height = 13
      Caption = 'lDomicilio'
    end
    object lComuna: TLabel
      Left = 81
      Top = 72
      Width = 41
      Height = 13
      Caption = 'lComuna'
    end
    object lRegion: TLabel
      Left = 81
      Top = 96
      Width = 36
      Height = 13
      Caption = 'lRegion'
    end
    object Label31: TLabel
      Left = 14
      Top = 24
      Width = 43
      Height = 13
      AutoSize = False
      Caption = 'Apellido:'
    end
    object Label33: TLabel
      Left = 14
      Top = 48
      Width = 59
      Height = 13
      AutoSize = False
      Caption = 'Domicilio:'
    end
    object Label34: TLabel
      Left = 14
      Top = 72
      Width = 43
      Height = 13
      AutoSize = False
      Caption = 'Comuna:'
    end
    object Label35: TLabel
      Left = 14
      Top = 96
      Width = 43
      Height = 13
      AutoSize = False
      Caption = 'Regi'#243'n:'
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 352
    Width = 640
    Height = 90
    Align = alBottom
    Caption = ' Domicilio del Comprobante '
    TabOrder = 2
    object lDomicilioFacturacion: TLabel
      Left = 81
      Top = 20
      Width = 44
      Height = 13
      Caption = 'lDomicilio'
    end
    object lComunaFacturacion: TLabel
      Left = 81
      Top = 44
      Width = 41
      Height = 13
      Caption = 'lComuna'
    end
    object lRegionFacturacion: TLabel
      Left = 81
      Top = 68
      Width = 36
      Height = 13
      Caption = 'lRegion'
    end
    object Label12: TLabel
      Left = 14
      Top = 20
      Width = 59
      Height = 13
      AutoSize = False
      Caption = 'Domicilio:'
    end
    object Label13: TLabel
      Left = 14
      Top = 44
      Width = 43
      Height = 13
      AutoSize = False
      Caption = 'Comuna:'
    end
    object Label14: TLabel
      Left = 14
      Top = 68
      Width = 43
      Height = 13
      AutoSize = False
      Caption = 'Regi'#243'n:'
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 562
    Width = 640
    Height = 41
    Align = alBottom
    TabOrder = 3
    DesignSize = (
      640
      41)
    object btnFacturar: TButton
      Left = 426
      Top = 6
      Width = 102
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = '&Facturar'
      TabOrder = 0
      OnClick = btnFacturarClick
    end
    object btnSalir: TButton
      Left = 532
      Top = 6
      Width = 102
      Height = 25
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = '&Salir'
      TabOrder = 1
      OnClick = btnSalirClick
    end
  end
  object ObtenerCLiente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    CommandTimeout = 420
    ProcedureName = 'ObtenerCliente'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@NumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 11
        Value = Null
      end>
    Left = 222
    Top = 412
  end
  object ObtenerDatosMedioPagoAutomaticoConvenio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    CommandTimeout = 420
    ProcedureName = 'ObtenerDatosMedioPagoAutomaticoConvenio'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 254
    Top = 412
  end
  object GenerarMovimientosTransitos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CommandTimeout = 420
    ProcedureName = 'GenerarMovimientosTransitos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ConvenioAnterior'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@GrupoFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaCorte'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaEmision'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@FechaVencimiento'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@NumeroProcesoFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NoGenerarAjusteSencillo'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@NoGenerarIntereses'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@ConveniosProcesados'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@DescripcionError'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 255
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 320
    Top = 360
  end
  object ObtenerUltimosConsumos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    CommandTimeout = 420
    Parameters = <>
    Left = 160
    Top = 208
  end
  object dsInformeFactura: TDataSource
    DataSet = cdConceptos
    Left = 128
    Top = 208
  end
  object spObtenerComprobantePreFacturado: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    CommandTimeout = 420
    ProcedureName = 'ObtenerComprobantePreFacturado'
    Parameters = <>
    Left = 197
    Top = 320
  end
  object spObtenerConveniosCliente: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    CommandTimeout = 420
    ProcedureName = 'ObtenerConveniosCliente'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 235
    Top = 320
  end
  object spObtenerDomicilioFacturacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CommandTimeout = 420
    ProcedureName = 'ObtenerDomicilioFacturacion'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Domicilio'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 255
        Value = Null
      end
      item
        Name = '@Comuna'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = Null
      end
      item
        Name = '@Region'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = Null
      end>
    Left = 454
    Top = 360
  end
  object spObtenerDatosFacturacionDetallada: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    CommandTimeout = 420
    ProcedureName = 'ObtenerDatosFacturacionDetallada'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end>
    Left = 420
    Top = 360
  end
  object spCrearProcesoFacturacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CommandTimeout = 420
    ProcedureName = 'CrearProcesoFacturacion'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Operador'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@FacturacionManual'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@NumeroProcesoFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 209
    Top = 360
  end
  object ilImages: TImageList
    Left = 128
    Top = 240
    Bitmap = {
      494C010102000400BC0010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000F0FBFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000080808000984E0000984E0000984E0000984E0000984E0000984E0000984E
      000080808000F0FBFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000080808000F0FBFF000000000000000000984E000000000000000000000000
      000080808000F0FBFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F0FBFF00C0C0C000C0C0C000C0C0
      C00000808000C0C0C00080808000808080008080800080808000C0C0C0000000
      000080808000F0FBFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C0C0C00000808000008080000080
      8000008080000080800000808000008080000080800099A8AC00808080000000
      000080808000F0FBFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C0C0C0000080800000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000080800080808000808080000000
      000080808000F0FBFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C0C0C0000080800000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000808000984E0000000000000000
      000080808000F0FBFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C0C0C0000080800000FFFF000080
      800000808000008080000080800000FFFF0000808000C0C0C000984E00000000
      000080808000F0FBFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C0C0C0000080800000FFFF0000FF
      FF0000FFFF00008080000080800000FFFF000080800080808000984E00000000
      000080808000F0FBFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C0C0C0000080800000FFFF0000FF
      FF00008080000080800000FFFF0000FFFF0000808000C0C0C000FFFFFF000000
      000080808000F0FBFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C0C0C0000080800000FFFF000080
      800000808000008080000080800000FFFF0000808000C0C0C000000000000000
      000080808000F0FBFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C0C0C0000080800000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000808000C0C0C000F0FBFF00F0FB
      FF0080808000F0FBFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C0C0C0000080800000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000080800080808000808080008080
      800080808000F0FBFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C0C0C0000080800000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000808000C0C0C000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C0C0C00000808000008080000080
      8000008080000080800000808000008080000080800099A8AC00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F0FBFF0099A8AC0099A8AC0099A8
      AC0099A8AC0099A8AC0099A8AC0099A8AC0099A8AC00F0FBFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00F003FFFF00000000F003FFFF00000000
      F373FFFF000000000013FFFF000000000013FFFF000000000013FFFF00000000
      0033FFFF000000000013FFFF000000000013FFFF000000000013FFFF00000000
      0033FFFF000000000003FFFF000000000003FFFF00000000003FFFFF00000000
      003FFFFF00000000003FFFFF0000000000000000000000000000000000000000
      000000000000}
  end
  object PopFacturacionManual: TPopupMenu
    OnPopup = PopFacturacionManualPopup
    Left = 248
    Top = 248
    object MnuSeleccionarTodo: TMenuItem
      Caption = 'Seleccionar Todo'
      Hint = 'Selecciona todos los conceptos de la grilla'
      OnClick = MnuSeleccionarTodoClick
    end
    object MnuDeSeleccionarTodo: TMenuItem
      Caption = 'Deseleccionar Todo'
      Hint = 'Remueve todas las selecciones de la grilla'
      OnClick = MnuDeSeleccionarTodoClick
    end
  end
  object SpCargarConceptosAFacturarFacturadorManual: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CargarConceptosAFacturarFacturadorManual;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroProcesoFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConcepto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 239
    Top = 360
  end
  object SPActualizarFechaHoraFinProcesoFacturacion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarFechaHoraFinProcesoFacturacion;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroProcesoFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 516
    Top = 320
  end
  object spActualizarFechaImpresionComprobantes: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarFechaImpresionComprobantes;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 546
    Top = 320
  end
  object spGenerarTablaTrabajoInteresesPorFacturar: TADOStoredProc
    AutoCalcFields = False
    Connection = DMConnections.BaseCAC
    CommandTimeout = 0
    ProcedureName = 'GenerarTablaTrabajoInteresesPorFacturar'
    Parameters = <
      item
        Name = '@CodigoGrupoFacturacion'
        DataType = ftSmallint
        Value = Null
      end
      item
        Name = '@NumeroProcesoFacturacion'
        DataType = ftInteger
        Value = Null
      end
      item
        Name = '@FechaEmision'
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        DataType = ftInteger
        Value = Null
      end>
    Left = 272
    Top = 360
  end
  object spLlenarDetalleConsumoComprobantes: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CommandTimeout = 0
    ProcedureName = 'LlenarDetalleConsumoComprobantes;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroProcesoFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 360
    Top = 360
  end
  object spTipificarYNumerarComprobante: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'TipificarYNumerarComprobante;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@DescripcionError'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = Null
      end
      item
        Name = '@TipoComprobanteFiscal'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobanteFiscal'
        Attributes = [paNullable]
        DataType = ftLargeint
        Direction = pdInputOutput
        Precision = 19
        Value = Null
      end>
    Left = 344
    Top = 472
  end
  object spActualizarSaldoConvenio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarSaldoConvenio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@SaldoComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end>
    Left = 376
    Top = 472
  end
  object spReaplicarConvenio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ReaplicarConvenio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 408
    Top = 472
  end
  object spInsertarNumeroProcesoFacturacionEnColaEnvioDBNet: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'InsertarNumeroProcesoFacturacionEnColaEnvioDBNet'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroProcesoFacturacion'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 440
    Top = 472
  end
  object cdConceptos: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Seleccionado'
        DataType = ftBoolean
      end
      item
        Name = 'Descripcion'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'Importe'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CodigoConcepto'
        DataType = ftInteger
      end
      item
        Name = 'AfectaFechaCorte'
        DataType = ftBoolean
      end
      item
        Name = 'FacturacionInmediata'
        DataType = ftBoolean
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 160
    Top = 240
  end
  object spCobrar_o_DescontarReimpresion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'Cobrar_o_DescontarReimpresion'
    Parameters = <>
    Left = 472
    Top = 472
  end
  object spGenerarMovimientosInteres: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'GenerarMovimientosInteres'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaEmision'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@CodigoConvenio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 312
    Top = 472
  end
  object spActualizarInfraccionesFacturadas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CommandTimeout = 120
    ProcedureName = 'ActualizarInfraccionesFacturadas;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 424
    Top = 264
  end
  object spLlenarDetalleConsumoComprobantesMorosos: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CommandTimeout = 0
    ProcedureName = 'LlenarDetalleConsumoComprobantesMorosos;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroProcesoFacturacion'
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 311
    Top = 510
  end
  object spObtenerCantTransitosPorInfraccion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerCantTransitosPorInfraccion;1'
    Parameters = <>
    Left = 343
    Top = 510
  end
end
