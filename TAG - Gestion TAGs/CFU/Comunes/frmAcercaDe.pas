{------------------------------------------
                frmAcercaDe
                
    Author      :   mbecerra
    Date        :   23-12-2013
    Description :   Nueva Ventana que despliega información de Chile, en vez de DPS o Argentina.

    Firma       : SS_1147_NDR_20141216
    Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

}
unit frmAcercaDe;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, LMDPNGImage, ExtCtrls, StdCtrls, ShellAPI, Util;

type
  TAcercaDeForm = class(TForm)
    imgLogoKapsch: TImage;
    lblNombre: TLabel;
    lblDireccion: TLabel;
    lblURL: TLabel;
    lblFono: TLabel;
    btnOk: TButton;
    procedure lblURLClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
  public
    { Public declarations }
  end;

var
  AcercaDeForm: TAcercaDeForm;

implementation

{$R *.dfm}

procedure TAcercaDeForm.btnOkClick(Sender: TObject);
begin
    Close;
end;

procedure TAcercaDeForm.FormCreate(Sender: TObject);
begin
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216
end;

procedure TAcercaDeForm.lblURLClick(Sender: TObject);
begin
    with (Sender as TLabel) do begin
        ShellExecute(Application.Handle, PChar('open'),  PChar(Caption), PChar(0), nil, SW_NORMAL);
    end;
end;

end.
