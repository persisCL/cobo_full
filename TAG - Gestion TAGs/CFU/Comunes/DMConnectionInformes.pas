{********************************** Unit Header ********************************
File Name : DMConnectionInformes.pas
Author : llazarte
Date Created: 26/12/2005
Language : ES-AR
Description : Arma la cadena de conexi�n de acuerdo a la base de datos del reporte solicitado

Revision 1
Author:
Date:
Description: SS 744
*******************************************************************************}


unit DMConnectionInformes;

interface

uses
  SysUtils, Classes, ADODB, DB, Util, Forms, UtilProc, Windows;

const	// Rev. 1 (SS 744)
    CONST_USUARIO_INFORMES_STANDARD = 'usr_informes';	// Rev. 1 (SS 744)
  
type
  TDMConnInformes = class(TDataModule)
    BaseInformes: TADOConnection;
  private
    { Private declarations }
  public
    { Public declarations }
    function ConectarServer(Server, DataBase: string; UserName, Password: string): boolean;	// Rev. 1 (SS 744)
  end;

var
  DMConnInformes: TDMConnInformes;

implementation

{$R *.dfm}

{ TDMConnInformes }

{******************************** Function Header ******************************
Function Name: Conectar
Author : llazarte
Date Created : 26/12/2005
Description : arma la cadena de conexi�n de acuerdo al servidor y base de datos enviados
como parametros
Parameters : Server, DataBase: string
Return Value : boolean
*******************************************************************************}

function TDMConnInformes.ConectarServer(Server, DataBase: string; UserName, Password: string): boolean;
resourcestring
    MSG_ERROR_CONNECTION_INFORMES = 'Error de conexi�n con la base de datos';
    TXT_ERROR = 'Error';
var	// Rev. 1 (SS 744)
    Autenticacion: string;	// Rev. 1 (SS 744)
begin
    Result := False;
    BaseInformes.Connected        := False;
	BaseInformes.KeepConnection   := True;
	// Rev. 1 (SS 744)
    Autenticacion := UserName;
    if Password <> EmptyStr then
         Autenticacion := Autenticacion + '; Password=' + Password;
    Autenticacion := Autenticacion + ';';    
	// Fin Rev. 1 (SS 744)
	BaseInformes.ConnectionString :=
	  'Provider=SQLOLEDB.1;User ID='+ Autenticacion + // Rev. 1 (SS 744)
	  'Initial Catalog=' + DataBase + ';' +
	  'Data Source=' +
		Server + ';' +
	  'Application Name=' + Application.Title + ';' +
	  'WorkStation ID='   + GetMachineName();
    try
        BaseInformes.Open;
        Result := True;
    except
        on e: Exception do begin
            MsgBoxErr(MSG_ERROR_CONNECTION_INFORMES, e.Message, TXT_ERROR, MB_ICONEXCLAMATION);
        end;
    end;
end;

end.
