unit PagoVentanilla;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, VariantComboBox, ADODB, DMConnection, ExtCtrls,
  Validate, DateEdit, DmiCtrls, Buttons, ComCtrls, Util, UtilProc, DB,
  PeaProcs, PeaTypes, UtilDB, ConstParametrosGenerales, RStrings, UtilFacturacion,
  ListBoxEx, DBListEx, FrmAgregarCheque, DBClient, Mask, TimeEdit, SysUtilsCN, XMLDoc, XMLIntf, Diccionario;

type
  TFormaPago = (tpEfectivo, tpTarjetaCredito, tpTarjetaDebito, tpCheque, tpValeVista, tpDebitoCuenta, tpPagare, tpAvenimiento);

  TfrmPagoVentanilla = class(TForm)
    Bevel1: TBevel;
    Bevel2: TBevel;
    btn_DatosClienteValeVista: TSpeedButton;
    btnAceptar: TButton;
    BtnAgregarItem: TButton;
    btnCerrar: TButton;
    btnDatosClienteCheque: TSpeedButton;
    btnDatosClientePagare: TSpeedButton;
    BtnEliminarItem: TButton;
  	cb_BancosPAC: TVariantComboBox;
    cb_BancosVV: TVariantComboBox;
    cb_CanalesDePago: TVariantComboBox;
    cb_EmisorTarjetaCreditoPAT: TVariantComboBox;
    cb_TipoTarjetaCreditoPAT: TVariantComboBox;
    cbChequeBancos: TVariantComboBox;
    cbPagareBancos: TVariantComboBox;
    cdsCheques: TClientDataSet;
    CerrarPagoComprobante: TADOStoredProc;
    CrearRecibo: TADOStoredProc;
    DBItemsCheque: TDBListEx;
    de_FechaVV: TDateEdit;
    deChequeFecha: TDateEdit;
    deFechaPago: TDateEdit;
    dePagareFecha: TDateEdit;
    DsCheques: TDataSource;
    edAutorizacion: TEdit;
    edCuotas: TNumericEdit;
    edCupon: TNumericEdit;
    lbl_CanalDePago: TLabel;
    lbl_FormaPago: TLabel;
    lbl_Moneda: TLabel;
    lbl_MontoPagado: TLabel;
    lblaca: TLabel;
    lblAcaMonto: TLabel;
    lblChequeBanco: TLabel;
    lblChequeFecha: TLabel;
    lblChequeNumero: TLabel;
    lblChequeRUT: TLabel;
    lblChequeTitular: TLabel;
    lblChequeTitulo: TLabel;
    lblDebitoCuentaPACBanco: TLabel;
    lblDebitoCuentaPACNumeroCuenta: TLabel;
    lblDebitoCuentaPACSucursal: TLabel;
    lblDebitoCuentaPACTitular: TLabel;
    lblDebitoCuentaPACTitulo: TLabel;
    lblDebitoCuentaPATEmisorTarjeta: TLabel;
    lblDebitoCuentaPATFechaVencimiento: TLabel;
    lblDebitoCuentaPATNumeroTarjeta: TLabel;
    lblDebitoCuentaPATTipoTarjeta: TLabel;
    lblDebitoCuentaPATTitulo: TLabel;
    lblEfectivoMontoRecibido: TLabel;
    lblEfectivoTitulo: TLabel;
    lblEfectivoVuelto: TLabel;
    lblFechaDePago: TLabel;
    lblPagareBanco: TLabel;
    lblPagareFecha: TLabel;
    lblPagareNumero: TLabel;
    lblPagareRUT: TLabel;
    lblPagareTitular: TLabel;
    lblPagareTitulo: TLabel;
    lblSiDeseaCambiarElMontoPagado: TLabel;
    lblSiDeseaCambiarFechaPago: TLabel;
    lblSiDeseaIncluirUnCheque: TLabel;
    lblTarjetaAutorizacion: TLabel;
    lblTarjetaCuotas: TLabel;
    lblTarjetaCupon: TLabel;
    lblTarjetaTitulo: TLabel;
    lbltitulototal: TLabel;
    lblTotal: TLabel;
    lblValeVistaBanco: TLabel;
    lblValeVistaFecha: TLabel;
    lblValeVistaNumero: TLabel;
    lblValeVistaRUT: TLabel;
    lblValeVistaTitulo: TLabel;
    lblValeVistaTomadoPor: TLabel;
    nbOpciones: TNotebook;
    neCambio: TNumericEdit;
    neImporte: TNumericEdit;
    neImportePagado: TNumericEdit;
    nePagacon: TNumericEdit;
    RegistrarPagoNotaCobroInfraccion: TADOStoredProc;
    sp_RegistrarDetallePagoComprobante: TADOStoredProc;
    sp_RegistrarPagoComprobante: TADOStoredProc;
    txt_NroCuentaBancariaPAC: TEdit;
    txt_NroVV: TEdit;
    txt_NumeroTarjetaCreditoPAT: TEdit;
    txt_RUTVV: TEdit;
    txt_SucursalPAC: TEdit;
    txt_TitularPAC: TEdit;
    txt_TomadoPorVV: TEdit;
    txtChequeDocumento: TEdit;
    txtChequeNombreTitular: TEdit;
    txtChequeNumero: TEdit;
    txtPagareDocumento: TEdit;
    txtPagareNombreTitular: TEdit;
    txtPagareNumero: TEdit;
    txt_FechaVencimiento: TMaskEdit;
    lblTarjetaCreditoTitulo: TLabel;
    edtTarjetaCreditoTitular: TEdit;
    spbTarjetaCreditoCopiarDatosCliente: TSpeedButton;
    lblTarjetaCreditoTitular: TLabel;
    vcbTarjetaCreditoTarjetas: TVariantComboBox;
    lblTarjetaCreditoTarjeta: TLabel;
    edtTarjetaCreditoNroConfirmacion: TEdit;
    lblTarjetaCreditoNroConfirmacion: TLabel;
    vcbTarjetaCreditoBancos: TVariantComboBox;
    lblTarjetaCreditoBanco: TLabel;
    nedtTarjetaCreditoCuotas: TNumericEdit;
    lblTarjetaCreditoCuotas: TLabel;
    lblTarjetaDebitoTitulo: TLabel;
    lblTarjetaDebitoTitular: TLabel;
    lblTarjetaDebitoBanco: TLabel;
    lblTarjetaDebitoNroConfirmacion: TLabel;
    edtTarjetaDebitoTitular: TEdit;
    spbTarjetaDebitoCopiarDatosCliente: TSpeedButton;
    edtTarjetaDebitoNroConfirmacion: TEdit;
    vcbTarjetaDebitoBancos: TVariantComboBox;
    edtCodigoFormaPago: TEdit;
    edtDescFormaPago: TEdit;
    spValidarFormaDePagoEnCanal: TADOStoredProc;
    Label1: TLabel;
    edtRepactacion: TEdit;
    Label9: TLabel;
    edtRepactacionCheque: TEdit;
    Label2: TLabel;
    spActualizarInfraccionesPagadas: TADOStoredProc;
    chkNoCalcularIntereses: TCheckBox;
    ADOStoredProc1: TADOStoredProc;
    sp_ActualizarClienteMoroso: TADOStoredProc;
    procedure btn_DatosClienteValeVistaClick(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
    procedure BtnAgregarItemClick(Sender: TObject);
    procedure btnCerrarClick(Sender: TObject);
    procedure btnDatosClienteChequeClick(Sender: TObject);
    procedure btnDatosClientePagareClick(Sender: TObject);
    procedure BtnEliminarItemClick(Sender: TObject);
    procedure cb_CanalesDePagoChange(Sender: TObject);
    procedure cb_FormasPagoChange(Sender: TObject);
    procedure deFechaPagoChange(Sender: TObject);
    procedure lblacaClick(Sender: TObject);
    procedure lblAcaMontoClick(Sender: TObject);
    procedure neImporteKeyPress(Sender: TObject; var Key: Char);
    procedure neImportePagadoChange(Sender: TObject);
    procedure nePagaconChange(Sender: TObject);
    procedure spbTarjetaCreditoCopiarDatosClienteClick(Sender: TObject);
    procedure edtTarjetaCreditoNroConfirmacionKeyPress(Sender: TObject; var Key: Char);
    procedure spbTarjetaDebitoCopiarDatosClienteClick(Sender: TObject);
    procedure edtCodigoFormaPagoChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure chkNoCalcularInteresesClick(Sender: TObject);
  private
    FCodigoBanco: Integer;
    FCodigoCanalPago: Integer;
    FCodigoFormaPago: Integer;
    FCodigoPantalla,
    FCodigoTarjeta: Integer;
    FComprobantesCobrar: TListaComprobante;
    FFechamaximaCheque: TDate;
    FFechaMinimaCheque: TDate;
    FFechaMaximaPagare: TDate;
    FFechaMinimaPagare: TDate;
    FFormaPagoActual: TFormaPago;
    FImporte, FImportePagado: Double;
    FImporteComprobantesPagados: Int64;
    FImprimirRecibo : boolean;
    FNumeroPOS, FPuntoEntrega, FPuntoVenta: Integer;
    FNumeroRecibo: Int64;
    FPagoNotaCobroInfraccion: Boolean;
    FRUT : string;
    FTitular : string;
    FFechaHoraEmision : TDateTime;
    FNoCalcularIntereses: Boolean;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;
  	function CalcularImportePagado(i: Integer): Int64;
  	function CanalDePagoCargado(CodigoCanalPago: Byte): Boolean;
  	function CargarBancos(Combo: TVariantComboBox): Boolean;
    function GenerarPago(NumeroRecibo: Integer; FormaPago: TFormaPago; var MensajeError: String): Boolean;
    function GenerarPagoMultiplesCheques(NumeroRecibo: Integer; FormaPago: TFormaPago; var MensajeError: String): Boolean;
  	function GenerarRecibo(var NumeroRecibo: Int64): Boolean;
  	function HabilitarModificarMontoPagado: Boolean;
  	function ObtenerImporte(Comprobantes: TListaComprobante): Double;
    Function ObtenerImporteCheques : INT64;
  	function RegistrarPago(FormaPago: TFormaPago; NumeroRecibo: Integer; var MensajeError: String): Boolean;
  	function ValidarDatos(FormaPago: TFormaPago):Boolean;
  	function ValidarDatosTurno: Boolean;
    Function ValidarImporteCheques : Boolean;
  	procedure CerrarRegistroPagoComprobante(TipoComprobante: AnsiString; NumeroComprobante: Int64; FechaPago: TDateTime);
  	procedure FiltrarCanalesDePago;
  	procedure MostrarDatosPACComprobante(NumeroComprobante: Integer; TipoComprobante: AnsiString);
  	procedure MostrarDatosPATComprobante(NumeroComprobante: Integer; TipoComprobante: AnsiString);
  public
    function ImprimirRecibo : Boolean;
    function Inicializar(ComprobantesCobrar: TListaComprobante; RUT, Titular: String; NumeroPOS, PuntoEntrega, PuntoVenta, CodigoCanalDePago: Integer; PagoNotaCobroInfraccion: Boolean = False): Boolean;
    function NumeroRecibo : int64;
    property CodigoCanalPago: Integer read FCodigoCanalPago;
    function GenerarPagoComprobante(FormaPago: TFormaPago; var MensajeError: String; var NumeroRecibo: Int64): Boolean;
    //function CrearObjetoPagoComprobante(FormaPago:TFormaPago): TPagoComprobante;
  end;

var
  frmPagoVentanilla: TfrmPagoVentanilla;

implementation

uses Contnrs;

{$R *.dfm}

function TfrmPagoVentanilla.Inicializar(ComprobantesCobrar: TListaComprobante; RUT, Titular: String; NumeroPOS, PuntoEntrega, PuntoVenta, CodigoCanalDePago: Integer; PagoNotaCobroInfraccion: Boolean = False): Boolean;
resourcestring
    MSG_POS = 'POSNET : ';
    MSG_LISTA_COMPROBANTES_VACIA = 'No hay comprobantes para cobrar.';
    MSG_COBRO_COMPROBANTES = 'Cobro de Comprobantes';
begin
    Result := True;

    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

    FPagoNotaCobroInfraccion := PagoNotaCobroInfraccion;
    FComprobantesCobrar := ComprobantesCobrar;
    if FComprobantesCobrar = nil then begin
        MsgBox(MSG_LISTA_COMPROBANTES_VACIA, Caption, MB_ICONSTOP);
        Result := False;
        Exit;
    end;

    FCodigoCanalPago := CodigoCanalDePago;
    FNumeroPOS := NumeroPOS;
    FPuntoEntrega := PuntoEntrega;
    FPuntoVenta := PuntoVenta;
	FImporteComprobantesPagados := 0;
    lblACA.Enabled := ExisteAcceso('modif_fecha_cobro');
    FNoCalcularIntereses := chkNoCalcularIntereses.Checked;                     //SS_295_437_NDR_20121030

    nbOpciones.PageIndex := CONST_PAGE_EFECTIVO;

//    Result := CargarBancos(cbChequeBancos)     // Rev. 3 (SS 809)
//                and CargarBancos(cb_BancosVV)
//                and CargarBancos(cbPagareBancos);

//    if not Result then Exit;

    try
        // Agregado en Revision 1
        //deFechaPago.Date    := NowBase(DMConnections.BaseCAC);                                    //SS_1006_1015_NDR_20121009
        deFechaPago.Date    := SysUtilsCN.NowBaseCN(DMConnections.BaseCAC);                         //SS_1006_1015_NDR_20121009

        FFechaHoraEmision   := TDate(deFechaPago.Date); //Revision 16
        deChequeFecha.Date  := deFechaPago.Date;
        de_FechaVV.Date     := deFechaPago.Date;
        dePagareFecha.Date  := deFechaPago.Date;

        if FCodigoCanalPago < 0 then FCodigoCanalPago := 0;
        CargarCanalesDePago(DMConnections.BaseCAC, cb_CanalesDePago, FCodigoCanalPago, 0); // SS_637_20101123
        FiltrarCanalesDePago;
        cb_CanalesDePagoChange(cb_CanalesDePago);

        //lblAcaMonto.Enabled := HabilitarModificarMontoPagado; // Rev. 2 (SS809)

//        CargarBancosPAC(DMConnections.BaseCAC, cb_BancosPAC);   // Rev. 3 (SS 809)
//        CargarTiposTarjetasCredito(DMConnections.BaseCAC, cb_TipoTarjetaCreditoPAT, 0, True);
//        CargarEmisoresTarjetasCredito(DMConnections.BaseCAC, cb_EmisorTarjetaCreditoPAT, 0, True);
    except
        Result := False;
    end; // except

    if Result then begin
        FNumeroRecibo := 0;
        FImprimirRecibo := False;
    	FRUT := RUT;
        FTitular := Titular;
        FImporte := ObtenerImporte(FComprobantesCobrar);
        neImporte.Value := FImporte;
        FImportePagado := FImporte;
        neImportePagado.Value := FImportePagado;

        FImprimirRecibo := False;
        // Comentado en Revision 1
        //        deFechaPago.Date := StrToDate(DateToStr(NowBase(DMConnections.BaseCAC)));
        //        deChequeFecha.Date := StrToDate(DateToStr(NowBase(DMConnections.BaseCAC)));
        //        de_FechaVV.Date := StrToDate(DateToStr(NowBase(DMConnections.BaseCAC)));
        //        dePagareFecha.Date := StrToDate(DateToStr(NowBase(DMConnections.BaseCAC)));

        if FComprobantesCobrar.Count > 1 then begin
            Caption := MSG_COBRO_COMPROBANTES;
        end else begin
            Caption := Caption + Space(1) + '-' + Space(1) +
                        DescriTipoComprobante( TComprobante(FComprobantesCobrar.Items[0]).TipoComprobante ) + ':' + Space (1) +
                        IntToStr(TComprobante(FComprobantesCobrar.Items[0]).NumeroComprobante);
        end;
        Caption := Caption + Space(1) + '-' + Space(1) + MSG_POS + IntToStr(NumeroPOS);
    end;
end;

procedure TfrmPagoVentanilla.cb_FormasPagoChange(Sender: TObject);
resourcestring
    MSG_ERROR_OBTENER_CODIGO_BANCO_PAGARE = 'No se ha podido obtener el banco por defecto para la forma de pago.'
                                                + CRLF + 'Deber� seleccionar otro banco.';
    MSG_ERROR_FORMA_NO_DEFINIDA = 'La forma de Pago especificada NO tiene definida su entrada de Datos.';
var
    MaxDias: Integer;
    MinDias: Integer;
    FFechaPago: TDateTime;
begin
    // FCodigoFormaPago := Integer(cb_FormasPago.Value); // Rev. 2 (SS 809)
    //neImportePagado.Value := FImporte;
    //lblAcaMonto.Enabled := HabilitarModificarMontoPagado; // Rev. 2 (SS 809)
    neImportePagado.Enabled := False; // SS 809

    case FCodigoPantalla of
        CONST_PAGE_TARJETA_DEBITO: begin
            FFormaPagoActual := tpTarjetaDebito;
            // Rev. 1 (SS 809)
            vcbTarjetaDebitoBancos.Clear;
            CargarBancosPagoVentanilla(DMConnections.BaseCAC,vcbTarjetaDebitoBancos,FCodigoBanco,FCodigoCanalPago,True);
            nbOpciones.PageIndex := CONST_PAGE_TARJETA_DEBITO;
            if Sender = edtCodigoFormaPago then ActiveControl := edtTarjetaDebitoTitular;
            // Fin Rev. 1 (SS 809)
        end;

        CONST_PAGE_TARJETA_CREDITO: begin
            FFormaPagoActual := tpTarjetaCredito;
            //lblAcaMonto.Enabled := False; // Rev. 2 (SS 809)
            // Rev. 1 (SS 809)
            vcbTarjetaCreditoTarjetas.Clear;
            CargarTiposTarjetasCreditoVentanilla(DMConnections.BaseCAC,vcbTarjetaCreditoTarjetas,FCodigoTarjeta,True,True);
            vcbTarjetaCreditoBancos.Clear;
            CargarBancosPagoVentanilla(DMConnections.BaseCAC,vcbTarjetaCreditoBancos,FCodigoBanco,FCodigoCanalPago,True);
            nbOpciones.PageIndex := CONST_PAGE_TARJETA_CREDITO;
            if Sender = edtCodigoFormaPago then ActiveControl := edtTarjetaCreditoTitular;
            // Fin Rev. 1 (SS 809)
        end;

        CONST_PAGE_DEBITO_CUENTA_PAC: begin
            FFormaPagoActual := tpDebitoCuenta;
            if FCodigoCanalPago = CONST_CANAL_PAGO_PAC then begin
                nbOpciones.PageIndex := CONST_PAGE_DEBITO_CUENTA_PAC;
                CargarBancosPAC(DMConnections.BaseCAC, cb_BancosPAC,FCodigoBanco,True);
                (* Mostrar los datos del comprobante. *)
                MostrarDatosPACComprobante(TComprobante(FComprobantesCobrar.Items[0]).NumeroComprobante, TComprobante(FComprobantesCobrar.Items[0]).TipoComprobante);
                if cb_BancosPAC.Value = 0 then cb_BancosPAC.ItemIndex := cb_BancosPAC.Items.IndexOfValue(FCodigoBanco);
                
                if Sender = edtCodigoFormaPago then ActiveControl := cb_BancosPAC;
            end else begin
                CargarTiposTarjetasCredito(DMConnections.BaseCAC, cb_TipoTarjetaCreditoPAT, FCodigoTarjeta, True);
                CargarEmisoresTarjetasCredito(DMConnections.BaseCAC, cb_EmisorTarjetaCreditoPAT, FCodigoBanco, True);
                nbOpciones.PageIndex := CONST_PAGE_DEBITO_CUENTA_PAT;
                (* Mostrar los datos del comprobante. *)
                MostrarDatosPATComprobante(TComprobante(FComprobantesCobrar.Items[0]).NumeroComprobante, TComprobante(FComprobantesCobrar.Items[0]).TipoComprobante);
                if cb_EmisorTarjetaCreditoPAT.Value = 0 then cb_EmisorTarjetaCreditoPAT.ItemIndex := cb_EmisorTarjetaCreditoPAT.Items.IndexOfValue(FCodigoBanco);
                if cb_TipoTarjetaCreditoPAT.Value = 0 then cb_TipoTarjetaCreditoPAT.ItemIndex := cb_TipoTarjetaCreditoPAT.Items.IndexOfValue(FCodigoTarjeta);

                if Sender = edtCodigoFormaPago then ActiveControl := cb_TipoTarjetaCreditoPAT;
            end;
        end;

        CONST_PAGE_CHEQUE: begin
            cbChequeBancos.Clear;
            CargarBancosPagoVentanilla(DMConnections.BaseCAC,cbChequeBancos,FCodigoBanco,FCodigoCanalPago,True);
            //Obtengo fecha maxima y minima para el cheque
            FFechaPago := Trunc(deFechaPago.Date);
            ObtenerParametroGeneral(DMConnections.BaseCAC, 'DIAS_FECHA_MINIMA_CHEQUE', MinDias);
            ObtenerParametroGeneral(DMConnections.BaseCAC, 'DIAS_FECHA_MAXIMA_CHEQUE', MaxDias);

            FFechaMaximaCheque := FFechaPago + MaxDias + 1;
            FFechaMinimaCheque := FFechaPago - MinDias;

            //Si no es un pago de una NI
            if not FPagoNotaCobroInfraccion then begin
                //Permitimos ingresar un solo cheque
                nbOpciones.PageIndex := CONST_PAGE_CHEQUE;
                FFormaPagoActual := tpCheque;
                if Sender = edtCodigoFormaPago then ActiveControl := txtChequeNombreTitular;
            //Si esta pagando un NI
            end else begin
                //Permitimos ingresar multiples cheques
                nbOpciones.PageIndex := CONST_PAGE_CHEQUE_NI;
                FFormaPagoActual := tpCheque;
            end;
        end;

        CONST_PAGE_EFECTIVO: begin
            nbOpciones.PageIndex := CONST_PAGE_EFECTIVO;
            ActiveControl := nePagacon;
            FFormaPagoActual := tpEfectivo;
        end;

        CONST_PAGE_VALE_VISTA: begin
            nbOpciones.PageIndex := CONST_PAGE_VALE_VISTA;
            FFormaPagoActual := tpValeVista;
            CargarBancosPagoVentanilla(DMConnections.BaseCAC,cb_BancosVV,FCodigoBanco,FCodigoCanalPago,True);
            FFechaPago := Trunc(de_FechaVV.Date);
            ObtenerParametroGeneral(DMConnections.BaseCAC, 'DIAS_FECHA_MINIMA_CHEQUE', MinDias);
            ObtenerParametroGeneral(DMConnections.BaseCAC, 'DIAS_FECHA_MAXIMA_CHEQUE', MaxDias);
            FFechaMaximaCheque := FFechaPago + MaxDias + 1;
            FFechaMinimaCheque := FFechaPago - MinDias;
            if Sender = edtCodigoFormaPago then ActiveControl := txt_TomadoPorVV;
        end;

        CONST_PAGE_PAGARE: begin
            nbOpciones.PageIndex := CONST_PAGE_PAGARE;
            FFormaPagoActual := tpPagare;
            cbPagareBancos.Clear;
            CargarBancosPagoVentanilla(DMConnections.BaseCAC,cbPagareBancos,FCodigoBanco,FCodigoCanalPago,True);
            txtPagareDocumento.Text := '';
            txtPagareNombreTitular.Text := '';
            txtPagareNumero.Text := '';
            edtRepactacion.Text := '';

            if Sender = edtCodigoFormaPago then ActiveControl := txtPagareNombreTitular;

            FFechaPago := Trunc(deFechaPago.Date);
            // Revision 15
            ObtenerParametroGeneral(DMConnections.BaseCAC, 'DIAS_FECHA_MINIMA_PAGARE', MinDias);
            ObtenerParametroGeneral(DMConnections.BaseCAC, 'DIAS_FECHA_MAXIMA_PAGARE', MaxDias);

            FFechaMaximaPagare := FFechaPago + MaxDias + 1;
            FFechaMinimaPagare := FFechaPago - MinDias;
        end;
    else
        nbOpciones.PageIndex := CONST_PAGE_NO_DEFINIDO;
        FCodigoFormaPago        := 0;
        edtCodigoFormaPago.Tag  := 0;
        lblAcaMonto.Enabled     := False;
        neImportePagado.Enabled := False;
        MsgBox(MSG_ERROR_FORMA_NO_DEFINIDA,Caption,MB_ICONSTOP);
    end;
end;

procedure TfrmPagoVentanilla.lblacaClick(Sender: TObject);
begin
    deFechaPago.Enabled := True;    //Revision 16
    lblAca.Enabled := False;
end;

function TfrmPagoVentanilla.CargarBancos(Combo: TVariantComboBox): Boolean;
var
    Qry:TADOQuery;
begin
    Result := True;

    Qry := TADOQuery.Create(nil);
    Qry.Connection := DMConnections.BaseCAC;
    (* Obtener los bancos que est�n habilitados para el Pago en Ventanilla. *)
    Qry.SQL.Add('SELECT CodigoBanco As Codigo, Descripcion As Descripcion'
        + ' From Bancos (nolock)'
		+ ' Where (Activo = 1) And (HabilitadoPagoVentanilla = 1)'
		+ ' Order By Descripcion');
    try
        try
            Qry.Open;
            if Qry.IsEmpty then begin
                Qry.Close;
                Exit;
            end;
            while not Qry.Eof do begin
                Combo.Items.Add(Qry.FieldByName('Descripcion').AsString, Qry.FieldByName('Codigo').AsInteger);
                Qry.Next;
            end;
            Combo.ItemIndex := 0;
            Combo.OnChange(Self);
        except
            Result := False;
        end;
    finally
        FreeAndNil(Qry);
    end;
end;

function TfrmPagoVentanilla.GenerarRecibo(var NumeroRecibo: Int64): Boolean;
resourcestring
    MSG_ERROR_RECIBO = 'Ha ocurrido un error creando el comprobante de cancelaci�n';
begin
    try
        // Creamos el recibo
        CrearRecibo.Parameters.ParamByName('@FechaHora').Value := FFechaHoraEmision; //Revision 16
        CrearRecibo.Parameters.ParamByName('@Importe').Value := FImportePagado * 100;
        CrearRecibo.Parameters.ParamByName('@NumeroRecibo').Value := Null;
        CrearRecibo.Parameters.ParamByName('@CodigoTipoMedioPago').Value := FCodigoCanalPago;
        // Revision 14
        CrearRecibo.Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;
        CrearRecibo.ExecProc;
        // Rev.24 / 20-Enero-2010 / Nelson Droguett Sierra ----------------------------------------------------
        if (CrearRecibo.Parameters.ParamByName('@RETURN_VALUE').Value <> -1) then begin
          NumeroRecibo := CrearRecibo.Parameters.ParamByName('@NumeroRecibo').Value;
          Result := True;
        end
        else
        begin
          NumeroRecibo:=-1;
          MsgBox('Error en la creaci�n del recibo', MSG_ERROR_RECIBO, MB_ICONERROR);
          Result := False;
        end;
        // -----------------------------------------------------------------------------------------------------
    except
        on e: Exception do begin
            raise Exception.Create(MSG_ERROR_RECIBO + CRLF + e.Message);
        end;
    end;
end;

procedure TfrmPagoVentanilla.btnAceptarClick(Sender: TObject);
resourcestring
    MSG_ERROR = 'Error. El Pago no ha sido registrado.';
    MSG_REGISTRO_CORRECTO = 'El pago se ha registrado correctamente';
    MSG_CONFIRMAR_RECIBO = 'Desea Imprimir el Comprobante de Cancelaci�n ?';
    MSG_CAPTION = 'Registrar Pago';
    MSG_CONFIRMAR_PAGO = 'Confirma registrar este pago ?';
    MSG_SELECCIONAR_CANAL_PAGO = 'Debe seleccionar un canal de pago.';
    MSG_SELECCIONAR_FORMA_PAGO = 'Debe indicar una forma de pago.';
    MSG_MONTO_PAGADO_MENOR = 'Se debe indicar un importe distinto de cero.';
    MSG_ERROR_PAGOS_FUTUROS = 'La fecha de pago no puede ser posterior a la fecha actual';                  //SS_1300_MCA_20150610
    MSG_ERROR_PAGOS_PASADOS = 'La fecha de pago no puede ser inferior a %d d�as.';                          //SS_1300_MCA_20150610
var
    NumeroRecibo: Int64;
    MensajeError: String;
    FFechaHoraEmision, FFechaActual : TDateTime;                                        //SS_1300_MCA_20150610
    DiasFechaMinimaPago     : integer;                                                  //SS_1300_MCA_20150610


begin
	//Rev.25 / 20-Agosto-2010 / Nelson Droguett Sierra
    try
        btnAceptar.Enabled := False;
        EmptyKeyQueue; // SS_914_20100921
        Application.ProcessMessages;

        (* Verificar que se haya seleccionado el Canal y la Forma de Pago. *)
        if not ValidateControls([cb_CanalesDePago, edtCodigoFormaPago, neImportePagado], // Rev. 2 (SS 809)
          [cb_CanalesDePago.ItemIndex >= 0, edtCodigoFormaPago.Tag = 1, (neImportePagado.Value <> 0)], // or (FComprobantesCobrar.Count = 1) or ((neImportePagado.enabled) and (FComprobantesCobrar.Count > 1) and(neImportePagado.ValueInt >= neImporte.ValueInt)))],
          MSG_CAPTION, [MSG_SELECCIONAR_CANAL_PAGO, MSG_SELECCIONAR_FORMA_PAGO, MSG_MONTO_PAGADO_MENOR]) then
            Exit;

        if not ValidarDatos(FFormaPagoActual) then Exit;

        if not ValidarDatosTurno then Exit;

        FFechaHoraEmision := TDate(deFechaPago.Date); //    Revision 16                                             //SS_1300_MCA_20150610

        FFechaActual := SysUtilsCN.NowBaseCN(DMConnections.BaseCAC);                                                //SS_1300_MCA_20150610
        ObtenerParametroGeneral(DMConnections.BaseCAC, 'DIAS_FECHA_MINIMA_PAGO_VENTANILLA', DiasFechaMinimaPago);   //SS_1300_MCA_20150610

        if FFechaHoraEmision > FFechaActual then begin                                                              //SS_1300_MCA_20150610
            MsgBoxBalloon(MSG_ERROR_PAGOS_FUTUROS, Caption, MB_ICONSTOP, deFechaPago);                              //SS_1300_MCA_20150610
            //deFechaPago.Date := FFechaActual;                                                                     //SS_1300_MCA_20150610
            exit;                                                                                                   //SS_1300_MCA_20150610
        end;                                                                                                        //SS_1300_MCA_20150610

        if FFechaHoraEmision <= FFechaActual - DiasFechaMinimaPago then begin                                       //SS_1300_MCA_20150610
            MsgBoxBalloon(Format(MSG_ERROR_PAGOS_PASADOS, [DiasFechaMinimaPago]), Caption, MB_ICONSTOP, deFechaPago);            //SS_1300_MCA_20150610
            //deFechaPago.Date := FFechaActual;                                                                       //SS_1300_MCA_20150610
            exit;                                                                                                   //SS_1300_MCA_20150610
        end;                                                                                                        //SS_1300_MCA_20150610

        if MsgBox(MSG_CONFIRMAR_PAGO, MSG_CAPTION, MB_ICONQUESTION or MB_DEFBUTTON2 or MB_YESNO) <> IDYES then Exit;

{INICIO: TASK_032_JMA_20160706
        //REV 19
        //DMConnections.BaseCAC.BeginTrans;
        QueryExecute(DMConnections.BaseCAC, 'BEGIN TRAN PagoVentanilla');
        try
            if GenerarRecibo(NumeroRecibo) then begin
                if RegistrarPago(FFormaPagoActual, NumeroRecibo, MensajeError) then begin
                    //REV 19
                    //DMConnections.BaseCAC.CommitTrans;
                    QueryExecute(DMConnections.BaseCAC, 'COMMIT TRAN PagoVentanilla');
                    FImprimirRecibo := True;
                    FNumeroRecibo := NumeroRecibo;
                    ModalResult := mrOk;
                //Revision 1
                    cb_CanalesDePago.ItemIndex := 0;
    //                cb_FormasPago.ItemIndex := 0;
                    neImportePagado.Value := 0;
                //Fin de Revision 1
                end else begin
                    //Rev 19
                    //DMConnections.BaseCAC.RollbackTrans;
                    // Rev.24 / 20-Enero-2010 / Nelson Droguett Sierra. --------------------------------
                    QueryExecute(DMConnections.BaseCAC, 'IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION PagoVentanilla END');
                    // ---------------------------------------------------------------------------------
                    MsgBox(MensajeError, MSG_ERROR, MB_ICONERROR);
                    ModalResult := mrAbort;
                end;
            end
            // Rev.24 / 20-Enero-2010 / Nelson Droguett Sierra. --------------------------------
            // Si no pudo generar el recibo, deshace la transaccion y actua como si se hubiera
            // caido en la grabacion
            else begin
                // Rev.24 / 20-Enero-2010 / Nelson Droguett Sierra. --------------------------------
                QueryExecute(DMConnections.BaseCAC, 'IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION PagoVentanilla END');
                // ---------------------------------------------------------------------------------
                MsgBox(MensajeError, MSG_ERROR, MB_ICONERROR);
                ModalResult := mrAbort;
            end;
            // ---------------------------------------------------------------------------------
        except
            on e: Exception do begin
                //DMConnections.BaseCAC.RollbackTrans;
                // Rev.24 / 20-Enero-2010 / Nelson Droguett Sierra. --------------------------------
                QueryExecute(DMConnections.BaseCAC, 'IF @@TRANCOUNT>0 BEGIN ROLLBACK TRANSACTION PagoVentanilla END');
                // ---------------------------------------------------------------------------------
                MsgBoxErr(MSG_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
            end;
        end;
    finally
}
         try
            IF GenerarPagoComprobante(FFormaPagoActual, MensajeError, NumeroRecibo) then
            begin
                FImprimirRecibo := True;
                FNumeroRecibo := NumeroRecibo;
                ModalResult := mrOk;
                cb_CanalesDePago.ItemIndex := 0;
                neImportePagado.Value := 0;
            end
            else
            begin
                MsgBoxErr(MSG_ERROR, MensajeError, MSG_ERROR, MB_ICONERROR);
                ModalResult := mrAbort;
            end;
        except
            on e: Exception do begin
                MsgBoxErr(MSG_ERROR, e.Message, MSG_ERROR, MB_ICONERROR);
            end;
        end;
    finally
        EmptyKeyQueue;
        Application.ProcessMessages;
        if ModalResult <> mrOk then
            btnAceptar.Enabled := True;
    end;
{TERMINO: TASK_032_JMA_20160706}
end;

function TfrmPagoVentanilla.RegistrarPago(FormaPago: TFormaPago; NumeroRecibo: Integer; var MensajeError: String): Boolean;
begin
    if (FPagoNotaCobroInfraccion) and (FormaPago = tpCheque) then
        //multiples cheques para una NI
        Result := GenerarPagoMultiplesCheques(NumeroRecibo, FormaPago, MensajeError)
    else
        Result := GenerarPago(NumeroRecibo, FormaPago, MensajeError);
end;

procedure TfrmPagoVentanilla.spbTarjetaCreditoCopiarDatosClienteClick(Sender: TObject);
begin
    edtTarjetaCreditoTitular.Text := FTitular;
end;

procedure TfrmPagoVentanilla.spbTarjetaDebitoCopiarDatosClienteClick(Sender: TObject);
begin
    edtTarjetaDebitoTitular.Text := FTitular;
end;

procedure TfrmPagoVentanilla.edtCodigoFormaPagoChange(Sender: TObject);
    resourcestring
        MSG_FORMA_DE_PAGO_ERRONEA = 'La Forma de Pago NO es V�lida para el Canal seleccionado.';
        MSG_FORMA_DE_PAGO_NO_EXISTE = 'La Forma de Pago NO Existe.';
        MSG_CAPTION_VALIDACION = 'Validar Forma de Pago';
        MSG_ERROR_VALIDACION = 'Se produjo un Error al Validar Forma de Pago';
        MSG_ERROR_VALIDACION_ERROR = 'Error: %s';
    const
        SQLValidarFormaDePagoEnCanal = 'SELECT dbo.ValidarFormaDePagoEnCanal(%s, ''%s'')';

    {******************************** Procedure Header *****************************
    Procedure Name: ResetearControlesFormaPago
    Author : pdominguez
    Date Created : 12/08/2009
    Parameters : None
    Description : SS 809
        - Reseta los valores e indicadores para la forma de pago cuando no est�
        validada.
        - Utilizamos el Tag como Flag para saber si est� validada o no
        la forma de pago.
    *******************************************************************************}
    procedure ResetearControlesFormaPago;
    begin
        FCodigoFormaPago        := 0;
        nbOpciones.Visible      := False;
        edtCodigoFormaPago.Tag  := 0;
        lblAcaMonto.Enabled     := False;
        neImportePagado.Enabled := False;
    end;
begin
    case Length(edtCodigoFormaPago.Text) of
        2: try
            with spValidarFormaDePagoEnCanal do begin
                if Active then Close;

                Parameters.ParamByName('@CodigoTipoMedioPago').Value := cb_CanalesDePago.Value;
                Parameters.ParamByName('@CodigoEntradaUsuario').Value := edtCodigoFormaPago.Text;
                // Asignamos 0 ya que el SP no estamos en la ventana de Pago Anticipado
                Parameters.ParamByName('@ValidarPagoAnticipado').Value := 0;
                Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;						//SS_1410_MCA_20151027
                Parameters.ParamByName('@CodigoSistema').Value := SistemaActual;						//SS_1410_MCA_20151027

                Parameters.ParamByName('@CodigoFormaDePago').Value := Null;
                Parameters.ParamByName('@DescripcionFormaDePago').Value := Null;
                Parameters.ParamByName('@EsValidaEnCanal').Value := Null;
                Parameters.ParamByName('@AceptaPagoParcial').Value := Null;
                Parameters.ParamByName('@CodigoPantalla').Value := Null; // Rev. 1 (SS 809)
                Parameters.ParamByName('@CodigoBanco').Value := Null;    // Rev. 1 (SS 809)
                Parameters.ParamByName('@CodigoTarjeta').Value := Null;  // Rev. 1 (SS 809)
                ExecProc;

                edtDescFormaPago.Text := Parameters.ParamByName('@DescripcionFormaDePago').Value;

                if Parameters.ParamByName('@EsValidaEnCanal').Value = 0 then begin
                    ResetearControlesFormaPago;
                    if Parameters.ParamByName('@CodigoFormaDePago').Value = 0 then
                        MsgBox(MSG_FORMA_DE_PAGO_NO_EXISTE, MSG_CAPTION_VALIDACION, MB_ICONERROR)
                    else MsgBox(MSG_FORMA_DE_PAGO_ERRONEA, MSG_CAPTION_VALIDACION,MB_ICONERROR);
                end else begin
                    FCodigoFormaPago := Parameters.ParamByName('@CodigoFormaDePago').Value;
                    // Rev. 1 (SS 809)
                    FCodigoPantalla := Parameters.ParamByName('@CodigoPantalla').Value;
                    FCodigoBanco    := Parameters.ParamByName('@CodigoBanco').Value;
                    FCodigoTarjeta  := Parameters.ParamByName('@CodigoTarjeta').Value;
                    // Fin Rev. 1 (SS 809)
                    nbOpciones.Visible := True;
                    edtCodigoFormaPago.Tag := 1;
                    lblAcaMonto.Enabled :=
                        HabilitarModificarMontoPagado and (Parameters.ParamByName('@AceptaPagoParcial').Value = 1);
                    cb_FormasPagoChange(edtCodigoFormaPago);
                end;
            end;
        except
            on E:Exception do begin
                ResetearControlesFormaPago;
                MsgBoxErr(
                    MSG_ERROR_VALIDACION,
                    Format(MSG_ERROR_VALIDACION_ERROR,[E.Message]),
                    MSG_CAPTION_VALIDACION,
                    MB_ICONERROR);
            end;
        end;
    else
        ResetearControlesFormaPago;
        edtDescFormaPago.Text   := '';
    end;
end;

procedure TfrmPagoVentanilla.edtTarjetaCreditoNroConfirmacionKeyPress(Sender: TObject; var Key: Char);
begin
//	if  not (Key in [#8, #9, #13, '0'..'9']) then Key := #0;
end;

Function  TfrmPagoVentanilla.ValidarImporteCheques : Boolean;
var
    MontoCalculado: Int64;
begin
    MontoCalculado := 0;
    with cdsCheques do begin
        DisableControls;
        try
            First;
            while not EOF do begin
                MontoCalculado := MontoCalculado + FieldByName('Monto').AsVariant;
                Next;
            end;
        finally
            EnableControls;
        end;
    end;
    Result := MontoCalculado = FImportePagado;
end;

Function  TfrmPagoVentanilla.ObtenerImporteCheques : INT64;
var
    MontoCalculado: Int64;
begin
    MontoCalculado := 0;
    with cdsCheques do begin
        DisableControls;
        try
            First;
            while not EOF do begin
                MontoCalculado := MontoCalculado + FieldByName('Monto').AsVariant;
                Next;
            end;
        finally
            EnableControls;
        end;
    end;
    Result := MontoCalculado;
end;

function TfrmPagoVentanilla.ValidarDatos(FormaPago: TFormaPago): Boolean;

    {-----------------------------------------------------------------------------
      Procedure: ValidarFechaVencimientoTarjetaCredito
      Author:  lgisuk
      Date: 07/03/07
      Arguments: Verifico si la fecha de vencimiento es valida
      Result:    Boolean
    ------------------------------------------------------------------------------
      Revision 1
      lgisuk
      21/03/07
      Cuando obtengo el mes lo completo con ceros a la izquierda para que ocupe dos
      caracteres
    --------------------------------------------------------------------------------
     Revision 2
      lgisuk
      21/03/07
      Quito validacion de que la fecha de vencimiento no es mayor a la fecha actual
    --------------------------------------------------------------------------------}
    function ValidarFechaVencimientoTarjetaCredito(FechaVencimiento : String) : Boolean;
    var
        Mes : Integer;
    begin
        Result := False;

        //si la fecha de vencimiento es 00/00, significa que no se conoce
        //asi que se permite registrar
        if (Trim(FechaVencimiento) = ('00' + DateSeparator + '00') ) then begin
            Result := True;
            Exit;
        end;

        //si el campo no tiene cinco caracteres
        if not (Length(Trim(FechaVencimiento)) = 5) then begin
           //salgo
           Exit;
        end;

        //Obtengo el mes y a�o
        try
           Mes := StrToInt(StrLeft(txt_FechaVencimiento.Text, 2));
           StrToInt(StrRight(txt_FechaVencimiento.Text, 2)); //solo se comprueba que sea valido.
           //Si se lo asigno a una variable, Delphi me la declara que no se usa para nada
        except
           //salgo
           Exit;
        end;

        //si el mes no esta entre 1 y 12
        if not ((Mes > 0) and (Mes <= 12)) then begin
            //salgo
            Exit;
        end;

        //si en el medio del mes y el a�o no hay un separador
        if not (FechaVencimiento[3] = DateSeparator) then begin
            //salgo
            Exit;
        end;

        //Obtengo el a�o de la tarjeta
        //AnioTarjeta := 2000 + AnioTarjeta;

        //Si la fecha de vencimiento no es mayor a la fecha actual
        //if not ((StrToInt(IntToStr(AnioTarjeta) + PadL(IntToStr(Mes),2,'0'))) >=  StrToInt(FormatDateTime('yyyymm', NowBase(DMConnections.BaseCAC)))) then begin
            //salgo
            //Exit;
        //end;

        Result := True;
    end;

resourcestring
    MSG_CAPTION_VALIDACION_TARJETA = 'Datos de la Tarjeta de Cr�dito/D�bito';
    MSG_ERROR_AUTORIZACION = 'Ingrese un c�digo de Autorizaci�n';
    MSG_ERROR_AUTORIZACION_DUPLICADO = 'C�digo de Autorizaci�n duplicado';
    MSG_ERROR_CUPON = 'Ingrese un n�mero de cup�n';
    MSG_ERROR_CUOTAS = 'Ingrese un n�mero de cuotas';
    MSG_ERROR_TIPO_CUENTA = 'Ingrese un n�mero de cuenta';
    MSG_ERROR_NOMBRE_TITULAR = 'Ingrese el nombre del titular';
    MSG_ERROR_DOCUMENTO = 'Debe ingresar el RUT';
    MSG_ERROR_RUT_INVALIDO = 'El RUT ingresado es inv�lido';
    MSG_CAPTION_VALIDACION_CHEQUE = 'Datos del pago con cheque';
    MSG_ERROR_NUMERO_CHEQUE = 'Ingrese el n�mero de cheque';
    MSG_ERROR_IMPORTE_CHEQUE = 'El importe del cheque debe ser superior o igual al monto a pagar.';
    MSG_ERROR_IMPORTE_CHEQUES_NO_COINCIDEN = 'La suma de los importes de los cheques no coinciden con el monto pagado';
    MSG_ERROR_FECHA_CHEQUE_INVALIDA = 'La fecha del cheque no es v�lida';
    MSG_ERROR_FECHA_CHEQUE_RANGO = 'La fecha del cheque debe estar entre el %s y el %s ';
    MSG_CAPTION_VALIDACION_VALE_VISTA = 'Datos del pago con vale vista';
    MSG_ERROR_NOMBRE_TOMADO_POR = 'Ingrese el nombre de quien toma el vale vista';
    MSG_ERROR_NUMERO_VALE_VISTA = 'Ingrese el n�mero de vale vista';
    MSG_ERROR_FECHA_VALE_VISTA_INVALIDA = 'La fecha del vale vista no es v�lida';
    MSG_ERROR_FECHA_VALE_VISTA_RANGO = 'La fecha del vale vista debe estar entre el %s y el %s ';
    MSG_CAPTION_DEBITO_CUENTA_PAC = 'Datos del D�bito de Cuenta PAC.';
    MSG_CAPTION_DEBITO_CUENTA_PAT = 'Datos del D�bito de Cuenta PAT.';
    MSG_ERROR_BANCO = 'Debe seleccionar un banco.';
    MSG_ERROR_NUMERO_CUENTA_BANCARIA = 'Debe ingresar una cuenta bancaria.';
    MSG_CAPTION = 'Cobro de Comprobante';
    MSG_IMPORTE_PAGADO_MENOR_A_CERO = 'El importe pagado debe ser mayor que cero.';
    MSG_IMPORTE_RECIBIDO_INFERIOR_A_IMPORTE_PAGAR = 'El monto recibibo no puede ser menor que el monto a pagar.';
    MSG_CAPTION_VALIDACION_PAGARE   = 'Datos del pago con pagar�';
    MSG_ERROR_BANCO_PAGARE          = 'Debe seleccionar un banco';
    MSG_ERROR_NUMERO_PAGARE         = 'Ingrese el n�mero de pagar�';
    MSG_ERROR_IMPORTE_PAGARE        = 'El importe del pagar� debe ser superior o igual al monto a pagar.';
    MSG_ERROR_FECHA_PAGARE_INVALIDA = 'La fecha del pagar� no es v�lida';
    MSG_ERROR_FECHA_PAGARE_RANGO    = 'La fecha del pagar� debe estar entre el %s y el %s ';
    MSG_ERROR_TIPO_TARJETA_CREDITO_VACIO = 'Tipo Tarjeta Cr�dito vac�o' ;
    MSG_ERROR_NUMERO_TARJETA_CREDITO_VACIO = 'N�mero Tarjeta Cr�dito vac�o';
    MSG_ERROR_EMISOR_TARJETA_CREDITO_VACIO = 'Emisor Tarjeta Cr�dito vac�o'  ;
    MSG_ERROR_NUMERO_TARJETA_CREDITO = 'Error en N�mero Tarjeta Cr�dito'     ;
    MSG_ERROR_FECHA_TARJETA_CREDITO  = 'Error en fecha de vencimiento de tarjeta';
    MSG_ERROR_TARJETA_CREDITO_TARJETA = 'Debe Seleccionar un Tipo de Tarjeta de Cr�dito';
    MSG_ERROR_REPACTACION_DUPLICADO = 'N�mero de Repactaci�n duplicado';   // Rev. 22 (SS 809)
    MSG_ERROR_REPACTACION_REQUERIDO = 'Ingrese un N�mero de Repactaci�n'; // Rev. 22 (SS 809)
var
    ErrorNumeroTarjetaCredito: String;
    NumeroValidoTarjeta: Boolean;
begin
    FImportePagado := neImportePagado.ValueInt;
    Result := False;

    //Validamos que lo que se desea pagar sea un monto al menos mayor a cero
    if (FComprobantesCobrar.Count = 1) and (not ValidateControls([neImportePagado], [(FImportePagado > 0)],
      MSG_CAPTION, [MSG_IMPORTE_PAGADO_MENOR_A_CERO])) then begin
        Exit;
    end;

    // Ok, ahora s�lo nos resta pagar seg�n la forma elegida.
    case FormaPago of
        tpEfectivo: begin
		    // Validamos que lo que el monto entregado por el cliente sea al menos igual o mayor que lo que
		    // el cliente dijo que desea pagar.
            // Revision 7 todo este bloque estaba afuera del case y solo funcionaba cuando era tpEfectivo,
            // y daba errores para las otras formas de pago Kanav 1455
           Result := ValidateControls([nePagaCon], [nePagaCon.ValueInt >= FImportePagado],
				      MSG_CAPTION, [MSG_IMPORTE_RECIBIDO_INFERIOR_A_IMPORTE_PAGAR]);
        end;
        tpCheque :
            begin
                //Si no esta pagando una NI
                if not fPagoNotaCobroInfraccion then begin
                    //Valido los datos del cheque individual
                    Result :=
                        ValidateControls(
                            [txtChequeNombreTitular,
                             txtChequeDocumento,
                             txtChequeDocumento,
                             cbChequeBancos, // Rev. 23 (SS 834)
                             txtChequeNumero, // Rev. 23 (SS 834)
                             deChequeFecha,
                             deChequeFecha,
                             edtRepactacionCheque], // Rev. 23 (SS 834)
                            [(txtChequeNombreTitular.Text <> ''),
                             (txtChequeDocumento.Text <> ''),
                             (ValidarRUT(DMConnections.BaseCAC, trim(txtChequeDocumento.Text))),
                             (cbChequeBancos.Value <> 0), // Rev. 22 (SS 809)
                             (txtChequeNumero.Text <> ''), // Rev. 23 (SS 834)
                             (IsValidDate(DateTimeToStr(deChequeFecha.Date))),
                             ((deChequeFecha.Date >= FFechaMinimaCheque) AND (deChequeFecha.Date < FFechaMaximaCheque)),
                             (QueryGetValue(
                                    DMConnections.BaseCAC,
                                    Format('select dbo.ExisteRepactacionNumeroFormaPago(''%s'', %d)',[edtRepactacionCheque.Text, FCodigoFormaPago])) = 'False')],  // Rev. 23 (SS 834)
                            MSG_CAPTION_VALIDACION_CHEQUE,
                            [MSG_ERROR_NOMBRE_TITULAR,
                             MSG_ERROR_DOCUMENTO,
                             MSG_ERROR_RUT_INVALIDO,
                             MSG_ERROR_BANCO, // Rev. 23 (SS 834)
                             MSG_ERROR_NUMERO_CHEQUE, // Rev. 23 (SS 834)
                             MSG_ERROR_FECHA_CHEQUE_INVALIDA,
                             Format(MSG_ERROR_FECHA_CHEQUE_RANGO, [FormatDateTime('dd/mm/yyyy',FFechaMinimaCheque), FormatDateTime('dd/mm/yyyy',FFechaMaximaCheque-1)]),
                             MSG_ERROR_REPACTACION_DUPLICADO]); // Rev. 23 (SS 834)

                //si esta pagando una NI
                end else begin
                    //Valido que la suma de los cheques coincida con el total a pagar
                    Result := ValidateControls([DbItemsCheque], [(ValidarImporteCheques = True)], MSG_CAPTION_VALIDACION_CHEQUE, [MSG_ERROR_IMPORTE_CHEQUES_NO_COINCIDEN]);
                end;

            end;
        tpValeVista :
            begin
                Result :=
                    ValidateControls(
                        [txt_TomadoPorVV,
                         txt_RUTVV,
                         txt_RUTVV,
                         cb_BancosVV,
                         txt_NroVV,
                         de_FechaVV,
                         de_FechaVV],
                         [(txt_TomadoPorVV.Text <> ''),
                          (txt_RUTVV.Text <> ''),
                          (ValidarRUT(DMConnections.BaseCAC,trim(txt_RUTVV.Text))),
                          (cb_BancosVV.Value <> 0), // Rev. 22 (SS 809)
                          (txt_NroVV.Text <> ''),
                          (IsValidDate(DateTimeToStr(de_FechaVV.Date))),
                          ((de_FechaVV.Date >= FFechaMinimaCheque) AND (de_FechaVV.Date < FFechaMaximaCheque))],
                         MSG_CAPTION_VALIDACION_VALE_VISTA,
                         [MSG_ERROR_NOMBRE_TITULAR,
                          MSG_ERROR_DOCUMENTO,
                          MSG_ERROR_RUT_INVALIDO,
                          MSG_ERROR_BANCO,
                          MSG_ERROR_NUMERO_VALE_VISTA,
                          MSG_ERROR_FECHA_VALE_VISTA_INVALIDA,
                          Format(MSG_ERROR_FECHA_VALE_VISTA_RANGO, [FormatDateTime('dd/mm/yyyy',FFechaMinimaCheque), FormatDateTime('dd/mm/yyyy',FFechaMaximaCheque-1)])]);
            end;
        tpTarjetaDebito:
            begin
                // Rev. 14 (SS 809)
{                    Result := ValidateControls([edAutorizacion, edCupon, edCuotas, edAutorizacion],
                      [(edAutorizacion.text <> ''),
                       (edCupon.text <> ''),
                       (edCupon.text <> ''),
                       (QueryGetValue(DMConnections.BaseCAC, format('select dbo.ExisteCodigoAutorizacionWebPay(''%s'')',[edAutorizacion.text])) = 'False')
                       ],
                        MSG_CAPTION_VALIDACION_TARJETA,[MSG_ERROR_AUTORIZACION, MSG_ERROR_CUPON, MSG_ERROR_CUOTAS,MSG_ERROR_AUTORIZACION_DUPLICADO]);}
                Result :=
                    ValidateControls(
                        [edtTarjetaDebitoTitular,
                         vcbTarjetaDebitoBancos,
                         edtTarjetaDebitoNroConfirmacion,
                         edtTarjetaDebitoNroConfirmacion],
                        [(edtTarjetaDebitoTitular.Text <> ''),
                         (vcbTarjetaDebitoBancos.Value <> 0), // Rev. 22 (SS 809)
                         (edtTarjetaDebitoNroConfirmacion.Text <> ''),
                         (QueryGetValue(DMConnections.BaseCAC, format('select dbo.ExisteCodigoAutorizacionWebPay(''%s'')',[edtTarjetaDebitoNroConfirmacion.text])) = 'False')],
                        MSG_CAPTION_VALIDACION_TARJETA,
                        [MSG_ERROR_NOMBRE_TITULAR,
                         MSG_ERROR_BANCO,
                         MSG_ERROR_AUTORIZACION,
                         MSG_ERROR_AUTORIZACION_DUPLICADO]);

                // Fin Rev. 14 (SS 809)
            end;

        tpTarjetaCredito:
            begin
                // Rev. 14 (SS 809)
{                    Result := ValidateControls([edAutorizacion, edCupon, edCuotas, edAutorizacion],
                     [(edAutorizacion.text <> ''),
                      (edCupon.text <> ''),
                      (edCuotas.text <> ''),
                      (QueryGetValue(DMConnections.BaseCAC, format('select dbo.ExisteCodigoAutorizacionWebPay(''%s'')',[edAutorizacion.text])) = 'False')
                      ],
                     MSG_CAPTION_VALIDACION_TARJETA, [MSG_ERROR_AUTORIZACION, MSG_ERROR_CUPON, MSG_ERROR_CUOTAS,MSG_ERROR_AUTORIZACION_DUPLICADO]);}

                Result :=
                    ValidateControls(
                        [edtTarjetaCreditoTitular,
                         vcbTarjetaCreditoBancos,
                         vcbTarjetaCreditoTarjetas,
                         edtTarjetaCreditoNroConfirmacion,
                         edtTarjetaCreditoNroConfirmacion,
                         nedtTarjetaCreditoCuotas],
                        [(edtTarjetaCreditoTitular.Text <> ''),
                         (vcbTarjetaCreditoBancos.Value <> 0), // Rev. 22 (SS 809)
                         (vcbTarjetaCreditoTarjetas.Value <> 0), // Rev. 22 (SS 809)
                         (edtTarjetaCreditoNroConfirmacion.Text <> ''),
                         (QueryGetValue(DMConnections.BaseCAC, format('select dbo.ExisteCodigoAutorizacionWebPay(''%s'')',[edtTarjetaCreditoNroConfirmacion.text])) = 'False'),
                         (nedtTarjetaCreditoCuotas.Text <> '')],
                        MSG_CAPTION_VALIDACION_TARJETA,
                        [MSG_ERROR_NOMBRE_TITULAR,
                         MSG_ERROR_BANCO,
                         MSG_ERROR_TARJETA_CREDITO_TARJETA,
                         MSG_ERROR_AUTORIZACION,
                         MSG_ERROR_AUTORIZACION_DUPLICADO,
                         MSG_ERROR_CUOTAS]);

                // Fin Rev. 14 (SS 809)
            end;

        tpDebitoCuenta:
            begin
                if FCodigoCanalPago = CONST_CANAL_PAGO_PAC then begin
                    Result :=
                        ValidateControls(
                            [cb_BancosPAC,
                             txt_NroCuentaBancariaPAC],
                            [(cb_BancosPAC.Value <> 0), // Rev. 22 (SS 809)
                             Trim(txt_NroCuentaBancariaPAC.Text) <> EmptyStr],
                            MSG_CAPTION_DEBITO_CUENTA_PAC,
                            [MSG_ERROR_BANCO,
                             MSG_ERROR_NUMERO_CUENTA_BANCARIA]);
                end else if FCodigoCanalPago = CONST_CANAL_PAGO_PAT then begin
                    //Revision 11 start
                    Result :=
                        ValidateControls(
                            [cb_TipoTarjetaCreditoPAT,
                             txt_NumeroTarjetaCreditoPAT,
                             cb_EmisorTarjetaCreditoPAT,
                             txt_FechaVencimiento],
                            [
                            (cb_TipoTarjetaCreditoPAT.Value <> 0), // Rev. 22 (SS 809)
                            Trim(txt_NumeroTarjetaCreditoPAT.Text)<> EmptyStr ,
                            (cb_EmisorTarjetaCreditoPAT.Value <> 0), // Rev. 22 (SS 809)
                            ValidarFechaVencimientoTarjetaCredito(txt_FechaVencimiento.Text) = True //modificado en revision 13
                            ],
                            MSG_CAPTION_DEBITO_CUENTA_PAT,
                            [MSG_ERROR_TIPO_TARJETA_CREDITO_VACIO ,
                            MSG_ERROR_NUMERO_TARJETA_CREDITO_VACIO,
                            MSG_ERROR_EMISOR_TARJETA_CREDITO_VACIO,
                            MSG_ERROR_FECHA_TARJETA_CREDITO ]);
                    NumeroValidoTarjeta := True;
                    if Result then begin
                        NumeroValidoTarjeta := ValidarTarjetaCredito(DMConnections.BaseCAC,
                                               StrToInt(Trim(StrRight(cb_TipoTarjetaCreditoPAT.Value, 30))),
                                               txt_NumeroTarjetaCreditoPAT.Text,
                                               ErrorNumeroTarjetaCredito);
                        if not NumeroValidoTarjeta then
                            MsgBoxErr(MSG_ERROR_NUMERO_TARJETA_CREDITO, ErrorNumeroTarjetaCredito , MSG_ERROR_NUMERO_TARJETA_CREDITO, MB_ICONERROR);
                    end;
                    Result := Result and NumeroValidoTarjeta;
                end else begin
                    Result := False;
                    MsgBoxErr('MSG_ERROR', 'Debito en cuenta solo para PAT o PAC' , 'MSG_ERROR', MB_ICONERROR);
                end;
                //Revision 11 end
            end;
        tpPagare:
            begin
                // Revision 15
                Result :=
                    ValidateControls(
                        [txtPagareNombreTitular,
                         txtPagareDocumento,
                         txtPagareDocumento,
                         cbPagareBancos,
                         txtPagareNumero,
                         dePagareFecha,
                         dePagareFecha,
                         edtRepactacion,
                         edtRepactacion],
                        [(txtPagareNombreTitular.Text <> EmptyStr),
                         (txtPagareDocumento.Text <> EmptyStr),
                         (ValidarRUT(DMConnections.BaseCAC, Trim(txtPagareDocumento.Text))),
                         cbPagareBancos.Value <> 0, // Rev. 22 (SS 809)
                         (txtPagareNumero.Text <> EmptyStr),
                         (IsValidDate(DateTimeToStr(dePagareFecha.Date))),
                         ((dePagareFecha.Date >= FFechaMinimaPagare) AND (dePagareFecha.Date < FFechaMaximaPagare)),
                         (Trim(edtRepactacion.Text) <> ''),
                         (QueryGetValue(
                                DMConnections.BaseCAC,
                                Format('select dbo.ExisteRepactacionNumeroFormaPago(''%s'', %d)',[edtRepactacion.Text, FCodigoFormaPago])) = 'False')],
                         MSG_CAPTION_VALIDACION_PAGARE,
                         [MSG_ERROR_NOMBRE_TITULAR,
                          MSG_ERROR_DOCUMENTO,
                          MSG_ERROR_RUT_INVALIDO,
                          MSG_ERROR_BANCO_PAGARE,
                          MSG_ERROR_NUMERO_PAGARE,
                          MSG_ERROR_FECHA_PAGARE_INVALIDA,
                          Format(MSG_ERROR_FECHA_PAGARE_RANGO, [FormatDateTime('dd/mm/yyyy', FFechaMinimaPagare),
                          FormatDateTime('dd/mm/yyyy', FFechaMaximaPagare - 1)]),
                          MSG_ERROR_REPACTACION_REQUERIDO,
                          MSG_ERROR_REPACTACION_DUPLICADO]);
            end;
        else
            Result := False;
    end;
end;

function TfrmPagoVentanilla.ImprimirRecibo : boolean;
begin
	result := FImprimirRecibo;
end;

function TfrmPagoVentanilla.NumeroRecibo : INt64;
begin
	result := FNumeroRecibo;
end;

procedure TfrmPagoVentanilla.btnDatosClienteChequeClick(Sender: TObject);
begin
    txtChequeNombreTitular.Text := FTitular;
	txtChequeDocumento.Text     := FRut;
end;

procedure TfrmPagoVentanilla.deFechaPagoChange(Sender: TObject);
var
    MaxDias,
    MinDias,
    FCodigoFormaPago    : integer;
    FFechaPago: TDateTime;
begin
    //Asigno la nueva fecha a nuestra variable de fecha emision
    FFechaHoraEmision := TDate(deFechaPago.Date); //    Revision 16

    if Sender <> ActiveControl then exit;
    //FCodigoFormaPago := Integer(cb_FormasPago.Value); // Rev1 (SS 809)
    if FCodigoFormaPago <> 4 then Exit;

    // Recalculo las fechas minimas y maximas para validar un cheque!
    FFechaPago := Trunc(deFechaPago.Date);
    ObtenerParametroGeneral(DMConnections.BaseCAC,'DIAS_FECHA_MINIMA_CHEQUE',MinDias);
    ObtenerParametroGeneral(DMConnections.BaseCAC,'DIAS_FECHA_MAXIMA_CHEQUE',MaxDias);
    FFechaMaximaCheque := FFechaPago + MaxDias + 1;
    FFechaMinimaCheque := FFechaPago - MinDias;
end;

procedure TfrmPagoVentanilla.btn_DatosClienteValeVistaClick(Sender: TObject);
begin
    txt_TomadoPorVV.Text := FTitular;
	txt_RUTVV.Text := FRut;
end;

function TfrmPagoVentanilla.ValidarDatosTurno: Boolean;
var
    DatosTurno: TDatosTurno;
begin
    (* Verificar que el turno que se est� por utilizar est� abierto y sea del
    usuario logueado. *)
    if VerTurnoAbierto(DMConnections.BaseCAC, FPuntoEntrega, FPuntoVenta, UsuarioSistema, DatosTurno) then begin
        GNumeroTurno := DatosTurno.NumeroTurno;
        Result := True;
    end else begin
        if (DatosTurno.NumeroTurno <> -1)
                and ((DatosTurno.Estado = TURNO_ESTADO_ABIERTO)
                and (Trim(DatosTurno.CodigoUsuario) <> (UpperCase(UsuarioSistema)))) then begin
            MsgBox(Format(MSG_ERROR_TURNO_ABIERTO_OTRO_USUARIO, [DatosTurno.CodigoUsuario]),
                Self.Caption, MB_ICONSTOP);
        end else begin
            MsgBox(MSG_ERROR_TURNO_CERRADO, Self.Caption, MB_ICONSTOP);
        end;
        Result := False;
    end;

end;

procedure TfrmPagoVentanilla.cb_CanalesDePagoChange(Sender: TObject);
begin
    FCodigoCanalPago := Integer(cb_CanalesDePago.Value);
    edtCodigoFormaPago.Text := ''; // Rev. 1 (SS 809)
    //neImportePagado.Value := FImporte;

// Rev. 1 (SS 809)
//    lblAcaMonto.Enabled := HabilitarModificarMontoPagado;
    (* Cargar el combo de Medio de Pago. *)
{    cb_FormasPago.Items.Clear;
    if cb_CanalesDePago.ItemIndex >= 0 then begin
        CargarFormasPago(DMConnections.BaseCAC, cb_FormasPago, cb_CanalesDePago.Value);
        FiltrarFormasDePago((cbChequeBancos.Items.Count > 0), (FNumeroPOS > 0));
        cb_FormasPagoChange(cb_CanalesDePago);
    end;}

    if Self.Visible then edtCodigoFormaPago.SetFocus; // Rev. 1 (SS 809)
//
end;

procedure TfrmPagoVentanilla.MostrarDatosPACComprobante(NumeroComprobante: Integer; TipoComprobante: AnsiString);
var
    sp: TADOStoredProc;
begin
    cb_BancosPAC.Value := -1;
    txt_NroCuentaBancariaPAC.Clear;
    txt_SucursalPAC.Clear;
    txt_TitularPAC.Clear;

    sp := TADOStoredProc.Create(Nil);
    try
        sp.Connection := DMConnections.BaseCAC;
        sp.ProcedureName := 'ObtenerDatosPACComprobante';
        sp.Parameters.Refresh;
        sp.Parameters.ParamByName('@NumeroComprobante').Value := NumeroComprobante;
        sp.Parameters.ParamByName('@TipoComprobante').Value := TipoComprobante;
        sp.Open;
        if sp.RecordCount > 0 then begin
            cb_BancosPAC.Value := sp.FieldByName('PAC_CodigoBanco').AsInteger;
            txt_NroCuentaBancariaPAC.Text := sp.FieldByName('PAC_NroCuentaBancaria').AsString;
            txt_SucursalPAC.Text := sp.FieldByName('PAC_Sucursal').AsString;
            txt_TitularPAC.Text := EmptyStr;
        end;
    finally
        sp.Close;
        sp.Free;
    end; // finally


end;

procedure TfrmPagoVentanilla.MostrarDatosPATComprobante(NumeroComprobante: Integer; TipoComprobante: AnsiString);
var
    sp: TADOStoredProc;
begin
    cb_TipoTarjetaCreditoPAT.ItemIndex := -1;
    txt_NumeroTarjetaCreditoPAT.Clear;
    txt_FechaVencimiento.Clear;
    cb_EmisorTarjetaCreditoPAT.ItemIndex := -1;

    sp := TADOStoredProc.Create(Nil);
    try
        sp.Connection := DMConnections.BaseCAC;
        sp.ProcedureName := 'ObtenerDatosPATComprobante';
        sp.Parameters.Refresh;
        sp.Parameters.ParamByName('@NumeroComprobante').Value := NumeroComprobante;
        sp.Parameters.ParamByName('@TipoComprobante').Value := TipoComprobante;
        sp.Open;
        if sp.RecordCount > 0 then begin
            cb_TipoTarjetaCreditoPAT.Value := sp.FieldByName('PAT_CodigoTipoTarjetaCredito').AsInteger;
            txt_NumeroTarjetaCreditoPAT.Text := sp.FieldByName('PAT_NumeroTarjetaCredito').AsString;
            if not sp.FieldByName('PAT_FechaVencimiento').IsNull then begin

                txt_FechaVencimiento.Text := Copy(sp.FieldByName('PAT_FechaVencimiento').AsString,1,2) +
                DateSeparator + Copy(sp.FieldByName('PAT_FechaVencimiento').AsString,4,2);

            end;
            cb_EmisorTarjetaCreditoPAT.Value := sp.FieldByName('PAT_CodigoEmisorTarjetaCredito').AsInteger;
        end;
    finally
        sp.Close;
        sp.Free;
    end; // finally
end;

function TfrmPagoVentanilla.ObtenerImporte(Comprobantes: TListaComprobante): Double;
var
    i: Integer;
begin
    Result := 0;
    for i := 0 to Comprobantes.Count - 1 do begin
        Result := Result + TComprobante(Comprobantes.Items[i]).TotalAPagar;
    end;
end;

procedure TfrmPagoVentanilla.CerrarRegistroPagoComprobante(TipoComprobante: AnsiString; NumeroComprobante: Int64; FechaPago: TDateTime);
resourcestring
    MSG_ERROR_CIERRE = 'Error completando el registro del pago';
begin
   // Por �ltimo, cerramos el comprobante
   try
        with CerrarPagoComprobante.Parameters do begin
            ParamByName('@TipoComprobante').Value 	:= TipoComprobante;
            ParamByName('@NumeroComprobante').Value := NumeroComprobante;
            ParamByName('@FechaHora').Value		    := FechaPago;
         end;
         CerrarPagoComprobante.ExecProc;
    except
        on e: Exception do begin
            raise Exception.Create(MSG_ERROR_CIERRE + CRLF + e.Message);
        end;
    end;
end;

procedure TfrmPagoVentanilla.FiltrarCanalesDePago;
var
    CodigoItemSeleccionado: Integer;
begin
    if FComprobantesCobrar.Count > 1 then begin
        if CanalDePagoCargado(CONST_CANAL_PAGO_PAC) then begin
            CodigoItemSeleccionado := cb_CanalesDePago.Value;
            cb_CanalesDePago.Value := CONST_CANAL_PAGO_PAC;
            cb_CanalesDePago.Items.Delete(cb_CanalesDePago.ItemIndex);
            if cb_CanalesDePago.Items.Count > 0 then begin
                if CodigoItemSeleccionado = CONST_CANAL_PAGO_PAC then begin
                    cb_CanalesDePago.Value := cb_CanalesDePago.Items[0].Value;
                end else begin
                    cb_CanalesDePago.Value := CodigoItemSeleccionado;
                end;
            end;
        end;

        if CanalDePagoCargado(CONST_CANAL_PAGO_PAT) then begin
            CodigoItemSeleccionado := cb_CanalesDePago.Value;
            cb_CanalesDePago.Value := CONST_CANAL_PAGO_PAT;
            cb_CanalesDePago.Items.Delete(cb_CanalesDePago.ItemIndex);
            if cb_CanalesDePago.Items.Count > 0 then begin
                if CodigoItemSeleccionado = CONST_CANAL_PAGO_PAT then begin
                    cb_CanalesDePago.Value := cb_CanalesDePago.Items[0].Value;
                end else begin
                    cb_CanalesDePago.Value := CodigoItemSeleccionado;
                end;
            end;
        end;
    end;
end;

procedure TfrmPagoVentanilla.FormShow(Sender: TObject);
begin
    EmptyKeyQueue;
end;

function TfrmPagoVentanilla.CanalDePagoCargado(CodigoCanalPago: Byte): Boolean;
var
    ItemSeleccionado, i: Integer;
begin
    Result := False;
    if cb_CanalesDePago.Items.Count > 0 then begin
        ItemSeleccionado := cb_CanalesDePago.Value;
        for i := 0 to (cb_CanalesDePago.Items.Count - 1) do begin
            if cb_CanalesDePago.Items[i].Value = CodigoCanalPago then begin
                Result := True;
                Break;
            end;
        end; // for
        cb_CanalesDePago.Value := ItemSeleccionado;
    end;
end;

procedure TfrmPagoVentanilla.nePagaconChange(Sender: TObject);
begin
	if ( nePagacon.Value >= neImportePagado.Value ) then
    	neCambio.Value := nePagacon.Value - neImportePagado.Value
    else
    	neCambio.Clear;
end;

procedure TfrmPagoVentanilla.lblAcaMontoClick(Sender: TObject);
begin
    neImportePagado.Enabled := True;
    lblAcaMonto.Enabled := False;
end;

procedure TfrmPagoVentanilla.neImporteKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = '-' then Key := #0;
end;

function TfrmPagoVentanilla.HabilitarModificarMontoPagado: Boolean;
begin
    Result := ExisteAcceso('modif_monto_pagado') and (TComprobante(FComprobantesCobrar.Items[0]).TipoComprobante <> 'NI');
    //  or (FCodigoFormaPago <> CONST_FORMA_PAGO_EFECTIVO);
     // and (FComprobantesCobrar.Count = 1) or (FCodigoFormaPago <> CONST_FORMA_PAGO_EFECTIVO);
end;

procedure TfrmPagoVentanilla.neImportePagadoChange(Sender: TObject);
begin
	nePagaConChange(Self);
end;

function TfrmPagoVentanilla.CalcularImportePagado(i: Integer): Int64;
begin
	// Primero chequeamos si es el �timo comprobante de la lista
	// Si es el �ltimo retornamos la diferencia entre la suma de los comprobantes ya pagados y lo indicado
	if (neImportePagado.ValueInt * 100 - FImporteComprobantesPagados) > 0 then begin
		if ((neImportePagado.ValueInt * 100 - FImporteComprobantesPagados) <
		  (trunc(TComprobante(FComprobantesCobrar.Items[i]).TotalAPagar) * 100)) or
		  (i = FComprobantesCobrar.Count - 1) then begin
			result := (neImportePagado.ValueInt * 100 - FImporteComprobantesPagados);
		end else begin
			result := trunc(TComprobante(FComprobantesCobrar.Items[i]).TotalAPagar) * 100;
		end;
		FImporteComprobantesPagados := FImporteComprobantesPagados +
		  trunc(TComprobante(FComprobantesCobrar.Items[i]).TotalAPagar) * 100;
	end else begin
		result := 0;
	end;
	(*
	if FImporteComprobantesPagados > neImportePagado.ValueInt * 100 then begin
		result := neImportePagado.ValueInt * 100 - FImporteComprobantesPagados;
	end else begin
		// Si no es el �ltimo retornamos el importe del comprobante y actualizamos la variable global
		FImporteComprobantesPagados := FImporteComprobantesPagados +
		  trunc(TComprobante(FComprobantesCobrar.Items[i]).TotalAPagar) * 100;
		if
		result := trunc(TComprobante(FComprobantesCobrar.Items[i]).TotalAPagar) * 100;
	end;
	*)
end;

procedure TfrmPagoVentanilla.btnDatosClientePagareClick(Sender: TObject);
begin
    txtPagareNombreTitular.Text := FTitular;
  	txtPagareDocumento.Text     := FRut;
end;

procedure TfrmPagoVentanilla.BtnAgregarItemClick(Sender: TObject);
var
	f : TfrmAgregarCheque;
  ChequeTitular : String;
  ChequeRUT : String;
  ChequeCodigoBanco : Integer;
begin
    Application.CreateForm(TfrmAgregarCheque, f);
    f.Visible := False;

    //Obtengo los datos del ultimo cheque ingresado para mostrarlos por defecto
    cdsCheques.Last;
    ChequeTitular := cdsCheques.FieldByName('Titular').AsString;
    ChequeRUT := cdsCheques.FieldByName('RUT').AsString;
    ChequeCodigoBanco := cdsCheques.FieldByName('CodigoBanco').Asinteger;

    with cdsCheques do begin
    	  if f.Inicializar(FTitular, FRut, FFechaMinimaCheque, FFechaMaximaCheque, ChequeTitular, ChequeRUT, ChequeCodigoBanco) then begin
    		    if f.ShowModal = mrOK then begin
                 Append;
                 FieldByName('Titular').AsString := F.EdTitular.Text;
                 FieldByName('RUT').AsString    := F.edrut.Text;
                 FieldByName('Banco').AsString := F.cbBanco.Text;
                 FieldByName('NumeroCheque').AsString := F.ednumerocheque.Text;
                 FieldByName('FechaCheque').AsDateTime := F.edFecha.Date;
                 FieldByName('Monto').AsVariant := F.edMonto.ValueInt;
                 FieldByName('CodigoBanco').AsInteger := F.cbBanco.Value;
                 Post;
                 lbltotal.Caption := IntToStr(ObtenerImporteCheques);
            end;
        end;
    end;
    f.Free;
end;

procedure TfrmPagoVentanilla.BtnEliminarItemClick(Sender: TObject);
resourcestring
	MSG_ITEM_DELETE_CONFIRMATION = 'Desea eliminar el cheque?';
begin
    // Si no hay items no permitir eliminar.
    if cdsCheques.IsEmpty then Exit;
    //confirmo si lo desea eliminar
  	if MsgBox(MSG_ITEM_DELETE_CONFIRMATION, Caption, MB_ICONQUESTION or MB_DEFBUTTON2 or MB_YESNO) = IDYES then begin
        //elimino el cheque
		    cdscheques.Delete;
        lbltotal.Caption := IntToStr(ObtenerImporteCheques);
    end;
end;

procedure TfrmPagoVentanilla.btnCerrarClick(Sender: TObject);
begin
    Close;
end;


function TfrmPagoVentanilla.GenerarPago(NumeroRecibo: Integer; FormaPago:TFormaPago; var MensajeError: String): Boolean;
resourcestring
    MSG_PAGO_EFECTIVO = 'Pago en efectivo';
	MSG_PAGO_TARJETA_CREDITO  = 'Pago con tarjeta de credito';
	MSG_PAGO_TARJETA_DEBITO  = 'Pago con tarjeta de d�bito';
	MSG_PAGO_DEBITO_CUENTA = 'Pago con d�bito cuenta';
	MSG_PAGO_CHEQUE = 'Pago con cheque';
	MSG_PAGO_VALE_VISTA = 'Pago con vale vista';
	MSG_PAGO_PAGARE                 = 'Pago con pagar�';

    MSG_ERROR_PAGO_EFECTIVO = 'Error registrando un pago en efectivo';
	MSG_ERROR_PAGO_TARJETA = 'Error registrando un pago con tarjeta de cr�dito/d�bito';
	MSG_ERROR_PAGO_DEBITO_CUENTA = 'Error registrando un pago con con d�bito cuenta';
	MSG_ERROR_PAGO_CHEQUE = 'Error registrando un pago con cheque';
	MSG_ERROR_PAGO_VALE_VISTA = 'Error registrando un pago con vale vista';
	MSG_ERROR_PAGO_PAGARE           = 'Error registrando un pago con pagar�';

    MSG_ERROR_ID_DETALLE_IS_NULL = 'No se pudo registrar el Detalle del Pago';
    MSG_ERROR_NUMERO_PAGO_IS_NULL = 'No se pudo registrar del Pago del Comprobante'#10#13'%s %d';
    MSG_ERROR_NO_BALANCEAN_PAGOS_CON_RECIBO = 'El Recibo %d es de un importe de %f y la suma de sus pagos asociados es de %f';
var
	I: Integer;
	Comprobante: TComprobante;
	MensajeDeError: string;
    ImportePago, SumatoriaPagosControlRecibo : Int64;
begin
	Result := False;
	FImporteComprobantesPagados := 0;
	MensajeError := '';
	SumatoriaPagosControlRecibo := 0;


    try

        sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@CodigoTipoMedioPago').Value := FCodigoCanalPago;
        sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@CodigoFormaPago').Value     := FCodigoFormaPago;
        // Rev. 20 (SS 809)
        if (FormaPago = tpTarjetaCredito) or (FormaPago = tpTarjetaDebito) then begin
            if FormaPago = tpTarjetaDebito then begin
                sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@Titular').Value           := Trim(edtTarjetaDebitoTitular.Text);
                sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@CodigoBancoEmisor').Value := vcbTarjetaDebitoBancos.Value;
                sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@Autorizacion').Value      := Trim(UpperCase(edtTarjetaDebitoNroConfirmacion.Text));
            end;
            if FormaPago = tpTarjetaCredito then begin
                sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@Titular').Value           := Trim(edtTarjetaCreditoTitular.Text);
                sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@CodigoBancoEmisor').Value := vcbTarjetaCreditoBancos.Value;
                sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@CodigoTipoTarjeta').Value := vcbTarjetaCreditoTarjetas.Value;
                sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@Autorizacion').Value      := Trim(UpperCase(edtTarjetaCreditoNroConfirmacion.Text));
                sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@Cuotas').Value            := nedtTarjetaCreditoCuotas.Value;
            end;

            //    sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@Cupon').Value 		  := IStr0(edCupon.ValueInt);
            //    sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@Autorizacion').Value    := Trim(UpperCase(edAutorizacion.Text));
            //    sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@Cuotas').Value          := edCuotas.Value;
        // Fin Rev. 20 (SS 809)
            if (FNumeroPOS > 0) then begin
                sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@NumeroPOS').Value := FNumeroPOS;
            end else begin
                sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@NumeroPOS').Value := Null;
            end;
        end;
        if FormaPago = tpDebitoCuenta then begin
            if FCodigoCanalPago = CONST_CANAL_PAGO_PAC then begin
                sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@PAC_CodigoBanco').Value       := cb_BancosPAC.Value;
                sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@PAC_NroCuentaBancaria').Value := Trim(txt_NroCuentaBancariaPAC.Text);
                sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@PAC_Sucursal').Value          := Trim(txt_SucursalPAC.Text);
                sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@PAC_Titular').Value           := Trim(txt_TitularPAC.Text);
            end else begin // FCodigoCanalPago = CONST_CANAL_PAGO_PAT
                sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@PAT_CodigoTipoTarjetaCredito').Value   := iif(cb_TipoTarjetaCreditoPAT.Value = 0, Null, cb_TipoTarjetaCreditoPAT.Value);
                sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@PAT_NumeroTarjetaCredito').Value       := iif(Trim(txt_NumeroTarjetaCreditoPAT.Text) = EmptyStr, Null, Trim(txt_NumeroTarjetaCreditoPAT.Text));
                sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@PAT_FechaVencimiento').Value           := iif(Trim(txt_FechaVencimiento.Text) = EmptyStr, Null, Trim(txt_FechaVencimiento.Text));
                sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@PAT_CodigoEmisorTarjetaCredito').Value := iif(cb_EmisorTarjetaCreditoPAT.Value = 0, Null, cb_EmisorTarjetaCreditoPAT.Value);
            end;
        end;
        if FormaPago = tpCheque then begin
            sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@ChequeCodigoBanco').Value     := cbChequeBancos.Value; // (SS 809)
            sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@ChequeNumero').Value          := Trim(UpperCase(txtChequeNumero.Text));
            sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@ChequeTitular').Value         := Trim(UpperCase(txtChequeNombreTitular.Text));
            sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@ChequeCodigoDocumento').Value := 'RUT';
            sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@ChequeNumeroDocumento').Value := PADL(Trim(txtChequeDocumento.Text), 9, '0');
            sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@ChequeMonto').Value           := FImportePagado * 100;
            sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@ChequeFecha').Value           := deChequeFecha.Date;
            sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@RepactacionNumero').Value     := iif(Trim(edtRepactacionCheque.Text) <> '',Trim(edtRepactacionCheque.Text),NULL); // Rev. 23 (SS 834)
        end;
        if FormaPago = tpValeVista then begin
            sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@ChequeCodigoBanco').Value     := cb_BancosVV.Value; // (SS 809)
            sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@ChequeNumero').Value          := Trim(UpperCase(txt_NroVV.Text));
            sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@ChequeTitular').Value         := Trim(UpperCase(txt_TomadoPorVV.Text));
            sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@ChequeCodigoDocumento').Value := 'RUT';
            sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@ChequeNumeroDocumento').Value := PADL(Trim(txt_RUTVV.Text), 9, '0');
            sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@ChequeMonto').Value           := FImportePagado * 100;
            sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@ChequeFecha').Value           := de_FechaVV.Date;
        end;
        if FormaPago = tpPagare then begin
            sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@ChequeCodigoBanco').Value     := cbPagareBancos.Value; // (SS 809)
            sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@ChequeNumero').Value	        := Trim(UpperCase(txtPagareNumero.Text));
            sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@ChequeTitular').Value         := Trim(UpperCase(txtPagareNombreTitular.Text));
            sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@ChequeCodigoDocumento').Value := 'RUT';
            sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@ChequeNumeroDocumento').Value := PADL(Trim(txtPagareDocumento.Text), 9, '0');
            sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@ChequeMonto').Value           := FImportePagado * 100;
            sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@ChequeFecha').Value           := dePagareFecha.Date;
            sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@RepactacionNumero').Value     := iif(Trim(edtRepactacion.Text) <> '',Trim(edtRepactacion.Text),NULL); // Rev. 22 (SS 809)
        end;

        sp_RegistrarDetallePagoComprobante.ExecProc;
        if sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@IDDetallePagoComprobante').Value = Null then begin
            MensajeError := MSG_ERROR_ID_DETALLE_IS_NULL;
            Exit;
        end;

    except
        on E: Exception do begin
            MensajeError := MSG_ERROR_ID_DETALLE_IS_NULL + CRLF + E.Message;
            Exit;
        end;
    end; // except

    if not FPagoNotaCobroInfraccion then begin      // NO SE PAGA NOTA DE COBRO INFRACCION
        for I := 0 to (FComprobantesCobrar.Count - 1) do begin

            Comprobante := TComprobante(FComprobantesCobrar.Items[I]);
            ImportePago := CalcularImportePagado(i);
            SumatoriaPagosControlRecibo := SumatoriaPagosControlRecibo + ImportePago;

            if ImportePago > 0 then begin
                try
                    sp_RegistrarPagoComprobante.Parameters.ParamByName('@IDDetallePagoComprobante').Value := sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@IDDetallePagoComprobante').Value;
                    sp_RegistrarPagoComprobante.Parameters.ParamByName('@TipoComprobante').Value          := Comprobante.TipoComprobante;
                    sp_RegistrarPagoComprobante.Parameters.ParamByName('@NumeroComprobante').Value        := Comprobante.NumeroComprobante;
                    sp_RegistrarPagoComprobante.Parameters.ParamByName('@FechaHora').Value                := FFechaHoraEmision;  //deFechaPago.Date; //Revision 16
                    sp_RegistrarPagoComprobante.Parameters.ParamByName('@NumeroRecibo').Value             := NumeroRecibo;
                    sp_RegistrarPagoComprobante.Parameters.ParamByName('@CodigoConceptoPago').Value       := ObtenerCodigoConceptoPago(DMConnections.BaseCAC, FCodigoCanalPago);
                    case FormaPago of
                        tpEfectivo      :sp_RegistrarPagoComprobante.Parameters.ParamByName('@DescripcionPago').Value := MSG_PAGO_EFECTIVO;
                        tpTarjetaCredito:sp_RegistrarPagoComprobante.Parameters.ParamByName('@DescripcionPago').Value := MSG_PAGO_TARJETA_CREDITO;
                        tpTarjetaDebito :sp_RegistrarPagoComprobante.Parameters.ParamByName('@DescripcionPago').Value := MSG_PAGO_TARJETA_DEBITO;
                        tpCheque        :sp_RegistrarPagoComprobante.Parameters.ParamByName('@DescripcionPago').Value := MSG_PAGO_CHEQUE;
                        tpValeVista     :sp_RegistrarPagoComprobante.Parameters.ParamByName('@DescripcionPago').Value := MSG_PAGO_VALE_VISTA;
                        tpDebitoCuenta  :sp_RegistrarPagoComprobante.Parameters.ParamByName('@DescripcionPago').Value := MSG_PAGO_DEBITO_CUENTA;
                        tpPagare        :sp_RegistrarPagoComprobante.Parameters.ParamByName('@DescripcionPago').Value := MSG_PAGO_PAGARE;
                    end;
                    sp_RegistrarPagoComprobante.Parameters.ParamByName('@Importe').Value     := ImportePago;
                    sp_RegistrarPagoComprobante.Parameters.ParamByName('@NumeroTurno').Value := GNumeroTurno;
                    sp_RegistrarPagoComprobante.Parameters.ParamByName('@Usuario').Value     := UsuarioSistema;

                    if I < (FComprobantesCobrar.Count - 1) then //si no se paga el �ltimo, se paga puntualmente este solo Rev 11
                        sp_RegistrarPagoComprobante.Parameters.ParamByName('@PagarSoloComprobanteSolicitado').Value     := 1
                    else
                        sp_RegistrarPagoComprobante.Parameters.ParamByName('@PagarSoloComprobanteSolicitado').Value     := 2;
                        // cuando es el ultimo, se paga primero ese y luego el resto de la lista de pendientes
                    sp_RegistrarPagoComprobante.Parameters.ParamByName('@NoGenerarIntereses').Value := FNoCalcularIntereses;  //SS_295_437_NDR_20121030
                              sp_RegistrarPagoComprobante.Parameters.ParamByName('@NumeroPago').Value := null;                          //SS_295_437_MDI_20130405

                    sp_RegistrarPagoComprobante.ExecProc;

                    if (sp_RegistrarPagoComprobante.Parameters.ParamByName('@NumeroPago').Value <> null) then
                        CerrarRegistroPagoComprobante(Comprobante.TipoComprobante, Comprobante.NumeroComprobante, FFechaHoraEmision)
                    else begin
                        MensajeError := Format(MSG_ERROR_NUMERO_PAGO_IS_NULL, [DescriTipoComprobante(Comprobante.TipoComprobante), Comprobante.NumeroComprobante]);
                        Exit;
                    end;

                    // Rev. 25 (Infractores Fase 2)
                    //
                    //if Comprobante.TieneConceptosInfractor then
                    //    with spActualizarInfraccionesPagadas do begin
                    //        Parameters.ParamByName('@NumeroComprobante').Value := Comprobante.NumeroComprobante;
                    //        Parameters.ParamByName('@TipoComprobante').Value   := Comprobante.TipoComprobante;
                    //        ExecProc;
                    //    end;
                    //
                    // Fin Rev. 25 (Infractores Fase 2)
                except
                    on e: Exception do begin
                        case FormaPago of
                            tpEfectivo      : MensajeDeError := MSG_ERROR_PAGO_EFECTIVO;
                            tpTarjetaCredito: MensajeDeError := MSG_ERROR_PAGO_TARJETA;
                            tpTarjetaDebito : MensajeDeError := MSG_ERROR_PAGO_TARJETA;
                            tpCheque        : MensajeDeError := MSG_ERROR_PAGO_CHEQUE;
                            tpValeVista     : MensajeDeError := MSG_ERROR_PAGO_VALE_VISTA;
                            tpDebitoCuenta  : MensajeDeError := MSG_ERROR_PAGO_DEBITO_CUENTA;
                            tpPagare        : MensajeDeError := MSG_ERROR_PAGO_PAGARE;
                         end;
                        MensajeError :=  MensajeDeError + CRLF + e.Message ;
                        Exit;
                    end;
                end;
            end;
        end;
        if (SumatoriaPagosControlRecibo <> (FImportePagado * 100)) then begin
            //la sumatoria de los pagos difiere del importe del recibo creado
            MensajeError := Format(MSG_ERROR_NO_BALANCEAN_PAGOS_CON_RECIBO, [NumeroRecibo, (SumatoriaPagosControlRecibo/100), FImportePagado]);
            Exit;
        end;

    end else begin      // SE PAGA NOTA DE COBRO INFRACCION
        Comprobante := TComprobante(FComprobantesCobrar.Items[0]);
        try
            RegistrarPagoNotaCobroInfraccion.Parameters.Refresh;
            RegistrarPagoNotaCobroInfraccion.Parameters.ParamByName('@IDDetallePagoComprobante').Value := sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@IDDetallePagoComprobante').Value;
            RegistrarPagoNotaCobroInfraccion.Parameters.ParamByName('@TipoComprobante').Value          := Comprobante.TipoComprobante;
            RegistrarPagoNotaCobroInfraccion.Parameters.ParamByName('@NumeroComprobante').Value        := Comprobante.NumeroComprobante;
            RegistrarPagoNotaCobroInfraccion.Parameters.ParamByName('@FechaHora').Value                := FFechaHoraEmision;  {deFechaPago.Date;} //Revision 16
            RegistrarPagoNotaCobroInfraccion.Parameters.ParamByName('@NumeroRecibo').Value             := NumeroRecibo;
            RegistrarPagoNotaCobroInfraccion.Parameters.ParamByName('@CodigoConceptoPago').Value       := ObtenerCodigoConceptoPago(DMConnections.BaseCAC, FCodigoCanalPago);
            case FormaPago of
                tpEfectivo      :RegistrarPagoNotaCobroInfraccion.Parameters.ParamByName('@DescripcionPago').Value := MSG_PAGO_EFECTIVO;
                tpTarjetaCredito:RegistrarPagoNotaCobroInfraccion.Parameters.ParamByName('@DescripcionPago').Value := MSG_PAGO_TARJETA_CREDITO;
                tpTarjetaDebito :RegistrarPagoNotaCobroInfraccion.Parameters.ParamByName('@DescripcionPago').Value := MSG_PAGO_TARJETA_DEBITO;
                tpCheque        :RegistrarPagoNotaCobroInfraccion.Parameters.ParamByName('@DescripcionPago').Value := MSG_PAGO_CHEQUE;
                tpValeVista     :RegistrarPagoNotaCobroInfraccion.Parameters.ParamByName('@DescripcionPago').Value := MSG_PAGO_VALE_VISTA;
                tpDebitoCuenta  :RegistrarPagoNotaCobroInfraccion.Parameters.ParamByName('@DescripcionPago').Value := MSG_PAGO_DEBITO_CUENTA;
                tpPagare        :RegistrarPagoNotaCobroInfraccion.Parameters.ParamByName('@DescripcionPago').Value := MSG_PAGO_PAGARE;
            end;
            RegistrarPagoNotaCobroInfraccion.Parameters.ParamByName('@Importe').Value     := neImportePagado.ValueInt * 100;
            RegistrarPagoNotaCobroInfraccion.Parameters.ParamByName('@NumeroTurno').Value := GNumeroTurno;
            RegistrarPagoNotaCobroInfraccion.Parameters.ParamByName('@Usuario').Value     := UsuarioSistema;
            RegistrarPagoNotaCobroInfraccion.Parameters.ParamByName('@NumeroPago').Value  := 0;
            RegistrarPagoNotaCobroInfraccion.ExecProc;
        except
            on e: Exception do begin
                case FormaPago of
                    tpEfectivo      : MensajeDeError := MSG_ERROR_PAGO_EFECTIVO;
                    tpTarjetaCredito: MensajeDeError := MSG_ERROR_PAGO_TARJETA;
                    tpTarjetaDebito : MensajeDeError := MSG_ERROR_PAGO_TARJETA;
                    tpCheque        : MensajeDeError := MSG_ERROR_PAGO_CHEQUE;
                    tpValeVista     : MensajeDeError := MSG_ERROR_PAGO_VALE_VISTA;
                    tpDebitoCuenta  : MensajeDeError := MSG_ERROR_PAGO_DEBITO_CUENTA;
                    tpPagare        : MensajeDeError := MSG_ERROR_PAGO_PAGARE;
                end;
                MensajeError := MensajeDeError + CRLF + e.Message;
                Exit;
            end;
        end;

    end;

    Result := True;
end;

function TfrmPagoVentanilla.GenerarPagoMultiplesCheques(NumeroRecibo: Integer; FormaPago:TFormaPago; var MensajeError: String): Boolean;
resourcestring
	MSG_PAGO_CHEQUE = 'Pago con cheque';
	MSG_ERROR_PAGO_CHEQUE = 'Error registrando un pago con cheque';
    MSG_ERROR_ID_DETALLE_IS_NULL = 'No se pudo registrar el Detalle del Pago';
var
	Comprobante: TComprobante;
begin
    // Rev.24 / 20-Enero-2010 / Nelson Droguett Sierra --------------------------------------------------------
    MensajeError:='';
    //---------------------------------------------------------------------------------------------------------
    Result := False;
    Comprobante := TComprobante(FComprobantesCobrar.Items[0]);
    //Recorro los cheques ingresados
    cdsCheques.DisableControls;
    try
        cdsCheques.First;
        //Mientras halla cheques
        while not cdsCheques.eof do begin
            //Registro detalle de cada pago
            try
                sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@CodigoTipoMedioPago').Value    := FCodigoCanalPago;
                sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@CodigoFormaPago').Value        := FCodigoFormaPago;
                sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@ChequeCodigoBanco').Value	    := cdsCheques.FieldByName('CodigoBanco').AsInteger;
                sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@ChequeNumero').Value	        := Trim(UpperCase(cdsCheques.FieldByName('NumeroCheque').asString));
                sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@ChequeTitular').Value          := Trim(UpperCase(cdsCheques.FieldByName('Titular').asString));
                sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@ChequeCodigoDocumento').Value	:= 'RUT';
                sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@ChequeNumeroDocumento').Value	:= PADL(Trim(cdsCheques.FieldByName('Rut').asString), 9, '0');
                sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@ChequeMonto').Value            := cdsCheques.FieldByName('Monto').AsFloat * 100;
                sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@ChequeFecha').Value            := cdsCheques.FieldByName('FechaCheque').AsDateTime;
                sp_RegistrarDetallePagoComprobante.CommandTimeout := 500;
                sp_RegistrarDetallePagoComprobante.ExecProc;

                if sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@IDDetallePagoComprobante').Value = Null then begin
                  MensajeError := MSG_ERROR_ID_DETALLE_IS_NULL;
                  Exit;
                end;
            except
                on e: Exception do begin
                    MensajeError := MSG_ERROR_PAGO_CHEQUE + CRLF + e.Message;
                    Exit;
                end;
            end;
            //Registro cada pago
            try
                RegistrarPagoNotaCobroInfraccion.Parameters.Refresh;
                RegistrarPagoNotaCobroInfraccion.Parameters.ParamByName('@IDDetallePagoComprobante').Value := sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@IDDetallePagoComprobante').Value;
                RegistrarPagoNotaCobroInfraccion.Parameters.ParamByName('@TipoComprobante').Value    := Comprobante.TipoComprobante;
                RegistrarPagoNotaCobroInfraccion.Parameters.ParamByName('@NumeroComprobante').Value  := Comprobante.NumeroComprobante;
                RegistrarPagoNotaCobroInfraccion.Parameters.ParamByName('@FechaHora').Value          := FFechaHoraEmision;  {deFechaPago.Date;} //Revision 16
                RegistrarPagoNotaCobroInfraccion.Parameters.ParamByName('@NumeroRecibo').Value       := NumeroRecibo;
                RegistrarPagoNotaCobroInfraccion.Parameters.ParamByName('@CodigoConceptoPago').Value := ObtenerCodigoConceptoPago(DMConnections.BaseCAC, FCodigoCanalPago);
                RegistrarPagoNotaCobroInfraccion.Parameters.ParamByName('@DescripcionPago').Value    := MSG_PAGO_CHEQUE;
                RegistrarPagoNotaCobroInfraccion.Parameters.ParamByName('@Importe').Value            := cdsCheques.FieldByName('Monto').AsFloat * 100;
                RegistrarPagoNotaCobroInfraccion.Parameters.ParamByName('@NumeroTurno').Value        := GNumeroTurno;
                RegistrarPagoNotaCobroInfraccion.Parameters.ParamByName('@Usuario').Value            := UsuarioSistema;
                RegistrarPagoNotaCobroInfraccion.Parameters.ParamByName('@NumeroPago').Value         := 0;
                RegistrarPagoNotaCobroInfraccion.CommandTimeout := 500;
                RegistrarPagoNotaCobroInfraccion.ExecProc;
            except
                on e: Exception do begin
                    MensajeError := MSG_ERROR_PAGO_CHEQUE + CRLF + e.Message;
                    Exit;
                end;
            end;

            cdsCheques.Next;
        end;
    finally
        cdsCheques.EnableControls;
    end;
    Result := True;
end;

procedure TfrmPagoVentanilla.chkNoCalcularInteresesClick(Sender: TObject);      //SS_295_437_NDR_20121030
begin                                                                           //SS_295_437_NDR_20121030
    FNoCalcularIntereses := chkNoCalcularIntereses.Checked;                     //SS_295_437_NDR_20121030
end;                                                                            //SS_295_437_NDR_20121030


{INICIO: TASK_032_JMA_20160706

function TfrmPagoVentanilla.CrearObjetoPagoComprobante(FormaPago:TFormaPago): TPagoComprobante;
resourcestring
    MSG_PAGO_EFECTIVO = 'Pago en efectivo';
	MSG_PAGO_TARJETA_CREDITO  = 'Pago con tarjeta de credito';
	MSG_PAGO_TARJETA_DEBITO  = 'Pago con tarjeta de d�bito';
	MSG_PAGO_DEBITO_CUENTA = 'Pago con d�bito cuenta';
	MSG_PAGO_CHEQUE = 'Pago con cheque';
	MSG_PAGO_VALE_VISTA = 'Pago con vale vista';
	MSG_PAGO_PAGARE                 = 'Pago con pagar�';

    MSG_ERROR_PAGO_EFECTIVO = 'Error registrando un pago en efectivo';
	MSG_ERROR_PAGO_TARJETA = 'Error registrando un pago con tarjeta de cr�dito/d�bito';
	MSG_ERROR_PAGO_DEBITO_CUENTA = 'Error registrando un pago con con d�bito cuenta';
	MSG_ERROR_PAGO_CHEQUE = 'Error registrando un pago con cheque';
	MSG_ERROR_PAGO_VALE_VISTA = 'Error registrando un pago con vale vista';
	MSG_ERROR_PAGO_PAGARE           = 'Error registrando un pago con pagar�';

    MSG_ERROR_ID_DETALLE_IS_NULL = 'No se pudo registrar el Detalle del Pago';
    MSG_ERROR_NUMERO_PAGO_IS_NULL = 'No se pudo registrar del Pago del Comprobante'#10#13'%s %d';
    MSG_ERROR_NO_BALANCEAN_PAGOS_CON_RECIBO = 'El Recibo %d es de un importe de %f y la suma de sus pagos asociados es de %f';
var
	I: Integer;
	Comprobante: TComprobante;
	MensajeDeError: string;
    ImportePago, SumatoriaPagosControlRecibo : Int64;
begin
    for I := 0 to (FComprobantesCobrar.Count - 1) do begin
			Comprobante := TComprobante(FComprobantesCobrar.Items[I]);
			ImportePago := CalcularImportePagado(i);
			SumatoriaPagosControlRecibo := SumatoriaPagosControlRecibo + ImportePago;

			if ImportePago > 0 then begin
				try
                    sp_RegistrarPagoComprobante.Parameters.ParamByName('@IDDetallePagoComprobante').Value := sp_RegistrarDetallePagoComprobante.Parameters.ParamByName('@IDDetallePagoComprobante').Value;
                    sp_RegistrarPagoComprobante.Parameters.ParamByName('@TipoComprobante').Value          := Comprobante.TipoComprobante;
                    sp_RegistrarPagoComprobante.Parameters.ParamByName('@NumeroComprobante').Value        := Comprobante.NumeroComprobante;
                    sp_RegistrarPagoComprobante.Parameters.ParamByName('@FechaHora').Value                := FFechaHoraEmision;  //deFechaPago.Date; //Revision 16
                    sp_RegistrarPagoComprobante.Parameters.ParamByName('@NumeroRecibo').Value             := NumeroRecibo;
                    sp_RegistrarPagoComprobante.Parameters.ParamByName('@CodigoConceptoPago').Value       := ObtenerCodigoConceptoPago(DMConnections.BaseCAC, FCodigoCanalPago);
                    case FormaPago of
                        tpEfectivo      :sp_RegistrarPagoComprobante.Parameters.ParamByName('@DescripcionPago').Value := MSG_PAGO_EFECTIVO;
                        tpTarjetaCredito:sp_RegistrarPagoComprobante.Parameters.ParamByName('@DescripcionPago').Value := MSG_PAGO_TARJETA_CREDITO;
                        tpTarjetaDebito :sp_RegistrarPagoComprobante.Parameters.ParamByName('@DescripcionPago').Value := MSG_PAGO_TARJETA_DEBITO;
                        tpCheque        :sp_RegistrarPagoComprobante.Parameters.ParamByName('@DescripcionPago').Value := MSG_PAGO_CHEQUE;
                        tpValeVista     :sp_RegistrarPagoComprobante.Parameters.ParamByName('@DescripcionPago').Value := MSG_PAGO_VALE_VISTA;
                        tpDebitoCuenta  :sp_RegistrarPagoComprobante.Parameters.ParamByName('@DescripcionPago').Value := MSG_PAGO_DEBITO_CUENTA;
                        tpPagare        :sp_RegistrarPagoComprobante.Parameters.ParamByName('@DescripcionPago').Value := MSG_PAGO_PAGARE;
                    end;
                    sp_RegistrarPagoComprobante.Parameters.ParamByName('@Importe').Value     := ImportePago;
                    sp_RegistrarPagoComprobante.Parameters.ParamByName('@NumeroTurno').Value := GNumeroTurno;
                    sp_RegistrarPagoComprobante.Parameters.ParamByName('@Usuario').Value     := UsuarioSistema;

                    if I < (FComprobantesCobrar.Count - 1) then //si no se paga el �ltimo, se paga puntualmente este solo Rev 11
                        sp_RegistrarPagoComprobante.Parameters.ParamByName('@PagarSoloComprobanteSolicitado').Value     := 1
                    else
                        sp_RegistrarPagoComprobante.Parameters.ParamByName('@PagarSoloComprobanteSolicitado').Value     := 2;
                        // cuando es el ultimo, se paga primero ese y luego el resto de la lista de pendientes
                    sp_RegistrarPagoComprobante.Parameters.ParamByName('@NoGenerarIntereses').Value := FNoCalcularIntereses;  //SS_295_437_NDR_20121030
					          sp_RegistrarPagoComprobante.Parameters.ParamByName('@NumeroPago').Value := null;                          //SS_295_437_MDI_20130405

					sp_RegistrarPagoComprobante.ExecProc;

					if (sp_RegistrarPagoComprobante.Parameters.ParamByName('@NumeroPago').Value <> null) then
						CerrarRegistroPagoComprobante(Comprobante.TipoComprobante, Comprobante.NumeroComprobante, FFechaHoraEmision)
					else begin
						MensajeError := Format(MSG_ERROR_NUMERO_PAGO_IS_NULL, [DescriTipoComprobante(Comprobante.TipoComprobante), Comprobante.NumeroComprobante]);
						Exit;
					end;
				except
					on e: Exception do begin
                        case FormaPago of
                            tpEfectivo      : MensajeDeError := MSG_ERROR_PAGO_EFECTIVO;
                            tpTarjetaCredito: MensajeDeError := MSG_ERROR_PAGO_TARJETA;
                            tpTarjetaDebito : MensajeDeError := MSG_ERROR_PAGO_TARJETA;
                            tpCheque        : MensajeDeError := MSG_ERROR_PAGO_CHEQUE;
                            tpValeVista     : MensajeDeError := MSG_ERROR_PAGO_VALE_VISTA;
                            tpDebitoCuenta  : MensajeDeError := MSG_ERROR_PAGO_DEBITO_CUENTA;
                            tpPagare        : MensajeDeError := MSG_ERROR_PAGO_PAGARE;
                         end;
                        MensajeError :=  MensajeDeError + CRLF + e.Message ;
                        Exit;
					end;
				end;
			end;
		end;
end;

TERMINO: TASK_032_JMA_20160706}

function TfrmPagoVentanilla.GenerarPagoComprobante(FormaPago: TFormaPago; var MensajeError: String; var NumeroRecibo: Int64): Boolean;
resourcestring
    MSG_PAGO_EFECTIVO = 'Pago en efectivo';
	MSG_PAGO_TARJETA_CREDITO  = 'Pago con tarjeta de credito';
	MSG_PAGO_TARJETA_DEBITO  = 'Pago con tarjeta de d�bito';
	MSG_PAGO_DEBITO_CUENTA = 'Pago con d�bito cuenta';
	MSG_PAGO_CHEQUE = 'Pago con cheque';
	MSG_PAGO_VALE_VISTA = 'Pago con vale vista';
	MSG_PAGO_PAGARE                 = 'Pago con pagar�';
var
	Comprobante: TComprobante;
    sp: TADOStoredProc;

begin
	Result := False;
	MensajeError := '';

    try

        sp := TADOStoredProc.Create(nil);
        sp.Connection := DMConnections.BaseCAC;
        sp.ProcedureName := 'PagarConvenio';
        sp.Parameters.Refresh;

        sp.Parameters.ParamByName('@FechaHora').Value := FFechaHoraEmision;
        sp.Parameters.ParamByName('@Importe').Value := FImportePagado * 100;
        sp.Parameters.ParamByName('@CodigoTipoMedioPago').Value := FCodigoCanalPago;
        sp.Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema;
        sp.Parameters.ParamByName('@CodigoFormaPago').Value := FCodigoFormaPago;

        if FormaPago = tpEfectivo then
            sp.Parameters.ParamByName('@DescripcionPago').Value := MSG_PAGO_EFECTIVO
        else if FormaPago = tpTarjetaDebito then begin
            sp.Parameters.ParamByName('@DescripcionPago').Value := MSG_PAGO_TARJETA_DEBITO;
            sp.Parameters.ParamByName('@Titular').Value :=  Trim(edtTarjetaDebitoTitular.Text);
            sp.Parameters.ParamByName('@CodigoBancoEmisor').Value := vcbTarjetaDebitoBancos.Value;
            sp.Parameters.ParamByName('@Autorizacion').Value := Trim(UpperCase(edtTarjetaDebitoNroConfirmacion.Text));
            sp.Parameters.ParamByName('@NumeroPOS').Value := IIf(FNumeroPOS>0,FNumeroPOS, null);
        end
        else if FormaPago = tpTarjetaCredito then begin
            sp.Parameters.ParamByName('@DescripcionPago').Value := MSG_PAGO_TARJETA_CREDITO;
            sp.Parameters.ParamByName('@Titular').Value := Trim(edtTarjetaCreditoTitular.Text);
            sp.Parameters.ParamByName('@CodigoBancoEmisor').Value := vcbTarjetaCreditoBancos.Value;
            sp.Parameters.ParamByName('@Autorizacion').Value := Trim(UpperCase(edtTarjetaCreditoNroConfirmacion.Text));
            sp.Parameters.ParamByName('@NumeroPOS').Value := IIf(FNumeroPOS>0,FNumeroPOS, null);
            sp.Parameters.ParamByName('@CodigoTipoTarjeta').Value := vcbTarjetaCreditoTarjetas.Value;
            sp.Parameters.ParamByName('@Cuotas').Value := nedtTarjetaCreditoCuotas.Value;
        end
        else if FormaPago = tpDebitoCuenta then begin
            sp.Parameters.ParamByName('@DescripcionPago').Value := MSG_PAGO_DEBITO_CUENTA;
            if FCodigoCanalPago = CONST_CANAL_PAGO_PAC then begin
                sp.Parameters.ParamByName('@PAC_CodigoBanco').Value := cb_BancosPAC.Value;
                sp.Parameters.ParamByName('@PAC_NroCuentaBancaria').Value := Trim(txt_NroCuentaBancariaPAC.Text);
                sp.Parameters.ParamByName('@PAC_Sucursal').Value := Trim(txt_SucursalPAC.Text);
                sp.Parameters.ParamByName('@PAC_Titular').Value := Trim(txt_TitularPAC.Text);
            end else begin
                sp.Parameters.ParamByName('@PAT_CodigoTipoTarjetaCredito').Value := iif(cb_TipoTarjetaCreditoPAT.Value = 0, '', cb_TipoTarjetaCreditoPAT.Value);
                sp.Parameters.ParamByName('@PAT_NumeroTarjetaCredito').Value := iif(Trim(txt_NumeroTarjetaCreditoPAT.Text) = EmptyStr, '', Trim(txt_NumeroTarjetaCreditoPAT.Text));
                sp.Parameters.ParamByName('@PAT_FechaVencimiento').Value := iif(Trim(txt_FechaVencimiento.Text) = EmptyStr, '', Trim(txt_FechaVencimiento.Text));
                sp.Parameters.ParamByName('@PAT_CodigoEmisorTarjetaCredito').Value := iif(cb_EmisorTarjetaCreditoPAT.Value = 0, '', cb_EmisorTarjetaCreditoPAT.Value);
            end;
        end
        else if FormaPago = tpCheque then begin
            sp.Parameters.ParamByName('@DescripcionPago').Value := MSG_PAGO_CHEQUE;
            sp.Parameters.ParamByName('@ChequeCodigoBanco').Value := cbChequeBancos.Value;
            sp.Parameters.ParamByName('@ChequeNumero').Value := Trim(UpperCase(txtChequeNumero.Text));
            sp.Parameters.ParamByName('@ChequeTitular').Value := Trim(UpperCase(txtChequeNombreTitular.Text));
            sp.Parameters.ParamByName('@ChequeCodigoDocumento').Value := 'RUT';
            sp.Parameters.ParamByName('@ChequeNumeroDocumento').Value := PADL(Trim(txtChequeDocumento.Text), 9, '0');
            sp.Parameters.ParamByName('@ChequeMonto').Value := FImportePagado * 100;
            sp.Parameters.ParamByName('@ChequeFecha').Value := deChequeFecha.Date;
            sp.Parameters.ParamByName('@RepactacionNumero').Value := iif(Trim(edtRepactacionCheque.Text) <> '',Trim(edtRepactacionCheque.Text),'');
        end
        else if FormaPago = tpValeVista then begin
            sp.Parameters.ParamByName('@DescripcionPago').Value := MSG_PAGO_VALE_VISTA;
            sp.Parameters.ParamByName('@ChequeCodigoBanco').Value := cb_BancosVV.Value;
            sp.Parameters.ParamByName('@ChequeNumero').Value := Trim(UpperCase(txt_NroVV.Text));
            sp.Parameters.ParamByName('@ChequeTitular').Value := Trim(UpperCase(txt_TomadoPorVV.Text));
            sp.Parameters.ParamByName('@ChequeCodigoDocumento').Value := 'RUT';
            sp.Parameters.ParamByName('@ChequeNumeroDocumento').Value := PADL(Trim(txt_RUTVV.Text), 9, '0');
            sp.Parameters.ParamByName('@ChequeMonto').Value := FloatToStr(FImportePagado * 100);
            sp.Parameters.ParamByName('@ChequeFecha').Value := de_FechaVV.Date;
        end
        else if FormaPago = tpPagare then begin
            sp.Parameters.ParamByName('@DescripcionPago').Value := MSG_PAGO_PAGARE;
            sp.Parameters.ParamByName('@ChequeCodigoBanco').Value := cbPagareBancos.Value;
            sp.Parameters.ParamByName('@ChequeNumero').Value := Trim(UpperCase(txtPagareNumero.Text));
            sp.Parameters.ParamByName('@ChequeTitular').Value := Trim(UpperCase(txtPagareNombreTitular.Text));
            sp.Parameters.ParamByName('@ChequeCodigoDocumento').Value := 'RUT';
            sp.Parameters.ParamByName('@ChequeNumeroDocumento').Value := PADL(Trim(txtPagareDocumento.Text), 9, '0');
            sp.Parameters.ParamByName('@ChequeMonto').Value := FImportePagado * 100;
            sp.Parameters.ParamByName('@ChequeFecha').Value := dePagareFecha.Date;
            sp.Parameters.ParamByName('@RepactacionNumero').Value := iif(Trim(edtRepactacion.Text) <> '',Trim(edtRepactacion.Text),'');
        end;

        sp.Parameters.ParamByName('@NumeroTurno').Value := IntToStr(GNumeroTurno);
        sp.Parameters.ParamByName('@NoGenerarIntereses').Value := BoolToStr(FNoCalcularIntereses);
        sp.Parameters.ParamByName('@CodigoConceptoPago').Value := IntToStr(ObtenerCodigoConceptoPago(DMConnections.BaseCAC, FCodigoCanalPago));

        if FComprobantesCobrar.Count>0 then
        begin
            Comprobante := TComprobante(FComprobantesCobrar.Items[0]);
            sp.Parameters.ParamByName('@TipoComprobante').Value := Comprobante.TipoComprobante;
            sp.Parameters.ParamByName('@NumeroComprobante').Value := IntToStr(Comprobante.NumeroComprobante);
        end;
        sp.Parameters.ParamByName('@NumeroRecibo').Value := null;

        sp.ExecProc;

        if sp.Parameters.ParamByName('@RETURN_VALUE').Value = 0 then
        begin
            Result:= True;
            NumeroRecibo := sp.Parameters.ParamByName('@NumeroRecibo').Value;
        end
        else
        begin
            MensajeError := sp.Parameters.ParamByName('@ErrorDescription').Value;
        end;

    except
        on E: Exception do begin
            MensajeError := e.Message;
        end;
    end;

end;


end.


