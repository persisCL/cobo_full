{-------------------------------------------------------------------------------
 File Name: FrmTransitosReclamadosAcciones.pas
 Author: lgisuk
 Date Created: 05/07/05
 Language: ES-AR
 Description: Informo que se realizaron acciones sobre los siguientes transitos
              porque la empresa acepto el reclamo del cliente.
              Las Acciones son:
                - Se Genero Ajuste.
                - Se Pasaron los Transitos a No Facturable.
                - Se Dio infracci�n por Anulada.
                
Author      : Claudio Quezada Ib��ez
Date        : 16-Mayo-2013
Firma       : SS_1091_CQU_20130516
Description : Se modifica la visualizaci�n de im�genes para poder desplegar im�genes de p�rticos Italianos.
-------------------------------------------------------------------------------}
unit FrmTransitosReclamadosAcciones;

interface

uses
  //Reclamo
  DMConnection,                   //Coneccion a base de datos OP_CAC
  Util,                           //CRLF
  UtilProc,                       //Mensajes
  ImagenesTransitos,              //Muestra la Ventana con la imagen
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls,  DB, ADODB,RStrings, ExtCtrls,
  VariantComboBox, Grids, DBGrids, ListBoxEx, DBListEx, ImgList, Menus;

type
  TFormTransitosReclamadosAcciones = class(TForm)
    Ltitulo: TLabel;
    DataSource: TDataSource;
    spObtenerTransitosReclamadosAcciones: TADOStoredProc;
    DBLTransitos: TDBListEx;
    ilCamara: TImageList;
    btnCerrar: TButton;
    procedure DBLTransitosDrawText(Sender: TCustomDBListEx;Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;var Text: String; var TxtRect: TRect; var ItemWidth: Integer;var DefaultDraw: Boolean);
    procedure DBLTransitosLinkClick(Sender: TCustomDBListEx;Column: TDBListExColumn);
    procedure btnCerrarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function Inicializar(ListaTransitos:tstringlist): Boolean;
  end;

var
  FormTransitosReclamadosAcciones: TFormTransitosReclamadosAcciones;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    lgisuk
  Date Created: 05/07/05
  Description: Incializacion de este formulario
  Parameters: ListaTransitos:tstringlist
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFormTransitosReclamadosAcciones.Inicializar(ListaTransitos:tstringlist): Boolean;

    {-----------------------------------------------------------------------------
      Function Name: ObtenerDatosTransitos
      Author:    lgisuk
      Date Created: 05/07/05
      Description: Cargo la grilla de transitos reclamados
      Parameters: Transitos:string
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function ObtenerDatosTransitos(Transitos : TStringList):boolean;

            {----------------------------------------------------------------
              Function Name: ObtenerEnumeracion
              Author:    lgisuk
              Date Created: 23/09/05
              Description: Convierto la lista a un string separado por comas
              Parameters: Transitos:string
              Return Value: string
            -----------------------------------------------------------------}
            function ObtenerEnumeracion(Lista: TStringList): String;
            var
                Enum: AnsiString;
                i: Integer;
            begin
                Enum := '';
                for i:= 0 to Lista.Count - 1 do begin
                    //Enum := Enum + Lista.Strings[i] + ',';
                    Enum := Enum + ParseParamByNumber(Lista.Strings[i],1,'=') + ',';
                end;
                Delete(Enum, Length(Enum), 1);
                Result := Enum;
            end;

    begin
        with spObtenerTransitosReclamadosAcciones do begin
            close;
            parameters.Refresh;
            parameters.ParamByName('@Enumeracion').value := ObtenerEnumeracion(Transitos);
            open;
        end;
        result:=true;
    end;

resourcestring
    MSG_ERROR   = 'Error';
begin
    Result := False;
    try
        //Si es un reclamo nuevo obtengo los reclamos del stringlist
        if ListaTransitos <> nil then begin
            //Cargo la grilla con los transitos
            ObtenerDatosTransitos(ListaTransitos);
            //Si la consulta viene vacia no muestro el cartel
            if SPObtenerTransitosReclamadosAcciones.RecordCount = 0 then exit;
        end;
        //Centro el form
        CenterForm(Self);
        //Titulo
        ActiveControl := dbltransitos;
        Result := true;
    except
        on e: exception do begin
            MsgBox(e.message, MSG_ERROR, MB_ICONSTOP);
            Result := False;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: DBLTransitosDrawText
  Author:    lgisuk
  Date Created: 05/07/05
  Description: doy formato a la grilla
  Parameters: Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;State: TOwnerDrawState; var Text: String; var TxtRect: TRect;var ItemWidth: Integer; var DefaultDraw: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormTransitosReclamadosAcciones.DBLTransitosDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;State: TOwnerDrawState; var Text: String; var TxtRect: TRect;var ItemWidth: Integer; var DefaultDraw: Boolean);

    {-----------------------------------------------------------------------------
      Function Name: DibujarBitmap
      Author:    lgisuk
      Date Created: 05/07/05
      Description: Dibujo el bitmap seleccionado en la columna de la grilla
      Parameters: Grilla:TCustomDBlistEx;ImageList:TimageList;numero:integer
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function DibujarBitmap(Grilla: TCustomDBlistEx; ImageList: TimageList; IndiceImg: integer):boolean;
    var
        bmp: TBitMap;
    begin
        bmp := TBitMap.Create;
        try
            ImageList.GetBitmap(IndiceImg, Bmp);
            Grilla.Canvas.Draw((Rect.Left + Rect.Right - Bmp.Width) div 2, Rect.Top, Bmp);
            Result := true;
        finally
            bmp.Free;
        end;
    end;
    
var
    i:integer;
begin
    //FechaHora
    if Column = dbltransitos.Columns[2] then begin
       if text <> '' then Text := FormatDateTime('dd/mm/yyyy hh:nn', spObtenerTransitosReclamadosAcciones.FieldByName('FechaHora').AsDateTime);
    end;

    //si el transito tiene imagen le crea un link para que se pueda acceder a verla
    if Column = dblTransitos.Columns[3] then begin
        i:=iif(spObtenerTransitosReclamadosAcciones.FieldByName('RegistrationAccessibility').AsInteger > 0, 1, 0);
        //Dibujo el icono correspondiente
        DibujarBitmap(dbltransitos,ilCamara,i);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: DBLTransitosLinkClick
  Author:    lgisuk
  Date Created: 05/07/05
  Description: permito rechazar o aceptar un transito
  Parameters: Sender: TCustomDBListEx; Column: TDBListExColumn
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormTransitosReclamadosAcciones.DBLTransitosLinkClick(Sender: TCustomDBListEx; Column: TDBListExColumn);
begin
    //Mostrar Imagen del Transito
    if Column = dblTransitos.Columns[3] then begin
        //Si hay imagen
        if spObtenerTransitosReclamadosAcciones.FieldByName('RegistrationAccessibility').AsInteger > 0 then begin
            //La muestra
            //MostrarVentanaImagen(Self,spObtenerTransitosReclamadosAcciones['NumCorrCA'], spObtenerTransitosReclamadosAcciones['FechaHora']);  // SS_1091_CQU_20130516
            MostrarVentanaImagen(Self,                                                                                                          // SS_1091_CQU_20130516
                spObtenerTransitosReclamadosAcciones['NumCorrCA'],                                                                              // SS_1091_CQU_20130516
                spObtenerTransitosReclamadosAcciones['FechaHora'],                                                                              // SS_1091_CQU_20130516
                spObtenerTransitosReclamadosAcciones['EsItaliano']);                                                                            // SS_1091_CQU_20130516
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: btnCerrarClick
  Author:    lgisuk
  Date Created: 05/07/05
  Description: Permito cerrar el formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormTransitosReclamadosAcciones.btnCerrarClick(Sender: TObject);
begin
    ModalResult := mrYes;
end;

end.
