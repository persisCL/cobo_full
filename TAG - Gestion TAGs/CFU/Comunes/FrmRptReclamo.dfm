object FRptReclamo: TFRptReclamo
  Left = 247
  Top = 196
  Caption = 'FRptReclamo'
  ClientHeight = 133
  ClientWidth = 283
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object rptReclamo: TppReport
    AutoStop = False
    DataPipeline = ppDBPipeline
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = 'Report'
    PrinterSetup.PaperName = 'A4'
    PrinterSetup.PrinterName = 'Default'
    PrinterSetup.SaveDeviceSettings = False
    PrinterSetup.mmMarginBottom = 2540
    PrinterSetup.mmMarginLeft = 2540
    PrinterSetup.mmMarginRight = 2540
    PrinterSetup.mmMarginTop = 2540
    PrinterSetup.mmPaperHeight = 297127
    PrinterSetup.mmPaperWidth = 210079
    PrinterSetup.PaperSize = 9
    DeviceType = 'Screen'
    EmailSettings.ReportFormat = 'PDF'
    LanguageID = 'Default'
    OutlineSettings.CreateNode = True
    OutlineSettings.CreatePageNodes = True
    OutlineSettings.Enabled = True
    OutlineSettings.Visible = True
    PDFSettings.EmbedFontOptions = [efUseSubset]
    PDFSettings.EncryptSettings.AllowCopy = True
    PDFSettings.EncryptSettings.AllowInteract = True
    PDFSettings.EncryptSettings.AllowModify = True
    PDFSettings.EncryptSettings.AllowPrint = True
    PDFSettings.EncryptSettings.Enabled = False
    PDFSettings.FontEncoding = feAnsi
    RTFSettings.DefaultFont.Charset = DEFAULT_CHARSET
    RTFSettings.DefaultFont.Color = clWindowText
    RTFSettings.DefaultFont.Height = -13
    RTFSettings.DefaultFont.Name = 'Arial'
    RTFSettings.DefaultFont.Style = []
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = True
    Left = 105
    Top = 8
    Version = '12.04'
    mmColumnWidth = 203200
    DataPipelineName = 'ppDBPipeline'
    object ppHeaderBand1: TppHeaderBand
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 95250
      mmPrintPosition = 0
      object ppImage2: TppImage
        UserName = 'Image2'
        AlignHorizontal = ahCenter
        AlignVertical = avCenter
        AutoSize = True
        MaintainAspectRatio = False
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Picture.Data = {
          0A544A504547496D6167659E160000FFD8FFE000104A46494600010001006000
          600000FFFE001F4C45414420546563686E6F6C6F6769657320496E632E205631
          2E303100FFDB0084000505050805080C07070C0C0909090C0D0C0C0C0C0D0D0D
          0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D
          0D0D0D0D0D0D0D0D0D010508080A070A0C07070C0D0C0A0C0D0D0D0D0D0D0D0D
          0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D
          0D0D0D0D0D0D0D0D0D0DFFC401A2000001050101010101010000000000000000
          0102030405060708090A0B010003010101010101010101000000000000010203
          0405060708090A0B100002010303020403050504040000017D01020300041105
          122131410613516107227114328191A1082342B1C11552D1F02433627282090A
          161718191A25262728292A3435363738393A434445464748494A535455565758
          595A636465666768696A737475767778797A838485868788898A929394959697
          98999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3
          D4D5D6D7D8D9DAE1E2E3E4E5E6E7E8E9EAF1F2F3F4F5F6F7F8F9FA1100020102
          0404030407050404000102770001020311040521310612415107617113223281
          08144291A1B1C109233352F0156272D10A162434E125F11718191A262728292A
          35363738393A434445464748494A535455565758595A636465666768696A7374
          75767778797A82838485868788898A92939495969798999AA2A3A4A5A6A7A8A9
          AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3E4E5
          E6E7E8E9EAF2F3F4F5F6F7F8F9FAFFC0001108004600FA030111000211010311
          01FFDA000C03010002110311003F00FB2E802A5F5D0B1824B820B0894B6077C0
          CE2AE11E7928AEAD231AB3F650954FE54DFDC70C3E2043FF003C1FFEFA15EB7D
          42695EE8F9A59CC1B4B91EF6FEB53D0227F31038E37007F3AF1DAB687D445F32
          52EE4948B0A002800A002800A002800A002800A002800A002800A002800A0028
          00A002800A002800A002800A002800A002800A002803335881AE2CE6863C6E78
          D80C9C0C91DCF6ADA94B92719766BF339311075294E11DDC5A5F733C847856F8
          63FD576FF96AB5F4EF1906AD67B3E8CF808E595A2D3D374F75DCF6BB75291AA9
          EAAA07E42BE4DEECFD1A0B96297644D9A468412DD4301C4922213D0332AFF322
          A945BD93FB89E64BAAFBC8BFB46D7FE7B45FF7F13FC69F24BB3FB98B9A3DD7DE
          83FB46D7FE7B45FF007F13FC68E49767F730E68F75F7A0FED1B5FF009ED17FDF
          C4FF001A3925D9FDCC39A3DD7DE83FB46D7FE7B45FF7F17FC68E49767F730E68
          F75F7A0FED1B5FF9ED17FDFC5FF1A3925D9FDCC39A3DD7DE83FB46D7FE7B45FF
          007F17FC68E49767F730E68F75F7A0FED1B5FF009ED17FDFC5FF001A3925D9FD
          CC39A3DD7DE83FB46D7FE7B45FF7F17FC68E59767F730E68F75F7A268AE229BF
          D53ABE3AED607F91A969ADD343524F66BEF26CD2282800A002800A002800A002
          800A002800A002800A002800A002800A0028038DF88448F0E6A4471FE87374FF
          0070D6D4BE38FAA32A9F04ADD8FCE759A4C2FCEFFC3FC6DEA3DEBEA5C63CBB2B
          D8F998CE4A56BBDCFD3CD38E2D61CFFCF28FFF004015F22F73EA56C9F91F2A7C
          4FF8D3772DCCBA478764F220858C72DDA60C9238386584F4445391BC7CCC7EE9
          02BD9C3E155B9EAAF447915F1367CB4F63E73B8BC9AE5CCB712C923B1C967919
          989F724E4D7ACA118E892FC0F2DCA6F66FF120F371FC47FEFA3FE35568AE8BF0
          15E5DDFE21E77FB47FEFA3FE345A3D97E017979FE21E77FB47FEFA3FE345A3D9
          7E017979FE21E77FB47FEFA3FE345A3D97E017979FE21E6FFB47FEFA3FE34AD1
          ECBF00BCBCC4F3BFDA3FF7D1FF001A768F65F805E5DDFE22F9DFED1FFBE8FF00
          8D2B47B2FC02F2F3FC43CEFF0068FF00DF47FC69DA3D97E0179777F8976C753B
          BD2E55B8B29E5B7950E55E391948FC8E08F50720F7A974E1256697E0353941DD
          367D95F07BE2849E3047D2F542BFDA36E9BD640028B888705B6F41221C6F0382
          0EE00722BE7F1387F62F9A3F0FE47BB87AFED572BDCF7515E76C7A014C02800A
          002800A002800A002800A002800A002800A002800A0028038DF887FF0022DEA5
          FF005E737FE806B6A5FC48FF00897E66553E097A1F9C4BC05FAAFF00315F592D
          9FA1F2F1F8BE67E8E7882EA5B1F0CDC5C5BE7CD8F4F665C7507C9EA3DD7AFE15
          F27049D449F73E9A7A53BADEC7E718385CF538CD7D6EC92EC7CBBDCFBB7C01F0
          E7C37068F6D7096F05FC9710A48F712A890B3300580CE422AB657680318E79AF
          98AB5AA73B576926CFA1A3469A82695EE8EDFF00E105D03FE81D67FF007E23FF
          000AC7DACFF99FDE747B287F2A0FF8417401FF0030EB3FFBF11FF851ED67FCCC
          3D943F9507FC20DA00FF00987D9FFDF84FF0A5ED67B7330F650FE541FF00082E
          81FF0040FB3FFBF11FF851ED67FCCC3D943F9507FC20BA07FD03ECFF00EFC47F
          E147B59FF330F650FE541FF083681FF40FB3FF00BF11FF00851ED67FCCC3D943
          F9507FC20BA07FD03ED3FEFC47FE14FDACFF009987B287F2A0FF008417401FF3
          0EB3FF00BF11FF00851ED67FCCC3D943F951F38FC75F01697E1FB7B7D5F4B896
          D1A59BC99628C6236CA960E17A2B02B838C0607A679AF570956526E3377D343C
          CC5528C1294558F2BF8597525A78A74E688ED2F3F96DDB2AEACAC0FD476AEDC4
          ABD395FB5CE2C3DD548D8FD0CE95F2C7D306698150DFDB8B8FB1F991FDA0A798
          22DC3CC299C6FD99DDB73C6EC633C53B3B5FA0AEAF6EA32CB54B4D44BADA4D14
          E616D9208A45731BFF0075F693B5BD8E0D0E2E3BAF412927B152E3C47A659A3C
          B3DDDB451C52F92ECF322AA4B8CF94C4B00B2639DA7E6C738A6A327A242E64BA
          94D3C69A148CA89A8D9333901545CC44924E00037E492780075ABF6735BC5895
          48F71F75E2ED16C65682E6FECE1963387492E225653E8CA58107D88A4A126AE9
          0F9E29DAE58BAF12697611473DCDDDB4315C0DD13C93468B20C0398D8B00C304
          1CAE7822928C9DD25B07325BB1E9AFE9CF6C6FD2EADDAD53EF4C254312F38E5C
          36D1C9C726972C93B583993574CD412AEDDF91B31BB7678C6339CF4C639CFA54
          EDA15E667C1AD58DCF95E4DC4127DAB71836C887CDD9F7FCBC1F9F6FF16DCE3B
          D538B57D36DC9524F67BEC4B7BAA5AE9BB3ED73456FE6B048FCD754DEE7A2AEE
          23731F41CD249CB61B6A3B925E5F41A7C46E2EA448224C6E7918222E4E065988
          0324E07BD249BD16E36EDAB2D2B06191C83C83F5A062D00140050071BF10FF00
          E45BD4BFEBCE6FFD00D6D4BF891FF12FCCCAA7C12F467E71A7017FE03FCC57D6
          4B67E87CB47E2F99FA756D6E975611C128DD1CB6EA8EA7A1568C2B0FC4135F1D
          B3BADEE7D5A578A4FB1F02FC40F00DE7812F9E29559AC64626DAE00F91909C84
          63D16441C1071BB1B8641AFA7A15E3563ABB491F3B5A8CA9B765A1CFE95E29D5
          F4243169D79716B1939291C84264F7DBC807DC0ADA54613D64AE631AB3869176
          35BFE163789BFE82577FF7DFFF005AB2FAB52FE52FDB54EE1FF0B1BC4DFF0041
          2BAFFBEFFF00AD47D5E97F2A0F6F35D4DBD1BE3178A3479031BB37680E4C572A
          1D587A6E003AFD41E3D2B39E169C968AC690C4CE2F7D0FACFE1E7C4AB2F1EC0C
          235FB35EC0019ADD8E700F1BE33C6F8C9E338CA9E187427C4AD41D07AEDD19ED
          51AD1AAB4D197FC77E3EB0F01DA0B8BBCCB3CB9105BA11BE461D4E7F8517F89C
          F03A0C9E2A29529567CB1DBBF62AAD58D1577B9F236BBF1A7C4DACC8C62B8161
          093F2C56EA06076CC8C0BB1F7C81ED5EEC3094E0ACF5678B3C54E5B688E6FF00
          E163789BFE82577FF7DFFF005AB5FABD3FE5463EDE7DD87FC2C7F137FD04AEFF
          00EFBFFEB51F57A5FCA83DBD4EE775E22D5EF35BF005A5D6A133DCCFFDAB22F9
          921CB6D08D819F41DAB969C142B351565CABF3675CE4E745393EAFF24711F0D7
          FE468D33FEBE93F91AECC47F0E5E8CE4A1F1C6DDCFA82F35BBF49E4559E40164
          70067A00C78E9574F0D4A504F975B1F31571B5E352515376BBFEB63D53C2F3C9
          73A7C52CCC5DCEECB1EA70C6BE7313150A8E31D123EEB2FA92AB42339BBB6DFE
          870728FF008B8919FF00A82B7FE941A3FE5CAFF1FE875FFCBDB7F77FCCF23F04
          CAFE0F9CF8A149FB0DDEAD7BA76A4BFC28A6E0FD9AE31DB6392AC7D1BAF35D75
          6D35ECBAA5CCBCF7D0E585E9BF69D1BB3F23D57C03A6DA6A936BD15E4315CC5F
          DB72B859115D73E54586018119C1E0FA1AE6ACDC392DA3B7F91D149295FB5FFC
          CCFF0086FE1ED326BFD70C9696EDF67D55845BA143E585452A132BF280790171
          CD5569CAD0B3DE3AFDC89A5057936B66FF003673FA3DEA5A6B5AEA9D165D6B76
          A6E7CC8E282411FEED06C2652083DF038E69CD5E10F7B97DDDB6B930769CEEAF
          EF337BE213C167ADF87D9EC9AEA245BCFF00438A1491F0605C2AC47E43B3A903
          818E29525784F5B79FC8AABEECA3A5D76F9991E0DD0ADBC632EBD3DB4234ED33
          508C597D8480AE9731A9DD3CB0AF10B6482ABD48C9A751FB3F669BBB5ADFCBFE
          188A6B9F9DA565B25E7FF0E4B178AEE7FE10336B93FDAAB27F626DFE2FB417F2
          01C7AF95F3FE19A5C8BDAA6F66B9BEFBBFCCA52FDDDBAAD0D9BBD1A2F0FEB9E1
          7D3200025A417918C772B02066FF00813649FAD0A5CD0A927D7FE00DC792505D
          ADF7EA6178FAD20F1CEB379A7BCD1C29A1581683748A99D466C4B1919233E5A2
          2838E3E722AE95E9C62ED7E77F86CC8AB69B7676E55F8EE5DF15EBEBE29F8727
          523F7E58EDD651D712A4F1A480FBEF527F1ACE31E4AAE2FB37F7A6D1A4A5CD49
          35DD2FB99EED69C411FF00B89FFA08AE17B9D6B65F22CD0505001401C6FC43FF
          00916F52FF00AF39BFF4035B52FE247FC4BF331ABF04BD0FCE35E02FFC07F98A
          FAB97C2CF975F17CCFD3FD308369011C8F263FFD016BE3DDD36BCCFAB8FC29AE
          C87DED85BEA50B5B5DC693C2E30D1C8A1948F70411FE1426E3AA761B4A5A3479
          55EFC0BF0ADDC8645825B7CFF0C333A27E0A4903F0AEB589A91EBF81CAF0D4DF
          42A7FC281F0B7F76EBFF00021BFC2ABEB753BAFB89FAAD30FF008501E16FEEDD
          7FE0437F851F5BA9DD7DC1F5581E3FF15BE105A783AC46AFA54B21816458E586
          621986FE15A37C024678656C9EE0F515E861B112A92E49F6382BE1D525CF13CC
          7E1EEAF3685E21B1B980905A7485C7668E621194FB739FA806BB311053A6D3DD
          1C9424E9CD5B6B9B9F187579B55F14DE2CA4ECB3616D12E78554009C7A6E6258
          D6585828D34D6EF5FBCD7132729B5D13B1D0FC26F8576FE398A5D435195E3B48
          24F29638480EEE00662CC41DA83200C0CB73C8ACF1388749A8C56AD1A61F0EAA
          2E696C8F71FF008503E16FEEDD7FE0437F85799F5BA9DD7DC7A1F55A7D84FF00
          8503E161FC375FF810DFE147D6EA775F70FEAB4FB1C27C63F0BD8F83FC2D6BA6
          69DBD601A8798048E5DB732316E4F2474FA574E166EA54729765F99CF8982A74
          D463DDFE48F11F86BC78A34CFF00AFA4FE46BD3C4694E56ECCF370EAD517A9F7
          3CBE0AD3E57676F332EC58FCE7A93935E2C719522ACAD64754B2AA126E4EF77E
          BFE6743A7D847A642B6F067626719393C9CF5AE39CDD46E52DCF5A8D18E1E1EC
          E1B23947F0CCEDE294F1007416EBA79B431F3E66F3297DDFDDDB83EB9CD573AF
          67ECFAF35CAE47CFCFD2D632FC37E02FB0E897DA1EAA639E3D42EAF263B33809
          72FB93AE0874E0E470180C554EA7BCA71D2C4AA768CA2FAFF90BF0CBC1979E09
          B4B9B6BF9D2EE4B8BA32AC8BBB2536246BBF77F1E139C647BD556A8AAB4D6965
          FE42A54DD34D3EE69F843C333F87AE7549E77475D4AF9AE63099CAA150007C81
          F36476E2B39CD494576562E11E5BBEECE560F0DF8AB41D4751B9D1DF4D7B7D4E
          ECDD017227DEB95550BFBB2A38C73D6B5E6A738C54AE9A5631E49C65271B59BB
          F43A2BCF0DEA1A96A7A3EAB72F02C9A62DC7DA9630E159E68827EE7764ED0D9F
          BE738F7ACD4D463282EAF4FB8D5C5B7193E9FE64963E16B8D2FC4973ACDABA2D
          96A36E82E61E437DA62384957036F29C312739CD0E7CD0507BC76050E59B92D9
          9CE0F86F32F8A4EB4264FECB32FDB4DAF3BBEDC22F2449FDDC01F3FF00BDDBBD
          69ED57B3E4B7BDB5FC9197B27CFCCB6DEC755ADF872E352D6F4CD5A27458B4D1
          73E62B677319915576E063823E6CF6E959C66A30943B9ACA0DC94974303C39F0
          CAC608669BC4105AEA5A8DDDCCD712CCD10703CC6CA2217190AAB8E3039269CE
          ABBA54F48A564888524AFCFAB7B98B71F0C6F63D2B58D0ECA5823B4BFB94B8B1
          43BF107CCAF2C6E00E14B20D9B738C9CD68AAA728CE4B54ACFEEB11EC9C538AD
          9B4D1DB78753C5114E1359FECEFB22C781F651389778C05FF58C576E339E33E9
          58CB93EC5EE6D0E75A4AD63B91C5626E1400500666B3A6A6B16371612709730C
          9113E9BD4AE7F0CE6AA2F924A5D9A64497345C7BA3F36359D1EE740BC9B4DBD4
          31CF6CE51948EA07DD61EAACB86523820D7D6D39AA8935D8F969C1D3938BD353
          D13C3DF19BC47E1CB54B189E1B98620163FB42166451D143AB292A3A0DD92070
          0E2B9678584DB96CD9D50C54E0B97B1BBFF0D09E24FF009E765FF7EE4FFE2EB2
          FA943BB34FAE4BB07FC3427893FE79D97FDFB7FF00E2E8FA943BBFBC3EB93EC1
          FF000D09E241FF002CECBFEFDBFF00F1747D4A1DD87D725D83FE1A13C49FF3CE
          CBFEFDBFFF001747D4A1DD87D725E5F71C2F8BFE236B3E37091EA5222C111DCB
          042BB23DDD3737259980E9B8E073815D34B0F0A3771DCE6AB5E55747A23A1F83
          9E0D9FC4BADC378508B1D39D6696423E5691798E207BB16C3301D1473D45638A
          AAA11E55F1335C352729293D91B7F1D7C1D3E8FABB6B71296B3D44A9670388E7
          036B2B9EDBC00CA4F04E475159E12B2E5F66F75F97435C5526A5CEB6679CF84B
          C75AB782657934A902A4B8F3219177C4E47425720861D3729071C1AECAB4615B
          E238E9D6951F84F44FF8684F127FCF3B2FFBF727FF00175C9F52877675FD727D
          83FE1A13C49FF3CECBFEFDBFFF001747D4A1DD87D727D8F39F1678DF55F1ACCB
          36AB20658B3E5451AEC8A3CF5217272C7A16624E38E95D74A8C28AF737392AD5
          955D247A0FC0AF0A4DAC6B6BAB329169A6658B9E034ECA42229EE5412EDE8319
          E48AE4C655518722DD9D784A6DCB9DEC8FB74715F3C7BAB41681898A6018A003
          140062800C5200C629806290074A61B0628D803140062801718A003A50014005
          000680383F19FC3AD23C7083EDF194B88C623B888859547A13821D7FD96047A6
          2BA29569D17EE6C73D4A31A9F12D7B9E2737ECD6779F2753C2760F6E0B7E2448
          07E4057A2B1DDE3FD7DE707D4BCFFAFB88BFE19AA5FF00A0A2FF00E037FF006E
          A7F5EFEEFF005F78BEA5E7FD7DC1FF000CD52FFD0517FF00018FFF001EA3EBDF
          DDFEBEF0FA979FF5F707FC3354BFF4145FFC06FF00EDD47D7BFBBFD7DE1F52F3
          FEBEE0FF00866A97FE828BFF0080DFFDBA8FAF7F77FAFBC3EA5E7FD7DC6D68FF
          00B38D85BC81F54BC96E901CF971208437B3365DB1EBB48FAD653C6C9FC0ADFD
          7A9A470718BBC9DCFA074AD22D343B64B2D3E24B7B78861510600F527B927BB1
          C927A9AF325272776F53D18C54172C55912DFE9F6FA9C0F69791A4F04A0ABC6E
          032B03EA0FE9DC1E4524DC5DE3B8DC54959EC7806B9FB3AE997721974ABA96C4
          31CF94EA268C7FBB92AE07A02C6BD38636715692BFF5EA79D2C245FC3A7F5E87
          3DFF000CD52FFD0517FF0001BFFB6D6DF5EFEEFF005F7997D4BCFF00AFB83FE1
          9AA5FF00A0A2FF00E037FF006EA3EBDFDDFEBEF0FA979FF5F71B3A47ECE3636F
          207D4EF65BA453931C48210DEC5B2ED8FA107DEB29E364D5A2ADFD7A9A470718
          BBC9FF005F71F40E93A3D9E856C963A7C496F6F10C2A20C0F727B963D4B1C927
          A9AF3252737CD27A9E8C63182E58AB1A7525EC14005001400500140050014005
          0014005001400500140050014005001400631400500140050014005001400500
          1D2800C62800A002800C62800A003A5001400500140050014005001400500140
          0500140050014005001401FFD9}
        mmHeight = 18521
        mmLeft = 5292
        mmTop = 4233
        mmWidth = 66146
        BandType = 0
      end
      object ppLtitulo: TppLabel
        UserName = 'Ltitulo'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 4233
        mmLeft = 70908
        mmTop = 22754
        mmWidth = 67469
        BandType = 0
      end
      object ppLine1: TppLine
        UserName = 'Line1'
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Weight = 0.750000000000000000
        mmHeight = 1588
        mmLeft = 2381
        mmTop = 42069
        mmWidth = 193146
        BandType = 0
      end
      object ppLine5: TppLine
        UserName = 'Line5'
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Weight = 0.750000000000000000
        mmHeight = 1588
        mmLeft = 2910
        mmTop = 30956
        mmWidth = 191559
        BandType = 0
      end
      object Ltusuario: TppLabel
        UserName = 'Ltusuario'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'Usuario:'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4233
        mmLeft = 8731
        mmTop = 34396
        mmWidth = 14288
        BandType = 0
      end
      object LtFechaHora: TppLabel
        UserName = 'LtFechaHora'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'Fecha / Hora: '
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4233
        mmLeft = 129911
        mmTop = 34131
        mmWidth = 23283
        BandType = 0
      end
      object ppusuario: TppLabel
        UserName = 'usuario'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'XXXXXXXXXXXXXXXXXXX'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 3969
        mmLeft = 25400
        mmTop = 34660
        mmWidth = 44979
        BandType = 0
      end
      object ppfechahora: TppLabel
        UserName = 'fechahora'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'XXXXXXXXXXXXX'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 3969
        mmLeft = 162190
        mmTop = 34396
        mmWidth = 30692
        BandType = 0
      end
      object ppLabel2: TppLabel
        UserName = 'Ltusuario1'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'Datos Generales:'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold, fsUnderline]
        Transparent = True
        mmHeight = 4233
        mmLeft = 5292
        mmTop = 43656
        mmWidth = 28840
        BandType = 0
      end
      object ppLabel3: TppLabel
        UserName = 'Label3'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'Compromiso con el cliente:'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold, fsUnderline]
        Transparent = True
        mmHeight = 4233
        mmLeft = 107421
        mmTop = 43656
        mmWidth = 46038
        BandType = 0
      end
      object ppLabel4: TppLabel
        UserName = 'Label4'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'Identificaci'#243'n del Usuario:'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold, fsUnderline]
        Transparent = True
        mmHeight = 4233
        mmLeft = 5821
        mmTop = 72231
        mmWidth = 43921
        BandType = 0
      end
      object ppLabel5: TppLabel
        UserName = 'Label5'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'Informaci'#243'n de Contacto:'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold, fsUnderline]
        Transparent = True
        mmHeight = 4233
        mmLeft = 107156
        mmTop = 59531
        mmWidth = 42598
        BandType = 0
      end
      object ppLabel7: TppLabel
        UserName = 'Ltusuario2'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'Fecha Creaci'#243'n:'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4233
        mmLeft = 11642
        mmTop = 48419
        mmWidth = 27252
        BandType = 0
      end
      object ppLabel8: TppLabel
        UserName = 'Label8'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'Numero Orden:'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4498
        mmLeft = 11377
        mmTop = 53975
        mmWidth = 28310
        BandType = 0
      end
      object ppLabel9: TppLabel
        UserName = 'Label9'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'Prioridad:'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4233
        mmLeft = 114565
        mmTop = 48154
        mmWidth = 16669
        BandType = 0
      end
      object ppLabel10: TppLabel
        UserName = 'Label10'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'Fecha:'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4233
        mmLeft = 115094
        mmTop = 53975
        mmWidth = 11377
        BandType = 0
      end
      object ppTipoContacto: TppLabel
        UserName = 'TipoContacto'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'XXXXX'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 3969
        mmLeft = 116152
        mmTop = 66146
        mmWidth = 11906
        BandType = 0
      end
      object ppContacto: TppLabel
        UserName = 'Label101'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'XXXXX'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 3969
        mmLeft = 116152
        mmTop = 71967
        mmWidth = 11906
        BandType = 0
      end
      object ppLabel13: TppLabel
        UserName = 'Label13'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'RUT:'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4233
        mmLeft = 11377
        mmTop = 76729
        mmWidth = 8467
        BandType = 0
      end
      object ppLabel14: TppLabel
        UserName = 'Label14'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'Nombre y Apellido:'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4233
        mmLeft = 11113
        mmTop = 82550
        mmWidth = 32015
        BandType = 0
      end
      object ppLabel15: TppLabel
        UserName = 'Label15'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'Numero Convenio:'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4233
        mmLeft = 11113
        mmTop = 88900
        mmWidth = 31485
        BandType = 0
      end
      object ppFechaCreacion: TppLabel
        UserName = 'Label1'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'XXXXX'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 3969
        mmLeft = 46302
        mmTop = 48683
        mmWidth = 11906
        BandType = 0
      end
      object ppNumeroOrden: TppLabel
        UserName = 'NumeroOrden'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'XXXXX'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 3969
        mmLeft = 46302
        mmTop = 53711
        mmWidth = 11906
        BandType = 0
      end
      object ppRut: TppLabel
        UserName = 'Rut'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'XXXXX'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 3969
        mmLeft = 46038
        mmTop = 77258
        mmWidth = 11906
        BandType = 0
      end
      object ppNombreyApellido: TppLabel
        UserName = 'NombreyApellido'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'XXXXX'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 3969
        mmLeft = 45773
        mmTop = 83079
        mmWidth = 11906
        BandType = 0
      end
      object ppNumeroConvenio: TppLabel
        UserName = 'NumeroConvenio'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'XXXXX'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 3969
        mmLeft = 45773
        mmTop = 89165
        mmWidth = 11906
        BandType = 0
      end
      object ppPrioridad: TppLabel
        UserName = 'Prioridad'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'XXXXX'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 3969
        mmLeft = 180182
        mmTop = 48419
        mmWidth = 11906
        BandType = 0
      end
      object ppFecha: TppLabel
        UserName = 'Fecha'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'XXXXX'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        TextAlignment = taRightJustified
        Transparent = True
        mmHeight = 3969
        mmLeft = 180182
        mmTop = 53975
        mmWidth = 11906
        BandType = 0
      end
      object ppLine3: TppLine
        UserName = 'Line3'
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Weight = 0.750000000000000000
        mmHeight = 1588
        mmLeft = 5027
        mmTop = 93663
        mmWidth = 190236
        BandType = 0
      end
      object ppLabel23: TppLabel
        UserName = 'Ltitulo1'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'Reclamo'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        TextAlignment = taCentered
        Transparent = True
        mmHeight = 4233
        mmLeft = 96309
        mmTop = 16933
        mmWidth = 14552
        BandType = 0
      end
      object ppLabel1: TppLabel
        UserName = 'Label2'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'Concesionaria:'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4191
        mmLeft = 11377
        mmTop = 59002
        mmWidth = 25400
        BandType = 0
      end
      object ppConcesionaria: TppLabel
        UserName = 'ppConcesionaria'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'XXXXX'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 3969
        mmLeft = 46302
        mmTop = 59002
        mmWidth = 11906
        BandType = 0
      end
      object ppLabel12: TppLabel
        UserName = 'Label12'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'Sub Tipo Orden:'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4191
        mmLeft = 10848
        mmTop = 64029
        mmWidth = 27644
        BandType = 0
      end
      object ppSubTipoOrdenServicio: TppLabel
        UserName = 'ppSubTipoOrdenServicio'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'XXXXX'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 3969
        mmLeft = 45773
        mmTop = 64029
        mmWidth = 11906
        BandType = 0
      end
    end
    object ppDetailBand1: TppDetailBand
      Background1.Brush.Style = bsClear
      Background1.Gradient.EndColor = clWhite
      Background1.Gradient.StartColor = clWhite
      Background1.Gradient.Style = gsNone
      Background2.Brush.Style = bsClear
      Background2.Gradient.EndColor = clWhite
      Background2.Gradient.StartColor = clWhite
      Background2.Gradient.Style = gsNone
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 135202
      mmPrintPosition = 0
      object ppLabel6: TppLabel
        UserName = 'Label6'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'Detalle del Reclamo:'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold, fsUnderline]
        Transparent = True
        mmHeight = 4233
        mmLeft = 7938
        mmTop = 0
        mmWidth = 34396
        BandType = 4
      end
      object ppMemo: TppMemo
        UserName = 'Memo'
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = True
        Border.Weight = 1.000000000000000000
        CharWrap = True
        Color = clBlack
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = []
        Transparent = True
        mmHeight = 128059
        mmLeft = 7673
        mmTop = 4233
        mmWidth = 171980
        BandType = 4
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        mmLeading = 0
      end
    end
    object ppSummaryBand1: TppSummaryBand
      AlignToBottom = False
      mmBottomOffset = 0
      mmHeight = 23813
      mmPrintPosition = 0
      object pptImporteTotal: TppLabel
        UserName = 'tImporteTotal'
        HyperlinkColor = clBlue
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Caption = 'Firma Cliente'
        Ellipsis = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Name = 'Arial'
        Font.Size = 10
        Font.Style = [fsBold]
        Transparent = True
        mmHeight = 4233
        mmLeft = 155575
        mmTop = 19050
        mmWidth = 22225
        BandType = 7
      end
      object ppLine4: TppLine
        UserName = 'Line4'
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Weight = 0.750000000000000000
        mmHeight = 3969
        mmLeft = 142611
        mmTop = 16404
        mmWidth = 50271
        BandType = 7
      end
      object ppLine2: TppLine
        UserName = 'Line2'
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        Weight = 0.750000000000000000
        mmHeight = 1588
        mmLeft = 3704
        mmTop = 1058
        mmWidth = 187325
        BandType = 7
      end
    end
    object ppParameterList1: TppParameterList
    end
  end
  object ppDBPipeline: TppDBPipeline
    DataSource = DataSource
    UserName = 'DBPipeline'
    Left = 72
    Top = 8
  end
  object DataSource: TDataSource
    DataSet = SpObtenerReporteReclamo
    Left = 40
    Top = 8
  end
  object SpObtenerReporteReclamo: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerReporteReclamo;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoOrdenServicio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end>
    Left = 8
    Top = 8
  end
  object RBI_Listado: TRBInterface
    Report = rptReclamo
    OrderIndex = 0
    OnExecute = RBI_ListadoExecute
    Left = 137
    Top = 8
  end
end
