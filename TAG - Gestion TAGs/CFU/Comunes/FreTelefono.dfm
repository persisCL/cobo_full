object FrameTelefono: TFrameTelefono
  Left = 0
  Top = 0
  Width = 612
  Height = 25
  TabOrder = 0
  TabStop = True
  object lblTelefono: TLabel
    Left = 7
    Top = 6
    Width = 103
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tel'#233'fono principal:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblDesde: TLabel
    Left = 413
    Top = 6
    Width = 34
    Height = 13
    Caption = 'Desde:'
    FocusControl = cbDesde
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lblHasta: TLabel
    Left = 515
    Top = 6
    Width = 32
    Height = 13
    Caption = 'Hasta:'
    FocusControl = cbHasta
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object txtTelefono: TEdit
    Left = 178
    Top = 3
    Width = 93
    Height = 21
    Hint = 'N'#250'mero de tel'#233'fono'
    Color = 16444382
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    MaxLength = 9
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    OnExit = txtTelefonoExit
    OnKeyPress = txtTelefonoKeyPress
  end
  object cbTipoTelefono: TVariantComboBox
    Left = 273
    Top = 3
    Width = 133
    Height = 21
    Hint = 'Tipo de tel'#233'fono'
    Style = vcsDropDownList
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ItemHeight = 13
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
    OnExit = cbTipoTelefonoExit
    Items = <>
  end
  object cbDesde: TComboBox
    Left = 450
    Top = 3
    Width = 55
    Height = 22
    Hint = 'Horario s'#243'lo para d'#237'as h'#225'biles'
    Style = csOwnerDrawFixed
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ItemHeight = 16
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 3
    OnChange = cbDesdeChange
  end
  object cbHasta: TComboBox
    Left = 551
    Top = 3
    Width = 55
    Height = 22
    Hint = 'Horario s'#243'lo para d'#237'as h'#225'biles'
    Style = csOwnerDrawFixed
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ItemHeight = 16
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 4
    OnChange = cbHastaChange
  end
  object cbCodigosArea: TComboBox
    Left = 117
    Top = 3
    Width = 55
    Height = 21
    Style = csDropDownList
    Color = 16444382
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ItemHeight = 13
    ParentFont = False
    TabOrder = 0
  end
end
