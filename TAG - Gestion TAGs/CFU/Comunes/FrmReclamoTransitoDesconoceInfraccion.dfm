object FormReclamoTransitoDesconoceInfraccion: TFormReclamoTransitoDesconoceInfraccion
  Left = 98
  Top = 163
  BorderStyle = bsDialog
  Caption = 'Reclamo de Transito Desconoce Infracci'#243'n'
  ClientHeight = 523
  ClientWidth = 862
  Color = clBtnFace
  Constraints.MinHeight = 451
  Constraints.MinWidth = 870
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  Visible = True
  OnClose = FormClose
  OnDestroy = FormDestroy
  OnKeyPress = FormKeyPress
  DesignSize = (
    862
    523)
  PixelsPerInch = 96
  TextHeight = 13
  inline FrameContactoReclamo1: TFrameContactoReclamo
    Left = 1
    Top = 0
    Width = 627
    Height = 172
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 2
    TabStop = True
    ExplicitLeft = 1
    inherited gbUsuario: TGroupBox
      inherited LNumeroConvenio: TLabel
        Width = 103
        ExplicitWidth = 103
      end
    end
  end
  object PAbajo: TPanel
    Left = 0
    Top = 484
    Width = 862
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object PDerecha: TPanel
      Left = 616
      Top = 0
      Width = 246
      Height = 39
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object CKRetenerOrden: TCheckBox
        Left = 3
        Top = 11
        Width = 89
        Height = 17
        Caption = 'Retener Orden'
        TabOrder = 0
      end
      object AceptarBTN: TButton
        Left = 96
        Top = 6
        Width = 70
        Height = 25
        Caption = '&Aceptar'
        Default = True
        TabOrder = 1
        OnClick = AceptarBTNClick
      end
      object btnCancelar: TButton
        Left = 171
        Top = 6
        Width = 70
        Height = 25
        Cancel = True
        Caption = '&Cancelar'
        ModalResult = 2
        TabOrder = 2
        OnClick = btnCancelarClick
      end
    end
  end
  object PageControl: TPageControl
    Left = 5
    Top = 173
    Width = 610
    Height = 344
    ActivePage = TabSheet1
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Datos del Reclamo'
      DesignSize = (
        602
        316)
      object Label3: TLabel
        Left = 2
        Top = 190
        Width = 97
        Height = 13
        Caption = 'Otras observaciones'
      end
      object LJustificacion: TLabel
        Left = 6
        Top = 99
        Width = 61
        Height = 13
        Caption = 'Justificaci'#243'n:'
      end
      object txtDetalle: TMemo
        Left = 1
        Top = 203
        Width = 592
        Height = 114
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 0
      end
      object DBLTransitos: TDBListEx
        Left = 1
        Top = 1
        Width = 592
        Height = 90
        Anchors = [akLeft, akTop, akRight]
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 56
            Header.Caption = 'Tr'#225'nsito'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'numcorrca'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 60
            Header.Caption = 'P.Cobro'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'PuntoCobro'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 100
            Header.Caption = 'Patente'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Patente'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 100
            Header.Caption = 'Fecha / Hora'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'FechaHora'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 100
            Header.Caption = 'Usuario Rechaza'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = True
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 70
            Header.Caption = 'Resoluci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsUnderline]
            Width = 60
            Header.Caption = 'Im'#225'gen'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taCenter
            IsLink = True
          end>
        DataSource = DataSource
        DragReorder = True
        ParentColor = False
        PopupMenu = PopupResolver
        TabOrder = 1
        TabStop = True
        OnContextPopup = DBLTransitosContextPopup
        OnCheckLink = DBLTransitosCheckLink
        OnDrawText = DBLTransitosDrawText
        OnLinkClick = DBLTransitosLinkClick
      end
      object CBMotivosDesconoceInfraccion: TVariantComboBox
        Left = 101
        Top = 97
        Width = 282
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 0
        TabOrder = 2
        OnSelect = CBMotivosDesconoceInfraccionSelect
        Items = <>
      end
      object PDatosJustificacion: TPanel
        Left = 2
        Top = 119
        Width = 591
        Height = 73
        Anchors = [akLeft, akTop, akRight]
        BevelOuter = bvNone
        Caption = 'PDatosJustificacion'
        TabOrder = 3
        object PPaseDiario: TPanel
          Left = 0
          Top = 0
          Width = 591
          Height = 24
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object LNumeroSerie: TLabel
            Left = 5
            Top = 5
            Width = 67
            Height = 13
            Caption = 'N'#250'mero Serie:'
          end
          object ENumeroSerie: TEdit
            Left = 99
            Top = 1
            Width = 171
            Height = 21
            MaxLength = 22
            TabOrder = 0
          end
        end
        object nbDatosJustificacion: TNotebook
          Left = 0
          Top = 24
          Width = 591
          Height = 49
          Align = alClient
          TabOrder = 1
          object TPage
            Left = 0
            Top = 0
            Caption = 'Comun'
            object Label1: TLabel
              Left = 7
              Top = 7
              Width = 72
              Height = 13
              Caption = 'Fecha Compra:'
            end
            object Label4: TLabel
              Left = 6
              Top = 29
              Width = 48
              Height = 13
              Caption = 'Categoria:'
            end
            object Label5: TLabel
              Left = 303
              Top = 10
              Width = 40
              Height = 13
              Caption = 'Patente:'
            end
            object EFechaCompra: TDateEdit
              Left = 98
              Top = 4
              Width = 170
              Height = 21
              AutoSelect = False
              TabOrder = 0
              Date = -693594.000000000000000000
            end
            object EPatente: TEdit
              Left = 393
              Top = 9
              Width = 170
              Height = 21
              MaxLength = 10
              TabOrder = 1
            end
            object cbCategoria: TVariantComboBox
              Left = 98
              Top = 27
              Width = 171
              Height = 21
              Style = vcsDropDownList
              ItemHeight = 0
              TabOrder = 2
              Items = <>
            end
          end
          object TPage
            Left = 0
            Top = 0
            Caption = 'Televia'
            object LEtiqueta: TLabel
              Left = 7
              Top = 11
              Width = 42
              Height = 13
              Caption = 'Etiqueta:'
            end
            object LConcesionaria: TLabel
              Left = 7
              Top = 32
              Width = 67
              Height = 13
              Caption = 'Concesionaria'
            end
            object LNumeroConvenio: TLabel
              Left = 288
              Top = 7
              Width = 88
              Height = 13
              Caption = 'Numero Convenio:'
            end
            object EEtiqueta: TEdit
              Left = 99
              Top = 3
              Width = 171
              Height = 21
              MaxLength = 11
              TabOrder = 0
            end
            object Enumeroconvenio: TEdit
              Left = 380
              Top = 6
              Width = 168
              Height = 21
              MaxLength = 30
              TabOrder = 1
            end
            object CBConcesionaria: TVariantComboBox
              Left = 99
              Top = 26
              Width = 171
              Height = 21
              Style = vcsDropDownList
              ItemHeight = 0
              TabOrder = 2
              Items = <>
            end
          end
        end
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'Progreso / soluci'#243'n'
      ImageIndex = 3
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      DesignSize = (
        602
        316)
      object Ldetalledelprogreso: TLabel
        Left = 3
        Top = 7
        Width = 149
        Height = 13
        Caption = 'Detalles del progreso / soluci'#243'n'
      end
      object txtDetalleSolucion: TMemo
        Left = 1
        Top = 24
        Width = 592
        Height = 287
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 0
      end
    end
    object TabSheetRespuesta: TTabSheet
      Caption = 'Respuesta'
      ImageIndex = 3
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      inline FrameRespuestaOrdenServicio1: TFrameRespuestaOrdenServicio
        Left = 0
        Top = 0
        Width = 602
        Height = 316
        Align = alClient
        TabOrder = 0
        TabStop = True
        ExplicitWidth = 602
        ExplicitHeight = 316
        inherited LComentariosDelCliente: TLabel
          Width = 116
          ExplicitWidth = 116
        end
        inherited PContactarCliente: TPanel
          Width = 602
          ExplicitWidth = 602
        end
        inherited txtDetalleRespuesta: TMemo
          Width = 602
          ExplicitWidth = 602
        end
        inherited TxtComentariosdelCliente: TMemo
          Width = 602
          Height = 142
          ExplicitWidth = 602
          ExplicitHeight = 142
        end
      end
    end
    object TabSheet5: TTabSheet
      Caption = 'Historia'
      ImageIndex = 4
      inline FrameHistoriaOrdenServicio: TFrameHistoriaOrdenServicio
        Left = 0
        Top = 0
        Width = 602
        Height = 316
        Align = alClient
        TabOrder = 0
        TabStop = True
        ExplicitWidth = 602
        ExplicitHeight = 316
        inherited DBLHistoria: TDBListEx
          Width = 602
          Height = 240
          Columns = <
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 120
              Header.Caption = 'F. Modificaci'#243'n'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'FechaHoraModificacion'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 100
              Header.Caption = 'F. Compromiso'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'FechaCompromiso'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 60
              Header.Caption = 'Usuario'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'Usuario'
            end
            item
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 60
              Header.Caption = 'Prioridad'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              Header.Alignment = taCenter
              IsLink = False
              FieldName = 'Prioridad'
            end
            item
              Alignment = taLeftJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Width = 60
              Header.Caption = 'Estado'
              Header.Font.Charset = DEFAULT_CHARSET
              Header.Font.Color = clWindowText
              Header.Font.Height = -12
              Header.Font.Name = 'Tahoma'
              Header.Font.Style = []
              IsLink = False
              FieldName = 'DescEstado'
            end>
          ExplicitWidth = 602
          ExplicitHeight = 240
        end
        inherited Pencabezado: TPanel
          Width = 602
          ExplicitWidth = 602
          inherited Pder: TPanel
            Left = 594
            ExplicitLeft = 594
          end
          inherited Parriva: TPanel
            Width = 602
            ExplicitWidth = 602
          end
        end
        inherited Pabajo: TPanel
          Top = 275
          Width = 602
          ExplicitTop = 275
          ExplicitWidth = 602
        end
      end
    end
  end
  inline FrameCompromisoOrdenServicio1: TFrameCompromisoOrdenServicio
    Left = 623
    Top = 69
    Width = 239
    Height = 102
    Anchors = [akTop, akRight]
    TabOrder = 3
    TabStop = True
    ExplicitLeft = 623
    ExplicitTop = 69
    inherited GBCompromisoCliente: TGroupBox
      Anchors = [akTop, akRight]
      inherited Label1: TLabel
        Width = 44
        ExplicitWidth = 44
      end
      inherited Label2: TLabel
        Width = 103
        ExplicitWidth = 103
      end
    end
  end
  inline FrameSolucionOrdenServicio1: TFrameSolucionOrdenServicio
    Left = 622
    Top = 171
    Width = 240
    Height = 116
    Anchors = [akTop, akRight]
    TabOrder = 4
    TabStop = True
    ExplicitLeft = 622
    ExplicitTop = 171
    ExplicitHeight = 116
    inherited GBEstadoDelReclamo: TGroupBox
      inherited Label1: TLabel
        Width = 36
        ExplicitWidth = 36
      end
      inherited PAdicional: TPanel
        inherited Label2: TLabel
          Width = 72
          ExplicitWidth = 72
        end
        inherited Label3: TLabel
          Width = 39
          ExplicitWidth = 39
        end
      end
    end
  end
  inline FrameFuenteReclamoOrdenServicio1: TFrameFuenteReclamoOrdenServicio
    Left = 622
    Top = 6
    Width = 239
    Height = 65
    Anchors = [akTop, akRight]
    TabOrder = 5
    TabStop = True
    ExplicitLeft = 622
    ExplicitTop = 6
  end
  inline FrameNumeroOrdenServicio1: TFrameNumeroOrdenServicio
    Left = 622
    Top = 285
    Width = 239
    Height = 68
    Anchors = [akTop, akRight]
    TabOrder = 6
    TabStop = True
    ExplicitLeft = 622
    ExplicitTop = 285
    inherited PReclamo: TGroupBox
      inherited LOrden: TLabel
        Width = 72
        ExplicitWidth = 72
      end
    end
  end
  inline FrameConcesionariaReclamoOrdenServicio1: TFrameConcesionariaReclamoOrdenServicio
    Left = 623
    Top = 351
    Width = 235
    Height = 56
    Anchors = [akTop, akRight]
    TabOrder = 7
    ExplicitLeft = 623
    ExplicitTop = 351
    inherited GBConcesionaria: TGroupBox
      inherited lblConcesionaria: TLabel
        Width = 70
        ExplicitWidth = 70
      end
    end
  end
  inline FrameSubTipoReclamoOrdenServicio1: TFrameSubTipoReclamoOrdenServicio
    Left = 623
    Top = 401
    Width = 240
    Height = 55
    Anchors = [akTop, akRight]
    TabOrder = 8
    ExplicitLeft = 623
    ExplicitTop = 401
    inherited GBSubTipo: TGroupBox
      inherited lblSubTipo: TLabel
        Width = 88
        ExplicitWidth = 88
      end
    end
  end
  object DataSource: TDataSource
    DataSet = spObtenerTransitosReclamo
    Left = 455
    Top = 167
  end
  object lnCheck: TImageList
    BkColor = clWhite
    Height = 15
    Width = 15
    Left = 560
    Top = 167
    Bitmap = {
      494C01010200080010000F000F00FFFFFF00FF10FFFFFFFFFFFFFFFF424D3600
      00000000000036000000280000003C0000000F0000000100200000000000100E
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00000000000000
      0000FFFFFF00000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000424D3E000000000000003E000000280000003C0000000F00000001000100
      00000000780000000000000000000000000000000000000000000000FFFFFF00
      FFFFFFFC000000008003000400000000BFFB7FF400000000BFFB77F400000000
      BFFB63F400000000BFFB41F400000000BFFB48F400000000BFFB5C7400000000
      BFFB7E3400000000BFFB7F1400000000BFFB7F9400000000BFFB7FD400000000
      BFFB7FF4000000008003000400000000FFFFFFFC000000000000000000000000
      0000000000000000000000000000}
  end
  object spObtenerOrdenServicio: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerOrdenServicio;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoOrdenServicio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 489
    Top = 167
  end
  object spObtenerTransitosReclamo: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerTransitosReclamo;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoOrdenServicio'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@Enumeracion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 500
        Value = Null
      end>
    Left = 424
    Top = 167
  end
  object spObtenerOrdenServicioDesconoceInfraccion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerOrdenServicioDesconoceInfraccion'
    Parameters = <>
    Left = 392
    Top = 167
  end
  object PopupResolver: TPopupMenu
    Left = 360
    Top = 167
    object mnu_Resolver: TMenuItem
      Caption = 'Resolver'
      object Mnu_Pendiente: TMenuItem
        Tag = 1
        Caption = 'Pendiente'
        OnClick = Mnu_ComunClick
      end
      object Mnu_Aceptado: TMenuItem
        Tag = 2
        Caption = 'Aceptado'
        OnClick = Mnu_ComunClick
      end
      object Mnu_Denegado: TMenuItem
        Tag = 3
        Caption = 'Denegado'
        OnClick = Mnu_ComunClick
      end
      object Mnu_Comun: TMenuItem
        Visible = False
        OnClick = Mnu_ComunClick
      end
    end
  end
  object SPAnularTransitoInfraccion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'AnularTransitoInfraccion;1'
    Parameters = <>
    Left = 525
    Top = 167
  end
  object ilCamara: TImageList
    Height = 15
    Width = 15
    Left = 558
    Top = 168
    Bitmap = {
      494C01010200080010000F000F00FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      00000000000036000000280000003C0000000F0000000100200000000000100E
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000000000004A4D4A004A4D4A004A4D
      4A004A4D4A004A4D4A004A4D4A004A4D4A004A4D4A004A4D4A004A4D4A004A4D
      4A004A4D4A000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      00004A4D4A0000EBFF0000EBFF0000EBFF0000EBFF0000EBFF0000EBFF0000EB
      FF0000EBFF0000EBFF0000EBFF0000EBFF0000EBFF004A4D4A00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000004A4D4A0000EBFF00B5FBFF00B5FB
      FF00B5FBFF00848284004A4D4A004A4D4A004A4D4A00B5FBFF00B5FBFF00B5FB
      FF0000EBFF004A4D4A0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      00004A4D4A0000EBFF00B5FBFF00FFFFFF004A4D4A0084828400B5B2B500B5B2
      B500848284004A4D4A00FFFFFF00B5FBFF0000EBFF004A4D4A00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000004A4D4A0000EBFF00B5FBFF00FFFF
      FF004A4D4A008482840084828400848284004A4D4A004A4D4A00FFFFFF00B5FB
      FF0000EBFF004A4D4A0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      00004A4D4A0000EBFF00B5FBFF00FFFFFF004A4D4A0084828400B5B2B500B5B2
      B500848284004A4D4A00FFFFFF00B5FBFF0000EBFF004A4D4A00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000004A4D4A0000EBFF00B5FBFF00B5FB
      FF00B5FBFF004A4D4A004A4D4A004A4D4A004A4D4A00B5FBFF00B5FBFF00B5FB
      FF0000EBFF004A4D4A0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      00004A4D4A0000EBFF0000EBFF0000EBFF0000EBFF0000EBFF0000EBFF0000EB
      FF0000EBFF0000EBFF0000EBFF0000EBFF0000EBFF004A4D4A00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000000000004A4D4A004A4D4A004A4D
      4A004A4D4A004A4D4A004A4D4A004A4D4A004A4D4A004A4D4A004A4D4A004A4D
      4A004A4D4A000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      000000000000000000004A4D4A0000EBFF0000EBFF004A4D4A00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000004A4D4A004A4D
      4A004A4D4A004A4D4A0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000424D3E000000000000003E000000280000003C0000000F00000001000100
      00000000780000000000000000000000000000000000000000000000FFFFFF00
      8001FFFC000000008001FFFC0000000080018004000000008001000000000000
      8001000000000000800100000000000080010000000000008001000000000000
      8001000000000000800100000000000080018004000000008001C3FC00000000
      8001C3FC000000008001FFFC00000000FFFFFFFC000000000000000000000000
      0000000000000000000000000000}
  end
end
