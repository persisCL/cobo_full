{-----------------------------------------------------------------------------
 File Name: FrmAnulacionRecibos.pas
 Author:    jconcheyro
 Date Created: 18/10/2005
 Language: ES-AR
 Description: Permite generar una nota de Debito DK para un convenio, o anular un
 recibo y generarle la nota de debito DK.
 Se arm� en base al form anterior FrmAnulacionRecibos.pas

 Revisiones anteriores hechas en el FrmAnulacionRecibos.pas:

 Author:    ndonadio
 Date Created: 22/06/2005
 Language: ES-AR
 Description: Se elimino el control de turno abierto.

 Revision       : 2
 Author         : ggomez
 Date           : 14/07/2005
 Description    : Agregu� el m�todo Inicializar con el par�metro NumeroRecibo.

 Revisiones nuevas:
 Revision 		:	3
 Author         : jconcheyro
 Date           : 30/11/2005
 Description    : se toma la hora de la base de datos para agregarla a la fecha de anulacion para que quede completa.

 Revisiones nuevas:
 Revision 		:	4
 Author         : jconcheyro
 Date           : 14/03/2006
 Description    : modificaciones para anular recibos de pago de NI no de Convenio

 Revision 		:	5
 Author         : nefernandez
 Date           : 09/03/2007
 Description    : Se pasa el parametro CodigoUsuario al SP CrearNotaDebitoDK
                  (para Auditoria)

 Revision 		:	7
 Author         : nefernandez
 Date           : 22/08/2007
 Description    : SS 548: Cambio del atributo FNumeroDebito de Integer a Int64.

 Revision 		:	8
 Author         : FSandi
 Date           : 03-10-2007
 Description    : SS 316 - Ahora listan solamente los motivos de anulaci�n correspondientes a esta pantalla

 Revision 		:	9
 Author         : Rharris
 Date           : 12-06-2009
 Description    : (Ref.: Facturaci�n Electr�nica)
                  Se agrega parametro @FechaAnulacion al spCrearNotaDebitoDK

 Revision 		:	10
 Author         : Rharris
 Date           : 29-06-2009
 Description    : (Ref.: Facturaci�n Electr�nica)
                  Se valida que el Recibo a anular no tenga asociado un Comprobante debito
                  anulado previamente por una Nota de Cr�dito

 Revision       :   11
 Author         : pdominguez
 Date           : 04/06/2010
 Description    : Infractores Fase 2
    - Se a�aden los siguientes objetos:
        spActualizarInfraccionesPagoAnulado: TADOStoredProc.

    - Se modificaron los siguientes procedimientos / funciones:
        btnAnularClick

 Firma        : SS-1014-NDR-20120123
 Description  : Quitar la b�squeda incremental.

Author		: mvillarroel
Date		: 07-Julio-2013
Firma   : SS_660_MVI_20130711
Description	:		(Ref: SS 660)
                Se agrega validaci�n para indicar si un Convenio est� en Lista Amarilla

 Autor          :   CQuezadaI
 Fecha          :   23-07-2013
 Firma          :   SS_660_CQU_20130711
 Descripcion    :   Se alinea el Label de Lista Amarilla
                    Se agrega el par�metro CodigoConcesionaria en la funci�n EstaConvenioEnListaAmarilla

Firma       :   SS_1120_MVI_20130820
Descripcion :   Se cambia estilo del comboBox a: vcsOwnerDrawFixed,
                se crear el evento DrawItem y se llama al procedimeinto ItemComboBoxColor.

Firma       :   SS_660_MVI_20130909
Descripcion :   Se modifica validaci�n para saber si se obtiene resultado del SP ObtenerEstadoConvenioEnListaAmarilla.
                Se acorta tama�o del lblEnListaAmarilla y se elimina texto inicial, ya que siempre es reemplazado.

Firma       : SS_1408_MCA_20151027
Descripcion : muestra un mensaje si el cliente se encuentre en registrado en Tabla ClientesAContactar

 -----------------------------------------------------------------------------}

unit FrmAnulacionRecibos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ListBoxEx, DBListEx, StdCtrls, DPSControls, VariantComboBox,
  DmiCtrls, DB, ADODB, DMCOnnection, utilDB, UtilFacturacion, utilProc, buscaClientes,
  PeaTypes, RStrings, DBCtrls, peaProcs, Util, PagoVentanilla, Validate,
  DateEdit, ExtCtrls, CobranzasRoutines, PeaProcsCN;

const
	KEY_F9 	= VK_F9;
type
  Tfrm_AnulacionRecibos = class(TForm)
    gbPorCliente: TGroupBox;
    lb_CodigoCliente: TLabel;
    lblNumeroConvenio: TLabel;
    peNumeroDocumento: TPickEdit;
    cbConvenios: TVariantComboBox;
    gbDatosRecibo: TGroupBox;
    gbDatosDetalleRecibo: TGroupBox;
    spObtenerRecibosParaAnulacion: TADOStoredProc;
    lblNumeroRecibo: TLabel;
    edNumeroRecibo: TNumericEdit;
    DBListEx1: TDBListEx;
    dsComprobantesRecibo: TDataSource;
    gbAnulacion: TGroupBox;
    lblFechaAnulacion: TLabel;
    lblMotivoAnulacion: TLabel;
    deFechaAnulacion: TDateEdit;
    cbMotivosAnulacion: TVariantComboBox;
    spObtenerComprobantesRecibo: TADOStoredProc;
    spAnularRecibo: TADOStoredProc;
    btnAnular: TButton;
	btnSalir: TButton;
	DBListEx2: TDBListEx;
	dsRecibos: TDataSource;
	cbCanalesDePago: TVariantComboBox;
	lblCanalPago: TLabel;
	spCrearNotaDebitoDK: TADOStoredProc;
	spObtenerCanalPago: TADOStoredProc;
    SPObtenerMotivosAnulacionPagos: TADOStoredProc;
    spActualizarInfraccionesPagoAnulado: TADOStoredProc;
    btnFiltrar: TButton;																			//SS-1014-NDR-20120123
    lblEnListaAmarilla: TLabel;																			 //SS_660_MVI_20130711
	procedure txtImporteDebitoKeyPress(Sender: TObject; var Key: Char);
	procedure dsRecibosDataChange(Sender: TObject; Field: TField);
	procedure peNumeroDocumentoChange(Sender: TObject);
	procedure cbConveniosChange(Sender: TObject);
	procedure FormCreate(Sender: TObject);
	procedure peNumeroDocumentoButtonClick(Sender: TObject);
	procedure btnSalirClick(Sender: TObject);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure edNumeroReciboChange(Sender: TObject);
	procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
	procedure btnAnularClick(Sender: TObject);
	procedure HabilitarRadioOpciones;
    procedure btnFiltrarClick(Sender: TObject);                       //SS-1014-NDR-20120123
    procedure cbConveniosDrawItem(Control: TWinControl; Index: Integer;     //SS_1120_MVI_20130820
      Rect: TRect; State: TOwnerDrawState);                                 //SS_1120_MVI_20130820

  private
    { Private declarations }
    FUltimaBusqueda: TBusquedaCliente;
	  FNumeroRecibo: integer;
    FNumeroDebito: int64;
    FImporteRecibo: Double;
    FCodigoConvenio: integer;
    FCodigoCanalPago: Integer;
	FNInodeConvenioPagada: integer; // se alamacena el numero de NI para no Convenio pagada en el recibo a anular
//    FPuntoEntrega, FPuntoVenta: Integer;
    dFechaRecibo: TDateTime;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
	function  CargarRecibos : boolean;
    function  CargarDetalleRecibos(NumeroRecibo: Int64) : boolean;
    function  ObtenerMotivosAnulacion : boolean;
    function  ObtenerCanalesDePago: boolean;
    procedure LimpiarDetalles;
    function  GenerarDebitoDK : boolean;
//    function ValidarDatosTurno: Boolean;
  public
    { Public declarations }
    function Inicializar: Boolean; overload;
    function Inicializar(NumeroRecibo: Int64): Boolean; overload;
  end;

var
  frm_AnulacionRecibos: Tfrm_AnulacionRecibos;

implementation

{$R *.dfm}

resourcestring
	MSG_CAPTION			   = 'Anulaci�n de recibos';
	MSG_ERROR_INICIALIZAR   = 'Error inicializando el formulario.';
    MSG_SIN_PERMISOS = 'Ud. no tiene permisos para trabajar con canales de pago. Consulte al administrador del sistema.';

function Tfrm_AnulacionRecibos.Inicializar: Boolean;
begin
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

    LimpiarDetalles;
	HabilitarRadioOpciones;
    FCodigoCanalPago := 0 ;
	FNInodeConvenioPagada := 0;
    if not ObtenerCanalesDePago then begin
        MsgBox(MSG_SIN_PERMISOS, MSG_CAPTION, MB_ICONINFORMATION + MB_OK);
        Result := False;
        Exit;
    end;
    Result := ObtenerMotivosAnulacion;
    //Result := Result and ObtenerCanalesDePago;
   lblEnListaAmarilla.Visible:=False;                                                    //SS_660_MVI_20130711
end;

procedure Tfrm_AnulacionRecibos.FormCreate(Sender: TObject);
begin
    CenterForm(Self);
end;

{-----------------------------------------------------------------------------
  Function Name: peNumeroDocumentoChange
  Author:    flamas
  Date Created: 20/12/2004
  Description: Actualiza los Convenios y Comprobantes Impagos de un cliente determinado
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure Tfrm_AnulacionRecibos.peNumeroDocumentoChange(Sender: TObject);
//Var                                                                                                       //SS_1408_MCA_20151027
//    CodigoCliente: Integer;                                                                               //SS_1408_MCA_20151027
begin
    if ActiveControl <> Sender then Exit;
    edNumeroRecibo.Clear;
//    CodigoCliente := QueryGetValueInt(DMConnections.BaseCAC, Format(                                       //SS-1014-NDR-20120123
//      'SELECT CodigoPersona FROM Personas WHERE CodigoDocumento = ''RUT'' ' +                              //SS-1014-NDR-20120123
//      'AND NumeroDocumento = ''%s''', [iif(peNumeroDocumento.Text <> '',                                   //SS-1014-NDR-2012012
//      									trim(PadL(peNumeroDocumento.Text, 9, '0')), '')]));              //SS-1014-NDR-20120123
//    if CodigoCliente > 0 then                                                                              //SS-1014-NDR-20120123
//        CargarConveniosRUT(DMConnections.BaseCAC, cbConvenios, 'RUT', peNumeroDocumento.Text, False, 0)    //SS-1014-NDR-20120123
//    else begin                                                                                             //SS-1014-NDR-20120123
//    	cbConvenios.Clear;                                                                                   //SS-1014-NDR-20120123
//    end;                                                                                                   //SS-1014-NDR-20120123
//                                                                                                           //SS-1014-NDR-20120123
//    if cbConvenios.Items.Count > 0 then cbConvenios.ItemIndex := 0;                                        //SS-1014-NDR-20120123
//                                                                                                           //SS-1014-NDR-20120123
//    CargarRecibos;                                                                                         //SS-1014-NDR-20120123
end;

{-----------------------------------------------------------------------------
  Function Name: cbConveniosChange
  Author:    flamas
  Date Created: 21/12/2004
  Description: Procesa un Cambio de Convenio - Trae los comprobantes impagos
  				del dicho Convenio
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure Tfrm_AnulacionRecibos.cbConveniosChange(Sender: TObject);
begin
    if ActiveControl <> Sender then Exit;                                     //SS-1014-NDR-20120123
    CargarRecibos;                                                            //SS-1014-NDR-20120123
end;

//BEGIN:SS_1120_MVI_20130820 -------------------------------------------------
{-----------------------------------------------------------------------------
  Procedure: Tfrm_AnulacionRecibos.cbConveniosDrawItem
  Date:      20-August-2013
  Description: Verifica si existe alg�n convenio de baja para llamar a la funci�n
                que colorea de rojo el item.
-----------------------------------------------------------------------------}
procedure Tfrm_AnulacionRecibos.cbConveniosDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  if Pos( TXT_CONVENIO_DE_BAJA, cbConvenios.Items[Index].Caption ) > 0 then  begin

     ItemComboBoxColor(Control, Index, Rect, State, clred, true);

  end
  else begin

     ItemComboBoxColor(Control, Index, Rect, State);

  end;

end;
//END:SS_1120_MVI_20130820 ---------------------------------------------------

{-----------------------------------------------------------------------------
  Function Name: ObtenerMotivosAnulacion
  Author:    flamas
  Date Created: 17/05/2005
  Description:
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------
  Revision 1
  lgisuk
  12/03/07
  Ahora lista todos los motivos de anulacion, menos el motivo "Cheque Rechazado"
-----------------------------------------------------------------------------}
function  Tfrm_AnulacionRecibos.ObtenerMotivosAnulacion : boolean;
resourcestring
	MSG_ERROR_LOADING_VOID_CAUSES 	= 'Error obteniendo motivos de anulaci�n';
    MSG_ERROR						= 'Error';
begin
	result := False;
	try
        SPObtenerMotivosAnulacionPagos.Close;
        SPObtenerMotivosAnulacionPagos.Parameters.ParamByName('@OrigenPedido').Value := 'Recibo';   //Revision 8
   	 	SPObtenerMotivosAnulacionPagos.Open;
        SPObtenerMotivosAnulacionPagos.First;
        while not SPObtenerMotivosAnulacionPagos.Eof do begin
	        	cbMotivosAnulacion.Items.Add( SPObtenerMotivosAnulacionPagos.FieldByName('Descripcion').AsString, SPObtenerMotivosAnulacionPagos.FieldByName('CodigoMotivoAnulacion').AsInteger);
            SPObtenerMotivosAnulacionPagos.Next;
        end;
        SPObtenerMotivosAnulacionPagos.Close;
        result := cbMotivosAnulacion.Items.Count > 0;
    except
        on E : Exception do begin
            MsgBoxErr(MSG_ERROR_LOADING_VOID_CAUSES, e.Message, MSG_ERROR, MB_ICONERROR);
        end;
    end;

end;


{-----------------------------------------------------------------------------
  Function Name: CargarRecibos
  Author:    flamas
  Date Created: 20/12/2004
  Description: Carga los comprobantes impagos de un Convenio determinado
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
function Tfrm_AnulacionRecibos.CargarRecibos : boolean;
resourcestring
    MSG_ERROR_LOADING_TICKET 	= 'Error obteniendo el recibo';
    MSG_ERROR					= 'Error';
    //SQL_ESTACONVENIOENLISTAAMARILLA = 'SELECT dbo.EstaConvenioEnListaAmarilla( ''%s'' ,GETDATE() )';      // SS_660_CQU_20130711 //SS_660_MVI_20130711
    //SQL_ESTACONVENIOENLISTAAMARILLA = 'SELECT dbo.EstaConvenioEnListaAmarilla( ''%s'' ,GETDATE(), NULL )';  //SS_660_MVI_20130729 // SS_660_CQU_20130711
   SQL_ESTACONVENIOENLISTAAMARILLA = 'SELECT dbo.ObtenerEstadoConvenioEnListaAmarilla(%s) ';                                                //SS_660_MVI_20130729
   SQL_OBTENER_NUMERODOCUMENTO_CLIENTE = 'SELECT dbo.ObtenerNumeroDocumentoPorConvenio(%s)';                                                //SS_1408_MCA_20151027
var                                                                                                                                          //SS_660_MVI_20130729
     EstadoListaAmarilla : String;                                                                                                           //SS_660_MVI_20130729
     NumeroDocumento : String;                                                                                                              //SS_1408_MCA_20151027
begin

      //  lblEnListaAmarilla.Color:=clYellow;                                                                                            //SS_660_MVI_20130729      //SS_660_MVI_20130711
      //  lblEnListaAmarilla.Visible  := QueryGetValue(   DMConnections.BaseCAC,                                                         //SS_660_MVI_20130729      //SS_660_MVI_20130711
      //                                                  Format(SQL_ESTACONVENIOENLISTAAMARILLA,                                        //SS_660_MVI_20130729      //SS_660_MVI_20130711
      //                                                          [   cbConvenios.Value                                                  //SS_660_MVI_20130729      //SS_660_MVI_20130711
      //                                                          ]                                                                      //SS_660_MVI_20130729      //SS_660_MVI_20130711
      //                                                        )                                                                        //SS_660_MVI_20130729      //SS_660_MVI_20130711
      //                                               )='True';                                                                         //SS_660_MVI_20130729      //SS_660_MVI_20130711

       EstadoListaAmarilla:= QueryGetValue(   DMConnections.BaseCAC,                                                                         //SS_660_MVI_20130729
                                                 Format(SQL_ESTACONVENIOENLISTAAMARILLA,                                                     //SS_660_MVI_20130729
                                                         [   IntToStr(cbConvenios.Value)                                                    //SS_660_MVI_20130729
                                                         ]                                                                                   //SS_660_MVI_20130729
                                                       )                                                                                     //SS_660_MVI_20130729
                                                       );                                                                                    //SS_660_MVI_20130729
                                                                                                                                             //SS_660_MVI_20130729
//        if EstadoListaAmarilla <> 'NULL' then begin                                                                                        //SS_660_MVI_20130909  //SS_660_MVI_20130729
       if EstadoListaAmarilla <> '' then begin                                                                                           //SS_660_MVI_20130909  //SS_660_MVI_20130729
        lblEnListaAmarilla.Visible  := True;                                                                                                 //SS_660_MVI_20130729
        lblEnListaAmarilla.Caption  := EstadoListaAmarilla;                                                                                  //SS_660_MVI_20130729
                                                                                                                                             //SS_660_MVI_20130729
        end                                                                                                                                  //SS_660_MVI_20130729
        else begin                                                                                                                           //SS_660_MVI_20130729
                                                                                                                                             //SS_660_MVI_20130729
        lblEnListaAmarilla.Visible  := False;                                                                                                //SS_660_MVI_20130729
                                                                                                                                             //SS_660_MVI_20130729
        end;


	  Result := False;
    spObtenerRecibosParaAnulacion.Close;
	  spObtenerRecibosParaAnulacion.Parameters.ParamByName('@CodigoConvenio').Value := cbConvenios.Value;
	  if (edNumeroRecibo.Text <> '') then
        spObtenerRecibosParaAnulacion.Parameters.ParamByName('@NumeroRecibo').Value := edNumeroRecibo.Value
	  else
        spObtenerRecibosParaAnulacion.Parameters.ParamByName('@NumeroRecibo').Value := null;

    try
        spObtenerComprobantesRecibo.DisableControls;
		    spObtenerRecibosParaAnulacion.Open;
        spObtenerComprobantesRecibo.EnableControls;
	      LimpiarDetalles;

        // Cargamos el Detalle del Recibo.
        with spObtenerRecibosParaAnulacion do begin
        	if not IsEmpty then begin
                DisableControls;
                FNumeroRecibo := FieldByName('NumeroRecibo').AsInteger;
                FImporteRecibo := FieldByName('Importe').AsFloat;
                FCodigoConvenio := FieldByName('CodigoConvenio').AsInteger;
                dFechaRecibo := FieldByName('FechaHora').AsDateTime;
                EnableControls;
                CargarDetalleRecibos( FieldByName('NumeroRecibo').AsInteger );
                deFechaAnulacion.Date := NowBase(DMCOnnections.BaseCAC);
                deFechaAnulacion.ReadOnly := not ExisteAcceso('Modif_Fecha_Anulacion_Recibo');
                cbMotivosAnulacion.ItemIndex := 0;
                cbCanalesDePago.ItemIndex := 0;

                NumeroDocumento :=   Trim(QueryGetValue(DMConnections.BaseCAC, Format(SQL_OBTENER_NUMERODOCUMENTO_CLIENTE, [IntToStr(FCodigoConvenio)]))); //SS_1408_MCA_20151027

                if NumeroDocumento <> EmptyStr then                                                                                                         //SS_1408_MCA_20151027
                EstaClienteConMensajeEspecial(DMCOnnections.BaseCAC, NumeroDocumento);                                                                      //SS_1408_MCA_20151027


            end;
            HabilitarRadioOpciones;

        end;
        Result := true;
    except
        on e: Exception do
            MsgBoxErr(MSG_ERROR_LOADING_TICKET, e.Message, MSG_ERROR, MB_ICONERROR);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: LimpiarDetalles;
  Author:    flamas
  Date Created: 26/01/2005
  Description: Borra los detalles del Comprobante
  Parameters: None
  Return Value: None
-----------------------------------------------------------------------------}
procedure Tfrm_AnulacionRecibos.LimpiarDetalles;
begin
    deFechaAnulacion.Date := NullDate;
    cbMotivosAnulacion.Value := Null;
    cbCanalesDePago.Value  := Null;
    btnAnular.Enabled := False;
    DBListEx1.Invalidate;
    DBListEx2.Invalidate;
end;

{-----------------------------------------------------------------------------
  Function Name: peNumeroDocumentoButtonClick
  Author:    flamas
  Date Created: 21/12/2004
  Description: Procesa un cambio en el RUT - Muestra los comprobantes pendientes
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure Tfrm_AnulacionRecibos.peNumeroDocumentoButtonClick(Sender: TObject);
var
    f: TFormBuscaClientes;
begin
    Application.CreateForm(TFormBuscaClientes,F);
	if F.Inicializa(FUltimaBusqueda) then begin
        F.ShowModal;
        if F.ModalResult = idOk then begin
            FUltimaBusqueda := F.UltimaBusqueda;
            peNumeroDocumento.Text := f.Persona.NumeroDocumento;
        end;
	end;
    F.free;
end;

{-----------------------------------------------------------------------------
  Function Name: edNumeroReciboChange
  Author:    flamas
  Date Created: 17/05/2005
  Description:
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure Tfrm_AnulacionRecibos.edNumeroReciboChange(Sender: TObject);
//    const																															//SS-1014-NDR-20120123
//        MSG_ERROR_TITULO         = 'Anulaci�n Recibo';																			//SS-1014-NDR-20120123
//        MSG_ERROR_RECIBO_REFINAN = 'El Recibo pertenece a una Cancelaci�n por Avenimiento o Repactaci�n. NO se puede Anular.';	//SS-1014-NDR-20120123
begin
    if ActiveControl = Sender then begin
        peNumeroDocumento.Clear;
        cbConvenios.Clear;
//        if not EsReciboCancelacionRefinanciacion(DMConnections.BaseCAC, edNumeroRecibo.ValueInt) then begin               //SS-1014-NDR-20120123
//            CargarRecibos;                                                                                                //SS-1014-NDR-20120123
//        end                                                                                                               //SS-1014-NDR-20120123
//        else begin                                                                                                        //SS-1014-NDR-20120123
//            if spObtenerRecibosParaAnulacion.Active then spObtenerRecibosParaAnulacion.Close;                             //SS-1014-NDR-20120123
//            if spObtenerComprobantesRecibo.Active then spObtenerComprobantesRecibo.Close;                                 //SS-1014-NDR-20120123
//            ShowMsgBoxCN(MSG_ERROR_TITULO, MSG_ERROR_RECIBO_REFINAN, MB_ICONWARNING, Self);                               //SS-1014-NDR-20120123
//       end;                                                                                                               //SS-1014-NDR-20120123
    end;
end;

procedure Tfrm_AnulacionRecibos.btnFiltrarClick(Sender: TObject);                                                           //SS-1014-NDR-20120123
Var                                                                                                                         //SS-1014-NDR-20120123
    CodigoCliente: Integer;                                                                                                 //SS-1014-NDR-20120123
const                                                                                                                       //SS-1014-NDR-20120123
  MSG_ERROR_TITULO         = 'Anulaci�n Recibo';                                                                            //SS-1014-NDR-20120123
  MSG_ERROR_RECIBO_REFINAN = 'El Recibo pertenece a una Cancelaci�n por Avenimiento o Repactaci�n. NO se puede Anular.';    //SS-1014-NDR-20120123
begin                                                                                                                       //SS-1014-NDR-20120123
  if edNumeroRecibo.ValueInt>0 then                                                                                         //SS-1014-NDR-20120123
  begin                                                                                                                     //SS-1014-NDR-20120123
    if not EsReciboCancelacionRefinanciacion(DMConnections.BaseCAC, edNumeroRecibo.ValueInt) then begin                     //SS-1014-NDR-20120123
        CargarRecibos;                                                                                                      //SS-1014-NDR-20120123
    end                                                                                                                     //SS-1014-NDR-20120123
    else begin                                                                                                              //SS-1014-NDR-20120123
        if spObtenerRecibosParaAnulacion.Active then spObtenerRecibosParaAnulacion.Close;                                   //SS-1014-NDR-20120123
        if spObtenerComprobantesRecibo.Active then spObtenerComprobantesRecibo.Close;                                       //SS-1014-NDR-20120123
                                                                                                                            //SS-1014-NDR-20120123
        ShowMsgBoxCN(MSG_ERROR_TITULO, MSG_ERROR_RECIBO_REFINAN, MB_ICONWARNING, Self);                                     //SS-1014-NDR-20120123
    end;                                                                                                                    //SS-1014-NDR-20120123
  end                                                                                                                       //SS-1014-NDR-20120123
  else if Trim(peNumeroDocumento.Text)<>'' then                                                                             //SS-1014-NDR-20120123
  begin                                                                                                                     //SS-1014-NDR-2012012
    edNumeroRecibo.Clear;                                                                                                   //SS-1014-NDR-20120123
    CodigoCliente := QueryGetValueInt(DMConnections.BaseCAC, Format(                                                        //SS-1014-NDR-20120123
      'SELECT CodigoPersona FROM Personas WHERE CodigoDocumento = ''RUT'' ' +                                               //SS-1014-NDR-20120123
      'AND NumeroDocumento = ''%s''', [iif(peNumeroDocumento.Text <> '',                                                    //SS-1014-NDR-20120123
                        trim(PadL(peNumeroDocumento.Text, 9, '0')), '')]));                                                 //SS-1014-NDR-20120123
    if CodigoCliente > 0 then begin                                                                                         //SS_1408_MCA_20151027 //SS-1014-NDR-20120123
        EstaClienteConMensajeEspecial(DMConnections.BaseCAC, peNumeroDocumento.Text);                                       //SS_1408_MCA_20151027
        CargarConveniosRUT(DMConnections.BaseCAC, cbConvenios, 'RUT', peNumeroDocumento.Text, False, 0);                    //SS-1014-NDR-20120123
    end                                                                                                                     //SS_1408_MCA_20151027
    else begin                                                                                                              //SS-1014-NDR-20120123
      cbConvenios.Clear;                                                                                                    //SS-1014-NDR-20120123
    end;                                                                                                                    //SS-1014-NDR-20120123
    if cbConvenios.Items.Count>0 then																						//SS-1014-NDR-20120123
    begin																													//SS-1014-NDR-20120123
      if cbconvenios.ItemIndex>cbConvenios.Items.Count-1 then																//SS-1014-NDR-20120123
      begin																													//SS-1014-NDR-20120123
        cbConvenios.ItemIndex:=0;																							//SS-1014-NDR-20120123
      end;																													//SS-1014-NDR-20120123
      CargarRecibos;                                                                                                        //SS-1014-NDR-20120123
    end;																													//SS-1014-NDR-20120123
  end;                                                                                                                      //SS-1014-NDR-20120123
end;                                                                                                                        //SS-1014-NDR-20120123

procedure Tfrm_AnulacionRecibos.btnSalirClick(Sender: TObject);
begin
    close;
end;

procedure Tfrm_AnulacionRecibos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    action := caFree;
end;

procedure Tfrm_AnulacionRecibos.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if (Key = KEY_F9) and btnAnular.Enabled then btnAnular.Click;
end;

{
    Procedure Name: btnAnularClick
    Parameters : Sender: TObject
    Author : flamas
    Date Created : 21/12/2004

    Description : Anula un recibo.

    Revision : 11
        Author: pdominguez
        Date: 04/06/2010
        Description: Infractores Fase 2
            - Actualiza las infracciones asociadas a los comprobantes cubiertos por el
            recibo que se anula, en el caso de que estos tengan Conceptos Infractores.
}
procedure Tfrm_AnulacionRecibos.btnAnularClick(Sender: TObject);
resourcestring
    MSG_ERROR 								= 'Error';
    MSG_ERROR_ANULANDO_RECIBO				= 'Error al anular el Recibo';
    MSG_RECIBO_ANULADO_CORRECTAMENTE		= 'El recibo N� %d se anul� correctamente';
    MSG_CONFIRMAR_ANULACION 				= 'Desea anular el recibo ?';
    MSG_CAPTION 							= 'Anulaci�n de Recibo';
    MSG_FECHA_ANULACION_MAYOR_RECIBO 		= 'La fecha de anulaci�n debe ser mayor o igual a la del recibo.';
    MSG_DEBE_SELECCIONAR_MOTIVO_ANULACION 	= 'Debe seleccionar un motivo de anulaci�n.';
    MSG_ANULA_OTRO_RECIBO 					= 'Desea anular otro recibo ?';
    MSG_DEBE_SELECCIONAR_CANAL_DE_PAGO      = 'Debe seleccionar canal de pago';
    MSG_ERROR_RECIBO_ANULADO                = 'El recibo seleccionado ya estaba anulado';
    //REV 10
    MSG_RECIBO_NO_ANULABLE                  = 'El Recibo no se puede anular porque el Comprobante pagado con �ste ya fue anulado por Nota de Cr�dito. '#10#13 +
                                              'Favor avisar al Departamento de Sistemas';
var
  dHoraBase : TDateTime;
begin
	  // Valida los datos para anular
    if not ValidateControls(
      [deFechaAnulacion, cbMotivosAnulacion],
      [Trunc(deFechaAnulacion.Date) >= Trunc(dFechaRecibo), cbMotivosAnulacion.ItemIndex <> -1 ],
      caption,
      [MSG_FECHA_ANULACION_MAYOR_RECIBO,
       MSG_DEBE_SELECCIONAR_MOTIVO_ANULACION]) then begin
        exit;
	end;
    if (spObtenerRecibosParaAnulacion.Active and (not spObtenerRecibosParaAnulacion.IsEmpty)
        and (spObtenerRecibosParaAnulacion.FieldByName('Estado').AsString = 'A')) then
    begin
		    MsgBoxErr(MSG_ERROR_RECIBO_ANULADO,'',MSG_ERROR,MB_ICONERROR);
    end;

    // REV 10: si el recibo no es anulable, envio un mensaje y salgo del m�dulo
    if QueryGetValueInt (DMConnections.BaseCAC, Format ('SELECT dbo.ReciboEsAnulable(%d)',[FNumeroRecibo])) = 0 then begin
        MsgBox(MSG_RECIBO_NO_ANULABLE, MSG_CAPTION, MB_ICONWARNING);
        btnAnular.Enabled := False;
        Exit;
    end;

    if MsgBox(MSG_CONFIRMAR_ANULACION, MSG_CAPTION, MB_ICONQUESTION or MB_DEFBUTTON2 or MB_YESNO) <> IDYES then
        Exit;
		//    if not ValidarDatosTurno then Exit;

   	Screen.Cursor := crHourGlass;
    try
        try
            DMConnections.BaseCAC.BeginTrans;

            if not GenerarDebitoDK then begin

                Screen.Cursor := crDefault;
                DMConnections.BaseCAC.RollbackTrans;
                Exit;
            end;

            dHoraBase := NowBase(DMCOnnections.BaseCAC);

            with spAnularRecibo, spAnularRecibo.Parameters do begin

	            ParamByName('@NumeroRecibo').Value 			:= FNumeroRecibo;
	            ParamByName('@FechaAnulacion').Value 		:= StrToDateTime(DateTimeToStr(deFechaAnulacion.Date + (dHoraBase - Int(dHoraBase)) ));
                ParamByName('@CodigoMotivoAnulacion').Value := cbMotivosAnulacion.Value;
                ParamByName('@CodigoUsuario').Value 		:= UsuarioSistema;
                ParamByName('@TipoComprobante').Value		:= (QueryGetValue(DMConnections.BaseCAC, 'SELECT DBO.CONST_TIPO_COMPROBANTE_NOTA_DEBITO_DK ()'));
                ParamByName('@NumeroComprobante').Value     := spCrearNotaDebitoDK.Parameters.ParamByName('@NumeroNotaDebitoDK').Value;
                ParamByName('@NumeroMovimiento').Value      :=  spCrearNotaDebitoDK.Parameters.ParamByName('@NumeroMovimiento').Value;
                ParamByName('@CodigoAnulacion').Value 		:= Null;
                ExecProc;

            end;

            with spActualizarInfraccionesPagoAnulado, spActualizarInfraccionesPagoAnulado.Parameters do begin
                spObtenerComprobantesRecibo.First;
                 while not spObtenerComprobantesRecibo.Eof do begin
                     if spObtenerComprobantesRecibo.FieldByName('TieneConceptosInfractor').AsInteger = 1 then begin
                        ParamByName('@NumeroComprobante').Value := spObtenerComprobantesRecibo.FieldByName('NumeroComprobante').AsInteger;
                        ParamByName('@TipoComprobante').Value   := spObtenerComprobantesRecibo.FieldByName('TipoComprobante').AsString;
                        ExecProc;
                    end;
                    spObtenerComprobantesRecibo.Next;
                end;
            end;

            DMConnections.BaseCAC.CommitTrans;
            MsgBox(Format(MSG_RECIBO_ANULADO_CORRECTAMENTE, [FNumeroRecibo]),MSG_CAPTION, MB_ICONINFORMATION);

	        except
	            on e: Exception do begin
					DMConnections.BaseCAC.RollbackTrans;
	                MsgBoxErr(MSG_ERROR_ANULANDO_RECIBO, e.Message, MSG_ERROR, MB_ICONERROR);
	            end;
	        end;
    finally
        Screen.Cursor := crDefault;
		edNumeroRecibo.Clear;
		peNumeroDocumento.Clear;
		cbConvenios.Clear;
		LimpiarDetalles;
        btnAnular.Enabled := False;
		spObtenerRecibosParaAnulacion.Close;
        spObtenerComprobantesRecibo.Close;
	    if MsgBox(MSG_ANULA_OTRO_RECIBO, MSG_CAPTION, MB_ICONQUESTION or MB_DEFBUTTON2 or MB_YESNO) <> IDYES then Close
	end;
end;

Function Tfrm_AnulacionRecibos.GenerarDebitoDK : boolean;
resourcestring
	MSG_ERROR_GENERANDO_DEBITO_DK  			= 'Error generando Debito';
	MSG_ERROR								= 'Error';
	MSG_DEBITODK_GENERADO_CORRECTAMENTE     = 'El Debito N� %d se gener� correctamente';
    MSG_CAPTION                             = 'Generar Debito';
begin
    try
        Result := True;
        spCrearNotaDebitoDK.Parameters.Refresh;
        if (FCodigoConvenio = 0) then
            spCrearNotaDebitoDK.Parameters.ParamByName('@CodigoConvenio').Value      := null
        else
        spCrearNotaDebitoDK.Parameters.ParamByName('@CodigoConvenio').Value      := FCodigoConvenio ;

        spCrearNotaDebitoDK.Parameters.ParamByName('@Importe').Value             := FImporteRecibo  ;  //el store lo hace *100
        spCrearNotaDebitoDK.Parameters.ParamByName('@CodigoTipoMedioPago').Value := cbCanalesDePago.Value;
        spCrearNotaDebitoDK.Parameters.ParamByName('@NumeroComprobanteNIPagado').Value := FNInodeConvenioPagada ;
        // Revision 5
        spCrearNotaDebitoDK.Parameters.ParamByName('@CodigoUsuario').Value := UsuarioSistema ;
        spCrearNotaDebitoDK.Parameters.ParamByName('@FechaAnulacion').Value := deFechaAnulacion.Date; // REV 9
        spCrearNotaDebitoDK.ExecProc;
        FNumeroDebito := spCrearNotaDebitoDK.Parameters.ParamByName('@NumeroNotaDebitoDK').Value;

        MsgBox(Format(MSG_DEBITODK_GENERADO_CORRECTAMENTE, [FNumeroDebito]),MSG_CAPTION, MB_ICONINFORMATION);
    except
        on e: Exception do begin

            MsgBoxErr(MSG_ERROR_GENERANDO_DEBITO_DK, e.Message, MSG_ERROR, MB_ICONERROR);
            Result := False;
        end;
    end;
end;

(*
function Tfrm_AnulacionRecibos.ValidarDatosTurno: Boolean;
var
    DatosTurno: TDatosTurno;
begin
    if VerTurnoAbierto(DMConnections.BaseCAC, FPuntoEntrega, FPuntoVenta, UsuarioSistema, DatosTurno) then begin
        GNumeroTurno := DatosTurno.NumeroTurno;
        Result := True;
    end else begin
        if (DatosTurno.NumeroTurno <> -1)
                and ((DatosTurno.Estado = TURNO_ESTADO_ABIERTO)
                and (Trim(DatosTurno.CodigoUsuario) <> (UpperCase(UsuarioSistema)))) then begin
            MsgBox(Format(MSG_ERROR_TURNO_ABIERTO_OTRO_USUARIO, [DatosTurno.CodigoUsuario]),
                Self.Caption, MB_ICONSTOP);
        end else begin
            MsgBox(MSG_ERROR_TURNO_CERRADO, Self.Caption, MB_ICONSTOP);
        end;
        Result := False;
    end;
end;
 *)
{******************************** Function Header ******************************
Function Name: Tfrm_AnulacionRecibos.Inicializar
Author : ggomez
Date Created : 14/07/2005
Description : Realiza la inicializaci�n del form para anular el recibo pasado
    como par�metro.
Parameters : NumeroRecibo: Int64
Return Value : Boolean
*******************************************************************************}
function Tfrm_AnulacionRecibos.Inicializar(NumeroRecibo: Int64): Boolean;
begin
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

    LimpiarDetalles;
	HabilitarRadioOpciones;
    FCodigoCanalPago := 0 ;
	FNInodeConvenioPagada := 0;
    if not ObtenerCanalesDePago then begin
        //MsgBoxErr(MSG_ERROR_INICIALIZAR, MSG_SIN_PERMISOS, MSG_CAPTION, MB_ICONERROR);
        MsgBox(MSG_SIN_PERMISOS, MSG_CAPTION, MB_ICONINFORMATION + MB_OK);
        Result := False;
        Exit;
    end;

    Result := ObtenerMotivosAnulacion;
    //Result := Result and ObtenerCanalesDePago;
    edNumeroRecibo.ValueInt := NumeroRecibo;
    CargarRecibos;
end;

procedure Tfrm_AnulacionRecibos.dsRecibosDataChange(Sender: TObject;
  Field: TField);
begin
	CargarDetalleRecibos(spObtenerRecibosParaAnulacion.FieldByName('NumeroRecibo').AsInteger);
	FNumeroRecibo   := spObtenerRecibosParaAnulacion.FieldByName('NumeroRecibo').AsInteger;
    FImporteRecibo  := spObtenerRecibosParaAnulacion.FieldByName('Importe').AsFloat;
    FCodigoConvenio := spObtenerRecibosParaAnulacion.FieldByName('CodigoConvenio').AsInteger;
    dFechaRecibo    := spObtenerRecibosParaAnulacion.FieldByName('FechaHora').AsDateTime;

    HabilitarRadioOpciones;
end;


function Tfrm_AnulacionRecibos.CargarDetalleRecibos(NumeroRecibo: Int64): boolean;
resourcestring
    MSG_ERROR_LOADING_RECIBO_DETAIL 	= 'Error obteniendo el detalle del recibo';
    MSG_ERROR					= 'Error';
begin
    Result := False;
    try
		spObtenerComprobantesRecibo.Close;
		spObtenerComprobantesRecibo.Parameters.ParamByName('@NumeroRecibo').Value := NumeroRecibo;
		spObtenerComprobantesRecibo.Open;
		//Rev 4 buscando datos para saber si el recibo es el pago de una NI no de convenio
		spObtenerComprobantesRecibo.First;
		if (spObtenerComprobantesRecibo.FieldByName('CodigoConvenio').AsInteger = 0) then //convenio null
			while not spObtenerComprobantesRecibo.eof do begin
				if (spObtenerComprobantesRecibo.FieldByName('TipoComprobante').AsString = TC_NOTA_COBRO_INFRACCIONES) then begin
				  //es de una NI no de convenio
					FNInodeConvenioPagada := spObtenerComprobantesRecibo.FieldByName('NumeroComprobante').AsInteger;
					Break;
				end;
				spObtenerComprobantesRecibo.Next;
			end;
      	Result := True;
	except
        on e: Exception do
            MsgBoxErr(MSG_ERROR_LOADING_RECIBO_DETAIL, e.Message, MSG_ERROR, MB_ICONERROR);
    end;
end;

{******************************** Function Header ******************************
Function Name: ObtenerCanalesDePago
Author :
Date Created :
Description :
Parameters : None
Return Value : boolean
Revision : 1
    Author : vpaszkowicz
    Date : 24/08/2007
    Description : Vuelvo la funci�n como estaba, ya que si no puede cambiar las
    formas de pago, no deber�a poder acceder a la ventana.
*******************************************************************************}
function Tfrm_AnulacionRecibos.ObtenerCanalesDePago: boolean;
resourcestring
	MSG_ERROR_LOADING_PAYMENT_CHANELS 	= 'Error obteniendo canales de Pago';
    MSG_ERROR						= 'Error';
begin
	result := False;
	try

// jconcheyro 10/01/2005 solicitud servicio 43 Ahora tienen que aparecer todos los canales de pago
//        spObtenerCanalPago.Parameters.ParamByName('@CodigoCanalPago').Value := Trim(QueryGetValue(DMConnections.BaseCAC, 'SELECT DBO.CONST_CANAL_PAGO_VENTANILLA()'));
//        spObtenerCanalPago.Open;
//        cbCanalesDePago.Items.Add( spObtenerCanalPago.FieldByName('Descripcion').AsString , spObtenerCanalPago.FieldByName('CodigoTipoMedioPago').AsInteger);
//       cbCanalesDePago.Items.Add('Ninguno', 0);

        {CargarCanalesDePago(DMConnections.BaseCAC, cbCanalesDePago, 0);
        if not (cbCanalesDePago.Items.Count > 0) then begin     //Revision 6
            cbCanalesDePago.Enabled := False;                   //Revision 6
            cbCanalesDePago.Value  := Null;                     //Revision 6
        end;                                                    //Revision 6
        Result := True;}

        CargarCanalesDePago(DMConnections.BaseCAC, cbCanalesDePago, 0, 0);

        Result := cbCanalesDePago.Items.Count > 0;
    except
        on e: Exception do
            MsgBoxErr(MSG_ERROR_LOADING_PAYMENT_CHANELS, e.Message, MSG_ERROR, MB_ICONERROR);
    end;
end;



procedure Tfrm_AnulacionRecibos.txtImporteDebitoKeyPress(Sender: TObject;
  var Key: Char);
begin
    if Key = '-' then Key := #0;
end;


procedure Tfrm_AnulacionRecibos.HabilitarRadioOpciones;
begin
    cbCanalesDePago.Enabled    :=  False ;
    cbMotivosAnulacion.Enabled :=  False ;
    btnAnular.Enabled          :=  False ;

    if (spObtenerRecibosParaAnulacion.Active and not spObtenerRecibosParaAnulacion.IsEmpty) then begin
	    cbMotivosAnulacion.Enabled  :=  True ;
        if cbCanalesDePago.Items.Count > 0 then cbCanalesDePago.Enabled := True ; //Revision 6
	    btnAnular.Enabled           :=  (spObtenerRecibosParaAnulacion.FieldByName('Estado').AsString <> 'A') ;
    end;


end;



end.
