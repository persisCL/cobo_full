{********************************** File Header ********************************
File Name   : freMovNoNominado
Author      : Malisia, Fabio
Date Created: 28/05/04
Language    : ES-AR
Description : frame para movimiento no nominado de TAGs
--------------------------------------------------------------------------------
Historial de Revisiones
--------------------------------------------------------------------------------

*******************************************************************************}

unit freMovNoNominado;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DMConnection,
  DB, ADODB, Grids, DBGrids, DBClient, Variants, peaProcs, util, UtilProc;

type
  TfreMovTeleviasNoNominado = class(TFrame)
    dbgEntrega: TDBGrid;
    ObtenerSaldosTAGsAlmacen: TADOStoredProc;
    cdsEntrega: TClientDataSet;
    dsEntrega: TDataSource;
    RegistrarMovimientoStockNoNominado: TADOStoredProc;
    qryCategorias: TADOQuery;
    procedure cdsEntregaNewRecord(DataSet: TDataSet);
  private
    FCodigoAlmacenOrigen: Integer;
    FCargando: Boolean;
    procedure setCodigoAlmacenOrigen(const Value: Integer);
    { Private declarations }
  public
    { Public declarations }
    property CodigoAlmacenOrigen: Integer read FCodigoAlmacenOrigen write setCodigoAlmacenOrigen;
    function ValidarMovimiento: Boolean;
    function GenerarMovimiento(aAlmacenDestino: Integer; aGuiaDespacho: Integer): Boolean;
    function Inicializar(aCodigoAlmacen: Integer): boolean;

  end;

implementation

uses RStrings;

//{$R *.dfm}

{ TFrame1 }

function TfreMovTeleviasNoNominado.GenerarMovimiento(aAlmacenDestino: Integer; aGuiaDespacho: Integer): Boolean;
begin
	result := false;
    if not ValidarMovimiento then exit;

	Screen.Cursor := crHourGlass;
    DMConnections.BaseCAC.BeginTrans;

    try
	    try
    	    with cdsEntrega do begin
        	    disableControls;
            	first;
	            while not eof do begin
    	        	if FieldByName('CantidadTAGs').asInteger <> 0 then begin
	    	            RegistrarMovimientoStockNoNominado.Parameters.ParamByName('@Categoria').Value :=
    	    	          FieldByName('CodigoCategoria').asInteger;
        	    	    RegistrarMovimientoStockNoNominado.Parameters.ParamByName('@Cantidad').Value :=
            	    	  FieldByName('CantidadTAGs').asInteger;
	                	RegistrarMovimientoStockNoNominado.Parameters.ParamByName('@CodigoAlmacenOrigen').Value :=
		                  FCodigoAlmacenOrigen;
    		            RegistrarMovimientoStockNoNominado.Parameters.ParamByName('@CodigoAlmacenDestino').Value :=
        		          aAlmacenDestino;
            		    RegistrarMovimientoStockNoNominado.Parameters.ParamByName('@CodigoGuiaDespacho').Value :=
                		  iif(aGuiaDespacho = 0, NULL, aGuiaDespacho);
		                RegistrarMovimientoStockNoNominado.Parameters.ParamByName('@Usuario').Value :=
    		              UsuarioSistema;
        		        RegistrarMovimientoStockNoNominado.ExecProc;
            	    end;

                	next;
	            end;

    	        cdsEntrega.first;
        	    while not cdsEntrega.eof do begin
            	    cdsEntrega.edit;
                	cdsEntrega.fieldByName('CantidadTAGs').AsInteger := 0;
	                next;
    	        end;

        	    SetCodigoAlmacenOrigen(FCodigoAlmacenOrigen);
            	enableControls;
            end;

            result := true;
        except
		    on e: Exception do begin
                MsgBoxErr(MSG_ERROR_GENERAR_MOVIMIENTO, e.message, self.caption, MB_ICONSTOP);
			    result := false
    		end;
        end;
    finally
        Screen.Cursor := crDefault;
        cdsEntrega.EnableControls;

        if result then begin
            DMConnections.BaseCAC.CommitTrans;
        end
        else DMConnections.BaseCAC.RollbackTrans;

	    if not cdsEntrega.isEmpty then
	        MsgBox('Datos grabados con �xito', self.caption, MB_ICONINFORMATION);
    end;
end;

function TfreMovTeleviasNoNominado.Inicializar(aCodigoAlmacen: Integer): boolean;
begin
    result := true;
    FCargando := true;
    try
        cdsEntrega.CreateDataSet;
        cdsEntrega.EmptyDataSet;
        cdsEntrega.open;
        with qryCategorias do begin
            close;
            open;
            while not eof do begin
                cdsEntrega.AppendRecord([fieldByName('Descripcion').AsString,
                  0, 0, fieldByName('Categoria').AsInteger]);
                next;
            end;
        end;
        CodigoAlmacenOrigen := aCodigoAlmacen;
    except
        result := false;
    end;
    FCargando := False;
end;

procedure TfreMovTeleviasNoNominado.setCodigoAlmacenOrigen(const Value: Integer);
begin
    FCodigoAlmacenOrigen := Value;
    with ObtenerSaldosTAGsAlmacen do begin
        close;
        Parameters.paramByname('@CodigoAlmacen').value := Value;
        open;
        while not eof do begin
            cdsEntrega.locate('CodigoCategoria', VarArrayOf([fieldByName('Categoria').asInteger]), []);
            cdsEntrega.edit;
            cdsEntrega.fieldByName('CantidadTAGsDisponibles').AsInteger := fieldByName('Saldo').AsInteger;
            cdsEntrega.post;
            next;
        end;
    end;
end;

function TfreMovTeleviasNoNominado.ValidarMovimiento: Boolean;
resourceString
    CAPTION_VALIDAR_ENTREGA = 'Movimiento de telev�as';
    ERROR_VALIDAR_ENTREGA = 'Existen cantidades a entregar superiores a las disponibles';
    ERROR_CANTIDAD_NULA = 'Se debe asignar la cantidad de telev�as';
var
    cantidadTotal: Integer;
begin
	result := true;

    with cdsEntrega do begin;
        disablecontrols;
        first;
        cantidadTotal := 0;
        while (not eof) do begin
            cantidadTotal := cantidadTotal + FieldByName('CantidadTAGs').asInteger;
            result := result and (fieldByName('CantidadTAGs').asInteger <= fieldByName('CantidadTAGsDisponibles').asInteger);
            next;
        end;
        enableControls;
    end;
    ValidateControls([dbgEntrega, dbgEntrega], [result, CantidadTotal > 0], CAPTION_VALIDAR_ENTREGA, [ERROR_VALIDAR_ENTREGA, ERROR_CANTIDAD_NULA]);
    result := result and (CantidadTotal > 0);
end;

procedure TfreMovTeleviasNoNominado.cdsEntregaNewRecord(DataSet: TDataSet);
begin
    if not FCargando then
        abort;
end;

end.


