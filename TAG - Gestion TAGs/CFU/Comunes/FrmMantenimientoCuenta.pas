{********************************** File Header ********************************
File Name   : FrmMantenimientoCuenta
Author      :
Date Created:
Language    : ES-AR
Description :
--------------------------------------------------------------------------------
Historial de Revisiones
--------------------------------------------------------------------------------
Date		: 05-Feb-2004
Author		: Castro, Ra�l <rcastro@dpsautomation.com>
Description : Agregado de Roles en Cuentas

Firma       : SS_1419_NDR_20151130
Descripcion : Se envia el parametro @CodigoUsuario al SP ActualizarDatosPersona
            Para grabarlo en los campos de auditoria de la tabla Personas y en
            el HistoricoPersonas
*******************************************************************************}

unit FrmMantenimientoCuenta;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DmiCtrls, validate, Dateedit, ComCtrls, DbList,
  DB, DBClient, ADODB, PeaTypes, PeaProcs, Util, UtilProc, UtilDB,
  FrmEditarDebito, MaskCombo, DPSControls, ExtCtrls, BuscaTab,
  BuscaClientes, Buttons, ListBoxEx, DBListEx, FrmEditarDomicilio,
  DMConnection, frmInfoPlanesComerciales, SeleccionarRolYPersona, UtilFacturacion;

type
  TFormMantenimientoCuenta = class(TForm)
	PageControl: TPageControl;
    tab_cliente: TTabSheet;
    tab_Cuenta: TTabSheet;
	GroupBoxVehiculo: TGroupBox;
    Label13: TLabel;
	Label14: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    chk_contag: TCheckBox;
    chk_detallepasadas: TCheckBox;
	cb_color: TComboBox;
    cb_marca: TComboBox;
    cb_modelo: TComboBox;
    GroupBoxCondiciones: TGroupBox;
    rb_prepago: TRadioButton;
    rb_pospagomanual: TRadioButton;
	rb_pospagoautomatico: TRadioButton;
    cb_debitoautomatico: TComboBox;
    tab_DebitosAutomaticos: TTabSheet;
    lblNombre: TLabel;
    lblApellido: TLabel;
	Label17: TLabel;
    lblFechaNacCreacion: TLabel;
    lblSexo: TLabel;
    txt_nombre: TEdit;
    txt_apellido: TEdit;
    txt_fechanacimiento: TDateEdit;
    cb_sexo: TComboBox;
    Label4: TLabel;
    tab_Contratos: TTabSheet;
	ListaDebitos: TDBList;
	Label1: TLabel;
    tab_Facturacion: TTabSheet;
	GroupBox4: TGroupBox;
	rb_separada: TRadioButton;
    rb_agrupar: TRadioButton;
    ListaGrupoCuentas: TDBList;
	Label16: TLabel;
    ListaContratos: TDBList;
	DebitosAutomaticos: TClientDataSet;
	BuscarCliente: TADOStoredProc;
    qry_Debitos: TADOQuery;
	Label23: TLabel;
    cb_TipoCliente: TComboBox;
	Contratos: TClientDataSet;
    qry_Contratos: TADOQuery;
    ObtenerContratoPlan: TADOStoredProc;
    qry_GruposCuentas: TADOQuery;
    Label15: TLabel;
    cb_GrupoFacturacion: TComboBox;
    Label22: TLabel;
    cb_PlanComercial: TComboBox;
	ActualizarDatosCliente: TADOStoredProc;
    ActualizarDebitoAutomatico: TADOStoredProc;
    chk_enviofacturas: TCheckBox;
    ActualizarCuentaCliente: TADOStoredProc;
    Label38: TLabel;
	CrearContrato: TADOStoredProc;
	qry_DatosCuenta: TADOQuery;
    qry_ContratosHechos: TADOQuery;
    qry_OS: TADOQuery;
    cbTipoNumeroDocumento: TMaskCombo;
    mcPatente: TMaskCombo;
	Label8: TLabel;
    cb_pais: TComboBox;
    lblApellidoMaterno: TLabel;
    txt_ApellidoMaterno: TEdit;
    btn_nueva: TDPSButton;
    btn_modificar: TDPSButton;
    btn_quitar: TDPSButton;
	btn_imprimir: TDPSButton;
    btn_reimprimir: TDPSButton;
    btn_firmar: TDPSButton;
    OPButton1: TDPSButton;
	OPButton2: TDPSButton;
	DebitosAutomaticosBorrados: TClientDataSet;
    qry_DebitosAutomaticosBorrar: TADOQuery;
    Label47: TLabel;
    anio: TNumericEdit;
    GroupDimensiones: TGroupBox;
    Label48: TLabel;
    Label49: TLabel;
    largoVehiculo: TNumericEdit;
    LargoAdicionalVehiculo: TNumericEdit;
    chk_acoplado: TCheckBox;
    txt_DescCategoria: TEdit;
    txt_codigoCategoria: TNumericEdit;
    cbPersoneria: TComboBox;
	Label53: TLabel;
    cb_actividad: TComboBox;
    Label12: TLabel;
    cbSituacionIVA: TComboBox;
    ObtenerDomiciliosPersona: TADOStoredProc;
    Label2: TLabel;
    Label3: TLabel;
    ObtenerOrdenServicioAdhesionDomicilios: TADOStoredProc;
    ActualizarDatosPersona: TADOStoredProc;
	ActualizarDomicilio: TADOStoredProc;
    ActualizarDomicilioEntregaPersona: TADOStoredProc;
	EliminarDomicilioPersona: TADOStoredProc;
    cbDomicilioFacturacion: TEdit;
    cbDomicilioInfoComercial: TEdit;
    ActualizarMedioComunicacionPersona: TADOStoredProc;
    Label34: TLabel;
    Roles: TTabSheet;
    btn_AgregarRol: TDPSButton;
    btn_QuitarRol: TDPSButton;
    qry_RolesCuenta: TADOQuery;
    ListaRoles: TDBListEx;
    DataSource1: TDataSource;
    Label35: TLabel;
    ActualizarRolesCuentas: TADOStoredProc;
    BorrarRolesCuentas: TADOStoredProc;
    ADOQuery1: TADOQuery;
    GBDomicilios: TGroupBox;
    lblnada: TLabel;
    Label5: TLabel;
    LblDomicilioComercial: TLabel;
    LblDomicilioParticular: TLabel;
    Label6: TLabel;
    BtnDomicilioComercialAccion: TDPSButton;
    BtnDomicilioParticularAccion: TDPSButton;
    RGDomicilioEntrega: TRadioGroup;
    ChkEliminarDomicilioParticular: TCheckBox;
    ChkEliminarDomicilioComercial: TCheckBox;
    GBMediosComunicacion: TGroupBox;
    Label7: TLabel;
    Label18: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    txtAreaTelefonoParticular: TEdit;
    txtTelefonoParticular: TEdit;
    txtTelefonoComercial: TEdit;
    txtTelefonoMovil: TEdit;
    txtEmailParticular: TEdit;
    txtAreaTelefonoMovil: TEdit;
    txtAreaTelefonoComercial: TEdit;
    ObtenerDatosDomicilio: TADOStoredProc;
    ActualizarDomicilioRelacionado: TADOStoredProc;
	procedure CambioDocumento(Sender: TObject);
	procedure ListaDebitosDrawItem(Sender: TDBList; Tabla: TDataSet;
	  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
	procedure ActualizarPago(Sender: TObject);
    procedure ListaContratosDrawItem(Sender: TDBList; Tabla: TDataSet;
      Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
    procedure CambioAgrupacion(Sender: TObject);
    procedure ListaGrupoCuentasDrawItem(Sender: TDBList; Tabla: TDataSet;
	  Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
	procedure ListaContratosRefresh(Sender: TObject);
	procedure ListaDebitosRefresh(Sender: TObject);
	procedure OPButton1Click(Sender: TObject);
	procedure chk_detallepasadasClick(Sender: TObject);
	procedure cb_PlanComercialClick(Sender: TObject);
	procedure btn_nuevaClick(Sender: TObject);
	procedure btn_modificarClick(Sender: TObject);
    procedure cbTipoNumeroDocumentoChange(Sender: TObject);
    procedure btn_quitarClick(Sender: TObject);
    procedure chk_acopladoClick(Sender: TObject);
    procedure cb_marcaChange(Sender: TObject);
    procedure anioChange(Sender: TObject);
    procedure cb_modeloChange(Sender: TObject);
    procedure cbPersoneriaChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
	procedure txtTelefonoParticularKeyPress(Sender: TObject;
      var Key: Char);
    procedure txtTelefonoComercialKeyPress(Sender: TObject; var Key: Char);
    procedure txtTelefonoMovilKeyPress(Sender: TObject; var Key: Char);
    procedure Label34Click(Sender: TObject);
    procedure btn_AgregarRolClick(Sender: TObject);
    procedure btn_QuitarRolClick(Sender: TObject);
    procedure BtnDomicilioParticularAccionClick(Sender: TObject);
    procedure BtnDomicilioComercialAccionClick(Sender: TObject);
    procedure ChkEliminarDomicilioParticularClick(Sender: TObject);
    procedure ChkEliminarDomicilioComercialClick(Sender: TObject);
  private
	{ Private declarations }
    FPIN: AnsiString;
	FCodigoPersona: Integer;
	FCodigoCuenta: Integer;
	FCodigoOrdenServicio: integer;
    CodigoDomicilioParticular, CodigoDomicilioComercial, CodigoDomicilioEntrega: integer;
    DomicilioParticular: TDatosDomicilio;
    DomicilioComercial: TDatosDomicilio;

    GuardarTelefonoParticular, GuardarTelefonoComercial,
    GuardarTelefonoMovil, GuardarEmail: boolean;
	function  CargarDatosCuenta: Boolean;
	procedure CargarDebitosAutomaticosGrilla;
	procedure CargarContratos;
	procedure CargarGruposCuentas;
	procedure ActualizarComboDebitos;
	procedure GuardarDatos;
	function  BuscarDebito(TipoDebito: AnsiString; CodigoEntidad: Integer;
	  CuentaDebito: AnsiString): Integer;
	procedure CrearNuevoDebito(TipoDebito: AnsiString; CodigoEntidad: Integer;
	  CuentaDebito: AnsiString);
    procedure TraerDimensionesVehiculo;
    procedure TraerDescripcionCategoria;
    procedure HabilitarCamposPersoneria(Personeria: Char);
	procedure GuardarMediosComunicacionContacto (persona: integer);
	procedure GuardarMedioComunicacion(persona, mediocontacto, tipoorigen: integer; valor: AnsiString);
//    procedure AltaDomicilio(tipoOrigenDatos: integer; var datosDomicilio: TDatosDomicilio);
    procedure ModificarDomicilio(tipoOrigenDatos: integer; var datosDomicilio: TDatosDomicilio);
    function  EstaElegidoComoDomicilioEntrega(tipoOrigenDatos: integer): boolean;
    procedure MarcarComoDomicilioEntrega(tipoOrigenDatos: integer);
    procedure PrepararMostrarDomicilio (descripcion: AnsiString; tipoOrigen: integer);
    procedure MostrarDomicilio (var L: TLabel; descripcion: AnsiString);
  public
	{ Public declarations }
	function Inicializa(IndicePaginaInicio: Integer = 0; CodigoCuenta: Integer = 0): Boolean;
	function InicializaOS(CodigoOS: Integer): Boolean;
    function PuedeBorrarDebito(Cliente, Debitoautomatico: integer;
                    Descripcion: AnsiString; var volverApreguntar: boolean): boolean;
  end;

var
  FormMantenimientoCuenta: TFormMantenimientoCuenta;

implementation


{$R *.dfm}
{ TFormMantenimientoCuenta }

resourcestring
    CAPTION_BOTON_DOMICILIO_ALTA = 'Alta';
    CAPTION_BOTON_DOMICILIO_MODIFICAR = 'Modificar';
    MSG_ERROR_CUENTA_DEBITO        = 'Ya existe otra Cuenta de D�bito con los datos indicados.';
    MSG_ASIGNAR_GRUPO_FACTURACION   = '<Asignado por el sistema>';

function TFormMantenimientoCuenta.Inicializa(IndicePaginaInicio: Integer; CodigoCuenta: Integer): Boolean;
resourcestring
	CAPTION_FORM = 'Modificaci�n de Cuenta';
begin
	FCodigoCuenta	:= CodigoCuenta;
	CargarPersoneria(cbPersoneria);
    CodigoDomicilioParticular := -1;
    CodigoDomicilioComercial := -1;

	CargarActividadesPersona(DMConnections.BaseCAC, cb_actividad);
	cbPersoneria.ItemIndex := 0;
	FCodigoPersona	:= QueryGetValueInt(DMConnections.BaseCAC, 'SELECT CodigoPersona FROM Cuentas WHERE CodigoCuenta = ' + IntToStr(FCodigoCuenta));
	FPIN  := QueryGetValue(DMConnections.BaseCAC, 'SELECT PIN FROM Personas WHERE CodigoPersona = ' + IntToStr(FCodigoPersona));
	CargarGruposFacturacion(DMConnections.BaseCAC, cb_GrupoFacturacion);
	if FCodigoCuenta <= 0 then begin
		cb_GrupoFacturacion.Items.Insert(0, MSG_ASIGNAR_GRUPO_FACTURACION);
		cb_GrupoFacturacion.ItemIndex := 0;
	end;
	DebitosAutomaticos.CreateDataSet;
	Contratos.CreateDataSet;

	Roles.Enabled := FCodigoCuenta > 0;

	// Si hay una cuenta, cargamos los datos de la cuenta y del cliente
	if FCodigoCuenta > 0 then begin
		Caption := CAPTION_FORM;
		Result := CargarDatosCuenta;
		cbTipoNumeroDocumento.OnChange := nil;
		case IndicePaginaInicio of
			0: begin
				   PageControl.ActivePage := tab_Cliente;
				   ActiveControl := cbTipoNumeroDocumento;
			   end;
			1: begin
				   PageControl.ActivePage := tab_DebitosAutomaticos;
				   ActiveControl := ListaDebitos;
			   end;
			2: begin
				   PageControl.ActivePage := tab_Cuenta;
				   ActiveControl := mcPatente;
			   end;
			3: begin
				   PageControl.ActivePage := tab_Contratos;
				   ActiveControl := ListaContratos;
			   end;
			4: begin
				   PageControl.ActivePage := tab_Facturacion;
				   ActiveControl := rb_Separada;
			   end;
		end;

		CambioDocumento(cbTipoNumeroDocumento);

		qry_RolesCuenta.Parameters.ParamByName('CodigoCuenta').Value := FCodigoCuenta;
		qry_RolesCuenta.Open
	end else begin
		Screen.Cursor := crHourGlass;
		CargarSexos(cb_Sexo);
		CargarTiposDocumento(DMConnections.BaseCAC, cbTipoNumeroDocumento);
		CargarTiposPatente(DMConnections.BaseCAC, mcPatente, '');

		CargarTiposCliente(DMConnections.BaseCAC, cb_TipoCliente);
		CargarPaises(DMConnections.BaseCAC, cb_Pais);

		CargarMarcasVehiculos(DMConnections.BaseCAC, cb_Marca);
		CargarColoresVehiculos(DMConnections.BaseCAC, cb_Color);
		ActualizarPago(nil);
		CargarGruposCuentas;
        Screen.Cursor := crDefault;
        PageControl.ActivePage := tab_cliente;
		ActiveControl := cbTipoNumeroDocumento;
		Result := True;
	end;
end;

procedure TFormMantenimientoCuenta.CambioDocumento(Sender: TObject);
var
    PaisAux: AnsiString;
begin
    CodigoDomicilioParticular := -1;
    CodigoDomicilioComercial  := -1;
    codigodomicilioentrega    := -1;

	with BuscarCliente.Parameters do begin
		ParamByName('@CodigoDocumento').Value	:= Trim(StrRight(cbTipoNumeroDocumento.ComboText, 20));
		ParamByName('@NumeroDocumento').Value	:= cbTipoNumeroDocumento.MaskText;
		ParamByName('@Apellido').Value			:= null;
        ParamByName('@ApellidoMaterno').Value   := null;
		ParamByName('@Nombre').Value 			:= null;
		ParamByName('@Patente').Value 			:= null;
		ParamByName('@CodigoCuenta').Value		:= null;
	end;
	With BuscarCliente do begin
		Open;
		if BuscarCliente.Eof then begin
			// Cliente Nuevo
			FCodigoPersona := 0;
            FPIN := '';
            cbPersoneria.ItemIndex := 0;
			txt_apellido.Clear;
			txt_ApellidoMaterno.Clear;
			txt_nombre.Clear;
            CargarPaises(DMConnections.BaseCAC, cb_pais, PAIS_CHILE);
			cb_sexo.Itemindex := 0;
            cb_actividad.ItemIndex := -1;
            cbSituacionIVA.ItemIndex := -1;
			txt_fechanacimiento.Clear;

            //Limpio Domicilio Personal
            LimpiarRegistroDomicilio(DomicilioParticular, TIPO_DOMICILIO_PARTICULAR);
			CodigoDomicilioParticular := -1;

            //Limpio Domicilio Comercial
            LimpiarRegistroDomicilio(DomicilioComercial, TIPO_DOMICILIO_COMERCIAL);
            CodigoDomicilioComercial := -1;

            CodigoDomicilioEntrega := -1;

            //Medios de comunicacion
            txtTelefonoParticular.Text := '';
            txtTelefonoComercial.Text := '';
            txtTelefonoMovil.Text := '';
			txtEmailParticular.Text := '';

		end else begin
			// Existe el cliente, cargamos los datos
			FCodigoPersona := FieldByName('CodigoPersona').AsInteger;
            FPIN := FieldByName('PIN').AsString;
			CargarPersoneria(cbPersoneria, FieldByName('Personeria').AsString);
			HabilitarCamposPersoneria(Trim(cbPersoneria.Text)[1]);
			CargarTiposDocumento(DMConnections.BaseCAC, cbTipoNumeroDocumento,
                                     FieldByName('CodigoDocumento').AsString);
			txt_apellido.Text := Trim(FieldByName('Apellido').AsString);
			txt_nombre.Text := Trim(FieldByName('Nombre').AsString);

			//completo Apellido Materno o Contacto comercial seg�n la Personeria
            if FieldByName('Personeria').AsString = PERSONERIA_FISICA then
                txt_ApellidoMaterno.Text := TrimRight(FieldByName('ApellidoMaterno').AsString)
            else
                //para las Juridicas mostramos el Contacto comercial
				txt_ApellidoMaterno.Text := TrimRight(FieldByName('ContactoComercial').AsString);

			CargarSexos(cb_sexo, FieldByName('Sexo').AsString);
			txt_fechanacimiento.Date := FieldByName('FechaNacimiento').AsDateTime;

            CargarTipoSituacionIVA(DMConnections.BaseCAC, cbSituacionIVA,
                                   FieldByName('CodigoSituacionIVA').AsString);
            PaisAux := iif(FieldByName('LugarNacimiento').IsNull, PAIS_CHILE,
                       Trim(FieldByName('LugarNacimiento').AsString));
            CargarPaises(DMConnections.BaseCAC, cb_Pais, PaisAux);
            txt_fechanacimiento.Date := FieldByName('FechaNacimiento').AsDateTime;
            if FieldByName('CodigoActividad').IsNull then
				CargarActividadesPersona(DMConnections.BaseCAC, cb_actividad)
			else
				CargarActividadesPersona(DMConnections.BaseCAC, cb_actividad, FieldByName('CodigoActividad').AsInteger);
			CodigoDomicilioEntrega := iif(FieldByName('CodigoDomicilioEntrega').IsNull, 0, FieldByName('CodigoDomicilioEntrega').AsInteger);
		end;
		Close;
	end;

    //***** CARGO LOS DOMICILIOS  ********//
    //1- Primero el Particular
    //2- Segundo el Comercial
    codigodomicilioparticular := ObtenerCodigoDomicilioPersona(FCodigoPersona, TIPO_DOMICILIO_PARTICULAR);

    if (codigodomicilioparticular <> -1) then begin
        {
        with ObtenerDatosDomicilio do
        begin
            Close;
            Parameters.ParamByName('@CodigoDomicilio').Value := codigodomicilioparticular;
            Open;
            if IsEmpty then begin
                LimpiarRegistroDomicilio(DomicilioParticular, TIPO_DOMICILIO_PARTICULAR);
            end
            else begin
                DomicilioParticular.CodigoDomicilio := codigodomicilioparticular;
                //IndiceDomicilio Lo utilizo para indicar que esta ingresado. Si tiene -1 es que no hay datos!!!
                DomicilioParticular.IndiceDomicilio := 1;
                DomicilioParticular.Descripcion := '';
                DomicilioParticular.CodigoTipoDomicilio := TIPO_DOMICILIO_PARTICULAR;
                DomicilioParticular.CodigoCalle := iif(FieldByName('CodigoCalle').IsNull, -1,FieldByName('CodigoCalle').AsInteger);
                DomicilioParticular.CodigoTipoCalle := iif(FieldByName('CodigoTipoCalle').IsNull, -1,FieldByName('CodigoTipoCalle').AsInteger);
                DomicilioParticular.NumeroCalle := FieldByName('Numero').AsInteger;
                DomicilioParticular.Piso := FieldByName('Piso').AsInteger;
                DomicilioParticular.Dpto := FieldByName('Dpto').AsString;
                DomicilioParticular.Detalle := FieldByName('Detalle').AsString;
                DomicilioParticular.CodigoPostal := FieldByName('CodigoPostal').AsString;
                DomicilioParticular.CodigoPais := FieldByName('CodigoPais').AsString;
                DomicilioParticular.CodigoRegion := FieldByName('CodigoRegion').AsString;
                DomicilioParticular.CodigoComuna := FieldByName('CodigoComuna').AsString;
                DomicilioParticular.CodigoCiudad := FieldByName('CodigoCiudad').AsString;
                DomicilioParticular.CalleDesnormalizada := FieldByName('CalleDesnormalizada').AsString;
                DomicilioParticular.CodigoTipoEdificacion := FieldByName('CodigoTipoEdificacion').AsInteger;
                DomicilioParticular.DomicilioEntrega := false;
                DomicilioParticular.CodigoCalleRelacionadaUno := iif(FieldByName('CodigoCalleRelacionadaUno').IsNull, -1,FieldByName('CodigoCalleRelacionadaUno').AsInteger);
                DomicilioParticular.NumeroCalleRelacionadaUno := iif(FieldByName('NumeroCalleRelacionadaUno').IsNull, -1,FieldByName('NumeroCalleRelacionadaUno').AsInteger);
                DomicilioParticular.CodigoCalleRelacionadaDos := iif(FieldByName('CodigoCalleRelacionadaDos').IsNull, -1,FieldByName('CodigoCalleRelacionadaDos').AsInteger);
                DomicilioParticular.NumeroCalleRelacionadaDos := iif(FieldByName('NumeroCalleRelacionadaDos').IsNull, -1,FieldByName('NumeroCalleRelacionadaDos').AsInteger);
                DomicilioParticular.Activo := FieldByName('Activo').AsBoolean;
            end;
        end;
        if (codigodomicilioparticular = codigodomicilioentrega) then
            MarcarComoDomicilioEntrega(TIPO_DOMICILIO_PARTICULAR);
        }
    end
    else LimpiarRegistroDomicilio(DomicilioParticular, TIPO_DOMICILIO_PARTICULAR);

    //***** DOMICILIO COMERCIAL ******//
    codigodomiciliocomercial := ObtenerCodigoDomicilioPersona(FCodigoPersona, TIPO_DOMICILIO_PARTICULAR);
    if (codigodomiciliocomercial <> -1) then begin
        {with ObtenerDatosDomicilio do
        begin
            Close;
            Parameters.ParamByName('@CodigoDomicilio').Value := codigodomiciliocomercial;
            Open;
            if IsEmpty then begin
                LimpiarRegistroDomicilio(DomicilioComercial, TIPO_DOMICILIO_PARTICULAR);
            end
            else begin
                Domiciliocomercial.CodigoDomicilio := codigodomiciliocomercial;
                Domiciliocomercial.IndiceDomicilio := 1;
                Domiciliocomercial.Descripcion := '';
                Domiciliocomercial.CodigoTipoDomicilio := TIPO_DOMICILIO_PARTICULAR;
                Domiciliocomercial.CodigoCalle := iif(FieldByName('CodigoCalle').IsNull, -1,FieldByName('CodigoCalle').AsInteger);
                DomicilioComercial.CodigoTipoCalle := iif(FieldByName('CodigoTipoCalle').IsNull, -1,FieldByName('CodigoTipoCalle').AsInteger);
                DomicilioComercial.NumeroCalle := FieldByName('Numero').AsInteger;
                DomicilioComercial.Piso := FieldByName('Piso').AsInteger;
                DomicilioComercial.Dpto := FieldByName('Dpto').AsString;
                DomicilioComercial.Detalle := FieldByName('Detalle').AsString;
                DomicilioComercial.CodigoPostal := FieldByName('CodigoPostal').AsString;
                DomicilioComercial.CodigoPais := FieldByName('CodigoPais').AsString;
                DomicilioComercial.CodigoRegion := FieldByName('CodigoRegion').AsString;
                DomicilioComercial.CodigoComuna := FieldByName('CodigoComuna').AsString;
                DomicilioComercial.CodigoCiudad := FieldByName('CodigoCiudad').AsString;
                DomicilioComercial.CalleDesnormalizada := FieldByName('CalleDesnormalizada').AsString;
                DomicilioComercial.CodigoTipoEdificacion := FieldByName('CodigoTipoEdificacion').AsInteger;
                DomicilioComercial.DomicilioEntrega := false;
                DomicilioComercial.CodigoCalleRelacionadaUno := iif(FieldByName('CodigoCalleRelacionadaUno').IsNull, -1,FieldByName('CodigoCalleRelacionadaUno').AsInteger);
                DomicilioComercial.NumeroCalleRelacionadaUno := iif(FieldByName('NumeroCalleRelacionadaUno').IsNull, -1,FieldByName('NumeroCalleRelacionadaUno').AsInteger);
                DomicilioComercial.CodigoCalleRelacionadaDos := iif(FieldByName('CodigoCalleRelacionadaDos').IsNull, -1,FieldByName('CodigoCalleRelacionadaDos').AsInteger);
                DomicilioComercial.NumeroCalleRelacionadaDos := iif(FieldByName('NumeroCalleRelacionadaDos').IsNull, -1,FieldByName('NumeroCalleRelacionadaDos').AsInteger);
                DomicilioComercial.Activo := FieldByName('Activo').AsBoolean;
            end;
        end;
        if (codigodomiciliocomercial = codigodomicilioentrega) then
            MarcarComoDomicilioEntrega(TIPO_DOMICILIO_PARTICULAR);
        }
    end
    else LimpiarRegistroDomicilio(DomicilioComercial, TIPO_DOMICILIO_PARTICULAR);


    //*** COMPLETAR  ***//
    PrepararMostrarDomicilio (iif(codigodomicilioparticular < 1, '',
                                ArmarDomicilioCompletoPorCodigo(DMConnections.BaseCAC, -1 ,codigodomicilioparticular)),
                                TIPO_ORIGEN_DATO_PARTICULAR);
    PrepararMostrarDomicilio (iif(codigodomiciliocomercial < 1, '',
                                ArmarDomicilioCompletoPorCodigo(DMConnections.BaseCAC, -1, codigodomiciliocomercial)),
                                TIPO_ORIGEN_DATO_COMERCIAL);


    //****** Fin cargar domicilios *******//

	txtTelefonoParticular.Text := ObtenerValorMediosComunicacion(FCodigoPersona, MC_TELEPHONE, TIPO_ORIGEN_DATO_PARTICULAR);
	GuardarTelefonoParticular := txtTelefonoParticular.Text <> '';
	txtTelefonoComercial.Text := ObtenerValorMediosComunicacion(FCodigoPersona, MC_TELEPHONE, TIPO_ORIGEN_DATO_COMERCIAL);
	GuardarTelefonoComercial := txtTelefonoComercial.Text <> '';
	txtTelefonoMovil.Text := ObtenerValorMediosComunicacion(FCodigoPersona, MC_MOVIL_PHONE, TIPO_ORIGEN_DATO_PARTICULAR);
	GuardarTelefonoMovil := txtTelefonoMovil.Text <> '';
	txtEMailParticular.Text := ObtenerValorMediosComunicacion(FCodigoPersona, MC_EMAIL, TIPO_ORIGEN_DATO_PARTICULAR);
	GuardarEmail := txtEMailParticular.Text <> '';

	CargarDebitosAutomaticosGrilla;
	CargarGruposCuentas;
end;

procedure TFormMantenimientoCuenta.CargarDebitosAutomaticosGrilla;
begin
    //Aca guardo si se borra alguno
    DebitosAutomaticosBorrados.Close;
	DebitosAutomaticosBorrados.CreateDataSet;

	//Aca guardo los que se van a mostrar
	DebitosAutomaticos.Close;
	DebitosAutomaticos.CreateDataSet;
	qry_Debitos.Parameters.ParamByName('CodigoPersona').Value := FCodigoPersona;
	qry_Debitos.Open;
	With qry_Debitos do while not Eof do begin
		DebitosAutomaticos.Append;
		DebitosAutomaticos.FieldByName('CodigoDebitoAutomatico').Assign(FieldByName('CodigoDebitoautomatico'));
		DebitosAutomaticos.FieldByName('TipoDebito').Assign(FieldByName('TipoDebito'));
		DebitosAutomaticos.FieldByName('CodigoEntidad').Assign(FieldByName('CodigoEntidad'));
		DebitosAutomaticos.FieldByName('CuentaDebito').Assign(FieldByName('CuentaDebito'));
		DebitosAutomaticos.FieldByName('Descripcion').Assign(FieldByName('Descripcion'));
		DebitosAutomaticos.Post;
		Next;
	end;
	qry_Debitos.Close;
	ListaDebitos.ReLoad;
	ActualizarComboDebitos;
end;

procedure TFormMantenimientoCuenta.ListaDebitosDrawItem(Sender: TDBList;
  Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState; Cols: TColPositions);
begin
	With Sender.Canvas, Tabla do begin
		FillRect(Rect);
		TextOut(Cols[0] + 1, Rect.Top + 1, FieldByName('Descripcion').AsString);
	end;
end;

procedure TFormMantenimientoCuenta.ActualizarComboDebitos;
Var
	B: TBookmark;
    indice: integer;
begin
	indice := cb_debitoautomatico.ItemIndex;
	cb_debitoautomatico.Clear;
	With DebitosAutomaticos do begin
		B := GetBookmark;
		First;
        if Eof then begin
            rb_pospagomanual.Checked;    
		end
        else
        begin
            While not Eof do begin
                cb_debitoautomatico.Items.Add(FieldByName('Descripcion').AsString);
                Next;
            end;
		end;
		GotoBookmark(B);
		FreeBookmark(B);
	end;
	ActualizarPago(nil);
	if indice < cb_DebitoAutomatico.Items.Count then cb_DebitoAutomatico.ItemIndex := Indice;

end;

procedure TFormMantenimientoCuenta.ActualizarPago(Sender: TObject);
Var
	CodPlan: Integer;
begin
	//if not chk_contag.checked then rb_prepago.Checked := true;
	rb_pospagomanual.Enabled := chk_contag.Checked;
	rb_pospagoautomatico.Enabled :=
	  chk_contag.Checked and (cb_debitoautomatico.Items.Count > 0);
	cb_debitoautomatico.Enabled := rb_pospagoautomatico.Checked;
	(*if rb_pospagoautomatico.Checked and not rb_pospagoautomatico.Enabled then begin
		if rb_pospagomanual.Enabled then rb_pospagomanual.Checked := True
		  else rb_prepago.Checked := True;
	end;*)

	// Actualizamos los planes comerciales
	CodPlan := IVal(StrRight(cb_PlanComercial.Text, 10));
	CargarPlanesComercialesPosibles(DMConnections.BaseCAC, cb_PlanComercial,
	  IVal(StrRight(cb_TipoCliente.Text, 20)), iif(chk_contag.Checked, 1, 2),
	  CodPlan);
	if (cb_PlanComercial.ItemIndex = -1) and (cb_PlanComercial.Items.Count = 1)
	  then cb_PlanComercial.ItemIndex := 0;
	cb_PlanComercial.OnClick(cb_PlanComercial);
	// Actualizamos los contratos
	//CargarContratos; //*** PENDIENTE!!!
	// Actualizamos las Agrupaciones de cuentas
	CargarGruposCuentas;
	// Mostramos el tab de Facturaci�n?
	//tab_Facturacion.TabVisible := not rb_prepago.Checked;
    //tab_DebitosAutomaticos.Visible := true;
end;

procedure TFormMantenimientoCuenta.CargarContratos;
var
	CodPlan: Integer;
begin
	Contratos.Close;
	Contratos.CreateDataSet;
	if FCodigoCuenta > 0 then begin
		// Cuenta Existente. Cargamos primero los contratos ya hechos.
		qry_ContratosHechos.Parameters.ParamByName('CodigoCuenta').Value := FCodigoCuenta;
		qry_ContratosHechos.Open;
		With qry_ContratosHechos do While not Eof do begin
			Contratos.Append;
			Contratos.FieldByName('NumeroContrato').Assign(qry_ContratosHechos.FieldByName('NumeroContrato'));
			Contratos.FieldByName('PlanComercial').Assign(qry_ContratosHechos.FieldByName('PlanComercial'));
			Contratos.FieldByName('Version').Assign(qry_ContratosHechos.FieldByName('Version'));
			Contratos.FieldByName('Descripcion').Assign(qry_ContratosHechos.FieldByName('Descripcion'));
			Contratos.Post;
			Next;
		end;
		qry_ContratosHechos.Close;
	end;
	// Si no tenemos un contrato para el plan actual, lo agregamos
	CodPlan := IVal(StrRight(cb_PlanComercial.Text, 20));
	if (CodPlan > 0) and not Contratos.Locate('PlanComercial', CodPlan, []) then begin
		ObtenerContratoPlan.Parameters.ParamByName('@PlanComercial').Value := CodPlan;
		ObtenerContratoPlan.Open;
		if not ObtenerContratoPlan.Eof then begin
			Contratos.Append;
			Contratos.FieldByName('PlanComercial').Assign(ObtenerContratoPlan.FieldByName('PlanComercial'));
			Contratos.FieldByName('Version').Assign(ObtenerContratoPlan.FieldByName('Version'));
			Contratos.FieldByName('Descripcion').Assign(ObtenerContratoPlan.FieldByName('Descripcion'));
			Contratos.Post;
		end;
		ObtenerContratoPlan.Close;
	end;
	// Listo
	Contratos.First;
	ListaContratos.Reload;
end;

procedure TFormMantenimientoCuenta.ListaContratosDrawItem(Sender: TDBList;
  Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState;
  Cols: TColPositions);
resourcestring
    MSG_CONTRATO_NUEVO = 'Nuevo';
begin
	With Sender.Canvas, Tabla do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top + 1, iif(FieldByName('NumeroContrato').IsNull,
          MSG_CONTRATO_NUEVO, FieldByName('NumeroContrato').AsString));
		TextOut(Cols[1], Rect.Top + 1, FieldByName('Descripcion').AsString);
		TextOut(Cols[3], Rect.Top + 1, FieldByName('FechaImpresion').AsString);
		TextOut(Cols[4], Rect.Top + 1, FieldByName('FechaFirma').AsString);
	end;
end;

procedure TFormMantenimientoCuenta.CambioAgrupacion(Sender: TObject);
begin
	ListaGrupoCuentas.Color := iif(rb_agrupar.Checked, clWindow, clBtnFace);
	ListaGrupoCuentas.Enabled := rb_agrupar.Checked;
	cb_GrupoFacturacion.Color := iif(rb_agrupar.Checked, clBtnFace, clWindow);
	cb_GrupoFacturacion.Enabled := rb_separada.Checked;
//	btn_editargruposcuentas.Enabled := ListaGrupoCuentas.Enabled;
end;

procedure TFormMantenimientoCuenta.CargarGruposCuentas;
begin
	qry_GruposCuentas.Close;
	qry_GruposCuentas.Parameters.ParamByName('CodigoPersona').Value := FCodigoPersona;
	qry_GruposCuentas.Parameters.ParamByName('CodigoCuenta').Value := FCodigoCuenta;
	(* No hay prepago por ahora
    qry_GruposCuentas.Parameters.ParamByName('TipoPago').Value := iif(rb_prepago.Checked, TP_PREPAGO, iif(rb_pospagomanual.Checked, TP_POSPAGO_MANUAL, TP_DEBITO_AUTOMATICO));
    *)
	qry_GruposCuentas.Parameters.ParamByName('TipoPago').Value := iif(rb_pospagomanual.Checked, TP_POSPAGO_MANUAL, TP_DEBITO_AUTOMATICO);
//	qry_GruposCuentas.Parameters.ParamByName('CodigoDebitoAutomatico').Value := iif(rb_pospagoautomatico.Checked, IVal(StrRight(cb_debitoautomatico.Text, 10)), 0);
	qry_GruposCuentas.Parameters.ParamByName('CodigoDebitoAutomatico').Value := iif(rb_pospagoautomatico.Checked, cb_debitoautomatico.ItemIndex + 1, 0);
	qry_GruposCuentas.Open;
	ListaGrupoCuentas.Enabled := True;
	ListaGrupoCuentas.Reload;
    qry_GruposCuentas.First;
	rb_agrupar.Enabled := not qry_GruposCuentas.Eof;
	if not rb_agrupar.Enabled then rb_separada.Checked := true;
	rb_separada.OnClick(rb_separada);
	ListaGrupoCuentas.Reload;
end;

procedure TFormMantenimientoCuenta.ListaGrupoCuentasDrawItem(
  Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState;
  Cols: TColPositions);
begin
	With Sender.Canvas, Tabla do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top + 1, FieldByName('Cuentas').AsString);
	end;
end;

procedure TFormMantenimientoCuenta.ListaContratosRefresh(Sender: TObject);
begin
	btn_firmar.Enabled := not ListaContratos.Empty
	  and Contratos.FieldByName('FechaFirma').IsNull
	  and not Contratos.FieldByName('NumeroContrato').IsNull
	  and False; //hola
	btn_imprimir.Enabled := not ListaContratos.Empty
	  and Contratos.FieldByName('FechaImpresion').IsNull
	  and not Contratos.FieldByName('NumeroContrato').IsNull
	  and False; //hola
	btn_reimprimir.Enabled := not ListaContratos.Empty
	  and not Contratos.FieldByName('FechaImpresion').IsNull
	  and False; //hola
end;

procedure TFormMantenimientoCuenta.ListaDebitosRefresh(Sender: TObject);

	function ChequearDependencias: Boolean;
	begin
		Result := True;
	end;

begin
	btn_modificar.Enabled := not ListaDebitos.Empty;
	//btn_quitar.Enabled := False;//not ListaDebitos.Empty and ChequearDependencias;
	btn_quitar.Enabled := not ListaDebitos.Empty;

	//Si esta vacio, no puede ser pospago debito automatico
	if rb_pospagoautomatico.Checked then
		rb_pospagomanual.Checked := ListaDebitos.Empty;
end;

procedure TFormMantenimientoCuenta.GuardarDatos;
	Function ValidarPatente: Boolean;
	begin
		Result := QueryGetValueInt(DMConnections.BaseCAC, Format(
			'SELECT 1 FROM Cuentas (INDEX = IX_Patente) WHERE Patente = ''%s'' ' +
			'AND FechaBaja IS NULL AND CodigoCuenta <> %d ',
		    [Trim(mcPatente.MaskText), FCodigoCuenta])) <> 1;
	end;

resourcestring
    // Mensajes de verificacion
    CAPTION_VALIDAR_CLIENTE  = 'Validar los datos del Cliente';
	MSG_NOMBRE          = 'Debe indicar un nombre';
    MSG_APELLIDO        = 'Debe indicar un apellido';
    MSG_DOCUMENTO       = 'Debe indicar un documento';
    MSG_NUMERODOCUMENTO = 'El documento especificado es incorrecto.';
    MSG_PAIS            = 'Debe indicar el pa�s de residencia';

	MSG_FECHANACIMIENTO = 'La fecha de nacimiento es incorrecta, o el cliente es menor de edad';
	MSG_MAIL            = 'La direcci�n de mail es inv�lida';
	CAPTION_VALIDAR_CUENTA = 'Validar los datos de la Cuenta';
    MSG_PATENTE         = 'Debe indicar una patente';
    MSG_FORMATOPATENTE  = 'La patente especificada es incorrecta.';
    MSG_EXISTE_VEHICULO = 'Ya existen una cuenta activa con la patente %s';
	MSG_CATEGORIA       = 'Debe indicar una categor�a';
    MSG_MARCA           = 'Debe indicar una marca.';
    MSG_MODELO          = 'Debe indicar un modelo.';
	MSG_ANIO            = 'El a�o es incorrecto. ' + #13#10 +
                          'Debe ser mayor a 1900 y menor al a�o siguiente al actual. ';
    MSG_COLOR           = 'Debe indicar un color.';
    MSG_DEBITO_AUTOMATICO = 'Debe indicar una cuenta de d�bito autom�tico.';
    MSG_PLAN_COMERCIAL  = 'Debe indicar un plan comercial.';
    MSG_GRUPO_FACTURACION = 'Debe indicar un grupo de facturaci�n.';
    // Mensajes de informaci�n
    CAPTION_PIN         = 'PIN Asignado';
    MSG_PIN             = 'El siguiente es el PIN que deber� utilizar el cliente para ' +
      'acceder a todos los servicios en forma telef�nica o v�a Web: ';
	MSG_DOMICILIO_DOCUMENTOS_INCOMPLETO = 'Ingres� el domicilio de documentos en forma incompleta. '+ CRLF +
                                          'Por favor revise el n�mero, la comuna y la regi�n.';
    MSG_DOMICILIO_NINGUNO = 'No ingres� ninguno de los domicilios. '+ CRLF +
							'Por favor complete los datos.';
    MSG_DOMICILIO_NINGUNO_ENTREGA = 'No seleccion� ninguno de los domicilios para que sea de entrega. ';
	MSG_DOMICILIO_ENTREGA_LIMPIO = 'Seleccion� un domicilio de entrega que tiene los datos incompletos. ';
    MSG_FALTA_MEDIO_CONTACTO    = 'Debe ingresar por lo menos un medio de comunicaci�n.';
	MSG_ACTUALIZACION           = 'Los datos se actualizaron correctamente.';
    MSG_ELIMINAR_DOMICILIO      =   'No se puede eliminar el domicilio. Puede que est� indicado para entrega de ' + #13#10 +
                                    'facturas y/o informaci�n comercial';
    MSG_ERROR_DOMICILIO_ENTREGA_BORRAR = 'Eligi� un domicilio de entrega que est� marcado para ser borrado.';
Var
	PatenteOk, GrupoFact, AgrupCuentas: Variant;
	DocCorrecto: Boolean;
    comunaDocumentosAux,
	regionDocumentosAux: AnsiString;
    MensajeAltaCliente: boolean;
begin
	// Validamos el documento del cliente
    DocCorrecto := cbTipoNumeroDocumento.ValidateMask;
    regionDocumentosAux := '';
    comunaDocumentosAux := '';

	// Validamos los datos del cliente
	if not ValidateControls(
	  [txt_nombre,
	  txt_apellido,
	  cbTipoNumeroDocumento,
  	  cbTipoNumeroDocumento,
	  cb_pais,
	  txt_fechanacimiento],
	  [Trim(txt_nombre.text) <> '',
	  Trim(txt_apellido.text) <> '',
	  Trim(cbTipoNumeroDocumento.MaskText) <> '',
      (Length(cbTipoNumeroDocumento.MaskText) <= 11) and DocCorrecto,
	  cb_pais.itemindex >= 0,
	  (txt_fechanacimiento.Text = '  /  /    ') or FechaNacimientoValida(txt_fechanacimiento.Date)],
	  CAPTION_VALIDAR_CLIENTE,
	  [MSG_NOMBRE,
		MSG_APELLIDO,
        MSG_DOCUMENTO,
		MSG_NUMERODOCUMENTO,
		MSG_PAIS,
        MSG_FECHANACIMIENTO]
	  ) then Exit;

    //Si no carg� ninguno de los dos domicilios, reboto!
    if (DomicilioParticular.IndiceDomicilio <> 1) and
       (DomicilioComercial.IndiceDomicilio <> 1) then begin
        MsgBox(MSG_DOMICILIO_NINGUNO, CAPTION_VALIDAR_CLIENTE, MB_ICONSTOP);
        Exit;
    end;

    //Si marc� el domicilio particular para entrega, pero no lo carg�, le aviso
    // y si esta marcado, pero tambien marcado para borrar, reboto!
    if (EstaElegidoComoDomicilioEntrega(TIPO_ORIGEN_DATO_PARTICULAR)) then begin
        if (DomicilioParticular.IndiceDomicilio <> 1) then begin
            MsgBox(MSG_DOMICILIO_ENTREGA_LIMPIO, CAPTION_VALIDAR_CLIENTE, MB_ICONSTOP);
            exit;
        end
        else begin
            if (ChkEliminarDomicilioParticular.Checked) then begin
                MsgBox(MSG_ERROR_DOMICILIO_ENTREGA_BORRAR, CAPTION_VALIDAR_CLIENTE, MB_ICONSTOP);
                exit;
            end;
        end;
    end;

    //Si marc� el domicilio comercial para entrega, pero no lo carg�, le aviso
    // y si esta marcado, pero tambien marcado para borrar, reboto!    
    if (EstaElegidoComoDomicilioEntrega(TIPO_ORIGEN_DATO_COMERCIAL)) then begin
        if (DomicilioComercial.IndiceDomicilio <> 1) then begin
            MsgBox(MSG_DOMICILIO_ENTREGA_LIMPIO, CAPTION_VALIDAR_CLIENTE, MB_ICONSTOP);
            exit;
        end
        else begin
            if (ChkEliminarDomicilioComercial.Checked) then begin
                MsgBox(MSG_ERROR_DOMICILIO_ENTREGA_BORRAR, CAPTION_VALIDAR_CLIENTE, MB_ICONSTOP);
                exit;
            end;
        end;
    end;

    //Si no marc� ninguno, me fijo cual ingres� y lo marco.
    if (not EstaElegidoComoDomicilioEntrega(TIPO_ORIGEN_DATO_PARTICULAR)) and
       (not EstaElegidoComoDomicilioEntrega(TIPO_ORIGEN_DATO_COMERCIAL)) then begin
        if StrRight(Trim(cbPersoneria.Text), 1) = PERSONERIA_FISICA then begin
            //Si es persona f�sica y cargo el domicilio particular lo marco...
            if (DomicilioParticular.IndiceDomicilio = 1) then
                MarcarComoDomicilioEntrega(TIPO_ORIGEN_DATO_PARTICULAR)
            else
                MarcarComoDomicilioEntrega(TIPO_ORIGEN_DATO_COMERCIAL);
        end
        else begin //Es Persona Jur�dica
            //Si es persona f�sica y cargo el domicilio comercial lo marco...
            if (DomicilioComercial.IndiceDomicilio = 1) then
                MarcarComoDomicilioEntrega(TIPO_ORIGEN_DATO_COMERCIAL)
            else
                MarcarComoDomicilioEntrega(TIPO_ORIGEN_DATO_PARTICULAR);
        end;
    end;

	//Debe ingresar por lo menos un medio de comunicaci�n.
    if ((trim(txtTelefonoParticular.Text) = '') and
        (trim(txtTelefonoComercial.Text) = '') and
		(trim(txtTelefonoMovil.Text) = ''))  then begin
        MsgBox(MSG_FALTA_MEDIO_CONTACTO, CAPTION_VALIDAR_CLIENTE, MB_ICONSTOP);
        //PageControlDomicilios.ActivePageIndex := 1;
        exit;
    end;

	// Validamos los datos del veh�culo
	PatenteOk := mcPatente.ValidateMask;
	if not ValidateControls(
	  [mcPatente,
	  mcPatente,
      mcPatente,
	  cb_marca,
	  cb_modelo,
      anio,
	  cb_PlanComercial],
	  [Trim(mcPatente.MaskText) <> '',
	  ValidarPatente,
      PatenteOk,
	  cb_marca.itemindex >= 0,
	  cb_modelo.itemindex >= 0,
      EsAnioCorrecto(anio.ValueInt),
	  cb_PlanComercial.ItemIndex >= 0],
	  CAPTION_VALIDAR_CUENTA,
	  [MSG_PATENTE,
	  Format(MSG_EXISTE_VEHICULO, [Trim(mcPatente.MaskText)]),
      MSG_FORMATOPATENTE,
      MSG_MARCA,
	  MSG_MODELO,
      MSG_ANIO,
      MSG_PLAN_COMERCIAL]

	  ) then Exit;

	// Validamos las formas de pago
	if rb_pospagoautomatico.checked and not ValidateControls(
	  [cb_debitoautomatico, cb_PlanComercial],
	  [cb_debitoautomatico.ItemIndex >= 0, cb_PlanComercial.ItemIndex >= 0],
	  CAPTION_VALIDAR_CUENTA, [MSG_DEBITO_AUTOMATICO, MSG_PLAN_COMERCIAL]) then Exit;

	// Validamos la agrupaci�n de cuentas
	if rb_separada.checked and not ValidateControls(
	  [cb_GrupoFacturacion],
	  [cb_GrupoFacturacion.ItemIndex >= 0],
	  CAPTION_VALIDAR_CUENTA, [MSG_GRUPO_FACTURACION]) then Exit;
    {
	if chkDomicilioEntregaParticular.Checked then
		TipoOrigenDatoDomicilioEntrega := TIPO_ORIGEN_DATO_PARTICULAR;
    if chkDomicilioEntregaComercial.Checked then
		TipoOrigenDatoDomicilioEntrega := TIPO_ORIGEN_DATO_COMERCIAL;
    }
	ActualizarDatosPersona.Close;
	With ActualizarDatosPersona.Parameters do begin
		ParamByName('@Personeria').Value        := iif( cbPersoneria.ItemIndex < 0, null,  Trim(StrRight( cbPersoneria.Text, 1)));
		ParamByName('@Nombre').Value			:= iif(Trim(txt_Nombre.Text)= '', null, Trim(txt_Nombre.Text));
		ParamByName('@Apellido').Value			:= iif(Trim(txt_Apellido.Text)= '', null, Trim(txt_Apellido.Text));

		if StrRight( Trim(cbPersoneria.Text), 1) = PERSONERIA_FISICA then begin
			ParamByName('@ApellidoMaterno').Value	    := iif(Trim(txt_ApellidoMaterno.Text)= '', null, Trim(txt_ApellidoMaterno.Text));
			ParamByName('@Sexo').Value 				    := iif( cb_Sexo.ItemIndex < 0, null, Trim(cb_Sexo.Text)[1]);
			ParamByName('@ContactoComercial').Value	    := null;
		end else if StrRight( Trim(cbPersoneria.Text), 1) = PERSONERIA_JURIDICA then begin
			ParamByName('@ApellidoMaterno').Value	    := null;
			ParamByName('@Sexo').Value 				    := null;
			ParamByName('@ContactoComercial').Value	    := iif(Trim(txt_ApellidoMaterno.Text)= '', null, Trim(txt_ApellidoMaterno.Text));
		end;

		ParamByName('@CodigoDocumento').Value	    := Trim(StrRight(cbTipoNumeroDocumento.ComboText, 10));
		ParamByName('@NumeroDocumento').Value	    := iif(Trim(cbTipoNumeroDocumento.MaskText)= '', null, Trim(cbTipoNumeroDocumento.MaskText));

		ParamByName('@LugarNacimiento').Value 	    := iif( cb_pais.ItemIndex < 0, null,  Trim( StrRight(cb_pais.Text, 10)));
		ParamByName('@FechaNacimiento').Value 	    := iif( txt_fechanacimiento.date < 0, null, txt_fechanacimiento.Date);
		ParamByName('@CodigoSituacionIVA').Value    := iif( cbSituacionIVA.ItemIndex < 0, null,  Trim(StrRight( cbSituacionIVA.Text, 2)));
		ParamByName('@CodigoActividad').Value 	    := iif( cb_Actividad.ItemIndex < 0, null,  Ival(StrRight( Trim(cb_Actividad.Text), 10)));

		//***chequear
		ParamByName('@PIN').Value                   := Trim(FPin);

		ParamByName('@CodigoPersona').Value 	    := FCodigoPersona;
		ParamByName('@CodigoUsuario').Value 	    := UsuarioSistema;              //SS_1419_NDR_20151130

	end;
	ActualizarDatosPersona.ExecProc;
	FCodigoPersona := ActualizarDatosPersona.Parameters.ParamByName('@CodigoPersona').Value;
	MensajeAltaCliente := false;
    //Si la persona no exist�a como cliente, le informo el PIN
    MensajeAltaCliente := QueryGetValueInt(DMConnections.BaseCAC, Format(
		'SELECT 1 FROM Clientes  WHERE CodigoPersona = %d ', [FCodigoPersona])) <> 1;

	//Actualizo la tabla clientes
    with ActualizarDatosCliente do
    begin
		Close;
        Parameters.ParamByName('@CodigoPersona').Value := FCodigoPersona;
        Parameters.ParamByName('@Estado').Value := ESTADO_PERSONA_ACTIVO;
        ExecProc;
    end;

    //Actualizo los domicilios
    //Ojo, solo pregunto si ingres� calle, numero y comuna porque
    //el pa�s y la regi�n ya est�n marcadas (Chile y Santiago) siempre
    //(adem�s si ingres� la comuna, obvio que ingres� pa�s y regi�n)
	with ActualizarDomicilio do
    begin
        //Particular
        Parameters.Refresh;
        Parameters.ParamByName('@CodigoPersona').Value         := FCodigoPersona;
        Parameters.ParamByName('@CodigoTipoOrigenDato').Value  := TIPO_ORIGEN_DATO_PARTICULAR;
        Parameters.ParamByName('@CodigoCalle').Value           := iif( DomicilioParticular.CodigoCalle < 1, null, DomicilioParticular.CodigoCalle);
        Parameters.ParamByName('@Numero').Value                := iif( DomicilioParticular.NumeroCalle < 1, null, DomicilioParticular.NumeroCalle);
        Parameters.ParamByName('@Piso').Value                  := iif( DomicilioParticular.Piso < 1, null, DomicilioParticular.Piso);
        Parameters.ParamByName('@Depto').Value                  := iif(DomicilioParticular.Dpto = '', null,  Trim(DomicilioParticular.Dpto));
        Parameters.ParamByName('@Detalle').Value               := iif(DomicilioParticular.Detalle = '', null,  Trim(DomicilioParticular.Detalle));
        Parameters.ParamByName('@CalleDesnormalizada').Value   := iif(DomicilioParticular.CodigoCalle >= 1, null,  Trim(DomicilioParticular.CalleDesnormalizada));
        Parameters.ParamByName('@CodigoPostal').Value          := iif(DomicilioParticular.CodigoPostal = '', null,  Trim(DomicilioParticular.CodigoPostal));
        Parameters.ParamByName('@Activo').Value                := DomicilioParticular.Activo;
        Parameters.ParamByName('@CodigoDomicilio').Value       := iif(CodigoDomicilioParticular < 1, null, CodigoDomicilioParticular);
        ExecProc;
        CodigoDomicilioParticular :=  Parameters.ParamByName('@CodigoDomicilio').Value;
        if (DomicilioParticular.CodigoCalle < 1) then begin
            with ActualizarDomicilioRelacionado do
            begin
                Parameters.Refresh;
                if (DomicilioParticular.CodigoCalleRelacionadaUno > 0) then begin
                    Parameters.ParamByName('@CodigoDomicilio').Value := CodigoDomicilioParticular;
                    Parameters.ParamByName('@CodigoCalle').Value := DomicilioParticular.CodigoCalleRelacionadaUno;
                    Parameters.ParamByName('@Numero').Value := DomicilioParticular.NumeroCalleRelacionadaUno;
                    ExecProc;
                end;
                Close;
                if (DomicilioParticular.CodigoCalleRelacionadaDos > 0) then begin
                    Parameters.ParamByName('@CodigoDomicilio').Value := CodigoDomicilioParticular;
                    Parameters.ParamByName('@CodigoCalle').Value := DomicilioParticular.CodigoCalleRelacionadaDos;
                    Parameters.ParamByName('@Numero').Value := DomicilioParticular.NumeroCalleRelacionadaDos;
                    ExecProc;
                end;
            end;
        end;
        //Si no marco ninguno de los dos domicilios y carga uno, lo marco como entrega
        if EstaElegidoComoDomicilioEntrega(TIPO_ORIGEN_DATO_PARTICULAR) then begin
            CodigoDomicilioEntrega := CodigoDomicilioParticular;
        end
        else
            if (not EstaElegidoComoDomicilioEntrega(TIPO_ORIGEN_DATO_COMERCIAL)) and
               (not EstaElegidoComoDomicilioEntrega(TIPO_ORIGEN_DATO_PARTICULAR)) then begin
                MarcarComoDomicilioEntrega(TIPO_ORIGEN_DATO_PARTICULAR);
                CodigoDomicilioEntrega := CodigoDomicilioParticular;
            end;

        if (DomicilioComercial.IndiceDomicilio = 1) then begin
            Close;
            with ActualizarDomicilio.Parameters do begin
                ParamByName('@CodigoPersona').Value         := FCodigoPersona;
                ParamByName('@CodigoTipoOrigenDato').Value  := TIPO_ORIGEN_DATO_COMERCIAL;
                ParamByName('@CodigoCalle').Value           := iif( DomicilioComercial.CodigoCalle < 1, null, DomicilioComercial.CodigoCalle);
                ParamByName('@Numero').Value                := iif( DomicilioComercial.NumeroCalle < 1, null, DomicilioComercial.NumeroCalle);
                ParamByName('@Piso').Value                  := iif( DomicilioComercial.Piso < 1, null, DomicilioComercial.Piso);
                ParamByName('@Depto').Value                  := iif(DomicilioComercial.Dpto = '', null,  Trim(DomicilioComercial.Dpto));
                ParamByName('@Detalle').Value               := iif(DomicilioComercial.Detalle = '', null,  Trim(DomicilioComercial.Detalle));
                ParamByName('@CalleDesnormalizada').Value   := iif(DomicilioComercial.CodigoCalle >= 1, null,  Trim(DomicilioComercial.CalleDesnormalizada));
                ParamByName('@CodigoPostal').Value          := iif(DomicilioComercial.CodigoPostal = '', null,  Trim(DomicilioComercial.CodigoPostal));
                ParamByName('@Activo').Value                := DomicilioComercial.Activo;
                ParamByName('@CodigoDomicilio').Value       := iif(CodigoDomicilioComercial < 1, null, CodigoDomicilioComercial);
            end;
            ExecProc;
            CodigoDomicilioComercial :=  ActualizarDomicilio.Parameters.ParamByName('@CodigoDomicilio').Value;
            if (DomicilioComercial.CodigoCalle < 1) then begin
                with ActualizarDomicilioRelacionado do
                begin
                    Parameters.Refresh;
                    if (DomicilioComercial.CodigoCalleRelacionadaUno > 0) then begin
                        Parameters.ParamByName('@CodigoDomicilio').Value := CodigoDomicilioComercial;
                        Parameters.ParamByName('@CodigoCalle').Value := DomicilioComercial.CodigoCalleRelacionadaUno;
                        Parameters.ParamByName('@Numero').Value := DomicilioComercial.NumeroCalleRelacionadaUno;
                        ExecProc;
                    end;
                    Close;
                    if (DomicilioComercial.CodigoCalleRelacionadaDos > 0) then begin
                        Parameters.ParamByName('@CodigoDomicilio').Value := CodigoDomicilioComercial;
                        Parameters.ParamByName('@CodigoCalle').Value := DomicilioComercial.CodigoCalleRelacionadaDos;
                        Parameters.ParamByName('@Numero').Value := DomicilioComercial.NumeroCalleRelacionadaDos;
                        ExecProc;
                    end;
                end;
            end;

            //Si no marco ninguno de los dos domicilios y carga uno, lo marco como entrega
            if EstaElegidoComoDomicilioEntrega(TIPO_ORIGEN_DATO_COMERCIAL) then begin
                CodigoDomicilioEntrega := CodigoDomicilioComercial;
            end
            else
                if (not EstaElegidoComoDomicilioEntrega(TIPO_ORIGEN_DATO_COMERCIAL)) and
                   (not EstaElegidoComoDomicilioEntrega(TIPO_ORIGEN_DATO_PARTICULAR)) then begin
                    MarcarComoDomicilioEntrega(TIPO_ORIGEN_DATO_COMERCIAL);
                    CodigoDomicilioEntrega := CodigoDomicilioComercial;
                end;
        end;
    end;
    
    //Actualizo en los datos de la persona el Codigo de Domicilio de Entrega.
    if (EstaElegidoComoDomicilioEntrega(TIPO_ORIGEN_DATO_PARTICULAR)) or
       (EstaElegidoComoDomicilioEntrega(TIPO_ORIGEN_DATO_COMERCIAL)) then begin
        with ActualizarDomicilioEntregaPersona do begin
            Close;
			Parameters.ParamByName('@CodigoPersona').Value             := FCodigoPersona;
            Parameters.ParamByName('@CodigoDomicilioEntrega').Value    := CodigoDomicilioEntrega;
            ExecProc;
			Close;
		end;
    end;


	//Eliminar los domicilios
    if (chkEliminarDomicilioParticular.Checked) and
       (CodigoDomicilioParticular > 0) then begin
        with EliminarDomicilioPersona do
        begin
            Close;
			Parameters.ParamByName('@CodigoDomicilio').Value := CodigoDomicilioParticular;
            try
                ExecProc;
            except
                on E: exception do
                    raise exception.Create(MSG_ELIMINAR_DOMICILIO);
            end;
        end;
    end;

	if (chkEliminarDomicilioComercial.Checked) and
       (CodigoDomicilioComercial > 0) then begin
        with EliminarDomicilioPersona do
		begin
            Close;
            Parameters.ParamByName('@CodigoDomicilio').Value := CodigoDomicilioComercial;
            try
                ExecProc;
            except
                on E: exception do
                    raise exception.Create(MSG_ELIMINAR_DOMICILIO);
            end;
        end;
    end;
    
    GuardarMediosComunicacionContacto(FCodigoPersona);

	// Actualizamos los d�bitos autom�ticos
	With DebitosAutomaticos do begin
		First;
		While not Eof do begin
			ActualizarDebitoAutomatico.Parameters.ParamByName('@CodigoPersona').Value :=
			  FCodigoPersona;
			ActualizarDebitoAutomatico.Parameters.ParamByName('@CodigoDebitoAutomatico').Value :=
			  FieldByName('CodigoDebitoAutomatico').AsInteger;
			ActualizarDebitoAutomatico.Parameters.ParamByName('@TipoDebito').Value :=
			  FieldByName('TipoDebito').AsString;
			ActualizarDebitoAutomatico.Parameters.ParamByName('@CodigoEntidad').Value :=
			  FieldByName('CodigoEntidad').AsString;
			ActualizarDebitoAutomatico.Parameters.ParamByName('@CuentaDebito').Value :=
			  FieldByName('CuentaDebito').AsString;
			ActualizarDebitoAutomatico.Parameters.ParamByName('@Vencimiento').Value :=
			  iif(FieldByName('Vencimiento').IsNull, NULL, FieldByName('Vencimiento').AsDateTime);
			ActualizarDebitoAutomatico.Parameters.ParamByName('@FechaCierre').Value := NULL;
			ActualizarDebitoAutomatico.ExecProc;
			// Si es un registro nuevo, lo actualizamos en nuestra tabla temporaria
			if FieldByName('CodigoDebitoAutomatico').AsInteger = 0 then begin
				Edit;
				FieldByName('CodigoDebitoAutomatico').AsInteger :=
				  ActualizarDebitoAutomatico.Parameters.ParamByName('@CodigoDebitoAutomatico').Value;
				Post;
			end;
			Next;
		end;
	end;

    //Borrar los Debitos Automaticos que corresponda
	With DebitosAutomaticosBorrados do begin
		First;
		While not Eof do begin
			qry_DebitosAutomaticosBorrar.Parameters.ParamByName('@CodigoPersona').Value := FCodigoPersona;
			qry_DebitosAutomaticosBorrar.Parameters.ParamByName('@CodigoDebitoAutomatico').Value :=
			  FieldByName('CodigoDebitoAutomatico').AsInteger;
			qry_DebitosAutomaticosBorrar.ExecSQL;
			Next;
		end;
	end;

	// Guardamos los datos de la cuenta
	With ActualizarCuentaCliente.Parameters do begin
		// Paramos la tabla de d�bitos en el d�bito seleccionado
		DebitosAutomaticos.First;
		DebitosAutomaticos.MoveBy(cb_debitoautomatico.Itemindex);
		// Cargamos los par�metros
		ParamByName('@CodigoPersona').Value := FCodigoPersona;
		ParamByName('@CodigoDebitoAutomatico').Value := iif(rb_pospagoautomatico.Checked,
		        DebitosAutomaticos.FieldByName('CodigoDebitoAutomatico').AsInteger, NULL);
        ParamByName('@TipoPatente').Value := trim(strRight(mcPatente.ComboText, 20));
		ParamByName('@Patente').Value := mcPatente.MaskText;
		ParamByName('@CodigoMarca').Value := IVal(StrRight(cb_Marca.text, 20));
		ParamByName('@CodigoModelo').Value := IVal(StrRight(cb_Modelo.text, 20));
        ParamByName('@Categoria').Value := txt_codigoCategoria.ValueInt;
		ParamByName('@AnioVehiculo').Value := anio.ValueInt;
        ParamByName('@CodigoColor').Value := IIf(cb_color.itemindex >= 0,IVal(StrRight(cb_color.Text, 20)),null);
        ParamByName('@TieneAcoplado').Value := chk_acoplado.Checked;
        if chk_acoplado.Checked then
            ParamByName('@LargoAdicionalVehiculo').Value := LargoAdicionalVehiculo.ValueInt
		else
			ParamByName('@LargoAdicionalVehiculo').Value := null;
		ParamByName('@TipoAdhesion').Value := iif(chk_contag.Checked, TA_CON_TAG, TA_SIN_TAG);
		ParamByName('@TipoCliente').Value := IVal(StrRight(cb_tipocliente.text, 20));
		ParamByName('@PlanComercial').Value := IVal(StrRight(cb_plancomercial.text, 20));
		ParamByName('@DetallePasadas').Value := chk_detallepasadas.Checked;
		ParamByName('@EnvioFacturas').Value := chk_enviofacturas.Checked;
		// Otros
		ParamByName('@FechaBaja').Value := NULL;
		(*ParamByName('@TipoPago').Value := iif(rb_prepago.Checked, TP_PREPAGO,
				iif(rb_pospagomanual.Checked, TP_POSPAGO_MANUAL, TP_DEBITO_AUTOMATICO));*)
        ParamByName('@TipoPago').Value := iif(rb_pospagomanual.Checked, TP_POSPAGO_MANUAL, TP_DEBITO_AUTOMATICO);
		ParamByName('@EstadoCuenta').Value := 'N';

        //***PENDIENTE Corregir esto..
        ParamByName('@CodigoDomicilioFacturacion').Value := CodigoDomicilioEntrega;
        ParamByName('@CodigoDomicilioInfoComercial').Value := CodigoDomicilioEntrega;
		ParamByName('@CodigoCuenta').Value := FCodigoCuenta;

		// Agrupaci�n de Cuentas
		(*if rb_prepago.Checked then begin
			// A los prepagos no se les factura
			AgrupCuentas := NULL;
			GrupoFact := NULL;
		end else*)
            if rb_separada.Checked then begin
                // Facturar la cuenta individualmente
                AgrupCuentas := NULL;
				GrupoFact := IVal(StrRight(cb_GrupoFacturacion.Text, 20));
			end else begin
				// Facturar con otro grupo
				AgrupCuentas := qry_GruposCuentas.FieldByName('AgrupacionCuentas').AsInteger;
                GrupoFact := NULL;
            end;

		ParamByName('@AgrupacionCuentas').Value := AgrupCuentas;
		ParamByName('@CodigoGrupoFacturacion').Value := GrupoFact;
		// Ejecutamos
		ActualizarCuentaCliente.ExecProc;
		if FCodigoCuenta = 0 then FCodigoCuenta := ParamByName('@CodigoCuenta').Value;

        if FCodigoOrdenServicio > 0 then begin
        	QueryExecute(DMConnections.BaseCAC,
            	'UPDATE OrdenesServicioAdhesion SET CodigoCuenta = ' +  IntToStr(FCodigoCuenta)+
                ' WHERE CodigoOrdenServicio = ' + IntToStr(FCodigoOrdenServicio));

        end;
	end;

	// Actualizo Roles, poniendo la Persona como Titular de la Cuenta
    {
	With ActualizarRolesCuentas, Parameters Do Begin
		ParamByName ('@CodigoRol').Value 	 := 1;
		ParamByName ('@CodigoCuenta').Value  := FCodigoCuenta;
		ParamByName ('@CodigoPersona').Value := FCodigoPersona;
		ExecProc;
	End;
    }
	//*** PENDIENTE
	// Guardamos los contratos nuevos
	{
	Contratos.First;
	While not Contratos.Eof do begin
		if Contratos.FieldByName('NumeroContrato').AsInteger = 0 then begin
			CrearContrato.Parameters.ParamByName('@NumeroContrato').Value := NULL;
			CrearContrato.Parameters.ParamByName('@CodigoCuenta').Value := FCodigoCuenta;
			CrearContrato.Parameters.ParamByName('@PlanComercial').Value := IVal(StrRight(cb_plancomercial.text, 20));
			CrearContrato.ExecProc;
		end;
		Contratos.Next;
	end;
    }
    if MensajeAltaCliente then
        MsgBox( MSG_ACTUALIZACION + CRLF + MSG_PIN + Trim(FPIN), self.Caption, MB_ICONINFORMATION);

	// Listo
	ModalResult := mrOk;
end;

procedure TFormMantenimientoCuenta.chk_detallepasadasClick(Sender: TObject);
begin
	if chk_detallepasadas.Checked then begin
		chk_enviofacturas.Checked := True;
		chk_enviofacturas.Enabled := False;
	end else begin
		chk_enviofacturas.Enabled := True;
	end;
end;

function TFormMantenimientoCuenta.CargarDatosCuenta: Boolean;
resourcestring
    MSG_CARGAR_CUENTA = 'La informaci�n es incosistenete. La cuenta %d no existe.';
    CAPTION_OBTENER_CUENTA = 'Obtener datos de la Cuenta';
Var
	i: Integer;
begin
	CargarDebitosAutomaticosGrilla;
	// Buscamos los datos de la cuenta y el cliente
    qry_DatosCuenta.Close;
	qry_DatosCuenta.Parameters.ParamByName('CodigoCuenta').Value	:= FCodigoCuenta;
	Result := OpenTables([qry_DatosCuenta]);
	Screen.Cursor := crHourGlass;
	try
		if not Result then Exit;
		if qry_DatosCuenta.Eof then begin
			MsgBox(format(MSG_CARGAR_CUENTA, [FCodigoCuenta]), CAPTION_OBTENER_CUENTA, MB_ICONSTOP);
			Result := False;
			Exit;
		end;
		// Datos del cliente
		CargarTiposDocumento(DMConnections.BaseCAC, cbTipoNumeroDocumento, Trim(qry_DatosCuenta.FieldByName('CodigoDocumento').AsString));
		cbTipoNumeroDocumento.MaskText := Trim(qry_DatosCuenta.FieldByName('NumeroDocumento').AsString);

		// DATOS DE LA CUENTA

		//Datos del veh�culo
        CargarTiposPatente(DMConnections.BaseCAC, mcPatente, Trim(qry_DatosCuenta.FieldByName('TipoPatente').AsString));
		CargarTiposCliente(DMConnections.BaseCAC, cb_TipoCliente, qry_DatosCuenta.FieldByName('TipoCliente').AsInteger);
		CargarMarcasVehiculos(DMConnections.BaseCAC, cb_Marca, qry_DatosCuenta.FieldByName('CodigoMarca').AsInteger);
		CargarModelosVehiculos(DMConnections.BaseCAC, cb_Modelo, qry_DatosCuenta.FieldByName('CodigoMarca').AsInteger, qry_DatosCuenta.FieldByName('CodigoModelo').AsInteger);

        anio.ValueInt := qry_DatosCuenta.FieldByName('AnioVehiculo').AsInteger;
		TraerDimensionesVehiculo;
		TraerDescripcionCategoria;
        chk_acoplado.Checked := qry_DatosCuenta.FieldByName('TieneAcoplado').AsBoolean;
        chk_acopladoClick(self);
        if qry_DatosCuenta.FieldByName('TieneAcoplado').AsBoolean then
            LargoAdicionalVehiculo.ValueInt := qry_DatosCuenta.FieldByName('LargoAdicionalVehiculo').AsInteger;

		CargarColoresVehiculos(DMConnections.BaseCAC, cb_Color, qry_DatosCuenta.FieldByName('CodigoColor').AsInteger);

		mcPatente.MaskText := Trim(qry_DatosCuenta.FieldByName('Patente').AsString);
		chk_contag.Checked := qry_DatosCuenta.FieldByName('TipoAdhesion').AsInteger = TA_CON_TAG;
		chk_detallepasadas.Checked := qry_DatosCuenta.FieldByName('DetallePasadas').AsBoolean;
		chk_enviofacturas.Checked := qry_DatosCuenta.FieldByName('EnvioFacturas').AsBoolean;

        //Condiciones comerciales
		//rb_prepago.Checked := (qry_DatosCuenta.FieldByName('TipoPago').AsString = TP_PREPAGO);
		rb_pospagomanual.Checked := (qry_DatosCuenta.FieldByName('TipoPago').AsString = TP_POSPAGO_MANUAL);

		rb_pospagoautomatico.Checked := (qry_DatosCuenta.FieldByName('TipoPago').AsString = TP_DEBITO_AUTOMATICO);

		if rb_pospagoautomatico.Checked then begin
			DebitosAutomaticos.First;
            i:= -1;
            cb_debitoautomatico.ItemIndex := -1;
			While not DebitosAutomaticos.Eof do begin
				inc(i);
				if DebitosAutomaticos.FieldByName('CodigoDebitoAutomatico').AsInteger = qry_DatosCuenta.FieldByName('CodigoDebitoAutomatico').AsInteger then begin
                	cb_debitoautomatico.ItemIndex := i;
                    break;
				end;
            	DebitosAutomaticos.Next;
            end;
			cb_debitoautomatico.OnClick(cb_debitoautomatico);
		end;

		(*if rb_pospagoautomatico.Checked then begin
        	cb_DebitoAutomatico.ItemIndex := -1;
			for i:= 0 to cb_debitoautomatico.Items.Count - 1 do begin
            	if Ival(StrRight(cb_DebitoAutomatico.Items[i], 10)) = qry_DatosCuenta.FieldByName('CodigoDebitoAutomatico').AsInteger then begin
					cb_DebitoAutomatico.ItemIndex := i;
                end;
            end;
			cb_debitoautomatico.OnClick(cb_debitoautomatico);
		end;        *)

		CargarPlanesComercialesPosibles(DMConnections.BaseCAC, cb_PlanComercial,
		  qry_DatosCuenta.FieldByName('TipoCliente').AsInteger,
		  qry_DatosCuenta.FieldByName('TipoAdhesion').AsInteger,
		  qry_DatosCuenta.FieldByName('PlanComercial').AsInteger);


		cb_GrupoFacturacion.ItemIndex := 0;
		for i := 0 to cb_GrupoFacturacion.Items.Count - 1 do begin
			if IVal(StrRight(cb_GrupoFacturacion.Items[i], 20)) =
			  qry_DatosCuenta.FieldByName('CodigoGrupoFacturacion').AsInteger then begin
				cb_GrupoFacturacion.ItemIndex := i;
				Break;
			end;
		end;
        CargarGruposCuentas;
		//CargarContratos; //***PENDIENTE
		// Listo
		qry_DatosCuenta.Close;
		Result := True;
	finally
		Screen.Cursor := crDefault;
	end;
end;

procedure TFormMantenimientoCuenta.cb_PlanComercialClick(Sender: TObject);
begin
	CargarContratos;
end;

procedure TFormMantenimientoCuenta.btn_nuevaClick(Sender: TObject);
resourcestring
	CAPTION_INSERTAR_CUENTA_DEBITO = 'Insertar Cuenta de D�bito';
Var
	f: TFormEditarDebito;
begin
	Application.CreateForm(TFormEditarDebito, f);
	f.TipoDebito := DA_TARJETA_CREDITO;
	if f.ShowModal = mrOk then begin
		if BuscarDebito(f.TipoDebito, f.CodigoEntidad, f.CuentaDebito) > 0 then begin
			MsgBox(MSG_ERROR_CUENTA_DEBITO, CAPTION_INSERTAR_CUENTA_DEBITO, MB_ICONSTOP);
		end else begin
			CrearNuevoDebito(f.TipoDebito, f.CodigoEntidad, f.CuentaDebito);
		end;
	end;
	f.Release;
end;

procedure TFormMantenimientoCuenta.btn_modificarClick(Sender: TObject);
resourcestring
    CAPTION_MODIFICAR_CUENTA_DEBITO = 'Modificar Cuenta de D�bito';
Var
	f: TFormEditarDebito;
begin
	Application.CreateForm(TFormEditarDebito, f);
	f.TipoDebito := DebitosAutomaticos.FieldByName('TipoDebito').AsString;
	f.CodigoEntidad := DebitosAutomaticos.FieldByName('CodigoEntidad').AsInteger;
	f.CuentaDebito := DebitosAutomaticos.FieldByName('CuentaDebito').AsString;
	if f.ShowModal = mrOk then begin
		if BuscarDebito(f.TipoDebito, f.CodigoEntidad, f.CuentaDebito) <> -1 then begin
			MsgBox(MSG_ERROR_CUENTA_DEBITO, CAPTION_MODIFICAR_CUENTA_DEBITO, MB_ICONSTOP);
		end else begin
			DebitosAutomaticos.Delete;
			CrearNuevoDebito(f.TipoDebito, f.CodigoEntidad, f.CuentaDebito);
		end;
	end;
	f.Release;
end;

function TFormMantenimientoCuenta.InicializaOS(CodigoOS: Integer): Boolean;
Var
	I: integer;
    PaisAux: AnsiString;
begin
	Screen.Cursor := crHourGlass;

    try
		CodigoDomicilioParticular := -1;
        CodigoDomicilioComercial := -1;
        CodigoDomicilioEntrega := -1;

		FCodigoOrdenServicio := CodigoOS;
		qry_OS.Parameters.ParamByName('CodigoOrdenServicio').Value := CodigoOS;
        Result := OpenTables([qry_OS]);
        if not Result then Exit;
        //
        FCodigoCuenta := 0;
        CargarGruposFacturacion(DMConnections.BaseCAC, cb_GrupoFacturacion);
		if FCodigoCuenta <= 0 then begin
            cb_GrupoFacturacion.Items.Insert(0, MSG_ASIGNAR_GRUPO_FACTURACION);
			cb_GrupoFacturacion.ItemIndex := 0;
        end;
        DebitosAutomaticos.CreateDataSet;
        Contratos.CreateDataSet;
        // Datos del cliente
        //CargarTiposDocumento(DMConnections.BaseCAC, cb_TipoDoc, Trim(qry_OS.FieldByName('CodigoDocumento').AsString));
        //txt_documento.Value := Ival(qry_OS.FieldByName('NumeroDocumento').AsString);

		CargarTiposDocumento(DMConnections.BaseCAC, cbTipoNumeroDocumento, Trim(qry_OS.FieldByName('CodigoDocumento').AsString));
		cbTipoNumeroDocumento.MaskText := Trim(qry_OS.FieldByName('NumeroDocumento').AsString);
        CambioDocumento(cbTipoNumeroDocumento);

		if FCodigoPersona = 0 then begin
			CargarPersoneria(cbPersoneria, qry_OS.FieldByName('Personeria').AsString);
            CargarTiposDocumento(DMConnections.BaseCAC, cbTipoNumeroDocumento,
                                     qry_OS.FieldByName('CodigoDocumento').AsString);
			txt_apellido.Text := Trim(qry_OS.FieldByName('Apellido').AsString);
			txt_nombre.Text := Trim(qry_OS.FieldByName('Nombre').AsString);

            //completo Apellido Materno o Contacto comercial seg�n la Personeria
			if qry_OS.FieldByName('Personeria').AsString = PERSONERIA_FISICA then
                txt_ApellidoMaterno.Text := TrimRight(qry_OS.FieldByName('ApellidoMaterno').AsString)
            else
				//para las Juridicas mostramos el Contacto comercial
				txt_ApellidoMaterno.Text := TrimRight(qry_OS.FieldByName('ContactoComercial').AsString);

			CargarSexos(cb_sexo, qry_OS.FieldByName('Sexo').AsString);
			txt_fechanacimiento.Date := qry_OS.FieldByName('FechaNacimiento').AsDateTime;

            CargarTipoSituacionIVA(DMConnections.BaseCAC, cbSituacionIVA,
                                   qry_OS.FieldByName('CodigoSituacionIVA').AsString);
			PaisAux := iif(qry_OS.FieldByName('LugarNacimiento').IsNull, PAIS_CHILE,
                       Trim(qry_OS.FieldByName('LugarNacimiento').AsString));
			CargarPaises(DMConnections.BaseCAC, cb_Pais, PaisAux);
            txt_fechanacimiento.Date := qry_OS.FieldByName('FechaNacimiento').AsDateTime;
            if qry_OS.FieldByName('CodigoActividad').IsNull then
                CargarActividadesPersona(DMConnections.BaseCAC, cb_actividad)
            else
                CargarActividadesPersona(DMConnections.BaseCAC, cb_actividad, qry_OS.FieldByName('CodigoActividad').AsInteger);


			//***** CARGO LOS DOMICILIOS  ********//
			//1- Primero el Particular
            //2- Segundo el Comercial
            codigodomicilioparticular := ObtenerCodigoDomicilioPersona(FCodigoPersona, TIPO_ORIGEN_DATO_PARTICULAR);
            if (codigodomicilioparticular <> -1) then begin
                {
                with ObtenerDatosDomicilio do
                begin
                    Close;
                    Parameters.ParamByName('@CodigoDomicilio').Value := codigodomicilioparticular;
                    Open;
                    if IsEmpty then begin
                        LimpiarRegistroDomicilio(DomicilioParticular, TIPO_DOMICILIO_PARTICULAR);
                    end
                    else begin
                        DomicilioParticular.CodigoDomicilio := codigodomicilioparticular;
                        //IndiceDomicilio Lo utilizo para indicar que esta ingresado. Si tiene -1 es que no hay datos!!!
                        DomicilioParticular.IndiceDomicilio := 1;
                        DomicilioParticular.Descripcion := '';
                        DomicilioParticular.CodigoTipoDomicilio := TIPO_DOMICILIO_PARTICULAR;
                        DomicilioParticular.CodigoCalle := iif(FieldByName('CodigoCalle').IsNull, -1,FieldByName('CodigoCalle').AsInteger);
                        DomicilioParticular.CodigoTipoCalle := iif(FieldByName('CodigoTipoCalle').IsNull, -1,FieldByName('CodigoTipoCalle').AsInteger);
                        DomicilioParticular.NumeroCalle := FieldByName('Numero').AsInteger;
                        DomicilioParticular.Piso := FieldByName('Piso').AsInteger;
                        DomicilioParticular.Dpto := FieldByName('Dpto').AsString;
                        DomicilioParticular.Detalle := FieldByName('Detalle').AsString;
                        DomicilioParticular.CodigoPostal := FieldByName('CodigoPostal').AsString;
                        DomicilioParticular.CodigoPais := FieldByName('CodigoPais').AsString;
                        DomicilioParticular.CodigoRegion := FieldByName('CodigoRegion').AsString;
                        DomicilioParticular.CodigoComuna := FieldByName('CodigoComuna').AsString;
                        DomicilioParticular.CodigoCiudad := FieldByName('CodigoCiudad').AsString;
                        DomicilioParticular.CalleDesnormalizada := FieldByName('CalleDesnormalizada').AsString;
                        DomicilioParticular.CodigoTipoEdificacion := FieldByName('CodigoTipoEdificacion').AsInteger;
                        DomicilioParticular.DomicilioEntrega := false;
                        DomicilioParticular.CodigoCalleRelacionadaUno := iif(FieldByName('CodigoCalleRelacionadaUno').IsNull, -1,FieldByName('CodigoCalleRelacionadaUno').AsInteger);
                        DomicilioParticular.NumeroCalleRelacionadaUno := iif(FieldByName('NumeroCalleRelacionadaUno').IsNull, -1,FieldByName('NumeroCalleRelacionadaUno').AsInteger);
                        DomicilioParticular.CodigoCalleRelacionadaDos := iif(FieldByName('CodigoCalleRelacionadaDos').IsNull, -1,FieldByName('CodigoCalleRelacionadaDos').AsInteger);
                        DomicilioParticular.NumeroCalleRelacionadaDos := iif(FieldByName('NumeroCalleRelacionadaDos').IsNull, -1,FieldByName('NumeroCalleRelacionadaDos').AsInteger);
                        DomicilioParticular.Activo := FieldByName('Activo').AsBoolean;
                    end;
                end;
                if (codigodomicilioparticular = codigodomicilioentrega) then
                    MarcarComoDomicilioEntrega(TIPO_ORIGEN_DATO_PARTICULAR);
                }
            end
            else LimpiarRegistroDomicilio(DomicilioParticular, TIPO_ORIGEN_DATO_PARTICULAR);

            //***** DOMICILIO COMERCIAL ******//
            codigodomiciliocomercial := ObtenerCodigoDomicilioPersona(FCodigoPersona, TIPO_ORIGEN_DATO_COMERCIAL);
            if (codigodomiciliocomercial <> -1) then begin
                {
                with ObtenerDatosDomicilio do
                begin
                    Close;
                    Parameters.ParamByName('@CodigoDomicilio').Value := codigodomiciliocomercial;
                    Open;
                    if IsEmpty then begin
                        LimpiarRegistroDomicilio(DomicilioComercial, TIPO_ORIGEN_DATO_COMERCIAL);
                    end
                    else begin
                        Domiciliocomercial.CodigoDomicilio := codigodomiciliocomercial;
                        Domiciliocomercial.IndiceDomicilio := 1;
                        Domiciliocomercial.Descripcion := '';
                        Domiciliocomercial.CodigoTipoDomicilio := TIPO_ORIGEN_DATO_COMERCIAL;
                        Domiciliocomercial.CodigoCalle := iif(FieldByName('CodigoCalle').IsNull, -1,FieldByName('CodigoCalle').AsInteger);
                        DomicilioComercial.CodigoTipoCalle := iif(FieldByName('CodigoTipoCalle').IsNull, -1,FieldByName('CodigoTipoCalle').AsInteger);
                        DomicilioComercial.NumeroCalle := FieldByName('Numero').AsInteger;
                        DomicilioComercial.Piso := FieldByName('Piso').AsInteger;
                        DomicilioComercial.Dpto := FieldByName('Dpto').AsString;
                        DomicilioComercial.Detalle := FieldByName('Detalle').AsString;
                        DomicilioComercial.CodigoPostal := FieldByName('CodigoPostal').AsString;
                        DomicilioComercial.CodigoPais := FieldByName('CodigoPais').AsString;
                        DomicilioComercial.CodigoRegion := FieldByName('CodigoRegion').AsString;
                        DomicilioComercial.CodigoComuna := FieldByName('CodigoComuna').AsString;
                        DomicilioComercial.CodigoCiudad := FieldByName('CodigoCiudad').AsString;
                        DomicilioComercial.CalleDesnormalizada := FieldByName('CalleDesnormalizada').AsString;
                        DomicilioComercial.CodigoTipoEdificacion := FieldByName('CodigoTipoEdificacion').AsInteger;
                        DomicilioComercial.DomicilioEntrega := false;
                        DomicilioComercial.CodigoCalleRelacionadaUno := iif(FieldByName('CodigoCalleRelacionadaUno').IsNull, -1,FieldByName('CodigoCalleRelacionadaUno').AsInteger);
                        DomicilioComercial.NumeroCalleRelacionadaUno := iif(FieldByName('NumeroCalleRelacionadaUno').IsNull, -1,FieldByName('NumeroCalleRelacionadaUno').AsInteger);
                        DomicilioComercial.CodigoCalleRelacionadaDos := iif(FieldByName('CodigoCalleRelacionadaDos').IsNull, -1,FieldByName('CodigoCalleRelacionadaDos').AsInteger);
                        DomicilioComercial.NumeroCalleRelacionadaDos := iif(FieldByName('NumeroCalleRelacionadaDos').IsNull, -1,FieldByName('NumeroCalleRelacionadaDos').AsInteger);
                        DomicilioComercial.Activo := FieldByName('Activo').AsBoolean;
                    end;
                end;
                if (codigodomiciliocomercial = codigodomicilioentrega) then
                    MarcarComoDomicilioEntrega(TIPO_ORIGEN_DATO_COMERCIAL);
                }
            end
            else LimpiarRegistroDomicilio(DomicilioComercial, TIPO_ORIGEN_DATO_COMERCIAL);


            //*** COMPLETAR  ***//
            PrepararMostrarDomicilio (iif(codigodomicilioparticular < 1, '',
                                        ArmarDomicilioCompletoPorCodigo(DMConnections.BaseCAC, -1,codigodomicilioparticular)),
                                        TIPO_ORIGEN_DATO_PARTICULAR);
            PrepararMostrarDomicilio (iif(codigodomiciliocomercial < 1, '',
                                        ArmarDomicilioCompletoPorCodigo(DMConnections.BaseCAC, -1,codigodomiciliocomercial)),
                                        TIPO_ORIGEN_DATO_COMERCIAL);


            //****** Fin cargar domicilios *******//
        end else begin
            //cb_tipodoc.OnChange := nil;
            //txt_documento.OnChange := nil;
            cbTipoNumeroDocumento.OnChange := nil;
        end;
		// Datos de la cuenta
		CargarTiposPatente(DMConnections.BaseCAC, mcPatente,
          Trim(qry_OS.FieldByName('TipoPatente').AsString));
		CargarTiposCliente(DMConnections.BaseCAC, cb_TipoCliente, 1);	// Hola qry_OS.FieldByName('TipoCliente').AsInteger);
        CargarMarcasVehiculos(DMConnections.BaseCAC, cb_Marca,
          qry_OS.FieldByName('CodigoMarca').AsInteger);
        CargarModelosVehiculos(DMConnections.BaseCAC, cb_Modelo,
          qry_OS.FieldByName('CodigoMarca').AsInteger,
          qry_OS.FieldByName('CodigoModelo').AsInteger);
		anio.ValueInt := qry_OS.FieldByName('AnioVehiculo').AsInteger;
        TraerDimensionesVehiculo;
		TraerDescripcionCategoria;
		chk_acoplado.Checked := qry_OS.FieldByName('TieneAcoplado').AsBoolean;
        chk_acopladoClick(self);
        LargoAdicionalVehiculo.ValueInt := qry_OS.FieldByName('LargoAdicionalVehiculo').AsInteger;

        CargarColoresVehiculos(DMConnections.BaseCAC, cb_Color,
          qry_OS.FieldByName('CodigoColor').AsInteger);
        mcPatente.MaskText :=
		  Trim(qry_OS.FieldByName('Patente').AsString);
        chk_contag.Checked :=
		  qry_OS.FieldByName('TipoAdhesion').AsInteger = TA_CON_TAG;
		chk_detallepasadas.Checked :=
          qry_OS.FieldByName('DetallePasadas').AsBoolean;
        chk_enviofacturas.Checked :=
          qry_OS.FieldByName('EnvioFacturas').AsBoolean;

		// Tipo de Pago
		if qry_OS.FieldByName('TipoPago').AsString = TP_DEBITO_AUTOMATICO then begin
			// Si es un d�bito autom�tico nuevo, lo creamos
			i := BuscarDebito(qry_OS.FieldByName('TipoDebito').AsString,
			  qry_OS.FieldByName('CodigoEntidad').AsInteger,
			  qry_OS.FieldByName('CuentaDebito').AsString);
			if (i <= 0) then begin
				CrearNuevoDebito(
				  qry_OS.FieldByName('TipoDebito').AsString,
				  qry_OS.FieldByName('CodigoEntidad').AsInteger,
				  qry_OS.FieldByName('CuentaDebito').AsString);
				i := DebitosAutomaticos.RecNo;
			end;
			cb_debitoautomatico.ItemIndex := i - 1;
			rb_pospagoautomatico.Checked := True;
		end else if qry_OS.FieldByName('TipoPago').AsString = TP_POSPAGO_MANUAL then begin
			rb_pospagomanual.Checked := True;
		end else begin
			rb_prepago.Checked := True; //igual esta invisible el control este.
		end;
		// Plan Comercial
		CargarPlanesComercialesPosibles(DMConnections.BaseCAC, cb_PlanComercial,
		  1, qry_OS.FieldByName('TipoAdhesion').AsInteger);		// Hola qry_OS.FieldByName('TipoCliente').AsInteger
        // Listo
		qry_OS.Close;
        CargarContratos;
		ActualizarPago(nil);
		CargarGruposCuentas;
        PageControl.ActivePage := tab_cliente;
	finally
        Screen.Cursor := crDefault;
    end;
end;

procedure TFormMantenimientoCuenta.OPButton1Click(Sender: TObject);
resourcestring
	CAPTION_GUARDAR = 'Guardar cuenta';
begin
	Screen.Cursor := crHourGlass;
	try
        try
            DMConnections.BaseCAC.BeginTrans;
			GuardarDatos;
            DMConnections.BaseCAC.CommitTrans;
		except
            on E: Exception do begin
                DMConnections.BaseCAC.RollbackTrans;
                MsgBox(e.Message, CAPTION_GUARDAR, MB_ICONSTOP);
            end;
		end;
    finally
    	Screen.Cursor := crDefault;
	end;
end;

function TFormMantenimientoCuenta.BuscarDebito(TipoDebito: AnsiString;
  CodigoEntidad: Integer; CuentaDebito: AnsiString): Integer;
Var
	b: TBookmark;
begin
	b := DebitosAutomaticos.GetBookmark;
	if DebitosAutomaticos.Locate('TipoDebito;CodigoEntidad;CuentaDebito',
	  VarArrayOf([TipoDebito, CodigoEntidad, CuentaDebito]), [loCaseInsensitive]) then
		Result := DebitosAutomaticos.Recno
	else begin
		Result := -1;
	end;
	DebitosAutomaticos.GotoBookmark(b);
	DebitosAutomaticos.FreeBookmark(b);
end;

procedure TFormMantenimientoCuenta.CrearNuevoDebito(TipoDebito: AnsiString;
  CodigoEntidad: Integer; CuentaDebito: AnsiString);
begin
	DebitosAutomaticos.Append;
    DebitosAutomaticos.FieldByName('CodigoDebitoAutomatico').AsInteger := 0;
	DebitosAutomaticos.FieldByName('TipoDebito').AsString := TipoDebito;
	DebitosAutomaticos.FieldByName('CodigoEntidad').AsInteger := CodigoEntidad;
	DebitosAutomaticos.FieldByName('CuentaDebito').AsString := CuentaDebito;
	DebitosAutomaticos.FieldByName('Descripcion').AsString := ObtenerDescripcionDebito(
	  DMConnections.BaseCAC, TipoDebito, CodigoEntidad, CuentaDebito);
	DebitosAutomaticos.Post;
	ListaDebitos.Reload;
	ActualizarComboDebitos;
end;

procedure TFormMantenimientoCuenta.cbTipoNumeroDocumentoChange(
  Sender: TObject);
begin
	if (Sender = ActiveControl) then
        CambioDocumento(Sender);
end;

procedure TFormMantenimientoCuenta.btn_quitarClick(Sender: TObject);
resourcestring
    CAPTION_MODIFICAR_CUENTA_DEBITO = 'Quitar Cuenta de D�bito';
    ESTA_SEGURO = '�Esta seguro de borrar esta cuenta?';
var
	desc: string;
    accion: TModalResult;
    volverAPreguntar: boolean;
begin
	//CheckBrowseMode "post" cambios pendientes de un registro previo, si es nesario
    desc := '';
    volverAPreguntar := true;
    accion := mrYes;
	if (DebitosAutomaticos.Active) and (not DebitosAutomaticos.IsEmpty) then
	begin
        desc := DebitosAutomaticos.FieldByName('Descripcion').AsString;
        if PuedeBorrarDebito(FCodigoPersona,
							 DebitosAutomaticos.FieldByName('CodigoDebitoautomatico').AsInteger,
							 desc, volverAPreguntar) then begin
            if volverAPreguntar then
                accion := MsgBox(ESTA_SEGURO + #13#10 + desc, CAPTION_MODIFICAR_CUENTA_DEBITO, MB_YESNO);
			if accion = mrYes then begin
                //DebitosAutomaticos.CheckBrowseMode;
				if (DebitosAutomaticos.FieldByName('CodigoDebitoAutomatico').AsInteger <> 0) then begin
                    //La pongo en la tabla de Borradas si es que exist�a de antes, o sea en la base
                    //por que puede ser que la haya creado y borrado en el momento
                    with DebitosAutomaticosBorrados do
                    begin
                        Append;
                        //La clave para borrar es CodigoCliente + CodigoDebitoAutomatico
						FieldByName('CodigoDebitoAutomatico').Assign(DebitosAutomaticos.FieldByName('CodigoDebitoautomatico'));
						//FieldByName('CodigoCliente').Assign(DebitosAutomaticos.FieldByName('CodigoCliente'));
						Post;
					end;
                end;
                DebitosAutomaticos.Delete;
				ActualizarComboDebitos;
				ListaDebitos.Reload;
            end;
        end;
    end;
end;

procedure TFormMantenimientoCuenta.TraerDimensionesVehiculo;
var LargoAux: integer;
begin
    //Para traer las dimensiones se necesita la marca, el modelo y el a�o
    //y con eso se va a buscar a la tabla Vehiculos
    if (cb_marca.itemindex >= 0) and
	   (cb_modelo.itemindex >= 0) and
	   (EsAnioCorrecto(anio.ValueInt)) then begin
		LargoAux := ObtenerLargoVehiculo(DMConnections.BaseCAC, IVal(StrRight(cb_marca.Text, 20)),
                            IVal(StrRight(cb_modelo.Text, 20)), anio.ValueInt);
        if LargoAux = 0 then largoVehiculo.Clear
        else largoVehiculo.ValueInt := LargoAux;
    end;
end;

procedure TFormMantenimientoCuenta.chk_acopladoClick(Sender: TObject);
begin
    if chk_acoplado.Checked then begin
        LargoAdicionalVehiculo.Enabled := true;
		LargoAdicionalVehiculo.Color := clWindow;
    end else begin
		LargoAdicionalVehiculo.Enabled := false;
        LargoAdicionalVehiculo.Color := largoVehiculo.Color;
    end;
end;

procedure TFormMantenimientoCuenta.cb_marcaChange(Sender: TObject);
begin
	CargarModelosVehiculos(DMConnections.BaseCAC, cb_Modelo, IVal(StrRight(cb_Marca.text, 20)));
end;

procedure TFormMantenimientoCuenta.TraerDescripcionCategoria;
var
    CategoriaAux: integer;
begin
    if (cb_marca.itemindex >= 0) and
	   (cb_modelo.itemindex >= 0) then begin
//          txt_DescCategoria.text := BuscarDescripcionCategoriaVehiculo(DMConnections.BaseCAC,
//                                          IVal(StrRight(cb_marca.text, 20)),
//                                          IVal(StrRight(cb_modelo.text, 20)),
//                                          CategoriaAux);
          txt_codigoCategoria.ValueInt := CategoriaAux;
	end
	else
    begin
		txt_DescCategoria.Clear;
        txt_codigoCategoria.Clear;
    end;
end;

procedure TFormMantenimientoCuenta.anioChange(Sender: TObject);
begin
	TraerDimensionesVehiculo;
end;

procedure TFormMantenimientoCuenta.cb_modeloChange(Sender: TObject);
begin
    TraerDimensionesVehiculo;
	TraerDescripcionCategoria;
end;

function TFormMantenimientoCuenta.PuedeBorrarDebito(Cliente,
  DebitoAutomatico: integer; Descripcion: AnsiString; var volverApreguntar: boolean): boolean;
resourceString
    CAPTION_BORRAR_DEBITO = 'Quitar D�bito autom�tico';
	BORRAR_DEBITO = 'El debito autom�tico se encuentra seleccionado como medio de pago. ';
    DESEA_BORRAR = '�Desea borrarlo de todas maneras? ';
var
    ok: boolean;
    accion: TModalResult;
begin
	ok := QueryGetValueInt(DMConnections.BaseCAC, Format(
        'SELECT 1 FROM Cuentas (INDEX = IX_CodigoCliente) WHERE CodigoPersona = %d ' +
        'AND CodigoDebitoAutomatico = %d ',
		[Cliente, DebitoAutomatico])) <> 1;
    if not ok then begin
        accion := MsgBox(BORRAR_DEBITO + #13#10 + Descripcion + #13#10 +
						 DESEA_BORRAR, CAPTION_BORRAR_DEBITO, MB_YESNO);
		if accion = mrYes then begin
			//1- Borrar el debito de la cuenta
            //2- Refrescar el combo
            //3- No dejar seleccionado ninguno
            QuitarDebitoAutomaticoDecuenta(DMConnections.BaseCAC, FCodigoCuenta);
            CargarDebitosAutomaticos(DMConnections.BaseCAC,
                                     cb_debitoautomatico,
									 FCodigoPersona);
            cb_debitoautomatico.ItemIndex := 0;
			ActualizarComboDebitos;
			//Si esta asociado a la cuenta y lo borra, todo ok
            ok := true;
        end;
        volverApreguntar := false;
	end
    else begin
		if rb_pospagoautomatico.Checked then
        if IVal(StrRight(cb_debitoautomatico.Text, 10)) = 0 then
			ActualizarComboDebitos;
	end;
    result := ok;
end;

procedure TFormMantenimientoCuenta.HabilitarCamposPersoneria(Personeria: Char);
ResourceString
    LBL_PERSONA_RAZON_SOCIAL        = 'Raz�n Social';
    LBL_PERSONA_NOMBRE_FANTASIA     = 'Nombre de Fantas�a:';
	LBL_PERSONA_SITUACION_IVA       = 'Situaci�n de IVA:';
	LBL_PERSONA_FECHA_CREACION      = 'Fecha de Creaci�n:';
	LBL_PERSONA_APELLIDO            = 'Apellido:';
	LBL_PERSONA_NOMBRE              = 'Nombre:';
	LBL_PERSONA_APELLIDO_MATERNO    = 'Apellido Materno:';
    LBL_PERSONA_CONTACTO_COMERCIAL  = 'Contacto Comercial:';
    LBL_PERSONA_SEXO                = 'Sexo:';
    LBL_PERSONA_FECHA_NACIMIENTO    = 'Fecha de Nacimiento:';
begin
    Case Personeria of
		PERSONERIA_JURIDICA:
			begin
                lblApellido.Caption         := LBL_PERSONA_RAZON_SOCIAL;
                lblNombre.Caption           := LBL_PERSONA_NOMBRE_FANTASIA;
                lblApellidoMaterno.Caption  := LBL_PERSONA_CONTACTO_COMERCIAL;
                lblSexo.Visible             := False;
                cb_Sexo.Visible             := False;
                lblFechaNacCreacion.Caption := LBL_PERSONA_FECHA_CREACION;
			end;
		PERSONERIA_FISICA:
			begin
                lblApellido.Caption         := LBL_PERSONA_APELLIDO;
                lblNombre.Caption           := LBL_PERSONA_NOMBRE;
                lblApellidoMaterno.Caption  := LBL_PERSONA_APELLIDO_MATERNO;
                lblSexo.Caption             := LBL_PERSONA_SEXO;
                lblSexo.Visible             := True;
                cb_Sexo.Visible             := True;
				lblFechaNacCreacion.Caption := LBL_PERSONA_FECHA_NACIMIENTO;
            end;
	end;
end;


procedure TFormMantenimientoCuenta.cbPersoneriaChange(Sender: TObject);
begin
	HabilitarCamposPersoneria(StrRight(Trim(cbPersoneria.Text), 1)[1]);
end;

procedure TFormMantenimientoCuenta.FormCreate(Sender: TObject);
begin
	CodigoDomicilioParticular := -1;
	CodigoDomicilioComercial := -1;
	CodigoDomicilioEntrega := -1;
end;

procedure TFormMantenimientoCuenta.GuardarMedioComunicacion(persona,
  mediocontacto, tipoorigen: integer; valor: AnsiString);
begin
	ActualizarMedioComunicacionPersona.Close;
	with ActualizarMedioComunicacionPersona.Parameters do begin
		ParamByName('@CodigoPersona').Value         := persona;
		ParamByName('@CodigoMedioContacto').Value   := mediocontacto;
		ParamByName('@CodigoTipoOrigenDato').Value  := tipoorigen;
		ParamByName('@Valor').Value                 := valor;
		ParamByName('@CodigoDomicilio').Value       := null;
		ParamByName('@HorarioDesde').Value          := null;
		ParamByName('@HorarioHasta').Value          := null;
		ParamByName('@Observaciones').Value         := null;
        ParamByName('@Activo').Value                := 1;
        ActualizarMedioComunicacionPersona.ExecProc;
    end;
end;

procedure TFormMantenimientoCuenta.GuardarMediosComunicacionContacto(
  persona: integer);
resourcestring
	MSG_ACTUALIZAR_MEDIO_CONTACTO_ERROR = 'No se pudo actualizar el medio de comunicaci�n del contacto';
	MSG_ACTUALIZAR_MEDIO_CONTACTO_CAPTION = 'Actualizar contacto';
begin
    //Guardo los datos en la tabla MediosComunicacion
	try
        //Telefono Particular
		if (GuardarTelefonoParticular) or (trim(txtTelefonoParticular.Text) <> '') then begin
            GuardarMedioComunicacion(persona, MC_TELEPHONE,
                    TIPO_ORIGEN_DATO_PARTICULAR, txtTelefonoParticular.Text);
        end;
        //Telefono Comercial
        if (GuardarTelefonoComercial) or (trim(txtTelefonoComercial.Text) <> '') then begin
            GuardarMedioComunicacion(persona, MC_TELEPHONE,
					TIPO_ORIGEN_DATO_COMERCIAL, txtTelefonoComercial.Text);
        end;
		//Telefono Movil
        if (GuardarTelefonoMovil) or (trim(txtTelefonoMovil.Text) <>'') then begin
			GuardarMedioComunicacion(persona, MC_MOVIL_PHONE,
                    TIPO_ORIGEN_DATO_PARTICULAR, txtTelefonoMovil.Text);
		end;
		//Email Particular
        if (GuardarEmail) or (trim(txtEmailParticular.Text) <> '') then begin
			GuardarMedioComunicacion(persona, MC_EMAIL,
					TIPO_ORIGEN_DATO_COMERCIAL, txtEmailParticular.Text);
		end;
	except
        on E: exception do begin
            MsgBoxErr( MSG_ACTUALIZAR_MEDIO_CONTACTO_ERROR, e.message, MSG_ACTUALIZAR_MEDIO_CONTACTO_CAPTION, MB_ICONSTOP);
            ActualizarMedioComunicacionPersona.Close;
            Screen.Cursor := crDefault;
            Exit;
        end;
    end;
end;

procedure TFormMantenimientoCuenta.txtTelefonoParticularKeyPress(
  Sender: TObject; var Key: Char);
begin
    if not (Key  in ['0'..'9','-', #8]) then
        Key := #0;
end;

procedure TFormMantenimientoCuenta.txtTelefonoComercialKeyPress(
  Sender: TObject; var Key: Char);
begin
	if not (Key  in ['0'..'9','-', #8]) then
        Key := #0;
end;

procedure TFormMantenimientoCuenta.txtTelefonoMovilKeyPress(
  Sender: TObject; var Key: Char);
begin
    if not (Key  in ['0'..'9','-', #8]) then
		Key := #0;
end;

procedure TFormMantenimientoCuenta.Label34Click(Sender: TObject);
begin
	FInfoPlanesComerciales.ShowModal;
end;

procedure TFormMantenimientoCuenta.btn_AgregarRolClick(Sender: TObject);
resourcestring
	MSG_AGREGAR_ROL_ERROR = 'No se pudo agregar el rol';
	MSG_AGREGAR_ROL_CAPTION = 'Agregar Rol';
begin
	with frmSeleccionarRolYPersona Do
		If (ShowModal = mrOk) And (CodigoPersona <> 0) And (CodigoRol <> 0) Then Begin
			Screen.Cursor := crHourGlass;

			try
				With ActualizarRolesCuentas, Parameters Do
				Begin
					ParamByName ('@CodigoRol').Value 	:= CodigoRol;
					ParamByName ('@CodigoCuenta').Value := FCodigoCuenta;
					ParamByName ('@CodigoPersona').Value := CodigoPersona;
					ExecProc;
				end;
			except
				On E: exception do begin
					MsgBoxErr(MSG_AGREGAR_ROL_CAPTION, e.message, MSG_AGREGAR_ROL_ERROR, MB_ICONSTOP);
					ActualizarRolesCuentas.Close;
				end
			end;

			Screen.Cursor := crDefault
		End;

	qry_RolesCuenta.Close;
	qry_RolesCuenta.Open
end;

procedure TFormMantenimientoCuenta.btn_QuitarRolClick(Sender: TObject);
resourcestring
	MSG_BORRAR_ROL_TITULAR_ERROR = 'No se puede borrar el rol Titular de la Cuenta';
	MSG_BORRAR_ROL_ERROR = 'No se pudo borrar el rol';
	MSG_BORRAR_ROL_CAPTION = 'Borrar Rol';
begin
	if qry_RolesCuenta.FieldByName('CodigoRol').AsInteger = 1 then
		MsgBox(MSG_BORRAR_ROL_TITULAR_ERROR, MSG_BORRAR_ROL_CAPTION, MB_ICONERROR or MB_OK)
	else begin
		Screen.Cursor := crHourGlass;

		try
			With BorrarRolesCuentas, Parameters Do
			Begin
				ParamByName ('@CodigoRol').Value 	:= qry_RolesCuenta.FieldByName('CodigoRol').AsInteger;
				ParamByName ('@CodigoCuenta').Value := qry_RolesCuenta.FieldByName('CodigoCuenta').AsInteger;
				ParamByName ('@CodigoPersona').Value := qry_RolesCuenta.FieldByName('CodigoPersona').AsInteger;

				ExecProc;
				Close
			end
		except
			On E: exception do begin
				MsgBoxErr(MSG_BORRAR_ROL_CAPTION, e.message, MSG_BORRAR_ROL_CAPTION, MB_ICONSTOP);
				BorrarRolesCuentas.Close;
			end
		end;

		Screen.Cursor := crDefault
	end;
	qry_RolesCuenta.Close;
	qry_RolesCuenta.Open
end;

procedure AltaDomicilio(tipoOrigenDatos: integer; var datosDomicilio: TDatosDomicilio);
ResourceString
    MSG_AGREGAR_CAPTION = 'Agregar Domicilio';
    MSG_ERROR_AGREGAR   = 'No se ha podido agregar el domicilio';
var
    f: TFormEditarDomicilio;
begin
    {LimpiarRegistroDomicilio(datosDomicilio, tipoOrigenDatos);
    Application.CreateForm(TFormEditarDomicilio, f);
    if f.Inicializar(tipoOrigenDatos, true) and (f.ShowModal = mrOK) then begin
        try
            with datosDomicilio do begin
                CodigoTipoDomicilio := f.TipoDomicilio;
                //lo utilizo para indicar que hay datos. Si tiene -1 es porque esta vac�o
                IndiceDomicilio := 1;
                CodigoCalle := f.CodigoCalle;
                CodigoTipoCalle := f.TipoCalle;
                NumeroCalle := f.Numero;
                Piso := f.Piso;
                Dpto := f.Depto;
                Detalle := f.Detalle;
                CodigoPostal := f.CodigoPostal;
                CodigoPais := f.Pais;
                CodigoRegion := f.Region;
                CodigoComuna := f.Comuna;
                CodigoCiudad := f.Ciudad;
                CalleDesnormalizada := f.DescripcionCalle;
                CodigoTipoEdificacion := f.TipoEdificacion;
                DomicilioEntrega := f.DomicilioEntrega;
                Normalizado := f.CodigoCalle > 0;
                CodigoCalleRelacionadaUno := f.CalleRelacionadaUno;
                NumeroCalleRelacionadaUno := f.NumeroCalleRelacionadaUno;
                CodigoCalleRelacionadaDos := f.CalleRelacionadaDos;
                NumeroCalleRelacionadaDos := f.NumeroCalleRelacionadaDos;
                Descripcion := f.DomicilioCompleto;
//                if f.DomicilioEntrega then MarcarComoDomicilioEntrega(tipoOrigenDatos);
//                PrepararMostrarDomicilio(Descripcion,tipoOrigenDatos);
            end;
        except
            On E: Exception do begin
                MsgBoxErr( MSG_ERROR_AGREGAR, E.message, MSG_AGREGAR_CAPTION, MB_ICONSTOP);
                //dsDomicilios.DataSet.Cancel;
            end;
        end;
    end;
    f.Release;}
end;


function TFormMantenimientoCuenta.EstaElegidoComoDomicilioEntrega(
  tipoOrigenDatos: integer): boolean;
begin
    if tipoOrigenDatos = TIPO_ORIGEN_DATO_PARTICULAR then
        result := RGDomicilioEntrega.ItemIndex = 0
    else
        if tipoOrigenDatos = TIPO_ORIGEN_DATO_COMERCIAL then
            result := RGDomicilioEntrega.ItemIndex = 1
        else
            result := false;
end;

procedure TFormMantenimientoCuenta.MarcarComoDomicilioEntrega(
  tipoOrigenDatos: integer);
begin
    if tipoOrigenDatos = TIPO_ORIGEN_DATO_PARTICULAR then
        RGDomicilioEntrega.ItemIndex := 0
    else
        if tipoOrigenDatos = TIPO_ORIGEN_DATO_COMERCIAL then
            RGDomicilioEntrega.ItemIndex := 1
        else
            RGDomicilioEntrega.ItemIndex := -1;
end;

procedure TFormMantenimientoCuenta.ModificarDomicilio(
  tipoOrigenDatos: integer; var datosDomicilio: TDatosDomicilio);
ResourceString
    MSG_EDITAR_CAPTION = 'Modificar Domicilio';
    MSG_ERROR_EDITAR   = 'No se ha podido modificar el domicilio';

var
	f: TFormEditarDomicilio;
begin
    Application.CreateForm(TFormEditarDomicilio, f);
    {
    if f.Inicializar(
        datosDomicilio.CodigoDomicilio,
        tipoOrigenDatos,
        datosDomicilio.CodigoPais,
        datosDomicilio.CodigoRegion,
        datosDomicilio.CodigoComuna,
        datosDomicilio.CodigoCiudad,
        datosDomicilio.CodigoTipoCalle,
        datosDomicilio.CodigoCalle,
        datosDomicilio.CalleDesnormalizada,
        datosDomicilio.NumeroCalle,
        datosDomicilio.Piso,
        datosDomicilio.Dpto,
        datosDomicilio.Detalle,
        datosDomicilio.CodigoPostal,
        datosDomicilio.CodigoTipoEdificacion,
        EstaElegidoComoDomicilioEntrega(tipoOrigenDatos),
        datosDomicilio.CodigoCalleRelacionadaUno,
        datosDomicilio.NumeroCalleRelacionadaUno,
        datosDomicilio.CodigoCalleRelacionadaDos,
        datosDomicilio.NumeroCalleRelacionadaDos
        ) and (f.ShowModal = mrOK) then begin
        try
            with datosDomicilio do begin
                CodigoTipoDomicilio := f.TipoDomicilio;
                IndiceDomicilio := 1; //significa que hay datos
                CodigoCalle := f.CodigoCalle;
                CodigoTipoCalle := f.TipoCalle;
                NumeroCalle := f.Numero;
                Piso := f.Piso;
                Dpto := f.Depto;
                Detalle := f.Detalle;
                CodigoPostal := f.CodigoPostal;
                CodigoPais := f.Pais;
                CodigoRegion := f.Region;
                CodigoComuna := f.Comuna;
                CodigoCiudad := f.Ciudad;
                CalleDesnormalizada := f.DescripcionCalle;
                CodigoTipoEdificacion := f.TipoEdificacion;
                DomicilioEntrega := f.DomicilioEntrega;
                Normalizado := f.CodigoCalle > 0;
                CodigoCalleRelacionadaUno := f.CalleRelacionadaUno;
                NumeroCalleRelacionadaUno := f.NumeroCalleRelacionadaUno;
                CodigoCalleRelacionadaDos := f.CalleRelacionadaDos;
                NumeroCalleRelacionadaDos := f.NumeroCalleRelacionadaDos;
                Descripcion := f.DomicilioCompleto;
                if f.DomicilioEntrega then MarcarComoDomicilioEntrega(tipoOrigenDatos);
                PrepararMostrarDomicilio(Descripcion, tipoOrigenDatos);
            end;
        except
            On E: Exception do begin
                MsgBoxErr( MSG_ERROR_EDITAR, E.message, MSG_EDITAR_CAPTION, MB_ICONSTOP);
            end;
        end;
    end;
    }
    f.Release;
end;

procedure TFormMantenimientoCuenta.MostrarDomicilio(var L: TLabel;
  descripcion: AnsiString);
resourcestring
    FALTA_INGRESAR = 'Ingrese el domicilio';
begin
    if descripcion = '' then begin
        l.Font.Style := [fsItalic];
        l.Caption := FALTA_INGRESAR;
    end
    else begin
        l.Font.Style := [];
        l.Caption := descripcion;
    end;
end;

procedure TFormMantenimientoCuenta.PrepararMostrarDomicilio(
  descripcion: AnsiString; tipoOrigen: integer);
begin
    if tipoOrigen = TIPO_ORIGEN_DATO_PARTICULAR then begin
        if (descripcion = '') then
            BtnDomicilioParticularAccion.Caption := CAPTION_BOTON_DOMICILIO_ALTA
        else
            BtnDomicilioParticularAccion.Caption := CAPTION_BOTON_DOMICILIO_MODIFICAR;
        ChkEliminarDomicilioParticular.Enabled := (descripcion <> '') and (codigodomicilioparticular > 0);
        MostrarDomicilio (lblDomicilioParticular, descripcion);
    end
    else begin
        if (descripcion = '') then
            BtnDomicilioComercialAccion.Caption := CAPTION_BOTON_DOMICILIO_ALTA
        else
            BtnDomicilioComercialAccion.Caption := CAPTION_BOTON_DOMICILIO_MODIFICAR;
        ChkEliminarDomicilioComercial.Enabled := (descripcion <> '') and (codigodomiciliocomercial > 0);
        MostrarDomicilio (lblDomicilioComercial, descripcion);
    end;
end;

procedure TFormMantenimientoCuenta.BtnDomicilioParticularAccionClick(
  Sender: TObject);
begin
    if (BtnDomicilioParticularAccion.Caption = CAPTION_BOTON_DOMICILIO_ALTA) then begin
        //Alta
        AltaDomicilio(TIPO_ORIGEN_DATO_PARTICULAR, DomicilioParticular);
        //Si todav�a no eligi� ning�n domicilio para entrega, lo marco
        if (not EstaElegidoComoDomicilioEntrega(TIPO_ORIGEN_DATO_PARTICULAR)) and
           (not EstaElegidoComoDomicilioEntrega(TIPO_ORIGEN_DATO_COMERCIAL)) then
            MarcarComoDomicilioEntrega(TIPO_ORIGEN_DATO_PARTICULAR);
    end
    else begin
        //Modificacion
        ModificarDomicilio(TIPO_ORIGEN_DATO_PARTICULAR, DomicilioParticular);
    end;
end;

procedure TFormMantenimientoCuenta.BtnDomicilioComercialAccionClick(
  Sender: TObject);
begin
    if (BtnDomicilioComercialAccion.Caption = CAPTION_BOTON_DOMICILIO_ALTA) then begin
        //Alta
        AltaDomicilio(TIPO_ORIGEN_DATO_COMERCIAL, DomicilioComercial);
        //Si todav�a no eligi� ning�n domicilio para entrega, lo marco
        if (not EstaElegidoComoDomicilioEntrega(TIPO_ORIGEN_DATO_PARTICULAR)) and
           (not EstaElegidoComoDomicilioEntrega(TIPO_ORIGEN_DATO_COMERCIAL)) then
            MarcarComoDomicilioEntrega(TIPO_ORIGEN_DATO_COMERCIAL);
    end
    else begin
        //Modificacion
        ModificarDomicilio(TIPO_ORIGEN_DATO_COMERCIAL, DomicilioComercial);
    end;
end;

procedure TFormMantenimientoCuenta.ChkEliminarDomicilioParticularClick(
  Sender: TObject);
begin
    ClickCheckEliminar(chkEliminarDomicilioParticular,
                    EstaElegidoComoDomicilioEntrega(TIPO_ORIGEN_DATO_PARTICULAR));
end;

procedure TFormMantenimientoCuenta.ChkEliminarDomicilioComercialClick(
  Sender: TObject);
begin
    ClickCheckEliminar(chkEliminarDomicilioComercial,
                    EstaElegidoComoDomicilioEntrega(TIPO_ORIGEN_DATO_COMERCIAL));
end;

end.
