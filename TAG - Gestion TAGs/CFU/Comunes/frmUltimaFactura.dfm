object FormUltimaFactura: TFormUltimaFactura
  Left = 232
  Top = 120
  Width = 635
  Height = 453
  Caption = 'Presentaci'#243'n Preliminar'
  Color = clBtnFace
  Constraints.MinHeight = 200
  Constraints.MinWidth = 350
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object ControlBar1: TControlBar
    Left = 0
    Top = 0
    Width = 627
    Height = 44
    Align = alTop
    AutoSize = True
    BevelEdges = []
    TabOrder = 0
    object ToolBar1: TToolBar
      Left = 11
      Top = 2
      Width = 320
      Height = 40
      AutoSize = True
      ButtonHeight = 38
      Caption = 'ToolBar1'
      DragKind = dkDock
      DragMode = dmAutomatic
      EdgeBorders = []
      TabOrder = 0
      object Panel4: TPanel
        Left = 0
        Top = 2
        Width = 317
        Height = 38
        BevelOuter = bvNone
        TabOrder = 0
        object btn_cerrar: TSpeedButton
          Left = 3
          Top = 6
          Width = 25
          Height = 25
          Hint = 'Salir'
          Flat = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00330000000000
            03333377777777777F333301111111110333337F333333337F33330111111111
            0333337F333333337F333301111111110333337F333333337F33330111111111
            0333337F333333337F333301111111110333337F333333337F33330111111111
            0333337F3333333F7F333301111111B10333337F333333737F33330111111111
            0333337F333333337F333301111111110333337F33FFFFF37F3333011EEEEE11
            0333337F377777F37F3333011EEEEE110333337F37FFF7F37F3333011EEEEE11
            0333337F377777337F333301111111110333337F333333337F33330111111111
            0333337FFFFFFFFF7F3333000000000003333377777777777333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = btn_cerrarClick
        end
        object btn_copiar: TSpeedButton
          Left = 53
          Top = 6
          Width = 25
          Height = 25
          Hint = 'Copiar'
          Flat = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF003333330B7FFF
            FFB0333333777F3333773333330B7FFFFFB0333333777F3333773333330B7FFF
            FFB0333333777F3333773333330B7FFFFFB03FFFFF777FFFFF77000000000077
            007077777777777777770FFFFFFFF00077B07F33333337FFFF770FFFFFFFF000
            7BB07F3FF3FFF77FF7770F00F000F00090077F77377737777F770FFFFFFFF039
            99337F3FFFF3F7F777FF0F0000F0F09999937F7777373777777F0FFFFFFFF999
            99997F3FF3FFF77777770F00F000003999337F773777773777F30FFFF0FF0339
            99337F3FF7F3733777F30F08F0F0337999337F7737F73F7777330FFFF0039999
            93337FFFF7737777733300000033333333337777773333333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = btn_copiarClick
        end
        object btn_imprimir: TSpeedButton
          Left = 30
          Top = 6
          Width = 25
          Height = 25
          Hint = 'Imprimir'
          Flat = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00300000000000
            0003377777777777777308888888888888807F33333333333337088888888888
            88807FFFFFFFFFFFFFF7000000000000000077777777777777770F8F8F8F8F8F
            8F807F333333333333F708F8F8F8F8F8F9F07F333333333337370F8F8F8F8F8F
            8F807FFFFFFFFFFFFFF7000000000000000077777777777777773330FFFFFFFF
            03333337F3FFFF3F7F333330F0000F0F03333337F77773737F333330FFFFFFFF
            03333337F3FF3FFF7F333330F00F000003333337F773777773333330FFFF0FF0
            33333337F3F37F3733333330F08F0F0333333337F7337F7333333330FFFF0033
            33333337FFFF7733333333300000033333333337777773333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = btn_imprimirClick
        end
        object btn_open: TSpeedButton
          Left = 78
          Top = 6
          Width = 25
          Height = 25
          Hint = 'Abrir Reporte'
          Flat = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            8000008000000080800080000000800080008080000080808000C0C0C0000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD0000000000D
            DDDDD7777777777DDDDD003333333330DDDD778888888887DDDD0B0333333333
            0DDD7878888888887DDD0FB03333333330DD78878888888887DD0BFB03333333
            330D788878888888887D0FBFB0000000000078888777777777770BFBFBFBFB0D
            DDDD78888888887DDDDD0FBFBFBFBF0DDDDD78888888887DDDDD0BFB0000000D
            DDDD78887777777DDDDD0000DDDDDDDD000D7777DDDDDDDD777DDDDDDDDDDDDD
            D00DDDDDDDDDDDDDD77DDDDDDDDD0DDD0D0DDDDDDDDD7DDD7D7DDDDDDDDDD000
            DDDDDDDDDDDDD777DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = btn_openClick
        end
        object btn_save: TSpeedButton
          Left = 103
          Top = 6
          Width = 25
          Height = 25
          Hint = 'Guardar Reporte'
          Flat = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            8000008000000080800080000000800080008080000080808000C0C0C0000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDDDD0000000000000DDD0000000000000DD03300000088
            030DD08800000088080DD03300000088030DD08800000088080DD03300000088
            030DD08800000088080DD03300000000030DD08800000000080DD03333333333
            330DD08888888888880DD03300000000330DD08800000000880DD03088888888
            030DD08088888888080DD03088888888030DD08088888888080DD03088888888
            030DD08088888888080DD03088888888030DD08088888888080DD03088888888
            000DD08088888888000DD03088888888080DD08088888888080DD00000000000
            000DD00000000000000DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = btn_saveClick
        end
        object cb_Escala: TComboBox
          Left = 139
          Top = 8
          Width = 155
          Height = 21
          Hint = 'Zoom'
          ItemHeight = 13
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = cb_EscalaClick
          OnExit = cb_EscalaExit
          OnKeyPress = cb_EscalaKeyPress
        end
        object Panel3: TPanel
          Left = 302
          Top = 0
          Width = 15
          Height = 38
          Align = alRight
          BevelOuter = bvNone
          Caption = 'Panel3'
          TabOrder = 1
          object btn_pg_up: TSpeedButton
            Left = 0
            Top = 0
            Width = 15
            Height = 18
            Hint = 'Retroceso p'#225'gina'
            Enabled = False
            Glyph.Data = {
              EE000000424DEE000000000000007600000028000000110000000A0000000100
              0400000000007800000000000000000000001000000010000000000000000000
              8000008000000080800080000000800080008080000080808000C0C0C0000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
              333330000000300000003888FFFF300000003300000333733333300000003330
              00333337333330000000333303333333733330000000300000003888FFFF3000
              00003300000333733333300000003330003333373333304000A1333303333333
              733330000000333333333333333330000000}
            Margin = 1
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            OnClick = btn_pg_upClick
          end
          object btn_pg_dn: TSpeedButton
            Left = 0
            Top = 18
            Width = 15
            Height = 18
            Hint = 'Avance p'#225'gina'
            Enabled = False
            Glyph.Data = {
              EE000000424DEE000000000000007600000028000000110000000A0000000100
              0400000000007800000000000000000000001000000010000000000000000000
              8000008000000080800080000000800080008080000080808000C0C0C0000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
              333333333333333303333333F333333333333330003333373F33333333333300
              0003337333F333333333300000003777777F33333333333303333333F3333333
              33333330003333373F333333333333000003337333F333333333300000003777
              777F33333333333333333333333333333333}
            Margin = 1
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            OnClick = btn_pg_dnClick
          end
        end
      end
    end
  end
  object pnlBarras: TPanel
    Left = 0
    Top = 44
    Width = 186
    Height = 375
    Align = alLeft
    BevelOuter = bvNone
    Color = 15396586
    TabOrder = 1
    object Panel6: TPanel
      Left = 180
      Top = 6
      Width = 6
      Height = 369
      Align = alRight
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 0
    end
    object Panel5: TPanel
      Left = 0
      Top = 6
      Width = 6
      Height = 369
      Align = alLeft
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 1
    end
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 186
      Height = 6
      Align = alTop
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 2
    end
    object Panel7: TPanel
      Left = 6
      Top = 6
      Width = 174
      Height = 369
      Align = alClient
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 3
      object cpTareas: TCollapsablePanel
        Left = 0
        Top = 0
        Width = 174
        Height = 144
        Cursor = crHandPoint
        Align = alTop
        Color = clWhite
        ParentColor = False
        Caption = 'Tareas'
        BarColorStart = clActiveCaption
        BarColorEnd = clActiveCaption
        RightMargin = 0
        LeftMargin = 0
        ArrowLocation = cpaRightTop
        Style = cpsWinXP
        InternalSize = 121
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWhite
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = [fsBold]
        object Label1: TLabel
          Left = 32
          Top = 30
          Width = 133
          Height = 13
          Cursor = crHandPoint
          Caption = 'Enviar este reporte por mail'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          OnClick = mnu_emailClick
          OnMouseEnter = Label1MouseEnter
          OnMouseLeave = Label1MouseLeave
        end
        object Label2: TLabel
          Left = 32
          Top = 51
          Width = 108
          Height = 13
          Cursor = crHandPoint
          Caption = 'Convertir este reporte'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          OnClick = btn_saveClick
          OnMouseEnter = Label1MouseEnter
          OnMouseLeave = Label1MouseLeave
        end
        object Label3: TLabel
          Left = 32
          Top = 72
          Width = 89
          Height = 13
          Cursor = crHandPoint
          Caption = 'Copiar al escritorio'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          OnClick = mnu_desktopClick
          OnMouseEnter = Label1MouseEnter
          OnMouseLeave = Label1MouseLeave
        end
        object Label4: TLabel
          Left = 32
          Top = 93
          Width = 120
          Height = 13
          Cursor = crHandPoint
          Caption = 'Copiar a Mis Documentos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          OnClick = mnu_mydocumentsClick
          OnMouseEnter = Label1MouseEnter
          OnMouseLeave = Label1MouseLeave
        end
        object Label5: TLabel
          Left = 32
          Top = 114
          Width = 103
          Height = 13
          Cursor = crHandPoint
          Caption = 'Abrir Mis Documentos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          OnClick = Label5Click
          OnMouseEnter = Label1MouseEnter
          OnMouseLeave = Label1MouseLeave
        end
        object img_desktop: TImage
          Left = 11
          Top = 72
          Width = 16
          Height = 16
          Transparent = True
        end
        object img_mydocs: TImage
          Left = 11
          Top = 93
          Width = 16
          Height = 16
          Transparent = True
        end
        object img_openmydocs: TImage
          Left = 11
          Top = 113
          Width = 16
          Height = 16
          Transparent = True
        end
        object img_mail: TImage
          Left = 11
          Top = 30
          Width = 16
          Height = 16
          Transparent = True
        end
        object img_Save: TImage
          Left = 11
          Top = 50
          Width = 16
          Height = 16
          Picture.Data = {
            07544269746D6170F6000000424DF60000000000000076000000280000001000
            0000100000000100040000000000800000000000000000000000100000000000
            0000000000000000800000800000008080008000000080008000808000008080
            8000C0C0C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
            FF00DDDDDDDDDDDDDDDDDD0000000000000DD03300000088030DD03300000088
            030DD03300000088030DD03300000000030DD03333333333330DD03300000000
            330DD03088888888030DD03088888888030DD03088888888030DD03088888888
            030DD03088888888000DD03088888888080DD00000000000000DDDDDDDDDDDDD
            DDDD}
          Transparent = True
        end
      end
      object cpResumen: TCollapsablePanel
        Left = 0
        Top = 150
        Width = 174
        Height = 187
        Align = alTop
        Color = clWhite
        ParentColor = False
        Caption = 'Resumen'
        BarColorStart = clActiveCaption
        BarColorEnd = clActiveCaption
        RightMargin = 0
        LeftMargin = 0
        ArrowLocation = cpaRightTop
        Style = cpsWinXP
        InternalSize = 164
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWhite
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = [fsBold]
        object Label6: TLabel
          Left = 12
          Top = 109
          Width = 112
          Height = 13
          Caption = 'Tama'#241'o del reporte'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lblTamanio: TLabel
          Left = 12
          Top = 125
          Width = 3
          Height = 13
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label8: TLabel
          Left = 12
          Top = 145
          Width = 44
          Height = 13
          Caption = 'P'#225'ginas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lblPaginas: TLabel
          Left = 12
          Top = 161
          Width = 3
          Height = 13
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label10: TLabel
          Left = 12
          Top = 37
          Width = 32
          Height = 13
          Caption = 'T'#237'tulo'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lblTitulo: TLabel
          Left = 12
          Top = 53
          Width = 3
          Height = 13
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object lblFecha: TLabel
          Left = 12
          Top = 89
          Width = 3
          Height = 13
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label9: TLabel
          Left = 12
          Top = 73
          Width = 33
          Height = 13
          Caption = 'Fecha'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
      end
      object Panel8: TPanel
        Left = 0
        Top = 144
        Width = 174
        Height = 6
        Align = alTop
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 2
      end
    end
  end
  object pnlViewer: TPanel
    Left = 186
    Top = 44
    Width = 441
    Height = 375
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
  end
  object Reader: TppArchiveReader
    DeviceType = 'Screen'
    SuppressOutline = False
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = False
    Left = 208
    Top = 160
    Version = '7.03'
  end
  object OpenDialog: TOpenDialog
    DefaultExt = 'rpt'
    Filter = 'Archivos de Reporte|*.rpt|Todos los archivos|*.*'
    Options = [ofHideReadOnly, ofNoChangeDir, ofPathMustExist, ofFileMustExist]
    Left = 240
    Top = 160
  end
  object SaveDialog: TSaveDialog
    DefaultExt = 'rpt'
    Filter = 
      'Archivos de Reporte (*.rpt)|*.rpt|Archivos de texto (*.txt)|*.tx' +
      't|Archivos de Microsoft Excel (*.xls)|*.xls|Archivos de texto en' +
      'riquecido (*.rtf)|*.rtf|Archivos de Adobe Acrobat (*.pdf)|*.pdf'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofNoChangeDir, ofPathMustExist, ofNoReadOnlyReturn]
    Left = 272
    Top = 160
  end
  object PopupMenu1: TPopupMenu
    Images = ImageList1
    Left = 304
    Top = 160
    object Abrir1: TMenuItem
      Caption = '&Abrir...'
      OnClick = btn_openClick
    end
    object Guardarcomo1: TMenuItem
      Caption = '&Guardar como...'
      OnClick = btn_saveClick
    end
    object Imprimir1: TMenuItem
      Caption = '&Imprimir...'
      OnClick = btn_imprimirClick
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Copiar1: TMenuItem
      Caption = 'C&opiar'
      OnClick = btn_copiarClick
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object Enviara1: TMenuItem
      Caption = 'En&viar a'
      object mnu_email: TMenuItem
        Caption = 'Destinatario de &Correo'
        ImageIndex = 0
        OnClick = mnu_emailClick
      end
      object mnu_desktop: TMenuItem
        Caption = '&Escritorio'
        ImageIndex = 1
        OnClick = mnu_desktopClick
      end
      object mnu_mydocuments: TMenuItem
        Caption = 'MyDocuments'
        ImageIndex = 2
        OnClick = mnu_mydocumentsClick
      end
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Zoom1: TMenuItem
      Caption = 'Zoom'
      object mnu_zoom010: TMenuItem
        Caption = '10%'
        OnClick = mnu_zoomClick
      end
      object mnu_zoom025: TMenuItem
        Caption = '25%'
        OnClick = mnu_zoomClick
      end
      object mnu_zoom050: TMenuItem
        Caption = '50%'
        OnClick = mnu_zoomClick
      end
      object mnu_zoom075: TMenuItem
        Caption = '75%'
        OnClick = mnu_zoomClick
      end
      object mnu_zoom100: TMenuItem
        Caption = '100%'
        OnClick = mnu_zoomClick
      end
      object mnu_zoom200: TMenuItem
        Caption = '200%'
        OnClick = mnu_zoomClick
      end
      object N4: TMenuItem
        Caption = '-'
      end
      object AnchodePgina1: TMenuItem
        Caption = '&Ancho de P'#225'gina'
        OnClick = AnchodePgina1Click
      end
      object PginaEntera1: TMenuItem
        Caption = 'P'#225'gina &Entera'
        OnClick = PginaEntera1Click
      end
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Cerrar1: TMenuItem
      Caption = '&Cerrar'
      OnClick = btn_cerrarClick
    end
  end
  object ImageList1: TImageList
    Left = 336
    Top = 160
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001001000000000000008
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000C018C018C018C018
      C018C018C018C018C018C018C018C018C0180000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009301930193017902524A524A524A
      524A524A524A524A9319931993199319000000000000604E604E604E604E604E
      604E604E604E604E604E604E604EC018C0180000000000000000000000000000
      0000000000000000000000000000000000000000104200000000000000000000
      00000000000000000000000000000000000079023F337902F97FF97F3367F97F
      33673367B2563367B25693193F339319000000000000604EFF7F337FF37F337F
      F37F337F337F337F337F2C67604EC018C0180000000000000000000000000000
      0000000000000000000000000000000000000000104210421042104210421042
      10421042104210421042104210420000000079027902F97FF97F000000000000
      000000000000B2563367B2569319931900000000604EFF7FF37FF37F337FF37F
      337FF37F337F337F337F2C67C018604EC0180000000000000000000000000000
      0000000000000000000000000000000000000000104218631863186318631863
      1863186318631863186318631042000000007902F97FF97FF97F524AF97FF97F
      F97FF97F00003367B2563367B256931900000000604EFF7FF37FF37FF37FF37F
      F37F337FF37F337F337F604EC018604EC0180000000000000000000000000000
      0000000000000000000000000000000000000000104200421863186318630042
      004200421863186318630042104200000000B256F97FF97FF97F524AFF7FFF7F
      FF7FF97F000033673367B2563367524A0000604EFF7FF37FF37FF37FF37F337F
      F37FF37F337FF37F2C67C0187F4E0000C0180000000000000000000000000000
      00000000000000000000000000000000000000001042E07F0042186300420000
      000000000042186300421863104200000000B256F97FF97FF97F524AFF7FFF7F
      FF7FF97F0000F97F33673367B256524A0000604EFF7FF37FF37FF37FF37FF37F
      F37F337FF37F337F2C67C0183F673F6700000000000000000000000000000000
      00000000000000000000000000000000000000001042FF7FFF7F00420000FF7F
      FF7FE07F0000004218631863104200000000B256FF7FF97FF97F524AFF7F0000
      FF7FF97F00003367F97FB2563367524A0000604E604E604E604E604E604E604E
      604E604E604E604E604E7F4EFF673F6700000000000000000000000000000000
      00000000000000000000000000000000000000001042E07F18630000FF7FE07F
      FF7FFF7FFF7F000000421863104200000000B256FF7FFF7FF97F524AFF7F6C4E
      804D00000000F97F3367F97FB256524A0000000000429331FF673F6713187F4E
      3F6713187F4E3F673F673F673F670000C0180000000000000000000000000000
      0000000000000000000000000000000000000000104218630000E07FFF7FFF7F
      FF7FE07FFF7FFF7F0000004210420000000093013367FF7FFF7F524AFF7F6066
      207F804D0000B256F97F3367336793190000000000422C679331FF67FF7FD918
      FF4FFF7FD918FF7FFF673F6700002C67C0180000000000000000000000000000
      000000000000000000000000000000000000000010420000FF7FFF7FFF7FE07F
      FF7FFF7FFF7FE07FFF7F0000104200000000790293013367FF7F524A524A524A
      6066F37F804D0000B256F97F931993010000000000422C672C679331FF67FF7F
      9F01FF4FFF7FFF673F670000604E604E00000000000000000000000000000000
      00000000000000000000000000000000000000001042FF7FFF7FE07FFF7FFF7F
      FF7FE07FFF7FFF7FFF7FE07F0000000000007902790293013367FF7FFF7FF97F
      F97F6066207F804D000093193F33930100000000000000422C672C679331FF67
      FF7F7F1AFF673F67000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000104210421042104210421042
      10421042104210421042104210420000000079023F33790293013367FF7FFF7F
      F97FFF7F6066F37F804D00007902930100000000000000000042004200429331
      FF67FF7F3F670000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000079027902790279029301B256B256
      B256B256B2566066207F804D0000B25600000000000000000000000000000000
      9331FF6700000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000006066797E004C000000000000000000000000000000000000
      0000933100000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFF0000FFFF0000E0000000
      FFFF0000C000000080010000C000000080010000800000008001000080000000
      8001000000000000800100000000000080010000000000008001000080000000
      8001000080000000800100008001000080010000C00F000080010000E01F0000
      FFFF0000FE3F0000FFFFFFE3FF7F000000000000000000000000000000000000
      000000000000}
  end
end
