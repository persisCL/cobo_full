{-----------------------------------------------------------------------------
 Unit Name: FrmNoFacturarTransitosDeCuenta
 Author:    ggomez
 Purpose: Permitir colocar una cuenta de un convenio para que se eximan de
    facturaci�n los tr�nsitos de la misma.

 History:

Author      : ggomez
Date        : 04/05/2005
Description : Agregu� el control de que las fechas de la eximici�n no se solapen
    con las fechas de alguna bonificaci�n.

 Revision 2
 	Author: ggomez
    Date: 19/05/2005
    Description: Agregu� el control de que el rango de la eximici�n que se
    	est� por ingresar NO se solape con las fechas de alguna otra
        eximici�n.

 Revision 3
 	Author: ggomez
    Date: 19/05/2005
    Description: Correg� un bug de que no permit�a modificar los datos de una
        eximici�n vigente que ya exist�a en la BD.

 Revision 4
 	Author: mbecerra
    Date: 14-Septiembre-2009
    Description:		(Ref. SS 832)
            	El cliente solicita que no se muestre el digito verificador
-----------------------------------------------------------------------------}

unit FrmNoFacturarTransitosDeCuenta;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, Validate, DateEdit,
  RStrings, UtilProc, VariantComboBox, DMConnection, PeaProcs, PeaTypes,
  Util, DB, ADODB;

type

  TFormNoFacturarTransitosDeCuenta = class(TForm)
    gb_DatosVehiculo: TGroupBox;
    lbl_Patente: TLabel;
    lbl_Televia: TLabel;
    nb_Botones: TNotebook;
    pnlDatos: TPanel;
    lbl_FechaModificacion: TLabel;
    lbl_Usuario: TLabel;
    cb_Motivos: TVariantComboBox;
    lbl_Motivo: TLabel;
    lbl_FechaFin: TLabel;
    txt_FechaFin: TDateEdit;
    txt_FechaInicio: TDateEdit;
    lbl_FechaDesde: TLabel;
    lblPatente: TLabel;
    lblTelevia: TLabel;
    lblCodUsuario: TLabel;
    lblFechaModif: TLabel;
    btn_Aceptar: TButton;
    btn_Cancelar: TButton;
    btn_Salir: TButton;
    procedure btn_CancelarClick(Sender: TObject);
    procedure btn_AceptarClick(Sender: TObject);
    procedure btn_SalirClick(Sender: TObject);
  private
    { Private declarations }
    FCodigoConvenio,
    FIndiceVehiculo: Integer;
    FEximirFacturacion: TEximirFacturacion;
    FBonificarFacturacion: TBonificarFacturacion;
    FPatenteVehiculo,
    FDigitoVerificadorPatente,
    FNroTelevia: AnsiString;
    FModo: TStateConvenio;

    procedure HabilitarControlesIngresoDatos(Valor: Boolean);
    procedure MostrarDatos;
    function ExisteEximicion(FechaInicio, FechaFin: TDate): Boolean;
    function ExisteBonificacion(FechaInicio, FechaFin: TDate): Boolean;
    function SolapaConBonificacion: Boolean;

  public
    { Public declarations }
    function Inicializar(Modo: TStateConvenio; CodigoConvenio, IndiceVehiculo: Integer;
        EximirFacturacion: TEximirFacturacion;
        BonificarFacturacion: TBonificarFacturacion;
        PatenteVehiculo, DigitoVerificadorPatente, NroTelevia: AnsiString): Boolean;

    function GetEximirFacturacion: TEximirFacturacion;
  end;

var
  FormNoFacturarTransitosDeCuenta: TFormNoFacturarTransitosDeCuenta;

implementation

{$R *.dfm}

{ TFormNoFacturarTransitosDeCuenta }

{-----------------------------------------------------------------------------
  Procedure: TFormNoFacturarTransitosDeCuenta.Incializar
  Author:    ggomez
  Date:      04-May-2005
  Arguments: Modo: TStateConvenio,
    CodigoConvenio, IndiceVehiculo: Integer;
    EximirFacturacion: TEximirFacturacion. Datos de la eximici�n de facturaci�n.
    BonificacionFacturacion: TBonificacionFacturacion. Datos de la bonificaci�n de facturaci�n. 
    PatenteVehiculo, DigitoVerificadorPatente, NroTelevia: AnsiString
  Result:    Boolean
-----------------------------------------------------------------------------}
function TFormNoFacturarTransitosDeCuenta.Inicializar(Modo: TStateConvenio;
    CodigoConvenio, IndiceVehiculo: Integer;
    EximirFacturacion: TEximirFacturacion;
    BonificarFacturacion: TBonificarFacturacion; PatenteVehiculo,
    DigitoVerificadorPatente, NroTelevia: AnsiString): Boolean;
resourcestring
    STR_NO_FACTURAR_TRANSITOS = 'No Facturar Tr�nsitos';
begin
    try
        FModo := Modo;
        FCodigoConvenio := CodigoConvenio;
        FIndiceVehiculo := IndiceVehiculo;
        FEximirFacturacion := EximirFacturacion;
        FBonificarFacturacion := BonificarFacturacion;

        FPatenteVehiculo := Trim(PatenteVehiculo);
        FDigitoVerificadorPatente := Trim(DigitoVerificadorPatente);
        FNroTelevia := Trim(NroTelevia);

        lblPatente.Caption := FPatenteVehiculo; //REV.4 + '-' + FDigitoVerificadorPatente;
        lblTelevia.Caption := FNroTelevia;

        (* Cargar los motivos. *)
        CargarMotivosTransitoNoFacturable(DMConnections.BaseCAC, cb_Motivos);

        (* Habilitar los controles de ingreso de datos seg�n el Modo con el que
        se llama al form. *)
        HabilitarControlesIngresoDatos(FModo <> scNormal);

        (* Mostrar los datos de la eximici�n de facturaci�n para la cuenta. *)
        MostrarDatos;

        Result := True;
    except
        on e: Exception do begin
    		Result := False;
            MsgBoxErr(Format(MSG_ERROR_INICIALIZACION, [STR_NO_FACTURAR_TRANSITOS]),
                E.Message, Caption, MB_ICONSTOP);
        end;

    end;
end;

procedure TFormNoFacturarTransitosDeCuenta.btn_CancelarClick(
  Sender: TObject);
begin
    Close;
end;

procedure TFormNoFacturarTransitosDeCuenta.btn_AceptarClick(
  Sender: TObject);
resourcestring
    MSG_FECHA_DEBE_CONTENER_UN_VALOR = 'La Fecha debe contener un valor.';
    MSG_MOTIVO_DEBE_CONTENER_UN_VALOR_VALIDO = 'El Motivo debe ser uno v�lido.';
    MSG_FECHAFIN_DEBE_SER_POSTERIOR_A_FECHAINICIO = 'La Fecha Hasta debe ser posterior a la Fecha Desde';
    MSG_ERROR_EXISTE_EXIMICION_BD = 'El veh�culo tiene una eximici�n almacenada en la base de datos,'
        + CRLF + 'cuyas fechas se intercalan con las de esta eximici�n.'
        + CRLF + 'No se permite Eximir la Facturaci�n.';
    MSG_ERROR_EXISTE_BONIFICACION_BD = 'El veh�culo tiene una bonificaci�n almacenada en la base de datos,'
        + CRLF + 'cuyas fechas se intercalan con las de la eximici�n.'
        + CRLF + 'No se permite Eximir la Facturaci�n.';
    MSG_ERROR_EXISTE_BONIFICACION_MEMORIA = 'El veh�culo tiene una bonificaci�n entre los datos modificados recientemente,'
        + CRLF + 'cuyas fechas se intercalan con las de la eximici�n.'
        + CRLF + 'No se permite Eximir la Facturaci�n.';
begin
    (* Si el form NO se llam� para consultar, realizar el control de los
    datos. *)
    if FModo <> scNormal then begin
        (* Controlar los datos
        FechaInicio debe contener un valor.
        Si FechaFin se puede editar, debe ser posterior a la FechaInicio.
        Motivo debe contener un valor v�lido. *)
        if not (ValidateControls([txt_FechaInicio,
                txt_FechaFin,
                cb_Motivos],
                [not (txt_FechaInicio.IsEmpty),
                (txt_FechaFin.IsEmpty or txt_FechaFin.ReadOnly) or (txt_FechaFin.Date > txt_FechaInicio.Date),
                cb_Motivos.ItemIndex > 0],
                Caption,
                [MSG_FECHA_DEBE_CONTENER_UN_VALOR,
                MSG_FECHAFIN_DEBE_SER_POSTERIOR_A_FECHAINICIO,
                MSG_MOTIVO_DEBE_CONTENER_UN_VALOR_VALIDO])) then Exit;

        (* Si la FechaIncio es anterior a la FechaInicioOriginal, Controlar que
        no exista una eximici�n cuyo rango de fechas comparta parte o todo
        el rango de fechas de esta eximici�n.*)
        if (Trunc(txt_FechaInicio.Date) < FEximirFacturacion.FechaInicioOriginal)
                and (ExisteBonificacion(Trunc(txt_FechaInicio.Date), Trunc(txt_FechaFin.Date))) then begin
            MsgBox(MSG_ERROR_EXISTE_EXIMICION_BD, Caption, MB_ICONSTOP);
            Exit;
        end;

        (* Si la eximici�n NO exist�a en la BD, Controlar que no exista una
        eximici�n cuyo rango de fechas comparta parte o todo el rango de fechas
        de esta eximici�n.
        Si la eximici�n exist�a en la BD, es porque se est� haciendo una
        modificaci�n de los datos. *)
        if (not FEximirFacturacion.ExisteEnBaseDeDatos) and (ExisteEximicion(Trunc(txt_FechaInicio.Date), Trunc(txt_FechaFin.Date))) then begin
            MsgBox(MSG_ERROR_EXISTE_EXIMICION_BD, Caption, MB_ICONSTOP);
            Exit;
        end;

        (* Controlar que no exista una bonificaci�n cuyo rango de fechas
        comparta parte o todo el rango de fechas de la eximici�n. *)
        if ExisteBonificacion(Trunc(txt_FechaInicio.Date), Trunc(txt_FechaFin.Date)) then begin
            MsgBox(MSG_ERROR_EXISTE_BONIFICACION_BD, Caption, MB_ICONSTOP);
            Exit;
        end;
        if SolapaConBonificacion then begin
            MsgBox(MSG_ERROR_EXISTE_BONIFICACION_MEMORIA, Caption, MB_ICONSTOP);
            Exit;
        end;

        (* Actualizar los datos de eximici�n de facturaci�n. *)
        with FEximirFacturacion do begin
            FechaInicio := Trunc(txt_FechaInicio.Date);
            CodigoMotivoNoFacturable := cb_Motivos.Value;
            FechaFinalizacion := Trunc(txt_FechaFin.Date);
        end; // with
    end;
    ModalResult := mrOk;
end;

{-----------------------------------------------------------------------------
  Procedure: TFormNoFacturarTransitosDeCuenta.HabilitarControlesIngresoDatos
  Author:    ggomez
  Date:      04-May-2005
  Arguments: Valor: Boolean
  Result:    None
  Descrption: Seg�n el valor del par�metro habilita o deshabilita los controles
    para ingresar los datos.
-----------------------------------------------------------------------------}
procedure TFormNoFacturarTransitosDeCuenta.HabilitarControlesIngresoDatos(
  Valor: Boolean);
begin
    EnableControlsInContainer(pnlDatos, Valor);

    (* Si Valor es True, mostrar los botonos Aceptar y Cancelar. *)
    nb_Botones.PageIndex := iif(Valor, 0, 1);
end;

{-----------------------------------------------------------------------------
  Procedure: TFormNoFacturarTransitosDeCuenta.MostrarDatos
  Author:    ggomez
  Date:      04-May-2005
  Arguments: None
  Result:    None
  Description: Muestra los datos de la eximici�n de facturaci�n para la cuenta
    para la que se llam� el form.
    En el caso de que la cuenta no tenga datos asociados, se mostrar�n los datos
    como para ingresar una nueva eximici�n.
-----------------------------------------------------------------------------}
procedure TFormNoFacturarTransitosDeCuenta.MostrarDatos;
begin
    // Incializar los valores de los controles.
    with FEximirFacturacion do begin
        txt_FechaInicio.Date := FechaInicio;
        txt_FechaFin.Date := FechaFinalizacion;
        cb_Motivos.Value := CodigoMotivoNoFacturable;
        LblCodUsuario.Caption := Usuario;
        lblFechaModif.Caption := EmptyStr;
        if FechaHoraModificacion > 0 then
            lblFechaModif.Caption := FormatDateTime('dd/mm/yyyy hh:nn:ss', FechaHoraModificacion);

        txt_FechaInicio.ReadOnly := not FechaInicioEditable;
        txt_FechaFin.ReadOnly := not FechaFinalizacionEditable;
        (* Si la Fecha de Fin no se permite editar, NO permitir editar el motivo. *)
        cb_Motivos.Enabled := FechaFinalizacionEditable;
    end; // with

end;

procedure TFormNoFacturarTransitosDeCuenta.btn_SalirClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Procedure: TFormNoFacturarTransitosDeCuenta.GetEximirFacturacion
  Author:    ggomez
  Date:      05-May-2005
  Arguments: None
  Result:    TEximirFacturacion
-----------------------------------------------------------------------------}
function TFormNoFacturarTransitosDeCuenta.GetEximirFacturacion: TEximirFacturacion;
begin
    Result := FEximirFacturacion;
end;

{-----------------------------------------------------------------------------
  Procedure: TFormNoFacturarTransitosDeCuenta.ExisteBonificacion
  Author:    ggomez
  Date:      06-May-2005
  Arguments: FechaInicio, FechaFin: TDate
  Result:    Boolean
  Description: Retorna True si existe una bonificaci�n para el vehiculo cuyo
    rango de fechas comparta parte o todo el rango de fechas pasado como
    par�metro.
-----------------------------------------------------------------------------}
function TFormNoFacturarTransitosDeCuenta.ExisteBonificacion(
  FechaInicio, FechaFin: TDate): Boolean;
var
    sp_ExisteBonificacion: TADOStoredProc;
begin
    sp_ExisteBonificacion := TADOStoredProc.Create(Self);
    try
        with sp_ExisteBonificacion, Parameters do begin
            Connection := DMConnections.BaseCAC;
            ProcedureName := 'ExisteBonificacion';
            Parameters.Refresh;

            ParamByName('@CodigoConvenio').Value := FCodigoConvenio;
            ParamByName('@IndiceVehiculo').Value := FIndiceVehiculo;
            ParamByName('@FechaInicio').Value := FechaInicio;
            if FechaFin < 0 then begin
                ParamByName('@FechaFinalizacion').Value := Null;
            end else begin
                ParamByName('@FechaFinalizacion').Value := FechaFin;
            end;

            ExecProc;

            Result := (ParamByName('@RETURN_VALUE').Value = 1);
        end; // with
    finally
        sp_ExisteBonificacion.Free;
    end; // finally

end;

{-----------------------------------------------------------------------------
  Procedure: TFormNoFacturarTransitosDeCuenta.SolapaConBonificacion
  Author:    ggomez
  Date:      06-May-2005
  Arguments: None
  Result:    Boolean
  Description: Retorna True si las fechas de la bonificaci�n almacenada en
    memoria comparte parte o todo el rango de fechas de la eximicion.
-----------------------------------------------------------------------------}
function TFormNoFacturarTransitosDeCuenta.SolapaConBonificacion: Boolean;
begin
    Result := False;

    (* Si NO hay datos de bonificacion, NO testear pues no solapan. *)
    if (FBonificarFacturacion.FechaInicio < 0) then begin
        Exit;
    end; // if

    if txt_FechaFin.Date < 0 then begin
        (* NO hay fecha de Fin de Eximici�n. *)

        if (FBonificarFacturacion.FechaFinalizacion < 0) then begin
            (* NO hay Fecha de Fin de Bonificaci�n. S�lo testar por fecha de
            Inicio, luego, Solapan seguro. *)

            Result := True;
        end else begin
            (* HAY Fecha de Fin de Bonificaci�n. *)

            Result := (txt_FechaInicio.Date <= FBonificarFacturacion.FechaFinalizacion);
        end; // else if (FBonificarFacturacion.FechaFinalizacion < 0)
    end else begin
        (* HAY fecha de Fin de Eximici�n. *)

        if (FBonificarFacturacion.FechaFinalizacion < 0) then begin
            (* NO hay Fecha de Fin de Bonificaci�n. *)

            Result := (FBonificarFacturacion.FechaInicio <= txt_FechaFin.Date);

        end else begin
            (* HAY Fecha de Fin de Bonificaci�n. *)

            Result := ((txt_FechaInicio.Date >= FBonificarFacturacion.FechaInicio) and (txt_FechaInicio.Date <= FBonificarFacturacion.FechaFinalizacion))
                        or ((txt_FechaFin.Date >= FBonificarFacturacion.FechaInicio) and (txt_FechaFin.Date <= FBonificarFacturacion.FechaFinalizacion))
                        or ((FBonificarFacturacion.FechaInicio >= txt_FechaInicio.Date) and (FBonificarFacturacion.FechaInicio <= txt_FechaFin.Date))
                        or ((FBonificarFacturacion.FechaFinalizacion >= txt_FechaInicio.Date) and (FBonificarFacturacion.FechaInicio <= txt_FechaFin.Date));

        end; // else if (FBonificarFacturacion.FechaFinalizacion < 0)

    end; // else if txt_FechaFin.Date < 0
end;

function TFormNoFacturarTransitosDeCuenta.ExisteEximicion(FechaInicio,
  FechaFin: TDate): Boolean;
var
    sp_ExisteEximicion: TADOStoredProc;
begin
    sp_ExisteEximicion := TADOStoredProc.Create(Self);
    try
        with sp_ExisteEximicion, Parameters do begin
            Connection := DMConnections.BaseCAC;
            ProcedureName := 'ExisteEximicion';

            Parameters.Refresh;
            ParamByName('@CodigoConvenio').Value := FCodigoConvenio;
            ParamByName('@IndiceVehiculo').Value := FIndiceVehiculo;
            ParamByName('@FechaInicio').Value := FechaInicio;
            if FechaFin < 0 then begin
                ParamByName('@FechaFinalizacion').Value := Null;
            end else begin
                ParamByName('@FechaFinalizacion').Value := FechaFin;
            end;
            ExecProc;
            Result := (ParamByName('@RETURN_VALUE').Value = 1);
        end; // with
    finally
        sp_ExisteEximicion.Free;
    end; // finally
end;

end.
