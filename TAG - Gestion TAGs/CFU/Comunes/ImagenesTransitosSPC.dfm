object frmImagenesTransitosSPC: TfrmImagenesTransitosSPC
  Left = 0
  Top = 0
  Caption = 'Imagenes del Tr'#225'nsito'
  ClientHeight = 317
  ClientWidth = 425
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object pgcImages: TPageControl
    Left = 0
    Top = 0
    Width = 425
    Height = 317
    ActivePage = tsContexto3
    Align = alClient
    TabOrder = 0
    ExplicitLeft = -548
    ExplicitTop = -76
    ExplicitWidth = 973
    ExplicitHeight = 393
    object tsFrontal1: TTabSheet
      Caption = ' Frontal 1 '
      ExplicitWidth = 965
      ExplicitHeight = 365
      object imgFrontal1: TImage
        Left = 0
        Top = 0
        Width = 417
        Height = 289
        Align = alClient
        Stretch = True
        ExplicitLeft = 2
        ExplicitWidth = 965
        ExplicitHeight = 365
      end
      object lblSinImgFrontal1: TLabel
        Left = 112
        Top = 104
        Width = 204
        Height = 33
        Caption = 'No existe imagen'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGreen
        Font.Height = -27
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
    end
    object tsFrontal2: TTabSheet
      Caption = ' Frontal 2 '
      ImageIndex = 1
      object imgFrontal2: TImage
        Left = 0
        Top = 0
        Width = 417
        Height = 289
        Align = alClient
        Stretch = True
        ExplicitWidth = 618
        ExplicitHeight = 382
      end
      object lblSinImgFrontal2: TLabel
        Left = 112
        Top = 104
        Width = 204
        Height = 33
        Caption = 'No existe imagen'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGreen
        Font.Height = -27
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
    end
    object tsContexto1: TTabSheet
      Caption = ' Contexto 1 '
      ImageIndex = 2
      object imgContexto1: TImage
        Left = 0
        Top = 0
        Width = 417
        Height = 289
        Align = alClient
        Stretch = True
        ExplicitWidth = 411
        ExplicitHeight = 274
      end
      object lblSinImgContexto1: TLabel
        Left = 112
        Top = 104
        Width = 204
        Height = 33
        Caption = 'No existe imagen'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGreen
        Font.Height = -27
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
    end
    object tsContexto2: TTabSheet
      Caption = ' Contexto 2 '
      ImageIndex = 3
      object imgContexto2: TImage
        Left = 0
        Top = 0
        Width = 417
        Height = 289
        Align = alClient
        Stretch = True
        ExplicitWidth = 618
        ExplicitHeight = 382
      end
      object lblSinImgContexto2: TLabel
        Left = 112
        Top = 104
        Width = 204
        Height = 33
        Caption = 'No existe imagen'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGreen
        Font.Height = -27
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
    end
    object tsContexto3: TTabSheet
      Caption = ' Contexto 3 '
      ImageIndex = 4
      object imgContexto3: TImage
        Left = 0
        Top = 0
        Width = 417
        Height = 289
        Align = alClient
        Stretch = True
        ExplicitWidth = 618
        ExplicitHeight = 382
      end
      object lblSinImgContexto3: TLabel
        Left = 112
        Top = 104
        Width = 204
        Height = 33
        Caption = 'No existe imagen'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGreen
        Font.Height = -27
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
    end
    object tsContexto4: TTabSheet
      Caption = ' Contexto 4 '
      ImageIndex = 5
      object imgContexto4: TImage
        Left = 0
        Top = 0
        Width = 417
        Height = 289
        Align = alClient
        Stretch = True
        ExplicitWidth = 618
        ExplicitHeight = 382
      end
      object lblSinImgContexto4: TLabel
        Left = 112
        Top = 104
        Width = 204
        Height = 33
        Caption = 'No existe imagen'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGreen
        Font.Height = -27
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
    end
    object tsContexto5: TTabSheet
      Caption = ' Contexto 5 '
      ImageIndex = 6
      object imgContexto5: TImage
        Left = 0
        Top = 0
        Width = 417
        Height = 289
        Align = alClient
        Stretch = True
        ExplicitWidth = 618
        ExplicitHeight = 382
      end
      object lblSinImgContexto5: TLabel
        Left = 112
        Top = 104
        Width = 204
        Height = 33
        Caption = 'No existe imagen'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGreen
        Font.Height = -27
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
    end
    object tsContexto6: TTabSheet
      Caption = ' Contexto 6 '
      ImageIndex = 7
      object imgContexto6: TImage
        Left = 0
        Top = 0
        Width = 417
        Height = 289
        Align = alClient
        Stretch = True
        ExplicitWidth = 618
        ExplicitHeight = 382
      end
      object lblSinImgContexto6: TLabel
        Left = 112
        Top = 104
        Width = 204
        Height = 33
        Caption = 'No existe imagen'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGreen
        Font.Height = -27
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
    end
  end
end
