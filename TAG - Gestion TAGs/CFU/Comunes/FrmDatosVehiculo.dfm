object FormDatosVehiculo: TFormDatosVehiculo
  Left = 345
  Top = 131
  BorderStyle = bsDialog
  Caption = 'Datos del Veh'#237'culo'
  ClientHeight = 449
  ClientWidth = 474
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnlBotones: TPanel
    Left = 0
    Top = 408
    Width = 474
    Height = 41
    Align = alBottom
    TabOrder = 0
    object btnAceptar: TDPSButton
      Left = 319
      Top = 8
      Caption = '&Aceptar'
      Default = True
      TabOrder = 0
      OnClick = btnAceptarClick
    end
    object btnCancelar: TDPSButton
      Left = 395
      Top = 8
      Cancel = True
      Caption = '&Cancelar'
      ModalResult = 2
      TabOrder = 1
      OnClick = btnCancelarClick
    end
  end
  object GroupBox2: TGroupBox
    Left = 5
    Top = 10
    Width = 462
    Height = 215
    Caption = ' Datos del Veh'#237'culo '
    TabOrder = 1
    object Label38: TLabel
      Left = 20
      Top = 159
      Width = 61
      Height = 13
      Caption = 'Categor'#237'a:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblCategoria: TLabel
      Left = 101
      Top = 159
      Width = 146
      Height = 13
      Caption = 'Descripci'#243'n de Categor'#237'a'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label49: TLabel
      Left = 134
      Top = 190
      Width = 75
      Height = 13
      Caption = '&Largo adicional:'
      FocusControl = txt_LargoAdicionalVehiculo
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label1: TLabel
      Left = 291
      Top = 190
      Width = 59
      Height = 13
      Caption = 'cent'#237'metros.'
      FocusControl = txt_LargoAdicionalVehiculo
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object chk_acoplado: TCheckBox
      Left = 22
      Top = 189
      Width = 101
      Height = 17
      Caption = 'Tiene Acoplado'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      TabOrder = 2
      OnClick = chk_acopladoClick
    end
    object txt_LargoAdicionalVehiculo: TNumericEdit
      Left = 212
      Top = 182
      Width = 76
      Height = 21
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      Decimals = 0
    end
    inline FDatosVehiculo: TFrameDatosVehiculo
      Left = 15
      Top = 12
      Width = 333
      Height = 139
      TabOrder = 0
      inherited Label20: TLabel
        Top = 120
      end
      inherited lblTipoVehiculo: TLabel
        Top = 65
      end
      inherited Label47: TLabel
        Top = 95
      end
      inherited Label21: TLabel
        Top = 95
      end
      inherited txt_DescripcionModelo: TEdit
        Left = 336
        Top = 144
        Width = 228
        Visible = False
      end
      inherited cb_TipoVehiculo: TVariantComboBox
        Top = 61
      end
      inherited txt_anio: TNumericEdit
        Top = 88
      end
      inherited cb_color: TComboBox
        Top = 87
      end
    end
    object cbModelos: TComboBox
      Left = 103
      Top = 128
      Width = 237
      Height = 21
      ItemHeight = 13
      TabOrder = 1
      Text = 'cbModelos'
    end
  end
  object GroupBox4: TGroupBox
    Left = 5
    Top = 227
    Width = 462
    Height = 66
    Caption = 'Otros datos'
    TabOrder = 2
    object chk_contag: TCheckBox
      Left = 33
      Top = 18
      Width = 180
      Height = 17
      Caption = 'Equipar el veh'#237'culo con un TAG'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
    object chk_detallepasadas: TCheckBox
      Left = 33
      Top = 36
      Width = 260
      Height = 17
      Caption = 'Imprimir un detalle de pasadas para este veh'#237'culo'
      TabOrder = 1
    end
  end
  object gbInformacionComercial: TGroupBox
    Left = 6
    Top = 296
    Width = 461
    Height = 101
    Caption = ' Informaci'#243'n Comercial  '
    TabOrder = 3
    object Label22: TLabel
      Left = 9
      Top = 59
      Width = 89
      Height = 13
      Caption = 'Plan Comercial:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblInformacion: TLabel
      Left = 361
      Top = 59
      Width = 64
      Height = 13
      Cursor = crHandPoint
      Hint = 'Alternativas sobre planes comerciales'
      Caption = 'Informaci'#243'n...'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnClick = lblInformacionClick
    end
    object Label23: TLabel
      Left = 9
      Top = 32
      Width = 64
      Height = 13
      Caption = 'Utilizaci'#243'n:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object cb_PlanComercial: TComboBox
      Left = 102
      Top = 51
      Width = 244
      Height = 21
      Style = csDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 1
    end
    object cb_TipoCliente: TComboBox
      Left = 102
      Top = 27
      Width = 243
      Height = 21
      Style = csDropDownList
      Color = 16444382
      ItemHeight = 13
      TabOrder = 0
    end
  end
end
