unit FrmNormalizarDomicilio;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, Util, Peaprocs, UtilProc, DB,
  ADODB, BuscaTab, DmiCtrls, peatypes, UtilDB, ListBoxEx, DBListEx, Buttons, DMConnection,
  FrmEditarDomicilio,FrmBuscaCalle;

ResourceString
    MSG_NORMALIZADO_CAPTION = 'Error en el normalizado de domicilios.';
    MSG_NORMALIZADO_BUSCAR = 'No se puede cargar los domicilios no normalizados.';
    MSG_NORMALIZADO_ERROR = 'No se pudo normalizar el domicilio.';
type
  TFormNormalizarDomicilio = class(TForm)
    pnl_botones: TPanel;
    ObtenerDomiciliosDesnormalizado: TADOStoredProc;
    DSObtenerDomiciliosDesnormalizado: TDataSource;
    Panel1: TPanel;
    pnl_grilla: TGroupBox;
    dbl_Domicilios: TDBListEx;
    chk_VerNoSePuedeNorm: TCheckBox;
    Panel2: TPanel;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    lbl_Comuna: TLabel;
    Label3: TLabel;
    cbRegiones: TComboBox;
    cbCiudades: TComboBox;
    cbComunas: TComboBox;
    Panel3: TPanel;
    btn_Normalizar: TDPSButton;
    BtnSalir: TDPSButton;
    btn_NoSePuede: TDPSButton;
    ObtenerCallesYTramos: TADOStoredProc;
    NormalizarDomicilio: TADOStoredProc;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    procedure chk_VerNoSePuedeNormClick(Sender: TObject);
    procedure ObtenerDomiciliosDesnormalizadoAfterOpen(DataSet: TDataSet);
    procedure dbl_DomiciliosDblClick(Sender: TObject);
    procedure cbRegionesChange(Sender: TObject);
    procedure cbComunasChange(Sender: TObject);
    procedure cbCiudadesChange(Sender: TObject);
    procedure cbRegionesEnter(Sender: TObject);
    procedure cbComunasEnter(Sender: TObject);
    procedure cbCiudadesEnter(Sender: TObject);
    procedure btn_NormalizarClick(Sender: TObject);
    procedure btn_NoSePuedeClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FCodigoRegionAnterior: AnsiString;
    FCodigoComunaAnterior: AnsiString;
    FCodigoCiudadAnterior: AnsiString;
    FCodigoPaisAnterior: AnsiString;

    procedure BuscarDomicilios;
    function DarRegion:String;
    function DarComuna:String;
    function DarCiudad:String;

  public
    function Inicializa(MDIChild: Boolean;
                                CodigoPais: AnsiString;
                                CodigoRegion: AnsiString = '';
                                CodigoComuna: AnsiString = '';
                                CodigoCiudad: AnsiString = ''): Boolean;
  end;

var
  FormNormalizarDomicilio: TFormNormalizarDomicilio;

implementation

{$R *.dfm}

function TFormNormalizarDomicilio.Inicializa(MDIChild: Boolean;
                                CodigoPais: AnsiString;
                                CodigoRegion: AnsiString = '';
                                CodigoComuna: AnsiString = '';
                                CodigoCiudad: AnsiString = ''): Boolean;
Var
	S: TSize;
begin
    try
        if MDIChild then begin
            S := GetFormClientSize(Application.MainForm);
            SetBounds(0, 0, S.cx, S.cy);
        end else begin
            FormStyle := fsNormal;
            Visible := False;
        end;

        CodigoRegion:=trim(CodigoRegion);
        CodigoComuna:=trim(CodigoComuna);
        CodigoCiudad:=trim(CodigoCiudad);
        Codigopais:=trim(Codigopais);

        FCodigoPaisAnterior:=CodigoPais;

        FCodigoRegionAnterior           := '';
        FCodigoComunaAnterior           := '';
        FCodigoCiudadAnterior           := '';

        CargarRegiones(DMConnections.BaseCAC, cbRegiones, CodigoPais, CodigoRegion,true);
        cbRegiones.OnChange(cbRegiones);

        if Codigoregion=REGION_SANTIAGO then begin
            CargarComunas(DMConnections.BaseCAC, cbComunas, CodigoPais, CodigoRegion, CodigoComuna,true);
            cbComunas.OnChange(cbComunas);
        end else begin
            cbComunas.Clear;
            CargarCiudadesRegion(DMConnections.BaseCAC, cbCiudades, CodigoPais, CodigoRegion, CodigoCiudad,true);
            cbCiudades.OnChange(cbCiudades);
        end;

        BuscarDomicilios;
        Result := True;
    except
        result:=false;
    end;
end;

procedure TFormNormalizarDomicilio.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     action := caFree;
end;

procedure TFormNormalizarDomicilio.BtnSalirClick(Sender: TObject);
begin
    close;
end;

procedure TFormNormalizarDomicilio.BuscarDomicilios;
begin
    try
        with ObtenerDomiciliosDesnormalizado do begin
            close;
            Parameters.ParamByName('@NoSePuedeNormalizar').Value:=chk_VerNoSePuedeNorm.Checked;
            Parameters.ParamByName('@CodigoPais').Value:=FCodigoPaisAnterior;
            Parameters.ParamByName('@CodigoRegion').Value:=iif(trim(DarRegion)='',null,DarRegion);
            Parameters.ParamByName('@CodigoComuna').Value:=iif(trim(DarComuna)='',null,DarComuna);
            Parameters.ParamByName('@CodigoCiudad').Value:=iif(trim(DarCiudad)='',null,DarCiudad);
            open;
        end;
    except
        On E: Exception do begin
            btn_Normalizar.Enabled:=false;
            btn_NoSePuede.Enabled:=btn_Normalizar.Enabled;
            MsgBoxErr(MSG_NORMALIZADO_BUSCAR, E.Message, MSG_NORMALIZADO_CAPTION, MB_ICONSTOP);
        end;
    end;

end;
procedure TFormNormalizarDomicilio.chk_VerNoSePuedeNormClick(
  Sender: TObject);
begin
    btn_NoSePuede.Visible:=not(chk_VerNoSePuedeNorm.Checked);
    BuscarDomicilios;
    dbl_Domicilios.SetFocus;
end;

procedure TFormNormalizarDomicilio.ObtenerDomiciliosDesnormalizadoAfterOpen(
  DataSet: TDataSet);
begin
    btn_Normalizar.Enabled:=ObtenerDomiciliosDesnormalizado.RecordCount<>0;
    btn_NoSePuede.Enabled:=btn_Normalizar.Enabled;
end;

procedure TFormNormalizarDomicilio.dbl_DomiciliosDblClick(Sender: TObject);
begin
    if btn_Normalizar.Enabled then btn_Normalizar.Click;
end;

procedure TFormNormalizarDomicilio.cbRegionesChange(Sender: TObject);
begin
    if (FCodigoRegionAnterior <> DarRegion) or (Trim(cbRegiones.Text)=SIN_ESPECIFICAR) then begin
        if (FCodigoPaisAnterior = PAIS_CHILE) and (DarRegion = REGION_SANTIAGO) then begin
            CargarComunas(DMConnections.BaseCAC, cbComunas, FCodigoPaisAnterior, DarRegion,'',true);
            cbComunas.OnChange(cbComunas);
            cbCiudades.Enabled      := False;
            cbCiudades.Color        := clBtnFace;
            cbComunas.Color         := clWindow;
            cbComunas.Enabled       := True;
        end else if (FCodigoPaisAnterior = PAIS_CHILE) then begin
            cbComunas.Enabled       := False;
            cbComunas.Clear;
            cbCiudades.Enabled      := True;
            cbCiudades.Color        := clWindow;
            cbComunas.Color         := clBtnFace;
            CargarCiudadesRegion(DMConnections.BaseCAC, cbCiudades, FCodigoPaisAnterior, DarRegion,'',true);
            cbCiudades.OnChange(cbCiudades);
        end else begin
            cbComunas.Enabled       := True;
            cbCiudades.Enabled      := True;
            cbCiudades.Color        := clWindow;
            cbComunas.Color         := clWindow;
            CargarCiudadesRegion(DMConnections.BaseCAC, cbCiudades, FCodigoPaisAnterior, DarRegion,'',true);
        end;
    end;
    FCodigoRegionAnterior   := DarRegion;
    FCodigoComunaAnterior   := DarComuna;
    FCodigoCiudadAnterior   := DarCiudad;


end;

procedure TFormNormalizarDomicilio.cbComunasChange(Sender: TObject);
begin
    if (FCodigoComunaAnterior <> DarComuna) or (trim(cbComunas.Text)=SIN_ESPECIFICAR) then begin
        if (FCodigoPaisAnterior = PAIS_CHILE) and (DarRegion = REGION_SANTIAGO) then begin
            CargarCiudades(DMConnections.BaseCAC,cbCiudades,FCodigoPaisAnterior,DarRegion,DarComuna,'',true);
            if cbCiudades.Items.Count=2 then cbCiudades.ItemIndex:=1;
            cbComunas.Enabled       := True;
            cbCiudades.Enabled      := False;
            cbCiudades.Color        := clBtnFace;
            cbComunas.Color         := clWindow;
        end else if (FCodigoPaisAnterior = PAIS_CHILE) then begin
            cbComunas.Enabled       := False;
            cbCiudades.Enabled      := True;
            cbComunas.Color        := clBtnFace;
            cbCiudades.Color         := clWindow;
            CargarCiudadesRegion(DMConnections.BaseCAC, cbCiudades, FCodigoPaisAnterior, DarRegion, '', true);
        end else begin
            cbComunas.Enabled       := True;
            cbCiudades.Enabled      := True;
            cbCiudades.Color        := clWindow;
            cbComunas.Color         := clWindow;
            CargarCiudades(DMConnections.BaseCAC, cbCiudades, FCodigoPaisAnterior, DarRegion, DarComuna, '', true);
        end;
        BuscarDomicilios;
    end;
    FCodigoComunaAnterior   := DarComuna;
    FCodigoCiudadAnterior   := DarCiudad;

end;

procedure TFormNormalizarDomicilio.cbCiudadesChange(Sender: TObject);
begin
    cbComunas.Enabled       := False;
    cbCiudades.Enabled      := True;
    cbComunas.Color         := clBtnFace;
    BuscarDomicilios;
end;

procedure TFormNormalizarDomicilio.cbRegionesEnter(Sender: TObject);
begin
    FCodigoRegionAnterior   := DarRegion;
end;

procedure TFormNormalizarDomicilio.cbComunasEnter(Sender: TObject);
begin
    FCodigoComunaAnterior := DarComuna;
end;

procedure TFormNormalizarDomicilio.cbCiudadesEnter(Sender: TObject);
begin
    FCodigoCiudadAnterior := DarCiudad;
end;

function TFormNormalizarDomicilio.DarRegion:String;
begin
    result:=Trim(StrRight(cbRegiones.Text,3));
end;

function TFormNormalizarDomicilio.DarComuna:String;
begin
    if DarRegion=REGION_SANTIAGO then result:=Trim(StrRight(cbComunas.Text,3))
    else result:=Trim(StrLeft(StrRight(cbCiudades.Text,20),10));
end;

function TFormNormalizarDomicilio.DarCiudad:String;
begin
    result:=Trim(StrRight(cbCiudades.Text,3));
end;

{******************************** Function Header ******************************
Function Name: TFormNormalizarDomicilio.btn_NormalizarClick
Author       : dcalani
Date Created : 04/03/2004
Description  : Realiza la normalizacion propiamente dicha
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFormNormalizarDomicilio.btn_NormalizarClick(Sender: TObject);
var
    f:TFormBuscaCalle;
    RegionEsquina,ComunaEsquina,CiudadEsquina:String;
begin
    try
        try
            if (ObtenerDomiciliosDesnormalizado.FieldByName('Esquina1Calle').IsNull) and (ObtenerDomiciliosDesnormalizado.FieldByName('Esquina2Calle').IsNull) then begin
                RegionEsquina:=REGION_SANTIAGO;
                ComunaEsquina:='';
                CiudadEsquina:='';
            end else begin
                with ObtenerCallesYTramos do begin
                    close;
                    if not(ObtenerDomiciliosDesnormalizado.FieldByName('Esquina1Calle').IsNull) then begin
                        Parameters.ParamByName('@CodigoCalle').Value:=ObtenerDomiciliosDesnormalizado.FieldByName('Esquina1Calle').Value;
                        Parameters.ParamByName('@Numero').Value:=ObtenerDomiciliosDesnormalizado.FieldByName('Esquina1Numero').Value;
                    end else begin
                        Parameters.ParamByName('@CodigoCalle').Value:=ObtenerDomiciliosDesnormalizado.FieldByName('Esquina2Calle').Value;
                        Parameters.ParamByName('@Numero').Value:=ObtenerDomiciliosDesnormalizado.FieldByName('Esquina2Numero').Value;
                    end;
                    open;
                    RegionEsquina:=FieldByName('CodigoRegion').Value;
                    ComunaEsquina:=FieldByName('CodigoComuna').Value;
                    CiudadEsquina:=FieldByName('CodigoCiudad').Value;
                    close;
                end;
            end;
            Application.CreateForm(TFormBuscaCalle,f);
            with ObtenerDomiciliosDesnormalizado do begin
                if f.inicializar(FCodigoPaisAnterior,RegionEsquina ,ComunaEsquina, FieldByName('Numero').AsString, FieldByName('CalleDesnormalizada').AsString)
                    and (f.ShowModal=mrOk) then begin
                        try
                            NormalizarDomicilio.Parameters.ParamByName('@CodigoDomicilio').Value:=FieldByName('CodigoDomicilio').Value;
                            NormalizarDomicilio.Parameters.ParamByName('@CodigoCalle').Value:=f.CodigoCalle;
                            NormalizarDomicilio.ExecProc;

                            close;
                            Open;
                        except
                            On E: Exception do begin
                                MsgBoxErr(MSG_NORMALIZADO_ERROR, E.Message, MSG_NORMALIZADO_CAPTION, MB_ICONSTOP);
                            end;
                        end;
                end;
            end;
        except
            On E: Exception do begin
                MsgBoxErr(MSG_ERROR_BUSQUEDA, E.Message, MSG_NORMALIZADO_CAPTION, MB_ICONSTOP);
            end;
        end;
    finally
        f.Release;
        dbl_Domicilios.SetFocus;
    end;
end;

procedure TFormNormalizarDomicilio.btn_NoSePuedeClick(Sender: TObject);
begin
    with ObtenerDomiciliosDesnormalizado do begin
        QueryExecute(DMConnections.BaseCAC,format('EXEC SetNoSePuedeNormalizar %d',[FieldByName('CodigoDomicilio').AsInteger]));
        close;
        open;
    end;
end;

procedure TFormNormalizarDomicilio.FormShow(Sender: TObject);
begin
    dbl_Domicilios.SetFocus;
end;

end.
