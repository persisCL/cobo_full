{********************************** File Header ********************************
File Name   : frmDatoPesona.pas
Author :
Date Created :
Language    : ES-AR
Description :

Revision: 1
Author : jconcheyro
Date Created : 17/10/2006
Description : Se agrega la property EsConvenioCN para poder hacer obligatorio o no
el Giro en ValidarDatos

Revision: 2
Author : lcanteros
Date Created : 12/03/2008
Description : Se agrega el combo de tipos de clientes y el led


Revision 3
Author: mbecerra
Date: 11-Noviembre-2009
Description:		(Ref SS 833)
                Se modifica para que siempre devuelva el Giro del Cliente,
                independiente del tipo de cliente que sea.

Revision 4
Author: jjofre
Date: 13-08-2010
Description:		(Ref SS 690 Semaforos)

            - Se a�adieron los siguientes Objetos

                spTiposClientes: TADOStoredProc;
                srcTipoCliente: TDataSource;
                lblSemaforo1: TLabel;
                cboSemaforo1: TDBLookupComboBox;
                LedSemaforo1: TLed;
                lblSemaforo3: TLabel;
                cboSemaforo3: TDBLookupComboBox;
                LedSemaforo3: TLed;
                spSemaforo1ObtenerLista: TADOStoredProc;
                srcSemaforo1: TDataSource;
                srcSemaforo3: TDataSource;
                spSemaforo3ObtenerLista: TADOStoredProc;
                FSemaforo1, FSemaforo3: Integer;
                FValorDefaultSemaforo1, FValorDefaultSemaforo3 : Integer;

            -Se modificaron/a�adieron los siguientes Procedimientos/Funciones
                 cboSemaforo3KeyPress
                 cboSemaforo1KeyPress
                 cboSemaforo1CloseUp
                 cboSemaforo3CloseUp
                 CargarSemaforo1
                 CargarSemaforo3
                 Create
                 cboTipoClienteCloseUp
                 SetRegistrodeDatos
                 getRegistrodeDatos
                 LimpiarRegistro


Revision 5
Author: jjofre
Date: 07-09-2010
Description:		(Ref //REV. 5 SS_690)
          - Se a�adieron los siguientes Objetos
                FValorDefaultSemaforo2
                ValorParametroDefaultSemaforo2
          - Se modificaron/a�adieron los siguientes Procedimientos/Funciones
                Create
                SetRegistrodeDatos

Revisi�n 6
Author: pdominguez
Date: 28/12/2010
Description: SS 942 - Error CAC
    - Se a�ade el control para la existencia en los Par�metros Generales de
    los valores por defecto para los sem�foros.
*******************************************************************************}
unit frmDatoPesona;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,UtilDB,ConstParametrosGenerales,
  ComCtrls, StdCtrls, Validate, DateEdit,Util,PeaProcs,PeaTypes,DmConnection,RStrings,UtilProc,
  ExtCtrls, DmiCtrls, VariantComboBox, DBCtrls, DB, ADODB, Variants, SysUtilsCN, PeaProcsCN;

type
  TFreDatoPresona = class(TFrame)
    Pc_Datos: TPageControl;
    Tab_Juridica: TTabSheet;
    Tab_Fisica: TTabSheet;
    txt_ApellidoMaterno: TEdit;
    Label4: TLabel;
    txt_Apellido: TEdit;
    Label6: TLabel;
    lbl_Sexo: TLabel;
    cbSexo: TComboBox;
    txtFechaNacimiento: TDateEdit;
    lbl_FechaNacimiento: TLabel;
    Label3: TLabel;
    txt_Nombre: TEdit;
    txtApellidoMaterno_Contacto: TEdit;
    cbSexo_Contacto: TComboBox;
    lblSexo_Contacto: TLabel;
    lblApellidoMaterno: TLabel;
    txtApellido_Contacto: TEdit;
    lblApellido: TLabel;
    txtNombre_Contacto: TEdit;
    lblNombre: TLabel;
    Panel1: TPanel;
    lbl_Personeria: TLabel;
    lblRutRun: TLabel;
    lblRazonSocial: TLabel;
    cbPersoneria: TComboBox;
    txtDocumento: TEdit;
    txtRazonSocial: TEdit;
    txtGiro: TEdit;
    lblGiro: TLabel;
    ledTipoCliente: TLed;
    lblTipo: TLabel;
    cboTipoCliente: TDBLookupComboBox;
    spTiposClientes: TADOStoredProc;
    srcTipoCliente: TDataSource;
    lblSemaforo1: TLabel;
    cboSemaforo1: TDBLookupComboBox;
    LedSemaforo1: TLed;
    lblSemaforo3: TLabel;
    cboSemaforo3: TDBLookupComboBox;
    LedSemaforo3: TLed;
    spSemaforo1ObtenerLista: TADOStoredProc;
    srcSemaforo1: TDataSource;
    srcSemaforo3: TDataSource;
    spSemaforo3ObtenerLista: TADOStoredProc;
    procedure cbPersoneriaChange(Sender: TObject);
    procedure txtDocumentoKeyPress(Sender: TObject; var Key: Char);
    procedure txtFechaNacimientoExit(Sender: TObject);
    procedure txt_NombreKeyPress(Sender: TObject; var Key: Char);
    procedure cboTipoClienteKeyPress(Sender: TObject; var Key: Char);
    procedure cboTipoClienteCloseUp(Sender: TObject);
    procedure cboSemaforo3KeyPress(Sender: TObject; var Key: Char);
    procedure cboSemaforo1CloseUp(Sender: TObject);
    procedure cboSemaforo3CloseUp(Sender: TObject);
    procedure cboSemaforo1KeyPress(Sender: TObject; var Key: Char);
  private
    FTieneAmbasPersonerias:Boolean;
    FPermiteNingunaPersonerias:Boolean;
    FEdadMinimaSolicitud, FTipoCliente, FSemaforo1, FSemaforo3: Integer;
    FValorDefaultSemaforo1, FValorDefaultSemaforo2 , FValorDefaultSemaforo3 : String; //REV. 5 SS_690 // SS_942_PDO_20101228
    aux:TDatosPersonales;
    FPermiteNingunSexo: Boolean;
    //FEsConvenioCN: Boolean;													//SS_1147_MCA_20140408
    FEsConvenioNativo: Boolean;													//SS_1147_MCA_20140408
    procedure SetPersoneria(const Value: AnsiString);
    function GetPersoneria:AnsiString;
    procedure SetRegistrodeDatos(const Value: TDatosPersonales);
    function GetRegistrodeDatos:TDatosPersonales;
    function GetSexo:String;
    function GetSexo_Contacto:String;
    function EdadValida:Boolean;
    procedure SetPermiteNingunSexo(const Value: Boolean);
    function CargarTiposDeCliente(): boolean;
    function CargarSemaforo1(): boolean;
    function CargarSemaforo3(): boolean;

    { Private declarations }
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;

    property Personeria:AnsiString read GetPersoneria write SetPersoneria;
    property TieneAmbasPersonerias:Boolean read FTieneAmbasPersonerias write FTieneAmbasPersonerias default false;
    property PermiteNingunaPersonerias:Boolean read FPermiteNingunaPersonerias write FPermiteNingunaPersonerias  default false;
	property RegistrodeDatos:TDatosPersonales read GetRegistrodeDatos write SetRegistrodeDatos;
    property PermiteNingunSexo: Boolean read FPermiteNingunSexo write SetPermiteNingunSexo default true;
    procedure Limpiar;
    function ValidarDatos:Boolean;
    //property EsConvenioCN: Boolean read FEsConvenioCN write FEsConvenioCN;	//SS_1147_MCA_20140408
    property EsConvenioNativo: Boolean read FEsConvenioNativo write FEsConvenioNativo;	//SS_1147_MCA_20140408
    class procedure LimpiarRegistro(var RegistroDatosPersonales:TDatosPersonales);
  end;

implementation

{$R *.dfm}

Const
    COLOR_DEFAULT = '00FFFFFF';

constructor TFreDatoPresona.Create(AOwner: TComponent);
resourcestring
	CAPTION_DATOS_PERSONA = 'Datos Persona';
var ValorParamGeneral: string;
    ValorParametroDefaultSemaforo1, ValorParametroDefaultSemaforo3 , ValorParametroDefaultSemaforo2: String; //REV. 5 SS_690 // SS_942_PDO_20101228
begin
    inherited;
    Caption:=CAPTION_DATOS_PERSONA;
    Limpiar;
    FPermiteNingunSexo := True;
    if ObtenerParametroGeneral(DMConnections.BaseCAC, 'SEMAFORO_2', ValorParamGeneral) then begin
      lblTipo.Caption := ValorParamGeneral;
    end;
    if ObtenerParametroGeneral(DMConnections.BaseCAC, 'SEMAFORO_1', ValorParamGeneral) then begin
      lblSemaforo1.Caption := Trim(ValorParamGeneral);
    end;
    if ObtenerParametroGeneral(DMConnections.BaseCAC, 'SEMAFORO_3', ValorParamGeneral) then begin
      lblSemaforo3.Caption := Trim(ValorParamGeneral);
    end;
    // SS_942_PDO_20101228
    if ObtenerParametroGeneral(DMConnections.BaseCAC, 'DEFAULT_SEMAFORO_1', ValorParametroDefaultSemaforo1) then begin
      FValorDefaultSemaforo1 := ValorParametroDefaultSemaforo1;
    end
    else raise Exception.Create('Falta el Par�metro General "DEFAULT_SEMAFORO_1". Comunique el Error al �rea de Sistemas.');

    //REV. 5 SS_690
    if ObtenerParametroGeneral(DMConnections.BaseCAC, 'DEFAULT_SEMAFORO_2', ValorParametroDefaultSemaforo2) then begin
        FValorDefaultSemaforo2 := ValorParametroDefaultSemaforo2;
    end
    else raise Exception.Create('Falta el Par�metro General "DEFAULT_SEMAFORO_2". Comunique el Error al �rea de Sistemas.');
    //REV. 5 SS_690

    if ObtenerParametroGeneral(DMConnections.BaseCAC, 'DEFAULT_SEMAFORO_3', ValorParametroDefaultSemaforo3) then begin
    FValorDefaultSemaforo3 := ValorParametroDefaultSemaforo3;
    end
    else raise Exception.Create('Falta el Par�metro General "DEFAULT_SEMAFORO_3". Comunique el Error al �rea de Sistemas.');
    // Fin rev. SS_942_PDO_20101228
    CargarSexos(cbSexo_Contacto, '', FPermiteNingunSexo);
    CargarSexos(cbSexo, '', FPermiteNingunSexo);
    GrabarComponentesObligatoriosyHints(self,DMConnections.BaseCAC,[cbPersoneria,txtDocumento,txtRazonSocial,txtGiro,txt_Apellido,txt_Nombre,txt_ApellidoMaterno,cboTipoCliente, cboSemaforo1, cboSemaforo3]); // SS 690
    ObtenerComponentesObligatoriosyHints(self,DMConnections.BaseCAC);
    ObtenerParametroGeneral(DMConnections.BaseCAC, PS_EDAD_MINIMA, FEdadMinimaSolicitud);
    CargarTiposDeCliente;
    CargarSemaforo1;
    CargarSemaforo3;
    FTipoCliente := 0;
    FSemaforo1 := 0;
    FSemaforo3 := 0;
    Color := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_MCA_20150107
end;

function TFreDatoPresona.GetPersoneria: AnsiString;
begin
    result:=StrRight(' '+trim(cbPersoneria.items[cbPersoneria.itemindex]),1);
end;

procedure TFreDatoPresona.SetPersoneria(const Value: AnsiString);
begin
  CargarPersoneria(cbPersoneria,Value,self.TieneAmbasPersonerias,self.PermiteNingunaPersonerias);
  cbPersoneria.OnChange(cbPersoneria);
end;

procedure TFreDatoPresona.cboSemaforo1CloseUp(Sender: TObject);
var AuxColor: string;
begin
    FSemaforo1 := 0;
    if cboSemaforo1.KeyValue = null then exit;
    if cboSemaforo1.KeyValue = 0  then begin
        cboSemaforo1.KeyValue := null;
        LedSemaforo1.ColorOn := HexToInt(COLOR_DEFAULT);
        exit;
    end;
    FSemaforo1 := cboSemaforo1.KeyValue;
    AuxColor := cboSemaforo1.ListSource.DataSet.FieldByName('Color').AsString;
    if AuxColor = '' then AuxColor := COLOR_DEFAULT;
    LedSemaforo1.ColorOn := HexToInt(AuxColor);

end;

procedure TFreDatoPresona.cboSemaforo1KeyPress(Sender: TObject; var Key: Char);
begin
    if Key in [#8] then begin
        cboSemaforo1.KeyValue := 0;
        cboSemaforo1CloseUp(Sender);
    end;
end;

procedure TFreDatoPresona.cboSemaforo3CloseUp(Sender: TObject);
var AuxColor: string;
begin
    FSemaforo3 := 0;
    if cboSemaforo3.KeyValue = null then exit;
    if cboSemaforo3.KeyValue = 0  then begin
        cboSemaforo3.KeyValue := null;
        LedSemaforo3.ColorOn := HexToInt(COLOR_DEFAULT);
        exit;
    end;
    FSemaforo3 := cboSemaforo3.KeyValue;
    AuxColor := cboSemaforo3.ListSource.DataSet.FieldByName('Color').AsString;
    if AuxColor = '' then AuxColor := COLOR_DEFAULT;
    LedSemaforo3.ColorOn := HexToInt(AuxColor);

end;

procedure TFreDatoPresona.cboSemaforo3KeyPress(Sender: TObject; var Key: Char);
begin
    if Key in [#8] then begin
        cboSemaforo3.KeyValue := 0;
        cboSemaforo3CloseUp(Sender);
    end;
end;
{-----------------------------------------------------------------------------
  Procedure: cboTipoClienteCloseUp
  Author:    lcanteros
  Date:      13-Mar-2008
  Arguments: Sender: TObject
  Result:    None
  Purpose:   Actualiza el led de color y la variable de tipo de persona
-----------------------------------------------------------------------------}
procedure TFreDatoPresona.cboTipoClienteCloseUp(Sender: TObject);
var AuxColor: string;
begin
    FTipoCliente := 0;
    if cboTipoCliente.KeyValue = null then exit;
    if cboTipoCliente.KeyValue = 0  then begin
        cboTipoCliente.KeyValue := null;
        ledTipoCliente.ColorOn := HexToInt(COLOR_DEFAULT);
        exit;
    end;
    FTipoCliente := cboTipoCliente.KeyValue;
    AuxColor := cboTipoCliente.ListSource.DataSet.FieldByName('Color').AsString;
    if AuxColor = '' then AuxColor := COLOR_DEFAULT;
    ledTipoCliente.ColorOn := HexToInt(AuxColor);
end;
{-----------------------------------------------------------------------------
  Procedure: cboTipoClienteKeyPress
  Author:    lcanteros
  Date:      13-Mar-2008
  Arguments: Sender: TObject; var Key: Char
  Result:    None
  Purpose:   Si el operador presiona BackSpace el comobo queda vacio y la variable de tipo de cliente en 0
-----------------------------------------------------------------------------}
procedure TFreDatoPresona.cboTipoClienteKeyPress(Sender: TObject; var Key: Char);
begin
    if Key in [#8] then begin
        cboTipoCliente.KeyValue := 0;
        cboTipoClienteCloseUp(Sender);
    end;
end;

procedure TFreDatoPresona.cbPersoneriaChange(Sender: TObject);
begin
    if self.Personeria=PERSONERIA_FISICA then begin
		Pc_Datos.ActivePage := Tab_Fisica;
    end;
    if self.Personeria=PERSONERIA_JURIDICA then begin
		Pc_Datos.ActivePage := Tab_Juridica;
    end;
    lblRazonSocial.Visible := self.Personeria = PERSONERIA_JURIDICA ;
    txtRazonSocial.Visible := self.Personeria = PERSONERIA_JURIDICA ;
    lblGiro.Visible:= self.Personeria = PERSONERIA_JURIDICA ;
    txtGiro.Visible:= self.Personeria = PERSONERIA_JURIDICA ;
end;

procedure TFreDatoPresona.Limpiar;
begin
    self.Personeria:=PERSONERIA_FISICA;
    txtDocumento.clear;
    txt_ApellidoMaterno.clear;
    txt_Apellido.clear;
    txtFechaNacimiento.clear;
    txt_Nombre.Clear;
    txtApellidoMaterno_Contacto.Clear;
    txtApellido_Contacto.Clear;
    txtRazonSocial.Clear;
    txtNombre_Contacto.Clear;
    cbSexo.ItemIndex := 0;
    cbSexo_Contacto.ItemIndex := 0;
end;


procedure TFreDatoPresona.txtDocumentoKeyPress(Sender: TObject;
  var Key: Char);
begin
    if not (Key  in ['0'..'9','K','k', #8]) then
       Key := #0;
end;

{
 Revision : 1
    Author : lcanteros
    Date : 14/03/2008
    Description : Agrego el campo TipoCliente para los datos en Persona
}
procedure TFreDatoPresona.SetRegistrodeDatos(const Value: TDatosPersonales);
begin
    self.Personeria:=Value.Personeria;
    txtRazonSocial.Text:=value.RazonSocial;
    txtGiro.Text:=value.Giro;
    if self.Personeria=PERSONERIA_FISICA then begin
        txt_Nombre.Text:=Value.Nombre;
        txt_Apellido.Text:=Value.Apellido;
        txt_ApellidoMaterno.Text:=Value.ApellidoMaterno;
        CargarSexos(cbSexo,Value.Sexo,FPermiteNingunSexo);
    end else begin
        txtNombre_Contacto.Text:=Value.Nombre;
        txtApellido_Contacto.Text:=Value.Apellido;
        txtApellidoMaterno_Contacto.Text:=Value.ApellidoMaterno;
		CargarSexos(cbSexo_Contacto,Value.Sexo,FPermiteNingunSexo);
    end;
    txtFechaNacimiento.Date:=Value.FechaNacimiento;
    if Value.TipoCliente <= 0 then
        cboTipoCliente.KeyValue := FValorDefaultSemaforo2  //REV. 5 SS_690
    else cboTipoCliente.KeyValue := Value.TipoCliente; // revision 1
    cboTipoClienteCloseUp(self);

    if Value.Semaforo1 <= 0 then
        cboSemaforo1.KeyValue := FValorDefaultSemaforo1
    else cboSemaforo1.KeyValue := Value.Semaforo1;
    cboSemaforo1CloseUp(self);

    if Value.Semaforo3<= 0 then
        cboSemaforo3.KeyValue := FValorDefaultSemaforo3
    else cboSemaforo3.KeyValue := Value.Semaforo3;
    cboSemaforo3CloseUp(self);

    Aux:=Value;
	txtDocumento.Text:=Value.NumeroDocumento;
end;

{
Revision : 1
    Author : lcanteros
    Date : 14/03/2008
    Description : Agrego el campo TipoCliente para los datos en Persona
}
function TFreDatoPresona.GetRegistrodeDatos: TDatosPersonales;
begin
    aux.Personeria := self.Personeria[1];
	aux.NumeroDocumento := Trim(txtDocumento.Text);
    aux.TipoDocumento := TIPO_DOCUMENTO_RUT;
    aux.RazonSocial := Trim(txtRazonSocial.Text);
    if self.Personeria = PERSONERIA_FISICA then begin
        Aux.Nombre := Trim(txt_Nombre.Text);
        Aux.Apellido := Trim(txt_Apellido.Text);
        Aux.ApellidoMaterno := Trim(txt_ApellidoMaterno.Text);
        aux.Sexo := GetSexo[1];
    end else begin
        Aux.Nombre := Trim(txtNombre_Contacto.Text);
        Aux.Apellido := Trim(txtApellido_Contacto.Text);
        Aux.ApellidoMaterno := Trim(txtApellidoMaterno_Contacto.Text);
		aux.Sexo := GetSexo_Contacto[1];
        //REV.3  aux.Giro := txtGiro.Text;
    end;

    aux.Giro		:= txtGiro.Text;			//REV.3
    	  																																																																																																																																																																																																																																																																																																																																						 
	aux.FechaNacimiento := txtFechaNacimiento.Date;
    aux.TipoCliente := FTipoCliente; // revision 1
    aux.Semaforo1 := FSemaforo1;
    aux.Semaforo3 := FSemaforo3;
	Result := aux;
end;

function TFreDatoPresona.GetSexo: String;
begin
    result:=StrRight(' '+trim(cbSexo.Text),1);
end;

function TFreDatoPresona.GetSexo_Contacto: String;
begin
    result:=StrRight(' '+trim(cbSexo_Contacto.Text),1);
end;

{-----------------------------------------------------------------------------
  Procedure: ValidarDatos
  Author:
  Date:
  Arguments: None
  Result:    Boolean
  Revision :1
      Author : vpaszkowicz
      Date : 19/01/2007
      Description : Agrego como obligatorio el Giro para convenios no CN.
-----------------------------------------------------------------------------}
function TFreDatoPresona.ValidarDatos: Boolean;
var
    validacionRUT:Integer;
    EsEdadValida:Boolean;
begin

    validacionRUT:=-1;
	if trim(txtDocumento.Text) <> '' then begin
		txtDocumento.Text := PadL(trim(txtDocumento.Text), 9, '0');
		validacionRUT:=QueryGetValueInt(DMConnections.BaseCAC, 'Exec VerificarRUT ''' +
			  StrLeft(txtDocumento.Text, (Length(txtDocumento.Text) -1))  +
			  ''',''' +
			  txtDocumento.Text[Length(txtDocumento.Text)] + '''');
	end;

    EsEdadValida:=(not(EsComponenteObligatorio(txtFechaNacimiento)) and (txtFechaNacimiento.IsEmpty)) or (EdadValida);

    result:=ValidateControls([  txtDocumento,
                                txtDocumento,
                                txtRazonSocial,
                                txtNombre_Contacto,
                                txtApellido_Contacto,
                                txtApellidoMaterno_Contacto,
                                cbSexo_Contacto,
                                txtGiro,
                                txt_Nombre,
                                txt_Apellido,
                                txt_ApellidoMaterno,
                                cbSexo,
                                txtFechaNacimiento],
                                [trim(txtDocumento.Text)<>'',
                                (validacionRUT=1),
                                (self.Personeria<>PERSONERIA_JURIDICA) or (txtRazonSocial.Text<>''),
                                (self.Personeria<>PERSONERIA_JURIDICA) or not(EsComponenteObligatorio(txtNombre_Contacto)) or (trim(txtNombre_Contacto.Text)<>''),
                                (self.Personeria<>PERSONERIA_JURIDICA) or not(EsComponenteObligatorio(txtApellido_Contacto)) or (Trim(txtApellido_Contacto.Text)<>''),
                                (self.Personeria<>PERSONERIA_JURIDICA) or not(EsComponenteObligatorio(txtApellidoMaterno_Contacto)) or (Trim(txtApellidoMaterno_Contacto.Text)<>''),
                                (self.Personeria<>PERSONERIA_JURIDICA) or not(EsComponenteObligatorio(cbSexo_Contacto)) or (FPermiteNingunSexo and (cbSexo_Contacto.ItemIndex > 0)) or ( not(FPermiteNingunSexo) and (cbSexo_Contacto.ItemIndex >= 0)) ,
//Si el Giro es de un Convenio No CN debe ingresarse tambi�n.
                                (self.Personeria<>PERSONERIA_JURIDICA) or not(EsComponenteObligatorio(txtGiro)) or (trim(txtGiro.Text)<>''), // or not FEsConvenioCN,
                                (self.Personeria<>PERSONERIA_FISICA) or not(EsComponenteObligatorio(txt_Nombre)) or (Trim(txt_Nombre.Text)<>''),
                                (self.Personeria<>PERSONERIA_FISICA) or not(EsComponenteObligatorio(txt_Apellido)) or (Trim(txt_Apellido.Text)<>''),
                                (self.Personeria<>PERSONERIA_FISICA) or not(EsComponenteObligatorio(txt_ApellidoMaterno)) or (Trim(txt_ApellidoMaterno.Text)<>''),
                                (self.Personeria<>PERSONERIA_FISICA) or not(EsComponenteObligatorio(cbSexo)) or (FPermiteNingunSexo and (cbSexo.ItemIndex > 0)) or ( not(FPermiteNingunSexo) and (cbSexo.ItemIndex >= 0 )),
                                (self.Personeria<>PERSONERIA_FISICA) or EsEdadValida],
                            format(MSG_CAPTION_VALIDAR,[STR_PERSONA]),
                            [Format(MSG_VALIDAR_DEBE_EL,[STR_RUT]),
                            MSG_ERROR_DOCUMENTO,
                            format(MSG_VALIDAR_DEBE_LA,[FLD_RAZON_SOCIAL]),
                            format(MSG_VALIDAR_DEBE_EL,[FLD_NOMBRE]),
                            format(MSG_VALIDAR_DEBE_EL,[FLD_APELLIDO]),
                            format(MSG_VALIDAR_DEBE_EL,[FLD_APELLIDO_MATERNO]),
                            format(MSG_VALIDAR_DEBE_EL,[FLD_SEXO]),
                            format(MSG_VALIDAR_DEBE_EL,[FLD_GIRO]),
                            format(MSG_VALIDAR_DEBE_EL,[FLD_NOMBRE]),
                            format(MSG_VALIDAR_DEBE_EL,[FLD_APELLIDO]),
                            format(MSG_VALIDAR_DEBE_EL,[FLD_APELLIDO_MATERNO]),
                            format(MSG_VALIDAR_DEBE_EL,[FLD_SEXO]),
                            Format(MSG_CAPTION_VALIDAR,[STR_CONTACTO])]);
end;

procedure TFreDatoPresona.txtFechaNacimientoExit(Sender: TObject);
begin
	//La fecha de naciemiento es optativa. Si la ingresan se valida
    if not(txtFechaNacimiento.IsEmpty) and  not(EdadValida) then begin
		MsgBoxBalloon(Format(MSG_VALIDAR_FECHA_NACIMIENTO,[FEdadMinimaSolicitud]),
          Format(MSG_CAPTION_VALIDAR,[STR_CONTACTO]), MB_ICONSTOP, txtFechaNacimiento);
    end;
end;

function TFreDatoPresona.EdadValida: Boolean;
begin
    result:= FechaNacimientoValida(txtFechaNacimiento.date, FEdadMinimaSolicitud);
end;


class procedure TFreDatoPresona.LimpiarRegistro(
  var RegistroDatosPersonales: TDatosPersonales);
begin
    with RegistroDatosPersonales do begin
        CodigoPersona:=-1;
        LugarNacimiento:=PAIS_CHILE;
        TipoDocumento := TIPO_DOCUMENTO_RUT;
        CodigoActividad:=-1;
        Password:='';
        Pregunta:='';
        Respuesta:='';
        RazonSocial:='';
        Personeria:=PERSONERIA_FISICA;
        NumeroDocumento:='';
        Apellido:='';
        ApellidoMaterno:='';
        Nombre:='';
        Sexo:=#0;
        FechaNacimiento:=NullDate;
        TipoCliente := 0;
        Semaforo1 := 0;
        Semaforo3 := 0
    end;
end;
{-----------------------------------------------------------------------------
  Procedure: CargarTiposDeCliente
  Author:    lcanteros
  Date:      13-Mar-2008
  Arguments: 
  Result:    boolean
  Purpose:   Carga en el combo los tipos de clientes
-----------------------------------------------------------------------------}
function TFreDatoPresona.CargarTiposDeCliente(): boolean;
resourcestring
    MSG_ERROR_OBTENER_TIPOS_CLIENTE = 'No se pudo obtener los tipos de clientes';
    CAPTION_ERROR = 'ERROR';
begin
    result:= false;
    try
        with spTiposClientes do begin
            Close;
            Open;
        end;
        result := true;
    except
        MsgBoxErr(MSG_ERROR_OBTENER_TIPOS_CLIENTE,'', CAPTION_ERROR, 0);
    end;
end;

function TFreDatoPresona.CargarSemaforo1(): boolean;
resourcestring
    MSG_ERROR_OBTENER_SEMAFORO1 = 'No se pudo obtener los datos del SEMAFORO1';
    CAPTION_ERROR = 'ERROR';
begin
    result:= false;
    try
        with spSemaforo1ObtenerLista do begin
            Close;
            Open;
        end;
        result := true;
    except
        MsgBoxErr(MSG_ERROR_OBTENER_SEMAFORO1,'', CAPTION_ERROR, 0);
    end;
end;

function TFreDatoPresona.CargarSemaforo3(): boolean;
resourcestring
    MSG_ERROR_OBTENER_SEMAFORO3 = 'No se pudo obtener los datos del SEMAFORO3';
    CAPTION_ERROR = 'ERROR';
begin
    result:= false;
    try
        with spSemaforo3ObtenerLista  do begin
            Close;
            Open;
        end;
        result := true;
    except
        MsgBoxErr(MSG_ERROR_OBTENER_SEMAFORO3,'', CAPTION_ERROR, 0);
    end;
end;


procedure TFreDatoPresona.SetPermiteNingunSexo(const Value: Boolean);
begin
    FPermiteNingunSexo := Value;
    CargarSexos(cbSexo_Contacto, '', FPermiteNingunSexo);
    CargarSexos(cbSexo, '', FPermiteNingunSexo);
end;

procedure TFreDatoPresona.txt_NombreKeyPress(Sender: TObject;
  var Key: Char);
begin
    if key in ['}',''''] then Key := #0;
end;

end.
