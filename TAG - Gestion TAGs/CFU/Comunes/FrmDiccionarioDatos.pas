{-----------------------------------------------------------------------------
 File Name: FrmDiccionarioDatos
 Author:
 Date Created:
 Language: ES-AR
 Description:

Revision : 1
Date: 25/02/2009
Author: mpiazza
Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para 
	los bloqueos de tablas en la lectura

Revision : 2
Date:
Author:
Description: SS 744
-----------------------------------------------------------------------------}
unit FrmDiccionarioDatos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, UtilProc, DB, ADODB, UtilDB, StdCtrls, DPSControls,
  ExtCtrls, CheckLst, DMConnectionInformes, DmiCtrls;

type
  TFormDiccionarioDatos = class(TForm)
    TreeView: TTreeView;
    qry_Campos: TADOQuery;
    qry_Tablas: TADOQuery;
    Panel2: TPanel;
    Panel1: TPanel;
    Notebook: TNotebook;
    BtnSalir: TButton;
    pnlEncabezado: TPanel;
    txtServidor: THistoryEdit;
    Label1: TLabel;
    Label2: TLabel;
    txtBase: THistoryEdit;
    BtnVer: TButton;
    procedure BtnVerClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnSalirClick(Sender: TObject);
    procedure TreeViewChange(Sender: TObject; Node: TTreeNode);
    function ArmarEstructura: boolean;
  private
    { Private declarations }
  public
    { Public declarations }
	Function Inicializar: boolean;
  end;

var
  FormDiccionarioDatos: TFormDiccionarioDatos;

implementation

{$R *.dfm}

function TFormDiccionarioDatos.Inicializar: boolean;
var
    S: TSize;
begin
	S := GetFormClientSize(Application.MainForm);
	SetBounds(0, 0, S.cx, S.cy);
    Repaint;
    txtServidor.Text := '';
    txtBase.Text     := '';
    Result := True;
end;

procedure TFormDiccionarioDatos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    DMConnInformes.BaseInformes.Close;
	Action := caFree;
end;

procedure TFormDiccionarioDatos.BtnSalirClick(Sender: TObject);
begin
    Close;
end;

procedure TFormDiccionarioDatos.TreeViewChange(Sender: TObject;
  Node: TTreeNode);
begin
//    txt_Descripcion.Text := AnsiString(Node.Data^);
end;

function TFormDiccionarioDatos.ArmarEstructura: boolean;
resourcestring
    MSG_TABLAS = 'Tablas';
var
    DescripcionNodo: ^AnsiString;
    miNodoRaiz, miNodoTabla, miNodoCampo, miNodoDetalleCampo: TTreeNode;
begin
    Result := False;
    // 1- Me conecto a la base de datos seleccionada
    if not DMConnInformes.ConectarServer(TrimRight(txtServidor.Text), TrimRight(txtBase.Text), CONST_USUARIO_INFORMES_STANDARD, EmptyStr) then Exit;	// Rev. 2 (SS 744)
    // 2- Obtengo los datos de las tablas
	if not OpenTables([qry_Tablas]) then
		Result := False
	else begin
    	Cursor := crHourGlass;
        New(DescripcionNodo);
        DescripcionNodo^ := MSG_TABLAS;
        miNodoRaiz := TreeView.Items.Add(nil, MSG_TABLAS + ' (' + TrimRight(txtServidor.Text) +
          ' - ' + TrimRight(txtBase.Text) + ')');
        miNodoRaiz.Data := DescripcionNodo;
       	While not qry_Tablas.Eof do begin
            New(DescripcionNodo);
            DescripcionNodo^ := Trim(qry_Tablas.FieldByName('Descripcion').AsString);
            miNodoTabla := TreeView.Items.AddChild(miNodoRaiz, Trim(qry_Tablas.FieldByName('Tabla').AsString));
            miNodoTabla.Data := DescripcionNodo;
            qry_Campos.Parameters.ParamByName('Tabla').Value := Trim(qry_Tablas.FieldByName('Tabla').AsString);
           	if OpenTables([qry_Campos]) then  begin
                while not qry_Campos.Eof do begin
                    New(DescripcionNodo);
                    DescripcionNodo^ := Trim(qry_Campos.FieldByName('Descripcion').AsString);
                    miNodoCampo := TreeView.Items.AddChild(miNodoTabla, Trim(qry_Campos.FieldByName('Columna').AsString));
                    miNodoCampo.Data := DescripcionNodo;
                    New(DescripcionNodo);
                    DescripcionNodo^ := '';
                    miNodoDetalleCampo := TreeView.Items.AddChild(miNodoCampo, 'Tipo: ' + Trim(qry_Campos.FieldByName('Tipo').AsString));
                    miNodoDetalleCampo.Data := DescripcionNodo;
                    New(DescripcionNodo);
                    DescripcionNodo^ := '';
                    miNodoDetalleCampo := TreeView.Items.AddChild(miNodoCampo, 'Longitud: ' + Trim(qry_Campos.FieldByName('Longitud').AsString));
                    miNodoDetalleCampo.Data := DescripcionNodo;
                    New(DescripcionNodo);
                    DescripcionNodo^ := '';
                    miNodoDetalleCampo := TreeView.Items.AddChild(miNodoCampo, 'Nulos: ' + Trim(qry_Campos.FieldByName('Nulos').AsString));
                    miNodoDetalleCampo.Data := DescripcionNodo;
                    qry_Campos.Next;
                end;
                qry_Campos.Close;
            end;
     	    qry_Tablas.Next;
        end;
        Result := True;
        qry_Tablas.Close;
        miNodoRaiz.Expanded := True;
        miNodoRaiz.Selected := True;
        Cursor := crDefault;
    end;
end;

{******************************** Function Header ******************************
Function Name: BtnVerClick
Author : llazarte
Date Created : 31/01/2006
Description : Busca el diccionario de datos de la base seleccionada
Parameters : Sender: TObject
Return Value : None
*******************************************************************************}

procedure TFormDiccionarioDatos.BtnVerClick(Sender: TObject);
resourcestring
    TXT_FALTA_SERVER    = 'Falta ingresar el servidor';
    MSG_WARNING         = 'Atenci�n';
    TXT_FALTA_BASEDATOS = 'Falta ingresar la base de datos';
begin
    if TrimRight(txtServidor.Text) = '' then begin
        MsgBoxBalloon(TXT_FALTA_SERVER, MSG_WARNING, MB_ICONINFORMATION, txtServidor);
        Exit;
    end;
    if TrimRight(txtBase.Text) = '' then begin
        MsgBoxBalloon(TXT_FALTA_BASEDATOS, MSG_WARNING, MB_ICONINFORMATION, txtBase);
        Exit;
    end;
    TreeView.Items.Clear;
    txtServidor.SaveHistory;
    txtBase.SaveHistory;
    ArmarEstructura;
end;

end.
