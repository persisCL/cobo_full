{********************************** File Header ********************************
File Name   : FrmEditarDomicilio.pas
Author      :
Date Created:
Language    : ES-AR
Description :
*******************************************************************************
--------------------------------------------------------------------------------
Historial de Revisiones
--------------------------------------------------------------------------------
Date		: 24-Feb-2004
Author		: Calani, Daniel <dcalni@dpsautomation.com>
Description : Agregado del form de Busqueda de Calle
--------------------------------------------------------------------------------
Historial de Revisiones
--------------------------------------------------------------------------------
Date		: 23-Mar-2004
Author		: Gonzalez, Dami�n <dgonzalez@dpsautomation.com>
Description : Agregado del Frame del Domicilio.
ATENCION: Ojo al cambiar las medidas y los controles ya que las medidas se
modifican en tiempo de ejecuci�n seg�n la calle sea normalizada o no.
--------------------------------------------------------------------------------
Historial de Revisiones
--------------------------------------------------------------------------------
Date		: 07-Abr-2004
Author		: Calani, Daniel <dcalani@dpsautomation.com>
Description : Correcion de errores, como ser: No permitia cargar domicilios relacionados
--------------------------------------------------------------------------------
Historial de Revisiones
--------------------------------------------------------------------------------
Date		: 28-Abr-2004
Author		: Calani, Daniel <dcalani@dpsautomation.com>
Description : Se sacaron los domicilios relacionados.
*******************************************************************************}

unit FrmEditarDomicilio;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,FreDomicilio,RStrings, StdCtrls, DPSControls, ExtCtrls,Peatypes,UtilDB,
  PeaProcs, Util, UtilProc;

type
  TformEditarDomicilio = class(TForm)
    FrameDomicilio: TFrameDomicilio;
    cbTipoDomicilio: TComboBox;
    lbl_TipoDomicilio: TLabel;
    pnlInferior: TPanel;
    chkDomicilioEntrega: TCheckBox;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    procedure BtnCancelarClick(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
  private
    { Private declarations }
    FRegistrodomicilio:TDatosDomicilio;
    function GetRegistroDomicilio: TDatosDomicilio;
    function GetDomicilioSimple:AnsiString;
    function GetDomicilioCompleto:AnsiString;
    procedure Habilitar(Control: TWinControl);
  public
    { Public declarations }
    property DomicilioSimple:AnsiString read GetDomicilioSimple;
    property DomicilioCompleto: AnsiString read GetDomicilioCompleto;
    property RegistroDomicilio:TDatosDomicilio read GetRegistroDomicilio;
    Function Inicializar(CaptionAMostar:String; RegistroDomicilio:TDatosDomicilio; CodigoTipoDomicilio: integer = TIPO_DOMICILIO_PARTICULAR; MostrarDomicioEntrega:Boolean=True; PantallaAdhesion: Boolean = False; Modo: TStateConvenio = scAlta): Boolean;
    Class procedure LimpiarRegistroDomicilio(var Registro: TDatosDomicilio);
  end;

var
  formEditarDomicilio: TformEditarDomicilio;

implementation

uses DMConnection;

{$R *.dfm}

procedure TformEditarDomicilio.BtnCancelarClick(Sender: TObject);
begin
    close;
end;

function TformEditarDomicilio.GetRegistroDomicilio: TDatosDomicilio;
begin
    with FRegistrodomicilio do begin
        Descripcion:=FrameDomicilio.DescripcionCalle; //DomicilioSimple;
        CodigoTipoDomicilio:=iif(cbTipoDomicilio.ItemIndex < 0 ,-1,Ival(Trim(StrRight(cbTipoDomicilio.Text, 10))));
        CodigoCalle:=FrameDomicilio.CodigoCalle;
        CodigoSegmento:=FrameDomicilio.CodigoSegmento;
        CodigoTipoCalle:=-1;
        NumeroCalle:=FormatearNumeroCalle(FrameDomicilio.Numero);
        NumeroCalleSTR:=FrameDomicilio.Numero;
        Piso:=FrameDomicilio.Piso;
        Dpto:=FrameDomicilio.Depto;
        Detalle:=FrameDomicilio.Detalle;
        CodigoPostal:=FrameDomicilio.CodigoPostal;
        CodigoPais:=FrameDomicilio.Pais;
        CodigoRegion:=FrameDomicilio.Region;
        CodigoComuna:=FrameDomicilio.Comuna;
        DescripcionCiudad:=FrameDomicilio.Ciudad;
        CalleDesnormalizada:=iif(FrameDomicilio.EsCalleNormalizada,'',FrameDomicilio.DescripcionCalle);
        DomicilioEntrega:=chkDomicilioEntrega.Checked;
        Normalizado:=FrameDomicilio.EsNormalizado;
    end;

    result:=FRegistrodomicilio;
end;

Function TformEditarDomicilio.Inicializar(CaptionAMostar:String; RegistroDomicilio:TDatosDomicilio; CodigoTipoDomicilio: integer = TIPO_DOMICILIO_PARTICULAR; MostrarDomicioEntrega:Boolean=True; PantallaAdhesion: Boolean = False; Modo: TStateConvenio = scAlta): Boolean;
begin
    self.Caption:=CaptionAMostar;
    FRegistrodomicilio:=RegistroDomicilio;
    chkDomicilioEntrega.Visible:=MostrarDomicioEntrega;
    if not(chkDomicilioEntrega.Visible) then chkDomicilioEntrega.Checked:=True
    else chkDomicilioEntrega.Checked:=False;
    cbTipoDomicilio.Visible:=PantallaAdhesion;
    lbl_TipoDomicilio.Visible:=PantallaAdhesion;
    CargarTipoDomicilio(DMConnections.BaseCAC, cbTipoDomicilio, CodigoTipoDomicilio);
    if trim(FRegistrodomicilio.CodigoPais)='' then
        FrameDomicilio.Inicializa()
    else begin
        FrameDomicilio.Inicializa(FRegistrodomicilio.CodigoPais,FRegistrodomicilio.CodigoRegion,FRegistrodomicilio.CodigoComuna);
        FrameDomicilio.CargarDatosDomicilio(FRegistrodomicilio);
    end;

    if Modo = scNormal then begin
        EnableControlsInContainer(self,False);
        Habilitar(BtnCancelar);
    end;

    result:=True;
end;

class procedure TformEditarDomicilio.LimpiarRegistroDomicilio(
  var Registro: TDatosDomicilio);
begin
    with Registro do begin
        //IndiceDomicilio :integer;
        Descripcion := '';
        CodigoDomicilio := -1;
        CodigoTipoDomicilio := -1;
        CodigoCalle := -1;
        NumeroCalleSTR := '';
        NumeroCalle:=-1;
        Dpto := '';
        Detalle := '';
        CodigoPostal := '';
        CodigoPais := PAIS_CHILE;
		CodigoRegion := Trim(QueryGetValue(DMConnections.BaseCAC, 'SELECT dbo.CONST_REGION_METROPOLITANA()'));
		CodigoComuna := '';
        CodigoCiudad := '';
        DescripcionCiudad := '';
        CalleDesnormalizada := '';
        DomicilioEntrega := false;;
        CodigoPaisRelacionadoUno := PAIS_CHILE;;
        CodigoRegionRelacionadoUno := Trim(QueryGetValue(DMConnections.BaseCAC, 'SELECT dbo.CONST_REGION_METROPOLITANA()'));
        CodigoComunaRelacionadoUno  := '';
        CodigoCalleRelacionadaUno := -1;
        NumeroCalleRelacionadaUno := '';
        CodigoPaisRelacionadoDos  := PAIS_CHILE;
        CodigoRegionRelacionadoDos := Trim(QueryGetValue(DMConnections.BaseCAC, 'SELECT dbo.CONST_REGION_METROPOLITANA()'));
        CodigoComunaRelacionadoDos  := '';
        CodigoCalleRelacionadaDos := -1;
        NumeroCalleRelacionadaDos := '';
    end;

end;

procedure TformEditarDomicilio.BtnAceptarClick(Sender: TObject);
begin
    if FrameDomicilio.ValidarDomicilio then begin
        if FrameDomicilio.EsCalleNormalizada and (FrameDomicilio.CodigoPostal = '') then FrameDomicilio.SetCodigoPostal;
        ModalResult := mrOk;
    end;
end;

function TformEditarDomicilio.GetDomicilioSimple: AnsiString;
begin
    result:=FrameDomicilio.DomicilioSimple;
end;

function TformEditarDomicilio.GetDomicilioCompleto: AnsiString;
begin
    Result:=FrameDomicilio.DomicilioCompleto;
end;

procedure TformEditarDomicilio.Habilitar(Control: TWinControl);
begin
    if Control <> nil then Begin
        Control.Enabled := True;
        Habilitar(Control.Parent);
    end;
end;

end.
