{********************************** File Header ********************************
File Name : XMLCAC.pas
Author : gcasais
Date Created: 29/09/2006
Language : ES-AR
Description : Generaci�n de Archivos XML ST5

Revision : 1
    Author : jjofre
    Date : 12/04/2010
    Description : SS 468: se modifica GenerarNovedadXML
                agergando el tag <depto> </depto>
                para el alta y modificacion de convenio

Revision : 4
    Author : pdominguez
    Date : 29/07/2010
    Description : SS 449 - CAC - Orden Acciones Vehiculos XML
        - Se modific� la funci�n GenerarNovedadXML.

Firma       : SS_1428_NDR_20151216
Descripcion : En el XML en la seccion domicilios, aunque los campos esten vacios se envian de todas formas
******************************************************************************}
unit XMLCAC;

interface

Uses
    Windows, Classes, Util, UtilDB, ADODB, SysUtils, DateUtils, Variants, httpApp, DB;

const
    // Constante con el tipo de evento a crear en la generaci�n de XML de Novedades.
    CODIGO_TIPO_EVENTO_GENERAR_XML = 213;

function HayCaracteresProhibidosEnXML(Cadena: AnsiString): Boolean;
function GenerarNovedadXML(Const DBConnection: TADOConnection; const CodigoConvenio, CodigoConcesionariaNativa: Integer; const OutputFileLOcation: AnsiString; Codificar: Boolean; out DescriError: AnsiString): Boolean;
procedure CrearEventoSOR(Conn: TADOConnection; CodigoTipoEvento, CodigoSistema: Integer;
    CodigoUsuario, App, Detalle: AnsiString);
function DateTimeToST5DateTime(const Fecha: TDateTime; Tipo: Smallint): string;

implementation

{******************************** Function Header ******************************
Function Name: HayCaracteresProhibidosEnXML
Author : ggomez
Date Created : 27/09/2006
Description : Retorna True si en la cadena existen lso siguientes caracteres:
    &, <, >, ".
Parameters : Cadena: AnsiString
Return Value : Boolean
*******************************************************************************}
function HayCaracteresProhibidosEnXML(Cadena: AnsiString): Boolean;
begin
    Result := False;
    Result := Result or (Pos('&', Cadena) <> 0);
    Result := Result or (Pos('<', Cadena) <> 0);
    Result := Result or (Pos('>', Cadena) <> 0);
    Result := Result or (Pos('"', Cadena) <> 0);
end;


{******************************** Function Header ******************************
Function Name: DateTimeToST5DateTime
Author :
Date Created :
Description : Formatea la fecha para obtener parte del nombre del archivo XML o
    el valor del campo Instal seg�n lo recibido en param Tipo.
Parameters : const Fecha: TDateTime; Tipo: Smallint
Return Value : string
*******************************************************************************}
function DateTimeToST5DateTime(const Fecha: TDateTime; Tipo: Smallint): string;
var
    dd, mm, aaaa, hh24, mi, ss, ms: word;
begin
    DecodeDateTime(Fecha, aaaa, mm, dd, hh24, mi, ss, ms);
    case Tipo of
        0:  begin
              result :=
                Istr0(aaaa, 4)  +
                Istr0(mm, 2)    +
                Istr0(dd, 2)    +
                Istr0(hh24, 2)  +
                Istr0(mi, 2)    +
                Istr0(ss, 2);
            end;
        1:  begin
                result :=
                    Istr0(dd, 2)    +
                    Istr0(mm, 2)    +
                    Istr0(aaaa, 4)  + ' ' +
                    Istr0(hh24, 2)  +
                    Istr0(mi, 2)    +
                    Istr0(ss, 2);
            end;
    end;
end;

{-----------------------------------------------------------------------------
  Procedure Name: GenerarNovedadXML
  Author:    ddiaz
  Date Created: 15/06/2006
  Description: Genera el archivo XML de Entradas (novedades) de cambios en los convenios para
                informar al RNUT
  Parameters: La conexi�n, el c�odigo del convenio modificado, la ubicaci�n del archivo en donde se generar� el XML
                una variable de salida informando descripci�n del error si lo hubo
  Return Value: boolean

  Revision 1:
      Author : ggomez
      Date : 27/09/2006
      Description : En las generaciones de datos de tags que se requiere
        codificaci�n para incorporarlo en el XML, agregu� el uso de la
        FN HayCaracteresProhibidosEnXML, y cambi� para usar htmlEncode en lugar
        de httpEncode.

Revision 2:
    Author : ggomez
    Date : 02/10/2006
    Description : Pas� el borrado de las tablas temporales al finally, asi
        siempre se borrar�n.

Revision 3:
    Author : ggomez
    Date : 10/10/2006
    Description : modifiqu� el c�digo para que en el caso de no poder crear el
        archivo temporal, el mensaje de error asociado sea m�s especifico para el
        caso.

Revision 4:
    Author : ggomez
    Date : 28/10/2006
    Description : Al SP BorrarXML, le pas� como par�metro @SPID el SPID con el
      que ejecut� el SP GenerarXML. Antes se estaba pasando Null, lo cual hac�a
      que no se borraran los datos (Ver SP Novedades_BorrarDatosXML).
      No es necesario llamar al SP que Borra desde el Delphi pues el SP GenerarXML
      borra los datos de la sesi�n, antes de realizar otra operaci�n.

Revision : 6
    Author : pdominguez
    Date : 29/07/2010
    Description: SS 449 - CAC - Orden Acciones Vehiculos XML.
        - Se a�ade al proceso de los registros identificados con el ID 5,
        los registros identificados con el ID 6 y 7.
        - Se deshabilita parte de la Rev. 5.
-----------------------------------------------------------------------------}
function GenerarNovedadXML(Const DBConnection: TADOConnection; const CodigoConvenio, CodigoConcesionariaNativa: Integer; const OutputFileLOcation: AnsiString; Codificar: Boolean; out DescriError: AnsiString): Boolean;
resourcestring
    MSG_ERROR_ARCHIVO = 'Error al escribir en el archivo XML.';
    MSG_ERROR_spGENERARXML = 'Error al procesar el procedimiento almacenado GenerarXML.';
    MSG_ERROR_spARCHIVONRO = 'Error al procesar el procedimiento almacenado ArchivoNro.';
    MSG_ERROR_spBorrarXML = 'Error al procesar el procedimiento almacenado BorrarXML.';
    MSG_ERROR_spGENERAR_VACIO = 'No existen cambios para ese c�digo.';
    MSG_ERROR_EXECSP = 'Fall� la ejecuci�n del Procedimiento Almacenado.';
    MSG_COPY_FILE_ERROR = 'Ha Ocurrido un error intentando copiar el archivo %s' + CRLF + 'a %s';
    MSG_RENAME_ERROR = 'No se ha podido renombrar el archivo temporal %s';
    MSG_ERROR_ARCHIVO_TEMPORAL = 'Error al generar el archivo temporal para el XML: %s';

var
    spGenerarXML, spBorrarXML, spArchivoNro: TADOStoredProc;
    ArchivoXML: TextFile;
    ArchivoNro: Integer;
    FechaHora, LocalFile: string;
    // Para almacenar el SPID con el que ejecut� el SP Novedades_GeneracionXML.
    SqlSPID: Integer;
    BookMark:TBookMark;                                                         //SS_638_NDR_20150811
begin
    // Inicializar la variable con -1 para indicar que no hay ning�n SPID..
    SqlSPID := -1;
    Result := False;
    spGenerarXML := TADOStoredProc.Create(nil);
    spArchivoNro := TADOStoredProc.Create(nil);
    spBorrarXML  := TADOStoredProc.Create(nil);
    spGenerarXML.Connection := DBConnection;
    spGenerarXML.ProcedureName := 'Novedades_GeneracionXML';
    try
        try
            spGenerarXML.Parameters.Refresh;
            spGenerarXML.Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
            spGenerarXML.Parameters.ParamByName('@SPID').Value := Null;
            spGenerarXML.Open;
            // Obtener el SPID
            SqlSPID := spGenerarXML.Parameters.ParamByName('@SPID').Value;
        except
            on e: Exception do begin
                DescriError := e.Message;
                Exit;
            end;
        end;

        // si el sp esta vac�o no se detectaron cambios.
        if spGenerarXML.isEmpty then begin
            DescriError := MSG_ERROR_spGENERAR_VACIO;
            Result := True;
            Exit;
        end;

        try
            //Obtengo ruta y nombres temporales para generar el XML
            //Creo el archivo
            LocalFile := GetTempDir + TempFile + '.xml';
            AssignFile(ArchivoXML, LocalFile);
            Rewrite(ArchivoXML);
        except
            on e: Exception do begin
                DescriError := Format(MSG_ERROR_ARCHIVO_TEMPORAL, [LocalFile]) + CRLF + e.Message ;
                Exit;
            end;

        end;

        try
            Writeln(ArchivoXML, '<?xml version="1.0" encoding="UTF-8"?>');
            Writeln(ArchivoXML, '<RNUT>');
            while not spGenerarXML.Eof do begin
                FechaHora:= EmptyStr;
                //pregunto por cada tipo de registro: 1 Contrato / 2 EntidadTitular / 3 EntidadRoles / 4 Direccion / 5 VehiculoTag
                if (copy(spGenerarXML.FieldByName('ClaseObjeto').Value, 1, 1) = '1') then begin
                    Write(ArchivoXML, #09);
                    Writeln(ArchivoXML, '<contrato>');
                    Write(ArchivoXML, #09#09);
                    Writeln(ArchivoXML, '<concesionaria>'   + Trim(spGenerarXML.FieldByName('Contrato_Concesionaria').AsString)+ '</concesionaria>');
                    //si es baja salgo luego de generar el encabezado. y de escribir Accion, FechaBaja (como atributo), Codigo y Estado.
                    //me voy y fuera del bucle cierro los tags que abri mas arriba.
                   Write(ArchivoXML, #09#09);
                    if (spGenerarXML.FieldByName('Contrato_Accion').Value = 3) then begin
                        FechaHora:=DateTimeToST5DateTime(spGenerarXML.FieldByName('Contrato_FechaAccion').AsDateTime,1);
                        Writeln(ArchivoXML, '<accion fechaAccion="' + FechaHora + '">' + Trim(spGenerarXML.FieldByName('Contrato_Accion').AsString) + '</accion>');
                        Write(ArchivoXML, #09#09);
                        Writeln(ArchivoXML, '<codigo>' + Trim(spGenerarXML.FieldByName('Contrato_Codigo').AsString) + '</codigo>');
                        Break;
                    end;
                    Writeln(ArchivoXML, '<accion>' + Trim(spGenerarXML.FieldByName('Contrato_Accion').AsString) + '</accion>');
                    Write(ArchivoXML, #09#09);
                    Writeln(ArchivoXML, '<codigo>' + Trim(spGenerarXML.FieldByName('Contrato_Codigo').AsString) + '</codigo>');
                    if not spGenerarXML.FieldByName('Contrato_Estado').IsNull then begin //si no es nulo (en las altas) se informa el estado.
                        Write(ArchivoXML, #09#09);
                        Writeln(ArchivoXML, '<estado>' + Trim(spGenerarXML.FieldByName('Contrato_Estado').AsString)+ '</estado>');
                    end;
                    //spGenerarXML.Next;//**************************************************************/
                end else if StrToInt((copy(spGenerarXML.FieldByName('ClaseObjeto').AsString, 1, 1))) in [2, 3] then begin // si es EntidadTitular o EntidadRol lleno la misma estructura en el tag <entidad>
                      Write(ArchivoXML, #09#09);
                      Writeln(ArchivoXML, '<entidad>');
                      Write(ArchivoXML, #09#09#09);
                      Writeln(ArchivoXML, '<accion>' + Trim(spGenerarXML.FieldByName('Entidad_Accion').AsString) + '</accion>');
                      Write(ArchivoXML, #09#09#09);
                      Writeln(ArchivoXML, '<RUT>' + Trim(spGenerarXML.FieldByName('Entidad_RUT').AsString) + '</RUT>');
                      Write(ArchivoXML, #09#09#09);
                      Writeln(ArchivoXML, '<DV>' + Trim(spGenerarXML.FieldByName('Entidad_DV').AsString) + '</DV>');
                      Write(ArchivoXML, #09#09#09);
                      Writeln(ArchivoXML, '<tipo>' + Trim(spGenerarXML.FieldByName('Entidad_Tipo').AsString) + '</tipo>');
                      Write(ArchivoXML, #09#09#09);
                      Writeln(ArchivoXML, '<nombre>' +
                          iif(Codificar or HayCaracteresProhibidosEnXML(Trim(spGenerarXML.FieldByName('Entidad_Nombre').AsString)),
                              htmlEncode(Trim(spGenerarXML.FieldByName('Entidad_Nombre').AsString)),
                              Utf8Encode(Trim(spGenerarXML.FieldByName('Entidad_Nombre').AsString))) +
                          '</nombre>');

                      Write(ArchivoXML, #09#09#09);
                      if spGenerarXML.FieldByName('Entidad_Tipo').AsInteger = 1 then begin
                          Writeln(ArchivoXML, '<apellidop>' +
                              iif(Codificar or HayCaracteresProhibidosEnXML(Trim(spGenerarXML.FieldByName('Entidad_ApellidoP').AsString)),
                                  htmlEncode(Trim(spGenerarXML.FieldByName('Entidad_ApellidoP').AsString)),
                                  utf8Encode(Trim(spGenerarXML.FieldByName('Entidad_ApellidoP').AsString ))) +
                              '</apellidop>');
                          Write(ArchivoXML, #09#09#09);
                          if not spGenerarXML.FieldByName('Entidad_ApellidoM').IsNull then begin
                              Writeln(ArchivoXML, '<apellidom>' +
                                  iif(Codificar or HayCaracteresProhibidosEnXML(Trim(spGenerarXML.FieldByName('Entidad_ApellidoM').AsString)),
                                      htmlEncode(Trim(spGenerarXML.FieldByName('Entidad_ApellidoM').AsString)),
                                      utf8Encode(Trim(spGenerarXML.FieldByName('Entidad_ApellidoM').AsString))) +
                                  '</apellidom>');
                              Write(ArchivoXML, #09#09#09);
                          end;
                      end else begin
                          if not spGenerarXML.FieldByName('Entidad_Fantasia').IsNull then begin
                              Writeln(ArchivoXML, '<fantasia>' +
                                  iif(Codificar or HayCaracteresProhibidosEnXML(Trim(spGenerarXML.FieldByName('Entidad_Fantasia').AsString)),
                                      htmlEncode(Trim(spGenerarXML.FieldByName('Entidad_Fantasia').AsString)),
                                      utf8Encode(Trim(spGenerarXML.FieldByName('Entidad_Fantasia').AsString))) +
                                  '</fantasia>');
                              Write(ArchivoXML, #09#09#09);
                          end;
                          Writeln(ArchivoXML, '<giro>' +
                              iif(Codificar or HayCaracteresProhibidosEnXML(Trim(spGenerarXML.FieldByName('Entidad_Giro').AsString)),
                                  htmlEncode(Trim(spGenerarXML.FieldByName('Entidad_Giro').AsString)),
                                  utf8Encode(Trim(spGenerarXML.FieldByName('Entidad_Giro').AsString))) +
                               '</giro>');
                          Write(ArchivoXML, #09#09#09);
                      end;
                      Writeln(ArchivoXML, '<rol>' + Trim(spGenerarXML.FieldByName('Entidad_Rol').AsString) + '</rol>');


                      //-----------------------------------------------------------------------------------------------------------------------
                      //BEGIN : SS_638_NDR_20150811 -------------------------------------------------------------------------------------------
                      //-----------------------------------------------------------------------------------------------------------------------
                      BookMark:=spGenerarXML.GetBookmark;
                      spGenerarXML.First;
                      while not spGenerarXML.Eof do begin
                        if (StrToInt(Copy(spGenerarXML.FieldByName('ClaseObjeto').Value, 1, 1)) in [8,9]) then
                        begin
                              Write(ArchivoXML, #09#09#09);
                              Writeln(ArchivoXML, '<contacto>');
                              Write(ArchivoXML, #09#09#09#09);
                              Writeln(ArchivoXML, '<accion>' + Trim(spGenerarXML.FieldByName('Contacto_Accion').AsString) + '</accion>');
                              Write(ArchivoXML, #09#09#09#09);
                              Writeln(ArchivoXML, '<dato>' + Trim(spGenerarXML.FieldByName('Contacto_Dato').AsString) + '</dato>');
                              Write(ArchivoXML, #09#09#09#09);
                              Writeln(ArchivoXML, '<tipo>' + Trim(spGenerarXML.FieldByName('Contacto_Tipo').AsString) + '</tipo>');
                              Write(ArchivoXML, #09#09#09#09);
                              Writeln(ArchivoXML, '<observacion>' + Trim(spGenerarXML.FieldByName('Contacto_Observacion').AsString) + '</observacion>');
                              Write(ArchivoXML, #09#09#09);
                              Writeln(ArchivoXML, '</contacto>');
                        end;
                        spGenerarXML.Next;
                      end;
                      spGenerarXML.GotoBookmark(BookMark);
                      //-----------------------------------------------------------------------------------------------------------------------
                      //END : SS_638_NDR_20150811 -------------------------------------------------------------------------------------------
                      //-----------------------------------------------------------------------------------------------------------------------
                      Write(ArchivoXML, #09#09);
                      Writeln(ArchivoXML, '</entidad>');
                      //spGenerarXML.Next;//**************************************************************/
                  end else if (copy(spGenerarXML.FieldByName('ClaseObjeto').Value,1,1)= '4') then begin
                          Write(ArchivoXML, #09#09);
                          Writeln(ArchivoXML, '<direccion>');
                          Write(ArchivoXML, #09#09#09);
                          Writeln(ArchivoXML, '<accion>' + Trim(spGenerarXML.FieldByName('Direccion_Accion').AsString) + '</accion>');
                          Write(ArchivoXML, #09#09#09);
                          Writeln(ArchivoXML, '<codigo>' + Trim(spGenerarXML.FieldByName('Direccion_Codigo').AsString) + '</codigo>');
                          Write(ArchivoXML, #09#09#09);
                          // Si la acci�n es alta o modificaci�n escribimos los campos adicionales.
                          // En la pr�ctica cuando se trata de una baja solo se env�a Accion y C�digo.
                          if spGenerarXML.FieldByName('Direccion_Accion').AsInteger in [1, 2] then begin
                              Writeln(ArchivoXML, '<tipo>' + Trim(spGenerarXML.FieldByName('Direccion_Tipo').AsString) + '</tipo>');
                              Write(ArchivoXML, #09#09#09);
                              Writeln(ArchivoXML, '<calle>' +
                                  iif(Codificar or HayCaracteresProhibidosEnXML(Trim(spGenerarXML.FieldByName('Direccion_Calle').AsString)),
                                      htmlEncode(Trim(spGenerarXML.FieldByName('Direccion_Calle').AsString)),
                                      utf8Encode(Trim(spGenerarXML.FieldByName('Direccion_Calle').AsString))) +
                                  '</calle>');
                              Write(ArchivoXML, #09#09#09);
                              Writeln(ArchivoXML, '<comuna>' +
                                  iif(Codificar or HayCaracteresProhibidosEnXML(Trim(spGenerarXML.FieldByName('Direccion_Comuna').AsString)),
                                      htmlEncode(Trim(spGenerarXML.FieldByName('Direccion_Comuna').AsString)),
                                      utf8Encode(Trim(spGenerarXML.FieldByName('Direccion_Comuna').AsString))) +
                                  '</comuna>');
                              Write(ArchivoXML, #09#09#09);
                              Writeln(ArchivoXML, '<altura>' + Trim(spGenerarXML.FieldByName('Direccion_Altura').AsString) + '</altura>');
                              //Revision 1
                              if Trim(spGenerarXML.FieldByName('Direccion_Depto').AsString) <>'' then begin
                                  Write(ArchivoXML, #09#09#09);
                                  Writeln(ArchivoXML, '<depto>' +
                                      iif(Codificar or HayCaracteresProhibidosEnXML(Trim(spGenerarXML.FieldByName('Direccion_Depto').AsString)),
                                      htmlEncode(Trim(spGenerarXML.FieldByName('Direccion_Depto').AsString)),
                                      utf8Encode(Trim(spGenerarXML.FieldByName('Direccion_Depto').AsString))) +
                                      '</depto>');
                              end                                                                 //SS_1428_NDR_20151216
                              else                                                                //SS_1428_NDR_20151216
                              begin                                                               //SS_1428_NDR_20151216
                                  Write(ArchivoXML, #09#09#09);                                   //SS_1428_NDR_20151216
                                  Writeln(ArchivoXML, '<depto/></depto>');                         //SS_1428_NDR_20151216
                              end;                                                                //SS_1428_NDR_20151216
                              //End Revision 1
                              if Trim(spGenerarXML.FieldByName('Direccion_Zip').AsString) <>'' then begin
                                  Write(ArchivoXML, #09#09#09);
                                  Writeln(ArchivoXML, '<zip>' + Trim(spGenerarXML.FieldByName('Direccion_Zip').AsString) + '</zip>');
                              end                                                                 //SS_1428_NDR_20151216
                              else                                                                //SS_1428_NDR_20151216
                              begin                                                               //SS_1428_NDR_20151216
                                  Write(ArchivoXML, #09#09#09);                                   //SS_1428_NDR_20151216
                                  Writeln(ArchivoXML, '<zip></zip>');                             //SS_1428_NDR_20151216
                              end;                                                                //SS_1428_NDR_20151216
                              if Trim(spGenerarXML.FieldByName('Direccion_Comentario').AsString) <>'' then begin
                                  Write(ArchivoXML, #09#09#09);
                                  Writeln(ArchivoXML, '<comentario>' +
                                      iif(Codificar or HayCaracteresProhibidosEnXML(Trim(spGenerarXML.FieldByName('Direccion_Comentario').AsString)),
                                          htmlEncode(Trim(spGenerarXML.FieldByName('Direccion_Comentario').AsString)),
                                          utf8Encode(Trim(spGenerarXML.FieldByName('Direccion_Comentario').AsString))) +
                                      '</comentario>');
                              end                                                                 //SS_1428_NDR_20151216
                              else                                                                //SS_1428_NDR_20151216
                              begin                                                               //SS_1428_NDR_20151216
                                  Write(ArchivoXML, #09#09#09);                                   //SS_1428_NDR_20151216
                                  Writeln(ArchivoXML, '<comentario></comentario>');               //SS_1428_NDR_20151216
                              end;                                                                //SS_1428_NDR_20151216
                          end;
                          Write(ArchivoXML, #09#09);
                          Writeln(ArchivoXML, '</direccion>');
                          //spGenerarXML.Next;//**************************************************************/
                end else if (StrToInt(Copy(spGenerarXML.FieldByName('ClaseObjeto').Value, 1, 1)) in [5,6,7]) then begin // Rev. 6 (SS 449)
                      Write(ArchivoXML, #09#09);
                      Writeln(ArchivoXML, '<vehiculo>');
                      Write(ArchivoXML, #09#09#09);
                      if spGenerarXML.FieldByName('Vehiculo_Accion').Value= 3 then begin
                          FechaHora:=DateTimeToST5DateTime(spGenerarXML.FieldByName('Vehiculo_FechaAccion').AsDateTime,1);
                          Writeln(ArchivoXML, '<accion fechaAccion="' + FechaHora + '">' + Trim(spGenerarXML.FieldByName('Vehiculo_Accion').AsString) + '</accion>');
                          Write(ArchivoXML, #09#09#09);
                          Writeln(ArchivoXML, '<patente>' + Trim(spGenerarXML.FieldByName('Vehiculo_Patente').AsString) + '</patente>');
                          Write(ArchivoXML, #09#09#09);
                          Writeln(ArchivoXML, '<DV>' + Trim(spGenerarXML.FieldByName('Vehiculo_DV').AsString) + '</DV>');
                          Write(ArchivoXML, #09#09);
                          Writeln(ArchivoXML, '</vehiculo>');
                          spGenerarXML.Next;   //**************************************************************/
                          Continue;   //se gener� la baja. loop
                      end else begin
                          Writeln(ArchivoXML, '<accion>' + Trim(spGenerarXML.FieldByName('Vehiculo_Accion').AsString) + '</accion>');
                          Write(ArchivoXML, #09#09#09);
                      end;
                      Writeln(ArchivoXML, '<patente>' + Trim(spGenerarXML.FieldByName('Vehiculo_Patente').AsString) + '</patente>');
                      Write(ArchivoXML, #09#09#09);
                      Writeln(ArchivoXML, '<DV>' + Trim(spGenerarXML.FieldByName('Vehiculo_DV').AsString) + '</DV>');
                      Write(ArchivoXML, #09#09#09);
                      Writeln(ArchivoXML, '<categoria>' + Trim(spGenerarXML.FieldByName('Vehiculo_Categoria').AsString) + '</categoria>');
                      Write(ArchivoXML, #09#09#09);
                      Writeln(ArchivoXML, '<marca>' +
                          iif(Codificar or HayCaracteresProhibidosEnXML(Trim(spGenerarXML.FieldByName('Vehiculo_Marca').AsString)),
                              htmlEncode(Trim(spGenerarXML.FieldByName('Vehiculo_Marca').AsString)),
                              Utf8Encode(Trim(spGenerarXML.FieldByName('Vehiculo_Marca').AsString))) +
                          '</marca>');
                      if not spGenerarXML.FieldByName('Vehiculo_Modelo').IsNull then begin
                          Write(ArchivoXML, #09#09#09);
                          Writeln(ArchivoXML, '<modelo>' +
                              iif(Codificar or HayCaracteresProhibidosEnXML(Trim(spGenerarXML.FieldByName('Vehiculo_Modelo').AsString)),
                                  htmlEncode(Trim(spGenerarXML.FieldByName('Vehiculo_Modelo').AsString)),
                                  Utf8Encode(Trim(spGenerarXML.FieldByName('Vehiculo_Modelo').AsString))) +
                              '</modelo>');
                      end;
                      Write(ArchivoXML, #09#09#09);
                      Writeln(ArchivoXML, '<anio>' + Trim(spGenerarXML.FieldByName('Vehiculo_Anio').AsString) + '</anio>');
                      if not spGenerarXML.FieldByName('Televia_Accion').IsNull then begin // si es distinto de null hay datos de TAG a procesar
                          Write(ArchivoXML, #09#09#09);
                          Writeln(ArchivoXML, '<TAG>');
                          Write(ArchivoXML, #09#09#09#09);
                          FechaHora:=DateTimeToST5DateTime(spGenerarXML.FieldByName('Televia_instal').AsDateTime,1);
                          Writeln(ArchivoXML, '<accion>' + Trim(spGenerarXML.FieldByName('Televia_Accion').AsString) + '</accion>');
                          Write(ArchivoXML, #09#09#09#09);
                          Writeln(ArchivoXML, '<serie>' + Trim(spGenerarXML.FieldByName('Televia_Serie').AsString) + '</serie>');
                          Write(ArchivoXML, #09#09#09#09);
                          Writeln(ArchivoXML, '<fabricante>' + Trim(spGenerarXML.FieldByName('Televia_Fabricante').AsString) + '</fabricante>');
                          Write(ArchivoXML, #09#09#09#09);
                          Writeln(ArchivoXML, '<caturb>' + Trim(spGenerarXML.FieldByName('Televia_Caturb').AsString) + '</caturb>');
                          Write(ArchivoXML, #09#09#09#09);
                          Writeln(ArchivoXML, '<context>' + Trim(spGenerarXML.FieldByName('Televia_Context').AsString) + '</context>');
                          Write(ArchivoXML, #09#09#09#09);
                          Writeln(ArchivoXML, '<contract>' + Trim(spGenerarXML.FieldByName('Televia_Contract').AsString) + '</contract>');
                          Write(ArchivoXML, #09#09#09#09);
                          Writeln(ArchivoXML, '<estado>' + Trim(spGenerarXML.FieldByName('Televia_Estado').AsString) + '</estado>');
                          Write(ArchivoXML, #09#09#09#09);
                          Writeln(ArchivoXML, '<instal>' + Trim(FechaHora) + '</instal>');
                          Write(ArchivoXML, #09#09#09);
                          Writeln(ArchivoXML, '</TAG>');
                      end;
                      Write(ArchivoXML, #09#09);
                      Writeln(ArchivoXML, '</vehiculo>');
                      //spGenerarXML.Next;//**************************************************************/
                end;
                spGenerarXML.Next;//se reemplaza todos los next por esta linea al final del bucle
            end;   // while
        except // de la escritura del archivo
            on e: Exception do begin
                DescriError := MSG_ERROR_ARCHIVO + CRLF + e.Message ;
                Exit;
            end;
        end;

        //cierro el los tag y el XML
        Write(ArchivoXML, #09);
        Writeln(ArchivoXML, '</contrato>');
        Writeln(ArchivoXML, '</RNUT>');
        CloseFile(ArchivoXML);
        try
            spArchivoNro.Connection := DBConnection;
            spArchivoNro.ProcedureName := 'Convenio_NumeracionXml';
            spArchivoNro.Parameters.Refresh;
            spArchivoNro.Parameters.ParamByName('@Numeracion').Value := Null;
            try
                spArchivoNro.ExecProc;
            except
                on e: Exception do begin
                    DescriError := MSG_ERROR_EXECSP + CRLF + e.Message;
                    Exit;
                end;
            end;
            ArchivoNro := spArchivoNro.Parameters.ParamByName('@Numeracion').Value;
            FechaHora := DateTimeToST5DateTime(Now, 0);
            if not RenameFile(LocalFile, GetTempDir + 'entrada_'+ TRIM(IntToStr(CodigoConcesionariaNativa)) +'_'+ IntToStr(ArchivoNro) + '_' + FechaHora + '.xml') then begin
                DescriError := Format(MSG_RENAME_ERROR, [GetTempDir + TempFile + '.xml']);
                Exit;
            end else begin
                if not FileCopy(GetTempDir + 'entrada_'+ TRIM(IntToStr(CodigoConcesionariaNativa)) +'_'+ IntToStr(ArchivoNro) + '_' + FechaHora + '.xml',
                  GoodDir(OutputFileLocation) + 'entrada_'+ TRIM(IntToStr(CodigoConcesionariaNativa)) +'_'+ IntToStr(ArchivoNro) + '_' + FechaHora + '.xml') then begin
                  //DescriError := Format(MSG_COPY_FILE_ERROR, [GetTempDir + TempFile + '.xml', GoodDir(OutputFileLocation)                                                                                          //SS_638_NDR_20150811
                  DescriError := Format(MSG_COPY_FILE_ERROR, [GetTempDir + 'entrada_'+ TRIM(IntToStr(CodigoConcesionariaNativa)) +'_'+ IntToStr(ArchivoNro) + '_' + FechaHora + '.xml', GoodDir(OutputFileLocation)  //SS_638_NDR_20150811
                  + 'entrada_'+ TRIM(IntToStr(CodigoConcesionariaNativa)) +'_'+ IntToStr(ArchivoNro) + '_' + FechaHora + '.xml']);            //SS_1330_NDR_20150702
                    Exit;
                end;
			end;
        except
            on e: Exception do begin
                DescriError := DescriError + MSG_ERROR_spARCHIVONRO + CRLF + e.Message;
                Exit;
            end;
        end;
        Result := True;
    finally
        try
            spBorrarXML.Close;
            spBorrarXML.Connection := DBConnection;
            spBorrarXML.ProcedureName := 'Novedades_BorrarDatosXML';
            spBorrarXML.Parameters.Refresh;
            spBorrarXML.Parameters.ParamByName('@SPID').Value := SqlSPID;
            spBorrarXML.ExecProc;
        except
            on e: Exception do begin
                DescriError := MSG_ERROR_spBorrarXML + CRLF + e.Message;
            end;
        end;
        spGenerarXML.Close;
        spArchivoNro.Close;
        spBorrarXML.Close;
        FreeAndNil(spGenerarXML);
        FreeAndNil(spArchivoNro);
        FreeAndNil(spBorrarXML);
    end;
end;

{******************************** Function Header ******************************
Function Name: CrearEventoSOR
Author : ggomez
Date Created : 02/10/2006
Description : Registra un evento en el SOR con los datos de los par�metros.
Parameters : Conn: TADOConnection; CodigoTipoEvento, CodigoSistema: Integer; CodigoUsuario, App, Detalle: AnsiString
Return Value : None
*******************************************************************************}
procedure CrearEventoSOR(Conn: TADOConnection; CodigoTipoEvento, CodigoSistema: Integer;
    CodigoUsuario, App, Detalle: AnsiString);
var
    spEventosSOR: TADOStoredProc;
begin
    spEventosSOR :=  TADOStoredProc.Create(nil);
    try
        spEventosSOR.Connection := Conn;
        spEventosSOR.ProcedureName := 'CrearEvento';
        spEventosSOR.Parameters.Refresh;
        spEventosSOR.Parameters.ParamByName('@CodigoTipoEvento').Value := CodigoTipoEvento;
        spEventosSOR.Parameters.ParamByName('@CodigoSistema').Value := CodigoSistema;
        spEventosSOR.Parameters.ParamByName('@CodigoUsuario').Value := CodigoUsuario;
        spEventosSOR.Parameters.ParamByName('@NombreArchivo').Value := App;
        spEventosSOR.Parameters.ParamByName('@Detalle').Value := Detalle;
        spEventosSOR.ExecProc;
    finally
        spEventosSOR.Free;
    end; // finally

end;

end.

