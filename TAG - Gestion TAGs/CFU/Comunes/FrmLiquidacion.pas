unit FrmLiquidacion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, StdCtrls, DBGrids, DMConnection, DB, ADODB, UtilDb, Util,
  Utilproc, DmiCtrls, ExtCtrls, PeaProcs, DPSControls, ComCtrls,
  frmRepoLiquidacion, Buttons, BuscaTab;

type
  TFormLiquidacion = class(TForm)
    btn_Aceptar: TDPSButton;
    btn_Cancelar: TDPSButton;
    spObtenerLiquidacionTurnoEfectivo: TADOStoredProc;
    spAgregarLiquidacionEfectivo: TADOStoredProc;
    spAgregarLiquidacionCupones: TADOStoredProc;
    GrillaBilletes: TStringGrid;
    Label1: TLabel;
    Label2: TLabel;
    edCuponesPAC: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    edCheques: TEdit;
    edCuponesPAT: TEdit;
    spCerrarTurnoLiquidacionCaja: TADOStoredProc;
    spObtenerLiquidacionTurnoCupones: TADOStoredProc;
    procedure btn_AceptarClick(Sender: TObject);
    procedure btn_CancelarClick(Sender: TObject);
    procedure GrillaBilletesSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
    procedure GrillaBilletesKeyPress(Sender: TObject; var Key: Char);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
    FNumeroTurno: integer;
    FLastCol : integer;
    FLastRow : integer;
  public
    { Public declarations }
    function Inicializar(NumeroTurno: Integer): boolean;
  end;

var
  FormLiquidacion: TFormLiquidacion;

implementation

{$R *.dfm}

{ TFormLiquidacion }
var
  Conjunto : set of char;

function TFormLiquidacion.Inicializar(NumeroTurno: Integer): boolean;
resourcestring
    CAPTION_MONEDA          = 'Moneda';
    CAPTION_VALOR           = 'Valor';
    CAPTION_CANTIDAD        = 'Cantidad';

var
	i: integer;
    DescripcionPAT, DescripcionPAC, DescripcionCuentaNumero: AnsiString;
begin
	FNumeroTurno := NumeroTurno;
    DescripcionPAT := '';
    DescripcionPAC := '';
    DescripcionCuentaNumero := '';
	Conjunto :=  [#8, #9, #13, #27, '0'..'9'];

	result := False;
    if ( FNumeroTurno > 0 ) then begin
    	spObtenerLiquidacionTurnoEfectivo.Parameters.ParamByName( '@NumeroTurno' ).Value := FNumeroTurno;
        spObtenerLiquidacionTurnoCupones.Parameters.ParamByName( '@NumeroTurno' ).Value := FNumeroTurno;
		result := (OpenTables([spObtenerLiquidacionTurnoEfectivo, spObtenerLiquidacionTurnoCupones]));

		{-----------------------------------------------------------------}
    	{ Carga las denominaciones de los Billetes y la cantidad que hay
		{-----------------------------------------------------------------}
    	GrillaBilletes.Cells[0,0] := CAPTION_MONEDA;
    	GrillaBilletes.Cells[1,0] := CAPTION_VALOR;
    	GrillaBilletes.Cells[2,0] := CAPTION_CANTIDAD;

		with spObtenerLiquidacionTurnoEfectivo, GrillaBilletes do begin
    		i:= 0;
        	RowCount :=  RecordCount + 1;

    		while not Eof do begin
				inc(i);
        		Cells[0,i] := PadR(Trim(FieldByName('Descripcion').AsString), 300, ' ') + Istr(FieldByName('CodigoDenominacionMoneda').AsInteger);
        		Cells[1,i] := '$' + PadL(FloatToStrF(FieldByName('ValorMonedaLocal').AsFloat, ffFixed,12, 0), 13, ' ');
        		Cells[2,i] := FieldByName('Cantidad').AsString;
        		Next;
    		end;

    		Close;
			Row := 1;
   			Col := 2;
    	end;

		{-----------------------------------------------------------------}
    	{ Carga las cantidades de cupones y cheques
		{-----------------------------------------------------------------}
		with spObtenerLiquidacionTurnoCupones do begin
        	edCheques.Text		:= iif( FieldByName( 'CantidadCheques' ).AsString 	 = '0', '', FieldByName( 'CantidadCheques' ).AsString );
        	edCuponesPAC.Text 	:= iif( FieldByName( 'CantidadCuponesPAC' ).AsString = '0', '', FieldByName( 'CantidadCuponesPAC' ).AsString );
        	edCuponesPAT.Text 	:= iif( FieldByName( 'CantidadCuponesPAT' ).AsString = '0', '', FieldByName( 'CantidadCuponesPAT' ).AsString );
            Close;
    	end;
    end;
end;

procedure TFormLiquidacion.FormActivate(Sender: TObject);
begin
	// Se posiciona sobre la primera celda y entra en modo EDIT
    GrillaBilletes.SetFocus;
	GrillaBilletes.Row := 1;
   	GrillaBilletes.Col := 2;
   	SendMessage(GrillaBilletes.Handle, WM_CHAR, 13, 0); // Fuerza el modo Edit
    // Lo hace en el OnActivate y no en el inicializar porque la ventana
    // debe estar visible para hacer un SetFocus
end;

{-----------------------------------------------------------------------------
  Function Name: btn_AceptarClick
  Author:    flamas
  Date Created: 16/12/2004
  Description: Acepta los datos y los vuelca a la Base de datos
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormLiquidacion.btn_AceptarClick(Sender: TObject);
resourcestring
    // Mensajes de confirmaci�n
    MSG_GUARDAR_LIQUIDACION     = '�Desea guardar la liquidaci�n con la informaci�n que ve en pantalla?';
    CAPTION_CIERRE_TURNO        = 'Cierre de Turno';
    MSG_IMPRIMIR_DETALLE        = '�Desea imprimir el detalle de la liquidaci�n ahora?';
    CAPTION_IMPRIMIR_DETALLE    = 'Imprimir Detalle Liquidaci�n';

    // Mensajes de error
    MSG_FECHA_CIERRE     	    = 'No se pudo actualizar la fecha de cierre de turno.';
    MSG_DETALLE_LIQUIDACION     = 'No se pudo agregar el detalle de liquidaci�n.';
var
	i: integer;
    TodoOk : Boolean;
    f: TFormReporteLiquidacion;
begin
	if MsgBox(MSG_GUARDAR_LIQUIDACION, CAPTION_CIERRE_TURNO, MB_ICONQUESTION or MB_YESNO or MB_DEFBUTTON2) <> IDYES then Exit;

	Screen.Cursor := crHourGlass;

    TodoOk := False;
    try
        DMConnections.BaseCAC.BeginTrans;

		{ Actualiza la Fecha de Cierre de liquidaci�n }
        try
        	with spCerrarTurnoLiquidacionCaja, Parameters do begin
        		ParamByName('@NumeroTurno').Value := FNumeroTurno;
            	ExecProc;
			end;
        except
        	on E: Exception do begin
            	MsgBoxErr(MSG_FECHA_CIERRE, e.message, CAPTION_CIERRE_TURNO, MB_ICONSTOP);
                Exit;
                end;
        end; 


        // Actualizo la Cantidad de Efectivo
        for i := 1 to GrillaBilletes.RowCount - 1 do begin
            try
            	if Ival(GrillaBilletes.Cells[2, i]) > 0 then
                   	with spAgregarLiquidacionEfectivo, Parameters, GrillaBilletes do begin
                    	ParamByName('@NumeroTurno').Value 				:= FNumeroTurno;
                		ParamByName('@CodigoDenominacionMoneda').Value 	:= Ival(strRight(Cells[0, i], 10));
                		ParamByName('@ValorMonedaLocal').Value 		  	:= StrToFloat(StrRight(Cells[1, i], 13)) * 100;
                		ParamByName('@Cantidad').Value 					:= Ival(Cells[2, i]);
                      	ExecProc;
                   end;
            except
                 on E: Exception do begin
                    MsgBoxErr(MSG_DETALLE_LIQUIDACION, e.message, CAPTION_CIERRE_TURNO, MB_ICONSTOP);
                    Exit;
                 end;
            end;
        end;

        // Actualizo la Cantidad de Cupones
        try
        	with spAgregarLiquidacionCupones, Parameters do begin
            	ParamByName('@NumeroTurno').Value			:= FNumeroTurno;
                ParamByName('@CantidadCheques').Value		:= Ival(edCheques.Text);
                ParamByName('@CantidadCuponesTarjetaDebito').Value 	:= Ival(edCuponesPAC.Text);
                ParamByName('@CantidadCuponesTarjetaCredito').Value 	:= Ival(edCuponesPAT.Text);
                ExecProc;
            end;
        except
        	on E: Exception do begin
            	MsgBoxErr(MSG_DETALLE_LIQUIDACION, e.message, CAPTION_CIERRE_TURNO, MB_ICONSTOP);
                Exit;
            end;
        end;

        TodoOk := True;

    finally
        if TodoOk then begin
            DMConnections.BaseCAC.CommitTrans;
        	ModalResult := mrOk;
        end else begin
            DMConnections.BaseCAC.RollbackTrans;
        end;
        Screen.Cursor := crDefault;
    end;

    // Si corresponde, adem�s imprimimos la liquidaci�n
    if TodoOk and (MsgBox(MSG_IMPRIMIR_DETALLE, CAPTION_IMPRIMIR_DETALLE, MB_ICONQUESTION or MB_YESNO) = IDYES) then begin
        Application.CreateForm(TFormReporteLiquidacion, f);
        f.NumeroTurno := FNumeroTurno;
        if f.Inicializa then f.Ejecutar;
        f.Release;
    end;
end;

procedure TFormLiquidacion.btn_CancelarClick(Sender: TObject);
begin
	Close;
end;

procedure TFormLiquidacion.GrillaBilletesSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
begin
	CanSelect := ( ACol = 2 );
	// Hace un trim de las cantidades para que queden alineadas
	GrillaBilletes.Cells[FLastCol, FLastRow] := trim ( GrillaBilletes.Cells[FLastCol, FLastRow] );
    FLastCol := ACol;
    FLastRow := ARow;
end;

procedure TFormLiquidacion.GrillaBilletesKeyPress(Sender: TObject; var Key: Char);
begin
	if  not (Key in Conjunto) then Key := #0;
end;

end.
