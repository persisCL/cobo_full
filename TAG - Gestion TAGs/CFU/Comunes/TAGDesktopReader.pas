unit TAGDesktopReader;

interface

uses
    sysutils, Forms,
    Util, DeclHard, DeclTag;

type
    TTAG = record
        IssuerId: string;
        ContractSerialNumber: Cardinal;
        VehicleClass: byte;
    end;

function LeerTag(var TAG: TTAG; var Error: string): boolean;

implementation

var
    TagReader: TTagReader;
    fTAG: TTAG;
    fHuboError: boolean;
    fError: string;
    fAtributosLeidos: boolean;
    fDetectadoTAG: boolean;

const
    MASTER_ELEMENT_ACCESS_KEY    = #$9E#$3D#$7E#$D1#$41#$A9#$79#$5B#$7B#$EA#$75#$6C#$71#$96#$BD#$F0;

procedure FOnTagConnected(Sender: TObject; LID: Cardinal; AppList: TApplicationList);
var
    i: integer;
    Atr: TAttributeList;
begin
    fDetectadoTAG := true;
	for i := 0 to AppList.nApplications - 1 do begin
        // Solo nos interesa la aplicacion 1, la de peaje...
        if (AppList.Applications[i].ApplicationID = 1) and (AppList.Applications[i].ElementID = 1) then begin
            fTAG.IssuerId := AppList.Applications[i].ContextMark.IssuerId;
        end;
	end;

    // ahora leemos los atributos...
    // solo los 2 que nos interesan
    Atr.nAttribs := 2;
    Atr.Attributes[0] := 1; // ContractSerialNumber
    Atr.Attributes[1] := 17; // VehicleClass

    if not TagReader.LeerAtributos(LID, 1, Atr, MASTER_ELEMENT_ACCESS_KEY) then begin
        fError := 'No se pudieron leer los atributos. La antena dijo: ' + TagReader.GetLastError;
        fHuboError := true;
        exit;
    end;

    if not TagReader.ObtenerAtributo(1, fTAG.ContractSerialNumber, 4) then begin
        fError := 'No se pudo leer el ContractSerialNumber. La antena dijo: ' + TagReader.GetLastError;
        fHuboError := true;
        exit;
    end;

    if not TagReader.ObtenerAtributo(17, fTAG.VehicleClass, 1) then begin
        fError := 'No se pudo leer el VehicleClass. La antena dijo: ' + TagReader.GetLastError;
        fHuboError := true;
        exit;
    end;

    fHuboError := false;
    fAtributosLeidos := true;

//    TagReader.Liberar(LID);
end;

function LeerTag(var TAG: TTAG; var Error: string): boolean;
var
    Fin: int64;
begin
    result := false;
    fHuboError := false;
    fAtributosLeidos := false;
    fDetectadoTAG := false;

    TagReader := TTagReader.Create(nil);
    try
        try
            with TagReader do begin
                DLL := 'combtech.dll';
                ID := 1;
                PollingMode := pmSinglethreaded;
                @OnTagConnected := @FOnTagConnected;
            end;

            TagReader.Active := true;
            if not TagReader.Active then begin
                Error := 'No se pudo activar la antena. ' + TagReader.GetLastError;
                exit;
            end;

            fin := GetMSCounter + 90000; // 90 segundos
            while (fin > GetMSCounter) and not fAtributosLeidos and not fHuboError do application.ProcessMessages;

            if not fAtributosLeidos then begin
                if fHuboError then begin
                    Error := fError;
                end else begin
                    Error := 'Timeout';
                end;
                if not fDetectadoTAG then Error := 'No se detect� TAG. ' + Error;
                exit;
            end;

            // los leimos
            TAG := fTAG;
            result := true;
            TAGReader.Active := false;
        except
            on e: exception do begin
                Error := e.Message;
            end;
        end;
    finally
        TAGReader.free;
    end;
end;

end.
