{-------------------------------------------------------------------------------
 File Name: FrmReclamoEstacionamiento.pas
 Author: DDeMarco
 Date Created:
 Language: ES-AR
 Description: Ingreso y edicion de reclamos

 Revision		: 1
 Author			: Nelson Droguett Sierra
 Date			: 24-Junio-2010
 Description	: (Ref.Fase 2) Agregar concesionaria y subtipo de reclamo


 Firma        : SS-1006-NDR-20120802
 Description  : Validar concesionaria en reclamos


 Firma        : SS_1006P_NDR_20140715
 Description  : Limpiar lista de estacionamientos reclamados
 -------------------------------------------------------------------------------}
unit FrmReclamoEstacionamiento;

interface

uses
  //Reclamo
  DMConnection,                   //Coneccion a base de datos OP_CAC
  Util,                           //CRLF
  UtilDB,                         //Rutinas para base de datos
  UtilProc,                       //Mensajes
  FreContactoReclamo,             //Frame contacto
  FreCompromisoOrdenServicio,     //Frame compromiso
  FreSolucionOrdenServicio,       //Frame solucion
  FreHistoriaOrdenServicio,       //Frame Historia
  FreFuenteReclamoOrdenServicio,  //Frame Fuente Reclamo
  FreNumeroOrdenServicio,         //Frame Numero Orden Servicio
  FreRespuestaOrdenServicio,      //Frame Respuesta Orden Servicio 
  //ImagenesEstacionamientos,              //Muestra la Ventana con la imagen
  FrmEstacionamientosReclamadosAcciones, //Acciones que se hicieron a los Estacionamientos al cerrar el reclamo
  OrdenesServicio,                //Registra la orden de  servicio
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls,  DB, ADODB,RStrings, ExtCtrls,
  VariantComboBox, Grids, DBGrids, ListBoxEx, DBListEx, ImgList, Menus,
  FreConcesionariaReclamoOrdenServicio, FreSubTipoReclamoOrdenServicio;

type
  TFormReclamoEstacionamiento = class(TForm)
    PageControl: TPageControl;
    TabSheetDatos: TTabSheet;
    txtDetalle: TMemo;
    TabSheetProgreso: TTabSheet;
    TabSheetHistoria: TTabSheet;
    FrameHistoriaOrdenServicio: TFrameHistoriaOrdenServicio;
    Label3: TLabel;
    DataSource: TDataSource;
    DBLEstacionamientos: TDBListEx;
    lnCheck: TImageList;
    spObtenerOrdenServicio: TADOStoredProc;
    spObtenerEstacionamientosReclamo: TADOStoredProc;
    PopupResolver: TPopupMenu;
    mnu_Resolver: TMenuItem;
    Mnu_Pendiente: TMenuItem;
    Mnu_Aceptado: TMenuItem;
    Mnu_Denegado: TMenuItem;
    Mnu_Comun: TMenuItem;
    PAbajo: TPanel;
    PDerecha: TPanel;
    CKRetenerOrden: TCheckBox;
    AceptarBTN: TButton;
    btnCancelar: TButton;
    FrameContactoReclamo1: TFrameContactoReclamo;
    FrameCompromisoOrdenServicio1: TFrameCompromisoOrdenServicio;
    FrameSolucionOrdenServicio1: TFrameSolucionOrdenServicio;
    Ldetalledelprogreso: TLabel;
    txtDetalleSolucion: TMemo;
    FrameFuenteReclamoOrdenServicio1: TFrameFuenteReclamoOrdenServicio;
    ilCamara: TImageList;
    FrameNumeroOrdenServicio1: TFrameNumeroOrdenServicio;
    TabSheetRespuesta: TTabSheet;
    FrameRespuestaOrdenServicio1: TFrameRespuestaOrdenServicio;
    FrameConcesionariaReclamoOrdenServicio1: TFrameConcesionariaReclamoOrdenServicio;
    FrameSubTipoReclamoOrdenServicio1: TFrameSubTipoReclamoOrdenServicio;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure AceptarBTNClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure DBLEstacionamientosDrawText(Sender: TCustomDBListEx;Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;var Text: String; var TxtRect: TRect; var ItemWidth: Integer;var DefaultDraw: Boolean);
    procedure DBLEstacionamientosLinkClick(Sender: TCustomDBListEx;Column: TDBListExColumn);
    procedure DBLEstacionamientosCheckLink(Sender: TCustomDBListEx;Column: TDBListExColumn; var IsLink: Boolean);
    procedure DBLEstacionamientosContextPopup(Sender: TObject; MousePos: TPoint;var Handled: Boolean);
    procedure Mnu_ComunClick(Sender: TObject);
  private
    { Private declarations }
    FTipoOrdenServicio : Integer;
    FCodigoOrdenServicio : Integer;
    FListaEstacionamientos : TStringList;
    FEditando : Boolean; //Indica si estoy editando o no
    Function  GetCodigoOrdenServicio : Integer;
    Function  ListaEstacionamientosInclude(Lista : TStringList; Valor : String) : Boolean;
    Function  ListaEstacionamientosExclude(Lista : TStringList; Valor : String) : Boolean;
    Function  ListaEstacionamientosIn(Lista : TStringList ; Valor : String) : Boolean;
    Function  ListaEstacionamientosEmpresaIn(Valor : String; Var Resolucion : Integer) : Boolean;
    Function  ValidarEstadoEstacionamientos : Boolean;
  public
    { Public declarations }
    Function Inicializar(TipoOrdenServicio : Integer; CodigoConvenio : Integer; ListaEstacionamientos : TStringList ) : Boolean;
    Function InicializarEditando(CodigoOrdenServicio : Integer) : Boolean;
    Function InicializarSolucionando(CodigoOrdenServicio : Integer): Boolean;
    Property CodigoOrdenServicio: Integer Read GetCodigoOrdenServicio;
  end;

var
  FormReclamoEstacionamiento : TFormReclamoEstacionamiento;
  FListaEstacionamientosCliente : TStringList; //Guarda los Estacionamientos que el usuario no rechaza
  FListaEstacionamientosEmpresa : TStringList; //Guarda la resolucion de la empresa sobre cada Estacionamiento
  FListaEstacionamientosSeleccionados : TStringList; //Guarda la resolucion de la empresa sobre cada Estacionamiento

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:  DDeMarco
  Date Created:
  Description:  Inicializaci�n de este formulario
  Parameters: TipoOrdenServicio: Integer; CodigoConvenio: Integer = 0
  Return Value: Boolean
-----------------------------------------------------------------------------}
Function TFormReclamoEstacionamiento.Inicializar(TipoOrdenServicio : Integer; CodigoConvenio : Integer; ListaEstacionamientos: TStringList ) : Boolean;

    {-----------------------------------------------------------------------------
      Function Name: ObtenerDatosEstacionamientos
      Author:  lgisuk
      Date Created: 06/02/2006
      Description:  Cargo la grilla de reclamos
      Parameters:
      Return Value: Boolean
    -----------------------------------------------------------------------------}
    Function ObtenerDatosEstacionamientos(Estacionamientos : TStringList) : Boolean;

        {-----------------------------------------------------------------------------
          Function Name: ObtenerEnumeracion
          Author:  lgisuk
          Date Created: 06/02/2006
          Description:  Obtengo la Enumeraci�n
          Parameters:
          Return Value: Boolean
        -----------------------------------------------------------------------------}
        function ObtenerEnumeracion(Lista : TStringList) : String;
        var
            Enum: AnsiString;
            i: Integer;
        begin
            Enum := '';
            for i:= 0 to Lista.Count - 1 do begin
                //Enum := Enum + Lista.Strings[i] + ',';
                //Enum := Enum + Copy( Lista.Strings[i], 0 , Pos('=',Lista.Strings[i])-1) + ',';
                Enum := Enum + ParseParamByNumber(Lista.Strings[i],1,'=') + ',';
            end;
            Delete(Enum, Length(Enum), 1);
            Result := Enum;
        end;

    begin
        with SPObtenerEstacionamientosReclamo do begin
            Close;
            Parameters.ParamByName('@CodigoOrdenServicio').Value := NULL;
            Parameters.ParamByName('@Enumeracion').Value := ObtenerEnumeracion(ListaEstacionamientos);
            Open;
        end;
        Result := True;
    end;

Resourcestring
    MSG_ERROR_INIT =  'Error al inicializar Reclamo Estacionamiento';
    MSG_ERROR = 'Error';
Const
    STR_RECLAMO = 'Reclamo de Estacionamiento - ';
Var
	  Sz: TSize;
begin
    FTipoOrdenServicio := TipoOrdenServicio;
    try
        //si es un reclamo nuevo obtengo los reclamos del stringlist
        if ListaEstacionamientos <> nil then begin
            FListaEstacionamientos := ListaEstacionamientos;
            //cargo la grilla con los Estacionamientos
            ObtenerDatosEstacionamientos(ListaEstacionamientos);
            //obtengo el codigo de convenio
            CodigoConvenio:= QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.ObtenerCodigoConvenioEstacionamiento (' + spObtenerEstacionamientosReclamo.FieldByName('NumCorrEstacionamiento').AsString + ')');
            //la empresa aun no resolvio nada es la primera vez
            FListaEstacionamientosEmpresa.Clear;
        end else begin
            //Creo la lista la voy a cargar una consulta
            FListaEstacionamientos := TStringList.Create;
        end;

        //Centro el form
        CenterForm(Self);
        //ajusto el form al tama�o disponible
        SZ := GetFormClientSize(Application.MainForm);
  	    SetBounds(0, 0, SZ.cx, sz.cy);
        //titulo
        Caption := STR_RECLAMO + QueryGetValue(DMConnections.BaseCAC, Format(
                   'SELECT dbo.ObtenerDescripcionTipoOrdenServicio (%d)',
                   [TipoOrdenServicio]));
        PageControl.ActivePageIndex := 0;
        ActiveControl := txtDetalle;
        Result := FrameContactoReclamo1.Inicializar and
                        FrameCompromisoOrdenServicio1.Inicializar(10) and
                                FrameSolucionOrdenServicio1.Inicializar and
                                        FrameRespuestaOrdenServicio1.Inicializar and
                                        //Rev.1 / 24-Junio-2010 / Nelson Droguett Sierra-----------------
                                        	FrameConcesionariaReclamoOrdenServicio1.Inicializar and
                                            	FrameSubTipoReclamoOrdenServicio1.Inicializar;
                                        //---------------------------------------------------------------        

        if Result then FrameContactoReclamo1.CodigoConvenio := CodigoConvenio;

    except
        on E: Exception do begin
            MsgBoxErr(MSG_ERROR_INIT, E.message, MSG_ERROR, MB_ICONSTOP);
            Result := False;
        end;
    end;

    //termino de inicializar
    FrameFuenteReclamoOrdenServicio1.Inicializar;
    FrameNumeroOrdenServicio1.Visible := False;
    //No estoy Editando
    FEditando := False;
    //Pongo una fecha comprometida por defecto en funcion del reclamo
    if FrameCompromisoOrdenServicio1.FechaCompromiso = NULLDATE then FrameCompromisoOrdenServicio1.FechaCompromiso := ObtenerFechaComprometidaOS(now, FTipoOrdenServicio, FrameFuenteReclamoOrdenServicio1.FuenteReclamo, FrameCompromisoOrdenServicio1.Prioridad);
    AddOSForm(Self);
end;

{-----------------------------------------------------------------------------
  Function Name: InicializarEditando
  Author:  DDeMarco
  Date Created:
  Description: Modifico Reclamo que ya existe
  Parameters: CodigoOrdenServicio: Integer
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFormReclamoEstacionamiento.InicializarEditando(CodigoOrdenServicio: Integer): Boolean;

   {-----------------------------------------------------------------------------
      Function Name: ReadOnly
      Author:  lgisuk
      Date Created: 06/02/2006
      Description:  Cambia al formulario al modo solo lectura
      Parameters:
      Return Value: Boolean
    -----------------------------------------------------------------------------}
    Procedure ReadOnly;
    Const
        STR_EXIT = 'Salir';
    begin
        dblEstacionamientos.PopupMenu := Nil;
        //Comunes
        FrameContactoReclamo1.Deshabilitar;
        FrameCompromisoOrdenServicio1.Enabled := False;
        FrameSolucionordenServicio1.Enabled := False;
        FrameRespuestaOrdenServicio1.Deshabilitar;
        TxtDetalle.ReadOnly := True;
        TxtDetalleSolucion.ReadOnly := True;
        CKRetenerOrden.Visible := False;
        BtnCancelar.Caption := STR_EXIT;
        AceptarBtn.Visible := False;
        //Rev.1 / 29-Junio-2010 / Nelson Droguett Sierra ----------------------------------------------------------
    	FrameConcesionariaReclamoOrdenServicio1.Enabled:=False;
        FrameSubTipoReclamoOrdenServicio1.Enabled:=False;
    	//FinRev.1-------------------------------------------------------------------------------------------------
    end;

    {-----------------------------------------------------------------------------
      Function Name: ObtenerDatosEstacionamientos
      Author:  lgisuk
      Date Created: 06/02/2006
      Description:  Obtiene los datos de los Estacionamientos
      Parameters:
      Return Value: Boolean
    -----------------------------------------------------------------------------}
    Function ObtenerDatosEstacionamientos (CodigoOrdenServicio : Integer) : Boolean;

        {-----------------------------------------------------------------------------
          Function Name: ObtenerEstacionamientos
          Author:  lgisuk
          Date Created: 06/02/2006
          Description:  Obtiene los Estacionamientos que pertencen al reclamo
          Parameters:
          Return Value: Boolean
        -----------------------------------------------------------------------------}
        Function ObtenerEstacionamientos (CodigoOrdenServicio : Integer) : Boolean;
        begin
           FListaEstacionamientos.Clear;
           with spObtenerEstacionamientosReclamo do begin
              DisableControls;
              first;
              while not eof do begin
                  FListaEstacionamientos.Add(FieldByName('NumCorrEstacionamiento').AsString);
                  next;
              end;
              EnableControls;
            end;
            Result := True;
        end;

        {-----------------------------------------------------------------------------
          Function Name: ObtenerEstacionamientosAceptadosCliente
          Author:  lgisuk
          Date Created: 06/02/2006
          Description:  Arma una lista de los Estacionamientos aceptados por el cliente
          Parameters:
          Return Value: Boolean
        -----------------------------------------------------------------------------}
        Function ObtenerEstacionamientosAceptadosCliente(CodigoOrdenServicio : Integer) : Boolean;
        var
            NumCorrEstacionamiento : String;
            Rechaza : Boolean;
        begin
           FListaEstacionamientosCliente.Clear;
           with spObtenerEstacionamientosReclamo do begin
              DisableControls;
              First;
              while not eof do begin
                  NumCorrEstacionamiento := FieldByName('NumCorrEstacionamiento').AsString;
                  Rechaza := QueryGetValue(DMConnections.BaseCAC,'select dbo.ObtenerRechazaOSEstacionamiento('+ IntToStr(CodigoOrdenServicio) + ',' + NumCorrEstacionamiento + ')'  ) = 'True';
                  if not Rechaza then ListaEstacionamientosInclude(FListaEstacionamientosCliente, NumCorrEstacionamiento);
                  Next;
              end;
              EnableControls;
            end;
            Result := True;
        end;

        {-----------------------------------------------------------------------------
          Function Name: ObtenerResolucionEmpresa
          Author:  lgisuk
          Date Created: 06/02/2006
          Description:  Arma una lista de los Estacionamientos aceptados por la empresa
          Parameters:
          Return Value: Boolean
        -----------------------------------------------------------------------------}
        Function ObtenerResolucionEmpresa(CodigoOrdenServicio : Integer) : Boolean;
        var
            NumCorrEstacionamiento : String;
            CodigoResolucion : String;
            Bloque : String;
        begin
           FListaEstacionamientosEmpresa.Clear;
           with spObtenerEstacionamientosReclamo do begin
              DisableControls;
              First;
              while not Eof do begin
                  NumCorrEstacionamiento := FieldByName('NumCorrEstacionamiento').AsString;
                  CodigoResolucion := FieldByName('CodigoResolucionReclamoEstacionamiento').AsString;
                  CodigoResolucion := IIF( CodigoResolucion = '' , '1' , CodigoResolucion);
                  Bloque := NumCorrEstacionamiento + ';' + CodigoResolucion;
                  if CodigoResolucion <> '1' then ListaEstacionamientosInclude(FListaEstacionamientosEmpresa,Bloque);
                  next;
              end;
              EnableControls;
            end;
            Result := True;
        end;

    begin

        with SpObtenerEstacionamientosReclamo do begin
            Close;
            Parameters.ParamByName('@CodigoOrdenServicio').value := CodigoOrdenServicio;
            Parameters.ParamByName('@Enumeracion').value := NULL;
            Open;
        end;

        ObtenerEstacionamientos (CodigoOrdenServicio);
        ObtenerEstacionamientosAceptadosCliente (CodigoOrdenServicio);
        ObtenerResolucionEmpresa (CodigoOrdenServicio);

        Result := True;
    end;

var
    Estado : String;
begin
    FCodigoOrdenServicio := CodigoOrdenServicio;
    spObtenerOrdenServicio.Parameters.Refresh;
    spObtenerOrdenServicio.Parameters.ParamByName('@CodigoOrdenServicio').Value := CodigoOrdenServicio;
    Result := OpenTables([spObtenerOrdenServicio]);
    if not Result then Exit;

    // Cargamos los datos
    Result := Inicializar(spObtenerOrdenServicio.FieldByName('TipoOrdenServicio').AsInteger, 0, nil);

    txtDetalle.Text := spObtenerOrdenServicio.FieldByName('ObservacionesSolicitante').AsString;
    txtDetalleSolucion.Text := spObtenerOrdenServicio.FieldByName('ObservacionesEjecutante').AsString;

    FrameCompromisoOrdenServicio1.LoadFromDataset(spObtenerOrdenServicio);
    FrameContactoReclamo1.LoadFromDataset(spObtenerOrdenServicio);
    FrameSolucionOrdenServicio1.LoadFromDataset(spObtenerOrdenServicio);
    FrameRespuestaOrdenServicio1.LoadFromDataset(spObtenerOrdenServicio);

    //Rev.1 / 29-Junio-2010 / Nelson Droguett Sierra --------------------------------------------------------------
    FrameConcesionariaReclamoOrdenServicio1.LoadFromDataset(spObtenerOrdenServicio);
    FrameSubTipoReclamoOrdenServicio1.LoadFromDataset(spObtenerOrdenServicio);
    //FinRev.1------------------------------------------------------------------------------------------------------

    //cargo la grilla con los Estacionamientos objetados
    ObtenerDatosEstacionamientos(CodigoOrdenServicio);

    //obtengo la historia para esa orden de servicio
    FrameHistoriaOrdenServicio.Inicializar(CodigoOrdenServicio);

    //obtengo la fuente del reclamo
    FrameFuenteReclamoOrdenServicio1.LoadFromDataset(spObtenerOrdenServicio);

    //Obtengo el Estado de la OS para saber si permito editar o read only
    Estado := spObtenerOrdenServicio.FieldByName('Estado').AsString;
    //si esta finalizada solo se puede leer
    if ((Estado = 'C') or (Estado = 'T')) then begin
        ReadOnly;
    end;

    // Listo
    spObtenerOrdenServicio.Close;

    //termino de inicializar
    FrameNumeroOrdenServicio1.Visible := True;
    FrameNumeroOrdenServicio1.Inicializar(IntToStr(CodigoOrdenServicio));
    //por Default no retengo la orden de servicio
    CKRetenerOrden.Checked := False;
    //la fuente del reclamo no puede ser cambiada despues de creada la OS
    FrameFuenteReclamoOrdenServicio1.Deshabilitar;
    //Estoy Editando
    FEditando := True;
end;

{-----------------------------------------------------------------------------
  Function Name: InicializarSolucionando
  Author:  DDeMarco
  Date Created:
  Description: Soluciono un Reclamo
  Parameters: CodigoOrdenServicio: Integer
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFormReclamoEstacionamiento.InicializarSolucionando(CodigoOrdenServicio: Integer): Boolean;
begin
    Result := InicializarEditando(CodigoOrdenServicio);
    if Result then begin
        PageControl.ActivePageIndex := 1;
        FrameSolucionOrdenServicio1.Estado := 'T';
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: GetCodigoOrdenServicio
  Author:    lgisuk
  Date Created: 12/04/2005
  Description: obtengo el codigoordenservicio
  Parameters: None
  Return Value: integer
-----------------------------------------------------------------------------}
function TFormReclamoEstacionamiento.GetCodigoOrdenServicio: integer;
begin
    try
        Result := StrToInt(FrameNumeroOrdenServicio1.CodigoOrdenServicio);
    except
        Result := 0;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: FormKeyPress
  Author:    DDeMarco
  Date Created:
  Description: permite cancelar
  Parameters: Sender: TObject; var Key: Char
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoEstacionamiento.FormKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #27 then btnCancelar.Click;
end;

{-----------------------------------------------------------------------------
  Function Name: ListaEstacionamientosInclude
  Author:    lgisuk
  Date Created: 14/04/2005
  Description: agrego un valor a la lista de Estacionamientos
  Parameters: valor:string
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFormReclamoEstacionamiento.ListaEstacionamientosInclude(Lista : TStringList; Valor : String) : Boolean;
begin
    Result := False;
    try
        if not ListaEstacionamientosIn(Lista, Valor) then Lista.Add(Valor);
        Result := True;
    except
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ListaEstacionamientosExclude
  Author:    lgisuk
  Date Created: 14/04/2005
  Description: quito un valor a la lista de Estacionamientos
  Parameters: valor:string
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFormReclamoEstacionamiento.ListaEstacionamientosExclude(Lista:tstringlist;valor:string):boolean;
var
    Index:integer;
begin
    Result := False;
    try
        Index := Lista.IndexOf(valor);
        if Index <> -1 then Lista.Delete(index);
        Result := True;
    except
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ListaEstacionamientosIn
  Author:    lgisuk
  Date Created: 14/04/2005
  Description: Devuelve si un valor esta en la lista Estacionamientos
  Parameters: valor:string
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFormReclamoEstacionamiento.ListaEstacionamientosIn(Lista:tstringlist;valor:string):boolean;
begin
    Result := not (Lista.IndexOf(valor) = -1);
end;

{-----------------------------------------------------------------------------
  Function Name: ListaEstacionamientosEmpresaIn
  Author:    lgisuk
  Date Created: 19/05/2005
  Description: busca un Estacionamiento, se fija si esta y trae su resolucion
  Parameters: valor:string; var Resolucion:integer
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFormReclamoEstacionamiento.ListaEstacionamientosEmpresaIn(valor:string; var Resolucion : Integer) : Boolean;
var
    I : Integer;
begin
    Result := False;
    Resolucion := 1;
    I := 0;
    while I <= FListaEstacionamientosEmpresa.Count-1 do
    begin
        if ParseParamByNumber(FListaEstacionamientosEmpresa.Strings[i],1,';') = Valor then begin
            Resolucion := StrToInt(ParseParamByNumber(FListaEstacionamientosEmpresa.Strings[i],2,';'));
            I := FListaEstacionamientosEmpresa.Count;
            Result := True;
        end;
        inc(i);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: DBLEstacionamientosDrawText
  Author:    lgisuk
  Date Created: 14/04/2005
  Description: doy formato a la grilla
  Parameters: Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;State: TOwnerDrawState; var Text: String; var TxtRect: TRect;var ItemWidth: Integer; var DefaultDraw: Boolean
  Return Value: None
-----------------------------------------------------------------------------
  Revision 1
  Author:    lgisuk
  Date Created: 04/12/2006
  Description: Ahora se permite la selecci�n r�pida de m�ltiples Estacionamientos
                para luego aceptar el reclamo.
-----------------------------------------------------------------------------}
procedure TFormReclamoEstacionamiento.DBLEstacionamientosDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;State: TOwnerDrawState; var Text: String; var TxtRect: TRect;var ItemWidth: Integer; var DefaultDraw: Boolean);

    {-----------------------------------------------------------------------------
      Function Name: DibujarBitmap
      Author:    lgisuk
      Date Created: 29/06/2005
      Description: Dibujo el bitmap seleccionado en la columna de la grilla
      Parameters: Grilla:TCustomDBlistEx;ImageList:TimageList;numero:integer
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function DibujarBitmap(Grilla: TCustomDBlistEx; ImageList: TimageList; IndiceImg: integer):boolean;
    var
        bmp: TBitMap;
    begin
        bmp := TBitMap.Create;
        try
            ImageList.GetBitmap(IndiceImg, Bmp);
            Grilla.Canvas.Draw((Rect.Left + Rect.Right - Bmp.Width) div 2, Rect.Top, Bmp);
            Result := true;
        finally
            bmp.Free;
        end;
    end;

var
    I : Integer;
    NumCorrEstacionamiento : String;
    Resolucion : Integer;
begin

    //FechaHora
    if (Column.FieldName = 'FechaHoraEntrada') or (Column.FieldName = 'FechaHoraSalida') then begin
        if Text <> '' then Text := FormatDateTime('dd/mm/yyyy hh:nn', spObtenerEstacionamientosReclamo.FieldByName(Column.FieldName).AsDateTime);
    end;

    //muestra si el cliente rechazo un Estacionamiento o no
  	if (Column.DisplayName = 'Usuario Rechaza') then begin
        DefaultDraw := False;
        //verifica si esta seleccionado o no
        I := iif(ListaEstacionamientosIn(FListaEstacionamientosCliente,spObtenerEstacionamientosReclamo.FieldByName('NumCorrEstacionamiento').Asstring), 0, 1);
        //Dibujo el icono correspondiente
        DibujarBitmap(dblEstacionamientos, LnCheck, I);
  	end;

    //Muestra la resolucion de la empresa con respecto al Estacionamiento
  	if (Column.DisplayName = 'Resoluci�n') then begin
        //Obtengo el numero de Estacionamiento
        NumCorrEstacionamiento := spObtenerEstacionamientosReclamo.fieldbyname('NumCorrEstacionamiento').asstring;
        //Busco el Estacionamiento. Si esta me devuelve su resolucion actual.
        if not ListaEstacionamientosEmpresaIn(NumCorrEstacionamiento, Resolucion) then begin
            Text := 'Pendiente';
        end else begin
            if resolucion = 1 then text := 'Pendiente';
            if resolucion = 2 then text := 'Aceptado';
            if resolucion = 3 then text := 'Denegado';
        end;
  	end;

    //muestra si el cliente rechazo un Estacionamiento o no
  	if (Column.DisplayName = 'Seleccionado') then begin  //Agregado en Revision 1
        DefaultDraw := False;
        //verifica si esta seleccionado o no
        I := iif(ListaEstacionamientosIn(FListaEstacionamientosSeleccionados,spObtenerEstacionamientosReclamo.FieldByName('NumCorrEstacionamiento').Asstring), 1, 0);
        //Dibujo el icono correspondiente
        DibujarBitmap(dblEstacionamientos, LnCheck, I);
        //Text := IIF(I = 0, 'SI', 'NO');
  	end;

end;


{-----------------------------------------------------------------------------
  Function Name: DBLEstacionamientosCheckLink
  Author:    lgisuk
  Date Created: 18/05/2005
  Description: habilito el link solo si hay imagen
  Parameters: Sender: TCustomDBListEx; Column: TDBListExColumn; var IsLink: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoEstacionamiento.DBLEstacionamientosCheckLink(Sender: TCustomDBListEx; Column: TDBListExColumn; var IsLink: Boolean);
begin
    //permito hacer click en el link o no
    if (Column.DisplayName = 'Im�gen') then begin
        Column.IsLink := (spObtenerEstacionamientosReclamo['RegistrationAccessibility'] > 0);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: DBLEstacionamientosLinkClick
  Author:    lgisuk
  Date Created: 14/04/2005
  Description: permito rechazar o aceptar un Estacionamiento
  Parameters: Sender: TCustomDBListEx; Column: TDBListExColumn
  Return Value: None
-----------------------------------------------------------------------------
  Revision 1
  Author:    lgisuk
  Date Created: 04/12/2006
  Description: Ahora se permite la selecci�n r�pida de m�ltiples Estacionamientos
                para luego aceptar el reclamo.
-----------------------------------------------------------------------------}
procedure TFormReclamoEstacionamiento.DBLEstacionamientosLinkClick(Sender: TCustomDBListEx; Column: TDBListExColumn);
Resourcestring
    MSG_TITLE   = 'Sistema de Reclamos';
    MSG_CONFIRM = 'Desea cambiar la opci�n que eligio el usuario?';
Var
  	NumCorrEstacionamiento: string;
    RealizarCambio:boolean;
    Estado: string;
begin

    //Mostrar Imagen del Estacionamiento
    if (Column.DisplayName = 'Im�gen') then begin
        //Si hay imagen
        if spObtenerEstacionamientosReclamo.FieldByName('RegistrationAccessibility').AsInteger > 0 then begin
            //La muestra
            //MostrarVentanaImagen(Self,spObtenerEstacionamientosReclamo['NumCorrCA'], spObtenerEstacionamientosReclamo['FechaHora']);
        end;
    end;

    if (Column.DisplayName = 'Usuario Rechaza') then begin
        //Selecciono/desselecciono Estacionamiento  cliente
        if Column.IsLink = false then Exit;
    end;

    //Si la OS ya esta cerrada evito que hagan click en el link
    Estado :=FrameSolucionOrdenServicio1.cbEstado.value;
    if ((Estado = 'C') or (Estado = 'T')) then Exit;

    if (Column.DisplayName = 'Usuario Rechaza') then begin
        //no realizar cambio hasta  confirmacion operador
        RealizarCambio:= False;
        // Si es un reclamo nuevo
        if feditando = false then begin
            RealizarCambio:= true;
        end;
        //si se esta modificando
        if fEditando = true then begin
           //confirmo si desea cambiar la opcion que eligio el usuario
           if MsgBox(MSG_CONFIRM, MSG_TITLE, 1) = 1 then begin
                RealizarCambio:= true;
           end;
        end;
        //realizo el cambio en la opcion del cliente
        if realizarcambio then begin
            NumCorrEstacionamiento := spObtenerEstacionamientosReclamo.FieldByName('NumCorrEstacionamiento').Asstring;
            if ListaEstacionamientosin(FListaEstacionamientosCliente,NumCorrEstacionamiento) then ListaEstacionamientosExclude(FListaEstacionamientosCliente,NumCorrEstacionamiento)
            else ListaEstacionamientosInclude(FListaEstacionamientosCliente,NumCorrEstacionamiento);
            Sender.Invalidate;
       end;
    end;

    if (Column.DisplayName = 'Seleccionado') then begin       //Agregado en Revision 1
            NumCorrEstacionamiento := spObtenerEstacionamientosReclamo.FieldByName('NumCorrEstacionamiento').Asstring;
            if ListaEstacionamientosin(FListaEstacionamientosSeleccionados,NumCorrEstacionamiento) then ListaEstacionamientosExclude(FListaEstacionamientosSeleccionados,NumCorrEstacionamiento)
            else ListaEstacionamientosInclude(FListaEstacionamientosSeleccionados,NumCorrEstacionamiento);
            Sender.Invalidate;
    end;

end;

{-----------------------------------------------------------------------------
  Function Name: DBLEstacionamientosContextPopup
  Author:    lgisuk
  Date Created: 19/05/2005
  Description: habilito el menu contextual solo si hay registros
  Parameters: Sender: TObject;MousePos: TPoint; var Handled: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoEstacionamiento.DBLEstacionamientosContextPopup(Sender: TObject;MousePos: TPoint; var Handled: Boolean);
begin
  	if not spObtenerEstacionamientosReclamo.IsEmpty then begin
        Handled := False;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: Mnu_ComunClick
  Author:    lgisuk
  Date Created: 19/05/2005
  Description:  Permite cambiar la Resolucion de la empresa
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------
  Revision 1
  Author:    lgisuk
  Date Created: 04/12/2006
  Description: Ahora se permite la selecci�n r�pida de m�ltiples Estacionamientos
                para luego aceptar el reclamo.
-----------------------------------------------------------------------------}
procedure TFormReclamoEstacionamiento.Mnu_ComunClick(Sender: TObject);
var
  	NumCorrEstacionamiento: String;
    CodigoResolucionActual : String;
    CodigoResolucionAnterior : Integer;
begin
    //Obtengo codigo de Resolucion Actual
    CodigoResolucionActual := IntToStr((Sender as TMenuItem).Tag);

    //Recorro los Estacionamientos
    with spObtenerEstacionamientosReclamo do begin
        DisableControls;
        first;
        while not eof do begin

            //si estan entre los seleccionados
            if ListaEstacionamientosIn(FListaEstacionamientosSeleccionados, fieldbyname('NumCorrEstacionamiento').asstring) then begin

                //Obtengo numero de Estacionamiento
                NumCorrEstacionamiento := spObtenerEstacionamientosReclamo.FieldByName('NumCorrEstacionamiento').Asstring;
                //Obtengo Resolucion Anterior porque la necesito para localizar el registro
                ListaEstacionamientosEmpresaIn(NumCorrEstacionamiento, CodigoResolucionAnterior);
                //Elimino Resolucion Anterior porque solo puede existir una
                ListaEstacionamientosExclude(FListaEstacionamientosEmpresa, NumCorrEstacionamiento + ';' + IntToStr(CodigoResolucionAnterior));
                //Inserto Nueva Resolucion
                ListaEstacionamientosInclude(FListaEstacionamientosEmpresa, NumCorrEstacionamiento + ';' + CodigoResolucionActual);

            end;

            Next;
        end;
        EnableControls;
    end;

    //Borro la lista de seleccionados porque ya realice la accion
    FListaEstacionamientosSeleccionados.Clear;

    //Refresco la vista
    dblEstacionamientos.Invalidate;
end;

{-----------------------------------------------------------------------------
  Function Name: ValidarEstadoEstacionamientos
  Author:    lgisuk
  Date Created: 19/05/2005
  Description: valido que todos los Estacionamientos incluidos en el reclamo
               hallan sido analizados y resueltos por la empresa
               antes de dar por finalizado el reclamo
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFormReclamoEstacionamiento.ValidarEstadoEstacionamientos : Boolean;
var CodigoResolucion : Integer;
begin
    Result := True;
    //si el sp no esta abierto salgo
    if spObtenerEstacionamientosReclamo.Active = False then Exit;
    with spObtenerEstacionamientosReclamo do begin
        DisableControls;
        First;
        while not eof do begin
            ListaEstacionamientosEmpresaIn(fieldbyname('NumCorrEstacionamiento').asstring, CodigoResolucion);
            //basta que uno este sin resolver no permito Guardar
            if CodigoResolucion = 1 then begin
                Result := False;
            end;
            Next;
        end;
        EnableControls;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: AceptarBTNClick
  Author: DDeMarco
  Date Created:
  Description: registra el reclamo
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoEstacionamiento.AceptarBTNClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: Validar
      Author:    lgisuk
      Date Created: 16/06/2005
      Description:  Valido el contenido del reclamo antes de guardarlo
      Parameters: None
      Return Value: Boolean
    -----------------------------------------------------------------------------}
    Function Validar : Boolean;
    resourcestring
        MSG_ERROR_DET       = 'Debe indicar alg�n detalle para el reclamo';
        MSG_ERROR_FUENTE    = 'Debe indicar la fuente del reclamo';
        MSG_ERROR_FECHA     = 'La Fecha de Compromiso Interna o Externa '+ CRLF +
                              'debe ser menor a la Fecha Comprometida con el Cliente';
        MSG_ERROR_EstacionamientoS = 'Todos los Estacionamientos deben estar resueltos para cerrar el reclamo';
    begin
        Result := False;

        //Validamos fuente de reclamo
        if FEditando = False then begin
            if not FrameFuenteReclamoOrdenServicio1.Validar then Exit;
        end;

        //si es un reclamo que va a ser cancelado, no valida el resto de los
        //campos para no forzar al usuario a que los complete y asi pueda
        //salir rapidamente.
        if FrameSolucionOrdenServicio1.Estado = 'C' then begin
            Result := True;
            Exit;
        end;

        //Validamos identificacion de usuario y contacto
        if not FrameContactoReclamo1.Validar then Exit;

        //Validamos compromiso con el cliente
        if FEditando = false then begin
            if not FrameCompromisoOrdenServicio1.Validar then Exit;
        end;

        //valido que todos los Estacionamientos esten resueltos
        if (FrameSolucionOrdenServicio1.Estado = 'T') then begin
            if not ValidarEstadoEstacionamientos then begin
                PageControl.ActivePageIndex := 0;
                MsgBoxBalloon(MSG_ERROR_EstacionamientoS, STR_ERROR, MB_ICONSTOP, dblEstacionamientos);
                dblEstacionamientos.SetFocus;
                Exit;
            end;
        end;

        //Validamos detalle del reclamo
        if Trim(txtDetalle.Text) = '' then begin
            PageControl.ActivePageIndex := 0;
            MsgBoxBalloon(MSG_ERROR_DET, STR_ERROR, MB_ICONSTOP, txtDetalle);
            txtDetalle.SetFocus;
            Exit;
        end;

        //Validamos progreso / solucion
        if not FrameSolucionOrdenServicio1.Validar then Exit;
        //Si es un reclamo pendiente de respuesta Interna o Externa
        if ((FrameSolucionOrdenServicio1.Estado = 'I') or (FrameSolucionOrdenServicio1.Estado = 'E')) then begin
            //Valido que la fecha comprometida interna/externa
            //sea menor a la fecha comprometida con el cliente
            if (FrameCompromisoOrdenServicio1.txtFechaCompromiso.Date < FrameSolucionOrdenServicio1.EFechaCompromiso.Date) then begin
                MsgBoxBalloon(MSG_ERROR_FECHA, STR_ERROR, MB_ICONSTOP, FrameSolucionOrdenServicio1.EFechaCompromiso);
                FrameSolucionOrdenServicio1.EFechaCompromiso.SetFocus;
                Exit;
            end;
        end;

        if not FrameConcesionariaReclamoOrdenServicio1.Validar then Exit ;      //SS-1006-NDR-20120802

        Result := True;
    end;

    {-----------------------------------------------------------------------------
      Function Name: InformarEstacionamientosReclamadosAcciones
      Author:    lgisuk
      Date Created: 05/07/2005
      Description: Informo los Estacionamientos reclamados
      Parameters: ListaEstacionamientos:tstringlist
      Return Value: None
    -----------------------------------------------------------------------------}
    Procedure InformarEstacionamientosReclamadosAcciones(ListaEstacionamientos : TStringList);
    Var
        F :  TFormEstacionamientosReclamadosAcciones;
    begin
         Application.CreateForm(TFormEstacionamientosReclamadosAcciones, F);
         if F.Inicializar(ListaEstacionamientos) then F.ShowModal;
    end;
resourcestring
    MSG_PROCESS_ERROR = 'Error al procesar los Estacionamientos reclamados';
    MSG_ERROR = 'Error';
Var
    OS : Integer;
    OK : Boolean;
    CodigoResolucion : Integer;
begin

    //Valido el reclamo antes de guardarlo
    if not Validar then exit;

    //Guardamos
    //Rev.1 / 29-Junio-2010 / Nelson Droguett Sierra----------------------------------
    DMConnections.BaseCAC.BeginTrans;
    OS := GuardarOrdenServicioBase(FCodigoOrdenServicio,
                                   FTipoOrdenServicio,
                                   FrameFuenteReclamoOrdenServicio1.FuenteReclamo,
                                   FrameContactoReclamo1,
                                   FrameCompromisoOrdenServicio1,
                                   FrameSolucionOrdenServicio1,
                                   txtDetalle.Text,
                                   txtDetalleSolucion.Text,
                                   FrameRespuestaOrdenServicio1.ObservacionesRespuesta,
                                   FrameRespuestaOrdenServicio1.ObservacionesComentariosCliente,
                                   FrameRespuestaOrdenServicio1.Notificado,
                                   FrameRespuestaOrdenServicio1.Conforme,
                                   FrameConcesionariaReclamoOrdenServicio1,
                                   FrameSubTipoReclamoOrdenServicio1);
    //Rev.1---------------------------------------------------------------------------

    if OS <= 0 then begin
        //Cancelo la transaccion y salgo
        DMConnections.BaseCAC.RollbackTrans;
        Exit;
    end;

    //Especifico de Estacionamientos
    try
        OK := True;
        with spObtenerEstacionamientosReclamo do begin
            DisableControls;
            first;
            while not eof do begin
                ListaEstacionamientosEmpresaIn(fieldbyname('NumCorrEstacionamiento').AsString, CodigoResolucion);
                //Guardo cada Estacionamiento en la bd
                if GuardarOrdenServicioEstacionamiento(OS, FieldByName('NumCorrEstacionamiento').AsString, not (ListaEstacionamientosIn(FListaEstacionamientosCliente,FieldByName('NumCorrEstacionamiento').AsString)), CodigoResolucion ) = False then Ok := False;
                next;
            end;
            EnableControls;
        end;
        if not OK then begin
            //Cancelo la transaccion y salgo
            DMConnections.BaseCAC.RollbackTrans;
            Exit;
        end;
    except
        on E : Exception do begin
            //Cancelo la transaccion, Informo la situacion al operador y salgo
            DMConnections.BaseCAC.RollbackTrans;
            MsgBoxErr(MSG_PROCESS_ERROR, E.message, MSG_ERROR, MB_ICONSTOP);
            Exit;
        end;
    end;

    //En el caso que se acepte el reclamo del cliente
    //se pone el Estacionamiento en estado no facturable
    if FrameSolucionOrdenServicio1.Estado = 'T' then begin
        //Actualizo el estado de los Estacionamientos reclamados
        if not ActualizarEstacionamientosReclamosAceptados(OS) then begin
            //Cancelo la transaccion y salgo
            DMConnections.BaseCAC.RollbackTrans;
            Exit;
        end;
    end;

    //Listo
    DMConnections.BaseCAC.CommitTrans;

    //Muestro un cartel con las acciones realizadas sobre
    //los Estacionamientos reclamados
    if FrameSolucionOrdenServicio1.Estado = 'T' then begin
        InformarEstacionamientosReclamadosAcciones(FListaEstacionamientos);
    end;

    //Si es un reclamo nuevo
    if FEditando = False then begin
        //Informo al operador el numero de Reclamo que se genero
        InformarNumeroReclamo(inttostr(OS));
        DesbloquearOrdenServicio(OS);
    end;

    Close;

end;

{-----------------------------------------------------------------------------
  Function Name: btnCancelarClick
  Author:    lgisuk
  Date Created: 12/04/2005
  Description: Cierro el formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoEstacionamiento.btnCancelarClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormDestroy
  Author:    lgisuk
  Date Created: 12/04/2005
  Description:  remuevo al form de la lista de creados
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoEstacionamiento.FormDestroy(Sender: TObject);
begin
    RemoveOSform(self);
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 05/04/2005
  Description: desbloqueo la orden de servicio antes de salir
  Parameters: Sender: TObject;var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoEstacionamiento.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    //Si se esta modificando un reclamo
    if feditando = true then begin
        //Verifico si desea desbloquear el reclamo
        if CKRetenerOrden.Checked = false then begin

            //Desbloqueo la orden de servicio
            DesbloquearOrdenServicio(FCodigoOrdenServicio);
        end;
    end;
    //Borro la lista
    FListaEstacionamientos.Clear;                                               //SS_1006P_NDR_20140715
    FListaEstacionamientosCliente.Clear;                                        //SS_1006P_NDR_20140715
    FListaEstacionamientosSeleccionados.Clear;                                  //SS_1006P_NDR_20140715
    FListaEstacionamientosEmpresa.Clear;                                        //SS_1006P_NDR_20140715
    //Lo libero de memoria
    Action := caFree;
end;

initialization
    FListaEstacionamientosCliente := TStringList.Create;                        //SS_1006P_NDR_20140715
    FListaEstacionamientosEmpresa := TStringList.Create;
    FListaEstacionamientosSeleccionados := TStringList.Create;
finalization
    FreeAndNil(FListaEstacionamientosCliente);
    FreeAndNil(FListaEstacionamientosEmpresa);
    FreeAndNil(FListaEstacionamientosSeleccionados);
end.
