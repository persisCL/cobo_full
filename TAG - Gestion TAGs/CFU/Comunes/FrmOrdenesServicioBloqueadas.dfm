object FOrdenesServicioBloquedas: TFOrdenesServicioBloquedas
  Left = 26
  Top = 99
  Width = 870
  Height = 458
  Caption = 'Tareas Bloqueadas'
  Color = clBtnFace
  Constraints.MinHeight = 458
  Constraints.MinWidth = 870
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnClose = FormClose
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object GBCriterios: TGroupBox
    Left = 0
    Top = 0
    Width = 862
    Height = 61
    Align = alTop
    TabOrder = 0
    object PCriterios: TPanel
      Left = 769
      Top = 15
      Width = 91
      Height = 44
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object BuscarBTN: TButton
        Left = 8
        Top = 5
        Width = 75
        Height = 25
        Caption = 'Buscar'
        TabOrder = 0
        OnClick = BuscarBTNClick
      end
    end
  end
  object DBLReclamos: TDBListEx
    Left = 0
    Top = 61
    Width = 862
    Height = 322
    Align = alClient
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 20
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
      end
      item
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 50
        Header.Caption = 'Orden'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taRightJustify
        IsLink = False
        OnHeaderClick = DBLReclamosColumns0HeaderClick
        FieldName = 'CodigoOrdenServicio'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 160
        Header.Caption = 'Reclamo'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        OnHeaderClick = DBLReclamosColumns0HeaderClick
        FieldName = 'Descripcion'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 100
        Header.Caption = 'F. Creaci'#243'n'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = DBLReclamosColumns0HeaderClick
        FieldName = 'FechaHoraCreacion'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 95
        Header.Caption = 'F. Compromiso'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = DBLReclamosColumns0HeaderClick
        FieldName = 'FechaCompromiso'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 100
        Header.Caption = 'F. Cierre'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = DBLReclamosColumns0HeaderClick
        FieldName = 'FechaHoraFin'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 60
        Header.Caption = 'Prioridad'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = DBLReclamosColumns0HeaderClick
        FieldName = 'Prioridad'
      end
      item
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 60
        Header.Caption = 'Estado'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        OnHeaderClick = DBLReclamosColumns0HeaderClick
        FieldName = 'DescEstado'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 150
        Header.Caption = 'Usuario'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Usuario'
      end>
    DataSource = DataSource
    DragReorder = True
    ParentColor = False
    TabOrder = 1
    TabStop = True
    OnDblClick = DBLReclamosDblClick
    OnDrawText = DBLReclamosDrawText
    OnMouseMove = DBLReclamosMouseMove
  end
  object PBotones: TPanel
    Left = 0
    Top = 383
    Width = 862
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object LCantidadTotal: TLabel
      Left = 645
      Top = 16
      Width = 3
      Height = 13
      Alignment = taRightJustify
    end
    object PBotonesDerecha: TPanel
      Left = 677
      Top = 0
      Width = 185
      Height = 41
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object DesbloquearBTN: TButton
        Left = 22
        Top = 8
        Width = 75
        Height = 25
        Caption = 'Desbloquear'
        TabOrder = 0
        OnClick = DesbloquearBTNClick
      end
      object SalirBTN: TButton
        Left = 102
        Top = 8
        Width = 75
        Height = 25
        Caption = 'Salir'
        TabOrder = 1
        OnClick = SalirBTNClick
      end
    end
  end
  object SpObtenerOrdenesServicioBloqueadas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerOrdenesServicioBloqueadas;1'
    Parameters = <>
    Left = 736
    Top = 165
    object SpObtenerOrdenesServicioBloqueadasCodigoOrdenServicio: TAutoIncField
      FieldName = 'CodigoOrdenServicio'
      ReadOnly = True
    end
    object SpObtenerOrdenesServicioBloqueadasRut: TStringField
      FieldName = 'Rut'
      FixedChar = True
      Size = 11
    end
    object SpObtenerOrdenesServicioBloqueadasDescripcion: TStringField
      FieldName = 'Descripcion'
      Size = 255
    end
    object SpObtenerOrdenesServicioBloqueadasFechaHoraCreacion: TDateTimeField
      FieldName = 'FechaHoraCreacion'
    end
    object SpObtenerOrdenesServicioBloqueadasFechaCompromiso: TDateTimeField
      FieldName = 'FechaCompromiso'
    end
    object SpObtenerOrdenesServicioBloqueadasPrioridad: TWordField
      FieldName = 'Prioridad'
    end
    object SpObtenerOrdenesServicioBloqueadasNumeroConvenio: TStringField
      FieldName = 'NumeroConvenio'
      ReadOnly = True
      FixedChar = True
      Size = 30
    end
    object SpObtenerOrdenesServicioBloqueadasFechaHoraFin: TDateTimeField
      FieldName = 'FechaHoraFin'
    end
    object SpObtenerOrdenesServicioBloqueadasDescEstado: TStringField
      FieldName = 'DescEstado'
      ReadOnly = True
      Size = 9
    end
    object SpObtenerOrdenesServicioBloqueadasPatente: TStringField
      FieldName = 'Patente'
      FixedChar = True
      Size = 10
    end
    object SpObtenerOrdenesServicioBloqueadasUsuario: TStringField
      FieldName = 'Usuario'
      FixedChar = True
    end
    object SpObtenerOrdenesServicioBloqueadasEstado: TStringField
      FieldName = 'Estado'
      FixedChar = True
      Size = 1
    end
  end
  object DataSource: TDataSource
    DataSet = SpObtenerOrdenesServicioBloqueadas
    Left = 768
    Top = 165
  end
end
