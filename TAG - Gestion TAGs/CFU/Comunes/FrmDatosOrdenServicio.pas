unit FrmDatosOrdenServicio;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, Util, UtilDB, DB, ADODB,
  peaProcs, PeaTypes, UtilProc, FrmTerminarTarea, DPSControls, Grids,
  DBGrids;

type
  TFormDatosOrdenServicio = class(TForm)
	re_Tareas: TRichEdit;
	Label1: TLabel;
	Label2: TLabel;
	re_General: TRichEdit;
	OrdenServicio: TADOQuery;
	qry_Facturacion: TADOQuery;
	OPButton1: TDPSButton;
	btn_Ejecutar: TDPSButton;
	qry_DatosMoroso: TADOQuery;
	qry_reparacion: TADOQuery;
	qry_Adhesion: TADOQuery;
    ObtenerDomiciliosPersonaParticular: TADOStoredProc;
    ObtenerDomiciliosPersonaComercial: TADOStoredProc;
	ObtenerTareasTerminadas: TADOStoredProc;
	procedure FormKeyPress(Sender: TObject; var Key: Char);
	procedure btn_EjecutarClick(Sender: TObject);
  private
	{ Private declarations }
	FTipoOrdenServicio: Integer;
	FCodigoOrdenServicio, FTarea: Integer;
	procedure AddText(re: TRichEdit; const Text: AnsiString;
	  FontName: TFontName = 'Arial'; FontSize: Integer = 10; FontStyle: TFontStyles = [];
	  FontColor: TColor = clBlack);
	procedure CargarAdhesion(caption_boton: AnsiString);
	procedure CargarCierreCuenta(caption_boton: AnsiString);
	procedure CargarFacturacion1(caption_boton: AnsiString);
	procedure CargarFacturacion2(caption_boton: AnsiString);
    procedure CargarTAGMAlFuncionamiento;
	procedure CargarTag(caption_boton: AnsiString);
    procedure TratamientoGeneral(CODIGO_OS: integer; caption_boton: AnsiString);
	procedure TratamientoMoroso(CODIGO_OS: integer; caption_boton: AnsiString; OS: integer);
	procedure CargarOSReparacion(CODIGO_OS: integer; OS: integer; caption_boton: AnsiString = '');
	procedure EjecutarAdhesion;
	procedure EjecutarCierreCuenta;
	procedure EjecutarCargarTag(tipoOS: integer);
	procedure EjecutarCargarFacturacion1;
	procedure EjecutarDevolverTarea(OS, Tarea: integer);
	function DameSelectOSReparacion(tabla, OS: AnsiString): AnsiString;
  public
	{ Public declarations }
	function Inicializa(CodigoOrdenServicio: Integer; Tarea: integer = -1): Boolean;
  end;

var
  FormDatosOrdenServicio: TFormDatosOrdenServicio;

implementation

uses DMConnection, FrmMantenimientoCuenta, Navigator, FrmCerraCuenta,
	 frmHabilitarDeshabilitar;

{$R *.dfm}

{ TFormDatosOrdenServicio }

resourcestring
	MSG_CLIENTE  = 'Cliente:';
	MSG_CUENTA   = 'Cuenta:';
	MSG_CONTACTO = 'Contacto:';

function TFormDatosOrdenServicio.Inicializa(CodigoOrdenServicio: Integer; Tarea: integer = -1): Boolean;

resourcestring
	MSG_COMENTARIOS   = 'Comentarios';
	MSG_TERMINADA_EL  = 'Terminada el:';
	MSG_RESPONSABLE   = 'Responsable:';
	MSG_SOLICITADO_EL = 'Solicitado el:';
	CAPTION_BTN       = '';
	CAPTION_BTN_REALIZAR_ADHESION = '&Realizar adhesi�n';
	CAPTION_BTN_CIERRE_CUENTA = '&Cerrar cuenta';
	CAPTION_BTN_INHABILITAR_TAG = '&Inhabilitar TAG';
	CAPTION_BTN_REHABILITAR_TAG = '&Rehabilitar TAG';
	CAPTION_BTN_FACTURACION_INCORRECTA = '&Facturaci�n incorrecta';
	CAPTION_BTN_FACTURA_NO_RECIBIDA = '&Facturaci�n no recibida';
	CAPTION_BTN_RECLAMO_MALAS_INSTALACIONES = '&OS Malas Instalaciones';
	CAPTION_BTN_ACCIDENTE = '&OS Accidente';
	CAPTION_BTN_FALTA_SEGURIDAD = '&OS Falta de seguridad';
	CAPTION_BTN_RECLAMO_GENERAL = '&OS Reclamo general';
	CAPTION_BTN_RECOMENDACION = '&OS Recomendaci�n';
	CAPTION_BTN_AGRADECIMIENTO = '&OS Agradecimiento';

Var
	Cmt: AnsiString;
begin
	OrdenServicio.Parameters.ParamByName('CodigoOrdenServicio').Value := CodigoOrdenServicio;
	ObtenerTareasTerminadas.Parameters.ParamByName('@CodigoOrdenServicio').Value := CodigoOrdenServicio;
	qry_Adhesion.Parameters.ParamByName('CodigoOrdenServicio').Value := CodigoOrdenServicio;
	qry_Facturacion.Parameters.ParamByName('CodigoOrdenServicio').Value := CodigoOrdenServicio;

	Result := OpenTables([OrdenServicio, ObtenerTareasTerminadas, qry_Adhesion, qry_Facturacion]);
	if not Result then Exit;

	With ObtenerDomiciliosPersonaParticular, Parameters Do Begin
	  Close;
	  ParamByName ('@CodigoPersona').Value := OrdenServicio.FieldByName ('CodigoPersona').AsInteger;
	  ParamByName ('@CodigoTipoOrigenDato').Value := TIPO_ORIGEN_DATO_PARTICULAR
	End;

	With ObtenerDomiciliosPersonaComercial, Parameters Do Begin
	  Close;
	  ParamByName ('@CodigoPersona').Value := OrdenServicio.FieldByName ('CodigoPersona').AsInteger;
	  ParamByName ('@CodigoTipoOrigenDato').Value := TIPO_ORIGEN_DATO_COMERCIAL
	End;

	Result := OpenTables([ObtenerDomiciliosPersonaParticular, ObtenerDomiciliosPersonaComercial]);
	if not Result then Exit;

	// Cargamos la informaci�n b�sica de la Orden de Servicio
	With OrdenServicio do begin
		FCodigoOrdenServicio := CodigoOrdenServicio;

		//El codigo de Tarea es necesario para crear la cuenta, ya que se necesita
		//al momento de TERMINAR la tarea.
		FTarea := Tarea;
		FTipoOrdenServicio := FieldByName('CodigoWorkflow').AsInteger;

		// Datos b�sicos
		AddText(re_General, '', 'Arial', 10, [fsUnderline], clGreen);
		AddText(re_General, Trim(FieldByName('Descripcion').AsString),
		  'Arial', 10, [fsUnderline], clGreen);
		AddText(re_General, CRLF + CRLF + MSG_SOLICITADO_EL + #9 +
		  FormatShortDate(FieldByName('FechaHoraCreacion').AsDateTime) + ' ' +
		  FormatTime(FieldByName('FechaHoraCreacion').AsDateTime));
		AddText(re_General, CRLF + MSG_CONTACTO + #9 + Trim(FieldByName('NombreContacto').AsString));

		// Datos particulares
		case FieldByName('CodigoWorkflow').AsInteger of
			OS_ADHESION: CargarAdhesion(CAPTION_BTN_REALIZAR_ADHESION);
			OS_CIERRE_CUENTA: CargarCierreCuenta(CAPTION_BTN_CIERRE_CUENTA);
			OS_MODIFICACION_DATOS_CUENTA_CLIENTE:;
			OS_CAMBIO_MODALIDAD_PAGO:;
			OS_INHABILITACION_TAG: CargarTag(CAPTION_BTN_INHABILITAR_TAG);
			OS_REHABILITACION_TAG: CargarTag(CAPTION_BTN_REHABILITAR_TAG);
			OS_FACTURACION_INCORRECTA: CargarFacturacion1(CAPTION_BTN_FACTURACION_INCORRECTA);
			//OS_RECLAMO_FACTURACION_ADEUDADA: CargarFacturacion1;
			OS_FACTURA_NO_RECIBIDA: CargarFacturacion2(CAPTION_BTN_FACTURA_NO_RECIBIDA);
			OS_RECLAMO_MALAS_INSTALACIONES: TratamientoGeneral(OS_RECLAMO_MALAS_INSTALACIONES,CAPTION_BTN_RECLAMO_MALAS_INSTALACIONES);
			OS_ACCIDENTE: TratamientoGeneral(OS_ACCIDENTE,CAPTION_BTN_ACCIDENTE);
			OS_FALTA_SEGURIDAD: TratamientoGeneral(OS_FALTA_SEGURIDAD,CAPTION_BTN_FALTA_SEGURIDAD);
			OS_RECLAMO_GENERAL: TratamientoGeneral(OS_RECLAMO_GENERAL,CAPTION_BTN_RECLAMO_GENERAL);
			OS_RECOMENDACION: TratamientoGeneral(OS_RECOMENDACION,CAPTION_BTN_RECOMENDACION);
			OS_AGRADECIMIENTO: TratamientoGeneral(OS_AGRADECIMIENTO,CAPTION_BTN_AGRADECIMIENTO);
			OS_LLAMAR_POR_TELEFONO_CLIENTE: TratamientoMoroso(OS_LLAMAR_POR_TELEFONO_CLIENTE,CAPTION_BTN, CodigoOrdenServicio);
			OS_ENVIAR_MAIL_CLIENTE: TratamientoMoroso(OS_ENVIAR_MAIL_CLIENTE,CAPTION_BTN, CodigoOrdenServicio);
			OS_ENVIAR_CARTA_CLIENTE: TratamientoMoroso(OS_ENVIAR_CARTA_CLIENTE,CAPTION_BTN, CodigoOrdenServicio);
			OS_ENVIAR_FAX_CLIENTE: TratamientoMoroso(OS_ENVIAR_FAX_CLIENTE,CAPTION_BTN, CodigoOrdenServicio);
			OS_ENVIAR_VOICE_MAIL_CLIENTE: TratamientoMoroso(OS_ENVIAR_VOICE_MAIL_CLIENTE,CAPTION_BTN, CodigoOrdenServicio);
			OS_MAL_FUNCIONAMIENTO_TAG: CargarTAGMalFuncionamiento;
			//OS_GENERAR_ORDEN_DE_TRABAJO:
			OS_REPARAR_POSTE_SOS: CargarOSReparacion(OS_REPARAR_POSTE_SOS, CodigoOrdenServicio);
			OS_REPARAR_PORTICO: CargarOSReparacion(OS_REPARAR_PORTICO, CodigoOrdenServicio);
			OS_REPARAR_PAVIMENTO: CargarOSReparacion(OS_REPARAR_PAVIMENTO, CodigoOrdenServicio);

			else TratamientoGeneral(FieldByName('CodigoWorkflow').AsInteger,'');
		end;

		if btn_Ejecutar.caption = '' then btn_Ejecutar.visible := false;

		// Observaciones
		Cmt := FieldByName('Observaciones').AsString;
		if Trim(Cmt) <> '' then begin
			AddText(re_General, CRLF + CRLF + MSG_COMENTARIOS, 'Arial', 10, [fsBold], clBlack);
			AddText(re_General, CRLF + Cmt);
		end;
		AddText(re_General, CRLF + CRLF);
	end;
	re_General.SelStart := 0;

	// Cargamos los comentarios de las tareas en el RichEdit de abajo
	AddText(re_Tareas, '', 'Arial', 10, [fsUnderline], clGreen);
	With ObtenerTareasTerminadas do while not Eof do begin
		AddText(re_Tareas, Trim(FieldByName('DescripcionTarea').AsString),
		  'Arial', 10, [fsUnderline], clGreen);
		AddText(re_Tareas, CRLF + CRLF + MSG_TERMINADA_EL + #9 +
		  FormatShortDate(FieldByName('FechaHoraFin').AsDateTime) + ' ' +
		  FormatTime(FieldByName('FechaHoraFin').AsDateTime), 'Arial', 10, [], clBlack);
		AddText(re_Tareas, CRLF + MSG_RESPONSABLE + #9 + Trim(FieldByName('CodigoUsuario').AsString),
		  'Arial', 10, [], clBlack);
		Cmt := FieldByName('Observaciones').AsString;
		if Trim(Cmt) <> '' then begin
			AddText(re_Tareas, CRLF + CRLF + MSG_COMENTARIOS, 'Arial', 10, [fsBold], clBlack);
			AddText(re_Tareas, CRLF + Cmt);
		end;
		AddText(re_Tareas, CRLF + CRLF);
		ObtenerTareasTerminadas.Next;
	end;
	OrdenServicio.Close;
	ObtenerTareasTerminadas.Close;
	re_Tareas.SelStart := 0;
end;

procedure TFormDatosOrdenServicio.AddText(re: TRichEdit; const Text: AnsiString;
  FontName: TFontName = 'Arial'; FontSize: Integer = 10; FontStyle: TFontStyles = [];
  FontColor: TColor = clBlack);
begin
	re.SelAttributes.Name := FontName;
	re.SelAttributes.Size := FontSize;
	re.SelAttributes.Style := FontStyle;
	re.SelAttributes.Color := FontColor;
	re.SelText := Text;
end;

procedure TFormDatosOrdenServicio.CargarCierreCuenta(caption_boton: AnsiString);
resourcestring
	MSG_DATOS_CUENTA_CERRAR = 'Datos de la Cuenta a cerrar';

begin
	AddText(re_General, CRLF + CRLF + MSG_DATOS_CUENTA_CERRAR,
	  'Arial', 10, [fsBold], clBlack);
	AddText(re_General, CRLF + CRLF + MSG_CLIENTE + #9#9 +
	  OrdenServicio.FieldByName('CodigoPersona').AsString + ' ' +
		  ArmaNombreCompleto(
			OrdenServicio.FieldByName('Apellido').AsString,
			OrdenServicio.FieldByName('ApellidoMaterno').AsString,
			OrdenServicio.FieldByName('Nombre').AsString));
	AddText(re_General, CRLF + MSG_CUENTA + #9#9 +
	  qry_Facturacion.FieldByName('CodigoCuenta').AsString);
	btn_Ejecutar.Visible := True;
	btn_Ejecutar.Caption := caption_boton;
end;

procedure TFormDatosOrdenServicio.CargarFacturacion1(caption_boton: AnsiString);
resourcestring
	MSG_DATOS_ERROR_FACTURACION = 'Datos relativos al Error de Facturaci�n';
	MSG_FACTURA = 'Factura:';
begin
	AddText(re_General, CRLF + CRLF + MSG_DATOS_ERROR_FACTURACION,
	  'Arial', 10, [fsBold], clBlack);
	AddText(re_General, CRLF + CRLF + MSG_CLIENTE + #9#9 +
	  OrdenServicio.FieldByName('CodigoPersona').AsString + ' ' +
		  ArmaNombreCompleto(
			OrdenServicio.FieldByName('Apellido').AsString,
			OrdenServicio.FieldByName('ApellidoMaterno').AsString,
			OrdenServicio.FieldByName('Nombre').AsString));
	AddText(re_General, CRLF + MSG_FACTURA + #9 +
	  qry_Facturacion.FieldByName('NumeroFactura').AsString);
	btn_Ejecutar.Visible := True;
	btn_Ejecutar.Caption := caption_boton;
end;

procedure TFormDatosOrdenServicio.CargarFacturacion2(caption_boton: AnsiString);
resourcestring
	MSG_DATOS_FACT_FALT = 'Datos relativos a la factura faltante';
	MSG_NUNCA = 'Nunca se recibieron facturas';
	MSG_ULTIMA_FACTURA = '�ltima Factura:';
	MSG_DE = 'de';
Var
	cStr: AnsiString;
	Mes, Anio: Integer;
begin
	AddText(re_General, CRLF + CRLF + MSG_DATOS_FACT_FALT, 'Arial', 10, [fsBold]);
	AddText(re_General, CRLF + CRLF + MSG_CLIENTE + #9#9 +
		OrdenServicio.FieldByName('CodigoPersona').AsString + ' ' +
		ArmaNombreCompleto(
			OrdenServicio.FieldByName('Apellido').AsString,
			OrdenServicio.FieldByName('ApellidoMaterno').AsString,
			OrdenServicio.FieldByName('Nombre').AsString));

	if qry_Facturacion.FieldByName('FechaFaltante').IsNull then begin
		cStr := MSG_NUNCA;
	end else begin
		Mes := Month(qry_Facturacion.FieldByName('FechaFaltante').AsDateTime);
		Anio := Year(qry_Facturacion.FieldByName('FechaFaltante').AsDateTime);
		cStr := LongMonthNames[Mes] + ' ' + MSG_DE + ' ' + IntToStr(Anio);
	end;

	AddText(re_General, CRLF + MSG_ULTIMA_FACTURA + #9 + cStr);
	btn_Ejecutar.Visible := True;
	btn_Ejecutar.Caption := caption_boton;
end;


procedure TFormDatosOrdenServicio.CargarAdhesion(caption_boton: AnsiString);
resourcestring
	MSG_DATOS_ADHESION = 'Datos de la nueva Adhesi�n';
    MSG_APELLIDO    = 'Apellido:';
	MSG_APELLIDO_MATERNO    = 'Apellido mat.:';
	MSG_NOMBRE      = 'Nombre:';
    MSG_DOCUMENTO   = 'Documento:';
	MSG_SEXO        = 'Sexo:';
	MSG_NACIMIENTO  = 'Nacimiento:';
	MSG_DOMICILIO_PERSONAL   = 'Domicilio Particular:';
	MSG_DOMICILIO_DOCUMENTOS = 'Domicilio Comercial:';
    MSG_DOMICILIO   = 'Domicilio:';
	MSG_PAIS        = 'Pais:';
    MSG_REGION      = 'Regi�n:';
    MSG_CIUDAD      = 'Ciudad:';
	MSG_COMUNA      = 'Comuna:';
	MSG_CODIGO_POSTAL = 'C�digo Postal:';
	MSG_TELEFONO    = 'Tel�fono:';
	MSG_MAIL        = 'e-mail:';
    MSG_VEHICULO    = 'Veh�culo:';
	MSG_PATENTE     = 'Patente:';
    MSG_CATEGORIA   = 'Categor�a:';
	MSG_IDENTIFICACION = 'Identificaci�n:';
	MSG_FORMA_PAGO  = 'Forma de Pago:';
	MSG_PREPAGO     = 'Prepago';
	MSG_POSPAGO_MANUAL = 'Pospago con pago Manual';
	MSG_TARJETA_CREDITO = 'Tarjeta de Cr�dito';
	MSG_ENTIDAD_CREDITICIA = 'Tarjeta:';
	MSG_CUENTA_BANCARIA = 'Cuenta Bancaria';
	MSG_ENTIDAD_BANCARIA = 'Banco:';
	MSG_OTRAS_OPCIONES = 'Otras opciones de adhesi�n';
	MSG_DETALLE_PASADAS = 'El cliente desea recibir el detalle de pasadas';

begin
	//fsUnderline
	AddText(re_General, CRLF + CRLF + MSG_DATOS_ADHESION,
	  'Arial', 10, [fsBold]);
	//
	AddText(re_General, CRLF + CRLF + MSG_APELLIDO + #9 +
	  Trim(OrdenServicio.FieldByName('Apellido').AsString));
	if trim(OrdenServicio.FieldByName('ApellidoMaterno').AsString) <> '' then begin
		AddText(re_General, CRLF + MSG_APELLIDO_MATERNO + #9 +
		  Trim(OrdenServicio.FieldByName('ApellidoMaterno').AsString))
	end;
	AddText(re_General, CRLF + MSG_NOMBRE + #9 +
	  Trim(OrdenServicio.FieldByName('Nombre').AsString));
	AddText(re_General, CRLF + MSG_DOCUMENTO + #9 +
	  Trim(OrdenServicio.FieldByName('CodigoDocumento').AsString) + ' ' +
	  Trim(OrdenServicio.FieldByName('NumeroDocumento').AsString));
	AddText(re_General, CRLF + MSG_SEXO + #9#9 +
	  Trim(OrdenServicio.FieldByName('Sexo').AsString));
	AddText(re_General, CRLF + MSG_NACIMIENTO + #9 +
	  iif(OrdenServicio.FieldByName('FechaNacimiento').IsNull, '',
	  Trim(OrdenServicio.FieldByName('FechaNacimiento').AsString)));
	//MOSTRAR EL RESTO DE LOS DATOS.

	AddText(re_General, CRLF + MSG_PAIS + #9#9 +
	  Trim(ObtenerDomiciliosPersonaParticular.FieldByName('DescriPais').AsString));

	// Domicilio Personal
	AddText(re_General, CRLF + CRLF + MSG_DOMICILIO_PERSONAL,
			'Arial', 10, [fsUnderline]);

	AddText(re_General, CRLF + CRLF + MSG_DOMICILIO + #9 +
            ObtenerDomiciliosPersonaParticular.FieldByName('DireccionCompleta').AsString);
	if not (ObtenerDomiciliosPersonaParticular.FieldByName('CodigoPostal').IsNull) then begin
		AddText(re_General, CRLF + MSG_CODIGO_POSTAL + #9 +
			Trim(ObtenerDomiciliosPersonaParticular.FieldByName('CodigoPostal').AsString))
	end;
	AddText(re_General, CRLF + MSG_REGION + #9#9 +
	  Trim(ObtenerDomiciliosPersonaParticular.FieldByName('DescriRegion').AsString));
	AddText(re_General, CRLF + MSG_CIUDAD + #9#9 +
	  Trim(ObtenerDomiciliosPersonaParticular.FieldByName('DescriCiudad').AsString));
	AddText(re_General,
	CRLF + MSG_COMUNA + #9 +
	  Trim(ObtenerDomiciliosPersonaParticular.FieldByName('DescriComuna').AsString));

	if not OrdenServicio.FieldByName('TelefonoPersonal').IsNull  then begin
	  AddText(re_General, CRLF + CRLF + MSG_TELEFONO + #9 +
		  OrdenServicio.FieldByName('TelefonoPersonal').AsString);
	end;

	// Domicilio Documentos
	if  (trim(ObtenerDomiciliosPersonaComercial.FieldByName('DireccionCompleta').AsString) <> '') then begin
		AddText(re_General, CRLF + CRLF + MSG_DOMICILIO_DOCUMENTOS,
				'Arial', 10, [fsUnderline]);
		AddText(re_General, CRLF + CRLF  + MSG_DOMICILIO + #9 +
			ObtenerDomiciliosPersonaComercial.FieldByName('DireccionCompleta').AsString);
		if not (ObtenerDomiciliosPersonaComercial.FieldByName('CodigoPostal').IsNull) then begin
			AddText(re_General, CRLF + MSG_CODIGO_POSTAL + #9 +
				Trim(ObtenerDomiciliosPersonaComercial.FieldByName('CodigoPostal').AsString))
		end;            
		AddText(re_General, CRLF + MSG_REGION + #9#9 +
		  Trim(ObtenerDomiciliosPersonaComercial.FieldByName('DescriRegion').AsString));
    	AddText(re_General, CRLF + MSG_CIUDAD + #9#9 +
	      Trim(ObtenerDomiciliosPersonaParticular.FieldByName('DescriCiudad').AsString));
		AddText(re_General, CRLF + MSG_COMUNA + #9 +
		  Trim(ObtenerDomiciliosPersonaComercial.FieldByName('DescriComuna').AsString));
	end;

	AddText(re_General, CRLF + MSG_MAIL + #9#9 +
		OrdenServicio.FieldByName('Email').AsString);

	AddText(re_General, CRLF + MSG_CONTACTO + #9 +
	  Trim(OrdenServicio.FieldByName('ContactoComercial').AsString));

	AddText(re_General, CRLF + CRLF + MSG_VEHICULO + #9 +
	  Trim(qry_Adhesion.FieldByName('DescriMarca').AsString) + ' ' +
	  Trim(qry_Adhesion.FieldByName('DescriModelo').AsString) + ' ' +
	  Trim(qry_Adhesion.FieldByName('DescriColor').AsString));
	AddText(re_General, CRLF + MSG_PATENTE + #9 +
	  Trim(qry_Adhesion.FieldByName('Patente').AsString));
	AddText(re_General, CRLF + MSG_CATEGORIA + #9 +
	  Trim(qry_Adhesion.FieldByName('DescriCategoria').AsString));
	//
	AddText(re_General, CRLF + CRLF + MSG_IDENTIFICACION + #9 +
	  iif(qry_Adhesion.FieldByName('TipoAdhesion').AsInteger = TA_CON_TAG, 'Con TAG', 'Sin TAG'));
	//
	if qry_Adhesion.FieldByName('TipoPago').AsString = TP_PREPAGO then begin
		AddText(re_General, CRLF + CRLF + MSG_FORMA_PAGO + #9 + MSG_PREPAGO);
	end else if qry_Adhesion.FieldByName('TipoPago').AsString = TP_POSPAGO_MANUAL then begin
		AddText(re_General, CRLF + CRLF + MSG_FORMA_PAGO + #9 + MSG_POSPAGO_MANUAL);
	end else begin
		if qry_Adhesion.FieldByName('TipoDebito').AsString = DA_TARJETA_CREDITO then begin
			AddText(re_General, CRLF + CRLF + MSG_FORMA_PAGO + #9 + MSG_TARJETA_CREDITO);
			AddText(re_General, CRLF + MSG_ENTIDAD_CREDITICIA + #9#9 +
			  Trim(QueryGetValue(DMConnections.BaseCAC,
			  'SELECT Descripcion FROM vw_TarjetasCredito WHERE CodigoTipoTarjetaCredito = ' +
			  IntToStr(qry_Adhesion.FieldByName('CodigoEntidad').AsInteger))) + ' ' +
			  Trim(qry_Adhesion.FieldByName('CuentaDebito').AsString));
		end else begin
			AddText(re_General, CRLF + CRLF + MSG_FORMA_PAGO + #9 + MSG_CUENTA_BANCARIA);
			AddText(re_General, CRLF + MSG_ENTIDAD_BANCARIA + #9#9 +
			  Trim(QueryGetValue(DMConnections.BaseCAC,
			  'SELECT Descripcion FROM vw_TarjetasDebito WHERE CodigoTipoTarjetaDebito = ' +
			  IntToStr(qry_Adhesion.FieldByName('CodigoEntidad').AsInteger))));
			AddText(re_General, CRLF + MSG_CUENTA + #9#9 +
			  Trim(qry_Adhesion.FieldByName('CuentaDebito').AsString));
		end;
	end;
	//
	if qry_Adhesion.FieldByName('DetallePasadas').AsBoolean then begin
		AddText(re_General, CRLF + CRLF + MSG_OTRAS_OPCIONES, 'Arial', 10, [fsBold]);
		AddText(re_General, CRLF + CRLF + MSG_DETALLE_PASADAS);
	end;
	// Mostramos el bot�n de "Realizar Adhesi�n", pero s�lo si no est� hecha.
	if QueryGetValueInt(DMConnections.BaseCAC, Format(
	  'SELECT 1 FROM Cuentas WHERE Patente = ''%s''',
	  [qry_Adhesion.FieldByName('Patente').AsString])) <> 1 then begin
		btn_Ejecutar.Visible := True;
		btn_Ejecutar.Caption := caption_boton;
	end;
end;

procedure TFormDatosOrdenServicio.CargarTag(caption_boton: AnsiString);
resourcestring
	MSG_DATOS_CUENTA_TAG = 'Datos de la Cuenta asocida al TAG';
begin
	AddText(re_General, CRLF + CRLF + MSG_DATOS_CUENTA_TAG,
	  'Arial', 10, [fsBold], clBlack);
	AddText(re_General, CRLF + CRLF + MSG_CLIENTE + #9#9 +
	  OrdenServicio.FieldByName('CodigoPersona').AsString + ' ' +
	  Trim(OrdenServicio.FieldByName('Apellido').AsString) + ', ' +
	  Trim(OrdenServicio.FieldByName('Nombre').AsString));
	AddText(re_General, CRLF + MSG_CUENTA + #9#9 +
	  qry_Facturacion.FieldByName('CodigoCuenta').AsString);
	btn_Ejecutar.Visible := True;
	btn_Ejecutar.Caption := caption_boton;
end;

procedure TFormDatosOrdenServicio.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #27 then Close;
end;

procedure TFormDatosOrdenServicio.btn_EjecutarClick(Sender: TObject);
begin
	case FTipoOrdenServicio of
		OS_ADHESION: EjecutarAdhesion;
		OS_CIERRE_CUENTA: EjecutarCierreCuenta;
		OS_INHABILITACION_TAG: EjecutarCargarTag(OS_INHABILITACION_TAG);
		OS_REHABILITACION_TAG: EjecutarCargarTag(OS_REHABILITACION_TAG);
		OS_FACTURACION_INCORRECTA: EjecutarCargarFacturacion1;
		//OS_RECLAMO_FACTURACION_ADEUDADA: CargarFacturacion1;
		OS_FACTURA_NO_RECIBIDA: EjecutarCargarFacturacion1;
		(*
		OS_RECLAMO_MALAS_INSTALACIONES: EjecutarTratamientoGeneral(OS_RECLAMO_MALAS_INSTALACIONES);
		OS_ACCIDENTE: EjecutarTratamientoGeneral(OS_ACCIDENTE);
		OS_FALTA_SEGURIDAD: EjecutarTratamientoGeneral(OS_FALTA_SEGURIDAD);
		OS_RECLAMO_GENERAL: EjecutarTratamientoGeneral(OS_RECLAMO_GENERAL);
		OS_RECOMENDACION: EjecutarTratamientoGeneral(OS_RECOMENDACION);
        OS_AGRADECIMIENTO: EjecutarTratamientoGeneral(OS_AGRADECIMIENTO);
        OS_LLAMAR_POR_TELEFONO_CLIENTE: EjecutarTratamientoMoroso(OS_LLAMAR_POR_TELEFONO_CLIENTE);
		OS_ENVIAR_MAIL_CLIENTE: EjecutarTratamientoMoroso(OS_ENVIAR_MAIL_CLIENTE);
        OS_ENVIAR_CARTA_CLIENTE: EjecutarTratamientoMoroso(OS_ENVIAR_CARTA_CLIENTE);
		OS_ENVIAR_FAX_CLIENTE: EjecutarTratamientoMoroso(OS_ENVIAR_FAX_CLIENTE);
        OS_ENVIAR_VOICE_MAIL_CLIENTE: EjecutarTratamientoMoroso(OS_ENVIAR_VOICE_MAIL_CLIENTE);
		*)
	end;
	ModalResult := mrOk
end;

procedure TFormDatosOrdenServicio.EjecutarAdhesion;
Var
	f: TFormMantenimientoCuenta;
	g: TFormTerminarTarea;
    //h: tform
begin
	//Obtengo la primer Tareas de la Orden de Servicio de Adhesi�n
    // para Finalizarla si sale todo OK
	Application.CreateForm(TFormMantenimientoCuenta, f);
	if f.InicializaOS(FCodigoOrdenServicio) then begin
		Hide;
		if (f.ShowModal = mrOk) then begin
			//Termino la Tarea.
			Application.CreateForm(TFormTerminarTarea, g);

			if g.Inicializa(FCodigoOrdenServicio, FTarea) and (g.ShowModal = mrOk) then //ReloadTasks;

			g.Release;
        end
		else begin
			(* 10/11/2003  solicitado por Diego Ferruz
			Ordenes de Servicio, si ejecuto la tarea desde Orden,
			trae la infor de la tarea, doy click en realizar adhesion y
            luego cancelo no devuelve la tarea.*)
			EjecutarDevolverTarea(FCodigoOrdenServicio, FTarea);
        end;
	end;
	f.Release;
end;

procedure TFormDatosOrdenServicio.TratamientoGeneral(CODIGO_OS: integer; caption_boton: AnsiString);
resourcestring
    MSG_RECLAMO_MALAS_INSTALACIONES = 'Reclamo por mal estado de las Instalaciones';
	MSG_RECLAMO_ACCIDENTE = 'Reclamo por Accidente';
    MSG_RECLAMO_FALTA_DEGURIDAD = 'Reclamo por Falta de Seguridad';
	MSG_RECLAMO_GENERAL = 'Reclamo General';
    MSG_RECLAMO_RECOMENDACION = 'Recomendaci�n';
	MSG_RECLAMO_AGRADECIMIENTO = 'Agradecimiento';


    MSG_APELLIDO    = 'Apellido:';
	MSG_NOMBRE      = 'Nombre:';
	MSG_DOCUMENTO   = 'Documento:';
    MSG_SEXO        = 'Sexo:';
	MSG_NACIMIENTO  = 'Nacimiento:';
    MSG_DOMICILIO   = 'Domicilio:';
    MSG_PAIS        = 'Pais:';
    MSG_REGION      = 'Regi�n:';
    MSG_COMUNA      = 'Comuna:';
    MSG_CODIGO_POSTAL = 'C�digo Postal:';
	MSG_TELEFONO    = 'Tel�fono:';
	MSG_MAIL        = 'e-mail:';
var
	DescWorkflow: AnsiString;
begin
	DescWorkflow := Trim(QueryGetValue(DMConnections.BaseCAC,
                    'SELECT Descripcion FROM Workflows WHERE CodigoWorkflow = ' +
					IntToStr(CODIGO_OS)));
	Case CODIGO_OS of
		OS_RECLAMO_MALAS_INSTALACIONES: AddText(re_General, CRLF + CRLF + MSG_RECLAMO_MALAS_INSTALACIONES, 'Arial', 10, [fsBold]);
		OS_ACCIDENTE: AddText(re_General, CRLF + CRLF + MSG_RECLAMO_ACCIDENTE, 'Arial', 10, [fsBold]);
		OS_FALTA_SEGURIDAD: AddText(re_General, CRLF + CRLF + MSG_RECLAMO_FALTA_DEGURIDAD, 'Arial', 10, [fsBold]);
		OS_RECLAMO_GENERAL: AddText(re_General, CRLF + CRLF + MSG_RECLAMO_GENERAL, 'Arial', 10, [fsBold]);
		OS_RECOMENDACION: AddText(re_General, CRLF + CRLF + MSG_RECLAMO_RECOMENDACION, 'Arial', 10, [fsBold]);
		OS_AGRADECIMIENTO: AddText(re_General, CRLF + CRLF + MSG_RECLAMO_AGRADECIMIENTO, 'Arial', 10, [fsBold]);
		else AddText(re_General, CRLF + CRLF + DescWorkflow, 'Arial', 10, [fsBold]);
	end;

	AddText(re_General, CRLF + CRLF + MSG_APELLIDO + #9 + Trim(OrdenServicio.FieldByName('Apellido').AsString) +
				' ' + Trim(OrdenServicio.FieldByName('ApellidoMaterno').AsString));
	AddText(re_General, CRLF + MSG_NOMBRE + #9 + Trim(OrdenServicio.FieldByName('Nombre').AsString));
	AddText(re_General, CRLF + MSG_DOCUMENTO + #9 + Trim(OrdenServicio.FieldByName('CodigoDocumento').AsString) + ' '
			+  Trim(OrdenServicio.FieldByName('NumeroDocumento').AsString));
	AddText(re_General, CRLF + MSG_SEXO + #9#9 + Trim(OrdenServicio.FieldByName('Sexo').AsString));
	AddText(re_General, CRLF + MSG_NACIMIENTO + #9 +
				iif(OrdenServicio.FieldByName('FechaNacimiento').IsNull, '',
					Trim(OrdenServicio.FieldByName('FechaNacimiento').AsString)));

	AddText(re_General, CRLF + CRLF + MSG_DOMICILIO + #9 +
            ObtenerDomiciliosPersonaParticular.FieldByName('DireccionCompleta').AsString);
	if not (ObtenerDomiciliosPersonaParticular.FieldByName('CodigoPostal').IsNull) then begin
		AddText(re_General, CRLF + MSG_CODIGO_POSTAL + #9 +
			Trim(ObtenerDomiciliosPersonaParticular.FieldByName('CodigoPostal').AsString))
	end;                    


	AddText(re_General, CRLF + CRLF + MSG_TELEFONO + #9 +
		OrdenServicio.FieldByName('TelefonoPersonal').AsString);
	AddText(re_General, CRLF + MSG_MAIL + #9#9 +
		OrdenServicio.FieldByName('Email').AsString);

    btn_Ejecutar.Visible := True;
    btn_Ejecutar.Caption := caption_boton;
end;

procedure TFormDatosOrdenServicio.TratamientoMoroso(CODIGO_OS: integer; caption_boton: AnsiString; OS: integer);
resourcestring
	MSG_APELLIDO      = 'Apellido:';
	MSG_NOMBRE        = 'Nombre:';
	MSG_DOCUMENTO     = 'Documento:';
    MSG_SEXO          = 'Sexo:';
    MSG_NACIMIENTO    = 'Nacimiento:';
	MSG_DOMICILIO     = 'Domicilio:';
    MSG_PAIS          = 'Pais:';
    MSG_REGION        = 'Regi�n:';
    MSG_COMUNA        = 'Comuna:';
	MSG_CODIGO_POSTAL = 'C�digo Postal:';
	MSG_TELEFONO      = 'Tel�fono:';
    MSG_MAIL          = 'e-mail:';
var
    DescWorkflow: AnsiString;
begin
    qry_DatosMoroso.Parameters.ParamByName('CodigoOrdenServicio').Value := OS;
	If (OpenTables([qry_DatosMoroso]) = False) then Exit;

	DescWorkflow := Trim(QueryGetValue(DMConnections.BaseCAC,
                    'SELECT Descripcion FROM Workflows WHERE CodigoWorkflow = ' +
                    IntToStr(CODIGO_OS)));

	AddText(re_General, CRLF + CRLF + DescWorkflow, 'Arial', 10, [fsBold]);
	AddText(re_General, CRLF + CRLF + MSG_APELLIDO + #9 + Trim(qry_DatosMoroso.FieldByName('Apellido').AsString) +
                ' ' + Trim(qry_DatosMoroso.FieldByName('ApellidoMaterno').AsString));
	AddText(re_General, CRLF + MSG_NOMBRE + #9 + Trim(qry_DatosMoroso.FieldByName('Nombre').AsString));
	AddText(re_General, CRLF + MSG_DOCUMENTO + #9 + Trim(qry_DatosMoroso.FieldByName('CodigoDocumento').AsString) + ' '
            +  Trim(qry_DatosMoroso.FieldByName('NumeroDocumento').AsString));
    //AddText(re_General, CRLF + MSG_SEXO + #9#9 + Trim(qry_DatosMoroso.FieldByName('Sexo').AsString));
	AddText(re_General, CRLF + MSG_NACIMIENTO + #9 +
				iif(qry_DatosMoroso.FieldByName('FechaNacimiento').IsNull, '',
					Trim(qry_DatosMoroso.FieldByName('FechaNacimiento').AsString)));
	AddText(re_General, CRLF + CRLF + MSG_DOMICILIO + #9 +
			ArmarDomicilioSimple(DMConnections.BaseCAC,
                        ObtenerDomiciliosPersonaParticular.FieldByName('Calle').AsString,
						ObtenerDomiciliosPersonaParticular.FieldByName('Numero').AsInteger,
                        ObtenerDomiciliosPersonaParticular.FieldByName('Piso').AsInteger,
                        ObtenerDomiciliosPersonaParticular.FieldByName('Dpto').AsString,
						ObtenerDomiciliosPersonaParticular.FieldByName('Detalle').AsString));
	AddText(re_General, CRLF + CRLF + MSG_TELEFONO + #9 +
		OrdenServicio.FieldByName('TelefonoPersonal').AsString);
	AddText(re_General, CRLF + MSG_MAIL + #9#9 +
		OrdenServicio.FieldByName('Email').AsString);

    btn_Ejecutar.Visible := True;
    btn_Ejecutar.Caption := caption_boton;

	qry_DatosMoroso.Close;
end;

procedure TFormDatosOrdenServicio.EjecutarCierreCuenta;
Var
	l: TFormCerrarCuenta;
	g: TFormTerminarTarea;
begin
	Application.createForm(TFormCerrarCuenta, l);
	if l.inicializar(FCodigoOrdenServicio) then begin
		Hide;
		if (l.ShowModal = mrOk) then begin
			//Termino la Tarea.
			Application.CreateForm(TFormTerminarTarea, g);

			if g.Inicializa(FCodigoOrdenServicio, FTarea) and (g.ShowModal = mrOk) then //ReloadTasks;

			g.Release;
        end;
    end;
    l.Release;
end;


procedure TFormDatosOrdenServicio.EjecutarCargarTag(tipoOS: integer);
var
	l: TfrmHabilitarDeshabilitarTag;
    g: TFormTerminarTarea;
begin
	//Revisar si hay que usar TipoOS, si se manda siempre false en el inicializa
    Application.createForm(TfrmHabilitarDeshabilitarTag, l);
	if l.inicializa(FCodigoOrdenServicio, False) then begin
        Hide;
		if (l.ShowModal = mrOk) then begin
        	//Termino la Tarea.
			Application.CreateForm(TFormTerminarTarea, g);

			if g.Inicializa(FCodigoOrdenServicio, FTarea) and (g.ShowModal = mrOk) then //ReloadTasks;

			g.Release;
        end;
    end;
	l.Release;
end;

procedure TFormDatosOrdenServicio.EjecutarCargarFacturacion1;
var
	h: TFrmAjusteFacturacion;
    g: TFormTerminarTarea;
begin
//	Application.createForm(TfrmAjusteFacturacion, h);
//    if h.inicializa(False, FCodigoOrdenServicio) then begin
//		Hide;
//		if (h.ShowModal = mrOk) then begin
//        	//Termino la Tarea.
//			Application.CreateForm(TFormTerminarTarea, g);
//
//			if g.Inicializa(FCodigoOrdenServicio, FTarea) and (g.ShowModal = mrOk) then //ReloadTasks;
//
//			g.Release;
//        end;
//    end;
//	h.Release;
end;

procedure TFormDatosOrdenServicio.CargarTAGMAlFuncionamiento;
resourcestring
	MSG_DATOS_CLIENTE = 'Datos del Cliente';
	MSG_APELLIDO    = 'Apellido:';
    MSG_APELLIDO_MATERNO    = 'Apellido mat.:';
    MSG_NOMBRE      = 'Nombre:';
	MSG_DOCUMENTO   = 'Documento:';
    MSG_DOMICILIO_PERSONAL   = 'Domicilio Particular:';
    MSG_DOMICILIO_DOCUMENTOS = 'Domicilio Comercial:';
    MSG_DOMICILIO   = 'Domicilio:';
	MSG_REGION      = 'Regi�n:';
    MSG_COMUNA      = 'Comuna:';
	MSG_TELEFONO    = 'Tel�fono:';
	MSG_MAIL        = 'e-mail:';
    MSG_DATOS_VEHICULO = 'Datos del Veh�culo:';
	MSG_VEHICULO    = 'Veh�culo:';
	MSG_PATENTE     = 'Patente:';
	MSG_CATEGORIA   = 'Categor�a:';
    MSG_OTRAS_OPCIONES = 'Otras opciones de adhesi�n';
    MSG_TAG = 'TAG:';

begin
    //fsUnderline
	AddText(re_General, CRLF + CRLF + MSG_DATOS_CLIENTE,
	  'Arial', 10, [fsBold]);
	//
	AddText(re_General, CRLF + CRLF + MSG_APELLIDO + #9 +
	  Trim(OrdenServicio.FieldByName('Apellido').AsString));
	if trim(OrdenServicio.FieldByName('ApellidoMaterno').AsString) <> '' then begin
		AddText(re_General, CRLF + MSG_APELLIDO_MATERNO + #9 +
		  Trim(OrdenServicio.FieldByName('ApellidoMaterno').AsString))
	end;
	AddText(re_General, CRLF + MSG_NOMBRE + #9 +
	  Trim(OrdenServicio.FieldByName('Nombre').AsString));
	AddText(re_General, CRLF + MSG_DOCUMENTO + #9 +
	  Trim(OrdenServicio.FieldByName('CodigoDocumento').AsString) + ' ' +
	  Trim(OrdenServicio.FieldByName('NumeroDocumento').AsString));

	// Domicilio Personal
	AddText(re_General, CRLF + CRLF + MSG_DOMICILIO_PERSONAL,
            'Arial', 10, [fsUnderline]);

	AddText(re_General, CRLF + CRLF + MSG_DOMICILIO + #9 +
			ArmarDomicilioSimple(DMConnections.BaseCAC,
                        ObtenerDomiciliosPersonaParticular.FieldByName('Calle').AsString,
						ObtenerDomiciliosPersonaParticular.FieldByName('Numero').AsInteger,
                        ObtenerDomiciliosPersonaParticular.FieldByName('Piso').AsInteger,
                        ObtenerDomiciliosPersonaParticular.FieldByName('Dpto').AsString,
						ObtenerDomiciliosPersonaParticular.FieldByName('Detalle').AsString));
	AddText(re_General, CRLF + MSG_REGION + #9#9 +
	  Trim(ObtenerDomiciliosPersonaParticular.FieldByName('DescriRegion').AsString));
	AddText(re_General, CRLF + MSG_COMUNA + #9 +
	  Trim(ObtenerDomiciliosPersonaParticular.FieldByName('DescriComuna').AsString));

	if not (OrdenServicio.FieldByName('TelefonoPersonal').IsNull) then begin
		AddText(re_General, CRLF + CRLF + MSG_TELEFONO + #9 +
			OrdenServicio.FieldByName('TelefonoPersonal').AsString)
	end;

	// Domicilio Documentos
	if  (trim(ObtenerDomiciliosPersonaComercial.FieldByName('Calle').AsString) <> '') then begin
		AddText(re_General, CRLF + CRLF + MSG_DOMICILIO_DOCUMENTOS,
				'Arial', 10, [fsUnderline]);
		AddText(re_General, CRLF + CRLF  + MSG_DOMICILIO + #9 +
			ArmarDomicilioSimple(DMConnections.BaseCAC,
                        ObtenerDomiciliosPersonaParticular.FieldByName('Calle').AsString,
						ObtenerDomiciliosPersonaParticular.FieldByName('Numero').AsInteger,
                        ObtenerDomiciliosPersonaParticular.FieldByName('Piso').AsInteger,
                        ObtenerDomiciliosPersonaParticular.FieldByName('Dpto').AsString,
						ObtenerDomiciliosPersonaParticular.FieldByName('Detalle').AsString));
		AddText(re_General, CRLF + MSG_REGION + #9#9 +
		  Trim(ObtenerDomiciliosPersonaComercial.FieldByName('DescriRegion').AsString));
		AddText(re_General, CRLF + MSG_COMUNA + #9 +
		  Trim(ObtenerDomiciliosPersonaComercial.FieldByName('DescriComuna').AsString));
	end;

	AddText(re_General, CRLF + MSG_MAIL + #9#9 +
		OrdenServicio.FieldByName('Email').AsString);
	AddText(re_General, CRLF + CRLF + MSG_DATOS_VEHICULO,
        'Arial', 10, [fsUnderline]);

	AddText(re_General, CRLF + CRLF + MSG_VEHICULO + #9 +
	  Trim(qry_Facturacion.FieldByName('DescriMarca').AsString) + ' ' +
	  Trim(qry_Facturacion.FieldByName('DescriModelo').AsString) + ' ' +
	  Trim(qry_Facturacion.FieldByName('DescriColor').AsString));
	AddText(re_General, CRLF + MSG_PATENTE + #9 +
	  Trim(qry_Facturacion.FieldByName('Patente').AsString));
	AddText(re_General, CRLF + MSG_CATEGORIA + #9 +
	  Trim(qry_Facturacion.FieldByName('DescriCategoria').AsString));
    AddText(re_General, CRLF + MSG_TAG + #9#9 +
	  Trim(qry_Facturacion.FieldByName('ContextMark').AsString) + ' - ' +
	  Trim(qry_Facturacion.FieldByName('ContractSerialNumber').AsString));
	//
end;

procedure TFormDatosOrdenServicio.CargarOSReparacion(CODIGO_OS: integer; OS: integer; caption_boton: AnsiString = '');
resourcestring
    MSG_RECLAMO_MALAS_INSTALACIONES = 'Reclamo por mal estado de las Instalaciones';
    MSG_RECLAMO_ACCIDENTE = 'Reclamo por Accidente';
    MSG_RECLAMO_FALTA_DEGURIDAD = 'Reclamo por Falta de Seguridad';
    MSG_RECLAMO_GENERAL = 'Reclamo General';
    MSG_RECLAMO_RECOMENDACION = 'Recomendaci�n';
    MSG_RECLAMO_AGRADECIMIENTO = 'Agradecimiento';

    MSG_APELLIDO        = 'Apellido:';
    MSG_NOMBRE          = 'Nombre:';
    MSG_FALLA           = 'Tipo de falla:';
    MSG_CAUSA           = 'Causa:';
    MSG_MANTENIMIENTO   = 'Mantenimiento:';
    MSG_UBICACION       = 'Ubicacion:';

var
	tabla, DescWorkflow: AnsiString;
begin
	DescWorkflow := Trim(QueryGetValue(DMConnections.BaseCAC,
                    'SELECT Descripcion FROM Workflows WHERE CodigoWorkflow = ' +
                    IntToStr(CODIGO_OS)));
    tabla := '';

	Case CODIGO_OS of
        OS_REPARAR_POSTE_SOS: tabla := 'OrdenesServicioReparacionPostesSOS';
        OS_REPARAR_PORTICO: tabla := 'OrdenesServicioReparacionPortico';
        OS_REPARAR_PAVIMENTO: tabla := 'OrdenesServicioReparacionPavimento';
        OS_GENERAR_ORDEN_DE_TRABAJO: tabla := 'OrdenesServicioSolicitudTrabajo';
	end;

    AddText(re_General, CRLF + CRLF + DescWorkflow, 'Arial', 10, [fsBold]);
	AddText(re_General, CRLF + CRLF + MSG_APELLIDO + #9 + Trim(OrdenServicio.FieldByName('Apellido').AsString) +
				' ' + Trim(OrdenServicio.FieldByName('ApellidoMaterno').AsString));
	AddText(re_General, CRLF + MSG_NOMBRE + #9 + Trim(OrdenServicio.FieldByName('Nombre').AsString));

    if tabla <> ''  then begin
        with qry_reparacion do
        begin
			Close;
			SQL.Clear;
            SQL.Text := '';
            SQL.Text := DameSelectOSReparacion(tabla, IntToStr(OS));
            //Parameters.ParamByName('CodigoOrdenServicio').Value := OS;
            OpenTables([qry_reparacion]);
            AddText(re_General, CRLF + MSG_FALLA + #9 + Trim(qry_reparacion.FieldByName('DescTipoFalla').AsString));
			AddText(re_General, CRLF + MSG_CAUSA + #9 + #9 +Trim(qry_reparacion.FieldByName('DescCausa').AsString));
            AddText(re_General, CRLF + MSG_MANTENIMIENTO + #9 + Trim(qry_reparacion.FieldByName('DescMantenimiento').AsString));
            AddText(re_General, CRLF + MSG_UBICACION + #9 + Trim(qry_reparacion.FieldByName('DescUbicacion').AsString));
        end;
    end;
    btn_Ejecutar.Visible := True;
    btn_Ejecutar.Caption := caption_boton;
end;

function TFormDatosOrdenServicio.DameSelectOSReparacion(
  tabla, OS: AnsiString): AnsiString;
begin
    result := 'SELECT DescTipoFalla = (SELECT Descripcion from TiposFalla WHERE ' +
            'CodigoTipoFalla = '+tabla+'.CodigoTipoFalla), ' +
	  'DescUbicacion = (SELECT Descripcion from UbicacionesMantenimiento WHERE ' +
		'CodigoUbicacion = '+tabla+'.CodigoUbicacion), ' +
      'DescMantenimiento = (SELECT Descripcion from TiposMAntenimiento WHERE ' +
        'CodigoTipoMantenimiento = '+tabla+'.CodigoTipoMantenimiento), ' +
      'DescCausa = (SELECT Descripcion from TiposCausa WHERE ' +
        'CodigoTipoCausa = '+tabla+'.CodigoTipoCausa) ' +
    ' FROM ' + tabla +' WHERE CodigoOrdenServicio = '+ OS;
end;

procedure TFormDatosOrdenServicio.EjecutarDevolverTarea(OS, Tarea: integer);
resourcestring
    CAPTION_DEVOLVER_TAREA  = 'Devolver Tarea';
begin
	if MsgBox(MSG_DEVOLVER_TAREA, CAPTION_DEVOLVER_TAREA, MB_ICONQUESTION or MB_YESNO) <> IDNO then begin
		QueryExecute(DMConnections.BaseCAC, Format(
		  'UPDATE OrdenesServicioTareas SET CodigoUsuario = NULL WHERE ' +
		  'CodigoOrdenServicio = %d AND ' +
		  ' CodigoTarea = %d', [OS, Tarea]));
	end;
end;

end.

