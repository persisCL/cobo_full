{********************************** Unit Header ********************************
File Name : frmRptCierreTurno.pas
Author :
Date Created:
Language : ES-AR
Description : Reporte de Cierre de Turno

Revision 1:
    Author : ggomez
    Date : 11/01/2006
    Description :
        - Agregu� los Totalizadores de los grupos de Cheques y Cupones,
        y en el de Canal y Forma de Pago.
        - En la parte de Diferencia en Efectivo Calculada por Sistema, agregu�
        las Notas de Cobro a Infractores, los Cobros Anticipados y los Cobros
        con Vales Vista.

Revision 2:
    Author : ggomez
    Date : 12/01/2006
    Description : Quit� la l�nea Cobros en Efectivo del Turno.

Revision 3:
    Author: nfernandez
    Date: 16/04/2007
    Description : Se agreg� el Total Pagado de Cuotas de Arriendo en el resumen
    del reporte "Liquidaci�n de Efectivo y Cupones".

Revision 4:
    Author: nfernandez
    Date: 18/04/2007
    Description : Se agreg� "Cobros con Pagar�" en el resumen del reporte
    "Liquidaci�n de Efectivo y Cupones".

Revision 5:
    Author: nfernandez
    Date: 19/04/2008
    Description : Se agrega el Sub Reporte de Anulaciones de Pagos y en todos
    los otros subreportes se consideran los importes tambien de los pagos
    anulados.

Revision 6:
    Author: nfernandez
    Date: 13/05/2008
    Description : Se hace las siguientes modificaciones en la seccion de resumen de la pagina
    "Liquidaci�n de Efectivo y Cupones":
    - Para los debitos se consideran todos los pagos.
    - Para los creditos se consideran los No Anulados y los Anulados que tengan FechaAnulaci�n posterior a
      la fecha de cierre del turno.
    - Se agregan las lineas "Cobros Efectivo" y "Cobros Anulados en el Turno"
    - Se agrega un linea para calcular la diferencia entre debitos y creditos
    - La linea "Diferencia en efectivo calculada por sistema" se genera con el
      calculo: "Monto de apertura de caja" + "Cobros Efectivo" - "Liquidaci�n en efectivo del turno"


Firma       :   SS_959_A_MBE_20131008
Description :   Se le "restan" los comprobantes DV a la secci�n
                Diferencias por forma de Pago, campo "Infome Sistema", forma de pago "efectivo"
                en el informe de cierre de turno


Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.

*******************************************************************************}
unit frmRptCierreTurno;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, UtilRB, ppParameter, ppModule, raCodMod, ppBands,
  ppCtrls, ppPrnabl, ppClass, ppCache, ppProd, ppReport, ppEndUsr, ppDB,
  ppComm, ppRelatv, ppDBPipe, DMConnection, ppStrtch, ppSubRpt, Grids,
  DBGrids, ppRegion, PeaTypes, PeaProcs, CobranzasResources, PeaProcsCN, SysUtilsCN,
  ConstParametrosGenerales, UtilProc, jpeg;                                                   //SS_1147_NDR_20140710

type
  TFormRptCierreTurno = class(TForm)
    DBPInformadoSaldoFinal: TppDBPipeline;
    rbiListado: TRBInterface;
    dsInformadoSaldoFinal: TDataSource;
    ObtenerSaldoFinalInformadoTurno: TADOStoredProc;
    DBPCalculadoSaldoInicial: TppDBPipeline;
    ObtenerSaldoInicialTurno: TADOStoredProc;
    dsCalculadoSaldoInicial: TDataSource;
    DBPCalculadoEntregado: TppDBPipeline;
    ObtenerSaldoEntregadoTurno: TADOStoredProc;
    dsCalculadoEntregado: TDataSource;
    DBPCalculadoSaldoFinal: TppDBPipeline;
    ObtenerSaldoFinalCalculadoTurno: TADOStoredProc;
    dsCalculadoSaldoFinal: TDataSource;
    DBPSaldoDiferencia: TppDBPipeline;
    ObtenerSaldoDiferenciaTurno: TADOStoredProc;
    dsSaldoDiferencia: TDataSource;
    DBPTotalesConvenios: TppDBPipeline;
    ObtenerTotalesConveniosTurno: TADOStoredProc;
    dsTotalesConvenios: TDataSource;
    DBPEncabezadoCierreTurno: TppDBPipeline;
    ObtenerEncabezadoCierreTurno: TADOStoredProc;
    dsEncabezadoCierreTurno: TDataSource;
    dsSaldosAdicionales: TDataSource;
    ObtenerSaldosAdicionales: TADOStoredProc;
    dbpSaldosAdicionales: TppDBPipeline;
    ObtenerSaldosAdicionalesNumeroVale: TStringField;
    ObtenerSaldosAdicionalesCodigocategoria: TWordField;
    ObtenerSaldosAdicionalesCategoria: TStringField;
    ObtenerSaldosAdicionalesCantidadInicial: TLargeintField;
    ObtenerSaldoFinalInformadoTurnoNumeroVale: TStringField;
    ObtenerSaldoFinalInformadoTurnoCodigocategoria: TWordField;
    ObtenerSaldoFinalInformadoTurnoCategoria: TStringField;
    ObtenerSaldoFinalInformadoTurnoCantidadFinal: TLargeintField;
    ObtenerSaldoInicialTurnoNumeroVale: TStringField;
    ObtenerSaldoInicialTurnoCodigocategoria: TWordField;
    ObtenerSaldoInicialTurnoCategoria: TStringField;
    ObtenerSaldoInicialTurnoCantidadInicial: TLargeintField;
    ObtenerSaldoInicialTurnoNumeroTurno: TIntegerField;
    DBCalculadoDevuelto: TppDBPipeline;
    dsObtenerSaldoDevueltoTurno: TDataSource;
    ObtenerSaldoDevueltoTurno: TADOStoredProc;
    rptCierreTurno: TppReport;
    ppDetalleCobrosEfectivo: TppDBPipeline;
    ppDetalleCobrosEfectivoppField1: TppField;
    ppDetalleCobrosEfectivoppField2: TppField;
    ppDetalleCobrosEfectivoppField3: TppField;
    ppDetalleCobrosEfectivoppField4: TppField;
    ppDetalleCobrosEfectivoppField5: TppField;
    ppDetalleCobrosEfectivoppField6: TppField;
    dsLiquidacionEfectivo: TDataSource;
    spObtenerLiquidacionTurnoEfectivo: TADOStoredProc;
    dsDetallesTurno: TDataSource;
    ppDBPipelineDetalleTurno: TppDBPipeline;
    spObtenerDetalleTurno: TADOStoredProc;
    spObtenerLiquidacionTurnoCupones: TADOStoredProc;
    dsObtenerLiquidacionTurnoCupones: TDataSource;
    ppDBPipelineLiquidacionCupones: TppDBPipeline;
    sp_ObtenerTotalCobradoNotasCobroTurno: TADOStoredProc;
    sp_ObtenerTotalCobradoTarjetaCreditoTurno: TADOStoredProc;
    sp_ObtenerTotalCobradoTarjetaDebitoTurno: TADOStoredProc;
    sp_ObtenerTotalNotasCreditoPagadasTurno: TADOStoredProc;
    ppLiquidacionPorCanal: TppDBPipeline;
    dsLiquidacionPorCanal: TDataSource;
    spObtenerLiquidacionPorCanal: TADOStoredProc;
    spObtenerTotalCobradoNotasCobroInfractorTurno: TADOStoredProc;
    spObtenerTotalCobradoSinComprobante: TADOStoredProc;
    spObtenerTotalCobradoValesVistaTurno: TADOStoredProc;
    sp_ObtenerTotalCobradoBoletasTurno: TADOStoredProc;
    spObtenerTotalCobradoChequesTurnoDia: TADOStoredProc;
    sp_ObtenerTotalCobradoCuotasArriendoTurno: TADOStoredProc;
    sp_ObtenerTotalCobradoPagareTurno: TADOStoredProc;
    spObtenerAnulacionesCierreTurno: TADOStoredProc;
    dsObtenerAnulacionesCierreTurno: TDataSource;
    ppObtenerAnulacionesCierreTurno: TppDBPipeline;
    spObtenerTotalCobrosAnuladosTurno: TADOStoredProc;
    spObtenerTotalCobradoEfectivoTurno: TADOStoredProc;
    spObtenerLiquidacionPorCanalRefinan: TADOStoredProc;
    ppDBPipeline1LiquidacionPorCanalRefinan: TppDBPipeline;
    dsObtenerLiquidacionPorCanalRefinan: TDataSource;
    spObtenerRefinanciacionTotalesPagosTurno: TADOStoredProc;
    spObtenerRefinanciacionDiferenciasPiesNoLiquidados: TADOStoredProc;
    dsObtenerRefinanciacionDiferenciasPiesNoLiquidados: TDataSource;
    ppDBPipelinePiesNoLiquidados: TppDBPipeline;
    spObtenerTotalCobradoDepositoAnticipadoTurno: TADOStoredProc;
    spObtenerLiquidacionPorCanalDetalle: TADOStoredProc;
    dsObtenerLiquidacionPorCanalDetalle: TDataSource;
    ppDBpplnObtenerLiquidacionPorCanalDetalle: TppDBPipeline;
    ppParameterList1: TppParameterList;
    ppDetailBand10: TppDetailBand;
    ppLiquidacionEfectivoReport: TppSubReport;
    ppChildReport11: TppChildReport;
    ppTitleBand19: TppTitleBand;
    ppLabel64: TppLabel;
    ppLabel65: TppLabel;
    ppDBText50: TppDBText;
    ppLine41: TppLine;
    ppLabel66: TppLabel;
    ppLabel67: TppLabel;
    ppLabel68: TppLabel;
    ppDBText51: TppDBText;
    ppDBText52: TppDBText;
    ppDBText53: TppDBText;
    ppLine42: TppLine;
    ppLabel69: TppLabel;
    ppLabel71: TppLabel;
    ppDBText54: TppDBText;
    ppLabel72: TppLabel;
    ppImage3: TppImage;
    ppLabel73: TppLabel;
    ppLabel40: TppLabel;
    ppDetailBand21: TppDetailBand;
    ppRegionEfectivo: TppRegion;
    ppSubReportEfectivo: TppSubReport;
    ppChildReport20: TppChildReport;
    ppTitleBand20: TppTitleBand;
    ppShape3: TppShape;
    ppLabel74: TppLabel;
    ppLabel75: TppLabel;
    ppLabel76: TppLabel;
    ppLabel77: TppLabel;
    ppLabel78: TppLabel;
    ppDetailBand22: TppDetailBand;
    ppDBText55: TppDBText;
    ppDBText56: TppDBText;
    ppDBText57: TppDBText;
    ppDBText58: TppDBText;
    ppSummaryBand20: TppSummaryBand;
    ppShape1: TppShape;
    ppLabel79: TppLabel;
    ppSumaEfectivo: TppDBCalc;
    raCodeModule3: TraCodeModule;
    ppRegionCupones: TppRegion;
    ppSubReportCupones: TppSubReport;
    ppSurRepCupones: TppChildReport;
    ppTitleBand9: TppTitleBand;
    ppShape2: TppShape;
    ppLabel41: TppLabel;
    ppLabel42: TppLabel;
    ppLabel84: TppLabel;
    ppLabel87: TppLabel;
    ppLabel70: TppLabel;
    ppLabel85: TppLabel;
    ppDetailBand11: TppDetailBand;
    ppDBText30: TppDBText;
    ppDBText64: TppDBText;
    ppDBText31: TppDBText;
    ppDBText63: TppDBText;
    ppDBText29: TppDBText;
    ppSummaryBand9: TppSummaryBand;
    ppShape14: TppShape;
    ppLabel7: TppLabel;
    ppDBCalc1: TppDBCalc;
    raCodeModule4: TraCodeModule;
    ppRegion1: TppRegion;
    ppsrDetalleCanales: TppSubReport;
    ppChildReport1: TppChildReport;
    ppTitleBand1: TppTitleBand;
    ppShape13: TppShape;
    ppLabel1: TppLabel;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppDetailBand1: TppDetailBand;
    ppDBText1: TppDBText;
    ppDBText2: TppDBText;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppSummaryBand1: TppSummaryBand;
    ppShape15: TppShape;
    ppLabel9: TppLabel;
    ppDBCalc2: TppDBCalc;
    raCodeModule1: TraCodeModule;
    raCodeModule5: TraCodeModule;
    ppSubReport2: TppSubReport;
    ppChildReport4: TppChildReport;
    ppDetailBand4: TppDetailBand;
    ppShape4: TppShape;
    ppLabel86: TppLabel;
    lbl_TitTotalCobradoNotasCobro: TppLabel;
    lblTitTotalCobradoNotaCobroInfractor: TppLabel;
    ppLabel4: TppLabel;
    ppLabel8: TppLabel;
    ppLabel14: TppLabel;
    lbl_TotalCobradoCuotas: TppLabel;
    lbl_TotalCobradoBoletas: TppLabel;
    lbl_TotalPagadoNotasCredito: TppLabel;
    lblTotalCobradoNotasCobroInfractor: TppLabel;
    lbl_TotalCobradoNotasCobro: TppLabel;
    lblCobrosAnticipados: TppLabel;
    lblTitCobrosAnticipados: TppLabel;
    ppLabel28: TppLabel;
    lblCobrosAnulados: TppLabel;
    ppSubReport3: TppSubReport;
    ppChildReport5: TppChildReport;
    ppTitleBand4: TppTitleBand;
    ppShape19: TppShape;
    ppLabel95: TppLabel;
    ppLabel96: TppLabel;
    ppLabel97: TppLabel;
    ppLabel98: TppLabel;
    ppLabel99: TppLabel;
    ppShape16: TppShape;
    ppLabel88: TppLabel;
    ppDetailBand5: TppDetailBand;
    ppDBText18: TppDBText;
    ppDBText19: TppDBText;
    ppDBText20: TppDBText;
    ppDBText21: TppDBText;
    ppSummaryBand4: TppSummaryBand;
    ppShape20: TppShape;
    ppLabel100: TppLabel;
    ppDBCalc5: TppDBCalc;
    ppSubReport1: TppSubReport;
    ppChildReport3: TppChildReport;
    ppDetailBand3: TppDetailBand;
    ppShape28: TppShape;
    ppLabel105: TppLabel;
    ppLabel108: TppLabel;
    ppLabel109: TppLabel;
    ppLabel110: TppLabel;
    ppLabel111: TppLabel;
    ppLabel112: TppLabel;
    ppLabel113: TppLabel;
    ppLabel114: TppLabel;
    ppLabel115: TppLabel;
    pplblChequesImporteSistema: TppLabel;
    pplblChequesImporteLiquidado: TppLabel;
    pplblChequesImporteDiferencia: TppLabel;
    pplblTarjetaCreditoImporteSistema: TppLabel;
    pplblTarjetaCreditoImporteLiquidado: TppLabel;
    pplblTarjetaCreditoImporteDiferencia: TppLabel;
    pplblTarjetaDebitoImporteSistema: TppLabel;
    pplblTarjetaDebitoImporteLiquidado: TppLabel;
    pplblTarjetaDebitoImporteDiferencia: TppLabel;
    pplblValeVistaImporteSistema: TppLabel;
    pplblValeVistaImporteLiquidado: TppLabel;
    pplblValeVistaImporteDiferencia: TppLabel;
    ppShape29: TppShape;
    ppLabel117: TppLabel;
    ppLabel118: TppLabel;
    pplblEfectivoImporteSistema: TppLabel;
    pplblEfectivoImporteLiquidado: TppLabel;
    pplblEfectivoImporteDiferencia: TppLabel;
    ppLabel122: TppLabel;
    pplblEfectivoImporteApertura: TppLabel;
    ppLabel124: TppLabel;
    ppLabel125: TppLabel;
    ppLabel126: TppLabel;
    ppLabel127: TppLabel;
    ppSubReport4: TppSubReport;
    ppChildReport6: TppChildReport;
    ppTitleBand3: TppTitleBand;
    ppShape30: TppShape;
    ppLabel80: TppLabel;
    ppLabel81: TppLabel;
    ppLabel82: TppLabel;
    ppLabel83: TppLabel;
    ppLabel119: TppLabel;
    ppLabel120: TppLabel;
    ppDetailBand6: TppDetailBand;
    ppDBText14: TppDBText;
    ppDBText15: TppDBText;
    ppDBText16: TppDBText;
    ppDBText17: TppDBText;
    ppDBText22: TppDBText;
    ppLabel116: TppLabel;
    ppLabel121: TppLabel;
    pplblDAImporteSistema: TppLabel;
    pplblDAImporteLiquidado: TppLabel;
    pplblDAImporteDiferencia: TppLabel;
    ppSubReport5: TppSubReport;
    ppChildReport7: TppChildReport;
    ppDetailBand7: TppDetailBand;
    ppDBText59: TppDBText;
    ppDBText60: TppDBText;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppLabel123: TppLabel;
    ppDBText23: TppDBText;
    ppShape25: TppShape;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppAnulaciones: TppSubReport;
    ppChildReport2: TppChildReport;
    ppTitleBand2: TppTitleBand;
    ppImage1: TppImage;
    ppLabel16: TppLabel;
    ppLabel17: TppLabel;
    ppLine2: TppLine;
    ppLabel18: TppLabel;
    ppDBText3: TppDBText;
    ppLabel19: TppLabel;
    ppDBText6: TppDBText;
    ppLabel20: TppLabel;
    ppDBText7: TppDBText;
    ppLabel21: TppLabel;
    ppLabel22: TppLabel;
    ppDBText8: TppDBText;
    ppDBText9: TppDBText;
    ppLine3: TppLine;
    ppLabel23: TppLabel;
    ppLabel24: TppLabel;
    ppLabel25: TppLabel;
    ppLabel26: TppLabel;
    ppDetailBand2: TppDetailBand;
    ppDBText10: TppDBText;
    ppDBText11: TppDBText;
    ppDBText12: TppDBText;
    ppDBText13: TppDBText;
    ppSummaryBand2: TppSummaryBand;
    ppLabel27: TppLabel;
    ppDBCalc3: TppDBCalc;
    ppLiquidacionTAGsReport: TppSubReport;
    ppChildReport10: TppChildReport;
    ppTitleBand10: TppTitleBand;
    ppLabel30: TppLabel;
    ppLabel31: TppLabel;
    ppDBText24: TppDBText;
    ppLine11: TppLine;
    ppLabel32: TppLabel;
    ppLabel33: TppLabel;
    ppLabel34: TppLabel;
    ppDBText25: TppDBText;
    ppDBText26: TppDBText;
    ppDBText27: TppDBText;
    ppLine20: TppLine;
    ppLabel35: TppLabel;
    ppLabel37: TppLabel;
    ppDBText28: TppDBText;
    ppLabel38: TppLabel;
    ppImage2: TppImage;
    ppLabel39: TppLabel;
    ppLabel36: TppLabel;
    ppDetailBand12: TppDetailBand;
    ppSummaryBand10: TppSummaryBand;
    ppGroup3: TppGroup;
    ppGroupHeaderBand3: TppGroupHeaderBand;
    ppSubReport6: TppSubReport;
    ppChildReport12: TppChildReport;
    ppTitleBand11: TppTitleBand;
    ppShape11: TppShape;
    ppLabel43: TppLabel;
    ppLabel44: TppLabel;
    ppLine23: TppLine;
    ppDBText32: TppDBText;
    ppDetailBand13: TppDetailBand;
    ppDBText33: TppDBText;
    ppDBText34: TppDBText;
    ppSummaryBand11: TppSummaryBand;
    ppLine24: TppLine;
    ppLabel45: TppLabel;
    ppDBCalc9: TppDBCalc;
    ppSubReport7: TppSubReport;
    ppChildReport13: TppChildReport;
    ppTitleBand12: TppTitleBand;
    ppDetailBand14: TppDetailBand;
    ppDBText35: TppDBText;
    ppDBText36: TppDBText;
    ppSummaryBand12: TppSummaryBand;
    ppGroup4: TppGroup;
    ppGroupHeaderBand4: TppGroupHeaderBand;
    ppShape12: TppShape;
    ppLabel46: TppLabel;
    ppDBText37: TppDBText;
    ppLine25: TppLine;
    ppLabel47: TppLabel;
    ppGroupFooterBand4: TppGroupFooterBand;
    ppLine26: TppLine;
    ppLabel48: TppLabel;
    ppDBCalc10: TppDBCalc;
    ppGroupFooterBand3: TppGroupFooterBand;
    ppSubReport8: TppSubReport;
    ppChildReport14: TppChildReport;
    ppTitleBand13: TppTitleBand;
    ppShape10: TppShape;
    ppLabel49: TppLabel;
    ppLabel50: TppLabel;
    ppLine27: TppLine;
    ppDBText38: TppDBText;
    ppDetailBand15: TppDetailBand;
    ppDBText39: TppDBText;
    ppDBText40: TppDBText;
    ppSummaryBand13: TppSummaryBand;
    ppLine28: TppLine;
    ppLabel51: TppLabel;
    ppDBCalc11: TppDBCalc;
    raCodeModule2: TraCodeModule;
    ppSubReport9: TppSubReport;
    ppChildReport15: TppChildReport;
    ppTitleBand14: TppTitleBand;
    ppShape9: TppShape;
    ppLabel52: TppLabel;
    ppDBText41: TppDBText;
    ppLine29: TppLine;
    ppLine30: TppLine;
    ppDetailBand16: TppDetailBand;
    ppSummaryBand14: TppSummaryBand;
    ppSubReport10: TppSubReport;
    ppChildReport16: TppChildReport;
    ppTitleBand15: TppTitleBand;
    ppShape7: TppShape;
    ppLabel53: TppLabel;
    ppLine31: TppLine;
    ppDetailBand17: TppDetailBand;
    ppDBText42: TppDBText;
    ppDBText43: TppDBText;
    ppSummaryBand15: TppSummaryBand;
    ppLine32: TppLine;
    ppLabel54: TppLabel;
    ppDBCalc12: TppDBCalc;
    ppSubReport11: TppSubReport;
    ppChildReport17: TppChildReport;
    ppTitleBand16: TppTitleBand;
    ppShape6: TppShape;
    ppLabel55: TppLabel;
    ppLine33: TppLine;
    ppDetailBand18: TppDetailBand;
    ppDBText44: TppDBText;
    ppDBText45: TppDBText;
    ppSummaryBand16: TppSummaryBand;
    ppLine34: TppLine;
    ppLabel56: TppLabel;
    ppDBCalc13: TppDBCalc;
    pplblSaldoDiferencias: TppLabel;
    ppLineSaldoDiferencias1: TppLine;
    srCalculadoSaldoFinal: TppSubReport;
    ppChildReport18: TppChildReport;
    ppTitleBand17: TppTitleBand;
    ppShape5: TppShape;
    ppLabel58: TppLabel;
    ppLine36: TppLine;
    ppLabel59: TppLabel;
    ppDetailBand19: TppDetailBand;
    ppDBText46: TppDBText;
    ppDBText47: TppDBText;
    ppSummaryBand17: TppSummaryBand;
    ppLine37: TppLine;
    ppLabel60: TppLabel;
    ppDBCalc14: TppDBCalc;
    srSaldoDiferencia: TppSubReport;
    ppChildReport19: TppChildReport;
    ppTitleBand18: TppTitleBand;
    ppShape8: TppShape;
    ppLabel61: TppLabel;
    ppLine38: TppLine;
    ppLabel62: TppLabel;
    ppDetailBand20: TppDetailBand;
    ppDBText48: TppDBText;
    ppDBText49: TppDBText;
    ppSummaryBand18: TppSummaryBand;
    ppLine39: TppLine;
    ppLabel63: TppLabel;
    ppDBCalc15: TppDBCalc;
    ppLineSaldoDiferencias: TppLine;
    raCodeModule6: TraCodeModule;
    ppShape26: TppShape;
    ppLabel128: TppLabel;
    ppDBCalc4: TppDBCalc;
    ppSummaryBand3: TppSummaryBand;
    ppDBCalc6: TppDBCalc;
    ppShape17: TppShape;
    ppLabel10: TppLabel;
    spObtenerComprobantesDVPorTurno: TADOStoredProc;
    spObtenerMaestroConcesionaria: TADOStoredProc;
    procedure ppReporteBeforePrint(Sender: TObject);
  private
    FCodigoPuntoEntrega: integer;
    FCodigoPuntoVenta: integer;
    FUsuario: string;
    FNumeroTurno: longint;
  public
    function Inicializar (CodigoPuntoEntrega: integer; CodigoPuntoVenta: integer; Usuario: string;
    	NumeroTurno: longint; bHabilitarTags, bHabilitarEfectivo : boolean): Boolean;
    function Ejecutar:Boolean;
  end;



var
  FormRptCierreTurno: TFormRptCierreTurno;

implementation

{$R *.dfm}

function TFormRptCierreTurno.Inicializar(CodigoPuntoEntrega: integer; CodigoPuntoVenta: integer;
		Usuario: string; NumeroTurno: longint; bHabilitarTags, bHabilitarEfectivo : boolean): Boolean;
begin
    FCodigoPuntoEntrega := CodigoPuntoEntrega;
    FCodigoPuntoVenta := CodigoPuntoVenta;
    FUsuario := Usuario;
    FNumeroTurno := NumeroTurno;
	ppLiquidacionTAGsReport.Visible 	:= bHabilitarTags;
	ppLiquidacionEfectivoReport.Visible := bHabilitarEfectivo;
    result := true;
end;

function TFormRptCierreTurno.Ejecutar:Boolean;
var                                                                                                     //SS_1147_NDR_20140710
  RutaLogo, sDireccion: AnsiString;                                                          //SS_1147_NDR_20140710
begin
   ObtenerParametroGeneral(DMConnection.DMConnections.BaseCAC, LOGO_REPORTES, RutaLogo);               //SS_1147_NDR_20140710
   try                                                                                                 //SS_1147_NDR_20140710
     ppImage1.Picture.LoadFromFile(Trim(RutaLogo));
     ppImage1.Stretch := True;                                       //SS_1147_NDR_20140710
     ppImage2.Picture.LoadFromFile(Trim(RutaLogo));
     ppImage2.Stretch := True;                                                    //SS_1147_NDR_20140710
     ppImage3.Picture.LoadFromFile(Trim(RutaLogo));
     ppImage3.Stretch := True;                                                     //SS_1147_NDR_20140710

   except                                                                                              //SS_1147_NDR_20140710
     On E: Exception do begin                                                                          //SS_1147_NDR_20140710
       Screen.Cursor := crDefault;                                                                     //SS_1147_NDR_20140710
       MsgBox(E.message, Self.Caption, MB_ICONSTOP);                                                   //SS_1147_NDR_20140710
       Screen.Cursor := crHourGlass;                                                                   //SS_1147_NDR_20140710
     end;                                                                                              //SS_1147_NDR_20140710
   end;                                                                                                //SS_1147_NDR_20140710

   with spObtenerMaestroConcesionaria do                                                               //SS_1147_NDR_20140710
   begin                                                                                               //SS_1147_NDR_20140710
     Close;                                                                                            //SS_1147_NDR_20140710
     Parameters.Refresh;                                                                               //SS_1147_NDR_20140710
     Parameters.ParamByName('@CodigoConcesionaria').Value := ObtenerCodigoConcesionariaNativa;         //SS_1147_NDR_20140710
     Open;                                                                                             //SS_1147_NDR_20140710
     sDireccion    := Trim(FieldByName('DireccionConcesionaria').AsString) + ' ' +                     //SS_1147_NDR_20140710
                      Trim(FieldByName('Comuna').AsString);                                            //SS_1147_NDR_20140710
                                                                                                       //SS_1147_NDR_20140710
     ppLabel73.Caption := sDireccion;
     ppLabel72.Caption := '';               //JLO - 20160624                                           //SS_1147_NDR_20140710
     ppLabel39.Caption := sDireccion;                                                                  //SS_1147_NDR_20140710
   end;                                                                                                //SS_1147_NDR_20140710

	 rbiListado.Execute(True);
	 result := true;
end;

procedure TFormRptCierreTurno.ppReporteBeforePrint(Sender: TObject);
var
    TotalCobrosValesVista,
    TotalCobradoNotasCobroInfractor,
    TotalCobrosAnticipados,
    DiferenciaCobros, DiferenciaEfectivo,
    RefTotalEfectivo,
    RefTotalDA, // SS_637_PDO_20110503
    RefTotalChequeSTD,
    RefTotalChequeOtros,
    RefTotalValeVista,
//    RefTotalPagare,
    RefTotalTarjetaCredito,
    RefTotalTarjetaDebito: Int64;

    CantidadChequesSistema, CantidadChequesLiquidada,
    CantidadCuponesTarjetaDebitoSistema, CantidadCuponesTarjetaDebitoLiquidada,
    CantidadCuponesTarjetaCreditoSistema, CantidadCuponesTarjetaCreditoLiquidada,
    CantidadValesVistaSistema, CantidadValesVistaLiquidada,
    CantidadDASistema, CantidadDALiquidada, // SS_637_PDO_20110503
    CantidadPagaresSistema, CantidadPagaresLiquidada: SmallInt;

    ImporteChequesSistema, ImporteChequesLiquidada,
    ImporteTarjetasDebitoSistema, ImporteTarjetasDebitoLiquidada,
    ImporteTarjetasCreditoSistema, ImporteTarjetasCreditoLiquidada,
    ImporteDASistema, ImporteDALiquidada,   // SS_637_PDO_20110503
    ImporteValesVistaSistema, ImporteValesVistaLiquidada: Int64;
//    ImportePagaresSistema, ImportePagaresLiquidada: Int64;

    Ejecutando: String;

    nImporteComprobantesDVPorTurno : Int64;     //SS_959_A_MBE_20131008
    
    // Temporal a revisar
    lbl_CobrosChequeSTDDia,
    lbl_CobrosChequeSTDDiaRef,
    lbl_CobrosChequeOtrosDia,
    lblCobrosEfectivo1Ref,
    lblCobrosValesVista,
    lbl_CobrosTarjetaCredito,
    lblCobrosDA,
    lbl_CobrosTarjetaDebito,
    lblCobrosDARef,
    lbl_CobrosTarjetaCreditoRef,
    lbl_CobrosTarjetaDebitoRef,
    lblCobrosValesVistaRef,
    lbl_CobrosChequeOtrosDiaRef,
    lblCobrosEfectivo1,
    lblDiferenciaCobros: String;

begin
    try
        CantidadChequesSistema                 := 0;
        CantidadChequesLiquidada               := 0;
        CantidadCuponesTarjetaDebitoSistema    := 0;
        CantidadCuponesTarjetaDebitoLiquidada  := 0;
        CantidadCuponesTarjetaCreditoSistema   := 0;
        CantidadCuponesTarjetaCreditoLiquidada := 0;
        CantidadValesVistaSistema              := 0;
        CantidadValesVistaLiquidada            := 0;
        CantidadPagaresSistema                 := 0;
        CantidadPagaresLiquidada               := 0;
        CantidadDASistema                      := 0;    // SS_637_PDO_20110503
        CantidadDALiquidada                    := 0;    // SS_637_PDO_20110503

        ImporteChequesSistema                  := 0;
        ImporteChequesLiquidada                := 0;
        ImporteTarjetasDebitoSistema           := 0;
        ImporteTarjetasDebitoLiquidada         := 0;
        ImporteTarjetasCreditoSistema          := 0;
        ImporteTarjetasCreditoLiquidada        := 0;
        ImporteValesVistaSistema               := 0;
        ImporteValesVistaLiquidada             := 0;
        ImporteDASistema                       := 0;    // SS_637_PDO_20110503
        ImporteDALiquidada                     := 0;    // SS_637_PDO_20110503
//        ImportePagaresSistema                  := 0;
//        ImportePagaresLiquidada                := 0;

        with spObtenerDetalleTurno do begin
            Ejecutando := ProcedureName;
            if Active then Close;
            Parameters.Refresh;

            Parameters.ParamByName('@NumeroTurno').Value	:= FNumeroTurno;
            Open;

            CantidadChequesLiquidada               := FieldByName('CantidadCheques').AsInteger;
            CantidadCuponesTarjetaDebitoLiquidada  := FieldByName('CantidadCuponesTarjetaDebito').AsInteger;
            CantidadCuponesTarjetaCreditoLiquidada := FieldByName('CantidadCuponesTarjetaCredito').AsInteger;
            CantidadValesVistaLiquidada            := FieldByName('CantidadValesVista').AsInteger;
            CantidadPagaresLiquidada               := FieldByName('CantidadPagares').AsInteger;
            CantidadDALiquidada                    := FieldByName('CantidadDepositosAnticipados').AsInteger;    // SS_637_PDO_20110503

            ImporteChequesLiquidada                := FieldByName('ImporteCheques').AsInteger;
            ImporteTarjetasDebitoLiquidada         := FieldByName('ImporteTarjetasDebito').AsInteger;
            ImporteTarjetasCreditoLiquidada        := FieldByName('ImporteTarjetasCredito').AsInteger;
            ImporteValesVistaLiquidada             := FieldByName('ImporteValesVista').AsInteger;
            ImporteDALiquidada                     := FieldByName('ImporteDepositosAnticipados').AsInteger;     // SS_637_PDO_20110503
//            ImportePagaresLiquidada                := FieldByName('ImportePagares').AsInteger;
        end;

        //----------------------------------------------
        // inicio bloque        SS_959_A_MBE_20131008
        //----------------------------------------------
        Ejecutando := spObtenerComprobantesDVPorTurno.ProcedureName;
        spObtenerComprobantesDVPorTurno.Close;
        spObtenerComprobantesDVPorTurno.Parameters.Refresh;
        spObtenerComprobantesDVPorTurno.Parameters.ParamByName('@UsuarioTurno').Value           := spObtenerDetalleTurno.FieldByName('CodigoUsuario').AsString;
        spObtenerComprobantesDVPorTurno.Parameters.ParamByName('@FechaHoraInicioTurno').Value   := spObtenerDetalleTurno.FieldByName('FechaHoraApertura').AsDateTime;
        spObtenerComprobantesDVPorTurno.Parameters.ParamByName('@FechaHoraFinTurno').Value      := spObtenerDetalleTurno.FieldByName('FechaHoraCierre').AsDateTime;
        spObtenerComprobantesDVPorTurno.Open;
        nImporteComprobantesDVPorTurno := 0;
        while not spObtenerComprobantesDVPorTurno.Eof do begin
            nImporteComprobantesDVPorTurno := nImporteComprobantesDVPorTurno + spObtenerComprobantesDVPorTurno.FieldByName('TotalComprobante').AsInteger;
            spObtenerComprobantesDVPorTurno.Next;
        end;

        spObtenerComprobantesDVPorTurno.Close;
        nImporteComprobantesDVPorTurno := nImporteComprobantesDVPorTurno div 100;

        //----------------------------------------------
        // fin bloque        SS_959_A_MBE_20131008
        //----------------------------------------------

        Ejecutando := ObtenerSaldoFinalInformadoTurno.ProcedureName;
        ObtenerSaldoFinalInformadoTurno.Close;
        ObtenerSaldoFinalInformadoTurno.Parameters.Refresh;
        ObtenerSaldoFinalInformadoTurno.Parameters.ParamByName('@CodigoPuntoEntrega').Value := FCodigoPuntoEntrega;
        ObtenerSaldoFinalInformadoTurno.Parameters.ParamByName('@CodigoPuntoVenta').Value := FCodigoPuntoVenta;
        ObtenerSaldoFinalInformadoTurno.Parameters.ParamByName('@Usuario').Value := FUsuario;
        ObtenerSaldoFinalInformadoTurno.Parameters.ParamByName('@NumeroTurno').Value := FNumeroTurno;
        ObtenerSaldoFinalInformadoTurno.Open;
    //    showMessage('Final Informado: ' + intToStr(ObtenerSaldoFinalInformadoTurno.recordCount));

        Ejecutando := ObtenerSaldoInicialTurno.ProcedureName;
        ObtenerSaldoInicialTurno.Close;
        ObtenerSaldoInicialTurno.Parameters.Refresh;
        ObtenerSaldoInicialTurno.Parameters.ParamByName('@CodigoPuntoEntrega').Value := FCodigoPuntoEntrega;
        ObtenerSaldoInicialTurno.Parameters.ParamByName('@CodigoPuntoVenta').Value := FCodigoPuntoVenta;
        ObtenerSaldoInicialTurno.Parameters.ParamByName('@Usuario').Value := FUsuario;
        ObtenerSaldoInicialTurno.Parameters.ParamByName('@NumeroTurno').Value := FNumeroTurno;
        ObtenerSaldoInicialTurno.Open;
    //    showMessage('Inicial : ' + intToStr(ObtenerSaldoFinalInformadoTurno.recordCount));

        Ejecutando := ObtenerSaldosAdicionales.ProcedureName;
        ObtenerSaldosAdicionales.close;
        ObtenerSaldosAdicionales.Parameters.Refresh;
        ObtenerSaldosAdicionales.Parameters.ParamByName('@CodigoPuntoEntrega').Value := FCodigoPuntoEntrega;
        ObtenerSaldosAdicionales.Parameters.ParamByName('@CodigoPuntoVenta').Value := FCodigoPuntoVenta;
        ObtenerSaldosAdicionales.Parameters.ParamByName('@Usuario').Value := FUsuario;
        ObtenerSaldosAdicionales.Parameters.ParamByName('@NumeroTurno').Value := FNumeroTurno;
        ObtenerSaldosAdicionales.Open;
    //    showMessage('Adicionales: ' + intToStr(ObtenerSaldosAdicionales.recordCount));

        Ejecutando := ObtenerSaldoEntregadoTurno.ProcedureName;
        ObtenerSaldoEntregadoTurno.Close;
        ObtenerSaldoEntregadoTurno.Parameters.Refresh;
        ObtenerSaldoEntregadoTurno.Parameters.ParamByName('@CodigoPuntoEntrega').Value := FCodigoPuntoEntrega;
        ObtenerSaldoEntregadoTurno.Parameters.ParamByName('@CodigoPuntoVenta').Value := FCodigoPuntoVenta;
        ObtenerSaldoEntregadoTurno.Parameters.ParamByName('@Usuario').Value := FUsuario;
        ObtenerSaldoEntregadoTurno.Parameters.ParamByName('@NumeroTurno').Value := FNumeroTurno;
        ObtenerSaldoEntregadoTurno.Open;
    //    showMessage('Entregado: ' + intToStr(ObtenerSaldoFinalInformadoTurno.recordCount));

        Ejecutando := ObtenerSaldoFinalCalculadoTurno.ProcedureName;
        ObtenerSaldoFinalCalculadoTurno.Close;
        ObtenerSaldoFinalCalculadoTurno.Parameters.Refresh;
        ObtenerSaldoFinalCalculadoTurno.Parameters.ParamByName('@CodigoPuntoEntrega').Value := FCodigoPuntoEntrega;
        ObtenerSaldoFinalCalculadoTurno.Parameters.ParamByName('@CodigoPuntoVenta').Value := FCodigoPuntoVenta;
        ObtenerSaldoFinalCalculadoTurno.Parameters.ParamByName('@Usuario').Value := FUsuario;
        ObtenerSaldoFinalCalculadoTurno.Parameters.ParamByName('@NumeroTurno').Value := FNumeroTurno;
        ObtenerSaldoFinalCalculadoTurno.Open;
    //    showMessage('Final Calculado: ' + intToStr(ObtenerSaldoFinalInformadoTurno.recordCount));

        Ejecutando := ObtenerSaldoDiferenciaTurno.ProcedureName;
        ObtenerSaldoDiferenciaTurno.Close;
        ObtenerSaldoDiferenciaTurno.Parameters.Refresh;
        ObtenerSaldoDiferenciaTurno.Parameters.ParamByName('@CodigoPuntoEntrega').Value := FCodigoPuntoEntrega;
        ObtenerSaldoDiferenciaTurno.Parameters.ParamByName('@CodigoPuntoVenta').Value := FCodigoPuntoVenta;
        ObtenerSaldoDiferenciaTurno.Parameters.ParamByName('@Usuario').Value := FUsuario;
        ObtenerSaldoDiferenciaTurno.Parameters.ParamByName('@NumeroTurno').Value := FNumeroTurno;
        ObtenerSaldoDiferenciaTurno.Open;
     //   showMessage('Diferencia: ' + intToStr(ObtenerSaldoFinalInformadoTurno.recordCount));

        Ejecutando := ObtenerTotalesConveniosTurno.ProcedureName;
        ObtenerTotalesConveniosTurno.Close;
        ObtenerTotalesConveniosTurno.Parameters.Refresh;
        ObtenerTotalesConveniosTurno.Parameters.ParamByName('@CodigoPuntoEntrega').Value := FCodigoPuntoEntrega;
        ObtenerTotalesConveniosTurno.Parameters.ParamByName('@CodigoPuntoVenta').Value := FCodigoPuntoVenta;
        ObtenerTotalesConveniosTurno.Parameters.ParamByName('@Usuario').Value := FUsuario;
        ObtenerTotalesConveniosTurno.Parameters.ParamByName('@NumeroTurno').Value := FNumeroTurno;
        ObtenerTotalesConveniosTurno.Open;
    //    showMessage('Cantidad convenios: ' + intToStr(ObtenerSaldoFinalInformadoTurno.recordCount));

        Ejecutando := ObtenerEncabezadoCierreTurno.ProcedureName;
        ObtenerEncabezadoCierreTurno.Close;
        ObtenerEncabezadoCierreTurno.Parameters.Refresh;
        ObtenerEncabezadoCierreTurno.Parameters.ParamByName('@CodigoPuntoEntrega').Value := FCodigoPuntoEntrega;
        ObtenerEncabezadoCierreTurno.Parameters.ParamByName('@CodigoPuntoVenta').Value := FCodigoPuntoVenta;
        ObtenerEncabezadoCierreTurno.Parameters.ParamByName('@Usuario').Value := FUsuario;
        ObtenerEncabezadoCierreTurno.Parameters.ParamByName('@NumeroTurno').Value := FNumeroTurno;
        ObtenerEncabezadoCierreTurno.Open;
    //    showMessage('TV por Convenios: ' + intToStr(ObtenerSaldoFinalInformadoTurno.recordCount));

        Ejecutando := ObtenerSaldoDevueltoTurno.ProcedureName;
        ObtenerSaldoDevueltoTurno.Close;
        ObtenerSaldoDevueltoTurno.Parameters.Refresh;
        ObtenerSaldoDevueltoTurno.Parameters.ParamByName('@CodigoPuntoEntrega').Value := FCodigoPuntoEntrega;
        ObtenerSaldoDevueltoTurno.Parameters.ParamByName('@CodigoPuntoVenta').Value := FCodigoPuntoVenta;
        ObtenerSaldoDevueltoTurno.Parameters.ParamByName('@Usuario').Value := FUsuario;
        ObtenerSaldoDevueltoTurno.Parameters.ParamByName('@NumeroTurno').Value := FNumeroTurno;
        ObtenerSaldoDevueltoTurno.Open;

        Ejecutando := spObtenerLiquidacionTurnoEfectivo.ProcedureName;
        spObtenerLiquidacionTurnoEfectivo.Close;
        spObtenerLiquidacionTurnoEfectivo.Parameters.ParamByName('@NumeroTurno').Value	:= FNumeroTurno;
        spObtenerLiquidacionTurnoEfectivo.Open;
        {
        spObtenerLiquidacionTurnoCupones.Close;                                                           // SS_637_PDO_20110503
        spObtenerLiquidacionTurnoCupones.Parameters.ParamByName('@NumeroTurno').Value	:= FNumeroTurno;  // SS_637_PDO_20110503
        spObtenerLiquidacionTurnoCupones.Open;                                                            // SS_637_PDO_20110503
        }

        with spObtenerLiquidacionPorCanal do begin
            Ejecutando := ProcedureName;
            if Active then Close;
            //Parameters.Refresh;

            Parameters.ParamByName('@NumeroTurno').Value	    := FNumeroTurno;
            Parameters.ParamByName('@PorRefinanciacion').Value	:= 0;
            Parameters.ParamByName('@DesgloseCheques').Value	:= 0;
            Open;
        end;

        with spObtenerLiquidacionPorCanalDetalle do begin
            Ejecutando := ProcedureName;
            if Active then Close;
            //Parameters.Refresh;

            Parameters.ParamByName('@NumeroTurno').Value	    := FNumeroTurno;
            Parameters.ParamByName('@PorRefinanciacion').Value	:= 0;
            Parameters.ParamByName('@DesgloseCheques').Value	:= 1;
            Open;
        end;

        with spObtenerLiquidacionPorCanalRefinan do begin
            Ejecutando := ProcedureName;
            if Active then Close;
            //Parameters.Refresh;

            Parameters.ParamByName('@NumeroTurno').Value	    := FNumeroTurno;
            Parameters.ParamByName('@PorRefinanciacion').Value	:= 1;
            Parameters.ParamByName('@DesgloseCheques').Value	:= 0;
            Open;
        end;

        Ejecutando := sp_ObtenerTotalCobradoNotasCobroTurno.ProcedureName;
        // Obtener el Total Cobrado de Notas de Cobro.
        sp_ObtenerTotalCobradoNotasCobroTurno.Close;
        sp_ObtenerTotalCobradoNotasCobroTurno.Parameters.ParamByName('@NumeroTurno').Value := FNumeroTurno;
        sp_ObtenerTotalCobradoNotasCobroTurno.ExecProc;
        lbl_TotalCobradoNotasCobro.Caption := sp_ObtenerTotalCobradoNotasCobroTurno.Parameters.ParamByName('@TotalCobradoNotasCobro').Value;
        sp_ObtenerTotalCobradoNotasCobroTurno.Close;

        Ejecutando := sp_ObtenerTotalCobradoBoletasTurno.ProcedureName;
        // Obtener el Total Cobrado de Boletas.
        sp_ObtenerTotalCobradoBoletasTurno.Close;
        sp_ObtenerTotalCobradoBoletasTurno.Parameters.ParamByName('@NumeroTurno').Value := FNumeroTurno;
        sp_ObtenerTotalCobradoBoletasTurno.ExecProc;
        lbl_TotalCobradoBoletas.Caption := sp_ObtenerTotalCobradoBoletasTurno.Parameters.ParamByName('@TotalCobradoBoletas').Value;
        sp_ObtenerTotalCobradoBoletasTurno.Close;

        // Revision 3
        // Obtener el Total Cobrado de Cuotas Arriendo.
        Ejecutando := sp_ObtenerTotalCobradoCuotasArriendoTurno.ProcedureName;
        sp_ObtenerTotalCobradoCuotasArriendoTurno.Close;
        sp_ObtenerTotalCobradoCuotasArriendoTurno.Parameters.ParamByName('@NumeroTurno').Value := FNumeroTurno;
        sp_ObtenerTotalCobradoCuotasArriendoTurno.ExecProc;
        lbl_TotalCobradoCuotas.Caption := sp_ObtenerTotalCobradoCuotasArriendoTurno.Parameters.ParamByName('@TotalCobradoCuotasArriendo').Value;
        sp_ObtenerTotalCobradoCuotasArriendoTurno.Close;

        // Obtener el Total Pagado de Notas de Cr�dito.
        Ejecutando := sp_ObtenerTotalNotasCreditoPagadasTurno.ProcedureName;
        sp_ObtenerTotalNotasCreditoPagadasTurno.Close;
        sp_ObtenerTotalNotasCreditoPagadasTurno.Parameters.ParamByName('@NumeroTurno').Value := FNumeroTurno;
        sp_ObtenerTotalNotasCreditoPagadasTurno.ExecProc;
        lbl_TotalPagadoNotasCredito.Caption := sp_ObtenerTotalNotasCreditoPagadasTurno.Parameters.ParamByName('@TotalNotasCredito').Value;
        sp_ObtenerTotalNotasCreditoPagadasTurno.Close;

        // Obtener el Total de Cobros con Cheque del Turno Santander Dia.
        with spObtenerTotalCobradoChequesTurnoDia do begin
            Ejecutando := ProcedureName;
            if Active then Close;
            Parameters.Refresh;

            Parameters.ParamByName('@NumeroTurno').Value         := FNumeroTurno;
            Parameters.ParamByName('@ChequesSantander').Value    := 1;
            Parameters.ParamByName('@TotalCobradoCheques').Value := 0;
            ExecProc;

            lbl_CobrosChequeSTDDia := Parameters.ParamByName('@TotalCobradoCheques').Value;
        end;

        // Obtener el Total de Cobros con Cheque del Turno Otros Dia.
        with spObtenerTotalCobradoChequesTurnoDia do begin
            Ejecutando := ProcedureName;
            if Active then Close;
            Parameters.Refresh;

            Parameters.ParamByName('@NumeroTurno').Value         := FNumeroTurno;
            Parameters.ParamByName('@ChequesSantander').Value    := 0;
            Parameters.ParamByName('@TotalCobradoCheques').Value := 0;
            ExecProc;

            lbl_CobrosChequeOtrosDia := Parameters.ParamByName('@TotalCobradoCheques').Value;
        end;

        // Obtener el Total de Cobros con Vales Vista del Turno.
        Ejecutando := spObtenerTotalCobradoValesVistaTurno.ProcedureName;
        spObtenerTotalCobradoValesVistaTurno.Close;
        spObtenerTotalCobradoValesVistaTurno.Parameters.ParamByName('@NumeroTurno').Value := FNumeroTurno;
        spObtenerTotalCobradoValesVistaTurno.ExecProc;
        TotalCobrosValesVista := spObtenerTotalCobradoValesVistaTurno.Parameters.ParamByName('@TotalCobradoValesVista').Value;
        lblCobrosValesVista := spObtenerTotalCobradoValesVistaTurno.Parameters.ParamByName('@DescriTotalCobradoValesVista').Value;

        // Obtener el Total de Cobros con Tarjeta de Cr�dito del Turno.
        Ejecutando := sp_ObtenerTotalCobradoTarjetaCreditoTurno.ProcedureName;
        sp_ObtenerTotalCobradoTarjetaCreditoTurno.Close;
        sp_ObtenerTotalCobradoTarjetaCreditoTurno.Parameters.ParamByName('@NumeroTurno').Value := FNumeroTurno;
        sp_ObtenerTotalCobradoTarjetaCreditoTurno.ExecProc;
        lbl_CobrosTarjetaCredito := sp_ObtenerTotalCobradoTarjetaCreditoTurno.Parameters.ParamByName('@TotalCobradoTarjetaCredito').Value;

        // Obtener el Total de Cobros con Dep�sito Anticipado del Turno.                                                                        // SS_637_PDO_20110503
        Ejecutando := spObtenerTotalCobradoDepositoAnticipadoTurno.ProcedureName;                                                               // SS_637_PDO_20110503
        spObtenerTotalCobradoDepositoAnticipadoTurno.Close;                                                                                     // SS_637_PDO_20110503
        spObtenerTotalCobradoDepositoAnticipadoTurno.Parameters.ParamByName('@NumeroTurno').Value := FNumeroTurno;                              // SS_637_PDO_20110503
        spObtenerTotalCobradoDepositoAnticipadoTurno.ExecProc;                                                                                  // SS_637_PDO_20110503
        lblCobrosDA := spObtenerTotalCobradoDepositoAnticipadoTurno.Parameters.ParamByName('@TotalCobradoDepositoAnticipado').Value;    // SS_637_PDO_20110503

        // Obtener el Total de Cobros con Tarjeta de D�bito del Turno.
        Ejecutando := sp_ObtenerTotalCobradoTarjetaDebitoTurno.ProcedureName;
        sp_ObtenerTotalCobradoTarjetaDebitoTurno.Close;
        sp_ObtenerTotalCobradoTarjetaDebitoTurno.Parameters.ParamByName('@NumeroTurno').Value := FNumeroTurno;
        sp_ObtenerTotalCobradoTarjetaDebitoTurno.ExecProc;
        lbl_CobrosTarjetaDebito := sp_ObtenerTotalCobradoTarjetaDebitoTurno.Parameters.ParamByName('@TotalCobradoTarjetaDebito').Value;

        // Obtener el Total de Cobros de Notas de Cobro a Infractores
        Ejecutando := spObtenerTotalCobradoNotasCobroInfractorTurno.ProcedureName;
        spObtenerTotalCobradoNotasCobroInfractorTurno.Close;
        spObtenerTotalCobradoNotasCobroInfractorTurno.Parameters.ParamByName('@NumeroTurno').Value := FNumeroTurno;
        spObtenerTotalCobradoNotasCobroInfractorTurno.ExecProc;
        TotalCobradoNotasCobroInfractor := spObtenerTotalCobradoNotasCobroInfractorTurno.Parameters.ParamByName('@TotalCobradoNotasCobroInfractor').Value;
        lblTotalCobradoNotasCobroInfractor.Caption := spObtenerTotalCobradoNotasCobroInfractorTurno.Parameters.ParamByName('@DescriTotalCobradoNotasCobroInfractor').Value;

        // Obtener el Total de Cobros Anticipados
        Ejecutando := spObtenerTotalCobradoSinComprobante.ProcedureName;
        spObtenerTotalCobradoSinComprobante.Close;
        spObtenerTotalCobradoSinComprobante.Parameters.ParamByName('@NumeroTurno').Value := FNumeroTurno;
        spObtenerTotalCobradoSinComprobante.ExecProc;
        TotalCobrosAnticipados := spObtenerTotalCobradoSinComprobante.Parameters.ParamByName('@TotalCobradoSinComprobante').Value;
        lblCobrosAnticipados.Caption := spObtenerTotalCobradoSinComprobante.Parameters.ParamByName('@DescriTotalCobradoSinComprobante').Value;

        // Revision 4: Obtener el Total de Cobros con Pagar�
        {
        Ejecutando := sp_ObtenerTotalCobradoPagareTurno.ProcedureName;
        sp_ObtenerTotalCobradoPagareTurno.Close;
        sp_ObtenerTotalCobradoPagareTurno.Parameters.ParamByName('@NumeroTurno').Value := FNumeroTurno;
        sp_ObtenerTotalCobradoPagareTurno.ExecProc;
        lbl_CobrosPagare.Caption := sp_ObtenerTotalCobradoPagareTurno.Parameters.ParamByName('@TotalCobradoPagare').Value;
        }
        // Pago Cuotas Refinanciacion
        RefTotalEfectivo       := 0;
        RefTotalChequeSTD      := 0;
        RefTotalChequeOtros    := 0;
        RefTotalValeVista      := 0;
//        RefTotalPagare         := 0;
        RefTotalTarjetaCredito := 0;
        RefTotalTarjetaDebito  := 0;
        RefTotalDA             := 0;    // SS_637_PDO_20110503

        with spObtenerRefinanciacionTotalesPagosTurno do begin
            Ejecutando := ProcedureName;
            if Active then Close;
            Parameters.Refresh;

            Parameters.ParamByName('@NumeroTurno').Value := FNumeroTurno;
            Open;

            while not Eof do begin
                if FieldByName('CodigoFormaPago').AsInteger = FORMA_PAGO_EFECTIVO then begin
                    RefTotalEfectivo := RefTotalEfectivo + FieldByName('TotalFormaPago').AsInteger;
                end
                else if FieldByName('CodigoFormaPago').AsInteger = FORMA_PAGO_DEPOSITO_ANTICIPADO then begin    // SS_637_PDO_20110503
                    RefTotalDA       := RefTotalDA + FieldByName('TotalFormaPago').AsInteger;                   // SS_637_PDO_20110503
                    ImporteDASistema := ImporteDASistema + FieldByName('TotalFormaPago').AsInteger;             // SS_637_PDO_20110503
                end                                                                                             // SS_637_PDO_20110503
                {
                else if FieldByName('CodigoFormaPago').AsInteger = FORMA_PAGO_PAGARE then begin
                    RefTotalPagare := RefTotalPagare + FieldByName('TotalFormaPago').AsInteger;
                    ImportePagaresSistema := ImportePagaresSistema + FieldByName('TotalFormaPago').AsInteger;
                end
                }
                else if FieldByName('CodigoFormaPago').AsInteger = FORMA_PAGO_TARJETA_CREDITO then begin
                    RefTotalTarjetaCredito := RefTotalTarjetaCredito + FieldByName('TotalFormaPago').AsInteger;
                    ImporteTarjetasCreditoSistema := ImporteTarjetasCreditoSistema + FieldByName('TotalFormaPago').AsInteger;
                end
                else if FieldByName('CodigoFormaPago').AsInteger = FORMA_PAGO_TARJETA_DEBITO then begin
                    RefTotalTarjetaDebito := RefTotalTarjetaDebito + FieldByName('TotalFormaPago').AsInteger;
                    ImporteTarjetasDebitoSistema := ImporteTarjetasDebitoSistema + FieldByName('TotalFormaPago').AsInteger;
                end
                else if FieldByName('CodigoFormaPago').AsInteger = FORMA_PAGO_VALE_VISTA then begin
                    RefTotalValeVista := RefTotalValeVista + FieldByName('TotalFormaPago').AsInteger;
                    ImporteValesVistaSistema := ImporteValesVistaSistema + FieldByName('TotalFormaPago').AsInteger;
                end
                else if FieldByName('CodigoFormaPago').AsInteger = FORMA_PAGO_CHEQUE then begin
                    ImporteChequesSistema := ImporteChequesSistema + FieldByName('TotalFormaPago').AsInteger;

                    if FieldByName('CodigoBanco').AsInteger = CODIGO_BANCO_SANTANDER then begin
                        RefTotalChequeSTD := RefTotalChequeSTD + FieldByName('TotalFormaPago').AsInteger;
                    end
                    else RefTotalChequeOtros := RefTotalChequeOtros + FieldByName('TotalFormaPago').AsInteger;
                end;

                Next;
            end;

            lblCobrosEfectivo1Ref               := FormatFloat(FORMATO_IMPORTE, RefTotalEfectivo);
            lblCobrosDARef                      := FormatFloat(FORMATO_IMPORTE, RefTotalDA);    // SS_637_PDO_20110503
//            lbl_CobrosPagareRef.Caption         := FormatFloat(FORMATO_IMPORTE, RefTotalPagare);
            lbl_CobrosTarjetaCreditoRef         := FormatFloat(FORMATO_IMPORTE, RefTotalTarjetaCredito);
            lbl_CobrosTarjetaDebitoRef          := FormatFloat(FORMATO_IMPORTE, RefTotalTarjetaDebito);
            lblCobrosValesVistaRef      := FormatFloat(FORMATO_IMPORTE, RefTotalValeVista);
            lbl_CobrosChequeSTDDiaRef   := FormatFloat(FORMATO_IMPORTE, RefTotalChequeSTD);
            lbl_CobrosChequeOtrosDiaRef := FormatFloat(FORMATO_IMPORTE, RefTotalChequeOtros);
        end;

        // Revision 5: Obtener las Anulaciones de Pagos
        Ejecutando := spObtenerAnulacionesCierreTurno.ProcedureName;
        spObtenerAnulacionesCierreTurno.Close;
        spObtenerAnulacionesCierreTurno.Parameters.ParamByName('@NumeroTurno').Value := FNumeroTurno;
        spObtenerAnulacionesCierreTurno.Open;
        if (spObtenerAnulacionesCierreTurno.RecordCount > 0) then
            ppAnulaciones.Visible := True
        else
            ppAnulaciones.Visible := False;

        // Revision 6
        Ejecutando := spObtenerTotalCobradoEfectivoTurno.ProcedureName;
        spObtenerTotalCobradoEfectivoTurno.Close;
        spObtenerTotalCobradoEfectivoTurno.Parameters.ParamByName('@NumeroTurno').Value := FNumeroTurno;
        spObtenerTotalCobradoEfectivoTurno.ExecProc;

        lblCobrosEfectivo1 := spObtenerTotalCobradoEfectivoTurno.Parameters.ParamByName('@TotalCobradoEfectivo').Value;
        pplblEfectivoImporteSistema.Caption :=
            FormatFloat(FORMATO_IMPORTE,
                            ImporteStrToInt(DMConnections.BaseCAC, spObtenerTotalCobradoEfectivoTurno.Parameters.ParamByName('@TotalCobradoEfectivo').Value) +
                            RefTotalEfectivo - nImporteComprobantesDVPorTurno);             // SS_959_A_MBE_20131008
    {
        lblCobrosEfectivo2.Caption :=
            FormatFloat(FORMATO_IMPORTE,
                            ImporteStrToInt(DMConnections.BaseCAC, spObtenerTotalCobradoEfectivoTurno.Parameters.ParamByName('@TotalCobradoEfectivo').Value) +
                            RefTotalEfectivo);
    }
        Ejecutando := spObtenerTotalCobrosAnuladosTurno.ProcedureName;
        spObtenerTotalCobrosAnuladosTurno.Close;
        spObtenerTotalCobrosAnuladosTurno.Parameters.ParamByName('@NumeroTurno').Value := FNumeroTurno;
        spObtenerTotalCobrosAnuladosTurno.ExecProc;
        lblCobrosAnulados.Caption := spObtenerTotalCobrosAnuladosTurno.Parameters.ParamByName('@TotalCobrosAnuladosTurno').Value;

    //    pplblAperturaCaja.Caption   	:= spObtenerLiquidacionTurnoEfectivo.Parameters.ParamByName('@MontoAperturaCaja').Value;
        pplblEfectivoImporteApertura.Caption := spObtenerLiquidacionTurnoEfectivo.Parameters.ParamByName('@MontoAperturaCaja').Value;
    //    pplblCierreCaja.Caption     	:= spObtenerLiquidacionTurnoEfectivo.Parameters.ParamByName('@MontoCierreCaja').Value;
        pplblEfectivoImporteLiquidado.Caption := spObtenerLiquidacionTurnoEfectivo.Parameters.ParamByName('@MontoCierreCaja').Value;;
        Ejecutando := 'DiferenciaCobros';
        DiferenciaCobros :=
    //                        ImporteStrToInt(DMConnections.BaseCAC, lbl_TotalCobradoNotasCobro.Caption)
    //                        + ImporteStrToInt(DMConnections.BaseCAC, lbl_TotalCobradoBoletas.Caption)
    //                        + ImporteStrToInt(DMConnections.BaseCAC, lbl_TotalCobradoCuotas.Caption) // Revision 3
    //                       + Integer(TotalCobradoNotasCobroInfractor)
    //                        - ImporteStrToInt(DMConnections.BaseCAC, lbl_TotalPagadoNotasCredito.Caption)
    //                        + Integer(TotalCobrosAnticipados)

//                            Integer(TotalCobrosAnticipados) +
                            ImporteStrToInt(DMConnections.BaseCAC, lbl_CobrosChequeSTDDia) +
                            ImporteStrToInt(DMConnections.BaseCAC, lbl_CobrosChequeOtrosDia) +
    //                        - ImporteStrToInt(DMConnections.BaseCAC, lbl_CobrosChequeSTDFecha.Caption)
    //                        - ImporteStrToInt(DMConnections.BaseCAC, lbl_CobrosChequeOtrosFecha.Caption)
                            Integer(TotalCobrosValesVista) +
                            ImporteStrToInt(DMConnections.BaseCAC, lbl_CobrosTarjetaCredito) +
                            ImporteStrToInt(DMConnections.BaseCAC, lbl_CobrosTarjetaDebito) +
//                            ImporteStrToInt(DMConnections.BaseCAC, lbl_CobrosPagare.Caption) +
                            ImporteStrToInt(DMConnections.BaseCAC, lblCobrosEfectivo1) +
                            ImporteStrToInt(DMConnections.BaseCAC, lblCobrosDA) + // SS_637_PDO_20110503
                            RefTotalEfectivo +
                            RefTotalDA + // SS_637_PDO_20110503
                            RefTotalChequeSTD +
                            RefTotalChequeOtros +
                            RefTotalValeVista +
//                            RefTotalPagare +
                            RefTotalTarjetaCredito +
                            RefTotalTarjetaDebito;
                            { -                                                                     // SS_637_PDO_20110503
                            ImporteStrToInt(DMConnections.BaseCAC, lblCobrosAnulados.Caption);      // SS_637_PDO_20110503
                            }                                                                       // SS_637_PDO_20110503
        Ejecutando := 'ImporteChequesSistema';
        ImporteChequesSistema := ImporteChequesSistema +
                                 ImporteStrToInt(DMConnections.BaseCAC, lbl_CobrosChequeSTDDia) +
                                 ImporteStrToInt(DMConnections.BaseCAC, lbl_CobrosChequeOtrosDia);

        ImporteValesVistaSistema := ImporteValesVistaSistema + TotalCobrosValesVista;

        ImporteTarjetasCreditoSistema := ImporteTarjetasCreditoSistema + ImporteStrToInt(DMConnections.BaseCAC, lbl_CobrosTarjetaCredito);

        ImporteTarjetasDebitoSistema := ImporteTarjetasDebitoSistema + ImporteStrToInt(DMConnections.BaseCAC, lbl_CobrosTarjetaDebito);

        ImporteDASistema             := ImporteDASistema + ImporteStrToInt(DMConnections.BaseCAC, lblCobrosDA); // SS_637_PDO_20110503

//        ImportePagaresSistema := ImportePagaresSistema + ImporteStrToInt(DMConnections.BaseCAC, lbl_CobrosPagare.Caption);

        lblDiferenciaCobros	:= FormatearImporte(DMConnections.BaseCAC, DiferenciaCobros);
        Ejecutando := 'DiferenciaEfectivo';
        DiferenciaEfectivo := ImporteStrToInt(DMConnections.BaseCAC, pplblEfectivoImporteApertura.Caption)
                            + ImporteStrToInt(DMConnections.BaseCAC, pplblEfectivoImporteSistema.Caption)
                            - ImporteStrToInt(DMConnections.BaseCAC, pplblEfectivoImporteLiquidado.Caption);

    //    pplblDiferenciaefectivo.Caption	:= FormatearImporte(DMConnections.BaseCAC, DiferenciaEfectivo);
        Ejecutando := 'pplblEfectivoImporteDiferencia';
        pplblEfectivoImporteDiferencia.Caption := FormatearImporte(DMConnections.BaseCAC, DiferenciaEfectivo);

        // Diferencias entre Formas de Pago

        pplblChequesImporteSistema.Caption    := FormatFloat(FORMATO_IMPORTE, ImporteChequesSistema);
        pplblChequesImporteLiquidado.Caption  := FormatFloat(FORMATO_IMPORTE,ImporteChequesLiquidada );
        pplblChequesImporteDiferencia.Caption := FormatFloat('$ #,##0', ImporteChequesSistema - ImporteChequesLiquidada);

        pplblTarjetaCreditoImporteSistema.Caption    := FormatFloat(FORMATO_IMPORTE, ImporteTarjetasCreditoSistema);
        pplblTarjetaCreditoImporteLiquidado.Caption  := FormatFloat(FORMATO_IMPORTE, ImporteTarjetasCreditoLiquidada);
        pplblTarjetaCreditoImporteDiferencia.Caption := FormatFloat('$ #,##0', ImporteTarjetasCreditoSistema - ImporteTarjetasCreditoLiquidada);

        pplblTarjetaDebitoImporteSistema.Caption    := FormatFloat(FORMATO_IMPORTE, ImporteTarjetasDebitoSistema);
        pplblTarjetaDebitoImporteLiquidado.Caption  := FormatFloat(FORMATO_IMPORTE, ImporteTarjetasDebitoLiquidada);
        pplblTarjetaDebitoImporteDiferencia.Caption := FormatFloat('$ #,##0', ImporteTarjetasDebitoSistema - ImporteTarjetasDebitoLiquidada);

        pplblValeVistaImporteSistema.Caption    := FormatFloat(FORMATO_IMPORTE, ImporteValesVistaSistema);
        pplblValeVistaImporteLiquidado.Caption  := FormatFloat(FORMATO_IMPORTE, ImporteValesVistaLiquidada);
        pplblValeVistaImporteDiferencia.Caption := FormatFloat('$ #,##0', ImporteValesVistaSistema - ImporteValesVistaLiquidada);

//        pplblPagareImporteSistema.Caption    := FormatFloat(FORMATO_IMPORTE, ImportePagaresSistema);
//        pplblPagareImporteLiquidado.Caption  := FormatFloat(FORMATO_IMPORTE, ImportePagaresLiquidada);
//        pplblPagareImporteDiferencia.Caption := FormatFloat('$ #,##0', ImportePagaresSistema - ImportePagaresLiquidada);

        pplblDAImporteSistema.Caption    := FormatFloat(FORMATO_IMPORTE, ImporteDASistema);                 // SS_637_PDO_20110503
        pplblDAImporteLiquidado.Caption  := FormatFloat(FORMATO_IMPORTE,ImporteDALiquidada );               // SS_637_PDO_20110503
        pplblDAImporteDiferencia.Caption := FormatFloat('$ #,##0', ImporteDASistema - ImporteDALiquidada);  // SS_637_PDO_20110503


        with spObtenerRefinanciacionDiferenciasPiesNoLiquidados do begin
            Parameters.Refresh;
            Parameters.ParamByName('@NumeroTurno').Value := FNumeroTurno;
            Open;
        end;
    except
        on e: Exception do begin
            raise Exception.Create(Format('Ejecutando: %s. Error: %s', [Ejecutando, e.Message]));
        end;
    end;
end;

end.
