{-----------------------------------------------------------------------------
        frmReporteBoletaFacturaElectronica

	Author: mbecerra
    Date: 12-Marzo-2009
    Description:	(Ref. Factura Electr�nica)
                	Imprime la Boleta y/o Factura Electr�nica,
                    y tambi�n las notas de cobro antiguas.

                    Basado en el formulario ReporteFactura.


  Revision    : 2
  Fecha       : 28-04-2009
  Autor       : Nelson Droguett Sierra
  Descripcion : Se hace que el membrete del detalle sea concordante con el
                tipo de documento fiscal.
  Revision    : 3
  Fecha       : 27-05-2009
  Autor       : Nelson Droguett Sierra
  Descripcion : Se saca el mensaje "PERSONA QUE RECIBE" cuando es copia cedible.
                ademas se pone invisible el mensaje de copia fiel. (todo esto en el reporte)


  Revision    : 4
  Fecha       : 23-07-2009
  Autor       : Nelson Droguett Sierra
  Descripcion : Se modifica la posicion del codigo de barrar inferior para evitar que se
                pase a una segund hoja

  Revision    : 5
  Author      : pdominguez
  Date        : 16/11/2009
  Description : SS 845
        - Se modificaron/a�adieron los siguientes objetos
            Se puso la propiedad CloseDataSource a False para todos los
            objetos TppDBPipeline, ya que todos los objetos TADOStoredProc
            ya se cierran en el evento OnClose del Formulario.

        - Se modificaron/a�adieron los siguientes procedimientos/funciones:
            Inicializar
            FormClose
            Preparar
            rptReporteFacturaBeforePrint
            ppLblCopiaFielPrint

 Revision 6
 Author: mbecerra
 Date: 03-Febrero-2010
 Description:	Se agrega una funci�n que borre el archivo JPG generado para el
            	Timbre electr�nico.

                Se agrega la llamada a esta funci�n en la funci�n Ejecutar, para que
                borre el archivo autom�ticamente cuando imprime el Reporte.

                Se crea esta funci�n ya que los servicios de Mailer no usan la
                funci�n Ejecutar. De esta manera se puede invocar despu�s de
                generar el archivo PDF.

 Revision 7
 Author: jjofre
 Date: 25-Febrero-2010
 Description:	-Se elimina del procedimiento rptReporteFacturaBeforePrint el
                MsgBoxErr(MSG_ERROR_PREPARANDO,DescError,MSG_CAPTION_ERROR,MB_ICONSTOP);
                ya que este MSgBox provocaba el TimeOut al no encontrar CAF desde la web,
                ya que la funcion preparar()  retornaba error y seguidamente se emitia el MSgBox
                que desde Cobranza-facturacion-CAC funcionaba, pero en la WEB generaba un Time Out.

              - Se agrega un raise y se controla que
                muestre el MSGBOX en ComprobantesEmitidos.

              -Se modifica el raise de la funcion preparar() para
              que sea mas claro a la hora de devolver el mensaje cuando no
              logro obtener el timbre del comprobante.

Revision        : 4
Author          : Javier De Barbieri
Date            : 21-Diciembre-2010
Description     : Ref (943)
                            En la funci�n inicializa se agrega la consulta a la
                            funci�n de la base de datos que retorna si el comprobante
                            es facturado manualmente seg�n los mensajes asociados al
                            comprobante generado, para que en el caso que tenta PAC
                            o PAT no imprima el sello. El resultado de esa funci�n
                            es almacenado en una variable de tipo Boolean global.

Revision        : 5
Author          : Alejandro Labra
Date            : 07-Julio-2011
Description     : Se agrega Agregar a la ventana "Opciones de Impresion", unit
                  ImprimirWO, las siguientes validaciones:
                        - Que se pueda imprimir Factura Cedible solamente si:
                            - Estado de la factura se encuentre pagado
                            - Saldo de la factura sea 0.
Firma           : PAR00114-ALA-07072011

Firma           : SS_1015_HUR_20120418
Descripcion     : Se agrega el total de saldo inicial otras concesionarias al
                  estado de cuenta.

Firma           : SS-1015-NDR-20120510
Descripcion     : Se agrega el total de cuotas otras concesionarias al
                  estado de cuenta.

Firma           : SS_1045_CQU_20120604
Descripcion     : Se agregan propiedades que nos permitir�n conservar las opciones elegidas por el usuario
                  cuando se imprima de manera autom�tica desde el m�dulo fFacturacionInfracciones,
                  espec�fricamente desde la secci�n Facturaci�n por Rut de dicho formulario.


Firma        : SS_1147_NDR_20140710
Descripcion  : El logo de la concesionaria, debe salir de un archivo, segun parametrosgenerales. Lo mismo la linea con la direccion.

Firma           : SS_1182_CQU_20140424
Descripcion     : Se elimina el label lblTestLegth ya que s�lo era usado para dimensiones y ya no es util.
                  Se Redimensionan dbtNombre, dbtDir1 y dbtDir2. Tambi�n se les agrega
                  la propiedad WordWrap = True.


Firma           : SS_1307_NDR_20150804
Descripcion     : Se declara una variable publica para saber si la impresion se esta realizando desde el sistema,
                  desde la WEB, desde JORDAN o desde un servicio y segun eso cargar parametros diferenciados, por ejemplo para
                  la carpeta temporal.
                  LA funcion ReporteAPDF borra el archivo temporal creado, ya que ahora no necesariamente se crea en la carpeta
				  TEMP de Windows, por lo que debe eliminarlo para que no se acumulen

Firma           : SS_1332_CQU_20150721
Descripcion     : Se agrega un control al generar un comprobante en PDF,
                  ahora verifica que el archivo se haya creado en la carpeta temporal

 Firma          : SS_1332_CQU_20150909
 Descripcion    : Se agrega una funci�n para liberar la DTE,
                  es usada por el webservice OficinaVirtual (..\WEB\Comunes\ImpresionDocumentos.pas)

 Firma          : SS_1399_NDR_20151014
 Descripcion    : Si no se puede imprimir porque el campo FechaHoraTimbre es NULL  verifica si el comprobante es 
				anterior a la migracion de VS , si es asi avisar�  que no est� disponible para ser impreso, 
				si no es el caso avisar� con un error indicando que no tiene Fecha de timbraje electronico

 Firma          : SS_1397_MGO_20151127
 Descripcion    : Se a�ade logo, m�rgenes de columnas y leyendas correctas en el header

 Firma       : SS_1397_MCA_20160105
Descripcion : Se agrega par�metro LOGO_REPORTES_WS
 -----------------------------------------------------------------------------}
unit frmReporteBoletaFacturaElectronica;

interface

uses

  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ppBands, ppReport, ppSubRpt, ppCtrls,
  ppStrtch, ppMemo, ppParameter, ppBarCod, jpeg, ppPrnabl, ppClass, UtilRB,
  ppCache, ppComm, ppRelatv, ppProd, ppModule, raCodMod, DB,
  ADODB, ppDB, ppDBPipe,util,utilproc, ppVar, StdCtrls, daDataModule,
  ppRegion, ppCTMain, ppTxPipe, rbSetup, TeEngine, Series, ExtCtrls,
  TeeProcs, Chart, DbChart, TXComp, ppDevice, ConstParametrosGenerales, Printers, PeaTypes, UtilDb,
  ImprimirWO, TXRB, ppBarCode2D,
  DTEControlDLL, DateUtils, ppPDFDevice, StrUtils, ppZLib, ppPDFSettings,
  LMDPNGImage, GIFImg;

type
  TTipoPago = (tpNinguno, tpPAT, tpPAC);
  TReporteBoletaFacturaElectronicaForm = class(TForm)
    rbiFactura: TRBInterface;
    spComprobantes: TADOStoredProc;
    dsComprobantes: TDataSource;
    dsUltimosDoce: TDataSource;
    dsObtenerConsumos: TDataSource;
    spUltimosDoce: TADOStoredProc;
    ppDBConsumos: TppDBPipeline;
    dsMensaje: TDataSource;
    spMensaje: TADOStoredProc;
    ppMensaje: TppDBPipeline;
    ppUltimosDoce: TppDBPipeline;
    spObtenerConsumos: TADOStoredProc;
    rptReporteConsumo: TppReport;
    ppDetailBand5: TppDetailBand;
    ppDBText7: TppDBText;
    ppDBText10: TppDBText;
    ppDBText11: TppDBText;
    ppDBText12: TppDBText;
    ppDBText13: TppDBText;
    rbiConsumo: TRBInterface;
    ppHeaderBand2: TppHeaderBand;
    ppGroup2: TppGroup;
    ppGroupHeaderBand2: TppGroupHeaderBand;
    ppGroupFooterBand2: TppGroupFooterBand;
    ppLine7: TppLine;
    ppLabel2: TppLabel;
    ppLblCongestion17: TppLabel;
    ppLblPunta17: TppLabel;
    pplblNormal17: TppLabel;
    ppColumnHeaderBand1: TppColumnHeaderBand;
    ppColumnFooterBand1: TppColumnFooterBand;
    ppLabel14: TppLabel;
    ppLabel21: TppLabel;
    ppLabel22: TppLabel;
    ppLabel23: TppLabel;
    ppLabel24: TppLabel;
    ppLabel27: TppLabel;
    ppLine5: TppLine;
    ppLine8: TppLine;
    ppLbltotal17: TppLabel;
    ppDBText14: TppDBText;
    ppRegion2: TppRegion;
    ppShape10: TppShape;
    ppShape11: TppShape;
    ppShape12: TppShape;
    ppLabel1: TppLabel;
    ppShape13: TppShape;
    ppLabel3: TppLabel;
    ppShape14: TppShape;
    ppShape15: TppShape;
    ppShape16: TppShape;
    ppLine6: TppLine;
    ppLabel9: TppLabel;
    ppLabel11: TppLabel;
    ppDBText15: TppDBText;
    ppDBText16: TppDBText;
    ppDBText17: TppDBText;
    ppDBText18: TppDBText;
    ppDBText19: TppDBText;
    ppLabel19: TppLabel;
    ppShape19: TppShape;
    Label1: TLabel;
    Label2: TLabel;
    ppLine11: TppLine;
    ppLine12: TppLine;
    ppComprobantes: TppDBPipeline;
    ppReport1: TppReport;
    ppHeaderBand3: TppHeaderBand;
    ppDetailBand6: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppBarCode2: TppBarCode;
    ppBarCode1: TppBarCode;
    ppBarCode3: TppBarCode;
    ppBarCode4: TppBarCode;
    ppBarCode5: TppBarCode;
    ppBarCode6: TppBarCode;
    ppDB2DBarCode1: TppDB2DBarCode;
    ExtraOptions1: TExtraOptions;
    ppDBDetalleCuenta: TppDBPipeline;
    dsDetalleCuenta: TDataSource;
    spObtenerDetalleCuenta: TADOStoredProc;
    spObtenerDatosParaTimbreElectronico: TADOStoredProc;
    dbcUltimosDoce: TDBChart;
    Series1: TBarSeries;
    ppLabel17: TppLabel;
    ppDBText32: TppDBText;
    pplblKm17: TppLabel;
    rptReporteFactura: TppReport;
    ppParameterList4: TppParameterList;
    spObtenerMaestroConcesionaria: TADOStoredProc;
	ppLogo: TppImage;
    ppDetailBand7: TppDetailBand;
    ppsCuadroFactura: TppShape;
    ppTipoDocumentoElectronico: TppLabel;
    ppRutConcesionaria: TppLabel;
    ppNumeroDocumentoElectronico: TppLabel;
    ppLabel63: TppLabel;
    ppSubReport3: TppSubReport;
    ppChildReport3: TppChildReport;
    ppDetailBand4: TppDetailBand;
    ppDBText8: TppDBText;
    ppDBText9: TppDBText;
    raCodeModule4: TraCodeModule;
    ppSubReport1: TppSubReport;
    ppChildReport1: TppChildReport;
    ppTitleBand1: TppTitleBand;
    ppLabel25: TppLabel;
    ppLabel26: TppLabel;
    ppLabel28: TppLabel;
    ppLabel29: TppLabel;
    ppLabel30: TppLabel;
    ppDetailBand2: TppDetailBand;
    ppDBText1: TppDBText;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    raCodeModule1: TraCodeModule;
    ppTotalComprobante: TppDBText;
    imgGraficoUltimosDoce: TppImage;
    ppdtFechaEmision: TppDBText;
    ppDBText22: TppDBText;
    ppDBText25: TppDBText;
    ppdtPeriodoIni: TppDBText;
    ppLabel4: TppLabel;
    ppdtPeriodoFin: TppDBText;
    ppSubReport2: TppSubReport;
    ppChildReport2: TppChildReport;
    ppTitleBand2: TppTitleBand;
    ppDetailBand3: TppDetailBand;
    ppDBText2: TppDBText;
    ppLabel5: TppLabel;
    ppSummaryBand2: TppSummaryBand;
    raCodeModule2: TraCodeModule;
    ppDBGiroCliente: TppDBText;
    ppDBText31: TppDBText;
    ppLabel18: TppLabel;
    ppGiroCliente: TppLabel;
    ppLabel20: TppLabel;
    ppdbtNumConvenioFormat: TppDBText;
    ppdbtDir2: TppDBText;
    ppdbtDir1: TppDBText;
    ppdbtNombre: TppDBText;
    pplblTotal: TppLabel;
    ppDBText24: TppDBText;
    ppDBText29: TppDBText;
    imgTimbre: TppImage;
    ppResolucionSII: TppLabel;
    ppdbbcConvenio2: TppDBBarCode;
    ppDBText23: TppDBText;
    ppdbtConvenio2: TppDBText;
    ppdbtVencimiento2: TppDBText;
    ppLblCopiaFiel: TppLabel;
    raCodeModule3: TraCodeModule;
    ppSubReport4: TppSubReport;
    ppChildReport4: TppChildReport;
    ppDetailBand1: TppDetailBand;
    ppDBText20: TppDBText;
    ppDBText21: TppDBText;
    raCodeModule5: TraCodeModule;
    spFacturaEstadoCuenta: TADOStoredProc;
    dsFacturaEstadoCuenta: TDataSource;
    ppFacturaEstadoCuenta: TppDBPipeline;
    ImagePAT: TppImage;
    ImageServipag: TppImage;
    ImagePAC: TppImage;
    ppLabel8: TppLabel;
    ppLabel6: TppLabel;
    ppImageFondo: TppImage;
    plblCIdPagoServiPag: TppLabel;                                                           //SS_1397_MGO_20151127
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure rptReporteConsumoPreviewFormCreate(Sender: TObject);
    procedure rptReporteFactura_BeforePrint(Sender: TObject);
    procedure ppdbbcConvenio2Print(Sender: TObject);
    procedure ppLblCopiaFielPrint(Sender: TObject);
    procedure rbiFacturaExecute(Sender: TObject; var Cancelled: Boolean);
    procedure ppdbtVencimiento2Print(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ppDetailBand4BeforePrint(Sender: TObject);
   private
    { Private declarations }
    FTipoBoletaAfecta, FTipoFacturaAfecta,
    FTipoBoletaExenta, FTipoFacturaExenta,
    FTipoNotaCobro,
    FTipoComprobante,
    FTipoComprobanteFiscal,
    FGlosaResolucionSIIBoleta,
    FGlosaResolucionSIIFactura : String;
    FNumeroComprobanteFiscal,		// TASK_063_MGO_20160819
    FNumeroComprobante: Int64;
    FDirImagenPAT, FDirImagenPAC,
    FNombreImagenFondo,
    FNombreImagenFondoPAC,          // SS_1017_PDO_20120416
    FNombreImagenFondoPAT,          // SS_1017_PDO_20120416
    FDirImagenFondo : AnsiString;
//    FNombreImagenPAC,
//    FNombreImagenPAT: AnsiString;
    FConexion: TADOConnection;
    FTipoPago : TTipoPago;
    FImprimirCedible,
    FMostrarConsumoAparte: Boolean;
    FIncluirFondo: boolean;
    FIsPDF: Boolean;
    Linea: integer;
    FPreparado: boolean;
    FEsConfiguracion : boolean;
    FRutaImagenTimbreElectronico : string;
    FEstadoPago     : string;                                                   //PAR0114-ALA-07072011
    FSaldo          : Int64;                                                    //PAR0114-ALA-07072011
    // Inicio Bloque SS_1045_CQU_20120604
    FHecho, FOpcion1, FOpcion2 : Boolean;
    FRespuesta : TRespuesta;
    // Fin Bloque SS_1045_CQU_20120604

    function Preparar(var DescError: String): Boolean;
  public
    { Public declarations }
    FImprimioComprobante: Boolean;
    FOrigenImpresion:TOrigenImpresion;                                         			//SS_1307_NDR_20150804
    function Inicializar( Conexion: TADOConnection;
                          TipoComprobante: string;
                          NumeroComprobante: int64;
                          var Error: string;
                          IncluirFondo: Boolean = True;
                          EsWEB: boolean = false;                                      	//SS_1307_NDR_20150804
                          oiOrigenImpresion:TOrigenImpresion=oiSistema                 	//SS_1307_NDR_20150804
                         ): Boolean;
    function Ejecutar(ActivarOpciones: Boolean = False): Boolean;
    function EjecutarPDF(var Resultado: string; var Error: String):Boolean;
    function EjecutarDetallePDF(var Resultado: string; var Error: String):Boolean;
    function ExportarReporte(Reporte: TppReport; Dispositivo: TppPDFDevice; var Error: string): boolean;
    function ReporteAPDF(Reporte: TppReport; var Resultado: string; var Error: string): boolean;
    property MostrarConsumoAparte: boolean read FMostrarConsumoAparte;
    //Indicar que es s�lo configuraci�n de la impresora
    procedure ConfigurarDestino(EsConfigurar : boolean);

        //REV.6
        function EliminarImagenTimbreElectronico : boolean;
    // Inicio Bloque SS_1045_CQU_20120604
    property Hecho : Boolean read FHecho write FHecho;
    property Opcion1 : Boolean read FOpcion1 write FOpcion1;
    property Opcion2 : Boolean read FOpcion2 write FOpcion2;
    property Respuesta : TRespuesta read FRespuesta write FRespuesta;
    // Fin Bloque SS_1045_CQU_20120604

    function ObtenerDirectorioTemporal(oiOrigenImpresion:TOrigenImpresion):string;		//SS_1307_NDR_20150804
    Procedure LiberarDBNet();    // SS_1332_CQU_20150909
  end;

var
    ReporteBoletaFacturaElectronicaForm: TReporteBoletaFacturaElectronicaForm;

implementation

{$R *.dfm}


resourcestring
    ERROR_INIT                        = 'Error de Inicializaci�n';
    ERROR_PRINTING                    = 'Error de Impresi�n';
    ERROR_INIT_DESCRIPTION            = 'No Se Encuentra El Comprobante';

{-----------------------------------------------------------------------------
  				Inicializar
  Author: mbecerra
  Date: 26-Marzo-2009
  Description:	Inicializa y adquiere los datos para imprimir el comprobante

  Revision : 1
      Author : pdominguez
      Date   : 16/11/2009
      Description : SS 845
        - Se pasa la carga de la Librer�a de la facturaci�n electr�nica a la
        funci�n Preparar.
-----------------------------------------------------------------------------}
function TReporteBoletaFacturaElectronicaForm.Inicializar(Conexion: TADOConnection;
                                                          TipoComprobante: string;
                                                          NumeroComprobante: int64;
                                                          var Error: string;
                                                          IncluirFondo: Boolean = True;
                                                          EsWEB: boolean = false;                               //SS_1307_NDR_20150804
                                                          oiOrigenImpresion:TOrigenImpresion=oiSistema          //SS_1307_NDR_20150804
                                                         ): Boolean;
resourcestring
    MSG_MISSED_IMAGES_FILES	=	'No existen todas las im�genes requeridas para '
        						+ CRLF + 'poder imprimir una Nota de Cobro en el sistema. Imagen: %s';  // SS_1017_PDO_20120416
    MSG_SIN_GLOSA     = '<Sin Glosa>';

    MSG_ERROR	= 'Error';
    SQL_BA		= 'SELECT dbo.CONST_TIPO_COMPROBANTE_BOLETA_AFECTA()';
    SQL_BE		= 'SELECT dbo.CONST_TIPO_COMPROBANTE_BOLETA_EXENTA()';
    SQL_FA		= 'SELECT dbo.CONST_TIPO_COMPROBANTE_FACTURA_AFECTA()';
    SQL_FE		= 'SELECT dbo.CONST_TIPO_COMPROBANTE_FACTURA_EXENTA()';
    SQL_NK		= 'SELECT dbo.CONST_TIPO_COMPROBANTE_NOTA_COBRO()';
    SQL_FISCAL	= 	'SELECT TipoComprobanteFiscal, EstadoPago, NumeroComprobanteFiscal, saldo FROM Comprobantes (NOLOCK) ' +           // TASK_063_MGO_20160819      //PAR0114-ALA-07072011
    				'WHERE TipoComprobante = ''%s'' AND NumeroComprobante = %d';
var
  MensajeError      : string;
  aqryComprobante   : TADOQuery;                                                //PAR0114-SS-ALA-07072011
  RutaLogo          : AnsiString;                                               //SS_1397_MGO_20151127
  //  CodErrorDLL : integer; {Rev.1 SS (845)}
  sRazonSocial,sRut,sDireccion,sLocalidad: AnsiString;                                                //SS_1147_NDR_20140710
begin
    FOrigenImpresion:=oiOrigenImpresion;                                        //SS_1307_NDR_20150804
    try
        if not Assigned(Conexion) then raise Exception.Create('Se debe especificar una Conexi�n');
        if not Conexion.Connected then Conexion.Open;
        FTipoComprobante := TipoComprobante;
        FNumeroComprobante := NumeroComprobante;
        FConexion := Conexion;
        //Asignamos la conexion que nos indicaron usar
        spComprobantes.Connection			:= Conexion;
        spUltimosDoce.Connection			:= Conexion;
        spObtenerConsumos.Connection		:= Conexion;
        spMensaje.Connection				:= Conexion;
        spObtenerDetalleCuenta.Connection	:= Conexion;
        spObtenerDatosParaTimbreElectronico.Connection := Conexion;
        spObtenerMaestroConcesionaria.Connection	:= Conexion;                                    //SS_1147_NDR_20140710
        //INICIO:	20160809 CFU
        spFacturaEstadoCuenta.Connection	:= Conexion;
        //TERMINO:	20160809 CFU

        //Obtener los tipos de documentos electr�nicos
        FTipoBoletaAfecta	:= QueryGetValue(Conexion, SQL_BA);
        FTipoBoletaExenta	:= QueryGetValue(Conexion, SQL_BE);
        FTipoFacturaAfecta	:= QueryGetValue(Conexion, SQL_FA);
        FTipoFacturaExenta	:= QueryGetValue(Conexion, SQL_FE);
        FTipoNotaCobro		:= QueryGetValue(Conexion, SQL_NK);



        with spObtenerMaestroConcesionaria do                                                               //SS_1147_NDR_20140710
        begin                                                                                               //SS_1147_NDR_20140710
          Close;                                                                                            //SS_1147_NDR_20140710
          Parameters.Refresh;                                                                               //SS_1147_NDR_20140710
          Parameters.ParamByName('@CodigoConcesionaria').Value := QueryGetValueInt(Conexion, 'SELECT dbo.ObtenerConcesionariaNativa()');         //SS_1147_NDR_20140710
          Open;                                                                                             //SS_1147_NDR_20140710
          sRazonSocial  := FieldByName('RazonSocialConcesionaria').AsString;                                //SS_1147_NDR_20140710
          sRut          := 'Giro :' + Trim(FieldByName('GiroConcesionaria').AsString);                      //SS_1147_NDR_20140710
          sDireccion    := Trim(FieldByName('DireccionConcesionaria').AsString) + ' - ' +                   //SS_1147_NDR_20140710
                           Trim(FieldByName('Comuna').AsString) + ' - ' +                                   //SS_1147_NDR_20140710
                           Trim(FieldByName('Region').AsString)+ ' - ' +                                    //SS_1147_NDR_20140710
                           Trim(FieldByName('Pais').AsString);                                              //SS_1147_NDR_20140710

          //ppRazonSocial.Caption := sRazonSocial;                                                            //SS_1147_NDR_20140710
          //ppRutGiro.Caption := sRut;                                                                        //SS_1147_NDR_20140710
          //ppLabel50.Caption := sDireccion;                                                      //SS_1147_NDR_20140710


          if Parameters.ParamByName('@CodigoConcesionaria').Value = CODIGO_CN then                                            //SS_1147_NDR_20140710
          begin                                                                                                               //SS_1147_NDR_20140710
            //ppTelefonosCasaMatriz.Caption:='Tel�fono : (56-2) 490 0900 / Fax : (56-2) 490 0748';                              //SS_1147_NDR_20140710
            //ppSucursal1.Caption := 'Costanera Norte - Sta. Mar�a 5621 A - 5621 B - Vitacura - Santiago';                      //SS_1147_NDR_20140710
            //ppSucursal2.Caption := 'Avda. Kennedy 5413 Local S 7276 - Las Condes - Santiago';                                 //SS_1147_NDR_20140710
            //ppSucursal3.Caption := 'Av. Bicentenario 3800 - Centro C�vico Municipalidad - Vitacura';                          //SS_1147_NDR_20140710
            //ppSucursal4.Caption := 'Estaci�n Metro U. de Chile l�nea 1, local 3 - Santiago ';                                 //SS_1147_NDR_20140710
            //ppSucursal5.Caption := 'Av. Holanda 1998, Providencia - Santiago (Sucursal no realiza atenci�n de p�blico)';      //SS_1147_NDR_20140710
            //ppSucursal6.Caption := 'Acceso Vial AMB - Calzada Poniente - Pudahuel';                                           //SS_1147_NDR_20140710
            //ppSucursal7.Caption := 'Bodega : Las Alpacas 903 - Renca';                                                        //SS_1147_NDR_20140710
          end                                                                                                                 //SS_1147_NDR_20140710
          else if Parameters.ParamByName('@CodigoConcesionaria').Value = CODIGO_VS then                                       //SS_1147_NDR_20140710
          begin                                                                                                               //SS_1147_NDR_20140710
//            ppTelefonosCasaMatriz.Caption:='Mesa Central:(56-2) 26943500';                                                    //SS_1147_NDR_20140710
//            ppSucursal1.Caption := '' ;                                                                                       //SS_1371_NDR_20150827 SS_1147_NDR_20140710
//            ppSucursal2.Caption := 'Av. Am�rico Vespucio 4665 - Macul - Santiago';                                            //SS_1371_NDR_20150827 SS_1147_NDR_20140710
//            ppSucursal3.Caption := 'Av. Am�rico Vespucio 1501, Mall Plaza Oeste, Local 133 - Cerrillos - Santiago';           //SS_1371_NDR_20150827 SS_1147_NDR_20140710
//            ppSucursal4.Caption := 'Mar Tirreno 3349, Mall Paseo Quil�n, Local 1123 - Pe�alol�n - Santiago';                  //SS_1371_NDR_20150827 SS_1147_NDR_20140710
//            ppSucursal5.Caption := 'Metro Estaci�n Universidad de Chile L�nea 1, Local 3 - Santiago';                         //SS_1371_NDR_20150827 SS_1147_NDR_20140710
//            ppSucursal6.Caption := '';                                                                                        //SS_1147_NDR_20140710
//            ppSucursal7.Caption := '';                                                                                        //SS_1147_NDR_20140710
          end;                                                                                                                //SS_1147_NDR_20140710
        end;


        //Ver que tipo de comprobante fiscal es
        try                                                                                             //PAR0114-SS-ALA-07072011
            aqryComprobante := TADOQuery.Create(nil);                                                   //PAR0114-SS-ALA-07072011
            aqryComprobante.SQL.Text := Format(SQL_FISCAL, [TipoComprobante, NumeroComprobante]);       //PAR0114-SS-ALA-07072011
            aqryComprobante.Connection := Conexion;                                                     //PAR0114-SS-ALA-07072011
            aqryComprobante.Open;                                                                       //PAR0114-SS-ALA-07072011

            FTipoComprobanteFiscal  := aqryComprobante.FieldByName('TipoComprobanteFiscal').AsString; //QueryGetValue(Conexion, Format(SQL_FISCAL, [TipoComprobante, NumeroComprobante]));   //PAR0114-SS-ALA-07072011
            FNumeroComprobanteFiscal:= aqryComprobante.FieldByName('NumeroComprobanteFiscal').AsInteger;	// TASK_063_MGO_20160819
            FSaldo                  := aqryComprobante.FieldByName('saldo').AsInteger;                  //PAR0114-SS-ALA-07072011
            FEstadoPago             := Trim(aqryComprobante.FieldByName('EstadoPago').AsString);        //PAR0114-SS-ALA-07072011
        finally                                                                                         //PAR0114-SS-ALA-07072011
            aqryComprobante.Close;                                                                      //PAR0114-SS-ALA-07072011
            FreeAndNil(aqryComprobante);                                                                //PAR0114-SS-ALA-07072011
        end;                                                                                            //PAR0114-SS-ALA-07072011

        if EsWeb then begin
        	//Obtener par�metro del Timbre Electr�nico y el fondo de los documentos electr�nicos
        	ObtenerParametroGeneral(Conexion, 'DIR_WEB_IMAGEN_FONDO_ELECTRONICAS', FDirImagenFondo);
            ObtenerParametroGeneral(Conexion, 'DIR_WEB_IMAGENES_NC', FDirImagenPAT);
            ObtenerParametroGeneral(Conexion, 'DIR_WEB_IMAGENES_NC', FDirImagenPAC);
                            //ObtenerParametroGeneral(Conexion, 'LOGO_REPORTES_WEB', RutaLogo);                         //SS_1397_MCA_20160105  //SS_1397_MGO_20151127
            ObtenerParametroGeneral(Conexion, 'LOGO_REPORTES_WS', RutaLogo);                            //SS_1397_MCA_20160105
            rptReporteConsumo.ColumnPositions.Add('0,1');                                               //SS_1397_MGO_20151127
            rptReporteConsumo.ColumnPositions.Add('4,3');                                               //SS_1397_MGO_20151127
        end else begin
        	//Obtener par�metro del Timbre Electr�nico y el fondo de los documentos electr�nicos
        	//ObtenerParametroGeneral(Conexion, 'DIR_IMAGEN_FONDO_BF_ELECTRONICAS', FDirImagenFondo);
            //INICIO:	20160809 CFU
            {ObtenerParametroGeneral(Conexion, 'DIR_IMAGEN_SELLO_PAT', FDirImagenPAT);
            ObtenerParametroGeneral(Conexion, 'DIR_IMAGEN_SELLO_PAC', FDirImagenPAC);
            ObtenerParametroGeneral(Conexion, 'LOGO_REPORTES', RutaLogo);                               //SS_1397_MGO_20151127
            }
        	//TERMINO:	20160809 CFU
            rptReporteConsumo.ColumnPositions.Add('0,9');                                               //SS_1397_MGO_20151127
            rptReporteConsumo.ColumnPositions.Add('4,3');   
                                                        //SS_1397_MGO_20151127
        end;

        ObtenerParametroGeneral(Conexion, 'NOMBRE_IMAGEN_FONDO_BF_ELECTRONICAS', FNombreImagenFondo);
//        ObtenerParametroGeneral(Conexion, 'NOMBRE_IMAGEN_SELLO_PAT', FNombreImagenPAT);
//        ObtenerParametroGeneral(Conexion, 'NOMBRE_IMAGEN_SELLO_PAC', FNombreImagenPAC);
        ObtenerParametroGeneral(Conexion, 'NOMBRE_IMAGEN_FONDO_BF_ELEC_PAT', FNombreImagenFondoPAT);        // SS_1017_PDO_20120416
        ObtenerParametroGeneral(Conexion, 'NOMBRE_IMAGEN_FONDO_BF_ELEC_PAC', FNombreImagenFondoPAC);        // SS_1017_PDO_20120416

        //INICIO:	20160809 CFU
        {FDirImagenPAT		    := GoodDir(FDirImagenPAT);
        FDirImagenPAC		    := GoodDir(FDirImagenPAC);
        FDirImagenFondo		    := GoodDir(FDirImagenFondo);
        //FNombreImagenPAT	    := FDirImagenPAT + FNombreImagenPAT;
        //FNombreImagenPAC	    := FDirImagenPAC + FNombreImagenPAC;
        FNombreImagenFondo	    := FDirImagenFondo + FNombreImagenFondo;
        FNombreImagenFondoPAT	:= FDirImagenFondo + FNombreImagenFondoPAT;     // SS_1017_PDO_20120416
        FNombreImagenFondoPAC	:= FDirImagenFondo + FNombreImagenFondoPAC;     // SS_1017_PDO_20120416
		// Determina si usa el fondo o no
        FIncluirfondo := IncluirFondo;

        try                                                                     //SS_1397_MGO_20151127
            ppLogo.Picture.LoadFromFile(Trim(RutaLogo));                        //SS_1397_MGO_20151127
        except                                                                  //SS_1397_MGO_20151127
            On E: Exception do begin                                            //SS_1397_MGO_20151127
                Screen.Cursor := crDefault;                                     //SS_1397_MGO_20151127
                MsgBox(E.message, Self.Caption, MB_ICONSTOP);                   //SS_1397_MGO_20151127
                Screen.Cursor := crHourGlass;                                   //SS_1397_MGO_20151127
            end;                                                                //SS_1397_MGO_20151127
        end;                                                                    //SS_1397_MGO_20151127
        }
        //TERMINO:	20160809 CFU
        // Verificamos que existan las im�genes que se necesitan obligatoriamente
        //para poder imprimir una nota de cobro PAT y PAC


//        if (not FileExists(FNombreImagenPAT)) then begin                                        // SS_1017_PDO_20120416
//            raise Exception.Create(Format(MSG_MISSED_IMAGES_FILES, [FNombreImagenPAT]));        // SS_1017_PDO_20120416
//        end;                                                                                    // SS_1017_PDO_20120416
//
//        if (not FileExists(FNombreImagenPAC)) then begin                                        // SS_1017_PDO_20120416
//            raise Exception.Create(Format(MSG_MISSED_IMAGES_FILES, [FNombreImagenPAC]));        // SS_1017_PDO_20120416
//        end;                                                                                    // SS_1017_PDO_20120416

		//INICIO:	20160809 CFU
        {if (not FileExists(FNombreImagenFondoPAT)) then begin                                   // SS_1017_PDO_20120416
            raise Exception.Create(Format(MSG_MISSED_IMAGES_FILES, [FNombreImagenFondoPAT]));   // SS_1017_PDO_20120416
        end;                                                                                    // SS_1017_PDO_20120416

        if (not FileExists(FNombreImagenFondoPAC)) then begin                                   // SS_1017_PDO_20120416
            raise Exception.Create(Format(MSG_MISSED_IMAGES_FILES, [FNombreImagenFondoPAC]));   // SS_1017_PDO_20120416
        end;                                                                                    // SS_1017_PDO_20120416

        // Fondo
        if (not FileExists(FNombreImagenFondo)) then begin
            raise Exception.Create(Format(MSG_MISSED_IMAGES_FILES, [FNombreImagenFondo]));      // SS_1017_PDO_20120416
        end;
        }

        spFacturaEstadoCuenta.Parameters.Refresh;
        spFacturaEstadoCuenta.Parameters.ParamByName('@TipoComprobanteFiscal').Value	:= FTipoComprobanteFiscal;		// TASK_063_MGO_20160819
        spFacturaEstadoCuenta.Parameters.ParamByName('@NumeroComprobanteFiscal').Value	:= FNumeroComprobanteFiscal;	// TASK_063_MGO_20160819
        spFacturaEstadoCuenta.Open;
        if spFacturaEstadoCuenta.RecordCount = 0 then begin
        	
        end;
        //TERMINO:	20160809 CFU

         //Determinar la carga de la DLL
        if not ConfigurarVariable_EGATE_HOME(Conexion, EsWEB, MensajeError) then
            raise Exception.Create(MensajeError);
        // Rev. 1 (SS 845)
        //CodErrorDLL := CargarDLLq1	();
        //if CodErrorDLL < 0 then raise Exception.Create(ObtieneErrorAlCargarDLL(CodErrorDLL));
        // Fin Rev. 1 (SS 845)

        //Colocamos la glosa de la resoluci�n SII
        if not ObtenerParametroGeneral(Conexion, 'RESOLUCION_SII_BOLETA_ELECTRONICA', FGlosaResolucionSIIBoleta) then FGlosaResolucionSIIBoleta := MSG_SIN_GLOSA;
        if not ObtenerParametroGeneral(Conexion, 'RESOLUCION_SII_FACTURA_ELECTRONICA', FGlosaResolucionSIIFactura) then FGlosaResolucionSIIFactura := MSG_SIN_GLOSA;

        // Inicio Bloque SS_1045_CQU_20120604
        FHecho := False;
        FOpcion1 := False;
        FOpcion2 := False;
        FillChar(FRespuesta, SizeOf(FRespuesta), 0);
        // Fin Bloque SS_1045_CQU_20120604

        FPreparado := False;
        FEsConfiguracion := False;
        Result := True;
    except
        on e:Exception do begin
            Result := False;
            Error := e.Message;
        end;
    end;
end;

{--------------------------------------------------------------------------
                	ConfigurarDestino

Author: mbecerra
Date: 16-Abril-2009
Description:	(Ref. Facturaci�n Electr�nica)
            	Permite indicar que la impresi�n es s�lo de configuraci�n
----------------------------------------------------------------------------}
procedure TReporteBoletaFacturaElectronicaForm.ConfigurarDestino;
begin
	FEsConfiguracion := EsConfigurar;
end;

{--------------------------------------------------------------------------
                    EliminarImagenTimbreElectronico

Author: mbecerra
Date: 03-Febrero-2010
Description:	Borra la imagen JPG del Timbre Electr�nico
            	Retorna verdadero si la imagen no existe despu�s de borrarla.
                Falso en caso contrario
----------------------------------------------------------------------------}
function TReporteBoletaFacturaElectronicaForm.EliminarImagenTimbreElectronico;
begin
    DeleteFile(FRutaImagenTimbreElectronico);
    Result := not FileExists(FRutaImagenTimbreElectronico);

end;

{-----------------------------------------------------------------------------
 				Ejecutar
  Author: ndonadio
  Date: 28/12/2004
  Description:		Abre los origenes de datos con los parametros pasados, genera la informacion a
            		mostrar y muestra el reporte.
-----------------------------------------------------------------------------}
function TReporteBoletaFacturaElectronicaForm.Ejecutar(ActivarOpciones: Boolean = False): Boolean;
resourcestring
    MSG_DETAIL_FREE_UNTIL_DATE      = 'La facturacion detallada es gratuita hasta el d�a %s';
    MSG_DETAIL_FREE_UNTIL_ZERO      = 'Le restan %d impresiones de la facturacion detallada';
    MSG_DETAIL_CHARGED              = 'La facturaci�n detallada tiene un costo de $ %d';
    MSG_DETAIL_FREE_ALWAYS          = 'La facturacion detallada es gratuita.';
    MSG_WANT_DETAIL                 = 'Desea imprimir la Facturaci�n Detallada?';
    TITLE_DETAIL                    = 'Facturaci�n Detallada';
    PRINTING_COMPLETE               = 'La impresi�n ha sido satisfactoria?';
    PRINTING_TITLE                  = 'Facturaci�n Detallada';
    MSG_PRINTING_DETCON				= 'El Detalle de Consumos se imprimir� en otra hoja, desea imprimirlo?';
    MSG_PRINTING_DETCON_TITLE		= 'Impresi�n de Detalle de Consumos'                        ;
    MSG_PRINT_OPTIONS_AND_PREVIEW	= 'Mostrar opciones de configuraci�n y previsualizaci�n?';
    MSG_PRINT_BAKGROUND_IMAGE		= 'Incluir Fondo de la Nota de Cobro?';
    MSG_PRINTING_INVOICE_TITLE		= 'Impresi�n de comprobante';
    MSG_PRINTING_INVOICE			= 'El Comprobante se va a imprimir en: ' + crlf +'%s ( %s )'+crlf+crlf+'Confirme la impresi�n';
    MSG_PRINT_CEDIBLE				= 'Imprimir la opci�n "CEDIBLE" ? (s�lo facturas)';
var
    DfltConfig: TRBConfig;
    FImp: TRespuesta;
    FActivarOpciones: Array[1..3] of Integer;
begin

    try
        FillChar(FImp, SizeOf(FImp), 0);
        FIsPDF := False;
        DfltConfig := rbiFactura.GetConfig;
        // Revision 5.
        if ActivarOpciones then begin
            FActivarOpciones[1] := 1;
            FActivarOpciones[2] := 1;
            FActivarOpciones[3] := 1;
        end
        else begin
        	//predefinidamente debe tener las dos primeras opciones activadas.
            FActivarOpciones[1] := 1;
            FActivarOpciones[2] := 1;
            FActivarOpciones[3] := 0;
        end;

        if  (FSaldo = 0) and                                                                                             //PAR0114-ALA-07072011
            (FEstadoPago = COMPROBANTE_PAGO) and                                                                         //PAR0114-ALA-07072011
            ((FTipoComprobanteFiscal = FTipoFacturaAfecta) or (FTipoComprobanteFiscal = FTipoFacturaExenta)) then begin
            // Inicio Bloque SS_1045_CQU_20120604
            {Fimp := ImprimirWO.Ejecutar(MSG_PRINTING_INVOICE_TITLE,
                                            Format(MSG_PRINTING_INVOICE, [DfltConfig.PrinterName, DfltConfig.BinName]),
                                            ['Si','No'],
                                            [MSG_PRINT_BAKGROUND_IMAGE, MSG_PRINT_OPTIONS_AND_PREVIEW, MSG_PRINT_CEDIBLE],
                                            FActivarOpciones,
                                            twoCHeck);}
            if Hecho and not Visible then
                FImp := Respuesta
            else begin
                Fimp := ImprimirWO.Ejecutar(MSG_PRINTING_INVOICE_TITLE,
                                            Format(MSG_PRINTING_INVOICE, [DfltConfig.PrinterName, DfltConfig.BinName]),
                                            ['Si','No'],
                                            [MSG_PRINT_BAKGROUND_IMAGE, MSG_PRINT_OPTIONS_AND_PREVIEW, MSG_PRINT_CEDIBLE],
                                            FActivarOpciones,
                                            twoCHeck);
            // Fin Bloque SS_1045_CQU_20120604
            end;
        end
        else begin
            // si es boleta no se imprime la opci�n "cedible"
            // Inicio Bloque SS_1045_CQU_20120604
            {Fimp := ImprimirWO.Ejecutar(MSG_PRINTING_INVOICE_TITLE,
                                            Format(MSG_PRINTING_INVOICE, [DfltConfig.PrinterName, DfltConfig.BinName]),
                                            ['Si','No'],
                                            [MSG_PRINT_BAKGROUND_IMAGE, MSG_PRINT_OPTIONS_AND_PREVIEW],
                                            FActivarOpciones,
                                            twoCHeck);}
            if Hecho and not Visible then
                FImp := Respuesta
            else begin
                Fimp := ImprimirWO.Ejecutar(MSG_PRINTING_INVOICE_TITLE,
                                            Format(MSG_PRINTING_INVOICE, [DfltConfig.PrinterName, DfltConfig.BinName]),
                                            ['Si','No'],
                                            [MSG_PRINT_BAKGROUND_IMAGE, MSG_PRINT_OPTIONS_AND_PREVIEW],
                                            FActivarOpciones,
                                            twoCHeck);
            // Fin Bloque SS_1045_CQU_20120604
            end;
        end;

        if not Hecho and not Visible then   // SS_1045_CQU_20120604
            Respuesta := FImp;              // SS_1045_CQU_20120604

        //Si no desea imprimir, sale...
        if FImp[0] = mrCancel then begin
            // Inicio Bloque SS_1045_CQU_20120604
            //Result := False;
            //Exit;
            if Hecho and not Visible and not (FImp[0] = mrCancel)  then
                Result := true
            else begin
                Result := False;
                Hecho := True;
                Exit;
            end;
            // Fin Bloque SS_1045_CQU_20120604
        end;

        //Tengo que incluir el fondo?
        FIncluirFondo := (FImp[1] = 1);

        // Si elegi Opciones de Impresion, por defecto va a Vista Previa
        if (FImp[2]=1) then DfltConfig.DeviceType := 'Screen';
        rptReporteFactura.DeviceType :=  DfltConfig.DeviceType;

        //ver si debe imprimir la Leyenda "CEDIBLE"
        FImprimirCedible := (FImp[3] = 1) and (	(FTipoComprobanteFiscal = FTipoFacturaAfecta) or
                            					(FTipoComprobanteFiscal = FTipoFacturaExenta));

        //rbiFactura.Report.PrinterSetup.BinName := BIN_NK;
        rbiFactura.SetConfig(DfltConfig);

        if Hecho and not Visible then begin    // SS_1045_CQU_20120604
            //Result := Opcion2;                 		// SS_1045_CQU_20120604
            if Opcion2 then                            	// SS_1045_CQU_20120604
                Result := rbiFactura.Execute(False)    	// SS_1045_CQU_20120604
            else                                       	// SS_1045_CQU_20120604
                Result := Opcion2;                     	// SS_1045_CQU_20120604
        end else begin                         // SS_1045_CQU_20120604
            //Ejecuto el report...
            Result := rbiFactura.Execute( (FImp[2]=1) );
        end;
        FImprimioComprobante := Result;

        if not Hecho and not Visible then      // SS_1045_CQU_20120604
            Opcion2 := Result;                 // SS_1045_CQU_20120604

        if not Result then Exit;

        //REV.6
        EliminarImagenTimbreElectronico();

        if (FMostrarConsumoAparte) then begin
            Fimp := ImprimirWO.Ejecutar( MSG_PRINTING_DETCON_TITLE,MSG_PRINTING_DETCON,['Si','No'],
                    [MSG_PRINT_OPTIONS_AND_PREVIEW],[0], twoCHeck );
            if FImp[0] = mrOK then begin
                Result := rbiConsumo.Execute(FImp[1] = 1);
            end;
        end;
        Hecho := True;                         // SS_1045_CQU_20120604
    except
      on E:Exception do begin
        Result := False;
        FImprimioComprobante := False;
        raise Exception.Create(E.Message);//Rev. 7
      end;
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    ndonadio
  Date Created: 28/12/2004
  Description: Cierra los origenes de datos al cerrar el Form.
  Parameters: Sender: TObject; var Action: TCloseAction
  Return Value: None

  Revision : 1
    Author : pdominguez
    Date   : 16/11/2009
    Description : SS 845
        - Se pasa la descarga de la librer�a de la facturaci�n electr�nica
        a la funci�n Preparar.
-----------------------------------------------------------------------------}
procedure TReporteBoletaFacturaElectronicaForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    spComprobantes.Close;
    spUltimosDoce.Close;
    spMensaje.Close;
    spObtenerConsumos.Close;
    spObtenerDetalleCuenta.Close;
    spObtenerDatosParaTimbreElectronico.Close;
    //INICIO: 	20160809 CFU
    spFacturaEstadoCuenta.Close;
    //TERMINO:	20160809 CFU

    //Liberar la DLL
    //LiberarDLL();
end;

procedure TReporteBoletaFacturaElectronicaForm.FormCreate(Sender: TObject);
begin

end;

{---------------------------------------------------------------------------
        	Preparar

 Description: se encarga de obtener los datos para la presentaci�n del reporte

 Revision : 1
     Author : pdominguez
     Date   : 16/11/2009
     Description : SS 845
        - Convertimos el procedimiento en funci�n, a�adiendo el par�metro DescError.
        - Se controlan las posibles excepciones dentro de la funci�n.
        - Se pasa la carga y descarga de la librer�a para la facturaci�n
        electr�nica a la funci�n.
        - Cambiamos los Msgbox de Error por Raises para pasar las posibles
        excepciones a la variable par�metro DescError.
 ------------------------------------------------------------------------------}
function TReporteBoletaFacturaElectronicaForm.Preparar(var DescError: String): Boolean;
resourcestring
    TIPO_DOCUMENTO_FACTURA_AFECTA	= 'FACTURA ELECTRONICA';
    TIPO_DOCUMENTO_FACTURA_EXENTA	= 'FACTURA NO AFECTA O EXENTA ELECTRONICA';
    TIPO_DOCUMENTO_BOLETA_AFECTA	= 'BOLETA ELECTRONICA';
    TIPO_DOCUMENTO_BOLETA_EXENTA	= 'BOLETA NO AFECTA O EXENTA ELECTRONICA';
    TIPO_DOCUMENTO_NOTA_COBRO		= 'NOTA DE COBRO';
    MSG_ERROR_TIMBRE            = 'No se pudo timbrar el documento electr�nico';
    MSG_ERROR_TIMBRE_X_FECHA    = 'No se pudo timbrar el documento electr�nico, no tiene Fecha de timbraje electr�nico';         //SS_1399_NDR_20151014
    MSG_ERROR_COMPROBANTE_NO_DISPONIBLE= 'Este comprobante no est� disponible para ser impreso por el Sistema';                  //SS_1399_NDR_20151014
    MSG_ERROR_IMAGEN    = 'No se encontr� el archivo %s';
    FACTURACION_MANUAL = 'SELECT dbo.EsFacturacionManual (%d)';  // Rev. 4 - SS 943 - JBA
var
    TmpFile: String;
    CodigoTipoMedioPago: Byte;
	OldDecimalSeparator : char;
    OldThousandSeparator : char;
    Desplazamiento: single;
    oldHeight: single;
    oldWidth: single;
    CantVehiculos, RutEmisor: integer;
    TipoDocumentoElectronico, DvEmisor : string;
    Respuesta : TTimbreArchivo;
    CodErrorDLL,
    NumeroProcesoFacturacion : integer; // Rev. 4 - SS 943 - JBA
    EsFacturadoManualmente : Boolean;   // Rev. 4 - SS 943 - JBA
    GlosaSaldoInicialOtrasConcesionarias: string;                               // SS_1015_HUR_20120418
    GlosaCuotasOtrasConcesionarias: string;                                     // SS-1015-NDR-20120510
    sDirectorioTemporal:string;                                                 //SS_1307_NDR_20150804
    dFechaInicioConcesionaria:TDate;                                           //SS_1399_NDR_20151014
begin
    sDirectorioTemporal:=ObtenerDirectorioTemporal(FOrigenImpresion);			//SS_1307_NDR_20150804
    try
        try
            // Rev. 1 (SS 845)
            Result := False;
            CodErrorDLL := CargarDLL();
            if CodErrorDLL < 0 then
                raise Exception.Create(ObtieneErrorAlCargarDLL(CodErrorDLL));
            // Fin Rev. 1 (SS 845)
            FPreparado := true;
            if spComprobantes.Active    then spComprobantes.Close;
            Linea := 1;
            if spObtenerConsumos.Active then spObtenerConsumos.Close;
            Linea := 2;
            if spMensaje.Active         then spMensaje.Close;
            Linea := 3;
            if spUltimosDoce.Active     then spUltimosDoce.Close;
            Linea := 4;
            if spObtenerDetalleCuenta.Active then spObtenerDetalleCuenta.Close;
            Linea := 5;
            if spObtenerDatosParaTimbreElectronico.Active then spObtenerDatosParaTimbreElectronico.Close;

            //INICIO	:20160809 CFU
            if spFacturaEstadoCuenta.Active then spFacturaEstadoCuenta.Close;
            //TERMINO	:20160809


            // Guarda los separadores decimales y los cambia
            // por "," para decimales y "." para miles
            OldDecimalSeparator	:= DecimalSeparator;
            OldThousandSeparator:= ThousandSeparator;
            DecimalSeparator	:= ',';
            ThousandSeparator	:= '.';
            spComprobantes.Parameters.ParamByName('@TipoComprobante').Value := FTipoComprobante;
            spComprobantes.Parameters.ParamByName('@NumeroComprobante').Value := FNumeroComprobante;
            Linea := 6;
            spComprobantes.Open;
            Linea := 7;
            //Detalle de factura Electr�nica
            spObtenerDetalleCuenta.Parameters.Refresh;
            spObtenerDetalleCuenta.Parameters.ParamByName('@TipoComprobante').Value := FTipoComprobante;
            spObtenerDetalleCuenta.Parameters.ParamByName('@NumeroComprobante').Value := FNumeroComprobante;
            spObtenerDetalleCuenta.Open;

            //Determinamos el tipo de comprobante Electr�nico
            TipoDocumentoElectronico := spComprobantes.FieldByName('TipoComprobanteFiscal').AsString;
            if TipoDocumentoElectronico = FTipoBoletaAfecta then ppTipoDocumentoElectronico.Caption := TIPO_DOCUMENTO_BOLETA_AFECTA;
            if TipoDocumentoElectronico = FTipoBoletaExenta then ppTipoDocumentoElectronico.Caption := TIPO_DOCUMENTO_BOLETA_EXENTA;
            if TipoDocumentoElectronico = FTipoFacturaAfecta then ppTipoDocumentoElectronico.Caption := TIPO_DOCUMENTO_FACTURA_AFECTA;
            if TipoDocumentoElectronico = FTipoFacturaExenta then ppTipoDocumentoElectronico.Caption := TIPO_DOCUMENTO_FACTURA_EXENTA;
            if TipoDocumentoElectronico = FTipoNotaCobro then ppTipoDocumentoElectronico.Caption := TIPO_DOCUMENTO_NOTA_COBRO;

            // Rev.2 / 28-04-2009  / Nelson Droguett Sierra
            if TipoDocumentoElectronico = FTipoBoletaAfecta then ppLabel1.Caption := TIPO_DOCUMENTO_BOLETA_AFECTA;
            if TipoDocumentoElectronico = FTipoBoletaExenta then ppLabel1.Caption := TIPO_DOCUMENTO_BOLETA_EXENTA;
            if TipoDocumentoElectronico = FTipoFacturaAfecta then ppLabel1.Caption := TIPO_DOCUMENTO_FACTURA_AFECTA;
            if TipoDocumentoElectronico = FTipoFacturaExenta then ppLabel1.Caption := TIPO_DOCUMENTO_FACTURA_EXENTA;

             //obtenemos el timbre digital
            with spObtenerDatosParaTimbreElectronico do begin
            	Parameters.Refresh;
                Parameters.ParamByName('@TipoComprobante').Value := FTipoComprobante;
                Parameters.ParamByName('@NumeroComprobante').Value := FNumeroComprobante;
                ExecProc;
                //No timbramos si el documento es una NK antigua
                if TipoDocumentoElectronico <> FTipoNotaCobro then
              //  if false then
                begin
                  //BEGIN : SS_1399_NDR_20151014 -------------------------------------------------------------------------------------
                  if (Parameters.ParamByName('@FechaTimbre').Value<>null) then
                  begin
                    if ObtenerTimbreDBNet(	Parameters.ParamByName('@RutEmisor').Value,
                                              Parameters.ParamByName('@DVRutEmisor').Value,
                                              Parameters.ParamByName('@CodigoTipoDocumentoElectronico').Value,
                                              Parameters.ParamByName('@NumeroComprobanteFiscal').Value,
                                              Parameters.ParamByName('@FechaEmision').Value,
                                              Parameters.ParamByName('@TotalComprobante').Value,
                                              Parameters.ParamByName('@DescripcionPrimerItem').Value,
                                              Parameters.ParamByName('@RutReceptor').Value,
                                              Parameters.ParamByName('@DVRutReceptor').Value,
                                              Parameters.ParamByName('@RazonSocialONombrePersona').Value,
                                              Parameters.ParamByName('@FechaTimbre').Value,
                                              True,
                                              Respuesta
                              ) then begin
//                        FRutaImagenTimbreElectronico := PARAM_DIR_EGATE_HOME + 'out\html\' + Respuesta.Archivo + '.jpg';
                         FRutaImagenTimbreElectronico := PARAM_DIR_EGATE_HOME + 'in\ted\' + Respuesta.Archivo + '.jpg';     // Cambio para Versi�n esuite INTERVIAL
                       if FileExists(FRutaImagenTimbreElectronico) then begin
                            imgTimbre.Picture.LoadFromFile(FRutaImagenTimbreElectronico);
                        end else
                            raise Exception.Create(MSG_ERROR_TIMBRE + '-' + Respuesta.Mensaje); //   //Revision 7
                        
                          //raise Exception.Create(Format(MSG_ERROR_IMAGEN, [FRutaImagenTimbreElectronico])); // Rev. 1 (SS 845)

                    end else raise Exception.Create(MSG_ERROR_TIMBRE); // Rev. 1 (SS 845)
                  end else begin
                    dFechaInicioConcesionaria := QueryGetValueDateTime(FConexion,'SELECT dbo.ObtenerParametroGeneralDatetime('+''''+'FECHA_INICIO_CONCESIONARIA'+''''+')');
                    if Parameters.ParamByName('@FechaEmision').Value < dFechaInicioConcesionaria then begin
                      raise Exception.Create(MSG_ERROR_COMPROBANTE_NO_DISPONIBLE);
                    end else begin
                      raise Exception.Create(MSG_ERROR_TIMBRE_X_FECHA);
                    end;
                  end;
                  //END : SS_1399_NDR_20151014 -------------------------------------------------------------------------------------
                end;
            end;

            //Rut Concesionaria
            // s�lo si el documento es electr�nico
            if TipoDocumentoElectronico <> FTipoNotaCobro then begin

    	        RutEmisor := spObtenerDatosParaTimbreElectronico.Parameters.ParamByName('@RutEmisor').Value;
              	DVEmisor  := spObtenerDatosParaTimbreElectronico.Parameters.ParamByName('@DVRutEmisor').Value;

             //   RutEmisor := 99999999;
              //	DVEmisor  := '9';

    	      ppRutConcesionaria.Caption :=	'R.U.T. ' + Format('%.0n', [1.0 * RutEmisor]) + '-' + DvEmisor;
              ppRutConcesionaria.Caption := ReplaceText(ppRutConcesionaria.Caption,',','.');


                //Glosa Resoluci�n SII
                if	(TipoDocumentoElectronico = FTipoBoletaAfecta) or (TipoDocumentoElectronico = FTipoBoletaExenta) then begin
                	ppResolucionSII.Caption := FGlosaResolucionSIIBoleta;
                    ppGiroCliente.Visible := False;
                    ppDBGiroCliente.Visible := False;
                end
                else ppResolucionSII.Caption := FGlosaResolucionSIIFactura;

            end
            else begin
    	        // si es una NK antigua, colocar el fondo gris y el texto en blanco
                ppRutConcesionaria.Visible := False;
                ppsCuadroFactura.Shape := stRoundRect;
                ppsCuadroFactura.Brush.Color := clGrayText;
                ppNumeroDocumentoElectronico.Font.Color := clWhite;
                ppTipoDocumentoElectronico.Font.Color := clWhite;
                ppTipoDocumentoElectronico.Font.Style := [fsBold];
                imgTimbre.Visible := False;
                ppResolucionSII.Visible := False;
            end;

            ppNumeroDocumentoElectronico.Caption := 'N� ' + Format('%.0n', [1.0 * spComprobantes.FieldByName('NumeroComprobanteFiscal').AsInteger]);

            // ver si debe imprimir la leyenda CEDIBLE
            //pplblCedible.Visible := FImprimirCedible;
           // ppRegionCedible.Visible := FImprimirCedible;

            GlosaSaldoInicialOtrasConcesionarias := VarToStr(spComprobantes.FieldByName('GlosaSaldoInicialOtrasConcesionarias').Value);

            //INICIO: 20160809 CFU
            {if Trim(GlosaSaldoInicialOtrasConcesionarias) = '' then
            begin
               ppdbtSaldoInicial.Visible := False;
               ppdbtGlosaSaldoInicial.Visible := False;
            end;
            
            GlosaCuotasOtrasConcesionarias := VarToStr(spComprobantes.FieldByName('GlosaCuotasOtrasConcesionarias').Value);  //SS-1015-NDR-20120510
            if Trim(GlosaCuotasOtrasConcesionarias) = '' then                                                                //SS-1015-NDR-20120510
            begin                                                                                                            //SS-1015-NDR-20120510
               ppdbtCuotas.Visible := False;                                                                                 //SS-1015-NDR-20120510
               ppdbtGlosaCuotas.Visible := False;                                                                            //SS-1015-NDR-20120510
            end;                                                                                                             //SS-1015-NDR-20120510
            }
            //TERMINO: 20160809 CFU
            //Determinamos el tipo de medio de pago del comprobante y si est� pagado
            CodigoTipoMedioPago := spComprobantes.FieldByName('CodigoTipoMedioPago').AsInteger;
            Linea := 8;
            Linea := 9;
            FTipoPago := TTipoPago(CodigoTipoMedioPago);
            Linea := 9;
            Linea := 10;
            Linea := 11;
            Linea := 12;
            spObtenerConsumos.Parameters.Refresh;
            spObtenerConsumos.Parameters.ParamByName('@TipoComprobante').Value      := FTipoComprobante;
            spObtenerConsumos.Parameters.ParamByName('@NumeroComprobante').Value    := FNumeroComprobante;
            spObtenerConsumos.Parameters.ParamByName('@TotalKm').Value              := 0;
        	spObtenerConsumos.Parameters.ParamByName('@TotalN').Value               := 0;
        	spObtenerConsumos.Parameters.ParamByName('@TotalP').Value               := 0;
        	spObtenerConsumos.Parameters.ParamByName('@TotalC').Value               := 0;
        	spObtenerConsumos.Parameters.ParamByName('@TotalTotal').Value           := 0;
            spObtenerConsumos.Open;
            Linea := 13;

            spMensaje.Parameters.Refresh;
            spMensaje.Parameters.ParamByName('@TipoComprobante').Value := FTipoComprobante;
            spMensaje.Parameters.ParamByName('@NumeroComprobante').Value := FNumeroComprobante;
            Linea := 14;
            spMensaje.Open;
            Linea := 15;

            spUltimosDoce.Parameters.Refresh;
            spUltimosDoce.Parameters.ParamByName('@TipoComprobante').Value := FTipoComprobante;
            spUltimosDoce.Parameters.ParamByName('@NumeroComprobante').Value := FNumeroComprobante;
            Linea := 16;
            spUltimosDoce.Open;
            Linea := 17;

            // Creamos el gr�fico y lo guardamos como imagen a disco
            dbcUltimosDoce.RefreshData ;
            Linea := 18;
            //TmpFile := GoodDir(GetTempDir) + TempFile + '.bmp';               //SS_1307_NDR_20150804
            TmpFile := sDirectorioTemporal + TempFile + '.bmp';                 //SS_1307_NDR_20150804
            Linea := 19;
            dbcUltimosDoce.SaveToBitmapFile(TmpFile);
            Linea := 20;

            // Asigno la imagen del grafico al img del Report
            imgGraficoUltimosDoce.Picture.LoadFromFile(TmpFile);
            Linea := 21;
            DeleteFile(TmpFile);
            Linea := 22;

            //determinamos si lleva fondo o no, y lo cargamos
            //INICIO:	20160809 CFU
            {
            if FIncluirFondo then begin
                ppImageFondo.Visible := True;
                ppImageFondo.Picture.LoadFromFile(FNombreImagenFondo);
            end else
                ppImageFondo.Visible := False;
            }
            //TERMINO:	20160809 CFU
            Linea := 23;


            //Determinamos cuales de todas las imagenes de pago tenemos que mostrar
            //imgPACPAT.Visible := False;

            // Rev. 4: Se obtiene el Numero Proceso Facturacion correspondiente al
            NumeroProcesoFacturacion := spComprobantes.FieldByName('NumeroProcesoFacturacion').AsInteger; // Rev. 4 - SS 943 - JBA

            // Rev. 4 Se verifica si se trata de un comprobante generado manualmente
            EsFacturadoManualmente := StrToBool(QueryGetValue(FConexion, Format(FACTURACION_MANUAL, [NumeroProcesoFacturacion]))); // Rev. 4 - SS 943 - JBA

//            if Not EsFacturadoManualmente then begin // Rev. 4 - SS 943 - JBA
                case FTipoPago of
        	        tpPAC:
                    	begin
                        	//INICIO:	20160809 CFU
                            {if FIncluirFondo then begin                                     // SS_1017_PDO_20120416
                                ppImageFondo.Picture.LoadFromFile(FNombreImagenFondoPAC);   // SS_1017_PDO_20120416
                            end;                                                             // SS_1017_PDO_20120416
                            }
                            ImagePAC.Visible := True;
                            //TERMINO:	20160809 CFU
                            
//                            else begin                                                      // SS_1017_PDO_20120416
//                        		imgPACPAT.Picture.LoadFromFile(FNombreImagenPAC);           // SS_1017_PDO_20120416
//                	            imgPACPAT.Visible := True;                                  // SS_1017_PDO_20120416
//                            end;                                                            // SS_1017_PDO_20120416
                        	Linea := 25;
                            
                    	end;

                    tpPAT:
                    	begin
                        	//INICIO:	20160809 CFU
                            {if FIncluirFondo then begin                                     // SS_1017_PDO_20120416
                                ppImageFondo.Picture.LoadFromFile(FNombreImagenFondoPAT);   // SS_1017_PDO_20120416
                            end;                                                             // SS_1017_PDO_20120416
                            }
                            ImagePAT.Visible := True;
                            //TERMINO:	20160809 CFU
//                            else begin                                                      // SS_1017_PDO_20120416
//                                imgPACPAT.Picture.LoadFromFile(FNombreImagenPAT);           // SS_1017_PDO_20120416
//                                imgPACPAT.Visible := True;                                  // SS_1017_PDO_20120416
//                            end;                                                            // SS_1017_PDO_20120416
                            Linea := 26;
                        end
					else
                    	begin
                            ImageServipag.Visible := True;
                        end;
                end;
//            end;

            Linea := 27;
            Linea := 28;

            // Vuelve a dejar los separadores decimales como estaban
            DecimalSeparator  := OldDecimalSeparator;
            ThousandSeparator := OldThousandSeparator;

            if not FIsPDF then begin
                // Chequea que no haya lineas sin completar en la direccion
                Desplazamiento := 0;
                if    (  spComprobantes.FieldByName('Domicilio').IsNull)
                   or (  TRIM( spComprobantes.FieldByName('Domicilio').AsString) = '')
                then begin
                        Linea := 29;
                        With  ppdbtDir1 do begin
                            Desplazamiento := Desplazamiento +  Height;
                            Visible := False;
                        end;
                        Linea := 30;
                end
                else begin
                        Linea := 31;
                       if Length(ppdbtDir1.Text) < 40 then begin
                            Linea := 32;
                            oldHeight   := ppdbtDir1.Height;
                            oldWidth    := ppdbtDir1.Width ;
                            ppdbtDir1.Autosize := True;
                            Desplazamiento := Desplazamiento + (oldHeight - ppdbtDir1.Height);
                            ppdbtDir1.Autosize := False;
                            ppdbtDir1.Width := oldWidth;
                            Linea := 33;
                       end;
                       ppdbtDir1.WordWrap := True;
                       Linea := 34;
                end;
                //lblTestLegth.Visible := FALSE;    // SS_1182_CQU_20140424
                Linea := 35;

                if spComprobantes.FieldByName('Comuna').IsNull
                   or (  TRIM( spComprobantes.FieldByName('Comuna').AsString) = '')
                then begin
                    Linea := 36;
                    Desplazamiento := Desplazamiento +  ppdbtDir2.Height;
                    ppdbtDir2.Visible := False;
                    Linea := 37;
                end
                else begin
                    Linea := 38;
                    ppdbtDir2.Top := ppdbtDir2.Top - Desplazamiento;
                    Linea := 39;
                end;
                {
                if spComprobantes.FieldByName('CodigoPostal').IsNull
                   or (  TRIM( spComprobantes.FieldByName('CodigoPostal').AsString) = '')
                then begin
                            ppdbbcConvenio.Visible := False;
                end
                else begin
                    ppdbbcConvenio.Top := ppdbbcConvenio.Top - Desplazamiento;
                end;

                Linea := 40;

                ppdbtNumConvenioFormat.Top := ppdbtNumConvenioFormat.Top - Desplazamiento - iif( ppdbbcConvenio.Visible, 0, ppdbbcConvenio.Height);
                pplabel20.Top := pplabel20.Top - Desplazamiento - iif( ppdbbcConvenio.Visible, 0, ppdbbcConvenio.Height);
                Linea := 41;
                 }
            end;
            //lblTestLegth.Visible := FALSE;    // SS_1182_CQU_20140424

//            pplblKm.Caption         := iif( spObtenerConsumos.Parameters.ParamByName('@TotalKm').Value = null,'',
//                                    spObtenerConsumos.Parameters.ParamByName('@TotalKm').Value );
                              { iif( spObtenerConsumos.Parameters.ParamByName('@TotalKm').Value = null,'',
                            Trim(Format('%.2n', [real(spObtenerConsumos.Parameters.ParamByName('@TotalKm').Value)] )));  }
{            Linea := 42;
            pplblNormal.Caption     := iif( spObtenerConsumos.Parameters.ParamByName('@TotalN').Value = null,'',
                                    spObtenerConsumos.Parameters.ParamByName('@TotalN').Value );
            Linea := 43;
            pplblCongestion.Caption := iif( spObtenerConsumos.Parameters.ParamByName('@TotalC').Value = null,'',
                                    spObtenerConsumos.Parameters.ParamByName('@TotalC').Value );
            Linea := 44;
            pplblPunta.Caption      := iif( spObtenerConsumos.Parameters.ParamByName('@TotalP').Value = null,'',
                                    spObtenerConsumos.Parameters.ParamByName('@TotalP').Value );
 }
            Linea := 45;
            pplblTotal.Caption      := iif( spObtenerConsumos.Parameters.ParamByName('@TotalTotal').Value = null,'',
                            spObtenerConsumos.Parameters.ParamByName('@TotalTotal').Value );

            Linea := 46;
            CantVehiculos :=  spObtenerconsumos.RecordCount ;
            Linea := 47;
            FMostrarConsumoAparte := False;
            if CantVehiculos > 17 then begin
                Linea := 48;
                ppDetailBand2.Visible := False;
                //INICIO:	20160809 CFU
                //ppSummaryBand1.Visible := True;
                //TERMINO:	20160809 CFU
                {
                ppDBText1.Visible := False;
                ppDBText2.Visible := False;
                ppDBText3.Visible := False;
                ppDBText4.Visible := False;
                ppDBText5.Visible := False;
                ppDBText6.Visible := False;
                ppLblAnexo.Visible := True;
                ppchildReport1.Bands[1].Height := 2.2080;
                }
{                pplblKm17.Caption         := iif( spObtenerConsumos.Parameters.ParamByName('@TotalKm').Value = null,'',
                                            Trim(Format('%.2n', [real(spObtenerConsumos.Parameters.ParamByName('@TotalKm').Value)] )));
                pplblNormal17.Caption     := iif( spObtenerConsumos.Parameters.ParamByName('@TotalN').Value = null,'',
                                            spObtenerConsumos.Parameters.ParamByName('@TotalN').Value );
                pplblCongestion17.Caption := iif( spObtenerConsumos.Parameters.ParamByName('@TotalC').Value = null,'',
                                            spObtenerConsumos.Parameters.ParamByName('@TotalC').Value );
                pplblPunta17.Caption      := iif( spObtenerConsumos.Parameters.ParamByName('@TotalP').Value = null,'',
                                            spObtenerConsumos.Parameters.ParamByName('@TotalP').Value );
 }               pplblTotal17.Caption      := iif( spObtenerConsumos.Parameters.ParamByName('@TotalTotal').Value = null,'',
                                            spObtenerConsumos.Parameters.ParamByName('@TotalTotal').Value );

                FMostrarConsumoAparte := True;
                Linea := 49;
            end
            else begin
                Linea := 50;
                ppDetailBand2.Visible := True;
                //INICIO:	20160809 CFU
                //ppSummaryBand1.Visible := False;
                //TERMINO:	20160809 CFU
                {
                ppDBText1.Visible := True;
                ppDBText2.Visible := True;
                ppDBText3.Visible := True;
                ppDBText4.Visible := True;
                ppDBText5.Visible := True;
                ppDBText6.Visible := True;
                ppLblAnexo.Visible := False;
                }
                ppchildReport1.Bands[1].Height := 0.1775;
                FMostrarConsumoAparte := False;
                Linea := 51;
            end;
            Linea := 52;

            // Verificamos que si es Web (IsPDF) el BarCode no se zarpe!
            // medidas en inches!
        //    ppdbbcConvenio2.AutoSize := False;
        //    ppdbbcConvenio2.WideBarRatio := 2;


            //Verificamos  la fechahora impreso
            //      si era null, hacemos un SET
            //      sino hacemos visible la lbl COPIA FIEL DEL ORIGINAL
            {
            ppLblCopiaFiel.Top := ppdbtDir1.Top;
            if ( QueryGetValueInt(FConexion, 'SELECT ISNULL('+
                                          '(Select 1 From Comprobantes with (NOLOCK) '+
                                          Format(' Where TipoComprobante = ''%s'' and NumeroComprobante = %d ',
                                          [FTipoComprobante, FNumeroComprobante])+
                                          'AND FechaHoraImpreso IS NOT NULL )'+
                                          ',0)') = 0 ) then begin
                    // era null --> es original
                    // Le debo setear la fechahora impreso... ???
                   Linea := 53;
                   ppLblCopiaFiel.Visible := False;
            end
            else begin
                   Linea := 54;
                    // es una copia
                   //ppLblCopiaFiel.Visible := True;
                   ppLblCopiaFiel.Visible := False;
            end;
            }
            Linea := 55;
            {pplblAnulada.Visible := spComprobantes.FieldByName('EstadoPago').AsString = 'A';
            ppLblCopiaFiel.Visible := ppLblCopiaFiel.Visible and not(pplblAnulada.Visible);}
            Linea := 56;

            //if ppImageFondo.Top <> 0 then  ppImageFondo.Top := 0;
            Linea := 100;
            Result := True;
        except
            on e:Exception do begin 
                DescError := e.Message
            end;
        end;
    finally
        if hndDLLHandle > 0 then LiberarDLL;
    end;
end;

function TReporteBoletaFacturaElectronicaForm.EjecutarPDF(var Resultado: string; var Error: String):Boolean;
begin
    //Ejecuto el report...
    try
        Result := False;
        FIsPDF := True;
//        Preparar;  //ahora lo llama desde el BeforePrint...
		Result := reporteAPDF(rptReporteFactura, Resultado, Error);
	except
        on e:Exception do begin
            Error := 'EjecutarPDF: ' + e.Message;
            Result := False;
        end;
    end;
end;

function TReporteBoletaFacturaElectronicaForm.ExportarReporte(Reporte: TppReport; Dispositivo: TppPDFDevice; var Error: string): boolean;
begin
    try
        if (FOrigenImpresion=oiServicio) then                                   //SS_1307_NDR_20150804
        begin                                                                   //SS_1307_NDR_20150804
          Reporte.ShowPrintDialog:=False;                                       //SS_1307_NDR_20150804
          Reporte.ShowCancelDialog:=False;                                      //SS_1307_NDR_20150804
          Dispositivo.PDFSettings.OpenPDFFile:=False;                           //SS_1307_NDR_20150804
        end;                                                                    //SS_1307_NDR_20150804
        Dispositivo.Publisher := Reporte.Publisher;
        Dispositivo.Reset;
        Reporte.PrintToDevices;
        result := true;
    except
        on e: exception do begin
            result := false;
            Error := '[ExportarReporte] [' + iif(FPreparado, 'Preparado Linea: ' + inttostr(Linea), 'No Preparado') +  '] ' + e.message;
        end;
    end;
end;

function TReporteBoletaFacturaElectronicaForm.ReporteAPDF(Reporte: TppReport; var Resultado: string; var Error: string): boolean;
var
    Exportador: TppPDFDevice;
    Salida: TStringStream;
    Salida2: TMemoryStream;
    sDIrectorioTemporal:string;                                                 //SS_1307_NDR_20150804
begin
    sDirectorioTemporal:=ObtenerDirectorioTemporal(FOrigenImpresion);           //SS_1307_NDR_20150804
    try
        Salida := TStringStream.Create('');
        Salida2 := TMemoryStream.Create;
        Error := '';
        Exportador := TppPDFDevice.Create(nil);

        Exportador.PDFSettings.OptimizeImageExport := False;                                                                                              // SS_995_PDO_20111006
        Exportador.PDFSettings.ScaleImages := False;
//        Exportador.PDFSettings.CompressionLevel := clMax;
//        Exportador.PDFSettings.EmbedFontOptions :=
        //Exportador.FileName := GetTempDir + FormatDateTime('yyyymmddhhnnsszzz', Now) + IntToStr(FNumeroComprobante) + '.pdf';             //SS_1307_NDR_20150804
        Exportador.FileName := sDirectorioTemporal + FormatDateTime('yyyymmddhhnnsszzz', Now) + IntToStr(FNumeroComprobante) + '.pdf';      //SS_1307_NDR_20150804

//    Exportador.PrintToStream := True;
//    Exportador.ReportStream := Salida;

        Result := ExportarReporte(Reporte, Exportador, Error);

        //CopyFile(PChar(Exportador.FileName), PChar('C:\' + ExtractFileName(Exportador.FileName)), False);

        if (Error = '') and FileExists(Exportador.FileName) then    // SS_1332_CQU_20150721
        begin                                                       // SS_1332_CQU_20150721
            // Aumento el tabulado                                  // SS_1332_CQU_20150721
            Salida2.LoadFromFile(Exportador.FileName);
            Salida2.Position := 0;
            Salida2.SaveToStream(Salida);

            Resultado := Salida.DataString; 
            DeleteFile(Exportador.FileName);                        // SS_1307_NDR_20150804
        end;                                                        // SS_1332_CQU_20150721
    finally
      if Assigned(Exportador) then FreeAndNil(Exportador);
      if Assigned(Salida) then FreeAndNil(Salida);
      if Assigned(Salida2) then FreeAndNil(Salida2);
    end;
end;


procedure TReporteBoletaFacturaElectronicaForm.rptReporteConsumoPreviewFormCreate(
  Sender: TObject);
begin
	rptReporteConsumo.PreviewForm.SendToBack ;
end;

{******************************* Procedure Header ******************************
Procedure Name: rptReporteFacturaBeforePrint
Author :
Date Created :
Parameters : Sender: TObject
Description :

Revision : 1
    Author : pdominguez
    Date   : 16/11/2009
    Description : SS 845
        - Llamamos a la funci�n Preparar y si esta no finaliza con �xito,
        cancelamos el Report.
*******************************************************************************}
procedure TReporteBoletaFacturaElectronicaForm.rptReporteFactura_BeforePrint(Sender: TObject);

        // Revision 7
        // resourcestring
        // Rev. 1 (SS 845)
        //MSG_ERROR_PREPARANDO = 'Se produjo un Error mientras se preparaba el Reporte de la Factura.';
        //MSG_CAPTION_ERROR    = 'Preparando Reporte Boleta Factura Electr�nica.';
        // Fin Rev. 1 (SS 845)
    var
        DescError: String;
begin
    //ppLblCopiaFiel.Top := ppdbtDir1.Top;
    // Rev. 1 (SS 845)
    if not Preparar(DescError) then begin
         //Revision 7
        //MsgBoxErr(MSG_ERROR_PREPARANDO,DescError,MSG_CAPTION_ERROR,MB_ICONSTOP);
        rptReporteFactura.Cancel;

      //  Revision 7
        raise Exception.Create(DescError);
     end;
    // Fin Rev. 1 (SS 845)
end;

procedure TReporteBoletaFacturaElectronicaForm.ppdbbcConvenio2Print(Sender: TObject);
begin
{    ppdbbcConvenio2.AutoSizeFont := true;
    ppdbbcConvenio2.PrintHumanReadable := true;
    ppdbbcConvenio2.AutoSize := false;
    ppdbbcConvenio2.WideBarRatio := 2;
    ppdbbcConvenio2.Top := 35;
    ppdbbcConvenio2.Width := 73.29;
    ppdbbcConvenio2.Height := 16.30;}
end;

procedure TReporteBoletaFacturaElectronicaForm.ppdbtVencimiento2Print(
  Sender: TObject);
begin
	//INICIO: 20160809 CFU
    //ppdbtVencimiento2.Visible := ppComprobantes.DataSource.DataSet.FieldByName('FechaVencimiento').AsDateTime >= StartOfTheDay(spComprobantes.FieldByName('FechaHoy').AsDateTime);
    //TERMINO: 20160809 CFU
end;

procedure TReporteBoletaFacturaElectronicaForm.ppDetailBand4BeforePrint(
  Sender: TObject);
begin
    if spObtenerDetalleCuenta.FieldByName('Monto').AsString = EmptyStr then begin

        ppDBText8.Font.Style :=  ppDBText8.Font.Style + [fsBold];
    end
    else begin
        ppDBText8.Font.Style :=  ppDBText8.Font.Style - [fsBold];
    end;
end;

{******************************* Procedure Header ******************************
Procedure Name: ppLblCopiaFielPrint
Author :
Date Created :
Parameters : Sender: TObject
Description :

Revision : 1
    Author : pdominguez
    Date   : 16/11/2009
    Description : SS 845
        - Se deshabilita el c�digo relacionado con la variable Conf, ya que no se
        usa.
*******************************************************************************}
procedure TReporteBoletaFacturaElectronicaForm.ppLblCopiaFielPrint(Sender: TObject);
//var                   // Rev. 1 (SS 845)
//    Conf: TRBConfig;  // Rev. 1 (SS 845)
begin
//    Conf := rbiFactura.GetConfig;                                     // Rev. 1 (SS 845)
    ppLblCopiaFiel.Top := ppdbtDir1.Top ;
//    if (Trim(Conf.DeviceType) = 'PDFFile') or FIsPDF then  begin      // Rev. 1 (SS 845)
        //ppLblCopiaFiel.Top := ppdbtDir1.Top + ppLblCopiaFiel.Height ;
//    end;                                                              // Rev. 1 (SS 845)
end;

function TReporteBoletaFacturaElectronicaForm.EjecutarDetallePDF(var Resultado,
  Error: String): Boolean;
begin
    //Ejecuto el report...
    try
        FIsPDF := True;
        Result := reporteAPDF(rptReporteConsumo, Resultado, Error);
	except
        on e:Exception do begin
            Error := 'EjecutarDetallePDF: ' + e.Message;
            Result := False;
        end;
    end;
end;

{------------------------------------------------------------------
                          rbiFacturaExecute

Author: mbecerra
Date: 17-Abril-2009
Description:	(Ref. Facturaci�n Electr�nica)
            	Permite imprimir o no el documento
-----------------------------------------------------------------------}
procedure TReporteBoletaFacturaElectronicaForm.rbiFacturaExecute(
  Sender: TObject; var Cancelled: Boolean);
begin
	Cancelled := FEsConfiguracion;
end;


//------------------------------------------------------------------------------------------------------------------
//BEGIN :SS_1307_NDR_20150804 --------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------
function TReporteBoletaFacturaElectronicaForm.ObtenerDirectorioTemporal(oiOrigenImpresion: TOrigenImpresion): string;
var
  sDirectorioTemporal:string;
begin
    sDirectorioTemporal:='';
    case oiOrigenImpresion of
      oiSistema:
      begin
        ObtenerParametroGeneral(FConexion, 'DIRECTORIO_TEMPORAL_SISTEMA', sDirectorioTemporal);
        //sDirectorioTemporal:='C:\TRABAJO\CARPETAS\TEMPORAL\SISTEMA';

      end;
      oiJordan:
      begin
        ObtenerParametroGeneral(FConexion, 'DIRECTORIO_TEMPORAL_JORDAN', sDirectorioTemporal);
      end;
      oiWeb:
      begin
        ObtenerParametroGeneral(FConexion, 'DIRECTORIO_TEMPORAL_WEB', sDirectorioTemporal);
      end;
      oiServicio:
      begin
        ObtenerParametroGeneral(FConexion, 'DIRECTORIO_TEMPORAL_SERVICIO', sDirectorioTemporal);
        //sDirectorioTemporal:='C:\TRABAJO\CARPETAS\TEMPORAL\SERVICIO';
      end
      else
      begin
        sDirectorioTemporal:=GetTempDir;
      end;
    end;
    if Trim(sDirectorioTemporal)='' then sDirectorioTemporal:=GetTempDir;
    Result:=GoodDir(sDirectorioTemporal);
end;
//------------------------------------------------------------------------------------------------------------------
//END :SS_1307_NDR_20150804 --------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------

procedure TReporteBoletaFacturaElectronicaForm.LiberarDBNet;        // SS_1332_CQU_20150909
begin                                                               // SS_1332_CQU_20150909
        // Sin Try para controla el error en la llamada             // SS_1332_CQU_20150909
        if not (LiberarDLL) then                                    // SS_1332_CQU_20150909
            raise Exception.Create('No se pudo liberar la Dte');    // SS_1332_CQU_20150909
end;                                                                // SS_1332_CQU_20150909

end.



