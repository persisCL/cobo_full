{-------------------------------------------------------------------------------
 File Name: FrmReclamoCuenta.pas
 Author: DDeMarco
 Date Created:
 Language: ES-AR
 Description: Ingreso y edicion de reclamos

 Revisi�n 1:
 Author      : nefernandez
 Date        : 25/04/2007
 Description : Cambi� la conversi�n del par�metro ContractSerialNumber
 (de asInteger a AsVariant)

 Revision		: 2
 Author			: Nelson Droguett Sierra
 Date			: 29-Junio-2010
 Description	: (Ref. Fase 2) Se agrega concesionaria y SubTipoReclamo

 Firma        : SS-1006-NDR-20120802
 Description  : Validar concesionaria en reclamos

 Author      : Claudio Quezada Ib��ez
 Date        : 16-Mayo-2013
 Firma       : SS_1091_CQU_20130516
 Description : Se modifica la visualizaci�n de im�genes para poder desplegar im�genes de p�rticos Italianos.
-------------------------------------------------------------------------------}
unit FrmReclamoCuenta;

interface

uses
  //Reclamo
  DMConnection,
  Util,                         //NULLDATE
  UtilDB,                       //Rutinas para base de datos
  UtilProc,                     //Mensajes
  RStrings,                     //Resource Strings
  FreContactoReclamo,           //Frame Contacto
  FreCompromisoOrdenServicio,   //Frame Compromiso
  FreSolucionOrdenServicio,     //Frame Solucion
  FreHistoriaOrdenServicio,     //Frame Historia
  FreFuenteReclamoOrdenServicio,//Frame Fuente Reclamo
  FreNumeroOrdenServicio,       //Frame Numero Orden Servicio
  FreRespuestaOrdenServicio,    //Frame Respuesta Orden Servicio
  ImagenesTransitos,            //Muestra la Ventana con la imagen
  OrdenesServicio,              //Registra la orden de  servicio
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, DB, ADODB,ExtCtrls, VariantComboBox, Validate,
  DateEdit, TimeEdit, ListBoxEx, DBListEx, ImgList, Menus,
  FreSubTipoReclamoOrdenServicio, FreConcesionariaReclamoOrdenServicio;

type
  TFormReclamoCuenta = class(TForm)
    PageControl: TPageControl;
    TabSheetDatos: TTabSheet;
	lblPatenteTag: TLabel;
    txtPatenteTag: TEdit;
    Panel1: TPanel;
    txtDetalle: TMemo;
    Panel3: TPanel;
    Label3: TLabel;
    pnlPatenteDeconocida: TPanel;
    LPatenteFaltante: TLabel;
    LComprobante: TLabel;
    txtPatenteFaltante: TEdit;
    cbComprobante: TVariantComboBox;
    TabSheetProgreso: TTabSheet;
    TabSheetHistoria: TTabSheet;
    FrameHistoriaOrdenServicio: TFrameHistoriaOrdenServicio;
    spObtenerOrdenServicioCuenta: TADOStoredProc;
    spObtenerComprobantesxConvenio: TADOStoredProc;
    spObtenerDatosCuenta: TADOStoredProc;
    CBMotivo: TVariantComboBox;
    Ehora: TTimeEdit;
    Efecha: TDateEdit;
    LFechahora: TLabel;
    LMotivos: TLabel;
    DBLTransitos: TDBListEx;
    lnCheck: TImageList;
    DataSource: TDataSource;
    SpObtenerTransitosReclamo: TADOStoredProc;
    PopupResolver: TPopupMenu;
    mnu_Resolver: TMenuItem;
    Mnu_Pendiente: TMenuItem;
    Mnu_Aceptado: TMenuItem;
    Mnu_Denegado: TMenuItem;
    Mnu_Comun: TMenuItem;
    PAbajo: TPanel;
    PDerecha: TPanel;
    CKRetenerOrden: TCheckBox;
    AceptarBTN: TButton;
    btnCancelar: TButton;
    FrameCompromisoOrdenServicio1: TFrameCompromisoOrdenServicio;
    FrameContactoReclamo1: TFrameContactoReclamo;
    FrameSolucionOrdenServicio1: TFrameSolucionOrdenServicio;
    Ldetalledelprogreso: TLabel;
    txtDetalleSolucion: TMemo;
    FrameFuenteReclamoOrdenServicio1: TFrameFuenteReclamoOrdenServicio;
    ilCamara: TImageList;
    FrameNumeroOrdenServicio1: TFrameNumeroOrdenServicio;
    TabSheetRespuesta: TTabSheet;
    FrameRespuestaOrdenServicio1: TFrameRespuestaOrdenServicio;
    FrameConcesionariaReclamoOrdenServicio1: TFrameConcesionariaReclamoOrdenServicio;
    FrameSubTipoReclamoOrdenServicio1: TFrameSubTipoReclamoOrdenServicio;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure AceptarBTNClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure DBLTransitosDrawText(Sender: TCustomDBListEx;Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;var Text: String; var TxtRect: TRect; var ItemWidth: Integer;var DefaultDraw: Boolean);
    procedure DBLTransitosLinkClick(Sender: TCustomDBListEx;Column: TDBListExColumn);
    procedure EfechaChange(Sender: TObject);
    procedure DBLTransitosCheckLink(Sender: TCustomDBListEx;Column: TDBListExColumn; var IsLink: Boolean);
    procedure Mnu_ComunClick(Sender: TObject);
    procedure DBLTransitosContextPopup(Sender: TObject; MousePos: TPoint;var Handled: Boolean);
  private
    { Private declarations }
  	FContextMark: Integer;
    FCodigoConvenio: Integer;
    FIndiceVehiculo: Integer;
    FTipoOrdenServicio: Integer;
    FContractSerialNumber: Int64;
    FCodigoOrdenServicio: Integer;
    FPatenteDesconocida: AnsiString;
    FEditando : Boolean; //Indica si estoy editando o no
    Function GetCodigoOrdenServicio : Integer;
    Function ListaTransitosInclude(Lista: TStringList ; Valor : String) : Boolean;
    Function ListaTransitosExclude(Lista: TStringList ; Valor : String) : Boolean;
    Function ListaTransitosIn(Lista : TStringList ; Valor : String) : Boolean;
    Function ListaTransitosEmpresaIn(Valor : String; var Resolucion : Integer) : Boolean;
    Function ValidarEstadoTransitos : Boolean;
    Function CargarTransitos : Boolean;
  public
    { Public declarations }
    Function Inicializar(TipoOrdenServicio: Integer; CodigoConvenio, IndiceVehiculo: Integer): Boolean;
    Function InicializarEditando(CodigoOrdenServicio: Integer): Boolean;
    Function InicializarSolucionando(CodigoOrdenServicio: Integer): Boolean;
    Property CodigoOrdenServicio: Integer Read GetCodigoOrdenServicio;
  end;

var
  FormReclamoCuenta: TFormReclamoCuenta;
  FListaTransitosCliente : TStringList; //Guarda los transitos que el usuario no rechaza
  FListaTransitosEmpresa : TStringList; //Guarda la resolucion de la empresa sobre cada transito

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:   DDeMarco
  Date Created:
  Description:  Inicializaci�n de este formulario
  Parameters: TipoOrdenServicio: Integer; TipoComprobante: AnsiString; NumeroComprobante: Int64
  Return Value: Boolean
-----------------------------------------------------------------------------}
Function TFormReclamoCuenta.Inicializar(TipoOrdenServicio: Integer;CodigoConvenio, IndiceVehiculo: Integer) : Boolean;

    Procedure CargarMotivosDesconocePatente(Combo : TVariantComboBox);
    Const
        CONST_NINGUNO = 'Ninguno';
    Var
        SP: TAdoStoredProc;
    Begin
        Try
            Combo.Items.Clear;
            Combo.Items.Add(CONST_NINGUNO, 0);
            Try
                SP := TADOStoredProc.Create(nil);
                SP.Connection := DMConnections.BaseCAC;
                SP.ProcedureName := 'ObtenerMotivosDesconocePatente';
                SP.Open;
                while not sp.Eof do begin
                    Combo.Items.Add(SP.FieldByName('Descripcion').AsString,SP.FieldByName('CodigoMotivoDesconocePatente').AsInteger);
                    SP.Next;
                end;
                SP.Close;
                Combo.ItemIndex := 0;
            Except
            End;
        Finally
            FreeAndNil(SP);
        End;
    End;

Resourcestring
    MSG_ERROR_INIT =  'Error al inicializar Reclamo Cuenta';
    MSG_ERROR = 'Error';
Const
    CONST_NINGUNO = 'Ninguno';
Var
    Sz: TSize;
begin
    FTipoOrdenServicio := TipoOrdenServicio;
    FCodigoConvenio := CodigoConvenio;
    FIndiceVehiculo := IndiceVehiculo;
    PageControl.ActivePageIndex := 0;
    try
        //Centro el form
        CenterForm(Self);
        //ajusto el form al tama�o disponible
        SZ := GetFormClientSize(Application.MainForm);
	      SetBounds(0, 0, SZ.cx, sz.cy);
        //titulo
        Caption := QueryGetValue(DMConnections.BaseCAC, Format(
                    'select dbo.ObtenerDescripcionTipoOrdenServicio (%d)',
              		  [TipoOrdenServicio]));
    		FrameContactoReclamo1.CodigoConvenio := CodigoConvenio;
    		FrameContactoReclamo1.IndiceVehiculo := IndiceVehiculo;
        spObtenerDatosCuenta.Parameters.Refresh;
        spObtenerDatosCuenta.Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
        spObtenerDatosCuenta.Parameters.ParamByName('@IndiceVehiculo').Value := IndiceVehiculo;
        spObtenerDatosCuenta.Open;
        case TipoOrdenServicio of
            301, 302:
        				begin
          					PnlPatenteDeconocida.Visible := False;
          					LblPatenteTag.Caption := STR_TELEVIA + ': ';
          					FPatenteDesconocida := '';
                    TxtPatenteTag.Text := SpObtenerDatosCuenta.FieldByName('Etiqueta').AsString;
                    FContextMark := SpObtenerDatosCuenta.FieldByName('ContextMark').AsInteger;
                    FContractSerialNumber := SpObtenerDatosCuenta.FieldByName('ContractSerialNumber').AsVariant; // Revision 1
                    ActiveControl := TxtDetalle;
                end;
            211:
                begin
                    //Reclamo desconoce Patente
          					PnlPatenteDeconocida.Visible := True;
          					LblPatenteTag.Caption := STR_PATENTE + ': ';
          					FPatenteDesconocida := spObtenerdatoscuenta.FieldByName('Patente').AsString;
                    txtPatenteTag.Text := FPatenteDesconocida;
                    FContextMark := 0;
                    FContractSerialNumber := 0;
                    spObtenerComprobantesxConvenio.Parameters.Refresh;
                    spObtenerComprobantesxConvenio.Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
                    spObtenerComprobantesxConvenio.Open;
	          				cbComprobante.Items.Add(CONST_NINGUNO, 0);
                    while not spObtenerComprobantesxConvenio.Eof do begin
                        cbComprobante.Items.Add(spObtenerComprobantesxConvenio.fieldbyname('DescTipoComprobante').asstring+' - '+spObtenerComprobantesxConvenio.fieldbyname('NumeroComprobante').asstring,spObtenerComprobantesxConvenio.fieldbyname('TipoComprobante').asstring+','+spObtenerComprobantesxConvenio.fieldbyname('NumeroComprobante').asstring);
                        spObtenerComprobantesxConvenio.Next;
                    end;
                    spObtenerComprobantesxConvenio.Close;
                    ActiveControl := txtPatenteFaltante;
        				end;
        end;
        spObtenerdatoscuenta.Close;

    		Result := FrameContactoReclamo1.Inicializar and
                          FrameCompromisoOrdenServicio1.Inicializar(5) and
                                  FrameSolucionOrdenServicio1.Inicializar and
                                          FrameRespuestaOrdenServicio1.Inicializar and
                                        //Rev.2 / 29-Junio-2010 / Nelson Droguett Sierra-----------------
                                        	FrameConcesionariaReclamoOrdenServicio1.Inicializar and
                                            	FrameSubTipoReclamoOrdenServicio1.Inicializar;
                                        //---------------------------------------------------------------


        if Result then FrameContactoReclamo1.CodigoConvenio := CodigoConvenio;

    except
        on E: Exception do begin
			      MsgBoxErr(MSG_ERROR_INIT, E.Message, MSG_ERROR, MB_ICONSTOP);
            Result := False;
        end;
    end;

    //termino de inicializar
    FrameFuenteReclamoOrdenServicio1.Inicializar;
    CargarMotivosDesconocePatente(cbMotivo);
    FrameNumeroOrdenServicio1.Visible := False;
    //No estoy Editando
    FEditando := False;
    CbMotivo.Value := 0;
    //Pongo una fecha comprometida por defecto en funcion del reclamo
    if FrameCompromisoOrdenServicio1.FechaCompromiso = NULLDATE then FrameCompromisoOrdenServicio1.FechaCompromiso := ObtenerFechaComprometidaOS(now, FTipoOrdenServicio, FrameFuenteReclamoOrdenServicio1.FuenteReclamo, framecompromisoOrdenServicio1.Prioridad);
    AddOSForm(Self);
end;

{-----------------------------------------------------------------------------
  Function Name: InicializarEditando
  Author:   DDeMarco
  Date Created:
  Description:  Modifico Reclamo Cuenta que ya existe
  Parameters: CodigoOrdenServicio: Integer
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFormReclamoCuenta.InicializarEditando(CodigoOrdenServicio : Integer) : Boolean;

    //form read Only
    Procedure ReadOnly;
    begin
      Dbltransitos.PopupMenu := Nil;
      TxtPatenteFaltante.Enabled := False;
      Efecha.Enabled := False;
      Ehora.Enabled := False;
      CBMotivo.Enabled := False;
      CBComprobante.Enabled := False;
      //comunes
      FrameContactoReclamo1.Deshabilitar;
      FrameCompromisoOrdenServicio1.Enabled := False;
      FrameSolucionordenServicio1.Enabled := False;
      FrameRespuestaOrdenServicio1.Deshabilitar;
      Txtdetalle.ReadOnly := True;
      TxtDetalleSolucion.ReadOnly := True;
      CKRetenerOrden.Visible := False;
      Btncancelar.Caption := 'Salir';
      Aceptarbtn.Visible := False;
    end;

    Function ObtenerDatosTransitos(CodigoOrdenServicio : Integer) : Boolean;

        Function ObtenerTransitosAceptadosCliente(CodigoOrdenServicio : Integer) : Boolean;
        var
            NumCorrCa : String;
            Rechaza : Boolean;
        begin
           FListaTransitosCliente.Clear;
           with spObtenerTransitosReclamo do begin
              DisableControls;
              First;
              while not Eof do begin
                  NumCorrCa:=fieldbyname('NumCorrCa').AsString;
                  if QueryGetValue(DMconnections.BaseCAC,'SELECT dbo.ObtenerRechazaOSTransito('+ IntToStr(CodigoOrdenServicio) + ',' + NumCorrCa + ')'  ) = 'True' then begin
                      Rechaza := True;
                  end else begin
                      Rechaza := False;
                  end;
                  if not Rechaza then ListaTransitosInclude(FListaTransitosCliente,NumCorrCa);
                  Next;
              end;
              EnableControls;
            end;
            Result := True;
        end;

        Function ObtenerResolucionEmpresa(CodigoOrdenServicio : Integer) : Boolean;
        var
            NumCorrCA : String;
            CodigoResolucion : String;
            Bloque : String;
        begin
           FListaTransitosEmpresa.Clear;
           with spObtenerTransitosReclamo do begin
              DisableControls;
              First;
              while not Eof do begin
                  NumCorrCA := FieldByName('NumCorrCa').AsString;
                  CodigoResolucion := FieldByName('CodigoResolucionReclamoTransito').AsString;
                  CodigoResolucion := IIF(CodigoResolucion = '', '1', CodigoResolucion);
                  Bloque := NumCorrCa + ';' + CodigoResolucion;
                  if CodigoResolucion <> '1' then ListaTransitosInclude(FListaTransitosEmpresa, Bloque);
                  Next;
              end;
              EnableControls;
            end;
            Result := True;
        end;

    begin
        DataSource.DataSet:= SpObtenerTransitosReclamo;
        with spObtenerTransitosReclamo do begin
            Close;
            Parameters.ParamByName('@CodigoOrdenServicio').Value:= CodigoOrdenServicio;
            Parameters.ParamByName('@Enumeracion').Value:= NULL;
            Open;
        end;

        ObtenerTransitosAceptadosCliente(CodigoOrdenServicio);
        ObtenerResolucionEmpresa(CodigoOrdenServicio);

        Result := True;
    end;

var
    TipoOrdenServicio: Integer;
    CodigoConvenio: Integer;
    IndiceVehiculo: Integer;
    Estado: String;
begin
    //asigno codigoordenservicio a una variable global
    FCodigoOrdenServicio := CodigoOrdenServicio;

    //asigo los parametros
    with spObtenerOrdenServicioCuenta.parameters do begin
        Refresh;
        ParamByName('@CodigoOrdenServicio').Value := CodigoOrdenServicio;
    end;

    //abro la consulta
    Result := OpenTables([spObtenerOrdenServicioCuenta]);

    //si no puede abrir la consulta sale
    if not Result then Exit;

    //asigno los valores
    with spObtenerOrdenServicioCuenta do begin
        TipoOrdenServicio:= FieldByName('TipoOrdenServicio').AsInteger;
        CodigoConvenio:= FieldByName('CodigoConvenio').AsInteger;
        IndiceVehiculo:= FieldByName('IndiceVehiculo').AsInteger;
    end;

    // Cargamos los datos
    Result := Inicializar(
                            TipoOrdenServicio,
                            CodigoConvenio,
                            IndiceVehiculo
                          );

    //cargo los frames
    FrameCompromisoOrdenServicio1.LoadFromDataset(spObtenerOrdenServicioCuenta);
    FrameContactoReclamo1.LoadFromDataset(spObtenerOrdenServicioCuenta);
    FrameSolucionOrdenServicio1.LoadFromDataset(spObtenerOrdenServicioCuenta);
    FrameFuenteReclamoOrdenServicio1.LoadFromDataset(spObtenerOrdenServicioCuenta);
    FrameRespuestaOrdenServicio1.LoadFromDataset(spObtenerOrdenServicioCuenta);

    //cargo la grilla con los transitos objetados
    ObtenerDatosTransitos(CodigoOrdenServicio);

    FrameHistoriaOrdenServicio.Inicializar(CodigoOrdenServicio);

    //Obtengo patente faltante, comprobante, detalle ,fuente del reclamo
    //y el Estado de la OS
    with spObtenerOrdenServicioCuenta do begin
        TxtPatenteFaltante.Text := Trim(FieldByName('PatenteFaltante').AsString);
        Cbcomprobante.ItemIndex:= Iif (CbComprobante.Items.IndexOf(FieldByName('DescTipoComprobante').Asstring+' - '+FieldByName('NumeroComprobante').Asstring) = -1 , 0 , cbcomprobante.Items.IndexOf(FieldByName('DescTipoComprobante').Asstring + ' - ' + FieldByName('NumeroComprobante').Asstring));
        TxtDetalle.Text := FieldByName('ObservacionesSolicitante').AsString;
        TxtDetalleSolucion.Text := FieldByName('ObservacionesEjecutante').AsString;
        EFecha.Date := FieldByName('FechaHoraDesconoce').AsDateTime;
        EHora.Time := FieldByName('FechaHoraDesconoce').AsDateTime;
        CbMotivo.Value := FieldByName('CodigoMotivoDesconocePatente').Asinteger;
        Estado := FieldByName('Estado').AsString;
    End;

    // Listo
    spObtenerOrdenServicioCuenta.Close;

    //si esta finalizada solo se puede leer
    if ((Estado = 'C') or (Estado = 'T')) then begin
        ReadOnly;
    end;

    //termino de inicializar
    FrameNumeroOrdenServicio1.Visible := True;
    FrameNumeroOrdenServicio1.Inicializar(IntToStr(CodigoOrdenServicio));

    //por Default no retengo la orden de servicio
    CKRetenerOrden.Checked:=false;

    //la fuente del reclamo no puede ser cambiada despues de creada la OS
    FrameFuenteReclamoOrdenServicio1.Deshabilitar;

    //Estoy Editando
    FEditando := True;
end;


{-----------------------------------------------------------------------------
  Function Name: InicializarSolucionando
  Author:   DDeMarco
  Date Created:
  Description:  Soluciono un Reclamo Cuenta
  Parameters: CodigoOrdenServicio: Integer
  Return Value: Boolean
-----------------------------------------------------------------------------}
Function TFormReclamoCuenta.InicializarSolucionando(CodigoOrdenServicio : Integer) : Boolean;
begin
    Result := InicializarEditando(CodigoOrdenServicio);
    if Result then begin
        PageControl.ActivePageIndex := 1;
        FrameSolucionOrdenServicio1.Estado := 'T';
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: GetCodigoOrdenServicio
  Author:    lgisuk
  Date Created: 11/04/2005
  Description: obtengo el codigoordenservicio
  Parameters: None
  Return Value: integer
-----------------------------------------------------------------------------}
Function TFormReclamoCuenta.GetCodigoOrdenServicio : Integer;
begin
    try
        Result := StrToInt(FrameNumeroOrdenServicio1.CodigoOrdenServicio);
    except
        Result := 0;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ListaTransitosInclude
  Author:    lgisuk
  Date Created: 14/04/2005
  Description: agrego un valor a la lista de transitos
  Parameters: valor:string
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFormReclamoCuenta.ListaTransitosInclude( Lista : TStringList ; Valor : String ) : Boolean;
begin
    Result := False;
    try
        if not ListaTransitosIn(Lista , Valor) then Lista.Add(Valor);
        Result := True;
    except
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ListaTransitosExclude
  Author:    lgisuk
  Date Created: 14/04/2005
  Description: quito un valor a la lista de Transitos
  Parameters: valor:string
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFormReclamoCuenta.ListaTransitosExclude(Lista : TStringList ; Valor : String) : Boolean;
var
    Index : Integer;
begin
    Result := False;
    try
        Index := Lista.IndexOf(Valor);
        if Index <> -1 then Lista.Delete(Index);
        Result := True;
    except
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ListaTransitosIn
  Author:    lgisuk
  Date Created: 14/04/2005
  Description: Devuelve si un valor esta en la lista transitos
  Parameters: valor:string
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFormReclamoCuenta.ListaTransitosIn( Lista : TStringList ; Valor : String ) : Boolean;
begin
    if Lista.IndexOf(Valor) = -1 then Result := False
    else Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: ListaTransitosEmpresaIn
  Author:    lgisuk
  Date Created: 19/05/2005
  Description: busca un transito, se fija si esta y trae su resolucion
  Parameters: valor:string; var Resolucion:integer
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFormReclamoCuenta.ListaTransitosEmpresaIn(Valor : String; Var Resolucion : Integer) : Boolean;
var
    I : Integer;
begin
    Result := False;
    Resolucion := 1;
    i := 0;
    while i <= FListaTransitosEmpresa.Count-1 do
    begin
        if ParseParamByNumber(FListaTransitosEmpresa.Strings[i],1,';') = Valor then begin
            Resolucion := StrToInt(ParseParamByNumber(FListaTransitosEmpresa.Strings[I],2,';'));
            I := FListaTransitosEmpresa.Count;
            Result := True;
        end;
        Inc(I);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: CargarTransitos
  Author:    lgisuk
  Date Created: 17/05/2005
  Description: Cargo los transitos desde la fecha y hora que desconoce
               la patente.
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
Function TFormReclamoCuenta.CargarTransitos : Boolean;

    //obtiene la fecha y hora
    Function ObtenerFechaHoraDesconoce : TDateTime;
    begin
        Result:= NULLDATE;
        try
            Result := StrToDateTime(DateToStr(EFecha.Date) + ' ' + TimeToStr(EHora.Time));
        except
        end;
    end;

var
    FechaHora : TDateTime;
begin
    //obtengo la fecha y hora desde la cual desconoce la patente
    FechaHora:= ObtenerFechaHoraDesconoce;
    //traigo todos los transitos desde esa fecha
    with SpObtenerTransitosReclamo do begin
        Close;
        ProcedureName := 'ObtenerTransitosDesconocePatente';
        Parameters.Refresh;
        Parameters.ParamByName('@CodigoConvenio').Value := FCodigoConvenio;
        Parameters.ParamByName('@Patente').Value := FPatenteDesconocida;
        Parameters.ParamByName('@FechaHora').Value := Iif(FechaHora = NULLDATE, NULL, FechaHora);
        Open;
    end;

    Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: EfechaChange
  Author:    lgisuk
  Date Created: 17/05/2005
  Description: si la fecha o la hora cambia
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoCuenta.EfechaChange(Sender: TObject);
begin
    CargarTransitos;
end;

{-----------------------------------------------------------------------------
  Function Name: DBLTransitosDrawText
  Author:    lgisuk
  Date Created: 16/05/2005
  Description:
  Parameters: Sender: TCustomDBListEx;Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;var Text: String; var TxtRect: TRect; var ItemWidth: Integer;var DefaultDraw: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoCuenta.DBLTransitosDrawText(Sender: TCustomDBListEx;Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;var Text: String; var TxtRect: TRect; var ItemWidth: Integer;var DefaultDraw: Boolean);

    {-----------------------------------------------------------------------------
      Function Name: DibujarBitmap
      Author:    lgisuk
      Date Created: 29/06/2005
      Description: Dibujo el bitmap seleccionado en la columna de la grilla
      Parameters: Grilla:TCustomDBlistEx;ImageList:TimageList;numero:integer
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function DibujarBitmap(Grilla: TCustomDBlistEx; ImageList: TimageList; IndiceImg: Integer) : Boolean;
    var
        Bmp: TBitMap;
    begin
        Bmp := TBitMap.Create;
        try
            ImageList.GetBitmap(IndiceImg, Bmp);
            Grilla.Canvas.Draw((Rect.Left + Rect.Right - Bmp.Width) div 2, Rect.Top, Bmp);
            Result := true;
        finally
            Bmp.Free;
        end;
    end;

var
    I : Integer;
    NumCorrCA : String;
    Resolucion : Integer;
begin
    //FechaHora
    if Column = DblTransitos.Columns[3] then begin
       if Text <> '' then Text := FormatDateTime('dd/mm/yyyy hh:nn', spObtenerTransitosReclamo.FieldByName('FechaHora').AsDateTime);
    end;

    //muestra si el cliente rechazo un transito o no
  	if Column = DblTransitos.Columns[4] then begin
        //verifica si esta seleccionado o no
        I := Iif(ListaTransitosIn(FListaTransitosCliente, spObtenerTransitosReclamo.FieldByName('NumCorrCa').Asstring), 0, 1);
        //Dibujo el icono correspondiente
        DibujarBitmap(dbltransitos, lncheck, i);
  	end;

    //Muestra la resolucion de la empresa con respecto al transito
  	if Column = dbltransitos.Columns[5] then begin
        //Obtengo el numero de transito
        NumCorrCA := spObtenerTransitosReclamo.FieldByName('NumCorrCa').AsString;
        //Busco el transito. Si esta me devuelve su resolucion actual.
        if not ListaTransitosEmpresaIn(NumCorrCa, Resolucion) then begin
            Text := 'Pendiente';
        end else begin
            if Resolucion = 1 then Text := 'Pendiente';
            if Resolucion = 2 then Text := 'Aceptado';
            if Resolucion = 3 then Text := 'Denegado';
        end;
  	end;

    //si el transito tiene imagen le crea un link para que se pueda acceder a verla
    if Column = dblTransitos.Columns[6] then begin
        //Muestro el dibujito de una camara cuando hay una foto del transito
        I := Iif(spObtenerTransitosReclamo.FieldByName('RegistrationAccessibility').AsInteger > 0, 1, 0);
        //Dibujo el icono correspondiente
        DibujarBitmap(dbltransitos, ilCamara, I);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: DBLTransitosCheckLink
  Author:    lgisuk
  Date Created: 18/05/2005
  Description: permito hacer click en el link o no
  Parameters: Sender: TCustomDBListEx;Column: TDBListExColumn; var IsLink: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoCuenta.DBLTransitosCheckLink(Sender: TCustomDBListEx;Column: TDBListExColumn; var IsLink: Boolean);
begin
    //permito hacer click en el link o no
    if  Column = dblTransitos.Columns[6] then begin
        IsLink := (spObtenerTransitosReclamo['RegistrationAccessibility'] > 0);
    end;
end;


{-----------------------------------------------------------------------------
  Function Name: DBLTransitosLinkClick
  Author:    lgisuk
  Date Created: 16/05/2005
  Description:
  Parameters: Sender: TCustomDBListEx;Column: TDBListExColumn
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoCuenta.DBLTransitosLinkClick(Sender: TCustomDBListEx;Column: TDBListExColumn);
Var
  	Numcorrca: String;
    Estado: String;
begin

    //Mostrar Imagen del Transito
    if Column = dblTransitos.Columns[6] then begin
        //Si hay imagen
        if spObtenerTransitosReclamo.FieldByName('RegistrationAccessibility').AsInteger > 0 then begin
            //La muestra
            //MostrarVentanaImagen(Self,spObtenerTransitosReclamo['NumCorrCA'], spObtenerTransitosReclamo['FechaHora']);    // SS_1091_CQU_20130516
            MostrarVentanaImagen(Self,                                                                                      // SS_1091_CQU_20130516
                spObtenerTransitosReclamo['NumCorrCA'],                                                                     // SS_1091_CQU_20130516
                spObtenerTransitosReclamo['FechaHora'],                                                                     // SS_1091_CQU_20130516
                spObtenerTransitosReclamo['EsItaliano']);                                                                   // SS_1091_CQU_20130516
        end;
    end;

    //selecciono/desselecciono transito  cliente
    if Dbltransitos.Columns[4].IsLink = False then Exit;

    //Si la OS ya esta cerrada evito que hagan click en el link
    Estado := FrameSolucionOrdenServicio1.cbEstado.value;
    if ((Estado = 'C') or (Estado = 'T')) then Exit;

    if Column = dbltransitos.Columns[4] then begin
    	Numcorrca := spObtenerTransitosReclamo.FieldByName('NumCorra').AsString;
	    if ListaTransitosin(FListaTransitosCliente, NumCorrCa) then ListaTransitosExclude(FListaTransitosCliente, NumCorrCa)
	    else ListaTransitosInclude(FListaTransitosCliente, NumCorrCa);
  	    Sender.Invalidate;
        if spObtenerTransitosReclamo.Active then spobtenertransitosreclamo.Requery;
    end;

end;

{-----------------------------------------------------------------------------
  Function Name: FormKeyPress
  Author:    DDeMarco
  Date Created:
  Description: si tocan escape permito cancelar
  Parameters: Sender: TObject; var Key: Char
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoCuenta.FormKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #27 then btnCancelar.Click;
end;

{-----------------------------------------------------------------------------
  Function Name: DBLTransitosContextPopup
  Author:    lgisuk
  Date Created: 19/05/2005
  Description: habilito el menu contextual solo si hay registros
  Parameters: Sender: TObject;MousePos: TPoint; var Handled: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoCuenta.DBLTransitosContextPopup(Sender: TObject;MousePos: TPoint; var Handled: Boolean);
begin
  	if not spObtenerTransitosReclamo.IsEmpty then begin
        Handled := false;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: Mnu_ComunClick
  Author:    lgisuk
  Date Created: 19/05/2005
  Description: Permite cambiar la Resolucion de la empresa
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoCuenta.Mnu_ComunClick(Sender: TObject);
var
  	NumcorrCa : String;
    CodigoResolucionActual : String;
    CodigoResolucionAnterior : Integer;
begin
    //Obtengo codigo de Resolucion Actual
    CodigoResolucionActual := IntToStr((Sender as TMenuItem).Tag);
    //Obtengo numero de transito
    NumcorrCa := spObtenerTransitosReclamo.FieldByName('NumCorrCa').Asstring;
    //Obtengo Resolucion Anterior porque la necesito para localizar el registro
    ListaTransitosEmpresaIn(NumCorrCa, CodigoResolucionAnterior);
    //Elimino Resolucion Anterior porque solo puede existir una
    ListaTransitosExclude(FListaTransitosEmpresa, NumCorrCa + ';' + IntToStr(CodigoResolucionAnterior) );
    //Inserto Nueva Resolucion
    ListaTransitosInclude(FListaTransitosEmpresa, NumCorrCa + ';' + CodigoResolucionActual );
    //Refresco la vista
    Dbltransitos.Invalidate;
end;

{-----------------------------------------------------------------------------
  Function Name: ValidarEstadoTransitos
  Author:    lgisuk
  Date Created: 19/05/2005
  Description: valido que todos los transitos incluidos en el reclamo
               hallan sido analizados y resueltos por la empresa
               antes de dar por finalizado el reclamo
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
Function TFormReclamoCuenta.ValidarEstadoTransitos : Boolean;
var
    CodigoResolucion : Integer;
begin
    Result := True;
    //si el sp no esta abierto salgo
    if spObtenerTransitosReclamo.Active = False then Exit;
    with spObtenerTransitosReclamo do begin
        DisableControls;
        First;
        while not Eof do begin
            ListaTransitosEmpresaIn(FieldByName('NumCorrCa').AsString, CodigoResolucion);
            //basta que uno este sin resolver no permito Guardar
            if CodigoResolucion = 1 then begin
                Result := False;
            end;
            Next;
        end;
        EnableControls;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: AceptarBTNClick
  Author:    DDeMarco
  Date Created:
  Description:  acepta el reclamo
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoCuenta.AceptarBTNClick(Sender: TObject);

    {-----------------------------------------------------------------------------
      Function Name: Validar
      Author:    lgisuk
      Date Created: 16/06/2005
      Description:  Valido el contenido del reclamo antes de guardarlo
      Parameters: None
      Return Value: Boolean
    -----------------------------------------------------------------------------}
    Function Validar : Boolean;
    resourcestring
        MSG_ERROR_DET       = 'Debe indicar alg�n detalle para el reclamo';
        MSG_ERROR_FUENTE    = 'Debe indicar la fuente del reclamo';
        MSG_ERROR_FECHA     = 'La Fecha de Compromiso Interna o Externa '+ CRLF +
                              'debe ser menor a la Fecha Comprometida con el Cliente';
        MSG_ERROR_TRANSITOS = 'Todos los Tr�nsitos deben estar resueltos para cerrar el reclamo';
    begin
        Result := False;

        //Validamos fuente de reclamo
        if FEditando = False then begin
            if not FrameFuenteReclamoOrdenServicio1.Validar then Exit;
        end;

        //si es un reclamo que va a ser cancelado, no valida el resto de los
        //campos para no forzar al usuario a que los complete y asi pueda
        //salir rapidamente.
        if FrameSolucionOrdenServicio1.Estado = 'C' then begin
            Result := True;
            Exit;
        end;

        //Validamos identificacion de usuario y contacto
        if not FrameContactoReclamo1.Validar then Exit;

        //Validamos compromiso con el cliente
        if FEditando = False then begin
            if not FrameCompromisoOrdenServicio1.Validar then Exit;
        end;

        //valido que todos los transitos esten resueltos
        if (FrameSolucionOrdenServicio1.Estado = 'T') then begin
            if not ValidarEstadoTransitos then begin
                PageControl.ActivePageIndex := 0;
                MsgBoxBalloon(MSG_ERROR_TRANSITOS, STR_ERROR, MB_ICONSTOP, dbltransitos);
                DblTransitos.SetFocus;
                Exit;
            end;
        end;

        //Validamos detalle del reclamo
        if Trim(txtDetalle.Text) = '' then begin
            PageControl.ActivePageIndex := 0;
            MsgBoxBalloon(MSG_ERROR_DET, STR_ERROR, MB_ICONSTOP, txtDetalle);
            TxtDetalle.SetFocus;
            Exit;
        end;

        //Validamos progreso / solucion
        if not FrameSolucionOrdenServicio1.Validar then Exit;
        //Si es un reclamo pendiente de respuesta Interna o Externa
        if ((FrameSolucionOrdenServicio1.Estado = 'I') or (FrameSolucionOrdenServicio1.Estado = 'E')) then begin
            //Valido que la fecha comprometida interna/externa
            //sea menor a la fecha comprometida con el cliente
            if (FrameCompromisoOrdenServicio1.txtFechaCompromiso.Date < FrameSolucionOrdenServicio1.EFechaCompromiso.Date) then begin
                MsgBoxBalloon(MSG_ERROR_FECHA, STR_ERROR, MB_ICONSTOP, FrameSolucionOrdenServicio1.EFechaCompromiso);
                FrameSolucionOrdenServicio1.EFechaCompromiso.SetFocus;
                Exit;
            end;
        end;


        if not FrameConcesionariaReclamoOrdenServicio1.Validar then Exit ;      //SS-1006-NDR-20120802

        Result := True;
    end;

    //Obtengo Fecha/Hora desconoce
    Function ObtenerFechaHoraDesconoce : TDateTime;
    begin
        Result := NULLDATE;
        try
            Result := StrtoDateTime(DateToStr(EFecha.Date)+' '+TimeToStr(EHora.Time));
        except
        end;
    end;

Var
  	OS: Integer;
    NumeroComprobante : Integer;
    TipoComprobante , NC : String;
    Ok: Boolean;
    CodigoResolucion: Integer;
begin

    //Valido el reclamo antes de guardarlo
    if not Validar then exit;

    // Guardamos
    //Rev.2 / 29-Junio-2010 / Nelson Droguett Sierra ---------------------------------
    DMConnections.BaseCAC.BeginTrans;
    OS := GuardarOrdenServicioBase(FCodigoOrdenServicio,
                                   FTipoOrdenServicio,
                                   FrameFuenteReclamoOrdenServicio1.FuenteReclamo,
                                   FrameContactoReclamo1,
                                   framecompromisoOrdenServicio1,
                                   FrameSolucionOrdenServicio1,
                                   txtDetalle.Text,
                                   txtDetalleSolucion.Text,
                                   FrameRespuestaOrdenServicio1.ObservacionesRespuesta,
                                   FrameRespuestaOrdenServicio1.ObservacionesComentariosCliente,
                                   FrameRespuestaOrdenServicio1.Notificado,
                                   FrameRespuestaOrdenServicio1.Conforme,
                                   FrameConcesionariaReclamoOrdenServicio1,
                                   FrameSubTipoReclamoOrdenServicio1);
    //FinRev.2------------------------------------------------------------------------
    if OS <= 0 then begin
        DMConnections.BaseCAC.RollbackTrans;
        Exit;
    end;

    // Especifico de Cuenta
    TipoComprobante := ParseParamByNumber(CbComprobante.Value,1,',');
    NC := ParseParamByNumber(CbComprobante.Value,2,',');
    if NC = '' then begin
        NumeroComprobante := StrToInt('0');
    end else begin
        NumeroComprobante := StrToInt(NC);
    end;
    if not GuardarOrdenServicioCuenta(OS, FPatenteDesconocida, txtPatenteFaltante.Text, ObtenerFechaHoraDesconoce, Cbmotivo.value, TipoComprobante, NumeroComprobante, FContextMark, FContractSerialNumber) then begin
        DMConnections.BaseCAC.RollbackTrans;
        Exit;
    end;

    //Elimino los transitos
    QueryExecute(DMConnections.BaseCAC,'DELETE FROM OrdenesServicioTransito Where CodigoOrdenServicio = ' + IntToStr(OS));

    // Especifico de Transitos
    Ok := True;
    with spObtenerTransitosReclamo do begin
        DisableControls;
        //si hay al menos un transito
        if not Eof then begin
            First;
            while not Eof do begin
                ListaTransitosEmpresaIn(Fieldbyname('NumCorrCa').AsString, CodigoResolucion);
                //guardo cada transito en la bd
                if GuardarOrdenServicioTransito(OS, FieldByName('NumCorrCa').AsString, not (ListaTransitosIn(FListaTransitosCliente, FieldByName('NumCorrCa').AsString)), CodigoResolucion) = False then Ok := False;
                next;
            end;
        end;
        EnableControls;
    end;
    if not Ok then begin
        DMConnections.BaseCAC.RollbackTrans;
        Exit;
    end;

    // Listo
    DMConnections.BaseCAC.CommitTrans;

    //si es un reclamo nuevo
    if FEditando = False then begin

        //informo al operador el numero de Reclamo que se genero
        InformarNumeroReclamo(inttostr(OS));
        DesbloquearOrdenServicio(OS);
    end;

    Close;
end;

{-----------------------------------------------------------------------------
  Function Name: btnCancelarClick
  Author:    lgisuk
  Date Created: 11/04/2005
  Description: cierro el formulario
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoCuenta.btnCancelarClick(Sender: TObject);
begin
    Close;
end;

{-----------------------------------------------------------------------------
  Function Name: FormDestroy
  Author:    lgisuk
  Date Created: 11/04/2005
  Description:  remuevo al form de la lista de creados
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoCuenta.FormDestroy(Sender: TObject);
begin
    RemoveOSform(Self);
end;

{-----------------------------------------------------------------------------
  Function Name: FormClose
  Author:    lgisuk
  Date Created: 05/04/2005
  Description:  desbloqueo la orden de servicio antes de salir
  Parameters: Sender: TObject;var Action: TCloseAction
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormReclamoCuenta.FormClose(Sender: TObject;var Action: TCloseAction);
begin
    //si se esta modificando un reclamo
    if FEditando = True then begin
        //verifico si desea desbloquear el reclamo
        if CKRetenerOrden.Checked = False then begin
            //Desbloqueo la orden de servicio
            DesbloquearOrdenServicio(FCodigoOrdenServicio);
        end;
    end;
    //lo libero de memoria
    Action := caFree;
end;


initialization
    FListaTransitosCliente := TStringList.Create;
    FListaTransitosEmpresa := TStringList.Create;
finalization
    FreeAndNil(FListaTransitosCliente);
    FreeAndNil(FListaTransitosEmpresa);
end.
