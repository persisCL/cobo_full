unit FrmConfigReporteComunicaciones;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, OPButtons, DmiCtrls, validate, Dateedit,
  ExtCtrls, peaprocs, util, DMConnection, DB, ADODB, UtilDb;

type
  TFrm_ConfigReporteComunicaciones = class(TForm)
    btn_Aceptar: TOPButton;
    btn_Cancelar: TOPButton;
    Label1: TLabel;
    cb_Usuarios: TComboBox;
    Bevel1: TBevel;
    qry_Usuarios: TADOQuery;
    gb_fechas: TGroupBox;
    Label7: TLabel;
    cb_Fechas: TComboBox;
    rd_FechasPredefinidas: TRadioButton;
    gb_rango: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    rd_Rango: TRadioButton;
    dt_FechaDesde: TDateEdit;
    dt_FechaHasta: TDateEdit;
    procedure btn_CancelarClick(Sender: TObject);
    procedure btn_AceptarClick(Sender: TObject);
    procedure RangoFechas(Sender: TObject);
  protected
    function getFechadesde: TDateTime;
    function getFechaHasta: TDateTime;    
  private
    { Private declarations }
  public
    { Public declarations }
    property FechaDesde: TDateTime read getFechaDesde;
    property FechaHasta: TDateTime read getFechaHasta;
    function inicializa: boolean;
  end;

var
  Frm_ConfigReporteComunicaciones: TFrm_ConfigReporteComunicaciones;

implementation

{$R *.dfm}

procedure TFrm_ConfigReporteComunicaciones.btn_CancelarClick(Sender: TObject);
begin
	Close;
end;

procedure TFrm_ConfigReporteComunicaciones.btn_AceptarClick(Sender: TObject);
begin
	if ValidateControls(
	  [dt_FechaDesde],
	  [(dt_FechaDesde.date = nullDate) or (dt_FechaHasta.date = nulldate)
		or (dt_FechaDesde.date <= dt_FechaHasta.date)],
	  	'Reporte de Comunicaciones',
		['El rango de Fechas ingresado es incorrecto']) then ModalResult := mrOk;
end;

function TFrm_ConfigReporteComunicaciones.inicializa: boolean;
begin
	Result := OpenTables([qry_Usuarios]);
    //Combo Usuarios.
    cb_Usuarios.Items.Clear;
  	cb_Usuarios.Items.Add(padR('Todos', 300, ' ')+ '0');
    while not qry_Usuarios.Eof do begin
    	cb_Usuarios.Items.Add(
		    PadR(Trim(qry_Usuarios.FieldByName('NombreCompleto').AsString), 300, ' ') +
            Trim(qry_Usuarios.FieldByName('CodigoUsuario').AsString)
        );
        qry_Usuarios.Next;
    end;
    cb_Usuarios.ItemIndex := 0;
    qry_Usuarios.Close;

    CargarFechasParaReportes(cb_Fechas);

    //Fechas Predefinidas.
end;

function TFrm_ConfigReporteComunicaciones.getFechadesde: TDateTime;
begin
	if rd_FechasPredefinidas.checked then

    	Result := EncodeDate(
        		Ival(Copy(cb_Fechas.Text, 201, 4)),
                Ival(Copy(cb_Fechas.Text, 205, 2)),
                Ival(Copy(cb_Fechas.Text, 207, 2)))
	else
    	result:= dt_FechaDesde.Date;
end;

function TFrm_ConfigReporteComunicaciones.getFechaHasta: TDateTime;
begin
	if rd_FechasPredefinidas.checked then
    	Result := EncodeDate(
        		Ival(Copy(cb_Fechas.Text, 209, 4)),
                Ival(Copy(cb_Fechas.Text, 213, 2)),
                Ival(Copy(cb_Fechas.Text, 215, 2)))
	else
		result := dt_FechaHasta.Date;
end;

procedure TFrm_ConfigReporteComunicaciones.RangoFechas(Sender: TObject);
begin
	if Sender = rd_rango then
		rd_fechasPredefinidas.Checked := not rd_rango.Checked
    else
		rd_rango.Checked := not rd_fechasPredefinidas.Checked;
end;

end.
