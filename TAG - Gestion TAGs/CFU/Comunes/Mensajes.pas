{-----------------------------------------------------------------------------
 File Name: Mensajes
 Author:
 Date Created:
 Language: ES-AR
 Description:

Revision : 1
Date: 25/02/2009
Author: mpiazza
Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
	los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
unit Mensajes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ComCtrls, ADODB;

type
  TFormMensajes = class(TForm)
    Mensaje: TRichEdit;
    lblComentario: TLabel;
    btnAceptar: TButton;
    procedure btnAceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function Inicializa(Conn: TADOConnection; CodigoMensaje: integer): boolean;
  end;

var
  FormMensajes: TFormMensajes;

implementation

{$R *.dfm}

procedure TFormMensajes.btnAceptarClick(Sender: TObject);
begin
    Close;
end;
{-----------------------------------------------------------------------------
  Function Name: Inicializa
  Author:
  Date Created:  /  /
  Description:
  Parameters: Conn: TADOConnection; CodigoMensaje: integer
  Return Value: boolean;

  Revision : 1
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function TFormMensajes.Inicializa(Conn: TADOConnection; CodigoMensaje: integer): boolean;
Var Qry: TADOQuery;
begin
    ModalResult := mrCancel;
	Qry := TAdoQuery.Create(nil);
	try
        with Qry do
        begin
            Connection	:= Conn;
		    SQL.Text	:= 'SELECT * FROM Mensajes  WITH (NOLOCK) where codigomensaje = :xCodigoMensaje';
            Parameters.ParamByName('xCodigoMensaje').Value := CodigoMensaje;
		    Open;
            if not EOF then begin
                Self.Caption := FieldByName('TituloMensaje').AsString;
                lblComentario.Caption := FieldByName('Comentario').AsString;
                Mensaje.Text := FieldByName('Descripcion').AsString;
                result := true;
                ModalResult := mrOk;
		    end
            else result := false;
        end;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;


procedure TFormMensajes.FormShow(Sender: TObject);
begin
    btnAceptar.SetFocus;
end;

end.

