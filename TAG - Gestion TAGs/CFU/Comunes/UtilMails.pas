{-----------------------------------------------------------------------------
Author		: Nelson Droguett Sierra (mpiazza)
Date		: 28-Abril-2010
Description	: Utilidades para el envio de mails.
			SS 680: Se agregan a las funciones "ExisteMailPendienteComprobante" y
            "InsertarEMailComprobante" el parametro que indica el tipo de documento
            a considerar (Reporte de Nota de Cobro, Detalle de Transitos y otros
            para m�s adelante.

Revision	: 1
Author		: Nelson Droguett Sierra
Date		: 28-Abril-2010
Description	: Se agrega Funcion "ExisteConvenioAEnviarDetalle"
-----------------------------------------------------------------------------}
unit UtilMails;

interface

uses
    SysUtils, ADODB, utilHTTP, util, Variants, RStrings, DB, eventlog, UtilDB, UtilProc, Windows, ConstParametrosGenerales;


Function ArmarMailSolicitud(ArchivoPlantilla, Apellido, ApellidoMaterno, Nombre, Password, Mensaje: AnsiString;
  Sexo: Char; var ContenidoMail: AnsiString; var error: AnsiString): Boolean;
Function InsertarEMailSalida(Conn: TADOConnection; Asunto, Destino, NombreDestino,
  Contenido: AnsiString; var CodigoEMail: Integer): Boolean;
function ObtenerContenidoMail(Conn: TADOConnection; CodigoContenidoEmail: integer;
            var MensajeEmail: AnsiString; var AsuntoEmail: AnsiString): boolean;
Function InsertarEMailComprobante(const Conn: TADOConnection; const TipoComprobante: string; const NumeroComprobante: integer; const CodigoImpresoraFiscal: Integer;
    const Email: string; const Usuario: string; const Convenio: integer; out DescriError: string): Boolean;
Function ExisteMailPendienteComprobante(const DatabaseConnection: TADOConnection; const TipoComprobante: String; NumeroComprobante: Int64; CodigoImpresoraFiscal: Integer):Boolean;
//Rev.1 / 28-Abril-2010 / Nelson Droguett Sierra
function ExisteConvenioAEnviarDetalle(const DatabaseConnection: TADOConnection; CodigoConvenio: integer; const TipoComprobante: String; NumeroComprobante: Int64 ):Boolean;
function InsertarConvenioAEnviarDetalle(const Conn: TADOConnection; const CodigoConvenio: integer; const TipoComprobante: string; const NumeroComprobante: integer; const Usuario: string;out DescriError: string): Boolean;




implementation


function ObtenerPlantilla(ArchivoPlantilla: AnsiString; var Plantilla: AnsiString; var error: AnsiString): boolean;
var
    Cabecera: string;
begin
    result := false;
    try
        // Obtenemos la plantilla MHT desde el archvo especifico
        Plantilla := FileToString(ArchivoPlantilla);
//        Plantilla := Trim(ArchivoPlantilla);

        if trim(Plantilla) <> '' then begin
            // verifico que realmente sea una plantilla MHT
             // para eso, la cabecera del email esta separada del cuerpo por una linea en blanco
            if pos(CRLF + CRLF, Plantilla) <> 0 then begin
                Cabecera := copy(Plantilla, 1, pos(CRLF + CRLF, Plantilla));
                // que tenga dos lineas en blanco no significa que sea un email en regla, por eso
                // voy a ver si tiene los campos tipicos de una cabecera: content-type
                if pos('Content-Type:', cabecera) = 0 then begin
                    // no es mht, por lo tanto, le agrego una cabecera
                    Plantilla := 'Content-Type: ' +
                      iif((pos('<HTMl>', Plantilla) <> 0) or (pos('<BODY>', Plantilla) <> 0),
                      'text/html;', 'text/plain;') + CRLF + CRLF + Plantilla;
                end;
            end else begin
                // no es mht, por lo tanto, le agrego una cabecera
                Plantilla := 'Content-Type: ' +
                  iif((pos('<HTMl>', Plantilla) <> 0) or (pos('<BODY>', Plantilla) <> 0),
                  'text/html;', 'text/plain;') + CRLF + CRLF + Plantilla;
            end;
            result := true;
        end;
    except
        on e: exception do begin
            Plantilla := '';
            error := e.message;
        end;
    end;
end;


function ArmarMailSolicitud(ArchivoPlantilla, Apellido, ApellidoMaterno, Nombre, Password, Mensaje: AnsiString;
  Sexo: Char; var ContenidoMail: AnsiString; var error: AnsiString): Boolean;
var
    DescripcionSexo: AnsiString;
begin
    result := False;
    ContenidoMail := '';

    // Obtenemos la plantilla desde el archivo
    if ObtenerPlantilla(ArchivoPlantilla, ContenidoMail, error) then begin
        // Obtenemos las descripci�n del Sexo
        if (Sexo = 'M') then DescripcionSexo := FLD_SR
        else if (Sexo = 'F') then DescripcionSexo := FLD_SRA
            else DescripcionSexo := '';

        // Obtenemos la declaraci�n del nombre completo
        Nombre := Format('%s: %s %s, %s', [DescripcionSexo, Apellido, ApellidoMaterno, Nombre]);

        // Reemplazamos en los TAG de la plantilla los datos obtenidos
        ContenidoMail := ReplaceTAG(ContenidoMail, 'Nombre', Nombre);
        ContenidoMail := ReplaceTAG(ContenidoMail, 'Mensaje', Mensaje);
        ContenidoMail := ReplaceTAG(ContenidoMail, 'clave', Password);

        result := true;
    end;
end;


Function InsertarEMailSalida(Conn: TADOConnection; Asunto, Destino, NombreDestino,
  Contenido: AnsiString; var CodigoEMail: Integer): Boolean;
var
    SP: TADOStoredProc;
begin
    SP := TAdoStoredProc.Create(nil);
    try
        try
            SP.Connection := Conn;
            SP.ProcedureName := 'AgregarEMailsSalida';
            SP.Parameters.Refresh;

            SP.Parameters.ParamByName('@Asunto').Value := Asunto;
            SP.Parameters.ParamByName('@Destino').Value := Destino;
            SP.Parameters.ParamByName('@NombreDestino').Value := NombreDestino;
            SP.Parameters.ParamByName('@Contenido').Value := Contenido;
            SP.Parameters.ParamByName('@CodigoEMail').value := null;
            SP.execProc;
            CodigoEMail := SP.Parameters.ParamByName('@CodigoEMail').value;

            if CodigoEMail = Null then Result:=False
            else result:=True;
        except
            on e: exception do begin
                EventLogReportEvent(elError, 'Error enviando mail: ' + e.message, '');
                Result:=False;
            end;
        end;
    finally
        SP.Close;
        SP.Free;
    end;
end;

function ObtenerContenidoMail(Conn: TADOConnection; CodigoContenidoEmail: integer;
            var MensajeEmail: AnsiString; var AsuntoEmail: AnsiString): boolean;
var
    SP: TAdoStoredProc;
begin
    SP := TAdoStoredProc.Create(nil);
    try
        try
            SP.ProcedureName := 'ObtenerContenidoEmail';
            SP.Connection := Conn;
            SP.Parameters.Refresh;
            SP.Parameters.ParamByName('@CodigoContenidoEmail').value := CodigoContenidoEmail;
            SP.ExecProc;

            if SP.Parameters.ParamByName('@TextoMensaje').Value = null then MensajeEmail := ''
            else MensajeEmail := SP.Parameters.ParamByName('@TextoMensaje').value;

            if SP.Parameters.ParamByName('@Asunto').value = null then AsuntoEmail := ''
            else AsuntoEmail :=  SP.Parameters.ParamByName('@Asunto').value;

            result := MensajeEmail <> '';
        except
            Result:=False;
        end;

    finally
        SP.Close;
        SP.Free;
    end;
end;

function InsertarEMailComprobante(const Conn: TADOConnection; const TipoComprobante: string; const NumeroComprobante: integer; const CodigoImpresoraFiscal: Integer;
    const Email: string; const Usuario: string; const Convenio: integer; out DescriError: string): Boolean;
var
    spEMAIL_AgregarMensaje: TADOStoredProc;
begin
    Result := False;
    DescriError := EmptyStr;
    try
        spEMAIL_AgregarMensaje := TADOStoredProc.Create(nil);
        spEMAIL_AgregarMensaje.Connection := Conn;
        spEMAIL_AgregarMensaje.ProcedureName := 'EMAIL_AgregarMensajeComprobante';
        try
            spEMAIL_AgregarMensaje.Parameters.Refresh;
            spEMAIL_AgregarMensaje.Parameters.ParamByName('@TipoComprobante').Value := TipoComprobante;
            spEMAIL_AgregarMensaje.Parameters.ParamByName('@NumeroComprobante').Value := NumeroComprobante;
            spEMAIL_AgregarMensaje.Parameters.ParamByName('@Email').Value := Email;
            spEMAIL_AgregarMensaje.Parameters.ParamByName('@CodigoUsuario').Value := Usuario;
            spEMAIL_AgregarMensaje.Parameters.ParamByName('@CodigoConvenio').Value := Convenio;
            If CodigoImpresoraFiscal <> -1 Then Begin
                spEMAIL_AgregarMensaje.Parameters.ParamByName('@CodigoImpresoraFiscal').Value := CodigoImpresoraFiscal;
            End
            Else Begin
                spEMAIL_AgregarMensaje.Parameters.ParamByName('@CodigoImpresoraFiscal').Value := Null;
            End;
            spEMAIL_AgregarMensaje.ExecProc;
            Result := True;
        except
            on e: Exception do begin
                DescriError := e.Message;
            end;
        end;
    finally
        FreeAndNil(spEMAIL_AgregarMensaje);
    end;
end;

function ExisteMailPendienteComprobante(const DatabaseConnection: TADOConnection; const TipoComprobante: String; NumeroComprobante: Int64; CodigoImpresoraFiscal:Integer):Boolean;
resourcestring
    MSG_MISSED_CONN = 'Debe especificar una instancia de conexi�n a base de datos para utilizar esta funci�n';
    MSG_ERROR = 'Ha ocurrido un error verificando si existe un e-mail pendiente para el tipo de comprobante %s y n�mero %d. Detalle: %s';
    MSG_ERROR_TITLE = 'Error';
const
    FUNC_CALL = 'SELECT dbo.ExisteEmailPendienteComprobante(%s, %d, %d)';
begin
    Result := False;
    try
        if not Assigned(DataBaseConnection) then raise Exception.Create(MSG_MISSED_CONN);
            Result := (QueryGetValueInt(DataBaseConnection, Format(FUNC_CALL, [QuotedStr(TipoComprobante), NumeroComprobante, CodigoImpresoraFiscal])) = 1);
    except
        on e: Exception do begin
            MsgBoxErr(MSG_ERROR_TITLE, Format(MSG_ERROR, [TipoComprobante, NumeroComprobante, e.message]), MSG_ERROR_TITLE, MB_ICONERROR);
        end;
    end;
end;



function ExisteConvenioAEnviarDetalle(const DatabaseConnection: TADOConnection; CodigoConvenio: integer; const TipoComprobante: String; NumeroComprobante: Int64):Boolean;
resourcestring
    MSG_MISSED_CONN = 'Debe especificar una instancia de conexi�n a base de datos para utilizar esta funci�n';
    MSG_ERROR = 'Ha ocurrido un error verificando si existe un Detalle por e-mail pendiente pendiente para el tipo de comprobante %s y n�mero %d. Detalle: %s';
    MSG_ERROR_TITLE = 'Error';
const
    FUNC_CALL = 'SELECT dbo.ExisteConvenioAEnviarDetalle(%d, %s, %d)';
begin
    Result := False;
    try
        if not Assigned(DataBaseConnection) then raise Exception.Create(MSG_MISSED_CONN);
            Result := (QueryGetValueInt(DataBaseConnection, Format(FUNC_CALL, [CodigoConvenio, QuotedStr(TipoComprobante), NumeroComprobante ])) = 1);
    except
        on e: Exception do begin
            MsgBoxErr(MSG_ERROR_TITLE, Format(MSG_ERROR, [TipoComprobante, NumeroComprobante, e.message]), MSG_ERROR_TITLE, MB_ICONERROR);
        end;
    end;
end;

function InsertarConvenioAEnviarDetalle(const Conn: TADOConnection; const CodigoConvenio: integer; const TipoComprobante: string; const NumeroComprobante: integer; const Usuario: string; out DescriError: string): Boolean;
var
    spEMAIL_AgregarMensaje: TADOStoredProc;
begin
    Result := False;
    DescriError := EmptyStr;
    try
        spEMAIL_AgregarMensaje := TADOStoredProc.Create(nil);
        spEMAIL_AgregarMensaje.Connection := Conn;
        spEMAIL_AgregarMensaje.ProcedureName := 'InsertarConvenioAEnviarDetalle';
        try
            spEMAIL_AgregarMensaje.Parameters.Refresh;
            spEMAIL_AgregarMensaje.Parameters.ParamByName('@CodigoConvenio').Value 		:= CodigoConvenio;
            spEMAIL_AgregarMensaje.Parameters.ParamByName('@TipoComprobante').Value 	:= TipoComprobante;
            spEMAIL_AgregarMensaje.Parameters.ParamByName('@NumeroComprobante').Value 	:= NumeroComprobante;
            spEMAIL_AgregarMensaje.Parameters.ParamByName('@CodigoUsuario').Value 		:= Usuario;
            spEMAIL_AgregarMensaje.Parameters.ParamByName('@Estado').Value				:= 0;
            spEMAIL_AgregarMensaje.ExecProc;
            Result := True;
        except
            on e: Exception do begin
                DescriError := e.Message;
            end;
        end;
    finally
        FreeAndNil(spEMAIL_AgregarMensaje);
    end;
end;

end.
