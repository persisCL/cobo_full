object FrameSubTipoReclamoOrdenServicio: TFrameSubTipoReclamoOrdenServicio
  Left = 0
  Top = 0
  Width = 240
  Height = 55
  Anchors = [akTop, akRight]
  TabOrder = 0
  object GBSubTipo: TGroupBox
    Left = 2
    Top = 1
    Width = 235
    Height = 48
    Caption = 'Sub Tipo Reclamo'
    TabOrder = 0
    object lblSubTipo: TLabel
      Left = 7
      Top = 23
      Width = 85
      Height = 13
      Caption = 'SubTipo Reclamo:'
    end
    object vcbSubTipoReclamo: TVariantComboBox
      Left = 118
      Top = 15
      Width = 108
      Height = 21
      Style = vcsDropDownList
      DroppedWidth = 0
      ItemHeight = 13
      TabOrder = 0
      Items = <>
    end
  end
end
