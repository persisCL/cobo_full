unit frmSeleccionConvenio;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, ListBoxEx, DBListEx, StdCtrls, ExtCtrls, DPSControls,
  DMConnection, Peaprocs, Peatypes, Util, UtilDB, UtilProc, frmRegistrarLlamada, RStrings,
  FreDatosPersonaConsulta, CheckLst;

type
  TformSeleccionConvenio = class(TForm)
    GBConvenios: TGroupBox;
    PanelDeAbajo: TPanel;
    dbl_Convenios: TDBListEx;
    ObtenerConveniosPersona: TADOStoredProc;
    DS_SolicitudContacto: TDataSource;
    BtnCancelar: TDPSButton;
    btnAceptar: TDPSButton;
    GBPersona: TGroupBox;
    FrameDatosPersonaConsulta1: TFrameDatosPersonaConsulta;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dbl_ConveniosDblClick(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
  private
    { Private declarations }
    FCodigoConvenio: integer;
    function GetCodigoConvenio: integer;
  public
    { Public declarations }
    property CodigoConvenio: integer read GetCodigoConvenio;
    function Inicializar(CodigoDocumento, NumeroDocumento: AnsiString; HabilitarRUT: boolean = false): Boolean;
  end;

var
  formSeleccionConvenio: TformSeleccionConvenio;

implementation

{$R *.dfm}

procedure TformSeleccionConvenio.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
//    action := caFree;
end;

function TformSeleccionConvenio.GetCodigoConvenio: integer;
begin
    Result := FCodigoConvenio;
end;

function TformSeleccionConvenio.Inicializar(CodigoDocumento, NumeroDocumento: AnsiString; HabilitarRUT: boolean = false): Boolean;
resourcestring
    CONVENIOS = 'Convenios';
    MSG_OPENTABLES = 'Error al buscar los convenios.';
begin
    //HabilitarRUT es para que solo puedan buscar con el RUT buscado...

    {
    with ObtenerConveniosPersona do begin
        close;
        Parameters.Refresh;
        Parameters.ParamByName('@RUT').Value := NumeroDocumento;
        try
            Open;
        except
            on E: Exception do begin
              MsgBoxErr(MSG_OPENTABLES,e.Message,self.Caption,MB_ICONSTOP);
              result := false;
              exit;
            end;
        end;
    end;}
    result := true;
end;

procedure TformSeleccionConvenio.dbl_ConveniosDblClick(
  Sender: TObject);
begin

    if not (ObtenerConveniosPersona.IsEmpty) then begin
        FCodigoConvenio := ObtenerConveniosPersona.FieldByName('CodigoConvenioContacto').AsInteger;
        ModalResult := mrOk;
    end;

end;

procedure TformSeleccionConvenio.BtnCancelarClick(
  Sender: TObject);
begin
    FCodigoConvenio := 0;
    ModalResult := mrCancel;
end;

procedure TformSeleccionConvenio.btnAceptarClick(
  Sender: TObject);
begin
    {if not (ObtenerConveniosPersona.IsEmpty) then begin
        FCodigoConvenio := ObtenerConveniosPersona.FieldByName('CodigoConvenio').AsInteger;
        ModalResult := mrOk;
    end;
    }
end;

end.
