{-----------------------------------------------------------------------------
 File Name: ComunInterfaces.pas
 Author:    mbecerra
 Date: 17-Julio-2008
 Description:
                Constantes, variables, procedimientos y funciones para todas las
                aplicaciones de Interfaces.

                Ser� utilizada por las Interfaces Homologadas y en la generaci�n
                de N�minas.

                Esta Unit est� basada en las units antiguas:
                	* \Comunes\RutinasComunesInterfaces
                    * \Interfaces\ComunesInterfaces
-----------------------------------------------------------------------------}
unit ComunInterfaces;

interface

uses
     UtilProc,
     UtilDB,
     Util,
     Classes, ADODB, VariantComboBox, Windows, Variants, Controls, SysUtils;


    {********************************** Funtions *********************************}
    {LogOperacionesInterfases}
	  function ActualizarLogOperacionesInterfaseAlFinal(Conn: TAdoConnection; CodigoOperacionInterfase : integer; Observaciones : string; Var DescError: string): boolean; overload;
    function ActualizarLogOperacionesInterfaseAlFinal(Conn: TAdoConnection; CodigoOperacionInterfase, LineasArchivo : integer; MontoArchivo : Int64; Observaciones : string; Var DescError: string): boolean; overload;
    function ActualizarLogOperacionesInterfacesAlFinal(Conn: TAdoConnection; CodigoOperacionInterface, TotalLineasArchivo, TotalRegistrosProcesados, TotalPagosRecibidos : integer; MontoArchivo, MontoPagosRecibidos : Int64; Observaciones : string; HayErrores: boolean; Var DescError: string): boolean;
    function RegistrarOperacionEnLogInterface(Conn: TAdoConnection; CodigoModulo: Integer; NombreArchivo, Usuario, Observaciones: string; recibido, Reprocesamiento: boolean; FechaHoraRecepcion:tdatetime;UltimoRegistroEnviado: integer; var CodigoOperacionInterfase:integer;Var DescError : string):boolean;
    function RegistrarCancelacionInterface(Conn : TAdoConnection; CodigoOperacionInterfase : Integer; var DescError : string) : Boolean;
    function AgregarEnErroresInterfaces(Conn: TAdoConnection; CodigoOperacionInterfase, CodigoTipoError : Integer; DescripcionError : String) : Boolean;
    function VerificarArchivoProcesado ( Base:TADOConnection; sFileName : string ) : boolean;

    {manejo de archivos}
    function MoverArchivoProcesado(MsgTitulo, PathNameOrigen, PathNameDestino: AnsiString; NoInfo: Boolean = False): boolean;

    {uso general}
    function GetItem(Texto : string; NumeroItem : integer; Separador : Char) : string;
    
implementation

{-------------------------------------------------------------------------------
 Function GetItem
 Revision 1:
    Author : mbecerra
    Date : 08-04-2008
    Description : Function que recibe un string de la forma <campo1>sep<campo2>sep<campo3>
                	y devuelve el <campo i> indicado en el par�metro, o un string vac�o
                    si no lo encuentra
-------------------------------------------------------------------------------}
function GetItem;
var
    Elemento : string;
    i, Cual : integer;
begin
    i := 1;
    Elemento := '';
    Cual := 0;
    Texto := Texto + Separador;  {para asegurarnos de encontrar el separador en el �ltimo elemento}
    while i <= Length(Texto) do begin
        if Texto[i] = Separador then begin
            Inc(Cual);
            if Cual = NumeroItem then   {es el elemento que se debe retornar}
            	i := Length(Texto) + 1
            else
                Elemento := '';         {no es el elemento que se debe retornar}

        end
        else
          Elemento := Elemento + Texto[i];

    	Inc(i);
    end;

    Result := Elemento;
end;

{-----------------------------------------------------------------------------
  Function Name: RegistrarOperacionEnLogInterface
  Author:    lgisuk
  Date Created: 12/01/2005
  Description: Registra la Operacion en el Log
  Parameters: Conn: TAdoConnection;CodigoModulo:Integer;Fecha:tdatetime;NombreArchivo,Usuario,Observaciones:string;recibido, Reprocesamiento:boolean;FechaHoraRecepcion:tdatetime;UltimoRegistroEnviado: integer;var CodigoOperacionInterfase:integer;Var DescError:Ansistring
  Return Value: boolean
-----------------------------------------------------------------------------}
Function  RegistrarOperacionEnLogInterface;
var
	sp : TADOStoredProc;
begin
  	CodigoOperacionInterfase := 0;
    Result := False;
    sp := TADOStoredProc.Create(Nil);
    try
        try
            sp.Connection := Conn;
            sp.ProcedureName := 'AgregarLogOperacionesInterfases';
            with sp.Parameters do begin
                Refresh;
                ParamByName('@CodigoModulo').Value             := CodigoModulo;
                ParamByName('@NombreArchivo').Value            := NombreArchivo;
                ParamByName('@Usuario').Value                  := Usuario;
                ParamByName('@Observaciones').Value            := Observaciones;
                ParamByName('@Reprocesamiento').Value          := Reprocesamiento;
                ParamByName('@FechaHoraRecepcion').Value       := FechaHoraRecepcion;
                ParamByName('@UltimoRegistroEnviado').Value    := IIF( UltimoRegistroEnviado = 0, null, UltimoRegistroEnviado );
                ParamByName('@CodigoOperacionInterfase').Value := 0;
            end;

            sp.ExecProc;                                                                             //Ejecuto el procedimiento
            CodigoOperacionInterfase:= SP.Parameters.ParamByName('@CodigoOperacionInterfase').Value; //Obtengo el Id
            result:=true;
        except on E : Exception do begin
                DescError := E.Message;
            end;
        end;

    finally
        sp.Free;
    end;
end;

{-----------------------------------------------------------------------------
				RegistrarCancelacionInterface
  Author: mbecerra
  Date: 17-Nov-2008
  Description: Registra que el proceso ha sido cancelado por el usuario
-----------------------------------------------------------------------------}
Function  RegistrarCancelacionInterface;
Const
  MSG_CANCEL = 'Proceso Cancelado por el Usuario';
begin
    Result := ActualizarLogOperacionesInterfacesAlFinal(Conn,CodigoOperacionInterfase,0,0,0,0,0,MSG_CANCEL,True,DescError);
end;


{-----------------------------------------------------------------------------
  Function Name: ActualizarLogOperacionesInterfaseAlFinal
  Author:    lgisuk
  Date Created: 04/07/2005
  Description: Asigna la fecha de finalizaci�n al final y las observaciones
  Parameters: Conn: TAdoConnection;CodigoOperacionInterfase:integer;Observaciones:string;Var DescError:Ansistring
  Return Value: boolean
-----------------------------------------------------------------------------}
Function  ActualizarLogOperacionesInterfaseAlFinal(Conn : TAdoConnection; CodigoOperacionInterfase : Integer; Observaciones : String; Var DescError : Ansistring) : Boolean;
var sp : TADOStoredProc;
begin
    Result := False;
    sp := TADOStoredProc.Create(Nil);
    try
        try
            sp.Connection := Conn;
            sp.ProcedureName := 'ActualizarLogOperacionesInterfaseAlFinal';
            with SP.Parameters do begin
                Refresh;
                ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacionInterfase;
                ParamByName('@Observaciones').Value := Observaciones;
            end;
            SP.ExecProc;                                                                             //Ejecuto el procedimiento
            Result := True;
		    except
            on E : Exception do begin
                DescError := E.Message;
            end;
        end;
    finally
        SP.Free;
  	end;
end;

{-----------------------------------------------------------------------------
  Function Name: ActualizarLogOperacionesInterfaseAlFinal
  Author:    mbecerra
  Date: 17-Julio-2008
  Description:
                Actualiza la tabla LogOperacionesInterfases con la
                Observaci�n, LineasArchivo y MontoArchivo.
-----------------------------------------------------------------------------}
function ActualizarLogOperacionesInterfaseAlFinal(Conn: TAdoConnection; CodigoOperacionInterfase, LineasArchivo : integer; MontoArchivo : Int64; Observaciones : string; Var DescError: string): boolean; overload;
var sp : TADOStoredProc;
begin
    Result := False;
    sp := TADOStoredProc.Create(Nil);
    DescError := '';
    try
        try
            sp.Connection := Conn;
            sp.ProcedureName := 'ActualizarLogOperacionesInterfaseAlFinal';
            with SP.Parameters do begin
                Refresh;
                ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacionInterfase;
                ParamByName('@Observaciones').Value := Observaciones;
                ParamByName('@LineasArchivo').Value := LineasArchivo;
                ParamByName('@MontoArchivo').Value  := MontoArchivo;
            end;
            SP.ExecProc;
            Result := True;
		    except
            on E : Exception do begin
                DescError := E.Message;
            end;
        end;
    finally
        SP.Free;
  	end;
end;


{-----------------------------------------------------------------------------
  Function Name: ActualizarLogOperacionesInterfacesAlFinal
  Author:    mbecerra
  Date: 25-Sept-2008
  Description:
                Actualiza la tabla LogOperacionesInterfases con la
                Observaci�n, LineasArchivo, LineasParseadasConError,
                LineasConExito, LineasSinGrabar, MontoArchivo.
-----------------------------------------------------------------------------}
function ActualizarLogOperacionesInterfacesAlFinal;
var sp : TADOStoredProc;
begin
    Result := False;
    sp := TADOStoredProc.Create(Nil);
    try
    	sp.Connection := Conn;
        sp.ProcedureName := 'ActualizarLogOperacionesInterfacesAlFinal';
        sp.Parameters.Refresh;
        sp.Parameters.ParamByName('@CodigoOperacionInterface').Value	:= CodigoOperacionInterface;
        sp.Parameters.ParamByName('@Observaciones').Value				:= Observaciones;
        sp.Parameters.ParamByName('@TotalLineasArchivo').Value			:= TotalLineasArchivo;
        sp.Parameters.ParamByName('@TotalRegistrosProcesados').Value	:= TotalRegistrosProcesados;
        sp.Parameters.ParamByName('@TotalPagosRecibidos').Value			:= TotalPagosRecibidos;
        sp.Parameters.ParamByName('@MontoArchivo').Value  				:= MontoArchivo;
        sp.Parameters.ParamByName('@MontoPagosRecibidos').Value  		:= MontoPagosRecibidos;
        sp.Parameters.ParamByName('@FinalizoOK').Value  				:= not HayErrores;
        sp.ExecProc;
        Result := True;
    except on e : exception do begin
    		DescError := E.Message;
    	end;
    end;

    sp.Free;
end;


{-----------------------------------------------------------------------------
					AgregarEnErroresInterfaces
  Author: mbecerra
  Date: 13-Nov-2008
  Description: Agrega un mensaje de Error en la tabla ErroresInterfases
-----------------------------------------------------------------------------}
function AgregarEnErroresInterfaces;
resourcestring
    MSG_ERROR_GENERATING_ERROR_LOG = 'Error generando log de errores';
  	MSG_ERROR = 'Error';
var
    spAgregarErrorInterfaz : TADOStoredProc;
begin
  	Result := False;
    spAgregarErrorInterfaz := TADOStoredProc.Create(Nil);
   	try
    	with spAgregarErrorInterfaz, Parameters do begin
        	Connection := Conn;
            ProcedureName := 'AgregarErrorInterfase';
            Refresh;
            ParamByName('@CodigoOperacionInterfase').Value := CodigoOperacionInterfase;
            ParamByName('@CodigoTipoError').Value := CodigoTipoError;
            ParamByName('@DescripcionError').Value := DescripcionError;
            ExecProc;
            Result := True;
        end;
    except on e: Exception do begin
    		MsgBoxErr(MSG_ERROR_GENERATING_ERROR_LOG, e.Message, MSG_ERROR, MB_ICONERROR);
    	end;
    end;

    spAgregarErrorInterfaz.Free;
end;

{-----------------------------------------------------------------------------
  Function Name: VerificarArchivoProcesado
  Author:    flamas
  Date Created: 23/12/2004
  Description: Verifica si el Archivo ha sido procesado en una interface
  Parameters: sFileName : string
  Return Value: boolean
-----------------------------------------------------------------------------
  Revision 1
  lgisuk
  26/09/2006
  Ahora se le env�a la fecha actual por par�metro para obtener el a�o actual.
-----------------------------------------------------------------------------}
function VerificarArchivoProcesado;
resourcestring
    MSG_FILE_ALREADY_PROCESSED = 'El archivo %s ya fue procesado';
begin
    Result := ( QueryGetValue(Base, 'SELECT dbo.VerificarArchivoInterfaseProcesado(''' + ExtractFileName( sFileName ) + ''',getdate())') = 'True' );
end;

{-----------------------------------------------------------------------------
  Function Name: MoverArchivoProcesado
  Author:    ndonadio
  Date Created: 02/05/2005
  Description: Mueve el archivo de origen al de destino.
                Se utiliza principalmente para mover los archivos procesados
                exitosamente a un directorio de archivos procesados.

  Parameters: PathNameOrigen, PathNameDestino: AnsiString
  Return Value: boolean
-----------------------------------------------------------------------------}
function MoverArchivoProcesado(MsgTitulo, PathNameOrigen, PathNameDestino: AnsiString; NoInfo: Boolean = False): boolean;
resourcestring
    MSG_ERROR_MOVING_FILE   =   'El archivo %s '+CrLf+
                                'no se ha podido mover a: %s'+CrLf +
                                'Deber� completar la operaci�n manualmente.';
    MSG_MOVING_COMPLETE     =   'El archivo %s '+CrLf+
                                'ha sido movido a: %s';
begin
 try
    if TRIM(PathNameOrigen) = TRIM(PathNameDestino) then
    begin
        Result := True;
        Exit;
    end;
    if MoveFile(PChar(PathNameOrigen), PChar(PathNameDestino)) then begin
        Result := True;
        if NOT NoInfo then MsgBox(Format(MSG_MOVING_COMPLETE,[PathNameOrigen, PathNameDestino]),'INFORMACI�N: '+MsgTitulo,MB_ICONINFORMATION);
    end
    else begin
        Result := False;
        MsgBox(Format(MSG_ERROR_MOVING_FILE, [PathNameOrigen, PathNameDestino]),'ADVERTENCIA: '+MsgTitulo,MB_ICONWARNING);
    end;
 except
    Result := False;
    MsgBox(Format(MSG_ERROR_MOVING_FILE,[PathNameOrigen, PathNameDestino]),'ADVERTENCIA: '+MsgTitulo,MB_ICONWARNING);
 end;
end;





end.
