{-----------------------------------------------------------------------------
        ImprimirWO

Revision        : 1
Author          : Alejandro Labra
Date            : 07-Julio-2011
Description     : Se aumenta la constante por la cual aumenta el tama�o de la
                  ventana de acuerdo a las opciones entragadas, de 175 a 230.
                  Esto debido a que no eran visibles todas las opciones.
Firma           : PAR00114-ALA-07072011
 -----------------------------------------------------------------------------}
unit ImprimirWO;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, DPSControls, CheckLst, VariantComboBox;

type
  TWOStyle = (twoRadio, twoCheck, twoAuto);
  TRespuesta = array of integer;
  TfrmImprimirWO = class(TForm)
    lbl_Mensaje: TLabel;
    img: TImage;
    rg1: TRadioGroup;
    pn1: TPanel;
    cbl: TVariantCheckListBox;
    btn_Aceptar: TButton;
    btn_Cancelar: TButton;
    procedure btn_AceptarClick(Sender: TObject);
    procedure btn_CancelarClick(Sender: TObject);
  private
    { Private declarations }
    FOpciones: integer;
    FValorRetorno: TRespuesta;
  public
    { Public declarations }
  end;

var
  frmImprimirWO: TfrmImprimirWO;
  function Ejecutar(txtCaption, txtMensaje: AnsiString; Botones: array of string;
    Options: array of string; Default: array of integer;  Style: TWOStyle  = twoAuto): TRespuesta;

implementation


{$R *.dfm}

{ TfrmImprimirWO }

procedure TfrmImprimirWO.btn_AceptarClick(Sender: TObject);
var
    i : Integer;
begin
    if (FOpciones >= 0) then begin
        if rg1.Visible then  begin
            FValorRetorno[1] := rg1.ItemIndex + 1 ;
        end
        else begin

            for i := 1 to cbl.Items.Count do begin
                if cbl.Checked[i-1] then
                    FValorRetorno[i] :=  1
                else
                    FValorRetorno[i] :=  0;
            end;
        end;
    end;

    FValorRetorno[0] := mrOk;
    Close;
end;

function Ejecutar(txtCaption, txtMensaje: AnsiString; Botones: array of string;
  Options: array of string ; Default: array of integer; Style: TWOStyle  = twoAuto): TRespuesta;
var
    i:integer;
begin
    with TfrmImprimirWO.Create(Application) do begin
        try
            if High(Botones) >= 1 then begin
                btn_Aceptar.Caption := Botones[0];
                btn_Cancelar.Caption := Botones[1];
            end;
            Caption := txtCaption;
            lbl_Mensaje.Caption := txtMensaje;
            lbl_Mensaje.AutoSize := True;
            lbl_Mensaje.Update;
            FOpciones := High(Options);
            SetLength(FValorRetorno,FOpciones+2);
            if FOpciones =  -1 then begin
                pn1.Visible := False;
                rg1.Visible := False;
            end else begin
                if (FOPciones > 0) AND ((Style = twoAuto) OR (Style = twoRadio)) then begin
                    pn1.Visible := False;
                    rg1.Visible := True;
                    Height := 175 + (18 * (FOpciones - 1));
                    For  i := 0 to FOpciones do begin
                        rg1.Items.Add(Options[i]);
                    end;

                    if (High(Default) >= 0) AND (Default[0] <= FOpciones) AND (Default[0] >= -1) then
                        rg1.ItemIndex := Default[0] else rg1.ItemIndex := -1;
                end else begin
                    // Ahora tengo que ver como corno
                    // agrego checkboxes en tiempo de ejecucion
                    rg1.Visible := False;
                    pn1.Visible := True;
                    Height := 230 + ((cbl.ItemHeight+1) * (FOpciones - 2));     //PAR00114-ALA-07072011 
                    While (cbl.Height <= (cbl.ItemHeight * cbl.Items.Count )) do Height := Height + 1;
                    cbl.Items.Clear;
                    For  i := 0 to FOpciones do begin
                        cbl.Items.Add(Options[i],0);
                    end;
                    For  i := 0 to FOpciones do begin
                        cbl.Checked[i] := Default[i] = 1;
                    end;
                end;
            end;
            FValorRetorno[0] := mrCancel;
            ShowModal;
            Result := FValorRetorno;
        finally
            Release;
        end;
    end;
end;

procedure TfrmImprimirWO.btn_CancelarClick(Sender: TObject);
begin
    FValorRetorno[0] := mrCancel;
    Close;
end;
end.
