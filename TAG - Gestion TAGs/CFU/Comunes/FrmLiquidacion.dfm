object FormLiquidacion: TFormLiquidacion
  Left = 262
  Top = 139
  BorderStyle = bsDialog
  Caption = 'Liquidaci'#243'n al Cierre de Turno'
  ClientHeight = 412
  ClientWidth = 704
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnActivate = FormActivate
  DesignSize = (
    704
    412)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 8
    Width = 39
    Height = 13
    Caption = 'Efectivo'
  end
  object Label2: TLabel
    Left = 16
    Top = 252
    Width = 204
    Height = 13
    Caption = 'Cantidad de cupones de Tarjeta de D'#233'bito:'
  end
  object Label3: TLabel
    Left = 16
    Top = 276
    Width = 206
    Height = 13
    Caption = 'Cantidad de cupones de Tarjeta de Cr'#233'dito:'
  end
  object Label4: TLabel
    Left = 16
    Top = 229
    Width = 105
    Height = 13
    Caption = 'Cantidad de Cheques:'
  end
  object btn_Aceptar: TDPSButton
    Left = 541
    Top = 381
    Anchors = [akRight, akBottom]
    Caption = '&Aceptar'
    Default = True
    TabOrder = 0
    OnClick = btn_AceptarClick
  end
  object btn_Cancelar: TDPSButton
    Left = 621
    Top = 381
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Cancelar'
    TabOrder = 1
    OnClick = btn_CancelarClick
  end
  object GrillaBilletes: TStringGrid
    Left = 14
    Top = 25
    Width = 675
    Height = 193
    ColCount = 3
    DefaultRowHeight = 18
    FixedColor = 14732467
    FixedCols = 0
    RowCount = 13
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing, goAlwaysShowEditor]
    ParentFont = False
    ScrollBars = ssVertical
    TabOrder = 2
    OnKeyPress = GrillaBilletesKeyPress
    OnSelectCell = GrillaBilletesSelectCell
    ColWidths = (
      434
      112
      102)
  end
  object edCuponesPAC: TEdit
    Left = 223
    Top = 248
    Width = 121
    Height = 21
    MaxLength = 4
    TabOrder = 4
    OnKeyPress = GrillaBilletesKeyPress
  end
  object edCheques: TEdit
    Left = 223
    Top = 224
    Width = 121
    Height = 21
    MaxLength = 4
    TabOrder = 3
    OnKeyPress = GrillaBilletesKeyPress
  end
  object edCuponesPAT: TEdit
    Left = 223
    Top = 272
    Width = 121
    Height = 21
    MaxLength = 4
    TabOrder = 5
    OnKeyPress = GrillaBilletesKeyPress
  end
  object spObtenerLiquidacionTurnoEfectivo: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerLiquidacionTurnoEfectivo;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 89
    Top = 96
  end
  object spAgregarLiquidacionEfectivo: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'AgregarLiquidacionEfectivo;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoDenominacionMoneda'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@ValorMonedaLocal'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Value = Null
      end
      item
        Name = '@Cantidad'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 376
    Top = 72
  end
  object spAgregarLiquidacionCupones: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'AgregarLiquidacionCupones;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CantidadCheques'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CantidadCuponesPAC'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CantidadCuponesPAT'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 375
    Top = 124
  end
  object spCerrarTurnoLiquidacionCaja: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CerrarTurnoLiquidacionCaja;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 88
    Top = 200
  end
  object spObtenerLiquidacionTurnoCupones: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerLiquidacionTurnoCupones;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 89
    Top = 144
  end
end
