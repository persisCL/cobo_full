object FormParam: TFormParam
  Left = 165
  Top = 140
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Par'#225'metros Generales del Sistema Central de Peaje'
  ClientHeight = 458
  ClientWidth = 825
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl: TPageControl
    Left = 0
    Top = 0
    Width = 825
    Height = 425
    ActivePage = TabSheet1
    Align = alTop
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Par'#225'metros'
      object Lista: TDBListEx
        Left = 0
        Top = 41
        Width = 817
        Height = 356
        Align = alClient
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 250
            Header.Caption = 'Descripci'#243'n'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            OnHeaderClick = ListaColumns0HeaderClick
            FieldName = 'Descripcion'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsUnderline]
            Width = 200
            Header.Caption = 'Valor'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Color = clSilver
            IsLink = True
            OnHeaderClick = ListaColumns1HeaderClick
            FieldName = 'Valor'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 250
            Header.Caption = 'Por Defecto'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            OnHeaderClick = ListaColumns2HeaderClick
            FieldName = 'ValorDefault'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 100
            Header.Caption = 'Tipo'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            OnHeaderClick = ListaColumns3HeaderClick
            FieldName = 'TipoParametro'
          end>
        DataSource = DataSource1
        DragReorder = True
        ParentColor = False
        TabOrder = 0
        TabStop = True
        OnDrawText = ListaDrawText
        OnLinkClick = ListaLinkClick
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 817
        Height = 41
        Align = alTop
        BevelInner = bvLowered
        BevelOuter = bvNone
        BorderStyle = bsSingle
        TabOrder = 1
        object Label1: TLabel
          Left = 8
          Top = 12
          Width = 214
          Height = 13
          Caption = 'S'#243'lo ver par'#225'metros configurables relativos a:'
        end
        object cbFiltros: TVariantComboBox
          Left = 224
          Top = 8
          Width = 241
          Height = 21
          Style = vcsDropDownList
          ItemHeight = 13
          TabOrder = 0
          OnChange = cbFiltrosChange
          Items = <>
        end
      end
    end
  end
  object ButtonCancelar: TButton
    Left = 750
    Top = 430
    Width = 71
    Height = 24
    Cancel = True
    Caption = '&Cancelar'
    TabOrder = 2
    OnClick = ButtonCancelarClick
  end
  object ButtonAceptar: TButton
    Left = 668
    Top = 431
    Width = 71
    Height = 24
    Caption = '&Aceptar'
    Default = True
    TabOrder = 1
    OnClick = ButtonAceptarClick
  end
  object Parametros: TADOTable
    Connection = DMConnections.BaseBO_Master
    TableName = 'Parametros'
    Left = 2
    Top = 420
  end
  object OpenDialog: TOpenDialog
    Options = [ofHideReadOnly, ofPathMustExist, ofEnableSizing]
    Left = 36
    Top = 420
  end
  object DataSource1: TDataSource
    DataSet = Generales
    Left = 96
    Top = 424
  end
  object Generales: TADOTable
    Connection = DMConnections.BaseBO_Master
    CursorType = ctStatic
    TableName = 'ParametrosGenerales'
    Left = 64
    Top = 421
    object GeneralesClaseParametro: TStringField
      FieldName = 'ClaseParametro'
      FixedChar = True
      Size = 1
    end
    object GeneralesNombre: TStringField
      FieldName = 'Nombre'
      Size = 35
    end
    object GeneralesDescripcion: TStringField
      FieldName = 'Descripcion'
      Size = 255
    end
    object GeneralesValor: TStringField
      FieldName = 'Valor'
      Size = 255
    end
    object GeneralesTipoParametro: TStringField
      FieldName = 'TipoParametro'
      FixedChar = True
      Size = 1
    end
    object GeneralesValorDefault: TStringField
      FieldName = 'ValorDefault'
      Size = 255
    end
  end
end
