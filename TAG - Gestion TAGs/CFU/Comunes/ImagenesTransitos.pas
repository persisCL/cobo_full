{********************************** Unit Header ********************************
Revision    : 1
Date        : 07-Junio-2011
Author      : Nelson Droguett Sierra
Description :  (Ref.Fase2) Las imagenes se transfieren como imagenes individuales.
                Ya no se usar� el formato "archivo padre".
              Se Agrega la unit UtilDB
              Se agrega la variable FImagePathNFI
              Function CargarImagen : Se agrega la variable NombreCorto y se llena
              con el resultado de un QueryGetValue
              Se llama a la funcion ObtenerImagenTransito con el parametro NombreCorto y el nuevo path
              Procedure FormCreate : Se obtiene el parametro general de la nueva carpeta para imagenes.
Firma       : SS-377-NDR-20110607

Author      : Claudio Quezada Ib��ez
Date        : 16-Mayo-2013
Firma       : SS_1091_CQU_20130516
Description : Se modifica la visualizaci�n de im�genes para poder desplegar im�genes de p�rticos Italianos.
              Agrega una sobrecarga al m�todo MostrarVentanaImagen

Author      : Claudio Quezada Ib��ez
Date        : 17-Septiembre-2013
Firma       : SS_1214_CQU_20140917
Description : Se agregan nuevas funciones para cargar las im�genes de referencia de una patente.

Author      : Claudio Quezada Ib��ez
Date        : 07-Mayo-2015
Firma       : SS_1147_CQU_20150507
Description : Se agega funcionalidad para cargar las imagenes directamente del archivo sin pasar por las funciones
              ya que esto es un simple visor y no conlleva tratamiento alguno.
              Las funciones son:
                                - CargarImagenesDeReferencia
                                - CargarImagenesDelTransito
              Ojo, para poder colocar la m�scara se tuvo que cargar las fotos en un objeto y luego pasarlo a otro.
              Si estos m�todos no cargan las imagenes, entonces carga con el m�todo antiguo.

*******************************************************************************}
unit ImagenesTransitos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, UtilImg, imgTypes, JpegPlus, ImgProcs,
  ConstParametrosGenerales, DMConnection, Filtros, UtilProc, DB, ADODB,
  Util, jpeg,                                                                   // SS_1147_CQU_20150507
  StdCtrls, UtilDB;                                                             //SS-377-NDR-20110607

type
  TfrmImagenesTransitos = class(TForm)
    pcImagenes: TPageControl;
    tsFrontal1: TTabSheet;
    imgFrontal: TImage;
    tsPosterior1: TTabSheet;
    imgPosterior: TImage;
    tsOverview1: TTabSheet;
    imgOverview1: TImage;
    tsOverview2: TTabSheet;
    imgOverview2: TImage;
    tsOverview3: TTabSheet;
    imgOverview3: TImage;
    tsFrontal2: TTabSheet;
    imgFrontal2: TImage;
    tsPosterior2: TTabSheet;
    imgPosterior2: TImage;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    //FImagePath,                                                               //SS-377-NDR-20110607
    FImagePathNFI: AnsiString;                                                  //SS-377-NDR-20110607
    FEsItaliano : Boolean;                                                                                      // SS_1091_CQU_20130516
    FImagePlatePathNFI : AnsiString;                                                                            // SS_1214_CQU_20140917
    function CargarImagen(NumCorrCA: Int64; FechaHora: TDateTime): Boolean; overload;                           // SS_1091_CQU_20130516
    function CargarImagen(NumCorrCA: Int64; FechaHora: TDateTime; EsItaliano : Boolean): Boolean;  overload;    // SS_1091_CQU_20130516
    function CargarImagenReferencia(Patente : string): Boolean;                                                 // SS_1214_CQU_20140917
    function CargarImagenesDelTransito(NumCorrCA: Int64; FechaHora: TDateTime): Boolean;                        // SS_1147_CQU_20150507
    function CargarImagenesDeReferencia(Patente : string): Boolean;                                             // SS_1147_CQU_20150507
  public
  end;

//function MostrarVentanaImagen(AOwner: TObject; NumCorrCA: Int64; FechaHora: TDateTime): Boolean; overload;                      // SS_1091_CQU_20130516
function MostrarVentanaImagen(AOwner: TObject; NumCorrCA: Int64; FechaHora: TDateTime; EsItaliano : Boolean = False): Boolean;    // SS_1091_CQU_20130516
function MostrarVentanaImagenReferencia(AOwner: TObject; Patente : string): Boolean;                                              // SS_1214_CQU_20140917
function VentanaImagenVisible: Boolean;
procedure CerrarVentanaImagen(AOwner: TObject);

implementation

{$R *.dfm}

Var
    GVentana: TfrmImagenesTransitos = nil;
    GOwner: TObject = nil;

//function MostrarVentanaImagen(AOwner: TObject; NumCorrCA: Int64; FechaHora: TDateTime): Boolean;                      // SS_1091_CQU_20130516
function MostrarVentanaImagen(AOwner: TObject; NumCorrCA: Int64; FechaHora: TDateTime; EsItaliano : Boolean): Boolean;  // SS_1091_CQU_20130516
resourcestring
	MSG_NO_IMAGES = 'Im�genes del tr�nsito no disponibles';
begin
	GOwner := AOwner;
	if not Assigned(GVentana) then Application.CreateForm(TfrmImagenesTransitos, GVentana);
    //Result := GVentana.CargarImagen(NumCorrCA, FechaHora);                                                            // SS_1091_CQU_20130516
	Result := GVentana.CargarImagen(NumCorrCA, FechaHora, EsItaliano);                                                  // SS_1091_CQU_20130516

	if not Result then MsgBox(MSG_NO_IMAGES, 'Im�genes Tr�nsito', MB_ICONINFORMATION);
end;

{------------------------------------------------------------------------------------------------
  Procedure     :   MostrarVentanaImagenReferencia
  Author        :   CQuezadaI
  Date          :   22-09-2014
  Firma         :   SS_1214_CQU_20140917
  Description   :   Muestra la ventana con las imagenes de referencia o un mensaje si no existen.
-------------------------------------------------------------------------------------------------}
function MostrarVentanaImagenReferencia(AOwner: TObject; Patente : string): Boolean;
resourcestring
	MSG_NO_IMAGES = 'Im�genes de referencia no disponibles';
begin
	GOwner := AOwner;
	if not Assigned(GVentana) then Application.CreateForm(TfrmImagenesTransitos, GVentana);
	Result := GVentana.CargarImagenReferencia(Patente);

	if not Result then MsgBox(MSG_NO_IMAGES, 'Im�genes de referencia', MB_ICONINFORMATION);
end;

function VentanaImagenVisible: Boolean;
begin
    Result := Assigned(GVentana);
end;

procedure CerrarVentanaImagen(AOwner: TObject);
begin
    if Assigned(GVentana) and (AOwner = GOwner) and
      not (csDestroying in GVentana.COmponentState)
      then GVentana.Release;
end;

{ TfrmImagenesTransitos }

function TfrmImagenesTransitos.CargarImagen(NumCorrCA: Int64; FechaHora: TDateTime): Boolean;
resourcestring
    CAPTION_IMAGEN_TRANSITO = 'Imagenes del tr�nsito (Patente: %s - Fecha y Hora: %s)';
    CAPTION_IMAGEN_TRANSITO_DEFAULT = 'Imagenes del tr�nsito';

var
	Error: TTipoErrorImg;
    JPG12: TJPEGPlusImage;
    JPGItaliano : TBitmap;          // SS_1091_CQU_20130516
    ImgOverview: TBitMap;
	DescriError, NombreCorto: AnsiString;                                       //SS-377-NDR-20110607
    DataImage: TDataImage;
    bTieneImagen : Boolean;                                             // SS_1147_CQU_20150507
begin
	// Cargamos las foto
    screen.Cursor := crHourGlass;
    tsFrontal1.TabVisible := False;
    tsPosterior1.TabVisible := False;
    tsOverview1.TabVisible := False;
    tsOverview2.TabVisible := False;
    tsOverview3.TabVisible := False;
    tsFrontal2.TabVisible := False;
    tsPosterior2.TabVisible := False;
    JPG12 := TJPEGPlusImage.Create;
    JPGItaliano := TBitMap.create;  // SS_1091_CQU_20130516
    ImgOverview := TBitMap.create;

    if CargarImagenesDelTransito(NumCorrCA, FechaHora) then begin       // SS_1147_CQU_20150507
        screen.Cursor := crDefault;                                     // SS_1147_CQU_20150507
        Visible := True;                                                // SS_1147_CQU_20150507
        Result := True;                                                 // SS_1147_CQU_20150507
    end else begin                                                      // SS_1147_CQU_20150507
    // Aumento TABULADO                                                 // SS_1147_CQU_20150507
        NombreCorto := QueryGetValue(DMConnections.BaseCAC,Format('SELECT Concesionarias.NombreCorto FROM Transitos (NOLOCK) INNER JOIN Concesionarias (NOLOCK) ON Concesionarias.CodigoConcesionaria=Transitos.CodigoConcesionaria WHERE NumCorrCA=%d', [NumCorrCA]));               //SS-377-NDR-20110607
        try
            if FEsItaliano and ObtenerImagenTransito(FImagePathNFI, NombreCorto, NumCorrCA, FechaHora, tiFrontal,   // SS_1091_CQU_20130516
                JPGItaliano, DataImage, DescriError, Error) then begin                                              // SS_1091_CQU_20130516
                ImgOverview.assign(JPGItaliano);                                                                    // SS_1091_CQU_20130516
                DrawMask(ImgOverview, DataImage.DataImageVR.UpperLeftLPN.Y - 6);                                    // SS_1091_CQU_20130516
                ImgFrontal.Picture.Assign(ImgOverview);                                                             // SS_1091_CQU_20130516
                tsFrontal1.TabVisible := true;                                                                      // SS_1091_CQU_20130516
            end else                                                                                                // SS_1091_CQU_20130516
            if ObtenerImagenTransito(FImagePathNFI, NombreCorto, NumCorrCA, FechaHora, tiFrontal, JPG12, DataImage,                              //SS-377-NDR-20110607
              DescriError, Error) then begin
                ImgOverview.assign(JPG12);
                DrawMask(ImgOverview, DataImage.DataImageVR.UpperLeftLPN.Y - 6);
                ImgFrontal.Picture.Assign(ImgOverview);
                tsFrontal1.TabVisible := true;
            end;

            if FEsItaliano and ObtenerImagenTransito(FImagePathNFI, NombreCorto, NumCorrCA, FechaHora, tiPosterior, // SS_1091_CQU_20130516
                JPGItaliano, DataImage, DescriError, Error) then begin                                              // SS_1091_CQU_20130516
                ImgOverview.assign(JPGItaliano);                                                                    // SS_1091_CQU_20130516
                DrawMask(ImgOverview, DataImage.DataImageVR.UpperLeftLPN.Y - 6);                                    // SS_1091_CQU_20130516
                ImgPosterior.Picture.Assign(ImgOverview);                                                           // SS_1091_CQU_20130516
                tsPosterior1.TabVisible := true;                                                                    // SS_1091_CQU_20130516
            end else                                                                                                // SS_1091_CQU_20130516
            if ObtenerImagenTransito(FImagePathNFI, NombreCorto, NumCorrCA, FechaHora, tiPosterior, JPG12, DataImage,                            //SS-377-NDR-20110607
              DescriError, Error) then begin
                ImgOverview.assign(JPG12);
                DrawMask(ImgOverview, DataImage.DataImageVR.UpperLeftLPN.Y - 6);
                ImgPosterior.Picture.Assign(ImgOverview);
                tsPosterior1.TabVisible := true;
            end;
            if ObtenerImagenTransito(FImagePathNFI, NombreCorto, NumCorrCA, FechaHora, tiOverview1, ImgOverview, DataImage,                      //SS-377-NDR-20110607
              DescriError, Error) then begin
                ImgOverview1.Picture.Assign(ImgOverview);
                tsOverview1.TabVisible := true;
            end;
            if ObtenerImagenTransito(FImagePathNFI, NombreCorto, NumCorrCA, FechaHora, tiOverview2, ImgOverview, DataImage,                      //SS-377-NDR-20110607
              DescriError, Error) then begin
                ImgOverview2.Picture.Assign(ImgOverview);
                tsOverview2.TabVisible := true;
            end;
            if ObtenerImagenTransito(FImagePathNFI, NombreCorto, NumCorrCA, FechaHora, tiOverview3, ImgOverview, DataImage,                      //SS-377-NDR-20110607
              DescriError, Error) then begin
                ImgOverview3.Picture.Assign(ImgOverview);
                tsOverview3.TabVisible := true;
            end;
            if FEsItaliano and ObtenerImagenTransito(FImagePathNFI, NombreCorto, NumCorrCA, FechaHora, tiFrontal2,  // SS_1091_CQU_20130516
                JPGItaliano, DataImage, DescriError, Error) then begin                                              // SS_1091_CQU_20130516
                ImgOverview.assign(JPGItaliano);                                                                    // SS_1091_CQU_20130516
                DrawMask(ImgOverview, DataImage.DataImageVR.UpperLeftLPN.Y - 6);                                    // SS_1091_CQU_20130516
                ImgFrontal2.Picture.Assign(ImgOverview);                                                            // SS_1091_CQU_20130516
                tsFrontal2.TabVisible := true;                                                                      // SS_1091_CQU_20130516
            end else                                                                                                // SS_1091_CQU_20130516
            if ObtenerImagenTransito(FImagePathNFI, NombreCorto, NumCorrCA, FechaHora, tiFrontal2, JPG12, DataImage,                             //SS-377-NDR-20110607
              DescriError, Error) then begin
                ImgOverview.assign(JPG12);
                DrawMask(ImgOverview, DataImage.DataImageVR.UpperLeftLPN.Y - 6);
                ImgFrontal2.Picture.Assign(ImgOverview);
                tsFrontal2.TabVisible := true;
            end;
            if FEsItaliano and ObtenerImagenTransito(FImagePathNFI, NombreCorto, NumCorrCA, FechaHora, tiPosterior2,    // SS_1091_CQU_20130516
                JPGItaliano, DataImage, DescriError, Error) then begin                                                  // SS_1091_CQU_20130516
                ImgOverview.assign(JPGItaliano);                                                                        // SS_1091_CQU_20130516
                DrawMask(ImgOverview, DataImage.DataImageVR.UpperLeftLPN.Y - 6);                                        // SS_1091_CQU_20130516
                ImgPosterior2.Picture.Assign(ImgOverview);                                                              // SS_1091_CQU_20130516
                tsPosterior2.TabVisible := true;                                                                        // SS_1091_CQU_20130516
            end else                                                                                                    // SS_1091_CQU_20130516
            if ObtenerImagenTransito(FImagePathNFI, NombreCorto, NumCorrCA, FechaHora, tiPosterior2, JPG12, DataImage,                           //SS-377-NDR-20110607
              DescriError, Error) then begin
                ImgOverview.assign(JPG12);
                DrawMask(ImgOverview, DataImage.DataImageVR.UpperLeftLPN.Y - 6);
                ImgPosterior2.Picture.Assign(ImgOverview);
                tsPosterior2.TabVisible := true;
            end;
        finally
            JPG12.free;
            JPGItaliano.Free;   // SS_1091_CQU_20130516
            ImgOverview.Free;
            screen.Cursor := crDefault;
            result := tsFrontal1.TabVisible or tsPosterior1.TabVisible or tsOverview1.TabVisible or
              tsOverview2.TabVisible or tsOverview3.TabVisible or tsFrontal2.TabVisible or tsPosterior2.TabVisible;
        end;
    end;                                                                //SS_1147_CQU_20150507
    if Result and not Visible then Show;
end;

function TfrmImagenesTransitos.CargarImagen(NumCorrCA: Int64; FechaHora: TDateTime; EsItaliano : Boolean): Boolean; // SS_1091_CQU_20130516
begin                                                                                                               // SS_1091_CQU_20130516
    FEsItaliano := EsItaliano;                                                                                      // SS_1091_CQU_20130516
    result :=  CargarImagen(NumCorrCA, FechaHora);                                                                  // SS_1091_CQU_20130516
end;                                                                                                                // SS_1091_CQU_20130516

  procedure TfrmImagenesTransitos.FormCreate(Sender: TObject);
begin
    //ObtenerParametroGeneral(DMConnections.BaseCAC, DIR_IMAGENES_TRANSITOS, FImagePath);                        //SS-377-NDR-20110607
    ObtenerParametroGeneral(DMConnections.BaseCAC, DIR_IMAGENES_TRANSITOS_NUEVO_FORMATO_IMAGEN, FImagePathNFI);  //SS-377-NDR-20110607
    ObtenerParametroGeneral(DMConnections.BaseCAC, DIR_IMAGENES_PATENTES_NUEVO_FORMATO_IMAGEN, FImagePlatePathNFI);    // SS_1214_CQU_20140917
    if (Mouse.CursorPos.X > (Screen.Width div 2)) then
        Left := 10
    else
        Left := (Screen.Width - Width - 10);
    if (Mouse.CursorPos.Y > (Screen.Height div 2)) then
        top := 10
    else
        top := (Screen.Height - Height - 10);
end;

procedure TfrmImagenesTransitos.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    Action := caFree;
    GVentana := nil;
end;

{----------------------------------------------------------------------------------
  Procedure     :   CargarImagenReferencia
  Author        :   CQuezadaI
  Date          :   22-09-2014
  Firma         :   SS_1214_CQU_20140917
  Description   :   Devuelve True si logra cargar AL MENOS una imagen; False si no.
-----------------------------------------------------------------------------------}
function TfrmImagenesTransitos.CargarImagenReferencia(Patente : string): Boolean;
var
	Error: TTipoErrorImg;
    JPG12: TJPEGPlusImage;
    JPGItaliano : TBitmap;
    ImgOverview: TBitMap;
	DescriError, NombreCorto: AnsiString;
    DataImage: TDataImage;
begin
    screen.Cursor := crHourGlass;
    tsFrontal1.TabVisible := False;
    tsPosterior1.TabVisible := False;
    tsOverview1.TabVisible := False;
    tsOverview2.TabVisible := False;
    tsOverview3.TabVisible := False;
    tsFrontal2.TabVisible := False;
    tsPosterior2.TabVisible := False;
    JPG12 := TJPEGPlusImage.Create;
    JPGItaliano := TBitMap.create;
    ImgOverview := TBitMap.create;
    NombreCorto := 'CN';    // BORRAR
    try
        if CargarImagenesDeReferencia(Patente) then begin       // SS_1147_CQU_20150507
            screen.Cursor := crDefault;                         // SS_1147_CQU_20150507
            Visible := True;                                    // SS_1147_CQU_20150507
            Result := True;                                     // SS_1147_CQU_20150507
        end else begin                                          // SS_1147_CQU_20150507
        // Aumento TABULADO                                     // SS_1147_CQU_20150507
            // Imagen Frontal
            if ObtenerImagenPatente(FImagePlatePathNFI, Patente, NombreCorto, tiFrontal, JPGItaliano, DataImage, DescriError, Error) then begin
                ImgOverview.assign(JPGItaliano);
                DrawMask(ImgOverview, DataImage.DataImageVR.UpperLeftLPN.Y - 6);
                ImgFrontal.Picture.Assign(ImgOverview);
                tsFrontal1.TabVisible := true;
            end else if ObtenerImagenPatente(FImagePlatePathNFI, Patente, NombreCorto, tiFrontal, JPG12, DataImage, DescriError, Error) then begin
                ImgOverview.assign(JPG12);
                DrawMask(ImgOverview, DataImage.DataImageVR.UpperLeftLPN.Y - 6);
                ImgFrontal.Picture.Assign(ImgOverview);
                tsFrontal1.TabVisible := true;
            end;

            // Imagen Posterior
            if ObtenerImagenPatente(FImagePlatePathNFI, Patente, NombreCorto, tiPosterior, JPGItaliano, DataImage, DescriError, Error) then begin
                ImgOverview.assign(JPGItaliano);
                DrawMask(ImgOverview, DataImage.DataImageVR.UpperLeftLPN.Y - 6);
                ImgPosterior.Picture.Assign(ImgOverview);
                tsPosterior1.TabVisible := true;
            end else if ObtenerImagenPatente(FImagePlatePathNFI, Patente, NombreCorto, tiPosterior, JPG12, DataImage, DescriError, Error) then begin
                ImgOverview.assign(JPG12);
                DrawMask(ImgOverview, DataImage.DataImageVR.UpperLeftLPN.Y - 6);
                ImgPosterior.Picture.Assign(ImgOverview);
                tsPosterior1.TabVisible := true;
            end;

            // Imagen Overview 1
            if ObtenerImagenPatente(FImagePlatePathNFI, Patente, NombreCorto, tiOverview1, ImgOverview, DataImage, DescriError, Error) then begin
                ImgOverview1.Picture.Assign(ImgOverview);
                tsOverview1.TabVisible := true;
            end;
            // Imagen Overview 2
            if ObtenerImagenPatente(FImagePlatePathNFI, Patente, NombreCorto, tiOverview2, ImgOverview, DataImage, DescriError, Error) then begin
                ImgOverview2.Picture.Assign(ImgOverview);
                tsOverview2.TabVisible := true;
            end;
            // Imagen Overview 3
            if ObtenerImagenPatente(FImagePlatePathNFI, Patente, NombreCorto, tiOverview3, ImgOverview, DataImage, DescriError, Error) then begin
                ImgOverview3.Picture.Assign(ImgOverview);
                tsOverview3.TabVisible := true;
            end;

            // Imagen Posterior
            if ObtenerImagenPatente(FImagePlatePathNFI, Patente, NombreCorto, tiFrontal2, JPGItaliano, DataImage, DescriError, Error) then begin
                ImgOverview.assign(JPGItaliano);
                DrawMask(ImgOverview, DataImage.DataImageVR.UpperLeftLPN.Y - 6);
                ImgFrontal2.Picture.Assign(ImgOverview);
                tsFrontal2.TabVisible := true;
            end else if ObtenerImagenPatente(FImagePlatePathNFI, Patente, NombreCorto, tiFrontal2, JPG12, DataImage, DescriError, Error) then begin
                ImgOverview.assign(JPG12);
                DrawMask(ImgOverview, DataImage.DataImageVR.UpperLeftLPN.Y - 6);
                ImgFrontal2.Picture.Assign(ImgOverview);
                tsFrontal2.TabVisible := true;
            end;

            if ObtenerImagenPatente(FImagePlatePathNFI, Patente, NombreCorto, tiPosterior2, JPGItaliano, DataImage, DescriError, Error) then begin
                ImgOverview.assign(JPGItaliano);
                DrawMask(ImgOverview, DataImage.DataImageVR.UpperLeftLPN.Y - 6);
                ImgPosterior2.Picture.Assign(ImgOverview);
                tsPosterior2.TabVisible := true;
            end else if ObtenerImagenPatente(FImagePlatePathNFI, Patente, NombreCorto, tiPosterior2, JPG12, DataImage, DescriError, Error) then begin
                ImgOverview.assign(JPG12);
                DrawMask(ImgOverview, DataImage.DataImageVR.UpperLeftLPN.Y - 6);
                ImgPosterior2.Picture.Assign(ImgOverview);
                tsPosterior2.TabVisible := true;
            end;
        end;                                                                //SS_1147_CQU_20150507
    finally
        JPG12.free;
        JPGItaliano.Free;
        ImgOverview.Free;
        screen.Cursor := crDefault;
        result :=   tsFrontal1.TabVisible
                    or tsPosterior1.TabVisible
                    or tsOverview1.TabVisible
                    or tsOverview2.TabVisible
                    or tsOverview3.TabVisible
                    or tsFrontal2.TabVisible
                    or tsPosterior2.TabVisible;
    end;
    if Result and not Visible then begin
        Caption := 'Imagen de Referencia';
        Show;
    end;
end;

{----------------------------------------------------------------------------------
  Procedure     :   CargarImagenesDelTransito
  Author        :   CQuezadaI
  Date          :   07-05-2015
  Firma         :   SS_1147_CQU_20150507
  Description   :   Obtiene la ruta de la imagen del tr�nsito desde la base de datos
                    salt�ndose las funciones normales ya que esto es s�lo un visor
                    y no hay tratamiento especial.
-----------------------------------------------------------------------------------}
function TfrmImagenesTransitos.CargarImagenesDelTransito(NumCorrCA: Int64; FechaHora: TDateTime): Boolean;
var
    spImagenesDelTransito   : TADOStoredProc;
    bResultado,
    bCargoImagen            : Boolean;
    sNombreArchivo,
    sDescriError,
    sNombreCorto            : AnsiString;
    iPosicion               : SmallInt;
  	Error                   : TTipoErrorImg;
    JPG12                   : TJPEGPlusImage;
    oDataImage              : TDataImage;
    oPicture                : TPicture;
    oImagen                 : TJpegImage;
begin
    // Inicializo los objetos
    oPicture    := TPicture.Create;
    oImagen     := TJPEGImage.Create;
    JPG12       := TJPEGPlusImage.Create;
    bResultado  := False;
    try
        try
            spImagenesDelTransito := TADOStoredProc.Create(nil);
            spImagenesDelTransito.ProcedureName := 'ObtenerRutaImagenesTransito';
            spImagenesDelTransito.Connection := DMConnections.BaseCAC;
            spImagenesDelTransito.Parameters.CreateParameter('@NumCorrCA', ftLargeint, pdInput, 0, NumCorrCA);
            spImagenesDelTransito.Parameters.CreateParameter('@SoloUna', ftBoolean, pdInput, 0, False);
            spImagenesDelTransito.Open;
            while not spImagenesDelTransito.Eof do begin
                // Obtengo el Nombre y Ruta del Archivo
                sNombreArchivo  := spImagenesDelTransito.FieldByName('RutaCompleta').AsString;
                iPosicion       := spImagenesDelTransito.FieldByName('Posicion').AsInteger;
                sNombreCorto    := spImagenesDelTransito.FieldByName('Nivel2').AsString;
                FImagePathNFI   := spImagenesDelTransito.FieldByName('Directorio').AsString;
                // Valido que exista y pese m�s que 0
                if FileExists(sNombreArchivo) and (GetFileSize(sNombreArchivo) > 0) then
                begin
                    try
                        // Intento cargar la imagen.
                        oImagen.LoadFromFile(sNombreArchivo);
                        // La cargo en el objeto final.
                        oPicture.Bitmap.Assign(oImagen);
                        // Logr� cargar la imagen.
                        bCargoImagen := True;
                    except
                        bCargoImagen := False;
                    end;
                    if not bCargoImagen then begin
                        try
                            // Si no es una imagen comun, carga en formato Kapsch (Jpeg12)
                            if ObtenerImagenTransito(FImagePathNFI, sNombreCorto, NumCorrCA,
                                                    FechaHora, TTipoImagen(iPosicion), JPG12,
                                                    oDataImage, sDescriError, Error)
                            then begin
                                oPicture.Bitmap.Assign(JPG12);
                                bCargoImagen := True;
                            end else bCargoImagen := False;
                        except
                            bCargoImagen := False;
                        end;
                    end;
                    if bCargoImagen then begin
                        // La m�scara s�lo va en las frontales y posteriores
                        if      (TTipoImagen(iPosicion) = tiFrontal)
                            or  (TTipoImagen(iPosicion) = tiFrontal2)
                            or  (TTipoImagen(iPosicion) = tiPosterior)
                            or  (TTipoImagen(iPosicion) = tiPosterior2)
                        then begin
                            if oDataImage.DataImageVR.UpperLeftLPN.Y > 0 then
                                DrawMask(oPicture.Bitmap, oDataImage.DataImageVR.UpperLeftLPN.Y - 6)
                            else
                                DrawMask(oPicture.Bitmap, 0);
                        end;
                        // La imagen se carg� en el objeto as� que se puede cargar en los visores.
                        case TTipoImagen(iPosicion) of
                            tiFrontal : begin
                                tsFrontal1.TabVisible := True;
                                ImgFrontal.Picture.Assign(oPicture);
                            end;
                            tiFrontal2 : begin
                                tsFrontal2.TabVisible := True;
                                ImgFrontal2.Picture.Assign(oPicture);
                            end;
                            tiOverview1 : begin
                                tsOverview1.TabVisible := True;
                                imgOverview1.Picture.Assign(oPicture);
                            end;
                            tiOverview2 : begin
                                tsOverview2.TabVisible := True;
                                imgOverview2.Picture.Assign(oPicture);
                            end;
                            tiOverview3 : begin
                                tsOverview3.TabVisible := True;
                                imgOverview3.Picture.Assign(oPicture);
                            end;
                            tiPosterior : begin
                                tsPosterior1.TabVisible := True;
                                imgPosterior.Picture.Assign(oPicture);
                            end;
                            tiPosterior2 : begin
                                tsPosterior2.TabVisible := True;
                                imgPosterior2.Picture.Assign(oPicture);
                            end;
                        else
                            raise Exception.Create('El Tipo de Imagen No Existe');
                        end; // EndCase
                    end; // end if bCargoImagen
                end; // end if FileExists()
                spImagenesDelTransito.Next;
            end; // end While
            spImagenesDelTransito.Close;
        except

        end;
    finally
        spImagenesDelTransito.Free;
        JPG12.Free;
        oPicture.Free;
        oImagen.Free;
        Result := tsFrontal1.TabVisible
                or tsPosterior1.TabVisible
                or tsOverview1.TabVisible
                or tsOverview2.TabVisible
                or tsOverview3.TabVisible
                or tsFrontal2.TabVisible
                or tsPosterior2.TabVisible;
    end;
end;

{----------------------------------------------------------------------------------
  Procedure     :   CargarImagenesDeReferencia
  Author        :   CQuezadaI
  Date          :   14-05-2015
  Firma         :   SS_1147_CQU_20150507
  Description   :   Obtiene la ruta de la imagen de referencia desde la base de datos
                    salt�ndose las funciones normales ya que esto es s�lo un visor
                    y no hay tratamiento especial.
-----------------------------------------------------------------------------------}
function TfrmImagenesTransitos.CargarImagenesDeReferencia(Patente : string): Boolean;
var
    spImagenesDeReferencia      : TADOStoredProc;
    bResultado, bCargoImagen    : Boolean;
    sNombreArchivo,
    sDescriError,
    sNombreCorto                : AnsiString;
    iPosicion                   : SmallInt;
  	Error                       : TTipoErrorImg;
    JPG12                       : TJPEGPlusImage;
    oDataImage                  : TDataImage;
    oPicture                    : TPicture;
    oImagen                     : TJpegImage;
begin
    // Inicializo los objetos
    JPG12       := TJPEGPlusImage.Create;
    oPicture    := TPicture.Create;
    oImagen     := TJPEGImage.Create;
    bResultado  := False;
    try
        try
            spImagenesDeReferencia := TADOStoredProc.Create(nil);
            spImagenesDeReferencia.ProcedureName := 'ObtenerRutaImagenReferencia';
            spImagenesDeReferencia.Connection := DMConnections.BaseCAC;
            spImagenesDeReferencia.Parameters.CreateParameter('@Patente', ftString, pdInput, 10, Patente);
            spImagenesDeReferencia.Open;
            while not spImagenesDeReferencia.Eof do begin
                // Obtengo el Nombre y Ruta del Archivo
                sNombreArchivo  := spImagenesDeReferencia.FieldByName('RutaCompleta').AsString;
                FImagePathNFI   := spImagenesDeReferencia.FieldByName('Directorio').AsString;
                sNombreCorto    := spImagenesDeReferencia.FieldByName('Nivel1').AsString;
                iPosicion       := spImagenesDeReferencia.FieldByName('Posicion').AsInteger;
                // Valido que exista y pese m�s que 0
                if FileExists(sNombreArchivo) and (GetFileSize(sNombreArchivo) > 0) then
                begin
                    try
                        // Intento cargar la imagen.
                        oImagen.LoadFromFile(sNombreArchivo);
                        // La cargo en el objeto final.
                        oPicture.Bitmap.Assign(oImagen);
                        // Logr� cargar la imagen.
                        bCargoImagen := True;
                    except
                        bCargoImagen := False;
                    end;
                    if not bCargoImagen then begin
                        try
                            // Si no es una imagen comun, carga en formato Kapsch (Jpeg12)
                            if ObtenerImagenPatente(FImagePathNFI, Patente, sNombreCorto, TTipoImagen(iPosicion), JPG12, oDataImage, sDescriError, Error)
                            then begin
                                oPicture.Bitmap.Assign(JPG12);
                                bCargoImagen := True;
                            end else bCargoImagen := False;
                        except
                            bCargoImagen := False;
                        end;
                    end;
                    if bCargoImagen then begin
                        // La m�scara s�lo va en las frontales y posteriores
                        if      (TTipoImagen(iPosicion) = tiFrontal)
                            or  (TTipoImagen(iPosicion) = tiFrontal2)
                            or  (TTipoImagen(iPosicion) = tiPosterior)
                            or  (TTipoImagen(iPosicion) = tiPosterior2)
                        then begin
                            if oDataImage.DataImageVR.UpperLeftLPN.Y > 0 then
                                DrawMask(oPicture.Bitmap, oDataImage.DataImageVR.UpperLeftLPN.Y - 6)
                            else
                                DrawMask(oPicture.Bitmap, 0);
                        end;
                        // La imagen se carg� en el objeto as� que se puede cargar en los visores.
                        case TTipoImagen(iPosicion) of
                            tiFrontal : begin
                                tsFrontal1.TabVisible := True;
                                ImgFrontal.Picture.Assign(oPicture);
                            end;
                            tiFrontal2 : begin
                                tsFrontal2.TabVisible := True;
                                ImgFrontal2.Picture.Assign(oPicture);
                            end;
                            tiOverview1 : begin
                                tsOverview1.TabVisible := True;
                                imgOverview1.Picture.Assign(oPicture);
                            end;
                            tiOverview2 : begin
                                tsOverview2.TabVisible := True;
                                imgOverview2.Picture.Assign(oPicture);
                            end;
                            tiOverview3 : begin
                                tsOverview3.TabVisible := True;
                                imgOverview3.Picture.Assign(oPicture);
                            end;
                            tiPosterior : begin
                                tsPosterior1.TabVisible := True;
                                imgPosterior.Picture.Assign(oPicture);
                            end;
                            tiPosterior2 : begin
                                tsPosterior2.TabVisible := True;
                                imgPosterior2.Picture.Assign(oPicture);
                            end;
                        else
                            raise Exception.Create('El Tipo de Imagen No Existe');
                        end; // EndCase
                    end; // end if bCargoImagen
                end; // end if FileExists()
                spImagenesDeReferencia.Next;
            end; // end While
            spImagenesDeReferencia.Close;
        except

        end;
    finally
        spImagenesDeReferencia.Free;
        oPicture.Free;
        oImagen.Free;
        JPG12.Free;
        Result := tsFrontal1.TabVisible
                or tsPosterior1.TabVisible
                or tsOverview1.TabVisible
                or tsOverview2.TabVisible
                or tsOverview3.TabVisible
                or tsFrontal2.TabVisible
                or tsPosterior2.TabVisible;;
    end;
end;

end.
