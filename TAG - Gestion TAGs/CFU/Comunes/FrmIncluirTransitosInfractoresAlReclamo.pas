{-------------------------------------------------------------------------------
 File Name: FrmIncluirTransitosInfractoresAlReclamo.pas
 Author: lgisuk
 Date Created: 27/06/05
 Language: ES-AR
 Description: Informo que se encontraron tr�nsitos infractores pertenecientes
              a las patentes y fechas seleccionadas. y permito incluirlos al
              reclamo.

Author      : Claudio Quezada Ib��ez
Date        : 16-Mayo-2013
Firma       : SS_1091_CQU_20130516
Description : Se modifica la visualizaci�n de im�genes para poder desplegar im�genes de p�rticos Italianos.
-------------------------------------------------------------------------------}
unit FrmIncluirTransitosInfractoresAlReclamo;

interface

uses
  //Reclamo
  DMConnection,                   //Coneccion a base de datos OP_CAC
  Util,                           //CRLF
  UtilProc,                       //Mensajes
  ImagenesTransitos,              //Muestra la Ventana con la imagen
  //General
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls,  DB, ADODB,RStrings, ExtCtrls,
  VariantComboBox, Grids, DBGrids, ListBoxEx, DBListEx, ImgList, Menus;

type
  TFormIncluirTransitosInfractoresAlReclamo = class(TForm)
    DataSource: TDataSource;
    spObtenerTransitosInfractoresAReclamar: TADOStoredProc;
    DBLTransitos: TDBListEx;
    Ltitulo: TLabel;
    Lmensaje: TLabel;
    PBotonera: TPanel;
    SIBTN: TButton;
    btnNo: TButton;
    CancelBTN: TButton;
    ilCamara: TImageList;
    procedure SIBTNClick(Sender: TObject);
    procedure btnNoClick(Sender: TObject);
    procedure DBLTransitosDrawText(Sender: TCustomDBListEx;Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;var Text: String; var TxtRect: TRect; var ItemWidth: Integer;var DefaultDraw: Boolean);
    procedure DBLTransitosLinkClick(Sender: TCustomDBListEx;Column: TDBListExColumn);
    procedure CancelBTNClick(Sender: TObject);
  private
    { Private declarations }
    FListaTransitos: TStringList;
    FListaTodos: TStringList;
    FLista: TStringList;
    function GetLista: TStringList;
  public
    { Public declarations }
    function Inicializar(ListaTransitos:tstringlist): Boolean;
    property Lista: TStringList read GetLista;
  end;

var
  FormIncluirTransitosInfractoresAlReclamo: TFormIncluirTransitosInfractoresAlReclamo;

implementation

{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    lgisuk
  Date Created: 27/06/2005
  Description: Incializacion de este formulario
  Parameters: ListaTransitos:tstringlist
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFormIncluirTransitosInfractoresAlReclamo.Inicializar(ListaTransitos : TStringList) : Boolean;

    {-----------------------------------------------------------------------------
      Function Name: ObtenerDatosTransitos
      Author:    lgisuk
      Date Created: 27/06/2005
      Description: Cargo la grilla de reclamos
      Parameters: Transitos:string
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function ObtenerDatosTransitos(Transitos : TStringList) : Boolean;

            {----------------------------------------------------------------
              Function Name: ObtenerEnumeracion
              Author:    lgisuk
              Date Created: 27/06/2005
              Description: Convierto la lista a un string separado por comas
              Parameters: Transitos:string
              Return Value: string
            -----------------------------------------------------------------}
            function ObtenerEnumeracion(Lista: TStringList): String;
            var
                Enum: AnsiString;
                i: Integer;
            begin
                Enum := '';
                for i:= 0 to Lista.Count - 1 do begin
                    //Enum := Enum + Lista.Strings[i] + ',';
                    Enum := Enum + ParseParamByNumber(Lista.Strings[i],1,'=') + ',';
                end;
                Delete(Enum, Length(Enum), 1);
                Result := Enum;
            end;

    begin
        with SPObtenerTransitosInfractoresAReclamar do begin
            close;
            parameters.Refresh;
            parameters.ParamByName('@Enumeracion').value := ObtenerEnumeracion(Transitos);
            open;
        end;
        result:=true;
    end;

Resourcestring
    MSG_ERROR_INIT =  'Error al inicializar Incluir transitos infractores al reclamo';
    MSG_ERROR = 'Error';
Const
    STR_MENSAJE = 'Todos los tr�nsitos seleccionados estan incluidos en reclamos anteriores!';
begin
    try
        //Si es un reclamo nuevo obtengo los reclamos del stringlist
        if ListaTransitos <> nil then begin
            //Cargo la grilla con los transitos
            ObtenerDatosTransitos(ListaTransitos);
        end;
        FListaTransitos := ListaTransitos;
        //Centro el form
        CenterForm(Self);
        //Titulo
        ActiveControl := dbltransitos;
        //creo la lista
        FListaTodos := TstringList.Create;
        Result := true;
    except
        on E: Exception do begin
            MsgBoxErr(MSG_ERROR_INIT, E.Message, MSG_ERROR, MB_ICONSTOP);
            Result := False;
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: DBLTransitosDrawText
  Author:    lgisuk
  Date Created: 27/06/2005
  Description: doy formato a la grilla
  Parameters: Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;State: TOwnerDrawState; var Text: String; var TxtRect: TRect;var ItemWidth: Integer; var DefaultDraw: Boolean
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormIncluirTransitosInfractoresAlReclamo.DBLTransitosDrawText(Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;State: TOwnerDrawState; var Text: String; var TxtRect: TRect;var ItemWidth: Integer; var DefaultDraw: Boolean);

    {-----------------------------------------------------------------------------
      Function Name: DibujarBitmap
      Author:    lgisuk
      Date Created: 29/06/2005
      Description: Dibujo el bitmap seleccionado en la columna de la grilla
      Parameters: Grilla:TCustomDBlistEx;ImageList:TimageList;numero:integer
      Return Value: boolean
    -----------------------------------------------------------------------------}
    Function DibujarBitmap(Grilla: TCustomDBlistEx; ImageList: TimageList; IndiceImg: integer) : Boolean;
    var
        Bmp: TBitMap;
    begin
        Bmp := TBitMap.Create;
        try
            ImageList.GetBitmap(IndiceImg, Bmp);
            Grilla.Canvas.Draw((Rect.Left + Rect.Right - Bmp.Width) div 2, Rect.Top, Bmp);
            Result := true;
        finally
            Bmp.Free;
        end;
    end;
    
var
    I : Integer;
begin
    //FechaHora
    if Column = dbltransitos.Columns[2] then begin
       if text <> '' then Text := FormatDateTime('dd/mm/yyyy hh:nn', SPObtenerTransitosInfractoresAReclamar.FieldByName('FechaHora').AsDateTime);
    end;

    //si el transito tiene imagen le crea un link para que se pueda acceder a verla
    if Column = dblTransitos.Columns[3] then begin
        I := iif(SPObtenerTransitosInfractoresAReclamar.FieldByName('RegistrationAccessibility').AsInteger > 0, 1, 0);
        //Dibujo el icono correspondiente
        DibujarBitmap(dbltransitos, ilCamara, i);
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: DBLTransitosLinkClick
  Author:    lgisuk
  Date Created: 27/06/2005
  Description: permito rechazar o aceptar un transito
  Parameters: Sender: TCustomDBListEx; Column: TDBListExColumn
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormIncluirTransitosInfractoresAlReclamo.DBLTransitosLinkClick(Sender: TCustomDBListEx; Column: TDBListExColumn);
begin
    //Mostrar Imagen del Transito
    if Column = dblTransitos.Columns[3] then begin
        //Si hay imagen
        if SPObtenerTransitosInfractoresAReclamar.FieldByName('RegistrationAccessibility').AsInteger > 0 then begin
            //La muestra
            //MostrarVentanaImagen(Self,SPObtenerTransitosInfractoresAReclamar['NumCorrCA'], SPObtenerTransitosInfractoresAReclamar['FechaHora']);  // SS_1091_CQU_20130516
            MostrarVentanaImagen(Self,                                                                                                              // SS_1091_CQU_20130516
                SPObtenerTransitosInfractoresAReclamar['NumCorrCA'],                                                                                // SS_1091_CQU_20130516
                SPObtenerTransitosInfractoresAReclamar['FechaHora'],                                                                                // SS_1091_CQU_20130516
                SPObtenerTransitosInfractoresAReclamar['EsItaliano']);                                                                              // SS_1091_CQU_20130516
        end;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: SIBTNClick
  Author:    lgisuk
  Date Created: 27/06/2005
  Description: Incluyo los los transitos propuestos al reclamo
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormIncluirTransitosInfractoresAlReclamo.SIBTNClick(Sender: TObject);
begin
    //Si el sp no esta abierto salgo
    if spObtenerTransitosInfractoresAReclamar.Active = false then exit;
    with spObtenerTransitosInfractoresAReclamar do begin
        DisableControls;
        First;
        //Recorro los Transitos
        while not eof do begin
            //Los cargo a la lista
            FListaTodos.Add(FieldByName('NumCorrCA').AsString);
            Next;
        end;
        EnableControls;
    end;
    //Incluyo los transitos propuestos
    Flista := FListaTodos;
    ModalResult := mrYes;
end;

{-----------------------------------------------------------------------------
  Function Name: btnNoClick
  Author:    lgisuk
  Date Created: 27/06/2005
  Description: No Incluyo los transitos propuestos al reclamo
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormIncluirTransitosInfractoresAlReclamo.btnNoClick(Sender: TObject);
begin
    //No incluyo los transitos propuestos
    Flista := FListaTransitos;
    ModalResult := mrNO;
end;

{-----------------------------------------------------------------------------
  Function Name: CancelBTNClick
  Author:    lgisuk
  Date Created: 27/06/2005
  Description: Cancelo el reclamo Desconoce Infraccion
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFormIncluirTransitosInfractoresAlReclamo.CancelBTNClick(Sender: TObject);
begin
    //Devuelvo la lista vacia
    Flista := FListaTodos;
    ModalResult:= mrCancel;
end;

{-----------------------------------------------------------------------------
  Function Name: GetLista
  Author:    lgisuk
  Date Created: 27/06/2005
  Description: Obtengo la lista de transitos seleccionada por el operador
  Parameters: None
  Return Value: integer
-----------------------------------------------------------------------------}
Function TFormIncluirTransitosInfractoresAlReclamo.GetLista: TStringList;
Resourcestring
    MSG_ERROR = 'Error!. No se pudo obtenter la lista de transitos seleccionada por el operador.';
begin
    if not Assigned(Flista) then begin
        Raise Exception.Create(MSG_ERROR);
        Result := nil;
        Exit;
    end;
    Result := FLista;
end;

end.
