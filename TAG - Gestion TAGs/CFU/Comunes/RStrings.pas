unit RStrings;
{********************************** File Header ********************************
File Name   : RStrings.pas
Author      :
Date Created:
Language    : ES-AR
Description :

Revision 		: 1
Author      : mpiazza
Date        : 09/03/2010
Description : ss-870 : se agrega el mensaje MSG_ERROR_TURNO_SUSPENDER_VEHICULO

Revision : 2
    Author: pdominguez
    Date  : 30/06/2010
    Description: SS 900 - Error cambio almacen m�dulo Gesti�n Telev�a
        - Se a�ade el resourcestring MSG_ERROR_CANTIDAD_TAGS_PARAMETROS.

Revision : 4
    Author: ebaeza
    Date : 05/01/2011
    Descripci�n: SS 786 - Reversar Patente Liberada  (// Rev.4 SS 786 20110105 EBA)
        - Se a�aden 4 resourcestring
                        -    CAPTION_REVERSA_PATENTE 
                        -    MSG_PATENTE_REVERSADA_EXITOSAMENTE
                        -    MSG_CONFIRMA_REVERSA_PATENTE
                        -    MSG_PATENTE_PARA_REVERSAR

Revision : 5
Firma       : SS-1092-MVI-20130627
Descripcion : Se cambia el valor de la variable MSG_VALIDAR_DIRECCION_EMAIL
             de 'La direcci�n de Mail indicada es incorrecta'
             a 'La direcci�n de email no es v�lida';

Firma		: CAC-CLI-066_MGO_20160218
Descripcion	: Se agrega FLD_TIPO_CORRESPONDENCIA
*******************************************************************************}

interface

const
    FIN_LINEA = #13#10;

resourcestring

	BTN_CAPTION_CODIFICAR  				= 'Codificar';

	FLD_ANCHO_COLUMNA					= 'Ancho de Columna';
	FLD_APELLIDO 						= 'Apellido';
    FLD_APELLIDO_MATERNO 		        = 'Apellido Materno';
    FLD_ANEXO                           = 'Anexo';
    FLD_BANCO                           = 'Banco';
	FLD_CALLE                           = 'Calle';
	FLD_CATEGORIA 						= 'Categor�a';
	FLD_CATEGORIA_EMPLEADO 				= 'Categor�a de Empleado';
	FLD_CENTRO_COSTOS 					= 'Centro de Costos';
	FLD_CIUDAD							= 'Ciudad';
	FLD_CLIENTE							= 'Cliente';
	FLD_CODIGO_DOCUMENTO 				= 'C�digo de Documento';
	FLD_CODIGO_ORIGINAL 				= 'C�digo Original';
    FLD_CODIGO_POSTAL                   = 'C�digo Postal';
	FLD_COLOR 							= 'Color';
    FLD_COMBINACION_PATENTE             = 'Combinaci�n de Patente';
	FLD_COMUNA 							= 'Comuna';
	FLD_CONTACTO_COMERCIAL 		        = 'Contacto Comercial';
	FLD_CUENTA							= 'Cuenta';
    FLD_CODIGO_AREA                     = 'C�digo de �rea';
	FLD_DESCRIPCION 					= 'Descripci�n';
	FLD_DESTINO 						= 'Destino';
    FLD_DETALLE                         = 'Detalle';
	FLD_DIAS 							= 'D�as';
    FLD_DIGITO                          = 'D�gito Verificador';
	FLD_DOMICILIO 						= 'Domicilio';
	FLD_DOMICILIO_RELACIONADO 			= 'Domicilio Relacionado';
	FLD_EQUIPO 							= 'Equipo';
	FLD_ESTACION_MANTENIMIENTO 			= 'Estaci�n de Mantenimiento';
	FLD_FECHA 							= 'Fecha';
    FLD_FECHA_NACIMIENTO        		= 'Fecha de Nacimiento:';
    FLD_FORMULARIO                      = 'Formularios';
	FLD_GENERO 							= 'G�nero';
	FLD_HORA 							= 'Hora';
	FLD_MARCA 							= 'Marca';
	FLD_MATERIAL 						= 'Material';
	FLD_MEDIO_CONTACTO 					= 'Medio de Contacto';
    FLD_MEDIO_PAGO                      = 'Medio de Pago.';
	FLD_MESES 							= 'Meses';
	FLD_MODELO_MATERIAL 				= 'Modelo de Material';
	FLD_NOMBRE	 						= 'Nombre';
    FLD_NOMBRE_FANTASIA                 = 'Nombre de Fantas�a';
	FLD_NUMERO							= 'N�mero';
    FLD_NUMERO_CALLE                    = 'N�mero de Calle';
    FLD_NUMERO_CUENTA                   = 'N�mero de Cuenta';
	FLD_ORDEN 							= '�rden';
	FLD_ORDEN_COMPRA					= 'Orden de Compra';
	FLD_PREGUNTA						= 'Pregunta';
    FLD_PERSONERIA						= 'Personeria';
    FLD_RAZON_SOCIAL 			        = 'Raz�n Social';
	FLD_REGION 							= 'Regi�n';
	FLD_SECTOR							= 'Sector';
	FLD_SEMANAS 						= 'Semanas';
    FLD_SEXO                            = 'Sexo';
    FLD_GIRO                            = 'Giro';
	FLD_SUBTITULO						= 'SubT�tulo';
    FLD_SUCURSAL                        = 'Sucursal';
	FLD_TAG								= 'Telev�a';
	FLD_TIPO_CALLE						= 'Tipo de Calle';
	FLD_TIPO_CAUSA						= 'Tipo de Causa';
    FLD_TOP_COMBINACION                 = 'Tipo de Combinaci�n';
	FLD_TIPO_FALLA 						= 'Tipo de Falla';
	FLD_TIPO_MANTENIMIENTO				= 'Tipo de Mantenimiento';
	FLD_TIPO_MATERIAL 					= 'Tipo de Material';
	FLD_TIPO_ORDEN 						= 'Tipo de Orden';
	FLD_TIPO_UBICACION 					= 'Tipo de Ubicaci�n';
    FLD_TIPO_TARJETA                    = 'Tipo de Tarjeta';
    FLD_TIPO_CORRESPONDENCIA            = 'Tipo de Correspondencia';				//CAC-CLI-066_MGO_20160218
    FLD_TIPO_EMISOR                     = 'Emisor de Tarjeta de Credito';
    FLD_NRO_TARJETA                     = 'N�mero de Tarjeta';
    FLD_MONTO                           = 'Monto';
    FLD_AUTORIZACION                    = 'Autorizaci�n';
    FLD_CUPON                     		= 'Cup�n';
    FLD_CUOTAS                          = 'Cuotas';
    FLD_TITULAR                         = 'Titular';
    FLD_TIPO_TELEFONO                   = 'Tipo de Telefono';
	FLD_TITULO							= 'T�tulo';
	FLD_VEHICULOS                       = 'Veh�culo';
	FLD_ZONA		 					= 'Zona';
	FLD_ZONA_MANTENIMIENTO 				= 'Zona de Mantenimiento';
	FLD_ZONA_SECTOR 					= 'Zona/Sector';
    FLD_FUENTE_ORIGEN_CONTACTO          = 'Fuente de origen del contacto';
    FLD_ORDEN_MOSTRAR                   = 'Orden a mostrar';
    FLD_SR                              = 'Sr.';  //SE�OR
    FLD_SRA                             = 'Sra.'; //SE�ORA

	HINT_BACKOFFICE 					= 'BackOffice';
	HINT_MANTENIMIENTO 					= 'Configuraci�n del Sistema';
	HINT_PERSONAL 						= 'Administraci�n de Personal';
	HINT_STOCK 							= 'Administraci�n de Stock';


	LBL_PERSONA_APELLIDO 				= 'Apellido:';
    LBL_PERSONA_APELLIDO_CONTACTO       = 'Apellido Contacto:';
	LBL_PERSONA_APELLIDO_MATERNO 		= 'Apellido Materno:';
	LBL_PERSONA_CONTACTO_COMERCIAL 		= 'Contacto Comercial:';
	LBL_PERSONA_FECHA_CREACION 			= 'Fecha de Creaci�n:';
	LBL_PERSONA_FECHA_NACIMIENTO 		= 'Fecha de Nacimiento:';
	LBL_PERSONA_NOMBRE 					= 'Nombre:';
    LBL_PERSONA_NOMBRE_CONTACTO         = 'Nombre Contacto:';
	LBL_PERSONA_NOMBRE_FANTASIA 		= 'Nombre de Fantas�a:';
	LBL_PERSONA_RAZON_SOCIAL 			= 'Raz�n Social:';
	LBL_PERSONA_SEXO 					= 'Sexo:';
	LBL_PERSONA_SITUACION_IVA 			= 'Situaci�n de IVA:';
	LBL_TITULO_MANTENIMIENTO 			= 'Sistema de Mantenimiento';

	MSG_ADMINISTRACION 					= 'Administraci�n de %s';
	MSG_APTITUD_LOTE_OK					= 'La aptitud de calidad del lote' + #13 + ' fue actualizada satisfactoriamente.';
	MSG_BACKOFFICE 						= 'BackOffice';
	MSG_CAMBIAR_UBICACION    			= 'Cambiar ubicaci�n de Tags';
	MSG_CAMBIO_CONTRASENIA 				= 'Cambio de contrase�a';
	MSG_CAMBIO_UBICACION_INVALIDO		= 'No se puede asignar la ubicaci�n en poder del cliente sin indicar el mismo.';
    MSG_ESTA_SEGURO_SIN_VEHICULOS       = '�Desea guardar la solicitud sin haber cargado veh�culos?';
    MSG_ERROR_SOLICITUD_CONTACTO        = 'Error al guardar la solicitud';
    MSG_ERROR_SOLICITUD_CONTRATO        = 'Error al guardar el contrato';
    MSG_ERROR_SOLICITUD_CONVENIO        = 'Error al guardar el Convenio';
    MSG_ERROR_NO_HAY_CONVENIO           = 'No hay Convenios para ser informados al RNUT.';
    MSG_ERROR_EXPORTAR_CONVENIO         = 'Error al Exportar Convenio.';
    MSG_ERROR_OBTENER_MEDIOENVIODOCUMENTOCOBRO = 'Error al obtener los c�digos para guardar la configuraci�n de los env�os de documentos de cobro' + #13#10 +
                                                'Consulte al administrador.';
    MSG_ERROR_CONECCION                 = 'No se puede conectar a la base de datos, verifique las conexiones e intente nuevamente';
    MSG_ERROR_INVOCAR_CORREO_PREDETERMINADO = 'No se pudo invocar al programa de correo predeterminado.';
    MSG_ERROR_DETALLE_INVOCAR_CORREO_PREDETERMINADO = 'Verifique si posee un programa de correo o su configuraci�n';
    MSG_CAPTION_ENVIO                   = 'Envio de Email';
    MSG_ERROR_MEDIO_PAGO                = 'Especifique otro medio de Pago';

    MSG_CAPTION_MEDIO_ENVIO_DOCUMENTOS_COBRO = 'Medio de env�o de los documentos de cobro';
    MSG_ERROR_OBTENER_PERSONA           = 'Error al obtener Datos de la Persona';
	MSG_CAPTION_ACERCA_DE_GESTION_TAGS 	= 'Gesti�n de Tags';
    MSG_CAPTION_ASIGNAR_TAG             = 'Asignar telev�a';
	MSG_CAPTION_ACTUALIZAR 				= 'Actualizar %s';
	MSG_CAPTION_AGREGAR 				= 'Agregar %s';
    MSG_CAPTION_AGREGAR_MULTIPLE        = 'Agregar %s de: %s para: %s';
	MSG_CAPTION_BAJA 					= 'Dar de baja %s';
    MSG_CAPTION_BAJA_CONVENIO           = 'La Baja del Convenio se Realizo Exitosamente.';
	MSG_CAPTION_BUSCAR_PERSONA 			= 'Buscar Persona';
	MSG_CAPTION_BUSQUEDA				= 'B�squeda de %s';
	MSG_CAPTION_CAMBIO_UBICACION		= 'Cambio de Ubicaci�n';
    MSG_CAPTION_CARGA_CAMPOS_OBLIGATORIO= 'Carga de Obligatoriedad de los campos.';
    MSG_CAPTION_GRAVAR_CAMPOS_OBLIGATORIO= 'Almacenamiento de Obligatoriedad de los campos.';
	MSG_CAPTION_CANCELAR				= 'Cancelar %s';
	MSG_CAPTION_CODIFICACION			= 'Codificaci�n de %s';
	MSG_CAPTION_CODIFICACION_TAGS		= 'Codificaci�n de TAGs  -  %s';
	MSG_CAPTION_CONFIGURAR_ANTENA		= 'Configurar Antena de TAG';
	MSG_CAPTION_CONTROL_CALIDAD_LOTE	= 'Control de Calidad, Lote: %d    Cantidad Total de Tags: %d';
	MSG_CAPTION_CONTROL_CALIDAD			= 'Control de Calidad';
	MSG_CAPTION_CUENTAS					= 'Cuentas: %s ';
//	MSG_CAPTION_DOMICILIO_RELACIONADO 	= 'No se pudo actualizar el %s';
    MSG_CAPTION_DOMICILIO               = 'Datos Domicilio';
    MSG_CAPTION_ERROR_MEDIO_PAGO        = 'Medio de pago Cargado';
	MSG_CAPTION_EDITAR 					= 'Modificar %s';
	MSG_CAPTION_EDITAR_CONSULTA			= 'Consulta SQL';
	MSG_CAPTION_ELIMINAR 				= 'Eliminar %s';
	MSG_CAPTION_ENTREGAR_TAG			= 'Entregar TAG.';
    MSG_CAPTION_GESTION                 = 'Gesti�n de %s.';
	MSG_CAPTION_GRABAR					= 'Grabar';
	MSG_CAPTION_GUARDAR_REGISTRO_PRUEBAS= 'Grabar Registro de Pruebas';
	MSG_CAPTION_HABILITACION_TAGS		= 'Habilitaci�n de TAGs';
    MSG_CAPTION_IMPORTAR_TAGS			= 'Importaci�n de telev�a desde Archivo del Proveedor';
    MSG_CAPTION_IMPORTAR_VEHICULOS      = 'Importar Veh�culos';
    MSG_CAPTION_IMPRESION_CONVENIOS     = 'Impresi�n de Convenios';
    MSG_CAPTION_IMPRESION_OK            = 'Se termino de imprimir el Convenio';
	MSG_CAPTION_INGRESAR_TAGS			= 'Ingresar TAGs';
	MSG_CAPTION_INGRESO_LOTE_TAGS		= 'Ingreso de TAGs a almac�n. Lote: %d';
	MSG_CAPTION_INHABILITACION_TAGS		= 'Inhabilitaci�n de TAGs';
	MSG_CAPTION_INICIALIZACION			= 'Inicializar %s';
	MSG_CAPTION_INICIALIZAR_HARDWARE 	= 'Inicializar el Hardware';
    MSG_CAPTION_LISTSOLICITUD           = 'Listado de Solicitudes.';
    MSG_CAPTION_LIST                    = 'Listado de %S.';
	MSG_CAPTION_OBTENER					= 'Obtener datos de %s';
	MSG_CAPTION_RECEPCION_TAGS			= 'Recepci�n de TAGs';
	MSG_CAPTION_REGISTRO_CONFORME		= 'Registrar Conforme';
    MSG_CAPTION_VALIDA					= 'Validar datos';
	MSG_CAPTION_VALIDAR					= 'Validar datos de %S';
    MSG_CAPTION_VALIDAR_DOCUMENTACION   = 'Validar documentaci�n';
    MSG_CAPTION_AGREGAR_VEHICULO        = 'Agregar Veh�culo';
    MSG_CAPTION_EDITAR_VEHICULO         = 'Editar Veh�culo';
    MSG_CAPTION_CAMBIAR_VEHICULO        = 'Cambiar Veh�culo';
    MSG_CAPTION_ACTIVAR_CUENTA          = 'Activar Cuenta';
    MSG_CAPTION_DIGITO_VERIFICADOR      = 'D�gito Verificador';
    MSG_CAPTION_PERSONA_EXISTE          = 'La persona ya est� registrada.'  + #13#10 + 'Cualquier modificaci�n alterar�n los datos previamente ingresados.';
    MSG_CAPTION_PERSONA_REGISTRADA      = 'La persona ya est� registrada. Los datos no podr�n ser modificados.';
    MSG_CAPTION_PERSONA_CONVENIO        = 'Esta persona pertenece a un convenio, por lo cual sus datos no pueden ser modificados por este medio.';
    MSG_CAPTION_INGRESE_CONTRASENA      = 'Ingrese contrase�a';
    MSG_CAPTION_TURNO_ABIERTO           = 'El turno ha sido abierto con �xito.';
    MSG_CAPTION_TURNO_ADICION           = 'Los telev�a han sido adicionados con �xito.';
    MSG_CAPTION_TURNO_CERRADO           = 'El turno ha sido cerrado con �xito. Ahora se imprimir� el reporte de cierre.';
    MSG_CAPTION_RECEPCION               = 'La recepci�n ha sido realizada con �xito.';
    MSG_CAPTION_VER_CONVENIO            = 'Ver Convenio';
    MSG_CAPTION_PAPEL_BLANCO            = 'Seleccione la bandeja que tiene la hoja en blanco en la impresora por default de Windows';
    MSG_CAPTION_PAPEL_CONVENIO          = 'Seleccione la bandeja que tiene la hoja del convenio en la impresora por default de Windows';
    MSG_CAPTION_ESTADO_TURNO_ABIERTO    = 'TURNO ABIERTO';
    MSG_CAPTION_ESTADO_TURNO_CERRADO    = 'TURNO CERRADO';
    MSG_CAPTION_LLAMADA_REGISTRADA      = 'La llamada se ha registrado con �xito.';
    MSG_CAPTION_MODIFICAR_CONVENIO      = 'Modificar Convenio';    
    MSG_CAPTION_DOC_A_IMPRIMIR          = 'No ha seleccionada ning�n Documento, �Desea continuar?';
    MSG_CAPTION_CAMBIO_TAG_CAMBIO       = 'Cambio de Telev�a';
    MSG_CAPTION_CAMBIO_TAG_DEVOLUCION   = 'Devoluci�n Tag';
	MSG_CICLOS					 		= 'Ciclo :';
	MSG_CODIFICACION         			= 'Codificaci�n';
	MSG_CODIFICACION_TAG     			= 'Codificaci�n de TAGs';
	MSG_COMPRAS              			= 'Compras';
	MSG_CONFIGURACION_ANTENA 			= 'Configuraci�n de la antena de codificaci�n';
	MSG_CONTROL_INSATISFACTORIO			= 'Las pruebas han arrojado errores,' + #13 + 'por lo tanto la disposici�n del lote sera NO CONFORME.';
	MSG_CONTROL_SATISFACTORIO			= 'Los Controles de Calidad han sido satisfactorios.';
	MSG_CONTROL_SATISFACTORIO_TAG		= 'Los Controles de Calidad han sido satisfactorios sobre este Tag.' + #13 + 'Presione "Aceptar" para continuar.';
    MSG_CAPTION_BLANQUEAR_CLAVE         = 'La nueva clave es "%s".';
    MSG_CAPTION_EDIT_PROMOCION          = 'No se puede editar una promoci�n que se encuentra Finalizada';
    MSG_CAPTION_TURNO_ABIERTO_OTRO_USUARIO= 'Atenci�n. Ya existe un turno abierto por el usuario %s. Debe cerrarse para poder abrir el siguiente.';
    MSG_CAPTION_CONVENIO_OK             = 'El Convenio se ha guardado exitosamente';
    MSG_ERRORABRIRTURNO                 = 'Debe abrir un turno para efectuar un Pago.';
    MSG_PASES_DIARIOS                   = 'Pases diarios';
    MSG_PASES_DIARIOS_MODIF             = 'Pases diarios modificados';
    MSG_CAPTION_BATERIA_BAJA            = 'Los telev�as de los siguientes veh�culos tienen su Bater�a Baja:';
    //MSG_CAPTION_PEDIDO_PRESENTARCION    = 'Por favor, dir�jase a las oficinas de Costanera Norte para regularizar esta situaci�n.';				//SS_1147_MCA_20140408


	MSG_RECEPCION_TAGS 					= 'Recepci�n';
	MSG_IMPORTAR_ARCHIVO_PROVEEDOR      = 'Importar Archivo Proveedor';
	MSG_VER_TELEVIAS_EN_INTERFACE		= 'Ver telev�a pendientes de ingreso al Maestro';
	MSG_RECIBIR_ENVIOS_TAGS             = 'Recibir Env�os';
	MSG_CONTROL_CALIDAD                 = 'Control de Calidad';
	MSG_MOVIMIENTOS_TAGS				= 'Movimientos';
    MSG_ENTREGAR_TAGS_A_TRANSPORTES     = 'Entregar a Transportes';
    MSG_RECIBIR_TAGS_DE_TRANSPORTES     = 'Recibir de Transportes';
    MSG_CAMBIAR_UBICACION_TAGS          = 'Cambiar Ubicaciones';
    MSG_VER_SALDOS_ALMACENES            = 'Ver Saldos en Almacenes';
    MSG_CAPTION_MOVIMIENTOS_STOCK       = 'Movimientos de Stock';    

    MSG_ERROR_PUNTO_ENTREGA_VENTA       = 'El Punto de Entrega y de Venta est�n mal configurados.';
	MSG_ERROR_ACTUALIZAR 				= 'No se pudieron actualizar los datos de %s';
    MSG_ERROR_ARCHIVO_ABRIR             = 'Error al abrir el archivo: "%s"';
    MSG_ERROR_INDICAR_ARCHIVO           = 'Debe indicarse el nombre del archivo';
	MSG_ERROR_AGREGAR 					= 'No se ha podido agregar %s';
	MSG_ERROR_ANALISIS					= 'Ha ocurrido un error en el an�lisis de %s.';
	MSG_ERROR_APTITUD_LOTE_TAGS			= 'Error al actualizar la aptitud de calidad de un lote.';
	MSG_ERROR_BAJA 						= 'No se puede dar de baja este %s porque hay datos que dependen de �l.';
    MSG_ERROR_GRAVAR_CAMPOS_OBLIGATORIO = 'No se pudieron grabar los campos obligatorios.';
    MSG_ERROR_CARGA_CAMPOS_OBLIGATORIO  = 'No se pudieron grabar los campos obligatorios.';
    MSG_ERROR_DAR_BAJA_ESTE             = 'No se ha podido dar de baja este %s';
    MSG_ERROR_DOCUMENTACION             = 'No detall� esta documentaci�n';
    MSG_ERROR_MEDIO_ENVIO_DOCUMENTACION = 'No detall� el env�o de documentaci�n de cobro';
    MSG_ERROR_MEDIO_ENVIO_DOC           = 'Detall� un medio de env�o por E-Mail, pero no detall� una direccion v�lida.';
    MSG_ERROR_PERSONAL_ES_USUARIO       = 'No se puede eliminar el empleado ya que es usuario del sistema';
    MSG_ERROR_DAR_BAJA_ESTA             = 'No se ha podido dar de baja esta %s';
    MSG_ERROR_CALLE_DESNORMALIZADA      = 'Calle desnormalizada, debe seleccionar una comuna.';
	MSG_ERROR_CAMBIANDO_UBIC			= 'No se ha podido cambiar la ubicaci�n del TAG indicado.';
	MSG_ERROR_CAMBIANDO_UBIC_RANGO		= 'No se ha podido cambiar la ubicaci�n para el rango de TAGs indicados.';
	MSG_ERROR_CAMBIO_UBICACION_TAG		= 'Error al registrar Cambio de Ubicaci�n de TAG';
	MSG_ERROR_CANCELAR					= 'No se puede cancelar %s';
	MSG_ERROR_CANTIDAD					= 'Debe ingresar cantidad mayor a cero.';
	MSG_ERROR_CANTIDAD_PEDIDA			= 'La cantidad que se desea ingresar o modificar supera o iguala a la pedida.';
	MSG_ERROR_CODIFICACION_TAG			= 'No se ha podido Codificar el TAG Indicado.';
	MSG_ERROR_CONFIGURACION_ANTENA 		= 'Ha ocurrido un error al intentar comenzar a configurar la antena'+ #13 + 'de codificaci�n de Tags, no se ha podido crear la ventana de configuraci�n.' + #13 + #13 + #13 + #13 + 'IMPORTANTE: Recuerde que esta version de software requiere Windows NT 4 con Service Pack 6 ' + 'para un correcto funcionamiento del hardware de codificaci�n de Tags.';
	MSG_ERROR_CONSULTA					= 'La consulta SQL contiene errores.';
    MSG_ERROR_DUPLICADO                 = 'La %s ya se encuentra cargada.';
    MSG_ERROR_DUPLICADO_EL              = 'El %s ya se encuentra cargada.';
    MSG_ERROR_VERIFICADOR_TAG           = 'Error en el Digito de verificaci�n del Telev�a.';    
    MSG_ERROR_VEHICULO                  = 'Debe ingresar al menos un vehiculo';
	MSG_ERROR_DOCUMENTO                 = 'El n�mero de documento especificado es incorrecto.';
	MSG_ERROR_DUPLICACION				= 'No pueden actualizarse los datos. %s duplicado.';
	MSG_ERROR_EDITAR 					= 'No se ha podido modificar %s';
    MSG_ERROR_EXISTE_EL                 = 'El %s ya existe.';
    MSG_ERROR_EXISTE_LA                 = 'La %s ya existe.';
	MSG_ERROR_ELIMINAR 					= 'No se pudo eliminar %s seleccionado.';
	MSG_ERROR_ELIMINAR_DEPENDENCIA 		= 'No se puede eliminar %s porque hay dependencia de datos.';
	MSG_ERROR_FECHA						= 'La fecha indicada para %s es incorrecta.';
    MSG_ERROR_FECHA_VENCIMIENTO         = 'La fecha de vencimiento es erronea.';
	MSG_ERROR_GUARDAR_REGISTRO_PRUEBAS	= 'Error al intentar grabar el registro de pruebas.';
	MSG_ERROR_HABILITACION_TAG			= 'No se ha podido Habilitar el TAG Indicado.';
	MSG_ERROR_INGRESAR_TAG				= 'Error al ingresar un Tag';
	MSG_ERROR_INHABILITACION_TAG		= 'No se ha podido Inhabilitar el TAG Indicado.';
	MSG_ERROR_INICIALIZACION			= 'Error al inicializar %s';
	MSG_ERROR_LECTURA					= 'Error al Leer %s.';
	MSG_ERROR_LIMITE_ACEPTACION			= 'La cantidad de errores supero el limite de aceptaci�n.';
	MSG_ERROR_MAYOR_CANTIDAD			= 'El rango indicado excede a la cantidad permitida para ingresar';
    MSG_ERROR_MEDIO_PAGO_DUPLICADO      = 'Este Medio de Pago ya esta Cargado para esta Persona';
    MSG_ERROR_NUMERO_CALLE              = 'El n�mero no pertenece a la calle.';
    MSG_ERROR_NO_TIENE_MAIL             = 'No puede tildar esta opcion hasta no ingresar una direcci�n de E-Mail.';
    MSG_ERROR_FALTA_NUMERO_TARJETA      = 'Debe ingresar el Nro. de tarjeta.';
    MSG_ERROR_NUMERO_TARJETA            = 'Nro de Tarjeta inv�lido - %s';
    MSG_ERROR_NUMERO_CUENTA             = 'Nro de Cuenta Invalido';
    MSG_NUMERO_DESNORMALIZADO           = 'N�mero no pertenece a la calle seleccionada o no se encuentra el CP, tendra que especificar un CP.';
	MSG_ERROR_OBTENER_DATOS				= 'No se pudieron obtener los datos de %s.';
	MSG_ERROR_ORDEN_COMPRA_ABIERTA		= 'Error! No existe ninguna Orden de Compra abierta.';
    MSG_ERROR_OPENTABLE                 = 'Error al obtener registro de %s.';
	MSG_ERROR_RANGO_TAGS				= 'El campo N�mero de TAG "Hasta" es menor que el campo n�mero de TAG "Desde"';
	MSG_ERROR_RANGO						= 'Valor fuera del rango %d - %d';
	MSG_ERROR_RESOLUCION_ORDEN_SERVICIO	= 'El tipo de Orden de Servicio no puede ser resuelto mediante esta pantalla.';
	MSG_ERROR_TAG_CONCESIONARIA 		= 'El TAG no es de esta Concecionaria.';
    MSG_ERROR_AREA_TELEFONO             = 'El telefono no pertenece al codigo de area.';
    MSG_ERROR_UBICACION					= 'Ha ocurrido un error en el an�lisis de la ubicaci�n de un Tag.';
    MSG_ERROR_VEHICULO_EXISTENTE_SOLIC  = 'La patente ingresada ya se encuentra cargada en la solicitud.';
    MSG_PATENTE_CARGADA_EN_OTRA_SOLIC   = 'La patente %s ya fue cargada en otra solicitud.';
    MSG_EXCEL_REGISTRO_OPERACIONES      = 'Error al grabar en el registro de operaciones.';
    MSG_ERROR_FALTA_RUT_ARCHIVO         = 'No se encontr� el RUT/RUN en el archivo.';
    MSG_ERROR_FALTA_SOLICITUD           = 'No hay ninguna solicitud para el RUT/RUN especificado en el archivo.';
    MSG_ERROR_PLANILLA_EXCEL            = 'La planilla no tiene el formato correcto.';
    MSG_ERROR_AGREGAR_VEHICULO          = 'No se pudo agregar los datos de un veh�culo';
    MSG_ERROR_GENERACION_ARCHIVO_XLS    = 'No se pudo crear el archivo.';
    MSG_ERROR_SIN_PERMISO               = 'No tiene permiso para ejecutar esta acci�n.';
    MSG_ERROR_ARCHIVO_DEMASIADO_GRANDE  = 'El Archivo %s es demasiado grande.';
    MSG_ERROR_ARCHIVO_SIN_EXTENSION_XLS = 'El Archivo %s no tiene extensi�n Excel.';
    MSG_ERROR_FORMATO_INCORRECTO        = 'El Archivo %s no tiene el formato correcto.';
    MSG_ERROR_EXTENSION_INCORRECTA      = 'El Archivo %s no tiene una extensi�n correcta.';
    MSG_ERROR_GENERACION                = 'Error al generar.';
    MSG_EXCEL_ERROR                     = 'Error al conectarse con Excel, puede que no est� instalado o no funcione.';
    MSG_ERROR_DIRECTORIO                = 'No se pudo acceder al directorio donde se encuentran los archivos de Excel.';
    MSG_ERROR_DIRECTORIO_ACCESO         = 'Puede que no tenga permisos para acceder al directorio o que este no exista.';
	MSG_ERROR_TAG_VACIO					= 'Debe ingresar un n�mero de telev�a';
    MSG_ERROR_NUMERO_TAG                = 'El N�mero de telev�a no es correcto.';
    MSG_ERROR_ALTA_RUT_CONVENIO         = 'El N�mero de RUT Ingresado tiene un convenio cargado.';
    MSG_ERROR_NO_ESTA_TAG               = 'El N�mero de telev�a no se encuentre en el Maestro.';
    MSG_ERROR_UBICACION_TAG             = 'El N�mero de telev�a est� asignado a otro punto de entrega.';
    MSG_ERROR_TAN_EN_CLIENTE            = 'El Telev�a se encuentra en poder del cliente.';
    MSG_ERROR_SITUACION_TAG             = 'El telev�a no est� disponible para su entrega.';
	MSG_ERROR_TAG_VENDIDO_A_OTRO		= 'El telev�a fue vendido a otra persona.';
    MSG_ERROR_CATEGORIA_TAG             = 'El N�mero de telev�a no es de la misma categor�a que la del veh�culo.';
    MSG_ERROR_STOCK_TAG                 = 'No hay stock para asignar el telev�a.';
    MSG_ERROR_STOCK_TAG_ASIGNADOS       = 'Se asignaron m�s telev�a que los que hay en stock';
    MSG_ERROR_STOCK_TAG_TURNO           = 'No hay suficientes telev�a en stock.';
    MSG_ERROR_SUSPENSION                = 'La fecha limite de Suspensi�n, no puede ser menor a la fecha Actual';
    MSG_ERROR_STOCK_TAG_TURNO_ASIGNADOS = 'No hay suficientes telev�as, estos ya fueron asignados a otros turnos.';
    MSG_ERROR_DOCUMENTOS_IGUALES        = 'Los n�meros de documento no pueden ser iguales.';
    MSG_ERROR_ES_JURIDICA               = 'Este n�mero de documento ya est� registrado como Empresa.';
    MSG_ERROR_REPRESENTANTE_ORDEN       = 'Por favor agregue los Representantes en orden.';
    MSG_ERROR_NUMERO_DOCUMENTO          = 'Error en el N�mero de Documento';
    MSG_ERROR_MISMA_PERSONA_CONVENIO    = 'Ha ingresado el mismo RUT que en el convenio.';
    MSG_ERROR_ACCESO		            = 'No se ha podido acceder a la Base de Datos.';
    MSG_ERROR_CONTRASENA                = 'La contrase�a no es correcta.';
	MSG_ERROR_IMPRIMIR_CONVENIO         = 'Error al imprimir el convenio';
	MSG_ERROR_IMPRIMIR_ETIQUETA         = 'Error al imprimir el etiqueta';
    MSG_ERROR_TIPO_MEDIO_PAGO_AUTOMATICO= 'Tipo de medio de pago autom�tico desconocido.';
    MSG_ERROR_APERTURA                  = 'No se pudieron obtener los datos de apertura/cierre.';
    MSG_ERROR_TURNO_ABIERTO             = 'El Turno anterior no ha sido cerrado. Debe cerrarse para poder abrir el siguiente.';
    MSG_ERROR_TURNO_ABIERTO_MISMO_USUARIO = 'Ya existe un turno abierto por el usuario %s. Debe cerrarse para poder abrir el siguiente.';
    MSG_ERROR_TURNO_ABIERTO_OTRO_USUARIO= 'El Turno anterior ha sido abierto por el usuario %s. Debe cerrarse para poder abrir el siguiente.';
    MSG_ERROR_TURNO_CERRADO             = 'No hay un turno abierto.';
    MSG_ERROR_TURNO_CERRADO_USUARIO     = 'El Turno debe ser cerrado por el usuario que lo abri�.';
    MSG_ERROR_ADICIONAR_TELEVIA_USUARIO = 'Solo pueden adicionar telev�as al usuario que abri� el turno.';
    MSG_ERROR_ADICIONAR_DINERO_USUARIO 	= 'Solo pueden adicionar dinero el usuario que abri� el turno.';
    MSG_ERROR_TURNO_ABRIR               = 'Para poder cargar un Convenio debe abrir un turno.';
    MSG_ERROR_TURNO_MODIF_CONVENIO      = 'Para poder Agregar o Eliminar una Cuenta del Convenio debe abrir un turno.';
    MSG_ERROR_TURNO_BAJA_CONVENIO       = 'Para dar de Baja un Convenio debe abrir un turno.';
    MSG_ERROR_TURNO_SUSPENDER_VEHICULO  = 'Para poder %s un Vehiculo debe abrir un turno.';
    MSG_ERROR_TURNO_GUARDAR             = 'No se pudieron guardar los datos del turno.';
    MSG_ERROR_RECIBIR_TELEVIAS_GUARDAR  = 'No se pudieron guardar los datos de la recepci�n.';
    MSG_ERROR_MOVIMIENTO_TELEVIAS       = 'No se pudieron guardar los datos del movimiento.';
    MSG_ERROR_NUMERO_TAG_COVENIO        = 'Este telev�a ya lo asign� a otro veh�culo en este convenio.';
    MSG_ERROR_VEHICULO_OTRA_CONCESIONARIA = 'Este Veh�culo pertenece a otra concesionaria.';
    MSG_ERROR_TRANSPORTISTA_SELECCIONAR = 'Debe seleccionar un transportista.';
    MSG_ERROR_RECEPCION_TAGS            = 'La gu�a de despacho pertenece a otro Punto de Entrega.';
    MSG_ERROR_VERSION                   = 'La aplicaci�n que usted est� ejecutando no es una versi�n v�lida.';
    MSG_ERROR_VERSION_APLICACION        = 'Usted esta ejecutando una versi�n que ya no tiene vigencia.';
    MSG_ERROR_PUNTO_VENTA               = 'No se encuentra la identificaci�n del Punto de Venta.';
    MSG_ERROR_PUNTO_ENTREGA             = 'No se encuentra la identificaci�n del Punto de Entrega.';
    MSG_ERROR_CANTIDAD_TAGS_RECIBIDOS   = 'La cantidad de telev�a recibidos debe ser menor o igual a los enviados. Por favor guarde una observaci�n de las diferencias.';
    MSG_ERROR_CANTIDAD_TAGS_VALORES     = 'Los datos ingresados no son correctos';
    MSG_ERROR_CARGA_SOLICITUD           = 'Error al cargar el convenio.';
    MSG_ERROR_RESULTADO                 = 'Debe seleccionar un resultado.';
    MSG_ERROR_ACTUALIZAR_RESULTADO      = 'No se pudo actualizar el resultado.';
    MSG_ERROR_LECTURA_DATOS_TAG         = 'No pudo leerse los datos del Telev�a';
    MSG_ERROR_GRABACION_DATOS_TAG       = 'No pudo grabarse lo datos del Telev�a';
    MSG_ERROR_GUARDAR_REP_LEGAL         = 'Error al guardar el Rep. Legal';
    MSG_ERROR_GENERAR_FACTURA           = 'Error al generar la Facturaci�n.';
    MSG_ERROR_EJECUTAR_SP               = 'Error al ejecutar el SP';

    MSG_ERROR_CATEGORIA_FUERA_DE_LISTA 	= 'La categor�a especificada es incorrecta';
	MSG_ERROR_CANTIDAD_ES_CERO 			= 'La cantidad a ingresar debe ser superior a 0 (cero)';
	MSG_ERROR_TOTAL_INGRESADO		 	= 'La suma de los telev�a ingresados para la categor�a %s no se corrsponde con la cantidad indicada o no hay saldo suficiente en stock';
	MSG_ERROR_TOTAL_INGRESADO_NOMINADO  = 'Se deben detallar todos los telev�a ya que el almac�n es nominado';
	MSG_ERROR_CATEGORIA 				= 'La Categor�a es incorrecta';
	MSG_ERROR_CATEGORIA_PARA_TAG		= 'La categor�a especificada no se corresponde con la categor�a del %s';
	MSG_ERROR_NO_EXISTE_TAG				= 'El %s no existe en el Sistema';
	MSG_ERROR_DATOS_NO_EDITABLES 		= 'Los datos no son editables ya que su contenido fue procesado';
	MSG_ERROR_EMBALAJE_INCORRECTO	 	= 'El tipo de Embalaje es incorrecto';
	MSG_ERROR_ETIQUETA_TAG 				= '%s (%s): El n�mero del telev�a es incorrecto';
	MSG_ERROR_CANTIDAD_DISPONIBLE 		= 'La cantidad indicada supera a la disponible para recibir: ';
	MSG_ERROR_NUMERO_TAG_INCORRECTO 	= 'No se ha especificado un n�mero v�lido de %s';
	MSG_ERROR_RANGO_TAGS_MAYOR_MENOR 	= 'El n�mero de Telev�a Inicial debe ser menor o igual al n�mero de Telev�a Final';
	MSG_ERROR_RANGO_TAGS_NO_VALIDO 		= 'Debe indicarse un rango de telev�a correcto (uno de ellos es incorrecto)';
    MSG_ERROR_NUMERO_TELEVIA_BORRAR     = 'El nro de Telev�a Ingresado no coincide con el asociado al Veh�culo.';
	MSG_ERROR_TAG_YA_INGRESADO 			= ': El telev�a ya fue ingresado previamente';
  	MSG_ERROR_TAG_NO_EXISTE_EN_LISTA 	= ': El telev�a no existe en la lista de telev�a v�lidos (no se encuentra en Interface o Maestro)';
	MSG_ERROR_RANGO_SUPERPUESTO			= 'Existe repetici�n de telev�a en alguno de los rangos de telev�a ingresados (hay rangos superpuestos total o parcialmente)';
    MSG_ERROR_NO_EDITABLE 				= 'El embalaje no puede ser eliminado ya que su contenido fue procesado';
	MSG_ERROR_ID_EMBALAJE_NULO			= 'El ID del Embalaje no puede ser 0';
	MSG_ERROR_YA_EXISTE_EMBALAJE		= 'El ID del Embalaje ya fue ingresado previamente';
	MSG_ERROR_CANTIDAD_TAGS				= 'La cantidad de telev�a disponibles dentro del rango indicado es distinta a la cantidad especificada';
	MSG_ERROR_CANTIDAD_TAGS_CATEGORIA   = 'La cantidad de telev�a disponibles (correspondientes a la categor�a ingresada) dentro del rango indicado es distinta a la cantidad especificada';
	MSG_ERROR_CANTIDAD_TAGS_ALMACEN     = 'La cantidad de telev�a disponibles dentro del rango indicado no esta disponible, en su totalidad, en el almac�n origen indicado';
	MSG_ERROR_CANTIDAD_TAGS_CATEGORIA_ALMACEN='La cantidad de telev�a disponibles (correspondientes a la categor�a ingresada) dentro del rango indicado no esta disponible, en su totalidad, en el almac�n origen especificado';
    MSG_ERROR_CANTIDAD_TAGS_PARAMETROS  = 'La Cantidad de Telev�as disponibles entre el rango especificado, desde el Telev�a %s, hasta el Telev�a %s, para el almac�n %s - %s y Categor�a %d, no coincide con la cantidad especificada.' + #13#10 + #13#10 +
                                          'En el sistema constan %d Telev�as disponibles para los Par�metros indicados.';

	MSG_ERROR_SALDOS_ALMACENES 			= 'No se pueden obtener los saldos de Telev�a en los almacenes';
	MSG_ERROR_TAGS_INTERFACE 			= 'No se pueden obtener datos de los Telev�a pendientes de incorporar al maestro de Telev�a';
	MSG_ERROR_DATOS_LISTADO 			= 'No se pueden obtener datos para generar el listado';
	MSG_ERROR_DATOS_EMBALAJE 			= 'No se pueden obtener datos para embalaje de tipo %d';
	MSG_ERROR_CONSISTENCIA_LINEA		= 'No puede evaluarse consistencia de datos en la l�nea';
    MSG_ERROR_VALIDACION_EXISTENCIA_TAG	= 'No puede validarse la existencia de Telev�a';
    MSG_ERROR_CONTROL_CALIDAD			= 'No puede registrarse el resultado del control de calidad de Telev�a';
    MSG_ERROR_GRABAR_RECEPCION 			= 'Grabaci�n de Recepci�n de Env�os de Telev�a';
    MSG_ERROR_REGISTRO_RECEPCION 		= 'Registro de Movimiento de Recepci�n de Telev�a';
    MSG_ERROR_GUIA_DESPACHO 			= 'Debe indicar una gu�a de despacho';
    MSG_ERROR_ORDEN_COMPRA 				= 'Debe indicar una orden de compra';
    MSG_ERROR_PROVEEDOR 				= 'Debe indicar un proveedor';
    MSG_ERROR_ID_PALLET 				= 'Debe indicar un ID de Pallet correcto.';
	MSG_ERROR_GENERAR_MOVIMIENTO		= 'No pudieron registrarse los movimientos de telev�a';
    MSG_ERROR_BAJA_CONVENIO             = 'Error al dar de baja el Convenio.';
    MSG_ERROR_BLANQUEAR_CLAVE           = 'Error al Blanquear Clave.';
    MSG_ERROR_CLIENTE_SUSPENDIDO        = 'El Cliente est� suspendido.';
    MSG_ERROR_TELEFONO_MANDANTE         = 'No se puede eliminar el Tel�fono ya que est� vinculado a un convenio de esta persona.';

    MSG_ERROR_OBTENER_OP_PENDIENTES		= 'No puede obtenerse la lista de Ordenes de Pedido de Telev�a pendientes';
	MSG_ERROR_CANCELAR_ORDEN_PEDIDOS	= 'No puede cancelarse la Orden de Pedido de Telev�a';

    MSG_ERROR_NO_HAY_FAQ                = 'No se encontraron preguntas frecuentes.';
    MSG_CAPTION_SELECCIONAR_POR         = 'Debe seleccionar un POS';
    MSG_CONV_EXISTENTE                  = 'Esta Persona ya posee un convenio, Solo podra modificar los datos del convenio existente.';    
    MSG_ERROR_ELIMINAR_SOLICITUD        = 'No se pudo eliminar la solicitud.';
    MSG_ERROR_PATENTE                   = 'Error Patente';
    MSG_ERROR_DATOS_VEHICULO            = 'Error en Datos del Veh�culo';
    MSG_ERROR_TAG_LST_NEGRA             = 'No se puede dar de alta un Telev�a que se encuentra en lista Negra';
    MSG_PATENTE_REPETIDA                = 'Patente repetida';
    MSG_ERROR_VALIDAR_FORMATO_PATENTE   = 'La patente especificada es incorrecta.';
    MSG_ERROR_PATENTE_CUENTA            = 'La patente especificada ya existe en otra cuenta.';
    MSG_ERROR_PATENTE_CUENTA_CONVENIO   = 'La patente especificada ya existe en otra cuenta'+FIN_LINEA+'Convenio: %s';
    MSG_ERROR_PATENTE_CUENTA_ASOCIADA_DIGITO_Z = 'La patente indicada esta asociada como RNVM al'+FIN_LINEA+'Convenio: %s'+FIN_LINEA+'Patente: %s';
    MSG_ERROR_DIGITO_VERIFICADOR        = 'El d�gito verificador especificado es incorrecto para esta patente.';
    MSG_ERROR_DIGITO_VERIFICADOR_PATENTE= 'Usted ha ingresado un d�gito verificador incorrecto.';
	MSG_ESPERA_INICIO_LECTORES			= 'Inicializando Lectores. Aguarde por favor ...';
	MSG_FALTA_CLIENTE					= 'Debe seleccionar un Cliente.';
	MSG_FALTA_CUENTA					= 'Debe ingresar una Cuenta.';
	MSG_FIN_CODIFICACION_TAGS			= 'No hay m�s TAGs para codificar.';
	MSG_HABILITACION_TAG     			= 'Habilitaci�n de TAGs';
	MSG_INFORMACION_NO_ENCONTRADA		= 'No se pudo obtener la informaci�n solicitada.';
    MSG_QUESTION_CERRAR_TURNO           = 'Con estos datos se cerrar� el turno. �Desea guardarlos?';
    MSG_INFORMACION_SIN_ANTENA			= 'Este modo no utiliza la antena de codificaci�n de TAGs, ingrese el n�mero de TAG en el campo correspondiente y presione el bot�n "Codificar" o bien ESC para cancelar.';
	MSG_INFORMES             			= 'Informes';
	MSG_INGRESAR_CONTEXT_MARK 			= 'Debe elegir el Context Mark para continuar.';
	MSG_INGRESAR_NUMERO_TAG   			= 'Debe ingresar un N�mero de TAG para continuar.';
	MSG_INGRESO_ALMACENES    			= 'Ingreso a Almacenes';
	MSG_INHABILITACION_TAG   			= 'Inhabilitaci�n de TAGs';
	MSG_LECTURA_CORRECTA				= 'Lectura Correcta';
	MSG_LECTURA_INCORRECTA				= 'Error de Lectura';
	MSG_LIMITE_REGISTROS				= '*** SE HA ALCANZADO EL LIMITE DE REGISTROS ***';
	MSG_LOTE							= 'LOTE: %d Cantidad Total de Tags: %d';
	MSG_LOTE_INGRESADO_OK				= 'El ingreso fue realizado con �xito.';
	MSG_MANTENIMIENTO 					= 'Mantenimiento de %s';
	MSG_NO_EXISTE_TAG 					= 'El TAG %d-%s no se encuentra en el Maestro de TAGs.';
	MSG_NO_EXISTEN_TAGS					= 'No hay ning�n TAG para codificar.';
	MSG_NRO_TAG_INVALIDO				= 'Debe ingresar un numero de TAG v�lido';
	MSG_NUEVA_UBICACION					= 'Ubicaci�n de TAG: %s';
	MSG_ORDENES_COMPRA       			= 'Gesti�n de �rdenes de Compra';
	MSG_PRUEBAS_SIN_ANTENA				= '*** Pruebas Realizadas sin Antena ***';


    MSG_QUESTION_BAJA_ESTA_SEGURO		= '�Est� seguro de eliminar el registro?';
	MSG_QUESTION_BAJA 					= '�Est� seguro de querer dar de baja este %s?';
    MSG_QUESTION_BAJA_ESTA 				= '�Est� seguro de querer dar de baja esta %s?';
	MSG_QUESTION_BUSCAR_PERSONA 		= '�Desea cargar los datos de la persona?' + #13 + 'Al hacerlo se perder�n los datos cargados hasta el momento';
    MSG_QUESTION_BAJA_VEHIC_SOLO        = '�Est� seguro de querer DEJAR el Telev�a y dar de baja el Veh�culo?'; 
    MSG_QUESTION_BAJA_CONVENIO_SIN_VEHICULO = 'El convenio NO tiene Vehiculos, �lo quiere dar de baja?.';
    MSG_QUESTION_BAJA_ULT_CUENTA        = 'Est� por dar de baja la ultima cuenta, �Desea dar baja el Convenio';         
	MSG_QUESTION_CANCELAR				= '�Desea cancelar %s?';
    MSG_QUESTION_DOMICILIO_RELACIONADO  = 'Ya existe un domicilio relacionado. �Desea reemplazarlo por este?';
	MSG_QUESTION_ELIMINAR 				= '%s: �Est� seguro de querer eliminar los datos?';
	MSG_QUESTION_ELIMINAR_CAPTION 		= 'Confirmaci�n...';
	MSG_QUESTION_ELIMINAR_DATO          = '�Desea quedarse sin %s?';
    MSG_QUESTION_GUARDAR_CONVENIO       = '�Est� seguro que los datos ingresados en el Convenio son los correctos?';
    MSG_QUESTION_PERMISOS_PADRE         = '�Quiere que las funciones descendientes hereden este permiso?';
	MSG_QUESTION_GUARDAR_REGISTRO_PRUEBAS= '�Desea guardar el registro existente antes de continuar?';
	MSG_QUESTION_GUARDAR_RESULTADO		= '�Desea registrar la conformidad del resultado de las pruebas sobre este lote?';
    MSG_QUESTION_GUARDAR_SOLICTUD       = '�Desea guardar los datos como una solicitud?';
	MSG_QUESTION_INGRESO_TAGS			= '�Desea continuar ingresando mas TAGs?';
	MSG_QUESTION_MODIFICAR_DATO         = 'Existe %s como %s. �Desea cambiarlo?';
    MSG_QUESTION_MULTIPLES_SOLICITUDES  = 'Existe m�s de una solicitud para el RUT/RUN especificado en el archivo. �Desea Continuar?';
    MSG_QUESTION_EXISTENCIA_VEHICULOS   = 'Ya existen veh�culos cargados en esta solicitud. �Desea Continuar?';
    MSG_QUESTION_DESEA_CONTINUAR        = '�Desea Continuar?';
    MSG_QUESTION_RECHAZAR_ENTREGA       = '�Est� seguro que desea rechazar la Entrega?';
    MSG_QUESTION_BORRAR_TELEFONES_MANDANTE = 'El mandante tiene m�s de un telefono, �Quiere borrar todos los telefonos?';
    MSG_QUETION_BLANQUEAR_CLAVE         = '�Est� seguro que desea Blanquear la clave de esta persona?';
    MSG_QUESTION_IMPRIMIR_CONVENIO      = '�Quiere Imprimir los Documentos Asociados a esta Modificaci�n?';
    MSG_QUESTION_TAG_ROBADO             = '�Quiere dar de baja esta cuenta por Telev�a Robado?';
    MSG_QUESTION_ALTA_TAG               = 'El Cliente tiene una o m�s Cuentas Suspendidas, �Asignar Telev�a de todos modos?';

	MSG_RANGO_EN_PODER_CLIENTE			= 'Hay TAGs que se encuentran en poder del cliente para el rango definido.'+ #13#10 + '�Desea cambiar?';
	MSG_REGISTRO_PRUEBAS				= '*** Registro de Pruebas de Calidad ***';
	MSG_REINICIAR						= 'Los cambios tendr�n efecto cuando reinicie el programa.';
	MSG_REPORTE_SEGUIMIENTO  			= 'Reporte de Seguimiento de Tags';
	MSG_REPORTE_STOCK        			= 'Reporte de Stock por ubicaci�n';
	MSG_RESUMEN					 		= 'Realizado: %s   Usuario: %s';
	MSG_RETIRAR_TAG						= 'Por favor, retire el TAG de la antena.';
	MSG_SELECCION_MOTIVO_BAJA			= 'Seleccione el motivo de baja.';
	MSG_SELECCIONAR 					= '(Seleccionar %)';
	MSG_SOLICITUD_PONER_TAG				= 'Por Favor, Coloque el TAG Dentro del Dispositivo de Codificaci�n Correspondiente. Si Desea Cancelar la Operaci�n, Presione el Bot�n "Cancelar".';
	MSG_SOLICITUD_TRABAJO 				= 'Nueva Solicitud de Trabajo';
	MSG_STOCK 							= 'Stock';
	MSG_TAG_ASIGNADO  					= 'El TAG %d-%s se encuentra actualmente asignado.';
	MSG_TAG_HABILITADO					= 'El TAG Indicado fue Exitosamente Habilitado.';
	MSG_TAG_INHABILITADO				= 'El TAG Indicado fue Exitosamente Inhabilitado.';
	MSG_TAG_NO_EXISTE					= '< Nro. de TAG incorrecto >';
	MSG_TAG_NO_INGRESADOS				= 'Debe ingresar el TAG "Desde" y el TAG "Hasta"';
	MSG_TAG_YA_HABILITADO				= 'El n�mero de TAG indicado ya esta habilitado.';
	MSG_TAREAS_MANTENIMIENTO 			= 'Tareas de Mantenimiento';
	MSG_UBIC_NO_PUEDE_CAMBIAR			= 'Ubicaci�n de TAG: %s. Esta ubicaci�n no puede ser cambiada';
	MSG_UBIC_NO_PUEDE_CAMBIAR_A_CLIENTE	= 'No se puede cambiar un rango de TAGs para pasarlos a poder del cliente' + #13#10 + 'Esto debe ser indicado por cada TAG.';
	MSG_UBICACION_CAMBIADA_OK			= 'La Ubicaci�n fue cambiada con exito.';
	MSG_USER 							= 'Usuario: %s';
    MSG_CONECTADO_A						= 'Conectado a: %s - %s';
    MSG_NO_PUEDE_VALIDAR_PATENTE        = 'Atenci�n. No se podr� validar la patente.';
    MSG_PUNTO_ENTREGA_VENTA             = 'El Punto de Entrega o Venta no son v�lidos.';
    MSG_BAJA_VEHICULO                   = '�Desea dar de baja el veh�culo?';

    MSG_VALIDAR_CODIGO_AREA             = 'Debe ingresar el c�digo de �rea';
    MSG_NUMERO_TELEFONO                 = 'El n�mero de telefono no es v�lido';
    MSG_RANGO_HORARIO                   = 'La hora final debe ser mayor a la inicial';
    MSG_VALIDAR_CARGA_CALLE             = 'Debe ingresar primero una calle';
	MSG_VALIDAR_CANTIDAD_DATOS			= 'Debe ingresar al menos un %s';
	MSG_VALIDAR_CHEQUEAR_DIAS 			= 'Debe ingresar al menos un d�a en el que se ejecutar� la %s';
    MSG_VALIDAR_DATO                    = 'Debe ingresar una %s valida.';
	MSG_VALIDAR_DETALLE 				= 'Debe ingresar %s para detallar %s';
    MSG_VALIDAR_DEBE_EL                 = 'Debe ingresar el %s';
    MSG_VALIDAR_DEBE_LA                 = 'Debe ingresar la %s';
    MSG_VALIDAR_DOMICILIO_ENTREGA		= 'Debe ingresar el domicilio de entrega.';
    MSG_VALIDAR_DOMICILIO_DESNORAMLIZADO = 'Usted esta guardando una calle del %s desnormalizada. �Desea continuar?';
    MSG_VALIDAR_DOMICILIO_NRO_DESNORAMLIZADO = 'Usted esta guardando un n�mero desconocido para la calle del %s. �Desea continuar?';
    MSG_VALIDAR_DIRECCION_EMAIL         = 'La direcci�n de email no es v�lida';                                                       //SS-1092-MVI-20130627
	MSG_VALIDAR_REPRESENTANTES_LEGALES  = 'Debe verificar la informaci�n de los Representantes Legales';
    //INICIO	: 20161013 CFU TASK_054_CFU_20161013-CRM_Validar_RUT_Representante_Legal
	MSG_VALIDAR_RUT_REP_LEGALES  		= 'El RUT de los Representantes Legales no puede ser mayor a 49.999.999';
    //TERMINO	: 20161013 CFU TASK_054_CFU_20161013-CRM_Validar_RUT_Representante_Legal
	MSG_VALIDAR_SINTAXIS_EMAIL          = 'La sintaxis del Mail indicada es incorrecta';
    MSG_DEBE_DIRECCION_EMAIL            = 'Debe indicar la direcci�n de Mail.';
    MSG_DEBE_PUNTO_ENTREGA              = 'Debe seleccionar un Punto de Entrega a donde concurrir�.';
    MSG_VALIDAR_DOMICILIOS_DUPLICADOS   = 'El domicilio se encuentra ya cargado.';
    MSG_VALIDAR_FECHA                   = 'La Fecha debe ser mayor a 1900 y menor a %d';
	MSG_VALIDAR_FECHA_DESDE 			= 'Debe ingresar la %s de inicio de ejecuci�n de la %s';
	MSG_VALIDAR_FRECUENCIA 				= 'Debe ingresar la frecuencia en %s para ejecutar la %s';
    MSG_VALIDAR_MAX_DOMICILIOS_RELACIONADOS = 'Se puede especificar como m�ximo 2 domicilios relacionados.';
	MSG_VALIDAR_HORA_EJECUCION 			= 'Debe ingresar la hora de ejecuci�n de la %s';
    MSG_VALIDAR_HORA_DESDE_HASTA        = 'La hora Desde debe ser menor que la hora Hasta.';
    MSG_VALIDAR_HORA_RANGO              = 'El intervalo horario especificado es incorrecto.';
    MSG_VALIDAR_RANGO_NUMERICO          = 'El rango telefonico esta mal cargado.';
    MSG_VALIDAR_ORDEN_FECHA             = 'La fecha desde debe ser menor que la fecha hasta.';
    MSG_VALIDAR_FECHA_MAYOR_HOY         = 'La fecha desde debe ser mayor a la fecha de hoy.';
	MSG_VALIDAR_SELECCION				= 'Debe seleccionar %s para detallar %s';
	MSG_VALIDAR_SITUACION_IVA 			= 'Debe indicar la Situaci�n con Impositiva del %s';
    MSG_VALIDAR_TIPO_TELEFONO           = 'Debe ingresar el tipo de tel�fono';
    MSG_VALIDAR_FECHA_MIN_NACIMIENTO    = 'La fecha de nacimiento debe ser mayor del 1900';
    MSG_VALIDAR_FECHA_NACIMIENTO        = 'La edad m�nima para registrar una solicitud debe ser mayor o igual a %d';
	MSG_VALIDAR_SELECCIONAR		        = 'Debe seleccionar %s ';
    MSG_VALIDAR_SELECCIONAR_UNO         = 'Solo debe seleccionar un %s';
    MSG_VALIDAR_CARGA_TAG               = 'Debe ingresar un N�mero de telev�a';
    MSG_VALIDAR_GUIA_DESPACHO           = 'Debe seleccionar una gu�a de despacho.';
    MSG_VALIDAR_BODEGA                  = 'Debe ingresar una bodega.';
    MSG_VALIDAR_TOP_LISTA_CONVENIO      = 'El n�mero M�ximo permitido es de 9999.';
    MSG_VALIDAR_TOP_LISTA_CONVENIO_MIN  = 'No se permiten n�meros negativos.';
    MSG_VALIDAR_PATENTE_Y_PATENTERVM_IGUALES = 'La patente y la patente RVM debe ser distintas';
    MSG_VALIDAR_CODIGO_POSTAL_DIGITOS   = 'Verifique que en este campo los datos sean solamente num�ricos.';

	STR_ADVERTNCIA                      = 'Advertencia';
	STR_ALMACEN 						= 'Almac�n';
    STR_ALIAS                           = 'Alias';
	STR_ARBOL							= 'Arbol FAQs';
	STR_BODEGA_CENTRAL                  = 'Bodega Central';
	STR_CAJA							= 'Caja';
	STR_CATEGORIAS_EMPLEADO 			= 'Categor�as de Empleados';
	STR_CENTRO_COSTOS 					= 'Centro de Costos';
	STR_COLORES_MATERIALES 				= 'Colores de los Materiales';
    STR_CONTRATO                        = 'Contrato';
    STR_CONVENIO                        = 'Convenio';
	STR_CONDICIONES_RETIRO 				= 'Condiciones de Retiro de Materiales';
	STR_CONFIRMACION					= 'Confirmaci�n...';
	STR_CONTACTO   						= 'Contacto';
    //STR_COSTANERA_NORTE                 = 'Costanera Norte';					//SS_1147_MCA_20140408
	STR_DOMICILIO_PARTICULAR    		= 'Domicilio particular';
    STR_DOMICILIO_ALTERNATIVO           = 'Domicilio alternativo';
	STR_DATOS_COLUMNA					= 'Datos Columna Listado';
	STR_DATOS_DOMICILIO 				= 'Datos del Domicilio';
	STR_PALLET							= 'Pallet';
	STR_DATOS_EMPLEADO 					= 'Datos del Empleado';
	STR_DATOS_PROVEEDOR 				= 'Datos del Proveedor';
	STR_DISPOSITIVO_CODIFICACION 		= 'Dispositivo de Codificaci�n';
	STR_EMPLEADO 						= 'Empleado';
	STR_EQUIPOS 						= 'Equipos';
	STR_ESTACIONES_MANTENIMIENTO		= 'Estaciones de Mantenimiento';
    STR_INGRESE_TAG_ELIMINAR            = 'Ingrese el nro de Telev�a del Veh�culo %s';
    STR_FORMA_PAGO                      = 'Medio de Pago';
	STR_LOTE_TAGS						= 'Lote de TAGs';
	STR_LUGARES_VENTA 					= 'Lugares de Venta';
	STR_MAESTRO_MATERIALES 				= 'Maestro de Materiales';
	STR_MAESTRO_PERSONAL 				= 'Maestro de Personal';
	STR_MAESTRO_ALMACENES				= 'Maestro de Almacenes';
	STR_MAESTRO_TRANSPORTES				= 'Maestro de Transportes';
	STR_MANTENIMIENTO 					= 'Mantenimiento';
	STR_MARCAS 							= 'Marcas';
	STR_MODELOS_MATERIALES 				= 'Modelos de Materiales';
    STR_MEDIOS_PAGOS                    = 'Medios de Pagos';
    STR_NO_CORRESPONDE                  = 'No corresponde';
	STR_OPERADOR_LOGISTICO   			= 'Operador Log�stico';
	STR_PATENTE                         = 'Patente';
    STR_PLANTILLA_AUTORIZACION_VOLUNTARIA = 'Plantilla de Autorizaci�n Voluntaria';
    STR_PLANTILLA_CONVENIO_PERSONA_NATURAL = 'Plantilla de Convenio de Persona Natural';
    STR_PLANTILLA_CONVENIO_PERSONA_JURIDICA = 'Plantilla de Convenio de Persona Juridica';
    STR_PLANTILLA_MANDATO               = 'Plantilla de Mandato pago automatico de cuentas';
    STR_PLANTILLA_INDIVIDUALIZACION_VEHICULO = 'Plantilla de Individualizaci�n de Veh�culos';
    STR_PLANTILLA_RECEPCION_TAG         = 'Documento de Recepci�n Conforme de telev�a';
    STR_PLANTILLA_CARATULA_JURIDICA     = 'Plantilla de Caratula personeria Jur�dica';
    STR_PLANTILLA_CARATULA_NATURAL      = 'Plantilla de Caratula personeria Natural';
    STR_PUNTO_ENTREGA                   = 'Puntos de Entrega';
    STR_PUNTO_ENTREGA_NO_DISPONIBLES    = 'Puntos de Entrega no Disponibles';
	STR_REPRESENTANTE					= 'Datos del Representante';
    STR_PERSONAL 						= 'Personal';
    STR_PERSONA                         = 'Persona';
	STR_PROGRAMACION 					= 'Programaci�n';
	STR_PROGRAMACION_OS 				= 'Programaci�n de �rden de Servicio';
	STR_PROVEEDOR 						= 'Proveedor';
	STR_PROVEEDORES_MATERIALES 			= 'Proveedores de Materiales';
	STR_PUNTOS_VENTA         			= 'Puntos de Venta';
	STR_REGISTRO_CENTRO_COSTOS 			= 'registro de Centro de Costos';
	STR_RUT_RUN                			= 'RUT/RUN';
	STR_RUT                 			= 'RUT';
	STR_RUN                 			= 'RUN';
    STR_SOLICITUD                       = 'Solicitud';
	STR_STOCK							= 'Stock';
	STR_TELEVIA							= 'Telev�a';
	STR_TAGS							= 'TAGs';
    STR_TELEFONO                        = 'Tel�fono: ';
	STR_TELEFONO_PRINCIPAL 				= 'Tel�fono Principal';
	STR_TELEFONO_ALTERNATIVO			= 'Tel�fono Alternativo';
	STR_TAREA_PROGRAMADA 				= 'Tarea Programada';
	STR_TIPOS_CAUSA 	 				= 'Tipos de Causa';
	STR_TIPOS_FALLA 					= 'Tipos de Falla';
	STR_TIPOS_IMPACTO 					= 'Tipos de Impacto';
	STR_TIPOS_MANTENIMIENTO 			= 'Tipos de Mantenimiento';
	STR_TIPOS_MATERIALES 				= 'Tipos de Materiales';
	STR_TIPOS_UBICACIONES 				= 'Tipos de Ubicaciones';
	STR_ZONAS_MANTENIMIENTO 			= 'Zonas de Mantenimiento';
    STR_SOLICITUD_NRO                   = 'Solicitud n�mero ';
    STR_OBSERVACION                     = 'Ver Observaci�n';
    STR_NUMERO_DOCUMENTO                = 'N�mero de Documento';
    STR_PASE_DIARIO						= 'Pase diario';

   MSG_ERROR_TELEVIA = 'El n�mero del telev�a es incorrecto';
   MSG_ERROR_ESTADO_TELEVIA = 'Debe indicar el estado del telev�a';

    CAPTION_VALIDAR_DATOS_VEHICULO  = 'Validar datos del Veh�culo';
	MSG_ERROR_VALIDAR_PATENTE       = 'Debe indicar una patente.';
    MSG_ERROR_VALIDAR_MARCA         = 'Debe indicar una marca.';
    MSG_VALIDAR_DIGITO_VERIFICADOR  = 'Debe ingresar el d�gito verificador de la patente';
    MSG_ERROR_VALIDAR_ANIO          = 'El a�o es incorrecto. ' + #13#10 + 'Debe ser mayor a 1900 y menor al a�o siguiente al actual. ';
    MSG_ERROR_VALIDAR_TIPO          = 'Debe indicar un tipo.';
    CAPTION_VEHICULO_ROBADO         = 'Veh�culo Robado';
    MSG_ERROR_VEHICULO_ROBADO       = 'No se puede ingresar un veh�culo robado';
    MSG_ERROR_TELEVIA_MALO          = 'No se puede utilizar el telev�a ingresado porque su estado no es bueno';

	CAPTION_TITULAR_TARJETA_CREDITO =' - Datos del Mandante - Titular de la Tarjeta de Credito';
	CAPTION_TITULAR_CUENTA_BANCARIA =' - Datos del Mandante - Titular de la Cuenta Bancaria';
    STR_TARJETA = 'TARJETA:';
	STR_CUENTA = 'CUENTA:';
	STR_DATOS_MANDANTE = 'DATOS DEL MANDANTE: ';
	STR_ERROR = 'Error';
	MSG_ERROR_INICIALIZACION_FORMULARIO = 'Error de Inicializaci�n de Formulario';
    // liberacion de patentes
    CAPTION_ERROR = 'Error';
    MSG_ERROR_LIBERAR_PATENTE = 'Error al liberar patente';
    MSG_ERROR_BUSQUEDA = 'Error de b�squeda';
    TXT_BUSQUEDA_DE_PATENTES = 'B�squeda de Patentes';
    MSG_NO_SE_DETECTARON_VEHICULOS = 'No se han detectado veh�culos con esta patente';
    MSG_NO_SE_DETECTARON_VEHICULOS_CUENTA_ACTIVA = 'La patente no esta asociada a ninguna cuenta activa';
    MSG_VARIOS_VEHICULOS_PARA_LA_PATENTE = 'Atenci�n, se han detectado varios veh�culos para la patente ';
    MSG_PATENTE_SIN_FORMATO_NACIONAL = 'La patente ingresada no presenta el formato de patente nacional.';
    //MSG_PATENTE_DE_COSTANERA_NORTE = 'La patente es de Costanera-Norte.';		//SS_1147_MCA_20140408		
    MSG_CONFIRMA_LIBERAR_PATENTE = 'Confirma liberar la patente ';
    CAPTION_LIBERAR_PATENTE = 'Liberar Patente';
    MSG_DEBE_INGRESAR_PATENTE  = 'Debe ingresar una patente para realizar la b�squeda';
    MSG_DEBE_INGRESAR_DIGITO = 'Debe ingresar el digito verificador de la patente';
    MSG_SELECCIONO_DV_Z = 'Usted esta seleccionando una patente de digito verificador Z para liberar, esta seguro que desea continuar ?';
    MSG_DIGITO_VERIFICADOR_INCORRECTO = 'El digito verificador es incorrecto.';
    MSG_PATENTE_NO_EXISTE = 'La patente no existe.';
    MSG_PATENTE_LIBERADA = 'La patente ingresada ha sido previamente liberada.';
    MSG_PATENTE_NO_LIBERADA = 'No es posible liberar la patente ingresada.';
    MSG_PATENTE_LIBERADA_EXITOSAMENTE = 'La patente seleccionada se liber� exitosamente';
    MSG_CUENTA_SUSPENDIDA = 'La cuenta de la patente seleccionada est� suspendida';
    MSG_ERROR_BD_VALIDAR_FORMATO_PATENTE = 'Error al validar el formato de la patente';
    MSG_ERROR_OPEN_SHIFT = 'Este usuario ya posee un turno abierto en otro puesto de venta y/o estaci�n de trabajo. Debe cerrarse para poder abrir el siguiente.'; // SS 510 


    //SS 833
    TDCBX_AUTOMATICO_STR = '(Autom�tico)';
    TDCBX_AUTOMATICO_COD = 'AU';


    // Rev.4 SS 786 20110105 EBA
    CAPTION_REVERSA_PATENTE = 'Reversar Patente Liberada';
    MSG_PATENTE_REVERSADA_EXITOSAMENTE = 'La patente fue reversada exitosamente';
    MSG_CONFIRMA_REVERSA_PATENTE = 'Confirma reversar la liberacion de patente ';
    MSG_PATENTE_PARA_REVERSAR = 'Patente previamente liberada, s�lo es posible reversar';

    // Inicio Mensajes del WebService de AutoInscripcionPAK                                // SS_1151_CQU_20140103
    rsRespuestaGeneral          = 'No es posible adherir AraucoTag,'                       // SS_1151_CQU_20140103
                                  + ' favor cont�ctese con Costanera Norte al 24 900 900'  // SS_1151_CQU_20140103
                                  + ' o Centro de Atenci�n en primer piso';                // SS_1151_CQU_20140103
    rsRutInvalido               = 'Rut Inv�lido';                                          // SS_1151_CQU_20140103
    rsErrorInesperado           = 'Error inesperado, mensaje del sistema: %s';             // SS_1151_CQU_20140103
    rsDemasiadasPatentes        = 'Ha enviado m�s patentes de las permitidas (%d)';        // SS_1151_CQU_20140103
    rsClienteDemasiadasPatentes = 'El Cliente tiene m�s patentes de las permitidas (%d)';  // SS_1151_CQU_20140103
    rsClientePersonaJuridica    = 'Rut no pertenece a Persona Natural';                    // SS_1151_CQU_20140103
    rsClienteNoExiste           = 'El Cliente no existe';                                  // SS_1151_CQU_20140103    
    rsClienteYaAdherido         = 'El Cliente ya est� adherido al servicio AraucoTAG.';    // SS_1151_CQU_20140103
    rsClientePuedeAdeherir      = 'El Cliente puede adherir servicio AraucoTAG.';          // SS_1151_CQU_20140103
    rsClienteNoPuedeAdeherir    = 'El Cliente no puede adherir servicio AraucoTAG.';       // SS_1151_CQU_20140103
    rsClienteSinCuentas         = 'Cliente no tiene cuentas o no est�n activas';           // SS_1151_CQU_20140103
    rsClienteNoHabilitadoMail   = 'No tiene habilitado el env�o por eMail.';               // SS_1151_CQU_20140103
    rsErrorConsulta             = 'Ha ocurrido un error, mensaje del sistema: %s';         // SS_1151_CQU_20140103
    rsPatenteYaAdherido         = 'La patente ya est� adherida al servicio AraucoTAG.';    // SS_1151_CQU_20140103
    rsPatentePuedeAdeherir      = 'La patente puede adherir servicio AraucoTAG.';          // SS_1151_CQU_20140103
    rsPatenteNoPuedeAdeherir    = 'La patente no puede adherir servicio AraucoTAG.';       // SS_1151_CQU_20140103
    rsInfraccionesPendiente     = 'Infracciones Pendientes';                               // SS_1151_CQU_20140103
    rsTagVencido                = 'TAGs Vencidos';                                         // SS_1151_CQU_20140103
    rsCuentaSuspendida          = 'Cuentas Suspendidas';                                   // SS_1151_CQU_20140103
    rsSinTag                    = 'Tr�nsitos sin lectura TAG';                             // SS_1151_CQU_20140103
    rsListaAmarilla             = 'En Lista Amarilla';                                     // SS_1151_CQU_20140103
    rsCuentaBaja                = 'Sin Cuentas Activas';                                   // SS_1151_CQU_20140103
    rsNoSeEncuentra             = 'No se Encontr� la Cuenta para el Cliente Indicado';     // SS_1151_CQU_20140103
    rsSinPatente                = 'Debe enviar al menos una patente para adherir';         // SS_1151_CQU_20140103
    rsMailInvalido              = 'eMail Inv�lido';                                        // SS_1151_CQU_20140103
    rsNoSePudoActualizarMail    = 'No se pudo actualizar el mail';                         // SS_1151_CQU_20140103
    rsNoenvioMail               = 'No tiene habilitado el env�o por eMail y no indic� uno';// SS_1151_CQU_20140103
    rsErrorEnAlta               = 'Ocurri� un problema en el Alta';                        // SS_1151_CQU_20140103
    rsAltaOK                    = 'El Alta de patente ha sido realizada correctamente';    // SS_1151_CQU_20140103
    rsClienteOK                 = 'Cliente fue adherido exitosamente';                     // SS_1151_CQU_20140103
    rsRutNoPertenece            = 'El Rut recibido no coincide con el Consultado';         // SS_1151_CQU_20140103
    rsBajaForzada               = 'Baja Forzada';                                          // SS_1151_CQU_20140103
    // Fin Mensajes del WebService de AutoInscripcionPAK                                   // SS_1151_CQU_20140103


implementation

end.
