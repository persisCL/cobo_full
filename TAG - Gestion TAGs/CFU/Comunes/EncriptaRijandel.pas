{********************************** File Header ********************************
File Name   : EncriptaRijandel
Author      : Svigay
Date Created: 21/10/2008
Language    : ES-AR
Description :
                Reliza la Encriptacion con el algoritmo simetrico Rijandel que
                es la implementacion del AES usado por el gobierno de USA.
                Admite que la clave sea  hasta 32 caracteres

*}





unit EncriptaRijandel;


interface

const
 // Se definen Contrase�as para los diferentes modulos

 //Contrase�a para reporte
CONTRASENIA_REPORTE = '123';
//Contrase�a para leer la  contrase�a Registro de SmartCard
CONTRASENIA_LECTURA_SMARTCARD ='987654321';


function Cifrar(Str,Clave: String): String;

function Descifrar(Str,Clave: String): String;


implementation

uses Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, AES, Base64, StdCtrls;



function Cifrar(Str,Clave: String): String;
var
  Src: TStringStream;
  Dst: TMemoryStream;
  Size: Integer;
  Key: TAESKey;
  ExpandedKey: TAESExpandedKey;
begin
  Result:= EmptyStr;
  Src:= TStringStream.Create(Str);
  try
    Dst:= TMemoryStream.Create;
    try
      // Preparamos la clave, lo ideal es que tenga 32 caracteres
      FillChar(Key,Sizeof(Key),#0);
      if Length(Clave) > Sizeof(Key) then
        move(PChar(Clave)^,Key,Sizeof(key))
      else
        move(PChar(Clave)^,Key,Length(Clave));
      AEsExpandKey(ExpandedKey,Key);
      // Guardamos el tama�o del texto original
      Size:= Src.Size;
      Dst.WriteBuffer(Size,Sizeof(Size));
      // Ciframos el texto
      AESEncryptStreamECB(Src,Dst,ExpandedKey);
      // Lo codificamos a base64
      Result:= Trim(BinToStr(Dst.Memory,Dst.Size));
    finally
      Dst.Free;
    end;
  finally
    Src.Free;
  end;
end;

 function Descifrar(Str,Clave: String): String;
var
  Src: TMemoryStream;
  Dst: TStringStream;
  Size: Integer;
  Key: TAESKey;
  ExpandedKey: TAESExpandedKey;
begin
  Result:= EmptyStr;
  Src:= TMemoryStream.Create;
  try
    Dst:= TStringStream.Create(Str);
    try
      StrToStream(Str,Src);
      Src.Position:= 0;
      FillChar(Key,Sizeof(Key),#0);
      if Length(Clave) > Sizeof(Key) then
        move(PChar(Clave)^,Key,Sizeof(key))
      else
        move(PChar(Clave)^,Key,Length(Clave));
      AESExpandKey(ExpandedKey,Key);
      // Leemos el tama�o del texto
      Src.ReadBuffer(Size,Sizeof(Size));
      AESDecryptStreamECB(Src,Dst,ExpandedKey);
      Dst.Size:= Size;
      Result:=Dst.DataString;
    finally
      Dst.Free;
    end;
  finally
    Src.Free;
  end;
end;


















end.
