object FormSolicitudTrabajo: TFormSolicitudTrabajo
  Left = 223
  Top = 204
  Width = 682
  Height = 369
  Caption = 'Nueva Solicitud de Trabajo'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object btnAceptar: TButton
    Left = 501
    Top = 300
    Width = 75
    Height = 25
    Caption = '&Aceptar'
    Default = True
    TabOrder = 0
  end
  object btnCancelar: TButton
    Left = 580
    Top = 300
    Width = 75
    Height = 25
    Cancel = True
    Caption = '&Cancelar'
    TabOrder = 1
  end
end
