{
    File Name: CambiarUbicaciones
    Author : Malisia, Fabio
    Date Created: 27/05/04
    Language : ES-AR

    Description : Cambio de ubicaci�n de TV entre almacenes

    Revision : 1
        Author: pdominguez
        Date: 29/06/2010
        Description: SS 900 - Error cambio almacen m�dulo Gesti�n Telev�a
            - Se a�aden los siguientes objetos:
                btnAgregarLinea, btnEditarLinea: TButton;

            - Se a�aden/modifican los siguientes procedimientos/funciones:
                frameMovimientoStockcdsTAGsAfterPost,
                frameMovimientoStockcdsTAGsBeforeEdit,
                frameMovimientoStockcdsTAGsNewRecord,
                btnAgregarLineaClick,
                btnBorrarLineaClick,
                btnEditarLineaClick,
                ActualizaControlesEntradaDatos,
                inicializa,
                btn_aceptarClick
Revision : 2
        Author: jjofre
        Date: 19/07/2010
        Description: SS 904 - Televias en NULL
            - Se a�aden/modifican los siguientes procedimientos/funciones:
                btn_aceptarClick

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

}
unit CambiarUbicaciones;

interface    

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, BuscaClientes, DmiCtrls,Util, UtilDB, UtilProc, StrUtils,
  DMConnection, PeaProcs, DB, ADODB, ExtCtrls, DPSControls, Buttons,
  DBCtrls, peatypes, VariantComboBox, freMovimientoStock;

type
  TfrmCambiarUbicaciones = class(TForm)
    qryOrigen: TADOQuery;
    qryDestino: TADOQuery;
	btn_aceptar: TButton;
	btn_cancelar: TButton;
    gbAlmacenes: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    cbOrigen: TVariantComboBox;
    Label1: TLabel;
    cbDestino: TVariantComboBox;
    frameMovimientoStock: TframeMovimientoStock;
    btnBorrarLinea: TButton;
    btnAgregarLinea: TButton;
    btnEditarLinea: TButton;
	procedure btn_aceptarClick(Sender: TObject);
	procedure btn_cancelarClick(Sender: TObject);
    procedure rgTipoMovimientoClick(Sender: TObject);
    procedure cbOrigenChange(Sender: TObject);
    procedure btnBorrarLineaClick(Sender: TObject);
    procedure btnAgregarLineaClick(Sender: TObject);
    procedure frameMovimientoStockcdsTAGsAfterPost(DataSet: TDataSet);
    procedure btnEditarLineaClick(Sender: TObject);
    procedure frameMovimientoStockcdsTAGsBeforeEdit(DataSet: TDataSet);
    procedure frameMovimientoStockcdsTAGsNewRecord(DataSet: TDataSet);
    procedure frameMovimientoStockbtnGrabarClick(Sender: TObject);
  private
    FCodigoBodegaOrigen		: integer;
	FUsuarioAdministrador	: boolean;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
    procedure CargarAlmacenesOrigen(CodigoAlmacen: Integer);
    procedure CargarAlmacenesDestino(CodigoAlmacen: Integer);
    procedure ActualizaControlesEntradaDatos(EntradaDatosHabilitada: Boolean);
  public
    function inicializa(MDIChild: boolean; aCaption: AnsiString; OrdenServicio: integer = 0; CodigoAlmacenOrigen: integer = -1):Boolean;
  end;

const
    OP_TAG_MOV_ENTRE_ALMACENES = 7;

implementation

{$R *.dfm}

procedure TfrmCambiarUbicaciones.CargarAlmacenesOrigen(CodigoAlmacen: Integer);
var
    i: Integer;
begin
    i := -1;
    qryOrigen.Close;
    qryOrigen.Parameters.ParamByName('CodigoOperacion').value := OP_TAG_MOV_ENTRE_ALMACENES;
    if FUsuarioAdministrador then begin
	    qryOrigen.Parameters.ParamByName('CodigoBodegaOrigen').value := 0;
	    qryOrigen.Parameters.ParamByName('CodigoBodegaOrigenAux').value := 0
    end
    else begin
	    qryOrigen.Parameters.ParamByName('CodigoBodegaOrigen').value := FCodigoBodegaOrigen;
	    qryOrigen.Parameters.ParamByName('CodigoBodegaOrigenAux').value := FCodigoBodegaOrigen;
    end;

 	qryOrigen.Open;
    While not qryOrigen.Eof do begin
    	cbOrigen.Items.Add(qryOrigen.FieldByName('Descripcion').AsString, qryOrigen.FieldByName('CodigoAlmacen').AsInteger);
		if qryOrigen.FieldByName('CodigoAlmacen').AsInteger = CodigoAlmacen then i := cbOrigen.Items.Count - 1;
		qryOrigen.Next;
	end;
	cbOrigen.ItemIndex := i;
    cbOrigen.Refresh
end;

procedure TfrmCambiarUbicaciones.CargarAlmacenesDestino(CodigoAlmacen: Integer);
var
    i: Integer;
begin
    i := -1;
    cbDestino.Clear;
    qryDestino.Close;
    qryDestino.SQL.Text := 'select  MaestroAlmacenes.CodigoAlmacen, MaestroAlmacenes.Descripcion ';
    qryDestino.SQL.Text := qryDestino.SQL.Text + 'from AlmacenesDestinoOperacion  WITH (NOLOCK) , MaestroAlmacenes  WITH (NOLOCK) ';
    qryDestino.SQL.Text := qryDestino.SQL.Text + 'where AlmacenesDestinoOperacion.CodigoAlmacen = MaestroAlmacenes.CodigoAlmacen AND ';
    qryDestino.SQL.Text := qryDestino.SQL.Text + 'CodigoOperacion = ' + inttostr(OP_TAG_MOV_ENTRE_ALMACENES);
    if not FUsuarioAdministrador then begin
        qryDestino.SQL.Text := qryDestino.SQL.Text + ' AND MaestroAlmacenes.CodigoBodega = ' + inttostr(FCodigoBodegaOrigen);
    end;

 	qryDestino.Open;
    While not qryDestino.Eof do begin
    	cbDestino.Items.Add(qryDestino.FieldByName('Descripcion').AsString, qryDestino.FieldByName('CodigoAlmacen').AsInteger);
		if qryDestino.FieldByName('CodigoAlmacen').AsInteger = CodigoAlmacen then i := cbDestino.Items.Count - 1;
		qryDestino.Next;
	end;
	cbDestino.ItemIndex := i;
end;

{
    Revision : 1
        Author: pdominguez
        Date: 29/06/2010
        Description: SS 900 - Error cambio almacen m�dulo Gesti�n Telev�a
            - Se coloca el cursor en espera mientras el formulario carga.
}
function TfrmCambiarUbicaciones.inicializa(MDIChild: boolean; aCaption: AnsiString; OrdenServicio: integer = 0; CodigoAlmacenOrigen: integer = -1): Boolean;
Var
	S: TSize;
begin
    try
        FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
        FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
        FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
        FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
        Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216
        Screen.Cursor := crHourGlass;
        Application.ProcessMessages;

        result := false;
	    if MDIChild then begin
		    S := GetFormClientSize(Application.MainForm);
    		SetBounds(0, 0, S.cx, S.cy);
	    end else begin
		    FormStyle := fsNormal;
		    Visible := False;
		    CenterForm(Self);
    	end;

        FCodigoBodegaOrigen  := QueryGetValueInt(DMConnections.BaseCAC, 'select ISNULL((Select CodigoBodega From MaestroAlmacenes  WITH (NOLOCK) where MaestroAlmacenes.CodigoAlmacen = ' + IntToStr(CodigoAlmacenOrigen) + '), -1)');
        // INICIO	: 20160617 CFU
	    //FUsuarioAdministrador:= QueryGetValueInt(DMConnections.BaseCAC, 'select ISNULL((Select 1 From GruposUsuario  WITH (NOLOCK) where GruposUsuario.CodigoUsuario = ' + QuotedStr(UsuarioSistema) + ' and CodigoGrupo = ' + QuotedStr('Administradores') + '), 0)') = 1;
	    FUsuarioAdministrador:= QueryGetValueInt(DMConnections.BaseBO_Master, 'select ISNULL((Select 1 From GruposUsuario  WITH (NOLOCK) where GruposUsuario.CodigoUsuario = ' + QuotedStr(UsuarioSistema) + ' and CodigoGrupo = ' + QuotedStr('Administradores') + '), 0)') = 1;
        //Revision 1
        //FUsuarioAdministrador:= True;   //dejo la funcionalidad anterior por si en algun momento la requieren nuevamente...
        // TERMINO	: 20160617 CFU

        try
//        frameMovimientoStock.Inicializar(1);
	        self.Caption := AnsiReplaceStr(aCaption, '&', '');

            if FUsuarioAdministrador then begin
                CargarAlmacenesOrigen(1);
                frameMovimientoStock.inicializar(cbOrigen.Value);
                CargarAlmacenesDestino(cbOrigen.Value);
            end else begin
                CargarAlmacenesOrigen(CodigoAlmacenOrigen);
                frameMovimientoStock.inicializar(CodigoAlmacenOrigen);
                cbOrigen.enabled := False;
                CargarAlmacenesDestino(CodigoAlmacenOrigen);
            end;

//        CargarAlmacenesDestino(-1);

            Result := True;
        except
            on e: Exception do begin
                MsgBoxErr('Error', e.Message, 'Error al Inicializar', 0);
            end;
        end;
    finally
        Screen.Cursor := crDefault;
        Application.ProcessMessages;
    end;
end;

{
    Revision : 1
        Author: pdominguez
        Date: 30/06/2010
        Description: SS 900 - Error cambio almacen m�dulo Gesti�n Telev�a
            - Se habilitan los combos si se grab� con exito los cambios.
}
procedure TfrmCambiarUbicaciones.btn_aceptarClick(Sender: TObject);
resourcestring
    CAPTION_VALIDAR_ENTREGA = 'Movimiento de telev�as';
    ERROR_VALIDAR_ENTREGA = 'Para realizar un movimiento entre almacenes, los mismos no pueden ser iguales.';
    ERROR_ALMACEN_ORIGEN = 'Para realizar un movimiento entre almacenes, debe seleccionar el almac�n origen.';
    ERROR_ALMACEN_DESTINO = 'Para realizar un movimiento entre almacenes, debe seleccionar el almac�n destino.';
    MSG_OK = 'Movimiento realizado con �xito';
begin
    // Validamos que las cantidades en la grilla sean correctas
    if not frameMovimientoStock.ValidarMovimiento(cbDestino.value) then exit;

    if not ValidateControls([cbDestino, cbOrigen, cbDestino],
      [cbOrigen.value <> cbDestino.value, cbOrigen.ItemIndex <> -1, cbDestino.ItemIndex <> -1],
      CAPTION_VALIDAR_ENTREGA,
      [ERROR_VALIDAR_ENTREGA, ERROR_ALMACEN_ORIGEN, ERROR_ALMACEN_DESTINO]) then exit;

    if frameMovimientoStock.GenerarMovimiento(cbDestino.Value, -1, OP_TAG_MOV_ENTRE_ALMACENES) then begin
        //REV. 2
        //MsgBox(MSG_OK, caption, MB_ICONINFORMATION);
        frameMovimientoStock.Limpiar;
        frameMovimientoStock.CodigoAlmacenOrigen := cbOrigen.Value;
        cbOrigen.Enabled := True;
        cbDestino.Enabled := True;
    end;

end;

procedure TfrmCambiarUbicaciones.btn_cancelarClick(Sender: TObject);
begin
    Close;
end;

procedure TfrmCambiarUbicaciones.rgTipoMovimientoClick(Sender: TObject);
begin
    qryOrigen.close;
    qryOrigen.Parameters.ParamByName('CodigoOperacion').value := OP_TAG_MOV_ENTRE_ALMACENES;
    qryOrigen.Parameters.ParamByName('Todos').value := 1;
    qryOrigen.Open;

    qryDestino.close;
    qryDestino.Parameters.ParamByName('CodigoOperacion').value := OP_TAG_MOV_ENTRE_ALMACENES;
    qryDestino.Parameters.ParamByName('Todos').value := 1;
    qryDestino.Open;
end;

procedure TfrmCambiarUbicaciones.cbOrigenChange(Sender: TObject);
begin
    frameMovimientoStock.CodigoAlmacenOrigen := cbOrigen.Value;
    FCodigoBodegaOrigen := cbOrigen.Value;
    CargarAlmacenesDestino(-1);
end;

{
    Procedure Name: frameMovimientoStockcdsTAGsAfterPost
    Parameters : DataSet: TDataSet
    Author : pdominguez
    Date Created : 29/06/2010

    Description : SS 900 - Error cambio almacen m�dulo Gesti�n Telev�a
        - Ejecuta el evento correspondiente del frame y actualiza los controles del Formulario.
}
procedure TfrmCambiarUbicaciones.frameMovimientoStockbtnGrabarClick(
  Sender: TObject);
begin
  frameMovimientoStock.btnGrabarClick(Sender);

end;

procedure TfrmCambiarUbicaciones.frameMovimientoStockcdsTAGsAfterPost(
  DataSet: TDataSet);
begin
    frameMovimientoStock.cdsTAGsAfterPost(DataSet);
    ActualizaControlesEntradaDatos(False);
end;

{
    Procedure Name: frameMovimientoStockcdsTAGsBeforeEdit
    Parameters : DataSet: TDataSet
    Author : pdominguez
    Date Created : 29/06/2010

    Description : SS 900 - Error cambio almacen m�dulo Gesti�n Telev�a
        - Ejecuta el evento correspondiente del frame y actualiza los controles del Formulario.
}
procedure TfrmCambiarUbicaciones.frameMovimientoStockcdsTAGsBeforeEdit(
  DataSet: TDataSet);
begin
    frameMovimientoStock.cdsTAGsBeforeEdit(DataSet);
    ActualizaControlesEntradaDatos(True);
end;

{
    Procedure Name: frameMovimientoStockcdsTAGsNewRecord
    Parameters : DataSet: TDataSet
    Author : pdominguez
    Date Created : 29/06/2010

    Description : SS 900 - Error cambio almacen m�dulo Gesti�n Telev�a
        - Ejecuta el evento correspondiente del frame y actualiza los controles del Formulario.
}
procedure TfrmCambiarUbicaciones.frameMovimientoStockcdsTAGsNewRecord(
  DataSet: TDataSet);
begin
    frameMovimientoStock.cdsTAGsNewRecord(DataSet);
    ActualizaControlesEntradaDatos(True);
end;

{
    Procedure Name: btnAgregarLineaClick
    Parameters : Sender: TObject
    Author : pdominguez
    Date Created : 29/06/2010

    Description : SS 900 - Error cambio almacen m�dulo Gesti�n Telev�a
        - A�ade un Registro en el objeto cdsTAGs.
}
procedure TfrmCambiarUbicaciones.btnAgregarLineaClick(Sender: TObject);
begin
    if (frameMovimientoStock.cdsTAGs.Active) then frameMovimientoStock.cdsTAGs.Append;
end;

{
    Revision : 1
        Author: pdominguez
        Date: 29/06/2010
        Description: SS 900 - Error cambio almacen m�dulo Gesti�n Telev�a
            - Se deshabilita el bot�n de borrado, en el caso de que se haya
            eliminado el �ltimo registro.
            - S�lo se habiltan los combos de almacenes si no existe ning�n
            registro en el objeto cdsTAGs.
}
procedure TfrmCambiarUbicaciones.btnBorrarLineaClick(Sender: TObject);
begin
    try
        if (frameMovimientoStock.cdsTAGs.Active) and (frameMovimientoStock.cdsTAGs.RecordCount > 0) then
    	    frameMovimientoStock.cdsTAGs.Delete;
    finally
        btnBorrarLinea.Enabled := frameMovimientoStock.cdsTAGs.RecordCount > 0;
        cbOrigen.Enabled  := frameMovimientoStock.cdsTAGs.RecordCount = 0;
        cbDestino.Enabled := frameMovimientoStock.cdsTAGs.RecordCount = 0;
        frameMovimientoStock.dbgTags.SetFocus;
    end;
end;

{
    Procedure Name: btnEditarLineaClick
    Parameters : Sender: TObject
    Author : pdominguez
    Date Created : 29/06/2010

    Description : SS 900 - Error cambio almacen m�dulo Gesti�n Telev�a
        - Pone en edici�n el Registro activo del objeto cdsTAGs.
}
procedure TfrmCambiarUbicaciones.btnEditarLineaClick(Sender: TObject);
begin
    if (frameMovimientoStock.cdsTAGs.Active) then frameMovimientoStock.cdsTAGs.Edit;
end;

{
    Procedure Name: ActualizaControlesEntradaDatos
    Parameters    : EntradaDatosHabilitada: Boolean

    Author       : pdominguez
    Date Created : 29/06/2010
    Description  : SS 900 - Error cambio almacen m�dulo Gesti�n Telev�a
        - Habilita o Deshabilita los controles de la pantalla en funci�n de si
        est� o no el panel de entrada de Datos habilitado.
}
procedure TfrmCambiarUbicaciones.ActualizaControlesEntradaDatos(EntradaDatosHabilitada: Boolean);
begin
    btnBorrarLinea.Enabled  := (Not EntradaDatosHabilitada) and (frameMovimientoStock.cdsTAGs.RecordCount > 0);
    btnAgregarLinea.Enabled := Not EntradaDatosHabilitada;
    btnEditarLinea.Enabled  := Not EntradaDatosHabilitada;
    btn_aceptar.Enabled     := Not EntradaDatosHabilitada;
    btn_cancelar.Enabled    := Not EntradaDatosHabilitada;
    cbOrigen.Enabled        := (Not EntradaDatosHabilitada) and (frameMovimientoStock.cdsTAGs.RecordCount = 0);
    cbDestino.Enabled       := (Not EntradaDatosHabilitada) and (frameMovimientoStock.cdsTAGs.RecordCount = 0);
end;

end.
