{********************************** File Header ********************************
File Name   : CambioPassword.pas
Author      :
Date Created:
Language    : ES-AR
Description :
--------------------------------------------------------------------------------
Historial de Revisiones
--------------------------------------------------------------------------------
Author      : Castro, Ra�l
Date        : 28/10/04
Description : Agregado de Fecha de Cambio de Password

Firma       : SS_1147_NDR_20141216
Descripcion : Color de fondo del formulario segun concesionaria nativa (sale del install.ini)

Etiqueta	: 20160307 MGO
Descripci�n	: Se usa BaseBO_Master para ValidarLogin
*******************************************************************************}
unit CambioPassword;

interface

uses
{ INICIO : 20160307 MGO
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBTables, Db, StdCtrls, ExtCtrls, UtilProc, UtilDB, ADODB, Variants,
  DPSControls, Util;
}
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBTables, Db, StdCtrls, ExtCtrls, UtilProc, UtilDB, ADODB, Variants,
  DPSControls, Util, DMConnection;
// FIN : 20160307 MGO
type
  TFormCambioPassword = class(TForm)
	Label1: TLabel;
	Label2: TLabel;
	Label3: TLabel;
	Edit1: TEdit;
	Edit2: TEdit;
	Edit3: TEdit;
	Bevel1: TBevel;
    ValidarLogin: TADOStoredProc;
    Button1: TButton;
    Button2: TButton;
	procedure Button1Click(Sender: TObject);
  private
	{ Private declarations }
	FCodigoUsuario: AnsiString;
    FColorMenu, FColorFont, FColorMenuSel, FColorFontSel : TColor;                      //SS_1147_NDR_20141216
  public
	{ Public declarations }
	Function Inicializa(Conn: TADOConnection; CodigoUsuario: AnsiString): Boolean;
  end;

var
  FormCambioPassword: TFormCambioPassword;

implementation

{$R *.DFM}

Function TFormCambioPassword.Inicializa(Conn: TADOConnection;
  CodigoUsuario: AnsiString): Boolean;
begin
    FColorMenu      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenu', 'clSkyBlue'));          //SS_1147_NDR_20141216
    FColorFont      := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFont', 'clNavy'));             //SS_1147_NDR_20141216
    FColorMenuSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorMenuSel', 'clNavy'));          //SS_1147_NDR_20141216
    FColorFontSel   := StringToColor(InstallIni.ReadString('ColorAPP', 'ColorFontSel', 'clWhite'));         //SS_1147_NDR_20141216
    Color := FColorMenu;                                                                                    //SS_1147_NDR_20141216

	{ INICIO : 20160207 MGO
	ValidarLogin.Connection := Conn;
	} // FIN : 20160307 MGO
	FCodigoUsuario := CodigoUsuario;
	Result := True;
end;

procedure TFormCambioPassword.Button1Click(Sender: TObject);
resourcestring
	MSG_PASSWORD                = 'La contrase�a y su confirmaci�n no coinciden.';
	MSG_SAME_PASSWORD           = 'La contrase�a actual debe ser distinta a la nueva';
	MSG_ERROR_PASSWORD_CHANGE   = 'No se ha podido cambiar la contrase�a actual.';
	MSG_PASSWORD_CHANGED        = 'La contrase�a ha sido cambiada con �xito.';
	CAPTION_PASSWORD_CHANGE     = 'Cambiar Contrase�a';
  MSG_PASSWORD_MIN_CARACTER   = 'La contrase�a debe poseer al menos 6 caracteres';
  MSG_WRONG_CURRENT_PASSWORD  = 'La contrase�a actual es incorrecta';

Var
	Resu: Integer;
    SQL: String;
begin
	if Edit2.Text <> Edit3.text then begin
		MsgBox(MSG_PASSWORD, CAPTION_PASSWORD_CHANGE, MB_ICONSTOP);
		Exit;
	end;

	if Edit1.Text = Edit2.text then begin
		MsgBox(MSG_SAME_PASSWORD, CAPTION_PASSWORD_CHANGE, MB_ICONSTOP);
		Exit;
	end;

	if Length(Edit2.Text) < 6 then begin
		MsgBox(MSG_PASSWORD_MIN_CARACTER, CAPTION_PASSWORD_CHANGE, MB_ICONSTOP);
		Exit;
	end;

    ValidarLogin.Parameters.Refresh;
	  ValidarLogin.Parameters.ParamByName('@CodigoUsuario').Value := FCodigoUsuario;
	  ValidarLogin.Parameters.ParamByName('@Password').Value := Edit1.Text;
	  ValidarLogin.Parameters.ParamByName('@NombreCompleto').Value := NULL;
	  ValidarLogin.Parameters.ParamByName('@DescriError').Value 	 := NULL;
    ValidarLogin.Parameters.ParamByName('@PedirCambiarPassword').Value:= NULL;
	try
		ValidarLogin.ExecProc;
	except
		on e: Exception do begin
			MsgBoxErr(MSG_ERROR_PASSWORD_CHANGE, e.message, CAPTION_PASSWORD_CHANGE, MB_ICONSTOP);
			Exit;
		end;
	end;
	Resu := ValidarLogin.Parameters.ParamByName('@Return_Value').Value;
	if Resu <> 0 then begin
    if Resu = -2 then begin
		  MsgBox(MSG_WRONG_CURRENT_PASSWORD, CAPTION_PASSWORD_CHANGE, MB_ICONSTOP);
	  end
    else begin
		  MsgBox(Trim(ValidarLogin.Parameters.ParamByName('@DescriError').Value), CAPTION_PASSWORD_CHANGE, MB_ICONSTOP);
    end;
		Exit;
	end;
    SQL := 'UPDATE UsuariosSistemas SET Password = CONVERT(VARBINARY(256), pwdencrypt(RTRIM(''' + Trim(Edit2.Text) + '''))) ' +
           ', FechaCambioPassword = GetDate() WHERE CodigoUsuario = ''' + FCodigoUsuario + '''';
	QueryExecute(ValidarLogin.Connection, SQL);

	MsgBox(MSG_PASSWORD_CHANGED, CAPTION_PASSWORD_CHANGE, MB_ICONINFORMATION);
	ModalResult := mrOk;
end;

end.
