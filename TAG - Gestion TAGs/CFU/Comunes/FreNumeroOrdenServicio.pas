{-------------------------------------------------------------------------------
 File Name: FreNumeroOrdenServicio.pas
 Author: Lgisuk
 Date Created: 28/06/05
 Language: ES-AR
 Description: Seccion con la información del Numero de la Orden de Servicio
              permite navegar desde un reclamo cerrado hasta la orden de
              servicio contactar al cliente
-------------------------------------------------------------------------------}
unit FreNumeroOrdenServicio;

interface

uses
  //FreNumeroOrdenServicio
  DMConnection,
  Util,
  Utildb,
  RStrings,
  OrdenesServicio,
  //General
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, VariantComboBox, Validate, DateEdit, UtilProc, DB,ADODB;

type
  TFrameNumeroOrdenServicio = class(TFrame)
    PReclamo: TGroupBox;
    LOrden: TLabel;
    EReclamo: TEdit;
    LContactarCliente: TLabel;
    procedure LContactarClienteClick(Sender: TObject);
  private
    Function  GetCodigoOrdenServicio: string;
    Procedure SetCodigoOrdenServicio(const Value: string);
    { Private declarations }
  public
    { Public declarations }
    Function  Inicializar: Boolean; overload;
    Function  Inicializar(CodigoOrdenServicio: string): Boolean; overload;
    Function  Deshabilitar:boolean;
    Function  Validar: Boolean;
    Procedure LoadFromDataset(DS: TDataset);
    //
    Property CodigoOrdenServicio: string read GetCodigoOrdenServicio write SetCodigoOrdenServicio;
  end;

implementation


{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    lgisuk
  Date Created: 28/06/2005
  Description: inicializacion del frame
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFrameNumeroOrdenServicio.Inicializar: Boolean;
begin
    Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    lgisuk
  Date Created: 28/06/2005
  Description: inicializo el frame eligiendo una fuente de reclamo
  Parameters: CodigoOrdenServicio: Integer
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFrameNumeroOrdenServicio.Inicializar(CodigoOrdenServicio: string): Boolean;
var
    SQLQuery:string;
    TieneOS:string;
begin
    //Muestro el numero de Orden de Servicio
    ereclamo.text := CodigoOrdenServicio;
    //Verifico si tengo que motrar el link para ir a contactar cliente
    SQLQuery:= 'SELECT dbo.TieneOSContactarCliente('+CodigoOrdenServicio+')';
    TieneOS:=queryGetvalue(dmconnections.BaseCAC, SQLQuery);
    lContactarCliente.Visible:= (TieneOs = 'True');
    Result := Inicializar;
end;

{-----------------------------------------------------------------------------
  Function Name: GetCodigoOrdenServicio
  Author:    lgisuk
  Date Created: 28/06/2005
  Description:  Obtengo el codigo de orden de servicio
  Parameters: None
  Return Value: string
-----------------------------------------------------------------------------}
function TFrameNumeroOrdenServicio.GetCodigoOrdenServicio: string;
begin
    result:= ereclamo.text;
end;

{-----------------------------------------------------------------------------
  Function Name: SetFuenteReclamo
  Author:    lgisuk
  Date Created: 28/06/2005
  Description: Asigno el codigo de orden de servicio
  Parameters: const Value: string
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrameNumeroOrdenServicio.SetCodigoOrdenServicio(const Value: string);
begin
    eReclamo.text := Value;
end;

{-----------------------------------------------------------------------------
  Function Name: LoadFromDataset
  Author:    lgisuk
  Date Created: 28/06/2005
  Description: cargo la fuente de reclamo de un dataset
  Parameters: DS: TDataset
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrameNumeroOrdenServicio.LoadFromDataset(DS: TDataset);
begin
    Ereclamo.text := DS['CodigoOrdenServicio'];
end;

{-----------------------------------------------------------------------------
  Function Name: Deshabilitar
  Author:    lgisuk
  Date Created: 28/06/2005
  Description:  deshabilito los campos
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
function TFrameNumeroOrdenServicio.Deshabilitar:boolean;
begin
    result:= true;
end;

{-----------------------------------------------------------------------------
  Function Name: Validar
  Author:    lgisuk
  Date Created: 28/06/2005
  Description: valido el contenido del frame
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFrameNumeroOrdenServicio.Validar: Boolean;
begin
    Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: LContactarClienteClick
  Author:    lgisuk
  Date Created: 28/06/2005
  Description: Permito navegar desde el reclamo cerrado hasta la OS
               contactar cliente.
  Parameters: Sender: TObject
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrameNumeroOrdenServicio.LContactarClienteClick(Sender: TObject);
Resourcestring
    MSG_TITLE = 'Sistema de Reclamos';
    MSG_ERROR = 'Error';
var
    SQLQuery:string;
    CodigoOS:Integer;
begin
    try
        //Verifico si tengo que motrar el link para ir a contactar cliente
        SQLQuery:= 'SELECT dbo.ObtenerOSContactarCliente('+CodigoOrdenServicio+')';
        CodigoOS:= QueryGetValueINT(dmconnections.BaseCAC, SQLQuery);
        EditarOrdenServicio(CodigoOS);
    except
        on E: Exception do begin
             MsgBoxErr(MSG_ERROR,e.Message,MSG_TITLE,0);
        end;
    end;
end;



end.
