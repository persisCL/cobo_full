object frmValueEdit: TfrmValueEdit
  Left = 215
  Top = 221
  BorderStyle = bsDialog
  Caption = 'Editar Valor'
  ClientHeight = 160
  ClientWidth = 548
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 8
    Top = 8
    Width = 537
    Height = 121
  end
  object Label1: TLabel
    Left = 24
    Top = 64
    Width = 27
    Height = 13
    Caption = 'Valor:'
  end
  object lblDescri: TLabel
    Left = 32
    Top = 20
    Width = 505
    Height = 37
    AutoSize = False
    Caption = 'lblDescri'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    WordWrap = True
  end
  object Label2: TLabel
    Left = 57
    Top = 95
    Width = 404
    Height = 13
    Caption = 
      'Si desea explorar para indicar un directorio como valor para est' +
      'e par'#225'metro haga click'
  end
  object Label3: TLabel
    Left = 464
    Top = 95
    Width = 18
    Height = 13
    Cursor = crHandPoint
    Caption = 'ac'#225
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsUnderline]
    ParentFont = False
    OnClick = Label3Click
  end
  object btnAceptar: TButton
    Left = 383
    Top = 132
    Width = 75
    Height = 25
    Caption = '&Aceptar'
    Default = True
    TabOrder = 1
    OnClick = btnAceptarClick
  end
  object btnCancelar: TButton
    Left = 461
    Top = 132
    Width = 75
    Height = 25
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 2
  end
  object txtValor: TEdit
    Left = 55
    Top = 61
    Width = 482
    Height = 21
    TabOrder = 0
    Text = 'txtValor'
  end
end
