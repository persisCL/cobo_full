object FrameCompromisoOrdenServicio: TFrameCompromisoOrdenServicio
  Left = 0
  Top = 0
  Width = 239
  Height = 102
  TabOrder = 0
  object GBCompromisoCliente: TGroupBox
    Left = 2
    Top = 2
    Width = 235
    Height = 96
    Caption = 'Compromiso con el cliente'
    TabOrder = 0
    object Label1: TLabel
      Left = 7
      Top = 27
      Width = 44
      Height = 13
      Caption = 'Prioridad:'
    end
    object Label2: TLabel
      Left = 7
      Top = 55
      Width = 103
      Height = 13
      Caption = 'Fecha Comprometida:'
    end
    object cbPrioridad: TVariantComboBox
      Left = 119
      Top = 24
      Width = 107
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 0
      Items = <
        item
          Caption = 'Muy Alta'
          Value = 1
        end
        item
          Caption = 'Alta'
          Value = 2
        end
        item
          Caption = 'Media'
          Value = 3
        end
        item
          Caption = 'Baja'
          Value = 4
        end>
    end
    object txtFechaCompromiso: TDateEdit
      Left = 119
      Top = 52
      Width = 107
      Height = 21
      AutoSelect = False
      TabOrder = 1
      Date = -693594
    end
  end
end
