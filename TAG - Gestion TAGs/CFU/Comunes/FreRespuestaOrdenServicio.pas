{-------------------------------------------------------------------------------
 File Name: FreRespuestaOrdenServicio.pas
 Author: Lgisuk
 Date Created: 28/06/05
 Language: ES-AR
 Description: Seccion con la información de la respuesta de la concesionaria
              y los comentarios del cliente.
-------------------------------------------------------------------------------}
unit FreRespuestaOrdenServicio;

interface

uses
  //FreRespuestaOrdenServicio
  DMConnection,
  Util,
  Utildb,
  RStrings,
  OrdenesServicio,
  //General
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, VariantComboBox, Validate, DateEdit, UtilProc, DB,ADODB,
  ExtCtrls;

type
  TFrameRespuestaOrdenServicio = class(TFrame)
    LRespuestadelaConcesionaria: TLabel;
    PContactarCliente: TPanel;
    cknotificado: TCheckBox;
    ckconforme: TCheckBox;
    txtDetalleRespuesta: TMemo;
    LComentariosDelCliente: TLabel;
    TxtComentariosdelCliente: TMemo;
  private
    Function  GetObservacionesRespuesta: string;
    Procedure SetObservacionesRespuesta(const Value: string);
    Function  GetObservacionesComentariosCliente: string;
    Procedure SetObservacionesComentariosCliente(const Value: string);
    Function  GetNotificado: boolean;
    Procedure SetNotificado(const Value: boolean);
    Function  GetConforme: boolean;
    Procedure SetConforme(const Value: boolean);
    { Private declarations }
  public
    { Public declarations }
    Function  Inicializar: Boolean; overload;
    Function  Inicializar(ObservacionesRespuesta: string;ObservacionesComentariosCliente: string; Notificado, Conforme:Boolean): Boolean; overload;
    Function  Deshabilitar:boolean;
    Function  Validar: Boolean;
    Procedure LoadFromDataset(DS: TDataset);
    //
    Property  ObservacionesRespuesta: string read GetObservacionesRespuesta write SetObservacionesRespuesta;
    Property  ObservacionesComentariosCliente: string read GetObservacionesComentariosCliente write SetObservacionesComentariosCliente;
    Property  Notificado: Boolean read GetNotificado write SetNotificado;
    Property  Conforme: Boolean read GetConforme write SetConforme;
  end;

implementation


{$R *.dfm}

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    lgisuk
  Date Created: 28/06/2005
  Description: inicializacion del frame
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFrameRespuestaOrdenServicio.Inicializar: Boolean;
begin
    Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: Inicializar
  Author:    lgisuk
  Date Created: 28/06/2005
  Description: inicializo el frame con los datos
  Parameters:
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFrameRespuestaOrdenServicio.Inicializar(ObservacionesRespuesta: string;ObservacionesComentariosCliente: string; Notificado, Conforme:Boolean): Boolean;
begin
    txtdetallerespuesta.text     := ObservacionesRespuesta;
    txtcomentariosdelcliente.Text:= ObservacionesComentariosCliente;
    ckNotificado.Checked         := Notificado;
    ckconforme.Checked           := Conforme;
    Result := Inicializar;
end;

{-----------------------------------------------------------------------------
  Function Name: GetObservacionesRespuesta
  Author:    lgisuk
  Date Created: 28/06/2005
  Description:  Obtengo la respuesta de la concecionaria
  Parameters: None
  Return Value: string
-----------------------------------------------------------------------------}
function TFrameRespuestaOrdenServicio.GetObservacionesRespuesta: string;
begin
    result:= txtdetallerespuesta.text;
end;

{-----------------------------------------------------------------------------
  Function Name: SetObservacionesRespuesta
  Author:    lgisuk
  Date Created: 28/06/2005
  Description: Asigno la respuesta de la concecionaria
  Parameters: const Value: string
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrameRespuestaOrdenServicio.SetObservacionesRespuesta(const Value: string);
begin
    txtdetallerespuesta.text := Value;
end;

{-----------------------------------------------------------------------------
  Function Name: GetObservacionesComentariosCliente
  Author:    lgisuk
  Date Created: 28/06/2005
  Description:  Obtengo los comentarios del cliente
  Parameters: None
  Return Value: string
-----------------------------------------------------------------------------}
function TFrameRespuestaOrdenServicio.GetObservacionesComentariosCliente: string;
begin
    result:= txtComentariosdelCliente.text;
end;

{-----------------------------------------------------------------------------
  Function Name: SetObservacionesComentariosCliente
  Author:    lgisuk
  Date Created: 28/06/2005
  Description: Asigno los comentarios del cliente
  Parameters: const Value: string
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrameRespuestaOrdenServicio.SetObservacionesComentariosCliente(const Value: string);
begin
    txtComentariosdelCliente.text := Value;
end;

{-----------------------------------------------------------------------------
  Function Name: GetNotificado
  Author:    lgisuk
  Date Created: 28/06/2005
  Description:  obtengo notificado
  Parameters: None
  Return Value:
-----------------------------------------------------------------------------}
function TFrameRespuestaOrdenServicio.GetNotificado: boolean;
begin
    result:= ckNotificado.Checked;
end;

{-----------------------------------------------------------------------------
  Function Name: SetNotificado
  Author:    lgisuk
  Date Created: 28/06/2005
  Description:  asigno notificado
  Parameters:
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrameRespuestaOrdenServicio.SetNotificado(const Value: boolean);
begin
    ckNotificado.Checked := Value;
end;

{-----------------------------------------------------------------------------
  Function Name: GetConforme
  Author:    lgisuk
  Date Created: 28/06/2005
  Description: obtengo conforme
  Parameters: None
  Return Value: string
-----------------------------------------------------------------------------}
function TFrameRespuestaOrdenServicio.GetConforme: boolean;
begin
    result:= ckconforme.Checked;
end;

{-----------------------------------------------------------------------------
  Function Name: SetConforme
  Author:    lgisuk
  Date Created: 28/06/2005
  Description:
  Parameters:  asigno conforme
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrameRespuestaOrdenServicio.SetConforme(const Value: boolean);
begin
    ckconforme.Checked := Value;
end;

{-----------------------------------------------------------------------------
  Function Name: LoadFromDataset
  Author:    lgisuk
  Date Created: 28/06/2005
  Description: cargo la fuente de reclamo de un dataset
  Parameters: DS: TDataset
  Return Value: None
-----------------------------------------------------------------------------}
procedure TFrameRespuestaOrdenServicio.LoadFromDataset(DS: TDataset);
begin
    txtdetallerespuesta.text      := DS['ObservacionesRespuesta'];
    txtComentariosdelCliente.text := DS['ObservacionesComentariosCliente'];
    cknotificado.Checked          := DS.FieldByName('Notificado').AsBoolean;
    ckconforme.Checked            := DS.FieldByName('Conforme').AsBoolean;
end;

{-----------------------------------------------------------------------------
  Function Name: Deshabilitar
  Author:    lgisuk
  Date Created: 28/06/2005
  Description:  deshabilito los campos
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
function TFrameRespuestaOrdenServicio.Deshabilitar:boolean;
begin
    txtdetallerespuesta.enabled:= false;
    txtComentariosdelCliente.enabled:= false;
    cknotificado.Enabled:=false;
    ckconforme.Enabled:=false;
    result:= true;
end;

{-----------------------------------------------------------------------------
  Function Name: Validar
  Author:    lgisuk
  Date Created: 28/06/2005
  Description: valido el contenido del frame
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFrameRespuestaOrdenServicio.Validar: Boolean;
begin
    Result := True;
end;

end.
