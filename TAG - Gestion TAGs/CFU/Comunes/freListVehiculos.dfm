object FrameListVehiculos: TFrameListVehiculos
  Left = 0
  Top = 0
  Width = 812
  Height = 270
  TabOrder = 0
  DesignSize = (
    812
    270)
  object dblVehiculos: TDBListEx
    Left = 0
    Top = 15
    Width = 812
    Height = 221
    Anchors = [akLeft, akTop, akRight, akBottom]
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 80
        Header.Caption = 'Tipo Patente'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -12
        Header.Font.Name = 'MS Sans Serif'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'TipoPatente'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 90
        Header.Caption = 'Patente'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'Patente'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 250
        Header.Caption = 'Veh'#237'culo'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'DetalleVehiculoSimple'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Width = 80
        Header.Caption = 'A'#241'o'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        IsLink = False
        FieldName = 'AnioVehiculo'
      end>
    DataSource = dsOrdenServicio
    DragReorder = True
    ParentColor = False
    TabOrder = 0
    TabStop = True
    OnDblClick = dblVehiculosDblClick
  end
  object btnIngresarVehiculo: TDPSButton
    Left = 581
    Top = 242
    Anchors = [akRight, akBottom]
    Caption = '&Agregar'
    TabOrder = 1
    OnClick = btnIngresarVehiculoClick
  end
  object btnEditarVehiculo: TDPSButton
    Left = 658
    Top = 242
    Anchors = [akRight, akBottom]
    Caption = '&Editar'
    TabOrder = 2
    OnClick = btnEditarVehiculoClick
  end
  object btnEliminarVehiculo: TDPSButton
    Left = 735
    Top = 242
    Anchors = [akRight, akBottom]
    Caption = 'E&liminar'
    TabOrder = 3
    OnClick = btnEliminarVehiculoClick
  end
  object ObtenerOrdenServicioAdhesionCuentas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerOrdenServicioAdhesionCuentas;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoOrdenServicio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IndiceCuentas'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end>
    Left = 17
    Top = 199
  end
  object cdsOrdenesServicioAdhesionCuentas: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoOrdenServicio'
        DataType = ftInteger
      end
      item
        Name = 'IndiceCuentas'
        DataType = ftSmallint
      end
      item
        Name = 'TipoPatente'
        DataType = ftString
        Size = 4
      end
      item
        Name = 'Patente'
        Attributes = [faFixed]
        DataType = ftString
        Size = 10
      end
      item
        Name = 'CodigoMarca'
        DataType = ftInteger
      end
      item
        Name = 'DescripcionMarca'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'CodigoModelo'
        DataType = ftInteger
      end
      item
        Name = 'DescripcionModelo'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'CodigoColor'
        DataType = ftInteger
      end
      item
        Name = 'DescripcionColor'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'DetalleVehiculoSimple'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'DetalleVehiculoCompleto'
        DataType = ftString
        Size = 275
      end
      item
        Name = 'TipoAdhesion'
        DataType = ftSmallint
      end
      item
        Name = 'DetallePasadas'
        DataType = ftBoolean
      end
      item
        Name = 'AnioVehiculo'
        DataType = ftSmallint
      end
      item
        Name = 'TieneAcoplado'
        DataType = ftBoolean
      end
      item
        Name = 'Categoria'
        DataType = ftSmallint
      end
      item
        Name = 'DescripcionCategoria'
        Attributes = [faFixed]
        DataType = ftString
        Size = 60
      end
      item
        Name = 'LargoAdicionalVehiculo'
        Attributes = [faFixed]
        DataType = ftString
        Size = 10
      end
      item
        Name = 'CodigoTipoVehiculo'
        DataType = ftSmallint
      end>
    IndexDefs = <
      item
        Name = 'patente'
        Fields = 'patente'
      end>
    IndexName = 'patente'
    Params = <>
    ProviderName = 'dspOrdenesServicioAdhesionCuentas'
    StoreDefs = True
    AfterOpen = cdsOrdenesServicioAdhesionCuentasAfterOpen
    AfterInsert = cdsOrdenesServicioAdhesionCuentasAfterOpen
    AfterEdit = cdsOrdenesServicioAdhesionCuentasAfterOpen
    AfterPost = cdsOrdenesServicioAdhesionCuentasAfterOpen
    AfterDelete = cdsOrdenesServicioAdhesionCuentasAfterOpen
    Left = 49
    Top = 199
    object cdsOrdenesServicioAdhesionCuentasCodigoOrdenServicio: TIntegerField
      FieldName = 'CodigoOrdenServicio'
    end
    object cdsOrdenesServicioAdhesionCuentasIndiceCuentas: TSmallintField
      FieldName = 'IndiceCuentas'
    end
    object cdsOrdenesServicioAdhesionCuentasTipoPatente: TStringField
      FieldName = 'TipoPatente'
      Size = 4
    end
    object cdsOrdenesServicioAdhesionCuentasPatente: TStringField
      FieldName = 'Patente'
      FixedChar = True
      Size = 10
    end
    object cdsOrdenesServicioAdhesionCuentasCodigoMarca: TIntegerField
      FieldName = 'CodigoMarca'
    end
    object cdsOrdenesServicioAdhesionCuentasDescripcionMarca: TStringField
      FieldName = 'DescripcionMarca'
      Size = 30
    end
    object cdsOrdenesServicioAdhesionCuentasCodigoModelo: TIntegerField
      FieldName = 'CodigoModelo'
    end
    object cdsOrdenesServicioAdhesionCuentasDescripcionModelo: TStringField
      FieldName = 'DescripcionModelo'
      Size = 30
    end
    object cdsOrdenesServicioAdhesionCuentasCodigoColor: TIntegerField
      FieldName = 'CodigoColor'
    end
    object cdsOrdenesServicioAdhesionCuentasDescripcionColor: TStringField
      FieldName = 'DescripcionColor'
      Size = 30
    end
    object cdsOrdenesServicioAdhesionCuentasDetalleVehiculoSimple: TStringField
      FieldName = 'DetalleVehiculoSimple'
      Size = 255
    end
    object cdsOrdenesServicioAdhesionCuentasDetalleVehiculoCompleto: TStringField
      FieldName = 'DetalleVehiculoCompleto'
      Size = 275
    end
    object cdsOrdenesServicioAdhesionCuentasTipoAdhesion: TSmallintField
      FieldName = 'TipoAdhesion'
    end
    object cdsOrdenesServicioAdhesionCuentasDetallePasadas: TBooleanField
      FieldName = 'DetallePasadas'
    end
    object cdsOrdenesServicioAdhesionCuentasAnioVehiculo: TSmallintField
      FieldName = 'AnioVehiculo'
    end
    object cdsOrdenesServicioAdhesionCuentasTieneAcoplado: TBooleanField
      FieldName = 'TieneAcoplado'
    end
    object cdsOrdenesServicioAdhesionCuentasCategoria: TSmallintField
      FieldName = 'Categoria'
    end
    object cdsOrdenesServicioAdhesionCuentasDescripcionCategoria: TStringField
      FieldName = 'DescripcionCategoria'
      FixedChar = True
      Size = 60
    end
    object cdsOrdenesServicioAdhesionCuentasLargoAdicionalVehiculo: TStringField
      FieldName = 'LargoAdicionalVehiculo'
      FixedChar = True
      Size = 10
    end
    object cdsOrdenesServicioAdhesionCuentasCodigoTipoVehiculo: TSmallintField
      FieldName = 'CodigoTipoVehiculo'
    end
  end
  object dsOrdenServicio: TDataSource
    DataSet = cdsOrdenesServicioAdhesionCuentas
    Left = 80
    Top = 198
  end
  object cdsOrdenesServicioAdhesionCuentasBorradas: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoOrdenServicio'
        DataType = ftInteger
      end
      item
        Name = 'IndiceCuentas'
        DataType = ftSmallint
      end
      item
        Name = 'TipoPatente'
        DataType = ftString
        Size = 4
      end
      item
        Name = 'Patente'
        Attributes = [faFixed]
        DataType = ftString
        Size = 10
      end
      item
        Name = 'CodigoMarca'
        DataType = ftInteger
      end
      item
        Name = 'DescripcionMarca'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'CodigoModelo'
        DataType = ftInteger
      end
      item
        Name = 'DescripcionModelo'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'CodigoColor'
        DataType = ftInteger
      end
      item
        Name = 'DescripcionColor'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'DetalleVehiculoSimple'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'DetalleVehiculoCompleto'
        DataType = ftString
        Size = 275
      end
      item
        Name = 'TipoAdhesion'
        DataType = ftSmallint
      end
      item
        Name = 'DetallePasadas'
        DataType = ftBoolean
      end
      item
        Name = 'AnioVehiculo'
        DataType = ftSmallint
      end
      item
        Name = 'TieneAcoplado'
        DataType = ftBoolean
      end
      item
        Name = 'Categoria'
        DataType = ftSmallint
      end
      item
        Name = 'DescripcionCategoria'
        Attributes = [faFixed]
        DataType = ftString
        Size = 60
      end
      item
        Name = 'LargoAdicionalVehiculo'
        Attributes = [faFixed]
        DataType = ftString
        Size = 10
      end
      item
        Name = 'CodigoTipoVehiculo'
        DataType = ftSmallint
      end>
    IndexDefs = <>
    Params = <>
    ProviderName = 'dspOrdenesServicioAdhesionCuentas'
    StoreDefs = True
    Left = 537
    Top = 239
    object cdsOrdenesServicioAdhesionCuentasBorradasCodigoOrdenServicio: TIntegerField
      FieldName = 'CodigoOrdenServicio'
    end
    object cdsOrdenesServicioAdhesionCuentasBorradasIndiceCuentas: TSmallintField
      FieldName = 'IndiceCuentas'
    end
    object cdsOrdenesServicioAdhesionCuentasBorradasTipoPatente: TStringField
      FieldName = 'TipoPatente'
      Size = 4
    end
    object cdsOrdenesServicioAdhesionCuentasBorradasPatente: TStringField
      FieldName = 'Patente'
      FixedChar = True
      Size = 10
    end
    object cdsOrdenesServicioAdhesionCuentasBorradasCodigoMarca: TIntegerField
      FieldName = 'CodigoMarca'
    end
    object cdsOrdenesServicioAdhesionCuentasBorradasDescripcionMarca: TStringField
      FieldName = 'DescripcionMarca'
      Size = 30
    end
    object cdsOrdenesServicioAdhesionCuentasBorradasCodigoModelo: TIntegerField
      FieldName = 'CodigoModelo'
    end
    object cdsOrdenesServicioAdhesionCuentasBorradasDescripcionModelo: TStringField
      FieldName = 'DescripcionModelo'
      Size = 30
    end
    object cdsOrdenesServicioAdhesionCuentasBorradasCodigoColor: TIntegerField
      FieldName = 'CodigoColor'
    end
    object cdsOrdenesServicioAdhesionCuentasBorradasDescripcionColor: TStringField
      FieldName = 'DescripcionColor'
      Size = 30
    end
    object cdsOrdenesServicioAdhesionCuentasBorradasDetalleVehiculoSimple: TStringField
      FieldName = 'DetalleVehiculoSimple'
      Size = 255
    end
    object cdsOrdenesServicioAdhesionCuentasBorradasDetalleVehiculoCompleto: TStringField
      FieldName = 'DetalleVehiculoCompleto'
      Size = 275
    end
    object cdsOrdenesServicioAdhesionCuentasBorradasTipoAdhesion: TSmallintField
      FieldName = 'TipoAdhesion'
    end
    object cdsOrdenesServicioAdhesionCuentasBorradasDetallePasadas: TBooleanField
      FieldName = 'DetallePasadas'
    end
    object cdsOrdenesServicioAdhesionCuentasBorradasAnioVehiculo: TSmallintField
      FieldName = 'AnioVehiculo'
    end
    object cdsOrdenesServicioAdhesionCuentasBorradasTieneAcoplado: TBooleanField
      FieldName = 'TieneAcoplado'
    end
    object cdsOrdenesServicioAdhesionCuentasBorradasCategoria: TSmallintField
      FieldName = 'Categoria'
    end
    object cdsOrdenesServicioAdhesionCuentasBorradasDescripcionCategoria: TStringField
      FieldName = 'DescripcionCategoria'
      FixedChar = True
      Size = 60
    end
    object cdsOrdenesServicioAdhesionCuentasBorradasLargoAdicionalVehiculo: TStringField
      FieldName = 'LargoAdicionalVehiculo'
      FixedChar = True
      Size = 10
    end
    object cdsOrdenesServicioAdhesionCuentasBorradasCodigoTipoVehiculo: TSmallintField
      FieldName = 'CodigoTipoVehiculo'
    end
  end
  object dspOrdenesServicioAdhesionCuentas: TDataSetProvider
    DataSet = ObtenerOrdenServicioAdhesionCuentas
    Constraints = True
    Left = 112
    Top = 200
  end
end
