{********************************** File Header ********************************
File Name   : SeleccionarRolYPersona
Author      : Castro, Ra�l <rcastro@dpsautomation.com>
Date Created: 09-Feb-2004
Language    : ES-AR
Description : Permite seleccionar una Persona y su Rol en relaci�n a una cuenta

Revision 1:
    Date: 25/02/2009
    Author: mpiazza
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura del DFM


*******************************************************************************}

unit SeleccionarRolYPersona;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Util,
  Dialogs, StdCtrls, DPSControls, DmiCtrls, BuscaTab, DB,
  ADODB, peatypes;

type
  TfrmSeleccionarRolYPersona = class(TForm)
	BuscaPersona: TBuscaTabEdit;
	Label1: TLabel;
    cb_Roles: TComboBox;
    qry_Roles: TADOQuery;
    btnAceptar: TButton;
    btnCancelar: TButton;
	procedure BuscaPersonaButtonClick(Sender: TObject);
	procedure FormCreate(Sender: TObject);
	procedure Iniciar;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
	{ Private declarations }
  public
    { Public declarations }
	CodigoRol,
	CodigoPersona: Integer
  end;

var
  frmSeleccionarRolYPersona: TfrmSeleccionarRolYPersona;

implementation

uses BuscaClientes;

{$R *.dfm}

procedure TfrmSeleccionarRolYPersona.BuscaPersonaButtonClick(
  Sender: TObject);
var
	f: TFormBuscaClientes;
begin
	Application.CreateForm(TFormBuscaClientes , f);
(*
	if f.Inicializa('','', '', '', '', TBP_PERSONAS_NATURALES) then begin
		if (f.ShowModal = mrok) then begin
			CodigoPersona 	    := f.Persona.CodigoPersona;
			BuscaPersona.Text   := Trim(f.Persona.Apellido) + ' ' + Trim(f.Persona.ApellidoMaterno) + ', ' + Trim(f.Persona.Nombre);
		end;
	end;
*)
	f.Release;
end;

procedure TfrmSeleccionarRolYPersona.FormCreate(Sender: TObject);
begin
  Iniciar
end;

procedure TfrmSeleccionarRolYPersona.Iniciar;
Var
	i: Integer;
begin
	CodigoPersona := 0;
	CodigoRol := 0;

	try
		qry_Roles.Open;
		cb_Roles.Clear;
		i := -1;
		While not qry_Roles.Eof do
		begin
			cb_Roles.Items.Add(Copy (qry_Roles.FieldByName('Descripcion').AsString + Space(200), 1, 200) + Trim(qry_Roles.FieldByName('CodigoRol').AsString));
			qry_Roles.Next;
		end;
		cb_Roles.ItemIndex := i;
	finally
		qry_Roles.Close
	end
end;

procedure TfrmSeleccionarRolYPersona.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	If cb_Roles.ItemIndex <> -1 then CodigoRol := StrToInt (Trim (Copy(cb_Roles.Text, 201, 10)))
end;

end.
