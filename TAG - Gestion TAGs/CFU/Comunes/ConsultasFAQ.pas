{********************************** File Header ********************************
File Name   : ArbolFAQ
Author      : Castro, Ra�l <rcastro@dpsautomation.com>
Date Created: 08-Mar-2004
Language    : ES-AR
Description : Arbol FAQ
*******************************************************************************}

unit ArbolFAQ;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, DB, ADODB, Grids, DBGrids, DBCtrls, Mask, UtilProc,
  ExtCtrls, Buttons, DPSControls, DmiCtrls, RStrings, ListBoxEx, DBListEx, utildb, PeaProcs,
  Menus, DPSGrid;

type
  TCodigos = Class(TObject)
  private
	FCodigoTitulo: Integer;
	FCodigoSubTitulo: Integer;
  public
	property CodigoTitulo: Integer read FCodigoTitulo write FCodigoTitulo;
	property CodigosubTitulo: Integer read FCodigosubTitulo write FCodigosubTitulo;
  end;

  TfrmArbolFAQ = class(TForm)
	dsTitulosFAQ: TDataSource;
	dsPreguntasFAQ: TDataSource;
    splArbol: TSplitter;
    Timer: TTimer;
    ActualizarEstadisticaFAQ: TADOStoredProc;
	ObtenerTitulosFAQ: TADOStoredProc;
    ObtenerSubTitulosFAQ: TADOStoredProc;
    ObtenerPreguntasFAQTitulo: TADOStoredProc;
    panDerecho: TPanel;
    panPyR: TPanel;
	Splitter1: TSplitter;
	panRespuesta: TPanel;
    Respuesta: TDBRichEdit;
	panPreguntas: TPanel;
    Preguntas: TDBLookupListBox;
    qryPreguntas: TADOQuery;
    dsPreguntas: TDataSource;
	dsTitulos: TDataSource;
    qryTitulos: TADOQuery;
    dsSubTitulos: TDataSource;
    qrySubTitulos: TADOQuery;
    qryExistenTSP: TADOQuery;
    qryEliminarTSP: TADOQuery;
    qryAgregarTSP: TADOQuery;
    panArbol: TPanel;
    ArbolFAQ: TTreeView;
    Splitter3: TSplitter;
    panListaPreguntas: TPanel;
    dblPreguntas: TDBListEx;
    DBListEx2: TDBListEx;
    panABM: TPanel;
    btnABMSubTitulos: TDPSButton;
    btnABMPreguntas: TDPSButton;
    btnABMTitulos: TDPSButton;
    panABArbol: TPanel;
    btnAgregarEnArbol: TDPSButton;
    btnEliminarDeArbol: TDPSButton;
    DPSGrid1: TDPSGrid;
	procedure ArbolFAQClick(Sender: TObject);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	function CrearArbol: Boolean;
	function EliminarArbol: Boolean;
	procedure TimerTimer(Sender: TObject);
	procedure PreguntasEnter(Sender: TObject);
	procedure PreguntasDblClick(Sender: TObject);
	procedure PreguntasKeyDown(Sender: TObject; var Key: Word;
	  Shift: TShiftState);
	procedure dblPreguntasDrawText(Sender: TCustomDBListEx;
	  Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
	  var Text: String; var TxtRect: TRect; var ItemWidth: Integer;
	  var DefaultDraw: Boolean);
    procedure btnABMTitulosClick(Sender: TObject);
    procedure btnABMSubTitulosClick(Sender: TObject);
    procedure btnABMPreguntasClick(Sender: TObject);
    procedure btnEliminarDeArbolClick(Sender: TObject);
    procedure btnAgregarEnArbolClick(Sender: TObject);
    procedure ArbolFAQDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure ArbolFAQDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
  private
	{ Private declarations }
  public
	{ Public declarations }
	function Inicializar(MDIChild: Boolean; Edicion: Boolean = False): Boolean;
  end;

var
  frmArbolFAQ: TfrmArbolFAQ;

implementation

uses DMConnection, ABMAcciones, ABMPreguntasFAQ, ABMSubTitulosFAQ,
  ABMTitulosFAQ;

{$R *.dfm}

function TfrmArbolFAQ.CrearArbol: Boolean;
var
	SubNodo: TTreeNode;
	Codigos : TCodigos;

	procedure ArmarSubArbol (CodigoTitulo: Integer);
	begin
		With ObtenerSubTitulosFAQ Do Begin
			Close;
			Parameters.ParamByName ('@CodigoTitulo').Value := CodigoTitulo;
			Open;

			While Not EoF Do Begin
				Codigos := TCodigos.Create;

				Codigos.CodigoTitulo := CodigoTitulo;
				Codigos.CodigosubTitulo := FieldByName('CodigosubTitulo').AsInteger;
				ArbolFAQ.Items.AddChildObject(SubNodo, Trim(FieldByName('SubTitulo').AsString), Codigos);

				Next;
			End;

			Close;
		End;
	end;

begin
	ArbolFAQ.Items.Clear;
	ObtenerPreguntasFAQTitulo.Close;

	With ObtenerTitulosFAQ Do Begin
		Close;
		Open;

		While Not EoF Do Begin
			SubNodo := ArbolFAQ.Items.Add(nil, Trim(FieldByName('Titulo').AsString));

			ArmarSubArbol (FieldByName('CodigoTitulo').AsInteger);

			Next
		End;

		Close
	End;

	Result := ArbolFAQ.Items.GetFirstNode <> nil;
	ArbolFAQ.FullExpand
end;

function TfrmArbolFAQ.Inicializar(MDIChild: Boolean; Edicion: Boolean = False): Boolean;
var
	S: TSize;
begin
	result := False;

	if MDIChild then begin
		S := GetFormClientSize(Application.MainForm);
		SetBounds(0, 0, S.cx, S.cy);
	end else begin
		FormStyle := fsNormal;
		Visible := False;
	end;

	if not OpenTables([qryTitulos,qrySubTitulos,qryPreguntas]) then exit;

	panRespuesta.Visible := False;
	Timer.Enabled := not Edicion;

	panListaPreguntas.Visible := Edicion;

	panABArbol.Visible := Edicion;
	splArbol.Visible := Edicion;
	Splitter1.Visible := False;
	
	result := CrearArbol;
	if not result then
		MsgBox(MSG_ERROR_NO_HAY_FAQ, self.Caption);
end;

procedure TfrmArbolFAQ.ArbolFAQClick(Sender: TObject);
begin
	panRespuesta.Visible := False;
	ObtenerPreguntasFAQTitulo.Close;

	if Assigned(ArbolFAQ.Selected) and (ArbolFAQ.Selected.Level <> 0) then begin
		ObtenerPreguntasFAQTitulo.Parameters.ParamByName('@CodigoTitulo').value :=  TCodigos(ArbolFAQ.Selected.data).CodigoTitulo;
		ObtenerPreguntasFAQTitulo.Parameters.ParamByName('@CodigosubTitulo').value := TCodigos(ArbolFAQ.Selected.data).CodigosubTitulo;
		ObtenerPreguntasFAQTitulo.Parameters.ParamByName('@UsoWEB').value := false;
		ObtenerPreguntasFAQTitulo.Open;

		PanPyR.SetFocus;
		Preguntas.SetFocus
	end;
end;

procedure TfrmArbolFAQ.FormClose(Sender: TObject; var Action: TCloseAction);

begin
	EliminarArbol;

	qryPreguntas.Close;
	qryTitulos.Close;
	qrySubTitulos.Close;

	action := caFree;
end;

function TfrmArbolFAQ.EliminarArbol: Boolean;
var
	ItemArbol: TTreeNode;
begin
	// Primer nodo del �rbol
	ItemArbol := ArbolFAQ.Items.GetFirstNode;

	// Recorro hasta el final
	while ItemArbol <> nil do begin
		// Es nodo con info, libero la memoria asociada a ella
		if ItemArbol.Data <> nil then TCodigos(ItemArbol.data).Free;

		// Pr�ximo nodo
		ItemArbol := ItemArbol.GetNext
	end;

	Result := True
end;

procedure TfrmArbolFAQ.TimerTimer(Sender: TObject);
begin
	EliminarArbol;
	CrearArbol;
end;

procedure TfrmArbolFAQ.PreguntasEnter(Sender: TObject);
begin
	panRespuesta.Visible := False;
	Splitter1.Visible := False;
end;

procedure TfrmArbolFAQ.PreguntasDblClick(Sender: TObject);
begin
	panRespuesta.Visible := True;
	Splitter1.Visible := True;

	if not panListaPreguntas.Enabled then begin
		With ActualizarEstadisticaFAQ, Parameters Do Begin
			ParamByName ('@CodigoPregunta').Value := ObtenerPreguntasFAQTitulo.FieldByName ('CodigoPregunta').AsInteger;
			ExecProc
		End
	end
end;

procedure TfrmArbolFAQ.PreguntasKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if key = VK_RETURN then begin
        PreguntasDblClick(Preguntas);
	end;
end;

procedure TfrmArbolFAQ.dblPreguntasDrawText(Sender: TCustomDBListEx;
  Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
  var Text: String; var TxtRect: TRect; var ItemWidth: Integer;
  var DefaultDraw: Boolean);
begin
	if Sender.DataSource.DataSet.FieldByName('Cantidad').AsInteger = 0 then Sender.Canvas.Font.Color := clRed;
end;

procedure TfrmArbolFAQ.btnABMTitulosClick(Sender: TObject);
var
	f: TfrmABMTitulosFAQ;
begin
	qryTitulos.Close;

	if FindFormOrCreate(TfrmABMTitulosFAQ, f) then
		f.Show
	else begin
		if not f.Inicializar(True) then f.Release;
	end;

	qryTitulos.Open;

	EliminarArbol;
	CrearArbol;
end;

procedure TfrmArbolFAQ.btnABMSubTitulosClick(Sender: TObject);
var
	f: TfrmABMSubTitulosFAQ;
begin
	if FindFormOrCreate(TfrmABMSubTitulosFAQ, f) then
		f.Show
	else begin
		if not f.Inicializar(True) then f.Release;
	end;

end;

procedure TfrmArbolFAQ.btnABMPreguntasClick(Sender: TObject);
var
	f: TfrmABMPreguntasFAQ;
begin
	if FindFormOrCreate(TfrmABMPreguntasFAQ, f) then
		f.Show
	else begin
		if not f.Inicializar(True) then f.Release;
	end;
end;

procedure TfrmArbolFAQ.btnEliminarDeArbolClick(Sender: TObject);
begin
	if Not (Assigned(ArbolFAQ.Selected) and (ArbolFAQ.Selected.Level <> 0)) then exit;

	Screen.Cursor := crHourGlass;

	If MsgBox(Format(MSG_QUESTION_ELIMINAR,[STR_ARBOL]), STR_CONFIRMACION, MB_YESNO + MB_ICONQUESTION) = IDYES then begin
		try
			with qryEliminarTSP, Parameters do begin
				Close;
				ParamByName('CodigoTitulo').Value	:= TCodigos(ArbolFAQ.Selected.data).CodigoTitulo;
				ParamByName('CodigoSubtitulo').Value:= TCodigos(ArbolFAQ.Selected.data).CodigosubTitulo;
				ParamByName('CodigoPregunta').Value	:= ObtenerPreguntasFAQTitulo.FieldByName ('CodigoPregunta').AsInteger;
				ExecSQL;
			end;

			EliminarArbol;
			CrearArbol;
		Except
			On E: Exception do begin
				MsgBoxErr(Format(MSG_ERROR_ELIMINAR,[STR_ARBOL]), e.message, Format(MSG_CAPTION_ELIMINAR,[STR_ARBOL]), MB_ICONSTOP);
			end;
		end;
	end;
	
	Screen.Cursor      := crDefault;
end;

procedure TfrmArbolFAQ.btnAgregarEnArbolClick(Sender: TObject);
begin
	// Verifico que no exista la combinaci�n T�tulo-Subt�tulo-Pregunta
	with qryExistenTSP, Parameters do begin
		Close;
		ParamByName('CodigoTitulo').Value	:= qryTitulos.FieldByName ('CodigoTitulo').AsInteger;
		ParamByName('CodigoSubtitulo').Value:= qrySubTitulos.FieldByName ('CodigoSubTitulo').AsInteger;;
		ParamByName('CodigoPregunta').Value	:= qryPreguntas.FieldByName ('CodigoPregunta').AsInteger;
		Open;

		If RecordCount <> 0 then begin
			MsgBox(Format(MSG_ERROR_DUPLICACION,[FLD_TITULO+'/'+FLD_SUBTITULO+'/'+FLD_PREGUNTA]), Format(MSG_CAPTION_AGREGAR,[STR_ARBOL]), MB_OK + MB_ICONQUESTION);
			Close;

			Exit
		end;

		Close
	end;

	Screen.Cursor := crHourGlass;

	try
		with qryAgregarTSP, Parameters do begin
			Close;
			ParamByName('CodigoTitulo').Value	:= qryTitulos.FieldByName ('CodigoTitulo').AsInteger;
			ParamByName('CodigoSubtitulo').Value:= qrySubTitulos.FieldByName ('CodigoSubTitulo').AsInteger;;
			ParamByName('CodigoPregunta').Value	:= qryPreguntas.FieldByName ('CodigoPregunta').AsInteger;;
			ExecSQL
		end;

		EliminarArbol;
		CrearArbol;
	except
		On E: EDataBaseError do begin
			MsgBoxErr(Format(MSG_ERROR_ACTUALIZAR,[STR_ARBOL]), E.message, Format (MSG_CAPTION_ACTUALIZAR,[STR_ARBOL]), MB_ICONSTOP);
		end;
	end;

	Screen.Cursor := crDefault;
end;

procedure TfrmArbolFAQ.ArbolFAQDragDrop(Sender, Source: TObject; X,
  Y: Integer);
begin
  if (Sender is TTreeView) and (Source is TDBListEx) then btnAgregarEnArbolClick (Sender)
end;

procedure TfrmArbolFAQ.ArbolFAQDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
	Accept := Source is TDBListEx
end;

end.

