object FrameDatosPersonaConsulta: TFrameDatosPersonaConsulta
  Left = 0
  Top = 0
  Width = 554
  Height = 71
  TabOrder = 0
  object lblDescNombre: TLabel
    Left = 220
    Top = 8
    Width = 158
    Height = 13
    Caption = 'JUAN MANUEL PEREZ GARCIA'
  end
  object lblEmail: TLabel
    Left = 331
    Top = 51
    Width = 211
    Height = 13
    AutoSize = False
    Caption = 'juanperez@hotmail.com'
  end
  object lblRutRun: TLabel
    Left = 9
    Top = 8
    Width = 31
    Height = 13
    Caption = 'RUT:'
    FocusControl = txtDocumento
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblNombrePersoneria: TLabel
    Left = 161
    Top = 8
    Width = 48
    Height = 13
    Caption = 'Nombre:'
    FocusControl = txtDocumento
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label1: TLabel
    Left = 9
    Top = 31
    Width = 56
    Height = 13
    Caption = 'Domicilio:'
    FocusControl = txtDocumento
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 9
    Top = 51
    Width = 107
    Height = 13
    Caption = 'Tel'#233'fono principal:'
    FocusControl = txtDocumento
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 290
    Top = 51
    Width = 35
    Height = 13
    Caption = 'Email:'
    FocusControl = txtDocumento
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblDescTelefono: TLabel
    Left = 118
    Top = 51
    Width = 166
    Height = 13
    AutoSize = False
    Caption = '2  -  6367440  - Particular'
  end
  object lblDescDomicilio: TLabel
    Left = 77
    Top = 31
    Width = 168
    Height = 13
    Caption = 'LUIS PASTEUR 4950 - VITACURA'
  end
  object txtDocumento: TEdit
    Left = 45
    Top = 3
    Width = 105
    Height = 21
    Hint = 'N'#250'mero de documento'
    CharCase = ecUpperCase
    Color = 16444382
    MaxLength = 9
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    OnKeyPress = txtDocumentoKeyPress
  end
  object ADOStoredProc1: TADOStoredProc
    Parameters = <>
    Left = 488
    Top = 24
  end
end
