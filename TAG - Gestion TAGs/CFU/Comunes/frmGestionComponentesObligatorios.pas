unit frmGestionComponentesObligatorios;

{
    Firma       : SS_1147_MCA_20150212
    Descripcion : el boton salir del toolbar no funcionaba, se agrega CLOSE; para salir de la ventana.
}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ADODB, DB, DbList, PeaTypes, Abm_obj, StdCtrls, DPSControls, ExtCtrls,Util,PeaProcs,
  utildb,UtilProc,DMConnection,RStrings;

type
  TFormGestionComponentesObligatorios = class(TForm)
    Panel2: TPanel;
    Notebook: TNotebook;
    AbmToolbar1: TAbmToolbar;
    GroupB: TPanel;
    DBList: TAbmList;
    ComponentesSistemas: TADOTable;
    Label1: TLabel;
    txt_Campo: TEdit;
    cb_Obligatorio: TCheckBox;
    Label3: TLabel;
    txt_Hint: TEdit;
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    cb_Formulario: TComboBox;
    BtnSalir: TButton;
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    procedure DBListDrawItem(Sender: TDBList; Tabla: TDataSet; Rect: TRect;
      State: TOwnerDrawState; Cols: TColPositions);
    procedure BtnSalirClick(Sender: TObject);
    procedure DBListClick(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DBListEdit(Sender: TObject);
    procedure cb_FormularioClick(Sender: TObject);
    procedure AbmToolbar1Close(Sender: TObject);
  private
    { Private declarations }
    FSistema:Integer;
    function CargarFiltro(Sistema:Integer;Formulario:String;Indice:Integer):Boolean;
    function DarFormulario:String;
    function DarIndice:Integer;
    procedure Volver_Campos;
  public
    function inicializa(MDIChild: Boolean; Sistema:Integer):Boolean;
  end;

var
  FormGestionComponentesObligatorios: TFormGestionComponentesObligatorios;

implementation

{$R *.dfm}

procedure TFormGestionComponentesObligatorios.DBListDrawItem(
  Sender: TDBList; Tabla: TDataSet; Rect: TRect; State: TOwnerDrawState;
  Cols: TColPositions);
begin
	With Sender.Canvas  do begin
		FillRect(Rect);
		TextOut(Cols[0], Rect.Top, Tabla.FieldByName('DescripComponente').AsString);
        if Tabla.FieldByName('Obligatorio').IsNull then TextOut(Cols[1], Rect.Top,'')
        else TextOut(Cols[1], Rect.Top, iif(Tabla.FieldByName('Obligatorio').AsBoolean,MSG_SI,MSG_NO));
        TextOut(Cols[2], Rect.Top, Tabla.FieldByName('Hint').AsString);
	end;

end;

procedure TFormGestionComponentesObligatorios.BtnSalirClick(
  Sender: TObject);
begin
    close;
end;

function TFormGestionComponentesObligatorios.inicializa(MDIChild: Boolean; Sistema:Integer):Boolean;
var
    S: TSize;
begin
    if MDIChild then begin
        S := GetFormClientSize(Application.MainForm);
        SetBounds(0, 0, S.cx, S.cy);
    end else begin
        FormStyle := fsNormal;
        Visible := False;
    end;

    result:=False;
    Volver_Campos;
    FSistema:=Sistema;
    CargarFormularios(DMConnections.BaseCAC,cb_Formulario);
    if not(CargarFiltro(FSistema,DarFormulario,DarIndice)) then exit;
    result:=True;
end;

procedure TFormGestionComponentesObligatorios.Volver_Campos;
begin
    Notebook.PageIndex:=0;
    DBList.Enabled:=True;
    txt_Campo.Clear;
    txt_Hint.Clear;
    cb_Obligatorio.Checked:=False;
    GroupB.Enabled:=False;
    DBList.Estado := Normal;
    DBList.SetFocus;
end;

procedure TFormGestionComponentesObligatorios.DBListClick(Sender: TObject);
begin
    with ComponentesSistemas do begin
        txt_Campo.Text:=fieldbyname('DescripComponente').AsString;
        cb_Obligatorio.Checked:=fieldbyname('Obligatorio').AsBoolean;
        cb_Obligatorio.Enabled:=not(fieldbyname('Obligatorio').IsNull);
        txt_Hint.Text:=fieldbyname('Hint').AsString;
    end;
end;

procedure TFormGestionComponentesObligatorios.BtnCancelarClick(
  Sender: TObject);
begin
    Volver_Campos;
end;

procedure TFormGestionComponentesObligatorios.AbmToolbar1Close(Sender: TObject);
begin
    Close;                 //SS_1147_MCA_20150212
end;

procedure TFormGestionComponentesObligatorios.BtnAceptarClick(
  Sender: TObject);
begin
    with ComponentesSistemas do begin
        edit;
        if cb_Obligatorio.Enabled then fieldbyname('Obligatorio').AsBoolean:=cb_Obligatorio.Checked;
        fieldbyname('Hint').AsString:=trim(txt_Hint.Text);
        post;
    end;
    DBList.Reload;
    Volver_Campos;
end;

procedure TFormGestionComponentesObligatorios.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TFormGestionComponentesObligatorios.DBListEdit(Sender: TObject);
begin
    GroupB.Enabled:=true;
    Notebook.PageIndex:=1;
    DBList.Enabled:=not(GroupB.Enabled);
    txt_Hint.SetFocus;
end;

function TFormGestionComponentesObligatorios.CargarFiltro(Sistema: Integer;
  Formulario:String; Indice: Integer): Boolean;
begin
    result:=true;
    ComponentesSistemas.Filter:=format('CodigoSistema=%d and Formulario=''%s'' and Indice=%d',[Sistema,DarFormulario,Indice]);
    if not(OpenTables([ComponentesSistemas])) then result:=False;
    DBList.Reload;
end;

function TFormGestionComponentesObligatorios.DarFormulario: String;
begin
    result:=trim(strleft(StrRight(cb_Formulario.Text,33),30));
end;

procedure TFormGestionComponentesObligatorios.cb_FormularioClick(
  Sender: TObject);
begin
    if not(CargarFiltro(FSistema,DarFormulario,DarIndice)) then begin
        MsgBox(format(MSG_ERROR_ACTUALIZAR,[FLD_FORMULARIO]),FLD_FORMULARIO,MB_ICONSTOP);
        exit;
    end;
    DBList.Reload;
end;

function TFormGestionComponentesObligatorios.DarIndice: Integer;
begin
    result:=strtoint(trim(StrRight(cb_Formulario.Text,3)));
end;

end.
