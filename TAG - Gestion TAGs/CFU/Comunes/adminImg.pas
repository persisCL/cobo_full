{********************************** File Header ********************************
File Name : adminImg.pas
Author :
Date Created:
Language : ES-AR
Description :

Revision: 1
Author : jconcheyro
Date Created: 05/10/2006
Language : ES-AR
Description : En ObtenerPosicionAGrabarPatente y AlmacenarImagenIMGPatente agrego
    informaci�n a los mensajes de error para conocer los archivos involucrados

Revision : 3
    Author : ggomez
    Date : 14/11/2006
    Description : Modifiqu� la cantidad de patentes que puede almacenar un
        archivo de patentes, ahora permite almacenar diez mil patentes. Lo que
        cambi� es la constante CANT_PATENTES ahora es 10000 (diez mil) y antes
        era 1000 (mil).
        Este cambio se hizo pues existen archivos que ya contienen mil patentes
        y ocurre un error cuando se intenta agregar una patente nueva, pues ya
        no hay lugar.

Revision    : 4
Author      : Nelson Droguett Sierra
Date        : 7-Junio-2011
Description :
           - Se a�adi� la funci�n AlmacenarImagenIMGPatenteNuevoFormato para almacenar
        las Imagenes de Patentes en Archivos Individuales. Se mantuvo la misma estructura
        en los par�metros que su predecesora AlmacenarImagenIMGPatente.
           - Se a�adi� la funci�n ObtenerImagenIMGPatenteNuevoFormato para recuperar las
        Imagenes de Patentes de los Archivos Individuales. Se mantuvo la misma estructura
        en los par�metros que su predecesora ObtenerImagenIMGPatente
           - Se a�adi� la funci�n AlmacenarImagenIMGTransitoNuevoFormato para almacenar
        las Im�genes de los Tr�nsitos en Archivos Individuales. Se mantuvo la misma estructura
        en los par�metros que su predecesora AlmacenarImagenIMGTransito.
           - Se a�adi� la funci�n ObtenerImagenIMGTransitoNuevoFormato para recuperar
        las Im�genes de los Tr�nsitos de los Archivos Individuales. Se mantuvo la misma
        estructura en los par�metros que su predecesora ObtenerImagenIMGTransito.
                Se crearon dos funciones con la directiva overload, una para obtener las im�genes a trav�s de la clase
        TJpegPlusImage y la otra para la clase TBitmap.
            Se modific� la estructura de la funci�n CargarImagenAlmacenada para
        eliminar dos raises que bloqueaban la liberaci�n de recursos.
Firma       : SS-377-NDR-20110607

Fecha       :   03-10-2013
Autor       :   CQuezadaI
Firma       :   SS_1091_CQU_20131001
Descripcion :   Se crea sobrecarga (overload) a la funci�n ObtenerImagenIMGPatenteNuevoFormato
                para poder recibir como par�metro un objeto TBitmap y procesar las im�genes italinas
                Se corrige el metodo ObtenerImagenIMGPatenteNuevoFormato para que cuando
                ejecute el m�todo ObtenerImagen y �ste devuelva True, se limpie la variable DescriError
                y se devuelta Result := True ya que como est� implementado, nunca limpia dicha variable.

Fecha       :   01-06-2015
Autor       :   CQuezadaI
Firma       :   SS_1288_CQU_20150601
Descripcion :   Se replica el codigo de la SS_1261_CQU_20150506 agregando validaci�n al tama�o del archivo
*******************************************************************************}
unit adminImg;

interface

uses
  	windows, ImgTypes, SysUtils, Jpeg, JpegPlus, util, utilImg, Graphics;

// Funciones para administrar la estructura de Almacenamiento de Imagenes de Patentes
function AlmacenarImagenIMGPatente(SourceFile, DestFile: AnsiString; Patente: AnsiString; TipoImg: TTipoImagen;
  var DescriError: AnsiString): Boolean;
function AlmacenarImagenIMGPatenteNuevoFormato(SourceFile, DestFile: AnsiString; Patente: AnsiString; TipoImg: TTipoImagen;                //SS-377-NDR-20110607
  var DescriError: AnsiString): Boolean;                                                                                                   //SS-377-NDR-20110607

function ObtenerImagenIMGPatente(FileName: AnsiString; Patente: AnsiString; TipoImg: TTipoImagen;
  var Imagen: TJPEGPLusIMage; var DataIMage: TDataIMage; var DescriError: AnsiString;
  var tipoError: TTipoErrorImg): Boolean;

function ObtenerImagenIMGPatenteNuevoFormato(FileName: AnsiString; Patente: AnsiString; TipoImg: TTipoImagen;                              //SS-377-NDR-20110607
  var Imagen: TJPEGPLusIMage; var DataIMage: TDataIMage; var DescriError: AnsiString;                                                      //SS-377-NDR-20110607
  var tipoError: TTipoErrorImg): Boolean; overload;                                                                                        // SS_660_CQU_20131001   //SS-377-NDR-20110607

function ObtenerImagenIMGPatenteNuevoFormato(FileName: AnsiString; Patente: AnsiString; TipoImg: TTipoImagen;                              // SS_660_CQU_20131001
  var Imagen: TBitmap; var DataIMage: TDataIMage; var DescriError: AnsiString; var tipoError: TTipoErrorImg): Boolean; overload;           // SS_660_CQU_20131001                                                                                                  // SS_660_CQU_20131001

// Funciones para administrar la estructura de Almacenamiento de Imagenes de Tr�nsitos
function ObtenerImagenIMGTransito(FileName: AnsiString;
  NumCorrCA: Int64;
  TipoImg: TTipoImagen; var Imagen: TJPEGPlusImage; var DataImage: TDataIMage;
  var DescriError: AnsiString;
  var tipoError: TTipoErrorImg): Boolean; overload;

function ObtenerImagenIMGTransito(FileName: AnsiString;
  NumCorrCA: Int64;
  TipoImg: TTipoImagen; var Imagen: TBitmap; var DataImage: TDataIMage;
  var DescriError: AnsiString;
  var tipoError: TTipoErrorImg): Boolean; overload;

function ObtenerImagenIMGTransitoNuevoFormato(FileName: AnsiString; NumCorrCA: Int64; TipoImg: TTipoImagen; var Imagen: TJPEGPlusImage;    //SS-377-NDR-20110607
   var DataImage: TDataIMage; var DescriError: AnsiString; var tipoError: TTipoErrorImg): Boolean; overload;                               //SS-377-NDR-20110607

function ObtenerImagenIMGTransitoNuevoFormato(FileName: AnsiString; NumCorrCA: Int64; TipoImg: TTipoImagen; var Imagen: TBitmap;           //SS-377-NDR-20110607
   var DataImage: TDataIMage; var DescriError: AnsiString; var tipoError: TTipoErrorImg): Boolean; overload;                               //SS-377-NDR-20110607

function ExisteImagenIMGTransito(FileName: AnsiString; NumCorrCA: Int64; TipoImagen: TTipoImagen; var DescriError: AnsiString): Boolean;

function AlmacenarImagenIMGTransito(SourceFile, DestFile: AnsiString; NumCorrCA: Integer;
  TipoImagen: TTipoImagen; var DescriError: AnsiString): Boolean;

function AlmacenarImagenIMGTransitoNuevoFormato(SourceFile, DestFile: AnsiString; NumCorrCA: Integer;                                      //SS-377-NDR-20110607
  TipoImagen: TTipoImagen; var DescriError: AnsiString): Boolean;                                                                          //SS-377-NDR-20110607


const IMAGENES_POR_TRANSITO		= 7;		// Cantidad de Im�genes por tr�nsito

implementation

const
	FIRMA_VIAJES				= 'Archivo Imagen';
	FIRMA_PATENTE           	= 'Archivo Patente';
	CANT_TRANSITOS 				= 10000;	// Cantidad de Transitos por Archivo (con CANT_IMG_VIAJES c/u)
	CANT_PATENTES				= 10000;	// Cantidad de Patentes por Archivo (con CANT_IMG_PATENTES c/u)
	IMAGENES_POR_PATENTE		= 2;		// Cantidad de Im�genes por Patente


(*    TTipoErrorImg =
        (teFileImageNoExist,
         teFileImageOpen,
         teFileIMGCreate,
         teFileIMGOpen,
         teFileIMGComplete,
         teFileIMGUpdate,
         teFileIMGNoExist,
         teImageNoExist,
	     teFormatFileIMGWrong,
         teGetImageError,
         teObjectImageNil);
*)


type
	// Archivos de Almacenamiento de im�genes
	TItemHeader = record
        TipoImagen: TTipoImagen;
		PosicionInicial: Integer;
		BytesDatos: Integer;
		BytesEstructuraAddicional: Byte;
	end;

	TDirPlatesHeader = packed record
		Patente: String[10];
	end;

	TImagePlateHeader = packed record
		Firma:		  Array[1..16] of Char;
		CantPatentes: Integer;
		TiposImagen:  byte;
		OffsetSgte:	  Integer;
	end;

   	TDirViajesHeader = packed record
		NumCorrCA: Int64;
	end;

	TImageFileHeader = packed record
		Firma:		   Array[1..16] of Char;
		CantTransitos: Integer;
		TiposImagen:   byte;
		OffsetSgte:	   Integer;
	end;

    TArrayDirViajes = Array of TDirViajesHeader;
    TArrayDirPatentes = Array of TDirPlatesHeader;
    TArrayItemHeader = Array of TItemHeader;

function CrearArchivoPadrePatente(FileName: AnsiString; Patente: AnsiString;
  var HFile: Integer; var DescriError: AnsiString): Boolean;
var
	i: integer;
	Dir: AnsiString;
	headerGral: TImagePlateHeader;
	headerDir:  TArrayDirPatentes;
	headerItem: TArrayItemHeader;
begin
	try
		// Si el archivo Existe, llegamos ac� por otro problema.
		if FileExists(FileName) then begin
            raise Exception.Create('El Archivo a crear ya existe.');
        end;
		// Si el Directorio no existe, lo creamos.
		Dir := ExtractFilePath(FileName);
		if not DirectoryExists(Dir) then
			if not ForceDirectories(Dir) then raise Exception.Create('No se pudo crear el directorio.');
		// Creamos el Archivo
		HFile := FileCreate(FileName, fmOpenReadWrite or fmShareExclusive);
		if HFile = -1 then raise Exception.Create('No se pudo crear el archivo.');
		// Insertamos su header vac�o
		FillChar(headerGral, SizeOf(TImagePlateHeader), 0);
		SetLength(headerDir, CANT_PATENTES);
		SetLength(headerItem, IMAGENES_POR_PATENTE * CANT_PATENTES);
		// Calculamos la firma del archivo
		i := 1;
		while i <= length(FIRMA_PATENTE) do begin
			headerGral.Firma[i] := FIRMA_PATENTE[i];
			Inc(i);
		end;
        // Actualizamosel header general
		headerGral.CantPatentes := CANT_PATENTES;
		headerGral.TiposImagen  := IMAGENES_POR_PATENTE;
		headerGral.OffsetSgte   := SizeOf(TImagePlateHeader) + SizeOf(TDirPlatesHeader) * CANT_PATENTES
		  + SizeOf(TItemHeader) * IMAGENES_POR_PATENTE * CANT_PATENTES;
		// Grabamos en el archivo el Header general
		FileSeek(HFile, 0, 0);
		FileWrite(HFile, headerGral, SizeOf(TImagePlateHeader));
		FileWrite(HFile, headerDir[0], (SizeOf(TDirPlatesHeader) * CANT_PATENTES));
		FileWrite(HFile, headerItem[0], (SizeOf(TItemHeader) * IMAGENES_POR_PATENTE * CANT_PATENTES));
		//
		Result := True;
	except
		on e: Exception do begin
			DescriError := e.Message;
			Result := False;
		end;
	end;
end;


function ObtenerPosicionAGrabarPatente(HFilePadre: Integer; ArchivoTransito: AnsiString; Patente: AnsiString; TipoImagen: TTipoImagen;
  var FGralPatentePadre: TImagePlateHeader; var FDirPatentePadre: TArrayDirPatentes;
  var ItemsPadre: TArrayItemHeader; var IndItemHeader: Integer;
  var NumArchivo: Integer; var OffsetItem: Integer;
  var OffsetGrabar: Integer; var DescriError: AnsiString): Boolean;
	//
    {******************************** Function Header ******************************
    Function Name: Buscar
    Author :
    Date Created :
    Description :
    Parameters : Patente: AnsiString; var NumArchivo: Integer; var OffsetItem: Integer
    Return Value : Boolean

    Revision : 1
        Author : ggomez
        Date : 14/11/2006
        Description : Cambi� la condici�n del while para que use la cantidad de
            patentes que el archivo almacena, en lugar de usar la constante
            que se usa en la creaci�n de un archivo de iamgenes de patentes.
            Este cambio se hizo para que la b�squeda de una patente se haga
            recorriendo la cantidad de patentes que indica el archivo sobre el
            cual se busca. Este cambio es necesario pues al cambiar la cantidad
            de patentes que almacenar�n los archivos nuevos, debe seguir
            funcionando para los archivos que ya est�n creados.
    Revision : 2
        Author : vpaszkowicz
        Date : 19/05/2008
        Description : Cambio los procedimientos FileRead por funciones, ya que el
        bug del archivo padre lleno parece estar porque lee la cabecera y le da que
        el tama�o m�ximo del archivo es cero.
    *******************************************************************************}
	function Buscar(Patente: AnsiString; var NumArchivo: Integer; var OffsetItem: Integer): Boolean;
	var
		i: Integer;
	begin
		Result := False;
		i := 0;
		while i < FGralPatentePadre.CantPatentes do begin

			if (Trim(FDirPatentePadre[i].Patente) = Trim(Patente)) or (length(FDirPatentePadre[i].Patente) = 0) then begin
				// El archivo ya tiene registro � Llegamos al ultimo N� de tr�nsito guardado.
				// Buscar que posicion tiene.
				OffsetItem := SizeOf(TImagePlateHeader)
				  + (SizeOf(TDirPlatesHeader) * FGralPatentePadre.CantPatentes)
				  + (SizeOf(TItemHeader) * i * FGralPatentePadre.TiposImagen);
				NumArchivo := i;
				Result := True;
				Break;
			end;
			Inc(i);
		end;
	end;

    // Revision 4
    {******************************** Function Header ******************************
    Function Name: LlenarArrayErrores
    Author : FSandi
    Date Created : 21/12/2006
    Description : Almacena todo el arreglo de valores actual de las patentes, dentro de un
                archivo .txt para poder verificar los datos.
    Parameters : var Error: AnsiString; var Archivo: Ansistring
    Return Value : Boolean
    *******************************************************************************}
    function LlenarArrayErrores (var Error:AnsiString; var Archivo:Ansistring): Boolean;
	var
		i: Integer;
        F: TextFile;
	begin
        Archivo := EmptyStr;
		try
            i := 0;
            Archivo := 'LogError ' + FormatDateTime ('-yyyy-mm-dd--hh-nn-ss-zzz', Now) + '.txt';
            AssignFile (F, Archivo);
            try
                Rewrite(F);
                WriteLn(F, 'Patente que ocasion� el error: ' + Patente);
                while i < FGralPatentePadre.CantPatentes do begin
                    WriteLn(F, FDirPatentePadre[i].Patente);
                    Inc(i);
                end;
            finally
                CloseFile(F);
            end;
            Result := True;
        except
    		on e: Exception do begin
	    		Error := e.Message;
		    	Result := False;
    		end;
	    end;
	end;
    //Fin de Revision 4
resourceString
    MSG_ERROR_LEYENDO_HEADER = 'No se puede leer la cabecera del archivo. El archivo padre de patentes no est� disponible en este momento.';
    MSG_ERROR_LEYENDO_LISTA_PATENTES = 'No se puede obtener las patentes contenidas en el archivo. El archivo padre de patentes no est� disponible en este momento.';
    MSG_ERROR_LEYENDO_DATO_PATENTE = 'No se puede obtener la patentes. El archivo padre de patentes no est� disponible en este momento.';
var
	i, H, Tamanio: Integer;
    Error, Archivo : AnsiString;
    NroBytesALeer, NroBytesLeidos: longint;
begin
	try
		// Cargamos el general
		FillChar(FGralPatentePadre, SizeOf(FGralPatentePadre), 0);
		FileSeek(HFilePadre, 0, 0);
        //Ac� controlo el tama�o del registro en si, ya que no puede estar vacio.
        NroBytesALeer := FileRead(HFilePadre, FGralPatentePadre, SizeOf(TImagePlateHeader));
		if  NroBytesALeer <> SizeOf(TImagePlateHeader) then begin
            FileClose(HFilePadre);
            raise exception.Create(MSG_ERROR_LEYENDO_HEADER)
        end;
		// Cargamos solo el Directorio de Patentes.
		SetLength(FDirPatentePadre, FGralPatentePadre.CantPatentes);
		FileSeek(HFilePadre, SizeOf(TImagePlateHeader), 0);
        NroBytesALeer := (FGralPatentePadre.CantPatentes * SizeOf(TDirPlatesHeader));
        NroBytesLeidos := FileRead(HFilePadre, FDirPatentePadre[0], NroBytesALeer);
		if  NroBytesLeidos <> NroBytesALeer then begin
            FileClose(HFilePadre);
            raise exception.Create(MSG_ERROR_LEYENDO_LISTA_PATENTES)
        end;
		// Buscamos la posici�n que le corresponde al item de esa Patente
		if Buscar(Patente, NumArchivo, OffsetItem) then begin
			// Leemos los items
			FileSeek(HFilePadre, OffsetItem, 0);
          	SetLength(ItemsPadre, SizeOf(TItemHeader) * FGralPatentePadre.TiposImagen);
            NroBytesALeer := SizeOf(TItemHeader) * FGralPatentePadre.TiposImagen;
            NroBytesLeidos := FileRead(HFilePadre, ItemsPadre[0], NroBytesALeer);
			if  NroBytesALeer <> NroBytesLeidos then begin
                FileClose(HFilePadre);
                raise exception.Create(MSG_ERROR_LEYENDO_DATO_PATENTE);
            end;
            // Buscar si el tipo de imagen ya esta almacenado o en caso contrario el primer lugar libre
            i := 0;
            while (i < FGralPatentePadre.TiposImagen) and (ItemsPadre[i].TipoImagen <> TipoImagen)
              and (ItemsPadre[i].PosicionInicial <> 0) do begin
                inc(i);
            end;
            // Si se recorre toda la estructura sin encontrar lugar retornamos un error
            if i = FGralPatentePadre.TiposImagen then begin
                raise exception.Create('No se puede insertar el archivo. No existe lugar para un nuevo tipo de imagen.');
            end;
            IndItemHeader := i;
			// Si ya existe lugar, podr�a ocurrir que una imagen de ese tipo ya este almacenada
			if ItemsPadre[i].PosicionInicial <> 0 then begin
				// Ya existe la imagen de Referencia para = �tem.
				H := FileOpenRetry(ArchivoTransito, fmOpenRead or fmShareDenyWrite);
				if H = -1 then
                    raise exception.Create('Error al abrir archivo de la imagen.');
				Tamanio := FileSeek(H, 0, 2);
				FileCLose(H);
				// El nuevo tiene tama�o <= actual
				if Tamanio <= (ItemsPadre[i].BytesDatos + BYTES_EXTRAS_VR) then
                    OffsetGrabar := ItemsPadre[i].PosicionInicial
    			else
                    OffsetGrabar := FGralPatentePadre.OffsetSgte;
			end else begin
				// Es una Patente sin imagen de referencia, Grabar al final del archivo Padre.
				OffsetGrabar := FGralPatentePadre.OffsetSgte;
			end;
		end else begin
		    // Ya no hay lugar para el archivo, se super� la cantidad
//Fin de Revision 4
            if LlenarArrayErrores(Error, Archivo) then begin
                Error := ' .Se creo el archivo de error:' + Archivo;
            end else begin
                Error := ' .No se pudo crear el archivo de error:' + Archivo + 'Error: ' + Error ;
            end;
//Fin de Revision 4
  		    raise exception.Create('No se puede insertar Patente '+ Patente + ' en el archivo. Archivo Padre lleno.'
                 + CRLF + 'Cantidad Pat. Arch.: ' + IntToStr(FGralPatentePadre.CantPatentes) + Error);

		end;
		//
		Result := True;
	except
		on e:exception do begin
			DescriError := e.Message;
			Result := False;
		end;
	end;
end;


function InsertarArchivo(ArchivoTransito: AnsiString; HFilePadre: Integer; offsetSgte: Integer; TipoImagen: TTipoImagen;
  var ItemsHeader: TArrayItemHeader; IndItemHeader: Integer; var Tamanio: Integer; var DescriError: AnsiString): Boolean;
var
	H: Integer;
	Buffer: PChar;
    BytesEscritos: Int64;
begin
    try
        // Abrimos el archivo
        H := FileOpenRetry(ArchivoTransito, fmOpenRead or fmShareDenyWrite);
        if H = -1 then begin
            raise Exception.Create('Error al abrir archivo.');
        end;
        // Calculamos el tama�o del archivo a almacenar y lo almacenamos
        Tamanio := FileSeek(H, 0, 2);
        FileSeek(H, 0, 0);
        FileSeek(HFilePadre, OffsetSgte, 0);
        Buffer := AllocMem(Tamanio);
        if FileRead(H, Buffer^, Tamanio) < tamanio then begin
            FreeMem(Buffer);
		    FileClose(H);
            raise Exception.Create('Error al leer los datos del archivo fuente.');
        end;
        BytesEscritos := FileWrite(HFilePadre, Buffer^, Tamanio);
        if BytesEscritos <> Tamanio then begin
        	FreeMem(Buffer);
			FileClose(H);
            raise Exception.Create('Error al copiar los datos en el archivo destino.');
        end;
        FreeMem(Buffer);
		FileClose(H);
        // Tenemos el item con todas las imagenes. Actualizar el que insertamos.
        ItemsHeader[IndItemHeader].TipoImagen      := TipoImagen;
        ItemsHeader[IndItemHeader].PosicionInicial := OffsetSgte;
        //
        if TipoImagen in [tiOverview1, tiOverview2, tiOverview3] then begin
            ItemsHeader[IndItemHeader].BytesDatos := Tamanio - BYTES_EXTRAS_OVERVIEW;
            ItemsHeader[IndItemHeader].BytesEstructuraAddicional := 0;
        end else begin
            ItemsHeader[IndItemHeader].BytesDatos := Tamanio - BYTES_EXTRAS_VR;
            ItemsHeader[IndItemHeader].BytesEstructuraAddicional := 0;
        end;
        //
        Result := True;
    except
        on e: exception do begin
            DescriError := e.Message;
            Result := False;
        end;
    end;
end;

function ActualizarHeaderPatentes(HFilePadre: Integer; FGralPatentePadre: TImagePlateHeader;
  FDirPatentePadre: TArrayDirPatentes; ItemsHeader: TArrayItemHeader; NumArchivo: Integer; OffsetItem: Integer): Boolean;
var
	Res, Offset: Integer;
begin
	try
		// Actualizar General del ArchivoPadre
		FileSeek(HFilePadre, 0, 0);
		Res := FileWrite(HFilePadre, FGralPatentePadre, SizeOf(TImagePlateHeader));
		if Res < 0 then raise Exception.Create('Error actualizando el header principal.');

		// Actualizar Header del ArchivoPadre
		Offset := SizeOf(TImagePlateHeader) + (SizeOf(TDirPlatesHeader) * NumArchivo);
		FileSeek(HFilePadre, Offset, 0);
		Res := FileWrite(HFilePadre, FDirPatentePadre[NumArchivo], SizeOf(TDirPlatesHeader));
		if Res < 0 then raise Exception.Create('Error actualizando el directorio de patentes.');

		// Actualizar Item del ArchivoPadre
		FileSeek(HFilePadre, OffsetItem, 0);
		Res := FileWrite(HFilePadre, ItemsHeader[0], SizeOf(TItemHeader) * FGralPatentePadre.TiposImagen);
		if Res < 0 then raise Exception.Create('Error actualizando el header del item.');
		//
		Result := True;
	except
		Result := False;
	end;
end;

function ObtenerPosicionALeerPatente(HFilePadre: Integer; Patente: AnsiString; TipoImg: TTipoImagen;
  var FGralPatentePadre: TImagePlateHeader; var FDirPatentePadre: TArrayDirPatentes;
  var OffsetArchivo: Integer; var BytesALeer: Integer; var DescriError: AnsiString): Boolean;
	//
	function ValidarHeader: Boolean;
	begin
		Result := True;
		if (Trim(FGralPatentePadre.Firma) <> FIRMA_PATENTE) then Result := False;
	end;
	//
	function Buscar(Patente: AnsiString; var NumArchivo: Integer; var OffsetItem: Integer): Boolean;
	var
		i: Integer;
	begin
		Result := False;
		try
			i := 0;
			while i < FGralPatentePadre.CantPatentes do begin
				if (length(FDirPatentePadre[i].Patente) = 0) then begin
					Result := False;
					Break;
				end;
				if (Trim(FDirPatentePadre[i].Patente) = Trim(Patente)) then begin
					// El archivo ya tiene registro � Llegamos al ultimo N� de tr�nsito guardado.
					// Buscar que tiene posicion tiene.
					OffsetItem := SizeOf(TImagePlateHeader)
					  + (SizeOf(TDirPlatesHeader) * FGralPatentePadre.CantPatentes)
					  + (SizeOf(TItemHeader) * i * FGralPatentePadre.TiposImagen);
					NumArchivo := i;
					Result := True;
					Break;
				end;
				Inc(i);
			end;
		except
		end;
	end;

var
	i: Integer;
    OffsetItem, NumArchivo: Integer;
    ItemsHeader: TArrayItemHeader;
begin
	try
		// Obtener header General
		FillChar(FGralPatentePadre, SizeOf(FGralPatentePadre), 0);
		FileSeek(HFilePadre, 0, 0);
		FileRead(HFilePadre, FGralPatentePadre, SizeOf(TImagePlateHeader));
		// Validar los valores de concesionaria y Pto de cobro de los headers
		if not ValidarHeader then
		    raise exception.Create('El header del Archivo no es correcto.');

		// Crear los Arrays din�micos. Obtener Header de Patentes (Dir)
		SetLength(FDirPatentePadre, FGralPatentePadre.CantPatentes);
		FileSeek(HFilePadre, SizeOf(TImagePlateHeader), 0);
		FileRead(HFilePadre, FDirPatentePadre[0], (FGralPatentePadre.CantPatentes * SizeOf(TDirPlatesHeader)));

		// Buscar la Posicion del Item
		if not Buscar(Patente, NumArchivo, OffsetItem) then
		    raise exception.Create('No se encuentra la Patente correspondiente.');

        SetLength(ItemsHeader, SizeOf(TItemHeader) * FGralPatentePadre.TiposImagen);
		FileSeek(HFilePadre, OffsetItem, 0);
		FileRead(HFilePadre, ItemsHeader[0], SizeOf(TItemHeader) * FGralPatentePadre.TiposImagen);
        // Buscar si el tipo de imagen ya esta almacenado o en caso contrario el primer lugar libre
        i := 0;
        while (i < FGralPatentePadre.TiposImagen) and (ItemsHeader[i].TipoImagen <> TipoImg)
          and (ItemsHeader[i].PosicionInicial <> 0) do begin
            inc(i);
        end;
		// Validar si la imagen existe
		if (ItemsHeader[i].PosicionInicial = 0) or (i = FGralPatentePadre.TiposImagen) then
		    raise exception.Create('No se encuentra Imagen.');
		// Calcular la posicion del Archivo
		OffsetArchivo := ItemsHeader[i].PosicionInicial;
		BytesALeer    := ItemsHeader[i].BytesDatos + BYTES_EXTRAS_VR;
		//
		Result := True;
	except
		on e: Exception do begin
			Result := False;
			DescriError := e.message;
		end;
	end;
end;

function CargarImagenAlmacenada(HFilePadre: Integer; OffsetArchivo, BytesALeer: Integer;
  var Imagen: TJPEGPLusImage; var DataImage: TDataImage; var DescriError: AnsiString;
  var tipoError: TTipoErrorImg): Boolean; overload;
var
	BytesLeidos, H: Integer;
	Buffer: PChar;
	ArchiAux: AnsiString;
begin
    Try                                                                                                              //SS-377-NDR-20110607
    	Result  := False;                                                                                            //SS-377-NDR-20110607
        // Creamos un archivo temporal                                                                               //SS-377-NDR-20110607
        ArchiAux := GetTempDir + TempFile + '.j12';                                                                  //SS-377-NDR-20110607
        H := FileCreate(ArchiAux, fmOpenWrite or fmShareDenyWrite);                                                  //SS-377-NDR-20110607
        if H = -1 then begin                                                                                         //SS-377-NDR-20110607
            tipoError := teAbrirArchivo;                                                                             //SS-377-NDR-20110607
            DescriError := DescripcionError[teLeerArchivo];                                                          //SS-377-NDR-20110607
            FileClose(H);                                                                                            //SS-377-NDR-20110607
            FreeMem(Buffer);                                                                                         //SS-377-NDR-20110607
        end                                                                                                          //SS-377-NDR-20110607
        else begin                                                                                                   //SS-377-NDR-20110607
            FileSeek(H, 0, 0);                                                                                       //SS-377-NDR-20110607
            // Leemos la imagen desde el Archivo IMG                                                                 //SS-377-NDR-20110607
            FileSeek(HFilePadre, OffsetArchivo, 0);                                                                  //SS-377-NDR-20110607
            Buffer := AllocMem(BytesALeer);                                                                          //SS-377-NDR-20110607
            BytesLeidos := FileRead(HFilePadre, Buffer^, BytesALeer);                                                //SS-377-NDR-20110607
            if (BytesLeidos <> BytesALeer) or (BytesLeidos < 1000) or (Buffer[6] <> 'J') or (Buffer[7] <> 'F') or    //SS-377-NDR-20110607
                (Buffer[8] <> 'I') or (Buffer[9] <> 'F') then begin                                                  //SS-377-NDR-20110607
                tipoError := teFormatoImagen;                                                                        //SS-377-NDR-20110607
                DescriError := DescripcionError[teFormatoImagen];                                                    //SS-377-NDR-20110607
                FileClose(H);                                                                                        //SS-377-NDR-20110607
                FreeMem(Buffer);                                                                                     //SS-377-NDR-20110607
            end                                                                                                      //SS-377-NDR-20110607
            else begin                                                                                               //SS-377-NDR-20110607
                // Guardamos la imagen cargada en el archivo temporal                                                //SS-377-NDR-20110607
                FileWrite(H, Buffer^, BytesALeer);                                                                   //SS-377-NDR-20110607
                FileClose(H);                                                                                        //SS-377-NDR-20110607
                FreeMem(Buffer);                                                                                     //SS-377-NDR-20110607
                // Retornamos Imagen                                                                                 //SS-377-NDR-20110607
                ObtenerImagen(ArchiAux, Imagen, DataImage, DescriError);                                             //SS-377-NDR-20110607
                // Finalmente eliminamos el archivo temporal                                                         //SS-377-NDR-20110607
                DeleteFile(ArchiAux);                                                                                //SS-377-NDR-20110607
                Result := True;                                                                                      //SS-377-NDR-20110607
            end;                                                                                                     //SS-377-NDR-20110607
        end;                                                                                                         //SS-377-NDR-20110607
    finally                                                                                                          //SS-377-NDR-20110607
    end;                                                                                                             //SS-377-NDR-20110607
end;                                                                                                                 //SS-377-NDR-20110607

function CargarImagenAlmacenada(HFilePadre: Integer; OffsetArchivo, BytesALeer: Integer;
  var Imagen: TBitmap; var DataImage: TDataImage; var DescriError: AnsiString;
  var tipoError: TTipoErrorImg): Boolean; overload;
var
	H, BytesLeidos: Integer;
	Buffer: PChar;
	ArchiAux: AnsiString;
begin
	Result  := False;
    try
        // Creamos un archivo temporal
        ArchiAux := GetTempDir + TempFile + '.jpg';
        H := FileCreate(ArchiAux, fmOpenWrite or fmShareDenyWrite);
        if H = -1 then begin
            tipoError := teLeerArchivo;
            raise exception.Create('Error al abrir archivo.');
        end;
        FileSeek(H, 0, 0);
        // Leemos la imagen desde el Archivo IMG
        FileSeek(HFilePadre, OffsetArchivo, 0);
        Buffer := AllocMem(BytesALeer);
        BytesLeidos := FileRead(HFilePadre, Buffer^, BytesALeer);
        if (BytesLeidos <> BytesALeer) or (BytesLeidos < 1000) or
          (Buffer[6] <> 'J') or (Buffer[7] <> 'F') or (Buffer[8] <> 'I') or (Buffer[9] <> 'F') then begin
            tipoError := teFormatoImagen;
            raise exception.Create('Formato de imagen incorrecto.');
        end;
        // Guardamos la imagen cargada en el archivo temporal
        FileWrite(H, Buffer^, BytesALeer);
        FileClose(H);
        FreeMem(Buffer);
        // Retornamos Imagen
        ObtenerImagen(ArchiAux, Imagen, DataImage, DescriError);
        // Finalmente eliminamos el archivo temporal
        DeleteFile(ArchiAux);
        //
        Result := True;
    except
        on e: exception do begin
            DescriError := e.Message;
        end;
    end;
end;

function ObtenerPosicionALeerTransito(HFilePadre: Integer; NumCorrCA: Int64;
  TipoImg: TTipoImagen; var GralViajesPadre: TImageFileHeader; var DirViajesPadre: TArrayDirViajes;
  var OffsetArchivo: Integer; var BytesALeer: Integer; var DescriError: AnsiString): Boolean;
	//
	function Buscar(NumCorrCA: Int64; var NumArchivo: Integer; var OffsetItem: Integer): Boolean;
	var
		i: Integer;
	begin
		Result := False;
		try
			i := 0;
			while i < GralViajesPadre.CantTransitos do begin
				if (DirViajesPadre[i].NumCorrCA = 0) then begin
					Result := False;
					Break;
				end;
				if (DirViajesPadre[i].NumCorrCA = NumCorrCA) then begin
					// Buscar que tiene posicion tiene.
					OffsetItem := SizeOf(TImageFileHeader)
					  + (SizeOf(TDirViajesHeader) * GralViajesPadre.CantTransitos)
					  + (SizeOf(TItemHeader) * i * GralViajesPadre.TiposImagen);
					NumArchivo := i;
					Result := True;
					Break;
				end;
				Inc(i);
			end;
		except
		end;
	end;

resourceString
    MSG_ERROR_LEYENDO_HEADER = 'No se puede leer la cabecera del archivo. El archivo padre de tr�nsitos no est� disponible en este momento.';
    MSG_ERROR_LEYENDO_LISTA_VIAJES = 'No se puede obtener los tr�nsitos contenidas en el archivo. El archivo padre de tr�nsitos no est� disponible en este momento.';
    MSG_ERROR_LEYENDO_DATO_VIAJE = 'No se puede obtener la imagen del tr�nsito. El archivo padre de tr�nsitos no est� disponible en este momento.';

var
	i, OffsetItem, NumArchivo: Integer;
    ItemsHeader: TArrayItemHeader;
    CantBytesALeer: longInt;
begin
	try
		// Obtener header General
		FillChar(GralViajesPadre, SizeOf(GralViajesPadre), 0);
		FileSeek(HFilePadre, 0, 0);
		if FileRead(HFilePadre, GralViajesPadre, SizeOf(TImageFileHeader)) <> SizeOf(GralViajesPadre) then begin
            FileClose(HFilePadre);
            raise exception.Create(MSG_ERROR_LEYENDO_HEADER)
        end;

		// Crear los Arrays din�micos. Obtener Header de Viajes (Dir)
		SetLength(DirViajesPadre, GralViajesPadre.CantTransitos);
		FileSeek(HFilePadre, SizeOf(TImageFileHeader), 0);
        CantBytesALeer :=  (GralViajesPadre.CantTransitos * SizeOf(TDirViajesHeader));
		if FileRead(HFilePadre, DirViajesPadre[0], CantBytesALeer) <> CantBytesALeer then begin
            FileClose(HFilePadre);
            raise exception.Create(MSG_ERROR_LEYENDO_LISTA_VIAJES)
        end;

		// Buscar la Posici�n del ItemHeader de las imagenes del tr�nsito
		if not Buscar(NumCorrCA, NumArchivo, OffsetItem) then
		    raise exception.Create('No se encuentra el Viaje correspondiente.');

		FileSeek(HFilePadre, OffsetItem, 0);
        SetLength(ItemsHeader, GralViajesPadre.TiposImagen);
        CantBytesALeer := SizeOf(TItemHeader) * GralViajesPadre.TiposImagen;
		if FileRead(HFilePadre, ItemsHeader[0], CantBytesALeer) <> CantBytesALeer then begin
            FileClose(HFilePadre);
            raise exception.Create(MSG_ERROR_LEYENDO_DATO_VIAJE);
        end;
        // Buscar si el tipo de imagen ya esta almacenado o el en caso contrario el primer lugar libre
        i := 0;
        while (i < GralViajesPadre.TiposImagen) and (ItemsHeader[i].TipoImagen <> TipoImg)
          and (ItemsHeader[i].PosicionInicial <> 0) do begin
            inc(i);
        end;
		// Validar si la imagen existe
		if (ItemsHeader[i].PosicionInicial = 0) or (i = GralViajesPadre.TiposImagen) then
		    raise exception.Create('No se encuentra Imagen.');
		// Calcular la posicion y tama�o del archivo
		OffsetArchivo := ItemsHeader[i].PosicionInicial;
		BytesALeer    := ItemsHeader[i].BytesDatos;
        case TipoImg of
            tiFrontal, tiFrontal2, tiPosterior, tiPosterior2:
                Inc(BytesALeer, BYTES_EXTRAS_VR);
            tiOverview1, tiOverview2, tiOverview3:
                Inc(BytesALeer, BYTES_EXTRAS_OVERVIEW);
        end;
		//
		Result := True;
	except
		on e: Exception do begin
			DescriError := e.Message;
			Result := False;
		end;
	end;
end;

function CrearArchivoPadreTransito(ArchivoP: AnsiString;
  var HFilePadre: Integer; var DescriError: AnsiString): Boolean;
var
	i: integer;
	Dir: AnsiString;
	headerGral: TImageFileHeader;
	headerDir:  array of TDirViajesHeader;
	headerItem: array of TItemHeader;
begin
	try
		// Si el archivo Existe, llegamos ac� por otro problema.
		if FileExists(ArchivoP) then raise Exception.Create('El Archivo a crear ya existe.');
		// Si el Directorio no existe, lo creamos.
		Dir := ExtractFilePath(ArchivoP);
		if not DirectoryExists(Dir) then
			if not ForceDirectories(Dir) then raise Exception.Create('');
		// Creamos el Archivo   
		HFilePadre := FileCreate(ArchivoP, fmOpenReadWrite or fmShareExclusive);
		if HFilePadre = -1 then raise Exception.Create('No se pudo crear el archivo.');
		// Insertamos su header vac�o, excepto por el dato de concesionaria y punto de cobro
		FillChar(headerGral, SizeOf(TImageFileHeader), 0);
		SetLength(headerDir, CANT_TRANSITOS);
		SetLength(headerItem, IMAGENES_POR_TRANSITO * CANT_TRANSITOS);
		//
		i := 1;
		while i <= length(FIRMA_VIAJES) do begin
			headerGral.Firma[i] := FIRMA_VIAJES[i];
			Inc(i);
		end;
		headerGral.CantTransitos := CANT_TRANSITOS;
		headerGral.TiposImagen   := IMAGENES_POR_TRANSITO;
		headerGral.OffsetSgte    := SizeOf(TImageFileHeader) + SizeOf(TDirViajesHeader) * CANT_TRANSITOS
		  + SizeOf(TItemHeader) * IMAGENES_POR_TRANSITO * CANT_TRANSITOS;
		//
		FileSeek(HFilePadre, 0, 0);
		FileWrite(HFilePadre, headerGral, SizeOf(TImageFileHeader));
		FileWrite(HFilePadre, headerDir[0], (SizeOf(TDirViajesHeader) * CANT_TRANSITOS));
		FileWrite(HFilePadre, headerItem[0], (SizeOf(TItemHeader) * IMAGENES_POR_TRANSITO * CANT_TRANSITOS));
		//
		Result := True;
	except
		on e: Exception do begin
			DescriError := e.Message + SysErrorMessage(GetLastError);
			Result := False;
		end;
	end;
end;

function ObtenerPosicionAGrabarTransito(HFilePadre: Integer; NumeroViaje: Integer; TipoImagen: TTipoImagen;
  var GralViajesPadre: TImageFileHeader; var DirViajesPadre: TArrayDirViajes;
  var ItemsHeader: TArrayItemHeader; var IndItemHeader: Integer;
  var NumArchivo: Integer; var OffsetItem: Integer; var DescriError: AnsiString): Boolean;
	//
	function Buscar(GralViajesPadre: TImageFileHeader; NumCorrCA: Int64; var NumArchivo: Integer; var OffsetItem: Integer): Boolean;
	var
		i: Integer;
	begin
		Result := False;
		i := 0;
		while i < CANT_TRANSITOS do begin
			 if (DirViajesPadre[i].NumCorrCA = NumCorrCA) or (DirViajesPadre[i].NumCorrCA = 0) then begin
				// El archivo ya tiene registro � Llegamos al ultimo N� de tr�nsito guardado.
				// Buscar que tiene posicion tiene.
				OffsetItem := SizeOf(TImageFileHeader)
				  + (SizeOf(TDirViajesHeader) * GralViajesPadre.CantTransitos)
				  + (SizeOf(TItemHeader) * i * GralViajesPadre.TiposImagen);
				NumArchivo := i;
				Result := True;
				Break;
			end;
			Inc(i);
		end;
	end;

resourceString
    MSG_ERROR_LEYENDO_HEADER = 'No se puede leer la cabecera del archivo. El archivo padre de tr�nsitos no est� disponible en este momento.';
    MSG_ERROR_LEYENDO_LISTA_TRANSITOS = 'No se puede obtener los tr�nsitos contenidos en el archivo. El archivo padre de tr�nsitos no est� disponible en este momento.';
    MSG_ERROR_LEYENDO_DATO_TRANSITO = 'No se puede obtener la imagen del tr�nsito. El archivo padre de tr�nsitos no est� disponible en este momento.';
var
    i: Integer;
    NroBytesALeer, NroBytesLeidos: longint;
begin
	try
		// Cargamos el general
		FillChar(GralViajesPadre, SizeOf(GralViajesPadre), 0);
		FileSeek(HFilePadre, 0, 0);
		NroBytesALeer := FileRead(HFilePadre, GralViajesPadre, SizeOf(TImageFileHeader));
		if  NroBytesALeer <> SizeOf(TImageFileHeader) then begin
            FileClose(HFilePadre);
            raise exception.Create(MSG_ERROR_LEYENDO_HEADER)
        end;

		// Cargamos solo el Directorio de Transitos.
		SetLength(DirViajesPadre, GralViajesPadre.CantTransitos);
		FileSeek(HFilePadre, SizeOf(TImageFileHeader), 0);
        NroBytesALeer := (GralViajesPadre.CantTransitos * SizeOf(TDirViajesHeader));
		NroBytesLeidos := FileRead(HFilePadre, DirViajesPadre[0], NroBytesALeer);
        if  NroBytesLeidos <> NroBytesALeer then begin
            FileClose(HFilePadre);
            raise exception.Create(MSG_ERROR_LEYENDO_LISTA_TRANSITOS)
        end;

		// Buscamos que posici�n le corresponde al item de ese tr�nsito.
		if Buscar(GralViajesPadre, NumeroViaje, NumArchivo, OffsetItem) then begin
            SetLength(ItemsHeader, SizeOf(TItemHeader) * GralViajesPadre.TiposImagen);
			FileSeek(HFilePadre, OffsetItem, 0);
            NroBytesALeer := (SizeOf(TItemHeader) * GralViajesPadre.TiposImagen);
			NroBytesLeidos := FileRead(HFilePadre, ItemsHeader[0], NroBytesALeer);
            if  NroBytesALeer <> NroBytesLeidos then begin
                FileClose(HFilePadre);
                raise exception.Create(MSG_ERROR_LEYENDO_DATO_TRANSITO);
            end;
            // Buscar si el tipo de imagen ya esta almacenado o el en caso contrario el primer lugar libre
            i := 0;
            while (i < GralViajesPadre.TiposImagen) and (ItemsHeader[i].TipoImagen <> TipoImagen)
              and (ItemsHeader[i].PosicionInicial <> 0) do begin
                inc(i);
            end;
            // Si se recorre toda la estructura sin encontrar lugar retornamos un error
            if i = GralViajesPadre.TiposImagen then begin
                raise exception.Create('No se puede insertar el archivo. No existe lugar para un nuevo tipo de imagen.');
            end;
			// Podr�a ser que ese Item ya tenga valor = imagen ya guardada para ese Tr�nsito. Hay algun Error, descartarlo.
			if ItemsHeader[i].PosicionInicial <> 0 then
			    raise exception.Create('Esa tr�nsito ya estaba almacenado.');
            // Finalmente asignamos el indice del item de la imagen segun el tipo
            IndItemHeader := i;
		end else begin
		   // Ya no hay lugar para el archivo, se supero la cantidad
		   raise exception.Create('No se puede insertar Tr�nsito '+ IntToStr(NumeroViaje) +'  en el archivo. Archivo Padre lleno.');
		end;
		//
		Result := True;
	except
		on e:exception do begin
			DescriError := e.Message;
			Result := False;
		end;
	end;
end;

function ActualizarHeaderViajes(HFilePadre: Integer; GralViajesPadre: TImageFileHeader;
  DirViajesPadre: TArrayDirViajes; ItemsHeader: TArrayItemHeader; NumArchivo: Integer; OffsetItem: Integer): Boolean;
var
	Res, Offset: Integer;
begin
	try
		// Actualizar General del ArchivoPadre
		FileSeek(HFilePadre, 0, 0);
		Res := FileWrite(HFilePadre, GralViajesPadre, SizeOf(TImageFileHeader));
		if Res < 0 then raise Exception.Create('');

		// Actualizar Header del ArchivoPadre
		Offset := SizeOf(TImageFileHeader) + (SizeOf(TDirViajesHeader) * NumArchivo);
		FileSeek(HFilePadre, Offset, 0);
		Res := FileWrite(HFilePadre, DirViajesPadre[NumArchivo], SizeOf(TDirViajesHeader));
		if Res < 0 then raise Exception.Create('');

		// Actualizar Item del ArchivoPadre
		FileSeek(HFilePadre, OffsetItem, 0);
		Res := FileWrite(HFilePadre, ItemsHeader[0], SizeOf(TItemHeader) * GralViajesPadre.TiposImagen);
		if Res < 0 then raise Exception.Create('');
		//
		Result := True;
	except
		Result := False;
	end;
end;



//Funciones Publicas
function AlmacenarImagenIMGPatente(SourceFile, DestFile: AnsiString; Patente: AnsiString; TipoImg: TTipoImagen;
  var DescriError: AnsiString): Boolean;
var
	HFilePadre: Integer;
	ItemsHeader: TArrayItemHeader;
	FDirPatentePadre: TArrayDirPatentes;
	FGralPatentePadre: TImagePlateHeader;
	OffsetItem, OffsetGrabar, Tamanio, NumArchivo, IndItemHeader: Integer;
begin
	// La imagen original est� en pendientes y debe pasar a la estructura de almacenamiento de Referencia.
	Result := False;
	HFilePadre := 0;
	try
		try
			// Verificamos si el archivo fuente existe
			if not FileExists(SourceFile) then begin
                raise exception.Create('No se encuentra la Imagen del tr�nsito. Archivo ' + SourceFile);
			end;
            if GetFileSize(SourceFile) <= 0 then begin                                 // SS_1288_CQU_20150601
                raise exception.Create('Tama�o del archivo incorrecto + SourceFile');  // SS_1288_CQU_20150601
            end;                                                                       // SS_1288_CQU_20150601
			// Bloqueamos el archivo para que otro no pueda modificarlo al mismo tiempo
			HFilePadre := FileOpenRetry(DestFile, fmOpenReadWrite or fmShareDenyWrite);
			if HFilePadre = -1 then begin
				// No existe el archivo
				if not CrearArchivoPadrePatente(DestFile, Patente, HFilePadre, DescriError) then begin
					raise exception.Create(DescriError);
				end;
			end;
			// Buscar header del ArchivoPadre.
			if not ObtenerPosicionAGrabarPatente(HFilePadre, SourceFile, Patente, TipoImg, FGralPatentePadre, FDirPatentePadre,
			  ItemsHeader, IndItemHeader, NumArchivo, OffsetItem, OffsetGrabar, DescriError) then begin
                DescriError := DescriError + ' ObtenerPosicionAGrabarPatente Archivo origen: ' + SourceFile + ' Archivo destino: ' + DestFile ;
				raise exception.Create(DescriError);
			end;
			// Insertar en el ArchivoPadre donde indique OffsetGrabar
			if not InsertarArchivo(SourceFile, HFilePadre, OffsetGrabar,
              TipoImg, ItemsHeader, indItemHeader, Tamanio, DescriError) then begin
                DescriError := DescriError + ' Archivo origen: ' + SourceFile + ' Archivo destino: ' + DestFile ;
				raise exception.Create(DescriError);
			end;
			// Actualizar el header del Padre
			if OffsetGrabar = FGralPatentePadre.OffsetSgte then begin
				// Si insertamos al final del archivo, Cambia el Offset del �ltimo. Sino, queda =.
				FGralPatentePadre.OffsetSgte := FGralPatentePadre.OffsetSgte + Tamanio;
				FDirPatentePadre[NumArchivo].Patente := Trim(Patente);
			end;
			//
			if not ActualizarHeaderPatentes(HFilePadre, FGralPatentePadre, FDirPatentePadre,
              ItemsHeader, NumArchivo, OffsetItem) then begin
				raise exception.Create('Error al actualizar Headers.' + ' Archivo destino: ' + DestFile );
			end;
			// Listo
			Result := True;
		except
			on e: Exception do begin
				DescriError := e.Message;
				Result := False;
			end;
		end;
	finally
		if HFilePadre > 0 then FileClose(HFilePadre);
		Finalize(FDirPatentePadre);
	end;
end;

function AlmacenarImagenIMGPatenteNuevoFormato(SourceFile, DestFile: AnsiString; Patente: AnsiString; TipoImg: TTipoImagen;      //SS-377-NDR-20110607
  var DescriError: AnsiString): Boolean;                                                                                         //SS-377-NDR-20110607
ResourceString                                                                                                                   //SS-377-NDR-20110607
   MSG_NO_SE_ENCUENTRA_ARCHIVO       = 'No se encuentra la Imagen de la Patente.';                                               //SS-377-NDR-20110607
   MSG_NO_SE_PUDO_CREAR_RUTA_DESTINO = 'No se pudo Crear la Ruta de Destino.';                                                   //SS-377-NDR-20110607
   MSG_NO_SE_PUDO_COPIAR_ARCHIVO     = 'No se pudo Copiar la Imagen de la Patente';                                              //SS-377-NDR-20110607
   MSG_ERROR_EN_FUNCION              = 'Copiar Imagen Patente Nuevo Formato: %s';                                                //SS-377-NDR-20110607
Var                                                                                                                              //SS-377-NDR-20110607
   NombreArchivoDestino: String;                                                                                                 //SS-377-NDR-20110607
begin                                                                                                                            //SS-377-NDR-20110607
   Result := False;                                                                                                              //SS-377-NDR-20110607
   try                                                                                                                           //SS-377-NDR-20110607
       // Comprobamos que el Archivo de Origen Exista.                                                                           //SS-377-NDR-20110607
       if not FileExists(SourceFile) then                                                                                        //SS-377-NDR-20110607
           raise exception.Create(MSG_NO_SE_ENCUENTRA_ARCHIVO);                                                                  //SS-377-NDR-20110607
       if GetFileSize(SourceFile) <= 0 then begin                                                                                // SS_1288_CQU_20150601
            DescriError := 'Tama�o del archivo incorrecto';                                                                      // SS_1288_CQU_20150601
            raise exception.Create(DescriError);                                                                                 // SS_1288_CQU_20150601
       end;                                                                                                                      // SS_1288_CQU_20150601
        // Comprobamos que la Ruta de Destino exista, sino la creamos.                                                           //SS-377-NDR-20110607
        If not ForceDirectories(DestFile) then                                                                                   //SS-377-NDR-20110607
           raise exception.Create(MSG_NO_SE_PUDO_CREAR_RUTA_DESTINO);                                                            //SS-377-NDR-20110607
        // Montamos la nomenclatura del Archivo de Destino                                                                       //SS-377-NDR-20110607
        NombreArchivoDestino :=                                                                                                  //SS-377-NDR-20110607
           GoodDir(DestFile) +                                                                                                   //SS-377-NDR-20110607
           Patente + '-' +                                                                                                       //SS-377-NDR-20110607
           SufijoImagen[TipoImg] + '.jpg';                                                                                       //SS-377-NDR-20110607
        // Copiamos la Imagen a la Estructura de Patentes                                                                        //SS-377-NDR-20110607
        if Not FileCopy(SourceFile,NombreArchivoDestino) then                                                                    //SS-377-NDR-20110607
           raise exception.Create(MSG_NO_SE_PUDO_COPIAR_ARCHIVO);                                                                //SS-377-NDR-20110607
                                                                                                                                 //SS-377-NDR-20110607
        Result := True;                                                                                                          //SS-377-NDR-20110607
    except                                                                                                                       //SS-377-NDR-20110607
        on e: Exception do begin                                                                                                 //SS-377-NDR-20110607
            DescriError := Format(MSG_ERROR_EN_FUNCION,[e.Message]);                                                             //SS-377-NDR-20110607
        end;                                                                                                                     //SS-377-NDR-20110607
    end;                                                                                                                         //SS-377-NDR-20110607
end;                                                                                                                             //SS-377-NDR-20110607



function ObtenerImagenIMGPatente(FileName: AnsiString; Patente: AnsiString; TipoImg: TTipoImagen;
  var Imagen: TJPEGPLusIMage; var DataIMage: TDataIMage; var DescriError: AnsiString;
  var tipoError: TTipoErrorImg): Boolean;
var
	HFilePadre: Integer;
	OffsetArchivo, BytesALeer: Integer;
	FDirPatentePadre: TArrayDirPatentes;
	FGralPatentePadre: TImagePlateHeader;
begin
	HFilePadre := 0;
	try
		Result := False;
		try
			// Verificar Par�metro
			if not Assigned(Imagen) then begin
                tipoError := teParametrosMal;
				raise exception.Create('El par�metro Imagen (TBitmap) no puede ser nil');
			end;
			// Verificar que exista
			if not FileExists(FileName) then begin
                tipoError := teArchivoNoExiste;
				raise exception.Create('La imagen no existe');
			end;
            if GetFileSize(FileName) <= 0 then begin                       // SS_1288_CQU_20150601
                tipoError := teLeerArchivo;                                // SS_1288_CQU_20150601
                raise Exception.Create('Tama�o del archivo incorrecto');   // SS_1288_CQU_20150601
            end;                                                           // SS_1288_CQU_20150601
			// Bloqueamos el archivo para que otro no pueda modificarlo al mismo tiempo   //VERRR
			HFilePadre := FileOpenRetry(FileName, fmOpenRead or fmShareDenyWrite);
			if HFilePadre = -1 then begin
                tipoError := teLeerArchivo;
			    raise exception.Create('No se puede acceder al archivo que almacena la imagen.');
            end;
			// Buscar la posici�n donde leer la imagen en el ArchivoPadre.
			if not ObtenerPosicionALeerPatente(HFilePadre, Patente, TipoImg, FGralPatentePadre, FDirPatentePadre,
			  OffsetArchivo, BytesALeer, DescriError) then begin
                tipoError := teLeerArchivo;
			    raise exception.Create('Error al Obtener posici�n.' + DescriError);
              end;
			// Leer
			if not CargarImagenAlmacenada(HFilePadre, OffsetArchivo, BytesALeer, Imagen, DataImage,
              DescriError, tipoerror) then begin
			    raise exception.Create(DescriError);
            end;
			// Listo
			Result := True;
		except
			on e:Exception do begin
				Result := False;
				DescriError := e.Message;
			end;
		end;
	finally
		if HFilePadre > 0 then FileClose(HFilePadre);
		Finalize(FDirPatentePadre);
	end;
end;

function ObtenerImagenIMGPatenteNuevoFormato(FileName: AnsiString; Patente: AnsiString; TipoImg: TTipoImagen;                        //SS-377-NDR-20110607
  var Imagen: TJPEGPLusIMage; var DataIMage: TDataIMage; var DescriError: AnsiString; var tipoError: TTipoErrorImg): Boolean; overload; // SS_1091_CQU_20131001 //SS-377-NDR-20110607
ResourceString                                                                                                                       //SS-377-NDR-20110607
   MSG_ERROR_EN_PARAMETRO_IMAGEN = 'El par�metro Imagen (TJPEGPLusIMage) no puede ser nil.';                                         //SS-377-NDR-20110607
   MSG_ERROR_IMAGEN_NO_EXISTE    = 'La imagen no existe.';                                                                           //SS-377-NDR-20110607
   MSG_ERROR_CARGA_IMAGEN        = 'Se produjo un Error al Cargar la Imagen.';                                                       //SS-377-NDR-20110607
   MSG_ERROR_ABRIR_ARCHIVO       = 'Error al abrir archivo';                                                                         //SS-377-NDR-20110607
var                                                                                                                                  //SS-377-NDR-20110607
   NombreArchivoOrigen: String;                                                                                                      //SS-377-NDR-20110607
   HandleArchivo: Integer;                                                                                                           //SS-377-NDR-20110607
begin                                                                                                                                //SS-377-NDR-20110607
   Result := False;                                                                                                                  //SS-377-NDR-20110607
       try                                                                                                                           //SS-377-NDR-20110607
           // Verificamos el Par�metro Imagen                                                                                        //SS-377-NDR-20110607
			if not Assigned(Imagen) then begin                                                                                       //SS-377-NDR-20110607
                tipoError := teParametrosMal;                                                                                        //SS-377-NDR-20110607
				raise exception.Create(MSG_ERROR_EN_PARAMETRO_IMAGEN);                                                               //SS-377-NDR-20110607
			end;                                                                                                                     //SS-377-NDR-20110607
           // Montamos la nomenclatura del Archivo a Recuperar.                                                                      //SS-377-NDR-20110607
            NombreArchivoOrigen :=                                                                                                   //SS-377-NDR-20110607
               GoodDir(FileName) +                                                                                                   //SS-377-NDR-20110607
               Patente + '-' +                                                                                                       //SS-377-NDR-20110607
               SufijoImagen[TipoImg] + '.jpg';                                                                                       //SS-377-NDR-20110607
			// Verificamos que exista el Archivo a Recuperar                                                                         //SS-377-NDR-20110607
			if not FileExists(NombreArchivoOrigen) then begin                                                                        //SS-377-NDR-20110607
                tipoError := teArchivoNoExiste;                                                                                      //SS-377-NDR-20110607
				raise exception.Create(MSG_ERROR_IMAGEN_NO_EXISTE);                                                                  //SS-377-NDR-20110607
			end;                                                                                                                     //SS-377-NDR-20110607
            if GetFileSize(NombreArchivoOrigen) <= 0 then begin                                                                      // SS_1288_CQU_20150601
                tipoError := teLeerArchivo;                                                                                          // SS_1288_CQU_20150601
                DescriError := 'Tama�o del archivo incorrecto';                                                                      // SS_1288_CQU_20150601
                raise Exception.Create(MSG_ERROR_CARGA_IMAGEN);                                                                      // SS_1288_CQU_20150601
            end;                                                                                                                     // SS_1288_CQU_20150601
           // Cargamos la Imagen del Archivo                                                                                         //SS-377-NDR-20110607
           Try                                                                                                                       //SS-377-NDR-20110607
               HandleArchivo := -1;                                                                                                  //SS-377-NDR-20110607
         		HandleArchivo := FileOpenRetry(NombreArchivoOrigen, fmOpenRead or fmShareDenyWrite);                                 //SS-377-NDR-20110607
   	    	if HandleArchivo = -1 then                                                                                               //SS-377-NDR-20110607
   		        raise exception.Create(MSG_ERROR_ABRIR_ARCHIVO);                                                                     //SS-377-NDR-20110607
      	    	//if not CargarImagenDeArchivoIndividual(HandleArchivo, Imagen, DataImage, DescriError) then                         //SS-377-NDR-20110607
                if not ObtenerImagen(NombreArchivoOrigen, Imagen, DataImage,DescriError) then                                        //SS-377-NDR-20110607
       			//    raise exception.Create(DescriError);                                                                           // SS_1091_CQU_20131001	//SS-377-NDR-20110607
      			//	Result := True;                                                                                                  // SS_1091_CQU_20131001    //SS-377-NDR-20110607
       		        raise exception.Create(DescriError)                                                                              // SS_1091_CQU_20131001
                else begin																											 // SS_1091_CQU_20131001
                    DescriError := EmptyStr;																						 // SS_1091_CQU_20131001
                    Result := True;																									 // SS_1091_CQU_20131001
                end;																											 	 // SS_1091_CQU_20131001
           finally                                                                                                                   //SS-377-NDR-20110607
               if HandleArchivo > 0 then FileClose(HandleArchivo);                                                                   //SS-377-NDR-20110607
           end;                                                                                                                      //SS-377-NDR-20110607
		except                                                                                                                       //SS-377-NDR-20110607
			on e:Exception do begin                                                                                                  //SS-377-NDR-20110607
				Result := False;                                                                                                     //SS-377-NDR-20110607
				DescriError := e.Message;                                                                                            //SS-377-NDR-20110607
			end;                                                                                                                     //SS-377-NDR-20110607
		end;                                                                                                                         //SS-377-NDR-20110607
end;                                                                                                                                 //SS-377-NDR-20110607

function ObtenerImagenIMGPatenteNuevoFormato(FileName: AnsiString; Patente: AnsiString; TipoImg: TTipoImagen;                       // SS_1091_CQU_20131001
    var Imagen: TBitmap; var DataIMage: TDataIMage; var DescriError: AnsiString; var tipoError: TTipoErrorImg): Boolean; overload;  // SS_1091_CQU_20131001
ResourceString                                                                                                                      // SS_1091_CQU_20131001
    MSG_ERROR_EN_PARAMETRO_IMAGEN = 'El par�metro Imagen (TBitmap) no puede ser nil.';                                              // SS_1091_CQU_20131001
    MSG_ERROR_IMAGEN_NO_EXISTE    = 'La imagen no existe.';                                                                         // SS_1091_CQU_20131001
    MSG_ERROR_CARGA_IMAGEN        = 'Se produjo un Error al Cargar la Imagen.';                                                     // SS_1091_CQU_20131001
    MSG_ERROR_ABRIR_ARCHIVO       = 'Error al abrir archivo';                                                                       // SS_1091_CQU_20131001
var                                                                                                                                 // SS_1091_CQU_20131001
    NombreArchivoOrigen: String;                                                                                                    // SS_1091_CQU_20131001
    HandleArchivo: Integer;                                                                                                         // SS_1091_CQU_20131001
begin                                                                                                                               // SS_1091_CQU_20131001
    Result := False;                                                                                                                // SS_1091_CQU_20131001
    try                                                                                                                             // SS_1091_CQU_20131001
        // Verificamos el Par�metro Imagen                                                                                          // SS_1091_CQU_20131001
        if not Assigned(Imagen) then begin                                                                                          // SS_1091_CQU_20131001
            tipoError := teParametrosMal;                                                                                           // SS_1091_CQU_20131001
            raise exception.Create(MSG_ERROR_EN_PARAMETRO_IMAGEN);                                                                  // SS_1091_CQU_20131001
        end;                                                                                                                        // SS_1091_CQU_20131001
        // Montamos la nomenclatura del Archivo a Recuperar.                                                                        // SS_1091_CQU_20131001
        NombreArchivoOrigen :=                                                                                                      // SS_1091_CQU_20131001
           GoodDir(FileName) +                                                                                                      // SS_1091_CQU_20131001
           Patente + '-' +                                                                                                          // SS_1091_CQU_20131001
           SufijoImagen[TipoImg] + '.jpg';                                                                                          // SS_1091_CQU_20131001
        // Verificamos que exista el Archivo a Recuperar                                                                            // SS_1091_CQU_20131001
        if not FileExists(NombreArchivoOrigen) then begin                                                                           // SS_1091_CQU_20131001
            tipoError := teArchivoNoExiste;                                                                                         // SS_1091_CQU_20131001
            raise exception.Create(MSG_ERROR_IMAGEN_NO_EXISTE);                                                                     // SS_1091_CQU_20131001
        end;                                                                                                                        // SS_1091_CQU_20131001
        if GetFileSize(NombreArchivoOrigen) <= 0 then begin                                                                         // SS_1288_CQU_20150601
            tipoError := teLeerArchivo;                                                                                             // SS_1288_CQU_20150601
            DescriError := 'Tama�o del archivo incorrecto';                                                                         // SS_1288_CQU_20150601
            raise Exception.Create(MSG_ERROR_CARGA_IMAGEN);                                                                         // SS_1288_CQU_20150601
        end;                                                                                                                        // SS_1288_CQU_20150601
        // Cargamos la Imagen del Archivo                                                                                           // SS_1091_CQU_20131001
        Try                                                                                                                         // SS_1091_CQU_20131001
            HandleArchivo := -1;                                                                                                    // SS_1091_CQU_20131001
            HandleArchivo := FileOpenRetry(NombreArchivoOrigen, fmOpenRead or fmShareDenyWrite);                                    // SS_1091_CQU_20131001
        if HandleArchivo = -1 then                                                                                                  // SS_1091_CQU_20131001
            raise exception.Create(MSG_ERROR_ABRIR_ARCHIVO);                                                                        // SS_1091_CQU_20131001
            if not ObtenerImagen(NombreArchivoOrigen, Imagen, DataImage, DescriError) then                                          // SS_1091_CQU_20131001
                raise exception.Create(DescriError)                                                                                 // SS_1091_CQU_20131001
            else begin                                                                                                              // SS_1091_CQU_20131001
                DescriError := EmptyStr;                                                                                            // SS_1091_CQU_20131001
                Result      := True;                                                                                                // SS_1091_CQU_20131001
            end;                                                                                                                    // SS_1091_CQU_20131001
        finally                                                                                                                     // SS_1091_CQU_20131001
           if HandleArchivo > 0 then FileClose(HandleArchivo);                                                                      // SS_1091_CQU_20131001
        end;                                                                                                                        // SS_1091_CQU_20131001
    except                                                                                                                          // SS_1091_CQU_20131001
        on e:Exception do begin                                                                                                     // SS_1091_CQU_20131001
            Result := False;                                                                                                        // SS_1091_CQU_20131001
            DescriError := e.Message;                                                                                               // SS_1091_CQU_20131001
        end;                                                                                                                        // SS_1091_CQU_20131001
    end;                                                                                                                            // SS_1091_CQU_20131001
end;                                                                                                                                // SS_1091_CQU_20131001

function ObtenerImagenIMGTransito(FileName: AnsiString;
  NumCorrCA: Int64;
  TipoImg: TTipoImagen; var Imagen: TJPEGPlusImage; var DataImage: TDataIMage;
  var DescriError: AnsiString;
  var tipoError: TTipoErrorImg): Boolean;
var
	HFilePadre: Integer;
	DirViajesPadre: TArrayDirViajes;
	GralViajesPadre: TImageFileHeader;
	OffsetArchivo, BytesALeer: Integer;
begin
	HFilePadre := 0;
	try
		Result := False;
		try
			// Verificar Par�metro
			if not Assigned(Imagen) then begin
                tipoError := teParametrosMal;
				DescriError := DescripcionError[teParametrosMal];
				Exit;
			end;
			// Verificar que exista el archivo
			if not FileExists(FileName) then begin
                tipoError := teArchivoNoExiste;
				DescriError := DescripcionError[teArchivoNoExiste];
				Exit;
			end;
            if GetFileSize(FileName) <= 0 then begin               // SS_1288_CQU_20150601
                tipoError := teLeerArchivo;                        // SS_1288_CQU_20150601
                DescriError := 'Tama�o del archivo incorrecto';    // SS_1288_CQU_20150601
                Exit;                                              // SS_1288_CQU_20150601
            end;                                                   // SS_1288_CQU_20150601
			// Bloqueamos el archivo para que otro no pueda modificarlo al mismo tiempo
			HFilePadre := FileOpenRetry(FileName, fmOpenRead or fmShareDenyWrite);
			if HFilePadre = -1 then begin
                tipoError := teAbrirArchivo;
			    raise exception.Create(DescripcionError[teAbrirArchivo]);
            end;
			// Buscar la posici�n donde leer la imagen en el ArchivoPadre.
			if not ObtenerPosicionALeerTransito(HFilePadre, NumCorrCA, TipoImg, GralViajesPadre,
			  DirViajesPadre, OffsetArchivo, BytesALeer, DescriError) then begin
                tipoError := teLeerArchivo;
			    raise exception.Create(DescripcionError[teLeerArchivo] + ' ' + DescriError);
            end;
			// Leer
			if not CargarImagenAlmacenada(HFilePadre, OffsetArchivo, BytesALeer, Imagen, DataImage, DescriError, tipoError) then begin
			    raise exception.Create(DescripcionError[teLeerArchivo] + ' ' + DescriError);
            end;
			// Listo
			Result := True;
		except
			on e:Exception do begin
				Result := False;
				DescriError := e.Message;
			end;
		end;
	finally
		if HFilePadre > 0 then FileClose(HFilePadre);
		Finalize(DirViajesPadre);
	end;
end;


function ObtenerImagenIMGTransito(FileName: AnsiString;
  NumCorrCA: Int64;
  TipoImg: TTipoImagen; var Imagen: TBitmap; var DataImage: TDataIMage;
  var DescriError: AnsiString;
  var tipoError: TTipoErrorImg): Boolean;
var
	HFilePadre: Integer;
	DirViajesPadre: TArrayDirViajes;
	GralViajesPadre: TImageFileHeader;
	OffsetArchivo, BytesALeer: Integer;
begin
	HFilePadre := 0;
	try
		Result := False;
		try
			// Verificar Par�metro
			if not Assigned(Imagen) then begin
                tipoError := teParametrosMal;
                DescriError := DescripcionError[teParametrosMal];
				Exit;
			end;
			// Verificar que exista el archivo
			if not FileExists(FileName) then begin
                tipoError := teArchivoNoExiste;
                DescriError := DescripcionError[teParametrosMal];
				Exit;
			end;
            if GetFileSize(FileName) <= 0 then begin                // SS_1288_CQU_20150601
                tipoError := teLeerArchivo;                         // SS_1288_CQU_20150601
                DescriError := 'Tama�o del archivo incorrecto';     // SS_1288_CQU_20150601
                Exit;                                               // SS_1288_CQU_20150601
            end;                                                    // SS_1288_CQU_20150601
			// Bloqueamos el archivo para que otro no pueda modificarlo al mismo tiempo
			HFilePadre := FileOpenRetry(FileName, fmOpenRead or fmShareDenyWrite);
			if HFilePadre = -1 then begin
                tipoError := teAbrirArchivo;
			    raise exception.Create(DescripcionError[teAbrirArchivo]);
            end;
			// Buscar la posici�n donde leer la imagen en el ArchivoPadre.
			if not ObtenerPosicionALeerTransito(HFilePadre, NumCorrCA, TipoImg, GralViajesPadre,
			  DirViajesPadre, OffsetArchivo, BytesALeer, DescriError) then begin
                tipoError := teLeerArchivo;
			    raise exception.Create(DescripcionError[teLeerArchivo] + ' ' + DescriError);
            end;
			// Leer
			if not CargarImagenAlmacenada(HFilePadre, OffsetArchivo, BytesALeer, Imagen, DataImage, DescriError, tipoError) then
		        raise exception.Create(DescripcionError[teLeerArchivo] + ' ' + DescriError);
			// Listo
			Result := True;
		except
			on e:Exception do begin
				Result := False;
				DescriError := e.Message;
			end;
		end;
	finally
		if HFilePadre > 0 then FileClose(HFilePadre);
		Finalize(DirViajesPadre);
	end;
end;


function ObtenerImagenIMGTransitoNuevoFormato(FileName: AnsiString; NumCorrCA: Int64; TipoImg: TTipoImagen;var Imagen: TJPEGPlusImage;             //SS-377-NDR-20110607
   var DataImage: TDataIMage; var DescriError: AnsiString; var tipoError: TTipoErrorImg): Boolean;                                                 //SS-377-NDR-20110607
ResourceString                                                                                                                                     //SS-377-NDR-20110607
   MSG_ERROR_EN_PARAMETRO_IMAGEN = 'El par�metro Imagen (TJPEGPLusIMage) no puede ser nil.';                                                       //SS-377-NDR-20110607
   MSG_ERROR_IMAGEN_NO_EXISTE    = 'La imagen no existe.';                                                                                         //SS-377-NDR-20110607
   MSG_ERROR_CARGA_IMAGEN        = 'Se produjo un Error al Cargar la Imagen.';                                                                     //SS-377-NDR-20110607
   MSG_ERROR_ABRIR_ARCHIVO       = 'Error al abrir archivo';                                                                                       //SS-377-NDR-20110607
var                                                                                                                                                //SS-377-NDR-20110607
   NombreArchivoOrigen: String;                                                                                                                    //SS-377-NDR-20110607
   HandleArchivo: Integer;                                                                                                                         //SS-377-NDR-20110607
begin                                                                                                                                              //SS-377-NDR-20110607
   Result := False;                                                                                                                                //SS-377-NDR-20110607
       try                                                                                                                                         //SS-377-NDR-20110607
           // Verificamos el Par�metro Imagen                                                                                                      //SS-377-NDR-20110607
			if not Assigned(Imagen) then begin                                                                                                     //SS-377-NDR-20110607
                tipoError := teParametrosMal;                                                                                                      //SS-377-NDR-20110607
				raise exception.Create(MSG_ERROR_EN_PARAMETRO_IMAGEN);                                                                             //SS-377-NDR-20110607
			end;                                                                                                                                   //SS-377-NDR-20110607
           // Montamos la nomenclatura del Archivo a Recuperar.                                                                                    //SS-377-NDR-20110607
            NombreArchivoOrigen :=                                                                                                                 //SS-377-NDR-20110607
               GoodDir(FileName) +                                                                                                                 //SS-377-NDR-20110607
               IntToStr(NumCorrCA) + '-' +                                                                                                         //SS-377-NDR-20110607
               SufijoImagen[TipoImg] + '.jpg';                                                                                                     //SS-377-NDR-20110607
			// Verificamos que exista el Archivo a Recuperar                                                                                       //SS-377-NDR-20110607
			if not FileExists(NombreArchivoOrigen) then begin                                                                                      //SS-377-NDR-20110607
                tipoError := teArchivoNoExiste;                                                                                                    //SS-377-NDR-20110607
				raise exception.Create(MSG_ERROR_IMAGEN_NO_EXISTE);                                                                                //SS-377-NDR-20110607
			end;                                                                                                                                   //SS-377-NDR-20110607
            if GetFileSize(NombreArchivoOrigen) <= 0 then begin                                                                                    // SS_1288_CQU_20150601
                DescriError := 'Tama�o del archivo incorrecto';                                                                                    // SS_1288_CQU_20150601
                tipoError := teLeerArchivo;                                                                                                        // SS_1288_CQU_20150601
                raise exception.Create(MSG_ERROR_CARGA_IMAGEN);                                                                                    // SS_1288_CQU_20150601
           end;                                                                                                                                    // SS_1288_CQU_20150601
           // Cargamos la Imagen del Archivo                                                                                                       //SS-377-NDR-20110607
           Try                                                                                                                                     //SS-377-NDR-20110607
               HandleArchivo := -1;                                                                                                                //SS-377-NDR-20110607
         		HandleArchivo := FileOpenRetry(NombreArchivoOrigen, fmOpenRead or fmShareDenyWrite);                                               //SS-377-NDR-20110607
   	    	if HandleArchivo = -1 then                                                                                                             //SS-377-NDR-20110607
   		        raise exception.Create(MSG_ERROR_ABRIR_ARCHIVO);                                                                                   //SS-377-NDR-20110607
      	    	//if not CargarImagenDeArchivoIndividual(HandleArchivo, Imagen, DataImage, DescriError) then                                       //SS-377-NDR-20110607
                if not ObtenerImagen(NombreArchivoOrigen, Imagen, DataImage,DescriError) then                                                      //SS-377-NDR-20110607
       		    raise exception.Create(DescriError);                                                                                               //SS-377-NDR-20110607
      			Result := True;                                                                                                                    //SS-377-NDR-20110607
           finally                                                                                                                                 //SS-377-NDR-20110607
               if HandleArchivo > 0 then FileClose(HandleArchivo);                                                                                 //SS-377-NDR-20110607
           end;                                                                                                                                    //SS-377-NDR-20110607
		except                                                                                                                                     //SS-377-NDR-20110607
			on e:Exception do begin                                                                                                                //SS-377-NDR-20110607
				Result := False;                                                                                                                   //SS-377-NDR-20110607
				DescriError := e.Message;                                                                                                          //SS-377-NDR-20110607
			end;                                                                                                                                   //SS-377-NDR-20110607
		end;                                                                                                                                       //SS-377-NDR-20110607
end;                                                                                                                                               //SS-377-NDR-20110607
                                                                                                                                                   //SS-377-NDR-20110607
function ObtenerImagenIMGTransitoNuevoFormato(FileName: AnsiString; NumCorrCA: Int64; TipoImg: TTipoImagen; var Imagen: TBitmap;                   //SS-377-NDR-20110607
   var DataImage: TDataIMage; var DescriError: AnsiString; var tipoError: TTipoErrorImg): Boolean;                                                 //SS-377-NDR-20110607
ResourceString                                                                                                                                     //SS-377-NDR-20110607
   MSG_ERROR_EN_PARAMETRO_IMAGEN = 'El par�metro Imagen (TBitmap) no puede ser nil.';                                                              //SS-377-NDR-20110607
   MSG_ERROR_IMAGEN_NO_EXISTE    = 'La imagen no existe.';                                                                                         //SS-377-NDR-20110607
   MSG_ERROR_CARGA_IMAGEN        = 'Se produjo un Error al Cargar la Imagen.';                                                                     //SS-377-NDR-20110607
   MSG_ERROR_ABRIR_ARCHIVO       = 'Error al abrir archivo';                                                                                       //SS-377-NDR-20110607
var                                                                                                                                                //SS-377-NDR-20110607
   NombreArchivoOrigen: String;                                                                                                                    //SS-377-NDR-20110607
   HandleArchivo: Integer;                                                                                                                         //SS-377-NDR-20110607
begin                                                                                                                                              //SS-377-NDR-20110607
   Result := False;                                                                                                                                //SS-377-NDR-20110607
       try                                                                                                                                         //SS-377-NDR-20110607
           // Verificamos el Par�metro Imagen                                                                                                      //SS-377-NDR-20110607
			if not Assigned(Imagen) then begin                                                                                                     //SS-377-NDR-20110607
                tipoError := teParametrosMal;                                                                                                      //SS-377-NDR-20110607
				raise exception.Create(MSG_ERROR_EN_PARAMETRO_IMAGEN);                                                                             //SS-377-NDR-20110607
			end;                                                                                                                                   //SS-377-NDR-20110607
           // Montamos la nomenclatura del Archivo a Recuperar.                                                                                    //SS-377-NDR-20110607
            NombreArchivoOrigen :=                                                                                                                 //SS-377-NDR-20110607
               GoodDir(FileName) +                                                                                                                 //SS-377-NDR-20110607
               IntToStr(NumCorrCA) + '-' +                                                                                                         //SS-377-NDR-20110607
               SufijoImagen[TipoImg] + '.jpg';                                                                                                     //SS-377-NDR-20110607
			// Verificamos que exista el Archivo a Recuperar                                                                                       //SS-377-NDR-20110607
			if not FileExists(NombreArchivoOrigen) then begin                                                                                      //SS-377-NDR-20110607
                tipoError := teArchivoNoExiste;                                                                                                    //SS-377-NDR-20110607
				raise exception.Create(MSG_ERROR_IMAGEN_NO_EXISTE);                                                                                //SS-377-NDR-20110607
			end;                                                                                                                                   //SS-377-NDR-20110607
            if GetFileSize(NombreArchivoOrigen) <= 0 then begin                                                                                    // SS_1288_CQU_20150601
                DescriError := 'Tama�o del archivo incorrecto';                                                                                    // SS_1288_CQU_20150601
                tipoError := teLeerArchivo;                                                                                                        // SS_1288_CQU_20150601
                raise exception.Create(MSG_ERROR_CARGA_IMAGEN);                                                                                    // SS_1288_CQU_20150601
           end;                                                                                                                                    // SS_1288_CQU_20150601
           // Cargamos la Imagen del Archivo                                                                                                       //SS-377-NDR-20110607
           Try                                                                                                                                     //SS-377-NDR-20110607
               HandleArchivo := -1;                                                                                                                //SS-377-NDR-20110607
         		HandleArchivo := FileOpenRetry(NombreArchivoOrigen, fmOpenRead or fmShareDenyWrite);                                               //SS-377-NDR-20110607
   	    	if HandleArchivo = -1 then                                                                                                             //SS-377-NDR-20110607
   		        raise exception.Create(MSG_ERROR_ABRIR_ARCHIVO);                                                                                   //SS-377-NDR-20110607
      	    	//if not CargarImagenDeArchivoIndividual(HandleArchivo, Imagen, DataImage, DescriError) then                                       //SS-377-NDR-20110607
                if not ObtenerImagen(NombreArchivoOrigen, Imagen, DataImage,DescriError) then                                                      //SS-377-NDR-20110607
       		    raise exception.Create(DescriError);                                                                                               //SS-377-NDR-20110607
      			Result := True;                                                                                                                    //SS-377-NDR-20110607
           finally                                                                                                                                 //SS-377-NDR-20110607
               if HandleArchivo > 0 then FileClose(HandleArchivo);                                                                                 //SS-377-NDR-20110607
           end;                                                                                                                                    //SS-377-NDR-20110607
		except                                                                                                                                     //SS-377-NDR-20110607
			on e:Exception do begin                                                                                                                //SS-377-NDR-20110607
				Result := False;                                                                                                                   //SS-377-NDR-20110607
				DescriError := e.Message;                                                                                                          //SS-377-NDR-20110607
			end;                                                                                                                                   //SS-377-NDR-20110607
		end;                                                                                                                                       //SS-377-NDR-20110607
end;                                                                                                                                               //SS-377-NDR-20110607


function ExisteImagenIMGTransito(FileName: AnsiString; NumCorrCA: Int64;
  TipoImagen: TTipoImagen; var DescriError: AnsiString): Boolean;
var
	HFilePadre: Integer;
	Offset, Bytes: Integer;
	DirViajesPadre: TArrayDirViajes;
	GralViajesPadre: TImageFileHeader;
begin
	HFilePadre := 0;
	// Verificar que existan imagenes de un tr�nsito en viaje
	try
		Result := False;
		try
			// Verificar que exista
			if not FileExists(FileName) then begin
				DescriError := 'La imagen no existe';
				Exit;
			end;
            if GetFileSize(FileName) <= 0 then begin                // SS_1288_CQU_20150601
                DescriError := 'Tama�o del archivo incorrecto';     // SS_1288_CQU_20150601
                Exit;                                               // SS_1288_CQU_20150601
            end;                                                    // SS_1288_CQU_20150601
			// Bloqueamos el archivo para que otro no pueda modificarlo al mismo tiempo
//			HFilePadre := FileOpenRetry(ArchivoPadre, fmOpenRead or fmShareDenyWrite);
            HFilePadre := FileOpenRetry(FileName, fmOpenRead or fmShareDenyWrite);

			if HFilePadre = -1 then
			  raise exception.Create('No se puede acceder al archivo que almacena la imagen.');
			// Buscar la posici�n donde leer la imagen en el ArchivoPadre.
			if not ObtenerPosicionALeerTransito(HFilePadre, NumCorrCA, TipoImagen, GralViajesPadre,
			  DirViajesPadre, Offset, Bytes, DescriError) then
			    raise exception.Create('No existe la imagen.' + DescriError);
			// Listo
			Result := True;
		except
			on e:Exception do begin
				Result := False;
				DescriError := e.Message;
			end;
		end;
	finally
		if HFilePadre > 0 then FileClose(HFilePadre);
		Finalize(DirViajesPadre);
	end;
end;


function AlmacenarImagenIMGTransito(SourceFile, DestFile: AnsiString; NumCorrCA: Integer;
  TipoImagen: TTipoImagen; var DescriError: AnsiString): Boolean;
var
	HFilePadre: Integer;
	ItemsHeader: TArrayItemHeader;
  	GralViajesPadre: TImageFileHeader;
	DirViajesPadre: TArrayDirViajes;
	OffsetItem, Tamanio, NumArchivo, IndItemHeader: Integer;
begin
	Result := False;
	HFilePadre := 0;
	try
		try
			// Verificamos si el archivo fuente existe
			if not FileExists(SourceFile) then begin
				raise exception.Create('No se encuentra la Imagen del tr�nsito.');
			end;
            if GetFileSize(SourceFile) <= 0 then begin                     // SS_1288_CQU_20150601
                raise exception.Create('Tama�o del archivo incorrecto.');  // SS_1288_CQU_20150601
            end;                                                           // SS_1288_CQU_20150601
			// Bloqueamos el archivo para que otro no pueda modificarlo al mismo tiempo
			HFilePadre := FileOpenRetry(DestFile, fmOpenReadWrite or fmShareDenyWrite);
			if HFilePadre = -1 then begin
				// No existe el archivo
				if not CrearArchivoPadreTransito(DestFile, HFilePadre, DescriError) then begin
        			raise exception.Create(DescriError);
				end;
			end;
            // Buscar header del ArchivoPadre.
            if not ObtenerPosicionAGrabarTransito(HFilePadre, NumCorrCA, TipoImagen,
              GralViajesPadre, DirViajesPadre, ItemsHeader, IndItemHeader,
              NumArchivo, OffsetItem, DescriError) then begin
                raise exception.Create(DescriError);
            end;
            // Insertamos el archivo
            if not InsertarArchivo(SourceFile, HFilePadre, GralViajesPadre.OffsetSgte, TipoImagen,
              ItemsHeader, IndItemHeader, Tamanio,  DescriError) then begin
                raise exception.Create(DescriError);
            end;
            // Actualizar el header del Padre
            GralViajesPadre.OffsetSgte 		        := GralViajesPadre.OffsetSgte + Tamanio;
            DirViajesPadre[NumArchivo].NumCorrCA    := NumCorrCA;
            if not ActualizarHeaderViajes(HFilePadre, GralViajesPadre, DirViajesPadre,
              ItemsHeader, NumArchivo, OffsetItem) then begin
                raise exception.Create('Error al actualizar Headers.');
            end;
			Result := True;
		except
			on e: Exception do begin
				DescriError := 'Copiar Imagen: ' + e.Message;
				Result := False;
			end;
		end;
	finally
		if HFilePadre > 0 then FileClose(HFilePadre);
		Finalize(DirViajesPadre);
	end;
end;

function AlmacenarImagenIMGTransitoNuevoFormato(SourceFile, DestFile: AnsiString; NumCorrCA: Integer; TipoImagen: TTipoImagen;                //SS-377-NDR-20110607
   var DescriError: AnsiString): Boolean;                                                                                                     //SS-377-NDR-20110607
ResourceString                                                                                                                                //SS-377-NDR-20110607
   MSG_NO_SE_ENCUENTRA_ARCHIVO       = 'No se encuentra la Imagen del Transito.';                                                             //SS-377-NDR-20110607
   MSG_NO_SE_PUDO_CREAR_RUTA_DESTINO = 'No se pudo Crear la Ruta de Destino.';                                                                //SS-377-NDR-20110607
   MSG_NO_SE_PUDO_COPIAR_ARCHIVO     = 'No se pudo Copiar la Imagen de la Patente';                                                           //SS-377-NDR-20110607
   MSG_ERROR_EN_FUNCION              = 'Copiar Imagen Transito Nuevo Formato: %s';                                                            //SS-377-NDR-20110607
Var                                                                                                                                           //SS-377-NDR-20110607
   NombreArchivoDestino: String;                                                                                                              //SS-377-NDR-20110607
begin                                                                                                                                         //SS-377-NDR-20110607
   Result := False;                                                                                                                           //SS-377-NDR-20110607
   try                                                                                                                                        //SS-377-NDR-20110607
       // Comprobamos que el Archivo de Origen Exista.                                                                                        //SS-377-NDR-20110607
       if not FileExists(SourceFile) then                                                                                                     //SS-377-NDR-20110607
           raise exception.Create(MSG_NO_SE_ENCUENTRA_ARCHIVO);                                                                               //SS-377-NDR-20110607
        if GetFileSize(SourceFile) <= 0 then begin                                                                                            // SS_1288_CQU_20150601
            DescriError := 'Tama�o del archivo incorrecto';                                                                                   // SS_1288_CQU_20150601
            raise exception.Create(DescriError);                                                                                              // SS_1288_CQU_20150601
        end;                                                                                                                                  // SS_1288_CQU_20150601
        // Comprobamos que la Ruta de Destino exista, sino la creamos.                                                                        //SS-377-NDR-20110607
        If not ForceDirectories(DestFile) then                                                                                                //SS-377-NDR-20110607
           raise exception.Create(MSG_NO_SE_PUDO_CREAR_RUTA_DESTINO);                                                                         //SS-377-NDR-20110607
        // Montamos la nomenclatura del Archivo de Destino                                                                                    //SS-377-NDR-20110607
        NombreArchivoDestino :=                                                                                                               //SS-377-NDR-20110607
           DestFile +                                                                                                                         //SS-377-NDR-20110607
           IntToStr(NumCorrCA) + '-' +                                                                                                        //SS-377-NDR-20110607
           SufijoImagen[TipoImagen] + '.jpg';                                                                                                 //SS-377-NDR-20110607
        // Copiamos la Imagen a la Estructura de Patentes                                                                                     //SS-377-NDR-20110607
        if Not FileCopy(SourceFile,NombreArchivoDestino) then                                                                                 //SS-377-NDR-20110607
           raise exception.Create(MSG_NO_SE_PUDO_COPIAR_ARCHIVO);                                                                             //SS-377-NDR-20110607
                                                                                                                                              //SS-377-NDR-20110607
        Result := True;                                                                                                                       //SS-377-NDR-20110607
    except                                                                                                                                    //SS-377-NDR-20110607
        on e: Exception do begin                                                                                                              //SS-377-NDR-20110607
            DescriError := Format(MSG_ERROR_EN_FUNCION,[e.Message]);                                                                          //SS-377-NDR-20110607
        end;                                                                                                                                  //SS-377-NDR-20110607
    end;                                                                                                                                      //SS-377-NDR-20110607
end;                                                                                                                                          //SS-377-NDR-20110607
                                                                                                                                              //SS-377-NDR-20110607
end.                                                                                                                                          //SS-377-NDR-20110607

