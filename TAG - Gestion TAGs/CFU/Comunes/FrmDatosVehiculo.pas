unit FrmDatosVehiculo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, DmiCtrls, MaskCombo, PeaProcs,
  DMCOnnection, Util, UtilProc, peatypes, frmInfoPlanesComerciales,
  FreDatosVehiculo,ADODB, UtilFacturacion;

type
  TFormDatosVehiculo = class(TForm)
    pnlBotones: TPanel;
    btnAceptar: TDPSButton;
    btnCancelar: TDPSButton;
    GroupBox2: TGroupBox;
    Label38: TLabel;
    GroupBox4: TGroupBox;
    chk_contag: TCheckBox;
    chk_detallepasadas: TCheckBox;
    lblCategoria: TLabel;
    chk_acoplado: TCheckBox;
    Label49: TLabel;
    txt_LargoAdicionalVehiculo: TNumericEdit;
    Label1: TLabel;
    gbInformacionComercial: TGroupBox;
    Label22: TLabel;
    cb_PlanComercial: TComboBox;
    lblInformacion: TLabel;
    Label23: TLabel;
    cb_TipoCliente: TComboBox;
    FDatosVehiculo: TFrameDatosVehiculo;
    cbModelos: TComboBox;
    procedure btnCancelarClick(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
    procedure cb_modeloChange(Sender: TObject);
    procedure chk_acopladoClick(Sender: TObject);
    procedure lblInformacionClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    FCategoria: integer;
    procedure ObtenerDescripcionCategoria;
    Function GetCodigoTipoPatente: AnsiString;
    Function GetPatente: AnsiString;
    Function GetCodigoMarca: integer;
    Function GetDescripcionMarca: AnsiString;
    Function GetCodigoModelo: integer;
    Function GetDescripcionModelo: AnsiString;
    Function GetCodigoCategoria: integer;
    Function GetAnio: integer;
    Function GetCodigoColor: integer;
    Function GetDescripcionColor: AnsiString;
    function GetTieneAcoplado: Boolean;
    Function GetLargoAdicionalVehiculo: integer;
    function GetTipoAdhesion: integer;
    function GetFacturacionDetallada: Boolean;
    function GetDescripcionCategoria: AnsiString;
    function GetDetalleVehiculoSimple: AnsiString;
    function GetDetalleVehiculoCompleta: AnsiString;
    Function GetPlanComercial: integer;
    Procedure SetPlanComercial(Const Value: integer);
    Function GetTipoCliente: integer;
    Procedure SetTipoCliente(Const Value: integer);
    function GetDescripcionPlanComercial: AnsiString;
    function GetDescripcionDatosVehiculoSimple:String;
    function GetDescripcionDatosVehiculoCompleta:String;
    procedure CambioComboMarca(Sender:TObject);
    function GetCodigoVehiculo:Integer;
    { Private declarations }
  public
    { Public declarations }
    Property CodigoTipoPatente: AnsiString read GetCodigoTipoPatente;
    Property Patente: AnsiString read GetPatente;
    property Marca: integer read GetCodigoMarca;
    property DescripcionMarca: AnsiString read GetDescripcionMarca;
    property Modelo: integer read GetCodigoModelo;
    property DescripcionModelo: AnsiString read GetDescripcionModelo;
    property Categoria: integer read GetCodigoCategoria;
    property DescripcionCategoria: AnsiString read GetDescripcionCategoria;
    property Anio: integer read GetAnio;
    property Color: integer read GetCodigoColor;
    property DescripcionColor: AnsiString read GetDescripcionColor;
    property TieneAcoplado: Boolean Read GetTieneAcoplado;
    property LargoAdicionalVehiculo: integer Read GetLargoAdicionalVehiculo;
    property TipoAdhesion: integer Read GetTipoAdhesion;
    property FacturacionDetallada: Boolean Read GetFacturacionDetallada;
    property DetalleVehiculoSimple: Ansistring read GetDetalleVehiculoSimple;
    property DetalleVehiculoCompleto: Ansistring read GetDetalleVehiculoCompleta;
    property PlanComercial: integer read GetPlanComercial;
    property DescripcionPlanComercial: AnsiString read GetDescripcionPlanComercial;
    property TipoCliente: integer read GetTipoCliente;
    property DescripcionDatosVehiculoSimple:String read GetDescripcionDatosVehiculoSimple;
    property DescripcionDatosVehiculoCompleta:String read GetDescripcionDatosVehiculoCompleta;
    property CodigoTipoVehiculo:Integer read GetCodigoVehiculo;

    Function Inicializar: Boolean;  overload; //Para el Alta.
    Function Inicializar(TipoPatente, Patente:AnsiString; CodigoMarca, CodigoModelo:Integer; DescripcionModelo:AnsiString; Anio, CodigoColor: integer;
        TieneAcoplado: Boolean; TipoVehiculo,LargoAdicionalVehiculo: integer; EquipadoConTAG: Boolean;
        TipoCliente, PlanComercial: integer): Boolean; overload;
  end;

var
  FormDatosVehiculo: TFormDatosVehiculo;

implementation

{$R *.dfm}

Function TFormDatosVehiculo.Inicializar(): Boolean;
begin
    result:=false;
    if not(FDatosVehiculo.Inicializar) then exit;
    SetTipoCliente(1);
    CambioComboMarca(cbModelos); // lo ejecuto para que cargue los modelos
    Result := True;
end;

Function TFormDatosVehiculo.Inicializar(TipoPatente, Patente:AnsiString; CodigoMarca, CodigoModelo:Integer; DescripcionModelo:AnsiString; Anio, CodigoColor: integer;
    TieneAcoplado: Boolean; TipoVehiculo, LargoAdicionalVehiculo: integer; EquipadoConTAG: Boolean;
    TipoCliente, PlanComercial: integer): Boolean;
begin
    result:=False;
    if not(FDatosVehiculo.Inicializar(TipoPatente,Patente,'',CodigoMarca,anio,TipoVehiculo,CodigoColor)) then exit;
    CambioComboMarca(cbModelos); // lo ejecuto para que cargue los modelos
    cbModelos.ItemIndex:=CodigoModelo-1;
    if CodigoModelo-1<0 then cbModelos.Text:=DescripcionModelo;

    chk_Acoplado.Checked := TieneAcoplado;
    chk_Acoplado.OnClick(chk_Acoplado);
    txt_LargoAdicionalVehiculo.Value    := LargoAdicionalVehiculo;
    SetTipoCliente(TipoCliente);
    SetPlanComercial(PlanComercial);
    Result := True;
end;

//TIPO DE PATENTE
Function TFormDatosVehiculo.GetCodigoTipoPatente: AnsiString;
begin
    Result := FDatosVehiculo.TipoPatente;
end;

Function TFormDatosVehiculo.GetDescripcionMarca: AnsiString;
begin
    result:=FDatosVehiculo.DescripcionMarca;
end;

Function TFormDatosVehiculo.GetDescripcionModelo: AnsiString;
begin
//    Result:=FDatosVehiculo.DescripcionModelo;
    result:=Trim(cbModelos.Text);
end;

//CATEGORIA
Function TFormDatosVehiculo.GetCodigoCategoria: integer;
begin
    Result := FCategoria;
end;

//ANIO
Function TFormDatosVehiculo.GetAnio: integer;
begin
    Result := FDatosVehiculo.Anio;
end;

//TIPO DE CLIENTE
Function TFormDatosVehiculo.GetTipoCliente: integer;
begin
    Result := Ival(StrRight(cb_TipoCliente.Text, 10));
end;

Procedure TFormDatosVehiculo.SetTipoCliente(Const Value: integer);
begin
    if Value = TipoCliente then Exit;
    CargarTiposCliente(DMConnections.BaseCAC, cb_TipoCliente, Value);
    SetPlanComercial(-1);
end;

//PLAN COMERCIAL
Function TFormDatosVehiculo.GetPlanComercial: integer;
begin
    Result := Ival(StrRight(cb_PlanComercial.Text, 10));
end;

Procedure TFormDatosVehiculo.SetPlanComercial(Const Value: integer);
begin
    if Value = PlanComercial then Exit;
    CargarPlanesComercialesPosibles(DMConnections.BaseCAC, cb_PlanComercial, TipoCliente, TipoAdhesion, Value);
end;

Function TFormDatosVehiculo.GetDescripcionPlanComercial: AnsiString;
begin
    if cb_PlanComercial.ItemIndex < 0 then Result := ''
    else Result := Trim(StrLeft(cb_PlanComercial.Text, 40));
end;


//COLOR.
Function TFormDatosVehiculo.GetCodigoColor: integer;
begin
    Result := FDatosVehiculo.Color;
end;


Function TFormDatosVehiculo.GetDescripcionColor: AnsiString;
begin
    Result:=FDatosVehiculo.DescripcionColor;
end;

//TIENE ACOPLADO.
Function TFormDatosVehiculo.GetTieneAcoplado: Boolean;
begin
    Result := chk_acoplado.Checked;
end;

//LARGO ADICIONAL.
Function TFormDatosVehiculo.GetLargoAdicionalVehiculo: integer;
begin
    if chk_Acoplado.Checked then Result := txt_LargoAdicionalVehiculo.ValueInt
    else Result := 0;
end;

//EQUIPADO CON TAG
Function TFormDatosVehiculo.GetTipoAdhesion: integer;
begin
    Result := iif(chk_contag.Checked, TA_CON_TAG, TA_SIN_TAG);
end;

//IMPRIMIR FACTURACION DETALLADA
Function TFormDatosVehiculo.GetFacturacionDetallada: Boolean;
begin
    Result := chk_detallepasadas.Checked;
end;

//CATEGORIA
Function TFormDatosVehiculo.GetDescripcionCategoria: AnsiString;
begin
    if FCategoria < 1 then Result := ''
    else Result := Trim(BuscarDescripcionCategoriaVehiculo(DMConnections.BaseCAC, Marca, Modelo, FCategoria));
end;

//DETALLES
Function TFormDatosVehiculo.GetDetalleVehiculoSimple: AnsiString;
begin
    Result := DescripcionMarca + ', ' + DescripcionModelo + ', ' + DescripcionColor;
end;

Function TFormDatosVehiculo.GetDetalleVehiculoCompleta: AnsiString;
begin
    Result := DetalleVehiculoSimple + ' Patente: ' + Patente;
end;


procedure TFormDatosVehiculo.btnCancelarClick(Sender: TObject);
begin
    Close;
end;

procedure TFormDatosVehiculo.btnAceptarClick(Sender: TObject);
ResourceString
    CAPTION_VALIDAR_DATOS_VEHICULO      = 'Validar datos del Veh�culo';
    MSG_ERROR_VALIDAR_PATENTE           = 'Debe indicar patente.';
    MSG_ERROR_VALIDAR_FORMATO_PATENTE   = 'La patente especificada es incorrecta.';
    MSG_ERROR_VALIDAR_CATEGORIA         = 'Debe indicar una categor�a.';
    MSG_ERROR_VALIDAR_MARCA             = 'Debe indicar una marca.';
    MSG_ERROR_VALIDAR_MODELO            = 'Debe indicar un modelo.';
    MSG_ERROR_VALIDAR_ANIO              = 'El a�o es incorrecto. ' + #13#10 + 'Debe ser mayor a 1900 y menor al a�o siguiente al actual. ';
    MSG_ERROR_VALIDAR_COLOR             = 'Debe indicar un color.';

begin
    if not(FDatosVehiculo.ValidarDatosVehiculo) then exit;

    ModalResult := mrOK;
end;

procedure TFormDatosVehiculo.ObtenerDescripcionCategoria;
begin
    if (FDatosVehiculo.Marca > -1) and (FDatosVehiculo.Modelo > -1) then begin
          lblCategoria.Caption  := BuscarDescripcionCategoriaVehiculo(DMConnections.BaseCAC, FDatosVehiculo.Marca, FDatosVehiculo.Modelo, FCategoria);
	end else  begin
		lblCategoria.Caption    := '';
        FCategoria              := 0;
    end;
end;



procedure TFormDatosVehiculo.cb_modeloChange(Sender: TObject);
begin
    ObtenerDescripcionCategoria;
end;


procedure TFormDatosVehiculo.chk_acopladoClick(Sender: TObject);
begin
    if chk_acoplado.Checked then begin
        txt_LargoAdicionalVehiculo.Enabled  := true;
        txt_LargoAdicionalVehiculo.Color    := $00FAEBDE;
    end else begin
        txt_LargoAdicionalVehiculo.Enabled  := False;
        txt_LargoAdicionalVehiculo.Color    := clBtnFace;
    end;
end;

procedure TFormDatosVehiculo.lblInformacionClick(Sender: TObject);
var
    f: TFInfoPlanesComerciales;
begin
    Application.CreateForm(TFInfoPlanesComerciales, f);
    if f.Inicializar then f.ShowModal;
    f.Release
end;

function TFormDatosVehiculo.GetDescripcionDatosVehiculoSimple: String;
begin
    if self.Modelo>=1 then result:=ArmarDescripcionDatosVehiculo(self.Marca,self.Modelo,self.Color)
    else result:=ArmarDescripcionDatosVehiculo(self.DescripcionMarca,self.DescripcionModelo,self.DescripcionColor);
end;

function TFormDatosVehiculo.GetCodigoMarca: integer;
begin
    result:=FDatosVehiculo.Marca;
end;

function TFormDatosVehiculo.GetCodigoModelo: integer;
begin
    result:=cbModelos.ItemIndex+1;
end;

function TFormDatosVehiculo.GetPatente: AnsiString;
begin
    result:=FDatosVehiculo.Patente;
end;

function TFormDatosVehiculo.GetDescripcionDatosVehiculoCompleta: String;
begin
    result:=Self.DetalleVehiculoSimple+' Patente: '+self.Patente;
end;


procedure TFormDatosVehiculo.FormShow(Sender: TObject);
begin
    FDatosVehiculo.txt_Patente.SetFocus;
end;

{******************************** Function Header ******************************
Function Name: TFormDatosVehiculo.CambioComboMarca
Author       : dcalani
Date Created : 29/03/2004
Description  : Es el evento que se ejecuta cuando cambia la marca en el frame
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFormDatosVehiculo.CambioComboMarca(Sender: TObject);
Var
	Qry: TADOQuery;
begin
	Qry := TAdoQuery.Create(nil);
	try
		Qry.Connection := DMConnections.BaseCAC;
        // No uso el procedimiento de PeaProcs porque el codigo de modelo esta puesto implicitamnte en
        // el index del combo
		Qry.SQL.Text := 'SELECT * FROM VehiculosModelos WHERE CodigoMarca = '+inttostr(FDatosVehiculo.Marca)+' ORDER BY CodigoMarca,CodigoModelo';
		Qry.Open;
		cbModelos.Clear;
		While not Qry.Eof do begin
			cbModelos.Items.Add(Qry.FieldByName('Descripcion').AsString);
			Qry.Next;
		end;
	finally
		Qry.Close;
		Qry.Free;
	end;
end;

{******************************** Function Header ******************************
Function Name: TFormDatosVehiculo.FormCreate
Author       : dcalani
Date Created : 29/03/2004
Description  : Asigno el evento de cambio de marca al frame
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
procedure TFormDatosVehiculo.FormCreate(Sender: TObject);
begin
    FDatosVehiculo.OnChangeMarca:=CambioComboMarca;
end;

function TFormDatosVehiculo.GetCodigoVehiculo: Integer;
begin
    result:=FDatosVehiculo.CodigoTipo;
end;

end.
