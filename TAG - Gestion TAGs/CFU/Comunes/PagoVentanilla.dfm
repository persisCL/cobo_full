object frmPagoVentanilla: TfrmPagoVentanilla
  Left = 261
  Top = 130
  ActiveControl = edtCodigoFormaPago
  BorderStyle = bsDialog
  Caption = 'Cobro de Comprobante'
  ClientHeight = 406
  ClientWidth = 676
  Color = clBtnFace
  Constraints.MinHeight = 431
  Constraints.MinWidth = 574
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  DesignSize = (
    676
    406)
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 8
    Top = 10
    Width = 657
    Height = 100
  end
  object lbl_FormaPago: TLabel
    Left = 218
    Top = 16
    Width = 71
    Height = 13
    Caption = 'Forma de pago'
  end
  object lbl_Moneda: TLabel
    Left = 423
    Top = 16
    Width = 76
    Height = 13
    Caption = 'Monto a Pagar: '
  end
  object lblFechaDePago: TLabel
    Left = 29
    Top = 60
    Width = 73
    Height = 13
    Caption = 'Fecha de Pago'
  end
  object lblSiDeseaCambiarFechaPago: TLabel
    Left = 126
    Top = 73
    Width = 179
    Height = 27
    AutoSize = False
    Caption = 
      'Si desea cambiar la fecha  con la que se registrar'#225' el pago haga' +
      ' click'
    Transparent = True
    WordWrap = True
  end
  object lblaca: TLabel
    Left = 277
    Top = 85
    Width = 18
    Height = 13
    Cursor = crHandPoint
    Caption = 'ac'#225
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsUnderline]
    ParentFont = False
    OnClick = lblacaClick
  end
  object lbl_CanalDePago: TLabel
    Left = 28
    Top = 16
    Width = 69
    Height = 13
    Caption = 'Canal de pago'
  end
  object lbl_MontoPagado: TLabel
    Left = 538
    Top = 16
    Width = 76
    Height = 13
    Caption = 'Monto Pagado: '
  end
  object Bevel2: TBevel
    Left = 8
    Top = 116
    Width = 657
    Height = 243
  end
  object lblSiDeseaCambiarElMontoPagado: TLabel
    Left = 325
    Top = 73
    Width = 132
    Height = 27
    AutoSize = False
    Caption = 'Si desea cambiar el monto pagado haga click'
    Transparent = True
    WordWrap = True
  end
  object lblAcaMonto: TLabel
    Left = 421
    Top = 85
    Width = 18
    Height = 13
    Cursor = crHandPoint
    Caption = 'ac'#225
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsUnderline]
    ParentFont = False
    OnClick = lblAcaMontoClick
  end
  object btnCerrar: TButton
    Left = 594
    Top = 376
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Cerrar'
    TabOrder = 7
    OnClick = btnCerrarClick
  end
  object neImporte: TNumericEdit
    Left = 422
    Top = 32
    Width = 105
    Height = 21
    Enabled = False
    MaxLength = 10
    TabOrder = 2
    OnKeyPress = neImporteKeyPress
  end
  object deFechaPago: TDateEdit
    Left = 28
    Top = 79
    Width = 97
    Height = 21
    AutoSelect = False
    Enabled = False
    TabOrder = 4
    OnChange = deFechaPagoChange
    Date = -693594.000000000000000000
  end
  object btnAceptar: TButton
    Left = 513
    Top = 376
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Aceptar'
    Default = True
    TabOrder = 6
    OnClick = btnAceptarClick
  end
  object nbOpciones: TNotebook
    Left = 29
    Top = 132
    Width = 623
    Height = 217
    TabOrder = 5
    Visible = False
    object TPage
      Left = 0
      Top = 0
      Caption = 'NoDefinido'
      object Label1: TLabel
        Left = 162
        Top = 79
        Width = 286
        Height = 24
        Caption = 'Entrada de Datos NO Definida.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
    object TPage
      Left = 0
      Top = 0
      Caption = 'Efectivo'
      ExplicitWidth = 0
      ExplicitHeight = 0
      object lblEfectivoTitulo: TLabel
        Left = 158
        Top = 132
        Width = 192
        Height = 13
        Caption = 'Este comprobante se pagar'#225' en efectivo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lblEfectivoVuelto: TLabel
        Left = 179
        Top = 73
        Width = 41
        Height = 13
        Caption = 'Vuelto:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblEfectivoMontoRecibido: TLabel
        Left = 81
        Top = 51
        Width = 140
        Height = 13
        Caption = 'Ingrese Monto Recibido:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object nePagacon: TNumericEdit
        Left = 224
        Top = 45
        Width = 105
        Height = 21
        MaxLength = 10
        TabOrder = 0
        OnChange = nePagaconChange
        OnKeyPress = neImporteKeyPress
      end
      object neCambio: TNumericEdit
        Left = 224
        Top = 69
        Width = 105
        Height = 21
        MaxLength = 10
        ReadOnly = True
        TabOrder = 1
      end
    end
    object TPage
      Left = 0
      Top = 0
      Caption = 'Tarjetas'
      ExplicitWidth = 0
      ExplicitHeight = 0
      object lblTarjetaAutorizacion: TLabel
        Left = 102
        Top = 67
        Width = 75
        Height = 13
        Caption = 'Autorizaci'#243'n:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblTarjetaCupon: TLabel
        Left = 102
        Top = 107
        Width = 41
        Height = 13
        Caption = 'Cup'#243'n:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblTarjetaTitulo: TLabel
        Left = 80
        Top = 24
        Width = 349
        Height = 14
        Caption = 'Complete los siguientes datos para completar y registrar el pago'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblTarjetaCuotas: TLabel
        Left = 102
        Top = 146
        Width = 44
        Height = 13
        Caption = 'Cuotas:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object edAutorizacion: TEdit
        Left = 199
        Top = 62
        Width = 208
        Height = 21
        CharCase = ecUpperCase
        Color = 16444382
        MaxLength = 20
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
      end
      object edCupon: TNumericEdit
        Left = 199
        Top = 102
        Width = 129
        Height = 21
        Color = 16444382
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        OnKeyPress = neImporteKeyPress
      end
      object edCuotas: TNumericEdit
        Left = 199
        Top = 142
        Width = 129
        Height = 21
        Color = 16444382
        TabOrder = 2
        OnKeyPress = neImporteKeyPress
        Value = 1.000000000000000000
      end
    end
    object TPage
      Left = 0
      Top = 0
      Caption = 'Cheque'
      object lblChequeNumero: TLabel
        Left = 13
        Top = 131
        Width = 76
        Height = 13
        Caption = '&Nro. Cheque:'
        Color = clBtnFace
        FocusControl = txtChequeNumero
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object lblChequeBanco: TLabel
        Left = 13
        Top = 107
        Width = 41
        Height = 13
        Caption = '&Banco:'
        Color = clBtnFace
        FocusControl = cbChequeBancos
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object lblChequeTitulo: TLabel
        Left = 80
        Top = 24
        Width = 349
        Height = 14
        Caption = 'Complete los siguientes datos para completar y registrar el pago'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblChequeRUT: TLabel
        Left = 13
        Top = 84
        Width = 31
        Height = 13
        Caption = 'RUT:'
        FocusControl = txtChequeDocumento
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblChequeTitular: TLabel
        Left = 13
        Top = 61
        Width = 41
        Height = 13
        Caption = 'Titular:'
        FocusControl = txtChequeNombreTitular
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblChequeFecha: TLabel
        Left = 13
        Top = 156
        Width = 87
        Height = 13
        Caption = 'Fecha Cheque:'
        Color = clBtnFace
        FocusControl = txtChequeNumero
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object btnDatosClienteCheque: TSpeedButton
        Left = 490
        Top = 57
        Width = 22
        Height = 21
        Hint = 'Copia los datos del Cliente comprobante'
        Glyph.Data = {
          AA040000424DAA04000000000000360000002800000013000000130000000100
          1800000000007404000000000000000000000000000000000000D8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9
          ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9
          ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
          D8E9EC840000840000840000840000840000840000840000840000840000D8E9
          ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC84
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000D8E9ECD8E9EC
          D8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC840000FFFF
          FF000000000000000000000000000000FFFFFF840000D8E9ECD8E9ECD8E9EC00
          0000D8E9EC000000000000000000000000000000000000840000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9
          EC000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000FFFFFF00000000000000
          0000000000000000FFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000
          FFFFFF000000000000000000000000840000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFF840000FFFFFF000000000000FFFFFF840000840000
          840000840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFF0000000000
          00000000000000840000FFFFFFFFFFFFFFFFFFFFFFFF840000FFFFFF840000D8
          E9ECD8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF840000FFFFFFFFFFFFFFFFFFFFFFFF840000840000D8E9ECD8E9ECD8E9
          ECD8E9ECD8E9EC000000D8E9EC000000FFFFFF000000000000FFFFFF00000084
          0000840000840000840000840000840000D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
          D8E9EC000000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF0000
          00D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC00
          0000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF000000000000D8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9
          EC000000000000000000000000000000000000D8E9ECD8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9
          ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9
          ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9EC000000}
        ParentShowHint = False
        ShowHint = True
        OnClick = btnDatosClienteChequeClick
      end
      object Label2: TLabel
        Left = 13
        Top = 179
        Width = 105
        Height = 13
        Caption = 'Nro. Repactaci'#243'n:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object txtChequeNumero: TEdit
        Left = 126
        Top = 128
        Width = 238
        Height = 21
        CharCase = ecUpperCase
        Color = clWhite
        MaxLength = 20
        TabOrder = 3
      end
      object cbChequeBancos: TVariantComboBox
        Left = 126
        Top = 104
        Width = 237
        Height = 21
        Style = vcsDropDownList
        Color = clWhite
        ItemHeight = 0
        TabOrder = 2
        Items = <>
      end
      object txtChequeNombreTitular: TEdit
        Left = 126
        Top = 57
        Width = 361
        Height = 21
        CharCase = ecUpperCase
        Color = clWhite
        MaxLength = 100
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
      end
      object txtChequeDocumento: TEdit
        Left = 126
        Top = 81
        Width = 108
        Height = 21
        Hint = 'RUT'
        CharCase = ecUpperCase
        Color = clWhite
        MaxLength = 9
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object deChequeFecha: TDateEdit
        Left = 126
        Top = 152
        Width = 90
        Height = 21
        AutoSelect = False
        TabOrder = 4
        Date = -693594.000000000000000000
      end
      object edtRepactacionCheque: TEdit
        Left = 126
        Top = 176
        Width = 238
        Height = 21
        TabOrder = 5
      end
    end
    object TPage
      Left = 0
      Top = 0
      Caption = 'Vale Vista'
      object lblValeVistaTitulo: TLabel
        Left = 80
        Top = 24
        Width = 349
        Height = 14
        Caption = 'Complete los siguientes datos para completar y registrar el pago'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblValeVistaTomadoPor: TLabel
        Left = 13
        Top = 61
        Width = 72
        Height = 13
        Caption = 'Tomado por:'
        FocusControl = txt_TomadoPorVV
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblValeVistaRUT: TLabel
        Left = 13
        Top = 84
        Width = 31
        Height = 13
        Caption = 'RUT:'
        FocusControl = txt_RUTVV
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblValeVistaBanco: TLabel
        Left = 13
        Top = 107
        Width = 41
        Height = 13
        Caption = 'Banco:'
        FocusControl = cb_BancosVV
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblValeVistaNumero: TLabel
        Left = 13
        Top = 131
        Width = 90
        Height = 13
        Caption = 'Nro. Vale Vista:'
        FocusControl = txt_NroVV
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblValeVistaFecha: TLabel
        Left = 13
        Top = 156
        Width = 101
        Height = 13
        Caption = 'Fecha Vale Vista:'
        FocusControl = de_FechaVV
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object btn_DatosClienteValeVista: TSpeedButton
        Left = 480
        Top = 57
        Width = 22
        Height = 21
        Hint = 'Copia los datos del Cliente comprobante'
        Glyph.Data = {
          AA040000424DAA04000000000000360000002800000013000000130000000100
          1800000000007404000000000000000000000000000000000000D8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9
          ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9
          ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
          D8E9EC840000840000840000840000840000840000840000840000840000D8E9
          ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC84
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000D8E9ECD8E9EC
          D8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC840000FFFF
          FF000000000000000000000000000000FFFFFF840000D8E9ECD8E9ECD8E9EC00
          0000D8E9EC000000000000000000000000000000000000840000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9
          EC000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000FFFFFF00000000000000
          0000000000000000FFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000
          FFFFFF000000000000000000000000840000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFF840000FFFFFF000000000000FFFFFF840000840000
          840000840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFF0000000000
          00000000000000840000FFFFFFFFFFFFFFFFFFFFFFFF840000FFFFFF840000D8
          E9ECD8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF840000FFFFFFFFFFFFFFFFFFFFFFFF840000840000D8E9ECD8E9ECD8E9
          ECD8E9ECD8E9EC000000D8E9EC000000FFFFFF000000000000FFFFFF00000084
          0000840000840000840000840000840000D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
          D8E9EC000000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF0000
          00D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC00
          0000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF000000000000D8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9
          EC000000000000000000000000000000000000D8E9ECD8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9
          ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9
          ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9EC000000}
        OnClick = btn_DatosClienteValeVistaClick
      end
      object txt_TomadoPorVV: TEdit
        Left = 116
        Top = 57
        Width = 361
        Height = 21
        TabOrder = 0
      end
      object txt_RUTVV: TEdit
        Left = 116
        Top = 81
        Width = 108
        Height = 21
        TabOrder = 1
      end
      object txt_NroVV: TEdit
        Left = 116
        Top = 128
        Width = 238
        Height = 21
        TabOrder = 3
      end
      object cb_BancosVV: TVariantComboBox
        Left = 116
        Top = 104
        Width = 237
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 0
        TabOrder = 2
        Items = <>
      end
      object de_FechaVV: TDateEdit
        Left = 116
        Top = 152
        Width = 90
        Height = 21
        AutoSelect = False
        TabOrder = 4
        Date = -693594.000000000000000000
      end
    end
    object TPage
      Left = 0
      Top = 0
      Caption = 'DebitoCuentaPAC'
      object lblDebitoCuentaPACTitulo: TLabel
        Left = 202
        Top = 24
        Width = 105
        Height = 14
        Caption = 'D'#233'bito Cuenta PAC'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblDebitoCuentaPACBanco: TLabel
        Left = 63
        Top = 61
        Width = 41
        Height = 13
        Caption = 'Banco:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblDebitoCuentaPACNumeroCuenta: TLabel
        Left = 63
        Top = 85
        Width = 117
        Height = 13
        Caption = 'N'#186' Cuenta Bancaria:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblDebitoCuentaPACSucursal: TLabel
        Left = 63
        Top = 109
        Width = 54
        Height = 13
        Caption = 'Sucursal:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblDebitoCuentaPACTitular: TLabel
        Left = 63
        Top = 133
        Width = 41
        Height = 13
        Caption = 'Titular:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object txt_NroCuentaBancariaPAC: TEdit
        Left = 193
        Top = 81
        Width = 121
        Height = 21
        TabOrder = 1
      end
      object txt_SucursalPAC: TEdit
        Left = 193
        Top = 105
        Width = 253
        Height = 21
        TabOrder = 2
      end
      object txt_TitularPAC: TEdit
        Left = 193
        Top = 129
        Width = 253
        Height = 21
        TabOrder = 3
      end
      object cb_BancosPAC: TVariantComboBox
        Left = 193
        Top = 57
        Width = 253
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 0
        TabOrder = 0
        Items = <>
      end
    end
    object TPage
      Left = 0
      Top = 0
      Caption = 'DebitoCuentaPAT'
      object lblDebitoCuentaPATTitulo: TLabel
        Left = 202
        Top = 24
        Width = 106
        Height = 14
        Caption = 'D'#233'bito Cuenta PAT'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblDebitoCuentaPATTipoTarjeta: TLabel
        Left = 33
        Top = 61
        Width = 118
        Height = 13
        Caption = 'Tipo Tarjeta Cr'#233'dito:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblDebitoCuentaPATNumeroTarjeta: TLabel
        Left = 33
        Top = 85
        Width = 107
        Height = 13
        Caption = 'N'#186' Tarjeta Cr'#233'dito:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblDebitoCuentaPATFechaVencimiento: TLabel
        Left = 33
        Top = 109
        Width = 167
        Height = 13
        Caption = 'Fecha Vencimiento (MM/AA):'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblDebitoCuentaPATEmisorTarjeta: TLabel
        Left = 33
        Top = 133
        Width = 130
        Height = 13
        Caption = 'Emisor Tarjeta Cr'#233'dito:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object txt_NumeroTarjetaCreditoPAT: TEdit
        Left = 202
        Top = 81
        Width = 158
        Height = 21
        MaxLength = 20
        TabOrder = 1
      end
      object cb_TipoTarjetaCreditoPAT: TVariantComboBox
        Left = 202
        Top = 57
        Width = 159
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 0
        TabOrder = 0
        Items = <>
      end
      object cb_EmisorTarjetaCreditoPAT: TVariantComboBox
        Left = 202
        Top = 129
        Width = 246
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 0
        TabOrder = 2
        Items = <>
      end
      object txt_FechaVencimiento: TMaskEdit
        Left = 202
        Top = 104
        Width = 36
        Height = 21
        EditMask = '99\/99;1;_'
        MaxLength = 5
        TabOrder = 3
        Text = '  /  '
      end
    end
    object TPage
      Left = 0
      Top = 0
      Caption = 'TarjetaCredito'
      object lblTarjetaCreditoTitulo: TLabel
        Left = 80
        Top = 24
        Width = 349
        Height = 14
        Caption = 'Complete los siguientes datos para completar y registrar el pago'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object spbTarjetaCreditoCopiarDatosCliente: TSpeedButton
        Left = 494
        Top = 57
        Width = 22
        Height = 22
        Glyph.Data = {
          AA040000424DAA04000000000000360000002800000013000000130000000100
          1800000000007404000000000000000000000000000000000000D8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9
          ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9
          ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
          D8E9EC840000840000840000840000840000840000840000840000840000D8E9
          ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC84
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000D8E9ECD8E9EC
          D8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC840000FFFF
          FF000000000000000000000000000000FFFFFF840000D8E9ECD8E9ECD8E9EC00
          0000D8E9EC000000000000000000000000000000000000840000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9
          EC000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000FFFFFF00000000000000
          0000000000000000FFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000
          FFFFFF000000000000000000000000840000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFF840000FFFFFF000000000000FFFFFF840000840000
          840000840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFF0000000000
          00000000000000840000FFFFFFFFFFFFFFFFFFFFFFFF840000FFFFFF840000D8
          E9ECD8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF840000FFFFFFFFFFFFFFFFFFFFFFFF840000840000D8E9ECD8E9ECD8E9
          ECD8E9ECD8E9EC000000D8E9EC000000FFFFFF000000000000FFFFFF00000084
          0000840000840000840000840000840000D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
          D8E9EC000000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF0000
          00D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC00
          0000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF000000000000D8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9
          EC000000000000000000000000000000000000D8E9ECD8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9
          ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9
          ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9EC000000}
        OnClick = spbTarjetaCreditoCopiarDatosClienteClick
      end
      object lblTarjetaCreditoTitular: TLabel
        Left = 13
        Top = 60
        Width = 41
        Height = 13
        Caption = 'Titular:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblTarjetaCreditoTarjeta: TLabel
        Left = 13
        Top = 107
        Width = 45
        Height = 13
        Caption = 'Tarjeta:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblTarjetaCreditoNroConfirmacion: TLabel
        Left = 13
        Top = 129
        Width = 106
        Height = 13
        Caption = 'Nro. Confirmaci'#243'n:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblTarjetaCreditoBanco: TLabel
        Left = 13
        Top = 83
        Width = 41
        Height = 13
        Caption = 'Banco:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblTarjetaCreditoCuotas: TLabel
        Left = 13
        Top = 152
        Width = 44
        Height = 13
        Caption = 'Cuotas:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object edtTarjetaCreditoTitular: TEdit
        Left = 127
        Top = 57
        Width = 361
        Height = 21
        TabOrder = 0
      end
      object vcbTarjetaCreditoTarjetas: TVariantComboBox
        Left = 127
        Top = 103
        Width = 245
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 0
        TabOrder = 2
        Items = <>
      end
      object edtTarjetaCreditoNroConfirmacion: TEdit
        Left = 127
        Top = 126
        Width = 245
        Height = 21
        TabOrder = 3
        OnKeyPress = edtTarjetaCreditoNroConfirmacionKeyPress
      end
      object vcbTarjetaCreditoBancos: TVariantComboBox
        Left = 127
        Top = 80
        Width = 245
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 0
        TabOrder = 1
        Items = <>
      end
      object nedtTarjetaCreditoCuotas: TNumericEdit
        Left = 127
        Top = 149
        Width = 121
        Height = 21
        TabOrder = 4
        Value = 1.000000000000000000
      end
    end
    object TPage
      Left = 0
      Top = 0
      Caption = 'TarjetaDebito'
      object lblTarjetaDebitoTitulo: TLabel
        Left = 80
        Top = 24
        Width = 349
        Height = 14
        Caption = 'Complete los siguientes datos para completar y registrar el pago'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblTarjetaDebitoTitular: TLabel
        Left = 13
        Top = 60
        Width = 41
        Height = 13
        Caption = 'Titular:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblTarjetaDebitoBanco: TLabel
        Left = 13
        Top = 83
        Width = 41
        Height = 13
        Caption = 'Banco:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblTarjetaDebitoNroConfirmacion: TLabel
        Left = 13
        Top = 106
        Width = 106
        Height = 13
        Caption = 'Nro. Confirmaci'#243'n:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object spbTarjetaDebitoCopiarDatosCliente: TSpeedButton
        Left = 494
        Top = 56
        Width = 22
        Height = 22
        Glyph.Data = {
          AA040000424DAA04000000000000360000002800000013000000130000000100
          1800000000007404000000000000000000000000000000000000D8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9
          ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9
          ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
          D8E9EC840000840000840000840000840000840000840000840000840000D8E9
          ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC84
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000D8E9ECD8E9EC
          D8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC840000FFFF
          FF000000000000000000000000000000FFFFFF840000D8E9ECD8E9ECD8E9EC00
          0000D8E9EC000000000000000000000000000000000000840000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9
          EC000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000FFFFFF00000000000000
          0000000000000000FFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000
          FFFFFF000000000000000000000000840000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFF840000FFFFFF000000000000FFFFFF840000840000
          840000840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFF0000000000
          00000000000000840000FFFFFFFFFFFFFFFFFFFFFFFF840000FFFFFF840000D8
          E9ECD8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF840000FFFFFFFFFFFFFFFFFFFFFFFF840000840000D8E9ECD8E9ECD8E9
          ECD8E9ECD8E9EC000000D8E9EC000000FFFFFF000000000000FFFFFF00000084
          0000840000840000840000840000840000D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
          D8E9EC000000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF0000
          00D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC00
          0000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF000000000000D8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9
          EC000000000000000000000000000000000000D8E9ECD8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9
          ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9
          ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9EC000000}
        OnClick = spbTarjetaDebitoCopiarDatosClienteClick
      end
      object edtTarjetaDebitoTitular: TEdit
        Left = 127
        Top = 57
        Width = 361
        Height = 21
        TabOrder = 0
      end
      object edtTarjetaDebitoNroConfirmacion: TEdit
        Left = 127
        Top = 103
        Width = 245
        Height = 21
        TabOrder = 2
      end
      object vcbTarjetaDebitoBancos: TVariantComboBox
        Left = 127
        Top = 80
        Width = 245
        Height = 21
        Style = vcsDropDownList
        ItemHeight = 0
        TabOrder = 1
        Items = <>
      end
    end
    object TPage
      Left = 0
      Top = 0
      Caption = 'Pagare'
      object lblPagareNumero: TLabel
        Left = 13
        Top = 131
        Width = 29
        Height = 13
        Caption = '&Nro.:'
        Color = clBtnFace
        FocusControl = txtPagareNumero
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object lblPagareBanco: TLabel
        Left = 13
        Top = 107
        Width = 69
        Height = 13
        Caption = '&Documento:'
        Color = clBtnFace
        FocusControl = cbPagareBancos
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object lblPagareTitulo: TLabel
        Left = 80
        Top = 24
        Width = 349
        Height = 14
        Caption = 'Complete los siguientes datos para completar y registrar el pago'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblPagareRUT: TLabel
        Left = 13
        Top = 84
        Width = 31
        Height = 13
        Caption = 'RUT:'
        FocusControl = txtPagareDocumento
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblPagareTitular: TLabel
        Left = 13
        Top = 60
        Width = 41
        Height = 13
        Caption = 'Titular:'
        FocusControl = txtPagareNombreTitular
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblPagareFecha: TLabel
        Left = 13
        Top = 156
        Width = 40
        Height = 13
        Caption = 'Fecha:'
        Color = clBtnFace
        FocusControl = dePagareFecha
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object btnDatosClientePagare: TSpeedButton
        Left = 498
        Top = 57
        Width = 22
        Height = 21
        Hint = 'Copia los datos del Cliente comprobante'
        Glyph.Data = {
          AA040000424DAA04000000000000360000002800000013000000130000000100
          1800000000007404000000000000000000000000000000000000D8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9
          ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9
          ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
          D8E9EC840000840000840000840000840000840000840000840000840000D8E9
          ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC84
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000D8E9ECD8E9EC
          D8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC840000FFFF
          FF000000000000000000000000000000FFFFFF840000D8E9ECD8E9ECD8E9EC00
          0000D8E9EC000000000000000000000000000000000000840000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9
          EC000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000FFFFFF00000000000000
          0000000000000000FFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000
          FFFFFF000000000000000000000000840000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFF840000FFFFFF000000000000FFFFFF840000840000
          840000840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFF0000000000
          00000000000000840000FFFFFFFFFFFFFFFFFFFFFFFF840000FFFFFF840000D8
          E9ECD8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF840000FFFFFFFFFFFFFFFFFFFFFFFF840000840000D8E9ECD8E9ECD8E9
          ECD8E9ECD8E9EC000000D8E9EC000000FFFFFF000000000000FFFFFF00000084
          0000840000840000840000840000840000D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
          D8E9EC000000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF0000
          00D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC00
          0000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF000000000000D8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9
          EC000000000000000000000000000000000000D8E9ECD8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9
          ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
          D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9
          ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8
          E9ECD8E9ECD8E9ECD8E9EC000000}
        ParentShowHint = False
        ShowHint = True
        OnClick = btnDatosClientePagareClick
      end
      object Label9: TLabel
        Left = 13
        Top = 179
        Width = 105
        Height = 13
        Caption = 'Nro. Repactaci'#243'n:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object txtPagareNumero: TEdit
        Left = 134
        Top = 128
        Width = 238
        Height = 21
        CharCase = ecUpperCase
        Color = clWhite
        MaxLength = 20
        TabOrder = 3
      end
      object cbPagareBancos: TVariantComboBox
        Left = 134
        Top = 104
        Width = 237
        Height = 21
        Style = vcsDropDownList
        Color = clWhite
        ItemHeight = 0
        TabOrder = 2
        Items = <>
      end
      object txtPagareNombreTitular: TEdit
        Left = 134
        Top = 57
        Width = 361
        Height = 21
        CharCase = ecUpperCase
        Color = clWhite
        MaxLength = 100
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
      end
      object txtPagareDocumento: TEdit
        Left = 134
        Top = 81
        Width = 108
        Height = 21
        Hint = 'RUT'
        CharCase = ecUpperCase
        Color = clWhite
        MaxLength = 9
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object dePagareFecha: TDateEdit
        Left = 134
        Top = 152
        Width = 90
        Height = 21
        AutoSelect = False
        TabOrder = 4
        Date = -693594.000000000000000000
      end
      object edtRepactacion: TEdit
        Left = 134
        Top = 176
        Width = 238
        Height = 21
        TabOrder = 5
      end
    end
    object TPage
      Left = 0
      Top = 0
      Caption = 'Cheques'
      ExplicitWidth = 0
      ExplicitHeight = 0
      object lblSiDeseaIncluirUnCheque: TLabel
        Left = 8
        Top = 197
        Width = 289
        Height = 13
        Caption = 'Si desea incluir un cheque haga click sobre el bot'#243'n agregar.'
      end
      object lbltitulototal: TLabel
        Left = 248
        Top = 168
        Width = 27
        Height = 13
        Caption = 'Total:'
      end
      object lblTotal: TLabel
        Left = 354
        Top = 168
        Width = 6
        Height = 13
        Alignment = taRightJustify
        Caption = '0'
      end
      object DBItemsCheque: TDBListEx
        Left = 8
        Top = 14
        Width = 409
        Height = 147
        BorderStyle = bsSingle
        Columns = <
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 70
            Header.Caption = 'Titular'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Titular'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 70
            Header.Caption = 'Rut'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'RUT'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 50
            Header.Caption = 'Banco'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'Banco'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 55
            Header.Caption = 'Cheque'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taRightJustify
            IsLink = False
            FieldName = 'NumeroCheque'
          end
          item
            Alignment = taLeftJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 70
            Header.Caption = 'F. Cheque'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            IsLink = False
            FieldName = 'FechaCheque'
          end
          item
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Width = 50
            Header.Caption = 'Monto'
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -12
            Header.Font.Name = 'Tahoma'
            Header.Font.Style = []
            Header.Alignment = taRightJustify
            IsLink = False
            FieldName = 'Monto'
          end>
        DataSource = DsCheques
        DragReorder = False
        ParentColor = False
        TabOrder = 0
        TabStop = True
      end
      object BtnAgregarItem: TButton
        Left = 424
        Top = 14
        Width = 75
        Height = 25
        Caption = '&Agregar'
        TabOrder = 1
        OnClick = BtnAgregarItemClick
      end
      object BtnEliminarItem: TButton
        Left = 424
        Top = 38
        Width = 75
        Height = 25
        Caption = '&Eliminar'
        TabOrder = 2
        OnClick = BtnEliminarItemClick
      end
    end
  end
  object cb_CanalesDePago: TVariantComboBox
    Left = 28
    Top = 32
    Width = 180
    Height = 21
    Style = vcsDropDownList
    ItemHeight = 13
    TabOrder = 0
    OnChange = cb_CanalesDePagoChange
    Items = <>
  end
  object neImportePagado: TNumericEdit
    Left = 538
    Top = 32
    Width = 105
    Height = 21
    TabStop = False
    Enabled = False
    MaxLength = 10
    TabOrder = 3
    OnChange = neImportePagadoChange
    OnKeyPress = neImporteKeyPress
  end
  object edtCodigoFormaPago: TEdit
    Left = 218
    Top = 32
    Width = 34
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 2
    TabOrder = 1
    OnChange = edtCodigoFormaPagoChange
  end
  object edtDescFormaPago: TEdit
    Left = 253
    Top = 32
    Width = 158
    Height = 21
    TabStop = False
    ReadOnly = True
    TabOrder = 8
  end
  object chkNoCalcularIntereses: TCheckBox
    Left = 470
    Top = 73
    Width = 185
    Height = 25
    TabStop = False
    Caption = 'No calcular intereses en la pr'#243'xima facturaci'#243'n'
    TabOrder = 9
    WordWrap = True
    OnClick = chkNoCalcularInteresesClick
  end
  object CrearRecibo: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CrearRecibo'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaHora'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@Importe'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroRecibo'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoMedioPago'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 77
    Top = 366
  end
  object CerrarPagoComprobante: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CerrarPagoComprobante'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@FechaHora'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 109
    Top = 366
  end
  object sp_RegistrarDetallePagoComprobante: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorLocation = clUseServer
    LockType = ltBatchOptimistic
    CommandTimeout = 60
    ProcedureName = 'RegistrarDetallePagoComprobante;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@IDDetallePagoComprobante'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoMedioPago'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoFormaPago'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@PAC_CodigoBanco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PAC_CodigoTipoCuentaBancaria'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@PAC_NroCuentaBancaria'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@PAC_Sucursal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@PAC_Titular'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@PAT_CodigoTipoTarjetaCredito'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@PAT_NumeroTarjetaCredito'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@PAT_FechaVencimiento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 5
        Value = Null
      end
      item
        Name = '@PAT_CodigoEmisorTarjetaCredito'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@PAT_Titular'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@ChequeCodigoBanco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ChequeNumero'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@ChequeTitular'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@ChequeFecha'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@ChequeCodigoDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@ChequeNumeroDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@ChequeMonto'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@Cupon'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Autorizacion'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@Cuotas'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@NumeroPOS'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@Web_NumeroFinalTarjetaCredito'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@Web_TipoVenta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@Titular'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@CodigoTipoTarjeta'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoBancoEmisor'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@RepactacionNumero'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 13
    Top = 366
  end
  object sp_RegistrarPagoComprobante: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorLocation = clUseServer
    LockType = ltBatchOptimistic
    CommandTimeout = 60
    ProcedureName = 'RegistrarPagoComprobante;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroPago'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@FechaHora'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@Importe'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@EstadoPago'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@NumeroRecibo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@IDDetallePagoComprobante'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoConceptoPago'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@DescripcionPago'
        Attributes = [paNullable]
        DataType = ftString
        Size = 60
        Value = Null
      end
      item
        Name = '@PagarSoloComprobanteSolicitado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NoGenerarIntereses'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 45
    Top = 366
  end
  object RegistrarPagoNotaCobroInfraccion: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'RegistrarPagoNotaCobroInfraccion;1'
    Parameters = <>
    Left = 141
    Top = 366
  end
  object cdsCheques: TClientDataSet
    Active = True
    Aggregates = <>
    AutoCalcFields = False
    FieldDefs = <
      item
        Name = 'Titular'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'RUT'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Banco'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'NumeroCheque'
        DataType = ftInteger
      end
      item
        Name = 'FechaCheque'
        DataType = ftDate
      end
      item
        Name = 'Monto'
        DataType = ftInteger
      end
      item
        Name = 'CodigoBanco'
        DataType = ftInteger
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 181
    Top = 365
    Data = {
      B30000009619E0BD010000001800000007000000000003000000B30007546974
      756C617201004900000001000557494454480200020014000352555401004900
      000001000557494454480200020014000542616E636F01004900000001000557
      494454480200020014000C4E756D65726F43686571756504000100000000000B
      46656368614368657175650400060000000000054D6F6E746F04000100000000
      000B436F6469676F42616E636F04000100000000000000}
  end
  object DsCheques: TDataSource
    DataSet = cdsCheques
    Left = 216
    Top = 368
  end
  object spValidarFormaDePagoEnCanal: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ValidarFormaDePagoEnCanal'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTipoMedioPago'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoEntradaUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@ValidarPagoAnticipado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoUsuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@CodigoSistema'
        Attributes = [paNullable]
        DataType = ftWord
        Precision = 3
        Value = Null
      end
      item
        Name = '@CodigoFormaDePago'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@DescripcionFormaDePago'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 30
        Value = Null
      end
      item
        Name = '@EsValidaEnCanal'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@AceptaPagoParcial'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPantalla'
        Attributes = [paNullable]
        DataType = ftSmallint
        Direction = pdInputOutput
        Precision = 5
        Value = Null
      end
      item
        Name = '@CodigoBanco'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTarjeta'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 316
    Top = 368
  end
  object spActualizarInfraccionesPagadas: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarInfraccionesPagadas'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end>
    Left = 384
    Top = 368
  end
  object ADOStoredProc1: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarInfraccionesPagadas'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroComprobante'
        Attributes = [paNullable]
        DataType = ftLargeint
        Precision = 19
        Value = Null
      end
      item
        Name = '@TipoComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end>
    Left = 352
    Top = 368
  end
  object sp_ActualizarClienteMoroso: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ActualizarClienteMoroso'
    Parameters = <
      item
        Name = '@NumeroDocumento'
        DataType = ftString
        Size = 11
        Value = Null
      end
      item
        Name = '@CodigoRefinanciacion'
        DataType = ftInteger
        Value = Null
      end
      item
        Name = '@Usuario'
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@ErrorDescription'
        DataType = ftString
        Direction = pdOutput
        Size = -1
        Value = Null
      end>
    Left = 424
    Top = 368
  end
end
