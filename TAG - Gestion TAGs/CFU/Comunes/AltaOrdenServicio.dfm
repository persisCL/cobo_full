object FormAltaOrdenServicio: TFormAltaOrdenServicio
  Left = 47
  Top = 115
  BorderStyle = bsDialog
  Caption = 'Nueva Orden de Servicio'
  ClientHeight = 513
  ClientWidth = 807
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  inline FrameAltaOSMultiple1: TFrameAltaOSMultiple
    Left = 0
    Top = 0
    Width = 807
    Height = 513
    HorzScrollBar.Visible = False
    VertScrollBar.Visible = False
    Align = alClient
    AutoScroll = False
    Constraints.MinHeight = 510
    Constraints.MinWidth = 750
    TabOrder = 0
    inherited Panel1: TPanel
      Width = 807
      Height = 513
      inherited pnlBotones: TPanel
        Top = 473
        Width = 807
        inherited Panel3: TPanel
          Left = 530
        end
      end
      inherited PageControl: TPageControl
        Width = 807
        Height = 473
        inherited tab_cuentas: TTabSheet
          inherited Panel2: TPanel
            Width = 799
            Height = 463
            inherited pc_Adhesion: TPageControl
              Width = 799
              Height = 463
              ActivePage = FrameAltaOSMultiple1.ts_Vehiculos
              TabIndex = 1
              inherited ts_Vehiculos: TTabSheet
                inherited pnlVehiculos: TPanel
                  Width = 791
                  Height = 435
                end
              end
              inherited ts_Contrato: TTabSheet
                inherited DPSButton1: TDPSButton
                  Left = 3417
                end
                inherited DPSButton2: TDPSButton
                  Left = 3417
                end
              end
            end
          end
        end
      end
    end
  end
end
