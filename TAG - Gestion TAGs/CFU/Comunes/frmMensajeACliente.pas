unit frmMensajeACliente;

interface

{
    Firma           : SS_1408_MCA_20151027
    Descripcion     : nuevo formulario que muestra un mensaje al cliente.
}
uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, jpeg, ExtCtrls, StdCtrls;

type
  TFormMensajeACliente = class(TForm)
    btnCerrar: TButton;
    pnl1: TPanel;
    lblMensajeError: TLabel;
    imgAdvertencia: TImage;
    procedure btnCerrarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function Inicializar(Mensaje: AnsiString): Boolean;
  end;

var
  FormMensajeACliente: TFormMensajeACliente;

implementation

{$R *.dfm}

function TFormMensajeACliente.Inicializar(Mensaje: AnsiString): Boolean;
begin
    lblMensajeError.Caption := Mensaje;
    Result:= True;
end;

procedure TFormMensajeACliente.btnCerrarClick(Sender: TObject);
begin
    Close;
end;

end.
