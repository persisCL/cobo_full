{********************************** Unit Header ********************************
File Name : BuscaClientes.pas
Author :
Date Created:
Language : ES-AR
Description :

Revision : 1
    Author : ggomez
    Date : 16/11/2006
    Description : Cambi� la forma de realizar la b�squeda. Ya no se usa un timer,
        ahora se usa un bot�n Buscar. Hasta que no se presiona el bot�n buscar,
        no se realiza la b�squeda de datos.

Revision : 2
    Author : ggomez
    Date : 16/11/2006
    Description : Cambi� para que habilite el Apellido Materno y el Nombre,
        salvo que la personer�a seleccionada por el operador sea Jur�dica.

Revision : 3
    Author : ggomez
    Date : 16/11/2006
    Description :
        - Modifiqu� para que el bot�n por defecto sea Buscar en lugar del bot�n Aceptar.
        - Agregu� un mensaje que se muestra en el caso de que no se obtengan
        datos al realizar la b�squeda.

    Firma       :   SS_1224_MCA_20141103
    Descripcion : se agrega nueva columna Fecha Alta Cuenta
*******************************************************************************}
unit BuscaClientes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, DbList, Db, DBTables, Util, DmiCtrls, UtilProc,
  DMConnection, ADODB, Variants, peaprocs, DPSControls, PeaTypes, DBClient, ListBoxEx,
  DBListEx{, FrmDatosContactoComunicacion};

type
  TBusquedaCliente = record
  	Personeria		: integer;
    RUT				: string;
    Patente		 	: string;
    Apellido		: string;
    ApellidoMaterno : string;
    Nombre			: string;
  end;

  TFormBuscaClientes = class(TForm)
    pnlDeAbajo: TPanel;
    pnlBotones: TPanel;
    BuscarClienteContacto: TADOStoredProc;
    gbFiltros: TGroupBox;
    lblRUT: TLabel;
    txt_rut: TEdit;
    cbPersoneria: TComboBox;
    lblPersoneria: TLabel;
    lblApellido: TLabel;
    txt_apellido: TEdit;
    lbl_apellidoMaterno: TLabel;
    txt_apellidoMaterno: TEdit;
    lbl_nombre: TLabel;
    txt_nombre: TEdit;
    lblPatente: TLabel;
    txt_Patente: TEdit;
    dbl_Personas: TDBListEx;
    dsBuscarClienteContacto: TDataSource;
    bt_aceptar: TButton;
    bt_cancelar: TButton;
    txt_TAG: TEdit;
    lblTAG: TLabel;
    btn_Limpiar: TButton;
    btnBuscar: TButton;
    procedure txt_TAGKeyPress(Sender: TObject; var Key: Char);
    procedure dbl_PersonasDblClick(Sender: TObject);
	procedure bt_aceptarClick(Sender: TObject);
    procedure BuscarClienteContactoAfterOpen(DataSet: TDataSet);
    procedure dbl_PersonasClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btn_LimpiarClick(Sender: TObject);
    procedure btnBuscarClick(Sender: TObject);
    procedure cbPersoneriaExit(Sender: TObject);
    procedure dbl_PersonasDrawText(Sender: TCustomDBListEx;                     //SS_1224_MCA_20141103
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;             //SS_1224_MCA_20141103
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;             //SS_1224_MCA_20141103
      var DefaultDraw: Boolean);                                                //SS_1224_MCA_20141103
  private
	{ Private declarations }
    FUltimaBusqueda : TBusquedaCliente;
    FPersona: TDatosPersonales;
	function GetPersona: TDatosPersonales;
	function GetUltimaBusqueda: TBusquedaCliente;
    procedure BuscarCliente;
  public
	{ Public declarations }
	function Inicializa( UltimaBusqueda : TBusquedaCliente ): Boolean;
	property Persona: TDatosPersonales Read GetPersona;
    property UltimaBusqueda : TBusquedaCliente read GetUltimaBusqueda;
  end;

var
  FormBuscaClientes: TFormBuscaClientes;

implementation


{$R *.dfm}

function TFormBuscaClientes.GetPersona: TDatosPersonales;
begin
    if not BuscarClienteContacto.IsEmpty then begin
        FPersona.CodigoPersona := BuscarClienteContacto.FieldByName('CodigoPersona').AsInteger;
        FPersona.RazonSocial := Trim(BuscarClienteContacto.FieldByName('RazonSocial').AsString);
        FPersona.Personeria := BuscarClienteContacto.FieldByName('Personeria').AsString[1];
        FPersona.TipoDocumento :=  Trim(BuscarClienteContacto.FieldByName('CodigoDocumento').AsString);
        FPersona.NumeroDocumento :=  Trim(BuscarClienteContacto.FieldByName('NumeroDocumento').AsString);
        FPersona.Apellido :=  iif(FPersona.Personeria = PERSONERIA_FISICA, Trim(BuscarClienteContacto.FieldByName('Apellido').AsString), Trim(BuscarClienteContacto.FieldByName('ApellidoContactoComercial').AsString));
        FPersona.ApellidoMaterno :=  iif(FPersona.Personeria = PERSONERIA_FISICA, Trim(BuscarClienteContacto.FieldByName('ApellidoMaterno').AsString), Trim(BuscarClienteContacto.FieldByName('ApellidoMaternoContactoComercial').AsString));
        FPersona.Nombre :=  iif(FPersona.Personeria = PERSONERIA_FISICA, Trim(BuscarClienteContacto.FieldByName('Nombre').AsString), Trim(BuscarClienteContacto.FieldByName('NombreContactoComercial').AsString));
        FPersona.Sexo := (iif(FPersona.Personeria = PERSONERIA_FISICA, BuscarClienteContacto.FieldByName('Sexo').AsString, BuscarClienteContacto.FieldByName('SexoContactoComercial').AsString)+' ')[1];
        FPersona.FechaNacimiento := BuscarClienteContacto.FieldByName('FechaNacimiento').AsDateTime;
        FPersona.Password :=  Trim(BuscarClienteContacto.FieldByName('Password').AsString);
        FPersona.Pregunta :=  Trim(BuscarClienteContacto.FieldByName('Pregunta').AsString);
        FPersona.Respuesta :=  Trim(BuscarClienteContacto.FieldByName('Respuesta').AsString);
    end;
    Result := FPersona;
end;

function TFormBuscaClientes.GetUltimaBusqueda: TBusquedaCliente;
begin
	result := FUltimaBusqueda;
end;

{******************************** Function Header ******************************
Function Name: Inicializa
Author :
Date Created :
Description :
Parameters : UltimaBusqueda : TBusquedaCliente
Return Value : Boolean

Revision : 1
    Author : ggomez
    Date : 16/11/2006
    Description : Cambi� el uso del procedimiento ActualizarBusqueda por el uso
        de BuscarCliente.

*******************************************************************************}
function TFormBuscaClientes.Inicializa( UltimaBusqueda : TBusquedaCliente ): Boolean;
begin
	visible := False;
	CargarPersoneria(cbPersoneria, PERSONERIA_AMBAS, true);

	with UltimaBusqueda do begin
		cbPersoneria.ItemIndex 	:= Personeria;
        txt_rut.Text 			:= RUT;
        txt_Patente.Text 		:= Patente;
	    txt_apellido.Text 		:= Apellido;
        txt_apellidoMaterno.Text:= ApellidoMaterno;
        txt_nombre.Text			:= Nombre;
    	if ( RUT <> '' ) or ( Apellido <> '' ) or
               ( ApellidoMaterno <> '' ) or ( Nombre <> '' ) then begin

           BuscarCliente;
        end;
    end;
    // Habilitar el Apellido Materno y Nombre, s�lo si la personera seleccionada
    // NO es Personera jurdica.
    txt_apellidoMaterno.Enabled := StrRight(cbPersoneria.Text, 1) <> PERSONERIA_JURIDICA;
    lbl_apellidoMaterno.Enabled := txt_apellidoMaterno.Enabled;
    txt_nombre.Enabled := txt_apellidoMaterno.Enabled;
    lbl_nombre.Enabled := txt_apellidoMaterno.Enabled;

    result := True;
end;

procedure TFormBuscaClientes.bt_aceptarClick(Sender: TObject);
begin
    with FUltimaBusqueda do begin
		Personeria 		:= cbPersoneria.ItemIndex;
        RUT         	:= txt_rut.Text;
        Patente			:= txt_Patente.Text;
        Apellido		:= txt_apellido.Text;
        ApellidoMaterno := txt_apellidoMaterno.Text;
        Nombre			:= txt_nombre.Text;
        Tag             := EtiquetaToSerialNumber(txt_TAG.Text);
    end;

    ModalResult := mrOk;
end;

{******************************** Function Header ******************************
Function Name: BuscarCliente
Author : ggomez
Date Created : 16/11/2006
Description : Realiza la b�squeda de clientes seg�n los datos ingresados en los
    filtros.
    Este c�digo es el que antes estaba en el timer.
Parameters : None
Return Value : None

Revision : 1
    Author : ggomez
    Date : 16/11/2006
    Description : Quit� la habilitaci�n o no, de los edits de Apellido Materno
        y el de Nombre.

Revision : 2
    Author : ggomez
    Date : 16/11/2006
    Description :
        - Agregu� el seteo inicial en Null del par�metro @Etiqueta.
        - Agregu� c�digo para respetar las reglas de codificaci�n.
        - Agregu� un mensaje que aparece en el caso de que no se hayan obtenido
        datos.
*******************************************************************************}
procedure TFormBuscaClientes.BuscarCliente;
resourcestring
	CAPTION_GET_CLIENTS 	= 'Obtener Clientes';
	MSG_CANNOT_GET_CLIENTS	= 'No se pudieron obtener los datos de un Cliente';
    MSG_NO_HAY_DATOS        = 'No se pudieron obtener datos de acuerdo a los filtros indicados.';
begin

   	Screen.Cursor := crHourGlass;
    BuscarClienteContacto.Close;

	// Primero los vacio a todos
	with BuscarClienteContacto.Parameters do begin
		ParamByName('@CodigoDocumento').Value	:= null;
		ParamByName('@NumeroDocumento').Value	:= null;
		ParamByName('@Apellido').Value			:= null;
		ParamByName('@ApellidoMaterno').Value   := null;
		ParamByName('@Nombre').Value 			:= null;
		ParamByName('@Patente').Value 			:= null;
        ParamByName('@Personeria').Value		:= null;
        ParamByName('@Etiqueta').Value		    := null;
	end;

	try
        BuscarClienteContacto.Parameters.ParamByName('@CodigoDocumento').Value := TIPO_DOCUMENTO_RUT;
        if (Trim(txt_rut.Text) <> '') then begin
            BuscarClienteContacto.Parameters.ParamByName('@NumeroDocumento').Value := Trim(txt_rut.Text);
        end;

		if (Trim(txt_apellido.Text) <> '') then begin
            BuscarClienteContacto.Parameters.ParamByName('@Apellido').value := Trim(txt_apellido.Text);
        end;

		if (txt_apellidoMaterno.Enabled) and (Trim(txt_apellidoMaterno.Text) <> '') and
        	( txt_apellidoMaterno.Enabled ) then begin

            BuscarClienteContacto.Parameters.ParamByName('@ApellidoMaterno').value := Trim(txt_apellidoMaterno.Text);
        end;

		if (txt_apellidoMaterno.Enabled) and (Trim(txt_nombre.Text) <> '') then begin
            BuscarClienteContacto.Parameters.ParamByName('@Nombre').Value := Trim(txt_nombre.Text);
        end;

        if Trim(txt_Patente.Text) <> '' then begin
            BuscarClienteContacto.Parameters.ParamByName('@Patente').Value := Trim(txt_Patente.Text);
        end;

        if Trim(txt_TAG.Text) <> '' then begin
            BuscarClienteContacto.Parameters.ParamByName('@Etiqueta').Value := Trim(txt_TAG.Text);
        end;

        BuscarClienteContacto.Parameters.ParamByName('@Personeria').Value   :=
            iif( StrRight(cbPersoneria.Text, 1) = PERSONERIA_AMBAS,
                Null, StrRight(cbPersoneria.Text, 1));

       	BuscarClienteContacto.Open;

        if BuscarClienteContacto.IsEmpty then begin
            MsgBox(MSG_NO_HAY_DATOS);
        end;

    except
		on e: Exception do begin
			MsgBoxErr(MSG_CANNOT_GET_CLIENTS, e.message, CAPTION_GET_CLIENTS, MB_ICONSTOP);
        end;
    end;
	Screen.Cursor := crDefault;

end;

procedure TFormBuscaClientes.BuscarClienteContactoAfterOpen(
  DataSet: TDataSet);
begin
    bt_aceptar.Enabled := not DataSet.IsEmpty;
end;

procedure TFormBuscaClientes.cbPersoneriaExit(Sender: TObject);
begin
    // Habilitar el Apellido Materno y Nombre, s�lo si la personer�a seleccionada
    // NO es Personera jur�dica.
    txt_apellidoMaterno.Enabled := StrRight(cbPersoneria.Text, 1) <> PERSONERIA_JURIDICA;
    lbl_apellidoMaterno.Enabled := txt_apellidoMaterno.Enabled;
    txt_nombre.Enabled := txt_apellidoMaterno.Enabled;
    lbl_nombre.Enabled := txt_apellidoMaterno.Enabled;
end;

{-----------------------------------------------------------------------------
REVISION 1:
  Author:    ddiaz
  Date Created: 24/05/2006
  Description: Se coment� la l�nea de c�digo del proc por que no permit�a hacer scroll sobre la grilla
                Se pas� el c�digo al evento DblClick.
-----------------------------------------------------------------------------}
procedure TFormBuscaClientes.dbl_PersonasClick(Sender: TObject);
begin
//    if bt_aceptar.Enabled then bt_aceptar.Click;
end;

procedure TFormBuscaClientes.FormShow(Sender: TObject);
begin
    txt_rut.SetFocus;
end;

{******************************** Function Header ******************************
Function Name: btn_LimpiarClick
Author :
Date Created :
Description :
Parameters : Sender: TObject
Return Value : None

Revision : 1
    Author : ggomez
    Date : 16/11/2006
    Description : Cambi� el uso del procedimiento ActualizarBusqueda por el uso
        de BuscarCliente.
*******************************************************************************}
procedure TFormBuscaClientes.btnBuscarClick(Sender: TObject);
begin
    // hacer setfocus para que ejecute el evento OnExit del control previo
    // cuando se presionan las teclas de shorcut del bot�n Buscar.
    btnBuscar.SetFocus;
    BuscarCliente;
end;

{******************************** Function Header ******************************
Function Name: btn_LimpiarClick
Author :
Date Created :
Description :
Parameters : Sender: TObject
Return Value : None

Revision : 1
    Author : ggomez
    Date : 16/11/2006
    Description :
        - Quite el uso de la variable FNoBuscar.
        - Cambi� para que al limpiar cierre el StoredProc que obtiene
        los datos, en lugar de ejecutar una b�squeda sin datos para filtrar.
        Esto lo hice pues me pareci� adecuado NO buscar datos sin haber ingresado
        datos para filtrar.
*******************************************************************************}
procedure TFormBuscaClientes.btn_LimpiarClick(Sender: TObject);
begin
	txt_rut.Text                := EmptyStr;
    txt_apellido.Text           := EmptyStr;
    txt_apellidoMaterno.Text    := EmptyStr;
    txt_nombre.Text             := EmptyStr;
    txt_Patente.Text            := EmptyStr;
    txt_TAG.Text                := EmptyStr;
    // Cerrar el SP que obtiene los datos.
    BuscarClienteContacto.Close;
    txt_rut.SetFocus;
end;

procedure TFormBuscaClientes.dbl_PersonasDblClick(Sender: TObject);
begin
    if bt_aceptar.Enabled then bt_aceptar.Click;
end;

procedure TFormBuscaClientes.dbl_PersonasDrawText(Sender: TCustomDBListEx;                              //SS_1224_MCA_20141103
  Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;                                         //SS_1224_MCA_20141103
  var Text: string; var TxtRect: TRect; var ItemWidth: Integer;                                         //SS_1224_MCA_20141103
  var DefaultDraw: Boolean);                                                                            //SS_1224_MCA_20141103
begin                                                                                                   //SS_1224_MCA_20141103
    with Sender do begin                                                                                //SS_1224_MCA_20141103
        if Column.Index = 5 then                                                                        //SS_1224_MCA_20141103
            if BuscarClienteContacto.FieldByName('FechaAltaCuentaPatente').AsString <> EmptyStr then    //SS_1224_MCA_20141103
               Text := BuscarClienteContacto.FieldByName('FechaAltaCuentaPatente').AsString             //SS_1224_MCA_20141103
            else if BuscarClienteContacto.FieldByName('FechaAltaCuentaTAG').AsString <> EmptyStr  then  //SS_1224_MCA_20141103
                Text := BuscarClienteContacto.FieldByName('FechaAltaCuentaTAG').AsString;               //SS_1224_MCA_20141103
    end;                                                                                                //SS_1224_MCA_20141103
end;                                                                                                    //SS_1224_MCA_20141103

procedure TFormBuscaClientes.txt_TAGKeyPress(Sender: TObject;
  var Key: Char);
begin
    if not (Key in ['0'..'9', #8]) then Key :=#0;
end;

end.
