object FormCancelarTarea: TFormCancelarTarea
  Left = 216
  Top = 224
  BorderStyle = bsDialog
  Caption = 'Cancelar Tarea'
  ClientHeight = 214
  ClientWidth = 457
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 11
    Top = 9
    Width = 443
    Height = 31
    Alignment = taCenter
    AutoSize = False
    Caption = 
      'Por favor, indique el motivo por el cual cancela esta tarea. Est' +
      'e motivo quedar'#225' almacenado para poder responder adecuadamente a' +
      'l cliente en caso de ser necesario.'
    WordWrap = True
  end
  object Memo1: TMemo
    Left = 5
    Top = 44
    Width = 447
    Height = 133
    TabOrder = 0
  end
  object OPButton1: TButton
    Left = 256
    Top = 184
    Width = 107
    Height = 25
    Caption = 'Cancelar &Tarea'
    Default = True
    TabOrder = 1
    OnClick = OPButton1Click
  end
  object OPButton2: TButton
    Left = 376
    Top = 184
    Width = 75
    Height = 25
    Cancel = True
    Caption = '&Salir'
    ModalResult = 2
    TabOrder = 2
  end
  object CancelarTarea: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'CancelarTarea'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoOrdenServicio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoWorkflow'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Version'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoTarea'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Observaciones'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end>
    Left = 8
    Top = 184
  end
end
