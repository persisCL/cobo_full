unit FreTelefono;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Validate, TimeEdit, VariantComboBox, StdCtrls, Peatypes, Peaprocs, RStrings,Util,
  UtilDB,UtilProc;

type
  TFrameTelefono = class(TFrame)
    lblTelefono: TLabel;
    cbCodigosArea: TComboBox;
    txtTelefono: TEdit;
    cbTipoTelefono: TVariantComboBox;
    lblDesde: TLabel;
    teDesde: TTimeEdit;
    lblHasta: TLabel;
    teHasta: TTimeEdit;
    procedure txtTelefonoKeyPress(Sender: TObject; var Key: Char);
  private
    FEsObligatorio: boolean;
    { Private declarations }
    function GetCodigoArea: integer;
    function GetNumeroTelefono: AnsiString;
    function GetCodigoTipoTelefono: integer;
    function GetDescriTipoTelefono: AnsiString;
    function GetHorarioDesde: TDateTime;
    function GetHorarioHasta: TDateTime;
    procedure SetEsObligatorio(const Value: boolean);
    procedure FormatearObligatorio(Obligatorio: boolean);
    function GetCargoTelelefono:Boolean;
  public
    { Public declarations }
   property CargoTelefono:Boolean read GetCargoTelelefono;
   property CodigoArea: integer read GetCodigoArea;
   property NumeroTelefono: AnsiString read GetNumeroTelefono;
   property CodigoTipoTelefono: integer read GetCodigoTipoTelefono;
   property DescriTipoTelefono: AnsiString read GetDescriTipoTelefono;
   property HorarioDesde: TDateTime read GetHorarioDesde;
   property HorarioHasta: TDateTime read GetHorarioHasta;
   property EsObligatorio: boolean read FEsObligatorio write SetEsObligatorio;
   procedure Inicializa(CaptionLblTelefono: AnsiString='Tel�fono principal:'; Obligatorio: boolean = false; PedirHora: boolean = true);
   procedure CargarTelefono(codArea: integer; nroTel: AnsiString; codTipoTelefono: integer;
                desde:TDateTime=NullDate; hasta: TDateTime=NullDate);
   function ValidarTelefono: boolean;
   procedure Limpiar;
  end;

implementation

uses DMConnection;

{$R *.dfm}

{ TFrameTelefono }

procedure TFrameTelefono.CargarTelefono(codArea: integer;
  nroTel: AnsiString; codTipoTelefono: integer; desde, hasta: TDateTime);
begin
    CargarCodigosAreaTelefono(DMConnections.BaseCAC, cbCodigosArea, codArea);
    txtTelefono.Text := nroTel;
    MarcarItemComboVariant(cbTipoTelefono, codTipoTelefono);
    if FEsObligatorio then begin
        teDesde.Time := iif(desde=NullDate,StrToTime(HORA_DESDE),desde);
        teHasta.Time := iif(hasta=NullDate,StrToTime(HORA_HASTA),hasta);
    end;
end;

procedure TFrameTelefono.FormatearObligatorio(Obligatorio: boolean);
begin
    if Obligatorio then begin
        lblTelefono.Font.Color := clNavy;
        cbCodigosArea.Color := COLOR_EDITS_OBLIGATORIO;
        txtTelefono.Color := COLOR_EDITS_OBLIGATORIO;
        cbTipoTelefono.Color := COLOR_EDITS_OBLIGATORIO;
    end
    else begin
        lblTelefono.Font.Color := clWindowText;
        cbCodigosArea.Color := clWindow;
        txtTelefono.Color := clWindow;
        cbTipoTelefono.Color := clWindow;
    end;
end;

function TFrameTelefono.GetCodigoArea: integer;
begin
    result := StrToInt(cbCodigosArea.Text);
end;

function TFrameTelefono.GetCodigoTipoTelefono: integer;
begin
    result := cbTipoTelefono.Value;
end;

function TFrameTelefono.GetDescriTipoTelefono: AnsiString;
begin
    result := cbTipoTelefono.Items.Items[cbTipoTelefono.itemindex].Caption;
end;

function TFrameTelefono.GetHorarioDesde: TDateTime;
begin
    result := teDesde.Time;
end;

function TFrameTelefono.GetHorarioHasta: TDateTime;
begin
    result := teHasta.Time;
end;

function TFrameTelefono.GetNumeroTelefono: AnsiString;
begin
    result := Trim(txtTelefono.Text);
end;

procedure TFrameTelefono.Inicializa(CaptionLblTelefono: AnsiString;
  Obligatorio, PedirHora: boolean);
begin
    {PARAMETROS:
        - CaptionLblTelefono: puede que quieran poner "Telefono principal" o "Telefono alternativo"
        - Obligatorio: es si todo el tel�fono es obligatorio. Entonces va color celeste
        - PedirHora:
    }
    if (trim(CaptionLblTelefono) <> '') then
        lblTelefono.Caption := CaptionLblTelefono;
    lblDesde.Visible := PedirHora;
    lblHasta.Visible := PedirHora;
    teDesde.Visible := PedirHora;
    teHasta.Visible := PedirHora;
    FEsObligatorio := Obligatorio;
    FormatearObligatorio(FEsObligatorio);

    Limpiar;
end;

procedure TFrameTelefono.SetEsObligatorio(const Value: boolean);
begin
    FEsObligatorio := value;
end;

function TFrameTelefono.ValidarTelefono: boolean;

    function TelefonoValido(CodigoArea:Integer; Telefono:AnsiString):Boolean;
    var
        te: Integer;
    begin
        try
            te := StrToInt(self.NumeroTelefono);
            result:=Trim(QueryGetValue(DMConnections.BaseCAC,format('SELECT * FROM CodigosAreaTelefono WHERE CodigoArea = %d AND ((RangoDesde IS NULL) OR (%d >= RangoDesde)) AND  ((RangoHasta IS NULL) OR (%d <= RangoHasta))',[CodigoArea,Te,Te])))<>'';
        except
            result:= false;
        end;
        if not(Result) then begin
            msgBoxBalloon(MSG_ERROR_AREA_TELEFONO,lblTelefono.Caption,MB_ICONSTOP,txtTelefono);
            txtTelefono.SetFocus;
        end;
    end;

begin
    Result:=True;

    // si es obligatorio tiene que ingresar todo!
    if FEsObligatorio then begin
        result := iif(ValidateControls([txtTelefono, cbTipoTelefono, cbCodigosArea, teDesde, txtTelefono],
                        [(trim(txtTelefono.Text) <> ''),
                        (cbTipoTelefono.ItemIndex > 0),
                        (cbCodigosArea.ItemIndex >= 0),
                        (teDesde.Time <= teHasta.Time),
                        TelefonoValido(self.CodigoArea, self.NumeroTelefono)],
                        lblTelefono.Caption,
                        [Format(MSG_VALIDAR_DEBE_EL,[lblTelefono.Caption]),
                        MSG_VALIDAR_TIPO_TELEFONO,
                        MSG_VALIDAR_CODIGO_AREA,
                        MSG_RANGO_HORARIO,
                        MSG_NUMERO_TELEFONO]), true, false);
    end
    else begin
        //si no es obligatorio, o no ingreso nada o lo ingres� bien!!!
        if (trim(txtTelefono.Text)<> '') then
            result := iif(ValidateControls([cbTipoTelefono, cbCodigosArea],
                        [(cbTipoTelefono.ItemIndex > 0),
                        (cbCodigosArea.ItemIndex >= 0)],
                        lblTelefono.Caption,
                        [MSG_VALIDAR_TIPO_TELEFONO,MSG_VALIDAR_CODIGO_AREA])
                        and
                        TelefonoValido(self.CodigoArea,self.NumeroTelefono),
            true,false);
    end;
end;

procedure TFrameTelefono.txtTelefonoKeyPress(Sender: TObject;
  var Key: Char);
begin
    if not (Key  in ['0'..'9','-', #8]) then
        Key := #0;
end;

function TFrameTelefono.GetCargoTelelefono: Boolean;
begin
    result:=trim(self.NumeroTelefono)<>'';
end;

procedure TFrameTelefono.Limpiar;
begin
    CargarCodigosAreaTelefono(DMConnections.BaseCAC, cbCodigosArea);
    if cbCodigosArea.ItemIndex = -1 then cbCodigosArea.ItemIndex := 0;
    CargarTiposMedioContacto(DMConnections.BaseCAC, cbTipoTelefono, 0, TMC_TELEFONO, False, True);

    txtTelefono.Clear;
    teDesde.Time:=StrToTime(HORA_DESDE);
    teHasta.Time:=StrToTime(HORA_HASTA);
end;

end.
