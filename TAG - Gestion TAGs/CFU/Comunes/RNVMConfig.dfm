object frmConfig: TfrmConfig
  Left = 320
  Top = 237
  BorderStyle = bsDialog
  Caption = 'Configuraci'#243'n del RNVM'
  ClientHeight = 145
  ClientWidth = 301
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  DesignSize = (
    301
    145)
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 2
    Top = 8
    Width = 292
    Height = 100
    Anchors = [akLeft, akTop, akRight, akBottom]
  end
  object lbl_ServerProxy: TLabel
    Left = 40
    Top = 50
    Width = 63
    Height = 13
    Caption = 'Server Proxy:'
  end
  object lbl_ServerPort: TLabel
    Left = 40
    Top = 77
    Width = 56
    Height = 13
    Caption = 'Server Port:'
  end
  object btnAceptar: TButton
    Left = 135
    Top = 117
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Aceptar'
    Default = True
    ModalResult = 1
    TabOrder = 3
    OnClick = btnAceptarClick
  end
  object btnCancelar: TButton
    Left = 215
    Top = 117
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 4
  end
  object chkProxy: TCheckBox
    Left = 16
    Top = 16
    Width = 137
    Height = 17
    Caption = 'Utilizar Servidor Proxy'
    TabOrder = 0
    OnClick = chkProxyClick
  end
  object txtServer: TEdit
    Left = 109
    Top = 47
    Width = 94
    Height = 21
    TabOrder = 1
    Text = 'txtServer'
  end
  object nePort: TNumericEdit
    Left = 109
    Top = 72
    Width = 49
    Height = 21
    TabOrder = 2
  end
end
