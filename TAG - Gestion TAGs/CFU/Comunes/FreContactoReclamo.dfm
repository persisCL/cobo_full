object FrameContactoReclamo: TFrameContactoReclamo
  Left = 0
  Top = 0
  Width = 627
  Height = 172
  TabOrder = 0
  TabStop = True
  DesignSize = (
    627
    172)
  object gbUsuario: TGroupBox
    Left = 5
    Top = 7
    Width = 370
    Height = 102
    Caption = ' Identificaci'#243'n del usuario '
    TabOrder = 0
    OnClick = CambioContactar
    object Label4: TLabel
      Left = 5
      Top = 23
      Width = 26
      Height = 13
      Caption = 'RUT:'
      Color = 16776699
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      Transparent = True
    end
    object Label2: TLabel
      Left = 5
      Top = 51
      Width = 88
      Height = 13
      Caption = 'Nombre y Apellido:'
      Color = 16776699
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      Transparent = True
    end
    object LNumeroConvenio: TLabel
      Left = 6
      Top = 80
      Width = 64
      Height = 13
      Caption = 'N'#176' Convenio:'
    end
    object lblVer: TLabel
      Left = 298
      Top = 75
      Width = 64
      Height = 13
      Caption = 'Ir al convenio'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      OnClick = lblVerClick
      OnMouseMove = lblVerMouseMove
    end
    object txtNombre: TEdit
      Left = 94
      Top = 48
      Width = 185
      Height = 21
      TabOrder = 0
    end
    object txtNumeroDocumento: TPickEdit
      Left = 94
      Top = 20
      Width = 140
      Height = 21
      CharCase = ecUpperCase
      Enabled = True
      TabOrder = 1
      OnEnter = txtNumeroDocumentoEnter
      OnExit = txtNumeroDocumentoExit
      OnKeyPress = txtNumeroDocumentoKeyPress
      EditorStyle = bteTextEdit
      OnButtonClick = txtNumeroDocumentoButtonClick
    end
    object TxtNumeroConvenio: TVariantComboBox
      Left = 94
      Top = 75
      Width = 200
      Height = 19
      Style = vcsOwnerDrawFixed
      ItemHeight = 13
      TabOrder = 2
      OnChange = TxtNumeroConvenioChange
      OnDrawItem = TxtNumeroConvenioDrawItem
      Items = <>
    end
  end
  object gbContacto: TGroupBox
    Left = 377
    Top = 7
    Width = 244
    Height = 161
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = ' Informaci'#243'n de contacto '
    TabOrder = 2
    object lblEnviarMail: TLabel
      Left = 189
      Top = 113
      Width = 30
      Height = 13
      Cursor = crHandPoint
      Caption = 'Enviar'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      OnClick = lblEnviarMailClick
      OnMouseEnter = lblEnviarMailMouseEnter
      OnMouseLeave = lblEnviarMailMouseLeave
    end
    object rbTelefono: TRadioButton
      Left = 5
      Top = 43
      Width = 235
      Height = 17
      Caption = 'El usuario desea ser contactado por tel'#233'fono'
      TabOrder = 1
      OnClick = CambioContactar
    end
    object rbEmail: TRadioButton
      Left = 5
      Top = 90
      Width = 227
      Height = 17
      Caption = 'El usuario desea ser contactado por e-mail '
      TabOrder = 4
      OnClick = CambioContactar
    end
    object rbNoContactar: TRadioButton
      Left = 5
      Top = 20
      Width = 193
      Height = 17
      Caption = 'El usuario no desea ser contactado'
      Checked = True
      TabOrder = 0
      TabStop = True
      OnClick = CambioContactar
    end
    object cbCodigoArea: TComboBox
      Left = 16
      Top = 66
      Width = 55
      Height = 21
      Style = csDropDownList
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ItemHeight = 13
      ParentFont = False
      TabOrder = 2
    end
    object txtEmail: TEdit
      Left = 5
      Top = 108
      Width = 177
      Height = 21
      TabOrder = 5
      OnChange = txtEmailChange
    end
    object txtTelefono: TEdit
      Left = 77
      Top = 66
      Width = 120
      Height = 21
      Hint = 'N'#250'mero de tel'#233'fono'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 9
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
    end
    object RBcarta: TRadioButton
      Left = 5
      Top = 135
      Width = 222
      Height = 17
      Caption = 'El usuario desea ser contactado por carta '
      TabOrder = 6
    end
  end
  object GBPersona: TGroupBox
    Left = 4
    Top = 113
    Width = 371
    Height = 54
    Anchors = [akLeft, akTop, akBottom]
    Caption = 'Identificaci'#243'n de la persona que llama'
    TabOrder = 1
    object Label3: TLabel
      Left = 12
      Top = 25
      Width = 88
      Height = 13
      Caption = 'Nombre y Apellido:'
      Color = 16776699
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      Transparent = True
    end
    object TXTNombrePersona: TEdit
      Left = 127
      Top = 22
      Width = 186
      Height = 21
      TabOrder = 0
    end
  end
  object SPObtenerConveniosxNumeroDocumento: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerConveniosxNumeroDocumento;1'
    Parameters = <>
    Left = 320
    Top = 19
  end
  object spObtenerDatosPersonaReclamo: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerDatosPersonaReclamo;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPersona'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 288
    Top = 19
  end
end
