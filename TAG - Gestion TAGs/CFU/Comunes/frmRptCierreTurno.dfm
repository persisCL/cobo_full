object FormRptCierreTurno: TFormRptCierreTurno
  Left = 391
  Top = 181
  Caption = 'Informe de Cierre de Turno'
  ClientHeight = 691
  ClientWidth = 1120
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object DBPInformadoSaldoFinal: TppDBPipeline
    DataSource = dsInformadoSaldoFinal
    UserName = 'DBPInformadoSaldoFinal'
    Left = 39
    Top = 49
    object pfldDBPInformadoSaldoFinalppField1: TppField
      FieldAlias = 'NumeroVale'
      FieldName = 'NumeroVale'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 0
      Searchable = False
      Sortable = False
    end
    object pfldDBPInformadoSaldoFinalppField2: TppField
      FieldAlias = 'Codigocategoria'
      FieldName = 'Codigocategoria'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 1
      Searchable = False
      Sortable = False
    end
    object pfldDBPInformadoSaldoFinalppField3: TppField
      FieldAlias = 'Categoria'
      FieldName = 'Categoria'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 2
      Searchable = False
      Sortable = False
    end
    object pfldDBPInformadoSaldoFinalppField4: TppField
      FieldAlias = 'Cantidad Final'
      FieldName = 'Cantidad Final'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 3
      Searchable = False
      Sortable = False
    end
  end
  object rbiListado: TRBInterface
    Report = rptCierreTurno
    OrderIndex = 0
    Left = 107
    Top = 9
  end
  object dsInformadoSaldoFinal: TDataSource
    DataSet = ObtenerSaldoFinalInformadoTurno
    Left = 106
    Top = 49
  end
  object ObtenerSaldoFinalInformadoTurno: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerSaldoFinalInformadoTurno'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoPuntoEntrega'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPuntoVenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 72
    Top = 48
    object ObtenerSaldoFinalInformadoTurnoNumeroVale: TStringField
      FieldName = 'NumeroVale'
    end
    object ObtenerSaldoFinalInformadoTurnoCodigocategoria: TWordField
      FieldName = 'Codigocategoria'
    end
    object ObtenerSaldoFinalInformadoTurnoCategoria: TStringField
      FieldName = 'Categoria'
      ReadOnly = True
      FixedChar = True
      Size = 60
    end
    object ObtenerSaldoFinalInformadoTurnoCantidadFinal: TLargeintField
      FieldName = 'Cantidad Final'
    end
  end
  object DBPCalculadoSaldoInicial: TppDBPipeline
    DataSource = dsCalculadoSaldoInicial
    UserName = 'DBPCalculadoSaldoInicial'
    Left = 229
    Top = 49
  end
  object ObtenerSaldoInicialTurno: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerSaldoInicialTurno'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoPuntoEntrega'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 1
      end
      item
        Name = '@CodigoPuntoVenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 1
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = 'dcalani'
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 2
      end>
    Left = 262
    Top = 48
    object ObtenerSaldoInicialTurnoNumeroVale: TStringField
      FieldName = 'NumeroVale'
    end
    object ObtenerSaldoInicialTurnoCodigocategoria: TWordField
      FieldName = 'Codigocategoria'
    end
    object ObtenerSaldoInicialTurnoCategoria: TStringField
      FieldName = 'Categoria'
      ReadOnly = True
      FixedChar = True
      Size = 60
    end
    object ObtenerSaldoInicialTurnoCantidadInicial: TLargeintField
      FieldName = 'Cantidad Inicial'
    end
    object ObtenerSaldoInicialTurnoNumeroTurno: TIntegerField
      FieldName = 'NumeroTurno'
    end
  end
  object dsCalculadoSaldoInicial: TDataSource
    DataSet = ObtenerSaldoInicialTurno
    Left = 295
    Top = 49
  end
  object DBPCalculadoEntregado: TppDBPipeline
    DataSource = dsCalculadoEntregado
    UserName = 'DBPCalculadoEntregado'
    Left = 39
    Top = 81
  end
  object ObtenerSaldoEntregadoTurno: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'ObtenerSaldoEntregadoTurno'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoPuntoEntrega'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPuntoVenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 72
    Top = 80
  end
  object dsCalculadoEntregado: TDataSource
    DataSet = ObtenerSaldoEntregadoTurno
    Left = 106
    Top = 81
  end
  object DBPCalculadoSaldoFinal: TppDBPipeline
    DataSource = dsCalculadoSaldoFinal
    UserName = 'DBPCalculadoSaldoFinal'
    Left = 229
    Top = 81
  end
  object ObtenerSaldoFinalCalculadoTurno: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerSaldoFinalCalculadoTurno'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoPuntoEntrega'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPuntoVenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 262
    Top = 80
  end
  object dsCalculadoSaldoFinal: TDataSource
    DataSet = ObtenerSaldoFinalCalculadoTurno
    Left = 295
    Top = 80
  end
  object DBPSaldoDiferencia: TppDBPipeline
    DataSource = dsSaldoDiferencia
    UserName = 'DBPSaldoDiferencia'
    Left = 39
    Top = 113
  end
  object ObtenerSaldoDiferenciaTurno: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'ObtenerSaldoDiferenciaTurno'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoPuntoEntrega'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPuntoVenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 72
    Top = 112
  end
  object dsSaldoDiferencia: TDataSource
    DataSet = ObtenerSaldoDiferenciaTurno
    Left = 106
    Top = 113
  end
  object DBPTotalesConvenios: TppDBPipeline
    DataSource = dsTotalesConvenios
    UserName = 'DBPTotalesConvenios'
    Left = 229
    Top = 113
  end
  object ObtenerTotalesConveniosTurno: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'ObtenerTotalesConveniosTurno;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoPuntoEntrega'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPuntoVenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 262
    Top = 112
  end
  object dsTotalesConvenios: TDataSource
    DataSet = ObtenerTotalesConveniosTurno
    Left = 295
    Top = 113
  end
  object DBPEncabezadoCierreTurno: TppDBPipeline
    DataSource = dsEncabezadoCierreTurno
    UserName = 'DBPEncabezadoCierreTurno'
    Left = 39
    Top = 145
  end
  object ObtenerEncabezadoCierreTurno: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerEncabezadoCierreTurno'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoPuntoEntrega'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPuntoVenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 72
    Top = 144
  end
  object dsEncabezadoCierreTurno: TDataSource
    DataSet = ObtenerEncabezadoCierreTurno
    Left = 106
    Top = 145
  end
  object dsSaldosAdicionales: TDataSource
    DataSet = ObtenerSaldosAdicionales
    Left = 295
    Top = 147
  end
  object ObtenerSaldosAdicionales: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'ObtenerSaldosAdicionales'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoPuntoEntrega'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPuntoVenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 262
    Top = 146
    object ObtenerSaldosAdicionalesNumeroVale: TStringField
      FieldName = 'NumeroVale'
    end
    object ObtenerSaldosAdicionalesCodigocategoria: TWordField
      FieldName = 'Codigocategoria'
    end
    object ObtenerSaldosAdicionalesCategoria: TStringField
      FieldName = 'Categoria'
      ReadOnly = True
      FixedChar = True
      Size = 60
    end
    object ObtenerSaldosAdicionalesCantidadInicial: TLargeintField
      FieldName = 'Cantidad Inicial'
    end
  end
  object dbpSaldosAdicionales: TppDBPipeline
    DataSource = dsSaldosAdicionales
    UserName = 'dbpSaldosAdicionales'
    Left = 229
    Top = 144
  end
  object DBCalculadoDevuelto: TppDBPipeline
    DataSource = dsObtenerSaldoDevueltoTurno
    UserName = 'DBPCalculadoDevuelto'
    Left = 39
    Top = 179
  end
  object dsObtenerSaldoDevueltoTurno: TDataSource
    DataSet = ObtenerSaldoDevueltoTurno
    Left = 106
    Top = 177
  end
  object ObtenerSaldoDevueltoTurno: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'ObtenerSaldoDevueltoTurno'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@CodigoPuntoEntrega'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CodigoPuntoVenta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Usuario'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 72
    Top = 176
  end
  object rptCierreTurno: TppReport
    AutoStop = False
    DataPipeline = DBPEncabezadoCierreTurno
    PrinterSetup.BinName = 'Default'
    PrinterSetup.Copies = 2
    PrinterSetup.DocumentName = 'Report'
    PrinterSetup.PaperName = 'A4'
    PrinterSetup.PrinterName = 'Default'
    PrinterSetup.SaveDeviceSettings = False
    PrinterSetup.mmMarginBottom = 6350
    PrinterSetup.mmMarginLeft = 6350
    PrinterSetup.mmMarginRight = 6350
    PrinterSetup.mmMarginTop = 6350
    PrinterSetup.mmPaperHeight = 297000
    PrinterSetup.mmPaperWidth = 210000
    PrinterSetup.PaperSize = 9
    Template.FileName = 
      'C:\SVN\BackOffice\Fuentes\COBO\01_Desarrollo\branches\Comunes\rp' +
      'tcierre.rtm'
    BeforePrint = ppReporteBeforePrint
    DeviceType = 'Screen'
    EmailSettings.ReportFormat = 'PDF'
    LanguageID = 'Default'
    OutlineSettings.CreateNode = True
    OutlineSettings.CreatePageNodes = True
    OutlineSettings.Enabled = True
    OutlineSettings.Visible = True
    PDFSettings.EmbedFontOptions = [efUseSubset]
    PDFSettings.EncryptSettings.AllowCopy = True
    PDFSettings.EncryptSettings.AllowInteract = True
    PDFSettings.EncryptSettings.AllowModify = True
    PDFSettings.EncryptSettings.AllowPrint = True
    PDFSettings.EncryptSettings.Enabled = False
    PDFSettings.FontEncoding = feAnsi
    RTFSettings.DefaultFont.Charset = DEFAULT_CHARSET
    RTFSettings.DefaultFont.Color = clWindowText
    RTFSettings.DefaultFont.Height = -13
    RTFSettings.DefaultFont.Name = 'Arial'
    RTFSettings.DefaultFont.Style = []
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = True
    Left = 158
    Top = 9
    Version = '12.04'
    mmColumnWidth = 0
    DataPipelineName = 'DBPEncabezadoCierreTurno'
    object ppDetailBand10: TppDetailBand
      Background1.Brush.Style = bsClear
      Background1.Gradient.EndColor = clWhite
      Background1.Gradient.StartColor = clWhite
      Background1.Gradient.Style = gsNone
      Background2.Brush.Style = bsClear
      Background2.Gradient.EndColor = clWhite
      Background2.Gradient.StartColor = clWhite
      Background2.Gradient.Style = gsNone
      PrintHeight = phDynamic
      mmBottomOffset = 0
      mmHeight = 21696
      mmPrintPosition = 0
      object ppLiquidacionEfectivoReport: TppSubReport
        UserName = 'ppLiquidacionEfectivoReport'
        ExpandAll = False
        KeepTogether = True
        NewPrintJob = True
        OutlineSettings.CreateNode = True
        PrintBehavior = pbSection
        ResetPageNo = False
        TraverseAllData = False
        DataPipelineName = 'ppDBPipelineDetalleTurno'
        mmHeight = 5027
        mmLeft = 0
        mmTop = 0
        mmWidth = 197300
        BandType = 4
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object ppChildReport11: TppChildReport
          AutoStop = False
          DataPipeline = ppDBPipelineDetalleTurno
          PrinterSetup.BinName = 'Default'
          PrinterSetup.Copies = 2
          PrinterSetup.DocumentName = 'Report'
          PrinterSetup.PaperName = 'A4'
          PrinterSetup.PrinterName = 'Default'
          PrinterSetup.SaveDeviceSettings = False
          PrinterSetup.mmMarginBottom = 6350
          PrinterSetup.mmMarginLeft = 6350
          PrinterSetup.mmMarginRight = 6350
          PrinterSetup.mmMarginTop = 6350
          PrinterSetup.mmPaperHeight = 297000
          PrinterSetup.mmPaperWidth = 210000
          PrinterSetup.PaperSize = 9
          Version = '12.04'
          mmColumnWidth = 0
          DataPipelineName = 'ppDBPipelineDetalleTurno'
          object ppTitleBand19: TppTitleBand
            mmBottomOffset = 0
            mmHeight = 42598
            mmPrintPosition = 0
            object ppLabel64: TppLabel
              UserName = 'Label301'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'Informe Cierre de Turno'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 16
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 6615
              mmLeft = 69321
              mmTop = 4498
              mmWidth = 69056
              BandType = 1
            end
            object ppLabel65: TppLabel
              UserName = 'Label65'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'Usuario:'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4233
              mmLeft = 118798
              mmTop = 28046
              mmWidth = 14288
              BandType = 1
            end
            object ppDBText50: TppDBText
              UserName = 'DBText50'
              HyperlinkColor = clBlue
              AutoSize = True
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              DataField = 'Usuario'
              DataPipeline = DBPEncabezadoCierreTurno
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              ParentDataPipeline = False
              Transparent = True
              DataPipelineName = 'DBPEncabezadoCierreTurno'
              mmHeight = 4022
              mmLeft = 152400
              mmTop = 28046
              mmWidth = 12107
              BandType = 1
            end
            object ppLine41: TppLine
              UserName = 'Line41'
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              ParentWidth = True
              Position = lpBottom
              Weight = 0.750000000000000000
              mmHeight = 1323
              mmLeft = 0
              mmTop = 41010
              mmWidth = 197300
              BandType = 1
            end
            object ppLabel66: TppLabel
              UserName = 'Label66'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'POS:'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4191
              mmLeft = 6085
              mmTop = 32015
              mmWidth = 8594
              BandType = 1
            end
            object ppLabel67: TppLabel
              UserName = 'Label67'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'Punto de Venta:'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4191
              mmLeft = 6085
              mmTop = 36777
              mmWidth = 26797
              BandType = 1
            end
            object ppLabel68: TppLabel
              UserName = 'Label68'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'Fecha de Cierre:'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4233
              mmLeft = 119063
              mmTop = 36777
              mmWidth = 28046
              BandType = 1
            end
            object ppDBText51: TppDBText
              UserName = 'DBText51'
              HyperlinkColor = clBlue
              AutoSize = True
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              DataField = 'FechaHoraCierre'
              DataPipeline = DBPEncabezadoCierreTurno
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              ParentDataPipeline = False
              Transparent = True
              DataPipelineName = 'DBPEncabezadoCierreTurno'
              mmHeight = 4022
              mmLeft = 152400
              mmTop = 36777
              mmWidth = 26952
              BandType = 1
            end
            object ppDBText52: TppDBText
              UserName = 'DBText52'
              HyperlinkColor = clBlue
              AutoSize = True
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              DataField = 'PuntoEntrega'
              DataPipeline = DBPEncabezadoCierreTurno
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              ParentDataPipeline = False
              Transparent = True
              DataPipelineName = 'DBPEncabezadoCierreTurno'
              mmHeight = 4022
              mmLeft = 17992
              mmTop = 32015
              mmWidth = 21414
              BandType = 1
            end
            object ppDBText53: TppDBText
              UserName = 'DBText53'
              HyperlinkColor = clBlue
              AutoSize = True
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              DataField = 'PuntoVenta'
              DataPipeline = DBPEncabezadoCierreTurno
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              ParentDataPipeline = False
              Transparent = True
              DataPipelineName = 'DBPEncabezadoCierreTurno'
              mmHeight = 4022
              mmLeft = 35454
              mmTop = 36777
              mmWidth = 18274
              BandType = 1
            end
            object ppLine42: TppLine
              UserName = 'Line42'
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              ParentWidth = True
              Position = lpBottom
              Weight = 0.750000000000000000
              mmHeight = 1323
              mmLeft = 0
              mmTop = 24871
              mmWidth = 197300
              BandType = 1
            end
            object ppLabel69: TppLabel
              UserName = 'Label69'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'Saldos Informados y Total de Convenios'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 11
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4657
              mmLeft = 66940
              mmTop = 18256
              mmWidth = 75142
              BandType = 1
            end
            object ppLabel71: TppLabel
              UserName = 'Label71'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'N'#250'mero Turno:'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4191
              mmLeft = 6085
              mmTop = 27252
              mmWidth = 25485
              BandType = 1
            end
            object ppDBText54: TppDBText
              UserName = 'DBText54'
              HyperlinkColor = clBlue
              AutoSize = True
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              DataField = 'NumeroTurno'
              DataPipeline = DBPEncabezadoCierreTurno
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              ParentDataPipeline = False
              Transparent = True
              DataPipelineName = 'DBPEncabezadoCierreTurno'
              mmHeight = 4022
              mmLeft = 35190
              mmTop = 27252
              mmWidth = 21802
              BandType = 1
            end
            object ppLabel72: TppLabel
              UserName = 'Label72'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'General Prieto 1430. Independencia'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              mmHeight = 3260
              mmLeft = 10054
              mmTop = 20902
              mmWidth = 45466
              BandType = 1
            end
            object ppImage3: TppImage
              UserName = 'Image3'
              AlignHorizontal = ahCenter
              AlignVertical = avCenter
              MaintainAspectRatio = False
              Stretch = True
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Picture.Data = {
                0A544A504547496D61676539490000FFD8FFE000104A46494600010101006000
                600000FFEC00114475636B7900010004000000640000FFDB0043000201010201
                010202020202020202030503030303030604040305070607070706070708090B
                0908080A0807070A0D0A0A0B0C0C0C0C07090E0F0D0C0E0B0C0C0CFFDB004301
                020202030303060303060C0807080C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C
                0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C
                FFC000110800A0019803012200021101031101FFC4001F000001050101010101
                0100000000000000000102030405060708090A0BFFC400B51000020103030204
                03050504040000017D01020300041105122131410613516107227114328191A1
                082342B1C11552D1F02433627282090A161718191A25262728292A3435363738
                393A434445464748494A535455565758595A636465666768696A737475767778
                797A838485868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4B5
                B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE1E2E3E4E5E6E7E8E9
                EAF1F2F3F4F5F6F7F8F9FAFFC4001F0100030101010101010101010000000000
                000102030405060708090A0BFFC400B511000201020404030407050404000102
                77000102031104052131061241510761711322328108144291A1B1C109233352
                F0156272D10A162434E125F11718191A262728292A35363738393A4344454647
                48494A535455565758595A636465666768696A737475767778797A8283848586
                8788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2
                C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3E4E5E6E7E8E9EAF2F3F4F5F6F7
                F8F9FAFFDA000C03010002110311003F00FDFCA28A2800A28A2800A28A2800A2
                8A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A2
                8A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A2
                8A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A28033F
                C51E28D33C11E1CBED635AD46C348D234C81EE6F2FAF6E12DEDAD22519692491
                C85450324B120003AD793FFC3C87F677FF00A2F5F05FFF000B7D33FF008F5798
                FF00C175D8A7FC126FE329048FF896DA8E3FEBFEDABF01FF00E0975FF04EABDF
                F829AFC7FD5FC0563E2AB5F084DA4787E6D7CDE4F60D78B2AC7736D0795B03A1
                049B90D9CFF0631CE47D2E519250C4E1278AC454715176D15FA2FF0033E5F38C
                F31185C5C30987A6A4E4AFABB756BF43FA5CF047EDC1F05BE266BD0E95E1CF8B
                FF000BBC41A9DC3058ACF4DF15D85DDC484F40A91CA589FA0AF52AFE69FF00E0
                A61FF0421F883FF04E6F85D0F8EE5F13687E36F078BA8AC6EEEACE092D2EAC24
                93211A485CB0F2CB0DA195C9C95CA8CE6BEDFF00F835F3F6FAF187C63B6F17FC
                1BF186AD79AFDBF8534D8B58F0F5D5E4E65B8B3B5122C12DAEE6C96894BC2501
                3F265C7DD2A16F1B90518E11E3707579E2B7D2DFD7A589C0F10D6962D60B1B4B
                924F6D6FFD7ADCFD30F8E1FB60FC2AFD9A752B4B3F881F117C19E0EBED422F3A
                DAD756D5A1B59E78F24798B1B3062995237631918CE6BB0F017C41D07E2A7842
                CBC41E18D6F49F11683A9A192D351D32EE3BBB4B950C54949632558065607078
                208EA2BF043FE0EB9FF94887833FEC9D58FF00E9CF54AFD38FF837ABFE50FF00
                F087FEE33FFA7BBFAE6C6E4D0A39752C6A936E6ED6E9B37FA1D382CE675F32AB
                81714941377EBA35FE67D8BADEB765E1AD16F352D4AF2D74FD3F4F81EE6EAEAE
                6558A0B68914B3C8EEC42AAAA824B120000935E69F09BF6E5F833F1EBC68DE1C
                F057C53F0178AB5E0ACEBA7E99ADDBDCDCCAA992CC88AC4BAA80492B90073D2B
                37FE0A43FF0028EFF8F5FF0064EBC43FFA6CB8AFC10FF8370BFE52CBE05FFB07
                6ADFFA413D2CB7278627075B132934E0B45DF4B8F33CE2786C6D1C2C629AA8D5
                DF6BBB1FD0278EFF006E1F82DF0BBC597BA0789BE307C2EF0E6BBA6B04BBD3B5
                4F155859DDDAB150C0491492ABA92ACA7903820F7ACAFF008790FECEFF00F45E
                BE0BFF00E16FA67FF1EAFCEEFDBBFF00E0DB2F19FED63FB5E78EFE23E93F133C
                31A55878BF51FED08ACEEF4F9DA6B7CC680AB1538386070476C74E95F955FF00
                050DFD87755FF8279FED2173F0DF59D734FF00115EDB585B5F9BCB285E289966
                5242ED7E7231CD7A79764397E2F9614EBB73B5DAB6DDFEE3CBCCB3FCC70779D4
                A0942F64EFBF6DBB9FD337FC3C87F677FF00A2F5F05FFF000B7D33FF008F57A6
                7C3AF89BE1BF8C1E11B6F10784BC43A1F8A341BD2EB6FA96917F15F5A4E51CA3
                849626646DAEACA70782A41E457E077ECCDFF06CEF8E7F698FD9F7C19F106C7E
                26F84F4CB2F196916FAB43693D85C3CB6EB320708C41C123382457EB27FC13FB
                F6628BFE092FFF0004F7BCD0BC5FE25B4D6ADFC1C9AA789355D42DA268608E11
                BE76081CE70B1A02738F989ED8AF3733CBB03463CB86ACE73BDAD6FEBA9E9657
                9963EBCB9B15454216BDEFE9FA1E91F153FE0A05F03BE07F8D6EBC37E2FF008B
                3E00F0F6BF6214DCE9D7BADC115CDB6E1B944885B284A9040600E083D08AEF7E
                157C60F0A7C74F06C1E22F05F89343F15E8374C522BFD26FA3BCB7665E1937C6
                480CA782A790782057F24DF14FC51E2DFDAE3E307C45F883358DC6A1A8DFDC5D
                78AB5B300DEB6104B72AACDED1A3CF1A0C0E015E8071FA99FF00069EFED4CBA7
                F88FE23FC1ABFB8654D4238FC57A3C6CD851247B2DEED47AB329B560073889CF
                38E3D3CCB856386C1BAF09B728DAEBA79FF5D8F332BE2C962B1AB0F3825195EC
                F5F97DFB7A9FB63451457C69F6A735F14BE31F843E06F86975AF1B78ABC37E0F
                D19E65B617FADEA70E9F6C6560C563124CCABBC856217393B4FA579E7FC3C87F
                677FFA2F5F05FF00F0B7D33FF8F57C2BFF000763FF00C999FC38FF00B1D07FE9
                0DD57E75FF00C12B7FE08C1AA7FC150BE1EF8B35FD3BC7F61E0F5F0B6A115834
                171A53DE7DA4BC5E6060CB2A6D03A6306BEA72FC8F0B5303F5DC4D57157B6D7E
                B63E4F30CFB174B1FF0051C2D25376BEF6E97F43FA27F867FB567C2EF8D7A87D
                8FC1BF127C03E2EBA39FDCE8BE20B4BF938193F2C5231E0577F5FCD3FEDD9FF0
                412F8DBFB02F81A4F1E25DE91E35F0AE90566BCD4B407956EB48008C4D2C2EA1
                9501C7EF232E17196D839AFAA7FE0801FF0005A9F15F887E2AE93F02FE2D6B57
                9E23B6D7CB41E17D7F509CCB79697014B2D94F237CD2C72052236625D5CAA7CC
                ACBE598BE1C87D59E2B0357DA456EBAF9FFC368184E259AC4AC263E97B393D9F
                47DBFE1F5D4FD8FF00895F15BC2FF063C2B2EBDE30F12E81E13D0E0748A4D475
                9D421B1B48DDCE154CB2B2A02C78009E4D79BFFC3C87F677FF00A2F5F05FFF00
                0B7D33FF008F57C61FF075B48C9FF04EEF07856650FF00112C430071B87F66EA
                6707F103F2AFCB9FF824F7FC123B51FF0082A7FF00C27DF60F1C597837FE104F
                ECEF33ED1A635EFDB3ED7F6AC636C89B76FD98FAE778E98E4CBB22C355C0BC6E
                26A38A4EDB5FAA5F986659F62A8E3D6070D494DB575ADBA5FD363FA15FF8790F
                ECEFFF0045EBE0BFFE16FA67FF001EAF4EF1C7C44D03E19F83AEFC45E23D7348
                D03C3FA7C626BAD4F50BB8EDACEDD090033CAE42282480093C923D457E2FFF00
                C4237E22FF00A2E1A2FF00E1312FFF0024D7DADFF05DEF0C47E08FF82267C47D
                1A29A5B88B48B2D02C92597979447ABE9C819BDC85C9FAD7257CBF02EBD1A384
                ACE7CF249E96B5DA5D52EE7650CC71EA856AD8BA2A1C916D6B7BD937E67D33F0
                47F6C9F84FFB4A6B177A7FC3FF0088FE0CF18EA3610FDA2E2D349D5A1BAB8862
                DC17CC68D58B04DCCA3763196033935E975FCFBFFC1A8DFF002910F19FFD93AB
                EFFD39E975FD045619DE5D1C0E29D083BAB27AF99D191E653C76156226ACEED6
                9E4148CC2352588000C927A0A5AE2FF68F729FB3CF8F0A9208F0EEA0411C11FE
                8D2579508F34947B9EB4E5CB172EC71B73FF000518FD9EECEE24866F8EDF06A2
                9A2628E8FE34D3559187041066C820F6AF53F08F8CB48F883E1AB4D6B41D574D
                D6F47D413CDB5BED3EE52E6DAE53246E49109561904641ED5FC727857C25A878
                D75396CF4CB692EAE61B3BABF744192B0DB5BC97133FD1628A463ECA6BF62FFE
                0D43FDACE46B9F885F05753BD91D0469E2AD06191FE58F0560BC45CFAEEB670A
                3D256C7535F639BF0A470B86957A537271B5D5BA1F1793F164F17898D0AB0515
                2BD9A7D7FAFD0FDA3AE1FE2DFED2FF000E3E00CF671F8F3E207827C1526A219A
                D135FD76D74D6B90A70C504CEBB80246719C66BB696558236776544404B31380
                A3D4D7F2B5FF00050CF8F5ABFF00C147BFE0A03F117C59A3B3DFE9F8BF93468C
                B929168FA5DA4B36E51CEDCDBDB493301C6F91CF726BC8C8F27FAFD59294B963
                1576FF002FD7EE3D8CFB39FECFA71708F34A4EC97E7FA7DE7F4D5F097F6A4F86
                5F1F753BAB1F027C46F01F8D6F6C2213DD5BE83AFDA6A52DBC64ED0EEB0C8C55
                49E3246335DED7F38BFF0006CB4EF17FC15174C55775597C37A9AB85380E3621
                C1F51900FE03D2BFA3AACF3BCB2380C4FB08CAEAC99AE459A4B1F86F6F38F2BB
                B5F91C0FC72FDA93E1BFECCD67613FC42F1D7857C171EAACE965FDB1A9C568D7
                6531BFCB5720BEDDCB9201C6E5CF51553E07FED83F0ABF696D4AEECFE1FF00C4
                5F0678C6FB4F8BCEB9B5D275686EA7823C81E6346AC5826580DD8C64E339AFC0
                AFF8387FF689BDFDA9BFE0A6D7BE10D10CBA8D8FC3F8ADFC29A75BC0778B8BE6
                60F71B57FE7A79F2F927FEB82D798FFC11FBF685BDFD877FE0A77E0ABAD6DA5D
                26CEE35493C25E238656D8218AE1BECEC25C7F0C5388A53EF0D7B74F8514B03F
                58E77ED1C79ADF8A5F77E2783578B5C71FF57E45ECD4B979B5F46FB6FF0081FD
                46579D7C70FDADFE17FECCF35945F107E20F83FC1B3EA6A5ED20D5F5586D66B9
                50705D11D833283C1603009033922BD16BF047FE0EC7FF0093CDF871FF00625A
                FF00E975D578392E5D1C6E2961E6EC9A7B791F419E6652C0E15E220AED35BF99
                FB9FF0D7E28F86FE32F82ACBC47E12D7F47F13E81A88636DA8E97791DDDACFB5
                8AB059109525581523390410704115C5F8E3F6E3F829F0C7C5579A17897E307C
                2DF0F6B7A7BF9775A7EA7E2BB0B4BAB66C03B5E29250EA7041C103AD7CD1FF00
                06E0FF00CA26FC0BFF00611D5BFF004BE6AFC1FF00F8281E9AFE24FF008298FC
                6BB133157BDF899AE5B091B2DB036A93A03F41C71ED5E9E5D9053C4E32B61A53
                6953BEBDF5B1E6665C43530D82A3898C13752DA76D2E7F4D1A7FFC143BE006AF
                7B1DB5A7C73F83D757131C2450F8CF4D7773D70009B26BD6B4BD56DB5BD3E1BC
                B2B9B7BBB4B84124534120923954F42AC3823DC57E0EFC73FF008353FE267C3D
                F86FA96B1E10F887E1EF1B6ABA740F703476D365D3E6BD0A33E5C2E5E45321C1
                0AADB41381B866BC1FFE0893FF000529F18FEC67FB58784BC2B73ACDFDCFC34F
                196AD0E8FAB68D713136B66F71288D6F22563889E3765672B8DE818104ED2BB3
                E1DC3D7A12AD97D6E771DD356FEBCB4D4C1712626857851CC28F2296CD3BFF00
                5E7AE87F4C55E7BF1D7F6B0F865FB3159453FC42F1EF84FC1C2E10C9047AAEA7
                15BCD72A3A98A263BE4C7FB0A6BE77FF0082DA7FC149A7FF0082737ECAC97BE1
                F303FC40F1ACEFA5F8744AA1D6CCAA069EF1948C3885590053905E58F20AE457
                E1A7EC5BFB087C66FF0082C7FC7CD7EFA1D665BD96074B9F1278B7C417324D1D
                B19376C5279792460AC1235C0017928A323932AC856228BC5E267C94D75EAFFA
                DBAEBA58EBCDB3F961EBAC1E161CF55F4E8BFADFA59753F7DF44FF0082CE7ECB
                7E20D59ECE0F8D9E0C8E647085AE6692DA2CE33C492A2A11C750D8AFA27C19E3
                AD17E23F86EDF59F0EEB1A5EBDA3DE2EEB7BED3AEE3BAB69C74CA4884AB0FA1A
                FC44F8BDFF00069978E3C33E0296FBC17F16741F167882188C874ABFD11F498E
                660325239C4F302C4E40DE88B9C64A8271F16FEC11FB77FC49FF0082547ED4AE
                77EAD69A5D86A874EF18F852772B1DDAC7218E6468CFCAB711E1B638C1565C12
                54B29EF5C3B83C55294B2DADCD28F47FF0CADF758E07C478DC2558C333A3CB19
                754FFE0B4FCF53FAA2A2B37C1FE2CD3FC7BE13D2F5DD22E63BDD275AB48AFECA
                E63FB97104A81E371ECCAC08FAD6957C734D3B33ED134D5D0514514861451450
                07C93FF05D9FF944D7C65FFB075A7FE97DB57E20FF00C10DFF006F5F03FF00C1
                3B3F6B1F10F8DBC7D16BB368FAA784AE74485749B54B99C4F25E594CA4AB3A00
                BB2DDF9CF5C71CE47EDF7FC1767FE5135F197FEC1D69FF00A5F6D5F8A7FF0004
                10FD89BE1EFEDE7FB5FF0089BC1FF1274CBCD5743D3FC1975ABDBC76D7B2DA3C
                77297B630ABEE8C827093C830720E470715F7BC3EE8ACA2BFD62FC9777B6F6B4
                763F3FE22F6DFDB343EAF6E7B2B5F6BDE5B9ED7FF0597FF82F8F85FF006EFF00
                D9F8FC2DF86DE18F10E9DA26A1A84377AB6ABAEA430CD3A40FBE38A186379301
                A408E6467560136ECF9891F407FC1AF9FB0278BBE0E58F8BFE3278C349BCD022
                F15E9D168FE1EB5BC85A2B8BBB4322CF35D14600AC4CC9008CFF001ED738DBB0
                B7C35FF05A0FF82603FF00C12D7E3FF86F53F03EA3AE49E08F1321BCD0EFE79B
                FD334ABD81D4C9019902FCC9BA29237C2B10C472632C7F617FE0851FF050EBEF
                DBF7F63C597C517B1DE7C40F02DC8D1F5E970AB25F215DD6D78CA3A79881958F
                01A4865200040179A3851C9D7F67AFDD4DFBCFAFF5756667957B4AD9CB798BFD
                EC16896DFD59DD77DCFCCEFF0083AE7FE5221E0CFF00B27563FF00A73D52BF4E
                3FE0DEAFF943FF00C21FFB8CFF00E9EEFEBF35BFE0EC1F0FCD6BFB7478035521
                BC8BCF0243689F210BBA2D42F9DB0DD09C4CBC76E3D457E88FFC1B91E39D33C5
                7FF049AF00E9F6372935DF862FB56D3B518C104DBCEFA8DC5D2A9C74CC373137
                3D9AB9F35D720C3B5D1AFCA474E54EDC43884FAA7F9C4FA03FE0A43FF28EFF00
                8F5FF64EBC43FF00A6CB8AFC10FF008370BFE52CBE05FF00B076ADFF00A413D7
                EEE7FC152BC5DA7F82BFE09BBF1D6F354BA8ACEDA6F036AF608F23001A7B9B49
                2DE04E7BBCB2C680772C2BF0ABFE0DB8D3A7BEFF0082AEF83A58627923B3D2B5
                59A765191121B291371F41B9D47D5854E41FF229C5BF27FF00A495C40FFE15F0
                8BCD7FE947F4A15FCE1FFC1CCFFF002948D5BFEC5CD33FF45B57F4795FCE1FFC
                1CCFFF002948D5BFEC5CD33FF45B572F06FF00BFBFF0BFCD1D7C69FF0022F5FE
                25FA9FB75FF04A1FF9469FC0CFFB12F4DFFD10B5F37FFC1CC3FB528F819FF04F
                C6F07595D1835AF8A9A947A4AAA3ED9058C244F74E3D54ED86261DC5C57D21FF
                0004A1FF009469FC0CFF00B12F4DFF00D10B5F89BFF072C7ED4A3E3C7FC14226
                F0958DC19745F857A747A32057DD1BDECB89EE9C0EC4168A16F7B6A8C9F05F58
                CE1DF68C9C9FC9E9F8D8ACEB1BF56C9A367ACA318AF9AD7F0B9F44FF00C1B5FF
                00B03699F193F656F8E5E28F13DAA9B1F88F672F806CE464CBC56BE4EFBB9101
                EB9925B7C1CF0F6C7A115F9EFF00B14FC5DD5FFE09B5FF000529F0C6ABAE674F
                B8F0178A25D13C4719DD84B6F31ECEF948EFB636919723EF2A9ED5EC5FB227FC
                1C1FF183F629FD9E7C3DF0D3C1DE0EF8512681E1B495609AFF004CBF92EEE1A5
                99E69249592F51598BC8DC85031818E2BE53FDAAFF0068ED53F6BAFDA07C4BF1
                1F5CD2740D1359F15DC25D5EDAE8B0CB0D90944688CE8B2C92382E537B65CE59
                988C0200FB5C2E0F152C4E23EB2BF775345AF6D3F15B9F0F8AC6E12385C37D59
                BF694F57A77D77F267F5F714AB3C6AE8CAE8E0156072187A8A757C95FF000441
                FDA8C7ED5BFF0004DBF87FAA4F3FDA359F0BDBFF00C22DAB64E584F661634663
                DD9EDCDBC84FAC86BEB5AFCA31342542B4A8CF78B6BEE3F5DC2E2235E8C6B436
                924FEF3F2B7FE0EC7FF9333F871FF63A0FFD21BAAC5FF834A7FE4DE3E2E7FD8C
                569FFA4C6B6BFE0EC7FF009333F871FF0063A0FF00D21BAAC5FF00834A7FE4DE
                3E2E7FD8C569FF00A4C6BEBA3FF24E3FF17FEDC8F8D97FC94CBFC3FF00B69FAC
                1AF68565E28D0EF74CD4AD6DEFB4FD4607B5BAB69D03C57113A9574753C15652
                4107A835FC8E6BCBFF000CA3FB6B5E8D26E645FF00856BE377FB1DC104B8FB0D
                F9F2DF00E73FBA07AFE35FD61FC73F8D7E1CFD9CFE11F883C71E2DD4ADF4AF0F
                786ACDEF6F2E2570BF2A8E1147F13BB6155472CCCAA01240AFE553E06F86F55F
                DB5FFE0A01E1DB38E06FED2F891E368EE2E562507C817379E6CF274C6D446918
                F1C043C76AD783138C2BCE7F0597A75FD0CB8D5A94E85387C7776EFD3F53F67F
                FE0EB9FF009477F833FECA2D8FFE9B354AFCC7FF008243FF00C15E7FE1D55FF0
                B0BFE2DEFF00C279FF0009E7F66FFCC77FB2FEC3F63FB5FF00D3BCDBF7FDABFD
                9DBB3BE78FD38FF83AE7FE51DFE0CFFB28B63FFA6CD52BE56FF83613F650F869
                FB4FFF00C2EFFF008589E05F0C78D3FB0FFB07FB3FFB5EC23BAFB1F9BFDA5E6E
                CDE0EDDFE5C79C75D8BE95D19654A10C8652C4C79A17D52D2FEF2B76EA73E6B4
                EBCF3F8C70D2E59DB46D5EDEEBBF47D0FA2BF63BFF00839CADFF006A9FDA83C1
                1F0E6E7E0B49E1B8FC67AA47A50D493C5BF6E368F26423793F628F702FB41F9C
                60127B60FD0FFF00070AFF00CA1FFE2F7FDC1BFF004F7615ECFF000EFF00E09C
                FF0001BE12F8D74FF11F863E11780342D7B4993CEB2BFB2D1A18AE2D5F046E46
                0B907048C8F5AF18FF0083857FE50FFF0017BFEE0DFF00A7BB0AF9EA55B07533
                1A0F074DC17346E9BBEBCCBCD9F49528E329E5B8858CA8A6F9656695B4E5F447
                E63FFC1A8DFF002910F19FFD93ABEFFD39E975FD0457F3EFFF0006A37FCA443C
                67FF0064EAFBFF004E7A5D7F4115BF17FF00C8C5FA239F837FE45ABD585717FB
                48FF00C9BC78F7FEC5CD47FF0049A4AED2B8BFDA47FE4DE3C7BFF62E6A3FFA4D
                257CDD1FE247D51F4D5BF872F467F35DFF000425F0569DF127FE0A91F0EFC3BA
                CDBADDE91AFD8F8834DBE80F49E09B41D463910FD5588FC6B33F672F196A9FF0
                49DFF82B169ADAC4D2A47F0DFC5B3689ACCBB1945D69CCEF6D3CA146721ADE43
                2A0E79D87B0AEA3FE0DEAFF94C07C21FFB8CFF00E992FEBDC3FE0E95FD9647C2
                EFDB13C3DF136C6029A77C4DD2FCABC654F97FB42C8242C49E8375BB5B607728
                E79E71FACE22BC5E68F0753E1A94FF0014E5FA5CFC870D8792CA96369FC54EA5
                FE4D47F5B1FA81FF0005C3FDAE53F652FF00826F78D354D3EF163D6FC6908F0B
                E8AF1BF264BC46124884774B659E4523F8956BF233FE08CFFB2BFF00C24DFB25
                FED71F18750B76369E18F85DAF7877497743B1AEAE34C9E5B8756FEFC70A46A4
                7A5DFD2BC4FF006EAFF8290EB5FB66FECEDF01FC0D7C6E953E16F871AC351791
                F22FEF848604989EAF8B382D8EE3CEF9661FED37ECA7C0BFD95BFE190BFE0DDE
                F1C7872E6D4DAEB9AB7C2EF1078835A5600482F2F34C9E528F8FE28E33145FF6
                C8726BC2FABBCAF011A12F8EA4ECFD13FCACBFF263DEFAC2CD73075E3F05285F
                E6D7E777FF00929F99DFF06CC7FCA52349FF00B17353FF00D16B5FBFFF00B507
                C77D37F661FD9DBC69F10756DA6C7C21A45C6A463638FB43A2131C20FAC8FB10
                7BB8AFC00FF83663FE5291A4FF00D8B9A9FF00E8B5AFBC7FE0E9DFDA90FC37FD
                933C2FF0BAC6E365FF00C48D4FED77C8ADCFD82C8A485580FEF5C3DB919EBE53
                7A718E7F8478ACE69D0EE95FD2EDBFC0DB87B18B0B9254AFD5376F5B24BF13F3
                C3FE0865F03AFF00F6D5FF0082AF683E20F106FD4E3F0E5D5CF8F35B9E4C9334
                F1C81E2627A126F2581883D406AADFF07067ECC67F66AFF8297F8B6EECEDCDB6
                8FF10A38FC5B625570BE64E596E86471BBED51CEF8EA048BEB93E73FF04EAFF8
                2A678EFF00E09957BE2ABAF01F873C09AC5E78B92DA2BBB9F1059DD5C4904701
                90AA45E4DC421558C84B641C944E98A7FF00C1453FE0AA5E3BFF00829ACBE159
                BC79E1BF01E9179E1017296773A059DDDBCB2A4FE5978E5335C4A1941894AE00
                2096E7E635F4EB0F8B599FB7497B2E5E5DFE77B7AE9E87CB3C4E0DE57EC5B7ED
                79B9B6F95AFE9AFA9FD157FC130FF6A4FF0086C8FD84BE1CF8F669C5C6ABA869
                6B69AB1CE5BEDF6C4DBDC13DC6E9236719FE1753CE735F91BFF0763FFC9E6FC3
                8FFB12D7FF004BAEABD47FE0D3CFDA97CCB7F88FF06750BA25A3F2FC57A2C4CC
                4FCA76DBDE019E0007EC8C147F79CFA9AF2EFF0083B1FF00E4F37E1C7FD896BF
                FA5D755F3195E0BEAB9F4A8ADB56BD1ABAFF0023EA735C6FD6F208D66F5F753F
                54ECFF00CCFD04FF0083707FE5137E05FF00B08EADFF00A5F357E147EDC5731D
                9FFC152BE304D348914517C54D69DDDD82AA28D5E7249278000EF5FBAFFF0006
                E0FF00CA26FC0BFF00611D5BFF004BE6AFC1DFDBFF00483E20FF008297FC6CB0
                12089AF7E26EBB6E1C8C84DFAACEB9C77EB5DD90FF00C8D3177EEFFF004A670E
                7FFF0022AC1DBB2FFD251FD167ED0DFF00058DFD9D3F67FF00857AAF893FE16C
                780FC5B75636EEF6BA478775DB6D52F750980F92154819CA6E6C0DEE02AE4927
                00D7F3A9FF0004FF00F849ACFED53FF0501F86FA169F6B2CB75ACF8AAD6FEF8D
                B2122D6DA39C5C5CCDCE70B1C4B2373E80739AFD1AD1FF00E0D1AD564D4621A8
                7C73D3E2B4CE6436FE1577908F401AE80E7D49E3D0F4AFD10FF8276FFC124BE1
                4FFC136F4CBB9FC236B7DACF8B35487ECF7FE23D59924BD962DC18C31850A90C
                5B803B5065B6AEE67DAB8F3E866196E5987A91C1CDD49CFC9A4B7B745DFE67A3
                5F2ECCF34C45396320A9C21E69B7B5FABEDE563F293FE0EB6F88571AE7EDC7E0
                8F0DF9AED61E1EF06C572B19E024F737773E611F58E183FEF9F6AFD05FF836C7
                E18D87813FE0959E15D62D628D2EFC69AB6A9AB5EB8E5A478EEE4B25CF1D92D1
                3039FD6BE03FF83AFBE1ADC687FB687803C5621D963E22F082D82BE0FEF2E2D2
                EE76939CF68EE60181FD6BEE1FF8364FE3AE9DF127FE09B569E1186EE27D57E1
                CEB37B63736D91E6450DCCEF7914840E76BB4D32827A989C76C0ACC537C3F45D
                3DAEAFF8FEA465AD2E22AEAA6ED3B7FE4BFA1FA255FCD37FC1C67F0FEC3C09FF
                000554F1A4DA7C6B0AF8874FD3B559D1142A899ED923761EEC62DE4F76735FD2
                CD7F2FDFF05DBFDA0F49FDA3BFE0A71F10B53D06EA0BED1B436B7F0FDB5CC443
                25C35AC2B1CCC08E197CFF003806190CA011C1AE4E0B8CBEBB292DB95DFEF476
                71BCA0B0314F7E656FB9DCFDDFFF00822CF8BEEBC6FF00F04B2F8297B78CCF34
                3A00D3D4939FDDDB4D2DB463F0489457D455E05FF04B5F84177F01FF00E09DDF
                07BC31A8406DB50B2F0D5B5C5DC0410D04D700DC491B03CEE579581F706BDF6B
                E6F1F28CB1351C767276FBD9F4D97C651C2D38CB7518DFEE4145145721D81451
                45007CA9FF0005B9F0EEA1E2BFF82587C60B1D2EC2F352BD974CB764B7B585A6
                95C25EDBBB10AA09202AB31E38009E82BF2DFF00E0D5BF01EBBA27EDF3E35D42
                F346D56CEC17C01776E6E67B4923844ADA8E9CCB197202EE2A8E40CE48463D8D
                7EF9D15ECE1338743035305CB7E7EB7DB6E96F23C4C5E4CABE3A9E35CEDC9D2D
                BEFD6FE67C83FF0005C7FD8E24FDB37FE09EBE2DD334CB192FBC53E1103C4DA1
                470C6649A59ED95BCD85147CCCD2DBB4C8AA3ABB2704802BF243FE0DD1F8B3E3
                1FD9DBFE0A2BA678766D0F5F5F0F7C45B49B45D5A3363284B79115A6B7B86180
                014910A1278559E435FD16D15782CEDD0C154C14E1CD195FAED7F97CC8C6E46A
                BE369E3A13E5946D7D2F7B7CFB69E87C3DFF0005CBFF00825DDFFF00C147FF00
                67DD267F08B5A27C46F024D35D68C97328862D4E099544F68CE78466F2E26466
                F94347B4950ECCBF875F05FF0068CFDA4FFE0905F12B52B5D317C4FF000EEF2F
                9C47A8E8FADE964D8EA453A318A6528E40FBB2C6436D3C3E09CFF54D515ED8C3
                A95ABC1710C57104A30F1C8A1D5C7A107835B659C432C350785AD4D54A7D9FFC
                33F5D8C334E1C8E2ABAC551A8E9D4EEBFE1D6BD37D8FE5D3F680FDBE7F69CFF8
                2B25F69FE0ED467D73C636B0CE92DBF86FC33A314B769B042CB2470296723E6C
                190B05F988DBCD7EB77FC102BFE0905AD7EC19E1DD5FE227C46861B6F891E2DB
                31610E991C8B37F60D86F591A3775255A695D232C1490AB1A0CE4B01FA37A6E9
                96DA459A5BDA5BC36B6F1E76450A0445C9C9C018032493F8D4F55987113AD87F
                AAE1E9AA707BA5D7F05F3FCC59770DC68623EB789A8EA4D6CDF4FC5FCBF20AFE
                76FF00E0E5AF879AFEA3FF000533BDBF8342D626B1BCF0E69DE45C4765234536
                D5756DAC061B07838E86BFA24A2B8327CD1E0311EDD479B46AD7B7F99E867395
                ACC30FEC1CB9754EF6BFF91F34FEC17AC5FF00C14FF824FF00C35D4AFF0046D4
                E4D47C2DF0F2DEF25D2FC864BB91E0B3DE611191B83B15DA0633922BF047FE09
                E7FB37F8BFF6FCFF0082A1785A6F17689AADDC1AFF0089A4F1578A6E66B1905B
                B449235E5C090B0C2ACAC3CA193D6551C9233FD42515D381CF1E17DB3843DEA9
                D6FB6FE5E7E472E3B218E2BD84673F769DB4B6FB79E9B7998BFF000ADFC3BFF4
                00D17FF0062FFE26BF387FE0E5FF00D89E2F8A9FB17E8DE39F0AF87A37D6FE1C
                6AC25B94D3ECFF007AFA7DD0114C76C632DB655B6624F0AAAE781935FA714579
                D81C754C2E2215E3AF2BFBFBAF9A3D2C7E029E2B0F3C3CB4525F7767F267E22F
                FC1A9FF15BC49E0DF8BDF123E19EA7A56B10689ADE971F88ADA59AD24586DEEA
                DE4481D771180658E753CF5FB38EFC1FDBAA28AD334C7AC6621E214796F6D2F7
                FF00233CA72F782C32C3B9F35AFADADBEBE67E647FC1D25F0CFC47F11FF634F0
                18F0F683ACEBADA7F8C525BA5D3ECE4BA6B746B3B950EE1012AA588193C6481D
                48CFE55FEC71FB587ED53FB04F8535CD1BE1868BE27D0AC7C43729777826F07F
                DB59E554D8AC0CD0B6303B0E2BFA8BA2BD3C0710AC3E13EA9528A9C6F7D5F9DF
                6B33CBCC3875E2317F5CA759C256B68BE5BDD1FCC67C4993F6DFFF0082A4EA76
                5A4F88F47F8B1E3AB5827568AD9F453A5E8F04DF743C812386D11C0246F7C100
                B720135FA97FF0446FF821CCBFB076AA7E26FC4B9EC351F89D776AF6DA7D85AB
                09ADBC37148312625E925C3AFC8CCBF2AA97552E18B57E94514B1DC4956BD0FA
                B5182A707BA5F97A7A21E038668D0AFF0059AD37526B672FCFD7E67E71FF00C1
                CFDF0CBC47F13BFE09EFE1C87C37A16ADAF4DA5F8EEC6FAEE2D3ED5EE24B783E
                C3A843E632A0242F99344B9F5917D6BF213F62CFDA3FF69DFF00827DFF00C24B
                FF000ABBC3FE27D17FE12EFB2FF69FDA3C22D79E77D9BCEF2B1E6C2DB71F6897
                A633919CE057F52F453CBB88161B0BF54A9494E37BEAFE7B598B31E1D789C5FD
                729D67095ADA2F96F747F3BBFF000FAEFDBDBFE79EB5FF0086FE1FFE47AFD4CF
                F82C87867C59F1B3FE08B3E3AB7B6D0EFF0050F166A5A3689A85D69B656AED3A
                3C7A858DC5C858B96FDDAA4AC4724043E95F6B515862738A53AB4AAD1A118383
                BE9D6CD3D745D8E8C2E4D5614AAD1AD5E5514D5B5E974D69ABEE7F283FB28F8E
                3F68CFD883E21DEF8AFE17E8BE34F0C6BFA869CFA4DC5D7FC2282F7CCB679629
                5A3D9716F220CBC311DC1770DB8CE0907E85B6FF0082C1FF00C1415EE63035CF
                1A4A4B0023FF008575A7FCFEDC58679F6AFE8E68AF4EB714D1AB2E7AB858C9F7
                766FF1479547852B518F252C54E2BB2BA5FF00A515B489EE2E74AB592EE15B7B
                B922469A256DC227206E50475C1C8CFB5731FB41D8CDA97C04F1BDB5B452DC5C
                5C6817F14514485DE476B69005503924920003D6BB0A2BE4632B4948FB0946F1
                713F9ABFF837D7E187892C3FE0AE1F0C2F27F0FEB705A6969AC3DE4D258CA91D
                AA9D22F6306462B8506474519FE2603A9AFD7AFF0082FF007EC8575FB5A7FC13
                BBC41FD8F6171A8F8A3C0773178974B82DE1324F70220D1DC44A17E639B79256
                0A01DCD1A0C67047DB5457BB8ECFA788C6D3C6C63CAE16D2F7D9B7E5BDEC7838
                0E1F861F035305297329DF5B5B749777B5AE7F2B9FF04B9FD86F5DFDABFF006E
                BF87DE14D4FC37AB1F0E26A91EA1AFBDC59C890258DB9F3A649188C2F9813CA1
                9EAD2A8AFE90BFE0A15A4DDEBFFB02FC71B0B1B6B8BDBDBDF87FAF416F6F046D
                24B3C8FA75C2AA228C9666240000C92715EC3454E6B9ECF1B5E159C79543657B
                F5BBFBF42B29C86181C3CE8A97339EEED6E965F76A7F3B7FF06D3FC3AF10E99F
                F0534B3BDB8D0B59B7B3B2F0EEA22E2792CA548A0DCAAABBD8AE172DC0C9193C
                5607FC17FBC6BE34FDA6FF00E0A7FE26D32CFC3BE25BBD3FC211DB786345B78F
                4E9DDA708BE648E8A17E6F32E25976951F3284EBC57F48745777FACFFEDCF1AE
                96BCBCA95F6D77D8E1FF0055BFD8160555D39B99BB6FA6DB9E19FB0B7EC71A17
                ECB5FB1FFC3BF015CE8BA44FA8F87745862D4657B58DCCB7AE0CB74F920F0679
                252393C11C9ABBFB63FEC7DE1DFDA77F657F1F7805746D1EDAEBC4FA2DC5A594
                E2D638CDB5DEDDD6F2E40C8D932C6DFF0001AF66A2BE71E2AAFB5F6F7F7AF7F9
                DEE7D22C252547D85BDDB5BE56B1FCB6FF00C12C3C59E36FD8EFFE0A4BF0DB5A
                B8F0BF896DDEDB5E5D0B58B36D3E6122DBDD136B3AB26DC96412170BFDE8D6BE
                BAFF0083AC7E1F6BDAE7ED63F0E353B2D1356BCD3BFE112FB2FDAA0B492487CD
                5BCB8631EE5046E0ACA719CE08AFDD8A2BE8EA713F363618DF656714D5AFBFE1
                D0F9BA7C2DC9829E0BDAE9269DEDB5BE7D6C7C45FF0006F0F86752F0A7FC12A3
                C076DAA69F7DA65C497BA9CEB15D40F0BB46F7D314701803B58720F422BF103F
                6D1F843E2CBAFF0082AD7C528A2F0BF88A496FBE28EA9716C8BA6CC5AE239354
                95E374017E657460CA4705482320E6BFA9CA2B9B03C412C3626AE2792FED2FA5
                F6BBBF63A71DC3D1C4E168E1B9EDECEDADB7B2B770A28A2BE74FA43E57FF0082
                B9FF00C1376CBFE0A57FB303F86ADEEED74AF19F87EE0EA7E1AD46E01F262B8D
                BB5E094A82C21957E562012AC237C36CDA7F002D742FDA6FFE08EDF1AAE75386
                C3C61F0CB5C857ECD2DE0B513E97A9C5B8614B957B5B98CB018E5806F4615FD5
                3D15F419571054C1D27879C14E9BE8FF00A7A7958F9DCDB8769E32AAC4539BA7
                51755FD2D7CEE7F32FF157FE0BBBFB56FED39E1493C1B1F8B9ACE2D5E336D3C5
                E1AD1E3B6BEBE561828248D4CAB9FF00A64549C91D38AF74FF00823CFF00C100
                BC6DF163E29E8BF107E367872F3C27E03D0E74BEB7D0B5487CAD43C432A30648
                E481BE686DF232FE600CE06D55C3175FDE7D23C3BA7E80AE2C2C6CEC44A41716
                F0AC41F1D33B40CF7ABB5D75F89F968BA382A2A95F76B7FC97EA7250E16E6AD1
                AD8EACEADB64F6FC5BFBB40A28A2BE50FAD0A28A2800A28A2800A28A2800A28A
                2800A28A2800A2835E51E22FDB5BE1EF857C417DA5DEEAB751DEE9B7125ADC20
                B19982488C558021707041E4574E1B075F10DC6841C9AEC9BFC8E1C766784C14
                54B175634D3DB99A57FBCF57A2B8AF84FF00B41785FE365CDEC5E1DBD9AEDF4F
                5479C3DBBC5B431207DE033F74F4AED6B3AF42A519BA7562E325D1AB335C2632
                862A92AD869A9C5ECD3BAD34DD051451591D2145145001451450014514500145
                1450014514500145145001451450014514500145145001451450014514500145
                1450014514500145145001451450014514500145145001451450014514500145
                14500145145001451450014514500046457E657C7A19F8E7E34FFB0EDF7FE943
                D7E9AD7E247ED5DFF0714EADF047F6A3F895E0B8FE077C35D5E3F08F8AB54D15
                6FAE8BF9F7A2DAEE5844B2617EFBECDC7DC9AFB2E0DC4D5A35AA3A54F9EE9754
                ADAF99F9C788D93D3CC30D46152AFB3B49BF85CAFA791F7E7FC1317FE43BE30F
                FAE16BFF00A14B5F5DD7E7A7FC1107FE0AA17BFF000519F14FC44B2BBF875E12
                F028F0A5AD8CEAFA316DD79E73CEA43E40E17CBE3FDE35FA164E2BCBE26AB3A9
                98CE7523CADDB4BDFECAEA8F73827030C1E4F4B0F09F3A5CDAD9ABDE4DECF50A
                01CD7C77FF00050FFF0082DBFC1DFF00827AEA12F87F53B9BBF18F8F11031F0E
                E8AC8D259E46E53752B1D9002083B7E6930CA761539AFCE7F167FC1DA1F13AF3
                5891F42F855E04D3AC0FDC86FEF2EEF665FAC88D083FF7C0ACF07C3F8FC4C3DA
                5387BAFAB695FEF3D0C6F116030B3F67567EF2E89376F5B1FBB9457E08FF00C4
                58FF0019FF00E89C7C30FF00BE2FBFF9228FF88B1FE33FFD138F861FF7C5F7FF
                00245767FAA3997F2AFBD1C5FEB7E5BFCCFEE67EF7515F823FF1163FC67FFA27
                1F0C3FEF8BEFFE48A3FE22C7F8CFFF0044E3E187FDF17DFF00C9147FAA3997F2
                AFBD07FADF96FF0033FB99FBDD457E08FF00C458FF0019FF00E89C7C30FF00BE
                2FBFF9228FF88B1FE33FFD138F861FF7C5F7FF002451FEA8E65FCABEF41FEB7E
                5BFCCFEE67EF7515F823FF001163FC67FF00A271F0C3FEF8BEFF00E48A3FE22C
                7F8CFF00F44E3E187FDF17DFFC9147FAA3997F2AFBD07FADF96FF33FB99FBDD4
                57E08FFC458FF19FFE89C7C30FFBE2FBFF009229D0FF00C1D91F18D664327C36
                F866F18605D545F2B30CF201F3CE0FBE0D1FEA8E65FCABEF43FF005BF2DFE67F
                733F7B01A2BF37FF00E09E9FF0720FC35FDADFC6161E0FF1E68EFF000BFC5BA9
                C8B6F612CD782EF49D46662156313ED468646278591769E9E612403FA3E0E6BC
                4C6606BE167ECF111E57FD6CCF6F058FC3E2E1ED30F2E65FD6EB7168A28AE43B
                028A28A0028A28A0028A28A0028A28A0028A28A0028A28A0028A28A0028A28A0
                028A28A0028A28A0028A28A0028A28A0028A28A0028A28A0028A28A0028A28A0
                02BF927FF8290FFCA443E3D7FD945F10FF00E9CEE2BFAD8AFE49FF00E0A43FF2
                910F8F5FF6517C43FF00A73B8AFB8E07FE3D5F45F99F0BC75FC0A5EAFF0023F4
                7BFE0D1C38F889F1C3FEC1DA47FE8DBBAFBDFF00E0B73FF0505BDFF827CFEC61
                77ABF87658E2F1D78BAE8687E1F76557FB148C8CF2DD946E18451A9C6411E63C
                5904122BE08FF83473FE4A27C70FFB07691FFA36EEBABFF83BA346D427F09FC0
                8D422598E956B77ADDB5CB027CB1348960D1023A64AC5363BE037BD18DC342BF
                112A557676FC237B7CEC4E0713530FC37ED697C4AFF8CED7F95CFC57D6758BBF
                116AF737FA85D5CDF5F5ECAD3DC5C5C4AD2CB3C8C72CEEEC496624924924935E
                E1F003FE0981F1FF00F6A2F08A6BFE06F857E28D6B439B260D41A24B4B6B919C
                131493B22CA33C65091907D0D78D7826FF004DD2FC67A45CEB36AF7FA45B5EC3
                2DF5B21C35CC0AE0C918E472CA08EA3AF5AFEBBBF668F8EDF0FBF685F841A36B
                FF000CB5AD1358F0A3DAC51DA2E98C8A9628100581A25C1819000A62655298C1
                0315F47C419CD5CBE1074617BF57B2F2D3B9F37C3B92D1CC67355A76B745BBBF
                AF63F9B3FF008713FED65FF446B5AFFC19587FF1FA3FE1C4FF00B597FD11AD6B
                FF0006561FFC7EBFA8A073457CB7FAED8CFE48FE3FE67D67FA8F82FE797DEBFC
                8FE5D7FE1C4FFB597FD11AD6BFF06561FF00C7E8FF008713FED65FF446B5AFFC
                19587FF1FAFEA289C519A3FD76C67F247EE7FE61FEA3E0BF9E5F7AFF0023F975
                FF008713FED65FF446B5AFFC19587FF1FA3FE1C4FF00B597FD11AD6BFF000656
                1FFC7EBFA8AA0D1FEBB633F923F8FF00987FA8F82FE797E1FE47F2EBFF000E27
                FDACBFE88D6B5FF832B0FF00E3F47FC389FF006B2FFA235AD7FE0CAC3FF8FD7F
                5121B34B9C9A3FD76C67F247EE7FE61FEA3E0BF9E5F87F91FCBAFF00C389FF00
                6B2FFA235AD7FE0CAC3FF8FD719F1C3FE094DFB44FECE3E0AB8F11F8C7E13F8A
                74CD0ACD4BDCDF4291DEC36883ABCAD03BF96BFED3E07BD7F57B4CB8B74BA81E
                2955648E4051D5802AC0F50477AA871B62EEB9A11B7CFF00CD933E07C234F92A
                493F93FD11FC6157F46FFF0006E6FEDEBAA7ED7FFB1FDEF85FC537F36A5E2FF8
                573C3A64D793B979AF6C25563692C8C79671E54D1127248855892589AFC1BFDB
                9BC13A5FC34FDB67E30F87344B48AC345F0FF8DF5AD36C2D63184B6B786FE78E
                38D47A2A2803E95FA39FF0694DCC89FB407C5E8448E2193C3D66EC818ED66172
                C0123D4066C1ED93EB5F49C4B4A9E232C759AD559AF9DBF467CC70BD6A987CD1
                514F495E2FE57FD51FBA345216C519AFC9CFD745A2BCD3E3C7ED63E11FD9CBC6
                BF0F340F1249A826A1F13B5C5F0F68A2DADBCD46BA6008F30E46C5E40CF3D6B8
                AF8F9FF0534F84BFB32FED47E10F843E32D66F749F16F8DADEDAE34D66B366B2
                0B71712DBC3E64C384DD2C2EBCF0382480735D14F0B5A76E48B774DFC96FF71C
                F53174617E79256697CDEDF79F40515E5DFB557ED81E0CFD8D7C2DE1BD63C6F3
                DFDB58F8ABC436BE18B16B4B56B863797092BC6180E55310BE5BB71EB5E3DFB4
                27FC1663E12FECC7E3FF0010F87BC53A57C4B8A6F0C4BE4DF5EDBF852E65B053
                80772CF808CBF30F9B38AAA382AF56CE9C1BBF65D88AD8EC3D1BAAB34ADDDF73
                EB2A2BE62FD97BFE0ACBF0CBF6BBF893A4F863C25A57C444BAD6EDE4BBB4BCD4
                BC31716761246B1197779EC3661907CA73F36463AD79F787BFE0BFFF0002FC61
                A5A5FE8FA4FC59D5AC242CB1DD59F82EEE786420E0E194107041AD16598B7271
                F66EEAD7D3BEDF933379A6114549D4567B6BDAD7FCD7DE7DBD457CF9FB427FC1
                4C7E1AFECC7F087E1EF8CFC531F8AE3D3FE27242DA1DA5AE8B2CFA848D2C0B3A
                C72403E64902BA82A790DC6322B2FF00662FF82B5FC17FDAB7E2D7FC203A16AF
                ADE89E379216B8B6D13C45A3DC695777B1AA97668448BB5C8505B686DDB55982
                9552442C0621D375541F2AEB6EDBFDC5BCC30CAA2A4E6B99DB4BF7DBEF3E97A2
                BC9BF64DFDB47C15FB6668DE29BAF083EA914FE0BD7AE3C39AC596A7686D6EAC
                EEE1237064C9F94E7839ECC0E0A9024F1F7ED8FE0DF877FB537823E0E5E36A77
                3E37F1ED95D6A36105ADB79B0DB5B408ECF2DC3E4796A7CB9029C1CB291D7159
                BC3555374DC5DD6AD79257FC8D56269382A8A4ACF44FCDBB7E67AAD15E59FB27
                7ED89E0DFDB3FC25E24D6BC1526A52597857C4375E18BEFB6DAFD9DC5E5BAC4D
                205193B936CC84377CF418AE53C63FF0530F851E05FDB6F45FD9FF0050D5AF13
                E216BB02CB0A2DB6EB382478DA58A0965CFCB2C88B955C1CEE4E416505AC2567
                395351778ABB56D92EA4BC5D0508D4725693B27DDBE87BF515E6BFB5B7ED5FE1
                1FD8A7E066A9F10FC712EA117877489608673656C6E272D34AB12054C8CFCCE3
                3CF4CD7A1691AA45ADE956D7B016305DC493465860956008E3E86B274E4A0AA3
                5A3D2FE9BFE68D555839BA69EAB5B793DBF2658A28A2A0D028A28A0028A28A00
                28A28A0028A28A0028A28A0028A28A0028A28A0028A28A002BF927FF008290FF
                00CA443E3D7FD945F10FFE9CEE2BFAD8AFE49FFE0A41FF00290FF8F5FF006517
                C43FFA73B8AFB8E07FE3D5F45F99F0BC75FC0A5EAFF23F47BFE0D1CFF9289F1C
                3FEC1DA47FE8DBBAFD3BFF008298FEC23A57FC144BF64CD73E1EDECF0E9FAB6F
                4D4B41D46552CBA6EA110611C840E7632BC91BF04EC95B032057E627FC1A3871
                F113E387FD83B48FFD1B775FB739C9AF3788EB4E966D2AB4DD9AE56BEE47A3C3
                34215B27852A8AE9F327FF008133F8F8FDA33F671F197ECA1F17355F03F8F343
                BBD07C43A4485648655F9274C90B344FF76489B19575C823E95C8695ACDE6877
                466B2BBB9B39882BBE095A36C77190738AFEBA3F6A0FD8CBE187ED9DE0D4D0BE
                26783748F155941B8DB493A18EEAC8B632609E32B2C44E067630CE06722BE12F
                197FC1A9FF000175CD6E5B9D27C61F13F43B6958B7D8D6F6CEE6287D0233DBEF
                C0FF006998FBD7D360B8CB0B3825898B8CBAD95D7F9FF5B9F318DE0BC542A5F0
                B2528F4BBB35FA7F5B1F82BFF0B1FC45FF0041ED6BFF000365FF00E2A93FE163
                F887FE83DAD7FE06CBFF00C557EEAFFC4271F067FE8A3FC4EFFBEAC7FF008C51
                FF00109C7C19FF00A28FF13BFEFAB1FF00E315D9FEB5E59DDFFE0271FF00AA59
                A765FF008123F0CF48F8C1E2DF0F6A70DED878A7C4563796EDBA29EDF529A296
                23EAACAC083F4AFBD3FE09EBFF000718FC5CFD98F5FB0D1FE275FDFF00C54F02
                1611CC6FE4126B7609DDE2B96F9A62064EC9CB67A074EB5F597C59FF00834BFC
                1971E0EB9FF8417E2AF8A2CFC40885ADC6BD6905CD9CCD8E11BC958DD013C171
                BF19CED38C1FC5CF8C9F08F5DF809F15FC43E0BF13DA7D835FF0BDFCBA75FC01
                830496362A76B0E194E3218704107BD7452C465B9B4254D252B77566BCD7FC03
                9AB61F33CA271A926E37ECEE9F93FF00827F5E1F043E36F85FF68CF853A278DB
                C19ABDBEB9E1AF10DB0B9B2BC872048B920A953CABAB06565601959482010457
                C65FF0587FF82DDE83FF0004E8B35F07784ED34FF15FC58BF844DF629E46365A
                044CB949AEB610CCCDC14855958A9DC59415DFF19FFC1B15FB6C5D7C34F077C6
                AF036B77135DF87BC29A04BE3EB1B732F36AB6F84BC5407A070D6E78180C09EA
                D5F957F1AFE2FEB9F1FF00E2EF893C6DE25B9377AEF8AB519B52BD979C192472
                C5541FBA8B9DAABD02800702BE672EE1787D7EA53ADAC216B79DF6BFA75FF23E
                9F32E2A9FF0067D3A94349D4BDFCADA3B7AF4F23D13F68CFF828E7C71FDABF5C
                9AF7C71F12FC55A9C72B6E5B082F1ACF4E83D365AC3B215E00E76E4E3924F35E
                59FF000B1FC439FF0090F6B5FF0081B2FF00F155F69FFC123BFE0883E20FF829
                8681A9F8C352F13C5E0CF006917C74D3751DAFDAAFB52B9554778E18CB2AA22A
                BA66462796002B61B6FE810FF834E7E0CFFD147F89DFF7D58FFF0018AFA4AF9D
                E59829FD5DBB35D12DBEED0F9AC3E479A6360B109369F572D5FDFA9F855FF0B1
                FC43FF0041ED6BFF000365FF00E2ABEB1FF821B78DF5AD53FE0AB1F07ADEEB57
                D52E609350BA0F1CB7523A3FFA05C9E413835FA49FF109C7C19FFA28FF0013BF
                EFAB1FFE315E95FB207FC1BA9F0BFF00635FDA43C2DF13342F1BF8F754D5BC29
                3C9716F6B7ED69F6798BC324243EC855B18909E08E40AF3F1BC4D9754C3D4A70
                6EEE2D2D3AB47A381E17CCA96269D49A56524DFBCB64D1F85FFF0005213FF1B0
                FF008F5FF6517C43FF00A73B8AFB3BFE0DBA95A0BDFDA2DD1991D7C0E84329C1
                1FBE6AF8C7FE0A41FF002910F8F5FF006517C43FFA73B8AFBB7FE0D66F0841E3
                FF008ABF1BF44B996582DF55F09DBDB492458DE8AD704123208CFE15EAE3AA46
                9E5AA73D9283FB9C4F170D4675B1F3A54FE2973A5EAD4AC7D2BFDBF7C7FE5F6E
                FF00EFF37F8D7E807EC497125D7ECC7E19796479246FB56598EE27FD2E61D6B8
                6FF8768F853FE83DE21FCE1FFE22BDAFE11FC33B4F83BF0F74FF000ED8CF7173
                6BA7799B259F1E63EF95E439C003AB91D3A0AF9AE2ACFB038EC2469619DE4A49
                ED6D2CD7EA75787BC1F9AE5598CF118E4945C1C55A49EAE517F9267C71FF0005
                78FF0093A3FD8C3FECABC1FF00A0A5787FFC1507F639B2FDBB7FE0AFD71F0EE7
                922B4D4AFF00F67F6BBD12F5F23EC1A8C3ADDCBDBCB91C85DC36311CEC91C77A
                FAB3FE0A2FFB33F8D7E3D7C79FD9975AF0A68EBA9E9BF0F3E20C5AE788253790
                41F60B3555CCBB64756939046D8C337B51AFFECCDE35BFFF0082D4E89F1722D1
                C37C3EB3F848FE189754FB6400AEA27549A710793BFCE3FBA756DE136751BB3C
                578983C6468D3A73849294613EBD79AEBEF3EF71B8275AA5484E0DC653874E9C
                A93FBBB9F9FDFB52FED91A8FED6DFF0004C6F81169E2C49ECFE26FC35F8E5A1F
                84BC636775C5CA5EDB5ADFA79EE3AE6551927A798B281F76BF453FE0B363FE35
                6FF1B3FEC5C93FF46257C89FF054EFF823C7C48F8AFF00B70F85FE23FC20B2B7
                BDF0D78B35FD2757F1BE96D7F059A5B5ED83B471EA1B6465DE0DBCD30223DCFB
                8C87693271F717FC14CBE0DF88FF00685FD82BE28F82BC21A70D5BC4DE23D15E
                D34EB337115BFDA252EA42EF959517383CB301EF5AE26BE19CF0D3A524973733
                5FCB771BA7D9277B791961A862543150AD16DF2A8A7FCD652B35DDBD2FE65CFF
                00826FFF00CA3BFE02FBFC3AF0F7FE9B2DEBF3A3FE08A3FB607C68F845FF0004
                F5F0A687E0DFD99BC4BF12740B6BDD45E0D7ACFC5163630DD33DDCACEA22946F
                1B189539EA573DEBF4D3F62DF875AB7C1EFD8E7E1378475FB75B3D7BC2BE0DD1
                F48D4A05956510DCDBD8C30CA81D495601D1865490719048AFCF5FF827DC3FB6
                B7FC13E7F661D23E16D97ECABA4F8B6D743BABB993547F88BA4DA1B8F3AE1E5E
                23F349006FC0C9CE072074AC70B2A738622368CAF28B4A52E54D7BFAA7CD1BEE
                BAF536C4C6A427879FBD1B4249B8C799A7EE68D72CADB3E9D0F45FF82E178FAE
                FE1E7C4CFD923C4B178735AD7EFB4AF88D1DE8D134B459EFEF1D6246FB3C2B90
                AF213F28E704F7AF3AF067C61D47FE0A9DFF00058BF86D3CBE0DBDF8393FECE3
                6575AC6A5A678A3FD1FC4BADA5C845444B70B8F251BCA392E405B8639F9D41FA
                1BF6BFF807F157F6A0F15FEC8BE2CFF842A1D3354F0578C6CFC47E35D3A3D62D
                A55F0F27948665590BA8B808E197F74189C02011CD1FF0502FD917C7DAA7EDB9
                F007E3D7C24D0A2D63C49E0CD4DB42F16DA8BD82D1EF741B8C8918B4D222BF92
                AF7185059B74E8C14ECE35C36228C68C293694F96694AFA26DBD1EB6D5689F9A
                6658AC3D79569D649B873536E36DD251D5697D1EAD7958F3FF008396A3F625FF
                0082F578EBC2601B5F097ED39E1D5F13E9C369119D66D0C8D71183D37328BC95
                BBFEFA31E95A3FF04F6B61FB5C7FC152BF68AF8F73EEB9D13C15347F0B3C2521
                1BA2096DB5EF9D0F4E6508EA47F0DD30E33CF5DFF0599FD903E237ED05E04F87
                5E3AF82B6A973F17FE11F891356D110DCC16C67B79405B88B7CCE91E094858AB
                380CA8CBC922BD43FE0973FB26DC7EC53FB0DF817C07A9C68BE24B7B46D435F6
                57590B6A372E669D59D490FE5B3F941C120AC4A6B1AD89A6F09EDD4BF7928A83
                5D6CB77F34A2BEF36A385AAB19EC1C5FB38C9CD3E976B45F29393FB8F9BFFE0D
                F9D7ECBC25FB2BFC7ED5752B98ECF4FD33E30788AEAEA790FCB0C51D9D833B9F
                60A09FC2BE3BF15FC37B9FDA07F613F89DFB655ADCD9D9FC5AB8F89B1FC41F0D
                0927417B69A2E9939B586D71B810B1A195CE3EFADAC7D78AFA17C1BFB09FED13
                F0AFFE0965FB42FC32D0BC1709F1DFC59F899A94D6509D6EC5634D0EF52CE396
                EDA4F3B600F143327979F300901D9DABE81F057FC1023F65BD07C03A4E97A97C
                30D3B56D4ACB4F86D6EB539350BD49AF6648D55E721660A19D8162000013D3B5
                7A12C761A857A988E7BB9497C36778C526D3D56926D7DC79D1C0E2711429E1D4
                2CA317F15E36949B49AD1EB14AFF0034799FFC16EFE33E97FB457FC10A6EFC79
                A2B86D2FC5D1681AADB8CE4C625BBB77287FDA524A91D8A9AFD00F86DC7C3BD0
                78FF009875BFFE8A5AFCB3BDFF00826F7ED08FFF00045AF893FB3A5CF8622D53
                5EF0DF8C01F04C9FDB1628BAF68E3508AE7CC5632ED84EE33B859D91B0CAB8C8
                02BE9EFD993F68EFDAD355F1FF0084FC37E39FD9774AF08784079769A96BC9F1
                034EBD7B0892323CD16F1333B92540DAA3BF5039AF3F178683C37B2A338B509C
                DFC514ECD46DBBD76E9D743D1C1E266B13ED6BC249CE105F0C9ABA72BA6D2D37
                EBD353EC9CE68A41D696BE78FA30A28A2800A28A2800A28A2800A28A2800A28A
                2800A28A2800A28A2800A28A280035FCB67FC16E7E065F7C05FF00829EFC58B4
                BB81A2B6F126AEFE27B1936E12E61BEFF482EBEA04AF2A13FDE8DABFA932335F
                19FF00C1623FE0927A47FC14D3E15D8CFA6DDDA787FE25785524FEC4D5668C98
                6EA36E5ACEE4A8DDE53360AB004C6C490086756FA1E1ACD2182C5F355F864ACF
                CBB33E7789B2A9E3709CB4BE28BBA5DFBA3F0A3FE0957FF0530D77FE0991F1F6
                E7C4D67A52788FC3BAFDA0D3F5DD21A7F21EE620DBD258A4C10B2C6D9C641055
                DD4E37065FD7AD1BFE0E9DFD9C2FF4E8A5B9D13E29D84EC3E7824D1ED5CA1FF7
                96E8823D3F90E95F87BFB4E7EC45F15FF63AF134DA5FC46F03EBDE1B68E431C7
                7935B992C2EF9EB0DCA6629074FBAC719C1C1E2BCA88AFD031991E033092C44B
                56FAA7BFE87E7782CF71F9745E1E3A24F692DBF53FA22FF88A47F669FF009F0F
                89FF00F824B7FF00E49A3FE2291FD9A7FE7C3E27FF00E092DFFF00926BF9DDA2
                B8FF00D4FCBFFBDF7FFC03B3FD73CC7FBBF77FC13FA22FF88A47F669FF009F0F
                89FF00F824B7FF00E49A4FF88A47F669FF009F0F89DFF824B7FF00E49AFE77A8
                A3FD4FCBFF00BDF7FF00C00FF5CF31FEEFDDFF0004FDF9F8B7FF000758FC14D0
                3C2174FE0DF06FC41F11EBED137D920BFB6B6D3ECB7F6F3651348EA3FDD8DBA1
                E95F86BF1EFE36EB9FB48FC6AF1478F7C4B24326BBE2DD4A6D4EF0C2A5624791
                8B6C4049211461541248550327AD72B656536A57915BDBC32DC4F33048E28D0B
                BC8C780001C927D057DD7FF04F7FF82017C66FDB13C49A7EA5E2CD1F52F861F0
                FB787BAD4F58B6306A17718E4ADADABE2466618C492058F07702F8D87AE860B2
                FCA212AB7E5BF56F5F45FF00011C988C76639C4E349AE6B744B4F57FF059EF3F
                F06C97EC637DF16F42F8E9E32D4227B4D03C41E199FE1EDADD107F7D25D8596E
                76E3FE79AA5B93FF005D571D0E3F2CFE287C36D67E0E7C48D7BC27E22B37D3F5
                DF0DDFCDA6DFDBBF586789CA38CF7195383D08C11C1AFEBAFF00671FD9DFC29F
                B297C17D03C03E09D3534BF0EF876D85BDBC7C19266EAF34AC00DF2C8C59DDB1
                CB313ED5F0D7FC167BFE085565FB795E4DF11FE1CDC58683F15218552F60B926
                3B2F13468A1516461FEAA75501564C10C02ABE000EBF3996F1453798549D6D21
                3B5BCADA2BFAF5ECCFA4CCB85AA2CBA9428FBD385EEBBDF576F4E9DD799F9D1F
                F0467FF82E41FF00826FF85B53F0278CBC39A8789FE1F6A57E753824D31E31A8
                E933BAAA4BB1242A92C6E110EC2E9B581218EE22BF4522FF0083A4FF0066A78D
                59B4DF8A085864A9D12DF2BEC71738AFC25FDA07F644F89DFB2B78865D2FE21F
                81BC49E14B989FCB0F7B66C2DA73923314EB98A55E38647607D6BCE7B57D0627
                87B2FC6CFEB1FCDD53D1FE67CF61788F31C1416196D1E925AAF2E87F445FF114
                8FECD3FF003E1F13BFF0496FFF00C935C0FED07FF075A7C2DD13C097A9F0CBC1
                5E33D7FC512C2C2CDF5C820B1D36DE42080F2149A495F69C128AABB871BD49C8
                FC1BA7DB5B497B711C30C6F2CB33044440599D89C0000EA49ED58C38472F8B52
                69BF566D3E30CCA51E54D2F4469F8F7C6FA9FC4DF1CEB5E24D6EE9AF75AF10DF
                CFA95FDCB001AE2E2691A491CE3B97663F8D7ECD7FC1A5DF016FAC3C35F163E2
                65D42F1E9FA9CD69E1CD39CF0267843CF7271DC0F36D803EBB87635F127EC05F
                F042CF8D9FB6A78B34EB8D53C3FA9FC3CF013BAC979AF6B968F6CF2C24827ECB
                0380F3B119DAD811FAB8E87FA34FD9C7F67AF0AFECA9F04FC3DF0FFC17A78D33
                C39E1AB616D6B113BA4909259E591B0374923B33B360659C9C0E95E7F1567142
                386782A2D393B5EDD12FD7C8F4384F26AF2C4AC6D74D455ED7EADFE9E7DCEDE8
                34515F9C1FA6084668C52D1400114628A2801314014B45002119A36F14B45002
                628DB4B45002018A31C52D140098E7AD2E28A280003145145001451450014514
                500145145001451450014514500145145001451450014514500141A28A0064F6
                E9730BC7222C91C8A5595864303C1041EA2B207C36F0E8FF0098068BFF008051
                7FF135B5453526B6138A7B98BFF0ADFC3BFF00400D17FF000062FF00E268FF00
                856FE1DFFA0068BFF80317FF00135B5453E797717247B18BFF000ADFC3BFF400
                D17FF0062FFE268FF856FE1DFF00A0068BFF0080317FF135B5451CF2EE1C91EC
                66E9BE0DD2345BA13D9695A6D9CE01512436C91B807A8C819AD1DB4B4526DBDC
                6925B0639A0D1452191DCDA457B6F24334693452A9474750CAEA460820F04115
                93FF000ADBC3BFF401D17FF0062FFE26B6A8A6A4D6C2714F7317FE15BF877FE8
                01A2FF00E00C5FFC4D58D33C1BA3E8B7427B3D2B4DB49C020490DB246E01EA32
                066B4A8A7CCFB89422BA08464528145152505145140051451400514514005145
                1400514514005145140051451400514514005145140051451400514514005145
                140051451401FFD9}
              mmHeight = 19579
              mmLeft = 6085
              mmTop = 0
              mmWidth = 48948
              BandType = 1
            end
            object ppLabel73: TppLabel
              UserName = 'Label73'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'General Prieto 1430. Independencia'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              mmHeight = 3260
              mmLeft = 10054
              mmTop = 20902
              mmWidth = 45466
              BandType = 1
            end
            object ppLabel40: TppLabel
              UserName = 'Label40'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'Liquidaci'#243'n de Efectivo y Cupones'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 13
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 5419
              mmLeft = 66426
              mmTop = 11906
              mmWidth = 75734
              BandType = 1
            end
          end
          object ppDetailBand21: TppDetailBand
            Background1.Brush.Style = bsClear
            Background1.Gradient.EndColor = clWhite
            Background1.Gradient.StartColor = clWhite
            Background1.Gradient.Style = gsNone
            Background2.Brush.Style = bsClear
            Background2.Gradient.EndColor = clWhite
            Background2.Gradient.StartColor = clWhite
            Background2.Gradient.Style = gsNone
            PrintHeight = phDynamic
            mmBottomOffset = 0
            mmHeight = 25929
            mmPrintPosition = 0
            object ppRegionEfectivo: TppRegion
              UserName = 'RegionEfectivo'
              ParentWidth = True
              Pen.Style = psClear
              Stretch = True
              mmHeight = 7938
              mmLeft = 0
              mmTop = 265
              mmWidth = 197300
              BandType = 4
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
              object ppSubReportEfectivo: TppSubReport
                UserName = 'ppSubReportEfectivo'
                ExpandAll = False
                KeepTogether = True
                NewPrintJob = False
                OutlineSettings.CreateNode = True
                ParentPrinterSetup = False
                TraverseAllData = False
                DataPipelineName = 'ppDetalleCobrosEfectivo'
                mmHeight = 5292
                mmLeft = 0
                mmTop = 1588
                mmWidth = 197300
                BandType = 4
                mmBottomOffset = 0
                mmOverFlowOffset = 0
                mmStopPosition = 0
                mmMinHeight = 0
                object ppChildReport20: TppChildReport
                  AutoStop = False
                  DataPipeline = ppDetalleCobrosEfectivo
                  PrinterSetup.BinName = 'Default'
                  PrinterSetup.DocumentName = 'Report'
                  PrinterSetup.PaperName = 'A4'
                  PrinterSetup.PrinterName = 'Default'
                  PrinterSetup.SaveDeviceSettings = False
                  PrinterSetup.mmMarginBottom = 0
                  PrinterSetup.mmMarginLeft = 0
                  PrinterSetup.mmMarginRight = 0
                  PrinterSetup.mmMarginTop = 0
                  PrinterSetup.mmPaperHeight = 297000
                  PrinterSetup.mmPaperWidth = 210000
                  PrinterSetup.PaperSize = 9
                  Units = utMillimeters
                  Version = '12.04'
                  mmColumnWidth = 0
                  DataPipelineName = 'ppDetalleCobrosEfectivo'
                  object ppTitleBand20: TppTitleBand
                    mmBottomOffset = 0
                    mmHeight = 12965
                    mmPrintPosition = 0
                    object ppShape3: TppShape
                      UserName = 'Shape3'
                      Brush.Color = 16444382
                      Gradient.EndColor = clWhite
                      Gradient.StartColor = clWhite
                      Gradient.Style = gsNone
                      ParentHeight = True
                      ParentWidth = True
                      Pen.Style = psClear
                      mmHeight = 12965
                      mmLeft = 0
                      mmTop = 0
                      mmWidth = 210000
                      BandType = 1
                    end
                    object ppLabel74: TppLabel
                      UserName = 'Label5'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      Caption = 'Moneda'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clNavy
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = [fsBold]
                      Transparent = True
                      mmHeight = 4233
                      mmLeft = 3704
                      mmTop = 7673
                      mmWidth = 13462
                      BandType = 1
                    end
                    object ppLabel75: TppLabel
                      UserName = 'Label6'
                      HyperlinkColor = clBlue
                      AutoSize = False
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      Caption = 'Cantidad'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clNavy
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = [fsBold]
                      TextAlignment = taRightJustified
                      Transparent = True
                      mmHeight = 4191
                      mmLeft = 73819
                      mmTop = 7938
                      mmWidth = 23283
                      BandType = 1
                    end
                    object ppLabel76: TppLabel
                      UserName = 'Label7'
                      HyperlinkColor = clBlue
                      AutoSize = False
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      Caption = 'Importe'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clNavy
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = [fsBold]
                      TextAlignment = taRightJustified
                      Transparent = True
                      mmHeight = 4191
                      mmLeft = 172509
                      mmTop = 7673
                      mmWidth = 35190
                      BandType = 1
                    end
                    object ppLabel77: TppLabel
                      UserName = 'Label8'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      Caption = 'Detalle de la Liquidaci'#243'n en Efectivo'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clNavy
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = [fsBold]
                      Transparent = True
                      mmHeight = 4233
                      mmLeft = 3440
                      mmTop = 1323
                      mmWidth = 61891
                      BandType = 1
                    end
                    object ppLabel78: TppLabel
                      UserName = 'Label13'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      Caption = 'Valor en Moneda Local'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clNavy
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = [fsBold]
                      TextAlignment = taRightJustified
                      Transparent = True
                      mmHeight = 4233
                      mmLeft = 107686
                      mmTop = 7938
                      mmWidth = 38894
                      BandType = 1
                    end
                  end
                  object ppDetailBand22: TppDetailBand
                    Background1.Brush.Style = bsClear
                    Background1.Gradient.EndColor = clWhite
                    Background1.Gradient.StartColor = clWhite
                    Background1.Gradient.Style = gsNone
                    Background2.Brush.Style = bsClear
                    Background2.Gradient.EndColor = clWhite
                    Background2.Gradient.StartColor = clWhite
                    Background2.Gradient.Style = gsNone
                    mmBottomOffset = 0
                    mmHeight = 4498
                    mmPrintPosition = 0
                    object ppDBText55: TppDBText
                      UserName = 'DBText1'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      DataField = 'Descripcion'
                      DataPipeline = ppDetalleCobrosEfectivo
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = []
                      Transparent = True
                      DataPipelineName = 'ppDetalleCobrosEfectivo'
                      mmHeight = 3969
                      mmLeft = 3704
                      mmTop = 265
                      mmWidth = 67469
                      BandType = 4
                    end
                    object ppDBText56: TppDBText
                      UserName = 'DBText2'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      DataField = 'Cantidad'
                      DataPipeline = ppDetalleCobrosEfectivo
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = []
                      TextAlignment = taRightJustified
                      Transparent = True
                      DataPipelineName = 'ppDetalleCobrosEfectivo'
                      mmHeight = 3969
                      mmLeft = 73819
                      mmTop = 265
                      mmWidth = 23283
                      BandType = 4
                    end
                    object ppDBText57: TppDBText
                      UserName = 'DBText3'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      DataField = 'Importe'
                      DataPipeline = ppDetalleCobrosEfectivo
                      DisplayFormat = '$#######0;($#######0)'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = []
                      TextAlignment = taRightJustified
                      Transparent = True
                      DataPipelineName = 'ppDetalleCobrosEfectivo'
                      mmHeight = 3969
                      mmLeft = 172509
                      mmTop = 265
                      mmWidth = 35190
                      BandType = 4
                    end
                    object ppDBText58: TppDBText
                      UserName = 'DBText15'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      DataField = 'ValorMonedaLocal'
                      DataPipeline = ppDetalleCobrosEfectivo
                      DisplayFormat = '$#######0;($#######0)'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = []
                      TextAlignment = taRightJustified
                      Transparent = True
                      DataPipelineName = 'ppDetalleCobrosEfectivo'
                      mmHeight = 3969
                      mmLeft = 107950
                      mmTop = 265
                      mmWidth = 38629
                      BandType = 4
                    end
                  end
                  object ppSummaryBand20: TppSummaryBand
                    AlignToBottom = False
                    mmBottomOffset = 0
                    mmHeight = 5821
                    mmPrintPosition = 0
                    object ppShape1: TppShape
                      UserName = 'Shape1'
                      Brush.Color = 16444382
                      Gradient.EndColor = clWhite
                      Gradient.StartColor = clWhite
                      Gradient.Style = gsNone
                      ParentWidth = True
                      Pen.Style = psClear
                      mmHeight = 5821
                      mmLeft = 0
                      mmTop = 0
                      mmWidth = 210000
                      BandType = 7
                    end
                    object ppLabel79: TppLabel
                      UserName = 'Label1'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      Caption = 'Total en Efectivo:'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clNavy
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = [fsBold]
                      Transparent = True
                      mmHeight = 4233
                      mmLeft = 113506
                      mmTop = 794
                      mmWidth = 30956
                      BandType = 7
                    end
                    object ppSumaEfectivo: TppDBCalc
                      UserName = 'SumaEfectivo'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      DataField = 'Importe'
                      DataPipeline = ppDetalleCobrosEfectivo
                      DisplayFormat = '$#######0;($#######0)'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clNavy
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = [fsBold]
                      TextAlignment = taRightJustified
                      Transparent = True
                      DataPipelineName = 'ppDetalleCobrosEfectivo'
                      mmHeight = 4233
                      mmLeft = 161396
                      mmTop = 794
                      mmWidth = 46302
                      BandType = 7
                    end
                  end
                  object raCodeModule3: TraCodeModule
                    ProgramStream = {00}
                  end
                end
              end
            end
            object ppRegionCupones: TppRegion
              UserName = 'RegionCupones'
              Caption = 'RegionCupones'
              ParentWidth = True
              Pen.Style = psClear
              ShiftRelativeTo = ppRegionEfectivo
              Stretch = True
              mmHeight = 8202
              mmLeft = 0
              mmTop = 8467
              mmWidth = 197300
              BandType = 4
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
              object ppSubReportCupones: TppSubReport
                UserName = 'ppSubReportCupones'
                ExpandAll = False
                KeepTogether = True
                NewPrintJob = False
                OutlineSettings.CreateNode = True
                ParentPrinterSetup = False
                TraverseAllData = False
                DataPipelineName = 'ppDBPipelineLiquidacionCupones'
                mmHeight = 5292
                mmLeft = 0
                mmTop = 10054
                mmWidth = 197300
                BandType = 4
                mmBottomOffset = 0
                mmOverFlowOffset = 0
                mmStopPosition = 0
                mmMinHeight = 0
                object ppSurRepCupones: TppChildReport
                  AutoStop = False
                  DataPipeline = ppDBPipelineLiquidacionCupones
                  PrinterSetup.BinName = 'Default'
                  PrinterSetup.DocumentName = 'Report'
                  PrinterSetup.PaperName = 'A4'
                  PrinterSetup.PrinterName = 'Default'
                  PrinterSetup.SaveDeviceSettings = False
                  PrinterSetup.mmMarginBottom = 0
                  PrinterSetup.mmMarginLeft = 0
                  PrinterSetup.mmMarginRight = 0
                  PrinterSetup.mmMarginTop = 0
                  PrinterSetup.mmPaperHeight = 297000
                  PrinterSetup.mmPaperWidth = 210000
                  PrinterSetup.PaperSize = 9
                  Units = utMillimeters
                  Version = '12.04'
                  mmColumnWidth = 0
                  DataPipelineName = 'ppDBPipelineLiquidacionCupones'
                  object ppTitleBand9: TppTitleBand
                    mmBottomOffset = 0
                    mmHeight = 12965
                    mmPrintPosition = 0
                    object ppShape2: TppShape
                      UserName = 'Shape3'
                      Brush.Color = 16444382
                      Gradient.EndColor = clWhite
                      Gradient.StartColor = clWhite
                      Gradient.Style = gsNone
                      ParentHeight = True
                      ParentWidth = True
                      Pen.Style = psClear
                      mmHeight = 12965
                      mmLeft = 0
                      mmTop = 0
                      mmWidth = 210000
                      BandType = 1
                    end
                    object ppLabel41: TppLabel
                      UserName = 'Label5'
                      HyperlinkColor = clBlue
                      AutoSize = False
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      Caption = 'Medio de Pago'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clNavy
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = [fsBold]
                      Transparent = True
                      mmHeight = 4191
                      mmLeft = 3704
                      mmTop = 7673
                      mmWidth = 27517
                      BandType = 1
                    end
                    object ppLabel42: TppLabel
                      UserName = 'Label6'
                      HyperlinkColor = clBlue
                      AutoSize = False
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      Caption = 'Cant. Liquidada'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clNavy
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = [fsBold]
                      TextAlignment = taRightJustified
                      Transparent = True
                      mmHeight = 4233
                      mmLeft = 57415
                      mmTop = 8202
                      mmWidth = 27517
                      BandType = 1
                    end
                    object ppLabel84: TppLabel
                      UserName = 'Label8'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      Caption = 'Detalle de la Liquidaci'#243'n de Cheques y Cupones'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clNavy
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = [fsBold]
                      Transparent = True
                      mmHeight = 4191
                      mmLeft = 3440
                      mmTop = 1323
                      mmWidth = 81068
                      BandType = 1
                    end
                    object ppLabel87: TppLabel
                      UserName = 'Label87'
                      HyperlinkColor = clBlue
                      AutoSize = False
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      Caption = 'Cant. Calc. Sistema'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clNavy
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = [fsBold]
                      TextAlignment = taRightJustified
                      Transparent = True
                      mmHeight = 4233
                      mmLeft = 91546
                      mmTop = 8202
                      mmWidth = 34925
                      BandType = 1
                    end
                    object ppLabel70: TppLabel
                      UserName = 'Label70'
                      HyperlinkColor = clBlue
                      AutoSize = False
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      Caption = 'Diferencia'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clNavy
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = [fsBold]
                      TextAlignment = taRightJustified
                      Transparent = True
                      mmHeight = 4233
                      mmLeft = 131763
                      mmTop = 8202
                      mmWidth = 27517
                      BandType = 1
                    end
                    object ppLabel85: TppLabel
                      UserName = 'Label85'
                      HyperlinkColor = clBlue
                      AutoSize = False
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      Caption = 'Monto Total'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clNavy
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = [fsBold]
                      TextAlignment = taRightJustified
                      Transparent = True
                      mmHeight = 4233
                      mmLeft = 174625
                      mmTop = 8202
                      mmWidth = 27517
                      BandType = 1
                    end
                  end
                  object ppDetailBand11: TppDetailBand
                    Background1.Brush.Style = bsClear
                    Background1.Gradient.EndColor = clWhite
                    Background1.Gradient.StartColor = clWhite
                    Background1.Gradient.Style = gsNone
                    Background2.Brush.Style = bsClear
                    Background2.Gradient.EndColor = clWhite
                    Background2.Gradient.StartColor = clWhite
                    Background2.Gradient.Style = gsNone
                    mmBottomOffset = 0
                    mmHeight = 4498
                    mmPrintPosition = 0
                    object ppDBText30: TppDBText
                      UserName = 'DBText2'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      DataField = 'CantidadLiquidada'
                      DataPipeline = ppDBPipelineLiquidacionCupones
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = []
                      TextAlignment = taRightJustified
                      Transparent = True
                      DataPipelineName = 'ppDBPipelineLiquidacionCupones'
                      mmHeight = 3969
                      mmLeft = 57415
                      mmTop = 265
                      mmWidth = 27517
                      BandType = 4
                    end
                    object ppDBText64: TppDBText
                      UserName = 'DBText64'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      DataField = 'CantidadSistema'
                      DataPipeline = ppDBPipelineLiquidacionCupones
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = []
                      TextAlignment = taRightJustified
                      Transparent = True
                      DataPipelineName = 'ppDBPipelineLiquidacionCupones'
                      mmHeight = 3969
                      mmLeft = 91811
                      mmTop = 265
                      mmWidth = 34660
                      BandType = 4
                    end
                    object ppDBText31: TppDBText
                      UserName = 'DBText31'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      DataField = 'DescriImporte'
                      DataPipeline = ppDBPipelineLiquidacionCupones
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = []
                      TextAlignment = taRightJustified
                      Transparent = True
                      DataPipelineName = 'ppDBPipelineLiquidacionCupones'
                      mmHeight = 3969
                      mmLeft = 166952
                      mmTop = 265
                      mmWidth = 35190
                      BandType = 4
                    end
                    object ppDBText63: TppDBText
                      UserName = 'DBText63'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      DataField = 'Diferencia'
                      DataPipeline = ppDBPipelineLiquidacionCupones
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = []
                      TextAlignment = taRightJustified
                      Transparent = True
                      DataPipelineName = 'ppDBPipelineLiquidacionCupones'
                      mmHeight = 3969
                      mmLeft = 131763
                      mmTop = 265
                      mmWidth = 27517
                      BandType = 4
                    end
                    object ppDBText29: TppDBText
                      UserName = 'DBText1'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      DataField = 'TipoMedioPago'
                      DataPipeline = ppDBPipelineLiquidacionCupones
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = []
                      Transparent = True
                      DataPipelineName = 'ppDBPipelineLiquidacionCupones'
                      mmHeight = 3969
                      mmLeft = 3969
                      mmTop = 265
                      mmWidth = 49213
                      BandType = 4
                    end
                  end
                  object ppSummaryBand9: TppSummaryBand
                    AlignToBottom = False
                    mmBottomOffset = 0
                    mmHeight = 5821
                    mmPrintPosition = 0
                    object ppShape14: TppShape
                      UserName = 'Shape14'
                      Brush.Color = 16444382
                      Gradient.EndColor = clWhite
                      Gradient.StartColor = clWhite
                      Gradient.Style = gsNone
                      ParentWidth = True
                      Pen.Style = psClear
                      mmHeight = 5821
                      mmLeft = 0
                      mmTop = 0
                      mmWidth = 210000
                      BandType = 7
                    end
                    object ppLabel7: TppLabel
                      UserName = 'Label1'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      Caption = 'Total en Cheques y Cupones:'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clNavy
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = [fsBold]
                      Transparent = True
                      mmHeight = 4191
                      mmLeft = 97367
                      mmTop = 794
                      mmWidth = 49488
                      BandType = 7
                    end
                    object ppDBCalc1: TppDBCalc
                      UserName = 'SumaEfectivo1'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      DataField = 'Importe'
                      DataPipeline = ppDBPipelineLiquidacionCupones
                      DisplayFormat = '$#######0;($#######0)'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clNavy
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = [fsBold]
                      TextAlignment = taRightJustified
                      Transparent = True
                      DataPipelineName = 'ppDBPipelineLiquidacionCupones'
                      mmHeight = 4233
                      mmLeft = 155840
                      mmTop = 794
                      mmWidth = 46302
                      BandType = 7
                    end
                  end
                  object raCodeModule4: TraCodeModule
                    ProgramStream = {00}
                  end
                end
              end
            end
            object ppRegion1: TppRegion
              UserName = 'RegionCupones1'
              Caption = 'RegionCanales'
              ParentWidth = True
              Pen.Style = psClear
              ShiftRelativeTo = ppRegionCupones
              Stretch = True
              mmHeight = 8202
              mmLeft = 0
              mmTop = 16669
              mmWidth = 197300
              BandType = 4
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
              object ppsrDetalleCanales: TppSubReport
                UserName = 'ppsrDetalleCanales'
                ExpandAll = False
                KeepTogether = True
                NewPrintJob = False
                OutlineSettings.CreateNode = True
                ParentPrinterSetup = False
                TraverseAllData = False
                DataPipelineName = 'ppLiquidacionPorCanal'
                mmHeight = 5292
                mmLeft = 0
                mmTop = 18256
                mmWidth = 197300
                BandType = 4
                mmBottomOffset = 0
                mmOverFlowOffset = 0
                mmStopPosition = 0
                mmMinHeight = 0
                object ppChildReport1: TppChildReport
                  AutoStop = False
                  DataPipeline = ppLiquidacionPorCanal
                  PrinterSetup.BinName = 'Default'
                  PrinterSetup.DocumentName = 'Report'
                  PrinterSetup.PaperName = 'A4'
                  PrinterSetup.PrinterName = 'Default'
                  PrinterSetup.SaveDeviceSettings = False
                  PrinterSetup.mmMarginBottom = 0
                  PrinterSetup.mmMarginLeft = 0
                  PrinterSetup.mmMarginRight = 0
                  PrinterSetup.mmMarginTop = 0
                  PrinterSetup.mmPaperHeight = 297000
                  PrinterSetup.mmPaperWidth = 210000
                  PrinterSetup.PaperSize = 9
                  Units = utMillimeters
                  Version = '12.04'
                  mmColumnWidth = 0
                  DataPipelineName = 'ppLiquidacionPorCanal'
                  object ppTitleBand1: TppTitleBand
                    mmBottomOffset = 0
                    mmHeight = 12965
                    mmPrintPosition = 0
                    object ppShape13: TppShape
                      UserName = 'Shape3'
                      Brush.Color = 16444382
                      Gradient.EndColor = clWhite
                      Gradient.StartColor = clWhite
                      Gradient.Style = gsNone
                      ParentHeight = True
                      ParentWidth = True
                      Pen.Style = psClear
                      mmHeight = 12965
                      mmLeft = 0
                      mmTop = 0
                      mmWidth = 210000
                      BandType = 1
                    end
                    object ppLabel1: TppLabel
                      UserName = 'Label5'
                      HyperlinkColor = clBlue
                      AutoSize = False
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      Caption = 'Canal'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clNavy
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = [fsBold]
                      Transparent = True
                      mmHeight = 4191
                      mmLeft = 3704
                      mmTop = 7673
                      mmWidth = 27517
                      BandType = 1
                    end
                    object ppLabel2: TppLabel
                      UserName = 'Label6'
                      HyperlinkColor = clBlue
                      AutoSize = False
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      Caption = 'Forma de Pago'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clNavy
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = [fsBold]
                      Transparent = True
                      mmHeight = 4233
                      mmLeft = 45244
                      mmTop = 8202
                      mmWidth = 83608
                      BandType = 1
                    end
                    object ppLabel3: TppLabel
                      UserName = 'Label8'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      Caption = 'Detalle de la Liquidaci'#243'n por Canal y Forma de Pago'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clNavy
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = [fsBold]
                      Transparent = True
                      mmHeight = 4233
                      mmLeft = 3440
                      mmTop = 1323
                      mmWidth = 90488
                      BandType = 1
                    end
                    object ppLabel5: TppLabel
                      UserName = 'Label70'
                      HyperlinkColor = clBlue
                      AutoSize = False
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      Caption = 'Cant. Pagos'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clNavy
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = [fsBold]
                      TextAlignment = taRightJustified
                      Transparent = True
                      mmHeight = 4233
                      mmLeft = 131763
                      mmTop = 8202
                      mmWidth = 27517
                      BandType = 1
                    end
                    object ppLabel6: TppLabel
                      UserName = 'Label85'
                      HyperlinkColor = clBlue
                      AutoSize = False
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      Caption = 'Monto Total'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clNavy
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = [fsBold]
                      TextAlignment = taRightJustified
                      Transparent = True
                      mmHeight = 4233
                      mmLeft = 174625
                      mmTop = 8202
                      mmWidth = 27517
                      BandType = 1
                    end
                  end
                  object ppDetailBand1: TppDetailBand
                    Background1.Brush.Style = bsClear
                    Background1.Gradient.EndColor = clWhite
                    Background1.Gradient.StartColor = clWhite
                    Background1.Gradient.Style = gsNone
                    Background2.Brush.Style = bsClear
                    Background2.Gradient.EndColor = clWhite
                    Background2.Gradient.StartColor = clWhite
                    Background2.Gradient.Style = gsNone
                    mmBottomOffset = 0
                    mmHeight = 4498
                    mmPrintPosition = 0
                    object ppDBText2: TppDBText
                      UserName = 'DBText2'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      DataField = 'FormaPago'
                      DataPipeline = ppLiquidacionPorCanal
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = []
                      ParentDataPipeline = False
                      Transparent = True
                      DataPipelineName = 'ppLiquidacionPorCanal'
                      mmHeight = 3969
                      mmLeft = 45508
                      mmTop = 265
                      mmWidth = 83079
                      BandType = 4
                    end
                    object ppDBText4: TppDBText
                      UserName = 'DBText31'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      DataField = 'Importe'
                      DataPipeline = ppLiquidacionPorCanal
                      DisplayFormat = '$ #,##0'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = []
                      ParentDataPipeline = False
                      TextAlignment = taRightJustified
                      Transparent = True
                      DataPipelineName = 'ppLiquidacionPorCanal'
                      mmHeight = 3969
                      mmLeft = 166952
                      mmTop = 265
                      mmWidth = 35190
                      BandType = 4
                    end
                    object ppDBText5: TppDBText
                      UserName = 'DBText63'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      DataField = 'Cantidad'
                      DataPipeline = ppLiquidacionPorCanal
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = []
                      ParentDataPipeline = False
                      TextAlignment = taRightJustified
                      Transparent = True
                      DataPipelineName = 'ppLiquidacionPorCanal'
                      mmHeight = 3969
                      mmLeft = 131763
                      mmTop = 265
                      mmWidth = 27517
                      BandType = 4
                    end
                    object ppDBText1: TppDBText
                      UserName = 'DBText1'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      DataField = 'TipoMedioPago'
                      DataPipeline = ppLiquidacionPorCanal
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = []
                      ParentDataPipeline = False
                      Transparent = True
                      DataPipelineName = 'ppLiquidacionPorCanal'
                      mmHeight = 3969
                      mmLeft = 3440
                      mmTop = 265
                      mmWidth = 40217
                      BandType = 4
                    end
                  end
                  object ppSummaryBand1: TppSummaryBand
                    AlignToBottom = False
                    mmBottomOffset = 0
                    mmHeight = 5821
                    mmPrintPosition = 0
                    object ppShape15: TppShape
                      UserName = 'Shape15'
                      Brush.Color = 16444382
                      Gradient.EndColor = clWhite
                      Gradient.StartColor = clWhite
                      Gradient.Style = gsNone
                      ParentWidth = True
                      Pen.Style = psClear
                      mmHeight = 5821
                      mmLeft = 0
                      mmTop = 0
                      mmWidth = 210000
                      BandType = 7
                    end
                    object ppLabel9: TppLabel
                      UserName = 'Label9'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      Caption = 'Total en Canal y Forma de Pago:'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clNavy
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = [fsBold]
                      Transparent = True
                      mmHeight = 4191
                      mmLeft = 96309
                      mmTop = 794
                      mmWidth = 54525
                      BandType = 7
                    end
                    object ppDBCalc2: TppDBCalc
                      UserName = 'DBCalc1'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      DataField = 'Importe'
                      DataPipeline = ppLiquidacionPorCanal
                      DisplayFormat = '$ #,##0'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clNavy
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = [fsBold]
                      TextAlignment = taRightJustified
                      Transparent = True
                      DataPipelineName = 'ppLiquidacionPorCanal'
                      mmHeight = 4233
                      mmLeft = 155840
                      mmTop = 794
                      mmWidth = 46302
                      BandType = 7
                    end
                  end
                  object raCodeModule1: TraCodeModule
                    ProgramStream = {00}
                  end
                end
              end
            end
          end
          object raCodeModule5: TraCodeModule
            ProgramStream = {00}
          end
        end
      end
      object ppSubReport2: TppSubReport
        UserName = 'SubReport2'
        ExpandAll = False
        NewPrintJob = True
        OutlineSettings.CreateNode = True
        PrintBehavior = pbSection
        ResetPageNo = False
        TraverseAllData = False
        mmHeight = 5027
        mmLeft = 0
        mmTop = 5556
        mmWidth = 197300
        BandType = 4
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object ppChildReport4: TppChildReport
          PrinterSetup.BinName = 'Default'
          PrinterSetup.Copies = 2
          PrinterSetup.DocumentName = 'Report'
          PrinterSetup.PaperName = 'A4'
          PrinterSetup.PrinterName = 'Default'
          PrinterSetup.SaveDeviceSettings = False
          PrinterSetup.mmMarginBottom = 6350
          PrinterSetup.mmMarginLeft = 6350
          PrinterSetup.mmMarginRight = 6350
          PrinterSetup.mmMarginTop = 6350
          PrinterSetup.mmPaperHeight = 297000
          PrinterSetup.mmPaperWidth = 210000
          PrinterSetup.PaperSize = 9
          Version = '12.04'
          mmColumnWidth = 0
          object ppDetailBand4: TppDetailBand
            Background1.Brush.Style = bsClear
            Background1.Gradient.EndColor = clWhite
            Background1.Gradient.StartColor = clWhite
            Background1.Gradient.Style = gsNone
            Background2.Brush.Style = bsClear
            Background2.Gradient.EndColor = clWhite
            Background2.Gradient.StartColor = clWhite
            Background2.Gradient.Style = gsNone
            PrintHeight = phDynamic
            mmBottomOffset = 0
            mmHeight = 55827
            mmPrintPosition = 0
            object ppShape4: TppShape
              UserName = 'Shape1'
              Brush.Color = 15579271
              Gradient.EndColor = clWhite
              Gradient.StartColor = clWhite
              Gradient.Style = gsNone
              ParentWidth = True
              Pen.Style = psClear
              mmHeight = 6350
              mmLeft = 0
              mmTop = 265
              mmWidth = 197300
              BandType = 4
            end
            object ppLabel86: TppLabel
              UserName = 'Label86'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'Pagos al D'#237'a'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Name = 'Arial'
              Font.Size = 11
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4586
              mmLeft = 3175
              mmTop = 1323
              mmWidth = 23072
              BandType = 4
            end
            object lbl_TitTotalCobradoNotasCobro: TppLabel
              UserName = 'lbl_TitTotalCobradoNotasCobro'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'Total Cobrado Notas de Cobro:'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4233
              mmLeft = 5821
              mmTop = 8202
              mmWidth = 52388
              BandType = 4
            end
            object lblTitTotalCobradoNotaCobroInfractor: TppLabel
              UserName = 'lblTitTotalCobradoNotaCobroInfractor'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'Total Cobrado Notas de Cobro Infractor:'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4191
              mmLeft = 5821
              mmTop = 12171
              mmWidth = 67522
              BandType = 4
            end
            object ppLabel4: TppLabel
              UserName = 'lbl_TitTotalCobradoNotasCobro1'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'Total Pagado Notas de Cr'#233'dito:'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4191
              mmLeft = 5821
              mmTop = 16140
              mmWidth = 52451
              BandType = 4
            end
            object ppLabel8: TppLabel
              UserName = 'lbl_TitTotalCobradoBoletas1'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'Total Pagado Boletas:'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4191
              mmLeft = 5821
              mmTop = 20108
              mmWidth = 36999
              BandType = 4
            end
            object ppLabel14: TppLabel
              UserName = 'Label14'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Total Pagado Cuotas de Arriendo:'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4191
              mmLeft = 5821
              mmTop = 24077
              mmWidth = 56811
              BandType = 4
            end
            object lbl_TotalCobradoCuotas: TppLabel
              UserName = 'lbl_TotalCobradoCuotas'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lbl_TotalCobradoCuotas'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 3970
              mmLeft = 83873
              mmTop = 24077
              mmWidth = 31486
              BandType = 4
            end
            object lbl_TotalCobradoBoletas: TppLabel
              UserName = 'lbl_TotalPagadoNotasCredito1'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'lbl_TotalPagadoBoletas'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 3969
              mmLeft = 83873
              mmTop = 20108
              mmWidth = 31485
              BandType = 4
            end
            object lbl_TotalPagadoNotasCredito: TppLabel
              UserName = 'lbl_TotalPagadoNotasCredito'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'lbl_TotalPagadoNotasCredito'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 3969
              mmLeft = 83873
              mmTop = 16140
              mmWidth = 31485
              BandType = 4
            end
            object lblTotalCobradoNotasCobroInfractor: TppLabel
              UserName = 'lblTotalCobradoNotasCobroInfractor'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'lblTotalCobradoNotasCobroInfractor'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 3969
              mmLeft = 83873
              mmTop = 12171
              mmWidth = 31485
              BandType = 4
            end
            object lbl_TotalCobradoNotasCobro: TppLabel
              UserName = 'lbl_TotalCobradoNotasCobro'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'lbl_TotalCobradoNotasCobro'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 3969
              mmLeft = 83873
              mmTop = 8202
              mmWidth = 31485
              BandType = 4
            end
            object lblCobrosAnticipados: TppLabel
              UserName = 'lblCobrosAnticipados'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'lblCobrosAnticipados'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 3969
              mmLeft = 83873
              mmTop = 29104
              mmWidth = 31485
              BandType = 4
            end
            object lblTitCobrosAnticipados: TppLabel
              UserName = 'lblTitCobrosAnticipados'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'Cobros Anticipados:'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4191
              mmLeft = 5821
              mmTop = 29104
              mmWidth = 34502
              BandType = 4
            end
            object ppLabel28: TppLabel
              UserName = 'Label28'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Cobros Anulados en el Turno:'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4233
              mmLeft = 5821
              mmTop = 33073
              mmWidth = 50536
              BandType = 4
            end
            object lblCobrosAnulados: TppLabel
              UserName = 'lblCobrosAnulados'
              HyperlinkColor = clBlue
              AutoSize = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'lblCobrosAnulados'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 3969
              mmLeft = 83873
              mmTop = 33073
              mmWidth = 31485
              BandType = 4
            end
            object ppSubReport3: TppSubReport
              UserName = 'SubReport3'
              ExpandAll = False
              NewPrintJob = False
              OutlineSettings.CreateNode = True
              ParentPrinterSetup = False
              ShiftRelativeTo = ppSubReport5
              TraverseAllData = False
              DataPipelineName = 'ppDBPipeline1LiquidacionPorCanalRefinan'
              mmHeight = 5027
              mmLeft = 0
              mmTop = 44450
              mmWidth = 197300
              BandType = 4
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
              object ppChildReport5: TppChildReport
                AutoStop = False
                DataPipeline = ppDBPipeline1LiquidacionPorCanalRefinan
                PrinterSetup.BinName = 'Default'
                PrinterSetup.Copies = 2
                PrinterSetup.DocumentName = 'Report'
                PrinterSetup.PaperName = 'A4'
                PrinterSetup.PrinterName = 'Default'
                PrinterSetup.SaveDeviceSettings = False
                PrinterSetup.mmMarginBottom = 6350
                PrinterSetup.mmMarginLeft = 6350
                PrinterSetup.mmMarginRight = 6350
                PrinterSetup.mmMarginTop = 6350
                PrinterSetup.mmPaperHeight = 297127
                PrinterSetup.mmPaperWidth = 210079
                PrinterSetup.PaperSize = 9
                Version = '12.04'
                mmColumnWidth = 0
                DataPipelineName = 'ppDBPipeline1LiquidacionPorCanalRefinan'
                object ppTitleBand4: TppTitleBand
                  mmBottomOffset = 0
                  mmHeight = 17727
                  mmPrintPosition = 0
                  object ppShape19: TppShape
                    UserName = 'Shape19'
                    Brush.Color = 16444382
                    Gradient.EndColor = clWhite
                    Gradient.StartColor = clWhite
                    Gradient.Style = gsNone
                    ParentWidth = True
                    Pen.Style = psClear
                    mmHeight = 10319
                    mmLeft = 0
                    mmTop = 7408
                    mmWidth = 197379
                    BandType = 1
                  end
                  object ppLabel95: TppLabel
                    UserName = 'Label901'
                    HyperlinkColor = clBlue
                    AutoSize = False
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'Canal'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clNavy
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = [fsBold]
                    Transparent = True
                    mmHeight = 4191
                    mmLeft = 3704
                    mmTop = 12700
                    mmWidth = 27517
                    BandType = 1
                  end
                  object ppLabel96: TppLabel
                    UserName = 'Label96'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'Detalle de la Liquidaci'#243'n por Canal y Forma de Pago'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clNavy
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = [fsBold]
                    Transparent = True
                    mmHeight = 4233
                    mmLeft = 3440
                    mmTop = 8202
                    mmWidth = 90488
                    BandType = 1
                  end
                  object ppLabel97: TppLabel
                    UserName = 'Label97'
                    HyperlinkColor = clBlue
                    AutoSize = False
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'Forma de Pago'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clNavy
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = [fsBold]
                    Transparent = True
                    mmHeight = 4233
                    mmLeft = 44715
                    mmTop = 13229
                    mmWidth = 84138
                    BandType = 1
                  end
                  object ppLabel98: TppLabel
                    UserName = 'Label98'
                    HyperlinkColor = clBlue
                    AutoSize = False
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'Cant. Pagos'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clNavy
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = [fsBold]
                    TextAlignment = taRightJustified
                    Transparent = True
                    mmHeight = 4233
                    mmLeft = 131763
                    mmTop = 13229
                    mmWidth = 27517
                    BandType = 1
                  end
                  object ppLabel99: TppLabel
                    UserName = 'Label99'
                    HyperlinkColor = clBlue
                    AutoSize = False
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'Monto Total'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clNavy
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = [fsBold]
                    TextAlignment = taRightJustified
                    Transparent = True
                    mmHeight = 4233
                    mmLeft = 170127
                    mmTop = 13229
                    mmWidth = 27517
                    BandType = 1
                  end
                  object ppShape16: TppShape
                    UserName = 'Shape16'
                    Brush.Color = 15579271
                    Gradient.EndColor = clWhite
                    Gradient.StartColor = clWhite
                    Gradient.Style = gsNone
                    ParentWidth = True
                    Pen.Style = psClear
                    mmHeight = 6350
                    mmLeft = 0
                    mmTop = 529
                    mmWidth = 197379
                    BandType = 1
                  end
                  object ppLabel88: TppLabel
                    UserName = 'Label88'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'Pagos a Fecha'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clNavy
                    Font.Name = 'Arial'
                    Font.Size = 11
                    Font.Style = [fsBold]
                    Transparent = True
                    mmHeight = 4498
                    mmLeft = 3175
                    mmTop = 1588
                    mmWidth = 27252
                    BandType = 1
                  end
                end
                object ppDetailBand5: TppDetailBand
                  Background1.Brush.Style = bsClear
                  Background1.Gradient.EndColor = clWhite
                  Background1.Gradient.StartColor = clWhite
                  Background1.Gradient.Style = gsNone
                  Background2.Brush.Style = bsClear
                  Background2.Gradient.EndColor = clWhite
                  Background2.Gradient.StartColor = clWhite
                  Background2.Gradient.Style = gsNone
                  mmBottomOffset = 0
                  mmHeight = 4233
                  mmPrintPosition = 0
                  object ppDBText18: TppDBText
                    UserName = 'DBText1'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    DataField = 'TipoMedioPago'
                    DataPipeline = ppDBPipeline1LiquidacionPorCanalRefinan
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = []
                    ParentDataPipeline = False
                    Transparent = True
                    DataPipelineName = 'ppDBPipeline1LiquidacionPorCanalRefinan'
                    mmHeight = 3969
                    mmLeft = 3704
                    mmTop = 265
                    mmWidth = 38629
                    BandType = 4
                  end
                  object ppDBText19: TppDBText
                    UserName = 'DBText2'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    DataField = 'FormaPago'
                    DataPipeline = ppDBPipeline1LiquidacionPorCanalRefinan
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = []
                    ParentDataPipeline = False
                    Transparent = True
                    DataPipelineName = 'ppDBPipeline1LiquidacionPorCanalRefinan'
                    mmHeight = 3969
                    mmLeft = 44715
                    mmTop = 265
                    mmWidth = 83873
                    BandType = 4
                  end
                  object ppDBText20: TppDBText
                    UserName = 'DBText3'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    DataField = 'Cantidad'
                    DataPipeline = ppDBPipeline1LiquidacionPorCanalRefinan
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = []
                    ParentDataPipeline = False
                    TextAlignment = taRightJustified
                    Transparent = True
                    DataPipelineName = 'ppDBPipeline1LiquidacionPorCanalRefinan'
                    mmHeight = 3969
                    mmLeft = 131763
                    mmTop = 264
                    mmWidth = 27517
                    BandType = 4
                  end
                  object ppDBText21: TppDBText
                    UserName = 'DBText21'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    DataField = 'Importe'
                    DataPipeline = ppDBPipeline1LiquidacionPorCanalRefinan
                    DisplayFormat = '$ #,##0'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = []
                    ParentDataPipeline = False
                    TextAlignment = taRightJustified
                    Transparent = True
                    DataPipelineName = 'ppDBPipeline1LiquidacionPorCanalRefinan'
                    mmHeight = 3969
                    mmLeft = 162454
                    mmTop = 264
                    mmWidth = 35190
                    BandType = 4
                  end
                end
                object ppSummaryBand4: TppSummaryBand
                  AlignToBottom = False
                  mmBottomOffset = 0
                  mmHeight = 5821
                  mmPrintPosition = 0
                  object ppShape20: TppShape
                    UserName = 'Shape20'
                    Brush.Color = 16444382
                    Gradient.EndColor = clWhite
                    Gradient.StartColor = clWhite
                    Gradient.Style = gsNone
                    ParentWidth = True
                    Pen.Style = psClear
                    mmHeight = 5821
                    mmLeft = 0
                    mmTop = 0
                    mmWidth = 197379
                    BandType = 7
                  end
                  object ppLabel100: TppLabel
                    UserName = 'Label100'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'Total en Canal y Forma de Pago:'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clNavy
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = [fsBold]
                    Transparent = True
                    mmHeight = 4191
                    mmLeft = 96309
                    mmTop = 794
                    mmWidth = 54525
                    BandType = 7
                  end
                  object ppDBCalc5: TppDBCalc
                    UserName = 'DBCalc1'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    DataField = 'Importe'
                    DataPipeline = ppDBPipeline1LiquidacionPorCanalRefinan
                    DisplayFormat = '$ #,##0'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clNavy
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = [fsBold]
                    TextAlignment = taRightJustified
                    Transparent = True
                    DataPipelineName = 'ppDBPipeline1LiquidacionPorCanalRefinan'
                    mmHeight = 4233
                    mmLeft = 151342
                    mmTop = 794
                    mmWidth = 46302
                    BandType = 7
                  end
                end
              end
            end
            object ppSubReport1: TppSubReport
              UserName = 'SubReport1'
              ExpandAll = False
              NewPrintJob = False
              OutlineSettings.CreateNode = True
              ShiftRelativeTo = ppSubReport3
              TraverseAllData = False
              mmHeight = 5027
              mmLeft = 0
              mmTop = 49742
              mmWidth = 197300
              BandType = 4
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
              object ppChildReport3: TppChildReport
                PrinterSetup.BinName = 'Default'
                PrinterSetup.Copies = 2
                PrinterSetup.DocumentName = 'Report'
                PrinterSetup.PaperName = 'A4'
                PrinterSetup.PrinterName = 'Default'
                PrinterSetup.SaveDeviceSettings = False
                PrinterSetup.mmMarginBottom = 6350
                PrinterSetup.mmMarginLeft = 6350
                PrinterSetup.mmMarginRight = 6350
                PrinterSetup.mmMarginTop = 6350
                PrinterSetup.mmPaperHeight = 297000
                PrinterSetup.mmPaperWidth = 210000
                PrinterSetup.PaperSize = 9
                Version = '12.04'
                mmColumnWidth = 0
                object ppDetailBand3: TppDetailBand
                  Background1.Brush.Style = bsClear
                  Background1.Gradient.EndColor = clWhite
                  Background1.Gradient.StartColor = clWhite
                  Background1.Gradient.Style = gsNone
                  Background2.Brush.Style = bsClear
                  Background2.Gradient.EndColor = clWhite
                  Background2.Gradient.StartColor = clWhite
                  Background2.Gradient.Style = gsNone
                  PrintHeight = phDynamic
                  mmBottomOffset = 0
                  mmHeight = 48154
                  mmPrintPosition = 0
                  object ppShape28: TppShape
                    UserName = 'Shape28'
                    Brush.Color = 16444382
                    Gradient.EndColor = clWhite
                    Gradient.StartColor = clWhite
                    Gradient.Style = gsNone
                    ParentWidth = True
                    Pen.Style = psClear
                    mmHeight = 10583
                    mmLeft = 0
                    mmTop = 8202
                    mmWidth = 197300
                    BandType = 4
                  end
                  object ppLabel105: TppLabel
                    UserName = 'Label105'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'Diferencias por Formas de Pago'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clNavy
                    Font.Name = 'Arial'
                    Font.Size = 11
                    Font.Style = [fsBold]
                    Transparent = True
                    mmHeight = 4586
                    mmLeft = 3440
                    mmTop = 8996
                    mmWidth = 59514
                    BandType = 4
                  end
                  object ppLabel108: TppLabel
                    UserName = 'Label108'
                    HyperlinkColor = clBlue
                    AutoSize = False
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'Forma de Pago'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clNavy
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = [fsBold]
                    Transparent = True
                    mmHeight = 4191
                    mmLeft = 8996
                    mmTop = 13758
                    mmWidth = 27517
                    BandType = 4
                  end
                  object ppLabel109: TppLabel
                    UserName = 'Label109'
                    HyperlinkColor = clBlue
                    AutoSize = False
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'Importe Sistema'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clNavy
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = [fsBold]
                    TextAlignment = taRightJustified
                    Transparent = True
                    mmHeight = 4233
                    mmLeft = 91017
                    mmTop = 13758
                    mmWidth = 31485
                    BandType = 4
                  end
                  object ppLabel110: TppLabel
                    UserName = 'Label110'
                    HyperlinkColor = clBlue
                    AutoSize = False
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'Importe Liquidado'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clNavy
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = [fsBold]
                    TextAlignment = taRightJustified
                    Transparent = True
                    mmHeight = 4163
                    mmLeft = 125942
                    mmTop = 13758
                    mmWidth = 34131
                    BandType = 4
                  end
                  object ppLabel111: TppLabel
                    UserName = 'Label1101'
                    HyperlinkColor = clBlue
                    AutoSize = False
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'Diferencia'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clNavy
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = [fsBold]
                    TextAlignment = taRightJustified
                    Transparent = True
                    mmHeight = 4163
                    mmLeft = 170127
                    mmTop = 13758
                    mmWidth = 27516
                    BandType = 4
                  end
                  object ppLabel112: TppLabel
                    UserName = 'lblTitCobrosValesVista1'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'Cheques'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clNavy
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = [fsBold]
                    Transparent = True
                    mmHeight = 4233
                    mmLeft = 10584
                    mmTop = 18785
                    mmWidth = 14817
                    BandType = 4
                  end
                  object ppLabel113: TppLabel
                    UserName = 'Label113'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'Tarjeta Cr'#233'dito'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clNavy
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = [fsBold]
                    Transparent = True
                    mmHeight = 4233
                    mmLeft = 10583
                    mmTop = 22754
                    mmWidth = 24871
                    BandType = 4
                  end
                  object ppLabel114: TppLabel
                    UserName = 'Label114'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'Tarjeta D'#233'bito'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clNavy
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = [fsBold]
                    Transparent = True
                    mmHeight = 4233
                    mmLeft = 10583
                    mmTop = 26723
                    mmWidth = 23548
                    BandType = 4
                  end
                  object ppLabel115: TppLabel
                    UserName = 'Label115'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'Vale Vista'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clNavy
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = [fsBold]
                    Transparent = True
                    mmHeight = 4233
                    mmLeft = 10583
                    mmTop = 30692
                    mmWidth = 16669
                    BandType = 4
                  end
                  object pplblChequesImporteSistema: TppLabel
                    UserName = 'pplblChequesImporteSistema'
                    HyperlinkColor = clBlue
                    AutoSize = False
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'pplblChequesImporteSistema'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    mmHeight = 3969
                    mmLeft = 91017
                    mmTop = 18785
                    mmWidth = 31485
                    BandType = 4
                  end
                  object pplblChequesImporteLiquidado: TppLabel
                    UserName = 'pplblChequesImporteLiquidado'
                    HyperlinkColor = clBlue
                    AutoSize = False
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'pplblChequesImporteLiquidado'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    mmHeight = 3969
                    mmLeft = 128588
                    mmTop = 18785
                    mmWidth = 31485
                    BandType = 4
                  end
                  object pplblChequesImporteDiferencia: TppLabel
                    UserName = 'pplblChequesImporteDiferencia'
                    HyperlinkColor = clBlue
                    AutoSize = False
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'pplblChequesImporteDiferencia'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    mmHeight = 3969
                    mmLeft = 166159
                    mmTop = 18785
                    mmWidth = 31485
                    BandType = 4
                  end
                  object pplblTarjetaCreditoImporteSistema: TppLabel
                    UserName = 'pplblTarjetaCreditoImporteSistema'
                    HyperlinkColor = clBlue
                    AutoSize = False
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'pplblTarjetaCreditoImporteSistema'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    mmHeight = 3969
                    mmLeft = 91017
                    mmTop = 22754
                    mmWidth = 31485
                    BandType = 4
                  end
                  object pplblTarjetaCreditoImporteLiquidado: TppLabel
                    UserName = 'pplblTarjetaCreditoImporteLiquidado'
                    HyperlinkColor = clBlue
                    AutoSize = False
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'pplblTarjetaCreditoImporteLiquidado'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    mmHeight = 3969
                    mmLeft = 128588
                    mmTop = 22754
                    mmWidth = 31485
                    BandType = 4
                  end
                  object pplblTarjetaCreditoImporteDiferencia: TppLabel
                    UserName = 'pplblTarjetaCreditoImporteDiferencia'
                    HyperlinkColor = clBlue
                    AutoSize = False
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'pplblTarjetaCreditoImporteDiferencia'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    mmHeight = 3969
                    mmLeft = 166159
                    mmTop = 22754
                    mmWidth = 31485
                    BandType = 4
                  end
                  object pplblTarjetaDebitoImporteSistema: TppLabel
                    UserName = 'pplblTarjetaDebitoImporteSistema'
                    HyperlinkColor = clBlue
                    AutoSize = False
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'pplblTarjetaDebitoImporteSistema'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    mmHeight = 3969
                    mmLeft = 91017
                    mmTop = 26723
                    mmWidth = 31485
                    BandType = 4
                  end
                  object pplblTarjetaDebitoImporteLiquidado: TppLabel
                    UserName = 'pplblTarjetaDebitoImporteLiquidado'
                    HyperlinkColor = clBlue
                    AutoSize = False
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'pplblTarjetaDebitoImporteLiquidado'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    mmHeight = 3969
                    mmLeft = 128588
                    mmTop = 26723
                    mmWidth = 31485
                    BandType = 4
                  end
                  object pplblTarjetaDebitoImporteDiferencia: TppLabel
                    UserName = 'pplblTarjetaDebitoImporteDiferencia'
                    HyperlinkColor = clBlue
                    AutoSize = False
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'pplblTarjetaDebitoImporteDiferencia'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    mmHeight = 3969
                    mmLeft = 166159
                    mmTop = 26723
                    mmWidth = 31485
                    BandType = 4
                  end
                  object pplblValeVistaImporteSistema: TppLabel
                    UserName = 'pplblValeVistaImporteSistema'
                    HyperlinkColor = clBlue
                    AutoSize = False
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'pplblValeVistaImporteSistema'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    mmHeight = 3969
                    mmLeft = 91017
                    mmTop = 30692
                    mmWidth = 31485
                    BandType = 4
                  end
                  object pplblValeVistaImporteLiquidado: TppLabel
                    UserName = 'pplblValeVistaImporteLiquidado'
                    HyperlinkColor = clBlue
                    AutoSize = False
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'pplblValeVistaImporteLiquidado'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    mmHeight = 3969
                    mmLeft = 128588
                    mmTop = 30692
                    mmWidth = 31485
                    BandType = 4
                  end
                  object pplblValeVistaImporteDiferencia: TppLabel
                    UserName = 'pplblValeVistaImporteDiferencia'
                    HyperlinkColor = clBlue
                    AutoSize = False
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'pplblValeVistaImporteDiferencia'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    mmHeight = 3969
                    mmLeft = 166159
                    mmTop = 30692
                    mmWidth = 31485
                    BandType = 4
                  end
                  object ppShape29: TppShape
                    UserName = 'Shape29'
                    Brush.Color = 15579271
                    Gradient.EndColor = clWhite
                    Gradient.StartColor = clWhite
                    Gradient.Style = gsNone
                    ParentWidth = True
                    Pen.Style = psClear
                    mmHeight = 6350
                    mmLeft = 0
                    mmTop = 1058
                    mmWidth = 197300
                    BandType = 4
                  end
                  object ppLabel117: TppLabel
                    UserName = 'Label117'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'C'#225'lculo de Diferencias'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clNavy
                    Font.Name = 'Arial'
                    Font.Size = 11
                    Font.Style = [fsBold]
                    Transparent = True
                    mmHeight = 4498
                    mmLeft = 3175
                    mmTop = 2117
                    mmWidth = 44186
                    BandType = 4
                  end
                  object ppLabel118: TppLabel
                    UserName = 'Label118'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'Efectivo'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clNavy
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = [fsBold]
                    Transparent = True
                    mmHeight = 4163
                    mmLeft = 10583
                    mmTop = 34660
                    mmWidth = 13688
                    BandType = 4
                  end
                  object pplblEfectivoImporteSistema: TppLabel
                    UserName = 'pplblEfectivoImporteSistema'
                    HyperlinkColor = clBlue
                    AutoSize = False
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'pplblEfectivoImporteSistema'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    mmHeight = 3969
                    mmLeft = 91017
                    mmTop = 34660
                    mmWidth = 31485
                    BandType = 4
                  end
                  object pplblEfectivoImporteLiquidado: TppLabel
                    UserName = 'pplblEfectivoImporteLiquidado'
                    HyperlinkColor = clBlue
                    AutoSize = False
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'pplblEfectivoImporteLiquidado'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    mmHeight = 3969
                    mmLeft = 128588
                    mmTop = 34660
                    mmWidth = 31485
                    BandType = 4
                  end
                  object pplblEfectivoImporteDiferencia: TppLabel
                    UserName = 'pplblEfectivoImporteDiferencia'
                    HyperlinkColor = clBlue
                    AutoSize = False
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'pplblEfectivoImporteDiferencia'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    mmHeight = 3969
                    mmLeft = 166159
                    mmTop = 34660
                    mmWidth = 31485
                    BandType = 4
                  end
                  object ppLabel122: TppLabel
                    UserName = 'Label122'
                    HyperlinkColor = clBlue
                    AutoSize = False
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'Importe Apertura'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clNavy
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = [fsBold]
                    TextAlignment = taRightJustified
                    Transparent = True
                    mmHeight = 4233
                    mmLeft = 51329
                    mmTop = 13758
                    mmWidth = 33338
                    BandType = 4
                  end
                  object pplblEfectivoImporteApertura: TppLabel
                    UserName = 'pplblEfectivoImporteApertura'
                    HyperlinkColor = clBlue
                    AutoSize = False
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'pplblEfectivoImporteApertura'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    mmHeight = 3969
                    mmLeft = 53181
                    mmTop = 34660
                    mmWidth = 31485
                    BandType = 4
                  end
                  object ppLabel124: TppLabel
                    UserName = 'Label124'
                    HyperlinkColor = clBlue
                    AutoSize = False
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = '$ 0'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    mmHeight = 3969
                    mmLeft = 53181
                    mmTop = 18785
                    mmWidth = 31485
                    BandType = 4
                  end
                  object ppLabel125: TppLabel
                    UserName = 'Label125'
                    HyperlinkColor = clBlue
                    AutoSize = False
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = '$ 0'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    mmHeight = 3969
                    mmLeft = 53181
                    mmTop = 22754
                    mmWidth = 31485
                    BandType = 4
                  end
                  object ppLabel126: TppLabel
                    UserName = 'Label126'
                    HyperlinkColor = clBlue
                    AutoSize = False
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = '$ 0'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    mmHeight = 3969
                    mmLeft = 53181
                    mmTop = 26723
                    mmWidth = 31485
                    BandType = 4
                  end
                  object ppLabel127: TppLabel
                    UserName = 'Label127'
                    HyperlinkColor = clBlue
                    AutoSize = False
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = '$ 0'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    mmHeight = 3969
                    mmLeft = 53181
                    mmTop = 30692
                    mmWidth = 31485
                    BandType = 4
                  end
                  object ppSubReport4: TppSubReport
                    UserName = 'SubReport4'
                    ExpandAll = False
                    NewPrintJob = False
                    OutlineSettings.CreateNode = True
                    TraverseAllData = False
                    DataPipelineName = 'ppDBPipelinePiesNoLiquidados'
                    mmHeight = 5027
                    mmLeft = 0
                    mmTop = 43127
                    mmWidth = 197300
                    BandType = 4
                    mmBottomOffset = 0
                    mmOverFlowOffset = 0
                    mmStopPosition = 0
                    mmMinHeight = 0
                    object ppChildReport6: TppChildReport
                      AutoStop = False
                      DataPipeline = ppDBPipelinePiesNoLiquidados
                      PrinterSetup.BinName = 'Default'
                      PrinterSetup.Copies = 2
                      PrinterSetup.DocumentName = 'Report'
                      PrinterSetup.PaperName = 'A4'
                      PrinterSetup.PrinterName = 'Default'
                      PrinterSetup.SaveDeviceSettings = False
                      PrinterSetup.mmMarginBottom = 6350
                      PrinterSetup.mmMarginLeft = 6350
                      PrinterSetup.mmMarginRight = 6350
                      PrinterSetup.mmMarginTop = 6350
                      PrinterSetup.mmPaperHeight = 297000
                      PrinterSetup.mmPaperWidth = 210000
                      PrinterSetup.PaperSize = 9
                      Version = '12.04'
                      mmColumnWidth = 0
                      DataPipelineName = 'ppDBPipelinePiesNoLiquidados'
                      object ppTitleBand3: TppTitleBand
                        mmBottomOffset = 0
                        mmHeight = 11377
                        mmPrintPosition = 0
                        object ppShape30: TppShape
                          UserName = 'Shape30'
                          Brush.Color = 16444382
                          Gradient.EndColor = clWhite
                          Gradient.StartColor = clWhite
                          Gradient.Style = gsNone
                          ParentWidth = True
                          Pen.Style = psClear
                          mmHeight = 10583
                          mmLeft = 0
                          mmTop = 794
                          mmWidth = 197300
                          BandType = 1
                        end
                        object ppLabel80: TppLabel
                          UserName = 'Label80'
                          HyperlinkColor = clBlue
                          Border.BorderPositions = []
                          Border.Color = clBlack
                          Border.Style = psSolid
                          Border.Visible = False
                          Border.Weight = 1.000000000000000000
                          Caption = 'Diferencias por Pies Refinanciaci'#243'n NO Liquidados'
                          Ellipsis = False
                          Font.Charset = DEFAULT_CHARSET
                          Font.Color = clNavy
                          Font.Name = 'Arial'
                          Font.Size = 11
                          Font.Style = [fsBold]
                          Transparent = True
                          mmHeight = 4498
                          mmLeft = 3440
                          mmTop = 1588
                          mmWidth = 94721
                          BandType = 1
                        end
                        object ppLabel81: TppLabel
                          UserName = 'Label81'
                          HyperlinkColor = clBlue
                          AutoSize = False
                          Border.BorderPositions = []
                          Border.Color = clBlack
                          Border.Style = psSolid
                          Border.Visible = False
                          Border.Weight = 1.000000000000000000
                          Caption = 'Forma de Pago'
                          Ellipsis = False
                          Font.Charset = DEFAULT_CHARSET
                          Font.Color = clNavy
                          Font.Name = 'Arial'
                          Font.Size = 10
                          Font.Style = [fsBold]
                          Transparent = True
                          mmHeight = 4233
                          mmLeft = 8996
                          mmTop = 6350
                          mmWidth = 27517
                          BandType = 1
                        end
                        object ppLabel82: TppLabel
                          UserName = 'Label82'
                          HyperlinkColor = clBlue
                          AutoSize = False
                          Border.BorderPositions = []
                          Border.Color = clBlack
                          Border.Style = psSolid
                          Border.Visible = False
                          Border.Weight = 1.000000000000000000
                          Caption = 'Nro. Refinanciaci'#243'n'
                          Ellipsis = False
                          Font.Charset = DEFAULT_CHARSET
                          Font.Color = clNavy
                          Font.Name = 'Arial'
                          Font.Size = 10
                          Font.Style = [fsBold]
                          TextAlignment = taRightJustified
                          Transparent = True
                          mmHeight = 4233
                          mmLeft = 51329
                          mmTop = 6350
                          mmWidth = 33338
                          BandType = 1
                        end
                        object ppLabel83: TppLabel
                          UserName = 'Label83'
                          HyperlinkColor = clBlue
                          AutoSize = False
                          Border.BorderPositions = []
                          Border.Color = clBlack
                          Border.Style = psSolid
                          Border.Visible = False
                          Border.Weight = 1.000000000000000000
                          Caption = 'Importe Cuotas'
                          Ellipsis = False
                          Font.Charset = DEFAULT_CHARSET
                          Font.Color = clNavy
                          Font.Name = 'Arial'
                          Font.Size = 10
                          Font.Style = [fsBold]
                          TextAlignment = taRightJustified
                          Transparent = True
                          mmHeight = 4233
                          mmLeft = 91017
                          mmTop = 6350
                          mmWidth = 31485
                          BandType = 1
                        end
                        object ppLabel119: TppLabel
                          UserName = 'Label1102'
                          HyperlinkColor = clBlue
                          AutoSize = False
                          Border.BorderPositions = []
                          Border.Color = clBlack
                          Border.Style = psSolid
                          Border.Visible = False
                          Border.Weight = 1.000000000000000000
                          Caption = 'Importe Liquidado'
                          Ellipsis = False
                          Font.Charset = DEFAULT_CHARSET
                          Font.Color = clNavy
                          Font.Name = 'Arial'
                          Font.Size = 10
                          Font.Style = [fsBold]
                          TextAlignment = taRightJustified
                          Transparent = True
                          mmHeight = 4233
                          mmLeft = 125942
                          mmTop = 6350
                          mmWidth = 34131
                          BandType = 1
                        end
                        object ppLabel120: TppLabel
                          UserName = 'Label120'
                          HyperlinkColor = clBlue
                          AutoSize = False
                          Border.BorderPositions = []
                          Border.Color = clBlack
                          Border.Style = psSolid
                          Border.Visible = False
                          Border.Weight = 1.000000000000000000
                          Caption = 'Diferencia'
                          Ellipsis = False
                          Font.Charset = DEFAULT_CHARSET
                          Font.Color = clNavy
                          Font.Name = 'Arial'
                          Font.Size = 10
                          Font.Style = [fsBold]
                          TextAlignment = taRightJustified
                          Transparent = True
                          mmHeight = 4233
                          mmLeft = 170127
                          mmTop = 6350
                          mmWidth = 27517
                          BandType = 1
                        end
                      end
                      object ppDetailBand6: TppDetailBand
                        Background1.Brush.Style = bsClear
                        Background1.Gradient.EndColor = clWhite
                        Background1.Gradient.StartColor = clWhite
                        Background1.Gradient.Style = gsNone
                        Background2.Brush.Style = bsClear
                        Background2.Gradient.EndColor = clWhite
                        Background2.Gradient.StartColor = clWhite
                        Background2.Gradient.Style = gsNone
                        mmBottomOffset = 0
                        mmHeight = 4233
                        mmPrintPosition = 0
                        object ppDBText14: TppDBText
                          UserName = 'DBText14'
                          HyperlinkColor = clBlue
                          Border.BorderPositions = []
                          Border.Color = clBlack
                          Border.Style = psSolid
                          Border.Visible = False
                          DataField = 'DescripcionFormaPago'
                          DataPipeline = ppDBPipelinePiesNoLiquidados
                          Ellipsis = False
                          Font.Charset = DEFAULT_CHARSET
                          Font.Color = clNavy
                          Font.Name = 'Arial'
                          Font.Size = 10
                          Font.Style = [fsBold]
                          Transparent = True
                          DataPipelineName = 'ppDBPipelinePiesNoLiquidados'
                          mmHeight = 4163
                          mmLeft = 10583
                          mmTop = 0
                          mmWidth = 39423
                          BandType = 4
                        end
                        object ppDBText15: TppDBText
                          UserName = 'DBText1'
                          HyperlinkColor = clBlue
                          Border.BorderPositions = []
                          Border.Color = clBlack
                          Border.Style = psSolid
                          Border.Visible = False
                          DataField = 'CodigoRefinanciacion'
                          DataPipeline = ppDBPipelinePiesNoLiquidados
                          Ellipsis = False
                          Font.Charset = DEFAULT_CHARSET
                          Font.Color = clBlack
                          Font.Name = 'Arial'
                          Font.Size = 10
                          Font.Style = []
                          TextAlignment = taRightJustified
                          Transparent = True
                          DataPipelineName = 'ppDBPipelinePiesNoLiquidados'
                          mmHeight = 3969
                          mmLeft = 57415
                          mmTop = 264
                          mmWidth = 26194
                          BandType = 4
                        end
                        object ppDBText16: TppDBText
                          UserName = 'DBText16'
                          HyperlinkColor = clBlue
                          Border.BorderPositions = []
                          Border.Color = clBlack
                          Border.Style = psSolid
                          Border.Visible = False
                          DataField = 'TotalFormaPagoCuotasPie'
                          DataPipeline = ppDBPipelinePiesNoLiquidados
                          DisplayFormat = '$ #,##0'
                          Ellipsis = False
                          Font.Charset = DEFAULT_CHARSET
                          Font.Color = clBlack
                          Font.Name = 'Arial'
                          Font.Size = 10
                          Font.Style = []
                          TextAlignment = taRightJustified
                          Transparent = True
                          DataPipelineName = 'ppDBPipelinePiesNoLiquidados'
                          mmHeight = 3969
                          mmLeft = 91016
                          mmTop = 0
                          mmWidth = 31486
                          BandType = 4
                        end
                        object ppDBText17: TppDBText
                          UserName = 'DBText17'
                          HyperlinkColor = clBlue
                          Border.BorderPositions = []
                          Border.Color = clBlack
                          Border.Style = psSolid
                          Border.Visible = False
                          DataField = 'TotalLiquidado'
                          DataPipeline = ppDBPipelinePiesNoLiquidados
                          DisplayFormat = '$ #,##0'
                          Ellipsis = False
                          Font.Charset = DEFAULT_CHARSET
                          Font.Color = clBlack
                          Font.Name = 'Arial'
                          Font.Size = 10
                          Font.Style = []
                          TextAlignment = taRightJustified
                          Transparent = True
                          DataPipelineName = 'ppDBPipelinePiesNoLiquidados'
                          mmHeight = 3969
                          mmLeft = 125941
                          mmTop = 0
                          mmWidth = 34130
                          BandType = 4
                        end
                        object ppDBText22: TppDBText
                          UserName = 'DBText22'
                          HyperlinkColor = clBlue
                          Border.BorderPositions = []
                          Border.Color = clBlack
                          Border.Style = psSolid
                          Border.Visible = False
                          DataField = 'Diferencia'
                          DataPipeline = ppDBPipelinePiesNoLiquidados
                          DisplayFormat = '$ #,##0'
                          Ellipsis = False
                          Font.Charset = DEFAULT_CHARSET
                          Font.Color = clBlack
                          Font.Name = 'Arial'
                          Font.Size = 10
                          Font.Style = []
                          TextAlignment = taRightJustified
                          Transparent = True
                          DataPipelineName = 'ppDBPipelinePiesNoLiquidados'
                          mmHeight = 3969
                          mmLeft = 166160
                          mmTop = 0
                          mmWidth = 31486
                          BandType = 4
                        end
                      end
                    end
                  end
                  object ppLabel116: TppLabel
                    UserName = 'Label116'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'Dep'#243'sito Anticipado'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clNavy
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = [fsBold]
                    Transparent = True
                    mmHeight = 4191
                    mmLeft = 10583
                    mmTop = 38629
                    mmWidth = 34121
                    BandType = 4
                  end
                  object ppLabel121: TppLabel
                    UserName = 'Label121'
                    HyperlinkColor = clBlue
                    AutoSize = False
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = '$ 0'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    mmHeight = 3969
                    mmLeft = 53181
                    mmTop = 38629
                    mmWidth = 31485
                    BandType = 4
                  end
                  object pplblDAImporteSistema: TppLabel
                    UserName = 'pplblDAImporteSistema'
                    HyperlinkColor = clBlue
                    AutoSize = False
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'pplblDAImporteSistema'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    mmHeight = 3969
                    mmLeft = 91017
                    mmTop = 38629
                    mmWidth = 31485
                    BandType = 4
                  end
                  object pplblDAImporteLiquidado: TppLabel
                    UserName = 'pplblDAImporteLiquidado'
                    HyperlinkColor = clBlue
                    AutoSize = False
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'pplblDAImporteLiquidado'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    mmHeight = 3969
                    mmLeft = 128588
                    mmTop = 38629
                    mmWidth = 31485
                    BandType = 4
                  end
                  object pplblDAImporteDiferencia: TppLabel
                    UserName = 'pplblDAImporteDiferencia'
                    HyperlinkColor = clBlue
                    AutoSize = False
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Border.Weight = 1.000000000000000000
                    Caption = 'pplblDAImporteDiferencia'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    mmHeight = 3969
                    mmLeft = 166160
                    mmTop = 38629
                    mmWidth = 31485
                    BandType = 4
                  end
                end
              end
            end
            object ppSubReport5: TppSubReport
              UserName = 'SubReport5'
              ExpandAll = False
              NewPrintJob = False
              OutlineSettings.CreateNode = True
              TraverseAllData = False
              DataPipelineName = 'ppDBpplnObtenerLiquidacionPorCanalDetalle'
              mmHeight = 5027
              mmLeft = 0
              mmTop = 39158
              mmWidth = 197300
              BandType = 4
              mmBottomOffset = 0
              mmOverFlowOffset = 0
              mmStopPosition = 0
              mmMinHeight = 0
              object ppChildReport7: TppChildReport
                AutoStop = False
                DataPipeline = ppDBpplnObtenerLiquidacionPorCanalDetalle
                PrinterSetup.BinName = 'Default'
                PrinterSetup.Copies = 2
                PrinterSetup.DocumentName = 'Report'
                PrinterSetup.PaperName = 'A4'
                PrinterSetup.PrinterName = 'Default'
                PrinterSetup.SaveDeviceSettings = False
                PrinterSetup.mmMarginBottom = 6350
                PrinterSetup.mmMarginLeft = 6350
                PrinterSetup.mmMarginRight = 6350
                PrinterSetup.mmMarginTop = 6350
                PrinterSetup.mmPaperHeight = 297000
                PrinterSetup.mmPaperWidth = 210000
                PrinterSetup.PaperSize = 9
                Version = '12.04'
                mmColumnWidth = 0
                DataPipelineName = 'ppDBpplnObtenerLiquidacionPorCanalDetalle'
                object ppDetailBand7: TppDetailBand
                  Background1.Brush.Style = bsClear
                  Background1.Gradient.EndColor = clWhite
                  Background1.Gradient.StartColor = clWhite
                  Background1.Gradient.Style = gsNone
                  Background2.Brush.Style = bsClear
                  Background2.Gradient.EndColor = clWhite
                  Background2.Gradient.StartColor = clWhite
                  Background2.Gradient.Style = gsNone
                  mmBottomOffset = 0
                  mmHeight = 4498
                  mmPrintPosition = 0
                  object ppDBText59: TppDBText
                    UserName = 'DBText59'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    DataField = 'Concepto'
                    DataPipeline = ppDBpplnObtenerLiquidacionPorCanalDetalle
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = []
                    Transparent = True
                    DataPipelineName = 'ppDBpplnObtenerLiquidacionPorCanalDetalle'
                    mmHeight = 3969
                    mmLeft = 11113
                    mmTop = 529
                    mmWidth = 102394
                    BandType = 4
                  end
                  object ppDBText60: TppDBText
                    UserName = 'DBText60'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    DataField = 'Importe'
                    DataPipeline = ppDBpplnObtenerLiquidacionPorCanalDetalle
                    DisplayFormat = '$ #,##0'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clBlack
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = []
                    TextAlignment = taRightJustified
                    Transparent = True
                    DataPipelineName = 'ppDBpplnObtenerLiquidacionPorCanalDetalle'
                    mmHeight = 3969
                    mmLeft = 115623
                    mmTop = 265
                    mmWidth = 30163
                    BandType = 4
                  end
                end
                object ppSummaryBand3: TppSummaryBand
                  AlignToBottom = False
                  mmBottomOffset = 0
                  mmHeight = 5556
                  mmPrintPosition = 0
                  object ppShape26: TppShape
                    UserName = 'Shape26'
                    Brush.Color = 15579271
                    Gradient.EndColor = clWhite
                    Gradient.StartColor = clWhite
                    Gradient.Style = gsNone
                    Pen.Style = psClear
                    mmHeight = 5027
                    mmLeft = 3440
                    mmTop = 529
                    mmWidth = 143669
                    BandType = 7
                  end
                  object ppLabel128: TppLabel
                    UserName = 'Label128'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    Caption = 'Total:'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clNavy
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = [fsBold]
                    Transparent = True
                    mmHeight = 4233
                    mmLeft = 5027
                    mmTop = 529
                    mmWidth = 9525
                    BandType = 7
                  end
                  object ppDBCalc6: TppDBCalc
                    UserName = 'DBCalc2'
                    HyperlinkColor = clBlue
                    Border.BorderPositions = []
                    Border.Color = clBlack
                    Border.Style = psSolid
                    Border.Visible = False
                    DataField = 'Importe'
                    DataPipeline = ppDBpplnObtenerLiquidacionPorCanalDetalle
                    DisplayFormat = '$ #,##0'
                    Ellipsis = False
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clNavy
                    Font.Name = 'Arial'
                    Font.Size = 10
                    Font.Style = [fsBold]
                    TextAlignment = taRightJustified
                    Transparent = True
                    DataPipelineName = 'ppDBpplnObtenerLiquidacionPorCanalDetalle'
                    mmHeight = 4191
                    mmLeft = 115623
                    mmTop = 529
                    mmWidth = 30163
                    BandType = 7
                  end
                end
                object ppGroup1: TppGroup
                  BreakName = 'FormaPago'
                  DataPipeline = ppDBpplnObtenerLiquidacionPorCanalDetalle
                  GroupFileSettings.NewFile = False
                  GroupFileSettings.EmailFile = False
                  KeepTogether = True
                  OutlineSettings.CreateNode = True
                  StartOnOddPage = False
                  UserName = 'Group1'
                  mmNewColumnThreshold = 0
                  mmNewPageThreshold = 0
                  DataPipelineName = 'ppDBpplnObtenerLiquidacionPorCanalDetalle'
                  NewFile = False
                  object ppGroupHeaderBand1: TppGroupHeaderBand
                    mmBottomOffset = 0
                    mmHeight = 6879
                    mmPrintPosition = 0
                    object ppShape25: TppShape
                      UserName = 'Shape25'
                      Brush.Color = 16444382
                      Gradient.EndColor = clWhite
                      Gradient.StartColor = clWhite
                      Gradient.Style = gsNone
                      Pen.Style = psClear
                      mmHeight = 5027
                      mmLeft = 3440
                      mmTop = 1588
                      mmWidth = 143668
                      BandType = 3
                      GroupNo = 0
                    end
                    object ppLabel123: TppLabel
                      UserName = 'Label123'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Caption = 'Cobros del Turno en:'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clNavy
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = [fsBold]
                      Transparent = True
                      mmHeight = 4233
                      mmLeft = 5821
                      mmTop = 2117
                      mmWidth = 35719
                      BandType = 3
                      GroupNo = 0
                    end
                    object ppDBText23: TppDBText
                      UserName = 'DBText23'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      DataField = 'FormaPago'
                      DataPipeline = ppDBpplnObtenerLiquidacionPorCanalDetalle
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clNavy
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = [fsBold]
                      Transparent = True
                      DataPipelineName = 'ppDBpplnObtenerLiquidacionPorCanalDetalle'
                      mmHeight = 4233
                      mmLeft = 42333
                      mmTop = 2117
                      mmWidth = 71438
                      BandType = 3
                      GroupNo = 0
                    end
                  end
                  object ppGroupFooterBand1: TppGroupFooterBand
                    HideWhenOneDetail = False
                    mmBottomOffset = 0
                    mmHeight = 5556
                    mmPrintPosition = 0
                    object ppShape17: TppShape
                      UserName = 'Shape17'
                      Brush.Color = 15579271
                      Gradient.EndColor = clWhite
                      Gradient.StartColor = clWhite
                      Gradient.Style = gsNone
                      Pen.Style = psClear
                      mmHeight = 5027
                      mmLeft = 3439
                      mmTop = 529
                      mmWidth = 143668
                      BandType = 5
                      GroupNo = 0
                    end
                    object ppLabel10: TppLabel
                      UserName = 'Label10'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Caption = 'SubTotal:'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clNavy
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = [fsBold]
                      Transparent = True
                      mmHeight = 4191
                      mmLeft = 5027
                      mmTop = 794
                      mmWidth = 16298
                      BandType = 5
                      GroupNo = 0
                    end
                    object ppDBCalc4: TppDBCalc
                      UserName = 'DBCalc1'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      DataField = 'Importe'
                      DataPipeline = ppDBpplnObtenerLiquidacionPorCanalDetalle
                      DisplayFormat = '$ #,##0'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clNavy
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = [fsBold]
                      ResetGroup = ppGroup1
                      TextAlignment = taRightJustified
                      Transparent = True
                      DataPipelineName = 'ppDBpplnObtenerLiquidacionPorCanalDetalle'
                      mmHeight = 4233
                      mmLeft = 115623
                      mmTop = 1058
                      mmWidth = 30163
                      BandType = 5
                      GroupNo = 0
                    end
                  end
                end
              end
            end
          end
        end
      end
      object ppAnulaciones: TppSubReport
        UserName = 'ppAnulaciones'
        ExpandAll = False
        KeepTogether = True
        NewPrintJob = True
        OutlineSettings.CreateNode = True
        PrintBehavior = pbSection
        ResetPageNo = False
        TraverseAllData = False
        DataPipelineName = 'ppObtenerAnulacionesCierreTurno'
        mmHeight = 5027
        mmLeft = 0
        mmTop = 11113
        mmWidth = 197300
        BandType = 4
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object ppChildReport2: TppChildReport
          AutoStop = False
          DataPipeline = ppObtenerAnulacionesCierreTurno
          PrinterSetup.BinName = 'Default'
          PrinterSetup.Copies = 2
          PrinterSetup.DocumentName = 'Report'
          PrinterSetup.PaperName = 'A4'
          PrinterSetup.PrinterName = 'Default'
          PrinterSetup.SaveDeviceSettings = False
          PrinterSetup.mmMarginBottom = 6350
          PrinterSetup.mmMarginLeft = 6350
          PrinterSetup.mmMarginRight = 6350
          PrinterSetup.mmMarginTop = 6350
          PrinterSetup.mmPaperHeight = 297000
          PrinterSetup.mmPaperWidth = 210000
          PrinterSetup.PaperSize = 9
          Version = '12.04'
          mmColumnWidth = 0
          DataPipelineName = 'ppObtenerAnulacionesCierreTurno'
          object ppTitleBand2: TppTitleBand
            mmBottomOffset = 0
            mmHeight = 47361
            mmPrintPosition = 0
            object ppImage1: TppImage
              UserName = 'Image1'
              AlignHorizontal = ahCenter
              AlignVertical = avCenter
              MaintainAspectRatio = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Picture.Data = {
                07544269746D6170F2870000424DF28700000000000036000000280000009E00
                0000490000000100180000000000BC870000C40E0000C40E0000000000000000
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE
                FEFEFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFEFEFE
                FFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7171712828283A3A3A
                E6E6E6FFFFFFFFFFFFA8A8A8272727282828818181FFFFFFFFFFFF7B7B7BF0F0
                F0FFFFFFE4E4E4515151FFFFFFFFFFFF9A9A9A2727272F2F2FC3C3C3FFFFFFFC
                FCFC3F3F3F3F3F3F3F3F3F2E2E2EF4F4F4FFFFFF5F5F5F313131323232CACACA
                FFFFFFC5C5C5A1A1A1FFFFFFFFFFFF5B5B5B262626323232D0D0D0FFFFFFF9F9
                F9666666FFFFFFFFFFFF878787ACACACE8E8E8787878FFFFFFFFFFFFE9E9E968
                6868FFFFFF535353FFFFFFFFFFFFDFDFDF696969FFFFFF939393D4D4D4C2C2C2
                9F9F9FFFFFFFFFFFFFC8C8C88C8C8CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF616161757575FFFFFFF9F9F9000000FF
                FFFFB4B4B4222222FFFFFFFFFFFF6D6D6D696969FFFFFF333333E9E9E9FFFFFF
                111111060606FFFFFFA6A6A6313131FFFFFFFFFFFF000000DDDDDDF8F8F81818
                18FFFFFFFFFFFFFFFFFFFFFFFF6C6C6C868686FFFFFFFFFFFF000000FFFFFFA7
                A7A7707070FFFFFF262626A7A7A7FFFFFFFFFFFF000000E8E8E8FBFBFB0F0F0F
                FFFFFFCCCCCC000000838383FFFFFF000000E8E8E8F0F0F05858589D9D9DFFFF
                FF000000FFFFFFFFFFFFC1C1C15C5C5CFFFFFF5B5B5BB5B5B5FFFFFF000000F9
                F9F9F4F4F4252525D1D1D1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFE1E1E1FFFFFF4242
                42D2D2D2FFFFFFFFFFFFFFFFFF000000FFFFFF3A3A3AFFFFFF4D4D4DC3C3C313
                1313FFFFFF373737DADADAFFFFFFFFFFFFEFEFEFECECECF9F9F9030303ABABAB
                9C9C9CAAAAAAFFFFFFFFFFFFE9E9E99191913D3D3D323232FFFFFFAAAAAA7575
                75FFFFFF000000FFFFFFFFFFFFFFFFFF9D9D9D787878FFFFFF1B1B1BFFFFFF00
                0000BCBCBC787878FFFFFF9D9D9D202020868686000000FFFFFFFFFFFF000000
                636363616161111111C0C0C0FFFFFF616161B8B8B8FFFFFF6767674242426A6A
                6A0C0C0CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFEFEFEFFFFFFF535353B9B9B9
                FFFFFFFFFFFFFFFFFF000000FFFFFF464646959595424242FFFFFF040404FFFF
                FF464646CACACAFFFFFFFFFFFFFBFBFBF8F8F8F8F8F80E0E0ED4D4D4C3C3C3CE
                CECEFFFFFFC3C3C3111111ADADADF3F3F3F8F8F8FFFFFFAAAAAA757575FFFFFF
                000000FFFFFFFFFFFFFFFFFF9393937E7E7EFFFFFF323232343434D6D6D6BBBB
                BB767676FFFFFFFFFFFF1B1B1BC6C6C66A6A6AFFFFFFFFFFFF000000FFFFFFFC
                FCFC959595707070FFFFFF616161B7B7B7FFFFFFF2F2F23E3E3E909090A1A1A1
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFA9A9A9151515DBDBDBB5B5B5000000FFFFFFECECEC000000BABABAD4
                D4D41B1B1B959595FFFFFF434343000000FFFFFFFFFFFF000000FFFFFFDCDCDC
                000000CCCCCCCFCFCF000000E9E9E9F9F9F9000000B8B8B8ABABAB9F9F9FFFFF
                FFC6C6C62E2E2EEBEBEB969696181818FFFFFFA3A3A36A6A6AFFFFFF7D7D7D34
                3434D9D9D9B1B1B1000000FBFBFBFFFFFF000000777777FFFFFFAAAAAA6B6B6B
                FFFFFFFFFFFF707070000000F7F7F7FFFFFFFFFFFF000000C8C8C8C0C0C05454
                546D6D6DFFFFFF565656B1B1B1FFFFFFFFFFFF3C3C3C000000FFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFE
                FFFFFFD3D3D3737373868686FFFFFFFFFFFFFFFFFFF8F8F87F7F7F757575D0D0
                D0FFFFFFFFFFFFC5C5C5E7E7E7FFFFFFFFFFFFB5B5B5FFFFFFFFFFFFEEEEEE79
                7979787878F0F0F0FFFFFFFEFEFEA6A6A6A0A0A0A0A0A0979797FFFFFFFFFFFF
                CECECE707070959595FFFFFFFFFFFFE3E3E3D3D3D3FFFFFFFFFFFFC2C2C27171
                71878787FFFFFFFFFFFFFEFEFEA4A4A4FFFFFFFFFFFFE5E5E5D4D4D4FFFFFFFF
                FFFFF8F8F8B5B5B5FFFFFFFFFFFFFFFFFFB2B2B29E9E9E9C9C9CBFBFBFFFFFFF
                FFFFFFCDCDCDE7E7E7FFFFFFFFFFFFE7E7E7C5C5C5FFFFFFFEFEFEFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFEFEFEFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFEFEFEFDFDFDFFFFFFFDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFDFDFDFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFFFFFFFFFFFF
                FFFFFFFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFD
                FDFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFDFDFDF6767671A1A1A0000000000000E0E0E515151B8B8B8FFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF8C8C8C1A1A1A000000000000363636B0B0B0
                FFFFFFFFFFFFFFFFFFFFFFFFD9D9D94D4D4D0606060000000303033B3B3BB7B7
                B7FFFFFFFFFFFFFFFFFFF4F4F43C3C3C000000000000B7B7B7FFFFFFE0E0E044
                4444000000000000393939C3C3C3FFFFFF5151519D9D9DFFFFFF474747B3B3B3
                FFFFFFFFFFFFFFFFFFFFFFFFE1E1E1464646C5C5C5FFFFFFFFFFFFF5F5F56565
                650E0E0E000000000000373737B2B2B2FFFFFFFFFFFFA3A3A34A4A4AFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFCDCDCD393939000000000000434343D3D3D3
                FFFFFF494949B2B2B2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF545454868686FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C8C8C4F4F4FCBCBCBFF
                FFFFFFFFFFFAFAFA6B6B6B0808080000000606064E4E4ED4D4D4FFFFFFFFFFFF
                FFFFFF4D4D4DA0A0A0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F9F94040
                40000000000000B1B1B1FFFFFFFFFFFFEEEEEE6161610C0C0C00000003030338
                3838B7B7B7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3B3B3B
                000000353535A4A4A4D5D5D5E0E0E0B9B9B96D6D6D000000000000D1D1D1FFFF
                FFFFFFFFF9F9F9000000000000BEBEBEF9F9F9F6F6F6A5A5A50000001E1E1EFF
                FFFFFFFFFFB2B2B2000000787878F9F9F9FFFFFFEFEFEF8D8D8D0000007F7F7F
                FFFFFFFFFFFF5C5C5C000000EBEBEBDCDCDCD0D0D0DEDEDE000000323232E6E6
                E6FCFCFCD4D4D4424242060606000000C2C2C2FFFFFF000000878787FFFFFFFF
                FFFFFFFFFFFFFFFFD1D1D1000000A1A1A1FFFFFFBDBDBD000000444444E5E5E5
                FFFFFFFBFBFB9F9F9F000000353535FFFFFF6A6A6A000000FFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFB8B8B80000004B4B4BEDEDEDFAFAFACBCBCB2B2B2B06060600
                0000DFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000003A3A3AFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF646464000000000000ACACACFFFFFFCBCB
                CB0000002F2F2FD6D6D6FBFBFBEEEEEE7D7D7D0000005F5F5FFFFFFFFFFFFF00
                0000676767FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF696969000000E8E8E8
                E1E1E1C7C7C7FFFFFFB5B5B50000004C4C4CE9E9E9FFFFFFF8F8F89C9C9C0000
                00434343FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF040404000000D2D2D2FF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF313131000000D4D4D4FFFFFF
                232323000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F9F9000000565656FFFF
                FF8A8A8A7D7D7DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000292929FFFFFFFF
                FFFF555555000000FFFFFFFFFFFFFFFFFF989898000000D4D4D4FFFFFFFFFFFF
                FFFFFFFFFFFF717171000000D0D0D0FFFFFF0000008C8C8CFFFFFFFFFFFFFFFF
                FFFFFFFFD4D4D4000000A7A7A7FFFFFF0000004F4F4FFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFF3F3F3B2B2B2FFFFFF717171000000FFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFF737373000000F9F9F9FFFFFFFFFFFFFFFFFFFFFFFF4D4D4D000000EDED
                EDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000444444FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF5050500000005C5C5C000000B2B2B2FFFFFF0000003A3A3A
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB1B1B1000000A7A7A7FFFFFF0000006E6E
                6EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF626262000000FFFFFFFFFFFFFF
                FFFFFFFFFF0000005D5D5DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEEEEEEB0B0B0
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFEFEFEFFFFFF5B5B5B000000C4C4C4FFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFF1B1B1B656565FFFFFF0000007E
                7E7EFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFF353535000000FFFFFFFFFFFF
                FFFFFF9090905C5C5C464646161616000000080808DEDEDEFFFFFFFFFFFF5757
                57000000FFFFFFFFFFFFFFFFFFFFFFFF1818180000005F5F5F9B9B9BC5C5C5FF
                FFFFC4C4C4000000D0D0D0FFFFFF0000008D8D8DFFFFFFFFFFFFFFFFFFFFFFFF
                D4D4D4000000ADADADD4D4D40000006767679393938B8B8B8B8B8B8B8B8B8C8C
                8C9999999B9B9BFFFFFF747474000000FFFFFFFEFEFEFFFFFFFFFFFFFFFFFFF5
                F5F50000000000006B6B6B9F9F9FCBCBCBFFFFFF9D9D9D000000EDEDEDFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF000000444444FFFFFFFFFFFFFDFDFDFFFFFFFFFF
                FF393939000000B0B0B0E0E0E0000000B8B8B8C3C3C3000000C5C5C5FFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF0000004B4B4BFFFFFF0000006F6F6FFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF646464000000FFFFFFFFFFFFFFFFFFBEBE
                BE0000007171719191918B8B8B8B8B8B8B8B8B8B8B8B9999999A9A9AF6F6F6FF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFF000000070707FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000005A5A5AFFFF
                FFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFF0000001E1E1EFFFFFFC7C7C700000011
                1111787878979797C1C1C1F3F3F3FFFFFFFFFFFFFEFEFEFFFFFF575757000000
                FFFFFFFFFFFFFDFDFDFFFFFFFFFFFFE9E9E99797977777775858583030301313
                13000000CFCFCFFFFFFF0000007A7A7AFFFFFFFFFFFFFFFFFFFFFFFFD9D9D900
                0000A7A7A7EEEEEE0000004E4E4E8787877B7B7B7B7B7B7B7B7B8787873B3B3B
                000000F4F4F4767676000000FFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFF
                FFDEDEDE939393737373535353262626111111000000ECECECFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF000000444444FFFFFFFFFFFFFFFFFFFFFFFF232323000000
                BFBFBFFFFFFFC8C8C8000000B7B7B7D9D9D9000000A9A9A9FFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFF000000727272FFFFFF0000005E5E5EFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFF646464000000FFFFFFFFFFFFFFFFFFD4D4D4000000
                5858588686867B7B7B7B7B7B7B7B7B878787363636000000EFEFEFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFF4F4F4000000474747FFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF707070000000CFCFCFFFFFFF
                FFFFFFFFFFFFFFFFFF7A7A7A000000C5C5C5FFFFFF686868000000FFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFF000000727272FFFFFFFFFFFF606060000000FFFFFFFF
                FFFFFFFFFFFFFFFF222222848484FFFFFFFFFFFFFFFFFFFFFFFFAFAFAF000000
                D5D5D5FEFEFE000000000000E1E1E1FFFFFFFFFFFFFFFFFFA3A3A3000000C6C6
                C6FFFFFF323232000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF161616080808FF
                FFFF7272720000002C2C2CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0C0C0CA5A5A5
                FFFFFFFFFFFFFFFFFFFFFFFF878787000000F4F4F4FFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF000000444444FFFFFFFFFFFFFFFFFF0D0D0D000000D5D5D5FFFFFFFF
                FFFFCACACA000000B1B1B1FFFFFF1D1D1D000000FEFEFEFFFFFFFFFFFFFFFFFF
                FFFFFF303030000000F9F9F9FFFFFF000000000000B8B8B8FFFFFFFFFFFFFFFF
                FFFEFEFEFFFFFF6F6F6F000000FFFFFFFFFFFFFFFFFFFFFFFF1D1D1D000000FF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF0C0C0C141414FFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFF0000001A1A1AFFFFFFFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFFFFFF7A7A7A00000036363682828278
                7878070707000000B6B6B6FFFFFFFFFFFFF8F8F80E0E0E0000005D5D5D7A7A7A
                616161000000222222FFFFFFFFFFFF0000000404040000001313130000009999
                99FFFFFFC1C1C10000002A2A2A787878818181535353000000353535FFFFFFF6
                F6F60000006464640606063232327676765252520000005C5C5CFFFFFFFFFFFF
                FFFFFF4242420000005A5A5A878787666666000000131313F9F9F9FFFFFF6363
                63000000C0C0C00000002C2C2C515151C6C6C6FFFFFFA3A3A30000003C3C3C7A
                7A7A7F7F7F4949490000005A5A5AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                000000444444FFFFFFFFFFFF000000000000E7E7E7FFFFFFFFFFFFFFFFFFCACA
                CA000000B0B0B0FFFFFFFFFFFF4040400000005252528686866B6B6B00000002
                0202E8E8E8FFFFFFFFFFFF000000686868444444000000474747626262FFFFFF
                000000030303000000131313000000939393FFFFFFFFFFFF3A3A3A0000005E5E
                5E8787876262620000001F1F1FFCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF55
                5555000000D8D8D8FFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFD7D7D7E7E7E7FFFFFFFFFFFFFFFFFFFFFFFFB0B0B0878787888888BFBF
                BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDDDDDD9F9F9F959595A1A1A1E2
                E2E2FFFFFFFFFFFFFFFFFFFAFAFA5151510C0C0CFFFFFFEDEDEDF8F8F8FFFFFF
                FFFFFFFFFFFFB8B8B89393938D8D8DA0A0A0DDDDDDFFFFFFFFFFFFFFFFFFEEEE
                EEFEFEFEFFFFFFC5C5C59494949D9D9DE6E6E6FFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFEEEEEEA0A0A08888889B9B9BDEDEDEFFFFFFFFFFFFFFFFFFF6F6F6EEEEEE
                FFFFFFFFFFFFC7C7C7909090DDDDDDFFFFFFFFFFFFFFFFFFB0B0B09191918F8F
                8FA2A2A2EAEAEAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000004D
                4D4DFFFFFF000000000000F6F6F6FFFFFFFFFFFFFFFFFFFFFFFFCACACA000000
                B0B0B0FFFFFFFFFFFFFFFFFFF0F0F0A3A3A37F7F7F929292D4D4D4FFFFFFFFFF
                FFFFFFFFFFFFFFEFEFEFF7F7F7FFFFFFF1F1F1A5A5A59A9A9AFFFFFFFAFAFA5F
                5F5F000000FFFFFFEEEEEEF5F5F5FFFFFFFFFFFFFFFFFFEAEAEA9E9E9E898989
                9D9D9DE1E1E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000
                00000000EBEBEBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3A3A3A00
                0000D2D2D2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFE
                FEFFFFFFFEFEFEFFFFFF474747000000FFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFEFEFEFFFFFFFFFFFF
                FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFDFDFDFEFEFEFFFFFFFDFDFDFE
                FEFEFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000003030300000
                00000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCACACA000000B0B0B0FF
                FFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FEFEFEFFFFFFFFFFFFFDFDFDFFFFFFFFFFFFFFFFFFFDFDFDFFFFFF5757570000
                00FFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3A3A3A
                000000515151C1C1C1EEEEEEF6F6F6DDDDDD8C8C8C000000000000BCBCBCFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFDADADACACACAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000001A1A1AFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC6C6C6000000ACACACFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDDDDDDC6C6C6FFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE1E1E169
                6969161616000000000000000000333333999999FFFFFFFFFFFFFEFEFEFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6464646C6C6CFFFFFFFFFFFFFDFDFDFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDF545454CECECEFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFEFEFEFEFEFE
                FEFEFEFDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFEFFFFFEFFFFFAFEFFFEFEFFFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFF
                FEFCFFFEFEFFFEFDFFFEFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFEFFFFFDFFFFFFFFFEFFFFFEFFFCFFFEFEFFFEFFFFFEFF
                FEFFFFFEFFFCFFFFFDFFFFFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFE
                FFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFF
                FEFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFAFEFF
                FFFDFFFEFEFFFDFFFFFEFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                F9FFFFFAFFFEFCFEFEFDFFFCFEFFFEFEFEFFFDFEFFFEFEFFFDFEFFFCFFFFFFFE
                FFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFE
                FEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFFFFFF
                FEFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFCFFFE
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFEFCFDFBFFFFFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFEFEFC
                FEFFFEFEFFFDFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFDFEFDFFFFFB
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F9F765B994
                71C19F71C39C71C29C71C29C71C39C71C29C71C29C70C39C65C19782BCA2FFFF
                FFFFFFFFFFFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFBFFFEE8F3EECDE8DAB5E0CD9FD4C18DCFB280C9A671C19E70C29D71
                C39C71C39C71C29C71C39C71C29C71C29C71C39C71C29C71C39C71C29C71C29C
                71C39C71C29C71C39C71C39C71C29C6EC29A7DC2A4FFFFFFFDFFFFFEFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFECF7F10EA15A28AA6A26
                AA6B27A96B24AA6B25AA6B26AA6B28A96B1EA96829A46BEEFDF5FFFFFFFBFFFF
                FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFEFFFFFBFFFEFEFDFEFFFEFEFEFFFEFFFDFEFFFFFEFDFEFFFDFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFE0F3EBB8E0CE8BD1B06BC1984AB08132AA70
                23A1641EA3641EA66422A86523A86526A86728AA692AAA6928AA6B26AA6B26AA
                6B26A96B26AA6B26AA6B26AA6B26AA6B26A96B26AA6B26AA6B26AA6B26AA6B26
                A96B26AA6B26AA6B26A96B22A9683AAA76FFFFFFFEFFFEFEFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7132AE7232AE
                7230AE7233AE7230AD712AAF7027A16AEAF9F3FFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFEFFFFFEFFFEFEFFFEFFFCFEFFFFFFFFFFFFFFFFFFFFFFE9F4F4B2DC
                CC7CC5A349B37F27A5691FA26220A86225A96828AB6C2BAC6E2FAD7030AE7131
                AE7131AD7231AD7231AE7231AE7231AD7231AE7231AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7231AE7233AD72
                33AD7131AE7219A362BFE4D8FFFFFFFEFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFA
                FEFEFEFEFFFFFFFFFFFFFFF7FFFABBDFCF6DC39E38A7741DA46422A96224AB69
                28AD6F2CAE702CAD712BAD7331AD7131AD7132AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE7230AD722EAE712E
                A96B71C199FFFFFFFAFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4FF
                FCABD8C555B78823A1671AA7662CA96B31AC7034AC7231AE7232AE7132AE7032
                AD7233AD7234AD7132AE7132AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722C
                AC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE7230AD7229AE7025A267FCFF
                FFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFFFFFEFEFFFC
                FFFFFFFEFEFEFFFEFFFEFDFDFEFEFFFFFFFFFFFFCDEADD68BE901F9F641BA767
                2AAB6D31AE7130AD7230AE7231AE7131AE7133AD7132AD7231AD7232AD7131AF
                702FAF7031AD7132AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE
                7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFEDF8F31AA36533AE7131AE7231AD7234AD7125AA668CCAB1FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFFFEFFFFFFFF
                FEFDFFFEFFFFFFFFFFFFB5DECA3EAA7922A3602AA96D2BAF7236AC7331AE7131
                AE7132AE7132AD7132AE7231AE7231AE7231AE7232AE7131AE7232AD7232AD72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFF
                FEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8
                F31AA36533AE7131AE7232AD722FAE711BA260F2FBFBFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFFFFFDFFFFFFFDFEFFFFFFFFFFFF
                BEE1D339AA751DA6642EAC6F33AC7230AE732BAD722EAD7332AE7132AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFC
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA365
                33AE7131AE7231AE712BAB6E52B587FFFFFFFFFDFEFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFE3F7F757B2891BA66024
                AE6B31AF6F35AE6F30AE7131AD7332AC7331AD7332AE7132AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131
                AE7231AD7125A96995D3B5FFFFFFFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FEFFFFFAFEFFFEFEFFFFFBFEFFFFFFA7DAC522A16328AB6C32AE7131AD7232AE
                7131AE7232AD7231AE7132AE7132AD7131AE7132AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AD
                711EA766C4E5D4FFFFFFFFFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFFFFFF
                FEFFFEFFFFFFFFFF7AC0A316A45E2AAF6E32AD7231AD7232AE7231AD7232AD72
                32AE7132AE7131AE7131AE7132AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE711AA564
                E1F0EAFFFFFFFFFEFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFF
                FF68BD951CA66332AE7133AE6F31AD7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE7119A463E6F4F1FF
                FFFFFEFFFDFFFFFFFFFFFFFEFFFFFEFFFFFEFFFFFCFFFFFFFFFF84C8AC18A663
                35AE712EAE722FAF7032AD7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722C
                AC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFEDF8F31AA36533AE7131AE7231AD721CA661E1F0EEFFFFFFFEFF
                FCFFFFFFFFFFFFFDFFFFFCFFFFFCFFFFFFFFFFCDE9D915A15F31AE7132AD7232
                AD7132AD7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE
                7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFEDF8F31AA36533AE7131AE7232AD721DA863C8E3D7FFFFFFFDFFFDFFFFFF
                FFFFFFFFFFFFFDFFFFFFFEFFFFFFFF3BAB772AAC6F33AF6D31AD7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFF
                FEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8
                F31AA36533AE7131AE7232AD7223A96897D3B7FFFFFFFEFFFEFFFFFFFFFFFFFD
                FFFFF9FFFFFFFFFFCAE9DF1BA66032AD7233AE6F31AD7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFC
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA365
                33AE7131AE7231AE712CAC6E53B587FFFFFFFEFEFEFEFFFFFEFFFFFDFFFEFFFF
                FEFFFFFF80C8AC28A96932AE7137AB7431AE7132AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131
                AE7231AE7132AD721BA161F1F9F7FFFDFFFEFFFFFBFFFFFCFFFEFFFFFDFFFFFF
                61BC9029AB6C31AE7231AE7132AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE
                7231AD7024A96B7ACAA3FFFFFFFFFFFFFCFFFFFBFDFFFBFFFFFFFFFF5ABA8E2A
                AB6D32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE7132AD72
                2FAD721AA264E9F9F0FFFFFFFFFEFFFEFDFFFCFFFEFFFFFF75C49D27AA6B32AE
                7131AE7132AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE7132AD7230AD722C
                AA6D54B084FFFFFFFFFDFFFFFDFFFFFFFCFFFFFFABD9C21EA8682EAD7232AD72
                31AE7132AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722C
                AC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE7232AE7132AE7131AE7120A7
                6880C7A7FFFFFFFFFFFCFFFEFEFFFEFFF7FFFD1C9F6534AE6E31AD7232AE7132
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE
                7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFEDF8F31AA36533AE7131AE7232AE7232AE7232AE7231AE7132AE711CA165
                98D2B6FFFFFFFCFEFFFEFDFFFFFFFF7FC5A61EAC6432AF7132AD7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFF
                FEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8
                F31AA36533AE7131AE7232AE7232AE7232AE7232AE712FAC742EAE7116A4638E
                CDB2FFFFFFFFFFFFFFFFFDFEFFFF34A47128AB6E32AE7131AE7132AD7232AD72
                31AE7132AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFC
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA365
                33AE7131AE7232AE7232AE7232AE7232AE7131AD7234AD7231AD721BA6606ABD
                97FFFFFFFFFFFFFFFFFFE8F8EE23A26A27AC7035AB7431AF7031AE7034AD7231
                AE7132AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131
                AE7232AE7232AE7232AE7232AE7232AE7131AE7132AE7130AD7125A9663AA977
                D4EEE6FFFFFFFFFFFFE7F7EF36A5741DAC6536AC7233AC7431AD7232AE7131AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F319A36433AE7131AE7131AE
                7131AE7131AE7131AE7131AE7131AE7131AD7231AF7038AC7429AB7116A45D7A
                C3A0FDFFFFFFFFFFFFFFFF69BC9612A2622BAE6C33AF6C2EAD7332AE7131AE71
                31AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE
                7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131
                AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE71
                31AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE
                7131AE7131AE712CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFEAF6F1069D5820A6651EA6651EA6651EA665
                1EA6651EA6651EA6651EA6651EA66522A5651CA76424A66326A66213A25E0F9C
                5883C9A8F3FCF6FFFFFFC8EADD45AB7A15A65F1FA6661EA6651EA6651EA6651E
                A6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA665
                1EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6
                651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651E
                A6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA665
                1EA6651AA56234A770FFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFF9FDFDBEE1D5C3E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5
                E4DAC5E4DAC5E4DAC5E4DAC6E4DAC6E4D9C5E4DAC5E4DAC5E3DBC5E5D9B6E0D1
                BDD9D3FDFEFFFFFFFFFFFFFFCDE7DFC0E3D8C6E4DAC5E4DAC5E4DAC5E4DAC5E4
                DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5
                E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DA
                C5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4
                DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5
                E3D9CAE4DCFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFDFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFDFEFEFCFEFEFEFFFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE
                FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFBFFFFFEFFFFFFFF
                FFFEFFFFFCFFFFFCFEFFFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE
                FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE
                FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE
                FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE
                FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFDFF
                FFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFD
                FFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFF
                FDFFFFFCFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFF
                FFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFD
                FFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFF
                FDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFF
                FFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFEFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000}
              mmHeight = 21960
              mmLeft = 4763
              mmTop = 0
              mmWidth = 53711
              BandType = 1
            end
            object ppLabel16: TppLabel
              UserName = 'Label16'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'Anulaciones'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 13
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 5419
              mmLeft = 90466
              mmTop = 10054
              mmWidth = 27178
              BandType = 1
            end
            object ppLabel17: TppLabel
              UserName = 'Label303'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'Informe Cierre de Turno'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 16
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 6646
              mmLeft = 72231
              mmTop = 3440
              mmWidth = 64093
              BandType = 1
            end
            object ppLine2: TppLine
              UserName = 'Line2'
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              ParentWidth = True
              Position = lpBottom
              Weight = 0.750000000000000000
              mmHeight = 1323
              mmLeft = 0
              mmTop = 22754
              mmWidth = 197300
              BandType = 1
            end
            object ppLabel18: TppLabel
              UserName = 'Label18'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'N'#250'mero Turno:'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4191
              mmLeft = 3969
              mmTop = 25135
              mmWidth = 25485
              BandType = 1
            end
            object ppDBText3: TppDBText
              UserName = 'DBText1'
              HyperlinkColor = clBlue
              AutoSize = True
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              DataField = 'NumeroTurno'
              DataPipeline = DBPEncabezadoCierreTurno
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              ParentDataPipeline = False
              Transparent = True
              DataPipelineName = 'DBPEncabezadoCierreTurno'
              mmHeight = 4022
              mmLeft = 33073
              mmTop = 25135
              mmWidth = 21802
              BandType = 1
            end
            object ppLabel19: TppLabel
              UserName = 'Label19'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'POS:'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4191
              mmLeft = 3969
              mmTop = 29898
              mmWidth = 8594
              BandType = 1
            end
            object ppDBText6: TppDBText
              UserName = 'DBText6'
              HyperlinkColor = clBlue
              AutoSize = True
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              DataField = 'PuntoEntrega'
              DataPipeline = DBPEncabezadoCierreTurno
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              ParentDataPipeline = False
              Transparent = True
              DataPipelineName = 'DBPEncabezadoCierreTurno'
              mmHeight = 4022
              mmLeft = 15875
              mmTop = 29898
              mmWidth = 21414
              BandType = 1
            end
            object ppLabel20: TppLabel
              UserName = 'Label1'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'Punto de Venta:'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4191
              mmLeft = 3969
              mmTop = 34660
              mmWidth = 26797
              BandType = 1
            end
            object ppDBText7: TppDBText
              UserName = 'DBText7'
              HyperlinkColor = clBlue
              AutoSize = True
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              DataField = 'PuntoVenta'
              DataPipeline = DBPEncabezadoCierreTurno
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              ParentDataPipeline = False
              Transparent = True
              DataPipelineName = 'DBPEncabezadoCierreTurno'
              mmHeight = 4022
              mmLeft = 33338
              mmTop = 34660
              mmWidth = 18274
              BandType = 1
            end
            object ppLabel21: TppLabel
              UserName = 'Label2'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'Usuario:'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4233
              mmLeft = 116681
              mmTop = 25929
              mmWidth = 14288
              BandType = 1
            end
            object ppLabel22: TppLabel
              UserName = 'Label3'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'Fecha de Cierre:'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4233
              mmLeft = 116946
              mmTop = 34660
              mmWidth = 28046
              BandType = 1
            end
            object ppDBText8: TppDBText
              UserName = 'DBText8'
              HyperlinkColor = clBlue
              AutoSize = True
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              DataField = 'FechaHoraCierre'
              DataPipeline = DBPEncabezadoCierreTurno
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              ParentDataPipeline = False
              Transparent = True
              DataPipelineName = 'DBPEncabezadoCierreTurno'
              mmHeight = 4022
              mmLeft = 150284
              mmTop = 34660
              mmWidth = 26952
              BandType = 1
            end
            object ppDBText9: TppDBText
              UserName = 'DBText9'
              HyperlinkColor = clBlue
              AutoSize = True
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              DataField = 'NombrePersona'
              DataPipeline = DBPEncabezadoCierreTurno
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              ParentDataPipeline = False
              Transparent = True
              DataPipelineName = 'DBPEncabezadoCierreTurno'
              mmHeight = 4022
              mmLeft = 150284
              mmTop = 25929
              mmWidth = 25647
              BandType = 1
            end
            object ppLine3: TppLine
              UserName = 'Line1'
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              ParentWidth = True
              Position = lpBottom
              Weight = 0.750000000000000000
              mmHeight = 1323
              mmLeft = 0
              mmTop = 38894
              mmWidth = 197300
              BandType = 1
            end
            object ppLabel23: TppLabel
              UserName = 'Label4'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Fecha Anulaci'#243'n'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4191
              mmLeft = 3970
              mmTop = 42598
              mmWidth = 28152
              BandType = 1
            end
            object ppLabel24: TppLabel
              UserName = 'Label5'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'N'#250'mero Recibo'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = [fsBold]
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 4191
              mmLeft = 50471
              mmTop = 42598
              mmWidth = 25993
              BandType = 1
            end
            object ppLabel25: TppLabel
              UserName = 'Label25'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Usuario'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4191
              mmLeft = 83078
              mmTop = 42863
              mmWidth = 46830
              BandType = 1
            end
            object ppLabel26: TppLabel
              UserName = 'Label26'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Importe'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = [fsBold]
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 4191
              mmLeft = 164096
              mmTop = 42863
              mmWidth = 12912
              BandType = 1
            end
          end
          object ppDetailBand2: TppDetailBand
            Background1.Brush.Style = bsClear
            Background1.Gradient.EndColor = clWhite
            Background1.Gradient.StartColor = clWhite
            Background1.Gradient.Style = gsNone
            Background2.Brush.Style = bsClear
            Background2.Gradient.EndColor = clWhite
            Background2.Gradient.StartColor = clWhite
            Background2.Gradient.Style = gsNone
            mmBottomOffset = 0
            mmHeight = 4498
            mmPrintPosition = 0
            object ppDBText10: TppDBText
              UserName = 'DBText10'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'FechaAnulacion'
              DataPipeline = ppObtenerAnulacionesCierreTurno
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              DataPipelineName = 'ppObtenerAnulacionesCierreTurno'
              mmHeight = 3969
              mmLeft = 3969
              mmTop = 265
              mmWidth = 42863
              BandType = 4
            end
            object ppDBText11: TppDBText
              UserName = 'DBText11'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'NumeroRecibo'
              DataPipeline = ppObtenerAnulacionesCierreTurno
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taRightJustified
              Transparent = True
              DataPipelineName = 'ppObtenerAnulacionesCierreTurno'
              mmHeight = 3969
              mmLeft = 50271
              mmTop = 265
              mmWidth = 26194
              BandType = 4
            end
            object ppDBText12: TppDBText
              UserName = 'DBText12'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'CodigoUsuario'
              DataPipeline = ppObtenerAnulacionesCierreTurno
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              Transparent = True
              DataPipelineName = 'ppObtenerAnulacionesCierreTurno'
              mmHeight = 4022
              mmLeft = 83079
              mmTop = 265
              mmWidth = 46831
              BandType = 4
            end
            object ppDBText13: TppDBText
              UserName = 'DBText13'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'DescImporte'
              DataPipeline = ppObtenerAnulacionesCierreTurno
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              TextAlignment = taRightJustified
              Transparent = True
              DataPipelineName = 'ppObtenerAnulacionesCierreTurno'
              mmHeight = 4233
              mmLeft = 134673
              mmTop = 265
              mmWidth = 42333
              BandType = 4
            end
          end
          object ppSummaryBand2: TppSummaryBand
            AlignToBottom = False
            mmBottomOffset = 0
            mmHeight = 13229
            mmPrintPosition = 0
            object ppLabel27: TppLabel
              UserName = 'Label27'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Caption = 'Total:'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = [fsBold]
              TextAlignment = taRightJustified
              Transparent = True
              mmHeight = 4191
              mmLeft = 110511
              mmTop = 4498
              mmWidth = 9610
              BandType = 7
            end
            object ppDBCalc3: TppDBCalc
              UserName = 'DBCalc1'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              DataField = 'Importe'
              DataPipeline = ppObtenerAnulacionesCierreTurno
              DisplayFormat = '$#######0;($#######0)'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = [fsBold]
              TextAlignment = taRightJustified
              Transparent = True
              DataPipelineName = 'ppObtenerAnulacionesCierreTurno'
              mmHeight = 4191
              mmLeft = 134674
              mmTop = 4498
              mmWidth = 42334
              BandType = 7
            end
          end
        end
      end
      object ppLiquidacionTAGsReport: TppSubReport
        UserName = 'ppLiquidacionTAGsReport'
        ExpandAll = False
        KeepTogether = True
        NewPrintJob = True
        OutlineSettings.CreateNode = True
        PrintBehavior = pbSection
        ResetPageNo = False
        TraverseAllData = False
        DataPipelineName = 'DBPEncabezadoCierreTurno'
        mmHeight = 5027
        mmLeft = 0
        mmTop = 16669
        mmWidth = 197300
        BandType = 4
        mmBottomOffset = 0
        mmOverFlowOffset = 0
        mmStopPosition = 0
        mmMinHeight = 0
        object ppChildReport10: TppChildReport
          AutoStop = False
          DataPipeline = DBPEncabezadoCierreTurno
          PrinterSetup.BinName = 'Default'
          PrinterSetup.Copies = 2
          PrinterSetup.DocumentName = 'Report'
          PrinterSetup.PaperName = 'A4'
          PrinterSetup.PrinterName = 'Default'
          PrinterSetup.SaveDeviceSettings = False
          PrinterSetup.mmMarginBottom = 6350
          PrinterSetup.mmMarginLeft = 6350
          PrinterSetup.mmMarginRight = 6350
          PrinterSetup.mmMarginTop = 6350
          PrinterSetup.mmPaperHeight = 297000
          PrinterSetup.mmPaperWidth = 210000
          PrinterSetup.PaperSize = 9
          Version = '12.04'
          mmColumnWidth = 0
          DataPipelineName = 'DBPEncabezadoCierreTurno'
          object ppTitleBand10: TppTitleBand
            mmBottomOffset = 0
            mmHeight = 40746
            mmPrintPosition = 0
            object ppLabel30: TppLabel
              UserName = 'Label30'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'Informe Cierre de Turno'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 16
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 6646
              mmLeft = 72331
              mmTop = 3440
              mmWidth = 64093
              BandType = 1
            end
            object ppLabel31: TppLabel
              UserName = 'Label31'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'Usuario:'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4233
              mmLeft = 116681
              mmTop = 25929
              mmWidth = 14288
              BandType = 1
            end
            object ppDBText24: TppDBText
              UserName = 'DBText24'
              HyperlinkColor = clBlue
              AutoSize = True
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              DataField = 'NombrePersona'
              DataPipeline = DBPEncabezadoCierreTurno
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              ParentDataPipeline = False
              Transparent = True
              DataPipelineName = 'DBPEncabezadoCierreTurno'
              mmHeight = 4022
              mmLeft = 150284
              mmTop = 25929
              mmWidth = 25647
              BandType = 1
            end
            object ppLine11: TppLine
              UserName = 'Line11'
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              ParentWidth = True
              Position = lpBottom
              Weight = 0.750000000000000000
              mmHeight = 1323
              mmLeft = 0
              mmTop = 38894
              mmWidth = 197300
              BandType = 1
            end
            object ppLabel32: TppLabel
              UserName = 'Label32'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'POS:'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4191
              mmLeft = 3969
              mmTop = 29898
              mmWidth = 8594
              BandType = 1
            end
            object ppLabel33: TppLabel
              UserName = 'Label33'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'Punto de Venta:'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4191
              mmLeft = 3969
              mmTop = 34660
              mmWidth = 26797
              BandType = 1
            end
            object ppLabel34: TppLabel
              UserName = 'Label34'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'Fecha de Cierre:'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4233
              mmLeft = 116946
              mmTop = 34660
              mmWidth = 28046
              BandType = 1
            end
            object ppDBText25: TppDBText
              UserName = 'DBText25'
              HyperlinkColor = clBlue
              AutoSize = True
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              DataField = 'FechaHoraCierre'
              DataPipeline = DBPEncabezadoCierreTurno
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              ParentDataPipeline = False
              Transparent = True
              DataPipelineName = 'DBPEncabezadoCierreTurno'
              mmHeight = 4022
              mmLeft = 150284
              mmTop = 34660
              mmWidth = 26952
              BandType = 1
            end
            object ppDBText26: TppDBText
              UserName = 'DBText26'
              HyperlinkColor = clBlue
              AutoSize = True
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              DataField = 'PuntoEntrega'
              DataPipeline = DBPEncabezadoCierreTurno
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              ParentDataPipeline = False
              Transparent = True
              DataPipelineName = 'DBPEncabezadoCierreTurno'
              mmHeight = 4022
              mmLeft = 15875
              mmTop = 29898
              mmWidth = 21414
              BandType = 1
            end
            object ppDBText27: TppDBText
              UserName = 'DBText27'
              HyperlinkColor = clBlue
              AutoSize = True
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              DataField = 'PuntoVenta'
              DataPipeline = DBPEncabezadoCierreTurno
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              ParentDataPipeline = False
              Transparent = True
              DataPipelineName = 'DBPEncabezadoCierreTurno'
              mmHeight = 4022
              mmLeft = 33338
              mmTop = 34660
              mmWidth = 18274
              BandType = 1
            end
            object ppLine20: TppLine
              UserName = 'Line102'
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              ParentWidth = True
              Position = lpBottom
              Weight = 0.750000000000000000
              mmHeight = 1323
              mmLeft = 0
              mmTop = 22754
              mmWidth = 197300
              BandType = 1
            end
            object ppLabel35: TppLabel
              UserName = 'Label35'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'Saldos Informados y Total de Convenios'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 11
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4657
              mmLeft = 66411
              mmTop = 16140
              mmWidth = 75142
              BandType = 1
            end
            object ppLabel37: TppLabel
              UserName = 'Label37'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'N'#250'mero Turno:'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = [fsBold]
              Transparent = True
              mmHeight = 4191
              mmLeft = 3969
              mmTop = 25135
              mmWidth = 25485
              BandType = 1
            end
            object ppDBText28: TppDBText
              UserName = 'DBText28'
              HyperlinkColor = clBlue
              AutoSize = True
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              DataField = 'NumeroTurno'
              DataPipeline = DBPEncabezadoCierreTurno
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 10
              Font.Style = []
              ParentDataPipeline = False
              Transparent = True
              DataPipelineName = 'DBPEncabezadoCierreTurno'
              mmHeight = 4022
              mmLeft = 33073
              mmTop = 25135
              mmWidth = 21802
              BandType = 1
            end
            object ppLabel38: TppLabel
              UserName = 'Label38'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'General Prieto 1430. Independencia'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              mmHeight = 3260
              mmLeft = 7938
              mmTop = 18785
              mmWidth = 45466
              BandType = 1
            end
            object ppImage2: TppImage
              UserName = 'Image2'
              AlignHorizontal = ahCenter
              AlignVertical = avCenter
              MaintainAspectRatio = False
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Picture.Data = {
                07544269746D6170F2870000424DF28700000000000036000000280000009E00
                0000490000000100180000000000BC870000C40E0000C40E0000000000000000
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE
                FEFEFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFEFEFE
                FFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7171712828283A3A3A
                E6E6E6FFFFFFFFFFFFA8A8A8272727282828818181FFFFFFFFFFFF7B7B7BF0F0
                F0FFFFFFE4E4E4515151FFFFFFFFFFFF9A9A9A2727272F2F2FC3C3C3FFFFFFFC
                FCFC3F3F3F3F3F3F3F3F3F2E2E2EF4F4F4FFFFFF5F5F5F313131323232CACACA
                FFFFFFC5C5C5A1A1A1FFFFFFFFFFFF5B5B5B262626323232D0D0D0FFFFFFF9F9
                F9666666FFFFFFFFFFFF878787ACACACE8E8E8787878FFFFFFFFFFFFE9E9E968
                6868FFFFFF535353FFFFFFFFFFFFDFDFDF696969FFFFFF939393D4D4D4C2C2C2
                9F9F9FFFFFFFFFFFFFC8C8C88C8C8CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF616161757575FFFFFFF9F9F9000000FF
                FFFFB4B4B4222222FFFFFFFFFFFF6D6D6D696969FFFFFF333333E9E9E9FFFFFF
                111111060606FFFFFFA6A6A6313131FFFFFFFFFFFF000000DDDDDDF8F8F81818
                18FFFFFFFFFFFFFFFFFFFFFFFF6C6C6C868686FFFFFFFFFFFF000000FFFFFFA7
                A7A7707070FFFFFF262626A7A7A7FFFFFFFFFFFF000000E8E8E8FBFBFB0F0F0F
                FFFFFFCCCCCC000000838383FFFFFF000000E8E8E8F0F0F05858589D9D9DFFFF
                FF000000FFFFFFFFFFFFC1C1C15C5C5CFFFFFF5B5B5BB5B5B5FFFFFF000000F9
                F9F9F4F4F4252525D1D1D1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFE1E1E1FFFFFF4242
                42D2D2D2FFFFFFFFFFFFFFFFFF000000FFFFFF3A3A3AFFFFFF4D4D4DC3C3C313
                1313FFFFFF373737DADADAFFFFFFFFFFFFEFEFEFECECECF9F9F9030303ABABAB
                9C9C9CAAAAAAFFFFFFFFFFFFE9E9E99191913D3D3D323232FFFFFFAAAAAA7575
                75FFFFFF000000FFFFFFFFFFFFFFFFFF9D9D9D787878FFFFFF1B1B1BFFFFFF00
                0000BCBCBC787878FFFFFF9D9D9D202020868686000000FFFFFFFFFFFF000000
                636363616161111111C0C0C0FFFFFF616161B8B8B8FFFFFF6767674242426A6A
                6A0C0C0CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFEFEFEFFFFFFF535353B9B9B9
                FFFFFFFFFFFFFFFFFF000000FFFFFF464646959595424242FFFFFF040404FFFF
                FF464646CACACAFFFFFFFFFFFFFBFBFBF8F8F8F8F8F80E0E0ED4D4D4C3C3C3CE
                CECEFFFFFFC3C3C3111111ADADADF3F3F3F8F8F8FFFFFFAAAAAA757575FFFFFF
                000000FFFFFFFFFFFFFFFFFF9393937E7E7EFFFFFF323232343434D6D6D6BBBB
                BB767676FFFFFFFFFFFF1B1B1BC6C6C66A6A6AFFFFFFFFFFFF000000FFFFFFFC
                FCFC959595707070FFFFFF616161B7B7B7FFFFFFF2F2F23E3E3E909090A1A1A1
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFA9A9A9151515DBDBDBB5B5B5000000FFFFFFECECEC000000BABABAD4
                D4D41B1B1B959595FFFFFF434343000000FFFFFFFFFFFF000000FFFFFFDCDCDC
                000000CCCCCCCFCFCF000000E9E9E9F9F9F9000000B8B8B8ABABAB9F9F9FFFFF
                FFC6C6C62E2E2EEBEBEB969696181818FFFFFFA3A3A36A6A6AFFFFFF7D7D7D34
                3434D9D9D9B1B1B1000000FBFBFBFFFFFF000000777777FFFFFFAAAAAA6B6B6B
                FFFFFFFFFFFF707070000000F7F7F7FFFFFFFFFFFF000000C8C8C8C0C0C05454
                546D6D6DFFFFFF565656B1B1B1FFFFFFFFFFFF3C3C3C000000FFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFE
                FFFFFFD3D3D3737373868686FFFFFFFFFFFFFFFFFFF8F8F87F7F7F757575D0D0
                D0FFFFFFFFFFFFC5C5C5E7E7E7FFFFFFFFFFFFB5B5B5FFFFFFFFFFFFEEEEEE79
                7979787878F0F0F0FFFFFFFEFEFEA6A6A6A0A0A0A0A0A0979797FFFFFFFFFFFF
                CECECE707070959595FFFFFFFFFFFFE3E3E3D3D3D3FFFFFFFFFFFFC2C2C27171
                71878787FFFFFFFFFFFFFEFEFEA4A4A4FFFFFFFFFFFFE5E5E5D4D4D4FFFFFFFF
                FFFFF8F8F8B5B5B5FFFFFFFFFFFFFFFFFFB2B2B29E9E9E9C9C9CBFBFBFFFFFFF
                FFFFFFCDCDCDE7E7E7FFFFFFFFFFFFE7E7E7C5C5C5FFFFFFFEFEFEFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFEFEFEFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFEFEFEFDFDFDFFFFFFFDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFDFDFDFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFFFFFFFFFFFF
                FFFFFFFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFD
                FDFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFDFDFDF6767671A1A1A0000000000000E0E0E515151B8B8B8FFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF8C8C8C1A1A1A000000000000363636B0B0B0
                FFFFFFFFFFFFFFFFFFFFFFFFD9D9D94D4D4D0606060000000303033B3B3BB7B7
                B7FFFFFFFFFFFFFFFFFFF4F4F43C3C3C000000000000B7B7B7FFFFFFE0E0E044
                4444000000000000393939C3C3C3FFFFFF5151519D9D9DFFFFFF474747B3B3B3
                FFFFFFFFFFFFFFFFFFFFFFFFE1E1E1464646C5C5C5FFFFFFFFFFFFF5F5F56565
                650E0E0E000000000000373737B2B2B2FFFFFFFFFFFFA3A3A34A4A4AFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFCDCDCD393939000000000000434343D3D3D3
                FFFFFF494949B2B2B2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF545454868686FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C8C8C4F4F4FCBCBCBFF
                FFFFFFFFFFFAFAFA6B6B6B0808080000000606064E4E4ED4D4D4FFFFFFFFFFFF
                FFFFFF4D4D4DA0A0A0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F9F94040
                40000000000000B1B1B1FFFFFFFFFFFFEEEEEE6161610C0C0C00000003030338
                3838B7B7B7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3B3B3B
                000000353535A4A4A4D5D5D5E0E0E0B9B9B96D6D6D000000000000D1D1D1FFFF
                FFFFFFFFF9F9F9000000000000BEBEBEF9F9F9F6F6F6A5A5A50000001E1E1EFF
                FFFFFFFFFFB2B2B2000000787878F9F9F9FFFFFFEFEFEF8D8D8D0000007F7F7F
                FFFFFFFFFFFF5C5C5C000000EBEBEBDCDCDCD0D0D0DEDEDE000000323232E6E6
                E6FCFCFCD4D4D4424242060606000000C2C2C2FFFFFF000000878787FFFFFFFF
                FFFFFFFFFFFFFFFFD1D1D1000000A1A1A1FFFFFFBDBDBD000000444444E5E5E5
                FFFFFFFBFBFB9F9F9F000000353535FFFFFF6A6A6A000000FFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFB8B8B80000004B4B4BEDEDEDFAFAFACBCBCB2B2B2B06060600
                0000DFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000003A3A3AFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF646464000000000000ACACACFFFFFFCBCB
                CB0000002F2F2FD6D6D6FBFBFBEEEEEE7D7D7D0000005F5F5FFFFFFFFFFFFF00
                0000676767FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF696969000000E8E8E8
                E1E1E1C7C7C7FFFFFFB5B5B50000004C4C4CE9E9E9FFFFFFF8F8F89C9C9C0000
                00434343FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF040404000000D2D2D2FF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF313131000000D4D4D4FFFFFF
                232323000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F9F9000000565656FFFF
                FF8A8A8A7D7D7DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000292929FFFFFFFF
                FFFF555555000000FFFFFFFFFFFFFFFFFF989898000000D4D4D4FFFFFFFFFFFF
                FFFFFFFFFFFF717171000000D0D0D0FFFFFF0000008C8C8CFFFFFFFFFFFFFFFF
                FFFFFFFFD4D4D4000000A7A7A7FFFFFF0000004F4F4FFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFF3F3F3B2B2B2FFFFFF717171000000FFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFF737373000000F9F9F9FFFFFFFFFFFFFFFFFFFFFFFF4D4D4D000000EDED
                EDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000444444FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF5050500000005C5C5C000000B2B2B2FFFFFF0000003A3A3A
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB1B1B1000000A7A7A7FFFFFF0000006E6E
                6EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF626262000000FFFFFFFFFFFFFF
                FFFFFFFFFF0000005D5D5DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEEEEEEB0B0B0
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFEFEFEFFFFFF5B5B5B000000C4C4C4FFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFF1B1B1B656565FFFFFF0000007E
                7E7EFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFF353535000000FFFFFFFFFFFF
                FFFFFF9090905C5C5C464646161616000000080808DEDEDEFFFFFFFFFFFF5757
                57000000FFFFFFFFFFFFFFFFFFFFFFFF1818180000005F5F5F9B9B9BC5C5C5FF
                FFFFC4C4C4000000D0D0D0FFFFFF0000008D8D8DFFFFFFFFFFFFFFFFFFFFFFFF
                D4D4D4000000ADADADD4D4D40000006767679393938B8B8B8B8B8B8B8B8B8C8C
                8C9999999B9B9BFFFFFF747474000000FFFFFFFEFEFEFFFFFFFFFFFFFFFFFFF5
                F5F50000000000006B6B6B9F9F9FCBCBCBFFFFFF9D9D9D000000EDEDEDFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF000000444444FFFFFFFFFFFFFDFDFDFFFFFFFFFF
                FF393939000000B0B0B0E0E0E0000000B8B8B8C3C3C3000000C5C5C5FFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF0000004B4B4BFFFFFF0000006F6F6FFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF646464000000FFFFFFFFFFFFFFFFFFBEBE
                BE0000007171719191918B8B8B8B8B8B8B8B8B8B8B8B9999999A9A9AF6F6F6FF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFF000000070707FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000005A5A5AFFFF
                FFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFF0000001E1E1EFFFFFFC7C7C700000011
                1111787878979797C1C1C1F3F3F3FFFFFFFFFFFFFEFEFEFFFFFF575757000000
                FFFFFFFFFFFFFDFDFDFFFFFFFFFFFFE9E9E99797977777775858583030301313
                13000000CFCFCFFFFFFF0000007A7A7AFFFFFFFFFFFFFFFFFFFFFFFFD9D9D900
                0000A7A7A7EEEEEE0000004E4E4E8787877B7B7B7B7B7B7B7B7B8787873B3B3B
                000000F4F4F4767676000000FFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFF
                FFDEDEDE939393737373535353262626111111000000ECECECFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF000000444444FFFFFFFFFFFFFFFFFFFFFFFF232323000000
                BFBFBFFFFFFFC8C8C8000000B7B7B7D9D9D9000000A9A9A9FFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFF000000727272FFFFFF0000005E5E5EFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFF646464000000FFFFFFFFFFFFFFFFFFD4D4D4000000
                5858588686867B7B7B7B7B7B7B7B7B878787363636000000EFEFEFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFF4F4F4000000474747FFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF707070000000CFCFCFFFFFFF
                FFFFFFFFFFFFFFFFFF7A7A7A000000C5C5C5FFFFFF686868000000FFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFF000000727272FFFFFFFFFFFF606060000000FFFFFFFF
                FFFFFFFFFFFFFFFF222222848484FFFFFFFFFFFFFFFFFFFFFFFFAFAFAF000000
                D5D5D5FEFEFE000000000000E1E1E1FFFFFFFFFFFFFFFFFFA3A3A3000000C6C6
                C6FFFFFF323232000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF161616080808FF
                FFFF7272720000002C2C2CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0C0C0CA5A5A5
                FFFFFFFFFFFFFFFFFFFFFFFF878787000000F4F4F4FFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF000000444444FFFFFFFFFFFFFFFFFF0D0D0D000000D5D5D5FFFFFFFF
                FFFFCACACA000000B1B1B1FFFFFF1D1D1D000000FEFEFEFFFFFFFFFFFFFFFFFF
                FFFFFF303030000000F9F9F9FFFFFF000000000000B8B8B8FFFFFFFFFFFFFFFF
                FFFEFEFEFFFFFF6F6F6F000000FFFFFFFFFFFFFFFFFFFFFFFF1D1D1D000000FF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF0C0C0C141414FFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFF0000001A1A1AFFFFFFFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFFFFFF7A7A7A00000036363682828278
                7878070707000000B6B6B6FFFFFFFFFFFFF8F8F80E0E0E0000005D5D5D7A7A7A
                616161000000222222FFFFFFFFFFFF0000000404040000001313130000009999
                99FFFFFFC1C1C10000002A2A2A787878818181535353000000353535FFFFFFF6
                F6F60000006464640606063232327676765252520000005C5C5CFFFFFFFFFFFF
                FFFFFF4242420000005A5A5A878787666666000000131313F9F9F9FFFFFF6363
                63000000C0C0C00000002C2C2C515151C6C6C6FFFFFFA3A3A30000003C3C3C7A
                7A7A7F7F7F4949490000005A5A5AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                000000444444FFFFFFFFFFFF000000000000E7E7E7FFFFFFFFFFFFFFFFFFCACA
                CA000000B0B0B0FFFFFFFFFFFF4040400000005252528686866B6B6B00000002
                0202E8E8E8FFFFFFFFFFFF000000686868444444000000474747626262FFFFFF
                000000030303000000131313000000939393FFFFFFFFFFFF3A3A3A0000005E5E
                5E8787876262620000001F1F1FFCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF55
                5555000000D8D8D8FFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFD7D7D7E7E7E7FFFFFFFFFFFFFFFFFFFFFFFFB0B0B0878787888888BFBF
                BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDDDDDD9F9F9F959595A1A1A1E2
                E2E2FFFFFFFFFFFFFFFFFFFAFAFA5151510C0C0CFFFFFFEDEDEDF8F8F8FFFFFF
                FFFFFFFFFFFFB8B8B89393938D8D8DA0A0A0DDDDDDFFFFFFFFFFFFFFFFFFEEEE
                EEFEFEFEFFFFFFC5C5C59494949D9D9DE6E6E6FFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFEEEEEEA0A0A08888889B9B9BDEDEDEFFFFFFFFFFFFFFFFFFF6F6F6EEEEEE
                FFFFFFFFFFFFC7C7C7909090DDDDDDFFFFFFFFFFFFFFFFFFB0B0B09191918F8F
                8FA2A2A2EAEAEAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000004D
                4D4DFFFFFF000000000000F6F6F6FFFFFFFFFFFFFFFFFFFFFFFFCACACA000000
                B0B0B0FFFFFFFFFFFFFFFFFFF0F0F0A3A3A37F7F7F929292D4D4D4FFFFFFFFFF
                FFFFFFFFFFFFFFEFEFEFF7F7F7FFFFFFF1F1F1A5A5A59A9A9AFFFFFFFAFAFA5F
                5F5F000000FFFFFFEEEEEEF5F5F5FFFFFFFFFFFFFFFFFFEAEAEA9E9E9E898989
                9D9D9DE1E1E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000
                00000000EBEBEBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3A3A3A00
                0000D2D2D2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFE
                FEFFFFFFFEFEFEFFFFFF474747000000FFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFEFEFEFFFFFFFFFFFF
                FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFDFDFDFEFEFEFFFFFFFDFDFDFE
                FEFEFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000003030300000
                00000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCACACA000000B0B0B0FF
                FFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FEFEFEFFFFFFFFFFFFFDFDFDFFFFFFFFFFFFFFFFFFFDFDFDFFFFFF5757570000
                00FFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3A3A3A
                000000515151C1C1C1EEEEEEF6F6F6DDDDDD8C8C8C000000000000BCBCBCFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFDADADACACACAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000001A1A1AFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC6C6C6000000ACACACFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDDDDDDC6C6C6FFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE1E1E169
                6969161616000000000000000000333333999999FFFFFFFFFFFFFEFEFEFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6464646C6C6CFFFFFFFFFFFFFDFDFDFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDF545454CECECEFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFEFEFEFEFEFE
                FEFEFEFDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFEFFFFFEFFFFFAFEFFFEFEFFFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFF
                FEFCFFFEFEFFFEFDFFFEFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFEFFFFFDFFFFFFFFFEFFFFFEFFFCFFFEFEFFFEFFFFFEFF
                FEFFFFFEFFFCFFFFFDFFFFFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFE
                FFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFF
                FEFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFAFEFF
                FFFDFFFEFEFFFDFFFFFEFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                F9FFFFFAFFFEFCFEFEFDFFFCFEFFFEFEFEFFFDFEFFFEFEFFFDFEFFFCFFFFFFFE
                FFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFE
                FEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFEFEFFFFFFFF
                FEFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFCFFFE
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFEFCFDFBFFFFFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFEFEFC
                FEFFFEFEFFFDFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFDFEFDFFFFFB
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F9F765B994
                71C19F71C39C71C29C71C29C71C39C71C29C71C29C70C39C65C19782BCA2FFFF
                FFFFFFFFFFFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFBFFFEE8F3EECDE8DAB5E0CD9FD4C18DCFB280C9A671C19E70C29D71
                C39C71C39C71C29C71C39C71C29C71C29C71C39C71C29C71C39C71C29C71C29C
                71C39C71C29C71C39C71C39C71C29C6EC29A7DC2A4FFFFFFFDFFFFFEFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFECF7F10EA15A28AA6A26
                AA6B27A96B24AA6B25AA6B26AA6B28A96B1EA96829A46BEEFDF5FFFFFFFBFFFF
                FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFEFFFFFBFFFEFEFDFEFFFEFEFEFFFEFFFDFEFFFFFEFDFEFFFDFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFE0F3EBB8E0CE8BD1B06BC1984AB08132AA70
                23A1641EA3641EA66422A86523A86526A86728AA692AAA6928AA6B26AA6B26AA
                6B26A96B26AA6B26AA6B26AA6B26AA6B26A96B26AA6B26AA6B26AA6B26AA6B26
                A96B26AA6B26AA6B26A96B22A9683AAA76FFFFFFFEFFFEFEFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7132AE7232AE
                7230AE7233AE7230AD712AAF7027A16AEAF9F3FFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFEFFFFFEFFFEFEFFFEFFFCFEFFFFFFFFFFFFFFFFFFFFFFE9F4F4B2DC
                CC7CC5A349B37F27A5691FA26220A86225A96828AB6C2BAC6E2FAD7030AE7131
                AE7131AD7231AD7231AE7231AE7231AD7231AE7231AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7231AE7233AD72
                33AD7131AE7219A362BFE4D8FFFFFFFEFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFA
                FEFEFEFEFFFFFFFFFFFFFFF7FFFABBDFCF6DC39E38A7741DA46422A96224AB69
                28AD6F2CAE702CAD712BAD7331AD7131AD7132AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE7230AD722EAE712E
                A96B71C199FFFFFFFAFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4FF
                FCABD8C555B78823A1671AA7662CA96B31AC7034AC7231AE7232AE7132AE7032
                AD7233AD7234AD7132AE7132AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722C
                AC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE7230AD7229AE7025A267FCFF
                FFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFFFFFEFEFFFC
                FFFFFFFEFEFEFFFEFFFEFDFDFEFEFFFFFFFFFFFFCDEADD68BE901F9F641BA767
                2AAB6D31AE7130AD7230AE7231AE7131AE7133AD7132AD7231AD7232AD7131AF
                702FAF7031AD7132AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE
                7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFEDF8F31AA36533AE7131AE7231AD7234AD7125AA668CCAB1FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFFFEFFFFFFFF
                FEFDFFFEFFFFFFFFFFFFB5DECA3EAA7922A3602AA96D2BAF7236AC7331AE7131
                AE7132AE7132AD7132AE7231AE7231AE7231AE7232AE7131AE7232AD7232AD72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFF
                FEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8
                F31AA36533AE7131AE7232AD722FAE711BA260F2FBFBFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFFFFFDFFFFFFFDFEFFFFFFFFFFFF
                BEE1D339AA751DA6642EAC6F33AC7230AE732BAD722EAD7332AE7132AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFC
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA365
                33AE7131AE7231AE712BAB6E52B587FFFFFFFFFDFEFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFE3F7F757B2891BA66024
                AE6B31AF6F35AE6F30AE7131AD7332AC7331AD7332AE7132AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131
                AE7231AD7125A96995D3B5FFFFFFFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FEFFFFFAFEFFFEFEFFFFFBFEFFFFFFA7DAC522A16328AB6C32AE7131AD7232AE
                7131AE7232AD7231AE7132AE7132AD7131AE7132AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AD
                711EA766C4E5D4FFFFFFFFFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFFFFFF
                FEFFFEFFFFFFFFFF7AC0A316A45E2AAF6E32AD7231AD7232AE7231AD7232AD72
                32AE7132AE7131AE7131AE7132AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE711AA564
                E1F0EAFFFFFFFFFEFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFF
                FF68BD951CA66332AE7133AE6F31AD7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE7119A463E6F4F1FF
                FFFFFEFFFDFFFFFFFFFFFFFEFFFFFEFFFFFEFFFFFCFFFFFFFFFF84C8AC18A663
                35AE712EAE722FAF7032AD7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722C
                AC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFEDF8F31AA36533AE7131AE7231AD721CA661E1F0EEFFFFFFFEFF
                FCFFFFFFFFFFFFFDFFFFFCFFFFFCFFFFFFFFFFCDE9D915A15F31AE7132AD7232
                AD7132AD7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE
                7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFEDF8F31AA36533AE7131AE7232AD721DA863C8E3D7FFFFFFFDFFFDFFFFFF
                FFFFFFFFFFFFFDFFFFFFFEFFFFFFFF3BAB772AAC6F33AF6D31AD7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFF
                FEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8
                F31AA36533AE7131AE7232AD7223A96897D3B7FFFFFFFEFFFEFFFFFFFFFFFFFD
                FFFFF9FFFFFFFFFFCAE9DF1BA66032AD7233AE6F31AD7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFC
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA365
                33AE7131AE7231AE712CAC6E53B587FFFFFFFEFEFEFEFFFFFEFFFFFDFFFEFFFF
                FEFFFFFF80C8AC28A96932AE7137AB7431AE7132AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131
                AE7231AE7132AD721BA161F1F9F7FFFDFFFEFFFFFBFFFFFCFFFEFFFFFDFFFFFF
                61BC9029AB6C31AE7231AE7132AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE
                7231AD7024A96B7ACAA3FFFFFFFFFFFFFCFFFFFBFDFFFBFFFFFFFFFF5ABA8E2A
                AB6D32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE7132AD72
                2FAD721AA264E9F9F0FFFFFFFFFEFFFEFDFFFCFFFEFFFFFF75C49D27AA6B32AE
                7131AE7132AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE7132AD7230AD722C
                AA6D54B084FFFFFFFFFDFFFFFDFFFFFFFCFFFFFFABD9C21EA8682EAD7232AD72
                31AE7132AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722C
                AC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFEDF8F31AA36533AE7131AE7232AE7232AE7132AE7131AE7120A7
                6880C7A7FFFFFFFFFFFCFFFEFEFFFEFFF7FFFD1C9F6534AE6E31AD7232AE7132
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE
                7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFEDF8F31AA36533AE7131AE7232AE7232AE7232AE7231AE7132AE711CA165
                98D2B6FFFFFFFCFEFFFEFDFFFFFFFF7FC5A61EAC6432AF7132AD7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFF
                FEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8
                F31AA36533AE7131AE7232AE7232AE7232AE7232AE712FAC742EAE7116A4638E
                CDB2FFFFFFFFFFFFFFFFFDFEFFFF34A47128AB6E32AE7131AE7132AD7232AD72
                31AE7132AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFC
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA365
                33AE7131AE7232AE7232AE7232AE7232AE7131AD7234AD7231AD721BA6606ABD
                97FFFFFFFFFFFFFFFFFFE8F8EE23A26A27AC7035AB7431AF7031AE7034AD7231
                AE7132AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F31AA36533AE7131
                AE7232AE7232AE7232AE7232AE7232AE7131AE7132AE7130AD7125A9663AA977
                D4EEE6FFFFFFFFFFFFE7F7EF36A5741DAC6536AC7233AC7431AD7232AE7131AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE72
                32AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE
                7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232AE7232
                AE7232AE7232AE7232AE722CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDF8F319A36433AE7131AE7131AE
                7131AE7131AE7131AE7131AE7131AE7131AD7231AF7038AC7429AB7116A45D7A
                C3A0FDFFFFFFFFFFFFFFFF69BC9612A2622BAE6C33AF6C2EAD7332AE7131AE71
                31AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE
                7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131
                AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE71
                31AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE7131AE
                7131AE7131AE712CAC6F43AE7BFFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFEAF6F1069D5820A6651EA6651EA6651EA665
                1EA6651EA6651EA6651EA6651EA66522A5651CA76424A66326A66213A25E0F9C
                5883C9A8F3FCF6FFFFFFC8EADD45AB7A15A65F1FA6661EA6651EA6651EA6651E
                A6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA665
                1EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6
                651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651E
                A6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA6651EA665
                1EA6651AA56234A770FFFFFFFEFFFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFF9FDFDBEE1D5C3E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5
                E4DAC5E4DAC5E4DAC5E4DAC6E4DAC6E4D9C5E4DAC5E4DAC5E3DBC5E5D9B6E0D1
                BDD9D3FDFEFFFFFFFFFFFFFFCDE7DFC0E3D8C6E4DAC5E4DAC5E4DAC5E4DAC5E4
                DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5
                E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DA
                C5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4
                DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5E4DAC5
                E3D9CAE4DCFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFDFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFDFEFEFCFEFEFEFFFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE
                FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFBFFFFFEFFFFFFFF
                FFFEFFFFFCFFFFFCFEFFFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE
                FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE
                FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE
                FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE
                FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFDFF
                FFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFD
                FFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFF
                FDFFFFFCFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFF
                FFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFD
                FFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFF
                FDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFF
                FFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFDFFFFFEFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000}
              mmHeight = 21960
              mmLeft = 4763
              mmTop = 0
              mmWidth = 53711
              BandType = 1
            end
            object ppLabel39: TppLabel
              UserName = 'Label39'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'General Prieto 1430. Independencia'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 8
              Font.Style = []
              Transparent = True
              mmHeight = 3260
              mmLeft = 7938
              mmTop = 18785
              mmWidth = 45466
              BandType = 1
            end
            object ppLabel36: TppLabel
              UserName = 'Label302'
              HyperlinkColor = clBlue
              Border.BorderPositions = []
              Border.Color = clBlack
              Border.Style = psSolid
              Border.Visible = False
              Border.Weight = 1.000000000000000000
              Caption = 'Liquidaci'#243'n de Telev'#237'as'
              Ellipsis = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Name = 'Arial'
              Font.Size = 13
              Font.Style = [fsBold]
              TextAlignment = taCentered
              Transparent = True
              mmHeight = 5419
              mmLeft = 78250
              mmTop = 10054
              mmWidth = 51477
              BandType = 1
            end
          end
          object ppDetailBand12: TppDetailBand
            Background1.Brush.Style = bsClear
            Background1.Gradient.EndColor = clWhite
            Background1.Gradient.StartColor = clWhite
            Background1.Gradient.Style = gsNone
            Background2.Brush.Style = bsClear
            Background2.Gradient.EndColor = clWhite
            Background2.Gradient.StartColor = clWhite
            Background2.Gradient.Style = gsNone
            mmBottomOffset = 0
            mmHeight = 0
            mmPrintPosition = 0
          end
          object ppSummaryBand10: TppSummaryBand
            AlignToBottom = False
            mmBottomOffset = 0
            mmHeight = 3704
            mmPrintPosition = 0
          end
          object ppGroup3: TppGroup
            BreakName = 'ppLabel30'
            BreakType = btCustomField
            GroupFileSettings.NewFile = False
            GroupFileSettings.EmailFile = False
            KeepTogether = True
            OutlineSettings.CreateNode = True
            StartOnOddPage = False
            UserName = 'Group3'
            mmNewColumnThreshold = 0
            mmNewPageThreshold = 0
            DataPipelineName = ''
            NewFile = False
            object ppGroupHeaderBand3: TppGroupHeaderBand
              PrintHeight = phDynamic
              mmBottomOffset = 0
              mmHeight = 63500
              mmPrintPosition = 0
              object ppSubReport6: TppSubReport
                UserName = 'SubReport6'
                ExpandAll = False
                NewPrintJob = False
                OutlineSettings.CreateNode = True
                TraverseAllData = False
                DataPipelineName = 'DBPCalculadoSaldoInicial'
                mmHeight = 26988
                mmLeft = 0
                mmTop = 794
                mmWidth = 197300
                BandType = 3
                GroupNo = 0
                mmBottomOffset = 0
                mmOverFlowOffset = 0
                mmStopPosition = 0
                mmMinHeight = 0
                object ppChildReport12: TppChildReport
                  AutoStop = False
                  DataPipeline = DBPCalculadoSaldoInicial
                  PrinterSetup.BinName = 'Default'
                  PrinterSetup.Copies = 2
                  PrinterSetup.DocumentName = 'Report'
                  PrinterSetup.PaperName = 'A4'
                  PrinterSetup.PrinterName = 'Default'
                  PrinterSetup.SaveDeviceSettings = False
                  PrinterSetup.mmMarginBottom = 6350
                  PrinterSetup.mmMarginLeft = 6350
                  PrinterSetup.mmMarginRight = 6350
                  PrinterSetup.mmMarginTop = 6350
                  PrinterSetup.mmPaperHeight = 297000
                  PrinterSetup.mmPaperWidth = 210000
                  PrinterSetup.PaperSize = 9
                  Version = '12.04'
                  mmColumnWidth = 0
                  DataPipelineName = 'DBPCalculadoSaldoInicial'
                  object ppTitleBand11: TppTitleBand
                    mmBottomOffset = 0
                    mmHeight = 6350
                    mmPrintPosition = 0
                    object ppShape11: TppShape
                      UserName = 'Shape11'
                      Brush.Color = 16444382
                      Gradient.EndColor = clWhite
                      Gradient.StartColor = clWhite
                      Gradient.Style = gsNone
                      ParentWidth = True
                      Pen.Style = psClear
                      mmHeight = 5292
                      mmLeft = 0
                      mmTop = 1058
                      mmWidth = 197300
                      BandType = 1
                    end
                    object ppLabel43: TppLabel
                      UserName = 'Label2'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      Caption = 'Apertura - Vale:'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 12
                      Font.Style = [fsBold]
                      Transparent = True
                      mmHeight = 4995
                      mmLeft = 3970
                      mmTop = 1323
                      mmWidth = 31496
                      BandType = 1
                    end
                    object ppLabel44: TppLabel
                      UserName = 'Label3'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      Caption = 'Cantidad de Telev'#237'as '
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 12
                      Font.Style = [fsBold]
                      TextAlignment = taCentered
                      Transparent = True
                      mmHeight = 5027
                      mmLeft = 102923
                      mmTop = 1323
                      mmWidth = 43392
                      BandType = 1
                    end
                    object ppLine23: TppLine
                      UserName = 'Line4'
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      ParentWidth = True
                      Position = lpBottom
                      Weight = 0.750000000000000000
                      mmHeight = 1323
                      mmLeft = 0
                      mmTop = 5027
                      mmWidth = 197300
                      BandType = 1
                    end
                    object ppDBText32: TppDBText
                      UserName = 'DBText20'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      DataField = 'NumeroVale'
                      DataPipeline = DBPCalculadoSaldoInicial
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 12
                      Font.Style = [fsBold]
                      ParentDataPipeline = False
                      Transparent = True
                      DataPipelineName = 'DBPCalculadoSaldoInicial'
                      mmHeight = 5027
                      mmLeft = 36513
                      mmTop = 1323
                      mmWidth = 17198
                      BandType = 1
                    end
                  end
                  object ppDetailBand13: TppDetailBand
                    Background1.Brush.Style = bsClear
                    Background1.Gradient.EndColor = clWhite
                    Background1.Gradient.StartColor = clWhite
                    Background1.Gradient.Style = gsNone
                    Background2.Brush.Style = bsClear
                    Background2.Gradient.EndColor = clWhite
                    Background2.Gradient.StartColor = clWhite
                    Background2.Gradient.Style = gsNone
                    mmBottomOffset = 0
                    mmHeight = 5292
                    mmPrintPosition = 0
                    object ppDBText33: TppDBText
                      UserName = 'DBText3'
                      HyperlinkColor = clBlue
                      AutoSize = True
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      DataField = 'Categoria'
                      DataPipeline = DBPCalculadoSaldoInicial
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = []
                      Transparent = True
                      DataPipelineName = 'DBPCalculadoSaldoInicial'
                      mmHeight = 4022
                      mmLeft = 3969
                      mmTop = 794
                      mmWidth = 15198
                      BandType = 4
                    end
                    object ppDBText34: TppDBText
                      UserName = 'DBText4'
                      HyperlinkColor = clBlue
                      AutoSize = True
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      DataField = 'Cantidad Inicial'
                      DataPipeline = DBPCalculadoSaldoInicial
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = []
                      TextAlignment = taCentered
                      Transparent = True
                      DataPipelineName = 'DBPCalculadoSaldoInicial'
                      mmHeight = 4022
                      mmLeft = 112623
                      mmTop = 794
                      mmWidth = 23918
                      BandType = 4
                    end
                  end
                  object ppSummaryBand11: TppSummaryBand
                    AlignToBottom = False
                    mmBottomOffset = 0
                    mmHeight = 5027
                    mmPrintPosition = 0
                    object ppLine24: TppLine
                      UserName = 'Line13'
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      ParentWidth = True
                      Weight = 0.750000000000000000
                      mmHeight = 3969
                      mmLeft = 0
                      mmTop = 0
                      mmWidth = 197300
                      BandType = 7
                    end
                    object ppLabel45: TppLabel
                      UserName = 'Label22'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      Caption = 'Total'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = []
                      TextAlignment = taCentered
                      Transparent = True
                      mmHeight = 3969
                      mmLeft = 68263
                      mmTop = 529
                      mmWidth = 7938
                      BandType = 7
                    end
                    object ppDBCalc9: TppDBCalc
                      UserName = 'DBCalc3'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      DataField = 'Cantidad Inicial'
                      DataPipeline = DBPCalculadoSaldoInicial
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = []
                      TextAlignment = taCentered
                      Transparent = True
                      DataPipelineName = 'DBPCalculadoSaldoInicial'
                      mmHeight = 3969
                      mmLeft = 116152
                      mmTop = 529
                      mmWidth = 17198
                      BandType = 7
                    end
                  end
                end
              end
              object ppSubReport7: TppSubReport
                UserName = 'srCalculadoSaldoInicial2'
                ExpandAll = False
                NewPrintJob = False
                OutlineSettings.CreateNode = True
                TraverseAllData = False
                DataPipelineName = 'dbpSaldosAdicionales'
                mmHeight = 33073
                mmLeft = 0
                mmTop = 29104
                mmWidth = 197300
                BandType = 3
                GroupNo = 0
                mmBottomOffset = 0
                mmOverFlowOffset = 0
                mmStopPosition = 0
                mmMinHeight = 0
                object ppChildReport13: TppChildReport
                  AutoStop = False
                  DataPipeline = dbpSaldosAdicionales
                  PrinterSetup.BinName = 'Default'
                  PrinterSetup.Copies = 2
                  PrinterSetup.DocumentName = 'Report'
                  PrinterSetup.PaperName = 'A4'
                  PrinterSetup.PrinterName = 'Default'
                  PrinterSetup.SaveDeviceSettings = False
                  PrinterSetup.mmMarginBottom = 6350
                  PrinterSetup.mmMarginLeft = 6350
                  PrinterSetup.mmMarginRight = 6350
                  PrinterSetup.mmMarginTop = 6350
                  PrinterSetup.mmPaperHeight = 297000
                  PrinterSetup.mmPaperWidth = 210000
                  PrinterSetup.PaperSize = 9
                  Version = '12.04'
                  mmColumnWidth = 0
                  DataPipelineName = 'dbpSaldosAdicionales'
                  object ppTitleBand12: TppTitleBand
                    mmBottomOffset = 0
                    mmHeight = 0
                    mmPrintPosition = 0
                  end
                  object ppDetailBand14: TppDetailBand
                    Background1.Brush.Style = bsClear
                    Background1.Gradient.EndColor = clWhite
                    Background1.Gradient.StartColor = clWhite
                    Background1.Gradient.Style = gsNone
                    Background2.Brush.Style = bsClear
                    Background2.Gradient.EndColor = clWhite
                    Background2.Gradient.StartColor = clWhite
                    Background2.Gradient.Style = gsNone
                    PrintHeight = phDynamic
                    mmBottomOffset = 0
                    mmHeight = 5027
                    mmPrintPosition = 0
                    object ppDBText35: TppDBText
                      UserName = 'DBText1'
                      HyperlinkColor = clBlue
                      AutoSize = True
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      DataField = 'Categoria'
                      DataPipeline = dbpSaldosAdicionales
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = []
                      Transparent = True
                      DataPipelineName = 'dbpSaldosAdicionales'
                      mmHeight = 4022
                      mmLeft = 6085
                      mmTop = 529
                      mmWidth = 15198
                      BandType = 4
                    end
                    object ppDBText36: TppDBText
                      UserName = 'DBText2'
                      HyperlinkColor = clBlue
                      AutoSize = True
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      DataField = 'Cantidad Inicial'
                      DataPipeline = dbpSaldosAdicionales
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = []
                      ParentDataPipeline = False
                      TextAlignment = taCentered
                      Transparent = True
                      DataPipelineName = 'dbpSaldosAdicionales'
                      mmHeight = 4022
                      mmLeft = 114512
                      mmTop = 794
                      mmWidth = 23918
                      BandType = 4
                    end
                  end
                  object ppSummaryBand12: TppSummaryBand
                    AlignToBottom = False
                    mmBottomOffset = 0
                    mmHeight = 0
                    mmPrintPosition = 0
                  end
                  object ppGroup4: TppGroup
                    BreakName = 'NumeroVale'
                    DataPipeline = dbpSaldosAdicionales
                    GroupFileSettings.NewFile = False
                    GroupFileSettings.EmailFile = False
                    KeepTogether = True
                    OutlineSettings.CreateNode = True
                    StartOnOddPage = False
                    UserName = 'Group2'
                    mmNewColumnThreshold = 0
                    mmNewPageThreshold = 0
                    DataPipelineName = 'dbpSaldosAdicionales'
                    NewFile = False
                    object ppGroupHeaderBand4: TppGroupHeaderBand
                      mmBottomOffset = 0
                      mmHeight = 6350
                      mmPrintPosition = 0
                      object ppShape12: TppShape
                        UserName = 'Shape12'
                        Brush.Color = 16444382
                        Gradient.EndColor = clWhite
                        Gradient.StartColor = clWhite
                        Gradient.Style = gsNone
                        ParentWidth = True
                        Pen.Style = psClear
                        mmHeight = 5821
                        mmLeft = 0
                        mmTop = 529
                        mmWidth = 197300
                        BandType = 3
                        GroupNo = 0
                      end
                      object ppLabel46: TppLabel
                        UserName = 'Label1'
                        HyperlinkColor = clBlue
                        Border.BorderPositions = []
                        Border.Color = clBlack
                        Border.Style = psSolid
                        Border.Visible = False
                        Border.Weight = 1.000000000000000000
                        Caption = 'Reabastecimiento - Vale:'
                        Ellipsis = False
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clBlack
                        Font.Name = 'Arial'
                        Font.Size = 12
                        Font.Style = [fsBold]
                        Transparent = True
                        mmHeight = 5027
                        mmLeft = 5821
                        mmTop = 265
                        mmWidth = 50271
                        BandType = 3
                        GroupNo = 0
                      end
                      object ppDBText37: TppDBText
                        UserName = 'DBText18'
                        HyperlinkColor = clBlue
                        Border.BorderPositions = []
                        Border.Color = clBlack
                        Border.Style = psSolid
                        Border.Visible = False
                        Border.Weight = 1.000000000000000000
                        DataField = 'NumeroVale'
                        DataPipeline = dbpSaldosAdicionales
                        Ellipsis = False
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clBlack
                        Font.Name = 'Arial'
                        Font.Size = 12
                        Font.Style = [fsBold]
                        Transparent = True
                        DataPipelineName = 'dbpSaldosAdicionales'
                        mmHeight = 5027
                        mmLeft = 57944
                        mmTop = 265
                        mmWidth = 17198
                        BandType = 3
                        GroupNo = 0
                      end
                      object ppLine25: TppLine
                        UserName = 'Line1'
                        Border.BorderPositions = []
                        Border.Color = clBlack
                        Border.Style = psSolid
                        Border.Visible = False
                        Border.Weight = 1.000000000000000000
                        ParentWidth = True
                        Position = lpBottom
                        Weight = 0.750000000000000000
                        mmHeight = 1323
                        mmLeft = 0
                        mmTop = 4763
                        mmWidth = 197300
                        BandType = 3
                        GroupNo = 0
                      end
                      object ppLabel47: TppLabel
                        UserName = 'Label2'
                        HyperlinkColor = clBlue
                        Border.BorderPositions = []
                        Border.Color = clBlack
                        Border.Style = psSolid
                        Border.Visible = False
                        Border.Weight = 1.000000000000000000
                        Caption = 'Cantidad de Telev'#237'as'
                        Ellipsis = False
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clBlack
                        Font.Name = 'Arial'
                        Font.Size = 12
                        Font.Style = [fsBold]
                        TextAlignment = taCentered
                        Transparent = True
                        mmHeight = 5027
                        mmLeft = 105040
                        mmTop = 529
                        mmWidth = 42863
                        BandType = 3
                        GroupNo = 0
                      end
                    end
                    object ppGroupFooterBand4: TppGroupFooterBand
                      HideWhenOneDetail = False
                      mmBottomOffset = 0
                      mmHeight = 5027
                      mmPrintPosition = 0
                      object ppLine26: TppLine
                        UserName = 'Line15'
                        Border.BorderPositions = []
                        Border.Color = clBlack
                        Border.Style = psSolid
                        Border.Visible = False
                        Border.Weight = 1.000000000000000000
                        ParentWidth = True
                        Weight = 0.750000000000000000
                        mmHeight = 3969
                        mmLeft = 0
                        mmTop = 529
                        mmWidth = 197300
                        BandType = 5
                        GroupNo = 0
                      end
                      object ppLabel48: TppLabel
                        UserName = 'Label20'
                        HyperlinkColor = clBlue
                        Border.BorderPositions = []
                        Border.Color = clBlack
                        Border.Style = psSolid
                        Border.Visible = False
                        Border.Weight = 1.000000000000000000
                        Caption = 'Total'
                        Ellipsis = False
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clBlack
                        Font.Name = 'Arial'
                        Font.Size = 10
                        Font.Style = []
                        Transparent = True
                        mmHeight = 3969
                        mmLeft = 70115
                        mmTop = 1058
                        mmWidth = 7938
                        BandType = 5
                        GroupNo = 0
                      end
                      object ppDBCalc10: TppDBCalc
                        UserName = 'DBCalc1'
                        HyperlinkColor = clBlue
                        Border.BorderPositions = []
                        Border.Color = clBlack
                        Border.Style = psSolid
                        Border.Visible = False
                        Border.Weight = 1.000000000000000000
                        DataField = 'Cantidad Inicial'
                        DataPipeline = dbpSaldosAdicionales
                        Ellipsis = False
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clBlack
                        Font.Name = 'Arial'
                        Font.Size = 10
                        Font.Style = []
                        TextAlignment = taCentered
                        Transparent = True
                        DataPipelineName = 'dbpSaldosAdicionales'
                        mmHeight = 3969
                        mmLeft = 114036
                        mmTop = 1058
                        mmWidth = 24871
                        BandType = 5
                        GroupNo = 0
                      end
                    end
                  end
                end
              end
            end
            object ppGroupFooterBand3: TppGroupFooterBand
              PrintHeight = phDynamic
              HideWhenOneDetail = False
              mmBottomOffset = 0
              mmHeight = 182298
              mmPrintPosition = 0
              object ppSubReport8: TppSubReport
                UserName = 'srInformadoSaldoFinal1'
                ExpandAll = False
                NewPrintJob = False
                OutlineSettings.CreateNode = True
                TraverseAllData = False
                DataPipelineName = 'DBPInformadoSaldoFinal'
                mmHeight = 26723
                mmLeft = 0
                mmTop = 529
                mmWidth = 197300
                BandType = 5
                GroupNo = 0
                mmBottomOffset = 0
                mmOverFlowOffset = 0
                mmStopPosition = 0
                mmMinHeight = 0
                object ppChildReport14: TppChildReport
                  AutoStop = False
                  DataPipeline = DBPInformadoSaldoFinal
                  PrinterSetup.BinName = 'Default'
                  PrinterSetup.Copies = 2
                  PrinterSetup.DocumentName = 'Report'
                  PrinterSetup.PaperName = 'A4'
                  PrinterSetup.PrinterName = 'Default'
                  PrinterSetup.SaveDeviceSettings = False
                  PrinterSetup.mmMarginBottom = 6350
                  PrinterSetup.mmMarginLeft = 6350
                  PrinterSetup.mmMarginRight = 6350
                  PrinterSetup.mmMarginTop = 6350
                  PrinterSetup.mmPaperHeight = 297000
                  PrinterSetup.mmPaperWidth = 210000
                  PrinterSetup.PaperSize = 9
                  Version = '12.04'
                  mmColumnWidth = 0
                  DataPipelineName = 'DBPInformadoSaldoFinal'
                  object ppTitleBand13: TppTitleBand
                    mmBottomOffset = 0
                    mmHeight = 6350
                    mmPrintPosition = 0
                    object ppShape10: TppShape
                      UserName = 'Shape10'
                      Brush.Color = 16444382
                      Gradient.EndColor = clWhite
                      Gradient.StartColor = clWhite
                      Gradient.Style = gsNone
                      ParentWidth = True
                      Pen.Style = psClear
                      mmHeight = 5292
                      mmLeft = 0
                      mmTop = 1058
                      mmWidth = 197300
                      BandType = 1
                    end
                    object ppLabel49: TppLabel
                      UserName = 'Label7'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      Caption = 'Cierre de Turno - Vale:'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 12
                      Font.Style = [fsBold]
                      Transparent = True
                      mmHeight = 4995
                      mmLeft = 3970
                      mmTop = 795
                      mmWidth = 45551
                      BandType = 1
                    end
                    object ppLabel50: TppLabel
                      UserName = 'Label8'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      Caption = 'Cantidad de Telev'#237'as'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 12
                      Font.Style = [fsBold]
                      TextAlignment = taCentered
                      Transparent = True
                      mmHeight = 5027
                      mmLeft = 103452
                      mmTop = 794
                      mmWidth = 42333
                      BandType = 1
                    end
                    object ppLine27: TppLine
                      UserName = 'Line3'
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      ParentWidth = True
                      Position = lpBottom
                      Weight = 0.750000000000000000
                      mmHeight = 1323
                      mmLeft = 0
                      mmTop = 5027
                      mmWidth = 197300
                      BandType = 1
                    end
                    object ppDBText38: TppDBText
                      UserName = 'DBText19'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      DataField = 'NumeroVale'
                      DataPipeline = DBPInformadoSaldoFinal
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 12
                      Font.Style = [fsBold]
                      Transparent = True
                      DataPipelineName = 'DBPInformadoSaldoFinal'
                      mmHeight = 5027
                      mmLeft = 51065
                      mmTop = 794
                      mmWidth = 17198
                      BandType = 1
                    end
                  end
                  object ppDetailBand15: TppDetailBand
                    Background1.Brush.Style = bsClear
                    Background1.Gradient.EndColor = clWhite
                    Background1.Gradient.StartColor = clWhite
                    Background1.Gradient.Style = gsNone
                    Background2.Brush.Style = bsClear
                    Background2.Gradient.EndColor = clWhite
                    Background2.Gradient.StartColor = clWhite
                    Background2.Gradient.Style = gsNone
                    mmBottomOffset = 0
                    mmHeight = 5292
                    mmPrintPosition = 0
                    object ppDBText39: TppDBText
                      UserName = 'DBText1'
                      HyperlinkColor = clBlue
                      AutoSize = True
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      DataField = 'Categoria'
                      DataPipeline = DBPInformadoSaldoFinal
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = []
                      Transparent = True
                      DataPipelineName = 'DBPInformadoSaldoFinal'
                      mmHeight = 4022
                      mmLeft = 3969
                      mmTop = 794
                      mmWidth = 15198
                      BandType = 4
                    end
                    object ppDBText40: TppDBText
                      UserName = 'DBText2'
                      HyperlinkColor = clBlue
                      AutoSize = True
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      DataField = 'Cantidad Final'
                      DataPipeline = DBPInformadoSaldoFinal
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = []
                      TextAlignment = taCentered
                      Transparent = True
                      DataPipelineName = 'DBPInformadoSaldoFinal'
                      mmHeight = 4022
                      mmLeft = 113337
                      mmTop = 794
                      mmWidth = 22564
                      BandType = 4
                    end
                  end
                  object ppSummaryBand13: TppSummaryBand
                    AlignToBottom = False
                    mmBottomOffset = 0
                    mmHeight = 4498
                    mmPrintPosition = 0
                    object ppLine28: TppLine
                      UserName = 'Line14'
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      ParentWidth = True
                      Weight = 0.750000000000000000
                      mmHeight = 3969
                      mmLeft = 0
                      mmTop = 0
                      mmWidth = 197300
                      BandType = 7
                    end
                    object ppLabel51: TppLabel
                      UserName = 'Label21'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      Caption = 'Total'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = []
                      Transparent = True
                      mmHeight = 3969
                      mmLeft = 68263
                      mmTop = 529
                      mmWidth = 7938
                      BandType = 7
                    end
                    object ppDBCalc11: TppDBCalc
                      UserName = 'DBCalc2'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      DataField = 'Cantidad Final'
                      DataPipeline = DBPInformadoSaldoFinal
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = []
                      TextAlignment = taCentered
                      Transparent = True
                      DataPipelineName = 'DBPInformadoSaldoFinal'
                      mmHeight = 3969
                      mmLeft = 116152
                      mmTop = 529
                      mmWidth = 17198
                      BandType = 7
                    end
                  end
                  object raCodeModule2: TraCodeModule
                    ProgramStream = {00}
                  end
                end
              end
              object ppSubReport9: TppSubReport
                UserName = 'srTotalesConvenios1'
                ExpandAll = False
                NewPrintJob = False
                OutlineSettings.CreateNode = True
                TraverseAllData = False
                DataPipelineName = 'DBPTotalesConvenios'
                mmHeight = 10319
                mmLeft = 0
                mmTop = 28046
                mmWidth = 197300
                BandType = 5
                GroupNo = 0
                mmBottomOffset = 0
                mmOverFlowOffset = 0
                mmStopPosition = 0
                mmMinHeight = 0
                object ppChildReport15: TppChildReport
                  AutoStop = False
                  DataPipeline = DBPTotalesConvenios
                  PrinterSetup.BinName = 'Default'
                  PrinterSetup.Copies = 2
                  PrinterSetup.DocumentName = 'Report'
                  PrinterSetup.PaperName = 'A4'
                  PrinterSetup.PrinterName = 'Default'
                  PrinterSetup.SaveDeviceSettings = False
                  PrinterSetup.mmMarginBottom = 6350
                  PrinterSetup.mmMarginLeft = 6350
                  PrinterSetup.mmMarginRight = 6350
                  PrinterSetup.mmMarginTop = 6350
                  PrinterSetup.mmPaperHeight = 297000
                  PrinterSetup.mmPaperWidth = 210000
                  PrinterSetup.PaperSize = 9
                  Version = '12.04'
                  mmColumnWidth = 0
                  DataPipelineName = 'DBPTotalesConvenios'
                  object ppTitleBand14: TppTitleBand
                    mmBottomOffset = 0
                    mmHeight = 6615
                    mmPrintPosition = 0
                    object ppShape9: TppShape
                      UserName = 'Shape9'
                      Brush.Color = 16444382
                      Gradient.EndColor = clWhite
                      Gradient.StartColor = clWhite
                      Gradient.Style = gsNone
                      ParentWidth = True
                      Pen.Style = psClear
                      mmHeight = 5292
                      mmLeft = 0
                      mmTop = 1323
                      mmWidth = 197300
                      BandType = 1
                    end
                    object ppLabel52: TppLabel
                      UserName = 'Label3'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      Caption = 'Cantidad de Convenios Afectados'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 12
                      Font.Style = [fsBold]
                      Transparent = True
                      mmHeight = 4995
                      mmLeft = 3970
                      mmTop = 1323
                      mmWidth = 68580
                      BandType = 1
                    end
                    object ppDBText41: TppDBText
                      UserName = 'DBText4'
                      HyperlinkColor = clBlue
                      AutoSize = True
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      DataField = 'Total Convenios'
                      DataPipeline = DBPTotalesConvenios
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = []
                      ParentDataPipeline = False
                      TextAlignment = taCentered
                      Transparent = True
                      DataPipelineName = 'DBPTotalesConvenios'
                      mmHeight = 4022
                      mmLeft = 123608
                      mmTop = 2381
                      mmWidth = 1947
                      BandType = 1
                    end
                    object ppLine29: TppLine
                      UserName = 'Line7'
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      ParentWidth = True
                      Position = lpBottom
                      Weight = 0.750000000000000000
                      mmHeight = 1323
                      mmLeft = 0
                      mmTop = 5292
                      mmWidth = 197300
                      BandType = 1
                    end
                    object ppLine30: TppLine
                      UserName = 'Line9'
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      ParentWidth = True
                      Position = lpBottom
                      Weight = 0.750000000000000000
                      mmHeight = 1323
                      mmLeft = 0
                      mmTop = 0
                      mmWidth = 197300
                      BandType = 1
                    end
                  end
                  object ppDetailBand16: TppDetailBand
                    Background1.Brush.Style = bsClear
                    Background1.Gradient.EndColor = clWhite
                    Background1.Gradient.StartColor = clWhite
                    Background1.Gradient.Style = gsNone
                    Background2.Brush.Style = bsClear
                    Background2.Gradient.EndColor = clWhite
                    Background2.Gradient.StartColor = clWhite
                    Background2.Gradient.Style = gsNone
                    mmBottomOffset = 0
                    mmHeight = 0
                    mmPrintPosition = 0
                  end
                  object ppSummaryBand14: TppSummaryBand
                    AlignToBottom = False
                    mmBottomOffset = 0
                    mmHeight = 0
                    mmPrintPosition = 0
                  end
                end
              end
              object ppSubReport10: TppSubReport
                UserName = 'srCalculadoEntregado1'
                ExpandAll = False
                NewPrintJob = False
                OutlineSettings.CreateNode = True
                TraverseAllData = False
                DataPipelineName = 'DBPCalculadoEntregado'
                mmHeight = 33073
                mmLeft = 0
                mmTop = 39158
                mmWidth = 197300
                BandType = 5
                GroupNo = 0
                mmBottomOffset = 0
                mmOverFlowOffset = 0
                mmStopPosition = 0
                mmMinHeight = 0
                object ppChildReport16: TppChildReport
                  AutoStop = False
                  DataPipeline = DBPCalculadoEntregado
                  PrinterSetup.BinName = 'Default'
                  PrinterSetup.Copies = 2
                  PrinterSetup.DocumentName = 'Report'
                  PrinterSetup.PaperName = 'A4'
                  PrinterSetup.PrinterName = 'Default'
                  PrinterSetup.SaveDeviceSettings = False
                  PrinterSetup.mmMarginBottom = 6350
                  PrinterSetup.mmMarginLeft = 6350
                  PrinterSetup.mmMarginRight = 6350
                  PrinterSetup.mmMarginTop = 6350
                  PrinterSetup.mmPaperHeight = 297000
                  PrinterSetup.mmPaperWidth = 210000
                  PrinterSetup.PaperSize = 9
                  Version = '12.04'
                  mmColumnWidth = 0
                  DataPipelineName = 'DBPCalculadoEntregado'
                  object ppTitleBand15: TppTitleBand
                    mmBottomOffset = 0
                    mmHeight = 6350
                    mmPrintPosition = 0
                    object ppShape7: TppShape
                      UserName = 'Shape7'
                      Brush.Color = 16444382
                      Gradient.EndColor = clWhite
                      Gradient.StartColor = clWhite
                      Gradient.Style = gsNone
                      ParentWidth = True
                      Pen.Style = psClear
                      mmHeight = 5292
                      mmLeft = 0
                      mmTop = 1058
                      mmWidth = 197300
                      BandType = 1
                    end
                    object ppLabel53: TppLabel
                      UserName = 'Label2'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      Caption = 'Cantidad de Telev'#237'as Entregados por Convenio'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 12
                      Font.Style = [fsBold]
                      Transparent = True
                      mmHeight = 4995
                      mmLeft = 3970
                      mmTop = 1058
                      mmWidth = 95081
                      BandType = 1
                    end
                    object ppLine31: TppLine
                      UserName = 'Line5'
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      ParentWidth = True
                      Position = lpBottom
                      Weight = 0.750000000000000000
                      mmHeight = 1323
                      mmLeft = 0
                      mmTop = 5027
                      mmWidth = 197300
                      BandType = 1
                    end
                  end
                  object ppDetailBand17: TppDetailBand
                    Background1.Brush.Style = bsClear
                    Background1.Gradient.EndColor = clWhite
                    Background1.Gradient.StartColor = clWhite
                    Background1.Gradient.Style = gsNone
                    Background2.Brush.Style = bsClear
                    Background2.Gradient.EndColor = clWhite
                    Background2.Gradient.StartColor = clWhite
                    Background2.Gradient.Style = gsNone
                    mmBottomOffset = 0
                    mmHeight = 5292
                    mmPrintPosition = 0
                    object ppDBText42: TppDBText
                      UserName = 'DBText3'
                      HyperlinkColor = clBlue
                      AutoSize = True
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      DataField = 'Categoria'
                      DataPipeline = DBPCalculadoEntregado
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = []
                      Transparent = True
                      DataPipelineName = 'DBPCalculadoEntregado'
                      mmHeight = 4022
                      mmLeft = 3969
                      mmTop = 794
                      mmWidth = 66040
                      BandType = 4
                    end
                    object ppDBText43: TppDBText
                      UserName = 'DBText4'
                      HyperlinkColor = clBlue
                      AutoSize = True
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      DataField = 'Cantidad Entregados'
                      DataPipeline = DBPCalculadoEntregado
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = []
                      TextAlignment = taCentered
                      Transparent = True
                      DataPipelineName = 'DBPCalculadoEntregado'
                      mmHeight = 4022
                      mmLeft = 123608
                      mmTop = 794
                      mmWidth = 1947
                      BandType = 4
                    end
                  end
                  object ppSummaryBand15: TppSummaryBand
                    AlignToBottom = False
                    mmBottomOffset = 0
                    mmHeight = 5292
                    mmPrintPosition = 0
                    object ppLine32: TppLine
                      UserName = 'Line16'
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      ParentWidth = True
                      Weight = 0.750000000000000000
                      mmHeight = 3969
                      mmLeft = 0
                      mmTop = 0
                      mmWidth = 197300
                      BandType = 7
                    end
                    object ppLabel54: TppLabel
                      UserName = 'Label5'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      Caption = 'Total'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = []
                      Transparent = True
                      mmHeight = 3969
                      mmLeft = 68263
                      mmTop = 529
                      mmWidth = 7938
                      BandType = 7
                    end
                    object ppDBCalc12: TppDBCalc
                      UserName = 'DBCalc4'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      DataField = 'Cantidad Entregados'
                      DataPipeline = DBPCalculadoEntregado
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = []
                      TextAlignment = taCentered
                      Transparent = True
                      DataPipelineName = 'DBPCalculadoEntregado'
                      mmHeight = 3969
                      mmLeft = 116152
                      mmTop = 529
                      mmWidth = 17198
                      BandType = 7
                    end
                  end
                end
              end
              object ppSubReport11: TppSubReport
                UserName = 'srCalculadoDevueltos1'
                ExpandAll = False
                NewPrintJob = False
                OutlineSettings.CreateNode = True
                TraverseAllData = False
                DataPipelineName = 'DBCalculadoDevuelto'
                mmHeight = 33073
                mmLeft = 0
                mmTop = 73025
                mmWidth = 197300
                BandType = 5
                GroupNo = 0
                mmBottomOffset = 0
                mmOverFlowOffset = 0
                mmStopPosition = 0
                mmMinHeight = 0
                object ppChildReport17: TppChildReport
                  AutoStop = False
                  DataPipeline = DBCalculadoDevuelto
                  PrinterSetup.BinName = 'Default'
                  PrinterSetup.Copies = 2
                  PrinterSetup.DocumentName = 'Report'
                  PrinterSetup.PaperName = 'A4'
                  PrinterSetup.PrinterName = 'Default'
                  PrinterSetup.SaveDeviceSettings = False
                  PrinterSetup.mmMarginBottom = 6350
                  PrinterSetup.mmMarginLeft = 6350
                  PrinterSetup.mmMarginRight = 6350
                  PrinterSetup.mmMarginTop = 6350
                  PrinterSetup.mmPaperHeight = 297000
                  PrinterSetup.mmPaperWidth = 210000
                  PrinterSetup.PaperSize = 9
                  Version = '12.04'
                  mmColumnWidth = 0
                  DataPipelineName = 'DBCalculadoDevuelto'
                  object ppTitleBand16: TppTitleBand
                    mmBottomOffset = 0
                    mmHeight = 6350
                    mmPrintPosition = 0
                    object ppShape6: TppShape
                      UserName = 'Shape6'
                      Brush.Color = 16444382
                      Gradient.EndColor = clWhite
                      Gradient.StartColor = clWhite
                      Gradient.Style = gsNone
                      ParentWidth = True
                      Pen.Style = psClear
                      mmHeight = 6350
                      mmLeft = 0
                      mmTop = 0
                      mmWidth = 197300
                      BandType = 1
                    end
                    object ppLabel55: TppLabel
                      UserName = 'Label2'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      Caption = 'Cantidad de Telev'#237'as Devueltos por Convenio'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 12
                      Font.Style = [fsBold]
                      Transparent = True
                      mmHeight = 4995
                      mmLeft = 3970
                      mmTop = 1058
                      mmWidth = 91948
                      BandType = 1
                    end
                    object ppLine33: TppLine
                      UserName = 'Line5'
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      ParentWidth = True
                      Position = lpBottom
                      Weight = 0.750000000000000000
                      mmHeight = 1323
                      mmLeft = 0
                      mmTop = 5027
                      mmWidth = 197300
                      BandType = 1
                    end
                  end
                  object ppDetailBand18: TppDetailBand
                    Background1.Brush.Style = bsClear
                    Background1.Gradient.EndColor = clWhite
                    Background1.Gradient.StartColor = clWhite
                    Background1.Gradient.Style = gsNone
                    Background2.Brush.Style = bsClear
                    Background2.Gradient.EndColor = clWhite
                    Background2.Gradient.StartColor = clWhite
                    Background2.Gradient.Style = gsNone
                    mmBottomOffset = 0
                    mmHeight = 5292
                    mmPrintPosition = 0
                    object ppDBText44: TppDBText
                      UserName = 'DBText3'
                      HyperlinkColor = clBlue
                      AutoSize = True
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      DataField = 'Categoria'
                      DataPipeline = DBCalculadoDevuelto
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = []
                      Transparent = True
                      DataPipelineName = 'DBCalculadoDevuelto'
                      mmHeight = 4022
                      mmLeft = 3969
                      mmTop = 794
                      mmWidth = 66040
                      BandType = 4
                    end
                    object ppDBText45: TppDBText
                      UserName = 'DBText4'
                      HyperlinkColor = clBlue
                      AutoSize = True
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      DataField = 'Cantidad Devueltos'
                      DataPipeline = DBCalculadoDevuelto
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = []
                      TextAlignment = taCentered
                      Transparent = True
                      DataPipelineName = 'DBCalculadoDevuelto'
                      mmHeight = 4022
                      mmLeft = 123608
                      mmTop = 794
                      mmWidth = 1947
                      BandType = 4
                    end
                  end
                  object ppSummaryBand16: TppSummaryBand
                    AlignToBottom = False
                    mmBottomOffset = 0
                    mmHeight = 5292
                    mmPrintPosition = 0
                    object ppLine34: TppLine
                      UserName = 'Line16'
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      ParentWidth = True
                      Weight = 0.750000000000000000
                      mmHeight = 3969
                      mmLeft = 0
                      mmTop = 0
                      mmWidth = 197300
                      BandType = 7
                    end
                    object ppLabel56: TppLabel
                      UserName = 'Label5'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      Caption = 'Total'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = []
                      Transparent = True
                      mmHeight = 3969
                      mmLeft = 68263
                      mmTop = 529
                      mmWidth = 7938
                      BandType = 7
                    end
                    object ppDBCalc13: TppDBCalc
                      UserName = 'DBCalc4'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      DataField = 'Cantidad Devueltos'
                      DataPipeline = DBCalculadoDevuelto
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = []
                      TextAlignment = taCentered
                      Transparent = True
                      DataPipelineName = 'DBCalculadoDevuelto'
                      mmHeight = 3969
                      mmLeft = 116152
                      mmTop = 529
                      mmWidth = 17198
                      BandType = 7
                    end
                  end
                end
              end
              object pplblSaldoDiferencias: TppLabel
                UserName = 'pplblSaldoDiferencias'
                HyperlinkColor = clBlue
                Border.BorderPositions = []
                Border.Color = clBlack
                Border.Style = psSolid
                Border.Visible = False
                Border.Weight = 1.000000000000000000
                Caption = 'Saldo Final Calculado y Diferencias'
                Ellipsis = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Name = 'Arial'
                Font.Size = 13
                Font.Style = [fsBold]
                Transparent = True
                mmHeight = 5292
                mmLeft = 58473
                mmTop = 108215
                mmWidth = 76994
                BandType = 5
                GroupNo = 0
              end
              object ppLineSaldoDiferencias1: TppLine
                UserName = 'LineSaldoDiferencias1'
                Border.BorderPositions = []
                Border.Color = clBlack
                Border.Style = psSolid
                Border.Visible = False
                Border.Weight = 1.000000000000000000
                ParentWidth = True
                Position = lpBottom
                Weight = 0.750000000000000000
                mmHeight = 1323
                mmLeft = 0
                mmTop = 112713
                mmWidth = 197300
                BandType = 5
                GroupNo = 0
              end
              object srCalculadoSaldoFinal: TppSubReport
                UserName = 'srCalculadoSaldoFinal'
                ExpandAll = False
                NewPrintJob = False
                OutlineSettings.CreateNode = True
                TraverseAllData = False
                DataPipelineName = 'DBPCalculadoSaldoFinal'
                mmHeight = 33073
                mmLeft = 0
                mmTop = 114565
                mmWidth = 197300
                BandType = 5
                GroupNo = 0
                mmBottomOffset = 0
                mmOverFlowOffset = 0
                mmStopPosition = 0
                mmMinHeight = 0
                object ppChildReport18: TppChildReport
                  AutoStop = False
                  DataPipeline = DBPCalculadoSaldoFinal
                  PrinterSetup.BinName = 'Default'
                  PrinterSetup.Copies = 2
                  PrinterSetup.DocumentName = 'Report'
                  PrinterSetup.PaperName = 'A4'
                  PrinterSetup.PrinterName = 'Default'
                  PrinterSetup.SaveDeviceSettings = False
                  PrinterSetup.mmMarginBottom = 6350
                  PrinterSetup.mmMarginLeft = 6350
                  PrinterSetup.mmMarginRight = 6350
                  PrinterSetup.mmMarginTop = 6350
                  PrinterSetup.mmPaperHeight = 297000
                  PrinterSetup.mmPaperWidth = 210000
                  PrinterSetup.PaperSize = 9
                  Version = '12.04'
                  mmColumnWidth = 0
                  DataPipelineName = 'DBPCalculadoSaldoFinal'
                  object ppTitleBand17: TppTitleBand
                    mmBottomOffset = 0
                    mmHeight = 6350
                    mmPrintPosition = 0
                    object ppShape5: TppShape
                      UserName = 'Shape5'
                      Brush.Color = 16444382
                      Gradient.EndColor = clWhite
                      Gradient.StartColor = clWhite
                      Gradient.Style = gsNone
                      ParentWidth = True
                      Pen.Style = psClear
                      mmHeight = 6350
                      mmLeft = 0
                      mmTop = 265
                      mmWidth = 197300
                      BandType = 1
                    end
                    object ppLabel58: TppLabel
                      UserName = 'Label2'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      Caption = 'Categor'#237'a de Telev'#237'as'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 12
                      Font.Style = [fsBold]
                      Transparent = True
                      mmHeight = 4995
                      mmLeft = 3970
                      mmTop = 1058
                      mmWidth = 43603
                      BandType = 1
                    end
                    object ppLine36: TppLine
                      UserName = 'Line6'
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      ParentWidth = True
                      Position = lpBottom
                      Weight = 0.750000000000000000
                      mmHeight = 1323
                      mmLeft = 0
                      mmTop = 5027
                      mmWidth = 197300
                      BandType = 1
                    end
                    object ppLabel59: TppLabel
                      UserName = 'Label12'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      Caption = 'Cantidad Final de Telev'#237'as'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 12
                      Font.Style = [fsBold]
                      TextAlignment = taCentered
                      Transparent = True
                      mmHeight = 5027
                      mmLeft = 98161
                      mmTop = 1323
                      mmWidth = 53181
                      BandType = 1
                    end
                  end
                  object ppDetailBand19: TppDetailBand
                    Background1.Brush.Style = bsClear
                    Background1.Gradient.EndColor = clWhite
                    Background1.Gradient.StartColor = clWhite
                    Background1.Gradient.Style = gsNone
                    Background2.Brush.Style = bsClear
                    Background2.Gradient.EndColor = clWhite
                    Background2.Gradient.StartColor = clWhite
                    Background2.Gradient.Style = gsNone
                    mmBottomOffset = 0
                    mmHeight = 5291
                    mmPrintPosition = 0
                    object ppDBText46: TppDBText
                      UserName = 'DBText3'
                      HyperlinkColor = clBlue
                      AutoSize = True
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      DataField = 'Categoria'
                      DataPipeline = DBPCalculadoSaldoFinal
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = []
                      ParentDataPipeline = False
                      Transparent = True
                      DataPipelineName = 'DBPCalculadoSaldoFinal'
                      mmHeight = 4022
                      mmLeft = 3969
                      mmTop = 794
                      mmWidth = 15198
                      BandType = 4
                    end
                    object ppDBText47: TppDBText
                      UserName = 'DBText4'
                      HyperlinkColor = clBlue
                      AutoSize = True
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      DataField = 'Cantidad Final'
                      DataPipeline = DBPCalculadoSaldoFinal
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = []
                      ParentDataPipeline = False
                      TextAlignment = taCentered
                      Transparent = True
                      DataPipelineName = 'DBPCalculadoSaldoFinal'
                      mmHeight = 4022
                      mmLeft = 113300
                      mmTop = 794
                      mmWidth = 22564
                      BandType = 4
                    end
                  end
                  object ppSummaryBand17: TppSummaryBand
                    AlignToBottom = False
                    mmBottomOffset = 0
                    mmHeight = 5027
                    mmPrintPosition = 0
                    object ppLine37: TppLine
                      UserName = 'Line17'
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      ParentWidth = True
                      Weight = 0.750000000000000000
                      mmHeight = 3969
                      mmLeft = 0
                      mmTop = 265
                      mmWidth = 197300
                      BandType = 7
                    end
                    object ppLabel60: TppLabel
                      UserName = 'Label23'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      Caption = 'Total'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = []
                      Transparent = True
                      mmHeight = 3969
                      mmLeft = 68263
                      mmTop = 794
                      mmWidth = 7938
                      BandType = 7
                    end
                    object ppDBCalc14: TppDBCalc
                      UserName = 'DBCalc6'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      DataField = 'Cantidad Final'
                      DataPipeline = DBPCalculadoSaldoFinal
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = []
                      TextAlignment = taCentered
                      Transparent = True
                      DataPipelineName = 'DBPCalculadoSaldoFinal'
                      mmHeight = 3969
                      mmLeft = 116152
                      mmTop = 794
                      mmWidth = 17198
                      BandType = 7
                    end
                  end
                end
              end
              object srSaldoDiferencia: TppSubReport
                UserName = 'srSaldoDiferencia'
                ExpandAll = False
                NewPrintJob = False
                OutlineSettings.CreateNode = True
                TraverseAllData = False
                DataPipelineName = 'DBPSaldoDiferencia'
                mmHeight = 33073
                mmLeft = 0
                mmTop = 148167
                mmWidth = 197300
                BandType = 5
                GroupNo = 0
                mmBottomOffset = 0
                mmOverFlowOffset = 0
                mmStopPosition = 0
                mmMinHeight = 0
                object ppChildReport19: TppChildReport
                  AutoStop = False
                  DataPipeline = DBPSaldoDiferencia
                  PrinterSetup.BinName = 'Default'
                  PrinterSetup.Copies = 2
                  PrinterSetup.DocumentName = 'Report'
                  PrinterSetup.PaperName = 'A4'
                  PrinterSetup.PrinterName = 'Default'
                  PrinterSetup.SaveDeviceSettings = False
                  PrinterSetup.mmMarginBottom = 6350
                  PrinterSetup.mmMarginLeft = 6350
                  PrinterSetup.mmMarginRight = 6350
                  PrinterSetup.mmMarginTop = 6350
                  PrinterSetup.mmPaperHeight = 297000
                  PrinterSetup.mmPaperWidth = 210000
                  PrinterSetup.PaperSize = 9
                  Version = '12.04'
                  mmColumnWidth = 0
                  DataPipelineName = 'DBPSaldoDiferencia'
                  object ppTitleBand18: TppTitleBand
                    mmBottomOffset = 0
                    mmHeight = 6350
                    mmPrintPosition = 0
                    object ppShape8: TppShape
                      UserName = 'Shape8'
                      Brush.Color = 16444382
                      Gradient.EndColor = clWhite
                      Gradient.StartColor = clWhite
                      Gradient.Style = gsNone
                      ParentWidth = True
                      Pen.Style = psClear
                      mmHeight = 6350
                      mmLeft = 0
                      mmTop = 0
                      mmWidth = 197300
                      BandType = 1
                    end
                    object ppLabel61: TppLabel
                      UserName = 'Label2'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      Caption = 'Categor'#237'a de Telev'#237'as'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 12
                      Font.Style = [fsBold]
                      Transparent = True
                      mmHeight = 4995
                      mmLeft = 3970
                      mmTop = 1058
                      mmWidth = 43603
                      BandType = 1
                    end
                    object ppLine38: TppLine
                      UserName = 'Line8'
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      ParentWidth = True
                      Position = lpBottom
                      Weight = 0.750000000000000000
                      mmHeight = 1323
                      mmLeft = 0
                      mmTop = 5027
                      mmWidth = 197300
                      BandType = 1
                    end
                    object ppLabel62: TppLabel
                      UserName = 'Label1'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      Caption = 'Diferencias de Telev'#237'as'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 12
                      Font.Style = [fsBold]
                      TextAlignment = taCentered
                      Transparent = True
                      mmHeight = 5027
                      mmLeft = 101336
                      mmTop = 1058
                      mmWidth = 46831
                      BandType = 1
                    end
                  end
                  object ppDetailBand20: TppDetailBand
                    Background1.Brush.Style = bsClear
                    Background1.Gradient.EndColor = clWhite
                    Background1.Gradient.StartColor = clWhite
                    Background1.Gradient.Style = gsNone
                    Background2.Brush.Style = bsClear
                    Background2.Gradient.EndColor = clWhite
                    Background2.Gradient.StartColor = clWhite
                    Background2.Gradient.Style = gsNone
                    mmBottomOffset = 0
                    mmHeight = 5292
                    mmPrintPosition = 0
                    object ppDBText48: TppDBText
                      UserName = 'DBText3'
                      HyperlinkColor = clBlue
                      AutoSize = True
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      DataField = 'Categoria'
                      DataPipeline = DBPSaldoDiferencia
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = []
                      ParentDataPipeline = False
                      Transparent = True
                      DataPipelineName = 'DBPSaldoDiferencia'
                      mmHeight = 3979
                      mmLeft = 3969
                      mmTop = 794
                      mmWidth = 72221
                      BandType = 4
                    end
                    object ppDBText49: TppDBText
                      UserName = 'DBText4'
                      HyperlinkColor = clBlue
                      AutoSize = True
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      DataField = 'Saldo Final'
                      DataPipeline = DBPSaldoDiferencia
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = []
                      ParentDataPipeline = False
                      TextAlignment = taCentered
                      Transparent = True
                      DataPipelineName = 'DBPSaldoDiferencia'
                      mmHeight = 3979
                      mmLeft = 111882
                      mmTop = 794
                      mmWidth = 25400
                      BandType = 4
                    end
                  end
                  object ppSummaryBand18: TppSummaryBand
                    AlignToBottom = False
                    mmBottomOffset = 0
                    mmHeight = 6350
                    mmPrintPosition = 0
                    object ppLine39: TppLine
                      UserName = 'Line18'
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      ParentWidth = True
                      Weight = 0.750000000000000000
                      mmHeight = 3969
                      mmLeft = 0
                      mmTop = 529
                      mmWidth = 197300
                      BandType = 7
                    end
                    object ppLabel63: TppLabel
                      UserName = 'Label24'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      Caption = 'Total'
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = []
                      Transparent = True
                      mmHeight = 3969
                      mmLeft = 68263
                      mmTop = 1058
                      mmWidth = 7938
                      BandType = 7
                    end
                    object ppDBCalc15: TppDBCalc
                      UserName = 'DBCalc5'
                      HyperlinkColor = clBlue
                      Border.BorderPositions = []
                      Border.Color = clBlack
                      Border.Style = psSolid
                      Border.Visible = False
                      Border.Weight = 1.000000000000000000
                      DataField = 'Saldo Final'
                      DataPipeline = DBPSaldoDiferencia
                      Ellipsis = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlack
                      Font.Name = 'Arial'
                      Font.Size = 10
                      Font.Style = []
                      TextAlignment = taCentered
                      Transparent = True
                      DataPipelineName = 'DBPSaldoDiferencia'
                      mmHeight = 3969
                      mmLeft = 116152
                      mmTop = 1058
                      mmWidth = 17198
                      BandType = 7
                    end
                  end
                end
              end
              object ppLineSaldoDiferencias: TppLine
                UserName = 'LineSaldoDiferencias'
                Border.BorderPositions = []
                Border.Color = clBlack
                Border.Style = psSolid
                Border.Visible = False
                Border.Weight = 1.000000000000000000
                ParentWidth = True
                Position = lpBottom
                Weight = 0.750000000000000000
                mmHeight = 1323
                mmLeft = 0
                mmTop = 106627
                mmWidth = 197300
                BandType = 5
                GroupNo = 0
              end
            end
          end
        end
      end
    end
    object raCodeModule6: TraCodeModule
      ProgramStream = {00}
    end
    object ppParameterList1: TppParameterList
    end
  end
  object ppDetalleCobrosEfectivo: TppDBPipeline
    DataSource = dsLiquidacionEfectivo
    OpenDataSource = False
    UserName = 'DetalleCobrosEfectivo'
    Left = 40
    Top = 225
    object ppDetalleCobrosEfectivoppField1: TppField
      FieldAlias = 'Tipo'
      FieldName = 'Tipo'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 0
      Searchable = False
      Sortable = False
    end
    object ppDetalleCobrosEfectivoppField2: TppField
      FieldAlias = 'CodigoDenominacionMoneda'
      FieldName = 'CodigoDenominacionMoneda'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 1
      Searchable = False
      Sortable = False
    end
    object ppDetalleCobrosEfectivoppField3: TppField
      FieldAlias = 'Descripcion'
      FieldName = 'Descripcion'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 2
      Searchable = False
      Sortable = False
    end
    object ppDetalleCobrosEfectivoppField4: TppField
      FieldAlias = 'ValorMonedaLocal'
      FieldName = 'ValorMonedaLocal'
      FieldLength = 0
      DataType = dtNotKnown
      DisplayWidth = 0
      Position = 3
      Searchable = False
      Sortable = False
    end
    object ppDetalleCobrosEfectivoppField5: TppField
      FieldAlias = 'Cantidad'
      FieldName = 'Cantidad'
      FieldLength = 0
      DataType = dtDouble
      DisplayFormat = '#0'
      DisplayWidth = 0
      Position = 4
      Searchable = False
      Sortable = False
    end
    object ppDetalleCobrosEfectivoppField6: TppField
      FieldAlias = 'Importe'
      FieldName = 'Importe'
      FieldLength = 0
      DataType = dtCurrency
      DisplayWidth = 0
      Position = 5
      Searchable = False
      Sortable = False
    end
  end
  object dsLiquidacionEfectivo: TDataSource
    DataSet = spObtenerLiquidacionTurnoEfectivo
    Left = 106
    Top = 225
  end
  object spObtenerLiquidacionTurnoEfectivo: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerLiquidacionTurnoEfectivo;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@MontoAperturaCaja'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 15
        Value = Null
      end
      item
        Name = '@MontoCierreCaja'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 15
        Value = Null
      end
      item
        Name = '@DiferenciaAperturaCierre'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 15
        Value = Null
      end
      item
        Name = '@PagosEfectivo'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 15
        Value = Null
      end
      item
        Name = '@DiferenciaEfectivoCalculada'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 15
        Value = Null
      end>
    Left = 74
    Top = 225
  end
  object dsDetallesTurno: TDataSource
    DataSet = spObtenerDetalleTurno
    Left = 107
    Top = 256
  end
  object ppDBPipelineDetalleTurno: TppDBPipeline
    DataSource = dsDetallesTurno
    UserName = 'DBPipelineDetalleTurno'
    Left = 38
    Top = 257
  end
  object spObtenerDetalleTurno: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerDetalleTurno;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 1
      end>
    Left = 74
    Top = 257
  end
  object spObtenerLiquidacionTurnoCupones: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerLiquidacionTurnoCupones'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end>
    Left = 75
    Top = 288
  end
  object dsObtenerLiquidacionTurnoCupones: TDataSource
    DataSet = spObtenerLiquidacionTurnoCupones
    Left = 107
    Top = 288
  end
  object ppDBPipelineLiquidacionCupones: TppDBPipeline
    DataSource = dsObtenerLiquidacionTurnoCupones
    UserName = 'DBPipelineDetalleTurno1'
    Left = 38
    Top = 289
  end
  object sp_ObtenerTotalCobradoNotasCobroTurno: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerTotalCobradoNotasCobroTurno;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@TotalCobradoNotasCobro'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 15
        Value = ''
      end>
    Left = 228
    Top = 209
  end
  object sp_ObtenerTotalCobradoTarjetaCreditoTurno: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerTotalCobradoTarjetaCreditoTurno;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@TotalCobradoTarjetaCredito'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 16
        Value = '0'
      end>
    Left = 231
    Top = 429
  end
  object sp_ObtenerTotalCobradoTarjetaDebitoTurno: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerTotalCobradoTarjetaDebitoTurno;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@TotalCobradoTarjetaDebito'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 16
        Value = '0'
      end>
    Left = 231
    Top = 459
  end
  object sp_ObtenerTotalNotasCreditoPagadasTurno: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerTotalNotasCreditoPagadasTurno;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalNotasCredito'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 15
        Value = Null
      end>
    Left = 228
    Top = 177
  end
  object ppLiquidacionPorCanal: TppDBPipeline
    DataSource = dsLiquidacionPorCanal
    UserName = 'LiquidacionPorCanal'
    Left = 40
    Top = 319
  end
  object dsLiquidacionPorCanal: TDataSource
    DataSet = spObtenerLiquidacionPorCanal
    Left = 106
    Top = 318
  end
  object spObtenerLiquidacionPorCanal: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'ObtenerLiquidacionPorCanal;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@PorRefinanciacion'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = False
      end
      item
        Name = '@DesgloseCheques'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = False
      end>
    Left = 74
    Top = 318
  end
  object spObtenerTotalCobradoNotasCobroInfractorTurno: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerTotalCobradoNotasCobroInfractorTurno'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalCobradoNotasCobroInfractor'
        Attributes = [paNullable]
        DataType = ftBCD
        Direction = pdInputOutput
        Precision = 18
        Value = Null
      end
      item
        Name = '@DescriTotalCobradoNotasCobroInfractor'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 15
        Value = Null
      end>
    Left = 280
    Top = 225
  end
  object spObtenerTotalCobradoSinComprobante: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerTotalCobradoSinComprobante'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalCobradoSinComprobante'
        Attributes = [paNullable]
        DataType = ftBCD
        Direction = pdInputOutput
        Precision = 18
        Value = Null
      end
      item
        Name = '@DescriTotalCobradoSinComprobante'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 15
        Value = Null
      end>
    Left = 304
    Top = 300
  end
  object spObtenerTotalCobradoValesVistaTurno: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerTotalCobradoValesVistaTurno;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalCobradoValesVista'
        Attributes = [paNullable]
        DataType = ftBCD
        Direction = pdInputOutput
        Precision = 18
        Value = Null
      end
      item
        Name = '@DescriTotalCobradoValesVista'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 15
        Value = Null
      end>
    Left = 280
    Top = 348
  end
  object sp_ObtenerTotalCobradoBoletasTurno: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerTotalCobradoBoletasTurno;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalCobradoBoletas'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 15
        Value = Null
      end>
    Left = 228
    Top = 240
  end
  object spObtenerTotalCobradoChequesTurnoDia: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerTotalCobradoChequesTurnoDia'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ChequesSantander'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@TotalCobradoCheques'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 15
        Value = Null
      end>
    Left = 544
    Top = 38
  end
  object sp_ObtenerTotalCobradoCuotasArriendoTurno: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerTotalCobradoCuotasArriendoTurno;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalCobradoCuotasArriendo'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 15
        Value = Null
      end>
    Left = 328
    Top = 240
  end
  object sp_ObtenerTotalCobradoPagareTurno: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerTotalCobradoPagareTurno;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalCobradoPagare'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 15
        Value = Null
      end>
    Left = 280
    Top = 429
  end
  object spObtenerAnulacionesCierreTurno: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerAnulacionesCierreTurno;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 72
    Top = 392
  end
  object dsObtenerAnulacionesCierreTurno: TDataSource
    DataSet = spObtenerAnulacionesCierreTurno
    Left = 104
    Top = 392
  end
  object ppObtenerAnulacionesCierreTurno: TppDBPipeline
    DataSource = dsObtenerAnulacionesCierreTurno
    UserName = 'ObtenerAnulacionesCierreTurno'
    Left = 40
    Top = 392
  end
  object spObtenerTotalCobrosAnuladosTurno: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerTotalCobrosAnuladosTurno;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalCobrosAnuladosTurno'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 15
        Value = Null
      end>
    Left = 336
    Top = 429
  end
  object spObtenerTotalCobradoEfectivoTurno: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerTotalCobradoEfectivoTurno;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalCobradoEfectivo'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 15
        Value = Null
      end>
    Left = 371
    Top = 429
  end
  object spObtenerLiquidacionPorCanalRefinan: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'ObtenerLiquidacionPorCanal'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PorRefinanciacion'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@DesgloseCheques'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 144
    Top = 512
  end
  object ppDBPipeline1LiquidacionPorCanalRefinan: TppDBPipeline
    DataSource = dsObtenerLiquidacionPorCanalRefinan
    UserName = 'DBPipeline1LiquidacionPorCanalRefinan'
    Left = 48
    Top = 512
  end
  object dsObtenerLiquidacionPorCanalRefinan: TDataSource
    DataSet = spObtenerLiquidacionPorCanalRefinan
    Left = 96
    Top = 512
  end
  object spObtenerRefinanciacionTotalesPagosTurno: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerRefinanciacionTotalesPagosTurno'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 584
    Top = 176
  end
  object spObtenerRefinanciacionDiferenciasPiesNoLiquidados: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerRefinanciacionDiferenciasPiesNoLiquidados'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 504
    Top = 288
  end
  object dsObtenerRefinanciacionDiferenciasPiesNoLiquidados: TDataSource
    DataSet = spObtenerRefinanciacionDiferenciasPiesNoLiquidados
    Left = 520
    Top = 304
  end
  object ppDBPipelinePiesNoLiquidados: TppDBPipeline
    DataSource = dsObtenerRefinanciacionDiferenciasPiesNoLiquidados
    UserName = 'DBPipelinePiesNoLiquidados'
    Left = 536
    Top = 336
  end
  object spObtenerTotalCobradoDepositoAnticipadoTurno: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerTotalCobradoDepositoAnticipadoTurno'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalCobradoDepositoAnticipado'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 15
        Value = Null
      end>
    Left = 544
    Top = 408
  end
  object spObtenerLiquidacionPorCanalDetalle: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'ObtenerLiquidacionPorCanal;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@NumeroTurno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 73130
      end
      item
        Name = '@PorRefinanciacion'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = False
      end
      item
        Name = '@DesgloseCheques'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = True
      end>
    Left = 856
    Top = 504
  end
  object dsObtenerLiquidacionPorCanalDetalle: TDataSource
    DataSet = spObtenerLiquidacionPorCanalDetalle
    Left = 872
    Top = 528
  end
  object ppDBpplnObtenerLiquidacionPorCanalDetalle: TppDBPipeline
    DataSource = dsObtenerLiquidacionPorCanalDetalle
    UserName = 'DBpplnObtenerLiquidacionPorCanalDetalle'
    Left = 888
    Top = 552
  end
  object spObtenerComprobantesDVPorTurno: TADOStoredProc
    Connection = DMConnections.BaseCAC
    ProcedureName = 'ObtenerComprobantesDVPorTurno'
    Parameters = <>
    Left = 400
    Top = 40
  end
  object spObtenerMaestroConcesionaria: TADOStoredProc
    Connection = DMConnections.BaseCAC
    CursorType = ctStatic
    ProcedureName = 'ObtenerMaestroConcesionaria'
    Parameters = <>
    Left = 632
    Top = 26
  end
end
