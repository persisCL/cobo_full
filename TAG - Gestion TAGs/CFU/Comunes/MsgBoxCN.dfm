object frmMsgBoxCN: TfrmMsgBoxCN
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = 'frmMsgBoxCN'
  ClientHeight = 110
  ClientWidth = 564
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  DesignSize = (
    564
    110)
  PixelsPerInch = 96
  TextHeight = 13
  object imgMsgBoxCN: TImage
    Left = 8
    Top = 17
    Width = 32
    Height = 32
  end
  object lblTitulo: TLabel
    Left = 49
    Top = 22
    Width = 28
    Height = 13
    Caption = 'Error'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Panel1: TPanel
    Left = 47
    Top = 40
    Width = 510
    Height = 62
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelOuter = bvLowered
    TabOrder = 0
    DesignSize = (
      510
      62)
    object btnCancelar: TButton
      Left = 430
      Top = 32
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = 'Cancelar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ModalResult = 2
      ParentFont = False
      TabOrder = 3
      Visible = False
    end
    object meMensajeAMostrar: TMemo
      Left = 10
      Top = 7
      Width = 490
      Height = 13
      TabStop = False
      Anchors = [akLeft, akTop, akRight, akBottom]
      BevelInner = bvLowered
      BevelOuter = bvNone
      BorderStyle = bsNone
      Color = clBtnFace
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
    end
    object chBxInfoExtendida: TCheckBox
      Left = 10
      Top = 39
      Width = 121
      Height = 17
      Anchors = [akLeft, akBottom]
      Caption = 'Ver Info Extendida'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      Visible = False
      OnClick = chBxInfoExtendidaClick
    end
    object btnAceptar: TButton
      Left = 430
      Top = 32
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Aceptar'
      Default = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ModalResult = 1
      ParentFont = False
      TabOrder = 2
    end
  end
end
