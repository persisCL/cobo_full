object frmAgregarCheque: TfrmAgregarCheque
  Left = 346
  Top = 189
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsDialog
  Caption = 'Agregar Cheque'
  ClientHeight = 258
  ClientWidth = 460
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  Visible = True
  OnClose = FormClose
  DesignSize = (
    460
    258)
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 8
    Top = 8
    Width = 441
    Height = 209
  end
  object lblConcepto: TLabel
    Left = 33
    Top = 105
    Width = 34
    Height = 13
    Caption = 'Banco:'
  end
  object lblObservaciones: TLabel
    Left = 33
    Top = 131
    Width = 63
    Height = 13
    Caption = 'Nro. Cheque:'
  end
  object lblImporte: TLabel
    Left = 33
    Top = 158
    Width = 73
    Height = 13
    Caption = 'Fecha Cheque:'
  end
  object lblImporteIVA: TLabel
    Left = 33
    Top = 182
    Width = 30
    Height = 13
    Caption = 'Monto'
  end
  object lblTipoComprobante: TLabel
    Left = 33
    Top = 45
    Width = 32
    Height = 13
    Caption = 'Titular:'
  end
  object lblFecha: TLabel
    Left = 34
    Top = 69
    Width = 20
    Height = 13
    Caption = 'Rut:'
  end
  object lblPagareTitulo: TLabel
    Left = 55
    Top = 18
    Width = 349
    Height = 14
    Caption = 'Complete los siguientes datos para completar y registrar el pago'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object btnDatosClientePagare: TSpeedButton
    Left = 320
    Top = 40
    Width = 22
    Height = 21
    Hint = 'Copia los datos del Cliente comprobante'
    Glyph.Data = {
      AA040000424DAA04000000000000360000002800000013000000130000000100
      1800000000007404000000000000000000000000000000000000D8E9ECD8E9EC
      D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9
      ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8
      E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
      D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9
      ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8
      E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
      D8E9EC840000840000840000840000840000840000840000840000840000D8E9
      ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC84
      0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000D8E9ECD8E9EC
      D8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC840000FFFF
      FF000000000000000000000000000000FFFFFF840000D8E9ECD8E9ECD8E9EC00
      0000D8E9EC000000000000000000000000000000000000840000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9
      EC000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF840000FFFFFF00000000000000
      0000000000000000FFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000
      FFFFFF000000000000000000000000840000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFF840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFF840000FFFFFF000000000000FFFFFF840000840000
      840000840000D8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFF0000000000
      00000000000000840000FFFFFFFFFFFFFFFFFFFFFFFF840000FFFFFF840000D8
      E9ECD8E9ECD8E9ECD8E9EC000000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFF840000FFFFFFFFFFFFFFFFFFFFFFFF840000840000D8E9ECD8E9ECD8E9
      ECD8E9ECD8E9EC000000D8E9EC000000FFFFFF000000000000FFFFFF00000084
      0000840000840000840000840000840000D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
      D8E9EC000000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF0000
      00D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC00
      0000D8E9EC000000FFFFFFFFFFFFFFFFFFFFFFFF000000000000D8E9ECD8E9EC
      D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9
      EC000000000000000000000000000000000000D8E9ECD8E9ECD8E9ECD8E9ECD8
      E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9EC
      D8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9
      ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8
      E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9EC
      D8E9ECD8E9ECD8E9ECD8E9ECD8E9EC000000D8E9ECD8E9ECD8E9ECD8E9ECD8E9
      ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8E9ECD8
      E9ECD8E9ECD8E9ECD8E9EC000000}
    ParentShowHint = False
    ShowHint = True
    OnClick = btnDatosClientePagareClick
  end
  object cbBanco: TVariantComboBox
    Left = 113
    Top = 99
    Width = 200
    Height = 21
    Style = vcsDropDownList
    ItemHeight = 13
    TabOrder = 2
    Items = <>
  end
  object btnCancelar: TButton
    Left = 371
    Top = 227
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 7
    OnClick = btnCancelarClick
  end
  object btnAceptar: TButton
    Left = 284
    Top = 227
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Aceptar'
    Default = True
    TabOrder = 6
    OnClick = btnAceptarClick
  end
  object ednumerocheque: TEdit
    Left = 113
    Top = 127
    Width = 136
    Height = 21
    TabOrder = 3
  end
  object edFecha: TDateEdit
    Left = 113
    Top = 153
    Width = 90
    Height = 21
    AutoSelect = False
    TabOrder = 4
    Date = -693594.000000000000000000
  end
  object edMonto: TNumericEdit
    Left = 113
    Top = 178
    Width = 77
    Height = 21
    MaxLength = 11
    TabOrder = 5
    Decimals = 2
  end
  object EdTitular: TEdit
    Left = 112
    Top = 40
    Width = 201
    Height = 21
    TabOrder = 0
  end
  object EdRut: TEdit
    Left = 112
    Top = 71
    Width = 121
    Height = 21
    TabOrder = 1
  end
end
