{
    File Name: freMovimientoStock
    Author :
    Date Created:
    Language : ES-AR

    Description :

    Revision : 1
        Author: pdominguez
        Date: 29/06/2010
        Description: SS 900 - Error cambio almacen m�dulo Gesti�n Telev�a
            - Se a�aden los siguientes objetos:
                pnlEntradaDatos: TPanel,
                gbAgregaTelevias: TGroupbox,
                Label1, Label2, Label3, Label4: TLabel,
                dbeTeleviaInicial, dbeTeleviaFinal, dbeCantidad, dbeCategoria: TDBEdit,
                btnGrabar, btnCancelar: TButton.

            - Se a�aden las siguientes variables:
                FNombreAlmacenOrigen: String;

            - Se elliminan los siguientes procedimientos:
                dbgRecibirTagsExit,
                dbgTagsExit.
                
            - Se a�aden/modifican los siguientes procedimientos/funciones:
                btnGrabarClick,
                btnCancelarClick,
                cdsTAGsAfterPost,
                cdsTAGsBeforeEdit,
                cdsTAGsBeforePost,
                cdsTAGsNewRecord,
                ActualizaControlesEntradaDatos,
                ValidarCantidadTAGsEnLinea,
                setCodigoAlmacenOrigen,
                cdsTAGsAfterDelete,
                ActualizarTotales.
Revision : 2
        Author: jjofre
        Date: 19/07/2010
        Description: SS 902 - Televias en NULL
            - Se a�aden/modifican los siguientes procedimientos/funciones:
                GenerarMovimiento

Revision : 3
        Author: pdominguez
        Date: 28/07/2010
        Description: SS 902 - Televias en NULL
            - Se a�aden/modifican los siguientes procedimientos/funciones:
                GenerarMovimiento
}
unit freMovimientoStock;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DB, DBClient, ADODB, Grids, DBGrids, Peaprocs, util, Peatypes, ValEdit, Variants, UtilDB, UtilProc,
  RStrings, StdCtrls, Comm, ExtCtrls, Mask, DBCtrls, PeaProcsCN, MsgBoxCN;

type
  TframeMovimientoStock = class(TFrame)
    spObtenerDatosPuntoEntrega: TADOStoredProc;
    spVerificarTAGEnArchivos: TADOStoredProc;
    spObtenerDatosRangoTAGsAlmacen: TADOStoredProc;
    dsTAGs: TDataSource;
    cdsTAGs: TClientDataSet;
    spRegistrarMovimientoStock: TADOStoredProc;
    ObtenerSaldosTAGsAlmacen: TADOStoredProc;
    cdsEntrega: TClientDataSet;
    dsEntrega: TDataSource;
    vleIngresados: TValueListEditor;
    vleRecibidos: TValueListEditor;
    qryCategorias: TADOQuery;
    pnlEntradaDatos: TPanel;
    Panel1: TPanel;
    dbgRecibirTags: TDBGrid;
    dbgTags: TDBGrid;
    lblDetalle: TLabel;
    gbAgregaTelevias: TGroupBox;
    dbeTeleviaInicial: TDBEdit;
    dbeTeleviaFinal: TDBEdit;
    dbeCantidad: TDBEdit;
    dbeCategoria: TDBEdit;
    btnGrabar: TButton;
    btnCancelar: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    dbeCategoriaUrbana: TDBEdit;
    lbl1: TLabel;
    procedure cdsTAGsBeforePost(DataSet: TDataSet);
    procedure cdsTAGsNewRecord(DataSet: TDataSet);
    procedure dbgRecibirTagsKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cdsTAGsAfterPost(DataSet: TDataSet);
    procedure btnGrabarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure cdsTAGsBeforeEdit(DataSet: TDataSet);
    procedure cdsTAGsAfterDelete(DataSet: TDataSet);
  private
    FAlmacenNominado: boolean;
    FCodigoAlmacenOrigen: Integer;
    FNombreAlmacenOrigen: String; // Rev. 1 (SS 900)
    FCargando: Boolean;
    FNoAgregar: boolean;
	TAGInicial,
    TAGFinal: DWORD;
    procedure setCodigoAlmacenOrigen(const Value: Integer);
	function ValidarCategoria (Categoria: ANSIString): Boolean;
	function ValidarCantidadTAGsEnLinea (TAGInicial, TAGFinal: DWORD; CantidadTAGS: LongInt; Categoria: Integer): Boolean;
	function ValidarCantidadTAGs: Boolean;
    function ValidarTodosCeros: boolean;
	function ValidarRangosSuperpuestos(CdsRangos: TClientDataSet; ConMsg: Boolean=False): Boolean;
	procedure CalcularTotalesIngresados;
	procedure ActualizarIngresados (Categoria: ANSIString; Cantidad: LongInt);
    procedure ActualizaControlesEntradaDatos(EntradaDatosHabilitada: Boolean);
    procedure ActualizarTotales;
  public
    property CodigoAlmacenOrigen: Integer read FCodigoAlmacenOrigen write setCodigoAlmacenOrigen;
    function ValidarMovimiento(aAlmacenDestino: Integer): Boolean;
    function GenerarMovimiento(aAlmacenDestino: Integer; aGuiaDespacho: Integer; CodigoOperacion: integer): Boolean;
    function Inicializar(aCodigoAlmacen: Integer): boolean;
    procedure Limpiar;
  end;

implementation

uses DMConnection;

{$R *.dfm}

procedure TframeMovimientoStock.ActualizarIngresados(Categoria: ANSIString;
  Cantidad: Integer);
var
    idxCategoria: Integer;
begin
    vleIngresados.FindRow(Categoria, idxCategoria);
    vleIngresados.Cells[1, idxCategoria] := IntToStr(StrToInt(vleIngresados.Cells[1, idxCategoria]) + Cantidad)
end;

{
    Procedure Name: btnGrabarClick
    Parameters : Sender: TObject
    Author : pdominguez
    Date Created : 29/06/2010

    Description : SS 900 - Error cambio almacen m�dulo Gesti�n Telev�a
        - Graba los cambios efectuados sobre el objeto cdsTAGs: TClientDataSet.
}
procedure TframeMovimientoStock.btnGrabarClick(Sender: TObject);
begin
    if cdsTAGs.State in dsEditModes then cdsTAGs.Post;
end;

{
    Procedure Name: btnCancelarClick
    Parameters : Sender: TObject
    Author : pdominguez
    Date Created : 29/06/2010

    Description : SS 900 - Error cambio almacen m�dulo Gesti�n Telev�a
        - Cancela los cambios efectuados sobre el objeto cdsTAGs: TClientDataSet.
}
procedure TframeMovimientoStock.btnCancelarClick(Sender: TObject);
begin
    if cdsTAGs.State in dsEditModes then cdsTAGs.Cancel;
end;

procedure TframeMovimientoStock.CalcularTotalesIngresados;
begin
    vleRecibidos.Strings.Clear;
    vleIngresados.Strings.Clear;

    cdsEntrega.DisableControls;
    cdsTAGs.DisableControls;
	with cdsEntrega do begin
    	First;
        while not EoF do begin
			vleRecibidos.InsertRow (FieldByName('Categoria').AsString, FieldByName('CantidadTAGs').AsString, true);
			vleIngresados.InsertRow (FieldByName('Categoria').AsString, '0', true);
            Next
        end
    end;

	with cdsTAGs do begin
    	First;
        while not EoF do begin
		    ActualizarIngresados (FieldByName('CategoriaTAGs').AsString, FieldByName('CantidadTAGs').AsInteger);
            Next
        end
    end;

    cdsEntrega.EnableControls;
    cdsTAGs.EnableControls;
end;

{
    Procedure Name: cdsTAGsAfterDelete
    Parameters : DataSet: TDataSet
    Author : pdominguez
    Date Created : 30/06/2010

    Description : SS 900 - Error cambio almacen m�dulo Gesti�n Telev�a
        - Actualiza totales del la grilla de Cantidades totales de Telev�as.
}
procedure TframeMovimientoStock.cdsTAGsAfterDelete(DataSet: TDataSet);
begin
    ActualizarTotales;
end;

{
    Procedure Name: cdsTAGsAfterPost
    Parameters : DataSet: TDataSet
    Author : pdominguez
    Date Created : 29/06/2010

    Description : SS 900 - Error cambio almacen m�dulo Gesti�n Telev�a
        - Actualiza los controles del Frame.
}
procedure TframeMovimientoStock.cdsTAGsAfterPost(DataSet: TDataSet);
begin
    ActualizarTotales;
    ActualizaControlesEntradaDatos(False);
    dbgTags.SetFocus;
end;

{
    Procedure Name: cdsTAGsBeforeEdit
    Parameters : DataSet: TDataSet
    Author : pdominguez
    Date Created : 29/06/2010

    Description : SS 900 - Error cambio almacen m�dulo Gesti�n Telev�a
        - Actualiza los controles del Frame.
}
procedure TframeMovimientoStock.cdsTAGsBeforeEdit(DataSet: TDataSet);
begin
    ActualizaControlesEntradaDatos(True);
    dbeTeleviaInicial.SetFocus;
end;

{
    Revision : 1
        Author: pdominguez
        Date: 29/06/2010
        Description: SS 900 - Error cambio almacen m�dulo Gesti�n Telev�a
            - Coloca el foco sobre el control que corresponda dependiendo de
            la validaci�n efectuada.
}
procedure TframeMovimientoStock.cdsTAGsBeforePost(DataSet: TDataSet);
begin
    if (cdsTAGs.FieldByName ('NumeroInicialTAG').AsFloat <> 0) and
       (cdsTAGs.FieldByName ('NumeroFinalTAG').AsFloat = 0) and
       (cdsTAGs.FieldByName ('CantidadTAGs').AsFloat = 0) and
       (cdsTAGs.FieldByName ('CategoriaTAGs').AsInteger = 1) then begin
        cdsTAGs.FieldByName ('NumeroFinalTAG').AsFloat 	:= cdsTAGs.FieldByName ('NumeroInicialTAG').AsFloat;
        cdsTAGs.FieldByName ('CantidadTAGs').AsInteger 	:= 1;
        cdsTAGs.FieldByName ('CategoriaTAGs').AsInteger := 1;
        //INICIO 	: CFU 20161102 TASK_057_CFU_20161017-Gesti�n_TAGs_Doble_Categoria
        cdsTAGs.FieldByName ('CategoriaUrbanaTAGs').AsInteger := 4;
        //TERMINO 	: CFU 20161102 TASK_057_CFU_20161017-Gesti�n_TAGs_Doble_Categoria
    end;

	TAGInicial := EtiquetaToSerialNumber(PadL(DataSet.FieldByName('NumeroInicialTAG').AsString, 11, '0'));
   	TAGFinal  := EtiquetaToSerialNumber(PadL(DataSet.FieldByName('NumeroFinalTAG').AsString, 11, '0'));

	if not ValidarEtiqueta(MSG_CAPTION_MOVIMIENTOS_STOCK, 'Telev�a Inicial', DataSet.FieldByName('NumeroInicialTAG').AsString, DAR_MENSAJE) then begin
        dbeTeleviaInicial.SetFocus;
        abort;
    end;
	if not ValidarEtiqueta(MSG_CAPTION_MOVIMIENTOS_STOCK, 'Telev�a Final', DataSet.FieldByName('NumeroFinalTAG').AsString, DAR_MENSAJE) then begin
        dbeTeleviaFinal.SetFocus;
        abort;
    end;
	if not ValidarRangosTAGs(MSG_CAPTION_MOVIMIENTOS_STOCK, TAGInicial, TAGFinal, true, DAR_MENSAJE) then begin
        dbeTeleviaInicial.SetFocus;
        abort;
    end;
	if not ValidarCategoria (DataSet.FieldByName('CategoriaTAGs').AsString) then begin
        dbeCategoria.SetFocus;
        abort;
    end;
	if not ValidarCategoriaTAG(MSG_CAPTION_MOVIMIENTOS_STOCK, 'Telev�a Inicial', TAGInicial, DataSet.FieldByName('CategoriaTAGs').AsInteger, DAR_MENSAJE) then begin
        dbeCategoria.SetFocus;
        abort;
    end;
	if not ValidarCategoriaTAG(MSG_CAPTION_MOVIMIENTOS_STOCK, 'Telev�a Final', TAGFinal, DataSet.FieldByName('CategoriaTAGs').AsInteger, DAR_MENSAJE) then begin
        dbeCategoria.SetFocus;
        abort;
    end;
	if not ValidarCantidadTAGsEnLinea (TAGInicial, TAGFinal, DataSet.FieldByName('CantidadTAGs').AsInteger, DataSet.FieldByName('CategoriaTAGs').AsInteger) then begin
        dbeCantidad.SetFocus;
        abort;
    end;
end;

{
    Revision : 1
        Author: pdominguez
        Date: 29/06/2010
        Description: SS 900 - Error cambio almacen m�dulo Gesti�n Telev�a
            - Actualiza los controles del Frame.
}
procedure TframeMovimientoStock.cdsTAGsNewRecord(DataSet: TDataSet);
begin
    cdsTAGs.FieldByName ('NumeroInicialTAG').AsFloat:= 0;
    cdsTAGs.FieldByName ('NumeroFinalTAG').AsFloat 	:= 0;
    cdsTAGs.FieldByName ('CantidadTAGs').AsInteger 	:= 0;
//    cdsTAGs.FieldByName ('CategoriaTAGs').AsInteger := cdsEntrega.FieldByName ('Categoria').AsInteger;
    cdsTAGs.FieldByName ('CategoriaTAGs').AsInteger := 1;
    //INICIO 	: CFU 20161102 TASK_057_CFU_20161017-Gesti�n_TAGs_Doble_Categoria
    cdsTAGs.FieldByName ('CategoriaUrbanaTAGs').AsInteger := 4;
    //TERMINO 	: CFU 20161102 TASK_057_CFU_20161017-Gesti�n_TAGs_Doble_Categoria
    ActualizaControlesEntradaDatos(True);
    dbeTeleviaInicial.SetFocus;
end;

function TframeMovimientoStock.GenerarMovimiento(aAlmacenDestino,
  aGuiaDespacho: Integer; CodigoOperacion: integer): Boolean;

resourcestring
TITULO_MOVIMIENTO_TELEVIA = 'Movimiento de Telev�as realizado con �xito'; //REV. 2

var
    TodoOk: Boolean;
    CantidadNoNominada: longint;
	idxCategoria: Integer;
    TextoMensaje: AnsiString;  //REV.2
    NombreAlmacenDestino: AnsiString; // Rev. 3 (SS 902)
begin

    result := False;

    NombreAlmacenDestino := Trim(QueryGetValue(DMConnections.BaseCAC, Format('SELECT Descripcion FROM MaestroAlmacenes WITH (NOLOCK) WHERE CodigoAlmacen = %d', [aAlmacenDestino]))); // Rev. 3 (SS 902)

	if cdsTAGs.State in [dsEdit, dsInsert] then cdsTAGs.Post;
    if cdsEntrega.State in [dsEdit, dsInsert] then cdsEntrega.Post;

    if not ValidarMovimiento(aAlmacenDestino) then Exit;

	Screen.Cursor := crHourGlass;
    Application.ProcessMessages;
    TodoOk:= False;
    DMConnections.BaseCAC.BeginTrans;

    try
        	//Para Almacen Nominado
            if not cdsTAGs.IsEmpty then begin
                cdsTAGs.DisableControls;
                cdsTAGs.First;
                while not cdsTAGs.Eof do begin
                    try
                        with spRegistrarMovimientoStock, Parameters do begin
                            Close;

                            ParamByName('@CodigoOperacion').Value 			:= CodigoOperacion;
                            ParamByName('@TipoMovimiento').Value 			:= CONST_MOVIMIENTO_NOMINADO;
                            ParamByName('@CodigoEmbalaje').Value 			:= NULL;

                            ParamByName('@NumeroInicialTAG').Value 			:= EtiquetaToSerialNumber(PadL(trim(cdsTAGs.FieldByName('NumeroInicialTAG').AsString), 11, '0'));
                            ParamByName('@NumeroFinalTAG').Value 			:= EtiquetaToSerialNumber(PadL(trim(cdsTAGs.FieldByName('NumeroFinalTAG').AsString), 11, '0'));

                            ParamByName('@CategoriaTAG').Value 				:= cdsTAGs.FieldByName('CategoriaTAGs').AsInteger;
                            ParamByName('@Cantidad').Value 					:= cdsTAGs.FieldByName('CantidadTAGs').AsInteger;
                            ParamByName('@EstadoControl').Value 			:= Trim(QueryGetValue(DMConnections.BaseCAC, 'SELECT DBO.CONST_TAG_CONTROL_CALIDAD_ACEPTADO()'));
                            ParamByName('@EstadoSituacion').Value 			:= QueryGetValueInt(DMConnections.BaseCAC, 'SELECT DBO.CONST_TAG_ESTADO_SITUACION_DISPONIBLE()');

                            ParamByName('@CodigoAlmacenOrigen').Value 		:= self.CodigoAlmacenOrigen;
                            ParamByName('@CodigoAlmacenDestino').Value 		:= aAlmacenDestino;

                            ParamByName('@GuiaDespacho').Value 				:= iif(aGuiaDespacho = -1, null, aGuiaDespacho);
                            ParamByName('@Usuario').Value 					:= UsuarioSistema;

                            ParamByName('@FechaHoraEvento').Value 			:= NowBase(DMConnections.BaseCAC);
                            ParamByName('@MotivoEntrega').Value 			:= null;
                            ParamByName('@ConstanciaCarabinero').Value 		:= null;
                            ParamByName('@FechaHoraConstancia').Value 		:= null;
                            ParamByName('@DescripcionMotivoRegistro').Value := null;
                            ParamByName('@CodigoComunicacion').Value 		:= null;

                            ExecProc;
                        end;
                        //REV. 2
                        TextoMensaje := TextoMensaje + 'Se ha movido la Cantidad de  ' + cdsTAGs.FieldByName('CantidadTAGs').AsString +  ' Telev�as disponibles entre el rango especificado, desde el Telev�a ' +  trim(cdsTAGs.FieldByName('NumeroInicialTAG').AsString) + ', hasta el Telev�a ' + trim(cdsTAGs.FieldByName('NumeroFinalTAG').AsString) +
                                    ', para el almac�n ' + IntToStr(Self.CodigoAlmacenOrigen) + '-' + Trim(FNombreAlmacenOrigen) + ' y Categor�a ' +  cdsTAGs.FieldByName('CategoriaTAGs').AsString +
                                    ', hacia el almac�n ' + IntToStr(aAlmacenDestino) + '-' + Trim(NombreAlmacenDestino) + '.' + #13#10 + #13#10; // Rev. 3 (SS 902)
                        cdsTAGs.Next;
                    except
                        on e: Exception do begin
                            Screen.Cursor := crDefault;

							DMConnections.BaseCAC.RollbackTrans;
                            MsgBoxErr(MSG_ERROR_MOVIMIENTO_TELEVIAS, e.message, MSG_CAPTION_MOVIMIENTOS_STOCK, MB_ICONSTOP);

                            Exit;
                        end;
                    end;
                end;
                TodoOk := True;
            end
            else begin

                cdsEntrega.DisableControls;
                cdsEntrega.First;

                while not cdsEntrega.Eof do begin
                    try
                        CantidadNoNominada := cdsEntrega.FieldByName('CantidadTags').AsInteger;
                        if (cdsEntrega.RecordCount > 0) then begin
                            if vleIngresados.FindRow(Trim(cdsEntrega.FieldByName('Categoria').AsString), idxCategoria) then begin
                                CantidadNoNominada := CantidadNoNominada  - strtoint(vleIngresados.Cells[1, idxCategoria]);
                            end;
                        end;

                        with spRegistrarMovimientoStock, Parameters do begin
                            Close;

                            if CantidadNoNominada <> 0 then begin
                                ParamByName('@CodigoOperacion').Value 			:= CodigoOperacion;
                                ParamByName('@TipoMovimiento').Value 			:= CONST_MOVIMIENTO_NO_NOMINADO;
                                ParamByName('@CodigoEmbalaje').Value 			:= NULL;

                                ParamByName('@NumeroInicialTAG').Value 			:= NULL;
                                ParamByName('@NumeroFinalTAG').Value 			:= NULL;

                                ParamByName('@CategoriaTAG').Value 				:= cdsEntrega.FieldByName('Categoria').AsInteger;
                                ParamByName('@Cantidad').Value 					:= CantidadNoNominada;
                                ParamByName('@EstadoControl').Value 			:= NULL;
                                ParamByName('@EstadoSituacion').Value 			:= NULL;

                                ParamByName('@CodigoAlmacenOrigen').Value 		:= self.CodigoAlmacenOrigen;
                                ParamByName('@CodigoAlmacenDestino').Value 		:= aAlmacenDestino;

                                ParamByName('@GuiaDespacho').Value 				:= iif(aGuiaDespacho = -1, null, aGuiaDespacho);
                                ParamByName('@Usuario').Value 					:= UsuarioSistema;

                                ParamByName('@FechaHoraEvento').Value 			:= NowBase(DMConnections.BaseCAC);
                                ParamByName('@MotivoEntrega').Value 			:= null;
                                ParamByName('@ConstanciaCarabinero').Value 		:= null;
                                ParamByName('@FechaHoraConstancia').Value 		:= null;
                                ParamByName('@DescripcionMotivoRegistro').Value := null;
                                ParamByName('@CodigoComunicacion').Value 		:= null;

                                ExecProc
                            end;
                        end;
                    except
                        on e: Exception do begin
                            Screen.Cursor := crDefault;

							DMConnections.BaseCAC.RollbackTrans;
                            MsgBoxErr(MSG_ERROR_MOVIMIENTO_TELEVIAS, e.message, MSG_CAPTION_MOVIMIENTOS_STOCK, MB_ICONSTOP);

                            Exit;
                        end;
                    end;
                    //REV. 2
                    TextoMensaje := TextoMensaje + 'Se ha movido la Cantidad de  ' + cdsTAGs.FieldByName('CantidadTAGs').AsString +  ' Telev�as disponibles entre el rango especificado, desde el Telev�a ' +  trim(cdsTAGs.FieldByName('NumeroInicialTAG').AsString) + ', hasta el Telev�a ' + trim(cdsTAGs.FieldByName('NumeroFinalTAG').AsString) +
                                    ', para el almac�n ' + IntToStr(Self.CodigoAlmacenOrigen) + '-' + Trim(FNombreAlmacenOrigen) + ' y Categor�a ' +  cdsTAGs.FieldByName('CategoriaTAGs').AsString +
                                    ', hacia el almac�n ' + IntToStr(aAlmacenDestino) + '-' + Trim(NombreAlmacenDestino) + '.' + #13#10 + #13#10; // Rev. 3 (SS 902)
                    cdsEntrega.Next;
                end;

                TodoOk := True;
            end;
    finally
        Screen.Cursor := crDefault;
        Application.ProcessMessages;
        result := TodoOk;
        cdsTAGs.EnableControls;
        cdsEntrega.EnableControls;
        if TodoOk then begin
            DMConnections.BaseCAC.CommitTrans;
//	        cdsTAGs.EmptyDataSet;
            //REV. 2
            ShowMsgBoxCN(
                        TITULO_MOVIMIENTO_TELEVIA,
                        TextoMensaje,
                        MB_ICONINFORMATION,
                        Self);
        end
    end;
end;

function TframeMovimientoStock.Inicializar(
  aCodigoAlmacen: Integer): boolean;
begin
    FNoAgregar:= False;
    result := true;
    FCargando := true;
    try

        cdsTAGs.Close;
        cdsTAGs.CreateDataSet;
        cdsTAGs.Open;

        cdsEntrega.Close;
        cdsEntrega.CreateDataSet;
        cdsEntrega.EmptyDataSet;
        cdsEntrega.open;
        with qryCategorias do begin
            close;
            open;
            while not eof do begin
            	{INICIO 	: CFU 20161027 TASK_057_CFU_20161017-Gesti�n_TAGs_Doble_Categoria
                cdsEntrega.AppendRecord([fieldByName('Descripcion').AsString,
                  0, 0, fieldByName('Categoria').AsInteger]);
                }
                cdsEntrega.AppendRecord([FieldByName('Descripcion').AsString,
            							 FieldByName('DscCategoriaUrbana').AsString,
                  						 0, 0, FieldByName('Categoria').AsInteger,
                                         FieldByName('CategoriaUrbana').AsInteger]);
            	//TERMINO 	: CFU 20161027 TASK_057_CFU_20161017-Gesti�n_TAGs_Doble_Categoria
                next;
            end;
        end;
        CodigoAlmacenOrigen := aCodigoAlmacen;

    cdsEntrega.First;
    except
    	{INICIO 	: CFU 20161027 TASK_057_CFU_20161017-Gesti�n_TAGs_Doble_Categoria
        result := false;
        }
        on E:Exception do begin
            MessageDlg('Error al obtener los datos' +#13+ E.Message, mtError, [mbOK], 0);
            Result := False;
        end;
        //TERMINO 	: CFU 20161027 TASK_057_CFU_20161017-Gesti�n_TAGs_Doble_Categoria
    end;
    FCargando := False;

end;

{
    Revision : 1
        Author: pdominguez
        Date: 30/06/2010
        Description: SS 900 - Error cambio almacen m�dulo Gesti�n Telev�a
            - Se almacena el nombre del Almac�n de Origen, para mostrarlo en los posibles errores
            de Telev�as disponibles que puedan surgir.
}
procedure TframeMovimientoStock.setCodigoAlmacenOrigen(const Value: Integer);
//INICIO	: 20160920 CFU
var
	i: Integer;
//TERMINO	: 20160920 CFU
begin
    FCodigoAlmacenOrigen := Value;
    FNombreAlmacenOrigen := Trim(QueryGetValue(DMConnections.BaseCAC, Format('SELECT Descripcion FROM MaestroAlmacenes WITH (NOLOCK) WHERE CodigoAlmacen = %d', [FCodigoAlmacenOrigen]))); // Rev. 1 (SS 900)
    FAlmacenNominado := Trim(QueryGetValue(DMConnections.BaseCAC, 'Select CASE SoloNominado WHEN 1 THEN ''1'' ELSE ''0'' END  from MaestroAlmacenes  WITH (NOLOCK) where CodigoAlmacen = ' + inttostr(FCodigoAlmacenOrigen))) = '1';
    cdsEntrega.DisableControls;
    with ObtenerSaldosTAGsAlmacen do begin
        close;
		//INICIO	: 20160920 CFU
        cdsEntrega.First;
        for i := 0 to cdsEntrega.RecordCount - 1 do begin
        	cdsEntrega.Edit;
            cdsEntrega.FieldByName('CantidadTAGsDisponibles').AsInteger	:= 0;
            cdsEntrega.FieldByName('CantidadTAGs').AsInteger 			:= 0;
            cdsEntrega.Post;
        	cdsEntrega.Next;
        end;
        dsEntrega.DataSet.Open;
        Parameters.Refresh;
        //TERMINO	: 20160920 CFU
        Parameters.paramByname('@CodigoAlmacen').value := Value;
        open;
        while not eof do begin
            cdsEntrega.locate('Categoria', VarArrayOf([fieldByName('Categoria').asInteger]), []);
            cdsEntrega.Edit;
            cdsEntrega.fieldByName('CantidadTAGsDisponibles').AsInteger := fieldByName('Saldo').AsInteger;
            cdsEntrega.fieldByName('CantidadTAGs').AsInteger := 0;
			vleRecibidos.InsertRow (fieldByName('Categoria').asString, '0', true);
            cdsEntrega.Post;
            next;
        end;
        cdsEntrega.first;
    end;
    cdsEntrega.EnableControls;
end;

function TframeMovimientoStock.ValidarCantidadTAGs: Boolean;
var
	idxCategoria: Integer;
begin
	result := true;

    cdsEntrega.DisableControls;
    cdsEntrega.First;

    for idxCategoria := 1 to vleRecibidos.RowCount-1 do begin
    	case FAlmacenNominado of
    		true: result := result and (StrToInt(vleRecibidos.Cells[1, idxCategoria]) = StrToInt (vleIngresados.Cells[1, idxCategoria]));
//            else result := result and (StrToInt(vleRecibidos.Cells[1, idxCategoria]) >= StrToInt (vleIngresados.Cells[1, idxCategoria]));
            else result := result and ((StrToInt (vleIngresados.Cells[1, idxCategoria])=0) And (cdsEntrega.FieldByName('CantidadTags').AsInteger <= cdsEntrega.FieldByName('CantidadTagsDisponibles').AsInteger)) or
										((StrToInt(vleRecibidos.Cells[1, idxCategoria]) = StrToInt (vleIngresados.Cells[1, idxCategoria])) And (cdsEntrega.FieldByName('CantidadTags').AsInteger <= cdsEntrega.FieldByName('CantidadTagsDisponibles').AsInteger))
        end;
	    if not result then begin
            MsgBox(iif(FAlmacenNominado, MSG_ERROR_TOTAL_INGRESADO_NOMINADO, Format (MSG_ERROR_TOTAL_INGRESADO,[IntToStr(idxCategoria)])), self.Caption, MB_OK + MB_ICONSTOP);
            cdsEntrega.EnableControls;

            Exit;
        end;
        cdsEntrega.Next;
    end;
    cdsEntrega.EnableControls;
end;

function TframeMovimientoStock.ValidarTodosCeros: boolean;
var
    TodoCero: boolean;
begin
    cdsEntrega.DisableControls;
    cdsEntrega.First;
    TodoCero:= not (cdsEntrega.FieldByName('CantidadTAGs').AsInteger > 0);
    while not cdsEntrega.Eof do begin
        TodoCero := (TodoCero and (not (cdsEntrega.FieldByName('CantidadTAGs').AsInteger > 0)));
        cdsEntrega.Next;
    end;
    cdsEntrega.EnableControls;
    result := not TodoCero;
    if TodoCero then begin
        MsgBoxBalloon('Debe asignar alguna cantidad.', self.Caption, MB_ICONSTOP, dbgRecibirTags);
        Exit;
    end;
end;

{
    Revision : 1
        Author: pdominguez
        Date: 30/06/2010
        Description: SS 900 - Error cambio almacen m�dulo Gesti�n Telev�a
            - Se clarifica el mensaje de error en caso de no coincidir la cantidad de telev�as indicados,
            con la cantidad de Telev�as disponibles en el sistema.
}
function TframeMovimientoStock.ValidarCantidadTAGsEnLinea(TAGInicial, TAGFinal: DWORD; CantidadTAGS, Categoria: Integer): Boolean;
    resourcestring
        rsTituloValidacionTelevias = 'Validaci�n Telev�as';
var
	errCantidadTAGs,
    errCantidadTAGsCategoria: boolean;
begin
   	result := CantidadTAGs > 0;
    if not result then begin
    	MsgBox(MSG_ERROR_CANTIDAD_ES_CERO, MSG_CAPTION_MOVIMIENTOS_STOCK, MB_OK + MB_ICONSTOP);
        Exit
    end;

    with spObtenerDatosRangoTAGsAlmacen, Parameters do begin
        Close;
        ParamByName('@TAGInicial').Value 		:= TAGInicial;
        ParamByName('@TAGFinal').Value  		:= TAGFinal;
        ParamByName('@CategoriaTAG').Value		:= Categoria;
        ParamByName('@CodigoAlmacen').Value     := FCodigoAlmacenOrigen;
        Open;

        if FAlmacenNominado then begin
			errCantidadTAGs 		:= spObtenerDatosRangoTAGsAlmacen.FieldByName('CantidadTAGsMaestro').AsInteger <> CantidadTAGS;
    		errCantidadTAGsCategoria:= spObtenerDatosRangoTAGsAlmacen.FieldByName('CantidadTAGsCategoriaMaestro').AsInteger <> CantidadTAGS;
        end else begin
			errCantidadTAGs 		:= FieldByName('CantidadTAGs').AsInteger <> CantidadTAGS;
    		errCantidadTAGsCategoria:= FieldByName('CantidadTAGsCategoria').AsInteger <> CantidadTAGS
        end;
       	result := not (errCantidadTAGs or errCantidadTAGsCategoria);

        if not result then begin
        	case FAlmacenNominado of
                True:
                    ShowMsgBoxCN(
                        rsTituloValidacionTelevias,
                        Format(
                            MSG_ERROR_CANTIDAD_TAGS_PARAMETROS,
                            [
                                cdsTAGs.FieldByName('NumeroInicialTAG').AsString,
                                cdsTAGs.FieldByName('NumeroFinalTAG').AsString,
                                IntToStr(FCodigoAlmacenOrigen),
                                FNombreAlmacenOrigen,
                                Categoria,
                                FieldByName('CantidadTAGsCategoriaMaestro').AsInteger]),
                        MB_ICONERROR,
                        Self);
                False:
                    ShowMsgBoxCN(
                        rsTituloValidacionTelevias,
                        Format(
                            MSG_ERROR_CANTIDAD_TAGS_PARAMETROS,
                            [
                                cdsTAGs.FieldByName('NumeroInicialTAG').AsString,
                                cdsTAGs.FieldByName('NumeroFinalTAG').AsString,
                                IntToStr(FCodigoAlmacenOrigen),
                                FNombreAlmacenOrigen,
                                Categoria,
                                FieldByName('CantidadTAGsCategoria').AsInteger]),
                        MB_ICONERROR,
                        Self);
            end;
        end;
        
        Close;
    end;
end;

function TframeMovimientoStock.ValidarCategoria(
  Categoria: ANSIString): Boolean;
var
	idxCategoria: Integer;
begin
    result := vleRecibidos.FindRow(Categoria, idxCategoria);
    if not result then MsgBox(MSG_ERROR_CATEGORIA_FUERA_DE_LISTA, MSG_CAPTION_MOVIMIENTOS_STOCK, MB_OK + MB_ICONSTOP);
end;

function TframeMovimientoStock.ValidarMovimiento(aAlmacenDestino: Integer): Boolean;
begin
	if cdsTAGs.State in [dsEdit, dsInsert] then cdsTAGs.Post;
    if cdsEntrega.State in [dsEdit, dsInsert] then cdsEntrega.Post;

    FAlmacenNominado := (Trim(QueryGetValue(DMConnections.BaseCAC, 'Select CASE SoloNominado WHEN 1 THEN ''1'' ELSE ''0'' END  from MaestroAlmacenes  WITH (NOLOCK) where CodigoAlmacen = ' + inttostr(FCodigoAlmacenOrigen))) = '1')
                        or (Trim(QueryGetValue(DMConnections.BaseCAC, 'Select CASE SoloNominado WHEN 1 THEN ''1'' ELSE ''0'' END  from MaestroAlmacenes  WITH (NOLOCK) where CodigoAlmacen = ' + inttostr(aAlmacenDestino))) = '1');

//    FAlmacenNominado := FAlmacenNominado or (Trim(QueryGetValue(DMConnections.BaseCAC, 'Select CASE SoloNominado WHEN 1 THEN ''1'' ELSE ''0'' END  from MaestroAlmacenes where CodigoAlmacen = ' + inttostr(aAlmacenDestino))) = '1');
    CalcularTotalesIngresados;
    result := ValidarCantidadTAGs and ValidarRangosSuperpuestos(cdsTAGs, true) and ValidarTodosCeros
end;

function TframeMovimientoStock.ValidarRangosSuperpuestos(
  CdsRangos: TClientDataSet; ConMsg: Boolean): Boolean;
var
	Actual: Integer;
    bm: TBookmark;
    NumeroInicialTAG,
    NumeroFinalTAG: DWORD;
begin
    result := true;
    cursor := crHourGlass;
    cdsRangos.disableControls;
    try
        with cdsRangos do begin
            first;
            while not eof do begin
                // Tomamos el registro actual para no verificar
                Actual := RecNo;
                NumeroInicialTAG := EtiquetaToSerialNumber(FieldByName('NumeroInicialTAG').asString);
                NumeroFinalTAG := EtiquetaToSerialNumber(FieldByName('NumeroFinalTAG').asString);
                bm := GetBookMark;
                try
                    // Recorremos el datase verificando el n�mero
                    first;
                    while not eof do begin
                        if Actual = RecNo then
                            next
                        else begin
                            if ((NumeroInicialTAG >= EtiquetaToSerialNumber(FieldByName('NumeroInicialTAG').asString)) and
                              (NumeroInicialTAG <= EtiquetaToSerialNumber(FieldByName('NumeroFinalTAG').asString))) then begin
                                result := false;
                                break;
                            end;

                            if ((NumeroFinalTAG >= EtiquetaToSerialNumber(FieldByName('NumeroInicialTAG').asString)) and
                              (NumeroFinalTAG <= EtiquetaToSerialNumber(FieldByName('NumeroFinalTAG').asString))) then begin
                                result := false;
                                break;
                            end;
                        next;
                        end;
                    end;
                    GotoBookMark(bm);
                    next;
                finally
                    FreeBookMark(bm);
                end;
            end;
        end;
    finally
        cdsRangos.EnableControls;
        cursor := crDefault;
    end;

	if ConMsg and (not result) then MsgBox(MSG_ERROR_RANGO_SUPERPUESTO, MSG_CAPTION_MOVIMIENTOS_STOCK, MB_OK + MB_ICONSTOP);
end;

procedure TframeMovimientoStock.dbgRecibirTagsKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    dbgRecibirTags.Options := dbgRecibirTags.Options + [dgEditing];
    if (ssCtrl in Shift) and (key = VK_DELETE) then key := 0;
    if (key = VK_INSERT) then key := 0;
    if (key = VK_DOWN) or (key = VK_TAB) then begin
        dbgRecibirTags.Options := dbgRecibirTags.Options - [dgEditing];
    end;
end;

procedure TframeMovimientoStock.Limpiar;
begin
    cdsTAGs.EmptyDataSet;
end;

{
    Procedure Name: ActualizaControlesEntradaDatos
    Parameters    : EntradaDatosHabilitada: Boolean

    Author       : pdominguez
    Date Created : 29/06/2010
    Description  : SS 900 - Error cambio almacen m�dulo Gesti�n Telev�a
        - Habilita o Deshabilita los controles del Frame en funci�n de si
        est� o no el panel de entrada de Datos habilitado.
}
procedure TframeMovimientoStock.ActualizaControlesEntradaDatos(EntradaDatosHabilitada: Boolean);
begin
    dbgRecibirTags.Enabled  := Not EntradaDatosHabilitada;
    dbgTags.Enabled         := Not EntradaDatosHabilitada;
    pnlEntradaDatos.Visible := EntradaDatosHabilitada;

    dbgRecibirTags.Refresh;
    dbgTags.Refresh;
end;

{
    Procedure Name: ActualizarTotales
    Parameters    : None

    Author       : pdominguez
    Date Created : 30/06/2010
    Description  : SS 900 - Error cambio almacen m�dulo Gesti�n Telev�a
        - Actualiza los totales del campo CantidadTAGs del objeto cdsEntrega.
}
procedure TframeMovimientoStock.ActualizarTotales;
    var
        lPunteroTAGs: TBookmark;
begin
    try
        cdsTAGs.DisableControls;
        cdsEntrega.DisableControls;
        
        dsTAGs.DataSet := Nil;
        dsEntrega.DataSet := Nil;

        lPunteroTAGs := cdsTAGs.GetBookmark;

        with cdsEntrega do begin
            First;
            while not Eof do begin
                Edit;
                FieldByName('CantidadTAGs').AsInteger := 0;
                Post;
                Next;
            end;
        end;

        with cdsTAGs do begin
            First;
            while not eof do begin
                if cdsEntrega.Locate('Categoria', FieldByName('CategoriaTAGs').AsInteger,[]) then begin
                    cdsEntrega.Edit;
                    cdsEntrega.FieldByName('CantidadTAGs').AsInteger := 
                        cdsEntrega.FieldByName('CantidadTAGs').AsInteger + FieldByName('CantidadTAGs').AsInteger;
                    cdsEntrega.Post;
                end;
                Next;
            end;
        end;
        
    finally
        cdsTAGs.GotoBookmark(lPunteroTAGs);
        cdsTAGs.FreeBookmark(lPunteroTAGs);
        
        dsEntrega.DataSet := cdsEntrega;
        dsTAGs.DataSet := cdsTAGs;
        
        cdsEntrega.EnableControls;
        cdsTAGs.EnableControls;
    end;
end;

end.



