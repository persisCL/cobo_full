unit freListVehiculos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, DPSControls, ListBoxEx, DBListEx, DB, DBClient, ADODB, Provider,
  FrmDatosVehiculo,UtilProc,Util,RStrings,Variants;

type
  TFrameListVehiculos = class(TFrame)
    dblVehiculos: TDBListEx;
    btnIngresarVehiculo: TDPSButton;
    btnEditarVehiculo: TDPSButton;
    btnEliminarVehiculo: TDPSButton;
    ObtenerOrdenServicioAdhesionCuentas: TADOStoredProc;
    cdsOrdenesServicioAdhesionCuentas: TClientDataSet;
    dsOrdenServicio: TDataSource;
    cdsOrdenesServicioAdhesionCuentasBorradas: TClientDataSet;
    dspOrdenesServicioAdhesionCuentas: TDataSetProvider;
    cdsOrdenesServicioAdhesionCuentasCodigoOrdenServicio: TIntegerField;
    cdsOrdenesServicioAdhesionCuentasIndiceCuentas: TSmallintField;
    cdsOrdenesServicioAdhesionCuentasTipoPatente: TStringField;
    cdsOrdenesServicioAdhesionCuentasPatente: TStringField;
    cdsOrdenesServicioAdhesionCuentasCodigoMarca: TIntegerField;
    cdsOrdenesServicioAdhesionCuentasDescripcionMarca: TStringField;
    cdsOrdenesServicioAdhesionCuentasCodigoModelo: TIntegerField;
    cdsOrdenesServicioAdhesionCuentasDescripcionModelo: TStringField;
    cdsOrdenesServicioAdhesionCuentasCodigoColor: TIntegerField;
    cdsOrdenesServicioAdhesionCuentasDescripcionColor: TStringField;
    cdsOrdenesServicioAdhesionCuentasDetalleVehiculoSimple: TStringField;
    cdsOrdenesServicioAdhesionCuentasDetalleVehiculoCompleto: TStringField;
    cdsOrdenesServicioAdhesionCuentasTipoAdhesion: TSmallintField;
    cdsOrdenesServicioAdhesionCuentasDetallePasadas: TBooleanField;
    cdsOrdenesServicioAdhesionCuentasAnioVehiculo: TSmallintField;
    cdsOrdenesServicioAdhesionCuentasTieneAcoplado: TBooleanField;
    cdsOrdenesServicioAdhesionCuentasCategoria: TSmallintField;
    cdsOrdenesServicioAdhesionCuentasDescripcionCategoria: TStringField;
    cdsOrdenesServicioAdhesionCuentasLargoAdicionalVehiculo: TStringField;
    cdsOrdenesServicioAdhesionCuentasCodigoTipoVehiculo: TSmallintField;
    cdsOrdenesServicioAdhesionCuentasBorradasCodigoOrdenServicio: TIntegerField;
    cdsOrdenesServicioAdhesionCuentasBorradasIndiceCuentas: TSmallintField;
    cdsOrdenesServicioAdhesionCuentasBorradasTipoPatente: TStringField;
    cdsOrdenesServicioAdhesionCuentasBorradasPatente: TStringField;
    cdsOrdenesServicioAdhesionCuentasBorradasCodigoMarca: TIntegerField;
    cdsOrdenesServicioAdhesionCuentasBorradasDescripcionMarca: TStringField;
    cdsOrdenesServicioAdhesionCuentasBorradasCodigoModelo: TIntegerField;
    cdsOrdenesServicioAdhesionCuentasBorradasDescripcionModelo: TStringField;
    cdsOrdenesServicioAdhesionCuentasBorradasCodigoColor: TIntegerField;
    cdsOrdenesServicioAdhesionCuentasBorradasDescripcionColor: TStringField;
    cdsOrdenesServicioAdhesionCuentasBorradasDetalleVehiculoSimple: TStringField;
    cdsOrdenesServicioAdhesionCuentasBorradasDetalleVehiculoCompleto: TStringField;
    cdsOrdenesServicioAdhesionCuentasBorradasTipoAdhesion: TSmallintField;
    cdsOrdenesServicioAdhesionCuentasBorradasDetallePasadas: TBooleanField;
    cdsOrdenesServicioAdhesionCuentasBorradasAnioVehiculo: TSmallintField;
    cdsOrdenesServicioAdhesionCuentasBorradasTieneAcoplado: TBooleanField;
    cdsOrdenesServicioAdhesionCuentasBorradasCategoria: TSmallintField;
    cdsOrdenesServicioAdhesionCuentasBorradasDescripcionCategoria: TStringField;
    cdsOrdenesServicioAdhesionCuentasBorradasLargoAdicionalVehiculo: TStringField;
    cdsOrdenesServicioAdhesionCuentasBorradasCodigoTipoVehiculo: TSmallintField;
    procedure cdsOrdenesServicioAdhesionCuentasAfterOpen(
      DataSet: TDataSet);
    procedure btnIngresarVehiculoClick(Sender: TObject);
    procedure btnEditarVehiculoClick(Sender: TObject);
    procedure btnEliminarVehiculoClick(Sender: TObject);
    procedure dblVehiculosDblClick(Sender: TObject);
  private
    { Private declarations }
    function VehiculoCargado(Patente:String):Boolean;
    function GetVehiculosCargados:TClientDataSet;
    function GetTieneVehiculosCargados:Boolean;
    function GetVehiculosBorrados:TClientDataSet;
    function GetTieneVehiculosBorrados:Boolean;

  public
    { Public declarations }
    property VehiculosCargados: TClientDataSet read GetVehiculosCargados;
    property TieneVehiculosCargados:Boolean read GetTieneVehiculosCargados;
    property VehiculosBorrados:TClientDataSet read GetVehiculosBorrados;
    property TieneVehiculosBorrados:Boolean read GetTieneVehiculosBorrados;


    function inicializar(CodigoOrdenServicio:Integer=-1;IndiceCuentas:Integer=-1):boolean;
    procedure LimpiarCampos;
    function CargarVehiculos(CodigoOrdenServicio:Integer;IndiceCuentas:Integer=-1):Boolean;

  end;

implementation

{$R *.dfm}

{ TFrameListVehiculos }

function TFrameListVehiculos.inicializar(CodigoOrdenServicio,
  IndiceCuentas: Integer): boolean;
begin
    LimpiarCampos;
    result:=true;
end;

procedure TFrameListVehiculos.LimpiarCampos;
begin
    cdsOrdenesServicioAdhesionCuentas.close;
    cdsOrdenesServicioAdhesionCuentas.CreateDataSet;
    cdsOrdenesServicioAdhesionCuentasBorradas.close;
    cdsOrdenesServicioAdhesionCuentasBorradas.CreateDataSet;
end;

procedure TFrameListVehiculos.cdsOrdenesServicioAdhesionCuentasAfterOpen(
  DataSet: TDataSet);
begin
    btnEditarVehiculo.Enabled:=(cdsOrdenesServicioAdhesionCuentas.Active) and (cdsOrdenesServicioAdhesionCuentas.RecordCount>0);
    btnEliminarVehiculo.Enabled:=btnEditarVehiculo.Enabled;
end;

procedure TFrameListVehiculos.btnIngresarVehiculoClick(Sender: TObject);
var
    f : TFormDatosVehiculo;
begin
    Application.CreateForm(TFormDatosVehiculo, f);
    try
        if f.Inicializar() and (f.ShowModal = mrOK) then begin
            //Cargo el Vehiculo en la Lista.
            try
                if VehiculoCargado(f.Patente) then MsgBox(MSG_ERROR_VEHICULO_EXISTENTE_SOLIC,format(MSG_CAPTION_GESTION,[FLD_VEHICULOS]),MB_ICONSTOP)
                else begin
                    with cdsOrdenesServicioAdhesionCuentas do begin
                        Append;

                        FieldByName('CodigoOrdenServicio').AsInteger:=-1;
                        FieldByName('IndiceCuentas').AsInteger:=-1;
                        FieldByName('TipoPatente').AsString:=f.CodigoTipoPatente;
                        FieldByName('Patente').AsString:=f.Patente;
                        FieldByName('CodigoMarca').AsInteger:=f.Marca;
                        FieldByName('DescripcionMarca').AsString:=f.DescripcionMarca;
                        FieldByName('CodigoModelo').AsInteger:=f.Modelo;
                        FieldByName('DescripcionModelo').AsString:=f.DescripcionModelo;
                        FieldByName('CodigoColor').AsInteger:=f.Color;
                        FieldByName('DescripcionColor').AsString:=f.DescripcionColor;
                        FieldByName('DetalleVehiculoSimple').AsString:=f.DescripcionDatosVehiculoSimple;
                        FieldByName('DetalleVehiculoCompleto').AsString:=f.DetalleVehiculoCompleto;
                        FieldByName('TipoAdhesion').AsInteger:=f.TipoAdhesion;
                        FieldByName('DetallePasadas').AsBoolean:=f.FacturacionDetallada;
                        FieldByName('AnioVehiculo').AsInteger:=f.Anio;
                        FieldByName('TieneAcoplado').AsBoolean:=f.TieneAcoplado;
                        FieldByName('Categoria').AsInteger:=f.Categoria;
                        FieldByName('DescripcionCategoria').AsString:=f.DescripcionCategoria;
                        FieldByName('LargoAdicionalVehiculo').AsInteger:=f.LargoAdicionalVehiculo;
                        FieldByName('CodigoTipoVehiculo').AsInteger:=f.CodigoTipoVehiculo;

                        Post;
                    end;
                end;
            except
                On E: Exception do begin
                    MsgBoxErr( format(MSG_CAPTION_AGREGAR,[FLD_VEHICULOS]), E.message, format(MSG_CAPTION_GESTION,[FLD_VEHICULOS]), MB_ICONSTOP);
                    dsOrdenServicio.DataSet.Cancel;
                end;
            end;
        end;
    finally
        f.Release;
        dblVehiculos.SetFocus;
    end;
end;

function TFrameListVehiculos.VehiculoCargado(Patente: String): Boolean;
begin
    result:=False;
    with cdsOrdenesServicioAdhesionCuentas do begin
        First;
        while not(eof) do begin
            if trim(FieldByName('Patente').AsString)=Trim(Patente) then result:=True;
            next;
        end;
        first;
    end;
end;

procedure TFrameListVehiculos.btnEditarVehiculoClick(Sender: TObject);
var
    f : TFormDatosVehiculo;
begin
    Application.CreateForm(TFormDatosVehiculo, f);
    try
        with cdsOrdenesServicioAdhesionCuentas do begin
            if f.Inicializar(FieldByName('TipoPatente').AsString,
                            FieldByName('Patente').AsString,
                            FieldByName('CodigoMarca').AsInteger,
                            FieldByName('CodigoModelo').AsInteger,
                            FieldByName('DescripcionModelo').AsString,
                            FieldByName('AnioVehiculo').AsInteger,
                            FieldByName('CodigoColor').AsInteger,
                            FieldByName('TieneAcoplado').AsBoolean,
                            FieldByName('CodigoTipoVehiculo').AsInteger,
                            FieldByName('LargoAdicionalVehiculo').AsInteger,
                            false,
                            -1,
                            FieldByName('TipoAdhesion').AsInteger
            ) and (f.ShowModal = mrOK) then begin
                try
                    edit;

                    FieldByName('CodigoOrdenServicio').AsInteger:=-1;
                    FieldByName('IndiceCuentas').AsInteger:=-1;
                    FieldByName('TipoPatente').AsString:=f.CodigoTipoPatente;
                    FieldByName('Patente').AsString:=f.Patente;
                    FieldByName('CodigoMarca').AsInteger:=f.Marca;
                    FieldByName('DescripcionMarca').AsString:=f.DescripcionMarca;
                    FieldByName('CodigoModelo').AsInteger:=f.Modelo;
                    FieldByName('DescripcionModelo').AsString:=f.DescripcionModelo;
                    FieldByName('CodigoColor').AsInteger:=f.Color;
                    FieldByName('DescripcionColor').AsString:=f.DescripcionColor;
                    FieldByName('DetalleVehiculoSimple').AsString:=f.DescripcionDatosVehiculoSimple;
                    FieldByName('DetalleVehiculoCompleto').AsString:=f.DetalleVehiculoCompleto;
                    FieldByName('TipoAdhesion').AsInteger:=f.TipoAdhesion;
                    FieldByName('DetallePasadas').AsBoolean:=f.FacturacionDetallada;
                    FieldByName('AnioVehiculo').AsInteger:=f.Anio;
                    FieldByName('TieneAcoplado').AsBoolean:=f.TieneAcoplado;
                    FieldByName('Categoria').AsInteger:=f.Categoria;
                    FieldByName('DescripcionCategoria').AsString:=f.DescripcionCategoria;
                    FieldByName('LargoAdicionalVehiculo').AsInteger:=f.LargoAdicionalVehiculo;
                    FieldByName('CodigoTipoVehiculo').AsInteger:=f.CodigoTipoVehiculo;

                    Post;
                except
                    On E: Exception do begin
                        MsgBoxErr( format(MSG_CAPTION_ACTUALIZAR,[FLD_VEHICULOS]), E.message, format(MSG_CAPTION_GESTION,[FLD_VEHICULOS]), MB_ICONSTOP);
                        dsOrdenServicio.DataSet.Cancel;
                    end;
                end;
            end;
        end;
    finally
        f.Release;
        dblVehiculos.SetFocus;
    end;

end;

procedure TFrameListVehiculos.btnEliminarVehiculoClick(Sender: TObject);
begin
	Screen.Cursor   := crHourGlass;
	If MsgBox(Format(MSG_QUESTION_ELIMINAR,[FLD_VEHICULOS]), Format(MSG_CAPTION_ELIMINAR,[FLD_VEHICULOS]), MB_YESNO + MB_ICONQUESTION) = IDYES then begin
		try

            with cdsOrdenesServicioAdhesionCuentasBorradas do begin
                Append;
                FieldByName('CodigoOrdenServicio').AsInteger:=cdsOrdenesServicioAdhesionCuentas.FieldByName('CodigoOrdenServicio').AsInteger;
                FieldByName('IndiceCuentas').AsInteger:=cdsOrdenesServicioAdhesionCuentas.FieldByName('IndiceCuentas').AsInteger;
                FieldByName('TipoPatente').AsString:=cdsOrdenesServicioAdhesionCuentas.FieldByName('TipoPatente').AsString;
                FieldByName('Patente').AsString:=cdsOrdenesServicioAdhesionCuentas.FieldByName('Patente').AsString;
                FieldByName('CodigoMarca').AsInteger:=cdsOrdenesServicioAdhesionCuentas.FieldByName('CodigoMarca').AsInteger;
                FieldByName('DescripcionMarca').AsString:=cdsOrdenesServicioAdhesionCuentas.FieldByName('DescripcionMarca').AsString;
                FieldByName('CodigoModelo').AsInteger:=cdsOrdenesServicioAdhesionCuentas.FieldByName('CodigoModelo').AsInteger;
                FieldByName('DescripcionModelo').AsString:=cdsOrdenesServicioAdhesionCuentas.FieldByName('DescripcionModelo').AsString;
                FieldByName('CodigoColor').AsInteger:=cdsOrdenesServicioAdhesionCuentas.FieldByName('CodigoColor').AsInteger;
                FieldByName('DescripcionColor').AsString:=cdsOrdenesServicioAdhesionCuentas.FieldByName('DescripcionColor').AsString;
                FieldByName('DetalleVehiculoSimple').AsString:=cdsOrdenesServicioAdhesionCuentas.FieldByName('DetalleVehiculoSimple').AsString;
                FieldByName('DetalleVehiculoCompleto').AsString:=cdsOrdenesServicioAdhesionCuentas.FieldByName('DetalleVehiculoCompleto').AsString;
                FieldByName('TipoAdhesion').AsInteger:=cdsOrdenesServicioAdhesionCuentas.FieldByName('TipoAdhesion').AsInteger;
                FieldByName('DetallePasadas').AsBoolean:=cdsOrdenesServicioAdhesionCuentas.FieldByName('DetallePasadas').AsBoolean;
                FieldByName('AnioVehiculo').AsInteger:=cdsOrdenesServicioAdhesionCuentas.FieldByName('AnioVehiculo').AsInteger;
                FieldByName('TieneAcoplado').AsBoolean:=cdsOrdenesServicioAdhesionCuentas.FieldByName('TieneAcoplado').AsBoolean;
                FieldByName('Categoria').AsInteger:=cdsOrdenesServicioAdhesionCuentas.FieldByName('Categoria').AsInteger;
                FieldByName('DescripcionCategoria').AsString:=cdsOrdenesServicioAdhesionCuentas.FieldByName('DescripcionCategoria').AsString;
                FieldByName('LargoAdicionalVehiculo').AsInteger:=cdsOrdenesServicioAdhesionCuentas.FieldByName('LargoAdicionalVehiculo').AsInteger;
                FieldByName('CodigoTipoVehiculo').AsInteger:=cdsOrdenesServicioAdhesionCuentas.FieldByName('CodigoTipoVehiculo').AsInteger;

                Post;
            end;
			dsOrdenServicio.DataSet.Delete;
            dblVehiculos.SetFocus;
		Except
			On E: Exception do begin
				dsOrdenServicio.DataSet.Cancel;
                cdsOrdenesServicioAdhesionCuentasBorradas.Cancel;
				MsgBoxErr( Format(MSG_ERROR_ELIMINAR,[FLD_VEHICULOS]), e.message, Format(MSG_CAPTION_ELIMINAR,[FLD_VEHICULOS]), MB_ICONSTOP);
                dblVehiculos.SetFocus;
			end;
		end;
	end;
	Screen.Cursor   := crDefault;
end;

procedure TFrameListVehiculos.dblVehiculosDblClick(Sender: TObject);
begin
    if btnEditarVehiculo.Visible then btnEditarVehiculo.Click;
end;

function TFrameListVehiculos.GetTieneVehiculosBorrados: Boolean;
begin
    result:=cdsOrdenesServicioAdhesionCuentasBorradas.RecordCount>0;
end;

function TFrameListVehiculos.GetTieneVehiculosCargados: Boolean;
begin
    result:=cdsOrdenesServicioAdhesionCuentas.RecordCount>0;
end;

function TFrameListVehiculos.GetVehiculosBorrados: TClientDataSet;
begin
    result:=cdsOrdenesServicioAdhesionCuentasBorradas;
end;

function TFrameListVehiculos.GetVehiculosCargados: TClientDataSet;
begin
    result:=cdsOrdenesServicioAdhesionCuentas;
end;

function TFrameListVehiculos.CargarVehiculos(CodigoOrdenServicio,
  IndiceCuentas: Integer): Boolean;
begin
    try
        with ObtenerOrdenServicioAdhesionCuentas do begin
            close;
            Parameters.ParamByName('@CodigoOrdenServicio').Value:=CodigoOrdenServicio;
            Parameters.ParamByName('@IndiceCuentas').Value:=iif(IndiceCuentas<=0,NULL,IndiceCuentas);
            Open
        end;

        cdsOrdenesServicioAdhesionCuentas.Close;
        cdsOrdenesServicioAdhesionCuentas.CreateDataSet;
        cdsOrdenesServicioAdhesionCuentasBorradas.Close;
        cdsOrdenesServicioAdhesionCuentasBorradas.CreateDataSet;

        cdsOrdenesServicioAdhesionCuentas.Data := dspOrdenesServicioAdhesionCuentas.Data;
        cdsOrdenesServicioAdhesionCuentas.AfterOpen(cdsOrdenesServicioAdhesionCuentas);
        ObtenerOrdenServicioAdhesionCuentas.Close;
        result:=true;
    except
        on E: Exception do begin
            result:=false;
            MsgBoxErr(format(MSG_ERROR_INICIALIZACION,[FLD_VEHICULOS]), e.Message, format(MSG_CAPTION_LIST,[FLD_VEHICULOS]), MB_ICONSTOP);
        end;
    end;
end;

end.
