{-----------------------------------------------------------------------------
 File Name: RVMTypes.pas
 Author:    gcasais
 Date Created: 22/02/2005
 Language: ES-AR
 Description: Tipos de datos usados por el cliente RNVM
              Según la especificación del Registro Nacional de Vehículos
              Motorizados "HandBook de Servicios Electrónicos v1.2
              del 12/04/2005 incluyendo el AMO-COM"
-----------------------------------------------------------------------------}
unit RVMTypes;
interface

Type
    //Tipos de resultados posibles para una consulta
    TRVMQueryResult = (qrRNVMUnknown, qrRNVMIncomplete, qrRNVMOk);

    // Estructura de Configuración del Servicio
    TRNVMConfig = record
        ServicePort,
        RequestProxyServerPort: Integer;
        RequestUseProxy: Boolean;
        UserName,
        PassWord,
        ServiceID,
        ServiceURL,
        SaveDirectory,
        FileName,
        RequestProxyServer: String;
    end;

    // Pregunta al RNVM
    TRNVMQuery = record
        Parameters: TRNVMConfig;
        LicensePlate: String;
    end;

    //Tipos especiales
    TLicensePlate = String;
    TVehicleNotes = String;

    //Información de un vehículo
    TVehicleInformation = record
        Category,
        Manufacturer,
        Model,
        Color,
        EngineID,
        ChassisID,
        Serial,
        VINNumber,
        Year: String;
    end;

    //Información del seguro de un vehículo
    TInsuranceInformation = record
        Entity,
        ContractID,
        ExpirationDate: String;
    end;

    // Información del propietario
    TOwnerInformation = record
        OwnerID1,
        OwnerName1: String;
    end;

    //Información del domicilio
    TAddressInformation = record
        DoorNumber,
        Street,
        Letter,
        OtherInfo,
        Commune,
        Field6: String;
    end;

    // Informacion sobre los datos de la Registración del vehiculo en el Registro Civil
    TOwnerRegistration = record
        AcquisitionDate,
        Registration,
        RegistrationNumber,
        RegistrationDate: String;
    end;

    //Información de Restricciones del vehículo
    TDomainConstraints = record
        Notas: Array[1..11] of string;
    end;

    //Información sobre pedidos formales de información
    //del vehículo que figuran en el registro.
    TDomainRequests = record
        Nota1: String;
        Nota2: String;
    end;

    // Información sobre propietarios anteriores
    TOthersOwnersInformation = TOwnerInformation;

    // Información de propietarios tipo COM
    TAmoCOM = record
        RUT1,
        Nombre1,
        RUT2,
        Nombre2: String;
    end;

    // Estructura de respuesta a una consulta
    TRVMInformation = record
        RequestedLicensePlate: TLicensePlate;
        VehicleInformation: TVehicleInformation;
        InsuranceInformation: TInsuranceInformation;
        OwnerInformation: TOwnerInformation;
        OwnerInformationCOM: TAmoCom;
        AddressInformation: TAddressInformation;
        OwnerRegistration: TOwnerRegistration;
        VehicleNotes: TVehicleNotes;
        DomainConstraints: TDomainConstraints;
        DomainRequests: TDomainRequests;
        OthersOwnersInformation: TOthersOwnersInformation;
    end;

    //Datos para completar una infracción
    TOwnerData = record
        ID,
        FullName,
        Street,
        DoorNumber,
        Commune,
        Details: String;
    end;

    // Datos para una infracción
    TEnforcementData = record
        Owner: TOwnerData;
        Registration: TOwnerRegistration;
        Vehicle: TVehicleInformation;
    end;

implementation
end.
