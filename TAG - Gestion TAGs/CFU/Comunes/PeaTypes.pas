{-----------------------------------------------------------------------------
 Unit Name: PeaTypes
 Author:
 Purpose:
 History:                         


Historial de Revisiones
------------------------

Revision 1:
    Author      : ggomez
    Date        : 05/05/2005
    Description :
        - Agregu� el tipo TEximirFacturacion.
        - Agregu� al tipo TCuentaVehiculo un campo del tipo TEximirFacturacion.

Revision 2:
    Author      : ggomez
    Date        : 06/05/2005
    Description :
        - Agregu� el tipo TBonificarFacturacion.
        - Agregu� al tipo TCuentaVehiculo un campo del tipo TBonificarFacturacion.

Revision 3:
    Author : ggomez
    Date : 05/07/2006
    Description : Agregu� las constantes ACTION_LIST_PEDIDO_ENVIO y
        ACTION_LIST_CONFIRMACION_PEDIDO_ENVIO.

Revision 4:
    Author : ggomez
    Date : 05/07/2006
    Description : Agregu� la constante ACTION_LIST_CANCELACION_PEDIDO.


Revision 5:
    Author : ggomez
    Date : 12/07/2006
    Description : Agregu� la constante RO_MOD_INTERFAZ_ENTRADA_PAGOS_SERVIPAG
        que es el c�digo de M�dulo de la recepci�n de rendiciones  Servipag.

Revision 6:
    Author : ggomez
    Date : 13/07/2006
    Description : Agregu� la constante RO_MOD_INTERFAZ_ENTRADA_PAGOS_SANTANDER
        que es el c�digo de M�dulo de la recepci�n de rendiciones Santander.

Revision 7:
    Author : ggomez
    Date : 25/07/2006
    Description : Modifiqu� las constantes de los Tipos de hist�rico de Action
        list.

Revision 8:
    Author : ggomez
    Date : 09/08/2006
    Description : Agregu� la constante CONST_FORMA_PAGO_PAGARE, para indicar el
        C�digo de la forma de pago Pagar�.

Revision 9:
    Author : jconcheyro
    Date : 30/08/2006
    Description : Agregu� la constante COMPROBANTE_ANULADO

Revision 10:
    Author : jconcheyro
    Date : 14/12/2006
    Description : Agregu� la constante CONST_ALMACEN_ARRIENDO

Revision 11:
    Author : nefernandez
    Date : 26/07/2007
    Description : Agregu� la constante RO_MOD_INTERFAZ_ENTRANTE_ANULACIONES_SERVIPAG
        que es el c�digo de M�dulo de la recepci�n de anulaciones Servipag.
Revision : 13
Author :   Fsandi
Date Created: 13-09-2007
Language : ES-AR
Description : Se agrega la funcionalidad de recuperar un veh�culo robado al momento de dar de alta.

Revision : 14
Author :   lcanteros
Date Created: 14-03-2008
Language : ES-AR
Description : Se agrega el campo TipoCliente a  TDatosPersonales

Revision : 15
Author :   dAllegretti
Date Created: 01/08/2008
Language : ES-AR
Description : Se quit� del tipo TCuentaVehiculo  el campo FechaModifCuenta, ya que este campo es solo auditor�a

Revision 16
Author: mbecerra
Date: 31-Diciembre-2008
Description:	(REF SS 769)	Se agregan las constantes:
					* CONST_CONCEPTO_ARRIENDO_TAG_CUOTAS, correspondiente al concepto de arriendo en cuotas.
					* CONST_ALMACEN_ENTREGADO_EN_CUOTAS correspondiente al almacen de arriendo en cuotas.
					* CONST_STR_ALMACEN_ENTREGADO_EN_CUOTAS correspondiente a la glosa del arriendo en cuotas.

    Revision : 17
    Author : vpaszkowicz
    Date : 19/03/2009
    Description : Agrego una constante para representar la aclaraci�n de que el
    convenio es sin cuenta.


Revision : 18
    Author : rharris
    Date : 06/04/2009
    Description : se agregan constantes para comprobantes electr�nicos

Revision : 19
    Author : pdominguez
    Date   : 08/06/2009
    Description : SS 809
            - Se a�ade la constante CONST_CANAL_PAGO_TARJETA_VENTANILLA.

Revision : 29
    Author : pdominguez
    Date   : 15/09/2009
    Description : SS 826
        - Se a�adieron las siguientes constantes:
             CONST_ESTADO_TRANSITO_POSIBLE_INFRACTOR.
             CONST_ALARMA_MODULO_GESTION_INFRACCIONES
             CONST_ALARMA_MODULO_CAC_REVALIDACION
             CONST_ALARMA_SEVERIDAD_3

Revision : 30
    Author : pdominguez
    Date   : 16/09/2009
    Description : SS 809
        - Se a�adieron las constantes para implementar la definici�n de las
        pantallas dentro del Cobro de Comprobantes, desde CONST_PAGE_NO_DEFINIDO
        a CONST_PAGE_TARJETA_DEBITO y desde CONST_PAGE_DESC_EFECTIVO a
        CONST_PAGE_DESC_TARJETA_DEBITO.

Revision : 31
    Author : Nelson Droguett Sierra
    Date   : 16/09/2009
    Description : SS 784 ?
                Se agregan los nuevos tipos de comprobantes internos para
                la nota de credito a NQ y a NB, estos nuevos tipos son CQ y CB
                respectivamente.
Revision : 33
    Author : mpiazza
    Date   : 28/01/2010
    Description : SS-510: Se le agrega al record  TDatosTurno el dato IdEstacion

Revision : 34
    Author : Nelson Droguett Sierra
    Date   : 13-Abril-2010
    Description : SS-875 Se agrega Bit de HabilitadoAMB a cada cuenta de un convenio
Revision : 35
    Author : mpiazza
    Date   : 15/10/2009
    Description : SS 787
        - Se a�ade el parametro RecibeClavePorMail a TDatosPersonales


Revision : 37
Author :   jjofre
Date Created: 03/06/2010
Description : ref: 884 Se agrega el valor CONST_ORIGEN_MEDIO_PAGO_BANCOCHILE := 'B'

Revision : 38
Author :   jjofre
Date Created: 01/08/2010
Description : ref: 469 Se agrega el valor al record TdatosPersonales
                    InactivaDomicilioRNUT    : Boolean;
                    InactivaMedioComunicacionRNUT   : Boolean;

Revision	: 39
Author		: mbecerra
Date		: 17-Junio-2011
Description	:		(Ref SS 959)
            	Se agrega el comprobante de devoluci�n de dinero
Firma		: SS959-20110617-MBE

Firma		: SS_740_MBE_20110720
Description	: Se agregan las constantes TC_TRASPASO_CREDITO y TC_TRASPASO_DEBITO

Firma       : SS-1006-NDR-20111105
Description : Se implementa el bit HabilitadoPA para las listas de acceso de parque arauco

Firma       : SS_1015_HUR_20120413
Descripcion : Se agregan los tipos de comprobante Saldo Inicial OC (SI) y Anula Saldo Inicial (AS)

Firma       : SS_1015_HUR_20120417
Descripcion : Se modifican los tipo de comprobante de Saldo Inicial

Firma       : SS-1015-NDR-20120510
Descripcion : Se agregan los tipos de comprobante Cuotas Otras Concesionarias (OC)
            y Anula Cuotas Otras Concesionarias (AC)

Firma       : SS-1006-NDR-20120614
Descripcion : El Bit HabilitadoPA ahora se llama AdheridoPA

Firma       : SS-1006-NDR-20120715
Descripcion : El Bit AdheridoPA ahora tambien se asocia a la persona, ya que
            firma solo 1 contrato para todos los convenios :)

Firma       :   SS_660_CQU_20121010
Descripcion :   Se agegan las Constantes para los estados en lista amarilla
                todas comienzan con CONST_ESTADO_CONVENIO_LISTA_AMARILLA_

Firma       :   SS_1091_CQU_20130516
Descripcion :   Se aumenta la dimensi�n del Arreglo TListaPuntosCobro de 17 a 19

Firma       :   SS_660_CQU_20130711
Descripcion :   Se agegan las Constantes para los nuevos estados en lista amarilla
                todas comienzan con CONST_ESTADO_CONVENIO_LISTA_AMARILLA_

Firma       :   SS_1120_MVI_20130820
Descripcion :   Se agrega TXT_CONVENIO_DE_BAJA para identificar a los convenios de baja y poder
                pintarlos de rojo.
                Adem�s se modifica el valor de la variable TXT_CONVENIO_SIN_CUENTA, de
                '(Convenio Sin Cuenta)' a '(Sin Cuenta)'. Esto para utilizar menos espacio.

Firma       : SS_1130_MCA_20130917
Descripcion : Se cambia mensaje PROCESO LEGAL A PROCESO DE COBRANZA.

Autor       :   Claudio Quezada Ib��ez
Firma       :   SS_1158_CQU_20140109
Fecha       :   09-01-2014
Descripcion :   Se modifica el Arreglo TListaPuntosCobro, se deja como din�mico ya que CN est� agregando p�rticos.

Autor       :   Claudio Quezada Ib��ez
Firma       :   SS_1147_CQU_20140408
Fecha       :   15-04-2014
Descripcion :   Se modifica el Record TPuntosCobroCN por TPuntosCobro


Firma       : SS_1147_NDR_20140710
Descripcion : Se indican constantes para codigos fijos de Concesionaria de CN y VS

Firma       :   SS_1151_CQU_20140103
Fecha       :   03-01-2014
Descripcion :   Se agregan constantes con motivo de inhabilitacion por PAK

Firma       : SS_1307_NDR_20150408
Descripcion : Se agrega enumeracion con los posibles origenes de impresion de comprobantes

Firma       : SS_1374A_CQU_20151002
Descripcion : Se agrega la Constante CONST_ESTADO_TRANSITO_LIMBO = 2

Firma       : SS_1400_NDR_20151007
Descripcion : Se agrega la constante RO_MOD_INTERFAZ_ENTRANTE_RESPUESTAS_CONSULTAS_RNVM

Firma       :   SS_1409_CQU_20151102
descripcion :   Se modifica TPuntoCobro agregando X, Y, NumeroPuntoCobro ya que ahora se pintan los porticos y no una l�nea
                por lo tanto se necesita saber qu� PuntoCobro pintar.
                Las coordenadas X,Y de la imagen est�n en la tabla PuntosCobro.
                Se agrega CODIGO_AMB

Etiqueta    :   20160603 MGO
Descripci�n :   Se agrega propiedad Foraneo a TVehiculos

-----------------------------------------------------------------------------}

Unit PeaTypes;

interface

Uses
	Windows, Util, Graphics, Controls;

resourcestring

    //TIPO DOCUMENTO
    TIPO_DOCUMENTO_RUT = 'RUT';

    //Sistemas
    SYS_ADMINISTRACION_TITLE       = 'Sistema de Administraci�n';
    SYS_FACTURACION_TITLE          = 'Sistema de Facturaci�n';
    SYS_MONITOR_PUNTOS_COBRO_TITLE = 'Sistema de Puntos de cobros';
    SYS_GESTION_TAGS_TITLE         = 'Sistema de Gesti�n de TAGs';
    SYS_PERMISOS_AREA_TITLE        = '';
    SYS_CAC_TITLE                  = 'Sistema de Atenci�n al Cliente';
    SYS_CAC_AC_TITLE               = 'Sistema de Atenci�n al Cliente - AC';
    SYS_VALIDACION_TITLE           = '';
    SYS_COBRANZAS_TITLE            = '';
    SYS_WEB_TITLE                  = '';
    SYS_GESTION_INFORMES_TITLE     = 'Sistema de Gesti�n de Infomes';
    SYS_INTERFACES_TITLE           = 'Sistema de interfaces';
    SYS_MANTENIMIENTO_TITLE        = 'Sistema de Mantenimiento';
    SYS_GENERADOR_TITLE            = 'Buz�n de Archivo';
	SYS_SCAN_DOCUMENTOS_TITLE  	   = 'Scan de Documentos';
	SYS_RNUT_PARALELO_TITLE	   	   = 'RNUT Paralelo';
    SYS_INFRACTORES_TITLE          = 'Sistema de infractores';

    SYS_LIBERADOR_PATENTES_TITLE   = 'Liberador de Patentes';

    MSG_SI  = 'Si';
    MSG_NO  = 'No';

    SEXO_MASCULINO = 'M';
    SEXO_FEMENINO = 'F';

    SEXO_MASCULINO_DESCRIP = 'Masculino';
    SEXO_FEMENINO_DESCRIP = 'Femenino';

    //Nombre de las Aplicaciones

  	MSG_MANTENIMIENTO_TITLE	= 'Sistema de Mantenimiento';
    MSG_COBRANZAS_TITLE		= 'Sistema de Cobranzas';

    // El rango de fecha personalizado es un item de los combos para filtrar por fecha
    MSG_RANGO_FECHA             = 'Rango de fecha personalizado';
    // Caption de facturas para pagos manuales
    MSG_FACTURA_PAGO_MANUAL     = 'Factura para Pago Manual';
    // T�tulos de los reportes de facturaci�n
    MSG_DOCUMENTO_DEMOSTRACION  = 'Documento de demostraci�n (Factura)';
    MSG_COPIA_DEMOSTRACION      = 'Documento de demostraci�n (Copia de factura)';
    MSG_DETALLE_VIAJES          = 'Detalle de viajes';
    // Caption de las pantallas de ingreso de par�metros de reportes
    CAPTION_INGRESO_PARAMETROS  = 'Configuraci�n de Par�metros';
    // En la mayoria de las pantallas de ingreso de par�metros se ingresa un rango de fecha
    MSG_RANGO_FECHA_INCORRECTO  = 'El rango de Fechas ingresado es incorrecto.';
    // Caption para informar si no se tienen permisos para ejecutar una opcion del menu
    MSG_NO_TIENE_PERMISOS       = 'No tiene permisos para realizar esta operaci�n.';
    MSG_DEVOLVER_TAREA = 'Devolver una tarea implica liberarla para que la pueda realizar cualquier otro usuario.' + CRLF +
      '�Est� seguro de que desea devolver esta tarea?';
    MSG_NO_ENCONTRO_DOMICILIO   = '< no tiene domicilio >';
    MSG_NO_ENCONTRO_COMUNA      = '< no tiene comuna >';
    MSG_NO_ENCONTRO_REGION      = '< no tiene region >';
    MSG_NO_ENCONTRO_CIUDAD      = '< no tiene ciudad >';

    // Patentente de Chile
    PATENTE_CHILE = 'CHL';

    //ResourceStrings para las TDPSGrid y DBGrids.

    MSG_COL_TITLE_FECHA_HORA            = 'Fecha / Hora';
    MSG_COL_TITLE_BANCO					= 'Banco';
    MSG_COL_TITLE_CUENTA				= 'Cuenta';
    MSG_COL_TITLE_CLIENTE				= 'Cliente';
    MSG_COL_TITLE_CATEGORIA				= 'Categor�a';
    MSG_COL_TITLE_PATENTE				= 'Patente';
	MSG_COL_TITLE_NUMERO				= 'N�mero';
	MSG_COL_TITLE_FECHA					= 'Fecha';
	MSG_COL_TITLE_PROVEEDOR 			= 'Proveedor';
	MSG_COL_TITLE_CANTIDAD				= 'Cantidad';
	MSG_COL_TITLE_ESTADO				= 'Estado';
	MSG_COL_TITLE_RECIBIDOS				= 'Recibidos';

	MSG_COL_TITLE_CONCESIONARIA 		= 'Concesionaria';
	MSG_COL_TITLE_DESCRIPCION			= 'Descripcion';
	MSG_COL_TITLE_IMPORTE				= 'Monto';

	MSG_COL_TITLE_NUMERO_DE_VIAJE 		= 'N�mero de Viaje';
	MSG_COL_TITLE_MOMENTO_DE_INICIO		= 'Momento de Inicio';
	MSG_COL_TITLE_PUNTOS_DE_RECORRIDOS	= 'Puntos de Recorridos';

	MSG_COL_TITLE_VENCIMIENTO			= 'Vencimiento';
	MSG_COL_TITLE_NOMBRE_Y_APELLIDO		= 'Nombre y Apellido';
	MSG_COL_TITLE_CONCEPTO				= 'Concepto';

	MSG_COL_TITLE_TIPO_DE_CLIENTE		= 'Tipo de Cliente';
	MSG_COL_TITLE_TIPO_DE_HORARIO		= 'Tipo de Horario';
	MSG_COL_TITLE_PRECIO_POR_KM			= 'Precio por KM';

	MSG_COL_TITLE_TIPO_DE_PAGO			= 'Tipo de Pago';
	MSG_COL_TITLE_VEHICULO				= 'Veh�culo';

    MSG_COL_TITLE_COMUNICACION			= 'Comunicaci�n';
	MSG_COL_TITLE_ORDEN_DE_SERVICIO		= '�rden de Servicio';
	MSG_COL_TITLE_INICIO_DE_COMUNICACION= 'Inicio de Comunicaci�n';
	MSG_COL_TITLE_USUARIO				= 'Usuario';
	MSG_COL_TITLE_TIEMPO_RESTANTE		= 'Tiempo Restante';

	MSG_COL_TITLE_EVENTO 				= 'Evento';
	MSG_COL_TITLE_ID_EVENTO 			= 'ID. Evento';
    MSG_COL_TITLE_OBSERVACIONES 		= 'Observaciones';

	MSG_COL_TITLE_MOVIMIENTO 			= 'Movimiento';
    MSG_COL_TITLE_TIPO 					= 'Tipo';
	MSG_COL_TITLE_DET_MOV 				= 'Det. Mov.';
	MSG_COL_TITLE_DEUDA_ANTERIOR 		= 'Deuda Anterior';
	MSG_COL_TITLE_DEUDA_ACTUAL 			= 'Deuda Actual';

	MSG_COL_TITLE_ORD_SERV 				= 'Ord.Srv.';
	MSG_COL_TITLE_INICIO_COMUNICACION 	= 'Inicio Comunicacion';

	MSG_COL_TITLE_DET_VIAJE 			= 'Det. Viaje';
	MSG_COL_TITLE_PUNTOS_RECORRIDOS 	= 'Puntos Recorridos';
	MSG_COL_TITLE_FECHA_FACT 			= 'Fecha Fact.';
	MSG_COL_TITLE_TIPO_COMP 			= 'Tipo Comp.';
	MSG_COL_TITLE_NRO_COMP 				= 'Nro.Comp';
	MSG_COL_TITLE_KMS					= 'Kms';

	MSG_COL_TITLE_DET_CTE 				= 'Det. Cte.';
	MSG_COL_TITLE_IMPRESION 			= 'Impresi�n';
	MSG_COL_TITLE_VIAJES 				= 'Viajes';
	MSG_COL_TITLE_SERVICIOS 			= 'Servicios';
	MSG_COL_TITLE_INTERESES 			= 'Intereses';
	MSG_COL_TITLE_IMPUESTOS 			= 'Impuestos';
	MSG_COL_TITLE_CONVENIOS 			= 'Convenios';
	MSG_COL_TITLE_OTROS_CONCEPTOS 		= 'Otros Conceptos';
	MSG_COL_TITLE_DESCUENTOS 			= 'Descuentos';

	MSG_COL_TITLE_DET_PAGO 				= 'Det. Pago';
	MSG_COL_TITLE_LUGAR_DE_PAGO 	  	= 'Lugar de Pago';
	MSG_COL_TITLE_DEUDA_POSTERIOR 		= 'Deuda Posterior';
	MSG_COL_TITLE_CTE_AJUSTADO 			= 'Cte. Ajustado';

	MSG_COL_TITLE_IMPORTE_FINANCIADO	= 'Monto Financiado';
	MSG_COL_TITLE_PIE_ENTREGADO 		= 'Pie Entregado';
	MSG_COL_TITLE_INTERES 				= 'Interes';
	MSG_COL_TITLE_VIGENCIA				= 'Vigencia';
	MSG_COL_TITLE_CUOTAS 				= 'Cuotas';
	MSG_COL_TITLE_CUOTAS_IMPAGAS 		= 'Cuotas Impagas';
	MSG_COL_TITLE_CUOTAS_PAGAS 			= 'Cuotas Pagas';
	MSG_COL_TITLE_PROXIMA_FACTURACION 	= 'Pr�xima Facturaci�n';
	MSG_COL_TITLE_PROXIMO_VENCIMIENTO 	= 'Proximo Vencimiento';

	MSG_COL_TITLE_DEF_INF				= 'Def. Inf.';
	MSG_COL_TITLE_DESCRIPCION_INFRACCION= 'Descripcion Infracci�n';
	MSG_COL_TITLE_EXPEDIENTE 			= 'Expediente';

	MSG_COL_TITLE_NUMERO_DE_TAG 		= 'N�mero de Tag';
	MSG_COL_TITLE_FECHA_ASIGNACION 		= 'Fecha Asignaci�n';
	MSG_COL_TITLE_UBICACION_ANTERIOR 	= 'Ubicaci�n Anterior';
	MSG_COL_TITLE_FECHA_INGRESO 		= 'Fecha Ingreso';

	MSG_COL_TITLE_FORMA_DE_PAGO			= 'Forma de Pago';
	MSG_COL_TITLE_CUPON					= 'Cup�n';
	MSG_COL_TITLE_AUTORIZACION 			= 'Autorizaci�n';
	MSG_COL_TITLE_TURNO					= 'Turno';

	MSG_COL_TITLE_COMPROBANTE			= 'Comprobante';
	MSG_COL_TITLE_FECHA_VENC			= 'Fecha Venc.';
	MSG_COL_TITLE_COMPROBANTE_AJUSTADO	= 'Comprobante Ajustado';
    MSG_COL_TITLE_DEBITO				= 'D�bito';
	MSG_COL_TITLE_IMPORTE_INFRACCION	= 'Monto Infracci�n';

   	MSG_COL_TITLE_CODIGO_CUENTA 	   	= 'C�digo Cuenta';
	MSG_COL_TITLE_FECHA_HORA_DE_INICIO 	= 'Fecha/Hora de inicio';
    MSG_COL_TITLE_PUNTOS_DE_COBRO		= 'Puntos de Cobro';
    MSG_COL_TITLE_VIAJE					= 'Viaje';

    MSG_COL_TITLE_ITE_AJUSTADO			= 'Monto Ajustado';
    MSG_COL_TITLE_HORA				    = 'Hora';
    MSG_COL_TITLE_PUNTO_COBRO   		= 'Punto de Cobro';
    MSG_COL_TITLE_FH_INICIO     		= 'Fecha/Hora de Inicio';
    MSG_COL_TITLE_PUNTOS_COBRO  		= 'Puntos de Cobro';
    MSG_COL_TITLE_DESC_CUENTA       	= 'Descripci�n Cuenta';
    MSG_COL_TITLE_NUMERO_VIAJE          = 'Viaje';

    MSG_COL_TITLE_ENTREGA_INICIAL       = 'Entrega Inicial';
    MSG_COL_TITLE_IMPORTE_CUOTA         = 'Monto Cuota';

	MSG_COL_TITLE_MARCA					= 'Marca';
	MSG_COL_TITLE_MODELO				= 'Modelo';
	MSG_COL_TITLE_COLOR					= 'Color';

    MSG_COL_TITLE_IMAGEN                = 'Img.';
    MSG_COL_TITLE_CONVENIO   			= 'Convenio';
    MSG_COL_TITLE_TRANSITOS_FACTURADOS  = 'Tr�nsitos Facturados';

    //MSG_CLIENTE_EN_PROCESO_LEGAL        = 'Atenci�n. El cliente se encuentra en PROCESO LEGAL.';                                                                         //SS_1130_MCA_20130917
    //MSG_CLIENTE_EN_PROCESO_LEGAL_ASIGNADO_A_EMPRESA = 'Atenci�n. El cliente se encuentra en PROCESO LEGAL.' + CRLF + 'El cliente fue asignado a la empresa: "%s".';      //SS_1130_MCA_20130917

    MSG_CLIENTE_EN_PROCESO_LEGAL        = 'Atenci�n. El cliente se encuentra en PROCESO DE COBRANZA.';                                                                     //SS_1130_MCA_20130917
    MSG_CLIENTE_EN_PROCESO_LEGAL_ASIGNADO_A_EMPRESA = 'Atenci�n. El cliente se encuentra en PROCESO DE COBRANZA.' + CRLF + 'El cliente fue asignado a la empresa: "%s".';  //SS_1130_MCA_20130917

    (* Constantes para los Origenes del Tipo de Medio de Pago. *)
    CONST_ORIGEN_MEDIO_PAGO_COSTANERA  = 'C';
    CONST_ORIGEN_MEDIO_PAGO_PRESTO     = 'P';
    CONST_ORIGEN_MEDIO_PAGO_RNUT       = 'R';
    CONST_ORIGEN_MEDIO_PAGO_TRANSBANK  = 'T';
    CONST_ORIGEN_MEDIO_PAGO_SANTANDER  = 'S';
    CONST_ORIGEN_MEDIO_PAGO_BANCOCHILE = 'B';

    //XML Conmvenio
    //Nombre en el archivo XML
    XML_CONTRATO='contrato';
    XML_LISTA='lista';
    XML_VEHICULOS='vehiculo';
    XML_ENTIDAD='entidad';
    XML_DIRECCION='direccion';
    XML_CONTACTO='contacto';
    XML_DATO_CONTACTO='dato';
    XML_TAG='TAG';
    XML_RUT='RUT';
    XML_PATENTE='patente';
    XML_DV='DV';
    XML_ACCION='accion';
    XML_CONCESIONARIO='concesionaria';
    XML_CODIGO='codigo';
    XML_CATEGORIA = 'categoria';
    XML_MARCA = 'marca';
    XML_MODELO = 'modelo';
    XML_VERSION = 'version';
    XML_COLOR = 'color';
    XML_SERIE = 'serie';
    XML_FABRICANTE = 'fabricante';
    XML_CATEGORIAURBANA = 'caturb';
    XML_CONTEXT = 'context';
    XML_CONTRACT = 'contract';
    XML_ESTADO = 'estado';
    XML_ANIO = 'anio';
    XML_TIPO = 'tipo';
    XML_NOMBRE = 'nombre';
    XML_CALLE = 'calle';
    XML_COMUNA = 'comuna';
    XML_ALTURA = 'altura';
    XML_DEPTO = 'depto';
    XML_ZIP = 'zip';
    XML_COMENTARIO = 'comentario';
    XML_SECUNDARIO = 'secundario';
    XML_APELLIDO_PATERNO = 'apellidop';
    XML_APELLIDO_MATERNO = 'apellidom';
    XML_FANTASIA = 'fantasia';
    XML_GIRO = 'giro';
    XML_ROL = 'rol';
    XML_INSTAL = 'instal';

    XML_TAGL = 'TAGL';
    XML_AMARILLA = '1';
    XML_NEGRA    = '2';
    XML_VERDE    = '3';
    XML_GRIS     = '4';
    XML_BLANCA   = '5';

    //Respuesta
    XML_RESPUESTA = 'respuesta';
    XML_COMPLEMENTO = 'complemento';
    XML_PROCESADO_OK = 'O';
    XML_PROCESADO_ERROR = 'E';
    XML_PROCESADO_PENDIENTE = 'P';
    XML_PROCESADO_CORREGIDO = 'C';


    //moviento
    XML_ALTA='1';
    XML_BAJA='3';
    XML_MODIFICACION='2';
    XML_NADA = '0';

    //personerias
    XML_PERSONERIA_JURIDICA = '2';
    XML_PERSONERIA_NATURAL  = '1';

    //roles
    XML_ROL_CLIENTE = '1';
    XML_ROL_REPRESENTATE_LEGAL = '2';
    XML_ROL_CONTACTO = '5';

    //tipos de direcciones
    XML_DIRECCION_FACTURACION = '1';
    XML_DIRECCION_CONTACTO = '2';
    XML_DIRECCION_POSTAL = '3';
    XML_DIRECCION_PRINCIPAL = '4';

    //tipo de contacto
    XML_CONTACTO_PARTICULAR = '1';
    XML_CONTACTO_OFICINA  = '2';
    XML_CONTACTO_CELULAR = '3';
    XML_CONTACTO_eMAIL = '4';
    XML_CONTACTO_WEB = '5';
    XML_CONTACTO_WAP = '6';

    XML_FABRICANTE_TAG = '3';   // Combitech

    //tipo estad de tag
    XML_ESTADO_TAG_PENDIENTE = '1';
    XML_ESTADO_TAG_DISPONIBLE = '2';
    XML_ESTADO_TAG_INSTALADO = '3';
    XML_ESTADO_TAG_ELIMINADO = '4';
    XML_ESTADO_TAG_ROBADO = '5';

    //Tipos categorias vehiculos
    XML_VEHICULO_AUTO = '1';
    XML_VEHICULO_CAMION = '2';
    XML_VEHICULO_TRACTOR = '3';
    XML_VEHICULO_MOTO = '4';

    XML_NOVEDAD_MANUAL = 'M';
    XML_NOVEDAD_AUTOMATICA = 'A';

    // Estados
    XML_ESTADO_ACTIVO = '1';

const
    //Fuentes Comunicacion
    FC_CALLCENTER = 1;
    TXT_CONVENIO_SIN_CUENTA = ' (Sin Cuenta)';                                    //SS_1120_MVI_20130820
    CANT_CARACTERES_NUMERO_CONVENIO = 17;
    TXT_CONVENIO_DE_BAJA = ' (Baja)';                                             //SS_1120_MVI_20130820
const

    // Action list. Constantes de los Tipos de Hist�rico de Pedido de Env�o.
    ACTION_LIST_PEDIDO_PENDIENTE    = 'PP';
    ACTION_LIST_PEDIDO_RECICLADO    = 'PR';
    ACTION_LIST_PEDIDO_CONFIRMADO   = 'PC';
    ACTION_LIST_CONFIRMACION_PEDIDO = 'CC';

    COLOR_EDITS_OBLIGATORIO    = $00FAEBDE;
    COLOR_EDITS_NO_OBLIGATORIO = clWindow;
    COLOR_LABEL_OBLIGATORIO    = clNavy;
    COLOR_LABEL_NO_OBLIGATORIO = clWindowText;
    STYLE_LABEL_OBLIGATORIO    = [fsBold];
    STYLE_LABEL_NO_OBLIGATORIO = [];

    MAX_CANT_PARAMETROS_REPORTE = 255;
    MAX_OS_DEFINIDAS			= 200;

    FORMATO_IMPORTE     = '$ #,##0; $ -#,##0';
    FORMATO_PORCENTAJE  = '#,##0.00; -#,##0.00';

    //Personeria
    PERSONERIA_FISICA   = 'F';
    PERSONERIA_JURIDICA = 'J';
    PERSONERIA_AMBAS    = 'A';

    PERSONERIA_FISICA_DESC      = 'Persona';
    PERSONERIA_JURIDICA_DESC    = 'Empresa';
    PERSONERIA_AMBAS_DESC       = 'Ambas';

    //Estado Solicitud
    ESTADO_SOLICITUD_CONVENIO_FIRMADO = 1;
    ESTADO_SOLICITUD_PENDIENTE = 2;
    ESTADO_SOLICITUD_AGENDADA = 3;
    ESTADO_SOLICITUD_CONTACTO_TELEFONICO = 4;
    ESTADO_SOLICITUD_CONTACTO_EMAIL = 5;
    CONTACTO_CORREO_POSTAL = 6;
    ESTADO_SOLICITUD_ANULADA = 7;

    //Estado Convenio
    ESTADO_CONVENIO_VIGENTE = 1;
    ESTADO_CONVENIO_BAJA    = 2;


    //Roles Convenio
    ROL_REPRESENTANTE_EMPRESA = 1;
    ROL_REPRESENTANTE_CLIENTE = 2;
    ROL_PERSONA               = 3;

    // Check List
    CONST_ENTREGAR_DENUNCIA    = 4;
    CONST_EMITIR_FAC_TELEVIA   = 5;
    CONST_ENTREGAR_FAC_TELEVIA = 6;
    CONST_CAMBIAR_TELEVIA      = 7;
    CONST_ELIMINAR_CUENTA      = 8;
    CONST_PENDIENTE_PAGAR_TAG  = 9;

    //Estado Cuenta Vehiculo
    ESTADO_CUENTA_VEHICULO_ACTIVO     = 1;
    ESTADO_CUENTA_VEHICULO_BAJA       = 2;
    ESTADO_CUENTA_VEHICULO_SUSPENDIDA = 3;

    //Codigo Estado de Infracciones
    {
    PENDIENTE_ENVIO     = 0;
    NO_CURSABLE         = 1;
    FIRMADO             = 2;
    PENDIENTE_RESPUESTA = 3;
    ANULADA             = 4;
    RESPUESTA_ERROR     = 5;
    IMAGEN_ANULADA      = 6;
    }
    CONST_ESTADO_INFRACCION_EXTERNO_PENDIENTE_ENVIO     = 0;
    CONST_ESTADO_INFRACCION_EXTERNO_ANULADA             = 1;
    CONST_ESTADO_INFRACCION_EXTERNO_PENDIENTE_RESPUESTA = 2;
    CONST_ESTADO_INFRACCION_EXTERNO_FIRMADA             = 3;

    CONST_ESTADO_INFRACCION_INTERNO_PENDIENTE  = 0;
    CONST_ESTADO_INFRACCION_INTERNO_ANULADA    = 1;
    CONST_ESTADO_INFRACCION_INTERNO_FACTURADA  = 2;
    CONST_ESTADO_INFRACCION_INTERNO_PAGADA     = 3;
    CONST_ESTADO_INFRACCION_INTERNO_VALORIZADA = 4;

    //Codigo Motivos Anulacion
    CONST_MOTIV_ANUL_INFRAC_RESCATADO_POR_LIMBO                     = 1;
    CONST_MOTIV_ANUL_INFRAC_RNVM_PATENTE_NO_EXISTE                  = 2;
    CONST_MOTIV_ANUL_INFRAC_SUPERO_CANT_CONSULTAS_RNVM              = 3;
    CONST_MOTIV_ANUL_INFRAC_JPL_INFRACCION_CON_ERROR                = 4;
    CONST_MOTIV_ANUL_INFRAC_RESP_RNVM_SIN_SUFICIENTES_DATOS         = 5;
    CONST_MOTIV_ANUL_INFRAC_TRANS_PREVIOS_A_FECHA_ADQUISICION_VEHIC = 6;
    CONST_MOTIV_ANUL_INFRAC_ERROR_VALIDACION_MANUAL                 = 7;
    CONST_MOTIV_ANUL_INFRAC_ERROR_RECLAMO_CLIENTE                   = 8;
    CONST_MOTIV_ANUL_INFRAC_ASIGNADO_PDU                            = 9;
    CONST_MOTIV_ANUL_INFRAC_ASIGNADO_BHTU                           = 10;
    CONST_MOTIV_ANUL_INFRAC_FACTURADO                               = 11;
    CONST_MOTIV_ANUL_INFRAC_ERROR_IMAGEN_NO_ENCONTRADA              = 12;

    //Tipos de Busqueda de Personas
    TBP_CLIENTES            = 0;
    TBP_PERSONAS            = 1;
    TBP_PERSONAS_NATURALES  = 2;
    TBP_PERSONAS_JURIDICAS  = 3;
    TBP_PROVEEDORES         = 4;
    TBP_BANCOS              = 5;
    TBP_SUCURSALES_BANCOS   = 6;
    TBP_TARJETAS_CREDITO    = 7;
    TBP_OPERADOR_LOGISTICO  = 8;

    //Tipos de fuentes de solicitud
    TF_WEB           = 'W';
    TF_CALL_CENTER   = 'T';
    TF_CUPON         = 'C';

    //Fuentes de solicitud
    FUENTE_CALLCENTER = 2;
    FUENTE_CUPONSANTANDER = 3;
    FUENTE_CUPONPRESTO= 6;

    //FUENTE ORIGEN CONTACTO (Tabla FuentesOrigenContacto)
    FUENTE_ORIGEN_CONTACTO_SANTANDER = 1;
    FUENTE_ORIGEN_CONTACTO_NO_SANTANDER = 1;


    // Medios de contacto
    MC_TELEPHONE    = 1;
    MC_EMAIL        = 2;
    MC_LETTER       = 3;
    MC_FAX          = 4;
    MC_VOICE_MAIL   = 5;
    MC_MOVIL_PHONE  = 6;
    MC_BEEPER       = 7;
    MC_WEB          = 8;

    // Gr�ficos de los reportes
    SIN_GRAFICO         = '';
    GRAFICO_BARRAS      = 'B';
    GRAFICO_TORTA       = 'T';
    GRAFICO_VECTORIAL   = 'V';

	// Incidencias de P�rticos
	INCID_NINGUNA				= 0;
	INCID_MENSAJEPEAJISTA		= 1;
	INCID_ROBO					= 2;
	INCID_CIERREFORZADO			= 3;
	INCID_ERRORLECTURAMEDIO		= 4;
	INCID_MEDIORECHAZADO		= 7;
	INCID_VIAONLINE				= 8;
	INCID_VIAOFFLINE			= 9;
	INCID_ERRORHARDWARE			= 10;
	INCID_INICIOSOFTWARE		= 11;

    //C�digos de registros de la tabla MENSAJES
    MENSAJE_RUT_INCORRECTO            = 1;
    MENSAJE_SOLICITUD_RUT_EXISTENTE_WEB = 2;
    MENSAJE_SOLICITUD_RUT_EXISTENTE_CAC = 5;
    MENSAJE_SOLICITUD_FINAL_CALL_CENTER = 6;
    MENSAJE_SOLICITUD_FINAL_WEB         = 7;
    MENSAJE_SOLICITUD_FINAL_CUPON       = 8;
    MENSAJE_SOLICITUD_FUENTE_ORIGEN_CONTACTO = 9;
    MENSAJE_PATENTE_EXISTENTE_SOLICITUD = 10;
    MENSAJE_PATENTE_EXISTENTE           = 11;
    MENSAJE_SCRIPT_SOLICITUD            = 12;
    MENSAJE_IMPORTACION_EXCEL           = 13;
    MENSAJE_FECHA_ENTREGA               = 14;
    MENSAJE_PATENTE_EXISTENTE_CONVENIO  = 15;

    //Documento cobro
    CONST_DOCUMENTO_COBRO_ELECTRONICO = 3;

    //C�digos de registros de la tabla ContenidosEmails
    CONTENIDO_EMAIL_ALTA_SOLICITUD  = 1;

	// Incidencias de Puntos de Cobro / Generales
	INCID_POCOESPACIOENDISCO	= 24;

    { WARM $ 'Estas constantes se deben elimianr en el momento de terminar de cambiarlas' }
    TIPO_ORIGEN_DATO_PARTICULAR     = 1;
    TIPO_ORIGEN_DATO_COMERCIAL      = 2;

    // Domicilios
    FALTA_INGRESAR_DOMICILIO      = 'Ingrese el domicilio';

    // Tipo Domicilios
    TIPO_DOMICILIO_PARTICULAR     = 1;
    TIPO_DOMICILIO_COMERCIAL      = 2;

    // Tipo Medios de Contacto (cada uno de ellos tiene un Formato, abajo de esto)
    TIPO_MEDIO_CONTACTO_E_MAIL          = 1;
    TIPO_MEDIO_CONTACTO_TE_COMERCIAL    = 2;
    TIPO_MEDIO_CONTACTO_TE_PARTICULAR   = 3;
    TIPO_MEDIO_CONTACTO_CORREO_POSTAL   = 4;
    TIPO_MEDIO_CONTACTO_TE_CELULAR      = 5;

    // Formato de los Tipo de Medio de Contacto
    TMC_TELEFONO     = 'T';
    TMC_EMAIL        = 'E';
    TMC_DIRECCION    = 'D';

	// Severidad de las Incidencias
	SEV_INFO					= 0;
	SEV_BAJA					= 1;
	SEV_MEDIA					= 2;
	SEV_ALTA					= 3;

    (* Constantes para representar los Canales de Pago. *)
    CONST_CANAL_PAGO_NINGUNO    = 0;
    CONST_CANAL_PAGO_PAT        = 1;
    CONST_CANAL_PAGO_PAC        = 2;
    CONST_CANAL_PAGO_WEB_PAY    = 7;
    CONST_CANAL_PAGO_LIDER      = 9;
    CONST_CANAL_PAGO_VENTANILLA = 10;
    CONST_CANAL_PAGO_TARJETA_VENTANILLA = 23; // SS 809

    // Motivos Anulaci�n de Infracciones
    MAI_RECLAMO  = 8;

    //Arriendo telev�a en Cuotas
	CONST_CONCEPTO_ARRIENDO_TAG_CUOTAS = 34;


    (* Constantes para representar las Formas de Pago. *)
    CONST_FORMA_PAGO_EFECTIVO           = 3;
    CONST_FORMA_PAGO_CHEQUE             = 4;
    CONST_FORMA_PAGO_TARJETA_DEBITO     = 5;
    CONST_FORMA_PAGO_TARJETA_CREDITO    = 6;
    CONST_FORMA_PAGO_VALE_VISTA         = 8;
    CONST_FORMA_PAGO_DEBITO_CUENTA      = 9;
    CONST_FORMA_PAGO_PAGARE             = 10;

    // Rev. 30 (SS 809)
    CONST_PAGE_NO_DEFINIDO          = 0;
    CONST_PAGE_EFECTIVO             = 1;
    CONST_PAGE_TARJETA              = 2;
    CONST_PAGE_CHEQUE               = 3;
    CONST_PAGE_VALE_VISTA           = 4;
    CONST_PAGE_DEBITO_CUENTA_PAC    = 5;
    CONST_PAGE_DEBITO_CUENTA_PAT    = 6;
    CONST_PAGE_TARJETA_CREDITO      = 7; // SS 809
    CONST_PAGE_TARJETA_DEBITO       = 8; // SS 809
    CONST_PAGE_PAGARE               = 9;
    CONST_PAGE_CHEQUE_NI            = 10;

    CONST_PAGE_DESC_EFECTIVO        = 'Efectivo';
    CONST_PAGE_DESC_CHEQUE          = 'Cheque';
    CONST_PAGE_DESC_VALE_VISTA      = 'Vale Vista';
    CONST_PAGE_DESC_DEBITO_CUENTA   = 'Debito Cuenta';
    CONST_PAGE_DESC_PAGARE          = 'Gen�rica Pagar�';
    CONST_PAGE_DESC_TARJETA_CREDITO = 'Tarjeta Credito';
    CONST_PAGE_DESC_TARJETA_DEBITO  = 'Tarjeta Debito';
    // Fin Rev. 30 (SS 809)

    (* Constantes para representar los Conceptos de Pago. *)
    CONST_CONCEPTO_PAGO_POR_VENTANILLA      = 104;
    CONST_CONCEPTO_PAGO_DEBITO_AUTOMATICO   = 8;
    CONST_CONCEPTO_PAGO_VENTANILLA_LIDER    = 107;
    CONST_CONCEPTO_PAGO_WEBPAY              = 32;

    //Tipos de medio de Pago Automatico
    //Tipos de medio de Pago Automatico - Ahora se incluyen Todos los tipos de pago
    // estan en la tabla TipoMedioPago
    TPA_SELECCIONE = '(Seleccione)';
    TPA_NINGUNO = 0;
    TPA_NINGUNO_DESC = 'Ninguno';
    TPA_PAT = 1;
    TPA_PAT_DESC = 'PAT';
    TPA_PAC = 2;
    TPA_PAC_DESC = 'PAC';
    TPA_EFECTIVO = 3;
    TPA_EFECTIVO_DESC = 'Efectivo';
    TPA_CHEQUE = 4;
    TPA_CHEQUE_DESC = 'Cheque';
    TMP_DEBITO = 5;
    TMP_CREDITO = 6;
    TMP_WEBPAY = 7;

    //Tipos de Pases Diarios
    TPD_NORMAL = 'N';
    TPD_NORMAL_DESC = 'Normal';
    TPD_TARDIO = 'T';
    TPD_TARDIO_DESC = 'Tard�o';

    //Tipos de Facturaci�n Detallada
    TFD_NINGUNA 	= 'N';
    TFD_PAGA 		= 'P';
    TFD_GRATIS 		= 'G';

    // Tipos de los Motivos de Denucia
    TIPO_DENUNCIA_COMENZAR      = 0;
    TIPO_DENUNCIA_DETENER       = 1;

    // Estados Personas (clientes)
    ESTADO_PERSONA_ACTIVO       = 'A';
    ESTADO_PERSONA_INACTIVO     = 'I';

    // Estados posibles de las Tareas (Workflow)
    ESTADO_TAREA_CANCELADA      = 'C';
    ESTADO_TAREA_PENDIENTE      = 'P';
    ESTADO_TAREA_TERMINADA      = 'T';

    // Estados posibles de MaestroDayPass
    // ESTADO_ACTIVO_DAY_PASS      = 'A';
    // ESTADO_PENDIENTE_DAY_PASS   = 'P';
    TIPO_DAYPASS_NORMAL		    = 'N';
    TIPO_DAYPASS_TARDIO		    = 'T';

	// Motivos de Bajas Fijas de TAGs.
	MB_CAMBIOPLAN				= 1;
	MB_CUENTAMOROSA				= 2;
	MB_TARJETAINHABILITADA		= 3;

	// Varios
	DB_USR_STARTUP				= 'usr_startup';
	PAIS_CHILE					= 'CHI';
    REGION_SANTIAGO				= '013';
    DESCRIPCION_CIUDAD_SANTIAGO = 'Santiago';
    CIUDAD_SANTIAGO				= 'SGO';
    CODIGO_AREA_SANTIAGO        = 2;
    //CODIGO_COTANERA_NORTE       = '001';      SS_1147_MCA_20140408
    //CODIGO_CN                   = 1;			SS_1147_MCA_20140408
    CODIGO_CN                   = 1;			//SS_1147_NDR_20140710
    CODIGO_VS                   = 3;			//SS_1147_NDR_20140710
    CODIGO_AMB                  = 5;            // SS_1409_CQU_20151102

    // Comodines Patente
    COMODIN_CHAR				= '&';
    COMODIN_INT					= '#';
    COMODIN_GRAL				= '*';

    // Estados de comprobante
	COMPROBANTE_IMPAGO 			= 'I';
	COMPROBANTE_PAGO 			= 'P';
	COMPROBANTE_CANCLEADO 		= 'C';
	COMPROBANTE_ANULADO 		= 'A';

	// Estado de financiamientos
	EF_PROGRAMADO				= 1;
	EF_VIGENTE					= 2;
	EF_FINALIZADO				= 3;
	EF_CADUCADO					= 4;

	// Tipos de comprobante
	TC_FACTURA 				= 'FC';
	TC_NOTA_CREDITO 		= 'NC';
	TC_NOTA_DEBITO			= 'ND';
	TC_TICKET           	= 'T';
    TC_NOTA_COBRO       	= 'NK';
    TC_BOLETA           	= 'BO';
	TC_NOTA_CREDITO_A_COBRO	= 'CK';
	TC_NOTA_CREDITO_A_NQ	= 'CQ';
	TC_NOTA_CREDITO_A_NB	= 'CB';

    TC_TRASPASO_CREDITO		= 'TC';		//SS_740_MBE_20110720
    TC_TRASPASO_DEBITO		= 'TD';		//SS_740_MBE_20110720
	TC_DEVOLUCION_DINERO	= 'DV';		//SS959-20110617-MBE

  //TC_SALDO_INICIA_OC = 'SI';                                                  // SS_1015_HUR_20120413 // SS_1015_HUR_20120417
  //TC_ANULA_SALDO_INICIAL_OC = 'AS';                                           // SS_1015_HUR_20120413 // SS_1015_HUR_20120417
  TC_SALDO_INICIAL_DEUDOR = 'SD';                                               // SS_1015_HUR_20120417
  TC_ANULA_SALDO_INICIAL_DEUDOR = 'AD';                                         // SS_1015_HUR_20120417
  TC_SALDO_INICIAL_ACREEDOR = 'SA';                                             // SS_1015_HUR_20120417
  TC_ANULA_SALDO_INICIAL_ACREEDOR = 'AA';                                       // SS_1015_HUR_20120417
  TC_CUOTAS_OTRAS_CONCESIONARIAS = 'CO';                                             // SS-1015-NDR-20120510
  TC_ANULA_CUOTAS_OTRAS_CONCESIONARIAS = 'AC';                                       // SS-1015-NDR-20120510

    //Tipos Comprobantes Electr�nicos
    TC_FACTURA_EXENTA       = 'FE';
    TC_FACTURA_AFECTA       = 'FA';
    TC_BOLETA_EXENTA        = 'BE';
    TC_BOLETA_AFECTA        = 'BA';
    TC_NOTA_CREDITO_ELECTRONICA = 'NE';

    // Tipo de Comprobante para las Notas de Cobro Directas para PDU.
	TC_NOTA_COBRO_DIRECTA	    = 'NQ';
    // Tipo de Comprobante para las Notas de Cobro Directas para BHTU.
	TC_NOTA_COBRO_DIRECTA_BHTU	= 'NB';
    TC_COMPROBANTE_DEUDA        = 'CD';
    TC_COMPROBANTE_CUOTA        = 'CU';

    //en la base, ObtenerListaMonedas devuelve estas menos el peso
    MN_MONEDA_PESO            = 'PES';
    MN_MONEDA_UTM             = 'UTM';
    MN_MONEDA_UF              = 'UF';



	TIPO_FACTURA_CHILE 	= 'F';
	TC_RECIBO 				= 'RC';
    TC_NOTA_COBRO_INFRACCIONES = 'NI';
    TC_FACTURACION_INMEDIATA = '_FI';    // SS_964_PDO_20110531
    TC_FACTURACION_POR_BAJA = '_FB';     // SS_964_PDO_20110531


	// Tipos de Adhesi�n
	TA_CON_TAG										= 	1;
	TA_SIN_TAG                                      =	2;
	TA_CON_TAG_DESCRIPCION  						= 	'Con TAG';
	TA_SIN_TAG_DESCRIPCION                          =	'Sin TAG';

	// Tipos de Pago
	TP_PREPAGO										=	'PR';
	TP_POSPAGO_MANUAL								=	'PM';
	TP_DEBITO_AUTOMATICO							=	'DA';

	// Tipos de D�bito autom�tico
	DA_TARJETA_CREDITO								=	'TC';
	DA_TARJETA_DEBITO								=	'TD';
	DA_CUENTA_BANCARIA								=	'CB';

    // Tipo de Pago
    TP_CHEQUE                                       = 'CH';
    TP_EFECTIVO                                     = 'EF';

	// Lista de Sistemas
	SYS_ADMINISTRACION								= 1;
	SYS_FACTURACION									= 2;
	SYS_MONITOR_PUNTOS_COBRO						= 3;
	SYS_GESTION_TAGS								= 4;
	SYS_PERMISOS_AREA								= 5;
	SYS_CAC											= 6;
	SYS_VALIDACION              					= 7;
	SYS_COBRANZAS	              					= 8;
    SYS_WEB                                			= 9;
    SYS_GESTION_INFORMES                            = 10;
	SYS_MANTENIMIENTO								= 11;
    SYS_GENERADOR                                   = 13;
    SYS_ST4                                         = 14;
	SYS_SCAN_DOCUMENTOS								= 15;
    SYS_CAC_AC										= 16;
	SYS_RNUT_PARALELO								= 17;
    SYS_GESTION_INFRACCIONES                        = 18;

	SYS_DEMO_SIMULADOR								= 91;
    SYS_GESTION_INTERFACES                          = 12;

    SYS_LIBERADOR_PATENTES                          = 100;

	// Registro de Operaiones - Categorias de Modulos
	RO_CM_MODULOS_DE_ABM		   					= 1;
	RO_CM_INTERFACES_SALIENTES	   					= 2;
	RO_CM_PROCESOS_AUTOMATICOS	   					= 3;
	RO_CM_INTERFACES_ENTRANTES	   					= 4;
	RO_CM_GESTION_DE_CUENTAS	   					= 5;
	RO_CM_LOGIN_LOGOUT			   					= 6;

    // Registro de Operaiones - Modulos
    RO_MOD_SALIDA_TARJETA_CREDITO			 		= 1;
    RO_MOD_SALIDA_JUZGADO_LOCAL				 		= 2;
    RO_MOD_SALIDA_AGENCIA_COBRANZA			 		= 3;
    RO_MOD_ENTRADA_REGISTRO_PATENTES   				= 4;
    RO_MOD_AVISOS_INFRACCION						= 5;
    RO_MOD_MARCAR_CUENTA_MOROSA						= 6;
    RO_MOD_ENTRADA_TARJETA_CREDITO                  = 7;
    RO_MOD_INHABILITAR_POR_MORA						= 8;
    RO_MOD_SALIDA_BANCO						 		= 9;
    RO_MOD_ENTRADA_BANCO			   				= 10;
    RO_MOD_ENTRADA_JUZGADO_LOCAL	   				= 11;
    RO_MOD_CREAR_CUENTA								= 12;
    RO_MOD_LOGIN									= 13;
    RO_MOD_LOGOUT									= 14;
    RO_MOD_USUARIO_INCORRECTO						= 15;
    RO_MOD_CONTRASENA_INCORRECTA					= 16;
    RO_MOD_IMPORTACION_VEHICULOS                    = 17;
    RO_MOD_IMPORTACION_TAGS							= 18;
    RO_MOD_IMPORTACION_CITAS                        = 19;
    RO_MOD_IMPORTACION_SOLICITUDES                  = 20;
    RO_MOD_PROCESO_DOCUMENTOS_ESCANEADOS            = 21;
    RO_MOD_INTERFAZ_SALIENTE_MANDATOS_PRESTO		= 28;
    RO_MOD_INTERFAZ_SALIENTE_IMPRESION              = 29;
    RO_MOD_INTERFAZ_ENTRANTE_MANDATOS_PRESTO		= 30;
    RO_MOD_CANCELAR_INTERFAZ_SALIENTE_IMPRESION     = 37;
    
    {** Day Pass **}
    RO_MOD_INTERFAZ_ENTRANTE_PASE_DIARIO			= 38;
  	RO_MOD_INTERFAZ_ENTRANTE_VENTAS_PASE_DIARIO		= 39;
  	RO_MOD_INTERFAZ_ENTRANTE_PASE_DIARIO_TARDIO		= 40;
    RO_MOD_INTERFAZ_ENTRANTE_DAYPASS_CAMBIO_FECHA	= 42;
   	RO_MOD_INTERFAZ_ENTRANTE_MAESTRO_PASE_DIARIO	= 45;
    RO_MOD_INTERFAZ_ENTRANTE_DAYPASS_DEVOLUCION		= 46;
    { ** Day Pass **}

    RO_MOD_PROCESO_RNUT_PARALELO					= 41;
    RO_MOD_INTERFAZ_SALIENTE_CARTAS_INFRACTORES     = 43;
    RO_MOD_CANCELAR_INTERFAZ_SALIENTE_CARTAS_INFRACTORES = 44;
    RO_MOD_INTERFAZ_SALIENTE_XML_INFRACCIONES       = 47;
    RO_MOD_INTERFAZ_SALIENTE_NOVEDADES_CMR          = 48; // SS 812
    RO_MOD_INTERFAZ_ENTRANTE_XML_INFRACCIONES       = 54;
    RO_MOD_INTERFAZ_SALIENTE_ACTIVACION_SERBANC     = 56;
    RO_MOD_INTERFAZ_SALIENTE_DESACTIVACION_SERBANC  = 57;
    RO_MOD_INTERFAZ_ENTRADA_ACCIONES_SERBANC        = 58;
    RO_MOD_INTERFAZ_ENTRADA_PAGOS_SERBANC           = 59;
    RO_MOD_INTERFAZ_ENTRADA_CAMBIOS_SERBANC         = 60;
    RO_MOD_INTERFAZ_ENTRADA_DIRECCIONES_SERBANC     = 61;
    RO_MOD_INTERFAZ_ENTRADA_PAGOS_SERVIPAG          = 67;
    RO_MOD_INTERFAZ_ENTRADA_PAGOS_SANTANDER         = 71;
    RO_MOD_INTERFAZ_ENTRADA_PAGOS_MISCUENTAS        = 73;
    RO_MOD_INTERFAZ_ENTRADA_PAGOS_BDP               = 74;
    RO_MOD_INTERFAZ_ENTRANTE_ANULACIONES_SERVIPAG   = 78;

    // Clearing de BHTU
    RO_MOD_INTERFAZ_SALIDA_INFRACCIONES_CLEARING_BHTU   = 62;
    RO_MOD_INTERFAZ_ENTRADA_INFRACCIONES_CLEARING_BHTU  = 63;
    RO_MOD_INTERFAZ_SALIDA_ASIGNACIONES_CLEARING_BHTU   = 64;
    RO_MOD_PROCESO_ASIGNACION_BHTU                      = 66;
    //COPEC PDU y PDUT
    RO_MOD_INTERFAZ_ENTRADA_COPEC_PDU				= 90;
    RO_MOD_INTERFAZ_ENTRADA_COPEC_PDUT				= 91;

    //GENERACION DE NOMINAS
    RO_MOD_INTERFAZ_SALIENTE_GENERACION_NOMINAS		= 95;

    RO_MOD_INTERFAZ_ENTRANTE_BAJAS_FORZADAS		= 109;                   //SS_1156B_NDR_20140128
    RO_MOD_INTERFAZ_ENTRANTE_RESPUESTAS_CONSULTAS_RNVM		= 111;                   //SS_1400_NDR_20151007

	// Lista de Acciones
	RO_AC_ALTA_REGISTRO								= 1;
	RO_AC_MODIFICACION_REGISTRO						= 2;
	RO_AC_EJECUCION_INTERFAZ		   				= 3;
	RO_AC_EJECUCION_PROCESO_AUTOMATICO				= 4;
	RO_AC_ERROR_EJECUTANDO_INTERFAZ					= 5;
	RO_AC_LOGIN										= 6;
   	RO_AC_LOGOUT									= 7;
	RO_AC_VALIDAR_DATOS_USUARIO						= 8;
	RO_IMPORTAR_VEHICULOS   						= 9;
	RO_AC_IMPORTAR_TAGS  				    		= 10;
	RO_IMPORTAR_CITAS                               = 11;
	RO_IMPORTAR_SOLICITUDES                         = 12;
	RO_AC_PROCESAR_DOCUMENTOS_ESCANEADOS			= 13;
	RO_AC_PROCESAR_TRANSITOS_RNUT_PARALELO			= 14;

	// Entidades constantes
	NE_AGENCIA_COBRANZA								= 'Agencia de Cobranza';
	NE_JUZGADO_LOCAL								= 'Juzgado de Polic�a Local';
	NE_REGISTRO_PATENTES							= 'Registro de Patentes';

    // Estados de la infracci�n
    EI_SIN_CONFIRMAR                                = 'Sin Confirmar';
    EI_CONFIRMADA                                   = 'Confirmada';
    EI_NOTIFICADA                                   = 'Notificada';
    EI_EN_TRAMITE_JUDICIAL                          = 'En Tr�mite Judicial';
    EI_REGULARIZADA                                 = 'Regularizada';

	// Codigos de severidad
	CS_CRITICO										= 4;
	CS_ALTA											= 3;
	CS_REGULAR                                      = 2;
	CS_BAJA											= 1;


	// Tipos de Orden de Servicio
	OS_ADHESION									   =	3;
	OS_INHABILITACION_TAG						   =	1;
	OS_CIERRE_CUENTA							   =	4;
	OS_FACTURACION_INCORRECTA					   =	5;
	OS_REHABILITACION_TAG						   =	6;
    OS_ATENDER_MAIL								   =	7;
	OS_ATENDER_AUDIO							   =	8;
	OS_AGRADECIMIENTO							   =	9;
	OS_ATENDER_FAX								   =	10;
	OS_RECLAMO_MALAS_INSTALACIONES				   =	11;
	OS_ACCIDENTE								   =	12;
	OS_FALTA_SEGURIDAD							   =	13;
	OS_RECLAMO_GENERAL							   =	14;
	OS_MODIFICACION_DATOS_CUENTA_CLIENTE		   =	15;
	OS_CAMBIO_MODALIDAD_PAGO					   =	16;
	OS_RECOMENDACION					   		   =	17;
//	OS_RECLAMO_FACTURACION_ADEUDADA	  			   =	19;
	OS_FACTURA_NO_RECIBIDA					   	   =	20;
	OS_GENERAR_ORDEN_DE_TRABAJO				   	   =	21;
	OS_REPARAR_POSTE_SOS					       =	22;
	OS_REPARAR_PORTICO							   =	23;
	OS_REPARAR_PAVIMENTO						   =	24;
    OS_CAMBIO_TAG								   = 	25;
    OS_LLAMAR_POR_TELEFONO_CLIENTE                 =    26;
    OS_ENVIAR_MAIL_CLIENTE                         =    27;
    OS_ENVIAR_CARTA_CLIENTE                        =    28;
    OS_ENVIAR_FAX_CLIENTE                          =    29;
    OS_ENVIAR_VOICE_MAIL_CLIENTE                   =    30;
    OS_MAL_FUNCIONAMIENTO_TAG                      =    31;

// Categogorias de Ordenes de Servicio
	CAT_OS_RECLAMO									= 1;
    CAT_OS_CONSULTA									= 2;
    CAT_OS_INFORMACION								= 3;
    CAT_OS_ACTUALIZACION_CUENTA						= 4;
    CAT_OS_MANTENIMIENTO							= 5;
    CAT_OS_FACTURACION								= 6;
    CAT_OS_MOROSOS  								= 7;
    CAT_OS_VALIDACION                               = 8;

    //Tipos de Contacto
    TIPO_CLIENTE                                    = 'C';
    TIPO_CONTACTO                                   = 'O';
    TIPO_LEGAJO                                     = 'L';

	// Tipos de Prioridad
	PRI_BAJA										= 	10;
	PRI_NORMAL										= 	5;
	PRI_ALTA										= 	3;
	PRI_URGENTE										= 	1;

    //Ventanas Autom�ticas para resolver las Tareas
    VA_MANUAL				= 0;
	VA_ALTA_CUENTA			= 1;
    VA_INHABILITAR_TAG		= 2;
    VA_REHABILITAR_TAG		= 3;
    VA_AJUSTE_FACTURACION	= 4;
    VA_VINCULAR_TAG			= 5;
    VA_ENTRAR_TAG			= 6;
    VA_CONECTORES			= 7;
    VA_CERRAR_CUENTA		= 8;
    VA_ORDEN_TRABAJO		= 9;
    VA_RESOLVER_TAREA_OT    = 10;
    VA_TAREA_MANTENIMIENTO  = 11;
    VA_ANALIZAR_MOROSO      = 12;

    //AREAS
    AREA_PROCESOS_AUTOMATICOS = 12;

    //Tipos de Procesos
    TPRO_ENVIO_AUTOMATICO_EMAIL       = 13;
    TPRO_RECEPCION_AUTOMATICO_EMAIL   = 14;
    TPRO_ENVIO_AUTOMATICO_FAX         = 15;
    TPRO_RECEPCION_AUTOMATICO_FAX     = 16;

	// Tipo de descuento
	TD_PORCENTUAL 	= 'P';
	TD_IMPORTE 		= 'I';

	// Codigo de la concesionaria
    //COD_CONCESIONARIA = 1;			SS_1147_MCA_20140408


    //Invocacion a la pantalla Codificacion de TAGs (de la unit GrabacionMedioPago)
    CODIFICACION_CONTROL = 1;
    CODIFICACION_ASIGNACION = 2;
    CODIFICACION_DEVOLUCION = 3;

    //Para subir
    CONSTANTE_PARA_SUBIR_TEAMSOURCE = 'Sube';

    // D�as de la semana
    DIA_SEMANA_DOMINGO   = 'Domingo';
    DIA_SEMANA_LUNES     = 'Lunes';
    DIA_SEMANA_MARTES    = 'Martes';
    DIA_SEMANA_MIERCOLES = 'Mi�rcoles';
    DIA_SEMANA_JUEVES    = 'Jueves';
    DIA_SEMANA_VIERNES   = 'Viernes';
    DIA_SEMANA_SABADO    = 'S�bado';

    //Acciones de morosos
    ACCION_PRIMER_AVISO           = 1;
    ACCION_SEGUNDO_AVISO          = 2;
    ACCION_ENVIO_LISTA_AMARILLA   = 3;
    ACCION_TERCER_AVISO           = 4;
    ACCION_ENVIO_LISTA_NEGRA      = 5;
    ACCION_ENVIO_PRIMER_CARTA     = 6;
    ACCION_ENVIO_SEGUNDA_CARTA    = 7;
    ACCION_ENVIO_AGENCIA_COBRANZA = 8;
    ACCION_ENVIO_JUZGADO          = 9;
    ACCION_ENVIO_INCOBRABLE       = 10;

    //Programacion de Ordenes de Servicio

    PROG_ORDEN_PRIMERO            = 1;
    PROG_ORDEN_SEGUNDO            = 2;
    PROG_ORDEN_TERCERO            = 3;
    PROG_ORDEN_CUARTO             = 4;
    PROG_ORDEN_ULTIMO             = 5;

    PROG_TIPO_ORDEN_LUNES         = 1;
    PROG_TIPO_ORDEN_MARTES        = 2;
    PROG_TIPO_ORDEN_MIERCOLES     = 4;
    PROG_TIPO_ORDEN_JUEVES        = 5;
    PROG_TIPO_ORDEN_VIERNES       = 6;
    PROG_TIPO_ORDEN_SABADO        = 7;
    PROG_TIPO_ORDEN_DOMINGO       = 8;
    PROG_TIPO_ORDEN_FERIADO       = 9;
    PROG_TIPO_ORDEN_DIA           = 10;
    PROG_TIPO_ORDEN_FIN_DE_SEMANA = 11;
	PROG_TIPO_ORDEN_DIA_LABORAL   = 12;

	//Estados de Ordenes de Compra
	OC_CERRADA = 0;
	OC_ABIERTA = 1;
	OC_CANCELADA = 2;

    // Usuario interno para registrar la Operaciones Internas de las Aplicaciones

    USR_SISTEMA = 'Sistema';


    //GRUPOS QUE NO SE PUEDEN ELIMINAR
    USUARIO_TODOS = 'Todos';
    USUARIO_ADMINISTRADORES = 'Administradores';


    //HORARIO POR DEFAULT DE LOS MEDIOS DE COMUNICACION
    HORA_DESDE = '09:00';
    HORA_HASTA = '18:00';

    //UBICACIONES DE LOS TAG
    CUSTOMER_LOCATION_CODE: Integer = 2; //Ubicacion: "En poder del cliente"
    UBICACION_TAG_ROBADO_CONCECIONARIA: Integer = 201;
    UBICACION_TAG_SINIESTRADO: Integer = 202;
    CONST_PREFIJO_CASO_VALIDACION = 'caso_validacion';
    // Manejo de Encueta
    CONST_PREGUNTA_MULTIPLE   = 1;
    CONST_PREGUNTA_EXCLUYENTE = 0;
    CONST_PREGUNTA_ABIERTA    = 2;

    CONST_TIPO_DATO_TEXTO     = 1;
    CONST_TIPO_DATO_FECHA     = 3;
    CONST_TIPO_DATO_NUMERICO  = 2;
    CONST_TIPO_DATO_BINARIO   = 0;

    // Gesti�n de TAGs
	CONST_ESTADO_SITUACION_PENDIENTE 	= 1;
    CONST_ESTADO_SITUACION_RECHAZADO	= 2;
    CONST_ESTADO_SITUACION_DISPONIBLE	= 3;
	CONST_EMBALAJE_PALLET               = 0;
    CONST_EMBALAJE_CAJA                 = 1;
    CONST_EMBALAJE_UNIDAD               = 2;
	CONST_CONTENEDOR_PALLET             = 'PALLET';
    CONST_CONTENEDOR_CAJA               = 'CAJA';
    CONST_CONTENEDOR_UNIDAD             = 'UNIDAD';
    DAR_MENSAJE							= TRUE;
    SIN_MENSAJE							= FALSE;
    NOMINADO							= TRUE;
    NONOMINADO							= FALSE;
    CONST_RECIBIR_TAG_DE_TRANSPORTE		= 9;
	CONST_MOVIMIENTO_NO_NOMINADO		= 0;
	CONST_MOVIMIENTO_NOMINADO			= 1;
    CONST_TAG_DISPONIBLE				= 3;
    CONST_TAG_DEVUELTO					= 8;
    CONST_TAG_ROBADO					= 10;
    CONST_TAG_BAJA   					= 11;
    CONST_ALMACEN_VENDIDO               = 4;
    CONST_ALMACEN_EN_COMODATO           = 3;
    CONST_ALMACEN_ARRIENDO              = 62;
    CONST_ALMACEN_ENTREGADO_EN_CUOTAS	= 15;

    CONST_STR_ALMACEN_COMODATO          = 'Comodato';
    CONST_STR_ALMACEN_ARRIENDO          = 'Arriendo';
    CONST_STR_ALMACEN_ENTREGADO_EN_CUOTAS = 'Arriendo en Cuotas';


    CONST_OPERACION_TAG_ELIMINAR        = 1 ;
    CONST_OPERACION_TAG_CAMBIAR         = 2 ;
    CONST_OPERACION_TAG_PERDIDO         = 3 ;
    CONST_OPERACION_TAG_AGREGAR         = 4 ;
    CONST_OPERACION_TAG_CBIOVEH         = 5 ;
    CONST_OPERACION_TAG_REACTIVAR       = 6 ;

    CONST_OPERACION_TAG_ELIMINAR_STR    = 'Eliminar' ;
    CONST_OPERACION_TAG_CAMBIAR_STR     = 'Cambiar' ;
    CONST_OPERACION_TAG_PERDIDO_STR     = 'Perdido' ;
    CONST_OPERACION_TAG_AGREGAR_STR     = 'Agregar' ;
    CONST_OPERACION_TAG_CBIOVEH_STR     = 'CambioVeh' ;
    CONST_OPERACION_TAG_REACTIVAR_STR   = 'Reactivar' ;


    CONST_OP_TRANSFERENCIA_TAG_NUEVO            = 'Nuevo' ;
    CONST_OP_TRANSFERENCIA_TAG_TRANSFERENCIA    = 'Transferencia' ;





    ESTADO_CONSERVACION_SIN_ESPECIFICAR = 0;
    ESTADO_CONSERVACION_BUENO           = 1;
    ESTADO_CONSERVACION_REGULAR         = 2;
    ESTADO_CONSERVACION_MALO            = 3;
    ESTADO_CONSERVACION_NO_DEVUELTO     = 4;

    TIPO_PERDIDA_ROBADO                 = 0;
    TIPO_PERDIDA_EXTRAVIO               = 1;

    ESPACIO_ENTRE_PREGUNTA    = 10;
    MARGEN_IZQUIERDO          = 30;
    MARGEN_SUPERIOR           = 10;
    ESPACIO_ENTRE_RENGLON     = 5;
    ESPACIO_ENTRE_ITEM        = 2;

    TAMANO_TITULO = 9;
    LETRA_TITULO = 'Arial';

//Importar Vehiculos Excel en Solicitud

    COLUMNA_RUT                = 'RUT:';
    COLUMNA_PERSONERIA_CLIENTE = 'Datos Personales';
    COLUMNA_PERSONERIA_EMPRESA = 'Datos de la Empresa';
    COLUMNA_PATENTE            = 'PATENTE (*)';

    NUMERO_COLUMNA_PERSONERIA = 2;
    NUMERO_FILA_CLIENTE = 4;
    NUMERO_FILA_EMPRESA = 4;
    NUMERO_COLUMNA_CAPTION_RUT = 3;
    NUMERO_COLUMNA_RUT = 4;
    NUMERO_COLUMNA_PATENTE = 3;
    NUMERO_COLUMNA_DIGITO_PATENTE = 4;
    NUMERO_COLUMNA_MARCA = 6;
    NUMERO_COLUMNA_MODELO = 7;
    NUMERO_COLUMNA_ANIO = 8;
    NUMERO_COLUMNA_TIPO_VEHICULO = 5;
    NUMERO_COLUMNA_PROPIEDAD = 9;

//Estados de Importacion de los archivos xls
    ESTADO_PENDIENTE   = 1;
    ESTADO_IMPORTADO   = 2;
    ESTADO_ELIMINADO   = 3;
    ESTADO_CANCELADO   = 4;
    ESTADO_CON_ERRORES = 5;
    ESTADO_A_CORREGIR  = 6;
    ESTADO_IMPORTADO_CON_RECHAZOS  = 7;

// Tipos de Archivos para importacion
    ARCHIVO_IMPORTACION = 'I';
    ARCHIVO_RECHAZO     = 'R';

// Resultados del procesamiento de Archivos para importacion
    RUT_INCORRECTO       = 1;
    FALTA_RUT_EXCEL      = 2;
    FALTA_SOLICITUD      = 3;
    AGREGADOS_RECHAZADOS = 4;
    CANCELADO_USUARIO    = 5;
    TODO_INCORPORADO     = 6;
    FALTAN_VEHICULOS     = 7;
// Agregado para importar interfaz de Santander
    //Resultado Correcto
    SOLICITUD_VEHICULOS = 8;
    //Resultado Incorrecto
    ARCHIVO_CONTIENERRORES = 9;

//Contrasena Desproteger Excel
    PROTEGER_EXCEL = 'benifernando';

//Context Mark para asignar Tags
//    CONTEXT_MARK = 1; OJO NO USAR ESTA CONST, USAR LA FUNCION

//Existencia de TAGs
	TAG_EN_INTERFASE = 0;
	TAG_EN_MAESTRO 	 = 1;
	TAG_NO_EXISTENTE = 2;

//Tipos Movimiento Turnos Tag
    TURNO_APERTURA = 1;
    TURNO_CIERRE   = 2;
    TURNO_ADICIONAR_TAG = 3;
    TURNO_ENTREGAR_TAG_CLIENTE = 4;

//Estado del Turno
    TURNO_ESTADO_ABIERTO = 'A';
    TURNO_ESTADO_CERRADO = 'C';

//CODIGOS DE DOCUMENTACION
    CODIGO_DOC_MANDATO_PAC=111;
    CODIGO_DOC_MANDATO_PAT=40;
    CODIGO_DOC_TARJETA_CREDITO=113;


    CODIGO_DOCUMENTACION_PRIMER_FIRMANTE = 11;
    CODIGO_DOCUMENTACION_DOMICILIO_PRIMER_FIRMANTE = 12;
    CODIGO_DOCUMENTACION_CONSTANCIA_RUT_PRIMER_FIRMANTE = 13;
    CODIGO_DOCUMENTACION_PODER_PRIMER_FIRMANTE = 14;
    CODIGO_DOCUMENTACION_RECEPCION_DOC_EMAIL = 17;

    CODIGO_DOCUMENTACION_SEGUNDO_FIRMANTE = 21;
    CODIGO_DOCUMENTACION_DOMICILIO_SEGUNDO_FIRMANTE = 22;
    CODIGO_DOCUMENTACION_CONSTANCIA_RUT_SEGUNDO_FIRMANTE = 23;
    CODIGO_DOCUMENTACION_PODER_SEGUNDO_FIRMANTE = 24;

    CODIGO_DOCUMENTACION_TERCER_FIRMANTE = 31;
    CODIGO_DOCUMENTACION_DOMICILIO_TERCER_FIRMANTE = 32;
    CODIGO_DOCUMENTACION_CONSTANCIA_RUT_TERCER_FIRMANTE = 33;
    CODIGO_DOCUMENTACION_PODER_TERCER_FIRMANTE = 34;

    CODIGO_DOCUMENTACION_UBICACION_CARPETA  = 112;
    DOCUMENTACION_UBICACION_CARPETA = 'Ubicaci�n fisica de la carpeta';
    //Estados Guias de Despacho
    ESTADO_ENVIADO = 'E';
    ESTADO_ACEPTADO = 'A';
    ESTADO_RECHAZADO = 'R';
    ESTADO_ACEPTADO_CON_OBSERVACIONES = 'O';

    // Tipo de tarjeta
    TARJETA_PRESTO      = 6;
    TARJETA_TRANSBANK   = 7;
    TARJETA_CMR         = 8;


    SYNCMOPT = 'SyncMopt';
    SYNCMOPT_INTERVALO = 'Intervalo';
    SYNCMOPT_ULTIDCONTRATO = 'UltIDContrato';
    SYNCMOPT_RESINCRONIZARDESDE = 'ResincronizarDesde';
    SYNCMOPT_RESINCRONIZARHASTA = 'ResincronizarHasta';
    SYNCMOPT_FTPCONTRASENIA = 'ServidorFTPcontrasenia';
    SYNCMOPT_FTPHOST = 'ServidorFTPHost';

    //Conceptos Facturacion
//    CONCEPTO_AJUSTE_PEAJE = 3;                                                //SS_1147_MCA_20150422
//    CONCEPTO_AJUSTE_INTERESES = 13;                                           //SS_1147_MCA_20150422
//    CONCEPTO_PAGO_TELEVIA = 21;                                               //SS_1147_MCA_20150422
//    CONCEPTO_PAGO_DANO_TELEVIA = 22;                                          //SS_1147_MCA_20150422
//    CONCEPTO_PAGO_SOPORTE = 23;                                               //SS_1147_MCA_20150422
//    CONCEPTO_DEVOLUCION_TAG = 24;                                             //SS_1147_MCA_20150422
//    CONCEPTO_CANCELACION_NOTACREDITO = 34;                                    //SS_1147_MCA_20150422
//    CONCEPTO_IMPR_FACT_DETALLADA = 05;                                        //SS_1147_MCA_20150422
//    CONCEPTO_REIMPR_COMPROBANTE = 06;                                         //SS_1147_MCA_20150422

    CONCEPTO_AJUSTE_PEAJE = 1003;                                               //SS_1147_MCA_20150422
    CONCEPTO_AJUSTE_INTERESES = 1013;                                           //SS_1147_MCA_20150422
    CONCEPTO_PAGO_TELEVIA = 1021;                                               //SS_1147_MCA_20150422
    CONCEPTO_PAGO_DANO_TELEVIA = 1022;                                          //SS_1147_MCA_20150422
    CONCEPTO_PAGO_SOPORTE = 1023;                                               //SS_1147_MCA_20150422
    CONCEPTO_DEVOLUCION_TAG = 1024;                                             //SS_1147_MCA_20150422
    CONCEPTO_CANCELACION_NOTACREDITO = 1034;                                    //SS_1147_MCA_20150422
    CONCEPTO_IMPR_FACT_DETALLADA = 1105;                                        //SS_1147_MCA_20150422
    CONCEPTO_REIMPR_COMPROBANTE = 1106;
    CONCEPTO_ITEM_FACTURAR = 1;

    //Documentos a imprimir
    IMPRIMIR_CONVENIO = 1;
    IMPRIMIR_ANEXOS   = 2;
    IMPRIMIR_MANDATO  = 3;
    IMPRIMIR_BAJA_CONVENIO = 4;
    REIMPRIMIR_MOTOS = 5;

    //Codigos de Respuesta Santander (Mandatos)
    CONST_RESPUESTA_MANDATO_PENDIENTE = 1;
    CONST_RESPUESTA_MANDATO_ACEPTADA = 2;
    CONST_RESPUESTA_MANDATO_RECHAZADA = 3;
    CONST_RESPUESTA_MANDATO_CANCELADA = 4;
    CONST_RESPUESTA_MANDATO_RESUELTA = 5;
    CONST_RESPUESTA_MANDATO_NO_RESUELTA = 6;

    //Estados del Medio de Pago Automatico
    CONST_ESTADO_MEDIO_PAGO_SIN_CAMBIO = 1;	//Sin Cambio
    CONST_ESTADO_ALTA_PAC              = 2;	//Alta PAC
    CONST_ESTADO_MODIF_PAC             = 3;	//Modificacion PAC
    CONST_ESTADO_BAJA_PAC              = 4;	//Baja PAC
	CONST_ESTADO_CAMBIO_PAT_PAC        = 5;	//Cambio de PAT a PAC
    CONST_ESTADO_ALTA_PAT              = 6;	//Alta PAT
    CONST_ESTADO_MODIF_PAT             = 7;	//Modificacion PAT
    CONST_ESTADO_BAJA_PAT              = 8;	//Baja PAT
	CONST_ESTADO_CAMBIO_PAC_PAT        = 9;	//Cambio de PAC a PAT

	// Mensajes
	CONST_CODIGO_MSG_PERDIDA_TAG = 20;

    (* Constantes para representar los Tipos de Medios de Envio de Documentaci�n. *)
    CONST_TIPO_MEDIO_ENVIO_CORREO_POSTAL        = 1;
    CONST_TIPO_MEDIO_ENVIO_CORREO_ELECTRONICO   = 2;

    (* Constantes para representar los Medios de Envio de Documentaci�n. *)
    CONST_MEDIO_ENVIO_DOCUMENTACION_COBRO   = 1;
    CONST_MEDIO_ENVIO_DETALLE_FACTURACION   = 2;
    CONST_MEDIO_ENVIO_INFORME_TRIMESTRAL    = 3;

    // Constante para representar la m�nima fecha v�lida para ingresar en los
    // procesos que realicen control de fechas.
    // 18264 es la fecha 01/01/1950.
    MINIMA_FECHA_VALIDA = 18264;

    // Estados Tr�nsitos
    CONST_ESTADO_TRANSITO_POSIBLE_INFRACTOR = 3;
    CONST_ESTADO_TRANSITO_LIMBO             = 2;    // SS_1374A_CQU_20151002

    // Constantes para los Eventos de Alarma
    CONST_ALARMA_MODULO_GESTION_INFRACCIONES = 'GestionInfracciones';
    CONST_ALARMA_MODULO_CAC_REVALIDACION     = 'CAC_Revalidacion';

    CONST_ALARMA_SEVERIDAD_3 = 3;

    // Constantes de Estados de Convenios en Lista Amarilla (tabla EstadosConveniosListaAmarilla)
    CONST_ESTADO_CONVENIO_LISTA_AMARILLA_PRE_EVALUACION = 1;    //  SS_660_CQU_20121010
    CONST_ESTADO_CONVENIO_LISTA_AMARILLA_CARTA_ENVIADA  = 2;    //  SS_660_CQU_20121010
    CONST_ESTADO_CONVENIO_LISTA_AMARILLA_INHABILITADO   = 3;    //  SS_660_CQU_20121010
    CONST_ESTADO_CONVENIO_LISTA_AMARILLA_PAGADO         = 4;    //  SS_660_CQU_20121010
    CONST_ESTADO_CONVENIO_LISTA_AMARILLA_ELIMINADO      = 5;    //  SS_660_CQU_20121010
	CONST_ESTADO_CONVENIO_LISTA_AMARILLA_RE_EVALUA  	= 6;    //  SS_660_CQU_20130711

    // Constantes con los motivos de restriccion de PAK
    CONST_CODIGO_MOTIVO_RESTRIC_PAK_PUEDE_ADHERIR           =   0;  // SS_1151_CQU_20140103    
    CONST_CODIGO_MOTIVO_RESTRIC_PAK_INFRACCIONES_PENDIENTES =   1;  // SS_1151_CQU_20140103
    CONST_CODIGO_MOTIVO_RESTRIC_PAK_TAGS_VENCIDOS           =   2;  // SS_1151_CQU_20140103
    CONST_CODIGO_MOTIVO_RESTRIC_PAK_CUENTAS_SUSPENDIDAS     =   3;  // SS_1151_CQU_20140103
    CONST_CODIGO_MOTIVO_RESTRIC_PAK_COMPROBANTES_IMPAGOS    =   4;  // SS_1151_CQU_20140103
    CONST_CODIGO_MOTIVO_RESTRIC_PAK_TTOO_SIN_TAG            =   5;  // SS_1151_CQU_20140103
    CONST_CODIGO_MOTIVO_RESTRIC_PAK_SIN_CUENTAS_ACTIVAS     =   6;  // SS_1151_CQU_20140103
    CONST_CODIGO_MOTIVO_RESTRIC_PAK_DEAUDA_REFINAN          =   7;  // SS_1151_CQU_20140103
    CONST_CODIGO_MOTIVO_RESTRIC_PAK_LISTA_AMARILLA          =   8;  // SS_1151_CQU_20140103
    CONST_CODIGO_MOTIVO_RESTRIC_PAK_BAJA_FORZADA            =   9;  // SS_1151_CQU_20140103

type
	{ Tipos B�sicos }

    TMsgError   = array[0..512] of char;

    TPatente					= String[10];
	TModalidadPago				= CHAR;
	TBandaISO					= String[40];

    TConfiabilidadCaracter = Packed record
		Caracter: CHAR;
		Confiabilidad: BYTE;
	end;

    TConfiabilidadPatente = Packed record
		Patente: AnsiString;			   //La Patente guardada con comodines en los caracteres de baja confiabilidad.
		Caracteres: Array[1..10, 1..4] of TConfiabilidadCaracter;
	end;


	// Estructura / Campo                 Tama�o 		Comentarios
	//----------------------------------- ------------- -----------------
	TMPStaticData = packed record  		// (8 bytes)
		Categoria: BYTE;                // 1 byte
		Reserva: array[0..6] of BYTE;	// 4 bytes
	end;

	TMPReadWriteData = packed record  	// (20 bytes)
		Saldo: DWORD;					// 4 bytes		En Centavos
		Estacion: BYTE;					// 1 byte
		Via: BYTE;						// 1 byte
		FechaHora: DWORD;				// 4 bytes		Segundos desde 1/1/1990 0:00
		Monto: DWORD;					// 4 bytes		En Centavos
		TipoSesion:	BYTE;				// 1 byte
		Reserva: Array[0..4] of BYTE;	// 5 bytes
	end;

	TMPHistoryData = packed record 		// (20 bytes)
		Saldo: DWORD;					// 4 bytes		En Centavos
		Estacion: BYTE;					// 1 byte
		Via: BYTE;						// 1 byte
		FechaHora: DWORD;				// 4 bytes		Segundos desde 1/1/1990 0:00
		Monto: DWORD;					// 4 bytes		En Centavos
		TipoSesion:	BYTE;				// 1 byte
		Reserva: Array[0..4] of BYTE;	// 5 bytes
	end;

	TMPRefillData = packed record  		// (16 bytes)
		Saldo: DWORD;					// 4 bytes		En Centavos
		Estacion: BYTE;					// 1 byte
		Via: BYTE;						// 1 byte
		FechaHora: DWORD;				// 4 bytes		Segundos desde 1/1/1990 0:00
		Monto: DWORD;					// 4 bytes		En Centavos
		TipoSesion:	BYTE;				// 1 byte
		Reserva: BYTE;					// 1 byte
	end;

	TMPAttribute = (mpStatic, mpReadWrite, mpHistory, mpRefill);
	TMPAttributes = set of TMPAttribute;

	TMPData = packed record				// (64 bytes)
		Static: TMPStaticData;			// 8 bytes
		ReadWrite: TMPReadWriteData;	// 20 bytes
		History: TMPHistorydata;		// 20 bytes
		Refill: TMPRefillData;			// 16 bytes
	end;

//******************************************************************************
// Gantry Date & Time
//******************************************************************************
Type
	TUnixDateTime = packed record
		Seconds: DWORD;
		Microseconds: DWORD;
	end;


type
	TDescripcionUrgencia = record
		CodigoUrgencia: string[2];
		Descripcion: AnsiString;
	end;

const
	MaxTarjetas = 10;
	MaxCheques = 10;
type

	TRecTarjeta = packed record
        TipoTarjeta:        Array[0..2] of char;
		DescriTipoTarjeta:  Array[0..40] of char; // solo para mostrar
		CodigoTarjeta:      integer;
		DescriTarjeta:      Array[0..40] of char; // solo para mostrar
		NroTarjeta:         Array[0..16] of char;
		Titular:            Array[0..40] of char;
		Seguridad:          Array[0..20] of char;
		Monto:              Double;
		MesVencimiento:     Integer;
		AnioVencimiento:    Integer;
		Cuotas:             Integer;
		Cupon:              Integer;
		Autorizacion:       Array[0..20] of char;
        FechaHoraOperacion: TDateTime;
	end;
  	TTarjetas		= array [0..MaxTarjetas] of TRecTarjeta;


	TRecCheque = packed record
		CodigoBanco:        integer;
		DescriBanco:        Array[0..40] of char; // solo para mostrar
		NumeroCuenta:       Array[0..16] of char;
		NumeroCheque:       Array[0..16] of char;
		Titular:            Array[0..40] of char;
		CodigoDocumento:    Array[0..4] of char;
		NumeroDocumento:    Array[0..20] of char;
		Monto:              Double;
		Autorizacion:       Array[0..20] of char;
        FechaHoraOperacion: TDateTime;
	end;
  	TCheques		= array [0..MaxCheques] of TRecCheque;


const
    MaxDomiciliosRelacionados = 2;

// Codigos de are� de Telefonos Celulares
    TIPO_LINEA_CELULAR = 'C';
    TIPO_LINEA_FIJA = 'F';

type

    TDatosDomicilio = Record
        IndiceDomicilio:integer; //utilizado en los programas por una grilla o algo as�
        Descripcion: AnsiString;
        CodigoDomicilio: integer;
        CodigoTipoDomicilio: integer;
        CodigoCalle: integer;
        CodigoSegmento: integer;
        CodigoTipoCalle: integer;
        NumeroCalle: integer;
        NumeroCalleSTR: String;
        Piso: integer;
        Dpto: AnsiString;
        Detalle: AnsiString;
        CodigoPostal: AnsiString;
        CodigoPais: AnsiString;
		CodigoRegion: AnsiString;
		CodigoComuna: AnsiString;
        CodigoCiudad: AnsiString;
        DescripcionCiudad: AnsiString;
        CalleDesnormalizada: AnsiString;
        DomicilioEntrega: boolean;
        CodigoPaisRelacionadoUno: AnsiString;
        CodigoRegionRelacionadoUno: AnsiString;
        CodigoComunaRelacionadoUno: AnsiString;
        CodigoCalleRelacionadaUno: integer;
        NumeroCalleRelacionadaUno: AnsiString;
        CodigoPaisRelacionadoDos: AnsiString;
        CodigoRegionRelacionadoDos: AnsiString;
        CodigoComunaRelacionadoDos: AnsiString;
        CodigoCalleRelacionadaDos: integer;
        NumeroCalleRelacionadaDos: AnsiString;
        Normalizado: boolean;
        Activo: boolean;
        Principal : boolean;
    end;
    TDomiciliosRelacionados = array[0..MaxDomiciliosRelacionados-1] of TDatosDomicilio;

Type
  TVehiculos = record
    CodigoVehiculo: Integer;
    Patente: string[10];
    DigitoPatente: string[1];
    CodigoTipoPatente: String;
    PatenteRVM: string[10];
    DigitoPatenteRVM: string[1];
    CodigoMarca: integer;
    Marca: string[30];
    Modelo: string[30];
    CodigoTipo: Integer;
    Tipo: string[40];
    Anio: string[4];
    Propiedad: string[30];
    //Revision 13
    Robado: Boolean;
    Recuperado: boolean;
    //Fin de Revision 13
    CodigoColor: Integer; //TASK_016_ECA_20160526
    Foraneo: Boolean; //20160603 MGO
  end;

type
    TIndicadorListas = record
        Verde: Boolean;
        Negra: Boolean;
        Gris: Boolean;
        Amarilla: Boolean;
    end;

    (* Estructura para mantener los datos de eximici�n de facturaci�n de
    tr�nsitos de la cuenta. *)
    TEximirFacturacion = record
        (* Indica si la eximici�n existe en la Base de datos. *)
        ExisteEnBaseDeDatos: Boolean;
        (* Fecha de Inicio Original del rango en el cual no se facturar�n los
        tr�nsitos. Este campo es necesario para cuando se necesitan modificar
        los datos de la eximici�n, se pueda encontrar el registro en la BD. *)
        FechaInicioOriginal: TDate;
        // Fecha de Inicio del rango en el cual no se facturar�n los tr�nsitos.
        FechaInicio: TDate;
        // Fecha de Finalizaci�n del rango en el cual no se facturar�n los tr�nsitos.
        FechaFinalizacion: TDate;
        // C�digo del Motivo por el cual no se faturar�n los tr�nsitos.
        CodigoMotivoNoFacturable: Integer;
        // Usuario que modific� los datos de la eximici�n.
        Usuario: AnsiString;
        // Fecha y Hora de modificaci�n de los datos de la eximici�n.
        FechaHoraModificacion: TDateTime;
        // Indica si la Fecha de Inicio se puede editar.
        FechaInicioEditable: Boolean;
        // Indica si la Fecha de Finalizaci�n se puede editar.
        FechaFinalizacionEditable: Boolean;
    end; // record

    (* Estructura para mantener los datos de bonificaci�n de facturaci�n de
    tr�nsitos de la cuenta. *)
    TBonificarFacturacion = record
        (* Indica si la bonificaci�n existe en la Base de datos. *)
        ExisteEnBaseDeDatos: Boolean;
        (* Fecha de Inicio Original del rango en el cual se bonificar�n los
        tr�nsitos. Este campo es necesario para cuando se necesitan modificar
        los datos de la bonificaci�n, se pueda encontrar el registro en la BD. *)
        FechaInicioOriginal: TDate;
        // Fecha de Inicio del rango en el cual se bonificar�n los tr�nsitos.
        FechaInicio: TDate;
        // Fecha de Finalizaci�n del rango en el cual se bonificar�n los tr�nsitos.
        FechaFinalizacion: TDate;
        (* Importe que se bonifica. El valor de este campo es excluyente con
        Porcentaje, es decir, si este campo tiene un valor v�lido, Porcentaje
        NO debe tener valor. *)
        Importe: Int64;
        (* Porcentaje que se bonifica. El valor de este campo es excluyente con
        Importe, es decir, si este campo tiene un valor v�lido, Importe
        NO debe tener valor. *)
        Porcentaje: Single;
        // Usuario que modific� los datos de la bonificaci�n.
        Usuario: AnsiString;
        // Fecha y Hora de modificaci�n de los datos de la bonificaci�n.
        FechaHoraModificacion: TDateTime;
        // Indica si la Fecha de Inicio se puede editar.
        FechaInicioEditable: Boolean;
        // Indica si la Fecha de Finalizaci�n se puede editar.
        FechaFinalizacionEditable: Boolean;
    end; // record

    TCuentaVehiculo = record
        Vehiculo: TVehiculos;
        IndiceVehiculo : Integer;
        ContextMark: Smallint;
        ContactSerialNumber: DWORD;
        Patente: string;
        TieneAcoplado: Boolean;
        //FechaModifCuenta: TDateTime; //Revision 15
        FechaBajaCuenta : TDateTime;
        FechaCreacion: TDateTime;
        FechaAltaTag: TDateTime;
        FechaBajaTag: TDateTime;
        FechaVencimientoTag: TDateTime;
        ContactSerialNumberOtraConcesionaria: DWORD;
        SituacionTag: Integer;
        EstadoConservacionTAG: integer;
        Suspendida: TDateTime;
        SuspensionForzada: Boolean;
        DeadlineSuspension: TDateTime;
        TagEnClienteOtroConvenio: Boolean;
        IndicadorListas: TIndicadorListas;
        (* Para mantener los datos de eximici�n de facturaci�n de tr�nsitos de
        la cuenta. *)
        EximirFacturacion: TEximirFacturacion;
        BonificarFacturacion: TBonificarFacturacion;
        CodigoAlmacenDestino: integer; //para saber si va a Arriendo o Comodato
        TipoAsignacionTag: string;
        TieneFechaRetroactiva: Boolean;
        //Agrego el estado para poder representar la suspensi�n
        EstaSuspendida: Boolean;
        // Rev.34/ 13-Abril-2010 / Nelson Droguett Sierra
        //HabilitadoAMB: Boolean;                           //SS-1006-NDR-20111105
        //HabilitadoPA:Boolean                              //SS-1006-NDR-20120614 //SS-1006-NDR-20111105
        //AdheridoPA:Boolean                                  //SS-1006-NDR-20120614 //TASK_004_ECA_20160411
        CodigoConvenioFacturacion :Integer;                   //TASK_004_ECA_20160411
        NumeroConvenioFacturacion :string;                    //TASK_004_ECA_20160411
        CodigoConvenioRNUT        :Integer;                   //TASK_009_ECA_20160506
        SerialNumberaMostrar      :string;                    //TASK_009_ECA_20160506
        DescripcionTipoVehiculo   :string;                    //TASK_009_ECA_20160506
        CodigoMotivoBaja          :Integer;                   //TASK_011_ECA_20160514
        DescripcionMotivoBaja     :string;                   //TASK_011_JMA_20160505
        DescripcionEstadoTAG      :string;                   //TASK_011_JMA_20160505
        SuscritoListaBlanca       :Boolean;                //TASK_051_ECA_20160707
        HabilitadoListaBlanca     :Boolean;                //TASK_051_ECA_20160707
    end;

    TRegCuentaVehiculos = Array of TCuentaVehiculo;

type
  TRegistroDocumentacion = record
{FIXME Luego sacar esto}       CodigoOrdenServicio : Integer;
        CodigoDocumentacionRespaldo:Integer;
        IndiceVehiculoRelativo:Shortint;
        TipoPatente:String;
        Patente:String;
        Marcado: boolean; //Posibilidades: 1 Que este Marcado y No corresponde en false, 2 No Corresponde y Marcado en false, Ambos en falso (falta seleccionar)
        NoCorresponde:Boolean;
        InfoAdicional:String;
        Obligatorio:Boolean;
        Orden: integer;
        SoloImpresion : Integer;
  end;
type
    TRegistroDocumentacionPorDoc = record
        DatosDocumento : TRegistroDocumentacion;
        ListaCodigoTipoOrdenServicio: array of integer;
    end;
type
    RegDocumentacionPresentada = array of TRegistroDocumentacion;

type
  TOrdenServicio = record
    CodigoTipoOrdenServicio: Integer;
    AccionRNUT : Integer;
    Patente: String;
    ContactSerialNumer: Integer;
    Documentacion: RegDocumentacionPresentada;
  end;

type
    ATOrdenServicio = array of TOrdenServicio;

type
    ATRegistroDocumentacionPorDoc = array of TRegistroDocumentacionPorDoc;

type
  TTipoMedioComunicacion = record
    CodigoTipoMedioContacto:Integer;
    CodigoArea:Integer;
    Valor:String;
    Anexo:Integer;
    HoraDesde:TDateTime;
    HoraHasta:TDateTime;
    Indice:Integer;
    Principal: Boolean;
  end;

Type

  TDatosPersona = record
    Personeria: string[10];
    Documento: String[9];
    RazonSocial: string[60];
    Nombre: string[60];
    Apellido: string[60];
    ApellidoMaterno: string[30];
    Sexo: string[10];
    FechaNacimiento: string[30];
    Email: string[30];
    TelefonoPrincipal: string[30];
    TelefonoAlternativo: string[30];
    Domicilio: string[60];
  end;

  // Tipos Medios de Pagos
  TPAC = record
    TipoCuenta : Integer;
    CodigoBanco  : Integer;
    NroCuentaBancaria  : String[30];
    Sucursal   : String[50];
    NumeroCheque     : String[20];
    Confirmado: Boolean;
  end;

  TPAT = record
    TipoTarjeta     : Integer;
    EmisorTarjetaCredito: Integer;
    NroTarjeta      : String[20];
    FechaVencimiento: String[5];
    Cuotas          : integer;
    Confirmado: Boolean;    
  end;

  TTiposMediosPago = (NINGUNO, PAT, PAC, EFECTIVO, CHEQUE, TDEBITO, TCREDITO);
  TDatosMediosPago = record
    TipoMedioPago: TTiposMediosPago;
    PAC: TPAC;
    PAT: TPAT;
    RUT: String[11];
    Nombre: String[100];
    Telefono:String[30];
    CodigoArea: integer;
    CodigoPersona:Integer;
    //Datos utilizados para efectuar un pago, no se piden en el alta de un convenio
    Cupon: integer;
    Autorizacion: string[20];
    Monto: Int64;
  end;


Type
	// IMPORTANTE: No cambiar el orden de estos enumerados. Si se agregan items,
	// deben agregarse al final.
	TEstadoSolicitud = (esIncorrecto, esPendiente, esResueltaInsatisfecho,
	  esResueltaSatisfecho, esResueltaInjustificado);
	TDescripcionEstado = record
		CodigoEstado: string[2];
		Descripcion: AnsiString;
	end;


const
	DescripcionEstado: Array[TEstadoSolicitud] of TDescripcionEstado = (
	  (CodigoEstado: ''; Descripcion: 'Estado Incorrecto'),
	  (CodigoEstado: 'PE'; Descripcion: 'Pendiente'),
	  (CodigoEstado: 'RI'; Descripcion: 'Resuelto Insatisfecho'),
	  (CodigoEstado: 'RS'; Descripcion: 'Resuelto-Satisfecho'),
	  (CodigoEstado: 'RN'; Descripcion: 'Reclamo Injustificado'));

    // Son todos los componentes que pueden contener datos, y ser seteados como obigatorios

	TMPCOMPONENTESDATO: Array[1..3] of String = ('TCustomEdit','TCustomComboBox','TDBLookupControl');
    TMPCOMPONENTESHINT: Array[1..5]  of String = ('TCustomEdit','TCustomComboBox','TButton','TCustomListBoxEx','TDBLookupControl');

type
    TTipoOperacionConvenio = (
        tocALTA_CONVENIO        ,
        tocMODIF_TITULAR_MAYOR  ,
        tocMODIF_TITULAR_MENOR  ,
        tocMODIF_REP_LEGAL_1    ,
        tocMODIF_REP_LEGAL_2    ,
        tocMODIF_REP_LEGAL_3    ,
        tocALTA_REP_LEGAL_1     ,
        tocALTA_REP_LEGAL_2     ,
        tocALTA_REP_LEGAL_3     ,
        tocMODIF_DOMICILIO_FAC  ,
        tocMODIF_DOMICILIO_CLI  ,
        tocMODIF_TIPO_MANDATE_PAT ,
        tocMODIF_TIPO_MANDATE_PAC ,
        tocMODIF_TIPO_MANDATE_NIN ,
        tocMODIF_VEHICULO_MENOR ,
        tocMODIF_VEHICULO_MAYOR ,
        tocMODIF_BAJA_CUENTA    ,
        tocMODIF_ALTA_CUENTA    ,
        tocBAJA_VEHICULO        ,
        tocALTA_VEHICULO        ,
        tocBAJA_REP_LEGAL_1     ,
        tocBAJA_REP_LEGAL_2     ,
        tocBAJA_REP_LEGAL_3     ,
        tocBAJA_CONVENIO        ,
        tocALTA_MEDIO_CONTACTO_TELEFONO_1  ,
        tocMODIF_MEDIO_CONTACTO_TELEFONO_1 ,
        tocBAJA_MEDIO_CONTACTO_TELEFONO_1,
        tocALTA_MEDIO_CONTACTO_TELEFONO_2  ,
        tocMODIF_MEDIO_CONTACTO_TELEFONO_2 ,
        tocBAJA_MEDIO_CONTACTO_TELEFONO_2,
        tocALTA_MEDIO_CONTACTO_EMAIL,
        tocMODIF_MEDIO_CONTACTO_EMAIL,
        tocBAJA_MEDIO_CONTACTO_EMAIL,
        tocSUSPENDIDO,
        tocACTIVACION_VEHICULO,
        tocPERDIDA_TAG,
        tocALTA_CONVENIO_SIN_CUENTA,
        tocALTA_SIN_CUENTA_REP_LEGAL_1,
        tocALTA_SIN_CUENTA_REP_LEGAL_2,
        tocALTA_SIN_CUENTA_REP_LEGAL_3
    );

type
  TRegOperacionConvenio = record
                  CodigoOperacion: TTipoOperacionConvenio;
                  CodigoTipoOperacionConvenio: Integer;
                  AccionRNUT: Byte;
              end;

const
  TOperacionConvenio: array[TTipoOperacionConvenio] of TRegOperacionConvenio = (
      (CodigoOperacion: tocALTA_CONVENIO; CodigoTipoOperacionConvenio: 1; AccionRNUT: 1),
      (CodigoOperacion: tocMODIF_TITULAR_MAYOR; CodigoTipoOperacionConvenio: 2; AccionRNUT: 2),
      (CodigoOperacion: tocMODIF_TITULAR_MENOR; CodigoTipoOperacionConvenio: 30; AccionRNUT: 2),
      (CodigoOperacion: tocMODIF_REP_LEGAL_1; CodigoTipoOperacionConvenio: 41; AccionRNUT: 2),
      (CodigoOperacion: tocMODIF_REP_LEGAL_2; CodigoTipoOperacionConvenio: 42; AccionRNUT: 2),
      (CodigoOperacion: tocMODIF_REP_LEGAL_3; CodigoTipoOperacionConvenio: 43; AccionRNUT: 2),
      (CodigoOperacion: tocALTA_REP_LEGAL_1; CodigoTipoOperacionConvenio: 51; AccionRNUT: 1),
      (CodigoOperacion: tocALTA_REP_LEGAL_2; CodigoTipoOperacionConvenio: 52; AccionRNUT: 1),
      (CodigoOperacion: tocALTA_REP_LEGAL_3; CodigoTipoOperacionConvenio: 53; AccionRNUT: 1),
      (CodigoOperacion: tocMODIF_DOMICILIO_FAC; CodigoTipoOperacionConvenio: 6; AccionRNUT: 2),
      (CodigoOperacion: tocMODIF_DOMICILIO_CLI; CodigoTipoOperacionConvenio: 5; AccionRNUT: 2),
      (CodigoOperacion: tocMODIF_TIPO_MANDATE_PAT; CodigoTipoOperacionConvenio: 91; AccionRNUT: 0),
      (CodigoOperacion: tocMODIF_TIPO_MANDATE_PAC; CodigoTipoOperacionConvenio: 92; AccionRNUT: 0),
      (CodigoOperacion: tocMODIF_TIPO_MANDATE_NIN; CodigoTipoOperacionConvenio: 93; AccionRNUT: 0),
      (CodigoOperacion: tocMODIF_VEHICULO_MENOR; CodigoTipoOperacionConvenio: 11; AccionRNUT: 2),
      (CodigoOperacion: tocMODIF_VEHICULO_MAYOR; CodigoTipoOperacionConvenio: 12; AccionRNUT: 2),
      (CodigoOperacion: tocMODIF_BAJA_CUENTA; CodigoTipoOperacionConvenio: 13; AccionRNUT: 3),
      (CodigoOperacion: tocMODIF_ALTA_CUENTA; CodigoTipoOperacionConvenio: 14; AccionRNUT: 1),
      (CodigoOperacion: tocBAJA_VEHICULO; CodigoTipoOperacionConvenio: 15; AccionRNUT: 3),
      (CodigoOperacion: tocALTA_VEHICULO; CodigoTipoOperacionConvenio: 16; AccionRNUT: 1),
      (CodigoOperacion: tocBAJA_REP_LEGAL_1; CodigoTipoOperacionConvenio: 61; AccionRNUT: 3),
      (CodigoOperacion: tocBAJA_REP_LEGAL_2; CodigoTipoOperacionConvenio: 62; AccionRNUT: 3),
      (CodigoOperacion: tocBAJA_REP_LEGAL_3; CodigoTipoOperacionConvenio: 63; AccionRNUT: 3),
      (CodigoOperacion: tocBAJA_CONVENIO; CodigoTipoOperacionConvenio: 100; AccionRNUT: 3),
      (CodigoOperacion: tocALTA_MEDIO_CONTACTO_TELEFONO_1; CodigoTipoOperacionConvenio: 70; AccionRNUT: 1),
      (CodigoOperacion: tocMODIF_MEDIO_CONTACTO_TELEFONO_1; CodigoTipoOperacionConvenio: 71; AccionRNUT: 2),
      (CodigoOperacion: tocBAJA_MEDIO_CONTACTO_TELEFONO_1; CodigoTipoOperacionConvenio: 72; AccionRNUT: 3),
      (CodigoOperacion: tocALTA_MEDIO_CONTACTO_TELEFONO_2; CodigoTipoOperacionConvenio: 73; AccionRNUT: 1),
      (CodigoOperacion: tocMODIF_MEDIO_CONTACTO_TELEFONO_2; CodigoTipoOperacionConvenio: 74; AccionRNUT: 2),
      (CodigoOperacion: tocBAJA_MEDIO_CONTACTO_TELEFONO_2; CodigoTipoOperacionConvenio: 75; AccionRNUT: 3),
      (CodigoOperacion: tocALTA_MEDIO_CONTACTO_EMAIL; CodigoTipoOperacionConvenio: 76; AccionRNUT: 1),
      (CodigoOperacion: tocMODIF_MEDIO_CONTACTO_EMAIL; CodigoTipoOperacionConvenio: 77; AccionRNUT: 2),
      (CodigoOperacion: tocBAJA_MEDIO_CONTACTO_EMAIL; CodigoTipoOperacionConvenio: 78; AccionRNUT: 3),
      (CodigoOperacion: tocSUSPENDIDO; CodigoTipoOperacionConvenio: 17; AccionRNUT: 3),
      (CodigoOperacion: tocACTIVACION_VEHICULO; CodigoTipoOperacionConvenio: 18; AccionRNUT: 1),
      (CodigoOperacion: tocPERDIDA_TAG; CodigoTipoOperacionConvenio: 19; AccionRNUT: 3),
      (CodigoOperacion: tocALTA_CONVENIO_SIN_CUENTA; CodigoTipoOperacionConvenio: 81; AccionRNUT: 0),
      (CodigoOperacion: tocALTA_SIN_CUENTA_REP_LEGAL_1; CodigoTipoOperacionConvenio: 82; AccionRNUT: 0),
      (CodigoOperacion: tocALTA_SIN_CUENTA_REP_LEGAL_2; CodigoTipoOperacionConvenio: 83; AccionRNUT: 0),
      (CodigoOperacion: tocALTA_SIN_CUENTA_REP_LEGAL_3; CodigoTipoOperacionConvenio: 84; AccionRNUT: 0));

type

    TDatosPersonales = Record
        CodigoPersona   : integer;
        RazonSocial     : AnsiString;
        Personeria      : Char;
        TipoDocumento   : AnsiString;
        NumeroDocumento : AnsiString;
    	Apellido        : AnsiString;
        ApellidoMaterno : AnsiString;
    	Nombre          : Ansistring;
		Sexo            : char;
        Giro            : AnsiString;
        LugarNacimiento : AnsiString;
		FechaNacimiento : TDateTime;
    	CodigoActividad : integer;
        Password        : AnsiString;
        Pregunta        : AnsiString;
        Respuesta       : AnsiString;
        TipoCliente     : Integer;
        RecibeClavePorMail: boolean;
        InactivaDomicilioRNUT    : Boolean;
        InactivaMedioComunicacionRNUT   : Boolean;
        Semaforo1 : Integer;
        Semaforo3 : Integer;
        InicializarNotificacionesTAGs: Boolean; // SS_960_PDO_20110608
        TieneNotificacionesTAGs: Boolean;       // SS_960_PDO_20110608
        AdheridoPA: Boolean;                    //SS-1006-NDR-20120715
    end;

    TDatosContactoComunicacion = Record
        CodigoComunicacion: integer;
        Persona: TDatosPersonales;
        Domicilio: AnsiString;
        NumeroCalle: AnsiString;
        Telefono: AnsiString;
        Email: AnsiString;
    end;

    TDatosComunicacion = record
        CodigoComunicacion: Integer;
        CodigoPersona: Integer;
        CodigoUsuario: String;
        FechaHoraInicio: TDatetime;
        FechaHoraFin: TDatetime;
        MotivoContacto: Byte;
        CodigoFuenteComunicacion: Byte;
        Estado: String[1];
    end;

    TDatosTurno = Record
        NumeroTurno: longint;
        CodigoUsuario: string;
        FechaHoraApertura: TDatetime;
        FechaHoraCierre: TDatetime;
        CodigoPuntoVenta: integer;
        Estado: char;
        IdEstacion : string;
    end;

type
  TRegistroMedioEnvioDocumentosCobro = record
        CodigoMedioEnvioDocumento:Integer;
        CodigoTipoMedioEnvio:Integer;
  end;

// Variables Globales
var
    GNumeroTurno: Integer;
    GCodigoComunicacion: Integer;
type
    TTipoHoja = (tthNinguno, tthConvenio, tthBlanco);
    TMotivoCancelacion = (OK, ERROR, CANCELADO_USR);

type
  //TPuntoCobroCN = record          // SS_1147_CQU_20140408
  TPuntoCobro = record              // SS_1147_CQU_20140408
    //CodigoCN: Integer;            // SS_1147_CQU_20140408
    CodigoConcesionaria : Integer;  // SS_1147_CQU_20140408
    Sentido: Integer;
    EsTransito: Boolean;
    X: Integer;                 // SS_1409_CQU_20151102
    Y: Integer;                 // SS_1409_CQU_20151102
    NumeroPuntoCobro : Integer; // SS_1409_CQU_20151102
  end;

  //TListaPuntosCobro = Array[1..17] of TPuntoCobroCN;  // SS_1091_CQU_20130516
  //TListaPuntosCobro = Array[1..19] of TPuntoCobroCN;  // SS_1158_CQU_20140109  // SS_1091_CQU_20130516
  //TListaPuntosCobro = Array of TPuntoCobroCN;         // SS_1147_CQU_20140408  // SS_1158_CQU_20140109
  TListaPuntosCobro = Array of TPuntoCobro;             // SS_1147_CQU_20140408

  TStateConvenio = (scAlta, scBaja, scModi, scNormal, scSuspendido);

  TStateFrmVehiculo = (Solicitud, ConvenioAltaVehiculo, ConvenioModiVehiculo, ConvenioCambiarVehiculo, ConvenioActivarCuenta);

  TOrigenImpresion = (oiSistema,oiJordan,oiWeb,oiServicio);                     //SS_1307_NDR_20150408


    // Tipos de Operacion Convenio

type
    TRCheckListConvenio = record
        CodigoCheckList: integer;
        Descripcion: String;
        Marcado: boolean;
    end;

    TACheckListConvenio = array of TRCheckListConvenio;

type
    TRCheckListCuenta = record
        CodigoCheckList: integer;
        IndiceVehiculo: Integer;
        Descripcion: String;
        Marcado: boolean;
        Vencimiento: TDateTime;
        PuedeCambiarOperador: Boolean;
        Eliminar: Boolean;
        Patente: String[10];
    end;

    TACheckListCuenta = array of TRCheckListCuenta;


implementation

end.
