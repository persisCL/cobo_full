object formDocPresentada: TformDocPresentada
  Left = 213
  Top = 94
  BorderStyle = bsDialog
  Caption = 'Documentaci'#243'n Presentada'
  ClientHeight = 486
  ClientWidth = 621
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object gb_Convenio: TGroupBox
    Left = 0
    Top = 0
    Width = 621
    Height = 447
    Align = alClient
    Caption = 'Documentaci'#243'n del Convenio y de los vehiculos'
    TabOrder = 0
    DesignSize = (
      621
      447)
    object sb_Convenio: TScrollBox
      Left = 16
      Top = 24
      Width = 592
      Height = 412
      Anchors = [akLeft, akTop, akRight, akBottom]
      TabOrder = 0
    end
  end
  object Panel10: TPanel
    Left = 0
    Top = 447
    Width = 621
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      621
      39)
    object btn_Cancelar: TButton
      Left = 335
      Top = 8
      Width = 75
      Height = 25
      Hint = 'Cancelar'
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = '&Cancelar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = btn_CancelarClick
    end
    object btn_ok: TButton
      Left = 249
      Top = 8
      Width = 75
      Height = 25
      Hint = 'Documentaci'#243'n presentada en forma correcta'
      Anchors = [akRight, akBottom]
      Caption = '&Aceptar'
      Default = True
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = btn_okClick
    end
  end
  object sp: TADOStoredProc
    Connection = DMConnections.BaseCAC
    Parameters = <>
    Left = 680
    Top = 24
  end
end
