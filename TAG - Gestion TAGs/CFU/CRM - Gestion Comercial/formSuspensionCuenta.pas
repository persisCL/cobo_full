{-----------------------------------------------------------------------------
 Unit Name: formSuspensionCuenta
 Author:
 Date:
 Description:
 History:
 Revision 1:
    Author :   lcanteros
    Date Created: 22-04-2008
    Language : ES-AR
    Description : Se agrega la confirmacion de la patente con su digito verificador
    por parte del operador.

 Revision 1
 Author: mbecerra
 Date: 05-Febrero-2010
 Description:		Se corrige la obtenci�n de la fecha de suspensi�n, para que
                	busque un d�a h�bil.

 Revision 2
 Author: mpiazza
 Date: 18-06-2010
 Description:   SS-894 para la eliminaci�n del DV de la pantalla Suspensi�n de Cuenta
                se borra edit del digito verificador
                y se modifica el codigo btn_AceptarClick

 Firma        : SS_1156_NDR_20140203
 Descripcion  : Permitir pegar en la patente
-----------------------------------------------------------------------------}
unit formSuspensionCuenta;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, UtilProc, RStrings,
  Dialogs, StdCtrls, Validate, DateEdit, Util, ConstParametrosGenerales, DMConnection, PeaProcs,
  ExtCtrls, Clipbrd;											//SS_1156_NDR_20140203

type
  TTipoEdicion = (teSuspender, teEditar);
  TfrmSuspensionCuenta = class(TForm)
    ch_Notificado: TCheckBox;
    txt_FechaDeadline: TDateEdit;
    Label1: TLabel;
    pnlAbajo: TPanel;
    btn_Aceptar: TButton;
    btn_Cancelar: TButton;
    gbDatosPatente: TGroupBox;
    txtPatente: TEdit;
    lblPatente: TLabel;
    procedure btn_CancelarClick(Sender: TObject);
    procedure btn_AceptarClick(Sender: TObject);
    procedure txtPatenteKeyPress(Sender: TObject; var Key: Char);
    procedure txtDigitoVerificadorKeyPress(Sender: TObject; var Key: Char);
    procedure txtPatenteKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);	//SS_1156_NDR_20140203
  private
    FTipoEdicion: TTipoEdicion;
    FPatente, FDigitoVerificador: string;
    function GetNotificadoPorCliente: Boolean;
    function GetFechaLimite: TDatetime;
    { Private declarations }
  public
    { Public declarations }
    property Patente: string read FPatente write FPatente;
    property DigitoVerificador: string read FDigitoVerificador write FDigitoVerificador;
    property NotificadoPorCliente: Boolean read GetNotificadoPorCliente;
	property FechaLimite: TDatetime read GetFechaLimite;
    function Inicializar(TipoEdicion: TTipoEdicion; FechaDeadline: TDateTime; Voluntaria: Boolean): Boolean; overload;
    function Inicializar(AuxPatente, Digito:string): Boolean; overload;

  end;

var
  frmSuspensionCuenta: TfrmSuspensionCuenta;

implementation

{$R *.dfm}

resourcestring
	STR_SUSPENSION_CUENTA = 'Suspensi�n de Cuenta';

{ TfrmSuspensionCuenta }

function TfrmSuspensionCuenta.GetFechaLimite: TDatetime;
begin
    Result := txt_FechaDeadline.Date;
end;

function TfrmSuspensionCuenta.GetNotificadoPorCliente: Boolean;
begin
    Result := ch_Notificado.Checked;
end;

function TfrmSuspensionCuenta.Inicializar(TipoEdicion: TTipoEdicion; FechaDeadline: TDateTime; Voluntaria: Boolean): Boolean;
var
	nDiasDeadLine : integer;
begin
	try
    	// Por default la fecha limite es 30 dias a partir de la fecha de hoy
    	ObtenerParametroGeneral(DMConnections.BaseCAC, 'DEADLINE_SUSPENSION', nDiasDeadLine);

        //---------REV.1
    	//if FechaDeadline = NullDate then txt_FechaDeadline.Date := NowBase(DMConnections.BaseCAC) + nDiasDeadLine
        //else txt_FechaDeadline.Date := FechaDeadline;
        if FechaDeadline = NullDate then begin
            FechaDeadline := NowBase(DMConnections.BaseCAC) + nDiasDeadLine;
            while not EsDiaHabil(DMConnections.BaseCAC, FechaDeadline)do FechaDeadline := FechaDeadline + 1;
            txt_FechaDeadline.Date := FechaDeadline;
        end
        else txt_FechaDeadline.Date := FechaDeadline;
        //----FIN REV.1


    	ch_Notificado.Checked := Voluntaria;

        FTipoEdicion := TipoEdicion;

    	Result := True;
    except
    	on e: exception do begin
			MsgBoxErr(fORMAT(MSG_ERROR_INICIALIZACION, [STR_SUSPENSION_CUENTA]), e.Message, STR_ERROR, MB_ICONERROR);
            Result := False;
		end;
    end;
end;

procedure TfrmSuspensionCuenta.btn_CancelarClick(Sender: TObject);
begin
    close;
end;
{-----------------------------------------------------------------------------
Procedure: btn_AceptarClick
Author:
Date:
Arguments: Sender: TObject
Result:    None
Description: Realiza comprobaciones y cierra el formulario si todo es correcto
History:

Revision 1
Author: mpiazza
Date: 18-06-2010
Description: SS-894 para la eliminaci�n del DV de la pantalla Suspensi�n de Cuenta
             se cambia validacion de controles

-----------------------------------------------------------------------------}
procedure TfrmSuspensionCuenta.btn_AceptarClick(Sender: TObject);
resourcestring
    MSG_ERROR_DIA_NO_HABIL = 'La fecha l�mite de Suspensi�n debe ser un d�a h�bil';
    MSG_ERROR_DIA_FERIADO  = 'La fecha l�mite de Suspensi�n no debe ser un d�a feriado';
    MSG_ERROR_PATENTE_SELECCIONADA = 'La patente no coincide con el veh�culo seleccionado';
    MSG_ERROR_DIGITO_VERIFICADOR_SELECCIONADO = 'El digito verificador no coincide con la patente seleccionada';
var
    Detalle: String;
begin
    if StrToDate(DateToStr(FechaLimite)) < StrToDate(DateToStr(NowBase(DMConnections.BaseCAC))) then begin
        MsgBoxBalloon(MSG_ERROR_Suspension, Caption, MB_ICONSTOP, txt_FechaDeadline);
        txt_FechaDeadline.SetFocus;
        exit;
    end;

    if not EsDiaHabil(DMConnections.BaseCAC, FechaLimite) then begin
        MsgBoxBalloon(MSG_ERROR_DIA_NO_HABIL, Caption, MB_ICONSTOP, txt_FechaDeadline);
        txt_FechaDeadline.SetFocus;
        exit;
    end;

    if FTipoEdicion = teEditar then  //Cuando edita la fecha de susp, ademas valido que no sea feriado
        if EsDiaFeriado(DMConnections.BaseCAC, FechaLimite, Detalle) then begin
            MsgBoxBalloon(MSG_ERROR_DIA_FERIADO, Caption, MB_ICONSTOP, txt_FechaDeadline);
            txt_FechaDeadline.SetFocus;
            exit;
        end;

    if not ValidateControls(
        [txtPatente,
        txtPatente],
        [txtPatente.Text<>EmptyStr,
        trim(txtPatente.Text) = trim(FPatente)],
        CAPTION_VALIDAR_DATOS_VEHICULO,
        [MSG_ERROR_VALIDAR_PATENTE,
        MSG_ERROR_PATENTE_SELECCIONADA])
        then begin

        Exit;
    end;

    ModalResult := mrOk;
end;

function TfrmSuspensionCuenta.Inicializar(AuxPatente, Digito:string): Boolean;
begin
    FPatente := AuxPatente;
    FDigitoVerificador := Digito;
    Result := Inicializar(teSuspender, nulldate, false);
end;
{-----------------------------------------------------------------------------
  Procedure: txtDigitoVerificadorKeyPress
  Author:    lcanteros
  Date:      23-Abr-2008
  Arguments: Sender: TObject; var Key: Char
  Result:    None
  Description: valida que sean letras y numeros solamente
  History:
-----------------------------------------------------------------------------}
procedure TfrmSuspensionCuenta.txtDigitoVerificadorKeyPress(Sender: TObject;
  var Key: Char);
begin
    Key := PermitirSoloLetrasNumeros(Key);
end;
{-----------------------------------------------------------------------------
  Procedure: txtPatenteKeyPress
  Author:    lcanteros
  Date:      23-Abr-2008
  Arguments: Sender: TObject; var Key: Char
  Result:    None
  Description: vaslida que sean numeros solamente
  History:
-----------------------------------------------------------------------------}
procedure TfrmSuspensionCuenta.txtPatenteKeyPress(Sender: TObject;
  var Key: Char);
begin
    Key := PermitirSoloLetrasNumeros(Key);
end;


//BEGIN : SS_1156_NDR_20140203--------------------------------------------------
procedure TfrmSuspensionCuenta.txtPatenteKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   if ((ssCtrl in Shift) AND (Key = ord('V'))) then
   begin
     if Clipboard.HasFormat(CF_TEXT) then
     begin
       txtPatente.Text:=Clipboard.AsText;
     end;
     Key := 0;
   end;
end;
//END : SS_1156_NDR_20140203--------------------------------------------------

end.
