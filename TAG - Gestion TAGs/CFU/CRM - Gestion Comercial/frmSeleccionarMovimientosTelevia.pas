{ -------------------------------------------------------------------------

	Revision 1
    Author: mbecerra
    Date: 23-Dic-2008
    Description:	Se agrega el campo IDMotivoMovimientoCuentaTelevia al stored y
                    al ClientDataSet.

                    29-Dic-2008, si el almacen que viene es arriendo en cuotas,
                    se cambia por Arriendo, para que el stored me traiga los conceptos asociados.

	Revision 2
    Author: Nelson Droguett Sierra
    Date: 03-07-2009
    Description:  si el almacen que viene es arriendo en cuotas,
                   YA NO se cambia por Arriendo
---------------------------------------------------------------------------}
unit frmSeleccionarMovimientosTelevia;

interface

uses
	PeaTypes,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ListBoxEx, DBListEx, StdCtrls, Util, UtilProc, UtilDB, DMConnection,
  DB, ADODB, DBClient, ImgList;

type
  TfrmSeleccionarMovimientosTeleviaFORM = class(TForm)
    btnAceptar: TButton;
    btnCancelar: TButton;
    dblConceptos: TDBListEx;
    spConceptos: TADOStoredProc;
    cdConceptos: TClientDataSet;
    dsConceptos: TDataSource;
    ilImages: TImageList;
    procedure dblConceptosDrawText(Sender: TCustomDBListEx;
      Column: TDBListExColumn; Rect: TRect; State: TOwnerDrawState;
      var Text: string; var TxtRect: TRect; var ItemWidth: Integer;
      var DefaultDraw: Boolean);
    procedure dblConceptosDblClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
  private
    FSeleccionados : Integer;
    FLista: TClientDataSet;
    function ObtenerConceptos(const Operacion: Integer; Almacen: Integer; TipoAsignacion: string): Boolean;
    procedure CargarDataset;
  public
    function Inicializar(const OperacionTag:Integer; const CodigoAlmacen: Integer; const TipoAsignacion: string): Boolean;
    property ListaConceptos: TClientDataSet read FLista;
  end;

var
  frmSeleccionarMovimientosTeleviaFORM: TfrmSeleccionarMovimientosTeleviaFORM;

implementation

{$R *.dfm}

{ TfrmSeleccionarMovimientosTeleviaFORM }

procedure TfrmSeleccionarMovimientosTeleviaFORM.btnAceptarClick(
  Sender: TObject);
begin
    FLista := cdConceptos;
end;

procedure TfrmSeleccionarMovimientosTeleviaFORM.btnCancelarClick(
  Sender: TObject);
begin
    Close;
end;

procedure TfrmSeleccionarMovimientosTeleviaFORM.CargarDataset;
var
    Comentario: String;
begin
    cdConceptos.CreateDataSet;
    Comentario := EmptyStr;
    while not spConceptos.Eof do begin
        // Verificamos los motivos por los cuales este concepto podr�a ser
        // no seleccionable para determinar el comentario que vamos a mostrar
        if (spConceptos.FieldByName('CodigoConcepto').AsInteger > 0) and
          (spConceptos.FieldByName('PrecioOrigen').AsInteger = 0) and
          (spConceptos.FieldByName('Cotizacion').AsFloat = 0) then begin
            Comentario := 'No existe precio ni cotizaci�n'
        end else if(spConceptos.FieldByName('CodigoConcepto').AsInteger > 0) and
          (spConceptos.FieldByName('PrecioOrigen').AsInteger = 0) then begin
            Comentario := 'Concepto sin precio';
        end else if (spConceptos.FieldByName('CodigoConcepto').AsInteger > 0) and
          (spConceptos.FieldByName('Cotizacion').AsFloat = 0) then begin
            Comentario := 'Sin cotizaci�n';
        end;
        // Insertamos nuestro registro
        cdConceptos.AppendRecord([False, spConceptos.FieldByName('CodigoConcepto').AsInteger,
          spConceptos.FieldByName('DescripcionMotivo').AsString,
          spConceptos.FieldByName('PrecioOrigen').AsInteger,
          spConceptos.FieldByName('Moneda').AsString,
          spConceptos.FieldByName('Cotizacion').AsFloat,
          spConceptos.FieldByName('PrecioEnPesos').AsInteger, Comentario,
		  //Revision 1
          spConceptos.FieldByName('IDMotivoMovimientoCuentaTelevia').AsInteger]);
        spConceptos.Next;
        Comentario := EmptyStr;
    end;
end;

procedure TfrmSeleccionarMovimientosTeleviaFORM.dblConceptosDblClick(
  Sender: TObject);
begin
    if cdConceptos.FieldByName('Comentario').AsString <> EmptyStr then Exit;
    cdConceptos.Edit;
    if cdConceptos.FieldByName('Seleccionado').AsBoolean then begin
        cdConceptos.FieldByName('Seleccionado').AsBoolean := False;
        Dec(FSeleccionados);
    end else begin
        cdConceptos.FieldByName('Seleccionado').AsBoolean := True;
        Inc(FSeleccionados);
    end;
    cdConceptos.Post;
    btnAceptar.Enabled := (FSeleccionados > 0);
end;

procedure TfrmSeleccionarMovimientosTeleviaFORM.dblConceptosDrawText(
  Sender: TCustomDBListEx; Column: TDBListExColumn; Rect: TRect;
  State: TOwnerDrawState; var Text: string; var TxtRect: TRect;
  var ItemWidth: Integer; var DefaultDraw: Boolean);
begin
    if Column.FieldName = 'Seleccionado' then begin
          if cdConceptos.FieldByName('Comentario').AsString <> EmptyStr then begin
            ilImages.Draw(Sender.Canvas, Rect.Left, Rect.Top, 2);
          end else if cdConceptos.FieldByName('Seleccionado').AsBoolean = True then begin
              ilImages.Draw(Sender.Canvas, Rect.Left, Rect.Top, 1);
          end else if cdConceptos.FieldByName('Seleccionado').AsBoolean = False then begin
              ilImages.Draw(Sender.Canvas, Rect.Left, Rect.Top, 0);
          end;
          DefaultDraw := False;
    end;
end;

function TfrmSeleccionarMovimientosTeleviaFORM.Inicializar(const OperacionTag:Integer; const CodigoAlmacen: Integer; const TipoAsignacion: string): Boolean;
var
	AlmacenAux : integer;
begin
    FLista := nil;
    FSeleccionados := 0;
    //Rev 1
    AlmacenAux := CodigoAlmacen;
    // Rev.2 / 03-07-2009 / Nelson Droguett Sierra -----------------------------------------------
    //if AlmacenAux = CONST_ALMACEN_ENTREGADO_EN_CUOTAS then AlmacenAux := CONST_ALMACEN_ARRIENDO;
    // -------------------------------------------------------------------------------------------

    Result := ObtenerConceptos(OperacionTag, AlmacenAux, TipoAsignacion);
    if Result then CargarDataSet;
end;

function TfrmSeleccionarMovimientosTeleviaFORM.ObtenerConceptos(const Operacion: Integer; Almacen: Integer; TipoAsignacion: string): Boolean;
begin
	Result := False;
    spConceptos.Close;
    spConceptos.Parameters.ParamByName('@OperacionTag').Value := Operacion;
    spConceptos.Parameters.ParamByName('@CodigoAlmacen').Value := Almacen;
    spConceptos.Parameters.ParamByName('@TipoAsignacionTAG').Value := TipoAsignacion;

    try
        spConceptos.Open;
        //mostrar la ventana, aunque sea vac�a.
        //REV.2 Result := not spConceptos.IsEmpty;
        Result := True;
    except
        on e: Exception do begin
            MsgBoxErr('Error obteniendo los conceptos para un Telev�a', e.Message, Caption, MB_ICONERROR);
        end;
    end;
end;

end.
