unit frmCodigoConvenioFacturacion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ListBoxEx, DBListEx, StdCtrls, DMConnection, Util, PeaProcsCN, DB,
  ADODB, UtilProc, ExtCtrls;

type
    TOnClickRegSeleccionado = procedure (NumeroConvenio: String; CodigoConvenio: Integer) of object;

type
  TformCodigoConvenioFacturacion = class(TForm)
    grp_PACgrp1: TGroupBox;
    lbl_Personeria: TLabel;
    lbl1: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    lbl_Nombre: TLabel;
    lblRazonSocial: TLabel;
    txt_RUT: TEdit;
    txt_Nombre: TEdit;
    txt_Apellido: TEdit;
    txt_ApellidoMaterno: TEdit;
    txt_RazonSocial: TEdit;
    txt_Personeria: TEdit;
    lstConvenios: TDBListEx;
    BtnSalir: TButton;
    ds_ConveniosRUT: TDataSource;
    spObtenerConveniosRUT: TADOStoredProc;
    btn_Seleccionar: TButton;
    procedure BtnSalirClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lstConveniosDblClick(Sender: TObject);
    procedure btn_SeleccionarClick(Sender: TObject);
  private
    { Private declarations }
    FOnClickRegSeleccionado: TOnClickRegSeleccionado;
    procedure InicializarVariables();
    function GetOnClickRegSeleccionado: TOnClickRegSeleccionado;
    procedure SetOnClickRegSeleccionado(const Value: TOnClickRegSeleccionado);

  public
    { Public declarations }
    ConvenioFacturacion: Integer;
    function Inicializa(NumeroDocumento: string): Boolean; overload;
    property OnClickRegSeleccionado: TOnClickRegSeleccionado read GetOnClickRegSeleccionado write SetOnClickRegSeleccionado;
  end;

var
  formCodigoConvenioFacturacion: TformCodigoConvenioFacturacion;

implementation

{$R *.dfm}

procedure TformCodigoConvenioFacturacion.BtnSalirClick(Sender: TObject);
begin
    spObtenerConveniosRUT.Close;
    close;
end;

 procedure TformCodigoConvenioFacturacion.InicializarVariables();
 begin
    txt_Personeria.Text:= '';
    txt_RUT.Text:= '';
    txt_Nombre.Text:= '';
    txt_Apellido.Text:= '';
    txt_ApellidoMaterno.Text:= '';
    txt_RazonSocial.Text:= '';
    txt_RazonSocial.Visible:=False;
    lblRazonSocial.Visible:=False;
    lbl_Nombre.Caption:='Nombre:'
 end;

procedure TformCodigoConvenioFacturacion.lstConveniosDblClick(Sender: TObject);
begin
    btn_SeleccionarClick(Sender);
end;

procedure TformCodigoConvenioFacturacion.btn_SeleccionarClick(Sender: TObject);
begin
    if not spObtenerConveniosRUT.Active then Exit;

    if Assigned(Self.OnClickRegSeleccionado) then begin
        self.OnClickRegSeleccionado(spObtenerConveniosRUT.FieldByName('NumeroConvenio').AsString,spObtenerConveniosRUT.FieldByName('CodigoConvenio').AsInteger);
       spObtenerConveniosRUT.Close;
       Close;
    end;
end;

procedure TformCodigoConvenioFacturacion.FormShow(Sender: TObject);
begin
  if txt_RazonSocial.Text <>'' then
    lbl_Nombre.Caption:='Contacto:'
end;

function TformCodigoConvenioFacturacion.Inicializa(NumeroDocumento: string): Boolean;
var
    MSG_RESULT_SEARCH: string;
    MSG_ATENTION : string;
begin
    MSG_RESULT_SEARCH := 'No se ha encontrado registros para la b�squeda realizada';
    MSG_ATENTION      := 'Atenci�n';
    Result := True;
    try
        InicializarVariables();

      //Cargar los convenios por RUT
       spObtenerConveniosRUT.Close;
       spObtenerConveniosRUT.Parameters.ParamByName('@NumeroDocumento').Value := NumeroDocumento;
       spObtenerConveniosRUT.Open;
       if spObtenerConveniosRUT.IsEmpty then begin
            MsgBox(MSG_RESULT_SEARCH, MSG_ATENTION, MB_ICONINFORMATION);
            Result := False;
       end;
       //spObtenerConveniosRUT.First;
       //spObtenerConveniosRUT.Close;
       //Notebook.PageIndex:=0;
    except
        on e: Exception do begin
            Result := False;
            ShowMsgBoxCN(e, Self);
        end;
    end;
end;

function TformCodigoConvenioFacturacion.GetOnClickRegSeleccionado: TOnClickRegSeleccionado;
begin
    result := FOnClickRegSeleccionado;
end;

procedure TformCodigoConvenioFacturacion.SetOnClickRegSeleccionado(const Value: TOnClickRegSeleccionado);
begin
    FOnClickRegSeleccionado := Value;
end;

end.
