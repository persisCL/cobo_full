{********************************** Unit Header ********************************
File Name : frmCambiarEstadoTag.pas
Author : llazarte
Date Created: 30/03/2006
Language : ES-AR
Description : Permite guardar los bits del actionlist

Revision :1
Author : jconcheyro
Date Created: 18/04/2007
Language : ES-AR
Description : Cambio el campo ContractSerialNumber a Float porque volaba con
un caso de VS, con contractSerialNumber = 2235582908

Revision: 2
Author: nefernandez
Date: 25/04/2007
Language : ES-AR
Description : Continuando con el cambio del campo ContractSerialNumber a Float (revision 1)
en el SP spObtenerDatosActionTAG

Firma       : SS-1006-NDR-20120715
Description : Enviar el @Usuario a CambiarEstadoTAG, para el caso de activar/desactivar la Lista Negra, si la cuenta esta Adherida a PA
              enviar la alta o la baja segun corresponda y poder dejar registro de auditoria con el usuario quee realizo el cambio


Firma       : SS_660_MCA_20130513
Descripcion : Se valida el Set y Clear del HistoricoPedidosEnviosActionList, para el caso de que se cambie una lista
              aplique las bajas o altas seg�n corresponda (formulario de Historico de Envios)

Firma       : SS_660_MCA_20130530
Descripcion : Se corrige error al intentar cambiar estado de un tag cuando el convenio est� de baja.

Autor       :   CQuezadaI
Fecha       :   23-07-2013
Firma       :   SS_660_CQU_20130711
Desripcion  :   Se agrega el par�metro CodigoConcesionaria en la funci�n EstaConvenioEnListaAmarilla.

Author      : Mcabello
Fecha       : 12-09-2013
Firma       : SS_660_MCA_20130912
Descripcion : Se inhabilita el GreenList debido a que ser� desactivado desde el TSMC y s�lo es usado para TAG nuevos que vienen de f�brica,
              adem�s se agrega el Bit MMINotOk

*******************************************************************************}

unit frmCambiarEstadoTag;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DB, DBClient, ADODB, Grids, DBGrids, DPSControls, Util,
  ExtCtrls, DMConnection, PeaProcs, UtilProc, StrUtils, VariantComboBox, Buttons,
  UtilDB, PeaTypes, DmiCtrls;

resourcestring
    MSG_ERROR_SELECTION = 'Activar la Lista Verde o activar el ExceptionHandling producen el mismo efecto';
    TXT_ATENCION        = 'Atenci�n';
    TXT_ERROR           = 'Error';

type
  TFormCambiarEstadoTag = class(TForm)
    BtnAceptar: TButton;
    BtnCancelar: TButton;
    gbDetallleActionList: TGroupBox;
    lblDescriFuncionalidad: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    lblPatente: TLabel;
    lblTelevia: TLabel;
    spObtenerDatosActionTAG: TADOStoredProc;
    spCambiarEstadoTag: TADOStoredProc;
    gbListaVerde: TGroupBox;
    chkSetGreenList: TCheckBox;
    chkClearGreenList: TCheckBox;
    gbListaAmarilla: TGroupBox;
    chkSetYellowList: TCheckBox;
    chkClearYellowList: TCheckBox;
    gbListaGris: TGroupBox;
    chkSetGrayList: TCheckBox;
    chkClearGrayList: TCheckBox;
    gbListaNegra: TGroupBox;
    chkSetBlackList: TCheckBox;
    chkClearBlackList: TCheckBox;
    gbExceptionHandling: TGroupBox;
    chkSetExceptionHandling: TCheckBox;
    gbContactOperator: TGroupBox;
    chkSetMMIContactOperator: TCheckBox;
    Label2: TLabel;
    lblFechaUltimoEnvio: TLabel;
    spCrearHistoricoPedidoEnvioActionList: TADOStoredProc;
    chkClearExceptionHandling: TCheckBox;
    chkClearMMIContactOperator: TCheckBox;
    lblDescrFuncionalidad2: TLabel;
    ImgAyuda: TImage;
    lblInfoEstado: TLabel;                                                      //SS_660_MCA_20130129
    ldEstado: TLed;                                                             //SS_660_MCA_20130129
    lblEstado: TLabel;                                                          //SS_660_MCA_20130129
    lblFechaSolicitud: TLabel;                                                  //SS_660_MCA_20130129
    lblFechaUltimaSol: TLabel;                                                  // SS_660_MCA_20130129
    gbMMINotOk: TGroupBox;                                                      //SS_660_MCA_20130912
    chkSetMMINotOk: TCheckBox;                                                  //SS_660_MCA_20130912
    chkClearMMINotOk: TCheckBox;                                                // SS_660_MCA_20130912
    procedure ImgAyudaClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure chkSetMMIContactOperatorClick(Sender: TObject);
    procedure chkClearMMIContactOperatorClick(Sender: TObject);
    procedure chkSetExceptionHandlingClick(Sender: TObject);
    procedure chkClearExceptionHandlingClick(Sender: TObject);
    procedure chkSetGrayListClick(Sender: TObject);
    procedure chkClearGrayListClick(Sender: TObject);
    procedure chkSetBlackListClick(Sender: TObject);
    procedure chkClearBlackListClick(Sender: TObject);
    procedure chkSetYellowListClick(Sender: TObject);
    procedure chkClearYellowListClick(Sender: TObject);
    procedure chkSetGreenListClick(Sender: TObject);
    procedure chkClearGreenListClick(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
    procedure BtnAceptarClick(Sender: TObject);
    function ValidacionesBasicas: Boolean;
    Procedure GuardarRegistroHistorico(Enviar: Boolean);
    procedure chkSetMMINotOkClick(Sender: TObject);                             // SS_660_MCA_20130912
    procedure chkClearMMINotOkClick(Sender: TObject);                           // SS_660_MCA_20130912
  private                                                                       
    { Private declarations }
    function HayCambios: Boolean;
  public
    { Public declarations }
    function Inicializar(CodigoConvenio, IndiceVehiculo: Integer): Boolean;
  end;

var
  FormCambiarEstadoTag: TFormCambiarEstadoTag;

implementation

{$R *.dfm}


{******************************** Function Header ******************************
Function Name: Inicializar
Author : llazarte
Date Created : 30/03/2006
Description : Inicializa el form con los datos de los pedidos de action list
    del Telev�a asociado a la cuenta que se pasa como par�metro.
Parameters : Patente, Televia: String
Return Value : boolean

Revision 1:
    Author : ggomez
    Date : 31/05/2006
    Description :
        - Quit� el c�digo que le rellenaba con 0 (ceros) a la izquierda
        la Etiqueta del Telev�a.
        - Cambi� la asignaci�n Result := True, al final para indicar realmente
        si el inicializar ejecut� con exito.
        - Si el Tag NO es de Terceros, preguntar por el permiso de Modificar
        ActionList de Tags Propios, sino pregutar por el permiso de Modificar
        ActionList de Tags de Terceros.

Revision 2:
    Author : ggomez
    Date : 30/06/2006
    Description :
        - Setear los check de Clear de ExceptionHandling y MMIContactOperator.

Revision 3:
    Author : ggomez
    Date : 19/07/2006
    Description : En el caos de Telev�a de Terceros se deshabilitan los controles
        de las listas colores.

*******************************************************************************}
function TFormCambiarEstadoTag.Inicializar(CodigoConvenio, IndiceVehiculo: integer): boolean;
ResourceString
    MSG_ERROR_TAG   = 'Error buscando los datos del Tag';
    MSG_EMPTY_DATA  = 'No se encontraron datos para este telev�a';
    MSG_EMPTY_DATE  = '< Sin Datos >';
    //SQL_ESTACONVENIOENLISTAAMARILLA = 'SELECT dbo.EstaConvenioEnListaAmarilla(%d,''%s'')';        // SS_660_CQU_20130711 // SS_660_MCA_20130221
    SQL_ESTACONVENIOENLISTAAMARILLA = 'SELECT dbo.EstaConvenioEnListaAmarilla(%d,''%s'', NULL)';    // SS_660_CQU_20130711
Var
    EsListaAmarilla: Boolean;  // SS_660_MCA_20130221
begin
    Result := False;
    lblPatente.Caption  := EmptyStr;
    lblTelevia.Caption  := EmptyStr;
    try
      	spObtenerDatosActionTAG.Close;
        spObtenerDatosActionTAG.Parameters.Refresh;
        spObtenerDatosActionTAG.Parameters.ParamByName('@CodigoConvenio').Value := CodigoConvenio;
        spObtenerDatosActionTAG.Parameters.ParamByName('@IndiceVehiculo').Value := IndiceVehiculo;
        // Incializar los valores de los par�metros de salida con null.
        spObtenerDatosActionTAG.Parameters.ParamByName('@Patente').Value := Null;
        spObtenerDatosActionTAG.Parameters.ParamByName('@Televia').Value := Null;
        spObtenerDatosActionTAG.Parameters.ParamByName('@EsTagDeOtraConcesionaria').Value := Null;
        spObtenerDatosActionTAG.Parameters.ParamByName('@Csn').Value := Null;                       //SS_660_MCA_20130129
        spObtenerDatosActionTAG.Parameters.ParamByName('@Cmk').Value := Null;                       //SS_660_MCA_20130129
        spObtenerDatosActionTAG.Parameters.ParamByName('@Estado').Value := Null;                    //SS_660_MCA_20130129
        spObtenerDatosActionTAG.Open;
    except
        on e: Exception do begin
            MsgBoxErr(MSG_ERROR_TAG, e.Message, TXT_ERROR, MB_ICONWARNING);
            Exit;
        end;
    end;

    // Setear los check seg�n los datos obtenidos.
    if not spObtenerDatosActionTAG.IsEmpty then begin
        {chkSetGreenList.Checked       := spObtenerDatosActionTAG.FieldByName('SetGreenList').AsBoolean;    //SS_660_MCA_20130129
        chkClearGreenList.Checked     := spObtenerDatosActionTAG.FieldByName('ClearGreenList').AsBoolean;   //SS_660_MCA_20130129

        chkSetYellowList.Checked      := spObtenerDatosActionTAG.FieldByName('SetYellowList').AsBoolean;    //SS_660_MCA_20130129
        chkClearYellowList.Checked    := spObtenerDatosActionTAG.FieldByName('ClearYellowList').AsBoolean;  //SS_660_MCA_20130129

        chkSetGrayList.Checked        := spObtenerDatosActionTAG.FieldByName('SetGrayList').AsBoolean;      //SS_660_MCA_20130129
        chkClearGrayList.Checked      := spObtenerDatosActionTAG.FieldByName('ClearGrayList').AsBoolean;    //SS_660_MCA_20130129

        chkSetBlackList.Checked       := spObtenerDatosActionTAG.FieldByName('SetBlackList').AsBoolean;     //SS_660_MCA_20130129
        chkClearBlackList.Checked     := spObtenerDatosActionTAG.FieldByName('ClearBlackList').AsBoolean;}  //SS_660_MCA_20130129
          // siempre se mostrar� apagada la lista verde
          // SS_660_MCA_20130912
{        if spObtenerDatosActionTAG.FieldByName('GreenListDeseado').AsBoolean then       //SS_660_MCA_20130129
            chkSetGreenList.Checked := True                                             //SS_660_MCA_20130129
        else chkClearGreenList.Checked := True;                                         //SS_660_MCA_20130129}
        chkClearGreenList.Checked := True;                                              //SS_660_MCA_20130912
        if spObtenerDatosActionTAG.FieldByName('YellowListDeseado').AsBoolean then      //SS_660_MCA_20130129
            chkSetYellowList.Checked := True                                            //SS_660_MCA_20130129
        else chkClearYellowList.Checked := True;                                        //SS_660_MCA_20130129
        if spObtenerDatosActionTAG.FieldByName('GrayListDeseado').AsBoolean then        //SS_660_MCA_20130129
            chkSetGrayList.Checked := True                                              //SS_660_MCA_20130129
        else chkClearGrayList.Checked := True;                                          //SS_660_MCA_20130129
        if spObtenerDatosActionTAG.FieldByName('BlackListDeseado').AsBoolean then       //SS_660_MCA_20130129
            chkSetBlackList.Checked := True                                             //SS_660_MCA_20130129
        else chkClearBlackList.Checked := True;                                         //SS_660_MCA_20130129


        if spObtenerDatosActionTAG.Parameters.ParamByName('@Estado').Value = True then begin              //SS_660_MCA_20130129
            ldEstado.ColorOn := clGreen;                                                                  //SS_660_MCA_20130129
            lblInfoEstado.Caption := 'CONFIRMADO';                                                        //SS_660_MCA_20130129
            ldEstado.Enabled := True;                                                                     //SS_660_MCA_20130129
        end                                                                                               //SS_660_MCA_20130129
        else begin                                                                                        //SS_660_MCA_20130129
            ldEstado.ColorOn := clRed;                                                                    //SS_660_MCA_20130129
            lblInfoEstado.Caption := 'PENDIENTE';                                                         //SS_660_MCA_20130129
            ldEstado.Enabled := True;                                                                     //SS_660_MCA_20130129
        end;                                                                                              //SS_660_MCA_20130129


        // Si est� levantado el bit de ExceptionHandling, chequear el Set sino
        // chequear el Clear. Este se hace as� ya que para el ExceptionHandling
        // s�lo se tiene un bit que indica Levantado o No Levantado.
        chkSetExceptionHandling.Checked     := spObtenerDatosActionTAG.FieldByName('ExceptionHandling').AsBoolean;
        chkClearExceptionHandling.Checked   := not chkSetExceptionHandling.Checked;

        // Si est� levantado el bit de MMIContactOperator, chequear el Set sino
        // chequear el Clear. Este se hace as� ya que para el ExceptionHandling
        // s�lo se tiene un bit que indica Levantado o No Levantado.
        chkSetMMIContactOperator.Checked    := spObtenerDatosActionTAG.FieldByName('MMIContactOperator').AsBoolean;
        chkClearMMIContactOperator.Checked  := not chkSetMMIContactOperator.Checked;

        chkSetMMINotOk.Checked    := spObtenerDatosActionTAG.FieldByName('MMINotOk').AsBoolean;     //SS_660_MCA_20130912
        chkClearMMINotOk.Checked  := not chkSetMMINotOk.Checked;                                    //SS_660_MCA_20130912

        //datos que se muestran
        lblPatente.Caption          := spObtenerDatosActionTAG.Parameters.ParamByName('@Patente').Value;
        lblTelevia.Caption          := spObtenerDatosActionTAG.Parameters.ParamByName('@Televia').value;
        lblFechaUltimoEnvio.Caption := iif(spObtenerDatosActionTAG.FieldByName('FechaUltimoEnvio').IsNull,
            MSG_EMPTY_DATE,
            FormatDateTime('dd/mm/yyyy hh:mm', spObtenerDatosActionTAG.FieldByName('FechaUltimoEnvio').AsDateTime));

        lblFechaUltimaSol.Caption :=  iif(spObtenerDatosActionTAG.FieldByName('FechaUltimaSol').IsNull,               //SS_660_MCA_20130129
           MSG_EMPTY_DATE,                                                                                            //SS_660_MCA_20130129
            FormatDateTime('dd/mm/yyyy hh:mm', spObtenerDatosActionTAG.FieldByName('FechaUltimaSol').AsDateTime));    //SS_660_MCA_20130129
        // Permiso para modificar
        // Si el Tag NO es de Terceros, preguntar por el permiso de Modificar ActionList de Tags Propios,
        // sino pregutar por el permiso de Modificar ActionList de Tags de Terceros.
        if spObtenerDatosActionTAG.Parameters.ParamByName('@EsTagDeOtraConcesionaria').Value then begin
            // Deshabilitar los controles de las listas de colores.
            EnableControlsInContainer(gbListaVerde, False);
            EnableControlsInContainer(gbListaAmarilla, False);
            EnableControlsInContainer(gbListaGris, False);
            EnableControlsInContainer(gbListaNegra, False);

            gbDetallleActionList.Enabled  := ExisteAcceso('EDICION_ACTIONLIST_TAG_TERCEROS');
        end else begin
            gbDetallleActionList.Enabled  := ExisteAcceso('EDICION_ACTIONLIST_TAG_PROPIOS');
            EnableControlsInContainer(gbListaAmarilla, False);
            EnableControlsInContainer(gbListaVerde, False);                                                               //SS_660_MCA_20130912
        end;
        EsListaAmarilla := StrToBool(QueryGetValue(   DMConnections.BaseCAC,                                                               //SS_660_MCA_20130221
                                                        Format(SQL_ESTACONVENIOENLISTAAMARILLA,                                              //SS_660_MCA_20130221
                                                                [   CodigoConvenio,                                                          //SS_660_MCA_20130221
                                                                    FormatDateTime('YYYYMMDD HH:NN:SS',NowBase(DMConnections.BaseCAC))     //SS_660_MCA_20130221
                                                                ]                                                                            //SS_660_MCA_20130221
                                                              )                                                                              //SS_660_MCA_20130221
                                                     ));
            if EsListaAmarilla then begin                                       //SS_660_MCA_20130221
                EnableControlsInContainer(gbExceptionHandling, False);          //SS_660_MCA_20130221
            end                                                                 //SS_660_MCA_20130221
            else begin                                                          //SS_660_MCA_20130221
                EnableControlsInContainer(gbExceptionHandling, True);           //SS_660_MCA_20130221

            end;                                                                //SS_660_MCA_20130221
            // solo sera visual, no se permite modificar.                       //SS_660_MCA_20130912
            EnableControlsInContainer(gbMMINotOk, False);                       //SS_660_MCA_20130912
            

        BtnAceptar.Enabled := gbDetallleActionList.Enabled;
        Result := True;
    end else begin
       { MsgBox(MSG_EMPTY_DATA, TXT_ATENCION, MB_ICONINFORMATION);
        Result := False;}

        // Si el tag no existe en MaestroActionList no se marcan los bit
        // begin SS_660_MCA_20121020
        chkSetGreenList.Checked       := False;                                 //SS_660_MCA_20130129
        chkClearGreenList.Checked     := False;                                 //SS_660_MCA_20130129

        chkSetYellowList.Checked      := False;                                 //SS_660_MCA_20130129
        chkClearYellowList.Checked    := False;                                 //SS_660_MCA_20130129

        chkSetGrayList.Checked        := False;                                 //SS_660_MCA_20130129
        chkClearGrayList.Checked      := False;                                 //SS_660_MCA_20130129

        chkSetBlackList.Checked       := False;                                 //SS_660_MCA_20130129
        chkClearBlackList.Checked     := False;                                 //SS_660_MCA_20130129

        chkSetExceptionHandling.Checked     := False;                           //SS_660_MCA_20130129
        chkClearExceptionHandling.Checked   := False;                           //SS_660_MCA_20130129

        chkSetMMIContactOperator.Checked    := False;                           //SS_660_MCA_20130129
        chkClearMMIContactOperator.Checked  := False;                           //SS_660_MCA_20130129

        chkSetMMINotOk.Checked    := False;                                     //SS_660_MCA_20130912
        chkClearMMINotOk.Checked  := False;                                     //SS_660_MCA_20130912

        lblPatente.Caption          := spObtenerDatosActionTAG.Parameters.ParamByName('@Patente').Value;        //SS_660_MCA_20130129
        lblTelevia.Caption          := spObtenerDatosActionTAG.Parameters.ParamByName('@Televia').value;        //SS_660_MCA_20130129
        lblFechaUltimoEnvio.Caption := iif(spObtenerDatosActionTAG.FieldByName('FechaUltimoEnvio').IsNull,      //SS_660_MCA_20130129
           MSG_EMPTY_DATE,
         FormatDateTime('dd/mm/yyyy hh:mm', spObtenerDatosActionTAG.FieldByName('FechaUltimoEnvio').AsDateTime));   //SS_660_MCA_20130129

        lblFechaUltimaSol.Caption :=  iif(spObtenerDatosActionTAG.FieldByName('FechaUltimaSol').IsNull,             //SS_660_MCA_20130129
           MSG_EMPTY_DATE,                                                                                          //SS_660_MCA_20130129
         FormatDateTime('dd/mm/yyyy hh:mm', spObtenerDatosActionTAG.FieldByName('FechaUltimaSol').AsDateTime));     //SS_660_MCA_20130129

        if spObtenerDatosActionTAG.Parameters.ParamByName('@EsTagDeOtraConcesionaria').Value then begin             //SS_660_MCA_20130129
            // Deshabilitar los controles de las listas de colores.
            EnableControlsInContainer(gbListaVerde, False);                                                         //SS_660_MCA_20130129
            EnableControlsInContainer(gbListaAmarilla, False);                                                      //SS_660_MCA_20130129
            EnableControlsInContainer(gbListaGris, False);                                                          //SS_660_MCA_20130129
            EnableControlsInContainer(gbListaNegra, False);                                                         //SS_660_MCA_20130129

            gbDetallleActionList.Enabled  := ExisteAcceso('EDICION_ACTIONLIST_TAG_TERCEROS');                       //SS_660_MCA_20130129
        end else begin                                                                                              //SS_660_MCA_20130129
            gbDetallleActionList.Enabled  := ExisteAcceso('EDICION_ACTIONLIST_TAG_PROPIOS');                        //SS_660_MCA_20130129
        end;

        EnableControlsInContainer(gbListaAmarilla, False);                                                          //SS_660_MCA_20121012
        EnableControlsInContainer(gbListaVerde, False);                                                             //SS_660_MCA_20130912
        EnableControlsInContainer(gbMMINotOk, False);                                                               //SS_660_MCA_20130912
        BtnAceptar.Enabled := gbDetallleActionList.Enabled;                                                         //SS_660_MCA_20130129

        Result := True;                                                                                             //SS_660_MCA_20130129
        // end //SS_660_MCA_20121020
    end;
end;

{******************************** Function Header ******************************
Function Name: HayCambios
Author : aganc
Date Created : 16/05/2006
Description : Testea si hubo cambios entre los datos leidos
    del tag y los q estan en los check , si no hay cambios retorna false
Parameters : none
Return Value : boolean
*******************************************************************************}
function TFormCambiarEstadoTag.HayCambios: Boolean;
begin
    result := ( (chkSetGreenList.Checked <> spObtenerDatosActionTAG.FieldByName('GreenListDeseado').AsBoolean)
    //      (chkSetGreenList.Checked <> spObtenerDatosActionTAG.FieldByName('SetGreenList').AsBoolean)
    //  or (chkClearGreenList.Checked     <> spObtenerDatosActionTAG.FieldByName('ClearGreenList').AsBoolean)

        or (chkSetYellowList.Checked      <> spObtenerDatosActionTAG.FieldByName('YellowListDeseado').AsBoolean)
    //        or (chkSetYellowList.Checked      <> spObtenerDatosActionTAG.FieldByName('SetYellowList').AsBoolean)
    //        or (chkClearYellowList.Checked    <> spObtenerDatosActionTAG.FieldByName('ClearYellowList').AsBoolean)

        or (chkSetGrayList.Checked        <> spObtenerDatosActionTAG.FieldByName('GrayListDeseado').AsBoolean)
    //      or (chkSetGrayList.Checked        <> spObtenerDatosActionTAG.FieldByName('SetGrayList').AsBoolean)
    //      or (chkClearGrayList.Checked      <> spObtenerDatosActionTAG.FieldByName('ClearGrayList').AsBoolean)

        or (chkSetBlackList.Checked       <> spObtenerDatosActionTAG.FieldByName('BlackListDeseado').AsBoolean)
    //    or (chkSetBlackList.Checked       <> spObtenerDatosActionTAG.FieldByName('SetBlackList').AsBoolean)
    //    or (chkClearBlackList.Checked     <> spObtenerDatosActionTAG.FieldByName('ClearBlackList').AsBoolean)

        or (chkSetExceptionHandling.Checked  <> spObtenerDatosActionTAG.FieldByName('ExceptionHandling').AsBoolean)
        or (chkSetMMIContactOperator.Checked <> spObtenerDatosActionTAG.FieldByName('MMIContactOperator').AsBoolean)
        or (chkSetMMINotOk.Checked <> spObtenerDatosActionTAG.FieldByName('MMINotOk').AsBoolean) ); // SS_660_MCA_20130912
end;

{******************************** Function Header ******************************
Function Name: BtnAceptarClick
Author : llazarte
Date Created : 02/05/2006
Description : Guarda los cambios en la BD
Parameters : Sender: TObject
Return Value : None
revision 1:
autor : aganc
descripcion:
se reescribio codigo y elimino una variable inecesaria
 se comento 2 mensajes uno donde se pregunta si se quiere guardar los cambios y otro,
 informando la actualizacion
 se elimino el check enviar ,ahora el usuario no pued emodificar este campo
 desde este form
 y se setea en true por defecto.
*******************************************************************************}

procedure TFormCambiarEstadoTag.BtnAceptarClick(Sender: TObject);
resourcestring
    MSG_ERROR_CAMBIOS_ACTIONLIST = 'Error guardando datos';
var					// SS_660_MCA_20130912
  Enviar: Boolean;	// SS_660_MCA_20130912
begin
    ModalResult := mrCancel;

    if not HayCambios then begin
        ModalResult := mrCancel;
//        Close;
    end else begin
        // Controlo el ingreso por las dudas
        if not ValidacionesBasicas then Exit;

        // Guardo los cambios
        Cursor := crHourGlass;
        DMConnections.BaseCAC.BeginTrans;
        try
            try
                // graba los datos
                spCambiarEstadoTag.Close;
                spCambiarEstadoTag.Parameters.Refresh;                                                                //SS-1006-NDR-20120715
                spCambiarEstadoTag.Parameters.ParamByName('@Usuario').Value := UsuarioSistema;                        //SS-1006-NDR-20120715

                // Lista Verde. Si el dato no cambi� informo Null. Si cambi�
                // informo el nuevo dato.
                //if (spObtenerDatosActionTAG.FieldByName('SetGreenList').AsBoolean = chkSetGreenList.Checked) then begin     //SS_660_MCA_20130912
               // if (spObtenerDatosActionTAG.FieldByName('GreenListDeseado').AsBoolean = chkSetGreenList.Checked) then begin //SS_660_MCA_20130912
               //No se debe activar el greenflag ya que s�lo viene activado de f�brica para televias nuevos                       //SS_660_MCA_20130912
               // se enviar� en 0 para desactivar del maestroactionlist aquellos televias que tengan activado el GreenList
                    spCambiarEstadoTag.Parameters.ParamByName('@SetGreenList').Value := 0;                                    //SS_660_MCA_20130912
                    Enviar := False;                                                                                          //SS_660_MCA_20130912
               // end else begin                                                                                              //SS_660_MCA_20130912
              //      spCambiarEstadoTag.Parameters.ParamByName('@SetGreenList').Value := chkSetGreenList.Checked;            //SS_660_MCA_20130912
             //   end;                                                                                                        //SS_660_MCA_20130912

                // Lista Amarilla. Si el dato no cambi� informo Null. Si cambi�
                // informo el nuevo dato.
                //if (spObtenerDatosActionTAG.FieldByName('SetYellowList').AsBoolean = chkSetYellowList.Checked) then begin
                if (spObtenerDatosActionTAG.FieldByName('YellowListDeseado').AsBoolean = chkSetYellowList.Checked) then begin
                    spCambiarEstadoTag.Parameters.ParamByName('@SetYellowList').Value := Null;
                end else begin
                    spCambiarEstadoTag.Parameters.ParamByName('@SetYellowList').Value := chkSetYellowList.Checked;
                    Enviar := True;                                                                                             //SS_660_MCA_20130912
                end;

                // Lista Gris. Si el dato no cambi� informo Null. Si cambi�
                // informo el nuevo dato.
                //if (spObtenerDatosActionTAG.FieldByName('SetGrayList').AsBoolean = chkSetGrayList.Checked) then begin
                if (spObtenerDatosActionTAG.FieldByName('GrayListDeseado').AsBoolean = chkSetGrayList.Checked) then begin
                    spCambiarEstadoTag.Parameters.ParamByName('@SetGrayList').Value := Null;
                end else begin
                    spCambiarEstadoTag.Parameters.ParamByName('@SetGrayList').Value := chkSetGrayList.Checked;
                    Enviar := True;                                                                                              //SS_660_MCA_20130912
                end;

                // Lista Black. Si el dato no cambi� informo Null. Si cambi�
                // informo el nuevo dato.
                //if (spObtenerDatosActionTAG.FieldByName('SetBlackList').AsBoolean = chkSetBlackList.Checked) then begin
                if (spObtenerDatosActionTAG.FieldByName('BlackListDeseado').AsBoolean = chkSetBlackList.Checked) then begin
                    spCambiarEstadoTag.Parameters.ParamByName('@SetBlackList').Value := Null
                end else begin
                    spCambiarEstadoTag.Parameters.ParamByName('@SetBlackList').Value := chkSetBlackList.Checked;
                    Enviar := True;                                                                                              //SS_660_MCA_20130912
                end;


                // Clear Lista Verde. Si el dato no cambi� informo Null. Si cambi�
                // informo el nuevo dato.
            {    if (spObtenerDatosActionTAG.FieldByName('ClearGreenList').AsBoolean = chkClearGreenList.Checked) then begin
                    spCambiarEstadoTag.Parameters.ParamByName('@ClearGreenList').Value := Null;
                end else begin
                    spCambiarEstadoTag.Parameters.ParamByName('@ClearGreenList').Value := chkClearGreenList.Checked;
                end;

                // Clear Lista Amarilla. Si el dato no cambi� informo Null. Si cambi�
                // informo el nuevo dato.
                if (spObtenerDatosActionTAG.FieldByName('ClearYellowList').AsBoolean = chkClearYellowList.Checked) then begin
                    spCambiarEstadoTag.Parameters.ParamByName('@ClearYellowList').Value := Null;
                end else begin
                    spCambiarEstadoTag.Parameters.ParamByName('@ClearYellowList').Value := chkClearYellowList.Checked;
                end;

                // Clear Lista Gris. Si el dato no cambi� informo Null. Si cambi�
                // informo el nuevo dato.
                if (spObtenerDatosActionTAG.FieldByName('ClearGrayList').AsBoolean = chkClearGrayList.Checked) then begin
                    spCambiarEstadoTag.Parameters.ParamByName('@ClearGrayList').Value := Null;
                end else begin
                    spCambiarEstadoTag.Parameters.ParamByName('@ClearGrayList').Value := chkClearGrayList.Checked;
                end;

                // Clear Lista Black. Si el dato no cambi� informo Null. Si cambi�
                // informo el nuevo dato.
                if (spObtenerDatosActionTAG.FieldByName('ClearBlackList').AsBoolean = chkClearBlackList.Checked) then begin
                    spCambiarEstadoTag.Parameters.ParamByName('@ClearBlackList').Value := Null;
                end else begin
                    spCambiarEstadoTag.Parameters.ParamByName('@ClearBlackList').Value := chkClearBlackList.Checked;
                end;
              }
                // Exception Handling. Si el dato no cambi� informo Null. Si cambi�
                // informo el nuevo dato.
                if spObtenerDatosActionTAG.FieldByName('ExceptionHandling').AsBoolean = chkSetExceptionHandling.Checked then begin
                    spCambiarEstadoTag.Parameters.ParamByName('@ExceptionHandling').Value := Null;
                end else begin
                    spCambiarEstadoTag.Parameters.ParamByName('@ExceptionHandling').Value := chkSetExceptionHandling.Checked;
                    Enviar := True;                                                                                                     //SS_660_MCA_20130912
                end;

                // Contact Operator. Si el dato no cambi� informo Null. Si cambi�
                // informo el nuevo dato.
                if (spObtenerDatosActionTAG.FieldByName('MMIContactOperator').AsBoolean = chkSetMMIContactOperator.Checked) then begin
                    spCambiarEstadoTag.Parameters.ParamByName('@MMIContactOperator').Value := Null
                end else begin
                    spCambiarEstadoTag.Parameters.ParamByName('@MMIContactOperator').Value := chkSetMMIContactOperator.Checked;
                    Enviar := True;                                                                                                    //SS_660_MCA_20130912
                end;

                // Contact Operator. Si el dato no cambi� informo Null. Si cambi�									// SS_660_MCA_20130912
                // informo el nuevo dato.																			// SS_660_MCA_20130912
                if (spObtenerDatosActionTAG.FieldByName('MMINotOk').AsBoolean = chkSetMMINotOk.Checked) then begin	// SS_660_MCA_20130912
                    spCambiarEstadoTag.Parameters.ParamByName('@MMINotOk').Value := Null							// SS_660_MCA_20130912
                end else begin																						// SS_660_MCA_20130912
                    spCambiarEstadoTag.Parameters.ParamByName('@MMINotOk').Value := chkSetMMINotOk.Checked;			// SS_660_MCA_20130912
                    Enviar := True;																					// SS_660_MCA_20130912
                end;																								// SS_660_MCA_20130912

                // Par�metros comunes
                spCambiarEstadoTag.Parameters.ParamByName('@ContextMark').Value             := IIf(spObtenerDatosActionTAG.FieldByName('ContextMark').AsInteger = 0, spObtenerDatosActionTAG.Parameters.ParamByName('@Cmk').Value, spObtenerDatosActionTAG.FieldByName ('ContextMark').AsInteger); //SS_660_MCA_20130530
                //rev 1 lo paso a Float
                spCambiarEstadoTag.Parameters.ParamByName('@ContractSerialNumber').Value    := IIf(spObtenerDatosActionTAG.FieldByName('ContractSerialNumber').AsFloat = 0, spObtenerDatosActionTAG.Parameters.ParamByName('@Csn').Value, spObtenerDatosActionTAG.FieldByName ('ContractSerialNumber').AsFloat); //SS_660_MCA_20130530
                spCambiarEstadoTag.Parameters.ParamByName('@FechaActivacion').Value         := NowBase(DMConnections.BaseCAC);

                // Tengo que enviar, porque se modific� alguno de los checks.
                spCambiarEstadoTag.Parameters.ParamByName('@Enviar').Value := Enviar;       //SS_660_MCA_20130912

                spCambiarEstadoTag.ExecProc;
                // Datos para el hist�rico   - agregar por separado
                GuardarRegistroHistorico(True);
                DMConnections.BaseCAC.CommitTrans;
                ModalResult := mrOk;
            except
                on E: Exception do begin
                    DMConnections.BaseCAC.RollbackTrans;
                    MsgBoxErr(MSG_ERROR_CAMBIOS_ACTIONLIST, E.Message, Self.Caption, MB_ICONSTOP);
                    Exit;
                end;
            end;
        finally
            Cursor := crDefault;
            ModalResult := mrOk;
//            Close;
        end;
    end; // else if not HayCambios
end;

procedure TFormCambiarEstadoTag.chkSetExceptionHandlingClick(Sender: TObject);
begin
    chkClearExceptionHandling.Checked := not chkSetExceptionHandling.Checked;

    if (chkSetGreenList.Checked and chkSetExceptionHandling.Checked) then begin
        MsgBox(MSG_ERROR_SELECTION, TXT_ATENCION, MB_ICONINFORMATION);
        chkSetExceptionHandling.Checked := False;
        chkClearExceptionHandling.Checked := True;
    end;
end;

procedure TFormCambiarEstadoTag.chkSetGreenListClick(Sender: TObject);
begin
    // Si est� chequeado el Set, entonces deschequear el Clear
    if chkSetGreenList.Checked then begin
        chkClearGreenList.Checked := False;
    end
    else begin                                                                  //SS_660_MCA_20130129
        chkClearGreenList.Checked := True;                                      //SS_660_MCA_20130129
    end;

//    chkClearGreenList.Checked := not chkSetGreenList.Checked;
    if (chkSetGreenList.Checked and chkSetExceptionHandling.Checked) then begin
        MsgBox(MSG_ERROR_SELECTION, TXT_ATENCION, MB_ICONINFORMATION);
        chkSetGreenList.Checked := False;
    end;
end;

{******************************** Function Header ******************************
Function Name: ValidacionesBasicas
Author : llazarte
Date Created : 24/04/2006
Description :
Parameters : None
Return Value : boolean
*******************************************************************************}

function TFormCambiarEstadoTag.ValidacionesBasicas: boolean;
resourcestring
    MSG_AMBOS_SELECCION = 'La selecci�n realizada no es v�lida';
begin
    Result := False;
    // se controla esto porque sino sale un mensaje de violacion de clave.
    if (chkSetGreenList.Checked and chkClearGreenList.Checked) then begin
        MsgBoxBalloon(MSG_AMBOS_SELECCION, TXT_ATENCION, MB_ICONINFORMATION, chkSetGreenList);
        Exit;
    end;
    // se controla esto porque sino sale un mensaje de violacion de clave.
    if (chkSetYellowList.Checked and chkClearYellowList.Checked) then begin
        MsgBoxBalloon(MSG_AMBOS_SELECCION, TXT_ATENCION, MB_ICONINFORMATION, chkSetYellowList);
        Exit;
    end;

    // se controla esto porque sino sale un mensaje de violacion de clave.
    if (chkSetGrayList.Checked and chkClearGrayList.Checked) then begin
        MsgBoxBalloon(MSG_AMBOS_SELECCION, TXT_ATENCION, MB_ICONINFORMATION, chkSetGrayList);
        Exit;
    end;

    // se controla esto porque sino sale un mensaje de violacion de clave.
    if (chkSetBlackList.Checked and chkClearBlackList.Checked) then begin
        MsgBoxBalloon(MSG_AMBOS_SELECCION, TXT_ATENCION, MB_ICONINFORMATION, chkSetBlackList);
        Exit;
    end;
    Result := True;
end;

{******************************** Function Header ******************************
Function Name: GuardarRegistroHistorico
Author : llazarte
Date Created : 10/05/2006
Description : Guarda un registro historico con los cambios realizados
Parameters : None
Return Value : None

Revision 1:
    Author : ggomez
    Date : 31/05/2006
    Description : Coment� el try except, ya que este hace que si ocurre una
        excepci�n no se propaga, haciendo que el m�todo que lo llama NO se
        entere de que ocurri� una excepci�n.

Revision 1:
    Author : ggomez
    Date : 05/07/2006
    Description : Agregu� el par�metro @TipoHistorico.

Revision 2:
    Author : ggomez
    Date : 25/07/2006
    Description : Modifiqu� el nombre de la constante que representa el Tipo de
        Hist�rico para Pedido Pendiente.

*******************************************************************************}
procedure TFormCambiarEstadoTag.GuardarRegistroHistorico(Enviar: boolean);
begin
    spCrearHistoricoPedidoEnvioActionList.Close;
    with spCrearHistoricoPedidoEnvioActionList.Parameters do begin
        Refresh;
        ParamByName('@ContextMark').Value          := spObtenerDatosActionTAG.FieldByName ('ContextMark').AsInteger;
        ParamByName('@ContractSerialNumber').Value := spObtenerDatosActionTAG.FieldByName ('ContractSerialNumber').AsFloat; // Revision 2
        ParamByName('@CodigoUsuario').Value        := UsuarioSistema;

        ParamByName('@SetGreenList').Value         := chkSetGreenList.Checked;
        ParamByName('@SetYellowList').Value        := chkSetYellowList.Checked;
        ParamByName('@SetGrayList').Value          := chkSetGrayList.Checked;
        ParamByName('@SetBlackList').Value         := chkSetBlackList.Checked;

        if ((spObtenerDatosActionTAG.FieldByName('GreenListDeseado').AsBoolean = True) and chkClearGreenList.Checked = True) then   //SS_660_MCA_20130513
            ParamByName('@ClearGreenList').Value       := True                                                                      //SS_660_MCA_20130513
        else                                                                                                                        //SS_660_MCA_20130513
            ParamByName('@ClearGreenList').Value       := False;                                                                    //SS_660_MCA_20130513

        if ((spObtenerDatosActionTAG.FieldByName('BlackListDeseado').AsBoolean = True) and chkClearBlackList.Checked = True) then   //SS_660_MCA_20130513
            ParamByName('@ClearBlackList').Value       := True                                                                      //SS_660_MCA_20130513
        else                                                                                                                        //SS_660_MCA_20130513
            ParamByName('@ClearBlackList').Value       := False;                                                                    //SS_660_MCA_20130513

        if ((spObtenerDatosActionTAG.FieldByName('YellowListDeseado').AsBoolean = True) and chkClearYellowList.Checked = True) then //SS_660_MCA_20130513
            ParamByName('@ClearYellowList').Value       := True                                                                     //SS_660_MCA_20130513
        else                                                                                                                        //SS_660_MCA_20130513
            ParamByName('@ClearYellowList').Value       := False;                                                                   //SS_660_MCA_20130513

        if ((spObtenerDatosActionTAG.FieldByName('GrayListDeseado').AsBoolean = True) and chkClearGrayList.Checked = True) then     //SS_660_MCA_20130513
            ParamByName('@ClearGrayList').Value       := True                                                                       //SS_660_MCA_20130513
        else                                                                                                                        //SS_660_MCA_20130513
            ParamByName('@ClearGrayList').Value       := False;                                                                     //SS_660_MCA_20130513

//        ParamByName('@ClearGreenList').Value       := chkClearGreenList.Checked;          //SS_660_MCA_20130513
//        ParamByName('@ClearYellowList').Value      := chkClearYellowList.Checked;         //SS_660_MCA_20130513
//        ParamByName('@ClearGrayList').Value        := chkClearGrayList.Checked;           //SS_660_MCA_20130513
//        ParamByName('@ClearBlackList').Value       := chkClearBlackList.Checked;          //SS_660_MCA_20130513

        ParamByName('@ExceptionHandling').Value    := chkSetExceptionHandling.Checked;
        ParamByName('@MMIContactOperator').Value   := chkSetMMIContactOperator.Checked;

        ParamByName('@CodigoSistema').Value        := SistemaActual;

        ParamByName('@TipoHistorico').Value        := ACTION_LIST_PEDIDO_PENDIENTE;
    end;
    spCrearHistoricoPedidoEnvioActionList.ExecProc;
end;

procedure TFormCambiarEstadoTag.BtnCancelarClick(Sender: TObject);
begin
    ModalResult := mrCancel;
//    Close;
end;

procedure TFormCambiarEstadoTag.chkClearGreenListClick(Sender: TObject);
begin
    // Si est� chequeado el Clear, entonces deschequear el Set.
    if chkClearGreenList.Checked then begin
        chkSetGreenList.Checked := False;
    end                                                                         //SS_660_MCA_20130129
    else begin                                                                  //SS_660_MCA_20130129
        chkSetGreenList.Checked := True;                                        //SS_660_MCA_20130129
    end;                                                                        //SS_660_MCA_20130129

//    chkSetGreenList.Checked := not chkClearGreenList.Checked;
end;

procedure TFormCambiarEstadoTag.chkSetYellowListClick(Sender: TObject);
begin
    // Si est� chequeado el Set, entonces deschequear el Clear
    if chkSetYellowList.Checked then begin
        chkClearYellowList.Checked := False;
    end;
//    chkClearYellowList.Checked := not chkSetYellowList.Checked;
end;

procedure TFormCambiarEstadoTag.chkClearYellowListClick(Sender: TObject);
begin
    // Si est� chequeado el Clear, entonces deschequear el Set.
    if chkClearYellowList.Checked then begin
        chkSetYellowList.Checked := False;
    end;
end;

procedure TFormCambiarEstadoTag.chkSetGrayListClick(Sender: TObject);
begin
    // Si est� chequeado el Set, entonces deschequear el Clear
    if chkSetGrayList.Checked then begin
        chkClearGrayList.Checked := False;
    end                                                                         //SS_660_MCA_20130129
    else begin                                                                  //SS_660_MCA_20130129
        chkClearGrayList.Checked := True;                                       //SS_660_MCA_20130129
    end;                                                                        //SS_660_MCA_20130129
//    chkClearGrayList.Checked := not chkSetGrayList.Checked;
end;

procedure TFormCambiarEstadoTag.chkSetBlackListClick(Sender: TObject);
begin
    // Si est� chequeado el Set, entonces deschequear el Clear
    if chkSetBlackList.Checked then begin
        chkClearBlackList.Checked := False;
    end                                                                         //SS_660_MCA_20130129
    else begin                                                                  //SS_660_MCA_20130129
        chkClearBlackList.Checked := True;                                      //SS_660_MCA_20130129
    end;                                                                        //SS_660_MCA_20130129
//    chkClearBlackList.Checked := not chkSetBlackList.Checked;
end;

procedure TFormCambiarEstadoTag.chkClearBlackListClick(Sender: TObject);
begin
    // Si est� chequeado el Clear, entonces deschequear el Set.
    if chkClearBlackList.Checked then begin
        chkSetBlackList.Checked := False;
    end                                                                         //SS_660_MCA_20130129
    else begin                                                                  //SS_660_MCA_20130129
        chkSetBlackList.Checked := True;                                        //SS_660_MCA_20130129
    end;                                                                        //SS_660_MCA_20130129
end;

procedure TFormCambiarEstadoTag.chkClearGrayListClick(Sender: TObject);
begin
    // Si est� chequeado el Clear, entonces deschequear el Set.
    if chkClearGrayList.Checked then begin
        chkSetGrayList.Checked := False;
    end                                                                         //SS_660_MCA_20130129
    else begin                                                                  //SS_660_MCA_20130129
        chkSetGrayList.Checked := True;                                         //SS_660_MCA_20130129
    end;                                                                        //SS_660_MCA_20130129
end;

procedure TFormCambiarEstadoTag.chkClearExceptionHandlingClick(Sender: TObject);
begin
    chkSetExceptionHandling.Checked := not chkClearExceptionHandling.Checked;
end;

procedure TFormCambiarEstadoTag.chkSetMMIContactOperatorClick(Sender: TObject);
begin
    chkClearMMIContactOperator.Checked := not chkSetMMIContactOperator.Checked;
end;

procedure TFormCambiarEstadoTag.chkSetMMINotOkClick(Sender: TObject);           // SS_660_MCA_20130912
begin                                                                           // SS_660_MCA_20130912
  chkClearMMINotOk.Checked := not chkSetMMINotOk.Checked;                       // SS_660_MCA_20130912
end;                                                                            // SS_660_MCA_20130912

procedure TFormCambiarEstadoTag.chkClearMMIContactOperatorClick(
  Sender: TObject);
begin
    chkSetMMIContactOperator.Checked := not chkClearMMIContactOperator.Checked;
end;

procedure TFormCambiarEstadoTag.chkClearMMINotOkClick(Sender: TObject);         // SS_660_MCA_20130912
begin                                                                           // SS_660_MCA_20130912
  chkSetMMINotOk.Checked := not chkClearMMINotOk.Checked;                       // SS_660_MCA_20130912
end;                                                                            // SS_660_MCA_20130912

procedure TFormCambiarEstadoTag.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
//    Action := caFree;
end;

procedure TFormCambiarEstadoTag.ImgAyudaClick(Sender: TObject);
Resourcestring
    MSG_AYUDA   = ' ' + CRLF
            + 'En el caso de las listas de colores, se puede solicitar levantar (tildar) o bajar (no tildar) el Activar' + CRLF
            + 'y tambi�n se puede solicitar levantar (tildar) o bajar (no tildar) el Desactivar.' + CRLF
            + CRLF
            + 'En el caso de Exception Handling y Contact Operator, siempre se requiere que seleccione' + CRLF
            + 'alguna opci�n (Activar/Desactivar).' + CRLF
            + ' ';
begin
    // Mostrar el mensaje
    MsgBoxBalloon(MSG_AYUDA, Caption, MB_ICONQUESTION, IMGAYUDA);
end;

end.
