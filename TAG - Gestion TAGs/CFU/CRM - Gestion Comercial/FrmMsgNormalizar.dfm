object FormMsgNormalizar: TFormMsgNormalizar
  Left = 281
  Top = 286
  BorderStyle = bsDialog
  ClientHeight = 83
  ClientWidth = 470
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object lblMensaje: TLabel
    Left = 2
    Top = 16
    Width = 462
    Height = 21
    Alignment = taCenter
    AutoSize = False
    Caption = 'Usted esta por Guardar una calle Desnormalizada.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object btnNormalizar: TButton
    Left = 113
    Top = 52
    Width = 113
    Height = 25
    Caption = '&Normalizar'
    Default = True
    ModalResult = 2
    TabOrder = 0
  end
  object btnContinuar: TButton
    Left = 257
    Top = 52
    Width = 113
    Height = 25
    Caption = '&Continuar'
    ModalResult = 1
    TabOrder = 1
  end
end
