{********************************** Unit Header ********************************
File Name : FrameSCDatosVehiculo.pas
Author :
Date Created:
Language : ES-AR
Description :

Revision : 1
Author :   Fsandi
Date Created: 18-05-2007
Language : ES-AR
Description : Se agreg� una verificaci�n para que cuando se ingrese un DV Z, el sistema
              verifique que esa patente no exist�a ya en la base de datos.

Revision : 2
Author :   Fsandi
Date Created: 23-07-2007
Language : ES-AR
Description : Se agrego una validacion para limpiar el codigo de vehiculo si se detecta que es
			de otra concesionaria, esto evita que puedan duplicarse vehiculos mediante el uso
			la liberacion de patentes. (SS 564 - Vehiculos activos en dos convenios.)

Revision : 3
Author :   Fsandi
Date Created: 10-08-2007
Language : ES-AR
Description : Se agrega una validacion al televia para confirmar que solo se permiten arriendos
            para tags identificados como propiedad CN en MaestroTags.

Revision : 4
Author :   Fsandi
Date Created: 22-08-2007
Language : ES-AR
Description : Se cambia el mensaje de error cuando el televia esta en blanco

Revision : 5
Author :   Fsandi
Date Created: 13-09-2007
Language : ES-AR
Description : Se agrega la funcionalidad de recuperar un veh�culo robado al momento de dar de alta.

Revision :6
    Author : vpaszkowicz
    Date : 16/04/2008
    Description : agrando el maxLength de la patente a 8 caracteres.
Revision :7
    Author : lcanteros
    Date : 21/04/2008
    Description : Se modifica para que muestre los datos del vehiculo despues
    que se ingrese el digito verificador.
Revision :8
    Author : vpaszkowicz
    Date : 15/08/2008
    Description :Reorganizo las validaciones.
        Si es patente diplom�tica valido:
            -Que no exista una patente chilena id�ntica (sin dv. Z).
        Si es patente chilena tambi�n valido que no exista una diplom�tica id�n-
        tica.
        Para la RVM valido: que sea chilena y que el DV sea distinto de Z

Revision 9
Author: mbecerra
Date: 20-Julio-2009
Description: 		Se corrige un bug, y es que si el usuario se equivoca e ingresa
            	una patente "Nueva" que ya existe, entonces el sistema despliega el mensaje,
                pero al ingresar una nueva patente inexistente, se queda con el
                c�digo de veh�culo anterior.

                Para ello se fuerza que FCodigoVehiculo = -1 cuando da el mensaje de error.
                    	(funci�n ValidarPatentes) 
                    	
    Revision :10
    Author : vpaszkowicz
    Date : 27/08/2009
    Description :Agrego la funci�n
        ComprobarCodigoVehiculo

Revision 20
	Author: mbecerra
	Date: 14-Septiembre-2009
    Description:	(Ref SS 832)
            	El cliente solicita que en las pantallas de Modificaci�n y Cambio
                de vehiculo no se vea el Digito Verificador.

Revision 11
Author: mbecerra
Date: 30-Septiembre-2009
Description:		(Ref. SS 835)
            Se agrega la validaci�n en las Altas y cambios de telev�as, con respecto
            al campo FechaFinGarantiaMoptt

    	10-Noviembre-2009
            1.-		Se agrega un mensaje de advertencia de que el telev�a est�
                	en comodato (en Altas, cambio veh�culo y cambio Televia)

Revision 12
Author: vpaszkowicz
Date: 11/11/2009
Description: Cambio la validacion por DVs. Valida por algoritmo.

Revision 13
Author: jjofre
Date: 25/03/2010
Description: (REF. SS 800-676)
            Se crea la funcion ValidaPatenteInfraccionesPendientes la cual valida
            y retorna true si la patente no posee infracciones pendientes de
            regularizar, dicha funcion es llamada desde DigitosVerificadoresExit.

Revision 14
Author      : mbecerra
Date        : 21-09-2010
Description :   (Ref SS 921)
                Se corrige un bug que al editar veh�culo, entrega el Focus al
                d�gito verificador que est� invisible
                (Ver Revision 20, SS 832)

Etiqueta	: 20160603 MGO
Descripci�n	: Se agrega baja forzada para cuentas a�adidas que pertenezcan a un 
			  convenio for�neo, pidiendo confirmaci�n al operador y resaltando en rojo

Etiqueta	: TASK_055_MGO_20160801
Descripci�n : Se deja de validar en RNVM

*******************************************************************************}

unit FrameSCDatosVehiculo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DB, ADODB, StdCtrls, VariantComboBox, DmiCtrls, RStrings, PeaProcs, PeaTypes,
  Util, UtilDB, UtilProc, DMConnection, Mensajes, FrmObservacionesGeneral,
  Convenios,Variants;
  // Rev. 13 se agrega Variants
type
  TFmeSCDatosVehiculo = class(TFrame)
    Label13: TLabel;
    Label14: TLabel;
    Label20: TLabel;
    Label47: TLabel;
    lblTipoVehiculo: TLabel;
    lblTelevia: TLabel;
    lbl_PatenteRVM: TLabel;
    cb_marca: TComboBox;
    txt_anio: TNumericEdit;
    cbTipoVehiculo: TVariantComboBox;
    txtDescripcionModelo: TEdit;
    txtPatente: TEdit;
    txtDigitoVerificador: TEdit;
    txtNumeroTelevia: TEdit;
    txt_PatenteRVM: TEdit;
    txt_DigitoVerificadorRVM: TEdit;
    VerificarFormatoPatenteChilena: TADOStoredProc;
    VerificarFormatoPatenteDiplomatica: TADOStoredProc;
    Label21: TLabel;
    cb_color: TComboBox;
    cb_modelo: TComboBox;
    GBListas: TGroupBox;
    ledVerde: TLed;
    ledGris: TLed;
    ledAmarilla: TLed;
    ledNegra: TLed;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    spLeerDatosTAG: TADOStoredProc;
    lblVehiculoRobado: TLabel;
    BtnRecuperar: TButton;
    spValidarSiTagTieneGarantia: TADOStoredProc;
    procedure PatentesKeyPress(Sender: TObject; var Key: Char);
    procedure txt_anioExit(Sender: TObject);
    procedure txtNumeroTeleviaExit(Sender: TObject);
    procedure txtNumeroTeleviaKeyPress(Sender: TObject; var Key: Char);
    procedure DigitosVerificadoresExit(DigitoVerificador: TObject);
    procedure txtDigitoVerificadorChange(Sender: TObject);
    procedure txtDescripcionModeloKeyPress(Sender: TObject; var Key: Char);
    procedure DigitosVerificadoresKeyPress(Sender: TObject; var Key: Char);
    procedure PatentesExit(Sender: TObject);
    procedure txtPatenteEnter(Sender: TObject);
    procedure txtNumeroTeleviaChange(Sender: TObject);
    procedure BtnRecuperarClick(Sender: TObject);
    procedure txtPatenteChange(Sender: TObject);
  private
    FIndicadorListas: TIndicadorListas;
    FCodigoVehiculoOriginal: Integer;
    MiListaPatente: TStringList;
    FCodigoSolicitud: Integer;
    FObservaciones: string;
    FEsConvenio: Boolean;
    FPuedeValidar: Boolean;
    FContextMark: Integer;
    FListaTags: TStringList;
    FNumeroTag: String;
    FNumeroTagAnterior: String;
    FAsignarTag: Boolean;
    FCodigoVehiculo: Integer;
    FCodigoConvenio: Integer;
    FCodigoPersona: Integer;
    FcdsVehiculos: TDataSet;
    FCambioElTag: Boolean;
    FFechaCreacion: TDateTime;
    FPuntoEntrega: Integer;
    FCodigoEstadoConservacion: integer;
    FindiceVehiculo: Integer;
    FRobado, FRecuperado : Boolean; //Revision 5
    FForaneo: Boolean; // 20160603 MGO
    FEstado: TStateFrmVehiculo;
    FCodigoUbicacionTag : integer;		//REV.11
    Function GetForaneo: Boolean;                                            // TASK_055_MGO_20160801
    Function GetTipoPatente: AnsiString;
    Procedure SetTipoPatente(Const Value: AnsiString);
    Function GetPatente: AnsiString;
    Procedure SetPatente(Const Value: AnsiString);
    Function GetCodigoMarca: Integer;
    Procedure SetCodigoMarca(Const Value: Integer);
    Function GetDescripcionMarca: AnsiString;
    Procedure SetCodigoTipo(Const Value: Integer);
    Function GetCodigoTipo: Integer;
    Function GetDescripcionTipo: AnsiString;
    Function GetDescripcionModelo: AnsiString;
    Procedure SetDescripcionModelo(Const Value: AnsiString);
    Function GetAnio: Integer;
    Procedure SetAnio(Const Value: Integer);
    function GetCategoria: Integer;
    Procedure SetCodigoColor(Const Value: Integer);
    Function GetDescripcionColor: AnsiString;
    function GetDetalleVehiculoSimple: AnsiString;
    procedure SetDigitoVerificador(Value: AnsiString);
    function GetDigitoVerificador: AnsiString;
    function ValidarDigitoVerificador(Patente, DigitoVerificador : TEdit): Boolean;
    function GetDetalleVehiculoCompleta: AnsiString;
    function BuscarPatenteSolicitud: Boolean;
    function BuscarPatenteCuenta(Patente : TEdit): Boolean;
    function BuscarExistePatente(Patente : TEdit): Boolean;
    function BuscarExistePatenteDV(Patente : TEdit; DVZ: Byte = 0): boolean;
    function ValidarAnio: Boolean;
    function ValidarPatenteChilena(Sender : TEdit): Boolean;
    function ValidaPatenteInfraccionesPendientes(Patente: TEdit): Boolean;
    //function ValidarPatenteDiplomatica(Sender : TEdit): Boolean;
    function ValidarSolicitud: Boolean;
    function ValidarConvenio: Boolean;
    function GetCodigoColor: Integer;
    function GetSerialNumber: DWord;
    function GetTag: Ansistring;
    function ValidarDigitoVerificadorTag(NumeroTag: String): Boolean;
    function GetDatoCuenta: TCuentaVehiculo;
    function GetPatenteRVM: String;
    function GetDigitoVerificadorCorrecto: Boolean;
    function GetDigitoVerificadorCorrectoRVM: Boolean;
    function GetDigitoVerificadorRVM: AnsiString;
    function CantidadTeleviasNuevos(Categoria: integer): integer;
    function GetContextMark: integer;
    function GetFechaCreacion: TDateTime;
    function ValidarArriendoTelevia (SerialNumber:DWORD; ContextMark:Integer):Boolean;  //Revision 3
    function AsignarValoresAControles(Sender:TObject; AuxTxtPatente: TEdit): boolean;
//Agregadas en rev,8
    function ValidarDatosEntrePatentes(unaPatente: TEdit; unDigitoVerificador: TEdit): Boolean;
    { Private declarations }
  public
    { Public declarations }
    Property TipoPatente: AnsiString read GetTipoPatente;
    Property Patente: AnsiString read GetPatente;
    property Foraneo: Boolean read GetForaneo;                                  // TASK_055_MGO_20160801
    property Marca: integer read GetCodigoMarca;
    property DescripcionMarca: AnsiString read GetDescripcionMarca;
    property CodigoTipo: integer read GetCodigoTipo;
    property DescripcionTipo: AnsiString read GetDescripcionTipo;
    property DescripcionModelo: AnsiString read GetDescripcionModelo;
    property Anio: integer read GetAnio;
    property Color: integer read GetCodigoColor;
    property DescripcionColor: AnsiString read GetDescripcionColor;
    property CodigoCategoria: integer read GetCategoria;
    property DigitoVerificadorCorrecto: boolean read GetDigitoVerificadorCorrecto;
    property DigitoVerificadorCorrectoRVM: boolean read GetDigitoVerificadorCorrectoRVM;
    property DigitoVerificador: AnsiString read GetDigitoVerificador;
    property DetalleVehiculoSimple: Ansistring read GetDetalleVehiculoSimple;
    property DetalleVehiculoCompleto: Ansistring read GetDetalleVehiculoCompleta;
    property Observaciones: Ansistring read FObservaciones;
    property SerialNumber:DWORD read GetSerialNumber;
    property CodigoVehiculo:Integer read FCodigoVehiculo;
    property ContextMark: integer read GetContextMark;
    property NumeroTag:AnsiString read GetTag;
    property DatoCuenta:TCuentaVehiculo read GetDatoCuenta;
    property PatenteRVM  : String read GetPatenteRVM;
    property DigitoVerificadorRVM: AnsiString read GetDigitoVerificadorRVM;
    property FechaCreacion: TDateTime read GetFechaCreacion;
    property PuntoEntrega: Integer read FPuntoEntrega;
    property CodigoEstadoConservacion: integer read FCodigoEstadoConservacion;
    property Robado: Boolean read FRobado;         //Revision 5
    property Recuperado: Boolean read FRecuperado; //Revision 5

    Function Inicializar(PuntoEntrega: Integer; CodigoTipoVehiculo: integer = 1;
        Solicitud: integer = -1; ListaPatentes: TStringList = nil;
        EsConvenio: boolean = False; ListaTags: TStringList = nil;
        ContextMark: integer = -1; CodigoConvenio: integer = -1;
        cdsVehiculos: TDataSet = nil;
        CodigoPersona: integer = -1; Estado: TStateFrmVehiculo = Solicitud; MostrarLista: boolean = True;
        FechaCreacion: TDateTime = Nulldate): Boolean; overload; //Para el Alta.

    Function Inicializar(TipoPatente, Patente, PatenteRVM , Modelo: AnsiString;
        CodigoMarca, Anio, CodigoTipo, CodigoColor: integer;
        DigitoVerificador, DigitoVerificadorRVM: string;
        PuntoEntrega: Integer;
        CodVechiculo : Integer = -1; Observaciones: string = '';
        Solicitud: integer = -1; ListaPatentes: TStringList = nil;
        EsConvenio: boolean = False;
        ListaTags: TStringList = nil; ContextMark: integer = -1;
        NumeroTag: string = ''; AsignarTag: boolean = False;
        PuedeEditarTag: Boolean = True;
        cdsVehiculos: TDataSet = nil;
        CodigoConvenio: integer = -1;
        Vendido: Boolean = False; CodigoPersona: integer = -1; Estado: TStateFrmVehiculo = Solicitud; MostrarLista: boolean = True;
        FechaCreacion: TDateTime = Nulldate; IndiceVehiculo: Integer = 0;
        esRecuperado: boolean = False): Boolean; overload;    //Revision 5

    	procedure ActualizarControlActivo;
    	function ValidarDatos(AlmacenDestino: Integer): Boolean;      //Revision 3
		function ValidarPatentes : boolean;
		function ValidarDatosVehiculo : boolean;
		function ValidarTelevia (AlmacenDestino: Integer) : boolean;   //Revision 3
        procedure ComprobarCodigoVehiculo;
  end;

implementation

resourcestring
    CAPTION_VEHICULO_RECUPERADO         = 'Veh�culo Recuperado';    //Revision 5
    BTN_RECUPERAR                       = 'Recuperar';          //Revision 5
    BTN_RESTAURAR                       = 'Cancelar';          //Revision 5

var
  //Se usa en los proc. DeshabilitarBotonDefault y HabilitarBotonDefault
  BtnConDefault: TButton = Nil;

{$R *.dfm}

function TFmeSCDatosVehiculo.Inicializar(
    PuntoEntrega: Integer;
    CodigoTipoVehiculo: integer = 1; Solicitud: integer = -1; ListaPatentes: TStringList = nil;
    EsConvenio: boolean = False; ListaTags: TStringList = nil;
    ContextMark: Integer = -1; CodigoConvenio: integer = -1;
    cdsVehiculos: TDataSet = nil;
    CodigoPersona: integer = -1; Estado: TStateFrmVehiculo = Solicitud; MostrarLista: boolean = True;
    FechaCreacion: TDateTime = Nulldate): Boolean; //Para el Alta.
begin

    FRobado := False;       //Revision 5
    FRecuperado := False;   //Revision 5
    FForaneo := False; // 20160603 MGO
    BtnRecuperar.Caption := BTN_RECUPERAR; //Revision 5
    FEstado :=  Estado;
    FPuntoEntrega := PuntoEntrega;
    FCodigoVehiculoOriginal := -1;
    SetTipoPatente(PATENTE_CHILE);
    SetCodigoMarca(1);
    SetCodigoColor(1);
    SetCodigoTipo(CodigoTipoVehiculo);
    FCodigoSolicitud := Solicitud;
    MiListaPatente := ListaPatentes;
    FEsConvenio := EsConvenio;
    FListaTags:= ListaTags;
    FContextMark:= ContextMark;
    FAsignarTag:= False;
    FCodigoVehiculo := -1;
    FPuedeValidar := True;
    FCodigoConvenio := CodigoConvenio;
    FCodigoPersona := CodigoPersona;
    FcdsVehiculos := cdsVehiculos;
    FFechaCreacion := FechaCreacion;

    ledGris.Enabled 	:= False;
    ledVerde.Enabled	:= False;
    ledNegra.Enabled 	:= False;
    ledAmarilla.Enabled := False;

    
    lblVehiculoRobado.Caption := CAPTION_VEHICULO_ROBADO;
    lblVehiculoRobado.Visible := False;
    BtnRecuperar.Enabled := False; // Revision 5
    BtnRecuperar.Visible := False; // Revision 5

    GBListas.Visible := MostrarLista;
    if not MostrarLista then Height := 155;

    case Estado of
        Peatypes.Solicitud:
            begin
                txtDigitoVerificador.Visible := False;
                txt_PatenteRVM.Enabled := False;
                txt_DigitoVerificadorRVM.Enabled := False;
                lbl_PatenteRVM.Enabled := False;
                cbTipoVehiculo.Width := cb_marca.Width;

                txt_PatenteRVM.Enabled := False;
                txt_DigitoVerificadorRVM.Enabled := txt_PatenteRVM.Enabled;
                lbl_PatenteRVM.Enabled := txt_PatenteRVM.Enabled;
            end;
        Peatypes.ConvenioAltaVehiculo:
            begin
                txtDigitoVerificador.Visible := True;
                txt_PatenteRVM.Enabled := True;
                txt_DigitoVerificadorRVM.Enabled := True;
                lbl_PatenteRVM.Enabled := True;
                lblTelevia.Visible := True;
                txtNumeroTelevia.Visible := True;
                txtNumeroTelevia.Color := $00FAEBDE;
                lblTelevia.Font.Color := clNavy;
                lblTelevia.Font.Style := [fsBold];
                txt_PatenteRVM.Enabled := False;
                txt_DigitoVerificadorRVM.Enabled := txt_PatenteRVM.Enabled;
                lbl_PatenteRVM.Enabled := txt_PatenteRVM.Enabled;
                cb_color.Enabled:=True;   // TASK_016_ECA_20160526
            end;
        Peatypes.ConvenioCambiarVehiculo:
            begin
                txtDigitoVerificador.Visible := True;
                lblTelevia.Visible := True;
                txtNumeroTelevia.Visible := True;
                txtNumeroTelevia.Color := $00FAEBDE;
                lblTelevia.Font.Color := clNavy;
                lblTelevia.Font.Style := [fsBold];
                EnableControlsInContainer(self,False);
                self.Enabled := False;
            end;
    end;
    FCambioElTag := False;
    Result := True;
end;

{******************************** Function Header ******************************
Function Name: Inicializar
Author :
Date Created :
Description :
Parameters : TipoPatente, Patente, PatenteRVM, Modelo: AnsiString; CodigoMarca, Anio, CodigoTipo, CodigoColor: integer; DigitoVerificador, DigitoVerificadorRVM: string; PuntoEntrega: Integer; CodVechiculo: Integer; Observaciones: string; Solicitud: integer; ListaPatentes: TStringList; EsConvenio: boolean; ListaTags: TStringList; ContextMark: integer; NumeroTag: string; AsignarTag, PuedeEditarTag: Boolean; cdsVehiculos: TDataSet; CodigoConvenio: integer; Vendido: Boolean; CodigoPersona: integer; Estado: TStateFrmVehiculo; MostrarLista: boolean; FechaCreacion: TDateTime; IndiceVehiculo: Integer; esRecuperado: boolean
Return Value : Boolean
Revision : 1
    Author : vpaszkowicz
    Date : 30/10/2007
    Description : Le dejo visible el bot�n si el veh�culo es recuperado y no
    tiene permisos
*******************************************************************************}
function TFmeSCDatosVehiculo.Inicializar(
    TipoPatente, Patente, PatenteRVM, Modelo: AnsiString; CodigoMarca, Anio,
    CodigoTipo, CodigoColor: integer; DigitoVerificador,
    DigitoVerificadorRVM: string;
    PuntoEntrega: Integer;
    CodVechiculo: Integer;
    Observaciones: string; Solicitud: integer; ListaPatentes: TStringList;
    EsConvenio: boolean; ListaTags: TStringList;
    ContextMark: integer; NumeroTag: string; AsignarTag,
    PuedeEditarTag: Boolean;
    cdsVehiculos: TDataSet;
    CodigoConvenio: integer; Vendido: Boolean;
    CodigoPersona: integer; Estado: TStateFrmVehiculo; MostrarLista: boolean;
    FechaCreacion: TDateTime;
    IndiceVehiculo: Integer;
    esRecuperado: boolean): Boolean;  //Revision 5
begin
    FEstado :=  Estado;
    FPuntoEntrega := PuntoEntrega;
    SetTipoPatente(TipoPatente);
    SetPatente(Patente);
    SetCodigoMarca(CodigoMarca);
    SetDescripcionModelo(Modelo);
    SetCodigoColor(CodigoColor);
    if Anio <> -1 then
        SetAnio(Anio);
    SetCodigoTipo(CodigoTipo);
    FCodigoSolicitud := Solicitud;
    FObservaciones := Observaciones;
    MiListaPatente := ListaPatentes;
    FFechaCreacion := FechaCreacion;
    FindiceVehiculo := IndiceVehiculo;

    //Revision 5
    FRobado := False;
    if esRecuperado then begin
        FRecuperado := True;
        BtnRecuperar.Caption := BTN_RESTAURAR;
        lblVehiculoRobado.Caption := CAPTION_VEHICULO_RECUPERADO;
        lblVehiculoRobado.Visible := True;
        lblVehiculoRobado.font.color := clGreen;
        if ExisteAcceso('Boton_Recuperar_Vehiculo_Robado') then begin
            BtnRecuperar.Enabled := True;
            BtnRecuperar.Visible := True;
        end else begin
            BtnRecuperar.Enabled := False;
            //BtnRecuperar.Visible := False;
            BtnRecuperar.Visible := True;
        end;
    end else begin
        FRecuperado := False;
        BtnRecuperar.Caption := BTN_RECUPERAR;
        lblVehiculoRobado.Caption := CAPTION_VEHICULO_ROBADO;
        lblVehiculoRobado.font.color := clred;
        lblVehiculoRobado.Visible := False;
        BtnRecuperar.Enabled := False;
        BtnRecuperar.Visible := False;
    end;
    //Fin de Revision 5

    ledGris.Enabled 	:= False;
    ledVerde.Enabled	:= False;
    ledNegra.Enabled 	:= False;
    ledAmarilla.Enabled := False;

    GBListas.Visible := MostrarLista;
    if not MostrarLista then Height := 155;

    FEsConvenio := EsConvenio;
    FPuedeValidar := True;
    FCodigoConvenio := CodigoConvenio;
    FListaTags:= ListaTags;
    FContextMark:= ContextMark;
    FNumeroTag:= NumeroTag;
    FNumeroTagAnterior := NumeroTag;
    FAsignarTag := AsignarTag;
    FCodigoVehiculo := CodVechiculo;
    FCodigoPersona := CodigoPersona;
    FcdsVehiculos := cdsVehiculos;

    txt_PatenteRVM.Text := Trim(PatenteRVM);
    txt_DigitoVerificadorRVM.Text := Trim(DigitoVerificadorRVM);

    txtNumeroTelevia.text := trim(FNumeroTag);

        
    case Estado of
        Peatypes.Solicitud:
            begin
                txtDigitoVerificador.Visible := False;
                txt_PatenteRVM.Enabled := False;
                txt_DigitoVerificadorRVM.Enabled := False;
                lbl_PatenteRVM.Enabled := False;
                txtNumeroTelevia.Enabled := False;
                txt_PatenteRVM.Enabled := False;
                cbTipoVehiculo.Width := cb_marca.Width;
            end;
        Peatypes.ConvenioAltaVehiculo, ConvenioActivarCuenta:
            begin
                txtDigitoVerificador.Visible := True;
                txt_PatenteRVM.Enabled := False;
                txt_DigitoVerificadorRVM.Enabled := txt_PatenteRVM.Enabled;
                lbl_PatenteRVM.Enabled := txt_PatenteRVM.Enabled;
                txtNumeroTelevia.Enabled := PuedeEditarTag and not Vendido;
                txtDigitoVerificador.Enabled := txtPatente.Enabled;
                lblTelevia.Visible := True;
                txtNumeroTelevia.Visible := True;
                txtNumeroTelevia.Color := $00FAEBDE;
                lblTelevia.Font.Color := clNavy;
                lblTelevia.Font.Style := [fsBold];
                cb_color.Enabled:=True;   // TASK_016_ECA_20160526
            end;
        Peatypes.ConvenioModiVehiculo:
            begin
       //REV.20         txtDigitoVerificador.Visible := True;
                txt_PatenteRVM.Enabled := False;
                txt_DigitoVerificadorRVM.Enabled := txt_PatenteRVM.Enabled;
                lbl_PatenteRVM.Enabled := txt_PatenteRVM.Enabled;
                txtNumeroTelevia.Enabled := PuedeEditarTag and not Vendido;
                txtPatente.Enabled := PuedeEditarTag;
                txtDigitoVerificador.Enabled := txtPatente.Enabled;
                lblTelevia.Visible := True;
                txtNumeroTelevia.Visible := True;
                txtNumeroTelevia.Color := $00FAEBDE;
                lblTelevia.Font.Color := clNavy;
                lblTelevia.Font.Style := [fsBold];
                cb_color.Enabled:=True;   // TASK_016_ECA_20160526
            end;
        Peatypes.ConvenioCambiarVehiculo:
            begin
     //REV.20           txtDigitoVerificador.Visible := True;
                lblTelevia.Visible := True;
                txtNumeroTelevia.Visible := True;
                txtNumeroTelevia.Color := $00FAEBDE;
                lblTelevia.Font.Color := clNavy;
                lblTelevia.Font.Style := [fsBold];
                EnableControlsInContainer(self,False);
                self.Enabled := False;
                cb_color.Enabled:=True;   // TASK_016_ECA_20160526
            end;
    end;

    if trim(DigitoVerificador) <> '' then txtDigitoVerificador.Text := trim(DigitoVerificador[1]);

    FCodigoVehiculoOriginal := FCodigoVehiculo;
    FCambioElTag := False;

    Result := True;
end;

procedure TFmeSCDatosVehiculo.ActualizarControlActivo;
//Se llama en el OnShow del form que usa este Frame
begin
    if FAsignarTag then begin
       if (txtNumeroTelevia.Visible and txtNumeroTelevia.Enabled) then txtNumeroTelevia.SetFocus;
    end
    else begin
        if txtPatente.Enabled then
            txtPatente.setFocus
        else
            if (cbTipoVehiculo.Enabled) then cbTipoVehiculo.SetFocus;
    end;
end;

function ValidarExistenciaTelevia(Conn :TADOConnection; ContextMark: integer; SerialNumber: DWORD): boolean;
begin
    result := QueryGetValueInt(DMConnections.BaseCAC,'EXEC ValidarUbicacionTag' + '''' +
              inttostr(ContextMark) + ''',''' + intToStr(SerialNumber) + '''') = 1;
end;

function ValidarCategoriaTelevia(Conn :TADOConnection; CodigoTipoVehiculo: integer; ContextMark: integer; SerialNumber: DWORD): boolean;
begin
    Result := QueryGetValueInt(DMConnections.BaseCAC,'SELECT dbo.ValidarCategoriaTAGporTipoVehiculo(' + inttostr(CodigoTipoVehiculo) + ',' +
     '0 ,' + inttostr(ContextMark) + ',' + intToStr(SerialNumber) + ')') = 1;
end;

procedure ValidarTeleviaInterfase(Conn :TADOConnection; CodigoPuntoEntrega: integer; ContextMark: integer; SerialNumber: DWORD);
begin
    QueryExecute(DMConnections.BaseCAC,'EXEC ValidarInterfaseTag' + '''' +
              inttostr(ContextMark) + ''',''' + intToStr(SerialNumber) + ''',''' + inttostr(CodigoPuntoEntrega) + ''',''' + Trim(UsuarioSistema) + '''');
end;

function ValidarSituacionTelevia(Conn :TADOConnection; ContextMark: integer; SerialNumber: DWORD): boolean;
var
    CodigoSituacionEntregadoCliente: integer;
begin
    CodigoSituacionEntregadoCliente := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT DBO.CONST_TAG_ESTADO_SITUACION_PENDIENTE()');
    result := QueryGetValueInt(DMConnections.BaseCAC,'EXEC ValidarSituacionTag' + '''' +
              inttostr(ContextMark) + ''',''' + intToStr(SerialNumber) + ''',''' + inttostr(CodigoSituacionEntregadoCliente) + '''') = 1;
    if not result then begin
        CodigoSituacionEntregadoCliente := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT DBO.CONST_TAG_ESTADO_SITUACION_DISPONIBLE()');
        result := QueryGetValueInt(DMConnections.BaseCAC,'EXEC ValidarSituacionTag' + '''' +
                  inttostr(ContextMark) + ''',''' + intToStr(SerialNumber) + ''',''' + inttostr(CodigoSituacionEntregadoCliente) + '''') = 1;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ExisteTeleviaEnCuentaVigente
  Author:    lgisuk
  Date Created: 01/03/2005
  Description: Obtengo si el televia existe en una cuenta vigente
  Parameters: None
  Return Value: Boolean
  Revision :1
      Author : vpaszkowicz
      Date : 21/10/2008
      Description :Corrijo los tipos de string a integer ya que la funci�n falla.
-----------------------------------------------------------------------------}
function ExisteTeleviaEnCuentaVigente(Conn :TADOConnection; ContextMark: integer; SerialNumber: DWORD): boolean;
begin
    Result := QueryGetValueInt(DMConnections.BaseCAC,'SELECT DBO.ExisteTeleviaEnCuentaVigente(' + inttostr(ContextMark) + ',' + intToStr(SerialNumber) + ')') = 1;
end;


{-----------------------------------------------------------------------------
  Function Name: ValidarDatos
  Author:    flamas
  Date Created: 01/03/2005
  Description: Valida los Datos de Petente, Veh�culo, Telev�a...
  Parameters: None
  Return Value: Boolean
-----------------------------------------------------------------------------}
function TFmeSCDatosVehiculo.ValidarDatos(AlmacenDestino: Integer): Boolean;   //Revision 3
begin
    Result := False;
	Screen.Cursor := crHourglass;

    try
		// Valida la Patente y Patente RVM
    	if not ValidarPatentes then Exit;

		// Valida los Datos del Veh�culo
    	if not ValidarDatosVehiculo then Exit;

		// Valida los Datos del Telev�a
    	if not ValidarTelevia(AlmacenDestino) then Exit; //Revision 3

		// Si es Convenio Valida los Datos del Convenio
    	if FEsConvenio and not ValidarConvenio then Exit;

		// Si es Solicitud Valida los Datos de la Solicitud
    	if not FEsConvenio and not ValidarSolicitud then Exit;

    	Result := True;
    finally
		Screen.Cursor := crDefault;
    end;
end;

{-----------------------------------------------------------------------------
  Function Name: ValidaPatente
  Author:    flamas
  Date Created: 01/03/2005
  Description: Valida los datos de la Patente
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
function TFmeSCDatosVehiculo.ValidarPatentes : boolean;
resourcestring
MSG_ERROR_ROBADO = 'Veh�culo marcado como robado';
MSG_ERROR_VEHICULO_RECUPERAR_ROBADO = 'Recupere el veh�culo o ingrese otra patente';
MSG_OTRA_CONCESIONARIA = 'El veh�culo pertenece a otra concesionaria. Si lo a�ade, se forzar� la baja del convenio for�neo. �Desea continuar?';  // 20160603 MGO
MSG_OTRA_CONCESIONARIA_MISMO_TITULAR = 'El veh�culo pertenece a un convenio del mismo titular en otra concesionaria.';  // TASK_057_MGO_20160803
var
    Titulo, Descripcion, Comentario, ResultadoExiste, PatenteAsociada, ConvenioPatente: string;
    // INICIO : TASK_057_MGO_20160803
    function EsMismoTitular(CodigoPersona: Integer; Patente: string): Boolean;
    var
        spObtenerPersonaPatente: TADOStoredProc;
    begin
        Result := False;

        spObtenerPersonaPatente := TADOStoredProc.Create(Self);
        try
            spObtenerPersonaPatente.Connection := DMConnections.BaseCAC;
            spObtenerPersonaPatente.ProcedureName := 'ObtenerPersonaPatente';
            spObtenerPersonaPatente.Parameters.Refresh;
            spObtenerPersonaPatente.Parameters.ParamByName('@Patente').Value := Patente;
            spObtenerPersonaPatente.Open;

            if spObtenerPersonaPatente.Parameters.ParamByName('@RETURN_VALUE').Value < 0 then
                raise Exception.Create(spObtenerPersonaPatente.Parameters.ParamByName('@ErrorDescription').Value);

            Result := (spObtenerPersonaPatente.FieldByName('CodigoPersona').AsInteger = CodigoPersona);

            spObtenerPersonaPatente.Close;
        finally
            spObtenerPersonaPatente.Free;
        end;
    end;
    // FIN : TASK_057_MGO_20160803
begin
	Result := False;

    // La patente no puede quedar en blanco
    if (Trim(txtPatente.Text) = '') then begin
        MsgBoxBalloon(MSG_ERROR_VALIDAR_PATENTE, MSG_ERROR_PATENTE, MB_ICONERROR, txtPatente );
        txtPatente.SetFocus;
        txtPatente.SelStart := 1000;
        Exit;
    end;

    FForaneo := False;	// 20160603 MGO
	// Valida que el Veh�culo no sea de otra Concesionaria
	if ValidarVehiculoOtraConcesionaria(DMConnections.BaseCAC,Patente) then begin

        // INICIO : TASK_057_MGO_20160803
//        if (FCodigoPersona > -1) and EsMismoTitular(FCodigoPersona, txtPatente.Text) then begin
//            MsgBoxBalloon(MSG_OTRA_CONCESIONARIA_MISMO_TITULAR, MSG_ERROR_PATENTE, MB_ICONERROR, txtPatente);
//            txtPatente.SetFocus;
//            txtPatente.SelStart := 1000;
//            Exit;
//        end;
        // FIN : TASK_057_MGO_20160803

         if MsgBox(MSG_OTRA_CONCESIONARIA, 'Confirmar', MB_ICONQUESTION+MB_YESNO) = mrNo then begin
            txtPatente.SetFocus;
            txtPatente.SelStart := 1000;
            Exit;
        end;
        FForaneo := True;
    end;

    // Valida que est� puesto el D�gito Verificador
	if (txtDigitoVerificador.Visible) and (Trim(txtDigitoVerificador.Text) = '' )then begin
        MsgBoxBalloon(MSG_VALIDAR_DIGITO_VERIFICADOR, MSG_ERROR_PATENTE, MB_ICONERROR, txtDigitoVerificador);
        txtDigitoVerificador.SetFocus;
        txtDigitoVerificador.SelStart := 1000;
        Exit;
    end;

    // Valida que el Veh�culo no sea robado
	if (lblVehiculoRobado.Visible)then begin
    //revision 5
    //Verificamos que si el vehiculo es robado, puede que el usuario lo haya recuperado al dar de alta
        if not Recuperado then begin
            MsgBoxBalloon(MSG_ERROR_VEHICULO_RECUPERAR_ROBADO, MSG_ERROR_ROBADO, MB_ICONERROR, txtPatente );
            txtPatente.SetFocus;
            txtPatente.SelStart := 1000;
            Exit;
        end;
     //Fin de revision 5
    end;

	// Valida que la Patente no exista en este Convenio
    if ExisteCadenaEnStringList(MiListaPatente, trim(txtPatente.Text)) then begin
		ObtenerMensaje(DMConnections.BaseCAC, iif(FEsConvenio, MENSAJE_PATENTE_EXISTENTE_CONVENIO, MENSAJE_PATENTE_EXISTENTE_SOLICITUD), Titulo, Descripcion, Comentario);
        MsgBoxBalloon(Descripcion, Titulo, MB_ICONERROR,txtPatente );
        txtPatente.SetFocus;
        txtPatente.SelStart := 1000;
        FCodigoVehiculo := -1;				//REV.9
        Exit;
    end;

	// Valida que la Patente RVM no exista en este Convenio
    if (Trim(txt_PatenteRVM.Text) <> '') and (ExisteCadenaEnStringList(MiListaPatente, trim(txt_PatenteRVM.Text))) then begin
        ObtenerMensaje(DMConnections.BaseCAC, iif(FEsConvenio, MENSAJE_PATENTE_EXISTENTE_CONVENIO, MENSAJE_PATENTE_EXISTENTE_SOLICITUD), Titulo, Descripcion, Comentario);
        MsgBoxBalloon(Descripcion, Titulo, MB_ICONERROR, txt_PatenteRVM );
        txt_PatenteRVM.SetFocus;
        txt_PatenteRVM.SelStart := 1000;
        FCodigoVehiculo := -1;				//REV.9
        Exit;
    end;

//	// Valida que la Patente y la PAtente RVM no sean iguales
//    if (Trim(txt_PatenteRVM.Text) = Trim(txtPatente.Text)) then begin
//        MsgBoxBalloon(MSG_VALIDAR_PATENTE_Y_PATENTERVM_IGUALES, MSG_ERROR_PATENTE, MB_ICONERROR, txtPatente );
//        txtPatente.SetFocus;
//        txtPatente.SelStart := 1000;
//        Exit;
//    end;

    // Valida que la patente no este en algun otro convenio
    {if txtPatente.enabled and BuscarPatenteCuenta(txtPatente) then begin
        MsgBoxBalloon(MSG_ERROR_PATENTE_CUENTA, MSG_ERROR_PATENTE, MB_ICONSTOP, txtPatente);
        txtPatente.SetFocus;
        txtPatente.SelStart := 1000;
        Exit;
    end;}
    { INICIO : 20160603 MGO
    // Valida que la patente no este en algun otro convenio y avisa si esta asociada a una patente extranjera
    if txtPatente.enabled then begin
    }
    // Valida que la patente no este en algun otro convenio no for�neo
    if (txtPatente.Enabled) and (not FForaneo) then begin
    // FIN : 20160603 MGO
        ResultadoExiste := ExistePatenteCuenta(DMConnections.BaseCAC, txtPatente, FCodigoConvenio, PatenteAsociada, ConvenioPatente);
        if ResultadoExiste = '1' then begin
            MsgBoxBalloon(format(MSG_ERROR_PATENTE_CUENTA_CONVENIO, [trim(ConvenioPatente)]), MSG_ERROR_PATENTE, MB_ICONSTOP, txtPatente);
            txtPatente.SetFocus;
            txtPatente.SelStart := 1000;
            FCodigoVehiculo := -1;			//REV.9
            Exit;
        end else
            if ResultadoExiste = '2' then begin
                MsgBoxBalloon(Format(MSG_ERROR_PATENTE_CUENTA_ASOCIADA_DIGITO_Z, [trim(ConvenioPatente), trim(PatenteAsociada)]), MSG_ERROR_PATENTE, MB_ICONSTOP, txtPatente);
                txtPatente.SetFocus;
                txtPatente.SelStart := 1000;
                FCodigoVehiculo := -1;		//REV.9
                Exit;
            end;
    end;

    // Valida que la Patente RVM no exista en otra cuenta
    {if (txt_PatenteRVM.Enabled) and  (trim(txt_PatenteRVM.Text) <> '') and BuscarPatenteCuenta(txt_PatenteRVM) then begin
        MsgBoxBalloon(MSG_ERROR_PATENTE_CUENTA, MSG_ERROR_PATENTE, MB_ICONSTOP, txt_PatenteRVM);
        txt_PatenteRVM.SetFocus;
        txt_PatenteRVM.SelStart := 1000;
        Exit;
    end;}
    { INICIO : TASK_055_MGO_20160801
    if txt_PatenteRVM.enabled then begin
        ResultadoExiste := ExistePatenteCuenta(DMConnections.BaseCAC, txt_PatenteRVM, FCodigoConvenio, PatenteAsociada, ConvenioPatente);
        if ResultadoExiste = '1' then begin
            MsgBoxBalloon(format(MSG_ERROR_PATENTE_CUENTA_CONVENIO, [trim(ConvenioPatente)]), MSG_ERROR_PATENTE, MB_ICONSTOP, txt_PatenteRVM);
            txt_PatenteRVM.SetFocus;
            txt_PatenteRVM.SelStart := 1000;
            Exit;
        end else
            if ResultadoExiste = '2' then begin
                MsgBoxBalloon(Format(MSG_ERROR_PATENTE_CUENTA_ASOCIADA_DIGITO_Z, [trim(ConvenioPatente), trim(PatenteAsociada)]), MSG_ERROR_PATENTE, MB_ICONSTOP, txt_PatenteRVM);
                txt_PatenteRVM.SetFocus;
                txt_PatenteRVM.SelStart := 1000;
                Exit;
            end;
    end;
    } // FIN : TASK_055_MGO_20160801

    Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: ValidarDatosVehiculo
  Author:    flamas
  Date Created: 01/03/2005
  Description:	Valida los Datos del Veh�culo (Marca y A�o)
  Parameters: None
  Return Value: boolean
-----------------------------------------------------------------------------}
function TFmeSCDatosVehiculo.ValidarDatosVehiculo : boolean;
begin
    Result := False;

    // Valida que est� seleccionada la Marca del Veh�culo
    if (cb_Marca.ItemIndex < 0) then begin
        MsgBoxBalloon(MSG_ERROR_VALIDAR_MARCA, MSG_ERROR_DATOS_VEHICULO, MB_ICONERROR, cb_marca );
        cb_marca.SetFocus;
        Exit;
    end;

    // Valida que est� ingresado el A�o
    if (Trim(txt_anio.Text) = '') or (not EsAnioCorrecto(txt_anio.ValueInt)) then begin
        MsgBoxBalloon(MSG_ERROR_VALIDAR_ANIO, MSG_ERROR_DATOS_VEHICULO, MB_ICONERROR, txt_anio );
        txt_anio.SetFocus;
        txt_anio.SelStart := 1000;
        Exit;
    end;

    Result := True;
end;

{-----------------------------------------------------------------------------
  Function Name: ValidarTelevia
  Author:    flamas
  Date Created: 01/03/2005
  Description: Valida los datos del Telev�a
  Parameters: None
  Return Value: None
  Revision :1
      Author : vpaszkowicz
      Date : 16/10/2008
      Description : Quito la validaci�n del TagVendido y corrijo la condici�n de
      Tag en Cliente.

  Revision 2
  Author: mbecerra
  Date: 30-Dic-2009
  Description:	(Ref SS 769) Se modifica para incluir en la validaci�n MOP
            	los telev�as que est�n en el almacen de Arriendo en Cuotas
-----------------------------------------------------------------------------}
function TFmeSCDatosVehiculo.ValidarTelevia (AlmacenDestino: Integer) : boolean; //Revision 3
Resourcestring
    MSG_ERROR_EXIST_IN_ACTIVE_ACCOUNT = 'Existe en una Cuenta Activa o Suspendida.';
    MSG_ERROR_ARRIENDO_INVALIDO       = 'El Televia tiene propiedad MOP, no puede ser arrendado'; //Revision 3
var
    AlmacenPuntoEntrega	: integer;
    CategoriaVehiculo	: integer;

    //REV.11
    Mensaje : string;
    CodigoRetorno : integer;
begin
    Result := False;

    // Valida que el Tag corresponda a la Categor�a del Veh�culo
	if not ValidarCategoriaTelevia(DMConnections.BaseCAC, GetCodigoTipo, ContextMark, GetSerialNumber) then begin
        if txtNumeroTelevia.Enabled then begin
            MsgBoxBalloon(MSG_ERROR_CATEGORIA_TAG, MSG_CAPTION_ASIGNAR_TAG, MB_ICONERROR, txtNumeroTelevia );
            txtNumeroTelevia.SetFocus;
            txtNumeroTelevia.SelStart := 1000;
        end else begin
            MsgBoxBalloon(MSG_ERROR_CATEGORIA_TAG, MSG_CAPTION_ASIGNAR_TAG, MB_ICONERROR, cbTipoVehiculo );
            cbTipoVehiculo.SetFocus;
        end;
        Exit;
    end;

	// Si el telev�a no est� habilitado, no valida nada mas
    if not txtNumeroTelevia.Enabled then begin
    	Result := True;
        Exit;
    end;

    // Valida que el Telev�a no est� en Blanco
    if (trim(txtNumeroTelevia.Text) = '') then begin
        MsgBoxBalloon(MSG_ERROR_TAG_VACIO, MSG_CAPTION_ASIGNAR_TAG, MB_ICONERROR, txtNumeroTelevia );
        txtNumeroTelevia.SetFocus;
        txtNumeroTelevia.SelStart := 1000;
        Exit;
    end;

    // Valida la longitud del N�mero de Telev�a
	if (Length(txtNumeroTelevia.Text) < 10) then begin
        MsgBoxBalloon(MSG_ERROR_NUMERO_TAG, MSG_CAPTION_ASIGNAR_TAG, MB_ICONERROR, txtNumeroTelevia );
        txtNumeroTelevia.SetFocus;
        txtNumeroTelevia.SelStart := 1000;
        Exit;
    end;

    // Valida el D�gito Verificador del Telev�a
    if (not ValidarDigitoVerificadorTag(txtNumeroTelevia.Text)) then begin
        MsgBoxBalloon(MSG_ERROR_VERIFICADOR_TAG, MSG_CAPTION_ASIGNAR_TAG, MB_ICONERROR, txtNumeroTelevia );
        txtNumeroTelevia.SetFocus;
        txtNumeroTelevia.SelStart := 1000;
        Exit;
    end;

    // Valida el estado del Telev�a
	if not(FCodigoEstadoConservacion In [ESTADO_CONSERVACION_BUENO, ESTADO_CONSERVACION_NO_DEVUELTO]) then begin
        MsgBoxBalloon(MSG_ERROR_TELEVIA_MALO, MSG_CAPTION_ASIGNAR_TAG, MB_ICONERROR, txtNumeroTelevia );
        txtNumeroTelevia.SetFocus;
        txtNumeroTelevia.SelStart := 1000;
        Exit;
    end;

    // Valida si el TAG est� ingresado en este convenio
	if ExisteCadenaEnStringList(FListaTags,Trim(intToStr(GetSerialNumber))) then begin
        MsgBoxBalloon(MSG_ERROR_NUMERO_TAG_COVENIO, MSG_CAPTION_ASIGNAR_TAG, MB_ICONERROR, txtNumeroTelevia );
        txtNumeroTelevia.SetFocus;
        txtNumeroTelevia.SelStart := 1000;
        Exit;
    end;

    // Obtiene m�s Datos del Telev�a
  	AlmacenPuntoEntrega	:= ObtenerAlmacenPuntoEntrega(DMConnections.BaseCAC, PuntoEntrega);
    CategoriaVehiculo 	:= ObtenerCategoriaVehiculo(DMConnections.BaseCAC, GetCodigoTipo);

    // Valida que el Tag exista y est� en el Cliente
	if (not ValidarExistenciaTelevia(DMConnections.BaseCAC, ContextMark, GetSerialNumber)) then begin
        MsgBoxBalloon(MSG_ERROR_NO_ESTA_TAG, MSG_CAPTION_ASIGNAR_TAG, MB_ICONERROR, txtNumeroTelevia );
        txtNumeroTelevia.SetFocus;
        txtNumeroTelevia.SelStart := 1000;
        Exit;
    end;

    // Valida que no est� el Telev�a en una cuenta activa
    if	(Trim(FNumeroTagAnterior) <> Trim(txtNumeroTelevia.Text)) and
        (ExisteTeleviaEnCuentaVigente(DMConnections.BaseCAC, ContextMark, GetSerialNumber)) then begin
        MsgBoxBalloon(MSG_ERROR_SITUACION_TAG + MSG_ERROR_EXIST_IN_ACTIVE_ACCOUNT, MSG_CAPTION_ASIGNAR_TAG, MB_ICONERROR, txtNumeroTelevia );
        txtNumeroTelevia.SetFocus;
        txtNumeroTelevia.SelStart := 1000;
        Exit;
    end;
    
    // Valida que est� el Telev�a est� Disponible
    if	(Trim(FNumeroTagAnterior) <> Trim(txtNumeroTelevia.Text)) and
        (not ValidarSituacionTelevia(DMConnections.BaseCAC, ContextMark, GetSerialNumber)) then begin
        MsgBoxBalloon(MSG_ERROR_SITUACION_TAG, MSG_CAPTION_ASIGNAR_TAG, MB_ICONERROR, txtNumeroTelevia );
        txtNumeroTelevia.SetFocus;
        txtNumeroTelevia.SelStart := 1000;
        Exit;
    end;
            
    // Valida que el Telev�a est� en el Almac�n correspondiente
    if 	(Trim(FNumeroTagAnterior) <> Trim(txtNumeroTelevia.Text)) and
        (not ValidarUbicacionTelevia(DMConnections.BaseCAC, ContextMark, GetSerialNumber, PuntoEntrega)) then begin
        MsgBoxBalloon(MSG_ERROR_UBICACION_TAG, MSG_CAPTION_ASIGNAR_TAG, MB_ICONERROR, txtNumeroTelevia );
        txtNumeroTelevia.SetFocus;
        txtNumeroTelevia.SelStart := 1000;
        Exit;
    end;
            
    //Esta parte no va, ya que tiene que tener en stock por tener abierto un turno.
    // Valida que haya Stock de Telev�as
    {if	(Trim(FNumeroTagAnterior) <> Trim(txtNumeroTelevia.Text)) and
        (not ValidarStockAlmacen(DMConnections.BaseCAC,AlmacenPuntoEntrega,CategoriaVehiculo)) then begin
        MsgBoxBalloon(MSG_ERROR_STOCK_TAG, MSG_CAPTION_ASIGNAR_TAG, MB_ICONERROR, txtNumeroTelevia );
        txtNumeroTelevia.SetFocus;
        txtNumeroTelevia.SelStart := 1000;
        Exit;
    end;

    // Valida que haya Stock de Telev�as Asignados a esa Categor�a
    if	(Trim(FNumeroTagAnterior) <> Trim(txtNumeroTelevia.Text)) and
        (ObtenerStockAlmacen(DMConnections.BaseCAC, AlmacenPuntoEntrega, CategoriaVehiculo) < CantidadTeleviasNuevos(CategoriaVehiculo)+1) then begin
        MsgBoxBalloon(MSG_ERROR_STOCK_TAG_ASIGNADOS, MSG_CAPTION_ASIGNAR_TAG, MB_ICONERROR, txtNumeroTelevia );
        txtNumeroTelevia.SetFocus;
        txtNumeroTelevia.SelStart := 1000;
        Exit;
    end; }

//Revision 3
    // Valida que el Telev�a pueda darse en arriendo, si eso es lo que se pretende
    if  AlmacenDestino in [CONST_ALMACEN_ARRIENDO, CONST_ALMACEN_ENTREGADO_EN_CUOTAS] then begin //Revision 3
        if not ValidarArriendoTelevia(SerialNumber, ContextMark) then begin
            MsgBoxBalloon(MSG_ERROR_ARRIENDO_INVALIDO, MSG_CAPTION_ASIGNAR_TAG, MB_ICONERROR, txtNumeroTelevia );
            txtNumeroTelevia.SetFocus;
            txtNumeroTelevia.SelStart := 1000;
            Exit;
        end;
    end;

	Result := True;
end;

//TIPO DE PATENTE
Function TFmeSCDatosVehiculo.GetTipoPatente: AnsiString;
begin
    Result := PATENTE_CHILE;
end;

Procedure TFmeSCDatosVehiculo.SetTipoPatente(Const Value: AnsiString);
begin
//    if Trim(Value) = TipoPatente then Exit;
//    CargarTiposPatente(DMConnections.BaseCAC, mcPatente, Trim(Value));
end;

//PATENTE
Procedure TFmeSCDatosVehiculo.SetPatente(Const Value: AnsiString);
begin
    //if Trim(Value) = Patente then Exit;
    txtPatente.Text := Trim(Value);
end;
Function TFmeSCDatosVehiculo.GetPatente: AnsiString;
begin
    Result := Trim(txtPatente.Text);
end;

// INICIO : TASK_055_MGO_20160801
function TFmeSCDatosVehiculo.GetForaneo: Boolean;
begin
    Result := FForaneo;
end;
// FIN : TASK_055_MGO_20160801

//MARCA
Function TFmeSCDatosVehiculo.GetCodigoMarca: integer;
begin
    Result := Ival(StrRight(cb_Marca.Text, 10));
end;

Procedure TFmeSCDatosVehiculo.SetCodigoMarca(Const Value: integer);
begin
    if Value = Marca then Exit;
    CargarMarcasVehiculos(DMConnections.BaseCAC, cb_marca, Value);
end;

Function TFmeSCDatosVehiculo.GetDescripcionMarca: AnsiString;
begin
    if cb_Marca.ItemIndex < 0 then Result := ''
    else Result := Trim(StrLeft(cb_Marca.Text, 40));
end;

Procedure TFmeSCDatosVehiculo.SetCodigoTipo(Const Value: integer);
begin
   if Value = CodigoTipo then Exit;
   CargarVehiculosTipos(DMConnections.BaseCAC, cbTipoVehiculo, False, Value);
end;

Function TFmeSCDatosVehiculo.GetCodigoTipo: integer;
begin
    Result := Ival(cbTipoVehiculo.Value);
end;

Function TFmeSCDatosVehiculo.GetDescripcionModelo: AnsiString;
begin
//    if cb_Modelo.ItemIndex < 0 then Result := ''
//    else Result := Trim(StrLeft(cb_Modelo.Text, 40));
    result := Trim(txtDescripcionModelo.Text);
end;

//ANIO


Function TFmeSCDatosVehiculo.GetAnio: integer;
begin
    Result := txt_Anio.ValueInt;
end;

Procedure TFmeSCDatosVehiculo.SetAnio(Const Value: integer);
ResourceString
    MSG_CAPTION_VALIDAR_ANIO    = 'Validar A�o';
    MSG_ERROR_VALIDAR_ANIO      = 'El a�o especificado esta fuera del rango permitido';
begin
    if Value = Anio then Exit;
    if EsAnioCorrecto(Value) then txt_Anio.ValueInt := Value
    else MsgBox( MSG_ERROR_VALIDAR_ANIO, MSG_CAPTION_VALIDAR_ANIO, MB_ICONSTOP);
end;

//COLOR.
Function TFmeSCDatosVehiculo.GetCodigoColor: integer;
begin
    Result := Ival(StrRight(cb_Color.Text, 10));
end;

Procedure TFmeSCDatosVehiculo.SetCodigoColor(Const Value: integer);
begin
    if Value = Color then Exit;
    CargarColoresVehiculos(DMConnections.BaseCAC, cb_Color, Value);
end;

Function TFmeSCDatosVehiculo.GetDescripcionColor: AnsiString;
begin
    if cb_Color.ItemIndex < 0 then Result := ''
    else Result := Trim(StrLeft(cb_Color.Text, 40));
end;

Function TFmeSCDatosVehiculo.GetDescripcionTipo: AnsiString;
begin
    if cbTipoVehiculo.ItemIndex < 0 then Result := ''
    else Result := Trim(cbTipoVehiculo.Items[cbTipoVehiculo.ItemIndex].Caption);
end;

//DETALLES
Function TFmeSCDatosVehiculo.GetDetalleVehiculoSimple: AnsiString;
begin
    Result := DescripcionMarca + ', ' + DescripcionModelo + ', ' + DescripcionColor;
end;

Function TFmeSCDatosVehiculo.GetDetalleVehiculoCompleta: AnsiString;
begin
	Result := DetalleVehiculoSimple + ' ' + STR_PATENTE + ': ' + Patente;
end;

procedure TFmeSCDatosVehiculo.SetDescripcionModelo(
  const Value: AnsiString);
begin
    txtDescripcionModelo.Text := value;
end;


procedure TFmeSCDatosVehiculo.PatentesKeyPress(Sender: TObject;
  var Key: Char);
begin
    if not (Key  in ['0'..'9', 'a'..'z', 'A'..'Z', #8]) then
        Key := #0;
end;

function CancelandoCambios: Boolean;
begin
    Result := (Screen.ActiveControl is TButton) and (TButton(Screen.ActiveControl).Cancel);
end;

function AceptandoCambios: Boolean;
begin
    Result := (Screen.ActiveControl is TButton) and (TButton(Screen.ActiveControl).Default);
end;

procedure TFmeSCDatosVehiculo.txt_anioExit(Sender: TObject);
begin
    if CancelandoCambios then exit;
    ValidarAnio;
end;

function TFmeSCDatosVehiculo.ValidarAnio: boolean;
begin
    Result := False;
    if Trim(txt_anio.Text) <> '' then begin
        if not (EsAnioCorrecto(txt_anio.ValueInt)) then begin
            MsgBoxBalloon(MSG_ERROR_VALIDAR_ANIO, CAPTION_VALIDAR_DATOS_VEHICULO, MB_ICONSTOP, txt_Anio);
            Exit;
        end;
    end;
	Result := True;
end;

//Revision 3
function TFmeSCDatosVehiculo.ValidarArriendoTelevia(SerialNumber: DWORD;
  ContextMark: Integer): Boolean;
var
    Propiedad : string;
    NombreCortoConcesionariaNativa: string;                                     //SS_1147_MCA_20140408
begin
    Result := False;
    Propiedad := EmptyStr;
    Propiedad := QueryGetValue(DMConnections.BaseCAC,format('select dbo.ObtenerPropiedadTAG (%d,%d)', [SerialNumber, ContextMark]));

    NombreCortoConcesionariaNativa := QueryGetValue(DMConnections.BaseCAC, ('SELECT dbo.ObtenerNombreCortoConcesionaria(dbo.ObtenerConcesionariaNativa())'));	//SS_1147_MCA_20140408
    // Valida que el Telev�a solo puede darse en arriendo si es tipo nativo --'CN'
    //if Trim(Propiedad) = 'CN' then begin										//SS_1147_MCA_20140408
    if Trim(Propiedad) = NombreCortoConcesionariaNativa then begin              //SS_1147_MCA_20140408
        Result := True;
    end;
end;
//Fin de Revision 3


function TFmeSCDatosVehiculo.ValidarPatenteChilena(Sender : TEdit): Boolean;
begin
    Result := False;
    if CaracteresIngresadosSonDePatenteChilena(Sender.Text) then
        with VerificarFormatoPatenteChilena do begin
            Parameters.ParamByName('@Patente').value := Sender.Text;
            ExecProc;
            Result := (Parameters.ParamByName('@Return_Value').Value = 1);
        end;
end;

(*function TFmeSCDatosVehiculo.ValidarPatenteDiplomatica(Sender : TEdit): Boolean;
begin
    Result := False;
    if CaracteresIngresadosSonDePatenteChilena(Sender.Text) then
        with VerificarFormatoPatenteDiplomatica do begin
            Parameters.ParamByName('@Patente').Value := Sender.Text;
            ExecProc;
            Result := (Parameters.ParamByName('@Return_Value').Value = 1);
		end;
end;*)

function TFmeSCDatosVehiculo.GetDigitoVerificador: AnsiString;
begin
    if Trim(txtDigitoVerificador.Text) <> '' then result:= Trim(txtDigitoVerificador.Text)
    else result := '';
end;


{******************************** Function Header ******************************
Function Name: ValidarDigitoVerificador
Author :
Date Created :
Description :
Parameters : Patente,DigitoVerificador : TEdit
Return Value : Boolean
Revision : 2
    Author : vpaszkowicz
    Date : 11/11/2009
    Description : Comento las validaciones por existencia. Valido por algoritmo.
*******************************************************************************}
function TFmeSCDatosVehiculo.ValidarDigitoVerificador(Patente,DigitoVerificador : TEdit): boolean;
Resourcestring
    MSG_ERROR_PATENTE_RVM = 'La patente especificada para la patente RNVM ya existe';
    MSG_ERROR_PATENTE_EXISTE_COMO_NORMAL = 'La Patente ya existe en el Maestro de Veh�culos';
begin
  if Trim(DigitoVerificador.Text) = 'Z' then begin
       result := True
  end
//Revision  1
        // Valida que la patente (de la patente rnvm) no este en algun otro convenio
        //if txtPatente.enabled and BuscarExistePatente(txtPatente) then begin
      //  if txtPatente.enabled and BuscarExistePatenteDV(txtPatente) then begin
      //      MsgBoxBalloon(MSG_ERROR_PATENTE_EXISTE_COMO_NORMAL, MSG_ERROR_PATENTE, MB_ICONSTOP, txtDigitoVerificador);
      //      result := False;
      //  end else begin
      //      result := True;
      //  end;
//Fin Revision  1
    else begin
        if (QueryGetValue(DMConnections.BaseCAC, 'Exec VerificarPatente ''' + Trim(Patente.Text) +
            ''',''' + Trim(DigitoVerificador.Text) + '''') = '0') then
      //No es V�lida
            Result := False
        else
            Result := True;
    end;

end;

function TFmeSCDatosVehiculo.BuscarPatenteCuenta(Patente : TEdit): boolean;
begin
    if (QueryGetValue(DMConnections.BaseCAC, format('Exec ExisteCuenta ''%s'', ''%s'', %d',
            [Trim(Patente.Text), PATENTE_CHILE, FCodigoConvenio])) = '0') then
    //No Existe
        Result := False
    else
        Result := True;
end;

//Inicio de Revision 5
procedure TFmeSCDatosVehiculo.BtnRecuperarClick(Sender: TObject);
begin
    FRecuperado := Not(FRecuperado);
    if Frecuperado then begin
        lblVehiculoRobado.Font.Color := clGreen;
        lblVehiculoRobado.Caption := CAPTION_VEHICULO_RECUPERADO;
        BtnRecuperar.Caption := BTN_RESTAURAR;
    end else begin
        lblVehiculoRobado.Font.Color := clRed;
        lblVehiculoRobado.Caption := CAPTION_VEHICULO_ROBADO;
        BtnRecuperar.Caption := BTN_RECUPERAR;
    end;
end;
//Fin de Revision 5


function TFmeSCDatosVehiculo.BuscarExistePatente(Patente : TEdit): boolean;
begin
    if (QueryGetValue(DMConnections.BaseCAC, format('Exec ExistePatente ''%s''',
            [Trim(Patente.Text)])) = '0') then
    //No Existe
        Result := False
    else
        Result := True;
end;

{******************************** Function Header ******************************
Function Name: BuscarExistePatenteDV
Author : vpaszkowicz
Date Created : 28/09/2007
Description : Lo uso para ver si existe una patente con DV distinto de Z.
Lo uso para ver si la patente que intento poner como diplom�tica no est�
usada como normal chilena.
Parameters : Patente : TEdit
Return Value : boolean
*******************************************************************************}
function TFmeSCDatosVehiculo.BuscarExistePatenteDV(Patente : TEdit; DVZ: Byte = 0): boolean;
begin
    if (QueryGetValue(DMConnections.BaseCAC, format('Exec ExistePatenteConDV %s,%d',
            [QuotedStr(Trim(Patente.Text)), DVZ])) = '0') then
    //No Existe
        Result := False
    else
        Result := True;
end;

//Revision 1
//Lgisuk
//04-10-2007
//Ahora no se valida que la patente RNVM sea chilena si tiene digito verificador Z
function TFmeSCDatosVehiculo.ValidarConvenio: boolean;
begin
    Result := False;

    //Valida datos entre la patente y su RNVM
    //if not ValidarDatosEntrePatentes(txtPatente, txtDigitoVerificador) then exit;
    if Trim(txt_PatenteRVM.Text) <> '' then
        if not ValidarDatosEntrePatentes(txt_PatenteRVM, txt_DigitoVerificadorRVM) then exit;
    //Valida que si es com�n tenga el formato chileno.
	if (txtDigitoVerificador.Text <> 'Z') and (Trim(txt_DigitoVerificadorRVM.Text) <> 'Z') and (not ValidarPatenteChilena(txtPatente)) then //Modificado en revision 1
    begin
        MsgBoxBalloon(MSG_ERROR_VALIDAR_FORMATO_PATENTE, Format(MSG_CAPTION_VALIDAR,[STR_PATENTE]), MB_ICONSTOP, txtPatente);
        txtPatente.SetFocus;
        txtPatente.SelStart := 1000;
        Exit;
    end;

    //Valida que si no es diplom�tica sea chilena.
    if (Trim(txt_PatenteRVM.Text) <> '') and (Trim(txt_DigitoVerificadorRVM.Text) <> 'Z') and (not ValidarPatenteChilena(txt_PatenteRVM)) then //Modificado en Revision 1
    begin
        MsgBoxBalloon(MSG_ERROR_VALIDAR_FORMATO_PATENTE, Format(MSG_CAPTION_VALIDAR,[STR_PATENTE]), MB_ICONSTOP, txt_PatenteRVM);
        txt_PatenteRVM.SetFocus;
        txt_PatenteRVM.SelStart := 1000;
        Exit;
	end;

    if (txt_PatenteRVM.Enabled) and (txt_PatenteRVM.Text <> '') and (not ValidarDigitoVerificador(txt_PatenteRVM, txt_DigitoVerificadorRVM)) then begin
        MsgBoxBalloon(MSG_ERROR_DIGITO_VERIFICADOR_PATENTE, CAPTION_VALIDAR_DATOS_VEHICULO, MB_ICONERROR,txt_DigitoVerificadorRVM);
        Exit;
    end;

    Result := True;
end;

function TFmeSCDatosVehiculo.ValidarSolicitud: boolean;
begin
    Result := BuscarPatenteSolicitud;
end;

function TFmeSCDatosVehiculo.GetCategoria: integer;
begin
    Result := ObtenerCategoriaVehiculo(DMConnections.BaseCAC,self.CodigoTipo);
end;

function TFmeSCDatosVehiculo.GetTag: Ansistring;
begin
    if Trim(txtNumeroTelevia.Text) <> '' then
        Result := StrRight('0000000000'+Trim(txtNumeroTelevia.Text),11)
    else
        Result := '';
end;

function TFmeSCDatosVehiculo.GetSerialNumber: DWord;
begin
    if Trim(txtNumeroTelevia.Text) <> '' then
        Result := EtiquetaToSerialNumber(PadL(Trim(txtNumeroTelevia.Text),11,'0'))
    else
        Result := 0;
end;

function TFmeSCDatosVehiculo.ValidarDigitoVerificadorTag(NumeroTag: String): Boolean;
begin
	NumeroTag := Trim(NumeroTag);
    Result := True;
    if Luhn(Copy(NumeroTag,0,length(NumeroTag)-1))<>StrRight(NumeroTag,1) then
        Result := False;
end;

procedure TFmeSCDatosVehiculo.txtNumeroTeleviaExit(Sender: TObject);
resourcestring
    MSG_ADVERTENCIA_TAG_EN_COMODATO		= 'El telev�a est� en COMODATO. No entregar';   //REV.11
begin
    if AceptandoCambios or CancelandoCambios then Exit;
    //Revision 4
    if txtNumeroTelevia.Text = EmptyStr then begin
        MsgBoxBalloon(MSG_ERROR_TAG_VACIO,MSG_CAPTION_ASIGNAR_TAG, MB_ICONWARNING,txtNumeroTelevia);
    end
    else if not(ValidarDigitoVerificadorTag(txtNumeroTelevia.Text)) then begin
    	MsgBoxBalloon(MSG_ERROR_VERIFICADOR_TAG,MSG_CAPTION_ASIGNAR_TAG, MB_ICONWARNING,txtNumeroTelevia);
    end
    else if (FCodigoUbicacionTag = CONST_ALMACEN_EN_COMODATO) and
    		(FEstado in [Solicitud, ConvenioAltaVehiculo, ConvenioCambiarVehiculo]) then begin   //REV.11
    	MsgBoxBalloon(MSG_ADVERTENCIA_TAG_EN_COMODATO, MSG_CAPTION_ASIGNAR_TAG, MB_ICONWARNING, txtNumeroTelevia);
    end;
    //Fin de Revision 4
end;

function TFmeSCDatosVehiculo.GetDatoCuenta: TCuentaVehiculo;
begin
    Result.Vehiculo.Patente:=Patente;
    Result.Vehiculo.CodigoVehiculo := CodigoVehiculo;
    Result.Vehiculo.DigitoPatente:=DigitoVerificador;
    Result.Vehiculo.CodigoMarca:=Marca;
    Result.Vehiculo.Marca:=DescripcionMarca;
    Result.Vehiculo.Modelo:=DescripcionModelo;
    Result.Vehiculo.CodigoTipo:=CodigoTipo;
	Result.Vehiculo.Tipo:=DescripcionTipo;
    Result.Vehiculo.Anio:=inttostr(Anio);
    //Revision 5
    if lblVehiculoRobado.Visible then begin
        Result.Vehiculo.robado := FRobado;
        Result.Vehiculo.recuperado := FRecuperado;
    end else begin
        Result.Vehiculo.robado := False;
        Result.Vehiculo.recuperado := False;
    end;
    //Fin de Revision 5
    Result.Vehiculo.CodigoTipoPatente := PATENTE_CHILE;
    Result.Vehiculo.PatenteRVM := PatenteRVM;
    Result.Vehiculo.DigitoPatenteRVM := DigitoVerificadorRVM;
    Result.Vehiculo.Foraneo := FForaneo;	// 20160603 MGO
    Result.IndiceVehiculo := FindiceVehiculo;
    Result.ContextMark:= ContextMark;
    Result.ContactSerialNumber:=EtiquetaToSerialNumber(NumeroTag);
    Result.TieneAcoplado:=False;
    Result.EstadoConservacionTAG:= 1;
    Result.TagEnClienteOtroConvenio := TagEnCliente(DMConnections.BaseCAC, FCodigoPersona, SerialNumber, FCodigoConvenio);
    Result.Suspendida := NullDate;
    Result.EstadoConservacionTAG := CodigoEstadoConservacion;
    Result.IndicadorListas := FIndicadorListas;
    Result.FechaBajaCuenta := NullDate; // SS_916_PDO_20120103
end;

procedure TFmeSCDatosVehiculo.SetDigitoVerificador(Value: AnsiString);
begin
    txtDigitoVerificador.Text := Trim(Value);
end;

procedure TFmeSCDatosVehiculo.txtNumeroTeleviaKeyPress(Sender: TObject;
  var Key: Char);
begin
    if not (Key  in ['0'..'9', #8]) then
        Key := #0;
end;

function TFmeSCDatosVehiculo.GetPatenteRVM: String;
begin
    Result := Trim(txt_PatenteRVM.Text);
end;

{******************************** Function Header ******************************
Function Name: ValidarDatosPatente
Author : vpaszkowicz
Date Created : 15/08/2008
Description : Para las patentes valido
    Si tiene DV Z que no exista en la base una chilena igual y viceversa.
Parameters : None
Return Value : Boolean


Revision :1
    Author : mpiazza
    Date : 22/04/2009
    Description : Quitando la validacion Si es patente con DV Z
*******************************************************************************}
function TFmeSCDatosVehiculo.ValidarDatosEntrePatentes(unaPatente: TEdit; unDigitoVerificador: TEdit): Boolean;
resourceString
    MSG_ERROR_PATENTE_EXISTE_COMO_NORMAL = 'La Patente ya existe en el Maestro de Veh�culos';
    MSG_ERROR_PATENTE_EXISTE_COMO_DIPLOMATICA = 'La Patente ya existe en el Maestro de Veh�culos como patente especial con D�gito Verificador Z';
begin
    Result := False;
    if UpperCase(unDigitoVerificador.Text) = 'Z' then begin
        if unaPatente.enabled and BuscarExistePatenteDV(unaPatente) then begin
            MsgBoxBalloon(MSG_ERROR_PATENTE_EXISTE_COMO_NORMAL, MSG_ERROR_PATENTE, MB_ICONSTOP, unDigitoVerificador);
            unDigitoVerificador.Text := EmptyStr;
            unDigitoVerificador.SetFocus;
            Exit;
        end;
    end;
    Result := True;
end;

{******************************** Function Header ******************************
Function Name: DigitosVerificadoresExit
Author :
Date Created :
Description :
Parameters : DigitoVerificador: TObject
Return Value : None
Revision :1
    Author : vpaszkowicz
    Date : 13/08/2008
    Description : Borro todas las validaciones que est�n dem�s, ya que se hacen al
    final por el btn Aceptar del form.
*******************************************************************************}
procedure TFmeSCDatosVehiculo.DigitosVerificadoresExit(DigitoVerificador: TObject);
Resourcestring
    MSG_ERROR_PATENTE_RVM = 'La patente especificada para la patente RNVM ya existe';
    //Rev. 13
    MSG_ERROR_PATENTE_INFRACTORA = 'La patente posee infracciones pendientes de regularizar';
    MSG_ERROR_CAPTION_PATENTE_INFRACTORA = 'Patente Infractora';
    //end Rev. 13
var
    Patente : TEdit;
begin
    if AceptandoCambios or CancelandoCambios then exit;
    if  (((DigitoVerificador = txt_DigitoVerificadorRVM) and (txt_PatenteRVM.Text = '')) or
        ((DigitoVerificador = txtDigitoVerificador) and (txtPatente.Text = ''))) then
    begin
		TEdit(DigitoVerificador).Text := '';
        Exit;
    end;

    if DigitoVerificador = txt_DigitoVerificadorRVM then
        Patente := txt_PatenteRVM
    else
        Patente := txtPatente;

    if not ValidarDigitoVerificador(Patente , TEdit(DigitoVerificador)) then begin
        MsgBoxBalloon(MSG_ERROR_DIGITO_VERIFICADOR, MSG_CAPTION_DIGITO_VERIFICADOR, MB_ICONWARNING, TEdit(DigitoVerificador));
        TEdit(DigitoVerificador).Text := EmptyStr;
        TEdit(DigitoVerificador).SetFocus;
        Exit;
    end;

   //Rev. 13
    if not ValidaPatenteInfraccionesPendientes(Patente) then  begin
       MsgBox(MSG_ERROR_PATENTE_INFRACTORA,MSG_ERROR_CAPTION_PATENTE_INFRACTORA,MB_ICONWARNING)
    end;
   //end Rev. 13

    AsignarValoresAControles(DigitoVerificador, Patente);
end;

function TFmeSCDatosVehiculo.GetDigitoVerificadorCorrecto: boolean;
begin
    Result := ValidarDigitoVerificador(txtPatente, txtDigitoVerificador);
end;

function TFmeSCDatosVehiculo.GetDigitoVerificadorCorrectoRVM: boolean;
begin
    Result := ValidarDigitoVerificador(txt_PatenteRVM, txt_DigitoVerificadorRVM);
end;

function TFmeSCDatosVehiculo.GetDigitoVerificadorRVM: AnsiString;
begin
    Result := Trim(txt_DigitoVerificadorRVM.Text);
end;

procedure TFmeSCDatosVehiculo.txtDigitoVerificadorChange(Sender: TObject);
begin
    txt_PatenteRVM.Enabled := UpperCase(txtDigitoVerificador.Text) = 'Z';
    if not txt_PatenteRVM.Enabled then begin
        txt_PatenteRVM.Clear;
        txt_DigitoVerificadorRVM.Clear;
    end;
    txt_DigitoVerificadorRVM.Enabled := txt_PatenteRVM.Enabled;
	lbl_PatenteRVM.Enabled := txt_PatenteRVM.Enabled;
end;

procedure TFmeSCDatosVehiculo.txtDescripcionModeloKeyPress(Sender: TObject;
  var Key: Char);
begin
    if key = '}' then Key := #0;
end;

//Revision 1
//lgisuk
//01-10-2007
//Ahora se permite ingresar patente con digito verificador �Z� en ambos campos.
procedure TFmeSCDatosVehiculo.DigitosVerificadoresKeyPress(Sender: TObject;
  var Key: Char);
begin
    if not ((Key  in ['0'..'9','k','K','z','Z', #8])) then
        Key := #0;
end;

function TFmeSCDatosVehiculo.BuscarPatenteSolicitud: boolean;
var
    Titulo, Descripcion, Comentario: string;
    SolicitudPatente: integer;
    FObs: TFormObservacionesGeneral;
begin
    Result := False;

    //La funcion devuelve 0 si no la encontr�, con lo cual esta bien!!!
    SolicitudPatente := StrtoInt(QueryGetValue(DMConnections.BaseCAC,
                            'select dbo.ExistePatenteSolicitud('''
                            + Trim(txtPatente.Text) + ''')'));
    if SolicitudPatente > 0 then begin
        if SolicitudPatente <> FCodigoSolicitud then begin //si estoy en modificacion, la va a encontrar
            ObtenerMensaje(DMConnections.BaseCAC, MENSAJE_PATENTE_EXISTENTE, Titulo, Descripcion, Comentario);
            if MsgBox(Descripcion, Titulo, MB_ICONWARNING or MB_YESNO) <> mrYes then exit;
            Application.CreateForm(TFormObservacionesGeneral, FObs);
            if (FObs.inicializa(FObservaciones)) and (FObs.ShowModal = mrOk) then begin
                FObservaciones := FObs.Observaciones;
                FObs.Release;
            end;
        end;
    end;
    Result := True;
end;

procedure DeshabilitarBotonDefault(BuscarEn: TWinControl);
//Pone en falso la prop. Defualt del primer boton que encuenta que tenga dicha prop. en True
var
  i: Integer;
  ControlAct: TControl;
begin
    for i := 0 to BuscarEn.ControlCount-1 do begin
        ControlAct := BuscarEn.Controls[i];
        if (ControlAct is TButton) then begin
            if TButton(ControlAct).Default then begin
                BtnConDefault := TButton(ControlAct);  //Lo guardo para despues activarlo de nuevo
                BtnConDefault.Default := False;
                Exit;
            end
        end
        else
            if not (ControlAct is TForm) and (ControlAct is TWinControl) then
                DeshabilitarBotonDefault(TWinControl(ControlAct));
    end;
end;

procedure HabilitarBotonDefault;
begin
	if (BtnConDefault <> Nil) then
        BtnConDefault.Default := True;
end;

{******************************** Function Header ******************************
Function Name: PatentesExit
Author :
Date Created :
Description :
Parameters : Sender: TObject
Return Value : None
Revision : 1
    Author : vpaszkowicz
    Date : 30/10/2007
    Description : Si el veh�culo est� robado siempre muestro el bot�n. Si no
    tiene permisos va a aparecer deshabilitado.
Revision : 2
    Author : lcanteros
    Date : 21/04/2008
    Description : la obtencion de datos de vehiculos y las validaciones
    ahora las hace al salir de textbox digito verificador
*******************************************************************************}
procedure TFmeSCDatosVehiculo.PatentesExit(Sender: TObject);
{var
    DatosVehiculo: TVehiculos;}
begin

    HabilitarBotonDefault;   //Seria como btnAceptar.Default := true;

    if CancelandoCambios then Exit;
    if Sender = txtPatente then begin
        if not ValidateControls([txtPatente],
            [txtPatente.Text <> EmptyStr],
            CAPTION_VALIDAR_DATOS_VEHICULO,
            [MSG_ERROR_VALIDAR_PATENTE]) then exit;
        if FCodigoVehiculoOriginal <> -1 then
            FCodigoVehiculo := FCodigoVehiculoOriginal;
    end;

    // Si existe un codigo de vehiculo original es por que estamos editando
      //Subo esta l�nea.
    //FCodigoVehiculo := FCodigoVehiculoOriginal;

    if (TEdit(Sender).Text = '') then begin
        if Sender = txt_PatenteRVM then
            txt_DigitoVerificadorRVM.Text := EmptyStr
        else begin
            MsgBoxBalloon(MSG_ERROR_VALIDAR_FORMATO_PATENTE, Format(MSG_CAPTION_VALIDAR,[STR_PATENTE]), MB_ICONWARNING, txtPatente);
            Exit;
        end;
	end;
    if (Sender = txtPatente) and txtDigitoVerificador.Visible then txtDigitoVerificador.SetFocus;     //REV.14
end;

procedure TFmeSCDatosVehiculo.txtPatenteChange(Sender: TObject);
begin
    Frecuperado := False;
    if txtDigitoVerificador.Text <> EmptyStr then txtDigitoVerificador.Text := EmptyStr;
end;

procedure TFmeSCDatosVehiculo.txtPatenteEnter(Sender: TObject);
begin
    DeshabilitarBotonDefault(Screen.ActiveForm); //Seria como btnAceptar.Default := false;
end;

procedure TFmeSCDatosVehiculo.txtNumeroTeleviaChange(Sender: TObject);
begin
    FCambioElTag				:= True;
    FCodigoUbicacionTag			:= 0;				//REV.11
    ledVerde.Enabled 			:= False;
    ledAmarilla.Enabled 		:= False;
    ledGris.Enabled 			:= False;
    ledNegra.Enabled 			:= False;

    if GBListas.Visible and (length(Trim(txtNumeroTelevia.Text)) > 7) and (Trim(txtNumeroTelevia.Text) <> '') and (EtiquetaToSerialNumber(PadL(Trim(txtNumeroTelevia.Text),11,'0')) <> 0) then begin
        with spLeerDatosTAG, spLeerDatosTAG.Parameters do begin
            ParamByName ('@ContractSerialNumber').Value := SerialNumber;
        end;
        spLeerDatosTAG.Open;
        if spLeerDatosTAG.RecordCount = 1 then begin
			ledVerde.Enabled 			:= spLeerDatosTAG.FieldByName ('IndicadorListaVerde').AsBoolean;
            ledAmarilla.Enabled 		:= spLeerDatosTAG.FieldByName ('IndicadorListaAmarilla').AsBoolean;
            ledGris.Enabled 			:= spLeerDatosTAG.FieldByName ('IndicadorListaGris').AsBoolean;
            ledNegra.Enabled 			:= spLeerDatosTAG.FieldByName ('IndicadorListaNegra').AsBoolean;
            FCodigoEstadoConservacion   := spLeerDatosTAG.FieldByName ('CodigoEstadoConservacion').AsInteger;
            FIndicadorListas.Verde      := spLeerDatosTAG.FieldByName ('IndicadorListaVerde').AsBoolean;
            FIndicadorListas.Negra      := spLeerDatosTAG.FieldByName ('IndicadorListaNegra').AsBoolean;
            FIndicadorListas.Gris       := spLeerDatosTAG.FieldByName ('IndicadorListaGris').AsBoolean;
            FIndicadorListas.Amarilla   := spLeerDatosTAG.FieldByName ('IndicadorListaAmarilla').AsBoolean;
            //REV.11
            FCodigoUbicacionTag			:= spLeerDatosTAG.FieldByName ('CodigoUbicacionTag').AsInteger;
        end;
        spLeerDatosTAG.Close;
    end;
end;

function TFmeSCDatosVehiculo.CantidadTeleviasNuevos(
  Categoria: integer): integer;
begin
    result := 0;
    FcdsVehiculos.First;
    while not FcdsVehiculos.Eof do begin
        if (ObtenerCategoriaVehiculo(DMConnections.BaseCAC, FcdsVehiculos.FieldByName('CodigoTipoVehiculo').AsInteger) = Categoria)
            and (FcdsVehiculos.FieldByName('TeleviaNuevo').AsBoolean)
			and (not FcdsVehiculos.FieldByName('ContractSerialNumber').IsNull) then
            result := result + 1;
        FcdsVehiculos.Next;
    end;
end;

function TFmeSCDatosVehiculo.GetContextMark: integer;
begin
    if FCambioElTag or (FContextMark < 1) then FContextMark := ObtenerContextMarkTAG(DMConnections.BaseCAC, SerialNumber);
    result := FContextMark;
end;

function TFmeSCDatosVehiculo.GetFechaCreacion: TDateTime;
begin
    Result := iif(FFechaCreacion = NullDate, NowBase(DMConnections.BaseCAC), FFechaCreacion);
end;
{-----------------------------------------------------------------------------
  Procedure: AsignarValoresAControles
  Author:    lcanteros
  Date:      21-Abr-2008
  Arguments: Sender: TObject
  Result:    boolean
  Purpose:
-----------------------------------------------------------------------------}
function TFmeSCDatosVehiculo.AsignarValoresAControles(Sender: TObject; AuxTxtPatente: TEdit): boolean;
var
    DatosVehiculo: TVehiculos;
begin
    if Sender = txtDigitoVerificador then begin
        if TCuentasConvenio.ObtenerDatosVehiculo(Sender = txt_DigitoVerificadorRVM , TipoPatente, AuxTxtPatente.Text, DatosVehiculo) then begin
            txt_PatenteRVM.Text := DatosVehiculo.PatenteRVM;
            txt_DigitoVerificadorRVM.Text := DatosVehiculo.DigitoPatenteRVM;
            FCodigoVehiculo := DatosVehiculo.CodigoVehiculo;
            SetTipoPatente(DatosVehiculo.Tipo);
            SetCodigoMarca(DatosVehiculo.CodigoMarca);
            SetDescripcionModelo(DatosVehiculo.Modelo);
            SetAnio(strtoint(DatosVehiculo.Anio));
            SetCodigoTipo(DatosVehiculo.CodigoTipo);
            lblVehiculoRobado.Visible := DatosVehiculo.Robado;
            BtnRecuperar.Visible := DatosVehiculo.Robado; // Revision 5
            BtnRecuperar.Enabled := False;
            //La agrego esto ac� para que siempre setee si es o no robado, aunque no tenga permisos
            FRobado := DatosVehiculo.Robado;
            if ExisteAcceso('Boton_Recuperar_Vehiculo_Robado') then begin  // Revision 5
                BtnRecuperar.Enabled := DatosVehiculo.Robado; // Revision 5
                FRobado := DatosVehiculo.Robado;       //Revision 5
            end else begin
                BtnRecuperar.Enabled := False; // Revision 5
            end;
        end else begin
            lblVehiculoRobado.Visible := False;
            BtnRecuperar.Enabled := False; // Revision 5
            BtnRecuperar.Visible := False; // Revision 5
            FRobado := False;       //Revision 5
        end;
        //Inicio de Revision 5
        if Frecuperado then begin
            lblVehiculoRobado.Font.Color := clGreen;
            lblVehiculoRobado.Caption := CAPTION_VEHICULO_RECUPERADO;
            BtnRecuperar.Caption := BTN_RESTAURAR;
        end else begin
            lblVehiculoRobado.Font.Color := clRed;
            lblVehiculoRobado.Caption := CAPTION_VEHICULO_ROBADO;
            BtnRecuperar.Caption := BTN_RECUPERAR;
        end;
        //Fin de Revision 5
    end;

    if ((UpperCase(txtDigitoVerificador.Text) <> 'Z') and (Sender = txtPatente) and (not ValidarPatenteChilena(txtPatente))) or
       ((UpperCase(txtDigitoVerificador.Text) = 'Z') and (Sender = txt_PatenteRVM) and (not ValidarPatenteChilena(txt_PatenteRVM))) then
		MsgBoxBalloon(MSG_ERROR_VALIDAR_FORMATO_PATENTE, Format(MSG_CAPTION_VALIDAR,[STR_PATENTE]), MB_ICONWARNING, TEdit(Sender));
end;

{-----------------------------------------------------------------------------
  Procedure: ComprobarCodigoVehiculo
  Author:    vpaszkowicz
  Date:      27-Ago-2009
  Arguments: None
  Result:    Boolean
  Description: Se usa para corroborar al aceptar la ventana, que el c�digo
  veh�culo no se haya refrescado.
  Esto es, si se libera una patente, el c�digo de veh�culo cambiar� a -1
-----------------------------------------------------------------------------}
procedure TFmeSCDatosVehiculo.ComprobarCodigoVehiculo;
var
    Vehiculo: TVehiculos;
begin
    if not TCuentasConvenio.ObtenerDatosVehiculo(False, 'CHL',
        Trim(txtPatente.Text), Vehiculo) and (FCodigoVehiculo <> -1) then
        FCodigoVehiculo := -1;
end;

//Rev. 13
{-----------------------------------------------------------------------------
  Function: ValidaPatenteInfraccionesPendientes
  Author:    jjofre
  Date:      25-03-2010
  Description: Esta funcion retorna true si la patente a dar de alta no posee
                infracciones pendientes de pago.
-----------------------------------------------------------------------------}
function TFmeSCDatosVehiculo.ValidaPatenteInfraccionesPendientes(Patente: TEdit): Boolean;
var
spObtenerInfracciones:TADOStoredProc;

begin
    Result:=True;
    spObtenerInfracciones := TADOStoredProc.Create(nil);
    try
        spObtenerInfracciones.Connection:= DMConnections.BaseCAC;
        spObtenerInfracciones.ProcedureName:='ObtenerInfraccionesAFacturar';

        spObtenerInfracciones.Parameters.Refresh;
        with spObtenerInfracciones, Parameters do begin
            ParamByName('@Patente').Value := Patente.Text;
            ParamByName('@FechaInicial').Value := Null;
            ParamByName('@FechaFinal').Value := Null;
            ExecProc;

            if not ParamByName('@SePuedeEmitirCertificado').Value then begin
                Result:=False;
            end;
        end;
    finally
        spObtenerInfracciones.Close;
        spObtenerInfracciones.Free;
    end;
end;
//end Rev. 13
end.

