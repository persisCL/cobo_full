{-----------------------------------------------------------------------------
 Unit Name: FormConfirmarBajaModifConvenio.pas
 Author:    mtraversa
 Date Created: 23/03/2005
 Language: ES-AR
 Description: Formulario que se usa para la confirmacion de las bajas y de las
              modificaciones realizadas sobre convenios
 Revision : 1
 Author : nefernandez
 Date : 06/11/2007
 Description : SS 622: Se quita el bot�n "Reimpresi�n" pues no se usa, y se
 quita el boton de cerrar del formulario
-----------------------------------------------------------------------------}


unit FormConfirmarBajaModifConvenio;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TfrmConfirmarBajaModifConvenio = class(TForm)
    btnConfirmar: TButton;
    btnCancelar: TButton;
    txtEstado: TLabel;
  private
    { Private declarations }
  public
    function Inicializar(Caption: TCaption; ConfButtonCaption: TCaption): boolean;
  end;

var
  frmConfirmarBajaModifConvenio: TfrmConfirmarBajaModifConvenio;

implementation

{$R *.dfm}

function TfrmConfirmarBajaModifConvenio.Inicializar(Caption: TCaption; ConfButtonCaption: TCaption): boolean;
begin
    Self.Caption := Caption;
    btnConfirmar.Caption := ConfButtonCaption;
    Result := True;
end;

end.
