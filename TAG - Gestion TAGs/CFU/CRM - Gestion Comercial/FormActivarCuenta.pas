unit FormActivarCuenta;

{*******************************************************************************
Revision : 1
Author   : Fsandi
Date     : 28/11/2006
Description :   Se agrego una asignacio de valor NULLDATE para que cuando se
                Activa una cuenta no se muestre una fecha de baja 30/12/1899

Revision : 2
Author   : Fsandi
Date     : 05-07-2007
Description : Se agrega que al momento de activar un vehiculo, su cuenta adquiera la
            fecha vencimiento de Tag del vehiculo suspendido. (estaba quedando en NULL)
Revision : 3
    Author : vpaszkowicz
    Date : 30/10/2007
    Description : Comento los seteos del checkRobado ya que deje invisible a
    la componente por considerarla perteneciente a una funcionalidad obsoleta.
    Comento el seteo del Robado, ahora se setea en el registro y desde ese mis-
    mo pasa al form que lo necesite.

    Revision :4
        Author : vpaszkowicz
        Date : 01/09/2009
        Description : Se invoca a la funci�n ComprobarCodigoVehiculo para
        corregir el c�digo de veh�culo si la patente fue liberada.
*******************************************************************************}
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, FrameSCDatosVehiculo, Convenios, PeaProcs, PeaTypes, Util,
  frmSeleccionarMovimientosTelevia, DmiCtrls, ListBoxEx, DBListEx, Menus, DB,
  DBClient, DMCOnnection,UtilProc,UtilDB;

type
  TfrmActivarCuenta = class(TForm)
    freDatosVehiculo: TFmeSCDatosVehiculo;
    Panel1: TPanel;
    btn_Aceptar: TButton;
    btn_Cancelar: TButton;
    cb_VehiculoRobado: TCheckBox;
    gbModalidadEntregaTelevia: TGroupBox;
    Label1: TLabel;
    lblModalidadEntregaTelevia: TLabel;
    gbIndemnizaciones: TGroupBox;
    Bevel1: TBevel;
    lblSeleccionar: TLabel;
    Label10: TLabel;
    lblTotalMovimientosTxt: TLabel;
    lblTotalMovimientos: TLabel;
    dblConceptos: TDBListEx;
    rgGarantiaTag: TRadioGroup;
    cdConceptos: TClientDataSet;
    dsConceptos: TDataSource;
    mnuGrillaConceptos: TPopupMenu;
    mnuEliminarConcepto: TMenuItem;
    AgregarConceptos1: TMenuItem;
    GroupBox: TGroupBox;
    rgModalidadEntregaTag: TRadioGroup;
    procedure btn_CancelarClick(Sender: TObject);
    procedure btn_AceptarClick(Sender: TObject);
    procedure lblSeleccionarClick(Sender: TObject);
    procedure dblConceptosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure mnuEliminarConceptoClick(Sender: TObject);
    procedure AgregarConceptos1Click(Sender: TObject);
    procedure dblConceptosContextPopup(Sender: TObject; MousePos: TPoint;
      var Handled: Boolean);
    procedure frmVehiculotxtPatenteKeyPress(Sender: TObject; var Key: Char);
    procedure frmVehiculotxtPatenteChange(Sender: TObject);
    procedure frmVehiculotxtPatenteEnter(Sender: TObject);
    procedure frmVehiculotxtPatenteExit(Sender: TObject);
    procedure frmVehiculotxtDigitoVerificadorKeyPress(Sender: TObject;
      var Key: Char);
    procedure frmVehiculotxtDigitoVerificadorChange(Sender: TObject);
    procedure frmVehiculotxtDigitoVerificadorExit(Sender: TObject);
    procedure frmVehiculotxt_PatenteRVMKeyPress(Sender: TObject; var Key: Char);
    procedure frmVehiculotxt_PatenteRVMEnter(Sender: TObject);
    procedure frmVehiculotxt_PatenteRVMExit(Sender: TObject);
    procedure frmVehiculotxt_DigitoVerificadorRVMKeyPress(Sender: TObject;
      var Key: Char);
    procedure frmVehiculotxt_DigitoVerificadorRVMExit(Sender: TObject);
    procedure frmVehiculotxtDescripcionModeloKeyPress(Sender: TObject;
      var Key: Char);
    procedure frmVehiculotxt_anioExit(Sender: TObject);
    procedure frmVehiculotxtNumeroTeleviaKeyPress(Sender: TObject;
      var Key: Char);
    procedure frmVehiculotxtNumeroTeleviaChange(Sender: TObject);
    procedure frmVehiculotxtNumeroTeleviaExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure LimpiarDataSet(Sender: TObject);
  private
    { Private declarations }
    FCodigoAlmacenDestino: integer;
    FFechaVencimientoTag: TDateTime; //Revision 2
    // Rev.4 / 02-07-2009 / Nelson Droguett Sierra -----------------------------------
    FImporteTotalMovimientos: Int64;
    FListaVouchers: TStringList ;
    FPuntoEntrega: Integer;
    FCodigoVehiculo: Integer;
    FCambioFecha: Boolean;
    // -------------------------------------------------------------------------------
    function GetVehiculo: TVehiculosConvenio;

    function GetAnio: integer;
    function GetCategoria: integer;
    function GetCodigoColor: integer;
    function GetCodigoMarca: integer;
    function GetCodigoTipo: integer;
    function GetDatoCuenta: TCuentaVehiculo;
    function GetDescripcionColor: AnsiString;
    function GetDescripcionMarca: AnsiString;
    function GetDescripcionModelo: AnsiString;
    function GetDescripcionTipo: AnsiString;
    function GetDetalleVehiculoCompleta: Ansistring;
    function GetDetalleVehiculoSimple: Ansistring;
    function GetDigitoVerificador: AnsiString;
    function GetDigitoVerificadorCorrecto: boolean;
    function GetDigitoVerificadorCorrectoRVM: boolean;
    function GetDigitoVerificadorRVM: AnsiString;
    function GetPatente: AnsiString;
    function GetPatenteRVM: String;
    function GetSerialNumber: DWORD;
    function GetTag: AnsiString;
    function GetTipoPatente: AnsiString;
    function GetContextMark: integer;
    function GetObservaciones: Ansistring;
    function GetAlmacenDestino: integer;
    function GetTipoAsignacionTag: string;

  public
    Property TipoPatente: AnsiString read GetTipoPatente;
    Property Patente: AnsiString read GetPatente;
    property Marca: integer read GetCodigoMarca;
    property DescripcionMarca: AnsiString read GetDescripcionMarca;
    property CodigoTipo: integer read GetCodigoTipo;
    property DescripcionTipo: AnsiString read GetDescripcionTipo;
    property DescripcionModelo: AnsiString read GetDescripcionModelo;
    property Anio: integer read GetAnio;
    property Color: integer read GetCodigoColor;
    property DescripcionColor: AnsiString read GetDescripcionColor;
    property CodigoCategoria: integer read GetCategoria;
    property DigitoVerificadorCorrecto: boolean read GetDigitoVerificadorCorrecto;
    property DigitoVerificadorCorrectoRVM: boolean read GetDigitoVerificadorCorrectoRVM;
    property DigitoVerificador: AnsiString read GetDigitoVerificador;
    property DetalleVehiculoSimple: Ansistring read GetDetalleVehiculoSimple;
    property DetalleVehiculoCompleto: Ansistring read GetDetalleVehiculoCompleta;
    property Observaciones: Ansistring read GetObservaciones;
    property SerialNumber:DWORD read GetSerialNumber;
    property CodigoVehiculo:Integer read FCodigoVehiculo;
    property ContextMark: integer read GetContextMark;
    property NumeroTag:AnsiString read GetTag;
    property DatoCuenta:TCuentaVehiculo read GetDatoCuenta;
    property PatenteRVM  : String read GetPatenteRVM;
    property DigitoVerificadorRVM: AnsiString read GetDigitoVerificadorRVM;
    property PuntoEntrega: Integer read FPuntoEntrega;
    property AlmacenDestino: Integer read GetAlmacenDestino;
    property TipoAsignacionTag: string read GetTipoAsignacionTag;
    property CambioFecha: Boolean read FCambioFecha;

    { Public declarations }

    function ValidarExistenciaVoucher : boolean;

    function Inicializar(	Caption: String; AVehiculo: TVehiculosConvenio;
    						PuntoEntrega, CodigoConvenio, CodigoPersona: Integer;
                        	ListaPatente, ListaTags : TStringList;
                            ListaVouchers : TStringList = nil;
                        	cdsVehiculos: TDataSet = nil): Boolean;

    property Vehiculo: TVehiculosConvenio read GetVehiculo;
  end;

var
  frmActivarCuenta: TfrmActivarCuenta;

implementation

{$R *.dfm}

procedure TfrmActivarCuenta.btn_CancelarClick(Sender: TObject);
begin
    close;
end;

procedure TfrmActivarCuenta.dblConceptosContextPopup(Sender: TObject;
  MousePos: TPoint; var Handled: Boolean);
begin
    mnuEliminarConcepto.Enabled := not cdConceptos.IsEmpty;
end;

procedure TfrmActivarCuenta.dblConceptosKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if (Key = VK_DELETE) or (Key = VK_BACK) then mnuEliminarConceptoClick(Sender);
end;

procedure TfrmActivarCuenta.FormShow(Sender: TObject);
begin
   FreDatosVehiculo.ActualizarControlActivo;  //Para que ponga el foco en el contro correcto
end;

procedure TfrmActivarCuenta.frmVehiculotxtDescripcionModeloKeyPress(
  Sender: TObject; var Key: Char);
begin
  freDatosVehiculo.txtDescripcionModeloKeyPress(Sender, Key);

end;

procedure TfrmActivarCuenta.frmVehiculotxtDigitoVerificadorChange(
  Sender: TObject);
begin
  freDatosVehiculo.txtDigitoVerificadorChange(Sender);

end;

procedure TfrmActivarCuenta.frmVehiculotxtDigitoVerificadorExit(
  Sender: TObject);
begin
  freDatosVehiculo.DigitosVerificadoresExit(Sender);

end;

procedure TfrmActivarCuenta.frmVehiculotxtDigitoVerificadorKeyPress(
  Sender: TObject; var Key: Char);
begin
  freDatosVehiculo.DigitosVerificadoresKeyPress(Sender, Key);

end;

procedure TfrmActivarCuenta.frmVehiculotxtNumeroTeleviaChange(Sender: TObject);
begin
  freDatosVehiculo.txtNumeroTeleviaChange(Sender);

end;

procedure TfrmActivarCuenta.frmVehiculotxtNumeroTeleviaExit(Sender: TObject);
begin
  freDatosVehiculo.txtNumeroTeleviaExit(Sender);

end;

procedure TfrmActivarCuenta.frmVehiculotxtNumeroTeleviaKeyPress(Sender: TObject;
  var Key: Char);
begin
  freDatosVehiculo.txtNumeroTeleviaKeyPress(Sender, Key);

end;

procedure TfrmActivarCuenta.frmVehiculotxtPatenteChange(Sender: TObject);
begin
  freDatosVehiculo.txtPatenteChange(Sender);

end;

procedure TfrmActivarCuenta.frmVehiculotxtPatenteEnter(Sender: TObject);
begin
  freDatosVehiculo.txtPatenteEnter(Sender);

end;

procedure TfrmActivarCuenta.frmVehiculotxtPatenteExit(Sender: TObject);
begin
  freDatosVehiculo.PatentesExit(Sender);

end;

procedure TfrmActivarCuenta.frmVehiculotxtPatenteKeyPress(Sender: TObject;
  var Key: Char);
begin
  freDatosVehiculo.PatentesKeyPress(Sender, Key);

end;

procedure TfrmActivarCuenta.frmVehiculotxt_anioExit(Sender: TObject);
begin
  freDatosVehiculo.txt_anioExit(Sender);

end;

procedure TfrmActivarCuenta.frmVehiculotxt_DigitoVerificadorRVMExit(
  Sender: TObject);
begin
  freDatosVehiculo.DigitosVerificadoresExit(Sender);

end;

procedure TfrmActivarCuenta.frmVehiculotxt_DigitoVerificadorRVMKeyPress(
  Sender: TObject; var Key: Char);
begin
  freDatosVehiculo.DigitosVerificadoresKeyPress(Sender, Key);

end;

procedure TfrmActivarCuenta.frmVehiculotxt_PatenteRVMEnter(Sender: TObject);
begin
  freDatosVehiculo.txtPatenteEnter(Sender);

end;

procedure TfrmActivarCuenta.frmVehiculotxt_PatenteRVMExit(Sender: TObject);
begin
  freDatosVehiculo.PatentesExit(Sender);

end;

procedure TfrmActivarCuenta.frmVehiculotxt_PatenteRVMKeyPress(Sender: TObject;
  var Key: Char);
begin
  freDatosVehiculo.PatentesKeyPress(Sender, Key);

end;

procedure TfrmActivarCuenta.AgregarConceptos1Click(Sender: TObject);
begin
    lblSeleccionarClick(Sender);
end;

{******************************** Function Header ******************************
Function Name: btn_AceptarClick
Author :
Date Created :
Description :
Parameters :
Return Value : Boolean
Revision : 1
    Author : vpaszkowicz
    Date : 01/09/2009
    Description : Agrego la funci�n ComprobarCodigoVehiculo, que permite
    validar en caso de suspensi�n.
*******************************************************************************}
procedure TfrmActivarCuenta.btn_AceptarClick(Sender: TObject);
//begin
//    if not frmVehiculo.ValidarDatos(FCodigoAlmacenDestino) then exit;
//    ModalResult := mrOK;
//end;
resourcestring
    MSG_ERROR_MISMA_PATENTE = 'La patente del nuevo veh�culo no puede ser la misma que la del veh�culo original';
    MSG_DEBE_INGRESAR_UN_MOTIVO_INDEMNIZACION = 'Se debe ingresar al menos un motivo de indemnizaci�n';
    MSG_TRANSFERENCIA_NO_POSIBLE = 'El telev�a es nuevo, no se puede hacer transferencia';
    MSG_TRANSFERENCIA_DIFIERE_ALMACEN = 'La modalidad seleccionada de entrega difiere de la �ltima utilizada en este telev�a';
    MSG_VOUCHER_YA_UTILIZADO = 'El n�mero de voucher ya ha sido utilizado';
    MSG_FECHA_ALTA_ERROR = 'La fecha indicada como fecha de alta, no es v�lida para este telev�a y esta patente';
var
    UltimoAlmacenTag: integer;
begin
    if not FreDatosVehiculo.ValidarDatos(FCodigoAlmacenDestino) then exit;
	FreDatosVehiculo.ComprobarCodigoVehiculo;
    ModalResult := mrOK;
    if (ModalResult = mrOk) then begin
       if gbIndemnizaciones.Visible then begin
        	//REV.4 mbecerra
            //los Conceptos no son obligatorios
            {if cdConceptos.IsEmpty then begin
                MsgBoxBalloon(MSG_DEBE_INGRESAR_UN_MOTIVO_INDEMNIZACION,'Error',MB_ICONSTOP, dblConceptos);
                dblConceptos.SetFocus;
                ModalResult := mrNone;
                Exit;
            end;
            }

            UltimoAlmacenTag := QueryGetValueInt(DMConnections.BaseCAC, format('select dbo.ObtenerUltimaUbicacionTAG(%d,%d)',[FreDatosVehiculo.ContextMark , FreDatosVehiculo.SerialNumber]));
            if (UltimoAlmacenTag = 0) and ( rgGarantiaTag.ItemIndex = 1 )  then begin
                MsgBoxBalloon(MSG_TRANSFERENCIA_NO_POSIBLE,'Error',MB_ICONSTOP, rgGarantiaTag);
                dblConceptos.SetFocus;
                ModalResult := mrNone;
                Exit;
            end;
            if (UltimoAlmacenTag <> 0) and ( rgGarantiaTag.ItemIndex = 1 ) then begin
                if UltimoAlmacenTag <> AlmacenDestino  then begin
                    MsgBoxBalloon(MSG_TRANSFERENCIA_DIFIERE_ALMACEN,'Error',MB_ICONSTOP, rgModalidadEntregaTag);
                    dblConceptos.SetFocus;
                    ModalResult := mrNone;
                    Exit;
                end;
            end;
        end;
    end;

end;


{******************************** Function Header ******************************
Function Name: Inicializar
Author :
Date Created :
Description :
Parameters : Caption: String; AVehiculo: TVehiculosConvenio; PuntoEntrega: Integer; CodigoConvenio: Integer; CodigoPersona: Integer; ListaPatente: TStringList; ListaTags: TStringList
Return Value : Boolean
Revision : 1
    Author : vpaszkowicz
    Date : 30/10/2007
    Description : Le comento el visible del check del robado, ya que si el vehi-
    culo llega como robado en la cuenta, no me importa porque le voy a dar de
    alta nuevamente.
    Le pongo Visible en FALSE directamente a la componente.
*******************************************************************************}
function TfrmActivarCuenta.Inicializar(
  Caption: String;
  AVehiculo: TVehiculosConvenio;
  PuntoEntrega: Integer;
  CodigoConvenio: Integer;
  CodigoPersona: Integer;
  ListaPatente: TStringList;
  ListaTags: TStringList;
  ListaVouchers: TStringList = nil;
  cdsVehiculos: TDataSet = nil  ): Boolean;
begin
    try
        FListaVouchers := ListaVouchers;
        Self.Caption := Caption;
        FCodigoAlmacenDestino := AVehiculo.Cuenta.CodigoAlmacenDestino ;
        FFechaVencimientoTag :=  AVehiculo.Cuenta.FechaVencimientoTag;  //Revision 2
        //cb_VehiculoRobado.Checked := AVehiculo.Cuenta.Vehiculo.Robado;
        //cb_VehiculoRobado.Visible := AVehiculo.Cuenta.Vehiculo.Robado;
            if Assigned(cdsVehiculos) then begin
              if cdsVehiculos.FieldByName('Almacen').Value = CONST_STR_ALMACEN_COMODATO then rgModalidadEntregaTAG.ItemIndex := 0;
              if cdsVehiculos.FieldByName('Almacen').Value = CONST_STR_ALMACEN_ARRIENDO then rgModalidadEntregaTAG.ItemIndex := 1;
              if cdsVehiculos.FieldByName('Almacen').Value = CONST_STR_ALMACEN_ENTREGADO_EN_CUOTAS then rgModalidadEntregaTAG.ItemIndex := 2;
            end;


        Result := freDatosVehiculo.Inicializar(PATENTE_CHILE,
                                 '',
                                 '',
                                 '',
                                 1,
                                 -1,
                                 1,
                                 -1,
                                 '',
                                 '',
                                 PuntoEntrega,
                                 -1,
                                 '',
                                 -1,
                                 ListaPatente,
                                 True,
                                 ListaTags,
                                 AVehiculo.Cuenta.ContextMark,
                                 SerialNumberToEtiqueta(AVehiculo.Cuenta.ContactSerialNumber),
                                 False,
                                 False,
                                 nil,
                                 CodigoConvenio,
                                 False,
                                 CodigoPersona,
                                 ConvenioActivarCuenta,
                                 True,
                                 NullDate,
                                 AVehiculo.Cuenta.IndiceVehiculo);
    except
        Result := False;
    end;

end;

// Rev. 4 / 02-07-2009 / Nelson Droguett
procedure TfrmActivarCuenta.lblSeleccionarClick(Sender: TObject);
var
    f: TfrmSeleccionarMovimientosTeleviaFORM;
    ParametroTipoOperacion: string;
    ParametroCodigoAlmacen: integer;
    ParametroOperacionTAG: integer;
begin

	case rgModalidadEntregaTag.ItemIndex of
    	0 : ParametroCodigoAlmacen := CONST_ALMACEN_EN_COMODATO;
        1 : ParametroCodigoAlmacen := CONST_ALMACEN_ARRIENDO;
        2 : ParametroCodigoAlmacen := CONST_ALMACEN_ENTREGADO_EN_CUOTAS;
    end;

    if rgGarantiaTag.ItemIndex = 0 then ParametroTipoOperacion := CONST_OP_TRANSFERENCIA_TAG_NUEVO
    else ParametroTipoOperacion := CONST_OP_TRANSFERENCIA_TAG_TRANSFERENCIA;

    //OperacionTag va fija
    ParametroOperacionTAG := CONST_OPERACION_TAG_REACTIVAR;


    Application.CreateForm(TfrmSeleccionarMovimientosTeleviaFORM, f);
    if f.Inicializar(ParametroOperacionTAG, ParametroCodigoAlmacen, ParametroTipoOperacion ) and (f.ShowModal = mrOk) then begin
      // Cargamos el dataset con lo que se selecciono
        f.ListaConceptos.First;
        while not f.ListaConceptos.Eof do begin
        	if f.ListaConceptos.FieldByName('Seleccionado').AsBoolean then begin
              //Agregamos el concepto a la lista
                cdConceptos.AppendRecord([
                    f.ListaConceptos.FieldByName('Seleccionado'     ).Value,
                    f.ListaConceptos.FieldByName('Codigoconcepto'   ).Value,
                    f.ListaConceptos.FieldByName('DescripcionMotivo').Value,
                    f.ListaConceptos.FieldByName('PrecioOrigen'     ).Value,
                    f.ListaConceptos.FieldByName('Moneda'           ).Value,
                    f.ListaConceptos.FieldByName('Cotizacion'       ).Value,
                    f.ListaConceptos.FieldByName('PrecioEnPesos'    ).Value,
                    f.ListaConceptos.FieldByName('Comentario'       ).Value,
                    //Revision 5
                    f.ListaConceptos.FieldByName('IDMotivoMovCuentaTelevia').Value ]);
                FImporteTotalMovimientos := FImporteTotalMovimientos + f.ListaConceptos.FieldByName('PrecioEnPesos').AsVariant;
          	end;

          	f.ListaConceptos.Next;
      	end;
    end;

    f.Release;
    lblTotalMovimientos.Caption := FormatearImporte( DMConnections.BaseCAC , FImporteTotalMovimientos);
end;

procedure TfrmActivarCuenta.mnuEliminarConceptoClick(Sender: TObject);
begin
    if not cdConceptos.eof then begin
        FImporteTotalMovimientos := FImporteTotalMovimientos - cdConceptos.FieldByName('PrecioEnPesos').AsVariant ;
        lblTotalMovimientos.Caption := FormatearImporte( DMConnections.BaseCAC , FImporteTotalMovimientos);
        cdConceptos.Delete;
    end;
end;

procedure TfrmActivarCuenta.LimpiarDataSet(Sender: TObject);
begin
	cdConceptos.EmptyDataSet;
end;

function TfrmActivarCuenta.ValidarExistenciaVoucher: boolean;
resourcestring
    MSG_CONCEPTO_DEBE    = 'Falta el concepto "Promoci�n la Tercera"';
    MSG_CONCEPTO_NO_DEBE = 'Para incluir el concepto "Promoci�n la Tercera" debe ingresar un n�mero de voucher';

const
    CONST_CONCEPTO_LA_TERCERA = 152;

var
    ConceptoEncontrado : boolean;
    Book : TBookmark;
begin
	Result := False;
    ConceptoEncontrado := False;
    cdConceptos.DisableControls;
    Book := cdConceptos.GetBookmark;
    cdConceptos.First;
    while not (cdConceptos.Eof or ConceptoEncontrado) do begin
    	if not cdConceptos.FieldByName('CodigoConcepto').IsNull then begin
    		ConceptoEncontrado := (cdConceptos.FieldByName('CodigoConcepto').AsInteger = CONST_CONCEPTO_LA_TERCERA);
        end;

        cdConceptos.Next;
    end;


    if ConceptoEncontrado then
        MsgBox(MSG_CONCEPTO_NO_DEBE, Caption, MB_ICONEXCLAMATION)
    else
        Result := True;

    cdConceptos.GotoBookmark(Book);
    cdConceptos.EnableControls;
end;

{******************************** Function Header ******************************
Function Name: GetVehiculo
Author :
Date Created :
Description :
Parameters : None
Return Value : TVehiculosConvenio
Revision :
    Author : vpaszkowicz
    Date : 30/10/2007
    Description : Comento la parte de robado para que tome el valor de los ob-
    jetos.
*******************************************************************************}
function TfrmActivarCuenta.GetVehiculo: TVehiculosConvenio;
begin
    Result.Cuenta := freDatosVehiculo.DatoCuenta;
    Result.Cuenta.IndiceVehiculo := -1;
    //Result.Cuenta.Vehiculo.Robado := cb_VehiculoRobado.Checked;
    Result.EsCambioVehiculo := True;
// Revision 1
    Result.Cuenta.FechaBajaCuenta:=NULLDATE;
// Fin de la Revision 1
    Result.Documentacion := nil;
    Result.Cuenta.CodigoAlmacenDestino := FCodigoAlmacenDestino;
    Result.Cuenta.FechaVencimientoTag := FFechaVencimientoTag; //Revision 2
    //Le agrego las propiedades estas al igual que en todos los casos de Recuperaci�n.
    //En el resto de los forms esta en la parte de modificaci�n. Lo coloco en esta parte
    //para no llenar tanto el form de modificaci�n.
    Result.EsVehiculoRobado := Result.Cuenta.Vehiculo.Robado;
    Result.EsVehiculoRecuperado := Result.Cuenta.Vehiculo.Recuperado;


    // Rev.4 / 03-07-2009 / Nelson Droguett Sierra
    Result.EsActivacionCuenta:=TRUE;
    Result.EsVehiculoNuevo:=FALSE;
    Result.EsVehiculoModificado:=FALSE;
end;

// PROPIEDADES

function TfrmActivarCuenta.GetAlmacenDestino: integer;
var
	encontrado : boolean;
begin
    case rgModalidadEntregaTag.ItemIndex  of
    	0 : Result := CONST_ALMACEN_EN_COMODATO;
        1 : Result := CONST_ALMACEN_ARRIENDO;
        2 : Result := CONST_ALMACEN_ENTREGADO_EN_CUOTAS;
    end;

    {No usado------------REV:4 09-Julio-2009
    if rgModalidadEntregaTag.ItemIndex = 0 then Result := CONST_ALMACEN_EN_COMODATO
	else begin
    	// Rev 5   ==> SS 769
    	// si dentro de los IDMotivoMovimientosCuentastelevias est� el Arriendo en Cuotas,
    	// entonces devuelve como c�digo de almacen el se�alado en CodigoConcepto.
        //
        // 29-Dic-2008, se prefija el almacen
        encontrado := False;
        cdConceptos.First;
        while not (encontrado or cdConceptos.Eof) do begin
            encontrado := (cdConceptos.FieldByName('IDMotivoMovCuentaTelevia').Value = CONST_CONCEPTO_ARRIENDO_TAG_CUOTAS);
        	cdConceptos.Next;
        end;

        if encontrado then Result := CONST_ALMACEN_ENTREGADO_EN_CUOTAS
        else Result := CONST_ALMACEN_ARRIENDO;
    end;
    }

end;

function TfrmActivarCuenta.GetAnio: integer;
begin
  Result := FreDatosVehiculo.Anio;
end;

function TfrmActivarCuenta.GetCategoria: integer;
begin
    Result := FreDatosVehiculo.CodigoCategoria;
end;

function TfrmActivarCuenta.GetCodigoColor: integer;
begin
    Result := FreDatosVehiculo.Color;
end;

function TfrmActivarCuenta.GetCodigoMarca: integer;
begin
    Result := FreDatosVehiculo.Marca;
end;

function TfrmActivarCuenta.GetCodigoTipo: integer;
begin
    Result := FreDatosVehiculo.CodigoTipo;
end;

function TfrmActivarCuenta.GetDatoCuenta: TCuentaVehiculo;
begin
    Result := FreDatosVehiculo.DatoCuenta;
end;

function TfrmActivarCuenta.GetDescripcionColor: AnsiString;
begin
    Result := FreDatosVehiculo.DescripcionColor;
end;

function TfrmActivarCuenta.GetDescripcionMarca: AnsiString;
begin
    Result := FreDatosVehiculo.DescripcionMarca;
end;

function TfrmActivarCuenta.GetDescripcionModelo: AnsiString;
begin
    Result := FreDatosVehiculo.DescripcionModelo;
end;

function TfrmActivarCuenta.GetDescripcionTipo: AnsiString;
begin
    Result := FreDatosVehiculo.DescripcionTipo;
end;

function TfrmActivarCuenta.GetDetalleVehiculoCompleta: Ansistring;
begin
    Result := FreDatosVehiculo.DetalleVehiculoCompleto;
end;

function TfrmActivarCuenta.GetDetalleVehiculoSimple: Ansistring;
begin
    Result := FreDatosVehiculo.DetalleVehiculoSimple;
end;

function TfrmActivarCuenta.GetDigitoVerificador: AnsiString;
begin
    Result := FreDatosVehiculo.DigitoVerificador;
end;

function TfrmActivarCuenta.GetDigitoVerificadorCorrecto: boolean;
begin
    Result := FreDatosVehiculo.DigitoVerificadorCorrecto;
end;

function TfrmActivarCuenta.GetDigitoVerificadorCorrectoRVM: boolean;
begin
    Result := FreDatosVehiculo.DigitoVerificadorCorrectoRVM;
end;

function TfrmActivarCuenta.GetDigitoVerificadorRVM: AnsiString;
begin
    Result := FreDatosVehiculo.DigitoVerificadorRVM;
end;

function TfrmActivarCuenta.GetPatente: AnsiString;
begin
    Result := FreDatosVehiculo.Patente;
end;

function TfrmActivarCuenta.GetPatenteRVM: String;
begin
    Result := FreDatosVehiculo.PatenteRVM;
end;

function TfrmActivarCuenta.GetSerialNumber: DWORD;
begin
    Result := FreDatosVehiculo.SerialNumber;
end;

function TfrmActivarCuenta.GetTag: AnsiString;
begin
    Result := FreDatosVehiculo.NumeroTag;
end;

function TfrmActivarCuenta.GetTipoAsignacionTag: string;
begin
    if rgGarantiaTag.ItemIndex = 0 then Result := CONST_OP_TRANSFERENCIA_TAG_NUEVO
    else Result := CONST_OP_TRANSFERENCIA_TAG_TRANSFERENCIA ;
end;

function TfrmActivarCuenta.GetTipoPatente: AnsiString;
begin
    Result := FreDatosVehiculo.TipoPatente;
end;

function TfrmActivarCuenta.GetContextMark: integer;
begin
    Result := FreDatosVehiculo.ContextMark;
end;

function TfrmActivarCuenta.GetObservaciones: Ansistring;
begin
    Result := FreDatosVehiculo.Observaciones;
end;


end.
