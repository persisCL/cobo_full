object frm_PerdidaTelevia: Tfrm_PerdidaTelevia
  Left = 353
  Top = 158
  BorderStyle = bsDialog
  Caption = 'P'#233'rdida de Telev'#237'a'
  ClientHeight = 544
  ClientWidth = 420
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 8
    Top = 109
    Width = 407
    Height = 29
  end
  object lblSeleccionar: TLabel
    Left = 274
    Top = 117
    Width = 22
    Height = 13
    Cursor = crHandPoint
    Caption = 'ac'#225
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold, fsUnderline]
    ParentFont = False
    OnClick = lblSeleccionarClick
  end
  object Label10: TLabel
    Left = 14
    Top = 117
    Width = 254
    Height = 13
    Caption = 'Para seleccionar un motivo de facturaci'#243'n haga click '
  end
  object Label9: TLabel
    Left = 14
    Top = 143
    Width = 169
    Height = 13
    Caption = 'Motivos seleccionados actualmente'
  end
  object lblTotalMovimientos: TLabel
    Left = 333
    Top = 293
    Width = 78
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = '0'
  end
  object lblTotalMovimientosTxt: TLabel
    Left = 236
    Top = 293
    Width = 98
    Height = 13
    AutoSize = False
    Caption = 'Total Movimientos :'
  end
  object cb_Robado: TCheckBox
    Left = 8
    Top = 8
    Width = 113
    Height = 17
    Caption = '&Veh'#237'culo Robado'
    TabOrder = 0
  end
  object gb_Denuncia: TGroupBox
    Left = 8
    Top = 32
    Width = 201
    Height = 49
    Caption = 'Fecha L'#237'mite Env'#237'o Constancia'
    TabOrder = 1
    object txt_HoraLimDenuncia: TTimeEdit
      Left = 103
      Top = 18
      Width = 90
      Height = 21
      AutoSelect = False
      TabOrder = 0
      AllowEmpty = False
      ShowSeconds = False
    end
    object txt_FechaLimDenuncia: TDateEdit
      Left = 7
      Top = 18
      Width = 90
      Height = 21
      AutoSelect = False
      TabOrder = 1
      Date = -693594.000000000000000000
    end
  end
  object gb_Cuenta: TGroupBox
    Left = 8
    Top = 308
    Width = 407
    Height = 49
    Caption = 'Fecha L'#237'mite Cambio Telev'#237'a'
    TabOrder = 3
    object txt_HoraLimCambio: TTimeEdit
      Left = 103
      Top = 18
      Width = 90
      Height = 21
      AutoSelect = False
      TabOrder = 0
      AllowEmpty = False
      ShowSeconds = False
    end
    object txt_FechaLimCambio: TDateEdit
      Left = 7
      Top = 18
      Width = 90
      Height = 21
      AutoSelect = False
      TabOrder = 1
      Date = -693594.000000000000000000
    end
  end
  object pnl_Botones: TPanel
    Left = 0
    Top = 503
    Width = 420
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 5
    object btn_Cancelar: TButton
      Left = 340
      Top = 7
      Width = 75
      Height = 25
      Cancel = True
      Caption = '&Cancelar'
      TabOrder = 1
      OnClick = btn_CancelarClick
    end
    object btn_Aceptar: TButton
      Left = 259
      Top = 7
      Width = 75
      Height = 25
      Caption = '&Aceptar'
      TabOrder = 0
      OnClick = btn_AceptarClick
    end
  end
  object Memo_Mensaje: TMemo
    Left = 8
    Top = 368
    Width = 407
    Height = 122
    Lines.Strings = (
      'Mensaje para el Operador')
    ReadOnly = True
    TabOrder = 4
  end
  object dblConceptos: TDBListEx
    Left = 8
    Top = 163
    Width = 407
    Height = 125
    BorderStyle = bsSingle
    Columns = <
      item
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 60
        Header.Caption = 'Concepto'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taRightJustify
        IsLink = False
        FieldName = 'CodigoConcepto'
      end
      item
        Alignment = taLeftJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 250
        Header.Caption = 'Descripci'#243'n de Concepto'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'DescripcionMotivo'
      end
      item
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Width = 90
        Header.Caption = 'Precio'
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Alignment = taCenter
        IsLink = False
        FieldName = 'PrecioEnPesos'
      end>
    DataSource = dsConceptos
    DragReorder = True
    ParentColor = False
    PopupMenu = mnuGrillaConceptos
    TabOrder = 2
    TabStop = True
    OnContextPopup = dblConceptosContextPopup
    OnKeyDown = dblConceptosKeyDown
  end
  object gbModalidadEntregaTelevia: TGroupBox
    Left = 223
    Top = 32
    Width = 177
    Height = 49
    Caption = 'Modalidad de entrega del Telev'#237'a'
    TabOrder = 6
    object lblModalidadEntregaTelevia: TLabel
      Left = 10
      Top = 24
      Width = 158
      Height = 13
      Alignment = taCenter
      AutoSize = False
      Caption = 'lblModalidadEntregaTelevia'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object mnuGrillaConceptos: TPopupMenu
    Left = 126
    Top = 192
    object mnuEliminarConcepto: TMenuItem
      Caption = 'Eliminar Concepto'
      OnClick = mnuEliminarConceptoClick
    end
    object CargarConceptos1: TMenuItem
      Caption = 'Cargar Conceptos'
      OnClick = lblSeleccionarClick
    end
  end
  object dsConceptos: TDataSource
    DataSet = cdConceptos
    Left = 86
    Top = 192
  end
  object cdConceptos: TClientDataSet
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Seleccionado'
        DataType = ftBoolean
      end
      item
        Name = 'CodigoConcepto'
        DataType = ftInteger
      end
      item
        Name = 'DescripcionMotivo'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'PrecioOrigen'
        DataType = ftLargeint
      end
      item
        Name = 'Moneda'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'Cotizacion'
        DataType = ftFloat
      end
      item
        Name = 'PrecioEnPesos'
        DataType = ftLargeint
      end
      item
        Name = 'Comentario'
        DataType = ftString
        Size = 255
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 46
    Top = 192
    Data = {
      E40000009619E0BD010000001800000008000000000003000000E4000C53656C
      656363696F6E61646F02000300000000000E436F6469676F436F6E636570746F
      0400010000000000114465736372697063696F6E4D6F7469766F020049000000
      010005574944544802000200FF000C50726563696F4F726967656E0400010000
      000000064D6F6E656461020049000000010005574944544802000200FF000A43
      6F74697A6163696F6E08000400000000000D50726563696F456E5065736F7304
      000100000000000A436F6D656E746172696F0200490000000100055749445448
      02000200FF000000}
  end
end
