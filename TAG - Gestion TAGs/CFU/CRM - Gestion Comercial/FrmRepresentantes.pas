{********************************** Unit Header ********************************
File Name : FrmRepresentantes.pas
Author :
Date Created:
Language : ES-AR
Description :

Revision: 1
Author : jconcheyro
Date Created : 17/10/2006
Description : Se agrega la property EsConvenioCN para poder hacer obligatorio o no
el Giro en ValidarDatos. el giro en un convenio que no es de CN no es obligatorio

*******************************************************************************}
unit FrmRepresentantes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, frmDatoPesona, PeaTypes,
  DPSControls, ExtCtrls, Util, DB, ADODB, DmConnection, Buttons, UtilProc, RStrings,
  //INICIO	: 20161013 CFU TASK_054_CFU_20161013-CRM_Validar_RUT_Representante_Legal
  ConstParametrosGenerales;
  //TERMINO	: 20161013 CFU TASK_054_CFU_20161013-CRM_Validar_RUT_Representante_Legal

type
  TFormRepresentantes = class(TForm)
	dbRepresentanteUno: TGroupBox;
    dbRepresentanteDos: TGroupBox;
	FreRepresentanteUno: TFreDatoPresona;
	Panel: TPanel;
	FreRepresentanteDos: TFreDatoPresona;
    ObtenerDatosPersona: TADOStoredProc;
	btnBorrarDos: TSpeedButton;
	dbRepresentanteTres: TGroupBox;
	btnBorrarTres: TSpeedButton;
	FreRepresentanteTres: TFreDatoPresona;
	btnBorrarUno: TSpeedButton;
    btnAceptar: TButton;
    btnSalir: TButton;
    procedure btnAceptarClick(Sender: TObject);
	procedure btnSalirClick(Sender: TObject);
    procedure FreRepresentanteUnotxtDocumentoChange(Sender: TObject);
	procedure btnBorrarDosClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
	procedure btnBorrarTresClick(Sender: TObject);
	procedure Habilitar(Control: TWinControl);
    procedure btnBorrarUnoClick(Sender: TObject);

  private
	EstaInicializando: boolean;
    FDocumentoConvenio: string;
	//FEsconvenioCN,															//SS_1147_MCA_20140408
    FEsConvenioNativo,															//SS_1147_MCA_20140408
	FConvenioPuro: Boolean;
	function DarRepresentanteUno:TDatosPersonales;
	function DarRepresentanteDos:TDatosPersonales;
	function DarRepresentanteTres:TDatosPersonales;
	procedure VerificarPersona(FrePersona: TFreDatoPresona);

  public
    property RepresentanteUno:TDatosPersonales read DarRepresentanteUno;
    property RepresentanteDos:TDatosPersonales read DarRepresentanteDos;
    property RepresentanteTres:TDatosPersonales read DarRepresentanteTres;

    function Inicializar(Caption: TCaption;
                         RepresentanteUno:TDatosPersonales; //Datos del RepresentanteUno
						 RepresentanteDos:TDatosPersonales; //Datos del RepresentanteDos
						 RepresentanteTres:TDatosPersonales; //Datos del RepresentanteDos
                         DocumentoConvenio: String;
						 //EsconvenioCN: Boolean;								//SS_1147_MCA_20140408
                         EsConvenioNativo: Boolean;								//SS_1147_MCA_20140408
						 Modo: TStateConvenio = scAlta;
						 ConvenioPuro: Boolean = False): Boolean;

	class procedure Limpiar(var Representante: TDatosPersonales);
  end;

var
  FormRepresentantes: TFormRepresentantes;

implementation

{$R *.dfm}

{ TFormRepresentantes }


{ TFormRepresentantes }

function TFormRepresentantes.Inicializar(Caption: TCaption;
  RepresentanteUno: TDatosPersonales;
  RepresentanteDos: TDatosPersonales;
  RepresentanteTres: TDatosPersonales;
  DocumentoConvenio: String;
  //EsconvenioCN: Boolean;														//SS_1147_MCA_20140408
  EsConvenioNativo: Boolean;													//SS_1147_MCA_20140408
  Modo: TStateConvenio = scAlta;
  ConvenioPuro: Boolean = False): Boolean;
begin
	result := false;
	try
		EstaInicializando:= true;

		FConvenioPuro := ConvenioPuro;
		//FEsconvenioCN := EsconvenioCN;										//SS_1147_MCA_20140408
        FEsConvenioNativo := EsConvenioNativo;									//SS_1147_MCA_20140408
		FDocumentoConvenio := DocumentoConvenio;

		self.Caption := Caption;
		//Cargo los Datos del Primero
		RepresentanteUno.Personeria := PERSONERIA_FISICA;
		FreRepresentanteUno.Limpiar;
		FreRepresentanteUno.RegistrodeDatos := RepresentanteUno;
        //FreRepresentanteUno.EsConvenioCN := FEsConvenioCN;					//SS_1147_MCA_20140408
        FreRepresentanteUno.EsConvenioNativo := FEsConvenioNativo;				//SS_1147_MCA_20140408

		//Cargo los Datos del Segundo
		RepresentanteDos.Personeria := PERSONERIA_FISICA;
		FreRepresentanteDos.Limpiar;
		FreRepresentanteDos.RegistrodeDatos := RepresentanteDos;
        //FreRepresentanteDos.EsConvenioCN := FEsConvenioCN;					//SS_1147_MCA_20140408
        FreRepresentanteDos.EsConvenioNativo := FEsConvenioNativo;				//SS_1147_MCA_20140408

		//Cargo los Datos del Tercero
		RepresentanteTres.Personeria := PERSONERIA_FISICA;
		FreRepresentanteTres.Limpiar;
		FreRepresentanteTres.RegistrodeDatos := RepresentanteTres;
        //FreRepresentanteTres.EsConvenioCN := FEsConvenioCN;                   //SS_1147_MCA_20140408
        FreRepresentanteTres.EsConvenioNativo := FEsConvenioNativo;				//SS_1147_MCA_20140408

		if Modo = scNormal then begin
			EnableControlsInContainer(self,False);
			Habilitar(btnSalir);
		end
		else begin
			//case FEsconvenioCN of												//SS_1147_MCA_20140408
            case FEsConvenioNativo of											//SS_1147_MCA_20140408
				true: begin
					if Modo = scAlta then begin
						EnableControlsInContainer(FreRepresentanteUno, RepresentanteUno.CodigoPersona <= 0);
						EnableControlsInContainer(FreRepresentanteDos, RepresentanteDos.CodigoPersona <= 0);
						EnableControlsInContainer(FreRepresentanteTres, RepresentanteTres.CodigoPersona <= 0);

						if Trim(RepresentanteUno.Sexo) = '' then begin
							FreRepresentanteUno.lblSexo_Contacto.Enabled := True;
							Habilitar(FreRepresentanteUno.cbSexo);
						end;

						if Trim(RepresentanteDos.Sexo) = '' then begin
							FreRepresentanteDos.Enabled := True;
							Habilitar(FreRepresentanteDos.cbSexo);
						end;

						if Trim(RepresentanteTres.Sexo)= '' then begin
							FreRepresentanteTres.Enabled := True;
							Habilitar(FreRepresentanteTres.cbSexo);
						end;

						if FreRepresentanteUno.txtFechaNacimiento.Date = NULLDATE then begin
							FreRepresentanteUno.lbl_FechaNacimiento.Enabled := True;
							Habilitar(FreRepresentanteUno.txtFechaNacimiento)
						end;

						if FreRepresentanteDos.txtFechaNacimiento.Date = NULLDATE then begin
							FreRepresentanteDos.lbl_FechaNacimiento.Enabled := True;
							Habilitar(FreRepresentanteDos.txtFechaNacimiento)
						end;

						if FreRepresentanteTres.txtFechaNacimiento.Date= NULLDATE then begin
							FreRepresentanteTres.lbl_FechaNacimiento.Enabled := True;
							Habilitar(FreRepresentanteTres.txtFechaNacimiento)
						end;
					end;
				end;

				else begin
					EnableControlsInContainer(self,True);
				end
			end;

		end;

		btnBorrarUno.Enabled := True;
		btnBorrarDos.Enabled := True;
		btnBorrarTres.Enabled := True;

		EstaInicializando:= false;

		result := true;
	except
		Exit;
	end;
end;

procedure TFormRepresentantes.btnAceptarClick(Sender: TObject);
var
	AuxRegPersona:TDatosPersonales;
	i : Integer;
    //INICIO	: 20161013 CFU TASK_054_CFU_20161013-CRM_Validar_RUT_Representante_Legal
    strRUT: string;
    iRUT: Integer;
    //TERMINO	: 20161013 CFU TASK_054_CFU_20161013-CRM_Validar_RUT_Representante_Legal
begin
	if (Trim(FreRepresentanteUno.txtDocumento.Text) = '') and (Trim(FreRepresentanteDos.txtDocumento.Text) = '') and (Trim(FreRepresentanteTres.txtDocumento.Text) = '') then begin
		MsgBoxBalloon(Format(MSG_VALIDAR_DEBE_EL,[STR_NUMERO_DOCUMENTO]), MSG_ERROR_NUMERO_DOCUMENTO, MB_ICONSTOP,FreRepresentanteUno.txtDocumento);
		Exit;
	end;

    //INICIO	: 20161013 CFU TASK_054_CFU_20161013-CRM_Validar_RUT_Representante_Legal
    if (Trim(FreRepresentanteUno.txtDocumento.Text) <> '') then begin
    	try
            strRUT	:= Trim(FreRepresentanteUno.txtDocumento.Text);
            iRUT 	:= StrToInt(Copy(strRUT, 1, Length(strRUT)-1));
            if iRUT > VALOR_MAX_RUT_REP_LEGAL then begin
                MsgBoxBalloon(MSG_VALIDAR_RUT_REP_LEGALES, MSG_ERROR_NUMERO_DOCUMENTO, MB_ICONSTOP,FreRepresentanteUno.txtDocumento);
                Exit;
            end;
        except
            MsgBoxBalloon(MSG_VALIDAR_RUT_REP_LEGALES, MSG_ERROR_NUMERO_DOCUMENTO, MB_ICONSTOP,FreRepresentanteUno.txtDocumento);
            Exit;
        end;
    end;
    
    if (Trim(FreRepresentanteDos.txtDocumento.Text) <> '') then begin
    	try
            strRUT	:= Trim(FreRepresentanteDos.txtDocumento.Text);
            iRUT 	:= StrToInt(Copy(strRUT, 1, Length(strRUT)-1));
            if iRUT > VALOR_MAX_RUT_REP_LEGAL then begin
                MsgBoxBalloon(MSG_VALIDAR_RUT_REP_LEGALES, MSG_ERROR_NUMERO_DOCUMENTO, MB_ICONSTOP,FreRepresentanteDos.txtDocumento);
                Exit;
            end;
        except
            MsgBoxBalloon(MSG_VALIDAR_RUT_REP_LEGALES, MSG_ERROR_NUMERO_DOCUMENTO, MB_ICONSTOP,FreRepresentanteDos.txtDocumento);
            Exit;
        end;
    end;
    if (Trim(FreRepresentanteTres.txtDocumento.Text) <> '') then begin
    	try
            strRUT	:= Trim(FreRepresentanteTres.txtDocumento.Text);
            iRUT 	:= StrToInt(Copy(strRUT, 1, Length(strRUT)-1));
            if iRUT > VALOR_MAX_RUT_REP_LEGAL then begin
                MsgBoxBalloon(MSG_VALIDAR_RUT_REP_LEGALES, MSG_ERROR_NUMERO_DOCUMENTO, MB_ICONSTOP,FreRepresentanteTres.txtDocumento);
                Exit;
            end;
        except
            MsgBoxBalloon(MSG_VALIDAR_RUT_REP_LEGALES, MSG_ERROR_NUMERO_DOCUMENTO, MB_ICONSTOP,FreRepresentanteTres.txtDocumento);
            Exit;
        end;
    end;

    //TERMINO	: 20161013 CFU TASK_054_CFU_20161013-CRM_Validar_RUT_Representante_Legal

	if (Trim(FreRepresentanteUno.txtDocumento.Text) <> '') and (not FreRepresentanteUno.ValidarDatos) then Exit;
	if (Trim(FreRepresentanteDos.txtDocumento.Text) <> '') and (not FreRepresentanteDos.ValidarDatos) then Exit;
	if (Trim(FreRepresentanteTres.txtDocumento.Text) <> '') and (not FreRepresentanteTres.ValidarDatos) then Exit;

	if (Trim(FreRepresentanteDos.RegistrodeDatos.NumeroDocumento) <> '') and
	   (FreRepresentanteDos.RegistrodeDatos.TipoDocumento = FreRepresentanteUno.RegistrodeDatos.TipoDocumento) and
	   (FreRepresentanteDos.RegistrodeDatos.NumeroDocumento = FreRepresentanteUno.RegistrodeDatos.NumeroDocumento) then begin
		MsgBoxBalloon(MSG_ERROR_DOCUMENTOS_IGUALES, MSG_ERROR_NUMERO_DOCUMENTO, MB_ICONSTOP,FreRepresentanteDos.txtDocumento);
		Exit;
	end;

	if (Trim(FreRepresentanteDos.RegistrodeDatos.NumeroDocumento) <> '') and
	   (FreRepresentanteDos.RegistrodeDatos.TipoDocumento = FreRepresentanteTres.RegistrodeDatos.TipoDocumento) and
	   (FreRepresentanteDos.RegistrodeDatos.NumeroDocumento = FreRepresentanteTres.RegistrodeDatos.NumeroDocumento) then begin
		MsgBoxBalloon(MSG_ERROR_DOCUMENTOS_IGUALES, MSG_ERROR_NUMERO_DOCUMENTO, MB_ICONSTOP,FreRepresentanteTres.txtDocumento);
		Exit;
	end;

	if (Trim(FreRepresentanteUno.RegistrodeDatos.NumeroDocumento) <> '') and
	   (FreRepresentanteUno.RegistrodeDatos.TipoDocumento = FreRepresentanteTres.RegistrodeDatos.TipoDocumento) and
	   (FreRepresentanteUno.RegistrodeDatos.NumeroDocumento = FreRepresentanteTres.RegistrodeDatos.NumeroDocumento) then begin
        MsgBoxBalloon(MSG_ERROR_DOCUMENTOS_IGUALES, MSG_ERROR_NUMERO_DOCUMENTO, MB_ICONSTOP,FreRepresentanteTres.txtDocumento);
        Exit;
	end;

	if not FreRepresentanteUno.txt_Apellido.Enabled then begin
		if Trim(FreRepresentanteUno.txtDocumento.Text) = Trim(FDocumentoConvenio) then begin
            MsgBoxBalloon(MSG_ERROR_MISMA_PERSONA_CONVENIO, MSG_ERROR_NUMERO_DOCUMENTO, MB_ICONSTOP, FreRepresentanteUno.txtDocumento);
            Exit;
        end
        else begin
            if FreRepresentanteUno.RegistrodeDatos.Personeria <> PERSONERIA_FISICA then begin
                MsgBoxBalloon(MSG_ERROR_ES_JURIDICA, MSG_ERROR_NUMERO_DOCUMENTO, MB_ICONSTOP, FreRepresentanteUno.txtDocumento);
                Exit;
			end;
		end;
    end;

    if not FreRepresentanteDos.txt_Apellido.Enabled then begin
        if Trim(FreRepresentanteDos.txtDocumento.Text) = Trim(FDocumentoConvenio) then begin
            MsgBoxBalloon(MSG_ERROR_MISMA_PERSONA_CONVENIO, MSG_ERROR_NUMERO_DOCUMENTO, MB_ICONSTOP, FreRepresentanteDos.txtDocumento);
            Exit;
        end
        else begin
			if FreRepresentanteDos.RegistrodeDatos.Personeria <> PERSONERIA_FISICA  then begin
				MsgBoxBalloon(MSG_ERROR_ES_JURIDICA, MSG_ERROR_NUMERO_DOCUMENTO, MB_ICONSTOP, FreRepresentanteDos.txtDocumento);
                Exit;
            end;
        end;
    end;

    if not FreRepresentanteTres.txt_Apellido.Enabled then begin
        if Trim(FreRepresentanteTres.txtDocumento.Text) = Trim(FDocumentoConvenio) then begin
            MsgBoxBalloon(MSG_ERROR_MISMA_PERSONA_CONVENIO, MSG_ERROR_NUMERO_DOCUMENTO, MB_ICONSTOP, FreRepresentanteTres.txtDocumento);
            Exit;
		end
		else begin
            if FreRepresentanteTres.RegistrodeDatos.Personeria <> PERSONERIA_FISICA then begin
                MsgBoxBalloon(MSG_ERROR_ES_JURIDICA, MSG_ERROR_NUMERO_DOCUMENTO, MB_ICONSTOP, FreRepresentanteTres.txtDocumento);
                Exit;
            end;
        end;
    end;

	for i := 0 to 1 do begin // Esto hace que siempre los rep legales esten completados del 1ro a l 3ro
		if (Trim(FreRepresentanteDos.RegistrodeDatos.NumeroDocumento) <> '') and (Trim(FreRepresentanteUno.RegistrodeDatos.NumeroDocumento) = '') then begin //and (Trim(FreRepresentanteDos.RegistrodeDatos.NumeroDocumento) = '') then begin
			AuxRegPersona := FreRepresentanteDos.RegistrodeDatos;
			FreRepresentanteDos.Limpiar;
			FreRepresentanteUno.RegistrodeDatos := AuxRegPersona;
		end;

		if (Trim(FreRepresentanteTres.RegistrodeDatos.NumeroDocumento) <> '') and (Trim(FreRepresentanteDos.RegistrodeDatos.NumeroDocumento) = '') then begin//and (Trim(FreRepresentanteDos.RegistrodeDatos.NumeroDocumento) = '') then begin
			AuxRegPersona := FreRepresentanteTres.RegistrodeDatos;
			FreRepresentanteTres.Limpiar;
			FreRepresentanteDos.RegistrodeDatos := AuxRegPersona;
		end;
	end;

    ModalResult := mrOk;
end;

procedure TFormRepresentantes.btnSalirClick(Sender: TObject);
begin
    ModalResult:=mrCancel;
end;


//function TFormRepresentantes.DebeValidarRepresentanteDos: boolean;
//begin
//     result := (Trim(FreRepresentanteDos.txtDocumento.text) <> '') or
//     (Trim(FreRepresentanteDos.txt_Nombre.Text) <> '') or
//     (Trim(FreRepresentanteDos.txt_Apellido.Text) <> '') or
//     (Trim(FreRepresentanteDos.txt_ApellidoMaterno.Text) <> '') or
//     (FreRepresentanteDos.cbSexo.ItemIndex > 1) or
//     (FreRepresentanteDos.txtFechaNacimiento.Date <> nulldate);
//end;
//
//function TFormRepresentantes.DebeValidarRepresentanteTres: boolean;
//begin
//     result := (Trim(FreRepresentanteTres.txtDocumento.text) <> '') or
//     (Trim(FreRepresentanteTres.txt_Nombre.Text) <> '') or
//     (Trim(FreRepresentanteTres.txt_Apellido.Text) <> '') or
//     (Trim(FreRepresentanteTres.txt_ApellidoMaterno.Text) <> '') or
//     (FreRepresentanteTres.cbSexo.ItemIndex > 1) or
//     (FreRepresentanteTres.txtFechaNacimiento.Date <> nulldate);
//end;

procedure TFormRepresentantes.VerificarPersona(FrePersona: TFreDatoPresona);
var
    RegistroPersona: TDatosPersonales;
begin
    //Verifico si ya es una persona

	if ModalResult = mrOk then exit; // no valido

	RegistroPersona := FrePersona.RegistrodeDatos;

	if (Length(FrePersona.txtDocumento.Text) < 8) then begin
        if RegistroPersona.CodigoPersona <> -1 then begin
				RegistroPersona.CodigoPersona := -1;
				Limpiar(RegistroPersona);
				FrePersona.RegistrodeDatos := RegistroPersona;
                FrePersona.Limpiar;
                if not FConvenioPuro then EnableControlsInContainer(FrePersona.Parent, True);
            end;
        RegistroPersona.CodigoPersona := -1;
		FrePersona.RegistrodeDatos := RegistroPersona;
        FrePersona.txtDocumento.SelStart := Length(Trim(RegistroPersona.NumeroDocumento));
		exit;
	end;

	if Trim(FrePersona.txtDocumento.Text) = Trim(FDocumentoConvenio) then begin
		if not EstaInicializando then begin
			MsgBoxBalloon(MSG_ERROR_MISMA_PERSONA_CONVENIO, MSG_ERROR_NUMERO_DOCUMENTO, MB_ICONSTOP, FrePersona.txtDocumento);
		end;

		if not FConvenioPuro then EnableControlsInContainer(FrePersona.Parent, False);

		RegistroPersona.CodigoPersona := -1;
		FrePersona.lblRutRun.Enabled := true;
		FrePersona.RegistrodeDatos := RegistroPersona;
		FrePersona.txtDocumento.SelStart := Length(Trim(RegistroPersona.NumeroDocumento));
		Exit;
	end;

	ObtenerDatosPersona.Close;
	ObtenerDatosPersona.Parameters.Refresh;
	ObtenerDatosPersona.Parameters.ParamByName('@CodigoDocumento').Value := TIPO_DOCUMENTO_RUT;
	ObtenerDatosPersona.Parameters.ParamByName('@NumeroDocumento').Value := Trim(FrePersona.txtDocumento.Text);
	ObtenerDatosPersona.Open;

	if (ObtenerDatosPersona.RecordCount > 0) and (not EstaInicializando) then begin
	   if Trim(ObtenerDatosPersona.FieldByName('Personeria').AsString) = PERSONERIA_FISICA then begin
			RegistroPersona.Personeria := PERSONERIA_FISICA;
			RegistroPersona.Nombre := Trim(ObtenerDatosPersona.FieldByName('Nombre').AsString);
			RegistroPersona.Apellido := Trim(ObtenerDatosPersona.FieldByName('Apellido').AsString);
			RegistroPersona.ApellidoMaterno := Trim(ObtenerDatosPersona.FieldByName('ApellidoMaterno').AsString);
			RegistroPersona.FechaNacimiento := ObtenerDatosPersona.FieldByName('FechaNacimiento').AsDateTime;
            RegistroPersona.Sexo := (Trim(ObtenerDatosPersona.FieldByName('Sexo').AsString)+SEXO_MASCULINO)[1];
            RegistroPersona.LugarNacimiento := Trim(ObtenerDatosPersona.FieldByName('LugarNacimiento').AsString);
            RegistroPersona.CodigoActividad := ObtenerDatosPersona.FieldByName('CodigoActividad').AsInteger;
            RegistroPersona.Password := Trim(ObtenerDatosPersona.FieldByName('Password').AsString);
            RegistroPersona.Pregunta := Trim(ObtenerDatosPersona.FieldByName('Pregunta').AsString);
            RegistroPersona.Respuesta := Trim(ObtenerDatosPersona.FieldByName('Respuesta').AsString);
            RegistroPersona.TipoDocumento := Trim(ObtenerDatosPersona.FieldByName('CodigoDocumento').AsString);
            if (not EstaInicializando) and (not FConvenioPuro) then
				MsgBoxBalloon(MSG_CAPTION_PERSONA_REGISTRADA, self.Caption, MB_ICONWARNING, FrePersona.txtDocumento);

            if not FConvenioPuro then EnableControlsInContainer(FrePersona.Parent, False);
            if not FConvenioPuro then Habilitar(FrePersona.txtDocumento);

            FrePersona.lblRutRun.Enabled := true;
       end
       else begin
			if not EstaInicializando then
                MsgBoxBalloon(MSG_ERROR_ES_JURIDICA, MSG_ERROR_NUMERO_DOCUMENTO, MB_ICONSTOP, FrePersona.txtDocumento);
			Limpiar(RegistroPersona);
			FrePersona.RegistrodeDatos := RegistroPersona;
			RegistroPersona.Personeria := #0;
            FrePersona.txtDocumento.SelStart := Length(Trim(RegistroPersona.NumeroDocumento));
			if not FConvenioPuro then EnableControlsInContainer(FrePersona.Parent, False);
			if not FConvenioPuro then Habilitar(FrePersona.txtDocumento);

            FrePersona.lblRutRun.Enabled := true;
       end;

       RegistroPersona.CodigoPersona := ObtenerDatosPersona.FieldByName('CodigoPersona').AsInteger;

       FrePersona.RegistrodeDatos := RegistroPersona;
    end
    else begin
		if (RegistroPersona.CodigoPersona <> -1) then begin
                FrePersona.Limpiar;
                Limpiar(RegistroPersona);
                FrePersona.RegistrodeDatos := RegistroPersona;
				FrePersona.txtDocumento.SelStart := Length(Trim(RegistroPersona.NumeroDocumento));
                if not FConvenioPuro then EnableControlsInContainer(FrePersona.Parent, True);
        end
        else begin
            if (not FrePersona.txt_Apellido.Enabled) and (Trim(FrePersona.txtDocumento.Text) <> Trim(FDocumentoConvenio)) then begin
				if not FConvenioPuro then EnableControlsInContainer(FrePersona.Parent, True);
                Limpiar(RegistroPersona);
                FrePersona.Limpiar;
                FrePersona.RegistrodeDatos := RegistroPersona;
                FrePersona.txtDocumento.SelStart := Length(Trim(RegistroPersona.NumeroDocumento));
            end;
        end;
	end;
end;


procedure TFormRepresentantes.FreRepresentanteUnotxtDocumentoChange(
  Sender: TObject);
var
	Frame : TFreDatoPresona;
begin
	Frame := TFreDatoPresona(TEdit(Sender).parent.Parent);

	if not EstaInicializando then
		VerificarPersona(Frame)
	else begin
		if Trim(Frame.txt_Nombre.Text) <> '' then begin
			ObtenerDatosPersona.Close;
			ObtenerDatosPersona.Parameters.Refresh;
			ObtenerDatosPersona.Parameters.ParamByName('@CodigoDocumento').Value := TIPO_DOCUMENTO_RUT;
			ObtenerDatosPersona.Parameters.ParamByName('@NumeroDocumento').Value := Trim(Frame.txtDocumento.Text);
			ObtenerDatosPersona.Open;

			if not FConvenioPuro then EnableControlsInContainer(Frame.Parent, ObtenerDatosPersona.RecordCount = 0);
			if not FConvenioPuro then Habilitar(Frame.txtDocumento);

			ObtenerDatosPersona.Close;
		end;
	end;

	btnBorrarUno.Enabled := FreRepresentanteUno.txtDocumento.Enabled;
	btnBorrarDos.Enabled := FreRepresentanteDos.txtDocumento.Enabled;
	btnBorrarTres.Enabled := FreRepresentanteTres.txtDocumento.Enabled;
end;

function TFormRepresentantes.DarRepresentanteDos: TDatosPersonales;
begin
    result := FreRepresentanteDos.RegistrodeDatos;
end;

function TFormRepresentantes.DarRepresentanteUno: TDatosPersonales;
begin
    result := FreRepresentanteUno.RegistrodeDatos;
end;

class procedure TFormRepresentantes.Limpiar(var Representante: TDatosPersonales);
begin
	Representante.CodigoPersona := -1;
	Representante.RazonSocial := '';
    Representante.Apellido := '';
    Representante.ApellidoMaterno := '';
    Representante.Nombre := '';
    Representante.Sexo := #0;
    Representante.LugarNacimiento := '';
    Representante.FechaNacimiento := NullDate;
    Representante.CodigoActividad := -1;
	Representante.Password := '';
    Representante.Pregunta := '';
    Representante.Respuesta := '';
    Representante.Pregunta := '';
end;

procedure TFormRepresentantes.btnBorrarDosClick(Sender: TObject);
begin
	FreRepresentanteDos.Limpiar;
end;

procedure TFormRepresentantes.FormShow(Sender: TObject);
begin
	if FreRepresentanteUno.txtDocumento.Enabled then
		FreRepresentanteUno.txtDocumento.SetFocus;
end;

procedure TFormRepresentantes.btnBorrarTresClick(Sender: TObject);
begin
	FreRepresentanteTres.Limpiar;
end;

function TFormRepresentantes.DarRepresentanteTres: TDatosPersonales;
begin
    result := FreRepresentanteTres.RegistrodeDatos;
end;

procedure TFormRepresentantes.Habilitar(Control: TWinControl);
begin
    if Control <> nil then Begin
        Control.Enabled := True;
        Habilitar(Control.Parent);
    end;
end;
procedure TFormRepresentantes.btnBorrarUnoClick(Sender: TObject);
begin
	FreRepresentanteUno.Limpiar;
end;

end.
