object FormRegistrarLlamada: TFormRegistrarLlamada
  Left = 230
  Top = 275
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Registrar Llamada'
  ClientHeight = 110
  ClientWidth = 412
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object pnlBottom: TPanel
    Left = 0
    Top = 72
    Width = 412
    Height = 38
    Align = alBottom
    TabOrder = 0
    DesignSize = (
      412
      38)
    object btnAceptar: TButton
      Left = 238
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = '&Aceptar'
      Default = True
      TabOrder = 0
      OnClick = btnAceptarClick
    end
    object btnCancelar: TButton
      Left = 322
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = '&Cancelar'
      ModalResult = 2
      TabOrder = 1
      OnClick = btnCancelarClick
    end
  end
  object pnlMain: TPanel
    Left = 0
    Top = 0
    Width = 412
    Height = 72
    Align = alClient
    TabOrder = 1
    object lblResultado: TLabel
      Left = 16
      Top = 31
      Width = 116
      Height = 13
      Caption = 'Resultado de la llamada:'
    end
    object cbEstadosResultados: TVariantComboBox
      Left = 144
      Top = 28
      Width = 241
      Height = 21
      Style = vcsDropDownList
      ItemHeight = 13
      TabOrder = 0
      Items = <>
    end
  end
end
