unit formReacuperacionTag;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Convenios, PeaProcs, UtilProc;

type
  TfrmReacuperacionTag = class(TForm)
    pnlBotones: TPanel;
    btnAceptar: TButton;
    btnCancelar: TButton;
    txt_Tag: TEdit;
    Label1: TLabel;
    cb_VehiculoRecuperado: TCheckBox;
    procedure btnCancelarClick(Sender: TObject);
    procedure txt_TagKeyPress(Sender: TObject; var Key: Char);
    procedure btnAceptarClick(Sender: TObject);
  private
    { Private declarations }
    FVehiculo: TVehiculosConvenio;
    function GetRecuperado: Boolean;
  public
    { Public declarations }
    function Inicializa(Vehiculo: TVehiculosConvenio): Boolean;
    property Recuperado: Boolean read GetRecuperado;
  end;

var
  frmReacuperacionTag: TfrmReacuperacionTag;

implementation

{$R *.dfm}

procedure TfrmReacuperacionTag.btnCancelarClick(Sender: TObject);
begin
    close;
end;

function TfrmReacuperacionTag.GetRecuperado: Boolean;
begin
    Result := not(cb_VehiculoRecuperado.Visible) or cb_VehiculoRecuperado.Checked;
end;

function TfrmReacuperacionTag.Inicializa(
  Vehiculo: TVehiculosConvenio): Boolean;
begin
    FVehiculo := Vehiculo;
    txt_Tag.Clear;
    cb_VehiculoRecuperado.Visible := Vehiculo.Cuenta.Vehiculo.Robado;
    Result := True;
end;

procedure TfrmReacuperacionTag.txt_TagKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not (Key  in ['0'..'9', #8]) then
        Key := #0;
end;

procedure TfrmReacuperacionTag.btnAceptarClick(Sender: TObject);
resourcestring
	MSG_ERROR_TAG_DE_OTRA_CUENTA = 'El Televia no pertenece a esta cuenta';
	MSG_TAG_INVALIDO = 'Televia Invalido';
begin
    if  StrToInt64(txt_Tag.Text) <> StrToInt64(SerialNumberToEtiqueta(FVehiculo.Cuenta.ContactSerialNumber)) then begin
		MsgBoxBalloon(MSG_ERROR_TAG_DE_OTRA_CUENTA, MSG_TAG_INVALIDO, MB_ICONSTOP, txt_Tag);
        txt_Tag.SetFocus;
        exit;
    end;

    ModalResult := mrOk;
end;

end.
