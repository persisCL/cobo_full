unit frmAltaForzada;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables, StdCtrls, ExtCtrls, UtilProc, UtilDB, ADODB,
  PeaProcs, PeaTypes,Util, Variants, DMConnection, DPSControls, DmiCtrls,
  Mask, DBCtrls, Grids, DBGrids, DBClient, Convenios;

type
  TFormAltaForzada = class(TForm)
    lbl1: TLabel;
    dbgrdBajaForzada: TDBGrid;
    bvl1: TBevel;
    btn_ok: TButton;
    cdsBajaForzada: TClientDataSet;
    dsBajaForzada: TDataSource;
    procedure dbgrdBajaForzadaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dbgrdBajaForzadaCellClick(Column: TColumn);
    procedure dbgrdBajaForzadaColEnter(Sender: TObject);
    procedure dbgrdBajaForzadaColExit(Sender: TObject);
    procedure dbgrdBajaForzadaDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure btn_okClick(Sender: TObject);
  private
    { Private declarations }
    GridOriginalOptions: TDBGridOptions;
  public
    { Public declarations }
    FBajasForzadasConvenio: TBajasForzadasConvenio;

    function Inicializar(BajasForzadasConvenio: TBajasForzadasConvenio): Boolean;
  end;

var
  FormAltaForzada: TFormAltaForzada;

implementation

{$R *.dfm}


function TFormAltaForzada.Inicializar(BajasForzadasConvenio: TBajasForzadasConvenio): Boolean;
var
    I: Integer;
    ABajaForzada: TBajaForzada;
begin
    Result := True;
    FBajasForzadasConvenio := BajasForzadasConvenio;

    cdsBajaForzada.Active := False;
    for I := 0 to cdsBajaForzada.Fields.Count - 1 do
        cdsBajaForzada.Fields[i].ReadOnly := False;

    //try
    //    cdsBajaForzada.EmptyDataSet;
    //except
    //end;

    cdsBajaForzada.CreateDataSet;
    cdsBajaForzada.Active   := True;
    cdsBajaForzada.ReadOnly := False;

    for I := 0 to FBajasForzadasConvenio.CantidadBajasForzadas - 1 do begin
        ABajaForzada := FBajasForzadasConvenio.BajasForzadas[I];
        cdsBajaForzada.Append;

        cdsBajaForzada.FieldByName('CodigoBajaForzada').Value   := ABajaForzada.CodigoBajaForzada;
        cdsBajaForzada.FieldByName('Deshacer').Value            := ABajaForzada.Deshacer;
        cdsBajaForzada.FieldByName('Patente').Value             := ABajaForzada.Patente;
        cdsBajaForzada.FieldByName('TagNativo').Value           := ABajaForzada.TagNativo;
        cdsBajaForzada.FieldByName('ConvenioForaneo').Value     := ABajaForzada.ConvenioForaneo;
        cdsBajaForzada.FieldByName('TagForaneo').Value          := ABajaForzada.TagForaneo;
        cdsBajaForzada.FieldByName('RUTForaneo').Value          := ABajaForzada.RUTForaneo;
        cdsBajaForzada.FieldByName('NombreForaneo').Value       := ABajaForzada.NombreForaneo;
        cdsBajaForzada.FieldByName('FechaHora').Value           := ABajaForzada.FechaHora;
        cdsBajaForzada.FieldByName('CodigoUsuario').Value       := ABajaForzada.CodigoUsuario;

        cdsBajaForzada.Post;
    end;

    for I := 0 to cdsBajaForzada.Fields.Count - 1 do
        if cdsBajaForzada.Fields[I].DataType <> ftBoolean then
            cdsBajaForzada.Fields[i].ReadOnly := True;

    cdsBajaForzada.First;
end;

procedure TFormAltaForzada.btn_okClick(Sender: TObject);
begin
    cdsBajaForzada.First;
    while not cdsBajaForzada.Eof do begin
        FBajasForzadasConvenio.MarcarBajaForzadaDeshacer(
            cdsBajaForzada.FieldByName('CodigoBajaForzada').AsInteger,
            cdsBajaForzada.FieldByName('Deshacer').AsBoolean
        );

        cdsBajaForzada.Next;
    end;
     ModalResult := mrOk;
end;

procedure TFormAltaForzada.dbgrdBajaForzadaCellClick(Column: TColumn);
begin
     if Column.Field.DataType = ftBoolean then begin
        Column.Grid.DataSource.DataSet.Edit;
        Column.Field.Value := not Column.Field.AsBoolean; 
        Column.Grid.DataSource.DataSet.Post;
    end;
end;

procedure TFormAltaForzada.dbgrdBajaForzadaColEnter(Sender: TObject);
begin
    if dbgrdBajaForzada.SelectedField.DataType = ftBoolean then begin
        GridOriginalOptions := dbgrdBajaForzada.Options;
        dbgrdBajaForzada.Options := dbgrdBajaForzada.Options - [dgEditing];
    end;
end;

procedure TFormAltaForzada.dbgrdBajaForzadaColExit(Sender: TObject);
begin
    if dbgrdBajaForzada.SelectedField.DataType = ftBoolean then
        dbgrdBajaForzada.Options := GridOriginalOptions;
end;

procedure TFormAltaForzada.dbgrdBajaForzadaDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
const
   CtrlState: array[Boolean] of integer = (DFCS_BUTTONCHECK, DFCS_BUTTONCHECK or DFCS_CHECKED) ;
begin
    if Column.Field.DataType = ftBoolean then begin
        dbgrdBajaForzada.Canvas.FillRect(Rect);
        if VarIsNull(Column.Field.Value) then
            DrawFrameControl(dbgrdBajaForzada.Canvas.Handle,Rect, DFC_BUTTON, DFCS_BUTTONCHECK or DFCS_INACTIVE)
        else
            DrawFrameControl(dbgrdBajaForzada.Canvas.Handle,Rect, DFC_BUTTON, CtrlState[Column.Field.AsBoolean]);
    end;
end;

procedure TFormAltaForzada.dbgrdBajaForzadaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    if (dbgrdBajaForzada.SelectedField.DataType = ftBoolean) and (key = VK_SPACE) then begin
        dbgrdBajaForzada.DataSource.DataSet.Edit;
        dbgrdBajaForzada.SelectedField.Value:= not dbgrdBajaForzada.SelectedField.AsBoolean;
        dbgrdBajaForzada.DataSource.DataSet.Post;
    end;
end;

end.
