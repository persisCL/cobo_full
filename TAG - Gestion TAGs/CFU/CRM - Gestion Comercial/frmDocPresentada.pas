unit frmDocPresentada;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DPSControls, ExtCtrls, DB, ADODB,DMConnection,RStrings,
  DBClient,util,UtilProc, DmiCtrls,Peatypes, StrUtils;

type
    TRepresentante = (Primero,Segundo,Tercero);

type
  TformDocPresentada = class(TForm)
    gb_Convenio: TGroupBox;
    Panel10: TPanel;
    sp: TADOStoredProc;
    sb_Convenio: TScrollBox;
    btn_Cancelar: TButton;
    btn_ok: TButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btn_okClick(Sender: TObject);
    procedure btn_CancelarClick(Sender: TObject);
  private
    { Private declarations }
    FCodigoTipoOrdenServicio: integer;
    RegDocInterno:RegDocumentacionPresentada;
//    RegDocInternoPorOS: ATOrdenServicio;
    IndiceVehiculo:Shortint;
    FTienePAT: boolean;
    function MostrarDocumentacion(Panel:TScrollBox;MargenSuperior:Integer;
                    Descripcion, MasInformacion: String;
                    Obligatorio:Boolean;
                    CodigoDocumentacionRespaldo:Integer;
                    IndiceVehiculo:Shortint; Check: byte;
                    Explicacion: AnsiString = ''):Integer;
    procedure OnClickCombo(Sender: TObject);
    function BuscarComponente(Original:TCheckBox):TCheckBox;
    function GrabarTag(CodigoDocumentacionRespaldo:Integer;Indice:Shortint;Optativo:Boolean):Longint;
    function ObtenerTag(Tag:Longint;var CodigoDocumentacionRespaldo:Integer;var Indice:Shortint):boolean;
    function GetDocumentacionPresentada: RegDocumentacionPresentada;
    function CargadoAntes(DocumentacionPresentada: RegDocumentacionPresentada; CodigoDocumentacionRespaldo:integer; indice:integer):Byte;
    procedure SetTienePAT(const Value: boolean);
    function CalculaHeightLabel(AnchoLabel:integer;Texto:String): integer; //lenghtTexto
    procedure Habilitar(Control: TWinControl);

  public
    { Public declarations }
    function Inicializar(Caption: TCaption; Vehiculos:TClientDataSet;
            DocumentacionPresentada: RegDocumentacionPresentada;
            Personeria: char;
            MedioDePago : TTiposMediosPago;
            NombrePrimerFirmante: AnsiString;
            NombreSegundoFirmante: AnsiString;
            NombreTercerFirmante: AnsiString;
            AutorizacionRecepcionFacturasMail: boolean;
            var CantDocumentacion: Integer;
            Modo: TStateConvenio = scAlta;
            EsConvenioPuro : Boolean = True): Boolean;

    property DocumentacionPresentada: RegDocumentacionPresentada read GetDocumentacionPresentada;
    property TienePAT: boolean read FTienePAT write SetTienePAT;

    class function BorrarRegistro(Var Registro:RegDocumentacionPresentada;Patente,TipoPatente:String):Boolean; overload;
    class function BorrarRegistro(Var Registro:RegDocumentacionPresentada;CodigoDocumentacionRespaldo:Integer):Boolean; overload;
    class function BorrarRegistro(Var ArregloTipoOS: ATOrdenServicio;CodigoDocumentacionRespaldo:Integer):Boolean; overload;
    class function BorrarRegistroRepresentantes(Var Registro:RegDocumentacionPresentada; NumeroRepresentanteLegal: TRepresentante):Boolean;
    class procedure ArmarArregloDocumentacion(Var Registro:RegDocumentacionPresentada;ArregloTipoOS: ATOrdenServicio);
    class function ExisteCodigoDocumentoRespaldo(Registro: RegDocumentacionPresentada; CodigoDocumentacionRespaldo: integer; IndiceVehiculo: shortInt): integer;
  end;

var

  formDocPresentada: TformDocPresentada;

const
    MARGENTOP=17;
    MARGENLEFT=10;


implementation

{$R *.dfm}

{ TformDocPresentada }

function TformDocPresentada.Inicializar(Caption: TCaption; Vehiculos:TClientDataSet;
            DocumentacionPresentada: RegDocumentacionPresentada;
            Personeria: char;
            MedioDePago : TTiposMediosPago;
            NombrePrimerFirmante: AnsiString;
            NombreSegundoFirmante: AnsiString;
            NombreTercerFirmante: AnsiString;
            AutorizacionRecepcionFacturasMail: boolean;
            var CantDocumentacion: Integer;
            Modo: TStateConvenio = scAlta;
            EsConvenioPuro : Boolean = True): Boolean;

    function DameExplicacion(Explicacion, ExplicacionDefault: AnsiString): AnsiString;
    var s: AnsiString;
    begin
        s := '';
        //if Trim(Explicacion)  <> '' then s:= Explicacion
        //else s := ExplicacionDefault;
        if RightStr(trim(ExplicacionDefault),1) <> '.' then
            ExplicacionDefault := Trim(ExplicacionDefault)+'. ';
        result := ExplicacionDefault + Explicacion;
    end;
var
    i, codigo:Integer;
    aux: TBookmark;
    PedirDocumento: boolean;
    MasInfoDescrip,MasInfo: AnsiString;
    Explicacion: AnsiString;

begin

    Explicacion := '';
    self.Caption := Caption;
    MasInfo := '';
    try
        try
            if Vehiculos.RecordCount > 0 then
                FCodigoTipoOrdenServicio := TOperacionConvenio[tocALTA_CONVENIO].CodigoTipoOperacionConvenio
            else FCodigoTipoOrdenServicio := TOperacionConvenio[tocALTA_CONVENIO_SIN_CUENTA].CodigoTipoOperacionConvenio;
            SetLength(RegDocInterno,0);
            IndiceVehiculo:=0;
            SP.ProcedureName := 'ObtenerDocumentacionxTipoOperacionConvenio';
            SP.Parameters.Refresh;
            sp.Parameters.ParamByName('@Personeria').Value := Personeria;
            sp.Parameters.ParamByName('@Tipo').Value := 'C'; //cambiar por constante de convenio
            sp.Parameters.ParamByName('@CodigoTipoOperacionConvenio').Value := FCodigoTipoOrdenServicio;

            sp.Open;

            i:=0;
            while not(sp.Eof) do begin
                //Aca me fijo: si tiene representante y es la documentacion del representante
                //lo env�o.
                MasInfo:='';
                MasInfoDescrip:='';
                codigo := sp.Fieldbyname('CodigoDocumentacionRespaldo').AsInteger;
                MasInfo := '';

                if (codigo = CODIGO_DOC_MANDATO_PAC)  then
                    PedirDocumento := MedioDePago = PAC
                else begin

                    if (codigo = CODIGO_DOC_TARJETA_CREDITO) or (codigo = CODIGO_DOC_MANDATO_PAT) then begin
                        PedirDocumento := MedioDePago = PAT;
                    end else begin

                        if  ((codigo = CODIGO_DOCUMENTACION_PRIMER_FIRMANTE) or
                            (codigo = CODIGO_DOCUMENTACION_DOMICILIO_PRIMER_FIRMANTE) or
                            (codigo = CODIGO_DOCUMENTACION_CONSTANCIA_RUT_PRIMER_FIRMANTE) or
                            (codigo = CODIGO_DOCUMENTACION_PODER_PRIMER_FIRMANTE)) then begin
                            PedirDocumento := trim(NombrePrimerFirmante) <> '';
                            MasInfoDescrip:=' - '+NombrePrimerFirmante;
                            MasInfo := trim(NombrePrimerFirmante);
                        end else begin
                            if  ((codigo = CODIGO_DOCUMENTACION_SEGUNDO_FIRMANTE) or
                                (codigo = CODIGO_DOCUMENTACION_DOMICILIO_SEGUNDO_FIRMANTE) or
                                (codigo = CODIGO_DOCUMENTACION_CONSTANCIA_RUT_SEGUNDO_FIRMANTE) or
                                (codigo = CODIGO_DOCUMENTACION_PODER_SEGUNDO_FIRMANTE)) then begin
                                PedirDocumento := trim(NombreSegundoFirmante) <> '';
                                MasInfoDescrip:=' - '+NombreSegundoFirmante;
                                MasInfo := trim(NombreSegundoFirmante);
                            end else begin
                                if  ((codigo = CODIGO_DOCUMENTACION_TERCER_FIRMANTE) or
                                    (codigo = CODIGO_DOCUMENTACION_DOMICILIO_TERCER_FIRMANTE) or
                                    (codigo = CODIGO_DOCUMENTACION_CONSTANCIA_RUT_TERCER_FIRMANTE) or
                                    (codigo = CODIGO_DOCUMENTACION_PODER_TERCER_FIRMANTE)) then begin
                                    PedirDocumento := trim(NombreTercerFirmante) <> '';
                                    MasInfoDescrip:=' - '+NombreTercerFirmante;
                                    MasInfo := trim(NombreTercerFirmante);
                                end else begin
                                    if Codigo=CODIGO_DOCUMENTACION_RECEPCION_DOC_EMAIL then PedirDocumento:=AutorizacionRecepcionFacturasMail
                                    else PedirDocumento := true;
                                end;
                            end;
                        end;
                    end;
                end;


                Explicacion := '';

                if PedirDocumento then begin // Si el Documento hay que Guardarlo, entro
                    if sp.Fieldbyname('SoloImpresion').AsInteger <> 1 then begin //Entro si se tiene que mostrar
                        Explicacion := DameExplicacion(iif(sp.Fieldbyname('Explicacion').IsNull,'', sp.Fieldbyname('Explicacion').AsString),
                                                        iif(sp.Fieldbyname('ExplicacionDefault').IsNull,'', sp.Fieldbyname('ExplicacionDefault').AsString));
                        i := MostrarDocumentacion(sb_Convenio, i,
                                                sp.Fieldbyname('Descripcion').AsString+MasInfoDescrip,
                                                MasInfo,
                                                sp.Fieldbyname('Obligatorio').AsBoolean,
                                                sp.Fieldbyname('CodigoDocumentacionRespaldo').AsInteger,0,
                                                CargadoAntes(DocumentacionPresentada, sp.Fieldbyname('CodigoDocumentacionRespaldo').AsInteger, 0),
                                                trim(Explicacion));
                    end;

                    SetLength(RegDocInterno,length(RegDocInterno)+1);
                    RegDocInterno[length(RegDocInterno)-1].CodigoDocumentacionRespaldo:=sp.Fieldbyname('CodigoDocumentacionRespaldo').AsInteger;
                    RegDocInterno[length(RegDocInterno)-1].IndiceVehiculoRelativo:=IndiceVehiculo;
                    RegDocInterno[length(RegDocInterno)-1].TipoPatente:='';
                    RegDocInterno[length(RegDocInterno)-1].Patente:='';
                    RegDocInterno[length(RegDocInterno)-1].NoCorresponde:=False;
                    RegDocInterno[length(RegDocInterno)-1].InfoAdicional:=MasInfo;
                    RegDocInterno[length(RegDocInterno)-1].SoloImpresion := sp.Fieldbyname('SoloImpresion').AsInteger;;
                end;

                sp.next;
            end;
            sp.close;

            with Vehiculos do begin
                aux:=GetBookmark;
                First;
                SP.ProcedureName := 'ObtenerDocumentacionxTipoOperacionConvenio';
                SP.Parameters.Refresh;
                sp.Parameters.ParamByName('@Personeria').Value := Personeria;
                sp.Parameters.ParamByName('@Tipo').Value := 'V'; //cambiar por constante de convenio
                sp.Parameters.ParamByName('@CodigoTipoOperacionConvenio').Value := FCodigoTipoOrdenServicio;

                while not(eof) do begin

                    if (EsConvenioPuro) then begin

                        sp.close;
                        sp.Open;
                        indiceVehiculo:=indiceVehiculo+1;
                        MasInfo:='';
                        while not(sp.Eof) do begin
                            if sp.Fieldbyname('SoloImpresion').AsInteger <> 1 then begin
                                Explicacion := '';
                                Explicacion := DameExplicacion(iif(sp.Fieldbyname('Explicacion').IsNull,'', sp.Fieldbyname('Explicacion').AsString),
                                                                iif(sp.Fieldbyname('ExplicacionDefault').IsNull,'', sp.Fieldbyname('ExplicacionDefault').AsString));
                                i:=MostrarDocumentacion(sb_Convenio,i,
                                                        sp.Fieldbyname('Descripcion').AsString+' del veh�culo '+Fieldbyname('Patente').AsString,
                                                        '',//mas info
                                                        sp.Fieldbyname('Obligatorio').AsBoolean,
                                                        sp.Fieldbyname('CodigoDocumentacionRespaldo').AsInteger,
                                                        indiceVehiculo,
                                                        CargadoAntes(DocumentacionPresentada, sp.Fieldbyname('CodigoDocumentacionRespaldo').AsInteger, indiceVehiculo),
                                                        trim(Explicacion));

                                SetLength(RegDocInterno,length(RegDocInterno)+1);
                                RegDocInterno[length(RegDocInterno)-1].CodigoDocumentacionRespaldo:=sp.Fieldbyname('CodigoDocumentacionRespaldo').AsInteger;
                                RegDocInterno[length(RegDocInterno)-1].IndiceVehiculoRelativo:=indiceVehiculo;
                                RegDocInterno[length(RegDocInterno)-1].TipoPatente:=Fieldbyname('TipoPatente').AsString;
                                RegDocInterno[length(RegDocInterno)-1].Patente:=Fieldbyname('Patente').AsString;
                                RegDocInterno[length(RegDocInterno)-1].NoCorresponde:=False;
                                RegDocInterno[length(RegDocInterno)-1].InfoAdicional:=MasInfo;
                            end;
                            sp.next;
                        end;
                    end;
                    Next;
                end;
                GotoBookmark(aux);
                FreeBookmark(aux);
            end;

            CantDocumentacion := Length(RegDocInterno);
            result:=True;
        except
            result:=false;;
        end;

    finally
        sp.close;
    end;

    if Modo = scNormal then begin
        EnableControlsInContainer(self,False);
        Habilitar(sb_Convenio);
        Habilitar(btn_Cancelar);
    end;
end;

function TformDocPresentada.MostrarDocumentacion(
        Panel:TScrollBox;
        MargenSuperior: Integer;
        Descripcion, MasInformacion: String;
        Obligatorio: Boolean;
        CodigoDocumentacionRespaldo:Integer;
        IndiceVehiculo:Shortint; Check:byte;
        Explicacion: AnsiString = ''):Integer;
var
    CheckBoxAux, CheckBoxAuxNoObligatorio:TCheckBox;
    LabelExplicacion: TLabel;
    auxWidth,topLabel, heigthLabel: integer;
begin
    heigthLabel := 0;
    CheckBoxAux:=TCheckBox.Create(panel);
    with CheckBoxAux do begin //principal
        Parent:=Panel;
        top:=MargenSuperior+MARGENTOP;
        left:=MARGENLEFT;
        font.Style:=[];
        Alignment:= taLeftJustify;
        Caption:=STR_NO_CORRESPONDE; // trucada para que me de la long de un combo con este texto
        auxWidth:=Panel.Width-(5*MARGENLEFT)-Width;
        Caption:=Descripcion;
        Width:=auxWidth;        
        Hint := Descripcion + IIf(trim(MasInformacion) <>'', #13#10 + MasInformacion, '');
        ShowHint := true;
        Cursor:=crHandPoint;
        OnClick:=OnClickCombo;
        tag:=GrabarTag(CodigoDocumentacionRespaldo,IndiceVehiculo,false);
        font.Color:=clRed;
        name:='Check'+inttostr(panel.ComponentCount);
        Checked := Check=1;
    end;
    if not(Obligatorio) then begin // check "no corresponde"
        CheckBoxAuxNoObligatorio:= TCheckBox.Create(panel);
        with CheckBoxAuxNoObligatorio do begin
            parent:=Panel;
            left:=CheckBoxAux.Left+CheckBoxAux.Width+MARGENLEFT;
            top:=MargenSuperior+MARGENTOP;
            Caption:=STR_NO_CORRESPONDE;
            Alignment:=taLeftJustify;
            Cursor:=crHandPoint;
            OnClick:=OnClickCombo;
            tag:=GrabarTag(CodigoDocumentacionRespaldo,IndiceVehiculo,true);
            font.Color:=clRed;
            name:='Check'+inttostr(panel.ComponentCount);
            Checked := Check=2;
            if Checked then BuscarComponente(CheckBoxAuxNoObligatorio).font.Color:= clWindowText;
       end;
    end;
    topLabel := CheckBoxAux.Top + CheckBoxAux.Height;
    if trim(Explicacion) <> '' then begin //explicacion
        LabelExplicacion := TLabel.Create(panel);
        with LabelExplicacion do
        begin
            Parent:=Panel;
            top:= topLabel + 1;
            left:=MARGENLEFT;
            font.Style:=[];
            Alignment:= taLeftJustify;
            Caption:=Explicacion; // trucada para que me de la long de un combo con este texto
            Width:= CheckBoxAux.Width - 15;
            AutoSize := false;
            WordWrap := true;
            name:='lbl'+inttostr(panel.ComponentCount);
            topLabel := LabelExplicacion.Top;
            heigthLabel := CalculaHeightLabel(CheckBoxAux.Width,Explicacion);
            Height := heigthLabel;
        end;
    end;
    if not(Obligatorio) and (CheckBoxAux.Checked) then BuscarComponente(CheckBoxAux).font.Color:= clWindowText;

    if (trim(Explicacion) = '') then
        result:= MargenSuperior + (2*MARGENTOP)
    else
        result:= topLabel + heigthLabel;
end;

procedure TformDocPresentada.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action :=  caFree;
end;


procedure TformDocPresentada.OnClickCombo(Sender: TObject);
var
    auxColor:TColor;
begin
    with TCheckBox(sender) do begin
        if Checked then begin
            auxColor:=clWindowText;
        end else begin
            auxColor:=clRed;
        end;
        if BuscarComponente(TCheckBox(sender))<>nil then begin
            if Checked then
                BuscarComponente(TCheckBox(sender)).Checked:=not(Checked);
            BuscarComponente(TCheckBox(sender)).font.Color:=auxColor;
        end;
        TCheckBox(sender).Font.Color:=auxColor;
    end;

end;

function TformDocPresentada.BuscarComponente(Original:TCheckBox): TCheckBox;
var
    CodOriginal,CodDestino,i:Integer;
    IndOriginal,IndDestino:Shortint;
begin
    result:=nil;
    for i:=0 to Original.Parent.ComponentCount-1 do begin
        if (Original.Parent.Components[i].ClassType=TCheckBox) and
            (Original.Parent.Components[i].Name<>Original.Name) then begin
                ObtenerTag(Original.Parent.Components[i].Tag,CodOriginal,IndOriginal);
                ObtenerTag(Original.Tag,CodDestino,IndDestino);
                if (CodOriginal=CodDestino) and (IndOriginal=IndDestino) then
                    result:=TCheckBox(Original.Parent.Components[i]);
        end;
    end;
end;


function TformDocPresentada.GrabarTag(CodigoDocumentacionRespaldo: Integer;
  Indice:Shortint;Optativo:Boolean): Longint;
begin
    result:=(CodigoDocumentacionRespaldo*10000)+(Indice*10)+iif(Optativo,1,0);
end;

{******************************** Function Header ******************************
Function Name: TformDocPresentada.btn_okClick
Author       : dcalani
Date Created : 14/05/2004
Description  : Optiene El CodigoDocumentacionRespaldo y el indice del vehiculo codificado
               en el tag del objeto, si el objeto es "optativo" devuelve true else false
Parameters   : Sender: TObject
Return Value : None
*******************************************************************************}
function TformDocPresentada.ObtenerTag(Tag: Longint;
  var CodigoDocumentacionRespaldo: Integer; var Indice:Shortint):boolean;
begin
    CodigoDocumentacionRespaldo:=tag div 10000;
    Indice:=((tag - (CodigoDocumentacionRespaldo*10000)) div 10);
    result:=tag-(CodigoDocumentacionRespaldo*10000)-(indice*10)=1;
end;

procedure TformDocPresentada.btn_okClick(Sender: TObject);
var
    i,CodigoDocumentacionRespaldo:Integer;
    Indice:Shortint;
begin
    // verifico si quedo algo sin controlar
    for i := 0 to sb_Convenio.ComponentCount - 1 do begin
        if (sb_Convenio.Components[i].ClassType = TCheckBox) and
            not(ObtenerTag(TCheckBox(sb_Convenio.Components[i]).tag,CodigoDocumentacionRespaldo,Indice)) and
            not(TCheckBox(sb_Convenio.Components[i]).Checked) and
            ( (BuscarComponente(TCheckBox(sb_Convenio.Components[i])) = nil) or not(BuscarComponente(TCheckBox(sb_Convenio.Components[i])).Checked) )
        then begin
                MsgBoxBalloon(MSG_ERROR_DOCUMENTACION,MSG_CAPTION_VALIDAR_DOCUMENTACION,MB_ICONSTOP,TCheckBox(sb_Convenio.Components[i]));
                exit;
        end;
    end;
    // esta todo ok
    ModalResult := mrOk;
end;

procedure TformDocPresentada.btn_CancelarClick(Sender: TObject);
begin
    ModalResult:=mrCancel;
end;

function TformDocPresentada.GetDocumentacionPresentada: RegDocumentacionPresentada;

    function ObtenerIndice(Indice: Integer):Integer;
    resourcestring
        MSG_INVALID_INDEX = 'Error, No se encontro el indice en el RegInterno';
    var
        aux,i: Integer;
    begin
        aux := 0;
        Result := -1;
        i := 0;
        while (i <= High(RegDocInterno)) and (Result = -1) do begin
            if (RegDocInterno[i].SoloImpresion <> 1) then begin
                if (aux + 1 = Indice) then Result := i
                else inc(aux);
            end;
            inc(i);
        end;

        if Result = -1 then raise Exception.Create(MSG_INVALID_INDEX);
    end;

var
    i,CodigoDocumentacionRespaldo, j:Integer;
    Indice:Shortint;
    aux: Integer;
begin
    j:= 1;
    for i:=0 to sb_Convenio.ComponentCount-1 do begin
        if (sb_Convenio.Components[i].ClassType=TCheckBox) and
            not(ObtenerTag(TCheckBox(sb_Convenio.Components[i]).Tag,CodigoDocumentacionRespaldo,Indice)) then begin
                aux := ObtenerIndice(j);
                RegDocInterno[aux].NoCorresponde := not(TCheckBox(sb_Convenio.Components[i]).Checked);
                j:= j + 1;
        end;
    end;

   result := RegDocInterno;
end;

function TformDocPresentada.CargadoAntes(
  DocumentacionPresentada: RegDocumentacionPresentada;
  CodigoDocumentacionRespaldo: integer; indice:integer): byte;
var i: integer;
begin
    //Devuelve 1 esta verificado, 2 si No corresponde y 3 si no esta
    i:= 0;
    while (i <= Length(DocumentacionPresentada)-1) and
         ((DocumentacionPresentada[i].CodigoDocumentacionRespaldo <> CodigoDocumentacionRespaldo) or
        (indice <> DocumentacionPresentada[i].IndiceVehiculoRelativo)) do
        	i:= i + 1;

    if (i <= Length(DocumentacionPresentada)-1) then begin
        if DocumentacionPresentada[i].NoCorresponde then result := 2
        else result := 1;
    end else result := 3;
end;

procedure TformDocPresentada.SetTienePAT(const Value: boolean);
begin
  FTienePAT := Value;
end;

function TformDocPresentada.CalculaHeightLabel(
  AnchoLabel:integer;Texto:String): integer; //,lenghtTexto
var
    i,suficiente: integer;
    TlabelAux:Tlabel;
begin
    TlabelAux:=Tlabel.create(nil);
    with TlabelAux do begin
        parent:=self;
        AutoSize:=True;
        Caption:=Texto;
        WordWrap:=False;
        AutoSize:=False;
    end;

    i:=1;
    while TlabelAux.Width>AnchoLabel do begin
        i:=i+1;
        TlabelAux.Width:=TlabelAux.Width-AnchoLabel;
    end;

    suficiente :=16;// 13;
    {Esta funcion depende del width del label, habr�a que buscar
    la manera de calcula cuantos caracteres entran de un determinado
    font (con su tama�o y styles). Si se cambia el witdth, revisarlo esto...
    Tener en cuenta que las propiedades del label cuando se crea:
    Wordwrap = true
    Autosize = false
    }

    Result:=i*suficiente;
    TlabelAux.free;

end;

{******************************** Function Header ******************************
Function Name: TformDocPresentada.BorrarRegistro
Author       : dcalani
Date Created : 03/08/2004
Description  : Devuelve true, si saco algun registro 
Parameters   : var Registro: RegDocumentacionPresentada; Patente, TipoPatente: String
Return Value : Boolean
*******************************************************************************}
class function TformDocPresentada.BorrarRegistro(
  var Registro: RegDocumentacionPresentada; Patente, TipoPatente: String):Boolean;
var
    cantOrg, i:Integer;
    aux: RegDocumentacionPresentada;
begin
    i:=0;
    cantOrg := length(Registro);
    while i <= length(Registro) - 1 do begin
        if (trim(Registro[i].Patente)<>trim(Patente)) or (trim(registro[i].TipoPatente)<>trim(TipoPatente)) then begin
            SetLength(aux,length(aux)+1);
            aux[length(aux)-1]:=Registro[i];
        end;
        i:=i + 1;
    end;
    SetLength(Registro,0);
    SetLength(Registro,length(aux));
    Registro := aux;
    result := cantOrg > length(Registro);
end;

class function TformDocPresentada.BorrarRegistro(
  var Registro: RegDocumentacionPresentada;
  CodigoDocumentacionRespaldo: Integer):Boolean;
var
    i:Integer;
    aux:RegDocumentacionPresentada;
begin
    i:=0;
    while i<=length(Registro)-1 do begin
        if (Registro[i].CodigoDocumentacionRespaldo<>CodigoDocumentacionRespaldo) then begin
            SetLength(aux,length(aux)+1);
            aux[length(aux)-1]:=Registro[i];
        end;
        i:=i + 1;
    end;

    SetLength(Registro,0);
    SetLength(Registro,length(aux));
    Registro:=aux;
    result:=True;

end;
class function TformDocPresentada.BorrarRegistro(
    Var ArregloTipoOS:ATOrdenServicio;
    CodigoDocumentacionRespaldo:Integer):Boolean;
var
    i, IndiceTipoOS:Integer;
    aux:RegDocumentacionPresentada;
begin
    for IndiceTipoOS := 0  to  length(ArregloTipoOS)-1 do
    begin
        i:=0;
        while i<=length(ArregloTipoOS)-1 do begin
            if (ArregloTipoOS[IndiceTipoOS].Documentacion[i].CodigoDocumentacionRespaldo<>CodigoDocumentacionRespaldo) then begin
                SetLength(aux,length(aux)+1);
                aux[length(aux)-1] := ArregloTipoOS[IndiceTipoOS].Documentacion[i];
            end;
            i:=i + 1;
        end;
        SetLength(ArregloTipoOS[IndiceTipoOS].Documentacion,0);
        SetLength(ArregloTipoOS[IndiceTipoOS].Documentacion,length(aux));
        ArregloTipoOS[IndiceTipoOS].Documentacion := aux;
    end;
    result:=True;
end;

class function TformDocPresentada.BorrarRegistroRepresentantes(
  var Registro: RegDocumentacionPresentada;
  NumeroRepresentanteLegal: TRepresentante): Boolean;
begin
    case NumeroRepresentanteLegal of
        Primero: begin
                BorrarRegistro(Registro,CODIGO_DOCUMENTACION_PRIMER_FIRMANTE);
                BorrarRegistro(Registro,CODIGO_DOCUMENTACION_DOMICILIO_PRIMER_FIRMANTE);
                BorrarRegistro(Registro,CODIGO_DOCUMENTACION_CONSTANCIA_RUT_PRIMER_FIRMANTE);
                BorrarRegistro(Registro,CODIGO_DOCUMENTACION_PODER_PRIMER_FIRMANTE);
           end;
        Segundo: begin
                BorrarRegistro(Registro,CODIGO_DOCUMENTACION_SEGUNDO_FIRMANTE);
                BorrarRegistro(Registro,CODIGO_DOCUMENTACION_DOMICILIO_SEGUNDO_FIRMANTE);
                BorrarRegistro(Registro,CODIGO_DOCUMENTACION_CONSTANCIA_RUT_SEGUNDO_FIRMANTE);
                BorrarRegistro(Registro,CODIGO_DOCUMENTACION_PODER_SEGUNDO_FIRMANTE);
           end;
        Tercero: begin
                BorrarRegistro(Registro,CODIGO_DOCUMENTACION_TERCER_FIRMANTE);
                BorrarRegistro(Registro,CODIGO_DOCUMENTACION_DOMICILIO_TERCER_FIRMANTE);
                BorrarRegistro(Registro,CODIGO_DOCUMENTACION_CONSTANCIA_RUT_TERCER_FIRMANTE);
                BorrarRegistro(Registro,CODIGO_DOCUMENTACION_PODER_TERCER_FIRMANTE);
           end;

    end;
    result := true;
end;

class procedure TformDocPresentada.ArmarArregloDocumentacion(Var Registro: RegDocumentacionPresentada; ArregloTipoOS: ATOrdenServicio);
var
    i, pos, IndiceTipoOS : integer;
    CodigoDocumentacionRespaldoAux: integer;
    IndiceVehiculoAux: shortint;
begin
    SetLength(Registro,1);
    for IndiceTipoOS := 0 to Length(ArregloTipoOS)-1 do
    begin
        for i := 0 to length(ArregloTipoOS[IndiceTipoOS].Documentacion) - 1 do begin

            CodigoDocumentacionRespaldoAux := ArregloTipoOS[IndiceTipoOS].Documentacion[i].CodigoDocumentacionRespaldo;
            IndiceVehiculoAux := ArregloTipoOS[IndiceTipoOS].Documentacion[i].IndiceVehiculoRelativo;

            pos := ExisteCodigoDocumentoRespaldo(Registro, CodigoDocumentacionRespaldoAux, IndiceVehiculoAux);
            if (pos >= 0) then begin
                //Lo encontro. Asi que dejo los datos Marcado y NoCorresponde en forma correcta
                //Si esta Marcado, no hago nada, lo dejo tal cual!
                if (not Registro[pos].Marcado) then begin
                    if (ArregloTipoOS[IndiceTipoOS].Documentacion[i].Marcado) then begin
                        Registro[pos].Marcado := true;
                        Registro[pos].NoCorresponde := false;
                    end
                    else begin
                        //El original del arreglo no esta marcado como NoCorresponde, pero en otro documento ya lo marc�
                        if  (not Registro[pos].NoCorresponde) and (ArregloTipoOS[IndiceTipoOS].Documentacion[i].NoCorresponde) then begin
                            Registro[pos].NoCorresponde := true;
                        end;
                    end;
                end;
            end
            else begin
                //No lo encontro, lo inserto!
                SetLength(Registro,length(Registro)+1);
                Registro[length(Registro)-1] := ArregloTipoOS[IndiceTipoOS].Documentacion[i];
            end;
        end;
    end;
end;

procedure TformDocPresentada.Habilitar(Control: TWinControl);
begin
    if Control <> nil then Begin
        Control.Enabled := True;
        Habilitar(Control.Parent);
    end;
end;

class function TformDocPresentada.ExisteCodigoDocumentoRespaldo(
  Registro: RegDocumentacionPresentada;
  CodigoDocumentacionRespaldo: integer; IndiceVehiculo: shortInt): integer;
var
    i: integer;
    Encontro: boolean;
begin
    (*Esta funcion se fija si Existe el documento en el arreglo que se esta
    armando. En caso de encontrarlo devuelve la posici�n. Caso contrario
    retorna -1 *)
    i := 0;
    Encontro := false;
    while (i <= Length(Registro)-1) and (not Encontro) do
    begin
        if  (CodigoDocumentacionRespaldo = Registro[i].CodigoDocumentacionRespaldo) and
            (IndiceVehiculo = Registro[i].IndiceVehiculoRelativo) then begin
            Encontro := true;
        end
        else Inc(i);
    end;
    if Encontro then result := i
    else result := -1;
end;

end.

