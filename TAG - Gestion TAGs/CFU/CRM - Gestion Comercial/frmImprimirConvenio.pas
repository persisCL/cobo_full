{-------------------------------------------------------------------------------
 File Name: frmImprimirConvenio.pas
 Author:
 Date Created:
 Language: ES-AR
 Description: CAC

 Revision 1:
    Author : jconcheyro
    Date : 04/05/2007
    Description : en la funci�n ImprimirConvenio que se usa en el alta de nuevo
    convenio, agrego que se imprima el documento para motos si tiene alguna

Revision 2:
    Author: nefernandez
    Date: 10/05/2007
    Description: En la funci�n ImprimirAnexoVehiculos, que es llamado cuando el
    reporte tiene m�s de 1 p�gina, recorre la lista de vehiculos que falta
    imprimir y va llamando la funci�n ImprimirAnexoVehiculosGeneral (impresi�n
    de p�ginas restantes del reporte). Anteriormente hab�a llamadas de
    recursividad entre ambas funciones, que generaba un "Stack Overflow".
Revision 3 :
   Author:    lcanteros
   Date:      07/Ago/2008
   Description: Modificaciones (SS 725) en el metodo ImprimirMandatoPAC


Revision 4
Author: mbecerra
Date: 24-Dic-2008
Description:	(Ref SS 769) Se modifica para imprimir los anexos 3 y 4. Se agrega
				la funci�n: ImprimirAnexo3o4

                Adem�s se crean dos nuevos tipos: TDatosAnexo3 y TDatosAnexo4
                que contienen los datos para la impresi�n, ya que esta funci�n
                es invocada desde frmModificacionConvenio y frmSolicitudContacto

Revision : 5
    Date: 03/03/2009
    Author: pdominguez
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
                  los bloqueos de tablas en la lectura
Revision : 6
    Author: pdominguez
    Date: 18/03/2009
    Description: SS 733
        - Se a�ade la opci�n Packed a los tipos TCuentaABM, TDatosAnexo3,
        TDatosAnexo4, para bajar el tama�o de los datos almacenados en memoria
        por estas estructuras.
        - ImprimirAnexoVehiculos: Se Cambi� la forma del par�metro Veh�culo para pasarlo por referencia.
        - Se a�aden los objetos cdsDatosConvenioCompleto, dsDatosConvenioCompleto,
        DBPipeline1, ppReporteConvenioCompleto, para solventar el bug detectado
        a la hora de generar los reportes de mas de una hoja, los cuales en la
        vista previa y salida a archivo generaban una vista previa y archivo por p�gina.
        - Se modificaron los siguientes procedimientos / funciones:
            ImprimirConvenioPersonaJuridica,
            ImprimirConvenioPersonaNatural,
            ImprimirAnexoVehiculos,
            ImprimirAnexoVehiculosGeneral,
            ImprimirAnexoMotos,
            ImprimirAnexoMotosGeneral,
            ImprimirContratoMoto,
            ImprimirAnexo3o4,
            ImprimirCaratula,
            ImprimirMandatoPAC,
            ImprimirMandatoPAT

Revision : 7
    Author: vpaszkowicz
    Date: 23/07/2009
    Description: Agrego el giro dentro de la car�tula del convenio para las
    personas jur�dicas.

Revision : 8
    Author : pdominguez
    Date   : 06/08/2009
    Description : SS 733
        - Se a�adieron/modificaron los siguientes procedimientos/funciones:
            Inicializar,
            ImprimirCaratulaConvenio,
            ImprimirEstadoConvenio,
            ImprimirAnexoVehiculosGeneral,
            ImprimirConvenioPersonaJuridica,
            ImprimirConvenioPersonaNatural,
            ImprimirAnexo3o4,
            ImprimirMandatoPAC,
            ImprimirContratoMoto,
            ImprimirCaratula,
            ImprimirMandatoPAT,
            ImprimirAnexoMotosGeneral,
            ImprimirContratoMoto,
            ImprimirAnexoMotos,
            ImprimirEtiquetas

Revision : 9
    Author : pdominguez
    Date   : 31/08/2009
    Description : SS 733
        - Se a�adieron/modificaron los siguientes procedimientos/funciones:
            Imprimir,
            DarTextoOrden,
            ImprimirMandatoPAT,
            ImprimirMandatoPAC.

Revision : 10
    Author : jjofre
    Date   : 14/04/2010
    Description : SS 468
        - Se a�adieron/modificaron los siguientes procedimientos/funciones:
            PrepararEstadoConvenioJuridico
            PrepararEstadoConvenioNatural
            ImprimirConvenioPersonaNatural
            ImprimirConvenioPersonaJuridica

Revision : 11
    Author : jjofre
    Date   : 15/04/2010
    Description : SS 468
        - Se a�adieron/modificaron los siguientes procedimientos/funciones:
          PrepararEstadoConvenioJuridico

Revision : 12
    Author : pdominguez
    Date   : 07/06/2010
    Description :  SS 895 - Error Impresi�n Convenio CAC
        - Se modificaron los siguientes procedimientos / funciones:
            CargaplantillaRTF,
            ImprimirConvenioPersonaNatural,
            ImprimirConvenioPersonaJuridica,
            ImprimirAnexo3o4,
            ImprimirAnexoVehiculosGeneral,
            ImprimirAnexoMotosGeneral,
            ImprimirMandatoPAT,
            ImprimirMandatoPAC,
            ImprimirContratoMoto,
            ImprimirCaratula

Revision 13
Author: mbecerra
Date: 07-Enero-2011
Description:        (Ref SS 948)
                Se agregan las etoquetas para que la impresi�n de los valores de
                arriendo en cuotas y postal vengan desde la tabla ConceptosMovimiento

Firma		: SS_973_MBE_20110801
Description	: Se agrega el campo Departamento y DetalleDomicilio a los anexos 3 y 4

Firma       : SS-1006-NDR-20120727
Description : Imprimir si el convenio esta Adherido a PA

Firma       : SS_1069_CQU_20120911
Description : Se agrega firma a los reportes ppRptEstadoConvenioNatural y ppRptEstadoConvenioJuridico
              y se modifica la cantidad de registros a desplegar en la primera hoja,
              ahora se obtendr� ese valor desde la Base de Datos.

Firma       : SS_660_MVI_20130909
Description : Se agrega lblListaAmarilla para identificar si el convenio a imprimir est�
              en lista amarilla.
Firma       : SS_660_NDR_20131112
Descripcion : Imprimir etiqueta de lista amarilla en los anexos

Revision 14
Author: ebaeza
Date: 14/03/2014
Firma: SS_1171_EBA_20140311
Descriptio: SS_1171
        funciones / procedures / structuras / constantes involucradas
        ImprimirAnexo3o4
        TDatosAnexo3

      -.Se agregan dos campos "RazonSocial" y "RutRazonSocial" a la estructura
         TDatosAnexo3

      -.Se agrega el par�metro Personeria de tipo string, este par�metro permitir� diferenciar cual
        anexo se imprimir� si el par�metro es "J", significa que se est� imprimiendo el anexo llamado
        Anexo III Jur�dica

      -.Se agregan dos constantes "TAG_3_RAZON_SOCIAL", "TAG_3_RUT_RAZON_SOCIAL"
        para la impresion del anexo III juridico

Firma       : SS_1147_MCA_20150420
Descripcion : se agrega copia para vespucio sur, mensaje segun concesionaria nativa.

Firma       : SS_1147_NDR_20150514
Descripcion : Se elimina el prefijo '0' en el convenio, en la impresion del mandato PAC

Firma       : SS_1380_MCA_20150911
Descripcion : se comenta las linea que hacen referencia a la impresion de la caratula

Firma       : SS_1380_NDR_20151009
Descripcion : Se repone la impresion de caratula

Etiqueta    : 20160315 MGO
Descripci�n : Se utiliza InstallIni en vez de ApplicationIni

-------------------------------------------------------------------------------}
unit frmImprimirConvenio;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,utilhttp,Peatypes, frmDatoPesona,util,peaprocs,DMConnection,UtilDB,
  UtilRB, ppComm, ppRelatv, ppProd, ppClass, ppReport, ComCtrls,printers,frmDocPresentada, StrUtils,
  ppPrnabl, ppStrtch, ppRichTx, ppParameter, ppCache, ppBands,ConstParametrosGenerales,UtilProc,RStrings,
  DB, ADODB,FreTelefono, RBSetup, Comm, Convenios, jpeg, ppCtrls, ComObj, ppDB, ppDBPipe, DBClient, ppPageBreak, ppBarCod, myChkBox,
  ppDBBDE, ppSubRpt, ppMemo, ppVar, LMDPNGImage, DateUtils, SysUtilsCN;

const
	CANT_CARACTERES_MODELO = 16;
    CANTIDAD_CARACTERES_TIPO = 13;
    TAG_NUMERO_CONVENIO = '#NUMEROCONVENIO';

	INIT = #$1B#$40;
	EMP_ON  = #$1B#$45;
	EMP_OFF = #$1B#$46;
	CUT = #$1B#$69;
	FEED = #$0A;
	ALIN = #$1B#$61;
	MODE = #$1B#$21;
	ASK_STATUS = #$04;
    FONT_CODE_BAR = 'Free 3 of 9 Extended';
    FONT_MARCA = 'Wingdings';
    CARACTER_MARCA_CHECK = 'o';

    CM_TOP_MARGIN_FORM_PRE_IMPRESO = 4;
    CM_TOP_MARGIN_FORM_HOJA_BLANCA = 4;

type
  TTipoMov = (ttmAlta, ttmBaja, ttmModif, ttmModifTag, ttmRobado, ttmBajaSinTag,
              ttmVigente, ttmSuspendido, ttmActivado, ttmTagPerdido);
  TTipoAlign = (taIzquierda, taCentro, taDerecha);
  TModo = (tmNormal, tmNegrita, tmDoble, tmDobleNegrito);
// SS 733
  TCuentaABM = packed record
        TipoMovimiento : TTipoMov;
        Cuenta : TCuentaVehiculo;
  end;

  TRegCuentaABM = array of TCuentaABM;

  //Rev 4
  // SS 733
  TDatosAnexo3 = packed record
    APaterno,
    Amaterno,
    Nombre,
    Cedula,
    Televia,
    DomicilioCalle,
    DomicilioNumero,
    DomicilioDetalle,				//SS_973_MBE_20110801
    DomicilioDepto,					//SS_973_MBE_20110801
    DomicilioComuna : string;
    RazonSocial: string;      // SS_1171_EBA_20140311
    RutRazonSocial: string;   // SS_1171_EBA_20140311
  end;

  // SS 733
  TDatosAnexo4 = packed record
    RepAPaterno,
    RepAmaterno,
    RepNombre,
    RepCedula,
    CliCedula,		// SS_948_PDO_20120704
    EMail : string;
  end;

  TFormImprimirConvenio = class(TForm)
	RBInterface: TRBInterface;
    ppReporteImpresionConvenio: TppReport;
    ppDetailBand1: TppDetailBand;
    ppParameterList1: TppParameterList;
	CampoRich: TppRichText;
	RichEdit1: TRichEdit;
    SPort: TSerialPort;
    ObtenerMensajeConvenio: TADOStoredProc;
    spObtenerNumeroAnexo: TADOStoredProc;
    ppReporteImpresionFalabella: TppReport;
    ppDetailBand: TppDetailBand;
    pplblMandanteNombre: TppLabel;
    pplblMandanteDomicilio: TppLabel;
    pplblDuenoVehiculo: TppLabel;
    pplblMandanteRUT: TppLabel;
    pplblMandanteTelefono: TppLabel;
    pplblMandanteNumeroTarjetaCMR: TppLabel;
    pplblPatente1: TppLabel;
    pplblPatente2: TppLabel;
    pplblPatente3: TppLabel;
    pplblDia: TppLabel;
    pplblMes: TppLabel;
    pplblAnio: TppLabel;
    pplblMandanteEmail: TppLabel;
    ppLabel1: TppLabel;
    ppLabel: TppLabel;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppLabel5: TppLabel;
    ppLabel6: TppLabel;
    ppLabel7: TppLabel;
    ppLine1: TppLine;
    ppLabel8: TppLabel;
    ppLabel9: TppLabel;
    ppLabel10: TppLabel;
    ppLabel11: TppLabel;
    ppLabel12: TppLabel;
    ppLabel13: TppLabel;
    ppLabel14: TppLabel;
    ppLabel15: TppLabel;
    ppLabel16: TppLabel;
    ppLabel17: TppLabel;
    ppLabel18: TppLabel;
    ppLabel19: TppLabel;
    ppLine2: TppLine;
    ppRichText: TppRichText;
    ppShape1: TppShape;
    ppShape2: TppShape;
    ppShape3: TppShape;
    ppShape4: TppShape;
    ppShape5: TppShape;
    ppShape6: TppShape;
    ppShape7: TppShape;
    ppShape8: TppShape;
    ppShape9: TppShape;
    ppShape10: TppShape;
    ppShape11: TppShape;
    ppShape12: TppShape;
    ppShape13: TppShape;
    pplblLeyendaMasVehiculos: TppLabel;
    ppLineDireccion: TppLine;
    ppLineaMail: TppLine;
    RTCopia: TppRichText;
    ppLine5: TppLine;
    cdsDatosConvenioCompleto: TClientDataSet;
    cdsDatosConvenioCompletoMiRichEdit: TBlobField;
    ppReporteConvenioCompleto: TppReport;
    ppDetailBand2: TppDetailBand;
    ppParameterList2: TppParameterList;
    ppParameter2: TppParameter;
    ppDBRichText1: TppDBRichText;
    ppDBPipeline1: TppDBPipeline;
    dsDatosConvenioCompleto: TDataSource;
    ppRptCaratulaConvenio: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppDetailBand3: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppBCNumeroConvenioCaratula: TppBarCode;
    ppLabel20: TppLabel;
    pplblTituloCaratulaConvenio: TppLabel;
    ppLabel22: TppLabel;
    ppLabel23: TppLabel;
    ppLabel24: TppLabel;
    ppLine3: TppLine;
    ppLabel25: TppLabel;
    ppLabel26: TppLabel;
    ppLabel27: TppLabel;
    pplblNumeroConvenio: TppLabel;
    pplblNomobreCliente: TppLabel;
    pplblNumeroRUTCliente: TppLabel;
    pplblGiroCliente: TppLabel;
    pplblPuntoEntrega: TppLabel;
    pplblOperador: TppLabel;
    pplblFechaHora: TppLabel;
    ppLine4: TppLine;
    ppLabel35: TppLabel;
    ppLine6: TppLine;
    ppLabel37: TppLabel;
    ppDBText1: TppDBText;
    spObtenerReporteCaratulaConvenio: TADOStoredProc;
    dsObtenerReporteCaratulaConvenio: TDataSource;
    pplblFirmaRevisor: TppLabel;
    pplnFirmaRevisor: TppLine;
    ppShape14: TppShape;
    ppRptEstadoConvenioNatural: TppReport;
    ppDetailBand4: TppDetailBand;
    ppDBPlCaratulaConvenio: TppDBPipeline;
    ppDBPlEstadoConvenio: TppDBPipeline;
    dsObtenerReporteEstadoConvenio: TDataSource;
    spObtenerReporteEstadoConvenio: TADOStoredProc;
    spObtenerReporteEstadoConvenio2: TADOStoredProc;
    dsObtenerReporteEstadoConvenio2: TDataSource;
    ppDBPlEstadoConvenioAnexo: TppDBPipeline;
    ppSubReport1: TppSubReport;
    ppChildReport1: TppChildReport;
    ppDetailBand7: TppDetailBand;
    ppFooterBand5: TppFooterBand;
    ppDBText2: TppDBText;
    ppDBText3: TppDBText;
    ppDBText4: TppDBText;
    ppDBText5: TppDBText;
    ppDBText6: TppDBText;
    ppDBText7: TppDBText;
    ppDBText8: TppDBText;
    ppDBText9: TppDBText;
    ppShape15: TppShape;
    ppShape16: TppShape;
    ppShape17: TppShape;
    ppShape18: TppShape;
    ppShape19: TppShape;
    ppShape20: TppShape;
    ppShape21: TppShape;
    ppShape22: TppShape;
    ppLabel44: TppLabel;
    ppLabel49: TppLabel;
    ppLabel50: TppLabel;
    ppLabel51: TppLabel;
    ppLabel58: TppLabel;
    ppmDeclaracionesNatural: TppMemo;
    ppLabel57: TppLabel;
    ppLabel56: TppLabel;
    ppLabel55: TppLabel;
    ppLabel52: TppLabel;
    ppLabel53: TppLabel;
    ppLabel54: TppLabel;
    ppSubReport2: TppSubReport;
    ppChildReport2: TppChildReport;
    ppDetailBand8: TppDetailBand;
    ppHeaderBand2: TppHeaderBand;
    ppFooterBand2: TppFooterBand;
    ppSystemVariable1: TppSystemVariable;
    ppDBText18: TppDBText;
    ppDBText19: TppDBText;
    ppDBText20: TppDBText;
    ppDBText21: TppDBText;
    ppDBText22: TppDBText;
    ppDBText23: TppDBText;
    ppDBText24: TppDBText;
    ppDBText25: TppDBText;
    ppShape55: TppShape;
    ppShape56: TppShape;
    ppShape57: TppShape;
    ppShape58: TppShape;
    ppShape59: TppShape;
    ppShape60: TppShape;
    ppShape61: TppShape;
    ppShape62: TppShape;
    pplblNumeroConvenioNaturalAnexo: TppLabel;
    ppLabel77: TppLabel;
    ppShape47: TppShape;
    ppLabel78: TppLabel;
    ppShape48: TppShape;
    ppLabel79: TppLabel;
    ppShape49: TppShape;
    ppLabel80: TppLabel;
    ppShape50: TppShape;
    ppLabel81: TppLabel;
    ppShape51: TppShape;
    ppLabel82: TppLabel;
    ppShape52: TppShape;
    ppLabel83: TppLabel;
    ppShape53: TppShape;
    ppLabel84: TppLabel;
    ppShape54: TppShape;
    ppHeaderBand6: TppHeaderBand;
    pplblLugarFechaNatural: TppLabel;
    ppBCNumeroConvenioNatural: TppBarCode;
    pplblNumeroConvenioNatural: TppLabel;
    ppLabel28: TppLabel;
    ppLabel29: TppLabel;
    ppLabel30: TppLabel;
    ppLabel31: TppLabel;
    ppLabel32: TppLabel;
    ppLabel33: TppLabel;
    ppLabel34: TppLabel;
    ppLabel36: TppLabel;
    ppLabel38: TppLabel;
    ppLabel74: TppLabel;
    ppLabel73: TppLabel;
    ppLabel72: TppLabel;
    ppLabel67: TppLabel;
    ppLabel68: TppLabel;
    ppLabel69: TppLabel;
    ppLabel70: TppLabel;
    ppLabel71: TppLabel;
    pplblNombreNatural: TppLabel;
    pplblApellidoPaternoNatural: TppLabel;
    pplblApellidoMaternoNatural: TppLabel;
    pplblNumeroRUTNatural: TppLabel;
    pplblFechaNacimientoNatural: TppLabel;
    pplblTelefono1Natural: TppLabel;
    pplblTelefono2Natural: TppLabel;
    pplblEMailNatural: TppLabel;
    ppLabel42: TppLabel;
    ppLabel21: TppLabel;
    pplblCodigoArea1Natural: TppLabel;
    pplblCodigoArea2Natural: TppLabel;
    ppLabel45: TppLabel;
    ppLabel46: TppLabel;
    pplblNumeroTelefono2Natural: TppLabel;
    pplblNumeroTelefono1Natural: TppLabel;
    ppLabel39: TppLabel;
    pplblDomicilioEnvioFacturacionNatural: TppLabel;
    ppLabel40: TppLabel;
    pplblDetalleNatural: TppLabel;
    ppLabel41: TppLabel;
    pplblCodigoPostalNatural: TppLabel;
    ppLabel47: TppLabel;
    pplblComunaNatural: TppLabel;
    ppLabel48: TppLabel;
    pplblRegionNatural: TppLabel;
    ppLabel43: TppLabel;
    ppShape23: TppShape;
    ppLabel59: TppLabel;
    ppLabel60: TppLabel;
    ppShape24: TppShape;
    ppLabel61: TppLabel;
    ppLabel62: TppLabel;
    ppLabel63: TppLabel;
    ppShape27: TppShape;
    ppLabel64: TppLabel;
    ppShape28: TppShape;
    ppShape29: TppShape;
    ppLabel65: TppLabel;
    ppLabel66: TppLabel;
    ppShape30: TppShape;
    ppShape25: TppShape;
    ppShape26: TppShape;
    pplblLugarFechaCompleto: TppLabel;
    ppbcNumeroConvenioCompleto: TppBarCode;
    pplblNumeroConvenioCompleto: TppLabel;
    pplblAnexoConvenioCompleto: TppLabel;
    ppHeaderBand4: TppHeaderBand;
    ppSystemVariable3: TppSystemVariable;
    ppFooterBand4: TppFooterBand;
    ppsvConvenioCompleto: TppSystemVariable;
    ppReporteConvenioCompleto2: TppReport;
    ppHeaderBand7: TppHeaderBand;
    ppDetailBand6: TppDetailBand;
    ppDBRichText2: TppDBRichText;
    ppParameterList3: TppParameterList;
    ppParameter3: TppParameter;
    ppRptEstadoConvenioJuridico: TppReport;
    ppDetailBand11: TppDetailBand;
    ppSubReport4: TppSubReport;
    ppChildReport4: TppChildReport;
    ppDetailBand12: TppDetailBand;
    ppHeaderBand8: TppHeaderBand;
    ppFooterBand7: TppFooterBand;
    ppSubReport6: TppSubReport;
    ppChildReport6: TppChildReport;
    ppLabel137: TppLabel;
    ppLabel138: TppLabel;
    ppLabel141: TppLabel;
    ppLabel144: TppLabel;
    ppLabel145: TppLabel;
    ppLabel146: TppLabel;
    ppLabel142: TppLabel;
    ppLabel143: TppLabel;
    ppLabel139: TppLabel;
    ppLabel140: TppLabel;
    ppLabel147: TppLabel;
    ppmDeclaracionesJuridico: TppMemo;
    ppSystemVariable4: TppSystemVariable;
    ppDBText10: TppDBText;
    ppDBText11: TppDBText;
    ppDBText12: TppDBText;
    ppDBText13: TppDBText;
    ppDBText14: TppDBText;
    ppDBText15: TppDBText;
    ppDBText16: TppDBText;
    ppDBText17: TppDBText;
    ppShape46: TppShape;
    ppShape45: TppShape;
    ppShape44: TppShape;
    ppShape43: TppShape;
    ppShape42: TppShape;
    ppShape41: TppShape;
    ppShape40: TppShape;
    ppShape39: TppShape;
    pplblLugarFechaJuridico: TppLabel;
    ppbcNumeroConvenioJuridico: TppBarCode;
    pplblNumeroConvenioJuridico: TppLabel;
    ppLabel76: TppLabel;
    ppLabel85: TppLabel;
    ppLabel133: TppLabel;
    pplblNombreJuridico: TppLabel;
    ppLabel86: TppLabel;
    ppLabel132: TppLabel;
    pplblNumeroRUTJuridico: TppLabel;
    ppLabel87: TppLabel;
    ppLabel131: TppLabel;
    pplblNombreR1Juridico: TppLabel;
    ppLabel88: TppLabel;
    ppLabel130: TppLabel;
    pplblNumeroCI1Juridico: TppLabel;
    ppLabel89: TppLabel;
    ppLabel129: TppLabel;
    pplblNombreR2Juridico: TppLabel;
    ppLabel148: TppLabel;
    ppLabel134: TppLabel;
    pplblNumeroCI2Juridico: TppLabel;
    ppLabel149: TppLabel;
    ppLabel135: TppLabel;
    pplblNombreR3Juridico: TppLabel;
    ppLabel150: TppLabel;
    ppLabel136: TppLabel;
    pplblNumeroCI3Juridico: TppLabel;
    ppLabel90: TppLabel;
    ppLabel151: TppLabel;
    pplblTelefono1Juridico: TppLabel;
    ppLabel105: TppLabel;
    pplblCodigoArea1Juridico: TppLabel;
    ppLabel109: TppLabel;
    pplblNumeroTelefono1Juridico: TppLabel;
    pplblNumeroTelefono2Juridico: TppLabel;
    ppLabel110: TppLabel;
    pplblCodigoArea2Juridico: TppLabel;
    ppLabel106: TppLabel;
    pplblTelefono2Juridico: TppLabel;
    ppLabel152: TppLabel;
    ppLabel91: TppLabel;
    ppLabel92: TppLabel;
    ppLabel153: TppLabel;
    pplblEMailJuridico: TppLabel;
    ppLabel93: TppLabel;
    pplblDomicilioEnvioFacturacionJuridico: TppLabel;
    ppLabel94: TppLabel;
    pplblDetalleJuridico: TppLabel;
    ppLabel95: TppLabel;
    pplblCodigoPostalJuridico: TppLabel;
    ppLabel116: TppLabel;
    pplblComunaJuridico: TppLabel;
    ppLabel117: TppLabel;
    pplblRegionJuridico: TppLabel;
    ppLabel120: TppLabel;
    ppLabel121: TppLabel;
    ppLabel122: TppLabel;
    ppLabel123: TppLabel;
    ppLabel124: TppLabel;
    ppLabel125: TppLabel;
    ppLabel126: TppLabel;
    ppLabel127: TppLabel;
    ppLabel128: TppLabel;
    ppShape38: TppShape;
    ppShape37: TppShape;
    ppShape36: TppShape;
    ppShape35: TppShape;
    ppShape34: TppShape;
    ppShape33: TppShape;
    ppShape32: TppShape;
    ppShape31: TppShape;
    ppDetailBand13: TppDetailBand;
    ppDBText30: TppDBText;
    ppDBText31: TppDBText;
    ppDBText32: TppDBText;
    ppDBText33: TppDBText;
    ppDBText29: TppDBText;
    ppDBText28: TppDBText;
    ppDBText27: TppDBText;
    ppDBText26: TppDBText;
    ppShape78: TppShape;
    ppShape77: TppShape;
    ppShape76: TppShape;
    ppShape75: TppShape;
    ppShape74: TppShape;
    ppShape73: TppShape;
    ppShape72: TppShape;
    ppShape71: TppShape;
    ppHeaderBand3: TppHeaderBand;
    pplblNumeroConvenioJuridicoAnexo: TppLabel;
    ppLabel96: TppLabel;
    ppLabel97: TppLabel;
    ppLabel98: TppLabel;
    ppShape68: TppShape;
    ppLabel99: TppLabel;
    ppShape67: TppShape;
    ppLabel100: TppLabel;
    ppShape66: TppShape;
    ppShape69: TppShape;
    ppShape70: TppShape;
    ppLabel101: TppLabel;
    ppShape65: TppShape;
    ppLabel102: TppLabel;
    ppShape64: TppShape;
    ppLabel103: TppLabel;
    ppShape63: TppShape;
    ppFooterBand3: TppFooterBand;
    ppSystemVariable2: TppSystemVariable;
    pplblCopia: TppLabel;
    ppLabel75: TppLabel;                                                        //SS-1006-NDR-20120727
    ppLabel104: TppLabel;                                                       //SS-1006-NDR-20120727
    pplblAdheridoPA: TppLabel;
    ppLabel108: TppLabel;
    pplblAdheridoPAJuridico: TppLabel;                                                  //SS-1006-NDR-20120727
    pplblPieConcesionaria: TppLabel;                                            // SS_1069_CQU_20120911
    pplblPieCliente: TppLabel;                                                  // SS_1069_CQU_20120911
    pplblPieClienteNatural: TppLabel;                                           // SS_1069_CQU_20120911
    pplblPieConcesionariaNatural: TppLabel;                                     // SS_1069_CQU_20120911
    pplblListaAmarillaPersonaNatural: TppLabel;                                 //SS_660_MVI_20130909
    pplblListaAmarillaPersonaJuridica: TppLabel;                                //SS_660_MVI_20130909 
    ppLblListaAmarillaAnexo: TppLabel;                                			//SS_660_NDR_20131112
    spObtenerMaestroConcesionaria: TADOStoredProc;
    dsContratoArrendamientoTAG: TClientDataSet;
    rptBajaContrato: TppReport;
    prm1: TppParameterList;
    pdtlbnd1: TppDetailBand;
    dsAnexoVehiculosContrato: TClientDataSet;
    rptAltaContrato: TppReport;
    prm2: TppParameterList;
    pdtlbnd2: TppDetailBand;
    ContratoAlta: TppSubReport;
    pchldrprt1: TppChildReport;
    ptlbnd1: TppTitleBand;
    pdtlbnd3: TppDetailBand;
    psmrybnd1: TppSummaryBand;
    img1: TppImage;
    plbl1: TppLabel;
    plbl2: TppLabel;
    plbl5: TppLabel;
    pshp16: TppShape;
    plbl7: TppLabel;
    plbl8: TppLabel;
    plbl9: TppLabel;
    pshp18: TppShape;
    pshp19: TppShape;
    pshp20: TppShape;
    pshp21: TppShape;
    pshp22: TppShape;
    pshp23: TppShape;
    plbl33: TppLabel;
    plbl34: TppLabel;
    plbl35: TppLabel;
    NumeroConvenio1: TppLabel;
    Direccion1: TppLabel;
    Telefono1: TppLabel;
    Email1: TppLabel;
    Usuario1: TppLabel;
    Patente1: TppDBText;
    ClaseVehiculo1: TppDBText;
    Marca1: TppDBText;
    Modelo1: TppDBText;
    SerialTAG1: TppDBText;
    FechaInstalacion1: TppDBText;
    ContratoAnexoVehiculos: TppSubReport;
    pchldrprt2: TppChildReport;
    ptlbnd2: TppTitleBand;
    pdtlbnd4: TppDetailBand;
    psmrybnd2: TppSummaryBand;
    img3: TppImage;
    plbl22: TppLabel;
    plbl23: TppLabel;
    NumeroConvenio2: TppLabel;
    pshp30: TppShape;
    pshp31: TppShape;
    pshp32: TppShape;
    pshp33: TppShape;
    pshp34: TppShape;
    pshp35: TppShape;
    Modelo2: TppDBText;
    Marca2: TppDBText;
    Patente2: TppDBText;
    ClaseVehiculo2: TppDBText;
    SerialTAG2: TppDBText;
    FechaInstalacion2: TppDBText;
    plbl30: TppLabel;
    ContratoCaratula: TppSubReport;
    pchldrprt3: TppChildReport;
    phdrbnd2: TppHeaderBand;
    pdtlbnd5: TppDetailBand;
    pftrbnd2: TppFooterBand;
    ppBarCode1: TppBarCode;
    ppLabel182: TppLabel;
    ppLabel184: TppLabel;
    ppLabel185: TppLabel;
    ppLine7: TppLine;
    ppLabel187: TppLabel;
    ppLabel188: TppLabel;
    ppLabel189: TppLabel;
    ppLine8: TppLine;
    ppLabel203: TppLabel;
    Check0: TppShape;
    pln1: TppLine;
    plbl32: TppLabel;
    pln2: TppLine;
    CaratulaNumeroConvenio: TppLabel;
    CaratulaNombreCliente: TppLabel;
    CaratulaRutCliente: TppLabel;
    CaratulaGiroCliente: TppLabel;
    CaratulaPuntoEntrega: TppLabel;
    CaratulaOperador: TppLabel;
    CaratulaFechaHora: TppLabel;
    CaratulaLineaCatarula: TppDBText;
    CaratulaTitulo: TppLabel;
    ContratoBaja: TppSubReport;
    pchldrprt5: TppChildReport;
    ContratoBajaCaratula: TppSubReport;
    pchldrprt4: TppChildReport;
    ContratoBajaAnexo: TppSubReport;
    pchldrprt6: TppChildReport;
    pdtlbnd6: TppDetailBand;
    psmrybnd3: TppSummaryBand;
    ptlbnd4: TppTitleBand;
    pdtlbnd7: TppDetailBand;
    psmrybnd4: TppSummaryBand;
    plblTituloContrato: TppLabel;
    imgLogoConcesionaria: TppImage;
    plbl3: TppLabel;
    ppLabel107: TppLabel;
    ppShape79: TppShape;
    lblNombreCompleto: TppLabel;
    ppLabel113: TppLabel;
    ppLabel114: TppLabel;
    ppLabel115: TppLabel;
    plblOficina: TppLabel;
    ppShape80: TppShape;
    ppShape81: TppShape;
    ppShape82: TppShape;
    ppShape83: TppShape;
    ppShape89: TppShape;
    ppShape90: TppShape;
    ppShape91: TppShape;
    ppLabel158: TppLabel;
    ppLabel159: TppLabel;
    ppLabel160: TppLabel;
    ppLabel161: TppLabel;
    ppLabel162: TppLabel;
    ppLabel163: TppLabel;
    ppShape92: TppShape;
    ppLabel164: TppLabel;
    ppShape93: TppShape;
    ppLabel165: TppLabel;
    ppShape94: TppShape;
    ppLabel166: TppLabel;
    plblNumero: TppLabel;
    BajaNombreCompleto: TppLabel;
    BajaDireccion: TppLabel;
    BajaTelefonoPrincipal: TppLabel;
    BajaEmail: TppLabel;
    BajaOficina: TppLabel;
    ppShape95: TppShape;
    ppShape96: TppShape;
    ppShape97: TppShape;
    ppShape98: TppShape;
    ppShape99: TppShape;
    ppShape100: TppShape;
    ppShape101: TppShape;
    ppShape102: TppShape;
    ppShape103: TppShape;
    ppShape104: TppShape;
    DBCalcNumero: TppDBCalc;
    BajaPatente: TppDBText;
    BajaClaseVehiculo: TppDBText;
    BajaMarca: TppDBText;
    BajaModelo: TppDBText;
    BajaNumeroTAG: TppDBText;
    BajaCondicion: TppDBText;
    BajaMotivoDevolucion: TppDBText;
    BajaEstadoDevolucion: TppDBText;
    BajaModalidadContrato: TppDBText;
    ppShape105: TppShape;
    ppShape106: TppShape;
    ppLabel167: TppLabel;
    ppShape107: TppShape;
    ppLabel168: TppLabel;
    ppShape111: TppShape;
    ppShape110: TppShape;
    ppShape108: TppShape;
    ppShape109: TppShape;
    ppLabel169: TppLabel;
    ppLabel170: TppLabel;
    ppLabel171: TppLabel;
    ppShape112: TppShape;
    ppLabel173: TppLabel;
    ppLabel174: TppLabel;
    ppLabel175: TppLabel;
    ppLabel176: TppLabel;
    ppLabel178: TppLabel;
    phdrbnd1: TppHeaderBand;
    pdtlbnd8: TppDetailBand;
    pftrbnd1: TppFooterBand;
    ppBarCode2: TppBarCode;
    ppLabel111: TppLabel;
    ppLabel118: TppLabel;
    ppLabel119: TppLabel;
    ppLine9: TppLine;
    ppLabel155: TppLabel;
    ppLabel156: TppLabel;
    ppLabel177: TppLabel;
    ppLine10: TppLine;
    ppLabel179: TppLabel;
    ppShape84: TppShape;
    ppLine11: TppLine;
    ppLabel196: TppLabel;
    ppLine12: TppLine;
    ppLabel197: TppLabel;
    ppLabel198: TppLabel;
    ppImage1: TppImage;
    ppLabel199: TppLabel;
    ppShape119: TppShape;
    ppShape120: TppShape;
    ppShape121: TppShape;
    ppShape122: TppShape;
    ppShape123: TppShape;
    ppShape124: TppShape;
    ppShape125: TppShape;
    ppShape126: TppShape;
    ppShape127: TppShape;
    ppShape128: TppShape;
    ppDBCalc1: TppDBCalc;
    ppLabel212: TppLabel;
    BajaCaratulaNumeroConvenio: TppLabel;
    BajaCaratulaNombreCliente: TppLabel;
    BajaCaratulaRutCliente: TppLabel;
    BajaCaratulaGiroCliente: TppLabel;
    BajaCaratulaPuntoEntrega: TppLabel;
    BajaCaratulaOperador: TppLabel;
    BajaCaratulaFechaHora: TppLabel;
    BajaCaratulaLineaCaratula: TppDBText;
    BajaCaratulaTitulo: TppLabel;
    BajaAnexoVehiculosNumeroConvenio: TppLabel;
    BajaAnexoVehiculosPatente: TppDBText;
    BajaAnexoVehiculosClaseVehiculo: TppDBText;
    BajaAnexoVehiculosMarca: TppDBText;
    BajaAnexoVehiculosModelo: TppDBText;
    BajaAnexoVehiculosNumeroTAG: TppDBText;
    BajaAnexoVehiculosCondicion: TppDBText;
    BajaAnexoVehiculosMotivoDevolucion: TppDBText;
    BajaAnexoVehiculosEstadoDevolucion: TppDBText;
    BajaAnexoVehiculosModalidadContrato: TppDBText;
    BajaNumeroConvenio: TppLabel;
    BajaUsuario: TppLabel;
    dsDocumentacion: TClientDataSet;
    dsContratoBajaTAG: TClientDataSet;
    dsAnexoBaja: TClientDataSet;
    AltaLblRut: TppLabel;
    CaratulaLblGiro: TppLabel;
    ptlbnd3: TppTitleBand;
    BajaLblRUT: TppLabel;
    BajaRUT: TppLabel;
    BajaLblGiro: TppLabel;
    BajaGiro: TppLabel;
    BajaCaratulaLblGiro: TppLabel;
    plbl4: TppLabel;
    ContratoLblVersion: TppLabel;
    ContratoMandatoPACPAT: TppSubReport;
    pchldrprt7: TppChildReport;
    pdtlbnd9: TppDetailBand;
    ContratoTextoMandatoPACPAT: TppRichText;
    pgbrk1: TppPageBreak;
    ContratoTextoCondiciones2: TppRichText;
    ContratoCondiciones: TppRichText;
    ContratoRUT: TppLabel;
    ContratoGiro: TppLabel;
    ContratoLblGiro: TppLabel;
    plbl31: TppLabel;
    AnexoAltaVersion: TppLabel;
    prchtxt1: TppRichText;
    rpAltaContratoSuscripcion: TppReport;
    ppParameterList8: TppParameterList;
    ppDetailBand5: TppDetailBand;
    FAltaSuscripcion: TppSubReport;
    pchldrprt8: TppChildReport;
    FContratoSuscripcion: TppSubReport;
    pchldrprt9: TppChildReport;
    ppDetailBand9: TppDetailBand;
    ppDetailBand10: TppDetailBand;
    ppRichText1: TppRichText;
    ppImage2: TppImage;
    ptlbnd6: TppTitleBand;
    ppFooterBand8: TppFooterBand;
    ppImage8: TppImage;
    pshp7: TppShape;
    pshp8: TppShape;
    pshp15: TppShape;
    pshp36: TppShape;
    dbt_PatenteS: TppDBText;
    dbt_ClaseVehiculoS: TppDBText;
    dbt_MarcaS: TppDBText;
    dbt_ModeloS: TppDBText;
    ppHeaderBand5: TppHeaderBand;
    pshp3: TppShape;
    pshp4: TppShape;
    pshp5: TppShape;
    pshp6: TppShape;
    lbl_CodS1: TppLabel;
    lbl_CodS2: TppLabel;
    lbl_CodS6: TppLabel;
    lbl_CodS7: TppLabel;
    ppTitleBand1: TppTitleBand;
    ppFooterBand6: TppFooterBand;
    ppRichText3: TppRichText;
    ppImage4: TppImage;
    ppRichText4: TppRichText;
    lbl_NumeroConvenioS: TppLabel;
    ppImage3: TppImage;
    lbl_plbl36: TppLabel;
    lbl_NombreCliente: TppLabel;
    pln3: TppLine;
    lbl_1: TppLabel;
    pln8: TppLine;
    lbl_RUTS2: TppLabel;
    lbl_plbl47: TppLabel;
    pln7: TppLine;
    lbl_plbl48: TppLabel;
    ppImage5: TppImage;
    lbl_NombreCaptador: TppLabel;
    lbl_RutCaptador: TppLabel;
    pln11: TppLine;
    lbl_FechaS: TppLabel;
    lbl_plbl37: TppLabel;
    pln4: TppLine;
    lbl_plbl38: TppLabel;
    pln5: TppLine;
    lbl_plbl39: TppLabel;
    lbl_plbl40: TppLabel;
    lbl_plbl41: TppLabel;
    pln6: TppLine;
    ppImageImgBoleta: TppImage;
    lbl_plbl43: TppLabel;
    lbl_plbl44: TppLabel;
    ppImageImgPATPAC: TppImage;
    pshp1: TppShape;
    lbl_plbl45: TppLabel;
    lbl_plbl46: TppLabel;
    lbl_plbl49: TppLabel;
    lbl_plbl50: TppLabel;
    pln9: TppLine;
    pln10: TppLine;
    lbl_DireccionS: TppLabel;
    lbl_ComunaS: TppLabel;
    lbl_CodS: TppLabel;
    lbl_NumeroTlfS: TppLabel;
    lbl_CelularS: TppLabel;
    lbl_EmailS: TppLabel;
    ppImageImgOtros: TppImage;
    pshp2: TppShape;
    pshpShpNo: TppShape;
    ppSummaryBand1: TppSummaryBand;
    ContratoAnexoRepresentantesLegales: TppSubReport;
    pchldrprt10: TppChildReport;
    pdtlbnd10: TppDetailBand;
    plblRUTRepresentante: TppLabel;
    plblNombreRepresentante: TppLabel;
    plblTituloRepresentantes: TppLabel;
    ptlbnd5: TppTitleBand;
    ContratoAnexoFormaPago: TppSubReport;
    pchldrprt11: TppChildReport;
    pdtlbnd11: TppDetailBand;
    plbl10: TppLabel;
    pshp17: TppShape;
    plbl11: TppLabel;
    plbl12: TppLabel;
    plbl13: TppLabel;
    plbl14: TppLabel;
    MetodoPago1: TppLabel;
    RUTTitular1: TppLabel;
    NombreTitular1: TppLabel;
    pshp42: TppShape;
    pshp43: TppShape;
    plbl36: TppLabel;
    plbl37: TppLabel;
    plbl38: TppLabel;
    plbl39: TppLabel;
    plbl40: TppLabel;
    plbl41: TppLabel;
    Anexo1RepresentanteNombre: TppDBText;
    Anexo1RepresentanteRUT: TppDBText;
    imgEmisionSi: TppImage;
    imgEmisionNo: TppImage;
    dsRepresentantesLegales: TClientDataSet;
    pshp41: TppShape;
    pshp40: TppShape;
    pshp39: TppShape;
    pshp38: TppShape;
    plbl16: TppLabel;
    psmrybnd5: TppSummaryBand;
    pln12: TppLine;
    pln13: TppLine;
    pshp9: TppShape;
    pshp10: TppShape;
    plblNombreCliente: TppLabel;
    pshp11: TppShape;
    pshp12: TppShape;
    pshp13: TppShape;
    pshp14: TppShape;
    pshp37: TppShape;
    pshp44: TppShape;
    pshp45: TppShape;
    pshp46: TppShape;
    pshp47: TppShape;
    pshp48: TppShape;
    plbl6: TppLabel;
    plbl17: TppLabel;
    plbl18: TppLabel;
    plbl19: TppLabel;
    plbl20: TppLabel;
    plbl21: TppLabel;
    plbl42: TppLabel;
    plbl43: TppLabel;
    plbl44: TppLabel;
    plbl45: TppLabel;
    pshp24: TppShape;
    pshp25: TppShape;
    pshp26: TppShape;
    pshp27: TppShape;
    pshp28: TppShape;
    pshp29: TppShape;
    plbl24: TppLabel;
    plbl25: TppLabel;
    plbl26: TppLabel;
    plbl27: TppLabel;
    plbl28: TppLabel;
    plbl29: TppLabel;                              //SS_1147_NDR_20140710
    procedure FormCreate(Sender: TObject);
    procedure ppReporteImpresionConvenioBeforePrint(Sender: TObject);
    procedure ppFooterBand1BeforePrint(Sender: TObject);
    procedure ppReporteConvenioCompletoBeforePrint(Sender: TObject);
  private
	{ Private declarations }
	Hoja : TTipoHoja;
    FormularioPreImpreso: Boolean;
    FNroAnexoEmitido: Integer;
    FDatosConvenio: TDatosConvenio;

    function EstaFuenteMarcaInstalada:Boolean;
	function ObtenerDescripDocumentacion(DocPresentada:TRegistroDocumentacion):String;
	function DarTextoOrden(NroCopia,CantMaximaCopia:Integer;NombreBanco:String=''):String;
	//function SetImpresora(NroBandeja:Integer):Boolean;
	function Imprimir(TipoHoja : TTipoHoja ; CantCopias : Integer ; ImprimirNroCopia: Boolean = True; NombreBanco : String = ''):Boolean;
	procedure PrintText(Text: AnsiString; Alineacion:TTipoAlign;Modo:TModo);
    function ImprimirAnexoVehiculosGeneral(     Personeria:String;
                                                NumeroConvenio:String;
                                                TipoImpresion: TTipoImpresion;
                                                var Vehiculo:Array of TCuentaABM;
                                                var MotivoCancelacion:TMotivoCancelacion;
                                                CantCopias:Integer;
                                                PathArchivo : String = ''): Boolean;

    function ImprimirAnexoMotosGeneral(NumeroConvenio:String;
                                                CantidadFilasAnexo: integer;
                                                var Vehiculo:Array of TCuentaABM;
                                                var MotivoCancelacion:TMotivoCancelacion;
                                                CantCopias:Integer;
                                                PathArchivo : String = ''): Boolean;


    function FormatearTexto(S:AnsiString; CantidadMaxima: integer):AnsiString;

    procedure ReplaceCodeBar(Tag,NuevoTexto: AnsiString; RichEdit : TppRichText);
    procedure ReplaceMarcaCheck(Tag : AnsiString; RichEdit : TppRichText);
    function ObtenerDescripcionMedioComunicacion(CodigoMedioComunicacion : integer) : string;
    function PonerDescripcionMensajes(const Texto: String; TipoImpresion: TTipoImpresion; NumeroConvenio: String): String;
    procedure SetNroAnexoEmitido(const Value: Integer);
    function CargaPlantillaRTF(Plantilla: String): String;
{INICIO: TASK_011_JMA_20160505}
    function CrearCaratulaConvenio(Cuentas: array of TCuentaVehiculo):Boolean;
    function CrearAltaConvenio(Cuentas: array of TCuentaVehiculo): Boolean;
    function CrearAnexoVehiculosConvenio(Cuentas: array of TCuentaVehiculo): Boolean;
    function CrearBajaConvenio():Boolean;
    function CrearCaratulaBajaConvenio:Boolean;
    function CrearBajaAnexoVehiculosConvenio(): Boolean;
    function GetCodigoMotivoBaja(CodigoMotivo : Integer): Char;
    function GetEstadoConservacionTAG(EstadoConservacionTAG : Integer): SmallInt;
    function GetDescripcionModalidadContrato(CodigoAlmacenDestino : Integer): string;        
{TERMINO: TASK_011_JMA_20160505}
{INICIO:TASK_018_JMA_20160606}
    function CrearMandatoPACPAT():Boolean;
{TERMINO:TASK_018_JMA_20160606}

//INICIO: TASK_036_ECA_20160621
  procedure ObtenerDireccion(CodigoConvenio, CodigoPersona: Integer; DomicilioConvenio: Boolean; out Direccion: string; out Comuna: string);
  function ObtenerTipoPagoConvenio(CodigoConvenio, CodigoPersona: Integer): Integer;
  procedure ObtenerMedioContacto(CodigoPersona: Integer; out CodigoArea: string; out Telefono: string; out Celular: string);
  procedure ObtenerDatosCaptador(CodigoUsuario: string; Out Nombre: string; out NumeroDocumento: string);
//FIN: TASK_036_ECA_20160621
  public

    Comentario0: TppLabel;
	{ Public declarations }
	//procedure prueba;
{INICIO: TASK_011_JMA_20160505}
       function ImprimirBajaConvenio():Boolean;
       function ImprimirAltaConvenio(ImprimirCaratula, ImprimirContrato, ImprimirMandato: boolean; Cuentas: array of TCuentaVehiculo; DatosConvenio: TDatosConvenio = nil): Boolean;
{TERMINO: TASK_011_JMA_20160505}
{INICIO:  TASK_018_JMA_20160606_Impresi�n de Documentos Convenio (Reportes Finales)
	function ImprimirConvenio(NumeroConvenio:String;
                              TipoImpresion: TTipoImpresion;
                              DatoTitular:TDatosPersonales;
                              PrimerMedioComunicacion:TTipoMedioComunicacion;
                              SegundoMedioComunicacion:TTipoMedioComunicacion;
                              DomicilioTitular:TDatosDomicilio;
                              DomicilioFacturacion:TDatosDomicilio;
                              DatosMedioPago:TDatosMediosPago;
                              Vehiculo: TRegCuentaABM;
                              DatoRepLegal1:TDatosPersonales;
                              DatoRepLegal2:TDatosPersonales;
                              DatoRepLegal3:TDatosPersonales;
                              DocPresentada:RegDocumentacionPresentada;
                              RegDatoMandante:TDatosPersonales;
                              EnviaDocumentacionxMail:boolean;
                              Mail:String;
                              Operador : String;
                              PuntoEntrega : String;
                              FechaAlta : TDateTime;
                              var MotivoCancelacion:TMotivoCancelacion;
                              PuntoEntregaCodigo,
                              PuntoVentacodigo: Integer): Boolean;
}
{TERMINO:  TASK_018_JMA_20160606_Impresi�n de Documentos Convenio (Reportes Finales)}

(*	function ImprimirConvenioPersonaNatural(NumeroConvenio:String;
                                            DatoTitular: TDatosPersonales;
                                            PrimerMedioComunicacion:TFrameTelefono;
                                            SegundoMedioComunicacion:TFrameTelefono;
                                            EMail:String;
                                            DomicilioTitular:TDatosDomicilio;
                                            DomicilioFacturacion:TDatosDomicilio;
                                            DatosMedioPago:TDatosMediosPago;
                                            Vehiculo: TRegCuentaABM;
                                            EnviaDocumentacionxMail:Boolean;
                                            FechaAlta : TDateTime;
                                            var MotivoCancelacion:TMotivoCancelacion;
                                            CantCopias:Integer): Boolean; overload;
*)

	function ImprimirConvenioPersonaNatural(NumeroConvenio:String;
                                            TipoImpresion: TTipoImpresion;
                                            DatoTitular: TDatosPersonales;
                                            PrimerMedioComunicacion:TTipoMedioComunicacion;
                                            SegundoMedioComunicacion:TTipoMedioComunicacion;
                                            EMail:String;
                                            DomicilioTitular:TDatosDomicilio;
                                            DomicilioFacturacion:TDatosDomicilio;
                                            DatosMedioPago:TDatosMediosPago;
                                            Vehiculo: TRegCuentaABM;
                                            EnviaDocumentacionxMail:Boolean;
                                            FechaAlta : TDateTime;
                                            var MotivoCancelacion:TMotivoCancelacion;
                                            CantCopias:Integer;
                                            Plantilla:String = PLANTILLA_CONVENIO_PERSONA_NATURAL): Boolean;

	function ImprimirAnexoVehiculos(Personeria:String;
                                            NumeroConvenio:String;
                                            TipoImpresion: TTipoImpresion;
                                            var Vehiculo:Array of TCuentaABM;
                                            var MotivoCancelacion:TMotivoCancelacion;
                                            CantCopias:Integer): Boolean;

    function ImprimirAnexoMotos(Personeria:String;
                                            NumeroConvenio:String;
                                            //TipoImpresion: TTipoImpresion;
                                            var Vehiculo:Array of TCuentaABM;
                                            var MotivoCancelacion:TMotivoCancelacion;
                                            CantCopias:Integer): Boolean;

(*
	function ImprimirConvenioPersonaJuridica(NumeroConvenio:String;
                                            DatoTitular: TDatosPersonales;
											PrimerMedioComunicacion:TFrameTelefono;
                                            SegundoMedioComunicacion:TFrameTelefono;
                                            EMail:String;
                                            DomicilioTitular:TDatosDomicilio;
                                            DomicilioFacturacion:TDatosDomicilio;
                                            DatosMedioPago:TDatosMediosPago;
                                            DatosRepLegal1:TDatosPersonales;
                                            DatosRepLegal2:TDatosPersonales;
                                            DatosRepLegal3:TDatosPersonales;
                                            EnviaDocumentacionxMail:Boolean;
                                            FechaAlta : TDateTime;
                                            var MotivoCancelacion:TMotivoCancelacion;
                                            CantCopias:Integer;
                                            Vehiculo: TRegCuentaABM): Boolean; overload;
                                            *)

	function ImprimirConvenioPersonaJuridica(NumeroConvenio:String;
                                            TipoImpresion: TTipoImpresion;
                                            DatoTitular: TDatosPersonales;
                                            PrimerMedioComunicacion:TTipoMedioComunicacion;
                                            SegundoMedioComunicacion:TTipoMedioComunicacion;
                                            EMail:String;
											DomicilioTitular:TDatosDomicilio;
                                            DomicilioFacturacion:TDatosDomicilio;
                                            DatosMedioPago:TDatosMediosPago;
                                            DatosRepLegal1:TDatosPersonales;
                                            DatosRepLegal2:TDatosPersonales;
                                            DatosRepLegal3:TDatosPersonales;
                                            EnviaDocumentacionxMail:Boolean;
                                            FechaAlta : TDateTime;
                                            var MotivoCancelacion:TMotivoCancelacion;
											CantCopias:Integer;
                                            Vehiculo:TRegCuentaABM;
                                            Plantilla:String = PLANTILLA_CONVENIO_PERSONA_JURIDICA): Boolean;


	function ImprimirMandatoPAT(            NumeroConvenio: String;
                                            DatosMedioPago: TDatosMediosPago;
                                            RegDatoMandante,
                                            DatosTitular: TDatosPersonales;
                                            DomicilioTitular: TDatosDomicilio;
                                            EMailTitular: String;
                                            DatosCuentas: TRegCuentaABM;
                                            var MotivoCancelacion: TMotivoCancelacion;
                                            CantCopias: Integer): Boolean;

	function ImprimirMandatoPAC(NumeroConvenio:String;
                                            DatosMedioPago:TDatosMediosPago;
                                            RegDatoMandante:TDatosPersonales;
                                            var MotivoCancelacion:TMotivoCancelacion;
                                            CantCopias:Integer): Boolean;

    function ImprimirContratoMoto(NumeroConvenio:String;
                                            var Vehiculo:Array of TCuentaABM;
                                            DatoTitular:TDatosPersonales;
                                            TipoImpresion: TTipoImpresionMotos;
                                            var MotivoCancelacion:TMotivoCancelacion;
                                            CantCopias:Integer): Boolean;

	function ImprimirCaratula(Personeria:String;
                                            NumeroConvenio:String;
                                            DatoTitular:TDatosPersonales;
                                            DocPresentada:RegDocumentacionPresentada;
                                            var MotivoCancelacion:TMotivoCancelacion;
											CantCopias:Integer;
                                            Operador : String;
                                            PuntoEntrega : String;
                                            PrimeraHoja: Boolean = True): Boolean;

	function ImprimirEtiquetas(NumeroConvenio:String;DatoTitular:TDatosPersonales;var Vehiculo:Array of TCuentaABM; var MotivoCancelacion:TMotivoCancelacion):Boolean;

    property NroAnexoEmitido: Integer read FNroAnexoEmitido write SetNroAnexoEmitido;

    //Revision 4
	//function ImprimirAnexo3o4(Anexo : integer; EnvioEmail : boolean; DatosAnexo3 : TDatosAnexo3; DatosAnexo4 : TDatosAnexo4) : boolean;							// SS_1171_EBA_20140311
    function ImprimirAnexo3o4(Anexo : integer; EnvioEmail : boolean; DatosAnexo3 : TDatosAnexo3; DatosAnexo4 : TDatosAnexo4; Personeria: string = '') : boolean;	// SS_1171_EBA_20140311


{INICIO: TASK_005_JMA_20160418
    function ImprimirCaratulaConvenio: Boolean;
TERMINO: TASK_005_JMA_20160418}
    function ImprimirEstadoConvenio: Boolean;
    Procedure Inicializar(DatosConvenio: TDatosConvenio);
    function VerificaConvenioListaAmarilla(CodigoConvenio : Integer) : Boolean;                   //SS_660_MVI_20130909

    function ImprimirAltaSuscripcionListaBlanca(CodigoConvenio, CodigoPersona: Integer; NumeroDocumento, Nombre, Email: string; Cuentas: array of TCuentaVehiculo; MedioPago: Integer; ClienteAdheridoEnvioEMail, DireccionConvenio: Boolean; Fecha:TDateTime) : Boolean; //TASK_036_ECA_20160621
  end;

var
  FormImprimirConvenio: TFormImprimirConvenio;


Const
    ENTER='\par';

    TAG_COPIA = 'COPIA';
    TAG_DIA = 'DIA';
    TAG_MES = 'MES';
    TAG_ANIO = 'ANIO';
    //TAG_EMISION_ELEC = 'EMISION_ELEC';
    TAG_HORA = 'HORA';
    TAG_MENSAJE = 'MENSAJE';

    TAG_CODIGO_BARRA = 'CODIGO_BARRA';
    //ANVERSO DEL CONVENIO - PERSONA NATURAL
    TAG_CON_CONVENIO = 'NUM_CONVENIO';
    TAG_CON_NUMERO_ANEXO =   'NUMERO_ANEXO';

    TAG_CON_NOMBRE = 'NAT_NOMBRE';
    TAG_CON_APELLIDO_PATERNO = 'NAT_APELLIDO_PATERNO';
	TAG_CON_APELLIDO_MATERNO = 'NAT_APELLIDO_MATERNO';
    TAG_CON_RUT = 'NAT_RUT';
    TAG_CON_NACIMIENTO = 'NAT_FEC_NAC';
    TAG_CON_SEXO = 'NAT_SEXO';

    TAG_CON_TELEFONO_ETIQUETA1 = 'TEL_ETIQUETA1';
    TAG_CON_TELEFONO_AREA1 = 'TEL_AREA1';
    TAG_CON_TELEFONO_NUMERO1 = 'TEL_NUMERO1';

	TAG_CON_TELEFONO_ETIQUETA2 = 'TEL_ETIQUETA2';
    TAG_CON_TELEFONO_AREA2 = 'TEL_AREA2';
    TAG_CON_TELEFONO_NUMERO2 = 'TEL_NUMERO2';

    TAG_CON_EMAIL = 'MAIL';

    TAG_CON_CALLE_COM = 'NAT_CALLE';
    TAG_CON_NUMERO_COM = 'NAT_NUMERO';
    TAG_CON_DETALLE_COM = 'NAT_DETALLE';
    TAG_CON_COMUNA_COM = 'NAT_COMUNA';
    TAG_CON_REGION_COM = 'NAT_REGION';
    TAG_CON_CP_COM = 'NAT_CP';

    TAG_CON_ACCION_VEHICULO = 'ACC';
    TAG_CON_ACCION_FECHA = 'FA';
    TAG_CON_PATENTE_VEHICULO = 'PATE';
    TAG_CON_TIPO_VEHICULO = 'TIPO';
    TAG_CON_CATE_VEHICULO = 'CA';
    TAG_CON_MARCA_VEHICULO = 'MARCA';
    TAG_CON_MODELO_VEHICULO = 'MODELO';
    TAG_CON_ANIO_VEHICULO = 'AN';
    TAG_CON_TAG_VEHICULO = 'TAG';
	TAG_CON_FECHA_ALTA = 'FALTA';
    TAG_CON_FECHA_VTO_GTIA = 'FVGTIA';

    TAG_CON_CANTIDAD_MAX_VEHICULOS_HOJA              =  10;
    TAG_CON_CANTIDAD_MAX_VEHICULOS_ABM_CUENTA_HOJA   =  10;
    TAG_CON_CANTIDAD_MAX_VEHICULOS_ABM_CUENTA_ANEXO  =  30;
    TAG_CON_CANTIDAD_MAX_VEHICULOS_ANEXO             =  30;
    TAG_CON_CANTIDAD_MAX_MOTOS_HOJA                  =  5;
    TAG_CON_CANTIDAD_MAX_MOTOS_ANEXO                 =  30;
    TAG_CON_CANTIDAD_MAX_MENSAJES                    =  7;


	 //ANVERSO DEL CONVENIO - PERSONA JUR�DICA
    TAG_JUR_CONVENIO = 'NUM_CONVENIO';

    TAG_JUR_RAZON_SOCIAL = 'JUR_RAZON_SOCIAL';
    TAG_JUR_RUT = 'JUR_RUT';

    TAG_JUR_TELEFONO_ETIQUETA1 = 'TEL_ETIQUETA1';
    TAG_JUR_TELEFONO_AREA1 = 'TEL_AREA1';
    TAG_JUR_TELEFONO_NUMERO1 = 'TEL_NUMERO1';

    TAG_JUR_TELEFONO_ETIQUETA2 = 'TEL_ETIQUETA2';
    TAG_JUR_TELEFONO_AREA2 = 'TEL_AREA2';
    TAG_JUR_TELEFONO_NUMERO2 = 'TEL_NUMERO2';

    TAG_JUR_EMAIL = 'MAIL';

    TAG_JUR_CALLE_PAR = 'JUR_CALLE';
    TAG_JUR_NUMERO_PAR = 'JUR_NUMERO';
    TAG_JUR_DETALLE_PAR = 'JUR_DETALLE';
    TAG_JUR_COMUNA_PAR = 'JUR_COMUNA';
    TAG_JUR_REGION_PAR = 'JUR_REGION';
    TAG_JUR_CP_PAR = 'JUR_CP';

    TAG_JUR_NOMBRE1 = 'NOMBRE1';
    TAG_JUR_APE_PAT1 = 'APE_PAT1';
    TAG_JUR_APE_MAT1 = 'APE_MAT1';
    TAG_JUR_RUT1 = 'JUR_RUT1';
    TAG_JUR_NOMBRE2 = 'NOMBRE2';
    TAG_JUR_APE_PAT2 = 'APE_PAT2';
    TAG_JUR_APE_MAT2 = 'APE_MAT2';
    TAG_JUR_RUT2 = 'JUR_RUT2';
	TAG_JUR_NOMBRE3 = 'NOMBRE3';
    TAG_JUR_APE_PAT3 = 'APE_PAT3';
    TAG_JUR_APE_MAT3 = 'APE_MAT3';
    TAG_JUR_RUT3 = 'JUR_RUT3';

    TAG_JUR_CANTIDAD_MAX_VEHICULOS_HOJA  = 10;
    TAG_JUR_CANTIDAD_MAX_VEHICULOS_ANEXO = 30;

    //MANDAT O  PAGO AUTOMATICO DE CUENTAS
    TAG_PAGO_NOMBRE = 'PAGO_NOMBRE';
    TAG_PAGO_APELLIDO_PATERNO = 'APELLIDO_PATERNO';
	TAG_PAGO_APELLIDO_MATERNO = 'APELLIDO_MATERNO';
	TAG_PAGO_NOMBRE_SOLO = 'NOMBRE';
    TAG_PAGO_RUT = 'PAGO_RUT';
    TAG_PAGO_TELEFONO = 'PAGO_TELEFONO';
    TAG_PAGO_TIPO_CUENTA = 'PAGO_TIPO_CUENTA';
    TAG_PAGO_NUMERO_CUENTA = 'PAGO_NUMERO_CUENTA';
    TAG_PAGO_BANCO = 'PAGO_BANCO';
    TAG_PAGO_SUCURSAL = 'PAGO_SUCURSAL';

//    TAG_PAGO_IDENTIFICACION_SERVICIO = 'IDENTIFICACION_SERVICIO';
    TAG_PAGO_DIA = 'PAGO_DIA';
	TAG_PAGO_MES = 'PAGO_MES';
    TAG_PAGO_ANIO = 'PAGO_ANIO';

//    TAG_PAGO_DIRECION = 'PAGO_DIRECCION';
    TAG_PAGO_NOMBRE_TARJETA = 'PAGO_NOMBRE_TARJETA';
    TAG_PAGO_NUMERO_TARJETA = 'PAGO_NUMERO_TARJETA';
    TAG_PAGO_FECHA_VENC_MES = 'FECHA_VENC_MES';
    TAG_PAGO_FECHA_VENC_ANIO = 'FECHA_VENC_ANIO';

	//CAR�TUL A  PARA LA CARPETA DEL CLIENTE - PERSONA NATURAL/JURIDICA
    TAG_CAR_NUMERO_CONVENIO = 'NUMERO_CONVENIO';
    TAG_CAR_NOMBRE = 'NOMBRE';
    TAG_CAR_RUT = 'RUT';
    TAG_CAR_GIRO = 'GIRO';
    TAG_CAR_PUNTO_ENTREGA =   'PuntoEntrega';
    TAG_CAR_OPERADOR =   'Operador';
    TAG_CAR_FECHA_HORA =   'FechaHora';

    TAG_CAR_DOC = 'DOC';
    TAG_CAR_MARCA = 'MARCA';

    TAG_FIRMA_CONCESIONARA = 'FIRMA_CONCESIONARIA';
    TAG_FIRMA_CLIENTE = 'FIRMA_CLIENTE';

    TAG_LUGAR_FIRMA_REVISOR = 'LUGAR_FIRMA_REVISOR';
    TAG_FIRMA_REVISOR = 'FIRMA_REVISOR';
    TAG_ADHERIDOPA = 'ADHERIDOPA';

    ANEXO_NRO = 'Anexo N�. ';

    CAR_MAX_RENGLONES  =  18;

    //Agrego un tag para marcar que el convenio de motos contin�a en otra hoja
    TAG_CONTINUA_ANEXO_MOTO = 'ANEXO';


    //Revision 4
    //Campos para anexos 3
    TAG_3_NOMBRE			= 'NOMBRE';
    TAG_3_APELLIDO_PATERNO	= 'APELLIDO_PATERNO';
    TAG_3_APELLIDO_MATERNO	= 'APELLIDO_MATERNO';
    TAG_3_CEDULA_IDENTIDAD	= 'CEDULA_IDENTIDAD';
    TAG_3_DOMICILIO			= 'DOMICILIO';
    TAG_3_DOM_NUMERO		= 'DOM_NUMERO';
    TAG_3_COMUNA			= 'COMUNA';
    TAG_3_TELEVIA			= 'TELEVIA';
    TAG_3_390				= 'VALOR_390';
    TAG_3_450				= 'VALOR_450';
    TAG_3_VALOR_MAIL        = 'VALOR_MAIL';         //SS_948 20110107
    TAG_3_VALOR_POSTAL      = 'VALOR_POSTAL';       //SS_948 20110107
    TAG_3_GLOSA_MAIL        = 'GLOSA_MAIL';         //SS_948 20110107
    TAG_3_GLOSA_POSTAL      = 'GLOSA_POSTAL';       //SS_948 20110107
    TAG_3_DOMICILIO_DPTO	= 'DEPTO';				//SS_973_MBE_20110801
    TAG_3_DOMICILIO_DETA	= 'DETALLE_DOMICILIO';	//SS_973_MBE_20110801
    TAG_3_RAZON_SOCIAL      = 'RAZON_SOCIAL';       //SS_1171_EBA_20140311
    TAG_3_RUT_RAZON_SOCIAL  = 'RUT_RAZON_SOCIAL';   //SS_1171_EBA_20140311


    //Campos Anexo 4
    TAG_REP_NOMBRE				= 'R_NOMBRE';
    TAG_REP_APELLIDO_PATERNO	= 'R_APELLIDO_PATERNO';
    TAG_REP_APELLIDO_MATERNO	= 'R_APELLIDO_MATERNO';
    TAG_REP_CEDULA_IDENTIDAD	= 'R_CEDULA_IDENTIDAD';
    TAG_REP_REPRESENTADO_POR	= 'REPRESENTADO_POR';
    TAG_REP_RUT_NUMERO			= 'RUT_NUMERO';
    TAG_4_EMAIL					= 'EMAIL';



resourcestring
    STR_FIRMA_CONCESIONARIA = 'p.p. Concesionaria';
    STR_FIRMA_CLIENTE       = 'Cliente';
    STR_FIRMA_REVISOR       = 'Firma Revisor';
    STR_LUGAR_FIRMA_REVISOR = '_______________';

    // Rev. 8 (SS 733)
    LBL_LUGAR_FECHA = '"Santiago, a "dd" de "mmmm" de "yyyy';
    LBL_CONVENIO_FORMATEADO = 'Convenio N� %s';
    LBL_CONVENIO_BAJA_FORMATEADO = 'Baja del Convenio N� %s';
    // Fin Rev. 8 (SS 733)


implementation

{$R *.dfm}


{------------------------------------------------------------------------
            ImprimirAnexo3o4

Author: mbecerra
Date: 24-Dic-2008
Description:	(Ref SS 769) Se imprime el anexo 3

Revision : 1
    Author : pdominguez
    Date   : 17/07/2009
    Description : SS 733
        - Se adapt� para la impresi�n al Reporte ppReporteConvenioCompleto.

Revision : 2
    Author : pdominguez
    Date   : 19/08/2009
    Description : SS 733
        - Se cambia la asignaci�n del reporte al objeto
        ppReporteConvenioCompleto2: TppReport.

Revision: 12
    Author : pdominguez
    Date   : 07/06/2010
    Description : SS 895 - Error Impresi�n Convenio CAC
        - Se cambia el LoadFromFile del objeto CampoRich, por la carga a trav�s
        de la funci�n CargaPlantillaRTF.

Revision: 14
  Author : ebaeza
  Date: 14/03/2014
  firma: SS_1171_EBA_20140311
  descripcion: SS_1171
      -.Se agrega el par�metro Personeria de tipo string, este par�metro permitir� diferenciar cual
        anexo se imprimir� si el par�metro es "J", significa que se est� imprimiendo el anexo III Jur�dico

      -.Se agregan dos constantes "TAG_3_RAZON_SOCIAL", "TAG_3_RUT_RAZON_SOCIAL"
        para la impresion del anexo III Juridico

      -.Se agregan dos resourcesstring "PLANTILLA_3_J" y "MSG_REPORTE_3_J", para el manejo adecuado del
        anexo III juridico cuando el parametro personeria sea igual a "J"
------------------------------------------------------------------------}
function TFormImprimirConvenio.ImprimirAnexo3o4;
resourcestring
    PLANTILLA_3		= 'PLANTILLA_ANEXO_3';
    PLANTILLA_3_J   = 'PLANTILLA_ANEXO_3_JURIDICA';           // SS_1171_EBA_20140311
    PLANTILLA_4		= 'PLANTILLA_ANEXO_4';
    MSG_ERROR		= 'No se encontr� archivo de plantilla ';
    MSG_REPORTE_3	= 'Impresi�n ANEXO III';
    MSG_REPORTE_3_J = 'Impresi�n ANEXO III Jur�dico';       // SS_1171_EBA_20140311
    MSG_REPORTE_4	= 'Impresi�n ANEXO IV';
    MSG_ERROR_ANEXO	= 'Error al generar el anexo %s';
    MSG_REPRESENTADO	= ', representado por ';
    MSG_RUT_NUMERO	= 'RUT N�';

{INICIO: TASK_005_JMA_20160418
    MSG_SQL_MAIL    = 'SELECT dbo.CONST_CODIGO_CONCEPTO_ARRIENDO_TELEVIA_CUOTAS_MAIL()';            //SS948 20110107
    MSG_SQL_POSTAL  = 'SELECT dbo.CONST_CODIGO_CONCEPTO_ARRIENDO_TELEVIA_CUOTAS_POSTAL()';          //SS948 20110107
}
	MSG_SQL_MAIL    = 'SELECT dbo.CONST_CODIGO_CONCEPTO_ARRIENDO_TELEVIA_CUOTAS_MAIL(dbo.ObtenerConcesionariaNativa())';            
    MSG_SQL_POSTAL  = 'SELECT dbo.CONST_CODIGO_CONCEPTO_ARRIENDO_TELEVIA_CUOTAS_POSTAL(dbo.ObtenerConcesionariaNativa())';
{TERMINO: TASK_005_JMA_20160418}	
    MSG_SQL_VALOR   = 'SELECT dbo.ObtenerImporteTotalConceptoMovimiento( %d, ''%s'')';              //SS948 20110107

var
	Plantilla, Archivo, FechaHoy : string;
    RepLegal : TPersonaJuridica;
    ConceptoMail, ConceptoPostal, ValorMail, ValorPostal : Integer;         //SS948 20110107
    FechaHoyISO : string;                                                   //SS948 20110107
begin
	Result := False;
    try
    	FechaHoy    := FormatDateTime('ddmmyyyy', NowBase(DMConnections.BaseCAC));
        FechaHoyISO := Copy(FechaHoy, 5,4) + Copy(FechaHoy, 3,2) + Copy(FechaHoy, 1,2);
	    //if Anexo = 3 then Plantilla := PLANTILLA_3							//SS_1171_EBA_20140311
	    if Anexo = 3 then begin                                               	//SS_1171_EBA_20140311
            //Anexo 3 juridico --> SS_1171_EBA_20140311                         //SS_1171_EBA_20140311
            if Personeria = PERSONERIA_JURIDICA then Plantilla := PLANTILLA_3_J //SS_1171_EBA_20140311
            else Plantilla := PLANTILLA_3                                       //SS_1171_EBA_20140311
          end                                                                   //SS_1171_EBA_20140311
        else Plantilla := PLANTILLA_4;

    	if ObtenerParametroGeneral(DMConnections.BaseCAC, Plantilla, Archivo) then begin
    		if FileExists(Archivo) then begin
        		if Anexo = 3 then begin
                    // Rev. 12 (SS 895)
                    CampoRich.RichText := CargaPlantillaRTF(Archivo);
                    //CampoRich.LoadFromFile(Archivo);
                    // Fin Rev. 12 (SS 895)

                    //obtenemos los codigos de conceptos                                            //SS948 20110107
                    ConceptoMail := QueryGetValueInt(DMConnections.BaseCAC, MSG_SQL_MAIL);          //SS948 20110107
                    ConceptoPostal := QueryGetValueInt(DMConnections.BaseCAC, MSG_SQL_POSTAL);      //SS948 20110107
                    //obtenemos el Precio de cada Concepto                                          //SS948 20110107
                    ValorMail   := QueryGetValueInt(DMConnections.BaseCAC, Format(MSG_SQL_VALOR, [ConceptoMail, FechaHoyISO]));    //SS948 20110107
                    ValorPostal := QueryGetValueInt(DMConnections.BaseCAC, Format(MSG_SQL_VALOR, [ConceptoPostal, FechaHoyISO]));  //SS948 20110107
                    ValorMail   := ValorMail div 100;           //SS948 20110107
                    ValorPostal := ValorPostal div 100;         //SS948 20110107

		            CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_DIA, Copy(FechaHoy,1,2));
		            CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_MES, MesNombre(NowBase(DMConnections.BaseCAC)));
		            CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_ANIO, Copy(FechaHoy,5,4));

                    //Anexo 3 juridico --> SS_1171_EBA_20140311
                    if Personeria = PERSONERIA_JURIDICA then begin                                                              // SS_1171_EBA_20140311
                      CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_3_RAZON_SOCIAL, DatosAnexo3.RazonSocial);        // SS_1171_EBA_20140311
                      CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_3_RUT_RAZON_SOCIAL, DatosAnexo3.RutRazonSocial); // SS_1171_EBA_20140311
                    end;                                                                                                        // SS_1171_EBA_20140311
                
                    CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_3_APELLIDO_PATERNO, DatosAnexo3.APaterno);
                    CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_3_APELLIDO_MATERNO, DatosAnexo3.Amaterno);
                    CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_3_NOMBRE, DatosAnexo3.Nombre);
                    CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_3_CEDULA_IDENTIDAD, DatosAnexo3.Cedula);
                    CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_3_TELEVIA, DatosAnexo3.Televia);
                    CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_3_DOMICILIO, DatosAnexo3.DomicilioCalle);
                    CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_3_DOM_NUMERO, DatosAnexo3.DomicilioNumero);
                    CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_3_COMUNA, DatosAnexo3.DomicilioComuna);
                    CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_3_VALOR_MAIL, IntToStr(ValorMail));                //SS948 20110107
                    CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_3_VALOR_POSTAL, IntToStr(ValorPostal));            //SS948 20110107
                    CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_3_GLOSA_MAIL, NumeroAPalabras(ValorMail));         //SS948 20110107
                    CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_3_GLOSA_POSTAL, NumeroAPalabras(ValorPostal));     //SS948 20110107
                    if DatosAnexo3.DomicilioDepto <> '' then begin                                                                              //SS_973_MBE_20110801
                    	CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_3_DOMICILIO_DPTO, ' Depto: ' + DatosAnexo3.DomicilioDepto);	//SS_973_MBE_20110801
                    end                                                                                                                         //SS_973_MBE_20110801
                    else begin                                                                                                                  //SS_973_MBE_20110801
                    	CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_3_DOMICILIO_DPTO, DatosAnexo3.DomicilioDepto);					//SS_973_MBE_20110801
                    end;                                                                                                                        //SS_973_MBE_20110801

                    if (DatosAnexo3.DomicilioDepto <> '') and (DatosAnexo3.DomicilioDetalle <> '') then	begin									//SS_973_MBE_20110801
						CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_3_DOMICILIO_DETA, ', ' + DatosAnexo3.DomicilioDetalle)			//SS_973_MBE_20110801
                    end                                                                                                                         //SS_973_MBE_20110801
                    else begin                                                                                                                  //SS_973_MBE_20110801
                    	CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_3_DOMICILIO_DETA, DatosAnexo3.DomicilioDetalle);				//SS_973_MBE_20110801
                    end;                                                                                                                        //SS_973_MBE_20110801

					if EnvioEmail then begin
   		         		CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_3_390, 'X');
   		         		CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_3_450, '');
                	end
            		else begin
            			CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_3_390, '');
            			CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_3_450, 'X');
            		end;
            	end
            	else begin     // Anexo 4
                    // Rev. 12 (SS 895)
                    CampoRich.RichText := CargaPlantillaRTF(Archivo);
                    //CampoRich.LoadFromFile(Archivo);
                    // Fin Rev. 12 (SS 895)
	            	CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_DIA, Copy(FechaHoy,1,2));
	            	CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_MES, MesNombre(NowBase(DMConnections.BaseCAC)));
	            	CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_ANIO, Copy(FechaHoy,5,4));
                    CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_4_EMAIL, DatosAnexo4.EMail);
                    CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_3_DOMICILIO, DatosAnexo3.DomicilioCalle);
                    CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_3_DOM_NUMERO, DatosAnexo3.DomicilioNumero);
                    CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_3_COMUNA, DatosAnexo3.DomicilioComuna);
                    if DatosAnexo3.DomicilioDepto <> '' then begin                                                                              //SS_973_MBE_20110801
                    	CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_3_DOMICILIO_DPTO, ' Depto: ' + DatosAnexo3.DomicilioDepto);	//SS_973_MBE_20110801
                    end                                                                                                                         //SS_973_MBE_20110801
                    else begin                                                                                                                  //SS_973_MBE_20110801
                    	CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_3_DOMICILIO_DPTO, DatosAnexo3.DomicilioDepto);					//SS_973_MBE_20110801
                    end;                                                                                                                        //SS_973_MBE_20110801

                    if (DatosAnexo3.DomicilioDepto <> '') and (DatosAnexo3.DomicilioDetalle <> '') then	begin									//SS_973_MBE_20110801
						CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_3_DOMICILIO_DETA, ', ' + DatosAnexo3.DomicilioDetalle)			//SS_973_MBE_20110801
                    end                                                                                                                         //SS_973_MBE_20110801
                    else begin                                                                                                                  //SS_973_MBE_20110801
                    	CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_3_DOMICILIO_DETA, DatosAnexo3.DomicilioDetalle);				//SS_973_MBE_20110801
                    end;                                                                                                                        //SS_973_MBE_20110801

                    if DatosAnexo4.RepAPaterno <> EmptyStr then begin  {hay rep legal}
                        CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_3_APELLIDO_PATERNO, DatosAnexo3.APaterno); {Raz�n Social}
	            		CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_3_APELLIDO_MATERNO, EmptyStr);
	            		CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_3_NOMBRE, EmptyStr);
//                		CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_3_CEDULA_IDENTIDAD, DatosAnexo3.Cedula);		// SS_948_PDO_20120704
                		CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_3_CEDULA_IDENTIDAD, DatosAnexo4.CliCedula);	// SS_948_PDO_20120704
                        CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_REP_APELLIDO_PATERNO, DatosAnexo4.RepAPaterno);
                        CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_REP_APELLIDO_MATERNO, DatosAnexo4.RepAmaterno);
	            		CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_REP_NOMBRE, DatosAnexo4.RepNombre);
                		CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_REP_CEDULA_IDENTIDAD, DatosAnexo4.RepCedula);
                        CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_REP_REPRESENTADO_POR, MSG_REPRESENTADO);
                        CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_REP_RUT_NUMERO, MSG_RUT_NUMERO);
                    end
                    else begin
                    	CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_3_APELLIDO_PATERNO, DatosAnexo3.APaterno);
	            		CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_3_APELLIDO_MATERNO, DatosAnexo3.Amaterno);
	            		CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_3_NOMBRE, DatosAnexo3.Nombre);
                		CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_3_CEDULA_IDENTIDAD, DatosAnexo3.Cedula);
                    	CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_REP_APELLIDO_PATERNO, EmptyStr);
                    	CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_REP_APELLIDO_MATERNO, EmptyStr);
                    	CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_REP_NOMBRE, EmptyStr);
                    	CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_REP_CEDULA_IDENTIDAD, EmptyStr);
                        CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_REP_REPRESENTADO_POR, EmptyStr);
                        CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_REP_RUT_NUMERO, EmptyStr);
                    end;
            	end;

            	//if Anexo = 3 then RBInterface.Caption := MSG_REPORTE_3							// SS_1171_EBA_20140311
				if Anexo = 3 then begin                                                             // SS_1171_EBA_20140311
                	//Anexo 3 juridico --> SS_1171_EBA_20140311                                     // SS_1171_EBA_20140311
                	if Personeria = PERSONERIA_JURIDICA then RBInterface.Caption := MSG_REPORTE_3_J	// SS_1171_EBA_20140311
                	else RBInterface.Caption := MSG_REPORTE_3;                                      // SS_1171_EBA_20140311
              	end                                                                                 // SS_1171_EBA_20140311
            	else RBInterface.Caption := MSG_REPORTE_4;
                // Rev. 1 (SS 733)
                RBInterface.Report := ppReporteConvenioCompleto2; // Rev. 2 (SS 733)
            	ppReporteConvenioCompleto2.PrinterSetup.Copies := 1; // Rev. 2 (SS 733)

                cdsDatosConvenioCompleto.EmptyDataSet;
                cdsDatosConvenioCompleto.Append;
                cdsDatosConvenioCompletoMiRichEdit.AsString := CampoRich.RichText;
                cdsDatosConvenioCompleto.Post;
                cdsDatosConvenioCompleto.Append;
                cdsDatosConvenioCompletoMiRichEdit.AsString := CampoRich.RichText;
                cdsDatosConvenioCompleto.Post;

//            	Result := Imprimir(tthBlanco, 2, True);
            	Result := Imprimir(tthBlanco, 1, True);
                // Fin Rev. 1 (SS 733)

        	end
        	else MsgBox(MSG_ERROR + PLANTILLA, Caption, MB_ICONERROR);
    	end;

    except on e:exception do begin
        	//if Anexo = 3 then MsgBoxErr(Format(MSG_ERROR_ANEXO, [MSG_REPORTE_3]), e.Message, Caption, MB_ICONERROR)								// SS_1171_EBA_20140311
			if Anexo = 3 then begin                                                                                                                 // SS_1171_EBA_20140311
            	//Anexo 3 juridico --> SS_1171_EBA_20140311                                                                                         // SS_1171_EBA_20140311
            	if Personeria = PERSONERIA_JURIDICA then MsgBoxErr(Format(MSG_ERROR_ANEXO, [MSG_REPORTE_3_J]), e.Message, Caption, MB_ICONERROR)	// SS_1171_EBA_20140311
            	else MsgBoxErr(Format(MSG_ERROR_ANEXO, [MSG_REPORTE_3]), e.Message, Caption, MB_ICONERROR)                                          // SS_1171_EBA_20140311
          	end                                                                                                                                     // SS_1171_EBA_20140311
            else MsgBoxErr(Format(MSG_ERROR_ANEXO, [MSG_REPORTE_4]), e.Message, Caption, MB_ICONERROR)
      	end;
    end;


end;


{INICIO: TASK_011_JMA_20160505
{******************************** Function Header ******************************
Function Name: TFormImprimirConvenio.ImprimirConvenio
Author       : dcalani
Date Created : 26/06/2004
Description  : Imprime todo el convenio
Parameters   : NumeroConvenio: String; DatoTitular:TDatosPersonales;
    PrimerMedioComunicacion:TFrameTelefono; SegundoMedioComunicacion:TFrameTelefono;
    DomicilioTitular:TDatosDomicilio; DomicilioFacturacion:TDatosDomicilio;
    DatosMedioPago:TDatosMediosPago; Vehiculo:Array of TCuentaVehiculo;
    DatoRepLegal1:TDatosPersonales; DatoRepLegal2:TDatosPersonales;
    DatoRepLegal3:TDatosPersonales; DocPresentada:RegDocumentacionPresentada;
    RegDatoMandante:TDatosPersonales; EnviaDocumentacionxMail:boolean; Mail:String;
    var MotivoCancelacion:TMotivoCancelacion
Return Value : Boolean

Revision : 1
    Author : pdominguez
    Date   : 09/09/2009
    Description : SS 733
        - Se a�aden los par�metros PuntoEntregaCodigo y PuntoVentacodigo a la
        funci�n.
        - Se asignan los datos del convenio a la variable FDatosConvenio en caso
        de que estos no esten asignados.
*******************************************************************************
function TFormImprimirConvenio.ImprimirConvenio(NumeroConvenio: String;
          TipoImpresion: TTipoImpresion;
          DatoTitular:TDatosPersonales;
          PrimerMedioComunicacion:TTipoMedioComunicacion;
          SegundoMedioComunicacion:TTipoMedioComunicacion;
          DomicilioTitular:TDatosDomicilio;
          DomicilioFacturacion:TDatosDomicilio;
          DatosMedioPago:TDatosMediosPago;
          Vehiculo: TRegCuentaABM;
          DatoRepLegal1:TDatosPersonales;
          DatoRepLegal2:TDatosPersonales;
          DatoRepLegal3:TDatosPersonales;
          DocPresentada:RegDocumentacionPresentada;
          RegDatoMandante:TDatosPersonales;
          EnviaDocumentacionxMail:boolean;
          Mail:String;
          Operador : String;
          PuntoEntrega : String;
          FechaAlta : TDateTime;
		  var MotivoCancelacion:TMotivoCancelacion;
          PuntoEntregaCodigo,
          PuntoVentaCodigo: Integer): Boolean;
var
    CantidadCopias:Integer;
    i :Integer;
    auxDescripTipo, auxDescripCategoria, auxCategoria:String;
begin
    if Not assigned(FDatosConvenio) then FDatosConvenio := TDatosConvenio.Create(NumeroConvenio,PuntoVentaCodigo,PuntoEntregaCodigo); // Rev. 1 (SS 733)

    FormularioPreImpreso := True;

    DatoTitular.NumeroDocumento:=StrLeft(DatoTitular.NumeroDocumento,length(DatoTitular.NumeroDocumento)-1)+'-'+StrRight(DatoTitular.NumeroDocumento,1);
	if trim(DatoRepLegal1.NumeroDocumento)<>'' then DatoRepLegal1.NumeroDocumento:=StrLeft(DatoRepLegal1.NumeroDocumento,length(DatoRepLegal1.NumeroDocumento)-1)+'-'+StrRight(DatoRepLegal1.NumeroDocumento,1);
    if trim(DatoRepLegal2.NumeroDocumento)<>'' then DatoRepLegal2.NumeroDocumento:=StrLeft(DatoRepLegal2.NumeroDocumento,length(DatoRepLegal2.NumeroDocumento)-1)+'-'+StrRight(DatoRepLegal2.NumeroDocumento,1);
	if trim(DatoRepLegal3.NumeroDocumento)<>'' then DatoRepLegal3.NumeroDocumento:=StrLeft(DatoRepLegal3.NumeroDocumento,length(DatoRepLegal3.NumeroDocumento)-1)+'-'+StrRight(DatoRepLegal3.NumeroDocumento,1);

    result:=False;
    MotivoCancelacion := Peatypes.ERROR;

	// Revision 1
    for i := 1 to Length(Vehiculo) do begin
        ObtenerDescripTipoVehiculo(DMConnections.BaseCAC,Vehiculo[i-1].Cuenta.Vehiculo.CodigoTipo,auxDescripTipo,auxDescripCategoria,auxCategoria);
        if trim(auxCategoria) = XML_VEHICULO_MOTO then begin
            ObtenerParametroGeneral(DMConnections.BaseCAC,'CAN_COPIAS_DECLARACION_MOTO',CantidadCopias);
            ImprimirContratoMoto(NumeroConvenio, Vehiculo, DatoTitular, TiNuevas,MotivoCancelacion, CantidadCopias);
            Break;
        end;
    end;

    if DatoTitular.Personeria = PERSONERIA_JURIDICA then begin

        //convenio juridico
        ObtenerParametroGeneral(DMConnections.BaseCAC,CAN_COPIAS_CONVENIO_JURIDICA,CantidadCopias);
        if not(ImprimirConvenioPersonaJuridica(NumeroConvenio, TipoImpresion, DatoTitular, PrimerMedioComunicacion,SegundoMedioComunicacion,Mail,DomicilioTitular,DomicilioFacturacion,
                DatosMedioPago,DatoRepLegal1,DatoRepLegal2,DatoRepLegal3,EnviaDocumentacionxMail, FechaAlta,MotivoCancelacion, CantidadCopias,Vehiculo)) then exit; //i=1

        //Caratula
{        ObtenerParametroGeneral(DMConnections.BaseCAC,CAN_COPIAS_CARATULA,CantidadCopias);
        if not(ImprimirCaratula(PERSONERIA_JURIDICA,NumeroConvenio,DatoTitular,DocPresentada,MotivoCancelacion,CantidadCopias,Operador, PuntoEntrega)) then exit;
        if not ImprimirCaratulaConvenio then Exit; //SS_1380_NDR_20151009// Rev. (SS 733) SS_1380_MCA_20150911

        if DatosMedioPago.TipoMedioPago=PAC then begin
            //Mandato PAC
            ObtenerParametroGeneral(DMConnections.BaseCAC,CAN_COPIAS_MANDATO_PAC,CantidadCopias);
            if not(ImprimirMandatoPAC(NumeroConvenio,DatosMedioPago,RegDatoMandante,MotivoCancelacion,CantidadCopias)) then exit;
        end;

		if DatosMedioPago.TipoMedioPago=PAT then begin
            //Mandato PAT
            ObtenerParametroGeneral(DMConnections.BaseCAC,iif(DatosMedioPago.PAT.TipoTarjeta = TARJETA_PRESTO ,CAN_COPIAS_MANDATO_PAT_PRESTO, CAN_COPIAS_MANDATO_PAT),CantidadCopias);
            if not(ImprimirMandatoPAT(NumeroConvenio,DatosMedioPago,RegDatoMandante, DatoTitular, DomicilioTitular, Mail, Vehiculo, MotivoCancelacion,CantidadCopias)) then exit;
        end;

    end else begin

        //convenio fisica
		ObtenerParametroGeneral(DMConnections.BaseCAC,CAN_COPIAS_CONVENIO_FISICA,CantidadCopias);
        if not(ImprimirConvenioPersonaNatural(NumeroConvenio, TipoImpresion, DatoTitular, PrimerMedioComunicacion,
                SegundoMedioComunicacion, Mail, DomicilioTitular, DomicilioFacturacion, DatosMedioPago,
                Vehiculo, EnviaDocumentacionxMail, FechaAlta, MotivoCancelacion, CantidadCopias, PLANTILLA_CONVENIO_PERSONA_NATURAL)) then Exit;

        //Caratula
{        ObtenerParametroGeneral(DMConnections.BaseCAC,CAN_COPIAS_CARATULA,CantidadCopias);
        if not(ImprimirCaratula(PERSONERIA_FISICA,NumeroConvenio,DatoTitular,DocPresentada,MotivoCancelacion,CantidadCopias, Operador, PuntoEntrega)) then exit;
        if not ImprimirCaratulaConvenio then Exit; //SS_1380_NDR_20151009// Rev. (SS 733) SS_1380_MCA_20150911

        if DatosMedioPago.TipoMedioPago=PAC then begin
            //Mandato PAC
            ObtenerParametroGeneral(DMConnections.BaseCAC,CAN_COPIAS_MANDATO_PAC,CantidadCopias);
            if not(ImprimirMandatoPAC(NumeroConvenio,DatosMedioPago,RegDatoMandante,MotivoCancelacion,CantidadCopias)) then exit;
        end;

        if DatosMedioPago.TipoMedioPago=PAT then begin
            //Mandato PAT
            ObtenerParametroGeneral(DMConnections.BaseCAC,iif(DatosMedioPago.PAT.TipoTarjeta = TARJETA_PRESTO ,CAN_COPIAS_MANDATO_PAT_PRESTO, CAN_COPIAS_MANDATO_PAT),CantidadCopias);
            if not(ImprimirMandatoPAT(NumeroConvenio,DatosMedioPago,RegDatoMandante, DatoTitular, DomicilioTitular, Mail, Vehiculo, MotivoCancelacion,CantidadCopias)) then exit;
        end;

    end;
	result:=True;
    MotivoCancelacion:=OK;
end;
}
{INICIO:  TASK_018_JMA_20160606_Impresi�n de Documentos Convenio (Reportes Finales)
function TFormImprimirConvenio.ImprimirConvenio(NumeroConvenio: String;
          TipoImpresion: TTipoImpresion;
          DatoTitular:TDatosPersonales;
          PrimerMedioComunicacion:TTipoMedioComunicacion;
          SegundoMedioComunicacion:TTipoMedioComunicacion;
          DomicilioTitular:TDatosDomicilio;
          DomicilioFacturacion:TDatosDomicilio;
          DatosMedioPago:TDatosMediosPago;
          Vehiculo: TRegCuentaABM;
          DatoRepLegal1:TDatosPersonales;
          DatoRepLegal2:TDatosPersonales;
          DatoRepLegal3:TDatosPersonales;
          DocPresentada:RegDocumentacionPresentada;
          RegDatoMandante:TDatosPersonales;
          EnviaDocumentacionxMail:boolean;
          Mail:String;
          Operador : String;
          PuntoEntrega : String;
          FechaAlta : TDateTime;
		  var MotivoCancelacion:TMotivoCancelacion;
          PuntoEntregaCodigo,
          PuntoVentaCodigo: Integer): Boolean;
var
    CantidadCopias:Integer;
begin
    if Not assigned(FDatosConvenio) then FDatosConvenio := TDatosConvenio.Create(NumeroConvenio,PuntoVentaCodigo,PuntoEntregaCodigo); // Rev. 1 (SS 733)

    FormularioPreImpreso := True;

    DatoTitular.NumeroDocumento := StrLeft(DatoTitular.NumeroDocumento,length(DatoTitular.NumeroDocumento)-1) + '-' + StrRight(DatoTitular.NumeroDocumento,1);

    result:=False;
    MotivoCancelacion := Peatypes.ERROR;

    ImprimirAltaConvenio(true, true, true);

	result:=True;
    MotivoCancelacion:=OK;
end;
}
{TERMINO:  TASK_018_JMA_20160606_Impresi�n de Documentos Convenio (Reportes Finales)}

{
******************************** Function Header ******************************
Function Name: ImprimirCaratulaConvenio
Author : pdominguez
Date Created : 05/08/2009
Parameters : None
Return Value : Boolean
Description : SS 733
    - Se implementa un nuevo proceso para imprimir la Car�tula de los Convenios.
*******************************************************************************
function TFormImprimirConvenio.ImprimirCaratulaConvenio: Boolean;
    Resourcestring
        LBL_TITULO = 'Car�tula para la Carpeta del Cliente - Persona %s';
        LBL_PERSONA_JURIDICA = 'Jur�dica';
        LBL_PERSONA_NATURAL = 'Natural';
        LBL_FECHA_HORA = 'dd/mm/yyyy hh:nn';
        ERROR_PARAMETRO_GENERAL = 'Se produjo un Error al obtener el Par�metro General "%s".';
    	TITLE_CONVENIO_CARATULA = 'Car�tula Convenio';
        ERROR_EN_PROCESO = 'Se produjo un Error en la Impresi�n de la Car�tula del Convenio.';
    var
        CantidadCopias: Integer;
        DescError: AnsiString;

    ******************************** Function Header ******************************
    Function Name: PrepararRBInterface
    Author : pdominguez
    Date Created : 06/08/2009
    Parameters : var DescError: AnsiString
    Return Value : Boolean
    Description : SS 733
        - Asigna al componente RBInterface los par�metrosd de impresi�n.
    *******************************************************************************
    function PrepararRBInterface(var DescError: AnsiString): Boolean;
        ResourceString
            ERROR_PREPARAR_RBINTRERFACE = 'Se produjo un error al preparar el componente RBInterface. Error: %s';
        var
        	PRNConfig: TRBConfig;
    begin
        try
            Result := True;
            PRNConfig := RBInterface.GetConfig;
            PRNConfig.DeviceType := 'Screen';
            PRNConfig.Orientation := poPortrait;
            PRNConfig.PaperName := 'A4';
            PRNConfig.PaperHeight := 297;
            PRNConfig.PaperWidth := 210;
            PRNConfig.Copies := CantidadCopias;
            RBInterface.SetConfig(PRNConfig);
        except
            On E:Exception do begin
                DescError := Format(ERROR_PREPARAR_RBINTRERFACE,[E.Message]);
                Result := False;
            end;
        end;
    end;
begin
    // Revisamos si el tipo de hoja establecido, es el correcto para el reporte.
	if tthBlanco <> Hoja then
        if MsgBox(MSG_CAPTION_PAPEL_BLANCO,MSG_CAPTION_IMPRESION_CONVENIOS,MB_ICONWARNING+MB_YESNO) = mrNo then Exit;

    try
        Hoja := tthBlanco;
        Result := False;
        DescError := '';

        if ObtenerParametroGeneral(DMConnections.BaseCAC,CAN_COPIAS_CARATULA,CantidadCopias) then begin
            ppBCNumeroConvenioCaratula.Data := FDatosConvenio.NumeroConvenio;
            pplblNumeroConvenio.Caption := FDatosConvenio.NumeroConvenioFormateado;

            // Asignamos el T�tulo y el nombre formateado, dependiendo de la Personer�a.
            if FDatosConvenio.Cliente.Datos.Personeria = PERSONERIA_JURIDICA then begin
                pplblTituloCaratulaConvenio.Caption := Format(LBL_TITULO,[LBL_PERSONA_JURIDICA]);
                pplblNomobreCliente.Caption :=
                    ArmarNombrePersona(
                        PERSONERIA_JURIDICA,
                        ArmarNombrePersona(
                            PERSONERIA_FISICA,
                            FDatosConvenio.Cliente.Datos.Nombre,
                            FDatosConvenio.Cliente.Datos.Apellido,
                            FDatosConvenio.Cliente.Datos.ApellidoMaterno),
                        FDatosConvenio.Cliente.Datos.RazonSocial,'')
            end else begin
                pplblTituloCaratulaConvenio.Caption := Format(LBL_TITULO,[LBL_PERSONA_NATURAL]);
                pplblNomobreCliente.Caption :=
                    ArmarNombrePersona(
                        PERSONERIA_FISICA,
                        FDatosConvenio.Cliente.Datos.Nombre,
                        FDatosConvenio.Cliente.Datos.Apellido,
                        FDatosConvenio.Cliente.Datos.ApellidoMaterno);
            end;

            pplblNumeroRUTCliente.Caption := FDatosConvenio.Cliente.Datos.NumeroDocumento;
            pplblGiroCliente.Caption := FDatosConvenio.Cliente.Datos.Giro;

            pplblPuntoEntrega.Caption := ObtenerDescripcionPuntoEntrega(DMConnections.BaseCAC,FDatosConvenio.PuntoEntrega);
            pplblOperador.Caption := UsuarioSistema;
            pplblFechaHora.Caption := FormatDateTime(LBL_FECHA_HORA,NowBase(DMConnections.BaseCAC));

            spObtenerReporteCaratulaConvenio.Parameters.ParamByName('@CodigoConvenio').Value := FDatosConvenio.CodigoConvenio;
            spObtenerReporteCaratulaConvenio.Open;

            RBInterface.Caption := TITLE_CONVENIO_CARATULA;
            RBInterface.Report := ppRptCaratulaConvenio;
            if PrepararRBInterface(DescError) then begin
                RBInterface.Execute(True);
                Result := True;
            end;
        end else DescError := Format(ERROR_PARAMETRO_GENERAL,[CAN_COPIAS_CARATULA]);

        if not Result then MsgBoxErr(ERROR_EN_PROCESO, DescError, Self.Caption, MB_ICONERROR);
    except
        On E:Exception do begin
            MsgBoxErr(ERROR_EN_PROCESO, E.Message, Self.Caption, MB_ICONERROR);
        end;
    end;
end;
}
function TFormImprimirConvenio.ImprimirAltaConvenio(ImprimirCaratula, ImprimirContrato, ImprimirMandato: boolean; Cuentas: array of TCuentaVehiculo; DatosConvenio: TDatosConvenio = nil): Boolean;
resourcestring
    ERROR_EN_PROCESO = 'Se produjo un Error en la Impresi�n del Convenio.';
begin
   try
        if Not assigned(FDatosConvenio) then FDatosConvenio := DatosConvenio;

        if ImprimirCaratula then
            CrearCaratulaConvenio(Cuentas)
        else
            ContratoCaratula.Visible:= False;
        ContratoAnexoVehiculos.Visible := False;
        if ImprimirContrato then
        begin
            CrearAltaConvenio(Cuentas);
            if Length(Cuentas) > 5 then
            begin
                CrearAnexoVehiculosConvenio(Cuentas);
                ContratoAnexoVehiculos.Visible := True;
            end
        end
        else
            ContratoAlta.Visible := False;

        if ImprimirMandato AND (FDatosConvenio.MedioPago.MedioPago.TipoMedioPago <> NINGUNO) then
            CrearMandatoPACPAT()
        else
            ContratoMandatoPACPAT.Visible := False;
                                   
        if ImprimirCaratula or ImprimirContrato or ImprimirMandato then
        begin
            rptAltaContrato.PrinterSetup.Copies := 1;
            rbInterface.Report := rptAltaContrato;
            // Inicia la impresion...
            Result := Imprimir(tthBlanco, 1, True, '');
            //rbInterface.Report := ppReporteImpresionConvenio;
        end;
    except
        On E:Exception do begin
            MsgBoxErr(ERROR_EN_PROCESO, E.Message, Self.Caption, MB_ICONERROR);
        end;
    end;
end;

function TFormImprimirConvenio.CrearCaratulaConvenio(Cuentas: array of TCuentaVehiculo):Boolean;
    Resourcestring
        LBL_TITULO = 'Car�tula para la Carpeta del Cliente - Persona %s';
        LBL_PERSONA_JURIDICA = 'Jur�dica';
        LBL_PERSONA_NATURAL = 'Natural';
        LBL_FECHA_HORA = 'dd/mm/yyyy hh:nn';
        ERROR_EN_PROCESO = 'Se produjo un Error en la Impresi�n de la Car�tula del Convenio.';
var
    spCaratulaConvenio:TADOStoredProc;
    dSource : TDataSource;
    dPipeLine : TppDBPipeline;
begin

    try
        CaratulaNumeroConvenio.Caption := FDatosConvenio.NumeroConvenioFormateado;

        // Asignamos el T�tulo y el nombre formateado, dependiendo de la Personer�a.
        if FDatosConvenio.Cliente.Datos.Personeria = PERSONERIA_JURIDICA then begin
            CaratulaTitulo.Caption := Format(LBL_TITULO,[LBL_PERSONA_JURIDICA]);
            CaratulaNombreCliente.Caption := FDatosConvenio.Cliente.Datos.RazonSocial + ' (' +
                                            FDatosConvenio.Cliente.Datos.Apellido +
                                            FDatosConvenio.Cliente.Datos.Nombre + ')';
            CaratulaGiroCliente.Caption := FDatosConvenio.Cliente.Datos.Giro;
        end else begin
            CaratulaTitulo.Caption := Format(LBL_TITULO,[LBL_PERSONA_NATURAL]);
            CaratulaNombreCliente.Caption := FDatosConvenio.Cliente.Datos.Apellido + ' ' +
                                            FDatosConvenio.Cliente.Datos.ApellidoMaterno + ', ' +
                                            FDatosConvenio.Cliente.Datos.Nombre;
            CaratulaLblGiro.Visible := False;
            CaratulaGiroCliente.Visible := False;
        end;

		{INICIO		: 20160928 CFU TASK_043_CFU_20160928
        CaratulaRutCliente.Caption := FDatosConvenio.Cliente.Datos.NumeroDocumento;
        }
        CaratulaRutCliente.Caption := QueryGetValue(DMConnections.BaseCAC, 'select dbo.FormatearRutConPuntos(''' + Trim(FDatosConvenio.Cliente.Datos.NumeroDocumento) + ''')');
		//TERMINO	: 20160928 CFU TASK_043_CFU_20160928
        
        CaratulaGiroCliente.Caption := FDatosConvenio.Cliente.Datos.Giro;

        CaratulaPuntoEntrega.Caption := ObtenerDescripcionPuntoEntrega(DMConnections.BaseCAC,FDatosConvenio.PuntoEntrega);
        CaratulaOperador.Caption := UpperCase(UsuarioSistema);
        CaratulaFechaHora.Caption := FormatDateTime(LBL_FECHA_HORA,NowBase(DMConnections.BaseCAC));

        spCaratulaConvenio := TADOStoredProc.Create(nil);
        spCaratulaConvenio.Connection := DMConnections.BaseCAC;
        spCaratulaConvenio.ProcedureName := 'ObtenerReporteCaratulaConvenio';
        spCaratulaConvenio.Parameters.Refresh;
        spCaratulaConvenio.Parameters.ParamByName('@CodigoConvenio').Value := FDatosConvenio.CodigoConvenio;
        spCaratulaConvenio.Open;

        dSource := TDataSource.Create(nil);
        dSource.DataSet := spCaratulaConvenio;

        dPipeLine := TppDBPipeline.Create(nil);
        dPipeLine.DataSource := dSource;

        ContratoCaratula.DataPipeline := dPipeLine;

        //spCaratulaConvenio.Close;

        Result := True;

    except
        On E:Exception do begin
            MsgBoxErr(ERROR_EN_PROCESO, E.Message, Self.Caption, MB_ICONERROR);
        end;
    end;
end;

function TFormImprimirConvenio.CrearAltaConvenio(Cuentas: array of TCuentaVehiculo):boolean;
resourcestring
    ERROR_EN_PROCESO = 'Se produjo un Error en la Impresi�n de la Car�tula del Convenio.';
var
    varDireccion : string;
    varDSource : TDataSource;
    dpipeLine : TppDBPipeline;
    FechaDeHoy : TDateTime;
    CostoArriendo: Integer;
    CostoEmail : Integer;
    valorArriendo: Integer;
    valorEmail: Integer;
    i: Integer;
    Condiciones: string;
    EmailConcesionaria: string;
    URLConcesionaria: string;
    sp: TADOStoredProc;
    si : Boolean;
    RepresentantesLegales: TRepresentantesLegales; //INICIO: TASK_040_JMA_20160711
begin
    try

//DATOS BASICOS------------------------------------------------------------------------------
        NumeroConvenio1.Caption := FDatosConvenio.NumeroConvenio;

{INICIO:TASK_018_JMA_20160606}
        ContratoLblVersion.Caption := QueryGetValue(DMConnections.BaseCAC, 'select dbo.ObtenerVersionAnexo1(' + IntToStr(FDatosConvenio.CodigoConvenio) + ')');
{TERMINO: TASK_018_JMA_20160606}


		{INICIO		: 20160928 CFU TASK_043_CFU_20160928
        ContratoRUT.Caption := Trim(FDatosConvenio.Cliente.Datos.NumeroDocumento);
        }
        ContratoRUT.Caption := QueryGetValue(DMConnections.BaseCAC, 'select dbo.FormatearRutConPuntos(''' + Trim(FDatosConvenio.Cliente.Datos.NumeroDocumento) + ''')');
        //TERMINO	: 20160928 CFU TASK_043_CFU_20160928
        varDireccion := '';
        if FDatosConvenio.Cliente.Datos.Personeria = 'J' then
        begin
{INIICO: TASK_045_JMA_20160805
            plblNombreCliente.Caption:='Raz�n Social: ' + Trim(FDatosConvenio.Cliente.Datos.Nombre) + ' ' + Trim(FDatosConvenio.Cliente.Datos.Apellido) + ' ' + Trim(FDatosConvenio.Cliente.Datos.ApellidoMaterno);
}
            plblNombreCliente.Caption:='Raz�n Social: ' + Trim(FDatosConvenio.Cliente.Datos.RazonSocial);
{TERMINO: TASK_045_JMA_20160805}
            varDireccion := Trim(FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.Descripcion) + ' ' + Trim(FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.NumeroCalleSTR);
            if Length(Trim(FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.Dpto)) > 0 then
                varDireccion := varDireccion + ' ' + '    DEPTO: ' + Trim(FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.Dpto);
            varDireccion := varDireccion + ', ' + Trim(FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.CodigoPostal);
            varDireccion := varDireccion + ' ' + Trim(BuscarDescripcionComuna(DMConnections.BaseCAC,FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.CodigoPais,
                        FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.CodigoRegion,
                        FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.CodigoComuna));
            ContratoGiro.Text := FDatosConvenio.Cliente.Datos.Giro;
        end
        else begin
             plblNombreCliente.Caption:='Nombre Completo: ' + Trim(FDatosConvenio.Cliente.Datos.Nombre) + ' ' + Trim(FDatosConvenio.Cliente.Datos.Apellido) + ' ' + Trim(FDatosConvenio.Cliente.Datos.ApellidoMaterno);
             varDireccion := Trim(FDatosConvenio.Cliente.Domicilios.DomicilioPrincipal.Descripcion) + ' ' + Trim(FDatosConvenio.Cliente.Domicilios.DomicilioPrincipal.NumeroCalleSTR);
             if Length(Trim(FDatosConvenio.Cliente.Domicilios.DomicilioPrincipal.Dpto)) > 0 then
                varDireccion := varDireccion + ' ' + '    DEPTO: ' + Trim(FDatosConvenio.Cliente.Domicilios.DomicilioPrincipal.Dpto);
             if Length(Trim(FDatosConvenio.Cliente.Domicilios.DomicilioPrincipal.CodigoPostal)) > 0 then
                 varDireccion := varDireccion + ', ' + Trim(FDatosConvenio.Cliente.Domicilios.DomicilioPrincipal.CodigoPostal);
             varDireccion := varDireccion + ', ' + Trim(BuscarDescripcionComuna(DMConnections.BaseCAC,FDatosConvenio.Cliente.Domicilios.DomicilioPrincipal.CodigoPais,
                                                         FDatosConvenio.Cliente.Domicilios.DomicilioPrincipal.CodigoRegion,
                                                         FDatosConvenio.Cliente.Domicilios.DomicilioPrincipal.CodigoComuna));
             ContratoGiro.Visible := False;
             ContratoLblGiro.Visible:= False;
        End;

        varDireccion := varDireccion + ', ' + BuscarDescripcionRegion(DMConnections.BaseCAC,FDatosConvenio.Cliente.Domicilios.DomicilioPrincipal.CodigoPais,
        FDatosConvenio.Cliente.Domicilios.DomicilioPrincipal.CodigoRegion);
        Direccion1.Caption :=  UpperCase(varDireccion);

        if FDatosConvenio.Cliente.Telefonos.TieneMedioPrincipal then
            Telefono1.Caption := StrRight('0'+trim(inttostr(FDatosConvenio.Cliente.Telefonos.MedioPrincipal.CodigoArea)),2) + ' ' +
                                                FDatosConvenio.Cliente.Telefonos.MedioPrincipal.valor
        else if FDatosConvenio.Cliente.Telefonos.TieneMedioSecundario then
            Telefono1.Caption := StrRight('0'+trim(inttostr(FDatosConvenio.Cliente.Telefonos.MedioSecundario.CodigoArea)),2) + ' ' +
                                                FDatosConvenio.Cliente.Telefonos.MedioSecundario.valor;

        if FDatosConvenio.Cliente.Email.TieneMedioPrincipal then
            Email1.Caption := trim(FDatosConvenio.Cliente.Email.MedioPrincipal.Valor)
        else if FDatosConvenio.Cliente.Email.TieneMedioSecundario then
            Email1.Caption := trim(FDatosConvenio.Cliente.Email.MedioSecundario.Valor);

{INICIO: INICIO: TASK_040_JMA_20160711}
//REPRESENTANTES LEGALES---------------------------------------------------------------------------

         RepresentantesLegales := TPersonaJuridica(FDatosConvenio.Cliente).RepresentantesLegales;
         if (FDatosConvenio.Cliente.Datos.Personeria = 'J') AND (RepresentantesLegales.CantidadRepresentantes > 0) then
         begin
            for i := 0 to RepresentantesLegales.CantidadRepresentantes - 1 do
            begin
                dsRepresentantesLegales.Append;
				{INICIO		: 20160928 CFU TASK_043_CFU_20160928
                dsRepresentantesLegales.FieldByName('RUTRepresentante').Value :=  RepresentantesLegales.Representantes[i].Datos.NumeroDocumento;
        		}
                dsRepresentantesLegales.FieldByName('RUTRepresentante').Value :=  QueryGetValue(DMConnections.BaseCAC, 'select dbo.FormatearRutConPuntos(''' + Trim(RepresentantesLegales.Representantes[i].Datos.NumeroDocumento) + ''')');
                //TERMINO	: 20160928 CFU TASK_043_CFU_20160928

                dsRepresentantesLegales.FieldByName('NombreRepresentante').Value :=  Trim(RepresentantesLegales.Representantes[i].Datos.Nombre) + ' ' +
                                                                                    Trim(RepresentantesLegales.Representantes[i].Datos.Apellido) + ' ' +
                                                                                    Trim(RepresentantesLegales.Representantes[i].Datos.ApellidoMaterno);
            end;
            ContratoAnexoRepresentantesLegales.Visible:= True;
            varDSource := TDataSource.Create(nil);
            varDSource.DataSet := dsRepresentantesLegales;
            dpipeLine := TppDBPipeline.Create(nil);
            dpipeLine.DataSource := varDSource;
            ContratoAnexoRepresentantesLegales.DataPipeline := dpipeLine;
         end;   
{TERMINO: INICIO: TASK_040_JMA_20160711}

//MEDIO PAGO---------------------------------------------------------------------------------------

        if FDatosConvenio.MedioPago.MedioPago.TipoMedioPago <> NINGUNO then
        begin
            if FDatosConvenio.MedioPago.MedioPago.TipoMedioPago = PAT then
                MetodoPago1.Caption := 'PAT (Pago Autom�tico de Tarjeta)'
            else if FDatosConvenio.MedioPago.MedioPago.TipoMedioPago = PAC then
                MetodoPago1.Caption := 'PAC (Pago Autom�tico de Cuenta)';

            {INICIO		: 20160928 CFU TASK_043_CFU_20160928
            RUTTitular1.Caption := Trim(FDatosConvenio.MedioPago.Mandante.Datos.NumeroDocumento);
            }
            RUTTitular1.Caption := QueryGetValue(DMConnections.BaseCAC, 'select dbo.FormatearRutConPuntos(''' + Trim(FDatosConvenio.MedioPago.Mandante.Datos.NumeroDocumento) + ''')');
            //TERMINO	: 20160928 CFU TASK_043_CFU_20160928
            if FDatosConvenio.Cliente.Datos.Personeria = 'J' then
                NombreTitular1.Caption := Trim(FDatosConvenio.MedioPago.Mandante.Datos.RazonSocial)
            else
                NombreTitular1.Caption := Trim(FDatosConvenio.MedioPago.Mandante.Datos.Nombre) + ' ' + Trim(FDatosConvenio.MedioPago.Mandante.Datos.Apellido) + ' ' + Trim(FDatosConvenio.MedioPago.Mandante.Datos.ApellidoMaterno);
        end
        else
            MetodoPago1.Caption := 'NINGUNO';

//MEDIO ENVIO ELECTRONICO---------------------------------------------------------------------------
        si := False;
        If FDatosConvenio.MediosEnvioPago.CantidadMediosEnvio>0 then
        begin
            for i:=0 to FDatosConvenio.MediosEnvioPago.CantidadMediosEnvio - 1 do
            begin
                if FDatosConvenio.MediosEnvioPago.MediosEnvio[i].CodigoTipoMedioEnvio = 2 then
                    si := true;
            end;
        end;

        if si then
        begin
            imgEmisionSI.Visible := True;
//            if FDatosConvenio.Cliente.Email.TieneMedioPrincipal then
//                EmailFormaPago1.Caption := trim(FDatosConvenio.Cliente.Email.MedioPrincipal.Valor)
//            else if FDatosConvenio.Cliente.Email.TieneMedioSecundario then
//                EmailFormaPago1.Caption := trim(FDatosConvenio.Cliente.Email.MedioSecundario.Valor);
            end
        else
            imgEmisionNO.visible := True;

//PARRAFOS FINALES---------------------------------------------------------------------
        FechaDeHoy := NowBase(DMConnections.BaseCAC);
//        CostoArriendo	:= QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.CONST_CODIGO_CONCEPTO_ARRIENDO_TELEVIA(dbo.ObtenerConcesionariaNativa())');               //TASK_034_JMA_20160706
        CostoArriendo	:= QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.CONST_CODIGO_CONCEPTO_ARRIENDO_TELEVIA_CUOTAS_POSTAL(dbo.ObtenerConcesionariaNativa())'); //TASK_034_JMA_20160706
        CostoEMail := QueryGetValueInt(DMConnections.BaseCAC, 'SELECT dbo.CONST_CODIGO_CONCEPTO_ARRIENDO_TELEVIA_CUOTAS_MAIL(dbo.ObtenerConcesionariaNativa())');
        valorArriendo:= QueryGetValueInt(DMConnections.BaseCAC, Format('SELECT dbo.ObtenerImporteTotalConceptoMovimiento(%d, ''%s'')', [CostoArriendo, FormatDateTime('yyyymmdd', FechaDeHoy)]));
        valorEmail:= QueryGetValueInt(DMConnections.BaseCAC, Format('SELECT dbo.ObtenerImporteTotalConceptoMovimiento(%d, ''%s'')', [CostoEmail, FormatDateTime('yyyymmdd', FechaDeHoy)]));

        sp:= TADOStoredProc.Create(nil);
        sp.Connection := DMConnections.BaseCAC;
        sp.ProcedureName := 'COMM_Concesionarias_SELECT';
        sp.Parameters.Refresh;
        sp.Parameters.ParamByName('@CodigoConcesionaria').Value := FDatosConvenio.CodigoConcesionaria;
        sp.Open;
        sp.ExecProc;
        if not sp.Eof then
        begin
            sp.Next;
            EmailConcesionaria := sp.FieldByName('Email').AsString;
            URLConcesionaria   := sp.FieldByName('URL').AsString;
        end;
        sp.Close;

        Condiciones := ContratoCondiciones.RichText;
{INICIO: TASK_040_JMA_20160711
        Condiciones := StringReplace(Condiciones,'<#EMAIL>', EmailConcesionaria, [rfIgnoreCase]);
TERMINO: TASK_040_JMA_20160711}
        Condiciones := StringReplace(Condiciones,'<#URL_CONCESIONARIA>', URLConcesionaria, [rfIgnoreCase]);
        Condiciones := StringReplace(Condiciones,'<#fee_Completo>', FloatToStr(valorArriendo/100), [rfIgnoreCase]);
        Condiciones := StringReplace(Condiciones,'<#fee_Completo-Descuento>', FloatToStr(valorEmail/100), [rfIgnoreCase]);
        ContratoCondiciones.RichText := Condiciones;

        //MontoArrendamiento1.Caption := Format('%f',[valorArriendo/100]);
        //MontoElectronico1.Caption := Format('%f',[valorEmail/100]);

        Usuario1.Caption := Trim(QueryGetStringValue(DMConnections.BaseCAC, Format('SELECT dbo.ObtenerNombreUsuario(%s)', [QuotedStr(UsuarioSistema)]))) + '.';

//DETALLE---------------------------------------------------------------
        dsContratoArrendamientoTAG.Active := True;
        dsContratoArrendamientoTAG.EmptyDataSet;

        for i := 0 to Length(Cuentas) - 1 do
        begin
            dsContratoArrendamientoTAG.Append;
            dsContratoArrendamientoTAG.FieldByName('Patente').Value := Cuentas[i].Vehiculo.Patente;
            dsContratoArrendamientoTAG.FieldByName('ClaseVehiculo').Value := Cuentas[i].Vehiculo.Tipo;
            dsContratoArrendamientoTAG.FieldByName('Marca').Value := Cuentas[i].Vehiculo.Marca;
            dsContratoArrendamientoTAG.FieldByName('Modelo').Value := Cuentas[i].Vehiculo.Modelo;
            dsContratoArrendamientoTAG.FieldByName('SerialTAG').Value := SerialNumberToEtiqueta(Cuentas[i].ContactSerialNumber);
            dsContratoArrendamientoTAG.FieldByName('FechaInstalacion').Value := formatdatetime('dd/mm/yyyy',Cuentas[i].FechaAltaTag);
            if i = 5 then
                break;
        end;

 //CREAR REPORTE---------------------------------------------------------
        varDSource := TDataSource.Create(nil);
        varDSource.DataSet := dsContratoArrendamientoTAG;
        dpipeLine := TppDBPipeline.Create(nil);
        dpipeLine.DataSource := varDSource;
        ContratoAlta.DataPipeline := dpipeLine;

        Result := True;

    except
        On E:Exception do begin
            MsgBoxErr(ERROR_EN_PROCESO, E.Message, Self.Caption, MB_ICONERROR);
        end;
    end;
end;

function TFormImprimirConvenio.CrearAnexoVehiculosConvenio(Cuentas: array of TCuentaVehiculo): Boolean;
resourcestring
    ERROR_EN_PROCESO = 'Se produjo un Error en la Impresi�n de la Car�tula del Convenio.';
var
    varDSource : TDataSource;
    dataCliente: TClientDataSet;
    dpipeLine : TppDBPipeline;
    i: Integer;
begin
    try
        varDSource := TDataSource.Create(nil);

        NumeroConvenio2.Caption := FDatosConvenio.NumeroConvenio;
        AnexoAltaVersion.Caption := IntToStr(FDatosConvenio.NroAnexoEmitido);

        dsAnexoVehiculosContrato.Active := True;
        dsAnexoVehiculosContrato.EmptyDataSet;

        for i := 10 to Length(Cuentas) do
        begin
            dsAnexoVehiculosContrato.Append;
            dsAnexoVehiculosContrato.FieldByName('Patente').Value := Cuentas[i].Vehiculo.Patente;
            dsAnexoVehiculosContrato.FieldByName('ClaseVehiculo').Value := Cuentas[i].Vehiculo.Tipo;
            dsAnexoVehiculosContrato.FieldByName('Marca').Value := Cuentas[i].Vehiculo.Marca;
            dsAnexoVehiculosContrato.FieldByName('Modelo').Value := Cuentas[i].Vehiculo.Modelo;
            dsAnexoVehiculosContrato.FieldByName('SerialTAG').Value := SerialNumberToEtiqueta(Cuentas[i].ContactSerialNumber);
            dsAnexoVehiculosContrato.FieldByName('FechaInstalacion').Value := formatdatetime('dd/mm/yyyy',Cuentas[i].FechaAltaTag);
        end;

        varDSource := TDataSource.Create(nil);
        varDSource.DataSet := dsAnexoVehiculosContrato;
        dpipeLine := TppDBPipeline.Create(nil);
        dpipeLine.DataSource := varDSource;
        ContratoAnexoVehiculos.DataPipeline := dpipeLine;

        Result := True;

    except
        On E:Exception do begin
            MsgBoxErr(ERROR_EN_PROCESO, E.Message, Self.Caption, MB_ICONERROR);
        end;
    end;
end;

function TFormImprimirConvenio.CrearMandatoPACPAT():Boolean;
var
    Texto: string;
    sp: TADOStoredProc;
    nombreConcesionaria: string;
begin
           
    sp:= TADOStoredProc.Create(nil);
    sp.Connection := DMConnections.BaseCAC;
    sp.ProcedureName := 'COMM_Concesionarias_SELECT';
    sp.Parameters.Refresh;
    sp.Parameters.ParamByName('@CodigoConcesionaria').Value := FDatosConvenio.CodigoConcesionaria;
    sp.Open;
    sp.ExecProc;
    if not sp.Eof then
    begin
        sp.Next;
        nombreConcesionaria := sp.FieldByName('Descripcion').AsString;
    end;
    sp.Close;

    Texto:= ContratoTextoMandatoPACPAT.RichText;
    Texto := StringReplace(Texto,'<#CUENTA_COMERCIAL>', Trim(FDatosConvenio.NumeroConvenio), [rfReplaceAll,rfIgnoreCase]);
    Texto := StringReplace(Texto,'<#NOMBRE_COMERCIAL_CONCESIONARIA>', Trim(nombreConcesionaria), [rfReplaceAll,rfIgnoreCase]);
    ContratoTextoMandatoPACPAT.RichText := Texto;

    Texto:= ContratoTextoCondiciones2.RichText;
    Texto := StringReplace(Texto,'<#CUENTA_COMERCIAL>', Trim(FDatosConvenio.NumeroConvenio), [rfReplaceAll,rfIgnoreCase]);
    
    if FDatosConvenio.MedioPago.Mandante.Datos.Personeria = PERSONERIA_JURIDICA then begin

        Texto := StringReplace(Texto,'<#PAGO_NOMBRE>', Trim(FDatosConvenio.MedioPago.Mandante.Datos.RazonSocial), [rfReplaceAll,rfIgnoreCase]);
    end
    else begin

        Texto := StringReplace(Texto,'<#PAGO_NOMBRE>', Trim(FDatosConvenio.MedioPago.Mandante.Datos.Nombre) + ' ' +
                                                        Trim(FDatosConvenio.MedioPago.Mandante.Datos.Apellido) + ' ' +
                                                        Trim(FDatosConvenio.MedioPago.Mandante.Datos.ApellidoMaterno), [rfReplaceAll,rfIgnoreCase]);
    end;
    {INICIO		: 20160928 CFU TASK_043_CFU_20160928
    Texto := StringReplace(Texto,'<#PAGO_RUT>', Trim(FDatosConvenio.MedioPago.Mandante.Datos.NumeroDocumento), [rfReplaceAll,rfIgnoreCase]);
    }
    Texto := StringReplace(Texto,'<#PAGO_RUT>', QueryGetValue(DMConnections.BaseCAC, 'select dbo.FormatearRutConPuntos(''' + Trim(FDatosConvenio.MedioPago.Mandante.Datos.NumeroDocumento) + ''')'), [rfReplaceAll,rfIgnoreCase]);
    //TERMINO	: 20160928 CFU TASK_043_CFU_20160928
    //if FDatosConvenio.Cliente.Telefonos.TieneMedioPrincipal then              //TASK_033_JMA_20160706
    if FDatosConvenio.MedioPago.Mandante.Telefonos.TieneMedioPrincipal then     //TASK_033_JMA_20160706
        Texto := StringReplace(Texto,'<#PAGO_TELEFONO>', StrRight('0'+trim(inttostr(FDatosConvenio.MedioPago.Mandante.Telefonos.MedioPrincipal.CodigoArea)),2) + ' ' +
                                                        FDatosConvenio.MedioPago.Mandante.Telefonos.MedioPrincipal.valor, [rfReplaceAll,rfIgnoreCase])
   //else if FDatosConvenio.Cliente.Telefonos.TieneMedioSecundario then           //TASK_033_JMA_20160706
    else if FDatosConvenio.MedioPago.Mandante.Telefonos.TieneMedioSecundario then //TASK_033_JMA_20160706
        Texto := StringReplace(Texto,'<#PAGO_TELEFONO>', StrRight('0'+trim(inttostr(FDatosConvenio.MedioPago.Mandante.Telefonos.MedioSecundario.CodigoArea)),2) + ' ' +
                                                        FDatosConvenio.MedioPago.Mandante.Telefonos.MedioSecundario.valor, [rfReplaceAll,rfIgnoreCase])
    else
        Texto := StringReplace(Texto,'<#PAGO_TELEFONO>', ' ', [rfReplaceAll,rfIgnoreCase]);

    sp:= TADOStoredProc.Create(nil);
    sp.Connection := DMConnections.BaseCAC;
    sp.ProcedureName := 'ObtenerDatosMedioPagoAutomaticoConvenio';
    sp.Parameters.Refresh;
    sp.Parameters.ParamByName('@CodigoConvenio').Value := FDatosConvenio.CodigoConvenio;
    sp.Open;
    sp.ExecProc;
    if not sp.Eof then
    begin
        sp.First;
        if FDatosConvenio.MedioPago.MedioPago.TipoMedioPago = PAC then
        begin
            Texto := StringReplace(Texto,'<#PAGO_TIPO_CUENTA>', Trim(sp.FieldByName('NombreTipoCuentaBancaria').Value), [rfReplaceAll,rfIgnoreCase]);
            Texto := StringReplace(Texto,'<#PAGO_NUMERO_CUENTA>', Trim(sp.FieldByName('NroCuentaBancaria').Value), [rfReplaceAll,rfIgnoreCase]);
            Texto := StringReplace(Texto,'<#PAGO_BANCO>', Trim(sp.FieldByName('NombreBanco').Value), [rfReplaceAll,rfIgnoreCase]);

            Texto := StringReplace(Texto,'<#PAGO_NUMERO_TARJETA>', ' ', [rfReplaceAll,rfIgnoreCase]);
            Texto := StringReplace(Texto,'<#PAGO_TIPO_TARJETA>', ' ', [rfReplaceAll,rfIgnoreCase]);
            Texto := StringReplace(Texto,'<#PAGO_FECHA_VENCIMIENTO>', ' ', [rfReplaceAll,rfIgnoreCase]);
            Texto := StringReplace(Texto,'<#PAGO_BANCO_EMISOR>', ' ', [rfReplaceAll,rfIgnoreCase]);
        end
        else begin
            Texto := StringReplace(Texto,'<#PAGO_TIPO_CUENTA>', ' ', [rfReplaceAll,rfIgnoreCase]);
            Texto := StringReplace(Texto,'<#PAGO_NUMERO_CUENTA>', ' ', [rfReplaceAll,rfIgnoreCase]);
            Texto := StringReplace(Texto,'<#PAGO_BANCO>', ' ', [rfReplaceAll,rfIgnoreCase]);

            Texto := StringReplace(Texto,'<#PAGO_NUMERO_TARJETA>', Trim(sp.FieldByName('NumeroTarjetaCredito').Value), [rfReplaceAll,rfIgnoreCase]);
{INICIO: TASK_037_JMA_20160707
            Texto := StringReplace(Texto,'<#PAGO_TIPO_TARJETA>', Trim(sp.FieldByName('NumeroTarjetaCredito').Value), [rfReplaceAll,rfIgnoreCase]);
}
            Texto := StringReplace(Texto,'<#PAGO_TIPO_TARJETA>', Trim(sp.FieldByName('NombreTipoTarjetaCredito').Value), [rfReplaceAll,rfIgnoreCase]);
{TERMINO: TASK_037_JMA_20160707}
            Texto := StringReplace(Texto,'<#PAGO_FECHA_VENCIMIENTO>', Trim(sp.FieldByName('FechaVencimiento').Value), [rfReplaceAll,rfIgnoreCase]);
            Texto := StringReplace(Texto,'<#PAGO_BANCO_EMISOR>', Trim(sp.FieldByName('NombreEmisor').Value), [rfReplaceAll,rfIgnoreCase]);      
        end;
    end;
    sp.Close;

    Texto := StringReplace(Texto,'<#PAGO_DIA>', VarToStr(DayOfTheMonth(Now)), [rfReplaceAll,rfIgnoreCase]);
    Texto := StringReplace(Texto,'<#PAGO_MES>', VarToStr(MesNombre(Now)), [rfReplaceAll,rfIgnoreCase]);
    Texto := StringReplace(Texto,'<#PAGO_ANIO>', VarToStr(Year(Now)), [rfReplaceAll,rfIgnoreCase]);

    ContratoTextoCondiciones2.RichText := Texto;
end;


function TFormImprimirConvenio.ImprimirBajaConvenio(): Boolean;
resourcestring
    ERROR_EN_PROCESO = 'Se produjo un Error en la Impresi�n del Convenio.';
begin

     try
        //Result := CrearCaratulaBajaConvenio();

        //if not Result then
         //   Exit;
        ContratoBajaCaratula.Visible := False;
        
        Result := CrearBajaConvenio();

        if not Result then
            Exit;

        if FDatosConvenio.Cuentas.CantidadVehiculosEliminados > 10 then
            Result := CrearBajaAnexoVehiculosConvenio()
        else
        begin
            ContratoBajaAnexo.Visible := False;
        end;

        if not Result then
            Exit;

        rptBajaContrato.PrinterSetup.Copies := 1;
        rbInterface.Report := rptBajaContrato;
        // Inicia la impresion...
        Result := Imprimir(tthBlanco, 1, True, '');
        rbInterface.Report := ppReporteImpresionConvenio;

        Result := True;

    except
        On E:Exception do begin
            MsgBoxErr(ERROR_EN_PROCESO, E.Message, Self.Caption, MB_ICONERROR);
        end;
    end;

end;

function TFormImprimirConvenio.CrearCaratulaBajaConvenio:Boolean;
    Resourcestring
        LBL_TITULO = 'Car�tula para la Carpeta del Cliente - Persona %s';
        LBL_PERSONA_JURIDICA = 'Jur�dica';
        LBL_PERSONA_NATURAL = 'Natural';
        LBL_FECHA_HORA = 'dd/mm/yyyy hh:nn';
var
    spCaratulaConvenio:TADOStoredProc;
    dSource : TDataSource;
    dPipeLine : TppDBPipeline;
    i: Integer;
begin

    BajaCaratulaNumeroConvenio.Caption := FDatosConvenio.NumeroConvenioFormateado;

    // Asignamos el T�tulo y el nombre formateado, dependiendo de la Personer�a.
    if FDatosConvenio.Cliente.Datos.Personeria = PERSONERIA_JURIDICA then begin
        BajaCaratulaTitulo.Caption := Format(LBL_TITULO,[LBL_PERSONA_JURIDICA]);
        BajaCaratulaNombreCliente.Caption := Trim(FDatosConvenio.Cliente.Datos.RazonSocial) + ' (' +
                                        Trim(FDatosConvenio.Cliente.Datos.Apellido) +
                                        Trim(FDatosConvenio.Cliente.Datos.Nombre) + ')';
        BajaCaratulaGiroCliente.Caption := FDatosConvenio.Cliente.Datos.Giro;
    end else begin
        BajaCaratulaTitulo.Caption := Format(LBL_TITULO,[LBL_PERSONA_NATURAL]);
        BajaCaratulaNombreCliente.Caption := Trim(FDatosConvenio.Cliente.Datos.Apellido) + ' ' +
                                        Trim(FDatosConvenio.Cliente.Datos.ApellidoMaterno) + ', ' +
                                        Trim(FDatosConvenio.Cliente.Datos.Nombre);
        BajaCaratulaLblGiro.Visible := False;
        BajaCaratulaGiroCliente.Visible := False;
    end;

    {INICIO		: 20160928 CFU TASK_043_CFU_20160928
    BajaCaratulaRutCliente.Caption := FDatosConvenio.Cliente.Datos.NumeroDocumento;
    }
    BajaCaratulaRutCliente.Caption := QueryGetValue(DMConnections.BaseCAC, 'select dbo.FormatearRutConPuntos(''' + Trim(FDatosConvenio.Cliente.Datos.NumeroDocumento) + ''')');
    //TERMINO	: 20160928 CFU TASK_043_CFU_20160928
    BajaCaratulaGiroCliente.Caption := FDatosConvenio.Cliente.Datos.Giro;

    BajaCaratulaPuntoEntrega.Caption := ObtenerDescripcionPuntoEntrega(DMConnections.BaseCAC,FDatosConvenio.PuntoEntrega);
    BajaCaratulaOperador.Caption := UpperCase(UsuarioSistema);
    BajaCaratulaFechaHora.Caption := FormatDateTime(LBL_FECHA_HORA,NowBase(DMConnections.BaseCAC));

    
    dsDocumentacion.Active := True;
    dsDocumentacion.EmptyDataSet;
    for i := 0 to FDatosConvenio.DocumentacionConvenio.CantidadDocumentacion do
    begin
        dsDocumentacion.Append;
        dsDocumentacion.FieldByName('LineaCaratula').Value := FDatosConvenio.DocumentacionConvenio.DarDocumentacion(i).Descripcion;
    end;

    dSource := TDataSource.Create(nil);
    dSource.DataSet := dsDocumentacion;

    dPipeLine := TppDBPipeline.Create(nil);
    dPipeLine.DataSource := dSource;

    ContratoBajaCaratula.DataPipeline := dPipeLine;

    Result := True;

end;

function TFormImprimirConvenio.CrearBajaConvenio():Boolean;
var
    varDireccion : string;
    varDSource : TDataSource;
    dpipeLine : TppDBPipeline;
    FechaDeHoy : TDateTime;
    CostoArriendo: Integer;
    CostoEmail : Integer;
    valorArriendo: Integer;
    valorEmail: Integer;
    i: Integer;
    codigoMotivo: Char;
    codigoEstado : SmallInt;
    modalidad : string;
begin
    BajaNumeroConvenio.Caption := FDatosConvenio.NumeroConvenio;
    {INICIO		: 20160928 CFU TASK_043_CFU_20160928
    BajaRUT.Caption := FDatosConvenio.Cliente.Datos.NumeroDocumento;
    }
    BajaRUT.Caption := QueryGetValue(DMConnections.BaseCAC, 'select dbo.FormatearRutConPuntos(''' + Trim(FDatosConvenio.Cliente.Datos.NumeroDocumento) + ''')');
    //TERMINO	: 20160928 CFU TASK_043_CFU_20160928
    BajaOficina.Caption := ObtenerDescripcionPuntoEntrega(DMConnections.BaseCAC,FDatosConvenio.PuntoEntrega);

    varDireccion := '';
    if FDatosConvenio.Cliente.Datos.Personeria = 'J' then
    begin
        BajaNombreCompleto.Caption := Trim(FDatosConvenio.Cliente.Datos.RazonSocial);
        varDireccion := Trim(FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.Descripcion) + ' ' + Trim(FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.NumeroCalleSTR);
        if Length(Trim(FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.Dpto)) > 0 then
            varDireccion := varDireccion + ' ' + ' DEPTO: ' + Trim(FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.Dpto);
        varDireccion := varDireccion + ', ' + Trim(FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.CodigoPostal);
        varDireccion := varDireccion + ' ' + BuscarDescripcionComuna(DMConnections.BaseCAC,FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.CodigoPais,
                                                                    FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.CodigoRegion,
                                                                    FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.CodigoComuna);
        BajaGiro.Caption := FDatosConvenio.Cliente.Datos.Giro;
    end
    else begin
        BajaNombreCompleto.Caption := Trim(FDatosConvenio.Cliente.Datos.Nombre) + ' ' + Trim(FDatosConvenio.Cliente.Datos.Apellido) + ' ' + Trim(FDatosConvenio.Cliente.Datos.ApellidoMaterno);
        varDireccion := Trim(FDatosConvenio.Cliente.Domicilios.DomicilioPrincipal.Descripcion) + ' ' + Trim(FDatosConvenio.Cliente.Domicilios.DomicilioPrincipal.NumeroCalleSTR);
        if Length(Trim(FDatosConvenio.Cliente.Domicilios.DomicilioPrincipal.Dpto)) > 0 then
            varDireccion := varDireccion + ' ' + ' DEPTO: ' + Trim(FDatosConvenio.Cliente.Domicilios.DomicilioPrincipal.Dpto);
        if Length(Trim(FDatosConvenio.Cliente.Domicilios.DomicilioPrincipal.CodigoPostal)) > 0 then
             varDireccion := varDireccion + ', ' + FDatosConvenio.Cliente.Domicilios.DomicilioPrincipal.CodigoPostal;
        varDireccion := varDireccion + ', ' + BuscarDescripcionComuna(DMConnections.BaseCAC,
                                                                        FDatosConvenio.Cliente.Domicilios.DomicilioPrincipal.CodigoPais,
                                                                        FDatosConvenio.Cliente.Domicilios.DomicilioPrincipal.CodigoRegion,
                                                                        FDatosConvenio.Cliente.Domicilios.DomicilioPrincipal.CodigoComuna);
        BajaLblGiro.Visible := False;
        BajaGiro.Visible := False;
    End;

    varDireccion := varDireccion + ', ' + BuscarDescripcionRegion(DMConnections.BaseCAC,
                                                                FDatosConvenio.Cliente.Domicilios.DomicilioPrincipal.CodigoPais,
                                                                FDatosConvenio.Cliente.Domicilios.DomicilioPrincipal.CodigoRegion);
    BajaDireccion.Caption :=  UpperCase(varDireccion);

    if FDatosConvenio.Cliente.Telefonos.TieneMedioPrincipal then
        BajaTelefonoPrincipal.Caption := StrRight('0'+trim(inttostr(FDatosConvenio.Cliente.Telefonos.MedioPrincipal.CodigoArea)),2) + ' ' +
                                            FDatosConvenio.Cliente.Telefonos.MedioPrincipal.valor
    else if FDatosConvenio.Cliente.Telefonos.TieneMedioSecundario then
        BajaTelefonoPrincipal.Caption := StrRight('0'+trim(inttostr(FDatosConvenio.Cliente.Telefonos.MedioSecundario.CodigoArea)),2) + ' ' +
                                            FDatosConvenio.Cliente.Telefonos.MedioSecundario.valor;

    if FDatosConvenio.Cliente.Email.TieneMedioPrincipal then
        BajaEmail.Caption := trim(FDatosConvenio.Cliente.Email.MedioPrincipal.Valor)
    else if FDatosConvenio.Cliente.Email.TieneMedioSecundario then
        BajaEmail.Caption := trim(FDatosConvenio.Cliente.Email.MedioSecundario.Valor);

    BajaUsuario.Caption := UpperCase(UsuarioSistema);

    dsContratoBajaTAG.Active := True;
    dsContratoBajaTAG.EmptyDataSet;

    for i := 0 to FDatosConvenio.Cuentas.CantidadVehiculosEliminados - 1 do
    begin

        codigoMotivo:= GetCodigoMotivoBaja(FDatosConvenio.Cuentas.VehiculosEliminados[i].Cuenta.CodigoMotivoBaja);

        codigoEstado := GetEstadoConservacionTAG(FDatosConvenio.Cuentas.VehiculosEliminados[i].Cuenta.EstadoConservacionTAG);

        modalidad := GetDescripcionModalidadContrato(FDatosConvenio.Cuentas.VehiculosEliminados[i].Cuenta.CodigoAlmacenDestino);

        dsContratoBajaTAG.Append;
        dsContratoBajaTAG.FieldByName('Patente').Value := FDatosConvenio.Cuentas.VehiculosEliminados[i].Cuenta.Vehiculo.Patente;
        dsContratoBajaTAG.FieldByName('ClaseVehiculo').Value := FDatosConvenio.Cuentas.VehiculosEliminados[i].Cuenta.Vehiculo.Tipo;
        dsContratoBajaTAG.FieldByName('Marca').Value := FDatosConvenio.Cuentas.VehiculosEliminados[i].Cuenta.Vehiculo.Marca;
        dsContratoBajaTAG.FieldByName('Modelo').Value := FDatosConvenio.Cuentas.VehiculosEliminados[i].Cuenta.Vehiculo.Modelo;
        dsContratoBajaTAG.FieldByName('NumeroTAG').Value := FDatosConvenio.Cuentas.VehiculosEliminados[i].Cuenta.SerialNumberaMostrar;
        dsContratoBajaTAG.FieldByName('Condicion').Value := IIf(FDatosConvenio.Cuentas.VehiculosEliminados[i].Cuenta.DescripcionMotivoBaja = 'No Devuelto', '2', '1');
        dsContratoBajaTAG.FieldByName('MotivoDevolucion').Value := codigoMotivo;
        dsContratoBajaTAG.FieldByName('EstadoDevolucion').Value := codigoEstado;
        dsContratoBajaTAG.FieldByName('ModalidadContrato').Value := modalidad;
        if i = 10 then
            break;
    end;

    varDSource := TDataSource.Create(nil);
    varDSource.DataSet := dsContratoBajaTAG;
    dpipeLine := TppDBPipeline.Create(nil);
    dpipeLine.DataSource := varDSource;
    ContratoBaja.DataPipeline := dpipeLine;

    Result := True;
end;

function TFormImprimirConvenio.CrearBajaAnexoVehiculosConvenio(): Boolean;
var
    varDSource : TDataSource;
    dataCliente: TClientDataSet;
    dpipeLine : TppDBPipeline;
    i: Integer;
    codigoMotivo: Char;
    codigoEstado : SmallInt;
    modalidad : string;
begin

    varDSource := TDataSource.Create(nil);

    BajaAnexoVehiculosNumeroConvenio.Caption := FDatosConvenio.NumeroConvenio;

    dsAnexoBaja.Active := True;
    dsAnexoBaja.EmptyDataSet;

    for i := 10 to FDatosConvenio.Cuentas.CantidadVehiculosEliminados - 1 do
    begin

        codigoMotivo:= GetCodigoMotivoBaja(FDatosConvenio.Cuentas.VehiculosEliminados[i].Cuenta.CodigoMotivoBaja);

        codigoEstado := GetEstadoConservacionTAG(FDatosConvenio.Cuentas.VehiculosEliminados[i].Cuenta.EstadoConservacionTAG);

        modalidad := GetDescripcionModalidadContrato(FDatosConvenio.Cuentas.VehiculosEliminados[i].Cuenta.CodigoAlmacenDestino);

        dsAnexoBaja.Append;
        dsAnexoBaja.FieldByName('Patente').Value := FDatosConvenio.Cuentas.VehiculosEliminados[i].Cuenta.Vehiculo.Patente;
        dsAnexoBaja.FieldByName('ClaseVehiculo').Value := FDatosConvenio.Cuentas.VehiculosEliminados[i].Cuenta.Vehiculo.Tipo;
        dsAnexoBaja.FieldByName('Marca').Value := FDatosConvenio.Cuentas.VehiculosEliminados[i].Cuenta.Vehiculo.Marca;
        dsAnexoBaja.FieldByName('Modelo').Value := FDatosConvenio.Cuentas.VehiculosEliminados[i].Cuenta.Vehiculo.Modelo;
        dsAnexoBaja.FieldByName('NumeroTAG').Value := FDatosConvenio.Cuentas.VehiculosEliminados[i].Cuenta.SerialNumberaMostrar;
        dsAnexoBaja.FieldByName('Condicion').Value := IIf(FDatosConvenio.Cuentas.VehiculosEliminados[i].Cuenta.DescripcionMotivoBaja = 'No Devuelto', '2', '1');
        dsAnexoBaja.FieldByName('MotivoDevolucion').Value := codigoMotivo;
        dsAnexoBaja.FieldByName('EstadoDevolucion').Value := codigoEstado;
        dsAnexoBaja.FieldByName('ModalidadContrato').Value := modalidad;
    end;

    varDSource := TDataSource.Create(nil);
    varDSource.DataSet := dsAnexoBaja;
    dpipeLine := TppDBPipeline.Create(nil);
    dpipeLine.DataSource := varDSource;
    ContratoBajaAnexo.DataPipeline := dpipeLine;

    Result := True;

end;


function TFormImprimirConvenio.GetCodigoMotivoBaja(CodigoMotivo : Integer): Char;
begin
   case CodigoMotivo of
        1: Result := 'A';
        2: Result := 'B';
        3: Result := 'C';
        4: Result := 'D';
        5: Result := 'E';
        6: Result := 'F';
        7: Result := 'G';
        else Result := ' ';
    end;
end;

function TFormImprimirConvenio.GetEstadoConservacionTAG(EstadoConservacionTAG : Integer): SmallInt;
begin
   case EstadoConservacionTAG of
        1: Result := 1;
        2: Result := 2;
        3: Result := 2;
        4: Result := 3;
        else Result := 0;
    end;
end;

function TFormImprimirConvenio.GetDescripcionModalidadContrato(CodigoAlmacenDestino : Integer): string;
begin
   case CodigoAlmacenDestino of
        CONST_ALMACEN_EN_COMODATO: Result := CONST_STR_ALMACEN_COMODATO;
        CONST_ALMACEN_ARRIENDO: Result := CONST_STR_ALMACEN_ARRIENDO;
        CONST_ALMACEN_ENTREGADO_EN_CUOTAS: Result := CONST_STR_ALMACEN_ENTREGADO_EN_CUOTAS;
   end;
end;

{TERMINO: TASK_011_JMA_201605}



{******************************** Function Header ******************************
Function Name: TFormImprimirConvenio.ImprimirConvenioPersonaNatural
Author       : dcalani
Date Created : 26/07/2004
Description  : Imprime convenio persona natural.
Parameters   : NumeroConvenio:String; DatoTitular: TDatosPersonales; PrimerMedioComunicacion:TFrameTelefono; SegundoMedioComunicacion:TFrameTelefono; EMail:String; DomicilioTitular:TDatosDomicilio; DomicilioFacturacion:TDatosDomicilio; DatosMedioPago:TDatosMediosPago; Vehiculo:Array of TCuentaVehiculo; EnviaDocumentacionxMail:Boolean; var MotivoCancelacion:TMotivoCancelacion; CantCopias:Integer; TipoHoja : TTipoHoja=True
Return Value : Boolean
*******************************************************************************}
(*function TFormImprimirConvenio.ImprimirConvenioPersonaNatural(
              NumeroConvenio:String;
              DatoTitular: TDatosPersonales;
              PrimerMedioComunicacion:TFrameTelefono;
              SegundoMedioComunicacion:TFrameTelefono;
              EMail:String;
              DomicilioTitular:TDatosDomicilio;
              DomicilioFacturacion:TDatosDomicilio;
              DatosMedioPago:TDatosMediosPago;
              Vehiculo: TRegCuentaABM;
              EnviaDocumentacionxMail:Boolean;
              FechaAlta : TDateTime;
              var MotivoCancelacion:TMotivoCancelacion;
              CantCopias:Integer): Boolean;
var
    Aux_MeidoPago,PathArchivo:String;
    i:Integer;
	aux_Vehiculo: array of TCuentaABM;
    Accion, auxDescripTipo, auxDescripCategoria, auxCategoria:String;
begin

    if not EstaFuenteMarcaInstalada then begin
        Result := False;
        MotivoCancelacion := PeaTypes.ERROR;
		exit;
    end;

    try
        MotivoCancelacion := PeaTypes.ERROR;
        ObtenerParametroGeneral(DMConnections.BaseCAC,PLANTILLA_CONVENIO_PERSONA_NATURAL,PathArchivo);

        if not(FileExists(PathArchivo)) then begin
            MsgBox(format(MSG_ERROR_ARCHIVO_ABRIR,[PathArchivo]),MSG_CAPTION_IMPRESION_CONVENIOS,MB_ICONSTOP);
            result:=False;
            Exit;
        end;

        CampoRich.LoadFromFile(PathArchivo);

        //convenio
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_CONVENIO,NumeroConvenio);
        ReplaceCodeBar(TAG_CODIGO_BARRA, NumeroConvenio, CampoRich);
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_DIA,FormatDateTime('dd',FechaAlta));
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_MES,FormatDateTime('mmmm',FechaAlta));
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_ANIO,FormatDateTime('yyyy',FechaAlta));

        //datos der la persona
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_NOMBRE,DatoTitular.Nombre);
		CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_APELLIDO_PATERNO,DatoTitular.Apellido);
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_APELLIDO_MATERNO,DatoTitular.ApellidoMaterno);
        DatoTitular.NumeroDocumento:=trim(DatoTitular.NumeroDocumento);
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_RUT,DatoTitular.NumeroDocumento);

        if (DatoTitular.FechaNacimiento=nullDate) then
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_NACIMIENTO,'')
		else
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_NACIMIENTO,FormatDateTime('dd/mm/yyyy',DatoTitular.FechaNacimiento));
		CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_SEXO,iif(DatoTitular.Sexo=SEXO_MASCULINO,UpperCase(SEXO_MASCULINO_DESCRIP),UpperCase(SEXO_FEMENINO_DESCRIP)));

        //medios de comunicacion
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TELEFONO_ETIQUETA1,PrimerMedioComunicacion.DescriTipoTelefono);
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TELEFONO_AREA1,StrRight('0'+trim(inttostr(PrimerMedioComunicacion.CodigoArea)),2));
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TELEFONO_NUMERO1,PrimerMedioComunicacion.NumeroTelefono);

        if SegundoMedioComunicacion.CodigoTipoTelefono>0 then begin
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TELEFONO_ETIQUETA2,SegundoMedioComunicacion.DescriTipoTelefono);
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TELEFONO_AREA2,StrRight('0'+trim(inttostr(SegundoMedioComunicacion.CodigoArea)),2));
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TELEFONO_NUMERO2,SegundoMedioComunicacion.NumeroTelefono);
        end else begin
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TELEFONO_ETIQUETA2,'');
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TELEFONO_AREA2,'');
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TELEFONO_NUMERO2,'');
        end;

        if trim(EMail)<>'' then begin
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_EMAIL,EMail);
        end else begin
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_EMAIL,'');
        end;

        if DomicilioFacturacion.NumeroCalle<=0 then begin// no tiene domicilio alternativo
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_CALLE_COM,Trim(UpperCase(DomicilioTitular.Descripcion)));
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_NUMERO_COM,UpperCase(DomicilioTitular.NumeroCalleSTR));
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_DETALLE_COM,UpperCase(DomicilioTitular.Detalle));
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_COMUNA_COM,UpperCase(BuscarDescripcionComuna(DMConnections.BaseCAC,DomicilioTitular.CodigoPais,DomicilioTitular.CodigoRegion,DomicilioTitular.CodigoComuna)));
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_REGION_COM,UpperCase(BuscarDescripcionRegion(DMConnections.BaseCAC,DomicilioTitular.CodigoPais,DomicilioTitular.CodigoRegion)));
			CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_CP_COM,UpperCase(DomicilioTitular.CodigoPostal));
        end else begin
			//domicilio facturacion
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_CALLE_COM,Trim(UpperCase(DomicilioFacturacion.Descripcion)));
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_NUMERO_COM,UpperCase(DomicilioFacturacion.NumeroCalleSTR));
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_DETALLE_COM,UpperCase(DomicilioFacturacion.Detalle));
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_COMUNA_COM,UpperCase(BuscarDescripcionComuna(DMConnections.BaseCAC,DomicilioFacturacion.CodigoPais,DomicilioFacturacion.CodigoRegion,DomicilioFacturacion.CodigoComuna)));
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_REGION_COM,UpperCase(BuscarDescripcionRegion(DMConnections.BaseCAC,DomicilioFacturacion.CodigoPais,DomicilioFacturacion.CodigoRegion)));
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_CP_COM,UpperCase(DomicilioFacturacion.CodigoPostal));
        end;

        //vehiculos
        for i := 0 to TAG_CON_CANTIDAD_MAX_VEHICULOS_HOJA - 1 do begin
            if i < Length(Vehiculo) then begin

                case Vehiculo[i].TipoMovimiento of
                    ttmAlta : begin Accion := 'Agregado'; end;
                    ttmBaja : Accion   := 'Eliminado';
                    ttmModif :  Accion := 'Modificado';
                    ttmModifTag: begin  Accion := 'Modificado'; end;
                    ttmRobado :  Accion := 'Robado';
                    ttmBajaSinTag  :  Accion := 'Baja S/T';
                else
                    Accion := 'ERROR';
				end;

                ObtenerDescripTipoVehiculo(DMConnections.BaseCAC,Vehiculo[i].Cuenta.Vehiculo.CodigoTipo,auxDescripTipo,auxDescripCategoria,auxCategoria);
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText, TAG_CON_ACCION_VEHICULO + Inttostr(i+1), Accion);
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText, TAG_CON_ACCION_FECHA + Inttostr(i+1), FormatDateTime('dd/mm/yy hh:nn', Vehiculo[i].Cuenta.FechaModifCuenta));
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_PATENTE_VEHICULO + Inttostr(i+1),Vehiculo[i].Cuenta.Vehiculo.Patente);
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TIPO_VEHICULO + Inttostr(i+1),FormatearTexto(auxDescripTipo,CANTIDAD_CARACTERES_TIPO));
				CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_CATE_VEHICULO + Inttostr(i+1),auxCategoria);
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_MARCA_VEHICULO + Inttostr(i+1),Vehiculo[i].Cuenta.Vehiculo.Marca);
				CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_MODELO_VEHICULO + Inttostr(i+1),iif( Trim(Vehiculo[i].Cuenta.Vehiculo.Modelo) = '', '', Trim(strLeft(Vehiculo[i].Cuenta.Vehiculo.Modelo, CANT_CARACTERES_MODELO))+'.'));
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_ANIO_VEHICULO + Inttostr(i+1),Vehiculo[i].Cuenta.Vehiculo.Anio);
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TAG_VEHICULO + Inttostr(i+1),SerialNumberToEtiqueta(inttostr(Vehiculo[i].Cuenta.ContactSerialNumber)));
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText, TAG_CON_FECHA_ALTA + Inttostr(i+1) , FormatDateTime('dd/mm/yy hh:nn', Vehiculo[i].Cuenta.FechaAltaTag));
            end
            else begin
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText, TAG_CON_ACCION_VEHICULO + Inttostr(i+1), '');
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText, TAG_CON_ACCION_FECHA + Inttostr(i+1), '');
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_PATENTE_VEHICULO+Inttostr(i+1), '');
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TIPO_VEHICULO+Inttostr(i+1), '');
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_CATE_VEHICULO+Inttostr(i+1), '');
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_MARCA_VEHICULO+Inttostr(i+1),'');
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_MODELO_VEHICULO+Inttostr(i+1),'');
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_ANIO_VEHICULO+Inttostr(i+1),'');
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TAG_VEHICULO+Inttostr(i+1),'');
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText, TAG_CON_FECHA_ALTA + Inttostr(i+1), '');
            end;
        end;

        //CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_EMISION_ELEC,iif(EnviaDocumentacionxMail,MSG_SI,MSG_NO));

        CampoRich.RichText := PonerDescripcionMensajes(CampoRich.RichText, Vehiculo, NumeroConvenio);

        RBInterface.Caption:='Convenio Personer�a Natural';

        if Imprimir(tthConvenio ,CantCopias) then begin
            if Length(Vehiculo)>TAG_CON_CANTIDAD_MAX_VEHICULOS_HOJA then begin
                for i:=TAG_CON_CANTIDAD_MAX_VEHICULOS_HOJA to Length(Vehiculo)-1 do begin
                    SetLength(aux_Vehiculo,Length(aux_Vehiculo)+1);
					aux_Vehiculo[i-TAG_CON_CANTIDAD_MAX_VEHICULOS_HOJA]:=vehiculo[i];
                end;

                result := ImprimirAnexoVehiculos(PERSONERIA_FISICA,NumeroConvenio,aux_Vehiculo,MotivoCancelacion,CantCopias);
            end else begin
                result:=True;
                MotivoCancelacion:=OK;
            end;
        end else begin
            result:=False;
            MotivoCancelacion:=CANCELADO_USR;
        end;
    except
    	on e: Exception do begin
			MsgBoxErr('Error al imprimir Convenio Persona Natural', e.Message, 'Error de Impresi�n', MB_ICONERROR);
	        result:=False;
        end;
    end;
end;
*)

{******************************** Function Header ******************************
Function Name: TFormImprimirConvenio.ImprimirAnexoVehiculos
Author       : dcalini
Date Created : 26/07/2004
Description  : Imprimir anexo de vehiculos.
Parameters   : Personeria:String; NumeroConvenio:String; Vehiculo: array of TCuentaVehiculo; var MotivoCancelacion:TMotivoCancelacion; CantCopias:Integer; TipoHoja : TTipoHoja=False
Return Value : Boolean

Revision: 1
    Author: pdominguez
    Date: 18/03/2009
    Description: SS 733
        - Se Cambi� la forma del par�metro Veh�culo para pasarlo por referencia.
        - Se adapt� para la impresi�n al Reporte ppReporteConvenioCompleto.
*******************************************************************************}
function TFormImprimirConvenio.ImprimirAnexoVehiculos(
  Personeria:String;
  NumeroConvenio:String;
  TipoImpresion: TTipoImpresion;
  var Vehiculo: array of TCuentaABM;
  var MotivoCancelacion:TMotivoCancelacion;
  CantCopias:Integer): Boolean;
var
    PathArchivo:String;
    i: Integer;
    aux_Vehiculo, aux_Vehiculo_Restantes: array of TCuentaABM;
begin
    FormularioPreImpreso := True;

    if not ObtenerParametroGeneral(DMConnections.BaseCAC,iif(Personeria=PERSONERIA_JURIDICA, PLANTILLA_ANEXO_VEHICULOS_FISICA, PLANTILLA_ANEXO_VEHICULOS_NATURAL),PathArchivo)
    then begin
        MotivoCancelacion := PeaTypes.ERROR;
        Result := False;
    end else begin
        // Revision 2: Recorro los vehiculos restantes y los voy enviando a
        // impresi�n.
        SetLength(aux_Vehiculo,0);
        for i := 0 to Length(Vehiculo)-1 do begin
            SetLength(aux_Vehiculo,Length(aux_Vehiculo)+1);
    		aux_Vehiculo[i]:=vehiculo[i];
        end;

        while (Length(aux_Vehiculo) > 0) do begin
            ImprimirAnexoVehiculosGeneral(Personeria, NumeroConvenio, TipoImpresion, aux_Vehiculo, MotivoCancelacion, CantCopias, PathArchivo);

            // Quito los veh�culos que ya se imprimieron en la p�gina anterior.
            SetLength(aux_Vehiculo_Restantes,0);
            for i:=iif(Personeria=PERSONERIA_FISICA,TAG_JUR_CANTIDAD_MAX_VEHICULOS_ANEXO,TAG_CON_CANTIDAD_MAX_VEHICULOS_ANEXO) to Length(aux_Vehiculo)-1 do begin
                SetLength(aux_Vehiculo_Restantes,Length(aux_Vehiculo_Restantes)+1);
                aux_Vehiculo_Restantes[i-iif(Personeria=PERSONERIA_FISICA,TAG_JUR_CANTIDAD_MAX_VEHICULOS_ANEXO,TAG_CON_CANTIDAD_MAX_VEHICULOS_ANEXO)]:=aux_Vehiculo[i];
            end;

            SetLength(aux_Vehiculo,0);
            for i := 0 to Length(aux_Vehiculo_Restantes)-1 do begin
                SetLength(aux_Vehiculo,Length(aux_Vehiculo)+1);
                aux_Vehiculo[i]:=aux_Vehiculo_Restantes[i];
            end;
        end;

        // SS 733
        RBInterface.Report.PrinterSetup.Copies := CantCopias;

        if Imprimir(tthConvenio, CantCopias, (TipoImpresion <> tiEstadoActual)) then begin
            result:=True;
            MotivoCancelacion:=OK;
        end else begin
            Result:=False;
            MotivoCancelacion:=CANCELADO_USR;
        end;
        // Fin SS 733

    end;
end;


 {
 ******************************* Cabecera Funci�n *******************************
  Nombre Funci�n:
  Autor         :
  Fecha Creaci�n:
  Descripci�n   :
 ---------------------------------- Par�metros ----------------------------------
  Ninguno.
 ------------------------------- Valor de Retorno -------------------------------

 ---------------------------------- Comentarios ---------------------------------
  Ninguno.
 ---------------------------- Historial de revisiones ---------------------------
Revision: 2
    Author: pdominguez
    Date: 25/03/2009
    Description: SS 733
        - Se adapt� para la impresi�n al Reporte ppReporteConvenioCompleto.

Revision: 3
    Author : pdominguez
    Date   : 21/08/2009
    Description : SS 733
        - Se Cambi� la forma del par�metro Veh�culo para pasarlo por referencia.
 ********************************************************************************
 }
function TFormImprimirConvenio.ImprimirAnexoMotos(
  Personeria:String;
  NumeroConvenio:String;
  //TipoImpresion: TTipoImpresion;
  var Vehiculo: array of TCuentaABM;
  var MotivoCancelacion:TMotivoCancelacion;
  CantCopias:Integer): Boolean;
var
    PathArchivo:String;
    i, CantidadFilasAnexo: Integer;
    aux_Vehiculo, aux_Vehiculo_Restantes: array of TCuentaABM;
begin
    FormularioPreImpreso := True;

    if not ObtenerParametroGeneral(DMConnections.BaseCAC,PLANTILLA_ANEXO_MOTOS,PathArchivo)
    then begin
        MotivoCancelacion := PeaTypes.ERROR;
        Result := False;
    end else begin
        SetLength(aux_Vehiculo,0);
        for i := 0 to Length(Vehiculo)-1 do begin
            SetLength(aux_Vehiculo,Length(aux_Vehiculo)+1);
    		aux_Vehiculo[i]:=vehiculo[i];
        end;

        if not ObtenerParametroGeneral(DMConnections.BaseCAC,PLANTILLA_ANEXO_MOTOS, CantidadFilasAnexo) then
            CantidadFilasAnexo := TAG_CON_CANTIDAD_MAX_MOTOS_ANEXO;

        while (Length(aux_Vehiculo) > 0) do begin
            //Por ahora lo dejo as�, el d�a de ma�ana habr� que ver si hay que agregar
            //otro distinto para la columna de soporte de telev-ia.
            //Le pongo un par�metro con la cantidad de filas por hoja de anexo.
            ImprimirAnexoMotosGeneral(NumeroConvenio, CantidadFilasAnexo, aux_Vehiculo, MotivoCancelacion, CantCopias, PathArchivo);

            // Quito los veh�culos que ya se imprimieron en la p�gina anterior.
            SetLength(aux_Vehiculo_Restantes,0);
            //for i:= TAG_CON_CANTIDAD_MAX_MOTOS_ANEXO to Length(aux_Vehiculo)-1 do begin
            for i:= CantidadFilasAnexo to Length(aux_Vehiculo)-1 do begin
                SetLength(aux_Vehiculo_Restantes,Length(aux_Vehiculo_Restantes)+1);
                aux_Vehiculo_Restantes[i-CantidadFilasAnexo]:=aux_Vehiculo[i];
            end;

            SetLength(aux_Vehiculo,0);
            for i := 0 to Length(aux_Vehiculo_Restantes)-1 do begin
                SetLength(aux_Vehiculo,Length(aux_Vehiculo)+1);
                aux_Vehiculo[i]:=aux_Vehiculo_Restantes[i];
            end;
        end;

        // SS 733
        RBInterface.Report.PrinterSetup.Copies:=CantCopias;

        if Imprimir(tthConvenio, CantCopias) then begin
            result:=True;
            MotivoCancelacion:=OK;
        end else begin
            Result:=False;
            MotivoCancelacion:=CANCELADO_USR;
        end;
        // Fin SS 733
    end;
end;

{******************************** Function Header ******************************
Revision: 1
    Author: nefernandez
    Date: 08/11/2007
    Description: SS 620: Se imprime la FechaAltaCuenta en lugar de la FechaAltaTag

Revision: 2
    Author: pdominguez
    Date: 25/03/2009
    Description: SS 733
        - Se adapt� para la impresi�n al Reporte ppReporteConvenioCompleto.

Revision: 3
    Author : pdominguez
    Date   : 07/08/2009
    Description : SS 733
        - Se a�ade a la impresi�n de la patente el D�gito Verificador.

Revision: 12
    Author : pdominguez
    Date   : 07/06/2010
    Description : SS 895 - Error Impresi�n Convenio CAC
        - Se cambia el LoadFromFile del objeto CampoRich, por la carga a trav�s
        de la funci�n CargaPlantillaRTF.
********************************************************************************}
function TFormImprimirConvenio.ImprimirAnexoVehiculosGeneral(Personeria,
  NumeroConvenio: String; TipoImpresion: TTipoImpresion;
  var Vehiculo: array of TCuentaABM;
  var MotivoCancelacion: TMotivoCancelacion; CantCopias: Integer;
  PathArchivo:String): Boolean;
resourcestring
	CONST_ACCION_Agrega = 'Agrega';
	CONST_ACCION_Elimina = 'Elimina';
	CONST_ACCION_Modifica = 'Modifica';
	CONST_ACCION_Robado = 'Robado';
	CONST_ACCION_BajaST = 'Baja S/T';
	CONST_ACCION_Vigente = 'Vigente';
	CONST_ACCION_Suspende = 'Suspende';
	CONST_ACCION_Activa = 'Activa';
	CONST_ACCION_Perdida = 'P�rdida';
	CONST_ACCION_ERROR  = 'ERROR';
	CONST_ANEXO_VEHICULOS = 'Anexo de Veh�culos';
	CONST_MGS_ERROR_IMPRESION_ANEXO = 'Error al imprimir Anexo Veh�culos';
	CONST_MGS_ERROR_IMPRESION = 'Error de Impresi�n';
var
	i, NumeroAnexo:Integer;
	Accion, auxDescripTipo,auxDescripCategoria,auxCategoria:String;
begin
	try
        MotivoCancelacion:=PeaTypes.ERROR;

        if not(FileExists(PathArchivo)) then begin
            MsgBox(format(MSG_ERROR_ARCHIVO_ABRIR,[PathArchivo]),MSG_CAPTION_IMPRESION_CONVENIOS,MB_ICONSTOP);
			result:=False;
            Exit;
		end;

        // Rev. 12 (SS 895)
        CampoRich.RichText := CargaPlantillaRTF(PathArchivo);
        //CampoRich.LoadFromFile(PathArchivo);
        // Fin Rev. 12 (SS 895)

        //convenio
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_CONVENIO,NumeroConvenio);

        //anexo
        NumeroAnexo := -1;
		if (TipoImpresion <> tiEstadoActual) then begin
            with spObtenerNumeroAnexo do begin
				Parameters.Refresh;
				Parameters.ParamByName('@NumeroConvenio').Value := NumeroConvenio;
                ExecProc;
                NumeroAnexo := Parameters.ParamByName('@RETURN_VALUE').Value;
			end;
        end;
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_NUMERO_ANEXO, iif(NumeroAnexo > -1, ANEXO_NRO + IntToStr(NumeroAnexo), ''));
        //Asgino la prop.
        NroAnexoEmitido := NumeroAnexo;

        //vehiculos
		for i:=0 to iif(Personeria=PERSONERIA_FISICA,TAG_JUR_CANTIDAD_MAX_VEHICULOS_ANEXO,TAG_CON_CANTIDAD_MAX_VEHICULOS_ANEXO)-1 do begin
            if i<Length(Vehiculo) then begin

				case Vehiculo[i].TipoMovimiento of
					ttmAlta       : Accion := CONST_ACCION_Agrega;
					ttmBaja       : Accion := CONST_ACCION_Elimina;
					ttmModif      : Accion := CONST_ACCION_Modifica;
					ttmModifTag   : Accion := CONST_ACCION_Modifica;
					ttmRobado     : Accion := CONST_ACCION_Robado;
					ttmBajaSinTag : Accion := CONST_ACCION_BajaST;
					ttmVigente    : Accion := CONST_ACCION_Vigente;
					ttmSuspendido : Accion := CONST_ACCION_Suspende;
					ttmActivado   : Accion := CONST_ACCION_Activa;
					ttmTagPerdido : Accion := CONST_ACCION_Perdida;
				else
					Accion := CONST_ACCION_ERROR;
				end;

                ObtenerDescripTipoVehiculo(DMConnections.BaseCAC,Vehiculo[i].Cuenta.Vehiculo.CodigoTipo,auxDescripTipo,auxDescripCategoria,auxCategoria);
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText, TAG_CON_ACCION_VEHICULO + Inttostr(i+1), Accion);
				//CampoRich.RichText:=ReplaceTag(CampoRich.RichText, TAG_CON_ACCION_FECHA + Inttostr(i+1), FormatDateTime('dd/mm/yy hh:nn', Vehiculo[i].Cuenta.FechaModifCuenta));
				//CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_PATENTE_VEHICULO + Inttostr(i+1),Vehiculo[i].Cuenta.Vehiculo.Patente);
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_PATENTE_VEHICULO + Inttostr(i+1),Trim(Vehiculo[i].Cuenta.Vehiculo.Patente) + ' - ' + Vehiculo[i].Cuenta.Vehiculo.DigitoPatente); // SS 733
				CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TIPO_VEHICULO + Inttostr(i+1),FormatearTexto(auxDescripTipo, CANTIDAD_CARACTERES_TIPO));
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_CATE_VEHICULO + Inttostr(i+1),auxCategoria);
				CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_MARCA_VEHICULO + Inttostr(i+1),Vehiculo[i].Cuenta.Vehiculo.Marca);
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_MODELO_VEHICULO + Inttostr(i+1),iif(Trim(Vehiculo[i].Cuenta.Vehiculo.Modelo) = '', '', trim(strLeft(Vehiculo[i].Cuenta.Vehiculo.Modelo, CANT_CARACTERES_MODELO))+'.'));
				CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_ANIO_VEHICULO + Inttostr(i+1),Vehiculo[i].Cuenta.Vehiculo.Anio);
				CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TAG_VEHICULO + Inttostr(i+1),SerialNumberToEtiqueta(inttostr(Vehiculo[i].Cuenta.ContactSerialNumber)));
				//Revision 1 - CampoRich.RichText:=ReplaceTag(CampoRich.RichText, TAG_CON_FECHA_ALTA + Inttostr(i+1) , FormatDateTime('dd/mm/yy hh:nn', Vehiculo[i].Cuenta.FechaAltaTag));
				CampoRich.RichText:=ReplaceTag(CampoRich.RichText, TAG_CON_FECHA_ALTA + Inttostr(i+1) , FormatDateTime('dd/mm/yy hh:nn', Vehiculo[i].Cuenta.FechaCreacion));
				CampoRich.RichText:=ReplaceTag(CampoRich.RichText, TAG_CON_FECHA_VTO_GTIA + Inttostr(i+1) , FormatDateTime('dd/mm/yy hh:nn', Vehiculo[i].Cuenta.FechaVencimientoTag));
			end else begin
				CampoRich.RichText:=ReplaceTag(CampoRich.RichText, TAG_CON_ACCION_VEHICULO + Inttostr(i+1), '');
				//CampoRich.RichText:=ReplaceTag(CampoRich.RichText, TAG_CON_ACCION_FECHA + Inttostr(i+1), '');
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_PATENTE_VEHICULO + Inttostr(i+1),'');
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TIPO_VEHICULO + Inttostr(i+1),'');
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_CATE_VEHICULO + Inttostr(i+1),'');
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_MARCA_VEHICULO + Inttostr(i+1),'');
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_MODELO_VEHICULO + Inttostr(i+1),'');
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_ANIO_VEHICULO + Inttostr(i+1),'');
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TAG_VEHICULO + Inttostr(i+1),'');
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText, TAG_CON_FECHA_ALTA + Inttostr(i+1) , '');
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText, TAG_CON_FECHA_VTO_GTIA + Inttostr(i+1) , '');
            end;
        end;

		CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_FIRMA_CONCESIONARA, iif(TipoImpresion <> tiEstadoActual, STR_FIRMA_CONCESIONARIA, ''));
		CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_FIRMA_CLIENTE, iif(TipoImpresion <> tiEstadoActual, STR_FIRMA_CLIENTE, ''));

        // SS 733
        cdsDatosConvenioCompleto.Append;
        cdsDatosConvenioCompletoMiRichEdit.AsString := CampoRich.RichText;
        cdsDatosConvenioCompleto.Post;


{		RBInterface.Caption:=CONST_ANEXO_VEHICULOS;

		ppReporteImpresionConvenio.PrinterSetup.Copies:=CantCopias;
        if Imprimir(tthConvenio, CantCopias, (TipoImpresion <> tiEstadoActual)) then begin}
            {*
            Revision 2: Se quita esto para evitar la recursividad. La lista de
            los siguientes vehiculos a imprimir se genera en la misma funci�n
            ImprimirAnexoVehiculos.
            if Length(Vehiculo)>iif(Personeria=PERSONERIA_FISICA,TAG_JUR_CANTIDAD_MAX_VEHICULOS_ANEXO,TAG_CON_CANTIDAD_MAX_VEHICULOS_ANEXO) then begin
				for i:=iif(Personeria=PERSONERIA_FISICA,TAG_JUR_CANTIDAD_MAX_VEHICULOS_ANEXO,TAG_CON_CANTIDAD_MAX_VEHICULOS_ANEXO) to Length(Vehiculo)-1 do begin
					SetLength(aux_Vehiculo,Length(aux_Vehiculo)+1);
					aux_Vehiculo[i-iif(Personeria=PERSONERIA_FISICA,TAG_JUR_CANTIDAD_MAX_VEHICULOS_ANEXO,TAG_CON_CANTIDAD_MAX_VEHICULOS_ANEXO)]:=vehiculo[i];
                end;
                Result := ImprimirAnexoVehiculos(Personeria,NumeroConvenio,TipoImpresion,aux_Vehiculo,MotivoCancelacion,CantCopias);
            end else begin
                result:=True;
                MotivoCancelacion:=OK;
            end;
            *}
{            result:=True;
            MotivoCancelacion:=OK;
        end else begin
            Result:=False;
            MotivoCancelacion:=CANCELADO_USR;
        end;}

        // Fin SS 733

    except
		on e: Exception do begin
			MsgBoxErr(CONST_MGS_ERROR_IMPRESION_ANEXO, e.Message, CONST_MGS_ERROR_IMPRESION, MB_ICONERROR);
			result:=False;
		end;

	end;

end;

{******************************** Function Header ******************************
Function Name: ImprimirAnexoMotosGeneral
Author : vpaszkowicz
Date Created : 18/05/2007
Description : Agrego lo mismo que en veh�culos pero para las motos
Parameters : Personeria, NumeroConvenio: String; TipoImpresion: TTipoImpresion; Vehiculo: array of TCuentaABM; var MotivoCancelacion: TMotivoCancelacion; CantCopias: Integer; PathArchivo:String
Return Value : Boolean

Revision: 1
    Author: pdominguez
    Date: 25/03/2009
    Description: SS 733
        - Se adapt� para la impresi�n al Reporte ppReporteConvenioCompleto.

Revision: 2
    Author : pdominguez
    Date   : 21/08/2009
    Description : SS 733
        - Se Cambi� la forma del par�metro Veh�culo para pasarlo por referencia.

Revision: 12
    Author : pdominguez
    Date   : 07/06/2010
    Description : SS 895 - Error Impresi�n Convenio CAC
        - Se cambia el LoadFromFile del objeto CampoRich, por la carga a trav�s
        de la funci�n CargaPlantillaRTF.
*******************************************************************************}
function TFormImprimirConvenio.ImprimirAnexoMotosGeneral(NumeroConvenio: String;
  CantidadFilasAnexo: integer;
  var Vehiculo: array of TCuentaABM;
  var MotivoCancelacion: TMotivoCancelacion; CantCopias: Integer;
  PathArchivo:String): Boolean;
resourcestring
	CONST_ANEXO_VEHICULOS = 'Anexo de Motos';
	CONST_MGS_ERROR_IMPRESION_ANEXO = 'Error al imprimir Anexo Motos';
	CONST_MGS_ERROR_IMPRESION = 'Error de Impresi�n';
var
	i, NumeroAnexo:Integer;
begin
	try
        MotivoCancelacion:=PeaTypes.ERROR;

        if not(FileExists(PathArchivo)) then begin
            MsgBox(format(MSG_ERROR_ARCHIVO_ABRIR,[PathArchivo]),MSG_CAPTION_IMPRESION_CONVENIOS,MB_ICONSTOP);
			result:=False;
            Exit;
		end;

        // Rev. 12 (SS 895)
        CampoRich.RichText := CargaPlantillaRTF(PathArchivo);
        //CampoRich.LoadFromFile(PathArchivo);
        // Fin Rev. 12 (SS 895)

        //convenio
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_CONVENIO,NumeroConvenio);

        //anexo
        NumeroAnexo := -1;
	    CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_NUMERO_ANEXO, iif(NumeroAnexo > -1, ANEXO_NRO + IntToStr(NumeroAnexo), ''));
        //Asgino la prop.
        NroAnexoEmitido := NumeroAnexo;

        //vehiculos
		for i:=0 to CantidadFilasAnexo do begin
            if i<Length(Vehiculo) then begin
				CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_PATENTE_VEHICULO + Inttostr(i+1),Vehiculo[i].Cuenta.Vehiculo.Patente);
				CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_MARCA_VEHICULO + Inttostr(i+1),Vehiculo[i].Cuenta.Vehiculo.Marca);
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_MODELO_VEHICULO + Inttostr(i+1),iif(Trim(Vehiculo[i].Cuenta.Vehiculo.Modelo) = '', '', trim(strLeft(Vehiculo[i].Cuenta.Vehiculo.Modelo, CANT_CARACTERES_MODELO))+'.'));
				CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TAG_VEHICULO + Inttostr(i+1),SerialNumberToEtiqueta(inttostr(Vehiculo[i].Cuenta.ContactSerialNumber)));
			end else begin
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_PATENTE_VEHICULO + Inttostr(i+1),'');
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_MARCA_VEHICULO + Inttostr(i+1),'');
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_MODELO_VEHICULO + Inttostr(i+1),'');
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TAG_VEHICULO + Inttostr(i+1),'');
            end;
        end;

		//CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_FIRMA_CONCESIONARA, iif(TipoImpresion <> tiEstadoActual, STR_FIRMA_CONCESIONARIA, ''));
		//CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_FIRMA_CLIENTE, iif(TipoImpresion <> tiEstadoActual, STR_FIRMA_CLIENTE, ''));
        CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_FIRMA_CONCESIONARA, '');
		CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_FIRMA_CLIENTE, '');

        // SS 733
        cdsDatosConvenioCompleto.Append;
        cdsDatosConvenioCompletoMiRichEdit.AsString := CampoRich.RichText;
        cdsDatosConvenioCompleto.Post;

{		RBInterface.Caption:=CONST_ANEXO_VEHICULOS;

		ppReporteImpresionConvenio.PrinterSetup.Copies:=CantCopias;
        if Imprimir(tthConvenio, CantCopias, True) then begin//(TipoImpresion <> tiEstadoActual)) then begin
            result:=True;
            MotivoCancelacion:=OK;
        end else begin
            Result:=False;
            MotivoCancelacion:=CANCELADO_USR;
        end;}
        // Fin SS 733

    except
		on e: Exception do begin
			MsgBoxErr(CONST_MGS_ERROR_IMPRESION_ANEXO, e.Message, CONST_MGS_ERROR_IMPRESION, MB_ICONERROR);
			result:=False;
		end;

	end;

end;

{

******************************** Function Header ******************************
Function Name: TFormImprimirConvenio.ImprimirConvenioPersonaJuridica
Author       : dcalani
Date Created : 26/06/2004
Description  : Imprime convenio para persona juridica
Parameters   : NumeroConvenio:String; DatoTitular: TDatosPersonales; PrimerMedioComunicacion:TFrameTelefono; SegundoMedioComunicacion:TFrameTelefono; EMail:String; DomicilioTitular:TDatosDomicilio; DomicilioFacturacion:TDatosDomicilio; DatosMedioPago:TDatosMediosPago; DatosRepLegal1:TDatosPersonales; DatosRepLegal2:TDatosPersonales; DatosRepLegal3:TDatosPersonales; EnviaDocumentacionxMail:Boolean; var MotivoCancelacion:TMotivoCancelacion; CantCopias:Integer; Vehiculo:Array of TCuentaVehiculo; TipoHoja : TTipoHoja=True
Return Value : Boolean
*******************************************************************************}
(*
function TFormImprimirConvenio.ImprimirConvenioPersonaJuridica(
              NumeroConvenio:String;
              DatoTitular: TDatosPersonales;
              PrimerMedioComunicacion:TFrameTelefono;
              SegundoMedioComunicacion:TFrameTelefono;
              EMail:String;
              DomicilioTitular:TDatosDomicilio;
              DomicilioFacturacion:TDatosDomicilio;
              DatosMedioPago:TDatosMediosPago;
              DatosRepLegal1:TDatosPersonales;
              DatosRepLegal2:TDatosPersonales;
              DatosRepLegal3:TDatosPersonales;
              EnviaDocumentacionxMail:Boolean;
              FechaAlta : TDateTime;
			  var MotivoCancelacion:TMotivoCancelacion;
              CantCopias:Integer;
              Vehiculo: TRegCuentaABM): Boolean;
var
    Aux_MeidoPago,PathArchivo:String;
    Accion, auxDescripTipo,auxDescripCategoria,auxCategoria:String;
    i:Integer;
    aux_Vehiculo:array of TCuentaABM;
	buff: String;
begin
    if not EstaFuenteMarcaInstalada then begin
        Result := False;
        MotivoCancelacion := PeaTypes.ERROR;
        exit;
    end;


	try
        //ALTA
        MotivoCancelacion:=PeaTypes.ERROR;
        ObtenerParametroGeneral(DMConnections.BaseCAC,PLANTILLA_CONVENIO_PERSONA_JURIDICA,PathArchivo);

		if not(FileExists(PathArchivo)) then begin
            MsgBox(format(MSG_ERROR_ARCHIVO_ABRIR,[PathArchivo]),MSG_CAPTION_IMPRESION_CONVENIOS,MB_ICONSTOP);
            result:=False;
            Exit;
        end;

        //CampoRich.LoadFromFile(PathArchivo);
        CampoRich.Clear;
        Buff := FileToString(PathArchivo);
        CampoRich.RichText := copy(buff,1,length(buff));


        //convenio
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_CONVENIO,NumeroConvenio);
        ReplaceCodeBar(TAG_CODIGO_BARRA, NumeroConvenio, CampoRich);
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_DIA,FormatDateTime('dd',FechaAlta));
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_MES,FormatDateTime('mmmm',FechaAlta));
		CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_ANIO,FormatDateTime('yyyy',FechaAlta));

        //EMPRESA
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_RAZON_SOCIAL,DatoTitular.RazonSocial);
        DatoTitular.NumeroDocumento:=trim(DatoTitular.NumeroDocumento);
		CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_RUT,DatoTitular.NumeroDocumento);

        //medios de comunicacion
		CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TELEFONO_ETIQUETA1,PrimerMedioComunicacion.DescriTipoTelefono);
		CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TELEFONO_AREA1,StrRight('0'+trim(inttostr(PrimerMedioComunicacion.CodigoArea)),2));
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TELEFONO_NUMERO1,PrimerMedioComunicacion.NumeroTelefono);

        if trim(SegundoMedioComunicacion.NumeroTelefono)<>'' then begin
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TELEFONO_ETIQUETA2,SegundoMedioComunicacion.DescriTipoTelefono);
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TELEFONO_AREA2,StrRight('0'+trim(inttostr(SegundoMedioComunicacion.CodigoArea)),2));
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TELEFONO_NUMERO2,SegundoMedioComunicacion.NumeroTelefono);
        end else begin
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TELEFONO_ETIQUETA2,'');
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TELEFONO_AREA2,'');
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TELEFONO_NUMERO2,'');
        end;

        if trim(EMail)<>'' then begin
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_EMAIL,EMail);
        end else begin
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_EMAIL,'');
        end;

        if DomicilioFacturacion.NumeroCalle > 0 then begin
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_CALLE_PAR,Trim(uppercase(DomicilioFacturacion.Descripcion)));
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_NUMERO_PAR,uppercase(DomicilioFacturacion.NumeroCalleSTR));
			CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_DETALLE_PAR,uppercase(DomicilioFacturacion.Detalle));
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_COMUNA_PAR,upperCase(BuscarDescripcionComuna(DMConnections.BaseCAC,DomicilioFacturacion.CodigoPais,DomicilioFacturacion.CodigoRegion,DomicilioFacturacion.CodigoComuna)));
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_REGION_PAR,upperCase(BuscarDescripcionRegion(DMConnections.BaseCAC,DomicilioFacturacion.CodigoPais,DomicilioFacturacion.CodigoRegion)));
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_CP_PAR,uppercase(DomicilioFacturacion.CodigoPostal));
        end else begin
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_CALLE_PAR,Trim(uppercase(DomicilioTitular.Descripcion)));
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_NUMERO_PAR,uppercase(DomicilioTitular.NumeroCalleSTR));
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_DETALLE_PAR,uppercase(DomicilioTitular.Detalle));
			CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_COMUNA_PAR,upperCase(BuscarDescripcionComuna(DMConnections.BaseCAC,DomicilioTitular.CodigoPais,DomicilioTitular.CodigoRegion,DomicilioTitular.CodigoComuna)));
			CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_REGION_PAR,uppercase(BuscarDescripcionRegion(DMConnections.BaseCAC,DomicilioTitular.CodigoPais,DomicilioTitular.CodigoRegion)));
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_CP_PAR,uppercase(DomicilioTitular.CodigoPostal));
        end;

        //REP LEGALES
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_NOMBRE1,DatosRepLegal1.Nombre);
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_APE_PAT1,DatosRepLegal1.Apellido);
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_APE_MAT1,DatosRepLegal1.ApellidoMaterno);
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_RUT1,DatosRepLegal1.NumeroDocumento);

        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_NOMBRE2,DatosRepLegal2.Nombre);
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_APE_PAT2,DatosRepLegal2.Apellido);
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_APE_MAT2,DatosRepLegal2.ApellidoMaterno);
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_RUT2,DatosRepLegal2.NumeroDocumento);

        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_NOMBRE3,DatosRepLegal3.Nombre);
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_APE_PAT3,DatosRepLegal3.Apellido);
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_APE_MAT3,DatosRepLegal3.ApellidoMaterno);
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_RUT3,DatosRepLegal3.NumeroDocumento);

        //CampoRich.RichText:=ReplaceTag(CampoRich.RichText, TAG_EMISION_ELEC, iif(EnviaDocumentacionxMail,MSG_SI,MSG_NO));

		//vehiculos
        for i:=0 to TAG_JUR_CANTIDAD_MAX_VEHICULOS_HOJA -1 do begin
            if i<Length(Vehiculo) then begin
                ObtenerDescripTipoVehiculo(DMConnections.BaseCAC,Vehiculo[i].Cuenta.Vehiculo.CodigoTipo,auxDescripTipo,auxDescripCategoria,auxCategoria);

                case Vehiculo[i].TipoMovimiento of
                    ttmAlta : begin Accion := 'Agregado'; end;
                    ttmBaja : Accion   := 'Eliminado';
					ttmModif :  Accion := 'Modificado';
					ttmModifTag: begin  Accion := 'Modificado'; end;
                    ttmRobado :  Accion := 'Robado';
                    ttmBajaSinTag  :  Accion := 'Baja S/T';
                else
                    Accion := 'ERROR';
                end;

                CampoRich.RichText:=ReplaceTag(CampoRich.RichText, TAG_CON_ACCION_VEHICULO + Inttostr(i+1), Accion);
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText, TAG_CON_ACCION_FECHA + Inttostr(i+1), FormatDateTime('dd/mm/yy hh:nn', Vehiculo[i].Cuenta.FechaModifCuenta));
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_PATENTE_VEHICULO+Inttostr(i+1),Vehiculo[i].Cuenta.Vehiculo.Patente);
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TIPO_VEHICULO+Inttostr(i+1),FormatearTexto(auxDescripTipo,CANTIDAD_CARACTERES_TIPO));
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_CATE_VEHICULO+Inttostr(i+1),auxCategoria);
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_MARCA_VEHICULO+Inttostr(i+1),Vehiculo[i].Cuenta.Vehiculo.Marca);
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_MODELO_VEHICULO+Inttostr(i+1),iif( Trim(Vehiculo[i].Cuenta.Vehiculo.Modelo) = '', '', Trim(strLeft(Vehiculo[i].Cuenta.Vehiculo.Modelo, CANT_CARACTERES_MODELO))+'.'));
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_ANIO_VEHICULO+Inttostr(i+1),Vehiculo[i].Cuenta.Vehiculo.Anio);
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TAG_VEHICULO+Inttostr(i+1),SerialNumberToEtiqueta(inttostr(Vehiculo[i].Cuenta.ContactSerialNumber)));
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText, TAG_CON_FECHA_ALTA + Inttostr(i+1) , FormatDateTime('dd/mm/yy hh:nn', Vehiculo[i].Cuenta.FechaAltaTag));
            end
            else begin
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText, TAG_CON_ACCION_VEHICULO + Inttostr(i+1), '');
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText, TAG_CON_ACCION_FECHA + Inttostr(i+1), '');
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_PATENTE_VEHICULO+Inttostr(i+1),'');
				CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TIPO_VEHICULO+Inttostr(i+1),'');
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_CATE_VEHICULO+Inttostr(i+1),'');
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_MARCA_VEHICULO+Inttostr(i+1),'');
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_MODELO_VEHICULO+Inttostr(i+1),'');
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_ANIO_VEHICULO+Inttostr(i+1),'');
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TAG_VEHICULO+Inttostr(i+1),'');
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText, TAG_CON_FECHA_ALTA + Inttostr(i+1) , '');

			end;
		end;

        CampoRich.RichText := PonerDescripcionMensajes(CampoRich.RichText, Vehiculo, NumeroConvenio);

        RBInterface.Caption:='Convenio Personer�a Juridica';

        if Imprimir(tthConvenio, CantCopias) then begin

            if Length(Vehiculo)>TAG_JUR_CANTIDAD_MAX_VEHICULOS_HOJA then begin
                for i:=TAG_JUR_CANTIDAD_MAX_VEHICULOS_HOJA to Length(Vehiculo)-1 do begin
                    SetLength(aux_Vehiculo,Length(aux_Vehiculo)+1);
                    aux_Vehiculo[i-TAG_JUR_CANTIDAD_MAX_VEHICULOS_HOJA]:=vehiculo[i];
                end;

                result:=ImprimirAnexoVehiculos(PERSONERIA_JURIDICA, NumeroConvenio, aux_Vehiculo, MotivoCancelacion, CantCopias);
            end else begin
                result:=True;
                MotivoCancelacion:=OK;
        end;
		end else begin
            result:=False;
            MotivoCancelacion:=CANCELADO_USR;
		end;

    except
    	on e: Exception do begin
        	MsgBoxErr('Error al imprimir Convenio Persona Jur�dica', e.Message, 'Error de Impresi�n', MB_ICONERROR);
	        result:=False;
        end;

	end;

end;
*)

{******************************** Function Header ******************************
Function Name: TFormImprimirConvenio.ImprimirMandatoPAT
Author       : dcalani
Date Created : 26/06/2004
Description  : Imprimir
Parameters   : NumeroConvenio:String; DatosMedioPago: TDatosMediosPago; RegDatoMandante: TDatosPersonales; var MotivoCancelacion:TMotivoCancelacion; CantCopias:Integer; MostrarInterface: Boolean
Return Value : Boolean

Revision 1
Author       : ndonadio
Date Created : 01/08/2005
Description  : Imprimir Mandato CMR - FALABELLA

Revision 2
Author : pdominguez
Date   : 17/07/2009
Description : SS 733
    - Se adapt� para la impresi�n al Reporte ppReporteConvenioCompleto.

Revision 3
    Author : pdominguez
    Date   : 21/08/2009
    Description : SS 733
        - Se cambia la asignaci�n del reporte al objeto ppReporteConvenioCompleto2: TppReport.

Revision 4
    Author : pdominguez
    Date   : 31/08/2009
    Description : SS 733
        - Se reestructura el bucle de impresi�n para sacar en cada hoja el literal
        'Copia de ...'.

Revision: 12
    Author : pdominguez
    Date   : 07/06/2010
    Description : SS 895 - Error Impresi�n Convenio CAC
        - Se cambia el LoadFromFile del objeto CampoRich, por la carga a trav�s
        de la funci�n CargaPlantillaRTF.
*******************************************************************************}
function TFormImprimirConvenio.ImprimirMandatoPAT(
                                            NumeroConvenio:String;
                                            DatosMedioPago:TDatosMediosPago;
                                            RegDatoMandante,
                                            DatosTitular: TDatosPersonales;
                                            DomicilioTitular: TDatosDomicilio;
                                            EMailTitular: String;
                                            DatosCuentas: TRegCuentaABM;
                                            var MotivoCancelacion:TMotivoCancelacion;
                                            CantCopias:Integer): Boolean;
resourcestring
  CONST_MANDATO_PAT = 'Mandato PAT';
  CONST_MANDATO_CMR = 'Mandato CMR-Falabella';
  CONST_COPIA_CMR   = 'CMR - Falabella';
  CONST_ERROR_IMPRESION = 'Error de Impresi�n';
  CONST_ERROR_IMPRESION_MANDATO_PAT = 'Error al imprimir Mandato PAT';
  CONST_ERROR_IMPRESION_MANDATO_CMR = 'Error al imprimir Mandato CMR-Falabella';
var
	PathArchivo:String;
    Error_Impresion_Mandato: AnsiString;
    Dia, Mes, Anio: Word;
    i,j,
    CantidadCopias: integer;
begin
	try
		MotivoCancelacion:=PeaTypes.ERROR;

        if DatosMedioPago.PAT.TipoTarjeta <> TARJETA_CMR then begin
                // PAT
                Error_Impresion_Mandato := CONST_ERROR_IMPRESION_MANDATO_PAT;
                if not(ObtenerParametroGeneral(DMConnections.BaseCAC,'PLANTILLA_PAT_TARJETA'+trim(inttostr(DatosMedioPago.PAT.EmisorTarjetaCredito)),PathArchivo)) then
                    ObtenerParametroGeneral(DMConnections.BaseCAC,PLANTILLA_MANDATO_PAT,PathArchivo);

                if not(FileExists(PathArchivo)) then begin
                    MsgBox(format(MSG_ERROR_ARCHIVO_ABRIR,[PathArchivo]),MSG_CAPTION_IMPRESION_CONVENIOS,MB_ICONSTOP);
                    result:=False;
                    Exit;
                end;
                // Rev. 4 (SS 733)
                cdsDatosConvenioCompleto.EmptyDataSet;

                for CantidadCopias := 1 to CantCopias do begin

                    // Rev. 12 (SS 895)
                    CampoRich.RichText := CargaPlantillaRTF(PathArchivo);
                    //CampoRich.LoadFromFile(PathArchivo);
                    // Fin Rev. 12 (SS 895)
                    CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_CONVENIO, NumeroConvenio);

                    if RegDatoMandante.Personeria= PERSONERIA_JURIDICA then begin
                        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_PAGO_NOMBRE,RegDatoMandante.RazonSocial);
                        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_PAGO_NOMBRE_SOLO, RegDatoMandante.RazonSocial);
                        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_PAGO_APELLIDO_PATERNO, '');
                        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_PAGO_APELLIDO_MATERNO, '');
                    end else begin
                        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_PAGO_NOMBRE,ArmarNombrePersona(RegDatoMandante.Personeria,RegDatoMandante.Nombre,RegDatoMandante.Apellido,RegDatoMandante.ApellidoMaterno));
                        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_PAGO_NOMBRE_SOLO, trim(RegDatoMandante.Nombre));
                        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_PAGO_APELLIDO_PATERNO, trim(RegDatoMandante.Apellido));
                        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_PAGO_APELLIDO_MATERNO, trim(RegDatoMandante.ApellidoMaterno));
                    end;

                    CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_PAGO_RUT,RegDatoMandante.NumeroDocumento);
                    if trim(DatosMedioPago.Telefono)<>'' then
                        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_PAGO_TELEFONO, '(' + StrRight('0' + trim(inttostr(DatosMedioPago.CodigoArea)), 2) + ') ' + DatosMedioPago.Telefono)
                    else
                        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_PAGO_TELEFONO,'');

                    CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_PAGO_NOMBRE_TARJETA,ObtenerTiposTarjetasCreditos(DMConnections.BaseCAC,DatosMedioPago.PAT.TipoTarjeta));
                    CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_PAGO_NUMERO_TARJETA,DatosMedioPago.PAT.NroTarjeta);
                    CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_PAGO_DIA,FormatDateTime('dd',NowBase(DMConnections.BaseCAC)));
                    CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_PAGO_MES,FormatDateTime('mm',NowBase(DMConnections.BaseCAC)));
                    CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_PAGO_ANIO,FormatDateTime('yyyy',NowBase(DMConnections.BaseCAC)));
                    CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_PAGO_FECHA_VENC_MES,StrLeft(DatosMedioPago.PAT.FechaVencimiento,2));
                    CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_PAGO_FECHA_VENC_ANIO,StrRight(DatosMedioPago.PAT.FechaVencimiento,2));
                    CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_COPIA,DarTextoOrden(CantidadCopias,CantCopias,iif(DatosMedioPago.PAT.TipoTarjeta = TARJETA_PRESTO,'', UpperCase(ObtenerEmisoresTarjetasCredito(DMConnections.BaseCAC,DatosMedioPago.PAT.EmisorTarjetaCredito))))); // Rev. 4 (SS 733)
                    // SS 733
                    cdsDatosConvenioCompleto.Append;
                    cdsDatosConvenioCompletoMiRichEdit.AsString := CampoRich.RichText;
                    cdsDatosConvenioCompleto.Post;
                end;
                // Fin Rev. 4 (SS 733)

                RBInterface.Caption:=CONST_MANDATO_PAT;
                RBInterface.Report := ppReporteConvenioCompleto2;       // Rev. 3 (SS 733)
                ppReporteConvenioCompleto2.PrinterSetup.Copies := 1;    // Rev. 3 (SS 733)

                Result:=Imprimir(tthBlanco, 1, True, iif(DatosMedioPago.PAT.TipoTarjeta = TARJETA_PRESTO,'', UpperCase(ObtenerEmisoresTarjetasCredito(DMConnections.BaseCAC,DatosMedioPago.PAT.EmisorTarjetaCredito))));
                // Fin SS 733
        end else begin
                //Falabella
                Error_Impresion_Mandato := CONST_ERROR_IMPRESION_MANDATO_CMR;

                // Cargar campos...
                pplblPatente1.Caption := '';
                pplblPatente2.Caption := '';
                pplblPatente3.Caption := '';
                j := 0;
                for i := 0 to High(DatosCuentas) do begin
                    if not (DatosCuentas[i].TipoMovimiento in [ttmBaja, ttmRobado, ttmBajaSinTag]) then begin

                         if pplblPatente1.Caption = '' then
                             pplblPatente1.Caption := Trim(DatosCuentas[i].Cuenta.Vehiculo.Patente) + '-' + DatosCuentas[i].Cuenta.Vehiculo.DigitoPatente

                         else if pplblPatente2.Caption = '' then
                             pplblPatente2.Caption := Trim(DatosCuentas[i].Cuenta.Vehiculo.Patente) + '-' + DatosCuentas[i].Cuenta.Vehiculo.DigitoPatente

                         else if pplblPatente3.Caption = '' then
                             pplblPatente3.Caption := Trim(DatosCuentas[i].Cuenta.Vehiculo.Patente) + '-' + DatosCuentas[i].Cuenta.Vehiculo.DigitoPatente;

                         j := j + 1;
                    end;
                end;

                if j > 3 then begin
                    pplblLeyendaMasVehiculos.Visible := True;
                end else begin
                    pplblLeyendaMasVehiculos.Visible := False;
                    ppLabel11.Top := ppLabel11.Top - pplblLeyendaMasVehiculos.Height;
                    ppShape7.Top := ppShape7.Top - pplblLeyendaMasVehiculos.Height;;
                    pplblDuenoVehiculo.Top := pplblDuenoVehiculo.Top - pplblLeyendaMasVehiculos.Height;;
                end;

                if DatosTitular.Personeria = 'J' then pplblDuenoVehiculo.Caption := DatosTitular.RazonSocial
                else pplblDuenoVehiculo.Caption := ArmarNombrePersona(DatosTitular.Personeria,DatosTitular.Nombre,DatosTitular.Apellido,DatosTitular.ApellidoMaterno);

                if RegDatoMandante.Personeria = 'J' then pplblMandanteNombre.Caption := RegDatoMandante.RazonSocial
                else pplblMandanteNombre.Caption := ArmarNombrePersona(RegDatoMandante.Personeria,RegDatoMandante.Nombre,RegDatoMandante.Apellido,RegDatoMandante.ApellidoMaterno);

                // obtengo el domicilio y el email si la persona (mandante)
                // es el mismo que el titular del Convenio
                if trim(RegDatoMandante.NumeroDocumento) = trim(DatosTitular.NumeroDocumento) then begin
                    ppLineDireccion.Visible := False;
                    pplblMandanteDomicilio.Caption := ArmarDomicilioSimple(DMConnections.BaseCAC, DomicilioTitular.CalleDesnormalizada,
                                                DomicilioTitular.NumeroCalleSTR, DomicilioTitular.Piso, DomicilioTitular.Dpto, DomicilioTitular.Detalle );
                    pplblMandanteEmail.Caption := EMailTitular;
                end else begin
                    ppLineDireccion.Visible := True;
                    pplblMandanteDomicilio.Caption := '';
                    pplblMandanteEmail.Caption := '';
                end;

                ppLineaMail.Visible := pplblMandanteEmail.Caption = '';

                if trim(DatosMedioPago.Telefono) <> '' then
                        pplblMandanteTelefono.Caption := '(' + StrRight('0' + trim(inttostr(DatosMedioPago.CodigoArea)), 2) + ') ' + DatosMedioPago.Telefono
                else    pplblMandanteTelefono.Caption := '';
                {INICIO		: 20160928 CFU TASK_043_CFU_20160928
                pplblMandanteRUT.Caption :=  FormatearNumeroRUT(RegDatoMandante.NumeroDocumento);                }
                pplblMandanteRUT.Caption :=  QueryGetValue(DMConnections.BaseCAC, 'select dbo.FormatearRutConPuntos(''' + Trim(RegDatoMandante.NumeroDocumento) + ''')');
                //TERMINO	: 20160928 CFU TASK_043_CFU_20160928
                
                pplblMandanteNumeroTarjetaCMR.Caption := DatosMedioPago.PAT.NroTarjeta;

                // Decodifico la fecha para ponerla por separado...
                DecodeDate(NowBase(DMConnections.BaseCAC), Anio, Mes, Dia);
                pplblDia.Caption := FormatFloat('00', Dia);
                pplblMes.Caption := FormatFloat('00', Mes);
                pplblAnio.Caption := FormatFloat('00', Anio - 2000);

                // Setear Reporte...
                RBInterface.Caption := CONST_MANDATO_CMR;
                ppReporteImpresionConvenio.PrinterSetup.Copies := CantCopias;
                rbInterface.Report := ppReporteImpresionFalabella;  // el reporte

                // Inicia la impresion...
                Result := Imprimir(tthBlanco, CantCopias, True, CONST_COPIA_CMR);
                rbInterface.Report := ppReporteImpresionConvenio; // SS 733
                // ...y el resto sigue igual...
        end;

		if Result then MotivoCancelacion:=OK
		else MotivoCancelacion:=CANCELADO_USR;

	except
		on e: Exception do begin
            MsgBoxErr(Error_Impresion_Mandato, e.Message, CONST_ERROR_IMPRESION, MB_ICONERROR);
			result:=False;
        end;

    end;
end;


{******************************** Function Header ******************************
Function Name: TFormImprimirConvenio.ImprimirMandatoPAC
Author       : dcalani
Date Created : 26/06/2004
Description  : Imprime Mandato PAC.
Parameters   : NumeroConvenio:String; DatosMedioPago: TDatosMediosPago; RegDatoMandante: TDatosPersonales; var MotivoCancelacion:TMotivoCancelacion; CantCopias:Integer; MostrarInterface: Boolean
Return Value : Boolean
    Revision 1 :
       Author:    lcanteros
       Date:      07/Ago/2008
       Description: se le quita el numero de concesionaria al numero de convenio formateado
       (SS 725)

    Revision 2 :
       Author:    nefernandez
       Date:      20/Ago/2008
       Description: SS 725: Se solicita que se agregue '0' a la izquierda en el campo N�mero Convenio

    Revision 3 :
       Author : pdominguez
       Date   : 17/07/2009
       Description : SS 733
            - Se adapt� para la impresi�n al Reporte ppReporteConvenioCompleto.

    Revision : 4
       Author : pdominguez
       Date   : 19/08/2009
       Description : SS 733
        - Se cambia la asignaci�n del reporte al objeto ppReporteConvenioCompleto2: TppReport.

    Revision : 5
      Author : pdominguez
      Date   : 31/08/2009
      Description : SS 733
        - Se reestructura el bucle de impresi�n para sacar en cada hoja el literal
        'Copia de ...'.

Revision: 12
    Author : pdominguez
    Date   : 07/06/2010
    Description : SS 895 - Error Impresi�n Convenio CAC
        - Se cambia el LoadFromFile del objeto CampoRich, por la carga a trav�s
        de la funci�n CargaPlantillaRTF.
*******************************************************************************}
function TFormImprimirConvenio.ImprimirMandatoPAC(
	NumeroConvenio:String;
  DatosMedioPago: TDatosMediosPago; RegDatoMandante: TDatosPersonales;
  var MotivoCancelacion:TMotivoCancelacion;
  CantCopias:Integer): Boolean;
resourcestring
  CONST_MANDATO_PAC = 'Mandato PAC';
  CONST_ERROR_IMPRESION = 'Error de Impresi�n';
  CONST_ERROR_IMPRESION_MANDATO_PAC = 'Error al imprimir Mandato PAC';
  CONST_PREFIJO_NUM_CONVENIO = '0';
var
	PathArchivo:String;
    CantidadCopias: Integer;
begin
    try
        MotivoCancelacion:=PeaTypes.ERROR;

        //PAC
        if not(ObtenerParametroGeneral(DMConnections.BaseCAC,'PLANTILLA_PAC_BANCO'+trim(inttostr(DatosMedioPago.PAC.CodigoBanco)),PathArchivo)) then
            ObtenerParametroGeneral(DMConnections.BaseCAC,PLANTILLA_MANDATO_PAC,PathArchivo);

        if not(FileExists(PathArchivo)) then begin
            MsgBox(format(MSG_ERROR_ARCHIVO_ABRIR,[PathArchivo]),MSG_CAPTION_IMPRESION_CONVENIOS,MB_ICONSTOP);
			result:=False;
            Exit;
        end;
        // Rev. 5 (SS 733)
        cdsDatosConvenioCompleto.EmptyDataSet;

        for CantidadCopias := 1 to CantCopias do begin

            // Rev. 12 (SS 895)
            CampoRich.RichText := CargaPlantillaRTF(PathArchivo);
            //CampoRich.LoadFromFile(PathArchivo);
            // Fin Rev. 12 (SS 895)
            //CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_CONVENIO, CONST_PREFIJO_NUM_CONVENIO + ObtenerNumeroConvenioSinConcesionaria(NumeroConvenio));  //SS_1147_NDR_20150514
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_CONVENIO, ObtenerNumeroConvenioSinConcesionaria(NumeroConvenio));                                 //SS_1147_NDR_20150514

    		if RegDatoMandante.Personeria= PERSONERIA_JURIDICA then begin
	    		CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_PAGO_NOMBRE,RegDatoMandante.RazonSocial);
	            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_PAGO_NOMBRE_SOLO, RegDatoMandante.RazonSocial);
			    CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_PAGO_APELLIDO_PATERNO, '');
    			CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_PAGO_APELLIDO_MATERNO, '');
            end else begin
            	CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_PAGO_NOMBRE,ArmarNombrePersona(RegDatoMandante.Personeria,RegDatoMandante.Nombre,RegDatoMandante.Apellido,RegDatoMandante.ApellidoMaterno));
	            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_PAGO_NOMBRE_SOLO, trim(RegDatoMandante.Nombre));
        	    CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_PAGO_APELLIDO_PATERNO, trim(RegDatoMandante.Apellido));
	    		CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_PAGO_APELLIDO_MATERNO, trim(RegDatoMandante.ApellidoMaterno));
            end;

            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_PAGO_RUT,RegDatoMandante.NumeroDocumento);

            if trim(DatosMedioPago.Telefono)<>'' then
            	CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_PAGO_TELEFONO,'('+StrRight('0'+trim(inttostr(DatosMedioPago.CodigoArea)),2)+') '+DatosMedioPago.Telefono)
            else
    			CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_PAGO_TELEFONO,'');

            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_PAGO_TIPO_CUENTA,ObtenerCuentasBancarias(DMConnections.BaseCAC,DatosMedioPago.PAC.TipoCuenta));
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_PAGO_NUMERO_CUENTA,DatosMedioPago.PAC.NroCuentaBancaria);
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_PAGO_BANCO,ObtenerDescripcionBanco(DMConnections.BaseCAC,DatosMedioPago.PAC.CodigoBanco));
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_PAGO_SUCURSAL,Trim(DatosMedioPago.PAC.Sucursal));
	    	CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_PAGO_DIA,FormatDateTime('dd',NowBase(DMConnections.BaseCAC)));
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_PAGO_MES,FormatDateTime('mmmmm',NowBase(DMConnections.BaseCAC)));
    		CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_PAGO_ANIO,FormatDateTime('yyyy',NowBase(DMConnections.BaseCAC)));
            // Rev. 3 (SS 733)
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_COPIA,DarTextoOrden(CantidadCopias,CantCopias, UpperCase(ObtenerDescripcionBanco(DMConnections.BaseCAC,DatosMedioPago.PAC.CodigoBanco)))); // Rev. 5 (SS 733)

            cdsDatosConvenioCompleto.Append;
            cdsDatosConvenioCompletoMiRichEdit.AsString := CampoRich.RichText;
            cdsDatosConvenioCompleto.Post;
        end;
        // Fin Rev. 5 (SS 733)
		RBInterface.Caption:=CONST_MANDATO_PAC;
        RBInterface.Report := ppReporteConvenioCompleto2; // Rev. 4 (SS 733)
        ppReporteConvenioCompleto2.PrinterSetup.Copies := 1; // Rev. 4 (SS 733)

        Result:=Imprimir(tthBlanco ,1, True, UpperCase(ObtenerDescripcionBanco(DMConnections.BaseCAC,DatosMedioPago.PAC.CodigoBanco)));
        // Fin Rev. 3 (SS 733)
		if Result then MotivoCancelacion:=OK
        else MotivoCancelacion:=CANCELADO_USR;

    except
    	on e: Exception do begin
			MsgBoxErr(CONST_ERROR_IMPRESION_MANDATO_PAC, e.Message, CONST_ERROR_IMPRESION, MB_ICONERROR);
	        result:=False;
        end;

    end;
end;

{******************************** Function Header ******************************
Function Name: TFormImprimirConvenio.ImprimirContratoMoto
Author       : FSandi
Date Created :
Description  : Imprime contrato de Motos
Parameters   : NumeroConvenio:String; RegDatoMandante: TDatosPersonales; var MotivoCancelacion:TMotivoCancelacion; CantCopias:Integer;
Return Value : Boolean

Revision:  1
Author:     FSandi
Date: 31-05-2007
Description: Se agrega el parametro TipoImpresion, para determinar si es impresion de contrato de motos nuevas, o reimpresion
            de todas las motos del convenio. Si es tinuevas, se imprimen las nuevas motos del convenio, si es tiReimpresion, se
            imprimen todas las motos del convenio.
            Tambien se corrigi� la impresi�n del anexo de motos, porque no estaba tomando en cuenta que el registro que recibia contenia
            todos los vehiculos, no solo motos. Ahora se le envia un Aux_Vehiculos controlado con los datos que estaba esperando antes.

Revision: 2
    Author: pdominguez
    Date: 25/03/2009
    Description: SS 733
        - Se adapt� para la impresi�n al Reporte ppReporteConvenioCompleto.

Revision : 3
    Author : pdominguez
    Date   : 19/08/2009
    Description : SS 733
        - Se cambia la asignaci�n del reporte al objeto ppReporteConvenioCompleto2: TppReport.

Revision : 4
    Author : pdominguez
    Date   : 21/08/2009
    Description : SS 733
        - Se Cambi� la forma del par�metro Veh�culo para pasarlo por referencia.

Revision: 12
    Author : pdominguez
    Date   : 07/06/2010
    Description : SS 895 - Error Impresi�n Convenio CAC
        - Se cambia el LoadFromFile del objeto CampoRich, por la carga a trav�s
        de la funci�n CargaPlantillaRTF.
*******************************************************************************}
function TFormImprimirConvenio.ImprimirContratoMoto(NumeroConvenio:String;
                                            var Vehiculo:Array of TCuentaABM;
                                            DatoTitular:TDatosPersonales;
                                            TipoImpresion: TTipoImpresionMotos;     // revision 1
                                            var MotivoCancelacion:TMotivoCancelacion;
                                            CantCopias:Integer): Boolean;
resourcestring
  CONST_CONTRATO_MOTO = 'Contrato Moto';
  CONST_ERROR_IMPRESION = 'Error de Impresi�n';
  CONST_ERROR_IMPRESION_CONTRATO_MOTO = 'Error al imprimir Contrato Moto';
var
	PathArchivo:String;
    i,ContadorMotos, cantidadMotos, posiciondelcontadormotos:Integer;
  	auxDescripTipo, auxDescripCategoria, auxCategoria, Continua:String;
    aux_Vehiculo:  array of TCuentaABM;
begin
    try
        MotivoCancelacion:=PeaTypes.ERROR;

        //Contrato Moto
        if not(ObtenerParametroGeneral(DMConnections.BaseCAC,'PLANTILLA_CONTRATO_MOTO',PathArchivo)) then
            ObtenerParametroGeneral(DMConnections.BaseCAC,PLANTILLA_CONTRATO_MOTO,PathArchivo);

        if not(FileExists(PathArchivo)) then begin
            MsgBox(format(MSG_ERROR_ARCHIVO_ABRIR,[PathArchivo]),MSG_CAPTION_IMPRESION_CONVENIOS,MB_ICONSTOP);
			result:=False;
            Exit;
        end;

        //Se carga la Plantilla
        // Rev. 12 (SS 895)
        CampoRich.RichText := CargaPlantillaRTF(PathArchivo);
        //CampoRich.LoadFromFile(PathArchivo);
        // Fin Rev. 12 (SS 895)

   //Se llenan los datos de la plantilla
        //Se carga la fecha Dia, Mes, A�o
 		CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_DIA,FormatDateTime('dd',NowBase(DMConnections.BaseCAC)));
		CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_MES,FormatDateTime('mmmm',NowBase(DMConnections.BaseCAC)));
    	CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_ANIO,FormatDateTime('yyyy',NowBase(DMConnections.BaseCAC)));

       //Se carga el nombre o razon social
		if DatoTitular.Personeria= PERSONERIA_JURIDICA then begin
			CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_NOMBRE,DatoTitular.RazonSocial);
			CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_APELLIDO_PATERNO, '');
			CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_APELLIDO_MATERNO, '');
        end else begin
        	CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_NOMBRE,Trim(DatoTitular.Nombre));
    	    CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_APELLIDO_PATERNO, trim(DatoTitular.Apellido));
			CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_APELLIDO_MATERNO, trim(DatoTitular.ApellidoMaterno));
        end;

        //Cargo el Rut
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_RUT,DatoTitular.NumeroDocumento);

        //Cargo las motos
        ContadorMotos := 0;
        posiciondelcontadormotos := 0;
//Revision 1
        if tipoimpresion = tiNuevas then begin
            //Llena todas las motos nuevas
            for i:=0 to Length(Vehiculo)-1 do begin
                ObtenerDescripTipoVehiculo(DMConnections.BaseCAC,Vehiculo[i].Cuenta.Vehiculo.CodigoTipo,auxDescripTipo,auxDescripCategoria,auxCategoria);
                if ((Trim (AuxCategoria) = '4') and (Vehiculo[i].TipoMovimiento = ttmAlta)) then  //categoria 4 Tag de Moto
                begin
                    ContadorMotos := ContadorMotos + 1;
                    CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_PATENTE_VEHICULO+Inttostr(ContadorMotos),Trim(Vehiculo[i].Cuenta.Vehiculo.Patente) + ' - ' + Vehiculo[i].Cuenta.Vehiculo.DigitoPatente);
                    CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_MARCA_VEHICULO+Inttostr(ContadorMotos),Vehiculo[i].Cuenta.Vehiculo.Marca);
                    CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_MODELO_VEHICULO+Inttostr(ContadorMotos),iif(Trim(Vehiculo[i].Cuenta.Vehiculo.Modelo) = '', '', Trim(strLeft(Vehiculo[i].Cuenta.Vehiculo.Modelo, CANT_CARACTERES_MODELO))+'.'));
                    CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TAG_VEHICULO+Inttostr(ContadorMotos),SerialNumberToEtiqueta(inttostr(Vehiculo[i].Cuenta.ContactSerialNumber)));
                    if ContadorMotos = TAG_CON_CANTIDAD_MAX_MOTOS_HOJA  then begin //5 motos m�ximo en la p�gina
                        posiciondelcontadormotos := i+1;
                        Break;
                    end;
                end;
            end;
        end;

        if tipoimpresion = tiReimpresion then begin
            //Llena todas las motos del convenio
            for i:=0 to Length(Vehiculo)-1 do begin
                ObtenerDescripTipoVehiculo(DMConnections.BaseCAC,Vehiculo[i].Cuenta.Vehiculo.CodigoTipo,auxDescripTipo,auxDescripCategoria,auxCategoria);
                if ((Trim (AuxCategoria) = '4') and (Vehiculo[i].TipoMovimiento in [ttmSuspendido, ttmActivado,ttmVigente,ttmModif, ttmModifTag, ttmAlta, ttmTagPerdido])) then  //categoria 4 Tag de Moto
                begin
                    ContadorMotos := ContadorMotos + 1;
                    CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_PATENTE_VEHICULO+Inttostr(ContadorMotos),Trim(Vehiculo[i].Cuenta.Vehiculo.Patente) + ' - ' + Vehiculo[i].Cuenta.Vehiculo.DigitoPatente);
                    CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_MARCA_VEHICULO+Inttostr(ContadorMotos),Vehiculo[i].Cuenta.Vehiculo.Marca);
                    CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_MODELO_VEHICULO+Inttostr(ContadorMotos),iif(Trim(Vehiculo[i].Cuenta.Vehiculo.Modelo) = '', '', Trim(strLeft(Vehiculo[i].Cuenta.Vehiculo.Modelo, CANT_CARACTERES_MODELO))+'.'));
                    CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TAG_VEHICULO+Inttostr(ContadorMotos),SerialNumberToEtiqueta(inttostr(Vehiculo[i].Cuenta.ContactSerialNumber)));
                    if ContadorMotos = TAG_CON_CANTIDAD_MAX_MOTOS_HOJA  then begin //5 motos m�ximo en la p�gina
                        posiciondelcontadormotos := i+1;
                        Break;
                    end;
                end;
            end;
        end;

//Fin de Revision 1

        //Llena con espacios vacios los espacios restantes que no tienen motos
		for i:=ContadorMotos to TAG_CON_CANTIDAD_MAX_MOTOS_HOJA do begin  //5 motos m�ximo en la p�gina
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_PATENTE_VEHICULO+Inttostr(i),'');
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_MARCA_VEHICULO+Inttostr(i),'');
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_MODELO_VEHICULO+Inttostr(i),'');
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TAG_VEHICULO+Inttostr(i),'');
		end;

        Continua := '';

   //Revision 1 modificamos el anexo de motos para que solamente levante motos, no todos los vehiculos
        CantidadMotos := 0;
        //Calculamos la cantidad de motos si es para motos nuevas
        if tipoimpresion = tiNuevas then begin
            for i := 0 to Length(Vehiculo)-1 do begin
                ObtenerDescripTipoVehiculo(DMConnections.BaseCAC,Vehiculo[i].Cuenta.Vehiculo.CodigoTipo,auxDescripTipo,auxDescripCategoria,auxCategoria);
                if ((Trim (AuxCategoria) = '4') and (Vehiculo[i].TipoMovimiento = ttmAlta)) then  begin
                    CantidadMotos := CantidadMotos +1;
                end;
            end;
        end;
        //Calculamos la cantidad de motos si es para reimpresion
        if tipoimpresion = tiReimpresion then begin
            for i := 0 to Length(Vehiculo)-1 do begin
                ObtenerDescripTipoVehiculo(DMConnections.BaseCAC,Vehiculo[i].Cuenta.Vehiculo.CodigoTipo,auxDescripTipo,auxDescripCategoria,auxCategoria);
                if ((Trim (AuxCategoria) = '4') and (Vehiculo[i].TipoMovimiento in [ttmSuspendido, ttmActivado,ttmVigente,ttmModif, ttmModifTag, ttmAlta, ttmTagPerdido])) then begin
                    CantidadMotos := CantidadMotos +1;
                end;
            end;
        end;

        //Si tengo mas de el m�ximo de motos tengo que poner un mensaje de que contin�a.
        if CantidadMotos> TAG_CON_CANTIDAD_MAX_MOTOS_HOJA  then begin
            //Obtengo el mensaje que le van a poner a la hoja
            ObtenerParametroGeneral(DMConnections.BaseCAC,'TAG_CONTINUA_ANEXO_MOTO',Continua)
        end;
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CONTINUA_ANEXO_MOTO,Continua);

        // SS 733
    	RBInterface.Caption:=CONST_CONTRATO_MOTO;
        RBInterface.Report := ppReporteConvenioCompleto2; // Rev. 3 (SS 733)
		RBInterface.Report.PrinterSetup.Copies:=CantCopias;

        cdsDatosConvenioCompleto.EmptyDataSet;
        cdsDatosConvenioCompleto.Append;
        cdsDatosConvenioCompletoMiRichEdit.AsString := CampoRich.RichText;
        cdsDatosConvenioCompleto.Post;


        ContadorMotos:= 0;

//        if Imprimir(tthBlanco ,CantCopias, True, '') then begin   // SS 733
            //Si la cantidad de motos que tengo, es mayor a la cantidad maxima de motos por hoja, entonces
            if CantidadMotos> TAG_CON_CANTIDAD_MAX_MOTOS_HOJA then begin
                SetLength(aux_Vehiculo,CantidadMotos - TAG_CON_CANTIDAD_MAX_MOTOS_HOJA); //Nuestro Aux_vehiculo, contendra todas las motos extra (si tenemos 9, y un maximo de 5, entonces contendra 4)
                for i := posiciondelcontadormotos to Length(Vehiculo)-1 do begin  //recorremos los vehiculos desde el ultimo que recorrimos en la primera tanda de motos
                    ObtenerDescripTipoVehiculo(DMConnections.BaseCAC,Vehiculo[i].Cuenta.Vehiculo.CodigoTipo,auxDescripTipo,auxDescripCategoria,auxCategoria);
                    if tipoimpresion = tiNuevas then begin
                        if ((Trim (AuxCategoria) = '4') and (Vehiculo[i].TipoMovimiento= ttmAlta)) then begin
                            aux_Vehiculo[ContadorMotos] := vehiculo[i];
                            ContadorMotos:= ContadorMotos + 1;
                        end;
                    end;
                    if tipoimpresion = tiReimpresion then begin
                        if ((Trim (AuxCategoria) = '4') and (Vehiculo[i].TipoMovimiento in [ttmSuspendido, ttmActivado,ttmVigente,ttmModif, ttmModifTag, ttmAlta, ttmTagPerdido])) then begin
                            aux_Vehiculo[ContadorMotos] := vehiculo[i];
                            ContadorMotos:= ContadorMotos + 1;
                        end;
                    end;
                end;
    //Fin de Revision 1 
                Result := ImprimirAnexoMotos(PERSONERIA_FISICA,NumeroConvenio, aux_Vehiculo,MotivoCancelacion,CantCopias);
            end else begin
                if Imprimir(tthBlanco ,CantCopias, True, '') then begin
                    result:=True;
                    MotivoCancelacion:=OK;
                end else begin
                    result:=False;
                    MotivoCancelacion:=CANCELADO_USR;
                end;
            end;
{        end else begin   // SS 733
            result:=False;
            MotivoCancelacion:=CANCELADO_USR;
        end;}

    except
    	on e: Exception do begin
			MsgBoxErr(CONST_ERROR_IMPRESION_CONTRATO_MOTO, e.Message, CONST_ERROR_IMPRESION, MB_ICONERROR);
	        result:=False;
        end;
    end;
end;

{******************************** Function Header ******************************
Function Name: TFormImprimirConvenio.ImprimirCaratula
Author       : dcalani
Date Created : 26/06/2004
Description  : Imprime Car�tula. La car�tula debe imprimirse siempre.
Parameters   : Personeria, NumeroConvenio: String; DatoTitular: TDatosPersonales; DocPresentada:RegDocumentacionPresentada; var MotivoCancelacion:TMotivoCancelacion; CantCopias:Integer; MostrarInterface: Boolean
Return Value : Boolean

Revision : 1
    Author : pdominguez
    Date   : 17/07/2009
    Description : SS 733
        - Se adapt� para la impresi�n al Reporte ppReporteConvenioCompleto. Se
        a�adi� el par�metro PrimeraHoja.

Revision : 2
    Author : pdominguez
    Date   : 19/08/2009
    Description : SS 733
        - Se cambia la asignaci�n del reporte al objeto ppReporteConvenioCompleto2: TppReport.

Revision: 12
    Author : pdominguez
    Date   : 07/06/2010
    Description : SS 895 - Error Impresi�n Convenio CAC
        - Se cambia el LoadFromFile del objeto CampoRich, por la carga a trav�s
        de la funci�n CargaPlantillaRTF.
*******************************************************************************}
function TFormImprimirConvenio.ImprimirCaratula(Personeria,
  NumeroConvenio: String; DatoTitular: TDatosPersonales;
  DocPresentada: RegDocumentacionPresentada;
  var MotivoCancelacion:TMotivoCancelacion;
  CantCopias:Integer;
  Operador : String;
  PuntoEntrega : String;
  PrimeraHoja: Boolean = True): Boolean;

const
    CaractPorLinea = 75;
resourcestring
	CONST_CARATULA_CONVENIO='Car�tula del Convenio - Personer�a ';
	CONST_FISICA = 'F�sica';
	CONST_NATURAL = 'Natural';
	CONST_ERROR_IMPRESION = 'Error de Impresi�n';
	CONST_ERROR_IMPRESION_CARATULA = 'Error al imprimir Car�tula';
var
	DescDoc, PathArchivo:String;
    Aux, j,i:Integer;
	aux_DocPresentada:RegDocumentacionPresentada;
	EstaDocUbicacion: Boolean;
begin
	if not EstaFuenteMarcaInstalada then begin
		Result := False;
		MotivoCancelacion := PeaTypes.ERROR;
		exit;
    end;


    try
        MotivoCancelacion:=PeaTypes.ERROR;

        EstaDocUbicacion := False;
        for i := 0 to Length(DocPresentada) - 1 do
            if DocPresentada[i].CodigoDocumentacionRespaldo = CODIGO_DOCUMENTACION_UBICACION_CARPETA then EstaDocUbicacion := True;

        if not EstaDocUbicacion then begin
            SetLength(DocPresentada, length(DocPresentada) + 1);
            DocPresentada[High(DocPresentada)].CodigoDocumentacionRespaldo := CODIGO_DOCUMENTACION_UBICACION_CARPETA;
        end;            


        if Personeria=PERSONERIA_JURIDICA then ObtenerParametroGeneral(DMConnections.BaseCAC,PLANTILLA_CARATULA_JURIDICA,PathArchivo)
		else ObtenerParametroGeneral(DMConnections.BaseCAC,PLANTILLA_CARATULA_NATURAL,PathArchivo);

        if not(FileExists(PathArchivo)) then begin
            MsgBox(format(MSG_ERROR_ARCHIVO_ABRIR,[PathArchivo]),MSG_CAPTION_IMPRESION_CONVENIOS,MB_ICONSTOP);
			result:=False;
            Exit;
        end;

        // Rev. 12 (SS 895)
        CampoRich.RichText := CargaPlantillaRTF(PathArchivo);
        //CampoRich.LoadFromFile(PathArchivo);
        // Fin Rev. 12 (SS 895)

        CampoRich.RichText := ReplaceTag(CampoRich.RichText,TAG_CAR_NUMERO_CONVENIO,NumeroConvenio);
        ReplaceCodeBar(TAG_CODIGO_BARRA, NumeroConvenio, CampoRich);

        if Personeria = PERSONERIA_JURIDICA then begin
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CAR_NOMBRE,ArmarNombrePersona(PERSONERIA_JURIDICA,ArmarNombrePersona(PERSONERIA_FISICA,DatoTitular.Nombre,DatoTitular.Apellido,DatoTitular.ApellidoMaterno),DatoTitular.RazonSocial,''));
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CAR_GIRO, DatoTitular.Giro);
        end
        else CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CAR_NOMBRE,ArmarNombrePersona(PERSONERIA_FISICA,DatoTitular.Nombre,DatoTitular.Apellido,DatoTitular.ApellidoMaterno));

        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CAR_RUT,DatoTitular.NumeroDocumento);

        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CAR_PUNTO_ENTREGA, PuntoEntrega);
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CAR_OPERADOR, Operador);
		CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CAR_FECHA_HORA,FormatDateTime('dd/mm/yyyy hh:nn',NowBase(DMConnections.BaseCAC)));

        j:=0;
        i:=0;
        while i < CAR_MAX_RENGLONES do begin
            if (j < Length(DocPresentada)) then begin
                if not(DocPresentada[j].NoCorresponde) and (DocPresentada[j].SoloImpresion <> 2) then begin

                    DescDoc := ObtenerDescripDocumentacion(DocPresentada[j]);
                    //Si la desc. ocupa mas de una linea la voy cortando
                    if (Length(DescDoc) > CaractPorLinea) then begin
                        while (Trim(DescDoc) <> EmptyStr) do begin
                            Aux := CaractPorLinea;
                            while (Aux > 0) and (DescDoc[Aux] <> ' ') do
                                Dec(Aux);
                            if Aux = 0 then begin
                                Insert('-', DescDoc, CaractPorLinea);
                                Aux := CaractPorLinea + 1;
                            end;
                            CampoRich.RichText := ReplaceTag(CampoRich.RichText,TAG_CAR_DOC+inttostr(i+1), Copy(DescDoc, 1, Aux));
                            if Length(DescDoc) <= CaractPorLinea then
                                ReplaceMarcaCheck(TAG_CAR_MARCA+inttostr(i+1),CampoRich)
                            else begin
                                CampoRich.RichText := ReplaceTag(CampoRich.RichText,TAG_CAR_MARCA+inttostr(i+1), EmptyStr);
                                Inc(i);
                            end;
                            Delete(DescDoc, 1, Aux);
                        end
                    end
                    else begin
                        CampoRich.RichText := ReplaceTag(CampoRich.RichText,TAG_CAR_DOC+inttostr(i+1), DescDoc);
                        ReplaceMarcaCheck(TAG_CAR_MARCA+inttostr(i+1),CampoRich);
                    end;

				end else dec(i);
            end else begin
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CAR_DOC+inttostr(i+1),'');
				CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CAR_MARCA+inttostr(i+1),'');
            end;
            inc(j);
            inc(i);
        end;

		CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_FIRMA_REVISOR, iif(Length(DocPresentada) <= CAR_MAX_RENGLONES, STR_FIRMA_REVISOR, ''));
		CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_LUGAR_FIRMA_REVISOR, iif(Length(DocPresentada) <= CAR_MAX_RENGLONES, STR_LUGAR_FIRMA_REVISOR, ''));

        // SS 733
        if PrimeraHoja then cdsDatosConvenioCompleto.EmptyDataSet;

        cdsDatosConvenioCompleto.Append;
        cdsDatosConvenioCompletoMiRichEdit.AsString := CampoRich.RichText;
        cdsDatosConvenioCompleto.Post;
        // Fin SS 733

        if Length(DocPresentada) > CAR_MAX_RENGLONES then begin
            for i := j to Length(DocPresentada) - 1 do begin
                SetLength(aux_DocPresentada,Length(aux_DocPresentada)+1);
                aux_DocPresentada[i - j]:=DocPresentada[i];
            end;
            Result:=ImprimirCaratula(Personeria,NumeroConvenio,DatoTitular,aux_DocPresentada,MotivoCancelacion,CantCopias, Operador, PuntoEntrega, False);    //SS_1380_NDR_20151009//SS_1380_MCA_20150911
        end else begin
            // SS 733
            RBInterface.Caption:=CONST_CARATULA_CONVENIO+iif(Personeria=PERSONERIA_FISICA,CONST_FISICA,CONST_NATURAL);
            RBInterface.Report := ppReporteConvenioCompleto2; // Rev. 2 (SS 733)

            ppReporteConvenioCompleto2.PrinterSetup.Copies:=CantCopias; // Rev. 2 (SS 733)
            // Fin SS 733
            if Imprimir(tthBlanco, CantCopias) then begin
                result:=True;
                MotivoCancelacion:=OK;
            end else begin
                Result:=False;
                MotivoCancelacion:=CANCELADO_USR;
            end;
        end;
    except
    	on e: Exception do begin
			MsgBoxErr(CONST_ERROR_IMPRESION_CARATULA, e.Message, CONST_ERROR_IMPRESION, MB_ICONERROR);
			result:=False;
		end;

    end;

end;
{-----------------------------------------------------------------------------
  Function Name: 
  Author:
  Date Created:  /  /
  Description:
  Parameters: Sender: TObject
  Return Value: N/A

  Revision : 1
    Date: 03/03/2009
    Author: pdominguez
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function TFormImprimirConvenio.ObtenerDescripDocumentacion(
  DocPresentada:TRegistroDocumentacion): String;
resourcestring
	CONST_PATENTE = ' - Patente: ';
var
    aux:String;
begin
    if DocPresentada.CodigoDocumentacionRespaldo = CODIGO_DOCUMENTACION_UBICACION_CARPETA then Result := DOCUMENTACION_UBICACION_CARPETA
    else begin
        aux:='';
		aux:=iif(trim(DocPresentada.Patente)='','',CONST_PATENTE+trim(DocPresentada.Patente))+iif(trim(DocPresentada.InfoAdicional)='','',' ('+trim(DocPresentada.InfoAdicional)+')');
        result:=trim(QueryGetValue(DMConnections.BaseCAC,'select Descripcion from maestrodocumentacionrequerida WITH (NOLOCK) WHERE CodigoDocumentacionRespaldo='+inttostr(DocPresentada.CodigoDocumentacionRespaldo)))
                    +aux;
    end;
end;

{******************************** Function Header ******************************
Procedure Name: DarTextoOrden
Author :
Date Created :
Parameters : NroCopia, CantMaximaCopia: Integer;NombreBanco:String=''
Return Value : String
Description :

Revision : 1
    Author : pdominguez
    Date   : 31/08/2009
    Description : SS 733
        - Se modificaron las constantes del texto 'Copia ...'
*******************************************************************************}
function TFormImprimirConvenio.DarTextoOrden(NroCopia,
  CantMaximaCopia: Integer;NombreBanco:String=''): String;
resourcestring
    //CONST_CONCESIONARIA = 'Copia para Costanera Norte';   //SS_1147_MCA_20150420 // Re. 1 (SS 733)
	CONST_CONCESIONARIA_CN = 'Copia para Costanera Norte';  //SS_1147_MCA_20150420 // Re. 1 (SS 733)
    CONST_CONCESIONARIA_VS = 'Copia para Vespucio Sur';     //SS_1147_MCA_20150420 // Re. 1 (SS 733)
	CONST_CLIENTE = 'Copia para el Cliente'; // Re. 1 (SS 733)
	CONST_TRIPLICADO = 'Triplicado';
	CONST_CUDRUPLICADO = 'Cuadriplicado';
	CONST_QUINTUPLICADO = 'Quintuplicado';
    CONST_BANCO = 'Copia para %s'; // Rev. 1 (SS 733)
begin
	result:='';
	case NroCopia of
        //1: result:=CONST_CONCESIONARIA;                                       //SS_1147_MCA_20150420
		1: result:=iif(ObtenerCodigoConcesionariaNativa()=CODIGO_VS, CONST_CONCESIONARIA_VS, CONST_CONCESIONARIA_CN);   //SS_1147_MCA_20150420
		2: result:=CONST_CLIENTE;
		3: result:=iif(trim(NombreBanco)='',CONST_TRIPLICADO,format(CONST_BANCO,[NombreBanco])); // Rev. (SS 733)
		4: result:=CONST_CUDRUPLICADO;
		5: result:=CONST_QUINTUPLICADO;
	else
		result:='';
    end;
end;

(*function TFormImprimirConvenio.SetImpresora(NroBandeja:Integer):Boolean;
var
   ADevice, ADriver, APort : array [0..255] of Char;
   ADeviceMode : THandle;
   DevMode : PDevMode;
begin
    case NroBandeja of
        1: NroBandeja:=264;
        2: NroBandeja:=263;
        3: NroBandeja:=262;
	end;

    with Printer do begin //FPrinters do begin
        GetPrinter (ADevice, ADriver, APort, ADeviceMode);
        SetPrinter (ADevice, ADriver, APort, 0);
        GetPrinter (ADevice, ADriver, APort, ADeviceMode);
        DevMode := GlobalLock(ADeviceMode);
        if not Assigned(DevMode) then begin
			ShowMessage('Error, todo mal con la impresora..');
			result:=False;
            Exit;
        end;
		with DevMode^ do begin
            dmDefaultSource := NroBandeja;
            dmFields := dmFields or DM_DEFAULTSOURCE;
        end;
        GlobalUnLock(ADeviceMode);
		SetPrinter(ADevice, ADriver, APort, ADeviceMode);
    end;

    result:=True;
end;
*)
(*procedure TFormImprimirConvenio.prueba;
var
    PathArchivo:String;
begin
    SetImpresora(2);
    RichEdit1.Clear;
    RichEdit1.Text:=FileToString(PathArchivo);
    RichEdit1.Print('pp');
	SetImpresora(3);
    RichEdit1.Print('pp');
end;
*)

{******************************** Function Header ******************************
Procedure Name: Imprimir
Author :
Date Created :
Parameters : TipoHoja : TTipoHoja; CantCopias:Integer; ImprimirNroCopia: Boolean = True; NombreBanco:String = ''
Return Value : Boolean
Description :

Revision : 1
    Author : pdominguez
    Date   : 31/08/2009
    Description : SS 733
        - Se cambia la impresi�n del TAG_COPIA de las plantillas al reporte
        predise�ado ppReporteConvenioCompleto. Valido para el estado de
        convenio persona jur�dica, natural y anexos.
*******************************************************************************}
function TFormImprimirConvenio.Imprimir(TipoHoja : TTipoHoja;
  CantCopias:Integer; ImprimirNroCopia: Boolean = True; NombreBanco:String = ''): Boolean;
var
	i:Integer;
	AuxStr: AnsiString;
	PRNConfig: TRBConfig;
	Delay: Int64;
	MostrarInterface : Boolean;
resourcestring
	CONST_MSG_ERROR_IMPRESION = 'Error de Impresi�n';
	CONST_MSG_ERROR = 'Error';
begin
	result:=true;
	i:=1;
	AuxStr:= CampoRich.RichText;
	MostrarInterface := False;

	if TipoHoja <> Hoja then begin
		if  TipoHoja = tthBlanco then begin
			if MsgBox(MSG_CAPTION_PAPEL_BLANCO,MSG_CAPTION_IMPRESION_CONVENIOS,MB_ICONWARNING+MB_YESNO) = mrNo then begin
				Result := False;
				exit;
			end else begin
				Hoja := tthBlanco;
				MostrarInterface := True;
			end;
		end;

		if  TipoHoja = tthConvenio then begin
			if MsgBox(MSG_CAPTION_PAPEL_CONVENIO,MSG_CAPTION_IMPRESION_CONVENIOS,MB_ICONWARNING+MB_YESNO) = mrNo then begin
				Result := False;
				exit;
			end else begin
				Hoja := tthConvenio;
				MostrarInterface := True;
			end;
		end;

	end;
	//Hacemos que el default sea la impresora para imprimir el convenio

	if MostrarInterface then begin
		if (InstallIni.ReadInteger('General', 'SoloImpresora', 1) = 1) then begin
			PRNConfig := RBInterface.GetConfig;
			PRNConfig.DeviceType := 'Printer';
			RBInterface.SetConfig(PRNConfig);
		end;
	end;
	while (result) and (i <= CantCopias) do begin
		try
            // Reporte NO Falabella
            // Rev. 1 (SS 733)
//            CampoRich.RichText:= ReplaceTag(AuxStr, TAG_COPIA, iif (ImprimirNroCopia, DarTextoOrden(i,CantCopias, NombreBanco), ''));
            pplblCopia.Caption := iif (ImprimirNroCopia, DarTextoOrden(i,CantCopias, NombreBanco), '');
            // Fin Rev. 1 (SS 733)
            // Reporte PAT CMR-Falabella
            RTCopia.Clear;
            RTCopia.RichText:= iif (ImprimirNroCopia, DarTextoOrden(i,CantCopias, NombreBanco), '');
            RTCopia.SelectAll;
            RTCopia.SelAttributes.Name := 'Arial';
            RTCopia.SelAttributes.Size := 12;
            RTCopia.Paragraph.Alignment := taRightJustify;
            // Mandamos a imprimir y hacemos una pausa para desagotar
            Result:= RBInterface.Execute(MostrarInterface);
            Delay := GetMSCounter + 3000;
            while Delay > GetMSCounter do begin
                Sleep(100);
	        end;
			MostrarInterface:=False;
			inc(i);
		except
			on e: Exception do begin
				MsgBoxErr(CONST_MSG_ERROR_IMPRESION, e.Message, CONST_MSG_ERROR, MB_ICONERROR);
				Break;
			end;
		end;
	end;
end;


{******************************** Function Header ******************************
Function Name: TFormImprimirConvenio.ImprimirEtiquetas
Author       : dcalani
Date Created : 26/06/2004
Description  : Imprime etiquetas para tags.
Parameters   : NumeroConvenio: String; DatoTitular: TDatosPersonales; Vehiculo: array of TCuentaVehiculo
Return Value : Boolean

Revision: 1
    Author : pdominguez
    Date   : 21/08/2009
    Description : SS 733
        - Se Cambi� la forma del par�metro Veh�culo para pasarlo por referencia.
*******************************************************************************}
function TFormImprimirConvenio.ImprimirEtiquetas(
  NumeroConvenio: String; DatoTitular: TDatosPersonales;
  var Vehiculo: array of TCuentaABM;
  var MotivoCancelacion:TMotivoCancelacion) : Boolean;
var
	i:Integer;
resourcestring
	CONST_ERROR_APERTURA_PUERTO = 'Error al abrir el puerto';
	CONST_PATENTE = 'Patente: ';
	CONST_TAG = 'Telev�a: ';
begin
	try
		//set puerto y velocidad
        //{ INICIO : 20160315 MGO
		SPort.ComNumber:=ApplicationIni.ReadInteger('Impresora Etiqueta','Puerto',1);
		SPort.BaudRate:=ApplicationIni.ReadInteger('Impresora Etiqueta','Velocidad',19200);
        {
        SPort.ComNumber:=InstallIni.ReadInteger('Impresora Etiqueta','Puerto',1);
		SPort.BaudRate:=InstallIni.ReadInteger('Impresora Etiqueta','Velocidad',19200);
        }
        // FIN : 20160315

		SPort.Active := true;
		if not(SPort.Active) then raise ERangeError.CreateFmt(CONST_ERROR_APERTURA_PUERTO,[]);

		SPort.SendString(INIT  + CR);

		for i:=0 to length(Vehiculo)-1 do begin
			// Impresion de las etiquetas

			PrintText(CONST_PATENTE + Vehiculo[i].Cuenta.Vehiculo.Patente,taCentro,tmDobleNegrito);
			PrintText(CONST_TAG + SerialNumberToEtiqueta(IntToStr(Vehiculo[i].Cuenta.ContactSerialNumber)),taCentro,tmNegrita);

			SPort.SendString(#$1B#$4A#203); // salto de eqtiqueta
		end;
		result:=True;
		SPort.Active := False;
		MotivoCancelacion := OK;
	except
		on e:Exception do begin
			MsgBoxErr(MSG_ERROR_IMPRIMIR_ETIQUETA,e.Message,caption,MB_ICONERROR);
			Result:=False;
            SPort.Active := False;
            MotivoCancelacion := PeaTypes.ERROR;
		end;
	end;
end;

procedure TFormImprimirConvenio.PrintText(Text: AnsiString;
  Alineacion: TTipoAlign; Modo: TModo);
resourcestring
	CONST_ERR_ALINEACION = 'Tipo de Alineaci�n Inexistente';
	CONST_ERR_TIPO_MODO = 'Tipo de Modo Inexistente';
begin
	if not Sport.Active then Exit;

	SPort.SendString(INIT);

	case Alineacion of
		taIzquierda: SPort.SendString(ALIN + '0');
		taDerecha: SPort.SendString(ALIN + '2');
		taCentro : SPort.SendString(ALIN + '1');
	else begin
			raise ERangeError.CreateFmt(CONST_ERR_ALINEACION,[]);
			exit;
		end;
	end;

	case Modo of
		tmNormal: SPort.SendString(MODE + #0 + CR);
		tmNegrita: SPort.SendString(MODE + #8 + CR);
		tmDoble: SPort.SendString(MODE + #48 + CR);
		tmDobleNegrito: SPort.SendString(MODE + #55 + CR);
	else begin
			raise ERangeError.CreateFmt(CONST_ERR_TIPO_MODO,[]);
			exit;
		end;
	end;

	SPort.SendString(Text + CRLF);
end;

{******************************** Function Header ******************************
Function Name: TFormImprimirConvenio.ImprimirConvenioPersonaJuridica
Author       : cchiappero
Date Created : 26/07/2004
Description  : Sobrecarga la otra para aceptar par�metros compatibles con las
				clases de modificaci�n de convenio
Parameters   : NumeroConvenio: String; DatoTitular: TDatosPersonales; PrimerMedioComunicacion, SegundoMedioComunicacion: TTipoMedioComunicacion; EMail: String; DomicilioTitular, DomicilioFacturacion: TDatosDomicilio; DatosMedioPago: TDatosMediosPago; DatosRepLegal1, DatosRepLegal2, DatosRepLegal3: TDatosPersonales; EnviaDocumentacionxMail: Boolean; var MotivoCancelacion: TMotivoCancelacion; CantCopias: Integer; Vehiculo: array of TCuentaVehiculo; MostrarInterface: Boolean
Return Value : Boolean

Revision: 1
    Author: nefernandez
    Date: 08/11/2007
    Description: SS 620: Se imprime la FechaAltaCuenta en lugar de la FechaAltaTag

Revision: 2
    Author: pdominguez
    Date: 25/03/2009
    Description: SS 733
        - Se adapt� para la impresi�n al Reporte ppReporteConvenioCompleto.

Revision: 3
    Author : pdominguez
    Date   : 07/08/2009
    Description : SS 733
        - Se pasa la impresi�n del Lugar y Fecha, C�digo de Barras de Nro. de
        Convenio, Nro de Convenio y Nro Anexo, al objeto ppReporteConvenioCompleto
        en vez de cargarlo de la plantilla en el objeto CampoRich.

Revision: 12
    Author : pdominguez
    Date   : 07/06/2010
    Description : SS 895 - Error Impresi�n Convenio CAC
        - Se cambia el LoadFromFile del objeto CampoRich, por la carga a trav�s
        de la funci�n CargaPlantillaRTF.
*******************************************************************************}
function TFormImprimirConvenio.ImprimirConvenioPersonaJuridica(
  NumeroConvenio: String;  TipoImpresion: TTipoImpresion;
  DatoTitular: TDatosPersonales;
  PrimerMedioComunicacion,
  SegundoMedioComunicacion: TTipoMedioComunicacion; EMail: String;
  DomicilioTitular, DomicilioFacturacion: TDatosDomicilio;
  DatosMedioPago: TDatosMediosPago; DatosRepLegal1, DatosRepLegal2,
  DatosRepLegal3: TDatosPersonales; EnviaDocumentacionxMail: Boolean;
  FechaAlta : TDateTime;
  var MotivoCancelacion: TMotivoCancelacion; CantCopias: Integer;
  Vehiculo: TRegCuentaABM; Plantilla:String): Boolean;
var
    PathArchivo:String;
    auxDescripTipo,auxDescripCategoria,auxCategoria:String;
    i, NumeroAnexo,Departamento:Integer;
    aux_Vehiculo:array of TCuentaABM;
	accion: String;
resourcestring
	CONST_ACCION_Agrega = 'Agrega';
	CONST_ACCION_Elimina = 'Elimina';
	CONST_ACCION_Modifica = 'Modifica';
	CONST_ACCION_Robado = 'Robado';
	CONST_ACCION_BajaST = 'Baja S/T';
	CONST_ACCION_Vigente = 'Vigente';
	CONST_ACCION_Suspende = 'Suspende';
	CONST_ACCION_Activa = 'Activa';
	CONST_ACCION_Perdida = 'P�rdida';
	CONST_ACCION_ERROR  = 'ERROR';
	CONST_CONVENIO_PERSONERIA_JURIDICA = 'Convenio Personer�a Jur�dica';
	CONST_ERROR_IMPRESION = 'Error de Impresi�n';
	CONST_ERROR_IMPRESION_CONVENIO = 'Error al imprimir Convenio Persona Jur�dica';

begin
	FormularioPreImpreso := True;
    ppLblListaAmarillaAnexo.Visible := VerificaConvenioListaAmarilla(FDatosConvenio.CodigoConvenio);  //SS_660_NDR_20131112
    try
        pplblLugarFechaCompleto.Visible := True;
        pplblNumeroConvenioCompleto.Visible := True;
        ppbcNumeroConvenioCompleto.Visible := True;
        ppsvConvenioCompleto.Visible := True;

	if not EstaFuenteMarcaInstalada then begin
		Result := False;
		MotivoCancelacion := PeaTypes.ERROR;
		exit;
	end;

	try   //modif
		MotivoCancelacion:=PeaTypes.ERROR;
		ObtenerParametroGeneral(DMConnections.BaseCAC,Plantilla,PathArchivo);

		if not(FileExists(PathArchivo)) then begin
			MsgBox(format(MSG_ERROR_ARCHIVO_ABRIR,[PathArchivo]),MSG_CAPTION_IMPRESION_CONVENIOS,MB_ICONSTOP);
			result:=False;
			Exit;
		end;

		//CampoRich.LoadFromFile(PathArchivo);
		CampoRich.Clear;
        CampoRich.RichText := CargaPlantillaRTF(PathArchivo); // Rev. 12 (SS 895)


        //convenio
        //CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_CONVENIO,NumeroConvenio);

        // Rev.3 (SS 733)
        pplblLugarFechaCompleto.Caption := FormatDateTime(LBL_LUGAR_FECHA,NowBase(DMConnections.BaseCAC));
        ppbcNumeroConvenioCompleto.Data := ANSIReplaceStr(NumeroConvenio,'-','');
        if (not Assigned(FDatosConvenio)) or (FDatosConvenio.CodigoEstadoConvenio = ESTADO_CONVENIO_VIGENTE) then
            pplblNumeroConvenioCompleto.Caption := Format(LBL_CONVENIO_FORMATEADO,[NumeroConvenio])
        else
            pplblNumeroConvenioCompleto.Caption := Format(LBL_CONVENIO_BAJA_FORMATEADO,[NumeroConvenio]);
        // Fin Rev.3 (SS 733)

        //anexo
		NumeroAnexo := -1;
        if (TipoImpresion <> tiEstadoActual) and
            ((not Assigned(FDatosConvenio)) or (FDatosConvenio.CodigoEstadoConvenio = ESTADO_CONVENIO_VIGENTE)) then begin // Rev. 3 (SS 733)
			with spObtenerNumeroAnexo do begin
                Parameters.Refresh;
                Parameters.ParamByName('@NumeroConvenio').Value := NumeroConvenio;
                ExecProc;
                NumeroAnexo := Parameters.ParamByName('@RETURN_VALUE').Value;
                // Rev. 3 (SS 733)
                pplblAnexoConvenioCompleto.Visible := True;
                pplblAnexoConvenioCompleto.Caption := Format(pplblAnexoConvenioCompleto.Caption,[NumeroAnexo]);
                // Fin Rev. 3 (SS 733)
            end;
		end;
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_NUMERO_ANEXO, iif(NumeroAnexo > -1, ANEXO_NRO + IntToStr(NumeroAnexo), ''));
        //Asgino la prop.
        NroAnexoEmitido := NumeroAnexo;

//        ReplaceCodeBar(TAG_CODIGO_BARRA, NumeroConvenio, CampoRich);

        // Deshabilitado en Rev.3 (SS 733)
{		if Plantilla = PLANTILLA_BAJA_CONVENIO_JURIDICA then begin
			ReplaceCodeBar(TAG_CODIGO_BARRA, ANSIReplaceStr(NumeroConvenio, '-', ''), CampoRich);
			CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_DIA,FormatDateTime('dd',NowBase(DMConnections.BaseCAC)));
			CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_MES,FormatDateTime('mmmm',NowBase(DMConnections.BaseCAC)));
			CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_ANIO,FormatDateTime('yyyy',NowBase(DMConnections.BaseCAC)));
			CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_HORA,FormatDateTime('hh:nn',NowBase(DMConnections.BaseCAC)));
		end
		else begin
			ReplaceCodeBar(TAG_CODIGO_BARRA, NumeroConvenio, CampoRich);
			CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_DIA, FormatDateTime('dd',NowBase(DMConnections.BaseCAC))); //FormatDateTime('dd',FechaAlta));
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_MES, FormatDateTime('mmmm',NowBase(DMConnections.BaseCAC))); //FormatDateTime('mmmm',FechaAlta));
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_ANIO, FormatDateTime('yyyy',NowBase(DMConnections.BaseCAC))); //FormatDateTime('yyyy',FechaAlta));
        end;}

        //EMPRESA
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_RAZON_SOCIAL,DatoTitular.RazonSocial);
        DatoTitular.NumeroDocumento:=trim(DatoTitular.NumeroDocumento);
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_RUT,DatoTitular.NumeroDocumento);

		//medios de comunicacion

        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TELEFONO_ETIQUETA1,ObtenerDescripcionMedioComunicacion(PrimerMedioComunicacion.CodigoTipoMedioContacto));
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TELEFONO_AREA1,StrRight('0'+trim(inttostr(PrimerMedioComunicacion.CodigoArea)),2));
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TELEFONO_NUMERO1,PrimerMedioComunicacion.valor);

        if trim(SegundoMedioComunicacion.Valor)<>'' then begin
			CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TELEFONO_ETIQUETA2,ObtenerDescripcionMedioComunicacion(SegundoMedioComunicacion.CodigoTipoMedioContacto));
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TELEFONO_AREA2,StrRight('0'+trim(inttostr(SegundoMedioComunicacion.CodigoArea)),2));
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TELEFONO_NUMERO2,SegundoMedioComunicacion.Valor);
        end else begin
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TELEFONO_ETIQUETA2,'');
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TELEFONO_AREA2,'');
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TELEFONO_NUMERO2,'');
        end;

		if trim(EMail)<>'' then begin
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_EMAIL,EMail);
        end else begin
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_EMAIL,'');
        end;

        if DomicilioFacturacion.NumeroCalle>0 then begin
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_CALLE_PAR,Trim(uppercase(DomicilioFacturacion.Descripcion)));
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_NUMERO_PAR,uppercase(DomicilioFacturacion.NumeroCalleSTR));
            //Revision 10
            //CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_DETALLE_PAR,uppercase(DomicilioFacturacion.Detalle));
            if TryStrToInt(DomicilioFacturacion.Dpto,Departamento) then begin
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_DETALLE_PAR,UpperCase(TrimRight(DomicilioFacturacion.Detalle) + ' ' + '    DEPTO: ' + IntToStr(Departamento)));
            end else begin
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_DETALLE_PAR,UpperCase(TrimRight(DomicilioFacturacion.Detalle) + '    ' + TrimRight(DomicilioFacturacion.Dpto)));
            end;
            //End Revision 10
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_COMUNA_PAR,upperCase(BuscarDescripcionComuna(DMConnections.BaseCAC,DomicilioFacturacion.CodigoPais,DomicilioFacturacion.CodigoRegion,DomicilioFacturacion.CodigoComuna)));
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_REGION_PAR,upperCase(BuscarDescripcionRegion(DMConnections.BaseCAC,DomicilioFacturacion.CodigoPais,DomicilioFacturacion.CodigoRegion)));
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_CP_PAR,uppercase(DomicilioFacturacion.CodigoPostal));
        end else begin
			CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_CALLE_PAR,Trim(uppercase(DomicilioTitular.Descripcion)));
			CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_NUMERO_PAR,uppercase(DomicilioTitular.NumeroCalleSTR));
            //Revision 10
            //CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_DETALLE_PAR,uppercase(DomicilioTitular.Detalle));
            if TryStrToInt(DomicilioTitular.Dpto,Departamento) then begin
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_DETALLE_PAR,UpperCase(TrimRight(DomicilioTitular.Detalle) + ' ' + '    DEPTO: ' + IntToStr(Departamento)));
            end else begin
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_DETALLE_PAR,UpperCase(TrimRight(DomicilioTitular.Detalle) + '    ' + TrimRight(DomicilioTitular.Dpto)));
            end;
            //End Revision 10
          	CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_COMUNA_PAR,upperCase(BuscarDescripcionComuna(DMConnections.BaseCAC,DomicilioTitular.CodigoPais,DomicilioTitular.CodigoRegion,DomicilioTitular.CodigoComuna)));
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_REGION_PAR,uppercase(BuscarDescripcionRegion(DMConnections.BaseCAC,DomicilioTitular.CodigoPais,DomicilioTitular.CodigoRegion)));
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_CP_PAR,uppercase(DomicilioTitular.CodigoPostal));
        end;

		//REP LEGALES
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_NOMBRE1,DatosRepLegal1.Nombre);
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_APE_PAT1,DatosRepLegal1.Apellido);
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_APE_MAT1,DatosRepLegal1.ApellidoMaterno);
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_RUT1,DatosRepLegal1.NumeroDocumento);

        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_NOMBRE2,DatosRepLegal2.Nombre);
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_APE_PAT2,DatosRepLegal2.Apellido);
		CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_APE_MAT2,DatosRepLegal2.ApellidoMaterno);
		CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_RUT2,DatosRepLegal2.NumeroDocumento);

        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_NOMBRE3,DatosRepLegal3.Nombre);
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_APE_PAT3,DatosRepLegal3.Apellido);
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_APE_MAT3,DatosRepLegal3.ApellidoMaterno);
		CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_JUR_RUT3,DatosRepLegal3.NumeroDocumento);

        //CampoRich.RichText:=ReplaceTag(CampoRich.RichText, TAG_EMISION_ELEC, iif(EnviaDocumentacionxMail,MSG_SI,MSG_NO));

        //vehiculos
        for i:=0 to TAG_JUR_CANTIDAD_MAX_VEHICULOS_HOJA -1 do begin
            if i<Length(Vehiculo) then begin
                ObtenerDescripTipoVehiculo(DMConnections.BaseCAC,Vehiculo[i].Cuenta.Vehiculo.CodigoTipo,auxDescripTipo,auxDescripCategoria,auxCategoria);

				case Vehiculo[i].TipoMovimiento of
					ttmAlta       : Accion := CONST_ACCION_Agrega;
					ttmBaja       : Accion := CONST_ACCION_Elimina;
					ttmModif      : Accion := CONST_ACCION_Modifica;
					ttmModifTag   : Accion := CONST_ACCION_Modifica;
					ttmRobado     : Accion := CONST_ACCION_Robado;
					ttmBajaSinTag : Accion := CONST_ACCION_BajaST;
					ttmVigente    : Accion := CONST_ACCION_Vigente;
					ttmSuspendido : Accion := CONST_ACCION_Suspende;
					ttmActivado   : Accion := CONST_ACCION_Activa;
					ttmTagPerdido : Accion := CONST_ACCION_Perdida;
				else
					Accion := CONST_ACCION_ERROR;
				end;

				CampoRich.RichText:=ReplaceTag(CampoRich.RichText, TAG_CON_ACCION_VEHICULO + Inttostr(i+1), Accion );
				//CampoRich.RichText:=ReplaceTag(CampoRich.RichText, TAG_CON_ACCION_FECHA + Inttostr(i+1), FormatDateTime('dd/mm/yy hh:nn', Vehiculo[i].Cuenta.FechaModifCuenta));
				CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_PATENTE_VEHICULO+Inttostr(i+1),Trim(Vehiculo[i].Cuenta.Vehiculo.Patente) + ' - ' + Vehiculo[i].Cuenta.Vehiculo.DigitoPatente);
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TIPO_VEHICULO+Inttostr(i+1),FormatearTexto(auxDescripTipo,CANTIDAD_CARACTERES_TIPO));
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_CATE_VEHICULO+Inttostr(i+1),auxCategoria);
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_MARCA_VEHICULO+Inttostr(i+1),Vehiculo[i].Cuenta.Vehiculo.Marca);
				CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_MODELO_VEHICULO+Inttostr(i+1),iif(Trim(Vehiculo[i].Cuenta.Vehiculo.Modelo) = '', '', Trim(strLeft(Vehiculo[i].Cuenta.Vehiculo.Modelo, CANT_CARACTERES_MODELO))+'.'));
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_ANIO_VEHICULO+Inttostr(i+1),Vehiculo[i].Cuenta.Vehiculo.Anio);
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TAG_VEHICULO+Inttostr(i+1),SerialNumberToEtiqueta(inttostr(Vehiculo[i].Cuenta.ContactSerialNumber)));
                //Revision 1 - CampoRich.RichText:=ReplaceTag(CampoRich.RichText, TAG_CON_FECHA_ALTA + Inttostr(i+1) , FormatDateTime('dd/mm/yy hh:nn', Vehiculo[i].Cuenta.FechaAltaTag));
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText, TAG_CON_FECHA_ALTA + Inttostr(i+1) , FormatDateTime('dd/mm/yy hh:nn', Vehiculo[i].Cuenta.FechaCreacion));
				CampoRich.RichText:=ReplaceTag(CampoRich.RichText, TAG_CON_FECHA_VTO_GTIA + Inttostr(i+1) , FormatDateTime('dd/mm/yy hh:nn', Vehiculo[i].Cuenta.FechaVencimientoTag));

            end
            else begin
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText, TAG_CON_ACCION_VEHICULO + Inttostr(i+1), '' );
                //CampoRich.RichText:=ReplaceTag(CampoRich.RichText, TAG_CON_ACCION_FECHA + Inttostr(i+1), '');
				CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_PATENTE_VEHICULO+Inttostr(i+1),'');
				CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TIPO_VEHICULO+Inttostr(i+1),'');
				CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_CATE_VEHICULO+Inttostr(i+1),'');
				CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_MARCA_VEHICULO+Inttostr(i+1),'');
				CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_MODELO_VEHICULO+Inttostr(i+1),'');
				CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_ANIO_VEHICULO+Inttostr(i+1),'');
				CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TAG_VEHICULO+Inttostr(i+1),'');
				CampoRich.RichText:=ReplaceTag(CampoRich.RichText, TAG_CON_FECHA_ALTA + Inttostr(i+1), '');
				CampoRich.RichText:=ReplaceTag(CampoRich.RichText, TAG_CON_FECHA_VTO_GTIA + Inttostr(i+1) ,'' );
			end;
		end;

		CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_FIRMA_CONCESIONARA, iif(TipoImpresion <> tiEstadoActual, STR_FIRMA_CONCESIONARIA, ''));
		CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_FIRMA_CLIENTE, iif(TipoImpresion <> tiEstadoActual, STR_FIRMA_CLIENTE, ''));

//        CampoRich.RichText:=ReplaceTag(CampoRich.RichText, TAG_MENSAJE, ObtenerDescripcionMensaje(Vehiculo));
		CampoRich.RichText := PonerDescripcionMensajes(CampoRich.RichText, TipoImpresion, NumeroConvenio);

        CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_ADHERIDOPA, iif(FDatosConvenio.AdheridoPA ,'S�', 'No'));

        // SS 733
		RBInterface.Caption:=CONST_CONVENIO_PERSONERIA_JURIDICA;
        RBInterface.Report := ppReporteConvenioCompleto;

        cdsDatosConvenioCompleto.EmptyDataSet;
        cdsDatosConvenioCompleto.Append;
        cdsDatosConvenioCompletoMiRichEdit.AsString := CampoRich.RichText;
        cdsDatosConvenioCompleto.Post;

//        if Imprimir(tthConvenio, CantCopias, (TipoImpresion <> tiEstadoActual)) then begin
            if Length(Vehiculo)>TAG_JUR_CANTIDAD_MAX_VEHICULOS_HOJA then begin
                for i:=TAG_JUR_CANTIDAD_MAX_VEHICULOS_HOJA to Length(Vehiculo)-1 do begin
                    SetLength(aux_Vehiculo,Length(aux_Vehiculo)+1);
                    aux_Vehiculo[i-TAG_JUR_CANTIDAD_MAX_VEHICULOS_HOJA]:=vehiculo[i];
                end;

                try
                    ppHeaderBand4.PrintOnFirstPage := True; // SS 733
                    result:=ImprimirAnexoVehiculos(PERSONERIA_JURIDICA,NumeroConvenio,TipoImpresion,aux_Vehiculo,MotivoCancelacion,CantCopias);
                finally
                    ppHeaderBand4.PrintOnFirstPage := False; // SS 733
                end;
            end else begin
                try
                    ppHeaderBand4.PrintOnFirstPage := True; // SS 733
                    ppHeaderBand4.PrintOnLastPage := True; // SS 733
                    if Imprimir(tthConvenio, CantCopias, (TipoImpresion <> tiEstadoActual)) then begin
                        result:=True;
                        MotivoCancelacion:=OK;
            		end else begin
	    	        	result:=False;
            			MotivoCancelacion:=CANCELADO_USR;
		            end;
                finally
                    ppHeaderBand4.PrintOnFirstPage := False; // SS 733
                    ppHeaderBand4.PrintOnLastPage := False; // SS 733
                end;
			end;
{		end else begin
			result:=False;
			MotivoCancelacion:=CANCELADO_USR;
		end;}
        // Fin SS 733

	except
		on e: Exception do begin
			MsgBoxErr(CONST_ERROR_IMPRESION_CONVENIO, e.Message, CONST_ERROR_IMPRESION, MB_ICONERROR);
			result:=False;
        end;

    end;
    finally
        pplblLugarFechaCompleto.Visible := False;
        pplblNumeroConvenioCompleto.Visible := False;
        ppbcNumeroConvenioCompleto.Visible := False;
        pplblAnexoConvenioCompleto.Visible := False;
        ppsvConvenioCompleto.Visible := False;
    end;
end;

{******************************** Function Header ******************************
Revision: 1
    Author: nefernandez
    Date: 08/11/2007
    Description: SS 620: Se imprime la FechaAltaCuenta en lugar de la FechaAltaTag

Revision: 2
    Author: pdominguez
    Date: 25/03/2009
    Description: SS 733
        - Se adapt� para la impresi�n al Reporte ppReporteConvenioCompleto.

Revision: 3
    Author : pdominguez
    Date   : 07/08/2009
    Description : SS 733
        - Se pasa la impresi�n del Lugar y Fecha, C�digo de Barras de Nro. de
        Convenio, Nro de Convenio y Nro Anexo, al objeto ppReporteConvenioCompleto
        en vez de cargarlo de la plantilla en el objeto CampoRich.

Revision: 12
    Author : pdominguez
    Date   : 07/06/2010
    Description : SS 895 - Error Impresi�n Convenio CAC
        - Se cambia el LoadFromFile del objeto CampoRich, por la carga a trav�s
        de la funci�n CargaPlantillaRTF.
********************************************************************************}
function TFormImprimirConvenio.ImprimirConvenioPersonaNatural(
  NumeroConvenio: String; TipoImpresion: TTipoImpresion;
  DatoTitular: TDatosPersonales;
  PrimerMedioComunicacion,
  SegundoMedioComunicacion: TTipoMedioComunicacion; EMail: String;
  DomicilioTitular, DomicilioFacturacion: TDatosDomicilio;
  DatosMedioPago: TDatosMediosPago; Vehiculo: TRegCuentaABM;
  EnviaDocumentacionxMail: Boolean;
  FechaAlta : TDateTime;
  var MotivoCancelacion: TMotivoCancelacion; CantCopias: Integer;
  Plantilla:String): Boolean;
resourcestring
	CONST_ACCION_Agrega = 'Agrega';
	CONST_ACCION_Elimina = 'Elimina';
	CONST_ACCION_Modifica = 'Modifica';
	CONST_ACCION_Robado = 'Robado';
	CONST_ACCION_BajaST = 'Baja S/T';
	CONST_ACCION_Vigente = 'Vigente';
	CONST_ACCION_Suspende = 'Suspende';
	CONST_ACCION_Activa = 'Activa';
	CONST_ACCION_Perdida = 'P�rdida';
	CONST_ACCION_ERROR  = 'ERROR';
	CONST_CONVENIO_PERSONERIA_NATURAL = 'Convenio Personer�a Natural';
	CONST_ERROR_IMPRESION = 'Error de Impresi�n';
	CONST_ERROR_IMPRESION_CONVENIO = 'Error al imprimir Convenio Persona Natural';
var
	PathArchivo:String;
	i, NumeroAnexo,Departamento:Integer; //Revision 10
	aux_Vehiculo:array of TCuentaABM;
	auxDescripTipo, auxDescripCategoria, auxCategoria:String;
	accion: String;
begin
    FormularioPreImpreso := True;
    ppLblListaAmarillaAnexo.Visible := VerificaConvenioListaAmarilla(FDatosConvenio.CodigoConvenio);  //SS_660_NDR_20131112

    try
        pplblLugarFechaCompleto.Visible := True;
        pplblNumeroConvenioCompleto.Visible := True;
        ppbcNumeroConvenioCompleto.Visible := True;
        ppsvConvenioCompleto.Visible := True;

    if not EstaFuenteMarcaInstalada then begin
        Result := False;
        MotivoCancelacion := PeaTypes.ERROR;
		exit;
	end;

    try
        MotivoCancelacion:=PeaTypes.ERROR;
        ObtenerParametroGeneral(DMConnections.BaseCAC,Plantilla,PathArchivo);

        if not(FileExists(PathArchivo)) then begin
            MsgBox(format(MSG_ERROR_ARCHIVO_ABRIR,[PathArchivo]),MSG_CAPTION_IMPRESION_CONVENIOS,MB_ICONSTOP);
			result:=False;
			Exit;
        end;

        // Rev. 12 (SS 895)
		//Buff := FileToString(PathArchivo);
        //CampoRich.RichText := copy(buff,1,length(buff));

        CampoRich.RichText := CargaPlantillaRTF(PathArchivo);
        //CampoRich.LoadFromFile(PathArchivo);
        // Fin Rev. 12 (SS 895)

        // Rev. 3 (SS 733)
        //convenio
//		CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_CONVENIO,NumeroConvenio);
        pplblLugarFechaCompleto.Caption := FormatDateTime(LBL_LUGAR_FECHA,NowBase(DMConnections.BaseCAC));
        ppbcNumeroConvenioCompleto.Data := ANSIReplaceStr(NumeroConvenio,'-','');

        if (not Assigned(FDatosConvenio)) or (FDatosConvenio.CodigoEstadoConvenio = ESTADO_CONVENIO_VIGENTE) then
            pplblNumeroConvenioCompleto.Caption := Format(LBL_CONVENIO_FORMATEADO,[NumeroConvenio])
        else pplblNumeroConvenioCompleto.Caption := Format(LBL_CONVENIO_BAJA_FORMATEADO,[NumeroConvenio]);
        // Fin Rev. 3 (SS 733)

		//anexo
		NumeroAnexo := -1;
        if (TipoImpresion <> tiEstadoActual) and
            ((not Assigned(FDatosConvenio)) or (FDatosConvenio.CodigoEstadoConvenio = ESTADO_CONVENIO_VIGENTE)) then begin  // Rev. 3 (SS 733)
            with spObtenerNumeroAnexo do begin
                Parameters.Refresh;
                Parameters.ParamByName('@NumeroConvenio').Value := NumeroConvenio;
                ExecProc;
				NumeroAnexo := Parameters.ParamByName('@RETURN_VALUE').Value;
                // Rev. 3 (SS 733)
                pplblAnexoConvenioCompleto.Visible := True;
                pplblAnexoConvenioCompleto.Caption :=
                    Format(pplblAnexoConvenioCompleto.Caption,[NumeroAnexo]);
                // Fin Rev. 3 (SS 733)
            end;
        end;
//        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_NUMERO_ANEXO, iif(NumeroAnexo > -1, ANEXO_NRO + IntToStr(NumeroAnexo), ''));
        //Asgino la prop.
        NroAnexoEmitido := NumeroAnexo;

        // Deshabilitado en Rev. 3 (SS 733)
//		ReplaceCodeBar(TAG_CODIGO_BARRA, NumeroConvenio, CampoRich);
{		if Plantilla = PLANTILLA_BAJA_CONVENIO_NATURAL then begin
			ReplaceCodeBar(TAG_CODIGO_BARRA, ANSIReplaceStr(NumeroConvenio, '-', ''), CampoRich);
			CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_DIA,FormatDateTime('dd',NowBase(DMConnections.BaseCAC)));
			CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_MES,FormatDateTime('mmmm',NowBase(DMConnections.BaseCAC)));
			CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_ANIO,FormatDateTime('yyyy',NowBase(DMConnections.BaseCAC)));
			CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_HORA,FormatDateTime('hh:nn',NowBase(DMConnections.BaseCAC)));
		end
		else begin
			ReplaceCodeBar(TAG_CODIGO_BARRA, NumeroConvenio, CampoRich);
			CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_DIA,FormatDateTime('dd',NowBase(DMConnections.BaseCAC))); //FormatDateTime('dd',FechaAlta));
			CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_MES,FormatDateTime('mmmm',NowBase(DMConnections.BaseCAC))); //FormatDateTime('mmmm',FechaAlta));
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_ANIO,FormatDateTime('yyyy',NowBase(DMConnections.BaseCAC))); //FormatDateTime('yyyy',FechaAlta));
        end;}

        //datos der la persona
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_NOMBRE,DatoTitular.Nombre);
		CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_APELLIDO_PATERNO,DatoTitular.Apellido);
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_APELLIDO_MATERNO,DatoTitular.ApellidoMaterno);
		DatoTitular.NumeroDocumento:=trim(DatoTitular.NumeroDocumento);
		CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_RUT,DatoTitular.NumeroDocumento);

        if (DatoTitular.FechaNacimiento=nullDate) then
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_NACIMIENTO,'')
        else
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_NACIMIENTO,FormatDateTime('dd/mm/yyyy',DatoTitular.FechaNacimiento));
		CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_SEXO,iif(DatoTitular.Sexo=SEXO_MASCULINO,UpperCase(SEXO_MASCULINO_DESCRIP),UpperCase(SEXO_FEMENINO_DESCRIP)));

        //medios de comunicacion
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TELEFONO_ETIQUETA1,ObtenerDescripcionMedioComunicacion(PrimerMedioComunicacion.CodigoTipoMedioContacto));
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TELEFONO_AREA1,StrRight('0'+trim(inttostr(PrimerMedioComunicacion.CodigoArea)),2));
        CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TELEFONO_NUMERO1,PrimerMedioComunicacion.Valor);

		if SegundoMedioComunicacion.CodigoTipoMedioContacto > 0 then begin
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TELEFONO_ETIQUETA2,ObtenerDescripcionMedioComunicacion(SegundoMedioComunicacion.CodigoTipoMedioContacto));
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TELEFONO_AREA2,StrRight('0'+trim(inttostr(SegundoMedioComunicacion.CodigoArea)),2));
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TELEFONO_NUMERO2,SegundoMedioComunicacion.Valor);
        end else begin
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TELEFONO_ETIQUETA2,'');
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TELEFONO_AREA2,'');
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TELEFONO_NUMERO2,'');
		end;

        if trim(EMail)<>'' then begin
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_EMAIL,EMail);
        end else begin
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_EMAIL,'');
        end;


		if DomicilioFacturacion.NumeroCalle<=0 then begin// no tiene domicilio alternativo
			CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_CALLE_COM,Trim(UpperCase(DomicilioTitular.Descripcion)));
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_NUMERO_COM,UpperCase(DomicilioTitular.NumeroCalleSTR));
            //Revision 10
            //CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_DETALLE_COM,UpperCase(DomicilioTitular.Detalle));
            if TryStrToInt(DomicilioTitular.Dpto,Departamento) then begin
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_DETALLE_COM,UpperCase(TrimRight(DomicilioTitular.Detalle) + ' ' + '    DEPTO: ' + IntToStr(Departamento)));
            end else begin
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_DETALLE_COM,UpperCase(TrimRight(DomicilioTitular.Detalle) + '    ' + TrimRight(DomicilioTitular.Dpto)));
            end;
            //End Revision 10
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_COMUNA_COM,UpperCase(BuscarDescripcionComuna(DMConnections.BaseCAC,DomicilioTitular.CodigoPais,DomicilioTitular.CodigoRegion,DomicilioTitular.CodigoComuna)));
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_REGION_COM,UpperCase(BuscarDescripcionRegion(DMConnections.BaseCAC,DomicilioTitular.CodigoPais,DomicilioTitular.CodigoRegion)));
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_CP_COM,UpperCase(DomicilioTitular.CodigoPostal));
		end else begin
            //domicilio facturacion
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_CALLE_COM,Trim(UpperCase(DomicilioFacturacion.Descripcion)));
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_NUMERO_COM,UpperCase(DomicilioFacturacion.NumeroCalleSTR));
            //Revision 10
            //CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_DETALLE_COM,UpperCase(DomicilioFacturacion.Detalle));
            if TryStrToInt(DomicilioFacturacion.Dpto,Departamento) then begin
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_DETALLE_COM,UpperCase(TrimRight(DomicilioFacturacion.Detalle) + ' ' + '    DEPTO: ' + IntToStr(Departamento)));
            end else begin
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_DETALLE_COM,UpperCase(TrimRight(DomicilioFacturacion.Detalle) + '    ' + TrimRight(DomicilioFacturacion.Dpto)));
            end;
            //End Revision 10
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_COMUNA_COM,UpperCase(BuscarDescripcionComuna(DMConnections.BaseCAC,DomicilioFacturacion.CodigoPais,DomicilioFacturacion.CodigoRegion,DomicilioFacturacion.CodigoComuna)));
            CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_REGION_COM,UpperCase(BuscarDescripcionRegion(DMConnections.BaseCAC,DomicilioFacturacion.CodigoPais,DomicilioFacturacion.CodigoRegion)));
			CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_CP_COM,UpperCase(DomicilioFacturacion.CodigoPostal));
		end;


		//vehiculos
		for i:=0 to TAG_CON_CANTIDAD_MAX_VEHICULOS_HOJA-1 do begin
			if i<Length(Vehiculo) then begin
				ObtenerDescripTipoVehiculo(DMConnections.BaseCAC,Vehiculo[i].Cuenta.Vehiculo.CodigoTipo,auxDescripTipo,auxDescripCategoria,auxCategoria);

				case Vehiculo[i].TipoMovimiento of
					ttmAlta       : Accion := CONST_ACCION_Agrega;
					ttmBaja       : Accion := CONST_ACCION_Elimina;
					ttmModif      : Accion := CONST_ACCION_Modifica;
					ttmModifTag   : Accion := CONST_ACCION_Modifica;
					ttmRobado     : Accion := CONST_ACCION_Robado;
					ttmBajaSinTag : Accion := CONST_ACCION_BajaST;
					ttmVigente    : Accion := CONST_ACCION_Vigente;
					ttmSuspendido : Accion := CONST_ACCION_Suspende;
					ttmActivado   : Accion := CONST_ACCION_Activa;
					ttmTagPerdido : Accion := CONST_ACCION_Perdida;
				else
					Accion := CONST_ACCION_ERROR;
				end;

				CampoRich.RichText:=ReplaceTag(CampoRich.RichText, TAG_CON_ACCION_VEHICULO + Inttostr(i+1), Accion );
                //CampoRich.RichText:=ReplaceTag(CampoRich.RichText, TAG_CON_ACCION_FECHA + Inttostr(i+1), FormatDateTime('dd/mm/yy hh:nn', Vehiculo[i].Cuenta.FechaModifCuenta));
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_PATENTE_VEHICULO+Inttostr(i+1),Trim(Vehiculo[i].Cuenta.Vehiculo.Patente) + ' - ' + Vehiculo[i].Cuenta.Vehiculo.DigitoPatente);
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TIPO_VEHICULO+Inttostr(i+1),FormatearTexto(auxDescripTipo,CANTIDAD_CARACTERES_TIPO));
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_CATE_VEHICULO+Inttostr(i+1),auxCategoria);
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_MARCA_VEHICULO+Inttostr(i+1),Vehiculo[i].Cuenta.Vehiculo.Marca);
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_MODELO_VEHICULO+Inttostr(i+1),iif(Trim(Vehiculo[i].Cuenta.Vehiculo.Modelo) = '', '', Trim(strLeft(Vehiculo[i].Cuenta.Vehiculo.Modelo, CANT_CARACTERES_MODELO))+'.'));
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_ANIO_VEHICULO+Inttostr(i+1),Vehiculo[i].Cuenta.Vehiculo.Anio);
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TAG_VEHICULO+Inttostr(i+1),SerialNumberToEtiqueta(inttostr(Vehiculo[i].Cuenta.ContactSerialNumber)));
                //Revision 1 - CampoRich.RichText:=ReplaceTag(CampoRich.RichText, TAG_CON_FECHA_ALTA + Inttostr(i+1) , FormatDateTime('dd/mm/yy hh:nn', Vehiculo[i].Cuenta.FechaAltaTag));
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText, TAG_CON_FECHA_ALTA + Inttostr(i+1) , FormatDateTime('dd/mm/yy hh:nn', Vehiculo[i].Cuenta.FechaCreacion));
				CampoRich.RichText:=ReplaceTag(CampoRich.RichText, TAG_CON_FECHA_VTO_GTIA + Inttostr(i+1) , FormatDateTime('dd/mm/yy hh:nn', Vehiculo[i].Cuenta.FechaVencimientoTag));
            end
            else begin
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText, TAG_CON_ACCION_VEHICULO + Inttostr(i+1), '');
                //CampoRich.RichText:=ReplaceTag(CampoRich.RichText, TAG_CON_ACCION_FECHA + Inttostr(i+1), '');
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_PATENTE_VEHICULO+Inttostr(i+1), '');
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TIPO_VEHICULO+Inttostr(i+1), '');
				CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_CATE_VEHICULO+Inttostr(i+1), '');
                CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_MARCA_VEHICULO+Inttostr(i+1),'');
				CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_MODELO_VEHICULO+Inttostr(i+1),'');
				CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_ANIO_VEHICULO+Inttostr(i+1),'');
				CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_CON_TAG_VEHICULO+Inttostr(i+1),'');
				CampoRich.RichText:=ReplaceTag(CampoRich.RichText, TAG_CON_FECHA_ALTA + Inttostr(i+1) , '');
				CampoRich.RichText:=ReplaceTag(CampoRich.RichText, TAG_CON_FECHA_VTO_GTIA + Inttostr(i+1) , '');
			end;
		end;

		CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_FIRMA_CONCESIONARA, iif(TipoImpresion <> tiEstadoActual, STR_FIRMA_CONCESIONARIA, ''));
		CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_FIRMA_CLIENTE, iif(TipoImpresion <> tiEstadoActual, STR_FIRMA_CLIENTE, ''));

		//CampoRich.RichText:=ReplaceTag(CampoRich.RichText,TAG_EMISION_ELEC,iif(EnviaDocumentacionxMail,MSG_SI,MSG_NO));
		CampoRich.RichText := PonerDescripcionMensajes(CampoRich.RichText, TipoImpresion, NumeroConvenio);

        CampoRich.RichText := ReplaceTag(CampoRich.RichText, TAG_ADHERIDOPA, iif(FDatosConvenio.AdheridoPA ,'S�', 'No'));

        // SS 733
		RBInterface.Caption:=CONST_CONVENIO_PERSONERIA_NATURAL;
        RBInterface.Report := ppReporteConvenioCompleto;

        cdsDatosConvenioCompleto.EmptyDataSet;
        cdsDatosConvenioCompleto.Append;
        cdsDatosConvenioCompletoMiRichEdit.AsString := CampoRich.RichText;
        cdsDatosConvenioCompleto.Post;


//        if Imprimir(tthConvenio, CantCopias, (TipoImpresion <> tiEstadoActual)) then begin
            if Length(Vehiculo)>TAG_CON_CANTIDAD_MAX_VEHICULOS_HOJA then begin
                for i:=TAG_CON_CANTIDAD_MAX_VEHICULOS_HOJA to Length(Vehiculo)-1 do begin
                    SetLength(aux_Vehiculo,Length(aux_Vehiculo)+1);
                    aux_Vehiculo[i-TAG_CON_CANTIDAD_MAX_VEHICULOS_HOJA] := vehiculo[i];
                end;
                try
                    ppHeaderBand4.PrintOnFirstPage := True; // SS 733
                    Result := ImprimirAnexoVehiculos(PERSONERIA_FISICA,NumeroConvenio,TipoImpresion,aux_Vehiculo,MotivoCancelacion,CantCopias);
                finally
                    ppHeaderBand4.PrintOnFirstPage := False; // SS 733
                end;
            end else begin
                try
                    ppHeaderBand4.PrintOnFirstPage := True; // SS 733
                    ppHeaderBand4.PrintOnLastPage := True; // SS 733
                    if Imprimir(tthConvenio, CantCopias, (TipoImpresion <> tiEstadoActual)) then begin
                        result:=True;
                        MotivoCancelacion:=OK;
                    end else begin
                        result:=False;
                        MotivoCancelacion:=CANCELADO_USR;
                    end;
                finally
                    ppHeaderBand4.PrintOnFirstPage := False; // SS 733
                    ppHeaderBand4.PrintOnLastPage := False; // SS 733
                end;
            end;
{        end else begin
            result:=False;
            MotivoCancelacion:=CANCELADO_USR;
        end;}
    // Fin SS 733
	except
		on e: Exception do begin
			MsgBoxErr(CONST_ERROR_IMPRESION_CONVENIO, e.Message, CONST_ERROR_IMPRESION, MB_ICONERROR);
			result:=False;
		end;
    end;
    finally
        pplblLugarFechaCompleto.Visible := False;
        pplblNumeroConvenioCompleto.Visible := False;
        ppbcNumeroConvenioCompleto.Visible := False;
        pplblAnexoConvenioCompleto.Visible := False;
        ppsvConvenioCompleto.Visible := False;
    end;
end;


function TFormImprimirConvenio.FormatearTexto(S: AnsiString;
  CantidadMaxima: integer): AnsiString;
begin
    if Length(S) >= CantidadMaxima then
		Result := StrLeft(S,CantidadMaxima)
	else
		Result := S;
end;

procedure TFormImprimirConvenio.ReplaceCodeBar(Tag,NuevoTexto: AnsiString;
	RichEdit : TppRichText);
var
	i: Integer;
resourcestring
	CONST_ERR_FALTA_LETRA = 'Falta el tipo de letra "%s", la cual es necesario para imprimir este el Documento.';
begin
	if Screen.Fonts.IndexOf(FONT_CODE_BAR) < 0 then begin
		raise Exception.Create(Format(CONST_ERR_FALTA_LETRA, [FONT_CODE_BAR]));
		exit;
	end;

	NuevoTexto := '*'+NuevoTexto+'*';
	NuevoTexto := StringReplace(NuevoTexto, '-', '',[rfReplaceAll]);

	i := RichEdit.FindText(Tag, 0, Length(RichEdit.RichText),[]);
	RichEdit.RichText := ReplaceTag(RichEdit.RichText, tag, NuevoTexto);
	with RichEdit do begin
		SelStart := i - 2;
		SelLength := length(NuevoTexto);
		SelAttributes.Name := FONT_CODE_BAR;
	end;
end;

function TFormImprimirConvenio.EstaFuenteMarcaInstalada: Boolean;
resourcestring
	CONST_ERR_FALTA_LETRA = 'Falta el tipo de letra "%s", la cual es necesario para imprimir este el Documento.';
begin
	Result := Screen.Fonts.IndexOf(FONT_MARCA) > 0;
	if not Result then raise Exception.Create(Format(CONST_ERR_FALTA_LETRA, [FONT_MARCA]));
end;
{-----------------------------------------------------------------------------
  Function Name: 
  Author:
  Date Created:  /  /
  Description:
  Parameters: Sender: TObject
  Return Value: N/A

  Revision : 1
    Date: 03/03/2009
    Author: pdominguez
    Description:  Ref (SRX01193) se agrega " WITH (NOLOCK) " para
      los bloqueos de tablas en la lectura

-----------------------------------------------------------------------------}
function TFormImprimirConvenio.ObtenerDescripcionMedioComunicacion(CodigoMedioComunicacion : integer) : string;
begin
	result := QueryGetValue(DMConnections.BaseCAC, 'select descripcion from tiposmediocontacto WITH (NOLOCK) where CodigoTipoMedioContacto = ' + inttostr(CodigoMedioComunicacion));
end;

procedure TFormImprimirConvenio.ReplaceMarcaCheck(Tag: AnsiString;
  RichEdit: TppRichText);
var
	i : Integer;
begin
    i := RichEdit.FindText(Tag, 0, Length(RichEdit.RichText),[]);
    RichEdit.RichText := ReplaceTag(RichEdit.RichText, tag, CARACTER_MARCA_CHECK );

    with RichEdit do begin
        SelStart := i - 2;
        SelLength := length(CARACTER_MARCA_CHECK);
        SelAttributes.Name := FONT_MARCA;
    end;

end;

procedure TFormImprimirConvenio.FormCreate(Sender: TObject);
begin
    Hoja := tthNinguno;
    FormularioPreImpreso := False;

    FNroAnexoEmitido := -1;
end;



function TFormImprimirConvenio.PonerDescripcionMensajes(
  const Texto: String; TipoImpresion: TTipoImpresion; NumeroConvenio: String): String;
var
    Posicion: Integer;
begin
    Result := Texto;

    Posicion := 1;
    with ObtenerMensajeConvenio do begin
        case TipoImpresion of
            tiAlta         : Parameters.ParamByName('@TipoImpresion').Value := 'A';
            tiModificacion : Parameters.ParamByName('@TipoImpresion').Value := 'M';
            tiPerdida      : Parameters.ParamByName('@TipoImpresion').Value := 'P';
            tiEstadoActual : Parameters.ParamByName('@TipoImpresion').Value := 'E';
        end;
        Open;

        while not Eof do begin
            Result := ReplaceTag(Result, TAG_MENSAJE + IntToStr(Posicion), FieldByName('Texto').AsString);
            Result := StringReplace(Result, TAG_NUMERO_CONVENIO, NumeroConvenio ,[rfReplaceAll, rfIgnoreCase]);
            if Posicion <= TAG_CON_CANTIDAD_MAX_MENSAJES then inc(Posicion);
            next;
        end;

        // Pongo espacios en los tags de mensajes que no uso
        while Posicion <= TAG_CON_CANTIDAD_MAX_MENSAJES do begin
            Result := ReplaceTag(Result, TAG_MENSAJE + IntToStr(Posicion), '');
            Inc(Posicion);
        end;

        Close;
    end;
end;

procedure TFormImprimirConvenio.ppFooterBand1BeforePrint(Sender: TObject);
begin
    if spObtenerReporteCaratulaConvenio.RecordCount = spObtenerReporteCaratulaConvenio.RecNo then begin
        pplnFirmaRevisor.Visible := True;
        pplblFirmaRevisor.Visible := True;
    end;

end;

procedure TFormImprimirConvenio.ppReporteConvenioCompletoBeforePrint(Sender: TObject);
    var
        numero:integer;
begin
    numero := 1;
end;

procedure TFormImprimirConvenio.ppReporteImpresionConvenioBeforePrint(Sender: TObject);
begin
  if FormularioPreImpreso then
    ppReporteImpresionConvenio.PrinterSetup.MarginTop := CM_TOP_MARGIN_FORM_PRE_IMPRESO * 10  //Paso de CM a MM
  else
		ppReporteImpresionConvenio.PrinterSetup.MarginTop := CM_TOP_MARGIN_FORM_HOJA_BLANCA * 10;

end;

procedure TFormImprimirConvenio.SetNroAnexoEmitido(const Value: Integer);
begin
  FNroAnexoEmitido := Value;
end;



{******************************** Function Header ******************************
Function Name: ImprimirEstadoConvenio
Author : pdominguez
Date Created : 06/08/2009
Parameters : None
Return Value : Boolean
Description : SS 733
    - Se implementa un nuevo proceso para imprimir el Estado de los Convenios.
*******************************************************************************}
function TFormImprimirConvenio.ImprimirEstadoConvenio: Boolean;
    Resourcestring
        LBL_FECHA_NACIMIENTO = 'dd/mm/yyyy';
        LBL_DOMICILIO_FACTURACION = '%s, N� %s';
    	TITLE_CONVENIO_PERSONERIA_JURIDICA = 'Estado Convenio Personer�a Jur�dica';
    	TITLE_CONVENIO_PERSONERIA_NATURAL = 'Estado Convenio Personer�a Natural';
    	TITLE_CONVENIO_ANEXO = 'Anexo Estado Convenio';
        ERROR_PARAMETRO_GENERAL = 'Se produjo un Error al obtener el Par�metro General "%s".';
        ERROR_EN_PROCESO = 'Se produjo un Error en la Impresi�n del Estado del Convenio.';
        REG_PRIMERA_PAG_FISICA      = 'CANT_REG_PRIMERA_PAG_FISICA';    // SS_1069_CQU_20120911
        REG_PRIMERA_PAG_JURIDICA    = 'CANT_REG_PRIMERA_PAG_JURIDICA';  // SS_1069_CQU_20120911
    var
        CantidadCopias,Departamento: Integer;
        DescError: AnsiString;
        CantRegPrimeraPagina : Integer;                                 // SS_1069_CQU_20120911


    {******************************** Function Header ******************************
    Function Name: PrepararEstadoConvenioNatural
    Author : pdominguez
    Date Created : 06/08/2009
    Parameters : var DescError: AnsiString
    Return Value : Boolean
    Description : SS 733
        - Preparamos la impresi�n del Estado de Convenio cunado la Personer�a es
        Natural (Persona).
    *******************************************************************************}
    function PrepararEstadoConvenioNatural(var DescError: AnsiString): Boolean;
    begin
        try
            Result := False;

            // Asignamos Lugar y Fecha, C�digo de Barras y Nro. de Convenio.
            pplblLugarFechaNatural.Caption := FormatDateTime(LBL_LUGAR_FECHA,NowBase(DMConnections.BaseCAC));
            ppBCNumeroConvenioNatural.Data := FDatosConvenio.NumeroConvenio;
            if FDatosConvenio.CodigoEstadoConvenio = ESTADO_CONVENIO_VIGENTE then
                pplblNumeroConvenioNatural.Caption := Format(LBL_CONVENIO_FORMATEADO,[FDatosConvenio.NumeroConvenioFormateado])
            else pplblNumeroConvenioNatural.Caption := Format(LBL_CONVENIO_BAJA_FORMATEADO,[FDatosConvenio.NumeroConvenioFormateado]);

            // Asignamos los datos del Cliente.
            pplblNombreNatural.Caption := FDatosConvenio.Cliente.Datos.Nombre;
            pplblApellidoPaternoNatural.Caption := FDatosConvenio.Cliente.Datos.Apellido;
            pplblApellidoMaternoNatural.Caption := FDatosConvenio.Cliente.Datos.ApellidoMaterno;
            pplblNumeroRUTNatural.Caption := FDatosConvenio.Cliente.Datos.NumeroDocumento;
            pplblFechaNacimientoNatural.Caption := FormatDateTime(LBL_FECHA_NACIMIENTO,FDatosConvenio.Cliente.Datos.FechaNacimiento);

            // Asignamos los datos de los Medios de Comunicaci�n.
            if FDatosConvenio.Cliente.Telefonos.TieneMedioPrincipal then begin
                pplblTelefono1Natural.Caption := ObtenerDescripcionMedioComunicacion(FDatosConvenio.Cliente.Telefonos.MedioPrincipal.CodigoTipoMedioContacto);
                pplblCodigoArea1Natural.Caption :=
                    StringOfChar('0', 2 - Length(IntToStr(FDatosConvenio.Cliente.Telefonos.MedioPrincipal.CodigoArea))) + IntToStr(FDatosConvenio.Cliente.Telefonos.MedioPrincipal.CodigoArea);
                pplblNumeroTelefono1Natural.Caption := FDatosConvenio.Cliente.Telefonos.MedioPrincipal.Valor;
            end else begin
                pplblTelefono1Natural.Caption := '';
                pplblCodigoArea1Natural.Caption := '';
                pplblNumeroTelefono1Natural.Caption := '';
            end;
             pplblListaAmarillaPersonaNatural.Visible := VerificaConvenioListaAmarilla(FDatosConvenio.CodigoConvenio);                                                          //SS_660_MVI_20130909
            if FDatosConvenio.Cliente.Telefonos.TieneMedioSecundario then begin
                pplblTelefono2Natural.Caption := ObtenerDescripcionMedioComunicacion(FDatosConvenio.Cliente.Telefonos.MedioSecundario.CodigoTipoMedioContacto);
                pplblCodigoArea2Natural.Caption :=
                    StringOfChar('0', 2 - Length(IntToStr(FDatosConvenio.Cliente.Telefonos.MedioSecundario.CodigoArea))) + IntToStr(FDatosConvenio.Cliente.Telefonos.MedioSecundario.CodigoArea);
                pplblNumeroTelefono2Natural.Caption := FDatosConvenio.Cliente.Telefonos.MedioSecundario.Valor;
            end else begin
                pplblTelefono2Natural.Caption := '';
                pplblCodigoArea2Natural.Caption := '';
                pplblNumeroTelefono2Natural.Caption := '';
            end;

            // Asignamos el e-Mail.
            if FDatosConvenio.Cliente.EMail.TieneMedioPrincipal then
                pplblEMailNatural.Caption := FDatosConvenio.Cliente.EMail.MedioPrincipal.Valor
            else if FDatosConvenio.Cliente.EMail.TieneMedioSecundario then
                pplblEMailNatural.Caption := FDatosConvenio.Cliente.EMail.MedioSecundario.Valor
            else pplblEMailNatural.Caption := '';

            pplblAdheridoPA.Caption := IfThen(FDatosConvenio.AdheridoPA,'S�','No'); //SS-1006-NDR-20120727

            // Asignamos los datos del Domicilio de Facturaci�n.
            pplblDomicilioEnvioFacturacionNatural.Caption :=
                UpperCase(
                    Format(
                        LBL_DOMICILIO_FACTURACION,
                            [Trim(FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.Descripcion),
                             FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.NumeroCalleSTR]));

            //Revision 10
            //pplblDetalleNatural.Caption := UpperCase(FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.Detalle);
            if TryStrToInt(FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.Dpto,Departamento) then begin
                pplblDetalleNatural.Caption := UpperCase(TrimRight(FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.Detalle) + ' ' + '    DEPTO: ' + IntToStr(Departamento));
            end else begin
                pplblDetalleNatural.Caption := UpperCase(TrimRight(FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.Detalle) +   '    '  + TrimRight(FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.Dpto));
            end;
            //End Revision 10
            pplblCodigoPostalNatural.Caption := FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.CodigoPostal;

            pplblComunaNatural.Caption :=
                UpperCase(
                    BuscarDescripcionComuna(
                        DMConnections.BaseCAC,
                        FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.CodigoPais,
                        FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.CodigoRegion,
                        FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.CodigoComuna));

            pplblRegionNatural.Caption :=
                UpperCase(
                    BuscarDescripcionRegion(
                        DMConnections.BaseCAC,
                        FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.CodigoPais,
                        FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.CodigoRegion));

            // Buscamos y asignamos los Mensajes a incluir para el Tipo de Impresi�n es "tiEstadoActual"
            if ObtenerMensajeConvenio.Active then ObtenerMensajeConvenio.Close;
            ObtenerMensajeConvenio.Parameters.ParamByName('@TipoImpresion').Value := 'E';
            ObtenerMensajeConvenio.Open;

            while not ObtenerMensajeConvenio.Eof do begin
                ppmDeclaracionesNatural.Lines.Add(
                    StringReplace(
                        ObtenerMensajeConvenio.FieldByName('Texto').AsString,
                        TAG_NUMERO_CONVENIO,
                        FDatosConvenio.NumeroConvenioFormateado,
                        [rfReplaceAll, rfIgnoreCase]));
                ObtenerMensajeConvenio.Next;
            end;

            pplblNumeroConvenioNaturalAnexo.Caption :=
                Format(pplblNumeroConvenioNaturalAnexo.Caption,[FDatosConvenio.NumeroConvenioFormateado]);

            RBInterface.Caption := TITLE_CONVENIO_PERSONERIA_NATURAL;
            RBInterface.Report  := ppRptEstadoConvenioNatural;

            Result := True;
        except
            On E:Exception do begin
                DescError := E.Message;
            end;
        end;
    end;

    {******************************** Function Header ******************************
    Function Name: PrepararEstadoConvenioJuridico
    Author : pdominguez
    Date Created : 06/08/2009
    Parameters : var DescError: AnsiString
    Return Value : Boolean
    Description : SS 733
        - Preparamos la impresi�n del Estado de Convenio cunado la Personer�a es
        Jur�dica (Empresa).
    *******************************************************************************}
    function PrepararEstadoConvenioJuridico(var DescError: AnsiString): Boolean;
        var
            TotalRepresentantes,
            Veces, Departamento: Integer;
    begin
        try
            Result := False;

            // Asignamos Lugar y Fecha, C�digo de Barras y Nro. de Convenio.
            pplblLugarFechaJuridico.Caption     := FormatDateTime(LBL_LUGAR_FECHA,NowBase(DMConnections.BaseCAC));
            ppbcNumeroConvenioJuridico.Data     := FDatosConvenio.NumeroConvenio;
            if FDatosConvenio.CodigoEstadoConvenio = ESTADO_CONVENIO_VIGENTE then
                pplblNumeroConvenioJuridico.Caption := Format(LBL_CONVENIO_FORMATEADO,[FDatosConvenio.NumeroConvenioFormateado])
            else pplblNumeroConvenioJuridico.Caption := Format(LBL_CONVENIO_BAJA_FORMATEADO,[FDatosConvenio.NumeroConvenioFormateado]);

            // Asignamos el Nombre del Cliente formateado
            pplblNombreJuridico.Caption :=
                ArmarNombrePersona(
                    PERSONERIA_JURIDICA,
                    ArmarNombrePersona(
                        PERSONERIA_FISICA,
                        FDatosConvenio.Cliente.Datos.Nombre,
                        FDatosConvenio.Cliente.Datos.Apellido,
                        FDatosConvenio.Cliente.Datos.ApellidoMaterno),
                    FDatosConvenio.Cliente.Datos.RazonSocial,'');
            pplblNumeroRUTJuridico.Caption := FDatosConvenio.Cliente.Datos.NumeroDocumento;

            //Obtenemos el total de Representantes Legales y asignamos sus datos.
            TotalRepresentantes := TPersonaJuridica(FDatosConvenio.Cliente).RepresentantesLegales.CantidadRepresentantes;

            pplblNombreR1Juridico.Caption  := '';
            pplblNumeroCI1Juridico.Caption := '';
            pplblNombreR2Juridico.Caption  := '';
            pplblNumeroCI2Juridico.Caption := '';
            pplblNombreR3Juridico.Caption  := '';
            pplblNumeroCI3Juridico.Caption := '';

            for Veces := 0 to TotalRepresentantes - 1 do begin
                case Veces of
                    0: begin
                        pplblNombreR1Juridico.Caption :=
                            ArmarNombrePersona(
                                PERSONERIA_FISICA,
                                TPersonaJuridica(FDatosConvenio.Cliente).RepresentantesLegales.Representantes[Veces].Datos.Nombre,
                                TPersonaJuridica(FDatosConvenio.Cliente).RepresentantesLegales.Representantes[Veces].Datos.Apellido,
                                TPersonaJuridica(FDatosConvenio.Cliente).RepresentantesLegales.Representantes[Veces].Datos.ApellidoMaterno);
                        pplblNumeroCI1Juridico.Caption :=
                            TPersonaJuridica(FDatosConvenio.Cliente).RepresentantesLegales.Representantes[Veces].Datos.NumeroDocumento;
                    end;
                    1: begin
                        pplblNombreR2Juridico.Caption :=
                            ArmarNombrePersona(
                                PERSONERIA_FISICA,
                                TPersonaJuridica(FDatosConvenio.Cliente).RepresentantesLegales.Representantes[Veces].Datos.Nombre,
                                TPersonaJuridica(FDatosConvenio.Cliente).RepresentantesLegales.Representantes[Veces].Datos.Apellido,
                                TPersonaJuridica(FDatosConvenio.Cliente).RepresentantesLegales.Representantes[Veces].Datos.ApellidoMaterno);
                        pplblNumeroCI2Juridico.Caption :=
                            TPersonaJuridica(FDatosConvenio.Cliente).RepresentantesLegales.Representantes[Veces].Datos.NumeroDocumento;
                    end;
                    2: begin
                        pplblNombreR3Juridico.Caption :=
                            ArmarNombrePersona(
                                PERSONERIA_FISICA,
                                TPersonaJuridica(FDatosConvenio.Cliente).RepresentantesLegales.Representantes[Veces].Datos.Nombre,
                                TPersonaJuridica(FDatosConvenio.Cliente).RepresentantesLegales.Representantes[Veces].Datos.Apellido,
                                TPersonaJuridica(FDatosConvenio.Cliente).RepresentantesLegales.Representantes[Veces].Datos.ApellidoMaterno);
                        pplblNumeroCI3Juridico.Caption :=
                            TPersonaJuridica(FDatosConvenio.Cliente).RepresentantesLegales.Representantes[Veces].Datos.NumeroDocumento;
                    end;
                end;
            end;

            // Asignamos los datos de los Medios de Comunicaci�n.
            if FDatosConvenio.Cliente.Telefonos.TieneMedioPrincipal then begin
                pplblTelefono1Juridico.Caption := ObtenerDescripcionMedioComunicacion(FDatosConvenio.Cliente.Telefonos.MedioPrincipal.CodigoTipoMedioContacto);
                pplblCodigoArea1Juridico.Caption :=
                    StringOfChar('0', 2 - Length(IntToStr(FDatosConvenio.Cliente.Telefonos.MedioPrincipal.CodigoArea))) + IntToStr(FDatosConvenio.Cliente.Telefonos.MedioPrincipal.CodigoArea);
                pplblNumeroTelefono1Juridico.Caption := FDatosConvenio.Cliente.Telefonos.MedioPrincipal.Valor;
            end else begin
                pplblTelefono1Juridico.Caption := '';
                pplblCodigoArea1Juridico.Caption := '';
                pplblNumeroTelefono1Juridico.Caption := '';
            end;

            if FDatosConvenio.Cliente.Telefonos.TieneMedioSecundario then begin
                pplblTelefono2Juridico.Caption := ObtenerDescripcionMedioComunicacion(FDatosConvenio.Cliente.Telefonos.MedioSecundario.CodigoTipoMedioContacto);
                pplblCodigoArea2Juridico.Caption :=
                    StringOfChar('0', 2 - Length(IntToStr(FDatosConvenio.Cliente.Telefonos.MedioSecundario.CodigoArea))) + IntToStr(FDatosConvenio.Cliente.Telefonos.MedioSecundario.CodigoArea);
                pplblNumeroTelefono2Juridico.Caption := FDatosConvenio.Cliente.Telefonos.MedioSecundario.Valor;
            end else begin
                pplblTelefono2Juridico.Caption := '';
                pplblCodigoArea2Juridico.Caption := '';
                pplblNumeroTelefono2Juridico.Caption := '';
            end;

            // Asignamos el e-Mail
            if FDatosConvenio.Cliente.EMail.TieneMedioPrincipal then
                pplblEMailJuridico.Caption := FDatosConvenio.Cliente.EMail.MedioPrincipal.Valor
            else if FDatosConvenio.Cliente.EMail.TieneMedioSecundario then
                pplblEMailJuridico.Caption := FDatosConvenio.Cliente.EMail.MedioSecundario.Valor
            else pplblEMailJuridico.Caption := '';

            pplblAdheridoPAJuridico.Caption := IfThen(FDatosConvenio.AdheridoPA,'S�','No'); //SS-1006-NDR-20120727            
            pplblListaAmarillaPersonaJuridica.Visible := VerificaConvenioListaAmarilla(FDatosConvenio.CodigoConvenio); //SS_660_MVI_20130909
            // Asignamos los datos del Domicilio de Facturaci�n.
            pplblDomicilioEnvioFacturacionJuridico.Caption :=
                UpperCase(
                    Format(
                        LBL_DOMICILIO_FACTURACION,
                        [Trim(FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.Descripcion),
                         FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.NumeroCalleSTR]));

            //Revision 10
            //pplblDetalleJuridico.Caption := UpperCase(FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.Detalle);
            if TryStrToInt(FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.Dpto,Departamento) then begin
                pplblDetalleJuridico.Caption := UpperCase(TrimRight(FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.Detalle) + ' ' + '    DEPTO: ' + IntToStr(Departamento)); //Revision 11
            end else begin
                pplblDetalleJuridico.Caption := UpperCase(TrimRight(FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.Detalle) +   '    '  + TrimRight(FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.Dpto)); //Revision 11
            end;
            //End Revision 10

            pplblCodigoPostalJuridico.Caption := FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.CodigoPostal;

            pplblComunaJuridico.Caption :=
                UpperCase(
                    BuscarDescripcionComuna(
                        DMConnections.BaseCAC,
                        FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.CodigoPais,
                        FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.CodigoRegion,
                        FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.CodigoComuna));

            pplblRegionJuridico.Caption :=
                UpperCase(
                    BuscarDescripcionRegion(
                        DMConnections.BaseCAC,
                        FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.CodigoPais,
                        FDatosConvenio.Cliente.Domicilios.DomicilioFacturacion.CodigoRegion));

            // Buscamos y asignamos los Mensajes a incluir para el Tipo de Impresi�n es "tiEstadoActual"
            if ObtenerMensajeConvenio.Active then ObtenerMensajeConvenio.Close;
            ObtenerMensajeConvenio.Parameters.ParamByName('@TipoImpresion').Value := 'E';
            ObtenerMensajeConvenio.Open;

            while not ObtenerMensajeConvenio.Eof do begin
                ppmDeclaracionesJuridico.Lines.Add(
                    StringReplace(
                        ObtenerMensajeConvenio.FieldByName('Texto').AsString,
                        TAG_NUMERO_CONVENIO,
                        FDatosConvenio.NumeroConvenioFormateado,
                        [rfReplaceAll, rfIgnoreCase]));
                ObtenerMensajeConvenio.Next;
            end;

            pplblNumeroConvenioJuridicoAnexo.Caption :=
                Format(pplblNumeroConvenioJuridicoAnexo.Caption,[FDatosConvenio.NumeroConvenioFormateado]);

            RBInterface.Caption := TITLE_CONVENIO_PERSONERIA_JURIDICA;
            RBInterface.Report  := ppRptEstadoConvenioJuridico;
            Result := True;
        except
            On E:Exception do begin
                DescError := E.Message;
            end;
        end;
    end;

    {******************************** Function Header ******************************
    Function Name: PrepararRBInterface
    Author : pdominguez
    Date Created : 06/08/2009
    Parameters : var DescError: AnsiString
    Return Value : Boolean
    Description : SS 733
        - Asigna al componente RBInterface los par�metrosd de impresi�n.
    *******************************************************************************}

    function PrepararRBInterface(var DescError: AnsiString): Boolean;
         var
        	PRNConfig: TRBConfig;
    begin
        try
            Result := False;
            PRNConfig := RBInterface.GetConfig;
            PRNConfig.DeviceType := 'Screen';
            PRNConfig.Orientation := poPortrait;
            PRNConfig.PaperName := 'A4';
            PRNConfig.PaperHeight := 297;
            PRNConfig.PaperWidth := 210;
            PRNConfig.Copies := CantidadCopias;
            RBInterface.SetConfig(PRNConfig);
            Result := True;
        except
            On E:Exception do begin
                DescError := E.Message;
            end;
        end;
    end;

    {******************************** Function Header ******************************
    Function Name: ObtenerCantidadCopias
    Author : pdominguez
    Date Created : 06/08/2009
    Parameters : var DescError: AnsiString
    Return Value : Boolean
    Description : SS 733
        - Obtiene la cantidad de Copias a imprimir el Estado del Convenio dependiendo
        de la Personer�a del Cliente.
    *******************************************************************************}
    function ObtenerCantidadCopias(var DescError: AnsiString): Boolean;
    begin
        if FDatosConvenio.Cliente.Datos.Personeria = PERSONERIA_FISICA then begin
            Result := ObtenerParametroGeneral(DMConnections.BaseCAC,CAN_COPIAS_CONVENIO_FISICA,CantidadCopias);
            if not Result then DescError := Format(ERROR_PARAMETRO_GENERAL,[CAN_COPIAS_CONVENIO_FISICA]);

        end else begin
            Result := ObtenerParametroGeneral(DMConnections.BaseCAC,CAN_COPIAS_CONVENIO_JURIDICA,CantidadCopias);
            if not Result then DescError := Format(ERROR_PARAMETRO_GENERAL,[CAN_COPIAS_CONVENIO_JURIDICA]);
        end;
    end;

    {******************************** Function Header ******************************
    Function Name   : ObtenerCantidadRegPrimeraPagina
    Author          : CQuezada
    Date Created    : 12/09/2012
    Parameters      : var DescError: AnsiString
    Return Value    : Boolean
    Description     : Obtiene la cantidad de registros a desplegar en la primera p�gina
                      al imprimir el Estado del Convenio dependiendo de la Personer�a del Cliente.
    Firma           : SS_1069_CQU_20120911
    *******************************************************************************}
    function ObtenerCantidadRegPrimeraPagina(var DescError : AnsiString) : Boolean;
    begin
        if FDatosConvenio.Cliente.Datos.Personeria = PERSONERIA_FISICA then begin
            Result := ObtenerParametroGeneral(DMConnections.BaseCAC, REG_PRIMERA_PAG_FISICA, CantRegPrimeraPagina);
            if not Result then DescError := Format(ERROR_PARAMETRO_GENERAL, [REG_PRIMERA_PAG_FISICA]);
        end else begin
            Result := ObtenerParametroGeneral(DMConnections.BaseCAC, REG_PRIMERA_PAG_JURIDICA, CantRegPrimeraPagina);
            if not Result then DescError := Format(ERROR_PARAMETRO_GENERAL, [REG_PRIMERA_PAG_JURIDICA]);
        end;
    end;

begin
    // Revisamos si el tipo de hoja establecido, es el correcto para el reporte.
	if tthConvenio <> Hoja then
        if MsgBox(MSG_CAPTION_PAPEL_CONVENIO,MSG_CAPTION_IMPRESION_CONVENIOS,MB_ICONWARNING+MB_YESNO) = mrNo then Exit;

    try
        Hoja := tthConvenio;
        Result := True;
        DescError := '';

        if ObtenerCantidadCopias(DescError) then begin
            if spObtenerReporteEstadoConvenio.Active then spObtenerReporteEstadoConvenio.Close;
            if spObtenerReporteEstadoConvenio2.Active then spObtenerReporteEstadoConvenio2.Close;

            // Asignamos el total de vehiculos a imprimir en la primera hoja, dependiendo de la Personer�a.
            //if FDatosConvenio.Cliente.Datos.Personeria = PERSONERIA_FISICA then                                               // SS_1069_CQU_20120911
            //    spObtenerReporteEstadoConvenio.Parameters.ParamByName('@RegistrosPrimeraPagina').Value := 17                  // SS_1069_CQU_20120911
            //else spObtenerReporteEstadoConvenio.Parameters.ParamByName('@RegistrosPrimeraPagina').Value := 13;                // SS_1069_CQU_20120911

            if not ObtenerCantidadRegPrimeraPagina(DescError) then begin                                                        // SS_1069_CQU_20120911
                MsgBoxErr(ERROR_EN_PROCESO, DescError, Self.Caption, MB_ICONERROR);                                             // SS_1069_CQU_20120911
                Exit;                                                                                                           // SS_1069_CQU_20120911
            end else begin                                                                                                      // SS_1069_CQU_20120911
                spObtenerReporteEstadoConvenio.Parameters.ParamByName('@RegistrosPrimeraPagina').Value := CantRegPrimeraPagina; // SS_1069_CQU_20120911
            end;                                                                                                                // SS_1069_CQU_20120911

            // Obtenemos los datos de los veh�culos a imprimir para la primera hoja y para el Anexo.
            spObtenerReporteEstadoConvenio.Parameters.ParamByName('@CodigoConvenio').Value := FDatosConvenio.CodigoConvenio;
            spObtenerReporteEstadoConvenio.Parameters.ParamByName('@IndiceVehiculo').Value := 0;
            spObtenerReporteEstadoConvenio.Open;

            spObtenerReporteEstadoConvenio.Last;
            spObtenerReporteEstadoConvenio2.Parameters.ParamByName('@CodigoConvenio').Value := FDatosConvenio.CodigoConvenio;
            spObtenerReporteEstadoConvenio2.Parameters.ParamByName('@IndiceVehiculo').Value := spObtenerReporteEstadoConvenio.FieldByName('IndiceVehiculo').AsInteger;
            spObtenerReporteEstadoConvenio2.Open;
            spObtenerReporteEstadoConvenio.First;


            // Preparamos la impresi�n del Estado de Convenio dependiendo de la Personer�a.
            if FDatosConvenio.Cliente.Datos.Personeria = PERSONERIA_FISICA then begin
                if PrepararEstadoConvenioNatural(DescError) then begin
                    // Sino hay datos que imprimir en el anexo, cerramos el recordset y
                    // deshabilitamos la impresi�n del subReporte
                    if spObtenerReporteEstadoConvenio2.IsEmpty then begin
                        spObtenerReporteEstadoConvenio2.Close;
                        ppSubReport2.Visible := False;
                    end;
                end else Result := False;
            end else
                if PrepararEstadoConvenioJuridico(DescError) then begin
                    // Sino hay datos que imprimir en el anexo, cerramos el recordset y
                    // deshabilitamos la impresi�n del subReporte
                    if spObtenerReporteEstadoConvenio2.IsEmpty then begin
                        spObtenerReporteEstadoConvenio2.Close;
                        ppSubReport6.Visible := False;
                    end;
                end else Result := False;

            if Result then begin
                if PrepararRBInterface(DescError) then
                    RBInterface.Execute(True)
                else Result := False;
            end;
        end;

        if not Result then MsgBoxErr(ERROR_EN_PROCESO, DescError, Self.Caption, MB_ICONERROR);
    except
        on E:Exception do begin
            MsgBoxErr(ERROR_EN_PROCESO, E.Message, Self.Caption, MB_ICONERROR);
        end;
    end;
end;

{******************************** Function Header ******************************
Procedure Name: Inicializar
Author : pdominguez
Date Created : 06/08/2009
Parameters : None
Return Value : Boolean
Description : SS 733
    - Asignamos los Datos del convenio a la variable privada FDatosConvenio para
    tener todos los datos accesibles.
*******************************************************************************}
procedure TFormImprimirConvenio.Inicializar(DatosConvenio: TDatosConvenio);
var
  sRazonSocial,sRut,sDireccion,sLocalidad: AnsiString;                                                //SS_1147_NDR_20140710
begin

    with spObtenerMaestroConcesionaria do                                                          //SS_1147_NDR_20140710
    begin                                                                                          //SS_1147_NDR_20140710
      Close;                                                                                       //SS_1147_NDR_20140710
      Parameters.Refresh;                                                                          //SS_1147_NDR_20140710
      Parameters.ParamByName('@CodigoConcesionaria').Value := ObtenerCodigoConcesionariaNativa;    //SS_1147_NDR_20140710
      Open;                                                                                        //SS_1147_NDR_20140710
      sRazonSocial  := FieldByName('RazonSocialConcesionaria').AsString;                           //SS_1147_NDR_20140710
      sRut          := FormatFloat('#.#',FieldByName('RutConcesionaria').AsInteger) + '-' +        //SS_1147_NDR_20140710
                       FieldByName('DigitoVerificadorConcesionaria').AsString ;                    //SS_1147_NDR_20140710
      sDireccion    := Trim(FieldByName('DireccionConcesionaria').AsString) + ' - ' +              //SS_1147_NDR_20140710
                       Trim(FieldByName('Comuna').AsString) + ' - ' +                              //SS_1147_NDR_20140710
                       Trim(FieldByName('Region').AsString);                                       //SS_1147_NDR_20140710

      pplabel52.Caption := sRazonSocial;                                                           //SS_1147_NDR_20140710
      pplabel53.Caption := sRut;                                                                   //SS_1147_NDR_20140710
      pplabel54.Caption := sDireccion;                                                             //SS_1147_NDR_20140710

      pplabel141.Caption := sRazonSocial;                                                          //SS_1147_NDR_20140710
      pplabel142.Caption := sRut;                                                                  //SS_1147_NDR_20140710
      pplabel143.Caption := sDireccion;                                                            //SS_1147_NDR_20140710
    end;                                                                                           //SS_1147_NDR_20140710

    FDatosConvenio := DatosConvenio;
end;

{
    Function Name: CargaPlantillaRTF
    Parameters : Plantilla: String
    Return Value: String
    Author : pdominguez
    Date Created : 07/06/2010

    Description : Carga una plantilla RTF y devuelve su contenido a�n estando
        esta abierta por otro proceso.
}
function TFormImprimirConvenio.CargaPlantillaRTF(Plantilla: String): String;
    resourcestring
        MSG_ERROR_EN_CARGA =
            'Se produjo un error en la carga de la plantilla: %s' + CRLF + CRLF +
            'La plantilla est� en blanco o est� bloqueda por otro Usuario.';
begin
    try
        Result := FileToString(Plantilla, fmShareDenyNone);
    finally
        if Trim(Result) = '' then Raise Exception.Create(Format(MSG_ERROR_EN_CARGA, [Plantilla]));
    end;
end;

//BEGIN:SS_660_MVI_20130909 -------------------------------------------------
{*******************************************************************************
    Procedure Name  : VerificaConvenioListaAmarilla
    Author          : MVillarroel
    Date            : 13/09/2013
    Firma           : SS_660_MVI_20130909
    Description     : Se varifica si est� en Lista Amarilla por CodigoConvenio
*******************************************************************************}
function TFormImprimirConvenio.VerificaConvenioListaAmarilla(CodigoConvenio : Integer) : Boolean;
resourcestring
    SQL_CONSULTACONVENIO = 'SELECT dbo.ObtenerEstadoConvenioEnListaAmarilla(%d)';
    CONST_MGS_ERROR_LISTA_AMARILLA  = 'Error al determinar si convenio est� en Lista Amarilla';
    CONST_MGS_ERROR_IMPRESION  = 'Error de impresi�n';
begin
    Result      :=  False;
    if CodigoConvenio > 0 then begin
      try
          Result:= QueryGetValue(   DMConnections.BaseCAC,
                               Format(SQL_CONSULTACONVENIO,
                                             [   CodigoConvenio
                                             ]
                                     )
                          ) <> '';

      except
          on e: Exception do begin
         			MsgBoxErr(CONST_MGS_ERROR_LISTA_AMARILLA, e.Message, CONST_MGS_ERROR_IMPRESION, MB_ICONERROR);
          end;
      end;
    end;
end;
//END:SS_660_MVI_20130909 -------------------------------------------------

//INICIO: TASK_036_ECA_20160621
procedure TFormImprimirConvenio.ObtenerDireccion(CodigoConvenio, CodigoPersona: Integer; DomicilioConvenio: Boolean; out Direccion: string; out Comuna: string);
var
  sp: TADOStoredProc;
begin
    sp:= TADOStoredProc.Create(nil);
    sp.Connection:=DMConnections.BaseCAC;

    if DomicilioConvenio then
    begin
        sp.ProcedureName := 'ObtenerDomicilioConvenio';
        sp.Parameters.Refresh;
        sp.Parameters.ParamByName('@CodigoConvenio').Value  := CodigoConvenio;
        sp.Open;
        if not sp.Eof then
        begin
            Direccion  := sp.FieldByName('DireccionCompleta').AsString;
            Comuna     := sp.FieldByName('DescriComuna').AsString;
        end;
    end
    else
    begin
        sp.ProcedureName := 'ObtenerDomicilioPrincipalPersona';
        sp.Parameters.Refresh;
        sp.Parameters.ParamByName('@CodigoPersona').Value  := CodigoPersona;
        sp.Open;
        if not sp.Eof then
        begin
            Direccion  := sp.FieldByName('DireccionCompleta').AsString;
            Comuna     := sp.FieldByName('DescriComuna').AsString;
        end;
    end;
    sp.Close;
    FreeAndNil(sp);
end;

function TFormImprimirConvenio.ObtenerTipoPagoConvenio(CodigoConvenio, CodigoPersona: Integer): Integer;
var
  sp: TADOStoredProc;
begin
    Result := 0;
    TRY
        try
            sp:= TADOStoredProc.Create(nil);
            sp.Connection:=DMConnections.BaseCAC;
            sp.ProcedureName := 'CRM_TipoPagoConvenio_SELECT';
            sp.Parameters.Refresh;
            sp.Parameters.ParamByName('@CodigoPersona').Value  := CodigoPersona;
            sp.Parameters.ParamByName('@CodigoConvenio').Value  := CodigoConvenio;
            sp.Open;

            if not sp.Eof then
            begin
                Result := sp.FieldByName('TipoMedioPago').AsInteger;
            end;
        except
            on e: Exception do begin
                Result:=0;
            end;
        end;
    FINALLY
        sp.Close;
        FreeAndNil(sp);
    END;
end;

procedure TFormImprimirConvenio.ObtenerMedioContacto(CodigoPersona: Integer; out CodigoArea: string; out Telefono: string; out Celular: string);
var
  sp: TADOStoredProc;
begin
    sp:= TADOStoredProc.Create(nil);
    sp.Connection:=DMConnections.BaseCAC;
    sp.ProcedureName := 'ObtenerMediosComunicacionPersona';
    sp.Parameters.Refresh;
    sp.Parameters.ParamByName('@CodigoPersona').Value  := CodigoPersona;
    sp.Parameters.ParamByName('@CodigoTipoMedioContacto').Value  := null;
    sp.Open;

    if not sp.Eof then
    begin
        while not sp.Eof do
        begin
            if (sp.FieldByName('CodigoTipoMedioContacto').AsInteger=2) or (sp.FieldByName('CodigoTipoMedioContacto').AsInteger=3) then
            begin
                CodigoArea:=sp.FieldByName('CodigoArea').AsString;
                Telefono:=sp.FieldByName('Valor').AsString
            end;

            if sp.FieldByName('CodigoTipoMedioContacto').AsInteger=5  then
                Celular:=sp.FieldByName('Valor').AsString;
        
            sp.Next;
        end;

    end;
    sp.Close;
    FreeAndNil(sp);
end;

procedure TFormImprimirConvenio.ObtenerDatosCaptador(CodigoUsuario: string; Out Nombre: string; out NumeroDocumento: string);
var
  sp: TADOStoredProc;
begin
    sp:= TADOStoredProc.Create(nil);
    sp.Connection:=DMConnections.BaseCAC;
    sp.ProcedureName := 'CRM_DatosUsuarioSistema_SELECT';
    sp.Parameters.Refresh;
    sp.Parameters.ParamByName('@CodigoUsuario').Value  := CodigoUsuario;
    sp.Parameters.ParamByName('@Nombre').Value  := null;
    sp.Parameters.ParamByName('@NumeroDocumento').Value  := null;
    sp.ExecProc;

    Nombre:=sp.Parameters.ParamByName('@Nombre').Value;
    NumeroDocumento:=sp.Parameters.ParamByName('@NumeroDocumento').Value;

    FreeAndNil(sp)
end;

function TFormImprimirConvenio.ImprimirAltaSuscripcionListaBlanca(CodigoConvenio, CodigoPersona: Integer; NumeroDocumento, Nombre, Email: string; Cuentas: array of TCuentaVehiculo;
MedioPago: Integer; ClienteAdheridoEnvioEMail, DireccionConvenio: Boolean; Fecha:TDateTime) : Boolean;
resourcestring
    ERROR_EN_PROCESO = 'Se produjo un Error en la Impresi�n del Contrato de Suscripci�n.';
var
    Direccion, Comuna: string;
    TipoPago: Integer;
    CodigoArea, Telefono, Celular: string;
    NombreCaptador: string;
    RUTCaptador: string;
    varDSource : TDataSource;
    dPipeLine : TppDBPipeline;
    i: Integer;
begin
    try
        Result:=False;

        //lbl_RUTS1.Caption           := NumeroDocumento; //TASK_049_ECA_20160706

        lbl_NumeroConvenioS.Caption :=  Trim(QueryGetStringValue(DMConnections.BaseCAC, 'SELECT dbo.ObtenerNumeroConvenio(' + IntToStr(CodigoConvenio) +')' ));
        {INICIO		: 20160928 CFU TASK_043_CFU_20160928
        lbl_RUTS2.Caption           := NumeroDocumento;
        }
        NumeroDocumento				:= ReplaceStr(ReplaceStr(NumeroDocumento, '-', ''), '.', '');
        lbl_RUTS2.Caption           := QueryGetValue(DMConnections.BaseCAC, 'select dbo.FormatearRutConPuntos(''' + Trim(NumeroDocumento) + ''')');
        //TERMINO	: 20160928 CFU TASK_043_CFU_20160928
        lbl_NombreCliente.Caption   := Nombre;

        ObtenerDireccion(CodigoConvenio,CodigoPersona,DireccionConvenio,Direccion,Comuna);

        lbl_DireccionS.Caption      := Direccion;
        lbl_ComunaS.Caption         := Comuna;

        ObtenerMedioContacto(CodigoPersona,CodigoArea, Telefono, Celular);

        lbl_CodS.Caption            := CodigoArea;
        lbl_NumeroTlfS.Caption      := Telefono;
        lbl_CelularS.Caption        := Celular;
        lbl_EmailS.Caption          := Email;
        lbl_FechaS.Caption          := formatdatetime('dd/mm/yyyy', Fecha);

        //INICIO: TASK_049_ECA_20160706
        //Rtxt_Patentes.RichText      := Copy(Patentes,1,length(Patentes)-1);

        dsContratoArrendamientoTAG.Active := True;
        dsContratoArrendamientoTAG.EmptyDataSet;

        for i := 0 to Length(Cuentas) - 1 do
        begin
            dsContratoArrendamientoTAG.Append;
            dsContratoArrendamientoTAG.FieldByName('Patente').Value := Cuentas[i].Vehiculo.Patente;
            dsContratoArrendamientoTAG.FieldByName('ClaseVehiculo').Value := Cuentas[i].Vehiculo.Tipo;
            dsContratoArrendamientoTAG.FieldByName('Marca').Value := Cuentas[i].Vehiculo.Marca;
            dsContratoArrendamientoTAG.FieldByName('Modelo').Value := Cuentas[i].Vehiculo.Modelo;
        end;

        varDSource := TDataSource.Create(nil);
        varDSource.DataSet := dsContratoArrendamientoTAG;
        dpipeLine := TppDBPipeline.Create(nil);
        dpipeLine.DataSource := varDSource;
        FAltaSuscripcion.DataPipeline := dpipeLine;
        //FIN: TASK_049_ECA_20160706

        //INICIO: TASK_054_ECA_20160708
        if ClienteAdheridoEnvioEMail then
           ppImageImgBoleta.Visible  :=True
        else
        begin
            ClienteAdheridoEnvioEMail:= QueryGetBooleanValue(DMConnections.BaseCAC, Format('SELECT dbo.ValidarConveniosPersonaEnvioPorEMail(%d)', [CodigoPersona]));
            if ClienteAdheridoEnvioEMail then
               ppImageImgBoleta.Visible  :=True
         end;
        //FIN: TASK_054_ECA_20160708
        
        TipoPago:= ObtenerTipoPagoConvenio(CodigoConvenio, CodigoPersona);

        if (TipoPago=1) or (TipoPago=2) then
             ppImageImgPATPAC.Visible:=True
        else
            ppImageImgOtros.Visible:=True;

        ObtenerDatosCaptador(UsuarioSistema,NombreCaptador,RUTCaptador);
        lbl_NombreCaptador.Caption  := NombreCaptador;
        lbl_RutCaptador.Caption     := RUTCaptador;

        rpAltaContratoSuscripcion.PrinterSetup.Copies := 1;
        rbInterface.Report := rpAltaContratoSuscripcion;
       // Inicia la impresion...
        Result := Imprimir(tthBlanco, 1, True, '');
    except
        On E:Exception do begin
            MsgBoxErr(ERROR_EN_PROCESO, E.Message, Self.Caption, MB_ICONERROR);
        end;
    end;
end;
//FIN: TASK_036_ECA_20160621

end.
