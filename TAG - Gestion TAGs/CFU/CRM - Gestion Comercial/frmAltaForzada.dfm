object FormAltaForzada: TFormAltaForzada
  Left = 0
  Top = 0
  BorderIcons = []
  Caption = 'Deshacer Baja Forzada'
  ClientHeight = 251
  ClientWidth = 645
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    645
    251)
  PixelsPerInch = 96
  TextHeight = 13
  object lbl1: TLabel
    Left = 8
    Top = 8
    Width = 600
    Height = 26
    Caption = 
      'Las siguientes Patentes se dieron de alta en el Convenio actual ' +
      'provocando una Baja Forzada en un Convenio de otra Concesionaria' +
      '. Seleccione aquellas Bajas Forzadas que desea deshacer, lo que ' +
      'dar'#225' nuevamente de alta el Veh'#237'culo for'#225'neo.'
    WordWrap = True
  end
  object bvl1: TBevel
    Left = 8
    Top = 206
    Width = 629
    Height = 4
    Anchors = [akLeft, akRight, akBottom]
    Shape = bsTopLine
    ExplicitTop = 207
    ExplicitWidth = 657
  end
  object dbgrdBajaForzada: TDBGrid
    Left = 8
    Top = 40
    Width = 629
    Height = 160
    Anchors = [akLeft, akTop, akRight, akBottom]
    DataSource = dsBajaForzada
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnCellClick = dbgrdBajaForzadaCellClick
    OnColEnter = dbgrdBajaForzadaColEnter
    OnColExit = dbgrdBajaForzadaColExit
    OnDrawColumnCell = dbgrdBajaForzadaDrawColumnCell
    OnKeyDown = dbgrdBajaForzadaKeyDown
    Columns = <
      item
        Expanded = False
        FieldName = 'CodigoBajaForzada'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'Deshacer'
        Title.Caption = ' '
        Width = 21
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Patente'
        Width = 51
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TagNativo'
        Title.Caption = 'TAG Nativo'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'ConvenioForaneo'
        Title.Caption = 'Convenio For'#225'neo'
        Width = 94
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TagForaneo'
        Title.Caption = 'TAG For'#225'neo'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'RUTForaneo'
        Title.Caption = 'RUT For'#225'neo'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NombreForaneo'
        Title.Caption = 'Nombre For'#225'neo'
        Width = 189
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FechaHora'
        Title.Caption = 'Fecha Baja'
        Width = 106
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CodigoUsuario'
        Title.Caption = 'Usuario'
        Width = 97
        Visible = True
      end>
  end
  object btn_ok: TButton
    Left = 562
    Top = 218
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Aceptar'
    Default = True
    TabOrder = 1
    OnClick = btn_okClick
  end
  object cdsBajaForzada: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CodigoBajaForzada'
        DataType = ftInteger
      end
      item
        Name = 'Deshacer'
        DataType = ftBoolean
      end
      item
        Name = 'Patente'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'TagNativo'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'ConvenioForaneo'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'TagForaneo'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'RUTForaneo'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'NombreForaneo'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'FechaHora'
        DataType = ftDateTime
      end
      item
        Name = 'CodigoUsuario'
        DataType = ftString
        Size = 20
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 192
    Top = 80
  end
  object dsBajaForzada: TDataSource
    DataSet = cdsBajaForzada
    Left = 280
    Top = 80
  end
end
